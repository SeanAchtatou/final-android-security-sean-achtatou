package com.android.mms.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.List;
import vc.lx.sms2.R;

public class IconListAdapter extends ArrayAdapter<IconListItem> {
    private static final int mResource = 2130903092;
    protected LayoutInflater mInflater;

    public static class IconListItem {
        private final int mResource;
        private final String mTitle;

        public IconListItem(String str, int i) {
            this.mResource = i;
            this.mTitle = str;
        }

        public int getResource() {
            return this.mResource;
        }

        public String getTitle() {
            return this.mTitle;
        }
    }

    public IconListAdapter(Context context, List<IconListItem> list) {
        super(context, (int) R.layout.icon_list_item, list);
        this.mInflater = (LayoutInflater) context.getSystemService("layout_inflater");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i, View view, ViewGroup viewGroup) {
        View inflate = view == null ? this.mInflater.inflate((int) R.layout.icon_list_item, viewGroup, false) : view;
        ((TextView) inflate.findViewById(R.id.text1)).setText(((IconListItem) getItem(i)).getTitle());
        ((ImageView) inflate.findViewById(R.id.icon)).setImageResource(((IconListItem) getItem(i)).getResource());
        return inflate;
    }
}
