package com.android.mms.ui;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;
import com.android.drm.mobile1.DrmException;
import com.android.mms.model.AudioModel;
import com.android.mms.model.ImageModel;
import com.android.mms.model.LayoutModel;
import com.android.mms.model.MediaModel;
import com.android.mms.model.Model;
import com.android.mms.model.RegionMediaModel;
import com.android.mms.model.RegionModel;
import com.android.mms.model.SlideModel;
import com.android.mms.model.SlideshowModel;
import com.android.mms.model.TextModel;
import com.android.mms.model.VideoModel;
import com.android.mms.ui.AdaptableSlideViewInterface;
import java.util.Iterator;
import vc.lx.sms2.R;

public class SlideshowPresenter extends Presenter {
    private static final boolean DEBUG = false;
    private static final boolean LOCAL_LOGV = false;
    private static final String TAG = "SlideshowPresenter";
    protected final Handler mHandler = new Handler();
    protected float mHeightTransformRatio;
    protected int mLocation = 0;
    protected final int mSlideNumber = ((SlideshowModel) this.mModel).size();
    private final AdaptableSlideViewInterface.OnSizeChangedListener mViewSizeChangedListener = new AdaptableSlideViewInterface.OnSizeChangedListener() {
        public void onSizeChanged(int i, int i2) {
            LayoutModel layout = ((SlideshowModel) SlideshowPresenter.this.mModel).getLayout();
            SlideshowPresenter.this.mWidthTransformRatio = SlideshowPresenter.this.getWidthTransformRatio(i, layout.getLayoutWidth());
            SlideshowPresenter.this.mHeightTransformRatio = SlideshowPresenter.this.getHeightTransformRatio(i2, layout.getLayoutHeight());
        }
    };
    protected float mWidthTransformRatio;

    public SlideshowPresenter(Context context, ViewInterface viewInterface, Model model) {
        super(context, viewInterface, model);
        if (viewInterface instanceof AdaptableSlideViewInterface) {
            ((AdaptableSlideViewInterface) viewInterface).setOnSizeChangedListener(this.mViewSizeChangedListener);
        }
    }

    /* access modifiers changed from: private */
    public float getHeightTransformRatio(int i, int i2) {
        if (i > 0) {
            return ((float) i2) / ((float) i);
        }
        return 1.0f;
    }

    /* access modifiers changed from: private */
    public float getWidthTransformRatio(int i, int i2) {
        if (i > 0) {
            return ((float) i2) / ((float) i);
        }
        return 1.0f;
    }

    private int transformHeight(int i) {
        return (int) (((float) i) / this.mHeightTransformRatio);
    }

    private int transformWidth(int i) {
        return (int) (((float) i) / this.mWidthTransformRatio);
    }

    public int getLocation() {
        return this.mLocation;
    }

    public void goBackward() {
        if (this.mLocation > 0) {
            this.mLocation--;
        }
    }

    public void goForward() {
        if (this.mLocation < this.mSlideNumber - 1) {
            this.mLocation++;
        }
    }

    public void onModelChanged(final Model model, final boolean z) {
        final SlideViewInterface slideViewInterface = (SlideViewInterface) this.mView;
        if (!(model instanceof SlideshowModel)) {
            if (model instanceof SlideModel) {
                if (((SlideModel) model).isVisible()) {
                    this.mHandler.post(new Runnable() {
                        public void run() {
                            SlideshowPresenter.this.presentSlide(slideViewInterface, (SlideModel) model);
                        }
                    });
                } else {
                    this.mHandler.post(new Runnable() {
                        public void run() {
                            SlideshowPresenter.this.goForward();
                        }
                    });
                }
            } else if (!(model instanceof MediaModel)) {
                if (model instanceof RegionModel) {
                }
            } else if (model instanceof RegionMediaModel) {
                this.mHandler.post(new Runnable() {
                    public void run() {
                        try {
                            SlideshowPresenter.this.presentRegionMedia(slideViewInterface, (RegionMediaModel) model, z);
                        } catch (DrmException e) {
                            Log.e(SlideshowPresenter.TAG, e.getMessage(), e);
                            Toast.makeText(SlideshowPresenter.this.mContext, SlideshowPresenter.this.mContext.getString(R.string.insufficient_drm_rights), 0).show();
                        }
                    }
                });
            } else if (((MediaModel) model).isAudio()) {
                this.mHandler.post(new Runnable() {
                    public void run() {
                        try {
                            SlideshowPresenter.this.presentAudio(slideViewInterface, (AudioModel) model, z);
                        } catch (DrmException e) {
                            Log.e(SlideshowPresenter.TAG, e.getMessage(), e);
                            Toast.makeText(SlideshowPresenter.this.mContext, SlideshowPresenter.this.mContext.getString(R.string.insufficient_drm_rights), 0).show();
                        }
                    }
                });
            }
        }
    }

    public void present() {
        presentSlide((SlideViewInterface) this.mView, ((SlideshowModel) this.mModel).get(this.mLocation));
    }

    /* access modifiers changed from: protected */
    public void presentAudio(SlideViewInterface slideViewInterface, AudioModel audioModel, boolean z) throws DrmException {
        if (z) {
            slideViewInterface.setAudio(audioModel.getUriWithDrmCheck(), audioModel.getSrc(), audioModel.getExtras());
        }
        MediaModel.MediaAction currentAction = audioModel.getCurrentAction();
        if (currentAction == MediaModel.MediaAction.START) {
            slideViewInterface.startAudio();
        } else if (currentAction == MediaModel.MediaAction.PAUSE) {
            slideViewInterface.pauseAudio();
        } else if (currentAction == MediaModel.MediaAction.STOP) {
            slideViewInterface.stopAudio();
        } else if (currentAction == MediaModel.MediaAction.SEEK) {
            slideViewInterface.seekAudio(audioModel.getSeekTo());
        }
    }

    /* access modifiers changed from: protected */
    public void presentImage(SlideViewInterface slideViewInterface, ImageModel imageModel, RegionModel regionModel, boolean z) throws DrmException {
        if (z) {
            slideViewInterface.setImage(imageModel.getSrc(), imageModel.getBitmapWithDrmCheck());
        }
        if (slideViewInterface instanceof AdaptableSlideViewInterface) {
            ((AdaptableSlideViewInterface) slideViewInterface).setImageRegion(transformWidth(regionModel.getLeft()), transformHeight(regionModel.getTop()), transformWidth(regionModel.getWidth()), transformHeight(regionModel.getHeight()));
        }
        slideViewInterface.setImageRegionFit(regionModel.getFit());
        slideViewInterface.setImageVisibility(imageModel.isVisible());
    }

    /* access modifiers changed from: protected */
    public void presentRegionMedia(SlideViewInterface slideViewInterface, RegionMediaModel regionMediaModel, boolean z) throws DrmException {
        RegionModel region = regionMediaModel.getRegion();
        if (regionMediaModel.isText()) {
            presentText(slideViewInterface, (TextModel) regionMediaModel, region, z);
        } else if (regionMediaModel.isImage()) {
            presentImage(slideViewInterface, (ImageModel) regionMediaModel, region, z);
        } else if (regionMediaModel.isVideo()) {
            presentVideo(slideViewInterface, (VideoModel) regionMediaModel, region, z);
        }
    }

    /* access modifiers changed from: protected */
    public void presentSlide(SlideViewInterface slideViewInterface, SlideModel slideModel) {
        slideViewInterface.reset();
        try {
            Iterator<MediaModel> it = slideModel.iterator();
            while (it.hasNext()) {
                MediaModel next = it.next();
                if (next instanceof RegionMediaModel) {
                    presentRegionMedia(slideViewInterface, (RegionMediaModel) next, true);
                } else if (next.isAudio()) {
                    presentAudio(slideViewInterface, (AudioModel) next, true);
                }
            }
        } catch (DrmException e) {
            Log.e(TAG, e.getMessage(), e);
            Toast.makeText(this.mContext, this.mContext.getString(R.string.insufficient_drm_rights), 0).show();
        }
    }

    /* access modifiers changed from: protected */
    public void presentText(SlideViewInterface slideViewInterface, TextModel textModel, RegionModel regionModel, boolean z) {
        if (z) {
            slideViewInterface.setText(textModel.getSrc(), textModel.getText());
        }
        if (slideViewInterface instanceof AdaptableSlideViewInterface) {
            ((AdaptableSlideViewInterface) slideViewInterface).setTextRegion(transformWidth(regionModel.getLeft()), transformHeight(regionModel.getTop()), transformWidth(regionModel.getWidth()), transformHeight(regionModel.getHeight()));
        }
        slideViewInterface.setTextVisibility(textModel.isVisible());
    }

    /* access modifiers changed from: protected */
    public void presentVideo(SlideViewInterface slideViewInterface, VideoModel videoModel, RegionModel regionModel, boolean z) throws DrmException {
        if (z) {
            slideViewInterface.setVideo(videoModel.getSrc(), videoModel.getUriWithDrmCheck());
        }
        if (slideViewInterface instanceof AdaptableSlideViewInterface) {
            ((AdaptableSlideViewInterface) slideViewInterface).setVideoRegion(transformWidth(regionModel.getLeft()), transformHeight(regionModel.getTop()), transformWidth(regionModel.getWidth()), transformHeight(regionModel.getHeight()));
        }
        slideViewInterface.setVideoVisibility(videoModel.isVisible());
        MediaModel.MediaAction currentAction = videoModel.getCurrentAction();
        if (currentAction == MediaModel.MediaAction.START) {
            slideViewInterface.startVideo();
        } else if (currentAction == MediaModel.MediaAction.PAUSE) {
            slideViewInterface.pauseVideo();
        } else if (currentAction == MediaModel.MediaAction.STOP) {
            slideViewInterface.stopVideo();
        } else if (currentAction == MediaModel.MediaAction.SEEK) {
            slideViewInterface.seekVideo(videoModel.getSeekTo());
        }
    }

    public void setLocation(int i) {
        this.mLocation = i;
    }
}
