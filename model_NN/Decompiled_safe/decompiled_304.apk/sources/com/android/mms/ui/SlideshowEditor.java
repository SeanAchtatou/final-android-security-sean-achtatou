package com.android.mms.ui;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import com.android.mms.model.AudioModel;
import com.android.mms.model.ImageModel;
import com.android.mms.model.MediaModel;
import com.android.mms.model.RegionModel;
import com.android.mms.model.SlideModel;
import com.android.mms.model.SlideshowModel;
import com.android.mms.model.TextModel;
import com.android.mms.model.VideoModel;
import com.google.android.mms.ContentType;
import com.google.android.mms.MmsException;

public class SlideshowEditor {
    private static final int MAX_SLIDE_NUM = 10;
    private static final String TAG = "Mms:slideshow";
    private final Context mContext;
    private final SlideshowModel mModel;

    public SlideshowEditor(Context context, SlideshowModel slideshowModel) {
        this.mContext = context;
        this.mModel = slideshowModel;
    }

    public boolean addNewSlide() {
        return addNewSlide(this.mModel.size());
    }

    public boolean addNewSlide(int i) {
        int size = this.mModel.size();
        if (size < 10) {
            SlideModel slideModel = new SlideModel(this.mModel);
            slideModel.add((MediaModel) new TextModel(this.mContext, ContentType.TEXT_PLAIN, "text_" + size + ".txt", this.mModel.getLayout().getTextRegion()));
            this.mModel.add(i, slideModel);
            return true;
        }
        Log.w(TAG, "The limitation of the number of slides is reached.");
        return false;
    }

    public void changeAudio(int i, Uri uri) throws MmsException {
        AudioModel audioModel = new AudioModel(this.mContext, uri);
        SlideModel slideModel = this.mModel.get(i);
        slideModel.add((MediaModel) audioModel);
        slideModel.updateDuration(audioModel.getDuration());
    }

    public void changeDuration(int i, int i2) {
        if (i2 >= 0) {
            this.mModel.get(i).setDuration(i2);
        }
    }

    public void changeImage(int i, Uri uri) throws MmsException {
        this.mModel.get(i).add((MediaModel) new ImageModel(this.mContext, uri, this.mModel.getLayout().getImageRegion()));
    }

    public void changeLayout(int i) {
        this.mModel.getLayout().changeTo(i);
    }

    public void changeText(int i, String str) {
        if (str != null) {
            SlideModel slideModel = this.mModel.get(i);
            TextModel text = slideModel.getText();
            if (text == null) {
                TextModel textModel = new TextModel(this.mContext, ContentType.TEXT_PLAIN, "text_" + i + ".txt", this.mModel.getLayout().getTextRegion());
                textModel.setText(str);
                slideModel.add((MediaModel) textModel);
            } else if (!str.equals(text.getText())) {
                text.setText(str);
            }
        }
    }

    public void changeVideo(int i, Uri uri) throws MmsException {
        VideoModel videoModel = new VideoModel(this.mContext, uri, this.mModel.getLayout().getImageRegion());
        SlideModel slideModel = this.mModel.get(i);
        slideModel.add((MediaModel) videoModel);
        slideModel.updateDuration(videoModel.getDuration());
    }

    public RegionModel getImageRegion() {
        return this.mModel.getLayout().getImageRegion();
    }

    public RegionModel getTextRegion() {
        return this.mModel.getLayout().getTextRegion();
    }

    public void moveSlideDown(int i) {
        this.mModel.add(i + 1, this.mModel.remove(i));
    }

    public void moveSlideUp(int i) {
        this.mModel.add(i - 1, this.mModel.remove(i));
    }

    public void removeAllSlides() {
        while (this.mModel.size() > 0) {
            removeSlide(0);
        }
    }

    public boolean removeAudio(int i) {
        return this.mModel.get(i).removeAudio();
    }

    public boolean removeImage(int i) {
        return this.mModel.get(i).removeImage();
    }

    public void removeSlide(int i) {
        this.mModel.remove(i);
    }

    public boolean removeText(int i) {
        return this.mModel.get(i).removeText();
    }

    public boolean removeVideo(int i) {
        return this.mModel.get(i).removeVideo();
    }
}
