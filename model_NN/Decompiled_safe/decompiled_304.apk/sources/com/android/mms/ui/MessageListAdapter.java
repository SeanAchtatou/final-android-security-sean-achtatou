package com.android.mms.ui;

import android.content.Context;
import android.database.Cursor;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CursorAdapter;
import android.widget.ListView;
import com.android.internal.telephony.Phone;
import com.android.provider.Telephony;
import com.google.android.mms.MmsException;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import vc.lx.sms.caches.RemoteResourceManager;
import vc.lx.sms.cmcc.http.data.SongItem;
import vc.lx.sms.data.MessagetToComposeDate;
import vc.lx.sms.db.TopMusicService;
import vc.lx.sms2.R;

public class MessageListAdapter extends CursorAdapter {
    private static final int CACHE_SIZE = 50;
    public static final int COLUMN_ID = 1;
    public static final int COLUMN_MMS_DATE = 12;
    public static final int COLUMN_MMS_DELIVERY_REPORT = 16;
    public static final int COLUMN_MMS_ERROR_TYPE = 18;
    public static final int COLUMN_MMS_LOCKED = 19;
    public static final int COLUMN_MMS_MESSAGE_BOX = 15;
    public static final int COLUMN_MMS_MESSAGE_TYPE = 14;
    public static final int COLUMN_MMS_READ = 13;
    public static final int COLUMN_MMS_READ_REPORT = 17;
    public static final int COLUMN_MMS_SUBJECT = 10;
    public static final int COLUMN_MMS_SUBJECT_CHARSET = 11;
    public static final int COLUMN_MSG_TYPE = 0;
    public static final int COLUMN_SMS_ADDRESS = 3;
    public static final int COLUMN_SMS_BODY = 4;
    public static final int COLUMN_SMS_DATE = 5;
    public static final int COLUMN_SMS_LOCKED = 9;
    public static final int COLUMN_SMS_READ = 6;
    public static final int COLUMN_SMS_STATUS = 8;
    public static final int COLUMN_SMS_TYPE = 7;
    public static final int COLUMN_THREAD_ID = 2;
    private static final boolean DEBUG = false;
    private static final boolean LOCAL_LOGV = false;
    public static final String[] PROJECTION = {Telephony.MmsSms.TYPE_DISCRIMINATOR_COLUMN, "_id", "thread_id", "address", Telephony.TextBasedSmsColumns.BODY, "date", "read", "type", "status", "locked", Telephony.BaseMmsColumns.SUBJECT, Telephony.BaseMmsColumns.SUBJECT_CHARSET, "date", "read", Telephony.BaseMmsColumns.MESSAGE_TYPE, Telephony.BaseMmsColumns.MESSAGE_BOX, Telephony.BaseMmsColumns.DELIVERY_REPORT, Telephony.BaseMmsColumns.READ_REPORT, Telephony.MmsSms.PendingMessages.ERROR_TYPE, "locked"};
    private static final String TAG = "MessageListAdapter";
    private final ColumnsMap mColumnsMap;
    private Context mContext;
    private String mHighlight;
    protected LayoutInflater mInflater;
    private boolean mIsMultiMode = false;
    private final ListView mListView;
    private final LinkedHashMap<Long, MessageItem> mMessageItemCache;
    /* access modifiers changed from: private */
    public Handler mMsgListItemHandler;
    private boolean mMultiRecipient = false;
    private OnDataSetChangedListener mOnDataSetChangedListener;
    private RemoteResourceManager mRrm;
    public HashSet<String> mSelectedIds = new HashSet<>();
    private List<SongItem> songItems = null;
    private TopMusicService topService = null;

    public static class ColumnsMap {
        public int mColumnMmsDate;
        public int mColumnMmsDeliveryReport;
        public int mColumnMmsErrorType;
        public int mColumnMmsLocked;
        public int mColumnMmsMessageBox;
        public int mColumnMmsMessageType;
        public int mColumnMmsRead;
        public int mColumnMmsReadReport;
        public int mColumnMmsSubject;
        public int mColumnMmsSubjectCharset;
        public int mColumnMsgId;
        public int mColumnMsgType;
        public int mColumnSmsAddress;
        public int mColumnSmsBody;
        public int mColumnSmsDate;
        public int mColumnSmsLocked;
        public int mColumnSmsRead;
        public int mColumnSmsStatus;
        public int mColumnSmsType;

        public ColumnsMap() {
            this.mColumnMsgType = 0;
            this.mColumnMsgId = 1;
            this.mColumnSmsAddress = 3;
            this.mColumnSmsBody = 4;
            this.mColumnSmsDate = 5;
            this.mColumnSmsType = 7;
            this.mColumnSmsStatus = 8;
            this.mColumnSmsLocked = 9;
            this.mColumnMmsSubject = 10;
            this.mColumnMmsSubjectCharset = 11;
            this.mColumnMmsMessageType = 14;
            this.mColumnMmsMessageBox = 15;
            this.mColumnMmsDeliveryReport = 16;
            this.mColumnMmsReadReport = 17;
            this.mColumnMmsErrorType = 18;
            this.mColumnMmsLocked = 19;
        }

        public ColumnsMap(Cursor cursor) {
            try {
                this.mColumnMsgType = cursor.getColumnIndexOrThrow(Telephony.MmsSms.TYPE_DISCRIMINATOR_COLUMN);
            } catch (IllegalArgumentException e) {
                Log.w("colsMap", e.getMessage());
            }
            try {
                this.mColumnMsgId = cursor.getColumnIndexOrThrow("_id");
            } catch (IllegalArgumentException e2) {
                Log.w("colsMap", e2.getMessage());
            }
            try {
                this.mColumnSmsAddress = cursor.getColumnIndexOrThrow("address");
            } catch (IllegalArgumentException e3) {
                Log.w("colsMap", e3.getMessage());
            }
            try {
                this.mColumnSmsBody = cursor.getColumnIndexOrThrow(Telephony.TextBasedSmsColumns.BODY);
            } catch (IllegalArgumentException e4) {
                Log.w("colsMap", e4.getMessage());
            }
            try {
                this.mColumnSmsDate = cursor.getColumnIndexOrThrow("date");
            } catch (IllegalArgumentException e5) {
                Log.w("colsMap", e5.getMessage());
            }
            try {
                this.mColumnSmsType = cursor.getColumnIndexOrThrow("type");
            } catch (IllegalArgumentException e6) {
                Log.w("colsMap", e6.getMessage());
            }
            try {
                this.mColumnSmsStatus = cursor.getColumnIndexOrThrow("status");
            } catch (IllegalArgumentException e7) {
                Log.w("colsMap", e7.getMessage());
            }
            try {
                this.mColumnSmsLocked = cursor.getColumnIndexOrThrow("locked");
            } catch (IllegalArgumentException e8) {
                Log.w("colsMap", e8.getMessage());
            }
            try {
                this.mColumnMmsSubject = cursor.getColumnIndexOrThrow(Telephony.BaseMmsColumns.SUBJECT);
            } catch (IllegalArgumentException e9) {
                Log.w("colsMap", e9.getMessage());
            }
            try {
                this.mColumnMmsSubjectCharset = cursor.getColumnIndexOrThrow(Telephony.BaseMmsColumns.SUBJECT_CHARSET);
            } catch (IllegalArgumentException e10) {
                Log.w("colsMap", e10.getMessage());
            }
            try {
                this.mColumnMmsMessageType = cursor.getColumnIndexOrThrow(Telephony.BaseMmsColumns.MESSAGE_TYPE);
            } catch (IllegalArgumentException e11) {
                Log.w("colsMap", e11.getMessage());
            }
            try {
                this.mColumnMmsMessageBox = cursor.getColumnIndexOrThrow(Telephony.BaseMmsColumns.MESSAGE_BOX);
            } catch (IllegalArgumentException e12) {
                Log.w("colsMap", e12.getMessage());
            }
            try {
                this.mColumnMmsDeliveryReport = cursor.getColumnIndexOrThrow(Telephony.BaseMmsColumns.DELIVERY_REPORT);
            } catch (IllegalArgumentException e13) {
                Log.w("colsMap", e13.getMessage());
            }
            try {
                this.mColumnMmsReadReport = cursor.getColumnIndexOrThrow(Telephony.BaseMmsColumns.READ_REPORT);
            } catch (IllegalArgumentException e14) {
                Log.w("colsMap", e14.getMessage());
            }
            try {
                this.mColumnMmsErrorType = cursor.getColumnIndexOrThrow(Telephony.MmsSms.PendingMessages.ERROR_TYPE);
            } catch (IllegalArgumentException e15) {
                Log.w("colsMap", e15.getMessage());
            }
            try {
                this.mColumnMmsLocked = cursor.getColumnIndexOrThrow("locked");
            } catch (IllegalArgumentException e16) {
                Log.w("colsMap", e16.getMessage());
            }
        }
    }

    public interface OnDataSetChangedListener {
        void onContentChanged(MessageListAdapter messageListAdapter);

        void onDataSetChanged(MessageListAdapter messageListAdapter);
    }

    public MessageListAdapter(Context context, Cursor cursor, ListView listView, boolean z, String str, boolean z2, boolean z3) {
        super(context, cursor, z2);
        this.mContext = context;
        this.mHighlight = str != null ? str.toLowerCase() : null;
        this.mMultiRecipient = z3;
        this.mInflater = (LayoutInflater) context.getSystemService("layout_inflater");
        this.mListView = listView;
        this.mMessageItemCache = new LinkedHashMap<Long, MessageItem>(10, 1.0f, true) {
            /* access modifiers changed from: protected */
            public boolean removeEldestEntry(Map.Entry entry) {
                return size() > 50;
            }
        };
        this.topService = new TopMusicService(context);
        if (z) {
            this.mColumnsMap = new ColumnsMap();
        } else {
            this.mColumnsMap = new ColumnsMap(cursor);
        }
    }

    private static long getKey(String str, long j) {
        return str.equals(Phone.APN_TYPE_MMS) ? -j : j;
    }

    public void bindView(View view, Context context, Cursor cursor) {
        if (view instanceof MessageListItem) {
            String string = cursor.getString(this.mColumnsMap.mColumnMsgType);
            long j = cursor.getLong(this.mColumnsMap.mColumnMsgId);
            CheckBox checkBox = (CheckBox) view.findViewById(R.id.multi_checkbox);
            view.findViewById(R.id.sms_image).setVisibility(8);
            checkBox.setVisibility(this.mIsMultiMode ? 0 : 8);
            final String str = string + "#" + String.valueOf(j);
            checkBox.setChecked(this.mSelectedIds.contains(str));
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                    if (z) {
                        MessageListAdapter.this.mSelectedIds.add(str);
                    } else {
                        MessageListAdapter.this.mSelectedIds.remove(str);
                    }
                }
            });
            final MessageItem cachedMessageItem = getCachedMessageItem(string, j, cursor);
            if (this.songItems == null) {
                this.songItems = this.topService.queryAll();
            }
            if (cachedMessageItem != null) {
                ((MessageListItem) view).setMultiRecipient(this.mMultiRecipient);
                ((MessageListItem) view).setMsgListItemHandler(this.mMsgListItemHandler);
                ((MessageListItem) view).bind(cachedMessageItem, this.songItems, this.mRrm);
            }
            view.findViewById(R.id.mms_layout_view_parent).setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    if (MessageListAdapter.this.mMsgListItemHandler != null) {
                        Message obtain = Message.obtain(MessageListAdapter.this.mMsgListItemHandler, 3);
                        MessagetToComposeDate messagetToComposeDate = new MessagetToComposeDate();
                        messagetToComposeDate.view = view;
                        messagetToComposeDate.msgItem = cachedMessageItem;
                        obtain.obj = messagetToComposeDate;
                        obtain.sendToTarget();
                    }
                }
            });
        }
    }

    public void changeMsgItem(long j, String str, SongItem songItem) {
        long key = getKey(str, j);
        MessageItem messageItem = this.mMessageItemCache.get(Long.valueOf(key));
        messageItem.mSongItem = songItem;
        this.mMessageItemCache.remove(Long.valueOf(key));
        this.mMessageItemCache.put(Long.valueOf(key), messageItem);
    }

    public void clearSelectedIds() {
        this.mSelectedIds.clear();
    }

    public MessageItem getCachedMessageItem(String str, long j, Cursor cursor) {
        MessageItem messageItem;
        MessageItem messageItem2 = this.mMessageItemCache.get(Long.valueOf(getKey(str, j)));
        if (messageItem2 != null) {
            return messageItem2;
        }
        try {
            MessageItem messageItem3 = new MessageItem(this.mContext, str, cursor, this.mColumnsMap, this.mHighlight, this.mRrm);
            try {
                this.mMessageItemCache.put(Long.valueOf(getKey(messageItem3.mType, messageItem3.mMsgId)), messageItem3);
                return messageItem3;
            } catch (MmsException e) {
                MmsException mmsException = e;
                messageItem = messageItem3;
                e = mmsException;
            }
        } catch (MmsException e2) {
            e = e2;
            messageItem = messageItem2;
            Log.e(TAG, e.getMessage());
            return messageItem;
        }
    }

    public HashSet<String> getSelectedIds() {
        return this.mSelectedIds;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        return this.mInflater.inflate((int) R.layout.message_list_item, viewGroup, false);
    }

    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        this.mListView.setSelection(this.mListView.getCount());
        this.mMessageItemCache.clear();
        if (this.mOnDataSetChangedListener != null) {
            this.mOnDataSetChangedListener.onDataSetChanged(this);
        }
    }

    public void notifyDataSetChangedWithoutChangingPos() {
        super.notifyDataSetChanged();
        if (this.mOnDataSetChangedListener != null) {
            this.mOnDataSetChangedListener.onDataSetChanged(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onContentChanged() {
        if (getCursor() != null && !getCursor().isClosed() && this.mOnDataSetChangedListener != null) {
            this.mOnDataSetChangedListener.onContentChanged(this);
        }
    }

    public void setMsgListItemHandler(Handler handler) {
        this.mMsgListItemHandler = handler;
    }

    public void setMultiMode(boolean z) {
        this.mIsMultiMode = z;
    }

    public void setMultiRecipient(boolean z) {
        this.mMultiRecipient = z;
    }

    public void setOnDataSetChangedListener(OnDataSetChangedListener onDataSetChangedListener) {
        this.mOnDataSetChangedListener = onDataSetChangedListener;
    }

    public void setRemoteResourceManager(RemoteResourceManager remoteResourceManager) {
        this.mRrm = remoteResourceManager;
    }
}
