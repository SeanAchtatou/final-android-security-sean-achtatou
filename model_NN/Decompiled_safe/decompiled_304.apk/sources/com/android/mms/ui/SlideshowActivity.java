package com.android.mms.ui;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.MediaController;
import com.android.mms.dom.AttrImpl;
import com.android.mms.dom.smil.SmilDocumentImpl;
import com.android.mms.dom.smil.SmilPlayer;
import com.android.mms.model.LayoutModel;
import com.android.mms.model.RegionModel;
import com.android.mms.model.SlideshowModel;
import com.android.mms.model.SmilHelper;
import com.google.android.mms.MmsException;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.events.Event;
import org.w3c.dom.events.EventListener;
import org.w3c.dom.events.EventTarget;
import org.w3c.dom.smil.SMILDocument;
import org.w3c.dom.smil.SMILElement;
import vc.lx.sms.ui.ComposeMessageActivity;
import vc.lx.sms2.R;

public class SlideshowActivity extends Activity implements EventListener {
    private static final boolean DEBUG = false;
    private static final boolean LOCAL_LOGV = false;
    private static final String TAG = "SlideshowActivity";
    private Handler mHandler;
    /* access modifiers changed from: private */
    public MediaController mMediaController;
    /* access modifiers changed from: private */
    public SlideView mSlideView;
    /* access modifiers changed from: private */
    public SMILDocument mSmilDoc;
    /* access modifiers changed from: private */
    public SmilPlayer mSmilPlayer;

    private class SmilPlayerController implements MediaController.MediaPlayerControl {
        private final SmilPlayer mPlayer;

        public SmilPlayerController(SmilPlayer smilPlayer) {
            this.mPlayer = smilPlayer;
        }

        public boolean canPause() {
            return true;
        }

        public boolean canSeekBackward() {
            return true;
        }

        public boolean canSeekForward() {
            return true;
        }

        public int getBufferPercentage() {
            return 100;
        }

        public int getCurrentPosition() {
            return this.mPlayer.getCurrentPosition();
        }

        public int getDuration() {
            return this.mPlayer.getDuration();
        }

        public boolean isPlaying() {
            if (this.mPlayer != null) {
                return this.mPlayer.isPlayingState();
            }
            return false;
        }

        public void pause() {
            if (this.mPlayer != null) {
                this.mPlayer.pause();
            }
        }

        public void seekTo(int i) {
        }

        public void start() {
            if (this.mPlayer != null) {
                this.mPlayer.start();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.widget.MediaController.<init>(android.content.Context, boolean):void}
     arg types: [com.android.mms.ui.SlideshowActivity, int]
     candidates:
      ClspMth{android.widget.MediaController.<init>(android.content.Context, android.util.AttributeSet):void}
      ClspMth{android.widget.MediaController.<init>(android.content.Context, boolean):void} */
    /* access modifiers changed from: private */
    public void initMediaController() {
        this.mMediaController = new MediaController((Context) this, false);
        this.mMediaController.setMediaPlayer(new SmilPlayerController(this.mSmilPlayer));
        this.mMediaController.setAnchorView(findViewById(R.id.slide_view));
        this.mMediaController.setPrevNextListeners(new View.OnClickListener() {
            public void onClick(View view) {
                SlideshowActivity.this.mSmilPlayer.next();
            }
        }, new View.OnClickListener() {
            public void onClick(View view) {
                SlideshowActivity.this.mSmilPlayer.prev();
            }
        });
    }

    /* access modifiers changed from: private */
    public static final boolean isMMSConformance(SMILDocument sMILDocument) {
        SMILElement head = sMILDocument.getHead();
        if (head == null) {
            return false;
        }
        NodeList childNodes = head.getChildNodes();
        if (childNodes == null || childNodes.getLength() != 1) {
            return false;
        }
        Node item = childNodes.item(0);
        if (item == null || !"layout".equals(item.getNodeName())) {
            return false;
        }
        NodeList childNodes2 = item.getChildNodes();
        if (childNodes2 == null) {
            return false;
        }
        int length = childNodes2.getLength();
        if (length <= 0) {
            return false;
        }
        for (int i = 0; i < length; i++) {
            Node item2 = childNodes2.item(i);
            if (item2 == null) {
                return false;
            }
            String nodeName = item2.getNodeName();
            if (!"root-layout".equals(nodeName)) {
                if (!"region".equals(nodeName)) {
                    return false;
                }
                NamedNodeMap attributes = item2.getAttributes();
                for (int i2 = 0; i2 < attributes.getLength(); i2++) {
                    Node item3 = attributes.item(i2);
                    if (item3 == null) {
                        return false;
                    }
                    String nodeName2 = item3.getNodeName();
                    if (!"left".equals(nodeName2) && !"top".equals(nodeName2) && !"height".equals(nodeName2) && !"width".equals(nodeName2) && !"fit".equals(nodeName2)) {
                        if (!"id".equals(nodeName2)) {
                            return false;
                        }
                        if (!(item3 instanceof AttrImpl)) {
                            return false;
                        }
                        String value = ((AttrImpl) item3).getValue();
                        if (!LayoutModel.TEXT_REGION_ID.equals(value) && !LayoutModel.IMAGE_REGION_ID.equals(value)) {
                            return false;
                        }
                    }
                }
                continue;
            }
        }
        return true;
    }

    public void handleEvent(final Event event) {
        this.mHandler.post(new Runnable() {
            public void run() {
                if (event.getType().equals(SmilDocumentImpl.SMIL_DOCUMENT_END_EVENT)) {
                    SlideshowActivity.this.finish();
                }
            }
        });
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.mHandler = new Handler();
        requestWindowFeature(1);
        getWindow().setFormat(-3);
        setContentView((int) R.layout.slideshow);
        try {
            final SlideshowModel createFromMessageUri = SlideshowModel.createFromMessageUri(this, getIntent().getData());
            this.mSlideView = (SlideView) findViewById(R.id.slide_view);
            PresenterFactory.getPresenter("SlideshowPresenter", this, this.mSlideView, createFromMessageUri);
            this.mHandler.post(new Runnable() {
                private boolean isRotating() {
                    return SlideshowActivity.this.mSmilPlayer.isPausedState() || SlideshowActivity.this.mSmilPlayer.isPlayingState() || SlideshowActivity.this.mSmilPlayer.isPlayedState();
                }

                public void run() {
                    int i;
                    int i2;
                    int i3;
                    int i4;
                    int i5;
                    int i6;
                    SmilPlayer unused = SlideshowActivity.this.mSmilPlayer = SmilPlayer.getPlayer();
                    SlideshowActivity.this.initMediaController();
                    SlideshowActivity.this.mSlideView.setMediaController(SlideshowActivity.this.mMediaController);
                    SMILDocument unused2 = SlideshowActivity.this.mSmilDoc = SmilHelper.getDocument(createFromMessageUri);
                    if (SlideshowActivity.isMMSConformance(SlideshowActivity.this.mSmilDoc)) {
                        LayoutModel layout = createFromMessageUri.getLayout();
                        if (layout != null) {
                            RegionModel imageRegion = layout.getImageRegion();
                            if (imageRegion != null) {
                                i6 = imageRegion.getLeft();
                                i5 = imageRegion.getTop();
                            } else {
                                i5 = 0;
                                i6 = 0;
                            }
                            RegionModel textRegion = layout.getTextRegion();
                            if (textRegion != null) {
                                int left = textRegion.getLeft();
                                i = textRegion.getTop();
                                int i7 = left;
                                i4 = i6;
                                i3 = i5;
                                i2 = i7;
                            } else {
                                i = 0;
                                i4 = i6;
                                i3 = i5;
                                i2 = 0;
                            }
                        } else {
                            i = 0;
                            i2 = 0;
                            i3 = 0;
                            i4 = 0;
                        }
                        SlideshowActivity.this.mSlideView.enableMMSConformanceMode(i2, i, i4, i3);
                    }
                    ((EventTarget) SlideshowActivity.this.mSmilDoc).addEventListener(SmilDocumentImpl.SMIL_DOCUMENT_END_EVENT, SlideshowActivity.this, false);
                    SlideshowActivity.this.mSmilPlayer.init(SlideshowActivity.this.mSmilDoc);
                    if (isRotating()) {
                        SlideshowActivity.this.mSmilPlayer.reload();
                    } else {
                        SlideshowActivity.this.mSmilPlayer.play();
                    }
                }
            });
        } catch (MmsException e) {
            Log.e(TAG, "Cannot present the slide show.", e);
            finish();
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        switch (i) {
            case 4:
            case 82:
                if (this.mSmilPlayer != null && (this.mSmilPlayer.isPausedState() || this.mSmilPlayer.isPlayingState() || this.mSmilPlayer.isPlayedState())) {
                    this.mSmilPlayer.stop();
                    break;
                }
            case 19:
            case 20:
            case 21:
            case ComposeMessageActivity.REQUEST_CODE_CALLING_CARD_SELECT /*22*/:
            case 24:
            case ComposeMessageActivity.REQUEST_CODE_CREATE_VOTE /*25*/:
                break;
            default:
                if (!(this.mSmilPlayer == null || this.mMediaController == null)) {
                    this.mMediaController.show();
                    break;
                }
        }
        return super.onKeyDown(i, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.mSmilDoc != null) {
            ((EventTarget) this.mSmilDoc).removeEventListener(SmilDocumentImpl.SMIL_DOCUMENT_END_EVENT, this, false);
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        if (this.mSmilPlayer != null) {
            if (isFinishing()) {
                this.mSmilPlayer.stop();
            } else {
                this.mSmilPlayer.stopWhenReload();
            }
            if (this.mMediaController != null) {
                this.mMediaController.hide();
            }
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.mSmilPlayer == null || this.mMediaController == null) {
            return false;
        }
        this.mMediaController.show();
        return false;
    }
}
