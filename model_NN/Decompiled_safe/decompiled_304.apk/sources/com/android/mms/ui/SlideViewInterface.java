package com.android.mms.ui;

import android.graphics.Bitmap;
import android.net.Uri;
import java.util.Map;

public interface SlideViewInterface extends ViewInterface {
    void pauseAudio();

    void pauseVideo();

    void seekAudio(int i);

    void seekVideo(int i);

    void setAudio(Uri uri, String str, Map<String, ?> map);

    void setImage(String str, Bitmap bitmap);

    void setImageRegionFit(String str);

    void setImageVisibility(boolean z);

    void setText(String str, String str2);

    void setTextVisibility(boolean z);

    void setVideo(String str, Uri uri);

    void setVideoVisibility(boolean z);

    void startAudio();

    void startVideo();

    void stopAudio();

    void stopVideo();
}
