package com.android.mms.ui;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.Map;
import vc.lx.sms.cmcc.MMHttpDefines;
import vc.lx.sms2.R;

public class AudioAttachmentView extends LinearLayout implements SlideViewInterface {
    private static final String TAG = "AudioAttachmentView";
    private TextView mAlbumView;
    private TextView mArtistView;
    private Uri mAudioUri;
    private TextView mErrorMsgView;
    private boolean mIsPlaying;
    private MediaPlayer mMediaPlayer;
    private TextView mNameView;
    private final Resources mRes;

    public AudioAttachmentView(Context context) {
        super(context);
        this.mRes = context.getResources();
    }

    public AudioAttachmentView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.mRes = context.getResources();
    }

    private void cleanupMediaPlayer() {
        if (this.mMediaPlayer != null) {
            try {
                this.mMediaPlayer.stop();
                this.mMediaPlayer.release();
            } finally {
                this.mMediaPlayer = null;
            }
        }
    }

    /* access modifiers changed from: private */
    public void onPlaybackError() {
        Log.e(TAG, "Error occurred while playing audio.");
        showErrorMessage(this.mRes.getString(R.string.cannot_play_audio));
        stopAudio();
    }

    private void showErrorMessage(String str) {
        this.mErrorMsgView.setText(str);
        this.mErrorMsgView.setVisibility(0);
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        this.mNameView = (TextView) findViewById(R.id.audio_name);
        this.mAlbumView = (TextView) findViewById(R.id.album_name);
        this.mArtistView = (TextView) findViewById(R.id.artist_name);
        this.mErrorMsgView = (TextView) findViewById(R.id.audio_error_msg);
    }

    public void pauseAudio() {
    }

    public void pauseVideo() {
    }

    public void reset() {
        synchronized (this) {
            if (this.mIsPlaying) {
                stopAudio();
            }
        }
        this.mErrorMsgView.setVisibility(8);
    }

    public void seekAudio(int i) {
    }

    public void seekVideo(int i) {
    }

    public void setAudio(Uri uri, String str, Map<String, ?> map) {
        synchronized (this) {
            this.mAudioUri = uri;
        }
        this.mNameView.setText(str);
        this.mAlbumView.setText((String) map.get(MMHttpDefines.TAG_ALBUM));
        this.mArtistView.setText((String) map.get("artist"));
    }

    public void setImage(String str, Bitmap bitmap) {
    }

    public void setImageRegionFit(String str) {
    }

    public void setImageVisibility(boolean z) {
    }

    public void setText(String str, String str2) {
    }

    public void setTextVisibility(boolean z) {
    }

    public void setVideo(String str, Uri uri) {
    }

    public void setVideoVisibility(boolean z) {
    }

    public void setVisibility(boolean z) {
    }

    public synchronized void startAudio() {
        if (!this.mIsPlaying && this.mAudioUri != null) {
            this.mMediaPlayer = MediaPlayer.create(getContext(), this.mAudioUri);
            if (this.mMediaPlayer != null) {
                this.mMediaPlayer.setAudioStreamType(3);
                this.mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    public void onCompletion(MediaPlayer mediaPlayer) {
                        AudioAttachmentView.this.stopAudio();
                    }
                });
                this.mMediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                    public boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
                        AudioAttachmentView.this.onPlaybackError();
                        return true;
                    }
                });
                this.mIsPlaying = true;
                this.mMediaPlayer.start();
            }
        }
    }

    public void startVideo() {
    }

    public synchronized void stopAudio() {
        try {
            cleanupMediaPlayer();
            this.mIsPlaying = false;
        } catch (Throwable th) {
            this.mIsPlaying = false;
            throw th;
        }
    }

    public void stopVideo() {
    }
}
