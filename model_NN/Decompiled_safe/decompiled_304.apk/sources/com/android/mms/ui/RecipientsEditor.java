package com.android.mms.ui;

import android.content.Context;
import android.telephony.PhoneNumberUtils;
import android.text.Annotation;
import android.text.Editable;
import android.text.Layout;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.ContextMenu;
import android.view.MotionEvent;
import android.widget.MultiAutoCompleteTextView;
import com.android.mms.MmsConfig;
import com.android.provider.Telephony;
import java.util.Iterator;
import java.util.List;
import vc.lx.sms.data.Contact;
import vc.lx.sms.data.ContactList;
import vc.lx.sms.util.MessageUtils;

public class RecipientsEditor extends MultiAutoCompleteTextView {
    /* access modifiers changed from: private */
    public char mLastSeparator = ',';
    private int mLongPressedPosition = -1;
    private final RecipientsEditorTokenizer mTokenizer;

    public static class RecipientContextMenuInfo implements ContextMenu.ContextMenuInfo {
        public final Contact recipient;

        RecipientContextMenuInfo(Contact contact) {
            this.recipient = contact;
        }
    }

    private class RecipientsEditorTokenizer implements MultiAutoCompleteTextView.Tokenizer {
        private final Context mContext;
        private final MultiAutoCompleteTextView mList;

        RecipientsEditorTokenizer(Context context, MultiAutoCompleteTextView multiAutoCompleteTextView) {
            this.mList = multiAutoCompleteTextView;
            this.mContext = context;
        }

        public int findTokenEnd(CharSequence charSequence, int i) {
            int length = charSequence.length();
            for (int i2 = i; i2 < length; i2++) {
                char charAt = charSequence.charAt(i2);
                if (charAt == ',' || charAt == ';') {
                    return i2;
                }
            }
            return length;
        }

        public int findTokenStart(CharSequence charSequence, int i) {
            int i2 = i;
            while (i2 > 0) {
                char charAt = charSequence.charAt(i2 - 1);
                if (charAt == ',' || charAt == ';') {
                    break;
                }
                i2--;
            }
            while (i2 < i && charSequence.charAt(i2) == ' ') {
                i2++;
            }
            return i2;
        }

        /* JADX WARNING: CFG modification limit reached, blocks count: 147 */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.util.List<java.lang.String> getNumbers() {
            /*
                r9 = this;
                r5 = 0
                java.lang.String r8 = ">"
                java.lang.String r7 = "<"
                android.widget.MultiAutoCompleteTextView r0 = r9.mList
                android.text.Editable r0 = r0.getText()
                int r1 = r0.length()
                java.util.ArrayList r2 = new java.util.ArrayList
                r2.<init>()
                java.lang.String r3 = r0.toString()
                if (r3 == 0) goto L_0x00b8
                java.lang.String r4 = "<"
                boolean r4 = r3.contains(r7)
                if (r4 == 0) goto L_0x00b8
                java.lang.String r4 = ">"
                boolean r4 = r3.contains(r8)
                if (r4 == 0) goto L_0x00b8
                java.lang.String r0 = ","
                java.lang.String[] r0 = r3.split(r0)
                r1 = r5
            L_0x0031:
                int r3 = r0.length
                if (r1 >= r3) goto L_0x00b7
                r3 = r0[r1]
                java.lang.String r4 = "<"
                boolean r3 = r3.contains(r7)
                if (r3 == 0) goto L_0x0066
                r3 = r0[r1]
                java.lang.String r4 = ">"
                boolean r3 = r3.contains(r8)
                if (r3 == 0) goto L_0x0066
                r3 = r0[r1]
                r4 = r0[r1]
                java.lang.String r5 = "<"
                int r4 = r4.indexOf(r7)
                int r4 = r4 + 1
                r5 = r0[r1]
                java.lang.String r6 = ">"
                int r5 = r5.indexOf(r8)
                java.lang.String r3 = r3.substring(r4, r5)
                r2.add(r3)
            L_0x0063:
                int r1 = r1 + 1
                goto L_0x0031
            L_0x0066:
                java.lang.String r3 = ""
                r4 = r0[r1]
                java.lang.String r4 = r4.trim()
                boolean r3 = r3.equals(r4)
                if (r3 != 0) goto L_0x0063
                r3 = r0[r1]
                if (r3 == 0) goto L_0x0063
                r3 = r0[r1]
                r2.add(r3)
                goto L_0x0063
            L_0x007e:
                r4 = r3
            L_0x007f:
                int r5 = r1 + 1
                if (r3 >= r5) goto L_0x00b7
                if (r3 == r1) goto L_0x0091
                char r5 = r0.charAt(r3)
                r6 = 44
                if (r5 == r6) goto L_0x0091
                r6 = 59
                if (r5 != r6) goto L_0x00b4
            L_0x0091:
                if (r3 <= r4) goto L_0x00a5
                android.content.Context r5 = r9.mContext
                java.lang.String r5 = com.android.mms.ui.RecipientsEditor.getNumberAt(r0, r4, r3, r5)
                r2.add(r5)
                android.content.Context r5 = r9.mContext
                int r4 = com.android.mms.ui.RecipientsEditor.getSpanLength(r0, r4, r3, r5)
                if (r4 <= r3) goto L_0x00a5
                r3 = r4
            L_0x00a5:
                int r3 = r3 + 1
            L_0x00a7:
                if (r3 >= r1) goto L_0x007e
                char r4 = r0.charAt(r3)
                r5 = 32
                if (r4 != r5) goto L_0x007e
                int r3 = r3 + 1
                goto L_0x00a7
            L_0x00b4:
                int r3 = r3 + 1
                goto L_0x007f
            L_0x00b7:
                return r2
            L_0x00b8:
                r3 = r5
                r4 = r5
                goto L_0x007f
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.mms.ui.RecipientsEditor.RecipientsEditorTokenizer.getNumbers():java.util.List");
        }

        public CharSequence terminateToken(CharSequence charSequence) {
            char charAt;
            int length = charSequence.length();
            while (length > 0 && charSequence.charAt(length - 1) == ' ') {
                length--;
            }
            if (length > 0 && ((charAt = charSequence.charAt(length - 1)) == ',' || charAt == ';')) {
                return charSequence;
            }
            String str = RecipientsEditor.this.mLastSeparator + " ";
            if (!(charSequence instanceof Spanned)) {
                return ((Object) charSequence) + str;
            }
            SpannableString spannableString = new SpannableString(((Object) charSequence) + str);
            TextUtils.copySpansFrom((Spanned) charSequence, 0, charSequence.length(), Object.class, spannableString, 0);
            return spannableString;
        }
    }

    public RecipientsEditor(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, 16842859);
        this.mTokenizer = new RecipientsEditorTokenizer(context, this);
        setTokenizer(this.mTokenizer);
        setImeOptions(5);
        addTextChangedListener(new TextWatcher() {
            private Annotation[] mAffected;

            public void afterTextChanged(Editable editable) {
                if (this.mAffected != null) {
                    for (Annotation removeSpan : this.mAffected) {
                        editable.removeSpan(removeSpan);
                    }
                }
                this.mAffected = null;
            }

            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                this.mAffected = (Annotation[]) ((Spanned) charSequence).getSpans(i, i + i2, Annotation.class);
            }

            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                if (i2 == 0 && i3 == 1) {
                    char charAt = charSequence.charAt(i);
                    if (charAt == ',' || charAt == ';') {
                        char unused = RecipientsEditor.this.mLastSeparator = charAt;
                    }
                }
            }
        });
    }

    public static CharSequence contactToToken(Contact contact) {
        SpannableString spannableString = new SpannableString(contact.getNameAndNumber());
        int length = spannableString.length();
        if (length != 0) {
            spannableString.setSpan(new Annotation("number", contact.getNumber()), 0, length, 33);
        }
        return spannableString;
    }

    private static String getAnnotation(Annotation[] annotationArr, String str) {
        for (int i = 0; i < annotationArr.length; i++) {
            if (annotationArr[i].getKey().equals(str)) {
                return annotationArr[i].getValue();
            }
        }
        return "";
    }

    private static String getFieldAt(String str, Spanned spanned, int i, int i2, Context context) {
        String annotation = getAnnotation((Annotation[]) spanned.getSpans(i, i2, Annotation.class), str);
        return TextUtils.isEmpty(annotation) ? TextUtils.substring(spanned, i, i2) : annotation;
    }

    /* access modifiers changed from: private */
    public static String getNumberAt(Spanned spanned, int i, int i2, Context context) {
        return getFieldAt("number", spanned, i, i2, context);
    }

    /* access modifiers changed from: private */
    public static int getSpanLength(Spanned spanned, int i, int i2, Context context) {
        Annotation[] annotationArr = (Annotation[]) spanned.getSpans(i, i2, Annotation.class);
        if (annotationArr.length > 0) {
            return spanned.getSpanEnd(annotationArr[0]);
        }
        return 0;
    }

    private boolean isValidAddress(String str, boolean z) {
        return z ? MessageUtils.isValidMmsAddress(str) : PhoneNumberUtils.isWellFormedSmsAddress(str) || Telephony.Mms.isEmailAddress(str);
    }

    private int pointToPosition(int i, int i2) {
        int compoundPaddingLeft = (i - getCompoundPaddingLeft()) + getScrollX();
        int extendedPaddingTop = (i2 - getExtendedPaddingTop()) + getScrollY();
        Layout layout = getLayout();
        if (layout == null) {
            return -1;
        }
        return layout.getOffsetForHorizontal(layout.getLineForVertical(extendedPaddingTop), (float) compoundPaddingLeft);
    }

    public ContactList constructContactsFromInput() {
        List<String> numbers = this.mTokenizer.getNumbers();
        ContactList contactList = new ContactList();
        for (String next : numbers) {
            Contact contact = Contact.get(next, false, getContext());
            contact.setNumber(next);
            contactList.add(contact);
        }
        return contactList;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.text.TextUtils.indexOf(java.lang.CharSequence, char):int}
     arg types: [android.text.Editable, int]
     candidates:
      ClspMth{android.text.TextUtils.indexOf(java.lang.CharSequence, java.lang.CharSequence):int}
      ClspMth{android.text.TextUtils.indexOf(java.lang.CharSequence, char):int} */
    public boolean containsEmail() {
        if (TextUtils.indexOf((CharSequence) getText(), '@') == -1) {
            return false;
        }
        for (String isEmailAddress : this.mTokenizer.getNumbers()) {
            if (Telephony.Mms.isEmailAddress(isEmailAddress)) {
                return true;
            }
        }
        return false;
    }

    public boolean enoughToFilter() {
        if (!super.enoughToFilter()) {
            return false;
        }
        return getSelectionEnd() == getText().length();
    }

    public String formatInvalidNumbers(boolean z) {
        StringBuilder sb = new StringBuilder();
        for (String next : this.mTokenizer.getNumbers()) {
            if (!isValidAddress(next, z)) {
                if (sb.length() != 0) {
                    sb.append(", ");
                }
                sb.append(next);
            }
        }
        return sb.toString();
    }

    /* access modifiers changed from: protected */
    public ContextMenu.ContextMenuInfo getContextMenuInfo() {
        int findTokenStart;
        int findTokenEnd;
        if (this.mLongPressedPosition >= 0) {
            Editable text = getText();
            if (this.mLongPressedPosition <= text.length() && (findTokenEnd = this.mTokenizer.findTokenEnd(text, (findTokenStart = this.mTokenizer.findTokenStart(text, this.mLongPressedPosition)))) != findTokenStart) {
                return new RecipientContextMenuInfo(Contact.get(getNumberAt(getText(), findTokenStart, findTokenEnd, getContext()), false, getContext()));
            }
        }
        return null;
    }

    public List<String> getNumbers() {
        return this.mTokenizer.getNumbers();
    }

    public int getRecipientCount() {
        return this.mTokenizer.getNumbers().size();
    }

    public boolean hasInvalidRecipient(boolean z) {
        for (String next : this.mTokenizer.getNumbers()) {
            if (!isValidAddress(next, z)) {
                if (MmsConfig.getEmailGateway() == null) {
                    return true;
                }
                if (!MessageUtils.isAlias(next)) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean hasValidRecipient(boolean z) {
        for (String isValidAddress : this.mTokenizer.getNumbers()) {
            if (isValidAddress(isValidAddress, z)) {
                return true;
            }
        }
        return false;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        int x = (int) motionEvent.getX();
        int y = (int) motionEvent.getY();
        if (action == 0) {
            this.mLongPressedPosition = pointToPosition(x, y);
        }
        return super.onTouchEvent(motionEvent);
    }

    public void populate(ContactList contactList) {
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        Iterator it = contactList.iterator();
        while (it.hasNext()) {
            Contact contact = (Contact) it.next();
            if (spannableStringBuilder.length() != 0) {
                spannableStringBuilder.append((CharSequence) ", ");
            }
            spannableStringBuilder.append(contactToToken(contact));
        }
        setText(spannableStringBuilder);
    }

    public void putContact(Contact contact) {
        Contact.put(contact);
    }
}
