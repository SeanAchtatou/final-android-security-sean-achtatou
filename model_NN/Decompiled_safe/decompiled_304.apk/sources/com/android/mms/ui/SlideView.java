package com.android.mms.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.net.Uri;
import android.text.method.HideReturnsTransformationMethod;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsoluteLayout;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.VideoView;
import com.android.mms.layout.LayoutManager;
import com.android.mms.ui.AdaptableSlideViewInterface;
import java.io.IOException;
import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;
import vc.lx.sms2.R;

public class SlideView extends AbsoluteLayout implements AdaptableSlideViewInterface {
    private static final int AUDIO_INFO_HEIGHT = 82;
    private static final boolean DEBUG = false;
    private static final boolean LOCAL_LOGV = false;
    private static final String TAG = "SlideView";
    private View mAudioInfoView;
    /* access modifiers changed from: private */
    public MediaPlayer mAudioPlayer;
    private boolean mConformanceMode;
    private Context mContext;
    private ImageView mImageView;
    /* access modifiers changed from: private */
    public boolean mIsPrepared;
    /* access modifiers changed from: private */
    public MediaController mMediaController;
    MediaPlayer.OnPreparedListener mPreparedListener = new MediaPlayer.OnPreparedListener() {
        public void onPrepared(MediaPlayer mediaPlayer) {
            boolean unused = SlideView.this.mIsPrepared = true;
            if (SlideView.this.mSeekWhenPrepared > 0) {
                SlideView.this.mAudioPlayer.seekTo(SlideView.this.mSeekWhenPrepared);
                int unused2 = SlideView.this.mSeekWhenPrepared = 0;
            }
            if (SlideView.this.mStartWhenPrepared) {
                SlideView.this.mAudioPlayer.start();
                boolean unused3 = SlideView.this.mStartWhenPrepared = false;
                SlideView.this.displayAudioInfo();
            }
            if (SlideView.this.mStopWhenPrepared) {
                SlideView.this.mAudioPlayer.stop();
                SlideView.this.mAudioPlayer.release();
                MediaPlayer unused4 = SlideView.this.mAudioPlayer = null;
                boolean unused5 = SlideView.this.mStopWhenPrepared = false;
                SlideView.this.hideAudioInfo();
            }
        }
    };
    private ScrollView mScrollText;
    private ScrollView mScrollViewPort;
    /* access modifiers changed from: private */
    public int mSeekWhenPrepared;
    private AdaptableSlideViewInterface.OnSizeChangedListener mSizeChangedListener;
    /* access modifiers changed from: private */
    public boolean mStartWhenPrepared;
    /* access modifiers changed from: private */
    public boolean mStopWhenPrepared;
    private TextView mTextView;
    private VideoView mVideoView;
    private LinearLayout mViewPort;

    private class Position {
        public int mLeft;
        public int mTop;

        public Position(int i, int i2) {
            this.mTop = i2;
            this.mLeft = i;
        }
    }

    public SlideView(Context context) {
        super(context);
        this.mContext = context;
    }

    public SlideView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.mContext = context;
    }

    /* access modifiers changed from: private */
    public void displayAudioInfo() {
        if (this.mAudioInfoView != null) {
            this.mAudioInfoView.setVisibility(0);
        }
    }

    /* access modifiers changed from: private */
    public void hideAudioInfo() {
        if (this.mAudioInfoView != null) {
            this.mAudioInfoView.setVisibility(8);
        }
    }

    private void initAudioInfoView(String str) {
        if (this.mAudioInfoView == null) {
            this.mAudioInfoView = LayoutInflater.from(getContext()).inflate((int) R.layout.playing_audio_info, (ViewGroup) null);
            this.mAudioInfoView.getHeight();
            ((TextView) this.mAudioInfoView.findViewById(R.id.name)).setText(str);
            if (this.mConformanceMode) {
                this.mViewPort.addView(this.mAudioInfoView, new LinearLayout.LayoutParams(-1, (int) AUDIO_INFO_HEIGHT));
            } else {
                addView(this.mAudioInfoView, new AbsoluteLayout.LayoutParams(-1, AUDIO_INFO_HEIGHT, 0, getHeight() - AUDIO_INFO_HEIGHT));
            }
        }
        this.mAudioInfoView.setVisibility(8);
    }

    public void enableMMSConformanceMode(int i, int i2, int i3, int i4) {
        this.mConformanceMode = true;
        if (this.mScrollViewPort == null) {
            this.mScrollViewPort = new ScrollView(this.mContext) {
                private int mBottomY;

                /* access modifiers changed from: protected */
                public void onLayout(boolean z, int i, int i2, int i3, int i4) {
                    super.onLayout(z, i, i2, i3, i4);
                    if (getChildCount() > 0) {
                        int height = getChildAt(0).getHeight();
                        int height2 = getHeight();
                        this.mBottomY = height2 < height ? height - height2 : 0;
                    }
                }

                /* access modifiers changed from: protected */
                public void onScrollChanged(int i, int i2, int i3, int i4) {
                    if ((i2 == 0 || i2 >= this.mBottomY) && SlideView.this.mMediaController != null) {
                        SlideView.this.mMediaController.show();
                    }
                }
            };
            this.mScrollViewPort.setScrollBarStyle(50331648);
            this.mViewPort = new LinearLayout(this.mContext);
            this.mViewPort.setOrientation(1);
            this.mViewPort.setGravity(17);
            this.mViewPort.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    if (SlideView.this.mMediaController != null) {
                        SlideView.this.mMediaController.show();
                    }
                }
            });
            this.mScrollViewPort.addView(this.mViewPort, new FrameLayout.LayoutParams(-1, -2));
            addView(this.mScrollViewPort);
        }
        TreeMap treeMap = new TreeMap(new Comparator<Position>() {
            public int compare(Position position, Position position2) {
                int i = position.mLeft;
                int i2 = position.mTop;
                int i3 = position2.mLeft;
                int i4 = i2 - position2.mTop;
                int i5 = i4 == 0 ? i - i3 : i4;
                if (i5 == 0) {
                    return -1;
                }
                return i5;
            }
        });
        if (i >= 0 && i2 >= 0) {
            this.mTextView = new TextView(this.mContext);
            this.mTextView.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            this.mTextView.setTextSize(18.0f);
            this.mTextView.setPadding(5, 5, 5, 5);
            treeMap.put(new Position(i, i2), this.mTextView);
        }
        if (i3 >= 0 && i4 >= 0) {
            this.mImageView = new ImageView(this.mContext);
            this.mImageView.setPadding(0, 5, 0, 5);
            treeMap.put(new Position(i3, i4), this.mImageView);
            this.mVideoView = new VideoView(this.mContext);
            treeMap.put(new Position(i3 + 1, i4), this.mVideoView);
        }
        for (View view : treeMap.values()) {
            if (view instanceof VideoView) {
                this.mViewPort.addView(view, new LinearLayout.LayoutParams(-1, LayoutManager.getInstance().getLayoutParameters().getHeight()));
            } else {
                this.mViewPort.addView(view, new LinearLayout.LayoutParams(-1, -2));
            }
            view.setVisibility(8);
        }
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        if (this.mSizeChangedListener != null) {
            this.mSizeChangedListener.onSizeChanged(i, i2 - AUDIO_INFO_HEIGHT);
        }
    }

    public void pauseAudio() {
        if (this.mAudioPlayer != null && this.mIsPrepared && this.mAudioPlayer.isPlaying()) {
            this.mAudioPlayer.pause();
        }
        this.mStartWhenPrepared = false;
    }

    public void pauseVideo() {
        if (this.mVideoView != null) {
            this.mVideoView.pause();
        }
    }

    public void reset() {
        if (this.mScrollText != null) {
            this.mScrollText.setVisibility(8);
        }
        if (this.mImageView != null) {
            this.mImageView.setVisibility(8);
        }
        if (this.mAudioPlayer != null) {
            stopAudio();
        }
        if (this.mVideoView != null) {
            stopVideo();
            this.mVideoView.setVisibility(8);
        }
        if (this.mTextView != null) {
            this.mTextView.setVisibility(8);
        }
        if (this.mScrollViewPort != null) {
            this.mScrollViewPort.scrollTo(0, 0);
            this.mScrollViewPort.setLayoutParams(new AbsoluteLayout.LayoutParams(-1, -1, 0, 0));
        }
    }

    public void seekAudio(int i) {
        if (this.mAudioPlayer == null || !this.mIsPrepared) {
            this.mSeekWhenPrepared = i;
        } else {
            this.mAudioPlayer.seekTo(i);
        }
    }

    public void seekVideo(int i) {
        if (this.mVideoView != null && i > 0) {
            this.mVideoView.seekTo(i);
        }
    }

    public void setAudio(Uri uri, String str, Map<String, ?> map) {
        if (uri == null) {
            throw new IllegalArgumentException("Audio URI may not be null.");
        }
        if (this.mAudioPlayer != null) {
            this.mAudioPlayer.reset();
            this.mAudioPlayer.release();
            this.mAudioPlayer = null;
        }
        this.mIsPrepared = false;
        try {
            this.mAudioPlayer = new MediaPlayer();
            this.mAudioPlayer.setOnPreparedListener(this.mPreparedListener);
            this.mAudioPlayer.setDataSource(this.mContext, uri);
            this.mAudioPlayer.prepareAsync();
        } catch (IOException e) {
            Log.e(TAG, "Unexpected IOException.", e);
            this.mAudioPlayer.release();
            this.mAudioPlayer = null;
        }
        initAudioInfoView(str);
    }

    public void setImage(String str, Bitmap bitmap) {
        Bitmap bitmap2;
        if (this.mImageView == null) {
            this.mImageView = new ImageView(this.mContext);
            this.mImageView.setPadding(0, 5, 0, 5);
            addView(this.mImageView, new AbsoluteLayout.LayoutParams(-2, -2, 0, 0));
        }
        if (bitmap == null) {
            try {
                bitmap2 = BitmapFactory.decodeResource(getResources(), R.drawable.ic_missing_thumbnail_picture);
            } catch (OutOfMemoryError e) {
                Log.e(TAG, "setImage: out of memory: ", e);
                return;
            }
        } else {
            bitmap2 = bitmap;
        }
        this.mImageView.setVisibility(0);
        this.mImageView.setImageBitmap(bitmap2);
    }

    public void setImageRegion(int i, int i2, int i3, int i4) {
        if (this.mImageView != null && !this.mConformanceMode) {
            this.mImageView.setLayoutParams(new AbsoluteLayout.LayoutParams(i3, i4, i, i2));
        }
    }

    public void setImageRegionFit(String str) {
    }

    public void setImageVisibility(boolean z) {
        int i = 0;
        if (this.mImageView == null) {
            return;
        }
        if (this.mConformanceMode) {
            ImageView imageView = this.mImageView;
            if (!z) {
                i = 8;
            }
            imageView.setVisibility(i);
            return;
        }
        ImageView imageView2 = this.mImageView;
        if (!z) {
            i = 4;
        }
        imageView2.setVisibility(i);
    }

    public void setMediaController(MediaController mediaController) {
        this.mMediaController = mediaController;
    }

    public void setOnSizeChangedListener(AdaptableSlideViewInterface.OnSizeChangedListener onSizeChangedListener) {
        this.mSizeChangedListener = onSizeChangedListener;
    }

    public void setText(String str, String str2) {
        if (!this.mConformanceMode) {
            if (this.mScrollText == null) {
                this.mScrollText = new ScrollView(this.mContext);
                this.mScrollText.setScrollBarStyle(50331648);
                addView(this.mScrollText, new AbsoluteLayout.LayoutParams(-2, -2, 0, 0));
            }
            if (this.mTextView == null) {
                this.mTextView = new TextView(this.mContext);
                this.mTextView.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                this.mScrollText.addView(this.mTextView);
            }
            this.mScrollText.requestFocus();
        }
        this.mTextView.setVisibility(0);
        this.mTextView.setText(str2);
    }

    public void setTextRegion(int i, int i2, int i3, int i4) {
        if (this.mScrollText != null && !this.mConformanceMode) {
            this.mScrollText.setLayoutParams(new AbsoluteLayout.LayoutParams(i3, i4, i, i2));
        }
    }

    public void setTextVisibility(boolean z) {
        int i = 0;
        if (this.mScrollText == null) {
            return;
        }
        if (this.mConformanceMode) {
            TextView textView = this.mTextView;
            if (!z) {
                i = 8;
            }
            textView.setVisibility(i);
            return;
        }
        ScrollView scrollView = this.mScrollText;
        if (!z) {
            i = 4;
        }
        scrollView.setVisibility(i);
    }

    public void setVideo(String str, Uri uri) {
        if (this.mVideoView == null) {
            this.mVideoView = new VideoView(this.mContext);
            addView(this.mVideoView, new AbsoluteLayout.LayoutParams(-2, -2, 0, 0));
        }
        this.mVideoView.setVisibility(0);
        this.mVideoView.setVideoURI(uri);
    }

    public void setVideoRegion(int i, int i2, int i3, int i4) {
        if (this.mVideoView != null && !this.mConformanceMode) {
            this.mVideoView.setLayoutParams(new AbsoluteLayout.LayoutParams(i3, i4, i, i2));
        }
    }

    public void setVideoVisibility(boolean z) {
        int i = 0;
        if (this.mVideoView == null) {
            return;
        }
        if (this.mConformanceMode) {
            VideoView videoView = this.mVideoView;
            if (!z) {
                i = 8;
            }
            videoView.setVisibility(i);
            return;
        }
        VideoView videoView2 = this.mVideoView;
        if (!z) {
            i = 4;
        }
        videoView2.setVisibility(i);
    }

    public void setVisibility(boolean z) {
    }

    public void startAudio() {
        if (this.mAudioPlayer == null || !this.mIsPrepared) {
            this.mStartWhenPrepared = true;
            return;
        }
        this.mAudioPlayer.start();
        this.mStartWhenPrepared = false;
        displayAudioInfo();
    }

    public void startVideo() {
        if (this.mVideoView != null) {
            this.mVideoView.start();
        }
    }

    public void stopAudio() {
        if (this.mAudioPlayer == null || !this.mIsPrepared) {
            this.mStopWhenPrepared = true;
            return;
        }
        this.mAudioPlayer.stop();
        this.mAudioPlayer.release();
        this.mAudioPlayer = null;
        hideAudioInfo();
    }

    public void stopVideo() {
        if (this.mVideoView != null) {
            this.mVideoView.stopPlayback();
        }
    }
}
