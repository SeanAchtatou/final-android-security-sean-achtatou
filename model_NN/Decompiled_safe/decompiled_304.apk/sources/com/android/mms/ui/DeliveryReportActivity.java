package com.android.mms.ui;

import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.ListView;
import com.android.provider.Telephony;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import vc.lx.sms.util.SqliteWrapper;
import vc.lx.sms2.R;

public class DeliveryReportActivity extends ListActivity {
    static final int COLUMN_DELIVERY_REPORT = 1;
    static final int COLUMN_DELIVERY_STATUS = 1;
    static final int COLUMN_READ_REPORT = 2;
    static final int COLUMN_READ_STATUS = 2;
    static final int COLUMN_RECIPIENT = 0;
    private static final String LOG_TAG = "DeliveryReportActivity";
    static final String[] MMS_REPORT_REQUEST_PROJECTION = {"address", Telephony.BaseMmsColumns.DELIVERY_REPORT, Telephony.BaseMmsColumns.READ_REPORT};
    static final String[] MMS_REPORT_STATUS_PROJECTION = {"address", "delivery_status", Telephony.BaseMmsColumns.READ_STATUS};
    static final String[] SMS_REPORT_STATUS_PROJECTION = {"address", "status"};
    private long mMessageId;
    private String mMessageType;

    private static final class MmsReportRequest {
        private final boolean mIsDeliveryReportRequsted;
        private final boolean mIsReadReportRequested;
        private final String mRecipient;

        public MmsReportRequest(String str, int i, int i2) {
            this.mRecipient = str;
            this.mIsDeliveryReportRequsted = i == 128;
            this.mIsReadReportRequested = i2 == 128;
        }

        public String getRecipient() {
            return this.mRecipient;
        }

        public boolean isDeliveryReportRequested() {
            return this.mIsDeliveryReportRequsted;
        }

        public boolean isReadReportRequested() {
            return this.mIsReadReportRequested;
        }
    }

    private static final class MmsReportStatus {
        final int deliveryStatus;
        final int readStatus;

        public MmsReportStatus(int i, int i2) {
            this.deliveryStatus = i;
            this.readStatus = i2;
        }
    }

    private long getMessageId(Bundle bundle, Intent intent) {
        long j = bundle != null ? bundle.getLong("message_id") : 0;
        return j == 0 ? intent.getLongExtra("message_id", 0) : j;
    }

    private String getMessageType(Bundle bundle, Intent intent) {
        String str = null;
        if (bundle != null) {
            str = bundle.getString("message_type");
        }
        return str == null ? intent.getStringExtra("message_type") : str;
    }

    private List<DeliveryReportItem> getMmsReportItems() {
        List<MmsReportRequest> mmsReportRequests = getMmsReportRequests();
        if (mmsReportRequests == null) {
            return null;
        }
        if (mmsReportRequests.size() == 0) {
            return null;
        }
        Map<String, MmsReportStatus> mmsReportStatus = getMmsReportStatus();
        ArrayList arrayList = new ArrayList();
        for (MmsReportRequest next : mmsReportRequests) {
            arrayList.add(new DeliveryReportItem(getString(R.string.recipient_label) + next.getRecipient(), getString(R.string.status_label) + getMmsReportStatusText(next, mmsReportStatus)));
        }
        return arrayList;
    }

    /* JADX INFO: finally extract failed */
    private List<MmsReportRequest> getMmsReportRequests() {
        Cursor query = SqliteWrapper.query(this, getContentResolver(), Uri.withAppendedPath(Telephony.Mms.REPORT_REQUEST_URI, String.valueOf(this.mMessageId)), MMS_REPORT_REQUEST_PROJECTION, null, null, null);
        if (query == null) {
            return null;
        }
        try {
            if (query.getCount() <= 0) {
                query.close();
                return null;
            }
            ArrayList arrayList = new ArrayList();
            while (query.moveToNext()) {
                arrayList.add(new MmsReportRequest(query.getString(0), query.getInt(1), query.getInt(2)));
            }
            query.close();
            return arrayList;
        } catch (Throwable th) {
            query.close();
            throw th;
        }
    }

    /* JADX INFO: finally extract failed */
    private Map<String, MmsReportStatus> getMmsReportStatus() {
        Cursor query = SqliteWrapper.query(this, getContentResolver(), Uri.withAppendedPath(Telephony.Mms.REPORT_STATUS_URI, String.valueOf(this.mMessageId)), MMS_REPORT_STATUS_PROJECTION, null, null, null);
        if (query == null) {
            return null;
        }
        try {
            HashMap hashMap = new HashMap();
            while (query.moveToNext()) {
                String string = query.getString(0);
                hashMap.put(Telephony.Mms.isEmailAddress(string) ? Telephony.Mms.extractAddrSpec(string) : PhoneNumberUtils.stripSeparators(string), new MmsReportStatus(query.getInt(1), query.getInt(2)));
            }
            query.close();
            return hashMap;
        } catch (Throwable th) {
            query.close();
            throw th;
        }
    }

    private String getMmsReportStatusText(MmsReportRequest mmsReportRequest, Map<String, MmsReportStatus> map) {
        if (map == null) {
            return getString(R.string.status_pending);
        }
        String recipient = mmsReportRequest.getRecipient();
        MmsReportStatus queryStatusByRecipient = queryStatusByRecipient(map, Telephony.Mms.isEmailAddress(recipient) ? Telephony.Mms.extractAddrSpec(recipient) : PhoneNumberUtils.stripSeparators(recipient));
        if (queryStatusByRecipient == null) {
            return getString(R.string.status_pending);
        }
        if (mmsReportRequest.isReadReportRequested() && queryStatusByRecipient.readStatus != 0) {
            switch (queryStatusByRecipient.readStatus) {
                case 128:
                    return getString(R.string.status_read);
                case 129:
                    return getString(R.string.status_unread);
            }
        }
        switch (queryStatusByRecipient.deliveryStatus) {
            case 0:
                return getString(R.string.status_pending);
            case 129:
            case 134:
                return getString(R.string.status_received);
            case 130:
                return getString(R.string.status_rejected);
            default:
                return getString(R.string.status_failed);
        }
    }

    private List<DeliveryReportItem> getReportItems() {
        return this.mMessageType.equals("sms") ? getSmsReportItems() : getMmsReportItems();
    }

    /* JADX INFO: finally extract failed */
    private List<DeliveryReportItem> getSmsReportItems() {
        Cursor query = SqliteWrapper.query(this, getContentResolver(), Telephony.Sms.CONTENT_URI, SMS_REPORT_STATUS_PROJECTION, "_id = " + this.mMessageId, null, null);
        if (query == null) {
            return null;
        }
        try {
            if (query.getCount() <= 0) {
                query.close();
                return null;
            }
            ArrayList arrayList = new ArrayList();
            while (query.moveToNext()) {
                arrayList.add(new DeliveryReportItem(getString(R.string.recipient_label) + query.getString(0), getString(R.string.status_label) + getSmsStatusText(query.getInt(1))));
            }
            query.close();
            return arrayList;
        } catch (Throwable th) {
            query.close();
            throw th;
        }
    }

    private String getSmsStatusText(int i) {
        return i == -1 ? getString(R.string.status_none) : i >= 128 ? getString(R.string.status_failed) : i >= 64 ? getString(R.string.status_pending) : getString(R.string.status_received);
    }

    private void initListAdapter() {
        List reportItems = getReportItems();
        if (reportItems == null) {
            reportItems = new ArrayList(1);
            reportItems.add(new DeliveryReportItem("", getString(R.string.status_none)));
            Log.w(LOG_TAG, "cursor == null");
        }
        setListAdapter(new DeliveryReportAdapter(this, reportItems));
    }

    private void initListView() {
        getListView().addHeaderView(getLayoutInflater().inflate((int) R.layout.delivery_report_header, (ViewGroup) null), null, true);
    }

    private static MmsReportStatus queryStatusByRecipient(Map<String, MmsReportStatus> map, String str) {
        for (String next : map.keySet()) {
            if (Telephony.Mms.isEmailAddress(str)) {
                if (TextUtils.equals(next, str)) {
                    return map.get(next);
                }
            } else if (PhoneNumberUtils.compare(next, str)) {
                return map.get(next);
            }
        }
        return null;
    }

    private void refreshDeliveryReport() {
        ListView listView = getListView();
        listView.invalidateViews();
        listView.requestFocus();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        setContentView((int) R.layout.delivery_report_activity);
        Intent intent = getIntent();
        this.mMessageId = getMessageId(bundle, intent);
        this.mMessageType = getMessageType(bundle, intent);
        initListView();
        initListAdapter();
    }

    public void onResume() {
        super.onResume();
        refreshDeliveryReport();
    }
}
