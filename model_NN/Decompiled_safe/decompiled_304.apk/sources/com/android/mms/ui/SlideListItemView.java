package com.android.mms.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.net.Uri;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.io.IOException;
import java.util.Map;
import vc.lx.sms2.R;

public class SlideListItemView extends LinearLayout implements SlideViewInterface {
    private static final String TAG = "SlideListItemView";
    private ImageView mAttachmentIcon;
    private TextView mAttachmentName;
    private ImageView mImagePreview;
    private TextView mTextPreview;

    public SlideListItemView(Context context) {
        super(context);
    }

    public SlideListItemView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        this.mTextPreview = (TextView) findViewById(R.id.text_preview);
        this.mTextPreview.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        this.mImagePreview = (ImageView) findViewById(R.id.image_preview);
        this.mAttachmentName = (TextView) findViewById(R.id.attachment_name);
        this.mAttachmentIcon = (ImageView) findViewById(R.id.attachment_icon);
    }

    public void pauseAudio() {
    }

    public void pauseVideo() {
    }

    public void reset() {
    }

    public void seekAudio(int i) {
    }

    public void seekVideo(int i) {
    }

    public void setAudio(Uri uri, String str, Map<String, ?> map) {
        if (str != null) {
            this.mAttachmentName.setText(str);
            this.mAttachmentIcon.setImageResource(R.drawable.ic_mms_music);
            return;
        }
        this.mAttachmentName.setText("");
        this.mAttachmentIcon.setImageDrawable(null);
    }

    public void setImage(String str, Bitmap bitmap) {
        this.mImagePreview.setImageBitmap(bitmap == null ? BitmapFactory.decodeResource(getResources(), R.drawable.ic_missing_thumbnail_picture) : bitmap);
    }

    public void setImageRegionFit(String str) {
    }

    public void setImageVisibility(boolean z) {
    }

    public void setText(String str, String str2) {
        this.mTextPreview.setText(str2);
        this.mTextPreview.setVisibility(TextUtils.isEmpty(str2) ? 8 : 0);
    }

    public void setTextVisibility(boolean z) {
    }

    public void setVideo(String str, Uri uri) {
        if (str != null) {
            this.mAttachmentName.setText(str);
            this.mAttachmentIcon.setImageResource(R.drawable.movie);
        } else {
            this.mAttachmentName.setText("");
            this.mAttachmentIcon.setImageDrawable(null);
        }
        MediaPlayer mediaPlayer = new MediaPlayer();
        try {
            mediaPlayer.setDataSource(getContext(), uri);
        } catch (IOException e) {
            Log.e(TAG, "Unexpected IOException.", e);
        } finally {
            mediaPlayer.release();
        }
    }

    public void setVideoVisibility(boolean z) {
    }

    public void setVisibility(boolean z) {
    }

    public void startAudio() {
    }

    public void startVideo() {
    }

    public void stopAudio() {
    }

    public void stopVideo() {
    }
}
