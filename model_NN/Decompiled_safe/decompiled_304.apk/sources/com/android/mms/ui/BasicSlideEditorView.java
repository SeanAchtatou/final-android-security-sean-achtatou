package com.android.mms.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.Map;
import vc.lx.sms2.R;

public class BasicSlideEditorView extends LinearLayout implements SlideViewInterface {
    private static final String TAG = "BasicSlideEditorView";
    private TextView mAudioNameView;
    private View mAudioView;
    private EditText mEditText;
    private ImageView mImageView;
    /* access modifiers changed from: private */
    public OnTextChangedListener mOnTextChangedListener;
    /* access modifiers changed from: private */
    public boolean mOnTextChangedListenerEnabled = true;

    public interface OnTextChangedListener {
        void onTextChanged(String str);
    }

    public BasicSlideEditorView(Context context) {
        super(context);
    }

    public BasicSlideEditorView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void onFinishInflate() {
        this.mImageView = (ImageView) findViewById(R.id.image);
        this.mAudioView = findViewById(R.id.audio);
        this.mAudioNameView = (TextView) findViewById(R.id.audio_name);
        this.mEditText = (EditText) findViewById(R.id.text_message);
        this.mEditText.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable editable) {
            }

            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                if (BasicSlideEditorView.this.mOnTextChangedListenerEnabled && BasicSlideEditorView.this.mOnTextChangedListener != null) {
                    BasicSlideEditorView.this.mOnTextChangedListener.onTextChanged(charSequence.toString());
                }
            }
        });
    }

    public void pauseAudio() {
    }

    public void pauseVideo() {
    }

    public void reset() {
        this.mImageView.setImageDrawable(null);
        this.mAudioView.setVisibility(8);
        this.mOnTextChangedListenerEnabled = false;
        this.mEditText.setText("");
        this.mOnTextChangedListenerEnabled = true;
    }

    public void seekAudio(int i) {
    }

    public void seekVideo(int i) {
    }

    public void setAudio(Uri uri, String str, Map<String, ?> map) {
        this.mAudioView.setVisibility(0);
        this.mAudioNameView.setText(str);
    }

    public void setImage(String str, Bitmap bitmap) {
        this.mImageView.setImageBitmap(bitmap == null ? BitmapFactory.decodeResource(getResources(), R.drawable.ic_missing_thumbnail_picture) : bitmap);
    }

    public void setImageRegionFit(String str) {
    }

    public void setImageVisibility(boolean z) {
    }

    public void setOnTextChangedListener(OnTextChangedListener onTextChangedListener) {
        this.mOnTextChangedListener = onTextChangedListener;
    }

    public void setText(String str, String str2) {
        this.mOnTextChangedListenerEnabled = false;
        if (str2 != null && !str2.equals(this.mEditText.getText().toString())) {
            this.mEditText.setText(str2);
            this.mEditText.setSelection(str2.length());
        }
        this.mOnTextChangedListenerEnabled = true;
    }

    public void setTextVisibility(boolean z) {
    }

    public void setVideo(String str, Uri uri) {
        Bitmap createVideoThumbnail = VideoAttachmentView.createVideoThumbnail(getContext(), uri);
        if (createVideoThumbnail == null) {
            createVideoThumbnail = BitmapFactory.decodeResource(getResources(), R.drawable.ic_missing_thumbnail_video);
        }
        this.mImageView.setImageBitmap(createVideoThumbnail);
    }

    public void setVideoVisibility(boolean z) {
    }

    public void setVisibility(boolean z) {
    }

    public void startAudio() {
    }

    public void startVideo() {
    }

    public void stopAudio() {
    }

    public void stopVideo() {
    }
}
