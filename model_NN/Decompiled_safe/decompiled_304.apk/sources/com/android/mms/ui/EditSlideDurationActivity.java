package com.android.mms.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import vc.lx.sms.ui.ComposeMessageActivity;
import vc.lx.sms2.R;

public class EditSlideDurationActivity extends Activity {
    private static final boolean DEBUG = false;
    private static final boolean LOCAL_LOGV = false;
    public static final String SLIDE_DUR = "dur";
    public static final String SLIDE_INDEX = "slide_index";
    public static final String SLIDE_TOTAL = "slide_total";
    private static final String STATE = "state";
    private static final String TAG = "EditSlideDurationActivity";
    private int mCurSlide;
    private Button mDone;
    private EditText mDur;
    private TextView mLabel;
    private final View.OnClickListener mOnDoneClickListener = new View.OnClickListener() {
        public void onClick(View view) {
            EditSlideDurationActivity.this.editDone();
        }
    };
    private final View.OnKeyListener mOnKeyListener = new View.OnKeyListener() {
        public boolean onKey(View view, int i, KeyEvent keyEvent) {
            if (keyEvent.getAction() != 0) {
                return false;
            }
            switch (i) {
                case ComposeMessageActivity.REQUEST_CODE_SMS_TEMPLATE /*23*/:
                    EditSlideDurationActivity.this.editDone();
                    break;
            }
            return false;
        }
    };
    private Bundle mState;
    private int mTotal;

    private void notifyUser(int i) {
        this.mDur.requestFocus();
        this.mDur.selectAll();
        Toast.makeText(this, i, 0).show();
    }

    /* access modifiers changed from: protected */
    public void editDone() {
        try {
            if (Integer.valueOf(this.mDur.getText().toString()).intValue() <= 0) {
                notifyUser(R.string.duration_zero);
                return;
            }
            setResult(-1, new Intent(this.mDur.getText().toString()));
            finish();
        } catch (NumberFormatException e) {
            notifyUser(R.string.duration_not_a_number);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        int i;
        super.onCreate(bundle);
        requestWindowFeature(1);
        setContentView((int) R.layout.edit_slide_duration);
        if (bundle == null) {
            Intent intent = getIntent();
            this.mCurSlide = intent.getIntExtra("slide_index", 1);
            this.mTotal = intent.getIntExtra(SLIDE_TOTAL, 1);
            i = intent.getIntExtra(SLIDE_DUR, 8);
        } else {
            this.mState = bundle.getBundle("state");
            this.mCurSlide = this.mState.getInt("slide_index", 1);
            this.mTotal = this.mState.getInt(SLIDE_TOTAL, 1);
            i = this.mState.getInt(SLIDE_DUR, 8);
        }
        this.mLabel = (TextView) findViewById(R.id.label);
        this.mLabel.setText(getString(R.string.duration_selector_title) + " " + (this.mCurSlide + 1) + "/" + this.mTotal);
        this.mDur = (EditText) findViewById(R.id.text);
        this.mDur.setText(String.valueOf(i));
        this.mDur.setOnKeyListener(this.mOnKeyListener);
        this.mDone = (Button) findViewById(R.id.done);
        this.mDone.setOnClickListener(this.mOnDoneClickListener);
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        int i;
        super.onSaveInstanceState(bundle);
        this.mState = new Bundle();
        this.mState.putInt("slide_index", this.mCurSlide);
        this.mState.putInt(SLIDE_TOTAL, this.mTotal);
        try {
            i = Integer.parseInt(this.mDur.getText().toString());
        } catch (NumberFormatException e) {
            i = 5;
        }
        this.mState.putInt(SLIDE_DUR, i);
        bundle.putBundle("state", this.mState);
    }
}
