package com.android.mms.layout;

public class HVGALayoutParameters implements LayoutParameters {
    private static final boolean DEBUG = false;
    private static final int IMAGE_HEIGHT_LANDSCAPE = 240;
    private static final int IMAGE_HEIGHT_PORTRAIT = 320;
    private static final boolean LOCAL_LOGV = false;
    private static final String TAG = "HVGALayoutParameters";
    private static final int TEXT_HEIGHT_LANDSCAPE = 80;
    private static final int TEXT_HEIGHT_PORTRAIT = 160;
    private int mType = -1;

    public HVGALayoutParameters(int i) {
        if (i == 10 || i == 11) {
            this.mType = i;
            return;
        }
        throw new IllegalArgumentException("Bad layout type detected: " + i);
    }

    public int getHeight() {
        return this.mType == 10 ? 320 : 480;
    }

    public int getImageHeight() {
        if (this.mType == 10) {
            return IMAGE_HEIGHT_LANDSCAPE;
        }
        return 320;
    }

    public int getTextHeight() {
        return this.mType == 10 ? 80 : 160;
    }

    public int getType() {
        return this.mType;
    }

    public String getTypeDescription() {
        return this.mType == 10 ? "HVGA-L" : "HVGA-P";
    }

    public int getWidth() {
        return this.mType == 10 ? 480 : 320;
    }
}
