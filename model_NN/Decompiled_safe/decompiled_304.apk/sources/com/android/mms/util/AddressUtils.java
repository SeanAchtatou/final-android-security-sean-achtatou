package com.android.mms.util;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import com.android.provider.Telephony;
import com.google.android.mms.pdu.EncodedStringValue;
import com.google.android.mms.pdu.PduPersister;
import vc.lx.sms.util.SqliteWrapper;
import vc.lx.sms2.R;

public class AddressUtils {
    private static final String TAG = "AddressUtils";

    private AddressUtils() {
    }

    /* JADX INFO: finally extract failed */
    public static String getFrom(Context context, Uri uri) {
        String lastPathSegment = uri.getLastPathSegment();
        Uri.Builder buildUpon = Telephony.Mms.CONTENT_URI.buildUpon();
        buildUpon.appendPath(lastPathSegment).appendPath("addr");
        Cursor query = SqliteWrapper.query(context, context.getContentResolver(), buildUpon.build(), new String[]{"address", Telephony.Mms.Addr.CHARSET}, "type=137", null, null);
        if (query != null) {
            try {
                if (query.moveToFirst()) {
                    String string = query.getString(0);
                    if (!TextUtils.isEmpty(string)) {
                        String string2 = new EncodedStringValue(query.getInt(1), PduPersister.getBytes(string)).getString();
                        query.close();
                        return string2;
                    }
                }
                query.close();
            } catch (Throwable th) {
                query.close();
                throw th;
            }
        }
        return context.getString(R.string.hidden_sender_address);
    }
}
