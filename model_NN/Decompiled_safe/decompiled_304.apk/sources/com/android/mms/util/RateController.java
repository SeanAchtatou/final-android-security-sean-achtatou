package com.android.mms.util;

import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.util.Log;
import com.android.provider.Telephony;
import vc.lx.sms.util.SqliteWrapper;

public class RateController {
    private static final int ANSWER_NO = 2;
    public static final int ANSWER_TIMEOUT = 20000;
    private static final int ANSWER_YES = 1;
    private static final boolean DEBUG = false;
    private static final boolean LOCAL_LOGV = false;
    private static final int NO_ANSWER = 0;
    private static final long ONE_HOUR = 3600000;
    private static final int RATE_LIMIT = 100;
    public static final String RATE_LIMIT_CONFIRMED_ACTION = "com.android.mms.harmony.RATE_LIMIT_CONFIRMED";
    public static final String RATE_LIMIT_SURPASSED_ACTION = "com.android.mms.harmony.RATE_LIMIT_SURPASSED";
    private static final String TAG = "RateController";
    private static RateController sInstance;
    private static boolean sMutexLock;
    /* access modifiers changed from: private */
    public int mAnswer;
    private final BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if (RateController.RATE_LIMIT_CONFIRMED_ACTION.equals(intent.getAction())) {
                synchronized (this) {
                    int unused = RateController.this.mAnswer = intent.getBooleanExtra("answer", false) ? 1 : 2;
                    notifyAll();
                }
            }
        }
    };
    private final Context mContext;

    private RateController(Context context) {
        this.mContext = context;
    }

    public static RateController getInstance() {
        if (sInstance != null) {
            return sInstance;
        }
        throw new IllegalStateException("Uninitialized.");
    }

    public static void init(Context context) {
        if (sInstance != null) {
            Log.w(TAG, "Already initialized.");
        }
        sInstance = new RateController(context);
    }

    private synchronized int waitForAnswer() {
        int i = 0;
        while (this.mAnswer == 0 && i < 20000) {
            try {
                wait(1000);
            } catch (InterruptedException e) {
            }
            i += 1000;
        }
        return this.mAnswer;
    }

    public synchronized boolean isAllowedByUser() {
        while (sMutexLock) {
            try {
                wait();
            } catch (InterruptedException e) {
            }
        }
        sMutexLock = true;
        this.mContext.registerReceiver(this.mBroadcastReceiver, new IntentFilter(RATE_LIMIT_CONFIRMED_ACTION));
        this.mAnswer = 0;
        try {
            Intent intent = new Intent(RATE_LIMIT_SURPASSED_ACTION);
            intent.addFlags(268435456);
            this.mContext.startActivity(intent);
        } finally {
            this.mContext.unregisterReceiver(this.mBroadcastReceiver);
            sMutexLock = false;
            notifyAll();
        }
        return waitForAnswer() == 1;
    }

    /* JADX INFO: finally extract failed */
    public final boolean isLimitSurpassed() {
        Cursor query = SqliteWrapper.query(this.mContext, this.mContext.getContentResolver(), Telephony.Mms.Rate.CONTENT_URI, new String[]{"COUNT(*) AS rate"}, "sent_time>" + (System.currentTimeMillis() - ONE_HOUR), null, null);
        if (query != null) {
            try {
                if (query.moveToFirst()) {
                    boolean z = query.getInt(0) >= 100;
                    query.close();
                    return z;
                }
                query.close();
            } catch (Throwable th) {
                query.close();
                throw th;
            }
        }
        return false;
    }

    public final void update() {
        ContentValues contentValues = new ContentValues(1);
        contentValues.put(Telephony.Mms.Rate.SENT_TIME, Long.valueOf(System.currentTimeMillis()));
        SqliteWrapper.insert(this.mContext, this.mContext.getContentResolver(), Telephony.Mms.Rate.CONTENT_URI, contentValues);
    }
}
