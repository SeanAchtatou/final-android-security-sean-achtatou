package com.android.mms.util;

import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.provider.ContactsContract;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import android.util.Log;
import com.android.provider.Telephony;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import vc.lx.sms.util.MessageUtils;
import vc.lx.sms.util.SqliteWrapper;
import vc.lx.sms2.R;

public class ContactInfoCache {
    private static final String[] CALLER_ID_PROJECTION = {"data1", "data3", "display_name", Telephony.Mms.Addr.CONTACT_ID, "contact_presence", "contact_status"};
    private static final String CALLER_ID_SELECTION = "PHONE_NUMBERS_EQUAL(data1,?) AND mimetype='vnd.android.cursor.item/phone_v2' AND raw_contact_id IN (SELECT raw_contact_id  FROM phone_lookup WHERE normalized_number GLOB('+*'))";
    private static final int CONTACT_ID_COLUMN = 3;
    private static final int CONTACT_NAME_COLUMN = 2;
    private static final int CONTACT_PRESENCE_COLUMN = 4;
    private static final int CONTACT_STATUS_COLUMN = 5;
    private static final int EMAIL_CONTACT_NAME_COLUMN = 3;
    private static final int EMAIL_ID_COLUMN = 2;
    private static final int EMAIL_NAME_COLUMN = 0;
    private static final String[] EMAIL_PROJECTION = {"data4", "contact_presence", Telephony.Mms.Addr.CONTACT_ID, "display_name"};
    private static final String EMAIL_SELECTION = "data1=? AND mimetype='vnd.android.cursor.item/email_v2'";
    private static final int EMAIL_STATUS_COLUMN = 1;
    private static final Uri EMAIL_WITH_PRESENCE_URI = ContactsContract.Data.CONTENT_URI;
    private static final boolean LOCAL_DEBUG = false;
    private static final int OFFLINE = 0;
    private static final Uri PHONES_WITH_PRESENCE_URI = ContactsContract.Data.CONTENT_URI;
    private static final int PHONE_LABEL_COLUMN = 1;
    private static final int PHONE_NUMBER_COLUMN = 0;
    private static final String SEPARATOR = ";";
    private static final String TAG = "Mms/cache";
    private static ContactInfoCache sInstance;
    private final HashMap<String, CacheEntry> mCache = new HashMap<>();
    private final Context mContext;

    public class CacheEntry {
        /* access modifiers changed from: private */
        public boolean isStale;
        public BitmapDrawable mAvatar;
        public String name;
        public long person_id;
        public String phoneLabel;
        public String phoneNumber;
        public int presenceResId;
        public String presenceText;

        public CacheEntry() {
        }

        public boolean isStale() {
            return this.isStale;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder("name=" + this.name);
            sb.append(", phone=" + this.phoneNumber);
            sb.append(", pid=" + this.person_id);
            sb.append(", presence=" + this.presenceResId);
            sb.append(", stale=" + this.isStale);
            return sb.toString();
        }
    }

    private ContactInfoCache(Context context) {
        this.mContext = context;
    }

    private String getCallerId(String str) {
        CacheEntry contactInfo = getContactInfo(str);
        return (contactInfo == null || TextUtils.isEmpty(contactInfo.name)) ? str : contactInfo.name;
    }

    private static String getEmailDisplayName(String str) {
        Matcher matcher = Telephony.Mms.QUOTED_STRING_PATTERN.matcher(str);
        return matcher.matches() ? matcher.group(1) : str;
    }

    public static ContactInfoCache getInstance() {
        return sInstance;
    }

    private int getPresenceIconResourceId(int i) {
        if (i != 0) {
            return ContactsContract.StatusUpdates.getPresenceIconResourceId(i);
        }
        return 0;
    }

    public static void init(Context context) {
        sInstance = new ContactInfoCache(context);
    }

    private void loadAvatar(CacheEntry cacheEntry, Cursor cursor) {
        InputStream openContactPhotoInputStream;
        if (cacheEntry.person_id != 0 && cacheEntry.mAvatar == null && (openContactPhotoInputStream = ContactsContract.Contacts.openContactPhotoInputStream(this.mContext.getContentResolver(), ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, cacheEntry.person_id))) != null) {
            cacheEntry.mAvatar = new BitmapDrawable(this.mContext.getResources(), BitmapFactory.decodeStream(openContactPhotoInputStream));
            try {
                openContactPhotoInputStream.close();
            } catch (IOException e) {
                cacheEntry.mAvatar = null;
            }
        }
    }

    private void log(String str) {
        Log.d(TAG, "[ContactInfoCache] " + str);
    }

    /* JADX INFO: finally extract failed */
    private CacheEntry queryContactInfoByNumber(String str) {
        CacheEntry cacheEntry = new CacheEntry();
        cacheEntry.phoneNumber = str;
        String replace = CALLER_ID_SELECTION.replace("+", PhoneNumberUtils.toCallerIDMinMatch(str));
        Cursor query = this.mContext.getContentResolver().query(PHONES_WITH_PRESENCE_URI, CALLER_ID_PROJECTION, replace, new String[]{str}, null);
        if (query == null) {
            Log.w(TAG, "queryContactInfoByNumber(" + str + ") returned NULL cursor!" + " contact uri used " + PHONES_WITH_PRESENCE_URI);
            return cacheEntry;
        }
        try {
            if (query.moveToFirst()) {
                cacheEntry.phoneLabel = query.getString(1);
                cacheEntry.name = query.getString(2);
                cacheEntry.person_id = query.getLong(3);
                cacheEntry.presenceResId = getPresenceIconResourceId(query.getInt(4));
                cacheEntry.presenceText = query.getString(5);
                loadAvatar(cacheEntry, query);
            }
            query.close();
            return cacheEntry;
        } catch (Throwable th) {
            query.close();
            throw th;
        }
    }

    private CacheEntry queryEmailDisplayName(String str) {
        CacheEntry cacheEntry = new CacheEntry();
        Cursor query = SqliteWrapper.query(this.mContext, this.mContext.getContentResolver(), EMAIL_WITH_PRESENCE_URI, EMAIL_PROJECTION, EMAIL_SELECTION, new String[]{str}, null);
        if (query != null) {
            while (true) {
                try {
                    if (!query.moveToNext()) {
                        break;
                    }
                    cacheEntry.presenceResId = getPresenceIconResourceId(query.getInt(1));
                    cacheEntry.person_id = query.getLong(2);
                    String string = query.getString(0);
                    if (TextUtils.isEmpty(string)) {
                        string = query.getString(3);
                    }
                    if (!TextUtils.isEmpty(string)) {
                        cacheEntry.name = string;
                        loadAvatar(cacheEntry, query);
                        break;
                    }
                } finally {
                    query.close();
                }
            }
        }
        return cacheEntry;
    }

    public void dump() {
        synchronized (this.mCache) {
            Log.i(TAG, "ContactInfoCache.dump");
            for (String next : this.mCache.keySet()) {
                CacheEntry cacheEntry = this.mCache.get(next);
                if (cacheEntry != null) {
                    Log.i(TAG, "key=" + next + ", cacheEntry={" + cacheEntry.toString() + '}');
                } else {
                    Log.i(TAG, "key=" + next + ", cacheEntry={null}");
                }
            }
        }
    }

    public CacheEntry getContactInfo(String str) {
        return getContactInfo(str, true);
    }

    public CacheEntry getContactInfo(String str, boolean z) {
        return Telephony.Mms.isEmailAddress(str) ? getContactInfoForEmailAddress(str, z) : getContactInfoForPhoneNumber(str, z);
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x002d A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.android.mms.util.ContactInfoCache.CacheEntry getContactInfoForEmailAddress(java.lang.String r4, boolean r5) {
        /*
            r3 = this;
            java.util.HashMap<java.lang.String, com.android.mms.util.ContactInfoCache$CacheEntry> r1 = r3.mCache
            monitor-enter(r1)
            java.util.HashMap<java.lang.String, com.android.mms.util.ContactInfoCache$CacheEntry> r0 = r3.mCache     // Catch:{ all -> 0x0022 }
            boolean r0 = r0.containsKey(r4)     // Catch:{ all -> 0x0022 }
            if (r0 == 0) goto L_0x001d
            java.util.HashMap<java.lang.String, com.android.mms.util.ContactInfoCache$CacheEntry> r0 = r3.mCache     // Catch:{ all -> 0x0022 }
            java.lang.Object r0 = r0.get(r4)     // Catch:{ all -> 0x0022 }
            com.android.mms.util.ContactInfoCache$CacheEntry r0 = (com.android.mms.util.ContactInfoCache.CacheEntry) r0     // Catch:{ all -> 0x0022 }
            if (r5 == 0) goto L_0x001b
            boolean r2 = r0.isStale()     // Catch:{ all -> 0x0022 }
            if (r2 != 0) goto L_0x0025
        L_0x001b:
            monitor-exit(r1)     // Catch:{ all -> 0x0022 }
        L_0x001c:
            return r0
        L_0x001d:
            if (r5 != 0) goto L_0x0025
            r0 = 0
            monitor-exit(r1)     // Catch:{ all -> 0x0022 }
            goto L_0x001c
        L_0x0022:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0022 }
            throw r0
        L_0x0025:
            monitor-exit(r1)     // Catch:{ all -> 0x0022 }
            com.android.mms.util.ContactInfoCache$CacheEntry r0 = r3.queryEmailDisplayName(r4)
            java.util.HashMap<java.lang.String, com.android.mms.util.ContactInfoCache$CacheEntry> r1 = r3.mCache
            monitor-enter(r1)
            java.util.HashMap<java.lang.String, com.android.mms.util.ContactInfoCache$CacheEntry> r2 = r3.mCache     // Catch:{ all -> 0x0034 }
            r2.put(r4, r0)     // Catch:{ all -> 0x0034 }
            monitor-exit(r1)     // Catch:{ all -> 0x0034 }
            goto L_0x001c
        L_0x0034:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0034 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.mms.util.ContactInfoCache.getContactInfoForEmailAddress(java.lang.String, boolean):com.android.mms.util.ContactInfoCache$CacheEntry");
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0031 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.android.mms.util.ContactInfoCache.CacheEntry getContactInfoForPhoneNumber(java.lang.String r5, boolean r6) {
        /*
            r4 = this;
            java.lang.String r1 = android.telephony.PhoneNumberUtils.stripSeparators(r5)
            java.util.HashMap<java.lang.String, com.android.mms.util.ContactInfoCache$CacheEntry> r2 = r4.mCache
            monitor-enter(r2)
            java.util.HashMap<java.lang.String, com.android.mms.util.ContactInfoCache$CacheEntry> r0 = r4.mCache     // Catch:{ all -> 0x0026 }
            boolean r0 = r0.containsKey(r1)     // Catch:{ all -> 0x0026 }
            if (r0 == 0) goto L_0x0021
            java.util.HashMap<java.lang.String, com.android.mms.util.ContactInfoCache$CacheEntry> r0 = r4.mCache     // Catch:{ all -> 0x0026 }
            java.lang.Object r0 = r0.get(r1)     // Catch:{ all -> 0x0026 }
            com.android.mms.util.ContactInfoCache$CacheEntry r0 = (com.android.mms.util.ContactInfoCache.CacheEntry) r0     // Catch:{ all -> 0x0026 }
            if (r6 == 0) goto L_0x001f
            boolean r3 = r0.isStale()     // Catch:{ all -> 0x0026 }
            if (r3 != 0) goto L_0x0029
        L_0x001f:
            monitor-exit(r2)     // Catch:{ all -> 0x0026 }
        L_0x0020:
            return r0
        L_0x0021:
            if (r6 != 0) goto L_0x0029
            r0 = 0
            monitor-exit(r2)     // Catch:{ all -> 0x0026 }
            goto L_0x0020
        L_0x0026:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0026 }
            throw r0
        L_0x0029:
            monitor-exit(r2)     // Catch:{ all -> 0x0026 }
            com.android.mms.util.ContactInfoCache$CacheEntry r0 = r4.queryContactInfoByNumber(r1)
            java.util.HashMap<java.lang.String, com.android.mms.util.ContactInfoCache$CacheEntry> r2 = r4.mCache
            monitor-enter(r2)
            java.util.HashMap<java.lang.String, com.android.mms.util.ContactInfoCache$CacheEntry> r3 = r4.mCache     // Catch:{ all -> 0x0038 }
            r3.put(r1, r0)     // Catch:{ all -> 0x0038 }
            monitor-exit(r2)     // Catch:{ all -> 0x0038 }
            goto L_0x0020
        L_0x0038:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0038 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.mms.util.ContactInfoCache.getContactInfoForPhoneNumber(java.lang.String, boolean):com.android.mms.util.ContactInfoCache$CacheEntry");
    }

    public String getContactName(String str) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        for (String str2 : str.split(";")) {
            if (str2.length() > 0) {
                sb.append(";");
                if (MessageUtils.isLocalNumber(this.mContext, str2)) {
                    sb.append(this.mContext.getString(R.string.me));
                } else if (Telephony.Mms.isEmailAddress(str2)) {
                    sb.append(getDisplayName(str2));
                } else {
                    sb.append(getCallerId(str2));
                }
            }
        }
        return sb.length() > 0 ? sb.substring(1) : "";
    }

    public String getDisplayName(String str) {
        Matcher matcher = Telephony.Mms.NAME_ADDR_EMAIL_PATTERN.matcher(str);
        if (matcher.matches()) {
            return getEmailDisplayName(matcher.group(1));
        }
        CacheEntry contactInfoForEmailAddress = getContactInfoForEmailAddress(str, true);
        return (contactInfoForEmailAddress == null || contactInfoForEmailAddress.name == null) ? str : contactInfoForEmailAddress.name;
    }

    public void invalidateCache() {
        synchronized (this.mCache) {
            for (Map.Entry<String, CacheEntry> value : this.mCache.entrySet()) {
                boolean unused = ((CacheEntry) value.getValue()).isStale = true;
            }
        }
    }

    public void invalidateContact(String str) {
        synchronized (this.mCache) {
            CacheEntry cacheEntry = this.mCache.get(str);
            if (cacheEntry != null) {
                boolean unused = cacheEntry.isStale = true;
            }
        }
    }
}
