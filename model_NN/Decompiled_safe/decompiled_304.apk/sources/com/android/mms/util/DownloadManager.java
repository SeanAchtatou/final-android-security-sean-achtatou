package com.android.mms.util;

import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.os.SystemProperties;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;
import com.android.internal.telephony.TelephonyIntents;
import com.android.internal.telephony.TelephonyProperties;
import com.android.provider.Telephony;
import com.google.android.mms.MmsException;
import com.google.android.mms.pdu.EncodedStringValue;
import com.google.android.mms.pdu.NotificationInd;
import com.google.android.mms.pdu.PduPersister;
import vc.lx.sms.data.Contact;
import vc.lx.sms.ui.MessagingPreferenceActivity;
import vc.lx.sms.util.SqliteWrapper;
import vc.lx.sms2.R;

public class DownloadManager {
    private static final boolean DEBUG = false;
    private static final int DEFERRED_MASK = 4;
    private static final boolean LOCAL_LOGV = false;
    public static final int STATE_DOWNLOADING = 129;
    public static final int STATE_PERMANENT_FAILURE = 135;
    public static final int STATE_TRANSIENT_FAILURE = 130;
    public static final int STATE_UNSTARTED = 128;
    private static final String TAG = "DownloadManager";
    /* access modifiers changed from: private */
    public static DownloadManager sInstance;
    /* access modifiers changed from: private */
    public boolean mAutoDownload;
    /* access modifiers changed from: private */
    public final Context mContext;
    private final Handler mHandler;
    /* access modifiers changed from: private */
    public final SharedPreferences mPreferences;
    private final SharedPreferences.OnSharedPreferenceChangeListener mPreferencesChangeListener = new SharedPreferences.OnSharedPreferenceChangeListener() {
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String str) {
            if (MessagingPreferenceActivity.AUTO_RETRIEVAL.equals(str) || MessagingPreferenceActivity.RETRIEVAL_DURING_ROAMING.equals(str)) {
                synchronized (DownloadManager.sInstance) {
                    boolean unused = DownloadManager.this.mAutoDownload = DownloadManager.getAutoDownloadState(sharedPreferences);
                }
            }
        }
    };
    private final BroadcastReceiver mRoamingStateListener = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if (TelephonyIntents.ACTION_SERVICE_STATE_CHANGED.equals(intent.getAction())) {
                boolean z = intent.getExtras().getBoolean("roaming");
                synchronized (DownloadManager.sInstance) {
                    boolean unused = DownloadManager.this.mAutoDownload = DownloadManager.getAutoDownloadState(DownloadManager.this.mPreferences, z);
                }
            }
        }
    };

    private DownloadManager(Context context) {
        this.mContext = context;
        this.mHandler = new Handler();
        this.mPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        this.mPreferences.registerOnSharedPreferenceChangeListener(this.mPreferencesChangeListener);
        context.registerReceiver(this.mRoamingStateListener, new IntentFilter(TelephonyIntents.ACTION_SERVICE_STATE_CHANGED));
        this.mAutoDownload = getAutoDownloadState(this.mPreferences);
    }

    static boolean getAutoDownloadState(SharedPreferences sharedPreferences) {
        return getAutoDownloadState(sharedPreferences, isRoaming());
    }

    static boolean getAutoDownloadState(SharedPreferences sharedPreferences, boolean z) {
        if (sharedPreferences.getBoolean(MessagingPreferenceActivity.AUTO_RETRIEVAL, true)) {
            boolean z2 = sharedPreferences.getBoolean(MessagingPreferenceActivity.RETRIEVAL_DURING_ROAMING, false);
            if (!z || z2) {
                return true;
            }
        }
        return false;
    }

    public static DownloadManager getInstance() {
        if (sInstance != null) {
            return sInstance;
        }
        throw new IllegalStateException("Uninitialized.");
    }

    /* access modifiers changed from: private */
    public String getMessage(Uri uri) throws MmsException {
        NotificationInd notificationInd = (NotificationInd) PduPersister.getPduPersister(this.mContext).load(uri);
        EncodedStringValue subject = notificationInd.getSubject();
        String string = subject != null ? subject.getString() : this.mContext.getString(R.string.no_subject);
        EncodedStringValue from = notificationInd.getFrom();
        return this.mContext.getString(R.string.dl_failure_notification, string, from != null ? Contact.get(from.getString(), false, this.mContext).getName() : this.mContext.getString(R.string.unknown_sender));
    }

    public static void init(Context context) {
        if (sInstance != null) {
            Log.w(TAG, "Already initialized.");
        }
        sInstance = new DownloadManager(context);
    }

    static boolean isRoaming() {
        return "true".equals(SystemProperties.get(TelephonyProperties.PROPERTY_OPERATOR_ISROAMING, null));
    }

    /* JADX INFO: finally extract failed */
    public int getState(Uri uri) {
        Cursor query = SqliteWrapper.query(this.mContext, this.mContext.getContentResolver(), uri, new String[]{Telephony.BaseMmsColumns.STATUS}, null, null, null);
        if (query != null) {
            try {
                if (query.moveToFirst()) {
                    int i = query.getInt(0) & -5;
                    query.close();
                    return i;
                }
                query.close();
            } catch (Throwable th) {
                query.close();
                throw th;
            }
        }
        return 128;
    }

    public boolean isAuto() {
        return this.mAutoDownload;
    }

    public void markState(final Uri uri, int i) {
        int i2;
        try {
            if (((NotificationInd) PduPersister.getPduPersister(this.mContext).load(uri)).getExpiry() >= System.currentTimeMillis() / 1000 || i != 129) {
                if (i == 135) {
                    this.mHandler.post(new Runnable() {
                        public void run() {
                            try {
                                Toast.makeText(DownloadManager.this.mContext, DownloadManager.this.getMessage(uri), 1).show();
                            } catch (MmsException e) {
                                Log.e(DownloadManager.TAG, e.getMessage(), e);
                            }
                        }
                    });
                    i2 = i;
                } else {
                    i2 = !this.mAutoDownload ? i | 4 : i;
                }
                ContentValues contentValues = new ContentValues(1);
                contentValues.put(Telephony.BaseMmsColumns.STATUS, Integer.valueOf(i2));
                SqliteWrapper.update(this.mContext, this.mContext.getContentResolver(), uri, contentValues, null, null);
                return;
            }
            this.mHandler.post(new Runnable() {
                public void run() {
                    Toast.makeText(DownloadManager.this.mContext, (int) R.string.dl_expired_notification, 1).show();
                }
            });
            SqliteWrapper.delete(this.mContext, this.mContext.getContentResolver(), uri, null, null);
        } catch (MmsException e) {
            Log.e(TAG, e.getMessage(), e);
        }
    }

    public void showErrorCodeToast(final int i) {
        this.mHandler.post(new Runnable() {
            public void run() {
                try {
                    Toast.makeText(DownloadManager.this.mContext, i, 1).show();
                } catch (Exception e) {
                    Log.e(DownloadManager.TAG, "Caught an exception in showErrorCodeToast");
                }
            }
        });
    }
}
