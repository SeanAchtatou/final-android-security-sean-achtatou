package com.android.mms.util;

import android.content.Context;
import android.content.res.Resources;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.SimpleAdapter;
import com.android.internal.widget.Smileys;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import vc.lx.sms2.R;

public class SmileyParser {
    public static final int DEFAULT_SMILEY_NAMES = 2131099653;
    public static final int[] DEFAULT_SMILEY_RES_IDS = {Smileys.getSmileyResource(Smileys.HAPPY), Smileys.getSmileyResource(Smileys.SAD), Smileys.getSmileyResource(Smileys.WINKING), Smileys.getSmileyResource(Smileys.TONGUE_STICKING_OUT), Smileys.getSmileyResource(Smileys.SURPRISED), Smileys.getSmileyResource(Smileys.KISSING), Smileys.getSmileyResource(Smileys.YELLING), Smileys.getSmileyResource(Smileys.COOL), Smileys.getSmileyResource(Smileys.MONEY_MOUTH), Smileys.getSmileyResource(Smileys.FOOT_IN_MOUTH), Smileys.getSmileyResource(Smileys.ANGEL), Smileys.getSmileyResource(Smileys.UNDECIDED), Smileys.getSmileyResource(Smileys.CRYING), Smileys.getSmileyResource(Smileys.LIPS_ARE_SEALED), Smileys.getSmileyResource(Smileys.LAUGHING), Smileys.getSmileyResource(Smileys.WTF)};
    public static final int DEFAULT_SMILEY_TEXTS = 2131099648;
    private static SmileyParser sInstance;
    private final Context mContext;
    private final Pattern mPattern = buildPattern();
    private final String[] mSmileyTexts = this.mContext.getResources().getStringArray(R.array.default_smiley_texts);
    private final HashMap<String, Integer> mSmileyToRes = buildSmileyToRes();

    private SmileyParser(Context context) {
        this.mContext = context;
    }

    private Pattern buildPattern() {
        StringBuilder sb = new StringBuilder(this.mSmileyTexts.length * 3);
        sb.append('(');
        for (String quote : this.mSmileyTexts) {
            sb.append(Pattern.quote(quote));
            sb.append('|');
        }
        sb.replace(sb.length() - 1, sb.length(), ")");
        return Pattern.compile(sb.toString());
    }

    private HashMap<String, Integer> buildSmileyToRes() {
        if (DEFAULT_SMILEY_RES_IDS.length != this.mSmileyTexts.length) {
            throw new IllegalStateException("Smiley resource ID/text mismatch");
        }
        HashMap<String, Integer> hashMap = new HashMap<>(this.mSmileyTexts.length);
        for (int i = 0; i < this.mSmileyTexts.length; i++) {
            hashMap.put(this.mSmileyTexts[i], Integer.valueOf(DEFAULT_SMILEY_RES_IDS[i]));
        }
        return hashMap;
    }

    public static SmileyParser getInstance() {
        return sInstance;
    }

    public static SimpleAdapter getSmileyAdapter(Context context) {
        boolean z;
        int[] iArr = DEFAULT_SMILEY_RES_IDS;
        final Resources resources = context.getResources();
        int length = iArr.length;
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < length; i++) {
            int i2 = 0;
            while (true) {
                if (i2 >= i) {
                    z = false;
                    break;
                } else if (iArr[i] == iArr[i2]) {
                    z = true;
                    break;
                } else {
                    i2++;
                }
            }
            if (!z) {
                HashMap hashMap = new HashMap();
                hashMap.put("icon", Integer.valueOf(iArr[i]));
                arrayList.add(hashMap);
            }
        }
        SimpleAdapter simpleAdapter = new SimpleAdapter(context, arrayList, R.layout.smiley_menu_item, new String[]{"icon", "name", "text"}, new int[]{R.id.smiley_icon});
        simpleAdapter.setViewBinder(new SimpleAdapter.ViewBinder() {
            public boolean setViewValue(View view, Object obj, String str) {
                if (!(view instanceof ImageView)) {
                    return false;
                }
                ((ImageView) view).setImageDrawable(resources.getDrawable(((Integer) obj).intValue()));
                return true;
            }
        });
        return simpleAdapter;
    }

    public static void init(Context context) {
        sInstance = new SmileyParser(context);
    }

    public CharSequence addSmileySpans(CharSequence charSequence) {
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(charSequence);
        Matcher matcher = this.mPattern.matcher(charSequence);
        while (matcher.find()) {
            spannableStringBuilder.setSpan(new ImageSpan(this.mContext, this.mSmileyToRes.get(matcher.group()).intValue()), matcher.start(), matcher.end(), 33);
        }
        return spannableStringBuilder;
    }
}
