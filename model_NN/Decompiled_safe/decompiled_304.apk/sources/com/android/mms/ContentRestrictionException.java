package com.android.mms;

public class ContentRestrictionException extends RuntimeException {
    private static final long serialVersionUID = 516136015813043499L;

    public ContentRestrictionException() {
    }

    public ContentRestrictionException(Exception exc) {
        super(exc);
    }

    public ContentRestrictionException(String str) {
        super(str);
    }
}
