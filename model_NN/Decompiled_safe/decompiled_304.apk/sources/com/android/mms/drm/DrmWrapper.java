package com.android.mms.drm;

import android.net.Uri;
import android.util.Log;
import com.android.drm.mobile1.DrmException;
import com.android.drm.mobile1.DrmRawContent;
import com.android.drm.mobile1.DrmRights;
import com.android.drm.mobile1.DrmRightsManager;
import com.google.android.mms.ContentType;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class DrmWrapper {
    private static final boolean DEBUG = false;
    private static final boolean LOCAL_LOGV = false;
    private static final String LOG_TAG = "DrmWrapper";
    private final byte[] mData;
    private final Uri mDataUri;
    private byte[] mDecryptedData;
    private final DrmRawContent mDrmObject;
    private DrmRights mRight;

    public DrmWrapper(String str, Uri uri, byte[] bArr) throws DrmException, IOException {
        if (str == null || bArr == null) {
            throw new IllegalArgumentException("Content-Type or data may not be null.");
        }
        this.mDataUri = uri;
        this.mData = bArr;
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bArr);
        this.mDrmObject = new DrmRawContent(byteArrayInputStream, byteArrayInputStream.available(), str);
        if (!isRightsInstalled()) {
            installRights(bArr);
        }
    }

    private int getPermission() {
        String contentType = this.mDrmObject.getContentType();
        return (ContentType.isAudioType(contentType) || ContentType.isVideoType(contentType)) ? 1 : 2;
    }

    public boolean consumeRights() {
        if (this.mRight == null) {
            return false;
        }
        return this.mRight.consumeRights(getPermission());
    }

    public String getContentType() {
        return this.mDrmObject.getContentType();
    }

    public byte[] getDecryptedData() throws IOException {
        if (this.mDecryptedData == null && this.mRight != null) {
            InputStream contentInputStream = this.mDrmObject.getContentInputStream(this.mRight);
            try {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                byte[] bArr = new byte[256];
                while (true) {
                    int read = contentInputStream.read(bArr);
                    if (read <= 0) {
                        break;
                    }
                    byteArrayOutputStream.write(bArr, 0, read);
                }
                this.mDecryptedData = byteArrayOutputStream.toByteArray();
                try {
                } catch (IOException e) {
                    Log.e(LOG_TAG, e.getMessage(), e);
                }
            } finally {
                try {
                    contentInputStream.close();
                } catch (IOException e2) {
                    Log.e(LOG_TAG, e2.getMessage(), e2);
                }
            }
        }
        if (this.mDecryptedData == null) {
            return null;
        }
        byte[] bArr2 = new byte[this.mDecryptedData.length];
        System.arraycopy(this.mDecryptedData, 0, bArr2, 0, this.mDecryptedData.length);
        return bArr2;
    }

    public byte[] getOriginalData() {
        return this.mData;
    }

    public Uri getOriginalUri() {
        return this.mDataUri;
    }

    public String getRightsAddress() {
        if (this.mDrmObject == null) {
            return null;
        }
        return this.mDrmObject.getRightsAddress();
    }

    public void installRights(byte[] bArr) throws DrmException, IOException {
        if (bArr == null) {
            throw new DrmException("Right data may not be null.");
        }
        this.mRight = DrmRightsManager.getInstance().installRights(new ByteArrayInputStream(bArr), bArr.length, "application/vnd.oma.drm.message");
    }

    public boolean isAllowedToForward() {
        return 3 == this.mDrmObject.getRawType();
    }

    public boolean isRightsInstalled() {
        if (this.mRight != null) {
            return true;
        }
        this.mRight = DrmRightsManager.getInstance().queryRights(this.mDrmObject);
        return this.mRight != null;
    }
}
