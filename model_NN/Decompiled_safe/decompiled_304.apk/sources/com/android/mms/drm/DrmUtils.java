package com.android.mms.drm;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.util.Log;
import java.io.IOException;
import java.io.OutputStream;
import vc.lx.sms.util.SqliteWrapper;

public class DrmUtils {
    private static final Uri DRM_TEMP_URI = Uri.parse("content://mms/drm");
    private static final String TAG = "DrmUtils";

    private DrmUtils() {
    }

    public static void cleanupStorage(Context context) {
        SqliteWrapper.delete(context, context.getContentResolver(), DRM_TEMP_URI, null, null);
    }

    public static Uri insert(Context context, DrmWrapper drmWrapper) throws IOException {
        OutputStream outputStream;
        ContentResolver contentResolver = context.getContentResolver();
        Uri insert = SqliteWrapper.insert(context, contentResolver, DRM_TEMP_URI, new ContentValues(0));
        try {
            OutputStream openOutputStream = contentResolver.openOutputStream(insert);
            try {
                byte[] decryptedData = drmWrapper.getDecryptedData();
                if (decryptedData != null) {
                    openOutputStream.write(decryptedData);
                }
                if (openOutputStream != null) {
                    try {
                        openOutputStream.close();
                    } catch (IOException e) {
                        Log.e(TAG, e.getMessage(), e);
                    }
                }
                return insert;
            } catch (Throwable th) {
                Throwable th2 = th;
                outputStream = openOutputStream;
                th = th2;
                if (outputStream != null) {
                    try {
                        outputStream.close();
                    } catch (IOException e2) {
                        Log.e(TAG, e2.getMessage(), e2);
                    }
                }
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            outputStream = null;
        }
    }
}
