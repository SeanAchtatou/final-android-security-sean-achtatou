package com.android.mms;

import android.content.Context;
import android.content.res.XmlResourceParser;
import android.util.Log;
import com.android.internal.util.XmlUtils;
import com.google.android.mms.pdu.PduPart;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParserException;
import vc.lx.sms.data.LogTag;
import vc.lx.sms.util.MessageUtils;
import vc.lx.sms2.R;

public class MmsConfig {
    private static final boolean DEBUG = false;
    private static final String DEFAULT_HTTP_KEY_X_WAP_PROFILE = "x-wap-profile";
    private static final String DEFAULT_USER_AGENT = "Android-Mms/2.0";
    private static final boolean LOCAL_LOGV = false;
    private static final int MAX_IMAGE_HEIGHT = 480;
    private static final int MAX_IMAGE_WIDTH = 640;
    private static final String TAG = "MmsConfig";
    private static boolean mAliasEnabled = false;
    private static int mAliasRuleMaxChars = 48;
    private static int mAliasRuleMinChars = 2;
    private static boolean mAllowAttachAudio = true;
    private static int mDefaultMMSMessagesPerThread = 20;
    private static int mDefaultSMSMessagesPerThread = PduPart.P_CONTENT_TRANSFER_ENCODING;
    private static String mEmailGateway = null;
    private static String mHttpParams = null;
    private static String mHttpParamsLine1Key = null;
    private static int mHttpSocketTimeout = 60000;
    private static int mMaxImageHeight = 480;
    private static int mMaxImageWidth = MAX_IMAGE_WIDTH;
    private static int mMaxMessageCountPerThread = MessageUtils.MESSAGE_OVERHEAD;
    private static int mMaxMessageSize = 307200;
    private static int mMaxSizeScaleForPendingMmsAllowed = 4;
    private static int mMinMessageCountPerThread = 2;
    private static int mMinimumSlideElementDuration = 7;
    private static int mMmsEnabled = 1;
    private static boolean mNotifyWapMMSC = false;
    private static int mRecipientLimit = Integer.MAX_VALUE;
    private static int mSmsToMmsTextThreshold = 4;
    private static boolean mTransIdEnabled = false;
    private static String mUaProfTagName = DEFAULT_HTTP_KEY_X_WAP_PROFILE;
    private static String mUaProfUrl = null;
    private static String mUserAgent = DEFAULT_USER_AGENT;

    public static int getAliasMaxChars() {
        return mAliasRuleMaxChars;
    }

    public static int getAliasMinChars() {
        return mAliasRuleMinChars;
    }

    public static boolean getAllowAttachAudio() {
        return mAllowAttachAudio;
    }

    public static int getDefaultMMSMessagesPerThread() {
        return mDefaultMMSMessagesPerThread;
    }

    public static int getDefaultSMSMessagesPerThread() {
        return mDefaultSMSMessagesPerThread;
    }

    public static String getEmailGateway() {
        return mEmailGateway;
    }

    public static String getHttpParams() {
        return mHttpParams;
    }

    public static String getHttpParamsLine1Key() {
        return mHttpParamsLine1Key;
    }

    public static int getHttpSocketTimeout() {
        return mHttpSocketTimeout;
    }

    public static int getMaxImageHeight() {
        return mMaxImageHeight;
    }

    public static int getMaxImageWidth() {
        return mMaxImageWidth;
    }

    public static int getMaxMessageCountPerThread() {
        return mMaxMessageCountPerThread;
    }

    public static int getMaxMessageSize() {
        return mMaxMessageSize;
    }

    public static int getMaxSizeScaleForPendingMmsAllowed() {
        return mMaxSizeScaleForPendingMmsAllowed;
    }

    public static int getMinMessageCountPerThread() {
        return mMinMessageCountPerThread;
    }

    public static int getMinimumSlideElementDuration() {
        return mMinimumSlideElementDuration;
    }

    public static boolean getMmsEnabled() {
        return mMmsEnabled == 1;
    }

    public static boolean getNotifyWapMMSC() {
        return mNotifyWapMMSC;
    }

    public static int getRecipientLimit() {
        return mRecipientLimit;
    }

    public static int getSmsToMmsTextThreshold() {
        return mSmsToMmsTextThreshold;
    }

    public static boolean getTransIdEnabled() {
        return mTransIdEnabled;
    }

    public static String getUaProfTagName() {
        return mUaProfTagName;
    }

    public static String getUaProfUrl() {
        return mUaProfUrl;
    }

    public static String getUserAgent() {
        return mUserAgent;
    }

    public static void init(Context context) {
        loadMmsSettings(context);
    }

    public static boolean isAliasEnabled() {
        return mAliasEnabled;
    }

    private static void loadMmsSettings(Context context) {
        XmlResourceParser xml = context.getResources().getXml(R.xml.mms_config);
        try {
            XmlUtils.beginDocument(xml, "mms_config");
            while (true) {
                XmlUtils.nextElement(xml);
                String name = xml.getName();
                if (name == null) {
                    break;
                }
                String attributeName = xml.getAttributeName(0);
                String attributeValue = xml.getAttributeValue(0);
                String str = null;
                if (xml.next() == 4) {
                    str = xml.getText();
                }
                if ("name".equalsIgnoreCase(attributeName)) {
                    if ("bool".equals(name)) {
                        if ("enabledMMS".equalsIgnoreCase(attributeValue)) {
                            mMmsEnabled = "true".equalsIgnoreCase(str) ? 1 : 0;
                        } else if ("enabledTransID".equalsIgnoreCase(attributeValue)) {
                            mTransIdEnabled = "true".equalsIgnoreCase(str);
                        } else if ("enabledNotifyWapMMSC".equalsIgnoreCase(attributeValue)) {
                            mNotifyWapMMSC = "true".equalsIgnoreCase(str);
                        } else if ("aliasEnabled".equalsIgnoreCase(attributeValue)) {
                            mAliasEnabled = "true".equalsIgnoreCase(str);
                        } else if ("allowAttachAudio".equalsIgnoreCase(attributeValue)) {
                            mAllowAttachAudio = "true".equalsIgnoreCase(str);
                        }
                    } else if ("int".equals(name)) {
                        if ("maxMessageSize".equalsIgnoreCase(attributeValue)) {
                            mMaxMessageSize = Integer.parseInt(str);
                            if (Log.isLoggable(LogTag.APP, 2)) {
                                Log.d(LogTag.APP, "MmsConfig: MAX MESSAGE SIZE is " + mMaxMessageSize);
                            }
                        } else if ("maxImageHeight".equalsIgnoreCase(attributeValue)) {
                            mMaxImageHeight = Integer.parseInt(str);
                        } else if ("maxImageWidth".equalsIgnoreCase(attributeValue)) {
                            mMaxImageWidth = Integer.parseInt(str);
                        } else if ("defaultSMSMessagesPerThread".equalsIgnoreCase(attributeValue)) {
                            mDefaultSMSMessagesPerThread = Integer.parseInt(str);
                        } else if ("defaultMMSMessagesPerThread".equalsIgnoreCase(attributeValue)) {
                            mDefaultMMSMessagesPerThread = Integer.parseInt(str);
                        } else if ("minMessageCountPerThread".equalsIgnoreCase(attributeValue)) {
                            mMinMessageCountPerThread = Integer.parseInt(str);
                        } else if ("maxMessageCountPerThread".equalsIgnoreCase(attributeValue)) {
                            mMaxMessageCountPerThread = Integer.parseInt(str);
                        } else if ("smsToMmsTextThreshold".equalsIgnoreCase(attributeValue)) {
                            mSmsToMmsTextThreshold = Integer.parseInt(str);
                        } else if ("recipientLimit".equalsIgnoreCase(attributeValue)) {
                            mRecipientLimit = Integer.parseInt(str);
                            if (mRecipientLimit < 0) {
                                mRecipientLimit = Integer.MAX_VALUE;
                            }
                        } else if ("httpSocketTimeout".equalsIgnoreCase(attributeValue)) {
                            mHttpSocketTimeout = Integer.parseInt(str);
                        } else if ("minimumSlideElementDuration".equalsIgnoreCase(attributeValue)) {
                            mMinimumSlideElementDuration = Integer.parseInt(str);
                        } else if ("maxSizeScaleForPendingMmsAllowed".equalsIgnoreCase(attributeValue)) {
                            mMaxSizeScaleForPendingMmsAllowed = Integer.parseInt(str);
                        } else if ("aliasMinChars".equalsIgnoreCase(attributeValue)) {
                            mAliasRuleMinChars = Integer.parseInt(str);
                        } else if ("aliasMaxChars".equalsIgnoreCase(attributeValue)) {
                            mAliasRuleMaxChars = Integer.parseInt(str);
                        }
                    } else if ("string".equals(name)) {
                        if ("userAgent".equalsIgnoreCase(attributeValue)) {
                            mUserAgent = str;
                        } else if ("uaProfTagName".equalsIgnoreCase(attributeValue)) {
                            mUaProfTagName = str;
                        } else if ("uaProfUrl".equalsIgnoreCase(attributeValue)) {
                            mUaProfUrl = str;
                        } else if ("httpParams".equalsIgnoreCase(attributeValue)) {
                            mHttpParams = str;
                        } else if ("httpParamsLine1Key".equalsIgnoreCase(attributeValue)) {
                            mHttpParamsLine1Key = str;
                        } else if ("emailGatewayNumber".equalsIgnoreCase(attributeValue)) {
                            mEmailGateway = str;
                        }
                    }
                }
            }
        } catch (XmlPullParserException e) {
            Log.e(TAG, "loadMmsSettings caught ", e);
        } catch (NumberFormatException e2) {
            Log.e(TAG, "loadMmsSettings caught ", e2);
        } catch (IOException e3) {
            Log.e(TAG, "loadMmsSettings caught ", e3);
        } finally {
            xml.close();
        }
        String str2 = null;
        if (getMmsEnabled() && mUaProfUrl == null) {
            str2 = "uaProfUrl";
        }
        if (str2 != null) {
            String format = String.format("MmsConfig.loadMmsSettings mms_config.xml missing %s setting", str2);
            Log.e(TAG, format);
            throw new ContentRestrictionException(format);
        }
    }
}
