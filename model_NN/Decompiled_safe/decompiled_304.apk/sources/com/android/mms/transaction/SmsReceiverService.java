package com.android.mms.transaction;

import android.app.Service;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;
import com.android.internal.telephony.TelephonyIntents;
import com.android.mms.ui.ClassZeroActivity;
import com.android.provider.Telephony;
import com.flurry.android.FlurryAgent;
import com.google.android.mms.MmsException;
import org.apache.http.NameValuePair;
import vc.lx.sms.cmcc.MMHttpDefines;
import vc.lx.sms.data.Contact;
import vc.lx.sms.data.LogTag;
import vc.lx.sms.util.Recycler;
import vc.lx.sms.util.SqliteWrapper;
import vc.lx.sms.util.Util;
import vc.lx.sms2.R;

public class SmsReceiverService extends Service {
    public static final String CLASS_ZERO_BODY_KEY = "CLASS_ZERO_BODY";
    public static final String MESSAGE_SENT_ACTION = "com.android.mms.transaction.MESSAGE_SENT";
    private static final int REPLACE_COLUMN_ID = 0;
    private static final String[] REPLACE_PROJECTION = {"_id", "address", Telephony.TextBasedSmsColumns.PROTOCOL};
    private static final int SEND_COLUMN_ADDRESS = 2;
    private static final int SEND_COLUMN_BODY = 3;
    private static final int SEND_COLUMN_ID = 0;
    private static final int SEND_COLUMN_THREAD_ID = 1;
    private static final String[] SEND_PROJECTION = {"_id", "thread_id", "address", Telephony.TextBasedSmsColumns.BODY};
    private static final String TAG = "SmsReceiverService";
    private int mResultCode;
    private ServiceHandler mServiceHandler;
    private Looper mServiceLooper;
    public Handler mToastHandler = new Handler() {
        public void handleMessage(Message message) {
            Toast.makeText(SmsReceiverService.this, SmsReceiverService.this.getString(R.string.message_queued), 0).show();
        }
    };

    private final class ServiceHandler extends Handler {
        public ServiceHandler(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message message) {
            int i = message.arg1;
            Intent intent = (Intent) message.obj;
            if (intent != null) {
                String action = intent.getAction();
                if (SmsReceiverService.MESSAGE_SENT_ACTION.equals(intent.getAction())) {
                    SmsReceiverService.this.handleSmsSent(intent);
                } else if (Telephony.Sms.Intents.SMS_RECEIVED_ACTION.equals(action)) {
                    SmsReceiverService.this.handleSmsReceived(intent);
                } else if ("android.intent.action.BOOT_COMPLETED".equals(action)) {
                    SmsReceiverService.this.handleBootCompleted();
                } else if (TelephonyIntents.ACTION_SERVICE_STATE_CHANGED.equals(action)) {
                    SmsReceiverService.this.handleServiceStateChanged(intent);
                }
            }
            SmsReceiver.finishStartingService(SmsReceiverService.this, i);
        }
    }

    private void displayClassZeroMessage(Context context, SmsMessage smsMessage) {
        context.startActivity(new Intent(context, ClassZeroActivity.class).putExtra("pdu", smsMessage.getPdu()).setFlags(402653184));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    private ContentValues extractContentValues(SmsMessage smsMessage) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("address", smsMessage.getDisplayOriginatingAddress());
        contentValues.put("date", new Long(System.currentTimeMillis()));
        contentValues.put(Telephony.TextBasedSmsColumns.PROTOCOL, Integer.valueOf(smsMessage.getProtocolIdentifier()));
        contentValues.put("read", (Integer) 0);
        if (smsMessage.getPseudoSubject().length() > 0) {
            contentValues.put("subject", smsMessage.getPseudoSubject());
        }
        contentValues.put(Telephony.TextBasedSmsColumns.REPLY_PATH_PRESENT, Integer.valueOf(smsMessage.isReplyPathPresent() ? 1 : 0));
        contentValues.put(Telephony.TextBasedSmsColumns.SERVICE_CENTER, smsMessage.getServiceCenterAddress());
        return contentValues;
    }

    /* access modifiers changed from: private */
    public void handleBootCompleted() {
        moveOutboxMessagesToQueuedBox();
        sendFirstQueuedMessage();
        MessagingNotification.updateNewMessageIndicator(this);
    }

    /* access modifiers changed from: private */
    public void handleServiceStateChanged(Intent intent) {
        if (intent.getExtras().getInt("state") == 0) {
            sendFirstQueuedMessage();
        }
    }

    /* access modifiers changed from: private */
    public void handleSmsReceived(Intent intent) {
    }

    /* access modifiers changed from: private */
    public void handleSmsSent(Intent intent) {
        Uri data = intent.getData();
        if (this.mResultCode == -1) {
            if (!moveMessageToFolder(this, data, 2)) {
                Log.e(TAG, "handleSmsSent: failed to move message " + data + " to sent folder");
            }
            sendFirstQueuedMessage();
            MessagingNotification.updateSendFailedNotification(this);
        } else if (this.mResultCode == 2 || this.mResultCode == 4) {
            if (Log.isLoggable(LogTag.TRANSACTION, 2)) {
                Log.v(TAG, "handleSmsSent: no service, queuing message w/ uri: " + data);
            }
            registerForServiceStateChanges();
            moveMessageToFolder(this, data, 6);
            this.mToastHandler.sendEmptyMessage(1);
        } else {
            if (Log.isLoggable(LogTag.TRANSACTION, 2)) {
                Log.v(TAG, "handleSmsSent msg failed uri: " + data);
            }
            moveMessageToFolder(this, data, 5);
            MessagingNotification.notifySendFailed(getApplicationContext(), true);
            sendFirstQueuedMessage();
        }
    }

    private Uri insertMessage(Context context, SmsMessage[] smsMessageArr) {
        SmsMessage smsMessage = smsMessageArr[0];
        if (smsMessage.getMessageClass() != SmsMessage.MessageClass.CLASS_0) {
            return smsMessage.isReplace() ? replaceMessage(context, smsMessageArr) : storeMessage(context, smsMessageArr);
        }
        displayClassZeroMessage(context, smsMessage);
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public static boolean moveMessageToFolder(Context context, Uri uri, int i) {
        boolean z;
        boolean z2;
        if (uri == null) {
            return false;
        }
        switch (i) {
            case 1:
            case 3:
                z = false;
                z2 = false;
                break;
            case 2:
            case 4:
                z = true;
                z2 = false;
                break;
            case 5:
                z = true;
                z2 = false;
                break;
            case 6:
                z = false;
                z2 = true;
                break;
            default:
                return false;
        }
        ContentValues contentValues = new ContentValues(2);
        contentValues.put("type", Integer.valueOf(i));
        if (z2) {
            contentValues.put("read", (Integer) 0);
        } else if (z) {
            contentValues.put("read", (Integer) 1);
        }
        return 1 == SqliteWrapper.update(context, context.getContentResolver(), uri, contentValues, null, null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    private void moveOutboxMessagesToQueuedBox() {
        ContentValues contentValues = new ContentValues(1);
        contentValues.put("type", (Integer) 6);
        SqliteWrapper.update(getApplicationContext(), getContentResolver(), Telephony.Sms.Outbox.CONTENT_URI, contentValues, "type = 4", null);
    }

    private void registerForServiceStateChanges() {
        Context applicationContext = getApplicationContext();
        unRegisterForServiceStateChanges();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(TelephonyIntents.ACTION_SERVICE_STATE_CHANGED);
        if (Log.isLoggable(LogTag.TRANSACTION, 2)) {
            Log.v(TAG, "registerForServiceStateChanges");
        }
        applicationContext.registerReceiver(SmsReceiver.getInstance(), intentFilter);
    }

    /* JADX INFO: finally extract failed */
    private Uri replaceMessage(Context context, SmsMessage[] smsMessageArr) {
        SmsMessage smsMessage = smsMessageArr[0];
        ContentValues extractContentValues = extractContentValues(smsMessage);
        extractContentValues.put(Telephony.TextBasedSmsColumns.BODY, smsMessage.getMessageBody());
        ContentResolver contentResolver = context.getContentResolver();
        String[] strArr = {smsMessage.getOriginatingAddress(), Integer.toString(smsMessage.getProtocolIdentifier())};
        Cursor query = SqliteWrapper.query(context, contentResolver, Telephony.Sms.Inbox.CONTENT_URI, REPLACE_PROJECTION, "address = ? AND protocol = ?", strArr, null);
        if (query != null) {
            try {
                if (query.moveToFirst()) {
                    Uri withAppendedId = ContentUris.withAppendedId(Telephony.Sms.CONTENT_URI, query.getLong(0));
                    SqliteWrapper.update(context, contentResolver, withAppendedId, extractContentValues, null, null);
                    query.close();
                    return withAppendedId;
                }
                query.close();
            } catch (Throwable th) {
                query.close();
                throw th;
            }
        }
        return storeMessage(context, smsMessageArr);
    }

    private Uri storeMessage(Context context, SmsMessage[] smsMessageArr) {
        SmsMessage smsMessage = smsMessageArr[0];
        ContentValues extractContentValues = extractContentValues(smsMessage);
        if (r2 == 1) {
            extractContentValues.put(Telephony.TextBasedSmsColumns.BODY, smsMessage.getDisplayMessageBody());
        } else {
            StringBuilder sb = new StringBuilder();
            for (SmsMessage displayMessageBody : smsMessageArr) {
                sb.append(displayMessageBody.getDisplayMessageBody());
            }
            extractContentValues.put(Telephony.TextBasedSmsColumns.BODY, sb.toString());
        }
        Long asLong = extractContentValues.getAsLong("thread_id");
        String asString = extractContentValues.getAsString("address");
        Contact contact = Contact.get(asString, true, context);
        if (contact != null) {
            asString = contact.getNumber();
        }
        if ((asLong == null || asLong.longValue() == 0) && asString != null) {
            extractContentValues.put("thread_id", Long.valueOf(Telephony.Threads.getOrCreateThreadId(context, asString)));
        }
        Uri insert = SqliteWrapper.insert(context, context.getContentResolver(), Telephony.Sms.Inbox.CONTENT_URI, extractContentValues);
        Recycler.getSmsRecycler().deleteOldMessagesByThreadId(getApplicationContext(), extractContentValues.getAsLong("thread_id").longValue());
        return insert;
    }

    private void unRegisterForServiceStateChanges() {
        if (Log.isLoggable(LogTag.TRANSACTION, 2)) {
            Log.v(TAG, "unRegisterForServiceStateChanges");
        }
        try {
            getApplicationContext().unregisterReceiver(SmsReceiver.getInstance());
        } catch (IllegalArgumentException e) {
        }
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        HandlerThread handlerThread = new HandlerThread(TAG, 10);
        handlerThread.start();
        this.mServiceLooper = handlerThread.getLooper();
        this.mServiceHandler = new ServiceHandler(this.mServiceLooper);
        FlurryAgent.onStartSession(this, "5CUJT1BY9LVA9WH16LDI");
    }

    public void onDestroy() {
        this.mServiceLooper.quit();
        FlurryAgent.onEndSession(this);
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        this.mResultCode = intent != null ? intent.getIntExtra(MMHttpDefines.TAG_RESULT, 0) : 0;
        Message obtainMessage = this.mServiceHandler.obtainMessage();
        obtainMessage.arg1 = i2;
        obtainMessage.obj = intent;
        this.mServiceHandler.sendMessage(obtainMessage);
        Util.logEvent("sms_received", new NameValuePair[0]);
        return 2;
    }

    public synchronized void sendFirstQueuedMessage() {
        boolean z;
        boolean z2;
        Uri withAppendedId;
        Uri parse = Uri.parse("content://sms/queued");
        ContentResolver contentResolver = getContentResolver();
        Cursor query = SqliteWrapper.query(this, contentResolver, parse, SEND_PROJECTION, null, null, "date ASC");
        if (query != null) {
            try {
                if (query.moveToFirst()) {
                    String string = query.getString(3);
                    String[] strArr = {query.getString(2)};
                    int i = query.getInt(1);
                    SmsMessageSender smsMessageSender = new SmsMessageSender(this, strArr, string, (long) i);
                    withAppendedId = ContentUris.withAppendedId(Telephony.Sms.CONTENT_URI, (long) query.getInt(0));
                    if (Log.isLoggable(LogTag.TRANSACTION, 2)) {
                        Log.v(TAG, "sendFirstQueuedMessage and delete old msgUri " + withAppendedId + ", address: " + strArr + ", threadId: " + i + ", body: " + string);
                    }
                    smsMessageSender.sendMessage(-1);
                    int delete = SqliteWrapper.delete(this, contentResolver, withAppendedId, null, null);
                    if (delete != 1) {
                        Log.e(TAG, "sendFirstQueuedMessage: failed to delete old msgUri " + withAppendedId + ", result=" + delete);
                    }
                    z2 = true;
                } else {
                    z2 = true;
                }
            } catch (MmsException e) {
                Log.e(TAG, "sendFirstQueuedMessage: failed to send message " + withAppendedId + ", caught ", e);
                int delete2 = SqliteWrapper.delete(this, contentResolver, withAppendedId, null, null);
                if (delete2 != 1) {
                    Log.e(TAG, "sendFirstQueuedMessage: failed to delete old msgUri " + withAppendedId + ", result=" + delete2);
                }
                z2 = false;
            } catch (Throwable th) {
                query.close();
                throw th;
            }
            query.close();
            z = z2;
        } else {
            z = true;
        }
        if (z) {
            unRegisterForServiceStateChanges();
        }
    }
}
