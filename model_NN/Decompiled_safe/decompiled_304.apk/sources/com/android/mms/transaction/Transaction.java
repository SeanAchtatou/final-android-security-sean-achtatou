package com.android.mms.transaction;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.util.Log;
import com.android.net.NetworkUtils;
import java.io.IOException;
import vc.lx.sms.data.LogTag;

public abstract class Transaction extends Observable {
    public static final int NOTIFICATION_TRANSACTION = 0;
    public static final int READREC_TRANSACTION = 3;
    public static final int RETRIEVE_TRANSACTION = 1;
    public static final int SEND_TRANSACTION = 2;
    protected Context mContext;
    protected String mId;
    private final int mServiceId;
    protected TransactionSettings mTransactionSettings;
    protected TransactionState mTransactionState = new TransactionState();

    public Transaction(Context context, int i, TransactionSettings transactionSettings) {
        this.mContext = context;
        this.mServiceId = i;
        this.mTransactionSettings = transactionSettings;
    }

    private void ensureRouteToHost(String str, TransactionSettings transactionSettings) throws IOException {
        ConnectivityManager connectivityManager = (ConnectivityManager) this.mContext.getSystemService("connectivity");
        if (transactionSettings.isProxySet()) {
            int lookupHost = NetworkUtils.lookupHost(transactionSettings.getProxyAddress());
            if (lookupHost == -1) {
                throw new IOException("Cannot establish route for " + str + ": Unknown host");
            } else if (!connectivityManager.requestRouteToHost(2, lookupHost)) {
                throw new IOException("Cannot establish route to proxy " + lookupHost);
            }
        } else {
            int lookupHost2 = NetworkUtils.lookupHost(Uri.parse(str).getHost());
            if (lookupHost2 == -1) {
                throw new IOException("Cannot establish route for " + str + ": Unknown host");
            } else if (!connectivityManager.requestRouteToHost(2, lookupHost2)) {
                throw new IOException("Cannot establish route to " + lookupHost2 + " for " + str);
            }
        }
    }

    public TransactionSettings getConnectionSettings() {
        return this.mTransactionSettings;
    }

    /* access modifiers changed from: protected */
    public byte[] getPdu(String str) throws IOException {
        ensureRouteToHost(str, this.mTransactionSettings);
        return HttpUtils.httpConnection(this.mContext, -1, str, null, 2, this.mTransactionSettings.isProxySet(), this.mTransactionSettings.getProxyAddress(), this.mTransactionSettings.getProxyPort());
    }

    public int getServiceId() {
        return this.mServiceId;
    }

    public TransactionState getState() {
        return this.mTransactionState;
    }

    public abstract int getType();

    public boolean isEquivalent(Transaction transaction) {
        return getClass().equals(transaction.getClass()) && this.mId.equals(transaction.mId);
    }

    public abstract void process();

    /* access modifiers changed from: protected */
    public byte[] sendPdu(long j, byte[] bArr) throws IOException {
        String mmscUrl = this.mTransactionSettings.getMmscUrl();
        if (Log.isLoggable(LogTag.TRANSACTION, 2)) {
            Log.d(LogTag.TRANSACTION, "Transaction.sendPdu: mmscUrl=" + mmscUrl + ", token=" + j + ", payload_size=" + (bArr != null ? bArr.length : 0));
        }
        return sendPdu(j, bArr, mmscUrl);
    }

    /* access modifiers changed from: protected */
    public byte[] sendPdu(long j, byte[] bArr, String str) throws IOException {
        ensureRouteToHost(str, this.mTransactionSettings);
        return HttpUtils.httpConnection(this.mContext, j, str, bArr, 1, this.mTransactionSettings.isProxySet(), this.mTransactionSettings.getProxyAddress(), this.mTransactionSettings.getProxyPort());
    }

    /* access modifiers changed from: protected */
    public byte[] sendPdu(byte[] bArr) throws IOException {
        return sendPdu(-1, bArr, this.mTransactionSettings.getMmscUrl());
    }

    /* access modifiers changed from: protected */
    public byte[] sendPdu(byte[] bArr, String str) throws IOException {
        return sendPdu(-1, bArr, str);
    }

    public void setConnectionSettings(TransactionSettings transactionSettings) {
        this.mTransactionSettings = transactionSettings;
    }

    public String toString() {
        return getClass().getName() + ": serviceId=" + this.mServiceId;
    }
}
