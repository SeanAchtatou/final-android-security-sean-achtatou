package com.android.mms.transaction;

public abstract class AbstractRetryScheme {
    public static final int INCOMING = 2;
    public static final int OUTGOING = 1;
    protected int mRetriedTimes;

    public AbstractRetryScheme(int i) {
        this.mRetriedTimes = i;
    }

    public abstract int getRetryLimit();

    public abstract long getWaitingInterval();
}
