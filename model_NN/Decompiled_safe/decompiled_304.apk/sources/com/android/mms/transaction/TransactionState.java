package com.android.mms.transaction;

import android.net.Uri;

public class TransactionState {
    public static final int FAILED = 2;
    public static final int INITIALIZED = 0;
    public static final int SUCCESS = 1;
    private Uri mContentUri = null;
    private int mState = 0;

    public synchronized Uri getContentUri() {
        return this.mContentUri;
    }

    public synchronized int getState() {
        return this.mState;
    }

    /* access modifiers changed from: package-private */
    public synchronized void setContentUri(Uri uri) {
        this.mContentUri = uri;
    }

    /* access modifiers changed from: package-private */
    public synchronized void setState(int i) {
        if (i >= 0 || i <= 2) {
            this.mState = i;
        } else {
            throw new IllegalArgumentException("Bad state: " + i);
        }
    }
}
