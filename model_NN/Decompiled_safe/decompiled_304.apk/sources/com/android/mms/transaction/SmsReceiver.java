package com.android.mms.transaction;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import com.android.provider.Telephony;
import vc.lx.sms.cmcc.MMHttpDefines;

public class SmsReceiver extends BroadcastReceiver {
    static PowerManager.WakeLock mStartingService;
    static final Object mStartingServiceSync = new Object();
    private static SmsReceiver sInstance;

    public static void beginStartingService(Context context, Intent intent) {
        synchronized (mStartingServiceSync) {
            if (mStartingService == null) {
                mStartingService = ((PowerManager) context.getSystemService("power")).newWakeLock(1, "StartingAlertService");
                mStartingService.setReferenceCounted(false);
            }
            mStartingService.acquire();
            context.startService(intent);
        }
    }

    public static void finishStartingService(Service service, int i) {
        synchronized (mStartingServiceSync) {
            if (mStartingService != null && service.stopSelfResult(i)) {
                mStartingService.release();
            }
        }
    }

    public static SmsReceiver getInstance() {
        if (sInstance == null) {
            sInstance = new SmsReceiver();
        }
        return sInstance;
    }

    public void onReceive(Context context, Intent intent) {
        onReceiveWithPrivilege(context, intent, false);
    }

    /* access modifiers changed from: protected */
    public void onReceiveWithPrivilege(Context context, Intent intent, boolean z) {
        if (z || !intent.getAction().equals(Telephony.Sms.Intents.SMS_RECEIVED_ACTION)) {
            intent.setClass(context, SmsReceiverService.class);
            intent.putExtra(MMHttpDefines.TAG_RESULT, getResultCode());
            beginStartingService(context, intent);
            try {
                abortBroadcast();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
