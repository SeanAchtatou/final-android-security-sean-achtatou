package com.android.mms.transaction;

import android.content.Context;
import android.net.Uri;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.android.mms.MmsConfig;
import com.android.provider.Telephony;
import com.google.android.mms.MmsException;
import com.google.android.mms.pdu.NotificationInd;
import com.google.android.mms.pdu.NotifyRespInd;
import com.google.android.mms.pdu.PduComposer;
import com.google.android.mms.pdu.PduPersister;
import java.io.IOException;

public class NotificationTransaction extends Transaction implements Runnable {
    private static final boolean DEBUG = false;
    private static final boolean LOCAL_LOGV = false;
    private static final String TAG = "NotificationTransaction";
    private String mContentLocation;
    private NotificationInd mNotificationInd;
    private TelephonyManager mTelephonyManager;
    private Uri mUri;

    public NotificationTransaction(Context context, int i, TransactionSettings transactionSettings, NotificationInd notificationInd) {
        super(context, i, transactionSettings);
        try {
            this.mUri = PduPersister.getPduPersister(context).persist(notificationInd, Telephony.Mms.Inbox.CONTENT_URI);
            this.mNotificationInd = notificationInd;
            this.mId = new String(notificationInd.getTransactionId());
        } catch (MmsException e) {
            Log.e(TAG, "Failed to save NotificationInd in constructor.", e);
            throw new IllegalArgumentException();
        }
    }

    public NotificationTransaction(Context context, int i, TransactionSettings transactionSettings, String str) {
        super(context, i, transactionSettings);
        this.mUri = Uri.parse(str);
        this.mTelephonyManager = (TelephonyManager) context.getSystemService("phone");
        try {
            this.mNotificationInd = (NotificationInd) PduPersister.getPduPersister(context).load(this.mUri);
            this.mId = new String(this.mNotificationInd.getTransactionId());
            this.mContentLocation = new String(this.mNotificationInd.getContentLocation());
            attach(RetryScheduler.getInstance(context));
        } catch (MmsException e) {
            Log.e(TAG, "Failed to load NotificationInd from: " + str, e);
            throw new IllegalArgumentException();
        }
    }

    private void sendNotifyRespInd(int i) throws MmsException, IOException {
        NotifyRespInd notifyRespInd = new NotifyRespInd(18, this.mNotificationInd.getTransactionId(), i);
        if (MmsConfig.getNotifyWapMMSC()) {
            sendPdu(new PduComposer(this.mContext, notifyRespInd).make(), this.mContentLocation);
        } else {
            sendPdu(new PduComposer(this.mContext, notifyRespInd).make());
        }
    }

    public int getType() {
        return 0;
    }

    public void process() {
        new Thread(this).start();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:40:?, code lost:
        r12.mTransactionState.setState(2);
        r0 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00ed, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:?, code lost:
        android.util.Log.e(com.android.mms.transaction.NotificationTransaction.TAG, android.util.Log.getStackTraceString(r0));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x0130, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x0131, code lost:
        r12.mTransactionState.setContentUri(r12.mUri);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x013c, code lost:
        r12.mTransactionState.setState(1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x0147, code lost:
        if (r12.mTransactionState.getState() != 1) goto L_0x0149;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0149, code lost:
        r12.mTransactionState.setState(2);
        android.util.Log.e(com.android.mms.transaction.NotificationTransaction.TAG, "NotificationTransaction failed.");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x0155, code lost:
        notifyObservers();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x0158, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:?, code lost:
        return;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [B:6:0x001f, B:19:0x005a] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r12 = this;
            r6 = 0
            r9 = 2
            r8 = 1
            java.lang.String r11 = "NotificationTransaction failed."
            java.lang.String r10 = "NotificationTransaction"
            com.android.mms.util.DownloadManager r0 = com.android.mms.util.DownloadManager.getInstance()
            boolean r1 = r0.isAuto()
            android.telephony.TelephonyManager r2 = r12.mTelephonyManager
            int r2 = r2.getDataState()
            r3 = 3
            if (r2 != r3) goto L_0x0051
            r2 = r8
        L_0x0019:
            r3 = 131(0x83, float:1.84E-43)
            if (r1 == 0) goto L_0x001f
            if (r2 == 0) goto L_0x0053
        L_0x001f:
            android.net.Uri r4 = r12.mUri     // Catch:{ Throwable -> 0x00ed }
            r5 = 128(0x80, float:1.794E-43)
            r0.markState(r4, r5)     // Catch:{ Throwable -> 0x00ed }
            r12.sendNotifyRespInd(r3)     // Catch:{ Throwable -> 0x00ed }
            com.android.mms.transaction.TransactionState r0 = r12.mTransactionState
            android.net.Uri r3 = r12.mUri
            r0.setContentUri(r3)
            if (r1 == 0) goto L_0x0034
            if (r2 == 0) goto L_0x0039
        L_0x0034:
            com.android.mms.transaction.TransactionState r0 = r12.mTransactionState
            r0.setState(r8)
        L_0x0039:
            com.android.mms.transaction.TransactionState r0 = r12.mTransactionState
            int r0 = r0.getState()
            if (r0 == r8) goto L_0x004d
            com.android.mms.transaction.TransactionState r0 = r12.mTransactionState
            r0.setState(r9)
            java.lang.String r0 = "NotificationTransaction"
            java.lang.String r0 = "NotificationTransaction failed."
            android.util.Log.e(r10, r11)
        L_0x004d:
            r12.notifyObservers()
        L_0x0050:
            return
        L_0x0051:
            r2 = 0
            goto L_0x0019
        L_0x0053:
            android.net.Uri r4 = r12.mUri     // Catch:{ Throwable -> 0x00ed }
            r5 = 129(0x81, float:1.81E-43)
            r0.markState(r4, r5)     // Catch:{ Throwable -> 0x00ed }
            java.lang.String r0 = r12.mContentLocation     // Catch:{ IOException -> 0x00bd }
            byte[] r0 = r12.getPdu(r0)     // Catch:{ IOException -> 0x00bd }
        L_0x0060:
            if (r0 == 0) goto L_0x0159
            com.google.android.mms.pdu.PduParser r3 = new com.google.android.mms.pdu.PduParser     // Catch:{ Throwable -> 0x00ed }
            r3.<init>(r0)     // Catch:{ Throwable -> 0x00ed }
            com.google.android.mms.pdu.GenericPdu r0 = r3.parse()     // Catch:{ Throwable -> 0x00ed }
            if (r0 == 0) goto L_0x0075
            int r3 = r0.getMessageType()     // Catch:{ Throwable -> 0x00ed }
            r4 = 132(0x84, float:1.85E-43)
            if (r3 == r4) goto L_0x00c6
        L_0x0075:
            java.lang.String r0 = "NotificationTransaction"
            java.lang.String r3 = "Invalid M-RETRIEVE.CONF PDU."
            android.util.Log.e(r0, r3)     // Catch:{ Throwable -> 0x00ed }
            com.android.mms.transaction.TransactionState r0 = r12.mTransactionState     // Catch:{ Throwable -> 0x00ed }
            r3 = 2
            r0.setState(r3)     // Catch:{ Throwable -> 0x00ed }
            r0 = 132(0x84, float:1.85E-43)
        L_0x0084:
            switch(r0) {
                case 129: goto L_0x00e6;
                case 130: goto L_0x0087;
                case 131: goto L_0x0120;
                default: goto L_0x0087;
            }     // Catch:{ Throwable -> 0x00ed }
        L_0x0087:
            r12.sendNotifyRespInd(r0)     // Catch:{ Throwable -> 0x00ed }
            vc.lx.sms.util.Recycler$MmsRecycler r0 = vc.lx.sms.util.Recycler.getMmsRecycler()     // Catch:{ Throwable -> 0x00ed }
            android.content.Context r3 = r12.mContext     // Catch:{ Throwable -> 0x00ed }
            android.net.Uri r4 = r12.mUri     // Catch:{ Throwable -> 0x00ed }
            r0.deleteOldMessagesInSameThreadAsMessage(r3, r4)     // Catch:{ Throwable -> 0x00ed }
            com.android.mms.transaction.TransactionState r0 = r12.mTransactionState
            android.net.Uri r3 = r12.mUri
            r0.setContentUri(r3)
            if (r1 == 0) goto L_0x00a0
            if (r2 == 0) goto L_0x00a5
        L_0x00a0:
            com.android.mms.transaction.TransactionState r0 = r12.mTransactionState
            r0.setState(r8)
        L_0x00a5:
            com.android.mms.transaction.TransactionState r0 = r12.mTransactionState
            int r0 = r0.getState()
            if (r0 == r8) goto L_0x00b9
            com.android.mms.transaction.TransactionState r0 = r12.mTransactionState
            r0.setState(r9)
            java.lang.String r0 = "NotificationTransaction"
            java.lang.String r0 = "NotificationTransaction failed."
            android.util.Log.e(r10, r11)
        L_0x00b9:
            r12.notifyObservers()
            goto L_0x0050
        L_0x00bd:
            r0 = move-exception
            com.android.mms.transaction.TransactionState r0 = r12.mTransactionState     // Catch:{ Throwable -> 0x00ed }
            r4 = 2
            r0.setState(r4)     // Catch:{ Throwable -> 0x00ed }
            r0 = r6
            goto L_0x0060
        L_0x00c6:
            android.content.Context r3 = r12.mContext     // Catch:{ Throwable -> 0x00ed }
            com.google.android.mms.pdu.PduPersister r3 = com.google.android.mms.pdu.PduPersister.getPduPersister(r3)     // Catch:{ Throwable -> 0x00ed }
            android.net.Uri r4 = com.android.provider.Telephony.Mms.Inbox.CONTENT_URI     // Catch:{ Throwable -> 0x00ed }
            android.net.Uri r0 = r3.persist(r0, r4)     // Catch:{ Throwable -> 0x00ed }
            android.content.Context r3 = r12.mContext     // Catch:{ Throwable -> 0x00ed }
            android.content.Context r4 = r12.mContext     // Catch:{ Throwable -> 0x00ed }
            android.content.ContentResolver r4 = r4.getContentResolver()     // Catch:{ Throwable -> 0x00ed }
            android.net.Uri r5 = r12.mUri     // Catch:{ Throwable -> 0x00ed }
            r6 = 0
            r7 = 0
            vc.lx.sms.util.SqliteWrapper.delete(r3, r4, r5, r6, r7)     // Catch:{ Throwable -> 0x00ed }
            r12.mUri = r0     // Catch:{ Throwable -> 0x00ed }
            r0 = 129(0x81, float:1.81E-43)
            goto L_0x0084
        L_0x00e6:
            com.android.mms.transaction.TransactionState r3 = r12.mTransactionState     // Catch:{ Throwable -> 0x00ed }
            r4 = 1
            r3.setState(r4)     // Catch:{ Throwable -> 0x00ed }
            goto L_0x0087
        L_0x00ed:
            r0 = move-exception
            java.lang.String r3 = "NotificationTransaction"
            java.lang.String r0 = android.util.Log.getStackTraceString(r0)     // Catch:{ all -> 0x0130 }
            android.util.Log.e(r3, r0)     // Catch:{ all -> 0x0130 }
            com.android.mms.transaction.TransactionState r0 = r12.mTransactionState
            android.net.Uri r3 = r12.mUri
            r0.setContentUri(r3)
            if (r1 == 0) goto L_0x0102
            if (r2 == 0) goto L_0x0107
        L_0x0102:
            com.android.mms.transaction.TransactionState r0 = r12.mTransactionState
            r0.setState(r8)
        L_0x0107:
            com.android.mms.transaction.TransactionState r0 = r12.mTransactionState
            int r0 = r0.getState()
            if (r0 == r8) goto L_0x011b
            com.android.mms.transaction.TransactionState r0 = r12.mTransactionState
            r0.setState(r9)
            java.lang.String r0 = "NotificationTransaction"
            java.lang.String r0 = "NotificationTransaction failed."
            android.util.Log.e(r10, r11)
        L_0x011b:
            r12.notifyObservers()
            goto L_0x0050
        L_0x0120:
            com.android.mms.transaction.TransactionState r3 = r12.mTransactionState     // Catch:{ Throwable -> 0x00ed }
            int r3 = r3.getState()     // Catch:{ Throwable -> 0x00ed }
            if (r3 != 0) goto L_0x0087
            com.android.mms.transaction.TransactionState r3 = r12.mTransactionState     // Catch:{ Throwable -> 0x00ed }
            r4 = 1
            r3.setState(r4)     // Catch:{ Throwable -> 0x00ed }
            goto L_0x0087
        L_0x0130:
            r0 = move-exception
            com.android.mms.transaction.TransactionState r3 = r12.mTransactionState
            android.net.Uri r4 = r12.mUri
            r3.setContentUri(r4)
            if (r1 == 0) goto L_0x013c
            if (r2 == 0) goto L_0x0141
        L_0x013c:
            com.android.mms.transaction.TransactionState r1 = r12.mTransactionState
            r1.setState(r8)
        L_0x0141:
            com.android.mms.transaction.TransactionState r1 = r12.mTransactionState
            int r1 = r1.getState()
            if (r1 == r8) goto L_0x0155
            com.android.mms.transaction.TransactionState r1 = r12.mTransactionState
            r1.setState(r9)
            java.lang.String r1 = "NotificationTransaction"
            java.lang.String r1 = "NotificationTransaction failed."
            android.util.Log.e(r10, r11)
        L_0x0155:
            r12.notifyObservers()
            throw r0
        L_0x0159:
            r0 = r3
            goto L_0x0084
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.mms.transaction.NotificationTransaction.run():void");
    }
}
