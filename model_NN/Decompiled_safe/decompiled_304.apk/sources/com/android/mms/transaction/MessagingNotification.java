package com.android.mms.transaction;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.StyleSpan;
import com.android.mms.util.AddressUtils;
import com.android.provider.Telephony;
import com.google.android.mms.pdu.EncodedStringValue;
import com.google.android.mms.pdu.PduPersister;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import vc.lx.sms.data.Contact;
import vc.lx.sms.ui.ComposeMessageActivity;
import vc.lx.sms.ui.ConversationListActivity;
import vc.lx.sms.ui.MessagingPreferenceActivity;
import vc.lx.sms.util.SqliteWrapper;
import vc.lx.sms2.R;

public class MessagingNotification {
    private static final int COLUMN_DATE = 1;
    private static final int COLUMN_MMS_ID = 2;
    private static final int COLUMN_SMS_ADDRESS = 2;
    private static final int COLUMN_SMS_BODY = 4;
    private static final int COLUMN_SUBJECT = 3;
    private static final int COLUMN_SUBJECT_CS = 4;
    private static final int COLUMN_THREAD_ID = 0;
    public static final int DOWNLOAD_FAILED_NOTIFICATION_ID = 531;
    private static final MmsSmsNotificationInfoComparator INFO_COMPARATOR = new MmsSmsNotificationInfoComparator();
    public static final int MESSAGE_FAILED_NOTIFICATION_ID = 789;
    private static final String[] MMS_STATUS_PROJECTION = {"thread_id", "date", "_id", Telephony.BaseMmsColumns.SUBJECT, Telephony.BaseMmsColumns.SUBJECT_CHARSET};
    private static final String NEW_INCOMING_MM_CONSTRAINT = "(msg_box=1 AND read=0 AND (m_type=130 OR m_type=132))";
    private static final String NEW_INCOMING_SM_CONSTRAINT = "(type = 1 AND read = 0)";
    public static final int NOTIFICATION_ID = 321;
    private static final int OLD_NOTIFICATION_ID = 123;
    private static final String[] SMS_STATUS_PROJECTION = {"thread_id", "date", "address", "subject", Telephony.TextBasedSmsColumns.BODY};
    private static final String TAG = "Mms:app";
    private static final Uri UNDELIVERED_URI = Uri.parse("content://mms-sms/undelivered");

    private static final class MmsSmsNotificationInfo {
        public Intent mClickIntent;
        public int mCount;
        public String mDescription;
        public int mIconResourceId;
        public CharSequence mTicker;
        public long mTimeMillis;
        public String mTitle;

        public MmsSmsNotificationInfo(Intent intent, String str, int i, CharSequence charSequence, long j, String str2, int i2) {
            this.mClickIntent = intent;
            this.mDescription = str;
            this.mIconResourceId = i;
            this.mTicker = charSequence;
            this.mTimeMillis = j;
            this.mTitle = str2;
            this.mCount = i2;
        }

        public void deliver(Context context, boolean z, int i, int i2) {
            MessagingNotification.updateNotification(context, this.mClickIntent, this.mDescription, this.mIconResourceId, z, z ? this.mTicker : null, this.mTimeMillis, this.mTitle, i, i2);
        }

        public long getTime() {
            return this.mTimeMillis;
        }
    }

    private static final class MmsSmsNotificationInfoComparator implements Comparator<MmsSmsNotificationInfo> {
        private MmsSmsNotificationInfoComparator() {
        }

        public int compare(MmsSmsNotificationInfo mmsSmsNotificationInfo, MmsSmsNotificationInfo mmsSmsNotificationInfo2) {
            return Long.signum(mmsSmsNotificationInfo2.getTime() - mmsSmsNotificationInfo.getTime());
        }
    }

    private MessagingNotification() {
    }

    private static final int accumulateNotificationInfo(SortedSet sortedSet, MmsSmsNotificationInfo mmsSmsNotificationInfo) {
        if (mmsSmsNotificationInfo == null) {
            return 0;
        }
        sortedSet.add(mmsSmsNotificationInfo);
        return mmsSmsNotificationInfo.mCount;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    protected static CharSequence buildTickerMessage(Context context, String str, String str2, String str3) {
        String name = Contact.get(str, true, context).getName();
        StringBuilder sb = new StringBuilder(name == null ? "" : name.replace(10, ' ').replace(13, ' '));
        sb.append(':').append(' ');
        int length = sb.length();
        if (!TextUtils.isEmpty(str2)) {
            sb.append(str2.replace(10, ' ').replace(13, ' '));
            sb.append(' ');
        }
        if (!TextUtils.isEmpty(str3)) {
            sb.append(str3.replace(10, ' ').replace(13, ' '));
        }
        SpannableString spannableString = new SpannableString(sb.toString());
        spannableString.setSpan(new StyleSpan(1), 0, length, 33);
        return spannableString;
    }

    public static void cancelNotification(Context context, int i) {
        ((NotificationManager) context.getSystemService("notification")).cancel(i);
    }

    private static int getDownloadFailedMessageCount(Context context) {
        Cursor query = SqliteWrapper.query(context, context.getContentResolver(), Telephony.Mms.Inbox.CONTENT_URI, null, "m_type=" + String.valueOf(130) + " AND " + Telephony.BaseMmsColumns.STATUS + "=" + String.valueOf(135), null, null);
        if (query == null) {
            return 0;
        }
        int count = query.getCount();
        query.close();
        return count;
    }

    /* JADX INFO: finally extract failed */
    public static final MmsSmsNotificationInfo getMmsNewMessageNotificationInfo(Context context, Set<Long> set) {
        Cursor query = SqliteWrapper.query(context, context.getContentResolver(), Telephony.Mms.CONTENT_URI, MMS_STATUS_PROJECTION, NEW_INCOMING_MM_CONSTRAINT, null, "date desc");
        if (query == null) {
            return null;
        }
        try {
            if (!query.moveToFirst()) {
                query.close();
                return null;
            }
            String from = AddressUtils.getFrom(context, Telephony.Mms.CONTENT_URI.buildUpon().appendPath(Long.toString(query.getLong(2))).build());
            String mmsSubject = getMmsSubject(query.getString(3), query.getInt(4));
            long j = query.getLong(0);
            MmsSmsNotificationInfo newMessageNotificationInfo = getNewMessageNotificationInfo(from, mmsSubject, context, R.drawable.notification_icon, null, j, 1000 * query.getLong(1), query.getCount());
            set.add(Long.valueOf(j));
            while (query.moveToNext()) {
                set.add(Long.valueOf(query.getLong(0)));
            }
            query.close();
            return newMessageNotificationInfo;
        } catch (Throwable th) {
            query.close();
            throw th;
        }
    }

    private static String getMmsSubject(String str, int i) {
        return TextUtils.isEmpty(str) ? "" : new EncodedStringValue(i, PduPersister.getBytes(str)).getString();
    }

    private static final MmsSmsNotificationInfo getNewMessageNotificationInfo(String str, String str2, Context context, int i, String str3, long j, long j2, int i2) {
        Intent createIntentByThreadId = ComposeMessageActivity.createIntentByThreadId(context, j);
        createIntentByThreadId.setFlags(872415232);
        String obj = buildTickerMessage(context, str, null, null).toString();
        return new MmsSmsNotificationInfo(createIntentByThreadId, str2, i, buildTickerMessage(context, str, str3, str2), j2, obj.substring(0, obj.length() - 2), i2);
    }

    /* JADX INFO: finally extract failed */
    public static final MmsSmsNotificationInfo getSmsNewMessageNotificationInfo(Context context, Set<Long> set) {
        Cursor query = SqliteWrapper.query(context, context.getContentResolver(), Telephony.Sms.CONTENT_URI, SMS_STATUS_PROJECTION, NEW_INCOMING_SM_CONSTRAINT, null, "date desc");
        if (query == null) {
            return null;
        }
        try {
            if (!query.moveToFirst()) {
                query.close();
                return null;
            }
            String string = query.getString(2);
            String string2 = query.getString(4);
            long j = query.getLong(0);
            MmsSmsNotificationInfo newMessageNotificationInfo = getNewMessageNotificationInfo(string, string2, context, R.drawable.notification_icon, null, j, query.getLong(1), query.getCount());
            set.add(Long.valueOf(j));
            while (query.moveToNext()) {
                set.add(Long.valueOf(query.getLong(0)));
            }
            query.close();
            return newMessageNotificationInfo;
        } catch (Throwable th) {
            query.close();
            throw th;
        }
    }

    private static int getUndeliveredMessageCount(Context context, long[] jArr) {
        Cursor query = SqliteWrapper.query(context, context.getContentResolver(), UNDELIVERED_URI, new String[]{"thread_id"}, "read=0", null, null);
        if (query == null) {
            return 0;
        }
        int count = query.getCount();
        if (jArr != null) {
            try {
                if (query.moveToFirst()) {
                    jArr[0] = query.getLong(0);
                    if (jArr.length >= 2) {
                        long j = jArr[0];
                        while (true) {
                            if (query.moveToNext()) {
                                if (query.getLong(0) != j) {
                                    j = 0;
                                    break;
                                }
                            } else {
                                break;
                            }
                        }
                        jArr[1] = j;
                    }
                }
            } catch (Throwable th) {
                query.close();
                throw th;
            }
        }
        query.close();
        return count;
    }

    public static boolean isFailedToDeliver(Intent intent) {
        return intent != null && intent.getBooleanExtra("undelivered_flag", false);
    }

    public static boolean isFailedToDownload(Intent intent) {
        return intent != null && intent.getBooleanExtra("failed_download_flag", false);
    }

    public static void notifyDownloadFailed(Context context, long j) {
        notifyFailed(context, true, j, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    private static void notifyFailed(Context context, boolean z, long j, boolean z2) {
        String str;
        String str2;
        Intent intent;
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        if (defaultSharedPreferences.getBoolean(MessagingPreferenceActivity.NOTIFICATION_ENABLED, true)) {
            NotificationManager notificationManager = (NotificationManager) context.getSystemService("notification");
            long[] jArr = {0};
            int undeliveredMessageCount = getUndeliveredMessageCount(context, jArr);
            Notification notification = new Notification();
            if (undeliveredMessageCount > 1) {
                str = context.getString(R.string.notification_failed_multiple, Integer.toString(undeliveredMessageCount));
                str2 = context.getString(R.string.notification_failed_multiple_title);
                intent = new Intent(context, ConversationListActivity.class);
            } else {
                String string = z ? context.getString(R.string.message_download_failed_title) : context.getString(R.string.message_send_failed_title);
                String string2 = context.getString(R.string.message_failed_body);
                Intent intent2 = new Intent(context, ComposeMessageActivity.class);
                if (z) {
                    intent2.putExtra("failed_download_flag", true);
                } else {
                    j = jArr[0] != 0 ? jArr[0] : 0;
                    intent2.putExtra("undelivered_flag", true);
                }
                intent2.putExtra("thread_id", j);
                str = string2;
                str2 = string;
                intent = intent2;
            }
            intent.setFlags(335544320);
            PendingIntent activity = PendingIntent.getActivity(context, 0, intent, 134217728);
            notification.icon = R.drawable.stat_notify_sms_failed;
            notification.tickerText = str2;
            notification.setLatestEventInfo(context, str2, str, activity);
            if (z2) {
                if (defaultSharedPreferences.getBoolean(MessagingPreferenceActivity.NOTIFICATION_VIBRATE, false)) {
                    notification.defaults |= 2;
                }
                String string3 = defaultSharedPreferences.getString(MessagingPreferenceActivity.NOTIFICATION_RINGTONE, null);
                notification.sound = TextUtils.isEmpty(string3) ? null : Uri.parse(string3);
            }
            if (z) {
                notificationManager.notify(DOWNLOAD_FAILED_NOTIFICATION_ID, notification);
            } else {
                notificationManager.notify(MESSAGE_FAILED_NOTIFICATION_ID, notification);
            }
        }
    }

    public static void notifySendFailed(Context context) {
        notifyFailed(context, false, 0, false);
    }

    public static void notifySendFailed(Context context, boolean z) {
        notifyFailed(context, false, 0, z);
    }

    public static void updateAllNotifications(final Context context) {
        new Thread(new Runnable() {
            public void run() {
                MessagingNotification.updateNewMessageIndicator(context);
                MessagingNotification.updateSendFailedNotification(context);
                MessagingNotification.updateDownloadFailedNotification(context);
            }
        }).start();
    }

    public static void updateDownloadFailedNotification(Context context) {
        if (getDownloadFailedMessageCount(context) < 1) {
            cancelNotification(context, DOWNLOAD_FAILED_NOTIFICATION_ID);
        }
    }

    public static void updateNewMessageIndicator(Context context) {
        updateNewMessageIndicator(context, false);
    }

    public static void updateNewMessageIndicator(Context context, boolean z) {
        TreeSet treeSet = new TreeSet(INFO_COMPARATOR);
        HashSet hashSet = new HashSet(4);
        int accumulateNotificationInfo = 0 + accumulateNotificationInfo(treeSet, getMmsNewMessageNotificationInfo(context, hashSet)) + accumulateNotificationInfo(treeSet, getSmsNewMessageNotificationInfo(context, hashSet));
        cancelNotification(context, NOTIFICATION_ID);
        cancelNotification(context, OLD_NOTIFICATION_ID);
        if (!treeSet.isEmpty()) {
            ((MmsSmsNotificationInfo) treeSet.first()).deliver(context, z, accumulateNotificationInfo, hashSet.size());
        }
    }

    /* access modifiers changed from: private */
    public static void updateNotification(Context context, Intent intent, String str, int i, boolean z, CharSequence charSequence, long j, String str2, int i2, int i3) {
        Intent intent2;
        String str3;
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        if (defaultSharedPreferences.getBoolean(MessagingPreferenceActivity.NOTIFICATION_ENABLED, true)) {
            Notification notification = new Notification(i, charSequence, j);
            if (i3 > 1) {
                str3 = context.getString(R.string.notification_multiple_title);
                intent2 = new Intent(context, ConversationListActivity.class);
                intent2.setFlags(872415232);
            } else {
                intent2 = intent;
                str3 = str2;
            }
            if (i2 > 1) {
                str = context.getString(R.string.notification_multiple, Integer.toString(i2));
            }
            notification.setLatestEventInfo(context, str3, str, PendingIntent.getActivity(context, 0, intent2, 134217728));
            if (z) {
                if (defaultSharedPreferences.getBoolean(MessagingPreferenceActivity.NOTIFICATION_VIBRATE, false)) {
                    notification.defaults |= 2;
                }
                String string = defaultSharedPreferences.getString(MessagingPreferenceActivity.NOTIFICATION_RINGTONE, null);
                notification.sound = TextUtils.isEmpty(string) ? null : Uri.parse(string);
            }
            notification.flags |= 1;
            notification.ledARGB = -16711936;
            notification.ledOnMS = 500;
            notification.ledOffMS = 2000;
            ((NotificationManager) context.getSystemService("notification")).notify(NOTIFICATION_ID, notification);
        }
    }

    public static void updateSendFailedNotification(Context context) {
        if (getUndeliveredMessageCount(context, null) < 1) {
            cancelNotification(context, MESSAGE_FAILED_NOTIFICATION_ID);
        } else {
            notifySendFailed(context);
        }
    }

    public static void updateSendFailedNotificationForThread(Context context, long j) {
        long[] jArr = {0, 0};
        if (getUndeliveredMessageCount(context, jArr) > 0 && jArr[0] == j && jArr[1] != 0) {
            cancelNotification(context, MESSAGE_FAILED_NOTIFICATION_ID);
        }
    }
}
