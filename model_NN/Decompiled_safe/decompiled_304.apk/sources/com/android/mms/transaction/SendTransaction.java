package com.android.mms.transaction;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import com.android.mms.util.RateController;
import com.android.provider.Telephony;
import com.google.android.mms.pdu.EncodedStringValue;
import com.google.android.mms.pdu.PduComposer;
import com.google.android.mms.pdu.PduParser;
import com.google.android.mms.pdu.PduPersister;
import com.google.android.mms.pdu.SendConf;
import com.google.android.mms.pdu.SendReq;
import com.google.android.mms.util.SendingProgressTokenManager;
import java.util.Arrays;
import vc.lx.sms.data.LogTag;
import vc.lx.sms.util.MessageUtils;
import vc.lx.sms.util.SqliteWrapper;

public class SendTransaction extends Transaction implements Runnable {
    private static final boolean DEBUG = false;
    private static final boolean LOCAL_LOGV = false;
    private static final String TAG = "SendTransaction";
    private final Uri mSendReqURI;
    private Thread mThread;

    public SendTransaction(Context context, int i, TransactionSettings transactionSettings, String str) {
        super(context, i, transactionSettings);
        this.mSendReqURI = Uri.parse(str);
        this.mId = str;
        attach(RetryScheduler.getInstance(context));
    }

    public int getType() {
        return 2;
    }

    public void process() {
        this.mThread = new Thread(this);
        this.mThread.start();
    }

    public void run() {
        try {
            RateController instance = RateController.getInstance();
            if (!instance.isLimitSurpassed() || instance.isAllowedByUser()) {
                PduPersister pduPersister = PduPersister.getPduPersister(this.mContext);
                SendReq sendReq = (SendReq) pduPersister.load(this.mSendReqURI);
                long currentTimeMillis = System.currentTimeMillis() / 1000;
                sendReq.setDate(currentTimeMillis);
                ContentValues contentValues = new ContentValues(1);
                contentValues.put("date", Long.valueOf(currentTimeMillis));
                SqliteWrapper.update(this.mContext, this.mContext.getContentResolver(), this.mSendReqURI, contentValues, null, null);
                String localNumber = MessageUtils.getLocalNumber(this.mContext);
                if (!TextUtils.isEmpty(localNumber)) {
                    sendReq.setFrom(new EncodedStringValue(localNumber));
                }
                long parseId = ContentUris.parseId(this.mSendReqURI);
                byte[] sendPdu = sendPdu(SendingProgressTokenManager.get(Long.valueOf(parseId)), new PduComposer(this.mContext, sendReq).make());
                SendingProgressTokenManager.remove(Long.valueOf(parseId));
                if (Log.isLoggable(LogTag.TRANSACTION, 2)) {
                    Log.d(TAG, "[SendTransaction] run: send mms msg (" + this.mId + "), resp=" + new String(sendPdu));
                }
                SendConf sendConf = (SendConf) new PduParser(sendPdu).parse();
                if (sendConf == null) {
                    Log.e(TAG, "No M-Send.conf received.");
                }
                byte[] transactionId = sendReq.getTransactionId();
                byte[] transactionId2 = sendConf.getTransactionId();
                if (!Arrays.equals(transactionId, transactionId2)) {
                    Log.e(TAG, "Inconsistent Transaction-ID: req=" + new String(transactionId) + ", conf=" + new String(transactionId2));
                    if (this.mTransactionState.getState() != 1) {
                        this.mTransactionState.setState(2);
                        this.mTransactionState.setContentUri(this.mSendReqURI);
                        Log.e(TAG, "Delivery failed.");
                    }
                    notifyObservers();
                    return;
                }
                ContentValues contentValues2 = new ContentValues(2);
                int responseStatus = sendConf.getResponseStatus();
                contentValues2.put(Telephony.BaseMmsColumns.RESPONSE_STATUS, Integer.valueOf(responseStatus));
                if (responseStatus != 128) {
                    SqliteWrapper.update(this.mContext, this.mContext.getContentResolver(), this.mSendReqURI, contentValues2, null, null);
                    Log.e(TAG, "Server returned an error code: " + responseStatus);
                    if (this.mTransactionState.getState() != 1) {
                        this.mTransactionState.setState(2);
                        this.mTransactionState.setContentUri(this.mSendReqURI);
                        Log.e(TAG, "Delivery failed.");
                    }
                    notifyObservers();
                    return;
                }
                contentValues2.put(Telephony.BaseMmsColumns.MESSAGE_ID, PduPersister.toIsoString(sendConf.getMessageId()));
                SqliteWrapper.update(this.mContext, this.mContext.getContentResolver(), this.mSendReqURI, contentValues2, null, null);
                Uri move = pduPersister.move(this.mSendReqURI, Telephony.Mms.Sent.CONTENT_URI);
                this.mTransactionState.setState(1);
                this.mTransactionState.setContentUri(move);
                if (this.mTransactionState.getState() != 1) {
                    this.mTransactionState.setState(2);
                    this.mTransactionState.setContentUri(this.mSendReqURI);
                    Log.e(TAG, "Delivery failed.");
                }
                notifyObservers();
                return;
            }
            Log.e(TAG, "Sending rate limit surpassed.");
            if (this.mTransactionState.getState() != 1) {
                this.mTransactionState.setState(2);
                this.mTransactionState.setContentUri(this.mSendReqURI);
                Log.e(TAG, "Delivery failed.");
            }
            notifyObservers();
        } catch (Throwable th) {
            if (this.mTransactionState.getState() != 1) {
                this.mTransactionState.setState(2);
                this.mTransactionState.setContentUri(this.mSendReqURI);
                Log.e(TAG, "Delivery failed.");
            }
            notifyObservers();
            throw th;
        }
    }
}
