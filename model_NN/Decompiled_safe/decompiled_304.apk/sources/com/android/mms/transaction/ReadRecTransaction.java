package com.android.mms.transaction;

import android.content.Context;
import android.net.Uri;
import com.android.provider.Telephony;
import com.google.android.mms.MmsException;
import com.google.android.mms.pdu.EncodedStringValue;
import com.google.android.mms.pdu.PduComposer;
import com.google.android.mms.pdu.PduPersister;
import com.google.android.mms.pdu.ReadRecInd;
import java.io.IOException;
import vc.lx.sms.util.MessageUtils;

public class ReadRecTransaction extends Transaction {
    private static final boolean DEBUG = false;
    private static final boolean LOCAL_LOGV = false;
    private static final String TAG = "ReadRecTransaction";
    private final Uri mReadReportURI;

    public ReadRecTransaction(Context context, int i, TransactionSettings transactionSettings, String str) {
        super(context, i, transactionSettings);
        this.mReadReportURI = Uri.parse(str);
        this.mId = str;
        attach(RetryScheduler.getInstance(context));
    }

    public int getType() {
        return 3;
    }

    public void process() {
        PduPersister pduPersister = PduPersister.getPduPersister(this.mContext);
        try {
            ReadRecInd readRecInd = (ReadRecInd) pduPersister.load(this.mReadReportURI);
            readRecInd.setFrom(new EncodedStringValue(MessageUtils.getLocalNumber(this.mContext)));
            sendPdu(new PduComposer(this.mContext, readRecInd).make());
            Uri move = pduPersister.move(this.mReadReportURI, Telephony.Mms.Sent.CONTENT_URI);
            this.mTransactionState.setState(1);
            this.mTransactionState.setContentUri(move);
            if (this.mTransactionState.getState() != 1) {
                this.mTransactionState.setState(2);
                this.mTransactionState.setContentUri(this.mReadReportURI);
            }
            notifyObservers();
        } catch (IOException e) {
            if (this.mTransactionState.getState() != 1) {
                this.mTransactionState.setState(2);
                this.mTransactionState.setContentUri(this.mReadReportURI);
            }
            notifyObservers();
        } catch (MmsException e2) {
            if (this.mTransactionState.getState() != 1) {
                this.mTransactionState.setState(2);
                this.mTransactionState.setContentUri(this.mReadReportURI);
            }
            notifyObservers();
        } catch (RuntimeException e3) {
            if (this.mTransactionState.getState() != 1) {
                this.mTransactionState.setState(2);
                this.mTransactionState.setContentUri(this.mReadReportURI);
            }
            notifyObservers();
        } catch (Throwable th) {
            if (this.mTransactionState.getState() != 1) {
                this.mTransactionState.setState(2);
                this.mTransactionState.setContentUri(this.mReadReportURI);
            }
            notifyObservers();
            throw th;
        }
    }
}
