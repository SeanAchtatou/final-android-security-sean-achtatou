package com.android.mms.transaction;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import com.android.provider.Telephony;
import vc.lx.sms.cmcc.MMHttpDefines;
import vc.lx.sms.ui.ConversationListActivity;
import vc.lx.sms2.R;

public class SmsRejectedReceiver extends BroadcastReceiver {
    public static final int SMS_REJECTED_NOTIFICATION_ID = 239;

    public void onReceive(Context context, Intent intent) {
        int i;
        int i2;
        if (Settings.Secure.getInt(context.getContentResolver(), "device_provisioned", 0) == 1 && Telephony.Sms.Intents.SMS_REJECTED_ACTION.equals(intent.getAction())) {
            boolean z = intent.getIntExtra(MMHttpDefines.TAG_RESULT, -1) == 3;
            if (z) {
                NotificationManager notificationManager = (NotificationManager) context.getSystemService("notification");
                Intent intent2 = new Intent(context, ConversationListActivity.class);
                intent2.setAction("android.intent.action.VIEW");
                intent2.setFlags(872415232);
                PendingIntent activity = PendingIntent.getActivity(context, 0, intent2, 0);
                Notification notification = new Notification();
                notification.icon = R.drawable.stat_sys_no_sim;
                if (z) {
                    int i3 = R.string.sms_full_body;
                    i = R.string.sms_full_title;
                    i2 = i3;
                } else {
                    int i4 = R.string.sms_rejected_body;
                    i = R.string.sms_rejected_title;
                    i2 = i4;
                }
                notification.tickerText = context.getString(i);
                notification.defaults = -1;
                notification.setLatestEventInfo(context, context.getString(i), context.getString(i2), activity);
                notificationManager.notify(SMS_REJECTED_NOTIFICATION_ID, notification);
            }
        }
    }
}
