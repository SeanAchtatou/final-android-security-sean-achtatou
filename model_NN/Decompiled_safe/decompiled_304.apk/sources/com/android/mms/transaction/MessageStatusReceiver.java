package com.android.mms.transaction;

import android.content.BroadcastReceiver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.telephony.SmsMessage;
import android.util.Log;
import vc.lx.sms.util.SqliteWrapper;

public class MessageStatusReceiver extends BroadcastReceiver {
    private static final String[] ID_PROJECTION = {"_id"};
    private static final String LOG_TAG = "MessageStatusReceiver";
    public static final String MESSAGE_STATUS_RECEIVED_ACTION = "com.android.mms.transaction.MessageStatusReceiver.MESSAGE_STATUS_RECEIVED";
    private static final Uri STATUS_URI = Uri.parse("content://sms/status");
    private Context mContext;

    private void error(String str) {
        Log.e(LOG_TAG, "[MessageStatusReceiver] " + str);
    }

    private void log(String str) {
        Log.d(LOG_TAG, "[MessageStatusReceiver] " + str);
    }

    private void updateMessageStatus(Context context, Uri uri, byte[] bArr) {
        Cursor query = SqliteWrapper.query(context, context.getContentResolver(), uri, ID_PROJECTION, null, null, null);
        try {
            if (query.moveToFirst()) {
                Uri withAppendedId = ContentUris.withAppendedId(STATUS_URI, (long) query.getInt(0));
                int status = SmsMessage.createFromPdu(bArr).getStatus();
                ContentValues contentValues = new ContentValues(1);
                if (Log.isLoggable("Mms", 3)) {
                    log("updateMessageStatus: msgUrl=" + uri + ", status=" + status);
                }
                contentValues.put("status", Integer.valueOf(status));
                SqliteWrapper.update(context, context.getContentResolver(), withAppendedId, contentValues, null, null);
            } else {
                error("Can't find message for status update: " + uri);
            }
        } finally {
            query.close();
        }
    }

    public void onReceive(Context context, Intent intent) {
        this.mContext = context;
        Log.i(LOG_TAG, "message status changed");
        if (MESSAGE_STATUS_RECEIVED_ACTION.equals(intent.getAction())) {
            updateMessageStatus(context, intent.getData(), intent.getByteArrayExtra("pdu"));
            MessagingNotification.updateNewMessageIndicator(context, true);
        }
    }
}
