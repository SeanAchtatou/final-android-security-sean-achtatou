package com.android.mms.transaction;

import android.os.Bundle;

public class TransactionBundle {
    private static final String MMSC_URL = "mmsc-url";
    private static final String PROXY_ADDRESS = "proxy-address";
    private static final String PROXY_PORT = "proxy-port";
    private static final String PUSH_DATA = "mms-push-data";
    public static final String TRANSACTION_TYPE = "type";
    public static final String URI = "uri";
    private final Bundle mBundle;

    private TransactionBundle(int i) {
        this.mBundle = new Bundle();
        this.mBundle.putInt("type", i);
    }

    public TransactionBundle(int i, String str) {
        this(i);
        this.mBundle.putString("uri", str);
    }

    public TransactionBundle(Bundle bundle) {
        this.mBundle = bundle;
    }

    public Bundle getBundle() {
        return this.mBundle;
    }

    public String getMmscUrl() {
        return this.mBundle.getString(MMSC_URL);
    }

    public String getProxyAddress() {
        return this.mBundle.getString(PROXY_ADDRESS);
    }

    public int getProxyPort() {
        return this.mBundle.getInt(PROXY_PORT);
    }

    public byte[] getPushData() {
        return this.mBundle.getByteArray(PUSH_DATA);
    }

    public int getTransactionType() {
        return this.mBundle.getInt("type");
    }

    public String getUri() {
        return this.mBundle.getString("uri");
    }

    public void setConnectionSettings(TransactionSettings transactionSettings) {
        setConnectionSettings(transactionSettings.getMmscUrl(), transactionSettings.getProxyAddress(), transactionSettings.getProxyPort());
    }

    public void setConnectionSettings(String str, String str2, int i) {
        this.mBundle.putString(MMSC_URL, str);
        this.mBundle.putString(PROXY_ADDRESS, str2);
        this.mBundle.putInt(PROXY_PORT, i);
    }
}
