package com.android.mms.transaction;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.util.Log;
import com.android.provider.Telephony;
import com.google.android.mms.pdu.PduPersister;
import vc.lx.sms.data.LogTag;
import vc.lx.sms.util.SqliteWrapper;

public class RetryScheduler implements Observer {
    private static final boolean DEBUG = false;
    private static final boolean LOCAL_LOGV = false;
    private static final String TAG = "RetryScheduler";
    private static RetryScheduler sInstance;
    private final ContentResolver mContentResolver;
    private final Context mContext;

    private RetryScheduler(Context context) {
        this.mContext = context;
        this.mContentResolver = context.getContentResolver();
    }

    public static RetryScheduler getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new RetryScheduler(context);
        }
        return sInstance;
    }

    private int getResponseStatus(long j) {
        Cursor query = SqliteWrapper.query(this.mContext, this.mContentResolver, Telephony.Mms.Outbox.CONTENT_URI, null, "_id=" + j, null, null);
        try {
            int i = query.moveToFirst() ? query.getInt(query.getColumnIndexOrThrow(Telephony.BaseMmsColumns.RESPONSE_STATUS)) : 0;
            if (i != 0) {
                Log.e(TAG, "Response status is: " + i);
            }
            return i;
        } finally {
            query.close();
        }
    }

    private boolean isConnected() {
        return ((ConnectivityManager) this.mContext.getSystemService("connectivity")).getNetworkInfo(2).isConnected();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0163, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0167, code lost:
        throw r1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void scheduleRetry(android.net.Uri r15) {
        /*
            r14 = this;
            long r7 = android.content.ContentUris.parseId(r15)
            android.net.Uri r0 = com.android.provider.Telephony.MmsSms.PendingMessages.CONTENT_URI
            android.net.Uri$Builder r2 = r0.buildUpon()
            java.lang.String r0 = "protocol"
            java.lang.String r1 = "mms"
            r2.appendQueryParameter(r0, r1)
            java.lang.String r0 = "message"
            java.lang.String r1 = java.lang.String.valueOf(r7)
            r2.appendQueryParameter(r0, r1)
            android.content.Context r0 = r14.mContext
            android.content.ContentResolver r1 = r14.mContentResolver
            android.net.Uri r2 = r2.build()
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            android.database.Cursor r9 = vc.lx.sms.util.SqliteWrapper.query(r0, r1, r2, r3, r4, r5, r6)
            if (r9 == 0) goto L_0x0119
            int r0 = r9.getCount()     // Catch:{ all -> 0x0168 }
            r1 = 1
            if (r0 != r1) goto L_0x0116
            boolean r0 = r9.moveToFirst()     // Catch:{ all -> 0x0168 }
            if (r0 == 0) goto L_0x0116
            java.lang.String r0 = "msg_type"
            int r0 = r9.getColumnIndexOrThrow(r0)     // Catch:{ all -> 0x0168 }
            int r0 = r9.getInt(r0)     // Catch:{ all -> 0x0168 }
            java.lang.String r1 = "retry_index"
            int r1 = r9.getColumnIndexOrThrow(r1)     // Catch:{ all -> 0x0168 }
            int r1 = r9.getInt(r1)     // Catch:{ all -> 0x0168 }
            int r10 = r1 + 1
            r1 = 1
            com.android.mms.transaction.DefaultRetryScheme r2 = new com.android.mms.transaction.DefaultRetryScheme     // Catch:{ all -> 0x0168 }
            android.content.Context r3 = r14.mContext     // Catch:{ all -> 0x0168 }
            r2.<init>(r3, r10)     // Catch:{ all -> 0x0168 }
            android.content.ContentValues r11 = new android.content.ContentValues     // Catch:{ all -> 0x0168 }
            r3 = 4
            r11.<init>(r3)     // Catch:{ all -> 0x0168 }
            long r12 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0168 }
            r3 = 130(0x82, float:1.82E-43)
            if (r0 != r3) goto L_0x011a
            r0 = 1
        L_0x0066:
            r3 = 1
            int r4 = r14.getResponseStatus(r7)     // Catch:{ all -> 0x0168 }
            r5 = 132(0x84, float:1.85E-43)
            if (r4 != r5) goto L_0x007a
            com.android.mms.util.DownloadManager r3 = com.android.mms.util.DownloadManager.getInstance()     // Catch:{ all -> 0x0168 }
            r4 = 2131296339(0x7f090053, float:1.8210592E38)
            r3.showErrorCodeToast(r4)     // Catch:{ all -> 0x0168 }
            r3 = 0
        L_0x007a:
            int r4 = r2.getRetryLimit()     // Catch:{ all -> 0x0168 }
            if (r10 >= r4) goto L_0x011d
            if (r3 == 0) goto L_0x011d
            long r2 = r2.getWaitingInterval()     // Catch:{ all -> 0x0168 }
            long r2 = r2 + r12
            java.lang.String r4 = "Mms:transaction"
            r5 = 2
            boolean r4 = android.util.Log.isLoggable(r4, r5)     // Catch:{ all -> 0x0168 }
            if (r4 == 0) goto L_0x00be
            java.lang.String r4 = "RetryScheduler"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x0168 }
            r5.<init>()     // Catch:{ all -> 0x0168 }
            java.lang.String r6 = "scheduleRetry: retry for "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ all -> 0x0168 }
            java.lang.StringBuilder r5 = r5.append(r15)     // Catch:{ all -> 0x0168 }
            java.lang.String r6 = " is scheduled at "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ all -> 0x0168 }
            long r6 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0168 }
            long r6 = r2 - r6
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ all -> 0x0168 }
            java.lang.String r6 = "ms from now"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ all -> 0x0168 }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x0168 }
            android.util.Log.v(r4, r5)     // Catch:{ all -> 0x0168 }
        L_0x00be:
            java.lang.String r4 = "due_time"
            java.lang.Long r2 = java.lang.Long.valueOf(r2)     // Catch:{ all -> 0x0168 }
            r11.put(r4, r2)     // Catch:{ all -> 0x0168 }
            if (r0 == 0) goto L_0x00d2
            com.android.mms.util.DownloadManager r0 = com.android.mms.util.DownloadManager.getInstance()     // Catch:{ all -> 0x0168 }
            r2 = 130(0x82, float:1.82E-43)
            r0.markState(r15, r2)     // Catch:{ all -> 0x0168 }
        L_0x00d2:
            r0 = r1
        L_0x00d3:
            java.lang.String r1 = "err_type"
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ all -> 0x0168 }
            r11.put(r1, r0)     // Catch:{ all -> 0x0168 }
            java.lang.String r0 = "retry_index"
            java.lang.Integer r1 = java.lang.Integer.valueOf(r10)     // Catch:{ all -> 0x0168 }
            r11.put(r0, r1)     // Catch:{ all -> 0x0168 }
            java.lang.String r0 = "last_try"
            java.lang.Long r1 = java.lang.Long.valueOf(r12)     // Catch:{ all -> 0x0168 }
            r11.put(r0, r1)     // Catch:{ all -> 0x0168 }
            java.lang.String r0 = "_id"
            int r0 = r9.getColumnIndexOrThrow(r0)     // Catch:{ all -> 0x0168 }
            long r3 = r9.getLong(r0)     // Catch:{ all -> 0x0168 }
            android.content.Context r0 = r14.mContext     // Catch:{ all -> 0x0168 }
            android.content.ContentResolver r1 = r14.mContentResolver     // Catch:{ all -> 0x0168 }
            android.net.Uri r2 = com.android.provider.Telephony.MmsSms.PendingMessages.CONTENT_URI     // Catch:{ all -> 0x0168 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x0168 }
            r5.<init>()     // Catch:{ all -> 0x0168 }
            java.lang.String r6 = "_id="
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ all -> 0x0168 }
            java.lang.StringBuilder r3 = r5.append(r3)     // Catch:{ all -> 0x0168 }
            java.lang.String r4 = r3.toString()     // Catch:{ all -> 0x0168 }
            r5 = 0
            r3 = r11
            vc.lx.sms.util.SqliteWrapper.update(r0, r1, r2, r3, r4, r5)     // Catch:{ all -> 0x0168 }
        L_0x0116:
            r9.close()
        L_0x0119:
            return
        L_0x011a:
            r0 = 0
            goto L_0x0066
        L_0x011d:
            r7 = 10
            if (r0 == 0) goto L_0x016d
            android.content.Context r0 = r14.mContext     // Catch:{ all -> 0x0168 }
            android.content.Context r1 = r14.mContext     // Catch:{ all -> 0x0168 }
            android.content.ContentResolver r1 = r1.getContentResolver()     // Catch:{ all -> 0x0168 }
            r2 = 1
            java.lang.String[] r3 = new java.lang.String[r2]     // Catch:{ all -> 0x0168 }
            r2 = 0
            java.lang.String r4 = "thread_id"
            r3[r2] = r4     // Catch:{ all -> 0x0168 }
            r4 = 0
            r5 = 0
            r6 = 0
            r2 = r15
            android.database.Cursor r0 = vc.lx.sms.util.SqliteWrapper.query(r0, r1, r2, r3, r4, r5, r6)     // Catch:{ all -> 0x0168 }
            r1 = -1
            if (r0 == 0) goto L_0x0194
            boolean r3 = r0.moveToFirst()     // Catch:{ all -> 0x0163 }
            if (r3 == 0) goto L_0x0148
            r1 = 0
            long r1 = r0.getLong(r1)     // Catch:{ all -> 0x0163 }
        L_0x0148:
            r0.close()     // Catch:{ all -> 0x0168 }
            r0 = r1
        L_0x014c:
            r2 = -1
            int r2 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r2 == 0) goto L_0x0157
            android.content.Context r2 = r14.mContext     // Catch:{ all -> 0x0168 }
            com.android.mms.transaction.MessagingNotification.notifyDownloadFailed(r2, r0)     // Catch:{ all -> 0x0168 }
        L_0x0157:
            com.android.mms.util.DownloadManager r0 = com.android.mms.util.DownloadManager.getInstance()     // Catch:{ all -> 0x0168 }
            r1 = 135(0x87, float:1.89E-43)
            r0.markState(r15, r1)     // Catch:{ all -> 0x0168 }
            r0 = r7
            goto L_0x00d3
        L_0x0163:
            r1 = move-exception
            r0.close()     // Catch:{ all -> 0x0168 }
            throw r1     // Catch:{ all -> 0x0168 }
        L_0x0168:
            r0 = move-exception
            r9.close()
            throw r0
        L_0x016d:
            android.content.ContentValues r3 = new android.content.ContentValues     // Catch:{ all -> 0x0168 }
            r0 = 1
            r3.<init>(r0)     // Catch:{ all -> 0x0168 }
            java.lang.String r0 = "read"
            r1 = 0
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ all -> 0x0168 }
            r3.put(r0, r1)     // Catch:{ all -> 0x0168 }
            android.content.Context r0 = r14.mContext     // Catch:{ all -> 0x0168 }
            android.content.Context r1 = r14.mContext     // Catch:{ all -> 0x0168 }
            android.content.ContentResolver r1 = r1.getContentResolver()     // Catch:{ all -> 0x0168 }
            r4 = 0
            r5 = 0
            r2 = r15
            vc.lx.sms.util.SqliteWrapper.update(r0, r1, r2, r3, r4, r5)     // Catch:{ all -> 0x0168 }
            android.content.Context r0 = r14.mContext     // Catch:{ all -> 0x0168 }
            r1 = 1
            com.android.mms.transaction.MessagingNotification.notifySendFailed(r0, r1)     // Catch:{ all -> 0x0168 }
            r0 = r7
            goto L_0x00d3
        L_0x0194:
            r0 = r1
            goto L_0x014c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.mms.transaction.RetryScheduler.scheduleRetry(android.net.Uri):void");
    }

    public static void setRetryAlarm(Context context) {
        Cursor pendingMessages = PduPersister.getPduPersister(context).getPendingMessages(Long.MAX_VALUE);
        if (pendingMessages != null) {
            try {
                if (pendingMessages.moveToFirst()) {
                    long j = pendingMessages.getLong(pendingMessages.getColumnIndexOrThrow(Telephony.MmsSms.PendingMessages.DUE_TIME));
                    ((AlarmManager) context.getSystemService("alarm")).set(1, j, PendingIntent.getService(context, 0, new Intent(TransactionService.ACTION_ONALARM, null, context, TransactionService.class), 1073741824));
                    if (Log.isLoggable(LogTag.TRANSACTION, 2)) {
                        Log.v(TAG, "Next retry is scheduled at" + (j - System.currentTimeMillis()) + "ms from now");
                    }
                }
            } finally {
                pendingMessages.close();
            }
        }
    }

    public void update(Observable observable) {
        Transaction transaction;
        Uri contentUri;
        try {
            transaction = (Transaction) observable;
            if (Log.isLoggable(LogTag.TRANSACTION, 2)) {
                Log.v(TAG, "[RetryScheduler] update " + observable);
            }
            if ((transaction instanceof NotificationTransaction) || (transaction instanceof RetrieveTransaction) || (transaction instanceof ReadRecTransaction) || (transaction instanceof SendTransaction)) {
                TransactionState state = transaction.getState();
                if (state.getState() == 2 && (contentUri = state.getContentUri()) != null) {
                    scheduleRetry(contentUri);
                }
                transaction.detach(this);
            }
            if (isConnected()) {
                setRetryAlarm(this.mContext);
            }
        } catch (Throwable th) {
            if (isConnected()) {
                setRetryAlarm(this.mContext);
            }
            throw th;
        }
    }
}
