package com.android.mms.transaction;

import android.app.Service;
import android.content.ContentUris;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager;
import android.util.Log;
import android.widget.Toast;
import com.android.internal.telephony.Phone;
import com.android.mms.util.RateController;
import com.android.net.NetworkConnectivityListener;
import com.android.provider.Telephony;
import com.google.android.mms.pdu.PduPersister;
import java.io.IOException;
import java.util.ArrayList;
import vc.lx.sms.data.LogTag;
import vc.lx.sms2.R;

public class TransactionService extends Service implements Observer {
    public static final String ACTION_ONALARM = "android.intent.action.ACTION_ONALARM";
    private static final int APN_EXTENSION_WAIT = 30000;
    private static final int EVENT_CONTINUE_MMS_CONNECTIVITY = 3;
    private static final int EVENT_DATA_STATE_CHANGED = 2;
    private static final int EVENT_HANDLE_NEXT_PENDING_TRANSACTION = 4;
    private static final int EVENT_QUIT = 100;
    private static final int EVENT_TRANSACTION_REQUEST = 1;
    public static final String STATE = "state";
    public static final String STATE_URI = "uri";
    private static final String TAG = "TransactionService";
    private static final int TOAST_DOWNLOAD_LATER = 2;
    private static final int TOAST_MSG_QUEUED = 1;
    private static final int TOAST_NONE = -1;
    public static final String TRANSACTION_COMPLETED_ACTION = "android.intent.action.TRANSACTION_COMPLETED_ACTION";
    private ConnectivityManager mConnMgr;
    /* access modifiers changed from: private */
    public NetworkConnectivityListener mConnectivityListener;
    /* access modifiers changed from: private */
    public final ArrayList<Transaction> mPending = new ArrayList<>();
    /* access modifiers changed from: private */
    public final ArrayList<Transaction> mProcessing = new ArrayList<>();
    private ServiceHandler mServiceHandler;
    private Looper mServiceLooper;
    public Handler mToastHandler = new Handler() {
        public void handleMessage(Message message) {
            String str = null;
            if (message.what == 1) {
                str = TransactionService.this.getString(R.string.message_queued);
            } else if (message.what == 2) {
                str = TransactionService.this.getString(R.string.download_later);
            }
            if (str != null) {
                Toast.makeText(TransactionService.this, str, 1).show();
            }
        }
    };
    private PowerManager.WakeLock mWakeLock;

    private final class ServiceHandler extends Handler {
        public ServiceHandler(Looper looper) {
            super(looper);
        }

        private void processPendingTransaction(Transaction transaction, TransactionSettings transactionSettings) {
            Transaction transaction2;
            int size;
            if (Log.isLoggable(LogTag.TRANSACTION, 2)) {
                Log.v(TransactionService.TAG, "processPendingTxn: transaction=" + transaction);
            }
            synchronized (TransactionService.this.mProcessing) {
                transaction2 = TransactionService.this.mPending.size() != 0 ? (Transaction) TransactionService.this.mPending.remove(0) : transaction;
                size = TransactionService.this.mProcessing.size();
            }
            if (transaction2 != null) {
                if (transactionSettings != null) {
                    transaction2.setConnectionSettings(transactionSettings);
                }
                try {
                    int serviceId = transaction2.getServiceId();
                    if (Log.isLoggable(LogTag.TRANSACTION, 2)) {
                        Log.v(TransactionService.TAG, "processPendingTxn: process " + serviceId);
                    }
                    if (!processTransaction(transaction2)) {
                        TransactionService.this.stopSelf(serviceId);
                    } else if (Log.isLoggable(LogTag.TRANSACTION, 2)) {
                        Log.v(TransactionService.TAG, "Started deferred processing of transaction  " + transaction2);
                    }
                } catch (IOException e) {
                    Log.w(TransactionService.TAG, e.getMessage(), e);
                }
            } else if (size == 0) {
                if (Log.isLoggable(LogTag.TRANSACTION, 2)) {
                    Log.v(TransactionService.TAG, "processPendingTxn: no more transaction, endMmsConnectivity");
                }
                TransactionService.this.endMmsConnectivity();
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:38:0x00f4, code lost:
            sendMessageDelayed(obtainMessage(3), 30000);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:39:0x0104, code lost:
            if (android.util.Log.isLoggable(vc.lx.sms.data.LogTag.TRANSACTION, 2) == false) goto L_0x011e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:40:0x0106, code lost:
            android.util.Log.v(com.android.mms.transaction.TransactionService.TAG, "processTransaction: starting transaction " + r9);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:41:0x011e, code lost:
            r9.attach(r8.this$0);
            r9.process();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:53:?, code lost:
            return true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:54:?, code lost:
            return true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:55:?, code lost:
            return true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:56:?, code lost:
            return true;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private boolean processTransaction(com.android.mms.transaction.Transaction r9) throws java.io.IOException {
            /*
                r8 = this;
                r7 = 1
                r4 = 2
                java.lang.String r6 = "TransactionService"
                java.lang.String r5 = "Mms:transaction"
                com.android.mms.transaction.TransactionService r0 = com.android.mms.transaction.TransactionService.this
                java.util.ArrayList r1 = r0.mProcessing
                monitor-enter(r1)
                com.android.mms.transaction.TransactionService r0 = com.android.mms.transaction.TransactionService.this     // Catch:{ all -> 0x0129 }
                java.util.ArrayList r0 = r0.mPending     // Catch:{ all -> 0x0129 }
                java.util.Iterator r2 = r0.iterator()     // Catch:{ all -> 0x0129 }
            L_0x0017:
                boolean r0 = r2.hasNext()     // Catch:{ all -> 0x0129 }
                if (r0 == 0) goto L_0x0051
                java.lang.Object r0 = r2.next()     // Catch:{ all -> 0x0129 }
                com.android.mms.transaction.Transaction r0 = (com.android.mms.transaction.Transaction) r0     // Catch:{ all -> 0x0129 }
                boolean r0 = r0.isEquivalent(r9)     // Catch:{ all -> 0x0129 }
                if (r0 == 0) goto L_0x0017
                java.lang.String r0 = "Mms:transaction"
                r2 = 2
                boolean r0 = android.util.Log.isLoggable(r0, r2)     // Catch:{ all -> 0x0129 }
                if (r0 == 0) goto L_0x004e
                java.lang.String r0 = "TransactionService"
                java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0129 }
                r2.<init>()     // Catch:{ all -> 0x0129 }
                java.lang.String r3 = "Transaction already pending: "
                java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0129 }
                int r3 = r9.getServiceId()     // Catch:{ all -> 0x0129 }
                java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0129 }
                java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0129 }
                android.util.Log.v(r0, r2)     // Catch:{ all -> 0x0129 }
            L_0x004e:
                monitor-exit(r1)     // Catch:{ all -> 0x0129 }
                r0 = r7
            L_0x0050:
                return r0
            L_0x0051:
                com.android.mms.transaction.TransactionService r0 = com.android.mms.transaction.TransactionService.this     // Catch:{ all -> 0x0129 }
                java.util.ArrayList r0 = r0.mProcessing     // Catch:{ all -> 0x0129 }
                java.util.Iterator r2 = r0.iterator()     // Catch:{ all -> 0x0129 }
            L_0x005b:
                boolean r0 = r2.hasNext()     // Catch:{ all -> 0x0129 }
                if (r0 == 0) goto L_0x0095
                java.lang.Object r0 = r2.next()     // Catch:{ all -> 0x0129 }
                com.android.mms.transaction.Transaction r0 = (com.android.mms.transaction.Transaction) r0     // Catch:{ all -> 0x0129 }
                boolean r0 = r0.isEquivalent(r9)     // Catch:{ all -> 0x0129 }
                if (r0 == 0) goto L_0x005b
                java.lang.String r0 = "Mms:transaction"
                r2 = 2
                boolean r0 = android.util.Log.isLoggable(r0, r2)     // Catch:{ all -> 0x0129 }
                if (r0 == 0) goto L_0x0092
                java.lang.String r0 = "TransactionService"
                java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0129 }
                r2.<init>()     // Catch:{ all -> 0x0129 }
                java.lang.String r3 = "Duplicated transaction: "
                java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0129 }
                int r3 = r9.getServiceId()     // Catch:{ all -> 0x0129 }
                java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0129 }
                java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0129 }
                android.util.Log.v(r0, r2)     // Catch:{ all -> 0x0129 }
            L_0x0092:
                monitor-exit(r1)     // Catch:{ all -> 0x0129 }
                r0 = r7
                goto L_0x0050
            L_0x0095:
                java.lang.String r0 = "Mms:transaction"
                r2 = 2
                boolean r0 = android.util.Log.isLoggable(r0, r2)     // Catch:{ all -> 0x0129 }
                if (r0 == 0) goto L_0x00a5
                java.lang.String r0 = "TransactionService"
                java.lang.String r2 = "processTransaction: call beginMmsConnectivity..."
                android.util.Log.v(r0, r2)     // Catch:{ all -> 0x0129 }
            L_0x00a5:
                com.android.mms.transaction.TransactionService r0 = com.android.mms.transaction.TransactionService.this     // Catch:{ all -> 0x0129 }
                int r0 = r0.beginMmsConnectivity()     // Catch:{ all -> 0x0129 }
                if (r0 != r7) goto L_0x00c9
                com.android.mms.transaction.TransactionService r0 = com.android.mms.transaction.TransactionService.this     // Catch:{ all -> 0x0129 }
                java.util.ArrayList r0 = r0.mPending     // Catch:{ all -> 0x0129 }
                r0.add(r9)     // Catch:{ all -> 0x0129 }
                java.lang.String r0 = "Mms:transaction"
                r2 = 2
                boolean r0 = android.util.Log.isLoggable(r0, r2)     // Catch:{ all -> 0x0129 }
                if (r0 == 0) goto L_0x00c6
                java.lang.String r0 = "TransactionService"
                java.lang.String r2 = "processTransaction: connResult=APN_REQUEST_STARTED, defer transaction pending MMS connectivity"
                android.util.Log.v(r0, r2)     // Catch:{ all -> 0x0129 }
            L_0x00c6:
                monitor-exit(r1)     // Catch:{ all -> 0x0129 }
                r0 = r7
                goto L_0x0050
            L_0x00c9:
                java.lang.String r0 = "Mms:transaction"
                r2 = 2
                boolean r0 = android.util.Log.isLoggable(r0, r2)     // Catch:{ all -> 0x0129 }
                if (r0 == 0) goto L_0x00ea
                java.lang.String r0 = "TransactionService"
                java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0129 }
                r2.<init>()     // Catch:{ all -> 0x0129 }
                java.lang.String r3 = "Adding transaction to 'mProcessing' list: "
                java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0129 }
                java.lang.StringBuilder r2 = r2.append(r9)     // Catch:{ all -> 0x0129 }
                java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0129 }
                android.util.Log.v(r0, r2)     // Catch:{ all -> 0x0129 }
            L_0x00ea:
                com.android.mms.transaction.TransactionService r0 = com.android.mms.transaction.TransactionService.this     // Catch:{ all -> 0x0129 }
                java.util.ArrayList r0 = r0.mProcessing     // Catch:{ all -> 0x0129 }
                r0.add(r9)     // Catch:{ all -> 0x0129 }
                monitor-exit(r1)     // Catch:{ all -> 0x0129 }
                r0 = 3
                android.os.Message r0 = r8.obtainMessage(r0)
                r1 = 30000(0x7530, double:1.4822E-319)
                r8.sendMessageDelayed(r0, r1)
                java.lang.String r0 = "Mms:transaction"
                boolean r0 = android.util.Log.isLoggable(r5, r4)
                if (r0 == 0) goto L_0x011e
                java.lang.String r0 = "TransactionService"
                java.lang.StringBuilder r0 = new java.lang.StringBuilder
                r0.<init>()
                java.lang.String r1 = "processTransaction: starting transaction "
                java.lang.StringBuilder r0 = r0.append(r1)
                java.lang.StringBuilder r0 = r0.append(r9)
                java.lang.String r0 = r0.toString()
                android.util.Log.v(r6, r0)
            L_0x011e:
                com.android.mms.transaction.TransactionService r0 = com.android.mms.transaction.TransactionService.this
                r9.attach(r0)
                r9.process()
                r0 = r7
                goto L_0x0050
            L_0x0129:
                r0 = move-exception
                monitor-exit(r1)     // Catch:{ all -> 0x0129 }
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.mms.transaction.TransactionService.ServiceHandler.processTransaction(com.android.mms.transaction.Transaction):boolean");
        }

        /* JADX WARNING: Code restructure failed: missing block: B:140:0x0369, code lost:
            r0 = th;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:141:0x036a, code lost:
            r2 = null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:160:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:161:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:181:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:0x006f, code lost:
            if (android.util.Log.isLoggable(vc.lx.sms.data.LogTag.TRANSACTION, 2) == false) goto L_0x0078;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x0071, code lost:
            android.util.Log.v(com.android.mms.transaction.TransactionService.TAG, "handle EVENT_CONTINUE_MMS_CONNECTIVITY event...");
         */
        /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
            r0 = r13.this$0.beginMmsConnectivity();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:24:0x007e, code lost:
            if (r0 == 0) goto L_0x00a8;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:25:0x0080, code lost:
            android.util.Log.v(com.android.mms.transaction.TransactionService.TAG, "Extending MMS connectivity returned " + r0 + " instead of APN_ALREADY_ACTIVE");
         */
        /* JADX WARNING: Code restructure failed: missing block: B:27:0x00a0, code lost:
            android.util.Log.w(com.android.mms.transaction.TransactionService.TAG, "Attempt to extend use of MMS connectivity failed");
         */
        /* JADX WARNING: Code restructure failed: missing block: B:28:0x00a8, code lost:
            sendMessageDelayed(obtainMessage(3), 30000);
         */
        /* JADX WARNING: Removed duplicated region for block: B:117:0x030a  */
        /* JADX WARNING: Removed duplicated region for block: B:143:0x036d  */
        /* JADX WARNING: Removed duplicated region for block: B:172:? A[RETURN, SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:84:0x021c  */
        /* JADX WARNING: Removed duplicated region for block: B:95:0x025b  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void handleMessage(android.os.Message r14) {
            /*
                r13 = this;
                r10 = 0
                r8 = 2
                java.lang.String r11 = "Transaction was null. Stopping self: "
                java.lang.String r9 = "Mms:transaction"
                java.lang.String r7 = "TransactionService"
                java.lang.String r0 = "Mms:transaction"
                boolean r0 = android.util.Log.isLoggable(r9, r8)
                if (r0 == 0) goto L_0x0028
                java.lang.String r0 = "TransactionService"
                java.lang.StringBuilder r0 = new java.lang.StringBuilder
                r0.<init>()
                java.lang.String r1 = "Handling incoming message: "
                java.lang.StringBuilder r0 = r0.append(r1)
                java.lang.StringBuilder r0 = r0.append(r14)
                java.lang.String r0 = r0.toString()
                android.util.Log.v(r7, r0)
            L_0x0028:
                int r0 = r14.what
                switch(r0) {
                    case 1: goto L_0x014a;
                    case 2: goto L_0x00b3;
                    case 3: goto L_0x0050;
                    case 4: goto L_0x0398;
                    case 100: goto L_0x0048;
                    default: goto L_0x002d;
                }
            L_0x002d:
                java.lang.String r0 = "TransactionService"
                java.lang.StringBuilder r0 = new java.lang.StringBuilder
                r0.<init>()
                java.lang.String r1 = "what="
                java.lang.StringBuilder r0 = r0.append(r1)
                int r1 = r14.what
                java.lang.StringBuilder r0 = r0.append(r1)
                java.lang.String r0 = r0.toString()
                android.util.Log.w(r7, r0)
            L_0x0047:
                return
            L_0x0048:
                android.os.Looper r0 = r13.getLooper()
                r0.quit()
                goto L_0x0047
            L_0x0050:
                com.android.mms.transaction.TransactionService r0 = com.android.mms.transaction.TransactionService.this
                java.util.ArrayList r0 = r0.mProcessing
                monitor-enter(r0)
                com.android.mms.transaction.TransactionService r1 = com.android.mms.transaction.TransactionService.this     // Catch:{ all -> 0x0065 }
                java.util.ArrayList r1 = r1.mProcessing     // Catch:{ all -> 0x0065 }
                boolean r1 = r1.isEmpty()     // Catch:{ all -> 0x0065 }
                if (r1 == 0) goto L_0x0068
                monitor-exit(r0)     // Catch:{ all -> 0x0065 }
                goto L_0x0047
            L_0x0065:
                r1 = move-exception
                monitor-exit(r0)     // Catch:{ all -> 0x0065 }
                throw r1
            L_0x0068:
                monitor-exit(r0)     // Catch:{ all -> 0x0065 }
                java.lang.String r0 = "Mms:transaction"
                boolean r0 = android.util.Log.isLoggable(r9, r8)
                if (r0 == 0) goto L_0x0078
                java.lang.String r0 = "TransactionService"
                java.lang.String r0 = "handle EVENT_CONTINUE_MMS_CONNECTIVITY event..."
                android.util.Log.v(r7, r0)
            L_0x0078:
                com.android.mms.transaction.TransactionService r0 = com.android.mms.transaction.TransactionService.this     // Catch:{ IOException -> 0x009f }
                int r0 = r0.beginMmsConnectivity()     // Catch:{ IOException -> 0x009f }
                if (r0 == 0) goto L_0x00a8
                java.lang.String r1 = "TransactionService"
                java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x009f }
                r2.<init>()     // Catch:{ IOException -> 0x009f }
                java.lang.String r3 = "Extending MMS connectivity returned "
                java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ IOException -> 0x009f }
                java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ IOException -> 0x009f }
                java.lang.String r2 = " instead of APN_ALREADY_ACTIVE"
                java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ IOException -> 0x009f }
                java.lang.String r0 = r0.toString()     // Catch:{ IOException -> 0x009f }
                android.util.Log.v(r1, r0)     // Catch:{ IOException -> 0x009f }
                goto L_0x0047
            L_0x009f:
                r0 = move-exception
                java.lang.String r0 = "TransactionService"
                java.lang.String r0 = "Attempt to extend use of MMS connectivity failed"
                android.util.Log.w(r7, r0)
                goto L_0x0047
            L_0x00a8:
                r0 = 3
                android.os.Message r0 = r13.obtainMessage(r0)
                r1 = 30000(0x7530, double:1.4822E-319)
                r13.sendMessageDelayed(r0, r1)
                goto L_0x0047
            L_0x00b3:
                com.android.mms.transaction.TransactionService r0 = com.android.mms.transaction.TransactionService.this
                com.android.net.NetworkConnectivityListener r0 = r0.mConnectivityListener
                if (r0 == 0) goto L_0x0047
                com.android.mms.transaction.TransactionService r0 = com.android.mms.transaction.TransactionService.this
                com.android.net.NetworkConnectivityListener r0 = r0.mConnectivityListener
                android.net.NetworkInfo r0 = r0.getNetworkInfo()
                java.lang.String r1 = "Mms:transaction"
                boolean r1 = android.util.Log.isLoggable(r9, r8)
                if (r1 == 0) goto L_0x00e5
                java.lang.String r1 = "TransactionService"
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>()
                java.lang.String r2 = "Handle DATA_STATE_CHANGED event: "
                java.lang.StringBuilder r1 = r1.append(r2)
                java.lang.StringBuilder r1 = r1.append(r0)
                java.lang.String r1 = r1.toString()
                android.util.Log.v(r7, r1)
            L_0x00e5:
                if (r0 == 0) goto L_0x00ed
                int r1 = r0.getType()
                if (r1 == r8) goto L_0x00fe
            L_0x00ed:
                java.lang.String r0 = "Mms:transaction"
                boolean r0 = android.util.Log.isLoggable(r9, r8)
                if (r0 == 0) goto L_0x0047
                java.lang.String r0 = "TransactionService"
                java.lang.String r0 = "   type is not TYPE_MOBILE_MMS, bail"
                android.util.Log.v(r7, r0)
                goto L_0x0047
            L_0x00fe:
                boolean r1 = r0.isConnected()
                if (r1 != 0) goto L_0x0115
                java.lang.String r0 = "Mms:transaction"
                boolean r0 = android.util.Log.isLoggable(r9, r8)
                if (r0 == 0) goto L_0x0047
                java.lang.String r0 = "TransactionService"
                java.lang.String r0 = "   TYPE_MOBILE_MMS not connected, bail"
                android.util.Log.v(r7, r0)
                goto L_0x0047
            L_0x0115:
                com.android.mms.transaction.TransactionSettings r1 = new com.android.mms.transaction.TransactionSettings
                com.android.mms.transaction.TransactionService r2 = com.android.mms.transaction.TransactionService.this
                java.lang.String r0 = r0.getExtraInfo()
                r1.<init>(r2, r0)
                java.lang.String r0 = r1.getMmscUrl()
                boolean r0 = android.text.TextUtils.isEmpty(r0)
                if (r0 == 0) goto L_0x013b
                java.lang.String r0 = "Mms:transaction"
                boolean r0 = android.util.Log.isLoggable(r9, r8)
                if (r0 == 0) goto L_0x0047
                java.lang.String r0 = "TransactionService"
                java.lang.String r0 = "   empty MMSC url, bail"
                android.util.Log.v(r7, r0)
                goto L_0x0047
            L_0x013b:
                r0 = 3
                android.os.Message r0 = r13.obtainMessage(r0)
                r2 = 30000(0x7530, double:1.4822E-319)
                r13.sendMessageDelayed(r0, r2)
                r13.processPendingTransaction(r10, r1)
                goto L_0x0047
            L_0x014a:
                int r1 = r14.arg1
                java.lang.Object r0 = r14.obj     // Catch:{ Exception -> 0x01db }
                com.android.mms.transaction.TransactionBundle r0 = (com.android.mms.transaction.TransactionBundle) r0     // Catch:{ Exception -> 0x01db }
                java.lang.String r2 = r0.getMmscUrl()     // Catch:{ Exception -> 0x01db }
                if (r2 == 0) goto L_0x01d2
                com.android.mms.transaction.TransactionSettings r3 = new com.android.mms.transaction.TransactionSettings     // Catch:{ Exception -> 0x01db }
                java.lang.String r4 = r0.getProxyAddress()     // Catch:{ Exception -> 0x01db }
                int r5 = r0.getProxyPort()     // Catch:{ Exception -> 0x01db }
                r3.<init>(r2, r4, r5)     // Catch:{ Exception -> 0x01db }
                r2 = r3
            L_0x0164:
                int r3 = r0.getTransactionType()     // Catch:{ Exception -> 0x01db }
                java.lang.String r4 = "Mms:transaction"
                r5 = 2
                boolean r4 = android.util.Log.isLoggable(r4, r5)     // Catch:{ Exception -> 0x01db }
                if (r4 == 0) goto L_0x0189
                java.lang.String r4 = "TransactionService"
                java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01db }
                r5.<init>()     // Catch:{ Exception -> 0x01db }
                java.lang.String r6 = "handle EVENT_TRANSACTION_REQUEST: transactionType="
                java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x01db }
                java.lang.StringBuilder r5 = r5.append(r3)     // Catch:{ Exception -> 0x01db }
                java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x01db }
                android.util.Log.v(r4, r5)     // Catch:{ Exception -> 0x01db }
            L_0x0189:
                switch(r3) {
                    case 0: goto L_0x0248;
                    case 1: goto L_0x02e0;
                    case 2: goto L_0x02ee;
                    case 3: goto L_0x02fc;
                    default: goto L_0x018c;
                }     // Catch:{ Exception -> 0x01db }
            L_0x018c:
                java.lang.String r0 = "TransactionService"
                java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01db }
                r2.<init>()     // Catch:{ Exception -> 0x01db }
                java.lang.String r3 = "Invalid transaction type: "
                java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x01db }
                java.lang.StringBuilder r2 = r2.append(r1)     // Catch:{ Exception -> 0x01db }
                java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x01db }
                android.util.Log.w(r0, r2)     // Catch:{ Exception -> 0x01db }
                if (r10 != 0) goto L_0x0047
                java.lang.String r0 = "Mms:transaction"
                boolean r0 = android.util.Log.isLoggable(r9, r8)
                if (r0 == 0) goto L_0x01c6
                java.lang.String r0 = "TransactionService"
                java.lang.StringBuilder r0 = new java.lang.StringBuilder
                r0.<init>()
                java.lang.String r2 = "Transaction was null. Stopping self: "
                java.lang.StringBuilder r0 = r0.append(r11)
                java.lang.StringBuilder r0 = r0.append(r1)
                java.lang.String r0 = r0.toString()
                android.util.Log.v(r7, r0)
            L_0x01c6:
                com.android.mms.transaction.TransactionService r0 = com.android.mms.transaction.TransactionService.this
                r0.endMmsConnectivity()
                com.android.mms.transaction.TransactionService r0 = com.android.mms.transaction.TransactionService.this
                r0.stopSelf(r1)
                goto L_0x0047
            L_0x01d2:
                com.android.mms.transaction.TransactionSettings r2 = new com.android.mms.transaction.TransactionSettings     // Catch:{ Exception -> 0x01db }
                com.android.mms.transaction.TransactionService r3 = com.android.mms.transaction.TransactionService.this     // Catch:{ Exception -> 0x01db }
                r4 = 0
                r2.<init>(r3, r4)     // Catch:{ Exception -> 0x01db }
                goto L_0x0164
            L_0x01db:
                r0 = move-exception
                r2 = r10
            L_0x01dd:
                java.lang.String r3 = "TransactionService"
                java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x03a6 }
                r4.<init>()     // Catch:{ all -> 0x03a6 }
                java.lang.String r5 = "Exception occurred while handling message: "
                java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x03a6 }
                java.lang.StringBuilder r4 = r4.append(r14)     // Catch:{ all -> 0x03a6 }
                java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x03a6 }
                android.util.Log.w(r3, r4, r0)     // Catch:{ all -> 0x03a6 }
                if (r2 == 0) goto L_0x03ae
                com.android.mms.transaction.TransactionService r0 = com.android.mms.transaction.TransactionService.this     // Catch:{ Throwable -> 0x035c }
                r2.detach(r0)     // Catch:{ Throwable -> 0x035c }
                com.android.mms.transaction.TransactionService r0 = com.android.mms.transaction.TransactionService.this     // Catch:{ Throwable -> 0x035c }
                java.util.ArrayList r0 = r0.mProcessing     // Catch:{ Throwable -> 0x035c }
                boolean r0 = r0.contains(r2)     // Catch:{ Throwable -> 0x035c }
                if (r0 == 0) goto L_0x0219
                com.android.mms.transaction.TransactionService r0 = com.android.mms.transaction.TransactionService.this     // Catch:{ Throwable -> 0x035c }
                java.util.ArrayList r0 = r0.mProcessing     // Catch:{ Throwable -> 0x035c }
                monitor-enter(r0)     // Catch:{ Throwable -> 0x035c }
                com.android.mms.transaction.TransactionService r3 = com.android.mms.transaction.TransactionService.this     // Catch:{ all -> 0x0359 }
                java.util.ArrayList r3 = r3.mProcessing     // Catch:{ all -> 0x0359 }
                r3.remove(r2)     // Catch:{ all -> 0x0359 }
                monitor-exit(r0)     // Catch:{ all -> 0x0359 }
            L_0x0219:
                r0 = r10
            L_0x021a:
                if (r0 != 0) goto L_0x0047
                java.lang.String r0 = "Mms:transaction"
                boolean r0 = android.util.Log.isLoggable(r9, r8)
                if (r0 == 0) goto L_0x023c
                java.lang.String r0 = "TransactionService"
                java.lang.StringBuilder r0 = new java.lang.StringBuilder
                r0.<init>()
                java.lang.String r2 = "Transaction was null. Stopping self: "
                java.lang.StringBuilder r0 = r0.append(r11)
                java.lang.StringBuilder r0 = r0.append(r1)
                java.lang.String r0 = r0.toString()
                android.util.Log.v(r7, r0)
            L_0x023c:
                com.android.mms.transaction.TransactionService r0 = com.android.mms.transaction.TransactionService.this
                r0.endMmsConnectivity()
                com.android.mms.transaction.TransactionService r0 = com.android.mms.transaction.TransactionService.this
                r0.stopSelf(r1)
                goto L_0x0047
            L_0x0248:
                java.lang.String r3 = r0.getUri()     // Catch:{ Exception -> 0x01db }
                if (r3 == 0) goto L_0x0289
                com.android.mms.transaction.NotificationTransaction r0 = new com.android.mms.transaction.NotificationTransaction     // Catch:{ Exception -> 0x01db }
                com.android.mms.transaction.TransactionService r4 = com.android.mms.transaction.TransactionService.this     // Catch:{ Exception -> 0x01db }
                r0.<init>(r4, r1, r2, r3)     // Catch:{ Exception -> 0x01db }
            L_0x0255:
                boolean r2 = r13.processTransaction(r0)     // Catch:{ Exception -> 0x03a8, all -> 0x03a1 }
                if (r2 != 0) goto L_0x030a
                if (r10 != 0) goto L_0x0047
                java.lang.String r0 = "Mms:transaction"
                boolean r0 = android.util.Log.isLoggable(r9, r8)
                if (r0 == 0) goto L_0x027d
                java.lang.String r0 = "TransactionService"
                java.lang.StringBuilder r0 = new java.lang.StringBuilder
                r0.<init>()
                java.lang.String r2 = "Transaction was null. Stopping self: "
                java.lang.StringBuilder r0 = r0.append(r11)
                java.lang.StringBuilder r0 = r0.append(r1)
                java.lang.String r0 = r0.toString()
                android.util.Log.v(r7, r0)
            L_0x027d:
                com.android.mms.transaction.TransactionService r0 = com.android.mms.transaction.TransactionService.this
                r0.endMmsConnectivity()
                com.android.mms.transaction.TransactionService r0 = com.android.mms.transaction.TransactionService.this
                r0.stopSelf(r1)
                goto L_0x0047
            L_0x0289:
                byte[] r0 = r0.getPushData()     // Catch:{ Exception -> 0x01db }
                com.google.android.mms.pdu.PduParser r3 = new com.google.android.mms.pdu.PduParser     // Catch:{ Exception -> 0x01db }
                r3.<init>(r0)     // Catch:{ Exception -> 0x01db }
                com.google.android.mms.pdu.GenericPdu r0 = r3.parse()     // Catch:{ Exception -> 0x01db }
                r3 = 130(0x82, float:1.82E-43)
                if (r0 == 0) goto L_0x02ab
                int r4 = r0.getMessageType()     // Catch:{ Exception -> 0x01db }
                if (r4 != r3) goto L_0x02ab
                com.android.mms.transaction.NotificationTransaction r3 = new com.android.mms.transaction.NotificationTransaction     // Catch:{ Exception -> 0x01db }
                com.android.mms.transaction.TransactionService r4 = com.android.mms.transaction.TransactionService.this     // Catch:{ Exception -> 0x01db }
                com.google.android.mms.pdu.NotificationInd r0 = (com.google.android.mms.pdu.NotificationInd) r0     // Catch:{ Exception -> 0x01db }
                r3.<init>(r4, r1, r2, r0)     // Catch:{ Exception -> 0x01db }
                r0 = r3
                goto L_0x0255
            L_0x02ab:
                java.lang.String r0 = "TransactionService"
                java.lang.String r2 = "Invalid PUSH data."
                android.util.Log.e(r0, r2)     // Catch:{ Exception -> 0x01db }
                if (r10 != 0) goto L_0x0047
                java.lang.String r0 = "Mms:transaction"
                boolean r0 = android.util.Log.isLoggable(r9, r8)
                if (r0 == 0) goto L_0x02d4
                java.lang.String r0 = "TransactionService"
                java.lang.StringBuilder r0 = new java.lang.StringBuilder
                r0.<init>()
                java.lang.String r2 = "Transaction was null. Stopping self: "
                java.lang.StringBuilder r0 = r0.append(r11)
                java.lang.StringBuilder r0 = r0.append(r1)
                java.lang.String r0 = r0.toString()
                android.util.Log.v(r7, r0)
            L_0x02d4:
                com.android.mms.transaction.TransactionService r0 = com.android.mms.transaction.TransactionService.this
                r0.endMmsConnectivity()
                com.android.mms.transaction.TransactionService r0 = com.android.mms.transaction.TransactionService.this
                r0.stopSelf(r1)
                goto L_0x0047
            L_0x02e0:
                com.android.mms.transaction.RetrieveTransaction r3 = new com.android.mms.transaction.RetrieveTransaction     // Catch:{ Exception -> 0x01db }
                com.android.mms.transaction.TransactionService r4 = com.android.mms.transaction.TransactionService.this     // Catch:{ Exception -> 0x01db }
                java.lang.String r0 = r0.getUri()     // Catch:{ Exception -> 0x01db }
                r3.<init>(r4, r1, r2, r0)     // Catch:{ Exception -> 0x01db }
                r0 = r3
                goto L_0x0255
            L_0x02ee:
                com.android.mms.transaction.SendTransaction r3 = new com.android.mms.transaction.SendTransaction     // Catch:{ Exception -> 0x01db }
                com.android.mms.transaction.TransactionService r4 = com.android.mms.transaction.TransactionService.this     // Catch:{ Exception -> 0x01db }
                java.lang.String r0 = r0.getUri()     // Catch:{ Exception -> 0x01db }
                r3.<init>(r4, r1, r2, r0)     // Catch:{ Exception -> 0x01db }
                r0 = r3
                goto L_0x0255
            L_0x02fc:
                com.android.mms.transaction.ReadRecTransaction r3 = new com.android.mms.transaction.ReadRecTransaction     // Catch:{ Exception -> 0x01db }
                com.android.mms.transaction.TransactionService r4 = com.android.mms.transaction.TransactionService.this     // Catch:{ Exception -> 0x01db }
                java.lang.String r0 = r0.getUri()     // Catch:{ Exception -> 0x01db }
                r3.<init>(r4, r1, r2, r0)     // Catch:{ Exception -> 0x01db }
                r0 = r3
                goto L_0x0255
            L_0x030a:
                java.lang.String r2 = "Mms:transaction"
                r3 = 2
                boolean r2 = android.util.Log.isLoggable(r2, r3)     // Catch:{ Exception -> 0x03a8, all -> 0x03a1 }
                if (r2 == 0) goto L_0x032b
                java.lang.String r2 = "TransactionService"
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x03a8, all -> 0x03a1 }
                r3.<init>()     // Catch:{ Exception -> 0x03a8, all -> 0x03a1 }
                java.lang.String r4 = "Started processing of incoming message: "
                java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x03a8, all -> 0x03a1 }
                java.lang.StringBuilder r3 = r3.append(r14)     // Catch:{ Exception -> 0x03a8, all -> 0x03a1 }
                java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x03a8, all -> 0x03a1 }
                android.util.Log.v(r2, r3)     // Catch:{ Exception -> 0x03a8, all -> 0x03a1 }
            L_0x032b:
                if (r0 != 0) goto L_0x0047
                java.lang.String r0 = "Mms:transaction"
                boolean r0 = android.util.Log.isLoggable(r9, r8)
                if (r0 == 0) goto L_0x034d
                java.lang.String r0 = "TransactionService"
                java.lang.StringBuilder r0 = new java.lang.StringBuilder
                r0.<init>()
                java.lang.String r2 = "Transaction was null. Stopping self: "
                java.lang.StringBuilder r0 = r0.append(r11)
                java.lang.StringBuilder r0 = r0.append(r1)
                java.lang.String r0 = r0.toString()
                android.util.Log.v(r7, r0)
            L_0x034d:
                com.android.mms.transaction.TransactionService r0 = com.android.mms.transaction.TransactionService.this
                r0.endMmsConnectivity()
                com.android.mms.transaction.TransactionService r0 = com.android.mms.transaction.TransactionService.this
                r0.stopSelf(r1)
                goto L_0x0047
            L_0x0359:
                r2 = move-exception
                monitor-exit(r0)     // Catch:{ all -> 0x0359 }
                throw r2     // Catch:{ Throwable -> 0x035c }
            L_0x035c:
                r0 = move-exception
                java.lang.String r2 = "TransactionService"
                java.lang.String r3 = "Unexpected Throwable."
                android.util.Log.e(r2, r3, r0)     // Catch:{ all -> 0x0367 }
                r0 = r10
                goto L_0x021a
            L_0x0367:
                r0 = move-exception
                throw r0     // Catch:{ all -> 0x0369 }
            L_0x0369:
                r0 = move-exception
                r2 = r10
            L_0x036b:
                if (r2 != 0) goto L_0x0397
                java.lang.String r2 = "Mms:transaction"
                boolean r2 = android.util.Log.isLoggable(r9, r8)
                if (r2 == 0) goto L_0x038d
                java.lang.String r2 = "TransactionService"
                java.lang.StringBuilder r2 = new java.lang.StringBuilder
                r2.<init>()
                java.lang.String r3 = "Transaction was null. Stopping self: "
                java.lang.StringBuilder r2 = r2.append(r11)
                java.lang.StringBuilder r2 = r2.append(r1)
                java.lang.String r2 = r2.toString()
                android.util.Log.v(r7, r2)
            L_0x038d:
                com.android.mms.transaction.TransactionService r2 = com.android.mms.transaction.TransactionService.this
                r2.endMmsConnectivity()
                com.android.mms.transaction.TransactionService r2 = com.android.mms.transaction.TransactionService.this
                r2.stopSelf(r1)
            L_0x0397:
                throw r0
            L_0x0398:
                java.lang.Object r0 = r14.obj
                com.android.mms.transaction.TransactionSettings r0 = (com.android.mms.transaction.TransactionSettings) r0
                r13.processPendingTransaction(r10, r0)
                goto L_0x0047
            L_0x03a1:
                r2 = move-exception
                r12 = r2
                r2 = r0
                r0 = r12
                goto L_0x036b
            L_0x03a6:
                r0 = move-exception
                goto L_0x036b
            L_0x03a8:
                r2 = move-exception
                r12 = r2
                r2 = r0
                r0 = r12
                goto L_0x01dd
            L_0x03ae:
                r0 = r2
                goto L_0x021a
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.mms.transaction.TransactionService.ServiceHandler.handleMessage(android.os.Message):void");
        }
    }

    private void acquireWakeLock() {
        this.mWakeLock.acquire();
    }

    private synchronized void createWakeLock() {
        if (this.mWakeLock == null) {
            this.mWakeLock = ((PowerManager) getSystemService("power")).newWakeLock(1, "MMS Connectivity");
            this.mWakeLock.setReferenceCounted(false);
        }
    }

    private int getTransactionType(int i) {
        switch (i) {
            case 128:
                return 2;
            case 130:
                return 1;
            case 135:
                return 3;
            default:
                Log.w(TAG, "Unrecognized MESSAGE_TYPE: " + i);
                return -1;
        }
    }

    private boolean isNetworkAvailable() {
        return this.mConnMgr.getNetworkInfo(2).isAvailable();
    }

    private static boolean isTransientFailure(int i) {
        return i < 10 && i > 0;
    }

    private void launchTransaction(int i, TransactionBundle transactionBundle, boolean z) {
        if (z) {
            Log.w(TAG, "launchTransaction: no network error!");
            onNetworkUnavailable(i, transactionBundle.getTransactionType());
            return;
        }
        Message obtainMessage = this.mServiceHandler.obtainMessage(1);
        obtainMessage.arg1 = i;
        obtainMessage.obj = transactionBundle;
        if (Log.isLoggable(LogTag.TRANSACTION, 2)) {
            Log.v(TAG, "launchTransaction: sending message " + obtainMessage);
        }
        this.mServiceHandler.sendMessage(obtainMessage);
    }

    private void onNetworkUnavailable(int i, int i2) {
        if (Log.isLoggable(LogTag.TRANSACTION, 2)) {
            Log.v(TAG, "onNetworkUnavailable: sid=" + i + ", type=" + i2);
        }
        int i3 = i2 == 1 ? 2 : i2 == 2 ? 1 : -1;
        if (i3 != -1) {
            this.mToastHandler.sendEmptyMessage(i3);
        }
        stopSelf(i);
    }

    private void releaseWakeLock() {
        if (this.mWakeLock != null && this.mWakeLock.isHeld()) {
            this.mWakeLock.release();
        }
    }

    private void stopSelfIfIdle(int i) {
        synchronized (this.mProcessing) {
            if (this.mProcessing.isEmpty() && this.mPending.isEmpty()) {
                if (Log.isLoggable(LogTag.TRANSACTION, 2)) {
                    Log.v(TAG, "stopSelfIfIdle: STOP!");
                }
                if (Log.isLoggable(LogTag.TRANSACTION, 2)) {
                    Log.v(TAG, "stopSelfIfIdle: unRegisterForConnectionStateChanges");
                }
                MmsSystemEventReceiver.unRegisterForConnectionStateChanges(getApplicationContext());
                stopSelf(i);
            }
        }
    }

    /* access modifiers changed from: protected */
    public int beginMmsConnectivity() throws IOException {
        createWakeLock();
        int startUsingNetworkFeature = this.mConnMgr.startUsingNetworkFeature(0, Phone.FEATURE_ENABLE_MMS);
        if (Log.isLoggable(LogTag.TRANSACTION, 2)) {
            Log.v(TAG, "beginMmsConnectivity: result=" + startUsingNetworkFeature);
        }
        switch (startUsingNetworkFeature) {
            case 0:
            case 1:
                break;
            default:
                throw new IOException("Cannot establish MMS connectivity");
        }
        acquireWakeLock();
        return startUsingNetworkFeature;
    }

    /* access modifiers changed from: protected */
    public void endMmsConnectivity() {
        try {
            if (Log.isLoggable(LogTag.TRANSACTION, 2)) {
                Log.v(TAG, "endMmsConnectivity");
            }
            this.mServiceHandler.removeMessages(3);
            if (this.mConnMgr != null) {
                this.mConnMgr.stopUsingNetworkFeature(0, Phone.FEATURE_ENABLE_MMS);
            }
        } finally {
            releaseWakeLock();
        }
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        if (Log.isLoggable(LogTag.TRANSACTION, 2)) {
            Log.v(TAG, "Creating TransactionService");
        }
        HandlerThread handlerThread = new HandlerThread(TAG);
        handlerThread.start();
        this.mServiceLooper = handlerThread.getLooper();
        this.mServiceHandler = new ServiceHandler(this.mServiceLooper);
        this.mConnectivityListener = new NetworkConnectivityListener();
        this.mConnectivityListener.registerHandler(this.mServiceHandler, 2);
        this.mConnectivityListener.startListening(this);
    }

    public void onDestroy() {
        if (Log.isLoggable(LogTag.TRANSACTION, 2)) {
            Log.v(TAG, "Destroying TransactionService");
        }
        if (!this.mPending.isEmpty()) {
            Log.w(TAG, "TransactionService exiting with transaction still pending");
        }
        releaseWakeLock();
        this.mConnectivityListener.unregisterHandler(this.mServiceHandler);
        this.mConnectivityListener.stopListening();
        this.mConnectivityListener = null;
        this.mServiceHandler.sendEmptyMessage(100);
    }

    /* JADX INFO: finally extract failed */
    public int onStartCommand(Intent intent, int i, int i2) {
        if (intent == null) {
            return 2;
        }
        this.mConnMgr = (ConnectivityManager) getSystemService("connectivity");
        boolean z = !isNetworkAvailable();
        if (Log.isLoggable(LogTag.TRANSACTION, 2)) {
            Log.v(TAG, "onStart: #" + i2 + ": " + intent.getExtras() + " intent=" + intent);
            Log.v(TAG, "    networkAvailable=" + (!z));
        }
        if (ACTION_ONALARM.equals(intent.getAction()) || intent.getExtras() == null) {
            Cursor pendingMessages = PduPersister.getPduPersister(this).getPendingMessages(System.currentTimeMillis());
            if (pendingMessages != null) {
                try {
                    int count = pendingMessages.getCount();
                    if (Log.isLoggable(LogTag.TRANSACTION, 2)) {
                        Log.v(TAG, "onStart: cursor.count=" + count);
                    }
                    if (count == 0) {
                        if (Log.isLoggable(LogTag.TRANSACTION, 2)) {
                            Log.v(TAG, "onStart: no pending messages. Stopping service.");
                        }
                        RetryScheduler.setRetryAlarm(this);
                        stopSelfIfIdle(i2);
                        pendingMessages.close();
                        return 2;
                    }
                    int columnIndexOrThrow = pendingMessages.getColumnIndexOrThrow("msg_id");
                    int columnIndexOrThrow2 = pendingMessages.getColumnIndexOrThrow("msg_type");
                    if (z) {
                        if (Log.isLoggable(LogTag.TRANSACTION, 2)) {
                            Log.v(TAG, "onStart: registerForConnectionStateChanges");
                        }
                        MmsSystemEventReceiver.registerForConnectionStateChanges(getApplicationContext());
                    }
                    while (pendingMessages.moveToNext()) {
                        int transactionType = getTransactionType(pendingMessages.getInt(columnIndexOrThrow2));
                        if (z) {
                            onNetworkUnavailable(i2, transactionType);
                            pendingMessages.close();
                            return 2;
                        }
                        switch (transactionType) {
                            case 1:
                                if (!isTransientFailure(pendingMessages.getInt(pendingMessages.getColumnIndexOrThrow(Telephony.MmsSms.PendingMessages.ERROR_TYPE)))) {
                                    continue;
                                }
                                break;
                        }
                        launchTransaction(i2, new TransactionBundle(transactionType, ContentUris.withAppendedId(Telephony.Mms.CONTENT_URI, pendingMessages.getLong(columnIndexOrThrow)).toString()), false);
                    }
                    pendingMessages.close();
                } catch (Throwable th) {
                    pendingMessages.close();
                    throw th;
                }
            } else {
                if (Log.isLoggable(LogTag.TRANSACTION, 2)) {
                    Log.v(TAG, "onStart: no pending messages. Stopping service.");
                }
                RetryScheduler.setRetryAlarm(this);
                stopSelfIfIdle(i2);
            }
        } else {
            if (Log.isLoggable(LogTag.TRANSACTION, 2)) {
                Log.v(TAG, "onStart: launch transaction...");
            }
            launchTransaction(i2, new TransactionBundle(intent.getExtras()), z);
        }
        return 2;
    }

    public void update(Observable observable) {
        Transaction transaction = (Transaction) observable;
        int serviceId = transaction.getServiceId();
        if (Log.isLoggable(LogTag.TRANSACTION, 2)) {
            Log.v(TAG, "update transaction " + serviceId);
        }
        try {
            synchronized (this.mProcessing) {
                this.mProcessing.remove(transaction);
                if (this.mPending.size() > 0) {
                    if (Log.isLoggable(LogTag.TRANSACTION, 2)) {
                        Log.v(TAG, "update: handle next pending transaction...");
                    }
                    this.mServiceHandler.sendMessage(this.mServiceHandler.obtainMessage(4, transaction.getConnectionSettings()));
                } else {
                    if (Log.isLoggable(LogTag.TRANSACTION, 2)) {
                        Log.v(TAG, "update: endMmsConnectivity");
                    }
                    endMmsConnectivity();
                }
            }
            Intent intent = new Intent(TRANSACTION_COMPLETED_ACTION);
            TransactionState state = transaction.getState();
            int state2 = state.getState();
            intent.putExtra("state", state2);
            switch (state2) {
                case 1:
                    if (Log.isLoggable(LogTag.TRANSACTION, 2)) {
                        Log.v(TAG, "Transaction complete: " + serviceId);
                    }
                    intent.putExtra("uri", state.getContentUri());
                    switch (transaction.getType()) {
                        case 0:
                        case 1:
                            MessagingNotification.updateNewMessageIndicator(this, true);
                            MessagingNotification.updateDownloadFailedNotification(this);
                            break;
                        case 2:
                            RateController.getInstance().update();
                            break;
                    }
                case 2:
                    if (Log.isLoggable(LogTag.TRANSACTION, 2)) {
                        Log.v(TAG, "Transaction failed: " + serviceId);
                        break;
                    }
                    break;
                default:
                    if (Log.isLoggable(LogTag.TRANSACTION, 2)) {
                        Log.v(TAG, "Transaction state unknown: " + serviceId + " " + state2);
                        break;
                    }
                    break;
            }
            if (Log.isLoggable(LogTag.TRANSACTION, 2)) {
                Log.v(TAG, "update: broadcast transaction result " + state2);
            }
            sendBroadcast(intent);
        } finally {
            transaction.detach(this);
            MmsSystemEventReceiver.unRegisterForConnectionStateChanges(getApplicationContext());
            stopSelf(serviceId);
        }
    }
}
