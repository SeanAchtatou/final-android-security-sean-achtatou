package com.android.mms.transaction;

import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.util.Log;
import com.android.provider.Telephony;
import com.google.android.mms.InvalidHeaderValueException;
import com.google.android.mms.MmsException;
import com.google.android.mms.pdu.EncodedStringValue;
import com.google.android.mms.pdu.GenericPdu;
import com.google.android.mms.pdu.PduHeaders;
import com.google.android.mms.pdu.PduPersister;
import com.google.android.mms.pdu.ReadRecInd;
import com.google.android.mms.pdu.SendReq;
import com.google.android.mms.util.SendingProgressTokenManager;
import java.util.ArrayList;
import vc.lx.sms.ui.MessagingPreferenceActivity;

public class MmsMessageSender implements MessageSender {
    private static final boolean DEFAULT_DELIVERY_REPORT_MODE = false;
    private static final long DEFAULT_EXPIRY_TIME = 604800;
    private static final String DEFAULT_MESSAGE_CLASS = "personal";
    private static final int DEFAULT_PRIORITY = 129;
    private static final boolean DEFAULT_READ_REPORT_MODE = false;
    private static final String TAG = "MmsMessageSender";
    private final Context mContext;
    private final long mMessageSize;
    private final Uri mMessageUri;

    public MmsMessageSender(Context context, Uri uri, long j) {
        this.mContext = context;
        this.mMessageUri = uri;
        this.mMessageSize = j;
        if (this.mMessageUri == null) {
            throw new IllegalArgumentException("Null message URI.");
        }
    }

    public static void sendReadRec(Context context, String str, String str2, int i) {
        try {
            ReadRecInd readRecInd = new ReadRecInd(new EncodedStringValue(PduHeaders.FROM_INSERT_ADDRESS_TOKEN_STR.getBytes()), str2.getBytes(), 18, i, new EncodedStringValue[]{new EncodedStringValue(str)});
            readRecInd.setDate(System.currentTimeMillis() / 1000);
            PduPersister.getPduPersister(context).persist(readRecInd, Telephony.Mms.Outbox.CONTENT_URI);
            context.startService(new Intent(context, TransactionService.class));
        } catch (InvalidHeaderValueException e) {
            Log.e(TAG, "Invalide header value", e);
        } catch (MmsException e2) {
            Log.e(TAG, "Persist message failed", e2);
        }
    }

    private void updatePreferencesHeaders(SendReq sendReq) throws MmsException {
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this.mContext);
        sendReq.setExpiry(defaultSharedPreferences.getLong(MessagingPreferenceActivity.EXPIRY_TIME, DEFAULT_EXPIRY_TIME));
        sendReq.setPriority(defaultSharedPreferences.getInt(MessagingPreferenceActivity.PRIORITY, 129));
        sendReq.setDeliveryReport(defaultSharedPreferences.getBoolean(MessagingPreferenceActivity.MMS_DELIVERY_REPORT_MODE, false) ? 128 : 129);
        sendReq.setReadReport(defaultSharedPreferences.getBoolean(MessagingPreferenceActivity.READ_REPORT_MODE, false) ? 128 : 129);
    }

    public boolean sendMessage(long j) throws MmsException {
        PduPersister pduPersister = PduPersister.getPduPersister(this.mContext);
        GenericPdu load = pduPersister.load(this.mMessageUri);
        if (load.getMessageType() != 128) {
            throw new MmsException("Invalid message: " + load.getMessageType());
        }
        SendReq sendReq = (SendReq) load;
        updatePreferencesHeaders(sendReq);
        sendReq.setMessageClass("personal".getBytes());
        sendReq.setDate(System.currentTimeMillis() / 1000);
        sendReq.setMessageSize(this.mMessageSize);
        pduPersister.updateHeaders(this.mMessageUri, sendReq);
        pduPersister.move(this.mMessageUri, Telephony.Mms.Outbox.CONTENT_URI);
        SendingProgressTokenManager.put(Long.valueOf(ContentUris.parseId(this.mMessageUri)), j);
        this.mContext.startService(new Intent(this.mContext, TransactionService.class));
        return true;
    }

    public boolean sendMessage(long j, ArrayList<String> arrayList) throws MmsException {
        return false;
    }
}
