package com.android.mms.transaction;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.util.Log;
import com.android.internal.telephony.TelephonyIntents;
import com.android.provider.Telephony;
import com.google.android.mms.util.PduCache;
import vc.lx.sms.data.LogTag;

public class MmsSystemEventReceiver extends BroadcastReceiver {
    private static final String TAG = "MmsSystemEventReceiver";
    private static MmsSystemEventReceiver sMmsSystemEventReceiver;

    public static void registerForConnectionStateChanges(Context context) {
        unRegisterForConnectionStateChanges(context);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(TelephonyIntents.ACTION_ANY_DATA_CONNECTION_STATE_CHANGED);
        if (Log.isLoggable(LogTag.TRANSACTION, 2)) {
            Log.v(TAG, "registerForConnectionStateChanges");
        }
        if (sMmsSystemEventReceiver == null) {
            sMmsSystemEventReceiver = new MmsSystemEventReceiver();
        }
        context.registerReceiver(sMmsSystemEventReceiver, intentFilter);
    }

    public static void unRegisterForConnectionStateChanges(Context context) {
        if (Log.isLoggable(LogTag.TRANSACTION, 2)) {
            Log.v(TAG, "unRegisterForConnectionStateChanges");
        }
        if (sMmsSystemEventReceiver != null) {
            try {
                context.unregisterReceiver(sMmsSystemEventReceiver);
            } catch (IllegalArgumentException e) {
            }
        }
    }

    private static void wakeUpService(Context context) {
        if (Log.isLoggable(LogTag.TRANSACTION, 2)) {
            Log.v(TAG, "wakeUpService: start transaction service ...");
        }
        context.startService(new Intent(context, TransactionService.class));
    }

    public void onReceive(Context context, Intent intent) {
        if (Log.isLoggable(LogTag.TRANSACTION, 2)) {
            Log.v(TAG, "Intent received: " + intent);
        }
        String action = intent.getAction();
        if (action.equals(Telephony.Mms.Intents.CONTENT_CHANGED_ACTION)) {
            PduCache.getInstance().purge((Uri) intent.getParcelableExtra(Telephony.Mms.Intents.DELETED_CONTENTS));
        } else if (action.equals(TelephonyIntents.ACTION_ANY_DATA_CONNECTION_STATE_CHANGED)) {
            String stringExtra = intent.getStringExtra("state");
            if (Log.isLoggable(LogTag.TRANSACTION, 2)) {
                Log.v(TAG, "ANY_DATA_STATE event received: " + stringExtra);
            }
            if (stringExtra.equals("CONNECTED")) {
                wakeUpService(context);
            }
        } else if (action.equals("android.intent.action.BOOT_COMPLETED")) {
            MessagingNotification.updateNewMessageIndicator(context);
        }
    }
}
