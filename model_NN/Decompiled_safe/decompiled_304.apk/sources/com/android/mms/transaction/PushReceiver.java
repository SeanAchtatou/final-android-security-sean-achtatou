package com.android.mms.transaction;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.PowerManager;
import android.util.Log;
import com.android.mms.MmsConfig;
import com.android.provider.Telephony;
import com.google.android.mms.ContentType;
import com.google.android.mms.MmsException;
import com.google.android.mms.pdu.DeliveryInd;
import com.google.android.mms.pdu.GenericPdu;
import com.google.android.mms.pdu.NotificationInd;
import com.google.android.mms.pdu.PduParser;
import com.google.android.mms.pdu.PduPersister;
import com.google.android.mms.pdu.ReadOrigInd;
import vc.lx.sms.util.SqliteWrapper;

public class PushReceiver extends BroadcastReceiver {
    private static final boolean DEBUG = false;
    private static final boolean LOCAL_LOGV = false;
    private static final String TAG = "PushReceiver";

    private class ReceivePushTask extends AsyncTask<Intent, Void, Void> {
        private Context mContext;

        public ReceivePushTask(Context context) {
            this.mContext = context;
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Intent... intentArr) {
            GenericPdu parse = new PduParser(intentArr[0].getByteArrayExtra("data")).parse();
            if (parse == null) {
                Log.e(PushReceiver.TAG, "Invalid PUSH data");
                return null;
            }
            PduPersister pduPersister = PduPersister.getPduPersister(this.mContext);
            ContentResolver contentResolver = this.mContext.getContentResolver();
            int messageType = parse.getMessageType();
            switch (messageType) {
                case 130:
                    NotificationInd notificationInd = (NotificationInd) parse;
                    if (MmsConfig.getTransIdEnabled()) {
                        byte[] contentLocation = notificationInd.getContentLocation();
                        if (61 == contentLocation[contentLocation.length - 1]) {
                            byte[] transactionId = notificationInd.getTransactionId();
                            byte[] bArr = new byte[(contentLocation.length + transactionId.length)];
                            System.arraycopy(contentLocation, 0, bArr, 0, contentLocation.length);
                            System.arraycopy(transactionId, 0, bArr, contentLocation.length, transactionId.length);
                            notificationInd.setContentLocation(bArr);
                        }
                    }
                    if (!PushReceiver.isDuplicateNotification(this.mContext, notificationInd)) {
                        Uri persist = pduPersister.persist(parse, Telephony.Mms.Inbox.CONTENT_URI);
                        Intent intent = new Intent(this.mContext, TransactionService.class);
                        intent.putExtra("uri", persist.toString());
                        intent.putExtra("type", 0);
                        this.mContext.startService(intent);
                        break;
                    }
                    break;
                case 134:
                case 136:
                    long access$000 = PushReceiver.findThreadId(this.mContext, parse, messageType);
                    if (access$000 != -1) {
                        Uri persist2 = pduPersister.persist(parse, Telephony.Mms.Inbox.CONTENT_URI);
                        ContentValues contentValues = new ContentValues(1);
                        contentValues.put("thread_id", Long.valueOf(access$000));
                        SqliteWrapper.update(this.mContext, contentResolver, persist2, contentValues, null, null);
                        break;
                    }
                    break;
                default:
                    try {
                        Log.e(PushReceiver.TAG, "Received unrecognized PDU.");
                        break;
                    } catch (MmsException e) {
                        Log.e(PushReceiver.TAG, "Failed to save the data from PUSH: type=" + messageType, e);
                        break;
                    } catch (RuntimeException e2) {
                        Log.e(PushReceiver.TAG, "Unexpected RuntimeException.", e2);
                        break;
                    }
            }
            return null;
        }
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: private */
    public static long findThreadId(Context context, GenericPdu genericPdu, int i) {
        String str = i == 134 ? new String(((DeliveryInd) genericPdu).getMessageId()) : new String(((ReadOrigInd) genericPdu).getMessageId());
        StringBuilder sb = new StringBuilder(40);
        sb.append(Telephony.BaseMmsColumns.MESSAGE_ID);
        sb.append('=');
        sb.append(DatabaseUtils.sqlEscapeString(str));
        sb.append(" AND ");
        sb.append(Telephony.BaseMmsColumns.MESSAGE_TYPE);
        sb.append('=');
        sb.append(128);
        Cursor query = SqliteWrapper.query(context, context.getContentResolver(), Telephony.Mms.CONTENT_URI, new String[]{"thread_id"}, sb.toString(), null, null);
        if (query != null) {
            try {
                if (query.getCount() != 1 || !query.moveToFirst()) {
                    query.close();
                } else {
                    long j = query.getLong(0);
                    query.close();
                    return j;
                }
            } catch (Throwable th) {
                query.close();
                throw th;
            }
        }
        return -1;
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: private */
    public static boolean isDuplicateNotification(Context context, NotificationInd notificationInd) {
        Cursor query;
        byte[] contentLocation = notificationInd.getContentLocation();
        if (!(contentLocation == null || (query = SqliteWrapper.query(context, context.getContentResolver(), Telephony.Mms.CONTENT_URI, new String[]{"_id"}, "ct_l = ?", new String[]{new String(contentLocation)}, null)) == null)) {
            try {
                if (query.getCount() > 0) {
                    query.close();
                    return true;
                }
                query.close();
            } catch (Throwable th) {
                query.close();
                throw th;
            }
        }
        return false;
    }

    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(Telephony.Sms.Intents.WAP_PUSH_RECEIVED_ACTION) && ContentType.MMS_MESSAGE.equals(intent.getType())) {
            ((PowerManager) context.getSystemService("power")).newWakeLock(1, "MMS PushReceiver").acquire(5000);
            new ReceivePushTask(context).execute(intent);
        }
        abortBroadcast();
    }
}
