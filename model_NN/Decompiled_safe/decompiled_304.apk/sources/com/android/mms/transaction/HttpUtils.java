package com.android.mms.transaction;

import android.net.http.AndroidHttpClient;
import android.util.Log;
import com.android.mms.MmsConfig;
import java.io.IOException;
import java.util.Locale;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;

public class HttpUtils {
    private static final boolean DEBUG = false;
    private static final String HDR_KEY_ACCEPT = "Accept";
    private static final String HDR_KEY_ACCEPT_LANGUAGE = "Accept-Language";
    private static final String HDR_VALUE_ACCEPT = "*/*, application/vnd.wap.mms-message, application/vnd.wap.sic";
    private static final String HDR_VALUE_ACCEPT_LANGUAGE = getHttpAcceptLanguage();
    public static final int HTTP_GET_METHOD = 2;
    public static final int HTTP_POST_METHOD = 1;
    private static final boolean LOCAL_LOGV = false;
    private static final String TAG = "Mms:transaction";

    private HttpUtils() {
    }

    private static void addLocaleToHttpAcceptLanguage(StringBuilder sb, Locale locale) {
        String language = locale.getLanguage();
        if (language != null) {
            sb.append(language);
            String country = locale.getCountry();
            if (country != null) {
                sb.append("-");
                sb.append(country);
            }
        }
    }

    private static AndroidHttpClient createHttpClient() {
        String userAgent = MmsConfig.getUserAgent();
        AndroidHttpClient newInstance = AndroidHttpClient.newInstance(userAgent);
        HttpParams params = newInstance.getParams();
        HttpProtocolParams.setContentCharset(params, "UTF-8");
        int httpSocketTimeout = MmsConfig.getHttpSocketTimeout();
        if (Log.isLoggable("Mms:transaction", 3)) {
            Log.d("Mms:transaction", "[HttpUtils] createHttpClient w/ socket timeout " + httpSocketTimeout + " ms, " + ", UA=" + userAgent);
        }
        HttpConnectionParams.setSoTimeout(params, httpSocketTimeout);
        return newInstance;
    }

    private static String getHttpAcceptLanguage() {
        Locale locale = Locale.getDefault();
        StringBuilder sb = new StringBuilder();
        addLocaleToHttpAcceptLanguage(sb, locale);
        if (!locale.equals(Locale.US)) {
            if (sb.length() > 0) {
                sb.append(", ");
            }
            addLocaleToHttpAcceptLanguage(sb, Locale.US);
        }
        return sb.toString();
    }

    private static void handleHttpConnectionException(Exception exc, String str) throws IOException {
        Log.e("Mms:transaction", "Url: " + str + "\n" + exc.getMessage());
        IOException iOException = new IOException(exc.getMessage());
        iOException.initCause(exc);
        throw iOException;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.net.http.AndroidHttpClient.execute(org.apache.http.HttpHost, org.apache.http.HttpRequest):org.apache.http.HttpResponse
     arg types: [org.apache.http.HttpHost, org.apache.http.client.methods.HttpPost]
     candidates:
      android.net.http.AndroidHttpClient.execute(org.apache.http.client.methods.HttpUriRequest, org.apache.http.client.ResponseHandler):T
      android.net.http.AndroidHttpClient.execute(org.apache.http.client.methods.HttpUriRequest, org.apache.http.protocol.HttpContext):org.apache.http.HttpResponse
      android.net.http.AndroidHttpClient.execute(org.apache.http.HttpHost, org.apache.http.HttpRequest):org.apache.http.HttpResponse */
    /* JADX WARNING: Removed duplicated region for block: B:104:0x01ee  */
    /* JADX WARNING: Removed duplicated region for block: B:110:0x01fa  */
    /* JADX WARNING: Removed duplicated region for block: B:114:0x0203  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x015b  */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x01bf  */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:42:0x0115=Splitter:B:42:0x0115, B:107:0x01f5=Splitter:B:107:0x01f5, B:55:0x0156=Splitter:B:55:0x0156, B:101:0x01e9=Splitter:B:101:0x01e9, B:88:0x01ba=Splitter:B:88:0x01ba} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static byte[] httpConnection(android.content.Context r5, long r6, java.lang.String r8, byte[] r9, int r10, boolean r11, java.lang.String r12, int r13) throws java.io.IOException {
        /*
            if (r8 != 0) goto L_0x000a
            java.lang.IllegalArgumentException r5 = new java.lang.IllegalArgumentException
            java.lang.String r6 = "URL must not be null."
            r5.<init>(r6)
            throw r5
        L_0x000a:
            r0 = 0
            java.net.URI r1 = new java.net.URI     // Catch:{ URISyntaxException -> 0x0219, IllegalStateException -> 0x0215, IllegalArgumentException -> 0x0212, SocketException -> 0x01e7, Exception -> 0x01f3, all -> 0x01ff }
            r1.<init>(r8)     // Catch:{ URISyntaxException -> 0x0219, IllegalStateException -> 0x0215, IllegalArgumentException -> 0x0212, SocketException -> 0x01e7, Exception -> 0x01f3, all -> 0x01ff }
            org.apache.http.HttpHost r2 = new org.apache.http.HttpHost     // Catch:{ URISyntaxException -> 0x0219, IllegalStateException -> 0x0215, IllegalArgumentException -> 0x0212, SocketException -> 0x01e7, Exception -> 0x01f3, all -> 0x01ff }
            java.lang.String r3 = r1.getHost()     // Catch:{ URISyntaxException -> 0x0219, IllegalStateException -> 0x0215, IllegalArgumentException -> 0x0212, SocketException -> 0x01e7, Exception -> 0x01f3, all -> 0x01ff }
            int r1 = r1.getPort()     // Catch:{ URISyntaxException -> 0x0219, IllegalStateException -> 0x0215, IllegalArgumentException -> 0x0212, SocketException -> 0x01e7, Exception -> 0x01f3, all -> 0x01ff }
            java.lang.String r4 = "http"
            r2.<init>(r3, r1, r4)     // Catch:{ URISyntaxException -> 0x0219, IllegalStateException -> 0x0215, IllegalArgumentException -> 0x0212, SocketException -> 0x01e7, Exception -> 0x01f3, all -> 0x01ff }
            android.net.http.AndroidHttpClient r0 = createHttpClient()     // Catch:{ URISyntaxException -> 0x0219, IllegalStateException -> 0x0215, IllegalArgumentException -> 0x0212, SocketException -> 0x01e7, Exception -> 0x01f3, all -> 0x01ff }
            switch(r10) {
                case 1: goto L_0x0061;
                case 2: goto L_0x010c;
                default: goto L_0x0026;
            }
        L_0x0026:
            java.lang.String r5 = "Mms:transaction"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
            r6.<init>()     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
            java.lang.String r7 = "Unknown HTTP method: "
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
            java.lang.StringBuilder r6 = r6.append(r10)     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
            java.lang.String r7 = ". Must be one of POST["
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
            r7 = 1
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
            java.lang.String r7 = "] or GET["
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
            r7 = 2
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
            java.lang.String r7 = "]."
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
            java.lang.String r6 = r6.toString()     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
            android.util.Log.e(r5, r6)     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
            r5 = 0
            if (r0 == 0) goto L_0x0060
            r0.close()
        L_0x0060:
            return r5
        L_0x0061:
            com.android.mms.transaction.ProgressCallbackEntity r10 = new com.android.mms.transaction.ProgressCallbackEntity     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
            r10.<init>(r5, r6, r9)     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
            java.lang.String r6 = "application/vnd.wap.mms-message"
            r10.setContentType(r6)     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
            org.apache.http.client.methods.HttpPost r6 = new org.apache.http.client.methods.HttpPost     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
            r6.<init>(r8)     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
            r6.setEntity(r10)     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
        L_0x0073:
            org.apache.http.params.HttpParams r7 = r0.getParams()     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
            if (r11 == 0) goto L_0x0081
            org.apache.http.HttpHost r9 = new org.apache.http.HttpHost     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
            r9.<init>(r12, r13)     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
            org.apache.http.conn.params.ConnRouteParams.setDefaultProxy(r7, r9)     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
        L_0x0081:
            r6.setParams(r7)     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
            java.lang.String r7 = "Accept"
            java.lang.String r9 = "*/*, application/vnd.wap.mms-message, application/vnd.wap.sic"
            r6.addHeader(r7, r9)     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
            java.lang.String r7 = com.android.mms.MmsConfig.getUaProfTagName()     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
            java.lang.String r9 = com.android.mms.MmsConfig.getUaProfUrl()     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
            if (r9 == 0) goto L_0x00b9
            java.lang.String r10 = "Mms:transaction"
            r11 = 2
            boolean r10 = android.util.Log.isLoggable(r10, r11)     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
            if (r10 == 0) goto L_0x00b6
            java.lang.String r10 = "Mms:transaction"
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
            r11.<init>()     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
            java.lang.String r12 = "[HttpUtils] httpConn: xWapProfUrl="
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
            java.lang.StringBuilder r11 = r11.append(r9)     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
            java.lang.String r11 = r11.toString()     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
            android.util.Log.d(r10, r11)     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
        L_0x00b6:
            r6.addHeader(r7, r9)     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
        L_0x00b9:
            java.lang.String r7 = com.android.mms.MmsConfig.getHttpParams()     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
            if (r7 == 0) goto L_0x0120
            java.lang.String r9 = "phone"
            java.lang.Object r5 = r5.getSystemService(r9)     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
            android.telephony.TelephonyManager r5 = (android.telephony.TelephonyManager) r5     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
            java.lang.String r5 = r5.getLine1Number()     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
            java.lang.String r9 = com.android.mms.MmsConfig.getHttpParamsLine1Key()     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
            java.lang.String r10 = "\\|"
            java.lang.String[] r7 = r7.split(r10)     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
            int r10 = r7.length     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
            r11 = 0
        L_0x00d7:
            if (r11 >= r10) goto L_0x0120
            r12 = r7[r11]     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
            java.lang.String r13 = ":"
            r1 = 2
            java.lang.String[] r12 = r12.split(r13, r1)     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
            int r13 = r12.length     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
            r1 = 2
            if (r13 != r1) goto L_0x0109
            r13 = 0
            r13 = r12[r13]     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
            java.lang.String r13 = r13.trim()     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
            r1 = 1
            r12 = r12[r1]     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
            java.lang.String r12 = r12.trim()     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
            if (r9 == 0) goto L_0x00fa
            java.lang.String r12 = r12.replace(r9, r5)     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
        L_0x00fa:
            boolean r1 = android.text.TextUtils.isEmpty(r13)     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
            if (r1 != 0) goto L_0x0109
            boolean r1 = android.text.TextUtils.isEmpty(r12)     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
            if (r1 != 0) goto L_0x0109
            r6.addHeader(r13, r12)     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
        L_0x0109:
            int r11 = r11 + 1
            goto L_0x00d7
        L_0x010c:
            org.apache.http.client.methods.HttpGet r6 = new org.apache.http.client.methods.HttpGet     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
            r6.<init>(r8)     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
            goto L_0x0073
        L_0x0113:
            r5 = move-exception
            r6 = r0
        L_0x0115:
            handleHttpConnectionException(r5, r8)     // Catch:{ all -> 0x020a }
            if (r6 == 0) goto L_0x011d
            r6.close()
        L_0x011d:
            r5 = 0
            goto L_0x0060
        L_0x0120:
            java.lang.String r5 = "Accept-Language"
            java.lang.String r7 = com.android.mms.transaction.HttpUtils.HDR_VALUE_ACCEPT_LANGUAGE     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
            r6.addHeader(r5, r7)     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
            org.apache.http.HttpResponse r5 = r0.execute(r2, r6)     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
            org.apache.http.StatusLine r6 = r5.getStatusLine()     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
            int r7 = r6.getStatusCode()     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
            r9 = 200(0xc8, float:2.8E-43)
            if (r7 == r9) goto L_0x015f
            java.io.IOException r5 = new java.io.IOException     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
            r7.<init>()     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
            java.lang.String r9 = "HTTP error: "
            java.lang.StringBuilder r7 = r7.append(r9)     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
            java.lang.String r6 = r6.getReasonPhrase()     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
            java.lang.StringBuilder r6 = r7.append(r6)     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
            java.lang.String r6 = r6.toString()     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
            r5.<init>(r6)     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
            throw r5     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
        L_0x0154:
            r5 = move-exception
            r6 = r0
        L_0x0156:
            handleHttpConnectionException(r5, r8)     // Catch:{ all -> 0x020a }
            if (r6 == 0) goto L_0x011d
            r6.close()
            goto L_0x011d
        L_0x015f:
            org.apache.http.HttpEntity r5 = r5.getEntity()     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
            r6 = 0
            if (r5 == 0) goto L_0x0220
            long r9 = r5.getContentLength()     // Catch:{ all -> 0x01b1 }
            r11 = 0
            int r7 = (r9 > r11 ? 1 : (r9 == r11 ? 0 : -1))
            if (r7 <= 0) goto L_0x0186
            long r6 = r5.getContentLength()     // Catch:{ all -> 0x01b1 }
            int r6 = (int) r6     // Catch:{ all -> 0x01b1 }
            byte[] r6 = new byte[r6]     // Catch:{ all -> 0x01b1 }
            java.io.DataInputStream r7 = new java.io.DataInputStream     // Catch:{ all -> 0x01b1 }
            java.io.InputStream r9 = r5.getContent()     // Catch:{ all -> 0x01b1 }
            r7.<init>(r9)     // Catch:{ all -> 0x01b1 }
            r7.readFully(r6)     // Catch:{ all -> 0x01c4 }
            r7.close()     // Catch:{ IOException -> 0x0193 }
        L_0x0186:
            if (r5 == 0) goto L_0x021d
            r5.consumeContent()     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
            r5 = r6
        L_0x018c:
            if (r0 == 0) goto L_0x0060
            r0.close()
            goto L_0x0060
        L_0x0193:
            r7 = move-exception
            java.lang.String r9 = "Mms:transaction"
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ all -> 0x01b1 }
            r10.<init>()     // Catch:{ all -> 0x01b1 }
            java.lang.String r11 = "Error closing input stream: "
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ all -> 0x01b1 }
            java.lang.String r7 = r7.getMessage()     // Catch:{ all -> 0x01b1 }
            java.lang.StringBuilder r7 = r10.append(r7)     // Catch:{ all -> 0x01b1 }
            java.lang.String r7 = r7.toString()     // Catch:{ all -> 0x01b1 }
            android.util.Log.e(r9, r7)     // Catch:{ all -> 0x01b1 }
            goto L_0x0186
        L_0x01b1:
            r6 = move-exception
            if (r5 == 0) goto L_0x01b7
            r5.consumeContent()     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
        L_0x01b7:
            throw r6     // Catch:{ URISyntaxException -> 0x0113, IllegalStateException -> 0x0154, IllegalArgumentException -> 0x01b8, SocketException -> 0x020f, Exception -> 0x020c, all -> 0x0207 }
        L_0x01b8:
            r5 = move-exception
            r6 = r0
        L_0x01ba:
            handleHttpConnectionException(r5, r8)     // Catch:{ all -> 0x020a }
            if (r6 == 0) goto L_0x011d
            r6.close()
            goto L_0x011d
        L_0x01c4:
            r6 = move-exception
            r7.close()     // Catch:{ IOException -> 0x01c9 }
        L_0x01c8:
            throw r6     // Catch:{ all -> 0x01b1 }
        L_0x01c9:
            r7 = move-exception
            java.lang.String r9 = "Mms:transaction"
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ all -> 0x01b1 }
            r10.<init>()     // Catch:{ all -> 0x01b1 }
            java.lang.String r11 = "Error closing input stream: "
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ all -> 0x01b1 }
            java.lang.String r7 = r7.getMessage()     // Catch:{ all -> 0x01b1 }
            java.lang.StringBuilder r7 = r10.append(r7)     // Catch:{ all -> 0x01b1 }
            java.lang.String r7 = r7.toString()     // Catch:{ all -> 0x01b1 }
            android.util.Log.e(r9, r7)     // Catch:{ all -> 0x01b1 }
            goto L_0x01c8
        L_0x01e7:
            r5 = move-exception
            r6 = r0
        L_0x01e9:
            handleHttpConnectionException(r5, r8)     // Catch:{ all -> 0x020a }
            if (r6 == 0) goto L_0x011d
            r6.close()
            goto L_0x011d
        L_0x01f3:
            r5 = move-exception
            r6 = r0
        L_0x01f5:
            handleHttpConnectionException(r5, r8)     // Catch:{ all -> 0x020a }
            if (r6 == 0) goto L_0x011d
            r6.close()
            goto L_0x011d
        L_0x01ff:
            r5 = move-exception
            r6 = r0
        L_0x0201:
            if (r6 == 0) goto L_0x0206
            r6.close()
        L_0x0206:
            throw r5
        L_0x0207:
            r5 = move-exception
            r6 = r0
            goto L_0x0201
        L_0x020a:
            r5 = move-exception
            goto L_0x0201
        L_0x020c:
            r5 = move-exception
            r6 = r0
            goto L_0x01f5
        L_0x020f:
            r5 = move-exception
            r6 = r0
            goto L_0x01e9
        L_0x0212:
            r5 = move-exception
            r6 = r0
            goto L_0x01ba
        L_0x0215:
            r5 = move-exception
            r6 = r0
            goto L_0x0156
        L_0x0219:
            r5 = move-exception
            r6 = r0
            goto L_0x0115
        L_0x021d:
            r5 = r6
            goto L_0x018c
        L_0x0220:
            r5 = r6
            goto L_0x018c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.mms.transaction.HttpUtils.httpConnection(android.content.Context, long, java.lang.String, byte[], int, boolean, java.lang.String, int):byte[]");
    }
}
