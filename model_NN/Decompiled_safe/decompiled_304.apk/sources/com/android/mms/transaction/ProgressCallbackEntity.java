package com.android.mms.transaction;

import android.content.Context;
import android.content.Intent;
import java.io.IOException;
import java.io.OutputStream;
import org.apache.http.entity.ByteArrayEntity;

public class ProgressCallbackEntity extends ByteArrayEntity {
    private static final int DEFAULT_PIECE_SIZE = 4096;
    public static final int PROGRESS_ABORT = -2;
    public static final int PROGRESS_COMPLETE = 100;
    public static final int PROGRESS_START = -1;
    public static final String PROGRESS_STATUS_ACTION = "com.android.mms.PROGRESS_STATUS";
    private final byte[] mContent;
    private final Context mContext;
    private final long mToken;

    public ProgressCallbackEntity(Context context, long j, byte[] bArr) {
        super(bArr);
        this.mContext = context;
        this.mContent = bArr;
        this.mToken = j;
    }

    private void broadcastProgressIfNeeded(int i) {
        if (this.mToken > 0) {
            Intent intent = new Intent(PROGRESS_STATUS_ACTION);
            intent.putExtra("progress", i);
            intent.putExtra("token", this.mToken);
            this.mContext.sendBroadcast(intent);
        }
    }

    public void writeTo(OutputStream outputStream) throws IOException {
        boolean z = 0;
        if (outputStream == null) {
            throw new IllegalArgumentException("Output stream may not be null");
        }
        try {
            broadcastProgressIfNeeded(-1);
            int length = this.mContent.length;
            int i = z;
            while (i < length) {
                int i2 = length - i;
                if (i2 > DEFAULT_PIECE_SIZE) {
                    i2 = DEFAULT_PIECE_SIZE;
                }
                outputStream.write(this.mContent, i, i2);
                outputStream.flush();
                i += i2;
                broadcastProgressIfNeeded((i * 100) / length);
            }
            broadcastProgressIfNeeded(100);
            z = true;
        } finally {
            if (z == 0) {
                broadcastProgressIfNeeded(-2);
            }
        }
    }
}
