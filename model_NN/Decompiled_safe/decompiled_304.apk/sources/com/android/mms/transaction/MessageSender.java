package com.android.mms.transaction;

import com.google.android.mms.MmsException;
import java.util.ArrayList;

public interface MessageSender {
    public static final String RECIPIENTS_SEPARATOR = ";";

    boolean sendMessage(long j) throws MmsException;

    boolean sendMessage(long j, ArrayList<String> arrayList) throws MmsException;
}
