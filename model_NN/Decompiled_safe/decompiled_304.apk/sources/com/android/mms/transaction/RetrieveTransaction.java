package com.android.mms.transaction;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;
import com.android.mms.MmsConfig;
import com.android.mms.util.DownloadManager;
import com.android.provider.Telephony;
import com.google.android.mms.MmsException;
import com.google.android.mms.pdu.AcknowledgeInd;
import com.google.android.mms.pdu.EncodedStringValue;
import com.google.android.mms.pdu.PduComposer;
import com.google.android.mms.pdu.PduParser;
import com.google.android.mms.pdu.PduPersister;
import com.google.android.mms.pdu.RetrieveConf;
import java.io.IOException;
import vc.lx.sms.util.MessageUtils;
import vc.lx.sms.util.Recycler;
import vc.lx.sms.util.SqliteWrapper;

public class RetrieveTransaction extends Transaction implements Runnable {
    private static final boolean DEBUG = false;
    private static final boolean LOCAL_LOGV = false;
    private static final String TAG = "RetrieveTransaction";
    private final String mContentLocation;
    private final Uri mUri;

    public RetrieveTransaction(Context context, int i, TransactionSettings transactionSettings, String str) throws MmsException {
        super(context, i, transactionSettings);
        if (str.startsWith("content://")) {
            this.mUri = Uri.parse(str);
            String contentLocation = getContentLocation(context, this.mUri);
            this.mContentLocation = contentLocation;
            this.mId = contentLocation;
            attach(RetryScheduler.getInstance(context));
            return;
        }
        throw new IllegalArgumentException("Initializing from X-Mms-Content-Location is abandoned!");
    }

    private static String getContentLocation(Context context, Uri uri) throws MmsException {
        Cursor query = SqliteWrapper.query(context, context.getContentResolver(), uri, new String[]{Telephony.BaseMmsColumns.CONTENT_LOCATION}, null, null, null);
        if (query != null) {
            try {
                if (query.getCount() == 1 && query.moveToFirst()) {
                    return query.getString(0);
                }
                query.close();
            } finally {
                query.close();
            }
        }
        throw new MmsException("Cannot get X-Mms-Content-Location from: " + uri);
    }

    /* JADX INFO: finally extract failed */
    private static boolean isDuplicateMessage(Context context, RetrieveConf retrieveConf) {
        Cursor query;
        byte[] messageId = retrieveConf.getMessageId();
        if (!(messageId == null || (query = SqliteWrapper.query(context, context.getContentResolver(), Telephony.Mms.CONTENT_URI, new String[]{"_id"}, "(m_id = ? AND m_type = ?)", new String[]{new String(messageId), String.valueOf(132)}, null)) == null)) {
            try {
                if (query.getCount() > 0) {
                    query.close();
                    return true;
                }
                query.close();
            } catch (Throwable th) {
                query.close();
                throw th;
            }
        }
        return false;
    }

    private void sendAcknowledgeInd(RetrieveConf retrieveConf) throws MmsException, IOException {
        byte[] transactionId = retrieveConf.getTransactionId();
        if (transactionId != null) {
            AcknowledgeInd acknowledgeInd = new AcknowledgeInd(18, transactionId);
            acknowledgeInd.setFrom(new EncodedStringValue(MessageUtils.getLocalNumber(this.mContext)));
            if (MmsConfig.getNotifyWapMMSC()) {
                sendPdu(new PduComposer(this.mContext, acknowledgeInd).make(), this.mContentLocation);
            } else {
                sendPdu(new PduComposer(this.mContext, acknowledgeInd).make());
            }
        }
    }

    private static void updateContentLocation(Context context, Uri uri, String str) {
        ContentValues contentValues = new ContentValues(1);
        contentValues.put(Telephony.BaseMmsColumns.CONTENT_LOCATION, str);
        SqliteWrapper.update(context, context.getContentResolver(), uri, contentValues, null, null);
    }

    public int getType() {
        return 1;
    }

    public void process() {
        new Thread(this).start();
    }

    public void run() {
        Uri persist;
        try {
            DownloadManager.getInstance().markState(this.mUri, 129);
            RetrieveConf retrieveConf = (RetrieveConf) new PduParser(getPdu(this.mContentLocation)).parse();
            if (retrieveConf == null) {
                throw new MmsException("Invalid M-Retrieve.conf PDU.");
            }
            if (isDuplicateMessage(this.mContext, retrieveConf)) {
                this.mTransactionState.setState(2);
                this.mTransactionState.setContentUri(this.mUri);
                persist = null;
            } else {
                persist = PduPersister.getPduPersister(this.mContext).persist(retrieveConf, Telephony.Mms.Inbox.CONTENT_URI);
                this.mTransactionState.setState(1);
                this.mTransactionState.setContentUri(persist);
                updateContentLocation(this.mContext, persist, this.mContentLocation);
            }
            SqliteWrapper.delete(this.mContext, this.mContext.getContentResolver(), this.mUri, null, null);
            if (persist != null) {
                Recycler.getMmsRecycler().deleteOldMessagesInSameThreadAsMessage(this.mContext, persist);
            }
            sendAcknowledgeInd(retrieveConf);
            if (this.mTransactionState.getState() != 1) {
                this.mTransactionState.setState(2);
                this.mTransactionState.setContentUri(this.mUri);
                Log.e(TAG, "Retrieval failed.");
            }
            notifyObservers();
        } catch (Throwable th) {
            if (this.mTransactionState.getState() != 1) {
                this.mTransactionState.setState(2);
                this.mTransactionState.setContentUri(this.mUri);
                Log.e(TAG, "Retrieval failed.");
            }
            notifyObservers();
            throw th;
        }
    }
}
