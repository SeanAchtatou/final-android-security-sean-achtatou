package com.android.mms.transaction;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.telephony.SmsManager;
import android.util.Log;
import com.android.mms.MmsConfig;
import com.android.provider.Telephony;
import com.google.android.mms.MmsException;
import java.util.ArrayList;
import vc.lx.sms.data.LogTag;
import vc.lx.sms.ui.MessagingPreferenceActivity;
import vc.lx.sms.util.MessageUtils;
import vc.lx.sms.util.SqliteWrapper;

public class SmsMessageSender implements MessageSender {
    private static final int COLUMN_REPLY_PATH_PRESENT = 0;
    private static final int COLUMN_SERVICE_CENTER = 1;
    private static final boolean DEFAULT_DELIVERY_REPORT_MODE = false;
    private static final String[] SERVICE_CENTER_PROJECTION = {Telephony.TextBasedSmsColumns.REPLY_PATH_PRESENT, Telephony.TextBasedSmsColumns.SERVICE_CENTER};
    private final Context mContext;
    private final String[] mDests;
    private final String mMessageText;
    private final int mNumberOfDests;
    private final String mServiceCenter;
    private final long mThreadId;
    private long mTimestamp;

    public SmsMessageSender(Context context, String[] strArr, long j) {
        this.mContext = context;
        this.mMessageText = "";
        this.mNumberOfDests = strArr.length;
        this.mDests = new String[this.mNumberOfDests];
        System.arraycopy(strArr, 0, this.mDests, 0, this.mNumberOfDests);
        this.mTimestamp = System.currentTimeMillis();
        this.mThreadId = j;
        this.mServiceCenter = getOutgoingServiceCenter(this.mThreadId);
    }

    public SmsMessageSender(Context context, String[] strArr, String str, long j) {
        this.mContext = context;
        this.mMessageText = str;
        this.mNumberOfDests = strArr.length;
        this.mDests = new String[this.mNumberOfDests];
        System.arraycopy(strArr, 0, this.mDests, 0, this.mNumberOfDests);
        this.mTimestamp = System.currentTimeMillis();
        this.mThreadId = j;
        this.mServiceCenter = getOutgoingServiceCenter(this.mThreadId);
    }

    private String getOutgoingServiceCenter(long j) {
        Cursor cursor;
        try {
            Cursor query = SqliteWrapper.query(this.mContext, this.mContext.getContentResolver(), Telephony.Sms.CONTENT_URI, SERVICE_CENTER_PROJECTION, "thread_id = " + j, null, "date DESC");
            if (query != null) {
                try {
                    if (query.moveToFirst()) {
                        String string = 1 == query.getInt(0) ? query.getString(1) : null;
                        if (query != null) {
                            query.close();
                        }
                        return string;
                    }
                } catch (Throwable th) {
                    Throwable th2 = th;
                    cursor = query;
                    th = th2;
                }
            }
            if (query != null) {
                query.close();
            }
            return null;
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
        }
        if (cursor != null) {
            cursor.close();
        }
        throw th;
    }

    private void log(String str) {
        Log.d("Mms", "[SmsMsgSender] " + str);
    }

    public boolean sendMessage(long j) throws MmsException {
        ArrayList<String> divideMessage;
        Uri uri;
        if (this.mMessageText == null || this.mNumberOfDests == 0) {
            throw new MmsException("Null message body or dest.");
        }
        SmsManager smsManager = SmsManager.getDefault();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.mNumberOfDests) {
                return false;
            }
            if (MmsConfig.getEmailGateway() == null || (!Telephony.Mms.isEmailAddress(this.mDests[i2]) && !MessageUtils.isAlias(this.mDests[i2]))) {
                divideMessage = smsManager.divideMessage(this.mMessageText);
            } else {
                this.mDests[i2] = MmsConfig.getEmailGateway();
                divideMessage = smsManager.divideMessage(this.mDests[i2] + " " + this.mMessageText);
            }
            int size = divideMessage.size();
            if (size == 0) {
                throw new MmsException("SmsMessageSender.sendMessage: divideMessage returned empty messages. Original message is \"" + this.mMessageText + "\"");
            }
            ArrayList arrayList = new ArrayList(size);
            ArrayList arrayList2 = new ArrayList(size);
            boolean z = PreferenceManager.getDefaultSharedPreferences(this.mContext).getBoolean(MessagingPreferenceActivity.SMS_DELIVERY_REPORT_MODE, false);
            try {
                uri = Telephony.Sms.Outbox.addMessage(this.mContext.getContentResolver(), this.mDests[i2], this.mMessageText, null, Long.valueOf(this.mTimestamp), z, this.mThreadId);
            } catch (SQLiteException e) {
                SqliteWrapper.checkSQLiteException(this.mContext, e);
                uri = null;
            }
            for (int i3 = 0; i3 < size; i3++) {
                if (z) {
                    arrayList.add(PendingIntent.getBroadcast(this.mContext, 0, new Intent(MessageStatusReceiver.MESSAGE_STATUS_RECEIVED_ACTION, uri, this.mContext, MessageStatusReceiver.class), 0));
                }
                arrayList2.add(PendingIntent.getBroadcast(this.mContext, 0, new Intent(SmsReceiverService.MESSAGE_SENT_ACTION, uri, this.mContext, SmsReceiver.class), 0));
            }
            if (Log.isLoggable(LogTag.TRANSACTION, 2)) {
                log("sendMessage: address[" + i2 + "]=" + this.mDests[i2] + ", threadId=" + this.mThreadId + ", uri=" + uri + ", msgs.count=" + size);
            }
            int i4 = 0;
            while (i4 < size) {
                try {
                    smsManager.sendTextMessage(this.mDests[i4], this.mServiceCenter, divideMessage.get(0), (PendingIntent) arrayList2.get(i4), (PendingIntent) arrayList.get(i4));
                    i4++;
                } catch (Exception e2) {
                    throw new MmsException("SmsMessageSender.sendMessage: caught " + e2 + " from SmsManager.sendMultipartTextMessage()");
                }
            }
            i = i2 + 1;
        }
    }

    public boolean sendMessage(long j, ArrayList<String> arrayList) throws MmsException {
        ArrayList<String> arrayList2;
        Uri uri;
        if (this.mMessageText == null || this.mNumberOfDests == 0) {
            throw new MmsException("Null message body or dest.");
        }
        SmsManager smsManager = SmsManager.getDefault();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.mNumberOfDests) {
                return false;
            }
            if (MmsConfig.getEmailGateway() == null || (!Telephony.Mms.isEmailAddress(this.mDests[i2]) && !MessageUtils.isAlias(this.mDests[i2]))) {
                arrayList2 = arrayList;
            } else {
                this.mDests[i2] + " " + this.mMessageText;
                this.mDests[i2] = MmsConfig.getEmailGateway();
                arrayList2 = arrayList;
            }
            int size = arrayList2.size();
            if (size == 0) {
                throw new MmsException("SmsMessageSender.sendMessage: divideMessage returned empty messages. Original message is \"" + this.mMessageText + "\"");
            }
            ArrayList arrayList3 = new ArrayList(size);
            ArrayList arrayList4 = new ArrayList(size);
            boolean z = PreferenceManager.getDefaultSharedPreferences(this.mContext).getBoolean(MessagingPreferenceActivity.SMS_DELIVERY_REPORT_MODE, false);
            try {
                uri = Telephony.Sms.Outbox.addMessage(this.mContext.getContentResolver(), this.mDests[i2], arrayList.get(i2), null, Long.valueOf(this.mTimestamp), z, this.mThreadId);
            } catch (SQLiteException e) {
                SqliteWrapper.checkSQLiteException(this.mContext, e);
                uri = null;
            }
            for (int i3 = 0; i3 < size; i3++) {
                if (z) {
                    arrayList3.add(PendingIntent.getBroadcast(this.mContext, 0, new Intent(MessageStatusReceiver.MESSAGE_STATUS_RECEIVED_ACTION, uri, this.mContext, MessageStatusReceiver.class), 0));
                }
                arrayList4.add(PendingIntent.getBroadcast(this.mContext, 0, new Intent(SmsReceiverService.MESSAGE_SENT_ACTION, uri, this.mContext, SmsReceiver.class), 0));
            }
            if (Log.isLoggable(LogTag.TRANSACTION, 2)) {
                log("sendMessage: address[" + i2 + "]=" + this.mDests[i2] + ", threadId=" + this.mThreadId + ", uri=" + uri + ", msgs.count=" + size);
            }
            try {
                smsManager.sendTextMessage(this.mDests[i2], this.mServiceCenter, arrayList2.get(i2), (PendingIntent) arrayList4.get(i2), (PendingIntent) arrayList3.get(i2));
                i = i2 + 1;
            } catch (Exception e2) {
                throw new MmsException("SmsMessageSender.sendMessage: caught " + e2 + " from SmsManager.sendMultipartTextMessage()");
            }
        }
    }
}
