package com.android.mms.transaction;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import com.android.internal.telephony.Phone;
import com.android.provider.Telephony;
import vc.lx.sms.util.SqliteWrapper;

public class TransactionSettings {
    private static final String[] APN_PROJECTION = {"type", Telephony.Carriers.MMSC, Telephony.Carriers.MMSPROXY, Telephony.Carriers.MMSPORT};
    private static final int COLUMN_MMSC = 1;
    private static final int COLUMN_MMSPORT = 3;
    private static final int COLUMN_MMSPROXY = 2;
    private static final int COLUMN_TYPE = 0;
    private static final boolean DEBUG = true;
    private static final boolean LOCAL_LOGV = true;
    private static final String TAG = "TransactionSettings";
    private String mProxyAddress;
    private int mProxyPort = -1;
    private String mServiceCenter;

    public TransactionSettings(Context context, String str) {
        String string;
        Cursor query = SqliteWrapper.query(context, context.getContentResolver(), Uri.withAppendedPath(Telephony.Carriers.CONTENT_URI, Telephony.Carriers.CURRENT), APN_PROJECTION, str != null ? "apn='" + str + "'" : null, null, null);
        if (query == null) {
            Log.e(TAG, "Apn is not found in Database!");
            return;
        }
        boolean z = false;
        while (query.moveToNext() && TextUtils.isEmpty(this.mServiceCenter)) {
            try {
                if (isValidApnType(query.getString(0), Phone.APN_TYPE_MMS)) {
                    this.mServiceCenter = query.getString(1);
                    this.mProxyAddress = query.getString(2);
                    if (isProxySet()) {
                        string = query.getString(3);
                        this.mProxyPort = Integer.parseInt(string);
                        z = true;
                    } else {
                        z = true;
                    }
                }
            } catch (NumberFormatException e) {
                if (TextUtils.isEmpty(string)) {
                    Log.w(TAG, "mms port not set!");
                } else {
                    Log.e(TAG, "Bad port number format: " + string, e);
                }
            } catch (Throwable th) {
                query.close();
                throw th;
            }
        }
        query.close();
        if (z && TextUtils.isEmpty(this.mServiceCenter)) {
            Log.e(TAG, "Invalid APN setting: MMSC is empty");
        }
    }

    public TransactionSettings(String str, String str2, int i) {
        this.mServiceCenter = str;
        this.mProxyAddress = str2;
        this.mProxyPort = i;
    }

    private static boolean isValidApnType(String str, String str2) {
        if (TextUtils.isEmpty(str)) {
            return true;
        }
        for (String str3 : str.split(",")) {
            if (str3.equals(str2) || str3.equals("*")) {
                return true;
            }
        }
        return false;
    }

    public String getMmscUrl() {
        return this.mServiceCenter;
    }

    public String getProxyAddress() {
        return this.mProxyAddress;
    }

    public int getProxyPort() {
        return this.mProxyPort;
    }

    public boolean isProxySet() {
        return (this.mProxyAddress == null || this.mProxyAddress.trim().length() == 0) ? false : true;
    }
}
