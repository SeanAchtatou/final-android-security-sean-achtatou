package com.android.mms.dom;

import java.util.Vector;
import org.w3c.dom.DOMException;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

public class NamedNodeMapImpl implements NamedNodeMap {
    private Vector<Node> mNodes = new Vector<>();

    public int getLength() {
        return this.mNodes.size();
    }

    public Node getNamedItem(String str) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.mNodes.size()) {
                return null;
            }
            if (str.equals(this.mNodes.elementAt(i2).getNodeName())) {
                return this.mNodes.elementAt(i2);
            }
            i = i2 + 1;
        }
    }

    public Node getNamedItemNS(String str, String str2) {
        return null;
    }

    public Node item(int i) {
        if (i < this.mNodes.size()) {
            return this.mNodes.elementAt(i);
        }
        return null;
    }

    public Node removeNamedItem(String str) throws DOMException {
        Node namedItem = getNamedItem(str);
        if (namedItem == null) {
            throw new DOMException(8, "Not found");
        }
        this.mNodes.remove(namedItem);
        return namedItem;
    }

    public Node removeNamedItemNS(String str, String str2) throws DOMException {
        return null;
    }

    public Node setNamedItem(Node node) throws DOMException {
        Node namedItem = getNamedItem(node.getNodeName());
        if (namedItem != null) {
            this.mNodes.remove(namedItem);
        }
        this.mNodes.add(node);
        return namedItem;
    }

    public Node setNamedItemNS(Node node) throws DOMException {
        return null;
    }
}
