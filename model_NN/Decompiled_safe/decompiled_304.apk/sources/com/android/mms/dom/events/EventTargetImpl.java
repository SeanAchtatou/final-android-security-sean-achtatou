package com.android.mms.dom.events;

import android.util.Log;
import java.util.ArrayList;
import org.w3c.dom.events.Event;
import org.w3c.dom.events.EventException;
import org.w3c.dom.events.EventListener;
import org.w3c.dom.events.EventTarget;

public class EventTargetImpl implements EventTarget {
    private static final String TAG = "EventTargetImpl";
    private ArrayList<EventListenerEntry> mListenerEntries;
    private EventTarget mNodeTarget;

    static class EventListenerEntry {
        final EventListener mListener;
        final String mType;
        final boolean mUseCapture;

        EventListenerEntry(String str, EventListener eventListener, boolean z) {
            this.mType = str;
            this.mListener = eventListener;
            this.mUseCapture = z;
        }
    }

    public EventTargetImpl(EventTarget eventTarget) {
        this.mNodeTarget = eventTarget;
    }

    public void addEventListener(String str, EventListener eventListener, boolean z) {
        if (str != null && !str.equals("") && eventListener != null) {
            removeEventListener(str, eventListener, z);
            if (this.mListenerEntries == null) {
                this.mListenerEntries = new ArrayList<>();
            }
            this.mListenerEntries.add(new EventListenerEntry(str, eventListener, z));
        }
    }

    public boolean dispatchEvent(Event event) throws EventException {
        EventImpl eventImpl = (EventImpl) event;
        if (!eventImpl.isInitialized()) {
            throw new EventException(0, "Event not initialized");
        } else if (eventImpl.getType() == null || eventImpl.getType().equals("")) {
            throw new EventException(0, "Unspecified even type");
        } else {
            eventImpl.setTarget(this.mNodeTarget);
            eventImpl.setEventPhase(2);
            eventImpl.setCurrentTarget(this.mNodeTarget);
            if (!eventImpl.isPropogationStopped() && this.mListenerEntries != null) {
                for (int i = 0; i < this.mListenerEntries.size(); i++) {
                    EventListenerEntry eventListenerEntry = this.mListenerEntries.get(i);
                    if (!eventListenerEntry.mUseCapture && eventListenerEntry.mType.equals(eventImpl.getType())) {
                        try {
                            eventListenerEntry.mListener.handleEvent(eventImpl);
                        } catch (Exception e) {
                            Log.w(TAG, "Catched EventListener exception", e);
                        }
                    }
                }
            }
            if (eventImpl.getBubbles()) {
            }
            return eventImpl.isPreventDefault();
        }
    }

    public void removeEventListener(String str, EventListener eventListener, boolean z) {
        if (this.mListenerEntries != null) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < this.mListenerEntries.size()) {
                    EventListenerEntry eventListenerEntry = this.mListenerEntries.get(i2);
                    if (eventListenerEntry.mUseCapture == z && eventListenerEntry.mListener == eventListener && eventListenerEntry.mType.equals(str)) {
                        this.mListenerEntries.remove(i2);
                        return;
                    }
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }
}
