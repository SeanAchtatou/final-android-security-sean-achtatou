package com.android.mms.dom.smil;

import android.util.Log;
import com.android.mms.transaction.MessageSender;
import com.android.mms.ui.EditSlideDurationActivity;
import java.util.ArrayList;
import org.w3c.dom.DOMException;
import org.w3c.dom.smil.ElementTime;
import org.w3c.dom.smil.SMILElement;
import org.w3c.dom.smil.TimeList;

public abstract class ElementTimeImpl implements ElementTime {
    private static final String FILLDEFAULT_ATTRIBUTE_NAME = "fillDefault";
    private static final String FILL_ATTRIBUTE_NAME = "fill";
    private static final String FILL_AUTO_ATTRIBUTE = "auto";
    private static final String FILL_FREEZE_ATTRIBUTE = "freeze";
    private static final String FILL_HOLD_ATTRIBUTE = "hold";
    private static final String FILL_REMOVE_ATTRIBUTE = "remove";
    private static final String FILL_TRANSITION_ATTRIBUTE = "transition";
    private static final String TAG = "ElementTimeImpl";
    final SMILElement mSmilElement;

    ElementTimeImpl(SMILElement sMILElement) {
        this.mSmilElement = sMILElement;
    }

    public TimeList getBegin() {
        String[] split = this.mSmilElement.getAttribute("begin").split(MessageSender.RECIPIENTS_SEPARATOR);
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < split.length; i++) {
            try {
                arrayList.add(new TimeImpl(split[i], getBeginConstraints()));
            } catch (IllegalArgumentException e) {
            }
        }
        if (arrayList.size() == 0) {
            arrayList.add(new TimeImpl("0", 255));
        }
        return new TimeListImpl(arrayList);
    }

    /* access modifiers changed from: package-private */
    public int getBeginConstraints() {
        return 255;
    }

    public float getDur() {
        try {
            String attribute = this.mSmilElement.getAttribute(EditSlideDurationActivity.SLIDE_DUR);
            if (attribute != null) {
                return TimeImpl.parseClockValue(attribute) / 1000.0f;
            }
            return 0.0f;
        } catch (IllegalArgumentException e) {
            return 0.0f;
        }
    }

    public TimeList getEnd() {
        ArrayList arrayList = new ArrayList();
        String[] split = this.mSmilElement.getAttribute("end").split(MessageSender.RECIPIENTS_SEPARATOR);
        int length = split.length;
        if (!(length == 1 && split[0].length() == 0)) {
            for (int i = 0; i < length; i++) {
                try {
                    arrayList.add(new TimeImpl(split[i], getEndConstraints()));
                } catch (IllegalArgumentException e) {
                    Log.e(TAG, "Malformed time value.", e);
                }
            }
        }
        if (arrayList.size() == 0) {
            float dur = getDur();
            if (dur < 0.0f) {
                arrayList.add(new TimeImpl("indefinite", getEndConstraints()));
            } else {
                TimeList begin = getBegin();
                for (int i2 = 0; i2 < begin.getLength(); i2++) {
                    arrayList.add(new TimeImpl((begin.item(i2).getResolvedOffset() + ((double) dur)) + "s", getEndConstraints()));
                }
            }
        }
        return new TimeListImpl(arrayList);
    }

    /* access modifiers changed from: package-private */
    public int getEndConstraints() {
        return 255;
    }

    public short getFill() {
        short fillDefault;
        String attribute = this.mSmilElement.getAttribute(FILL_ATTRIBUTE_NAME);
        if (attribute.equalsIgnoreCase(FILL_FREEZE_ATTRIBUTE)) {
            return 1;
        }
        if (attribute.equalsIgnoreCase(FILL_REMOVE_ATTRIBUTE)) {
            return 0;
        }
        if (attribute.equalsIgnoreCase(FILL_HOLD_ATTRIBUTE)) {
            return 1;
        }
        if (attribute.equalsIgnoreCase(FILL_TRANSITION_ATTRIBUTE)) {
            return 1;
        }
        return (attribute.equalsIgnoreCase("auto") || (fillDefault = getFillDefault()) == 2) ? (this.mSmilElement.getAttribute(EditSlideDurationActivity.SLIDE_DUR).length() == 0 && this.mSmilElement.getAttribute("end").length() == 0 && this.mSmilElement.getAttribute("repeatCount").length() == 0 && this.mSmilElement.getAttribute("repeatDur").length() == 0) ? (short) 1 : 0 : fillDefault;
    }

    public short getFillDefault() {
        String attribute = this.mSmilElement.getAttribute(FILLDEFAULT_ATTRIBUTE_NAME);
        if (attribute.equalsIgnoreCase(FILL_REMOVE_ATTRIBUTE)) {
            return 0;
        }
        if (attribute.equalsIgnoreCase(FILL_FREEZE_ATTRIBUTE)) {
            return 1;
        }
        if (attribute.equalsIgnoreCase("auto")) {
            return 2;
        }
        if (attribute.equalsIgnoreCase(FILL_HOLD_ATTRIBUTE)) {
            return 1;
        }
        if (attribute.equalsIgnoreCase(FILL_TRANSITION_ATTRIBUTE)) {
            return 1;
        }
        ElementTime parentElementTime = getParentElementTime();
        if (parentElementTime == null) {
            return 2;
        }
        return ((ElementTimeImpl) parentElementTime).getFillDefault();
    }

    /* access modifiers changed from: package-private */
    public abstract ElementTime getParentElementTime();

    public float getRepeatCount() {
        try {
            float parseFloat = Float.parseFloat(this.mSmilElement.getAttribute("repeatCount"));
            if (parseFloat > 0.0f) {
                return parseFloat;
            }
            return 0.0f;
        } catch (NumberFormatException e) {
            return 0.0f;
        }
    }

    public float getRepeatDur() {
        try {
            float parseClockValue = TimeImpl.parseClockValue(this.mSmilElement.getAttribute("repeatDur"));
            if (parseClockValue > 0.0f) {
                return parseClockValue;
            }
            return 0.0f;
        } catch (IllegalArgumentException e) {
            return 0.0f;
        }
    }

    public short getRestart() {
        String attribute = this.mSmilElement.getAttribute("restart");
        if (attribute.equalsIgnoreCase("never")) {
            return 1;
        }
        return attribute.equalsIgnoreCase("whenNotActive") ? (short) 2 : 0;
    }

    public void setBegin(TimeList timeList) throws DOMException {
        this.mSmilElement.setAttribute("begin", "indefinite");
    }

    public void setDur(float f) throws DOMException {
        this.mSmilElement.setAttribute(EditSlideDurationActivity.SLIDE_DUR, Integer.toString((int) (1000.0f * f)) + "ms");
    }

    public void setEnd(TimeList timeList) throws DOMException {
        this.mSmilElement.setAttribute("end", "indefinite");
    }

    public void setFill(short s) throws DOMException {
        if (s == 1) {
            this.mSmilElement.setAttribute(FILL_ATTRIBUTE_NAME, FILL_FREEZE_ATTRIBUTE);
        } else {
            this.mSmilElement.setAttribute(FILL_ATTRIBUTE_NAME, FILL_REMOVE_ATTRIBUTE);
        }
    }

    public void setFillDefault(short s) throws DOMException {
        if (s == 1) {
            this.mSmilElement.setAttribute(FILLDEFAULT_ATTRIBUTE_NAME, FILL_FREEZE_ATTRIBUTE);
        } else {
            this.mSmilElement.setAttribute(FILLDEFAULT_ATTRIBUTE_NAME, FILL_REMOVE_ATTRIBUTE);
        }
    }

    public void setRepeatCount(float f) throws DOMException {
        String str = "indefinite";
        if (f > 0.0f) {
            str = Float.toString(f);
        }
        this.mSmilElement.setAttribute("repeatCount", str);
    }

    public void setRepeatDur(float f) throws DOMException {
        String str = "indefinite";
        if (f > 0.0f) {
            str = Float.toString(f) + "ms";
        }
        this.mSmilElement.setAttribute("repeatDur", str);
    }

    public void setRestart(short s) throws DOMException {
        if (s == 1) {
            this.mSmilElement.setAttribute("restart", "never");
        } else if (s == 2) {
            this.mSmilElement.setAttribute("restart", "whenNotActive");
        } else {
            this.mSmilElement.setAttribute("restart", "always");
        }
    }
}
