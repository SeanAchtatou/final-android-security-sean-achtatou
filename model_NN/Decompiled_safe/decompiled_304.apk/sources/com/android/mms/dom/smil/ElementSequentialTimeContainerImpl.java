package com.android.mms.dom.smil;

import com.android.mms.dom.NodeListImpl;
import java.util.ArrayList;
import org.w3c.dom.NodeList;
import org.w3c.dom.smil.ElementSequentialTimeContainer;
import org.w3c.dom.smil.ElementTime;
import org.w3c.dom.smil.SMILElement;

public abstract class ElementSequentialTimeContainerImpl extends ElementTimeContainerImpl implements ElementSequentialTimeContainer {
    ElementSequentialTimeContainerImpl(SMILElement sMILElement) {
        super(sMILElement);
    }

    public NodeList getActiveChildrenAt(float f) {
        NodeList timeChildren = getTimeChildren();
        ArrayList arrayList = new ArrayList();
        float f2 = f;
        for (int i = 0; i < timeChildren.getLength(); i++) {
            f2 -= ((ElementTime) timeChildren.item(i)).getDur();
            if (f2 < 0.0f) {
                arrayList.add(timeChildren.item(i));
                return new NodeListImpl(arrayList);
            }
        }
        return new NodeListImpl(arrayList);
    }

    public float getDur() {
        float dur = super.getDur();
        if (dur != 0.0f) {
            return dur;
        }
        NodeList timeChildren = getTimeChildren();
        float f = dur;
        for (int i = 0; i < timeChildren.getLength(); i++) {
            ElementTime elementTime = (ElementTime) timeChildren.item(i);
            if (elementTime.getDur() < 0.0f) {
                return -1.0f;
            }
            f += elementTime.getDur();
        }
        return f;
    }
}
