package com.android.mms.dom;

import java.util.ArrayList;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class NodeListImpl implements NodeList {
    private boolean mDeepSearch;
    private Node mRootNode;
    private ArrayList<Node> mSearchNodes;
    private ArrayList<Node> mStaticNodes;
    private String mTagName;

    public NodeListImpl(ArrayList<Node> arrayList) {
        this.mStaticNodes = arrayList;
    }

    public NodeListImpl(Node node, String str, boolean z) {
        this.mRootNode = node;
        this.mTagName = str;
        this.mDeepSearch = z;
    }

    private void fillList(Node node) {
        if (node == this.mRootNode) {
            this.mSearchNodes = new ArrayList<>();
        } else if (this.mTagName == null || node.getNodeName().equals(this.mTagName)) {
            this.mSearchNodes.add(node);
        }
        for (Node firstChild = node.getFirstChild(); firstChild != null; firstChild = firstChild.getNextSibling()) {
            if (this.mDeepSearch) {
                fillList(firstChild);
            } else if (this.mTagName == null || firstChild.getNodeName().equals(this.mTagName)) {
                this.mSearchNodes.add(firstChild);
            }
        }
    }

    public int getLength() {
        if (this.mStaticNodes != null) {
            return this.mStaticNodes.size();
        }
        fillList(this.mRootNode);
        return this.mSearchNodes.size();
    }

    public Node item(int i) {
        if (this.mStaticNodes == null) {
            fillList(this.mRootNode);
            try {
                return this.mSearchNodes.get(i);
            } catch (IndexOutOfBoundsException e) {
                return null;
            }
        } else {
            try {
                return this.mStaticNodes.get(i);
            } catch (IndexOutOfBoundsException e2) {
                return null;
            }
        }
    }
}
