package com.android.mms.dom.smil;

import org.w3c.dom.DOMException;
import org.w3c.dom.smil.SMILDocument;
import org.w3c.dom.smil.SMILRegionElement;

public class SmilRegionElementImpl extends SmilElementImpl implements SMILRegionElement {
    private static final String BACKGROUND_COLOR_ATTRIBUTE_NAME = "backgroundColor";
    private static final String BOTTOM_ATTRIBUTE_NAME = "bottom";
    private static final boolean DEBUG = false;
    private static final String FILL_ATTRIBUTE = "fill";
    private static final String FIT_ATTRIBUTE_NAME = "fit";
    private static final String HEIGHT_ATTRIBUTE_NAME = "height";
    private static final String HIDDEN_ATTRIBUTE = "hidden";
    private static final String ID_ATTRIBUTE_NAME = "id";
    private static final String LEFT_ATTRIBUTE_NAME = "left";
    private static final boolean LOCAL_LOGV = false;
    private static final String MEET_ATTRIBUTE = "meet";
    private static final String RIGHT_ATTRIBUTE_NAME = "right";
    private static final String SCROLL_ATTRIBUTE = "scroll";
    private static final String SLICE_ATTRIBUTE = "slice";
    private static final String TAG = "SmilRegionElementImpl";
    private static final String TITLE_ATTRIBUTE_NAME = "title";
    private static final String TOP_ATTRIBUTE_NAME = "top";
    private static final String WIDTH_ATTRIBUTE_NAME = "width";
    private static final String Z_INDEX_ATTRIBUTE_NAME = "z-index";

    SmilRegionElementImpl(SmilDocumentImpl smilDocumentImpl, String str) {
        super(smilDocumentImpl, str);
    }

    private int parseRegionLength(String str, boolean z) {
        if (str.endsWith("px")) {
            return Integer.parseInt(str.substring(0, str.indexOf("px")));
        }
        if (!str.endsWith("%")) {
            return Integer.parseInt(str);
        }
        double parseInt = 0.01d * ((double) Integer.parseInt(str.substring(0, str.length() - 1)));
        return (int) Math.round(z ? parseInt * ((double) ((SMILDocument) getOwnerDocument()).getLayout().getRootLayout().getWidth()) : parseInt * ((double) ((SMILDocument) getOwnerDocument()).getLayout().getRootLayout().getHeight()));
    }

    public String getBackgroundColor() {
        return getAttribute(BACKGROUND_COLOR_ATTRIBUTE_NAME);
    }

    public String getFit() {
        String attribute = getAttribute(FIT_ATTRIBUTE_NAME);
        return FILL_ATTRIBUTE.equalsIgnoreCase(attribute) ? FILL_ATTRIBUTE : MEET_ATTRIBUTE.equalsIgnoreCase(attribute) ? MEET_ATTRIBUTE : SCROLL_ATTRIBUTE.equalsIgnoreCase(attribute) ? SCROLL_ATTRIBUTE : SLICE_ATTRIBUTE.equalsIgnoreCase(attribute) ? SLICE_ATTRIBUTE : HIDDEN_ATTRIBUTE;
    }

    public int getHeight() {
        try {
            int parseRegionLength = parseRegionLength(getAttribute(HEIGHT_ATTRIBUTE_NAME), false);
            return parseRegionLength == 0 ? ((SMILDocument) getOwnerDocument()).getLayout().getRootLayout().getHeight() : parseRegionLength;
        } catch (NumberFormatException e) {
            int height = ((SMILDocument) getOwnerDocument()).getLayout().getRootLayout().getHeight();
            try {
                height -= parseRegionLength(getAttribute(TOP_ATTRIBUTE_NAME), false);
            } catch (NumberFormatException e2) {
            }
            try {
                return height - parseRegionLength(getAttribute(BOTTOM_ATTRIBUTE_NAME), false);
            } catch (NumberFormatException e3) {
                return height;
            }
        }
    }

    public String getId() {
        return getAttribute(ID_ATTRIBUTE_NAME);
    }

    public int getLeft() {
        try {
            return parseRegionLength(getAttribute(LEFT_ATTRIBUTE_NAME), true);
        } catch (NumberFormatException e) {
            try {
                return (((SMILDocument) getOwnerDocument()).getLayout().getRootLayout().getWidth() - parseRegionLength(getAttribute(RIGHT_ATTRIBUTE_NAME), true)) - parseRegionLength(getAttribute(WIDTH_ATTRIBUTE_NAME), true);
            } catch (NumberFormatException e2) {
                return 0;
            }
        }
    }

    public String getTitle() {
        return getAttribute("title");
    }

    public int getTop() {
        try {
            return parseRegionLength(getAttribute(TOP_ATTRIBUTE_NAME), false);
        } catch (NumberFormatException e) {
            try {
                return (((SMILDocument) getOwnerDocument()).getLayout().getRootLayout().getHeight() - parseRegionLength(getAttribute(BOTTOM_ATTRIBUTE_NAME), false)) - parseRegionLength(getAttribute(HEIGHT_ATTRIBUTE_NAME), false);
            } catch (NumberFormatException e2) {
                return 0;
            }
        }
    }

    public int getWidth() {
        try {
            int parseRegionLength = parseRegionLength(getAttribute(WIDTH_ATTRIBUTE_NAME), true);
            return parseRegionLength == 0 ? ((SMILDocument) getOwnerDocument()).getLayout().getRootLayout().getWidth() : parseRegionLength;
        } catch (NumberFormatException e) {
            int width = ((SMILDocument) getOwnerDocument()).getLayout().getRootLayout().getWidth();
            try {
                width -= parseRegionLength(getAttribute(LEFT_ATTRIBUTE_NAME), true);
            } catch (NumberFormatException e2) {
            }
            try {
                return width - parseRegionLength(getAttribute(RIGHT_ATTRIBUTE_NAME), true);
            } catch (NumberFormatException e3) {
                return width;
            }
        }
    }

    public int getZIndex() {
        try {
            return Integer.parseInt(getAttribute(Z_INDEX_ATTRIBUTE_NAME));
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    public void setBackgroundColor(String str) throws DOMException {
        setAttribute(BACKGROUND_COLOR_ATTRIBUTE_NAME, str);
    }

    public void setFit(String str) throws DOMException {
        if (str.equalsIgnoreCase(FILL_ATTRIBUTE) || str.equalsIgnoreCase(MEET_ATTRIBUTE) || str.equalsIgnoreCase(SCROLL_ATTRIBUTE) || str.equalsIgnoreCase(SLICE_ATTRIBUTE)) {
            setAttribute(FIT_ATTRIBUTE_NAME, str.toLowerCase());
        } else {
            setAttribute(FIT_ATTRIBUTE_NAME, HIDDEN_ATTRIBUTE);
        }
    }

    public void setHeight(int i) throws DOMException {
        setAttribute(HEIGHT_ATTRIBUTE_NAME, String.valueOf(i) + "px");
    }

    public void setId(String str) throws DOMException {
        setAttribute(ID_ATTRIBUTE_NAME, str);
    }

    public void setLeft(int i) throws DOMException {
        setAttribute(LEFT_ATTRIBUTE_NAME, String.valueOf(i));
    }

    public void setTitle(String str) throws DOMException {
        setAttribute("title", str);
    }

    public void setTop(int i) throws DOMException {
        setAttribute(TOP_ATTRIBUTE_NAME, String.valueOf(i));
    }

    public void setWidth(int i) throws DOMException {
        setAttribute(WIDTH_ATTRIBUTE_NAME, String.valueOf(i) + "px");
    }

    public void setZIndex(int i) throws DOMException {
        if (i > 0) {
            setAttribute(Z_INDEX_ATTRIBUTE_NAME, Integer.toString(i));
        } else {
            setAttribute(Z_INDEX_ATTRIBUTE_NAME, Integer.toString(0));
        }
    }

    public String toString() {
        return super.toString() + ": id=" + getId() + ", width=" + getWidth() + ", height=" + getHeight() + ", left=" + getLeft() + ", top=" + getTop();
    }
}
