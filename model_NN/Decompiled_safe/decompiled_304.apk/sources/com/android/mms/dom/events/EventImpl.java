package com.android.mms.dom.events;

import org.w3c.dom.events.Event;
import org.w3c.dom.events.EventTarget;

public class EventImpl implements Event {
    private boolean mCanBubble;
    private boolean mCancelable;
    private EventTarget mCurrentTarget;
    private short mEventPhase;
    private String mEventType;
    private boolean mInitialized;
    private boolean mPreventDefault;
    private int mSeekTo;
    private boolean mStopPropagation;
    private EventTarget mTarget;
    private final long mTimeStamp = System.currentTimeMillis();

    public boolean getBubbles() {
        return this.mCanBubble;
    }

    public boolean getCancelable() {
        return this.mCancelable;
    }

    public EventTarget getCurrentTarget() {
        return this.mCurrentTarget;
    }

    public short getEventPhase() {
        return this.mEventPhase;
    }

    public int getSeekTo() {
        return this.mSeekTo;
    }

    public EventTarget getTarget() {
        return this.mTarget;
    }

    public long getTimeStamp() {
        return this.mTimeStamp;
    }

    public String getType() {
        return this.mEventType;
    }

    public void initEvent(String str, boolean z, boolean z2) {
        this.mEventType = str;
        this.mCanBubble = z;
        this.mCancelable = z2;
        this.mInitialized = true;
    }

    public void initEvent(String str, boolean z, boolean z2, int i) {
        this.mSeekTo = i;
        initEvent(str, z, z2);
    }

    /* access modifiers changed from: package-private */
    public boolean isInitialized() {
        return this.mInitialized;
    }

    /* access modifiers changed from: package-private */
    public boolean isPreventDefault() {
        return this.mPreventDefault;
    }

    /* access modifiers changed from: package-private */
    public boolean isPropogationStopped() {
        return this.mStopPropagation;
    }

    public void preventDefault() {
        this.mPreventDefault = true;
    }

    /* access modifiers changed from: package-private */
    public void setCurrentTarget(EventTarget eventTarget) {
        this.mCurrentTarget = eventTarget;
    }

    /* access modifiers changed from: package-private */
    public void setEventPhase(short s) {
        this.mEventPhase = s;
    }

    /* access modifiers changed from: package-private */
    public void setTarget(EventTarget eventTarget) {
        this.mTarget = eventTarget;
    }

    public void stopPropagation() {
        this.mStopPropagation = true;
    }
}
