package com.android.mms.dom.smil;

import com.android.mms.dom.DocumentImpl;
import com.android.mms.dom.events.EventImpl;
import com.android.mms.model.SmilHelper;
import com.android.provider.Telephony;
import org.w3c.dom.DOMException;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.events.DocumentEvent;
import org.w3c.dom.events.Event;
import org.w3c.dom.smil.ElementSequentialTimeContainer;
import org.w3c.dom.smil.ElementTime;
import org.w3c.dom.smil.SMILDocument;
import org.w3c.dom.smil.SMILElement;
import org.w3c.dom.smil.SMILLayoutElement;
import org.w3c.dom.smil.TimeList;

public class SmilDocumentImpl extends DocumentImpl implements SMILDocument, DocumentEvent {
    public static final String SMIL_DOCUMENT_END_EVENT = "SimlDocumentEnd";
    public static final String SMIL_DOCUMENT_START_EVENT = "SmilDocumentStart";
    ElementSequentialTimeContainer mSeqTimeContainer;

    public boolean beginElement() {
        return this.mSeqTimeContainer.beginElement();
    }

    public Element createElement(String str) throws DOMException {
        String lowerCase = str.toLowerCase();
        return (lowerCase.equals("text") || lowerCase.equals("img") || lowerCase.equals(SmilHelper.ELEMENT_TAG_VIDEO)) ? new SmilRegionMediaElementImpl(this, lowerCase) : lowerCase.equals(SmilHelper.ELEMENT_TAG_AUDIO) ? new SmilMediaElementImpl(this, lowerCase) : lowerCase.equals("layout") ? new SmilLayoutElementImpl(this, lowerCase) : lowerCase.equals("root-layout") ? new SmilRootLayoutElementImpl(this, lowerCase) : lowerCase.equals("region") ? new SmilRegionElementImpl(this, lowerCase) : lowerCase.equals(SmilHelper.ELEMENT_TAG_REF) ? new SmilRefElementImpl(this, lowerCase) : lowerCase.equals("par") ? new SmilParElementImpl(this, lowerCase) : new SmilElementImpl(this, lowerCase);
    }

    public Event createEvent(String str) throws DOMException {
        if ("Event".equals(str)) {
            return new EventImpl();
        }
        throw new DOMException(9, "Not supported interface");
    }

    public boolean endElement() {
        return this.mSeqTimeContainer.endElement();
    }

    public NodeList getActiveChildrenAt(float f) {
        return this.mSeqTimeContainer.getActiveChildrenAt(f);
    }

    public TimeList getBegin() {
        return this.mSeqTimeContainer.getBegin();
    }

    public SMILElement getBody() {
        SMILElement documentElement = getDocumentElement();
        Node nextSibling = getHead().getNextSibling();
        if (nextSibling == null || !(nextSibling instanceof SMILElement)) {
            nextSibling = createElement(Telephony.TextBasedSmsColumns.BODY);
            documentElement.appendChild(nextSibling);
        }
        this.mSeqTimeContainer = new ElementSequentialTimeContainerImpl((SMILElement) nextSibling) {
            public boolean beginElement() {
                Event createEvent = SmilDocumentImpl.this.createEvent("Event");
                createEvent.initEvent(SmilDocumentImpl.SMIL_DOCUMENT_START_EVENT, false, false);
                SmilDocumentImpl.this.dispatchEvent(createEvent);
                return true;
            }

            public boolean endElement() {
                Event createEvent = SmilDocumentImpl.this.createEvent("Event");
                createEvent.initEvent(SmilDocumentImpl.SMIL_DOCUMENT_END_EVENT, false, false);
                SmilDocumentImpl.this.dispatchEvent(createEvent);
                return true;
            }

            /* access modifiers changed from: package-private */
            public ElementTime getParentElementTime() {
                return null;
            }

            public NodeList getTimeChildren() {
                return SmilDocumentImpl.this.getBody().getElementsByTagName("par");
            }

            public void pauseElement() {
            }

            public void resumeElement() {
            }

            public void seekElement(float f) {
            }
        };
        return (SMILElement) nextSibling;
    }

    public SMILElement getDocumentElement() {
        Element element;
        Node firstChild = getFirstChild();
        if (firstChild == null || !(firstChild instanceof SMILElement)) {
            Element createElement = createElement("smil");
            appendChild(createElement);
            element = createElement;
        } else {
            element = firstChild;
        }
        return (SMILElement) element;
    }

    public float getDur() {
        return this.mSeqTimeContainer.getDur();
    }

    public TimeList getEnd() {
        return this.mSeqTimeContainer.getEnd();
    }

    public short getFill() {
        return this.mSeqTimeContainer.getFill();
    }

    public short getFillDefault() {
        return this.mSeqTimeContainer.getFillDefault();
    }

    public SMILElement getHead() {
        Element element;
        SMILElement documentElement = getDocumentElement();
        Node firstChild = documentElement.getFirstChild();
        if (firstChild == null || !(firstChild instanceof SMILElement)) {
            Element createElement = createElement("head");
            documentElement.appendChild(createElement);
            element = createElement;
        } else {
            element = firstChild;
        }
        return (SMILElement) element;
    }

    public SMILLayoutElement getLayout() {
        SmilLayoutElementImpl smilLayoutElementImpl;
        SMILElement head = getHead();
        Node firstChild = head.getFirstChild();
        while (firstChild != null && !(firstChild instanceof SMILLayoutElement)) {
            firstChild = firstChild.getNextSibling();
        }
        if (firstChild == null) {
            SmilLayoutElementImpl smilLayoutElementImpl2 = new SmilLayoutElementImpl(this, "layout");
            head.appendChild(smilLayoutElementImpl2);
            smilLayoutElementImpl = smilLayoutElementImpl2;
        } else {
            smilLayoutElementImpl = firstChild;
        }
        return (SMILLayoutElement) smilLayoutElementImpl;
    }

    public float getRepeatCount() {
        return this.mSeqTimeContainer.getRepeatCount();
    }

    public float getRepeatDur() {
        return this.mSeqTimeContainer.getRepeatDur();
    }

    public short getRestart() {
        return this.mSeqTimeContainer.getRestart();
    }

    public NodeList getTimeChildren() {
        return this.mSeqTimeContainer.getTimeChildren();
    }

    public void pauseElement() {
        this.mSeqTimeContainer.pauseElement();
    }

    public void resumeElement() {
        this.mSeqTimeContainer.resumeElement();
    }

    public void seekElement(float f) {
        this.mSeqTimeContainer.seekElement(f);
    }

    public void setBegin(TimeList timeList) throws DOMException {
        this.mSeqTimeContainer.setBegin(timeList);
    }

    public void setDur(float f) throws DOMException {
        this.mSeqTimeContainer.setDur(f);
    }

    public void setEnd(TimeList timeList) throws DOMException {
        this.mSeqTimeContainer.setEnd(timeList);
    }

    public void setFill(short s) throws DOMException {
        this.mSeqTimeContainer.setFill(s);
    }

    public void setFillDefault(short s) throws DOMException {
        this.mSeqTimeContainer.setFillDefault(s);
    }

    public void setRepeatCount(float f) throws DOMException {
        this.mSeqTimeContainer.setRepeatCount(f);
    }

    public void setRepeatDur(float f) throws DOMException {
        this.mSeqTimeContainer.setRepeatDur(f);
    }

    public void setRestart(short s) throws DOMException {
        this.mSeqTimeContainer.setRestart(s);
    }
}
