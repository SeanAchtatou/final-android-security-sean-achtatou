package com.android.mms.dom.smil;

import java.util.ArrayList;
import org.w3c.dom.DOMException;
import org.w3c.dom.NodeList;
import org.w3c.dom.events.DocumentEvent;
import org.w3c.dom.events.Event;
import org.w3c.dom.smil.ElementParallelTimeContainer;
import org.w3c.dom.smil.ElementTime;
import org.w3c.dom.smil.SMILParElement;
import org.w3c.dom.smil.TimeList;

public class SmilParElementImpl extends SmilElementImpl implements SMILParElement {
    public static final String SMIL_SLIDE_END_EVENT = "SmilSlideEnd";
    public static final String SMIL_SLIDE_START_EVENT = "SmilSlideStart";
    ElementParallelTimeContainer mParTimeContainer = new ElementParallelTimeContainerImpl(this) {
        public boolean beginElement() {
            Event createEvent = ((DocumentEvent) SmilParElementImpl.this.getOwnerDocument()).createEvent("Event");
            createEvent.initEvent(SmilParElementImpl.SMIL_SLIDE_START_EVENT, false, false);
            SmilParElementImpl.this.dispatchEvent(createEvent);
            return true;
        }

        public boolean endElement() {
            Event createEvent = ((DocumentEvent) SmilParElementImpl.this.getOwnerDocument()).createEvent("Event");
            createEvent.initEvent(SmilParElementImpl.SMIL_SLIDE_END_EVENT, false, false);
            SmilParElementImpl.this.dispatchEvent(createEvent);
            return true;
        }

        public TimeList getBegin() {
            TimeList begin = super.getBegin();
            if (begin.getLength() <= 1) {
                return begin;
            }
            ArrayList arrayList = new ArrayList();
            arrayList.add(begin.item(0));
            return new TimeListImpl(arrayList);
        }

        /* access modifiers changed from: package-private */
        public ElementTime getParentElementTime() {
            return ((SmilDocumentImpl) this.mSmilElement.getOwnerDocument()).mSeqTimeContainer;
        }

        public NodeList getTimeChildren() {
            return SmilParElementImpl.this.getChildNodes();
        }

        public void pauseElement() {
        }

        public void resumeElement() {
        }

        public void seekElement(float f) {
        }
    };

    SmilParElementImpl(SmilDocumentImpl smilDocumentImpl, String str) {
        super(smilDocumentImpl, str.toUpperCase());
    }

    public boolean beginElement() {
        return this.mParTimeContainer.beginElement();
    }

    public boolean endElement() {
        return this.mParTimeContainer.endElement();
    }

    public NodeList getActiveChildrenAt(float f) {
        return this.mParTimeContainer.getActiveChildrenAt(f);
    }

    public TimeList getBegin() {
        return this.mParTimeContainer.getBegin();
    }

    /* access modifiers changed from: package-private */
    public int getBeginConstraints() {
        return 2;
    }

    public float getDur() {
        return this.mParTimeContainer.getDur();
    }

    public TimeList getEnd() {
        return this.mParTimeContainer.getEnd();
    }

    public String getEndSync() {
        return this.mParTimeContainer.getEndSync();
    }

    public short getFill() {
        return this.mParTimeContainer.getFill();
    }

    public short getFillDefault() {
        return this.mParTimeContainer.getFillDefault();
    }

    public float getImplicitDuration() {
        return this.mParTimeContainer.getImplicitDuration();
    }

    public float getRepeatCount() {
        return this.mParTimeContainer.getRepeatCount();
    }

    public float getRepeatDur() {
        return this.mParTimeContainer.getRepeatDur();
    }

    public short getRestart() {
        return this.mParTimeContainer.getRestart();
    }

    public NodeList getTimeChildren() {
        return this.mParTimeContainer.getTimeChildren();
    }

    public void pauseElement() {
        this.mParTimeContainer.pauseElement();
    }

    public void resumeElement() {
        this.mParTimeContainer.resumeElement();
    }

    public void seekElement(float f) {
        this.mParTimeContainer.seekElement(f);
    }

    public void setBegin(TimeList timeList) throws DOMException {
        this.mParTimeContainer.setBegin(timeList);
    }

    public void setDur(float f) throws DOMException {
        this.mParTimeContainer.setDur(f);
    }

    public void setEnd(TimeList timeList) throws DOMException {
        this.mParTimeContainer.setEnd(timeList);
    }

    public void setEndSync(String str) throws DOMException {
        this.mParTimeContainer.setEndSync(str);
    }

    public void setFill(short s) throws DOMException {
        this.mParTimeContainer.setFill(s);
    }

    public void setFillDefault(short s) throws DOMException {
        this.mParTimeContainer.setFillDefault(s);
    }

    public void setRepeatCount(float f) throws DOMException {
        this.mParTimeContainer.setRepeatCount(f);
    }

    public void setRepeatDur(float f) throws DOMException {
        this.mParTimeContainer.setRepeatDur(f);
    }

    public void setRestart(short s) throws DOMException {
        this.mParTimeContainer.setRestart(s);
    }
}
