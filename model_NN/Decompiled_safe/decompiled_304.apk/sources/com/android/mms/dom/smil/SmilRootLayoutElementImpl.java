package com.android.mms.dom.smil;

import org.w3c.dom.DOMException;
import org.w3c.dom.smil.SMILRootLayoutElement;

public class SmilRootLayoutElementImpl extends SmilElementImpl implements SMILRootLayoutElement {
    private static final String BACKGROUND_COLOR_ATTRIBUTE_NAME = "backgroundColor";
    private static final String HEIGHT_ATTRIBUTE_NAME = "height";
    private static final String TITLE_ATTRIBUTE_NAME = "title";
    private static final String WIDTH_ATTRIBUTE_NAME = "width";

    SmilRootLayoutElementImpl(SmilDocumentImpl smilDocumentImpl, String str) {
        super(smilDocumentImpl, str);
    }

    private int parseAbsoluteLength(String str) {
        try {
            return Integer.parseInt(str.endsWith("px") ? str.substring(0, str.indexOf("px")) : str);
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    public String getBackgroundColor() {
        return getAttribute(BACKGROUND_COLOR_ATTRIBUTE_NAME);
    }

    public int getHeight() {
        return parseAbsoluteLength(getAttribute(HEIGHT_ATTRIBUTE_NAME));
    }

    public String getTitle() {
        return getAttribute("title");
    }

    public int getWidth() {
        return parseAbsoluteLength(getAttribute(WIDTH_ATTRIBUTE_NAME));
    }

    public void setBackgroundColor(String str) throws DOMException {
        setAttribute(BACKGROUND_COLOR_ATTRIBUTE_NAME, str);
    }

    public void setHeight(int i) throws DOMException {
        setAttribute(HEIGHT_ATTRIBUTE_NAME, String.valueOf(i) + "px");
    }

    public void setTitle(String str) throws DOMException {
        setAttribute("title", str);
    }

    public void setWidth(int i) throws DOMException {
        setAttribute(WIDTH_ATTRIBUTE_NAME, String.valueOf(i) + "px");
    }
}
