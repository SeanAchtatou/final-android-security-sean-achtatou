package com.android.mms.dom;

import org.w3c.dom.Attr;
import org.w3c.dom.CDATASection;
import org.w3c.dom.Comment;
import org.w3c.dom.DOMException;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.DocumentType;
import org.w3c.dom.Element;
import org.w3c.dom.EntityReference;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.ProcessingInstruction;
import org.w3c.dom.Text;

public abstract class DocumentImpl extends NodeImpl implements Document {
    public DocumentImpl() {
        super(null);
    }

    public Attr createAttribute(String str) throws DOMException {
        return new AttrImpl(this, str);
    }

    public Attr createAttributeNS(String str, String str2) throws DOMException {
        return null;
    }

    public CDATASection createCDATASection(String str) throws DOMException {
        return null;
    }

    public Comment createComment(String str) {
        return null;
    }

    public DocumentFragment createDocumentFragment() {
        return null;
    }

    public abstract Element createElement(String str) throws DOMException;

    public Element createElementNS(String str, String str2) throws DOMException {
        return null;
    }

    public EntityReference createEntityReference(String str) throws DOMException {
        return null;
    }

    public ProcessingInstruction createProcessingInstruction(String str, String str2) throws DOMException {
        return null;
    }

    public Text createTextNode(String str) {
        return null;
    }

    public DocumentType getDoctype() {
        return null;
    }

    public abstract Element getDocumentElement();

    public Element getElementById(String str) {
        return null;
    }

    public NodeList getElementsByTagName(String str) {
        return null;
    }

    public NodeList getElementsByTagNameNS(String str, String str2) {
        return null;
    }

    public DOMImplementation getImplementation() {
        return null;
    }

    public String getNodeName() {
        return "#document";
    }

    public short getNodeType() {
        return 9;
    }

    public Node importNode(Node node, boolean z) throws DOMException {
        return null;
    }
}
