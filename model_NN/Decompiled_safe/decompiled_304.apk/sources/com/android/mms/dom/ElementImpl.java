package com.android.mms.dom;

import org.w3c.dom.Attr;
import org.w3c.dom.DOMException;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;

public class ElementImpl extends NodeImpl implements Element {
    private NamedNodeMap mAttributes = new NamedNodeMapImpl();
    private String mTagName;

    protected ElementImpl(DocumentImpl documentImpl, String str) {
        super(documentImpl);
        this.mTagName = str;
    }

    public String getAttribute(String str) {
        Attr attributeNode = getAttributeNode(str);
        return attributeNode != null ? attributeNode.getValue() : "";
    }

    public String getAttributeNS(String str, String str2) {
        return null;
    }

    public Attr getAttributeNode(String str) {
        return (Attr) this.mAttributes.getNamedItem(str);
    }

    public Attr getAttributeNodeNS(String str, String str2) {
        return null;
    }

    public NamedNodeMap getAttributes() {
        return this.mAttributes;
    }

    public NodeList getElementsByTagName(String str) {
        return new NodeListImpl(this, str, true);
    }

    public NodeList getElementsByTagNameNS(String str, String str2) {
        return null;
    }

    public String getNodeName() {
        return this.mTagName;
    }

    public short getNodeType() {
        return 1;
    }

    public String getTagName() {
        return this.mTagName;
    }

    public boolean hasAttribute(String str) {
        return getAttributeNode(str) != null;
    }

    public boolean hasAttributeNS(String str, String str2) {
        return false;
    }

    public boolean hasAttributes() {
        return this.mAttributes.getLength() > 0;
    }

    public void removeAttribute(String str) throws DOMException {
    }

    public void removeAttributeNS(String str, String str2) throws DOMException {
    }

    public Attr removeAttributeNode(Attr attr) throws DOMException {
        return null;
    }

    public void setAttribute(String str, String str2) throws DOMException {
        Attr attributeNode = getAttributeNode(str);
        if (attributeNode == null) {
            attributeNode = this.mOwnerDocument.createAttribute(str);
        }
        attributeNode.setNodeValue(str2);
        this.mAttributes.setNamedItem(attributeNode);
    }

    public void setAttributeNS(String str, String str2, String str3) throws DOMException {
    }

    public Attr setAttributeNode(Attr attr) throws DOMException {
        return null;
    }

    public Attr setAttributeNodeNS(Attr attr) throws DOMException {
        return null;
    }
}
