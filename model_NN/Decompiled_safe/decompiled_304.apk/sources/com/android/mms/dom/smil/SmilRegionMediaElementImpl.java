package com.android.mms.dom.smil;

import org.w3c.dom.NodeList;
import org.w3c.dom.smil.SMILDocument;
import org.w3c.dom.smil.SMILRegionElement;
import org.w3c.dom.smil.SMILRegionMediaElement;

public class SmilRegionMediaElementImpl extends SmilMediaElementImpl implements SMILRegionMediaElement {
    private SMILRegionElement mRegion;

    SmilRegionMediaElementImpl(SmilDocumentImpl smilDocumentImpl, String str) {
        super(smilDocumentImpl, str);
    }

    public SMILRegionElement getRegion() {
        if (this.mRegion == null) {
            NodeList elementsByTagName = ((SMILDocument) getOwnerDocument()).getLayout().getElementsByTagName("region");
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= elementsByTagName.getLength()) {
                    break;
                }
                SMILRegionElement sMILRegionElement = (SMILRegionElement) elementsByTagName.item(i2);
                if (sMILRegionElement.getId().equals(getAttribute("region"))) {
                    this.mRegion = sMILRegionElement;
                }
                i = i2 + 1;
            }
        }
        return this.mRegion;
    }

    public void setRegion(SMILRegionElement sMILRegionElement) {
        setAttribute("region", sMILRegionElement.getId());
        this.mRegion = sMILRegionElement;
    }
}
