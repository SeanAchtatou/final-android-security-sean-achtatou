package com.android.mms.dom.smil;

import com.android.mms.dom.NodeListImpl;
import java.util.ArrayList;
import org.w3c.dom.DOMException;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.smil.ElementParallelTimeContainer;
import org.w3c.dom.smil.ElementTime;
import org.w3c.dom.smil.SMILElement;
import org.w3c.dom.smil.Time;
import org.w3c.dom.smil.TimeList;

public abstract class ElementParallelTimeContainerImpl extends ElementTimeContainerImpl implements ElementParallelTimeContainer {
    private static final String ENDSYNC_ALL = "all";
    private static final String ENDSYNC_ATTRIBUTE_NAME = "endsync";
    private static final String ENDSYNC_FIRST = "first";
    private static final String ENDSYNC_LAST = "last";
    private static final String ENDSYNC_MEDIA = "media";

    ElementParallelTimeContainerImpl(SMILElement sMILElement) {
        super(sMILElement);
    }

    public NodeList getActiveChildrenAt(float f) {
        ArrayList arrayList = new ArrayList();
        NodeList timeChildren = getTimeChildren();
        int length = timeChildren.getLength();
        for (int i = 0; i < length; i++) {
            ElementTime elementTime = (ElementTime) timeChildren.item(i);
            TimeList begin = elementTime.getBegin();
            int length2 = begin.getLength();
            double d = 0.0d;
            boolean z = false;
            for (int i2 = 0; i2 < length2; i2++) {
                Time item = begin.item(i2);
                if (item.getResolved()) {
                    double resolvedOffset = item.getResolvedOffset() * 1000.0d;
                    if (resolvedOffset <= ((double) f) && resolvedOffset >= d) {
                        z = true;
                        d = resolvedOffset;
                    }
                }
            }
            TimeList end = elementTime.getEnd();
            int length3 = end.getLength();
            boolean z2 = z;
            double d2 = d;
            for (int i3 = 0; i3 < length3; i3++) {
                Time item2 = end.item(i3);
                if (item2.getResolved()) {
                    double resolvedOffset2 = item2.getResolvedOffset() * 1000.0d;
                    if (resolvedOffset2 <= ((double) f) && resolvedOffset2 >= d2) {
                        z2 = false;
                        d2 = resolvedOffset2;
                    }
                }
            }
            if (z2) {
                arrayList.add((Node) elementTime);
            }
        }
        return new NodeListImpl(arrayList);
    }

    public float getDur() {
        float dur = super.getDur();
        return dur == 0.0f ? getImplicitDuration() : dur;
    }

    public String getEndSync() {
        String attribute = this.mSmilElement.getAttribute(ENDSYNC_ATTRIBUTE_NAME);
        if (attribute == null || attribute.length() == 0) {
            setEndSync(ENDSYNC_LAST);
            return ENDSYNC_LAST;
        } else if (ENDSYNC_FIRST.equals(attribute) || ENDSYNC_LAST.equals(attribute) || ENDSYNC_ALL.equals(attribute) || ENDSYNC_MEDIA.equals(attribute)) {
            return attribute;
        } else {
            setEndSync(ENDSYNC_LAST);
            return ENDSYNC_LAST;
        }
    }

    public float getImplicitDuration() {
        if (!ENDSYNC_LAST.equals(getEndSync())) {
            return -1.0f;
        }
        NodeList timeChildren = getTimeChildren();
        int i = 0;
        float f = -1.0f;
        while (i < timeChildren.getLength()) {
            TimeList end = ((ElementTime) timeChildren.item(i)).getEnd();
            float f2 = f;
            for (int i2 = 0; i2 < end.getLength(); i2++) {
                Time item = end.item(i2);
                if (item.getTimeType() == 0) {
                    return -1.0f;
                }
                if (item.getResolved()) {
                    float resolvedOffset = (float) item.getResolvedOffset();
                    if (resolvedOffset > f2) {
                        f2 = resolvedOffset;
                    }
                }
            }
            i++;
            f = f2;
        }
        return f;
    }

    public void setEndSync(String str) throws DOMException {
        if (ENDSYNC_FIRST.equals(str) || ENDSYNC_LAST.equals(str) || ENDSYNC_ALL.equals(str) || ENDSYNC_MEDIA.equals(str)) {
            this.mSmilElement.setAttribute(ENDSYNC_ATTRIBUTE_NAME, str);
            return;
        }
        throw new DOMException(9, "Unsupported endsync value" + str);
    }
}
