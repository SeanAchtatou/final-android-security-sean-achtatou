package com.android.mms.dom.smil;

import android.util.Log;
import com.android.mms.model.SmilHelper;
import org.w3c.dom.DOMException;
import org.w3c.dom.events.DocumentEvent;
import org.w3c.dom.events.Event;
import org.w3c.dom.smil.ElementTime;
import org.w3c.dom.smil.SMILMediaElement;
import org.w3c.dom.smil.TimeList;

public class SmilMediaElementImpl extends SmilElementImpl implements SMILMediaElement {
    private static final boolean DEBUG = false;
    private static final boolean LOCAL_LOGV = false;
    public static final String SMIL_MEDIA_END_EVENT = "SmilMediaEnd";
    public static final String SMIL_MEDIA_PAUSE_EVENT = "SmilMediaPause";
    public static final String SMIL_MEDIA_SEEK_EVENT = "SmilMediaSeek";
    public static final String SMIL_MEDIA_START_EVENT = "SmilMediaStart";
    private static final String TAG = "Mms:smil";
    ElementTime mElementTime = new ElementTimeImpl(this) {
        private Event createEvent(String str) {
            Event createEvent = ((DocumentEvent) SmilMediaElementImpl.this.getOwnerDocument()).createEvent("Event");
            createEvent.initEvent(str, false, false);
            return createEvent;
        }

        private Event createEvent(String str, int i) {
            Event createEvent = ((DocumentEvent) SmilMediaElementImpl.this.getOwnerDocument()).createEvent("Event");
            createEvent.initEvent(str, false, false, i);
            return createEvent;
        }

        public boolean beginElement() {
            SmilMediaElementImpl.this.dispatchEvent(createEvent(SmilMediaElementImpl.SMIL_MEDIA_START_EVENT));
            return true;
        }

        public boolean endElement() {
            SmilMediaElementImpl.this.dispatchEvent(createEvent(SmilMediaElementImpl.SMIL_MEDIA_END_EVENT));
            return true;
        }

        public float getDur() {
            float dur = super.getDur();
            if (dur != 0.0f) {
                return dur;
            }
            String tagName = SmilMediaElementImpl.this.getTagName();
            if (tagName.equals(SmilHelper.ELEMENT_TAG_VIDEO) || tagName.equals(SmilHelper.ELEMENT_TAG_AUDIO)) {
                return -1.0f;
            }
            if (tagName.equals("text") || tagName.equals("img")) {
                return 0.0f;
            }
            Log.w(SmilMediaElementImpl.TAG, "Unknown media type");
            return dur;
        }

        /* access modifiers changed from: package-private */
        public ElementTime getParentElementTime() {
            return ((SmilParElementImpl) this.mSmilElement.getParentNode()).mParTimeContainer;
        }

        public void pauseElement() {
            SmilMediaElementImpl.this.dispatchEvent(createEvent(SmilMediaElementImpl.SMIL_MEDIA_PAUSE_EVENT));
        }

        public void resumeElement() {
            SmilMediaElementImpl.this.dispatchEvent(createEvent(SmilMediaElementImpl.SMIL_MEDIA_START_EVENT));
        }

        public void seekElement(float f) {
            SmilMediaElementImpl.this.dispatchEvent(createEvent(SmilMediaElementImpl.SMIL_MEDIA_SEEK_EVENT, (int) f));
        }
    };

    SmilMediaElementImpl(SmilDocumentImpl smilDocumentImpl, String str) {
        super(smilDocumentImpl, str);
    }

    public boolean beginElement() {
        return this.mElementTime.beginElement();
    }

    public boolean endElement() {
        return this.mElementTime.endElement();
    }

    public String getAbstractAttr() {
        return getAttribute("abstract");
    }

    public String getAlt() {
        return getAttribute("alt");
    }

    public String getAuthor() {
        return getAttribute("author");
    }

    public TimeList getBegin() {
        return this.mElementTime.getBegin();
    }

    public String getClipBegin() {
        return getAttribute("clipBegin");
    }

    public String getClipEnd() {
        return getAttribute("clipEnd");
    }

    public String getCopyright() {
        return getAttribute("copyright");
    }

    public float getDur() {
        return this.mElementTime.getDur();
    }

    public TimeList getEnd() {
        return this.mElementTime.getEnd();
    }

    public short getFill() {
        return this.mElementTime.getFill();
    }

    public short getFillDefault() {
        return this.mElementTime.getFillDefault();
    }

    public String getLongdesc() {
        return getAttribute("longdesc");
    }

    public String getPort() {
        return getAttribute("port");
    }

    public String getReadIndex() {
        return getAttribute("readIndex");
    }

    public float getRepeatCount() {
        return this.mElementTime.getRepeatCount();
    }

    public float getRepeatDur() {
        return this.mElementTime.getRepeatDur();
    }

    public short getRestart() {
        return this.mElementTime.getRestart();
    }

    public String getRtpformat() {
        return getAttribute("rtpformat");
    }

    public String getSrc() {
        return getAttribute("src");
    }

    public String getStripRepeat() {
        return getAttribute("stripRepeat");
    }

    public String getTitle() {
        return getAttribute("title");
    }

    public String getTransport() {
        return getAttribute("transport");
    }

    public String getType() {
        return getAttribute("type");
    }

    public void pauseElement() {
        this.mElementTime.pauseElement();
    }

    public void resumeElement() {
        this.mElementTime.resumeElement();
    }

    public void seekElement(float f) {
        this.mElementTime.seekElement(f);
    }

    public void setAbstractAttr(String str) throws DOMException {
        setAttribute("abstract", str);
    }

    public void setAlt(String str) throws DOMException {
        setAttribute("alt", str);
    }

    public void setAuthor(String str) throws DOMException {
        setAttribute("author", str);
    }

    public void setBegin(TimeList timeList) throws DOMException {
        this.mElementTime.setBegin(timeList);
    }

    public void setClipBegin(String str) throws DOMException {
        setAttribute("clipBegin", str);
    }

    public void setClipEnd(String str) throws DOMException {
        setAttribute("clipEnd", str);
    }

    public void setCopyright(String str) throws DOMException {
        setAttribute("copyright", str);
    }

    public void setDur(float f) throws DOMException {
        this.mElementTime.setDur(f);
    }

    public void setEnd(TimeList timeList) throws DOMException {
        this.mElementTime.setEnd(timeList);
    }

    public void setFill(short s) throws DOMException {
        this.mElementTime.setFill(s);
    }

    public void setFillDefault(short s) throws DOMException {
        this.mElementTime.setFillDefault(s);
    }

    public void setLongdesc(String str) throws DOMException {
        setAttribute("longdesc", str);
    }

    public void setPort(String str) throws DOMException {
        setAttribute("port", str);
    }

    public void setReadIndex(String str) throws DOMException {
        setAttribute("readIndex", str);
    }

    public void setRepeatCount(float f) throws DOMException {
        this.mElementTime.setRepeatCount(f);
    }

    public void setRepeatDur(float f) throws DOMException {
        this.mElementTime.setRepeatDur(f);
    }

    public void setRestart(short s) throws DOMException {
        this.mElementTime.setRestart(s);
    }

    public void setRtpformat(String str) throws DOMException {
        setAttribute("rtpformat", str);
    }

    public void setSrc(String str) throws DOMException {
        setAttribute("src", str);
    }

    public void setStripRepeat(String str) throws DOMException {
        setAttribute("stripRepeat", str);
    }

    public void setTitle(String str) throws DOMException {
        setAttribute("title", str);
    }

    public void setTransport(String str) throws DOMException {
        setAttribute("transport", str);
    }

    public void setType(String str) throws DOMException {
        setAttribute("type", str);
    }
}
