package com.android.mms.dom.smil;

import java.util.ArrayList;
import org.w3c.dom.smil.Time;
import org.w3c.dom.smil.TimeList;

public class TimeListImpl implements TimeList {
    private final ArrayList<Time> mTimes;

    TimeListImpl(ArrayList<Time> arrayList) {
        this.mTimes = arrayList;
    }

    public int getLength() {
        return this.mTimes.size();
    }

    public Time item(int i) {
        try {
            return this.mTimes.get(i);
        } catch (IndexOutOfBoundsException e) {
            return null;
        }
    }
}
