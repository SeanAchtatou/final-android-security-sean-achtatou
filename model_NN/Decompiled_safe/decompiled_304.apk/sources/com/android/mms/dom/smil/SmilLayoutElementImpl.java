package com.android.mms.dom.smil;

import com.android.mms.layout.LayoutManager;
import org.w3c.dom.NodeList;
import org.w3c.dom.smil.SMILLayoutElement;
import org.w3c.dom.smil.SMILRootLayoutElement;

public class SmilLayoutElementImpl extends SmilElementImpl implements SMILLayoutElement {
    SmilLayoutElementImpl(SmilDocumentImpl smilDocumentImpl, String str) {
        super(smilDocumentImpl, str);
    }

    public NodeList getRegions() {
        return getElementsByTagName("region");
    }

    public boolean getResolved() {
        return false;
    }

    public SMILRootLayoutElement getRootLayout() {
        NodeList childNodes = getChildNodes();
        SMILRootLayoutElement sMILRootLayoutElement = null;
        int length = childNodes.getLength();
        for (int i = 0; i < length; i++) {
            if (childNodes.item(i).getNodeName().equals("root-layout")) {
                sMILRootLayoutElement = (SMILRootLayoutElement) childNodes.item(i);
            }
        }
        if (sMILRootLayoutElement != null) {
            return sMILRootLayoutElement;
        }
        SMILRootLayoutElement sMILRootLayoutElement2 = (SMILRootLayoutElement) getOwnerDocument().createElement("root-layout");
        sMILRootLayoutElement2.setWidth(LayoutManager.getInstance().getLayoutParameters().getWidth());
        sMILRootLayoutElement2.setHeight(LayoutManager.getInstance().getLayoutParameters().getHeight());
        appendChild(sMILRootLayoutElement2);
        return sMILRootLayoutElement2;
    }

    public String getType() {
        return getAttribute("type");
    }
}
