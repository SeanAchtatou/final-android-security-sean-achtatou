package com.android.mms.dom;

import com.android.mms.dom.events.EventTargetImpl;
import java.util.NoSuchElementException;
import java.util.Vector;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.events.Event;
import org.w3c.dom.events.EventException;
import org.w3c.dom.events.EventListener;
import org.w3c.dom.events.EventTarget;

public abstract class NodeImpl implements Node, EventTarget {
    private final Vector<Node> mChildNodes = new Vector<>();
    private final EventTarget mEventTarget = new EventTargetImpl(this);
    DocumentImpl mOwnerDocument;
    private Node mParentNode;

    protected NodeImpl(DocumentImpl documentImpl) {
        this.mOwnerDocument = documentImpl;
    }

    private void setParentNode(Node node) {
        this.mParentNode = node;
    }

    public void addEventListener(String str, EventListener eventListener, boolean z) {
        this.mEventTarget.addEventListener(str, eventListener, z);
    }

    public Node appendChild(Node node) throws DOMException {
        ((NodeImpl) node).setParentNode(this);
        this.mChildNodes.remove(node);
        this.mChildNodes.add(node);
        return node;
    }

    public Node cloneNode(boolean z) {
        return null;
    }

    public boolean dispatchEvent(Event event) throws EventException {
        return this.mEventTarget.dispatchEvent(event);
    }

    public NamedNodeMap getAttributes() {
        return null;
    }

    public NodeList getChildNodes() {
        return new NodeListImpl(this, null, false);
    }

    public Node getFirstChild() {
        try {
            return this.mChildNodes.firstElement();
        } catch (NoSuchElementException e) {
            return null;
        }
    }

    public Node getLastChild() {
        try {
            return this.mChildNodes.lastElement();
        } catch (NoSuchElementException e) {
            return null;
        }
    }

    public String getLocalName() {
        return null;
    }

    public String getNamespaceURI() {
        return null;
    }

    public Node getNextSibling() {
        if (this.mParentNode == null || this == this.mParentNode.getLastChild()) {
            return null;
        }
        Vector<Node> vector = ((NodeImpl) this.mParentNode).mChildNodes;
        return vector.elementAt(vector.indexOf(this) + 1);
    }

    public abstract String getNodeName();

    public abstract short getNodeType();

    public String getNodeValue() throws DOMException {
        return null;
    }

    public Document getOwnerDocument() {
        return this.mOwnerDocument;
    }

    public Node getParentNode() {
        return this.mParentNode;
    }

    public String getPrefix() {
        return null;
    }

    public Node getPreviousSibling() {
        if (this.mParentNode == null || this == this.mParentNode.getFirstChild()) {
            return null;
        }
        Vector<Node> vector = ((NodeImpl) this.mParentNode).mChildNodes;
        return vector.elementAt(vector.indexOf(this) - 1);
    }

    public boolean hasAttributes() {
        return false;
    }

    public boolean hasChildNodes() {
        return !this.mChildNodes.isEmpty();
    }

    public Node insertBefore(Node node, Node node2) throws DOMException {
        return null;
    }

    public boolean isSupported(String str, String str2) {
        return false;
    }

    public void normalize() {
    }

    public Node removeChild(Node node) throws DOMException {
        if (this.mChildNodes.contains(node)) {
            this.mChildNodes.remove(node);
            ((NodeImpl) node).setParentNode(null);
            return null;
        }
        throw new DOMException(8, "Child does not exist");
    }

    public void removeEventListener(String str, EventListener eventListener, boolean z) {
        this.mEventTarget.removeEventListener(str, eventListener, z);
    }

    public Node replaceChild(Node node, Node node2) throws DOMException {
        if (this.mChildNodes.contains(node2)) {
            try {
                this.mChildNodes.remove(node);
            } catch (DOMException e) {
            }
            this.mChildNodes.setElementAt(node, this.mChildNodes.indexOf(node2));
            ((NodeImpl) node).setParentNode(this);
            ((NodeImpl) node2).setParentNode(null);
            return node2;
        }
        throw new DOMException(8, "Old child does not exist");
    }

    public void setNodeValue(String str) throws DOMException {
    }

    public void setPrefix(String str) throws DOMException {
    }
}
