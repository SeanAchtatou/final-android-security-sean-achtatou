package com.android.mms.dom;

import org.w3c.dom.Attr;
import org.w3c.dom.DOMException;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class AttrImpl extends NodeImpl implements Attr {
    private String mName;
    private String mValue;

    protected AttrImpl(DocumentImpl documentImpl, String str) {
        super(documentImpl);
        this.mName = str;
    }

    public String getName() {
        return this.mName;
    }

    public Node getNextSibling() {
        return null;
    }

    public String getNodeName() {
        return this.mName;
    }

    public short getNodeType() {
        return 2;
    }

    public Element getOwnerElement() {
        return null;
    }

    public Node getParentNode() {
        return null;
    }

    public Node getPreviousSibling() {
        return null;
    }

    public boolean getSpecified() {
        return this.mValue != null;
    }

    public String getValue() {
        return this.mValue;
    }

    public void setNodeValue(String str) throws DOMException {
        setValue(str);
    }

    public void setValue(String str) throws DOMException {
        this.mValue = str;
    }
}
