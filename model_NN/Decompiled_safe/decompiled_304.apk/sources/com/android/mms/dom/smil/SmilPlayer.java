package com.android.mms.dom.smil;

import android.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import org.w3c.dom.NodeList;
import org.w3c.dom.events.DocumentEvent;
import org.w3c.dom.events.Event;
import org.w3c.dom.events.EventTarget;
import org.w3c.dom.smil.ElementParallelTimeContainer;
import org.w3c.dom.smil.ElementSequentialTimeContainer;
import org.w3c.dom.smil.ElementTime;
import org.w3c.dom.smil.Time;
import org.w3c.dom.smil.TimeList;

public class SmilPlayer implements Runnable {
    private static final boolean DEBUG = false;
    private static final boolean LOCAL_LOGV = false;
    public static final String MEDIA_TIME_UPDATED_EVENT = "mediaTimeUpdated";
    private static final String TAG = "Mms/smil";
    private static final int TIMESLICE = 200;
    private static SmilPlayer sPlayer;
    private static final Comparator<TimelineEntry> sTimelineEntryComparator = new Comparator<TimelineEntry>() {
        public int compare(TimelineEntry timelineEntry, TimelineEntry timelineEntry2) {
            return Double.compare(timelineEntry.getOffsetTime(), timelineEntry2.getOffsetTime());
        }
    };
    private SmilPlayerAction mAction = SmilPlayerAction.NO_ACTIVE_ACTION;
    private ArrayList<ElementTime> mActiveElements;
    private ArrayList<TimelineEntry> mAllEntries;
    private int mCurrentElement;
    private int mCurrentSlide;
    private long mCurrentTime;
    private Event mMediaTimeUpdatedEvent;
    private Thread mPlayerThread;
    private ElementTime mRoot;
    private SmilPlayerState mState = SmilPlayerState.INITIALIZED;

    private enum SmilPlayerAction {
        NO_ACTIVE_ACTION,
        RELOAD,
        STOP,
        PAUSE,
        START,
        NEXT,
        PREV
    }

    private enum SmilPlayerState {
        INITIALIZED,
        PLAYING,
        PLAYED,
        PAUSED,
        STOPPED
    }

    private static final class TimelineEntry {
        static final int ACTION_BEGIN = 0;
        static final int ACTION_END = 1;
        private final int mAction;
        private final ElementTime mElement;
        /* access modifiers changed from: private */
        public final double mOffsetTime;

        public TimelineEntry(double d, ElementTime elementTime, int i) {
            this.mOffsetTime = d;
            this.mElement = elementTime;
            this.mAction = i;
        }

        public int getAction() {
            return this.mAction;
        }

        public ElementTime getElement() {
            return this.mElement;
        }

        public double getOffsetTime() {
            return this.mOffsetTime;
        }

        public String toString() {
            return "Type = " + this.mElement + " offset = " + getOffsetTime() + " action = " + getAction();
        }
    }

    private SmilPlayer() {
    }

    private synchronized void actionEntry(TimelineEntry timelineEntry) {
        switch (timelineEntry.getAction()) {
            case 0:
                timelineEntry.getElement().beginElement();
                this.mActiveElements.add(timelineEntry.getElement());
                break;
            case 1:
                timelineEntry.getElement().endElement();
                this.mActiveElements.remove(timelineEntry.getElement());
                break;
        }
    }

    private synchronized TimelineEntry actionNext() {
        stopCurrentSlide();
        return loadNextSlide();
    }

    private synchronized void actionPause() {
        pauseActiveElements();
        this.mState = SmilPlayerState.PAUSED;
        this.mAction = SmilPlayerAction.NO_ACTIVE_ACTION;
    }

    private synchronized TimelineEntry actionPrev() {
        stopCurrentSlide();
        return loadPrevSlide();
    }

    private synchronized void actionReload() {
        reloadActiveSlide();
        this.mAction = SmilPlayerAction.NO_ACTIVE_ACTION;
    }

    private synchronized void actionStop() {
        endActiveElements();
        this.mCurrentTime = 0;
        this.mCurrentElement = 0;
        this.mCurrentSlide = 0;
        this.mState = SmilPlayerState.STOPPED;
        this.mAction = SmilPlayerAction.NO_ACTIVE_ACTION;
    }

    private synchronized void beginSmilDocument() {
        actionEntry(this.mAllEntries.get(0));
    }

    private void dumpAllEntries() {
    }

    private synchronized void endActiveElements() {
        for (int size = this.mActiveElements.size() - 1; size >= 0; size--) {
            this.mActiveElements.get(size).endElement();
        }
    }

    private synchronized double getOffsetTime(ElementTime elementTime) {
        double d;
        int i = this.mCurrentSlide;
        while (true) {
            int i2 = i;
            if (i2 >= this.mCurrentElement) {
                d = -1.0d;
                break;
            }
            TimelineEntry timelineEntry = this.mAllEntries.get(i2);
            if (elementTime.equals(timelineEntry.getElement())) {
                d = timelineEntry.getOffsetTime() * 1000.0d;
                break;
            }
            i = i2 + 1;
        }
        return d;
    }

    private static ArrayList<TimelineEntry> getParTimeline(ElementParallelTimeContainer elementParallelTimeContainer, double d, double d2) {
        ArrayList<TimelineEntry> arrayList = new ArrayList<>();
        double resolvedOffset = elementParallelTimeContainer.getBegin().item(0).getResolvedOffset() + d;
        if (resolvedOffset > d2) {
            return arrayList;
        }
        arrayList.add(new TimelineEntry(resolvedOffset, elementParallelTimeContainer, 0));
        double resolvedOffset2 = elementParallelTimeContainer.getEnd().item(0).getResolvedOffset() + d;
        if (resolvedOffset2 > d2) {
            resolvedOffset2 = d2;
        }
        TimelineEntry timelineEntry = new TimelineEntry(resolvedOffset2, elementParallelTimeContainer, 1);
        NodeList timeChildren = elementParallelTimeContainer.getTimeChildren();
        for (int i = 0; i < timeChildren.getLength(); i++) {
            arrayList.addAll(getTimeline((ElementTime) timeChildren.item(i), d, resolvedOffset2));
        }
        Collections.sort(arrayList, sTimelineEntryComparator);
        NodeList activeChildrenAt = elementParallelTimeContainer.getActiveChildrenAt(((float) (resolvedOffset2 - d)) * 1000.0f);
        for (int i2 = 0; i2 < activeChildrenAt.getLength(); i2++) {
            arrayList.add(new TimelineEntry(resolvedOffset2, (ElementTime) activeChildrenAt.item(i2), 1));
        }
        arrayList.add(timelineEntry);
        return arrayList;
    }

    public static SmilPlayer getPlayer() {
        if (sPlayer == null) {
            sPlayer = new SmilPlayer();
        }
        return sPlayer;
    }

    private static ArrayList<TimelineEntry> getSeqTimeline(ElementSequentialTimeContainer elementSequentialTimeContainer, double d, double d2) {
        ArrayList<TimelineEntry> arrayList = new ArrayList<>();
        double resolvedOffset = elementSequentialTimeContainer.getBegin().item(0).getResolvedOffset() + d;
        if (resolvedOffset > d2) {
            return arrayList;
        }
        arrayList.add(new TimelineEntry(resolvedOffset, elementSequentialTimeContainer, 0));
        double resolvedOffset2 = elementSequentialTimeContainer.getEnd().item(0).getResolvedOffset() + d;
        if (resolvedOffset2 > d2) {
            resolvedOffset2 = d2;
        }
        TimelineEntry timelineEntry = new TimelineEntry(resolvedOffset2, elementSequentialTimeContainer, 1);
        NodeList timeChildren = elementSequentialTimeContainer.getTimeChildren();
        double d3 = d;
        for (int i = 0; i < timeChildren.getLength(); i++) {
            ArrayList<TimelineEntry> timeline = getTimeline((ElementTime) timeChildren.item(i), d3, resolvedOffset2);
            arrayList.addAll(timeline);
            d3 = timeline.get(timeline.size() - 1).getOffsetTime();
        }
        NodeList activeChildrenAt = elementSequentialTimeContainer.getActiveChildrenAt((float) (resolvedOffset2 - d));
        for (int i2 = 0; i2 < activeChildrenAt.getLength(); i2++) {
            arrayList.add(new TimelineEntry(resolvedOffset2, (ElementTime) activeChildrenAt.item(i2), 1));
        }
        arrayList.add(timelineEntry);
        return arrayList;
    }

    private static ArrayList<TimelineEntry> getTimeline(ElementTime elementTime, double d, double d2) {
        if (elementTime instanceof ElementParallelTimeContainer) {
            return getParTimeline((ElementParallelTimeContainer) elementTime, d, d2);
        }
        if (elementTime instanceof ElementSequentialTimeContainer) {
            return getSeqTimeline((ElementSequentialTimeContainer) elementTime, d, d2);
        }
        ArrayList<TimelineEntry> arrayList = new ArrayList<>();
        TimeList begin = elementTime.getBegin();
        for (int i = 0; i < begin.getLength(); i++) {
            Time item = begin.item(i);
            if (item.getResolved()) {
                double resolvedOffset = item.getResolvedOffset() + d;
                if (resolvedOffset <= d2) {
                    arrayList.add(new TimelineEntry(resolvedOffset, elementTime, 0));
                }
            }
        }
        TimeList end = elementTime.getEnd();
        for (int i2 = 0; i2 < end.getLength(); i2++) {
            Time item2 = end.item(i2);
            if (item2.getResolved()) {
                double resolvedOffset2 = item2.getResolvedOffset() + d;
                if (resolvedOffset2 <= d2) {
                    arrayList.add(new TimelineEntry(resolvedOffset2, elementTime, 1));
                }
            }
        }
        Collections.sort(arrayList, sTimelineEntryComparator);
        return arrayList;
    }

    private synchronized boolean isBeginOfSlide(TimelineEntry timelineEntry) {
        return timelineEntry.getAction() == 0 && (timelineEntry.getElement() instanceof SmilParElementImpl);
    }

    private synchronized boolean isNextAction() {
        return this.mAction == SmilPlayerAction.NEXT;
    }

    private synchronized boolean isPauseAction() {
        return this.mAction == SmilPlayerAction.PAUSE;
    }

    private synchronized boolean isPrevAction() {
        return this.mAction == SmilPlayerAction.PREV;
    }

    private synchronized boolean isReloadAction() {
        return this.mAction == SmilPlayerAction.RELOAD;
    }

    private synchronized boolean isStartAction() {
        return this.mAction == SmilPlayerAction.START;
    }

    private synchronized boolean isStopAction() {
        return this.mAction == SmilPlayerAction.STOP;
    }

    private TimelineEntry loadNextSlide() {
        int size = this.mAllEntries.size();
        for (int i = this.mCurrentElement; i < size; i++) {
            TimelineEntry timelineEntry = this.mAllEntries.get(i);
            if (isBeginOfSlide(timelineEntry)) {
                this.mCurrentElement = i;
                this.mCurrentSlide = i;
                this.mCurrentTime = (long) (timelineEntry.getOffsetTime() * 1000.0d);
                return timelineEntry;
            }
        }
        this.mCurrentElement++;
        if (this.mCurrentElement >= size) {
            return null;
        }
        TimelineEntry timelineEntry2 = this.mAllEntries.get(this.mCurrentElement);
        this.mCurrentTime = (long) (timelineEntry2.getOffsetTime() * 1000.0d);
        return timelineEntry2;
    }

    private TimelineEntry loadPrevSlide() {
        int i;
        int i2;
        int i3 = this.mCurrentSlide;
        int i4 = -1;
        int i5 = 1;
        while (i3 >= 0) {
            TimelineEntry timelineEntry = this.mAllEntries.get(i3);
            if (isBeginOfSlide(timelineEntry)) {
                i2 = i5 - 1;
                if (i5 == 0) {
                    this.mCurrentElement = i3;
                    this.mCurrentSlide = i3;
                    this.mCurrentTime = (long) (timelineEntry.getOffsetTime() * 1000.0d);
                    return timelineEntry;
                }
                i = i3;
            } else {
                i = i4;
                i2 = i5;
            }
            i3--;
            i5 = i2;
            i4 = i;
        }
        if (i4 == -1) {
            return null;
        }
        this.mCurrentElement = i4;
        this.mCurrentSlide = i4;
        return this.mAllEntries.get(this.mCurrentElement);
    }

    private synchronized void pauseActiveElements() {
        for (int size = this.mActiveElements.size() - 1; size >= 0; size--) {
            this.mActiveElements.get(size).pauseElement();
        }
    }

    private synchronized void reloadActiveSlide() {
        this.mActiveElements.clear();
        beginSmilDocument();
        for (int i = this.mCurrentSlide; i < this.mCurrentElement; i++) {
            actionEntry(this.mAllEntries.get(i));
        }
        seekActiveMedia();
    }

    private synchronized TimelineEntry reloadCurrentEntry() {
        return this.mCurrentElement < this.mAllEntries.size() ? this.mAllEntries.get(this.mCurrentElement) : null;
    }

    private synchronized void resumeActiveElements() {
        int size = this.mActiveElements.size();
        for (int i = 0; i < size; i++) {
            this.mActiveElements.get(i).resumeElement();
        }
    }

    private synchronized void seekActiveMedia() {
        for (int size = this.mActiveElements.size() - 1; size >= 0; size--) {
            ElementTime elementTime = this.mActiveElements.get(size);
            if (elementTime instanceof SmilParElementImpl) {
                break;
            }
            double offsetTime = getOffsetTime(elementTime);
            if (offsetTime >= 0.0d && offsetTime <= ((double) this.mCurrentTime)) {
                elementTime.seekElement((float) (((double) this.mCurrentTime) - offsetTime));
            }
        }
    }

    private void stopCurrentSlide() {
        HashSet hashSet = new HashSet();
        int size = this.mAllEntries.size();
        int i = this.mCurrentElement;
        while (i < size) {
            TimelineEntry timelineEntry = this.mAllEntries.get(i);
            int action = timelineEntry.getAction();
            if (!(timelineEntry.getElement() instanceof SmilParElementImpl) || action != 1) {
                if (action == 1 && !hashSet.contains(timelineEntry)) {
                    actionEntry(timelineEntry);
                } else if (action == 0) {
                    hashSet.add(timelineEntry);
                }
                i++;
            } else {
                actionEntry(timelineEntry);
                this.mCurrentElement = i;
                return;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    private synchronized void waitForEntry(long j) throws InterruptedException {
        long j2;
        long j3 = 0;
        long j4 = j;
        while (j4 > 0) {
            long currentTimeMillis = System.currentTimeMillis();
            long min = Math.min(j4, 200L);
            if (j3 < min) {
                wait(min - j3);
                this.mCurrentTime += min;
                j2 = min;
            } else {
                this.mCurrentTime = j3 + this.mCurrentTime;
                j2 = 0;
            }
            if (isStopAction() || isReloadAction() || isPauseAction() || isNextAction() || isPrevAction()) {
                break;
            }
            ((EventTarget) this.mRoot).dispatchEvent(this.mMediaTimeUpdatedEvent);
            j4 -= 200;
            j3 = (System.currentTimeMillis() - currentTimeMillis) - j2;
        }
    }

    private synchronized void waitForWakeUp() {
        while (!isStartAction() && !isStopAction() && !isReloadAction() && !isNextAction() && !isPrevAction()) {
            try {
                wait(200);
            } catch (InterruptedException e) {
                Log.e(TAG, "Unexpected InterruptedException.", e);
            }
        }
        if (isStartAction()) {
            this.mAction = SmilPlayerAction.NO_ACTIVE_ACTION;
            this.mState = SmilPlayerState.PLAYING;
        }
        return;
    }

    public synchronized int getCurrentPosition() {
        return (int) this.mCurrentTime;
    }

    public synchronized int getDuration() {
        return (this.mAllEntries == null || this.mAllEntries.isEmpty()) ? 0 : ((int) this.mAllEntries.get(this.mAllEntries.size() - 1).mOffsetTime) * 1000;
    }

    public synchronized void init(ElementTime elementTime) {
        this.mRoot = elementTime;
        this.mAllEntries = getTimeline(this.mRoot, 0.0d, 9.223372036854776E18d);
        this.mMediaTimeUpdatedEvent = ((DocumentEvent) this.mRoot).createEvent("Event");
        this.mMediaTimeUpdatedEvent.initEvent(MEDIA_TIME_UPDATED_EVENT, false, false);
        this.mActiveElements = new ArrayList<>();
    }

    public synchronized boolean isPausedState() {
        return this.mState == SmilPlayerState.PAUSED;
    }

    public synchronized boolean isPlayedState() {
        return this.mState == SmilPlayerState.PLAYED;
    }

    public synchronized boolean isPlayingState() {
        return this.mState == SmilPlayerState.PLAYING;
    }

    public synchronized boolean isStoppedState() {
        return this.mState == SmilPlayerState.STOPPED;
    }

    public synchronized void next() {
        if (isPlayingState() || isPausedState()) {
            this.mAction = SmilPlayerAction.NEXT;
            notifyAll();
        }
    }

    public synchronized void pause() {
        if (isPlayingState()) {
            this.mAction = SmilPlayerAction.PAUSE;
            notifyAll();
        } else {
            Log.w(TAG, "Error State: Playback is not playing!");
        }
    }

    public synchronized void play() {
        if (!isPlayingState()) {
            this.mCurrentTime = 0;
            this.mCurrentElement = 0;
            this.mCurrentSlide = 0;
            this.mPlayerThread = new Thread(this);
            this.mState = SmilPlayerState.PLAYING;
            this.mPlayerThread.start();
        } else {
            Log.w(TAG, "Error State: Playback is playing!");
        }
    }

    public synchronized void prev() {
        if (isPlayingState() || isPausedState()) {
            this.mAction = SmilPlayerAction.PREV;
            notifyAll();
        }
    }

    public synchronized void reload() {
        if (isPlayingState() || isPausedState()) {
            this.mAction = SmilPlayerAction.RELOAD;
            notifyAll();
        } else if (isPlayedState()) {
            actionReload();
        }
    }

    public void run() {
        if (!isStoppedState()) {
            int size = this.mAllEntries.size();
            this.mCurrentElement = 0;
            while (this.mCurrentElement < size) {
                TimelineEntry timelineEntry = this.mAllEntries.get(this.mCurrentElement);
                if (isBeginOfSlide(timelineEntry)) {
                    this.mCurrentSlide = this.mCurrentElement;
                }
                long offsetTime = (long) (timelineEntry.getOffsetTime() * 1000.0d);
                while (offsetTime > this.mCurrentTime) {
                    try {
                        waitForEntry(offsetTime - this.mCurrentTime);
                    } catch (InterruptedException e) {
                        Log.e(TAG, "Unexpected InterruptedException.", e);
                    }
                    while (true) {
                        if (isPauseAction() || isStopAction() || isReloadAction() || isNextAction() || isPrevAction()) {
                            if (isPauseAction()) {
                                actionPause();
                                waitForWakeUp();
                            }
                            if (isStopAction()) {
                                actionStop();
                                return;
                            }
                            if (isReloadAction()) {
                                actionReload();
                                timelineEntry = reloadCurrentEntry();
                                if (timelineEntry == null) {
                                    return;
                                }
                                if (isPausedState()) {
                                    this.mAction = SmilPlayerAction.PAUSE;
                                }
                            }
                            if (isNextAction()) {
                                TimelineEntry actionNext = actionNext();
                                if (actionNext != null) {
                                    timelineEntry = actionNext;
                                }
                                if (this.mState == SmilPlayerState.PAUSED) {
                                    this.mAction = SmilPlayerAction.PAUSE;
                                    actionEntry(timelineEntry);
                                } else {
                                    this.mAction = SmilPlayerAction.NO_ACTIVE_ACTION;
                                }
                                offsetTime = this.mCurrentTime;
                            }
                            if (isPrevAction()) {
                                TimelineEntry actionPrev = actionPrev();
                                if (actionPrev != null) {
                                    timelineEntry = actionPrev;
                                }
                                if (this.mState == SmilPlayerState.PAUSED) {
                                    this.mAction = SmilPlayerAction.PAUSE;
                                    actionEntry(timelineEntry);
                                } else {
                                    this.mAction = SmilPlayerAction.NO_ACTIVE_ACTION;
                                }
                                offsetTime = this.mCurrentTime;
                            }
                        }
                    }
                }
                this.mCurrentTime = offsetTime;
                actionEntry(timelineEntry);
                this.mCurrentElement++;
            }
            this.mState = SmilPlayerState.PLAYED;
        }
    }

    public synchronized void start() {
        if (isPausedState()) {
            resumeActiveElements();
            this.mAction = SmilPlayerAction.START;
            notifyAll();
        } else if (isPlayedState()) {
            play();
        } else {
            Log.w(TAG, "Error State: Playback can not be started!");
        }
    }

    public synchronized void stop() {
        if (isPlayingState() || isPausedState()) {
            this.mAction = SmilPlayerAction.STOP;
            notifyAll();
        } else if (isPlayedState()) {
            actionStop();
        }
    }

    public synchronized void stopWhenReload() {
        endActiveElements();
    }
}
