package com.nullwire.trace;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Log;
import java.io.File;
import java.io.FilenameFilter;
import java.lang.Thread;

public class ExceptionHandler {
    public static String TAG = "com.nullwire.trace.ExceptionsHandler";
    private static String[] stackTraceFileList = null;

    public static void register(Context context, String str) {
        Log.i(TAG, "Registering default exceptions handler: " + str);
        G.URL = str;
        register(context);
    }

    public static boolean register(Context context) {
        Log.i(TAG, "Registering default exceptions handler");
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            G.APP_VERSION = packageInfo.versionName;
            G.APP_PACKAGE = packageInfo.packageName;
            G.FILES_PATH = context.getFilesDir().getAbsolutePath();
            G.PHONE_MODEL = Build.MODEL;
            G.ANDROID_VERSION = Build.VERSION.RELEASE;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        Log.i(TAG, "TRACE_VERSION: " + G.TraceVersion);
        Log.d(TAG, "APP_VERSION: " + G.APP_VERSION);
        Log.d(TAG, "APP_PACKAGE: " + G.APP_PACKAGE);
        Log.d(TAG, "FILES_PATH: " + G.FILES_PATH);
        boolean z = searchForStackTraces().length > 0;
        new Thread() {
            public void run() {
                ExceptionHandler.submitStackTraces();
                Thread.UncaughtExceptionHandler defaultUncaughtExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
                if (defaultUncaughtExceptionHandler != null) {
                    Log.d(ExceptionHandler.TAG, "current handler class=" + defaultUncaughtExceptionHandler.getClass().getName());
                }
                if (!(defaultUncaughtExceptionHandler instanceof DefaultExceptionHandler)) {
                    Thread.setDefaultUncaughtExceptionHandler(new DefaultExceptionHandler(defaultUncaughtExceptionHandler));
                }
            }
        }.start();
        return z;
    }

    private static String[] searchForStackTraces() {
        if (stackTraceFileList != null) {
            return stackTraceFileList;
        }
        File file = new File(G.FILES_PATH + "/");
        file.mkdir();
        String[] list = file.list(new FilenameFilter() {
            public boolean accept(File file, String str) {
                return str.endsWith(".stacktrace");
            }
        });
        stackTraceFileList = list;
        return list;
    }

    /*  JADX ERROR: StackOverflow in pass: MarkFinallyVisitor
        jadx.core.utils.exceptions.JadxOverflowException: 
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    public static void submitStackTraces() {
        /*
            r13 = 0
            r12 = 0
            java.lang.String r0 = "/"
            java.lang.String r0 = com.nullwire.trace.ExceptionHandler.TAG     // Catch:{ Exception -> 0x00be }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00be }
            r1.<init>()     // Catch:{ Exception -> 0x00be }
            java.lang.String r2 = "Looking for exceptions in: "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00be }
            java.lang.String r2 = com.nullwire.trace.G.FILES_PATH     // Catch:{ Exception -> 0x00be }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00be }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x00be }
            android.util.Log.d(r0, r1)     // Catch:{ Exception -> 0x00be }
            java.lang.String[] r0 = searchForStackTraces()     // Catch:{ Exception -> 0x00be }
            if (r0 == 0) goto L_0x0165
            int r1 = r0.length     // Catch:{ Exception -> 0x00be }
            if (r1 <= 0) goto L_0x0165
            java.lang.String r1 = com.nullwire.trace.ExceptionHandler.TAG     // Catch:{ Exception -> 0x00be }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00be }
            r2.<init>()     // Catch:{ Exception -> 0x00be }
            java.lang.String r3 = "Found "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x00be }
            int r3 = r0.length     // Catch:{ Exception -> 0x00be }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x00be }
            java.lang.String r3 = " stacktrace(s)"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x00be }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x00be }
            android.util.Log.d(r1, r2)     // Catch:{ Exception -> 0x00be }
            r1 = r12
        L_0x0047:
            int r2 = r0.length     // Catch:{ Exception -> 0x00be }
            if (r1 >= r2) goto L_0x0165
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00be }
            r2.<init>()     // Catch:{ Exception -> 0x00be }
            java.lang.String r3 = com.nullwire.trace.G.FILES_PATH     // Catch:{ Exception -> 0x00be }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x00be }
            java.lang.String r3 = "/"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x00be }
            r3 = r0[r1]     // Catch:{ Exception -> 0x00be }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x00be }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x00be }
            r3 = r0[r1]     // Catch:{ Exception -> 0x00be }
            java.lang.String r4 = "-"
            java.lang.String[] r3 = r3.split(r4)     // Catch:{ Exception -> 0x00be }
            r4 = 0
            r3 = r3[r4]     // Catch:{ Exception -> 0x00be }
            java.lang.String r4 = com.nullwire.trace.ExceptionHandler.TAG     // Catch:{ Exception -> 0x00be }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00be }
            r5.<init>()     // Catch:{ Exception -> 0x00be }
            java.lang.String r6 = "Stacktrace in file '"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x00be }
            java.lang.StringBuilder r5 = r5.append(r2)     // Catch:{ Exception -> 0x00be }
            java.lang.String r6 = "' belongs to version "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x00be }
            java.lang.StringBuilder r5 = r5.append(r3)     // Catch:{ Exception -> 0x00be }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x00be }
            android.util.Log.d(r4, r5)     // Catch:{ Exception -> 0x00be }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00be }
            r4.<init>()     // Catch:{ Exception -> 0x00be }
            java.io.BufferedReader r5 = new java.io.BufferedReader     // Catch:{ Exception -> 0x00be }
            java.io.FileReader r6 = new java.io.FileReader     // Catch:{ Exception -> 0x00be }
            r6.<init>(r2)     // Catch:{ Exception -> 0x00be }
            r5.<init>(r6)     // Catch:{ Exception -> 0x00be }
            r2 = r13
            r6 = r13
        L_0x00a3:
            java.lang.String r7 = r5.readLine()     // Catch:{ Exception -> 0x00be }
            if (r7 == 0) goto L_0x00f0
            if (r6 != 0) goto L_0x00ad
            r6 = r7
            goto L_0x00a3
        L_0x00ad:
            if (r2 != 0) goto L_0x00b1
            r2 = r7
            goto L_0x00a3
        L_0x00b1:
            r4.append(r7)     // Catch:{ Exception -> 0x00be }
            java.lang.String r7 = "line.separator"
            java.lang.String r7 = java.lang.System.getProperty(r7)     // Catch:{ Exception -> 0x00be }
            r4.append(r7)     // Catch:{ Exception -> 0x00be }
            goto L_0x00a3
        L_0x00be:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x019d }
            java.lang.String[] r0 = searchForStackTraces()     // Catch:{ Exception -> 0x0198 }
            r1 = r12
        L_0x00c7:
            int r2 = r0.length     // Catch:{ Exception -> 0x0198 }
            if (r1 >= r2) goto L_0x0197
            java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x0198 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0198 }
            r3.<init>()     // Catch:{ Exception -> 0x0198 }
            java.lang.String r4 = com.nullwire.trace.G.FILES_PATH     // Catch:{ Exception -> 0x0198 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0198 }
            java.lang.String r4 = "/"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0198 }
            r4 = r0[r1]     // Catch:{ Exception -> 0x0198 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0198 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0198 }
            r2.<init>(r3)     // Catch:{ Exception -> 0x0198 }
            r2.delete()     // Catch:{ Exception -> 0x0198 }
            int r1 = r1 + 1
            goto L_0x00c7
        L_0x00f0:
            r5.close()     // Catch:{ Exception -> 0x00be }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x00be }
            java.lang.String r5 = com.nullwire.trace.ExceptionHandler.TAG     // Catch:{ Exception -> 0x00be }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00be }
            r7.<init>()     // Catch:{ Exception -> 0x00be }
            java.lang.String r8 = "Transmitting stack trace: "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Exception -> 0x00be }
            java.lang.StringBuilder r7 = r7.append(r4)     // Catch:{ Exception -> 0x00be }
            java.lang.String r7 = r7.toString()     // Catch:{ Exception -> 0x00be }
            android.util.Log.d(r5, r7)     // Catch:{ Exception -> 0x00be }
            org.apache.http.impl.client.DefaultHttpClient r5 = new org.apache.http.impl.client.DefaultHttpClient     // Catch:{ Exception -> 0x00be }
            r5.<init>()     // Catch:{ Exception -> 0x00be }
            org.apache.http.client.methods.HttpPost r7 = new org.apache.http.client.methods.HttpPost     // Catch:{ Exception -> 0x00be }
            java.lang.String r8 = com.nullwire.trace.G.URL     // Catch:{ Exception -> 0x00be }
            r7.<init>(r8)     // Catch:{ Exception -> 0x00be }
            java.util.ArrayList r8 = new java.util.ArrayList     // Catch:{ Exception -> 0x00be }
            r8.<init>()     // Catch:{ Exception -> 0x00be }
            org.apache.http.message.BasicNameValuePair r9 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x00be }
            java.lang.String r10 = "package_name"
            java.lang.String r11 = com.nullwire.trace.G.APP_PACKAGE     // Catch:{ Exception -> 0x00be }
            r9.<init>(r10, r11)     // Catch:{ Exception -> 0x00be }
            r8.add(r9)     // Catch:{ Exception -> 0x00be }
            org.apache.http.message.BasicNameValuePair r9 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x00be }
            java.lang.String r10 = "package_version"
            r9.<init>(r10, r3)     // Catch:{ Exception -> 0x00be }
            r8.add(r9)     // Catch:{ Exception -> 0x00be }
            org.apache.http.message.BasicNameValuePair r3 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x00be }
            java.lang.String r9 = "phone_model"
            r3.<init>(r9, r2)     // Catch:{ Exception -> 0x00be }
            r8.add(r3)     // Catch:{ Exception -> 0x00be }
            org.apache.http.message.BasicNameValuePair r2 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x00be }
            java.lang.String r3 = "android_version"
            r2.<init>(r3, r6)     // Catch:{ Exception -> 0x00be }
            r8.add(r2)     // Catch:{ Exception -> 0x00be }
            org.apache.http.message.BasicNameValuePair r2 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x00be }
            java.lang.String r3 = "stacktrace"
            r2.<init>(r3, r4)     // Catch:{ Exception -> 0x00be }
            r8.add(r2)     // Catch:{ Exception -> 0x00be }
            org.apache.http.client.entity.UrlEncodedFormEntity r2 = new org.apache.http.client.entity.UrlEncodedFormEntity     // Catch:{ Exception -> 0x00be }
            java.lang.String r3 = "UTF-8"
            r2.<init>(r8, r3)     // Catch:{ Exception -> 0x00be }
            r7.setEntity(r2)     // Catch:{ Exception -> 0x00be }
            r5.execute(r7)     // Catch:{ Exception -> 0x00be }
            int r1 = r1 + 1
            goto L_0x0047
        L_0x0165:
            java.lang.String[] r0 = searchForStackTraces()     // Catch:{ Exception -> 0x0193 }
            r1 = r12
        L_0x016a:
            int r2 = r0.length     // Catch:{ Exception -> 0x0193 }
            if (r1 >= r2) goto L_0x0197
            java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x0193 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0193 }
            r3.<init>()     // Catch:{ Exception -> 0x0193 }
            java.lang.String r4 = com.nullwire.trace.G.FILES_PATH     // Catch:{ Exception -> 0x0193 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0193 }
            java.lang.String r4 = "/"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0193 }
            r4 = r0[r1]     // Catch:{ Exception -> 0x0193 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0193 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0193 }
            r2.<init>(r3)     // Catch:{ Exception -> 0x0193 }
            r2.delete()     // Catch:{ Exception -> 0x0193 }
            int r1 = r1 + 1
            goto L_0x016a
        L_0x0193:
            r0 = move-exception
            r0.printStackTrace()
        L_0x0197:
            return
        L_0x0198:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0197
        L_0x019d:
            r0 = move-exception
            java.lang.String[] r1 = searchForStackTraces()     // Catch:{ Exception -> 0x01cc }
            r2 = r12
        L_0x01a3:
            int r3 = r1.length     // Catch:{ Exception -> 0x01cc }
            if (r2 >= r3) goto L_0x01d0
            java.io.File r3 = new java.io.File     // Catch:{ Exception -> 0x01cc }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01cc }
            r4.<init>()     // Catch:{ Exception -> 0x01cc }
            java.lang.String r5 = com.nullwire.trace.G.FILES_PATH     // Catch:{ Exception -> 0x01cc }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x01cc }
            java.lang.String r5 = "/"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x01cc }
            r5 = r1[r2]     // Catch:{ Exception -> 0x01cc }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x01cc }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x01cc }
            r3.<init>(r4)     // Catch:{ Exception -> 0x01cc }
            r3.delete()     // Catch:{ Exception -> 0x01cc }
            int r2 = r2 + 1
            goto L_0x01a3
        L_0x01cc:
            r1 = move-exception
            r1.printStackTrace()
        L_0x01d0:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.nullwire.trace.ExceptionHandler.submitStackTraces():void");
    }
}
