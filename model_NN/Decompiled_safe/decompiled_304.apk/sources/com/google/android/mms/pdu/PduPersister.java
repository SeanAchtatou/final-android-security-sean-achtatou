package com.google.android.mms.pdu;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import com.android.internal.telephony.Phone;
import com.android.provider.Telephony;
import com.google.android.mms.ContentType;
import com.google.android.mms.InvalidHeaderValueException;
import com.google.android.mms.MmsException;
import com.google.android.mms.util.PduCache;
import com.google.android.mms.util.PduCacheEntry;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import vc.lx.sms.util.SqliteWrapper;

public class PduPersister {
    private static final int[] ADDRESS_FIELDS = {129, 130, 137, 151};
    private static final HashMap<Integer, Integer> CHARSET_COLUMN_INDEX_MAP = new HashMap<>();
    private static final HashMap<Integer, String> CHARSET_COLUMN_NAME_MAP = new HashMap<>();
    private static final boolean DEBUG = false;
    private static final long DUMMY_THREAD_ID = Long.MAX_VALUE;
    private static final HashMap<Integer, Integer> ENCODED_STRING_COLUMN_INDEX_MAP = new HashMap<>();
    private static final HashMap<Integer, String> ENCODED_STRING_COLUMN_NAME_MAP = new HashMap<>();
    private static final boolean LOCAL_LOGV = false;
    private static final HashMap<Integer, Integer> LONG_COLUMN_INDEX_MAP = new HashMap<>();
    private static final HashMap<Integer, String> LONG_COLUMN_NAME_MAP = new HashMap<>();
    private static final HashMap<Uri, Integer> MESSAGE_BOX_MAP = new HashMap<>();
    private static final HashMap<Integer, Integer> OCTET_COLUMN_INDEX_MAP = new HashMap<>();
    private static final HashMap<Integer, String> OCTET_COLUMN_NAME_MAP = new HashMap<>();
    private static final int PART_COLUMN_CHARSET = 1;
    private static final int PART_COLUMN_CONTENT_DISPOSITION = 2;
    private static final int PART_COLUMN_CONTENT_ID = 3;
    private static final int PART_COLUMN_CONTENT_LOCATION = 4;
    private static final int PART_COLUMN_CONTENT_TYPE = 5;
    private static final int PART_COLUMN_FILENAME = 6;
    private static final int PART_COLUMN_ID = 0;
    private static final int PART_COLUMN_NAME = 7;
    private static final int PART_COLUMN_TEXT = 8;
    private static final String[] PART_PROJECTION = {"_id", Telephony.Mms.Part.CHARSET, Telephony.Mms.Part.CONTENT_DISPOSITION, Telephony.Mms.Part.CONTENT_ID, Telephony.Mms.Part.CONTENT_LOCATION, Telephony.Mms.Part.CONTENT_TYPE, Telephony.Mms.Part.FILENAME, "name", "text"};
    private static final PduCache PDU_CACHE_INSTANCE = PduCache.getInstance();
    private static final int PDU_COLUMN_CONTENT_CLASS = 11;
    private static final int PDU_COLUMN_CONTENT_LOCATION = 5;
    private static final int PDU_COLUMN_CONTENT_TYPE = 6;
    private static final int PDU_COLUMN_DATE = 21;
    private static final int PDU_COLUMN_DELIVERY_REPORT = 12;
    private static final int PDU_COLUMN_DELIVERY_TIME = 22;
    private static final int PDU_COLUMN_EXPIRY = 23;
    private static final int PDU_COLUMN_ID = 0;
    private static final int PDU_COLUMN_MESSAGE_BOX = 1;
    private static final int PDU_COLUMN_MESSAGE_CLASS = 7;
    private static final int PDU_COLUMN_MESSAGE_ID = 8;
    private static final int PDU_COLUMN_MESSAGE_SIZE = 24;
    private static final int PDU_COLUMN_MESSAGE_TYPE = 13;
    private static final int PDU_COLUMN_MMS_VERSION = 14;
    private static final int PDU_COLUMN_PRIORITY = 15;
    private static final int PDU_COLUMN_READ_REPORT = 16;
    private static final int PDU_COLUMN_READ_STATUS = 17;
    private static final int PDU_COLUMN_REPORT_ALLOWED = 18;
    private static final int PDU_COLUMN_RESPONSE_TEXT = 9;
    private static final int PDU_COLUMN_RETRIEVE_STATUS = 19;
    private static final int PDU_COLUMN_RETRIEVE_TEXT = 3;
    private static final int PDU_COLUMN_RETRIEVE_TEXT_CHARSET = 26;
    private static final int PDU_COLUMN_STATUS = 20;
    private static final int PDU_COLUMN_SUBJECT = 4;
    private static final int PDU_COLUMN_SUBJECT_CHARSET = 25;
    private static final int PDU_COLUMN_THREAD_ID = 2;
    private static final int PDU_COLUMN_TRANSACTION_ID = 10;
    private static final String[] PDU_PROJECTION = {"_id", Telephony.BaseMmsColumns.MESSAGE_BOX, "thread_id", Telephony.BaseMmsColumns.RETRIEVE_TEXT, Telephony.BaseMmsColumns.SUBJECT, Telephony.BaseMmsColumns.CONTENT_LOCATION, Telephony.BaseMmsColumns.CONTENT_TYPE, Telephony.BaseMmsColumns.MESSAGE_CLASS, Telephony.BaseMmsColumns.MESSAGE_ID, Telephony.BaseMmsColumns.RESPONSE_TEXT, Telephony.BaseMmsColumns.TRANSACTION_ID, Telephony.BaseMmsColumns.CONTENT_CLASS, Telephony.BaseMmsColumns.DELIVERY_REPORT, Telephony.BaseMmsColumns.MESSAGE_TYPE, Telephony.BaseMmsColumns.MMS_VERSION, Telephony.BaseMmsColumns.PRIORITY, Telephony.BaseMmsColumns.READ_REPORT, Telephony.BaseMmsColumns.READ_STATUS, Telephony.BaseMmsColumns.REPORT_ALLOWED, Telephony.BaseMmsColumns.RETRIEVE_STATUS, Telephony.BaseMmsColumns.STATUS, "date", Telephony.BaseMmsColumns.DELIVERY_TIME, Telephony.BaseMmsColumns.EXPIRY, Telephony.BaseMmsColumns.MESSAGE_SIZE, Telephony.BaseMmsColumns.SUBJECT_CHARSET, Telephony.BaseMmsColumns.RETRIEVE_TEXT_CHARSET};
    public static final int PROC_STATUS_COMPLETED = 3;
    public static final int PROC_STATUS_PERMANENTLY_FAILURE = 2;
    public static final int PROC_STATUS_TRANSIENT_FAILURE = 1;
    private static final String TAG = "PduPersister";
    public static final String TEMPORARY_DRM_OBJECT_URI = "content://mms/9223372036854775807/part";
    private static final HashMap<Integer, Integer> TEXT_STRING_COLUMN_INDEX_MAP = new HashMap<>();
    private static final HashMap<Integer, String> TEXT_STRING_COLUMN_NAME_MAP = new HashMap<>();
    private static PduPersister sPersister;
    private final ContentResolver mContentResolver;
    private final Context mContext;

    static {
        MESSAGE_BOX_MAP.put(Telephony.Mms.Inbox.CONTENT_URI, 1);
        MESSAGE_BOX_MAP.put(Telephony.Mms.Sent.CONTENT_URI, 2);
        MESSAGE_BOX_MAP.put(Telephony.Mms.Draft.CONTENT_URI, 3);
        MESSAGE_BOX_MAP.put(Telephony.Mms.Outbox.CONTENT_URI, 4);
        CHARSET_COLUMN_INDEX_MAP.put(150, 25);
        CHARSET_COLUMN_INDEX_MAP.put(154, 26);
        CHARSET_COLUMN_NAME_MAP.put(150, Telephony.BaseMmsColumns.SUBJECT_CHARSET);
        CHARSET_COLUMN_NAME_MAP.put(154, Telephony.BaseMmsColumns.RETRIEVE_TEXT_CHARSET);
        ENCODED_STRING_COLUMN_INDEX_MAP.put(154, 3);
        ENCODED_STRING_COLUMN_INDEX_MAP.put(150, 4);
        ENCODED_STRING_COLUMN_NAME_MAP.put(154, Telephony.BaseMmsColumns.RETRIEVE_TEXT);
        ENCODED_STRING_COLUMN_NAME_MAP.put(150, Telephony.BaseMmsColumns.SUBJECT);
        TEXT_STRING_COLUMN_INDEX_MAP.put(131, 5);
        TEXT_STRING_COLUMN_INDEX_MAP.put(132, 6);
        TEXT_STRING_COLUMN_INDEX_MAP.put(138, 7);
        TEXT_STRING_COLUMN_INDEX_MAP.put(139, 8);
        TEXT_STRING_COLUMN_INDEX_MAP.put(147, 9);
        TEXT_STRING_COLUMN_INDEX_MAP.put(152, 10);
        TEXT_STRING_COLUMN_NAME_MAP.put(131, Telephony.BaseMmsColumns.CONTENT_LOCATION);
        TEXT_STRING_COLUMN_NAME_MAP.put(132, Telephony.BaseMmsColumns.CONTENT_TYPE);
        TEXT_STRING_COLUMN_NAME_MAP.put(138, Telephony.BaseMmsColumns.MESSAGE_CLASS);
        TEXT_STRING_COLUMN_NAME_MAP.put(139, Telephony.BaseMmsColumns.MESSAGE_ID);
        TEXT_STRING_COLUMN_NAME_MAP.put(147, Telephony.BaseMmsColumns.RESPONSE_TEXT);
        TEXT_STRING_COLUMN_NAME_MAP.put(152, Telephony.BaseMmsColumns.TRANSACTION_ID);
        OCTET_COLUMN_INDEX_MAP.put(Integer.valueOf((int) PduHeaders.CONTENT_CLASS), 11);
        OCTET_COLUMN_INDEX_MAP.put(134, 12);
        OCTET_COLUMN_INDEX_MAP.put(140, 13);
        OCTET_COLUMN_INDEX_MAP.put(141, 14);
        OCTET_COLUMN_INDEX_MAP.put(143, 15);
        OCTET_COLUMN_INDEX_MAP.put(144, 16);
        OCTET_COLUMN_INDEX_MAP.put(155, 17);
        OCTET_COLUMN_INDEX_MAP.put(145, 18);
        OCTET_COLUMN_INDEX_MAP.put(153, 19);
        OCTET_COLUMN_INDEX_MAP.put(149, 20);
        OCTET_COLUMN_NAME_MAP.put(Integer.valueOf((int) PduHeaders.CONTENT_CLASS), Telephony.BaseMmsColumns.CONTENT_CLASS);
        OCTET_COLUMN_NAME_MAP.put(134, Telephony.BaseMmsColumns.DELIVERY_REPORT);
        OCTET_COLUMN_NAME_MAP.put(140, Telephony.BaseMmsColumns.MESSAGE_TYPE);
        OCTET_COLUMN_NAME_MAP.put(141, Telephony.BaseMmsColumns.MMS_VERSION);
        OCTET_COLUMN_NAME_MAP.put(143, Telephony.BaseMmsColumns.PRIORITY);
        OCTET_COLUMN_NAME_MAP.put(144, Telephony.BaseMmsColumns.READ_REPORT);
        OCTET_COLUMN_NAME_MAP.put(155, Telephony.BaseMmsColumns.READ_STATUS);
        OCTET_COLUMN_NAME_MAP.put(145, Telephony.BaseMmsColumns.REPORT_ALLOWED);
        OCTET_COLUMN_NAME_MAP.put(153, Telephony.BaseMmsColumns.RETRIEVE_STATUS);
        OCTET_COLUMN_NAME_MAP.put(149, Telephony.BaseMmsColumns.STATUS);
        LONG_COLUMN_INDEX_MAP.put(133, 21);
        LONG_COLUMN_INDEX_MAP.put(135, 22);
        LONG_COLUMN_INDEX_MAP.put(136, 23);
        LONG_COLUMN_INDEX_MAP.put(142, 24);
        LONG_COLUMN_NAME_MAP.put(133, "date");
        LONG_COLUMN_NAME_MAP.put(135, Telephony.BaseMmsColumns.DELIVERY_TIME);
        LONG_COLUMN_NAME_MAP.put(136, Telephony.BaseMmsColumns.EXPIRY);
        LONG_COLUMN_NAME_MAP.put(142, Telephony.BaseMmsColumns.MESSAGE_SIZE);
    }

    private PduPersister(Context context) {
        this.mContext = context;
        this.mContentResolver = context.getContentResolver();
    }

    private byte[] getByteArrayFromPartColumn(Cursor cursor, int i) {
        if (!cursor.isNull(i)) {
            return getBytes(cursor.getString(i));
        }
        return null;
    }

    public static byte[] getBytes(String str) {
        try {
            return str.getBytes(CharacterSets.MIMENAME_ISO_8859_1);
        } catch (UnsupportedEncodingException e) {
            Log.e(TAG, "ISO_8859_1 must be supported!", e);
            return new byte[0];
        }
    }

    private Integer getIntegerFromPartColumn(Cursor cursor, int i) {
        if (!cursor.isNull(i)) {
            return Integer.valueOf(cursor.getInt(i));
        }
        return null;
    }

    public static PduPersister getPduPersister(Context context) {
        if (sPersister == null || !context.equals(sPersister.mContext)) {
            sPersister = new PduPersister(context);
        }
        return sPersister;
    }

    private void loadAddress(long j, PduHeaders pduHeaders) {
        Cursor query = SqliteWrapper.query(this.mContext, this.mContentResolver, Uri.parse("content://mms/" + j + "/addr"), new String[]{"address", Telephony.Mms.Addr.CHARSET, "type"}, null, null, null);
        if (query != null) {
            while (query.moveToNext()) {
                try {
                    String string = query.getString(0);
                    if (!TextUtils.isEmpty(string)) {
                        int i = query.getInt(2);
                        switch (i) {
                            case 129:
                            case 130:
                            case 151:
                                pduHeaders.appendEncodedStringValue(new EncodedStringValue(query.getInt(1), getBytes(string)), i);
                                continue;
                            case 137:
                                pduHeaders.setEncodedStringValue(new EncodedStringValue(query.getInt(1), getBytes(string)), i);
                                continue;
                            default:
                                Log.e(TAG, "Unknown address type: " + i);
                                continue;
                        }
                    }
                } finally {
                    query.close();
                }
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:73:0x0152 A[SYNTHETIC, Splitter:B:73:0x0152] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.google.android.mms.pdu.PduPart[] loadParts(long r11) throws com.google.android.mms.MmsException {
        /*
            r10 = this;
            r7 = 0
            r4 = 0
            java.lang.String r0 = "Failed to close stream"
            java.lang.String r0 = "PduPersister"
            android.content.Context r0 = r10.mContext
            android.content.ContentResolver r1 = r10.mContentResolver
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "content://mms/"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r11)
            java.lang.String r3 = "/part"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            android.net.Uri r2 = android.net.Uri.parse(r2)
            java.lang.String[] r3 = com.google.android.mms.pdu.PduPersister.PART_PROJECTION
            r5 = r4
            r6 = r4
            android.database.Cursor r0 = vc.lx.sms.util.SqliteWrapper.query(r0, r1, r2, r3, r4, r5, r6)
            if (r0 == 0) goto L_0x0037
            int r1 = r0.getCount()     // Catch:{ all -> 0x010e }
            if (r1 != 0) goto L_0x003e
        L_0x0037:
            if (r0 == 0) goto L_0x003c
            r0.close()
        L_0x003c:
            r0 = r4
        L_0x003d:
            return r0
        L_0x003e:
            int r1 = r0.getCount()     // Catch:{ all -> 0x010e }
            com.google.android.mms.pdu.PduPart[] r1 = new com.google.android.mms.pdu.PduPart[r1]     // Catch:{ all -> 0x010e }
            r2 = r7
        L_0x0045:
            boolean r3 = r0.moveToNext()     // Catch:{ all -> 0x010e }
            if (r3 == 0) goto L_0x015f
            com.google.android.mms.pdu.PduPart r3 = new com.google.android.mms.pdu.PduPart     // Catch:{ all -> 0x010e }
            r3.<init>()     // Catch:{ all -> 0x010e }
            r5 = 1
            java.lang.Integer r5 = r10.getIntegerFromPartColumn(r0, r5)     // Catch:{ all -> 0x010e }
            if (r5 == 0) goto L_0x005e
            int r5 = r5.intValue()     // Catch:{ all -> 0x010e }
            r3.setCharset(r5)     // Catch:{ all -> 0x010e }
        L_0x005e:
            r5 = 2
            byte[] r5 = r10.getByteArrayFromPartColumn(r0, r5)     // Catch:{ all -> 0x010e }
            if (r5 == 0) goto L_0x0068
            r3.setContentDisposition(r5)     // Catch:{ all -> 0x010e }
        L_0x0068:
            r5 = 3
            byte[] r5 = r10.getByteArrayFromPartColumn(r0, r5)     // Catch:{ all -> 0x010e }
            if (r5 == 0) goto L_0x0072
            r3.setContentId(r5)     // Catch:{ all -> 0x010e }
        L_0x0072:
            r5 = 4
            byte[] r5 = r10.getByteArrayFromPartColumn(r0, r5)     // Catch:{ all -> 0x010e }
            if (r5 == 0) goto L_0x007c
            r3.setContentLocation(r5)     // Catch:{ all -> 0x010e }
        L_0x007c:
            r5 = 5
            byte[] r5 = r10.getByteArrayFromPartColumn(r0, r5)     // Catch:{ all -> 0x010e }
            if (r5 == 0) goto L_0x0106
            r3.setContentType(r5)     // Catch:{ all -> 0x010e }
            r6 = 6
            byte[] r6 = r10.getByteArrayFromPartColumn(r0, r6)     // Catch:{ all -> 0x010e }
            if (r6 == 0) goto L_0x0090
            r3.setFilename(r6)     // Catch:{ all -> 0x010e }
        L_0x0090:
            r6 = 7
            byte[] r6 = r10.getByteArrayFromPartColumn(r0, r6)     // Catch:{ all -> 0x010e }
            if (r6 == 0) goto L_0x009a
            r3.setName(r6)     // Catch:{ all -> 0x010e }
        L_0x009a:
            r6 = 0
            long r6 = r0.getLong(r6)     // Catch:{ all -> 0x010e }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ all -> 0x010e }
            r8.<init>()     // Catch:{ all -> 0x010e }
            java.lang.String r9 = "content://mms/part/"
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ all -> 0x010e }
            java.lang.StringBuilder r6 = r8.append(r6)     // Catch:{ all -> 0x010e }
            java.lang.String r6 = r6.toString()     // Catch:{ all -> 0x010e }
            android.net.Uri r6 = android.net.Uri.parse(r6)     // Catch:{ all -> 0x010e }
            r3.setDataUri(r6)     // Catch:{ all -> 0x010e }
            java.lang.String r5 = toIsoString(r5)     // Catch:{ all -> 0x010e }
            boolean r7 = com.google.android.mms.ContentType.isImageType(r5)     // Catch:{ all -> 0x010e }
            if (r7 != 0) goto L_0x00ff
            boolean r7 = com.google.android.mms.ContentType.isAudioType(r5)     // Catch:{ all -> 0x010e }
            if (r7 != 0) goto L_0x00ff
            boolean r7 = com.google.android.mms.ContentType.isVideoType(r5)     // Catch:{ all -> 0x010e }
            if (r7 != 0) goto L_0x00ff
            java.io.ByteArrayOutputStream r7 = new java.io.ByteArrayOutputStream     // Catch:{ all -> 0x010e }
            r7.<init>()     // Catch:{ all -> 0x010e }
            java.lang.String r8 = "text/plain"
            boolean r8 = r8.equals(r5)     // Catch:{ all -> 0x010e }
            if (r8 != 0) goto L_0x00e4
            java.lang.String r8 = "application/smil"
            boolean r5 = r8.equals(r5)     // Catch:{ all -> 0x010e }
            if (r5 == 0) goto L_0x0115
        L_0x00e4:
            r5 = 8
            java.lang.String r5 = r0.getString(r5)     // Catch:{ all -> 0x010e }
            com.google.android.mms.pdu.EncodedStringValue r6 = new com.google.android.mms.pdu.EncodedStringValue     // Catch:{ all -> 0x010e }
            r6.<init>(r5)     // Catch:{ all -> 0x010e }
            byte[] r5 = r6.getTextString()     // Catch:{ all -> 0x010e }
            r6 = 0
            int r8 = r5.length     // Catch:{ all -> 0x010e }
            r7.write(r5, r6, r8)     // Catch:{ all -> 0x010e }
        L_0x00f8:
            byte[] r5 = r7.toByteArray()     // Catch:{ all -> 0x010e }
            r3.setData(r5)     // Catch:{ all -> 0x010e }
        L_0x00ff:
            int r5 = r2 + 1
            r1[r2] = r3     // Catch:{ all -> 0x010e }
            r2 = r5
            goto L_0x0045
        L_0x0106:
            com.google.android.mms.MmsException r1 = new com.google.android.mms.MmsException     // Catch:{ all -> 0x010e }
            java.lang.String r2 = "Content-Type must be set."
            r1.<init>(r2)     // Catch:{ all -> 0x010e }
            throw r1     // Catch:{ all -> 0x010e }
        L_0x010e:
            r1 = move-exception
            if (r0 == 0) goto L_0x0114
            r0.close()
        L_0x0114:
            throw r1
        L_0x0115:
            android.content.ContentResolver r5 = r10.mContentResolver     // Catch:{ IOException -> 0x013d, all -> 0x0167 }
            java.io.InputStream r5 = r5.openInputStream(r6)     // Catch:{ IOException -> 0x013d, all -> 0x0167 }
            r6 = 256(0x100, float:3.59E-43)
            byte[] r6 = new byte[r6]     // Catch:{ IOException -> 0x016d, all -> 0x016a }
            int r8 = r5.read(r6)     // Catch:{ IOException -> 0x016d, all -> 0x016a }
        L_0x0123:
            if (r8 < 0) goto L_0x012e
            r9 = 0
            r7.write(r6, r9, r8)     // Catch:{ IOException -> 0x016d, all -> 0x016a }
            int r8 = r5.read(r6)     // Catch:{ IOException -> 0x016d, all -> 0x016a }
            goto L_0x0123
        L_0x012e:
            if (r5 == 0) goto L_0x00f8
            r5.close()     // Catch:{ IOException -> 0x0134 }
            goto L_0x00f8
        L_0x0134:
            r5 = move-exception
            java.lang.String r6 = "PduPersister"
            java.lang.String r8 = "Failed to close stream"
            android.util.Log.e(r6, r8, r5)     // Catch:{ all -> 0x010e }
            goto L_0x00f8
        L_0x013d:
            r1 = move-exception
            r2 = r4
        L_0x013f:
            java.lang.String r3 = "PduPersister"
            java.lang.String r4 = "Failed to load part data"
            android.util.Log.e(r3, r4, r1)     // Catch:{ all -> 0x014f }
            r0.close()     // Catch:{ all -> 0x014f }
            com.google.android.mms.MmsException r3 = new com.google.android.mms.MmsException     // Catch:{ all -> 0x014f }
            r3.<init>(r1)     // Catch:{ all -> 0x014f }
            throw r3     // Catch:{ all -> 0x014f }
        L_0x014f:
            r1 = move-exception
        L_0x0150:
            if (r2 == 0) goto L_0x0155
            r2.close()     // Catch:{ IOException -> 0x0156 }
        L_0x0155:
            throw r1     // Catch:{ all -> 0x010e }
        L_0x0156:
            r2 = move-exception
            java.lang.String r3 = "PduPersister"
            java.lang.String r4 = "Failed to close stream"
            android.util.Log.e(r3, r4, r2)     // Catch:{ all -> 0x010e }
            goto L_0x0155
        L_0x015f:
            if (r0 == 0) goto L_0x0164
            r0.close()
        L_0x0164:
            r0 = r1
            goto L_0x003d
        L_0x0167:
            r1 = move-exception
            r2 = r4
            goto L_0x0150
        L_0x016a:
            r1 = move-exception
            r2 = r5
            goto L_0x0150
        L_0x016d:
            r1 = move-exception
            r2 = r5
            goto L_0x013f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.mms.pdu.PduPersister.loadParts(long):com.google.android.mms.pdu.PduPart[]");
    }

    private void persistAddress(long j, int i, EncodedStringValue[] encodedStringValueArr) {
        ContentValues contentValues = new ContentValues(3);
        for (EncodedStringValue encodedStringValue : encodedStringValueArr) {
            contentValues.clear();
            contentValues.put("address", toIsoString(encodedStringValue.getTextString()));
            contentValues.put(Telephony.Mms.Addr.CHARSET, Integer.valueOf(encodedStringValue.getCharacterSet()));
            contentValues.put("type", Integer.valueOf(i));
            SqliteWrapper.insert(this.mContext, this.mContentResolver, Uri.parse("content://mms/" + j + "/addr"), contentValues);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0068 A[SYNTHETIC, Splitter:B:18:0x0068] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x006d A[SYNTHETIC, Splitter:B:21:0x006d] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void persistData(com.google.android.mms.pdu.PduPart r10, android.net.Uri r11, java.lang.String r12) throws com.google.android.mms.MmsException {
        /*
            r9 = this;
            r5 = 0
            java.lang.String r7 = "IOException while closing: "
            java.lang.String r6 = "PduPersister"
            r0 = 0
            byte[] r1 = r10.getData()     // Catch:{ FileNotFoundException -> 0x0055, IOException -> 0x012f, all -> 0x0175 }
            java.lang.String r2 = "text/plain"
            boolean r2 = r2.equals(r12)     // Catch:{ FileNotFoundException -> 0x0055, IOException -> 0x012f, all -> 0x0175 }
            if (r2 != 0) goto L_0x001a
            java.lang.String r2 = "application/smil"
            boolean r2 = r2.equals(r12)     // Catch:{ FileNotFoundException -> 0x0055, IOException -> 0x012f, all -> 0x0175 }
            if (r2 == 0) goto L_0x007e
        L_0x001a:
            android.content.ContentValues r0 = new android.content.ContentValues     // Catch:{ FileNotFoundException -> 0x0055, IOException -> 0x012f, all -> 0x0175 }
            r0.<init>()     // Catch:{ FileNotFoundException -> 0x0055, IOException -> 0x012f, all -> 0x0175 }
            java.lang.String r2 = "text"
            com.google.android.mms.pdu.EncodedStringValue r3 = new com.google.android.mms.pdu.EncodedStringValue     // Catch:{ FileNotFoundException -> 0x0055, IOException -> 0x012f, all -> 0x0175 }
            r3.<init>(r1)     // Catch:{ FileNotFoundException -> 0x0055, IOException -> 0x012f, all -> 0x0175 }
            java.lang.String r1 = r3.getString()     // Catch:{ FileNotFoundException -> 0x0055, IOException -> 0x012f, all -> 0x0175 }
            r0.put(r2, r1)     // Catch:{ FileNotFoundException -> 0x0055, IOException -> 0x012f, all -> 0x0175 }
            android.content.ContentResolver r1 = r9.mContentResolver     // Catch:{ FileNotFoundException -> 0x0055, IOException -> 0x012f, all -> 0x0175 }
            r2 = 0
            r3 = 0
            int r0 = r1.update(r11, r0, r2, r3)     // Catch:{ FileNotFoundException -> 0x0055, IOException -> 0x012f, all -> 0x0175 }
            r1 = 1
            if (r0 == r1) goto L_0x0071
            com.google.android.mms.MmsException r0 = new com.google.android.mms.MmsException     // Catch:{ FileNotFoundException -> 0x0055, IOException -> 0x012f, all -> 0x0175 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x0055, IOException -> 0x012f, all -> 0x0175 }
            r1.<init>()     // Catch:{ FileNotFoundException -> 0x0055, IOException -> 0x012f, all -> 0x0175 }
            java.lang.String r2 = "unable to update "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ FileNotFoundException -> 0x0055, IOException -> 0x012f, all -> 0x0175 }
            java.lang.String r2 = r11.toString()     // Catch:{ FileNotFoundException -> 0x0055, IOException -> 0x012f, all -> 0x0175 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ FileNotFoundException -> 0x0055, IOException -> 0x012f, all -> 0x0175 }
            java.lang.String r1 = r1.toString()     // Catch:{ FileNotFoundException -> 0x0055, IOException -> 0x012f, all -> 0x0175 }
            r0.<init>(r1)     // Catch:{ FileNotFoundException -> 0x0055, IOException -> 0x012f, all -> 0x0175 }
            throw r0     // Catch:{ FileNotFoundException -> 0x0055, IOException -> 0x012f, all -> 0x0175 }
        L_0x0055:
            r0 = move-exception
            r1 = r5
            r2 = r5
        L_0x0058:
            java.lang.String r3 = "PduPersister"
            java.lang.String r4 = "Failed to open Input/Output stream."
            android.util.Log.e(r3, r4, r0)     // Catch:{ all -> 0x0065 }
            com.google.android.mms.MmsException r3 = new com.google.android.mms.MmsException     // Catch:{ all -> 0x0065 }
            r3.<init>(r0)     // Catch:{ all -> 0x0065 }
            throw r3     // Catch:{ all -> 0x0065 }
        L_0x0065:
            r0 = move-exception
        L_0x0066:
            if (r2 == 0) goto L_0x006b
            r2.close()     // Catch:{ IOException -> 0x013f }
        L_0x006b:
            if (r1 == 0) goto L_0x0070
            r1.close()     // Catch:{ IOException -> 0x015a }
        L_0x0070:
            throw r0
        L_0x0071:
            r0 = r5
            r1 = r5
        L_0x0073:
            if (r1 == 0) goto L_0x0078
            r1.close()     // Catch:{ IOException -> 0x00f9 }
        L_0x0078:
            if (r0 == 0) goto L_0x007d
            r0.close()     // Catch:{ IOException -> 0x0114 }
        L_0x007d:
            return
        L_0x007e:
            android.content.ContentResolver r2 = r9.mContentResolver     // Catch:{ FileNotFoundException -> 0x0055, IOException -> 0x012f, all -> 0x0175 }
            java.io.OutputStream r2 = r2.openOutputStream(r11)     // Catch:{ FileNotFoundException -> 0x0055, IOException -> 0x012f, all -> 0x0175 }
            if (r1 != 0) goto L_0x00f2
            android.net.Uri r1 = r10.getDataUri()     // Catch:{ FileNotFoundException -> 0x018c, IOException -> 0x0184, all -> 0x017a }
            if (r1 == 0) goto L_0x008e
            if (r1 != r11) goto L_0x00d4
        L_0x008e:
            java.lang.String r1 = "PduPersister"
            java.lang.String r3 = "Can't find data for this part."
            android.util.Log.w(r1, r3)     // Catch:{ FileNotFoundException -> 0x018c, IOException -> 0x0184, all -> 0x017a }
            if (r2 == 0) goto L_0x009a
            r2.close()     // Catch:{ IOException -> 0x00ba }
        L_0x009a:
            if (r5 == 0) goto L_0x007d
            r0.close()     // Catch:{ IOException -> 0x00a0 }
            goto L_0x007d
        L_0x00a0:
            r0 = move-exception
            java.lang.String r1 = "PduPersister"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "IOException while closing: "
            java.lang.StringBuilder r1 = r1.append(r7)
            java.lang.StringBuilder r1 = r1.append(r5)
            java.lang.String r1 = r1.toString()
            android.util.Log.e(r6, r1, r0)
            goto L_0x007d
        L_0x00ba:
            r1 = move-exception
            java.lang.String r3 = "PduPersister"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "IOException while closing: "
            java.lang.StringBuilder r3 = r3.append(r7)
            java.lang.StringBuilder r2 = r3.append(r2)
            java.lang.String r2 = r2.toString()
            android.util.Log.e(r6, r2, r1)
            goto L_0x009a
        L_0x00d4:
            android.content.ContentResolver r0 = r9.mContentResolver     // Catch:{ FileNotFoundException -> 0x018c, IOException -> 0x0184, all -> 0x017a }
            java.io.InputStream r0 = r0.openInputStream(r1)     // Catch:{ FileNotFoundException -> 0x018c, IOException -> 0x0184, all -> 0x017a }
            r1 = 256(0x100, float:3.59E-43)
            byte[] r1 = new byte[r1]     // Catch:{ FileNotFoundException -> 0x00ea, IOException -> 0x0187, all -> 0x017e }
        L_0x00de:
            int r3 = r0.read(r1)     // Catch:{ FileNotFoundException -> 0x00ea, IOException -> 0x0187, all -> 0x017e }
            r4 = -1
            if (r3 == r4) goto L_0x00f0
            r4 = 0
            r2.write(r1, r4, r3)     // Catch:{ FileNotFoundException -> 0x00ea, IOException -> 0x0187, all -> 0x017e }
            goto L_0x00de
        L_0x00ea:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x0058
        L_0x00f0:
            r1 = r2
            goto L_0x0073
        L_0x00f2:
            r2.write(r1)     // Catch:{ FileNotFoundException -> 0x018c, IOException -> 0x0184, all -> 0x017a }
            r0 = r5
            r1 = r2
            goto L_0x0073
        L_0x00f9:
            r2 = move-exception
            java.lang.String r3 = "PduPersister"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "IOException while closing: "
            java.lang.StringBuilder r3 = r3.append(r7)
            java.lang.StringBuilder r1 = r3.append(r1)
            java.lang.String r1 = r1.toString()
            android.util.Log.e(r6, r1, r2)
            goto L_0x0078
        L_0x0114:
            r1 = move-exception
            java.lang.String r2 = "PduPersister"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "IOException while closing: "
            java.lang.StringBuilder r2 = r2.append(r7)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            android.util.Log.e(r6, r0, r1)
            goto L_0x007d
        L_0x012f:
            r0 = move-exception
            r1 = r5
            r2 = r5
        L_0x0132:
            java.lang.String r3 = "PduPersister"
            java.lang.String r4 = "Failed to read/write data."
            android.util.Log.e(r3, r4, r0)     // Catch:{ all -> 0x0065 }
            com.google.android.mms.MmsException r3 = new com.google.android.mms.MmsException     // Catch:{ all -> 0x0065 }
            r3.<init>(r0)     // Catch:{ all -> 0x0065 }
            throw r3     // Catch:{ all -> 0x0065 }
        L_0x013f:
            r3 = move-exception
            java.lang.String r4 = "PduPersister"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "IOException while closing: "
            java.lang.StringBuilder r4 = r4.append(r7)
            java.lang.StringBuilder r2 = r4.append(r2)
            java.lang.String r2 = r2.toString()
            android.util.Log.e(r6, r2, r3)
            goto L_0x006b
        L_0x015a:
            r2 = move-exception
            java.lang.String r3 = "PduPersister"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "IOException while closing: "
            java.lang.StringBuilder r3 = r3.append(r7)
            java.lang.StringBuilder r1 = r3.append(r1)
            java.lang.String r1 = r1.toString()
            android.util.Log.e(r6, r1, r2)
            goto L_0x0070
        L_0x0175:
            r0 = move-exception
            r1 = r5
            r2 = r5
            goto L_0x0066
        L_0x017a:
            r0 = move-exception
            r1 = r5
            goto L_0x0066
        L_0x017e:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x0066
        L_0x0184:
            r0 = move-exception
            r1 = r5
            goto L_0x0132
        L_0x0187:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x0132
        L_0x018c:
            r0 = move-exception
            r1 = r5
            goto L_0x0058
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.mms.pdu.PduPersister.persistData(com.google.android.mms.pdu.PduPart, android.net.Uri, java.lang.String):void");
    }

    private void setEncodedStringValueToHeaders(Cursor cursor, int i, PduHeaders pduHeaders, int i2) {
        String string = cursor.getString(i);
        if (string != null && string.length() > 0) {
            pduHeaders.setEncodedStringValue(new EncodedStringValue(cursor.getInt(CHARSET_COLUMN_INDEX_MAP.get(Integer.valueOf(i2)).intValue()), getBytes(string)), i2);
        }
    }

    private void setLongToHeaders(Cursor cursor, int i, PduHeaders pduHeaders, int i2) {
        if (!cursor.isNull(i)) {
            pduHeaders.setLongInteger(cursor.getLong(i), i2);
        }
    }

    private void setOctetToHeaders(Cursor cursor, int i, PduHeaders pduHeaders, int i2) throws InvalidHeaderValueException {
        if (!cursor.isNull(i)) {
            pduHeaders.setOctet(cursor.getInt(i), i2);
        }
    }

    private void setTextStringToHeaders(Cursor cursor, int i, PduHeaders pduHeaders, int i2) {
        String string = cursor.getString(i);
        if (string != null) {
            pduHeaders.setTextString(getBytes(string), i2);
        }
    }

    public static String toIsoString(byte[] bArr) {
        try {
            return new String(bArr, CharacterSets.MIMENAME_ISO_8859_1);
        } catch (UnsupportedEncodingException e) {
            Log.e(TAG, "ISO_8859_1 must be supported!", e);
            return "";
        }
    }

    private void updateAddress(long j, int i, EncodedStringValue[] encodedStringValueArr) {
        SqliteWrapper.delete(this.mContext, this.mContentResolver, Uri.parse("content://mms/" + j + "/addr"), "type=" + i, null);
        persistAddress(j, i, encodedStringValueArr);
    }

    private void updatePart(Uri uri, PduPart pduPart) throws MmsException {
        ContentValues contentValues = new ContentValues(7);
        int charset = pduPart.getCharset();
        if (charset != 0) {
            contentValues.put(Telephony.Mms.Part.CHARSET, Integer.valueOf(charset));
        }
        if (pduPart.getContentType() != null) {
            String isoString = toIsoString(pduPart.getContentType());
            contentValues.put(Telephony.Mms.Part.CONTENT_TYPE, isoString);
            if (pduPart.getFilename() != null) {
                contentValues.put(Telephony.Mms.Part.FILENAME, new String(pduPart.getFilename()));
            }
            if (pduPart.getName() != null) {
                contentValues.put("name", new String(pduPart.getName()));
            }
            if (pduPart.getContentDisposition() != null) {
                contentValues.put(Telephony.Mms.Part.CONTENT_DISPOSITION, toIsoString(pduPart.getContentDisposition()));
            }
            if (pduPart.getContentId() != null) {
                contentValues.put(Telephony.Mms.Part.CONTENT_ID, toIsoString(pduPart.getContentId()));
            }
            if (pduPart.getContentLocation() != null) {
                contentValues.put(Telephony.Mms.Part.CONTENT_LOCATION, toIsoString(pduPart.getContentLocation()));
            }
            SqliteWrapper.update(this.mContext, this.mContentResolver, uri, contentValues, null, null);
            if (pduPart.getData() != null || uri != pduPart.getDataUri()) {
                persistData(pduPart, uri, isoString);
                return;
            }
            return;
        }
        throw new MmsException("MIME type of the part must be set.");
    }

    public Cursor getPendingMessages(long j) {
        Uri.Builder buildUpon = Telephony.MmsSms.PendingMessages.CONTENT_URI.buildUpon();
        buildUpon.appendQueryParameter(Telephony.TextBasedSmsColumns.PROTOCOL, Phone.APN_TYPE_MMS);
        return SqliteWrapper.query(this.mContext, this.mContentResolver, buildUpon.build(), null, "err_type < ? AND due_time <= ?", new String[]{String.valueOf(10), String.valueOf(j)}, Telephony.MmsSms.PendingMessages.DUE_TIME);
    }

    public GenericPdu load(Uri uri) throws MmsException {
        GenericPdu readRecInd;
        PduPart[] loadParts;
        PduCacheEntry pduCacheEntry = (PduCacheEntry) PDU_CACHE_INSTANCE.get(uri);
        if (pduCacheEntry != null) {
            return pduCacheEntry.getPdu();
        }
        Cursor query = SqliteWrapper.query(this.mContext, this.mContentResolver, uri, PDU_PROJECTION, null, null, null);
        PduHeaders pduHeaders = new PduHeaders();
        long parseId = ContentUris.parseId(uri);
        if (query != null) {
            try {
                if (query.getCount() == 1 && query.moveToFirst()) {
                    int i = query.getInt(1);
                    long j = query.getLong(2);
                    for (Map.Entry next : ENCODED_STRING_COLUMN_INDEX_MAP.entrySet()) {
                        setEncodedStringValueToHeaders(query, ((Integer) next.getValue()).intValue(), pduHeaders, ((Integer) next.getKey()).intValue());
                    }
                    for (Map.Entry next2 : TEXT_STRING_COLUMN_INDEX_MAP.entrySet()) {
                        setTextStringToHeaders(query, ((Integer) next2.getValue()).intValue(), pduHeaders, ((Integer) next2.getKey()).intValue());
                    }
                    for (Map.Entry next3 : OCTET_COLUMN_INDEX_MAP.entrySet()) {
                        setOctetToHeaders(query, ((Integer) next3.getValue()).intValue(), pduHeaders, ((Integer) next3.getKey()).intValue());
                    }
                    for (Map.Entry next4 : LONG_COLUMN_INDEX_MAP.entrySet()) {
                        setLongToHeaders(query, ((Integer) next4.getValue()).intValue(), pduHeaders, ((Integer) next4.getKey()).intValue());
                    }
                    if (parseId == -1) {
                        throw new MmsException("Error! ID of the message: -1.");
                    }
                    loadAddress(parseId, pduHeaders);
                    int octet = pduHeaders.getOctet(140);
                    PduBody pduBody = new PduBody();
                    if ((octet == 132 || octet == 128) && (loadParts = loadParts(parseId)) != null) {
                        for (PduPart addPart : loadParts) {
                            pduBody.addPart(addPart);
                        }
                    }
                    switch (octet) {
                        case 128:
                            readRecInd = new SendReq(pduHeaders, pduBody);
                            break;
                        case 129:
                        case 137:
                        case 138:
                        case 139:
                        case 140:
                        case 141:
                        case 142:
                        case 143:
                        case 144:
                        case 145:
                        case 146:
                        case 147:
                        case 148:
                        case 149:
                        case 150:
                        case 151:
                            throw new MmsException("Unsupported PDU type: " + Integer.toHexString(octet));
                        case 130:
                            readRecInd = new NotificationInd(pduHeaders);
                            break;
                        case 131:
                            readRecInd = new NotifyRespInd(pduHeaders);
                            break;
                        case 132:
                            readRecInd = new RetrieveConf(pduHeaders, pduBody);
                            break;
                        case 133:
                            readRecInd = new AcknowledgeInd(pduHeaders);
                            break;
                        case 134:
                            readRecInd = new DeliveryInd(pduHeaders);
                            break;
                        case 135:
                            readRecInd = new ReadRecInd(pduHeaders);
                            break;
                        case 136:
                            readRecInd = new ReadOrigInd(pduHeaders);
                            break;
                        default:
                            throw new MmsException("Unrecognized PDU type: " + Integer.toHexString(octet));
                    }
                    PDU_CACHE_INSTANCE.put(uri, new PduCacheEntry(readRecInd, i, j));
                    return readRecInd;
                }
            } finally {
                if (query != null) {
                    query.close();
                }
            }
        }
        throw new MmsException("Bad uri: " + uri);
    }

    public Uri move(Uri uri, Uri uri2) throws MmsException {
        long parseId = ContentUris.parseId(uri);
        if (parseId == -1) {
            throw new MmsException("Error! ID of the message: -1.");
        }
        Integer num = MESSAGE_BOX_MAP.get(uri2);
        if (num == null) {
            throw new MmsException("Bad destination, must be one of content://mms/inbox, content://mms/sent, content://mms/drafts, content://mms/outbox, content://mms/temp.");
        }
        ContentValues contentValues = new ContentValues(1);
        contentValues.put(Telephony.BaseMmsColumns.MESSAGE_BOX, num);
        SqliteWrapper.update(this.mContext, this.mContentResolver, uri, contentValues, null, null);
        return ContentUris.withAppendedId(uri2, parseId);
    }

    public Uri persist(GenericPdu genericPdu, Uri uri) throws MmsException {
        long j;
        PduBody body;
        EncodedStringValue[] encodedStringValueArr;
        EncodedStringValue[] encodedStringValues;
        if (uri == null) {
            throw new MmsException("Uri may not be null.");
        } else if (MESSAGE_BOX_MAP.get(uri) == null) {
            throw new MmsException("Bad destination, must be one of content://mms/inbox, content://mms/sent, content://mms/drafts, content://mms/outbox, content://mms/temp.");
        } else {
            PDU_CACHE_INSTANCE.purge(uri);
            PduHeaders pduHeaders = genericPdu.getPduHeaders();
            ContentValues contentValues = new ContentValues();
            for (Map.Entry next : ENCODED_STRING_COLUMN_NAME_MAP.entrySet()) {
                int intValue = ((Integer) next.getKey()).intValue();
                EncodedStringValue encodedStringValue = pduHeaders.getEncodedStringValue(intValue);
                if (encodedStringValue != null) {
                    contentValues.put((String) next.getValue(), toIsoString(encodedStringValue.getTextString()));
                    contentValues.put(CHARSET_COLUMN_NAME_MAP.get(Integer.valueOf(intValue)), Integer.valueOf(encodedStringValue.getCharacterSet()));
                }
            }
            for (Map.Entry next2 : TEXT_STRING_COLUMN_NAME_MAP.entrySet()) {
                byte[] textString = pduHeaders.getTextString(((Integer) next2.getKey()).intValue());
                if (textString != null) {
                    contentValues.put((String) next2.getValue(), toIsoString(textString));
                }
            }
            for (Map.Entry next3 : OCTET_COLUMN_NAME_MAP.entrySet()) {
                int octet = pduHeaders.getOctet(((Integer) next3.getKey()).intValue());
                if (octet != 0) {
                    contentValues.put((String) next3.getValue(), Integer.valueOf(octet));
                }
            }
            for (Map.Entry next4 : LONG_COLUMN_NAME_MAP.entrySet()) {
                long longInteger = pduHeaders.getLongInteger(((Integer) next4.getKey()).intValue());
                if (longInteger != -1) {
                    contentValues.put((String) next4.getValue(), Long.valueOf(longInteger));
                }
            }
            HashMap hashMap = new HashMap(ADDRESS_FIELDS.length);
            for (int i : ADDRESS_FIELDS) {
                if (i == 137) {
                    EncodedStringValue encodedStringValue2 = pduHeaders.getEncodedStringValue(i);
                    encodedStringValues = encodedStringValue2 != null ? new EncodedStringValue[]{encodedStringValue2} : null;
                } else {
                    encodedStringValues = pduHeaders.getEncodedStringValues(i);
                }
                hashMap.put(Integer.valueOf(i), encodedStringValues);
            }
            HashSet hashSet = new HashSet();
            int messageType = genericPdu.getMessageType();
            if (messageType == 130 || messageType == 132 || messageType == 128) {
                switch (messageType) {
                    case 128:
                        encodedStringValueArr = (EncodedStringValue[]) hashMap.get(151);
                        break;
                    case 129:
                    case 131:
                    default:
                        encodedStringValueArr = null;
                        break;
                    case 130:
                    case 132:
                        encodedStringValueArr = (EncodedStringValue[]) hashMap.get(137);
                        break;
                }
                if (encodedStringValueArr != null) {
                    for (EncodedStringValue encodedStringValue3 : encodedStringValueArr) {
                        if (encodedStringValue3 != null) {
                            hashSet.add(encodedStringValue3.getString());
                        }
                    }
                }
                j = Telephony.Threads.getOrCreateThreadId(this.mContext, hashSet);
            } else {
                j = Long.MAX_VALUE;
            }
            contentValues.put("thread_id", Long.valueOf(j));
            long currentTimeMillis = System.currentTimeMillis();
            if ((genericPdu instanceof MultimediaMessagePdu) && (body = ((MultimediaMessagePdu) genericPdu).getBody()) != null) {
                int partsNum = body.getPartsNum();
                for (int i2 = 0; i2 < partsNum; i2++) {
                    persistPart(body.getPart(i2), currentTimeMillis);
                }
            }
            Uri insert = SqliteWrapper.insert(this.mContext, this.mContentResolver, uri, contentValues);
            if (insert == null) {
                throw new MmsException("persist() failed: return null.");
            }
            long parseId = ContentUris.parseId(insert);
            ContentValues contentValues2 = new ContentValues(1);
            contentValues2.put(Telephony.Mms.Part.MSG_ID, Long.valueOf(parseId));
            SqliteWrapper.update(this.mContext, this.mContentResolver, Uri.parse("content://mms/" + currentTimeMillis + "/part"), contentValues2, null, null);
            Uri parse = Uri.parse(uri + "/" + parseId);
            for (int i3 : ADDRESS_FIELDS) {
                EncodedStringValue[] encodedStringValueArr2 = (EncodedStringValue[]) hashMap.get(Integer.valueOf(i3));
                if (encodedStringValueArr2 != null) {
                    persistAddress(parseId, i3, encodedStringValueArr2);
                }
            }
            return parse;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public Uri persistPart(PduPart pduPart, long j) throws MmsException {
        Uri parse = Uri.parse("content://mms/" + j + "/part");
        ContentValues contentValues = new ContentValues(8);
        int charset = pduPart.getCharset();
        if (charset != 0) {
            contentValues.put(Telephony.Mms.Part.CHARSET, Integer.valueOf(charset));
        }
        if (pduPart.getContentType() != null) {
            String isoString = toIsoString(pduPart.getContentType());
            contentValues.put(Telephony.Mms.Part.CONTENT_TYPE, isoString);
            if (ContentType.APP_SMIL.equals(isoString)) {
                contentValues.put(Telephony.Mms.Part.SEQ, (Integer) -1);
            }
            if (pduPart.getFilename() != null) {
                contentValues.put(Telephony.Mms.Part.FILENAME, new String(pduPart.getFilename()));
            }
            if (pduPart.getName() != null) {
                contentValues.put("name", new String(pduPart.getName()));
            }
            if (pduPart.getContentDisposition() != null) {
                contentValues.put(Telephony.Mms.Part.CONTENT_DISPOSITION, toIsoString(pduPart.getContentDisposition()));
            }
            if (pduPart.getContentId() != null) {
                contentValues.put(Telephony.Mms.Part.CONTENT_ID, toIsoString(pduPart.getContentId()));
            }
            if (pduPart.getContentLocation() != null) {
                contentValues.put(Telephony.Mms.Part.CONTENT_LOCATION, toIsoString(pduPart.getContentLocation()));
            }
            Uri insert = SqliteWrapper.insert(this.mContext, this.mContentResolver, parse, contentValues);
            if (insert == null) {
                throw new MmsException("Failed to persist part, return null.");
            }
            persistData(pduPart, insert, isoString);
            pduPart.setDataUri(insert);
            return insert;
        }
        throw new MmsException("MIME type of the part must be set.");
    }

    public void release() {
        SqliteWrapper.delete(this.mContext, this.mContentResolver, Uri.parse(TEMPORARY_DRM_OBJECT_URI), null, null);
    }

    public void updateHeaders(Uri uri, SendReq sendReq) {
        EncodedStringValue[] encodedStringValues;
        PDU_CACHE_INSTANCE.purge(uri);
        ContentValues contentValues = new ContentValues(10);
        byte[] contentType = sendReq.getContentType();
        if (contentType != null) {
            contentValues.put(Telephony.BaseMmsColumns.CONTENT_TYPE, toIsoString(contentType));
        }
        long date = sendReq.getDate();
        if (date != -1) {
            contentValues.put("date", Long.valueOf(date));
        }
        int deliveryReport = sendReq.getDeliveryReport();
        if (deliveryReport != 0) {
            contentValues.put(Telephony.BaseMmsColumns.DELIVERY_REPORT, Integer.valueOf(deliveryReport));
        }
        long expiry = sendReq.getExpiry();
        if (expiry != -1) {
            contentValues.put(Telephony.BaseMmsColumns.EXPIRY, Long.valueOf(expiry));
        }
        byte[] messageClass = sendReq.getMessageClass();
        if (messageClass != null) {
            contentValues.put(Telephony.BaseMmsColumns.MESSAGE_CLASS, toIsoString(messageClass));
        }
        int priority = sendReq.getPriority();
        if (priority != 0) {
            contentValues.put(Telephony.BaseMmsColumns.PRIORITY, Integer.valueOf(priority));
        }
        int readReport = sendReq.getReadReport();
        if (readReport != 0) {
            contentValues.put(Telephony.BaseMmsColumns.READ_REPORT, Integer.valueOf(readReport));
        }
        byte[] transactionId = sendReq.getTransactionId();
        if (transactionId != null) {
            contentValues.put(Telephony.BaseMmsColumns.TRANSACTION_ID, toIsoString(transactionId));
        }
        EncodedStringValue subject = sendReq.getSubject();
        if (subject != null) {
            contentValues.put(Telephony.BaseMmsColumns.SUBJECT, toIsoString(subject.getTextString()));
            contentValues.put(Telephony.BaseMmsColumns.SUBJECT_CHARSET, Integer.valueOf(subject.getCharacterSet()));
        } else {
            contentValues.put(Telephony.BaseMmsColumns.SUBJECT, "");
        }
        long messageSize = sendReq.getMessageSize();
        if (messageSize > 0) {
            contentValues.put(Telephony.BaseMmsColumns.MESSAGE_SIZE, Long.valueOf(messageSize));
        }
        PduHeaders pduHeaders = sendReq.getPduHeaders();
        HashSet hashSet = new HashSet();
        for (int i : ADDRESS_FIELDS) {
            if (i == 137) {
                EncodedStringValue encodedStringValue = pduHeaders.getEncodedStringValue(i);
                if (encodedStringValue != null) {
                    encodedStringValues = new EncodedStringValue[]{encodedStringValue};
                } else {
                    encodedStringValues = null;
                }
            } else {
                encodedStringValues = pduHeaders.getEncodedStringValues(i);
            }
            if (encodedStringValues != null) {
                updateAddress(ContentUris.parseId(uri), i, encodedStringValues);
                if (i == 151) {
                    for (EncodedStringValue encodedStringValue2 : encodedStringValues) {
                        if (encodedStringValue2 != null) {
                            hashSet.add(encodedStringValue2.getString());
                        }
                    }
                }
            }
        }
        contentValues.put("thread_id", Long.valueOf(Telephony.Threads.getOrCreateThreadId(this.mContext, hashSet)));
        SqliteWrapper.update(this.mContext, this.mContentResolver, uri, contentValues, null, null);
    }

    public void updateParts(Uri uri, PduBody pduBody) throws MmsException {
        PduCacheEntry pduCacheEntry = (PduCacheEntry) PDU_CACHE_INSTANCE.get(uri);
        if (pduCacheEntry != null) {
            ((MultimediaMessagePdu) pduCacheEntry.getPdu()).setBody(pduBody);
        }
        ArrayList arrayList = new ArrayList();
        HashMap hashMap = new HashMap();
        int partsNum = pduBody.getPartsNum();
        StringBuilder append = new StringBuilder().append('(');
        for (int i = 0; i < partsNum; i++) {
            PduPart part = pduBody.getPart(i);
            Uri dataUri = part.getDataUri();
            if (dataUri == null || !dataUri.getAuthority().startsWith(Phone.APN_TYPE_MMS)) {
                arrayList.add(part);
            } else {
                hashMap.put(dataUri, part);
                if (append.length() > 1) {
                    append.append(" AND ");
                }
                append.append("_id");
                append.append("!=");
                DatabaseUtils.appendEscapedSQLString(append, dataUri.getLastPathSegment());
            }
        }
        append.append(')');
        long parseId = ContentUris.parseId(uri);
        SqliteWrapper.delete(this.mContext, this.mContentResolver, Uri.parse(Telephony.Mms.CONTENT_URI + "/" + parseId + "/part"), append.length() > 2 ? append.toString() : null, null);
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            persistPart((PduPart) it.next(), parseId);
        }
        for (Map.Entry entry : hashMap.entrySet()) {
            updatePart((Uri) entry.getKey(), (PduPart) entry.getValue());
        }
    }
}
