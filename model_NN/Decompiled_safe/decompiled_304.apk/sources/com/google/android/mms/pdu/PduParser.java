package com.google.android.mms.pdu;

import android.os.SystemProperties;
import android.util.Log;
import com.android.provider.Telephony;
import com.google.android.mms.ContentType;
import com.google.android.mms.InvalidHeaderValueException;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.HashMap;

public class PduParser {
    static final /* synthetic */ boolean $assertionsDisabled = (!PduParser.class.desiredAssertionStatus());
    private static final boolean DEBUG = false;
    private static final int END_STRING_FLAG = 0;
    private static final int LENGTH_QUOTE = 31;
    private static final boolean LOCAL_LOGV = false;
    private static final String LOG_TAG = "PduParser";
    private static final int LONG_INTEGER_LENGTH_MAX = 8;
    private static final int QUOTE = 127;
    private static final int QUOTED_STRING_FLAG = 34;
    private static final int SHORT_INTEGER_MAX = 127;
    private static final int SHORT_LENGTH_MAX = 30;
    private static final int TEXT_MAX = 127;
    private static final int TEXT_MIN = 32;
    private static final int THE_FIRST_PART = 0;
    private static final int THE_LAST_PART = 1;
    private static final int TYPE_QUOTED_STRING = 1;
    private static final int TYPE_TEXT_STRING = 0;
    private static final int TYPE_TOKEN_STRING = 2;
    private static byte[] mStartParam = null;
    private static byte[] mTypeParam = null;
    private PduBody mBody = null;
    private PduHeaders mHeaders = null;
    private ByteArrayInputStream mPduDataStream = null;

    public PduParser(byte[] bArr) {
        this.mPduDataStream = new ByteArrayInputStream(bArr);
    }

    protected static boolean checkMandatoryHeader(PduHeaders pduHeaders) {
        if (pduHeaders == null) {
            return false;
        }
        int octet = pduHeaders.getOctet(140);
        if (pduHeaders.getOctet(141) == 0) {
            return false;
        }
        switch (octet) {
            case 128:
                if (pduHeaders.getTextString(132) == null) {
                    return false;
                }
                if (pduHeaders.getEncodedStringValue(137) == null) {
                    return false;
                }
                if (pduHeaders.getTextString(152) == null) {
                    return false;
                }
                break;
            case 129:
                if (pduHeaders.getOctet(146) == 0) {
                    return false;
                }
                if (pduHeaders.getTextString(152) == null) {
                    return false;
                }
                break;
            case 130:
                if (pduHeaders.getTextString(131) == null) {
                    return false;
                }
                if (-1 == pduHeaders.getLongInteger(136)) {
                    return false;
                }
                if (pduHeaders.getTextString(138) == null) {
                    return false;
                }
                if (-1 == pduHeaders.getLongInteger(142)) {
                    return false;
                }
                if (pduHeaders.getTextString(152) == null) {
                    return false;
                }
                break;
            case 131:
                if (pduHeaders.getOctet(149) == 0) {
                    return false;
                }
                if (pduHeaders.getTextString(152) == null) {
                    return false;
                }
                break;
            case 132:
                if (pduHeaders.getTextString(132) == null) {
                    return false;
                }
                if (-1 == pduHeaders.getLongInteger(133)) {
                    return false;
                }
                break;
            case 133:
                if (pduHeaders.getTextString(152) == null) {
                    return false;
                }
                break;
            case 134:
                if (-1 == pduHeaders.getLongInteger(133)) {
                    return false;
                }
                if (pduHeaders.getTextString(139) == null) {
                    return false;
                }
                if (pduHeaders.getOctet(149) == 0) {
                    return false;
                }
                if (pduHeaders.getEncodedStringValues(151) == null) {
                    return false;
                }
                break;
            case 135:
                if (pduHeaders.getEncodedStringValue(137) == null) {
                    return false;
                }
                if (pduHeaders.getTextString(139) == null) {
                    return false;
                }
                if (pduHeaders.getOctet(155) == 0) {
                    return false;
                }
                if (pduHeaders.getEncodedStringValues(151) == null) {
                    return false;
                }
                break;
            case 136:
                if (-1 == pduHeaders.getLongInteger(133)) {
                    return false;
                }
                if (pduHeaders.getEncodedStringValue(137) == null) {
                    return false;
                }
                if (pduHeaders.getTextString(139) == null) {
                    return false;
                }
                if (pduHeaders.getOctet(155) == 0) {
                    return false;
                }
                if (pduHeaders.getEncodedStringValues(151) == null) {
                    return false;
                }
                break;
            default:
                return false;
        }
        return true;
    }

    private static int checkPartPosition(PduPart pduPart) {
        byte[] contentType;
        byte[] contentId;
        if (!$assertionsDisabled && pduPart == null) {
            throw new AssertionError();
        } else if (mTypeParam == null && mStartParam == null) {
            return 1;
        } else {
            if (mStartParam == null || (contentId = pduPart.getContentId()) == null || true != Arrays.equals(mStartParam, contentId)) {
                return (mTypeParam == null || (contentType = pduPart.getContentType()) == null || true != Arrays.equals(mTypeParam, contentType)) ? 1 : 0;
            }
            return 0;
        }
    }

    protected static int extractByteValue(ByteArrayInputStream byteArrayInputStream) {
        if ($assertionsDisabled || byteArrayInputStream != null) {
            int read = byteArrayInputStream.read();
            if ($assertionsDisabled || -1 != read) {
                return read & 255;
            }
            throw new AssertionError();
        }
        throw new AssertionError();
    }

    protected static byte[] getWapString(ByteArrayInputStream byteArrayInputStream, int i) {
        if ($assertionsDisabled || byteArrayInputStream != null) {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            int read = byteArrayInputStream.read();
            if ($assertionsDisabled || -1 != read) {
                while (-1 != read && read != 0) {
                    if (i == 2) {
                        if (isTokenCharacter(read)) {
                            byteArrayOutputStream.write(read);
                        }
                    } else if (isText(read)) {
                        byteArrayOutputStream.write(read);
                    }
                    read = byteArrayInputStream.read();
                    if (!$assertionsDisabled && -1 == read) {
                        throw new AssertionError();
                    }
                }
                if (byteArrayOutputStream.size() > 0) {
                    return byteArrayOutputStream.toByteArray();
                }
                return null;
            }
            throw new AssertionError();
        }
        throw new AssertionError();
    }

    protected static boolean isText(int i) {
        if ((i >= TEXT_MIN && i <= 126) || (i >= 128 && i <= 255)) {
            return true;
        }
        switch (i) {
            case 9:
            case 10:
            case 13:
                return true;
            case 11:
            case 12:
            default:
                return false;
        }
    }

    protected static boolean isTokenCharacter(int i) {
        if (i < 33 || i > 126) {
            return false;
        }
        switch (i) {
            case QUOTED_STRING_FLAG /*34*/:
            case 40:
            case 41:
            case 44:
            case 47:
            case 58:
            case 59:
            case 60:
            case 61:
            case 62:
            case 63:
            case Telephony.TextBasedSmsColumns.STATUS_PENDING:
            case SystemProperties.PROP_VALUE_MAX:
            case 92:
            case 93:
            case 123:
            case 125:
                return false;
            default:
                return true;
        }
    }

    private static void log(String str) {
    }

    protected static byte[] parseContentType(ByteArrayInputStream byteArrayInputStream, HashMap<Integer, Object> hashMap) {
        byte[] parseWapString;
        if ($assertionsDisabled || byteArrayInputStream != null) {
            byteArrayInputStream.mark(1);
            int read = byteArrayInputStream.read();
            if ($assertionsDisabled || -1 != read) {
                byteArrayInputStream.reset();
                int i = read & 255;
                if (i >= TEXT_MIN) {
                    return i <= 127 ? parseWapString(byteArrayInputStream, 0) : PduContentTypes.contentTypes[parseShortInteger(byteArrayInputStream)].getBytes();
                }
                int parseValueLength = parseValueLength(byteArrayInputStream);
                int available = byteArrayInputStream.available();
                byteArrayInputStream.mark(1);
                int read2 = byteArrayInputStream.read();
                if ($assertionsDisabled || -1 != read2) {
                    byteArrayInputStream.reset();
                    int i2 = read2 & 255;
                    if (i2 >= TEXT_MIN && i2 <= 127) {
                        parseWapString = parseWapString(byteArrayInputStream, 0);
                    } else if (i2 > 127) {
                        int parseShortInteger = parseShortInteger(byteArrayInputStream);
                        if (parseShortInteger < PduContentTypes.contentTypes.length) {
                            parseWapString = PduContentTypes.contentTypes[parseShortInteger].getBytes();
                        } else {
                            byteArrayInputStream.reset();
                            parseWapString = parseWapString(byteArrayInputStream, 0);
                        }
                    } else {
                        Log.e(LOG_TAG, "Corrupt content-type");
                        return PduContentTypes.contentTypes[0].getBytes();
                    }
                    int available2 = parseValueLength - (available - byteArrayInputStream.available());
                    if (available2 > 0) {
                        parseContentTypeParams(byteArrayInputStream, hashMap, Integer.valueOf(available2));
                    }
                    if (available2 >= 0) {
                        return parseWapString;
                    }
                    Log.e(LOG_TAG, "Corrupt MMS message");
                    return PduContentTypes.contentTypes[0].getBytes();
                }
                throw new AssertionError();
            }
            throw new AssertionError();
        }
        throw new AssertionError();
    }

    protected static void parseContentTypeParams(ByteArrayInputStream byteArrayInputStream, HashMap<Integer, Object> hashMap, Integer num) {
        if (!$assertionsDisabled && byteArrayInputStream == null) {
            throw new AssertionError();
        } else if ($assertionsDisabled || num.intValue() > 0) {
            int available = byteArrayInputStream.available();
            int intValue = num.intValue();
            while (intValue > 0) {
                int read = byteArrayInputStream.read();
                if ($assertionsDisabled || -1 != read) {
                    intValue--;
                    switch (read) {
                        case 129:
                            byteArrayInputStream.mark(1);
                            int extractByteValue = extractByteValue(byteArrayInputStream);
                            byteArrayInputStream.reset();
                            if ((extractByteValue <= TEXT_MIN || extractByteValue >= 127) && extractByteValue != 0) {
                                int parseIntegerValue = (int) parseIntegerValue(byteArrayInputStream);
                                if (hashMap != null) {
                                    hashMap.put(129, Integer.valueOf(parseIntegerValue));
                                }
                            } else {
                                byte[] parseWapString = parseWapString(byteArrayInputStream, 0);
                                try {
                                    hashMap.put(129, Integer.valueOf(CharacterSets.getMibEnumValue(new String(parseWapString))));
                                } catch (UnsupportedEncodingException e) {
                                    Log.e(LOG_TAG, Arrays.toString(parseWapString), e);
                                    hashMap.put(129, 0);
                                }
                            }
                            intValue = num.intValue() - (available - byteArrayInputStream.available());
                            break;
                        case 131:
                        case 137:
                            byteArrayInputStream.mark(1);
                            int extractByteValue2 = extractByteValue(byteArrayInputStream);
                            byteArrayInputStream.reset();
                            if (extractByteValue2 > 127) {
                                int parseShortInteger = parseShortInteger(byteArrayInputStream);
                                if (parseShortInteger < PduContentTypes.contentTypes.length) {
                                    hashMap.put(131, PduContentTypes.contentTypes[parseShortInteger].getBytes());
                                }
                            } else {
                                byte[] parseWapString2 = parseWapString(byteArrayInputStream, 0);
                                if (!(parseWapString2 == null || hashMap == null)) {
                                    hashMap.put(131, parseWapString2);
                                }
                            }
                            intValue = num.intValue() - (available - byteArrayInputStream.available());
                            break;
                        case 133:
                        case 151:
                            byte[] parseWapString3 = parseWapString(byteArrayInputStream, 0);
                            if (!(parseWapString3 == null || hashMap == null)) {
                                hashMap.put(151, parseWapString3);
                            }
                            intValue = num.intValue() - (available - byteArrayInputStream.available());
                            break;
                        case 138:
                        case 153:
                            byte[] parseWapString4 = parseWapString(byteArrayInputStream, 0);
                            if (!(parseWapString4 == null || hashMap == null)) {
                                hashMap.put(153, parseWapString4);
                            }
                            intValue = num.intValue() - (available - byteArrayInputStream.available());
                            break;
                        default:
                            if (-1 != skipWapValue(byteArrayInputStream, intValue)) {
                                intValue = 0;
                                break;
                            } else {
                                Log.e(LOG_TAG, "Corrupt Content-Type");
                                break;
                            }
                    }
                } else {
                    throw new AssertionError();
                }
            }
            if (intValue != 0) {
                Log.e(LOG_TAG, "Corrupt Content-Type");
            }
        } else {
            throw new AssertionError();
        }
    }

    protected static EncodedStringValue parseEncodedStringValue(ByteArrayInputStream byteArrayInputStream) {
        int i;
        if ($assertionsDisabled || byteArrayInputStream != null) {
            byteArrayInputStream.mark(1);
            int read = byteArrayInputStream.read();
            if ($assertionsDisabled || -1 != read) {
                int i2 = read & 255;
                byteArrayInputStream.reset();
                if (i2 < TEXT_MIN) {
                    parseValueLength(byteArrayInputStream);
                    i = parseShortInteger(byteArrayInputStream);
                } else {
                    i = 0;
                }
                byte[] parseWapString = parseWapString(byteArrayInputStream, 0);
                if (i == 0) {
                    return new EncodedStringValue(parseWapString);
                }
                try {
                    return new EncodedStringValue(i, parseWapString);
                } catch (Exception e) {
                    return null;
                }
            } else {
                throw new AssertionError();
            }
        } else {
            throw new AssertionError();
        }
    }

    protected static long parseIntegerValue(ByteArrayInputStream byteArrayInputStream) {
        if ($assertionsDisabled || byteArrayInputStream != null) {
            byteArrayInputStream.mark(1);
            int read = byteArrayInputStream.read();
            if ($assertionsDisabled || -1 != read) {
                byteArrayInputStream.reset();
                return read > 127 ? (long) parseShortInteger(byteArrayInputStream) : parseLongInteger(byteArrayInputStream);
            }
            throw new AssertionError();
        }
        throw new AssertionError();
    }

    protected static long parseLongInteger(ByteArrayInputStream byteArrayInputStream) {
        if ($assertionsDisabled || byteArrayInputStream != null) {
            int read = byteArrayInputStream.read();
            if ($assertionsDisabled || -1 != read) {
                int i = read & 255;
                if (i > 8) {
                    throw new RuntimeException("Octet count greater than 8 and I can't represent that!");
                }
                long j = 0;
                int i2 = 0;
                while (i2 < i) {
                    int read2 = byteArrayInputStream.read();
                    if ($assertionsDisabled || -1 != read2) {
                        j = (j << 8) + ((long) (read2 & 255));
                        i2++;
                    } else {
                        throw new AssertionError();
                    }
                }
                return j;
            }
            throw new AssertionError();
        }
        throw new AssertionError();
    }

    protected static boolean parsePartHeaders(ByteArrayInputStream byteArrayInputStream, PduPart pduPart, int i) {
        if (!$assertionsDisabled && byteArrayInputStream == null) {
            throw new AssertionError();
        } else if (!$assertionsDisabled && pduPart == null) {
            throw new AssertionError();
        } else if ($assertionsDisabled || i > 0) {
            int available = byteArrayInputStream.available();
            int i2 = i;
            while (i2 > 0) {
                int read = byteArrayInputStream.read();
                if ($assertionsDisabled || -1 != read) {
                    int i3 = i2 - 1;
                    if (read > 127) {
                        switch (read) {
                            case 142:
                                byte[] parseWapString = parseWapString(byteArrayInputStream, 0);
                                if (parseWapString != null) {
                                    pduPart.setContentLocation(parseWapString);
                                }
                                i2 = i - (available - byteArrayInputStream.available());
                                continue;
                            case 174:
                            case PduPart.P_CONTENT_DISPOSITION:
                                int parseValueLength = parseValueLength(byteArrayInputStream);
                                byteArrayInputStream.mark(1);
                                int available2 = byteArrayInputStream.available();
                                int read2 = byteArrayInputStream.read();
                                if (read2 == 128) {
                                    pduPart.setContentDisposition(PduPart.DISPOSITION_FROM_DATA);
                                } else if (read2 == 129) {
                                    pduPart.setContentDisposition(PduPart.DISPOSITION_ATTACHMENT);
                                } else if (read2 == 130) {
                                    pduPart.setContentDisposition(PduPart.DISPOSITION_INLINE);
                                } else {
                                    byteArrayInputStream.reset();
                                    pduPart.setContentDisposition(parseWapString(byteArrayInputStream, 0));
                                }
                                if (available2 - byteArrayInputStream.available() < parseValueLength) {
                                    if (byteArrayInputStream.read() == 152) {
                                        pduPart.setFilename(parseWapString(byteArrayInputStream, 0));
                                    }
                                    int available3 = byteArrayInputStream.available();
                                    if (available2 - available3 < parseValueLength) {
                                        int i4 = parseValueLength - (available2 - available3);
                                        byteArrayInputStream.read(new byte[i4], 0, i4);
                                    }
                                }
                                i2 = i - (available - byteArrayInputStream.available());
                                continue;
                            case 192:
                                byte[] parseWapString2 = parseWapString(byteArrayInputStream, 1);
                                if (parseWapString2 != null) {
                                    pduPart.setContentId(parseWapString2);
                                }
                                i2 = i - (available - byteArrayInputStream.available());
                                continue;
                            default:
                                if (-1 == skipWapValue(byteArrayInputStream, i3)) {
                                    Log.e(LOG_TAG, "Corrupt Part headers");
                                    return false;
                                }
                                i2 = 0;
                                continue;
                        }
                    } else if (read >= TEXT_MIN && read <= 127) {
                        byte[] parseWapString3 = parseWapString(byteArrayInputStream, 0);
                        byte[] parseWapString4 = parseWapString(byteArrayInputStream, 0);
                        if (true == "Content-Transfer-Encoding".equalsIgnoreCase(new String(parseWapString3))) {
                            pduPart.setContentTransferEncoding(parseWapString4);
                        }
                        i2 = i - (available - byteArrayInputStream.available());
                    } else if (-1 == skipWapValue(byteArrayInputStream, i3)) {
                        Log.e(LOG_TAG, "Corrupt Part headers");
                        return false;
                    } else {
                        i2 = 0;
                    }
                } else {
                    throw new AssertionError();
                }
            }
            if (i2 == 0) {
                return true;
            }
            Log.e(LOG_TAG, "Corrupt Part headers");
            return false;
        } else {
            throw new AssertionError();
        }
    }

    protected static PduBody parseParts(ByteArrayInputStream byteArrayInputStream) {
        if (byteArrayInputStream == null) {
            return null;
        }
        int parseUnsignedInt = parseUnsignedInt(byteArrayInputStream);
        PduBody pduBody = new PduBody();
        for (int i = 0; i < parseUnsignedInt; i++) {
            int parseUnsignedInt2 = parseUnsignedInt(byteArrayInputStream);
            int parseUnsignedInt3 = parseUnsignedInt(byteArrayInputStream);
            PduPart pduPart = new PduPart();
            int available = byteArrayInputStream.available();
            if (available <= 0) {
                return null;
            }
            HashMap hashMap = new HashMap();
            byte[] parseContentType = parseContentType(byteArrayInputStream, hashMap);
            if (parseContentType != null) {
                pduPart.setContentType(parseContentType);
            } else {
                pduPart.setContentType(PduContentTypes.contentTypes[0].getBytes());
            }
            byte[] bArr = (byte[]) hashMap.get(151);
            if (bArr != null) {
                pduPart.setName(bArr);
            }
            Integer num = (Integer) hashMap.get(129);
            if (num != null) {
                pduPart.setCharset(num.intValue());
            }
            int available2 = parseUnsignedInt2 - (available - byteArrayInputStream.available());
            if (available2 > 0) {
                if (!parsePartHeaders(byteArrayInputStream, pduPart, available2)) {
                    return null;
                }
            } else if (available2 < 0) {
                return null;
            }
            if (pduPart.getContentLocation() == null && pduPart.getName() == null && pduPart.getFilename() == null && pduPart.getContentId() == null) {
                pduPart.setContentLocation(Long.toOctalString(System.currentTimeMillis()).getBytes());
            }
            if (parseUnsignedInt3 > 0) {
                byte[] bArr2 = new byte[parseUnsignedInt3];
                byteArrayInputStream.read(bArr2, 0, parseUnsignedInt3);
                byte[] contentTransferEncoding = pduPart.getContentTransferEncoding();
                if (contentTransferEncoding != null) {
                    String str = new String(contentTransferEncoding);
                    if (str.equalsIgnoreCase(PduPart.P_BASE64)) {
                        bArr2 = Base64.decodeBase64(bArr2);
                    } else if (str.equalsIgnoreCase(PduPart.P_QUOTED_PRINTABLE)) {
                        bArr2 = QuotedPrintable.decodeQuotedPrintable(bArr2);
                    }
                }
                if (bArr2 == null) {
                    log("Decode part data error!");
                    return null;
                }
                pduPart.setData(bArr2);
            }
            if (checkPartPosition(pduPart) == 0) {
                pduBody.addPart(0, pduPart);
            } else {
                pduBody.addPart(pduPart);
            }
        }
        return pduBody;
    }

    protected static int parseShortInteger(ByteArrayInputStream byteArrayInputStream) {
        if ($assertionsDisabled || byteArrayInputStream != null) {
            int read = byteArrayInputStream.read();
            if ($assertionsDisabled || -1 != read) {
                return read & 127;
            }
            throw new AssertionError();
        }
        throw new AssertionError();
    }

    protected static int parseUnsignedInt(ByteArrayInputStream byteArrayInputStream) {
        if ($assertionsDisabled || byteArrayInputStream != null) {
            int i = 0;
            int read = byteArrayInputStream.read();
            if (read == -1) {
                return read;
            }
            do {
                int i2 = read;
                int i3 = i;
                int i4 = i2;
                if ((i4 & 128) != 0) {
                    i = (i4 & 127) | (i3 << 7);
                    read = byteArrayInputStream.read();
                } else {
                    return (i4 & 127) | (i3 << 7);
                }
            } while (read != -1);
            return read;
        }
        throw new AssertionError();
    }

    protected static int parseValueLength(ByteArrayInputStream byteArrayInputStream) {
        if ($assertionsDisabled || byteArrayInputStream != null) {
            int read = byteArrayInputStream.read();
            if ($assertionsDisabled || -1 != read) {
                int i = read & 255;
                if (i <= SHORT_LENGTH_MAX) {
                    return i;
                }
                if (i == 31) {
                    return parseUnsignedInt(byteArrayInputStream);
                }
                throw new RuntimeException("Value length > LENGTH_QUOTE!");
            }
            throw new AssertionError();
        }
        throw new AssertionError();
    }

    protected static byte[] parseWapString(ByteArrayInputStream byteArrayInputStream, int i) {
        if ($assertionsDisabled || byteArrayInputStream != null) {
            byteArrayInputStream.mark(1);
            int read = byteArrayInputStream.read();
            if ($assertionsDisabled || -1 != read) {
                if (1 == i && QUOTED_STRING_FLAG == read) {
                    byteArrayInputStream.mark(1);
                } else if (i == 0 && 127 == read) {
                    byteArrayInputStream.mark(1);
                } else {
                    byteArrayInputStream.reset();
                }
                return getWapString(byteArrayInputStream, i);
            }
            throw new AssertionError();
        }
        throw new AssertionError();
    }

    protected static int skipWapValue(ByteArrayInputStream byteArrayInputStream, int i) {
        if ($assertionsDisabled || byteArrayInputStream != null) {
            int read = byteArrayInputStream.read(new byte[i], 0, i);
            if (read < i) {
                return -1;
            }
            return read;
        }
        throw new AssertionError();
    }

    public GenericPdu parse() {
        if (this.mPduDataStream == null) {
            return null;
        }
        this.mHeaders = parseHeaders(this.mPduDataStream);
        if (this.mHeaders == null) {
            return null;
        }
        int octet = this.mHeaders.getOctet(140);
        if (!checkMandatoryHeader(this.mHeaders)) {
            log("check mandatory headers failed!");
            return null;
        }
        if (128 == octet || 132 == octet) {
            this.mBody = parseParts(this.mPduDataStream);
            if (this.mBody == null) {
                return null;
            }
        }
        switch (octet) {
            case 128:
                return new SendReq(this.mHeaders, this.mBody);
            case 129:
                return new SendConf(this.mHeaders);
            case 130:
                return new NotificationInd(this.mHeaders);
            case 131:
                return new NotifyRespInd(this.mHeaders);
            case 132:
                RetrieveConf retrieveConf = new RetrieveConf(this.mHeaders, this.mBody);
                byte[] contentType = retrieveConf.getContentType();
                if (contentType == null) {
                    return null;
                }
                String str = new String(contentType);
                if (str.equals(ContentType.MULTIPART_MIXED) || str.equals(ContentType.MULTIPART_RELATED)) {
                    return retrieveConf;
                }
                return null;
            case 133:
                return new AcknowledgeInd(this.mHeaders);
            case 134:
                return new DeliveryInd(this.mHeaders);
            case 135:
                return new ReadRecInd(this.mHeaders);
            case 136:
                return new ReadOrigInd(this.mHeaders);
            default:
                log("Parser doesn't support this message type in this version!");
                return null;
        }
    }

    /* access modifiers changed from: protected */
    public PduHeaders parseHeaders(ByteArrayInputStream byteArrayInputStream) {
        EncodedStringValue encodedStringValue;
        byte[] textString;
        if (byteArrayInputStream == null) {
            return null;
        }
        boolean z = true;
        PduHeaders pduHeaders = new PduHeaders();
        while (z && byteArrayInputStream.available() > 0) {
            int extractByteValue = extractByteValue(byteArrayInputStream);
            switch (extractByteValue) {
                case 129:
                case 130:
                case 151:
                    EncodedStringValue parseEncodedStringValue = parseEncodedStringValue(byteArrayInputStream);
                    if (parseEncodedStringValue == null) {
                        break;
                    } else {
                        byte[] textString2 = parseEncodedStringValue.getTextString();
                        if (textString2 != null) {
                            String str = new String(textString2);
                            int indexOf = str.indexOf("/");
                            try {
                                parseEncodedStringValue.setTextString((indexOf > 0 ? str.substring(0, indexOf) : str).getBytes());
                            } catch (NullPointerException e) {
                                log("null pointer error!");
                                return null;
                            }
                        }
                        try {
                            pduHeaders.appendEncodedStringValue(parseEncodedStringValue, extractByteValue);
                            break;
                        } catch (NullPointerException e2) {
                            log("null pointer error!");
                            break;
                        } catch (RuntimeException e3) {
                            log(extractByteValue + "is not Encoded-String-Value header field!");
                            return null;
                        }
                    }
                case 131:
                case 139:
                case 152:
                case PduHeaders.REPLY_CHARGING_ID:
                case PduHeaders.APPLIC_ID:
                case PduHeaders.REPLY_APPLIC_ID:
                case PduHeaders.AUX_APPLIC_ID:
                case PduHeaders.REPLACE_ID:
                case PduHeaders.CANCEL_ID:
                    byte[] parseWapString = parseWapString(byteArrayInputStream, 0);
                    if (parseWapString == null) {
                        break;
                    } else {
                        try {
                            pduHeaders.setTextString(parseWapString, extractByteValue);
                            break;
                        } catch (NullPointerException e4) {
                            log("null pointer error!");
                            break;
                        } catch (RuntimeException e5) {
                            log(extractByteValue + "is not Text-String header field!");
                            return null;
                        }
                    }
                case 132:
                    HashMap hashMap = new HashMap();
                    byte[] parseContentType = parseContentType(byteArrayInputStream, hashMap);
                    if (parseContentType != null) {
                        try {
                            pduHeaders.setTextString(parseContentType, 132);
                        } catch (NullPointerException e6) {
                            log("null pointer error!");
                        } catch (RuntimeException e7) {
                            log(extractByteValue + "is not Text-String header field!");
                            return null;
                        }
                    }
                    mStartParam = (byte[]) hashMap.get(153);
                    mTypeParam = (byte[]) hashMap.get(131);
                    z = false;
                    break;
                case 133:
                case 142:
                case PduHeaders.REPLY_CHARGING_SIZE:
                    try {
                        pduHeaders.setLongInteger(parseLongInteger(byteArrayInputStream), extractByteValue);
                        break;
                    } catch (RuntimeException e8) {
                        log(extractByteValue + "is not Long-Integer header field!");
                        return null;
                    }
                case 134:
                case 143:
                case 144:
                case 145:
                case 146:
                case 148:
                case 149:
                case 153:
                case 155:
                case 156:
                case PduHeaders.STORE:
                case PduHeaders.MM_STATE:
                case PduHeaders.STORE_STATUS:
                case PduHeaders.STORED:
                case PduHeaders.TOTALS:
                case PduHeaders.QUOTAS:
                case PduHeaders.DISTRIBUTION_INDICATOR:
                case PduHeaders.RECOMMENDED_RETRIEVAL_MODE:
                case PduHeaders.CONTENT_CLASS:
                case PduHeaders.DRM_CONTENT:
                case PduHeaders.ADAPTATION_ALLOWED:
                case PduHeaders.CANCEL_STATUS:
                    int extractByteValue2 = extractByteValue(byteArrayInputStream);
                    try {
                        pduHeaders.setOctet(extractByteValue2, extractByteValue);
                        break;
                    } catch (InvalidHeaderValueException e9) {
                        log("Set invalid Octet value: " + extractByteValue2 + " into the header filed: " + extractByteValue);
                        return null;
                    } catch (RuntimeException e10) {
                        log(extractByteValue + "is not Octet header field!");
                        return null;
                    }
                case 135:
                case 136:
                case 157:
                    parseValueLength(byteArrayInputStream);
                    int extractByteValue3 = extractByteValue(byteArrayInputStream);
                    try {
                        long parseLongInteger = parseLongInteger(byteArrayInputStream);
                        try {
                            pduHeaders.setLongInteger(129 == extractByteValue3 ? (System.currentTimeMillis() / 1000) + parseLongInteger : parseLongInteger, extractByteValue);
                            break;
                        } catch (RuntimeException e11) {
                            log(extractByteValue + "is not Long-Integer header field!");
                            return null;
                        }
                    } catch (RuntimeException e12) {
                        log(extractByteValue + "is not Long-Integer header field!");
                        return null;
                    }
                case 137:
                    parseValueLength(byteArrayInputStream);
                    if (128 == extractByteValue(byteArrayInputStream)) {
                        encodedStringValue = parseEncodedStringValue(byteArrayInputStream);
                        if (!(encodedStringValue == null || (textString = encodedStringValue.getTextString()) == null)) {
                            String str2 = new String(textString);
                            int indexOf2 = str2.indexOf("/");
                            try {
                                encodedStringValue.setTextString((indexOf2 > 0 ? str2.substring(0, indexOf2) : str2).getBytes());
                            } catch (NullPointerException e13) {
                                log("null pointer error!");
                                return null;
                            }
                        }
                    } else {
                        try {
                            encodedStringValue = new EncodedStringValue(PduHeaders.FROM_INSERT_ADDRESS_TOKEN_STR.getBytes());
                        } catch (NullPointerException e14) {
                            log(extractByteValue + "is not Encoded-String-Value header field!");
                            return null;
                        }
                    }
                    try {
                        pduHeaders.setEncodedStringValue(encodedStringValue, 137);
                        break;
                    } catch (NullPointerException e15) {
                        log("null pointer error!");
                        break;
                    } catch (RuntimeException e16) {
                        log(extractByteValue + "is not Encoded-String-Value header field!");
                        return null;
                    }
                case 138:
                    byteArrayInputStream.mark(1);
                    int extractByteValue4 = extractByteValue(byteArrayInputStream);
                    if (extractByteValue4 >= 128) {
                        if (128 != extractByteValue4) {
                            if (129 != extractByteValue4) {
                                if (130 != extractByteValue4) {
                                    if (131 != extractByteValue4) {
                                        break;
                                    } else {
                                        pduHeaders.setTextString(PduHeaders.MESSAGE_CLASS_AUTO_STR.getBytes(), 138);
                                        break;
                                    }
                                } else {
                                    pduHeaders.setTextString(PduHeaders.MESSAGE_CLASS_INFORMATIONAL_STR.getBytes(), 138);
                                    break;
                                }
                            } else {
                                pduHeaders.setTextString(PduHeaders.MESSAGE_CLASS_ADVERTISEMENT_STR.getBytes(), 138);
                                break;
                            }
                        } else {
                            try {
                                pduHeaders.setTextString(PduHeaders.MESSAGE_CLASS_PERSONAL_STR.getBytes(), 138);
                                break;
                            } catch (NullPointerException e17) {
                                log("null pointer error!");
                                break;
                            } catch (RuntimeException e18) {
                                log(extractByteValue + "is not Text-String header field!");
                                return null;
                            }
                        }
                    } else {
                        byteArrayInputStream.reset();
                        byte[] parseWapString2 = parseWapString(byteArrayInputStream, 0);
                        if (parseWapString2 == null) {
                            break;
                        } else {
                            try {
                                pduHeaders.setTextString(parseWapString2, 138);
                                break;
                            } catch (NullPointerException e19) {
                                log("null pointer error!");
                                break;
                            } catch (RuntimeException e20) {
                                log(extractByteValue + "is not Text-String header field!");
                                return null;
                            }
                        }
                    }
                case 140:
                    int extractByteValue5 = extractByteValue(byteArrayInputStream);
                    switch (extractByteValue5) {
                        case 137:
                        case 138:
                        case 139:
                        case 140:
                        case 141:
                        case 142:
                        case 143:
                        case 144:
                        case 145:
                        case 146:
                        case 147:
                        case 148:
                        case 149:
                        case 150:
                        case 151:
                            return null;
                        default:
                            try {
                                pduHeaders.setOctet(extractByteValue5, extractByteValue);
                                continue;
                            } catch (InvalidHeaderValueException e21) {
                                log("Set invalid Octet value: " + extractByteValue5 + " into the header filed: " + extractByteValue);
                                return null;
                            } catch (RuntimeException e22) {
                                log(extractByteValue + "is not Octet header field!");
                                return null;
                            }
                    }
                case 141:
                    int parseShortInteger = parseShortInteger(byteArrayInputStream);
                    try {
                        pduHeaders.setOctet(parseShortInteger, 141);
                        break;
                    } catch (InvalidHeaderValueException e23) {
                        log("Set invalid Octet value: " + parseShortInteger + " into the header filed: " + extractByteValue);
                        return null;
                    } catch (RuntimeException e24) {
                        log(extractByteValue + "is not Octet header field!");
                        return null;
                    }
                case 147:
                case 150:
                case 154:
                case PduHeaders.STORE_STATUS_TEXT:
                case PduHeaders.RECOMMENDED_RETRIEVAL_MODE_TEXT:
                case PduHeaders.STATUS_TEXT:
                    EncodedStringValue parseEncodedStringValue2 = parseEncodedStringValue(byteArrayInputStream);
                    if (parseEncodedStringValue2 == null) {
                        break;
                    } else {
                        try {
                            pduHeaders.setEncodedStringValue(parseEncodedStringValue2, extractByteValue);
                            break;
                        } catch (NullPointerException e25) {
                            log("null pointer error!");
                            break;
                        } catch (RuntimeException e26) {
                            log(extractByteValue + "is not Encoded-String-Value header field!");
                            return null;
                        }
                    }
                case PduHeaders.PREVIOUSLY_SENT_BY:
                    parseValueLength(byteArrayInputStream);
                    try {
                        parseIntegerValue(byteArrayInputStream);
                        EncodedStringValue parseEncodedStringValue3 = parseEncodedStringValue(byteArrayInputStream);
                        if (parseEncodedStringValue3 == null) {
                            break;
                        } else {
                            try {
                                pduHeaders.setEncodedStringValue(parseEncodedStringValue3, PduHeaders.PREVIOUSLY_SENT_BY);
                                break;
                            } catch (NullPointerException e27) {
                                log("null pointer error!");
                                break;
                            } catch (RuntimeException e28) {
                                log(extractByteValue + "is not Encoded-String-Value header field!");
                                return null;
                            }
                        }
                    } catch (RuntimeException e29) {
                        log(extractByteValue + " is not Integer-Value");
                        return null;
                    }
                case PduHeaders.PREVIOUSLY_SENT_DATE:
                    parseValueLength(byteArrayInputStream);
                    try {
                        parseIntegerValue(byteArrayInputStream);
                        try {
                            pduHeaders.setLongInteger(parseLongInteger(byteArrayInputStream), PduHeaders.PREVIOUSLY_SENT_DATE);
                            break;
                        } catch (RuntimeException e30) {
                            log(extractByteValue + "is not Long-Integer header field!");
                            return null;
                        }
                    } catch (RuntimeException e31) {
                        log(extractByteValue + " is not Integer-Value");
                        return null;
                    }
                case PduHeaders.MM_FLAGS:
                    parseValueLength(byteArrayInputStream);
                    extractByteValue(byteArrayInputStream);
                    parseEncodedStringValue(byteArrayInputStream);
                    break;
                case PduHeaders.ATTRIBUTES:
                case 174:
                case PduHeaders.ADDITIONAL_HEADERS:
                default:
                    log("Unknown header");
                    break;
                case PduHeaders.MBOX_TOTALS:
                case PduHeaders.MBOX_QUOTAS:
                    parseValueLength(byteArrayInputStream);
                    extractByteValue(byteArrayInputStream);
                    try {
                        parseIntegerValue(byteArrayInputStream);
                        break;
                    } catch (RuntimeException e32) {
                        log(extractByteValue + " is not Integer-Value");
                        return null;
                    }
                case PduHeaders.MESSAGE_COUNT:
                case PduHeaders.START:
                case PduHeaders.LIMIT:
                    try {
                        pduHeaders.setLongInteger(parseIntegerValue(byteArrayInputStream), extractByteValue);
                        break;
                    } catch (RuntimeException e33) {
                        log(extractByteValue + "is not Long-Integer header field!");
                        return null;
                    }
                case PduHeaders.ELEMENT_DESCRIPTOR:
                    parseContentType(byteArrayInputStream, null);
                    break;
            }
        }
        return pduHeaders;
    }
}
