package com.google.android.mms.util;

import android.content.ContentUris;
import android.content.UriMatcher;
import android.net.Uri;
import com.android.internal.telephony.Phone;
import com.android.provider.Telephony;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

public final class PduCache extends AbstractCache<Uri, PduCacheEntry> {
    private static final boolean DEBUG = false;
    private static final boolean LOCAL_LOGV = false;
    private static final HashMap<Integer, Integer> MATCH_TO_MSGBOX_ID_MAP = new HashMap<>();
    private static final int MMS_ALL = 0;
    private static final int MMS_ALL_ID = 1;
    private static final int MMS_CONVERSATION = 10;
    private static final int MMS_CONVERSATION_ID = 11;
    private static final int MMS_DRAFTS = 6;
    private static final int MMS_DRAFTS_ID = 7;
    private static final int MMS_INBOX = 2;
    private static final int MMS_INBOX_ID = 3;
    private static final int MMS_OUTBOX = 8;
    private static final int MMS_OUTBOX_ID = 9;
    private static final int MMS_SENT = 4;
    private static final int MMS_SENT_ID = 5;
    private static final String TAG = "PduCache";
    private static final UriMatcher URI_MATCHER = new UriMatcher(-1);
    private static PduCache sInstance;
    private final HashMap<Integer, HashSet<Uri>> mMessageBoxes = new HashMap<>();
    private final HashMap<Long, HashSet<Uri>> mThreads = new HashMap<>();

    static {
        URI_MATCHER.addURI(Phone.APN_TYPE_MMS, null, 0);
        URI_MATCHER.addURI(Phone.APN_TYPE_MMS, "#", 1);
        URI_MATCHER.addURI(Phone.APN_TYPE_MMS, "inbox", 2);
        URI_MATCHER.addURI(Phone.APN_TYPE_MMS, "inbox/#", 3);
        URI_MATCHER.addURI(Phone.APN_TYPE_MMS, "sent", 4);
        URI_MATCHER.addURI(Phone.APN_TYPE_MMS, "sent/#", 5);
        URI_MATCHER.addURI(Phone.APN_TYPE_MMS, "drafts", 6);
        URI_MATCHER.addURI(Phone.APN_TYPE_MMS, "drafts/#", 7);
        URI_MATCHER.addURI(Phone.APN_TYPE_MMS, "outbox", 8);
        URI_MATCHER.addURI(Phone.APN_TYPE_MMS, "outbox/#", 9);
        URI_MATCHER.addURI("mms-sms", "conversations", 10);
        URI_MATCHER.addURI("mms-sms", "conversations/#", 11);
        MATCH_TO_MSGBOX_ID_MAP.put(2, 1);
        MATCH_TO_MSGBOX_ID_MAP.put(4, 2);
        MATCH_TO_MSGBOX_ID_MAP.put(6, 3);
        MATCH_TO_MSGBOX_ID_MAP.put(8, 4);
    }

    private PduCache() {
    }

    public static final synchronized PduCache getInstance() {
        PduCache pduCache;
        synchronized (PduCache.class) {
            if (sInstance == null) {
                sInstance = new PduCache();
            }
            pduCache = sInstance;
        }
        return pduCache;
    }

    private Uri normalizeKey(Uri uri) {
        switch (URI_MATCHER.match(uri)) {
            case 1:
                return uri;
            case 2:
            case 4:
            case 6:
            case 8:
            default:
                return null;
            case 3:
            case 5:
            case 7:
            case 9:
                return Uri.withAppendedPath(Telephony.Mms.CONTENT_URI, uri.getLastPathSegment());
        }
    }

    private void purgeByMessageBox(Integer num) {
        HashSet remove;
        if (num != null && (remove = this.mMessageBoxes.remove(num)) != null) {
            Iterator it = remove.iterator();
            while (it.hasNext()) {
                Uri uri = (Uri) it.next();
                PduCacheEntry pduCacheEntry = (PduCacheEntry) super.purge((Object) uri);
                if (pduCacheEntry != null) {
                    removeFromThreads(uri, pduCacheEntry);
                }
            }
        }
    }

    private void purgeByThreadId(long j) {
        HashSet remove = this.mThreads.remove(Long.valueOf(j));
        if (remove != null) {
            Iterator it = remove.iterator();
            while (it.hasNext()) {
                Uri uri = (Uri) it.next();
                PduCacheEntry pduCacheEntry = (PduCacheEntry) super.purge((Object) uri);
                if (pduCacheEntry != null) {
                    removeFromMessageBoxes(uri, pduCacheEntry);
                }
            }
        }
    }

    private PduCacheEntry purgeSingleEntry(Uri uri) {
        PduCacheEntry pduCacheEntry = (PduCacheEntry) super.purge((Object) uri);
        if (pduCacheEntry == null) {
            return null;
        }
        removeFromThreads(uri, pduCacheEntry);
        removeFromMessageBoxes(uri, pduCacheEntry);
        return pduCacheEntry;
    }

    private void removeFromMessageBoxes(Uri uri, PduCacheEntry pduCacheEntry) {
        HashSet hashSet = this.mThreads.get(Integer.valueOf(pduCacheEntry.getMessageBox()));
        if (hashSet != null) {
            hashSet.remove(uri);
        }
    }

    private void removeFromThreads(Uri uri, PduCacheEntry pduCacheEntry) {
        HashSet hashSet = this.mThreads.get(Long.valueOf(pduCacheEntry.getThreadId()));
        if (hashSet != null) {
            hashSet.remove(uri);
        }
    }

    public synchronized PduCacheEntry purge(Uri uri) {
        PduCacheEntry pduCacheEntry;
        int match = URI_MATCHER.match(uri);
        switch (match) {
            case 0:
            case 10:
                purgeAll();
                pduCacheEntry = null;
                break;
            case 1:
                pduCacheEntry = purgeSingleEntry(uri);
                break;
            case 2:
            case 4:
            case 6:
            case 8:
                purgeByMessageBox(MATCH_TO_MSGBOX_ID_MAP.get(Integer.valueOf(match)));
                pduCacheEntry = null;
                break;
            case 3:
            case 5:
            case 7:
            case 9:
                pduCacheEntry = purgeSingleEntry(Uri.withAppendedPath(Telephony.Mms.CONTENT_URI, uri.getLastPathSegment()));
                break;
            case 11:
                purgeByThreadId(ContentUris.parseId(uri));
                pduCacheEntry = null;
                break;
            default:
                pduCacheEntry = null;
                break;
        }
        return pduCacheEntry;
    }

    public synchronized void purgeAll() {
        super.purgeAll();
        this.mMessageBoxes.clear();
        this.mThreads.clear();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.mms.util.AbstractCache.put(java.lang.Object, java.lang.Object):boolean
     arg types: [android.net.Uri, com.google.android.mms.util.PduCacheEntry]
     candidates:
      com.google.android.mms.util.PduCache.put(android.net.Uri, com.google.android.mms.util.PduCacheEntry):boolean
      com.google.android.mms.util.AbstractCache.put(java.lang.Object, java.lang.Object):boolean */
    public synchronized boolean put(Uri uri, PduCacheEntry pduCacheEntry) {
        HashSet hashSet;
        boolean put;
        int messageBox = pduCacheEntry.getMessageBox();
        HashSet hashSet2 = this.mMessageBoxes.get(Integer.valueOf(messageBox));
        if (hashSet2 == null) {
            HashSet hashSet3 = new HashSet();
            this.mMessageBoxes.put(Integer.valueOf(messageBox), hashSet3);
            hashSet = hashSet3;
        } else {
            hashSet = hashSet2;
        }
        long threadId = pduCacheEntry.getThreadId();
        HashSet hashSet4 = this.mThreads.get(Long.valueOf(threadId));
        if (hashSet4 == null) {
            hashSet4 = new HashSet();
            this.mThreads.put(Long.valueOf(threadId), hashSet4);
        }
        Uri normalizeKey = normalizeKey(uri);
        put = super.put((Object) normalizeKey, (Object) pduCacheEntry);
        if (put) {
            hashSet.add(normalizeKey);
            hashSet4.add(normalizeKey);
        }
        return put;
    }
}
