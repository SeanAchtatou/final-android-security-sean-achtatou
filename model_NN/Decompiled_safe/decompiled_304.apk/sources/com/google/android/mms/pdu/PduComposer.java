package com.google.android.mms.pdu;

import android.content.ContentResolver;
import android.content.Context;
import android.text.TextUtils;
import com.flurry.android.Constants;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;

public class PduComposer {
    static final /* synthetic */ boolean $assertionsDisabled = (!PduComposer.class.desiredAssertionStatus());
    private static final int END_STRING_FLAG = 0;
    private static final int LENGTH_QUOTE = 31;
    private static final int LONG_INTEGER_LENGTH_MAX = 8;
    private static final int PDU_COMPOSER_BLOCK_SIZE = 1024;
    private static final int PDU_COMPOSE_CONTENT_ERROR = 1;
    private static final int PDU_COMPOSE_FIELD_NOT_SET = 2;
    private static final int PDU_COMPOSE_FIELD_NOT_SUPPORTED = 3;
    private static final int PDU_COMPOSE_SUCCESS = 0;
    private static final int PDU_EMAIL_ADDRESS_TYPE = 2;
    private static final int PDU_IPV4_ADDRESS_TYPE = 3;
    private static final int PDU_IPV6_ADDRESS_TYPE = 4;
    private static final int PDU_PHONE_NUMBER_ADDRESS_TYPE = 1;
    private static final int PDU_UNKNOWN_ADDRESS_TYPE = 5;
    private static final int QUOTED_STRING_FLAG = 34;
    static final String REGEXP_EMAIL_ADDRESS_TYPE = "[a-zA-Z| ]*\\<{0,1}[a-zA-Z| ]+@{1}[a-zA-Z| ]+\\.{1}[a-zA-Z| ]+\\>{0,1}";
    static final String REGEXP_IPV4_ADDRESS_TYPE = "[0-9]{1,3}\\.{1}[0-9]{1,3}\\.{1}[0-9]{1,3}\\.{1}[0-9]{1,3}";
    static final String REGEXP_IPV6_ADDRESS_TYPE = "[a-fA-F]{4}\\:{1}[a-fA-F0-9]{4}\\:{1}[a-fA-F0-9]{4}\\:{1}[a-fA-F0-9]{4}\\:{1}[a-fA-F0-9]{4}\\:{1}[a-fA-F0-9]{4}\\:{1}[a-fA-F0-9]{4}\\:{1}[a-fA-F0-9]{4}";
    static final String REGEXP_PHONE_NUMBER_ADDRESS_TYPE = "\\+?[0-9|\\.|\\-]+";
    private static final int SHORT_INTEGER_MAX = 127;
    static final String STRING_IPV4_ADDRESS_TYPE = "/TYPE=IPV4";
    static final String STRING_IPV6_ADDRESS_TYPE = "/TYPE=IPV6";
    static final String STRING_PHONE_NUMBER_ADDRESS_TYPE = "/TYPE=PLMN";
    private static final int TEXT_MAX = 127;
    private static HashMap<String, Integer> mContentTypeMap;
    protected ByteArrayOutputStream mMessage = null;
    private GenericPdu mPdu = null;
    private PduHeaders mPduHeader = null;
    protected int mPosition = 0;
    private final ContentResolver mResolver;
    /* access modifiers changed from: private */
    public BufferStack mStack = null;

    private class BufferStack {
        private LengthRecordNode stack;
        int stackSize;
        private LengthRecordNode toCopy;

        private BufferStack() {
            this.stack = null;
            this.toCopy = null;
            this.stackSize = 0;
        }

        /* access modifiers changed from: package-private */
        public void copy() {
            PduComposer.this.arraycopy(this.toCopy.currentMessage.toByteArray(), 0, this.toCopy.currentPosition);
            this.toCopy = null;
        }

        /* access modifiers changed from: package-private */
        public PositionMarker mark() {
            PositionMarker positionMarker = new PositionMarker();
            int unused = positionMarker.c_pos = PduComposer.this.mPosition;
            int unused2 = positionMarker.currentStackSize = this.stackSize;
            return positionMarker;
        }

        /* access modifiers changed from: package-private */
        public void newbuf() {
            if (this.toCopy != null) {
                throw new RuntimeException("BUG: Invalid newbuf() before copy()");
            }
            LengthRecordNode lengthRecordNode = new LengthRecordNode();
            lengthRecordNode.currentMessage = PduComposer.this.mMessage;
            lengthRecordNode.currentPosition = PduComposer.this.mPosition;
            lengthRecordNode.next = this.stack;
            this.stack = lengthRecordNode;
            this.stackSize++;
            PduComposer.this.mMessage = new ByteArrayOutputStream();
            PduComposer.this.mPosition = 0;
        }

        /* access modifiers changed from: package-private */
        public void pop() {
            ByteArrayOutputStream byteArrayOutputStream = PduComposer.this.mMessage;
            int i = PduComposer.this.mPosition;
            PduComposer.this.mMessage = this.stack.currentMessage;
            PduComposer.this.mPosition = this.stack.currentPosition;
            this.toCopy = this.stack;
            this.stack = this.stack.next;
            this.stackSize--;
            this.toCopy.currentMessage = byteArrayOutputStream;
            this.toCopy.currentPosition = i;
        }
    }

    private static class LengthRecordNode {
        ByteArrayOutputStream currentMessage;
        public int currentPosition;
        public LengthRecordNode next;

        private LengthRecordNode() {
            this.currentMessage = null;
            this.currentPosition = 0;
            this.next = null;
        }
    }

    private class PositionMarker {
        /* access modifiers changed from: private */
        public int c_pos;
        /* access modifiers changed from: private */
        public int currentStackSize;

        private PositionMarker() {
        }

        /* access modifiers changed from: package-private */
        public int getLength() {
            if (this.currentStackSize == PduComposer.this.mStack.stackSize) {
                return PduComposer.this.mPosition - this.c_pos;
            }
            throw new RuntimeException("BUG: Invalid call to getLength()");
        }
    }

    static {
        mContentTypeMap = null;
        mContentTypeMap = new HashMap<>();
        for (int i = 0; i < PduContentTypes.contentTypes.length; i++) {
            mContentTypeMap.put(PduContentTypes.contentTypes[i], Integer.valueOf(i));
        }
    }

    public PduComposer(Context context, GenericPdu genericPdu) {
        this.mPdu = genericPdu;
        this.mResolver = context.getContentResolver();
        this.mPduHeader = genericPdu.getPduHeaders();
        this.mStack = new BufferStack();
        this.mMessage = new ByteArrayOutputStream();
        this.mPosition = 0;
    }

    private EncodedStringValue appendAddressType(EncodedStringValue encodedStringValue) {
        try {
            int checkAddressType = checkAddressType(encodedStringValue.getString());
            EncodedStringValue copy = EncodedStringValue.copy(encodedStringValue);
            if (1 == checkAddressType) {
                copy.appendTextString(STRING_PHONE_NUMBER_ADDRESS_TYPE.getBytes());
            } else if (3 == checkAddressType) {
                copy.appendTextString(STRING_IPV4_ADDRESS_TYPE.getBytes());
            } else if (4 == checkAddressType) {
                copy.appendTextString(STRING_IPV6_ADDRESS_TYPE.getBytes());
            }
            return copy;
        } catch (NullPointerException e) {
            return null;
        }
    }

    private int appendHeader(int i) {
        switch (i) {
            case 129:
            case 130:
            case 151:
                EncodedStringValue[] encodedStringValues = this.mPduHeader.getEncodedStringValues(i);
                if (encodedStringValues != null) {
                    for (EncodedStringValue appendAddressType : encodedStringValues) {
                        EncodedStringValue appendAddressType2 = appendAddressType(appendAddressType);
                        if (appendAddressType2 == null) {
                            return 1;
                        }
                        appendOctet(i);
                        appendEncodedString(appendAddressType2);
                    }
                    break;
                } else {
                    return 2;
                }
            case 131:
            case 132:
            case 135:
            case 140:
            case 142:
            case 146:
            case 147:
            case 148:
            case 153:
            case 154:
            default:
                return 3;
            case 133:
                long longInteger = this.mPduHeader.getLongInteger(i);
                if (-1 != longInteger) {
                    appendOctet(i);
                    appendDateValue(longInteger);
                    break;
                } else {
                    return 2;
                }
            case 134:
            case 143:
            case 144:
            case 145:
            case 149:
            case 155:
                int octet = this.mPduHeader.getOctet(i);
                if (octet != 0) {
                    appendOctet(i);
                    appendOctet(octet);
                    break;
                } else {
                    return 2;
                }
            case 136:
                long longInteger2 = this.mPduHeader.getLongInteger(i);
                if (-1 != longInteger2) {
                    appendOctet(i);
                    this.mStack.newbuf();
                    PositionMarker mark = this.mStack.mark();
                    append(129);
                    appendLongInteger(longInteger2);
                    int length = mark.getLength();
                    this.mStack.pop();
                    appendValueLength((long) length);
                    this.mStack.copy();
                    break;
                } else {
                    return 2;
                }
            case 137:
                appendOctet(i);
                EncodedStringValue encodedStringValue = this.mPduHeader.getEncodedStringValue(i);
                if (encodedStringValue != null && !TextUtils.isEmpty(encodedStringValue.getString()) && !new String(encodedStringValue.getTextString()).equals(PduHeaders.FROM_INSERT_ADDRESS_TOKEN_STR)) {
                    this.mStack.newbuf();
                    PositionMarker mark2 = this.mStack.mark();
                    append(128);
                    EncodedStringValue appendAddressType3 = appendAddressType(encodedStringValue);
                    if (appendAddressType3 != null) {
                        appendEncodedString(appendAddressType3);
                        int length2 = mark2.getLength();
                        this.mStack.pop();
                        appendValueLength((long) length2);
                        this.mStack.copy();
                        break;
                    } else {
                        return 1;
                    }
                } else {
                    append(1);
                    append(129);
                    break;
                }
            case 138:
                byte[] textString = this.mPduHeader.getTextString(i);
                if (textString != null) {
                    appendOctet(i);
                    if (!Arrays.equals(textString, PduHeaders.MESSAGE_CLASS_ADVERTISEMENT_STR.getBytes())) {
                        if (!Arrays.equals(textString, PduHeaders.MESSAGE_CLASS_AUTO_STR.getBytes())) {
                            if (!Arrays.equals(textString, PduHeaders.MESSAGE_CLASS_PERSONAL_STR.getBytes())) {
                                if (!Arrays.equals(textString, PduHeaders.MESSAGE_CLASS_INFORMATIONAL_STR.getBytes())) {
                                    appendTextString(textString);
                                    break;
                                } else {
                                    appendOctet(130);
                                    break;
                                }
                            } else {
                                appendOctet(128);
                                break;
                            }
                        } else {
                            appendOctet(131);
                            break;
                        }
                    } else {
                        appendOctet(129);
                        break;
                    }
                } else {
                    return 2;
                }
            case 139:
            case 152:
                byte[] textString2 = this.mPduHeader.getTextString(i);
                if (textString2 != null) {
                    appendOctet(i);
                    appendTextString(textString2);
                    break;
                } else {
                    return 2;
                }
            case 141:
                appendOctet(i);
                int octet2 = this.mPduHeader.getOctet(i);
                if (octet2 != 0) {
                    appendShortInteger(octet2);
                    break;
                } else {
                    appendShortInteger(18);
                    break;
                }
            case 150:
                EncodedStringValue encodedStringValue2 = this.mPduHeader.getEncodedStringValue(i);
                if (encodedStringValue2 != null) {
                    appendOctet(i);
                    appendEncodedString(encodedStringValue2);
                    break;
                } else {
                    return 2;
                }
        }
        return 0;
    }

    protected static int checkAddressType(String str) {
        if (str == null) {
            return 5;
        }
        if (str.matches(REGEXP_IPV4_ADDRESS_TYPE)) {
            return 3;
        }
        if (str.matches(REGEXP_PHONE_NUMBER_ADDRESS_TYPE)) {
            return 1;
        }
        if (str.matches(REGEXP_EMAIL_ADDRESS_TYPE)) {
            return 2;
        }
        return str.matches(REGEXP_IPV6_ADDRESS_TYPE) ? 4 : 5;
    }

    private int makeAckInd() {
        if (this.mMessage == null) {
            this.mMessage = new ByteArrayOutputStream();
            this.mPosition = 0;
        }
        appendOctet(140);
        appendOctet(133);
        if (appendHeader(152) != 0) {
            return 1;
        }
        if (appendHeader(141) != 0) {
            return 1;
        }
        appendHeader(145);
        return 0;
    }

    private int makeMessageBody() {
        int i;
        this.mStack.newbuf();
        PositionMarker mark = this.mStack.mark();
        Integer num = mContentTypeMap.get(new String(this.mPduHeader.getTextString(132)));
        if (num == null) {
            return 1;
        }
        appendShortInteger(num.intValue());
        PduBody body = ((SendReq) this.mPdu).getBody();
        if (body == null || body.getPartsNum() == 0) {
            appendUintvarInteger(0);
            this.mStack.pop();
            this.mStack.copy();
            return 0;
        }
        try {
            PduPart part = body.getPart(0);
            byte[] contentId = part.getContentId();
            if (contentId != null) {
                appendOctet(138);
                if (60 == contentId[0] && 62 == contentId[contentId.length - 1]) {
                    appendTextString(contentId);
                } else {
                    appendTextString("<" + new String(contentId) + ">");
                }
            }
            appendOctet(137);
            appendTextString(part.getContentType());
        } catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
        }
        int length = mark.getLength();
        this.mStack.pop();
        appendValueLength((long) length);
        this.mStack.copy();
        int partsNum = body.getPartsNum();
        appendUintvarInteger((long) partsNum);
        for (int i2 = 0; i2 < partsNum; i2++) {
            PduPart part2 = body.getPart(i2);
            this.mStack.newbuf();
            PositionMarker mark2 = this.mStack.mark();
            this.mStack.newbuf();
            PositionMarker mark3 = this.mStack.mark();
            byte[] contentType = part2.getContentType();
            if (contentType == null) {
                return 1;
            }
            Integer num2 = mContentTypeMap.get(new String(contentType));
            if (num2 == null) {
                appendTextString(contentType);
            } else {
                appendShortInteger(num2.intValue());
            }
            byte[] name = part2.getName();
            if (name == null && (name = part2.getFilename()) == null && (name = part2.getContentLocation()) == null) {
                return 1;
            }
            appendOctet(133);
            appendTextString(name);
            int charset = part2.getCharset();
            if (charset != 0) {
                appendOctet(129);
                appendShortInteger(charset);
            }
            int length2 = mark3.getLength();
            this.mStack.pop();
            appendValueLength((long) length2);
            this.mStack.copy();
            byte[] contentId2 = part2.getContentId();
            if (contentId2 != null) {
                appendOctet(192);
                if (60 == contentId2[0] && 62 == contentId2[contentId2.length - 1]) {
                    appendQuotedString(contentId2);
                } else {
                    appendQuotedString("<" + new String(contentId2) + ">");
                }
            }
            byte[] contentLocation = part2.getContentLocation();
            if (contentLocation != null) {
                appendOctet(142);
                appendTextString(contentLocation);
            }
            int length3 = mark2.getLength();
            int i3 = 0;
            byte[] data = part2.getData();
            if (data != null) {
                arraycopy(data, 0, data.length);
                i = data.length;
            } else {
                try {
                    byte[] bArr = new byte[1024];
                    InputStream openInputStream = this.mResolver.openInputStream(part2.getDataUri());
                    while (true) {
                        int read = openInputStream.read(bArr);
                        if (read == -1) {
                            break;
                        }
                        this.mMessage.write(bArr, 0, read);
                        this.mPosition += read;
                        i3 += read;
                    }
                    i = i3;
                } catch (FileNotFoundException e2) {
                    return 1;
                } catch (IOException e3) {
                    return 1;
                } catch (RuntimeException e4) {
                    return 1;
                }
            }
            if (i != mark2.getLength() - length3) {
                throw new RuntimeException("BUG: Length sanity check failed");
            }
            this.mStack.pop();
            appendUintvarInteger((long) length3);
            appendUintvarInteger((long) i);
            this.mStack.copy();
        }
        return 0;
    }

    private int makeNotifyResp() {
        if (this.mMessage == null) {
            this.mMessage = new ByteArrayOutputStream();
            this.mPosition = 0;
        }
        appendOctet(140);
        appendOctet(131);
        if (appendHeader(152) != 0) {
            return 1;
        }
        if (appendHeader(141) != 0) {
            return 1;
        }
        return appendHeader(149) != 0 ? 1 : 0;
    }

    private int makeReadRecInd() {
        if (this.mMessage == null) {
            this.mMessage = new ByteArrayOutputStream();
            this.mPosition = 0;
        }
        appendOctet(140);
        appendOctet(135);
        if (appendHeader(141) != 0) {
            return 1;
        }
        if (appendHeader(139) != 0) {
            return 1;
        }
        if (appendHeader(151) != 0) {
            return 1;
        }
        if (appendHeader(137) != 0) {
            return 1;
        }
        appendHeader(133);
        return appendHeader(155) != 0 ? 1 : 0;
    }

    private int makeSendReqPdu() {
        if (this.mMessage == null) {
            this.mMessage = new ByteArrayOutputStream();
            this.mPosition = 0;
        }
        appendOctet(140);
        appendOctet(128);
        appendOctet(152);
        byte[] textString = this.mPduHeader.getTextString(152);
        if (textString == null) {
            throw new IllegalArgumentException("Transaction-ID is null.");
        }
        appendTextString(textString);
        if (appendHeader(141) != 0) {
            return 1;
        }
        appendHeader(133);
        if (appendHeader(137) != 0) {
            return 1;
        }
        boolean z = appendHeader(151) != 1;
        if (appendHeader(130) != 1) {
            z = true;
        }
        if (appendHeader(129) != 1) {
            z = true;
        }
        if (!z) {
            return 1;
        }
        appendHeader(150);
        appendHeader(138);
        appendHeader(136);
        appendHeader(143);
        appendHeader(134);
        appendHeader(144);
        appendOctet(132);
        makeMessageBody();
        return 0;
    }

    /* access modifiers changed from: protected */
    public void append(int i) {
        this.mMessage.write(i);
        this.mPosition++;
    }

    /* access modifiers changed from: protected */
    public void appendDateValue(long j) {
        appendLongInteger(j);
    }

    /* access modifiers changed from: protected */
    public void appendEncodedString(EncodedStringValue encodedStringValue) {
        if ($assertionsDisabled || encodedStringValue != null) {
            int characterSet = encodedStringValue.getCharacterSet();
            byte[] textString = encodedStringValue.getTextString();
            if (textString != null) {
                this.mStack.newbuf();
                PositionMarker mark = this.mStack.mark();
                appendShortInteger(characterSet);
                appendTextString(textString);
                int length = mark.getLength();
                this.mStack.pop();
                appendValueLength((long) length);
                this.mStack.copy();
                return;
            }
            return;
        }
        throw new AssertionError();
    }

    /* access modifiers changed from: protected */
    public void appendLongInteger(long j) {
        long j2 = j;
        int i = 0;
        while (j2 != 0 && i < 8) {
            j2 >>>= 8;
            i++;
        }
        appendShortLength(i);
        int i2 = (i - 1) * 8;
        for (int i3 = 0; i3 < i; i3++) {
            append((int) ((j >>> i2) & 255));
            i2 -= 8;
        }
    }

    /* access modifiers changed from: protected */
    public void appendOctet(int i) {
        append(i);
    }

    /* access modifiers changed from: protected */
    public void appendQuotedString(String str) {
        appendQuotedString(str.getBytes());
    }

    /* access modifiers changed from: protected */
    public void appendQuotedString(byte[] bArr) {
        append(QUOTED_STRING_FLAG);
        arraycopy(bArr, 0, bArr.length);
        append(0);
    }

    /* access modifiers changed from: protected */
    public void appendShortInteger(int i) {
        append((i | 128) & 255);
    }

    /* access modifiers changed from: protected */
    public void appendShortLength(int i) {
        append(i);
    }

    /* access modifiers changed from: protected */
    public void appendTextString(String str) {
        appendTextString(str.getBytes());
    }

    /* access modifiers changed from: protected */
    public void appendTextString(byte[] bArr) {
        if ((bArr[0] & Constants.UNKNOWN) > Byte.MAX_VALUE) {
            append(127);
        }
        arraycopy(bArr, 0, bArr.length);
        append(0);
    }

    /* access modifiers changed from: protected */
    public void appendUintvarInteger(long j) {
        int i;
        int i2 = 0;
        long j2 = 127;
        while (true) {
            if (i2 >= 5) {
                i = i2;
                break;
            } else if (j < j2) {
                i = i2;
                break;
            } else {
                j2 = (j2 << 7) | 127;
                i2++;
            }
        }
        while (i > 0) {
            append((int) ((((j >>> (i * 7)) & 127) | 128) & 255));
            i--;
        }
        append((int) (j & 127));
    }

    /* access modifiers changed from: protected */
    public void appendValueLength(long j) {
        if (j < 31) {
            appendShortLength((int) j);
            return;
        }
        append(31);
        appendUintvarInteger(j);
    }

    /* access modifiers changed from: protected */
    public void arraycopy(byte[] bArr, int i, int i2) {
        this.mMessage.write(bArr, i, i2);
        this.mPosition += i2;
    }

    public byte[] make() {
        switch (this.mPdu.getMessageType()) {
            case 128:
                if (makeSendReqPdu() != 0) {
                    return null;
                }
                break;
            case 129:
            case 130:
            case 132:
            case 134:
            default:
                return null;
            case 131:
                if (makeNotifyResp() != 0) {
                    return null;
                }
                break;
            case 133:
                if (makeAckInd() != 0) {
                    return null;
                }
                break;
            case 135:
                if (makeReadRecInd() != 0) {
                    return null;
                }
                break;
        }
        return this.mMessage.toByteArray();
    }
}
