package com.google.android.mms.util;

import java.util.HashMap;

public abstract class AbstractCache<K, V> {
    private static final boolean DEBUG = false;
    private static final boolean LOCAL_LOGV = false;
    private static final int MAX_CACHED_ITEMS = 500;
    private static final String TAG = "AbstractCache";
    private final HashMap<K, CacheEntry<V>> mCacheMap = new HashMap<>();

    private static class CacheEntry<V> {
        int hit;
        V value;

        private CacheEntry() {
        }
    }

    protected AbstractCache() {
    }

    public V get(K k) {
        CacheEntry cacheEntry;
        if (k == null || (cacheEntry = this.mCacheMap.get(k)) == null) {
            return null;
        }
        cacheEntry.hit++;
        return cacheEntry.value;
    }

    public V purge(Object obj) {
        CacheEntry remove = this.mCacheMap.remove(obj);
        if (remove != null) {
            return remove.value;
        }
        return null;
    }

    public void purgeAll() {
        this.mCacheMap.clear();
    }

    public boolean put(Object obj, Object obj2) {
        if (this.mCacheMap.size() >= MAX_CACHED_ITEMS) {
            return false;
        }
        if (obj == null) {
            return false;
        }
        CacheEntry cacheEntry = new CacheEntry();
        cacheEntry.value = obj2;
        this.mCacheMap.put(obj, cacheEntry);
        return true;
    }

    public int size() {
        return this.mCacheMap.size();
    }
}
