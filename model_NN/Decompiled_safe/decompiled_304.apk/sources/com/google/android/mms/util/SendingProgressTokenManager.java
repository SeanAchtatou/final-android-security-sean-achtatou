package com.google.android.mms.util;

import java.util.HashMap;

public class SendingProgressTokenManager {
    private static final boolean DEBUG = false;
    private static final boolean LOCAL_LOGV = false;
    public static final long NO_TOKEN = -1;
    private static final String TAG = "SendingProgressTokenManager";
    private static final HashMap<Object, Long> TOKEN_POOL = new HashMap<>();

    public static synchronized long get(Object obj) {
        long longValue;
        synchronized (SendingProgressTokenManager.class) {
            Long l = TOKEN_POOL.get(obj);
            longValue = l != null ? l.longValue() : -1;
        }
        return longValue;
    }

    public static synchronized void put(Object obj, long j) {
        synchronized (SendingProgressTokenManager.class) {
            TOKEN_POOL.put(obj, Long.valueOf(j));
        }
    }

    public static synchronized void remove(Object obj) {
        synchronized (SendingProgressTokenManager.class) {
            TOKEN_POOL.remove(obj);
        }
    }
}
