package net.weweweb.android.bridge;

import android.view.View;
import android.widget.AdapterView;

/* compiled from: ProGuard */
final class bm implements AdapterView.OnItemSelectedListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SoloGameSettingsActivity f67a;

    bm(SoloGameSettingsActivity soloGameSettingsActivity) {
        this.f67a = soloGameSettingsActivity;
    }

    public final void onItemSelected(AdapterView adapterView, View view, int i, long j) {
        BridgeApp.q = (i + 1) * 1000;
        this.f67a.f24a.a("endTurnDelay", BridgeApp.q);
    }

    public final void onNothingSelected(AdapterView adapterView) {
    }
}
