package net.weweweb.android.bridge;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.widget.Button;
import net.weweweb.android.free.bridge.R;

/* compiled from: ProGuard */
final class l extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ BridgeApp f88a;

    l(BridgeApp bridgeApp) {
        this.f88a = bridgeApp;
    }

    public final void handleMessage(Message message) {
        if (message.what == 0) {
            this.f88a.k.m.a((String) message.obj);
        } else if (message.what == 1) {
            this.f88a.k.c((ab) message.obj);
        } else if (message.what == 2) {
            this.f88a.k.d((ab) message.obj);
        } else if (message.what == 3) {
            this.f88a.k.b((ac) message.obj);
        } else if (message.what == 4) {
            this.f88a.k.e((ab) message.obj);
        } else if (message.what == 5) {
            this.f88a.k.a((ac) message.obj);
        } else if (message.what == 6 && this.f88a.i != null) {
            if (message.arg1 == 1) {
                this.f88a.i.j = message.arg2;
            }
            this.f88a.i.b();
        } else if (message.what == 7) {
            if (this.f88a.e != null) {
                this.f88a.e.b();
            }
            this.f88a.d((String) message.obj);
        } else if (message.what == 8) {
            if (this.f88a.e != null) {
                ((Button) this.f88a.e.findViewById(R.id.btnSelectChannelOut)).setText((String) message.obj);
            }
        } else if (message.what == 9) {
            if (message.obj != null) {
                ((Button) message.obj).setEnabled(message.arg1 == 1);
            }
        } else if (message.what == 10) {
            if (this.f88a.m == null) {
                this.f88a.m = new am(this.f88a);
                GameRoomActivity gameRoomActivity = this.f88a.c;
                Intent intent = new Intent();
                intent.setClass(gameRoomActivity, NetGameActivity.class);
                gameRoomActivity.startActivity(intent);
            } else if (this.f88a.m != null && (this.f88a.m instanceof am)) {
                ((am) this.f88a.m).e();
            }
        } else if (message.what == 11) {
            if (this.f88a.m != null && (this.f88a.m instanceof am)) {
                ((am) this.f88a.m).d();
            }
        } else if (message.what == 12) {
            if (this.f88a.m != null && (this.f88a.m instanceof am)) {
                am amVar = (am) this.f88a.m;
                if (amVar.f32a.f != null) {
                    amVar.f32a.f.c();
                }
                amVar.f32a.i.b();
                amVar.a();
            }
        } else if (message.what == 13) {
            if (this.f88a.m != null && (this.f88a.m instanceof am)) {
                ((am) this.f88a.m).c();
            }
        } else if (message.what == 14) {
            if (this.f88a.m != null && (this.f88a.m instanceof am)) {
                ((am) this.f88a.m).a((byte) message.arg1, (byte) message.arg2);
            }
        } else if (message.what == 15) {
            this.f88a.k.a(message.arg1, (byte) message.arg2);
        } else if (message.what == 16) {
            if (this.f88a.c != null) {
                this.f88a.c.a();
            }
        } else if (message.what == 17) {
            if (this.f88a.k != null) {
                this.f88a.k.a(message.arg1, message.arg2, ((String[]) message.obj)[0], ((String[]) message.obj)[1]);
            }
        } else if (message.what == 18) {
            this.f88a.d((String) message.obj);
        } else if (message.what == 19) {
            this.f88a.c((String) message.obj);
        }
        super.handleMessage(message);
    }
}
