package net.weweweb.android.bridge;

import a.g;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;
import net.weweweb.android.free.bridge.R;

/* compiled from: ProGuard */
final class bl implements AdapterView.OnItemSelectedListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SoloGameSettingsActivity f66a;

    bl(SoloGameSettingsActivity soloGameSettingsActivity) {
        this.f66a = soloGameSettingsActivity;
    }

    public final void onItemSelected(AdapterView adapterView, View view, int i, long j) {
        if (i == 0) {
            BridgeApp.o = 0;
            this.f66a.b.b(-999);
            this.f66a.b.b(-998);
            this.f66a.b.b(-997);
        } else if (i == 1) {
            BridgeApp.o = 1;
        } else if (i == 2) {
            BridgeApp.o = 2;
        } else if (i == 3) {
            if (BridgeApp.o == 3) {
                return;
            }
            if (this.f66a.b.c(-997) >= BridgeApp.u || this.f66a.b.c(-1000) >= BridgeApp.u) {
                if (this.f66a.b.c(-997) < BridgeApp.u) {
                    this.f66a.b.b(-997);
                    this.f66a.b.a(BridgeApp.u);
                }
                BridgeApp.o = 3;
            } else {
                Toast.makeText(this.f66a, g.a(this.f66a.getString(R.string.min_saved_game_msg), Integer.toString(BridgeApp.u)), 0).show();
                ((Spinner) this.f66a.findViewById(R.id.soloGameSettingsScoringMethodSpinner)).setSelection(BridgeApp.o);
                return;
            }
        }
        this.f66a.f24a.a("soloGameScoreMethod", BridgeApp.o);
    }

    public final void onNothingSelected(AdapterView adapterView) {
    }
}
