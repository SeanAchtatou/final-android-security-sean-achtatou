package net.weweweb.android.bridge;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import java.util.ArrayList;
import java.util.HashMap;
import net.weweweb.android.free.bridge.R;

/* compiled from: ProGuard */
public class ToolMenuActivity extends Activity {
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.mainmenu);
        ListView listView = (ListView) findViewById(R.id.mainMenu);
        ArrayList arrayList = new ArrayList();
        new HashMap();
        HashMap hashMap = new HashMap();
        hashMap.put("icon", Integer.valueOf((int) R.drawable.ic_menu_archive));
        hashMap.put("title", "Game Records");
        hashMap.put("desc", "View and replay previous saved games.");
        arrayList.add(hashMap);
        HashMap hashMap2 = new HashMap();
        hashMap2.put("icon", Integer.valueOf((int) R.drawable.ic_menu_calculator));
        hashMap2.put("title", "Score Calculator");
        hashMap2.put("desc", "Calculate the score for a contract.");
        arrayList.add(hashMap2);
        listView.setAdapter((ListAdapter) new SimpleAdapter(this, arrayList, R.layout.mainmenuitem, new String[]{"icon", "title", "desc"}, new int[]{R.id.mainMenuItemImage, R.id.mainMenuItemTitle, R.id.mainMenuItemDesc}));
        listView.setOnItemClickListener(new bw(this));
    }
}
