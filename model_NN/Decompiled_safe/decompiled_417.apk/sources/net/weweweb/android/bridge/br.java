package net.weweweb.android.bridge;

import android.os.Message;
import java.util.TimerTask;

/* compiled from: ProGuard */
final class br extends TimerTask {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SplashScreen f71a;

    br(SplashScreen splashScreen) {
        this.f71a = splashScreen;
    }

    public final void run() {
        if (System.currentTimeMillis() - this.f71a.b >= 10000 || !this.f71a.c) {
            if (!this.f71a.d) {
                Message message = new Message();
                message.what = 0;
                this.f71a.f.sendMessage(message);
            } else {
                Message message2 = new Message();
                message2.what = 1;
                this.f71a.f.sendMessage(message2);
            }
            this.f71a.f25a.cancel();
            cancel();
        }
    }
}
