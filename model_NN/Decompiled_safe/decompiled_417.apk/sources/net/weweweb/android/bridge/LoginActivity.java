package net.weweweb.android.bridge;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import net.weweweb.android.free.bridge.R;

/* compiled from: ProGuard */
public class LoginActivity extends Activity implements View.OnClickListener {
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.login);
        if (BridgeApp.D != null) {
            ((TextView) findViewById(R.id.txtUsername)).setText(BridgeApp.D);
        }
        if (BridgeApp.E && BridgeApp.F != null) {
            ((TextView) findViewById(R.id.txtPassword)).setText(BridgeApp.F);
        }
        findViewById(R.id.btnLogin).setOnClickListener(this);
        findViewById(R.id.btnLoginCancel).setOnClickListener(this);
        findViewById(R.id.registerWebLink).setOnClickListener(this);
    }

    public void onClick(View view) {
        if (view.equals(findViewById(R.id.btnLoginCancel))) {
            BridgeApp bridgeApp = (BridgeApp) getApplicationContext();
            if (bridgeApp.k.d != null) {
                bridgeApp.k.d.b();
                bridgeApp.k.d = null;
                bridgeApp.k.m = null;
            }
            finish();
        } else if (view.equals(findViewById(R.id.btnLogin))) {
            a(false);
            BridgeApp bridgeApp2 = (BridgeApp) getApplicationContext();
            String obj = ((EditText) findViewById(R.id.txtUsername)).getText().toString();
            String obj2 = ((EditText) findViewById(R.id.txtPassword)).getText().toString();
            if (obj == null || obj2 == null || obj.length() == 0 || obj2.length() == 0) {
                a("Invalid Username/Passowrd.");
                a(true);
                return;
            }
            if (obj != null && !obj.equals("")) {
                BridgeApp.D = obj;
                bridgeApp2.a("loginName", BridgeApp.D);
            }
            if (BridgeApp.E && obj2 != null) {
                BridgeApp.F = obj2;
                bridgeApp2.a("savePassword", BridgeApp.F);
            }
            bridgeApp2.k.a(this);
        } else if (view == findViewById(R.id.registerWebLink)) {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse((String) ((TextView) view).getText())));
        }
    }

    private void a(boolean z) {
        findViewById(R.id.txtUsername).setEnabled(z);
        findViewById(R.id.txtPassword).setEnabled(z);
        findViewById(R.id.btnLogin).setEnabled(z);
        findViewById(R.id.btnLoginCancel).setEnabled(z);
    }

    /* access modifiers changed from: package-private */
    public final void a(String str) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Error");
        builder.setMessage(str);
        builder.setCancelable(true);
        builder.setNegativeButton("OK", (DialogInterface.OnClickListener) null);
        builder.show();
        a(true);
    }
}
