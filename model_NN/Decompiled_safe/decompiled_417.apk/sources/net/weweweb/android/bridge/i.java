package net.weweweb.android.bridge;

import a.b;
import android.content.Context;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import java.util.LinkedList;

/* compiled from: ProGuard */
public final class i {

    /* renamed from: a  reason: collision with root package name */
    private Context f85a = null;
    private ad b = null;
    private b c = null;
    private TableRow d = null;
    /* access modifiers changed from: private */
    public ScrollView e = null;
    private TableLayout f = null;
    private LinkedList g = new LinkedList();
    private byte h = -1;
    private byte i = BridgeApp.A;

    i(Context context, TableRow tableRow, ScrollView scrollView) {
        this.f85a = context;
        this.d = tableRow;
        this.e = scrollView;
        this.f = new TableLayout(this.f85a);
        this.f.setLayoutParams(new TableLayout.LayoutParams((ViewGroup.MarginLayoutParams) new TableLayout.LayoutParams(-1, -2)));
        this.f.setBackgroundColor(-16777216);
        this.e.removeAllViews();
        this.e.addView(this.f);
        b(4);
        this.f.setStretchAllColumns(true);
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < this.g.size()) {
                a((TableRow) this.g.get(i3));
                i2 = i3 + 1;
            } else {
                this.h = -1;
                return;
            }
        }
    }

    private static void a(TableRow tableRow) {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < 4) {
                ((TextView) tableRow.getChildAt(i3)).setText("");
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    private TextView a(int i2) {
        if (i2 >= this.g.size() * 4) {
            return null;
        }
        return (TextView) ((TableRow) this.g.get(i2 / 4)).getChildAt(i2 % 4);
    }

    public final void b() {
        if (this.b instanceof bp) {
            this.i = this.b.e;
        }
        for (byte b2 = 0; b2 < 4; b2 = (byte) (b2 + 1)) {
            TextView textView = (TextView) this.d.getChildAt(b2);
            if (this.b == null) {
                textView.setText(b.g[(this.i + b2) % 4]);
            } else if (this.b instanceof bp) {
                textView.setText(b.g[(this.i + b2) % 4] + "\n" + ((this.i + b2) % 4 == this.b.e ? "[You]" : "Robot"));
            } else if (this.b instanceof am) {
                textView.setText(Html.fromHtml(b.g[(this.i + b2) % 4] + "<br/>" + (((am) this.b).b((byte) ((this.i + b2) % 4)) ? this.b.g : (this.i + b2) % 4 == this.b.e ? ((am) this.b).r ? "<font color=#00ff00>" + ((am) this.b).o.h[(this.i + b2) % 4].d + "</font>" : "<font color=#00ff00>[You]</font>" : ((am) this.b).o.h[(this.i + b2) % 4].d)));
            }
        }
        b(4);
        a();
        d();
    }

    private void b(int i2) {
        int childCount = this.f.getChildCount();
        if (childCount > i2) {
            for (int i3 = childCount - 1; i3 >= i2; i3--) {
                TableRow tableRow = (TableRow) this.g.get(i3);
                this.f.removeView(tableRow);
                a(tableRow);
            }
        } else if (childCount < i2) {
            for (int i4 = childCount; i4 < i2; i4++) {
                if (this.g.size() <= i4) {
                    TableRow tableRow2 = new TableRow(this.f85a);
                    TableRow.LayoutParams layoutParams = new TableRow.LayoutParams();
                    layoutParams.setMargins(0, 0, 0, 1);
                    for (int i5 = 0; i5 < 4; i5++) {
                        TextView textView = new TextView(this.f85a);
                        textView.setText("");
                        textView.setTextSize(15.0f);
                        textView.setPadding(0, 1, 0, 1);
                        textView.setBackgroundColor(-1);
                        textView.setTextColor(-16777216);
                        textView.setGravity(17);
                        tableRow2.addView(textView, layoutParams);
                    }
                    this.g.add(tableRow2);
                }
                this.f.addView((View) this.g.get(i4));
            }
        }
    }

    public final void a(b bVar) {
        this.c = bVar;
    }

    public final void a(ad adVar) {
        this.b = adVar;
        this.c = adVar.b;
    }

    public final void c() {
        this.e.post(new j(this));
    }

    /* access modifiers changed from: package-private */
    public final void d() {
        int i2 = this.h;
        while (true) {
            i2++;
            if (i2 > this.c.e()) {
                break;
            }
            byte b2 = (byte) i2;
            if (this.f.getChildCount() * 4 <= this.c.e() + 1 + (((this.c.h.a() - this.i) + 4) % 4)) {
                b(this.f.getChildCount() + 1);
            }
            int a2 = (((this.c.h.a() - this.i) + 4) % 4) + b2;
            byte M = this.c.M(b2);
            boolean z = this.c.k[b2];
            TextView a3 = a(a2);
            if (a3 != null) {
                if (M == -1) {
                    a3.setText("");
                } else {
                    SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
                    if (M == 99) {
                        spannableStringBuilder.append((CharSequence) "Pass");
                    } else if (M == 97) {
                        spannableStringBuilder.append((CharSequence) "X");
                    } else if (M == 98) {
                        spannableStringBuilder.append((CharSequence) "XX");
                    } else {
                        spannableStringBuilder.append((CharSequence) Byte.toString(b.c(M)));
                        spannableStringBuilder.append((CharSequence) b.e[b.e(M)]);
                        if (b.e(M) == 1 || b.e(M) == 2) {
                            spannableStringBuilder.setSpan(new ForegroundColorSpan(-65536), 1, 2, 33);
                        }
                    }
                    if (z) {
                        spannableStringBuilder.append((CharSequence) "!");
                        spannableStringBuilder.setSpan(new ForegroundColorSpan(-65536), spannableStringBuilder.length() - 1, spannableStringBuilder.length(), 33);
                    }
                    a3.setText(spannableStringBuilder);
                }
            }
        }
        if (!this.c.I()) {
            if (this.f.getChildCount() * 4 <= this.c.e() + 1 + (((this.c.h.a() - this.i) + 4) % 4)) {
                b(this.f.getChildCount() + 1);
            }
            a(this.c.e() + 1 + (((this.c.h.a() - this.i) + 4) % 4)).setText("?");
        }
    }
}
