package net.weweweb.android.bridge;

import a.c;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import net.weweweb.android.free.bridge.R;

/* compiled from: ProGuard */
public class RecordBrowserActivity extends Activity implements View.OnClickListener, AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    BridgeApp f20a;
    LinkedList b;
    ArrayList c;
    SimpleAdapter d;
    w e = null;
    String[] f;
    int g;
    String h = null;
    Dialog i = null;

    private void a() {
        HashMap hashMap;
        this.b = this.e.a(this.h);
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < this.b.size()) {
                c cVar = (c) this.b.get(i3);
                if (cVar != null) {
                    ArrayList arrayList = this.c;
                    if (cVar == null) {
                        hashMap = null;
                    } else {
                        HashMap hashMap2 = new HashMap();
                        if (cVar != null) {
                            hashMap2.put("deck_no", Integer.toString(cVar.c) + ".");
                            hashMap2.put("result", cVar.a(true));
                            hashMap2.put("play_datetime", DateFormat.format("MMM dd, kk:mm", new Timestamp(cVar.i)));
                        }
                        hashMap = hashMap2;
                    }
                    arrayList.add(hashMap);
                    if (this.d != null) {
                        this.d.notifyDataSetChanged();
                    }
                }
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f20a = (BridgeApp) getApplicationContext();
        this.f = new String[]{getString(R.string.cancel), getString(R.string.view), getString(R.string.retry), getString(R.string.delete)};
        setContentView((int) R.layout.recordbrowser);
        this.e = new w(this.f20a);
        this.e.c();
        ListView listView = (ListView) findViewById(R.id.gameRecordListView);
        this.c = new ArrayList();
        a();
        this.d = new aw(this, this, this.c, new String[]{"deck_no", "result", "play_datetime"}, new int[]{R.id.recordBrowserListItemDeckNo, R.id.recordBrowserListItemGameResult, R.id.recordBrowserListItemGamePlayTime});
        listView.setAdapter((ListAdapter) this.d);
        listView.setOnItemClickListener(this);
        findViewById(R.id.btnSearchGameRecord).setOnClickListener(this);
        findViewById(R.id.btnDeleteAllGameRecord).setOnClickListener(this);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.i != null) {
            this.i.dismiss();
            this.i = null;
        }
        if (this.b != null) {
            this.b.clear();
            this.b = null;
        }
        if (this.e != null) {
            this.e.a();
            this.e = null;
        }
        super.onDestroy();
    }

    public void onItemClick(AdapterView adapterView, View view, int i2, long j) {
        this.g = i2;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Record options");
        builder.setItems(this.f, new az(this));
        builder.create().show();
    }

    public void onClick(View view) {
        if (view == findViewById(R.id.btnDeleteAllGameRecord)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Are you sure you want to delete all these saved game records?").setTitle((int) R.string.confirm_required).setCancelable(false).setPositiveButton("Yes", new ay(this)).setNegativeButton("No", new ax(this));
            builder.create().show();
        } else if (view == findViewById(R.id.btnSearchGameRecord)) {
            if (this.i == null) {
                this.i = new Dialog(this);
                this.i.setContentView((int) R.layout.searchdialog);
                this.i.setTitle(this.f20a.getString(R.string.search_dialog_title));
                this.i.findViewById(R.id.searchButtonOk).setOnClickListener(this);
                this.i.findViewById(R.id.searchButtonCancel).setOnClickListener(this);
                this.i.findViewById(R.id.searchAllCheckbox).setOnClickListener(this);
                this.i.findViewById(R.id.searchNTCheckbox).setOnClickListener(this);
                this.i.findViewById(R.id.searchMajorCheckbox).setOnClickListener(this);
                this.i.findViewById(R.id.searchMinorCheckbox).setOnClickListener(this);
                this.i.findViewById(R.id.searchGameCheckbox).setOnClickListener(this);
                this.i.findViewById(R.id.searchSlamCheckbox).setOnClickListener(this);
                this.i.findViewById(R.id.searchMissGameCheckbox).setOnClickListener(this);
                this.i.findViewById(R.id.searchMissSlamCheckbox).setOnClickListener(this);
                this.i.findViewById(R.id.searchSetOneCheckbox).setOnClickListener(this);
                this.i.findViewById(R.id.searchSetMoreCheckbox).setOnClickListener(this);
            }
            this.i.show();
        } else if (this.i != null && view == this.i.findViewById(R.id.searchButtonOk)) {
            StringBuilder sb = new StringBuilder();
            if (!((CheckBox) this.i.findViewById(R.id.searchAllCheckbox)).isChecked()) {
                sb.append("all=N;");
                if (((CheckBox) this.i.findViewById(R.id.searchNTCheckbox)).isChecked()) {
                    sb.append("nt=Y;");
                }
                if (((CheckBox) this.i.findViewById(R.id.searchMajorCheckbox)).isChecked()) {
                    sb.append("major=Y;");
                }
                if (((CheckBox) this.i.findViewById(R.id.searchGameCheckbox)).isChecked()) {
                    sb.append("game=Y;");
                }
                if (((CheckBox) this.i.findViewById(R.id.searchSlamCheckbox)).isChecked()) {
                    sb.append("slam=Y;");
                }
                if (((CheckBox) this.i.findViewById(R.id.searchMissGameCheckbox)).isChecked()) {
                    sb.append("game_miss=Y;");
                }
                if (((CheckBox) this.i.findViewById(R.id.searchMissSlamCheckbox)).isChecked()) {
                    sb.append("slam_miss=Y;");
                }
                if (((CheckBox) this.i.findViewById(R.id.searchSetOneCheckbox)).isChecked()) {
                    sb.append("one_set=Y;");
                }
                if (((CheckBox) this.i.findViewById(R.id.searchSetMoreCheckbox)).isChecked()) {
                    sb.append("more_set=Y;");
                }
            }
            this.h = sb.toString();
            b();
            this.i.dismiss();
        } else if (this.i != null && view == this.i.findViewById(R.id.searchButtonCancel)) {
            this.i.dismiss();
        } else if (this.i != null && view == this.i.findViewById(R.id.searchAllCheckbox)) {
            if (((CheckBox) this.i.findViewById(R.id.searchAllCheckbox)).isChecked()) {
                this.i.findViewById(R.id.searchNTCheckbox).setEnabled(false);
                this.i.findViewById(R.id.searchMajorCheckbox).setEnabled(false);
                this.i.findViewById(R.id.searchMinorCheckbox).setEnabled(false);
                this.i.findViewById(R.id.searchGameCheckbox).setEnabled(false);
                this.i.findViewById(R.id.searchSlamCheckbox).setEnabled(false);
                this.i.findViewById(R.id.searchMissGameCheckbox).setEnabled(false);
                this.i.findViewById(R.id.searchMissSlamCheckbox).setEnabled(false);
                this.i.findViewById(R.id.searchSetOneCheckbox).setEnabled(false);
                this.i.findViewById(R.id.searchSetMoreCheckbox).setEnabled(false);
                return;
            }
            this.i.findViewById(R.id.searchNTCheckbox).setEnabled(true);
            this.i.findViewById(R.id.searchMajorCheckbox).setEnabled(true);
            this.i.findViewById(R.id.searchMinorCheckbox).setEnabled(true);
            this.i.findViewById(R.id.searchGameCheckbox).setEnabled(true);
            this.i.findViewById(R.id.searchSlamCheckbox).setEnabled(true);
            this.i.findViewById(R.id.searchMissGameCheckbox).setEnabled(true);
            this.i.findViewById(R.id.searchMissSlamCheckbox).setEnabled(true);
            this.i.findViewById(R.id.searchSetOneCheckbox).setEnabled(true);
            this.i.findViewById(R.id.searchSetMoreCheckbox).setEnabled(true);
        }
    }

    /* access modifiers changed from: private */
    public void b() {
        if (this.b != null) {
            this.b.clear();
        }
        this.c.clear();
        if (this.d != null) {
            this.d.notifyDataSetChanged();
        }
        a();
    }
}
