package net.weweweb.android.bridge;

import a.g;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import net.weweweb.android.free.bridge.R;

/* compiled from: ProGuard */
public class BidRuleActivity extends Activity implements View.OnClickListener, Runnable {

    /* renamed from: a  reason: collision with root package name */
    m f9a;
    ProgressDialog b;
    public Handler c = new c(this);
    /* access modifiers changed from: private */
    public int d = 0;

    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        r0.close();
        r9.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        return r1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int a() {
        /*
            r9 = this;
            r1 = 0
            java.lang.String r2 = "RECORD-COUNT:"
            net.weweweb.android.bridge.m r0 = r9.f9a     // Catch:{ Exception -> 0x0076 }
            java.sql.Timestamp r0 = r0.c()     // Catch:{ Exception -> 0x0076 }
            if (r0 != 0) goto L_0x0012
            java.sql.Timestamp r0 = new java.sql.Timestamp     // Catch:{ Exception -> 0x0076 }
            r3 = 0
            r0.<init>(r3)     // Catch:{ Exception -> 0x0076 }
        L_0x0012:
            java.net.URL r3 = new java.net.URL     // Catch:{ Exception -> 0x0076 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0076 }
            r4.<init>()     // Catch:{ Exception -> 0x0076 }
            java.lang.String r5 = "http://servlet.weweweb.net/servlet/DataExporter?dataclass=bridge_bid_rule&countflag=Y&lastupdate="
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0076 }
            long r5 = r0.getTime()     // Catch:{ Exception -> 0x0076 }
            r7 = 1
            long r5 = r5 + r7
            java.lang.StringBuilder r0 = r4.append(r5)     // Catch:{ Exception -> 0x0076 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0076 }
            r3.<init>(r0)     // Catch:{ Exception -> 0x0076 }
            java.net.URLConnection r9 = r3.openConnection()     // Catch:{ Exception -> 0x0076 }
            java.net.HttpURLConnection r9 = (java.net.HttpURLConnection) r9     // Catch:{ Exception -> 0x0076 }
            r0 = 10000(0x2710, float:1.4013E-41)
            r9.setConnectTimeout(r0)     // Catch:{ Exception -> 0x0076 }
            r0 = 10000(0x2710, float:1.4013E-41)
            r9.setReadTimeout(r0)     // Catch:{ Exception -> 0x0076 }
            r9.connect()     // Catch:{ Exception -> 0x0076 }
            java.lang.Object r0 = r9.getContent()     // Catch:{ Exception -> 0x0076 }
            java.io.InputStream r0 = (java.io.InputStream) r0     // Catch:{ Exception -> 0x0076 }
            java.io.BufferedReader r3 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0076 }
            java.io.InputStreamReader r4 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0076 }
            r4.<init>(r0)     // Catch:{ Exception -> 0x0076 }
            r3.<init>(r4)     // Catch:{ Exception -> 0x0076 }
        L_0x0054:
            java.lang.String r4 = r3.readLine()     // Catch:{ Exception -> 0x006d }
            if (r4 == 0) goto L_0x006e
            boolean r5 = r4.startsWith(r2)     // Catch:{ Exception -> 0x006d }
            if (r5 == 0) goto L_0x006e
            int r5 = r2.length()     // Catch:{ Exception -> 0x006d }
            java.lang.String r4 = r4.substring(r5)     // Catch:{ Exception -> 0x006d }
            int r1 = java.lang.Integer.parseInt(r4)     // Catch:{ Exception -> 0x006d }
            goto L_0x0054
        L_0x006d:
            r2 = move-exception
        L_0x006e:
            r0.close()     // Catch:{ Exception -> 0x0076 }
            r9.disconnect()     // Catch:{ Exception -> 0x0076 }
            r0 = r1
        L_0x0075:
            return r0
        L_0x0076:
            r0 = move-exception
            java.lang.String r2 = "updateBridgeRule"
            java.lang.String r0 = r0.toString()
            android.util.Log.e(r2, r0)
            r0 = r1
            goto L_0x0075
        */
        throw new UnsupportedOperationException("Method not decompiled: net.weweweb.android.bridge.BidRuleActivity.a():int");
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.bidrule);
        this.f9a = new m(this);
        this.f9a.e();
        b();
        findViewById(R.id.bidRuleUpdateButton).setOnClickListener(this);
        findViewById(R.id.bidRuleClearButton).setOnClickListener(this);
        findViewById(R.id.bidRuleWebLink).setOnClickListener(this);
    }

    public void onClick(View view) {
        String str;
        if (view == findViewById(R.id.bidRuleUpdateButton)) {
            this.d = a();
            if (this.d == 0) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("No more to download or unable to connect to server.").setTitle((int) R.string.alert).setIcon(17301543).setCancelable(false).setPositiveButton("OK", new d(this));
                builder.create().show();
                return;
            }
            int i = this.d * 128;
            if (i >= 1048576) {
                str = Integer.toString((i / 1024) / 1024) + "." + Integer.toString((i / 1024) % 1024) + "MB";
            } else if (i >= 1024) {
                str = Integer.toString(i / 1024) + "KB";
            } else {
                str = Integer.toString(i) + "B";
            }
            AlertDialog.Builder builder2 = new AlertDialog.Builder(this);
            builder2.setMessage(g.a(getString(R.string.download_bid_rule), str)).setCancelable(false).setTitle((int) R.string.confirm_required).setPositiveButton("Yes", new f(this)).setNegativeButton("No", new e(this));
            builder2.create().show();
        } else if (view == findViewById(R.id.bidRuleClearButton)) {
            AlertDialog.Builder builder3 = new AlertDialog.Builder(this);
            builder3.setMessage(getString(R.string.clear_bid_rule)).setCancelable(false).setTitle((int) R.string.confirm_required).setPositiveButton("Yes", new h(this)).setNegativeButton("No", new g(this));
            builder3.create().show();
        } else if (view == findViewById(R.id.bidRuleWebLink)) {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse((String) ((TextView) view).getText())));
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.f9a != null) {
            this.f9a.b();
        }
        if (this.b != null) {
            this.b.dismiss();
        }
        super.onDestroy();
    }

    /* access modifiers changed from: private */
    public void b() {
        try {
            long d2 = this.f9a.d();
            ((TextView) findViewById(R.id.bidRuleCount)).setText("Total Rules: " + d2);
            if (d2 == 0) {
                findViewById(R.id.bidRuleClearButton).setEnabled(false);
            } else {
                findViewById(R.id.bidRuleClearButton).setEnabled(true);
            }
            ((TextView) findViewById(R.id.bidRuleLastUpdate)).setText("Last Update: " + (d2 == 0 ? "N/A" : new SimpleDateFormat("yyyy-MM-dd HH:mm").format(this.f9a.c())));
        } catch (Exception e) {
        }
    }

    public void run() {
        try {
            Timestamp c2 = this.f9a.c();
            if (c2 == null) {
                c2 = new Timestamp(0);
            }
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL("http://servlet.weweweb.net/servlet/DataExporter?dataclass=bridge_bid_rule&lastupdate=" + (c2.getTime() + 1)).openConnection();
            httpURLConnection.setConnectTimeout(10000);
            httpURLConnection.setReadTimeout(10000);
            httpURLConnection.connect();
            InputStream inputStream = (InputStream) httpURLConnection.getContent();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            int i = 0;
            while (true) {
                try {
                    String readLine = bufferedReader.readLine();
                    if (readLine != null) {
                        if (readLine.contains("@_@")) {
                            int i2 = i + 1;
                            String[] split = readLine.split("@_@");
                            if (split.length == 8) {
                                try {
                                    this.f9a.a(Integer.parseInt(split[0]), split[1], split[2], split[3], Short.parseShort(split[4]), split[5], split[6], new Timestamp(Long.parseLong(split[7])));
                                    if (i2 % 50 == 0) {
                                        Message obtain = Message.obtain();
                                        obtain.what = 2;
                                        obtain.arg1 = i2;
                                        this.c.sendMessage(obtain);
                                    }
                                    i = i2;
                                } catch (Exception e) {
                                    i = i2;
                                }
                            } else {
                                i = i2;
                            }
                        }
                    }
                } catch (Exception e2) {
                }
                break;
            }
            inputStream.close();
            httpURLConnection.disconnect();
        } catch (Exception e3) {
            Log.e("updateBridgeRule", e3.toString());
        }
        Message obtain2 = Message.obtain();
        obtain2.what = 1;
        this.c.sendMessage(obtain2);
    }
}
