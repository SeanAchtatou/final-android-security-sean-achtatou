package net.weweweb.android.bridge;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import java.util.ArrayList;
import java.util.HashMap;
import net.weweweb.android.free.bridge.R;

/* compiled from: ProGuard */
public class ReferenceMenuActivity extends Activity {
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.mainmenu);
        ListView listView = (ListView) findViewById(R.id.mainMenu);
        ArrayList arrayList = new ArrayList();
        HashMap hashMap = new HashMap();
        hashMap.put("icon", Integer.valueOf((int) R.drawable.ic_robot));
        hashMap.put("title", "Robot Bidding System");
        hashMap.put("desc", "The SAYC bidding standard used by robot.");
        arrayList.add(hashMap);
        listView.setAdapter((ListAdapter) new SimpleAdapter(this, arrayList, R.layout.mainmenuitem, new String[]{"icon", "title", "desc"}, new int[]{R.id.mainMenuItemImage, R.id.mainMenuItemTitle, R.id.mainMenuItemDesc}));
        listView.setOnItemClickListener(new bb(this));
    }
}
