package net.weweweb.android.bridge;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/* compiled from: ProGuard */
final class n extends SQLiteOpenHelper {
    n(Context context) {
        super(context, "bridge_bid_rule.db", (SQLiteDatabase.CursorFactory) null, 1);
    }

    public final void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("CREATE TABLE bridge_bid_rule (sys_id integer NOT NULL DEFAULT 0,bid_i character varying(255) NOT NULL,bid_o character varying(2) NOT NULL DEFAULT 'P',rules character varying(255) NOT NULL DEFAULT 'T',priority smallint DEFAULT 0,alert character(1) NOT NULL DEFAULT 'N',alert_des character varying(128),last_update timestamp,CONSTRAINT bridge_bid_rule_pk PRIMARY KEY (sys_id, bid_i, bid_o));");
    }

    public final void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        Log.w("BridgeBidRuleDBAdapter", "Upgrading database from version " + i + " to " + i2 + ", which will destroy all old data");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS bridge_bid_rule");
        onCreate(sQLiteDatabase);
    }
}
