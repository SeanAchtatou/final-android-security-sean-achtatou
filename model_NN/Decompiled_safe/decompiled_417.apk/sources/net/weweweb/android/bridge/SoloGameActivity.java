package net.weweweb.android.bridge;

import a.b;
import a.c;
import a.g;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import java.util.HashMap;
import net.weweweb.android.common.a;
import net.weweweb.android.free.bridge.R;

/* compiled from: ProGuard */
public class SoloGameActivity extends BridgeGameActivity implements View.OnClickListener {
    private ImageView[] A = new ImageView[13];
    bp e;
    FrameLayout f;
    byte g = 1;
    byte h = 1;
    i i = null;
    Button j = null;
    Button k = null;
    Button l = null;
    Button m = null;
    Button[] n = new Button[7];
    Button[] o = new Button[5];
    ScrollView p = null;
    TableLayout q = null;
    int r = 0;
    int s = 0;
    HashMap t = new HashMap();
    Dialog u = null;
    a v = null;
    bx w = null;
    Button[] x;
    Button y;
    m z;

    private void i() {
        TableLayout tableLayout = (TableLayout) findViewById(R.id.rubberScoreTableLayout);
        bj bjVar = this.e.o;
        if (bjVar.c()) {
            View view = new View(this);
            view.setBackgroundColor(-3355444);
            tableLayout.addView(view, new ViewGroup.LayoutParams(-1, 2));
            TableRow tableRow = new TableRow(this);
            tableRow.setBackgroundColor(-1);
            TableRow.LayoutParams layoutParams = new TableRow.LayoutParams();
            layoutParams.setMargins(0, 0, 0, 0);
            for (int i2 = 0; i2 < 3; i2++) {
                TextView textView = new TextView(this);
                textView.setPadding(0, 1, 0, 1);
                textView.setTextColor(-16777216);
                textView.setBackgroundColor(-1);
                textView.setGravity(17);
                tableRow.addView(textView, layoutParams);
                if (i2 == 0) {
                    textView.setText("Total");
                } else if (i2 == 1) {
                    textView.setText(Integer.toString(bjVar.g[this.f12a.m.e % 2]));
                } else {
                    textView.setText(Integer.toString(bjVar.g[(this.f12a.m.e + 1) % 2]));
                }
            }
            tableLayout.addView(tableRow);
            View view2 = new View(this);
            view2.setBackgroundColor(-3355444);
            tableLayout.addView(view2, new ViewGroup.LayoutParams(-1, 2));
            View view3 = new View(this);
            view3.setBackgroundColor(-1);
            tableLayout.addView(view3, new ViewGroup.LayoutParams(-1, 2));
            View view4 = new View(this);
            view4.setBackgroundColor(-3355444);
            tableLayout.addView(view4, new ViewGroup.LayoutParams(-1, 2));
        }
    }

    private void b(c cVar) {
        if (cVar.c() != 99) {
            TableLayout tableLayout = (TableLayout) findViewById(R.id.rubberScoreTableLayout);
            int a2 = b.a(cVar.c(), cVar.d(), cVar.h());
            int c = b.c(b.l(cVar.i(), cVar.g()), cVar.c(), cVar.d(), cVar.h());
            int a3 = b.a(b.l(cVar.i(), cVar.g()), cVar.c(), cVar.d(), cVar.h());
            int a4 = b.a(b.l(cVar.i(), cVar.g()), cVar.c(), cVar.h());
            int d = b.d(b.l(cVar.i(), cVar.g()), cVar.c(), cVar.d(), cVar.h());
            if (c > 0 || a3 > 0 || a4 > 0 || d < 0) {
                TableRow tableRow = new TableRow(this);
                tableRow.setBackgroundColor(-1);
                TableRow.LayoutParams layoutParams = new TableRow.LayoutParams();
                layoutParams.setMargins(0, 0, 0, 0);
                for (int i2 = 0; i2 < 3; i2++) {
                    TextView textView = new TextView(this);
                    textView.setPadding(0, 1, 0, 1);
                    textView.setTextColor(-16777216);
                    textView.setBackgroundColor(-1);
                    textView.setGravity(17);
                    tableRow.addView(textView, layoutParams);
                    if (i2 == 0) {
                        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
                        spannableStringBuilder.append((CharSequence) (cVar.c + ". "));
                        spannableStringBuilder.append((CharSequence) cVar.a(false));
                        int indexOf = spannableStringBuilder.toString().indexOf(b.e[1]);
                        if (indexOf >= 0) {
                            spannableStringBuilder.setSpan(new ForegroundColorSpan(-65536), indexOf, indexOf + 1, 33);
                        } else {
                            int indexOf2 = spannableStringBuilder.toString().indexOf(b.e[2]);
                            if (indexOf2 >= 0) {
                                spannableStringBuilder.setSpan(new ForegroundColorSpan(-65536), indexOf2, indexOf2 + 1, 33);
                            }
                        }
                        textView.setText(spannableStringBuilder);
                    } else if (i2 == 1) {
                        if (b.i(this.f12a.m.e, cVar.g()) && b.b(cVar.c(), cVar.h()) >= 0) {
                            if (c > 0) {
                                if (textView.getText().length() > 0) {
                                    textView.setText(((Object) textView.getText()) + "\n");
                                }
                                textView.setText(((Object) textView.getText()) + Integer.toString(c));
                            }
                            if (a3 > 0) {
                                if (textView.getText().length() > 0) {
                                    textView.setText(((Object) textView.getText()) + "\n");
                                }
                                textView.setText(((Object) textView.getText()) + Integer.toString(a3));
                            }
                            if (a4 > 0) {
                                if (textView.getText().length() > 0) {
                                    textView.setText(((Object) textView.getText()) + "\n");
                                }
                                textView.setText(((Object) textView.getText()) + Integer.toString(a4));
                            }
                        } else if (!b.i(this.f12a.m.e, cVar.g()) && b.b(cVar.c(), cVar.h()) < 0) {
                            textView.setText(Integer.toString(-d));
                        }
                    } else if (i2 == 2) {
                        if (!b.i(this.f12a.m.e, cVar.g()) && b.b(cVar.c(), cVar.h()) >= 0) {
                            if (c > 0) {
                                if (textView.getText().length() > 0) {
                                    textView.setText(((Object) textView.getText()) + "\n");
                                }
                                textView.setText(((Object) textView.getText()) + Integer.toString(c));
                            }
                            if (a3 > 0) {
                                if (textView.getText().length() > 0) {
                                    textView.setText(((Object) textView.getText()) + "\n");
                                }
                                textView.setText(((Object) textView.getText()) + Integer.toString(a3));
                            }
                            if (a4 > 0) {
                                if (textView.getText().length() > 0) {
                                    textView.setText(((Object) textView.getText()) + "\n");
                                }
                                textView.setText(((Object) textView.getText()) + Integer.toString(a4));
                            }
                        } else if (b.i(this.f12a.m.e, cVar.g()) && b.b(cVar.c(), cVar.h()) < 0) {
                            textView.setText(Integer.toString(-d));
                        }
                    }
                }
                tableLayout.addView(tableRow, 1);
            }
            if (a2 > 0) {
                TableRow tableRow2 = new TableRow(this);
                tableRow2.setBackgroundColor(-1);
                TableRow.LayoutParams layoutParams2 = new TableRow.LayoutParams();
                layoutParams2.setMargins(0, 0, 0, 0);
                for (int i3 = 0; i3 < 3; i3++) {
                    TextView textView2 = new TextView(this);
                    textView2.setPadding(0, 1, 0, 1);
                    textView2.setTextColor(-16777216);
                    textView2.setBackgroundColor(-1);
                    textView2.setGravity(17);
                    tableRow2.addView(textView2, layoutParams2);
                    if (i3 == 0) {
                        SpannableStringBuilder spannableStringBuilder2 = new SpannableStringBuilder();
                        spannableStringBuilder2.append((CharSequence) (cVar.c + ". "));
                        spannableStringBuilder2.append((CharSequence) cVar.a(false));
                        int indexOf3 = spannableStringBuilder2.toString().indexOf(b.e[1]);
                        if (indexOf3 >= 0) {
                            spannableStringBuilder2.setSpan(new ForegroundColorSpan(-65536), indexOf3, indexOf3 + 1, 33);
                        } else {
                            int indexOf4 = spannableStringBuilder2.toString().indexOf(b.e[2]);
                            if (indexOf4 >= 0) {
                                spannableStringBuilder2.setSpan(new ForegroundColorSpan(-65536), indexOf4, indexOf4 + 1, 33);
                            }
                        }
                        textView2.setText(spannableStringBuilder2);
                    } else if (i3 == 1) {
                        if (b.i(this.f12a.m.e, cVar.g()) && a2 > 0) {
                            textView2.setText(Integer.toString(a2));
                        }
                    } else if (i3 == 2 && !b.i(this.f12a.m.e, cVar.g()) && a2 > 0) {
                        textView2.setText(Integer.toString(a2));
                    }
                }
                tableLayout.addView(tableRow2);
            }
        }
    }

    private void a(int i2) {
        if (this.e != null) {
            int i3 = this.r;
            int i4 = this.s;
            c(this.e.a(-997, i2, 0));
            c(this.e.a(-997, i2, 1));
            if (this.r - i3 >= 0) {
                this.s += b.a(Math.abs(this.r - i3));
            } else {
                this.s -= b.a(Math.abs(i3 - this.r));
            }
            TableLayout tableLayout = (TableLayout) findViewById(R.id.duplicateScoreTableLayout);
            TableRow tableRow = new TableRow(this);
            TableRow.LayoutParams layoutParams = new TableRow.LayoutParams();
            layoutParams.setMargins(0, 1, 0, 1);
            for (int i5 = 0; i5 < 3; i5++) {
                TextView textView = new TextView(this);
                textView.setPadding(0, 1, 0, 1);
                textView.setBackgroundColor(Color.rgb(255, 255, 128));
                textView.setTextColor(-16777216);
                textView.setGravity(17);
                tableRow.addView(textView, layoutParams);
                if (i5 == 0) {
                    textView.setText("Board #" + Integer.toString(i2));
                } else if (i5 == 1) {
                    textView.setText(Integer.toString(this.r - i3));
                } else {
                    textView.setText(Integer.toString(this.s - i4));
                }
            }
            tableLayout.addView(tableRow, tableLayout.getChildCount() - 1);
            TableRow tableRow2 = (TableRow) tableLayout.getChildAt(tableLayout.getChildCount() - 1);
            ((TextView) tableRow2.getChildAt(1)).setText(Integer.toString(this.r));
            ((TextView) tableRow2.getChildAt(2)).setText(Integer.toString(this.s));
        }
    }

    private void c(c cVar) {
        int i2;
        if (cVar != null) {
            TableLayout tableLayout = (TableLayout) findViewById(R.id.duplicateScoreTableLayout);
            TableRow tableRow = new TableRow(this);
            TableRow.LayoutParams layoutParams = new TableRow.LayoutParams();
            layoutParams.setMargins(0, 0, 0, 0);
            for (int i3 = 0; i3 < 3; i3++) {
                byte b = this.f12a.m.e;
                for (int i4 = 0; i4 < cVar.j.length; i4++) {
                    if (cVar.j[i4] != null && cVar.j[i4].equals("[You]")) {
                        b = (byte) i4;
                    }
                }
                TextView textView = new TextView(this);
                textView.setPadding(0, 1, 0, 1);
                textView.setBackgroundColor(-1);
                textView.setTextColor(-16777216);
                textView.setGravity(17);
                tableRow.addView(textView, layoutParams);
                if (i3 == 0) {
                    SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
                    spannableStringBuilder.append((CharSequence) (Integer.toString(cVar.c) + "."));
                    if (b % 2 == 0) {
                        spannableStringBuilder.append((CharSequence) "NS: ");
                    } else {
                        spannableStringBuilder.append((CharSequence) "EW: ");
                    }
                    spannableStringBuilder.append((CharSequence) cVar.a(false));
                    int indexOf = spannableStringBuilder.toString().indexOf(b.e[1]);
                    if (indexOf >= 0) {
                        spannableStringBuilder.setSpan(new ForegroundColorSpan(-65536), indexOf, indexOf + 1, 33);
                    } else {
                        int indexOf2 = spannableStringBuilder.toString().indexOf(b.e[2]);
                        if (indexOf2 >= 0) {
                            spannableStringBuilder.setSpan(new ForegroundColorSpan(-65536), indexOf2, indexOf2 + 1, 33);
                        }
                    }
                    textView.setText(spannableStringBuilder);
                } else if (i3 == 1) {
                    if (cVar.c() != 99) {
                        int b2 = b.b(b.l(cVar.i(), cVar.g()), cVar.c(), cVar.d(), cVar.h());
                        if (!b.i(b, cVar.g())) {
                            i2 = b2 * -1;
                        } else {
                            i2 = b2;
                        }
                        textView.setText(Integer.toString(i2));
                    } else {
                        textView.setText("0");
                        i2 = 0;
                    }
                    this.r = i2 + this.r;
                } else if (i3 == 2) {
                    textView.setText("N/A");
                }
            }
            tableLayout.addView(tableRow, tableLayout.getChildCount() - 1);
        }
    }

    private void j() {
        TableLayout tableLayout = (TableLayout) findViewById(R.id.rubberScoreTableLayout);
        bj bjVar = this.e.o;
        if (bjVar.d()) {
            TableRow tableRow = new TableRow(this);
            tableRow.setBackgroundColor(-1);
            TableRow.LayoutParams layoutParams = new TableRow.LayoutParams();
            layoutParams.setMargins(0, 0, 0, 0);
            for (int i2 = 0; i2 < 3; i2++) {
                TextView textView = new TextView(this);
                textView.setPadding(0, 1, 0, 1);
                textView.setTextColor(-16777216);
                textView.setBackgroundColor(-1);
                textView.setGravity(17);
                tableRow.addView(textView, layoutParams);
                if (i2 == 0) {
                    textView.setText("Game Bonus");
                } else if (i2 == 1) {
                    if (bjVar.a((byte) (this.f12a.m.e % 2)) > 0) {
                        textView.setText(Integer.toString(bjVar.a((byte) (this.f12a.m.e % 2))));
                    }
                } else if (i2 == 2 && bjVar.a((byte) ((this.f12a.m.e + 1) % 2)) > 0) {
                    textView.setText(Integer.toString(bjVar.a((byte) ((this.f12a.m.e + 1) % 2))));
                }
            }
            tableLayout.addView(tableRow);
            View view = new View(this);
            view.setBackgroundColor(-3355444);
            tableLayout.addView(view, new ViewGroup.LayoutParams(-1, 2));
            TableRow tableRow2 = new TableRow(this);
            tableRow2.setBackgroundColor(-1);
            TableRow.LayoutParams layoutParams2 = new TableRow.LayoutParams();
            layoutParams2.setMargins(0, 0, 0, 0);
            for (int i3 = 0; i3 < 3; i3++) {
                TextView textView2 = new TextView(this);
                textView2.setPadding(0, 1, 0, 1);
                textView2.setTextColor(-16777216);
                textView2.setBackgroundColor(-1);
                textView2.setGravity(17);
                tableRow2.addView(textView2, layoutParams2);
                if (i3 == 0) {
                    textView2.setText("Total");
                } else if (i3 == 1) {
                    textView2.setText(Integer.toString(bjVar.f[this.f12a.m.e % 2]));
                } else {
                    textView2.setText(Integer.toString(bjVar.f[(this.f12a.m.e + 1) % 2]));
                }
            }
            tableLayout.addView(tableRow2);
            View view2 = new View(this);
            view2.setBackgroundColor(-3355444);
            tableLayout.addView(view2, new ViewGroup.LayoutParams(-1, 2));
            View view3 = new View(this);
            view3.setBackgroundColor(-1);
            tableLayout.addView(view3, new ViewGroup.LayoutParams(-1, 2));
            View view4 = new View(this);
            view4.setBackgroundColor(-3355444);
            tableLayout.addView(view4, new ViewGroup.LayoutParams(-1, 2));
        }
    }

    private void d(c cVar) {
        int i2;
        if (cVar != null) {
            TableLayout tableLayout = (TableLayout) findViewById(R.id.scoreTableLayout);
            TableRow tableRow = new TableRow(this);
            TableRow.LayoutParams layoutParams = new TableRow.LayoutParams();
            layoutParams.setMargins(0, 0, 0, 1);
            for (int i3 = 0; i3 < 3; i3++) {
                TextView textView = new TextView(this);
                textView.setPadding(0, 1, 0, 1);
                if (cVar.f2a % 2 == 0) {
                    textView.setBackgroundColor(Color.rgb(255, 255, 128));
                } else {
                    textView.setBackgroundColor(-1);
                }
                textView.setTextColor(-16777216);
                textView.setGravity(17);
                tableRow.addView(textView, layoutParams);
                if (i3 == 0) {
                    SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
                    spannableStringBuilder.append((CharSequence) cVar.a(true));
                    int indexOf = spannableStringBuilder.toString().indexOf(b.e[1]);
                    if (indexOf >= 0) {
                        spannableStringBuilder.setSpan(new ForegroundColorSpan(-65536), indexOf, indexOf + 1, 33);
                    } else {
                        int indexOf2 = spannableStringBuilder.toString().indexOf(b.e[2]);
                        if (indexOf2 >= 0) {
                            spannableStringBuilder.setSpan(new ForegroundColorSpan(-65536), indexOf2, indexOf2 + 1, 33);
                        }
                    }
                    textView.setText(spannableStringBuilder);
                } else if (i3 == 1) {
                    byte b = this.f12a.m.e;
                    if (this.e.n == 99 && cVar.j != null) {
                        for (byte b2 = 0; b2 < cVar.j.length; b2 = (byte) (b2 + 1)) {
                            if (cVar.j[b2] != null && cVar.j[b2].equals("[You]")) {
                                b = b2;
                            }
                        }
                    }
                    if (this.b.k() != 99) {
                        int b3 = b.b(b.l(cVar.i(), cVar.g()), cVar.c(), cVar.d(), cVar.h());
                        if (!b.i(b, cVar.g())) {
                            b3 *= -1;
                        }
                        textView.setText(Integer.toString(b3));
                        i2 = b3;
                    } else {
                        textView.setText("0");
                        i2 = 0;
                    }
                    this.r = i2 + this.r;
                } else if (i3 == 2) {
                    textView.setText("N/A");
                }
            }
            tableLayout.addView(tableRow, tableLayout.getChildCount() - 1);
            ((TextView) ((TableRow) tableLayout.getChildAt(tableLayout.getChildCount() - 1)).getChildAt(1)).setText(Integer.toString(this.r));
        }
    }

    private void k() {
        this.j = (Button) findViewById(R.id.btnBidPass);
        this.j.setOnClickListener(this);
        this.k = (Button) findViewById(R.id.btnDouble);
        this.k.setOnClickListener(this);
        this.l = (Button) findViewById(R.id.btnRedouble);
        this.l.setOnClickListener(this);
        this.m = (Button) findViewById(R.id.btnAlert);
        this.m.setOnClickListener(this);
        this.n[0] = (Button) findViewById(R.id.btnBidL1);
        this.n[1] = (Button) findViewById(R.id.btnBidL2);
        this.n[2] = (Button) findViewById(R.id.btnBidL3);
        this.n[3] = (Button) findViewById(R.id.btnBidL4);
        this.n[4] = (Button) findViewById(R.id.btnBidL5);
        this.n[5] = (Button) findViewById(R.id.btnBidL6);
        this.n[6] = (Button) findViewById(R.id.btnBidL7);
        this.o[4] = (Button) findViewById(R.id.btnBidNT);
        this.o[3] = (Button) findViewById(R.id.btnBidS);
        this.o[2] = (Button) findViewById(R.id.btnBidH);
        this.o[1] = (Button) findViewById(R.id.btnBidD);
        this.o[0] = (Button) findViewById(R.id.btnBidC);
        for (int i2 = 0; i2 < 7; i2++) {
            this.n[i2].setOnClickListener(this);
        }
        for (int i3 = 0; i3 < 5; i3++) {
            this.o[i3].setOnClickListener(this);
        }
    }

    /* access modifiers changed from: package-private */
    public final void c() {
        this.j.setEnabled(false);
        for (Button enabled : this.n) {
            enabled.setEnabled(false);
        }
        for (Button enabled2 : this.o) {
            enabled2.setEnabled(false);
        }
        this.k.setEnabled(false);
        this.l.setEnabled(false);
        this.m.setEnabled(false);
    }

    /* access modifiers changed from: package-private */
    public final void d() {
        if (this.b.i() == this.f12a.m.e) {
            this.j.setEnabled(true);
            for (int i2 = 0; i2 < this.n.length; i2++) {
                if (i2 >= this.g - 1) {
                    this.n[i2].setEnabled(true);
                }
            }
            for (Button enabled : this.o) {
                enabled.setEnabled(true);
            }
            if (this.b.L((byte) 97)) {
                this.k.setEnabled(true);
            }
            if (this.b.L((byte) 98)) {
                this.l.setEnabled(true);
            }
        }
    }

    public final void a(c cVar) {
        bp bpVar = (bp) this.f12a.m;
        if (bpVar.n == 1) {
            b((int) R.layout.rubberscoreboard);
            m();
            if (bpVar.b.k() == 99) {
                Toast.makeText(this, "All Pass", 1).show();
            }
        } else if (bpVar.n == 2) {
            b((int) R.layout.rubberscoreboard);
            m();
            if (bpVar.b.k() == 99) {
                Toast.makeText(this, "All Pass", 1).show();
            }
        } else if (bpVar.n == 3) {
            b((int) R.layout.duplicatescoreboard);
            m();
        } else {
            b((int) R.layout.singlegamescoreboard);
            if (((TableLayout) findViewById(R.id.scoreTableLayout)).getChildCount() == 2) {
                int i2 = 0;
                while (true) {
                    int i3 = i2;
                    if (i3 >= bpVar.i.size()) {
                        break;
                    }
                    if (bpVar.i.get(i3) != cVar) {
                        d((c) bpVar.i.get(i3));
                    }
                    i2 = i3 + 1;
                }
            }
            d(cVar);
            ((ScrollView) findViewById(R.id.scoreScrollView)).fullScroll(130);
        }
    }

    private void l() {
        TableLayout tableLayout = (TableLayout) findViewById(R.id.rubberScoreTableLayout);
        if (tableLayout != null && tableLayout.getChildCount() > 1) {
            tableLayout.removeViews(1, tableLayout.getChildCount() - 1);
            View view = new View(this);
            view.setBackgroundColor(-16777216);
            tableLayout.addView(view, new ViewGroup.LayoutParams(-1, 2));
        }
    }

    /* access modifiers changed from: package-private */
    public final void e() {
        b((int) R.layout.bidding);
        n();
        this.i.b();
        b((byte) 1);
        for (int i2 = 0; i2 < 13; i2++) {
            if (this.b.l[this.f12a.m.e][i2] != -1) {
                this.A[i2].setImageResource(a.k[this.b.l[this.f12a.m.e][i2]]);
            } else {
                this.A[i2].setImageBitmap(null);
            }
        }
        if (this.c != null) {
            this.c.e[0] = -1;
        }
    }

    public void onClick(View view) {
        if (view.equals(this.j)) {
            this.e.b((byte) 99);
        } else if (view.equals(this.k)) {
            this.e.b((byte) 97);
        } else if (view.equals(this.l)) {
            this.e.b((byte) 98);
        } else if (view.equals(this.m)) {
            setContentView((int) R.layout.sologame);
        } else if (view.equals(findViewById(R.id.singleGameScoreBoardCloseButton))) {
            if (this.e.n == 99) {
                finish();
                this.e.a();
                this.e = null;
            } else if (this.e.h == 2) {
                this.e.f();
            } else if (this.e.h == 0) {
                b((int) R.layout.bidding);
            } else if (this.e.h == 1) {
                b((int) R.layout.playing);
            } else {
                this.e.f();
            }
        } else if (view == findViewById(R.id.singleGameScoreBoardSaveButton) || view == findViewById(R.id.rubberScoreBoardSaveButton)) {
            if (!BridgeApp.p) {
                this.e.a(bp.a(this.e.b), this.e.e);
                view.setEnabled(false);
            }
        } else if (view == findViewById(R.id.rubberScoreBoardButtonClose)) {
            if (this.e.h == 2) {
                this.e.f();
            } else if (this.e.h == 0) {
                b((int) R.layout.bidding);
            } else if (this.e.h == 1) {
                b((int) R.layout.playing);
            } else {
                this.e.f();
            }
        } else if (view == findViewById(R.id.duplicateScoreBoardButtonClose)) {
            if (this.e.h != 2) {
                if (this.e.h == 0) {
                    b((int) R.layout.bidding);
                    return;
                } else if (this.e.h == 1) {
                    b((int) R.layout.playing);
                    return;
                }
            }
            if (this.e.o.i < this.e.o.h) {
                this.e.f();
            } else if (this.e.q.c(-1000) < BridgeApp.u) {
                Toast.makeText(this, g.a(getString(R.string.min_saved_game_msg), Integer.toString(BridgeApp.u)), 0).show();
                Toast.makeText(this, getString(R.string.score_method_switch_to_single_game), 0).show();
                this.e.q.b(-997);
                BridgeApp.o = 0;
                this.f12a.a("soloGameScoreMethod", BridgeApp.o);
                this.e.f();
            } else {
                this.e.q.b(-997);
                this.e.q.a(BridgeApp.u);
                bj bjVar = this.e.o;
                bjVar.d = null;
                bjVar.e = null;
                bjVar.f = null;
                bjVar.g = null;
                bjVar.j = null;
                this.e.o = null;
                for (int i2 = 0; i2 < this.e.i.size(); i2++) {
                    ((c) this.e.i.get(i2)).a();
                }
                this.e.i.clear();
                this.e.e();
                this.f12a.h.e();
                this.e.a(true);
            }
        } else if (view != this.y) {
            for (int i3 = 0; i3 < 5; i3++) {
                if (view.equals(this.o[i3])) {
                    this.e.b(b.a(((int) this.h) + b.d[i3]));
                    return;
                }
            }
            for (int i4 = 0; i4 < 7; i4++) {
                if (view.equals(this.n[i4])) {
                    a((byte) (i4 + 1));
                    return;
                }
            }
            for (int i5 = 0; i5 < 14 && this.x != null; i5++) {
                if (this.x[i5] != null && view == this.x[i5]) {
                    this.f12a.m.h = 3;
                    ((bp) this.f12a.m).p = (byte) i5;
                    this.f12a.m.d.a(b.l(this.f12a.m.e));
                    this.f12a.m.d.s = (byte) i5;
                    this.f12a.m.d.a(1004, ((bp) this.f12a.m).t);
                    this.u.hide();
                    Toast.makeText(this, "Robot is considering the claim...", 0).show();
                }
            }
        } else if (this.u != null) {
            this.u.hide();
        }
    }

    public void onCreate(Bundle bundle) {
        try {
            super.onCreate(bundle);
            this.f12a.h = this;
            this.e = (bp) this.f12a.m;
            setContentView((int) R.layout.sologame);
            this.f = (FrameLayout) findViewById(R.id.contentPane);
            if (this.e.b.K()) {
                if (this.e.n == 1 || this.e.n == 2) {
                    b((int) R.layout.rubberscoreboard);
                } else if (this.e.n == 3) {
                    b((int) R.layout.duplicatescoreboard);
                } else {
                    b((int) R.layout.singlegamescoreboard);
                }
                m();
            } else if (this.e.b.I()) {
                g();
            } else {
                b((int) R.layout.bidding);
            }
            this.z = new m(this);
            this.z.e();
        } catch (Exception e2) {
            finish();
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.sologame_option_menu, menu);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.f12a.h == this) {
            this.f12a.h = null;
        }
        if (this.t != null) {
            this.t.clear();
            this.t = null;
        }
        if (this.z != null) {
            this.z.b();
            this.z = null;
        }
        if (this.u != null) {
            this.u.dismiss();
            this.u = null;
        }
        this.y = null;
        if (this.v != null) {
            this.v.dismiss();
            this.v = null;
        }
        if (this.w != null) {
            this.w.dismiss();
            this.w = null;
        }
        super.onDestroy();
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return super.onKeyDown(i2, keyEvent);
        }
        new AlertDialog.Builder(this).setIcon(17301543).setTitle("Confirm Action").setMessage("This will quit the current game, are you sure?").setPositiveButton("Yes", new be(this)).setNegativeButton("No", (DialogInterface.OnClickListener) null).show();
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.soloGameOptionMenuItemUndo /*2131165384*/:
                if (this.e != null && !this.e.b.I()) {
                    bp bpVar = this.e;
                    if (!bpVar.b.I() && Byte.valueOf(bpVar.b.i()).byteValue() == bpVar.e && bpVar.b.e() >= 3) {
                        bpVar.b.R();
                        bpVar.b.R();
                        bpVar.b.R();
                        bpVar.b.R();
                        if (!(this.e == null || this.e.d == null)) {
                            this.e.d.a();
                        }
                        f();
                        break;
                    }
                } else {
                    bp bpVar2 = (bp) this.f12a.m;
                    if (bpVar2 != null && bpVar2.h == 1) {
                        Byte valueOf = Byte.valueOf(bpVar2.b.j());
                        if (valueOf.byteValue() == bpVar2.e && bpVar2.b.B(bpVar2.e) && bpVar2.b.h() > 3) {
                            do {
                                bpVar2.b.S();
                            } while (bpVar2.b.j() != bpVar2.e);
                        } else if (!b.i(valueOf.byteValue(), bpVar2.e) || bpVar2.b.B(bpVar2.e) || bpVar2.b.h() <= 1) {
                        } else {
                            do {
                                bpVar2.b.S();
                            } while (!b.i(bpVar2.b.j(), bpVar2.e));
                        }
                        for (int i2 = 0; i2 < 4; i2++) {
                            b.b(bpVar2.b.l[i2]);
                        }
                        this.c.e[0] = -1;
                        this.c.invalidate();
                        break;
                    }
                }
                break;
            case R.id.soloGameOptionMenuItemClaim /*2131165385*/:
                int C = 13 - this.f12a.m.b.C();
                if (this.u == null) {
                    this.u = new Dialog(this);
                    this.u.setContentView((int) R.layout.claimdialog);
                    this.u.setTitle(this.f12a.getString(R.string.claim_dialog_title));
                    this.y = (Button) this.u.findViewById(R.id.claimButtonCancel);
                    this.y.setOnClickListener(this);
                    this.x = new Button[14];
                    for (int i3 = 0; i3 < this.x.length; i3++) {
                        this.x[i3] = new Button(this);
                        this.x[i3].setText(Integer.toString(i3));
                        this.x[i3].setOnClickListener(this);
                    }
                }
                TableLayout tableLayout = (TableLayout) this.u.findViewById(R.id.claimButtonsTable);
                int childCount = tableLayout.getChildCount();
                for (int i4 = 0; i4 < childCount; i4++) {
                    ((TableRow) tableLayout.getChildAt(i4)).removeAllViews();
                }
                tableLayout.removeAllViews();
                TableRow tableRow = null;
                for (int i5 = C; i5 >= 0; i5--) {
                    if ((C - i5) % 5 == 0) {
                        tableRow = new TableRow(this.f12a);
                        tableLayout.addView(tableRow);
                    }
                    tableRow.addView(this.x[i5]);
                }
                this.u.show();
                break;
            case R.id.soloGameOptionMenuItemBid /*2131165386*/:
                if (this.v == null) {
                    this.v = new a(this);
                }
                this.v.a(this.f12a.m);
                this.v.show();
                break;
            case R.id.soloGameOptionMenuItemLastTurn /*2131165387*/:
                if (this.w == null) {
                    this.w = new bx(this);
                }
                this.w.a(this.f12a.m, (byte) (this.f12a.m.b.C() - 1));
                this.w.show();
                break;
            case R.id.soloGameOptionMenuItemScore /*2131165388*/:
                if (this.e.n != 1) {
                    if (this.e.n != 2) {
                        if (this.e.n != 3) {
                            b((int) R.layout.singlegamescoreboard);
                            break;
                        } else {
                            b((int) R.layout.duplicatescoreboard);
                            m();
                            break;
                        }
                    } else {
                        b((int) R.layout.rubberscoreboard);
                        m();
                        break;
                    }
                } else {
                    b((int) R.layout.rubberscoreboard);
                    m();
                    break;
                }
            case R.id.soloGameOptionMenuItemRedeal /*2131165389*/:
                new AlertDialog.Builder(this).setIcon(17301543).setTitle("Confirm Action").setMessage("This will redeal the hands, are you sure?").setPositiveButton("Yes", new bf(this)).setNegativeButton("No", (DialogInterface.OnClickListener) null).show();
                break;
        }
        return true;
    }

    public void onPause() {
        try {
            if (this.b.I() && this.b.k() == -1) {
                this.e.c();
            }
            this.e.g();
        } catch (Exception e2) {
        }
        super.onPause();
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean z2;
        boolean z3;
        boolean z4;
        boolean z5;
        boolean z6;
        boolean z7;
        boolean z8;
        MenuItem findItem = menu.findItem(R.id.soloGameOptionMenuItemUndo);
        MenuItem findItem2 = menu.findItem(R.id.soloGameOptionMenuItemClaim);
        MenuItem findItem3 = menu.findItem(R.id.soloGameOptionMenuItemBid);
        MenuItem findItem4 = menu.findItem(R.id.soloGameOptionMenuItemLastTurn);
        MenuItem findItem5 = menu.findItem(R.id.soloGameOptionMenuItemScore);
        MenuItem findItem6 = menu.findItem(R.id.soloGameOptionMenuItemRedeal);
        bp bpVar = (bp) this.f12a.m;
        if (!(findItem == null || bpVar == null)) {
            findItem.setEnabled(false);
            if (!bpVar.b.I()) {
                findItem.setEnabled(bpVar.b.i() == bpVar.e && bpVar.b.e() > 2);
            } else if (bpVar.h == 1) {
                byte j2 = bpVar.b.j();
                findItem.setEnabled((j2 == bpVar.e && bpVar.b.B(bpVar.e) && bpVar.b.h() > 3) || (b.i(bpVar.e, j2) && !bpVar.b.B(bpVar.e) && bpVar.b.h() > 1));
            }
        }
        if (!(findItem2 == null || bpVar == null)) {
            byte j3 = bpVar.b.j();
            if (bpVar.h != 1 || !b.i(bpVar.e, j3) || bpVar.b.B(bpVar.e) || bpVar.b.C() < 7) {
                z8 = false;
            } else {
                z8 = true;
            }
            findItem2.setEnabled(z8);
        }
        if (!(findItem3 == null || bpVar == null)) {
            if (bpVar.h == 1) {
                z7 = true;
            } else {
                z7 = false;
            }
            findItem3.setEnabled(z7);
        }
        if (!(findItem4 == null || bpVar == null)) {
            if (bpVar.h != 1 || bpVar.b.C() <= 0) {
                z6 = false;
            } else {
                z6 = true;
            }
            findItem4.setEnabled(z6);
        }
        if (findItem5 != null) {
            z2 = true;
        } else {
            z2 = false;
        }
        if (z2 && (bpVar != null)) {
            if (bpVar.h == 2 || this.e.n == 99) {
                z5 = false;
            } else {
                z5 = true;
            }
            findItem5.setEnabled(z5);
        }
        if (findItem6 != null) {
            z3 = true;
        } else {
            z3 = false;
        }
        if (z3 && (bpVar != null)) {
            if (bpVar.h == 2 || this.e.n != 0) {
                z4 = false;
            } else {
                z4 = true;
            }
            findItem6.setVisible(z4);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    /* access modifiers changed from: package-private */
    public final void f() {
        this.i.a();
        b((byte) 1);
        c();
        a(this.b);
        d();
        h();
    }

    private void m() {
        int a2;
        if (this.e.n == 1) {
            l();
            this.e.o.f();
            int[] iArr = new int[2];
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 < this.e.i.size()) {
                    c cVar = (c) this.e.i.get(i3);
                    if (cVar.c() != 99) {
                        TableLayout tableLayout = (TableLayout) findViewById(R.id.rubberScoreTableLayout);
                        int a3 = b.a(cVar.c(), cVar.d(), cVar.h());
                        int c = b.c(b.l(cVar.i(), cVar.g()), cVar.c(), cVar.d(), cVar.h());
                        int a4 = b.a(b.l(cVar.i(), cVar.g()), cVar.c(), cVar.h());
                        int d = b.d(b.l(cVar.i(), cVar.g()), cVar.c(), cVar.d(), cVar.h());
                        if (c > 0 || a4 > 0 || d < 0) {
                            TableRow tableRow = new TableRow(this);
                            tableRow.setBackgroundColor(-1);
                            TableRow.LayoutParams layoutParams = new TableRow.LayoutParams();
                            layoutParams.setMargins(0, 0, 0, 0);
                            for (int i4 = 0; i4 < 3; i4++) {
                                TextView textView = new TextView(this);
                                textView.setPadding(0, 1, 0, 1);
                                textView.setTextColor(-16777216);
                                textView.setBackgroundColor(-1);
                                textView.setGravity(17);
                                tableRow.addView(textView, layoutParams);
                                if (i4 == 0) {
                                    SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
                                    spannableStringBuilder.append((CharSequence) (cVar.c + ". "));
                                    spannableStringBuilder.append((CharSequence) cVar.a(false));
                                    int indexOf = spannableStringBuilder.toString().indexOf(b.e[1]);
                                    if (indexOf >= 0) {
                                        spannableStringBuilder.setSpan(new ForegroundColorSpan(-65536), indexOf, indexOf + 1, 33);
                                    } else {
                                        int indexOf2 = spannableStringBuilder.toString().indexOf(b.e[2]);
                                        if (indexOf2 >= 0) {
                                            spannableStringBuilder.setSpan(new ForegroundColorSpan(-65536), indexOf2, indexOf2 + 1, 33);
                                        }
                                    }
                                    textView.setText(spannableStringBuilder);
                                } else if (i4 == 1) {
                                    if (b.i(this.f12a.m.e, cVar.g()) && b.b(cVar.c(), cVar.h()) >= 0) {
                                        if (a4 > 0) {
                                            if (textView.getText().length() > 0) {
                                                textView.setText(((Object) textView.getText()) + "\n");
                                            }
                                            textView.setText(((Object) textView.getText()) + Integer.toString(a4));
                                        }
                                        if (c > 0) {
                                            if (textView.getText().length() > 0) {
                                                textView.setText(((Object) textView.getText()) + "\n");
                                            }
                                            textView.setText(((Object) textView.getText()) + Integer.toString(c));
                                        }
                                    } else if (!b.i(this.f12a.m.e, cVar.g()) && b.b(cVar.c(), cVar.h()) < 0) {
                                        textView.setText(Integer.toString(-d));
                                    }
                                } else if (i4 == 2) {
                                    if (!b.i(this.f12a.m.e, cVar.g()) && b.b(cVar.c(), cVar.h()) >= 0) {
                                        if (a4 > 0) {
                                            if (textView.getText().length() > 0) {
                                                textView.setText(((Object) textView.getText()) + "\n");
                                            }
                                            textView.setText(((Object) textView.getText()) + Integer.toString(a4));
                                        }
                                        if (c > 0) {
                                            if (textView.getText().length() > 0) {
                                                textView.setText(((Object) textView.getText()) + "\n");
                                            }
                                            textView.setText(((Object) textView.getText()) + Integer.toString(c));
                                        }
                                    } else if (b.i(this.f12a.m.e, cVar.g()) && b.b(cVar.c(), cVar.h()) < 0) {
                                        textView.setText(Integer.toString(-d));
                                    }
                                }
                            }
                            tableLayout.addView(tableRow, 1);
                        }
                        if (a3 > 0) {
                            TableRow tableRow2 = new TableRow(this);
                            tableRow2.setBackgroundColor(-1);
                            TableRow.LayoutParams layoutParams2 = new TableRow.LayoutParams();
                            layoutParams2.setMargins(0, 0, 0, 0);
                            for (int i5 = 0; i5 < 3; i5++) {
                                TextView textView2 = new TextView(this);
                                textView2.setPadding(0, 1, 0, 1);
                                textView2.setTextColor(-16777216);
                                textView2.setBackgroundColor(-1);
                                textView2.setGravity(17);
                                tableRow2.addView(textView2, layoutParams2);
                                if (i5 == 0) {
                                    SpannableStringBuilder spannableStringBuilder2 = new SpannableStringBuilder();
                                    spannableStringBuilder2.append((CharSequence) (cVar.c + ". "));
                                    spannableStringBuilder2.append((CharSequence) cVar.a(false));
                                    int indexOf3 = spannableStringBuilder2.toString().indexOf(b.e[1]);
                                    if (indexOf3 >= 0) {
                                        spannableStringBuilder2.setSpan(new ForegroundColorSpan(-65536), indexOf3, indexOf3 + 1, 33);
                                    } else {
                                        int indexOf4 = spannableStringBuilder2.toString().indexOf(b.e[2]);
                                        if (indexOf4 >= 0) {
                                            spannableStringBuilder2.setSpan(new ForegroundColorSpan(-65536), indexOf4, indexOf4 + 1, 33);
                                        }
                                    }
                                    textView2.setText(spannableStringBuilder2);
                                } else if (i5 == 1) {
                                    if (b.i(this.f12a.m.e, cVar.g()) && a3 > 0) {
                                        textView2.setText(Integer.toString(a3));
                                    }
                                } else if (i5 == 2 && !b.i(this.f12a.m.e, cVar.g()) && a3 > 0) {
                                    textView2.setText(Integer.toString(a3));
                                }
                            }
                            tableLayout.addView(tableRow2);
                        }
                    }
                    if (((c) this.e.i.get(i3)).f != 99 && (a2 = b.a(cVar.c(), cVar.d(), cVar.h())) > 0) {
                        int g2 = cVar.g() % 2;
                        iArr[g2] = a2 + iArr[g2];
                    }
                    if (iArr[0] >= 100 || iArr[1] >= 100) {
                        View view = new View(this);
                        view.setBackgroundColor(-3355444);
                        ((TableLayout) findViewById(R.id.rubberScoreTableLayout)).addView(view, new ViewGroup.LayoutParams(-1, 2));
                        iArr[1] = 0;
                        iArr[0] = 0;
                    }
                    i2 = i3 + 1;
                } else {
                    j();
                    return;
                }
            }
        } else if (this.e.n == 2) {
            l();
            this.e.o.e();
            int i6 = 0;
            while (true) {
                int i7 = i6;
                if (i7 < this.e.i.size()) {
                    b((c) this.e.i.get(i7));
                    i6 = i7 + 1;
                } else {
                    i();
                    return;
                }
            }
        } else if (this.e.n == 3) {
            TableLayout tableLayout2 = (TableLayout) findViewById(R.id.duplicateScoreTableLayout);
            while (tableLayout2.getChildCount() > 2) {
                tableLayout2.removeViewAt(1);
            }
            this.r = 0;
            this.s = 0;
            for (int i8 = 0; i8 < this.e.o.i; i8++) {
                a(i8 + 1);
            }
        } else {
            int i9 = 0;
            while (true) {
                int i10 = i9;
                if (i10 < this.e.i.size()) {
                    d((c) this.e.i.get(i10));
                    i9 = i10 + 1;
                } else {
                    return;
                }
            }
        }
    }

    private void n() {
        TextView textView = (TextView) findViewById(R.id.biddingBoardInfo);
        if (textView != null) {
            textView.setText("Dealer: " + b.g[this.b.h.a()] + "    " + "Vul: " + a.a.g[this.b.h.d()]);
        }
        TextView textView2 = (TextView) findViewById(R.id.playingBoardInfo);
        if (textView2 != null) {
            textView2.setText("Dealer: " + b.g[this.b.h.a()] + "    " + "Vul: " + a.a.g[this.b.h.d()]);
        }
    }

    private void a(byte b) {
        if (b > 0 && b <= 7 && b >= this.g) {
            this.n[this.h - 1].setTextColor(this.j.getTextColors());
            this.n[b - 1].setTextColor(-65536);
            this.h = b;
        }
    }

    private void a(Button button, Button button2) {
        if (BridgeApp.p) {
            button2.getLayoutParams().width = -1;
            button.setVisibility(8);
            return;
        }
        button2.getLayoutParams().width = 120;
        button.getLayoutParams().width = 120;
        button.setVisibility(0);
        button.setOnClickListener(this);
    }

    private void b(byte b) {
        if (b > 0 && b <= 7) {
            this.g = b;
            a(b);
        }
    }

    private void a(b bVar) {
        b(bVar.z());
    }

    private void b(int i2) {
        boolean z2;
        this.f.removeAllViews();
        if (this.t.containsKey(Integer.valueOf(i2))) {
            this.f.addView((View) this.t.get(Integer.valueOf(i2)));
            if (findViewById(R.id.singleGameScoreBoardSaveButton) != null) {
                findViewById(R.id.singleGameScoreBoardSaveButton).setEnabled(this.e.b.K());
            }
            if (findViewById(R.id.rubberScoreBoardSaveButton) != null) {
                findViewById(R.id.rubberScoreBoardSaveButton).setEnabled(this.e.b.K());
                z2 = false;
            } else {
                z2 = false;
            }
        } else {
            this.f.addView(((LayoutInflater) getSystemService("layout_inflater")).inflate(i2, (ViewGroup) null));
            this.t.put(Integer.valueOf(i2), this.f.getChildAt(0));
            z2 = true;
        }
        if (!z2) {
            return;
        }
        if (i2 == R.layout.bidding) {
            if (this.i == null) {
                this.i = new i(this, (TableRow) findViewById(R.id.biddingCellsHeader), (ScrollView) findViewById(R.id.biddingCellScrollView));
            }
            this.i.a(this.e);
            this.i.b();
            RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.biddingcardslayout);
            findViewById(R.id.biddingBoardInfo);
            n();
            int width = getWindowManager().getDefaultDisplay().getWidth();
            int i3 = (width - a.q) / 12;
            int i4 = (((width - (i3 * 12)) - a.q) / 2) - 1;
            for (int i5 = 0; i5 < 13; i5++) {
                ImageView imageView = new ImageView(this);
                this.A[i5] = imageView;
                if (this.b.l[this.f12a.m.e][i5] != -1) {
                    imageView.setImageResource(a.k[this.b.l[this.f12a.m.e][i5]]);
                } else {
                    imageView.setImageBitmap(null);
                }
                imageView.setLayoutParams(new WindowManager.LayoutParams(-2, -2));
                imageView.setPadding((i5 * i3) + i4, 0, 0, 0);
                relativeLayout.addView(imageView, new RelativeLayout.LayoutParams(-2, -2));
            }
            k();
            f();
        } else if (i2 == R.layout.singlegamescoreboard) {
            Button button = (Button) findViewById(R.id.singleGameScoreBoardCloseButton);
            button.setOnClickListener(this);
            Button button2 = (Button) findViewById(R.id.singleGameScoreBoardSaveButton);
            a(button2, button);
            if (this.e.h != 2) {
                button2.setEnabled(false);
            }
            if (this.e.n == 99) {
                button2.setEnabled(false);
            }
        } else if (i2 == R.layout.rubberscoreboard) {
            Button button3 = (Button) findViewById(R.id.rubberScoreBoardButtonClose);
            button3.setOnClickListener(this);
            Button button4 = (Button) findViewById(R.id.rubberScoreBoardSaveButton);
            a(button4, button3);
            if (this.e.h != 2) {
                button4.setEnabled(false);
            }
        } else if (i2 == R.layout.duplicatescoreboard) {
            ((Button) findViewById(R.id.duplicateScoreBoardButtonClose)).setOnClickListener(this);
        }
    }

    /* access modifiers changed from: package-private */
    public final void g() {
        b((int) R.layout.playing);
        this.c = (GamePlayingView) findViewById(R.id.gamePlayingView);
        this.c.requestFocus();
        this.c.a((BridgeGameActivity) this);
        n();
        b();
    }

    /* access modifiers changed from: package-private */
    public final void h() {
        this.i.d();
        a(this.b);
    }
}
