package net.weweweb.android.bridge;

import a.b;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import java.util.LinkedList;
import net.weweweb.android.free.bridge.R;

/* compiled from: ProGuard */
public final class at extends Dialog {

    /* renamed from: a  reason: collision with root package name */
    Context f47a = null;
    b b = null;
    ScrollView c = null;
    TableLayout d = null;
    LinkedList e = new LinkedList();

    at(Context context) {
        super(context);
        setContentView((int) R.layout.playlogdialog);
        setTitle((int) R.string.play_log);
        ((Button) findViewById(R.id.playLogDialogButtonOK)).setOnClickListener(new au(this));
        this.c = (ScrollView) findViewById(R.id.playLogDialogCellScrollView);
        this.d = new TableLayout(getContext());
        this.d.setLayoutParams(new TableLayout.LayoutParams((ViewGroup.MarginLayoutParams) new TableLayout.LayoutParams(-1, -2)));
        this.d.setBackgroundColor(-16777216);
        this.d.removeAllViews();
        this.c.addView(this.d);
        a(4);
        this.d.setStretchAllColumns(true);
    }

    private static void a(TableRow tableRow) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < 5) {
                ((TextView) tableRow.getChildAt(i2)).setText("");
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    private TextView a(byte b2, byte b3) {
        if (b2 >= this.e.size()) {
            return null;
        }
        return (TextView) ((TableRow) this.e.get(b2)).getChildAt(b3);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.b.a(int, int):byte
     arg types: [int, byte]
     candidates:
      a.b.a(int, byte):byte
      a.b.a(byte[], byte):byte
      a.b.a(byte[][], int):byte
      a.b.a(byte, byte):java.lang.String
      a.b.a(byte[], boolean):void
      a.b.a(byte, int):byte
      a.b.a(byte, char):int
      a.b.a(byte, boolean):boolean
      a.b.a(int, int):byte */
    /* access modifiers changed from: package-private */
    public final void a(b bVar) {
        String str;
        String str2;
        this.b = bVar;
        a(4);
        for (int i = 0; i < this.e.size(); i++) {
            a((TableRow) this.e.get(i));
        }
        if (this.b != null && this.b.h() != -1) {
            byte C = this.b.C();
            for (byte b2 = 0; b2 <= C; b2 = (byte) (b2 + 1)) {
                for (byte b3 = 1; b3 < 5; b3 = (byte) (b3 + 1)) {
                    if (b2 >= 0 && b2 < 13 && b3 >= 0 && b3 < 5 && this.b.a(b3 - 1, (int) b2) != -1) {
                        if (this.d.getChildCount() <= b2) {
                            a(b2 + 1);
                        }
                        TextView a2 = a(b2, b3);
                        if (a2 != null) {
                            if (this.b.t(b2) == b3 - 1) {
                                str = ">";
                            } else {
                                str = "";
                            }
                            if (!this.b.K(b2) || this.b.u(b2) != b3 - 1) {
                                str2 = "";
                            } else {
                                str2 = "*";
                            }
                            a2.setText(BridgeApp.a(str + b.f(this.b.a(b3 - 1, (int) b2)) + str2));
                            a(b2, (byte) 0).setText(Integer.toString(b2 + 1) + ".");
                        }
                    }
                }
            }
            this.c.fullScroll(130);
        }
    }

    private void a(int i) {
        int childCount = this.d.getChildCount();
        if (childCount > i) {
            for (int i2 = childCount - 1; i2 >= i; i2--) {
                TableRow tableRow = (TableRow) this.e.get(i2);
                this.d.removeView(tableRow);
                a(tableRow);
            }
        } else if (childCount < i) {
            for (int i3 = childCount; i3 < i; i3++) {
                if (this.e.size() <= i3) {
                    TableRow tableRow2 = new TableRow(getContext());
                    TableRow.LayoutParams layoutParams = new TableRow.LayoutParams();
                    layoutParams.setMargins(0, 0, 0, 1);
                    for (int i4 = 0; i4 < 5; i4++) {
                        TextView textView = new TextView(getContext());
                        textView.setText("");
                        textView.setPadding(0, 1, 0, 1);
                        textView.setBackgroundColor(-1);
                        textView.setTextColor(-16777216);
                        textView.setGravity(17);
                        tableRow2.addView(textView, layoutParams);
                    }
                    this.e.add(tableRow2);
                }
                this.d.addView((View) this.e.get(i3));
            }
        }
    }
}
