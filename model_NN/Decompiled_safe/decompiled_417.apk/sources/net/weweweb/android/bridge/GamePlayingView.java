package net.weweweb.android.bridge;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.View;
import net.weweweb.android.common.a;

/* compiled from: ProGuard */
public class GamePlayingView extends View {

    /* renamed from: a  reason: collision with root package name */
    BridgeApp f13a;
    BridgeGameActivity b;
    a c;
    byte d = 0;
    public byte[] e = {-1};
    Point[] f = {new Point(0, 0), new Point(0, 0), new Point(0, 0), new Point(0, 0)};
    private Paint g;
    private int h;
    private Path i;
    private int[] j = {14, 14, 14, 14};
    private Paint k = new Paint();

    public GamePlayingView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        a(context);
    }

    public GamePlayingView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        this.b = null;
        super.finalize();
    }

    private void a(Context context) {
        this.f13a = (BridgeApp) context.getApplicationContext();
        this.b = (BridgeGameActivity) context;
        this.g = new Paint(1);
        this.g.setARGB(255, 0, 0, 0);
        if (BridgeApp.H > 130 && BridgeApp.H > 200) {
            this.g.setTextSize(this.g.getTextSize() * 1.5f);
        }
        Paint paint = this.g;
        this.h = (int) (paint.descent() - paint.ascent());
        this.i = new Path();
        this.k.setStyle(Paint.Style.FILL);
        this.k.setColor(-65536);
        this.k.setAntiAlias(true);
        setFocusable(true);
        setClickable(true);
    }

    private static int a(int i2) {
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        if (mode == 0) {
            return 200;
        }
        return size;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.b.a(byte, char):int
     arg types: [byte, int]
     candidates:
      a.b.a(int, byte):byte
      a.b.a(byte[], byte):byte
      a.b.a(byte[][], int):byte
      a.b.a(byte, byte):java.lang.String
      a.b.a(byte[], boolean):void
      a.b.a(byte, int):byte
      a.b.a(int, int):byte
      a.b.a(byte, boolean):boolean
      a.b.a(byte, char):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.b.a(int, int):byte
     arg types: [byte, byte]
     candidates:
      a.b.a(int, byte):byte
      a.b.a(byte[], byte):byte
      a.b.a(byte[][], int):byte
      a.b.a(byte, byte):java.lang.String
      a.b.a(byte[], boolean):void
      a.b.a(byte, int):byte
      a.b.a(byte, char):int
      a.b.a(byte, boolean):boolean
      a.b.a(int, int):byte */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:110:0x0762  */
    /* JADX WARNING: Removed duplicated region for block: B:140:0x08a2  */
    /* JADX WARNING: Removed duplicated region for block: B:141:0x0914  */
    /* JADX WARNING: Removed duplicated region for block: B:142:0x0986  */
    /* JADX WARNING: Removed duplicated region for block: B:143:0x09f8  */
    /* JADX WARNING: Removed duplicated region for block: B:155:0x0ac0  */
    /* JADX WARNING: Removed duplicated region for block: B:177:0x0b10  */
    /* JADX WARNING: Removed duplicated region for block: B:199:0x0b60  */
    /* JADX WARNING: Removed duplicated region for block: B:219:0x0ba8  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x02ec  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0336  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x03f0  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x043a  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x04f6  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x0540  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x05ba  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x05fb  */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x0674  */
    /* JADX WARNING: Removed duplicated region for block: B:96:0x06ed  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onDraw(android.graphics.Canvas r25) {
        /*
            r24 = this;
            r0 = r24
            net.weweweb.android.bridge.BridgeApp r0 = r0.f13a
            r5 = r0
            net.weweweb.android.bridge.ad r13 = r5.m
            int r5 = r24.getWidth()
            int r6 = r24.getHeight()
            if (r13 != 0) goto L_0x0012
        L_0x0011:
            return
        L_0x0012:
            r7 = 0
            r8 = 128(0x80, float:1.794E-43)
            r9 = 0
            r0 = r25
            r1 = r7
            r2 = r8
            r3 = r9
            r0.drawRGB(r1, r2, r3)
            a.b r7 = r13.b
            byte[] r8 = r13.l
            r9 = 0
            byte r8 = r8[r9]
            r9 = 42
            int r7 = r7.a(r8, r9)
            r8 = 1
            int r7 = r7 - r8
            a.b r8 = r13.b
            byte[] r9 = r13.l
            r10 = 0
            byte r9 = r9[r10]
            boolean r8 = r8.C(r9)
            if (r8 != 0) goto L_0x0047
            a.b r8 = r13.b
            byte[] r9 = r13.l
            r10 = 0
            byte r9 = r9[r10]
            boolean r8 = r8.A(r9)
            if (r8 == 0) goto L_0x07c8
        L_0x0047:
            r8 = 0
            r9 = r8
            r8 = r5
        L_0x004a:
            if (r7 > 0) goto L_0x07da
            r0 = r24
            int[] r0 = r0.j
            r10 = r0
            r11 = 0
            r12 = 0
            r10[r11] = r12
        L_0x0055:
            r0 = r24
            android.graphics.Point[] r0 = r0.f
            r10 = r0
            r11 = 0
            r10 = r10[r11]
            int r8 = r8 - r9
            r0 = r24
            int[] r0 = r0.j
            r11 = r0
            r12 = 0
            r11 = r11[r12]
            int r7 = r7 * r11
            int r7 = r8 - r7
            int r8 = net.weweweb.android.common.a.q
            int r7 = r7 - r8
            int r7 = r7 / 2
            int r7 = r7 + r9
            r10.x = r7
            a.b r7 = r13.b
            byte[] r8 = r13.l
            r9 = 2
            byte r8 = r8[r9]
            r9 = 42
            int r7 = r7.a(r8, r9)
            r8 = 1
            int r7 = r7 - r8
            if (r7 > 0) goto L_0x0800
            r0 = r24
            int[] r0 = r0.j
            r8 = r0
            r9 = 2
            r10 = 0
            r8[r9] = r10
        L_0x008b:
            r0 = r24
            android.graphics.Point[] r0 = r0.f
            r8 = r0
            r9 = 2
            r8 = r8[r9]
            r9 = 0
            int r9 = r5 - r9
            r0 = r24
            int[] r0 = r0.j
            r10 = r0
            r11 = 2
            r10 = r10[r11]
            int r7 = r7 * r10
            int r7 = r9 - r7
            int r9 = net.weweweb.android.common.a.q
            int r7 = r7 - r9
            int r7 = r7 / 2
            int r7 = r7 + 0
            r8.x = r7
            r0 = r24
            android.graphics.Point[] r0 = r0.f
            r7 = r0
            r8 = 2
            r7 = r7[r8]
            int r8 = net.weweweb.android.common.a.r
            int r8 = r6 - r8
            r7.y = r8
            a.b r7 = r13.b
            byte[] r8 = r13.l
            r9 = 3
            byte r8 = r8[r9]
            r9 = 42
            int r7 = r7.a(r8, r9)
            r8 = 1
            int r7 = r7 - r8
            a.b r8 = r13.b
            byte[] r9 = r13.l
            r10 = 3
            byte r9 = r9[r10]
            boolean r8 = r8.C(r9)
            if (r8 == 0) goto L_0x0827
            r8 = 0
            int r9 = net.weweweb.android.common.a.r
            int r9 = r6 - r9
            r10 = 10
            int r9 = r9 - r10
            r23 = r9
            r9 = r8
            r8 = r23
        L_0x00e1:
            if (r7 > 0) goto L_0x0839
            r0 = r24
            int[] r0 = r0.j
            r10 = r0
            r11 = 3
            r12 = 0
            r10[r11] = r12
        L_0x00ec:
            r0 = r24
            android.graphics.Point[] r0 = r0.f
            r10 = r0
            r11 = 3
            r10 = r10[r11]
            r11 = 0
            r10.x = r11
            r0 = r24
            android.graphics.Point[] r0 = r0.f
            r10 = r0
            r11 = 3
            r10 = r10[r11]
            int r8 = r8 - r9
            r0 = r24
            int[] r0 = r0.j
            r11 = r0
            r12 = 3
            r11 = r11[r12]
            int r7 = r7 * r11
            int r7 = r8 - r7
            int r8 = net.weweweb.android.common.a.q
            int r7 = r7 - r8
            int r7 = r7 / 2
            int r7 = r7 + r9
            r10.y = r7
            a.b r7 = r13.b
            byte[] r8 = r13.l
            r9 = 1
            byte r8 = r8[r9]
            r9 = 42
            int r7 = r7.a(r8, r9)
            r8 = 1
            int r7 = r7 - r8
            a.b r8 = r13.b
            byte[] r9 = r13.l
            r10 = 1
            byte r9 = r9[r10]
            boolean r8 = r8.C(r9)
            if (r8 == 0) goto L_0x085f
            r8 = 0
            int r9 = net.weweweb.android.common.a.r
            int r9 = r6 - r9
            r10 = 10
            int r9 = r9 - r10
            r23 = r9
            r9 = r8
            r8 = r23
        L_0x013c:
            if (r7 > 0) goto L_0x0871
            r0 = r24
            int[] r0 = r0.j
            r10 = r0
            r11 = 1
            r12 = 0
            r10[r11] = r12
        L_0x0147:
            r0 = r24
            android.graphics.Point[] r0 = r0.f
            r10 = r0
            r11 = 1
            r10 = r10[r11]
            int r11 = net.weweweb.android.common.a.r
            int r11 = r5 - r11
            r10.x = r11
            r0 = r24
            android.graphics.Point[] r0 = r0.f
            r10 = r0
            r11 = 1
            r10 = r10[r11]
            int r9 = r8 - r9
            r0 = r24
            int[] r0 = r0.j
            r11 = r0
            r12 = 1
            r11 = r11[r12]
            int r7 = r7 * r11
            int r7 = r9 - r7
            int r9 = net.weweweb.android.common.a.q
            int r7 = r7 - r9
            int r7 = r7 / 2
            int r7 = r8 - r7
            int r8 = net.weweweb.android.common.a.q
            int r7 = r7 - r8
            r10.y = r7
            a.b r7 = r13.b
            byte r7 = r7.C()
            int r11 = r5 / 2
            int r12 = r6 / 2
            r0 = r24
            net.weweweb.android.bridge.BridgeApp r0 = r0.f13a
            r5 = r0
            net.weweweb.android.bridge.ad r5 = r5.m
            boolean r5 = r5.j
            if (r5 == 0) goto L_0x0bf0
            r5 = 1
            int r5 = r7 - r5
            byte r5 = (byte) r5
            r14 = r5
        L_0x0190:
            r0 = r24
            net.weweweb.android.bridge.BridgeApp r0 = r0.f13a
            r5 = r0
            net.weweweb.android.bridge.ad r5 = r5.m
            a.b r15 = r5.b
            r0 = r24
            net.weweweb.android.bridge.BridgeApp r0 = r0.f13a
            r5 = r0
            net.weweweb.android.bridge.ad r5 = r5.m
            r0 = r5
            byte[] r0 = r0.l
            r16 = r0
            r0 = r24
            net.weweweb.android.bridge.BridgeApp r0 = r0.f13a
            r5 = r0
            net.weweweb.android.bridge.ad r5 = r5.m
            r0 = r5
            byte[] r0 = r0.k
            r17 = r0
            r5 = 0
            float r6 = net.weweweb.android.bridge.BridgeApp.I
            float r5 = r5 * r6
            r0 = r5
            int r0 = (int) r0
            r18 = r0
            r5 = -1065353216(0xffffffffc0800000, float:-4.0)
            float r6 = net.weweweb.android.bridge.BridgeApp.I
            float r5 = r5 * r6
            r0 = r5
            int r0 = (int) r0
            r19 = r0
            r0 = r24
            int r0 = r0.h
            r20 = r0
            r0 = r24
            int r0 = r0.h
            r21 = r0
            java.lang.String r22 = ""
            android.graphics.Paint r10 = new android.graphics.Paint
            r10.<init>()
            r5 = 255(0xff, float:3.57E-43)
            r6 = 255(0xff, float:3.57E-43)
            r7 = 200(0xc8, float:2.8E-43)
            r8 = 0
            r10.setARGB(r5, r6, r7, r8)
            android.graphics.Paint$Style r5 = android.graphics.Paint.Style.FILL
            r10.setStyle(r5)
            int r5 = net.weweweb.android.common.a.q
            int r5 = r11 - r5
            int r5 = r5 - r19
            int r5 = r5 - r21
            float r6 = (float) r5
            int r5 = net.weweweb.android.common.a.r
            int r5 = r12 - r5
            int r5 = r5 - r18
            int r5 = r5 - r20
            float r7 = (float) r5
            int r5 = net.weweweb.android.common.a.q
            int r5 = r5 + r11
            int r5 = r5 + r19
            int r5 = r5 + r21
            float r8 = (float) r5
            int r5 = net.weweweb.android.common.a.r
            int r5 = r5 + r12
            int r5 = r5 + r18
            int r5 = r5 + r20
            float r9 = (float) r5
            r5 = r25
            r5.drawRect(r6, r7, r8, r9, r10)
            java.lang.String[] r5 = a.b.g
            r6 = 0
            byte r6 = r16[r6]
            r5 = r5[r6]
            r6 = 0
            r7 = 1
            java.lang.String r5 = r5.substring(r6, r7)
            r6 = 0
            byte r6 = r16[r6]
            boolean r6 = r15.C(r6)
            if (r6 == 0) goto L_0x0235
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.StringBuilder r5 = r6.append(r5)
            r0 = r5
            r1 = r22
            java.lang.StringBuilder r5 = r0.append(r1)
            java.lang.String r5 = r5.toString()
        L_0x0235:
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.StringBuilder r5 = r6.append(r5)
            java.lang.String r6 = " ("
            java.lang.StringBuilder r5 = r5.append(r6)
            r0 = r24
            net.weweweb.android.bridge.BridgeApp r0 = r0.f13a
            r6 = r0
            net.weweweb.android.bridge.ad r6 = r6.m
            r7 = 0
            byte r7 = r16[r7]
            java.lang.String r6 = r6.a(r7)
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r6 = ")"
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r6 = r5.toString()
            r0 = r24
            net.weweweb.android.bridge.BridgeApp r0 = r0.f13a
            r5 = r0
            net.weweweb.android.bridge.ad r5 = r5.m
            boolean r5 = r5 instanceof net.weweweb.android.bridge.am
            if (r5 == 0) goto L_0x0bed
            r0 = r24
            net.weweweb.android.bridge.BridgeApp r0 = r0.f13a
            r5 = r0
            net.weweweb.android.bridge.ad r5 = r5.m
            net.weweweb.android.bridge.am r5 = (net.weweweb.android.bridge.am) r5
            net.weweweb.android.bridge.ac r7 = r5.o
            byte[] r7 = r7.i
            r8 = 0
            byte r8 = r16[r8]
            byte r7 = r7[r8]
            byte r8 = net.weweweb.android.bridge.ac.m
            if (r7 != r8) goto L_0x0bed
            java.lang.String r7 = ")"
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r9 = "<-"
            java.lang.StringBuilder r8 = r8.append(r9)
            net.weweweb.android.bridge.ac r5 = r5.o
            java.lang.String[] r5 = r5.j
            r9 = 0
            byte r9 = r16[r9]
            r5 = r5[r9]
            java.lang.StringBuilder r5 = r8.append(r5)
            java.lang.String r8 = ")"
            java.lang.StringBuilder r5 = r5.append(r8)
            java.lang.String r5 = r5.toString()
            java.lang.String r5 = r6.replace(r7, r5)
        L_0x02a9:
            float r6 = (float) r11
            r0 = r24
            android.graphics.Paint r0 = r0.g
            r7 = r0
            float r7 = r7.measureText(r5)
            r8 = 1073741824(0x40000000, float:2.0)
            float r7 = r7 / r8
            float r6 = r6 - r7
            int r7 = net.weweweb.android.common.a.r
            int r7 = r7 + r18
            int r7 = r7 + r20
            r0 = r24
            int r0 = r0.h
            r8 = r0
            int r7 = r7 - r8
            int r7 = r12 - r7
            r8 = 2
            int r7 = r7 - r8
            float r7 = (float) r7
            r0 = r24
            android.graphics.Paint r0 = r0.g
            r8 = r0
            r0 = r25
            r1 = r5
            r2 = r6
            r3 = r7
            r4 = r8
            r0.drawText(r1, r2, r3, r4)
            java.lang.String[] r5 = a.b.g
            r6 = 1
            byte r6 = r16[r6]
            r5 = r5[r6]
            r6 = 0
            r7 = 1
            java.lang.String r5 = r5.substring(r6, r7)
            r6 = 1
            byte r6 = r16[r6]
            boolean r6 = r15.C(r6)
            if (r6 == 0) goto L_0x0300
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.StringBuilder r5 = r6.append(r5)
            r0 = r5
            r1 = r22
            java.lang.StringBuilder r5 = r0.append(r1)
            java.lang.String r5 = r5.toString()
        L_0x0300:
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.StringBuilder r5 = r6.append(r5)
            java.lang.String r6 = " ("
            java.lang.StringBuilder r5 = r5.append(r6)
            r0 = r24
            net.weweweb.android.bridge.BridgeApp r0 = r0.f13a
            r6 = r0
            net.weweweb.android.bridge.ad r6 = r6.m
            r7 = 1
            byte r7 = r16[r7]
            java.lang.String r6 = r6.a(r7)
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r6 = ")"
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r6 = r5.toString()
            r0 = r24
            net.weweweb.android.bridge.BridgeApp r0 = r0.f13a
            r5 = r0
            net.weweweb.android.bridge.ad r5 = r5.m
            boolean r5 = r5 instanceof net.weweweb.android.bridge.am
            if (r5 == 0) goto L_0x0375
            r0 = r24
            net.weweweb.android.bridge.BridgeApp r0 = r0.f13a
            r5 = r0
            net.weweweb.android.bridge.ad r5 = r5.m
            net.weweweb.android.bridge.am r5 = (net.weweweb.android.bridge.am) r5
            net.weweweb.android.bridge.ac r7 = r5.o
            byte[] r7 = r7.i
            r8 = 1
            byte r8 = r16[r8]
            byte r7 = r7[r8]
            byte r8 = net.weweweb.android.bridge.ac.m
            if (r7 != r8) goto L_0x0375
            java.lang.String r7 = ")"
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r9 = "<-"
            java.lang.StringBuilder r8 = r8.append(r9)
            net.weweweb.android.bridge.ac r5 = r5.o
            java.lang.String[] r5 = r5.j
            r9 = 1
            byte r9 = r16[r9]
            r5 = r5[r9]
            java.lang.StringBuilder r5 = r8.append(r5)
            java.lang.String r8 = ")"
            java.lang.StringBuilder r5 = r5.append(r8)
            java.lang.String r5 = r5.toString()
            java.lang.String r5 = r6.replace(r7, r5)
            r6 = r5
        L_0x0375:
            r0 = r24
            android.graphics.Path r0 = r0.i
            r5 = r0
            r5.reset()
            r0 = r24
            android.graphics.Path r0 = r0.i
            r5 = r0
            int r7 = net.weweweb.android.common.a.q
            int r7 = r7 + r19
            int r7 = r7 + r21
            int r7 = r7 + r11
            r0 = r24
            int r0 = r0.h
            r8 = r0
            int r7 = r7 - r8
            int r7 = r7 + 2
            float r7 = (float) r7
            float r8 = (float) r12
            r0 = r24
            android.graphics.Paint r0 = r0.g
            r9 = r0
            float r9 = r9.measureText(r6)
            r10 = 1073741824(0x40000000, float:2.0)
            float r9 = r9 / r10
            float r8 = r8 - r9
            r5.moveTo(r7, r8)
            r0 = r24
            android.graphics.Path r0 = r0.i
            r5 = r0
            int r7 = net.weweweb.android.common.a.q
            int r7 = r7 + r19
            int r7 = r7 + r21
            int r7 = r7 + r11
            r0 = r24
            int r0 = r0.h
            r8 = r0
            int r7 = r7 - r8
            int r7 = r7 + 2
            float r7 = (float) r7
            float r8 = (float) r12
            r0 = r24
            android.graphics.Paint r0 = r0.g
            r9 = r0
            float r9 = r9.measureText(r6)
            r10 = 1073741824(0x40000000, float:2.0)
            float r9 = r9 / r10
            float r8 = r8 + r9
            r5.lineTo(r7, r8)
            r0 = r24
            android.graphics.Path r0 = r0.i
            r7 = r0
            r8 = 0
            r9 = 0
            r0 = r24
            android.graphics.Paint r0 = r0.g
            r10 = r0
            r5 = r25
            r5.drawTextOnPath(r6, r7, r8, r9, r10)
            java.lang.String[] r5 = a.b.g
            r6 = 3
            byte r6 = r16[r6]
            r5 = r5[r6]
            r6 = 0
            r7 = 1
            java.lang.String r5 = r5.substring(r6, r7)
            r6 = 3
            byte r6 = r16[r6]
            boolean r6 = r15.C(r6)
            if (r6 == 0) goto L_0x0404
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.StringBuilder r5 = r6.append(r5)
            r0 = r5
            r1 = r22
            java.lang.StringBuilder r5 = r0.append(r1)
            java.lang.String r5 = r5.toString()
        L_0x0404:
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.StringBuilder r5 = r6.append(r5)
            java.lang.String r6 = " ("
            java.lang.StringBuilder r5 = r5.append(r6)
            r0 = r24
            net.weweweb.android.bridge.BridgeApp r0 = r0.f13a
            r6 = r0
            net.weweweb.android.bridge.ad r6 = r6.m
            r7 = 3
            byte r7 = r16[r7]
            java.lang.String r6 = r6.a(r7)
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r6 = ")"
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r6 = r5.toString()
            r0 = r24
            net.weweweb.android.bridge.BridgeApp r0 = r0.f13a
            r5 = r0
            net.weweweb.android.bridge.ad r5 = r5.m
            boolean r5 = r5 instanceof net.weweweb.android.bridge.am
            if (r5 == 0) goto L_0x0479
            r0 = r24
            net.weweweb.android.bridge.BridgeApp r0 = r0.f13a
            r5 = r0
            net.weweweb.android.bridge.ad r5 = r5.m
            net.weweweb.android.bridge.am r5 = (net.weweweb.android.bridge.am) r5
            net.weweweb.android.bridge.ac r7 = r5.o
            byte[] r7 = r7.i
            r8 = 3
            byte r8 = r16[r8]
            byte r7 = r7[r8]
            byte r8 = net.weweweb.android.bridge.ac.m
            if (r7 != r8) goto L_0x0479
            java.lang.String r7 = ")"
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r9 = "<-"
            java.lang.StringBuilder r8 = r8.append(r9)
            net.weweweb.android.bridge.ac r5 = r5.o
            java.lang.String[] r5 = r5.j
            r9 = 3
            byte r9 = r16[r9]
            r5 = r5[r9]
            java.lang.StringBuilder r5 = r8.append(r5)
            java.lang.String r8 = ")"
            java.lang.StringBuilder r5 = r5.append(r8)
            java.lang.String r5 = r5.toString()
            java.lang.String r5 = r6.replace(r7, r5)
            r6 = r5
        L_0x0479:
            r0 = r24
            android.graphics.Path r0 = r0.i
            r5 = r0
            r5.reset()
            r0 = r24
            android.graphics.Path r0 = r0.i
            r5 = r0
            int r7 = net.weweweb.android.common.a.q
            int r7 = r7 + r19
            int r7 = r7 + r21
            int r7 = r11 - r7
            r0 = r24
            int r0 = r0.h
            r8 = r0
            int r7 = r7 + r8
            r8 = 2
            int r7 = r7 - r8
            float r7 = (float) r7
            float r8 = (float) r12
            r0 = r24
            android.graphics.Paint r0 = r0.g
            r9 = r0
            float r9 = r9.measureText(r6)
            r10 = 1073741824(0x40000000, float:2.0)
            float r9 = r9 / r10
            float r8 = r8 + r9
            r5.moveTo(r7, r8)
            r0 = r24
            android.graphics.Path r0 = r0.i
            r5 = r0
            int r7 = net.weweweb.android.common.a.q
            int r7 = r7 + r19
            int r7 = r7 + r21
            int r7 = r11 - r7
            r0 = r24
            int r0 = r0.h
            r8 = r0
            int r7 = r7 + r8
            r8 = 2
            int r7 = r7 - r8
            float r7 = (float) r7
            float r8 = (float) r12
            r0 = r24
            android.graphics.Paint r0 = r0.g
            r9 = r0
            float r9 = r9.measureText(r6)
            r10 = 1073741824(0x40000000, float:2.0)
            float r9 = r9 / r10
            float r8 = r8 - r9
            r5.lineTo(r7, r8)
            r0 = r24
            android.graphics.Path r0 = r0.i
            r7 = r0
            r8 = 0
            r9 = 0
            r0 = r24
            android.graphics.Paint r0 = r0.g
            r10 = r0
            r5 = r25
            r5.drawTextOnPath(r6, r7, r8, r9, r10)
            java.lang.String[] r5 = a.b.g
            r6 = 2
            byte r6 = r16[r6]
            r5 = r5[r6]
            r6 = 0
            r7 = 1
            java.lang.String r5 = r5.substring(r6, r7)
            r6 = 2
            byte r6 = r16[r6]
            boolean r6 = r15.C(r6)
            if (r6 == 0) goto L_0x050a
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.StringBuilder r5 = r6.append(r5)
            r0 = r5
            r1 = r22
            java.lang.StringBuilder r5 = r0.append(r1)
            java.lang.String r5 = r5.toString()
        L_0x050a:
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.StringBuilder r5 = r6.append(r5)
            java.lang.String r6 = " ("
            java.lang.StringBuilder r5 = r5.append(r6)
            r0 = r24
            net.weweweb.android.bridge.BridgeApp r0 = r0.f13a
            r6 = r0
            net.weweweb.android.bridge.ad r6 = r6.m
            r7 = 2
            byte r7 = r16[r7]
            java.lang.String r6 = r6.a(r7)
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r6 = ")"
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r6 = r5.toString()
            r0 = r24
            net.weweweb.android.bridge.BridgeApp r0 = r0.f13a
            r5 = r0
            net.weweweb.android.bridge.ad r5 = r5.m
            boolean r5 = r5 instanceof net.weweweb.android.bridge.am
            if (r5 == 0) goto L_0x0bea
            r0 = r24
            net.weweweb.android.bridge.BridgeApp r0 = r0.f13a
            r5 = r0
            net.weweweb.android.bridge.ad r5 = r5.m
            net.weweweb.android.bridge.am r5 = (net.weweweb.android.bridge.am) r5
            net.weweweb.android.bridge.ac r7 = r5.o
            byte[] r7 = r7.i
            r8 = 2
            byte r8 = r16[r8]
            byte r7 = r7[r8]
            byte r8 = net.weweweb.android.bridge.ac.m
            if (r7 != r8) goto L_0x0bea
            java.lang.String r7 = ")"
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r9 = "<-"
            java.lang.StringBuilder r8 = r8.append(r9)
            net.weweweb.android.bridge.ac r5 = r5.o
            java.lang.String[] r5 = r5.j
            r9 = 2
            byte r9 = r16[r9]
            r5 = r5[r9]
            java.lang.StringBuilder r5 = r8.append(r5)
            java.lang.String r8 = ")"
            java.lang.StringBuilder r5 = r5.append(r8)
            java.lang.String r5 = r5.toString()
            java.lang.String r5 = r6.replace(r7, r5)
        L_0x057e:
            float r6 = (float) r11
            r0 = r24
            android.graphics.Paint r0 = r0.g
            r7 = r0
            float r7 = r7.measureText(r5)
            r8 = 1073741824(0x40000000, float:2.0)
            float r7 = r7 / r8
            float r6 = r6 - r7
            int r7 = net.weweweb.android.common.a.r
            int r7 = r7 + r18
            int r7 = r7 + r20
            int r7 = r7 + r12
            r8 = 2
            int r7 = r7 - r8
            float r7 = (float) r7
            r0 = r24
            android.graphics.Paint r0 = r0.g
            r8 = r0
            r0 = r25
            r1 = r5
            r2 = r6
            r3 = r7
            r4 = r8
            r0.drawText(r1, r2, r3, r4)
            if (r14 < 0) goto L_0x05aa
            r5 = 12
            if (r14 <= r5) goto L_0x0897
        L_0x05aa:
            r0 = r24
            net.weweweb.android.bridge.BridgeApp r0 = r0.f13a
            r5 = r0
            net.weweweb.android.bridge.ad r5 = r5.m
            a.b r5 = r5.b
            byte r5 = r5.j()
            r6 = -1
            if (r5 == r6) goto L_0x05ea
            r0 = r24
            net.weweweb.android.bridge.BridgeApp r0 = r0.f13a
            r6 = r0
            net.weweweb.android.bridge.ad r6 = r6.m
            byte[] r6 = r6.k
            byte r5 = r6[r5]
            int r6 = r24.getWidth()
            int r6 = r6 / 2
            int r7 = r24.getHeight()
            int r7 = r7 / 2
            if (r5 != 0) goto L_0x0a6a
            int r5 = net.weweweb.android.common.a.r
            int r5 = r7 - r5
            r7 = -2
            int r7 = r5 - r7
            r8 = -5
            r9 = 10
            r10 = 5
            r11 = 10
            r0 = r24
            android.graphics.Paint r0 = r0.k
            r12 = r0
            r5 = r25
            net.weweweb.android.common.c.a(r5, r6, r7, r8, r9, r10, r11, r12)
        L_0x05ea:
            boolean[] r5 = r13.m
            byte[] r6 = r13.l
            r7 = 1
            byte r6 = r6[r7]
            boolean r6 = r5[r6]
            r0 = r24
            byte r0 = r0.d
            r5 = r0
            r7 = 1
            if (r5 != r7) goto L_0x0ac0
            r0 = r13
            net.weweweb.android.bridge.am r0 = (net.weweweb.android.bridge.am) r0
            r5 = r0
            if (r6 != 0) goto L_0x0aba
            net.weweweb.android.bridge.aq r6 = r5.n
            net.weweweb.android.bridge.ab r6 = r6.i
            java.lang.String r6 = r6.d
            java.lang.String r7 = "willy"
            boolean r6 = r6.equals(r7)
            if (r6 == 0) goto L_0x060f
        L_0x060f:
            r6 = 0
        L_0x0610:
            if (r6 != 0) goto L_0x0624
            byte r5 = r5.h
            r6 = 3
            if (r5 != r6) goto L_0x0abd
            a.b r5 = r13.b
            byte[] r6 = r13.l
            r7 = 1
            byte r6 = r6[r7]
            boolean r5 = r5.A(r6)
            if (r5 == 0) goto L_0x0abd
        L_0x0624:
            r5 = 1
        L_0x0625:
            r12 = r5
        L_0x0626:
            r0 = r24
            int[] r0 = r0.j
            r5 = r0
            r6 = 1
            r5 = r5[r6]
            net.weweweb.android.common.a.c(r5)
            r0 = r24
            net.weweweb.android.common.a r0 = r0.c
            r5 = r0
            a.b r6 = r13.b
            byte[][] r6 = r6.l
            byte[] r7 = r13.l
            r8 = 1
            byte r7 = r7[r8]
            r7 = r6[r7]
            r0 = r24
            android.graphics.Point[] r0 = r0.f
            r6 = r0
            r8 = 1
            r6 = r6[r8]
            int r8 = r6.x
            r0 = r24
            android.graphics.Point[] r0 = r0.f
            r6 = r0
            r9 = 1
            r6 = r6[r9]
            int r9 = r6.y
            int[][] r6 = net.weweweb.android.common.a.s
            r10 = 0
            r6 = r6[r10]
            r10 = 0
            r10 = r6[r10]
            r11 = 2
            r6 = r25
            r5.a(r6, r7, r8, r9, r10, r11, r12)
            boolean[] r5 = r13.m
            byte[] r6 = r13.l
            r7 = 3
            byte r6 = r6[r7]
            boolean r5 = r5[r6]
            r0 = r24
            byte r0 = r0.d
            r6 = r0
            r7 = 1
            if (r6 != r7) goto L_0x0b10
            r0 = r13
            net.weweweb.android.bridge.am r0 = (net.weweweb.android.bridge.am) r0
            r14 = r0
            if (r5 != 0) goto L_0x0b0a
            net.weweweb.android.bridge.aq r5 = r14.n
            net.weweweb.android.bridge.ab r5 = r5.i
            java.lang.String r5 = r5.d
            java.lang.String r6 = "willy"
            boolean r5 = r5.equals(r6)
            if (r5 == 0) goto L_0x0688
        L_0x0688:
            r5 = 0
        L_0x0689:
            if (r5 != 0) goto L_0x069d
            byte r5 = r14.h
            r6 = 3
            if (r5 != r6) goto L_0x0b0d
            a.b r5 = r13.b
            byte[] r6 = r13.l
            r7 = 3
            byte r6 = r6[r7]
            boolean r5 = r5.A(r6)
            if (r5 == 0) goto L_0x0b0d
        L_0x069d:
            r5 = 1
        L_0x069e:
            r12 = r5
        L_0x069f:
            r0 = r24
            int[] r0 = r0.j
            r5 = r0
            r6 = 3
            r5 = r5[r6]
            net.weweweb.android.common.a.c(r5)
            r0 = r24
            net.weweweb.android.common.a r0 = r0.c
            r5 = r0
            a.b r6 = r13.b
            byte[][] r6 = r6.l
            byte[] r7 = r13.l
            r8 = 3
            byte r7 = r7[r8]
            r7 = r6[r7]
            r0 = r24
            android.graphics.Point[] r0 = r0.f
            r6 = r0
            r8 = 3
            r6 = r6[r8]
            int r8 = r6.x
            r0 = r24
            android.graphics.Point[] r0 = r0.f
            r6 = r0
            r9 = 3
            r6 = r6[r9]
            int r9 = r6.y
            int[][] r6 = net.weweweb.android.common.a.s
            r10 = 0
            r6 = r6[r10]
            r10 = 0
            r10 = r6[r10]
            r11 = 1
            r6 = r25
            r5.a(r6, r7, r8, r9, r10, r11, r12)
            boolean[] r5 = r13.m
            byte[] r6 = r13.l
            r7 = 0
            byte r6 = r6[r7]
            boolean r5 = r5[r6]
            r0 = r24
            byte r0 = r0.d
            r6 = r0
            r7 = 1
            if (r6 != r7) goto L_0x0b60
            r0 = r13
            net.weweweb.android.bridge.am r0 = (net.weweweb.android.bridge.am) r0
            r14 = r0
            if (r5 != 0) goto L_0x0b5a
            net.weweweb.android.bridge.aq r5 = r14.n
            net.weweweb.android.bridge.ab r5 = r5.i
            java.lang.String r5 = r5.d
            java.lang.String r6 = "willy"
            boolean r5 = r5.equals(r6)
            if (r5 == 0) goto L_0x0701
        L_0x0701:
            r5 = 0
        L_0x0702:
            if (r5 != 0) goto L_0x0716
            byte r5 = r14.h
            r6 = 3
            if (r5 != r6) goto L_0x0b5d
            a.b r5 = r14.b
            byte[] r6 = r14.l
            r7 = 0
            byte r6 = r6[r7]
            boolean r5 = r5.A(r6)
            if (r5 == 0) goto L_0x0b5d
        L_0x0716:
            r5 = 1
        L_0x0717:
            r10 = r5
        L_0x0718:
            r0 = r24
            int[] r0 = r0.j
            r5 = r0
            r6 = 0
            r5 = r5[r6]
            net.weweweb.android.common.a.c(r5)
            r0 = r24
            net.weweweb.android.common.a r0 = r0.c
            r5 = r0
            a.b r6 = r13.b
            byte[][] r6 = r6.l
            byte[] r7 = r13.l
            r8 = 0
            byte r7 = r7[r8]
            r7 = r6[r7]
            r0 = r24
            android.graphics.Point[] r0 = r0.f
            r6 = r0
            r8 = 0
            r6 = r6[r8]
            int r8 = r6.x
            r0 = r24
            android.graphics.Point[] r0 = r0.f
            r6 = r0
            r9 = 0
            r6 = r6[r9]
            int r9 = r6.y
            r0 = r24
            byte[] r0 = r0.e
            r11 = r0
            r6 = r25
            r5.a(r6, r7, r8, r9, r10, r11)
            boolean[] r5 = r13.m
            byte[] r6 = r13.l
            r7 = 2
            byte r6 = r6[r7]
            boolean r5 = r5[r6]
            r0 = r24
            byte r0 = r0.d
            r6 = r0
            r7 = 1
            if (r6 != r7) goto L_0x0ba8
            r0 = r13
            net.weweweb.android.bridge.am r0 = (net.weweweb.android.bridge.am) r0
            r14 = r0
            if (r5 != 0) goto L_0x0ba2
            net.weweweb.android.bridge.aq r5 = r14.n
            net.weweweb.android.bridge.ab r5 = r5.i
            java.lang.String r5 = r5.d
            java.lang.String r6 = "willy"
            boolean r5 = r5.equals(r6)
            if (r5 == 0) goto L_0x0776
        L_0x0776:
            r5 = 0
        L_0x0777:
            if (r5 != 0) goto L_0x078b
            byte r5 = r14.h
            r6 = 3
            if (r5 != r6) goto L_0x0ba5
            a.b r5 = r14.b
            byte[] r6 = r14.l
            r7 = 2
            byte r6 = r6[r7]
            boolean r5 = r5.A(r6)
            if (r5 == 0) goto L_0x0ba5
        L_0x078b:
            r5 = 1
        L_0x078c:
            r10 = r5
        L_0x078d:
            r0 = r24
            int[] r0 = r0.j
            r5 = r0
            r6 = 2
            r5 = r5[r6]
            net.weweweb.android.common.a.c(r5)
            r0 = r24
            net.weweweb.android.common.a r0 = r0.c
            r5 = r0
            a.b r6 = r13.b
            byte[][] r6 = r6.l
            byte[] r7 = r13.l
            r8 = 2
            byte r7 = r7[r8]
            r7 = r6[r7]
            r0 = r24
            android.graphics.Point[] r0 = r0.f
            r6 = r0
            r8 = 2
            r6 = r6[r8]
            int r8 = r6.x
            r0 = r24
            android.graphics.Point[] r0 = r0.f
            r6 = r0
            r9 = 2
            r6 = r6[r9]
            int r9 = r6.y
            r0 = r24
            byte[] r0 = r0.e
            r11 = r0
            r6 = r25
            r5.a(r6, r7, r8, r9, r10, r11)
            goto L_0x0011
        L_0x07c8:
            int r8 = net.weweweb.android.common.a.r
            int r8 = r8 + 10
            int r9 = net.weweweb.android.common.a.r
            int r9 = r5 - r9
            r10 = 10
            int r9 = r9 - r10
            r23 = r9
            r9 = r8
            r8 = r23
            goto L_0x004a
        L_0x07da:
            r0 = r24
            int[] r0 = r0.j
            r10 = r0
            r11 = 0
            int r12 = r8 - r9
            int r14 = net.weweweb.android.common.a.q
            int r12 = r12 - r14
            int r12 = r12 / r7
            r10[r11] = r12
            r0 = r24
            int[] r0 = r0.j
            r10 = r0
            r11 = 0
            r10 = r10[r11]
            int r11 = net.weweweb.android.common.a.q
            if (r10 <= r11) goto L_0x0055
            r0 = r24
            int[] r0 = r0.j
            r10 = r0
            r11 = 0
            int r12 = net.weweweb.android.common.a.q
            r10[r11] = r12
            goto L_0x0055
        L_0x0800:
            r0 = r24
            int[] r0 = r0.j
            r8 = r0
            r9 = 2
            r10 = 0
            int r10 = r5 - r10
            int r11 = net.weweweb.android.common.a.q
            int r10 = r10 - r11
            int r10 = r10 / r7
            r8[r9] = r10
            r0 = r24
            int[] r0 = r0.j
            r8 = r0
            r9 = 2
            r8 = r8[r9]
            int r9 = net.weweweb.android.common.a.q
            if (r8 <= r9) goto L_0x008b
            r0 = r24
            int[] r0 = r0.j
            r8 = r0
            r9 = 2
            int r10 = net.weweweb.android.common.a.q
            r8[r9] = r10
            goto L_0x008b
        L_0x0827:
            int r8 = net.weweweb.android.common.a.r
            int r8 = r8 + 10
            int r9 = net.weweweb.android.common.a.r
            int r9 = r6 - r9
            r10 = 10
            int r9 = r9 - r10
            r23 = r9
            r9 = r8
            r8 = r23
            goto L_0x00e1
        L_0x0839:
            r0 = r24
            int[] r0 = r0.j
            r10 = r0
            r11 = 3
            int r12 = r8 - r9
            int r14 = net.weweweb.android.common.a.q
            int r12 = r12 - r14
            int r12 = r12 / r7
            r10[r11] = r12
            r0 = r24
            int[] r0 = r0.j
            r10 = r0
            r11 = 3
            r10 = r10[r11]
            int r11 = net.weweweb.android.common.a.q
            if (r10 <= r11) goto L_0x00ec
            r0 = r24
            int[] r0 = r0.j
            r10 = r0
            r11 = 3
            int r12 = net.weweweb.android.common.a.q
            r10[r11] = r12
            goto L_0x00ec
        L_0x085f:
            int r8 = net.weweweb.android.common.a.r
            int r8 = r8 + 10
            int r9 = net.weweweb.android.common.a.r
            int r9 = r6 - r9
            r10 = 10
            int r9 = r9 - r10
            r23 = r9
            r9 = r8
            r8 = r23
            goto L_0x013c
        L_0x0871:
            r0 = r24
            int[] r0 = r0.j
            r10 = r0
            r11 = 1
            int r12 = r8 - r9
            int r14 = net.weweweb.android.common.a.q
            int r12 = r12 - r14
            int r12 = r12 / r7
            r10[r11] = r12
            r0 = r24
            int[] r0 = r0.j
            r10 = r0
            r11 = 1
            r10 = r10[r11]
            int r11 = net.weweweb.android.common.a.q
            if (r10 <= r11) goto L_0x0147
            r0 = r24
            int[] r0 = r0.j
            r10 = r0
            r11 = 1
            int r12 = net.weweweb.android.common.a.q
            r10[r11] = r12
            goto L_0x0147
        L_0x0897:
            byte r5 = r15.t(r14)
            byte r5 = r17[r5]
            switch(r5) {
                case 0: goto L_0x08a2;
                case 1: goto L_0x0914;
                case 2: goto L_0x0986;
                case 3: goto L_0x09f8;
                default: goto L_0x08a0;
            }
        L_0x08a0:
            goto L_0x05aa
        L_0x08a2:
            r0 = r24
            net.weweweb.android.common.a r0 = r0.c
            r5 = r0
            r6 = 0
            byte r6 = r16[r6]
            byte r7 = r15.a(r6, r14)
            int r6 = net.weweweb.android.common.a.q
            int r6 = r6 / 2
            int r8 = r11 - r6
            int r6 = net.weweweb.android.common.a.r
            int r6 = r12 - r6
            int r9 = r6 - r18
            r10 = 1
            r6 = r25
            r5.a(r6, r7, r8, r9, r10)
            r0 = r24
            net.weweweb.android.common.a r0 = r0.c
            r5 = r0
            r6 = 1
            byte r6 = r16[r6]
            byte r7 = r15.a(r6, r14)
            int r8 = r11 + r19
            int r6 = net.weweweb.android.common.a.r
            int r6 = r6 / 2
            int r9 = r12 - r6
            r10 = 1
            r6 = r25
            r5.a(r6, r7, r8, r9, r10)
            r0 = r24
            net.weweweb.android.common.a r0 = r0.c
            r5 = r0
            r6 = 2
            byte r6 = r16[r6]
            byte r7 = r15.a(r6, r14)
            int r6 = net.weweweb.android.common.a.q
            int r6 = r6 / 2
            int r8 = r11 - r6
            int r9 = r12 + r18
            r10 = 1
            r6 = r25
            r5.a(r6, r7, r8, r9, r10)
            r0 = r24
            net.weweweb.android.common.a r0 = r0.c
            r5 = r0
            r6 = 3
            byte r6 = r16[r6]
            byte r7 = r15.a(r6, r14)
            int r6 = net.weweweb.android.common.a.q
            int r6 = r11 - r6
            int r8 = r6 - r19
            int r6 = net.weweweb.android.common.a.r
            int r6 = r6 / 2
            int r9 = r12 - r6
            r10 = 1
            r6 = r25
            r5.a(r6, r7, r8, r9, r10)
            goto L_0x05aa
        L_0x0914:
            r0 = r24
            net.weweweb.android.common.a r0 = r0.c
            r5 = r0
            r6 = 1
            byte r6 = r16[r6]
            byte r7 = r15.a(r6, r14)
            int r8 = r11 + r19
            int r6 = net.weweweb.android.common.a.r
            int r6 = r6 / 2
            int r9 = r12 - r6
            r10 = 1
            r6 = r25
            r5.a(r6, r7, r8, r9, r10)
            r0 = r24
            net.weweweb.android.common.a r0 = r0.c
            r5 = r0
            r6 = 2
            byte r6 = r16[r6]
            byte r7 = r15.a(r6, r14)
            int r6 = net.weweweb.android.common.a.q
            int r6 = r6 / 2
            int r8 = r11 - r6
            int r9 = r12 + r18
            r10 = 1
            r6 = r25
            r5.a(r6, r7, r8, r9, r10)
            r0 = r24
            net.weweweb.android.common.a r0 = r0.c
            r5 = r0
            r6 = 3
            byte r6 = r16[r6]
            byte r7 = r15.a(r6, r14)
            int r6 = net.weweweb.android.common.a.q
            int r6 = r11 - r6
            int r8 = r6 - r19
            int r6 = net.weweweb.android.common.a.r
            int r6 = r6 / 2
            int r9 = r12 - r6
            r10 = 1
            r6 = r25
            r5.a(r6, r7, r8, r9, r10)
            r0 = r24
            net.weweweb.android.common.a r0 = r0.c
            r5 = r0
            r6 = 0
            byte r6 = r16[r6]
            byte r7 = r15.a(r6, r14)
            int r6 = net.weweweb.android.common.a.q
            int r6 = r6 / 2
            int r8 = r11 - r6
            int r6 = net.weweweb.android.common.a.r
            int r6 = r12 - r6
            int r9 = r6 - r18
            r10 = 1
            r6 = r25
            r5.a(r6, r7, r8, r9, r10)
            goto L_0x05aa
        L_0x0986:
            r0 = r24
            net.weweweb.android.common.a r0 = r0.c
            r5 = r0
            r6 = 2
            byte r6 = r16[r6]
            byte r7 = r15.a(r6, r14)
            int r6 = net.weweweb.android.common.a.q
            int r6 = r6 / 2
            int r8 = r11 - r6
            int r9 = r12 + r18
            r10 = 1
            r6 = r25
            r5.a(r6, r7, r8, r9, r10)
            r0 = r24
            net.weweweb.android.common.a r0 = r0.c
            r5 = r0
            r6 = 3
            byte r6 = r16[r6]
            byte r7 = r15.a(r6, r14)
            int r6 = net.weweweb.android.common.a.q
            int r6 = r11 - r6
            int r8 = r6 - r19
            int r6 = net.weweweb.android.common.a.r
            int r6 = r6 / 2
            int r9 = r12 - r6
            r10 = 1
            r6 = r25
            r5.a(r6, r7, r8, r9, r10)
            r0 = r24
            net.weweweb.android.common.a r0 = r0.c
            r5 = r0
            r6 = 0
            byte r6 = r16[r6]
            byte r7 = r15.a(r6, r14)
            int r6 = net.weweweb.android.common.a.q
            int r6 = r6 / 2
            int r8 = r11 - r6
            int r6 = net.weweweb.android.common.a.r
            int r6 = r12 - r6
            int r9 = r6 - r18
            r10 = 1
            r6 = r25
            r5.a(r6, r7, r8, r9, r10)
            r0 = r24
            net.weweweb.android.common.a r0 = r0.c
            r5 = r0
            r6 = 1
            byte r6 = r16[r6]
            byte r7 = r15.a(r6, r14)
            int r8 = r11 + r19
            int r6 = net.weweweb.android.common.a.r
            int r6 = r6 / 2
            int r9 = r12 - r6
            r10 = 1
            r6 = r25
            r5.a(r6, r7, r8, r9, r10)
            goto L_0x05aa
        L_0x09f8:
            r0 = r24
            net.weweweb.android.common.a r0 = r0.c
            r5 = r0
            r6 = 3
            byte r6 = r16[r6]
            byte r7 = r15.a(r6, r14)
            int r6 = net.weweweb.android.common.a.q
            int r6 = r11 - r6
            int r8 = r6 - r19
            int r6 = net.weweweb.android.common.a.r
            int r6 = r6 / 2
            int r9 = r12 - r6
            r10 = 1
            r6 = r25
            r5.a(r6, r7, r8, r9, r10)
            r0 = r24
            net.weweweb.android.common.a r0 = r0.c
            r5 = r0
            r6 = 0
            byte r6 = r16[r6]
            byte r7 = r15.a(r6, r14)
            int r6 = net.weweweb.android.common.a.q
            int r6 = r6 / 2
            int r8 = r11 - r6
            int r6 = net.weweweb.android.common.a.r
            int r6 = r12 - r6
            int r9 = r6 - r18
            r10 = 1
            r6 = r25
            r5.a(r6, r7, r8, r9, r10)
            r0 = r24
            net.weweweb.android.common.a r0 = r0.c
            r5 = r0
            r6 = 1
            byte r6 = r16[r6]
            byte r7 = r15.a(r6, r14)
            int r8 = r11 + r19
            int r6 = net.weweweb.android.common.a.r
            int r6 = r6 / 2
            int r9 = r12 - r6
            r10 = 1
            r6 = r25
            r5.a(r6, r7, r8, r9, r10)
            r0 = r24
            net.weweweb.android.common.a r0 = r0.c
            r5 = r0
            r6 = 2
            byte r6 = r16[r6]
            byte r7 = r15.a(r6, r14)
            int r6 = net.weweweb.android.common.a.q
            int r6 = r6 / 2
            int r8 = r11 - r6
            int r9 = r12 + r18
            r10 = 1
            r6 = r25
            r5.a(r6, r7, r8, r9, r10)
            goto L_0x05aa
        L_0x0a6a:
            r8 = 1
            if (r5 != r8) goto L_0x0a84
            int r5 = net.weweweb.android.common.a.q
            int r5 = r5 + r6
            int r6 = r5 + -2
            r8 = -10
            r9 = -5
            r10 = -10
            r11 = 5
            r0 = r24
            android.graphics.Paint r0 = r0.k
            r12 = r0
            r5 = r25
            net.weweweb.android.common.c.a(r5, r6, r7, r8, r9, r10, r11, r12)
            goto L_0x05ea
        L_0x0a84:
            r8 = 2
            if (r5 != r8) goto L_0x0a9e
            int r5 = net.weweweb.android.common.a.r
            int r5 = r5 + r7
            int r7 = r5 + -2
            r8 = -5
            r9 = -10
            r10 = 5
            r11 = -10
            r0 = r24
            android.graphics.Paint r0 = r0.k
            r12 = r0
            r5 = r25
            net.weweweb.android.common.c.a(r5, r6, r7, r8, r9, r10, r11, r12)
            goto L_0x05ea
        L_0x0a9e:
            r8 = 3
            if (r5 != r8) goto L_0x05ea
            int r5 = net.weweweb.android.common.a.q
            int r5 = r6 - r5
            r6 = -2
            int r6 = r5 - r6
            r8 = 10
            r9 = -5
            r10 = 10
            r11 = 5
            r0 = r24
            android.graphics.Paint r0 = r0.k
            r12 = r0
            r5 = r25
            net.weweweb.android.common.c.a(r5, r6, r7, r8, r9, r10, r11, r12)
            goto L_0x05ea
        L_0x0aba:
            r6 = 1
            goto L_0x0610
        L_0x0abd:
            r5 = 0
            goto L_0x0625
        L_0x0ac0:
            if (r6 != 0) goto L_0x0acb
            byte r5 = r13.e
            byte[] r6 = r13.l
            r7 = 1
            byte r6 = r6[r7]
            if (r5 != r6) goto L_0x0b04
        L_0x0acb:
            r5 = 1
        L_0x0acc:
            if (r5 != 0) goto L_0x0ae3
            a.b r5 = r13.b
            byte[] r6 = r13.l
            r7 = 1
            byte r6 = r6[r7]
            boolean r5 = r5.C(r6)
            if (r5 == 0) goto L_0x0b06
            a.b r5 = r13.b
            boolean r5 = r5.J()
            if (r5 != 0) goto L_0x0b06
        L_0x0ae3:
            r5 = 1
        L_0x0ae4:
            if (r5 != 0) goto L_0x0b00
            byte r5 = r13.e
            byte[] r6 = r13.l
            r7 = 1
            byte r6 = r6[r7]
            boolean r5 = a.b.h(r5, r6)
            if (r5 == 0) goto L_0x0b08
            a.b r5 = r13.b
            byte[] r6 = r13.l
            r7 = 1
            byte r6 = r6[r7]
            boolean r5 = r5.A(r6)
            if (r5 == 0) goto L_0x0b08
        L_0x0b00:
            r5 = 1
        L_0x0b01:
            r12 = r5
            goto L_0x0626
        L_0x0b04:
            r5 = 0
            goto L_0x0acc
        L_0x0b06:
            r5 = 0
            goto L_0x0ae4
        L_0x0b08:
            r5 = 0
            goto L_0x0b01
        L_0x0b0a:
            r5 = 1
            goto L_0x0689
        L_0x0b0d:
            r5 = 0
            goto L_0x069e
        L_0x0b10:
            if (r5 != 0) goto L_0x0b1b
            byte r5 = r13.e
            byte[] r6 = r13.l
            r7 = 3
            byte r6 = r6[r7]
            if (r5 != r6) goto L_0x0b54
        L_0x0b1b:
            r5 = 1
        L_0x0b1c:
            if (r5 != 0) goto L_0x0b33
            a.b r5 = r13.b
            byte[] r6 = r13.l
            r7 = 3
            byte r6 = r6[r7]
            boolean r5 = r5.C(r6)
            if (r5 == 0) goto L_0x0b56
            a.b r5 = r13.b
            boolean r5 = r5.J()
            if (r5 != 0) goto L_0x0b56
        L_0x0b33:
            r5 = 1
        L_0x0b34:
            if (r5 != 0) goto L_0x0b50
            byte r5 = r13.e
            byte[] r6 = r13.l
            r7 = 3
            byte r6 = r6[r7]
            boolean r5 = a.b.h(r5, r6)
            if (r5 == 0) goto L_0x0b58
            a.b r5 = r13.b
            byte[] r6 = r13.l
            r7 = 3
            byte r6 = r6[r7]
            boolean r5 = r5.A(r6)
            if (r5 == 0) goto L_0x0b58
        L_0x0b50:
            r5 = 1
        L_0x0b51:
            r12 = r5
            goto L_0x069f
        L_0x0b54:
            r5 = 0
            goto L_0x0b1c
        L_0x0b56:
            r5 = 0
            goto L_0x0b34
        L_0x0b58:
            r5 = 0
            goto L_0x0b51
        L_0x0b5a:
            r5 = 1
            goto L_0x0702
        L_0x0b5d:
            r5 = 0
            goto L_0x0717
        L_0x0b60:
            if (r5 != 0) goto L_0x0b6b
            byte r5 = r13.e
            byte[] r6 = r13.l
            r7 = 0
            byte r6 = r6[r7]
            if (r5 != r6) goto L_0x0b9c
        L_0x0b6b:
            r5 = 1
        L_0x0b6c:
            if (r5 != 0) goto L_0x0b7b
            a.b r5 = r13.b
            byte[] r6 = r13.l
            r7 = 0
            byte r6 = r6[r7]
            boolean r5 = r5.C(r6)
            if (r5 == 0) goto L_0x0b9e
        L_0x0b7b:
            r5 = 1
        L_0x0b7c:
            if (r5 != 0) goto L_0x0b98
            byte r5 = r13.e
            byte[] r6 = r13.l
            r7 = 0
            byte r6 = r6[r7]
            boolean r5 = a.b.h(r5, r6)
            if (r5 == 0) goto L_0x0ba0
            a.b r5 = r13.b
            byte[] r6 = r13.l
            r7 = 0
            byte r6 = r6[r7]
            boolean r5 = r5.A(r6)
            if (r5 == 0) goto L_0x0ba0
        L_0x0b98:
            r5 = 1
        L_0x0b99:
            r10 = r5
            goto L_0x0718
        L_0x0b9c:
            r5 = 0
            goto L_0x0b6c
        L_0x0b9e:
            r5 = 0
            goto L_0x0b7c
        L_0x0ba0:
            r5 = 0
            goto L_0x0b99
        L_0x0ba2:
            r5 = 1
            goto L_0x0777
        L_0x0ba5:
            r5 = 0
            goto L_0x078c
        L_0x0ba8:
            if (r5 != 0) goto L_0x0bb3
            byte r5 = r13.e
            byte[] r6 = r13.l
            r7 = 2
            byte r6 = r6[r7]
            if (r5 != r6) goto L_0x0be4
        L_0x0bb3:
            r5 = 1
        L_0x0bb4:
            if (r5 != 0) goto L_0x0bc3
            a.b r5 = r13.b
            byte[] r6 = r13.l
            r7 = 2
            byte r6 = r6[r7]
            boolean r5 = r5.C(r6)
            if (r5 == 0) goto L_0x0be6
        L_0x0bc3:
            r5 = 1
        L_0x0bc4:
            if (r5 != 0) goto L_0x0be0
            byte r5 = r13.e
            byte[] r6 = r13.l
            r7 = 2
            byte r6 = r6[r7]
            boolean r5 = a.b.h(r5, r6)
            if (r5 == 0) goto L_0x0be8
            a.b r5 = r13.b
            byte[] r6 = r13.l
            r7 = 2
            byte r6 = r6[r7]
            boolean r5 = r5.A(r6)
            if (r5 == 0) goto L_0x0be8
        L_0x0be0:
            r5 = 1
        L_0x0be1:
            r10 = r5
            goto L_0x078d
        L_0x0be4:
            r5 = 0
            goto L_0x0bb4
        L_0x0be6:
            r5 = 0
            goto L_0x0bc4
        L_0x0be8:
            r5 = 0
            goto L_0x0be1
        L_0x0bea:
            r5 = r6
            goto L_0x057e
        L_0x0bed:
            r5 = r6
            goto L_0x02a9
        L_0x0bf0:
            r14 = r7
            goto L_0x0190
        */
        throw new UnsupportedOperationException("Method not decompiled: net.weweweb.android.bridge.GamePlayingView.onDraw(android.graphics.Canvas):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x0084  */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x01ac  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onKeyDown(int r10, android.view.KeyEvent r11) {
        /*
            r9 = this;
            r4 = 21
            r8 = 12
            r7 = 1
            r6 = 0
            r5 = -1
            byte r0 = r9.d
            if (r0 != r7) goto L_0x001a
            net.weweweb.android.bridge.BridgeApp r0 = r9.f13a
            net.weweweb.android.bridge.ad r0 = r0.m
            net.weweweb.android.bridge.am r0 = (net.weweweb.android.bridge.am) r0
            boolean r0 = r0.p
            if (r0 != 0) goto L_0x001a
            boolean r0 = super.onKeyDown(r10, r11)
        L_0x0019:
            return r0
        L_0x001a:
            r0 = 23
            if (r10 != r0) goto L_0x0045
            byte[] r0 = r9.e
            byte r0 = r0[r6]
            if (r0 == r5) goto L_0x01b6
            byte r0 = r9.d
            if (r0 != r7) goto L_0x003f
            net.weweweb.android.bridge.BridgeApp r0 = r9.f13a
            net.weweweb.android.bridge.ad r0 = r0.m
            net.weweweb.android.bridge.am r0 = (net.weweweb.android.bridge.am) r0
            net.weweweb.android.bridge.BridgeApp r1 = r9.f13a
            net.weweweb.android.bridge.ad r1 = r1.m
            a.b r1 = r1.b
            byte r1 = r1.j()
            byte[] r2 = r9.e
            byte r2 = r2[r6]
            r0.c(r1, r2)
        L_0x003f:
            byte[] r0 = r9.e
            r0[r6] = r5
            r0 = r7
            goto L_0x0019
        L_0x0045:
            if (r10 == r4) goto L_0x004b
            r0 = 22
            if (r10 != r0) goto L_0x01b6
        L_0x004b:
            net.weweweb.android.bridge.BridgeApp r0 = r9.f13a
            net.weweweb.android.bridge.ad r0 = r0.m
            a.b r0 = r0.b
            byte r1 = r0.j()
            net.weweweb.android.bridge.BridgeApp r2 = r9.f13a
            net.weweweb.android.bridge.ad r2 = r2.m
            boolean r2 = r2.j
            if (r2 != 0) goto L_0x01a9
            net.weweweb.android.bridge.BridgeApp r2 = r9.f13a
            net.weweweb.android.bridge.ad r2 = r2.m
            byte r2 = r2.e
            boolean r2 = a.b.g(r2, r1)
            if (r2 != 0) goto L_0x0081
            net.weweweb.android.bridge.BridgeApp r2 = r9.f13a
            net.weweweb.android.bridge.ad r2 = r2.m
            byte r2 = r2.e
            boolean r2 = a.b.h(r2, r1)
            if (r2 == 0) goto L_0x0086
            net.weweweb.android.bridge.BridgeApp r2 = r9.f13a
            net.weweweb.android.bridge.ad r2 = r2.m
            byte r2 = r2.e
            boolean r2 = r0.B(r2)
            if (r2 == 0) goto L_0x0086
        L_0x0081:
            r0 = r5
        L_0x0082:
            if (r0 != r5) goto L_0x01ac
            r0 = r7
            goto L_0x0019
        L_0x0086:
            byte r2 = r9.d
            if (r2 != 0) goto L_0x00aa
            boolean r2 = net.weweweb.android.bridge.BridgeApp.n
            if (r2 == 0) goto L_0x00aa
            net.weweweb.android.bridge.BridgeApp r2 = r9.f13a
            net.weweweb.android.bridge.ad r2 = r2.m
            byte r2 = r2.e
            byte r3 = r0.j()
            boolean r2 = a.b.i(r2, r3)
            if (r2 == 0) goto L_0x00aa
            net.weweweb.android.bridge.BridgeApp r2 = r9.f13a
            net.weweweb.android.bridge.ad r2 = r2.m
            byte r2 = r2.e
            boolean r2 = r0.C(r2)
            if (r2 != 0) goto L_0x01a9
        L_0x00aa:
            if (r10 != r4) goto L_0x0126
            byte[] r2 = r9.e
            byte r2 = r2[r6]
            if (r2 != r5) goto L_0x00d3
            r2 = r8
        L_0x00b3:
            if (r2 < 0) goto L_0x01a9
            byte[][] r3 = r0.l
            r3 = r3[r1]
            byte r3 = r3[r2]
            if (r3 == r5) goto L_0x00d0
            byte[][] r3 = r0.l
            r3 = r3[r1]
            byte r3 = r3[r2]
            boolean r3 = r0.k(r3, r1)
            if (r3 == 0) goto L_0x00d0
            byte[][] r0 = r0.l
            r0 = r0[r1]
            byte r0 = r0[r2]
            goto L_0x0082
        L_0x00d0:
            int r2 = r2 - r7
            byte r2 = (byte) r2
            goto L_0x00b3
        L_0x00d3:
            byte[][] r2 = r0.l
            r2 = r2[r1]
            byte[] r3 = r9.e
            byte r3 = r3[r6]
            byte r2 = a.b.a(r2, r3)
            if (r2 == r5) goto L_0x01a9
            int r3 = r2 - r7
            byte r3 = (byte) r3
        L_0x00e4:
            if (r3 < 0) goto L_0x0104
            byte[][] r4 = r0.l
            r4 = r4[r1]
            byte r4 = r4[r3]
            if (r4 == r5) goto L_0x0101
            byte[][] r4 = r0.l
            r4 = r4[r1]
            byte r4 = r4[r3]
            boolean r4 = r0.k(r4, r1)
            if (r4 == 0) goto L_0x0101
            byte[][] r0 = r0.l
            r0 = r0[r1]
            byte r0 = r0[r3]
            goto L_0x0082
        L_0x0101:
            int r3 = r3 - r7
            byte r3 = (byte) r3
            goto L_0x00e4
        L_0x0104:
            r3 = r8
        L_0x0105:
            if (r3 < r2) goto L_0x01a9
            byte[][] r4 = r0.l
            r4 = r4[r1]
            byte r4 = r4[r3]
            if (r4 == r5) goto L_0x0123
            byte[][] r4 = r0.l
            r4 = r4[r1]
            byte r4 = r4[r3]
            boolean r4 = r0.k(r4, r1)
            if (r4 == 0) goto L_0x0123
            byte[][] r0 = r0.l
            r0 = r0[r1]
            byte r0 = r0[r3]
            goto L_0x0082
        L_0x0123:
            int r3 = r3 - r7
            byte r3 = (byte) r3
            goto L_0x0105
        L_0x0126:
            r2 = 22
            if (r10 != r2) goto L_0x01a9
            byte[] r2 = r9.e
            byte r2 = r2[r6]
            if (r2 != r5) goto L_0x0153
            r2 = r6
        L_0x0131:
            if (r2 > r8) goto L_0x01a9
            byte[][] r3 = r0.l
            r3 = r3[r1]
            byte r3 = r3[r2]
            if (r3 == r5) goto L_0x014f
            byte[][] r3 = r0.l
            r3 = r3[r1]
            byte r3 = r3[r2]
            boolean r3 = r0.k(r3, r1)
            if (r3 == 0) goto L_0x014f
            byte[][] r0 = r0.l
            r0 = r0[r1]
            byte r0 = r0[r2]
            goto L_0x0082
        L_0x014f:
            int r2 = r2 + 1
            byte r2 = (byte) r2
            goto L_0x0131
        L_0x0153:
            byte[][] r2 = r0.l
            r2 = r2[r1]
            byte[] r3 = r9.e
            byte r3 = r3[r6]
            byte r2 = a.b.a(r2, r3)
            if (r2 == r5) goto L_0x01a9
            int r3 = r2 + 1
            byte r3 = (byte) r3
        L_0x0164:
            if (r3 > r8) goto L_0x0186
            byte[][] r4 = r0.l
            r4 = r4[r1]
            byte r4 = r4[r3]
            if (r4 == r5) goto L_0x0182
            byte[][] r4 = r0.l
            r4 = r4[r1]
            byte r4 = r4[r3]
            boolean r4 = r0.k(r4, r1)
            if (r4 == 0) goto L_0x0182
            byte[][] r0 = r0.l
            r0 = r0[r1]
            byte r0 = r0[r3]
            goto L_0x0082
        L_0x0182:
            int r3 = r3 + 1
            byte r3 = (byte) r3
            goto L_0x0164
        L_0x0186:
            r3 = r6
        L_0x0187:
            if (r3 > r2) goto L_0x01a9
            byte[][] r4 = r0.l
            r4 = r4[r1]
            byte r4 = r4[r3]
            if (r4 == r5) goto L_0x01a5
            byte[][] r4 = r0.l
            r4 = r4[r1]
            byte r4 = r4[r3]
            boolean r4 = r0.k(r4, r1)
            if (r4 == 0) goto L_0x01a5
            byte[][] r0 = r0.l
            r0 = r0[r1]
            byte r0 = r0[r3]
            goto L_0x0082
        L_0x01a5:
            int r3 = r3 + 1
            byte r3 = (byte) r3
            goto L_0x0187
        L_0x01a9:
            r0 = r5
            goto L_0x0082
        L_0x01ac:
            byte[] r1 = r9.e
            r1[r6] = r0
            r9.invalidate()
            r0 = r7
            goto L_0x0019
        L_0x01b6:
            boolean r0 = super.onKeyDown(r10, r11)
            goto L_0x0019
        */
        throw new UnsupportedOperationException("Method not decompiled: net.weweweb.android.bridge.GamePlayingView.onKeyDown(int, android.view.KeyEvent):boolean");
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        int a2 = a(i2);
        int a3 = a(i3);
        setMeasuredDimension(a2, a3);
        a.b(a3 / 2);
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x008e  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x0151  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouchEvent(android.view.MotionEvent r14) {
        /*
            r13 = this;
            r12 = 0
            r11 = -1
            r10 = 1
            int r1 = r14.getAction()
            byte r0 = r13.d
            if (r0 != r10) goto L_0x0025
            net.weweweb.android.bridge.BridgeApp r0 = r13.f13a
            net.weweweb.android.bridge.ad r0 = r0.m
            if (r0 != 0) goto L_0x0016
            boolean r0 = super.onTouchEvent(r14)
        L_0x0015:
            return r0
        L_0x0016:
            net.weweweb.android.bridge.BridgeApp r0 = r13.f13a
            net.weweweb.android.bridge.ad r0 = r0.m
            net.weweweb.android.bridge.am r0 = (net.weweweb.android.bridge.am) r0
            boolean r0 = r0.p
            if (r0 != 0) goto L_0x0025
            boolean r0 = super.onTouchEvent(r14)
            goto L_0x0015
        L_0x0025:
            if (r1 != 0) goto L_0x0095
            float r0 = r14.getX()
            int r0 = (int) r0
            float r1 = r14.getY()
            int r1 = (int) r1
            net.weweweb.android.bridge.BridgeApp r2 = r13.f13a
            net.weweweb.android.bridge.ad r2 = r2.m
            a.b r6 = r2.b
            byte r7 = r6.j()
            net.weweweb.android.bridge.BridgeApp r2 = r13.f13a
            net.weweweb.android.bridge.ad r2 = r2.m
            byte[] r2 = r2.k
            byte r8 = r2[r7]
            net.weweweb.android.bridge.BridgeApp r2 = r13.f13a
            net.weweweb.android.bridge.ad r2 = r2.m
            boolean r2 = r2.j
            if (r2 != 0) goto L_0x014e
            net.weweweb.android.bridge.BridgeApp r2 = r13.f13a
            net.weweweb.android.bridge.ad r2 = r2.m
            byte r2 = r2.e
            boolean r2 = a.b.g(r2, r7)
            if (r2 != 0) goto L_0x006f
            net.weweweb.android.bridge.BridgeApp r2 = r13.f13a
            net.weweweb.android.bridge.ad r2 = r2.m
            byte r2 = r2.e
            boolean r2 = a.b.h(r2, r7)
            if (r2 == 0) goto L_0x0098
            net.weweweb.android.bridge.BridgeApp r2 = r13.f13a
            net.weweweb.android.bridge.ad r2 = r2.m
            byte r2 = r2.e
            boolean r2 = r6.B(r2)
            if (r2 == 0) goto L_0x0098
        L_0x006f:
            r0 = r11
        L_0x0070:
            if (r0 == r11) goto L_0x0095
            net.weweweb.android.bridge.BridgeApp r1 = r13.f13a
            net.weweweb.android.bridge.ad r1 = r1.m
            a.b r1 = r1.b
            net.weweweb.android.bridge.BridgeApp r2 = r13.f13a
            net.weweweb.android.bridge.ad r2 = r2.m
            a.b r2 = r2.b
            byte r2 = r2.j()
            boolean r1 = r1.k(r0, r2)
            if (r1 == 0) goto L_0x0095
            byte[] r1 = r13.e
            byte r1 = r1[r12]
            if (r1 == r0) goto L_0x0151
            byte[] r1 = r13.e
            r1[r12] = r0
            r13.invalidate()
        L_0x0095:
            r0 = r10
            goto L_0x0015
        L_0x0098:
            byte r2 = r13.d
            if (r2 != 0) goto L_0x00c6
            boolean r2 = net.weweweb.android.bridge.BridgeApp.n
            if (r2 == 0) goto L_0x00bc
            net.weweweb.android.bridge.BridgeApp r2 = r13.f13a
            net.weweweb.android.bridge.ad r2 = r2.m
            byte r2 = r2.e
            byte r3 = r6.j()
            boolean r2 = a.b.i(r2, r3)
            if (r2 == 0) goto L_0x00bc
            net.weweweb.android.bridge.BridgeApp r2 = r13.f13a
            net.weweweb.android.bridge.ad r2 = r2.m
            byte r2 = r2.e
            boolean r2 = r6.C(r2)
            if (r2 != 0) goto L_0x014e
        L_0x00bc:
            boolean r2 = net.weweweb.android.bridge.BridgeApp.B
            if (r2 == 0) goto L_0x00c6
            byte r2 = r6.F()
            if (r2 == r10) goto L_0x014e
        L_0x00c6:
            r2 = 12
            r9 = r2
        L_0x00c9:
            if (r9 < 0) goto L_0x014e
            byte[][] r2 = r6.l
            r2 = r2[r7]
            byte r2 = r2[r9]
            if (r2 == r11) goto L_0x0148
            byte[][] r2 = r6.l
            r2 = r2[r7]
            byte r2 = r2[r9]
            byte[] r3 = r13.e
            byte r3 = r3[r12]
            if (r2 == r3) goto L_0x010b
            android.graphics.Point[] r2 = r13.f
            r2 = r2[r8]
            int r2 = r2.x
            int[] r3 = r13.j
            net.weweweb.android.bridge.BridgeApp r4 = r13.f13a
            net.weweweb.android.bridge.ad r4 = r4.m
            byte[] r4 = r4.k
            byte r4 = r4[r7]
            r3 = r3[r4]
            int r3 = r3 * r9
            int r2 = r2 + r3
            android.graphics.Point[] r3 = r13.f
            r3 = r3[r8]
            int r3 = r3.y
            int r4 = net.weweweb.android.common.a.q
            int r5 = net.weweweb.android.common.a.r
            boolean r2 = a.g.a(r0, r1, r2, r3, r4, r5)
            if (r2 == 0) goto L_0x0148
            byte[][] r0 = r6.l
            r0 = r0[r7]
            byte r0 = r0[r9]
            goto L_0x0070
        L_0x010b:
            android.graphics.Point[] r2 = r13.f
            r2 = r2[r8]
            int r2 = r2.x
            int[] r3 = r13.j
            net.weweweb.android.bridge.BridgeApp r4 = r13.f13a
            net.weweweb.android.bridge.ad r4 = r4.m
            byte[] r4 = r4.k
            byte r4 = r4[r7]
            r3 = r3[r4]
            int r3 = r3 * r9
            int r2 = r2 + r3
            android.graphics.Point[] r3 = r13.f
            r3 = r3[r8]
            int r3 = r3.y
            net.weweweb.android.bridge.BridgeApp r4 = r13.f13a
            net.weweweb.android.bridge.ad r4 = r4.m
            byte r4 = r4.e
            if (r7 != r4) goto L_0x0146
            r4 = r11
        L_0x012e:
            int r5 = net.weweweb.android.common.a.c()
            int r4 = r4 * r5
            int r3 = r3 + r4
            int r4 = net.weweweb.android.common.a.q
            int r5 = net.weweweb.android.common.a.r
            boolean r2 = a.g.a(r0, r1, r2, r3, r4, r5)
            if (r2 == 0) goto L_0x0148
            byte[][] r0 = r6.l
            r0 = r0[r7]
            byte r0 = r0[r9]
            goto L_0x0070
        L_0x0146:
            r4 = r10
            goto L_0x012e
        L_0x0148:
            int r2 = r9 - r10
            byte r2 = (byte) r2
            r9 = r2
            goto L_0x00c9
        L_0x014e:
            r0 = r11
            goto L_0x0070
        L_0x0151:
            byte r0 = r13.d
            if (r0 != r10) goto L_0x016e
            net.weweweb.android.bridge.BridgeApp r0 = r13.f13a
            net.weweweb.android.bridge.ad r0 = r0.m
            net.weweweb.android.bridge.am r0 = (net.weweweb.android.bridge.am) r0
            net.weweweb.android.bridge.BridgeApp r1 = r13.f13a
            net.weweweb.android.bridge.ad r1 = r1.m
            a.b r1 = r1.b
            byte r1 = r1.j()
            byte[] r2 = r13.e
            byte r2 = r2[r12]
            r0.c(r1, r2)
            goto L_0x0095
        L_0x016e:
            net.weweweb.android.bridge.BridgeApp r0 = r13.f13a
            net.weweweb.android.bridge.ad r0 = r0.m
            net.weweweb.android.bridge.bp r0 = (net.weweweb.android.bridge.bp) r0
            byte[] r1 = r13.e
            byte r1 = r1[r12]
            r0.d(r1)
            goto L_0x0095
        */
        throw new UnsupportedOperationException("Method not decompiled: net.weweweb.android.bridge.GamePlayingView.onTouchEvent(android.view.MotionEvent):boolean");
    }

    /* access modifiers changed from: package-private */
    public final void a(BridgeGameActivity bridgeGameActivity) {
        this.b = bridgeGameActivity;
        this.d = bridgeGameActivity instanceof SoloGameActivity ? (byte) 0 : 1;
        this.c = ((BridgeApp) bridgeGameActivity.getApplicationContext()).l;
    }
}
