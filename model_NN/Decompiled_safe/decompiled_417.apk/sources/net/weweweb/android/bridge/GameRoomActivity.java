package net.weweweb.android.bridge;

import android.app.TabActivity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TabHost;
import net.weweweb.android.free.bridge.R;

/* compiled from: ProGuard */
public class GameRoomActivity extends TabActivity {

    /* renamed from: a  reason: collision with root package name */
    BridgeApp f14a;

    public final void a() {
        if (this.f14a.f != null) {
            this.f14a.f.c();
        }
        if (this.f14a.i != null) {
            TableListActivity tableListActivity = this.f14a.i;
            if (tableListActivity.f26a.i == tableListActivity) {
                tableListActivity.f26a.i = null;
            }
            tableListActivity.finish();
        }
        if (this.f14a.d != null) {
            PlayerListActivity playerListActivity = this.f14a.d;
            if (playerListActivity.f19a.d == playerListActivity) {
                playerListActivity.f19a.d = null;
            }
            playerListActivity.finish();
        }
        finish();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.gameroom);
        this.f14a = (BridgeApp) getApplicationContext();
        this.f14a.c = this;
        Resources resources = getResources();
        TabHost tabHost = getTabHost();
        tabHost.addTab(tabHost.newTabSpec("table").setIndicator("Tables", resources.getDrawable(R.drawable.ic_tab_table)).setContent(new Intent().setClass(this, TableListActivity.class)));
        tabHost.addTab(tabHost.newTabSpec("player").setIndicator("Players", resources.getDrawable(R.drawable.ic_tab_player)).setContent(new Intent().setClass(this, PlayerListActivity.class)));
        tabHost.setCurrentTab(0);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.gameroom_option_menu, menu);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.f14a.c == this) {
            this.f14a.c = null;
        }
        super.onDestroy();
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.gameRoomOptionMenuItemMessage /*2131165377*/:
                Intent intent = new Intent();
                intent.setClass(this, MessageActivity.class);
                startActivity(intent);
                return true;
            default:
                return true;
        }
    }
}
