package net.weweweb.android.bridge;

import android.os.Handler;
import android.os.Message;

/* compiled from: ProGuard */
final class an extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ am f41a;

    an(am amVar) {
        this.f41a = amVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: net.weweweb.android.bridge.am.a(byte, java.lang.String, boolean):void
     arg types: [byte, java.lang.String, int]
     candidates:
      net.weweweb.android.bridge.am.a(net.weweweb.android.bridge.am, byte, boolean):void
      net.weweweb.android.bridge.am.a(byte, java.lang.String, boolean):void */
    public final void handleMessage(Message message) {
        if (message.what == 1001) {
            this.f41a.a((byte) message.arg1, (String) message.obj, true);
        } else if (message.what == 1002) {
            this.f41a.c((byte) message.arg2, (byte) message.arg1);
        } else if (message.what == 1003) {
            this.f41a.j = false;
            if (this.f41a.f32a.f != null) {
                this.f41a.f32a.f.c.invalidate();
            }
        } else if (message.what == 1004) {
            this.f41a.a((byte) message.arg1, message.arg2 == 1);
        }
        super.handleMessage(message);
    }
}
