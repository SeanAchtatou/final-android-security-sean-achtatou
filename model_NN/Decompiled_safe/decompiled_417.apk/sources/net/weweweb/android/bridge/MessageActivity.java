package net.weweweb.android.bridge;

import a.h;
import android.app.Activity;
import android.app.AlertDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import java.util.ArrayList;
import net.weweweb.android.free.bridge.R;

/* compiled from: ProGuard */
public class MessageActivity extends Activity implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    BridgeApp f17a;
    String[] b;

    public final void a() {
        if (this.f17a.e == this) {
            this.f17a.e = null;
        }
        finish();
    }

    public void onClick(View view) {
        aq aqVar = this.f17a.k.d;
        if (view.equals(findViewById(R.id.btnSendMsg))) {
            EditText editText = (EditText) findViewById(R.id.txtMsgOut);
            String obj = editText.getText().toString();
            if (obj.length() > 0) {
                for (String indexOf : this.f17a.k.c) {
                    if (obj.replaceAll(" ", "").toLowerCase().indexOf(indexOf) >= 0) {
                        editText.setText("");
                        this.f17a.k.n = null;
                        return;
                    }
                }
                String replaceAll = obj.replaceAll("<", "&lt;");
                if (obj.charAt(0) == '/') {
                    this.f17a.k.b(replaceAll);
                } else if (aqVar.r >= 0) {
                    aa aaVar = aqVar.s;
                    int i = aqVar.r;
                    aaVar.c.a();
                    aaVar.c.a(i);
                    aaVar.c.a(aaVar.f29a.l);
                    aaVar.c.a(replaceAll);
                    aaVar.f29a.a(h.a(38, (byte) 6, (byte) aaVar.f29a.l, aaVar.c.f7a, aaVar.c.b, aaVar.c.c, aaVar.c.d, aaVar.c.e, aaVar.c.f));
                } else if (aqVar.q >= 0) {
                    aa aaVar2 = aqVar.s;
                    int i2 = aqVar.l;
                    int i3 = aqVar.q;
                    aaVar2.c.a();
                    aaVar2.c.a(i2);
                    aaVar2.c.a(i3);
                    aaVar2.c.a(replaceAll);
                    aaVar2.f29a.a(h.a(38, (byte) 9, (byte) aaVar2.f29a.l, aaVar2.c.f7a, aaVar2.c.b, aaVar2.c.c, aaVar2.c.d, aaVar2.c.e, aaVar2.c.f));
                } else if (aqVar.i.f == null || aqVar.i.f.p != 2) {
                    aqVar.a(h.a((byte) 4, replaceAll));
                } else {
                    aqVar.a(h.a((byte) 1, replaceAll));
                }
                editText.setText("");
            }
        } else if (view.equals(findViewById(R.id.btnSelectChannelOut))) {
            this.f17a.k.a();
            if (this.f17a.k.h.size() != 0) {
                this.b = new String[(this.f17a.k.h.size() + 1)];
                this.f17a.k.h.toArray(this.b);
                this.b[this.b.length - 1] = "Cancel";
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Channel Output");
                builder.setItems(this.b, new ah(this));
                builder.create().show();
            }
        } else if (view.equals(findViewById(R.id.btnSelectWhisperTo))) {
            c();
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.message);
        this.f17a = (BridgeApp) getApplicationContext();
        this.f17a.e = this;
        ((TextView) findViewById(R.id.messageIn)).setMovementMethod(new ScrollingMovementMethod());
        EditText editText = (EditText) findViewById(R.id.txtMsgOut);
        if (this.f17a.k.n != null) {
            editText.setText(this.f17a.k.n);
        }
        if (this.f17a.k != null && this.f17a.k.d != null && this.f17a.k.d.q != -1) {
            this.f17a.k.a(this.f17a.k.d.h[this.f17a.k.d.q]);
        } else if (!(this.f17a.k == null || this.f17a.k.d == null || this.f17a.k.d.r == -1)) {
            this.f17a.k.a(this.f17a.k.d.s.a(this.f17a.k.d.r));
        }
        ((Button) findViewById(R.id.btnSendMsg)).setOnClickListener(this);
        ((Button) findViewById(R.id.btnSelectChannelOut)).setOnClickListener(this);
        ((Button) findViewById(R.id.btnSelectWhisperTo)).setOnClickListener(this);
        b();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        EditText editText = (EditText) findViewById(R.id.txtMsgOut);
        if (editText.toString().length() > 0) {
            this.f17a.k.n = editText.getText().toString();
        }
        if (this.f17a.e != null && this.f17a.e.equals(this)) {
            this.f17a.e = null;
        }
        super.onDestroy();
    }

    public final void b() {
        TextView textView = (TextView) findViewById(R.id.messageIn);
        textView.setTextColor(Color.rgb(0, 255, 0));
        textView.setText(Html.fromHtml(((BridgeApp) getApplicationContext()).k.e.toString()));
    }

    private void c() {
        ArrayList arrayList = new ArrayList();
        arrayList.add("Cancel");
        synchronized (this.f17a.k.d.h) {
            for (int i = 0; i < this.f17a.k.d.h.length; i++) {
                if (!(this.f17a.k.d.h[i] == null || this.f17a.k.d.h[i] == this.f17a.k.d.i)) {
                    arrayList.add(this.f17a.k.d.h[i].d);
                }
            }
        }
        this.b = new String[arrayList.size()];
        arrayList.toArray(this.b);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Whisper To");
        builder.setItems(this.b, new ai(this));
        builder.create().show();
    }
}
