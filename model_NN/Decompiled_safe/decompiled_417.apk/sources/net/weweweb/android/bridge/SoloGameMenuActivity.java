package net.weweweb.android.bridge;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import java.util.ArrayList;
import java.util.HashMap;
import net.weweweb.android.free.bridge.R;

/* compiled from: ProGuard */
public class SoloGameMenuActivity extends Activity {

    /* renamed from: a  reason: collision with root package name */
    BridgeApp f23a;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f23a = (BridgeApp) getApplicationContext();
        setContentView((int) R.layout.mainmenu);
        ListView listView = (ListView) findViewById(R.id.mainMenu);
        ArrayList arrayList = new ArrayList();
        HashMap hashMap = new HashMap();
        hashMap.put("icon", Integer.valueOf((int) R.drawable.ic_menu_cards));
        hashMap.put("title", "Solo Game");
        hashMap.put("desc", "Play Solo Game now.");
        arrayList.add(hashMap);
        HashMap hashMap2 = new HashMap();
        hashMap2.put("icon", Integer.valueOf((int) R.drawable.ic_menu_manage));
        hashMap2.put("title", "Solo Game Setting");
        hashMap2.put("desc", "Settings related to the Solo Game.");
        arrayList.add(hashMap2);
        HashMap hashMap3 = new HashMap();
        hashMap3.put("icon", Integer.valueOf((int) R.drawable.ic_menu_arrow_down));
        hashMap3.put("title", "Bidding Rule Update");
        hashMap3.put("desc", "Download the bidding rule from server.");
        arrayList.add(hashMap3);
        listView.setAdapter((ListAdapter) new SimpleAdapter(this, arrayList, R.layout.mainmenuitem, new String[]{"icon", "title", "desc"}, new int[]{R.id.mainMenuItemImage, R.id.mainMenuItemTitle, R.id.mainMenuItemDesc}));
        listView.setOnItemClickListener(new bi(this));
    }
}
