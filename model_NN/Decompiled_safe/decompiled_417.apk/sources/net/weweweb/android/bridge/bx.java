package net.weweweb.android.bridge;

import a.b;
import android.app.Dialog;
import android.content.Context;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import net.weweweb.android.common.a;
import net.weweweb.android.free.bridge.R;

/* compiled from: ProGuard */
final class bx extends Dialog {

    /* renamed from: a  reason: collision with root package name */
    ad f77a = null;
    b b = null;
    byte c = 0;

    bx(Context context) {
        super(context);
        setContentView((int) R.layout.turncardsdialog);
        setTitle((int) R.string.turncards_dialog_title);
        ((Button) findViewById(R.id.turnCardsDialogButtonOK)).setOnClickListener(new by(this));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.b.a(int, int):byte
     arg types: [byte, byte]
     candidates:
      a.b.a(int, byte):byte
      a.b.a(byte[], byte):byte
      a.b.a(byte[][], int):byte
      a.b.a(byte, byte):java.lang.String
      a.b.a(byte[], boolean):void
      a.b.a(byte, int):byte
      a.b.a(byte, char):int
      a.b.a(byte, boolean):boolean
      a.b.a(int, int):byte */
    /* access modifiers changed from: package-private */
    public final void a(ad adVar, byte b2) {
        this.f77a = adVar;
        this.b = adVar.b;
        this.c = b2;
        if (b2 >= 0 && b2 <= 12 && this.b.C() > b2) {
            byte b3 = adVar.l[0];
            ((ImageView) findViewById(R.id.turnCardsDialogCard0)).setImageResource(a.k[this.b.a((int) b3, (int) b2)]);
            TextView textView = (TextView) findViewById(R.id.turnCardsDialogSeat0);
            textView.setText(b.g[b3].substring(0, 1));
            if (this.b.t(b2) == 0) {
                textView.setTextColor(-65536);
            } else {
                textView.setTextColor(-16777216);
            }
            byte b4 = adVar.l[1];
            ((ImageView) findViewById(R.id.turnCardsDialogCard1)).setImageResource(a.k[this.b.a((int) b4, (int) b2)]);
            TextView textView2 = (TextView) findViewById(R.id.turnCardsDialogSeat1);
            textView2.setText(b.g[b4].substring(0, 1));
            if (this.b.t(b2) == 1) {
                textView2.setTextColor(-65536);
            } else {
                textView2.setTextColor(-16777216);
            }
            byte b5 = adVar.l[2];
            ((ImageView) findViewById(R.id.turnCardsDialogCard2)).setImageResource(a.k[this.b.a((int) b5, (int) b2)]);
            TextView textView3 = (TextView) findViewById(R.id.turnCardsDialogSeat2);
            textView3.setText(b.g[b5].substring(0, 1));
            if (this.b.t(b2) == 2) {
                textView3.setTextColor(-65536);
            } else {
                textView3.setTextColor(-16777216);
            }
            byte b6 = adVar.l[3];
            ((ImageView) findViewById(R.id.turnCardsDialogCard3)).setImageResource(a.k[this.b.a((int) b6, (int) b2)]);
            TextView textView4 = (TextView) findViewById(R.id.turnCardsDialogSeat3);
            textView4.setText(b.g[b6].substring(0, 1));
            if (this.b.t(b2) == 3) {
                textView4.setTextColor(-65536);
            } else {
                textView4.setTextColor(-16777216);
            }
        }
    }
}
