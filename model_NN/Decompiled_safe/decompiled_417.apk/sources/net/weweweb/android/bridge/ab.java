package net.weweweb.android.bridge;

/* compiled from: ProGuard */
final class ab {

    /* renamed from: a  reason: collision with root package name */
    q f30a;
    public int b = -1;
    public byte c = -1;
    public String d = null;
    public String e = null;
    public ac f = null;
    public byte g = -1;
    public int h = 0;
    public short i = 0;
    public String j = "--";
    public String k = null;
    public long l;
    public String m;
    public String n;

    ab(q qVar) {
        this.f30a = qVar;
    }

    public final String a() {
        if (this.f == null) {
            return "Free";
        }
        if (this.f.p == 1) {
            return "Waiting";
        }
        if (this.f.p == 2) {
            return "Busy";
        }
        return "Unknown";
    }

    public final String toString() {
        String str = "";
        if (this.f == null) {
            str = "(Free)";
        } else if (this.f.p == 1) {
            str = "(Waiting)";
        } else if (this.f.p == 2) {
            str = "(Busy)";
        }
        return "[" + this.b + "] " + this.d + " " + str;
    }
}
