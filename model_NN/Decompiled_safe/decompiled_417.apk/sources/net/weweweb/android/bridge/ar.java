package net.weweweb.android.bridge;

import a.b;
import a.g;
import android.app.Dialog;
import android.content.Context;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import net.weweweb.android.free.bridge.R;

/* compiled from: ProGuard */
public final class ar extends Dialog {

    /* renamed from: a  reason: collision with root package name */
    Context f45a = null;
    b b = null;
    Button[] c = new Button[13];
    byte[] d = new byte[13];

    public ar(Context context) {
        super(context);
        this.f45a = context;
        setContentView((int) R.layout.playcarddialog);
        setTitle((int) R.string.play_card_dialog_title);
        ((Button) findViewById(R.id.playCardCancelButton)).setOnClickListener(new as(this));
        for (int i = 0; i < this.c.length; i++) {
            this.c[i] = new Button(context);
        }
    }

    private void a() {
        TableLayout tableLayout = (TableLayout) findViewById(R.id.playCardButtonsTable);
        int childCount = tableLayout.getChildCount();
        for (int i = 0; i < childCount; i++) {
            ((TableRow) tableLayout.getChildAt(i)).removeAllViews();
        }
        tableLayout.removeAllViews();
        TableRow tableRow = null;
        byte b2 = 0;
        for (int i2 = 0; i2 < this.d.length; i2++) {
            if (this.d[i2] != -1) {
                if (b2 % 5 == 0) {
                    tableRow = new TableRow(this.f45a);
                    tableLayout.addView(tableRow);
                }
                tableRow.addView(this.c[i2]);
                b2 = (byte) (b2 + 1);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(b bVar) {
        this.b = bVar;
        if (bVar != null && !bVar.K()) {
            byte j = bVar.j();
            setTitle(this.f45a.getString(R.string.play_card_dialog_title) + " (" + b.g[j] + ")");
            g.b(this.d, (byte) -1);
            for (int i = 0; i < bVar.l[j].length; i++) {
                if (bVar.l[j][i] != -1 && bVar.k(bVar.l[j][i], j)) {
                    this.c[i].setText(BridgeApp.a(b.f(bVar.l[j][i])));
                    this.d[i] = bVar.l[j][i];
                }
            }
            a();
        }
    }
}
