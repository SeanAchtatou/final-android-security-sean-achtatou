package net.weweweb.android.bridge;

import a.b;
import a.h;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import java.util.ArrayList;
import java.util.HashMap;
import net.weweweb.android.free.bridge.R;

/* compiled from: ProGuard */
public class TableListActivity extends Activity implements View.OnClickListener, AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    BridgeApp f26a;
    ArrayList b;
    SimpleAdapter c;
    Button d;
    Button e;
    Button f;
    Button g;
    String[] h;
    String i;
    int j = 0;
    int k = 0;

    /* access modifiers changed from: package-private */
    public final void a(ac acVar) {
        boolean z;
        if (acVar != null && acVar.p != 0 && acVar.c == 1) {
            int i2 = 0;
            while (true) {
                if (i2 >= this.b.size()) {
                    z = false;
                    break;
                } else if (((String) ((HashMap) this.b.get(i2)).get("id")).compareTo(d(acVar)) == 0) {
                    a((HashMap) this.b.get(i2), acVar);
                    z = true;
                    break;
                } else if (((String) ((HashMap) this.b.get(i2)).get("id")).compareTo(d(acVar)) > 0) {
                    this.b.add(i2, c(acVar));
                    z = true;
                    break;
                } else {
                    i2++;
                }
            }
            if (!z) {
                this.b.add(c(acVar));
            }
            if (this.c != null) {
                this.c.notifyDataSetChanged();
            }
        }
    }

    private void c() {
        ac[] acVarArr = this.f26a.k.d.j;
        synchronized (acVarArr) {
            for (int i2 = 0; i2 < acVarArr.length; i2++) {
                if (acVarArr[i2] != null) {
                    a(acVarArr[i2]);
                }
            }
        }
    }

    public final void a() {
        this.d.setEnabled(false);
        this.e.setEnabled(false);
        this.f.setEnabled(false);
        this.g.setEnabled(false);
    }

    public final void b() {
        if (this.f26a.k.d.i.f == null) {
            this.d.setEnabled(true);
            this.e.setEnabled(false);
            this.f.setEnabled(false);
        } else if (this.f26a.k.d.i.f.p == 1) {
            this.d.setEnabled(false);
            this.e.setEnabled(true);
            this.g.setEnabled(true);
        } else if (this.f26a.k.d.i.f.p == 2) {
            a();
        }
    }

    private static HashMap c(ac acVar) {
        if (acVar == null || acVar.b < 0) {
            return null;
        }
        HashMap hashMap = new HashMap();
        hashMap.put("id", d(acVar));
        a(hashMap, acVar);
        return hashMap;
    }

    /* access modifiers changed from: private */
    public static int b(String str) {
        try {
            return Integer.parseInt(str.substring(str.indexOf("#") + 1));
        } catch (Exception e2) {
            return -1;
        }
    }

    private static String d(ac acVar) {
        if (acVar == null) {
            return "Null table";
        }
        if (acVar.b < 0) {
            return "Null table";
        }
        if (acVar.b < 10) {
            return "Table #0" + Integer.toString(acVar.b);
        }
        return "Table #" + Integer.toString(acVar.b);
    }

    public void onClick(View view) {
        int i2 = 0;
        BridgeApp bridgeApp = (BridgeApp) getApplicationContext();
        if (view.equals(this.d)) {
            if (this.d.isEnabled()) {
                a();
                this.j = 1;
                synchronized (bridgeApp.k.d.j) {
                    while (true) {
                        if (i2 < bridgeApp.k.d.j.length) {
                            synchronized (bridgeApp.k.d.j[i2]) {
                                if (bridgeApp.k.d.j[i2].p == 0) {
                                    bridgeApp.k.d.a(h.a(i2, (byte) 0, (byte) 1, (byte) 0));
                                    this.k = i2;
                                }
                            }
                        }
                        i2++;
                    }
                }
            }
        } else if (view.equals(this.e)) {
            if (this.e.isEnabled() && bridgeApp.k.d.i.f != null) {
                a();
                this.j = 2;
                synchronized (bridgeApp.k.d.i.f) {
                    bridgeApp.k.d.a(h.a(bridgeApp.k.d.i.f.b, (byte) -1, (byte) 0, (byte) 0));
                }
            }
        } else if (view.equals(this.f)) {
            if (this.f.isEnabled()) {
                this.f.setEnabled(false);
                bridgeApp.k.d.e();
            }
        } else if (view.equals(this.g) && this.g.isEnabled()) {
            ArrayList arrayList = new ArrayList();
            arrayList.add("Cancel");
            synchronized (this.f26a.k.d.h) {
                while (i2 < this.f26a.k.d.h.length) {
                    if (!(this.f26a.k.d.h[i2] == null || this.f26a.k.d.h[i2] == this.f26a.k.d.i || this.f26a.k.d.h[i2].f != null)) {
                        arrayList.add(this.f26a.k.d.h[i2].d);
                    }
                    i2++;
                }
            }
            this.h = new String[arrayList.size()];
            arrayList.toArray(this.h);
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle((int) R.string.invite_player);
            builder.setItems(this.h, new bu(this));
            builder.create().show();
        }
    }

    public boolean onContextItemSelected(MenuItem menuItem) {
        byte b2;
        int b3 = b((String) ((HashMap) this.b.get((int) this.c.getItemId(((AdapterView.AdapterContextMenuInfo) menuItem.getMenuInfo()).position))).get("id"));
        if (b3 == -1) {
            return true;
        }
        String obj = menuItem.getTitle().toString();
        if (obj.equals("Cancel")) {
            return true;
        }
        BridgeApp bridgeApp = (BridgeApp) getApplicationContext();
        if (bridgeApp.k.d.i.f != null) {
            return true;
        }
        if (bridgeApp.k.d.j[b3] == null) {
            return true;
        }
        synchronized (bridgeApp.k.d.j[b3]) {
            if (obj.startsWith("Join")) {
                this.j = 3;
                b2 = 0;
            } else if (obj.startsWith("Watch")) {
                this.j = 4;
                b2 = 1;
            } else {
                this.j = 0;
                return true;
            }
            for (byte b4 = 0; b4 < b.g.length; b4 = (byte) (b4 + 1)) {
                if (obj.contains(" " + b.g[b4])) {
                    this.k = b3;
                    bridgeApp.k.d.a(h.a((byte) b3, b2, (byte) 1, b4));
                    a();
                    return true;
                }
            }
            return true;
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f26a = (BridgeApp) getApplicationContext();
        this.f26a.i = this;
        setContentView((int) R.layout.tablelist);
        ListView listView = (ListView) findViewById(R.id.tableListView);
        registerForContextMenu(listView);
        this.b = new ArrayList();
        c();
        this.c = new SimpleAdapter(this, this.b, R.layout.tablelistitem, new String[]{"id", "status", "ns", "ew", "watchers"}, new int[]{R.id.tableListViewItemId, R.id.tableListViewItemStatus, R.id.tableListViewItemNS, R.id.tableListViewItemEW, R.id.tableListViewItemWatchers});
        listView.setAdapter((ListAdapter) this.c);
        listView.setOnItemClickListener(this);
        this.d = (Button) findViewById(R.id.btnNewTable);
        this.d.setOnClickListener(this);
        this.e = (Button) findViewById(R.id.btnLeaveTable);
        this.e.setOnClickListener(this);
        this.f = (Button) findViewById(R.id.btnStartGame);
        this.f.setOnClickListener(this);
        this.g = (Button) findViewById(R.id.btnInvitePlayer);
        this.g.setOnClickListener(this);
        a();
        b();
    }

    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        try {
            String str = (String) ((HashMap) this.b.get((int) this.c.getItemId(((AdapterView.AdapterContextMenuInfo) contextMenuInfo).position))).get("id");
            contextMenu.setHeaderTitle(str);
            int b2 = b(str);
            BridgeApp bridgeApp = (BridgeApp) getApplicationContext();
            String str2 = null;
            if (!(b2 == -1 || bridgeApp.k.d.j[b2] == null)) {
                ac acVar = bridgeApp.k.d.j[b2];
                if (acVar.p == 1) {
                    str2 = "Join";
                } else if (acVar.p == 2) {
                    str2 = "Watch";
                }
                if (bridgeApp.k.d.i.f == null) {
                    for (int i2 = 0; i2 < 4; i2++) {
                        if ("Join".equals(str2)) {
                            if (acVar.h[i2] == null) {
                                contextMenu.add(0, 0, 0, str2 + " " + b.g[i2]);
                            }
                        } else if ("Watch".equals(str2) && acVar.h[i2] != null) {
                            contextMenu.add(0, 0, 0, str2 + " " + b.g[i2] + " (" + acVar.h[i2].d + ")");
                        }
                    }
                }
            }
            contextMenu.add(0, 0, 0, "Cancel");
        } catch (ClassCastException e2) {
            Log.e("TableListActivity.OnCreateMenu", "bad menuInfo", e2);
        }
    }

    public void onItemClick(AdapterView adapterView, View view, int i2, long j2) {
        if (adapterView == findViewById(R.id.tableListView)) {
            this.i = (String) ((HashMap) this.b.get((int) j2)).get("id");
            int b2 = b(this.i);
            if (b2 != -1 && this.f26a.k.d.i.f == null) {
                ArrayList arrayList = new ArrayList();
                arrayList.add("Cancel");
                String str = null;
                if (!(b2 == -1 || this.f26a.k.d.j[b2] == null)) {
                    ac acVar = this.f26a.k.d.j[b2];
                    if (acVar.p == 1) {
                        str = "Join";
                    } else if (acVar.p == 2) {
                        str = "Watch";
                    }
                    if (this.f26a.k.d.i.f == null) {
                        for (int i3 = 0; i3 < 4; i3++) {
                            if ("Join".equals(str)) {
                                if (acVar.h[i3] == null) {
                                    arrayList.add(str + " " + b.g[i3]);
                                }
                            } else if ("Watch".equals(str) && acVar.h[i3] != null) {
                                arrayList.add(str + " " + b.g[i3] + " (" + acVar.h[i3].d + ")");
                            }
                        }
                    }
                }
                this.h = new String[arrayList.size()];
                arrayList.toArray(this.h);
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(this.i);
                builder.setItems(this.h, new bv(this));
                builder.create().show();
            }
        }
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return super.onKeyDown(i2, keyEvent);
        }
        new AlertDialog.Builder(this).setIcon(17301543).setTitle(this.f26a.getString(R.string.prompt_confirm_title)).setMessage(this.f26a.getString(R.string.prompt_server_disconnect)).setPositiveButton("Yes", new bt(this)).setNegativeButton("No", (DialogInterface.OnClickListener) null).show();
        return true;
    }

    public void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        findViewById(R.id.btnStartGame).setEnabled(bundle.getBoolean("btnStartGame"));
    }

    public void onSaveInstanceState(Bundle bundle) {
        bundle.putBoolean("btnStartGame", findViewById(R.id.btnStartGame).isEnabled());
        super.onSaveInstanceState(bundle);
    }

    /* access modifiers changed from: package-private */
    public final void b(ac acVar) {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.b.size()) {
                return;
            }
            if (((String) ((HashMap) this.b.get(i3)).get("id")).equals(d(acVar))) {
                this.b.remove(i3);
                if (this.c != null) {
                    this.c.notifyDataSetChanged();
                    return;
                }
                return;
            }
            i2 = i3 + 1;
        }
    }

    private static void a(HashMap hashMap, ac acVar) {
        String str;
        String str2;
        String str3;
        int i2 = 0;
        if (acVar != null) {
            hashMap.put("status", acVar.b());
            String str4 = "<None>";
            StringBuilder sb = new StringBuilder();
            if (acVar.h[0] != null) {
                str = acVar.h[0].d;
            } else {
                str = str4;
            }
            if (acVar.h[1] != null) {
                str2 = acVar.h[1].d;
            } else {
                str2 = str4;
            }
            if (acVar.h[2] != null) {
                str3 = acVar.h[2].d;
            } else {
                str3 = str4;
            }
            if (acVar.h[3] != null) {
                str4 = acVar.h[3].d;
            }
            for (int i3 = 4; i3 < 12; i3++) {
                if (acVar.h[i3] != null) {
                    if (i2 > 0) {
                        sb.append(",");
                    }
                    sb.append(acVar.h[i3].d);
                    i2++;
                    if (i2 > 3) {
                        break;
                    }
                }
            }
            if (i2 == 0) {
                sb.append("<None>");
            }
            hashMap.put("ns", "NS:" + str + "," + str3);
            hashMap.put("ew", "EW:" + str2 + "," + str4);
            hashMap.put("watchers", "Watchers:" + sb.toString());
        }
    }
}
