package net.weweweb.android.bridge;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import java.util.List;
import net.weweweb.android.free.bridge.R;

/* compiled from: ProGuard */
final class aw extends SimpleAdapter {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RecordBrowserActivity f50a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    aw(RecordBrowserActivity recordBrowserActivity, Context context, List list, String[] strArr, int[] iArr) {
        super(context, list, R.layout.recordbrowserlistitem, strArr, iArr);
        this.f50a = recordBrowserActivity;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        View view2 = super.getView(i, view, viewGroup);
        TextView textView = (TextView) view2.findViewById(R.id.recordBrowserListItemGameResult);
        textView.setText(BridgeApp.a(textView.getText().toString()));
        return view2;
    }
}
