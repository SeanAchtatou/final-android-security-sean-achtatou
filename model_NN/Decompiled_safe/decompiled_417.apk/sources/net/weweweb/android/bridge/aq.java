package net.weweweb.android.bridge;

import a.f;
import a.g;
import a.h;
import a.i;
import a.j;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.LinkedList;
import net.weweweb.android.free.bridge.R;

/* compiled from: ProGuard */
final class aq implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    q f44a;
    Socket b = null;
    DataInputStream c = null;
    DataOutputStream d = null;
    Thread e = null;
    public boolean f = false;
    bd g;
    ab[] h = new ab[100];
    ab i = null;
    ac[] j = new ac[36];
    public Object k = new Object();
    int l;
    String m = null;
    boolean n;
    boolean o = false;
    boolean p;
    int q = -1;
    int r = -1;
    aa s;
    LinkedList t = null;
    i u = new i(80, 80, 80);

    aq(q qVar) {
        this.f44a = qVar;
        this.n = false;
        this.p = false;
        for (int i2 = 0; i2 < this.j.length; i2++) {
            this.j[i2] = new ac(qVar, i2);
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean a() {
        try {
            this.b = new Socket(f.f5a, 7777);
            try {
                this.c = new DataInputStream(this.b.getInputStream());
                this.d = new DataOutputStream(this.b.getOutputStream());
                this.s = new aa(this);
                this.e = new Thread(this);
                this.e.start();
                this.p = true;
                return true;
            } catch (IOException e2) {
                if (f.e >= 5) {
                    System.out.println(e2);
                }
                try {
                    this.b.close();
                } catch (Exception e3) {
                }
                if (f.e >= 5) {
                    System.out.println(e2);
                }
                return false;
            }
        } catch (Exception e4) {
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public final void b() {
        if (this.s != null) {
            this.s.f29a = null;
        }
        this.s = null;
        if (this.t != null) {
            this.t.clear();
        }
        if (this.g != null) {
            this.g.b = null;
            this.g.a();
        }
        this.g = null;
        if (this.d != null) {
            a(h.c());
        }
        try {
            if (this.c != null) {
                this.c.close();
            }
            if (this.d != null) {
                this.d.close();
            }
            if (this.b != null) {
                this.b.close();
            }
        } catch (IOException e2) {
        }
        this.p = false;
    }

    /* access modifiers changed from: package-private */
    public final ab a(String str) {
        for (short s2 = 0; s2 < this.h.length; s2 = (short) (s2 + 1)) {
            if (this.h[s2] != null && this.h[s2].d.equals(str)) {
                return this.h[s2];
            }
        }
        return null;
    }

    public final void c() {
        this.n = false;
        try {
            this.f44a.f91a.a(19, 0, 0, this.f44a.f91a.getString(R.string.server_disconnected));
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        this.e = null;
        if (this.f44a != null) {
            this.f44a.c();
        }
    }

    private void g() {
        if (this.f44a.f91a.m != null && this.i != null && this.i.f != null && this.i.f.c == 1) {
            this.f44a.f91a.a(12, 0, 0, null);
        }
    }

    private void a(int i2) {
        byte readByte = this.c.readByte();
        if (this.j[readByte] != null) {
            synchronized (this.j[readByte]) {
                switch (i2) {
                    case 24:
                        this.j[readByte].b((byte) 2);
                        this.f44a.f91a.a(6, 0, 0, null);
                        break;
                    case 25:
                        this.j[readByte].b((byte) 1);
                        break;
                }
                this.f44a.f91a.a(3, 0, 0, this.j[readByte]);
            }
            this.f44a.f91a.a(5, 0, 0, this.j[readByte]);
        }
    }

    public final synchronized void a(byte[] bArr) {
        try {
            this.d.write(bArr);
        } catch (IOException e2) {
            System.out.println("GB on output.");
            if (this.e != null) {
                this.e = null;
            }
            if (this.g != null) {
                this.g.a();
                this.g = null;
            }
        }
    }

    private void h() {
        synchronized (this.k) {
            this.f = true;
        }
    }

    public final void d() {
        a(h.a(this.i.f.b, (byte) -1, (byte) 0, (byte) 0));
    }

    /* access modifiers changed from: package-private */
    public final void e() {
        if (this.l != -1 && this.h[this.l] != null) {
            synchronized (this.h[this.l]) {
                if (this.h[this.l].f != null) {
                    if (this.h[this.l].g != -1) {
                        a(h.a());
                    }
                }
            }
        }
    }

    public final void f() {
        synchronized (this.k) {
            this.f = false;
            this.k.notify();
        }
    }

    public final void run() {
        ab abVar;
        Thread currentThread = Thread.currentThread();
        while (true) {
            if (this.e == currentThread) {
                synchronized (this.k) {
                    while (this.f) {
                        try {
                            this.k.wait();
                        } catch (Exception e2) {
                        }
                    }
                }
                try {
                    int readInt = this.c.readInt();
                    if (f.e >= 5) {
                        System.out.println("GameRoom.run:type=" + readInt);
                    }
                    switch (readInt) {
                        case 0:
                            this.l = this.c.readInt();
                            this.m = this.c.readUTF();
                            break;
                        case 1:
                            switch (this.c.readByte()) {
                                case 0:
                                    if (this.f44a.m != null) {
                                        this.f44a.m.finish();
                                        this.f44a.m = null;
                                    }
                                    this.n = true;
                                    this.i = new ab(this.f44a);
                                    this.i.b = this.l;
                                    this.g = new bd(this);
                                    this.g.start();
                                    this.f44a.b();
                                    if (this.t != null) {
                                        this.t.clear();
                                        break;
                                    } else {
                                        this.t = new LinkedList();
                                        continue;
                                    }
                                case 1:
                                    if (this.f44a.m != null) {
                                        this.f44a.f91a.b("You are already logged in.");
                                        break;
                                    } else {
                                        continue;
                                    }
                                case 2:
                                    if (this.f44a.m != null) {
                                        this.f44a.f91a.b("Bad password.");
                                        break;
                                    } else {
                                        continue;
                                    }
                                case 3:
                                    if (this.f44a.m != null) {
                                        this.f44a.f91a.b("That player already exists.");
                                        break;
                                    } else {
                                        continue;
                                    }
                                case 4:
                                    if (this.f44a.m != null) {
                                        this.f44a.f91a.b("No such player.");
                                        break;
                                    } else {
                                        continue;
                                    }
                                case 5:
                                    if (this.f44a.m != null) {
                                        this.f44a.f91a.b("Invalid email address, please update it first.");
                                        break;
                                    } else {
                                        continue;
                                    }
                                case 6:
                                    if (this.f44a.m != null) {
                                        this.f44a.f91a.b("Please activate your email first.");
                                        break;
                                    } else {
                                        continue;
                                    }
                                case 7:
                                    if (this.f44a.m != null) {
                                        this.f44a.f91a.b("Same IP address already login.");
                                        break;
                                    } else {
                                        continue;
                                    }
                                case 8:
                                    if (this.f44a.m != null) {
                                        this.f44a.f91a.b("Too many guests login.");
                                        break;
                                    } else {
                                        continue;
                                    }
                                case 9:
                                default:
                                    continue;
                                case 10:
                                    if (this.f44a.m != null) {
                                        this.f44a.f91a.b("Invalid client.");
                                        break;
                                    } else {
                                        continue;
                                    }
                                case 11:
                                    if (this.f44a.m != null) {
                                        this.f44a.f91a.b("Client version too old.");
                                        break;
                                    } else {
                                        continue;
                                    }
                            }
                        case 2:
                            byte readByte = this.c.readByte();
                            String readUTF = this.c.readUTF();
                            byte readByte2 = this.c.readByte();
                            String readUTF2 = this.c.readUTF();
                            int readInt2 = this.c.readInt();
                            boolean readBoolean = this.c.readBoolean();
                            short readShort = this.c.readShort();
                            String readUTF3 = this.c.readUTF();
                            String readUTF4 = this.c.readUTF();
                            this.h[readByte] = new ab(this.f44a);
                            if (readByte == this.l) {
                                this.i = this.h[readByte];
                            }
                            this.h[readByte].b = readByte;
                            this.h[readByte].d = readUTF;
                            this.h[readByte].c = readByte2;
                            this.h[readByte].e = readUTF2;
                            this.h[readByte].i = readShort;
                            this.h[readByte].h = readInt2;
                            this.h[readByte].m = readUTF3;
                            this.h[readByte].n = readUTF4;
                            if (readBoolean && readByte != this.l) {
                                this.f44a.i(">> " + this.h[readByte].d + " joined the server." + " (" + readUTF3 + " v." + readUTF4 + ")");
                            }
                            this.f44a.f91a.a(1, 0, 0, this.h[readByte]);
                            break;
                        case 3:
                            byte readByte3 = this.c.readByte();
                            byte readByte4 = this.c.readByte();
                            ab abVar2 = this.h[readByte3];
                            if (abVar2 != null) {
                                this.f44a.f91a.a(2, 0, 0, abVar2);
                                this.f44a.i(">> " + abVar2.d + " left the server (" + h.b[readByte4] + ").");
                            }
                            if (this.q == readByte3) {
                                System.out.println("deleteChatTo()");
                                this.r = -1;
                                this.q = -1;
                                this.f44a.d("");
                            }
                            this.h[readByte3] = null;
                            break;
                        case 5:
                            byte readByte5 = this.c.readByte();
                            byte readByte6 = this.c.readByte();
                            byte readByte7 = this.c.readByte();
                            byte readByte8 = this.c.readByte();
                            ac acVar = this.j[readByte5];
                            if (acVar != null) {
                                if (readByte8 != -1) {
                                    if (acVar.c == -1) {
                                        acVar.c = readByte7;
                                        acVar.b((byte) 1);
                                        acVar.e = j.b(readByte7);
                                        acVar.d = j.a(readByte7);
                                    }
                                    ab abVar3 = this.h[readByte8];
                                    abVar3.f = acVar;
                                    abVar3.g = readByte6;
                                    acVar.h[readByte6] = abVar3;
                                    if (abVar3.f == this.i.f) {
                                        if (abVar3.f.d()) {
                                            this.f44a.a(false);
                                            e();
                                        } else if (abVar3.f.c()) {
                                            this.f44a.a(true);
                                        }
                                    }
                                    this.f44a.f91a.a(6, 1, 0, null);
                                    this.f44a.f91a.a(4, 0, 0, abVar3);
                                    if (acVar.c != 1) {
                                        break;
                                    } else {
                                        this.f44a.f91a.a(5, 0, 0, acVar);
                                        break;
                                    }
                                } else {
                                    ab abVar4 = acVar.h[readByte6];
                                    if (abVar4 != null) {
                                        if (this.i.f != null && acVar.b == this.i.f.b && this.i.g != readByte6 && acVar.e() > 0) {
                                            this.f44a.i(">> " + acVar.h[readByte6].d + " left the table.");
                                        }
                                        if (this.i.f != null && acVar.b == this.i.f.b) {
                                            if (this.i.b == abVar4.b) {
                                                g();
                                            } else if (readByte6 < acVar.d) {
                                                if (j.e[acVar.c] == 0 || acVar.e() == 1) {
                                                    g();
                                                } else if (j.e[acVar.c] == 1 && this.f44a.f91a.m != null) {
                                                    ((am) this.f44a.f91a.m).c(readByte6);
                                                }
                                            }
                                        }
                                        if (readByte6 < acVar.d && (j.e[acVar.c] == 0 || acVar.e() == 1)) {
                                            acVar.b((byte) 1);
                                            if (this.i.f != null && this.i.f == acVar && acVar.c()) {
                                                this.f44a.a(true);
                                            }
                                        }
                                        abVar4.f = null;
                                        abVar4.g = -1;
                                        acVar.h[readByte6] = null;
                                        acVar.f[readByte6] = -1;
                                        acVar.a();
                                        this.f44a.f91a.a(6, 0, 0, null);
                                        this.f44a.f91a.a(4, 0, 0, abVar4);
                                        this.f44a.f91a.a(5, 0, 0, this.j[readByte5]);
                                        if (abVar4 != this.i) {
                                            break;
                                        } else {
                                            this.f44a.f91a.a(6, 1, 0, null);
                                            this.f44a.a(false);
                                            break;
                                        }
                                    } else {
                                        this.f44a.i("Table " + ((int) readByte5) + ", seat " + ((int) readByte6) + ": no such player.");
                                        break;
                                    }
                                }
                            } else {
                                this.f44a.i("Table " + ((int) readByte5) + ", no such table.");
                                break;
                            }
                        case 6:
                            byte readByte9 = this.c.readByte();
                            byte readByte10 = this.c.readByte();
                            String readUTF5 = this.c.readUTF();
                            if (readByte10 != -1) {
                                abVar = this.h[readByte10];
                            } else {
                                abVar = null;
                            }
                            if (readByte9 != 2) {
                                if (readByte9 != 4) {
                                    if (readByte9 != 1) {
                                        if (readByte9 == 3) {
                                            if (abVar != null) {
                                                this.f44a.h("[Whisper] [" + abVar.d + "]: " + readUTF5);
                                                break;
                                            } else {
                                                this.f44a.h("[Whisper]: " + readUTF5);
                                                break;
                                            }
                                        } else {
                                            this.f44a.i(">> " + readUTF5);
                                            break;
                                        }
                                    } else if (abVar != null) {
                                        this.f44a.e("[Table] [" + abVar.d + "]: " + readUTF5);
                                        break;
                                    } else {
                                        this.f44a.e("[Table]: " + readUTF5);
                                        break;
                                    }
                                } else if (abVar != null) {
                                    this.f44a.e("[Lobby] [" + abVar.d + "]: " + readUTF5);
                                    break;
                                } else {
                                    this.f44a.e("[Lobby]: " + readUTF5);
                                    break;
                                }
                            } else if (abVar != null) {
                                this.f44a.e("[World] [" + abVar.d + "]: " + readUTF5);
                                break;
                            } else {
                                this.f44a.e("[World]: " + readUTF5);
                                break;
                            }
                        case 23:
                            g();
                            break;
                        case 24:
                            a(readInt);
                            break;
                        case 25:
                            a(readInt);
                            break;
                        case 27:
                            byte readByte11 = this.c.readByte();
                            byte readByte12 = this.c.readByte();
                            byte readByte13 = this.c.readByte();
                            byte readByte14 = this.c.readByte();
                            ac acVar2 = this.j[readByte11];
                            if (acVar2 != null) {
                                ab abVar5 = this.h[readByte14];
                                abVar5.f = acVar2;
                                abVar5.g = readByte12;
                                acVar2.h[readByte12] = abVar5;
                                acVar2.f[readByte12] = readByte13;
                                this.f44a.f91a.a(4, 0, 0, abVar5);
                                this.f44a.f91a.a(5, 0, 0, this.j[readByte11]);
                                if (this.i.f != null && acVar2.b == this.i.f.b) {
                                    this.f44a.i(">> " + acVar2.h[readByte12].d + " is watching " + acVar2.h[readByte13].d + "'s game.");
                                    break;
                                }
                            } else {
                                this.f44a.i(">> Table " + ((int) readByte11) + ", no such table!!!");
                                break;
                            }
                        case 29:
                            synchronized (this.k) {
                                this.f44a.f91a.a(10, 0, 0, null);
                                h();
                            }
                            break;
                        case 30:
                            short readShort2 = this.c.readShort();
                            if (this.f44a.f91a.m != null) {
                                synchronized (this.k) {
                                    this.f44a.f91a.a(11, 0, 0, null);
                                    h();
                                }
                                break;
                            } else {
                                this.c.skipBytes(readShort2 - 6);
                                break;
                            }
                        case 31:
                            synchronized (this.k) {
                                this.f44a.f91a.a(13, 0, 0, null);
                                h();
                            }
                            break;
                        case 81:
                            this.c.readShort();
                            this.s.a();
                            break;
                        case 82:
                            this.c.readShort();
                            this.c.readByte();
                            this.c.readByte();
                            i iVar = new i(2, 2, 7);
                            iVar.a(this.c);
                            j.l = iVar.b[0] == 1;
                            j.m = iVar.b[1] == 1;
                            j.c = iVar.f[0];
                            break;
                        case 85:
                            short readShort3 = this.c.readShort();
                            if (this.f44a.f91a.m != null) {
                                this.c.readByte();
                                this.c.readByte();
                                this.u.a(this.c);
                                int i2 = this.u.d[0];
                                int i3 = this.u.d[1];
                                byte b2 = (byte) this.u.d[2];
                                byte b3 = (byte) this.u.d[3];
                                byte b4 = (byte) this.u.d[4];
                                if (b4 == -1) {
                                    if (i2 != this.i.b) {
                                        break;
                                    } else {
                                        this.f44a.f91a.a(14, b3, b4, null);
                                        this.f44a.a("Unable to join game!", j.d[b2]);
                                        break;
                                    }
                                } else {
                                    this.j[i3].h[b4] = this.h[i2];
                                    this.j[i3].h[b3] = null;
                                    this.j[i3].f[b3] = -1;
                                    this.h[i2].g = b4;
                                    if (this.i.f != null && this.i.f.b == i3) {
                                        this.f44a.f91a.a(14, b3, b4, null);
                                        break;
                                    }
                                }
                            } else {
                                this.c.skipBytes(readShort3 - 6);
                                break;
                            }
                        case 88:
                            this.c.readShort();
                            this.c.readByte();
                            this.c.readByte();
                            this.u.a(this.c);
                            byte b5 = this.u.b[0];
                            int i4 = this.u.d[0];
                            int i5 = this.u.d[1];
                            if (this.h[i4] != null) {
                                if (b5 == 1) {
                                    if (this.t.contains(this.h[i4].d)) {
                                        break;
                                    }
                                } else {
                                    this.u.j();
                                    this.u.a(this.f44a.f91a.getString(R.string.invitee_client_no_support));
                                    a(h.a(44, (byte) 10, (byte) 0, this.u));
                                    break;
                                }
                            }
                            if (i5 != this.i.b || this.i.f != null || this.h[i4].f == null || this.h[i4].f.p != 1 || this.h[i4].f.c != b5) {
                                this.u.j();
                                this.u.a(this.f44a.f91a.getString(R.string.invalid_player_info));
                                a(h.a(44, (byte) 10, (byte) 0, this.u));
                                break;
                            } else {
                                this.f44a.f91a.a(15, i4, b5, null);
                                break;
                            }
                        case 89:
                            this.c.readShort();
                            byte readByte15 = this.c.readByte();
                            this.c.readByte();
                            this.u.a(this.c);
                            int i6 = this.u.d[1];
                            if (readByte15 != 0) {
                                if (readByte15 != 1) {
                                    if (readByte15 != 2) {
                                        if (readByte15 != 10) {
                                            break;
                                        } else {
                                            this.f44a.f(g.a(this.f44a.f91a.getString(R.string.invite_error), this.h[i6].d, this.u.f[0]));
                                            break;
                                        }
                                    } else {
                                        this.f44a.i(">> " + g.a(this.f44a.f91a.getString(R.string.invite_ignore), this.h[i6].d));
                                        break;
                                    }
                                } else {
                                    this.f44a.i(">> " + g.a(this.f44a.f91a.getString(R.string.invite_reject), this.h[i6].d));
                                    break;
                                }
                            } else {
                                try {
                                    this.f44a.i(">> " + g.a(this.f44a.f91a.getString(R.string.invite_accept), this.h[i6].d));
                                    break;
                                } catch (Exception e3) {
                                    break;
                                }
                            }
                        case 92:
                            this.c.readShort();
                            byte readByte16 = this.c.readByte();
                            this.c.readByte();
                            this.u.a(this.c);
                            this.u.h();
                            this.u.i();
                            this.u.j();
                            int d2 = this.u.d();
                            String f2 = this.u.f();
                            ab abVar6 = this.h[d2];
                            if (abVar6 != null && abVar6.d.equals(f2) && readByte16 == 0) {
                                abVar6.c = this.u.c();
                                abVar6.l = this.u.e();
                                abVar6.h = this.u.d();
                                abVar6.j = this.u.f();
                                abVar6.k = this.u.f();
                                break;
                            }
                        case 94:
                            this.c.readShort();
                            this.c.readByte();
                            this.c.readByte();
                            this.u.a(this.c);
                            break;
                        default:
                            b();
                            break;
                    }
                } catch (IOException e4) {
                    c();
                    return;
                }
            }
        }
        this.e = null;
        if (this.f44a != null) {
            this.f44a.c();
        }
    }
}
