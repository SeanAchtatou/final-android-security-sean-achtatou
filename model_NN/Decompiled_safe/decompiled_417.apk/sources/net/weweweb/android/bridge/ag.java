package net.weweweb.android.bridge;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;

/* compiled from: ProGuard */
final class ag implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ MainActivity f35a;

    ag(MainActivity mainActivity) {
        this.f35a = mainActivity;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        if (i == 0) {
            Intent intent = new Intent();
            intent.setClass(this.f35a, SoloGameMenuActivity.class);
            this.f35a.startActivity(intent);
        } else if (i == 1) {
            Intent intent2 = new Intent();
            intent2.setClass(this.f35a, NetGameMenuActivity.class);
            this.f35a.startActivity(intent2);
        } else if (i == 2) {
            Intent intent3 = new Intent();
            intent3.setClass(this.f35a, ReferenceMenuActivity.class);
            this.f35a.startActivity(intent3);
        } else if (i == 3) {
            Intent intent4 = new Intent();
            intent4.setClass(this.f35a, ToolMenuActivity.class);
            this.f35a.startActivity(intent4);
        } else if (i == 4) {
            this.f35a.finish();
            if (BridgeApp.r > 0) {
                w wVar = new w(this.f35a);
                wVar.c();
                wVar.e(BridgeApp.r);
                wVar.a();
            }
            System.exit(0);
        }
    }
}
