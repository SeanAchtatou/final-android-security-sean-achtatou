package net.weweweb.android.bridge;

import a.e;
import java.util.LinkedList;
import java.util.ListIterator;

/* compiled from: ProGuard */
final class z extends e {
    z(int i, String str, ab abVar) {
        this.f4a = i;
        this.b = str;
        this.c = 2;
        this.d = 3;
        this.e = -1;
        this.f = abVar.b;
        this.g = new LinkedList();
        this.g.add(abVar);
    }

    /* access modifiers changed from: package-private */
    public final synchronized ab a(String str) {
        ab abVar;
        ListIterator listIterator = this.g.listIterator(0);
        while (true) {
            if (!listIterator.hasNext()) {
                abVar = null;
                break;
            }
            abVar = (ab) listIterator.next();
            if (abVar.d.equals(str)) {
                break;
            }
        }
        return abVar;
    }
}
