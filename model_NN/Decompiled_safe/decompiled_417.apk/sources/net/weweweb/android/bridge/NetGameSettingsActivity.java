package net.weweweb.android.bridge;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ToggleButton;
import net.weweweb.android.common.App;
import net.weweweb.android.free.bridge.R;

/* compiled from: ProGuard */
public class NetGameSettingsActivity extends Activity implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    App f18a;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f18a = (App) getApplicationContext();
        setContentView((int) R.layout.netgamesettings);
        findViewById(R.id.savePasswordToggleButton).setOnClickListener(this);
        ((ToggleButton) findViewById(R.id.savePasswordToggleButton)).setChecked(BridgeApp.E);
    }

    public void onClick(View view) {
        if (view == findViewById(R.id.savePasswordToggleButton)) {
            boolean isChecked = ((ToggleButton) view).isChecked();
            BridgeApp.E = isChecked;
            if (!isChecked) {
                BridgeApp.F = null;
            }
            this.f18a.a("savePasswordFlag", BridgeApp.E);
        }
    }
}
