package net.weweweb.android.bridge;

import android.content.DialogInterface;

/* compiled from: ProGuard */
final class bt implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TableListActivity f73a;

    bt(TableListActivity tableListActivity) {
        this.f73a = tableListActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        if (this.f73a.f26a.c != null) {
            this.f73a.f26a.c.a();
        }
        if (this.f73a.f26a.k != null) {
            this.f73a.f26a.k.c();
        }
        dialogInterface.dismiss();
    }
}
