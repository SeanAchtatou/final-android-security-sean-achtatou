package net.weweweb.android.bridge;

import a.b;
import a.d;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import net.weweweb.android.free.bridge.R;

/* compiled from: ProGuard */
public class GameViewerActivity extends Activity implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    BridgeApp f15a;
    byte b = 2;
    w c = null;
    String d;
    String e;
    int f;
    boolean g;
    b h = new b();
    y i = null;
    a j = null;
    at k = null;
    ar l = null;
    byte[] m = new byte[4];
    byte[] n = new byte[4];
    TextView[] o = new TextView[4];
    TextView[] p = new TextView[4];
    String[] q = {"↑", "→", "↓", "←"};
    Handler r = new ae(this);
    private String s = null;

    static /* synthetic */ void a(GameViewerActivity gameViewerActivity, byte b2) {
        if (gameViewerActivity.h != null) {
            gameViewerActivity.h.N(b2);
            gameViewerActivity.g = true;
            gameViewerActivity.b();
        }
    }

    private boolean a() {
        String[] split = this.d.split(",");
        if (split.length != 3) {
            return false;
        }
        try {
            if (!this.c.a(Integer.parseInt(split[0]), Integer.parseInt(split[1]), Long.parseLong(split[2]), this.h)) {
                return false;
            }
            return true;
        } catch (Exception e2) {
            return false;
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f15a = (BridgeApp) getApplicationContext();
        setContentView((int) R.layout.gameviewer);
        this.o[0] = (TextView) findViewById(R.id.viewerTopCardZone);
        this.o[1] = (TextView) findViewById(R.id.viewerRightCardZone);
        this.o[2] = (TextView) findViewById(R.id.viewerBottomCardZone);
        this.o[3] = (TextView) findViewById(R.id.viewerLeftCardZone);
        this.p[0] = (TextView) findViewById(R.id.viewerTopPlayCard);
        this.p[1] = (TextView) findViewById(R.id.viewerRightPlayCard);
        this.p[2] = (TextView) findViewById(R.id.viewerBottomPlayCard);
        this.p[3] = (TextView) findViewById(R.id.viewerLeftPlayCard);
        d();
        this.c = new w(this);
        this.c.c();
        this.d = getIntent().getExtras().getString("game_key");
        if (!a()) {
            finish();
            return;
        }
        this.e = this.h.A();
        this.f = this.h.h();
        c();
        b();
        findViewById(R.id.viewerResetButton).setOnClickListener(this);
        findViewById(R.id.viewerBackButton).setOnClickListener(this);
        findViewById(R.id.viewerNextButton).setOnClickListener(this);
        findViewById(R.id.viewerBiddingButton).setOnClickListener(this);
        findViewById(R.id.viewerPlayLogButton).setOnClickListener(this);
        findViewById(R.id.viewerDoubleDummyButton).setOnClickListener(this);
        findViewById(R.id.viewerRotateButton).setOnClickListener(this);
        findViewById(R.id.viewerPlayButton).setOnClickListener(this);
        findViewById(R.id.viewerRobotButton).setOnClickListener(this);
        findViewById(R.id.viewerAddFavoriteButton).setOnClickListener(this);
        findViewById(R.id.viewerFavoriteButton).setOnClickListener(this);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.c != null) {
            this.c.a();
            this.c = null;
        }
        if (this.j != null) {
            this.j.dismiss();
            this.j = null;
        }
        if (this.k != null) {
            this.k.dismiss();
            this.k = null;
        }
        if (this.l != null) {
            this.l.dismiss();
            this.l = null;
        }
        if (this.i != null) {
            this.i.b = null;
            this.i = null;
        }
        super.onDestroy();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.b.a(byte[], boolean):void
     arg types: [byte[], int]
     candidates:
      a.b.a(int, byte):byte
      a.b.a(byte[], byte):byte
      a.b.a(byte[][], int):byte
      a.b.a(byte, byte):java.lang.String
      a.b.a(byte, int):byte
      a.b.a(int, int):byte
      a.b.a(byte, char):int
      a.b.a(byte, boolean):boolean
      a.b.a(byte[], boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.b.a(int, int):byte
     arg types: [byte, byte]
     candidates:
      a.b.a(int, byte):byte
      a.b.a(byte[], byte):byte
      a.b.a(byte[][], int):byte
      a.b.a(byte, byte):java.lang.String
      a.b.a(byte[], boolean):void
      a.b.a(byte, int):byte
      a.b.a(byte, char):int
      a.b.a(byte, boolean):boolean
      a.b.a(int, int):byte */
    private void b() {
        String str;
        String str2;
        byte h2 = (byte) (((this.h.h() + 4) / 4) - 1);
        for (byte b2 = 0; b2 < 4; b2 = (byte) (b2 + 1)) {
            if (BridgeApp.t || ((this.h.C(b2) && this.h.h() >= 0) || ((this.h.C(this.b) && this.h.A(b2)) || ((this.h.C(b2) && this.h.A(this.b)) || b2 == this.b)))) {
                TextView textView = this.o[this.m[b2]];
                byte[] bArr = this.h.l[b2];
                b.a(bArr, false);
                StringBuilder sb = new StringBuilder();
                sb.append(b.g[b2]);
                for (byte b3 = 3; b3 >= 0; b3 = (byte) (b3 - 1)) {
                    sb.append("\n" + b.e[b3]);
                    int i2 = 0;
                    for (int i3 = 0; i3 < bArr.length; i3++) {
                        if (bArr[i3] != -1 && b.g(bArr[i3]) == b3) {
                            i2++;
                            if (d.b(bArr[i3]) == 'T') {
                                sb.append(" 10");
                            } else {
                                sb.append(" " + Character.toString(d.b(bArr[i3])));
                            }
                        }
                    }
                    if (i2 == 0) {
                        sb.append(" -");
                    }
                }
                textView.setText(BridgeApp.a(sb.toString()));
            } else {
                this.o[this.m[b2]].setText(b.g[b2]);
            }
            try {
                this.p[this.m[b2]].setText(BridgeApp.a(b.f(this.h.a((int) b2, (int) h2))));
            } catch (Exception e2) {
                this.p[this.m[b2]].setText("");
            }
        }
        TextView textView2 = (TextView) findViewById(R.id.viewerTopLeftZone);
        StringBuilder append = new StringBuilder().append("Deck #").append(Integer.toString(this.h.h.b())).append("\n").append("Play: ").append(Byte.toString(b.c(this.h.k()))).append(b.e[b.e(this.h.k())]);
        if (this.h.m() == 97) {
            str = "X";
        } else {
            str = "";
        }
        StringBuilder append2 = append.append(str);
        if (this.h.m() == 98) {
            str2 = "XX";
        } else {
            str2 = "";
        }
        textView2.setText(BridgeApp.a(append2.append(str2).append("\n").append("By: ").append(b.g[this.h.s()]).toString()));
        ((TextView) findViewById(R.id.viewerBottomLeftZone)).setText("\nTurn: " + Integer.toString((this.h.h() + 4) / 4) + "\nNS won: " + Integer.toString(this.h.p[0]) + "\nEW won: " + Integer.toString(this.h.p[1]));
        TextView textView3 = (TextView) findViewById(R.id.viewerCenterCell);
        if (this.h.q[0] + this.h.q[1] > 0) {
            textView3.setText("Claim " + Integer.toString(this.h.q[this.h.s() % 2]));
        } else {
            try {
                textView3.setText(this.q[this.m[this.h.j()]]);
            } catch (Exception e3) {
                textView3.setText("");
            }
        }
        if (this.h != null) {
            if (this.h.h() == -1) {
                findViewById(R.id.viewerBackButton).setEnabled(false);
                findViewById(R.id.viewerResetButton).setEnabled(false);
            } else {
                findViewById(R.id.viewerBackButton).setEnabled(true);
                findViewById(R.id.viewerResetButton).setEnabled(true);
            }
            if (this.h.K() || this.g) {
                findViewById(R.id.viewerNextButton).setEnabled(false);
            } else {
                findViewById(R.id.viewerNextButton).setEnabled(true);
            }
            if (this.h.K() || this.h.q[0] + this.h.q[1] > 0) {
                findViewById(R.id.viewerPlayButton).setEnabled(false);
                findViewById(R.id.viewerRobotButton).setEnabled(false);
            } else {
                findViewById(R.id.viewerPlayButton).setEnabled(true);
                findViewById(R.id.viewerRobotButton).setEnabled(true);
            }
            if (this.s == null) {
                findViewById(R.id.viewerFavoriteButton).setEnabled(false);
            } else {
                findViewById(R.id.viewerFavoriteButton).setEnabled(true);
            }
            if (this.h.h() == -1 || this.h.K()) {
                findViewById(R.id.viewerAddFavoriteButton).setEnabled(false);
            } else {
                findViewById(R.id.viewerAddFavoriteButton).setEnabled(true);
            }
        }
    }

    private void c() {
        while (this.h.h() >= 0) {
            this.h.S();
        }
        this.f = -1;
        this.g = false;
        findViewById(R.id.viewerNextButton).setEnabled(true);
        findViewById(R.id.viewerBackButton).setEnabled(false);
    }

    private void d() {
        for (int i2 = 0; i2 < 4; i2++) {
            this.m[i2] = (byte) (((i2 + 6) - this.b) % 4);
            this.n[i2] = (byte) (((i2 + 6) + this.b) % 4);
        }
    }

    public void onClick(View view) {
        boolean z;
        int i2;
        if (view == findViewById(R.id.viewerResetButton)) {
            c();
            b();
        } else if (view == findViewById(R.id.viewerBackButton)) {
            if (this.h.q[0] + this.h.q[1] == 0) {
                this.h.S();
            } else {
                byte[] bArr = this.h.q;
                this.h.q[1] = 0;
                bArr[0] = 0;
            }
            if (!this.g) {
                this.f--;
            } else if (this.f == this.h.h()) {
                this.g = false;
            }
            b();
        } else if (view == findViewById(R.id.viewerNextButton)) {
            if (this.h.K()) {
                view.setEnabled(false);
            } else if (this.e.length() / 2 <= this.f + 1) {
                view.setEnabled(false);
            } else {
                this.f++;
                if (this.e.substring(this.f * 2, (this.f * 2) + 1).equals("*")) {
                    System.out.println("Claim:playSeqPtr=" + this.f + ",playSeq=" + this.e);
                    try {
                        i2 = Integer.parseInt(this.e.substring((this.f * 2) + 1, (this.f * 2) + 2), 16);
                    } catch (Exception e2) {
                        i2 = 0;
                    }
                    this.h.q[this.h.s() % 2] = (byte) i2;
                    this.h.q[(this.h.s() + 1) % 2] = (byte) (((13 - this.h.p[this.h.s() % 2]) - this.h.p[(this.h.s() + 1) % 2]) - i2);
                } else {
                    this.h.N(d.a(this.e.substring(this.f * 2, (this.f * 2) + 2)));
                }
                findViewById(R.id.viewerBackButton).setEnabled(true);
                b();
            }
        } else if (view == findViewById(R.id.viewerBiddingButton)) {
            if (this.j == null) {
                this.j = new a(this);
            }
            this.j.a(this.h);
            this.j.show();
        } else if (view == findViewById(R.id.viewerPlayLogButton)) {
            if (this.k == null) {
                this.k = new at(this);
            }
            this.k.a(this.h);
            this.k.show();
        } else if (view == findViewById(R.id.viewerDoubleDummyButton)) {
            if (!BridgeApp.t) {
                z = true;
            } else {
                z = false;
            }
            BridgeApp.t = z;
            this.f15a.a("viewerDoubleDummy", BridgeApp.t);
            b();
        } else if (view == findViewById(R.id.viewerRotateButton)) {
            this.b = b.l(this.b);
            d();
            b();
        } else if (view == findViewById(R.id.viewerPlayButton)) {
            if (this.l == null) {
                this.l = new ar(this);
                for (Button onClickListener : this.l.c) {
                    onClickListener.setOnClickListener(this);
                }
            }
            this.l.a(this.h);
            this.l.show();
        } else if (view == findViewById(R.id.viewerRobotButton)) {
            byte j2 = this.h.j();
            if (j2 != -1) {
                if (this.i == null) {
                    this.i = new y(j2);
                    this.i.a(this.h);
                }
                this.i.a(j2);
                this.i.a(1002, this.r);
                findViewById(R.id.viewerResetButton).setEnabled(false);
                findViewById(R.id.viewerBackButton).setEnabled(false);
                findViewById(R.id.viewerNextButton).setEnabled(false);
                findViewById(R.id.viewerPlayButton).setEnabled(false);
                findViewById(R.id.viewerRobotButton).setEnabled(false);
                findViewById(R.id.viewerAddFavoriteButton).setEnabled(false);
                findViewById(R.id.viewerFavoriteButton).setEnabled(false);
            }
        } else if (view == findViewById(R.id.viewerAddFavoriteButton)) {
            this.s = this.h.A();
            b();
        } else if (view == findViewById(R.id.viewerFavoriteButton)) {
            if (this.s != null) {
                this.h.d(this.s);
                b();
            }
        } else if (this.l != null) {
            for (int i3 = 0; i3 < this.l.c.length; i3++) {
                if (view == this.l.c[i3]) {
                    this.l.dismiss();
                    byte j3 = this.h.j();
                    if (j3 != -1 && this.l.d[i3] != -1 && this.h.k(this.l.d[i3], j3)) {
                        this.h.N(this.l.d[i3]);
                        this.g = true;
                        b();
                        return;
                    }
                    return;
                }
            }
        }
    }
}
