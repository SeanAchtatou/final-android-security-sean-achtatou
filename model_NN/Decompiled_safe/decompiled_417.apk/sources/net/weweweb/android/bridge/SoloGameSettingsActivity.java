package net.weweweb.android.bridge;

import a.g;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.ToggleButton;
import net.weweweb.android.common.App;
import net.weweweb.android.free.bridge.R;

/* compiled from: ProGuard */
public class SoloGameSettingsActivity extends Activity implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    App f24a;
    w b = null;
    String[] c = {"Single Game", "Rubber (2-Games)", "Chicago (4-Deal)", "Duplicate (" + BridgeApp.u + "-boards)"};

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f24a = (App) getApplicationContext();
        setContentView((int) R.layout.sologamesettings);
        this.b = new w(this.f24a);
        this.b.c();
        Spinner spinner = (Spinner) findViewById(R.id.soloGameSettingsDeclererTypeSpinner);
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, 17367048, new String[]{"Human", "Robot"});
        arrayAdapter.setDropDownViewResource(17367049);
        spinner.setAdapter((SpinnerAdapter) arrayAdapter);
        spinner.setSelection(BridgeApp.n ? 1 : 0);
        spinner.setOnItemSelectedListener(new bk(this));
        Spinner spinner2 = (Spinner) findViewById(R.id.soloGameSettingsScoringMethodSpinner);
        ArrayAdapter arrayAdapter2 = new ArrayAdapter(this, 17367048, this.c);
        arrayAdapter2.setDropDownViewResource(17367049);
        spinner2.setAdapter((SpinnerAdapter) arrayAdapter2);
        spinner2.setSelection(BridgeApp.o);
        spinner2.setOnItemSelectedListener(new bl(this));
        Spinner spinner3 = (Spinner) findViewById(R.id.endTurnDelaySpinner);
        ArrayAdapter arrayAdapter3 = new ArrayAdapter(this, 17367048, new String[]{"1 sec.", "2 sec.", "3 sec.", "4 sec.", "5 sec."});
        arrayAdapter3.setDropDownViewResource(17367049);
        spinner3.setAdapter((SpinnerAdapter) arrayAdapter3);
        spinner3.setSelection((BridgeApp.q / 1000) - 1);
        spinner3.setOnItemSelectedListener(new bm(this));
        findViewById(R.id.soloAutoSaveToggleButton).setOnClickListener(this);
        ((ToggleButton) findViewById(R.id.soloAutoSaveToggleButton)).setChecked(BridgeApp.p);
        Spinner spinner4 = (Spinner) findViewById(R.id.autoPurgeSpinner);
        ArrayAdapter arrayAdapter4 = new ArrayAdapter(this, 17367048, new String[]{"disabled", "keep 100", "keep 200", "keep 300", "keep 400"});
        arrayAdapter4.setDropDownViewResource(17367049);
        spinner4.setAdapter((SpinnerAdapter) arrayAdapter4);
        try {
            spinner4.setSelection(g.a(BridgeApp.s, BridgeApp.r));
        } catch (Exception e) {
            spinner4.setSelection(0);
        }
        spinner4.setOnItemSelectedListener(new bn(this));
        Spinner spinner5 = (Spinner) findViewById(R.id.playEngineSpinner);
        ArrayAdapter arrayAdapter5 = new ArrayAdapter(this, 17367048, new String[]{"Classic", "Stable", "Experimental"});
        arrayAdapter5.setDropDownViewResource(17367049);
        spinner5.setAdapter((SpinnerAdapter) arrayAdapter5);
        spinner5.setSelection(BridgeApp.v);
        spinner5.setOnItemSelectedListener(new bo(this));
        findViewById(R.id.soloLuckyHandToggleButton).setOnClickListener(this);
        ((ToggleButton) findViewById(R.id.soloLuckyHandToggleButton)).setChecked(BridgeApp.w);
        findViewById(R.id.soloSoundOnToggleButton).setOnClickListener(this);
        ((ToggleButton) findViewById(R.id.soloSoundOnToggleButton)).setChecked(BridgeApp.x);
        findViewById(R.id.soloConfirmContractToggleButton).setOnClickListener(this);
        ((ToggleButton) findViewById(R.id.soloConfirmContractToggleButton)).setChecked(BridgeApp.y);
        findViewById(R.id.soloGameAutoPlaySingleCardToggleButton).setOnClickListener(this);
        ((ToggleButton) findViewById(R.id.soloGameAutoPlaySingleCardToggleButton)).setChecked(BridgeApp.B);
    }

    public void onClick(View view) {
        if (view == findViewById(R.id.soloAutoSaveToggleButton)) {
            BridgeApp.p = ((ToggleButton) view).isChecked();
            this.f24a.a("soloAutoSave", BridgeApp.p);
        } else if (view == findViewById(R.id.soloLuckyHandToggleButton)) {
            BridgeApp.w = ((ToggleButton) view).isChecked();
            this.f24a.a("luckyHand", BridgeApp.w);
        } else if (view == findViewById(R.id.soloSoundOnToggleButton)) {
            BridgeApp.x = ((ToggleButton) view).isChecked();
            this.f24a.a("soundOn", BridgeApp.x);
        } else if (view == findViewById(R.id.soloConfirmContractToggleButton)) {
            BridgeApp.y = ((ToggleButton) view).isChecked();
            this.f24a.a("confirmContract", BridgeApp.y);
        } else if (view == findViewById(R.id.soloGameAutoPlaySingleCardToggleButton)) {
            BridgeApp.B = ((ToggleButton) view).isChecked();
            this.f24a.a("soloGameAutoPlaySingleCard", BridgeApp.B);
        }
    }

    public void onDestroy() {
        if (this.b != null) {
            this.b.a();
            this.b = null;
        }
        super.onDestroy();
    }
}
