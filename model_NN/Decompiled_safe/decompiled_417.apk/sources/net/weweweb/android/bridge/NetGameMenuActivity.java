package net.weweweb.android.bridge;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import java.util.ArrayList;
import java.util.HashMap;
import net.weweweb.android.free.bridge.R;

/* compiled from: ProGuard */
public class NetGameMenuActivity extends Activity {
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.mainmenu);
        ListView listView = (ListView) findViewById(R.id.mainMenu);
        ArrayList arrayList = new ArrayList();
        HashMap hashMap = new HashMap();
        hashMap.put("icon", Integer.valueOf((int) R.drawable.ic_menu_login));
        hashMap.put("title", "Online Game Login");
        hashMap.put("desc", "Login to the WeWeWeb.NET Game Server.");
        arrayList.add(hashMap);
        HashMap hashMap2 = new HashMap();
        hashMap2.put("icon", Integer.valueOf((int) R.drawable.ic_menu_manage));
        hashMap2.put("title", "Network Game Setting");
        hashMap2.put("desc", "Settings related to the Network Game.");
        arrayList.add(hashMap2);
        HashMap hashMap3 = new HashMap();
        hashMap3.put("icon", Integer.valueOf((int) R.drawable.ic_menu_month));
        hashMap3.put("title", "Monthly Ranking");
        hashMap3.put("desc", "Server's Monthly Bridge result.");
        arrayList.add(hashMap3);
        HashMap hashMap4 = new HashMap();
        hashMap4.put("icon", Integer.valueOf((int) R.drawable.ic_menu_trophy));
        hashMap4.put("title", "Annual Ladder");
        hashMap4.put("desc", "The great players for the year!");
        arrayList.add(hashMap4);
        listView.setAdapter((ListAdapter) new SimpleAdapter(this, arrayList, R.layout.mainmenuitem, new String[]{"icon", "title", "desc"}, new int[]{R.id.mainMenuItemImage, R.id.mainMenuItemTitle, R.id.mainMenuItemDesc}));
        listView.setOnItemClickListener(new al(this));
    }
}
