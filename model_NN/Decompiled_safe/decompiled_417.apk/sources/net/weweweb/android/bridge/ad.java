package net.weweweb.android.bridge;

import a.a;
import a.b;
import android.content.Context;
import java.util.LinkedList;

/* compiled from: ProGuard */
public abstract class ad {

    /* renamed from: a  reason: collision with root package name */
    BridgeApp f32a;
    b b = new b();
    a c = new a();
    y d = new y((byte) 0);
    byte e = BridgeApp.z;
    int f = 0;
    String g = "Robot";
    byte h;
    LinkedList i = new LinkedList();
    public boolean j = false;
    byte[] k = new byte[4];
    byte[] l = new byte[4];
    boolean[] m = new boolean[4];

    public ad(Context context) {
        this.f32a = (BridgeApp) context;
        this.f32a.m = this;
    }

    public void a() {
        if (this.f32a.m == this) {
            this.f32a.m = null;
        }
        this.b = null;
        this.c = null;
        this.d = null;
        if (this.i != null) {
            this.i.clear();
            this.i = null;
        }
    }

    public final String a(byte b2) {
        try {
            if (this.f32a.f != null) {
                if (this.f32a.k.d.i.f.h[b2] != null) {
                    return this.f32a.k.d.i.f.h[b2].d;
                }
                return this.g;
            } else if (b2 == this.e) {
                return "[You]";
            } else {
                return this.g;
            }
        } catch (Exception e2) {
            return "Unknown";
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        for (int i2 = 0; i2 < 4; i2++) {
            this.k[i2] = (byte) (((i2 + 6) - this.e) % 4);
            this.l[i2] = (byte) (((i2 + 6) + this.e) % 4);
        }
    }
}
