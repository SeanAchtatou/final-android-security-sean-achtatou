package net.weweweb.android.bridge;

import a.b;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import java.util.LinkedList;
import net.weweweb.android.common.a;
import net.weweweb.android.free.bridge.R;

/* compiled from: ProGuard */
public class BiddingActivity extends Activity implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    BridgeApp f10a;
    NetGameActivity b;
    b c;
    int d = 0;
    byte e = 1;
    byte f = 1;
    ScrollView g = null;
    TableLayout h = null;
    LinkedList i = new LinkedList();
    byte j = -1;
    Button k = null;
    Button l = null;
    Button m = null;
    Button n = null;
    Button[] o = new Button[7];
    Button[] p = new Button[5];
    AlertDialog q = null;
    private ImageView[] r = new ImageView[13];

    static void a(TableRow tableRow) {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < 4) {
                ((TextView) tableRow.getChildAt(i3)).setText("");
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        if (this.f10a.e != null) {
            this.f10a.e.a();
        }
        if (this.f10a.f11a == this) {
            this.f10a.f11a = null;
        }
        if (this.q != null) {
            this.q.dismiss();
            this.q = null;
        }
        finish();
    }

    private void g() {
        this.k = (Button) findViewById(R.id.btnBidPass);
        this.k.setOnClickListener(this);
        this.l = (Button) findViewById(R.id.btnDouble);
        this.l.setOnClickListener(this);
        this.m = (Button) findViewById(R.id.btnRedouble);
        this.m.setOnClickListener(this);
        this.n = (Button) findViewById(R.id.btnAlert);
        this.n.setOnClickListener(this);
        this.o[0] = (Button) findViewById(R.id.btnBidL1);
        this.o[1] = (Button) findViewById(R.id.btnBidL2);
        this.o[2] = (Button) findViewById(R.id.btnBidL3);
        this.o[3] = (Button) findViewById(R.id.btnBidL4);
        this.o[4] = (Button) findViewById(R.id.btnBidL5);
        this.o[5] = (Button) findViewById(R.id.btnBidL6);
        this.o[6] = (Button) findViewById(R.id.btnBidL7);
        this.p[4] = (Button) findViewById(R.id.btnBidNT);
        this.p[3] = (Button) findViewById(R.id.btnBidS);
        this.p[2] = (Button) findViewById(R.id.btnBidH);
        this.p[1] = (Button) findViewById(R.id.btnBidD);
        this.p[0] = (Button) findViewById(R.id.btnBidC);
        for (int i2 = 0; i2 < 7; i2++) {
            this.o[i2].setOnClickListener(this);
        }
        for (int i3 = 0; i3 < 5; i3++) {
            this.p[i3].setOnClickListener(this);
        }
    }

    /* access modifiers changed from: package-private */
    public final void b() {
        if (this.c.i() == this.f10a.m.e && !((am) this.f10a.m).r) {
            this.k.setEnabled(true);
            for (int i2 = 0; i2 < this.o.length; i2++) {
                if (i2 >= this.e - 1) {
                    this.o[i2].setEnabled(true);
                }
            }
            for (Button enabled : this.p) {
                enabled.setEnabled(true);
            }
            if (this.c.L((byte) 97)) {
                this.l.setEnabled(true);
            }
            if (this.c.L((byte) 98)) {
                this.m.setEnabled(true);
            }
        }
    }

    private TextView a(int i2) {
        if (i2 >= this.i.size() * 4) {
            return null;
        }
        return (TextView) ((TableRow) this.i.get(i2 / 4)).getChildAt(i2 % 4);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: net.weweweb.android.bridge.am.a(byte, java.lang.String, boolean):void
     arg types: [byte, ?[OBJECT, ARRAY], int]
     candidates:
      net.weweweb.android.bridge.am.a(net.weweweb.android.bridge.am, byte, boolean):void
      net.weweweb.android.bridge.am.a(byte, java.lang.String, boolean):void */
    public void onClick(View view) {
        if (view.equals(this.k)) {
            ((am) this.f10a.m).a((byte) 99, (String) null, true);
        } else if (view.equals(this.l)) {
            ((am) this.f10a.m).a((byte) 97, (String) null, true);
        } else if (view.equals(this.m)) {
            ((am) this.f10a.m).a((byte) 98, (String) null, true);
        } else if (!view.equals(this.n)) {
            for (int i2 = 0; i2 < 5; i2++) {
                if (view.equals(this.p[i2])) {
                    ((am) this.f10a.m).a(b.a(((int) this.f) + b.d[i2]), (String) null, true);
                    return;
                }
            }
            for (int i3 = 0; i3 < 7; i3++) {
                if (view.equals(this.o[i3])) {
                    a((byte) (i3 + 1));
                    return;
                }
            }
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f10a = (BridgeApp) getApplicationContext();
        this.b = this.f10a.f;
        this.c = this.f10a.m.b;
        setContentView((int) R.layout.bidding);
        this.f10a.f11a = this;
        if (this.f10a.e != null) {
            this.f10a.e.a();
        }
        if (this.f10a.g != null) {
            this.f10a.g.a();
        }
        if (this.f10a.j != null) {
            this.f10a.j.a();
        }
        this.g = (ScrollView) findViewById(R.id.biddingCellScrollView);
        this.h = new TableLayout(this);
        this.h.setLayoutParams(new TableLayout.LayoutParams((ViewGroup.MarginLayoutParams) new TableLayout.LayoutParams(-1, -2)));
        this.h.setBackgroundColor(-16777216);
        this.g.removeAllViews();
        this.g.addView(this.h);
        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.biddingcardslayout);
        e();
        int width = getWindowManager().getDefaultDisplay().getWidth();
        int i2 = (width - a.q) / 12;
        int i3 = (((width - (i2 * 12)) - a.q) / 2) - 1;
        for (int i4 = 0; i4 < 13; i4++) {
            ImageView imageView = new ImageView(this);
            this.r[i4] = imageView;
            if (this.c.l[this.f10a.m.e][i4] != -1) {
                imageView.setImageResource(a.k[this.c.l[this.f10a.m.e][i4]]);
            } else {
                imageView.setImageBitmap(null);
            }
            imageView.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
            imageView.setPadding((i4 * i2) + i3, 0, 0, 0);
            relativeLayout.addView(imageView, new RelativeLayout.LayoutParams(-2, -2));
        }
        g();
        b((byte) 1);
        a((byte) 1);
        b(4);
        this.h.setStretchAllColumns(true);
        c();
        d();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.bidding_option_menu, menu);
        return true;
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return super.onKeyDown(i2, keyEvent);
        }
        if (((am) this.f10a.m).h == 0) {
            if (this.q == null) {
                this.q = new AlertDialog.Builder(this).setIcon(17301543).setTitle("Confirm Action").setMessage("This will quit the current game, are you sure?").setPositiveButton("Yes", new k(this)).setNegativeButton("No", (DialogInterface.OnClickListener) null).show();
            } else {
                this.q.show();
            }
            return true;
        }
        a();
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.biddingOptionMenuItemMessage /*2131165376*/:
                Intent intent = new Intent();
                intent.setClass(this, MessageActivity.class);
                startActivity(intent);
                return true;
            default:
                return true;
        }
    }

    /* access modifiers changed from: package-private */
    public final void c() {
        this.k.setEnabled(false);
        for (Button enabled : this.o) {
            enabled.setEnabled(false);
        }
        for (Button enabled2 : this.p) {
            enabled2.setEnabled(false);
        }
        this.l.setEnabled(false);
        this.m.setEnabled(false);
        f();
        if (((am) this.f10a.m).p && ((am) this.f10a.m).h == 0) {
            b();
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(int i2, byte b2, boolean z) {
        TextView a2 = a(i2);
        if (a2 != null) {
            if (b2 == -1) {
                a2.setText("");
                return;
            }
            SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
            if (b2 == 99) {
                spannableStringBuilder.append((CharSequence) "Pass");
            } else if (b2 == 97) {
                spannableStringBuilder.append((CharSequence) "X");
            } else if (b2 == 98) {
                spannableStringBuilder.append((CharSequence) "XX");
            } else {
                spannableStringBuilder.append((CharSequence) Byte.toString(b.c(b2)));
                spannableStringBuilder.append((CharSequence) b.e[b.e(b2)]);
                if (b.e(b2) == 1 || b.e(b2) == 2) {
                    spannableStringBuilder.setSpan(new ForegroundColorSpan(-65536), 1, 2, 33);
                }
            }
            if (z) {
                spannableStringBuilder.append((CharSequence) "!");
                spannableStringBuilder.setSpan(new ForegroundColorSpan(-65536), spannableStringBuilder.length() - 1, spannableStringBuilder.length(), 33);
            }
            a2.setText(spannableStringBuilder);
        }
    }

    /* access modifiers changed from: package-private */
    public final void d() {
        String str;
        if (this.c != null) {
            TableRow tableRow = (TableRow) findViewById(R.id.biddingCellsHeader);
            byte b2 = 0;
            while (true) {
                byte b3 = b2;
                if (b3 < 4) {
                    TextView textView = (TextView) tableRow.getChildAt(b3);
                    if (((am) this.f10a.m).b(b3)) {
                        str = this.f10a.m.g;
                    } else if (b3 != this.f10a.m.e) {
                        str = ((am) this.f10a.m).o.h[b3].d;
                    } else if (((am) this.f10a.m).r) {
                        str = "<font color=#00ff00>" + ((am) this.f10a.m).o.h[b3].d + "</font>";
                    } else {
                        str = "<font color=#00ff00>[You]</font>";
                    }
                    textView.setText(Html.fromHtml(b.g[b3] + "<br/>" + str));
                    b2 = (byte) (b3 + 1);
                } else {
                    return;
                }
            }
        }
    }

    private void b(int i2) {
        int childCount = this.h.getChildCount();
        if (childCount > i2) {
            for (int i3 = childCount - 1; i3 >= i2; i3--) {
                TableRow tableRow = (TableRow) this.i.get(i3);
                this.h.removeView(tableRow);
                a(tableRow);
            }
        } else if (childCount < i2) {
            for (int i4 = childCount; i4 < i2; i4++) {
                if (this.i.size() <= i4) {
                    TableRow tableRow2 = new TableRow(this);
                    TableRow.LayoutParams layoutParams = new TableRow.LayoutParams();
                    layoutParams.setMargins(0, 0, 0, 1);
                    for (int i5 = 0; i5 < 4; i5++) {
                        TextView textView = new TextView(this);
                        textView.setText("");
                        textView.setPadding(0, 1, 0, 1);
                        textView.setBackgroundColor(-1);
                        textView.setTextColor(-16777216);
                        textView.setGravity(17);
                        tableRow2.addView(textView, layoutParams);
                    }
                    this.i.add(tableRow2);
                }
                this.h.addView((View) this.i.get(i4));
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void e() {
        TextView textView = (TextView) findViewById(R.id.biddingBoardInfo);
        if (textView != null) {
            textView.setText("Dealer: " + b.g[this.c.h.a()] + "    " + "Vul: " + a.a.g[this.c.h.d()]);
        }
    }

    private void a(byte b2) {
        if (b2 > 0 && b2 <= 7 && b2 >= this.e) {
            this.o[this.f - 1].setTextColor(this.k.getTextColors());
            this.o[b2 - 1].setTextColor(-65536);
            this.f = b2;
        }
    }

    private void b(byte b2) {
        if (b2 <= 7 && b2 > 0) {
            this.e = b2;
            a(b2);
        }
    }

    /* access modifiers changed from: package-private */
    public final void f() {
        int i2 = this.j;
        while (true) {
            i2++;
            if (i2 > this.c.e()) {
                break;
            }
            byte b2 = (byte) i2;
            if (this.h.getChildCount() * 4 <= this.c.e() + 1 + this.c.h.a()) {
                b(this.h.getChildCount() + 1);
            }
            b(this.c.z());
            a(this.c.h.a() + b2, this.c.M(b2), this.c.k[b2]);
        }
        if (!this.c.I()) {
            if (this.h.getChildCount() * 4 <= this.c.e() + 1 + this.c.h.a()) {
                b(this.h.getChildCount() + 1);
            }
            a(this.c.e() + 1 + this.c.h.a()).setText("?");
        }
    }
}
