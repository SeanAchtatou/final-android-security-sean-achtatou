package net.weweweb.android.bridge;

import android.os.Handler;
import android.os.Message;

/* compiled from: ProGuard */
final class c extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ BidRuleActivity f79a;

    c(BidRuleActivity bidRuleActivity) {
        this.f79a = bidRuleActivity;
    }

    public final void handleMessage(Message message) {
        if (message.what == 1) {
            if (this.f79a.b != null) {
                this.f79a.b.dismiss();
            }
            this.f79a.b();
        } else if (message.what == 2 && this.f79a.b != null) {
            this.f79a.b.setProgress(message.arg1);
        }
    }
}
