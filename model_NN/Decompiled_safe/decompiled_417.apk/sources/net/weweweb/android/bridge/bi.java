package net.weweweb.android.bridge;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;

/* compiled from: ProGuard */
final class bi implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SoloGameMenuActivity f63a;

    bi(SoloGameMenuActivity soloGameMenuActivity) {
        this.f63a = soloGameMenuActivity;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        if (i == 0) {
            this.f63a.f23a.h = null;
            this.f63a.f23a.m = new bp(this.f63a.f23a);
            if (BridgeApp.o == 0) {
                ((bp) this.f63a.f23a.m).f();
            }
            Intent intent = new Intent();
            intent.setClass(this.f63a, SoloGameActivity.class);
            this.f63a.startActivity(intent);
        } else if (i == 1) {
            Intent intent2 = new Intent();
            intent2.setClass(this.f63a, SoloGameSettingsActivity.class);
            this.f63a.startActivity(intent2);
        } else if (i == 2) {
            Intent intent3 = new Intent();
            intent3.setClass(this.f63a, BidRuleActivity.class);
            this.f63a.startActivity(intent3);
        }
    }
}
