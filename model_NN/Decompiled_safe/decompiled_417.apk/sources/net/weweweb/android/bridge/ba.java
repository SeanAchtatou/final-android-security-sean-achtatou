package net.weweweb.android.bridge;

import a.c;
import android.content.DialogInterface;
import android.content.Intent;

/* compiled from: ProGuard */
final class ba implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ az f55a;

    ba(az azVar) {
        this.f55a = azVar;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        c a2;
        if (!(i == 0 || (a2 = this.f55a.f53a.e.a(((c) this.f55a.f53a.b.get(this.f55a.f53a.g)).c, ((c) this.f55a.f53a.b.get(this.f55a.f53a.g)).i)) == null)) {
            this.f55a.f53a.f20a.m = new bp(this.f55a.f53a.f20a);
            ((bp) this.f55a.f53a.f20a.m).a(a2, (byte) (i - 1));
            Intent intent = new Intent();
            intent.setClass(this.f55a.f53a, SoloGameActivity.class);
            this.f55a.f53a.startActivity(intent);
        }
        dialogInterface.dismiss();
    }
}
