package net.weweweb.android.bridge;

import a.f;
import a.g;
import a.h;
import a.i;
import a.j;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.util.Log;
import android.widget.EditText;
import java.security.MessageDigest;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.ListIterator;
import net.weweweb.android.free.bridge.R;

/* compiled from: ProGuard */
public final class q {
    static String b = "0.0.0";
    static int g = 4;
    static int p = Color.rgb(0, 0, 0);
    static int q = Color.rgb(0, 255, 0);
    static String r = "#FFFF00";
    static String s = "#99FFFF";
    static String t = "#00BFF3";
    static String u = "#FF0000";
    static int v = 12;
    static int w = 0;

    /* renamed from: a  reason: collision with root package name */
    BridgeApp f91a;
    String[] c = {"damn", "diu", "fuck", "shit", "asshole"};
    aq d = null;
    StringBuilder e = new StringBuilder();
    public LinkedList f = new LinkedList();
    public LinkedList h = new LinkedList();
    int i;
    int j;
    String k;
    String l;
    LoginActivity m;
    String n = null;
    i o = new i(80, 80, 80);

    public q(Context context) {
        this.f91a = (BridgeApp) context;
        try {
            b = this.f91a.getPackageManager().getPackageInfo(this.f91a.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e2) {
            b = "0.0.0";
        }
    }

    public final void a(String str) {
        if (str.startsWith("[")) {
            this.h.add(str);
        } else {
            this.h.add(0, str);
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(ab abVar) {
        this.d.q = abVar.b;
        this.d.r = -1;
        d("To: " + abVar.d);
    }

    /* access modifiers changed from: package-private */
    public final void a(z zVar) {
        this.d.q = -1;
        this.d.r = zVar.f4a;
        d("[" + zVar.b + "]: ");
    }

    public final void b(ab abVar) {
        try {
            synchronized (this.f) {
                if (!this.f.contains(abVar.d)) {
                    if (this.f.size() > g) {
                        c((String) this.f.getLast());
                        this.f.removeLast();
                    }
                    this.f.addFirst(abVar.d);
                    a(abVar.d);
                }
            }
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: package-private */
    public final void c(ab abVar) {
        if (abVar != null) {
            try {
                if (this.f91a.d != null) {
                    this.f91a.d.a(abVar);
                }
            } catch (Exception e2) {
            }
        }
    }

    public final void a() {
        this.d.r = -1;
        this.d.q = -1;
        d("");
    }

    /* access modifiers changed from: package-private */
    public final void b() {
        try {
            Intent intent = new Intent();
            intent.setClass(this.f91a.b, GameRoomActivity.class);
            this.f91a.b.startActivity(intent);
        } catch (Exception e2) {
            try {
                if (this.f91a.k != null) {
                    this.f91a.k.c();
                }
            } catch (Exception e3) {
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(int i2, int i3, String str, String str2) {
        ab abVar = this.d.h[i3];
        if (abVar != null) {
            Context context = null;
            if (this.f91a.e != null) {
                context = this.f91a.e;
            } else if (this.f91a.f11a != null) {
                context = this.f91a.f11a;
            } else if (this.f91a.f != null) {
                context = this.f91a.e;
            } else if (this.f91a.c != null) {
                context = this.f91a.c;
            }
            if (context != null) {
                this.j = i2;
                this.i = i3;
                this.k = str;
                this.l = str2;
                new AlertDialog.Builder(context).setIcon(17301543).setTitle((int) R.string.chat_invitation_dialog).setMessage(g.a(this.f91a.getString(R.string.chat_invitation_msg), abVar.d, str)).setPositiveButton("Accept", new s(this)).setNeutralButton("Reject", new r(this)).setCancelable(false).show();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(int i2, byte b2) {
        this.o.a();
        this.o.a(b2);
        this.o.a(i2);
        this.o.a(this.d.i.b);
        new AlertDialog.Builder(this.f91a.c).setIcon(17301543).setTitle("Confirm Action").setMessage(g.a(this.f91a.getString(R.string.invite_dialog_msg), this.d.h[i2].d, j.d[b2])).setPositiveButton("Accept", new v(this)).setNeutralButton("Reject", new u(this)).setNegativeButton("Ignore", new t(this)).show();
    }

    /* access modifiers changed from: package-private */
    public final void c() {
        if (this.d != null) {
            this.d.b();
        }
        this.d = null;
        this.e.setLength(0);
        this.f.clear();
        this.h.clear();
        this.f91a.a(16, 0, 0, null);
    }

    public final void b(String str) {
        String substring;
        String str2;
        String str3;
        if (str != null && str.charAt(0) == '/') {
            String a2 = g.a(str);
            if (str.indexOf(32) != -1) {
                substring = str.toLowerCase().substring(1, str.indexOf(32));
            } else {
                substring = str.toLowerCase().substring(1, str.length());
            }
            if (substring.equals("join")) {
                String[] split = a2.split(" ", 3);
                if (split.length >= 3) {
                    this.d.s.a(this.d.i.b, split[1], split[2]);
                } else if (split.length == 2) {
                    this.d.s.a(this.d.i.b, split[1], "");
                } else if (split.length == 1) {
                    this.d.s.a(this.d.i.b, "", "");
                }
            } else if (substring.equals("leave")) {
                String[] split2 = a2.split(" ", 3);
                if (split2.length >= 2) {
                    aa aaVar = this.d.s;
                    String str4 = split2[1];
                    if (aaVar.a(str4) == null) {
                        aaVar.f29a.f44a.i("Invalid chat channel!");
                        return;
                    }
                    aaVar.c.a();
                    aaVar.c.a(aaVar.f29a.l);
                    aaVar.c.a(str4);
                    aaVar.f29a.a(h.a(38, (byte) 4, (byte) aaVar.f29a.l, aaVar.c));
                }
            } else if (substring.equals("chatwho")) {
                String[] split3 = a2.split(" ", 3);
                if (split3.length == 1) {
                    aa aaVar2 = this.d.s;
                    if (aaVar2.f29a.i.i > 10) {
                        aaVar2.c.a();
                        aaVar2.c.a(aaVar2.f29a.i.b);
                        aaVar2.f29a.a(h.a(38, (byte) 11, (byte) aaVar2.f29a.l, aaVar2.c.f7a, aaVar2.c.b, aaVar2.c.c, aaVar2.c.d, aaVar2.c.e, aaVar2.c.f));
                        return;
                    }
                    aaVar2.f29a.f44a.f("Insufficient privilege to use this command!");
                    return;
                }
                aa aaVar3 = this.d.s;
                String str5 = split3[1];
                z a3 = aaVar3.a(str5);
                if (a3 != null) {
                    StringBuffer stringBuffer = new StringBuffer("Channel [" + str5 + "] member list:");
                    ListIterator listIterator = a3.g.listIterator(0);
                    while (listIterator.hasNext()) {
                        ab abVar = (ab) listIterator.next();
                        StringBuilder sb = new StringBuilder();
                        if (stringBuffer.indexOf(": ") >= 0) {
                            str2 = ",";
                        } else {
                            str2 = "";
                        }
                        StringBuilder append = sb.append(str2).append(" ");
                        if (abVar.b == a3.f) {
                            str3 = "*";
                        } else {
                            str3 = "";
                        }
                        stringBuffer.append(append.append(str3).append(abVar.d).toString());
                    }
                    stringBuffer.append(".");
                    aaVar3.f29a.f44a.i(stringBuffer.toString());
                    return;
                }
                aaVar3.f29a.f44a.f("Invalid chat channel!");
            } else if (substring.equals("invite")) {
                String[] split4 = a2.split(" ", 4);
                if (split4.length >= 3) {
                    aa aaVar4 = this.d.s;
                    String str6 = split4[1];
                    z a4 = aaVar4.a(split4[2]);
                    if (a4 == null) {
                        aaVar4.f29a.f44a.f("Invalid chat channel!");
                    } else if (a4.a(str6) != null) {
                        aaVar4.f29a.f44a.f("Player is already in chat channel!");
                    } else {
                        ab a5 = aaVar4.f29a.a(str6);
                        if (a5 == null) {
                            aaVar4.f29a.f44a.f("Invalid player!");
                        } else if (a5 == aaVar4.f29a.i) {
                            aaVar4.f29a.f44a.f("You can't invite yourself!");
                        } else {
                            aaVar4.c.a();
                            aaVar4.c.a(a4.f4a);
                            aaVar4.c.a(a5.b);
                            aaVar4.c.a(aaVar4.f29a.i.b);
                            aaVar4.f29a.a(h.a(38, (byte) 7, (byte) aaVar4.f29a.l, aaVar4.c.f7a, aaVar4.c.b, aaVar4.c.c, aaVar4.c.d, aaVar4.c.e, aaVar4.c.f));
                        }
                    }
                } else {
                    f("Invalid format! Usage: /chatinvite <player> <channel>");
                }
            } else if (substring.equals("whisper")) {
                ab a6 = this.d.a(a2.split(" ", 2)[1]);
                if (a6 != null && !a6.equals(this.d.i)) {
                    b(a6);
                    a(a6);
                }
            } else if (this.d.i.i > 99) {
                if (substring.equals("debug")) {
                    String[] split5 = a2.split(" ", 3);
                    try {
                        if (split5.length > 1) {
                            f.e = Integer.parseInt(split5[1]);
                        }
                    } catch (Exception e2) {
                        f("Invalid format! Usage: /debug <leve>");
                    }
                }
                this.d.a(h.a((byte) 0, a2));
            }
        }
    }

    public final void c(String str) {
        this.h.remove(str);
    }

    /* access modifiers changed from: package-private */
    public final void d(ab abVar) {
        if (abVar != null && this.f91a.d != null) {
            try {
                PlayerListActivity playerListActivity = this.f91a.d;
                for (int i2 = 0; i2 < playerListActivity.b.size(); i2++) {
                    if (((String) ((HashMap) playerListActivity.b.get(i2)).get("player")).equals(abVar.d)) {
                        playerListActivity.b.remove(i2);
                        if (playerListActivity.c != null) {
                            playerListActivity.c.notifyDataSetChanged();
                            return;
                        }
                        return;
                    }
                }
            } catch (Exception e2) {
                Log.e("BridgeClient.removePlayerFromList", e2.toString());
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(LoginActivity loginActivity) {
        this.m = loginActivity;
        if (this.d == null) {
            this.d = new aq(this);
        }
        if (!this.d.n) {
            if (this.d.p || this.d.a()) {
                for (int i2 = 0; i2 < 100 && this.d.m == null; i2++) {
                    g.a(500);
                }
                try {
                    aq aqVar = this.d;
                    String trim = ((EditText) this.m.findViewById(R.id.txtUsername)).getText().toString().trim();
                    String str = this.d.m;
                    String trim2 = ((EditText) this.m.findViewById(R.id.txtPassword)).getText().toString().trim();
                    MessageDigest instance = MessageDigest.getInstance("SHA");
                    instance.update(str.getBytes());
                    instance.update(trim2.getBytes());
                    aqVar.a(h.a(trim, instance.digest(), "Android Bridge Free", b, "Checksum not used!"));
                } catch (Exception e2) {
                    this.f91a.b(e2.toString());
                }
            } else {
                this.d.b();
                this.d = null;
                this.f91a.b("Unable to connect to server. Make sure your device is Internet enabled and the firewall has port 7777 opened.");
            }
        }
    }

    public final void a(boolean z) {
        int i2 = 0;
        try {
            BridgeApp bridgeApp = this.f91a;
            if (z) {
                i2 = 1;
            }
            bridgeApp.a(9, i2, 0, this.f91a.i.f);
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(ac acVar) {
        if (acVar != null && this.f91a.i != null) {
            if (acVar.p == 0) {
                this.f91a.i.b(acVar);
            } else {
                this.f91a.i.a(acVar);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void d(String str) {
        String str2;
        if (str == null) {
            str2 = "";
        } else {
            str2 = str;
        }
        if (str2.equals("")) {
            if (this.d.i == null || this.d.i.f == null || this.d.i.f.p != 2) {
                str2 = "[Lobby]";
            } else {
                str2 = "[Table]";
            }
        }
        this.f91a.a(8, 0, 0, str2);
    }

    private void j(String str) {
        synchronized (this.e) {
            if (this.e.length() > 2048) {
                this.e.setLength(1536);
            }
            this.e.insert(0, str + "<br/>");
            this.f91a.a(7, 0, 0, str);
        }
    }

    public final void a(String str, String str2, String str3) {
        j("<font color=" + s + ">[" + str + "] [" + str2 + "]: " + str3 + "</font>");
    }

    public final void e(String str) {
        j(str);
    }

    public final void f(String str) {
        j("<font color=" + u + ">[Error]: " + str + "</font>");
    }

    public final void a(String str, String str2) {
        if (str2 == null || str2.length() == 0) {
            f(str);
        } else {
            j("<font color=" + u + ">[Error] [" + str2 + "]: " + str + "</font>");
        }
    }

    public final void g(String str) {
        try {
            i("[" + j.d[this.d.i.f.c] + "]: " + str);
        } catch (Exception e2) {
        }
    }

    public final void h(String str) {
        j("<font color=" + t + ">" + str + "</font>");
    }

    /* access modifiers changed from: package-private */
    public final void i(String str) {
        j("<font color=" + r + ">" + str + "</font>");
    }

    /* access modifiers changed from: package-private */
    public final void e(ab abVar) {
        try {
            if (this.f91a.d != null) {
                this.f91a.d.a(abVar);
            }
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: package-private */
    public final void b(ac acVar) {
        if (acVar != null && this.f91a.d != null) {
            for (ab abVar : acVar.h) {
                if (abVar != null) {
                    this.f91a.d.a(abVar);
                }
            }
        }
    }
}
