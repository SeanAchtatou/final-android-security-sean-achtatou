package net.weweweb.android.bridge;

import a.a;
import a.b;
import a.c;
import a.f;
import a.g;
import a.h;
import a.i;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;
import android.widget.TableRow;
import android.widget.Toast;
import java.io.IOException;
import net.weweweb.android.free.bridge.R;

/* compiled from: ProGuard */
public final class am extends ad {
    private byte A = 0;
    private String[] B = new String[2];
    aq n = this.f32a.k.d;
    ac o = this.n.i.f;
    boolean p;
    byte q;
    boolean r;
    boolean s;
    boolean t;
    boolean u = true;
    i v = new i(80, 80, 80);
    Handler w = new an(this);
    private byte x;
    private byte y;
    private int z;

    am(Context context) {
        super(context);
        k();
        j();
        b();
        for (int i = 0; i < 4; i++) {
            this.m[i] = false;
        }
        this.b.a(this.c);
        this.b.f1a.b();
        this.b.d();
        this.b.c();
        this.b.h.d = a.a(this.f);
        this.b.h.e = a.b(this.f);
        e();
    }

    /* access modifiers changed from: package-private */
    public final void a(byte b, String str, boolean z2) {
        try {
            if (!this.b.L(b)) {
                this.f32a.k.a("Invalid Bidding!", "Bridge");
                Toast.makeText(this.f32a.f11a, "Error: Invalid bidding!", 0).show();
                return;
            }
            if (this.b != null) {
                this.b.a(b);
            }
            if (this.f32a.f11a != null) {
                this.f32a.f11a.c();
            }
            if (z2) {
                byte v2 = this.b.v();
                this.v.a();
                this.v.a(this.f);
                if (this.b.I()) {
                    this.v.a(this.b.b(0));
                    this.v.a(true);
                    this.b.T(this.b.p());
                    this.v.a(this.b.s());
                    this.b.R(this.b.l());
                    this.v.a(this.b.k());
                    this.b.S(this.b.n());
                    this.v.a(this.b.m());
                    this.n.a(h.a(16, (byte) 1, v2, this.v));
                } else {
                    this.v.a(this.b.b(0));
                    this.v.a(false);
                    this.v.a((byte) 0);
                    this.v.a((byte) 0);
                    this.v.a((byte) 0);
                    this.n.a(h.a(16, (byte) 1, v2, this.v));
                    if (str != null && str.length() > 0) {
                        byte e = this.b.e();
                        this.v.a();
                        this.v.a(e);
                        this.v.a(str);
                        this.n.a(h.a(16, (byte) 8, v2, this.v));
                    }
                }
                if (str != null && !str.equals("")) {
                    this.b.O(this.b.e());
                }
            }
        } catch (Exception e2) {
        }
    }

    private void f() {
        this.v.a();
        i iVar = this.v;
        y yVar = this.d;
        int i = yVar.M + 1;
        yVar.M = i;
        iVar.a(i);
        this.v.a(0);
        this.v.a(this.b.f());
        this.n.a(h.a(16, (byte) 21, this.b.i(), this.v));
    }

    public final void a() {
        this.f32a.f = null;
        super.a();
    }

    private byte d(byte b) {
        for (byte b2 = 0; b2 < this.o.d; b2 = (byte) (b2 + 1)) {
            if (this.o.h[b2] != null && b2 != b) {
                return b2;
            }
        }
        return -1;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.b.a(int, int):byte
     arg types: [int, byte]
     candidates:
      a.b.a(int, byte):byte
      a.b.a(byte[], byte):byte
      a.b.a(byte[][], int):byte
      a.b.a(byte, byte):java.lang.String
      a.b.a(byte[], boolean):void
      a.b.a(byte, int):byte
      a.b.a(byte, char):int
      a.b.a(byte, boolean):boolean
      a.b.a(int, int):byte */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void c() {
        /*
            r11 = this;
            r10 = 4
            r9 = -1
            r8 = 13
            r7 = 1
            r6 = 0
            net.weweweb.android.bridge.aq r0 = r11.n
            java.lang.Object r0 = r0.k
            monitor-enter(r0)
            net.weweweb.android.bridge.aq r1 = r11.n     // Catch:{ IOException -> 0x011c }
            java.io.DataInputStream r1 = r1.c     // Catch:{ IOException -> 0x011c }
            int r1 = r1.readInt()     // Catch:{ IOException -> 0x011c }
            r11.f = r1     // Catch:{ IOException -> 0x011c }
            net.weweweb.android.bridge.aq r1 = r11.n     // Catch:{ IOException -> 0x011c }
            java.io.DataInputStream r1 = r1.c     // Catch:{ IOException -> 0x011c }
            byte r1 = r1.readByte()     // Catch:{ IOException -> 0x011c }
            r11.q = r1     // Catch:{ IOException -> 0x011c }
            net.weweweb.android.bridge.aq r1 = r11.n     // Catch:{ IOException -> 0x011c }
            java.io.DataInputStream r1 = r1.c     // Catch:{ IOException -> 0x011c }
            byte r1 = r1.readByte()     // Catch:{ IOException -> 0x011c }
            r11.h = r1     // Catch:{ IOException -> 0x011c }
            a.b r1 = r11.b     // Catch:{ IOException -> 0x011c }
            net.weweweb.android.bridge.aq r2 = r11.n     // Catch:{ IOException -> 0x011c }
            java.io.DataInputStream r2 = r2.c     // Catch:{ IOException -> 0x011c }
            byte r2 = r2.readByte()     // Catch:{ IOException -> 0x011c }
            r1.P(r2)     // Catch:{ IOException -> 0x011c }
            a.b r1 = r11.b     // Catch:{ IOException -> 0x011c }
            byte r1 = r1.e()     // Catch:{ IOException -> 0x011c }
            if (r1 < 0) goto L_0x006a
            net.weweweb.android.bridge.aq r1 = r11.n     // Catch:{ IOException -> 0x011c }
            java.io.DataInputStream r1 = r1.c     // Catch:{ IOException -> 0x011c }
            a.b r2 = r11.b     // Catch:{ IOException -> 0x011c }
            byte[] r2 = r2.j     // Catch:{ IOException -> 0x011c }
            r3 = 0
            a.b r4 = r11.b     // Catch:{ IOException -> 0x011c }
            byte r4 = r4.i     // Catch:{ IOException -> 0x011c }
            int r4 = r4 + 1
            r1.read(r2, r3, r4)     // Catch:{ IOException -> 0x011c }
            r1 = r6
        L_0x0051:
            a.b r2 = r11.b     // Catch:{ IOException -> 0x011c }
            byte r2 = r2.i     // Catch:{ IOException -> 0x011c }
            int r2 = r2 + 1
            if (r1 >= r2) goto L_0x006a
            a.b r2 = r11.b     // Catch:{ IOException -> 0x011c }
            boolean[] r2 = r2.k     // Catch:{ IOException -> 0x011c }
            net.weweweb.android.bridge.aq r3 = r11.n     // Catch:{ IOException -> 0x011c }
            java.io.DataInputStream r3 = r3.c     // Catch:{ IOException -> 0x011c }
            boolean r3 = r3.readBoolean()     // Catch:{ IOException -> 0x011c }
            r2[r1] = r3     // Catch:{ IOException -> 0x011c }
            int r1 = r1 + 1
            goto L_0x0051
        L_0x006a:
            a.b r1 = r11.b     // Catch:{ IOException -> 0x011c }
            net.weweweb.android.bridge.aq r2 = r11.n     // Catch:{ IOException -> 0x011c }
            java.io.DataInputStream r2 = r2.c     // Catch:{ IOException -> 0x011c }
            byte r2 = r2.readByte()     // Catch:{ IOException -> 0x011c }
            r1.Q(r2)     // Catch:{ IOException -> 0x011c }
            a.b r1 = r11.b     // Catch:{ IOException -> 0x011c }
            byte r1 = r1.h()     // Catch:{ IOException -> 0x011c }
            if (r1 < 0) goto L_0x00b2
            r1 = r6
        L_0x0080:
            if (r1 >= r10) goto L_0x0095
            net.weweweb.android.bridge.aq r2 = r11.n     // Catch:{ IOException -> 0x011c }
            java.io.DataInputStream r2 = r2.c     // Catch:{ IOException -> 0x011c }
            a.b r3 = r11.b     // Catch:{ IOException -> 0x011c }
            byte[] r3 = r3.c(r1)     // Catch:{ IOException -> 0x011c }
            r4 = 0
            r5 = 13
            r2.read(r3, r4, r5)     // Catch:{ IOException -> 0x011c }
            int r1 = r1 + 1
            goto L_0x0080
        L_0x0095:
            net.weweweb.android.bridge.aq r1 = r11.n     // Catch:{ IOException -> 0x011c }
            java.io.DataInputStream r1 = r1.c     // Catch:{ IOException -> 0x011c }
            a.b r2 = r11.b     // Catch:{ IOException -> 0x011c }
            byte[] r2 = r2.o     // Catch:{ IOException -> 0x011c }
            r3 = 0
            r4 = 13
            int r1 = r1.read(r2, r3, r4)     // Catch:{ IOException -> 0x011c }
            if (r1 == r8) goto L_0x00b2
            net.weweweb.android.bridge.aq r1 = r11.n     // Catch:{ IOException -> 0x011c }
            r2 = 2
            java.lang.String r3 = "read problem!!!"
            byte[] r2 = a.h.a(r2, r3)     // Catch:{ IOException -> 0x011c }
            r1.a(r2)     // Catch:{ IOException -> 0x011c }
        L_0x00b2:
            net.weweweb.android.bridge.aq r1 = r11.n     // Catch:{ IOException -> 0x011c }
            java.io.DataInputStream r1 = r1.c     // Catch:{ IOException -> 0x011c }
            byte r1 = r1.readByte()     // Catch:{ IOException -> 0x011c }
            if (r1 < 0) goto L_0x0129
            r2 = r6
        L_0x00bd:
            if (r2 > r1) goto L_0x0129
            java.util.LinkedList r3 = r11.i     // Catch:{ IOException -> 0x011c }
            a.c r4 = new a.c     // Catch:{ IOException -> 0x011c }
            r4.<init>()     // Catch:{ IOException -> 0x011c }
            r3.add(r4)     // Catch:{ IOException -> 0x011c }
            net.weweweb.android.bridge.aq r3 = r11.n     // Catch:{ IOException -> 0x011c }
            java.io.DataInputStream r3 = r3.c     // Catch:{ IOException -> 0x011c }
            int r3 = r3.readInt()     // Catch:{ IOException -> 0x011c }
            r4.f2a = r3     // Catch:{ IOException -> 0x011c }
            net.weweweb.android.bridge.aq r3 = r11.n     // Catch:{ IOException -> 0x011c }
            java.io.DataInputStream r3 = r3.c     // Catch:{ IOException -> 0x011c }
            int r3 = r3.readInt()     // Catch:{ IOException -> 0x011c }
            r4.b = r3     // Catch:{ IOException -> 0x011c }
            net.weweweb.android.bridge.aq r3 = r11.n     // Catch:{ IOException -> 0x011c }
            java.io.DataInputStream r3 = r3.c     // Catch:{ IOException -> 0x011c }
            int r3 = r3.readInt()     // Catch:{ IOException -> 0x011c }
            r4.c = r3     // Catch:{ IOException -> 0x011c }
            net.weweweb.android.bridge.aq r3 = r11.n     // Catch:{ IOException -> 0x011c }
            java.io.DataInputStream r3 = r3.c     // Catch:{ IOException -> 0x011c }
            byte r3 = r3.readByte()     // Catch:{ IOException -> 0x011c }
            r4.d = r3     // Catch:{ IOException -> 0x011c }
            net.weweweb.android.bridge.aq r3 = r11.n     // Catch:{ IOException -> 0x011c }
            java.io.DataInputStream r3 = r3.c     // Catch:{ IOException -> 0x011c }
            byte r3 = r3.readByte()     // Catch:{ IOException -> 0x011c }
            r4.e = r3     // Catch:{ IOException -> 0x011c }
            net.weweweb.android.bridge.aq r3 = r11.n     // Catch:{ IOException -> 0x011c }
            java.io.DataInputStream r3 = r3.c     // Catch:{ IOException -> 0x011c }
            byte r3 = r3.readByte()     // Catch:{ IOException -> 0x011c }
            r4.f = r3     // Catch:{ IOException -> 0x011c }
            net.weweweb.android.bridge.aq r3 = r11.n     // Catch:{ IOException -> 0x011c }
            java.io.DataInputStream r3 = r3.c     // Catch:{ IOException -> 0x011c }
            byte r3 = r3.readByte()     // Catch:{ IOException -> 0x011c }
            r4.g = r3     // Catch:{ IOException -> 0x011c }
            net.weweweb.android.bridge.aq r3 = r11.n     // Catch:{ IOException -> 0x011c }
            java.io.DataInputStream r3 = r3.c     // Catch:{ IOException -> 0x011c }
            byte r3 = r3.readByte()     // Catch:{ IOException -> 0x011c }
            r4.h = r3     // Catch:{ IOException -> 0x011c }
            int r2 = r2 + 1
            goto L_0x00bd
        L_0x011c:
            r1 = move-exception
            net.weweweb.android.bridge.aq r1 = r11.n     // Catch:{ all -> 0x014c }
            r1.c()     // Catch:{ all -> 0x014c }
            net.weweweb.android.bridge.aq r1 = r11.n     // Catch:{ all -> 0x014c }
            r1.f()     // Catch:{ all -> 0x014c }
            monitor-exit(r0)     // Catch:{ all -> 0x014c }
        L_0x0128:
            return
        L_0x0129:
            net.weweweb.android.bridge.aq r1 = r11.n     // Catch:{ all -> 0x014c }
            r1.f()     // Catch:{ all -> 0x014c }
            monitor-exit(r0)     // Catch:{ all -> 0x014c }
            byte r0 = r11.h
            if (r0 != 0) goto L_0x0151
            a.b r0 = r11.b
            byte r0 = r0.i()
            r11.q = r0
            byte r0 = r11.e
            byte r1 = r11.q
            if (r0 != r1) goto L_0x014f
            boolean r0 = r11.r
            if (r0 != 0) goto L_0x014f
            r0 = r7
        L_0x0146:
            r11.p = r0
            r11.l()
            goto L_0x0128
        L_0x014c:
            r1 = move-exception
            monitor-exit(r0)
            throw r1
        L_0x014f:
            r0 = r6
            goto L_0x0146
        L_0x0151:
            a.b r0 = r11.b
            a.b r1 = r11.b
            byte r1 = r1.p()
            r0.T(r1)
            a.b r0 = r11.b
            a.b r1 = r11.b
            byte r1 = r1.l()
            r0.R(r1)
            a.b r0 = r11.b
            a.b r1 = r11.b
            byte r1 = r1.n()
            r0.S(r1)
            a.b r0 = r11.b
            byte r0 = r0.h()
            if (r0 != r9) goto L_0x01ae
            byte r0 = r11.e
            a.b r1 = r11.b
            byte r1 = r1.s()
            boolean r0 = a.b.i(r0, r1)
            if (r0 == 0) goto L_0x0192
            boolean[] r0 = r11.m     // Catch:{ Exception -> 0x0258 }
            byte[] r1 = r11.l     // Catch:{ Exception -> 0x0258 }
            r2 = 0
            byte r1 = r1[r2]     // Catch:{ Exception -> 0x0258 }
            r2 = 1
            r0[r1] = r2     // Catch:{ Exception -> 0x0258 }
        L_0x0192:
            a.b r0 = r11.b
            byte r0 = r0.u()
            r11.q = r0
            byte r0 = r11.e
            byte r1 = r11.q
            if (r0 != r1) goto L_0x01ac
            boolean r0 = r11.r
            if (r0 != 0) goto L_0x01ac
            r0 = r7
        L_0x01a5:
            r11.p = r0
            r11.h()
            goto L_0x0128
        L_0x01ac:
            r0 = r6
            goto L_0x01a5
        L_0x01ae:
            r0 = r6
        L_0x01af:
            a.b r1 = r11.b
            byte r1 = r1.h()
            if (r0 > r1) goto L_0x01dc
            int r1 = r0 / 4
            byte r1 = (byte) r1
            a.b r2 = r11.b
            byte r2 = r2.t(r1)
            int r3 = r0 % 4
            byte r3 = (byte) r3
            a.b r4 = r11.b
            byte[][] r4 = r4.l
            int r5 = r2 + r3
            int r5 = r5 % 4
            r4 = r4[r5]
            a.b r5 = r11.b
            int r2 = r2 + r3
            int r2 = r2 % 4
            byte r1 = r5.a(r2, r1)
            a.d.a(r4, r1)
            int r0 = r0 + 1
            goto L_0x01af
        L_0x01dc:
            r0 = r6
        L_0x01dd:
            if (r0 >= r10) goto L_0x01eb
            a.b r1 = r11.b
            byte[][] r1 = r1.l
            r1 = r1[r0]
            a.b.b(r1)
            int r0 = r0 + 1
            goto L_0x01dd
        L_0x01eb:
            boolean[] r0 = r11.m
            a.b r1 = r11.b
            byte r1 = r1.t()
            r0[r1] = r7
            a.b r0 = r11.b
            byte[] r0 = r0.o
            a.b r1 = r11.b
            byte r1 = r1.h()
            int r1 = r1 / 4
            byte r1 = (byte) r1
            byte r0 = r0[r1]
            if (r0 != r9) goto L_0x0244
            byte r0 = r11.q
            byte r0 = a.b.l(r0)
            r11.q = r0
        L_0x020e:
            byte r0 = r11.e
            a.b r1 = r11.b
            byte r1 = r1.s()
            int r1 = r1 + 2
            int r1 = r1 % 4
            byte r1 = (byte) r1
            if (r0 == r1) goto L_0x0256
            byte r0 = r11.q
            byte r1 = r11.e
            if (r0 == r1) goto L_0x0238
            byte r0 = r11.q
            byte r1 = r11.e
            int r1 = r1 + 2
            int r1 = r1 % 4
            byte r1 = (byte) r1
            if (r0 != r1) goto L_0x0256
            byte r0 = r11.e
            a.b r1 = r11.b
            byte r1 = r1.s()
            if (r0 != r1) goto L_0x0256
        L_0x0238:
            boolean r0 = r11.r
            if (r0 != 0) goto L_0x0256
            r0 = r7
        L_0x023d:
            r11.p = r0
            r11.h()
            goto L_0x0128
        L_0x0244:
            a.b r0 = r11.b
            byte[] r0 = r0.o
            a.b r1 = r11.b
            byte r1 = r1.h()
            int r1 = r1 / 4
            byte r1 = (byte) r1
            byte r0 = r0[r1]
            r11.q = r0
            goto L_0x020e
        L_0x0256:
            r0 = r6
            goto L_0x023d
        L_0x0258:
            r0 = move-exception
            goto L_0x0192
        */
        throw new UnsupportedOperationException("Method not decompiled: net.weweweb.android.bridge.am.c():void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: net.weweweb.android.bridge.am.a(byte, java.lang.String, boolean):void
     arg types: [byte, ?[OBJECT, ARRAY], int]
     candidates:
      net.weweweb.android.bridge.am.a(net.weweweb.android.bridge.am, byte, boolean):void
      net.weweweb.android.bridge.am.a(byte, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: net.weweweb.android.bridge.am.a(byte, boolean):void
     arg types: [byte, int]
     candidates:
      net.weweweb.android.bridge.am.a(byte, byte):void
      net.weweweb.android.bridge.am.a(byte, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: net.weweweb.android.bridge.am.a(byte, java.lang.String, boolean):void
     arg types: [byte, java.lang.String, int]
     candidates:
      net.weweweb.android.bridge.am.a(net.weweweb.android.bridge.am, byte, boolean):void
      net.weweweb.android.bridge.am.a(byte, java.lang.String, boolean):void */
    public final void d() {
        byte readByte;
        byte readByte2;
        String str;
        boolean z2;
        synchronized (this.n.k) {
            try {
                readByte = this.n.c.readByte();
                readByte2 = this.n.c.readByte();
                this.v.a(this.n.c);
                this.v.g();
                this.n.f();
            } catch (IOException e) {
                this.n.c();
                this.n.f();
                return;
            }
        }
        if (readByte == 1) {
            byte c = this.v.c();
            boolean b = this.v.b();
            byte c2 = this.v.c();
            byte c3 = this.v.c();
            byte c4 = this.v.c();
            if (f.e >= 5) {
                Log.d("hdPlay", "play_status=" + 0 + ",bid=" + ((int) c) + ",last_bid=" + b + ",bg.declarer=" + ((int) this.b.s()) + ",contract=" + ((int) c3) + ",contract_x_info=" + ((int) c4));
            }
            if ((readByte2 != this.e || this.r) && (!b(readByte2) || !this.t)) {
                a(c, (String) null, false);
            }
            if (b) {
                this.b.T(c2);
                this.b.R(c3);
                this.b.S(c4);
                if (c3 == 99) {
                    e((byte) 0);
                    return;
                }
                this.h = 1;
                if (b.i(this.e, this.b.s())) {
                    this.m[this.l[0]] = true;
                }
                this.q = this.b.u();
                this.p = this.e == this.q && !this.r;
                if (this.f32a.f11a != null) {
                    this.f32a.f11a.a();
                }
                if (this.t && (b(this.q) || (this.b.C(this.q) && b(b.m(this.q))))) {
                    this.d.a(this.q);
                    this.d.a(1002, this.w);
                }
                i();
                return;
            }
            this.q = b.l(readByte2);
            this.p = this.e == this.q && !this.r;
            if (this.f32a.f11a != null) {
                this.f32a.f11a.b();
            }
            if (this.t && b(this.q)) {
                f();
            }
        } else if (readByte == 2) {
            byte c5 = this.v.c();
            byte c6 = this.v.c();
            if (this.b.t() >= 0 && this.b.t() <= 3) {
                this.m[this.b.t()] = true;
                if (this.b.y() != c5) {
                    if (this.f32a.f != null) {
                        this.f32a.f.c.e[0] = -1;
                    }
                    this.b.N(c5);
                }
                if (c6 == -1) {
                    this.q = b.l(readByte2);
                    this.j = false;
                } else {
                    this.j = true;
                    h();
                    this.d.a(1003, this.w);
                    this.q = c6;
                }
                if (this.b.C(this.e) || ((this.q != this.e && (!b.h(this.e, this.q) || !this.b.A(this.e))) || this.r)) {
                    z2 = false;
                } else {
                    z2 = true;
                }
                this.p = z2;
                h();
                i();
                if (this.b.K()) {
                    byte b2 = 0;
                    for (byte b3 = 0; b3 < 13; b3 = (byte) (b3 + 1)) {
                        if (this.b.o[b3] % 2 == this.b.s() % 2) {
                            b2 = (byte) (b2 + 1);
                        }
                    }
                    e(b2);
                } else if (!this.t) {
                } else {
                    if ((this.b.B(this.q) && b(this.q)) || ((this.b.C(this.q) && b(b.m(this.q))) || (this.b.A(this.q) && b(this.q)))) {
                        this.d.a(this.q);
                        this.d.a(1002, this.w);
                    }
                }
            }
        } else if (readByte == 3) {
            this.v.d();
            this.z = this.v.d();
            this.x = this.v.c();
            this.y = this.v.c();
            this.f32a.k.g(a(readByte2) + " wants to claim " + ((int) this.y) + " out of " + (13 - this.x) + " tricks.");
            if (!this.r) {
                if (this.b.B(this.e)) {
                    this.h = 3;
                    h();
                    if (this.f32a.f != null) {
                        new AlertDialog.Builder(this.f32a.f).setIcon(17301543).setTitle("Claim").setMessage(g.a(this.f32a.getString(R.string.claim_prompt_msg), a(readByte2), Integer.toString(this.y), Integer.toString(13 - this.x))).setPositiveButton("Accept", new ap(this)).setNegativeButton("Reject", new ao(this)).show();
                    } else {
                        a(this.e, false);
                        this.h = 1;
                    }
                }
                if (this.b.B(b.l(readByte2)) && b(b.l(readByte2)) && this.t) {
                    this.d.a(b.l(readByte2));
                    this.d.s = this.y;
                    this.d.a(1004, this.w);
                }
                if (this.b.B(b.o(readByte2)) && b(b.o(readByte2)) && this.t) {
                    y yVar = new y(b.o(readByte2));
                    yVar.a(this.b);
                    yVar.s = this.y;
                    yVar.a(1004, this.w);
                }
            }
        } else if (readByte == 4) {
            this.v.d();
            this.v.d();
            this.v.c();
            this.v.c();
            if (this.v.b()) {
                this.f32a.k.g(a(readByte2) + " accepted the claim.");
                return;
            }
            this.f32a.k.g(a(readByte2) + " rejected the claim.");
            this.h = 1;
            h();
        } else if (readByte == 5) {
            byte b4 = 0;
            for (byte b5 = 0; b5 < this.x; b5 = (byte) (b5 + 1)) {
                if (this.b.o[b5] % 2 == this.b.s() % 2) {
                    b4 = (byte) (b4 + 1);
                }
            }
            e((byte) (b4 + this.y));
        } else if (readByte == 6) {
            byte c7 = this.v.c();
            this.v.a(this.o.i, c7);
            this.v.a(this.o.j, c7);
            g();
        } else if (readByte == 7) {
            byte c8 = this.v.c();
            this.o.i[c8] = this.v.c();
            this.o.j[c8] = this.v.f();
            g();
            if (this.f32a.f != null) {
                Toast.makeText(this.f32a.f, this.o.j[c8] + " is waiting to join...", 0).show();
            }
            h();
        } else if (readByte == 8) {
            byte c9 = this.v.c();
            this.b.O(c9);
            this.f32a.f11a.a(this.b.h.a() + c9, this.b.M(c9), true);
            if (this.o.h[readByte2] != null) {
                str = this.o.h[readByte2].d + " issued alert. " + this.v.f();
            } else {
                str = this.g + " issued alert. " + this.v.f();
            }
            this.f32a.k.g(str);
            if (this.f32a.f != null) {
                this.f32a.f.a();
            }
        } else if (readByte != 21 || this.v.d() != this.d.M) {
        } else {
            if (this.v.c() == 1) {
                this.d.a(this.b.i());
                this.d.a(1001, this.w);
                return;
            }
            String f = this.v.f();
            this.v.f();
            a(b.b(f), this.v.f(), true);
        }
    }

    public final void a(byte b, byte b2) {
        if (f.e >= 5) {
            System.out.println("BridgeCTable.hdWatcherToPlayer(" + ((int) b) + "," + ((int) b2) + ")");
        }
        if (b2 == -1) {
            g();
            return;
        }
        if (this.n.i.g == b2) {
            k();
            b();
        }
        j();
        this.o.i[b2] = ac.k;
        this.o.j[b2] = null;
        g();
        h();
        this.f32a.k.g(this.o.h[b2].d + " joins the game.");
    }

    public final boolean b(byte b) {
        if (b < 0 || b >= this.o.d) {
            return false;
        }
        return this.o.h[b] == null;
    }

    public final void e() {
        boolean z2;
        synchronized (this.n.k) {
            try {
                this.f = this.n.c.readInt();
                this.c.a(this.n.c);
                this.n.f();
            } catch (IOException e) {
                this.n.c();
                this.n.f();
                return;
            }
        }
        this.b.a();
        this.b.b();
        this.b.c();
        this.h = 0;
        if (!this.u || !this.r) {
            l();
        }
        this.d.a(this.b);
        this.d.a();
        this.q = this.b.o();
        for (byte b = 0; b < 4; b = (byte) (b + 1)) {
            if (b == 2) {
                this.m[this.l[2]] = true;
            } else {
                this.m[this.l[b]] = false;
            }
        }
        if (this.f32a.f11a != null) {
            this.f32a.f11a.e();
            this.f32a.f11a.d();
            BiddingActivity biddingActivity = this.f32a.f11a;
            for (int i = 0; i < biddingActivity.i.size(); i++) {
                BiddingActivity.a((TableRow) biddingActivity.i.get(i));
            }
            biddingActivity.j = -1;
            this.f32a.f11a.f();
        }
        if (this.e != this.b.o() || this.r) {
            z2 = false;
        } else {
            z2 = true;
        }
        this.p = z2;
        if (this.t && b(this.q)) {
            f();
        }
        this.u = false;
    }

    private void e(byte b) {
        this.h = 4;
        if (this.f32a.f != null) {
            this.f32a.f.c.e[0] = -1;
        }
        int i = this.f;
        int r2 = this.b.r();
        int q2 = this.b.q();
        byte G = this.b.G();
        byte s2 = this.b.s();
        byte k = this.b.k();
        byte m = this.b.m();
        c cVar = new c();
        cVar.f2a = i;
        cVar.b = r2;
        cVar.c = q2;
        cVar.d = G;
        cVar.e = s2;
        cVar.f = k;
        cVar.g = m;
        cVar.h = b;
        this.i.add(cVar);
        if (this.f32a.g != null) {
            this.f32a.g.a(cVar);
        }
        if (this.f32a.f != null) {
            NetGameActivity netGameActivity = this.f32a.f;
            Intent intent = new Intent();
            intent.setClass(netGameActivity, ScoreBoardActivity.class);
            netGameActivity.startActivity(intent);
        }
    }

    private void g() {
        if (this.r) {
            int i = 0;
            while (i < this.o.d) {
                if (this.o.j[i] == null || !this.n.i.d.equals(this.o.j[i])) {
                    i++;
                } else {
                    return;
                }
            }
        }
    }

    public final void c(byte b) {
        ab abVar;
        boolean z2;
        g.a(5, "QuitGamePlayer(" + ((int) b) + ")");
        if (this.o != null && (abVar = this.o.h[b]) != null) {
            if (this.o.a(abVar.g)) {
                for (byte b2 = 0; b2 < this.o.i.length; b2 = (byte) (b2 + 1)) {
                    if (this.o.j[b2] != null && this.o.j[b2].equals(abVar.d) && this.o.i[b2] == ac.m) {
                        this.o.i[b2] = ac.k;
                        this.o.j[b2] = null;
                    }
                }
                g();
                return;
            }
            if (this.r) {
                this.o.i[b] = ac.k;
                this.o.j[b] = null;
            }
            this.B[b % 2] = null;
            if (d((byte) -1) == b) {
                this.s = this.e == d(b) && !this.r;
                this.t = this.s;
                z2 = true;
            } else {
                z2 = false;
            }
            g.a(5, "QuitGamePlayer#1:firstHumanPlayerQuit=" + z2 + ",firstHumanPlayer=" + this.s + ",cur_player=" + ((int) this.q) + ",status=" + ((int) this.h));
            if (this.h == 0) {
                if (this.q >= 0 && this.t) {
                    if (this.o.h[this.q] == null || this.q == b) {
                        this.d.a(this.q);
                        this.d.a(1001, this.w);
                    }
                }
            } else if (this.h != 1) {
            } else {
                if (this.b.C(this.q)) {
                    if (this.b.s() >= 0 && this.t) {
                        if (this.o.h[this.b.s()] == null || this.b.s() == b) {
                            this.d.a(this.b.t());
                            this.d.a(1002, this.w);
                        }
                    }
                } else if (this.q >= 0 && this.t) {
                    if (this.o.h[this.q] == null || this.q == b) {
                        this.d.a(this.q);
                        this.d.a(1002, this.w);
                    }
                }
            }
        }
    }

    private void h() {
        if (this.f32a.f != null) {
            this.f32a.f.d();
        }
    }

    /* access modifiers changed from: private */
    public void a(byte b, boolean z2) {
        try {
            this.v.a();
            this.v.a(this.f);
            this.v.a(this.z);
            this.v.a(this.x);
            this.v.a(this.y);
            this.v.a(z2);
            this.n.a(h.a(16, (byte) 4, b, this.v));
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: package-private */
    public final void b(byte b, byte b2) {
        if (f.e >= 5) {
            System.out.println("BridgeCTable.sendClaimRequest(" + ((int) b) + "," + ((int) b2) + ")");
        }
        try {
            this.v.a();
            this.v.a(this.f);
            i iVar = this.v;
            int b3 = g.b(1000000);
            this.z = b3;
            iVar.a(b3);
            i iVar2 = this.v;
            byte C = this.b.C();
            this.x = C;
            iVar2.a(C);
            i iVar3 = this.v;
            this.y = b2;
            iVar3.a(b2);
            this.n.a(h.a(16, (byte) 3, b, this.v));
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: package-private */
    public final void c(byte b, byte b2) {
        try {
            byte C = this.b.C();
            this.b.N(b2);
            if (this.b.P()) {
                this.j = true;
            }
            if (this.f32a.f != null) {
                ((GamePlayingView) this.f32a.f.findViewById(R.id.gamePlayingView)).invalidate();
            }
            this.v.a();
            this.v.a(this.f);
            this.v.a(b2);
            this.v.a(this.b.K(C) ? this.b.u(C) : -1);
            this.n.a(h.a(16, (byte) 2, b, this.v));
        } catch (Exception e) {
        }
    }

    private void i() {
        if (this.f32a.f != null) {
            this.f32a.f.b();
        }
    }

    private void j() {
        int i = 0;
        while (true) {
            if (i >= this.o.d) {
                break;
            } else if (this.o.h[i] == null) {
                i++;
            } else if (this.n.i.g == i) {
                this.s = true;
            } else {
                this.s = false;
            }
        }
        this.t = this.s;
    }

    private void k() {
        if (this.n.i.g < this.o.d) {
            this.e = this.n.i.g;
            this.r = false;
            return;
        }
        this.e = this.o.f[this.n.i.g];
        this.r = true;
    }

    private void l() {
        if (this.f32a.f != null) {
            this.f32a.f.e();
        }
    }
}
