package net.weweweb.android.bridge;

import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.AdapterView;

/* compiled from: ProGuard */
final class al implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ NetGameMenuActivity f40a;

    al(NetGameMenuActivity netGameMenuActivity) {
        this.f40a = netGameMenuActivity;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        if (i == 0) {
            Intent intent = new Intent();
            intent.setClass(this.f40a, LoginActivity.class);
            this.f40a.startActivity(intent);
        } else if (i == 1) {
            Intent intent2 = new Intent();
            intent2.setClass(this.f40a, NetGameSettingsActivity.class);
            this.f40a.startActivity(intent2);
        } else if (i == 2) {
            this.f40a.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://servlet.weweweb.net/get-ranking.jsp?game_type=1&query_mode=top")));
        } else if (i == 3) {
            this.f40a.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://servlet.weweweb.net/bridge-player-rank.jsp?detail=Y")));
        }
    }
}
