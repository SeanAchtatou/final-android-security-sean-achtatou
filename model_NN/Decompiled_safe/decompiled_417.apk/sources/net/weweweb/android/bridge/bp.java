package net.weweweb.android.bridge;

import a.a;
import a.b;
import a.c;
import a.g;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Handler;
import android.widget.Toast;

/* compiled from: ProGuard */
public final class bp extends ad {
    static boolean r = false;
    static boolean s = false;
    byte n = 0;
    bj o = null;
    byte p;
    w q = null;
    Handler t = new bq(this);

    static /* synthetic */ void a(bp bpVar, boolean z) {
        if (z) {
            Toast.makeText(bpVar.f32a.h, bpVar.f32a.m.g + " accepted the claim", 0).show();
            bpVar.b.q[bpVar.b.s() % 2] = bpVar.p;
            bpVar.b.q[(bpVar.b.s() + 1) % 2] = (byte) (((13 - bpVar.b.p[0]) - bpVar.b.p[1]) - bpVar.b.q[bpVar.b.s() % 2]);
            bpVar.c((byte) (bpVar.b.p[bpVar.b.s() % 2] + bpVar.p));
            return;
        }
        Toast.makeText(bpVar.f32a.h, bpVar.f32a.m.g + " rejected the claim", 0).show();
        bpVar.h = 1;
    }

    public bp(Context context) {
        super(context);
        this.q = new w(context);
        this.q.c();
        byte b = BridgeApp.o;
        if (this.n == 0) {
            if (b == 1) {
                if (this.o == null) {
                    this.o = new bj();
                }
                this.i.clear();
                this.o.j = this.i;
                this.o.b();
            } else if (b == 2) {
                if (this.o == null) {
                    this.o = new bj();
                }
                this.i.clear();
                this.o.j = this.i;
                this.o.a();
            }
        } else if (this.n == 1) {
            if (b == 0) {
                this.o = null;
                this.i.clear();
            } else if (b == 2) {
                if (this.o == null) {
                    this.o = new bj();
                }
                this.i.clear();
                this.o.j = this.i;
                this.o.a();
            }
        } else if (this.n == 2) {
            if (b == 0) {
                this.o = null;
                this.i.clear();
            } else if (b == 1) {
                if (this.o == null) {
                    this.o = new bj();
                }
                this.i.clear();
                this.o.j = this.i;
                this.o.b();
            }
        }
        this.n = b;
        this.b.a(this.c);
        this.b.f1a.b();
        this.b.d();
        this.b.c();
        k();
        this.b.h.d = a.a(this.f);
        this.b.h.e = a.b(this.f);
        b();
        e();
        if (this.n != 3 && this.n != 1 && this.n != 2) {
            return;
        }
        if (this.b.K()) {
            this.h = 2;
        } else if (this.b.I()) {
            this.h = 1;
            d();
        } else {
            this.h = 0;
            a(true);
        }
    }

    /* access modifiers changed from: package-private */
    public final void b(byte b) {
        a(b, (String) null);
    }

    /* access modifiers changed from: private */
    public void a(byte b, String str) {
        if (this.b.L(b)) {
            j();
            this.b.a(b);
            if (str != null && str.length() > 0) {
                this.b.O(this.b.e());
            }
            if (!this.b.I()) {
                l();
                if (this.f32a.h != null) {
                    SoloGameActivity soloGameActivity = this.f32a.h;
                    if (soloGameActivity.e.b.e() >= 11) {
                        soloGameActivity.i.c();
                    }
                }
                a(false);
            } else if (!BridgeApp.y || this.f32a.h == null) {
                c();
            } else {
                l();
                SoloGameActivity soloGameActivity2 = this.f32a.h;
                new AlertDialog.Builder(soloGameActivity2.f12a.h).setIcon(17301543).setTitle("Confirm Contract").setMessage("The contract is " + b.a(soloGameActivity2.b.l(), soloGameActivity2.b.n()) + (soloGameActivity2.b.l() != 99 ? " by " + b.g[soloGameActivity2.b.p()] : "") + ", are you sure?").setPositiveButton("Yes", new bh(soloGameActivity2)).setNegativeButton("No", new bg(soloGameActivity2)).setCancelable(false).show();
            }
        }
    }

    public final void c() {
        this.b.T(this.b.p());
        this.b.R(this.b.l());
        this.b.S(this.b.n());
        if (this.b.k() != 99) {
            this.h = 1;
            if (this.f32a.h != null) {
                this.f32a.h.g();
            }
            d();
            return;
        }
        c((byte) 0);
    }

    private void h() {
        String str;
        String str2;
        String str3;
        String str4;
        c cVar = new c(null);
        cVar.b = this.b.r();
        cVar.c = this.b.q();
        cVar.d = this.b.G();
        cVar.i = this.b.s;
        cVar.j = new String[4];
        cVar.j[this.e] = "[You]";
        cVar.k = false;
        this.i.add(cVar);
        w wVar = this.q;
        b bVar = this.b;
        if (this.e == 0) {
            str = "[You]";
        } else {
            str = null;
        }
        if (this.e == 1) {
            str2 = "[You]";
        } else {
            str2 = null;
        }
        if (this.e == 2) {
            str3 = "[You]";
        } else {
            str3 = null;
        }
        if (this.e == 3) {
            str4 = "[You]";
        } else {
            str4 = null;
        }
        wVar.a(bVar, str, str2, str3, str4);
    }

    /* access modifiers changed from: private */
    public void i() {
        if (this.f32a.h != null) {
            this.f32a.h.a();
        }
    }

    static b a(b bVar) {
        if (bVar == null) {
            return null;
        }
        a aVar = new a();
        b bVar2 = new b();
        bVar.h.a(aVar);
        bVar.a(bVar2);
        bVar2.a(aVar);
        return bVar2;
    }

    private void j() {
        if (this.f32a.h != null) {
            this.f32a.h.c();
        }
    }

    public final void a() {
        if (this.q != null) {
            g();
            this.q.a();
            this.q = null;
        }
        super.a();
    }

    /* access modifiers changed from: package-private */
    public final void a(boolean z) {
        String[] strArr;
        j();
        if (r || this.f32a.m.e != this.b.i()) {
            if (this.f32a.h != null) {
                strArr = this.f32a.h.z.a(this.b, this.b.i(), this.b.f());
            } else {
                strArr = null;
            }
            if (strArr == null || strArr[0] == null) {
                this.f32a.m.d.a(this.b.i());
                this.f32a.m.d.a(1001, ((bp) this.f32a.m).t);
                return;
            }
            if (strArr[2] != null && strArr[2].length() == 0) {
                strArr[2] = null;
            }
            a(b.b(strArr[0]), strArr[2]);
            if (strArr[2] != null) {
                i();
                Toast.makeText(this.f32a.h, strArr[2], 1).show();
                return;
            }
            return;
        }
        if (z && this.f32a.h != null) {
            this.f32a.h.h();
        }
        if (this.f32a.h != null) {
            this.f32a.h.d();
        }
    }

    /* access modifiers changed from: package-private */
    public final void d() {
        if (s || b.g(this.b.j(), this.e) || ((b.h(this.b.j(), this.e) && this.b.B(this.e)) || ((BridgeApp.n && b.i(this.e, this.b.j()) && this.b.C(this.e)) || (BridgeApp.B && this.b.F() == 1)))) {
            this.f32a.m.d.a(this.b.j());
            this.f32a.m.d.a(1002, ((bp) this.f32a.m).t);
        }
    }

    /* access modifiers changed from: package-private */
    public final void c(byte b) {
        c cVar;
        this.h = 2;
        if (BridgeApp.p && (this.n == 0 || this.n == 1 || this.n == 2)) {
            a(a(this.b), this.e);
        }
        int i = this.f;
        int r2 = this.b.r();
        int q2 = this.b.q();
        byte G = this.b.G();
        byte s2 = this.b.s();
        byte k = this.b.k();
        byte m = this.b.m();
        if (this.n == 3) {
            cVar = a(r2, q2, 1);
            if (cVar == null) {
                cVar = new c();
                cVar.j = new String[4];
                cVar.j[this.e] = "[You]";
                this.i.add(cVar);
            }
        } else {
            cVar = new c();
            this.i.add(cVar);
        }
        cVar.f2a = i;
        cVar.b = r2;
        cVar.c = q2;
        cVar.d = G;
        cVar.e = s2;
        cVar.f = k;
        cVar.g = m;
        cVar.h = b;
        if (this.n == 1 && this.o != null) {
            this.o.f();
        } else if (this.n == 2 && this.o != null) {
            this.o.e();
        } else if (this.n == 3 && this.o != null) {
            this.o.i++;
        }
        if (this.f32a.h != null) {
            this.f32a.h.a(cVar);
        }
        g();
    }

    /* access modifiers changed from: package-private */
    public final c a(int i, int i2, int i3) {
        int i4;
        int i5 = 0;
        if (this.i == null) {
            return null;
        }
        int i6 = 0;
        while (i5 < this.i.size()) {
            c cVar = (c) this.i.get(i5);
            if (cVar == null) {
                return null;
            }
            if (cVar.f() != i || cVar.e() != i2) {
                i4 = i6;
            } else if (i6 == i3) {
                return cVar;
            } else {
                i4 = i6 + 1;
            }
            i5++;
            i6 = i4;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public final void e() {
        c cVar;
        c cVar2;
        c cVar3;
        c cVar4 = null;
        if (BridgeApp.o == 3) {
            this.e = 1;
            this.i = this.q.d(-997);
            this.o = new bj();
            this.o.b = 3;
            this.o.f64a = -997;
            this.o.j = this.i;
            this.o.h = 0;
            this.o.i = 0;
            int[] iArr = new int[64];
            g.b(iArr, 0);
            int i = 0;
            while (true) {
                cVar3 = cVar4;
                if (i >= this.i.size()) {
                    break;
                }
                if (((c) this.i.get(i)).e() > this.o.h) {
                    this.o.h = ((c) this.i.get(i)).e();
                }
                if (!((c) this.i.get(i)).k) {
                    cVar4 = (c) this.i.get(i);
                } else {
                    if (((c) this.i.get(i)).e() > 0 && ((c) this.i.get(i)).e() <= iArr.length) {
                        int e = ((c) this.i.get(i)).e() - 1;
                        iArr[e] = iArr[e] + 1;
                    }
                    cVar4 = cVar3;
                }
                i++;
            }
            for (int i2 = 0; i2 < iArr.length; i2++) {
                if (iArr[i2] != 0 && iArr[i2] == 2) {
                    this.o.i++;
                }
            }
            if (cVar3 != null) {
                this.q.a(cVar3.f(), cVar3.e(), cVar3.i, this.b);
            } else if (this.o.i == 0) {
                this.q.a(-997, 1, this.c);
                this.b.a();
                this.b.b();
                this.b.c();
                this.b.s = System.currentTimeMillis();
                h();
            } else {
                c a2 = a(-997, this.o.i, 1);
                if (a2 != null) {
                    this.q.a(a2.f(), a2.e(), a2.i, this.b);
                }
            }
        } else if (BridgeApp.o == 1) {
            this.e = BridgeApp.z;
            this.i = this.q.d(-999);
            this.o = new bj();
            this.o.b();
            this.o.f64a = -999;
            this.o.j = this.i;
            if (this.i.size() > 0) {
                c cVar5 = (c) this.i.getLast();
                if (!cVar5.k || cVar5.c() == 99) {
                    this.i.remove(cVar5);
                }
                cVar2 = cVar5;
            } else {
                cVar2 = null;
            }
            if (cVar2 == null || cVar2.c() == 99) {
                this.c.f0a = -999;
                if (cVar2 == null) {
                    this.c.b = 1;
                    this.c.e = 0;
                } else {
                    this.c.b = cVar2.e();
                    this.c.e = cVar2.i();
                }
                this.c.f = System.currentTimeMillis();
                this.b.f1a.b();
                this.b.d();
                this.b.c();
                k();
                this.b.h.d = a.a(this.c.b);
                this.b.a();
                this.b.b();
                this.q.a(this.c);
            } else {
                this.q.a(cVar2.f(), cVar2.e(), cVar2.i, this.b);
            }
        } else if (BridgeApp.o == 2) {
            this.e = BridgeApp.z;
            this.i = this.q.d(-998);
            this.o = new bj();
            this.o.a();
            this.o.f64a = -998;
            this.o.j = this.i;
            if (this.i.size() > 0) {
                c cVar6 = (c) this.i.getLast();
                if (!cVar6.k || cVar6.c() == 99) {
                    this.i.remove(cVar6);
                }
                cVar = cVar6;
            } else {
                cVar = null;
            }
            if (cVar == null || cVar.c() == 99) {
                this.c.f0a = -998;
                if (cVar == null) {
                    this.c.b = 1;
                    this.c.d = a.a(this.c.b - 1);
                    this.c.e = a.b(this.c.b);
                } else {
                    this.c.b = cVar.e();
                    this.c.e = cVar.i();
                }
                this.c.f = System.currentTimeMillis();
                this.b.f1a.b();
                this.b.d();
                this.b.c();
                k();
                this.b.a();
                this.b.b();
                this.q.a(this.c);
            } else {
                this.q.a(cVar.f(), cVar.e(), cVar.i, this.b);
            }
        }
        this.d.a(this.b);
        b();
    }

    /* access modifiers changed from: package-private */
    public final void f() {
        this.h = 0;
        this.d.a(this.b);
        this.d.a();
        this.b.a();
        this.b.b();
        this.b.f1a.b();
        this.b.d();
        this.b.c();
        k();
        this.f++;
        this.b.h.d = a.a(this.f);
        this.b.h.e = a.b(this.f);
        c cVar = null;
        if (this.i.size() > 0) {
            cVar = (c) this.i.getLast();
        }
        if (this.n == 1) {
            if (this.o.d()) {
                this.q.b(-999);
                this.o.j.clear();
                this.o.b();
                this.c.f0a = -999;
                this.c.b = 1;
                this.c.f = System.currentTimeMillis();
                this.c.d = a.a(this.c.b);
                this.c.e = 0;
            } else {
                if (cVar == null || cVar.c() == 99) {
                    this.i.remove(cVar);
                } else {
                    this.c.b++;
                }
                this.c.d = a.a(this.c.b);
                this.c.f = System.currentTimeMillis();
                if (this.o.e[0] == 1 && this.o.e[1] == 1) {
                    this.b.h.e = 3;
                } else if (this.o.e[0] == 1) {
                    this.b.h.e = 1;
                } else if (this.o.e[1] == 1) {
                    this.b.h.e = 2;
                } else {
                    this.b.h.e = 0;
                }
            }
            this.q.a(this.c);
        } else if (this.n == 2) {
            if (this.o.c()) {
                this.q.b(-998);
                this.o.j.clear();
                this.o.a();
                this.c.f0a = -998;
                this.c.b = 1;
            } else if (cVar == null || cVar.c() == 99) {
                this.i.remove(cVar);
            } else {
                this.c.b++;
            }
            this.c.f = System.currentTimeMillis();
            this.c.d = a.a(this.c.b - 1);
            this.c.e = a.b(this.c.b);
            this.q.a(this.c);
        } else if (this.n == 3) {
            this.q.a(-997, this.o.i + 1, this.c);
            this.b.c();
            this.b.s = System.currentTimeMillis();
            h();
        }
        if (this.f32a.h != null) {
            this.f32a.h.e();
        }
        a(true);
    }

    /* access modifiers changed from: package-private */
    public final void d(byte b) {
        this.b.N(b);
        if (this.b.P()) {
            this.j = true;
        }
        try {
            this.f32a.h.c.invalidate();
        } catch (Exception e) {
        }
        try {
            this.f32a.h.b();
        } catch (Exception e2) {
        }
        if (this.b.P()) {
            this.d.a(1003, this.t);
        } else {
            d();
        }
    }

    private void k() {
        if (BridgeApp.w && this.e >= 0 && this.e <= 3 && this.b.k(this.e) < 10) {
            byte b = this.e;
            int b2 = g.b(4);
            int i = 1;
            while (true) {
                if (i < 4) {
                    byte b3 = (byte) (((this.e + b2) + i) % 4);
                    if (b3 != this.e && this.b.k(b3) >= 10) {
                        b = b3;
                        break;
                    }
                    i++;
                } else {
                    break;
                }
            }
            if (b != this.e) {
                byte[] bArr = new byte[13];
                g.a(this.b.h.c[b], bArr);
                g.a(this.b.h.c[this.e], this.b.h.c[b]);
                g.a(bArr, this.b.h.c[this.e]);
                this.b.c();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void g() {
        String str;
        String str2;
        String str3;
        String str4;
        String str5;
        String str6;
        String str7;
        String str8;
        String str9;
        String str10;
        String str11;
        String str12;
        if (BridgeApp.o == 3) {
            if (this.b.K()) {
                bj bjVar = this.o;
                if (!(bjVar.i == bjVar.h) && this.b.s == 0) {
                    this.b.s = System.currentTimeMillis();
                }
            } else if (this.b.s == 0) {
                this.b.s = System.currentTimeMillis();
            }
            w wVar = this.q;
            b bVar = this.b;
            if (this.e == 0) {
                str9 = "[You]";
            } else {
                str9 = null;
            }
            if (this.e == 1) {
                str10 = "[You]";
            } else {
                str10 = null;
            }
            if (this.e == 2) {
                str11 = "[You]";
            } else {
                str11 = null;
            }
            if (this.e == 3) {
                str12 = "[You]";
            } else {
                str12 = null;
            }
            wVar.a(bVar, str9, str10, str11, str12);
        } else if (BridgeApp.o == 1) {
            this.b.h.f0a = -999;
            if (this.b.K()) {
                if (!this.o.d() && this.b.s == 0) {
                    this.b.s = System.currentTimeMillis();
                }
            } else if (this.b.s == 0) {
                this.b.s = System.currentTimeMillis();
            }
            if (this.b.k() != 99) {
                w wVar2 = this.q;
                b bVar2 = this.b;
                if (this.e == 0) {
                    str5 = "[You]";
                } else {
                    str5 = null;
                }
                if (this.e == 1) {
                    str6 = "[You]";
                } else {
                    str6 = null;
                }
                if (this.e == 2) {
                    str7 = "[You]";
                } else {
                    str7 = null;
                }
                if (this.e == 3) {
                    str8 = "[You]";
                } else {
                    str8 = null;
                }
                wVar2.a(bVar2, str5, str6, str7, str8);
            }
        } else if (BridgeApp.o == 2) {
            this.b.h.f0a = -998;
            if (this.b.K()) {
                if (!this.o.c() && this.b.s == 0) {
                    this.b.s = System.currentTimeMillis();
                }
            } else if (this.b.s == 0) {
                this.b.s = System.currentTimeMillis();
            }
            if (this.b.k() != 99) {
                w wVar3 = this.q;
                b bVar3 = this.b;
                if (this.e == 0) {
                    str = "[You]";
                } else {
                    str = null;
                }
                if (this.e == 1) {
                    str2 = "[You]";
                } else {
                    str2 = null;
                }
                if (this.e == 2) {
                    str3 = "[You]";
                } else {
                    str3 = null;
                }
                if (this.e == 3) {
                    str4 = "[You]";
                } else {
                    str4 = null;
                }
                wVar3.a(bVar3, str, str2, str3, str4);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(b bVar, byte b) {
        String str;
        String str2;
        String str3;
        String str4 = null;
        bVar.h.f0a = -1000;
        bVar.h.b = this.q.b() + 1;
        bVar.h.f = System.currentTimeMillis();
        bVar.s = bVar.h.f;
        this.q.a(bVar.h);
        w wVar = this.q;
        if (b == 0) {
            str = "[You]";
        } else {
            str = null;
        }
        if (b == 1) {
            str2 = "[You]";
        } else {
            str2 = null;
        }
        if (b == 2) {
            str3 = "[You]";
        } else {
            str3 = null;
        }
        if (b == 3) {
            str4 = "[You]";
        }
        wVar.a(bVar, str, str2, str3, str4);
    }

    /* access modifiers changed from: package-private */
    public final void a(c cVar, byte b) {
        cVar.f2a = 1;
        this.f = 1;
        this.e = b;
        b();
        this.i.clear();
        this.i.add(cVar);
        this.n = 99;
        cVar.b().h.a(this.b.h);
        this.h = 0;
        this.d.a(this.b);
        this.b.c();
        this.b.a();
        this.b.b();
        if (this.f32a.h != null) {
            this.f32a.h.e();
        }
        a(true);
    }

    private void l() {
        if (this.f32a.h != null) {
            this.f32a.h.h();
        }
    }
}
