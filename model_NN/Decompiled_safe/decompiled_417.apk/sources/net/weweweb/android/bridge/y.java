package net.weweweb.android.bridge;

import a.b;
import a.d;
import a.g;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import java.lang.reflect.Array;
import java.util.HashMap;

/* compiled from: ProGuard */
public final class y implements Runnable {
    static String L = "(P|[1-7]([CDHS]|NT)|X|XX)";
    public static int R = -1;
    public static int S = 0;
    public static int T = 1;
    public static int U = 100;
    public static int V = 101;
    public static int W = 200;
    public static int X = 201;
    public static int Y = 202;
    public static int Z = 203;
    public static int aA = 805;
    public static int aB = 806;
    public static int aC = 807;
    public static int aD = 808;
    public static int aE = 809;
    public static int aF = 810;
    public static int aG = 900;
    public static int aH = 901;
    public static int[] aI = {Y, ac, aG, aH, ad, ae, ag, aj, ak, an, ao, aq, ar, au};
    public static int[][] aJ = {new int[]{ax, b.a("2H")}, new int[]{ax, b.a("2S")}};
    public static int[] aK = {aH, ag, au};
    public static int[][] aL = {new int[]{X, 12, 21}, new int[]{Y, 12, 21}, new int[]{ab, 22, -1}, new int[]{ac, 8, 11}};
    public static int[][] aM = {new int[]{Z, 1, 15, 17}, new int[]{Z, 2, 20, 21}, new int[]{Z, 3, 25, 27}};
    public static int[][] aN = {new int[]{ad, b.a("1D"), -1, -1, 4, -1, -1, -1, -1, -1}, new int[]{ad, b.a("1H"), -1, -1, -1, -1, 4, -1, -1, -1}, new int[]{ad, b.a("1S"), -1, -1, -1, -1, -1, -1, 4, -1}, new int[]{aG, b.a("2C"), -1, -1, 4, -1, -1, -1, -1, -1}, new int[]{aG, b.a("2D"), -1, -1, 4, -1, -1, -1, -1, -1}, new int[]{aG, b.a("2H"), -1, -1, -1, -1, 3, -1, -1, -1}, new int[]{aG, b.a("2S"), -1, -1, -1, -1, -1, -1, 3, -1}, new int[]{aG, b.a("3C"), -1, -1, 4, -1, -1, -1, -1, -1}, new int[]{aG, b.a("3D"), -1, -1, 4, -1, -1, -1, -1, -1}, new int[]{aG, b.a("3H"), -1, -1, -1, -1, 3, -1, -1, -1}, new int[]{aG, b.a("3S"), -1, -1, -1, -1, -1, -1, 3, -1}, new int[]{ay, b.a("2D"), -1, -1, -1, -1, 5, -1, -1, -1}, new int[]{ay, b.a("2H"), -1, -1, -1, -1, -1, -1, 5, -1}, new int[]{ax, b.a("2D"), -1, -1, -1, -1, -1, 3, -1, 3}, new int[]{ax, b.a("2H"), -1, -1, -1, -1, 4, -1, -1, -1}, new int[]{ax, b.a("2S"), -1, -1, -1, -1, -1, -1, 4, -1}};
    private static HashMap aO = null;
    public static int aa = 204;
    public static int ab = 205;
    public static int ac = 206;
    public static int ad = 302;
    public static int ae = 303;
    public static int af = 304;
    public static int ag = 305;
    public static int ah = 401;
    public static int ai = 402;
    public static int aj = 501;
    public static int ak = 502;
    public static int al = 503;
    public static int am = 504;
    public static int an = 505;
    public static int ao = 506;
    public static int ap = 508;
    public static int aq = 601;
    public static int ar = 602;
    public static int as = 603;
    public static int at = 604;
    public static int au = 605;
    public static int av = 701;
    public static int aw = 801;
    public static int ax = 802;
    public static int ay = 803;
    public static int az = 804;
    static int t = 0;
    byte A;
    char B;
    String C;
    int[][] D = ((int[][]) Array.newInstance(Integer.TYPE, 4, 4));
    int[][] E = ((int[][]) Array.newInstance(Integer.TYPE, 4, 4));
    int[] F = new int[4];
    int[] G = new int[4];
    int[][] H = ((int[][]) Array.newInstance(Integer.TYPE, 4, 5));
    int[] I = new int[4];
    boolean[] J = new boolean[4];
    String[] K = {"NS1AKQ", "NE1AK", "NE1AKX", "NE1AKXX", "NS4AKXX", "NS2AQJ", "NS2AJT", "NE1AX", "NE3AXX", "NS4AXXX", "NS1KQ", "NE1QX", "NE3QXX", "NS4QXXX", "NS1JT", "NE1JX", "NE1JXX", "NS4JXXX", "NS1T9", "NE2XX", "NE3XXX", "NS4XXXX", "SS1AK", "SS2AQJ", "SS2AJT", "SE1AX", "SS2AXX", "SS1KQ", "SS2KJT", "SE1KX", "SE3KXX", "SS4KXXX", "SS1QJ", "SE1QT", "SE3QTX", "SS4QTXX", "SS1JT", "SE1JX", "SE3JXX", "SS4JXXX", "SE2XX", "SE3XXX", "SS4XXXX"};
    int M = 0;
    final String N = "";
    int O = 0;
    Handler P;
    public int[] Q = new int[80];

    /* renamed from: a  reason: collision with root package name */
    byte f98a;
    b b;
    boolean c = false;
    String d = "";
    StringBuffer e = new StringBuffer();
    byte[] f = new byte[52];
    boolean g = false;
    boolean h = false;
    b i = new b();
    byte j;
    int k;
    long l;
    long m;
    long n;
    byte[] o = new byte[52];
    byte[] p = new byte[52];
    int[] q = new int[52];
    int[] r = new int[52];
    public byte s;
    int u = t;
    byte v = 1;
    int w = 0;
    byte[] x = null;
    byte y;
    char z;

    public y(byte b2) {
        this.f98a = b2;
        if (aO == null) {
            aO = new HashMap();
        }
        aO.put("AXX", -8);
        aO.put("AX", -18);
        aO.put("KXX", -10);
        aO.put("KX", -20);
        aO.put("QXX", -9);
        aO.put("QX", -19);
        aO.put("JX", -7);
        aO.put("JXX", -8);
        aO.put("JXXX", -9);
    }

    private void a(String str) {
        this.c = true;
        this.d = str;
    }

    private int a(int i2, int i3) {
        int i4;
        int i5;
        byte[] bArr = new byte[(this.i.g() - this.i.C())];
        this.n++;
        if (this.i.m >= (this.i.g() * 4) - 1 || this.i.m >= (this.k * 4) - 1) {
            return this.i.p[this.j % 2];
        }
        byte j2 = this.i.j();
        byte a2 = a(this.i, bArr);
        int i6 = 0;
        int i7 = i3;
        int i8 = i2;
        while (true) {
            if (i6 >= a2) {
                i4 = i7;
                i5 = i8;
                break;
            }
            i(bArr[i6]);
            int a3 = a(i8, i7);
            this.i.S();
            if (j2 % 2 == this.j % 2) {
                if (a3 > i8) {
                    int i9 = this.i.m + 1;
                    if (a3 > this.q[i9]) {
                        this.p[i9] = this.o[i9];
                        this.o[i9] = bArr[i6];
                        this.q[i9] = a3;
                    }
                    if (a3 >= i7) {
                        i4 = i7;
                        i5 = a3;
                        break;
                    }
                    i8 = a3;
                } else {
                    continue;
                }
            } else if (a3 < i7) {
                int i10 = this.i.m + 1;
                if (a3 < this.r[i10]) {
                    this.p[i10] = this.o[i10];
                    this.o[i10] = bArr[i6];
                    this.r[i10] = a3;
                }
                if (i8 >= a3) {
                    i4 = a3;
                    i5 = i8;
                    break;
                }
                i7 = a3;
            } else {
                continue;
            }
            i6++;
        }
        if (j2 % 2 == this.j % 2) {
            return i5;
        }
        return i4;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.b.a(byte, boolean):boolean
     arg types: [byte, int]
     candidates:
      a.b.a(int, byte):byte
      a.b.a(byte[], byte):byte
      a.b.a(byte[][], int):byte
      a.b.a(byte, byte):java.lang.String
      a.b.a(byte[], boolean):void
      a.b.a(byte, int):byte
      a.b.a(int, int):byte
      a.b.a(byte, char):int
      a.b.a(byte, boolean):boolean */
    private byte b() {
        byte[] bArr;
        byte w2 = this.b.w();
        byte x2 = this.b.x();
        if (w2 == -1) {
            return 99;
        }
        byte e2 = b.e(w2);
        if (b.i(this.f98a, x2)) {
            if (this.f98a != x2 && !b.D(w2)) {
                if (b.G(w2)) {
                    if (this.b.a(this.f98a, true) && this.b.L(b.a("3NT"))) {
                        return b.a("3NT");
                    }
                } else if (a(this.f98a, e2)) {
                    if (b.E(e2) && this.b.L(b.a(4, e2))) {
                        return b.a(4, e2);
                    }
                    if (this.b.L(b.a(5, e2))) {
                        return b.a(5, e2);
                    }
                } else if (d(this.f98a, e2)) {
                    if (b.E(e2) && this.b.L(b.a(4, e2))) {
                        return b.a(4, e2);
                    }
                    if (this.b.L(b.a(5, e2))) {
                        return b.a(5, e2);
                    }
                } else if (f(this.f98a) != -1) {
                    return e(f(this.f98a));
                } else {
                    if (!this.b.a(this.f98a, true)) {
                        byte[] d2 = d(this.f98a);
                        if (d2 == null) {
                            return e(this.b.a(this.f98a, 1));
                        }
                        if (b.E(d2[0]) && this.b.L(b.a(4, d2[0]))) {
                            return b.a(4, d2[0]);
                        }
                        if (this.b.L(b.a(5, d2[0]))) {
                            return b.a(5, d2[0]);
                        }
                    } else if (this.b.L(b.a("3NT"))) {
                        return b.a("3NT");
                    }
                }
            }
        } else if (!b.D(w2)) {
            byte[] b2 = b(this.f98a);
            if (b2 == null) {
                byte b3 = this.f98a;
                byte[] bArr2 = new byte[4];
                int i2 = 0;
                for (byte b4 = 3; b4 >= 0; b4 = (byte) (b4 - 1)) {
                    if (d(b3, b4)) {
                        bArr2[i2] = b4;
                        i2++;
                    }
                }
                if (i2 == 0) {
                    bArr = null;
                } else {
                    bArr = new byte[i2];
                    System.arraycopy(bArr2, 0, bArr, 0, i2);
                }
                if (bArr != null) {
                    if (b.E(bArr[0]) && this.b.L(b.a(4, bArr[0]))) {
                        return b.a(4, bArr[0]);
                    }
                    if (this.b.L(b.a(5, bArr[0]))) {
                        return b.a(5, bArr[0]);
                    }
                } else if (f(this.f98a) != -1) {
                    return e(f(this.f98a));
                } else {
                    if (!this.b.a(this.f98a, true)) {
                        byte[] d3 = d(this.f98a);
                        if (d3 == null) {
                            return e(this.b.a(this.f98a, 1));
                        }
                        if (b.E(d3[0]) && this.b.L(b.a(4, d3[0]))) {
                            return b.a(4, d3[0]);
                        }
                        if (this.b.L(b.a(5, d3[0]))) {
                            return b.a(5, d3[0]);
                        }
                    } else if (this.b.L(b.a("3NT"))) {
                        return b.a("3NT");
                    }
                }
            } else if (b.E(b2[0]) && this.b.L(b.a(4, b2[0]))) {
                return b.a(4, b2[0]);
            } else {
                if (this.b.L(b.a(5, b2[0]))) {
                    return b.a(5, b2[0]);
                }
            }
        } else if (this.b.L((byte) 97)) {
            return 97;
        }
        return 99;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.b.a(byte, char):int
     arg types: [byte, int]
     candidates:
      a.b.a(int, byte):byte
      a.b.a(byte[], byte):byte
      a.b.a(byte[][], int):byte
      a.b.a(byte, byte):java.lang.String
      a.b.a(byte[], boolean):void
      a.b.a(byte, int):byte
      a.b.a(int, int):byte
      a.b.a(byte, boolean):boolean
      a.b.a(byte, char):int */
    private byte a(String str, String str2) {
        if (str.equals("4NT")) {
            if (str2.equals("04123")) {
                if (this.b.a(this.f98a, 'A') == 0 || this.b.a(this.f98a, 'A') == 4) {
                    return b.a("5C");
                }
                if (this.b.a(this.f98a, 'A') == 1) {
                    return b.a("5D");
                }
                if (this.b.a(this.f98a, 'A') == 2) {
                    return b.a("5H");
                }
                if (this.b.a(this.f98a, 'A') == 3) {
                    return b.a("5S");
                }
            }
        } else if (str.equals("5NT")) {
            if (str2.equals("01234")) {
                if (this.b.a(this.f98a, 'K') == 0) {
                    return b.a("6C");
                }
                if (this.b.a(this.f98a, 'K') == 1) {
                    return b.a("6D");
                }
                if (this.b.a(this.f98a, 'K') == 2) {
                    return b.a("6H");
                }
                if (this.b.a(this.f98a, 'K') == 3) {
                    return b.a("6S");
                }
                if (this.b.a(this.f98a, 'K') == 4) {
                    return b.a("6NT");
                }
            }
        } else if (str.equals("4C")) {
            if (str2.equals("04123")) {
                if (this.b.a(this.f98a, 'A') == 0 || this.b.a(this.f98a, 'A') == 4) {
                    return b.a("4D");
                }
                if (this.b.a(this.f98a, 'A') == 1) {
                    return b.a("4H");
                }
                if (this.b.a(this.f98a, 'A') == 2) {
                    return b.a("4S");
                }
                if (this.b.a(this.f98a, 'A') == 3) {
                    return b.a("4NT");
                }
            }
        } else if (str.equals("5C") && str2.equals("04123")) {
            if (this.b.a(this.f98a, 'K') == 0 || this.b.a(this.f98a, 'A') == 4) {
                return b.a("5D");
            }
            if (this.b.a(this.f98a, 'K') == 1) {
                return b.a("5H");
            }
            if (this.b.a(this.f98a, 'K') == 2) {
                return b.a("5S");
            }
            if (this.b.a(this.f98a, 'K') == 3) {
                return b.a("5NT");
            }
        }
        return 99;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.b.a(byte, char):int
     arg types: [byte, int]
     candidates:
      a.b.a(int, byte):byte
      a.b.a(byte[], byte):byte
      a.b.a(byte[][], int):byte
      a.b.a(byte, byte):java.lang.String
      a.b.a(byte[], boolean):void
      a.b.a(byte, int):byte
      a.b.a(int, int):byte
      a.b.a(byte, boolean):boolean
      a.b.a(byte, char):int */
    private byte c() {
        byte a2;
        int i2;
        byte[] b2 = b(this.f98a);
        if (b2 != null) {
            a2 = b2[0];
        } else {
            a2 = this.b.a(this.f98a, 1);
        }
        if (this.b.b(-1) == b.a("5C")) {
            if (this.b.a(this.f98a, 'A') <= 0) {
                i2 = 4;
            }
            i2 = 0;
        } else if (this.b.b(-1) == b.a("5D")) {
            i2 = 1;
        } else if (this.b.b(-1) == b.a("5H")) {
            i2 = 2;
        } else {
            if (this.b.b(-1) == b.a("5S")) {
                i2 = 3;
            }
            i2 = 0;
        }
        if (this.b.b(0) == 99 || this.b.b(0) == 97) {
            if (i2 + this.b.a(this.f98a, 'A') == 4) {
                return b.a("5NT");
            }
            if (b.e(this.b.b(-1)) == a2) {
                return 99;
            }
            if (this.b.L(b.a(5, a2))) {
                return b.a(5, a2);
            }
            return b.a(6, a2);
        } else if (this.b.L(b.a(5, a2))) {
            return b.a(5, a2);
        } else {
            if (this.b.e(this.f98a, b.e(this.b.b(0))) >= 4) {
                return 97;
            }
            if (this.b.L(b.a(6, a2))) {
                return b.a(6, a2);
            }
            return 99;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.b.a(byte, char, int):int
     arg types: [byte, int, int]
     candidates:
      a.b.a(byte[][], int, int):byte
      a.b.a(byte, byte, byte):int
      a.b.a(boolean, byte, byte):int
      a.b.a(byte[], byte, int):java.lang.String
      a.b.a(byte, byte, int):byte
      a.b.a(byte, char, boolean):boolean
      a.b.a(byte, char, int):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.b.a(byte, boolean):boolean
     arg types: [byte, int]
     candidates:
      a.b.a(int, byte):byte
      a.b.a(byte[], byte):byte
      a.b.a(byte[][], int):byte
      a.b.a(byte, byte):java.lang.String
      a.b.a(byte[], boolean):void
      a.b.a(byte, int):byte
      a.b.a(int, int):byte
      a.b.a(byte, char):int
      a.b.a(byte, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.b.a(byte, char, java.lang.String, int):boolean
     arg types: [byte, int, java.lang.String, int]
     candidates:
      a.b.a(boolean, byte, byte, byte):int
      a.b.a(byte, char, java.lang.String, int):boolean */
    private byte d() {
        boolean z2;
        byte b2;
        byte[] b3 = b(this.f98a);
        if (a(this.b.e() - 1) == ay) {
            if (this.b.b(-1) == b.a("2D") && this.b.b(0) == b.a("2H")) {
                return 97;
            }
            if (this.b.b(-1) == b.a("2H") && this.b.b(0) == b.a("2S")) {
                return 97;
            }
        }
        if (a(this.b.e() - 1) == aB) {
            return c();
        }
        byte m2 = b.m(this.f98a);
        if (this.H[m2][4] + this.H[m2][0] + this.H[m2][1] + this.H[m2][2] + this.H[m2][3] > 0) {
            z2 = true;
        } else {
            z2 = false;
        }
        if (z2) {
            if (this.x[this.y] >= 4 && this.w >= 6 && this.b.L(b.a(1, this.y))) {
                return b.a(1, this.y);
            }
            if (this.x[this.A] >= 4 && this.w >= 6 && this.b.L(b.a(1, this.A))) {
                return b.a(1, this.A);
            }
            if (this.x[this.y] >= 5 && this.w >= 10 && this.b.L(b.a(2, this.y))) {
                return b.a(2, this.y);
            }
            if (this.x[this.A] >= 5 && this.w >= 10 && this.b.L(b.a(2, this.A))) {
                return b.a(2, this.A);
            }
            if (this.w >= 8 && this.w <= 10 && this.b.L(b.a("1NT"))) {
                return b.a("1NT");
            }
        } else if (this.x[this.y] >= 5 && this.w >= 8 && this.b.L(b.a(1, this.y))) {
            return b.a(1, this.y);
        } else {
            if (this.x[this.A] >= 5 && this.w >= 8 && this.b.L(b.a(1, this.A))) {
                return b.a(1, this.A);
            }
            if (this.x[this.y] >= 5 && this.w >= 11 && this.b.L(b.a(2, this.y))) {
                return b.a(2, this.y);
            }
            if (this.x[this.A] >= 5 && this.w >= 11 && this.b.L(b.a(2, this.A))) {
                return b.a(2, this.A);
            }
        }
        if (b3 != null) {
            b2 = b3[0];
        } else if (b(this.f98a, (byte) 3)) {
            b2 = 3;
        } else if (b(this.f98a, (byte) 2)) {
            b2 = 2;
        } else if (b(this.f98a, (byte) 1)) {
            b2 = 1;
        } else if (b(this.f98a, (byte) 0)) {
            b2 = 0;
        } else {
            b2 = -1;
        }
        if (b2 == 3 && this.w + this.b.a(this.f98a, 'S', 321) + this.F[b.m(this.f98a)] >= 25 && this.b.L(b.a(4, (byte) 3))) {
            return b.a(4, (byte) 3);
        }
        if (b2 == 2 && this.w + this.b.a(this.f98a, 'H', 321) + this.F[b.m(this.f98a)] >= 25 && this.b.L(b.a(4, (byte) 2))) {
            return b.a(4, (byte) 2);
        }
        if (b.z(this.b.b(0))) {
            if (b.c(this.b.b(0)) >= 3) {
                if (!b.G(this.b.b(0))) {
                    byte e2 = b.e(this.b.b(0));
                    if (((this.x[e2] >= 4 && this.b.a(this.f98a, b.q(e2), "AKQ", 1)) || this.x[e2] >= 5) && this.w + this.F[b.m(this.f98a)] >= 20) {
                        return 97;
                    }
                } else if (this.w + this.F[b.m(this.f98a)] >= 18) {
                    return 97;
                }
            }
            if (this.v == 1) {
                if (this.b.a(this.f98a, true) && this.w >= 15 && this.w <= 18 && this.b.a(this.f98a, b.q(b.e(this.b.b(0))), "AK", 2) && this.b.L(b.a("1NT"))) {
                    return b.a("1NT");
                }
                if (this.x[this.y] >= 5 && this.w >= 8 && this.w <= 16 && this.b.a(this.f98a, this.z, "AKQJT", 2) && this.b.L(b.a(1, this.y))) {
                    return b.a(1, this.y);
                }
                if (this.y != b.e(this.b.b(0)) && this.x[this.y] >= 5 && this.w >= 10 && this.w <= 16 && this.b.a(this.f98a, this.z, "AKQJ", 2) && this.b.L(b.a(2, this.y))) {
                    return b.a(2, this.y);
                }
                if (this.b.b(-1) == b.a("1H")) {
                    if (this.x[2] >= 3 && this.w >= 11 && this.w <= 12 && this.b.a(this.f98a, 'H', "AKQ", 1)) {
                        return b.a("3H");
                    }
                    if (this.x[2] >= 3 && this.w >= 6 && this.w <= 10 && this.b.a(this.f98a, 'H', "AKQJ", 1)) {
                        return b.a("2H");
                    }
                    if (this.x[2] >= 4 && this.w >= 13) {
                        return b.a("2NT");
                    }
                }
            }
        } else if (b.z(this.b.b(-1))) {
            if (this.b.b(0) == 97) {
            }
        } else if (b.z(this.b.b(-2)) && b.c(this.b.b(-2)) >= 3) {
            if (!b.G(this.b.b(-2))) {
                byte e3 = b.e(this.b.b(-2));
                if (((this.x[e3] >= 4 && this.b.a(this.f98a, b.q(e3), "AKQ", 1)) || this.x[e3] >= 5) && this.w + this.F[b.m(this.f98a)] >= 20) {
                    return 97;
                }
            } else if (this.w + this.F[b.m(this.f98a)] >= 18) {
                return 97;
            }
        }
        if (this.F[b.m(this.f98a)] < 12 || this.w < 13 || b() == 99) {
            return 99;
        }
        return b();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.b.a(byte, char, java.lang.String, int):boolean
     arg types: [byte, int, java.lang.String, int]
     candidates:
      a.b.a(boolean, byte, byte, byte):int
      a.b.a(byte, char, java.lang.String, int):boolean */
    private byte e() {
        if (this.w >= 8 && this.w <= 10 && this.x[0] >= 7 && this.b.a(this.f98a, 'C', "AK", 2)) {
            return b.a("3C");
        }
        if (this.w >= 8 && this.w <= 10 && this.x[1] >= 7 && this.b.a(this.f98a, 'D', "AK", 2)) {
            return b.a("3D");
        }
        if (this.w >= 8 && this.w <= 10 && this.x[2] >= 7 && this.b.a(this.f98a, 'H', "AK", 2)) {
            return b.a("3H");
        }
        if (this.w >= 8 && this.w <= 10 && this.x[3] >= 7 && this.b.a(this.f98a, 'S', "AK", 2)) {
            return b.a("3S");
        }
        if (this.w < 8 || this.w > 14) {
            return ((this.w < 15 || this.w < 17 || !this.b.a(this.f98a, true)) && this.w < 18) ? (byte) 99 : 97;
        }
        if (this.x[0] >= 5 && this.x[1] >= 5) {
            a("Double minor suits.");
            return b.a("2NT");
        } else if (this.x[2] >= 5 && this.x[3] >= 5) {
            a("Double major suits.");
            return b.a("2D");
        } else if (this.x[2] >= 5 && (this.x[0] >= 5 || this.x[1] >= 5 || this.x[3] >= 5)) {
            return b.a("2H");
        } else {
            if (this.x[3] >= 5 && (this.x[0] >= 5 || this.x[1] >= 5 || this.x[2] >= 5)) {
                return b.a("2S");
            }
            if (this.x[0] >= 6 || this.x[1] >= 6 || this.x[2] >= 6 || this.x[3] >= 6) {
                a("one-suit hand, unknown suit with 6+.");
                return b.a("2C");
            }
        }
    }

    /* JADX WARN: Type inference failed for: r11v0, types: [net.weweweb.android.bridge.y] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private byte f() {
        /*
            r11 = this;
            r10 = 1
            r9 = 5
            r8 = 4
            r7 = 3
            r6 = 2
            byte r0 = r11.f98a
            java.lang.String r0 = r11.c(r0)
            java.lang.String r1 = "1C"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x00e1
            int r0 = r11.w
            if (r0 > r9) goto L_0x001a
            r0 = 99
        L_0x0019:
            return r0
        L_0x001a:
            int r0 = r11.w
            r1 = 19
            if (r0 < r1) goto L_0x0033
            byte[] r0 = r11.x
            byte r1 = r11.y
            byte r0 = r0[r1]
            if (r0 < r9) goto L_0x0033
            byte r0 = r11.y
            if (r0 == 0) goto L_0x0033
            byte r0 = r11.y
            byte r0 = a.b.a(r6, r0)
            goto L_0x0019
        L_0x0033:
            int r0 = r11.w
            r1 = 19
            if (r0 < r1) goto L_0x004c
            byte[] r0 = r11.x
            byte r1 = r11.y
            byte r0 = r0[r1]
            if (r0 < r9) goto L_0x004c
            byte r0 = r11.y
            if (r0 != 0) goto L_0x004c
            byte r0 = r11.y
            byte r0 = a.b.a(r7, r0)
            goto L_0x0019
        L_0x004c:
            int r0 = r11.w
            r1 = 16
            if (r0 < r1) goto L_0x0073
            int r0 = r11.w
            r1 = 17
            if (r0 > r1) goto L_0x0073
            a.b r0 = r11.b
            byte r1 = r11.f98a
            boolean r0 = r0.v(r1)
            if (r0 != 0) goto L_0x0073
            a.b r0 = r11.b
            byte r1 = r11.f98a
            boolean r0 = r0.a(r1, r10)
            if (r0 == 0) goto L_0x0073
            java.lang.String r0 = "3NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0073:
            int r0 = r11.w
            r1 = 13
            if (r0 < r1) goto L_0x009a
            int r0 = r11.w
            r1 = 15
            if (r0 > r1) goto L_0x009a
            a.b r0 = r11.b
            byte r1 = r11.f98a
            boolean r0 = r0.v(r1)
            if (r0 != 0) goto L_0x009a
            a.b r0 = r11.b
            byte r1 = r11.f98a
            boolean r0 = r0.a(r1, r10)
            if (r0 == 0) goto L_0x009a
            java.lang.String r0 = "2NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x009a:
            byte[] r0 = r11.x
            byte r0 = r0[r10]
            if (r0 < r8) goto L_0x00a8
            java.lang.String r0 = "1D"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x00a8:
            byte[] r0 = r11.x
            byte r0 = r0[r6]
            if (r0 < r8) goto L_0x00b6
            java.lang.String r0 = "1H"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x00b6:
            byte[] r0 = r11.x
            byte r0 = r0[r7]
            if (r0 < r8) goto L_0x00c4
            java.lang.String r0 = "1S"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x00c4:
            int r0 = r11.w
            r1 = 11
            if (r0 < r1) goto L_0x00d9
            byte[] r0 = r11.x
            r1 = 0
            byte r0 = r0[r1]
            if (r0 < r8) goto L_0x00d9
            java.lang.String r0 = "2C"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x00d9:
            java.lang.String r0 = "1NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x00e1:
            java.lang.String r1 = "1D"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x01b6
            int r0 = r11.w
            if (r0 > r9) goto L_0x00f1
            r0 = 99
            goto L_0x0019
        L_0x00f1:
            int r0 = r11.w
            r1 = 19
            if (r0 < r1) goto L_0x010f
            byte[] r0 = r11.x
            byte r1 = r11.y
            byte r0 = r0[r1]
            if (r0 < r9) goto L_0x010f
            byte r0 = r11.y
            if (r0 == r6) goto L_0x0107
            byte r0 = r11.y
            if (r0 != r7) goto L_0x010f
        L_0x0107:
            byte r0 = r11.y
            byte r0 = a.b.a(r6, r0)
            goto L_0x0019
        L_0x010f:
            int r0 = r11.w
            r1 = 19
            if (r0 < r1) goto L_0x012d
            byte[] r0 = r11.x
            byte r1 = r11.y
            byte r0 = r0[r1]
            if (r0 < r9) goto L_0x012d
            byte r0 = r11.y
            if (r0 == 0) goto L_0x0125
            byte r0 = r11.y
            if (r0 != r10) goto L_0x012d
        L_0x0125:
            byte r0 = r11.y
            byte r0 = a.b.a(r7, r0)
            goto L_0x0019
        L_0x012d:
            int r0 = r11.w
            r1 = 16
            if (r0 < r1) goto L_0x014b
            int r0 = r11.w
            r1 = 17
            if (r0 > r1) goto L_0x014b
            a.b r0 = r11.b
            byte r1 = r11.f98a
            boolean r0 = r0.v(r1)
            if (r0 != 0) goto L_0x014b
            java.lang.String r0 = "3NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x014b:
            int r0 = r11.w
            r1 = 13
            if (r0 < r1) goto L_0x0169
            int r0 = r11.w
            r1 = 15
            if (r0 > r1) goto L_0x0169
            a.b r0 = r11.b
            byte r1 = r11.f98a
            boolean r0 = r0.v(r1)
            if (r0 != 0) goto L_0x0169
            java.lang.String r0 = "2NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0169:
            byte[] r0 = r11.x
            byte r0 = r0[r6]
            if (r0 < r8) goto L_0x0177
            java.lang.String r0 = "1H"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0177:
            byte[] r0 = r11.x
            byte r0 = r0[r7]
            if (r0 < r8) goto L_0x0185
            java.lang.String r0 = "1S"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0185:
            int r0 = r11.w
            r1 = 11
            if (r0 < r1) goto L_0x019a
            byte[] r0 = r11.x
            r1 = 0
            byte r0 = r0[r1]
            if (r0 < r8) goto L_0x019a
            java.lang.String r0 = "2C"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x019a:
            int r0 = r11.w
            r1 = 11
            if (r0 < r1) goto L_0x01ae
            byte[] r0 = r11.x
            byte r0 = r0[r10]
            if (r0 < r8) goto L_0x01ae
            java.lang.String r0 = "2D"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x01ae:
            java.lang.String r0 = "1NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x01b6:
            java.lang.String r1 = "1H"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x02dd
            int r0 = r11.w
            r1 = 9
            if (r0 > r1) goto L_0x01e6
            byte[] r0 = r11.x
            byte r0 = r0[r6]
            if (r0 < r9) goto L_0x01e6
            a.b r0 = r11.b
            byte r1 = r11.f98a
            boolean r0 = r0.w(r1)
            if (r0 != 0) goto L_0x01de
            a.b r0 = r11.b
            byte r1 = r11.f98a
            boolean r0 = r0.x(r1)
            if (r0 == 0) goto L_0x01e6
        L_0x01de:
            java.lang.String r0 = "4H"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x01e6:
            int r0 = r11.w
            r1 = 6
            if (r0 < r1) goto L_0x0215
            int r0 = r11.w
            r1 = 10
            if (r0 > r1) goto L_0x0215
            byte[] r0 = r11.x
            byte r0 = r0[r6]
            if (r0 < r7) goto L_0x01ff
            java.lang.String r0 = "2H"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x01ff:
            byte[] r0 = r11.x
            byte r0 = r0[r7]
            if (r0 < r8) goto L_0x020d
            java.lang.String r0 = "1S"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x020d:
            java.lang.String r0 = "1NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0215:
            int r0 = r11.w
            r1 = 11
            if (r0 < r1) goto L_0x0249
            int r0 = r11.w
            r1 = 12
            if (r0 > r1) goto L_0x0249
            byte[] r0 = r11.x
            byte r0 = r0[r6]
            if (r0 < r7) goto L_0x022f
            java.lang.String r0 = "3H"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x022f:
            byte[] r0 = r11.x
            byte r0 = r0[r7]
            if (r0 < r8) goto L_0x0241
            byte r0 = r11.y
            if (r0 != r7) goto L_0x0241
            java.lang.String r0 = "1S"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0241:
            byte r0 = r11.y
            byte r0 = a.b.a(r6, r0)
            goto L_0x0019
        L_0x0249:
            int r0 = r11.w
            r1 = 13
            if (r0 < r1) goto L_0x0262
            byte[] r0 = r11.x
            byte r0 = r0[r6]
            if (r0 < r8) goto L_0x0262
            java.lang.String r0 = "Jacoby 2NT."
            r11.a(r0)
            java.lang.String r0 = "2NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0262:
            int r0 = r11.w
            r1 = 15
            if (r0 < r1) goto L_0x0281
            int r0 = r11.w
            r1 = 17
            if (r0 > r1) goto L_0x0281
            a.b r0 = r11.b
            byte r1 = r11.f98a
            r2 = 0
            boolean r0 = r0.a(r1, r2)
            if (r0 == 0) goto L_0x0281
            java.lang.String r0 = "3NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0281:
            int r0 = r11.w
            r1 = 13
            if (r0 < r1) goto L_0x02a1
            int r0 = r11.w
            r1 = 18
            if (r0 > r1) goto L_0x02a1
            byte r0 = r11.y
            if (r0 != r7) goto L_0x0299
            java.lang.String r0 = "1S"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0299:
            byte r0 = r11.y
            byte r0 = a.b.a(r6, r0)
            goto L_0x0019
        L_0x02a1:
            int r0 = r11.w
            r1 = 19
            if (r0 < r1) goto L_0x1541
            byte[] r0 = r11.x
            byte r0 = r0[r7]
            if (r0 < r9) goto L_0x02b9
            byte r0 = r11.y
            if (r0 != r7) goto L_0x02b9
            java.lang.String r0 = "2S"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x02b9:
            byte[] r0 = r11.x
            byte r1 = r11.y
            byte r0 = r0[r1]
            if (r0 < r9) goto L_0x02c9
            byte r0 = r11.y
            byte r0 = a.b.a(r7, r0)
            goto L_0x0019
        L_0x02c9:
            byte r0 = r11.y
            if (r0 != r7) goto L_0x02d5
            java.lang.String r0 = "1S"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x02d5:
            byte r0 = r11.y
            byte r0 = a.b.a(r6, r0)
            goto L_0x0019
        L_0x02dd:
            java.lang.String r1 = "1S"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x03ba
            int r0 = r11.w
            r1 = 9
            if (r0 > r1) goto L_0x030d
            byte[] r0 = r11.x
            byte r0 = r0[r7]
            if (r0 < r9) goto L_0x030d
            a.b r0 = r11.b
            byte r1 = r11.f98a
            boolean r0 = r0.w(r1)
            if (r0 != 0) goto L_0x0305
            a.b r0 = r11.b
            byte r1 = r11.f98a
            boolean r0 = r0.x(r1)
            if (r0 == 0) goto L_0x030d
        L_0x0305:
            java.lang.String r0 = "4S"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x030d:
            int r0 = r11.w
            r1 = 6
            if (r0 < r1) goto L_0x032e
            int r0 = r11.w
            r1 = 10
            if (r0 > r1) goto L_0x032e
            byte[] r0 = r11.x
            byte r0 = r0[r7]
            if (r0 < r7) goto L_0x0326
            java.lang.String r0 = "2S"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0326:
            java.lang.String r0 = "1NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x032e:
            int r0 = r11.w
            r1 = 11
            if (r0 < r1) goto L_0x0350
            int r0 = r11.w
            r1 = 12
            if (r0 > r1) goto L_0x0350
            byte[] r0 = r11.x
            byte r0 = r0[r7]
            if (r0 < r7) goto L_0x0348
            java.lang.String r0 = "3S"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0348:
            byte r0 = r11.y
            byte r0 = a.b.a(r6, r0)
            goto L_0x0019
        L_0x0350:
            int r0 = r11.w
            r1 = 13
            if (r0 < r1) goto L_0x0369
            byte[] r0 = r11.x
            byte r0 = r0[r7]
            if (r0 < r8) goto L_0x0369
            java.lang.String r0 = "Jacoby 2NT."
            r11.a(r0)
            java.lang.String r0 = "2NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0369:
            int r0 = r11.w
            r1 = 15
            if (r0 < r1) goto L_0x0388
            int r0 = r11.w
            r1 = 17
            if (r0 > r1) goto L_0x0388
            a.b r0 = r11.b
            byte r1 = r11.f98a
            r2 = 0
            boolean r0 = r0.a(r1, r2)
            if (r0 == 0) goto L_0x0388
            java.lang.String r0 = "3NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0388:
            int r0 = r11.w
            r1 = 13
            if (r0 < r1) goto L_0x039c
            int r0 = r11.w
            r1 = 18
            if (r0 > r1) goto L_0x039c
            byte r0 = r11.y
            byte r0 = a.b.a(r6, r0)
            goto L_0x0019
        L_0x039c:
            int r0 = r11.w
            r1 = 19
            if (r0 < r1) goto L_0x1541
            byte[] r0 = r11.x
            byte r1 = r11.y
            byte r0 = r0[r1]
            if (r0 < r9) goto L_0x03b2
            byte r0 = r11.y
            byte r0 = a.b.a(r7, r0)
            goto L_0x0019
        L_0x03b2:
            byte r0 = r11.y
            byte r0 = a.b.a(r6, r0)
            goto L_0x0019
        L_0x03ba:
            java.lang.String r1 = "1NT"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x0586
            int r0 = r11.w
            r1 = 22
            if (r0 < r1) goto L_0x03da
            a.b r0 = r11.b
            byte r1 = r11.f98a
            boolean r0 = r0.a(r1, r10)
            if (r0 == 0) goto L_0x03da
            java.lang.String r0 = "7NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x03da:
            int r0 = r11.w
            r1 = 20
            if (r0 < r1) goto L_0x03f8
            int r0 = r11.w
            r1 = 21
            if (r0 > r1) goto L_0x03f8
            a.b r0 = r11.b
            byte r1 = r11.f98a
            boolean r0 = r0.a(r1, r10)
            if (r0 == 0) goto L_0x03f8
            java.lang.String r0 = "5NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x03f8:
            int r0 = r11.w
            r1 = 18
            if (r0 < r1) goto L_0x0416
            int r0 = r11.w
            r1 = 19
            if (r0 > r1) goto L_0x0416
            a.b r0 = r11.b
            byte r1 = r11.f98a
            boolean r0 = r0.a(r1, r10)
            if (r0 == 0) goto L_0x0416
            java.lang.String r0 = "6NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0416:
            int r0 = r11.w
            r1 = 16
            if (r0 < r1) goto L_0x0434
            int r0 = r11.w
            r1 = 18
            if (r0 > r1) goto L_0x0434
            a.b r0 = r11.b
            byte r1 = r11.f98a
            boolean r0 = r0.a(r1, r10)
            if (r0 == 0) goto L_0x0434
            java.lang.String r0 = "4NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0434:
            int r0 = r11.w
            r1 = 9
            if (r0 < r1) goto L_0x045d
            a.b r0 = r11.b
            byte r1 = r11.f98a
            r2 = 72
            int r0 = r0.c(r1, r2)
            r1 = 6
            if (r0 < r1) goto L_0x045d
            a.b r0 = r11.b
            byte r1 = r11.f98a
            r2 = 72
            java.lang.String r3 = "AKQJ"
            boolean r0 = r0.a(r1, r2, r3, r6)
            if (r0 == 0) goto L_0x045d
            java.lang.String r0 = "3H"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x045d:
            int r0 = r11.w
            r1 = 9
            if (r0 < r1) goto L_0x0486
            a.b r0 = r11.b
            byte r1 = r11.f98a
            r2 = 83
            int r0 = r0.c(r1, r2)
            r1 = 6
            if (r0 < r1) goto L_0x0486
            a.b r0 = r11.b
            byte r1 = r11.f98a
            r2 = 83
            java.lang.String r3 = "AKQJ"
            boolean r0 = r0.a(r1, r2, r3, r6)
            if (r0 == 0) goto L_0x0486
            java.lang.String r0 = "3S"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0486:
            int r0 = r11.w
            r1 = 7
            if (r0 > r1) goto L_0x04ae
            a.b r0 = r11.b
            byte r1 = r11.f98a
            r2 = 67
            int r0 = r0.c(r1, r2)
            r1 = 6
            if (r0 < r1) goto L_0x04ae
            a.b r0 = r11.b
            byte r1 = r11.f98a
            r2 = 67
            java.lang.String r3 = "AKQJ"
            boolean r0 = r0.a(r1, r2, r3, r6)
            if (r0 == 0) goto L_0x04ae
            java.lang.String r0 = "3C"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x04ae:
            int r0 = r11.w
            r1 = 7
            if (r0 > r1) goto L_0x04d6
            a.b r0 = r11.b
            byte r1 = r11.f98a
            r2 = 68
            int r0 = r0.c(r1, r2)
            r1 = 6
            if (r0 < r1) goto L_0x04d6
            a.b r0 = r11.b
            byte r1 = r11.f98a
            r2 = 68
            java.lang.String r3 = "AKQJ"
            boolean r0 = r0.a(r1, r2, r3, r6)
            if (r0 == 0) goto L_0x04d6
            java.lang.String r0 = "3D"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x04d6:
            int r0 = r11.w
            r1 = 6
            if (r0 > r1) goto L_0x04f7
            byte[] r0 = r11.x
            r1 = 0
            byte r0 = r0[r1]
            r1 = 6
            if (r0 >= r1) goto L_0x04ea
            byte[] r0 = r11.x
            byte r0 = r0[r10]
            r1 = 6
            if (r0 < r1) goto L_0x04f7
        L_0x04ea:
            java.lang.String r0 = "Minor transfer."
            r11.a(r0)
            java.lang.String r0 = "2S"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x04f7:
            int r0 = r11.w
            r1 = 8
            if (r0 < r1) goto L_0x0522
            a.b r0 = r11.b
            byte r1 = r11.f98a
            r2 = 72
            int r0 = r0.c(r1, r2)
            if (r0 >= r8) goto L_0x0515
            a.b r0 = r11.b
            byte r1 = r11.f98a
            r2 = 83
            int r0 = r0.c(r1, r2)
            if (r0 < r8) goto L_0x0522
        L_0x0515:
            java.lang.String r0 = "Stayman."
            r11.a(r0)
            java.lang.String r0 = "2C"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0522:
            int r0 = r11.w
            r1 = 6
            if (r0 > r1) goto L_0x0540
            a.b r0 = r11.b
            byte r1 = r11.f98a
            r2 = 72
            int r0 = r0.c(r1, r2)
            if (r0 < r9) goto L_0x0540
            java.lang.String r0 = "Jacoby transfer."
            r11.a(r0)
            java.lang.String r0 = "2D"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0540:
            int r0 = r11.w
            r1 = 6
            if (r0 > r1) goto L_0x055e
            a.b r0 = r11.b
            byte r1 = r11.f98a
            r2 = 83
            int r0 = r0.c(r1, r2)
            if (r0 < r9) goto L_0x055e
            java.lang.String r0 = "Jacoby transfer."
            r11.a(r0)
            java.lang.String r0 = "2H"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x055e:
            int r0 = r11.w
            r1 = 8
            if (r0 < r1) goto L_0x0572
            int r0 = r11.w
            r1 = 9
            if (r0 > r1) goto L_0x0572
            java.lang.String r0 = "2NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0572:
            int r0 = r11.w
            r1 = 10
            if (r0 < r1) goto L_0x1541
            int r0 = r11.w
            r1 = 15
            if (r0 > r1) goto L_0x1541
            java.lang.String r0 = "3NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0586:
            java.lang.String r1 = "2D"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x0619
            int r0 = r11.w
            r1 = 13
            if (r0 < r1) goto L_0x05f1
            byte[] r0 = r11.x
            byte r0 = r0[r6]
            if (r0 < r9) goto L_0x05b0
            a.b r0 = r11.b
            byte r1 = r11.f98a
            r2 = 72
            java.lang.String r3 = "AKQ"
            boolean r0 = r0.a(r1, r2, r3, r6)
            if (r0 == 0) goto L_0x05b0
            java.lang.String r0 = "2H"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x05b0:
            byte[] r0 = r11.x
            byte r0 = r0[r7]
            if (r0 < r9) goto L_0x05cc
            a.b r0 = r11.b
            byte r1 = r11.f98a
            r2 = 83
            java.lang.String r3 = "AKQ"
            boolean r0 = r0.a(r1, r2, r3, r6)
            if (r0 == 0) goto L_0x05cc
            java.lang.String r0 = "2S"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x05cc:
            byte[] r0 = r11.x
            r1 = 0
            byte r0 = r0[r1]
            if (r0 < r9) goto L_0x05e9
            a.b r0 = r11.b
            byte r1 = r11.f98a
            r2 = 67
            java.lang.String r3 = "AKQ"
            boolean r0 = r0.a(r1, r2, r3, r6)
            if (r0 == 0) goto L_0x05e9
            java.lang.String r0 = "3C"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x05e9:
            java.lang.String r0 = "2NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x05f1:
            int r0 = r11.w
            r1 = 10
            if (r0 < r1) goto L_0x0605
            byte[] r0 = r11.x
            byte r0 = r0[r10]
            if (r0 < r8) goto L_0x0605
            java.lang.String r0 = "4D"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0605:
            int r0 = r11.w
            r1 = 8
            if (r0 < r1) goto L_0x1541
            byte[] r0 = r11.x
            byte r0 = r0[r10]
            if (r0 < r7) goto L_0x1541
            java.lang.String r0 = "3D"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0619:
            java.lang.String r1 = "2H"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x06ac
            int r0 = r11.w
            r1 = 13
            if (r0 < r1) goto L_0x0684
            byte[] r0 = r11.x
            byte r0 = r0[r7]
            if (r0 < r9) goto L_0x0643
            a.b r0 = r11.b
            byte r1 = r11.f98a
            r2 = 83
            java.lang.String r3 = "AKQ"
            boolean r0 = r0.a(r1, r2, r3, r6)
            if (r0 == 0) goto L_0x0643
            java.lang.String r0 = "2S"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0643:
            byte[] r0 = r11.x
            r1 = 0
            byte r0 = r0[r1]
            if (r0 < r9) goto L_0x0660
            a.b r0 = r11.b
            byte r1 = r11.f98a
            r2 = 67
            java.lang.String r3 = "AKQ"
            boolean r0 = r0.a(r1, r2, r3, r6)
            if (r0 == 0) goto L_0x0660
            java.lang.String r0 = "3C"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0660:
            byte[] r0 = r11.x
            byte r0 = r0[r10]
            if (r0 < r9) goto L_0x067c
            a.b r0 = r11.b
            byte r1 = r11.f98a
            r2 = 68
            java.lang.String r3 = "AKQ"
            boolean r0 = r0.a(r1, r2, r3, r6)
            if (r0 == 0) goto L_0x067c
            java.lang.String r0 = "3D"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x067c:
            java.lang.String r0 = "2NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0684:
            int r0 = r11.w
            r1 = 10
            if (r0 < r1) goto L_0x0698
            byte[] r0 = r11.x
            byte r0 = r0[r6]
            if (r0 < r8) goto L_0x0698
            java.lang.String r0 = "4H"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0698:
            int r0 = r11.w
            r1 = 8
            if (r0 < r1) goto L_0x1541
            byte[] r0 = r11.x
            byte r0 = r0[r6]
            if (r0 < r7) goto L_0x1541
            java.lang.String r0 = "3H"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x06ac:
            java.lang.String r1 = "2S"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x073f
            int r0 = r11.w
            r1 = 13
            if (r0 < r1) goto L_0x0717
            byte[] r0 = r11.x
            r1 = 0
            byte r0 = r0[r1]
            if (r0 < r9) goto L_0x06d7
            a.b r0 = r11.b
            byte r1 = r11.f98a
            r2 = 67
            java.lang.String r3 = "AKQ"
            boolean r0 = r0.a(r1, r2, r3, r6)
            if (r0 == 0) goto L_0x06d7
            java.lang.String r0 = "3C"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x06d7:
            byte[] r0 = r11.x
            byte r0 = r0[r10]
            if (r0 < r9) goto L_0x06f3
            a.b r0 = r11.b
            byte r1 = r11.f98a
            r2 = 68
            java.lang.String r3 = "AKQ"
            boolean r0 = r0.a(r1, r2, r3, r6)
            if (r0 == 0) goto L_0x06f3
            java.lang.String r0 = "3D"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x06f3:
            byte[] r0 = r11.x
            byte r0 = r0[r6]
            if (r0 < r9) goto L_0x070f
            a.b r0 = r11.b
            byte r1 = r11.f98a
            r2 = 72
            java.lang.String r3 = "AKQ"
            boolean r0 = r0.a(r1, r2, r3, r6)
            if (r0 == 0) goto L_0x070f
            java.lang.String r0 = "3H"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x070f:
            java.lang.String r0 = "2NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0717:
            int r0 = r11.w
            r1 = 10
            if (r0 < r1) goto L_0x072b
            byte[] r0 = r11.x
            byte r0 = r0[r7]
            if (r0 < r8) goto L_0x072b
            java.lang.String r0 = "4S"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x072b:
            int r0 = r11.w
            r1 = 8
            if (r0 < r1) goto L_0x1541
            byte[] r0 = r11.x
            byte r0 = r0[r7]
            if (r0 < r7) goto L_0x1541
            java.lang.String r0 = "3S"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x073f:
            java.lang.String r1 = "2NT"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x081d
            byte[] r0 = r11.x
            byte r1 = r11.y
            byte r0 = r0[r1]
            r1 = 6
            if (r0 < r1) goto L_0x0771
            a.b r0 = r11.b
            byte r1 = r11.f98a
            char r2 = r11.z
            java.lang.String r3 = "AKQ"
            boolean r0 = r0.a(r1, r2, r3, r6)
            if (r0 == 0) goto L_0x0771
            int r0 = r11.w
            r1 = 11
            if (r0 < r1) goto L_0x0771
            java.lang.String r0 = "Gerber (04123)"
            r11.a(r0)
            java.lang.String r0 = "4C"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0771:
            a.b r0 = r11.b
            byte r1 = r11.f98a
            r2 = 0
            boolean r0 = r0.a(r1, r2)
            if (r0 == 0) goto L_0x07d4
            int r0 = r11.w
            if (r0 > r8) goto L_0x0784
            r0 = 99
            goto L_0x0019
        L_0x0784:
            int r0 = r11.w
            if (r0 < r9) goto L_0x0796
            int r0 = r11.w
            r1 = 11
            if (r0 > r1) goto L_0x0796
            java.lang.String r0 = "3NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0796:
            int r0 = r11.w
            r1 = 12
            if (r0 != r1) goto L_0x07a4
            java.lang.String r0 = "4NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x07a4:
            int r0 = r11.w
            r1 = 13
            if (r0 < r1) goto L_0x07b8
            int r0 = r11.w
            r1 = 15
            if (r0 > r1) goto L_0x07b8
            java.lang.String r0 = "6NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x07b8:
            int r0 = r11.w
            r1 = 16
            if (r0 != r1) goto L_0x07c6
            java.lang.String r0 = "5NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x07c6:
            int r0 = r11.w
            r1 = 17
            if (r0 < r1) goto L_0x07d4
            java.lang.String r0 = "7NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x07d4:
            byte[] r0 = r11.x
            byte r0 = r0[r6]
            if (r0 < r9) goto L_0x07ec
            int r0 = r11.w
            r1 = 7
            if (r0 > r1) goto L_0x07ec
            java.lang.String r0 = "Jacoby transfer."
            r11.a(r0)
            java.lang.String r0 = "3D"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x07ec:
            byte[] r0 = r11.x
            byte r0 = r0[r7]
            if (r0 < r9) goto L_0x0804
            int r0 = r11.w
            r1 = 7
            if (r0 > r1) goto L_0x0804
            java.lang.String r0 = "Jacoby transfer."
            r11.a(r0)
            java.lang.String r0 = "3H"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0804:
            byte[] r0 = r11.x
            byte r0 = r0[r6]
            if (r0 >= r8) goto L_0x0810
            byte[] r0 = r11.x
            byte r0 = r0[r7]
            if (r0 < r8) goto L_0x1541
        L_0x0810:
            java.lang.String r0 = "Stayman."
            r11.a(r0)
            java.lang.String r0 = "3C"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x081d:
            java.lang.String r1 = "1C-1D"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x08ad
            int r0 = r11.w
            r1 = 15
            if (r0 > r1) goto L_0x086c
            byte[] r0 = r11.x
            byte r0 = r0[r6]
            if (r0 < r8) goto L_0x0839
            java.lang.String r0 = "1H"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0839:
            byte[] r0 = r11.x
            byte r0 = r0[r7]
            if (r0 < r8) goto L_0x0847
            java.lang.String r0 = "1S"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0847:
            byte[] r0 = r11.x
            byte r0 = r0[r10]
            if (r0 < r9) goto L_0x0855
            java.lang.String r0 = "2D"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0855:
            byte[] r0 = r11.x
            r1 = 0
            byte r0 = r0[r1]
            if (r0 < r9) goto L_0x0864
            java.lang.String r0 = "2C"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0864:
            java.lang.String r0 = "1NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x086c:
            byte[] r0 = r11.x
            byte r0 = r0[r6]
            if (r0 < r8) goto L_0x087a
            java.lang.String r0 = "2H"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x087a:
            byte[] r0 = r11.x
            byte r0 = r0[r7]
            if (r0 < r8) goto L_0x0888
            java.lang.String r0 = "2S"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0888:
            byte[] r0 = r11.x
            byte r0 = r0[r10]
            if (r0 < r9) goto L_0x0896
            java.lang.String r0 = "3D"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0896:
            byte[] r0 = r11.x
            r1 = 0
            byte r0 = r0[r1]
            if (r0 < r9) goto L_0x08a5
            java.lang.String r0 = "3C"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x08a5:
            java.lang.String r0 = "2NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x08ad:
            java.lang.String r1 = "1C-1H"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x093d
            int r0 = r11.w
            r1 = 15
            if (r0 > r1) goto L_0x08fc
            byte[] r0 = r11.x
            byte r0 = r0[r7]
            if (r0 < r8) goto L_0x08c9
            java.lang.String r0 = "1S"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x08c9:
            byte[] r0 = r11.x
            byte r0 = r0[r6]
            if (r0 < r8) goto L_0x08d7
            java.lang.String r0 = "2H"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x08d7:
            byte[] r0 = r11.x
            byte r0 = r0[r10]
            if (r0 < r9) goto L_0x08e5
            java.lang.String r0 = "2D"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x08e5:
            byte[] r0 = r11.x
            r1 = 0
            byte r0 = r0[r1]
            if (r0 < r9) goto L_0x08f4
            java.lang.String r0 = "2C"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x08f4:
            java.lang.String r0 = "1NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x08fc:
            byte[] r0 = r11.x
            byte r0 = r0[r7]
            if (r0 < r8) goto L_0x090a
            java.lang.String r0 = "2S"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x090a:
            byte[] r0 = r11.x
            byte r0 = r0[r6]
            if (r0 < r8) goto L_0x0918
            java.lang.String r0 = "3H"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0918:
            byte[] r0 = r11.x
            byte r0 = r0[r10]
            if (r0 < r9) goto L_0x0926
            java.lang.String r0 = "3D"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0926:
            byte[] r0 = r11.x
            r1 = 0
            byte r0 = r0[r1]
            if (r0 < r9) goto L_0x0935
            java.lang.String r0 = "3C"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0935:
            java.lang.String r0 = "2NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x093d:
            java.lang.String r1 = "1C-1S"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x09cd
            int r0 = r11.w
            r1 = 15
            if (r0 > r1) goto L_0x098c
            byte[] r0 = r11.x
            byte r0 = r0[r7]
            if (r0 < r8) goto L_0x0959
            java.lang.String r0 = "2S"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0959:
            byte[] r0 = r11.x
            byte r0 = r0[r6]
            if (r0 < r8) goto L_0x0967
            java.lang.String r0 = "2H"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0967:
            byte[] r0 = r11.x
            byte r0 = r0[r10]
            if (r0 < r9) goto L_0x0975
            java.lang.String r0 = "2D"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0975:
            byte[] r0 = r11.x
            r1 = 0
            byte r0 = r0[r1]
            if (r0 < r9) goto L_0x0984
            java.lang.String r0 = "2C"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0984:
            java.lang.String r0 = "1NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x098c:
            byte[] r0 = r11.x
            byte r0 = r0[r7]
            if (r0 < r8) goto L_0x099a
            java.lang.String r0 = "3S"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x099a:
            byte[] r0 = r11.x
            byte r0 = r0[r6]
            if (r0 < r8) goto L_0x09a8
            java.lang.String r0 = "3H"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x09a8:
            byte[] r0 = r11.x
            byte r0 = r0[r10]
            if (r0 < r9) goto L_0x09b6
            java.lang.String r0 = "3D"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x09b6:
            byte[] r0 = r11.x
            r1 = 0
            byte r0 = r0[r1]
            if (r0 < r9) goto L_0x09c5
            java.lang.String r0 = "3C"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x09c5:
            java.lang.String r0 = "2NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x09cd:
            java.lang.String r1 = "1C-2NT"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x0ad2
            a.b r0 = r11.b
            byte r1 = r11.f98a
            boolean r0 = r0.a(r1, r10)
            if (r0 == 0) goto L_0x0a03
            int r0 = r11.w
            r1 = 20
            if (r0 < r1) goto L_0x09ed
            java.lang.String r0 = "6NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x09ed:
            int r0 = r11.w
            r1 = 18
            if (r0 < r1) goto L_0x09fb
            java.lang.String r0 = "4NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x09fb:
            java.lang.String r0 = "3NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0a03:
            int r0 = r11.w
            r1 = 15
            if (r0 > r1) goto L_0x0a18
            byte[] r0 = r11.x
            r1 = 0
            byte r0 = r0[r1]
            if (r0 < r9) goto L_0x0a18
            java.lang.String r0 = "3C"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0a18:
            int r0 = r11.w
            r1 = 18
            if (r0 < r1) goto L_0x0aca
            byte[] r0 = r11.x
            r1 = 0
            byte r0 = r0[r1]
            r1 = 6
            if (r0 < r1) goto L_0x0a76
            a.b r0 = r11.b
            byte r1 = r11.f98a
            r2 = 67
            java.lang.String r3 = "AKQJ"
            boolean r0 = r0.a(r1, r2, r3, r7)
            if (r0 == 0) goto L_0x0a76
            a.b r0 = r11.b
            byte r1 = r11.f98a
            r2 = 68
            java.lang.String r3 = "A"
            boolean r0 = r0.a(r1, r2, r3, r10)
            if (r0 == 0) goto L_0x0a4a
            java.lang.String r0 = "3D"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0a4a:
            a.b r0 = r11.b
            byte r1 = r11.f98a
            r2 = 72
            java.lang.String r3 = "A"
            boolean r0 = r0.a(r1, r2, r3, r10)
            if (r0 == 0) goto L_0x0a60
            java.lang.String r0 = "3H"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0a60:
            a.b r0 = r11.b
            byte r1 = r11.f98a
            r2 = 83
            java.lang.String r3 = "A"
            boolean r0 = r0.a(r1, r2, r3, r10)
            if (r0 == 0) goto L_0x0aca
            java.lang.String r0 = "3S"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0a76:
            byte[] r0 = r11.x
            byte r0 = r0[r6]
            if (r0 < r8) goto L_0x0a92
            a.b r0 = r11.b
            byte r1 = r11.f98a
            r2 = 72
            java.lang.String r3 = "AKQ"
            boolean r0 = r0.a(r1, r2, r3, r6)
            if (r0 == 0) goto L_0x0a92
            java.lang.String r0 = "3H"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0a92:
            byte[] r0 = r11.x
            byte r0 = r0[r7]
            if (r0 < r8) goto L_0x0aae
            a.b r0 = r11.b
            byte r1 = r11.f98a
            r2 = 83
            java.lang.String r3 = "AKQ"
            boolean r0 = r0.a(r1, r2, r3, r6)
            if (r0 == 0) goto L_0x0aae
            java.lang.String r0 = "3S"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0aae:
            byte[] r0 = r11.x
            byte r0 = r0[r10]
            if (r0 < r8) goto L_0x0aca
            a.b r0 = r11.b
            byte r1 = r11.f98a
            r2 = 68
            java.lang.String r3 = "AKQ"
            boolean r0 = r0.a(r1, r2, r3, r6)
            if (r0 == 0) goto L_0x0aca
            java.lang.String r0 = "3D"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0aca:
            java.lang.String r0 = "3NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0ad2:
            java.lang.String r1 = "1D-1H"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x0b62
            int r0 = r11.w
            r1 = 15
            if (r0 > r1) goto L_0x0b21
            byte[] r0 = r11.x
            byte r0 = r0[r7]
            if (r0 < r8) goto L_0x0aee
            java.lang.String r0 = "1S"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0aee:
            byte[] r0 = r11.x
            byte r0 = r0[r6]
            if (r0 < r8) goto L_0x0afc
            java.lang.String r0 = "2H"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0afc:
            byte[] r0 = r11.x
            r1 = 0
            byte r0 = r0[r1]
            if (r0 < r9) goto L_0x0b0b
            java.lang.String r0 = "2C"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0b0b:
            byte[] r0 = r11.x
            byte r0 = r0[r10]
            if (r0 < r9) goto L_0x0b19
            java.lang.String r0 = "2D"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0b19:
            java.lang.String r0 = "1NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0b21:
            byte[] r0 = r11.x
            byte r0 = r0[r7]
            if (r0 < r8) goto L_0x0b2f
            java.lang.String r0 = "2S"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0b2f:
            byte[] r0 = r11.x
            byte r0 = r0[r6]
            if (r0 < r8) goto L_0x0b3d
            java.lang.String r0 = "3H"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0b3d:
            byte[] r0 = r11.x
            r1 = 0
            byte r0 = r0[r1]
            if (r0 < r9) goto L_0x0b4c
            java.lang.String r0 = "3C"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0b4c:
            byte[] r0 = r11.x
            byte r0 = r0[r10]
            if (r0 < r9) goto L_0x0b5a
            java.lang.String r0 = "3D"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0b5a:
            java.lang.String r0 = "2NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0b62:
            java.lang.String r1 = "1D-1S"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x0bf2
            int r0 = r11.w
            r1 = 15
            if (r0 > r1) goto L_0x0bb1
            byte[] r0 = r11.x
            byte r0 = r0[r7]
            if (r0 < r8) goto L_0x0b7e
            java.lang.String r0 = "2S"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0b7e:
            byte[] r0 = r11.x
            byte r0 = r0[r6]
            if (r0 < r8) goto L_0x0b8c
            java.lang.String r0 = "2H"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0b8c:
            byte[] r0 = r11.x
            r1 = 0
            byte r0 = r0[r1]
            if (r0 < r9) goto L_0x0b9b
            java.lang.String r0 = "2C"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0b9b:
            byte[] r0 = r11.x
            byte r0 = r0[r10]
            if (r0 < r9) goto L_0x0ba9
            java.lang.String r0 = "2D"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0ba9:
            java.lang.String r0 = "1NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0bb1:
            byte[] r0 = r11.x
            byte r0 = r0[r7]
            if (r0 < r8) goto L_0x0bbf
            java.lang.String r0 = "3S"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0bbf:
            byte[] r0 = r11.x
            byte r0 = r0[r6]
            if (r0 < r8) goto L_0x0bcd
            java.lang.String r0 = "3H"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0bcd:
            byte[] r0 = r11.x
            r1 = 0
            byte r0 = r0[r1]
            if (r0 < r9) goto L_0x0bdc
            java.lang.String r0 = "3C"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0bdc:
            byte[] r0 = r11.x
            byte r0 = r0[r10]
            if (r0 < r9) goto L_0x0bea
            java.lang.String r0 = "3D"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0bea:
            java.lang.String r0 = "2NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0bf2:
            java.lang.String r1 = "1D-2NT"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x0cf7
            a.b r0 = r11.b
            byte r1 = r11.f98a
            boolean r0 = r0.a(r1, r10)
            if (r0 == 0) goto L_0x0c28
            int r0 = r11.w
            r1 = 20
            if (r0 < r1) goto L_0x0c12
            java.lang.String r0 = "6NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0c12:
            int r0 = r11.w
            r1 = 18
            if (r0 < r1) goto L_0x0c20
            java.lang.String r0 = "4NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0c20:
            java.lang.String r0 = "3NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0c28:
            int r0 = r11.w
            r1 = 15
            if (r0 > r1) goto L_0x0c3d
            byte[] r0 = r11.x
            r1 = 0
            byte r0 = r0[r1]
            if (r0 < r9) goto L_0x0c3d
            java.lang.String r0 = "3D"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0c3d:
            int r0 = r11.w
            r1 = 18
            if (r0 < r1) goto L_0x0cef
            byte[] r0 = r11.x
            byte r0 = r0[r10]
            r1 = 6
            if (r0 < r1) goto L_0x0c9a
            a.b r0 = r11.b
            byte r1 = r11.f98a
            r2 = 68
            java.lang.String r3 = "AKQJ"
            boolean r0 = r0.a(r1, r2, r3, r7)
            if (r0 == 0) goto L_0x0c9a
            a.b r0 = r11.b
            byte r1 = r11.f98a
            r2 = 67
            java.lang.String r3 = "A"
            boolean r0 = r0.a(r1, r2, r3, r10)
            if (r0 == 0) goto L_0x0c6e
            java.lang.String r0 = "3C"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0c6e:
            a.b r0 = r11.b
            byte r1 = r11.f98a
            r2 = 72
            java.lang.String r3 = "A"
            boolean r0 = r0.a(r1, r2, r3, r10)
            if (r0 == 0) goto L_0x0c84
            java.lang.String r0 = "3H"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0c84:
            a.b r0 = r11.b
            byte r1 = r11.f98a
            r2 = 83
            java.lang.String r3 = "A"
            boolean r0 = r0.a(r1, r2, r3, r10)
            if (r0 == 0) goto L_0x0cef
            java.lang.String r0 = "3S"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0c9a:
            byte[] r0 = r11.x
            byte r0 = r0[r6]
            if (r0 < r8) goto L_0x0cb6
            a.b r0 = r11.b
            byte r1 = r11.f98a
            r2 = 72
            java.lang.String r3 = "AKQ"
            boolean r0 = r0.a(r1, r2, r3, r6)
            if (r0 == 0) goto L_0x0cb6
            java.lang.String r0 = "3H"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0cb6:
            byte[] r0 = r11.x
            byte r0 = r0[r7]
            if (r0 < r8) goto L_0x0cd2
            a.b r0 = r11.b
            byte r1 = r11.f98a
            r2 = 83
            java.lang.String r3 = "AKQ"
            boolean r0 = r0.a(r1, r2, r3, r6)
            if (r0 == 0) goto L_0x0cd2
            java.lang.String r0 = "3S"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0cd2:
            byte[] r0 = r11.x
            r1 = 0
            byte r0 = r0[r1]
            if (r0 < r8) goto L_0x0cef
            a.b r0 = r11.b
            byte r1 = r11.f98a
            r2 = 67
            java.lang.String r3 = "AKQ"
            boolean r0 = r0.a(r1, r2, r3, r6)
            if (r0 == 0) goto L_0x0cef
            java.lang.String r0 = "3C"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0cef:
            java.lang.String r0 = "3NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0cf7:
            java.lang.String r1 = "1H-1S"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x0d8f
            int r0 = r11.w
            r1 = 15
            if (r0 > r1) goto L_0x0d4a
            byte[] r0 = r11.x
            byte r0 = r0[r7]
            if (r0 < r8) goto L_0x0d13
            java.lang.String r0 = "2S"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0d13:
            a.b r0 = r11.b
            byte r1 = r11.f98a
            boolean r0 = r0.a(r1, r10)
            if (r0 == 0) goto L_0x0d25
            java.lang.String r0 = "1NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0d25:
            byte[] r0 = r11.x
            r1 = 0
            byte r0 = r0[r1]
            if (r0 < r9) goto L_0x0d34
            java.lang.String r0 = "2C"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0d34:
            byte[] r0 = r11.x
            byte r0 = r0[r10]
            if (r0 < r9) goto L_0x0d42
            java.lang.String r0 = "2D"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0d42:
            java.lang.String r0 = "2H"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0d4a:
            byte[] r0 = r11.x
            byte r0 = r0[r7]
            if (r0 < r8) goto L_0x0d58
            java.lang.String r0 = "3S"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0d58:
            a.b r0 = r11.b
            byte r1 = r11.f98a
            boolean r0 = r0.a(r1, r10)
            if (r0 == 0) goto L_0x0d6a
            java.lang.String r0 = "2NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0d6a:
            byte[] r0 = r11.x
            r1 = 0
            byte r0 = r0[r1]
            if (r0 < r9) goto L_0x0d79
            java.lang.String r0 = "3C"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0d79:
            byte[] r0 = r11.x
            byte r0 = r0[r10]
            if (r0 < r9) goto L_0x0d87
            java.lang.String r0 = "3D"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0d87:
            java.lang.String r0 = "3H"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0d8f:
            java.lang.String r1 = "1H-2NT"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x0d9d
            byte r0 = r11.h(r6)
            goto L_0x0019
        L_0x0d9d:
            java.lang.String r1 = "1H-2NT"
            boolean r1 = r0.startsWith(r1)
            if (r1 == 0) goto L_0x10b4
            r1 = 0
            r2 = 7
            java.lang.String r0 = r0.substring(r1, r2)
            java.lang.String r1 = "3[CDS]"
            boolean r1 = r0.matches(r1)
            if (r1 == 0) goto L_0x0dce
            int r1 = r11.w
            a.b r2 = r11.b
            byte r3 = r11.f98a
            r4 = 72
            r5 = 321(0x141, float:4.5E-43)
            int r2 = r2.a(r3, r4, r5)
            int r1 = r1 + r2
            r2 = 16
            if (r1 < r2) goto L_0x0e5b
            java.lang.String r0 = "4NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0dce:
            java.lang.String r1 = "4H"
            boolean r1 = r0.matches(r1)
            if (r1 == 0) goto L_0x0df5
            int r0 = r11.w
            a.b r1 = r11.b
            byte r2 = r11.f98a
            r3 = 72
            r4 = 321(0x141, float:4.5E-43)
            int r1 = r1.a(r2, r3, r4)
            int r0 = r0 + r1
            r1 = 18
            if (r0 < r1) goto L_0x0df1
            java.lang.String r0 = "4NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0df1:
            r0 = 99
            goto L_0x0019
        L_0x0df5:
            java.lang.String r1 = "3NT"
            boolean r1 = r0.matches(r1)
            if (r1 == 0) goto L_0x0e20
            int r0 = r11.w
            a.b r1 = r11.b
            byte r2 = r11.f98a
            r3 = 72
            r4 = 321(0x141, float:4.5E-43)
            int r1 = r1.a(r2, r3, r4)
            int r0 = r0 + r1
            r1 = 16
            if (r0 < r1) goto L_0x0e18
            java.lang.String r0 = "4NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0e18:
            java.lang.String r0 = "4H"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0e20:
            java.lang.String r1 = "3H"
            boolean r1 = r0.matches(r1)
            if (r1 == 0) goto L_0x0e30
            java.lang.String r0 = "4NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0e30:
            java.lang.String r1 = "4[CDS]"
            boolean r1 = r0.matches(r1)
            if (r1 == 0) goto L_0x0e5b
            int r0 = r11.w
            a.b r1 = r11.b
            byte r2 = r11.f98a
            r3 = 72
            r4 = 321(0x141, float:4.5E-43)
            int r1 = r1.a(r2, r3, r4)
            int r0 = r0 + r1
            r1 = 16
            if (r0 < r1) goto L_0x0e53
            java.lang.String r0 = "4NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0e53:
            java.lang.String r0 = "4H"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0e5b:
            r1 = 0
            java.lang.String r0 = r0.substring(r1, r8)
            java.lang.String r1 = "4H"
            boolean r1 = r0.matches(r1)
            if (r1 == 0) goto L_0x0e6c
            r0 = 99
            goto L_0x0019
        L_0x0e6c:
            java.lang.String r1 = "4NT-5C"
            boolean r1 = r0.matches(r1)
            if (r1 == 0) goto L_0x0f22
            a.b r0 = r11.b
            byte r1 = r11.f98a
            r2 = 65
            int r0 = r0.a(r1, r2)
            if (r0 == r8) goto L_0x0e8c
            a.b r0 = r11.b
            byte r1 = r11.f98a
            r2 = 65
            int r0 = r0.a(r1, r2)
            if (r0 != 0) goto L_0x0ee0
        L_0x0e8c:
            int r0 = r11.w
            a.b r1 = r11.b
            byte r2 = r11.f98a
            r3 = 72
            r4 = 321(0x141, float:4.5E-43)
            int r1 = r1.a(r2, r3, r4)
            int r0 = r0 + r1
            int[] r1 = r11.F
            byte r2 = r11.f98a
            byte r2 = a.b.m(r2)
            r1 = r1[r2]
            int r0 = r0 + r1
            r1 = 35
            if (r0 < r1) goto L_0x0eb2
            java.lang.String r0 = "7H"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0eb2:
            int r0 = r11.w
            a.b r1 = r11.b
            byte r2 = r11.f98a
            r3 = 72
            r4 = 321(0x141, float:4.5E-43)
            int r1 = r1.a(r2, r3, r4)
            int r0 = r0 + r1
            int[] r1 = r11.F
            byte r2 = r11.f98a
            byte r2 = a.b.m(r2)
            r1 = r1[r2]
            int r0 = r0 + r1
            r1 = 31
            if (r0 < r1) goto L_0x0ed8
            java.lang.String r0 = "6H"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0ed8:
            java.lang.String r0 = "5H"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0ee0:
            a.b r0 = r11.b
            byte r1 = r11.f98a
            r2 = 65
            int r0 = r0.a(r1, r2)
            if (r0 != r7) goto L_0x0f1a
            int r0 = r11.w
            a.b r1 = r11.b
            byte r2 = r11.f98a
            r3 = 72
            r4 = 321(0x141, float:4.5E-43)
            int r1 = r1.a(r2, r3, r4)
            int r0 = r0 + r1
            int[] r1 = r11.F
            byte r2 = r11.f98a
            byte r2 = a.b.m(r2)
            r1 = r1[r2]
            int r0 = r0 + r1
            r1 = 31
            if (r0 < r1) goto L_0x0f12
            java.lang.String r0 = "6H"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0f12:
            java.lang.String r0 = "5H"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0f1a:
            java.lang.String r0 = "5H"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0f22:
            java.lang.String r1 = "4NT-5D"
            boolean r1 = r0.matches(r1)
            if (r1 == 0) goto L_0x0fcc
            a.b r0 = r11.b
            byte r1 = r11.f98a
            r2 = 65
            int r0 = r0.a(r1, r2)
            if (r0 != r7) goto L_0x0f8a
            int r0 = r11.w
            a.b r1 = r11.b
            byte r2 = r11.f98a
            r3 = 72
            r4 = 321(0x141, float:4.5E-43)
            int r1 = r1.a(r2, r3, r4)
            int r0 = r0 + r1
            int[] r1 = r11.F
            byte r2 = r11.f98a
            byte r2 = a.b.m(r2)
            r1 = r1[r2]
            int r0 = r0 + r1
            r1 = 35
            if (r0 < r1) goto L_0x0f5c
            java.lang.String r0 = "7H"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0f5c:
            int r0 = r11.w
            a.b r1 = r11.b
            byte r2 = r11.f98a
            r3 = 72
            r4 = 321(0x141, float:4.5E-43)
            int r1 = r1.a(r2, r3, r4)
            int r0 = r0 + r1
            int[] r1 = r11.F
            byte r2 = r11.f98a
            byte r2 = a.b.m(r2)
            r1 = r1[r2]
            int r0 = r0 + r1
            r1 = 31
            if (r0 < r1) goto L_0x0f82
            java.lang.String r0 = "6H"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0f82:
            java.lang.String r0 = "5H"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0f8a:
            a.b r0 = r11.b
            byte r1 = r11.f98a
            r2 = 65
            int r0 = r0.a(r1, r2)
            if (r0 != r6) goto L_0x0fc4
            int r0 = r11.w
            a.b r1 = r11.b
            byte r2 = r11.f98a
            r3 = 72
            r4 = 321(0x141, float:4.5E-43)
            int r1 = r1.a(r2, r3, r4)
            int r0 = r0 + r1
            int[] r1 = r11.F
            byte r2 = r11.f98a
            byte r2 = a.b.m(r2)
            r1 = r1[r2]
            int r0 = r0 + r1
            r1 = 31
            if (r0 < r1) goto L_0x0fbc
            java.lang.String r0 = "6H"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0fbc:
            java.lang.String r0 = "5H"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0fc4:
            java.lang.String r0 = "5H"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x0fcc:
            java.lang.String r1 = "4NT-5H"
            boolean r1 = r0.matches(r1)
            if (r1 == 0) goto L_0x106a
            a.b r0 = r11.b
            byte r1 = r11.f98a
            r2 = 65
            int r0 = r0.a(r1, r2)
            if (r0 != r6) goto L_0x1030
            int r0 = r11.w
            a.b r1 = r11.b
            byte r2 = r11.f98a
            r3 = 72
            r4 = 321(0x141, float:4.5E-43)
            int r1 = r1.a(r2, r3, r4)
            int r0 = r0 + r1
            int[] r1 = r11.F
            byte r2 = r11.f98a
            byte r2 = a.b.m(r2)
            r1 = r1[r2]
            int r0 = r0 + r1
            r1 = 35
            if (r0 < r1) goto L_0x1006
            java.lang.String r0 = "7H"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x1006:
            int r0 = r11.w
            a.b r1 = r11.b
            byte r2 = r11.f98a
            r3 = 72
            r4 = 321(0x141, float:4.5E-43)
            int r1 = r1.a(r2, r3, r4)
            int r0 = r0 + r1
            int[] r1 = r11.F
            byte r2 = r11.f98a
            byte r2 = a.b.m(r2)
            r1 = r1[r2]
            int r0 = r0 + r1
            r1 = 31
            if (r0 < r1) goto L_0x102c
            java.lang.String r0 = "6H"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x102c:
            r0 = 99
            goto L_0x0019
        L_0x1030:
            a.b r0 = r11.b
            byte r1 = r11.f98a
            r2 = 65
            int r0 = r0.a(r1, r2)
            if (r0 != r10) goto L_0x1066
            int r0 = r11.w
            a.b r1 = r11.b
            byte r2 = r11.f98a
            r3 = 72
            r4 = 321(0x141, float:4.5E-43)
            int r1 = r1.a(r2, r3, r4)
            int r0 = r0 + r1
            int[] r1 = r11.F
            byte r2 = r11.f98a
            byte r2 = a.b.m(r2)
            r1 = r1[r2]
            int r0 = r0 + r1
            r1 = 31
            if (r0 < r1) goto L_0x1062
            java.lang.String r0 = "6H"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x1062:
            r0 = 99
            goto L_0x0019
        L_0x1066:
            r0 = 99
            goto L_0x0019
        L_0x106a:
            java.lang.String r1 = "4NT-5S"
            boolean r0 = r0.matches(r1)
            if (r0 == 0) goto L_0x1541
            a.b r0 = r11.b
            byte r1 = r11.f98a
            r2 = 65
            int r0 = r0.a(r1, r2)
            if (r0 != r10) goto L_0x10ac
            int r0 = r11.w
            a.b r1 = r11.b
            byte r2 = r11.f98a
            r3 = 72
            r4 = 321(0x141, float:4.5E-43)
            int r1 = r1.a(r2, r3, r4)
            int r0 = r0 + r1
            int[] r1 = r11.F
            byte r2 = r11.f98a
            byte r2 = a.b.m(r2)
            r1 = r1[r2]
            int r0 = r0 + r1
            r1 = 35
            if (r0 < r1) goto L_0x10a4
            java.lang.String r0 = "7H"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x10a4:
            java.lang.String r0 = "6H"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x10ac:
            java.lang.String r0 = "6H"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x10b4:
            java.lang.String r1 = "1S-2NT"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x10c2
            byte r0 = r11.h(r7)
            goto L_0x0019
        L_0x10c2:
            java.lang.String r1 = "1S-2NT"
            boolean r1 = r0.startsWith(r1)
            if (r1 != 0) goto L_0x1541
            java.lang.String r1 = "1NT-2C"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x1102
            a.b r0 = r11.b
            byte r1 = r11.f98a
            r2 = 72
            int r0 = r0.c(r1, r2)
            if (r0 < r8) goto L_0x10e6
            java.lang.String r0 = "2H"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x10e6:
            a.b r0 = r11.b
            byte r1 = r11.f98a
            r2 = 83
            int r0 = r0.c(r1, r2)
            if (r0 < r8) goto L_0x10fa
            java.lang.String r0 = "2S"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x10fa:
            java.lang.String r0 = "2D"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x1102:
            java.lang.String r1 = "1NT-2D"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x1112
            java.lang.String r0 = "2H"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x1112:
            java.lang.String r1 = "1NT-2H"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x1122
            java.lang.String r0 = "2S"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x1122:
            java.lang.String r1 = "2NT-3C"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x114e
            byte[] r0 = r11.x
            byte r0 = r0[r6]
            if (r0 < r8) goto L_0x1138
            java.lang.String r0 = "3H"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x1138:
            byte[] r0 = r11.x
            byte r0 = r0[r7]
            if (r0 < r8) goto L_0x1146
            java.lang.String r0 = "3S"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x1146:
            java.lang.String r0 = "3D"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x114e:
            java.lang.String r1 = "2NT-3D"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x115e
            java.lang.String r0 = "3H"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x115e:
            java.lang.String r1 = "2NT-3H"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x116e
            java.lang.String r0 = "3S"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x116e:
            java.lang.String r1 = "1C-1D-1H"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x1241
            byte[] r0 = r11.x
            byte r0 = r0[r6]
            if (r0 < r8) goto L_0x11aa
            int r0 = r11.w
            r1 = 16
            if (r0 < r1) goto L_0x118a
            java.lang.String r0 = "3H"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x118a:
            int r0 = r11.w
            r1 = 13
            if (r0 < r1) goto L_0x1198
            java.lang.String r0 = "4H"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x1198:
            int r0 = r11.w
            r1 = 10
            if (r0 < r1) goto L_0x11a6
            java.lang.String r0 = "2H"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x11a6:
            r0 = 99
            goto L_0x0019
        L_0x11aa:
            byte[] r0 = r11.x
            byte r0 = r0[r7]
            if (r0 < r8) goto L_0x11be
            a.b r0 = r11.b
            byte r1 = r11.f98a
            r2 = 83
            java.lang.String r3 = "AKQ"
            boolean r0 = r0.a(r1, r2, r3, r10)
            if (r0 != 0) goto L_0x11c4
        L_0x11be:
            byte[] r0 = r11.x
            byte r0 = r0[r7]
            if (r0 < r9) goto L_0x11cc
        L_0x11c4:
            java.lang.String r0 = "1S"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x11cc:
            byte[] r0 = r11.x
            byte r0 = r0[r7]
            if (r0 < r7) goto L_0x120a
            a.b r0 = r11.b
            byte r1 = r11.f98a
            r2 = 83
            java.lang.String r3 = "AK"
            boolean r0 = r0.a(r1, r2, r3, r10)
            if (r0 == 0) goto L_0x120a
            int r0 = r11.w
            r1 = 10
            if (r0 > r1) goto L_0x11ee
            java.lang.String r0 = "1NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x11ee:
            int r0 = r11.w
            r1 = 13
            if (r0 > r1) goto L_0x11fc
            java.lang.String r0 = "2NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x11fc:
            int r0 = r11.w
            r1 = 15
            if (r0 > r1) goto L_0x120a
            java.lang.String r0 = "3NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x120a:
            byte[] r0 = r11.x
            byte r0 = r0[r7]
            if (r0 > r7) goto L_0x1232
            a.b r0 = r11.b
            byte r1 = r11.f98a
            r2 = 83
            java.lang.String r3 = "AKQ"
            r4 = 0
            boolean r0 = r0.a(r1, r2, r3, r4)
            if (r0 == 0) goto L_0x1232
            int r0 = r11.w
            r1 = 13
            if (r0 < r1) goto L_0x1232
            java.lang.String r0 = "Fourth suit forcing (not-spade suit)."
            r11.a(r0)
            java.lang.String r0 = "2S"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x1232:
            byte[] r0 = r11.x
            byte r0 = r0[r10]
            r1 = 6
            if (r0 < r1) goto L_0x1541
            java.lang.String r0 = "2D"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x1241:
            java.lang.String r1 = "1C-1D-1S"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x12f2
            byte[] r0 = r11.x
            byte r0 = r0[r7]
            if (r0 < r8) goto L_0x127d
            int r0 = r11.w
            r1 = 16
            if (r0 < r1) goto L_0x125d
            java.lang.String r0 = "3S"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x125d:
            int r0 = r11.w
            r1 = 13
            if (r0 < r1) goto L_0x126b
            java.lang.String r0 = "4S"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x126b:
            int r0 = r11.w
            r1 = 10
            if (r0 < r1) goto L_0x1279
            java.lang.String r0 = "2S"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x1279:
            r0 = 99
            goto L_0x0019
        L_0x127d:
            byte[] r0 = r11.x
            byte r0 = r0[r6]
            if (r0 < r7) goto L_0x12bb
            a.b r0 = r11.b
            byte r1 = r11.f98a
            r2 = 72
            java.lang.String r3 = "AK"
            boolean r0 = r0.a(r1, r2, r3, r10)
            if (r0 == 0) goto L_0x12bb
            int r0 = r11.w
            r1 = 10
            if (r0 > r1) goto L_0x129f
            java.lang.String r0 = "1NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x129f:
            int r0 = r11.w
            r1 = 13
            if (r0 > r1) goto L_0x12ad
            java.lang.String r0 = "2NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x12ad:
            int r0 = r11.w
            r1 = 15
            if (r0 > r1) goto L_0x12bb
            java.lang.String r0 = "3NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x12bb:
            byte[] r0 = r11.x
            byte r0 = r0[r6]
            if (r0 > r7) goto L_0x12e3
            a.b r0 = r11.b
            byte r1 = r11.f98a
            r2 = 72
            java.lang.String r3 = "AKQ"
            r4 = 0
            boolean r0 = r0.a(r1, r2, r3, r4)
            if (r0 == 0) goto L_0x12e3
            int r0 = r11.w
            r1 = 13
            if (r0 < r1) goto L_0x12e3
            java.lang.String r0 = "Fourth suit forcing (not-heart suit)."
            r11.a(r0)
            java.lang.String r0 = "2H"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x12e3:
            byte[] r0 = r11.x
            byte r0 = r0[r10]
            r1 = 6
            if (r0 < r1) goto L_0x1541
            java.lang.String r0 = "2D"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x12f2:
            java.lang.String r1 = "1NT-2C-2H"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x13b8
            int r0 = r11.w
            r1 = 9
            if (r0 > r1) goto L_0x130e
            byte[] r0 = r11.x
            byte r0 = r0[r7]
            if (r0 < r9) goto L_0x130e
            java.lang.String r0 = "2S"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x130e:
            int r0 = r11.w
            r1 = 9
            if (r0 > r1) goto L_0x1322
            byte[] r0 = r11.x
            byte r0 = r0[r6]
            if (r0 < r8) goto L_0x1322
            java.lang.String r0 = "3H"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x1322:
            int r0 = r11.w
            r1 = 9
            if (r0 > r1) goto L_0x1330
            java.lang.String r0 = "2NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x1330:
            int r0 = r11.w
            r1 = 12
            if (r0 > r1) goto L_0x1344
            byte[] r0 = r11.x
            byte r0 = r0[r6]
            if (r0 < r8) goto L_0x1344
            java.lang.String r0 = "4H"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x1344:
            int r0 = r11.w
            a.b r1 = r11.b
            byte r2 = r11.f98a
            r3 = 72
            r4 = 321(0x141, float:4.5E-43)
            int r1 = r1.a(r2, r3, r4)
            int r0 = r0 + r1
            r1 = 17
            if (r0 < r1) goto L_0x1365
            byte[] r0 = r11.x
            byte r0 = r0[r6]
            if (r0 < r8) goto L_0x1365
            java.lang.String r0 = "4NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x1365:
            int r0 = r11.w
            r1 = 13
            if (r0 < r1) goto L_0x1379
            byte[] r0 = r11.x
            byte r0 = r0[r7]
            if (r0 < r9) goto L_0x1379
            java.lang.String r0 = "3S"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x1379:
            int r0 = r11.w
            r1 = 13
            if (r0 < r1) goto L_0x138d
            byte[] r0 = r11.x
            byte r0 = r0[r10]
            if (r0 < r9) goto L_0x138d
            java.lang.String r0 = "3D"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x138d:
            int r0 = r11.w
            r1 = 13
            if (r0 < r1) goto L_0x13a2
            byte[] r0 = r11.x
            r1 = 0
            byte r0 = r0[r1]
            if (r0 < r9) goto L_0x13a2
            java.lang.String r0 = "3C"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x13a2:
            byte[] r0 = r11.x
            byte r0 = r0[r6]
            if (r0 < r8) goto L_0x13b0
            java.lang.String r0 = "4H"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x13b0:
            java.lang.String r0 = "3NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x13b8:
            java.lang.String r1 = "1NT-2C-2S"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x1449
            int r0 = r11.w
            r1 = 9
            if (r0 > r1) goto L_0x13d4
            byte[] r0 = r11.x
            byte r0 = r0[r7]
            if (r0 < r8) goto L_0x13d4
            java.lang.String r0 = "3S"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x13d4:
            int r0 = r11.w
            r1 = 9
            if (r0 > r1) goto L_0x13e2
            java.lang.String r0 = "2NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x13e2:
            int r0 = r11.w
            r1 = 12
            if (r0 > r1) goto L_0x13f6
            byte[] r0 = r11.x
            byte r0 = r0[r7]
            if (r0 < r8) goto L_0x13f6
            java.lang.String r0 = "4S"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x13f6:
            int r0 = r11.w
            r1 = 12
            if (r0 > r1) goto L_0x1404
            java.lang.String r0 = "3NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x1404:
            int r0 = r11.w
            r1 = 13
            if (r0 < r1) goto L_0x1418
            byte[] r0 = r11.x
            byte r0 = r0[r6]
            if (r0 < r9) goto L_0x1418
            java.lang.String r0 = "3H"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x1418:
            int r0 = r11.w
            r1 = 13
            if (r0 < r1) goto L_0x142c
            byte[] r0 = r11.x
            byte r0 = r0[r10]
            if (r0 < r9) goto L_0x142c
            java.lang.String r0 = "3D"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x142c:
            int r0 = r11.w
            r1 = 13
            if (r0 < r1) goto L_0x1441
            byte[] r0 = r11.x
            r1 = 0
            byte r0 = r0[r1]
            if (r0 < r9) goto L_0x1441
            java.lang.String r0 = "3C"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x1441:
            java.lang.String r0 = "3NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x1449:
            java.lang.String r1 = "2NT-3C-3D"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x1459
            java.lang.String r0 = "3NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x1459:
            java.lang.String r1 = "2NT-3C-3H"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x14ac
            byte[] r0 = r11.x
            byte r0 = r0[r6]
            if (r0 < r9) goto L_0x1482
            int r0 = r11.w
            a.b r1 = r11.b
            byte r2 = r11.f98a
            r3 = 72
            r4 = 321(0x141, float:4.5E-43)
            int r1 = r1.a(r2, r3, r4)
            int r0 = r0 + r1
            r1 = 11
            if (r0 < r1) goto L_0x1482
            java.lang.String r0 = "4NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x1482:
            byte[] r0 = r11.x
            byte r0 = r0[r6]
            if (r0 < r8) goto L_0x1490
            java.lang.String r0 = "4H"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x1490:
            byte[] r0 = r11.x
            byte r0 = r0[r7]
            if (r0 < r9) goto L_0x14a4
            int r0 = r11.w
            r1 = 11
            if (r0 < r1) goto L_0x14a4
            java.lang.String r0 = "3S"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x14a4:
            java.lang.String r0 = "3NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x14ac:
            java.lang.String r1 = "2NT-3C-3S"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x14eb
            byte[] r0 = r11.x
            byte r0 = r0[r7]
            if (r0 < r9) goto L_0x14d5
            int r0 = r11.w
            a.b r1 = r11.b
            byte r2 = r11.f98a
            r3 = 72
            r4 = 321(0x141, float:4.5E-43)
            int r1 = r1.a(r2, r3, r4)
            int r0 = r0 + r1
            r1 = 11
            if (r0 < r1) goto L_0x14d5
            java.lang.String r0 = "4NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x14d5:
            byte[] r0 = r11.x
            byte r0 = r0[r7]
            if (r0 < r8) goto L_0x14e3
            java.lang.String r0 = "4S"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x14e3:
            java.lang.String r0 = "3NT"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x14eb:
            java.lang.String r1 = "[123]NT-4C"
            boolean r1 = r0.matches(r1)
            if (r1 == 0) goto L_0x14fd
            java.lang.String r0 = "4C"
            java.lang.String r1 = "04123"
            byte r0 = r11.a(r0, r1)
            goto L_0x0019
        L_0x14fd:
            java.lang.String r1 = "[123]NT-4C-(4[SHD]|4NT)-5C"
            boolean r1 = r0.matches(r1)
            if (r1 == 0) goto L_0x150f
            java.lang.String r0 = "5C"
            java.lang.String r1 = "04123"
            byte r0 = r11.a(r0, r1)
            goto L_0x0019
        L_0x150f:
            java.lang.String r1 = "4NT"
            boolean r1 = r0.endsWith(r1)
            if (r1 == 0) goto L_0x1521
            java.lang.String r0 = "4NT"
            java.lang.String r1 = "04123"
            byte r0 = r11.a(r0, r1)
            goto L_0x0019
        L_0x1521:
            java.lang.String r1 = "5NT"
            boolean r1 = r0.endsWith(r1)
            if (r1 == 0) goto L_0x1533
            java.lang.String r0 = "5NT"
            java.lang.String r1 = "01234"
            byte r0 = r11.a(r0, r1)
            goto L_0x0019
        L_0x1533:
            java.lang.String r1 = "2C"
            boolean r1 = r0.startsWith(r1)
            if (r1 == 0) goto L_0x1541
            byte r0 = r11.b(r0)
            goto L_0x0019
        L_0x1541:
            a.b r0 = r11.b
            byte r0 = r0.e()
            int r0 = r0 - r10
            int r0 = r11.a(r0)
            int r1 = net.weweweb.android.bridge.y.az
            if (r0 != r1) goto L_0x159c
            a.b r1 = r11.b
            r2 = -1
            byte r1 = r1.b(r2)
            java.lang.String r2 = "2H"
            byte r2 = a.b.a(r2)
            if (r1 != r2) goto L_0x1576
            int r0 = r11.w
            r1 = 6
            if (r0 <= r1) goto L_0x1572
            byte[] r0 = r11.x
            byte r0 = r0[r6]
            if (r0 <= r9) goto L_0x1572
            java.lang.String r0 = "3H"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x1572:
            r0 = 99
            goto L_0x0019
        L_0x1576:
            a.b r1 = r11.b
            r2 = -1
            byte r1 = r1.b(r2)
            java.lang.String r2 = "2S"
            byte r2 = a.b.a(r2)
            if (r1 != r2) goto L_0x159c
            int r0 = r11.w
            r1 = 6
            if (r0 <= r1) goto L_0x1598
            byte[] r0 = r11.x
            byte r0 = r0[r7]
            if (r0 <= r9) goto L_0x1598
            java.lang.String r0 = "3S"
            byte r0 = a.b.a(r0)
            goto L_0x0019
        L_0x1598:
            r0 = 99
            goto L_0x0019
        L_0x159c:
            int r1 = net.weweweb.android.bridge.y.aB
            if (r0 != r1) goto L_0x15a6
            byte r0 = r11.c()
            goto L_0x0019
        L_0x15a6:
            a.b r0 = r11.b
            r1 = -1
            byte r0 = r0.b(r1)
            r1 = 99
            if (r0 != r1) goto L_0x15b7
            a.b r0 = r11.b
            byte r0 = r0.w()
        L_0x15b7:
            byte r1 = a.b.e(r0)
            a.b r2 = r11.b
            byte r3 = r11.f98a
            boolean r2 = r2.H(r3)
            if (r2 == 0) goto L_0x16a8
            int r2 = r11.w
            r3 = 16
            if (r2 < r3) goto L_0x15d9
            byte r0 = r11.b()
            r1 = 99
            if (r0 == r1) goto L_0x16d0
            byte r0 = r11.b()
            goto L_0x0019
        L_0x15d9:
            byte r2 = r11.f98a
            byte r2 = a.b.m(r2)
            r3 = 0
        L_0x15e0:
            a.b r4 = r11.b
            byte r4 = r4.e()
            if (r3 > r4) goto L_0x1611
            a.b r4 = r11.b
            byte r4 = r4.p(r3)
            if (r4 != r2) goto L_0x160d
            int[] r4 = net.weweweb.android.bridge.y.aK
            int r5 = r11.a(r3)
            int r4 = a.g.a(r4, r5)
            if (r4 < 0) goto L_0x160d
            r2 = r10
        L_0x15fd:
            if (r2 == 0) goto L_0x1613
            byte r0 = r11.b()
            r1 = 99
            if (r0 == r1) goto L_0x16d0
            byte r0 = r11.b()
            goto L_0x0019
        L_0x160d:
            int r3 = r3 + 1
            byte r3 = (byte) r3
            goto L_0x15e0
        L_0x1611:
            r2 = 0
            goto L_0x15fd
        L_0x1613:
            boolean r2 = a.b.D(r0)
            if (r2 != 0) goto L_0x16d0
            boolean r0 = a.b.G(r0)
            if (r0 == 0) goto L_0x1643
            a.b r0 = r11.b
            byte r1 = r11.f98a
            r2 = 0
            boolean r0 = r0.a(r1, r2)
            if (r0 == 0) goto L_0x162e
            r0 = 99
            goto L_0x0019
        L_0x162e:
            byte r0 = r11.f98a
            byte r0 = r11.f(r0)
            r1 = -1
            if (r0 == r1) goto L_0x16d0
            byte r0 = r11.f98a
            byte r0 = r11.f(r0)
            byte r0 = r11.e(r0)
            goto L_0x0019
        L_0x1643:
            byte r0 = r11.f98a
            boolean r0 = r11.a(r0, r1)
            if (r0 != 0) goto L_0x16d0
            byte r0 = r11.f98a
            boolean r0 = r11.d(r0, r1)
            if (r0 == 0) goto L_0x1659
            byte r0 = r11.e(r1)
            goto L_0x0019
        L_0x1659:
            byte r0 = r11.f98a
            byte r0 = r11.f(r0)
            r2 = -1
            if (r0 == r2) goto L_0x167a
            byte r0 = r11.f98a
            byte r0 = r11.f(r0)
            byte r0 = a.b.c(r0)
            if (r0 > r7) goto L_0x167a
            byte r0 = r11.f98a
            byte r0 = r11.f(r0)
            byte r0 = r11.e(r0)
            goto L_0x0019
        L_0x167a:
            a.b r0 = r11.b
            byte r2 = r11.f98a
            char r1 = a.b.q(r1)
            int r0 = r0.c(r2, r1)
            if (r0 > r10) goto L_0x16d0
            a.b r0 = r11.b
            byte r1 = r11.f98a
            byte r0 = r0.a(r1, r10)
            byte r0 = r11.e(r0)
            byte r0 = a.b.c(r0)
            if (r0 > r6) goto L_0x16d0
            a.b r0 = r11.b
            byte r1 = r11.f98a
            byte r0 = r0.a(r1, r10)
            byte r0 = r11.e(r0)
            goto L_0x0019
        L_0x16a8:
            a.b r1 = r11.b
            byte r2 = r11.f98a
            byte r2 = a.b.m(r2)
            boolean r1 = r1.H(r2)
            if (r1 == 0) goto L_0x16d0
            int r1 = r11.w
            r2 = 13
            if (r1 < r2) goto L_0x16d0
            boolean r0 = a.b.D(r0)
            if (r0 != 0) goto L_0x16d0
            byte r0 = r11.b()
            r1 = 99
            if (r0 == r1) goto L_0x16d0
            byte r0 = r11.b()
            goto L_0x0019
        L_0x16d0:
            r0 = 99
            goto L_0x0019
        */
        throw new UnsupportedOperationException("Method not decompiled: net.weweweb.android.bridge.y.f():byte");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.b.c(byte, char):int
     arg types: [byte, int]
     candidates:
      a.b.c(byte, byte):byte
      a.b.c(byte, char):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.b.a(byte, char, java.lang.String, int):boolean
     arg types: [byte, int, java.lang.String, int]
     candidates:
      a.b.a(boolean, byte, byte, byte):int
      a.b.a(byte, char, java.lang.String, int):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.b.a(byte, boolean):boolean
     arg types: [byte, int]
     candidates:
      a.b.a(int, byte):byte
      a.b.a(byte[], byte):byte
      a.b.a(byte[][], int):byte
      a.b.a(byte, byte):java.lang.String
      a.b.a(byte[], boolean):void
      a.b.a(byte, int):byte
      a.b.a(int, int):byte
      a.b.a(byte, char):int
      a.b.a(byte, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.b.b(byte, char):int
     arg types: [byte, int]
     candidates:
      a.b.b(byte, byte):byte
      a.b.b(byte, int):char
      a.b.b(byte, char):int */
    private byte b(String str) {
        if (!str.equals("2C")) {
            if (str.equals("2C-2D")) {
                if (this.b.c(this.f98a, 'H') >= 6 && this.b.b(this.f98a, 'H') >= 9) {
                    return b.a("3H");
                }
                if (this.b.c(this.f98a, 'S') >= 6 && this.b.b(this.f98a, 'S') >= 9) {
                    return b.a("3S");
                }
                if (this.b.c(this.f98a, 'C') >= 5 && this.b.a(this.f98a, 'C', "AKQ", 2)) {
                    return b.a("3C");
                }
                if (this.b.c(this.f98a, 'D') >= 5 && this.b.a(this.f98a, 'D', "AKQ", 2)) {
                    return b.a("3D");
                }
                if (this.b.c(this.f98a, 'H') >= 5 && this.b.a(this.f98a, 'H', "AKQ", 2)) {
                    return b.a("2H");
                }
                if (this.b.c(this.f98a, 'S') >= 5 && this.b.a(this.f98a, 'S', "AKQ", 2)) {
                    return b.a("2S");
                }
                if (this.w >= 22 && this.w <= 24) {
                    return b.a("2NT");
                }
                if (this.w >= 25 && this.w <= 27) {
                    return b.a("3NT");
                }
                if (this.w >= 28 && this.w <= 30) {
                    return b.a("4NT");
                }
                if (this.w >= 31 && this.w <= 33) {
                    return b.a("5NT");
                }
                if (this.w >= 34 && this.w <= 36) {
                    return b.a("6NT");
                }
                if (this.w >= 37) {
                    return b.a("7NT");
                }
            } else if (str.equals("2C-2D-2H")) {
                if (this.w <= 4) {
                    a("Double negative.");
                    return b.a("3C");
                } else if (this.x[2] >= 3) {
                    return b.a("3H");
                } else {
                    if (this.x[3] >= 5) {
                        return b.a("2S");
                    }
                    if (this.x[1] >= 5) {
                        return b.a("3D");
                    }
                    if (this.x[0] >= 6) {
                        return b.a("4C");
                    }
                    return b.a("2NT");
                }
            } else if (str.equals("2C-2D-2S")) {
                if (this.w <= 4) {
                    a("Double negative.");
                    return b.a("3C");
                } else if (this.x[3] >= 3) {
                    return b.a("3S");
                } else {
                    if (this.x[2] >= 5) {
                        return b.a("3H");
                    }
                    if (this.x[1] >= 5) {
                        return b.a("3D");
                    }
                    if (this.x[0] >= 6) {
                        return b.a("4C");
                    }
                    return b.a("2NT");
                }
            } else if (str.equals("2C-2D-2NT")) {
                if (this.w <= 3) {
                    return 99;
                }
                if (this.x[3] >= 5) {
                    return b.a("3S");
                }
                if (this.x[2] >= 5) {
                    return b.a("3H");
                }
                if (this.x[1] >= 5) {
                    return b.a("3D");
                }
                if (this.x[0] >= 5) {
                    return b.a("3C");
                }
                return b.a("3NT");
            } else if (str.equals("2C-2D-3C")) {
                if (this.w <= 4) {
                    a("Double negative.");
                    return b.a("3D");
                } else if (this.x[0] >= 4) {
                    return b.a("4C");
                } else {
                    if (this.x[3] >= 5) {
                        return b.a("3S");
                    }
                    if (this.x[2] >= 5) {
                        return b.a("3H");
                    }
                    if (this.x[1] >= 6) {
                        return b.a("4D");
                    }
                    return b.a("3NT");
                }
            } else if (str.equals("2C-2D-3D")) {
                if (this.w <= 4) {
                    a("Double negative.");
                    return b.a("3NT");
                } else if (this.x[1] >= 4) {
                    return b.a("4D");
                } else {
                    if (this.x[3] >= 5) {
                        return b.a("3S");
                    }
                    if (this.x[2] >= 5) {
                        return b.a("3H");
                    }
                    if (this.x[0] >= 6) {
                        return b.a("4C");
                    }
                    return b.a("3NT");
                }
            } else if (str.equals("2C-2D-2H-3C")) {
                return b.a("3H");
            } else {
                if (str.equals("2C-2D-2S-3C")) {
                    return b.a("3S");
                }
                if (str.matches("2C-([23]([SHDC]|NT)-)*[23]NT-4C")) {
                    return a("4C", "04123");
                }
                if (str.matches("2C-([23]([SHDC]|NT)-)*[23]NT-4C-4([SHD]|NT)-5C")) {
                    return a("5C", "04123");
                }
            }
            return 99;
        } else if (this.w >= 8 && this.b.c(this.f98a, 'H') >= 5 && this.b.a(this.f98a, 'H', "AKQ", 2)) {
            return b.a("2H");
        } else {
            if (this.w >= 8 && this.b.c(this.f98a, 'S') >= 5 && this.b.a(this.f98a, 'S', "AKQ", 2)) {
                return b.a("2S");
            }
            if (this.w >= 8 && this.b.c(this.f98a, 'C') >= 5 && this.b.a(this.f98a, 'C', "AKQ", 2)) {
                return b.a("3C");
            }
            if (this.w >= 8 && this.b.c(this.f98a, 'D') >= 5 && this.b.a(this.f98a, 'D', "AKQ", 2)) {
                return b.a("3D");
            }
            if (this.w >= 8 && this.b.a(this.f98a, true)) {
                return b.a("2NT");
            }
            a("Negative or waiting.");
            return b.a("2D");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.b.a(byte, char, java.lang.String, int):boolean
     arg types: [byte, int, java.lang.String, int]
     candidates:
      a.b.a(boolean, byte, byte, byte):int
      a.b.a(byte, char, java.lang.String, int):boolean */
    private byte g() {
        String str;
        int[] iArr = new int[4];
        g.b(iArr, 0);
        if (this.b.M()) {
            if (b.h(this.b.k()) == 7) {
                if (this.b.a(this.f98a, 'S', "A", 1)) {
                    return d.a("SA");
                }
                if (this.b.a(this.f98a, 'H', "A", 1)) {
                    return d.a("HA");
                }
                if (this.b.a(this.f98a, 'D', "A", 1)) {
                    return d.a("DA");
                }
                if (this.b.a(this.f98a, 'C', "A", 1)) {
                    return d.a("CA");
                }
            }
            for (int i2 = 0; i2 < 4; i2++) {
                String a2 = b.a(this.b.l[this.f98a], (byte) i2, 10);
                if (this.x[i2] == 4) {
                    iArr[i2] = iArr[i2] + 1;
                }
                if (this.x[i2] == 5) {
                    iArr[i2] = iArr[i2] + 2;
                }
                if (this.x[i2] >= 6) {
                    iArr[i2] = iArr[i2] + 4;
                }
                if (this.b.b(this.f98a, b.q((byte) i2)) > 4) {
                    iArr[i2] = iArr[i2] + 2;
                }
                if (this.b.b(this.f98a, b.q((byte) i2)) > 7) {
                    iArr[i2] = iArr[i2] + 2;
                }
                if (this.H[b.m(this.f98a)][i2] == 1) {
                    iArr[i2] = iArr[i2] + 2;
                }
                if (this.H[b.m(this.f98a)][i2] >= 2) {
                    iArr[i2] = iArr[i2] + 3;
                }
                if (this.H[b.l(this.f98a)][i2] >= 2) {
                    iArr[i2] = iArr[i2] - 2;
                }
                if (this.H[b.o(this.f98a)][i2] >= 2) {
                    iArr[i2] = iArr[i2] - 4;
                }
                if (a2.startsWith("AQT")) {
                    iArr[i2] = iArr[i2] - 3;
                }
                if (a2.startsWith("AQX")) {
                    iArr[i2] = iArr[i2] - 3;
                }
                if (a2.startsWith("KJX")) {
                    iArr[i2] = iArr[i2] - 3;
                }
                if (a2.startsWith("AKQJ")) {
                    iArr[i2] = iArr[i2] + 5;
                }
                if (a2.equals("KX")) {
                    iArr[i2] = iArr[i2] - 2;
                } else if (a2.startsWith("AKQ")) {
                    iArr[i2] = iArr[i2] + 3;
                } else if (a2.startsWith("AQJ")) {
                    iArr[i2] = iArr[i2] + 1;
                } else if (a2.startsWith("AJT")) {
                    iArr[i2] = iArr[i2] + 1;
                } else if (a2.startsWith("KQJ")) {
                    iArr[i2] = iArr[i2] + 3;
                } else if (a2.startsWith("AK")) {
                    iArr[i2] = iArr[i2] + 2;
                } else if (a2.startsWith("KQ")) {
                    iArr[i2] = iArr[i2] + 2;
                } else if (a2.startsWith("QJT")) {
                    iArr[i2] = iArr[i2] + 2;
                } else if (a2.startsWith("QJ")) {
                    iArr[i2] = iArr[i2] + 1;
                } else if (a2.startsWith("JT")) {
                    iArr[i2] = iArr[i2] + 1;
                }
            }
            str = "N";
        } else {
            byte i3 = b.i(this.b.k());
            for (int i4 = 0; i4 < 4; i4++) {
                String a3 = b.a(this.b.l[this.f98a], (byte) i4, 10);
                if (this.x[i4] == 1 && i4 != i3) {
                    iArr[i4] = iArr[i4] + 3;
                }
                if (this.x[i4] == 2 && i4 != i3) {
                    iArr[i4] = iArr[i4] + 1;
                }
                if (i4 == i3) {
                    iArr[i4] = iArr[i4] - 2;
                }
                if (this.b.b(this.f98a, b.q((byte) i4)) > 4) {
                    iArr[i4] = iArr[i4] + 2;
                }
                if (this.b.b(this.f98a, b.q((byte) i4)) > 7) {
                    iArr[i4] = iArr[i4] + 2;
                }
                if (this.H[b.m(this.f98a)][i4] == 1) {
                    iArr[i4] = iArr[i4] + 2;
                }
                if (this.H[b.m(this.f98a)][i4] >= 2) {
                    iArr[i4] = iArr[i4] + 3;
                }
                if (this.H[b.l(this.f98a)][i4] >= 2) {
                    iArr[i4] = iArr[i4] - 2;
                }
                if (this.H[b.o(this.f98a)][i4] >= 2) {
                    iArr[i4] = iArr[i4] - 4;
                }
                if (a3.startsWith("AQT")) {
                    iArr[i4] = iArr[i4] - 3;
                }
                if (a3.startsWith("AQX")) {
                    iArr[i4] = iArr[i4] - 3;
                }
                if (a3.startsWith("KJX")) {
                    iArr[i4] = iArr[i4] - 2;
                }
                if (a3.equals("KX")) {
                    iArr[i4] = iArr[i4] - 2;
                }
                if (a3.equals("AX") && i4 != i3) {
                    iArr[i4] = iArr[i4] + 3;
                }
                if (a3.startsWith("AKQJ")) {
                    iArr[i4] = iArr[i4] + 5;
                } else if (a3.startsWith("AKQ")) {
                    iArr[i4] = iArr[i4] + 3;
                } else if (a3.startsWith("AQJ")) {
                    iArr[i4] = iArr[i4] + 1;
                } else if (a3.startsWith("AJT")) {
                    iArr[i4] = iArr[i4] + 1;
                } else if (a3.startsWith("KQJ")) {
                    iArr[i4] = iArr[i4] + 3;
                } else if (a3.startsWith("AK")) {
                    iArr[i4] = iArr[i4] + 2;
                } else if (a3.startsWith("KQ")) {
                    iArr[i4] = iArr[i4] + 2;
                } else if (a3.startsWith("QJT")) {
                    iArr[i4] = iArr[i4] + 2;
                } else if (a3.startsWith("QJ")) {
                    iArr[i4] = iArr[i4] + 1;
                } else if (a3.startsWith("JT")) {
                    iArr[i4] = iArr[i4] + 1;
                }
            }
            str = "S";
        }
        byte a4 = (byte) g.a(iArr);
        if (this.x[a4] == 1) {
            return this.b.a(this.f98a, a4, 1);
        }
        String a5 = b.a(this.b.l[this.f98a], a4, 9);
        for (int i5 = 0; i5 < this.K.length; i5++) {
            if (this.K[i5].startsWith(str)) {
                try {
                    String substring = this.K[i5].substring(3, this.K[i5].length());
                    int parseInt = Integer.parseInt(this.K[i5].substring(2, 3));
                    if (str.equals("N") && this.x[a4] >= 8 && a5.startsWith("AK")) {
                        return this.b.a(this.f98a, a4, 1);
                    }
                    if (this.K[i5].startsWith(str + "S")) {
                        if (a5.startsWith(substring)) {
                            return this.b.a(this.f98a, a4, parseInt);
                        }
                    } else if (this.K[i5].startsWith(str + "E") && a5.equals(substring)) {
                        return this.b.a(this.f98a, a4, parseInt);
                    }
                } catch (Exception e2) {
                }
            }
        }
        if (this.x[a4] >= 4) {
            return this.b.a(this.f98a, a4, 4);
        }
        if (this.x[a4] == 3) {
            return this.b.a(this.f98a, a4, 3);
        }
        if (this.x[a4] == 2) {
            return this.b.a(this.f98a, a4, 1);
        }
        Log.d("First Lead", "selSuit=" + ((int) a4) + ",card=" + b.a(this.b.l[this.f98a], a4, 2));
        return -1;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.b.a(int, int):byte
     arg types: [byte, byte]
     candidates:
      a.b.a(int, byte):byte
      a.b.a(byte[], byte):byte
      a.b.a(byte[][], int):byte
      a.b.a(byte, byte):java.lang.String
      a.b.a(byte[], boolean):void
      a.b.a(byte, int):byte
      a.b.a(byte, char):int
      a.b.a(byte, boolean):boolean
      a.b.a(int, int):byte */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.g.a(byte[], int, boolean, byte[]):void
     arg types: [byte[], int, int, byte[]]
     candidates:
      a.g.a(java.lang.String, java.lang.String, java.lang.String, java.lang.String):java.lang.String
      a.g.a(byte[], int, boolean, byte[]):void */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00f5, code lost:
        if (a.b.g(r1) != r5) goto L_0x00f7;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private byte a(a.b r12, byte[] r13) {
        /*
            r11 = this;
            r0 = 0
            byte r1 = r12.C()
            byte r2 = r12.t(r1)
            byte r3 = r12.j()
            byte r4 = r12.s(r1)
            byte r5 = r12.B()
            byte r6 = r12.t(r1)
            if (r3 != r6) goto L_0x003c
            r2 = 0
        L_0x001c:
            byte[][] r4 = r12.l
            r4 = r4[r3]
            int r4 = r4.length
            int r4 = r4 - r1
            if (r2 >= r4) goto L_0x00f7
            byte[][] r4 = r12.l
            r4 = r4[r3]
            byte r4 = r4[r2]
            r5 = -1
            if (r4 == r5) goto L_0x0039
            int r4 = r0 + 1
            byte r4 = (byte) r4
            byte[][] r5 = r12.l
            r5 = r5[r3]
            byte r5 = r5[r2]
            r13[r0] = r5
            r0 = r4
        L_0x0039:
            int r2 = r2 + 1
            goto L_0x001c
        L_0x003c:
            boolean r6 = r12.j(r3, r4)
            if (r6 != 0) goto L_0x006e
            r2 = 0
        L_0x0043:
            byte r4 = r12.g()
            int r4 = r4 - r1
            if (r2 >= r4) goto L_0x00f7
            byte[][] r4 = r12.l
            r4 = r4[r3]
            byte r4 = r4[r2]
            r5 = -1
            if (r4 == r5) goto L_0x006b
            byte[][] r4 = r12.l
            r4 = r4[r3]
            byte r4 = r4[r2]
            boolean r4 = r12.k(r4, r3)
            if (r4 == 0) goto L_0x006b
            int r4 = r0 + 1
            byte r4 = (byte) r4
            byte[][] r5 = r12.l
            r5 = r5[r3]
            byte r5 = r5[r2]
            r13[r0] = r5
            r0 = r4
        L_0x006b:
            int r2 = r2 + 1
            goto L_0x0043
        L_0x006e:
            r6 = 4
            byte[] r6 = new byte[r6]
            r7 = -1
            a.g.b(r6, r7)
            r7 = 0
        L_0x0076:
            byte[][] r8 = r12.l
            r8 = r8[r3]
            int r8 = r8.length
            int r8 = r8 - r1
            if (r7 >= r8) goto L_0x00d1
            byte[][] r8 = r12.l
            r8 = r8[r3]
            byte r8 = r8[r7]
            r9 = -1
            if (r8 == r9) goto L_0x009f
            byte[][] r8 = r12.l
            r8 = r8[r3]
            byte r8 = r8[r7]
            byte r8 = a.b.g(r8)
            if (r8 != r5) goto L_0x00a2
            int r8 = r0 + 1
            byte r8 = (byte) r8
            byte[][] r9 = r12.l
            r9 = r9[r3]
            byte r9 = r9[r7]
            r13[r0] = r9
            r0 = r8
        L_0x009f:
            int r7 = r7 + 1
            goto L_0x0076
        L_0x00a2:
            byte r9 = r6[r8]
            r10 = -1
            if (r9 != r10) goto L_0x00b6
            r6[r8] = r0
            int r8 = r0 + 1
            byte r8 = (byte) r8
            byte[][] r9 = r12.l
            r9 = r9[r3]
            byte r9 = r9[r7]
            r13[r0] = r9
            r0 = r8
            goto L_0x009f
        L_0x00b6:
            byte[][] r9 = r12.l
            r9 = r9[r3]
            byte r9 = r9[r7]
            byte r10 = r6[r8]
            byte r10 = r13[r10]
            boolean r9 = a.b.b(r9, r10, r5)
            if (r9 == 0) goto L_0x009f
            byte r8 = r6[r8]
            byte[][] r9 = r12.l
            r9 = r9[r3]
            byte r9 = r9[r7]
            r13[r8] = r9
            goto L_0x009f
        L_0x00d1:
            boolean r6 = r12.M()
            if (r6 != 0) goto L_0x00f7
            if (r4 == r5) goto L_0x00f7
            byte r4 = a.b.l(r2)
            if (r3 == r4) goto L_0x00f7
            r4 = 1
            if (r0 <= r4) goto L_0x00f7
            r4 = -1
            byte r6 = a.b.m(r2)
            if (r3 != r6) goto L_0x0107
            byte r2 = a.b.l(r2)
            byte r1 = r12.a(r2, r1)
            byte r2 = a.b.g(r1)
            if (r2 == r5) goto L_0x0147
        L_0x00f7:
            r1 = 0
            r2 = 0
        L_0x00f9:
            int r3 = r13.length
            if (r2 >= r3) goto L_0x01a9
            byte r3 = r13[r2]
            if (r3 < 0) goto L_0x0103
            int r1 = r1 + 1
            byte r1 = (byte) r1
        L_0x0103:
            int r2 = r2 + 1
            byte r2 = (byte) r2
            goto L_0x00f9
        L_0x0107:
            byte r6 = a.b.o(r2)
            if (r3 != r6) goto L_0x01eb
            byte r3 = a.b.l(r2)
            byte r3 = r12.a(r3, r1)
            byte r3 = a.b.g(r3)
            if (r3 != r5) goto L_0x017b
            byte r3 = a.b.m(r2)
            byte r3 = r12.a(r3, r1)
            byte r3 = a.b.g(r3)
            if (r3 != r5) goto L_0x017b
            byte r3 = a.b.l(r2)
            byte r3 = r12.a(r3, r1)
            byte r4 = a.b.m(r2)
            byte r4 = r12.a(r4, r1)
            boolean r3 = a.b.b(r3, r4, r5)
            if (r3 == 0) goto L_0x0172
            byte r2 = a.b.m(r2)
            byte r1 = r12.a(r2, r1)
        L_0x0147:
            r2 = 0
        L_0x0148:
            if (r2 >= r0) goto L_0x00f7
            r3 = 1
            if (r0 <= r3) goto L_0x00f7
            byte r3 = r13[r2]
            byte r3 = a.b.g(r3)
            if (r3 != r5) goto L_0x016f
            byte r3 = r13[r2]
            boolean r3 = a.b.b(r1, r3, r5)
            if (r3 != 0) goto L_0x016f
            r3 = 1
            int r3 = r0 - r3
            byte r3 = r13[r3]
            r13[r2] = r3
            r3 = 1
            int r3 = r0 - r3
            r4 = -1
            r13[r3] = r4
            r3 = 1
            int r0 = r0 - r3
            byte r0 = (byte) r0
            int r2 = r2 + -1
        L_0x016f:
            int r2 = r2 + 1
            goto L_0x0148
        L_0x0172:
            byte r2 = a.b.l(r2)
            byte r1 = r12.a(r2, r1)
            goto L_0x0147
        L_0x017b:
            byte r3 = a.b.l(r2)
            byte r3 = r12.a(r3, r1)
            byte r3 = a.b.g(r3)
            if (r3 != r5) goto L_0x0192
            byte r2 = a.b.l(r2)
            byte r1 = r12.a(r2, r1)
            goto L_0x0147
        L_0x0192:
            byte r3 = a.b.m(r2)
            byte r3 = r12.a(r3, r1)
            byte r3 = a.b.g(r3)
            if (r3 != r5) goto L_0x00f7
            byte r2 = a.b.m(r2)
            byte r1 = r12.a(r2, r1)
            goto L_0x0147
        L_0x01a9:
            a.g.a(r13)
            r2 = 1
            int r1 = r1 - r2
            r2 = 1
            byte[] r3 = r11.f
            a.g.a(r13, r1, r2, r3)
            r1 = 0
        L_0x01b5:
            r2 = 1
            int r2 = r0 - r2
            if (r1 >= r2) goto L_0x01ea
            byte[] r2 = a.d.b
            byte r3 = r13[r1]
            byte r2 = r2[r3]
            byte[] r3 = a.d.b
            int r4 = r1 + 1
            byte r4 = r13[r4]
            byte r3 = r3[r4]
            if (r2 != r3) goto L_0x01e7
            byte[] r2 = a.d.d
            byte r3 = r13[r1]
            byte r2 = r2[r3]
            int r2 = r2 + 1
            byte[] r3 = a.d.d
            int r4 = r1 + 1
            byte r4 = r13[r4]
            byte r3 = r3[r4]
            if (r2 != r3) goto L_0x01e7
            r2 = -1
            r13[r1] = r2
            int r1 = r1 + -1
            r2 = 1
            int r0 = r0 - r2
            byte r0 = (byte) r0
            a.b.b(r13)
        L_0x01e7:
            int r1 = r1 + 1
            goto L_0x01b5
        L_0x01ea:
            return r0
        L_0x01eb:
            r1 = r4
            goto L_0x0147
        */
        throw new UnsupportedOperationException("Method not decompiled: net.weweweb.android.bridge.y.a(a.b, byte[]):byte");
    }

    private byte[] b(byte b2) {
        byte[] bArr = new byte[4];
        int i2 = 0;
        for (byte b3 = 3; b3 >= 0; b3 = (byte) (b3 - 1)) {
            if (a(b2, b3)) {
                bArr[i2] = b3;
                i2++;
            }
        }
        if (i2 == 0) {
            return null;
        }
        byte[] bArr2 = new byte[i2];
        System.arraycopy(bArr, 0, bArr2, 0, i2);
        return bArr2;
    }

    private int a(int i2) {
        if (i2 < 0 || i2 >= this.Q.length) {
            return R;
        }
        return this.Q[i2];
    }

    private String c(byte b2) {
        boolean z2 = false;
        StringBuilder sb = new StringBuilder();
        for (int i2 = 0; i2 <= this.b.e(); i2++) {
            if (!b.g(this.b.p((byte) i2), b2) && (this.b.d(i2) != 99 || z2)) {
                z2 = true;
                if (sb.length() == 0) {
                    sb.append(b.d(this.b.d(i2)));
                } else {
                    sb.append("-").append(b.d(this.b.d(i2)));
                }
            }
        }
        return sb.toString();
    }

    private int b(int i2) {
        byte d2 = this.b.d(i2);
        if (!b.z(d2)) {
            return -1;
        }
        for (int i3 = i2 - 1; i3 >= 0; i3--) {
            if (b.z(this.b.d(i3))) {
                byte d3 = this.b.d(i3);
                if (!b.z(d3)) {
                    return -1;
                }
                if (!b.z(d2)) {
                    return -1;
                }
                if (b.c(d3) > b.c(d2)) {
                    return -1;
                }
                return b.e(d3) < b.e(d2) ? b.c(d2) - b.c(d3) : (b.c(d2) - b.c(d3)) - 1;
            }
        }
        return -1;
    }

    private byte[] d(byte b2) {
        byte[] bArr = new byte[4];
        int i2 = 0;
        for (byte b3 = 3; b3 >= 0; b3 = (byte) (b3 - 1)) {
            if (b(b2, b3)) {
                bArr[i2] = b3;
                i2 = (byte) (i2 + 1);
            }
        }
        if (i2 == 0) {
            return null;
        }
        byte[] bArr2 = new byte[i2];
        System.arraycopy(bArr, 0, bArr2, 0, i2);
        return bArr2;
    }

    private boolean a(byte b2, int i2) {
        for (byte b3 = 0; b3 <= this.b.e(); b3 = (byte) (b3 + 1)) {
            if (this.b.p(b3) == b2 && a((int) b3) == i2) {
                return true;
            }
        }
        return false;
    }

    private boolean a(byte b2, byte b3) {
        boolean[] zArr = {c(b2, b3), c(b.m(b2), b3)};
        return zArr[0] && zArr[1];
    }

    private boolean b(byte b2, byte b3) {
        if (a(b2, b3)) {
            return true;
        }
        byte[] a2 = b.a(this.b.l[b2]);
        for (byte b4 = 0; b4 <= this.b.e(); b4 = (byte) (b4 + 1)) {
            byte p2 = this.b.p(b4);
            int a3 = a((int) b4);
            if (b.h(b2, p2) && b.e(this.b.M(b4)) == b3 && a3 != U && a3 != V) {
                if (a3 == Y && a2[b3] >= 2) {
                    return true;
                }
                if (a3 == ac && a2[b3] >= 2) {
                    return true;
                }
                if ((a3 == ad || a3 == aq) && a2[b3] >= 3) {
                    return true;
                }
                if (a3 == ae && a2[b3] >= 2) {
                    return true;
                }
                if (a3 == ag && a2[b3] >= 3) {
                    return true;
                }
                if ((a3 == aj || a3 == ak || a3 == an) && a2[b3] >= 2) {
                    return true;
                }
                if (a3 == ao && a2[b3] >= 2) {
                    return true;
                }
                if (a3 == ar && a2[b3] >= 2) {
                    return true;
                }
                if (a3 == at && a2[b3] >= 2) {
                    return true;
                }
                if (a3 == au && a2[b3] >= 3) {
                    return true;
                }
            }
        }
        return a2[b3] + this.D[b.m(b2)][b3] >= 8;
    }

    /* JADX WARNING: Removed duplicated region for block: B:32:0x005e  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0057 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean c(byte r10, byte r11) {
        /*
            r9 = this;
            r0 = 3
            r8 = 2
            r7 = 1
            r6 = 0
            if (r11 == 0) goto L_0x000e
            if (r11 == r7) goto L_0x000e
            if (r11 == r8) goto L_0x000e
            if (r11 == r0) goto L_0x000e
            r0 = r6
        L_0x000d:
            return r0
        L_0x000e:
            if (r10 < 0) goto L_0x0012
            if (r10 <= r0) goto L_0x0014
        L_0x0012:
            r0 = r6
            goto L_0x000d
        L_0x0014:
            r0 = r6
        L_0x0015:
            a.b r1 = r9.b
            byte r1 = r1.e()
            if (r0 > r1) goto L_0x006c
            a.b r1 = r9.b
            byte r1 = r1.p(r0)
            if (r1 != r10) goto L_0x0068
            a.b r1 = r9.b
            byte r1 = r1.M(r0)
            byte r1 = a.b.e(r1)
            if (r1 != r11) goto L_0x0068
            int r1 = r9.a(r0)
            int[][] r2 = net.weweweb.android.bridge.y.aJ
            a.b r3 = r9.b
            byte r3 = r3.M(r0)
            if (r2 == 0) goto L_0x005c
            r4 = r6
        L_0x0040:
            int r5 = r2.length
            if (r4 >= r5) goto L_0x005c
            r5 = r2[r4]
            int r5 = r5.length
            if (r5 < r8) goto L_0x0059
            r5 = r2[r4]
            r5 = r5[r6]
            if (r5 != r1) goto L_0x0059
            r5 = r2[r4]
            r5 = r5[r7]
            if (r5 != r3) goto L_0x0059
            r2 = r4
        L_0x0055:
            if (r2 < 0) goto L_0x005e
            r0 = r7
            goto L_0x000d
        L_0x0059:
            int r4 = r4 + 1
            goto L_0x0040
        L_0x005c:
            r2 = -1
            goto L_0x0055
        L_0x005e:
            int[] r2 = net.weweweb.android.bridge.y.aI
            int r1 = a.g.a(r2, r1)
            if (r1 < 0) goto L_0x0068
            r0 = r7
            goto L_0x000d
        L_0x0068:
            int r0 = r0 + 1
            byte r0 = (byte) r0
            goto L_0x0015
        L_0x006c:
            r0 = r6
            goto L_0x000d
        */
        throw new UnsupportedOperationException("Method not decompiled: net.weweweb.android.bridge.y.c(byte, byte):boolean");
    }

    private boolean d(byte b2, byte b3) {
        if (b3 < 0 || b3 >= 4) {
            return false;
        }
        if (a(b2, b3)) {
            return true;
        }
        byte[] a2 = b.a(this.b.l[b2]);
        for (byte b4 = 0; b4 <= this.b.e(); b4 = (byte) (b4 + 1)) {
            byte p2 = this.b.p(b4);
            int a3 = a((int) b4);
            if (b.h(b2, p2) && b.e(this.b.M(b4)) == b3 && a3 != U && a3 != V) {
                if (a3 == Y && a2[b3] >= 3) {
                    return true;
                }
                if (a3 == ac && a2[b3] >= 2) {
                    return true;
                }
                if ((a3 == ad || a3 == aq) && a2[b3] >= 4) {
                    return true;
                }
                if (a3 == ae && a2[b3] >= 3) {
                    return true;
                }
                if (a3 == ag && a2[b3] >= 3) {
                    return true;
                }
                if ((a3 == aj || a3 == ak || a3 == an) && a2[b3] >= 4) {
                    return true;
                }
                if (a3 == ao && a2[b3] >= 3) {
                    return true;
                }
                if (a3 == ar && a2[b3] >= 3) {
                    return true;
                }
                if (a3 == at && a2[b3] >= 2) {
                    return true;
                }
                if (a3 == au && a2[b3] >= 3) {
                    return true;
                }
            }
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.b.a(byte, char):int
     arg types: [byte, int]
     candidates:
      a.b.a(int, byte):byte
      a.b.a(byte[], byte):byte
      a.b.a(byte[][], int):byte
      a.b.a(byte, byte):java.lang.String
      a.b.a(byte[], boolean):void
      a.b.a(byte, int):byte
      a.b.a(int, int):byte
      a.b.a(byte, boolean):boolean
      a.b.a(byte, char):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.b.a(int, int):byte
     arg types: [byte, byte]
     candidates:
      a.b.a(int, byte):byte
      a.b.a(byte[], byte):byte
      a.b.a(byte[][], int):byte
      a.b.a(byte, byte):java.lang.String
      a.b.a(byte[], boolean):void
      a.b.a(byte, int):byte
      a.b.a(byte, char):int
      a.b.a(byte, boolean):boolean
      a.b.a(int, int):byte */
    private void h() {
        byte b2;
        byte b3;
        byte b4;
        byte b5;
        byte d2;
        int i2;
        int i3;
        byte b6 = -1;
        this.x = b.a(this.b.l[this.f98a]);
        if (!this.h) {
            this.h = true;
        }
        b(this.i);
        byte C2 = this.i.C();
        if (this.b.P() && C2 == 0) {
            b6 = b.a(this.b.l[this.f98a], g());
        }
        if (b6 == -1 && C2 < 8 && BridgeApp.v == 2) {
            try {
                b bVar = new b();
                b bVar2 = new b();
                long currentTimeMillis = System.currentTimeMillis();
                byte C3 = this.b.C();
                byte j2 = this.b.j();
                byte[] bArr = new byte[(this.b.g() - C3)];
                byte a2 = a(this.b, bArr);
                if (a2 == 0) {
                    b3 = -1;
                } else if (a2 == 1) {
                    b3 = bArr[0];
                } else {
                    int[] iArr = new int[bArr.length];
                    this.b.a(bVar);
                    for (int i4 = 0; i4 < 100; i4++) {
                        if (this.f98a == bVar.t() || this.f98a == bVar.s()) {
                            b4 = b.l(this.f98a);
                            b5 = b.o(this.f98a);
                        } else {
                            b4 = bVar.s();
                            b5 = b.m(this.f98a);
                        }
                        int a3 = bVar.a(b4, '*');
                        int a4 = bVar.a(b5, '*');
                        byte[] bArr2 = new byte[(a3 + a4)];
                        int i5 = 0;
                        for (int i6 = 0; i6 < 13; i6++) {
                            if (bVar.l[b4][i6] != -1) {
                                bArr2[i5] = bVar.l[b4][i6];
                                bVar.l[b4][i6] = -1;
                                i5++;
                            }
                            if (bVar.l[b5][i6] != -1) {
                                bArr2[i5] = bVar.l[b5][i6];
                                bVar.l[b5][i6] = -1;
                                i5++;
                            }
                        }
                        int length = bArr2.length;
                        for (int i7 = 0; i7 < length; i7++) {
                            int b7 = g.b(length);
                            if (b7 != i7) {
                                byte b8 = bArr2[b7];
                                bArr2[b7] = bArr2[i7];
                                bArr2[i7] = b8;
                            }
                        }
                        int i8 = a4;
                        int i9 = a3;
                        int length2 = bArr2.length;
                        for (int i10 = 0; i10 < 4; i10++) {
                            if (this.E[b4][i10] == -1) {
                                int i11 = i8;
                                for (int i12 = 0; i12 < length2; i12++) {
                                    if (b.g(bArr2[i12]) == i10) {
                                        bVar.l[b5][i11 - 1] = bArr2[i12];
                                        i11--;
                                        bArr2[i12] = bArr2[length2];
                                        length2--;
                                    }
                                }
                                i8 = i11;
                            }
                            if (this.E[b5][i10] == -1) {
                                int i13 = i9;
                                for (int i14 = 0; i14 < length2; i14++) {
                                    if (b.g(bArr2[i14]) == i10) {
                                        bVar.l[b4][i13 - 1] = bArr2[i14];
                                        i13--;
                                        bArr2[i14] = bArr2[length2];
                                        length2--;
                                    }
                                }
                                i9 = i13;
                            }
                        }
                        for (int i15 = 0; i15 < length2; i15++) {
                            if (i9 > 0) {
                                bVar.l[b4][i9 - 1] = bArr2[i15];
                                i9--;
                            } else if (i8 > 0) {
                                bVar.l[b5][i8 - 1] = bArr2[i15];
                                i8--;
                            }
                        }
                        b.b(bVar.l[b4]);
                        b.b(bVar.l[b5]);
                        for (int i16 = 0; i16 < a2; i16++) {
                            bVar.a(bVar2);
                            bVar2.N(bArr[i16]);
                            while (!bVar2.K()) {
                                if (bVar2.P()) {
                                    byte C4 = bVar2.C();
                                    byte j3 = bVar2.j();
                                    byte B2 = bVar2.B();
                                    int[] iArr2 = new int[4];
                                    for (byte b9 = 0; b9 < 4; b9 = (byte) (b9 + 1)) {
                                        if (bVar2.j(j3, b9)) {
                                            iArr2[b9] = iArr2[b9] - 999999;
                                        } else {
                                            int c2 = bVar2.c(j3, b.q(b9));
                                            int c3 = bVar2.c(b.m(j3), b.q(b9));
                                            int c4 = bVar2.c(b.l(j3), b.q(b9));
                                            int c5 = bVar2.c(b.o(j3), b.q(b9));
                                            if (!bVar2.M()) {
                                                int c6 = bVar2.c(j3, b.q(B2));
                                                int c7 = bVar2.c(b.m(j3), b.q(B2));
                                                int c8 = bVar2.c(b.l(j3), b.q(B2));
                                                int c9 = bVar2.c(b.o(j3), b.q(B2));
                                                if (b9 != B2 && c2 > 0 && c3 > 0 && c4 == 0 && c5 == 0) {
                                                    if (c8 > 0 && c9 > 0) {
                                                        iArr2[b9] = iArr2[b9] - 8;
                                                        int i17 = c7;
                                                        i2 = c6;
                                                        i3 = i17;
                                                    } else if (c8 > 0 || c9 > 0) {
                                                        iArr2[b9] = iArr2[b9] - 4;
                                                        int i18 = c7;
                                                        i2 = c6;
                                                        i3 = i18;
                                                    }
                                                }
                                                int i19 = c7;
                                                i2 = c6;
                                                i3 = i19;
                                            } else {
                                                i2 = 0;
                                                i3 = 0;
                                            }
                                            if (bVar2.B(j3)) {
                                                if (bVar2.M() && (((c2 > 0 && c3 == 0 && i3 > 0) || (c2 == 0 && c3 > 0 && i2 > 0)) && c4 > 0 && c4 > 0)) {
                                                    iArr2[b9] = iArr2[b9] + 9;
                                                }
                                            } else if (!bVar2.M()) {
                                                if (b9 != B2) {
                                                    if (((c2 >= 3 && c3 == 2 && i3 > 0) || (c2 == 2 && c3 >= 3 && i2 > 0)) && c4 >= 3 && c4 >= 3) {
                                                        iArr2[b9] = iArr2[b9] + 3;
                                                    }
                                                    if (((c2 >= 2 && c3 == 1 && i3 > 0) || (c2 == 1 && c3 >= 2 && i2 > 0)) && c4 >= 2 && c4 >= 2) {
                                                        iArr2[b9] = iArr2[b9] + 6;
                                                    }
                                                    if (((c2 > 0 && c3 == 0 && i3 > 0) || (c2 == 0 && c3 > 0 && i2 > 0)) && c4 > 0 && c4 > 0) {
                                                        iArr2[b9] = iArr2[b9] + 9;
                                                    }
                                                } else if (c3 == 0 && c4 > 0 && c5 > 0) {
                                                    iArr2[b9] = iArr2[b9] + 3;
                                                } else if (c3 > 0 && c4 > 0 && c5 > 0) {
                                                    iArr2[b9] = iArr2[b9] + 2;
                                                } else if (c4 > 0 || c5 > 0) {
                                                    iArr2[b9] = iArr2[b9] + 1;
                                                }
                                            } else if (c2 + c3 > c4 + c5) {
                                                iArr2[b9] = (((c4 + c5) - c2) - c3) + iArr2[b9];
                                            }
                                            float f2 = bVar2.f((byte) (j3 % 2), b9);
                                            if (b9 != B2) {
                                                if (f2 >= 0.9f) {
                                                    iArr2[b9] = iArr2[b9] + 5;
                                                } else if (f2 >= 0.8f) {
                                                    iArr2[b9] = iArr2[b9] + 4;
                                                } else if (f2 >= 0.7f) {
                                                    iArr2[b9] = iArr2[b9] + 3;
                                                } else if (f2 >= 0.6f) {
                                                    iArr2[b9] = iArr2[b9] + 1;
                                                }
                                            }
                                        }
                                    }
                                    if (C4 != 0) {
                                        byte s2 = bVar2.s((byte) (C4 - 1));
                                        iArr2[s2] = iArr2[s2] + 5;
                                    }
                                    byte a5 = (iArr2[0] == 0 && iArr2[1] == 0 && iArr2[2] == 0 && iArr2[3] == 0) ? bVar2.a(j3, 1) : (byte) g.a(iArr2);
                                    int c10 = bVar2.c(b.m(j3), b.q(a5));
                                    int c11 = bVar2.c(b.l(j3), b.q(a5));
                                    int c12 = bVar2.c(b.o(j3), b.q(a5));
                                    byte[][] r2 = bVar2.r(a5);
                                    int g2 = bVar2.g() - bVar2.C();
                                    if (c10 <= 0 || (c11 <= 0 && c12 <= 0)) {
                                        if (b.i(j3, r2[1][0]) && !(r2[1][0] == j3 && b.a(r2, 0, 1) == b.m(j3))) {
                                            if (r2[1][0] == b.m(j3) && b.a(r2, 0, 1) == b.l(j3)) {
                                                if (b.a(r2, b.a(r2, 0), 1) == j3) {
                                                    d2 = b.a(r2, b.a(r2, 0), 0);
                                                }
                                            }
                                            if (r2[1][0] == j3 && b.a(r2, 0, 1) == b.l(j3) && b.a(r2, b.a(r2, 0), 1) == b.l(j3)) {
                                                d2 = r2[0][0];
                                            }
                                        }
                                        d2 = bVar2.d(j3, a5);
                                    } else {
                                        if (b.i(j3, r2[1][0]) && !(r2[1][0] == j3 && b.a(r2, 0, 1) == b.m(j3))) {
                                            if (r2[1][0] == b.m(j3) && b.a(r2, 0, 1) == b.l(j3)) {
                                                if (b.a(r2, b.a(r2, 0), 1) == j3) {
                                                    d2 = b.a(r2, b.a(r2, 0), 0);
                                                }
                                            }
                                            d2 = (r2[1].length >= 2 && r2[1][0] == j3 && r2[1][1] == j3) ? r2[0][0] : (r2[1][0] == j3 && b.a(r2, 0, 1) == b.l(j3) && b.a(r2, b.a(r2, 0), 1) == b.l(j3)) ? r2[0][0] : bVar2.d(j3, a5);
                                        }
                                        d2 = bVar2.d(j3, a5);
                                    }
                                    if (d2 == -1 || !bVar2.k(d2, j3)) {
                                        bVar2.N(bVar2.l[j3][g.b(g2)]);
                                    } else {
                                        bVar2.N(d2);
                                    }
                                } else {
                                    byte j4 = bVar2.j();
                                    if (!bVar2.j(j4, bVar2.D())) {
                                        byte j5 = bVar2.j();
                                        byte C5 = bVar2.C();
                                        byte t2 = bVar2.t(C5);
                                        byte D2 = bVar2.D();
                                        byte[] E2 = bVar2.E();
                                        if (E2[0] != t2 && j5 != b.l(t2)) {
                                            if (j5 == b.m(t2)) {
                                                bVar2.N(bVar2.c(j5, D2));
                                            } else {
                                                if (E2[0] != b.m(j5)) {
                                                    int i20 = 0;
                                                    while (true) {
                                                        if (i20 >= bVar2.g() - C5) {
                                                            break;
                                                        }
                                                        if (b.g(bVar2.l[j5][i20]) == D2) {
                                                            if (b.b(bVar2.a((int) t2, (int) C5), bVar2.l[j5][i20], D2)) {
                                                                if (b.b(bVar2.a((int) b.m(t2), (int) C5), bVar2.l[j5][i20], D2)) {
                                                                    bVar2.N(bVar2.l[j5][i20]);
                                                                    break;
                                                                }
                                                            } else {
                                                                continue;
                                                            }
                                                        }
                                                        i20++;
                                                    }
                                                }
                                            }
                                        }
                                        bVar2.N(bVar2.d(j5, D2));
                                    } else if (bVar2.M()) {
                                        c(bVar2);
                                    } else if (bVar2.j(j4, bVar2.B())) {
                                        c(bVar2);
                                    } else if (b.h(j4, bVar2.E()[0])) {
                                        c(bVar2);
                                    } else {
                                        bVar2.N(bVar2.d(bVar2.j(), bVar2.B()));
                                    }
                                }
                            }
                            iArr[i16] = iArr[i16] + bVar2.p[j2 % 2];
                        }
                        if (System.currentTimeMillis() - currentTimeMillis >= 2000) {
                            break;
                        }
                    }
                    b3 = bArr[g.a(iArr)];
                }
                b6 = b.a(this.b.l[this.f98a], b3);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        if (b6 == -1) {
            try {
                byte C6 = this.i.C();
                if (C6 >= 8) {
                    this.k = 13;
                } else {
                    this.k = C6 + 3;
                }
                b6 = b.a(this.b.l[this.f98a], c(0));
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        }
        if (b2 == -1) {
            g.a(0, "No card selected!");
            b2 = 0;
            while (true) {
                if (b2 < 13) {
                    if (this.b.l[this.f98a][b2] != -1 && this.b.k(this.b.l[this.f98a][b2], this.f98a)) {
                        break;
                    }
                    b2 = (byte) (b2 + 1);
                } else {
                    b2 = -1;
                    break;
                }
            }
        }
        if (this.P != null) {
            Message obtain = Message.obtain();
            obtain.what = 1002;
            obtain.arg1 = this.b.l[this.f98a][b2];
            obtain.arg2 = this.f98a;
            this.P.sendMessage(obtain);
        }
        this.O = 1000;
    }

    private byte e(byte b2) {
        if (b2 < 0 && b2 != 1 && b2 != 2 && b2 != 0 && b2 != 4) {
            return 99;
        }
        for (int z2 = this.b.z(); z2 <= 7; z2++) {
            if (this.b.L(b.a(z2, b2))) {
                return b.a(z2, b2);
            }
        }
        return 99;
    }

    private byte f(byte b2) {
        boolean z2;
        byte[] a2 = b.a(this.b.l[b2]);
        for (byte b3 = 0; b3 < a2.length; b3 = (byte) (b3 + 1)) {
            if (a2[b3] >= 4) {
                byte b4 = 0;
                while (true) {
                    if (b4 > this.b.e()) {
                        z2 = false;
                        break;
                    }
                    byte p2 = this.b.p(b4);
                    int a3 = a((int) b4);
                    if (b.i(b2, p2) && ((a3 == Y || a3 == ac || a3 == aG || a3 == ad || a3 == ae || a3 == aj || a3 == ak || a3 == an || a3 == aq || a3 == ar) && b.e(this.b.M(b4)) == b3)) {
                        z2 = true;
                        break;
                    }
                    b4 = (byte) (b4 + 1);
                }
                if (!z2) {
                    return b3;
                }
            }
        }
        return -1;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: net.weweweb.android.bridge.y.a(byte, boolean):void
     arg types: [byte, int]
     candidates:
      net.weweweb.android.bridge.y.a(a.b, byte[]):byte
      net.weweweb.android.bridge.y.a(java.lang.String, java.lang.String):byte
      net.weweweb.android.bridge.y.a(int, int):int
      net.weweweb.android.bridge.y.a(byte, byte):boolean
      net.weweweb.android.bridge.y.a(byte, int):boolean
      net.weweweb.android.bridge.y.a(int, android.os.Handler):void
      net.weweweb.android.bridge.y.a(byte, boolean):void */
    private void g(byte b2) {
        if (b2 == 0) {
            a(this.f98a, true);
        } else if (this.b.C() >= 7) {
            this.k = 13;
            b(this.i);
            if (c(1) >= this.i.p[this.i.s() % 2] + b2) {
                a(this.f98a, true);
            } else {
                a(this.f98a, false);
            }
        } else {
            a(this.f98a, false);
        }
    }

    public final void a() {
        g.b(this.Q, S);
        for (int i2 = 0; i2 < 4; i2++) {
            g.b(this.H[i2], 0);
            g.b(this.I, 0);
            this.J[i2] = false;
            int[] iArr = this.D[i2];
            int[] iArr2 = this.D[i2];
            int[] iArr3 = this.D[i2];
            this.D[i2][3] = 0;
            iArr3[2] = 0;
            iArr2[1] = 0;
            iArr[0] = 0;
            int[] iArr4 = this.E[i2];
            int[] iArr5 = this.E[i2];
            int[] iArr6 = this.E[i2];
            this.E[i2][3] = 13;
            iArr6[2] = 13;
            iArr5[1] = 13;
            iArr4[0] = 13;
            this.F[this.f98a] = 0;
            this.G[this.f98a] = 37;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.b.c(byte, char):int
     arg types: [byte, int]
     candidates:
      a.b.c(byte, byte):byte
      a.b.c(byte, char):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.b.a(byte, boolean):boolean
     arg types: [byte, int]
     candidates:
      a.b.a(int, byte):byte
      a.b.a(byte[], byte):byte
      a.b.a(byte[][], int):byte
      a.b.a(byte, byte):java.lang.String
      a.b.a(byte[], boolean):void
      a.b.a(byte, int):byte
      a.b.a(int, int):byte
      a.b.a(byte, char):int
      a.b.a(byte, boolean):boolean */
    private byte h(byte b2) {
        if (b2 == 2) {
            if (this.b.c(this.f98a, 'C') == 0 || this.b.c(this.f98a, 'D') == 0 || this.b.c(this.f98a, 'S') == 0 || this.b.c(this.f98a, 'C') == 0 || this.b.c(this.f98a, 'D') == 0 || this.b.c(this.f98a, 'S') == 0) {
                if (this.b.c(this.f98a, 'C') >= 5) {
                    return b.a("4C");
                }
                if (this.b.c(this.f98a, 'D') >= 5) {
                    return b.a("4D");
                }
                if (this.b.c(this.f98a, 'S') >= 5) {
                    return b.a("4S");
                }
            }
            if (this.b.c(this.f98a, 'C') == 0) {
                return b.a("3C");
            }
            if (this.b.c(this.f98a, 'D') == 0) {
                return b.a("3D");
            }
            if (this.b.c(this.f98a, 'S') == 0) {
                return b.a("3S");
            }
            if (this.b.c(this.f98a, 'C') == 1) {
                return b.a("3C");
            }
            if (this.b.c(this.f98a, 'D') == 1) {
                return b.a("3D");
            }
            if (this.b.c(this.f98a, 'S') == 1) {
                return b.a("3S");
            }
            if (this.w <= 14 && this.b.a(this.f98a, true)) {
                return b.a("4H");
            }
            if (this.w >= 15 && this.w <= 17 && this.b.a(this.f98a, true)) {
                return b.a("3NT");
            }
            if (this.w >= 18) {
                return b.a("3H");
            }
            return b.a("4H");
        } else if (b2 != 3) {
            return 99;
        } else {
            if (this.b.c(this.f98a, 'C') == 0 || this.b.c(this.f98a, 'D') == 0 || this.b.c(this.f98a, 'H') == 0 || this.b.c(this.f98a, 'C') == 0 || this.b.c(this.f98a, 'D') == 0 || this.b.c(this.f98a, 'H') == 0) {
                if (this.b.c(this.f98a, 'C') >= 5) {
                    return b.a("4C");
                }
                if (this.b.c(this.f98a, 'D') >= 5) {
                    return b.a("4D");
                }
                if (this.b.c(this.f98a, 'H') >= 5) {
                    return b.a("4H");
                }
            }
            if (this.b.c(this.f98a, 'C') == 0) {
                return b.a("3C");
            }
            if (this.b.c(this.f98a, 'D') == 0) {
                return b.a("3D");
            }
            if (this.b.c(this.f98a, 'H') == 0) {
                return b.a("3S");
            }
            if (this.b.c(this.f98a, 'C') == 1) {
                return b.a("3C");
            }
            if (this.b.c(this.f98a, 'D') == 1) {
                return b.a("3D");
            }
            if (this.b.c(this.f98a, 'H') == 1) {
                return b.a("3H");
            }
            if (this.w <= 14 && this.b.a(this.f98a, true)) {
                return b.a("4S");
            }
            if (this.w >= 15 && this.w <= 17 && this.b.a(this.f98a, true)) {
                return b.a("3NT");
            }
            if (this.w >= 18) {
                return b.a("3S");
            }
            return b.a("4S");
        }
    }

    /* JADX WARN: Type inference failed for: r12v0, types: [net.weweweb.android.bridge.y] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public final void run() {
        /*
            r12 = this;
            java.lang.String r0 = ""
            monitor-enter(r0)
            int r1 = r12.O     // Catch:{ all -> 0x019b }
            r2 = 1001(0x3e9, float:1.403E-42)
            if (r1 != r2) goto L_0x10f8
            r1 = 1000(0x3e8, float:1.401E-42)
            a.g.a(r1)     // Catch:{ all -> 0x019b }
            a.b r1 = r12.b     // Catch:{ all -> 0x019b }
            byte r1 = r1.z()     // Catch:{ all -> 0x019b }
            r12.v = r1     // Catch:{ all -> 0x019b }
            byte[] r1 = a.d.d     // Catch:{ all -> 0x019b }
            r2 = 0
            byte[] r3 = r12.f     // Catch:{ all -> 0x019b }
            r4 = 0
            byte[] r5 = r12.f     // Catch:{ all -> 0x019b }
            int r5 = r5.length     // Catch:{ all -> 0x019b }
            java.lang.System.arraycopy(r1, r2, r3, r4, r5)     // Catch:{ all -> 0x019b }
            a.b r1 = r12.b     // Catch:{ all -> 0x019b }
            byte r2 = r12.f98a     // Catch:{ all -> 0x019b }
            int r1 = r1.k(r2)     // Catch:{ all -> 0x019b }
            r12.w = r1     // Catch:{ all -> 0x019b }
            a.b r1 = r12.b     // Catch:{ all -> 0x019b }
            byte r2 = r12.f98a     // Catch:{ all -> 0x019b }
            java.lang.String r1 = r1.j(r2)     // Catch:{ all -> 0x019b }
            r12.C = r1     // Catch:{ all -> 0x019b }
            a.b r1 = r12.b     // Catch:{ all -> 0x019b }
            byte[][] r1 = r1.l     // Catch:{ all -> 0x019b }
            byte r2 = r12.f98a     // Catch:{ all -> 0x019b }
            r1 = r1[r2]     // Catch:{ all -> 0x019b }
            byte[] r1 = a.b.a(r1)     // Catch:{ all -> 0x019b }
            r12.x = r1     // Catch:{ all -> 0x019b }
            a.b r1 = r12.b     // Catch:{ all -> 0x019b }
            byte r2 = r12.f98a     // Catch:{ all -> 0x019b }
            r3 = 1
            byte r1 = r1.a(r2, r3)     // Catch:{ all -> 0x019b }
            r12.y = r1     // Catch:{ all -> 0x019b }
            byte r1 = r12.y     // Catch:{ all -> 0x019b }
            char r1 = a.b.q(r1)     // Catch:{ all -> 0x019b }
            r12.z = r1     // Catch:{ all -> 0x019b }
            a.b r1 = r12.b     // Catch:{ all -> 0x019b }
            byte r2 = r12.f98a     // Catch:{ all -> 0x019b }
            r3 = 2
            byte r1 = r1.a(r2, r3)     // Catch:{ all -> 0x019b }
            r12.A = r1     // Catch:{ all -> 0x019b }
            byte r1 = r12.A     // Catch:{ all -> 0x019b }
            char r1 = a.b.q(r1)     // Catch:{ all -> 0x019b }
            r12.B = r1     // Catch:{ all -> 0x019b }
            boolean r1 = r12.g     // Catch:{ all -> 0x019b }
            if (r1 != 0) goto L_0x0071
            r1 = 1
            r12.g = r1     // Catch:{ all -> 0x019b }
        L_0x0071:
            r1 = -1
            r2 = 1
            r3 = 0
        L_0x0074:
            a.b r4 = r12.b     // Catch:{ all -> 0x019b }
            byte r4 = r4.e()     // Catch:{ all -> 0x019b }
            if (r3 > r4) goto L_0x0085
            int r4 = r12.a(r3)     // Catch:{ all -> 0x019b }
            int r5 = net.weweweb.android.bridge.y.R     // Catch:{ all -> 0x019b }
            if (r4 == r5) goto L_0x00e8
            r2 = 0
        L_0x0085:
            if (r2 == 0) goto L_0x00ec
            r2 = 0
        L_0x0088:
            r3 = 4
            if (r2 >= r3) goto L_0x00ec
            int[][] r3 = r12.H     // Catch:{ all -> 0x019b }
            r3 = r3[r2]     // Catch:{ all -> 0x019b }
            r4 = 0
            a.g.b(r3, r4)     // Catch:{ all -> 0x019b }
            int[] r3 = r12.I     // Catch:{ all -> 0x019b }
            r4 = 0
            a.g.b(r3, r4)     // Catch:{ all -> 0x019b }
            boolean[] r3 = r12.J     // Catch:{ all -> 0x019b }
            r4 = 0
            r3[r2] = r4     // Catch:{ all -> 0x019b }
            int[] r3 = r12.F     // Catch:{ all -> 0x019b }
            r4 = 0
            r3[r2] = r4     // Catch:{ all -> 0x019b }
            int[] r3 = r12.G     // Catch:{ all -> 0x019b }
            r4 = 37
            r3[r2] = r4     // Catch:{ all -> 0x019b }
            int[][] r3 = r12.D     // Catch:{ all -> 0x019b }
            r3 = r3[r2]     // Catch:{ all -> 0x019b }
            r4 = 0
            int[][] r5 = r12.D     // Catch:{ all -> 0x019b }
            r5 = r5[r2]     // Catch:{ all -> 0x019b }
            r6 = 1
            int[][] r7 = r12.D     // Catch:{ all -> 0x019b }
            r7 = r7[r2]     // Catch:{ all -> 0x019b }
            r8 = 2
            int[][] r9 = r12.D     // Catch:{ all -> 0x019b }
            r9 = r9[r2]     // Catch:{ all -> 0x019b }
            r10 = 3
            r11 = 0
            r9[r10] = r11     // Catch:{ all -> 0x019b }
            r7[r8] = r11     // Catch:{ all -> 0x019b }
            r5[r6] = r11     // Catch:{ all -> 0x019b }
            r3[r4] = r11     // Catch:{ all -> 0x019b }
            int[][] r3 = r12.E     // Catch:{ all -> 0x019b }
            r3 = r3[r2]     // Catch:{ all -> 0x019b }
            r4 = 0
            int[][] r5 = r12.E     // Catch:{ all -> 0x019b }
            r5 = r5[r2]     // Catch:{ all -> 0x019b }
            r6 = 1
            int[][] r7 = r12.E     // Catch:{ all -> 0x019b }
            r7 = r7[r2]     // Catch:{ all -> 0x019b }
            r8 = 2
            int[][] r9 = r12.E     // Catch:{ all -> 0x019b }
            r9 = r9[r2]     // Catch:{ all -> 0x019b }
            r10 = 3
            r11 = 13
            r9[r10] = r11     // Catch:{ all -> 0x019b }
            r7[r8] = r11     // Catch:{ all -> 0x019b }
            r5[r6] = r11     // Catch:{ all -> 0x019b }
            r3[r4] = r11     // Catch:{ all -> 0x019b }
            int r2 = r2 + 1
            byte r2 = (byte) r2     // Catch:{ all -> 0x019b }
            goto L_0x0088
        L_0x00e8:
            int r3 = r3 + 1
            byte r3 = (byte) r3     // Catch:{ all -> 0x019b }
            goto L_0x0074
        L_0x00ec:
            r2 = 0
        L_0x00ed:
            a.b r3 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r3.e()     // Catch:{ all -> 0x019b }
            if (r2 > r3) goto L_0x0129
            a.b r3 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r3.p(r2)     // Catch:{ all -> 0x019b }
            a.b r4 = r12.b     // Catch:{ all -> 0x019b }
            byte r4 = r4.M(r2)     // Catch:{ all -> 0x019b }
            boolean r5 = a.b.z(r4)     // Catch:{ all -> 0x019b }
            if (r5 == 0) goto L_0x0119
            r5 = -1
            if (r1 != r5) goto L_0x010b
            r1 = r3
        L_0x010b:
            int[][] r5 = r12.H     // Catch:{ all -> 0x019b }
            r5 = r5[r3]     // Catch:{ all -> 0x019b }
            byte r6 = a.b.i(r4)     // Catch:{ all -> 0x019b }
            r7 = r5[r6]     // Catch:{ all -> 0x019b }
            int r7 = r7 + 1
            r5[r6] = r7     // Catch:{ all -> 0x019b }
        L_0x0119:
            r5 = 99
            if (r4 != r5) goto L_0x0125
            int[] r4 = r12.I     // Catch:{ all -> 0x019b }
            r5 = r4[r3]     // Catch:{ all -> 0x019b }
            int r5 = r5 + 1
            r4[r3] = r5     // Catch:{ all -> 0x019b }
        L_0x0125:
            int r2 = r2 + 1
            byte r2 = (byte) r2     // Catch:{ all -> 0x019b }
            goto L_0x00ed
        L_0x0129:
            r2 = 0
        L_0x012a:
            a.b r3 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r3.e()     // Catch:{ all -> 0x019b }
            if (r2 > r3) goto L_0x0585
            a.b r3 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r3.p(r2)     // Catch:{ all -> 0x019b }
            a.b r4 = r12.b     // Catch:{ all -> 0x019b }
            byte r4 = r4.M(r2)     // Catch:{ all -> 0x019b }
            int r5 = r12.a(r2)     // Catch:{ all -> 0x019b }
            int r6 = net.weweweb.android.bridge.y.S     // Catch:{ all -> 0x019b }
            if (r5 != r6) goto L_0x0165
            r5 = 99
            if (r4 != r5) goto L_0x0169
            r5 = 1
            int r5 = r2 - r5
            int r5 = r12.a(r5)     // Catch:{ all -> 0x019b }
            int r6 = net.weweweb.android.bridge.y.R     // Catch:{ all -> 0x019b }
            if (r5 == r6) goto L_0x0160
            r5 = 1
            int r5 = r2 - r5
            int r5 = r12.a(r5)     // Catch:{ all -> 0x019b }
            int r6 = net.weweweb.android.bridge.y.V     // Catch:{ all -> 0x019b }
            if (r5 != r6) goto L_0x0169
        L_0x0160:
            int r3 = net.weweweb.android.bridge.y.V     // Catch:{ all -> 0x019b }
            r12.b(r2, r3)     // Catch:{ all -> 0x019b }
        L_0x0165:
            int r2 = r2 + 1
            byte r2 = (byte) r2     // Catch:{ all -> 0x019b }
            goto L_0x012a
        L_0x0169:
            int r5 = r12.u     // Catch:{ all -> 0x019b }
            int r6 = net.weweweb.android.bridge.y.t     // Catch:{ all -> 0x019b }
            if (r5 != r6) goto L_0x0573
            r5 = 1
            int r5 = r2 - r5
            int r5 = r12.a(r5)     // Catch:{ all -> 0x019b }
            int r6 = net.weweweb.android.bridge.y.R     // Catch:{ all -> 0x019b }
            if (r5 == r6) goto L_0x0185
            r5 = 1
            int r5 = r2 - r5
            int r5 = r12.a(r5)     // Catch:{ all -> 0x019b }
            int r6 = net.weweweb.android.bridge.y.V     // Catch:{ all -> 0x019b }
            if (r5 != r6) goto L_0x021f
        L_0x0185:
            java.lang.String r5 = "1C"
            byte r5 = a.b.a(r5)     // Catch:{ all -> 0x019b }
            if (r4 == r5) goto L_0x0195
            java.lang.String r5 = "1D"
            byte r5 = a.b.a(r5)     // Catch:{ all -> 0x019b }
            if (r4 != r5) goto L_0x019e
        L_0x0195:
            int r3 = net.weweweb.android.bridge.y.X     // Catch:{ all -> 0x019b }
            r12.b(r2, r3)     // Catch:{ all -> 0x019b }
            goto L_0x0165
        L_0x019b:
            r1 = move-exception
            monitor-exit(r0)
            throw r1
        L_0x019e:
            java.lang.String r5 = "1H"
            byte r5 = a.b.a(r5)     // Catch:{ all -> 0x019b }
            if (r4 == r5) goto L_0x01ae
            java.lang.String r5 = "1S"
            byte r5 = a.b.a(r5)     // Catch:{ all -> 0x019b }
            if (r4 != r5) goto L_0x01b4
        L_0x01ae:
            int r3 = net.weweweb.android.bridge.y.Y     // Catch:{ all -> 0x019b }
            r12.b(r2, r3)     // Catch:{ all -> 0x019b }
            goto L_0x0165
        L_0x01b4:
            java.lang.String r5 = "1NT"
            byte r5 = a.b.a(r5)     // Catch:{ all -> 0x019b }
            if (r4 == r5) goto L_0x01cc
            java.lang.String r5 = "2NT"
            byte r5 = a.b.a(r5)     // Catch:{ all -> 0x019b }
            if (r4 == r5) goto L_0x01cc
            java.lang.String r5 = "3NT"
            byte r5 = a.b.a(r5)     // Catch:{ all -> 0x019b }
            if (r4 != r5) goto L_0x01d2
        L_0x01cc:
            int r3 = net.weweweb.android.bridge.y.Z     // Catch:{ all -> 0x019b }
            r12.b(r2, r3)     // Catch:{ all -> 0x019b }
            goto L_0x0165
        L_0x01d2:
            java.lang.String r5 = "2C"
            byte r5 = a.b.a(r5)     // Catch:{ all -> 0x019b }
            if (r4 != r5) goto L_0x01e0
            int r3 = net.weweweb.android.bridge.y.ab     // Catch:{ all -> 0x019b }
            r12.b(r2, r3)     // Catch:{ all -> 0x019b }
            goto L_0x0165
        L_0x01e0:
            java.lang.String r5 = "2D"
            byte r5 = a.b.a(r5)     // Catch:{ all -> 0x019b }
            if (r4 == r5) goto L_0x0218
            java.lang.String r5 = "2H"
            byte r5 = a.b.a(r5)     // Catch:{ all -> 0x019b }
            if (r4 == r5) goto L_0x0218
            java.lang.String r5 = "2S"
            byte r5 = a.b.a(r5)     // Catch:{ all -> 0x019b }
            if (r4 == r5) goto L_0x0218
            java.lang.String r5 = "3C"
            byte r5 = a.b.a(r5)     // Catch:{ all -> 0x019b }
            if (r4 == r5) goto L_0x0218
            java.lang.String r5 = "3D"
            byte r5 = a.b.a(r5)     // Catch:{ all -> 0x019b }
            if (r4 == r5) goto L_0x0218
            java.lang.String r5 = "3H"
            byte r5 = a.b.a(r5)     // Catch:{ all -> 0x019b }
            if (r4 == r5) goto L_0x0218
            java.lang.String r5 = "3S"
            byte r5 = a.b.a(r5)     // Catch:{ all -> 0x019b }
            if (r4 != r5) goto L_0x021f
        L_0x0218:
            int r3 = net.weweweb.android.bridge.y.ac     // Catch:{ all -> 0x019b }
            r12.b(r2, r3)     // Catch:{ all -> 0x019b }
            goto L_0x0165
        L_0x021f:
            a.b r5 = r12.b     // Catch:{ all -> 0x019b }
            byte r5 = r5.p(r2)     // Catch:{ all -> 0x019b }
            a.b r6 = r12.b     // Catch:{ all -> 0x019b }
            byte r6 = r6.M(r2)     // Catch:{ all -> 0x019b }
            java.lang.String r7 = "4NT"
            byte r7 = a.b.a(r7)     // Catch:{ all -> 0x019b }
            if (r6 != r7) goto L_0x0298
            int r7 = net.weweweb.android.bridge.y.Z     // Catch:{ all -> 0x019b }
            boolean r7 = r12.a(r5, r7)     // Catch:{ all -> 0x019b }
            if (r7 != 0) goto L_0x0298
            int r7 = net.weweweb.android.bridge.y.aa     // Catch:{ all -> 0x019b }
            boolean r7 = r12.a(r5, r7)     // Catch:{ all -> 0x019b }
            if (r7 != 0) goto L_0x0298
            byte r7 = a.b.m(r5)     // Catch:{ all -> 0x019b }
            int r8 = net.weweweb.android.bridge.y.Z     // Catch:{ all -> 0x019b }
            boolean r7 = r12.a(r7, r8)     // Catch:{ all -> 0x019b }
            if (r7 != 0) goto L_0x0298
            byte r7 = a.b.m(r5)     // Catch:{ all -> 0x019b }
            int r8 = net.weweweb.android.bridge.y.aa     // Catch:{ all -> 0x019b }
            boolean r7 = r12.a(r7, r8)     // Catch:{ all -> 0x019b }
            if (r7 != 0) goto L_0x0298
            int r7 = net.weweweb.android.bridge.y.aE     // Catch:{ all -> 0x019b }
            boolean r7 = r12.a(r5, r7)     // Catch:{ all -> 0x019b }
            if (r7 != 0) goto L_0x0298
            byte r7 = a.b.m(r5)     // Catch:{ all -> 0x019b }
            int r8 = net.weweweb.android.bridge.y.aE     // Catch:{ all -> 0x019b }
            boolean r7 = r12.a(r7, r8)     // Catch:{ all -> 0x019b }
            if (r7 != 0) goto L_0x0298
            int r5 = net.weweweb.android.bridge.y.aA     // Catch:{ all -> 0x019b }
            r12.b(r2, r5)     // Catch:{ all -> 0x019b }
            r5 = 1
        L_0x0275:
            if (r5 != 0) goto L_0x0165
            r5 = 1
            int r5 = r2 - r5
            int r5 = r12.a(r5)     // Catch:{ all -> 0x019b }
            int r6 = net.weweweb.android.bridge.y.X     // Catch:{ all -> 0x019b }
            if (r5 == r6) goto L_0x028d
            r5 = 1
            int r5 = r2 - r5
            int r5 = r12.a(r5)     // Catch:{ all -> 0x019b }
            int r6 = net.weweweb.android.bridge.y.Y     // Catch:{ all -> 0x019b }
            if (r5 != r6) goto L_0x0435
        L_0x028d:
            r5 = 97
            if (r4 != r5) goto L_0x03fb
            int r3 = net.weweweb.android.bridge.y.ah     // Catch:{ all -> 0x019b }
            r12.b(r2, r3)     // Catch:{ all -> 0x019b }
            goto L_0x0165
        L_0x0298:
            r7 = 2
            int r7 = r2 - r7
            int r7 = r12.a(r7)     // Catch:{ all -> 0x019b }
            int r8 = net.weweweb.android.bridge.y.aA     // Catch:{ all -> 0x019b }
            if (r7 != r8) goto L_0x02c4
            a.b r7 = r12.b     // Catch:{ all -> 0x019b }
            r8 = 1
            int r8 = r2 - r8
            byte r7 = r7.d(r8)     // Catch:{ all -> 0x019b }
            r8 = 99
            if (r7 == r8) goto L_0x02bd
            a.b r7 = r12.b     // Catch:{ all -> 0x019b }
            r8 = 1
            int r8 = r2 - r8
            byte r7 = r7.d(r8)     // Catch:{ all -> 0x019b }
            r8 = 97
            if (r7 != r8) goto L_0x02c4
        L_0x02bd:
            int r5 = net.weweweb.android.bridge.y.aB     // Catch:{ all -> 0x019b }
            r12.b(r2, r5)     // Catch:{ all -> 0x019b }
            r5 = 1
            goto L_0x0275
        L_0x02c4:
            java.lang.String r7 = "5NT"
            byte r7 = a.b.a(r7)     // Catch:{ all -> 0x019b }
            if (r6 != r7) goto L_0x0310
            int r7 = net.weweweb.android.bridge.y.Z     // Catch:{ all -> 0x019b }
            boolean r7 = r12.a(r5, r7)     // Catch:{ all -> 0x019b }
            if (r7 != 0) goto L_0x0310
            int r7 = net.weweweb.android.bridge.y.aa     // Catch:{ all -> 0x019b }
            boolean r7 = r12.a(r5, r7)     // Catch:{ all -> 0x019b }
            if (r7 != 0) goto L_0x0310
            byte r7 = a.b.m(r5)     // Catch:{ all -> 0x019b }
            int r8 = net.weweweb.android.bridge.y.Z     // Catch:{ all -> 0x019b }
            boolean r7 = r12.a(r7, r8)     // Catch:{ all -> 0x019b }
            if (r7 != 0) goto L_0x0310
            byte r7 = a.b.m(r5)     // Catch:{ all -> 0x019b }
            int r8 = net.weweweb.android.bridge.y.aa     // Catch:{ all -> 0x019b }
            boolean r7 = r12.a(r7, r8)     // Catch:{ all -> 0x019b }
            if (r7 != 0) goto L_0x0310
            int r7 = net.weweweb.android.bridge.y.aE     // Catch:{ all -> 0x019b }
            boolean r7 = r12.a(r5, r7)     // Catch:{ all -> 0x019b }
            if (r7 != 0) goto L_0x0310
            byte r5 = a.b.m(r5)     // Catch:{ all -> 0x019b }
            int r7 = net.weweweb.android.bridge.y.aE     // Catch:{ all -> 0x019b }
            boolean r5 = r12.a(r5, r7)     // Catch:{ all -> 0x019b }
            if (r5 != 0) goto L_0x0310
            int r5 = net.weweweb.android.bridge.y.aC     // Catch:{ all -> 0x019b }
            r12.b(r2, r5)     // Catch:{ all -> 0x019b }
            r5 = 1
            goto L_0x0275
        L_0x0310:
            r5 = 2
            int r5 = r2 - r5
            int r5 = r12.a(r5)     // Catch:{ all -> 0x019b }
            int r7 = net.weweweb.android.bridge.y.aC     // Catch:{ all -> 0x019b }
            if (r5 != r7) goto L_0x033d
            a.b r5 = r12.b     // Catch:{ all -> 0x019b }
            r7 = 1
            int r7 = r2 - r7
            byte r5 = r5.d(r7)     // Catch:{ all -> 0x019b }
            r7 = 99
            if (r5 == r7) goto L_0x0335
            a.b r5 = r12.b     // Catch:{ all -> 0x019b }
            r7 = 1
            int r7 = r2 - r7
            byte r5 = r5.d(r7)     // Catch:{ all -> 0x019b }
            r7 = 97
            if (r5 != r7) goto L_0x033d
        L_0x0335:
            int r5 = net.weweweb.android.bridge.y.aD     // Catch:{ all -> 0x019b }
            r12.b(r2, r5)     // Catch:{ all -> 0x019b }
            r5 = 1
            goto L_0x0275
        L_0x033d:
            r5 = 2
            int r5 = r2 - r5
            int r5 = r12.a(r5)     // Catch:{ all -> 0x019b }
            int r7 = net.weweweb.android.bridge.y.Z     // Catch:{ all -> 0x019b }
            if (r5 != r7) goto L_0x03aa
            a.b r5 = r12.b     // Catch:{ all -> 0x019b }
            r7 = 2
            int r7 = r2 - r7
            byte r5 = r5.d(r7)     // Catch:{ all -> 0x019b }
            java.lang.String r7 = "1NT"
            byte r7 = a.b.a(r7)     // Catch:{ all -> 0x019b }
            if (r5 != r7) goto L_0x0389
            java.lang.String r5 = "2C"
            byte r5 = a.b.a(r5)     // Catch:{ all -> 0x019b }
            if (r6 != r5) goto L_0x0369
            int r5 = net.weweweb.android.bridge.y.aw     // Catch:{ all -> 0x019b }
            r12.b(r2, r5)     // Catch:{ all -> 0x019b }
            r5 = 1
            goto L_0x0275
        L_0x0369:
            java.lang.String r5 = "2D"
            byte r5 = a.b.a(r5)     // Catch:{ all -> 0x019b }
            if (r6 != r5) goto L_0x0379
            int r5 = net.weweweb.android.bridge.y.ay     // Catch:{ all -> 0x019b }
            r12.b(r2, r5)     // Catch:{ all -> 0x019b }
            r5 = 1
            goto L_0x0275
        L_0x0379:
            java.lang.String r5 = "2H"
            byte r5 = a.b.a(r5)     // Catch:{ all -> 0x019b }
            if (r6 != r5) goto L_0x0389
            int r5 = net.weweweb.android.bridge.y.ay     // Catch:{ all -> 0x019b }
            r12.b(r2, r5)     // Catch:{ all -> 0x019b }
            r5 = 1
            goto L_0x0275
        L_0x0389:
            a.b r5 = r12.b     // Catch:{ all -> 0x019b }
            r7 = 2
            int r7 = r2 - r7
            byte r5 = r5.d(r7)     // Catch:{ all -> 0x019b }
            java.lang.String r7 = "2NT"
            byte r7 = a.b.a(r7)     // Catch:{ all -> 0x019b }
            if (r5 != r7) goto L_0x03aa
            java.lang.String r5 = "3C"
            byte r5 = a.b.a(r5)     // Catch:{ all -> 0x019b }
            if (r6 != r5) goto L_0x03aa
            int r5 = net.weweweb.android.bridge.y.aw     // Catch:{ all -> 0x019b }
            r12.b(r2, r5)     // Catch:{ all -> 0x019b }
            r5 = 1
            goto L_0x0275
        L_0x03aa:
            r5 = 2
            int r5 = r2 - r5
            int r5 = r12.a(r5)     // Catch:{ all -> 0x019b }
            int r7 = net.weweweb.android.bridge.y.aw     // Catch:{ all -> 0x019b }
            if (r5 != r7) goto L_0x03ca
            a.b r5 = r12.b     // Catch:{ all -> 0x019b }
            r7 = 1
            int r7 = r2 - r7
            byte r5 = r5.d(r7)     // Catch:{ all -> 0x019b }
            r7 = 99
            if (r5 != r7) goto L_0x03ca
            int r5 = net.weweweb.android.bridge.y.ax     // Catch:{ all -> 0x019b }
            r12.b(r2, r5)     // Catch:{ all -> 0x019b }
            r5 = 1
            goto L_0x0275
        L_0x03ca:
            r5 = 2
            int r5 = r2 - r5
            int r5 = r12.a(r5)     // Catch:{ all -> 0x019b }
            int r7 = net.weweweb.android.bridge.y.ay     // Catch:{ all -> 0x019b }
            if (r5 != r7) goto L_0x03f8
            r5 = 1
            int r5 = r2 - r5
            int r5 = r12.a(r5)     // Catch:{ all -> 0x019b }
            int r7 = net.weweweb.android.bridge.y.U     // Catch:{ all -> 0x019b }
            if (r5 != r7) goto L_0x03f8
            java.lang.String r5 = "2H"
            byte r5 = a.b.a(r5)     // Catch:{ all -> 0x019b }
            if (r6 == r5) goto L_0x03f0
            java.lang.String r5 = "2H"
            byte r5 = a.b.a(r5)     // Catch:{ all -> 0x019b }
            if (r6 != r5) goto L_0x03f8
        L_0x03f0:
            int r5 = net.weweweb.android.bridge.y.az     // Catch:{ all -> 0x019b }
            r12.b(r2, r5)     // Catch:{ all -> 0x019b }
            r5 = 1
            goto L_0x0275
        L_0x03f8:
            r5 = 0
            goto L_0x0275
        L_0x03fb:
            java.lang.String r5 = "1NT"
            byte r5 = a.b.a(r5)     // Catch:{ all -> 0x019b }
            if (r4 != r5) goto L_0x040a
            int r3 = net.weweweb.android.bridge.y.ap     // Catch:{ all -> 0x019b }
            r12.b(r2, r3)     // Catch:{ all -> 0x019b }
            goto L_0x0165
        L_0x040a:
            java.lang.String r5 = "3NT"
            byte r5 = a.b.a(r5)     // Catch:{ all -> 0x019b }
            if (r4 != r5) goto L_0x0419
            int r3 = net.weweweb.android.bridge.y.am     // Catch:{ all -> 0x019b }
            r12.b(r2, r3)     // Catch:{ all -> 0x019b }
            goto L_0x0165
        L_0x0419:
            byte r5 = a.b.c(r4)     // Catch:{ all -> 0x019b }
            r6 = 1
            if (r5 != r6) goto L_0x0427
            int r3 = net.weweweb.android.bridge.y.aj     // Catch:{ all -> 0x019b }
            r12.b(r2, r3)     // Catch:{ all -> 0x019b }
            goto L_0x0165
        L_0x0427:
            byte r5 = a.b.c(r4)     // Catch:{ all -> 0x019b }
            r6 = 2
            if (r5 != r6) goto L_0x0435
            int r3 = net.weweweb.android.bridge.y.ak     // Catch:{ all -> 0x019b }
            r12.b(r2, r3)     // Catch:{ all -> 0x019b }
            goto L_0x0165
        L_0x0435:
            r5 = 1
            int r5 = r2 - r5
            int r5 = r12.a(r5)     // Catch:{ all -> 0x019b }
            int r6 = net.weweweb.android.bridge.y.Z     // Catch:{ all -> 0x019b }
            if (r5 != r6) goto L_0x0444
            r5 = 97
            if (r4 == r5) goto L_0x0469
        L_0x0444:
            r5 = 3
            int r5 = r2 - r5
            int r5 = r12.a(r5)     // Catch:{ all -> 0x019b }
            int r6 = net.weweweb.android.bridge.y.Z     // Catch:{ all -> 0x019b }
            if (r5 != r6) goto L_0x0470
            r5 = 2
            int r5 = r2 - r5
            int r5 = r12.a(r5)     // Catch:{ all -> 0x019b }
            int r6 = net.weweweb.android.bridge.y.U     // Catch:{ all -> 0x019b }
            if (r5 != r6) goto L_0x0470
            r5 = 1
            int r5 = r2 - r5
            int r5 = r12.a(r5)     // Catch:{ all -> 0x019b }
            int r6 = net.weweweb.android.bridge.y.U     // Catch:{ all -> 0x019b }
            if (r5 != r6) goto L_0x0470
            r5 = 97
            if (r4 != r5) goto L_0x0470
        L_0x0469:
            int r3 = net.weweweb.android.bridge.y.ai     // Catch:{ all -> 0x019b }
            r12.b(r2, r3)     // Catch:{ all -> 0x019b }
            goto L_0x0165
        L_0x0470:
            r5 = 2
            int r5 = r2 - r5
            int r5 = r12.a(r5)     // Catch:{ all -> 0x019b }
            int r6 = net.weweweb.android.bridge.y.X     // Catch:{ all -> 0x019b }
            if (r5 != r6) goto L_0x0495
            boolean r5 = a.b.G(r4)     // Catch:{ all -> 0x019b }
            if (r5 == 0) goto L_0x0488
            int r3 = net.weweweb.android.bridge.y.af     // Catch:{ all -> 0x019b }
            r12.b(r2, r3)     // Catch:{ all -> 0x019b }
            goto L_0x0165
        L_0x0488:
            boolean r5 = a.b.y(r4)     // Catch:{ all -> 0x019b }
            if (r5 == 0) goto L_0x0495
            int r3 = net.weweweb.android.bridge.y.ad     // Catch:{ all -> 0x019b }
            r12.b(r2, r3)     // Catch:{ all -> 0x019b }
            goto L_0x0165
        L_0x0495:
            r5 = 2
            int r5 = r2 - r5
            int r5 = r12.a(r5)     // Catch:{ all -> 0x019b }
            int r6 = net.weweweb.android.bridge.y.Y     // Catch:{ all -> 0x019b }
            if (r5 != r6) goto L_0x04d4
            boolean r5 = a.b.G(r4)     // Catch:{ all -> 0x019b }
            if (r5 == 0) goto L_0x04ad
            int r3 = net.weweweb.android.bridge.y.af     // Catch:{ all -> 0x019b }
            r12.b(r2, r3)     // Catch:{ all -> 0x019b }
            goto L_0x0165
        L_0x04ad:
            byte r5 = a.b.e(r4)     // Catch:{ all -> 0x019b }
            a.b r6 = r12.b     // Catch:{ all -> 0x019b }
            r7 = 2
            int r7 = r2 - r7
            byte r6 = r6.d(r7)     // Catch:{ all -> 0x019b }
            byte r6 = a.b.e(r6)     // Catch:{ all -> 0x019b }
            if (r5 != r6) goto L_0x04c7
            int r3 = net.weweweb.android.bridge.y.aG     // Catch:{ all -> 0x019b }
            r12.b(r2, r3)     // Catch:{ all -> 0x019b }
            goto L_0x0165
        L_0x04c7:
            boolean r5 = a.b.y(r4)     // Catch:{ all -> 0x019b }
            if (r5 == 0) goto L_0x04d4
            int r3 = net.weweweb.android.bridge.y.ad     // Catch:{ all -> 0x019b }
            r12.b(r2, r3)     // Catch:{ all -> 0x019b }
            goto L_0x0165
        L_0x04d4:
            if (r3 != r1) goto L_0x0539
            boolean r5 = a.b.G(r4)     // Catch:{ all -> 0x019b }
            if (r5 == 0) goto L_0x04ea
            byte r5 = a.b.c(r4)     // Catch:{ all -> 0x019b }
            r6 = 3
            if (r5 > r6) goto L_0x04ea
            int r3 = net.weweweb.android.bridge.y.as     // Catch:{ all -> 0x019b }
            r12.b(r2, r3)     // Catch:{ all -> 0x019b }
            goto L_0x0165
        L_0x04ea:
            boolean r5 = a.b.y(r4)     // Catch:{ all -> 0x019b }
            if (r5 == 0) goto L_0x0573
            byte r5 = r12.f98a     // Catch:{ all -> 0x019b }
            byte r6 = a.b.e(r4)     // Catch:{ all -> 0x019b }
            boolean r5 = r12.c(r5, r6)     // Catch:{ all -> 0x019b }
            if (r5 == 0) goto L_0x0510
            int r3 = r12.b(r2)     // Catch:{ all -> 0x019b }
            if (r3 <= 0) goto L_0x0509
            int r3 = net.weweweb.android.bridge.y.at     // Catch:{ all -> 0x019b }
            r12.b(r2, r3)     // Catch:{ all -> 0x019b }
            goto L_0x0165
        L_0x0509:
            int r3 = net.weweweb.android.bridge.y.ar     // Catch:{ all -> 0x019b }
            r12.b(r2, r3)     // Catch:{ all -> 0x019b }
            goto L_0x0165
        L_0x0510:
            byte r3 = a.b.m(r3)     // Catch:{ all -> 0x019b }
            byte r4 = a.b.e(r4)     // Catch:{ all -> 0x019b }
            boolean r3 = r12.c(r3, r4)     // Catch:{ all -> 0x019b }
            if (r3 == 0) goto L_0x0525
            int r3 = net.weweweb.android.bridge.y.aG     // Catch:{ all -> 0x019b }
            r12.b(r2, r3)     // Catch:{ all -> 0x019b }
            goto L_0x0165
        L_0x0525:
            int r3 = r12.b(r2)     // Catch:{ all -> 0x019b }
            if (r3 <= 0) goto L_0x0532
            int r3 = net.weweweb.android.bridge.y.au     // Catch:{ all -> 0x019b }
            r12.b(r2, r3)     // Catch:{ all -> 0x019b }
            goto L_0x0165
        L_0x0532:
            int r3 = net.weweweb.android.bridge.y.aq     // Catch:{ all -> 0x019b }
            r12.b(r2, r3)     // Catch:{ all -> 0x019b }
            goto L_0x0165
        L_0x0539:
            boolean r3 = a.b.h(r3, r1)     // Catch:{ all -> 0x019b }
            if (r3 == 0) goto L_0x0559
            boolean r3 = a.b.G(r4)     // Catch:{ all -> 0x019b }
            if (r3 == 0) goto L_0x054c
            int r3 = net.weweweb.android.bridge.y.af     // Catch:{ all -> 0x019b }
            r12.b(r2, r3)     // Catch:{ all -> 0x019b }
            goto L_0x0165
        L_0x054c:
            boolean r3 = a.b.y(r4)     // Catch:{ all -> 0x019b }
            if (r3 == 0) goto L_0x0573
            int r3 = net.weweweb.android.bridge.y.ad     // Catch:{ all -> 0x019b }
            r12.b(r2, r3)     // Catch:{ all -> 0x019b }
            goto L_0x0165
        L_0x0559:
            boolean r3 = a.b.G(r4)     // Catch:{ all -> 0x019b }
            if (r3 == 0) goto L_0x0566
            int r3 = net.weweweb.android.bridge.y.ap     // Catch:{ all -> 0x019b }
            r12.b(r2, r3)     // Catch:{ all -> 0x019b }
            goto L_0x0165
        L_0x0566:
            boolean r3 = a.b.y(r4)     // Catch:{ all -> 0x019b }
            if (r3 == 0) goto L_0x0573
            int r3 = net.weweweb.android.bridge.y.an     // Catch:{ all -> 0x019b }
            r12.b(r2, r3)     // Catch:{ all -> 0x019b }
            goto L_0x0165
        L_0x0573:
            r3 = 99
            if (r4 != r3) goto L_0x057e
            int r3 = net.weweweb.android.bridge.y.U     // Catch:{ all -> 0x019b }
            r12.b(r2, r3)     // Catch:{ all -> 0x019b }
            goto L_0x0165
        L_0x057e:
            int r3 = net.weweweb.android.bridge.y.T     // Catch:{ all -> 0x019b }
            r12.b(r2, r3)     // Catch:{ all -> 0x019b }
            goto L_0x0165
        L_0x0585:
            android.os.Message r1 = android.os.Message.obtain()     // Catch:{ all -> 0x019b }
            r2 = 1001(0x3e9, float:1.403E-42)
            r1.what = r2     // Catch:{ all -> 0x019b }
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            boolean r2 = r2.N()     // Catch:{ all -> 0x019b }
            if (r2 == 0) goto L_0x08ba
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r12.f98a     // Catch:{ all -> 0x019b }
            boolean r2 = r2.J(r3)     // Catch:{ all -> 0x019b }
            if (r2 != 0) goto L_0x078c
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r2 = r2.e()     // Catch:{ all -> 0x019b }
            r3 = 2
            if (r2 > r3) goto L_0x078c
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r12.f98a     // Catch:{ all -> 0x019b }
            r4 = 83
            int r2 = r2.c(r3, r4)     // Catch:{ all -> 0x019b }
            r3 = 7
            if (r2 < r3) goto L_0x05ec
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r12.f98a     // Catch:{ all -> 0x019b }
            r4 = 83
            java.lang.String r5 = "AKQ"
            r6 = 2
            boolean r2 = r2.a(r3, r4, r5, r6)     // Catch:{ all -> 0x019b }
            if (r2 == 0) goto L_0x05ec
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r12.f98a     // Catch:{ all -> 0x019b }
            boolean r2 = r2.x(r3)     // Catch:{ all -> 0x019b }
            if (r2 != 0) goto L_0x05ec
            java.lang.String r2 = "3S"
            byte r2 = a.b.a(r2)     // Catch:{ all -> 0x019b }
        L_0x05d4:
            r1.arg1 = r2     // Catch:{ all -> 0x019b }
            boolean r2 = r12.c     // Catch:{ all -> 0x019b }
            if (r2 == 0) goto L_0x10f5
            java.lang.String r2 = r12.d     // Catch:{ all -> 0x019b }
        L_0x05dc:
            r1.obj = r2     // Catch:{ all -> 0x019b }
            android.os.Handler r2 = r12.P     // Catch:{ all -> 0x019b }
            r2.sendMessage(r1)     // Catch:{ all -> 0x019b }
            r1 = 1000(0x3e8, float:1.401E-42)
            r12.O = r1     // Catch:{ all -> 0x019b }
            r1 = 0
            r12.c = r1     // Catch:{ all -> 0x019b }
        L_0x05ea:
            monitor-exit(r0)     // Catch:{ all -> 0x019b }
            return
        L_0x05ec:
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r12.f98a     // Catch:{ all -> 0x019b }
            r4 = 72
            int r2 = r2.c(r3, r4)     // Catch:{ all -> 0x019b }
            r3 = 7
            if (r2 < r3) goto L_0x0619
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r12.f98a     // Catch:{ all -> 0x019b }
            r4 = 72
            java.lang.String r5 = "AKQ"
            r6 = 2
            boolean r2 = r2.a(r3, r4, r5, r6)     // Catch:{ all -> 0x019b }
            if (r2 == 0) goto L_0x0619
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r12.f98a     // Catch:{ all -> 0x019b }
            boolean r2 = r2.x(r3)     // Catch:{ all -> 0x019b }
            if (r2 != 0) goto L_0x0619
            java.lang.String r2 = "3H"
            byte r2 = a.b.a(r2)     // Catch:{ all -> 0x019b }
            goto L_0x05d4
        L_0x0619:
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r12.f98a     // Catch:{ all -> 0x019b }
            r4 = 68
            int r2 = r2.c(r3, r4)     // Catch:{ all -> 0x019b }
            r3 = 7
            if (r2 < r3) goto L_0x0646
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r12.f98a     // Catch:{ all -> 0x019b }
            r4 = 68
            java.lang.String r5 = "AKQ"
            r6 = 2
            boolean r2 = r2.a(r3, r4, r5, r6)     // Catch:{ all -> 0x019b }
            if (r2 == 0) goto L_0x0646
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r12.f98a     // Catch:{ all -> 0x019b }
            boolean r2 = r2.x(r3)     // Catch:{ all -> 0x019b }
            if (r2 != 0) goto L_0x0646
            java.lang.String r2 = "3D"
            byte r2 = a.b.a(r2)     // Catch:{ all -> 0x019b }
            goto L_0x05d4
        L_0x0646:
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r12.f98a     // Catch:{ all -> 0x019b }
            r4 = 67
            int r2 = r2.c(r3, r4)     // Catch:{ all -> 0x019b }
            r3 = 7
            if (r2 < r3) goto L_0x0674
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r12.f98a     // Catch:{ all -> 0x019b }
            r4 = 67
            java.lang.String r5 = "AKQ"
            r6 = 2
            boolean r2 = r2.a(r3, r4, r5, r6)     // Catch:{ all -> 0x019b }
            if (r2 == 0) goto L_0x0674
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r12.f98a     // Catch:{ all -> 0x019b }
            boolean r2 = r2.x(r3)     // Catch:{ all -> 0x019b }
            if (r2 != 0) goto L_0x0674
            java.lang.String r2 = "3C"
            byte r2 = a.b.a(r2)     // Catch:{ all -> 0x019b }
            goto L_0x05d4
        L_0x0674:
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r12.f98a     // Catch:{ all -> 0x019b }
            r4 = 83
            int r2 = r2.c(r3, r4)     // Catch:{ all -> 0x019b }
            r3 = 7
            if (r2 != r3) goto L_0x06a2
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r12.f98a     // Catch:{ all -> 0x019b }
            r4 = 83
            java.lang.String r5 = "AKQ"
            r6 = 1
            boolean r2 = r2.a(r3, r4, r5, r6)     // Catch:{ all -> 0x019b }
            if (r2 == 0) goto L_0x06a2
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r12.f98a     // Catch:{ all -> 0x019b }
            boolean r2 = r2.x(r3)     // Catch:{ all -> 0x019b }
            if (r2 != 0) goto L_0x06a2
            java.lang.String r2 = "2S"
            byte r2 = a.b.a(r2)     // Catch:{ all -> 0x019b }
            goto L_0x05d4
        L_0x06a2:
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r12.f98a     // Catch:{ all -> 0x019b }
            r4 = 72
            int r2 = r2.c(r3, r4)     // Catch:{ all -> 0x019b }
            r3 = 7
            if (r2 != r3) goto L_0x06d0
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r12.f98a     // Catch:{ all -> 0x019b }
            r4 = 72
            java.lang.String r5 = "AKQ"
            r6 = 1
            boolean r2 = r2.a(r3, r4, r5, r6)     // Catch:{ all -> 0x019b }
            if (r2 == 0) goto L_0x06d0
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r12.f98a     // Catch:{ all -> 0x019b }
            boolean r2 = r2.x(r3)     // Catch:{ all -> 0x019b }
            if (r2 != 0) goto L_0x06d0
            java.lang.String r2 = "2H"
            byte r2 = a.b.a(r2)     // Catch:{ all -> 0x019b }
            goto L_0x05d4
        L_0x06d0:
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r12.f98a     // Catch:{ all -> 0x019b }
            r4 = 68
            int r2 = r2.c(r3, r4)     // Catch:{ all -> 0x019b }
            r3 = 7
            if (r2 != r3) goto L_0x06fe
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r12.f98a     // Catch:{ all -> 0x019b }
            r4 = 68
            java.lang.String r5 = "AKQ"
            r6 = 1
            boolean r2 = r2.a(r3, r4, r5, r6)     // Catch:{ all -> 0x019b }
            if (r2 == 0) goto L_0x06fe
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r12.f98a     // Catch:{ all -> 0x019b }
            boolean r2 = r2.x(r3)     // Catch:{ all -> 0x019b }
            if (r2 != 0) goto L_0x06fe
            java.lang.String r2 = "2D"
            byte r2 = a.b.a(r2)     // Catch:{ all -> 0x019b }
            goto L_0x05d4
        L_0x06fe:
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r12.f98a     // Catch:{ all -> 0x019b }
            r4 = 83
            int r2 = r2.c(r3, r4)     // Catch:{ all -> 0x019b }
            r3 = 6
            if (r2 < r3) goto L_0x072c
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r12.f98a     // Catch:{ all -> 0x019b }
            r4 = 83
            java.lang.String r5 = "AKQ"
            r6 = 2
            boolean r2 = r2.a(r3, r4, r5, r6)     // Catch:{ all -> 0x019b }
            if (r2 == 0) goto L_0x072c
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r12.f98a     // Catch:{ all -> 0x019b }
            boolean r2 = r2.x(r3)     // Catch:{ all -> 0x019b }
            if (r2 != 0) goto L_0x072c
            java.lang.String r2 = "2S"
            byte r2 = a.b.a(r2)     // Catch:{ all -> 0x019b }
            goto L_0x05d4
        L_0x072c:
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r12.f98a     // Catch:{ all -> 0x019b }
            r4 = 72
            int r2 = r2.c(r3, r4)     // Catch:{ all -> 0x019b }
            r3 = 6
            if (r2 < r3) goto L_0x075a
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r12.f98a     // Catch:{ all -> 0x019b }
            r4 = 72
            java.lang.String r5 = "AKQ"
            r6 = 2
            boolean r2 = r2.a(r3, r4, r5, r6)     // Catch:{ all -> 0x019b }
            if (r2 == 0) goto L_0x075a
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r12.f98a     // Catch:{ all -> 0x019b }
            boolean r2 = r2.x(r3)     // Catch:{ all -> 0x019b }
            if (r2 != 0) goto L_0x075a
            java.lang.String r2 = "2H"
            byte r2 = a.b.a(r2)     // Catch:{ all -> 0x019b }
            goto L_0x05d4
        L_0x075a:
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r12.f98a     // Catch:{ all -> 0x019b }
            r4 = 68
            int r2 = r2.c(r3, r4)     // Catch:{ all -> 0x019b }
            r3 = 6
            if (r2 < r3) goto L_0x0788
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r12.f98a     // Catch:{ all -> 0x019b }
            r4 = 68
            java.lang.String r5 = "AKQ"
            r6 = 2
            boolean r2 = r2.a(r3, r4, r5, r6)     // Catch:{ all -> 0x019b }
            if (r2 == 0) goto L_0x0788
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r12.f98a     // Catch:{ all -> 0x019b }
            boolean r2 = r2.x(r3)     // Catch:{ all -> 0x019b }
            if (r2 != 0) goto L_0x0788
            java.lang.String r2 = "2D"
            byte r2 = a.b.a(r2)     // Catch:{ all -> 0x019b }
            goto L_0x05d4
        L_0x0788:
            r2 = 99
            goto L_0x05d4
        L_0x078c:
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r12.f98a     // Catch:{ all -> 0x019b }
            boolean r2 = r2.I(r3)     // Catch:{ all -> 0x019b }
            if (r2 != 0) goto L_0x07a3
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r2 = r2.e()     // Catch:{ all -> 0x019b }
            r3 = 3
            if (r2 != r3) goto L_0x07a3
            r2 = 99
            goto L_0x05d4
        L_0x07a3:
            int r2 = r12.w     // Catch:{ all -> 0x019b }
            r3 = 15
            if (r2 < r3) goto L_0x07c2
            int r2 = r12.w     // Catch:{ all -> 0x019b }
            r3 = 17
            if (r2 > r3) goto L_0x07c2
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r12.f98a     // Catch:{ all -> 0x019b }
            r4 = 1
            boolean r2 = r2.a(r3, r4)     // Catch:{ all -> 0x019b }
            if (r2 == 0) goto L_0x07c2
            java.lang.String r2 = "1NT"
            byte r2 = a.b.a(r2)     // Catch:{ all -> 0x019b }
            goto L_0x05d4
        L_0x07c2:
            int r2 = r12.w     // Catch:{ all -> 0x019b }
            r3 = 20
            if (r2 < r3) goto L_0x07e1
            int r2 = r12.w     // Catch:{ all -> 0x019b }
            r3 = 21
            if (r2 > r3) goto L_0x07e1
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r12.f98a     // Catch:{ all -> 0x019b }
            r4 = 1
            boolean r2 = r2.a(r3, r4)     // Catch:{ all -> 0x019b }
            if (r2 == 0) goto L_0x07e1
            java.lang.String r2 = "2NT"
            byte r2 = a.b.a(r2)     // Catch:{ all -> 0x019b }
            goto L_0x05d4
        L_0x07e1:
            int r2 = r12.w     // Catch:{ all -> 0x019b }
            r3 = 25
            if (r2 < r3) goto L_0x0800
            int r2 = r12.w     // Catch:{ all -> 0x019b }
            r3 = 27
            if (r2 > r3) goto L_0x0800
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r12.f98a     // Catch:{ all -> 0x019b }
            r4 = 1
            boolean r2 = r2.a(r3, r4)     // Catch:{ all -> 0x019b }
            if (r2 == 0) goto L_0x0800
            java.lang.String r2 = "3NT"
            byte r2 = a.b.a(r2)     // Catch:{ all -> 0x019b }
            goto L_0x05d4
        L_0x0800:
            int r2 = r12.w     // Catch:{ all -> 0x019b }
            r3 = 22
            if (r2 >= r3) goto L_0x0814
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r12.f98a     // Catch:{ all -> 0x019b }
            float r2 = r2.n(r3)     // Catch:{ all -> 0x019b }
            r3 = 1091567616(0x41100000, float:9.0)
            int r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
            if (r2 < 0) goto L_0x081c
        L_0x0814:
            java.lang.String r2 = "2C"
            byte r2 = a.b.a(r2)     // Catch:{ all -> 0x019b }
            goto L_0x05d4
        L_0x081c:
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r12.f98a     // Catch:{ all -> 0x019b }
            r4 = 72
            int r2 = r2.c(r3, r4)     // Catch:{ all -> 0x019b }
            r3 = 5
            if (r2 >= r3) goto L_0x0836
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r12.f98a     // Catch:{ all -> 0x019b }
            r4 = 83
            int r2 = r2.c(r3, r4)     // Catch:{ all -> 0x019b }
            r3 = 5
            if (r2 < r3) goto L_0x085c
        L_0x0836:
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r12.f98a     // Catch:{ all -> 0x019b }
            r4 = 83
            int r2 = r2.c(r3, r4)     // Catch:{ all -> 0x019b }
            a.b r3 = r12.b     // Catch:{ all -> 0x019b }
            byte r4 = r12.f98a     // Catch:{ all -> 0x019b }
            r5 = 72
            int r3 = r3.c(r4, r5)     // Catch:{ all -> 0x019b }
            if (r2 < r3) goto L_0x0854
            java.lang.String r2 = "1S"
            byte r2 = a.b.a(r2)     // Catch:{ all -> 0x019b }
            goto L_0x05d4
        L_0x0854:
            java.lang.String r2 = "1H"
            byte r2 = a.b.a(r2)     // Catch:{ all -> 0x019b }
            goto L_0x05d4
        L_0x085c:
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r12.f98a     // Catch:{ all -> 0x019b }
            r4 = 68
            int r2 = r2.c(r3, r4)     // Catch:{ all -> 0x019b }
            a.b r3 = r12.b     // Catch:{ all -> 0x019b }
            byte r4 = r12.f98a     // Catch:{ all -> 0x019b }
            r5 = 67
            int r3 = r3.c(r4, r5)     // Catch:{ all -> 0x019b }
            if (r2 > r3) goto L_0x08b2
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r12.f98a     // Catch:{ all -> 0x019b }
            r4 = 68
            int r2 = r2.c(r3, r4)     // Catch:{ all -> 0x019b }
            a.b r3 = r12.b     // Catch:{ all -> 0x019b }
            byte r4 = r12.f98a     // Catch:{ all -> 0x019b }
            r5 = 67
            int r3 = r3.c(r4, r5)     // Catch:{ all -> 0x019b }
            if (r2 >= r3) goto L_0x0890
            java.lang.String r2 = "1C"
            byte r2 = a.b.a(r2)     // Catch:{ all -> 0x019b }
            goto L_0x05d4
        L_0x0890:
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r12.f98a     // Catch:{ all -> 0x019b }
            r4 = 68
            int r2 = r2.c(r3, r4)     // Catch:{ all -> 0x019b }
            r3 = 3
            if (r2 != r3) goto L_0x08b2
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r12.f98a     // Catch:{ all -> 0x019b }
            r4 = 67
            int r2 = r2.c(r3, r4)     // Catch:{ all -> 0x019b }
            r3 = 3
            if (r2 != r3) goto L_0x08b2
            java.lang.String r2 = "1C"
            byte r2 = a.b.a(r2)     // Catch:{ all -> 0x019b }
            goto L_0x05d4
        L_0x08b2:
            java.lang.String r2 = "1D"
            byte r2 = a.b.a(r2)     // Catch:{ all -> 0x019b }
            goto L_0x05d4
        L_0x08ba:
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            boolean r2 = r2.L()     // Catch:{ all -> 0x019b }
            if (r2 == 0) goto L_0x08c8
            byte r2 = r12.f()     // Catch:{ all -> 0x019b }
            goto L_0x05d4
        L_0x08c8:
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            boolean r2 = r2.O()     // Catch:{ all -> 0x019b }
            if (r2 == 0) goto L_0x0b9a
            int r2 = r12.w     // Catch:{ all -> 0x019b }
            r3 = 8
            if (r2 < r3) goto L_0x0b96
            byte r2 = r12.v     // Catch:{ all -> 0x019b }
            r3 = -1
            if (r2 == r3) goto L_0x0b96
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r2 = r2.w()     // Catch:{ all -> 0x019b }
            a.b r3 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r3.w()     // Catch:{ all -> 0x019b }
            byte r3 = a.b.i(r3)     // Catch:{ all -> 0x019b }
            char r4 = a.b.q(r3)     // Catch:{ all -> 0x019b }
            byte r5 = r12.v     // Catch:{ all -> 0x019b }
            r6 = 1
            if (r5 != r6) goto L_0x0b88
            byte[] r2 = r12.x     // Catch:{ all -> 0x019b }
            byte r5 = r12.A     // Catch:{ all -> 0x019b }
            byte r2 = r2[r5]     // Catch:{ all -> 0x019b }
            r5 = 5
            if (r2 < r5) goto L_0x0995
            boolean r2 = a.b.F(r3)     // Catch:{ all -> 0x019b }
            if (r2 == 0) goto L_0x094b
            byte[] r2 = r12.x     // Catch:{ all -> 0x019b }
            r5 = 3
            byte r2 = r2[r5]     // Catch:{ all -> 0x019b }
            r5 = 5
            if (r2 < r5) goto L_0x0995
            byte[] r2 = r12.x     // Catch:{ all -> 0x019b }
            r5 = 2
            byte r2 = r2[r5]     // Catch:{ all -> 0x019b }
            r5 = 5
            if (r2 < r5) goto L_0x0995
            int r2 = r12.w     // Catch:{ all -> 0x019b }
            r5 = 16
            if (r2 < r5) goto L_0x0920
            r2 = 2
            byte r2 = a.b.a(r2, r3)     // Catch:{ all -> 0x019b }
            goto L_0x05d4
        L_0x0920:
            int r2 = r12.w     // Catch:{ all -> 0x019b }
            r5 = 8
            if (r2 < r5) goto L_0x0995
            int r2 = r12.w     // Catch:{ all -> 0x019b }
            r5 = 10
            if (r2 > r5) goto L_0x0995
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r5 = r12.f98a     // Catch:{ all -> 0x019b }
            r6 = 72
            int r2 = r2.b(r5, r6)     // Catch:{ all -> 0x019b }
            a.b r5 = r12.b     // Catch:{ all -> 0x019b }
            byte r6 = r12.f98a     // Catch:{ all -> 0x019b }
            r7 = 83
            int r5 = r5.b(r6, r7)     // Catch:{ all -> 0x019b }
            int r2 = r2 + r5
            r5 = 7
            if (r2 < r5) goto L_0x0995
            r2 = 2
            byte r2 = a.b.a(r2, r3)     // Catch:{ all -> 0x019b }
            goto L_0x05d4
        L_0x094b:
            byte[] r2 = r12.x     // Catch:{ all -> 0x019b }
            byte r2 = r2[r3]     // Catch:{ all -> 0x019b }
            r5 = 5
            if (r2 >= r5) goto L_0x0995
            byte[] r2 = r12.x     // Catch:{ all -> 0x019b }
            byte r5 = a.b.b(r3)     // Catch:{ all -> 0x019b }
            byte r2 = r2[r5]     // Catch:{ all -> 0x019b }
            r5 = 5
            if (r2 < r5) goto L_0x0995
            int r2 = r12.w     // Catch:{ all -> 0x019b }
            r5 = 16
            if (r2 < r5) goto L_0x096a
            r2 = 2
            byte r2 = a.b.a(r2, r3)     // Catch:{ all -> 0x019b }
            goto L_0x05d4
        L_0x096a:
            int r2 = r12.w     // Catch:{ all -> 0x019b }
            r5 = 8
            if (r2 < r5) goto L_0x0995
            int r2 = r12.w     // Catch:{ all -> 0x019b }
            r5 = 10
            if (r2 > r5) goto L_0x0995
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r5 = r12.f98a     // Catch:{ all -> 0x019b }
            r6 = 72
            int r2 = r2.b(r5, r6)     // Catch:{ all -> 0x019b }
            a.b r5 = r12.b     // Catch:{ all -> 0x019b }
            byte r6 = r12.f98a     // Catch:{ all -> 0x019b }
            r7 = 83
            int r5 = r5.b(r6, r7)     // Catch:{ all -> 0x019b }
            int r2 = r2 + r5
            r5 = 7
            if (r2 < r5) goto L_0x0995
            r2 = 2
            byte r2 = a.b.a(r2, r3)     // Catch:{ all -> 0x019b }
            goto L_0x05d4
        L_0x0995:
            int r2 = r12.w     // Catch:{ all -> 0x019b }
            r5 = 8
            if (r2 < r5) goto L_0x0a48
            int r2 = r12.w     // Catch:{ all -> 0x019b }
            r5 = 10
            if (r2 > r5) goto L_0x0a48
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r5 = r12.f98a     // Catch:{ all -> 0x019b }
            char r6 = r12.z     // Catch:{ all -> 0x019b }
            java.lang.String r7 = "AKQJT"
            r8 = 3
            boolean r2 = r2.a(r5, r6, r7, r8)     // Catch:{ all -> 0x019b }
            if (r2 != 0) goto L_0x09bf
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r5 = r12.f98a     // Catch:{ all -> 0x019b }
            char r6 = r12.z     // Catch:{ all -> 0x019b }
            java.lang.String r7 = "AKQ"
            r8 = 2
            boolean r2 = r2.a(r5, r6, r7, r8)     // Catch:{ all -> 0x019b }
            if (r2 == 0) goto L_0x0a48
        L_0x09bf:
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r5 = r12.f98a     // Catch:{ all -> 0x019b }
            char r6 = r12.z     // Catch:{ all -> 0x019b }
            int r2 = r2.c(r5, r6)     // Catch:{ all -> 0x019b }
            r5 = 6
            if (r2 != r5) goto L_0x09ef
            byte r2 = r12.y     // Catch:{ all -> 0x019b }
            if (r2 <= r3) goto L_0x09ef
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x019b }
            r2.<init>()     // Catch:{ all -> 0x019b }
            java.lang.String r3 = "2"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x019b }
            char r3 = r12.z     // Catch:{ all -> 0x019b }
            java.lang.String r3 = java.lang.Character.toString(r3)     // Catch:{ all -> 0x019b }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x019b }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x019b }
            byte r2 = a.b.a(r2)     // Catch:{ all -> 0x019b }
            goto L_0x05d4
        L_0x09ef:
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r5 = r12.f98a     // Catch:{ all -> 0x019b }
            char r6 = r12.z     // Catch:{ all -> 0x019b }
            int r2 = r2.c(r5, r6)     // Catch:{ all -> 0x019b }
            r5 = 7
            if (r2 != r5) goto L_0x0a1b
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x019b }
            r2.<init>()     // Catch:{ all -> 0x019b }
            java.lang.String r3 = "3"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x019b }
            char r3 = r12.z     // Catch:{ all -> 0x019b }
            java.lang.String r3 = java.lang.Character.toString(r3)     // Catch:{ all -> 0x019b }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x019b }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x019b }
            byte r2 = a.b.a(r2)     // Catch:{ all -> 0x019b }
            goto L_0x05d4
        L_0x0a1b:
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r5 = r12.f98a     // Catch:{ all -> 0x019b }
            char r6 = r12.z     // Catch:{ all -> 0x019b }
            int r2 = r2.c(r5, r6)     // Catch:{ all -> 0x019b }
            r5 = 8
            if (r2 != r5) goto L_0x0a48
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x019b }
            r2.<init>()     // Catch:{ all -> 0x019b }
            java.lang.String r3 = "4"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x019b }
            char r3 = r12.z     // Catch:{ all -> 0x019b }
            java.lang.String r3 = java.lang.Character.toString(r3)     // Catch:{ all -> 0x019b }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x019b }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x019b }
            byte r2 = a.b.a(r2)     // Catch:{ all -> 0x019b }
            goto L_0x05d4
        L_0x0a48:
            int r2 = r12.w     // Catch:{ all -> 0x019b }
            r5 = 12
            if (r2 < r5) goto L_0x0a6d
            byte[] r2 = r12.x     // Catch:{ all -> 0x019b }
            byte r2 = r2[r3]     // Catch:{ all -> 0x019b }
            r5 = 1
            if (r2 > r5) goto L_0x0a6d
            java.lang.String r2 = r12.C     // Catch:{ all -> 0x019b }
            java.lang.String r5 = "5-4-4-0"
            boolean r2 = r2.equals(r5)     // Catch:{ all -> 0x019b }
            if (r2 != 0) goto L_0x0a69
            java.lang.String r2 = r12.C     // Catch:{ all -> 0x019b }
            java.lang.String r5 = "4-4-4-1"
            boolean r2 = r2.equals(r5)     // Catch:{ all -> 0x019b }
            if (r2 == 0) goto L_0x0a6d
        L_0x0a69:
            r2 = 97
            goto L_0x05d4
        L_0x0a6d:
            int r2 = r12.w     // Catch:{ all -> 0x019b }
            r5 = 16
            if (r2 < r5) goto L_0x0a80
            byte[] r2 = r12.x     // Catch:{ all -> 0x019b }
            byte r5 = r12.y     // Catch:{ all -> 0x019b }
            byte r2 = r2[r5]     // Catch:{ all -> 0x019b }
            r5 = 5
            if (r2 < r5) goto L_0x0a80
            r2 = 97
            goto L_0x05d4
        L_0x0a80:
            int r2 = r12.w     // Catch:{ all -> 0x019b }
            r5 = 19
            if (r2 < r5) goto L_0x0a95
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r5 = r12.f98a     // Catch:{ all -> 0x019b }
            r6 = 1
            boolean r2 = r2.a(r5, r6)     // Catch:{ all -> 0x019b }
            if (r2 == 0) goto L_0x0a95
            r2 = 97
            goto L_0x05d4
        L_0x0a95:
            byte[] r2 = r12.x     // Catch:{ all -> 0x019b }
            byte r5 = r12.y     // Catch:{ all -> 0x019b }
            byte r2 = r2[r5]     // Catch:{ all -> 0x019b }
            r5 = 5
            if (r2 < r5) goto L_0x0b5c
            int r2 = r12.w     // Catch:{ all -> 0x019b }
            r4 = 15
            if (r2 > r4) goto L_0x0b96
            byte r2 = r12.y     // Catch:{ all -> 0x019b }
            if (r2 == r3) goto L_0x0b96
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r4 = r12.f98a     // Catch:{ all -> 0x019b }
            char r5 = r12.z     // Catch:{ all -> 0x019b }
            java.lang.String r6 = "AKQJ"
            r7 = 2
            boolean r2 = r2.a(r4, r5, r6, r7)     // Catch:{ all -> 0x019b }
            if (r2 == 0) goto L_0x0b10
            byte r2 = r12.y     // Catch:{ all -> 0x019b }
            if (r2 <= r3) goto L_0x0ada
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x019b }
            r2.<init>()     // Catch:{ all -> 0x019b }
            java.lang.String r3 = "1"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x019b }
            char r3 = r12.z     // Catch:{ all -> 0x019b }
            java.lang.String r3 = java.lang.Character.toString(r3)     // Catch:{ all -> 0x019b }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x019b }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x019b }
            byte r2 = a.b.a(r2)     // Catch:{ all -> 0x019b }
            goto L_0x05d4
        L_0x0ada:
            byte r2 = r12.y     // Catch:{ all -> 0x019b }
            if (r2 >= r3) goto L_0x0b10
            int r2 = r12.w     // Catch:{ all -> 0x019b }
            r3 = 10
            if (r2 >= r3) goto L_0x0af1
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r12.f98a     // Catch:{ all -> 0x019b }
            char r4 = r12.z     // Catch:{ all -> 0x019b }
            int r2 = r2.c(r3, r4)     // Catch:{ all -> 0x019b }
            r3 = 5
            if (r2 == r3) goto L_0x0b96
        L_0x0af1:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x019b }
            r2.<init>()     // Catch:{ all -> 0x019b }
            java.lang.String r3 = "2"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x019b }
            char r3 = r12.z     // Catch:{ all -> 0x019b }
            java.lang.String r3 = java.lang.Character.toString(r3)     // Catch:{ all -> 0x019b }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x019b }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x019b }
            byte r2 = a.b.a(r2)     // Catch:{ all -> 0x019b }
            goto L_0x05d4
        L_0x0b10:
            int r2 = r12.w     // Catch:{ all -> 0x019b }
            r4 = 13
            if (r2 < r4) goto L_0x0b96
            byte r2 = r12.y     // Catch:{ all -> 0x019b }
            if (r2 <= r3) goto L_0x0b39
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x019b }
            r2.<init>()     // Catch:{ all -> 0x019b }
            java.lang.String r3 = "1"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x019b }
            char r3 = r12.z     // Catch:{ all -> 0x019b }
            java.lang.String r3 = java.lang.Character.toString(r3)     // Catch:{ all -> 0x019b }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x019b }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x019b }
            byte r2 = a.b.a(r2)     // Catch:{ all -> 0x019b }
            goto L_0x05d4
        L_0x0b39:
            byte r2 = r12.y     // Catch:{ all -> 0x019b }
            if (r2 >= r3) goto L_0x0b96
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x019b }
            r2.<init>()     // Catch:{ all -> 0x019b }
            java.lang.String r3 = "2"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x019b }
            char r3 = r12.z     // Catch:{ all -> 0x019b }
            java.lang.String r3 = java.lang.Character.toString(r3)     // Catch:{ all -> 0x019b }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x019b }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x019b }
            byte r2 = a.b.a(r2)     // Catch:{ all -> 0x019b }
            goto L_0x05d4
        L_0x0b5c:
            int r2 = r12.w     // Catch:{ all -> 0x019b }
            r3 = 15
            if (r2 < r3) goto L_0x0b96
            int r2 = r12.w     // Catch:{ all -> 0x019b }
            r3 = 18
            if (r2 > r3) goto L_0x0b96
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r12.f98a     // Catch:{ all -> 0x019b }
            r5 = 1
            boolean r2 = r2.a(r3, r5)     // Catch:{ all -> 0x019b }
            if (r2 == 0) goto L_0x0b96
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r12.f98a     // Catch:{ all -> 0x019b }
            java.lang.String r5 = "AK"
            r6 = 1
            boolean r2 = r2.a(r3, r4, r5, r6)     // Catch:{ all -> 0x019b }
            if (r2 == 0) goto L_0x0b96
            java.lang.String r2 = "1NT"
            byte r2 = a.b.a(r2)     // Catch:{ all -> 0x019b }
            goto L_0x05d4
        L_0x0b88:
            java.lang.String r3 = "1NT"
            byte r3 = a.b.a(r3)     // Catch:{ all -> 0x019b }
            if (r2 != r3) goto L_0x0b96
            byte r2 = r12.e()     // Catch:{ all -> 0x019b }
            goto L_0x05d4
        L_0x0b96:
            r2 = 99
            goto L_0x05d4
        L_0x0b9a:
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            boolean r2 = r2.H()     // Catch:{ all -> 0x019b }
            if (r2 == 0) goto L_0x0df7
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r2 = r2.w()     // Catch:{ all -> 0x019b }
            byte r2 = a.b.i(r2)     // Catch:{ all -> 0x019b }
            a.b r3 = r12.b     // Catch:{ all -> 0x019b }
            r4 = -3
            byte r3 = r3.b(r4)     // Catch:{ all -> 0x019b }
            r4 = -1
            if (r3 != r4) goto L_0x0d56
            byte[] r3 = r12.x     // Catch:{ all -> 0x019b }
            byte r4 = r12.y     // Catch:{ all -> 0x019b }
            byte r3 = r3[r4]     // Catch:{ all -> 0x019b }
            r4 = 4
            if (r3 > r4) goto L_0x0be3
            a.b r3 = r12.b     // Catch:{ all -> 0x019b }
            byte r4 = r12.f98a     // Catch:{ all -> 0x019b }
            r5 = 1
            boolean r3 = r3.a(r4, r5)     // Catch:{ all -> 0x019b }
            if (r3 == 0) goto L_0x0be3
            int r3 = r12.w     // Catch:{ all -> 0x019b }
            r4 = 19
            if (r3 < r4) goto L_0x0be3
            int r3 = r12.w     // Catch:{ all -> 0x019b }
            r4 = 21
            if (r3 > r4) goto L_0x0be3
            byte r3 = r12.v     // Catch:{ all -> 0x019b }
            r4 = 1
            if (r3 != r4) goto L_0x0be3
            java.lang.String r2 = "2NT"
            byte r2 = a.b.a(r2)     // Catch:{ all -> 0x019b }
            goto L_0x05d4
        L_0x0be3:
            byte r3 = r12.y     // Catch:{ all -> 0x019b }
            if (r3 == r2) goto L_0x0d29
            byte[] r3 = r12.x     // Catch:{ all -> 0x019b }
            byte r4 = r12.y     // Catch:{ all -> 0x019b }
            byte r3 = r3[r4]     // Catch:{ all -> 0x019b }
            r4 = 5
            if (r3 < r4) goto L_0x0d29
            byte r3 = r12.v     // Catch:{ all -> 0x019b }
            r4 = 1
            if (r3 != r4) goto L_0x0ca6
            byte r3 = r12.y     // Catch:{ all -> 0x019b }
            if (r3 <= r2) goto L_0x0c69
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r12.f98a     // Catch:{ all -> 0x019b }
            char r4 = r12.z     // Catch:{ all -> 0x019b }
            java.lang.String r5 = "AKQ"
            r6 = 2
            boolean r2 = r2.a(r3, r4, r5, r6)     // Catch:{ all -> 0x019b }
            if (r2 == 0) goto L_0x0c36
            byte[] r2 = r12.x     // Catch:{ all -> 0x019b }
            byte r3 = r12.y     // Catch:{ all -> 0x019b }
            byte r2 = r2[r3]     // Catch:{ all -> 0x019b }
            r3 = 6
            if (r2 < r3) goto L_0x0c36
            int r2 = r12.w     // Catch:{ all -> 0x019b }
            r3 = 10
            if (r2 < r3) goto L_0x0c36
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x019b }
            r2.<init>()     // Catch:{ all -> 0x019b }
            java.lang.String r3 = "2"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x019b }
            char r3 = r12.z     // Catch:{ all -> 0x019b }
            java.lang.String r3 = java.lang.Character.toString(r3)     // Catch:{ all -> 0x019b }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x019b }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x019b }
            byte r2 = a.b.a(r2)     // Catch:{ all -> 0x019b }
            goto L_0x05d4
        L_0x0c36:
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r12.f98a     // Catch:{ all -> 0x019b }
            char r4 = r12.z     // Catch:{ all -> 0x019b }
            java.lang.String r5 = "AKQJ"
            r6 = 2
            boolean r2 = r2.a(r3, r4, r5, r6)     // Catch:{ all -> 0x019b }
            if (r2 == 0) goto L_0x0df3
            int r2 = r12.w     // Catch:{ all -> 0x019b }
            r3 = 5
            if (r2 < r3) goto L_0x0df3
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x019b }
            r2.<init>()     // Catch:{ all -> 0x019b }
            java.lang.String r3 = "1"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x019b }
            char r3 = r12.z     // Catch:{ all -> 0x019b }
            java.lang.String r3 = java.lang.Character.toString(r3)     // Catch:{ all -> 0x019b }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x019b }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x019b }
            byte r2 = a.b.a(r2)     // Catch:{ all -> 0x019b }
            goto L_0x05d4
        L_0x0c69:
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r12.f98a     // Catch:{ all -> 0x019b }
            char r4 = r12.z     // Catch:{ all -> 0x019b }
            java.lang.String r5 = "AKQ"
            r6 = 2
            boolean r2 = r2.a(r3, r4, r5, r6)     // Catch:{ all -> 0x019b }
            if (r2 == 0) goto L_0x0df3
            byte[] r2 = r12.x     // Catch:{ all -> 0x019b }
            byte r3 = r12.y     // Catch:{ all -> 0x019b }
            byte r2 = r2[r3]     // Catch:{ all -> 0x019b }
            r3 = 5
            if (r2 < r3) goto L_0x0df3
            int r2 = r12.w     // Catch:{ all -> 0x019b }
            r3 = 10
            if (r2 < r3) goto L_0x0df3
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x019b }
            r2.<init>()     // Catch:{ all -> 0x019b }
            java.lang.String r3 = "2"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x019b }
            char r3 = r12.z     // Catch:{ all -> 0x019b }
            java.lang.String r3 = java.lang.Character.toString(r3)     // Catch:{ all -> 0x019b }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x019b }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x019b }
            byte r2 = a.b.a(r2)     // Catch:{ all -> 0x019b }
            goto L_0x05d4
        L_0x0ca6:
            byte r3 = r12.v     // Catch:{ all -> 0x019b }
            r4 = 2
            if (r3 != r4) goto L_0x0df3
            byte r3 = r12.y     // Catch:{ all -> 0x019b }
            if (r3 <= r2) goto L_0x0cec
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r12.f98a     // Catch:{ all -> 0x019b }
            char r4 = r12.z     // Catch:{ all -> 0x019b }
            java.lang.String r5 = "AKQ"
            r6 = 2
            boolean r2 = r2.a(r3, r4, r5, r6)     // Catch:{ all -> 0x019b }
            if (r2 == 0) goto L_0x0df3
            byte[] r2 = r12.x     // Catch:{ all -> 0x019b }
            byte r3 = r12.y     // Catch:{ all -> 0x019b }
            byte r2 = r2[r3]     // Catch:{ all -> 0x019b }
            r3 = 6
            if (r2 < r3) goto L_0x0df3
            int r2 = r12.w     // Catch:{ all -> 0x019b }
            r3 = 10
            if (r2 < r3) goto L_0x0df3
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x019b }
            r2.<init>()     // Catch:{ all -> 0x019b }
            java.lang.String r3 = "2"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x019b }
            char r3 = r12.z     // Catch:{ all -> 0x019b }
            java.lang.String r3 = java.lang.Character.toString(r3)     // Catch:{ all -> 0x019b }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x019b }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x019b }
            byte r2 = a.b.a(r2)     // Catch:{ all -> 0x019b }
            goto L_0x05d4
        L_0x0cec:
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r12.f98a     // Catch:{ all -> 0x019b }
            char r4 = r12.z     // Catch:{ all -> 0x019b }
            java.lang.String r5 = "AKQ"
            r6 = 2
            boolean r2 = r2.a(r3, r4, r5, r6)     // Catch:{ all -> 0x019b }
            if (r2 == 0) goto L_0x0df3
            byte[] r2 = r12.x     // Catch:{ all -> 0x019b }
            byte r3 = r12.y     // Catch:{ all -> 0x019b }
            byte r2 = r2[r3]     // Catch:{ all -> 0x019b }
            r3 = 6
            if (r2 < r3) goto L_0x0df3
            int r2 = r12.w     // Catch:{ all -> 0x019b }
            r3 = 13
            if (r2 < r3) goto L_0x0df3
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x019b }
            r2.<init>()     // Catch:{ all -> 0x019b }
            java.lang.String r3 = "3"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x019b }
            char r3 = r12.z     // Catch:{ all -> 0x019b }
            java.lang.String r3 = java.lang.Character.toString(r3)     // Catch:{ all -> 0x019b }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x019b }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x019b }
            byte r2 = a.b.a(r2)     // Catch:{ all -> 0x019b }
            goto L_0x05d4
        L_0x0d29:
            byte[] r2 = r12.x     // Catch:{ all -> 0x019b }
            byte r3 = r12.y     // Catch:{ all -> 0x019b }
            byte r2 = r2[r3]     // Catch:{ all -> 0x019b }
            r3 = 4
            if (r2 > r3) goto L_0x0df3
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r12.f98a     // Catch:{ all -> 0x019b }
            r4 = 1
            boolean r2 = r2.a(r3, r4)     // Catch:{ all -> 0x019b }
            if (r2 == 0) goto L_0x0df3
            int r2 = r12.w     // Catch:{ all -> 0x019b }
            r3 = 12
            if (r2 < r3) goto L_0x0df3
            int r2 = r12.w     // Catch:{ all -> 0x019b }
            r3 = 14
            if (r2 > r3) goto L_0x0df3
            byte r2 = r12.v     // Catch:{ all -> 0x019b }
            r3 = 1
            if (r2 != r3) goto L_0x0df3
            java.lang.String r2 = "1NT"
            byte r2 = a.b.a(r2)     // Catch:{ all -> 0x019b }
            goto L_0x05d4
        L_0x0d56:
            a.b r3 = r12.b     // Catch:{ all -> 0x019b }
            r4 = -4
            byte r3 = r3.b(r4)     // Catch:{ all -> 0x019b }
            byte r3 = a.b.i(r3)     // Catch:{ all -> 0x019b }
            byte r4 = r12.y     // Catch:{ all -> 0x019b }
            if (r4 == r2) goto L_0x0df3
            if (r2 != r3) goto L_0x0df3
            byte[] r3 = r12.x     // Catch:{ all -> 0x019b }
            byte r4 = r12.y     // Catch:{ all -> 0x019b }
            byte r3 = r3[r4]     // Catch:{ all -> 0x019b }
            r4 = 5
            if (r3 < r4) goto L_0x0df3
            byte r3 = r12.v     // Catch:{ all -> 0x019b }
            r4 = 2
            if (r3 != r4) goto L_0x0df3
            byte r3 = r12.y     // Catch:{ all -> 0x019b }
            if (r3 <= r2) goto L_0x0db6
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r12.f98a     // Catch:{ all -> 0x019b }
            char r4 = r12.z     // Catch:{ all -> 0x019b }
            java.lang.String r5 = "AKQ"
            r6 = 2
            boolean r2 = r2.a(r3, r4, r5, r6)     // Catch:{ all -> 0x019b }
            if (r2 == 0) goto L_0x0df3
            byte[] r2 = r12.x     // Catch:{ all -> 0x019b }
            byte r3 = r12.y     // Catch:{ all -> 0x019b }
            byte r2 = r2[r3]     // Catch:{ all -> 0x019b }
            r3 = 5
            if (r2 < r3) goto L_0x0df3
            int r2 = r12.w     // Catch:{ all -> 0x019b }
            r3 = 8
            if (r2 < r3) goto L_0x0df3
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x019b }
            r2.<init>()     // Catch:{ all -> 0x019b }
            java.lang.String r3 = "2"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x019b }
            char r3 = r12.z     // Catch:{ all -> 0x019b }
            java.lang.String r3 = java.lang.Character.toString(r3)     // Catch:{ all -> 0x019b }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x019b }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x019b }
            byte r2 = a.b.a(r2)     // Catch:{ all -> 0x019b }
            goto L_0x05d4
        L_0x0db6:
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r12.f98a     // Catch:{ all -> 0x019b }
            char r4 = r12.z     // Catch:{ all -> 0x019b }
            java.lang.String r5 = "AKQ"
            r6 = 2
            boolean r2 = r2.a(r3, r4, r5, r6)     // Catch:{ all -> 0x019b }
            if (r2 == 0) goto L_0x0df3
            byte[] r2 = r12.x     // Catch:{ all -> 0x019b }
            byte r3 = r12.y     // Catch:{ all -> 0x019b }
            byte r2 = r2[r3]     // Catch:{ all -> 0x019b }
            r3 = 6
            if (r2 < r3) goto L_0x0df3
            int r2 = r12.w     // Catch:{ all -> 0x019b }
            r3 = 12
            if (r2 < r3) goto L_0x0df3
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x019b }
            r2.<init>()     // Catch:{ all -> 0x019b }
            java.lang.String r3 = "3"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x019b }
            char r3 = r12.z     // Catch:{ all -> 0x019b }
            java.lang.String r3 = java.lang.Character.toString(r3)     // Catch:{ all -> 0x019b }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x019b }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x019b }
            byte r2 = a.b.a(r2)     // Catch:{ all -> 0x019b }
            goto L_0x05d4
        L_0x0df3:
            r2 = 99
            goto L_0x05d4
        L_0x0df7:
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r2 = r2.e()     // Catch:{ all -> 0x019b }
            r3 = 2
            if (r2 != r3) goto L_0x10ef
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            r3 = 0
            byte r2 = r2.b(r3)     // Catch:{ all -> 0x019b }
            boolean r2 = a.b.z(r2)     // Catch:{ all -> 0x019b }
            if (r2 == 0) goto L_0x10ef
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            r3 = -1
            byte r2 = r2.b(r3)     // Catch:{ all -> 0x019b }
            r3 = 99
            if (r2 != r3) goto L_0x10ef
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            r3 = -2
            byte r2 = r2.b(r3)     // Catch:{ all -> 0x019b }
            boolean r2 = a.b.z(r2)     // Catch:{ all -> 0x019b }
            if (r2 == 0) goto L_0x10ef
            int r2 = r12.w     // Catch:{ all -> 0x019b }
            r3 = 8
            if (r2 < r3) goto L_0x10eb
            byte r2 = r12.v     // Catch:{ all -> 0x019b }
            r3 = -1
            if (r2 == r3) goto L_0x10eb
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r2 = r2.w()     // Catch:{ all -> 0x019b }
            a.b r3 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r3.w()     // Catch:{ all -> 0x019b }
            byte r3 = a.b.i(r3)     // Catch:{ all -> 0x019b }
            char r4 = a.b.q(r3)     // Catch:{ all -> 0x019b }
            byte r5 = r12.v     // Catch:{ all -> 0x019b }
            r6 = 1
            if (r5 != r6) goto L_0x10dd
            byte[] r2 = r12.x     // Catch:{ all -> 0x019b }
            byte r5 = r12.A     // Catch:{ all -> 0x019b }
            byte r2 = r2[r5]     // Catch:{ all -> 0x019b }
            r5 = 5
            if (r2 < r5) goto L_0x0eea
            boolean r2 = a.b.F(r3)     // Catch:{ all -> 0x019b }
            if (r2 == 0) goto L_0x0ea0
            byte[] r2 = r12.x     // Catch:{ all -> 0x019b }
            r5 = 3
            byte r2 = r2[r5]     // Catch:{ all -> 0x019b }
            r5 = 5
            if (r2 < r5) goto L_0x0eea
            byte[] r2 = r12.x     // Catch:{ all -> 0x019b }
            r5 = 2
            byte r2 = r2[r5]     // Catch:{ all -> 0x019b }
            r5 = 5
            if (r2 < r5) goto L_0x0eea
            int r2 = r12.w     // Catch:{ all -> 0x019b }
            r5 = 16
            if (r2 < r5) goto L_0x0e75
            r2 = 2
            byte r2 = a.b.a(r2, r3)     // Catch:{ all -> 0x019b }
            goto L_0x05d4
        L_0x0e75:
            int r2 = r12.w     // Catch:{ all -> 0x019b }
            r5 = 8
            if (r2 < r5) goto L_0x0eea
            int r2 = r12.w     // Catch:{ all -> 0x019b }
            r5 = 10
            if (r2 > r5) goto L_0x0eea
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r5 = r12.f98a     // Catch:{ all -> 0x019b }
            r6 = 72
            int r2 = r2.b(r5, r6)     // Catch:{ all -> 0x019b }
            a.b r5 = r12.b     // Catch:{ all -> 0x019b }
            byte r6 = r12.f98a     // Catch:{ all -> 0x019b }
            r7 = 83
            int r5 = r5.b(r6, r7)     // Catch:{ all -> 0x019b }
            int r2 = r2 + r5
            r5 = 7
            if (r2 < r5) goto L_0x0eea
            r2 = 2
            byte r2 = a.b.a(r2, r3)     // Catch:{ all -> 0x019b }
            goto L_0x05d4
        L_0x0ea0:
            byte[] r2 = r12.x     // Catch:{ all -> 0x019b }
            byte r2 = r2[r3]     // Catch:{ all -> 0x019b }
            r5 = 5
            if (r2 >= r5) goto L_0x0eea
            byte[] r2 = r12.x     // Catch:{ all -> 0x019b }
            byte r5 = a.b.b(r3)     // Catch:{ all -> 0x019b }
            byte r2 = r2[r5]     // Catch:{ all -> 0x019b }
            r5 = 5
            if (r2 < r5) goto L_0x0eea
            int r2 = r12.w     // Catch:{ all -> 0x019b }
            r5 = 16
            if (r2 < r5) goto L_0x0ebf
            r2 = 2
            byte r2 = a.b.a(r2, r3)     // Catch:{ all -> 0x019b }
            goto L_0x05d4
        L_0x0ebf:
            int r2 = r12.w     // Catch:{ all -> 0x019b }
            r5 = 8
            if (r2 < r5) goto L_0x0eea
            int r2 = r12.w     // Catch:{ all -> 0x019b }
            r5 = 10
            if (r2 > r5) goto L_0x0eea
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r5 = r12.f98a     // Catch:{ all -> 0x019b }
            r6 = 72
            int r2 = r2.b(r5, r6)     // Catch:{ all -> 0x019b }
            a.b r5 = r12.b     // Catch:{ all -> 0x019b }
            byte r6 = r12.f98a     // Catch:{ all -> 0x019b }
            r7 = 83
            int r5 = r5.b(r6, r7)     // Catch:{ all -> 0x019b }
            int r2 = r2 + r5
            r5 = 7
            if (r2 < r5) goto L_0x0eea
            r2 = 2
            byte r2 = a.b.a(r2, r3)     // Catch:{ all -> 0x019b }
            goto L_0x05d4
        L_0x0eea:
            int r2 = r12.w     // Catch:{ all -> 0x019b }
            r5 = 8
            if (r2 < r5) goto L_0x0f9d
            int r2 = r12.w     // Catch:{ all -> 0x019b }
            r5 = 10
            if (r2 > r5) goto L_0x0f9d
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r5 = r12.f98a     // Catch:{ all -> 0x019b }
            char r6 = r12.z     // Catch:{ all -> 0x019b }
            java.lang.String r7 = "AKQJT"
            r8 = 3
            boolean r2 = r2.a(r5, r6, r7, r8)     // Catch:{ all -> 0x019b }
            if (r2 != 0) goto L_0x0f14
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r5 = r12.f98a     // Catch:{ all -> 0x019b }
            char r6 = r12.z     // Catch:{ all -> 0x019b }
            java.lang.String r7 = "AKQ"
            r8 = 2
            boolean r2 = r2.a(r5, r6, r7, r8)     // Catch:{ all -> 0x019b }
            if (r2 == 0) goto L_0x0f9d
        L_0x0f14:
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r5 = r12.f98a     // Catch:{ all -> 0x019b }
            char r6 = r12.z     // Catch:{ all -> 0x019b }
            int r2 = r2.c(r5, r6)     // Catch:{ all -> 0x019b }
            r5 = 6
            if (r2 != r5) goto L_0x0f44
            byte r2 = r12.y     // Catch:{ all -> 0x019b }
            if (r2 <= r3) goto L_0x0f44
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x019b }
            r2.<init>()     // Catch:{ all -> 0x019b }
            java.lang.String r3 = "2"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x019b }
            char r3 = r12.z     // Catch:{ all -> 0x019b }
            java.lang.String r3 = java.lang.Character.toString(r3)     // Catch:{ all -> 0x019b }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x019b }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x019b }
            byte r2 = a.b.a(r2)     // Catch:{ all -> 0x019b }
            goto L_0x05d4
        L_0x0f44:
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r5 = r12.f98a     // Catch:{ all -> 0x019b }
            char r6 = r12.z     // Catch:{ all -> 0x019b }
            int r2 = r2.c(r5, r6)     // Catch:{ all -> 0x019b }
            r5 = 7
            if (r2 != r5) goto L_0x0f70
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x019b }
            r2.<init>()     // Catch:{ all -> 0x019b }
            java.lang.String r3 = "3"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x019b }
            char r3 = r12.z     // Catch:{ all -> 0x019b }
            java.lang.String r3 = java.lang.Character.toString(r3)     // Catch:{ all -> 0x019b }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x019b }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x019b }
            byte r2 = a.b.a(r2)     // Catch:{ all -> 0x019b }
            goto L_0x05d4
        L_0x0f70:
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r5 = r12.f98a     // Catch:{ all -> 0x019b }
            char r6 = r12.z     // Catch:{ all -> 0x019b }
            int r2 = r2.c(r5, r6)     // Catch:{ all -> 0x019b }
            r5 = 8
            if (r2 != r5) goto L_0x0f9d
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x019b }
            r2.<init>()     // Catch:{ all -> 0x019b }
            java.lang.String r3 = "4"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x019b }
            char r3 = r12.z     // Catch:{ all -> 0x019b }
            java.lang.String r3 = java.lang.Character.toString(r3)     // Catch:{ all -> 0x019b }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x019b }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x019b }
            byte r2 = a.b.a(r2)     // Catch:{ all -> 0x019b }
            goto L_0x05d4
        L_0x0f9d:
            int r2 = r12.w     // Catch:{ all -> 0x019b }
            r5 = 12
            if (r2 < r5) goto L_0x0fc2
            byte[] r2 = r12.x     // Catch:{ all -> 0x019b }
            byte r2 = r2[r3]     // Catch:{ all -> 0x019b }
            r5 = 1
            if (r2 > r5) goto L_0x0fc2
            java.lang.String r2 = r12.C     // Catch:{ all -> 0x019b }
            java.lang.String r5 = "5-4-4-0"
            boolean r2 = r2.equals(r5)     // Catch:{ all -> 0x019b }
            if (r2 != 0) goto L_0x0fbe
            java.lang.String r2 = r12.C     // Catch:{ all -> 0x019b }
            java.lang.String r5 = "4-4-4-1"
            boolean r2 = r2.equals(r5)     // Catch:{ all -> 0x019b }
            if (r2 == 0) goto L_0x0fc2
        L_0x0fbe:
            r2 = 97
            goto L_0x05d4
        L_0x0fc2:
            int r2 = r12.w     // Catch:{ all -> 0x019b }
            r5 = 16
            if (r2 < r5) goto L_0x0fd5
            byte[] r2 = r12.x     // Catch:{ all -> 0x019b }
            byte r5 = r12.y     // Catch:{ all -> 0x019b }
            byte r2 = r2[r5]     // Catch:{ all -> 0x019b }
            r5 = 5
            if (r2 < r5) goto L_0x0fd5
            r2 = 97
            goto L_0x05d4
        L_0x0fd5:
            int r2 = r12.w     // Catch:{ all -> 0x019b }
            r5 = 19
            if (r2 < r5) goto L_0x0fea
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r5 = r12.f98a     // Catch:{ all -> 0x019b }
            r6 = 1
            boolean r2 = r2.a(r5, r6)     // Catch:{ all -> 0x019b }
            if (r2 == 0) goto L_0x0fea
            r2 = 97
            goto L_0x05d4
        L_0x0fea:
            byte[] r2 = r12.x     // Catch:{ all -> 0x019b }
            byte r5 = r12.y     // Catch:{ all -> 0x019b }
            byte r2 = r2[r5]     // Catch:{ all -> 0x019b }
            r5 = 5
            if (r2 < r5) goto L_0x10b1
            int r2 = r12.w     // Catch:{ all -> 0x019b }
            r4 = 15
            if (r2 > r4) goto L_0x10eb
            byte r2 = r12.y     // Catch:{ all -> 0x019b }
            if (r2 == r3) goto L_0x10eb
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r4 = r12.f98a     // Catch:{ all -> 0x019b }
            char r5 = r12.z     // Catch:{ all -> 0x019b }
            java.lang.String r6 = "AKQJ"
            r7 = 2
            boolean r2 = r2.a(r4, r5, r6, r7)     // Catch:{ all -> 0x019b }
            if (r2 == 0) goto L_0x1065
            byte r2 = r12.y     // Catch:{ all -> 0x019b }
            if (r2 <= r3) goto L_0x102f
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x019b }
            r2.<init>()     // Catch:{ all -> 0x019b }
            java.lang.String r3 = "1"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x019b }
            char r3 = r12.z     // Catch:{ all -> 0x019b }
            java.lang.String r3 = java.lang.Character.toString(r3)     // Catch:{ all -> 0x019b }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x019b }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x019b }
            byte r2 = a.b.a(r2)     // Catch:{ all -> 0x019b }
            goto L_0x05d4
        L_0x102f:
            byte r2 = r12.y     // Catch:{ all -> 0x019b }
            if (r2 >= r3) goto L_0x1065
            int r2 = r12.w     // Catch:{ all -> 0x019b }
            r3 = 10
            if (r2 >= r3) goto L_0x1046
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r12.f98a     // Catch:{ all -> 0x019b }
            char r4 = r12.z     // Catch:{ all -> 0x019b }
            int r2 = r2.c(r3, r4)     // Catch:{ all -> 0x019b }
            r3 = 5
            if (r2 == r3) goto L_0x10eb
        L_0x1046:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x019b }
            r2.<init>()     // Catch:{ all -> 0x019b }
            java.lang.String r3 = "2"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x019b }
            char r3 = r12.z     // Catch:{ all -> 0x019b }
            java.lang.String r3 = java.lang.Character.toString(r3)     // Catch:{ all -> 0x019b }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x019b }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x019b }
            byte r2 = a.b.a(r2)     // Catch:{ all -> 0x019b }
            goto L_0x05d4
        L_0x1065:
            int r2 = r12.w     // Catch:{ all -> 0x019b }
            r4 = 13
            if (r2 < r4) goto L_0x10eb
            byte r2 = r12.y     // Catch:{ all -> 0x019b }
            if (r2 <= r3) goto L_0x108e
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x019b }
            r2.<init>()     // Catch:{ all -> 0x019b }
            java.lang.String r3 = "1"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x019b }
            char r3 = r12.z     // Catch:{ all -> 0x019b }
            java.lang.String r3 = java.lang.Character.toString(r3)     // Catch:{ all -> 0x019b }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x019b }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x019b }
            byte r2 = a.b.a(r2)     // Catch:{ all -> 0x019b }
            goto L_0x05d4
        L_0x108e:
            byte r2 = r12.y     // Catch:{ all -> 0x019b }
            if (r2 >= r3) goto L_0x10eb
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x019b }
            r2.<init>()     // Catch:{ all -> 0x019b }
            java.lang.String r3 = "2"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x019b }
            char r3 = r12.z     // Catch:{ all -> 0x019b }
            java.lang.String r3 = java.lang.Character.toString(r3)     // Catch:{ all -> 0x019b }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x019b }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x019b }
            byte r2 = a.b.a(r2)     // Catch:{ all -> 0x019b }
            goto L_0x05d4
        L_0x10b1:
            int r2 = r12.w     // Catch:{ all -> 0x019b }
            r3 = 15
            if (r2 < r3) goto L_0x10eb
            int r2 = r12.w     // Catch:{ all -> 0x019b }
            r3 = 18
            if (r2 > r3) goto L_0x10eb
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r12.f98a     // Catch:{ all -> 0x019b }
            r5 = 1
            boolean r2 = r2.a(r3, r5)     // Catch:{ all -> 0x019b }
            if (r2 == 0) goto L_0x10eb
            a.b r2 = r12.b     // Catch:{ all -> 0x019b }
            byte r3 = r12.f98a     // Catch:{ all -> 0x019b }
            java.lang.String r5 = "AK"
            r6 = 1
            boolean r2 = r2.a(r3, r4, r5, r6)     // Catch:{ all -> 0x019b }
            if (r2 == 0) goto L_0x10eb
            java.lang.String r2 = "1NT"
            byte r2 = a.b.a(r2)     // Catch:{ all -> 0x019b }
            goto L_0x05d4
        L_0x10dd:
            java.lang.String r3 = "1NT"
            byte r3 = a.b.a(r3)     // Catch:{ all -> 0x019b }
            if (r2 != r3) goto L_0x10eb
            byte r2 = r12.e()     // Catch:{ all -> 0x019b }
            goto L_0x05d4
        L_0x10eb:
            r2 = 99
            goto L_0x05d4
        L_0x10ef:
            byte r2 = r12.d()     // Catch:{ all -> 0x019b }
            goto L_0x05d4
        L_0x10f5:
            r2 = 0
            goto L_0x05dc
        L_0x10f8:
            int r1 = r12.O     // Catch:{ all -> 0x019b }
            r2 = 1002(0x3ea, float:1.404E-42)
            if (r1 != r2) goto L_0x1103
            r12.h()     // Catch:{ all -> 0x019b }
            goto L_0x05ea
        L_0x1103:
            int r1 = r12.O     // Catch:{ all -> 0x019b }
            r2 = 1003(0x3eb, float:1.406E-42)
            if (r1 != r2) goto L_0x111d
            int r1 = net.weweweb.android.bridge.BridgeApp.q     // Catch:{ all -> 0x019b }
            a.g.a(r1)     // Catch:{ all -> 0x019b }
            android.os.Message r1 = android.os.Message.obtain()     // Catch:{ all -> 0x019b }
            r2 = 1003(0x3eb, float:1.406E-42)
            r1.what = r2     // Catch:{ all -> 0x019b }
            android.os.Handler r2 = r12.P     // Catch:{ all -> 0x019b }
            r2.sendMessage(r1)     // Catch:{ all -> 0x019b }
            goto L_0x05ea
        L_0x111d:
            int r1 = r12.O     // Catch:{ all -> 0x019b }
            r2 = 1004(0x3ec, float:1.407E-42)
            if (r1 != r2) goto L_0x05ea
            byte r1 = r12.s     // Catch:{ all -> 0x019b }
            r12.g(r1)     // Catch:{ all -> 0x019b }
            goto L_0x05ea
        */
        throw new UnsupportedOperationException("Method not decompiled: net.weweweb.android.bridge.y.run():void");
    }

    private byte c(int i2) {
        this.n = 0;
        this.l = System.currentTimeMillis();
        byte[] bArr = new byte[(this.i.g() - this.i.C())];
        byte a2 = a(this.i, bArr);
        this.j = this.i.j();
        int i3 = 0;
        byte b2 = -1;
        int i4 = -13;
        while (true) {
            if (i3 < a2) {
                if (a2 == 1 && i2 != 1) {
                    b2 = bArr[0];
                    break;
                }
                i(bArr[i3]);
                int a3 = a(i4, 13);
                this.i.S();
                if (a3 > i4) {
                    int i5 = this.i.m + 1;
                    if (a3 > this.q[i5]) {
                        this.p[i5] = this.o[i5];
                        this.o[i5] = bArr[i3];
                        this.q[i5] = a3;
                    }
                    b2 = bArr[i3];
                    i4 = a3;
                }
                i3++;
            } else {
                break;
            }
        }
        this.m = System.currentTimeMillis();
        return i2 == 0 ? b2 : (byte) i4;
    }

    private void a(byte b2, boolean z2) {
        Message obtain = Message.obtain();
        obtain.what = 1004;
        obtain.arg1 = b2;
        obtain.arg2 = z2 ? 1 : 0;
        this.P.sendMessage(obtain);
        this.O = 1000;
    }

    private void b(int i2, int i3) {
        if (i2 >= 0 && i2 <= this.Q.length) {
            this.Q[i2] = i3;
            byte p2 = this.b.p((byte) i2);
            int a2 = a(i2);
            if (a2 != R) {
                int i4 = 0;
                while (true) {
                    if (i4 < aM.length) {
                        if (aM[i4][0] == a2 && b.c(this.b.d(i2)) == aM[i4][1]) {
                            a(p2, aM[i4][2], aM[i4][3]);
                            break;
                        }
                        i4++;
                    } else {
                        int i5 = 0;
                        while (true) {
                            if (i5 >= aL.length) {
                                break;
                            } else if (aL[i5][0] == a2) {
                                a(p2, aL[i5][1], aL[i5][2]);
                                break;
                            } else {
                                i5++;
                            }
                        }
                    }
                }
            }
            byte p3 = this.b.p((byte) i2);
            int a3 = a(i2);
            if (a3 != R) {
                byte d2 = this.b.d(i2);
                for (int i6 = 0; i6 < aN.length; i6++) {
                    if (aN[i6][0] == a3 && aN[i6][1] == d2) {
                        for (byte b2 = 0; b2 < 4; b2 = (byte) (b2 + 1)) {
                            int i7 = aN[i6][(b2 * 2) + 2];
                            int i8 = aN[i6][(b2 * 2) + 2 + 1];
                            if (p3 >= 0 && p3 <= 3) {
                                if (i7 >= 0 && i7 > this.D[p3][b2]) {
                                    this.D[p3][b2] = i7;
                                }
                                if (i8 >= 0 && i8 < this.E[p3][b2]) {
                                    this.E[p3][b2] = i8;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public final void a(b bVar) {
        this.b = bVar;
    }

    public final void a(byte b2) {
        this.f98a = b2;
    }

    private void a(byte b2, int i2, int i3) {
        if (b2 >= 0 && b2 <= 3) {
            if (i2 >= 0 && i2 > this.F[b2]) {
                this.F[b2] = i2;
            }
            if (i3 >= 0 && i3 < this.G[b2]) {
                this.G[b2] = i3;
            }
        }
    }

    private void b(b bVar) {
        this.b.a(bVar);
        g.b(this.q, -13);
        g.b(this.r, 13);
    }

    private void i(byte b2) {
        this.i.N(b2);
    }

    private static void c(b bVar) {
        int a2;
        byte j2 = bVar.j();
        byte B2 = bVar.B();
        int[] iArr = new int[4];
        for (byte b2 = 0; b2 < 4; b2 = (byte) (b2 + 1)) {
            if (bVar.j(j2, b2)) {
                iArr[b2] = iArr[b2] - 999999;
            } else {
                String a3 = b.a(bVar.l[j2], b2, 11);
                if (aO.get(a3) != null) {
                    iArr[b2] = ((Integer) aO.get(a3)).intValue() + iArr[b2];
                }
                int c2 = bVar.c(j2, b.q(b2));
                float f2 = bVar.f((byte) (j2 % 2), b2);
                if (!bVar.M()) {
                    if (c2 == 2 && d.d[bVar.c(j2, b2)] < 8 && b2 != B2) {
                        iArr[b2] = iArr[b2] + 1;
                    }
                    if (c2 == 1 && d.d[bVar.c(j2, b2)] < 8 && b2 != B2) {
                        iArr[b2] = iArr[b2] + 2;
                    }
                } else if (c2 >= 3) {
                    if (f2 > 0.9f) {
                        iArr[b2] = iArr[b2] - 15;
                    } else if (f2 > 0.8f) {
                        iArr[b2] = iArr[b2] - 10;
                    } else if (f2 > 0.7f) {
                        iArr[b2] = iArr[b2] - 5;
                    }
                }
                if (c2 >= 3) {
                    if (f2 < 0.1f) {
                        iArr[b2] = iArr[b2] + 10;
                    } else if (f2 < 0.2f) {
                        iArr[b2] = iArr[b2] + 6;
                    } else if (f2 < 0.3f) {
                        iArr[b2] = iArr[b2] + 3;
                    }
                }
            }
        }
        if (B2 != 4) {
            iArr[B2] = iArr[B2] - 20;
        }
        if (iArr[0] == 0 && iArr[1] == 0 && iArr[2] == 0 && iArr[3] == 0) {
            a2 = bVar.a(j2, 1);
        } else {
            a2 = g.a(iArr);
        }
        bVar.N(bVar.d(j2, (byte) a2));
    }

    public final void a(int i2, Handler handler) {
        synchronized ("") {
            this.O = i2;
            this.P = handler;
            new Thread(this).start();
        }
    }
}
