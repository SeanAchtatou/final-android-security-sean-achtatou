package net.weweweb.android.bridge;

import a.b;
import android.app.Dialog;
import android.content.Context;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TableRow;
import android.widget.TextView;
import net.weweweb.android.free.bridge.R;

/* compiled from: ProGuard */
final class a extends Dialog {

    /* renamed from: a  reason: collision with root package name */
    Context f28a = null;
    ad b = null;
    b c = null;
    i d = null;

    a(Context context) {
        super(context);
        setContentView((int) R.layout.biddialog);
        setTitle((int) R.string.bid_dialog_title);
        ((Button) findViewById(R.id.bidDialogButtonOK)).setOnClickListener(new b(this));
        this.d = new i(getContext(), (TableRow) findViewById(R.id.bidDialogCellsHeader), (ScrollView) findViewById(R.id.bidDialogCellScrollView));
    }

    private void a() {
        TextView textView = (TextView) findViewById(R.id.bidDialogBoardInfo);
        if (textView != null) {
            textView.setText("Dealer: " + b.g[this.c.h.a()] + "    " + "Vul: " + a.a.g[this.c.h.d()]);
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(b bVar) {
        this.c = bVar;
        this.d.a(bVar);
        a();
        this.d.b();
    }

    /* access modifiers changed from: package-private */
    public final void a(ad adVar) {
        this.b = adVar;
        this.c = adVar.b;
        this.d.a(adVar);
        a();
        this.d.b();
    }
}
