package net.weweweb.android.bridge;

import a.b;
import a.c;
import a.g;
import java.util.LinkedList;

/* compiled from: ProGuard */
public final class bj {

    /* renamed from: a  reason: collision with root package name */
    int f64a;
    byte b;
    boolean c;
    int[] d;
    int[] e;
    int[] f;
    int[] g;
    int h;
    int i = 0;
    LinkedList j;

    /* access modifiers changed from: package-private */
    public final void a() {
        this.b = 2;
        if (this.g == null) {
            this.g = new int[2];
        }
        g.b(this.g, 0);
    }

    /* access modifiers changed from: package-private */
    public final void b() {
        this.b = 1;
        if (this.d == null) {
            this.d = new int[2];
        }
        g.b(this.d, 0);
        if (this.e == null) {
            this.e = new int[2];
        }
        g.b(this.e, 0);
        if (this.f == null) {
            this.f = new int[2];
        }
        g.b(this.f, 0);
        this.c = false;
    }

    /* access modifiers changed from: package-private */
    public final int a(byte b2) {
        if (d()) {
            if (this.e[b2 % 2] < this.e[(b2 + 1) % 2]) {
                return 0;
            }
            if (this.e[(b2 + 1) % 2] == 0) {
                return 700;
            }
            if (this.e[(b2 + 1) % 2] == 1) {
                return 500;
            }
        }
        return 0;
    }

    /* access modifiers changed from: package-private */
    public final boolean c() {
        if (this.j == null) {
            return true;
        }
        if (this.j.size() == 0) {
            return false;
        }
        if (((c) this.j.getLast()).c() == 99) {
            return false;
        }
        return this.j.size() >= 4;
    }

    /* access modifiers changed from: package-private */
    public final boolean d() {
        return this.e[0] >= 2 || this.e[1] >= 2;
    }

    /* access modifiers changed from: package-private */
    public final void e() {
        g.b(this.g, 0);
        for (int i2 = 0; i2 < this.j.size(); i2++) {
            c cVar = (c) this.j.get(i2);
            if (cVar.c() != 99) {
                int a2 = b.a(cVar.c(), cVar.d(), cVar.h());
                int c2 = b.c(b.l(cVar.i(), cVar.g()), cVar.c(), cVar.d(), cVar.h());
                int a3 = b.a(b.l(cVar.i(), cVar.g()), cVar.c(), cVar.d(), cVar.h());
                int a4 = b.a(b.l(cVar.i(), cVar.g()), cVar.c(), cVar.h());
                int d2 = b.d(b.l(cVar.i(), cVar.g()), cVar.c(), cVar.d(), cVar.h());
                if (a2 > 0) {
                    int[] iArr = this.g;
                    int g2 = cVar.g() % 2;
                    iArr[g2] = a2 + c2 + a3 + a4 + iArr[g2];
                } else {
                    int[] iArr2 = this.g;
                    int g3 = (cVar.g() + 1) % 2;
                    iArr2[g3] = iArr2[g3] - d2;
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void f() {
        g.b(this.d, 0);
        g.b(this.e, 0);
        g.b(this.f, 0);
        for (int i2 = 0; i2 < this.j.size(); i2++) {
            c cVar = (c) this.j.get(i2);
            if (cVar.c() != 99) {
                this.c = false;
                int a2 = b.a(cVar.c(), cVar.d(), cVar.h());
                int c2 = b.c(b.l(cVar.i(), cVar.g()), cVar.c(), cVar.d(), cVar.h());
                int a3 = b.a(b.l(cVar.i(), cVar.g()), cVar.c(), cVar.h());
                int d2 = b.d(b.l(cVar.i(), cVar.g()), cVar.c(), cVar.d(), cVar.h());
                if (a2 > 0) {
                    int[] iArr = this.f;
                    int g2 = cVar.g() % 2;
                    iArr[g2] = c2 + a2 + a3 + iArr[g2];
                } else {
                    int[] iArr2 = this.f;
                    int g3 = (cVar.g() + 1) % 2;
                    iArr2[g3] = iArr2[g3] - d2;
                }
                if (a2 > 0) {
                    int[] iArr3 = this.d;
                    int g4 = cVar.g() % 2;
                    iArr3[g4] = a2 + iArr3[g4];
                    if (this.d[0] >= 100 || this.d[1] >= 100) {
                        if (this.d[0] >= 100) {
                            int[] iArr4 = this.e;
                            iArr4[0] = iArr4[0] + 1;
                        }
                        if (this.d[1] >= 100) {
                            int[] iArr5 = this.e;
                            iArr5[1] = iArr5[1] + 1;
                        }
                        g.b(this.d, 0);
                        this.c = true;
                    }
                    if (d()) {
                        int[] iArr6 = this.f;
                        iArr6[0] = iArr6[0] + a((byte) 0);
                        int[] iArr7 = this.f;
                        iArr7[1] = iArr7[1] + a((byte) 1);
                        return;
                    }
                } else {
                    continue;
                }
            }
        }
    }
}
