package net.weweweb.android.bridge;

import a.b;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

/* compiled from: ProGuard */
final class m {

    /* renamed from: a  reason: collision with root package name */
    private final Context f89a;
    private n b;
    private SQLiteDatabase c;

    public m(Context context) {
        this.f89a = context;
        this.b = new n(context);
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        this.c.execSQL("DELETE FROM bridge_bid_rule WHERE sys_id = 0");
    }

    public final void b() {
        this.b.close();
    }

    public final String[] a(b bVar, byte b2, String str) {
        String str2;
        o oVar = new o(bVar);
        String[] strArr = new String[4];
        strArr[3] = "Rule not found.";
        if (str == null || str.length() == 0) {
            str2 = "0";
        } else {
            str2 = str;
        }
        Cursor rawQuery = this.c.rawQuery("SELECT * FROM BRIDGE_BID_RULE WHERE sys_id = " + 0 + " AND bid_i = '" + str2 + "' ORDER BY priority DESC", null);
        while (true) {
            if (!rawQuery.moveToNext()) {
                break;
            }
            String string = rawQuery.getString(rawQuery.getColumnIndex("rules"));
            String string2 = rawQuery.getString(rawQuery.getColumnIndex("bid_o"));
            try {
                if (oVar.a(b2, string)) {
                    String string3 = rawQuery.getString(rawQuery.getColumnIndex("alert"));
                    String string4 = rawQuery.getString(rawQuery.getColumnIndex("alert_des"));
                    if (string4 != null && string4.equals("null")) {
                        string4 = null;
                    }
                    strArr[0] = string2;
                    strArr[1] = string3;
                    strArr[2] = string4;
                    strArr[3] = string;
                }
            } catch (Exception e) {
                e.printStackTrace();
                strArr[3] = e.toString();
            }
        }
        rawQuery.close();
        if (strArr[0] == null && str2.startsWith("P")) {
            StringBuilder sb = new StringBuilder(str2);
            while (sb.toString().startsWith("P")) {
                sb.deleteCharAt(0);
            }
            if (sb.length() == 0) {
                sb.append("0");
            }
            String sb2 = sb.toString();
            Cursor rawQuery2 = this.c.rawQuery("SELECT * FROM BRIDGE_BID_RULE WHERE sys_id = " + 0 + " AND bid_i = '" + sb2 + "' ORDER BY priority DESC", null);
            while (true) {
                if (!rawQuery2.moveToNext()) {
                    break;
                }
                String string5 = rawQuery2.getString(rawQuery2.getColumnIndex("rules"));
                String string6 = rawQuery2.getString(rawQuery2.getColumnIndex("bid_o"));
                try {
                    if (oVar.a(b2, string5)) {
                        Log.i("getDBBid", "Seat:" + ((int) b2) + ", bid_i=" + sb2 + ", bid_o=" + string6 + ", Rule:" + string5 + " fired.");
                        String string7 = rawQuery2.getString(rawQuery2.getColumnIndex("alert"));
                        String string8 = rawQuery2.getString(rawQuery2.getColumnIndex("alert_des"));
                        if (string8 != null && string8.equals("null")) {
                            string8 = null;
                        }
                        strArr[0] = string6;
                        strArr[1] = string7;
                        strArr[2] = string8;
                        strArr[3] = string5;
                    }
                } catch (Exception e2) {
                    Log.w("getDBBid", "Invalid bridge bid rule: " + string5 + " with bid_i: " + sb2 + " & bid_o:" + string6);
                    e2.printStackTrace();
                    strArr[3] = e2.toString();
                }
            }
            rawQuery2.close();
        }
        return strArr;
    }

    /* access modifiers changed from: package-private */
    public final Timestamp c() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        Cursor rawQuery = this.c.rawQuery("SELECT max(last_update) FROM BRIDGE_BID_RULE WHERE sys_id = 0", null);
        Timestamp timestamp = null;
        while (rawQuery.moveToNext()) {
            try {
                String string = rawQuery.getString(0);
                if (string.length() == 21) {
                    string = string + "00";
                } else if (string.length() == 22) {
                    string = string + "0";
                }
                timestamp = new Timestamp(simpleDateFormat.parse(string).getTime());
            } catch (Exception e) {
            }
        }
        rawQuery.close();
        return timestamp;
    }

    /* access modifiers changed from: package-private */
    public final long d() {
        try {
            return DatabaseUtils.queryNumEntries(this.c, "bridge_bid_rule");
        } catch (Exception e) {
            return 0;
        }
    }

    public final m e() {
        this.c = this.b.getWritableDatabase();
        return this;
    }

    /* access modifiers changed from: package-private */
    public final void a(int i, String str, String str2, String str3, short s, String str4, String str5, Timestamp timestamp) {
        SQLiteStatement compileStatement = this.c.compileStatement("REPLACE INTO bridge_bid_rule (sys_id, bid_i, bid_o, rules, priority, alert, alert_des, last_update) VALUES(?, ?, ?, ?, ?, ?, ?, ?)");
        compileStatement.bindLong(1, (long) i);
        compileStatement.bindString(2, str);
        compileStatement.bindString(3, str2);
        compileStatement.bindString(4, str3);
        compileStatement.bindLong(5, (long) s);
        compileStatement.bindString(6, str4);
        compileStatement.bindString(7, str5);
        compileStatement.bindString(8, timestamp.toString());
        try {
            compileStatement.execute();
        } catch (SQLException e) {
            Log.e("BridgeBidRuleDBAdapter", e.toString());
        }
        try {
            compileStatement.close();
        } catch (Exception e2) {
            Log.e("BridgeBidRuleDBAdapter", e2.toString());
        }
    }
}
