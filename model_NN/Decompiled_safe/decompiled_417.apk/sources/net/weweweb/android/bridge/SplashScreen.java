package net.weweweb.android.bridge;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.widget.TextView;
import java.util.Timer;
import java.util.TimerTask;
import net.weweweb.android.free.bridge.R;

/* compiled from: ProGuard */
public class SplashScreen extends Activity {

    /* renamed from: a  reason: collision with root package name */
    Timer f25a;
    long b;
    boolean c = true;
    boolean d = false;
    private final TimerTask e = new br(this);
    /* access modifiers changed from: private */
    public final Handler f = new bs(this);

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        switch (configuration.orientation) {
            case 1:
                this.c = false;
                return;
            case 2:
                this.c = false;
                return;
            default:
                return;
        }
    }

    public void onCreate(Bundle bundle) {
        int i;
        super.onCreate(bundle);
        setContentView((int) R.layout.splashscreen);
        ((TextView) findViewById(R.id.txtSplashScreenVersion)).setText("Version: " + q.b);
        String[] split = BridgeApp.C.split(",");
        try {
            i = Integer.parseInt(split[1]) + 1;
        } catch (Exception e2) {
            i = 1;
        }
        ((TextView) findViewById(R.id.txtSplashScreenExpiry)).setText("Expiry Date: 20" + split[0] + "-" + i + "-" + split[2]);
        this.f25a = new Timer(true);
        this.b = System.currentTimeMillis();
        this.f25a.schedule(this.e, 0, 100);
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4) {
            return super.onKeyDown(i, keyEvent);
        }
        this.d = true;
        this.c = false;
        return true;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() != 0) {
            return true;
        }
        this.c = false;
        return true;
    }
}
