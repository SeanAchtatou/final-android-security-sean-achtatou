package net.weweweb.android.bridge;

import a.b;
import a.h;
import android.content.DialogInterface;

/* compiled from: ProGuard */
final class bv implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TableListActivity f75a;

    bv(TableListActivity tableListActivity) {
        this.f75a = tableListActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        int a2;
        byte b;
        if (i != 0 && (a2 = TableListActivity.b(this.f75a.i)) != -1) {
            String str = this.f75a.h[i];
            if (this.f75a.f26a.k.d.i.f == null && this.f75a.f26a.k.d.j[a2] != null) {
                synchronized (this.f75a.f26a.k.d.j[a2]) {
                    if (str.startsWith("Join")) {
                        this.f75a.j = 3;
                        b = 0;
                    } else if (str.startsWith("Watch")) {
                        this.f75a.j = 4;
                        b = 1;
                    } else {
                        this.f75a.j = 0;
                    }
                    byte b2 = 0;
                    while (true) {
                        if (b2 >= b.g.length) {
                            break;
                        } else if (str.contains(" " + b.g[b2])) {
                            this.f75a.k = a2;
                            this.f75a.f26a.k.d.a(h.a((byte) a2, b, (byte) 1, b2));
                            this.f75a.a();
                            break;
                        } else {
                            b2 = (byte) (b2 + 1);
                        }
                    }
                }
            }
        }
        dialogInterface.dismiss();
    }
}
