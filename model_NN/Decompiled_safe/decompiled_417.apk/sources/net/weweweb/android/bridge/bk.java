package net.weweweb.android.bridge;

import android.view.View;
import android.widget.AdapterView;

/* compiled from: ProGuard */
final class bk implements AdapterView.OnItemSelectedListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SoloGameSettingsActivity f65a;

    bk(SoloGameSettingsActivity soloGameSettingsActivity) {
        this.f65a = soloGameSettingsActivity;
    }

    public final void onItemSelected(AdapterView adapterView, View view, int i, long j) {
        if (i == 0) {
            BridgeApp.n = false;
        } else {
            BridgeApp.n = true;
        }
        this.f65a.f24a.a("soloRobotDeclarer", BridgeApp.n);
    }

    public final void onNothingSelected(AdapterView adapterView) {
    }
}
