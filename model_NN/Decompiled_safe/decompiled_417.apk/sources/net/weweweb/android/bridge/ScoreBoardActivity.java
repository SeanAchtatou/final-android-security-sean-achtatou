package net.weweweb.android.bridge;

import a.b;
import a.c;
import a.g;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import java.util.HashMap;
import net.weweweb.android.free.bridge.R;

/* compiled from: ProGuard */
public class ScoreBoardActivity extends Activity implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    BridgeApp f21a;
    BridgeGameActivity b;
    ScrollView c = null;
    TableLayout d = null;
    int e = 0;
    HashMap f = new HashMap();

    /* access modifiers changed from: package-private */
    public final void a(c cVar) {
        int i;
        if (cVar != null) {
            TableLayout tableLayout = (TableLayout) findViewById(R.id.scoreTableLayout);
            TableRow tableRow = new TableRow(this);
            TableRow.LayoutParams layoutParams = new TableRow.LayoutParams();
            layoutParams.setMargins(0, 0, 0, 1);
            for (int i2 = 0; i2 < 3; i2++) {
                TextView textView = new TextView(this);
                textView.setPadding(0, 1, 0, 1);
                if (cVar.f2a % 2 == 0) {
                    textView.setBackgroundColor(Color.rgb(255, 255, 128));
                } else {
                    textView.setBackgroundColor(-1);
                }
                textView.setTextColor(-16777216);
                textView.setGravity(17);
                tableRow.addView(textView, layoutParams);
                if (i2 == 0) {
                    SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
                    spannableStringBuilder.append((CharSequence) cVar.a(true));
                    int indexOf = spannableStringBuilder.toString().indexOf(b.e[1]);
                    if (indexOf >= 0) {
                        spannableStringBuilder.setSpan(new ForegroundColorSpan(-65536), indexOf, indexOf + 1, 33);
                    } else {
                        int indexOf2 = spannableStringBuilder.toString().indexOf(b.e[2]);
                        if (indexOf2 >= 0) {
                            spannableStringBuilder.setSpan(new ForegroundColorSpan(-65536), indexOf2, indexOf2 + 1, 33);
                        }
                    }
                    textView.setText(spannableStringBuilder);
                } else if (i2 == 1) {
                    if (cVar.c() != 99) {
                        int b2 = b.b(b.l(cVar.i(), cVar.g()), cVar.c(), cVar.d(), cVar.h());
                        if (!b.i(this.f21a.m.e, cVar.g())) {
                            b2 *= -1;
                        }
                        textView.setText(Integer.toString(b2));
                        i = b2;
                    } else {
                        textView.setText("0");
                        i = 0;
                    }
                    this.e = i + this.e;
                } else if (i2 == 2) {
                    textView.setText("View");
                    textView.setFocusable(true);
                    this.f.put(textView, cVar);
                    textView.setOnClickListener(this);
                }
            }
            tableLayout.addView(tableRow, tableLayout.getChildCount() - 1);
            ((TextView) ((TableRow) tableLayout.getChildAt(tableLayout.getChildCount() - 1)).getChildAt(1)).setText(Integer.toString(this.e));
            this.c.fullScroll(130);
        }
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        if (this.f21a.j != null) {
            this.f21a.j.a();
        }
        if (this.f21a.g == this) {
            this.f21a.g = null;
        }
        finish();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f21a = (BridgeApp) getApplicationContext();
        this.b = this.f21a.f;
        setContentView((int) R.layout.scoreboard);
        this.f21a.g = this;
        this.c = (ScrollView) findViewById(R.id.scoreScrollView);
        this.e = 0;
        for (int i = 0; i < this.f21a.m.i.size(); i++) {
            a((c) this.f21a.m.i.get(i));
        }
        this.c.post(new bc(this));
        ((Button) findViewById(R.id.scoreBoardButtonClose)).setOnClickListener(this);
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4) {
            return super.onKeyDown(i, keyEvent);
        }
        a();
        return true;
    }

    public void onClick(View view) {
        if (view.equals(findViewById(R.id.scoreBoardButtonClose))) {
            a();
        } else if (this.f.containsKey(view)) {
            c cVar = (c) this.f.get(view);
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse(g.a(getApplicationContext().getString(R.string.dupresult_url), Integer.toString(cVar.f()), Integer.toString(cVar.e())))));
        }
    }
}
