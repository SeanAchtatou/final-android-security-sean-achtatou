package net.weweweb.android.bridge;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;

/* compiled from: ProGuard */
final class bs extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SplashScreen f72a;

    bs(SplashScreen splashScreen) {
        this.f72a = splashScreen;
    }

    public final void handleMessage(Message message) {
        switch (message.what) {
            case 0:
                this.f72a.finish();
                Intent intent = new Intent();
                intent.setClass(this.f72a, MainActivity.class);
                this.f72a.startActivity(intent);
                break;
            case 1:
                this.f72a.finish();
                break;
        }
        super.handleMessage(message);
    }
}
