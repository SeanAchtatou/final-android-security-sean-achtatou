package net.weweweb.android.bridge;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import net.weweweb.android.common.a;
import net.weweweb.android.free.bridge.R;

/* compiled from: ProGuard */
public class MainActivity extends Activity {

    /* renamed from: a  reason: collision with root package name */
    BridgeApp f16a;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f16a = (BridgeApp) getApplicationContext();
        BridgeApp.H = getResources().getDisplayMetrics().densityDpi;
        SharedPreferences sharedPreferences = getSharedPreferences("PrefFile", 0);
        BridgeApp.n = sharedPreferences.getBoolean("soloRobotDeclarer", false);
        BridgeApp.o = (byte) sharedPreferences.getInt("soloGameScoreMethod", 0);
        BridgeApp.p = sharedPreferences.getBoolean("soloAutoSave", false);
        BridgeApp.q = sharedPreferences.getInt("endTurnDelay", 2000);
        BridgeApp.r = sharedPreferences.getInt("autoPurgeKeep", 0);
        BridgeApp.t = sharedPreferences.getBoolean("viewerDoubleDummy", true);
        BridgeApp.v = sharedPreferences.getInt("playEngine", 0);
        BridgeApp.w = sharedPreferences.getBoolean("luckyHand", false);
        BridgeApp.x = sharedPreferences.getBoolean("soundOn", true);
        BridgeApp.y = sharedPreferences.getBoolean("confirmContract", true);
        BridgeApp.B = sharedPreferences.getBoolean("soloGameAutoPlaySingleCard", true);
        BridgeApp.D = sharedPreferences.getString("loginName", null);
        BridgeApp.E = sharedPreferences.getBoolean("savePasswordFlag", true);
        BridgeApp.F = sharedPreferences.getString("savePassword", null);
        setTitle(((Object) getTitle()) + " Ver. " + q.b);
        if (BridgeApp.H <= 0) {
            BridgeApp.I = 1.0f;
        } else {
            BridgeApp.I = (float) (160 / BridgeApp.H);
        }
        if (BridgeApp.H <= 130) {
            a.r = 54;
            a.q = 40;
            a.a(45);
        } else if (BridgeApp.H <= 200) {
            a.r = 72;
            a.q = 54;
            a.a(60);
        } else {
            a.r = 108;
            a.q = 81;
            a.a(90);
        }
        setContentView((int) R.layout.mainmenu);
        this.f16a.b = this;
        ListView listView = (ListView) findViewById(R.id.mainMenu);
        ArrayList arrayList = new ArrayList();
        HashMap hashMap = new HashMap();
        hashMap.put("icon", Integer.valueOf((int) R.drawable.ic_menu_contact));
        hashMap.put("title", "Solo Game");
        hashMap.put("desc", "Play bridge game by oneself offline.");
        arrayList.add(hashMap);
        HashMap hashMap2 = new HashMap();
        hashMap2.put("icon", Integer.valueOf((int) R.drawable.ic_menu_allfriends));
        hashMap2.put("title", "Online Game");
        hashMap2.put("desc", "Play bridge game with others by connecting to WeWeWeb.NET.");
        arrayList.add(hashMap2);
        HashMap hashMap3 = new HashMap();
        hashMap3.put("icon", Integer.valueOf((int) R.drawable.books));
        hashMap3.put("title", "References");
        hashMap3.put("desc", "Useful materials for the game.");
        arrayList.add(hashMap3);
        HashMap hashMap4 = new HashMap();
        hashMap4.put("icon", Integer.valueOf((int) R.drawable.ic_menu_preferences));
        hashMap4.put("title", "Tools");
        hashMap4.put("desc", "Tools and utilities for the game");
        arrayList.add(hashMap4);
        HashMap hashMap5 = new HashMap();
        hashMap5.put("icon", Integer.valueOf((int) R.drawable.ic_menu_logout));
        hashMap5.put("title", "Exit Application");
        hashMap5.put("desc", "Completely quit the application.");
        arrayList.add(hashMap5);
        listView.setAdapter((ListAdapter) new SimpleAdapter(this, arrayList, R.layout.mainmenuitem, new String[]{"icon", "title", "desc"}, new int[]{R.id.mainMenuItemImage, R.id.mainMenuItemTitle, R.id.mainMenuItemDesc}));
        listView.setOnItemClickListener(new ag(this));
        Calendar instance = Calendar.getInstance();
        String[] split = BridgeApp.C.split(",");
        try {
            instance.set(Integer.parseInt(split[0]) + 2000, Integer.parseInt(split[1]), Integer.parseInt(split[2]));
        } catch (Exception e) {
        }
        if (Calendar.getInstance().getTimeInMillis() > instance.getTimeInMillis()) {
            new AlertDialog.Builder(this).setIcon(17301543).setTitle("Confirm Action").setMessage("Program version expried, please use an up-to-date version.").setPositiveButton("OK", new af(this)).show();
        }
    }
}
