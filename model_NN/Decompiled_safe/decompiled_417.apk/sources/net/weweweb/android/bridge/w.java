package net.weweweb.android.bridge;

import a.a;
import a.c;
import a.g;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;
import java.lang.reflect.Array;
import java.sql.Timestamp;
import java.util.LinkedList;
import net.weweweb.android.common.b;

/* compiled from: ProGuard */
public final class w {

    /* renamed from: a  reason: collision with root package name */
    private final Context f97a;
    private x b;
    private SQLiteDatabase c;
    private final StringBuilder[] d = new StringBuilder[4];
    private final StringBuilder e = new StringBuilder();

    public w(Context context) {
        this.f97a = context;
        this.b = new x(context);
        for (int i = 0; i < this.d.length; i++) {
            this.d[i] = new StringBuilder();
        }
    }

    public final void a() {
        if (this.b != null) {
            this.b.close();
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(int i) {
        if (i > 0) {
            int[] iArr = new int[200];
            Cursor rawQuery = this.c.rawQuery("SELECT deck_no FROM bridge_deck WHERE deck_set = " + Integer.toString(-1000), null);
            int i2 = 0;
            while (i2 < iArr.length && rawQuery.moveToNext()) {
                iArr[i2] = rawQuery.getInt(rawQuery.getColumnIndex("deck_no"));
                i2++;
            }
            rawQuery.close();
            if (i2 >= i) {
                g.c(iArr, i2);
                for (int i3 = 0; i3 < i; i3++) {
                    int i4 = iArr[i3];
                    this.c.execSQL("DROP TABLE IF EXISTS t_bridge_deck");
                    this.c.execSQL("CREATE TABLE t_bridge_deck AS SELECT * FROM bridge_deck WHERE deck_set = " + -1000 + " AND deck_no = " + i4);
                    this.c.execSQL("UPDATE t_bridge_deck SET deck_set = " + -997 + ", deck_no = " + (i3 + 1));
                    this.c.execSQL("REPLACE INTO bridge_deck SELECT * FROM t_bridge_deck");
                    this.c.execSQL("DROP TABLE IF EXISTS t_bridge_deck");
                    int i5 = iArr[i3];
                    this.c.execSQL("DROP TABLE IF EXISTS t_bridge_dup_result");
                    this.c.execSQL("CREATE TABLE t_bridge_dup_result AS SELECT * FROM bridge_dup_result WHERE deck_set = " + -1000 + " AND deck_no = " + i5);
                    this.c.execSQL("UPDATE t_bridge_dup_result SET deck_set = " + -997 + ", deck_no = " + (i3 + 1));
                    this.c.execSQL("REPLACE INTO bridge_dup_result SELECT * FROM t_bridge_dup_result");
                    this.c.execSQL("DROP TABLE IF EXISTS t_bridge_dup_result");
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void b(int i) {
        this.c.execSQL("DELETE FROM bridge_dup_result WHERE deck_set = " + Integer.toString(i) + ";");
        this.c.execSQL("DELETE FROM bridge_deck WHERE deck_set = " + Integer.toString(i) + ";");
    }

    /* access modifiers changed from: package-private */
    public final void a(int i, int i2) {
        this.c.execSQL("DELETE FROM bridge_dup_result WHERE deck_set = " + Integer.toString(i) + " AND deck_no = " + Integer.toString(i2) + ";");
        this.c.execSQL("DELETE FROM bridge_deck WHERE deck_set = " + Integer.toString(i) + " AND deck_no = " + Integer.toString(i2) + ";");
    }

    /* access modifiers changed from: package-private */
    public final int c(int i) {
        int i2;
        Cursor rawQuery = this.c.rawQuery("SELECT count(*) FROM bridge_deck WHERE deck_set = " + Integer.toString(i), null);
        if (rawQuery.moveToNext()) {
            try {
                i2 = rawQuery.getInt(0);
            } catch (Exception e2) {
            }
            rawQuery.close();
            return i2;
        }
        i2 = 0;
        rawQuery.close();
        return i2;
    }

    /* access modifiers changed from: package-private */
    public final boolean a(int i, int i2, a aVar) {
        boolean z;
        if (aVar == null) {
            return false;
        }
        Cursor rawQuery = this.c.rawQuery("SELECT * FROM bridge_deck WHERE deck_set = " + Integer.toString(i) + " AND deck_no = " + Integer.toString(i2), null);
        if (rawQuery.moveToNext()) {
            aVar.f0a = rawQuery.getInt(rawQuery.getColumnIndex("deck_set"));
            aVar.b = rawQuery.getInt(rawQuery.getColumnIndex("deck_no"));
            aVar.d = (byte) rawQuery.getShort(rawQuery.getColumnIndex("dealer"));
            aVar.e = (byte) rawQuery.getShort(rawQuery.getColumnIndex("vul"));
            for (int i3 = 0; i3 < 4; i3++) {
                this.d[i3].setLength(0);
            }
            this.d[0].append(rawQuery.getString(rawQuery.getColumnIndex("club")));
            this.d[1].append(rawQuery.getString(rawQuery.getColumnIndex("diamond")));
            this.d[2].append(rawQuery.getString(rawQuery.getColumnIndex("heart")));
            this.d[3].append(rawQuery.getString(rawQuery.getColumnIndex("spade")));
            aVar.c = (byte[][]) Array.newInstance(Byte.TYPE, 4, 13);
            a(aVar.c);
            aVar.f = b.a(rawQuery.getString(rawQuery.getColumnIndex("create_datetime")));
            z = true;
        } else {
            z = false;
        }
        rawQuery.close();
        return z;
    }

    /* access modifiers changed from: package-private */
    public final c a(int i, long j) {
        Cursor rawQuery = this.c.rawQuery("SELECT * FROM bridge_dup_result WHERE deck_set = " + Integer.toString(-1000) + " AND deck_no = " + Integer.toString(i) + " AND play_datetime = '" + new Timestamp(j).toString() + "'", null);
        if (!rawQuery.moveToNext()) {
            rawQuery.close();
            return null;
        }
        a aVar = new a();
        if (!a(-1000, i, aVar)) {
            rawQuery.close();
            return null;
        }
        a.b bVar = new a.b();
        bVar.h = aVar;
        a(rawQuery, bVar);
        c cVar = new c(bVar);
        a(rawQuery, cVar);
        rawQuery.close();
        return cVar;
    }

    /* access modifiers changed from: package-private */
    public final LinkedList d(int i) {
        LinkedList linkedList = new LinkedList();
        Cursor rawQuery = this.c.rawQuery("SELECT a.vul, b.* FROM bridge_deck a, bridge_dup_result b WHERE a.deck_set = " + Integer.toString(i) + " AND a.deck_set = b.deck_set" + " AND a.deck_no = b.deck_no" + " ORDER BY b.deck_set, b.deck_no", null);
        int i2 = 0;
        while (rawQuery.moveToNext() && i2 < 100000) {
            c cVar = new c(null);
            a(rawQuery, cVar);
            cVar.d = (byte) rawQuery.getInt(rawQuery.getColumnIndex("vul"));
            linkedList.add(cVar);
            i2++;
        }
        rawQuery.close();
        return linkedList;
    }

    /* access modifiers changed from: package-private */
    public final boolean a(int i, int i2, long j, a.b bVar) {
        a aVar;
        if (bVar == null) {
            return false;
        }
        Cursor rawQuery = this.c.rawQuery("SELECT * FROM bridge_dup_result WHERE deck_set = " + Integer.toString(i) + " AND deck_no = " + Integer.toString(i2) + " AND play_datetime = '" + new Timestamp(j).toString() + "'", null);
        if (!rawQuery.moveToNext()) {
            rawQuery.close();
            return false;
        }
        if (bVar.h != null) {
            aVar = bVar.h;
        } else {
            aVar = new a();
        }
        if (!a(i, i2, aVar)) {
            rawQuery.close();
            return false;
        }
        if (bVar.h == null) {
            bVar.h = aVar;
        }
        a(rawQuery, bVar);
        rawQuery.close();
        return true;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:103:0x01f7  */
    /* JADX WARNING: Removed duplicated region for block: B:114:0x021d A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:123:0x0247  */
    /* JADX WARNING: Removed duplicated region for block: B:132:0x00eb A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00fd  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x0153  */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x01ab  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.LinkedList a(java.lang.String r29) {
        /*
            r28 = this;
            java.util.LinkedList r4 = new java.util.LinkedList
            r4.<init>()
            r0 = r28
            android.database.sqlite.SQLiteDatabase r0 = r0.c
            r5 = r0
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "SELECT a.vul, b.* FROM bridge_deck a, bridge_dup_result b WHERE a.deck_set = "
            java.lang.StringBuilder r6 = r6.append(r7)
            r7 = -1000(0xfffffffffffffc18, float:NaN)
            java.lang.String r7 = java.lang.Integer.toString(r7)
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r7 = " AND a.deck_set = b.deck_set"
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r7 = " AND a.deck_no = b.deck_no"
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r7 = " ORDER BY b.deck_set, b.deck_no DESC"
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r6 = r6.toString()
            r7 = 0
            android.database.Cursor r5 = r5.rawQuery(r6, r7)
            r6 = 0
            r7 = 1
            r8 = 0
            r9 = 0
            r10 = 0
            r11 = 0
            r12 = 0
            r13 = 0
            r14 = 0
            r15 = 0
            r16 = 0
            if (r29 == 0) goto L_0x02a2
            int r17 = r29.length()
            if (r17 == 0) goto L_0x02a2
            java.lang.String r17 = "all=N;"
            r0 = r29
            r1 = r17
            boolean r17 = r0.contains(r1)
            if (r17 == 0) goto L_0x005b
            r7 = 0
        L_0x005b:
            java.lang.String r17 = "nt=Y;"
            r0 = r29
            r1 = r17
            boolean r17 = r0.contains(r1)
            if (r17 == 0) goto L_0x0068
            r8 = 1
        L_0x0068:
            java.lang.String r17 = "major=Y;"
            r0 = r29
            r1 = r17
            boolean r17 = r0.contains(r1)
            if (r17 == 0) goto L_0x0075
            r9 = 1
        L_0x0075:
            java.lang.String r17 = "minor=Y;"
            r0 = r29
            r1 = r17
            boolean r17 = r0.contains(r1)
            if (r17 == 0) goto L_0x0082
            r10 = 1
        L_0x0082:
            java.lang.String r17 = "game=Y;"
            r0 = r29
            r1 = r17
            boolean r17 = r0.contains(r1)
            if (r17 == 0) goto L_0x008f
            r11 = 1
        L_0x008f:
            java.lang.String r17 = "slam=Y;"
            r0 = r29
            r1 = r17
            boolean r17 = r0.contains(r1)
            if (r17 == 0) goto L_0x009c
            r12 = 1
        L_0x009c:
            java.lang.String r17 = "game_miss=Y;"
            r0 = r29
            r1 = r17
            boolean r17 = r0.contains(r1)
            if (r17 == 0) goto L_0x00a9
            r13 = 1
        L_0x00a9:
            java.lang.String r17 = "slam_miss=Y;"
            r0 = r29
            r1 = r17
            boolean r17 = r0.contains(r1)
            if (r17 == 0) goto L_0x00b6
            r14 = 1
        L_0x00b6:
            java.lang.String r17 = "one_set=Y;"
            r0 = r29
            r1 = r17
            boolean r17 = r0.contains(r1)
            if (r17 == 0) goto L_0x00c3
            r15 = 1
        L_0x00c3:
            java.lang.String r17 = "more_set=Y;"
            r0 = r29
            r1 = r17
            boolean r17 = r0.contains(r1)
            if (r17 == 0) goto L_0x02a2
            r16 = 1
            r23 = r16
            r16 = r6
            r6 = r23
            r24 = r14
            r14 = r8
            r8 = r24
            r25 = r12
            r12 = r10
            r10 = r25
            r26 = r9
            r9 = r13
            r13 = r26
            r27 = r7
            r7 = r15
            r15 = r27
        L_0x00eb:
            boolean r17 = r5.moveToNext()
            if (r17 == 0) goto L_0x029e
            r17 = 50
            r0 = r16
            r1 = r17
            if (r0 >= r1) goto L_0x029e
            r17 = 1
            if (r15 == 0) goto L_0x00ff
            r17 = 0
        L_0x00ff:
            java.lang.String r18 = "contract"
            r0 = r5
            r1 = r18
            int r18 = r0.getColumnIndex(r1)
            r0 = r5
            r1 = r18
            int r18 = r0.getInt(r1)
            r0 = r18
            byte r0 = (byte) r0
            r18 = r0
            byte r19 = a.b.h(r18)
            byte r20 = a.b.i(r18)
            java.lang.String r21 = "win_tricks"
            r0 = r5
            r1 = r21
            int r21 = r0.getColumnIndex(r1)
            r0 = r5
            r1 = r21
            int r21 = r0.getInt(r1)
            r0 = r21
            byte r0 = (byte) r0
            r21 = r0
            if (r14 == 0) goto L_0x013d
            r22 = 4
            r0 = r20
            r1 = r22
            if (r0 != r1) goto L_0x013d
            r17 = 0
        L_0x013d:
            if (r13 == 0) goto L_0x0147
            boolean r22 = a.b.E(r20)
            if (r22 == 0) goto L_0x0147
            r17 = 0
        L_0x0147:
            if (r12 == 0) goto L_0x0151
            boolean r22 = a.b.F(r20)
            if (r22 == 0) goto L_0x0151
            r17 = 0
        L_0x0151:
            if (r11 == 0) goto L_0x0195
            r22 = 4
            r0 = r20
            r1 = r22
            if (r0 != r1) goto L_0x016d
            r22 = 3
            r0 = r19
            r1 = r22
            if (r0 < r1) goto L_0x016d
            r22 = 5
            r0 = r19
            r1 = r22
            if (r0 > r1) goto L_0x016d
            r17 = 0
        L_0x016d:
            boolean r22 = a.b.E(r20)
            if (r22 == 0) goto L_0x0185
            r22 = 4
            r0 = r19
            r1 = r22
            if (r0 < r1) goto L_0x0185
            r22 = 5
            r0 = r19
            r1 = r22
            if (r0 > r1) goto L_0x0185
            r17 = 0
        L_0x0185:
            boolean r22 = a.b.F(r20)
            if (r22 == 0) goto L_0x0195
            r22 = 5
            r0 = r19
            r1 = r22
            if (r0 != r1) goto L_0x0195
            r17 = 0
        L_0x0195:
            if (r10 == 0) goto L_0x01a9
            r22 = 6
            r0 = r19
            r1 = r22
            if (r0 < r1) goto L_0x01a9
            r22 = 7
            r0 = r19
            r1 = r22
            if (r0 > r1) goto L_0x01a9
            r17 = 0
        L_0x01a9:
            if (r9 == 0) goto L_0x01f5
            r22 = 4
            r0 = r20
            r1 = r22
            if (r0 != r1) goto L_0x01c5
            r22 = 3
            r0 = r19
            r1 = r22
            if (r0 >= r1) goto L_0x01c5
            r22 = 9
            r0 = r21
            r1 = r22
            if (r0 < r1) goto L_0x01c5
            r17 = 0
        L_0x01c5:
            boolean r22 = a.b.E(r20)
            if (r22 == 0) goto L_0x01dd
            r22 = 4
            r0 = r19
            r1 = r22
            if (r0 >= r1) goto L_0x01dd
            r22 = 10
            r0 = r21
            r1 = r22
            if (r0 < r1) goto L_0x01dd
            r17 = 0
        L_0x01dd:
            boolean r20 = a.b.F(r20)
            if (r20 == 0) goto L_0x01f5
            r20 = 5
            r0 = r19
            r1 = r20
            if (r0 >= r1) goto L_0x01f5
            r20 = 11
            r0 = r21
            r1 = r20
            if (r0 < r1) goto L_0x01f5
            r17 = 0
        L_0x01f5:
            if (r8 == 0) goto L_0x021b
            r20 = 6
            r0 = r19
            r1 = r20
            if (r0 >= r1) goto L_0x0209
            r20 = 12
            r0 = r21
            r1 = r20
            if (r0 < r1) goto L_0x0209
            r17 = 0
        L_0x0209:
            r20 = 7
            r0 = r19
            r1 = r20
            if (r0 >= r1) goto L_0x021b
            r20 = 13
            r0 = r21
            r1 = r20
            if (r0 < r1) goto L_0x021b
            r17 = 0
        L_0x021b:
            if (r19 <= 0) goto L_0x0245
            if (r7 == 0) goto L_0x0231
            r0 = r18
            r1 = r21
            byte r19 = a.b.b(r0, r1)
            r20 = -1
            r0 = r19
            r1 = r20
            if (r0 != r1) goto L_0x0231
            r17 = 0
        L_0x0231:
            if (r6 == 0) goto L_0x0245
            r0 = r18
            r1 = r21
            byte r18 = a.b.b(r0, r1)
            r19 = -1
            r0 = r18
            r1 = r19
            if (r0 >= r1) goto L_0x0245
            r17 = 0
        L_0x0245:
            if (r17 != 0) goto L_0x00eb
            a.a r17 = new a.a
            r17.<init>()
            r18 = -1000(0xfffffffffffffc18, float:NaN)
            java.lang.String r19 = "deck_no"
            r0 = r5
            r1 = r19
            int r19 = r0.getColumnIndex(r1)
            r0 = r5
            r1 = r19
            int r19 = r0.getInt(r1)
            r0 = r28
            r1 = r18
            r2 = r19
            r3 = r17
            boolean r17 = r0.a(r1, r2, r3)
            if (r17 == 0) goto L_0x00eb
            a.c r17 = new a.c
            r18 = 0
            r17.<init>(r18)
            r0 = r5
            r1 = r17
            a(r0, r1)
            java.lang.String r18 = "vul"
            r0 = r5
            r1 = r18
            int r18 = r0.getColumnIndex(r1)
            r0 = r5
            r1 = r18
            int r18 = r0.getInt(r1)
            r0 = r18
            byte r0 = (byte) r0
            r18 = r0
            r0 = r18
            r1 = r17
            r1.d = r0
            r0 = r4
            r1 = r17
            r0.add(r1)
            int r16 = r16 + 1
            goto L_0x00eb
        L_0x029e:
            r5.close()
            return r4
        L_0x02a2:
            r23 = r16
            r16 = r6
            r6 = r23
            r24 = r14
            r14 = r8
            r8 = r24
            r25 = r12
            r12 = r10
            r10 = r25
            r26 = r9
            r9 = r13
            r13 = r26
            r27 = r7
            r7 = r15
            r15 = r27
            goto L_0x00eb
        */
        throw new UnsupportedOperationException("Method not decompiled: net.weweweb.android.bridge.w.a(java.lang.String):java.util.LinkedList");
    }

    /* access modifiers changed from: package-private */
    public final int b() {
        int i;
        Cursor rawQuery = this.c.rawQuery("SELECT max(deck_no) FROM bridge_dup_result WHERE deck_set = " + Integer.toString(-1000), null);
        if (rawQuery.moveToNext()) {
            try {
                i = rawQuery.getInt(0);
            } catch (Exception e2) {
            }
            rawQuery.close();
            return i;
        }
        i = 0;
        rawQuery.close();
        return i;
    }

    public final w c() {
        this.c = this.b.getWritableDatabase();
        return this;
    }

    /* access modifiers changed from: package-private */
    public final void e(int i) {
        int i2;
        if (i < 0) {
            i2 = 0;
        } else {
            i2 = i;
        }
        int c2 = c(-1000);
        if (c2 != 0 && c2 >= i2) {
            Cursor rawQuery = this.c.rawQuery("SELECT * FROM bridge_deck WHERE deck_set = " + Integer.toString(-1000) + " ORDER BY create_datetime ASC", null);
            int i3 = c2 - i2;
            while (rawQuery.moveToNext() && i3 > 0) {
                a(rawQuery.getInt(rawQuery.getColumnIndex("deck_set")), rawQuery.getInt(rawQuery.getColumnIndex("deck_no")));
                i3--;
            }
            rawQuery.close();
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean a(a aVar) {
        byte[][] bArr = aVar.c;
        for (byte b2 = 0; b2 < 4; b2 = (byte) (b2 + 1)) {
            this.d[b2].setLength(0);
            for (byte b3 = 0; b3 < 13; b3 = (byte) (b3 + 1)) {
                this.d[b2].append(' ');
            }
        }
        for (byte b4 = 0; b4 < 4; b4 = (byte) (b4 + 1)) {
            for (byte b5 = 0; b5 < 13; b5 = (byte) (b5 + 1)) {
                if (b4 == 0) {
                    this.d[bArr[b4][b5] / 13].setCharAt(bArr[b4][b5] % 13, '0');
                }
                if (b4 == 1) {
                    this.d[bArr[b4][b5] / 13].setCharAt(bArr[b4][b5] % 13, '1');
                }
                if (b4 == 2) {
                    this.d[bArr[b4][b5] / 13].setCharAt(bArr[b4][b5] % 13, '2');
                }
                if (b4 == 3) {
                    this.d[bArr[b4][b5] / 13].setCharAt(bArr[b4][b5] % 13, '3');
                }
            }
        }
        SQLiteStatement compileStatement = this.c.compileStatement("REPLACE INTO bridge_deck (deck_set, deck_no, spade, heart, diamond, club, dealer, vul, create_datetime) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
        compileStatement.bindLong(1, (long) aVar.c());
        compileStatement.bindLong(2, (long) aVar.b());
        compileStatement.bindString(3, this.d[3].toString());
        compileStatement.bindString(4, this.d[2].toString());
        compileStatement.bindString(5, this.d[1].toString());
        compileStatement.bindString(6, this.d[0].toString());
        compileStatement.bindLong(7, (long) aVar.a());
        compileStatement.bindLong(8, (long) aVar.d());
        compileStatement.bindString(9, new Timestamp(aVar.f).toString());
        try {
            compileStatement.execute();
            try {
                compileStatement.close();
            } catch (Exception e2) {
            }
            return true;
        } catch (SQLException e3) {
            Log.e("BridgeGameDBAdapter", e3.toString());
            try {
                compileStatement.close();
            } catch (Exception e4) {
            }
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean a(a.b bVar, String str, String str2, String str3, String str4) {
        SQLiteStatement compileStatement = this.c.compileStatement("Replace INTO bridge_dup_result (deck_set, deck_no, play_datetime, player0, player1, player2, player3, declarer, contract, double_info, win_tricks, score, bid, play) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        compileStatement.bindLong(1, (long) bVar.r());
        compileStatement.bindLong(2, (long) bVar.q());
        compileStatement.bindString(3, new Timestamp(bVar.s).toString());
        if (str == null) {
            compileStatement.bindNull(4);
        } else {
            compileStatement.bindString(4, str);
        }
        if (str2 == null) {
            compileStatement.bindNull(5);
        } else {
            compileStatement.bindString(5, str2);
        }
        if (str3 == null) {
            compileStatement.bindNull(6);
        } else {
            compileStatement.bindString(6, str3);
        }
        if (str4 == null) {
            compileStatement.bindNull(7);
        } else {
            compileStatement.bindString(7, str4);
        }
        compileStatement.bindLong(8, (long) bVar.s());
        compileStatement.bindLong(9, (long) bVar.k());
        compileStatement.bindLong(10, (long) bVar.m());
        if (bVar.k() == 99 || bVar.k() == -1) {
            compileStatement.bindLong(11, 0);
            compileStatement.bindLong(12, 0);
        } else {
            compileStatement.bindLong(11, ((long) bVar.p[bVar.s() % 2]) + ((long) bVar.q[bVar.s() % 2]));
            compileStatement.bindLong(12, (long) ((bVar.s() % 2 == 0 ? 1 : -1) * a.b.b(bVar.Q(), bVar.k(), bVar.m(), bVar.p[bVar.s() % 2])));
        }
        compileStatement.bindString(13, bVar.f());
        compileStatement.bindString(14, bVar.A());
        try {
            compileStatement.execute();
            try {
                compileStatement.close();
            } catch (Exception e2) {
            }
            return true;
        } catch (SQLException e3) {
            Log.e("BridgeGameDBAdapter", e3.toString());
            try {
                compileStatement.close();
            } catch (Exception e4) {
            }
            return false;
        }
    }

    private static boolean a(Cursor cursor, a.b bVar) {
        if (cursor == null || bVar == null) {
            return false;
        }
        bVar.c();
        bVar.e(cursor.getInt(cursor.getColumnIndex("declarer")));
        bVar.R((byte) cursor.getInt(cursor.getColumnIndex("contract")));
        bVar.S((byte) cursor.getInt(cursor.getColumnIndex("double_info")));
        bVar.c(cursor.getString(cursor.getColumnIndex("bid")));
        bVar.d(cursor.getString(cursor.getColumnIndex("play")));
        bVar.s = b.a(cursor.getString(cursor.getColumnIndex("play_datetime")));
        if (bVar.I()) {
            bVar.T(bVar.p());
            bVar.R(bVar.l());
            bVar.S(bVar.n());
        }
        return true;
    }

    private static boolean a(Cursor cursor, c cVar) {
        if (cursor == null) {
            return false;
        }
        if (cVar == null) {
            return false;
        }
        cVar.b = cursor.getInt(cursor.getColumnIndex("deck_set"));
        cVar.c = cursor.getInt(cursor.getColumnIndex("deck_no"));
        if (cVar.b() != null) {
            cVar.d = cVar.b().G();
        } else {
            cVar.d = 0;
        }
        cVar.e = (byte) cursor.getInt(cursor.getColumnIndex("declarer"));
        cVar.f = (byte) cursor.getInt(cursor.getColumnIndex("contract"));
        cVar.g = (byte) cursor.getInt(cursor.getColumnIndex("double_info"));
        cVar.h = (byte) cursor.getInt(cursor.getColumnIndex("win_tricks"));
        cVar.i = b.a(cursor.getString(cursor.getColumnIndex("play_datetime")));
        cVar.j = new String[4];
        cVar.j[0] = cursor.getString(cursor.getColumnIndex("player0"));
        cVar.j[1] = cursor.getString(cursor.getColumnIndex("player1"));
        cVar.j[2] = cursor.getString(cursor.getColumnIndex("player2"));
        cVar.j[3] = cursor.getString(cursor.getColumnIndex("player3"));
        String string = cursor.getString(cursor.getColumnIndex("bid"));
        String string2 = cursor.getString(cursor.getColumnIndex("play"));
        if (string != null) {
            if (string.equals("PPPP")) {
                cVar.k = true;
            } else if (string.endsWith("PPP") && string.length() > 3 && string2 != null) {
                if (string2.length() == 104 || string2.contains("*")) {
                    cVar.k = true;
                } else {
                    cVar.k = false;
                }
            }
            return true;
        }
        cVar.k = false;
        return true;
    }

    private void a(byte[][] bArr) {
        int[] iArr = new int[4];
        for (byte b2 = 0; b2 < 4; b2 = (byte) (b2 + 1)) {
            iArr[b2] = 0;
        }
        for (byte b3 = 0; b3 < 4; b3 = (byte) (b3 + 1)) {
            for (byte b4 = 0; b4 < 13; b4 = (byte) (b4 + 1)) {
                if (this.d[b3].charAt(b4) == '0') {
                    byte[] bArr2 = bArr[0];
                    int i = iArr[0];
                    iArr[0] = i + 1;
                    bArr2[i] = (byte) ((b3 * 13) + b4);
                }
                if (this.d[b3].charAt(b4) == '1') {
                    byte[] bArr3 = bArr[1];
                    int i2 = iArr[1];
                    iArr[1] = i2 + 1;
                    bArr3[i2] = (byte) ((b3 * 13) + b4);
                }
                if (this.d[b3].charAt(b4) == '2') {
                    byte[] bArr4 = bArr[2];
                    int i3 = iArr[2];
                    iArr[2] = i3 + 1;
                    bArr4[i3] = (byte) ((b3 * 13) + b4);
                }
                if (this.d[b3].charAt(b4) == '3') {
                    byte[] bArr5 = bArr[3];
                    int i4 = iArr[3];
                    iArr[3] = i4 + 1;
                    bArr5[i4] = (byte) ((b3 * 13) + b4);
                }
            }
        }
    }
}
