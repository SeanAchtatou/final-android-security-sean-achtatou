package net.weweweb.android.bridge;

import a.b;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.widget.Toast;
import net.weweweb.android.common.App;
import net.weweweb.android.common.a;

/* compiled from: ProGuard */
public class BridgeApp extends App {
    public static byte A = 0;
    public static boolean B = true;
    public static String C = "11,9,1";
    public static String D = null;
    public static boolean E = true;
    public static String F = null;
    public static int G = 4;
    public static int H = 160;
    public static float I = 1.0f;
    public static boolean n = false;
    public static byte o = 0;
    public static boolean p = false;
    public static int q = 2000;
    public static int r = 0;
    public static final int[] s = {0, 100, 200, 300, 400};
    public static boolean t = true;
    public static int u = 16;
    public static int v = 0;
    public static boolean w = false;
    public static boolean x = true;
    public static boolean y = true;
    public static byte z = 2;
    public Handler J = new l(this);

    /* renamed from: a  reason: collision with root package name */
    BiddingActivity f11a;
    MainActivity b;
    GameRoomActivity c;
    PlayerListActivity d;
    MessageActivity e;
    NetGameActivity f;
    ScoreBoardActivity g;
    SoloGameActivity h = null;
    TableListActivity i;
    WebPageActivity j;
    public q k;
    public a l;
    public ad m;

    public void onCreate() {
        super.onCreate();
        this.k = new q(this);
        this.l = new a(this);
        a.a(40);
    }

    public static SpannableStringBuilder a(String str) {
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        spannableStringBuilder.append((CharSequence) str);
        int indexOf = spannableStringBuilder.toString().indexOf(b.e[1]);
        if (indexOf >= 0) {
            spannableStringBuilder.setSpan(new ForegroundColorSpan(-65536), indexOf, indexOf + 1, 33);
        }
        int indexOf2 = spannableStringBuilder.toString().indexOf(b.e[2]);
        if (indexOf2 >= 0) {
            spannableStringBuilder.setSpan(new ForegroundColorSpan(-65536), indexOf2, indexOf2 + 1, 33);
        }
        return spannableStringBuilder;
    }

    public final void b(String str) {
        a(0, 0, 0, str);
    }

    public final void a(int i2, int i3, int i4, Object obj) {
        Message obtain = Message.obtain();
        obtain.what = i2;
        obtain.arg1 = i3;
        obtain.arg2 = i4;
        obtain.obj = obj;
        this.J.sendMessage(obtain);
    }

    public final void c(String str) {
        if (str != null && this.b != null) {
            Toast.makeText(this.b, Html.fromHtml(str), 0).show();
        }
    }

    public final void d(String str) {
        if (str == null) {
            return;
        }
        if (this.f11a != null) {
            Toast.makeText(this.f11a, Html.fromHtml(str), 0).show();
        } else if (this.f != null) {
            Toast.makeText(this.f, Html.fromHtml(str), 0).show();
        } else {
            if (this.i != null) {
                Toast.makeText(this.i, Html.fromHtml(str), 0).show();
            }
            if (this.d != null) {
                Toast.makeText(this.d, Html.fromHtml(str), 0).show();
            }
        }
    }
}
