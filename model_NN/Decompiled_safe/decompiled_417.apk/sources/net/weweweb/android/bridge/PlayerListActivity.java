package net.weweweb.android.bridge;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import java.util.ArrayList;
import java.util.HashMap;
import net.weweweb.android.free.bridge.R;

/* compiled from: ProGuard */
public class PlayerListActivity extends Activity {

    /* renamed from: a  reason: collision with root package name */
    BridgeApp f19a;
    ArrayList b;
    SimpleAdapter c;

    /* access modifiers changed from: package-private */
    public final void a(ab abVar) {
        boolean z;
        if (abVar != null) {
            int i = 0;
            while (true) {
                if (i >= this.b.size()) {
                    z = false;
                    break;
                } else if (((String) ((HashMap) this.b.get(i)).get("player")).compareTo(abVar.d) == 0) {
                    ((HashMap) this.b.get(i)).put("status", abVar.a());
                    z = true;
                    break;
                } else if (((String) ((HashMap) this.b.get(i)).get("player")).compareTo(abVar.d) > 0) {
                    this.b.add(i, b(abVar));
                    return;
                } else {
                    i++;
                }
            }
            if (!z) {
                this.b.add(b(abVar));
            }
            if (this.c != null) {
                this.c.notifyDataSetChanged();
            }
        }
    }

    private void a() {
        ab[] abVarArr = ((BridgeApp) getApplicationContext()).k.d.h;
        synchronized (abVarArr) {
            for (int i = 0; i < abVarArr.length; i++) {
                if (abVarArr[i] != null) {
                    a(abVarArr[i]);
                }
            }
        }
    }

    private static HashMap b(ab abVar) {
        HashMap hashMap = new HashMap();
        hashMap.put("player", abVar.d);
        hashMap.put("status", abVar.a());
        return hashMap;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.playerlist);
        this.f19a = (BridgeApp) getApplicationContext();
        this.f19a.d = this;
        this.b = new ArrayList();
        a();
        this.c = new SimpleAdapter(this, this.b, R.layout.playerlistitem, new String[]{"player", "status"}, new int[]{R.id.playerListViewItemPlayer, R.id.playerListViewItemStatus});
        ((ListView) findViewById(R.id.playerListView)).setAdapter((ListAdapter) this.c);
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4) {
            return super.onKeyDown(i, keyEvent);
        }
        new AlertDialog.Builder(this).setIcon(17301543).setTitle(this.f19a.getString(R.string.prompt_confirm_title)).setMessage(this.f19a.getString(R.string.prompt_server_disconnect)).setPositiveButton("Yes", new av(this)).setNegativeButton("No", (DialogInterface.OnClickListener) null).show();
        return true;
    }
}
