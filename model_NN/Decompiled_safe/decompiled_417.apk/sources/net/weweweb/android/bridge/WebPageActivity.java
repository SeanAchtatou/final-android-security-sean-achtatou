package net.weweweb.android.bridge;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;
import net.weweweb.android.free.bridge.R;

/* compiled from: ProGuard */
public class WebPageActivity extends Activity {

    /* renamed from: a  reason: collision with root package name */
    BridgeApp f27a;
    WebView b;

    /* access modifiers changed from: package-private */
    public final void a() {
        if (this.f27a.j == this) {
            this.f27a.j = null;
        }
        finish();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f27a = (BridgeApp) getApplicationContext();
        this.f27a.j = this;
        setContentView((int) R.layout.webpage);
        this.b = (WebView) findViewById(R.id.webPageView);
        this.b.getSettings().setJavaScriptEnabled(true);
        this.b.getSettings().setJavaScriptCanOpenWindowsAutomatically(false);
        this.b.getSettings().setSupportMultipleWindows(false);
        Bundle extras = getIntent().getExtras();
        String string = getApplicationContext().getString(R.string.homepage_url);
        if (extras != null) {
            String string2 = extras.getString("URL");
            if (string2 != null) {
                this.b.loadUrl(string2);
            } else {
                this.b.loadUrl(string);
            }
        } else {
            this.b.loadUrl(string);
        }
    }
}
