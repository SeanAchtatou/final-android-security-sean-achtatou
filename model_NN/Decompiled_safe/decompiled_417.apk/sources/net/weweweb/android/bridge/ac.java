package net.weweweb.android.bridge;

/* compiled from: ProGuard */
final class ac {
    static byte k = 0;
    static byte l = 1;
    static byte m = 2;
    static byte n = 3;
    static byte o = 4;

    /* renamed from: a  reason: collision with root package name */
    q f31a;
    public int b = -1;
    public byte c = -1;
    public byte d = 0;
    public byte e = 0;
    public byte[] f = new byte[12];
    public boolean g = false;
    public ab[] h = new ab[12];
    byte[] i = new byte[12];
    String[] j = new String[12];
    public byte p = 0;

    public ac(q qVar, int i2) {
        this.f31a = qVar;
        this.b = i2;
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        if (this.c != -1) {
            byte b2 = 0;
            while (b2 < this.d) {
                if (this.h[b2] == null) {
                    b2 = (byte) (b2 + 1);
                } else {
                    return;
                }
            }
            this.c = -1;
            b((byte) 0);
        }
    }

    /* access modifiers changed from: package-private */
    public final String b() {
        if (this.p == 0) {
            return "Empty";
        }
        if (this.p == 1) {
            return "Waiting";
        }
        if (this.p == 2) {
            return "Playing";
        }
        return "Unknown";
    }

    /* access modifiers changed from: package-private */
    public final boolean c() {
        if (e() >= this.e) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public final boolean d() {
        if (e() >= this.d) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public final boolean a(byte b2) {
        return !(b2 < this.d);
    }

    /* access modifiers changed from: package-private */
    public final byte e() {
        byte b2;
        synchronized (this.h) {
            b2 = 0;
            for (byte b3 = 0; b3 < this.d; b3 = (byte) (b3 + 1)) {
                if (this.h[b3] != null) {
                    b2 = (byte) (b2 + 1);
                }
            }
        }
        return b2;
    }

    public final void b(byte b2) {
        if (b2 == 0 || b2 == 1 || b2 == 2) {
            this.p = b2;
        }
    }
}
