package net.weweweb.android.bridge;

import android.os.Handler;
import android.os.Message;
import android.widget.Toast;

/* compiled from: ProGuard */
final class bq extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ bp f70a;

    bq(bp bpVar) {
        this.f70a = bpVar;
    }

    public final void handleMessage(Message message) {
        if (this.f70a.b == null) {
            super.handleMessage(message);
            return;
        }
        if (message.what == 1001) {
            this.f70a.a((byte) message.arg1, (String) message.obj);
            if (message.obj != null) {
                this.f70a.i();
                Toast.makeText(this.f70a.f32a.h, (String) message.obj, 1).show();
            }
        } else if (message.what == 1002) {
            if (this.f70a.h == 1) {
                this.f70a.d((byte) message.arg1);
            }
        } else if (message.what == 1003) {
            this.f70a.j = false;
            try {
                this.f70a.f32a.h.c.invalidate();
            } catch (Exception e) {
            }
            if (!this.f70a.b.K()) {
                this.f70a.d();
            } else if (this.f70a.b.k() != 99) {
                this.f70a.c(this.f70a.b.p[this.f70a.b.s() % 2]);
            } else {
                this.f70a.c((byte) 0);
            }
        } else if (message.what == 1004) {
            bp.a(this.f70a, message.arg2 == 1);
        }
        super.handleMessage(message);
    }
}
