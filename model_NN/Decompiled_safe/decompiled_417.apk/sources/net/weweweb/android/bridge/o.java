package net.weweweb.android.bridge;

import a.b;
import java.util.LinkedList;

/* compiled from: ProGuard */
public final class o {
    static String[][] h = {new String[]{"cardtypecount", "IC"}, new String[]{"dp", "ICI"}, new String[]{"hascards", "BCSI"}, new String[]{"hasstopper", "BCB"}, new String[]{"longestsuit", "CI"}, new String[]{"suitcount", "IC"}};
    static String[][] i = {new String[]{"distribution", "S"}, new String[]{"balhand_5m", "B"}, new String[]{"balhand_n5m", "B"}, new String[]{"has4cm", "B"}, new String[]{"hassingleton", "B"}, new String[]{"hasvoid", "B"}, new String[]{"hcp", "I"}, new String[]{"quicktrick", "I"}, new String[]{"rule15", "B"}, new String[]{"rule20", "B"}};
    static String[][] j = {new String[]{","}, new String[]{"+", "-", "*", "/"}, new String[]{"==", "<=", ">=", "!=", "<", ">"}, new String[]{"!", "&", "|"}, new String[]{"(", ")"}, new String[]{"t", "f"}};

    /* renamed from: a  reason: collision with root package name */
    String f90a = null;
    LinkedList b = new LinkedList();
    String c = null;
    int d;
    byte e;
    b f = null;
    boolean g = false;

    public o() {
    }

    public o(b bVar) {
        this.f = bVar;
    }

    private boolean a() {
        boolean c2 = c();
        while (a("|")) {
            if (a("|")) {
                this.d++;
                c2 |= c();
            }
        }
        return c2;
    }

    private boolean b() {
        boolean z = false;
        while (a("!")) {
            if (a("!")) {
                this.d++;
                z = !z;
            }
        }
        if (!z) {
            return d();
        }
        if (!d()) {
            return true;
        }
        return false;
    }

    private boolean c() {
        boolean b2 = b();
        while (a("&")) {
            if (a("&")) {
                this.d++;
                b2 &= b();
            }
        }
        return b2;
    }

    private boolean d() {
        int i2;
        boolean z;
        String m;
        char c2;
        boolean v;
        boolean z2;
        if (a("t")) {
            this.d++;
            return true;
        } else if (a("f")) {
            this.d++;
            return false;
        } else if (a("(")) {
            this.d++;
            boolean a2 = a();
            if (!a(")")) {
                throw new p("Missing right parenthesis.");
            }
            this.d++;
            return a2;
        } else if (l()) {
            throw new p("Incomplete rule.");
        } else if (j() == 'F' && k() == 'B') {
            String i3 = i();
            this.d++;
            if (!a("(")) {
                throw new p("No \"(\" after " + i3);
            }
            this.d++;
            if (i3.equals("hascards")) {
                char h2 = h();
                if (!a(",")) {
                    throw new p("Invalid parameter for function " + i3);
                }
                this.d++;
                String m2 = m();
                if (!a(",")) {
                    throw new p("Invalid parameter for function " + i3);
                }
                this.d++;
                z2 = this.g ? true : this.f.a(this.e, Character.toUpperCase(h2), m2.toUpperCase(), e());
            } else if (i3.equals("hasstopper")) {
                char h3 = h();
                if (!a(",")) {
                    throw new p("Invalid parameter for function " + i3);
                }
                this.d++;
                z2 = this.g ? true : this.f.a(this.e, Character.toUpperCase(h3), a());
            } else {
                z2 = false;
            }
            if (!a(")")) {
                throw new p("No \")\" after " + i3);
            }
            this.d++;
            return z2;
        } else if (j() == 'I' && k() == 'B') {
            if (a("balhand_5m")) {
                v = a(true);
            } else if (a("balhand_n5m")) {
                v = a(false);
            } else if (a("rule15")) {
                v = this.g ? true : this.f.I(this.e);
            } else if (a("rule20")) {
                v = this.g ? true : this.f.J(this.e);
            } else if (a("has4cm")) {
                v = this.g ? true : this.f.v(this.e);
            } else {
                throw new p("Invalid boolean identifier " + i());
            }
            this.d++;
            return v;
        } else {
            char k = k();
            if (k == 'B') {
                z = a();
                i2 = 0;
                c2 = ' ';
                m = null;
            } else if (k == 'C') {
                i2 = 0;
                z = false;
                m = null;
                c2 = h();
            } else if (k == 'I') {
                i2 = e();
                z = false;
                c2 = ' ';
                m = null;
            } else if (k == 'S') {
                i2 = 0;
                z = false;
                m = m();
                c2 = ' ';
            } else {
                throw new p("Token " + ((String) this.b.get(this.d)) + " not recognized");
            }
            if (l()) {
                throw new p("Incomplete rule.");
            }
            if (a("<")) {
                this.d++;
                if (k == 'B') {
                    throw new p("Invalid expression for boolean < boolean");
                } else if (k == 'C') {
                    return c2 < h();
                } else {
                    if (k == 'I') {
                        return i2 < e();
                    }
                    if (k == 'S') {
                        return m.compareTo(m()) < 0;
                    }
                }
            } else if (a("<=")) {
                this.d++;
                if (k == 'B') {
                    throw new p("Invalid expression for boolean < boolean");
                } else if (k == 'C') {
                    return c2 <= h();
                } else {
                    if (k == 'I') {
                        return i2 <= e();
                    }
                    if (k == 'S') {
                        return m.compareTo(m()) <= 0;
                    }
                }
            } else if (a(">")) {
                this.d++;
                if (k == 'B') {
                    throw new p("Invalid expression for boolean < boolean");
                } else if (k == 'C') {
                    return c2 > h();
                } else {
                    if (k == 'I') {
                        return i2 > e();
                    }
                    if (k == 'S') {
                        return m.compareTo(m()) > 0;
                    }
                }
            } else if (a(">=")) {
                this.d++;
                if (k == 'B') {
                    throw new p("Invalid expression for boolean < boolean");
                } else if (k == 'C') {
                    return c2 >= h();
                } else {
                    if (k == 'I') {
                        return i2 >= e();
                    }
                    if (k == 'S') {
                        return m.compareTo(m()) >= 0;
                    }
                }
            } else if (a("==")) {
                this.d++;
                if (k == 'B') {
                    return z == a();
                }
                if (k == 'C') {
                    return c2 == h();
                }
                if (k == 'I') {
                    return i2 == e();
                }
                if (k == 'S') {
                    return m.compareTo(m()) == 0;
                }
            } else if (a("!=")) {
                this.d++;
                if (k == 'B') {
                    return z != a();
                }
                if (k == 'C') {
                    return c2 != h();
                }
                if (k == 'I') {
                    return i2 != e();
                }
                if (k == 'S') {
                    return m.compareTo(m()) != 0;
                }
            } else {
                throw new p("Unexpected token \"" + ((String) this.b.get(this.d)) + "\" encountered.");
            }
            return false;
        }
    }

    private int e() {
        int i2 = 1;
        if (a("-")) {
            this.d++;
            i2 = -1;
        }
        int g2 = i2 * g();
        while (true) {
            if (!a("+") && !a("-")) {
                return g2;
            }
            if (a("+")) {
                this.d++;
                g2 += g();
            } else {
                this.d++;
                g2 -= g();
            }
        }
    }

    private int f() {
        int n;
        if (a("-")) {
            this.d++;
            if (l()) {
                throw new p("End-of-line encountered in the middle of an expression.");
            }
            try {
                int parseInt = Integer.parseInt((String) this.b.get(this.d));
                this.d++;
                return parseInt * -1;
            } catch (Exception e2) {
                throw new p("Invalid numeric token \"" + ((String) this.b.get(this.d)) + "\".");
            }
        } else {
            try {
                int parseInt2 = Integer.parseInt((String) this.b.get(this.d));
                this.d++;
                return parseInt2;
            } catch (Exception e3) {
                if (a("(")) {
                    this.d++;
                    int e4 = e();
                    if (!a(")")) {
                        throw new p("Missing right parenthesis.");
                    }
                    this.d++;
                    return e4;
                } else if (j() == 'I') {
                    if (a("hcp")) {
                        n = this.g ? 13 : this.f.k(this.e);
                    } else if (a("quicktrick")) {
                        n = this.g ? 5 : (int) this.f.n(this.e);
                    } else {
                        throw new p("Invalid interger identifier " + i());
                    }
                    this.d++;
                    return n;
                } else if (j() == 'F') {
                    String str = (String) this.b.get(this.d);
                    int i2 = 0;
                    this.d++;
                    if (!a("(")) {
                        throw new p("No \"(\" after " + str);
                    }
                    this.d++;
                    if (str.equals("suitcount")) {
                        i2 = this.g ? 4 : this.f.c(this.e, Character.toUpperCase(h()));
                    } else if (str.equals("cardtypecount")) {
                        i2 = this.g ? 4 : this.f.a(this.e, Character.toUpperCase(h()));
                    } else if (str.equals("dp")) {
                        char h2 = h();
                        if (!a(",")) {
                            throw new p("Invalid parameter for function " + str);
                        }
                        this.d++;
                        i2 = this.g ? 1 : this.f.a(this.e, h2, e());
                    }
                    if (!a(")")) {
                        throw new p("No \")\" after " + str);
                    }
                    this.d++;
                    return i2;
                } else if (l()) {
                    throw new p("End-of-line encountered in the middle of an expression.");
                } else if (a(")")) {
                    throw new p("Extra right parenthesis.");
                } else if (a("+") || a("*") || a("/")) {
                    throw new p("Misplaced operator.");
                } else {
                    throw new p("Unexpected integer \"" + ((String) this.b.get(this.d)) + "\" encountered.");
                }
            }
        }
    }

    private int g() {
        int f2 = f();
        while (true) {
            if (!a("*") && !a("/")) {
                return f2;
            }
            if (a("*")) {
                this.d++;
                f2 *= f();
            } else {
                this.d++;
                f2 /= f();
            }
        }
    }

    private char h() {
        char c2;
        String str = (String) this.b.get(this.d);
        char k = k();
        char j2 = j();
        if (k != 'C') {
            throw new p("Token " + str + " expected chararacter type.");
        } else if (j2 == 'C') {
            this.d++;
            if (str.length() == 3) {
                return str.charAt(1);
            }
            throw new p("Invalid character token " + str + ".");
        } else if (j() == 'F') {
            String str2 = (String) this.b.get(this.d);
            this.d++;
            if (!a("(")) {
                throw new p("No \"(\" after " + str2);
            }
            this.d++;
            if (str2.equals("longestsuit")) {
                c2 = this.g ? 'S' : this.f.b(this.e, e());
            } else {
                c2 = ' ';
            }
            if (!a(")")) {
                throw new p("No \")\" after " + str2);
            }
            this.d++;
            return c2;
        } else if (j() == 'I') {
            this.d++;
            return ' ';
        } else {
            throw new p("Invalid type for Token " + str + ".");
        }
    }

    public final boolean a(byte b2, String str) {
        String str2;
        this.e = b2;
        if (str == null) {
            throw new p("Rule string is null.");
        }
        this.f90a = str.replaceAll(" ", "").toLowerCase();
        String str3 = this.f90a;
        this.b.clear();
        int i2 = 0;
        while (i2 < str3.length()) {
            int i3 = 0;
            while (true) {
                if (i3 < j.length) {
                    for (int i4 = 0; i4 < j[i3].length; i4++) {
                        if (str3.startsWith(j[i3][i4], i2)) {
                            str2 = j[i3][i4];
                            break;
                        }
                    }
                    i3++;
                } else {
                    int i5 = 0;
                    while (true) {
                        if (i5 >= h.length) {
                            int i6 = 0;
                            while (true) {
                                if (i6 < i.length) {
                                    if (str3.startsWith(i[i6][0], i2)) {
                                        str2 = i[i6][0];
                                        break;
                                    }
                                    i6++;
                                } else if (str3.startsWith("\"", i2)) {
                                    int indexOf = str3.indexOf("\"", i2 + 1);
                                    if (indexOf == -1) {
                                        throw new p("String constant is not properly closed");
                                    }
                                    str2 = str3.substring(i2, indexOf + 1);
                                } else if (str3.startsWith("'", i2)) {
                                    if (str3.length() <= i2 + 2 || str3.charAt(i2 + 2) != '\'') {
                                        throw new p("Character constant is not properly closed");
                                    }
                                    str2 = str3.substring(i2, i2 + 3);
                                } else if (str3.substring(i2, i2 + 1).matches("[0-9]")) {
                                    int i7 = 0;
                                    while (i2 + i7 < str3.length() && str3.substring(i2 + i7, i2 + i7 + 1).matches("[0-9]")) {
                                        i7++;
                                    }
                                    str2 = str3.substring(i2, i7 + i2);
                                } else {
                                    str2 = null;
                                }
                            }
                        } else if (str3.startsWith(h[i5][0], i2)) {
                            str2 = h[i5][0];
                            break;
                        } else {
                            i5++;
                        }
                    }
                }
            }
            if (str2 != null) {
                this.b.add(str2);
                i2 += str2.length();
            } else {
                throw new p("Unable to tokenize at " + str3.substring(i2));
            }
        }
        if (this.b.size() == 0) {
            throw new p("No token in rule string.");
        }
        this.d = 0;
        boolean a2 = a();
        if (this.b.size() <= this.d) {
            return a2;
        }
        throw new p("Extra token after " + ((String) this.b.get(this.d)));
    }

    private boolean a(boolean z) {
        if (this.g) {
            return true;
        }
        return this.f.a(this.e, z);
    }

    private String i() {
        return (String) this.b.get(this.d);
    }

    private char j() {
        String str = (String) this.b.get(this.d);
        for (String[] strArr : h) {
            if (str.equals(strArr[0])) {
                return 'F';
            }
        }
        for (String[] strArr2 : i) {
            if (str.equals(strArr2[0])) {
                return 'I';
            }
        }
        for (int i2 = 0; i2 < j.length; i2++) {
            for (String equals : j[i2]) {
                if (str.equals(equals)) {
                    return 'O';
                }
            }
        }
        if (str.matches("[TF]")) {
            return 'C';
        }
        if (str.startsWith("\"")) {
            return 'C';
        }
        if (str.startsWith("'")) {
            return 'C';
        }
        if (str.matches("[0-9]+")) {
            return 'C';
        }
        return 'X';
    }

    private char k() {
        String str = (String) this.b.get(this.d);
        for (int i2 = 0; i2 < h.length; i2++) {
            if (str.equals(h[i2][0])) {
                return h[i2][1].charAt(0);
            }
        }
        for (int i3 = 0; i3 < i.length; i3++) {
            if (str.equals(i[i3][0])) {
                return i[i3][1].charAt(0);
            }
        }
        if (str.matches("[TF]")) {
            return 'B';
        }
        if (str.startsWith("\"")) {
            return 'S';
        }
        if (str.startsWith("'")) {
            return 'C';
        }
        if (str.matches("[0-9]+")) {
            return 'I';
        }
        return '?';
    }

    private boolean a(String str) {
        if (l()) {
            return false;
        }
        return ((String) this.b.get(this.d)).equals(str);
    }

    private boolean l() {
        return this.d >= this.b.size();
    }

    private String m() {
        String n = n();
        while (a("+")) {
            this.d++;
            n = n + n();
        }
        return n;
    }

    private String n() {
        String i2 = i();
        if (i2.equals("(")) {
            this.d++;
            String m = m();
            if (!a(")")) {
                throw new p("Missing right parenthesis.");
            }
            this.d++;
            return m;
        } else if (k() != 'S') {
            throw new p("Token " + i2 + " expected string type.");
        } else if (i2.equals("\"\"")) {
            this.d++;
            return "";
        } else if (i2.startsWith("\"")) {
            this.d++;
            return i2.substring(1, i2.length() - 1);
        } else if (j() == 'F') {
            String str = (String) this.b.get(this.d);
            this.d++;
            if (!a("(")) {
                throw new p("No \"(\" after " + str);
            }
            this.d++;
            str.equals("sfunct");
            if (!a(")")) {
                throw new p("No \")\" after " + str);
            }
            this.d++;
            return null;
        } else if (j() != 'I') {
            return null;
        } else {
            String i3 = i();
            this.d++;
            if (i3.equals("distribution")) {
                return this.g ? "4-3-3-3" : this.f.j(this.e);
            }
            throw new p("Invalid string identifier " + i3 + ".");
        }
    }
}
