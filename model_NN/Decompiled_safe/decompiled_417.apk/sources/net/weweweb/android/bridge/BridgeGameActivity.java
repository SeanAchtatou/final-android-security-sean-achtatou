package net.weweweb.android.bridge;

import a.b;
import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.widget.TextView;
import net.weweweb.android.free.bridge.R;

/* compiled from: ProGuard */
public class BridgeGameActivity extends Activity {

    /* renamed from: a  reason: collision with root package name */
    BridgeApp f12a;
    b b;
    protected GamePlayingView c;
    MediaPlayer d;

    public final void a() {
        if (BridgeApp.x && this.d != null) {
            if (this.d.isPlaying()) {
                this.d.stop();
            }
            try {
                this.d.prepare();
            } catch (IllegalStateException e) {
                this.d.release();
                this.d = MediaPlayer.create(this, (int) R.raw.beep);
            } catch (Exception e2) {
            }
            this.d.start();
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.d = MediaPlayer.create(this, (int) R.raw.beep);
        this.f12a = (BridgeApp) getApplicationContext();
        this.b = this.f12a.m.b;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.text.SpannableStringBuilder.insert(int, java.lang.CharSequence):android.text.SpannableStringBuilder}
     arg types: [int, java.lang.String]
     candidates:
      ClspMth{android.text.SpannableStringBuilder.insert(int, java.lang.CharSequence):android.text.Editable}
      ClspMth{android.text.SpannableStringBuilder.insert(int, java.lang.CharSequence):android.text.SpannableStringBuilder} */
    /* access modifiers changed from: package-private */
    public final void b() {
        TextView textView = (TextView) findViewById(R.id.playingGameInfo);
        if (textView != null) {
            SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
            if (this.f12a.m.b.k() == -1) {
                spannableStringBuilder.append((CharSequence) "All Pass");
            } else {
                byte k = this.f12a.m.b.k();
                spannableStringBuilder.append((CharSequence) Byte.toString(b.c(k)));
                spannableStringBuilder.append((CharSequence) b.e[b.e(k)]);
                if (b.e(k) == 1 || b.e(k) == 2) {
                    spannableStringBuilder.setSpan(new ForegroundColorSpan(-65536), 1, 2, 33);
                }
                if (this.f12a.m.b.m() == 97) {
                    spannableStringBuilder.append((CharSequence) "X");
                } else if (this.f12a.m.b.m() == 98) {
                    spannableStringBuilder.append((CharSequence) "XX");
                }
            }
            spannableStringBuilder.insert(0, (CharSequence) "Contract: ");
            spannableStringBuilder.append((CharSequence) "  Declarer: ");
            if (this.f12a.m.b.s() == -1) {
                spannableStringBuilder.append((CharSequence) "N/A");
            } else {
                spannableStringBuilder.append((CharSequence) b.g[this.f12a.m.b.s()]);
            }
            spannableStringBuilder.append((CharSequence) ("   We: " + ((int) this.f12a.m.b.p[this.f12a.m.e % 2]) + " They: " + ((int) this.f12a.m.b.p[(this.f12a.m.e + 1) % 2])));
            textView.setText(spannableStringBuilder);
        }
    }
}
