package net.weweweb.android.bridge;

import a.c;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import net.weweweb.android.free.bridge.R;

/* compiled from: ProGuard */
final class az implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RecordBrowserActivity f53a;

    az(RecordBrowserActivity recordBrowserActivity) {
        this.f53a = recordBrowserActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        String str = this.f53a.f[i];
        if (str.equals(this.f53a.getString(R.string.cancel))) {
            dialogInterface.dismiss();
        } else if (str.equals(this.f53a.getString(R.string.delete))) {
            this.f53a.e.a(-1000, ((c) this.f53a.b.get(this.f53a.g)).c);
            this.f53a.b();
            dialogInterface.dismiss();
        } else if (str.equals(this.f53a.getString(R.string.view))) {
            Intent intent = new Intent();
            intent.setClass(this.f53a, GameViewerActivity.class);
            intent.putExtra("game_key", Integer.toString(((c) this.f53a.b.get(this.f53a.g)).b) + "," + Integer.toString(((c) this.f53a.b.get(this.f53a.g)).c) + "," + Long.toString(((c) this.f53a.b.get(this.f53a.g)).i));
            this.f53a.startActivity(intent);
        } else if (str.equals(this.f53a.getString(R.string.retry))) {
            dialogInterface.dismiss();
            AlertDialog.Builder builder = new AlertDialog.Builder(this.f53a);
            builder.setTitle("Retry: Select Seat");
            builder.setItems(new String[]{"Cancel", "North", "East", "South", "West"}, new ba(this));
            builder.create().show();
        }
    }
}
