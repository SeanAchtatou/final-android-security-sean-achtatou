package net.weweweb.android.bridge;

import a.a;
import a.b;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import java.util.ArrayList;
import net.weweweb.android.free.bridge.R;

/* compiled from: ProGuard */
public class NetGameActivity extends BridgeGameActivity implements View.OnClickListener {
    AlertDialog e = null;
    Dialog f = null;
    a g = null;
    bx h = null;
    Button[] i;
    Button j;
    String[] k;

    public final void c() {
        if (this.e != null) {
            this.e.dismiss();
            this.e = null;
        }
        if (this.f12a.f11a != null) {
            this.f12a.f11a.a();
        }
        if (this.f12a.g != null) {
            this.f12a.g.a();
        }
        if (this.f12a.f == this) {
            this.f12a.f = null;
        }
        finish();
    }

    public void onClick(View view) {
        if (view != this.j) {
            byte b = 0;
            while (true) {
                byte b2 = b;
                if (b2 < 14 && this.i != null) {
                    if (this.i[b2] != null && view == this.i[b2]) {
                        ((am) this.f12a.m).b(this.f12a.m.e, b2);
                        ((am) this.f12a.m).h = 3;
                        this.f.hide();
                    }
                    b = (byte) (b2 + 1);
                } else {
                    return;
                }
            }
        } else if (this.f != null) {
            this.f.hide();
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f12a.f = this;
        setContentView((int) R.layout.playing);
        this.c = (GamePlayingView) findViewById(R.id.gamePlayingView);
        this.c.requestFocus();
        this.c.a((BridgeGameActivity) this);
        TextView textView = (TextView) findViewById(R.id.playingBoardInfo);
        if (textView != null) {
            textView.setText("Dealer: " + b.g[this.f12a.m.b.h.a()] + "    " + "Vul: " + a.g[this.f12a.m.b.h.d()]);
        }
        b();
        if (((am) this.f12a.m).h == 0) {
            e();
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.netgame_option_menu, menu);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.f != null) {
            this.f.dismiss();
            this.f = null;
        }
        this.j = null;
        if (this.g != null) {
            this.g.dismiss();
            this.g = null;
        }
        if (this.h != null) {
            this.h.dismiss();
            this.h = null;
        }
        super.onDestroy();
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return super.onKeyDown(i2, keyEvent);
        }
        if (this.e == null) {
            this.e = new AlertDialog.Builder(this).setIcon(17301543).setTitle("Confirm Action").setMessage("This will quit the current game, are you sure?").setPositiveButton("Yes", new aj(this)).setNegativeButton("No", (DialogInterface.OnClickListener) null).show();
        } else {
            this.e.show();
        }
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.netGameOptionMenuItemMessage /*2131165378*/:
                Intent intent = new Intent();
                intent.setClass(this, MessageActivity.class);
                startActivity(intent);
                break;
            case R.id.netGameOptionMenuItemScore /*2131165379*/:
                Intent intent2 = new Intent();
                intent2.setClass(this, ScoreBoardActivity.class);
                startActivity(intent2);
                break;
            case R.id.netGameOptionMenuItemClaim /*2131165380*/:
                a(13 - this.f12a.m.b.C());
                this.f.show();
                break;
            case R.id.netGameOptionMenuItemBid /*2131165381*/:
                if (this.g == null) {
                    this.g = new a(this);
                }
                this.g.a(this.f12a.m);
                this.g.show();
                break;
            case R.id.netGameOptionMenuItemLastTurn /*2131165382*/:
                if (this.h == null) {
                    this.h = new bx(this);
                }
                this.h.a(this.f12a.m, (byte) (this.f12a.m.b.C() - 1));
                this.h.show();
                break;
            case R.id.netGameOptionMenuItemJoin /*2131165383*/:
                f();
                break;
        }
        return true;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean z;
        boolean z2;
        MenuItem findItem = menu.findItem(R.id.netGameOptionMenuItemClaim);
        MenuItem findItem2 = menu.findItem(R.id.netGameOptionMenuItemLastTurn);
        MenuItem findItem3 = menu.findItem(R.id.netGameOptionMenuItemJoin);
        am amVar = (am) this.f12a.m;
        if (!(findItem == null || amVar == null)) {
            findItem.setEnabled(amVar.p && amVar.b.A(amVar.e) && amVar.h == 1);
        }
        if (!(findItem2 == null || amVar == null)) {
            if (amVar.h != 1 || amVar.b.C() <= 0) {
                z2 = false;
            } else {
                z2 = true;
            }
            findItem2.setEnabled(z2);
        }
        if (!(findItem3 == null || amVar == null)) {
            ac acVar = this.f12a.k.d.i.f;
            if (amVar.r) {
                int i2 = 0;
                while (true) {
                    if (i2 < acVar.d) {
                        if (acVar.h[i2] == null && acVar.i[i2] == ac.k) {
                            z = true;
                            break;
                        }
                        i2++;
                    } else {
                        z = false;
                        break;
                    }
                }
                int i3 = 0;
                while (true) {
                    if (i3 < acVar.d) {
                        if (acVar.h[i3] == null && acVar.i[i3] == ac.m && this.f12a.k.d.i.d.equals(acVar.j[i3])) {
                            z = false;
                            break;
                        }
                        i3++;
                    } else {
                        break;
                    }
                }
            } else {
                z = false;
            }
            findItem3.setEnabled(z);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    private void a(int i2) {
        if (this.f == null) {
            this.f = new Dialog(this);
            this.f.setContentView((int) R.layout.claimdialog);
            this.f.setTitle(this.f12a.getString(R.string.claim_dialog_title));
            this.j = (Button) this.f.findViewById(R.id.claimButtonCancel);
            this.j.setOnClickListener(this);
            this.i = new Button[14];
            for (int i3 = 0; i3 < this.i.length; i3++) {
                this.i[i3] = new Button(this);
                this.i[i3].setText(Integer.toString(i3));
                this.i[i3].setOnClickListener(this);
            }
        }
        TableLayout tableLayout = (TableLayout) this.f.findViewById(R.id.claimButtonsTable);
        int childCount = tableLayout.getChildCount();
        for (int i4 = 0; i4 < childCount; i4++) {
            ((TableRow) tableLayout.getChildAt(i4)).removeAllViews();
        }
        tableLayout.removeAllViews();
        TableRow tableRow = null;
        for (int i5 = i2; i5 >= 0; i5--) {
            if ((i2 - i5) % 5 == 0) {
                tableRow = new TableRow(this.f12a);
                tableLayout.addView(tableRow);
            }
            tableRow.addView(this.i[i5]);
        }
    }

    /* access modifiers changed from: package-private */
    public final void d() {
        b();
        this.c.invalidate();
    }

    /* access modifiers changed from: package-private */
    public final void e() {
        if (this.f12a.e != null) {
            this.f12a.e.a();
        }
        if (this.f12a.g != null) {
            this.f12a.g.a();
        }
        if (this.f12a.j != null) {
            this.f12a.j.a();
        }
        if (this.f12a.f11a != null) {
            this.f12a.f11a.c();
            return;
        }
        Intent intent = new Intent();
        intent.setClass(this, BiddingActivity.class);
        startActivity(intent);
    }

    private void f() {
        ArrayList arrayList = new ArrayList();
        arrayList.add("Cancel");
        ac acVar = this.f12a.k.d.i.f;
        if (acVar != null) {
            synchronized (acVar) {
                for (int i2 = 0; i2 < acVar.d; i2++) {
                    if (acVar.h[i2] == null && acVar.i[i2] == ac.k) {
                        arrayList.add("Join " + b.g[i2]);
                    }
                }
            }
            this.k = new String[arrayList.size()];
            arrayList.toArray(this.k);
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle((int) R.string.join_next_game);
            builder.setItems(this.k, new ak(this));
            builder.create().show();
        }
    }
}
