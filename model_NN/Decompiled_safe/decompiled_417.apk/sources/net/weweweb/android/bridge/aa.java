package net.weweweb.android.bridge;

import a.h;
import a.i;
import a.j;
import java.util.LinkedList;
import java.util.ListIterator;

/* compiled from: ProGuard */
final class aa {

    /* renamed from: a  reason: collision with root package name */
    aq f29a;
    LinkedList b = new LinkedList();
    i c = new i(80, 80, 80);

    public aa(aq aqVar) {
        this.f29a = aqVar;
    }

    private void a(int i, int i2, int i3) {
        z a2 = a(i);
        if (a2 != null) {
            a2.g.remove(this.f29a.h[i2]);
            if (i2 != this.f29a.l) {
                this.f29a.f44a.i(">> " + this.f29a.h[i2].d + " left channel [" + a2.b + "].");
                if (i3 != a2.f) {
                    a2.a(i3);
                    this.f29a.f44a.i(">> Channel [" + a2.b + "] is now controlled by " + this.f29a.h[i3].d + ".");
                    return;
                }
                return;
            }
            this.f29a.f44a.i(">> Left channel [" + a2.b + "].");
            this.b.remove(a2);
            this.f29a.f44a.c("[" + a2.b + "]");
        }
    }

    /* access modifiers changed from: package-private */
    public final z a(int i) {
        ListIterator listIterator = this.b.listIterator(0);
        while (listIterator.hasNext()) {
            z zVar = (z) listIterator.next();
            if (zVar.f4a == i) {
                return zVar;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public final z a(String str) {
        ListIterator listIterator = this.b.listIterator(0);
        while (listIterator.hasNext()) {
            z zVar = (z) listIterator.next();
            if (zVar.b.equals(str)) {
                return zVar;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        String str;
        String str2;
        byte readByte = this.f29a.c.readByte();
        this.f29a.c.readByte();
        this.c.a(this.f29a.c);
        if (readByte == 2) {
            this.f29a.f44a.f(this.c.f[0]);
        } else if (readByte == 3) {
            int i = this.c.d[0];
            String str3 = this.c.f[0];
            int i2 = this.c.d[1];
            z a2 = a(str3);
            if (a2 == null) {
                this.b.add(new z(i, str3, this.f29a.h[i2]));
                this.f29a.f44a.a("[" + str3 + "]");
            } else {
                a2.g.add(this.f29a.h[i2]);
            }
            if (i2 != this.f29a.l) {
                this.f29a.f44a.i(">> " + this.f29a.h[i2].d + " joined channel [" + str3 + "].");
            } else {
                this.f29a.f44a.i(">> Joined channel [" + str3 + "].");
            }
        } else if (readByte == 5) {
            a(this.c.d[0], this.c.d[1], this.c.d[2]);
        } else if (readByte == 6) {
            z a3 = a(this.c.d[0]);
            if (a3 != null) {
                str = a3.b;
            } else {
                str = "?";
            }
            if (this.f29a.h[this.c.d[1]] != null) {
                str2 = this.f29a.h[this.c.d[1]].d;
            } else {
                str2 = "?";
            }
            this.f29a.f44a.a(str, str2, this.c.f[0]);
        } else if (readByte == 7) {
            try {
                this.f29a.f44a.f91a.a(17, this.c.d[0], this.c.d[1], new String[]{this.c.f[0], this.c.f[1]});
            } catch (Exception e) {
            }
        } else if (readByte == 8) {
            ab abVar = this.f29a.h[this.c.d[0]];
            if (abVar != null) {
                this.f29a.f44a.i(">> " + abVar.d + " refuses to join [" + this.c.f[0] + "].");
            }
        } else if (readByte == 9) {
            ab abVar2 = this.f29a.h[this.c.d[0]];
            ab abVar3 = this.f29a.h[this.c.d[1]];
            if (this.f29a.i == abVar2) {
                this.f29a.f44a.h("[Whisper] to [" + abVar3.d + "]: " + this.c.f[0]);
            } else {
                this.f29a.f44a.h("[Whisper] [" + abVar2.d + "]: " + this.c.f[0]);
            }
        } else if (readByte == 10) {
            z a4 = a(this.c.d[0]);
            if (a4 != null) {
                a4.f = this.c.d[1];
                a4.g.clear();
                for (int i3 = 2; i3 < this.c.c; i3++) {
                    if (this.f29a.h[this.c.d[i3]] != null) {
                        a4.g.add(this.f29a.h[this.c.d[i3]]);
                    }
                }
            }
        } else if (readByte == 11) {
            StringBuffer stringBuffer = new StringBuffer("Channel list:");
            for (int i4 = 0; i4 < this.c.e; i4++) {
                stringBuffer.append((stringBuffer.indexOf(": ") >= 0 ? "," : "") + " " + this.c.f[i4]);
            }
            stringBuffer.append(".");
            this.f29a.f44a.i(stringBuffer.toString());
        } else if (readByte == 12) {
            this.f29a.f44a.a(this.c.f[0], this.c.f[1]);
        } else if (readByte == 13) {
            try {
                this.f29a.f44a.i("[" + j.d[this.f29a.j[this.c.d[0]].c] + "]: " + this.c.f[0]);
            } catch (Exception e2) {
            }
        }
    }

    public final void a(int i, String str, String str2) {
        String str3;
        if (str == null || str.equals("")) {
            this.f29a.f44a.f("Invalid channel name!");
        } else if (a(str) != null) {
            this.f29a.f44a.f("Already joined channel!");
        } else {
            if (str2 == null) {
                str3 = "";
            } else {
                str3 = str2;
            }
            this.c.a();
            this.c.a(i);
            this.c.a(str);
            this.c.a(str3);
            this.f29a.a(h.a(38, (byte) 1, (byte) this.f29a.l, this.c));
        }
    }
}
