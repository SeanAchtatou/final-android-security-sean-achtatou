package net.weweweb.android.bridge;

import android.app.ProgressDialog;
import android.content.DialogInterface;

/* compiled from: ProGuard */
final class f implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ BidRuleActivity f82a;

    f(BidRuleActivity bidRuleActivity) {
        this.f82a = bidRuleActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f82a.b = new ProgressDialog(this.f82a);
        this.f82a.b.setProgressStyle(1);
        this.f82a.b.setMessage("Downloading...");
        this.f82a.b.setCancelable(false);
        this.f82a.b.show();
        this.f82a.b.setMax(this.f82a.d);
        new Thread(this.f82a).start();
        dialogInterface.dismiss();
    }
}
