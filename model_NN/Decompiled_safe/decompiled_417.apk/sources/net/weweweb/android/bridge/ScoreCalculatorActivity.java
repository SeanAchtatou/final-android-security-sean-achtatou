package net.weweweb.android.bridge;

import a.b;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.ToggleButton;
import net.weweweb.android.free.bridge.R;

/* compiled from: ProGuard */
public class ScoreCalculatorActivity extends Activity implements View.OnClickListener {
    static byte b = 0;
    static byte c = 1;
    static byte d = 3;
    static byte e = 4;
    static byte f = 5;
    private static byte r = 0;
    private static byte s = 1;

    /* renamed from: a  reason: collision with root package name */
    byte f22a;
    TextView g;
    TextView h;
    TextView i;
    TextView j;
    int k = 0;
    int l = 0;
    byte m = 0;
    byte n = 0;
    boolean o = false;
    boolean p = false;
    boolean q = false;
    private byte t = 0;
    private StringBuilder u = new StringBuilder();
    private int v = 0;
    private int w = 0;
    private int x = 0;

    private void a(int i2) {
        this.w += i2;
        this.x = b.a(Math.abs(this.w));
        b();
    }

    private void a(boolean z, byte b2, byte b3, byte b4) {
        this.v = Math.abs(b.b(z, b2, b3, b4));
        this.h.setText(Integer.toString(this.v));
    }

    private void a() {
        this.g.setText(BridgeApp.a(this.u.toString()));
        this.h.setText(Integer.toString(this.v));
    }

    private void b() {
        this.i.setText(Integer.toString(this.w));
        this.j.setText(Integer.toString(this.x));
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.scorecalculator);
        this.g = (TextView) findViewById(R.id.scoreCalculatorContractDisplay);
        this.h = (TextView) findViewById(R.id.scoreCalculatorScoreDisplay);
        this.i = (TextView) findViewById(R.id.scoreCalculatorMemoryDisplay);
        this.j = (TextView) findViewById(R.id.scoreCalculatorIMPDisplay);
        findViewById(R.id.btnScoreCalculatorVul).setOnClickListener(this);
        ((ToggleButton) findViewById(R.id.btnScoreCalculatorVul)).setChecked(this.o);
        findViewById(R.id.btnScoreCalculatorOne).setOnClickListener(this);
        findViewById(R.id.btnScoreCalculatorTwo).setOnClickListener(this);
        findViewById(R.id.btnScoreCalculatorThree).setOnClickListener(this);
        findViewById(R.id.btnScoreCalculatorFour).setOnClickListener(this);
        findViewById(R.id.btnScoreCalculatorFive).setOnClickListener(this);
        findViewById(R.id.btnScoreCalculatorSix).setOnClickListener(this);
        findViewById(R.id.btnScoreCalculatorSeven).setOnClickListener(this);
        findViewById(R.id.btnScoreCalculatorEight).setOnClickListener(this);
        findViewById(R.id.btnScoreCalculatorNine).setOnClickListener(this);
        findViewById(R.id.btnScoreCalculatorZero).setOnClickListener(this);
        findViewById(R.id.btnScoreCalculatorNT).setOnClickListener(this);
        findViewById(R.id.btnScoreCalculatorSpade).setOnClickListener(this);
        findViewById(R.id.btnScoreCalculatorHeart).setOnClickListener(this);
        findViewById(R.id.btnScoreCalculatorDiamond).setOnClickListener(this);
        findViewById(R.id.btnScoreCalculatorClub).setOnClickListener(this);
        findViewById(R.id.btnScoreCalculatorPlus).setOnClickListener(this);
        findViewById(R.id.btnScoreCalculatorMinus).setOnClickListener(this);
        findViewById(R.id.btnScoreCalculatorDbl).setOnClickListener(this);
        findViewById(R.id.btnScoreCalculatorReDbl).setOnClickListener(this);
        findViewById(R.id.btnScoreCalculatorEqual).setOnClickListener(this);
        findViewById(R.id.btnScoreCalculatorMP).setOnClickListener(this);
        findViewById(R.id.btnScoreCalculatorMM).setOnClickListener(this);
        findViewById(R.id.btnScoreCalculatorMC).setOnClickListener(this);
        findViewById(R.id.btnScoreCalculatorC).setOnClickListener(this);
        b();
    }

    public void onClick(View view) {
        if (view == findViewById(R.id.btnScoreCalculatorOne)) {
            if (this.f22a == b) {
                b(1);
            } else if (this.f22a == e) {
                d(1);
            } else if (this.f22a == f) {
                c(1);
            }
        } else if (view == findViewById(R.id.btnScoreCalculatorTwo)) {
            if (this.f22a == b) {
                b(2);
            } else if (this.f22a == e) {
                d(2);
            } else if (this.f22a == f) {
                c(2);
            }
        } else if (view == findViewById(R.id.btnScoreCalculatorThree)) {
            if (this.f22a == b) {
                b(3);
            } else if (this.f22a == e) {
                d(3);
            } else if (this.f22a == f) {
                c(3);
            }
        } else if (view == findViewById(R.id.btnScoreCalculatorFour)) {
            if (this.f22a == b) {
                b(4);
            } else if (this.f22a == e) {
                d(4);
            } else if (this.f22a == f) {
                c(4);
            }
        } else if (view == findViewById(R.id.btnScoreCalculatorFive)) {
            if (this.f22a == b) {
                b(5);
            } else if (this.f22a == e) {
                d(5);
            } else if (this.f22a == f) {
                c(5);
            }
        } else if (view == findViewById(R.id.btnScoreCalculatorSix)) {
            if (this.f22a == b) {
                b(6);
            } else if (this.f22a == e) {
                d(6);
            } else if (this.f22a == f) {
                c(6);
            }
        } else if (view == findViewById(R.id.btnScoreCalculatorSeven)) {
            if (this.f22a == b) {
                b(7);
            } else if (this.f22a == f) {
                c(7);
            }
        } else if (view == findViewById(R.id.btnScoreCalculatorEight)) {
            if (this.f22a == f) {
                c(8);
            }
        } else if (view == findViewById(R.id.btnScoreCalculatorNine)) {
            if (this.f22a == f) {
                c(9);
            }
        } else if (view == findViewById(R.id.btnScoreCalculatorZero)) {
            if (this.f22a == e) {
                d(0);
            } else if (this.f22a == f) {
                c(0);
            }
        } else if (view == findViewById(R.id.btnScoreCalculatorNT)) {
            if (this.f22a == c) {
                this.u.append("NT");
                this.m = 4;
                a();
                this.f22a = d;
            }
        } else if (view == findViewById(R.id.btnScoreCalculatorNT)) {
            if (this.f22a == c) {
                this.u.append("NT");
                this.m = 4;
                a();
                this.f22a = d;
            }
        } else if (view == findViewById(R.id.btnScoreCalculatorSpade)) {
            if (this.f22a == c) {
                this.m = 3;
                this.u.append(b.e[this.m]);
                a();
                this.f22a = d;
            }
        } else if (view == findViewById(R.id.btnScoreCalculatorHeart)) {
            if (this.f22a == c) {
                this.m = 2;
                this.u.append(b.e[this.m]);
                a();
                this.f22a = d;
            }
        } else if (view == findViewById(R.id.btnScoreCalculatorDiamond)) {
            if (this.f22a == c) {
                this.m = 1;
                this.u.append(b.e[this.m]);
                a();
                this.f22a = d;
            }
        } else if (view == findViewById(R.id.btnScoreCalculatorClub)) {
            if (this.f22a == c) {
                this.m = 0;
                this.u.append(b.e[this.m]);
                a();
                this.f22a = d;
            }
        } else if (view == findViewById(R.id.btnScoreCalculatorPlus)) {
            if (this.f22a == d) {
                this.p = true;
                this.u.append("+");
                a();
                this.f22a = e;
            }
        } else if (view == findViewById(R.id.btnScoreCalculatorMinus)) {
            if (this.f22a == d) {
                this.p = true;
                this.u.append("-");
                a();
                this.f22a = f;
            }
        } else if (view == findViewById(R.id.btnScoreCalculatorDbl)) {
            if (this.f22a == d && this.n == 0) {
                this.n = 97;
                this.u.append("X");
                a();
            }
        } else if (view == findViewById(R.id.btnScoreCalculatorReDbl)) {
            if (this.f22a == d && this.n == 0) {
                this.n = 98;
                this.u.append("XX");
                a();
            }
        } else if (view == findViewById(R.id.btnScoreCalculatorEqual)) {
            if (this.f22a == d || this.f22a == e || this.f22a == f) {
                byte b2 = this.n;
                a(this.o, (byte) ((this.k * 10) + this.m), b2, (byte) (this.k + 6 + this.l));
                this.f22a = b;
                this.q = true;
            }
        } else if (view == findViewById(R.id.btnScoreCalculatorVul)) {
            this.o = ((ToggleButton) view).isChecked();
        } else if (view == findViewById(R.id.btnScoreCalculatorMP)) {
            a(this.v);
            b();
        } else if (view == findViewById(R.id.btnScoreCalculatorMM)) {
            a(-this.v);
            b();
        } else if (view == findViewById(R.id.btnScoreCalculatorMC)) {
            a(-this.w);
            b();
        } else if (view == findViewById(R.id.btnScoreCalculatorC)) {
            this.u.setLength(0);
            this.v = 0;
            this.k = 0;
            this.l = 0;
            this.n = 0;
            this.p = false;
            a();
            this.f22a = b;
        }
    }

    private void b(int i2) {
        if (this.q) {
            this.u.setLength(0);
            this.v = 0;
            this.h.setText(Integer.toString(0));
            this.n = 0;
            this.l = 0;
            this.p = false;
        }
        this.k = i2;
        this.u.append(Integer.toString(this.k));
        this.g.setText(this.u.toString());
        this.f22a = c;
        this.q = false;
    }

    private void c(int i2) {
        int i3;
        if (this.l >= -1 && i2 - (this.l * 10) <= (i3 = this.k + 6)) {
            this.l = (this.l * 10) - i2;
            this.u.append(Integer.toString(i2));
            a();
            if (this.l != -1 || i3 < 10) {
                a(this.o, (byte) ((this.k * 10) + this.m), this.n, (byte) (this.k + 6 + this.l));
                this.f22a = b;
                this.q = true;
            }
        }
    }

    private void d(int i2) {
        if (this.l == 0 && i2 <= 7 - this.k) {
            this.l = i2;
            this.u.append(Integer.toString(this.l));
            a();
            byte b2 = this.n;
            a(this.o, (byte) ((this.k * 10) + this.m), b2, (byte) (this.k + 6 + this.l));
            this.f22a = b;
            this.q = true;
        }
    }
}
