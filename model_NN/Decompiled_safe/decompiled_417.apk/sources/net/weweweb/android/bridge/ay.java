package net.weweweb.android.bridge;

import a.c;
import android.content.DialogInterface;

/* compiled from: ProGuard */
final class ay implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RecordBrowserActivity f52a;

    ay(RecordBrowserActivity recordBrowserActivity) {
        this.f52a = recordBrowserActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        RecordBrowserActivity recordBrowserActivity = this.f52a;
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < recordBrowserActivity.b.size()) {
                recordBrowserActivity.e.a(((c) recordBrowserActivity.b.get(i3)).b, ((c) recordBrowserActivity.b.get(i3)).c);
                i2 = i3 + 1;
            } else {
                this.f52a.b();
                dialogInterface.dismiss();
                return;
            }
        }
    }
}
