package net.weweweb.android.bridge;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;

/* compiled from: ProGuard */
final class bw implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ToolMenuActivity f76a;

    bw(ToolMenuActivity toolMenuActivity) {
        this.f76a = toolMenuActivity;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        if (i == 0) {
            Intent intent = new Intent();
            intent.setClass(this.f76a, RecordBrowserActivity.class);
            this.f76a.startActivity(intent);
        } else if (i == 1) {
            Intent intent2 = new Intent();
            intent2.setClass(this.f76a, ScoreCalculatorActivity.class);
            this.f76a.startActivity(intent2);
        }
    }
}
