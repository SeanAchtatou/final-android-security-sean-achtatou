package net.weweweb.android.bridge;

import android.content.DialogInterface;

/* compiled from: ProGuard */
final class ah implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ MessageActivity f36a;

    ah(MessageActivity messageActivity) {
        this.f36a = messageActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        String str = this.f36a.b[i];
        if (!"Cancel".equals(str)) {
            if (str.startsWith("[")) {
                this.f36a.f17a.k.a(this.f36a.f17a.k.d.s.a(str.substring(1, str.length() - 1)));
                return;
            }
            this.f36a.f17a.k.a(this.f36a.f17a.k.d.a(str));
        }
    }
}
