package net.weweweb.android.bridge;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/* compiled from: ProGuard */
final class x extends SQLiteOpenHelper {
    x(Context context) {
        super(context, "bridge_game.db", (SQLiteDatabase.CursorFactory) null, 1);
    }

    public final void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("CREATE TABLE bridge_deck (deck_set integer NOT NULL DEFAULT -1000,deck_no integer NOT NULL,spade character(13),heart character(13),diamond character(13),club character(13),dealer smallint,vul smallint,game_played smallint DEFAULT 0,create_datetime timestamp,CONSTRAINT bridge_deck_pk PRIMARY KEY (deck_set, deck_no));");
        sQLiteDatabase.execSQL("CREATE TABLE bridge_dup_result (deck_set integer NOT NULL DEFAULT -1000,deck_no integer NOT NULL,play_datetime timestamp NOT NULL,player0 character varying(20),player1 character varying(20),player2 character varying(20),player3 character varying(20),declarer smallint,contract smallint,double_info smallint,win_tricks smallint,score smallint,ns_mp smallint,ew_mp smallint,bid character varying(255),play character varying(255),CONSTRAINT bridge_dup_result_pk PRIMARY KEY (deck_set, deck_no, play_datetime))");
    }

    public final void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        Log.w("BridgeGameDBAdapter", "Upgrading database from version " + i + " to " + i2 + ", which will destroy all old data");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS bridge_deck");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS bridge_dup_result");
        onCreate(sQLiteDatabase);
    }
}
