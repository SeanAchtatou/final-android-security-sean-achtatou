package net.weweweb.android.common;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;

/* compiled from: ProGuard */
public final class c {
    public static void a(Canvas canvas, int i, int i2, int i3, int i4, int i5, int i6, Paint paint) {
        Path path = new Path();
        path.moveTo((float) i, (float) i2);
        path.lineTo((float) (i + i3), (float) (i2 + i4));
        path.lineTo((float) (i + i5), (float) (i2 + i6));
        path.lineTo((float) i, (float) i2);
        canvas.drawPath(path, paint);
    }
}
