package net.weweweb.android.common;

import java.text.SimpleDateFormat;

/* compiled from: ProGuard */
public final class b {
    public static long a(String str) {
        String str2;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        if (str.length() == 21) {
            str2 = str + "00";
        } else {
            str2 = str.length() == 22 ? str + "0" : str;
        }
        try {
            return simpleDateFormat.parse(str2).getTime();
        } catch (Exception e) {
            return 0;
        }
    }
}
