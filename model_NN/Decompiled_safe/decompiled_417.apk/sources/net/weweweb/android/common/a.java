package net.weweweb.android.common;

import a.d;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import net.weweweb.android.free.bridge.R;

/* compiled from: ProGuard */
public final class a extends d {
    public static int[] k = {R.drawable.card_0_sa, R.drawable.card_0_s2, R.drawable.card_0_s3, R.drawable.card_0_s4, R.drawable.card_0_s5, R.drawable.card_0_s6, R.drawable.card_0_s7, R.drawable.card_0_s8, R.drawable.card_0_s9, R.drawable.card_0_st, R.drawable.card_0_sj, R.drawable.card_0_sq, R.drawable.card_0_sk, R.drawable.card_0_ha, R.drawable.card_0_h2, R.drawable.card_0_h3, R.drawable.card_0_h4, R.drawable.card_0_h5, R.drawable.card_0_h6, R.drawable.card_0_h7, R.drawable.card_0_h8, R.drawable.card_0_h9, R.drawable.card_0_ht, R.drawable.card_0_hj, R.drawable.card_0_hq, R.drawable.card_0_hk, R.drawable.card_0_ca, R.drawable.card_0_c2, R.drawable.card_0_c3, R.drawable.card_0_c4, R.drawable.card_0_c5, R.drawable.card_0_c6, R.drawable.card_0_c7, R.drawable.card_0_c8, R.drawable.card_0_c9, R.drawable.card_0_ct, R.drawable.card_0_cj, R.drawable.card_0_cq, R.drawable.card_0_ck, R.drawable.card_0_da, R.drawable.card_0_d2, R.drawable.card_0_d3, R.drawable.card_0_d4, R.drawable.card_0_d5, R.drawable.card_0_d6, R.drawable.card_0_d7, R.drawable.card_0_d8, R.drawable.card_0_d9, R.drawable.card_0_dt, R.drawable.card_0_dj, R.drawable.card_0_dq, R.drawable.card_0_dk};
    public static Bitmap[] l = new Bitmap[52];
    public static Bitmap[] m = new Bitmap[52];
    public static Bitmap n;
    public static Bitmap o;
    public static Bitmap[] p = new Bitmap[5];
    public static int q = 54;
    public static int r = 72;
    public static int[][] s = {new int[]{14, 14}, new int[]{20, 20}};
    public static final int[] t = {54, 71};
    public static final int[] u = {72, 96};
    public static int[] v = {14, 14};
    static int w = 0;
    private static int y = 14;
    private static int z = 160;
    Context x;

    public a() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public a(Context context) {
        this.x = context;
        if (n == null) {
            for (int i = 0; i < l.length; i++) {
                l[i] = BitmapFactory.decodeResource(this.x.getResources(), k[i]);
            }
            n = BitmapFactory.decodeResource(this.x.getResources(), R.drawable.card_0_back1);
            for (int i2 = 0; i2 < l.length; i2++) {
                l[i2] = BitmapFactory.decodeResource(this.x.getResources(), k[i2]);
            }
            Matrix matrix = new Matrix();
            matrix.reset();
            matrix.setRotate(90.0f, 0.0f, 0.0f);
            for (int i3 = 0; i3 < m.length; i3++) {
                m[i3] = Bitmap.createBitmap(l[i3], 0, 0, l[i3].getWidth(), l[i3].getHeight(), matrix, true);
            }
            o = Bitmap.createBitmap(n, 0, 0, n.getWidth(), n.getHeight(), matrix, true);
        }
    }

    public static int c() {
        return y;
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0021  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x003f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(android.graphics.Canvas r9, byte[] r10, int r11, int r12, boolean r13, byte[] r14) {
        /*
            r8 = this;
            r7 = 0
            r0 = r12
            r3 = r11
            r6 = r7
        L_0x0004:
            int r1 = r10.length
            if (r6 >= r1) goto L_0x004e
            byte r1 = r10[r6]
            if (r1 < 0) goto L_0x0051
            byte r1 = r10[r6]
            byte[] r2 = r8.h
            int r2 = r2.length
            if (r1 >= r2) goto L_0x0051
            if (r14 == 0) goto L_0x004f
            r1 = r7
        L_0x0015:
            int r2 = r14.length
            if (r1 >= r2) goto L_0x004f
            byte r2 = r14[r1]
            byte r4 = r10[r6]
            if (r2 != r4) goto L_0x003b
            r1 = 1
        L_0x001f:
            if (r1 != 0) goto L_0x003f
            r4 = r12
        L_0x0022:
            byte r2 = r10[r6]
            r0 = r8
            r1 = r9
            r5 = r13
            r0.a(r1, r2, r3, r4, r5)
            int[][] r0 = net.weweweb.android.common.a.s
            int r1 = net.weweweb.android.common.a.w
            r0 = r0[r1]
            r0 = r0[r7]
            int r0 = r0 + r3
            r1 = r0
            r0 = r4
        L_0x0035:
            int r2 = r6 + 1
            byte r2 = (byte) r2
            r3 = r1
            r6 = r2
            goto L_0x0004
        L_0x003b:
            int r1 = r1 + 1
            byte r1 = (byte) r1
            goto L_0x0015
        L_0x003f:
            int r1 = net.weweweb.android.common.a.z
            if (r0 >= r1) goto L_0x0048
            int r0 = net.weweweb.android.common.a.y
            int r0 = r0 + r12
            r4 = r0
            goto L_0x0022
        L_0x0048:
            int r0 = net.weweweb.android.common.a.y
            int r0 = r12 - r0
            r4 = r0
            goto L_0x0022
        L_0x004e:
            return
        L_0x004f:
            r1 = r7
            goto L_0x001f
        L_0x0051:
            r1 = r3
            goto L_0x0035
        */
        throw new UnsupportedOperationException("Method not decompiled: net.weweweb.android.common.a.a(android.graphics.Canvas, byte[], int, int, boolean, byte[]):void");
    }

    public final void a(Canvas canvas, byte b, int i, int i2, boolean z2) {
        if (b >= 0 && b < this.h.length) {
            if (z2) {
                if (l[b] == null) {
                    canvas.drawBitmap(n, (float) i, (float) i2, (Paint) null);
                } else {
                    canvas.drawBitmap(l[b], (float) i, (float) i2, (Paint) null);
                }
            } else if (n != null) {
                canvas.drawBitmap(n, (float) i, (float) i2, (Paint) null);
            }
        }
    }

    private void a(Canvas canvas, byte b, int i, int i2, byte b2, boolean z2) {
        if (b >= 0 && b < this.h.length) {
            Matrix matrix = new Matrix();
            matrix.reset();
            matrix.setTranslate((float) i, (float) i2);
            matrix.postScale(1.0f, 1.0f, (float) i, (float) i2);
            Paint paint = new Paint(5);
            if (b2 == 0) {
                if (z2) {
                    if (l[b] == null) {
                        canvas.drawBitmap(n, matrix, paint);
                    } else {
                        canvas.drawBitmap(l[b], matrix, paint);
                    }
                } else if (n != null) {
                    canvas.drawBitmap(n, matrix, paint);
                }
            } else if (z2) {
                if (m[b] == null) {
                    canvas.drawBitmap(o, matrix, null);
                } else {
                    canvas.drawBitmap(m[b], matrix, null);
                }
            } else if (n != null) {
                canvas.drawBitmap(o, matrix, null);
            }
        }
    }

    public final void a(Canvas canvas, byte[] bArr, int i, int i2, int i3, byte b, boolean z2) {
        int i4;
        int i5;
        int i6 = i;
        int i7 = i2;
        for (byte b2 = 0; b2 < bArr.length; b2 = (byte) (b2 + 1)) {
            if (bArr[b2] >= 0 && bArr[b2] < this.h.length) {
                if (b == 0) {
                    a(canvas, bArr[b2], i6, i7, b, z2);
                    i4 = i6 + i3;
                    i5 = i7;
                } else if (b == 1) {
                    a(canvas, bArr[b2], i6, i7, b, z2);
                    i5 = i7 + i3;
                    i4 = i6;
                } else if (b == 2) {
                    a(canvas, bArr[b2], i6, i7, b, z2);
                    i5 = i7 - i3;
                    i4 = i6;
                }
                i6 = i4;
                i7 = i5;
            }
            i4 = i6;
            i5 = i7;
            i6 = i4;
            i7 = i5;
        }
    }

    public static void a(int i) {
        y = i;
    }

    public static void b(int i) {
        z = i;
    }

    public static void c(int i) {
        s[w][0] = i;
    }
}
