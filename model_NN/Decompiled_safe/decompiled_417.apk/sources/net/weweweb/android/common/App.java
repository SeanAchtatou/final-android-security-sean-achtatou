package net.weweweb.android.common;

import android.app.Application;
import android.content.SharedPreferences;

/* compiled from: ProGuard */
public class App extends Application {
    public final void a(String str, boolean z) {
        SharedPreferences.Editor edit = getApplicationContext().getSharedPreferences("PrefFile", 0).edit();
        edit.putBoolean(str, z);
        edit.commit();
    }

    public final void a(String str, int i) {
        SharedPreferences.Editor edit = getApplicationContext().getSharedPreferences("PrefFile", 0).edit();
        edit.putInt(str, i);
        edit.commit();
    }

    public final void a(String str, String str2) {
        SharedPreferences.Editor edit = getApplicationContext().getSharedPreferences("PrefFile", 0).edit();
        edit.putString(str, str2);
        edit.commit();
    }
}
