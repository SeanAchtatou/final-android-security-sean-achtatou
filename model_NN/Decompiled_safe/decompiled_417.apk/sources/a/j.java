package a;

import java.net.URL;
import java.util.Random;

/* compiled from: ProGuard */
public final class j {

    /* renamed from: a  reason: collision with root package name */
    public static boolean f8a = true;
    public static long b = 120000;
    public static String c = "def";
    public static final String[] d = {"Xword", "Bridge", "Apple", "Big2", "CChess", "Marble", "Five", "Mahjong", "Wisk", "LastCard", "GBridge", "BBoss", "Plane", "Army", "Wetris", "Connect4", "Checkers", "Chess", "SkyDog", "Snooker", "Gammon", "Trivia VS(B)", "WarExp(A)", "Draw", "Go", "ShowHand(A)", "IBridge(A)"};
    public static final byte[] e = {0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1};
    public static final byte[] f = {26, 25, 24, 23, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22};
    public static int g = 500;
    public static int h = 360;
    public static Random i = new Random();
    public static StringBuffer j = new StringBuffer();
    public static URL k = null;
    public static boolean l = true;
    public static boolean m = false;
    public static String[][] n = {new String[]{":)", "icon_smile.gif"}, new String[]{":(", "icon_sad.gif"}, new String[]{":D", "icon_biggrin.gif"}, new String[]{":?", "icon_confused.gif"}, new String[]{":o", "icon_surprised.gif"}, new String[]{":x", "icon_mad.gif"}, new String[]{":P", "icon_razz.gif"}, new String[]{":lol:", "icon_lol.gif"}, new String[]{":cool:", "icon_cool.gif"}, new String[]{":evil:", "icon_evil.gif"}, new String[]{":idea:", "icon_idea.gif"}, new String[]{":oops:", "icon_redface.gif"}, new String[]{":roll:", "icon_rolleyes.gif"}, new String[]{":shock:", "icon_eek.gif"}, new String[]{":mrgreen:", "icon_mrgreen.gif"}};
    public static String o = "AU,BR,CA,CD,CH,CG,CN,DE,EG,ES,EU,FR,GB,GR,HK,ID,IN,IT,JP,KR,KP,MO,NZ,PH,PL,PT,RU,SE,SG,TW,US,ZA,--";

    public static byte a(byte b2) {
        switch (b2) {
            case 0:
                return 4;
            case 1:
                return 4;
            case 2:
            case 4:
            case 6:
            case 13:
            case 15:
            case 16:
            case 17:
            case 19:
            case 20:
            case 24:
                return 2;
            case 3:
                return 4;
            case 5:
                return 6;
            case 7:
                return 4;
            case 8:
                return 6;
            case 9:
                return 4;
            case 10:
                return 4;
            case 11:
                return 4;
            case 12:
                return 4;
            case 14:
                return 4;
            case 18:
                return 4;
            case 21:
                return 6;
            case 22:
                return 6;
            case 23:
                return 6;
            case 25:
                return 8;
            case 26:
                return 5;
            default:
                return 0;
        }
    }

    public static byte b(byte b2) {
        switch (b2) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
            case 22:
            case 24:
            case 25:
            case 26:
                return 1;
            case 21:
                return 2;
            case 23:
                return 2;
            default:
                return 0;
        }
    }
}
