package a;

/* compiled from: ProGuard */
public final class g {
    public static void a(byte[] bArr, byte[] bArr2) {
        try {
            System.arraycopy(bArr, 0, bArr2, 0, bArr.length);
        } catch (Exception e) {
        }
    }

    public static void a(byte[][] bArr, byte[][] bArr2) {
        for (int i = 0; i < bArr.length; i++) {
            a(bArr[i], bArr2[i]);
        }
    }

    public static void a(int i, String str) {
        if (f.e >= i) {
            System.out.println("Debug> " + str);
        }
    }

    public static void a(int i) {
        try {
            Thread.currentThread();
            Thread.sleep((long) i);
        } catch (InterruptedException e) {
        }
    }

    public static void a(byte[] bArr, int i, String str) {
        System.out.print(str);
        for (int i2 = 0; i2 < i; i2++) {
            System.out.print(((int) bArr[i2]) + ",");
        }
        System.out.println();
    }

    public static int a(int[] iArr) {
        int i = 0;
        if (iArr == null) {
            return -1;
        }
        int i2 = iArr[0];
        for (int i3 = 1; i3 < iArr.length; i3++) {
            if (iArr[i3] > i2) {
                i2 = iArr[i3];
                i = i3;
            }
        }
        return i;
    }

    public static int b(int i) {
        if (i <= 0) {
            return 0;
        }
        return Math.abs(j.i.nextInt()) % i;
    }

    public static int a(byte[] bArr, byte b) {
        for (int i = 0; i < bArr.length; i++) {
            if (bArr[i] == b) {
                return i;
            }
        }
        return -1;
    }

    public static int a(int[] iArr, int i) {
        for (int i2 = 0; i2 < iArr.length; i2++) {
            if (iArr[i2] == i) {
                return i2;
            }
        }
        return -1;
    }

    public static void b(byte[] bArr, byte b) {
        for (int i = 0; i < bArr.length; i++) {
            bArr[i] = b;
        }
    }

    public static void b(int[] iArr, int i) {
        for (int i2 = 0; i2 < iArr.length; i2++) {
            iArr[i2] = i;
        }
    }

    public static void a(byte[][] bArr) {
        for (int i = 0; i < bArr.length; i++) {
            for (int i2 = 0; i2 < bArr[i].length; i2++) {
                bArr[i][i2] = -1;
            }
        }
    }

    public static String a(String str, String str2) {
        try {
            return str.replace("%1%", str2);
        } catch (Exception e) {
            return e.toString();
        }
    }

    public static String a(String str, String str2, String str3) {
        try {
            return a(str, str2).replace("%2%", str3);
        } catch (Exception e) {
            return e.toString();
        }
    }

    public static String a(String str, String str2, String str3, String str4) {
        try {
            return a(str, str2, str3).replace("%3%", str4);
        } catch (Exception e) {
            return e.toString();
        }
    }

    public static void c(int[] iArr, int i) {
        for (int i2 = 0; i2 < i; i2++) {
            int b = b(i);
            if (b != i2) {
                int i3 = iArr[b];
                iArr[b] = iArr[i2];
                iArr[i2] = i3;
            }
        }
    }

    public static void a(byte[] bArr) {
        for (int i = 1; i < bArr.length; i++) {
            int i2 = i;
            while (i2 > 0 && bArr[i2] > bArr[i2 - 1]) {
                byte b = bArr[i2];
                bArr[i2] = bArr[i2 - 1];
                bArr[i2 - 1] = b;
                i2--;
            }
        }
    }

    public static void a(byte[] bArr, int i, boolean z, byte[] bArr2) {
        byte b;
        byte b2;
        for (int i2 = 1; i2 < i + 1; i2++) {
            if (!z) {
                for (int i3 = i2; i3 > 0; i3--) {
                    if (bArr[i3] < 0) {
                        b = bArr[i3];
                    } else {
                        b = bArr2[bArr[i3]];
                    }
                    if (bArr[i3 - 1] < 0) {
                        b2 = bArr[i3 - 1];
                    } else {
                        b2 = bArr2[bArr[i3 - 1]];
                    }
                    if (b <= b2) {
                        break;
                    }
                    byte b3 = bArr[i3];
                    bArr[i3] = bArr[i3 - 1];
                    bArr[i3 - 1] = b3;
                }
            } else {
                for (int i4 = i2; i4 > 0; i4--) {
                    if ((bArr[i4] < 0 ? bArr[i4] : bArr2[bArr[i4]]) >= (bArr[i4 - 1] < 0 ? bArr[i4 - 1] : bArr2[bArr[i4 - 1]])) {
                        break;
                    }
                    byte b4 = bArr[i4];
                    bArr[i4] = bArr[i4 - 1];
                    bArr[i4 - 1] = b4;
                }
            }
        }
    }

    public static String a(String str) {
        if (str == null) {
            return null;
        }
        StringBuffer stringBuffer = new StringBuffer(str);
        while (true) {
            int indexOf = stringBuffer.indexOf("  ");
            if (indexOf < 0) {
                return stringBuffer.toString();
            }
            stringBuffer.delete(indexOf, indexOf + 1);
        }
    }

    public static boolean a(int i, int i2, int i3, int i4, int i5, int i6) {
        if (i < i3 || i > i3 + i5 || i2 < i4 || i2 > i4 + i6) {
            return false;
        }
        return true;
    }
}
