package a;

import java.io.DataInputStream;

/* compiled from: ProGuard */
public final class i {

    /* renamed from: a  reason: collision with root package name */
    public short f7a;
    public byte[] b;
    public short c;
    public int[] d;
    public short e;
    public String[] f;
    public short g;
    private int h;
    private short i;
    private short j;
    private short k;
    private short l;
    private int m;
    private int n;

    public i() {
        this(80, 80, 80);
    }

    public i(int i2, int i3, int i4) {
        this.m = 512;
        this.n = 64;
        this.b = new byte[i2];
        this.d = new int[i3];
        this.f = new String[i4];
        a();
    }

    public final void a() {
        this.g = 0;
        this.e = 0;
        this.c = 0;
        this.f7a = 0;
    }

    public final void a(DataInputStream dataInputStream) {
        this.f7a = dataInputStream.readShort();
        if (this.f7a > this.b.length && this.f7a < this.m) {
            g.a(0, "Extending byte_para from " + this.b.length + " to " + ((int) this.f7a));
            this.b = new byte[this.f7a];
        }
        this.h = 0;
        while (this.h < this.f7a) {
            this.b[this.h] = dataInputStream.readByte();
            this.h++;
        }
        this.c = dataInputStream.readShort();
        if (this.c > this.d.length && this.c < this.m) {
            g.a(0, "Extending int_para from " + this.d.length + " to " + ((int) this.c));
            this.d = new int[this.c];
        }
        this.h = 0;
        while (this.h < this.c) {
            this.d[this.h] = dataInputStream.readInt();
            this.h++;
        }
        this.e = dataInputStream.readShort();
        if (this.e > this.f.length && this.e < this.m) {
            g.a(0, "Extending string_para from " + this.f.length + " to " + ((int) this.e));
            this.f = new String[this.e];
        }
        this.h = 0;
        while (this.h < this.e) {
            this.f[this.h] = dataInputStream.readUTF();
            this.h++;
        }
    }

    public final short a(byte[] bArr, int i2) {
        for (int i3 = 0; i3 < i2; i3++) {
            byte[] bArr2 = this.b;
            short s = this.i;
            this.i = (short) (s + 1);
            bArr[i3] = bArr2[s];
        }
        return this.j;
    }

    public final short a(String[] strArr, int i2) {
        for (int i3 = 0; i3 < i2; i3++) {
            String[] strArr2 = this.f;
            short s = this.k;
            this.k = (short) (s + 1);
            strArr[i3] = strArr2[s];
        }
        return this.k;
    }

    public final boolean b() {
        return c() != 0;
    }

    public final byte c() {
        byte[] bArr = this.b;
        short s = this.i;
        this.i = (short) (s + 1);
        return bArr[s];
    }

    public final int d() {
        int[] iArr = this.d;
        short s = this.j;
        this.j = (short) (s + 1);
        return iArr[s];
    }

    public final long e() {
        int[] iArr = this.d;
        short s = this.j;
        this.j = (short) (s + 1);
        int i2 = iArr[s];
        int[] iArr2 = this.d;
        short s2 = this.j;
        this.j = (short) (s2 + 1);
        int i3 = iArr2[s2];
        long j2 = (0 | ((long) i2)) << 32;
        if (i3 >= 0) {
            return ((long) i3) | j2;
        }
        return ((long) (Integer.MAX_VALUE & i3)) | j2 | 2147483648L;
    }

    public final String f() {
        String[] strArr = this.f;
        short s = this.k;
        this.k = (short) (s + 1);
        return strArr[s];
    }

    public final void a(boolean z) {
        a(z ? (byte) 1 : 0);
    }

    public final void a(byte b2) {
        if (this.f7a == this.b.length && this.f7a < this.m) {
            byte[] bArr = new byte[(this.b.length + this.n)];
            g.a(0, "Extending byte_para from " + this.b.length + " to " + bArr.length);
            System.arraycopy(this.b, 0, bArr, 0, this.b.length);
            this.b = bArr;
        }
        byte[] bArr2 = this.b;
        short s = this.f7a;
        this.f7a = (short) (s + 1);
        bArr2[s] = b2;
    }

    public final void a(int i2) {
        if (this.c == this.d.length && this.c < this.m) {
            int[] iArr = new int[(this.d.length + this.n)];
            g.a(0, "Extending int_para from " + this.d.length + " to " + iArr.length);
            System.arraycopy(this.d, 0, iArr, 0, this.d.length);
            this.d = iArr;
        }
        int[] iArr2 = this.d;
        short s = this.c;
        this.c = (short) (s + 1);
        iArr2[s] = i2;
    }

    public final void a(String str) {
        if (this.e == this.f.length && this.e < this.m) {
            String[] strArr = new String[(this.f.length + this.n)];
            g.a(0, "Extending string_para from " + this.f.length + " to " + strArr.length);
            System.arraycopy(this.f, 0, strArr, 0, this.f.length);
            this.f = strArr;
        }
        if (str == null) {
            String[] strArr2 = this.f;
            short s = this.e;
            this.e = (short) (s + 1);
            strArr2[s] = "";
            return;
        }
        String[] strArr3 = this.f;
        short s2 = this.e;
        this.e = (short) (s2 + 1);
        strArr3[s2] = str;
    }

    public final void g() {
        this.l = 0;
        this.k = 0;
        this.j = 0;
        this.i = 0;
    }

    public final boolean h() {
        if (this.b.length <= 0) {
            return false;
        }
        this.i = 0;
        return true;
    }

    public final boolean i() {
        if (this.d.length <= 0) {
            return false;
        }
        this.j = 0;
        return true;
    }

    public final boolean j() {
        if (this.f.length <= 0) {
            return false;
        }
        this.k = 0;
        return true;
    }
}
