package a;

/* compiled from: ProGuard */
public class d {

    /* renamed from: a  reason: collision with root package name */
    public static char[] f3a = new char[52];
    public static byte[] b = new byte[52];
    public static char[] c = new char[52];
    public static byte[] d = new byte[52];
    public static byte[] e = new byte[52];
    public static String f = "SHCD";
    public static String g = "A23456789TJQK";
    public static final String[] j = {"♦", "♣", "♥", "♠"};
    public byte[] h = new byte[52];
    public byte i;

    public d() {
        c();
        this.i = (byte) (this.h.length - 1);
        for (byte b2 = 0; b2 <= this.i; b2 = (byte) (b2 + 1)) {
            this.h[b2] = b2;
        }
    }

    public static String a(byte b2) {
        if (b2 < 0 || b2 >= c.length) {
            return null;
        }
        return Character.toString(f3a[b2]) + Character.toString(c[b2]);
    }

    public static char b(byte b2) {
        if (b2 < 0 || b2 >= c.length) {
            return ' ';
        }
        return c[b2];
    }

    private static void c() {
        if (f3a[0] != 'S') {
            for (byte b2 = 0; b2 < 13; b2 = (byte) (b2 + 1)) {
                f3a[b2] = 'S';
                b[b2] = 4;
            }
            for (byte b3 = 13; b3 < 26; b3 = (byte) (b3 + 1)) {
                f3a[b3] = 'H';
                b[b3] = 3;
            }
            for (byte b4 = 26; b4 < 39; b4 = (byte) (b4 + 1)) {
                f3a[b4] = 'C';
                b[b4] = 2;
            }
            for (byte b5 = 39; b5 < 52; b5 = (byte) (b5 + 1)) {
                f3a[b5] = 'D';
                b[b5] = 1;
            }
            for (byte b6 = 0; b6 < 52; b6 = (byte) (b6 + 13)) {
                c[b6] = 'A';
                d[b6] = 14;
            }
            for (byte b7 = 1; b7 < 52; b7 = (byte) (b7 + 13)) {
                c[b7] = '2';
                d[b7] = 2;
            }
            for (byte b8 = 2; b8 < 52; b8 = (byte) (b8 + 13)) {
                c[b8] = '3';
                d[b8] = 3;
            }
            for (byte b9 = 3; b9 < 52; b9 = (byte) (b9 + 13)) {
                c[b9] = '4';
                d[b9] = 4;
            }
            for (byte b10 = 4; b10 < 52; b10 = (byte) (b10 + 13)) {
                c[b10] = '5';
                d[b10] = 5;
            }
            for (byte b11 = 5; b11 < 52; b11 = (byte) (b11 + 13)) {
                c[b11] = '6';
                d[b11] = 6;
            }
            for (byte b12 = 6; b12 < 52; b12 = (byte) (b12 + 13)) {
                c[b12] = '7';
                d[b12] = 7;
            }
            for (byte b13 = 7; b13 < 52; b13 = (byte) (b13 + 13)) {
                c[b13] = '8';
                d[b13] = 8;
            }
            for (byte b14 = 8; b14 < 52; b14 = (byte) (b14 + 13)) {
                c[b14] = '9';
                d[b14] = 9;
            }
            for (byte b15 = 9; b15 < 52; b15 = (byte) (b15 + 13)) {
                c[b15] = 'T';
                d[b15] = 10;
            }
            for (byte b16 = 10; b16 < 52; b16 = (byte) (b16 + 13)) {
                c[b16] = 'J';
                d[b16] = 11;
            }
            for (byte b17 = 11; b17 < 52; b17 = (byte) (b17 + 13)) {
                c[b17] = 'Q';
                d[b17] = 12;
            }
            for (byte b18 = 12; b18 < 52; b18 = (byte) (b18 + 13)) {
                c[b18] = 'K';
                d[b18] = 13;
            }
            for (byte b19 = 0; b19 < 52; b19 = (byte) (b19 + 1)) {
                e[b19] = (byte) ((((d[b19] - 1) * 4) + b[b19]) - 5);
            }
        }
    }

    public static byte a(String str) {
        byte indexOf;
        if (str == null) {
            return -1;
        }
        if (str.length() != 2 || (indexOf = (byte) ((f.indexOf(str.substring(0, 1)) * 13) + g.indexOf(str.substring(1, 2)))) < 0 || indexOf > 51) {
            return -1;
        }
        return indexOf;
    }

    public final byte a() {
        if (this.i == -1) {
            return -1;
        }
        byte[] bArr = this.h;
        byte b2 = this.i;
        this.i = (byte) (b2 - 1);
        return bArr[b2];
    }

    public static void a(byte[] bArr, String str) {
        int length = bArr.length;
        System.out.print(str);
        if (bArr != null) {
            for (int i2 = 0; i2 < length; i2++) {
                System.out.print(a(bArr[i2]) + ",");
            }
        }
        System.out.println();
    }

    public static void a(byte[] bArr, byte b2) {
        for (int i2 = 0; i2 < bArr.length; i2++) {
            if (bArr[i2] == b2) {
                bArr[i2] = -1;
                return;
            }
        }
    }

    public final void b() {
        this.i = (byte) (this.h.length - 1);
        for (byte b2 = 0; b2 <= this.i; b2 = (byte) (b2 + 1)) {
            byte abs = (byte) (Math.abs(j.i.nextInt()) % (this.i + 1));
            if (abs != b2) {
                byte b3 = this.h[abs];
                this.h[abs] = this.h[b2];
                this.h[b2] = b3;
            }
        }
    }
}
