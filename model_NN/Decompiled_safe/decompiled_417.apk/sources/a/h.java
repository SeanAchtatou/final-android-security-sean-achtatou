package a;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/* compiled from: ProGuard */
public final class h extends DataOutputStream {
    public static final String[] b = {"disconnected", "connection timed out", "connection failed on read", "connection failed on write", "garbage read in", "security violation", "user banned"};
    public static final String[] c = {"Illegal play", "You cannot play twice in one turn!", "You do not have that tile!", "You must already have a triplet of those tiles!", "You do not have enough tiles to make a quadruplet!", "You are not the next in turn!", "You cannot make a connection that way!", "You cannot make a triplet that way!"};

    /* renamed from: a  reason: collision with root package name */
    ByteArrayOutputStream f6a;

    private h(ByteArrayOutputStream byteArrayOutputStream) {
        super(byteArrayOutputStream);
        this.f6a = byteArrayOutputStream;
    }

    private static h a(int i) {
        return new h(new ByteArrayOutputStream(i));
    }

    public static byte[] a(int i, byte b2, byte b3, i iVar) {
        return a(i, b2, b3, iVar.f7a, iVar.b, iVar.c, iVar.d, iVar.e, iVar.f);
    }

    public static byte[] a(int i, byte b2, byte b3, short s, byte[] bArr, short s2, int[] iArr, short s3, String[] strArr) {
        try {
            h a2 = a(9);
            a2.writeInt(i);
            a2.writeShort(0);
            a2.writeByte(b2);
            a2.writeByte(b3);
            a2.writeShort(s);
            for (short s4 = 0; s4 < s; s4 = (short) (s4 + 1)) {
                a2.writeByte(bArr[s4]);
            }
            a2.writeShort(s2);
            for (short s5 = 0; s5 < s2; s5 = (short) (s5 + 1)) {
                a2.writeInt(iArr[s5]);
            }
            a2.writeShort(s3);
            for (short s6 = 0; s6 < s3; s6 = (short) (s6 + 1)) {
                a2.writeUTF(strArr[s6]);
            }
            int i2 = a2.written;
            byte[] byteArray = a2.f6a.toByteArray();
            byteArray[4] = (byte) ((65280 & i2) >> 8);
            byteArray[5] = (byte) (i2 & 255);
            return byteArray;
        } catch (IOException e) {
            return null;
        }
    }

    public static byte[] a(String str, byte[] bArr, String str2, String str3, String str4) {
        try {
            h a2 = a(20);
            a2.writeInt(0);
            a2.writeByte(1);
            a2.writeUTF(str);
            a2.writeInt(bArr.length);
            a2.write(bArr);
            a2.writeByte(0);
            a2.writeUTF(str2);
            a2.writeUTF(str3);
            a2.writeUTF(str4);
            return a2.f6a.toByteArray();
        } catch (IOException e) {
            return null;
        }
    }

    public static byte[] a(byte b2, String str) {
        try {
            h a2 = a(19);
            a2.writeInt(1);
            a2.writeByte(b2);
            a2.writeUTF(str);
            return a2.f6a.toByteArray();
        } catch (IOException e) {
            return null;
        }
    }

    public static byte[] a() {
        try {
            h a2 = a(4);
            a2.writeInt(12);
            return a2.f6a.toByteArray();
        } catch (IOException e) {
            return null;
        }
    }

    public static byte[] a(int i, byte b2, byte b3, byte b4) {
        try {
            h a2 = a(8);
            a2.writeInt(2);
            a2.writeByte(i);
            a2.writeByte(b2);
            a2.writeByte(b3);
            a2.writeByte(b4);
            return a2.f6a.toByteArray();
        } catch (IOException e) {
            return null;
        }
    }

    public static byte[] b() {
        try {
            h a2 = a(4);
            a2.writeInt(6);
            return a2.f6a.toByteArray();
        } catch (IOException e) {
            return null;
        }
    }

    public static byte[] c() {
        try {
            h a2 = a(4);
            a2.writeInt(5);
            return a2.f6a.toByteArray();
        } catch (IOException e) {
            return null;
        }
    }
}
