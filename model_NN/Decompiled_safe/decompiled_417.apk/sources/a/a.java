package a;

import java.io.DataInputStream;
import java.lang.reflect.Array;

/* compiled from: ProGuard */
public final class a {
    public static final String[] g = {"NIL", "N-S", "E-W", "BOTH"};

    /* renamed from: a  reason: collision with root package name */
    public int f0a = -1;
    public int b = -1;
    public byte[][] c = ((byte[][]) Array.newInstance(Byte.TYPE, 4, 13));
    public byte d;
    public byte e;
    public long f;

    public final void a(a aVar) {
        if (aVar != null) {
            aVar.f0a = this.f0a;
            aVar.b = this.b;
            g.a(this.c, aVar.c);
            aVar.d = this.d;
            aVar.e = this.e;
            aVar.f = this.f;
        }
    }

    public final byte a() {
        return this.d;
    }

    public static byte a(int i) {
        return (byte) (Math.abs(i) % 4);
    }

    public final int b() {
        return this.b;
    }

    public final int c() {
        return this.f0a;
    }

    public final byte d() {
        return this.e;
    }

    public static byte b(int i) {
        switch (i % 16) {
            case 1:
                return 0;
            case 2:
                return 1;
            case 3:
                return 2;
            case 4:
                return 3;
            case 5:
                return 1;
            case 6:
                return 2;
            case 7:
                return 3;
            case 8:
                return 0;
            case 9:
                return 2;
            case 10:
                return 3;
            case 11:
                return 0;
            case 12:
                return 1;
            case 13:
                return 3;
            case 14:
                return 0;
            case 15:
                return 1;
            default:
                return 2;
        }
    }

    public final void a(DataInputStream dataInputStream) {
        this.d = dataInputStream.readByte();
        this.e = dataInputStream.readByte();
        this.f0a = dataInputStream.readInt();
        this.b = dataInputStream.readInt();
        for (int i = 0; i < this.c.length; i++) {
            for (int i2 = 0; i2 < this.c[i].length; i2++) {
                this.c[i][i2] = dataInputStream.readByte();
            }
        }
    }
}
