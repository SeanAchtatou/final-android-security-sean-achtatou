package a;

import java.lang.reflect.Array;

/* compiled from: ProGuard */
public final class b {
    public static byte[] b = null;
    public static byte[] c = {0, 1, 0, 2, 3};
    public static String[] d = {"C", "D", "H", "S", "NT"};
    public static String[] e = {"♣", "♦", "♥", "♠", "NT"};
    public static String[] f = {"&clubs;", "<font color=red>&diams;</font>", "<font color=red>&hearts;</font>", "&spades;", "NT"};
    public static String[] g = {"North", "East", "South", "West"};

    /* renamed from: a  reason: collision with root package name */
    public d f1a = new d();
    public a h;
    public byte i = -1;
    public byte[] j = new byte[80];
    public boolean[] k = new boolean[80];
    public byte[][] l = ((byte[][]) Array.newInstance(Byte.TYPE, 4, 13));
    public byte m = 0;
    public byte[][] n = ((byte[][]) Array.newInstance(Byte.TYPE, 4, 13));
    public byte[] o = new byte[13];
    public byte[] p = new byte[2];
    public byte[] q = new byte[2];
    public int[] r = new int[2];
    public long s;
    private byte t = -1;
    private byte u = -1;
    private byte v = -1;
    private byte w;
    private byte x;

    public b() {
        if (b == null) {
            b = new byte[52];
            for (int i2 = 0; i2 < 52; i2++) {
                b[i2] = (byte) (((d.b[i2] * 13) + d.d[i2]) - 2);
            }
        }
        this.x = 13;
        g.a(this.l);
        this.m = -1;
        g.a(this.n);
        g.b(this.o, (byte) -1);
        byte[] bArr = this.p;
        this.p[1] = 0;
        bArr[0] = 0;
        int[] iArr = this.r;
        this.r[1] = 0;
        iArr[0] = 0;
    }

    public final byte a(byte b2) {
        byte[] bArr = this.j;
        byte b3 = (byte) (this.i + 1);
        this.i = b3;
        bArr[b3] = b2;
        return this.i;
    }

    public static int a(boolean z, byte b2, byte b3, byte b4) {
        int i2;
        if (b2 == 99) {
            return 0;
        }
        if (b(b2, b4) < 0) {
            return 0;
        }
        if (b2 % 10 <= 1) {
            i2 = (b2 / 10) * 20;
        } else if (b2 % 10 <= 3) {
            i2 = (b2 / 10) * 30;
        } else {
            i2 = ((b2 / 10) * 30) + 10;
        }
        if (b3 == 98) {
            i2 *= 4;
        } else if (b3 == 97) {
            i2 *= 2;
        }
        if (i2 < 100) {
            return 50;
        }
        if (z) {
            return 500;
        }
        return 300;
    }

    public static int a(int i2) {
        if (i2 >= 4000) {
            return 24;
        }
        if (i2 >= 3500) {
            return 23;
        }
        if (i2 >= 3000) {
            return 22;
        }
        if (i2 >= 2500) {
            return 21;
        }
        if (i2 >= 2250) {
            return 20;
        }
        if (i2 >= 2000) {
            return 19;
        }
        if (i2 >= 1750) {
            return 18;
        }
        if (i2 >= 1500) {
            return 17;
        }
        if (i2 >= 1300) {
            return 16;
        }
        if (i2 >= 1100) {
            return 15;
        }
        if (i2 >= 900) {
            return 14;
        }
        if (i2 >= 750) {
            return 13;
        }
        if (i2 >= 600) {
            return 12;
        }
        if (i2 >= 500) {
            return 11;
        }
        if (i2 >= 430) {
            return 10;
        }
        if (i2 >= 370) {
            return 9;
        }
        if (i2 >= 320) {
            return 8;
        }
        if (i2 >= 270) {
            return 7;
        }
        if (i2 >= 220) {
            return 6;
        }
        if (i2 >= 170) {
            return 5;
        }
        if (i2 >= 130) {
            return 4;
        }
        if (i2 >= 90) {
            return 3;
        }
        if (i2 >= 50) {
            return 2;
        }
        if (i2 >= 20) {
            return 1;
        }
        return 0;
    }

    public static int b(boolean z, byte b2, byte b3, byte b4) {
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        if (b2 == 99) {
            return 0;
        }
        byte b5 = b(b2, b4);
        if (b5 >= 0) {
            if (b2 % 10 <= 1) {
                i2 = (b2 / 10) * 20;
            } else if (b2 % 10 <= 3) {
                i2 = (b2 / 10) * 30;
            } else {
                i2 = ((b2 / 10) * 30) + 10;
            }
            if (b3 == 98) {
                int i8 = i2 * 4;
                if (z) {
                    i3 = b5 * 400;
                    i4 = i8;
                    i5 = 100;
                } else {
                    i3 = b5 * 200;
                    i4 = i8;
                    i5 = 100;
                }
            } else if (b3 == 97) {
                int i9 = i2 * 2;
                if (z) {
                    i3 = b5 * 100;
                    i4 = i9;
                    i5 = 50;
                } else {
                    i3 = b5 * 50;
                    i4 = i9;
                    i5 = 50;
                }
            } else if (b2 % 10 <= 1) {
                i3 = b5 * 20;
                i4 = i2;
                i5 = 0;
            } else {
                i3 = b5 * 30;
                i4 = i2;
                i5 = 0;
            }
            if (i4 >= 100) {
                i6 = z ? 500 : 300;
            } else {
                i6 = 50;
            }
            if (b2 >= 70) {
                if (z) {
                    i7 = 1500;
                } else {
                    i7 = 1000;
                }
            } else if (b2 < 60) {
                i7 = 0;
            } else if (z) {
                i7 = 750;
            } else {
                i7 = 500;
            }
            return i4 + i6 + i3 + i7 + i5;
        } else if (z) {
            switch (b3) {
                case 97:
                    return ((b5 + 1) * 100) + (b5 * 200);
                case 98:
                    return ((b5 + 1) * 200) + (b5 * 400);
                default:
                    return b5 * 100;
            }
        } else {
            switch (b3) {
                case 97:
                    if (b5 >= -3) {
                        return ((b5 + 1) * 200) - 100;
                    }
                    return ((b5 + 3) * 300) - 500;
                case 98:
                    if (b5 >= -3) {
                        return ((b5 + 1) * 400) - 200;
                    }
                    return ((b5 + 3) * 600) - 1000;
                default:
                    return b5 * 50;
            }
        }
    }

    public static int a(byte b2, byte b3, byte b4) {
        int i2;
        if (b2 == 99) {
            return 0;
        }
        if (b(b2, b4) < 0) {
            return 0;
        }
        if (b2 % 10 <= 1) {
            i2 = (b2 / 10) * 20;
        } else if (b2 % 10 <= 3) {
            i2 = (b2 / 10) * 30;
        } else {
            i2 = ((b2 / 10) * 30) + 10;
        }
        if (b3 == 98) {
            return i2 * 4;
        }
        if (b3 == 97) {
            return i2 * 2;
        }
        return i2;
    }

    public static int c(boolean z, byte b2, byte b3, byte b4) {
        int i2;
        if (b2 == 99) {
            return 0;
        }
        byte b5 = b(b2, b4);
        if (b5 <= 0) {
            return 0;
        }
        if (b2 % 10 <= 1) {
            i2 = b5 * 20;
        } else {
            i2 = b5 * 30;
        }
        if (b3 == 98) {
            if (z) {
                return b5 * 400;
            }
            return b5 * 200;
        } else if (b3 != 97) {
            return i2;
        } else {
            if (z) {
                return b5 * 200;
            }
            return b5 * 100;
        }
    }

    public static int a(boolean z, byte b2, byte b3) {
        if (b2 == 99) {
            return 0;
        }
        if (b(b2, b3) < 0) {
            return 0;
        }
        if (b2 >= 70) {
            if (z) {
                return 1500;
            }
            return 1000;
        } else if (b2 < 60) {
            return 0;
        } else {
            if (z) {
                return 750;
            }
            return 500;
        }
    }

    public static int d(boolean z, byte b2, byte b3, byte b4) {
        if (b2 == 99) {
            return 0;
        }
        byte b5 = b(b2, b4);
        if (b5 >= 0) {
            return 0;
        }
        if (z) {
            switch (b3) {
                case 97:
                    return ((b5 + 1) * 100) + (b5 * 200);
                case 98:
                    return ((b5 + 1) * 200) + (b5 * 400);
                default:
                    return b5 * 100;
            }
        } else {
            switch (b3) {
                case 97:
                    if (b5 >= -3) {
                        return ((b5 + 1) * 200) - 100;
                    }
                    return ((b5 + 3) * 300) - 500;
                case 98:
                    if (b5 >= -3) {
                        return ((b5 + 1) * 400) - 200;
                    }
                    return ((b5 + 3) * 600) - 1000;
                default:
                    return b5 * 50;
            }
        }
    }

    public final void a() {
        this.i = -1;
        boolean[] zArr = this.k;
        for (int i2 = 0; i2 < zArr.length; i2++) {
            zArr[i2] = false;
        }
    }

    public final void b() {
        this.v = -1;
        this.t = -1;
        this.u = -1;
        this.m = -1;
        g.a(this.n);
        g.b(this.o, (byte) -1);
        g.b(this.p, (byte) 0);
        g.b(this.q, (byte) 0);
    }

    public final void a(b bVar) {
        if (bVar != null) {
            bVar.h = this.h;
            bVar.i = this.i;
            System.arraycopy(this.j, 0, bVar.j, 0, this.j.length);
            System.arraycopy(this.k, 0, bVar.k, 0, this.k.length);
            bVar.t = this.t;
            bVar.u = this.u;
            bVar.T(this.v);
            bVar.x = this.x;
            g.a(this.l, bVar.l);
            bVar.m = this.m;
            g.a(this.n, bVar.n);
            System.arraycopy(this.o, 0, bVar.o, 0, this.o.length);
            System.arraycopy(this.p, 0, bVar.p, 0, this.p.length);
            System.arraycopy(this.q, 0, bVar.q, 0, this.q.length);
            System.arraycopy(this.r, 0, bVar.r, 0, this.r.length);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.b.a(byte[], boolean):void
     arg types: [byte[], int]
     candidates:
      a.b.a(int, byte):byte
      a.b.a(byte[], byte):byte
      a.b.a(byte[][], int):byte
      a.b.a(byte, byte):java.lang.String
      a.b.a(byte, int):byte
      a.b.a(int, int):byte
      a.b.a(byte, char):int
      a.b.a(byte, boolean):boolean
      a.b.a(byte[], boolean):void */
    public final boolean c() {
        if (this.h == null) {
            return false;
        }
        a(this.h.c[0], true);
        a(this.h.c[1], true);
        a(this.h.c[2], true);
        a(this.h.c[3], true);
        for (int i2 = 0; i2 < this.l.length; i2++) {
            for (int i3 = 0; i3 < this.l[i2].length; i3++) {
                this.l[i2][i3] = this.h.c[i2][i3];
            }
        }
        return true;
    }

    public final void d() {
        for (byte b2 = 0; b2 < 13; b2 = (byte) (b2 + 1)) {
            this.h.c[0][b2] = this.f1a.a();
            this.h.c[1][b2] = this.f1a.a();
            this.h.c[2][b2] = this.f1a.a();
            this.h.c[3][b2] = this.f1a.a();
        }
    }

    public static byte b(byte b2) {
        return b2 == 2 ? (byte) 3 : 2;
    }

    public static byte a(String str) {
        int i2;
        if (str.equals("PASS")) {
            return 99;
        }
        if (str.equals("X")) {
            return 97;
        }
        if (str.equals("XX")) {
            return 98;
        }
        int i3 = (str.toCharArray()[0] - '1') + 1;
        String substring = str.substring(1, str.length());
        if (substring.equals("NT")) {
            i2 = 4;
        } else if (substring.equals("S")) {
            i2 = 3;
        } else if (substring.equals("H")) {
            i2 = 2;
        } else {
            i2 = substring.equals("D") ? 1 : 0;
        }
        return (byte) ((i3 * 10) + i2);
    }

    public static byte a(int i2, byte b2) {
        return a(Integer.toString(i2) + Character.toString(q(b2)));
    }

    public static byte b(String str) {
        if (str.equals("X")) {
            return 97;
        }
        if (str.equals("R")) {
            return 98;
        }
        if (str.endsWith("N")) {
            return a(str + "T");
        }
        if (str.matches("[1-7][SHDC]")) {
            return a(str);
        }
        return 99;
    }

    public final byte b(int i2) {
        if (i2 > 0) {
            if (i2 - 1 > this.i) {
                return -1;
            }
            return this.j[i2 - 1];
        } else if (this.i + i2 < 0 || this.i + i2 >= this.j.length) {
            return -1;
        } else {
            return this.j[this.i + i2];
        }
    }

    public static byte c(byte b2) {
        if (b2 == 99) {
            return 0;
        }
        if (b2 == 97) {
            return 0;
        }
        if (b2 == 98) {
            return 0;
        }
        return (byte) (b2 / 10);
    }

    public final byte e() {
        return this.i;
    }

    public final String f() {
        String str;
        StringBuilder sb = new StringBuilder();
        for (int i2 = 0; i2 <= this.i; i2++) {
            byte b2 = b(i2 + 1);
            if (b2 == 99) {
                str = "P";
            } else if (b2 == 97) {
                str = "X";
            } else if (b2 == 98) {
                str = "R";
            } else {
                str = (b2 / 10) + Character.toString(d[b2 % 10].charAt(0));
            }
            sb.append(str);
        }
        return sb.toString();
    }

    public static String d(byte b2) {
        if (b2 == 99) {
            return "P";
        }
        if (b2 == 97) {
            return "X";
        }
        if (b2 == 98) {
            return "XX";
        }
        return (b2 / 10) + d[b2 % 10];
    }

    public static byte e(byte b2) {
        if (b2 == 99) {
            return -1;
        }
        if (b2 == 97) {
            return -1;
        }
        if (b2 == 98) {
            return -1;
        }
        return (byte) (b2 % 10);
    }

    public final byte a(byte b2, byte b3, int i2) {
        if (b3 < 0 || b3 > 3) {
            return -1;
        }
        if (i2 <= 0 || i2 > 13) {
            return -1;
        }
        String a2 = a(this.l[b2], b3, 2);
        if (a2.equals("")) {
            return -1;
        }
        if (a2.length() < i2) {
            return -1;
        }
        return d.a(q(b3) + a2.substring(i2 - 1, i2));
    }

    public final byte g() {
        return this.x;
    }

    public final byte[] c(int i2) {
        return this.n[i2];
    }

    public final byte a(int i2, int i3) {
        return this.n[i2][i3];
    }

    public final byte h() {
        return this.m;
    }

    public static String f(byte b2) {
        if (b2 < 0 || b2 > 51) {
            return "";
        }
        return e[g(b2)] + (d.b(b2) == 'T' ? "10" : Character.toString(d.b(b2)));
    }

    public final int a(byte b2, char c2) {
        int i2 = 0;
        for (int i3 = 0; i3 < this.l[b2].length; i3++) {
            if (this.l[b2][i3] != -1 && (d.b(this.l[b2][i3]) == c2 || c2 == '*')) {
                i2++;
            }
        }
        return i2;
    }

    public static byte a(byte[] bArr, byte b2) {
        for (byte b3 = 0; b3 < bArr.length; b3 = (byte) (b3 + 1)) {
            if (bArr[b3] != -1 && bArr[b3] == b2) {
                return b3;
            }
        }
        return -1;
    }

    public static byte g(byte b2) {
        if (b2 < 0 || b2 > 51) {
            return -1;
        }
        if (b2 < 13) {
            return 3;
        }
        if (b2 < 26) {
            return 2;
        }
        if (b2 < 39) {
            return 0;
        }
        return 1;
    }

    public final byte i() {
        if (I()) {
            return -1;
        }
        return (byte) (((this.h.d + this.i) + 1) % 4);
    }

    public final byte j() {
        return (byte) (((t(C()) + this.m) + 1) % 4);
    }

    public final byte k() {
        return this.t;
    }

    public static byte h(byte b2) {
        if (b2 == -1) {
            return 0;
        }
        if (b2 == 99) {
            return 0;
        }
        return (byte) (b2 / 10);
    }

    public static byte i(byte b2) {
        if (b2 == -1) {
            return -1;
        }
        if (b2 == 99) {
            return -1;
        }
        return (byte) (b2 % 10);
    }

    public static String a(byte b2, byte b3) {
        if (b2 == -1) {
            return "N/A";
        }
        if (b2 == 99) {
            return "All Pass";
        }
        if (!z(b2)) {
            return "N/A";
        }
        StringBuilder sb = new StringBuilder();
        sb.append(Byte.toString(c(b2)));
        sb.append(e[e(b2)]);
        if (b3 == 97) {
            sb.append("X");
        }
        if (b3 == 98) {
            sb.append("XX");
        }
        return sb.toString();
    }

    public final byte l() {
        if (b(0) == 99 && b(-1) == 99 && b(-2) == 99 && b(-3) == 99) {
            return 99;
        }
        for (int i2 = -3; i2 > -10; i2--) {
            if (b(i2) == -1) {
                return -1;
            }
            if (z(b(i2))) {
                return b(i2);
            }
        }
        return -1;
    }

    public static byte b(byte b2, byte b3) {
        if (b2 == 99) {
            return 0;
        }
        return (byte) ((b3 - h(b2)) - 6);
    }

    public final byte m() {
        return this.u;
    }

    public final byte n() {
        if (b(-3) == 98) {
            return 98;
        }
        if (b(-3) == 97) {
            return 97;
        }
        return -1;
    }

    public final byte o() {
        if (this.h == null) {
            return -1;
        }
        return this.h.a();
    }

    public final byte p() {
        byte b2;
        byte b3;
        byte l2 = l();
        if (l2 == 99) {
            return -1;
        }
        if (n() != 97) {
            b2 = (byte) ((this.i + 1) % 2);
        } else {
            b2 = (byte) (this.i % 2);
        }
        int i2 = b2 % 2;
        while (true) {
            b3 = (byte) i2;
            if (b3 < this.i && b(b3 + 1) % 10 != l2 % 10) {
                i2 = b3 + 2;
            }
        }
        return (byte) ((o() + b3) % 4);
    }

    public final int q() {
        if (this.h == null) {
            return 0;
        }
        return this.h.b();
    }

    public final int r() {
        if (this.h == null) {
            return 0;
        }
        return this.h.c();
    }

    public final byte s() {
        return this.v;
    }

    public final String j(byte b2) {
        byte[] a2 = a(this.l[b2]);
        for (int i2 = 1; i2 < a2.length; i2++) {
            int i3 = i2;
            while (i3 > 0 && a2[i3] > a2[i3 - 1]) {
                byte b3 = a2[i3];
                a2[i3] = a2[i3 - 1];
                a2[i3 - 1] = b3;
                i3--;
            }
        }
        return ((int) a2[0]) + "-" + ((int) a2[1]) + "-" + ((int) a2[2]) + "-" + ((int) a2[3]);
    }

    public final int a(byte b2, char c2, int i2) {
        int i3 = 0;
        byte a2 = a(c2);
        for (byte b3 = 0; b3 < 4; b3 = (byte) (b3 + 1)) {
            if (b3 != a2) {
                int c3 = c(b2, q(b3));
                if (c3 == 0) {
                    i3 += i2 % 10;
                } else if (c3 == 1) {
                    i3 += (i2 % 100) / 10;
                } else if (c3 == 2) {
                    i3 += i2 / 100;
                }
            }
        }
        return i3;
    }

    public final byte t() {
        if (this.v == -1) {
            return -1;
        }
        return (byte) ((this.v + 2) % 4);
    }

    public final byte u() {
        return (byte) ((this.v + 1) % 4);
    }

    public final byte c(byte b2, byte b3) {
        byte b4 = -9999;
        byte b5 = -1;
        for (byte b6 = 0; b6 < 13; b6 = (byte) (b6 + 1)) {
            if (this.l[b2][b6] != -1 && d.d[this.l[b2][b6]] > b4 && c[d.b[this.l[b2][b6]]] == b3) {
                b4 = d.d[this.l[b2][b6]];
                b5 = b6;
            }
        }
        if (b5 == -1) {
            return b5;
        }
        return this.l[b2][b5];
    }

    public final byte d(byte b2, byte b3) {
        byte b4 = 9999;
        byte b5 = -1;
        for (byte b6 = 0; b6 < 13; b6 = (byte) (b6 + 1)) {
            if (this.l[b2][b6] != -1 && d.d[this.l[b2][b6]] < b4 && c[d.b[this.l[b2][b6]]] == b3) {
                b4 = d.d[this.l[b2][b6]];
                b5 = b6;
            }
        }
        if (b5 == -1) {
            return b5;
        }
        return this.l[b2][b5];
    }

    public final int k(byte b2) {
        if (b2 < 0 || b2 > 3) {
            return 0;
        }
        int i2 = 0;
        for (int i3 = 0; i3 < this.h.c[b2].length; i3++) {
            if (this.h.c[b2][i3] != -1) {
                switch (d.b(this.h.c[b2][i3])) {
                    case 'A':
                        i2 += 4;
                        continue;
                    case 'J':
                        i2++;
                        continue;
                    case 'K':
                        i2 += 3;
                        continue;
                    case 'Q':
                        i2 += 2;
                        continue;
                }
            }
        }
        return i2;
    }

    public final int b(byte b2, char c2) {
        byte a2 = a(c2);
        if (b2 < 0 || b2 > 3) {
            return 0;
        }
        int i2 = 0;
        for (int i3 = 0; i3 < this.h.c.length; i3++) {
            if (this.h.c[b2][i3] != -1 && g(this.h.c[b2][i3]) == a2) {
                switch (d.b(this.h.c[b2][i3])) {
                    case 'A':
                        i2 += 4;
                        continue;
                    case 'J':
                        i2++;
                        continue;
                    case 'K':
                        i2 += 3;
                        continue;
                    case 'Q':
                        i2 += 2;
                        continue;
                }
            }
        }
        return i2;
    }

    public final byte v() {
        if (this.i < 0) {
            return -1;
        }
        return (byte) ((this.h.d + this.i) % 4);
    }

    public final byte w() {
        byte b2;
        int i2 = 0;
        while (true) {
            int i3 = i2 - 1;
            b2 = b(i2);
            if (!z(b2) && b2 != -1) {
                i2 = i3;
            }
        }
        return b2;
    }

    public final byte x() {
        for (byte b2 = this.i; b2 >= 0; b2 = (byte) (b2 - 1)) {
            if (z(b(b2 + 1))) {
                return p(b2);
            }
        }
        return -1;
    }

    public final byte y() {
        if (this.m < 0 || this.m >= 52) {
            return -1;
        }
        return this.n[this.m == -1 ? -1 : (byte) ((t((byte) (this.m / 4)) + this.m) % 4)][this.m / 4];
    }

    public static byte l(byte b2) {
        return (byte) ((b2 + 1) % 4);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.g.a(byte[], int, boolean, byte[]):void
     arg types: [byte[], int, int, byte[]]
     candidates:
      a.g.a(java.lang.String, java.lang.String, java.lang.String, java.lang.String):java.lang.String
      a.g.a(byte[], int, boolean, byte[]):void */
    public final byte a(byte b2, int i2) {
        byte[] bArr = {0, 1, 2, 3};
        g.a(bArr, bArr.length - 1, false, a(this.l[b2]));
        if (i2 <= 0 || i2 > 4) {
            return bArr[0];
        }
        return bArr[i2 - 1];
    }

    public final char b(byte b2, int i2) {
        return q(a(b2, i2));
    }

    public final byte z() {
        byte w2 = w();
        if (w2 == -1) {
            return 1;
        }
        if (w2 == a("7NT")) {
            return -1;
        }
        if (i(w2) == 4) {
            return (byte) (h(w2) + 1);
        }
        return h(w2);
    }

    public static byte a(byte[][] bArr, int i2, int i3) {
        byte a2 = a(bArr, i2);
        if (a2 == -1) {
            return -1;
        }
        return bArr[i3][a2];
    }

    public static byte a(byte[][] bArr, int i2) {
        if (bArr == null) {
            return -1;
        }
        if (bArr.length != 2) {
            return -1;
        }
        if (i2 < 0 || i2 >= bArr[1].length) {
            return -1;
        }
        byte b2 = bArr[1][i2];
        for (int i3 = i2 + 1; i3 < bArr[1].length; i3++) {
            if (bArr[1][i3] != b2) {
                return (byte) i3;
            }
        }
        return -1;
    }

    public static byte m(byte b2) {
        return (byte) ((b2 + 2) % 4);
    }

    public final String A() {
        StringBuilder sb = new StringBuilder();
        for (int i2 = 0; i2 <= this.m; i2++) {
            byte b2 = (byte) (i2 / 4);
            sb.append(d.a(this.n[(t(b2) + (i2 % 4)) % 4][b2]));
        }
        if (this.q[0] + this.q[1] > 0) {
            sb.append("*");
            sb.append(Integer.toHexString(this.q[this.v % 2]));
        }
        return sb.toString();
    }

    public final float n(byte b2) {
        float f2 = 0.0f;
        for (byte b3 = 0; b3 < 4; b3 = (byte) (b3 + 1)) {
            if (b(b2, q(b3)) < 9 || c(b2, q(b3)) <= 3) {
                String a2 = a(this.l[b2], b3, 10);
                if (a2.indexOf("A") >= 0) {
                    if (a2.indexOf("AKQ") >= 0) {
                        f2 += 3.0f;
                    } else if (a2.indexOf("AKJ") >= 0) {
                        f2 += 2.5f;
                    } else if (a2.indexOf("AQJ") >= 0) {
                        f2 += 2.25f;
                    } else if (a2.indexOf("AQT") >= 0) {
                        f2 += 1.75f;
                    } else if (a2.indexOf("AJT") >= 0) {
                        f2 += 1.5f;
                    } else if (a2.indexOf("AJX") >= 0) {
                        f2 += 1.25f;
                    } else if (a2.indexOf("AK") >= 0) {
                        f2 += 2.0f;
                    } else if (a2.indexOf("AQ") >= 0) {
                        f2 += 1.5f;
                    } else if (a2.indexOf("AJ") >= 0) {
                        f2 += 1.25f;
                    }
                } else if (a2.indexOf("K") >= 0) {
                    if (a2.indexOf("KQJ") >= 0) {
                        f2 += 2.0f;
                    } else if (a2.indexOf("KQT") >= 0) {
                        f2 += 1.75f;
                    } else if (a2.indexOf("KQC") >= 0) {
                        f2 += 1.25f;
                    } else if (a2.indexOf("KJT") >= 0) {
                        f2 += 1.25f;
                    } else if (a2.indexOf("KJX") >= 0) {
                        f2 += 1.0f;
                    } else if (a2.indexOf("KQ") >= 0) {
                        f2 += 1.0f;
                    } else if (a2.indexOf("KJ") >= 0) {
                        f2 += 0.75f;
                    }
                } else if (a2.indexOf("Q") >= 0) {
                    if (a2.indexOf("QJT") >= 0) {
                        f2 += 1.0f;
                    } else if (a2.indexOf("QJX") >= 0) {
                        f2 += 0.75f;
                    } else if (a2.indexOf("QTX") >= 0) {
                        f2 += 0.5f;
                    } else if (a2.indexOf("QJ") >= 0) {
                        f2 += 0.5f;
                    }
                }
            } else {
                f2 += (float) (c(b2, q(b3)) - 3);
            }
        }
        return f2;
    }

    public static byte o(byte b2) {
        return (byte) ((b2 + 3) % 4);
    }

    public final byte p(byte b2) {
        if (b2 < 0) {
            return -1;
        }
        return (byte) ((o() + (b2 % 4)) % 4);
    }

    public static char q(byte b2) {
        switch (b2) {
            case 0:
                return 'C';
            case 1:
                return 'D';
            case 2:
                return 'H';
            case 3:
                return 'S';
            default:
                return ' ';
        }
    }

    private static byte a(char c2) {
        switch (c2) {
            case 'C':
                return 0;
            case 'D':
                return 1;
            case 'H':
                return 2;
            case 'S':
                return 3;
            default:
                return -1;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.b.a(int, int):byte
     arg types: [byte, byte]
     candidates:
      a.b.a(int, byte):byte
      a.b.a(byte[], byte):byte
      a.b.a(byte[][], int):byte
      a.b.a(byte, byte):java.lang.String
      a.b.a(byte[], boolean):void
      a.b.a(byte, int):byte
      a.b.a(byte, char):int
      a.b.a(byte, boolean):boolean
      a.b.a(int, int):byte */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.b.a(byte[], boolean):void
     arg types: [byte[], int]
     candidates:
      a.b.a(int, byte):byte
      a.b.a(byte[], byte):byte
      a.b.a(byte[][], int):byte
      a.b.a(byte, byte):java.lang.String
      a.b.a(byte, int):byte
      a.b.a(int, int):byte
      a.b.a(byte, char):int
      a.b.a(byte, boolean):boolean
      a.b.a(byte[], boolean):void */
    public final byte[][] r(byte b2) {
        byte C = C();
        byte[][] bArr = (byte[][]) Array.newInstance(Byte.TYPE, 2, 13);
        byte[] bArr2 = new byte[13];
        g.a(bArr);
        g.b(bArr2, (byte) -1);
        int i2 = 0;
        for (byte b3 = 0; b3 < 4; b3 = (byte) (b3 + 1)) {
            int i3 = i2;
            for (byte b4 = 0; b4 < 13; b4 = (byte) (b4 + 1)) {
                if (this.l[b3][b4] != -1 && g(this.l[b3][b4]) == b2) {
                    bArr[0][i3] = this.l[b3][b4];
                    bArr[1][i3] = b3;
                    i3++;
                }
            }
            if (a((int) b3, (int) C) == -1 || g(a((int) b3, (int) C)) != b2) {
                i2 = i3;
            } else {
                bArr[0][i3] = a((int) b3, (int) C);
                bArr[1][i3] = b3;
                i2 = i3 + 1;
            }
        }
        if (i2 == 0) {
            return null;
        }
        System.arraycopy(bArr[0], 0, bArr2, 0, bArr[0].length);
        a(bArr2, true);
        byte[][] bArr3 = (byte[][]) Array.newInstance(Byte.TYPE, 2, i2);
        for (byte b5 = 0; b5 < i2; b5 = (byte) (b5 + 1)) {
            byte a2 = (byte) g.a(bArr2, bArr[0][b5]);
            if (a2 < 0 || a2 >= i2) {
                System.out.println("i=" + ((int) b5) + ",j=" + ((int) a2));
                d.a(bArr2, "sortedCards=");
                d.a(bArr[0], "cards[0]=");
                g.a(bArr[1], bArr[1].length, "cards[1]=");
            }
            bArr3[0][a2] = bArr[0][a2];
            bArr3[1][a2] = bArr[1][a2];
        }
        return bArr3;
    }

    public final int e(byte b2, byte b3) {
        boolean z;
        if (E(b3) || F(b3)) {
            z = true;
        } else {
            z = false;
        }
        if (!z) {
            return 0;
        }
        return c(b2, q(b3));
    }

    public final int c(byte b2, char c2) {
        int i2 = 0;
        for (int i3 = 0; i3 < this.l[b2].length; i3++) {
            if (this.l[b2][i3] != -1 && d.f3a[this.l[b2][i3]] == c2) {
                i2++;
            }
        }
        return i2;
    }

    public static byte[] a(byte[] bArr) {
        byte[] bArr2 = new byte[4];
        for (int i2 = 0; i2 < bArr.length; i2++) {
            if (bArr[i2] != -1) {
                byte g2 = g(bArr[i2]);
                bArr2[g2] = (byte) (bArr2[g2] + 1);
            }
        }
        return bArr2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.b.a(byte[], boolean):void
     arg types: [byte[], int]
     candidates:
      a.b.a(int, byte):byte
      a.b.a(byte[], byte):byte
      a.b.a(byte[][], int):byte
      a.b.a(byte, byte):java.lang.String
      a.b.a(byte, int):byte
      a.b.a(int, int):byte
      a.b.a(byte, char):int
      a.b.a(byte, boolean):boolean
      a.b.a(byte[], boolean):void */
    public static String a(byte[] bArr, byte b2, int i2) {
        StringBuffer stringBuffer = new StringBuffer();
        byte[] bArr2 = new byte[bArr.length];
        System.arraycopy(bArr, 0, bArr2, 0, bArr2.length);
        a(bArr2, false);
        for (int i3 = 0; i3 < bArr.length; i3++) {
            if (bArr2[i3] != -1 && g(bArr2[i3]) == b2) {
                if (d.d[bArr2[i3]] >= i2) {
                    stringBuffer.append(d.b(bArr2[i3]));
                } else {
                    stringBuffer.append("X");
                }
            }
        }
        return stringBuffer.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.b.a(int, int):byte
     arg types: [int, byte]
     candidates:
      a.b.a(int, byte):byte
      a.b.a(byte[], byte):byte
      a.b.a(byte[][], int):byte
      a.b.a(byte, byte):java.lang.String
      a.b.a(byte[], boolean):void
      a.b.a(byte, int):byte
      a.b.a(byte, char):int
      a.b.a(byte, boolean):boolean
      a.b.a(int, int):byte */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.b.a(byte[], boolean):void
     arg types: [byte[], int]
     candidates:
      a.b.a(int, byte):byte
      a.b.a(byte[], byte):byte
      a.b.a(byte[][], int):byte
      a.b.a(byte, byte):java.lang.String
      a.b.a(byte, int):byte
      a.b.a(int, int):byte
      a.b.a(byte, char):int
      a.b.a(byte, boolean):boolean
      a.b.a(byte[], boolean):void */
    public final float f(byte b2, byte b3) {
        byte C = C();
        int i2 = 0;
        byte[] bArr = new byte[13];
        byte[] bArr2 = new byte[13];
        g.b(bArr, (byte) -1);
        g.b(bArr2, (byte) -1);
        int i3 = 0;
        for (int i4 = 0; i4 < 4; i4++) {
            int i5 = i3;
            int i6 = i2;
            for (int i7 = 0; i7 < 13; i7++) {
                if (this.l[i4][i7] != -1 && g(this.l[i4][i7]) == b3) {
                    int i8 = i5 + 1;
                    bArr[i5] = this.l[i4][i7];
                    if (i4 % 2 == b2) {
                        bArr2[i6] = this.l[i4][i7];
                        i6++;
                        i5 = i8;
                    } else {
                        i5 = i8;
                    }
                }
            }
            if (a(i4, (int) C) == -1 || g(a(i4, (int) C)) != b3) {
                i2 = i6;
                i3 = i5;
            } else {
                int i9 = i5 + 1;
                bArr[i5] = a(i4, (int) C);
                if (i4 % 2 == b2) {
                    int i10 = i6 + 1;
                    bArr2[i6] = a(i4, (int) C);
                    i3 = i9;
                    i2 = i10;
                } else {
                    int i11 = i6;
                    i3 = i9;
                    i2 = i11;
                }
            }
        }
        a(bArr, true);
        float f2 = 0.0f;
        float f3 = 0.0f;
        for (int i12 = 0; i12 < i3; i12++) {
            f2 += (float) (i12 + 1);
            if (g.a(bArr2, bArr[i12]) >= 0) {
                f3 += (float) (i12 + 1);
            }
        }
        if (f2 == 0.0f) {
            return 0.0f;
        }
        return f3 / f2;
    }

    public final byte B() {
        return (byte) (this.t % 10);
    }

    public final byte C() {
        return (byte) ((this.m + 1) / 4);
    }

    public final byte D() {
        return s(C());
    }

    public final byte s(byte b2) {
        byte t2 = t(b2);
        if (t2 == -1) {
            return -1;
        }
        if (this.n[t2][b2] == -1) {
            return -1;
        }
        return c[d.b[this.n[t2][b2]]];
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.b.a(int, int):byte
     arg types: [byte, byte]
     candidates:
      a.b.a(int, byte):byte
      a.b.a(byte[], byte):byte
      a.b.a(byte[][], int):byte
      a.b.a(byte, byte):java.lang.String
      a.b.a(byte[], boolean):void
      a.b.a(byte, int):byte
      a.b.a(byte, char):int
      a.b.a(byte, boolean):boolean
      a.b.a(int, int):byte */
    public final byte[] E() {
        byte[] a2;
        if (D() == -1) {
            return null;
        }
        byte j2 = j();
        byte C = C();
        byte t2 = t(C);
        byte a3 = a((int) t2, (int) C);
        if (j2 == ((byte) ((t2 + 1) % 4))) {
            a2 = a(a3, this.l[(byte) ((t2 + 1) % 4)], this.l[(byte) ((t2 + 2) % 4)], this.l[(byte) ((t2 + 3) % 4)], (byte) -1, (byte) -1);
        } else if (j2 == ((byte) ((t2 + 2) % 4))) {
            a2 = a(a3, null, this.l[(byte) ((t2 + 2) % 4)], this.l[(byte) ((t2 + 3) % 4)], a((int) ((byte) ((t2 + 1) % 4)), (int) C), (byte) -1);
        } else {
            a2 = a(a3, null, null, this.l[(byte) ((t2 + 3) % 4)], a((int) ((byte) ((t2 + 1) % 4)), (int) C), a((int) ((byte) ((t2 + 2) % 4)), (int) C));
        }
        a2[0] = (byte) ((a2[0] + t2) % 4);
        return a2;
    }

    public final byte F() {
        byte b2 = 0;
        byte C = C();
        byte j2 = j();
        for (int i2 = 0; i2 < this.l[j2].length - C; i2++) {
            if (this.l[j2][i2] != -1 && k(this.l[j2][i2], j2)) {
                b2 = (byte) (b2 + 1);
            }
        }
        return b2;
    }

    private static byte[] a(byte b2, byte[] bArr, byte[] bArr2, byte[] bArr3, byte b3, byte b4) {
        byte b5;
        byte b6;
        byte g2 = g(b2);
        if (b3 != -1) {
            if (b(b2, b3, g2)) {
                b5 = b3;
                b6 = 1;
            }
            b5 = b2;
            b6 = 0;
        } else {
            if (bArr != null) {
                b5 = b2;
                b6 = 0;
                for (int i2 = 0; i2 < bArr.length; i2++) {
                    if (bArr[i2] != -1 && b(b5, bArr[i2], g2)) {
                        b5 = bArr[i2];
                        b6 = 1;
                    }
                }
            }
            b5 = b2;
            b6 = 0;
        }
        if (b4 != -1) {
            if (b(b5, b4, g2)) {
                b5 = b4;
                b6 = 2;
            }
        } else if (bArr2 != null) {
            for (int i3 = 0; i3 < bArr2.length; i3++) {
                if (bArr2[i3] != -1 && b(b5, bArr2[i3], g2)) {
                    b5 = bArr2[i3];
                    b6 = 2;
                }
            }
        }
        if (bArr3 != null) {
            for (int i4 = 0; i4 < bArr3.length; i4++) {
                if (bArr3[i4] != -1 && b(b5, bArr3[i4], g2)) {
                    b6 = 3;
                    b5 = bArr3[i4];
                }
            }
        }
        return new byte[]{b6, b5};
    }

    public final byte t(byte b2) {
        if (b2 < 0 || b2 > 12) {
            return 0;
        }
        if (b2 == 0) {
            return this.w;
        }
        return this.o[b2 - 1];
    }

    public final byte u(byte b2) {
        byte b3;
        if (b2 < 0 || b2 > 12) {
            return -1;
        }
        byte t2 = t(b2);
        byte B = B();
        if (b(this.n[t2][b2], this.n[(t2 + 1) % 4][b2], B)) {
            b3 = (byte) ((t2 + 1) % 4);
        } else {
            b3 = t2;
        }
        if (b(this.n[b3][b2], this.n[(t2 + 2) % 4][b2], B)) {
            b3 = (byte) ((t2 + 2) % 4);
        }
        return b(this.n[b3][b2], this.n[(t2 + 3) % 4][b2], B) ? (byte) ((t2 + 3) % 4) : b3;
    }

    public final byte G() {
        if (this.h == null) {
            return 0;
        }
        return this.h.d();
    }

    public final boolean a(byte b2, char c2, String str, int i2) {
        byte[] bArr = this.h.c[b2];
        if (bArr != null) {
            byte a2 = a(c2);
            if (a2 < 0 || a2 > 3) {
                return false;
            }
            if (str == null || str.equals("")) {
                return false;
            }
            int i3 = 0;
            for (int i4 = 0; i4 < bArr.length; i4++) {
                if (c2 == '*' || g(bArr[i4]) == a2) {
                    int i5 = 0;
                    while (true) {
                        if (i5 >= str.length()) {
                            break;
                        } else if (d.b(bArr[i4]) == str.charAt(i5)) {
                            i3++;
                            break;
                        } else {
                            i5++;
                        }
                    }
                }
            }
            if (i3 >= i2) {
                return true;
            }
        }
        return false;
    }

    public final boolean v(byte b2) {
        return c(b2, 'H') >= 4 || c(b2, 'S') >= 4;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.b.c(byte, char):int
     arg types: [byte, int]
     candidates:
      a.b.c(byte, byte):byte
      a.b.c(byte, char):int */
    public final boolean w(byte b2) {
        if (c(b2, 'S') == 1) {
            return true;
        }
        if (c(b2, 'H') == 1) {
            return true;
        }
        if (c(b2, 'D') == 1) {
            return true;
        }
        if (c(b2, 'C') == 1) {
            return true;
        }
        return false;
    }

    public final boolean a(byte b2, char c2, boolean z) {
        if (a(b2, c2, "A", 1)) {
            return true;
        }
        if (a(b2, c2, "KQ", 2)) {
            return true;
        }
        if (a(b2, c2, "QJT", 3)) {
            return true;
        }
        if (a(b2, c2, "JT98", 4)) {
            return true;
        }
        if (!z) {
            return false;
        }
        if (a(b2, c2, "K", 1) && c(b2, c2) >= 2) {
            return true;
        }
        if (!a(b2, c2, "QJ", 2) || c(b2, c2) < 3) {
            return a(b2, c2, "JT9", 3) && c(b2, c2) >= 4;
        }
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.b.c(byte, char):int
     arg types: [byte, int]
     candidates:
      a.b.c(byte, byte):byte
      a.b.c(byte, char):int */
    public final boolean x(byte b2) {
        if (c(b2, 'S') == 0) {
            return true;
        }
        if (c(b2, 'H') == 0) {
            return true;
        }
        if (c(b2, 'D') == 0) {
            return true;
        }
        if (c(b2, 'C') == 0) {
            return true;
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.b.c(byte, char):int
     arg types: [byte, int]
     candidates:
      a.b.c(byte, byte):byte
      a.b.c(byte, char):int */
    public final boolean a(byte b2, boolean z) {
        if (c(b2, 'S') < 2 || ((c(b2, 'S') > 4 && !z) || c(b2, 'S') > 5)) {
            return false;
        }
        if (c(b2, 'H') < 2 || ((c(b2, 'H') > 4 && !z) || c(b2, 'H') > 5)) {
            return false;
        }
        if (c(b2, 'D') < 2 || c(b2, 'D') > 5) {
            return false;
        }
        if (c(b2, 'C') < 2 || c(b2, 'C') > 5) {
            return false;
        }
        return true;
    }

    public final boolean H() {
        if (b(0) != 99 || b(-1) != 99 || !z(b(-2)) || ((b(-3) != 99 && b(-3) != -1) || ((!z(b(-4)) && b(-4) != -1) || b(-5) != -1))) {
            return false;
        }
        return true;
    }

    public static boolean y(byte b2) {
        byte e2 = e(b2);
        return E(e2) || F(e2);
    }

    public static boolean b(byte b2, byte b3, byte b4) {
        if (c[d.b[b3]] == c[d.b[b2]]) {
            return d.d[b3] > d.d[b2];
        }
        if (c[d.b[b3]] == b4) {
            return true;
        }
        return false;
    }

    public final boolean I() {
        if (this.i < 3) {
            return false;
        }
        return b(0) == 99 && b(-1) == 99 && b(-2) == 99;
    }

    public static boolean z(byte b2) {
        if (b2 < 10 || b2 > 74 || b2 % 10 > 4) {
            return false;
        }
        return true;
    }

    public final boolean A(byte b2) {
        if (b2 < 0) {
            return false;
        }
        if (this.v < 0) {
            return false;
        }
        return b2 == this.v;
    }

    public final boolean B(byte b2) {
        if (b2 < 0) {
            return false;
        }
        if (this.v < 0) {
            return false;
        }
        return b2 % 2 != this.v % 2;
    }

    public final boolean C(byte b2) {
        if (b2 < 0) {
            return false;
        }
        if (this.v < 0) {
            return false;
        }
        return b2 == (this.v + 2) % 4;
    }

    public final boolean J() {
        return (this.t == -1 || this.t == 99 || this.m != -1) ? false : true;
    }

    public static boolean D(byte b2) {
        if (!z(b2)) {
            return false;
        }
        byte e2 = e(b2);
        if (e2 == 4 && h(b2) >= 3) {
            return true;
        }
        if (!E(e2) || h(b2) < 4) {
            return F(e2) && h(b2) >= 5;
        }
        return true;
    }

    public final boolean K() {
        if (this.t == 99) {
            return true;
        }
        if (this.p[0] + this.p[1] + this.q[0] + this.q[1] >= 13) {
            return true;
        }
        return this.m >= 51;
    }

    public static boolean E(byte b2) {
        return b2 == 3 || b2 == 2;
    }

    public static boolean F(byte b2) {
        return b2 == 0 || b2 == 1;
    }

    public final boolean L() {
        for (byte b2 = this.i; b2 >= 0; b2 = (byte) (b2 - 2)) {
            if (b(b2 + 1) != 99 && b(b2 + 1) != 97) {
                return false;
            }
        }
        return true;
    }

    public final boolean M() {
        return G(this.t);
    }

    public static boolean G(byte b2) {
        return b2 % 10 == 4;
    }

    public final boolean N() {
        if (this.i > 2) {
            return false;
        }
        for (byte b2 = 0; b2 < this.i + 1; b2 = (byte) (b2 + 1)) {
            if (b(b2 + 1) != 99) {
                return false;
            }
        }
        return true;
    }

    public static boolean g(byte b2, byte b3) {
        return b2 % 2 != b3 % 2;
    }

    public static boolean h(byte b2, byte b3) {
        if (b2 == b3) {
            return false;
        }
        return b2 % 2 == b3 % 2;
    }

    public final boolean H(byte b2) {
        int o2 = ((b2 + 4) - o()) % 4;
        if (this.i < o2) {
            return false;
        }
        return b(o2 + 1) != 99;
    }

    public final boolean O() {
        if (!z(b(0))) {
            return false;
        }
        for (int i2 = 1; i2 <= 3; i2++) {
            if (b(-i2) != 99 && b(-i2) != -1) {
                return false;
            }
        }
        return true;
    }

    public final boolean I(byte b2) {
        return k(b2) + c(b2, 'S') >= 15;
    }

    public final boolean J(byte b2) {
        return (k(b2) + c(b2, b(b2, 1))) + c(b2, b(b2, 2)) >= 20;
    }

    public static boolean i(byte b2, byte b3) {
        return b2 % 2 == b3 % 2;
    }

    public final boolean j(byte b2, byte b3) {
        for (byte b4 = 0; b4 < this.x; b4 = (byte) (b4 + 1)) {
            if (this.l[b2][b4] != -1 && c[d.b[this.l[b2][b4]]] == b3) {
                return false;
            }
        }
        return true;
    }

    public final boolean K(byte b2) {
        return C() > b2;
    }

    public final boolean P() {
        return (this.m + 1) % 4 == 0;
    }

    public final boolean L(byte b2) {
        if (b2 == 99) {
            return true;
        }
        if (b2 == 98) {
            if (b(0) == 97) {
                return true;
            }
            if (b(0) == 99 && b(-1) == 99 && b(-2) == 97) {
                return true;
            }
            return false;
        } else if (b2 == 97) {
            if (z(b(0))) {
                return true;
            }
            if (b(0) == 99 && b(-1) == 99 && z(b(-2))) {
                return true;
            }
            return false;
        } else if (z(b2)) {
            return b2 > w();
        } else {
            return false;
        }
    }

    public final boolean k(byte b2, byte b3) {
        byte C = C();
        byte t2 = t(C);
        if (b2 == -1) {
            return false;
        }
        if ((this.m + 1) % 4 == 0) {
            return true;
        }
        try {
            byte b4 = c[d.b[this.n[t2][C]]];
            if (c[d.b[b2]] == b4) {
                return true;
            }
            for (byte b5 = 0; b5 < this.x - C; b5 = (byte) (b5 + 1)) {
                if (this.l[b3][b5] != -1 && c[d.b[this.l[b3][b5]]] == b4) {
                    return false;
                }
            }
            return true;
        } catch (Exception e2) {
            return false;
        }
    }

    public final boolean Q() {
        if (this.h == null) {
            return false;
        }
        return l(this.h.e, this.v);
    }

    public static boolean l(byte b2, byte b3) {
        if (b3 < 0) {
            return false;
        }
        switch (b2) {
            case 0:
                return false;
            case 1:
                return b3 % 2 == 0;
            case 2:
                return b3 % 2 == 1;
            default:
                return true;
        }
    }

    public final byte M(byte b2) {
        return b(b2 + 1);
    }

    public final byte d(int i2) {
        return b(i2 + 1);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.b.a(byte[], boolean):void
     arg types: [byte[], int]
     candidates:
      a.b.a(int, byte):byte
      a.b.a(byte[], byte):byte
      a.b.a(byte[][], int):byte
      a.b.a(byte, byte):java.lang.String
      a.b.a(byte, int):byte
      a.b.a(int, int):byte
      a.b.a(byte, char):int
      a.b.a(byte, boolean):boolean
      a.b.a(byte[], boolean):void */
    public final void N(byte b2) {
        byte C = C();
        byte j2 = j();
        int i2 = 0;
        while (true) {
            if (i2 >= this.x - C) {
                break;
            } else if (this.l[j2][i2] == -1 || this.l[j2][i2] != b2) {
                i2++;
            } else {
                if (i2 != (this.x - C) - 1) {
                    this.l[j2][i2] = this.l[j2][(this.x - C) - 1];
                }
                this.l[j2][(this.x - C) - 1] = -1;
            }
        }
        this.n[j2][C] = b2;
        if ((this.m + 2) % 4 == 0) {
            byte u2 = u(C);
            this.o[C] = u2;
            byte[] bArr = this.p;
            int i3 = u2 % 2;
            bArr[i3] = (byte) (bArr[i3] + 1);
        }
        a(this.l[j2], true);
        this.m = (byte) (this.m + 1);
    }

    public final void O(byte b2) {
        this.k[b2] = true;
    }

    public final boolean c(String str) {
        if (str == null) {
            return false;
        }
        if (str.equals("0") || str.equals("")) {
            return true;
        }
        StringBuilder sb = new StringBuilder(str);
        while (sb.length() > 0) {
            if (this.i >= this.j.length) {
                return false;
            }
            if (I()) {
                return false;
            }
            if (sb.toString().startsWith("P")) {
                if (!L((byte) 99)) {
                    return false;
                }
                a((byte) 99);
                sb.delete(0, 1);
            } else if (sb.toString().startsWith("X")) {
                if (!L((byte) 97)) {
                    return false;
                }
                a((byte) 97);
                sb.delete(0, 1);
            } else if (sb.toString().startsWith("R")) {
                if (!L((byte) 98)) {
                    return false;
                }
                a((byte) 98);
                sb.delete(0, 1);
            } else if (!sb.substring(0, 1).matches("[1-7]")) {
                return false;
            } else {
                if (sb.length() < 2) {
                    return false;
                }
                if (sb.substring(1, 2).matches("[CDHS]")) {
                    if (!L(a(sb.substring(0, 2)))) {
                        return false;
                    }
                    a(a(sb.substring(0, 2)));
                    sb.delete(0, 2);
                } else if (!sb.toString().substring(1, 2).equals("N")) {
                    return false;
                } else {
                    if (!L(a(sb.substring(0, 2) + "T"))) {
                        return false;
                    }
                    a(a(sb.substring(0, 2) + "T"));
                    sb.delete(0, 2);
                }
            }
        }
        return true;
    }

    public final void P(byte b2) {
        this.i = b2;
    }

    public final void a(a aVar) {
        this.h = aVar;
    }

    public final void Q(byte b2) {
        this.m = b2;
    }

    public final void R(byte b2) {
        this.t = b2;
    }

    public final void S(byte b2) {
        this.u = b2;
    }

    public final void T(byte b2) {
        this.v = b2;
        this.w = (byte) ((b2 + 1) % 4);
    }

    public final void e(int i2) {
        T((byte) i2);
    }

    public final boolean d(String str) {
        if (str == null) {
            return false;
        }
        if (this.t == 99) {
            return true;
        }
        if (str.length() == 0) {
            return false;
        }
        if (str.length() % 2 != 0) {
            return false;
        }
        this.m = -1;
        g.a(this.n);
        g.b(this.o, (byte) -1);
        g.b(this.p, (byte) 0);
        g.b(this.q, (byte) 0);
        for (int i2 = 0; i2 < str.length() / 2; i2++) {
            if (!str.substring(i2 * 2, (i2 * 2) + 1).equals("*")) {
                try {
                    byte a2 = d.a(str.substring(i2 * 2, (i2 * 2) + 2));
                    if (a2 < 0 || a2 > 51) {
                        return false;
                    }
                    N(a2);
                } catch (Exception e2) {
                    return false;
                }
            } else if (str.length() != (i2 * 2) + 2) {
                return false;
            } else {
                try {
                    int parseInt = Integer.parseInt(str.substring((i2 * 2) + 1, (i2 * 2) + 2), 16);
                    this.q[this.v % 2] = (byte) parseInt;
                    this.q[(this.v + 1) % 2] = (byte) (((13 - this.p[this.v % 2]) - this.p[(this.v + 1) % 2]) - parseInt);
                } catch (Exception e3) {
                    return false;
                }
            }
        }
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.b.a(byte[], boolean):void
     arg types: [byte[], int]
     candidates:
      a.b.a(int, byte):byte
      a.b.a(byte[], byte):byte
      a.b.a(byte[][], int):byte
      a.b.a(byte, byte):java.lang.String
      a.b.a(byte, int):byte
      a.b.a(int, int):byte
      a.b.a(byte, char):int
      a.b.a(byte, boolean):boolean
      a.b.a(byte[], boolean):void */
    public static void b(byte[] bArr) {
        a(bArr, true);
    }

    public static void a(byte[] bArr, boolean z) {
        byte b2 = 0;
        for (byte b3 = 0; b3 < bArr.length; b3 = (byte) (b3 + 1)) {
            if (bArr[b3] >= 0) {
                b2 = (byte) (b2 + 1);
            }
        }
        g.a(bArr);
        g.a(bArr, b2 - 1, z, b);
    }

    public final void R() {
        if (this.i >= 0) {
            this.j[this.i] = 0;
            this.k[this.i] = false;
            this.i = (byte) (this.i - 1);
        }
    }

    public final void S() {
        this.m = (byte) (this.m - 1);
        byte C = C();
        byte j2 = j();
        if ((this.m + 2) % 4 == 0) {
            byte b2 = this.o[C];
            byte[] bArr = this.p;
            int i2 = b2 % 2;
            bArr[i2] = (byte) (bArr[i2] - 1);
            this.o[C] = -1;
        }
        byte[] bArr2 = this.q;
        this.q[1] = 0;
        bArr2[0] = 0;
        byte b3 = this.n[j2][C];
        this.n[j2][C] = -1;
        this.l[j2][(this.x - C) - 1] = b3;
    }
}
