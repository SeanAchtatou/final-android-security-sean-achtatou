package a;

/* compiled from: ProGuard */
public final class c {

    /* renamed from: a  reason: collision with root package name */
    public int f2a;
    public int b;
    public int c;
    public byte d;
    public byte e;
    public byte f;
    public byte g;
    public byte h;
    public long i;
    public String[] j;
    public boolean k;
    private b l;

    public c() {
        this.k = false;
        this.i = System.currentTimeMillis();
        this.l = null;
    }

    public c(b bVar) {
        this();
        this.l = bVar;
    }

    public final void a() {
        this.j = null;
        this.l = null;
    }

    public final b b() {
        return this.l;
    }

    public final byte c() {
        if (this.l != null) {
            return this.l.k();
        }
        return this.f;
    }

    public final byte d() {
        if (this.l != null) {
            return this.l.m();
        }
        return this.g;
    }

    public final int e() {
        if (this.l == null || this.l.h == null) {
            return this.c;
        }
        return this.l.h.b;
    }

    public final int f() {
        if (this.l == null || this.l.h == null) {
            return this.b;
        }
        return this.l.h.f0a;
    }

    public final byte g() {
        if (this.l != null) {
            return this.l.s();
        }
        return this.e;
    }

    public final String a(boolean z) {
        StringBuilder sb = new StringBuilder();
        try {
            if (this.f2a > 0 && z) {
                sb.append(Integer.toString(this.f2a) + ". ");
            }
            if (c() == 99) {
                sb.append("All Pass");
            } else if (g() < 0 || g() > 3) {
                sb.append("*ERROR*");
            } else {
                sb.append(b.g[g()].substring(0, 1) + " ");
                sb.append(Byte.toString(b.c(c())));
                sb.append(b.e[b.e(c())]);
                if (d() == 97) {
                    sb.append("X");
                }
                if (d() == 98) {
                    sb.append("XX");
                }
                if (b.b(c(), h()) >= 0) {
                    sb.append("+" + Integer.toString(b.b(c(), h())));
                } else {
                    sb.append(Integer.toString(b.b(c(), h())));
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return sb.toString();
    }

    public final byte h() {
        if (this.l != null) {
            try {
                return (byte) (this.l.p[g() % 2] + this.l.q[g() % 2]);
            } catch (Exception e2) {
            }
        }
        return this.h;
    }

    public final byte i() {
        if (this.l == null || this.l.h == null) {
            return this.d;
        }
        return this.l.G();
    }
}
