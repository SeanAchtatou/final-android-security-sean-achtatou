package at.aauer1.battery;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import at.aauer1.battery.service.BatteryService;

public class Autostart extends BroadcastReceiver {
    static final String TAG = "Autostart";

    public void onReceive(Context context, Intent intent) {
        context.startService(new Intent(context, BatteryService.class));
    }
}
