package at.aauer1.battery.theme;

import android.graphics.drawable.Drawable;

public class ThemeInfo {
    public String authority = null;
    public String class_name = null;
    public Drawable icon = null;
    public String label = null;
    public String package_name = null;
    public int theme_id = 0;

    public String toString() {
        return String.valueOf(new String("")) + "[" + this.label + "] " + this.package_name + "; " + this.class_name + "; " + this.authority;
    }
}
