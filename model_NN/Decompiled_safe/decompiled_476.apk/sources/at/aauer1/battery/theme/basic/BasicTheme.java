package at.aauer1.battery.theme.basic;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.util.Log;
import android.widget.RemoteViews;
import at.aauer1.battery.R;
import at.aauer1.battery.SmartBatteryMonitor;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;

public class BasicTheme extends IntentService {
    private static final String TAG = "BasicTheme";
    private static final double a = 0.0d;
    private static final double b = 0.0d;
    private static final double c = 84.0d;
    private static final double d = 500.0d;
    private static NotificationManager nm = null;
    private static Notification notification = null;

    public BasicTheme() {
        super(TAG);
        Log.d(TAG, "constructor");
    }

    public BasicTheme(String name) {
        super(name);
    }

    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate");
    }

    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        Log.d(TAG, "onStart");
    }

    /* access modifiers changed from: protected */
    public void onHandleIntent(Intent intent) {
        String action = intent.getAction();
        Log.d(TAG, "onHandleIntent");
        if (nm == null) {
            nm = (NotificationManager) getSystemService("notification");
        }
        if (action == null) {
            return;
        }
        if (action.equals("at.aauer1.battery.theme.SHOW_NOTIFICATION")) {
            notify(intent.getIntExtra(getString(R.string.key_level), 50), intent.getStringExtra(getString(R.string.key_title)), intent.getStringExtra(getString(R.string.key_message)));
            Log.d(TAG, "show notification");
        } else if (action.equals("at.aauer1.battery.theme.CANCEL_NOTIFICATION")) {
            nm.cancelAll();
            Log.d(TAG, "cancel notification");
        } else if (action.equals("at.aauer1.battery.theme.THEME")) {
            Log.d(TAG, "query theme");
        }
    }

    private void setRemoteViewLevel(RemoteViews remoteview, int level) {
        try {
            Method m = Class.forName(remoteview.getClass().getName()).getDeclaredMethod("setDrawableParameters", Integer.TYPE, Boolean.TYPE, Integer.TYPE, Integer.TYPE, PorterDuff.Mode.class, Integer.TYPE);
            m.setAccessible(true);
            m.invoke(remoteview, Integer.valueOf((int) R.id.notification_icon), false, -1, -1, null, Integer.valueOf(level));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void notify(int value, String title, String message) {
        int level = (int) ((((double) (value * value * value)) * 0.0d) + (((double) (value * value)) * 0.0d) + (c * ((double) value)) + d);
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");
        if (notification == null) {
            notification = new Notification(R.drawable.battery, title, System.currentTimeMillis());
            Intent intent = new Intent("android.intent.action.MAIN");
            intent.setClassName(SmartBatteryMonitor.AUTHORITY, "at.aauer1.battery.SmartBatteryMonitor");
            intent.addFlags(270532608);
            notification.contentIntent = PendingIntent.getActivity(this, 0, intent, 0);
            notification.contentView = new RemoteViews(getPackageName(), (int) R.layout.notification);
            notification.flags = 46;
        }
        notification.contentView.setTextViewText(R.id.notification_title, title);
        notification.contentView.setTextViewText(R.id.notification_text, message);
        notification.contentView.setTextViewText(R.id.notification_time, formatter.format(Long.valueOf(System.currentTimeMillis())));
        setRemoteViewLevel(notification.contentView, level);
        notification.iconLevel = level;
        try {
            nm.notify(1, notification);
        } catch (RuntimeException e) {
        }
    }
}
