package at.aauer1.battery.theme.basic;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.SQLException;
import android.net.Uri;
import at.aauer1.battery.R;

public class BasicThemeProvider extends ContentProvider {
    private static final String BATTERY_LARGE = "battery_large";
    private static final String TAG = "DefaultProvider";
    private static final int THEME_ITEMS = 1;
    private static final UriMatcher uri_matcher = new UriMatcher(-1);

    static {
        uri_matcher.addURI("at.aauer1.battery.theme.basic", "items", THEME_ITEMS);
    }

    public int delete(Uri arg0, String arg1, String[] arg2) {
        throw new SQLException("Operation not supported");
    }

    public String getType(Uri arg0) {
        return null;
    }

    public Uri insert(Uri arg0, ContentValues arg1) {
        throw new SQLException("Operation not supported");
    }

    public boolean onCreate() {
        return false;
    }

    public Cursor query(Uri uri, String[] projection, String selection, String[] selection_args, String sort_order) {
        String[] columns = new String[4];
        columns[0] = "ID";
        columns[THEME_ITEMS] = "resource";
        columns[2] = "width";
        columns[3] = "height";
        MatrixCursor cursor = new MatrixCursor(columns);
        switch (uri_matcher.match(uri)) {
            case THEME_ITEMS /*1*/:
                Object[] values = new Object[4];
                values[0] = new String(BATTERY_LARGE);
                values[THEME_ITEMS] = new Integer((int) R.drawable.battery_large);
                values[2] = new Integer(240);
                values[3] = new Integer(141);
                cursor.addRow(values);
                return cursor;
            default:
                return null;
        }
    }

    public int update(Uri arg0, ContentValues arg1, String arg2, String[] arg3) {
        throw new SQLException("Operation not supported");
    }
}
