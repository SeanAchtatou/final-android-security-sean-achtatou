package at.aauer1.battery.theme;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.content.pm.ResolveInfo;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

public class ThemeManager {
    private static ThemeManager instance = null;
    private HashMap<String, ThemeInfo> theme_info = new HashMap<>();

    private ThemeManager() {
    }

    public static ThemeManager getInstance() {
        if (instance == null) {
            instance = new ThemeManager();
        }
        return instance;
    }

    public int queryThemes(PackageManager manager) {
        Intent intent = new Intent("at.aauer1.battery.theme.THEME");
        intent.addCategory("android.intent.category.ALTERNATIVE");
        List<ResolveInfo> themes = manager.queryIntentServices(intent, 0);
        List<ProviderInfo> provider_info = manager.queryContentProviders(null, 0, 0);
        this.theme_info.clear();
        for (int i = 0; i < themes.size(); i++) {
            ResolveInfo theme = themes.get(i);
            ThemeInfo info = new ThemeInfo();
            info.icon = theme.loadIcon(manager);
            info.label = String.valueOf(theme.loadLabel(manager));
            info.package_name = theme.serviceInfo.packageName;
            info.class_name = theme.serviceInfo.name;
            int j = 0;
            while (true) {
                if (j < provider_info.size()) {
                    ProviderInfo temp = provider_info.get(j);
                    if (temp.packageName.equals(info.package_name) && temp.authority.contains("theme")) {
                        info.authority = temp.authority;
                        break;
                    }
                    j++;
                } else {
                    break;
                }
            }
            info.theme_id = -1;
            System.out.println(info.toString());
            this.theme_info.put(info.label, info);
        }
        return this.theme_info.size();
    }

    public Set<String> getThemeNames() {
        return this.theme_info.keySet();
    }

    public ThemeInfo getThemeInfo(String name) {
        return this.theme_info.get(name);
    }

    public ThemeInfo getDefaultThemeInfo() {
        return this.theme_info.get("Default");
    }

    public boolean hasThemes() {
        return !this.theme_info.isEmpty();
    }
}
