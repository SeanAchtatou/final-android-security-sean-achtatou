package at.aauer1.battery;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.preference.CheckBoxPreference;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;

public class IconCheckBoxPreference extends CheckBoxPreference {
    private Drawable icon;

    public IconCheckBoxPreference(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.icon = null;
        setLayoutResource(R.layout.iconcheckbox_preference);
    }

    public IconCheckBoxPreference(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public IconCheckBoxPreference(Context context) {
        this(context, null, 0);
    }

    /* access modifiers changed from: protected */
    public void onBindView(View view) {
        super.onBindView(view);
        ImageView imageview = (ImageView) view.findViewById(R.id.icon);
        if (imageview != null && this.icon != null) {
            imageview.setImageDrawable(this.icon);
        }
    }

    public void setIcon(Drawable icon2) {
        if ((icon2 == null && this.icon != null) || (icon2 != null && !icon2.equals(this.icon))) {
            this.icon = icon2;
            notifyChanged();
        }
    }

    public Drawable getIcon() {
        return this.icon;
    }
}
