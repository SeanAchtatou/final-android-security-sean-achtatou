package at.aauer1.battery;

public final class R {

    public static final class array {
        public static final int preferences_language_entries = 2131034112;
        public static final int preferences_language_values = 2131034113;
    }

    public static final class attr {
    }

    public static final class drawable {
        public static final int bat000 = 2130837504;
        public static final int bat001 = 2130837505;
        public static final int bat002 = 2130837506;
        public static final int bat003 = 2130837507;
        public static final int bat004 = 2130837508;
        public static final int bat005 = 2130837509;
        public static final int bat006 = 2130837510;
        public static final int bat007 = 2130837511;
        public static final int bat008 = 2130837512;
        public static final int bat009 = 2130837513;
        public static final int bat010 = 2130837514;
        public static final int bat011 = 2130837515;
        public static final int bat012 = 2130837516;
        public static final int bat013 = 2130837517;
        public static final int bat014 = 2130837518;
        public static final int bat015 = 2130837519;
        public static final int bat016 = 2130837520;
        public static final int bat017 = 2130837521;
        public static final int bat018 = 2130837522;
        public static final int bat019 = 2130837523;
        public static final int bat020 = 2130837524;
        public static final int bat021 = 2130837525;
        public static final int bat022 = 2130837526;
        public static final int bat023 = 2130837527;
        public static final int bat024 = 2130837528;
        public static final int bat025 = 2130837529;
        public static final int bat026 = 2130837530;
        public static final int bat027 = 2130837531;
        public static final int bat028 = 2130837532;
        public static final int bat029 = 2130837533;
        public static final int bat030 = 2130837534;
        public static final int bat031 = 2130837535;
        public static final int bat032 = 2130837536;
        public static final int bat033 = 2130837537;
        public static final int bat034 = 2130837538;
        public static final int bat035 = 2130837539;
        public static final int bat036 = 2130837540;
        public static final int bat037 = 2130837541;
        public static final int bat038 = 2130837542;
        public static final int bat039 = 2130837543;
        public static final int bat040 = 2130837544;
        public static final int bat041 = 2130837545;
        public static final int bat042 = 2130837546;
        public static final int bat043 = 2130837547;
        public static final int bat044 = 2130837548;
        public static final int bat045 = 2130837549;
        public static final int bat046 = 2130837550;
        public static final int bat047 = 2130837551;
        public static final int bat048 = 2130837552;
        public static final int bat049 = 2130837553;
        public static final int bat050 = 2130837554;
        public static final int bat051 = 2130837555;
        public static final int bat052 = 2130837556;
        public static final int bat053 = 2130837557;
        public static final int bat054 = 2130837558;
        public static final int bat055 = 2130837559;
        public static final int bat056 = 2130837560;
        public static final int bat057 = 2130837561;
        public static final int bat058 = 2130837562;
        public static final int bat059 = 2130837563;
        public static final int bat060 = 2130837564;
        public static final int bat061 = 2130837565;
        public static final int bat062 = 2130837566;
        public static final int bat063 = 2130837567;
        public static final int bat064 = 2130837568;
        public static final int bat065 = 2130837569;
        public static final int bat066 = 2130837570;
        public static final int bat067 = 2130837571;
        public static final int bat068 = 2130837572;
        public static final int bat069 = 2130837573;
        public static final int bat070 = 2130837574;
        public static final int bat071 = 2130837575;
        public static final int bat072 = 2130837576;
        public static final int bat073 = 2130837577;
        public static final int bat074 = 2130837578;
        public static final int bat075 = 2130837579;
        public static final int bat076 = 2130837580;
        public static final int bat077 = 2130837581;
        public static final int bat078 = 2130837582;
        public static final int bat079 = 2130837583;
        public static final int bat080 = 2130837584;
        public static final int bat081 = 2130837585;
        public static final int bat082 = 2130837586;
        public static final int bat083 = 2130837587;
        public static final int bat084 = 2130837588;
        public static final int bat085 = 2130837589;
        public static final int bat086 = 2130837590;
        public static final int bat087 = 2130837591;
        public static final int bat088 = 2130837592;
        public static final int bat089 = 2130837593;
        public static final int bat090 = 2130837594;
        public static final int bat091 = 2130837595;
        public static final int bat092 = 2130837596;
        public static final int bat093 = 2130837597;
        public static final int bat094 = 2130837598;
        public static final int bat095 = 2130837599;
        public static final int bat096 = 2130837600;
        public static final int bat097 = 2130837601;
        public static final int bat098 = 2130837602;
        public static final int bat099 = 2130837603;
        public static final int bat100 = 2130837604;
        public static final int bat_icon = 2130837605;
        public static final int bat_percent = 2130837606;
        public static final int battery = 2130837607;
        public static final int battery_bg = 2130837608;
        public static final int battery_green = 2130837609;
        public static final int battery_green_large = 2130837610;
        public static final int battery_large = 2130837611;
        public static final int battery_orange = 2130837612;
        public static final int battery_orange_large = 2130837613;
        public static final int battery_outline_large = 2130837614;
        public static final int battery_overlay = 2130837615;
        public static final int battery_overlay_large = 2130837616;
        public static final int battery_red = 2130837617;
        public static final int battery_red_large = 2130837618;
        public static final int ic_menu_cc = 2130837619;
        public static final int ic_menu_info_details = 2130837620;
        public static final int ic_menu_preferences = 2130837621;
        public static final int ic_menu_recent_history = 2130837622;
        public static final int ic_menu_search = 2130837623;
        public static final int icon = 2130837624;
        public static final int icon_old = 2130837625;
        public static final int paypal_donate = 2130837626;
    }

    public static final class id {
        public static final int aboutscrollview = 2131230720;
        public static final int battery_green_large = 2131230742;
        public static final int battery_headline = 2131230733;
        public static final int battery_image = 2131230729;
        public static final int battery_info = 2131230734;
        public static final int battery_large_linear = 2131230730;
        public static final int battery_orange_large = 2131230743;
        public static final int battery_outline_large = 2131230741;
        public static final int battery_overlay_large = 2131230744;
        public static final int battery_subtext = 2131230732;
        public static final int battery_text = 2131230731;
        public static final int dialog_changelog = 2131230721;
        public static final int dialog_image = 2131230724;
        public static final int dialog_info_donate = 2131230727;
        public static final int dialog_info_text = 2131230726;
        public static final int dialog_text = 2131230725;
        public static final int icon = 2131230722;
        public static final int layout_root = 2131230723;
        public static final int main_layout = 2131230728;
        public static final int menu_addons = 2131230746;
        public static final int menu_changelog = 2131230749;
        public static final int menu_info = 2131230748;
        public static final int menu_partner_apps = 2131230747;
        public static final int menu_settings = 2131230745;
        public static final int mobFoxView = 2131230735;
        public static final int notification_icon = 2131230736;
        public static final int notification_text = 2131230738;
        public static final int notification_time = 2131230739;
        public static final int notification_title = 2131230737;
        public static final int widgetText = 2131230740;
    }

    public static final class layout {
        public static final int changelog_view = 2130903040;
        public static final int iconcheckbox_preference = 2130903041;
        public static final int info_dialog = 2130903042;
        public static final int main = 2130903043;
        public static final int notification = 2130903044;
        public static final int widgetview = 2130903045;
    }

    public static final class menu {
        public static final int menu = 2131165184;
    }

    public static final class string {
        public static final int app_name = 2131099663;
        public static final int battery_headline = 2131099686;
        public static final int battery_health = 2131099689;
        public static final int battery_health_dead = 2131099696;
        public static final int battery_health_failure = 2131099698;
        public static final int battery_health_good = 2131099694;
        public static final int battery_health_over_voltage = 2131099697;
        public static final int battery_health_overheat = 2131099695;
        public static final int battery_health_unknown = 2131099693;
        public static final int battery_last_charge = 2131099706;
        public static final int battery_last_discharge = 2131099707;
        public static final int battery_level = 2131099687;
        public static final int battery_status = 2131099688;
        public static final int battery_status_ac = 2131099704;
        public static final int battery_status_charging = 2131099700;
        public static final int battery_status_discharging = 2131099701;
        public static final int battery_status_full = 2131099702;
        public static final int battery_status_na = 2131099699;
        public static final int battery_status_not_charging = 2131099703;
        public static final int battery_status_usb = 2131099705;
        public static final int battery_technology = 2131099692;
        public static final int battery_temperature = 2131099691;
        public static final int battery_voltage = 2131099690;
        public static final int db_event_last_charged = 2131099661;
        public static final int db_event_last_discharged = 2131099662;
        public static final int dialog_changelog = 2131099714;
        public static final int dialog_info_donate = 2131099713;
        public static final int dialog_info_text = 2131099712;
        public static final int key_changelog = 2131099653;
        public static final int key_fahrenheit = 2131099650;
        public static final int key_language = 2131099652;
        public static final int key_level = 2131099657;
        public static final int key_message = 2131099659;
        public static final int key_overlay = 2131099649;
        public static final int key_package = 2131099660;
        public static final int key_service = 2131099648;
        public static final int key_theme = 2131099651;
        public static final int key_timeduration = 2131099654;
        public static final int key_title = 2131099658;
        public static final int key_vibration_empty = 2131099656;
        public static final int key_vibration_full = 2131099655;
        public static final int menu_addons = 2131099666;
        public static final int menu_changelog = 2131099668;
        public static final int menu_info = 2131099664;
        public static final int menu_partner_apps = 2131099667;
        public static final int menu_settings = 2131099665;
        public static final int preferences_fahrenheit = 2131099675;
        public static final int preferences_fahrenheit_summary = 2131099676;
        public static final int preferences_general = 2131099669;
        public static final int preferences_language = 2131099683;
        public static final int preferences_language_summary = 2131099684;
        public static final int preferences_overlay = 2131099673;
        public static final int preferences_overlay_summary = 2131099674;
        public static final int preferences_service = 2131099671;
        public static final int preferences_service_summary = 2131099672;
        public static final int preferences_signals = 2131099670;
        public static final int preferences_themes = 2131099685;
        public static final int preferences_timeduration = 2131099677;
        public static final int preferences_timeduration_summary = 2131099678;
        public static final int preferences_vibration_empty = 2131099681;
        public static final int preferences_vibration_empty_summary = 2131099682;
        public static final int preferences_vibration_full = 2131099679;
        public static final int preferences_vibration_full_summary = 2131099680;
        public static final int since = 2131099708;
        public static final int time_format_long_p = 2131099709;
        public static final int time_format_long_s = 2131099710;
        public static final int time_format_short = 2131099711;
    }

    public static final class xml {
        public static final int preferences = 2130968576;
    }
}
