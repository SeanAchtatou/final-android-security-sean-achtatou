package at.aauer1.battery.service;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Binder;
import android.os.IBinder;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.util.Log;
import at.aauer1.battery.R;
import at.aauer1.battery.database.BatteryDatabaseLog;
import at.aauer1.battery.theme.ThemeInfo;
import at.aauer1.battery.theme.ThemeManager;

public class BatteryService extends Service {
    private static final String TAG = "BatteryService";
    /* access modifiers changed from: private */
    public static final long[] VIBRATE_EMPTY;
    /* access modifiers changed from: private */
    public static final long[] VIBRATE_FULL;
    private final BatteryBinder binder = new BatteryBinder();
    /* access modifiers changed from: private */
    public BatteryDataConverter converter = null;
    /* access modifiers changed from: private */
    public BatteryDatabaseLog log = null;
    /* access modifiers changed from: private */
    public boolean notification_enabled = false;
    public BroadcastReceiver receiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            BatteryDataItem item = new BatteryDataItem();
            item.fromBroadcast(intent);
            BatteryService.this.log.addItem(item);
            if (BatteryService.this.notification_enabled) {
                BatteryService.this.showNotification(item);
            }
            if (BatteryService.this.vibration_full) {
                if (BatteryService.this.vibration_full_done || item.status != 5) {
                    Log.d(BatteryService.TAG, "NOT vibrating... - full_done: " + BatteryService.this.vibration_full_done);
                } else {
                    BatteryService.this.vibration_full_done = true;
                    BatteryService.this.vibrator.vibrate(BatteryService.VIBRATE_FULL, -1);
                    Log.d(BatteryService.TAG, "Vibrating... - full_done: " + BatteryService.this.vibration_full_done);
                }
            }
            if (BatteryService.this.vibration_empty) {
                if (BatteryService.this.vibration_empty_done || BatteryService.this.converter.getScaledLevel(item) > 15) {
                    Log.d(BatteryService.TAG, "NOT Vibrating... - empty_done: " + BatteryService.this.vibration_empty_done);
                } else {
                    BatteryService.this.vibration_empty_done = true;
                    BatteryService.this.vibrator.vibrate(BatteryService.VIBRATE_EMPTY, -1);
                    Log.d(BatteryService.TAG, "Vibrating... - empty_done: " + BatteryService.this.vibration_empty_done);
                }
            }
            if (!(!BatteryService.this.vibration_full_done || item.plugged == 1 || item.plugged == 2)) {
                Log.d(BatteryService.TAG, "disconnected from charger --> vibration_full_done = false");
                BatteryService.this.vibration_full_done = false;
            }
            if (!BatteryService.this.vibration_empty_done) {
                return;
            }
            if ((item.plugged == 1 || item.plugged == 2) && BatteryService.this.converter.getScaledLevel(item) > 15) {
                BatteryService.this.vibration_empty_done = false;
            }
        }
    };
    private ThemeInfo theme = null;
    private ThemeManager theme_manager = null;
    /* access modifiers changed from: private */
    public boolean vibration_empty = false;
    /* access modifiers changed from: private */
    public boolean vibration_empty_done = false;
    /* access modifiers changed from: private */
    public boolean vibration_full = false;
    /* access modifiers changed from: private */
    public boolean vibration_full_done = false;
    /* access modifiers changed from: private */
    public Vibrator vibrator = null;

    static {
        long[] jArr = new long[2];
        jArr[1] = 300;
        VIBRATE_FULL = jArr;
        long[] jArr2 = new long[4];
        jArr2[1] = 300;
        jArr2[2] = 300;
        jArr2[3] = 300;
        VIBRATE_EMPTY = jArr2;
    }

    public class BatteryBinder extends Binder {
        public BatteryBinder() {
        }

        public BatteryService getService() {
            return BatteryService.this;
        }
    }

    public void onCreate() {
        this.theme_manager = ThemeManager.getInstance();
        this.converter = new BatteryDataConverter(getResources());
        this.vibrator = (Vibrator) getSystemService("vibrator");
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        this.notification_enabled = pref.getBoolean(getString(R.string.key_service), true);
        this.vibration_full = pref.getBoolean(getString(R.string.key_vibration_full), false);
        this.vibration_empty = pref.getBoolean(getString(R.string.key_vibration_empty), false);
        this.log = new BatteryDatabaseLog(this);
        applyTheme(pref.getString(getString(R.string.key_theme), "Default"));
        registerReceiver(this.receiver, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        setForeground(true);
    }

    public void onDestroy() {
        unregisterReceiver(this.receiver);
        setForeground(false);
    }

    public void onStart(Intent intent, int startId) {
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        return 1;
    }

    public IBinder onBind(Intent arg0) {
        return this.binder;
    }

    public void applyTheme(String name) {
        if (this.theme != null) {
            cancelNotification();
        }
        if (!this.theme_manager.hasThemes()) {
            this.theme_manager.queryThemes(getPackageManager());
        }
        this.theme = this.theme_manager.getThemeInfo(name);
        if (this.theme == null) {
            this.theme = this.theme_manager.getDefaultThemeInfo();
        }
        Log.d(TAG, this.theme.toString());
        BatteryDataItem item = this.log.getLastItem();
        if (item != null && this.notification_enabled) {
            showNotification(item);
        }
    }

    public void enableNotification(boolean enabled) {
        this.notification_enabled = enabled;
        if (this.notification_enabled) {
            BatteryDataItem item = this.log.getLastItem();
            if (item != null) {
                showNotification(item);
                return;
            }
            return;
        }
        cancelNotification();
    }

    public boolean isNotificationEnabled() {
        return this.notification_enabled;
    }

    public void enableVibrationFull(boolean vibration) {
        this.vibration_full = vibration;
    }

    public void enableVibrationEmpty(boolean vibration) {
        this.vibration_empty = vibration;
    }

    public ThemeInfo getCurrentTheme() {
        return this.theme;
    }

    public void showNotification() {
        BatteryDataItem item = this.log.getLastItem();
        if (item != null) {
            showNotification(item);
        }
    }

    public synchronized void showNotification(BatteryDataItem item) {
        String message = String.valueOf(String.valueOf(this.converter.getScaledLevel(item))) + "% - " + this.converter.getStatus(item);
        boolean time_duration = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getBoolean(getString(R.string.key_timeduration), true);
        if (item.plugged == 1 || item.plugged == 2) {
            long time = this.log.getLastDischarge();
            if (time != 0) {
                if (time_duration) {
                    message = String.valueOf(message) + " " + getString(R.string.since) + " " + this.converter.formatTimeDifference(System.currentTimeMillis() - time);
                } else {
                    message = String.valueOf(message) + " " + getString(R.string.since) + " " + BatteryDataConverter.formatTime(time);
                }
            }
        } else {
            long time2 = this.log.getLastCharge();
            if (time2 != 0) {
                if (time_duration) {
                    message = String.valueOf(message) + " " + getString(R.string.since) + " " + this.converter.formatTimeDifference(System.currentTimeMillis() - time2);
                } else {
                    message = String.valueOf(message) + " " + getString(R.string.since) + " " + BatteryDataConverter.formatTime(time2);
                }
            }
        }
        Intent i = new Intent("at.aauer1.battery.theme.SHOW_NOTIFICATION");
        i.setClassName(this.theme.package_name, this.theme.class_name);
        i.putExtra(getString(R.string.key_level), this.converter.getScaledLevel(item));
        i.putExtra(getString(R.string.key_title), getString(R.string.app_name));
        i.putExtra(getString(R.string.key_message), message);
        Log.d(TAG, "Show Notification: " + this.theme.class_name);
        try {
            startService(i);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized void cancelNotification() {
        Intent i = new Intent("at.aauer1.battery.theme.CANCEL_NOTIFICATION");
        i.setClassName(this.theme.package_name, this.theme.class_name);
        Log.d(TAG, "Cancel Notification: " + this.theme.class_name);
        try {
            startService(i);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return;
    }
}
