package at.aauer1.battery.service;

import android.content.res.Resources;
import at.aauer1.battery.R;
import java.text.DateFormat;
import java.util.Date;

public class BatteryDataConverter {
    private Resources resources;

    public BatteryDataConverter(Resources res) {
        this.resources = res;
    }

    public String getStatus(BatteryDataItem item) {
        String ret;
        if (!item.present) {
            return this.resources.getString(R.string.battery_status_na);
        }
        if (item.plugged == 1) {
            ret = "(" + this.resources.getString(R.string.battery_status_ac) + ") ";
        } else if (item.plugged == 2) {
            ret = "(" + this.resources.getString(R.string.battery_status_usb) + ") ";
        } else {
            ret = new String();
        }
        switch (item.status) {
            case 2:
                return String.valueOf(ret) + this.resources.getString(R.string.battery_status_charging);
            case 3:
                return String.valueOf(ret) + this.resources.getString(R.string.battery_status_discharging);
            case 4:
                return String.valueOf(ret) + this.resources.getString(R.string.battery_status_not_charging);
            case 5:
                return String.valueOf(ret) + this.resources.getString(R.string.battery_status_full);
            default:
                return ret;
        }
    }

    public String getHealth(BatteryDataItem item) {
        switch (item.health) {
            case 1:
                return this.resources.getString(R.string.battery_health_unknown);
            case 2:
                return this.resources.getString(R.string.battery_health_good);
            case 3:
                return this.resources.getString(R.string.battery_health_overheat);
            case 4:
                return this.resources.getString(R.string.battery_health_dead);
            case 5:
                return this.resources.getString(R.string.battery_health_over_voltage);
            case 6:
                return this.resources.getString(R.string.battery_health_failure);
            default:
                return null;
        }
    }

    public double getTemperatureCelsius(BatteryDataItem item) {
        return ((double) item.temperature) / 10.0d;
    }

    public double getTemperatureFahrenheit(BatteryDataItem item) {
        return (((double) (item.temperature * 9)) / 50.0d) + 32.0d;
    }

    public double getVoltage(BatteryDataItem item) {
        return ((double) item.voltage) / 1000.0d;
    }

    public int getScaledLevel(BatteryDataItem item) {
        return (item.level * 100) / item.scale;
    }

    public String formatTimeDifference(long time) {
        long time2 = (time / 1000) / 60;
        int minutes = (int) (time2 % 60);
        long time3 = time2 / 60;
        int hours = (int) (time3 % 24);
        int days = (int) (time3 / 24);
        if (days > 1) {
            return String.format(this.resources.getString(R.string.time_format_long_p), Integer.valueOf(days), Integer.valueOf(hours), Integer.valueOf(minutes));
        } else if (days == 1) {
            return String.format(this.resources.getString(R.string.time_format_long_s), Integer.valueOf(days), Integer.valueOf(hours), Integer.valueOf(minutes));
        } else {
            return String.format(this.resources.getString(R.string.time_format_short), Integer.valueOf(hours), Integer.valueOf(minutes));
        }
    }

    public static String formatTime(long time) {
        return DateFormat.getTimeInstance(3).format(new Date(time));
    }
}
