package at.aauer1.battery.service;

import android.content.Intent;
import java.util.Date;

public class BatteryDataItem {
    public static final String EXTRA_HEALTH = "health";
    public static final String EXTRA_ICON_SMALL = "icon-small";
    public static final String EXTRA_LEVEL = "level";
    public static final String EXTRA_PLUGGED = "plugged";
    public static final String EXTRA_PRESENT = "present";
    public static final String EXTRA_SCALE = "scale";
    public static final String EXTRA_STATUS = "status";
    public static final String EXTRA_TECHNOLOGY = "technology";
    public static final String EXTRA_TEMPERATURE = "temperature";
    public static final String EXTRA_VOLTAGE = "voltage";
    public Date date;
    public int health;
    public int level;
    public int plugged;
    public boolean present;
    public int scale;
    public int status;
    public String technology;
    public int temperature;
    public int voltage;

    public String toString() {
        return String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(new String("")) + "[" + this.date.toLocaleString() + "]") + " Level: " + String.valueOf(this.level)) + " Scale: " + String.valueOf(this.scale)) + " Voltage: " + String.valueOf(this.voltage)) + " Temperatur: " + String.valueOf(this.temperature)) + " Technology: " + this.technology) + " Status: " + String.valueOf(this.status)) + " Plugged: " + String.valueOf(this.plugged)) + " Present: " + String.valueOf(this.present)) + " Health: " + String.valueOf(this.health);
    }

    public void fromBroadcast(Intent intent) {
        this.date = new Date();
        this.level = intent.getIntExtra(EXTRA_LEVEL, 0);
        this.scale = intent.getIntExtra(EXTRA_SCALE, 0);
        this.voltage = intent.getIntExtra(EXTRA_VOLTAGE, 0);
        this.temperature = intent.getIntExtra(EXTRA_TEMPERATURE, 1000);
        this.plugged = intent.getIntExtra(EXTRA_PLUGGED, 0);
        this.health = intent.getIntExtra(EXTRA_HEALTH, 0);
        this.status = intent.getIntExtra(EXTRA_STATUS, 0);
        this.present = intent.getBooleanExtra(EXTRA_PRESENT, true);
        this.technology = intent.getStringExtra(EXTRA_TECHNOLOGY);
    }
}
