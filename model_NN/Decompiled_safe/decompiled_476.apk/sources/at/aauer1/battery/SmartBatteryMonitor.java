package at.aauer1.battery;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.preference.PreferenceManager;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import at.aauer1.battery.database.BatteryDatabaseLog;
import at.aauer1.battery.service.BatteryDataConverter;
import at.aauer1.battery.service.BatteryDataItem;
import at.aauer1.battery.service.BatteryService;
import at.aauer1.battery.theme.ThemeInfo;
import at.aauer1.battery.theme.ThemeManager;
import com.mobfox.sdk.BannerListener;
import com.mobfox.sdk.MobFoxView;
import com.mobfox.sdk.RequestException;
import java.util.Locale;

public class SmartBatteryMonitor extends Activity {
    public static final String AUTHORITY = "at.aauer1.battery";
    public static final Uri DB_URI = Uri.parse("content://at.aauer1.battery");
    private static final int REFRESH_AD = 101;
    private static final int REFRESH_INTERVAL = 30000;
    private static final int REQUEST_PREFERENCES = 1;
    private static final String TAG = "SmartBatteryMonitor";
    private Thread ad_refresh = new Thread() {
        public void run() {
            Log.i(SmartBatteryMonitor.TAG, "Refresh Thread started");
            Looper.prepare();
            SmartBatteryMonitor.this.refresh_looper = Looper.myLooper();
            SmartBatteryMonitor.this.refresh_handler = new Handler(SmartBatteryMonitor.this.refresh_looper) {
                public void handleMessage(Message msg) {
                    switch (msg.what) {
                        case SmartBatteryMonitor.REFRESH_AD /*101*/:
                            Log.i(SmartBatteryMonitor.TAG, String.valueOf(System.currentTimeMillis() / 1000) + " -- Refresh Ad message received. Requesting ad from MobFox");
                            SmartBatteryMonitor.this.mobfox_view.loadNextAd();
                            return;
                        default:
                            return;
                    }
                }
            };
            Looper.loop();
            Log.i(SmartBatteryMonitor.TAG, "Refresh Thread stopped");
        }
    };
    private BannerListener banner = new BannerListener() {
        public void noAdFound() {
            if (SmartBatteryMonitor.this.mobfox_view != null) {
                SmartBatteryMonitor.this.mobfox_view.setVisibility(8);
            }
            if (SmartBatteryMonitor.this.refresh_handler != null) {
                SmartBatteryMonitor.this.refresh_handler.removeMessages(SmartBatteryMonitor.REFRESH_AD);
                SmartBatteryMonitor.this.refresh_handler.sendEmptyMessageDelayed(SmartBatteryMonitor.REFRESH_AD, 30000);
            }
        }

        public void bannerLoadSucceeded() {
            if (SmartBatteryMonitor.this.mobfox_view != null) {
                SmartBatteryMonitor.this.mobfox_view.setVisibility(0);
            }
            if (SmartBatteryMonitor.this.refresh_handler != null) {
                SmartBatteryMonitor.this.refresh_handler.removeMessages(SmartBatteryMonitor.REFRESH_AD);
                SmartBatteryMonitor.this.refresh_handler.sendEmptyMessageDelayed(SmartBatteryMonitor.REFRESH_AD, 30000);
            }
        }

        public void bannerLoadFailed(RequestException arg0) {
            if (SmartBatteryMonitor.this.mobfox_view != null) {
                SmartBatteryMonitor.this.mobfox_view.setVisibility(8);
            }
            if (SmartBatteryMonitor.this.refresh_handler != null) {
                SmartBatteryMonitor.this.refresh_handler.removeMessages(SmartBatteryMonitor.REFRESH_AD);
                SmartBatteryMonitor.this.refresh_handler.sendEmptyMessageDelayed(SmartBatteryMonitor.REFRESH_AD, 30000);
            }
        }
    };
    private TextView battery_headline = null;
    private ImageView battery_image = null;
    private TextView battery_info = null;
    private TextView battery_subtext = null;
    private TextView battery_text = null;
    /* access modifiers changed from: private */
    public BatteryService bound_service;
    private ServiceConnection connection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            SmartBatteryMonitor.this.bound_service = ((BatteryService.BatteryBinder) service).getService();
        }

        public void onServiceDisconnected(ComponentName className) {
            SmartBatteryMonitor.this.bound_service = null;
        }
    };
    private BatteryDataConverter converter = null;
    private Html.ImageGetter image_getter = new Html.ImageGetter() {
        public Drawable getDrawable(String source) {
            if (!source.equals("paypal_donate.png")) {
                return null;
            }
            Drawable drawable = SmartBatteryMonitor.this.getResources().getDrawable(R.drawable.paypal_donate);
            drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
            return drawable;
        }
    };
    private boolean is_bound = false;
    private LinearLayout layout = null;
    private BatteryDatabaseLog log = null;
    /* access modifiers changed from: private */
    public MobFoxView mobfox_view = null;
    public BroadcastReceiver receiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            BatteryDataItem item = new BatteryDataItem();
            item.fromBroadcast(intent);
            Log.d(SmartBatteryMonitor.TAG, item.toString());
            SmartBatteryMonitor.this.updateBatteryInfo(item);
        }
    };
    /* access modifiers changed from: private */
    public Handler refresh_handler;
    /* access modifiers changed from: private */
    public Looper refresh_looper;
    private ThemeManager theme_manager = null;

    private void doBindService() {
        bindService(new Intent(getApplicationContext(), BatteryService.class), this.connection, REQUEST_PREFERENCES);
        this.is_bound = true;
    }

    private void doUnbindService() {
        if (this.is_bound) {
            unbindService(this.connection);
            this.is_bound = false;
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        View layout2 = ((LayoutInflater) getSystemService("layout_inflater")).inflate((int) R.layout.info_dialog, (ViewGroup) findViewById(R.id.layout_root));
        ((TextView) layout2.findViewById(R.id.dialog_info_text)).setText(Html.fromHtml(getString(R.string.dialog_info_text)));
        TextView temp = (TextView) layout2.findViewById(R.id.dialog_info_donate);
        temp.setMovementMethod(LinkMovementMethod.getInstance());
        temp.setText(Html.fromHtml(getString(R.string.dialog_info_donate), this.image_getter, null));
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(layout2);
        return builder.create();
    }

    private void displayChangeLog() {
        View view = LayoutInflater.from(this).inflate((int) R.layout.changelog_view, (ViewGroup) null);
        ((WebView) view.findViewById(R.id.dialog_changelog)).loadData(getString(R.string.dialog_changelog), "text/html", "utf-8");
        new AlertDialog.Builder(this).setTitle("Changelog").setIcon(17301569).setView(view).setNegativeButton("Close", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        }).show();
    }

    private void checkVersion() {
        try {
            int version_code = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
            SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            if (settings.getInt(getString(R.string.key_changelog), 0) < version_code) {
                SharedPreferences.Editor editor = settings.edit();
                editor.putInt(getString(R.string.key_changelog), version_code);
                editor.commit();
                displayChangeLog();
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.w("Unable to get version code. Will not show changelog", e);
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        this.ad_refresh.start();
        this.theme_manager = ThemeManager.getInstance();
        this.converter = new BatteryDataConverter(getResources());
        this.battery_headline = (TextView) findViewById(R.id.battery_headline);
        this.battery_info = (TextView) findViewById(R.id.battery_info);
        this.battery_text = (TextView) findViewById(R.id.battery_text);
        this.battery_subtext = (TextView) findViewById(R.id.battery_subtext);
        this.battery_image = (ImageView) findViewById(R.id.battery_image);
        this.layout = (LinearLayout) findViewById(R.id.battery_large_linear);
        this.mobfox_view = (MobFoxView) findViewById(R.id.mobFoxView);
        this.mobfox_view.setBannerListener(this.banner);
        this.log = new BatteryDatabaseLog(this);
        startService(new Intent(getApplicationContext(), BatteryService.class));
        doBindService();
        registerReceiver(this.receiver, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        applyTheme(pref.getString(getString(R.string.key_theme), "Default"));
        applySettings(pref);
        checkVersion();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.mobfox_view.pause();
        if (this.refresh_handler != null) {
            this.refresh_handler.removeMessages(REFRESH_AD);
        }
        if (this.refresh_handler != null) {
            this.refresh_handler.removeMessages(REFRESH_AD);
            this.refresh_handler.sendEmptyMessage(REFRESH_AD);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.mobfox_view.resume();
    }

    public void onDestroy() {
        super.onDestroy();
        doUnbindService();
        unregisterReceiver(this.receiver);
        if (this.refresh_looper != null) {
            this.refresh_looper.quit();
            this.refresh_looper = null;
            this.refresh_handler = null;
        }
    }

    private void applyLanguage(SharedPreferences pref) {
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        String value = pref.getString(getString(R.string.key_language), "Default");
        if (value.equals("English")) {
            conf.locale = Locale.ENGLISH;
        } else if (value.equals("German")) {
            conf.locale = Locale.GERMANY;
        } else {
            conf.locale = Locale.getDefault();
        }
        res.updateConfiguration(conf, dm);
        findViewById(R.id.main_layout).invalidate();
    }

    private void applySettings(SharedPreferences pref) {
        boolean overlay = pref.getBoolean(getString(R.string.key_overlay), true);
        applyLanguage(pref);
        if (this.layout != null) {
            int visibility = overlay ? 0 : 4;
            this.layout.setVisibility(visibility);
            Log.d(TAG, "text overlay: " + overlay + " : " + visibility);
        }
        if (!(this.bound_service == null || this.bound_service.isNotificationEnabled() == pref.getBoolean(getString(R.string.key_service), true))) {
            this.bound_service.enableNotification(pref.getBoolean(getString(R.string.key_service), true));
        }
        if (this.bound_service != null && this.bound_service.isNotificationEnabled()) {
            this.bound_service.showNotification();
        }
        if (this.bound_service != null) {
            this.bound_service.enableVibrationFull(pref.getBoolean(getString(R.string.key_vibration_full), false));
            this.bound_service.enableVibrationEmpty(pref.getBoolean(getString(R.string.key_vibration_empty), false));
        }
    }

    private void applyTheme(String name) {
        this.theme_manager.queryThemes(getPackageManager());
        ThemeInfo info = this.theme_manager.getThemeInfo(name);
        if (info == null) {
            info = this.theme_manager.getDefaultThemeInfo();
        }
        if (managedQuery(Uri.parse("content://" + info.authority + "/items"), null, null, null, null) == null) {
            info = this.theme_manager.getDefaultThemeInfo();
        }
        Cursor cursor = managedQuery(Uri.parse("content://" + info.authority + "/items"), null, null, null, null);
        cursor.moveToFirst();
        int width = -1;
        int height = -1;
        int id = cursor.getInt(REQUEST_PREFERENCES);
        int column = cursor.getColumnIndex("width");
        if (column != -1) {
            width = cursor.getInt(column);
        }
        int column2 = cursor.getColumnIndex("height");
        if (column2 != -1) {
            height = cursor.getInt(column2);
        }
        try {
            Resources res = getPackageManager().getResourcesForApplication(info.package_name);
            int level = this.battery_image.getDrawable().getLevel();
            this.battery_image.setImageDrawable(res.getDrawable(id));
            this.battery_image.getDrawable().setLevel(level);
            Log.d(TAG, "Bounds: " + this.battery_image.getDrawable().getIntrinsicWidth() + " - " + this.battery_image.getDrawable().getIntrinsicHeight());
            if (width != -1 && height != -1) {
                DisplayMetrics metrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(metrics);
                ViewGroup.LayoutParams params = this.battery_image.getLayoutParams();
                params.height = (int) (((float) height) * metrics.density);
                params.width = (int) (((float) width) * metrics.density);
                this.battery_image.setLayoutParams(params);
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_settings:
                startActivityForResult(new Intent(getApplicationContext(), Preferences.class), REQUEST_PREFERENCES);
                return REQUEST_PREFERENCES;
            case R.id.menu_addons:
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=SBM pub:\"Andreas Auer\" ")));
                return REQUEST_PREFERENCES;
            case R.id.menu_partner_apps:
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=com.lookout&referrer=utm_source%3DPartner%26utm_medium%3Dlink%26utm_campaign%3DAndreas")));
                return REQUEST_PREFERENCES;
            case R.id.menu_info:
                showDialog(REQUEST_PREFERENCES);
                return REQUEST_PREFERENCES;
            case R.id.menu_changelog:
                displayChangeLog();
                return REQUEST_PREFERENCES;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onActivityResult(int request_code, int result_code, Intent intent) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        if (request_code == REQUEST_PREFERENCES) {
            if (this.bound_service == null || !this.is_bound) {
                doBindService();
            }
            String theme = pref.getString(getString(R.string.key_theme), "Default");
            if (this.bound_service != null && !this.bound_service.getCurrentTheme().label.equals(theme)) {
                this.bound_service.applyTheme(theme);
            }
            applyTheme(theme);
            applySettings(pref);
            BatteryDataItem item = this.log.getLastItem();
            if (item != null) {
                updateBatteryInfo(item);
            }
        }
    }

    /* access modifiers changed from: private */
    public void updateBatteryInfo(BatteryDataItem item) {
        StringBuffer buffer = new StringBuffer("");
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        boolean time_duration = pref.getBoolean(getString(R.string.key_timeduration), true);
        this.battery_headline.setText((int) R.string.battery_headline);
        buffer.append(String.valueOf(getString(R.string.battery_level)) + ": " + this.converter.getScaledLevel(item) + "%\n");
        String status = this.converter.getStatus(item);
        if (status != null) {
            buffer.append(String.valueOf(getString(R.string.battery_status)) + ": " + status + "\n");
        }
        String health = this.converter.getHealth(item);
        if (health != null) {
            buffer.append(String.valueOf(getString(R.string.battery_health)) + ": " + health + "\n");
        }
        if (!(item.technology == null || item.technology.length() == 0)) {
            buffer.append(String.valueOf(getString(R.string.battery_technology)) + ": " + item.technology + "\n");
        }
        if (item.temperature != 1000) {
            if (pref.getBoolean(getString(R.string.key_fahrenheit), false)) {
                buffer.append(String.valueOf(getString(R.string.battery_temperature)) + ": " + String.valueOf(this.converter.getTemperatureFahrenheit(item)) + "°F\n");
            } else {
                buffer.append(String.valueOf(getString(R.string.battery_temperature)) + ": " + String.valueOf(this.converter.getTemperatureCelsius(item)) + "°C\n");
            }
        }
        if (item.voltage != 0) {
            buffer.append(String.valueOf(getString(R.string.battery_voltage)) + ": " + String.valueOf(this.converter.getVoltage(item)) + "V\n");
        }
        if (item.plugged == REQUEST_PREFERENCES || item.plugged == 2) {
            long charging = this.log.getLastDischarge();
            if (charging != 0) {
                if (time_duration) {
                    buffer.append(String.valueOf(getString(R.string.battery_last_discharge)) + ": " + this.converter.formatTimeDifference(System.currentTimeMillis() - charging));
                } else {
                    buffer.append(String.valueOf(getString(R.string.battery_last_discharge)) + ": " + BatteryDataConverter.formatTime(charging));
                }
            }
        } else {
            long last_charged = this.log.getLastCharge();
            if (last_charged != 0) {
                if (time_duration) {
                    buffer.append(String.valueOf(getString(R.string.battery_last_charge)) + ": " + this.converter.formatTimeDifference(System.currentTimeMillis() - last_charged));
                } else {
                    buffer.append(String.valueOf(getString(R.string.battery_last_discharge)) + ": " + BatteryDataConverter.formatTime(last_charged));
                }
            }
        }
        int level = (item.level * 70) + 1600;
        this.battery_info.setText(buffer.toString());
        this.battery_text.setText(String.valueOf(String.valueOf(this.converter.getScaledLevel(item))) + "%");
        this.battery_text.setGravity(REQUEST_PREFERENCES);
        this.battery_subtext.setText(this.converter.getStatus(item));
        this.battery_subtext.setGravity(REQUEST_PREFERENCES);
        this.battery_image.getDrawable().setLevel(level);
    }
}
