package at.aauer1.battery;

import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;
import at.aauer1.battery.theme.ThemeInfo;
import at.aauer1.battery.theme.ThemeManager;
import java.util.Locale;
import java.util.Vector;

public class Preferences extends PreferenceActivity implements Preference.OnPreferenceClickListener {
    private static final String TAG = "Preferences";
    private Preference.OnPreferenceChangeListener language_change = new Preference.OnPreferenceChangeListener() {
        public boolean onPreferenceChange(Preference preference, Object newValue) {
            SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(Preferences.this).edit();
            Resources res = Preferences.this.getResources();
            DisplayMetrics dm = res.getDisplayMetrics();
            Configuration conf = res.getConfiguration();
            Log.d(Preferences.TAG, (String) newValue);
            String value = (String) newValue;
            if (value.equals("English")) {
                conf.locale = Locale.ENGLISH;
            } else if (value.equals("German")) {
                conf.locale = Locale.GERMANY;
            } else {
                value = "Default";
                conf.locale = Locale.getDefault();
            }
            res.updateConfiguration(conf, dm);
            editor.putString(Preferences.this.getString(R.string.key_language), value);
            editor.commit();
            return true;
        }
    };
    private Vector<IconCheckBoxPreference> theme_list;
    private ThemeManager theme_manager;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
        ((ListPreference) getPreferenceScreen().findPreference(getString(R.string.key_language))).setOnPreferenceChangeListener(this.language_change);
        PreferenceCategory theme_category = (PreferenceCategory) getPreferenceScreen().findPreference("preferences_themes");
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        String theme_name = pref.getString(getString(R.string.key_theme), "Default");
        this.theme_list = new Vector<>();
        this.theme_manager = ThemeManager.getInstance();
        this.theme_manager.queryThemes(getPackageManager());
        if (this.theme_manager.getThemeInfo(theme_name) == null) {
            theme_name = "Default";
            SharedPreferences.Editor editor = pref.edit();
            editor.putString(getString(R.string.key_theme), theme_name);
            editor.commit();
        }
        for (String name : this.theme_manager.getThemeNames()) {
            ThemeInfo info = this.theme_manager.getThemeInfo(name);
            IconCheckBoxPreference checkbox = new IconCheckBoxPreference(this);
            checkbox.setIcon(info.icon);
            checkbox.setTitle(info.label);
            if (info.label.equals(theme_name)) {
                checkbox.setChecked(true);
            }
            checkbox.setOnPreferenceClickListener(this);
            this.theme_list.add(checkbox);
            theme_category.addPreference(checkbox);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        setResult(-1);
    }

    public boolean onPreferenceClick(Preference preference) {
        IconCheckBoxPreference checkbox = (IconCheckBoxPreference) preference;
        for (int i = 0; i < this.theme_list.size(); i++) {
            if (this.theme_list.get(i) == checkbox) {
                this.theme_list.get(i).setChecked(true);
            } else {
                this.theme_list.get(i).setChecked(false);
            }
        }
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
        editor.putString(getString(R.string.key_theme), String.valueOf(checkbox.getTitle()));
        editor.commit();
        return false;
    }
}
