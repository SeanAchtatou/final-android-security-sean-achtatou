package at.aauer1.battery.database;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;
import at.aauer1.battery.R;
import at.aauer1.battery.SmartBatteryMonitor;
import at.aauer1.battery.service.BatteryDataItem;
import java.util.Date;

public class BatteryDatabaseLog {
    private static final int DB_MAX_DATA = 1000;
    private static final String TAG = "BatteryDatabaseLog";
    private ContentResolver content = null;
    private Context context = null;
    private int db_data_count;

    public BatteryDatabaseLog(Context context2) {
        this.context = context2;
        this.content = context2.getContentResolver();
        Cursor cursor = this.content.query(Uri.withAppendedPath(SmartBatteryMonitor.DB_URI, "data"), null, null, null, null);
        this.db_data_count = cursor.getCount();
        cursor.close();
    }

    public void addItem(BatteryDataItem item) {
        ContentValues values = new ContentValues();
        values.put("Date", Long.valueOf(item.date.getTime()));
        values.put(BatteryDataItem.EXTRA_LEVEL, Integer.valueOf(item.level));
        values.put(BatteryDataItem.EXTRA_SCALE, Integer.valueOf(item.scale));
        values.put(BatteryDataItem.EXTRA_VOLTAGE, Integer.valueOf(item.voltage));
        values.put(BatteryDataItem.EXTRA_TEMPERATURE, Integer.valueOf(item.temperature));
        values.put(BatteryDataItem.EXTRA_PLUGGED, Integer.valueOf(item.plugged));
        values.put(BatteryDataItem.EXTRA_HEALTH, Integer.valueOf(item.health));
        values.put(BatteryDataItem.EXTRA_STATUS, Integer.valueOf(item.status));
        values.put(BatteryDataItem.EXTRA_TECHNOLOGY, item.technology);
        values.put(BatteryDataItem.EXTRA_PRESENT, Integer.valueOf(item.present ? 1 : 0));
        this.content.insert(Uri.withAppendedPath(SmartBatteryMonitor.DB_URI, "data"), values);
        this.db_data_count++;
        if (this.db_data_count >= DB_MAX_DATA) {
            this.content.delete(Uri.withAppendedPath(SmartBatteryMonitor.DB_URI, "data"), "_id = ( select min(_id) from Data)", null);
        }
        if (item.plugged == 1 || item.plugged == 2) {
            ContentValues values2 = new ContentValues();
            values2.put("Key", this.context.getString(R.string.db_event_last_charged));
            values2.put("Value", String.valueOf(item.date.getTime()));
            this.content.update(Uri.withAppendedPath(SmartBatteryMonitor.DB_URI, "events"), values2, "Key = '" + this.context.getString(R.string.db_event_last_charged) + "'", null);
            return;
        }
        ContentValues values3 = new ContentValues();
        values3.put("Key", this.context.getString(R.string.db_event_last_discharged));
        values3.put("Value", String.valueOf(item.date.getTime()));
        this.content.update(Uri.withAppendedPath(SmartBatteryMonitor.DB_URI, "events"), values3, "Key = '" + this.context.getString(R.string.db_event_last_discharged) + "'", null);
    }

    public long getLastCharge() {
        long ret = 0;
        Cursor cursor = this.content.query(Uri.withAppendedPath(SmartBatteryMonitor.DB_URI, "events"), null, "Key = '" + this.context.getString(R.string.db_event_last_charged) + "'", null, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            ret = Long.valueOf(cursor.getString(cursor.getColumnIndex("Value"))).longValue();
        }
        cursor.close();
        return ret;
    }

    public long getLastDischarge() {
        long ret = 0;
        Cursor cursor = this.content.query(Uri.withAppendedPath(SmartBatteryMonitor.DB_URI, "events"), null, "Key = '" + this.context.getString(R.string.db_event_last_discharged) + "'", null, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            ret = Long.valueOf(cursor.getString(cursor.getColumnIndex("Value"))).longValue();
        }
        cursor.close();
        return ret;
    }

    public BatteryDataItem getLastItem() {
        BatteryDataItem item = null;
        Cursor cursor = this.content.query(Uri.withAppendedPath(SmartBatteryMonitor.DB_URI, "data"), null, "Date = (select max(Date) from Data)", null, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            item = new BatteryDataItem();
            item.date = new Date(cursor.getLong(cursor.getColumnIndex("Date")));
            item.level = cursor.getInt(cursor.getColumnIndex(BatteryDataItem.EXTRA_LEVEL));
            item.scale = cursor.getInt(cursor.getColumnIndex(BatteryDataItem.EXTRA_SCALE));
            item.voltage = cursor.getInt(cursor.getColumnIndex(BatteryDataItem.EXTRA_VOLTAGE));
            item.temperature = cursor.getInt(cursor.getColumnIndex(BatteryDataItem.EXTRA_TEMPERATURE));
            item.plugged = cursor.getInt(cursor.getColumnIndex(BatteryDataItem.EXTRA_PLUGGED));
            item.health = cursor.getInt(cursor.getColumnIndex(BatteryDataItem.EXTRA_HEALTH));
            item.status = cursor.getInt(cursor.getColumnIndex(BatteryDataItem.EXTRA_STATUS));
            item.technology = cursor.getString(cursor.getColumnIndex(BatteryDataItem.EXTRA_TECHNOLOGY));
            item.present = cursor.getInt(cursor.getColumnIndex(BatteryDataItem.EXTRA_PRESENT)) != 0;
            Log.d(TAG, item.toString());
        }
        cursor.close();
        return item;
    }
}
