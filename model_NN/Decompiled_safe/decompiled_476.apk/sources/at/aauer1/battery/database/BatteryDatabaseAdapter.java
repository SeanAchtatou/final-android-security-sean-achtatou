package at.aauer1.battery.database;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import at.aauer1.battery.SmartBatteryMonitor;
import at.aauer1.battery.service.BatteryDataItem;
import java.util.HashMap;

public class BatteryDatabaseAdapter extends ContentProvider {
    private static final int DATA = 3;
    private static final int DATA_ID = 4;
    private static final String DB_DATA_CREATE = "create table Data (_id integer primary key autoincrement,  Date integer not null, level integer not null, scale integer not null, voltage integer not null, temperature integer not null, plugged integer not null, health integer not null, status integer not null, technology text not null, present integer not null);";
    private static final String DB_DATA_TABLE = "Data";
    private static final String DB_EVENTS_CREATE = "create table Events (_id integer primary key autoincrement, Key text not null, Value text not null);";
    private static final String DB_EVENTS_TABLE = "Events";
    private static final String DB_NAME = "sbm.db";
    private static final int DB_VERSION = 3;
    private static final int EVENTS = 1;
    private static final int EVENT_ID = 2;
    private static final String TAG = "BatteryDatabaseAdapter";
    private static final HashMap<String, String> data_map = new HashMap<>();
    private static final HashMap<String, String> events_map = new HashMap<>();
    private static final UriMatcher uri_matcher = new UriMatcher(-1);
    private BatteryDatabaseHelper helper = null;

    private static class BatteryDatabaseHelper extends SQLiteOpenHelper {
        BatteryDatabaseHelper(Context context) {
            super(context, BatteryDatabaseAdapter.DB_NAME, (SQLiteDatabase.CursorFactory) null, 3);
        }

        public void onCreate(SQLiteDatabase db) {
            Log.d(BatteryDatabaseAdapter.TAG, "Create database");
            db.execSQL(BatteryDatabaseAdapter.DB_EVENTS_CREATE);
            db.execSQL(BatteryDatabaseAdapter.DB_DATA_CREATE);
            db.execSQL("INSERT INTO Events( Key, Value ) values ('last_charged', '0');");
            db.execSQL("INSERT INTO Events( Key, Value ) values ('last_discharged', '0');");
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.w(BatteryDatabaseAdapter.TAG, "Upgrading database from version " + oldVersion + " to " + newVersion);
            db.execSQL("DROP TABLE IF EXISTS Events");
            db.execSQL("DROP TABLE IF EXISTS Data");
            onCreate(db);
        }
    }

    public int delete(Uri uri, String where, String[] whereArgs) {
        int count;
        SQLiteDatabase db = this.helper.getWritableDatabase();
        switch (uri_matcher.match(uri)) {
            case EVENTS /*1*/:
                count = db.delete(DB_EVENTS_TABLE, where, whereArgs);
                break;
            case EVENT_ID /*2*/:
                count = db.delete(DB_EVENTS_TABLE, "_id=" + uri.getPathSegments().get(EVENTS) + (!TextUtils.isEmpty(where) ? " AND (" + where + ')' : ""), whereArgs);
                break;
            case 3:
                count = db.delete(DB_DATA_TABLE, where, whereArgs);
                break;
            case DATA_ID /*4*/:
                count = db.delete(DB_DATA_TABLE, "_id=" + uri.getPathSegments().get(EVENTS) + (!TextUtils.isEmpty(where) ? " AND (" + where + ')' : ""), whereArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    public String getType(Uri uri) {
        return null;
    }

    public Uri insert(Uri uri, ContentValues values) {
        SQLiteDatabase db = this.helper.getWritableDatabase();
        switch (uri_matcher.match(uri)) {
            case EVENTS /*1*/:
                if (!values.containsKey("Key") || !values.containsKey("Value")) {
                    throw new IllegalArgumentException("No key or value set");
                }
                long row = db.insert(DB_EVENTS_TABLE, null, values);
                if (row > 0) {
                    Uri note_uri = ContentUris.withAppendedId(Uri.parse("content://at.aauer1.battery/events"), row);
                    getContext().getContentResolver().notifyChange(note_uri, null);
                    return note_uri;
                }
                break;
            case EVENT_ID /*2*/:
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
            case 3:
                long row2 = db.insert(DB_DATA_TABLE, null, values);
                if (row2 > 0) {
                    Uri note_uri2 = ContentUris.withAppendedId(Uri.parse("content://at.aauer1.battery/data"), row2);
                    getContext().getContentResolver().notifyChange(note_uri2, null);
                    return note_uri2;
                }
                break;
        }
        return null;
    }

    public boolean onCreate() {
        this.helper = new BatteryDatabaseHelper(getContext());
        return true;
    }

    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        Log.d(TAG, "query: " + uri);
        switch (uri_matcher.match(uri)) {
            case EVENTS /*1*/:
                qb.setTables(DB_EVENTS_TABLE);
                qb.setProjectionMap(events_map);
                break;
            case EVENT_ID /*2*/:
                qb.setTables(DB_EVENTS_TABLE);
                qb.setProjectionMap(events_map);
                qb.appendWhere("_id=" + uri.getPathSegments().get(EVENTS));
                break;
            case 3:
                qb.setTables(DB_DATA_TABLE);
                qb.setProjectionMap(data_map);
                break;
            case DATA_ID /*4*/:
                qb.setTables(DB_DATA_TABLE);
                qb.setProjectionMap(data_map);
                qb.appendWhere("_id=" + uri.getPathSegments().get(EVENTS));
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        String orderBy = null;
        if (!TextUtils.isEmpty(sortOrder)) {
            orderBy = sortOrder;
        }
        Cursor c = qb.query(this.helper.getReadableDatabase(), projection, selection, selectionArgs, null, null, orderBy);
        c.setNotificationUri(getContext().getContentResolver(), uri);
        return c;
    }

    public int update(Uri uri, ContentValues values, String where, String[] whereArgs) {
        int count;
        SQLiteDatabase db = this.helper.getWritableDatabase();
        switch (uri_matcher.match(uri)) {
            case EVENTS /*1*/:
                count = db.update(DB_EVENTS_TABLE, values, where, whereArgs);
                break;
            case EVENT_ID /*2*/:
                count = db.update(DB_EVENTS_TABLE, values, "_id=" + uri.getPathSegments().get(EVENTS) + (!TextUtils.isEmpty(where) ? " AND (" + where + ')' : ""), whereArgs);
                break;
            case 3:
                count = db.update(DB_DATA_TABLE, values, where, whereArgs);
                break;
            case DATA_ID /*4*/:
                count = db.update(DB_DATA_TABLE, values, "_id=" + uri.getPathSegments().get(EVENTS) + (!TextUtils.isEmpty(where) ? " AND (" + where + ')' : ""), whereArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    static {
        uri_matcher.addURI(SmartBatteryMonitor.AUTHORITY, "events", EVENTS);
        uri_matcher.addURI(SmartBatteryMonitor.AUTHORITY, "events/#", EVENT_ID);
        uri_matcher.addURI(SmartBatteryMonitor.AUTHORITY, "data", 3);
        uri_matcher.addURI(SmartBatteryMonitor.AUTHORITY, "data/#", DATA_ID);
        events_map.put("_id", "_id");
        events_map.put("Key", "Key");
        events_map.put("Value", "Value");
        data_map.put("_id", "_id");
        data_map.put("Date", "Date");
        data_map.put(BatteryDataItem.EXTRA_LEVEL, BatteryDataItem.EXTRA_LEVEL);
        data_map.put(BatteryDataItem.EXTRA_SCALE, BatteryDataItem.EXTRA_SCALE);
        data_map.put(BatteryDataItem.EXTRA_VOLTAGE, BatteryDataItem.EXTRA_VOLTAGE);
        data_map.put(BatteryDataItem.EXTRA_TEMPERATURE, BatteryDataItem.EXTRA_TEMPERATURE);
        data_map.put(BatteryDataItem.EXTRA_PLUGGED, BatteryDataItem.EXTRA_PLUGGED);
        data_map.put(BatteryDataItem.EXTRA_HEALTH, BatteryDataItem.EXTRA_HEALTH);
        data_map.put(BatteryDataItem.EXTRA_STATUS, BatteryDataItem.EXTRA_STATUS);
        data_map.put(BatteryDataItem.EXTRA_TECHNOLOGY, BatteryDataItem.EXTRA_TECHNOLOGY);
        data_map.put(BatteryDataItem.EXTRA_PRESENT, BatteryDataItem.EXTRA_PRESENT);
    }
}
