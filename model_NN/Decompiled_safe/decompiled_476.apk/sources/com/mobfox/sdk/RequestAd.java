package com.mobfox.sdk;

import android.util.Log;
import at.aauer1.battery.service.BatteryDataItem;
import com.mobfox.sdk.data.ClickType;
import com.mobfox.sdk.data.Request;
import com.mobfox.sdk.data.Response;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class RequestAd {
    public Response sendRequest(Request request) throws RequestException {
        return sendGetRequest(request);
    }

    private Response sendGetRequest(Request request) throws RequestException {
        if (Log.isLoggable(Const.TAG, 3)) {
            Log.d(Const.TAG, "send Request");
        }
        StringBuilder url = new StringBuilder(Const.REQUEST_URL);
        url.append("?rt=android_app");
        try {
            url.append("&o=");
            url.append(URLEncoder.encode(request.getDeviceId(), Const.ENCODING));
            url.append("&m=");
            url.append(URLEncoder.encode(request.getMode().toString().toLowerCase(), Const.ENCODING));
            url.append("&s=");
            url.append(URLEncoder.encode(request.getPublisherId(), Const.ENCODING));
            url.append("&u=");
            url.append(URLEncoder.encode(request.getUserAgent(), Const.ENCODING));
            url.append("&v=");
            url.append(URLEncoder.encode(request.getProtocolVersion(), Const.ENCODING));
            if (!(request.getLatitude() == 0.0d || request.getLongitude() == 0.0d)) {
                url.append("&latitude=");
                url.append(request.getLatitude());
                url.append("&longitude=");
                url.append(request.getLongitude());
            }
            if (Log.isLoggable(Const.TAG, 3)) {
                Log.d(Const.TAG, "Perform HTTP Get Url: " + ((Object) url));
            }
            DefaultHttpClient client = new DefaultHttpClient();
            HttpConnectionParams.setSoTimeout(client.getParams(), 15000);
            HttpConnectionParams.setConnectionTimeout(client.getParams(), 15000);
            try {
                return parse(client.execute(new HttpGet(url.toString())).getEntity().getContent());
            } catch (ClientProtocolException e) {
                throw new RequestException("Error in HTTP request", e);
            } catch (IOException e2) {
                throw new RequestException("Error in HTTP request", e2);
            } catch (Throwable th) {
                throw new RequestException("Error in HTTP request", th);
            }
        } catch (UnsupportedEncodingException e3) {
            throw new RequestException("Cannot create request URL", e3);
        }
    }

    private Response parse(InputStream inputStream) throws RequestException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        Response response = new Response();
        try {
            Document doc = dbf.newDocumentBuilder().parse(new InputSource(inputStream));
            Element element = doc.getDocumentElement();
            if (element == null) {
                throw new RequestException("Cannot parse Response, document is not an xml");
            }
            String errorValue = getValue(doc, "error");
            if (errorValue != null) {
                throw new RequestException("Error Response received: " + errorValue);
            }
            String type = element.getAttribute("type");
            element.normalize();
            if ("imageAd".equalsIgnoreCase(type)) {
                response.setType(AdType.IMAGE);
                response.setBannerWidth(getValueAsInt(doc, "bannerwidth"));
                response.setBannerHeight(getValueAsInt(doc, "bannerheight"));
                response.setClickType(ClickType.getValue(getValue(doc, "clicktype")));
                response.setClickUrl(getValue(doc, "clickurl"));
                response.setImageUrl(getValue(doc, "imageurl"));
                response.setRefresh(getValueAsInt(doc, "refresh"));
                response.setScale(getValueAsBoolean(doc, BatteryDataItem.EXTRA_SCALE));
                response.setSkipPreflight(getValueAsBoolean(doc, "skippreflight"));
            } else if ("textAd".equalsIgnoreCase(type)) {
                response.setType(AdType.TEXT);
                response.setText(getValue(doc, "htmlString"));
                response.setClickType(ClickType.getValue(getValue(doc, "clicktype")));
                response.setClickUrl(getValue(doc, "clickurl"));
                response.setRefresh(getValueAsInt(doc, "refresh"));
                response.setScale(getValueAsBoolean(doc, BatteryDataItem.EXTRA_SCALE));
                response.setSkipPreflight(getValueAsBoolean(doc, "skippreflight"));
            } else if ("noAd".equalsIgnoreCase(type)) {
                response.setType(AdType.NO_AD);
            } else {
                throw new RequestException("Unknown response type " + type);
            }
            return response;
        } catch (ParserConfigurationException e) {
            throw new RequestException("Cannot parse Response", e);
        } catch (SAXException e2) {
            throw new RequestException("Cannot parse Response", e2);
        } catch (IOException e3) {
            throw new RequestException("Cannot read Response", e3);
        } catch (Throwable th) {
            throw new RequestException("Cannot read Response", th);
        }
    }

    private boolean getValueAsBoolean(Document document, String name) {
        return "yes".equalsIgnoreCase(getValue(document, name));
    }

    private String getValue(Document document, String name) {
        Element element = (Element) document.getElementsByTagName(name).item(0);
        if (element != null) {
            NodeList nodeList = element.getChildNodes();
            if (nodeList.getLength() > 0) {
                return nodeList.item(0).getNodeValue();
            }
        }
        return null;
    }

    private int getValueAsInt(Document document, String name) {
        return getInteger(getValue(document, name));
    }

    private int getInteger(String text) {
        if (text == null) {
            return 0;
        }
        try {
            return Integer.parseInt(text);
        } catch (NumberFormatException e) {
            return 0;
        }
    }
}
