package com.mobfox.sdk;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.RelativeLayout;
import android.widget.ViewFlipper;
import com.mobfox.sdk.data.ClickType;
import com.mobfox.sdk.data.Request;
import com.mobfox.sdk.data.Response;
import java.io.IOException;
import java.lang.Thread;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.text.MessageFormat;
import java.util.Timer;
import java.util.UUID;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

public class MobFoxView extends RelativeLayout {
    private boolean animation;
    /* access modifiers changed from: private */
    public BannerListener bannerListener;
    int count;
    private Animation fadeInAnimation;
    private Animation fadeOutAnimation;
    private WebView firstWebView;
    private boolean includeLocation;
    private int isAccessCoarseLocation;
    private int isAccessFineLocation;
    boolean isInternalBrowser;
    /* access modifiers changed from: private */
    public Thread loadContentThread;
    private LocationManager locationManager;
    private Mode mode;
    private View.OnTouchListener onTouchListener;
    private String publisherId;
    private Timer reloadTimer;
    private Request request;
    /* access modifiers changed from: private */
    public Response response;
    private WebView secondWebView;
    final Runnable showContent;
    private int telephonyPermission;
    /* access modifiers changed from: private */
    public boolean touchMove;
    final Handler updateHandler;
    private ViewFlipper viewFlipper;
    private WebSettings webSettings;

    public MobFoxView(Context context, String publisherId2, boolean includeLocation2, boolean animation2) {
        this(context, publisherId2, Mode.LIVE, includeLocation2, animation2);
    }

    public MobFoxView(Context context, String publisherId2, Mode mode2, boolean includeLocation2, boolean animation2) {
        super(context);
        this.includeLocation = false;
        this.isInternalBrowser = false;
        this.fadeInAnimation = null;
        this.fadeOutAnimation = null;
        this.updateHandler = new Handler();
        this.showContent = new Runnable() {
            public void run() {
                MobFoxView.this.showContent();
            }
        };
        this.onTouchListener = new View.OnTouchListener() {
            private float distanceX;
            private float distanceY;

            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == 0) {
                    boolean unused = MobFoxView.this.touchMove = false;
                    this.distanceX = event.getX();
                    this.distanceY = event.getY();
                }
                if (event.getAction() == 2) {
                    if (Math.abs(this.distanceX - event.getX()) > 30.0f) {
                        boolean unused2 = MobFoxView.this.touchMove = true;
                    }
                    if (Math.abs(this.distanceY - event.getY()) > 30.0f) {
                        boolean unused3 = MobFoxView.this.touchMove = true;
                    }
                    if (Log.isLoggable(Const.TAG, 3)) {
                        Log.d(Const.TAG, "touchMove: " + MobFoxView.this.touchMove);
                    }
                    return true;
                } else if (event.getAction() != 1) {
                    return false;
                } else {
                    if (Log.isLoggable(Const.TAG, 3)) {
                        Log.d(Const.TAG, "size x: " + event.getX());
                        Log.d(Const.TAG, "getHistorySize: " + event.getHistorySize());
                    }
                    if (MobFoxView.this.response != null && !MobFoxView.this.touchMove) {
                        MobFoxView.this.openLink();
                    }
                    return false;
                }
            }
        };
        this.count = 0;
        this.publisherId = publisherId2;
        this.includeLocation = includeLocation2;
        this.mode = mode2;
        this.animation = animation2;
        initialize(context);
    }

    public MobFoxView(Context context, AttributeSet attributes) {
        super(context, attributes);
        this.includeLocation = false;
        this.isInternalBrowser = false;
        this.fadeInAnimation = null;
        this.fadeOutAnimation = null;
        this.updateHandler = new Handler();
        this.showContent = new Runnable() {
            public void run() {
                MobFoxView.this.showContent();
            }
        };
        this.onTouchListener = new View.OnTouchListener() {
            private float distanceX;
            private float distanceY;

            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == 0) {
                    boolean unused = MobFoxView.this.touchMove = false;
                    this.distanceX = event.getX();
                    this.distanceY = event.getY();
                }
                if (event.getAction() == 2) {
                    if (Math.abs(this.distanceX - event.getX()) > 30.0f) {
                        boolean unused2 = MobFoxView.this.touchMove = true;
                    }
                    if (Math.abs(this.distanceY - event.getY()) > 30.0f) {
                        boolean unused3 = MobFoxView.this.touchMove = true;
                    }
                    if (Log.isLoggable(Const.TAG, 3)) {
                        Log.d(Const.TAG, "touchMove: " + MobFoxView.this.touchMove);
                    }
                    return true;
                } else if (event.getAction() != 1) {
                    return false;
                } else {
                    if (Log.isLoggable(Const.TAG, 3)) {
                        Log.d(Const.TAG, "size x: " + event.getX());
                        Log.d(Const.TAG, "getHistorySize: " + event.getHistorySize());
                    }
                    if (MobFoxView.this.response != null && !MobFoxView.this.touchMove) {
                        MobFoxView.this.openLink();
                    }
                    return false;
                }
            }
        };
        this.count = 0;
        if (attributes != null) {
            this.publisherId = attributes.getAttributeValue(null, "publisherId");
            String modeValue = attributes.getAttributeValue(null, "mode");
            if (modeValue != null && modeValue.equalsIgnoreCase("test")) {
                this.mode = Mode.TEST;
            }
            String animationValue = attributes.getAttributeValue(null, "animation");
            if (animationValue == null || !animationValue.equals("false")) {
                this.animation = true;
            } else {
                this.animation = false;
            }
            this.includeLocation = attributes.getAttributeBooleanValue(null, "includeLocation", false);
        }
        initialize(context);
    }

    private void initialize(Context context) {
        if (Log.isLoggable(Const.TAG, 3)) {
            Log.d(Const.TAG, "MobFox SDK Version:1.2");
        }
        this.locationManager = null;
        this.telephonyPermission = context.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE");
        this.isAccessFineLocation = context.checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION");
        this.isAccessCoarseLocation = context.checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION");
        if (this.isAccessFineLocation == 0 || this.isAccessCoarseLocation == 0) {
            this.locationManager = (LocationManager) getContext().getSystemService("location");
        }
        this.firstWebView = createWebView(context);
        this.secondWebView = createWebView(context);
        if (Log.isLoggable(Const.TAG, 3)) {
            Log.d(Const.TAG, "Create view flipper");
        }
        this.viewFlipper = new ViewFlipper(getContext()) {
            /* access modifiers changed from: protected */
            public void onDetachedFromWindow() {
                try {
                    super.onDetachedFromWindow();
                } catch (IllegalArgumentException e) {
                    stopFlipping();
                }
            }
        };
        this.viewFlipper.addView(this.firstWebView);
        this.viewFlipper.addView(this.secondWebView);
        addView(this.viewFlipper);
        this.firstWebView.setOnTouchListener(this.onTouchListener);
        this.secondWebView.setOnTouchListener(this.onTouchListener);
        if (Log.isLoggable(Const.TAG, 3)) {
            Log.d(Const.TAG, "animation: " + this.animation);
        }
        if (this.animation) {
            this.fadeInAnimation = new TranslateAnimation(2, 0.0f, 2, 0.0f, 2, 1.0f, 2, 0.0f);
            this.fadeInAnimation.setDuration(1000);
            this.fadeOutAnimation = new TranslateAnimation(2, 0.0f, 2, 0.0f, 2, 0.0f, 2, -1.0f);
            this.fadeOutAnimation.setDuration(1000);
            this.viewFlipper.setInAnimation(this.fadeInAnimation);
            this.viewFlipper.setOutAnimation(this.fadeOutAnimation);
        }
    }

    private WebView createWebView(Context context) {
        WebView webView = new WebView(getContext());
        this.webSettings = webView.getSettings();
        this.webSettings.setJavaScriptEnabled(true);
        webView.setBackgroundColor(0);
        webView.setWebViewClient(new MobFoxWebViewClient(this, context));
        webView.setVerticalScrollBarEnabled(false);
        webView.setHorizontalScrollBarEnabled(false);
        return webView;
    }

    private String getDeviceId() {
        String androidId = Settings.Secure.getString(getContext().getContentResolver(), "android_id");
        if (androidId == null || androidId.equals("9774d56d682e549c")) {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
            androidId = prefs.getString(Const.PREFS_DEVICE_ID, null);
            if (androidId == null) {
                try {
                    String uuid = UUID.randomUUID().toString();
                    MessageDigest digest = MessageDigest.getInstance("MD5");
                    digest.update(uuid.getBytes(), 0, uuid.length());
                    androidId = String.format("%016X", new BigInteger(1, digest.digest()));
                } catch (Exception e) {
                    Log.d(Const.TAG, "Could not generate pseudo unique id", e);
                    androidId = "9774d56d682e549c";
                }
                prefs.edit().putString(Const.PREFS_DEVICE_ID, androidId).commit();
            }
            if (Log.isLoggable(Const.TAG, 3)) {
                Log.d(Const.TAG, "Unknown Android ID using pseudo unique id:" + androidId);
            }
        }
        return androidId;
    }

    /* access modifiers changed from: private */
    public Request getRequest() {
        if (this.request == null) {
            this.request = new Request();
            if (this.telephonyPermission == 0) {
                this.request.setDeviceId(((TelephonyManager) getContext().getSystemService("phone")).getDeviceId());
                this.request.setProtocolVersion(Const.PROTOCOL_VERSION);
            } else {
                this.request.setDeviceId(getDeviceId());
                this.request.setProtocolVersion("N3");
            }
            this.request.setMode(this.mode);
            this.request.setPublisherId(this.publisherId);
            this.request.setUserAgent(this.webSettings.getUserAgentString());
        }
        Location location = null;
        if (this.includeLocation) {
            location = getLocation();
        }
        if (location != null) {
            if (Log.isLoggable(Const.TAG, 3)) {
                Log.d(Const.TAG, "location is longitude: " + location.getLongitude() + ", latitude: " + location.getLatitude());
            }
            this.request.setLatitude(location.getLatitude());
            this.request.setLongitude(location.getLongitude());
        } else {
            this.request.setLatitude(0.0d);
            this.request.setLongitude(0.0d);
        }
        return this.request;
    }

    public boolean isInternalBrowser() {
        return this.isInternalBrowser;
    }

    public void setInternalBrowser(boolean isInternalBrowser2) {
        this.isInternalBrowser = isInternalBrowser2;
    }

    private void loadContent() {
        if (Log.isLoggable(Const.TAG, 3)) {
            Log.d(Const.TAG, "load content");
        }
        if (this.loadContentThread == null) {
            this.loadContentThread = new Thread(new Runnable() {
                public void run() {
                    RequestException ex;
                    if (Log.isLoggable(Const.TAG, 3)) {
                        Log.d(Const.TAG, "starting request thread");
                    }
                    try {
                        Response unused = MobFoxView.this.response = new RequestAd().sendRequest(MobFoxView.this.getRequest());
                        if (MobFoxView.this.response != null) {
                            if (Log.isLoggable(Const.TAG, 3)) {
                                Log.d(Const.TAG, "response received");
                            }
                            if (Log.isLoggable(Const.TAG, 3)) {
                                Log.d(Const.TAG, "getVisibility: " + MobFoxView.this.getVisibility());
                            }
                            MobFoxView.this.updateHandler.post(MobFoxView.this.showContent);
                        }
                    } catch (Throwable th) {
                        Throwable e = th;
                        if (Log.isLoggable(Const.TAG, 6)) {
                            Log.e(Const.TAG, "Uncaught exception in request thread", e);
                        }
                        if (MobFoxView.this.bannerListener != null) {
                            Log.d(Const.TAG, "notify bannerListener: " + MobFoxView.this.bannerListener.getClass().getName());
                            if (e instanceof RequestException) {
                                ex = (RequestException) e;
                            } else {
                                ex = new RequestException(e);
                            }
                            MobFoxView.this.bannerListener.bannerLoadFailed(ex);
                        }
                        Log.e(Const.TAG, e.getMessage(), e);
                    }
                    Thread unused2 = MobFoxView.this.loadContentThread = null;
                    if (Log.isLoggable(Const.TAG, 3)) {
                        Log.d(Const.TAG, "finishing request thread");
                    }
                }
            });
            this.loadContentThread.setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
                public void uncaughtException(Thread thread, Throwable ex) {
                    if (Log.isLoggable(Const.TAG, 6)) {
                        Log.e(Const.TAG, "Uncaught exception in request thread", ex);
                    }
                    Thread unused = MobFoxView.this.loadContentThread = null;
                }
            });
            this.loadContentThread.start();
        }
    }

    /* access modifiers changed from: private */
    public void showContent() {
        WebView webView;
        try {
            if (this.viewFlipper.getCurrentView() == this.firstWebView) {
                webView = this.secondWebView;
            } else {
                webView = this.firstWebView;
            }
            if (this.response.getType() == AdType.IMAGE) {
                String text = MessageFormat.format(Const.IMAGE, this.response.getImageUrl(), Integer.valueOf(this.response.getBannerWidth()), Integer.valueOf(this.response.getBannerHeight()));
                if (Log.isLoggable(Const.TAG, 3)) {
                    Log.d(Const.TAG, "set image: " + text);
                }
                webView.loadData(Uri.encode(Const.HIDE_BORDER + text), "text/html", Const.ENCODING);
                if (this.bannerListener != null) {
                    if (Log.isLoggable(Const.TAG, 3)) {
                        Log.d(Const.TAG, "notify bannerListener of load succeeded: " + this.bannerListener.getClass().getName());
                    }
                    this.bannerListener.bannerLoadSucceeded();
                }
            } else if (this.response.getType() == AdType.TEXT) {
                String text2 = Uri.encode(Const.HIDE_BORDER + this.response.getText());
                if (Log.isLoggable(Const.TAG, 3)) {
                    Log.d(Const.TAG, "set text: " + text2);
                }
                webView.loadData(text2, "text/html", Const.ENCODING);
                if (this.bannerListener != null) {
                    if (Log.isLoggable(Const.TAG, 3)) {
                        Log.d(Const.TAG, "notify bannerListener of load succeeded: " + this.bannerListener.getClass().getName());
                    }
                    this.bannerListener.bannerLoadSucceeded();
                }
            } else {
                if (Log.isLoggable(Const.TAG, 3)) {
                    Log.d(Const.TAG, "No Ad");
                }
                if (this.bannerListener != null) {
                    this.bannerListener.noAdFound();
                    return;
                }
                return;
            }
            if (this.viewFlipper.getCurrentView() == this.firstWebView) {
                if (Log.isLoggable(Const.TAG, 3)) {
                    Log.d(Const.TAG, "show next");
                }
                this.viewFlipper.showNext();
            } else {
                if (Log.isLoggable(Const.TAG, 3)) {
                    Log.d(Const.TAG, "show previous");
                }
                this.viewFlipper.showPrevious();
            }
            startReloadTimer();
        } catch (Throwable th) {
            Throwable t = th;
            if (Log.isLoggable(Const.TAG, 6)) {
                Log.e(Const.TAG, "Uncaught exception in show content", t);
            }
        }
    }

    private void startReloadTimer() {
        if (Log.isLoggable(Const.TAG, 3)) {
            Log.d(Const.TAG, "start reload timer");
        }
        if (this.reloadTimer != null) {
            int refreshTime = this.response.getRefresh() * 1000;
            if (Log.isLoggable(Const.TAG, 3)) {
                Log.d(Const.TAG, "set timer: " + refreshTime);
            }
            this.reloadTimer.schedule(new ReloadTask(this), (long) refreshTime);
        }
    }

    private Location getLocation() {
        if (this.locationManager != null) {
            if (this.isAccessFineLocation == 0 && this.locationManager.isProviderEnabled("gps")) {
                return this.locationManager.getLastKnownLocation("gps");
            }
            if (this.isAccessCoarseLocation == 0 && this.locationManager.isProviderEnabled("network")) {
                return this.locationManager.getLastKnownLocation("network");
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int visibility) {
        super.onWindowVisibilityChanged(visibility);
        if (visibility == 0) {
            resume();
        } else {
            pause();
        }
        if (Log.isLoggable(Const.TAG, 3)) {
            Log.d(Const.TAG, "onWindowVisibilityChanged: " + visibility);
        }
    }

    public void openLink() {
        if (this.response != null && this.response.getClickUrl() != null) {
            if (this.response.isSkipPreflight()) {
                doOpenUrl(this.response.getClickUrl());
                return;
            }
            if (Log.isLoggable(Const.TAG, 3)) {
                Log.d(Const.TAG, "prefetch url: " + this.response.getClickUrl());
            }
            DefaultHttpClient client = new DefaultHttpClient();
            HttpConnectionParams.setSoTimeout(client.getParams(), 15000);
            HttpConnectionParams.setConnectionTimeout(client.getParams(), 15000);
            HttpGet get = new HttpGet(this.response.getClickUrl());
            HttpContext httpContext = new BasicHttpContext();
            try {
                HttpResponse response2 = client.execute(get, httpContext);
                if (response2.getStatusLine().getStatusCode() != 200) {
                    throw new IOException(response2.getStatusLine().toString());
                }
                doOpenUrl(((HttpHost) httpContext.getAttribute("http.target_host")).toURI() + ((HttpUriRequest) httpContext.getAttribute("http.request")).getURI());
            } catch (ClientProtocolException e) {
                Log.e(Const.TAG, "Error in HTTP request", e);
            } catch (IOException e2) {
                Log.e(Const.TAG, "Error in HTTP request", e2);
            } catch (Throwable th) {
                Log.e(Const.TAG, "Error in HTTP request", th);
            }
        }
    }

    private void doOpenUrl(String url) {
        if (this.response.getClickType() == null || !this.response.getClickType().equals(ClickType.INAPP)) {
            getContext().startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url)));
            return;
        }
        Intent intent = new Intent(getContext(), InAppWebView.class);
        intent.putExtra(Const.REDIRECT_URI, this.response.getClickUrl());
        getContext().startActivity(intent);
    }

    public void loadNextAd() {
        if (Log.isLoggable(Const.TAG, 3)) {
            Log.d(Const.TAG, "load next ad");
        }
        loadContent();
    }

    public void setBannerListener(BannerListener bannerListener2) {
        this.bannerListener = bannerListener2;
    }

    public void pause() {
        if (this.reloadTimer != null) {
            try {
                if (Log.isLoggable(Const.TAG, 3)) {
                    Log.d(Const.TAG, "cancel reload timer");
                }
                this.reloadTimer.cancel();
                this.reloadTimer = null;
            } catch (Exception e) {
                Log.e(Const.TAG, "unable to cancel reloadTimer", e);
            }
        }
    }

    public void resume() {
        if (this.reloadTimer != null) {
            this.reloadTimer.cancel();
            this.reloadTimer = null;
        }
        this.reloadTimer = new Timer();
        if (Log.isLoggable(Const.TAG, 3)) {
            Log.d(Const.TAG, "response: " + this.response);
        }
        if (this.response == null || this.response.getRefresh() <= 0) {
            loadContent();
        } else {
            startReloadTimer();
        }
    }
}
