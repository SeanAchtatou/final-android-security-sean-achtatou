package com.riteshsahu.SMSBackupRestoreBase;

import java.util.Comparator;

public class MessageComparer implements Comparator<Message> {
    public int compare(Message first, Message second) {
        return first.getDate().compareTo(second.getDate());
    }
}
