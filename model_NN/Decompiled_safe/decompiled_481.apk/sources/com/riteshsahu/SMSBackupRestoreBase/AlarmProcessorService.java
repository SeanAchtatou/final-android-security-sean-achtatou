package com.riteshsahu.SMSBackupRestoreBase;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.IBinder;
import com.riteshsahu.SMSBackupRestore.R;
import java.util.GregorianCalendar;

public class AlarmProcessorService extends Service {
    private final IBinder mBinder = new LocalBinder();

    public class LocalBinder extends Binder {
        public LocalBinder() {
        }

        /* access modifiers changed from: package-private */
        public AlarmProcessorService getService() {
            return AlarmProcessorService.this;
        }
    }

    public IBinder onBind(Intent intent) {
        return this.mBinder;
    }

    public void onStart(Intent intent, int startId) {
        Common.logDebug("onStart Called");
        super.onStart(intent, startId);
        new BackupTask(this, null).execute(new Object[0]);
    }

    private class BackupTask extends AsyncTask<Object, Object, Object> {
        private BackupTask() {
        }

        /* synthetic */ BackupTask(AlarmProcessorService alarmProcessorService, BackupTask backupTask) {
            this();
        }

        /* access modifiers changed from: protected */
        public Object doInBackground(Object... params) {
            AlarmProcessorService.this.processBackup();
            return null;
        }
    }

    /* access modifiers changed from: private */
    public void processBackup() {
        try {
            Common.logDebug("before createBackup");
            Common.setBooleanPreference(this, PreferenceKeys.ScheduledBackupStarted, true);
            OperationResult result = SmsBackupProcessor.createBackup(this, new BackupFile(Common.getBackupFilePath(this), Common.getNewBackupFileName(this)), false, null, null);
            Common.setBooleanPreference(this, PreferenceKeys.ScheduledBackupStarted, false);
            Common.logDebug("after saveXml");
            int fileDeleteCount = 0;
            String backupDays = Common.getStringPreference(this, PreferenceKeys.ScheduleBackupsToKeep);
            if (backupDays.length() == 0) {
                backupDays = Common.MESSAGE_TYPE_ALL;
            }
            Integer backupDaysCount = Integer.valueOf(Integer.parseInt(backupDays));
            if (backupDaysCount.intValue() > 0) {
                GregorianCalendar date = new GregorianCalendar();
                date.add(5, -backupDaysCount.intValue());
                fileDeleteCount = Common.deleteOldFiles(date.getTimeInMillis(), this).intValue();
            }
            String message = result.getMessage();
            if (message == null || message.length() == 0) {
                message = String.format(getString(R.string.backup_complete_notification_message), Integer.valueOf(result.getSuccessful()), Integer.valueOf(result.getFailed()), Integer.valueOf(result.getTotal()), Integer.valueOf(fileDeleteCount));
            }
            showNotification(R.drawable.notification, String.valueOf(message) + performExtraProcessingAfterBackup(), false);
            Common.logDebug("end of processBackup");
        } catch (Exception e) {
            Exception e2 = e;
            Common.logDebug(e2.getMessage());
            showNotification(17301624, String.format(getString(R.string.backup_failed_error), e2.getMessage()), true);
        } finally {
            stopSelf();
            WakeLocker.releaseLock();
        }
    }

    /* access modifiers changed from: protected */
    public String performExtraProcessingAfterBackup() {
        return "";
    }

    private void showNotification(int statusBarIconId, CharSequence titleMessage, boolean errorOccured) {
        if (!Common.getBooleanPreference(this, PreferenceKeys.DisableNotifications).booleanValue() || errorOccured) {
            Notification notification = new Notification(statusBarIconId, titleMessage, System.currentTimeMillis());
            notification.setLatestEventInfo(this, getText(R.string.app_name), titleMessage, PendingIntent.getActivity(this, 0, new Intent(this, Main.class), 0));
            notification.flags |= 16;
            ((NotificationManager) getSystemService("notification")).notify(R.string.app_name, notification);
        }
    }
}
