package com.riteshsahu.SMSBackupRestoreBase;

import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.os.Environment;
import android.os.Handler;
import com.riteshsahu.SMSBackupRestore.R;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;

public class SmsBackupProcessor {
    private static final String ContactNameAttributeName = "contact_name";
    private static final String DefaultProtocolValue = "0";
    private static final String ReadableDateAttributeName = "readable_date";
    private static final String SerializerFeature = "http://xmlpull.org/v1/doc/features.html#indent-output";
    private static final String TemporaryFileName = "tempbackup.xml";
    private static final String XslTag = "xml-stylesheet type=\"text/xsl\" href=\"sms.xsl\"";
    private static HashMap<String, String> mContacts;

    public static OperationResult createBackup(Context context, BackupFile backupFile, Boolean useWakeLock, ProgressDialog progressDialog, Handler handler) throws CustomException {
        if (Common.getBooleanPreference(context, PreferenceKeys.UseArchiveMode).booleanValue()) {
            return saveXmlIncremental(context, backupFile, useWakeLock, progressDialog, handler);
        }
        return saveXml(context, backupFile, useWakeLock, progressDialog, handler);
    }

    private static String getAttributeValue(KXmlParser parser, String attributeName) {
        String value = parser.getAttributeValue("", attributeName);
        if (value == null) {
            return "";
        }
        return value;
    }

    private static String getColumnValue(Cursor cursor, String columnName) {
        int index = cursor.getColumnIndex(columnName);
        if (index < 0) {
            return Common.NullString;
        }
        String value = cursor.getString(index);
        if (value != null || !columnName.equalsIgnoreCase(Common.ProtocolAttributeName)) {
            return value == null ? Common.NullString : value;
        }
        return "0";
    }

    private static String getContactName(Context context, String number) {
        if (!mContacts.containsKey(number)) {
            mContacts.put(number, Common.getContactForNumber(context, number).getName());
        }
        return mContacts.get(number);
    }

    private static DateFormat getDateFormatToUse(Context context) {
        String dateFormatToUse = Common.getStringPreference(context, PreferenceKeys.ReadableDateFormat);
        if (dateFormatToUse.length() == 0 || dateFormatToUse.equalsIgnoreCase("Default")) {
            return SimpleDateFormat.getDateTimeInstance();
        }
        if (dateFormatToUse.equalsIgnoreCase("Short")) {
            return SimpleDateFormat.getDateTimeInstance(3, 3);
        }
        if (dateFormatToUse.equalsIgnoreCase("Medium")) {
            return SimpleDateFormat.getDateTimeInstance(2, 2);
        }
        if (dateFormatToUse.equalsIgnoreCase("Long")) {
            return SimpleDateFormat.getDateTimeInstance(1, 1);
        }
        if (dateFormatToUse.equalsIgnoreCase("Full")) {
            return SimpleDateFormat.getDateTimeInstance(0, 0);
        }
        return new SimpleDateFormat(dateFormatToUse);
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:1:0x002b */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:106:0x031a */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:50:0x016b */
    /* JADX INFO: additional move instructions added (2) to help type inference */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r29v1 */
    /* JADX WARN: Type inference failed for: r11v4 */
    /* JADX WARN: Type inference failed for: r11v5 */
    /* JADX WARN: Type inference failed for: r11v6 */
    /* JADX WARN: Type inference failed for: r29v11, types: [android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r11v8 */
    /* JADX WARN: Type inference failed for: r29v13 */
    /* JADX WARN: Type inference failed for: r11v10 */
    /* JADX WARN: Type inference failed for: r11v11 */
    /* JADX WARN: Type inference failed for: r29v25 */
    /* JADX WARN: Type inference failed for: r11v15 */
    /* JADX WARN: Type inference failed for: r11v16 */
    /* JADX WARN: Type inference failed for: r11v17 */
    /* JADX WARN: Type inference failed for: r29v38 */
    /* JADX WARN: Type inference failed for: r11v23 */
    /* JADX WARN: Type inference failed for: r11v24 */
    /* JADX WARN: Type inference failed for: r29v47 */
    /* JADX WARN: Type inference failed for: r6v37, types: [android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r11v28 */
    /* JADX WARN: Type inference failed for: r29v54 */
    /* JADX WARN: Type inference failed for: r11v32 */
    /* JADX WARN: Type inference failed for: r11v33 */
    /* JADX WARN: Type inference failed for: r11v34 */
    /* JADX WARN: Type inference failed for: r29v67 */
    /* JADX WARN: Type inference failed for: r29v76 */
    /* JADX WARN: Type inference failed for: r11v46 */
    /* JADX WARN: Type inference failed for: r11v47 */
    /* JADX WARN: Type inference failed for: r29v85 */
    /* JADX WARN: Type inference failed for: r6v72, types: [int] */
    /* JADX WARN: Type inference failed for: r11v52 */
    /* JADX WARN: Type inference failed for: r11v53 */
    /* JADX WARN: Type inference failed for: r29v94 */
    /* JADX WARN: Type inference failed for: r11v57 */
    /* JADX WARN: Type inference failed for: r11v58 */
    /* JADX WARN: Type inference failed for: r29v103 */
    /* JADX WARN: Type inference failed for: r11v62 */
    /* JADX WARN: Type inference failed for: r11v63 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x01da  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x01e3  */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:106:0x031a=Splitter:B:106:0x031a, B:50:0x016b=Splitter:B:50:0x016b} */
    /* JADX WARNING: Unknown variable types count: 4 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static com.riteshsahu.SMSBackupRestoreBase.OperationResult saveXml(android.content.Context r26, com.riteshsahu.SMSBackupRestoreBase.BackupFile r27, java.lang.Boolean r28, android.app.ProgressDialog r29, android.os.Handler r30) throws com.riteshsahu.SMSBackupRestoreBase.CustomException {
        /*
            com.riteshsahu.SMSBackupRestoreBase.Common.setupContactNameSettings()
            com.riteshsahu.SMSBackupRestoreBase.OperationResult r19 = new com.riteshsahu.SMSBackupRestoreBase.OperationResult
            r19.<init>()
            r16 = 0
            r21 = 0
            r22 = 0
            java.lang.String r17 = r27.getFullPath()
            java.lang.String r27 = com.riteshsahu.SMSBackupRestoreBase.PreferenceKeys.AddReadableDate
            java.lang.Boolean r11 = com.riteshsahu.SMSBackupRestoreBase.Common.getBooleanPreference(r26, r27)
            java.lang.String r27 = com.riteshsahu.SMSBackupRestoreBase.PreferenceKeys.AddContactNames
            java.lang.Boolean r27 = com.riteshsahu.SMSBackupRestoreBase.Common.getBooleanPreference(r26, r27)
            r13 = -1
            r18 = -1
            r14 = 0
            r15 = 0
            r12 = 0
            java.util.HashMap r5 = new java.util.HashMap
            r5.<init>()
            com.riteshsahu.SMSBackupRestoreBase.SmsBackupProcessor.mContacts = r5
            boolean r5 = r28.booleanValue()     // Catch:{ FileNotFoundException -> 0x02f4, IOException -> 0x0308, CustomException -> 0x0385, Exception -> 0x03d2, all -> 0x0475 }
            if (r5 == 0) goto L_0x0034
            com.riteshsahu.SMSBackupRestoreBase.WakeLocker.acquireLock(r26)     // Catch:{ FileNotFoundException -> 0x02f4, IOException -> 0x0308, CustomException -> 0x0385, Exception -> 0x03d2, all -> 0x0475 }
        L_0x0034:
            waitForStorageToBeReady(r29, r30)     // Catch:{ FileNotFoundException -> 0x02f4, IOException -> 0x0308, CustomException -> 0x0385, Exception -> 0x03d2, all -> 0x0475 }
            r5 = 2131230812(0x7f08005c, float:1.8077687E38)
            r6 = 0
            r0 = r29
            r1 = r30
            r2 = r5
            r3 = r6
            com.riteshsahu.SMSBackupRestoreBase.Common.resetProgressHandler(r0, r1, r2, r3)     // Catch:{ FileNotFoundException -> 0x02f4, IOException -> 0x0308, CustomException -> 0x0385, Exception -> 0x03d2, all -> 0x0475 }
            java.io.File r5 = new java.io.File     // Catch:{ FileNotFoundException -> 0x02f4, IOException -> 0x0308, CustomException -> 0x0385, Exception -> 0x03d2, all -> 0x0475 }
            java.lang.String r6 = com.riteshsahu.SMSBackupRestoreBase.Common.getBackupFilePath(r26)     // Catch:{ FileNotFoundException -> 0x02f4, IOException -> 0x0308, CustomException -> 0x0385, Exception -> 0x03d2, all -> 0x0475 }
            r5.<init>(r6)     // Catch:{ FileNotFoundException -> 0x02f4, IOException -> 0x0308, CustomException -> 0x0385, Exception -> 0x03d2, all -> 0x0475 }
            boolean r6 = r5.exists()     // Catch:{ FileNotFoundException -> 0x02f4, IOException -> 0x0308, CustomException -> 0x0385, Exception -> 0x03d2, all -> 0x0475 }
            if (r6 != 0) goto L_0x0056
            r5.mkdir()     // Catch:{ FileNotFoundException -> 0x02f4, IOException -> 0x0308, CustomException -> 0x0385, Exception -> 0x03d2, all -> 0x0475 }
        L_0x0056:
            boolean r5 = r5.canWrite()     // Catch:{ FileNotFoundException -> 0x02f4, IOException -> 0x0308, CustomException -> 0x0385, Exception -> 0x03d2, all -> 0x0475 }
            if (r5 == 0) goto L_0x02c0
            android.content.ContentResolver r5 = r26.getContentResolver()     // Catch:{ FileNotFoundException -> 0x02f4, IOException -> 0x0308, CustomException -> 0x0385, Exception -> 0x03d2, all -> 0x0475 }
            android.net.Uri r6 = com.riteshsahu.SMSBackupRestoreBase.Common.SmsContentUri     // Catch:{ FileNotFoundException -> 0x02f4, IOException -> 0x0308, CustomException -> 0x0385, Exception -> 0x03d2, all -> 0x0475 }
            r7 = 0
            java.lang.String r8 = com.riteshsahu.SMSBackupRestoreBase.ConversationProcessor.getConversationFilter(r26)     // Catch:{ FileNotFoundException -> 0x02f4, IOException -> 0x0308, CustomException -> 0x0385, Exception -> 0x03d2, all -> 0x0475 }
            r9 = 0
            java.lang.String r10 = "date ASC"
            android.database.Cursor r7 = r5.query(r6, r7, r8, r9, r10)     // Catch:{ FileNotFoundException -> 0x02f4, IOException -> 0x0308, CustomException -> 0x0385, Exception -> 0x03d2, all -> 0x0475 }
            if (r7 == 0) goto L_0x0296
            java.io.FileWriter r23 = new java.io.FileWriter     // Catch:{ FileNotFoundException -> 0x02ac, IOException -> 0x06b7, CustomException -> 0x0605, Exception -> 0x0553, all -> 0x048e }
            r0 = r23
            r1 = r17
            r0.<init>(r1)     // Catch:{ FileNotFoundException -> 0x02ac, IOException -> 0x06b7, CustomException -> 0x0605, Exception -> 0x0553, all -> 0x048e }
            com.riteshsahu.SMSBackupRestoreBase.KXmlSerializer r20 = new com.riteshsahu.SMSBackupRestoreBase.KXmlSerializer     // Catch:{ FileNotFoundException -> 0x02ac, IOException -> 0x06b7, CustomException -> 0x0605, Exception -> 0x0553, all -> 0x048e }
            r20.<init>()     // Catch:{ FileNotFoundException -> 0x02ac, IOException -> 0x06b7, CustomException -> 0x0605, Exception -> 0x0553, all -> 0x048e }
            java.lang.String r5 = "http://xmlpull.org/v1/doc/features.html#indent-output"
            r6 = 1
            r0 = r20
            r1 = r5
            r2 = r6
            r0.setFeature(r1, r2)     // Catch:{ FileNotFoundException -> 0x02ac, IOException -> 0x06b7, CustomException -> 0x0605, Exception -> 0x0553, all -> 0x048e }
            r0 = r20
            r1 = r23
            r0.setOutput(r1)     // Catch:{ FileNotFoundException -> 0x02ac, IOException -> 0x06b7, CustomException -> 0x0605, Exception -> 0x0553, all -> 0x048e }
            java.lang.String r5 = "UTF-8"
            r6 = 1
            java.lang.Boolean r6 = java.lang.Boolean.valueOf(r6)     // Catch:{ FileNotFoundException -> 0x02ac, IOException -> 0x06b7, CustomException -> 0x0605, Exception -> 0x0553, all -> 0x048e }
            r0 = r20
            r1 = r5
            r2 = r6
            r0.startDocument(r1, r2)     // Catch:{ FileNotFoundException -> 0x02ac, IOException -> 0x06b7, CustomException -> 0x0605, Exception -> 0x0553, all -> 0x048e }
            java.lang.String r5 = com.riteshsahu.SMSBackupRestoreBase.PreferenceKeys.AddXslTag     // Catch:{ FileNotFoundException -> 0x02ac, IOException -> 0x06b7, CustomException -> 0x0605, Exception -> 0x0553, all -> 0x048e }
            r0 = r26
            r1 = r5
            java.lang.Boolean r5 = com.riteshsahu.SMSBackupRestoreBase.Common.getBooleanPreference(r0, r1)     // Catch:{ FileNotFoundException -> 0x02ac, IOException -> 0x06b7, CustomException -> 0x0605, Exception -> 0x0553, all -> 0x048e }
            boolean r5 = r5.booleanValue()     // Catch:{ FileNotFoundException -> 0x02ac, IOException -> 0x06b7, CustomException -> 0x0605, Exception -> 0x0553, all -> 0x048e }
            if (r5 == 0) goto L_0x00b4
            java.lang.String r5 = "xml-stylesheet type=\"text/xsl\" href=\"sms.xsl\""
            r0 = r20
            r1 = r5
            r0.processingInstruction(r1)     // Catch:{ FileNotFoundException -> 0x02ac, IOException -> 0x06b7, CustomException -> 0x0605, Exception -> 0x0553, all -> 0x048e }
        L_0x00b4:
            java.lang.String r5 = ""
            java.lang.String r6 = "smses"
            r0 = r20
            r1 = r5
            r2 = r6
            r0.startTag(r1, r2)     // Catch:{ FileNotFoundException -> 0x02ac, IOException -> 0x06b7, CustomException -> 0x0605, Exception -> 0x0553, all -> 0x048e }
            java.lang.String r5 = ""
            java.lang.String r6 = "count"
            int r8 = r7.getCount()     // Catch:{ FileNotFoundException -> 0x02ac, IOException -> 0x06b7, CustomException -> 0x0605, Exception -> 0x0553, all -> 0x048e }
            java.lang.String r8 = java.lang.Integer.toString(r8)     // Catch:{ FileNotFoundException -> 0x02ac, IOException -> 0x06b7, CustomException -> 0x0605, Exception -> 0x0553, all -> 0x048e }
            r0 = r20
            r1 = r5
            r2 = r6
            r3 = r8
            r0.attribute(r1, r2, r3)     // Catch:{ FileNotFoundException -> 0x02ac, IOException -> 0x06b7, CustomException -> 0x0605, Exception -> 0x0553, all -> 0x048e }
            if (r29 == 0) goto L_0x00df
            int r5 = r7.getCount()     // Catch:{ FileNotFoundException -> 0x02ac, IOException -> 0x06b7, CustomException -> 0x0605, Exception -> 0x0553, all -> 0x048e }
            r0 = r29
            r1 = r5
            r0.setMax(r1)     // Catch:{ FileNotFoundException -> 0x02ac, IOException -> 0x06b7, CustomException -> 0x0605, Exception -> 0x0553, all -> 0x048e }
        L_0x00df:
            boolean r5 = r11.booleanValue()     // Catch:{ FileNotFoundException -> 0x02ac, IOException -> 0x06b7, CustomException -> 0x0605, Exception -> 0x0553, all -> 0x048e }
            if (r5 == 0) goto L_0x080f
            java.lang.String r5 = "date"
            int r6 = r7.getColumnIndex(r5)     // Catch:{ FileNotFoundException -> 0x02ac, IOException -> 0x06b7, CustomException -> 0x0605, Exception -> 0x0553, all -> 0x048e }
            if (r6 >= 0) goto L_0x01ec
            r5 = 0
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r5)     // Catch:{ FileNotFoundException -> 0x0769, IOException -> 0x06cb, CustomException -> 0x0619, Exception -> 0x0567, all -> 0x04a7 }
            r10 = r15
            r9 = r14
            r8 = r6
        L_0x00f5:
            boolean r6 = r27.booleanValue()     // Catch:{ FileNotFoundException -> 0x0794, IOException -> 0x06f6, CustomException -> 0x0644, Exception -> 0x0592, all -> 0x04d8 }
            if (r6 == 0) goto L_0x0805
            java.lang.String r6 = "address"
            int r6 = r7.getColumnIndex(r6)     // Catch:{ FileNotFoundException -> 0x0794, IOException -> 0x06f6, CustomException -> 0x0644, Exception -> 0x0592, all -> 0x04d8 }
            if (r6 >= 0) goto L_0x07fc
            r11 = 0
            java.lang.Boolean r27 = java.lang.Boolean.valueOf(r11)     // Catch:{ FileNotFoundException -> 0x07aa, IOException -> 0x070c, CustomException -> 0x065a, Exception -> 0x05a8, all -> 0x04ef }
            r13 = r6
            r12 = r22
            r14 = r21
            r11 = r16
        L_0x010f:
            boolean r6 = r7.moveToNext()     // Catch:{ FileNotFoundException -> 0x07bf, IOException -> 0x0721, CustomException -> 0x066f, Exception -> 0x05bd, all -> 0x0505 }
            if (r6 != 0) goto L_0x01fb
            java.lang.String r6 = ""
            java.lang.String r15 = "smses"
            r0 = r20
            r1 = r6
            r2 = r15
            r0.endTag(r1, r2)     // Catch:{ FileNotFoundException -> 0x07bf, IOException -> 0x0721, CustomException -> 0x066f, Exception -> 0x05bd, all -> 0x0505 }
            r20.endDocument()     // Catch:{ FileNotFoundException -> 0x07bf, IOException -> 0x0721, CustomException -> 0x066f, Exception -> 0x05bd, all -> 0x0505 }
            r20.flush()     // Catch:{ FileNotFoundException -> 0x07bf, IOException -> 0x0721, CustomException -> 0x066f, Exception -> 0x05bd, all -> 0x0505 }
            r23.close()     // Catch:{ FileNotFoundException -> 0x07bf, IOException -> 0x0721, CustomException -> 0x066f, Exception -> 0x05bd, all -> 0x0505 }
            r7.close()     // Catch:{ FileNotFoundException -> 0x07bf, IOException -> 0x0721, CustomException -> 0x066f, Exception -> 0x05bd, all -> 0x0505 }
            r6 = 0
            java.lang.String r7 = com.riteshsahu.SMSBackupRestoreBase.PreferenceKeys.DisableVerification     // Catch:{ FileNotFoundException -> 0x015c, IOException -> 0x0758, CustomException -> 0x06a6, Exception -> 0x05f4, all -> 0x0540 }
            r0 = r26
            r1 = r7
            java.lang.Boolean r7 = com.riteshsahu.SMSBackupRestoreBase.Common.getBooleanPreference(r0, r1)     // Catch:{ FileNotFoundException -> 0x015c, IOException -> 0x0758, CustomException -> 0x06a6, Exception -> 0x05f4, all -> 0x0540 }
            boolean r7 = r7.booleanValue()     // Catch:{ FileNotFoundException -> 0x015c, IOException -> 0x0758, CustomException -> 0x06a6, Exception -> 0x05f4, all -> 0x0540 }
            if (r7 != 0) goto L_0x044f
            r0 = r26
            r1 = r17
            r2 = r12
            r3 = r29
            r4 = r30
            boolean r29 = verifyXml(r0, r1, r2, r3, r4)     // Catch:{ FileNotFoundException -> 0x015c, IOException -> 0x0758, CustomException -> 0x06a6, Exception -> 0x05f4, all -> 0x0540 }
            if (r29 != 0) goto L_0x044f
            com.riteshsahu.SMSBackupRestoreBase.CustomException r29 = new com.riteshsahu.SMSBackupRestoreBase.CustomException     // Catch:{ FileNotFoundException -> 0x015c, IOException -> 0x0758, CustomException -> 0x06a6, Exception -> 0x05f4, all -> 0x0540 }
            r30 = 2131230888(0x7f0800a8, float:1.8077841E38)
            r0 = r26
            r1 = r30
            java.lang.String r30 = r0.getString(r1)     // Catch:{ FileNotFoundException -> 0x015c, IOException -> 0x0758, CustomException -> 0x06a6, Exception -> 0x05f4, all -> 0x0540 }
            r29.<init>(r30)     // Catch:{ FileNotFoundException -> 0x015c, IOException -> 0x0758, CustomException -> 0x06a6, Exception -> 0x05f4, all -> 0x0540 }
            throw r29     // Catch:{ FileNotFoundException -> 0x015c, IOException -> 0x0758, CustomException -> 0x06a6, Exception -> 0x05f4, all -> 0x0540 }
        L_0x015c:
            r29 = move-exception
            r30 = r6
            r7 = r10
            r6 = r9
            r10 = r13
            r9 = r11
            r11 = r14
            r24 = r5
            r5 = r8
            r8 = r29
            r29 = r24
        L_0x016b:
            r8.printStackTrace()     // Catch:{ all -> 0x01c4 }
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ all -> 0x01c4 }
            java.lang.String r14 = "App version: "
            r13.<init>(r14)     // Catch:{ all -> 0x01c4 }
            r14 = 2131230721(0x7f080001, float:1.8077503E38)
            r0 = r26
            r1 = r14
            java.lang.String r14 = r0.getString(r1)     // Catch:{ all -> 0x01c4 }
            java.lang.StringBuilder r13 = r13.append(r14)     // Catch:{ all -> 0x01c4 }
            java.lang.String r13 = r13.toString()     // Catch:{ all -> 0x01c4 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r13)     // Catch:{ all -> 0x01c4 }
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ all -> 0x01c4 }
            java.lang.String r14 = "File Not found: "
            r13.<init>(r14)     // Catch:{ all -> 0x01c4 }
            java.lang.String r14 = r8.getMessage()     // Catch:{ all -> 0x01c4 }
            java.lang.StringBuilder r13 = r13.append(r14)     // Catch:{ all -> 0x01c4 }
            java.lang.String r13 = r13.toString()     // Catch:{ all -> 0x01c4 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r13)     // Catch:{ all -> 0x01c4 }
            com.riteshsahu.SMSBackupRestoreBase.CustomException r13 = new com.riteshsahu.SMSBackupRestoreBase.CustomException     // Catch:{ all -> 0x01c4 }
            r14 = 2131230730(0x7f08000a, float:1.8077521E38)
            r0 = r26
            r1 = r14
            java.lang.String r26 = r0.getString(r1)     // Catch:{ all -> 0x01c4 }
            r14 = 1
            java.lang.Object[] r14 = new java.lang.Object[r14]     // Catch:{ all -> 0x01c4 }
            r15 = 0
            java.lang.String r8 = r8.getMessage()     // Catch:{ all -> 0x01c4 }
            r14[r15] = r8     // Catch:{ all -> 0x01c4 }
            r0 = r26
            r1 = r14
            java.lang.String r26 = java.lang.String.format(r0, r1)     // Catch:{ all -> 0x01c4 }
            r0 = r13
            r1 = r26
            r0.<init>(r1)     // Catch:{ all -> 0x01c4 }
            throw r13     // Catch:{ all -> 0x01c4 }
        L_0x01c4:
            r26 = move-exception
            r8 = r10
            r10 = r12
            r24 = r7
            r7 = r9
            r9 = r11
            r11 = r26
            r26 = r27
            r27 = r29
            r29 = r30
            r30 = r5
            r5 = r6
            r6 = r24
        L_0x01d8:
            if (r29 == 0) goto L_0x01dd
            r29.close()
        L_0x01dd:
            boolean r26 = r28.booleanValue()
            if (r26 == 0) goto L_0x01e6
            com.riteshsahu.SMSBackupRestoreBase.WakeLocker.releaseLock()
        L_0x01e6:
            java.util.HashMap<java.lang.String, java.lang.String> r26 = com.riteshsahu.SMSBackupRestoreBase.SmsBackupProcessor.mContacts
            r26.clear()
            throw r11
        L_0x01ec:
            java.text.DateFormat r5 = getDateFormatToUse(r26)     // Catch:{ FileNotFoundException -> 0x0769, IOException -> 0x06cb, CustomException -> 0x0619, Exception -> 0x0567, all -> 0x04a7 }
            java.util.Date r8 = new java.util.Date     // Catch:{ FileNotFoundException -> 0x077d, IOException -> 0x06df, CustomException -> 0x062d, Exception -> 0x057b, all -> 0x04c0 }
            r8.<init>()     // Catch:{ FileNotFoundException -> 0x077d, IOException -> 0x06df, CustomException -> 0x062d, Exception -> 0x057b, all -> 0x04c0 }
            r10 = r8
            r9 = r5
            r8 = r6
            r5 = r11
            goto L_0x00f5
        L_0x01fb:
            int r15 = r12 + 1
            r6 = 0
            java.lang.String r12 = ""
            java.lang.String r16 = "sms"
            r0 = r20
            r1 = r12
            r2 = r16
            r0.startTag(r1, r2)     // Catch:{ FileNotFoundException -> 0x07d0, IOException -> 0x0732, CustomException -> 0x0680, Exception -> 0x05ce, all -> 0x0518 }
            java.lang.String[] r16 = com.riteshsahu.SMSBackupRestoreBase.Common.ColumnNames     // Catch:{ FileNotFoundException -> 0x07d0, IOException -> 0x0732, CustomException -> 0x0680, Exception -> 0x05ce, all -> 0x0518 }
            r0 = r16
            int r0 = r0.length     // Catch:{ FileNotFoundException -> 0x07d0, IOException -> 0x0732, CustomException -> 0x0680, Exception -> 0x05ce, all -> 0x0518 }
            r18 = r0
            r12 = 0
            r21 = r12
            r12 = r6
        L_0x0215:
            r0 = r21
            r1 = r18
            if (r0 < r1) goto L_0x0279
            if (r12 == 0) goto L_0x028e
            int r6 = r11 + 1
            r11 = r14
        L_0x0220:
            boolean r12 = r5.booleanValue()     // Catch:{ FileNotFoundException -> 0x07e2, IOException -> 0x0744, CustomException -> 0x0692, Exception -> 0x05e0, all -> 0x052b }
            if (r12 == 0) goto L_0x0241
            long r21 = r7.getLong(r8)     // Catch:{ FileNotFoundException -> 0x07e2, IOException -> 0x0744, CustomException -> 0x0692, Exception -> 0x05e0, all -> 0x052b }
            r0 = r10
            r1 = r21
            r0.setTime(r1)     // Catch:{ FileNotFoundException -> 0x07e2, IOException -> 0x0744, CustomException -> 0x0692, Exception -> 0x05e0, all -> 0x052b }
            java.lang.String r12 = ""
            java.lang.String r14 = "readable_date"
            java.lang.String r16 = r9.format(r10)     // Catch:{ FileNotFoundException -> 0x07e2, IOException -> 0x0744, CustomException -> 0x0692, Exception -> 0x05e0, all -> 0x052b }
            r0 = r20
            r1 = r12
            r2 = r14
            r3 = r16
            r0.attribute(r1, r2, r3)     // Catch:{ FileNotFoundException -> 0x07e2, IOException -> 0x0744, CustomException -> 0x0692, Exception -> 0x05e0, all -> 0x052b }
        L_0x0241:
            boolean r12 = r27.booleanValue()     // Catch:{ FileNotFoundException -> 0x07e2, IOException -> 0x0744, CustomException -> 0x0692, Exception -> 0x05e0, all -> 0x052b }
            if (r12 == 0) goto L_0x0260
            java.lang.String r12 = ""
            java.lang.String r14 = "contact_name"
            java.lang.String r16 = r7.getString(r13)     // Catch:{ FileNotFoundException -> 0x07e2, IOException -> 0x0744, CustomException -> 0x0692, Exception -> 0x05e0, all -> 0x052b }
            r0 = r26
            r1 = r16
            java.lang.String r16 = getContactName(r0, r1)     // Catch:{ FileNotFoundException -> 0x07e2, IOException -> 0x0744, CustomException -> 0x0692, Exception -> 0x05e0, all -> 0x052b }
            r0 = r20
            r1 = r12
            r2 = r14
            r3 = r16
            r0.attribute(r1, r2, r3)     // Catch:{ FileNotFoundException -> 0x07e2, IOException -> 0x0744, CustomException -> 0x0692, Exception -> 0x05e0, all -> 0x052b }
        L_0x0260:
            java.lang.String r12 = ""
            java.lang.String r14 = "sms"
            r0 = r20
            r1 = r12
            r2 = r14
            r0.endTag(r1, r2)     // Catch:{ FileNotFoundException -> 0x07e2, IOException -> 0x0744, CustomException -> 0x0692, Exception -> 0x05e0, all -> 0x052b }
            if (r29 == 0) goto L_0x0274
            r12 = 1
            r0 = r29
            r1 = r12
            r0.incrementProgressBy(r1)     // Catch:{ FileNotFoundException -> 0x07e2, IOException -> 0x0744, CustomException -> 0x0692, Exception -> 0x05e0, all -> 0x052b }
        L_0x0274:
            r12 = r15
            r14 = r11
            r11 = r6
            goto L_0x010f
        L_0x0279:
            r6 = r16[r21]     // Catch:{ FileNotFoundException -> 0x07d0, IOException -> 0x0732, CustomException -> 0x0680, Exception -> 0x05ce, all -> 0x0518 }
            r0 = r20
            r1 = r6
            r2 = r7
            boolean r6 = setSerializerAttribute(r0, r1, r2)     // Catch:{ FileNotFoundException -> 0x07d0, IOException -> 0x0732, CustomException -> 0x0680, Exception -> 0x05ce, all -> 0x0518 }
            if (r6 != 0) goto L_0x07f9
            if (r12 != 0) goto L_0x07f6
            r6 = 1
        L_0x0288:
            int r12 = r21 + 1
            r21 = r12
            r12 = r6
            goto L_0x0215
        L_0x028e:
            int r6 = r14 + 1
            r24 = r6
            r6 = r11
            r11 = r24
            goto L_0x0220
        L_0x0296:
            java.lang.String r29 = "Could not find any records."
            com.riteshsahu.SMSBackupRestoreBase.Common.logDebug(r29)     // Catch:{ FileNotFoundException -> 0x02ac, IOException -> 0x06b7, CustomException -> 0x0605, Exception -> 0x0553, all -> 0x048e }
            com.riteshsahu.SMSBackupRestoreBase.CustomException r29 = new com.riteshsahu.SMSBackupRestoreBase.CustomException     // Catch:{ FileNotFoundException -> 0x02ac, IOException -> 0x06b7, CustomException -> 0x0605, Exception -> 0x0553, all -> 0x048e }
            r30 = 2131230735(0x7f08000f, float:1.8077531E38)
            r0 = r26
            r1 = r30
            java.lang.String r30 = r0.getString(r1)     // Catch:{ FileNotFoundException -> 0x02ac, IOException -> 0x06b7, CustomException -> 0x0605, Exception -> 0x0553, all -> 0x048e }
            r29.<init>(r30)     // Catch:{ FileNotFoundException -> 0x02ac, IOException -> 0x06b7, CustomException -> 0x0605, Exception -> 0x0553, all -> 0x048e }
            throw r29     // Catch:{ FileNotFoundException -> 0x02ac, IOException -> 0x06b7, CustomException -> 0x0605, Exception -> 0x0553, all -> 0x048e }
        L_0x02ac:
            r29 = move-exception
            r8 = r29
            r30 = r7
            r6 = r14
            r10 = r18
            r5 = r13
            r12 = r22
            r9 = r16
            r7 = r15
            r29 = r11
            r11 = r21
            goto L_0x016b
        L_0x02c0:
            java.lang.StringBuilder r29 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x02f4, IOException -> 0x0308, CustomException -> 0x0385, Exception -> 0x03d2, all -> 0x0475 }
            java.lang.String r30 = "No Write Access to file: "
            r29.<init>(r30)     // Catch:{ FileNotFoundException -> 0x02f4, IOException -> 0x0308, CustomException -> 0x0385, Exception -> 0x03d2, all -> 0x0475 }
            r0 = r29
            r1 = r17
            java.lang.StringBuilder r29 = r0.append(r1)     // Catch:{ FileNotFoundException -> 0x02f4, IOException -> 0x0308, CustomException -> 0x0385, Exception -> 0x03d2, all -> 0x0475 }
            java.lang.String r29 = r29.toString()     // Catch:{ FileNotFoundException -> 0x02f4, IOException -> 0x0308, CustomException -> 0x0385, Exception -> 0x03d2, all -> 0x0475 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logDebug(r29)     // Catch:{ FileNotFoundException -> 0x02f4, IOException -> 0x0308, CustomException -> 0x0385, Exception -> 0x03d2, all -> 0x0475 }
            com.riteshsahu.SMSBackupRestoreBase.CustomException r29 = new com.riteshsahu.SMSBackupRestoreBase.CustomException     // Catch:{ FileNotFoundException -> 0x02f4, IOException -> 0x0308, CustomException -> 0x0385, Exception -> 0x03d2, all -> 0x0475 }
            r30 = 2131230729(0x7f080009, float:1.807752E38)
            r0 = r26
            r1 = r30
            java.lang.String r30 = r0.getString(r1)     // Catch:{ FileNotFoundException -> 0x02f4, IOException -> 0x0308, CustomException -> 0x0385, Exception -> 0x03d2, all -> 0x0475 }
            r5 = 1
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ FileNotFoundException -> 0x02f4, IOException -> 0x0308, CustomException -> 0x0385, Exception -> 0x03d2, all -> 0x0475 }
            r6 = 0
            r5[r6] = r17     // Catch:{ FileNotFoundException -> 0x02f4, IOException -> 0x0308, CustomException -> 0x0385, Exception -> 0x03d2, all -> 0x0475 }
            r0 = r30
            r1 = r5
            java.lang.String r30 = java.lang.String.format(r0, r1)     // Catch:{ FileNotFoundException -> 0x02f4, IOException -> 0x0308, CustomException -> 0x0385, Exception -> 0x03d2, all -> 0x0475 }
            r29.<init>(r30)     // Catch:{ FileNotFoundException -> 0x02f4, IOException -> 0x0308, CustomException -> 0x0385, Exception -> 0x03d2, all -> 0x0475 }
            throw r29     // Catch:{ FileNotFoundException -> 0x02f4, IOException -> 0x0308, CustomException -> 0x0385, Exception -> 0x03d2, all -> 0x0475 }
        L_0x02f4:
            r29 = move-exception
            r8 = r29
            r30 = r12
            r7 = r15
            r6 = r14
            r10 = r18
            r5 = r13
            r9 = r16
            r12 = r22
            r29 = r11
            r11 = r21
            goto L_0x016b
        L_0x0308:
            r29 = move-exception
            r8 = r29
            r30 = r12
            r7 = r15
            r6 = r14
            r10 = r18
            r5 = r13
            r9 = r16
            r12 = r22
            r29 = r11
            r11 = r21
        L_0x031a:
            r8.printStackTrace()     // Catch:{ all -> 0x01c4 }
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ all -> 0x01c4 }
            java.lang.String r14 = "App version: "
            r13.<init>(r14)     // Catch:{ all -> 0x01c4 }
            r14 = 2131230721(0x7f080001, float:1.8077503E38)
            r0 = r26
            r1 = r14
            java.lang.String r14 = r0.getString(r1)     // Catch:{ all -> 0x01c4 }
            java.lang.StringBuilder r13 = r13.append(r14)     // Catch:{ all -> 0x01c4 }
            java.lang.String r13 = r13.toString()     // Catch:{ all -> 0x01c4 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r13)     // Catch:{ all -> 0x01c4 }
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ all -> 0x01c4 }
            java.lang.String r14 = "Count: "
            r13.<init>(r14)     // Catch:{ all -> 0x01c4 }
            java.lang.StringBuilder r13 = r13.append(r12)     // Catch:{ all -> 0x01c4 }
            java.lang.String r13 = r13.toString()     // Catch:{ all -> 0x01c4 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r13)     // Catch:{ all -> 0x01c4 }
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ all -> 0x01c4 }
            java.lang.String r14 = "IOException: "
            r13.<init>(r14)     // Catch:{ all -> 0x01c4 }
            java.lang.String r14 = r8.getMessage()     // Catch:{ all -> 0x01c4 }
            java.lang.StringBuilder r13 = r13.append(r14)     // Catch:{ all -> 0x01c4 }
            java.lang.String r13 = r13.toString()     // Catch:{ all -> 0x01c4 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r13)     // Catch:{ all -> 0x01c4 }
            com.riteshsahu.SMSBackupRestoreBase.CustomException r13 = new com.riteshsahu.SMSBackupRestoreBase.CustomException     // Catch:{ all -> 0x01c4 }
            r14 = 2131230725(0x7f080005, float:1.807751E38)
            r0 = r26
            r1 = r14
            java.lang.String r26 = r0.getString(r1)     // Catch:{ all -> 0x01c4 }
            r14 = 1
            java.lang.Object[] r14 = new java.lang.Object[r14]     // Catch:{ all -> 0x01c4 }
            r15 = 0
            java.lang.String r8 = r8.getMessage()     // Catch:{ all -> 0x01c4 }
            r14[r15] = r8     // Catch:{ all -> 0x01c4 }
            r0 = r26
            r1 = r14
            java.lang.String r26 = java.lang.String.format(r0, r1)     // Catch:{ all -> 0x01c4 }
            r0 = r13
            r1 = r26
            r0.<init>(r1)     // Catch:{ all -> 0x01c4 }
            throw r13     // Catch:{ all -> 0x01c4 }
        L_0x0385:
            r29 = move-exception
            r8 = r29
            r30 = r12
            r7 = r15
            r6 = r14
            r10 = r18
            r5 = r13
            r9 = r16
            r12 = r22
            r29 = r11
            r11 = r21
        L_0x0397:
            r8.printStackTrace()     // Catch:{ all -> 0x01c4 }
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ all -> 0x01c4 }
            java.lang.String r14 = "App version: "
            r13.<init>(r14)     // Catch:{ all -> 0x01c4 }
            r14 = 2131230721(0x7f080001, float:1.8077503E38)
            r0 = r26
            r1 = r14
            java.lang.String r26 = r0.getString(r1)     // Catch:{ all -> 0x01c4 }
            r0 = r13
            r1 = r26
            java.lang.StringBuilder r26 = r0.append(r1)     // Catch:{ all -> 0x01c4 }
            java.lang.String r26 = r26.toString()     // Catch:{ all -> 0x01c4 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r26)     // Catch:{ all -> 0x01c4 }
            java.lang.StringBuilder r26 = new java.lang.StringBuilder     // Catch:{ all -> 0x01c4 }
            java.lang.String r13 = "Count: "
            r0 = r26
            r1 = r13
            r0.<init>(r1)     // Catch:{ all -> 0x01c4 }
            r0 = r26
            r1 = r12
            java.lang.StringBuilder r26 = r0.append(r1)     // Catch:{ all -> 0x01c4 }
            java.lang.String r26 = r26.toString()     // Catch:{ all -> 0x01c4 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r26)     // Catch:{ all -> 0x01c4 }
            throw r8     // Catch:{ all -> 0x01c4 }
        L_0x03d2:
            r29 = move-exception
            r8 = r29
            r30 = r12
            r7 = r15
            r6 = r14
            r10 = r18
            r5 = r13
            r9 = r16
            r12 = r22
            r29 = r11
            r11 = r21
        L_0x03e4:
            r8.printStackTrace()     // Catch:{ all -> 0x01c4 }
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ all -> 0x01c4 }
            java.lang.String r14 = "App version: "
            r13.<init>(r14)     // Catch:{ all -> 0x01c4 }
            r14 = 2131230721(0x7f080001, float:1.8077503E38)
            r0 = r26
            r1 = r14
            java.lang.String r14 = r0.getString(r1)     // Catch:{ all -> 0x01c4 }
            java.lang.StringBuilder r13 = r13.append(r14)     // Catch:{ all -> 0x01c4 }
            java.lang.String r13 = r13.toString()     // Catch:{ all -> 0x01c4 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r13)     // Catch:{ all -> 0x01c4 }
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ all -> 0x01c4 }
            java.lang.String r14 = "Count: "
            r13.<init>(r14)     // Catch:{ all -> 0x01c4 }
            java.lang.StringBuilder r13 = r13.append(r12)     // Catch:{ all -> 0x01c4 }
            java.lang.String r13 = r13.toString()     // Catch:{ all -> 0x01c4 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r13)     // Catch:{ all -> 0x01c4 }
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ all -> 0x01c4 }
            java.lang.String r14 = "Error occurred during backup: "
            r13.<init>(r14)     // Catch:{ all -> 0x01c4 }
            java.lang.String r14 = r8.getMessage()     // Catch:{ all -> 0x01c4 }
            java.lang.StringBuilder r13 = r13.append(r14)     // Catch:{ all -> 0x01c4 }
            java.lang.String r13 = r13.toString()     // Catch:{ all -> 0x01c4 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r13)     // Catch:{ all -> 0x01c4 }
            com.riteshsahu.SMSBackupRestoreBase.CustomException r13 = new com.riteshsahu.SMSBackupRestoreBase.CustomException     // Catch:{ all -> 0x01c4 }
            r14 = 2131230725(0x7f080005, float:1.807751E38)
            r0 = r26
            r1 = r14
            java.lang.String r26 = r0.getString(r1)     // Catch:{ all -> 0x01c4 }
            r14 = 1
            java.lang.Object[] r14 = new java.lang.Object[r14]     // Catch:{ all -> 0x01c4 }
            r15 = 0
            java.lang.String r8 = r8.getMessage()     // Catch:{ all -> 0x01c4 }
            r14[r15] = r8     // Catch:{ all -> 0x01c4 }
            r0 = r26
            r1 = r14
            java.lang.String r26 = java.lang.String.format(r0, r1)     // Catch:{ all -> 0x01c4 }
            r0 = r13
            r1 = r26
            r0.<init>(r1)     // Catch:{ all -> 0x01c4 }
            throw r13     // Catch:{ all -> 0x01c4 }
        L_0x044f:
            if (r6 == 0) goto L_0x0454
            r6.close()
        L_0x0454:
            boolean r26 = r28.booleanValue()
            if (r26 == 0) goto L_0x045d
            com.riteshsahu.SMSBackupRestoreBase.WakeLocker.releaseLock()
        L_0x045d:
            java.util.HashMap<java.lang.String, java.lang.String> r26 = com.riteshsahu.SMSBackupRestoreBase.SmsBackupProcessor.mContacts
            r26.clear()
            r0 = r19
            r1 = r11
            r0.setFailed(r1)
            r0 = r19
            r1 = r14
            r0.setSuccessful(r1)
            r0 = r19
            r1 = r12
            r0.setTotal(r1)
            return r19
        L_0x0475:
            r26 = move-exception
            r29 = r12
            r6 = r15
            r5 = r14
            r8 = r18
            r30 = r13
            r10 = r22
            r9 = r21
            r7 = r16
            r24 = r26
            r26 = r27
            r27 = r11
            r11 = r24
            goto L_0x01d8
        L_0x048e:
            r26 = move-exception
            r29 = r7
            r6 = r15
            r5 = r14
            r8 = r18
            r30 = r13
            r10 = r22
            r9 = r21
            r7 = r16
            r24 = r26
            r26 = r27
            r27 = r11
            r11 = r24
            goto L_0x01d8
        L_0x04a7:
            r26 = move-exception
            r29 = r7
            r5 = r14
            r8 = r18
            r30 = r6
            r10 = r22
            r9 = r21
            r6 = r15
            r7 = r16
            r24 = r27
            r27 = r11
            r11 = r26
            r26 = r24
            goto L_0x01d8
        L_0x04c0:
            r26 = move-exception
            r29 = r7
            r8 = r18
            r30 = r6
            r10 = r22
            r9 = r21
            r6 = r15
            r7 = r16
            r24 = r26
            r26 = r27
            r27 = r11
            r11 = r24
            goto L_0x01d8
        L_0x04d8:
            r26 = move-exception
            r11 = r26
            r29 = r7
            r6 = r10
            r30 = r8
            r8 = r18
            r26 = r27
            r10 = r22
            r7 = r16
            r27 = r5
            r5 = r9
            r9 = r21
            goto L_0x01d8
        L_0x04ef:
            r26 = move-exception
            r11 = r26
            r29 = r7
            r30 = r8
            r8 = r6
            r26 = r27
            r7 = r16
            r27 = r5
            r6 = r10
            r10 = r22
            r5 = r9
            r9 = r21
            goto L_0x01d8
        L_0x0505:
            r26 = move-exception
            r29 = r7
            r6 = r10
            r30 = r8
            r8 = r13
            r10 = r12
            r7 = r11
            r11 = r26
            r26 = r27
            r27 = r5
            r5 = r9
            r9 = r14
            goto L_0x01d8
        L_0x0518:
            r26 = move-exception
            r29 = r7
            r6 = r10
            r30 = r8
            r8 = r13
            r10 = r15
            r7 = r11
            r11 = r26
            r26 = r27
            r27 = r5
            r5 = r9
            r9 = r14
            goto L_0x01d8
        L_0x052b:
            r26 = move-exception
            r29 = r7
            r30 = r8
            r8 = r13
            r7 = r6
            r6 = r10
            r10 = r15
            r24 = r27
            r27 = r5
            r5 = r9
            r9 = r11
            r11 = r26
            r26 = r24
            goto L_0x01d8
        L_0x0540:
            r26 = move-exception
            r29 = r6
            r30 = r8
            r7 = r11
            r8 = r13
            r11 = r26
            r6 = r10
            r26 = r27
            r10 = r12
            r27 = r5
            r5 = r9
            r9 = r14
            goto L_0x01d8
        L_0x0553:
            r29 = move-exception
            r8 = r29
            r30 = r7
            r6 = r14
            r10 = r18
            r5 = r13
            r12 = r22
            r9 = r16
            r7 = r15
            r29 = r11
            r11 = r21
            goto L_0x03e4
        L_0x0567:
            r29 = move-exception
            r8 = r29
            r30 = r7
            r10 = r18
            r5 = r6
            r12 = r22
            r9 = r16
            r29 = r11
            r7 = r15
            r6 = r14
            r11 = r21
            goto L_0x03e4
        L_0x057b:
            r29 = move-exception
            r8 = r29
            r30 = r7
            r10 = r18
            r12 = r22
            r9 = r16
            r29 = r11
            r7 = r15
            r11 = r21
            r24 = r5
            r5 = r6
            r6 = r24
            goto L_0x03e4
        L_0x0592:
            r29 = move-exception
            r30 = r7
            r6 = r9
            r12 = r22
            r11 = r21
            r7 = r10
            r9 = r16
            r10 = r18
            r24 = r5
            r5 = r8
            r8 = r29
            r29 = r24
            goto L_0x03e4
        L_0x05a8:
            r29 = move-exception
            r30 = r7
            r12 = r22
            r11 = r21
            r7 = r10
            r10 = r6
            r6 = r9
            r9 = r16
            r24 = r29
            r29 = r5
            r5 = r8
            r8 = r24
            goto L_0x03e4
        L_0x05bd:
            r29 = move-exception
            r30 = r7
            r6 = r9
            r7 = r10
            r9 = r11
            r10 = r13
            r11 = r14
            r24 = r5
            r5 = r8
            r8 = r29
            r29 = r24
            goto L_0x03e4
        L_0x05ce:
            r29 = move-exception
            r30 = r7
            r6 = r9
            r12 = r15
            r7 = r10
            r9 = r11
            r11 = r14
            r10 = r13
            r24 = r29
            r29 = r5
            r5 = r8
            r8 = r24
            goto L_0x03e4
        L_0x05e0:
            r29 = move-exception
            r30 = r7
            r12 = r15
            r7 = r10
            r10 = r13
            r24 = r9
            r9 = r6
            r6 = r24
            r25 = r5
            r5 = r8
            r8 = r29
            r29 = r25
            goto L_0x03e4
        L_0x05f4:
            r29 = move-exception
            r30 = r6
            r7 = r10
            r6 = r9
            r10 = r13
            r9 = r11
            r11 = r14
            r24 = r5
            r5 = r8
            r8 = r29
            r29 = r24
            goto L_0x03e4
        L_0x0605:
            r29 = move-exception
            r8 = r29
            r30 = r7
            r6 = r14
            r10 = r18
            r5 = r13
            r12 = r22
            r9 = r16
            r7 = r15
            r29 = r11
            r11 = r21
            goto L_0x0397
        L_0x0619:
            r29 = move-exception
            r8 = r29
            r30 = r7
            r10 = r18
            r5 = r6
            r12 = r22
            r9 = r16
            r29 = r11
            r7 = r15
            r6 = r14
            r11 = r21
            goto L_0x0397
        L_0x062d:
            r29 = move-exception
            r8 = r29
            r30 = r7
            r10 = r18
            r12 = r22
            r9 = r16
            r29 = r11
            r7 = r15
            r11 = r21
            r24 = r5
            r5 = r6
            r6 = r24
            goto L_0x0397
        L_0x0644:
            r29 = move-exception
            r30 = r7
            r6 = r9
            r12 = r22
            r11 = r21
            r7 = r10
            r9 = r16
            r10 = r18
            r24 = r5
            r5 = r8
            r8 = r29
            r29 = r24
            goto L_0x0397
        L_0x065a:
            r29 = move-exception
            r30 = r7
            r12 = r22
            r11 = r21
            r7 = r10
            r10 = r6
            r6 = r9
            r9 = r16
            r24 = r29
            r29 = r5
            r5 = r8
            r8 = r24
            goto L_0x0397
        L_0x066f:
            r29 = move-exception
            r30 = r7
            r6 = r9
            r7 = r10
            r9 = r11
            r10 = r13
            r11 = r14
            r24 = r5
            r5 = r8
            r8 = r29
            r29 = r24
            goto L_0x0397
        L_0x0680:
            r29 = move-exception
            r30 = r7
            r6 = r9
            r12 = r15
            r7 = r10
            r9 = r11
            r11 = r14
            r10 = r13
            r24 = r29
            r29 = r5
            r5 = r8
            r8 = r24
            goto L_0x0397
        L_0x0692:
            r29 = move-exception
            r30 = r7
            r12 = r15
            r7 = r10
            r10 = r13
            r24 = r9
            r9 = r6
            r6 = r24
            r25 = r5
            r5 = r8
            r8 = r29
            r29 = r25
            goto L_0x0397
        L_0x06a6:
            r29 = move-exception
            r30 = r6
            r7 = r10
            r6 = r9
            r10 = r13
            r9 = r11
            r11 = r14
            r24 = r5
            r5 = r8
            r8 = r29
            r29 = r24
            goto L_0x0397
        L_0x06b7:
            r29 = move-exception
            r8 = r29
            r30 = r7
            r6 = r14
            r10 = r18
            r5 = r13
            r12 = r22
            r9 = r16
            r7 = r15
            r29 = r11
            r11 = r21
            goto L_0x031a
        L_0x06cb:
            r29 = move-exception
            r8 = r29
            r30 = r7
            r10 = r18
            r5 = r6
            r12 = r22
            r9 = r16
            r29 = r11
            r7 = r15
            r6 = r14
            r11 = r21
            goto L_0x031a
        L_0x06df:
            r29 = move-exception
            r8 = r29
            r30 = r7
            r10 = r18
            r12 = r22
            r9 = r16
            r29 = r11
            r7 = r15
            r11 = r21
            r24 = r5
            r5 = r6
            r6 = r24
            goto L_0x031a
        L_0x06f6:
            r29 = move-exception
            r30 = r7
            r6 = r9
            r12 = r22
            r11 = r21
            r7 = r10
            r9 = r16
            r10 = r18
            r24 = r5
            r5 = r8
            r8 = r29
            r29 = r24
            goto L_0x031a
        L_0x070c:
            r29 = move-exception
            r30 = r7
            r12 = r22
            r11 = r21
            r7 = r10
            r10 = r6
            r6 = r9
            r9 = r16
            r24 = r29
            r29 = r5
            r5 = r8
            r8 = r24
            goto L_0x031a
        L_0x0721:
            r29 = move-exception
            r30 = r7
            r6 = r9
            r7 = r10
            r9 = r11
            r10 = r13
            r11 = r14
            r24 = r5
            r5 = r8
            r8 = r29
            r29 = r24
            goto L_0x031a
        L_0x0732:
            r29 = move-exception
            r30 = r7
            r6 = r9
            r12 = r15
            r7 = r10
            r9 = r11
            r11 = r14
            r10 = r13
            r24 = r29
            r29 = r5
            r5 = r8
            r8 = r24
            goto L_0x031a
        L_0x0744:
            r29 = move-exception
            r30 = r7
            r12 = r15
            r7 = r10
            r10 = r13
            r24 = r9
            r9 = r6
            r6 = r24
            r25 = r5
            r5 = r8
            r8 = r29
            r29 = r25
            goto L_0x031a
        L_0x0758:
            r29 = move-exception
            r30 = r6
            r7 = r10
            r6 = r9
            r10 = r13
            r9 = r11
            r11 = r14
            r24 = r5
            r5 = r8
            r8 = r29
            r29 = r24
            goto L_0x031a
        L_0x0769:
            r29 = move-exception
            r8 = r29
            r30 = r7
            r10 = r18
            r5 = r6
            r12 = r22
            r9 = r16
            r29 = r11
            r7 = r15
            r6 = r14
            r11 = r21
            goto L_0x016b
        L_0x077d:
            r29 = move-exception
            r8 = r29
            r30 = r7
            r10 = r18
            r12 = r22
            r9 = r16
            r29 = r11
            r7 = r15
            r11 = r21
            r24 = r5
            r5 = r6
            r6 = r24
            goto L_0x016b
        L_0x0794:
            r29 = move-exception
            r30 = r7
            r6 = r9
            r12 = r22
            r11 = r21
            r7 = r10
            r9 = r16
            r10 = r18
            r24 = r5
            r5 = r8
            r8 = r29
            r29 = r24
            goto L_0x016b
        L_0x07aa:
            r29 = move-exception
            r30 = r7
            r12 = r22
            r11 = r21
            r7 = r10
            r10 = r6
            r6 = r9
            r9 = r16
            r24 = r29
            r29 = r5
            r5 = r8
            r8 = r24
            goto L_0x016b
        L_0x07bf:
            r29 = move-exception
            r30 = r7
            r6 = r9
            r7 = r10
            r9 = r11
            r10 = r13
            r11 = r14
            r24 = r5
            r5 = r8
            r8 = r29
            r29 = r24
            goto L_0x016b
        L_0x07d0:
            r29 = move-exception
            r30 = r7
            r6 = r9
            r12 = r15
            r7 = r10
            r9 = r11
            r11 = r14
            r10 = r13
            r24 = r29
            r29 = r5
            r5 = r8
            r8 = r24
            goto L_0x016b
        L_0x07e2:
            r29 = move-exception
            r30 = r7
            r12 = r15
            r7 = r10
            r10 = r13
            r24 = r9
            r9 = r6
            r6 = r24
            r25 = r5
            r5 = r8
            r8 = r29
            r29 = r25
            goto L_0x016b
        L_0x07f6:
            r6 = r12
            goto L_0x0288
        L_0x07f9:
            r6 = r12
            goto L_0x0288
        L_0x07fc:
            r13 = r6
            r12 = r22
            r14 = r21
            r11 = r16
            goto L_0x010f
        L_0x0805:
            r13 = r18
            r12 = r22
            r14 = r21
            r11 = r16
            goto L_0x010f
        L_0x080f:
            r10 = r15
            r9 = r14
            r8 = r13
            r5 = r11
            goto L_0x00f5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.riteshsahu.SMSBackupRestoreBase.SmsBackupProcessor.saveXml(android.content.Context, com.riteshsahu.SMSBackupRestoreBase.BackupFile, java.lang.Boolean, android.app.ProgressDialog, android.os.Handler):com.riteshsahu.SMSBackupRestoreBase.OperationResult");
    }

    private static void waitForStorageToBeReady(ProgressDialog progressDialog, Handler handler) throws InterruptedException {
        boolean handlerSet = false;
        int waitCounter = 0;
        while (true) {
            Common.logDebug("Trying to getExternalStorageState");
            String state = Environment.getExternalStorageState();
            if ("mounted".equals(state)) {
                Common.logDebug("External storage is ready");
                return;
            } else if (waitCounter >= 100) {
                Common.logDebug("Waited " + waitCounter + " times. Now continuing..");
                return;
            } else {
                Common.logDebug("External storage state: " + state + ". Waiting...");
                if (!handlerSet) {
                    handlerSet = true;
                    Common.resetProgressHandler(progressDialog, handler, R.string.waiting_for_external_storage, 100);
                }
                waitCounter++;
                if (progressDialog != null) {
                    progressDialog.incrementProgressBy(1);
                }
                Thread.sleep((long) 2000);
            }
        }
    }

    /* JADX INFO: Multiple debug info for r7v49 long: [D('currentDate' long), D('lastId' java.lang.Long)] */
    /* JADX INFO: Multiple debug info for r7v63 java.lang.Long: [D('currentDate' long), D('lastDate' java.lang.Long)] */
    /* JADX INFO: Multiple debug info for r7v64 java.lang.Long: [D('currentId' long), D('lastId' java.lang.Long)] */
    /* JADX INFO: Multiple debug info for r23v3 com.riteshsahu.SMSBackupRestoreBase.KXmlParser: [D('parser' com.riteshsahu.SMSBackupRestoreBase.KXmlParser), D('oldBackupFileExists' boolean)] */
    /* JADX INFO: Multiple debug info for r5v107 int: [D('oldMessageCount' int), D('columnValue' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r5v119 java.lang.String: [D('name' java.lang.String), D('columnName' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r5v129 java.lang.Long: [D('currentDate' java.lang.Long), D('columnName' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r5v147 java.lang.Long: [D('lastId' java.lang.Long), D('lastDate' java.lang.Long)] */
    /* JADX INFO: Multiple debug info for r37v136 java.lang.Long: [D('backupFile' com.riteshsahu.SMSBackupRestoreBase.BackupFile), D('lastDate' java.lang.Long)] */
    /* JADX WARN: Type inference failed for: r36v17 */
    /* JADX WARN: Type inference failed for: r36v21 */
    /* JADX WARN: Type inference failed for: r36v26 */
    /* JADX WARN: Type inference failed for: r10v19, types: [int] */
    /* JADX WARN: Type inference failed for: r36v28 */
    /* JADX WARN: Type inference failed for: r36v30 */
    /* JADX WARN: Type inference failed for: r36v32 */
    /* JADX WARN: Type inference failed for: r36v34 */
    /* JADX WARN: Type inference failed for: r36v37 */
    /* JADX WARN: Type inference failed for: r0v69, types: [java.util.Date] */
    /* JADX WARN: Type inference failed for: r0v70, types: [java.text.DateFormat] */
    /* JADX WARN: Type inference failed for: r1v66, types: [java.util.Date] */
    /* JADX WARN: Type inference failed for: r0v96, types: [java.util.Date] */
    /* JADX WARN: Type inference failed for: r0v97, types: [java.text.DateFormat] */
    /* JADX WARN: Type inference failed for: r1v91, types: [java.util.Date] */
    /* JADX WARN: Type inference failed for: r6v103, types: [java.util.Date] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x02fd  */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x0306  */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:79:0x0290=Splitter:B:79:0x0290, B:222:0x0722=Splitter:B:222:0x0722} */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static com.riteshsahu.SMSBackupRestoreBase.OperationResult saveXmlIncremental(android.content.Context r36, com.riteshsahu.SMSBackupRestoreBase.BackupFile r37, java.lang.Boolean r38, android.app.ProgressDialog r39, android.os.Handler r40) throws com.riteshsahu.SMSBackupRestoreBase.CustomException {
        /*
            r15 = -1
            r22 = -1
            r16 = 0
            r17 = 0
            r14 = 0
            java.util.HashMap r5 = new java.util.HashMap
            r5.<init>()
            com.riteshsahu.SMSBackupRestoreBase.SmsBackupProcessor.mContacts = r5
            com.riteshsahu.SMSBackupRestoreBase.OperationResult r24 = new com.riteshsahu.SMSBackupRestoreBase.OperationResult
            r24.<init>()
            r18 = 0
            r26 = 0
            r28 = 0
            boolean r5 = r38.booleanValue()     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
            if (r5 == 0) goto L_0x0023
            com.riteshsahu.SMSBackupRestoreBase.WakeLocker.acquireLock(r36)     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
        L_0x0023:
            waitForStorageToBeReady(r39, r40)     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
            r5 = 2131230812(0x7f08005c, float:1.8077687E38)
            r6 = 0
            r0 = r39
            r1 = r40
            r2 = r5
            r3 = r6
            com.riteshsahu.SMSBackupRestoreBase.Common.resetProgressHandler(r0, r1, r2, r3)     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
            com.riteshsahu.SMSBackupRestoreBase.Common.setupContactNameSettings()     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
            java.lang.String r6 = com.riteshsahu.SMSBackupRestoreBase.Common.getBackupFilePath(r36)     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
            java.lang.String r6 = java.lang.String.valueOf(r6)     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
            r5.<init>(r6)     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
            java.lang.String r6 = "tempbackup.xml"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
            java.lang.String r27 = r5.toString()     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
            java.lang.String r13 = r37.getFullPath()     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
            java.lang.String r5 = com.riteshsahu.SMSBackupRestoreBase.PreferenceKeys.AddReadableDate     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
            r0 = r36
            r1 = r5
            java.lang.Boolean r12 = com.riteshsahu.SMSBackupRestoreBase.Common.getBooleanPreference(r0, r1)     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
            java.lang.String r5 = com.riteshsahu.SMSBackupRestoreBase.PreferenceKeys.AddContactNames     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
            r0 = r36
            r1 = r5
            java.lang.Boolean r11 = com.riteshsahu.SMSBackupRestoreBase.Common.getBooleanPreference(r0, r1)     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
            java.lang.String r5 = com.riteshsahu.SMSBackupRestoreBase.PreferenceKeys.LastBackupMessageId     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
            r0 = r36
            r1 = r5
            long r5 = com.riteshsahu.SMSBackupRestoreBase.Common.getLongPreference(r0, r1)     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
            java.lang.Long r6 = java.lang.Long.valueOf(r5)     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
            java.lang.String r5 = com.riteshsahu.SMSBackupRestoreBase.PreferenceKeys.LastBackupMessageDate     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
            r0 = r36
            r1 = r5
            long r7 = com.riteshsahu.SMSBackupRestoreBase.Common.getLongPreference(r0, r1)     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
            java.lang.Long r5 = java.lang.Long.valueOf(r7)     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
            r19 = 0
            boolean r23 = com.riteshsahu.SMSBackupRestoreBase.Common.backupExists(r37)     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
            if (r23 != 0) goto L_0x0c1f
            r5 = -1
            java.lang.Long r5 = java.lang.Long.valueOf(r5)     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
            r6 = -1
            java.lang.Long r37 = java.lang.Long.valueOf(r6)     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
            r20 = r37
            r21 = r5
        L_0x0095:
            java.io.File r37 = new java.io.File     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
            java.lang.String r5 = com.riteshsahu.SMSBackupRestoreBase.Common.getBackupFilePath(r36)     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
            r0 = r37
            r1 = r5
            r0.<init>(r1)     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
            boolean r5 = r37.exists()     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
            if (r5 != 0) goto L_0x00aa
            r37.mkdir()     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
        L_0x00aa:
            boolean r37 = r37.canWrite()     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
            if (r37 == 0) goto L_0x06bc
            java.lang.StringBuilder r37 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
            java.lang.String r6 = "( _id > "
            r5.<init>(r6)     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
            r0 = r5
            r1 = r21
            java.lang.StringBuilder r5 = r0.append(r1)     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
            java.lang.String r5 = r5.toString()     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
            r0 = r37
            r1 = r5
            r0.<init>(r1)     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
            long r5 = r20.longValue()     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
            r7 = 0
            int r5 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
            if (r5 == 0) goto L_0x00ec
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
            java.lang.String r6 = " OR date > "
            r5.<init>(r6)     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
            r0 = r5
            r1 = r20
            java.lang.StringBuilder r5 = r0.append(r1)     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
            java.lang.String r5 = r5.toString()     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
            r0 = r37
            r1 = r5
            r0.append(r1)     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
        L_0x00ec:
            java.lang.String r5 = " )"
            r0 = r37
            r1 = r5
            r0.append(r1)     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
            java.lang.String r5 = com.riteshsahu.SMSBackupRestoreBase.ConversationProcessor.getConversationFilter(r36)     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
            if (r5 == 0) goto L_0x0115
            int r6 = r5.length()     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
            if (r6 <= 0) goto L_0x0115
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
            java.lang.String r7 = " AND "
            r6.<init>(r7)     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
            java.lang.StringBuilder r5 = r6.append(r5)     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
            java.lang.String r5 = r5.toString()     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
            r0 = r37
            r1 = r5
            r0.append(r1)     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
        L_0x0115:
            android.content.ContentResolver r5 = r36.getContentResolver()     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
            android.net.Uri r6 = com.riteshsahu.SMSBackupRestoreBase.Common.SmsContentUri     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
            r7 = 0
            java.lang.String r8 = r37.toString()     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
            r9 = 0
            java.lang.String r10 = "date ASC"
            android.database.Cursor r9 = r5.query(r6, r7, r8, r9, r10)     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
            r37 = 0
            if (r9 == 0) goto L_0x068d
            int r5 = r9.getCount()     // Catch:{ FileNotFoundException -> 0x06a7, IOException -> 0x0aa4, CustomException -> 0x09ed, Exception -> 0x0936, all -> 0x086e }
            if (r5 <= 0) goto L_0x063f
            boolean r5 = r12.booleanValue()     // Catch:{ FileNotFoundException -> 0x06a7, IOException -> 0x0aa4, CustomException -> 0x09ed, Exception -> 0x0936, all -> 0x086e }
            if (r5 == 0) goto L_0x0c19
            java.text.DateFormat r5 = getDateFormatToUse(r36)     // Catch:{ FileNotFoundException -> 0x06a7, IOException -> 0x0aa4, CustomException -> 0x09ed, Exception -> 0x0936, all -> 0x086e }
            java.util.Date r6 = new java.util.Date     // Catch:{ FileNotFoundException -> 0x0b5b, IOException -> 0x0ab9, CustomException -> 0x0a02, Exception -> 0x094b, all -> 0x0885 }
            r6.<init>()     // Catch:{ FileNotFoundException -> 0x0b5b, IOException -> 0x0ab9, CustomException -> 0x0a02, Exception -> 0x094b, all -> 0x0885 }
            r16 = r6
            r14 = r5
        L_0x0143:
            java.io.FileWriter r29 = new java.io.FileWriter     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            r0 = r29
            r1 = r27
            r0.<init>(r1)     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            com.riteshsahu.SMSBackupRestoreBase.KXmlSerializer r25 = new com.riteshsahu.SMSBackupRestoreBase.KXmlSerializer     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            r25.<init>()     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            java.lang.String r5 = "http://xmlpull.org/v1/doc/features.html#indent-output"
            r6 = 1
            r0 = r25
            r1 = r5
            r2 = r6
            r0.setFeature(r1, r2)     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            r0 = r25
            r1 = r29
            r0.setOutput(r1)     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            java.lang.String r5 = "UTF-8"
            r6 = 1
            java.lang.Boolean r6 = java.lang.Boolean.valueOf(r6)     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            r0 = r25
            r1 = r5
            r2 = r6
            r0.startDocument(r1, r2)     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            java.lang.String r5 = com.riteshsahu.SMSBackupRestoreBase.PreferenceKeys.AddXslTag     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            r0 = r36
            r1 = r5
            java.lang.Boolean r5 = com.riteshsahu.SMSBackupRestoreBase.Common.getBooleanPreference(r0, r1)     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            boolean r5 = r5.booleanValue()     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            if (r5 == 0) goto L_0x0187
            java.lang.String r5 = "xml-stylesheet type=\"text/xsl\" href=\"sms.xsl\""
            r0 = r25
            r1 = r5
            r0.processingInstruction(r1)     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
        L_0x0187:
            java.lang.String r5 = ""
            java.lang.String r6 = "smses"
            r0 = r25
            r1 = r5
            r2 = r6
            r0.startTag(r1, r2)     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            r6 = 0
            if (r23 == 0) goto L_0x0c11
            if (r40 == 0) goto L_0x01a0
            r5 = 2131230840(0x7f080078, float:1.8077744E38)
            r0 = r40
            r1 = r5
            r0.sendEmptyMessage(r1)     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
        L_0x01a0:
            java.io.FileInputStream r10 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            r10.<init>(r13)     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            com.riteshsahu.SMSBackupRestoreBase.KXmlParser r23 = new com.riteshsahu.SMSBackupRestoreBase.KXmlParser     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            r23.<init>()     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            r5 = 0
            r0 = r23
            r1 = r10
            r2 = r5
            r0.setInput(r1, r2)     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            int r5 = r23.getEventType()     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            r19 = r6
        L_0x01b8:
            r6 = 1
            if (r5 != r6) goto L_0x030f
            r23 = r19
            r6 = r37
            r17 = r10
        L_0x01c1:
            if (r6 != 0) goto L_0x01d8
            java.lang.String r37 = ""
            java.lang.String r5 = "count"
            int r7 = r9.getCount()     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            java.lang.String r7 = java.lang.Integer.toString(r7)     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            r0 = r25
            r1 = r37
            r2 = r5
            r3 = r7
            r0.attribute(r1, r2, r3)     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
        L_0x01d8:
            r37 = 2131230812(0x7f08005c, float:1.8077687E38)
            int r5 = r9.getCount()     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            r0 = r39
            r1 = r40
            r2 = r37
            r3 = r5
            com.riteshsahu.SMSBackupRestoreBase.Common.resetProgressHandler(r0, r1, r2, r3)     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            java.lang.String r37 = "date"
            r0 = r9
            r1 = r37
            int r10 = r0.getColumnIndex(r1)     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            boolean r37 = r12.booleanValue()     // Catch:{ FileNotFoundException -> 0x0b85, IOException -> 0x0ae3, CustomException -> 0x0a2c, Exception -> 0x0975, all -> 0x08b3 }
            if (r37 == 0) goto L_0x0bff
            if (r10 >= 0) goto L_0x0bff
            r37 = 0
            java.lang.Boolean r37 = java.lang.Boolean.valueOf(r37)     // Catch:{ FileNotFoundException -> 0x0b85, IOException -> 0x0ae3, CustomException -> 0x0a2c, Exception -> 0x0975, all -> 0x08b3 }
            r5 = r37
        L_0x0202:
            boolean r37 = r11.booleanValue()     // Catch:{ FileNotFoundException -> 0x0b85, IOException -> 0x0ae3, CustomException -> 0x0a2c, Exception -> 0x0975, all -> 0x08b3 }
            if (r37 == 0) goto L_0x0bf9
            java.lang.String r37 = "address"
            r0 = r9
            r1 = r37
            int r7 = r0.getColumnIndex(r1)     // Catch:{ FileNotFoundException -> 0x0b85, IOException -> 0x0ae3, CustomException -> 0x0a2c, Exception -> 0x0975, all -> 0x08b3 }
            if (r7 >= 0) goto L_0x0bf3
            r37 = 0
            java.lang.Boolean r37 = java.lang.Boolean.valueOf(r37)     // Catch:{ FileNotFoundException -> 0x0b9a, IOException -> 0x0af8, CustomException -> 0x0a41, Exception -> 0x098a, all -> 0x08ca }
            r19 = r7
        L_0x021b:
            java.lang.String r7 = "_id"
            int r12 = r9.getColumnIndex(r7)     // Catch:{ FileNotFoundException -> 0x0bae, IOException -> 0x0b0c, CustomException -> 0x0a55, Exception -> 0x099e, all -> 0x08e0 }
            r15 = r20
            r11 = r18
            r20 = r26
            r18 = r21
            r21 = r28
        L_0x022b:
            boolean r7 = r9.moveToNext()     // Catch:{ FileNotFoundException -> 0x0bc3, IOException -> 0x0b21, CustomException -> 0x0a6a, Exception -> 0x09b3, all -> 0x08f7 }
            if (r7 != 0) goto L_0x04c6
            java.lang.String r7 = ""
            java.lang.String r8 = "smses"
            r0 = r25
            r1 = r7
            r2 = r8
            r0.endTag(r1, r2)     // Catch:{ FileNotFoundException -> 0x0bc3, IOException -> 0x0b21, CustomException -> 0x0a6a, Exception -> 0x09b3, all -> 0x08f7 }
            r25.endDocument()     // Catch:{ FileNotFoundException -> 0x0bc3, IOException -> 0x0b21, CustomException -> 0x0a6a, Exception -> 0x09b3, all -> 0x08f7 }
            r25.flush()     // Catch:{ FileNotFoundException -> 0x0bc3, IOException -> 0x0b21, CustomException -> 0x0a6a, Exception -> 0x09b3, all -> 0x08f7 }
            r29.close()     // Catch:{ FileNotFoundException -> 0x0bc3, IOException -> 0x0b21, CustomException -> 0x0a6a, Exception -> 0x09b3, all -> 0x08f7 }
            r9.close()     // Catch:{ FileNotFoundException -> 0x0bc3, IOException -> 0x0b21, CustomException -> 0x0a6a, Exception -> 0x09b3, all -> 0x08f7 }
            r7 = 0
            java.lang.String r8 = com.riteshsahu.SMSBackupRestoreBase.PreferenceKeys.DisableVerification     // Catch:{ FileNotFoundException -> 0x027e, IOException -> 0x0b47, CustomException -> 0x0a90, Exception -> 0x09d9, all -> 0x0922 }
            r0 = r36
            r1 = r8
            java.lang.Boolean r8 = com.riteshsahu.SMSBackupRestoreBase.Common.getBooleanPreference(r0, r1)     // Catch:{ FileNotFoundException -> 0x027e, IOException -> 0x0b47, CustomException -> 0x0a90, Exception -> 0x09d9, all -> 0x0922 }
            boolean r8 = r8.booleanValue()     // Catch:{ FileNotFoundException -> 0x027e, IOException -> 0x0b47, CustomException -> 0x0a90, Exception -> 0x09d9, all -> 0x0922 }
            if (r8 != 0) goto L_0x058d
            int r8 = r21 + r23
            r0 = r36
            r1 = r27
            r2 = r8
            r3 = r39
            r4 = r40
            boolean r39 = verifyXml(r0, r1, r2, r3, r4)     // Catch:{ FileNotFoundException -> 0x027e, IOException -> 0x0b47, CustomException -> 0x0a90, Exception -> 0x09d9, all -> 0x0922 }
            if (r39 != 0) goto L_0x058d
            com.riteshsahu.SMSBackupRestoreBase.CustomException r37 = new com.riteshsahu.SMSBackupRestoreBase.CustomException     // Catch:{ FileNotFoundException -> 0x027e, IOException -> 0x0b47, CustomException -> 0x0a90, Exception -> 0x09d9, all -> 0x0922 }
            r39 = 2131230888(0x7f0800a8, float:1.8077841E38)
            r0 = r36
            r1 = r39
            java.lang.String r39 = r0.getString(r1)     // Catch:{ FileNotFoundException -> 0x027e, IOException -> 0x0b47, CustomException -> 0x0a90, Exception -> 0x09d9, all -> 0x0922 }
            r0 = r37
            r1 = r39
            r0.<init>(r1)     // Catch:{ FileNotFoundException -> 0x027e, IOException -> 0x0b47, CustomException -> 0x0a90, Exception -> 0x09d9, all -> 0x0922 }
            throw r37     // Catch:{ FileNotFoundException -> 0x027e, IOException -> 0x0b47, CustomException -> 0x0a90, Exception -> 0x09d9, all -> 0x0922 }
        L_0x027e:
            r37 = move-exception
            r6 = r37
            r9 = r20
            r5 = r16
            r40 = r14
            r8 = r19
            r39 = r10
            r37 = r7
            r10 = r21
            r7 = r11
        L_0x0290:
            r6.printStackTrace()     // Catch:{ all -> 0x02e9 }
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ all -> 0x02e9 }
            java.lang.String r12 = "App version: "
            r11.<init>(r12)     // Catch:{ all -> 0x02e9 }
            r12 = 2131230721(0x7f080001, float:1.8077503E38)
            r0 = r36
            r1 = r12
            java.lang.String r12 = r0.getString(r1)     // Catch:{ all -> 0x02e9 }
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ all -> 0x02e9 }
            java.lang.String r11 = r11.toString()     // Catch:{ all -> 0x02e9 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r11)     // Catch:{ all -> 0x02e9 }
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ all -> 0x02e9 }
            java.lang.String r12 = "File Not found: "
            r11.<init>(r12)     // Catch:{ all -> 0x02e9 }
            java.lang.String r12 = r6.getMessage()     // Catch:{ all -> 0x02e9 }
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ all -> 0x02e9 }
            java.lang.String r11 = r11.toString()     // Catch:{ all -> 0x02e9 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r11)     // Catch:{ all -> 0x02e9 }
            com.riteshsahu.SMSBackupRestoreBase.CustomException r11 = new com.riteshsahu.SMSBackupRestoreBase.CustomException     // Catch:{ all -> 0x02e9 }
            r12 = 2131230730(0x7f08000a, float:1.8077521E38)
            r0 = r36
            r1 = r12
            java.lang.String r36 = r0.getString(r1)     // Catch:{ all -> 0x02e9 }
            r12 = 1
            java.lang.Object[] r12 = new java.lang.Object[r12]     // Catch:{ all -> 0x02e9 }
            r13 = 0
            java.lang.String r6 = r6.getMessage()     // Catch:{ all -> 0x02e9 }
            r12[r13] = r6     // Catch:{ all -> 0x02e9 }
            r0 = r36
            r1 = r12
            java.lang.String r36 = java.lang.String.format(r0, r1)     // Catch:{ all -> 0x02e9 }
            r0 = r11
            r1 = r36
            r0.<init>(r1)     // Catch:{ all -> 0x02e9 }
            throw r11     // Catch:{ all -> 0x02e9 }
        L_0x02e9:
            r36 = move-exception
            r6 = r8
            r8 = r10
            r35 = r9
            r9 = r36
            r36 = r37
            r37 = r39
            r39 = r40
            r40 = r5
            r5 = r7
            r7 = r35
        L_0x02fb:
            if (r36 == 0) goto L_0x0300
            r36.close()
        L_0x0300:
            boolean r36 = r38.booleanValue()
            if (r36 == 0) goto L_0x0309
            com.riteshsahu.SMSBackupRestoreBase.WakeLocker.releaseLock()
        L_0x0309:
            java.util.HashMap<java.lang.String, java.lang.String> r36 = com.riteshsahu.SMSBackupRestoreBase.SmsBackupProcessor.mContacts
            r36.clear()
            throw r9
        L_0x030f:
            r6 = 0
            switch(r5) {
                case 2: goto L_0x031e;
                default: goto L_0x0313;
            }
        L_0x0313:
            r5 = r6
            r6 = r19
        L_0x0316:
            int r5 = r23.next()     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            r19 = r6
            goto L_0x01b8
        L_0x031e:
            java.lang.String r5 = ""
            r6 = 0
            java.lang.Long r6 = java.lang.Long.valueOf(r6)     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            java.lang.String r17 = r23.getName()     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            java.lang.String r7 = "sms"
            r0 = r17
            r1 = r7
            boolean r7 = r0.equalsIgnoreCase(r1)     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            if (r7 == 0) goto L_0x0433
            java.lang.String r7 = ""
            java.lang.String r8 = "sms"
            r0 = r25
            r1 = r7
            r2 = r8
            r0.startTag(r1, r2)     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            java.lang.String[] r30 = com.riteshsahu.SMSBackupRestoreBase.Common.ColumnNames     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            r0 = r30
            int r0 = r0.length     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            r31 = r0
            r7 = 0
            r32 = r7
            r8 = r6
            r7 = r5
        L_0x034c:
            r0 = r32
            r1 = r31
            if (r0 < r1) goto L_0x03d7
            boolean r5 = r12.booleanValue()     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            if (r5 == 0) goto L_0x038e
            java.lang.String r5 = "readable_date"
            r0 = r23
            r1 = r5
            java.lang.String r5 = getAttributeValue(r0, r1)     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            int r6 = r5.length()     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            if (r6 != 0) goto L_0x0382
            long r30 = r8.longValue()     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            r32 = 0
            int r6 = (r30 > r32 ? 1 : (r30 == r32 ? 0 : -1))
            if (r6 == 0) goto L_0x0382
            long r5 = r8.longValue()     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            r0 = r16
            r1 = r5
            r0.setTime(r1)     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            r0 = r14
            r1 = r16
            java.lang.String r5 = r0.format(r1)     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
        L_0x0382:
            java.lang.String r6 = ""
            java.lang.String r8 = "readable_date"
            r0 = r25
            r1 = r6
            r2 = r8
            r3 = r5
            r0.attribute(r1, r2, r3)     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
        L_0x038e:
            boolean r5 = r11.booleanValue()     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            if (r5 == 0) goto L_0x03bc
            java.lang.String r5 = "contact_name"
            r0 = r23
            r1 = r5
            java.lang.String r5 = getAttributeValue(r0, r1)     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            int r6 = r5.length()     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            if (r6 != 0) goto L_0x03b0
            int r6 = r7.length()     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            if (r6 <= 0) goto L_0x03b0
            r0 = r36
            r1 = r7
            java.lang.String r5 = getContactName(r0, r1)     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
        L_0x03b0:
            java.lang.String r6 = ""
            java.lang.String r7 = "contact_name"
            r0 = r25
            r1 = r6
            r2 = r7
            r3 = r5
            r0.attribute(r1, r2, r3)     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
        L_0x03bc:
            int r5 = r19 + 1
            java.lang.String r6 = ""
            java.lang.String r7 = "sms"
            r0 = r25
            r1 = r6
            r2 = r7
            r0.endTag(r1, r2)     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            if (r39 == 0) goto L_0x0c08
            r6 = 1
            r0 = r39
            r1 = r6
            r0.incrementProgressBy(r1)     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            r6 = r5
            r5 = r17
            goto L_0x0316
        L_0x03d7:
            r5 = r30[r32]     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            r0 = r23
            r1 = r5
            java.lang.String r6 = getAttributeValue(r0, r1)     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            java.lang.String r33 = ""
            r0 = r25
            r1 = r33
            r2 = r5
            r3 = r6
            r0.attribute(r1, r2, r3)     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            java.lang.String r33 = "date"
            r0 = r5
            r1 = r33
            if (r0 != r1) goto L_0x0429
            int r33 = r6.length()     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            if (r33 <= 0) goto L_0x0429
            long r33 = java.lang.Long.parseLong(r6)     // Catch:{ NumberFormatException -> 0x040a }
            java.lang.Long r5 = java.lang.Long.valueOf(r33)     // Catch:{ NumberFormatException -> 0x040a }
            r6 = r5
            r5 = r7
        L_0x0402:
            int r7 = r32 + 1
            r32 = r7
            r8 = r6
            r7 = r5
            goto L_0x034c
        L_0x040a:
            r5 = move-exception
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            java.lang.String r33 = "String "
            r0 = r5
            r1 = r33
            r0.<init>(r1)     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            java.lang.String r6 = " could not be parsed to Long"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            java.lang.String r5 = r5.toString()     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            com.riteshsahu.SMSBackupRestoreBase.Common.logDebug(r5)     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            r6 = r8
            r5 = r7
            goto L_0x0402
        L_0x0429:
            java.lang.String r33 = "address"
            r0 = r5
            r1 = r33
            if (r0 != r1) goto L_0x0c0d
            r5 = r6
            r6 = r8
            goto L_0x0402
        L_0x0433:
            java.lang.String r5 = "smses"
            r0 = r17
            r1 = r5
            boolean r5 = r0.equalsIgnoreCase(r1)     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            if (r5 == 0) goto L_0x0c02
            java.lang.String r37 = "Trying to load message count in file..."
            com.riteshsahu.SMSBackupRestoreBase.Common.logDebug(r37)     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            java.lang.String r37 = ""
            java.lang.String r5 = "count"
            r0 = r23
            r1 = r37
            r2 = r5
            java.lang.String r37 = r0.getAttributeValue(r1, r2)     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            r6 = 0
            if (r37 == 0) goto L_0x04b3
            java.lang.String r5 = ""
            r0 = r37
            r1 = r5
            if (r0 == r1) goto L_0x04b3
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            java.lang.String r7 = "Found count string: "
            r5.<init>(r7)     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            r0 = r5
            r1 = r37
            java.lang.StringBuilder r5 = r0.append(r1)     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            java.lang.String r5 = r5.toString()     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            com.riteshsahu.SMSBackupRestoreBase.Common.logDebug(r5)     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            int r37 = java.lang.Integer.parseInt(r37)     // Catch:{ NumberFormatException -> 0x049a }
        L_0x0473:
            java.lang.String r5 = ""
            java.lang.String r6 = "count"
            int r7 = r9.getCount()     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            int r7 = r7 + r37
            java.lang.String r7 = java.lang.Integer.toString(r7)     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            r0 = r25
            r1 = r5
            r2 = r6
            r3 = r7
            r0.attribute(r1, r2, r3)     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            if (r39 == 0) goto L_0x0492
            r0 = r39
            r1 = r37
            r0.setMax(r1)     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
        L_0x0492:
            r37 = 1
            r5 = r17
            r6 = r19
            goto L_0x0316
        L_0x049a:
            r5 = move-exception
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            java.lang.String r7 = "Could not parse count to an integer: "
            r5.<init>(r7)     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            r0 = r5
            r1 = r37
            java.lang.StringBuilder r37 = r0.append(r1)     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            java.lang.String r37 = r37.toString()     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            com.riteshsahu.SMSBackupRestoreBase.Common.logDebug(r37)     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            r37 = r6
            goto L_0x0473
        L_0x04b3:
            java.lang.String r37 = "Count not found."
            com.riteshsahu.SMSBackupRestoreBase.Common.logDebug(r37)     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
            if (r39 == 0) goto L_0x04c3
            r37 = 10000(0x2710, float:1.4013E-41)
            r0 = r39
            r1 = r37
            r0.setMax(r1)     // Catch:{ FileNotFoundException -> 0x0b70, IOException -> 0x0ace, CustomException -> 0x0a17, Exception -> 0x0960, all -> 0x089c }
        L_0x04c3:
            r37 = r6
            goto L_0x0473
        L_0x04c6:
            long r7 = r9.getLong(r12)     // Catch:{ FileNotFoundException -> 0x0bc3, IOException -> 0x0b21, CustomException -> 0x0a6a, Exception -> 0x09b3, all -> 0x08f7 }
            long r30 = r18.longValue()     // Catch:{ FileNotFoundException -> 0x0bc3, IOException -> 0x0b21, CustomException -> 0x0a6a, Exception -> 0x09b3, all -> 0x08f7 }
            int r22 = (r30 > r7 ? 1 : (r30 == r7 ? 0 : -1))
            if (r22 >= 0) goto L_0x04d8
            java.lang.Long r7 = java.lang.Long.valueOf(r7)     // Catch:{ FileNotFoundException -> 0x0bc3, IOException -> 0x0b21, CustomException -> 0x0a6a, Exception -> 0x09b3, all -> 0x08f7 }
            r18 = r7
        L_0x04d8:
            long r7 = r9.getLong(r10)     // Catch:{ FileNotFoundException -> 0x0bc3, IOException -> 0x0b21, CustomException -> 0x0a6a, Exception -> 0x09b3, all -> 0x08f7 }
            long r30 = r15.longValue()     // Catch:{ FileNotFoundException -> 0x0bc3, IOException -> 0x0b21, CustomException -> 0x0a6a, Exception -> 0x09b3, all -> 0x08f7 }
            int r22 = (r30 > r7 ? 1 : (r30 == r7 ? 0 : -1))
            if (r22 >= 0) goto L_0x04e9
            java.lang.Long r7 = java.lang.Long.valueOf(r7)     // Catch:{ FileNotFoundException -> 0x0bc3, IOException -> 0x0b21, CustomException -> 0x0a6a, Exception -> 0x09b3, all -> 0x08f7 }
            r15 = r7
        L_0x04e9:
            int r21 = r21 + 1
            r7 = 0
            java.lang.String r8 = ""
            java.lang.String r22 = "sms"
            r0 = r25
            r1 = r8
            r2 = r22
            r0.startTag(r1, r2)     // Catch:{ FileNotFoundException -> 0x0bc3, IOException -> 0x0b21, CustomException -> 0x0a6a, Exception -> 0x09b3, all -> 0x08f7 }
            java.lang.String[] r22 = com.riteshsahu.SMSBackupRestoreBase.Common.ColumnNames     // Catch:{ FileNotFoundException -> 0x0bc3, IOException -> 0x0b21, CustomException -> 0x0a6a, Exception -> 0x09b3, all -> 0x08f7 }
            r0 = r22
            int r0 = r0.length     // Catch:{ FileNotFoundException -> 0x0bc3, IOException -> 0x0b21, CustomException -> 0x0a6a, Exception -> 0x09b3, all -> 0x08f7 }
            r26 = r0
            r8 = 0
            r28 = r8
            r8 = r7
        L_0x0503:
            r0 = r28
            r1 = r26
            if (r0 < r1) goto L_0x0572
            if (r8 == 0) goto L_0x0588
            int r7 = r11 + 1
            r8 = r20
        L_0x050f:
            boolean r11 = r5.booleanValue()     // Catch:{ FileNotFoundException -> 0x0bd7, IOException -> 0x0b35, CustomException -> 0x0a7e, Exception -> 0x09c7, all -> 0x090d }
            if (r11 == 0) goto L_0x0535
            long r30 = r9.getLong(r10)     // Catch:{ FileNotFoundException -> 0x0bd7, IOException -> 0x0b35, CustomException -> 0x0a7e, Exception -> 0x09c7, all -> 0x090d }
            r0 = r16
            r1 = r30
            r0.setTime(r1)     // Catch:{ FileNotFoundException -> 0x0bd7, IOException -> 0x0b35, CustomException -> 0x0a7e, Exception -> 0x09c7, all -> 0x090d }
            java.lang.String r11 = ""
            java.lang.String r20 = "readable_date"
            r0 = r14
            r1 = r16
            java.lang.String r22 = r0.format(r1)     // Catch:{ FileNotFoundException -> 0x0bd7, IOException -> 0x0b35, CustomException -> 0x0a7e, Exception -> 0x09c7, all -> 0x090d }
            r0 = r25
            r1 = r11
            r2 = r20
            r3 = r22
            r0.attribute(r1, r2, r3)     // Catch:{ FileNotFoundException -> 0x0bd7, IOException -> 0x0b35, CustomException -> 0x0a7e, Exception -> 0x09c7, all -> 0x090d }
        L_0x0535:
            boolean r11 = r37.booleanValue()     // Catch:{ FileNotFoundException -> 0x0bd7, IOException -> 0x0b35, CustomException -> 0x0a7e, Exception -> 0x09c7, all -> 0x090d }
            if (r11 == 0) goto L_0x0558
            java.lang.String r11 = ""
            java.lang.String r20 = "contact_name"
            r0 = r9
            r1 = r19
            java.lang.String r22 = r0.getString(r1)     // Catch:{ FileNotFoundException -> 0x0bd7, IOException -> 0x0b35, CustomException -> 0x0a7e, Exception -> 0x09c7, all -> 0x090d }
            r0 = r36
            r1 = r22
            java.lang.String r22 = getContactName(r0, r1)     // Catch:{ FileNotFoundException -> 0x0bd7, IOException -> 0x0b35, CustomException -> 0x0a7e, Exception -> 0x09c7, all -> 0x090d }
            r0 = r25
            r1 = r11
            r2 = r20
            r3 = r22
            r0.attribute(r1, r2, r3)     // Catch:{ FileNotFoundException -> 0x0bd7, IOException -> 0x0b35, CustomException -> 0x0a7e, Exception -> 0x09c7, all -> 0x090d }
        L_0x0558:
            java.lang.String r11 = ""
            java.lang.String r20 = "sms"
            r0 = r25
            r1 = r11
            r2 = r20
            r0.endTag(r1, r2)     // Catch:{ FileNotFoundException -> 0x0bd7, IOException -> 0x0b35, CustomException -> 0x0a7e, Exception -> 0x09c7, all -> 0x090d }
            if (r39 == 0) goto L_0x056d
            r11 = 1
            r0 = r39
            r1 = r11
            r0.incrementProgressBy(r1)     // Catch:{ FileNotFoundException -> 0x0bd7, IOException -> 0x0b35, CustomException -> 0x0a7e, Exception -> 0x09c7, all -> 0x090d }
        L_0x056d:
            r20 = r8
            r11 = r7
            goto L_0x022b
        L_0x0572:
            r7 = r22[r28]     // Catch:{ FileNotFoundException -> 0x0bc3, IOException -> 0x0b21, CustomException -> 0x0a6a, Exception -> 0x09b3, all -> 0x08f7 }
            r0 = r25
            r1 = r7
            r2 = r9
            boolean r7 = setSerializerAttribute(r0, r1, r2)     // Catch:{ FileNotFoundException -> 0x0bc3, IOException -> 0x0b21, CustomException -> 0x0a6a, Exception -> 0x09b3, all -> 0x08f7 }
            if (r7 != 0) goto L_0x0bf0
            if (r8 != 0) goto L_0x0bed
            r7 = 1
        L_0x0581:
            int r8 = r28 + 1
            r28 = r8
            r8 = r7
            goto L_0x0503
        L_0x0588:
            int r7 = r20 + 1
            r8 = r7
            r7 = r11
            goto L_0x050f
        L_0x058d:
            java.lang.String r39 = "Preparing to copy the new backup on old..."
            com.riteshsahu.SMSBackupRestoreBase.Common.logDebug(r39)     // Catch:{ FileNotFoundException -> 0x027e, IOException -> 0x0b47, CustomException -> 0x0a90, Exception -> 0x09d9, all -> 0x0922 }
            java.io.File r40 = new java.io.File     // Catch:{ FileNotFoundException -> 0x027e, IOException -> 0x0b47, CustomException -> 0x0a90, Exception -> 0x09d9, all -> 0x0922 }
            r0 = r40
            r1 = r13
            r0.<init>(r1)     // Catch:{ FileNotFoundException -> 0x027e, IOException -> 0x0b47, CustomException -> 0x0a90, Exception -> 0x09d9, all -> 0x0922 }
            java.io.File r8 = new java.io.File     // Catch:{ FileNotFoundException -> 0x027e, IOException -> 0x0b47, CustomException -> 0x0a90, Exception -> 0x09d9, all -> 0x0922 }
            java.lang.StringBuilder r39 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x027e, IOException -> 0x0b47, CustomException -> 0x0a90, Exception -> 0x09d9, all -> 0x0922 }
            java.lang.String r9 = java.lang.String.valueOf(r13)     // Catch:{ FileNotFoundException -> 0x027e, IOException -> 0x0b47, CustomException -> 0x0a90, Exception -> 0x09d9, all -> 0x0922 }
            r0 = r39
            r1 = r9
            r0.<init>(r1)     // Catch:{ FileNotFoundException -> 0x027e, IOException -> 0x0b47, CustomException -> 0x0a90, Exception -> 0x09d9, all -> 0x0922 }
            java.lang.String r9 = ".old"
            r0 = r39
            r1 = r9
            java.lang.StringBuilder r39 = r0.append(r1)     // Catch:{ FileNotFoundException -> 0x027e, IOException -> 0x0b47, CustomException -> 0x0a90, Exception -> 0x09d9, all -> 0x0922 }
            java.lang.String r39 = r39.toString()     // Catch:{ FileNotFoundException -> 0x027e, IOException -> 0x0b47, CustomException -> 0x0a90, Exception -> 0x09d9, all -> 0x0922 }
            r0 = r8
            r1 = r39
            r0.<init>(r1)     // Catch:{ FileNotFoundException -> 0x027e, IOException -> 0x0b47, CustomException -> 0x0a90, Exception -> 0x09d9, all -> 0x0922 }
            boolean r39 = r40.exists()     // Catch:{ FileNotFoundException -> 0x027e, IOException -> 0x0b47, CustomException -> 0x0a90, Exception -> 0x09d9, all -> 0x0922 }
            if (r39 == 0) goto L_0x05d0
            boolean r39 = r8.exists()     // Catch:{ FileNotFoundException -> 0x027e, IOException -> 0x0b47, CustomException -> 0x0a90, Exception -> 0x09d9, all -> 0x0922 }
            if (r39 == 0) goto L_0x05ca
            r8.delete()     // Catch:{ FileNotFoundException -> 0x027e, IOException -> 0x0b47, CustomException -> 0x0a90, Exception -> 0x09d9, all -> 0x0922 }
        L_0x05ca:
            r0 = r40
            r1 = r8
            r0.renameTo(r1)     // Catch:{ FileNotFoundException -> 0x027e, IOException -> 0x0b47, CustomException -> 0x0a90, Exception -> 0x09d9, all -> 0x0922 }
        L_0x05d0:
            java.io.File r39 = new java.io.File     // Catch:{ FileNotFoundException -> 0x027e, IOException -> 0x0b47, CustomException -> 0x0a90, Exception -> 0x09d9, all -> 0x0922 }
            r0 = r39
            r1 = r27
            r0.<init>(r1)     // Catch:{ FileNotFoundException -> 0x027e, IOException -> 0x0b47, CustomException -> 0x0a90, Exception -> 0x09d9, all -> 0x0922 }
            boolean r9 = r39.exists()     // Catch:{ FileNotFoundException -> 0x027e, IOException -> 0x0b47, CustomException -> 0x0a90, Exception -> 0x09d9, all -> 0x0922 }
            if (r9 == 0) goto L_0x05e5
            r39.renameTo(r40)     // Catch:{ FileNotFoundException -> 0x027e, IOException -> 0x0b47, CustomException -> 0x0a90, Exception -> 0x09d9, all -> 0x0922 }
            r8.delete()     // Catch:{ FileNotFoundException -> 0x027e, IOException -> 0x0b47, CustomException -> 0x0a90, Exception -> 0x09d9, all -> 0x0922 }
        L_0x05e5:
            java.lang.String r39 = com.riteshsahu.SMSBackupRestoreBase.PreferenceKeys.LastBackupMessageId     // Catch:{ FileNotFoundException -> 0x027e, IOException -> 0x0b47, CustomException -> 0x0a90, Exception -> 0x09d9, all -> 0x0922 }
            long r8 = r18.longValue()     // Catch:{ FileNotFoundException -> 0x027e, IOException -> 0x0b47, CustomException -> 0x0a90, Exception -> 0x09d9, all -> 0x0922 }
            r0 = r36
            r1 = r39
            r2 = r8
            com.riteshsahu.SMSBackupRestoreBase.Common.setLongPreference(r0, r1, r2)     // Catch:{ FileNotFoundException -> 0x027e, IOException -> 0x0b47, CustomException -> 0x0a90, Exception -> 0x09d9, all -> 0x0922 }
            java.lang.String r39 = com.riteshsahu.SMSBackupRestoreBase.PreferenceKeys.LastBackupMessageDate     // Catch:{ FileNotFoundException -> 0x027e, IOException -> 0x0b47, CustomException -> 0x0a90, Exception -> 0x09d9, all -> 0x0922 }
            long r8 = r15.longValue()     // Catch:{ FileNotFoundException -> 0x027e, IOException -> 0x0b47, CustomException -> 0x0a90, Exception -> 0x09d9, all -> 0x0922 }
            r0 = r36
            r1 = r39
            r2 = r8
            com.riteshsahu.SMSBackupRestoreBase.Common.setLongPreference(r0, r1, r2)     // Catch:{ FileNotFoundException -> 0x027e, IOException -> 0x0b47, CustomException -> 0x0a90, Exception -> 0x09d9, all -> 0x0922 }
            r39 = r6
            r9 = r17
            r36 = r37
            r13 = r20
            r8 = r11
            r40 = r7
            r12 = r19
            r11 = r18
            r37 = r5
            r7 = r16
            r6 = r14
            r14 = r21
            r5 = r10
            r10 = r15
        L_0x0619:
            if (r40 == 0) goto L_0x061e
            r40.close()
        L_0x061e:
            boolean r36 = r38.booleanValue()
            if (r36 == 0) goto L_0x0627
            com.riteshsahu.SMSBackupRestoreBase.WakeLocker.releaseLock()
        L_0x0627:
            java.util.HashMap<java.lang.String, java.lang.String> r36 = com.riteshsahu.SMSBackupRestoreBase.SmsBackupProcessor.mContacts
            r36.clear()
            r0 = r24
            r1 = r8
            r0.setFailed(r1)
            r0 = r24
            r1 = r13
            r0.setSuccessful(r1)
            r0 = r24
            r1 = r14
            r0.setTotal(r1)
            return r24
        L_0x063f:
            long r39 = r20.longValue()     // Catch:{ FileNotFoundException -> 0x06a7, IOException -> 0x0aa4, CustomException -> 0x09ed, Exception -> 0x0936, all -> 0x086e }
            r5 = 0
            int r39 = (r39 > r5 ? 1 : (r39 == r5 ? 0 : -1))
            if (r39 != 0) goto L_0x0be9
            long r39 = java.lang.System.currentTimeMillis()     // Catch:{ FileNotFoundException -> 0x06a7, IOException -> 0x0aa4, CustomException -> 0x09ed, Exception -> 0x0936, all -> 0x086e }
            java.lang.Long r39 = java.lang.Long.valueOf(r39)     // Catch:{ FileNotFoundException -> 0x06a7, IOException -> 0x0aa4, CustomException -> 0x09ed, Exception -> 0x0936, all -> 0x086e }
        L_0x0651:
            java.lang.String r40 = com.riteshsahu.SMSBackupRestoreBase.PreferenceKeys.LastBackupMessageDate     // Catch:{ FileNotFoundException -> 0x06a7, IOException -> 0x0aa4, CustomException -> 0x09ed, Exception -> 0x0936, all -> 0x086e }
            long r5 = r39.longValue()     // Catch:{ FileNotFoundException -> 0x06a7, IOException -> 0x0aa4, CustomException -> 0x09ed, Exception -> 0x0936, all -> 0x086e }
            r0 = r36
            r1 = r40
            r2 = r5
            com.riteshsahu.SMSBackupRestoreBase.Common.setLongPreference(r0, r1, r2)     // Catch:{ FileNotFoundException -> 0x06a7, IOException -> 0x0aa4, CustomException -> 0x09ed, Exception -> 0x0936, all -> 0x086e }
            r40 = 2131230851(0x7f080083, float:1.8077766E38)
            r0 = r36
            r1 = r40
            java.lang.String r40 = r0.getString(r1)     // Catch:{ FileNotFoundException -> 0x06a7, IOException -> 0x0aa4, CustomException -> 0x09ed, Exception -> 0x0936, all -> 0x086e }
            r0 = r24
            r1 = r40
            r0.setMessage(r1)     // Catch:{ FileNotFoundException -> 0x06a7, IOException -> 0x0aa4, CustomException -> 0x09ed, Exception -> 0x0936, all -> 0x086e }
            r10 = r39
            r36 = r11
            r14 = r28
            r13 = r26
            r8 = r18
            r40 = r9
            r7 = r17
            r6 = r16
            r5 = r15
            r39 = r37
            r11 = r21
            r9 = r19
            r37 = r12
            r12 = r22
            goto L_0x0619
        L_0x068d:
            java.lang.String r37 = "Could not find any records."
            com.riteshsahu.SMSBackupRestoreBase.Common.logDebug(r37)     // Catch:{ FileNotFoundException -> 0x06a7, IOException -> 0x0aa4, CustomException -> 0x09ed, Exception -> 0x0936, all -> 0x086e }
            com.riteshsahu.SMSBackupRestoreBase.CustomException r37 = new com.riteshsahu.SMSBackupRestoreBase.CustomException     // Catch:{ FileNotFoundException -> 0x06a7, IOException -> 0x0aa4, CustomException -> 0x09ed, Exception -> 0x0936, all -> 0x086e }
            r39 = 2131230735(0x7f08000f, float:1.8077531E38)
            r0 = r36
            r1 = r39
            java.lang.String r39 = r0.getString(r1)     // Catch:{ FileNotFoundException -> 0x06a7, IOException -> 0x0aa4, CustomException -> 0x09ed, Exception -> 0x0936, all -> 0x086e }
            r0 = r37
            r1 = r39
            r0.<init>(r1)     // Catch:{ FileNotFoundException -> 0x06a7, IOException -> 0x0aa4, CustomException -> 0x09ed, Exception -> 0x0936, all -> 0x086e }
            throw r37     // Catch:{ FileNotFoundException -> 0x06a7, IOException -> 0x0aa4, CustomException -> 0x09ed, Exception -> 0x0936, all -> 0x086e }
        L_0x06a7:
            r37 = move-exception
            r6 = r37
            r10 = r28
            r7 = r18
            r5 = r17
            r40 = r16
            r8 = r22
            r39 = r15
            r37 = r9
            r9 = r26
            goto L_0x0290
        L_0x06bc:
            java.lang.StringBuilder r37 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
            java.lang.String r39 = "No Write Access to file: "
            r0 = r37
            r1 = r39
            r0.<init>(r1)     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
            r0 = r37
            r1 = r27
            java.lang.StringBuilder r37 = r0.append(r1)     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
            java.lang.String r37 = r37.toString()     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logDebug(r37)     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
            com.riteshsahu.SMSBackupRestoreBase.CustomException r37 = new com.riteshsahu.SMSBackupRestoreBase.CustomException     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
            r39 = 2131230729(0x7f080009, float:1.807752E38)
            r0 = r36
            r1 = r39
            java.lang.String r39 = r0.getString(r1)     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
            r40 = 1
            r0 = r40
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
            r40 = r0
            r5 = 0
            r40[r5] = r27     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
            java.lang.String r39 = java.lang.String.format(r39, r40)     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
            r0 = r37
            r1 = r39
            r0.<init>(r1)     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
            throw r37     // Catch:{ FileNotFoundException -> 0x06fa, IOException -> 0x070f, CustomException -> 0x078d, Exception -> 0x07db, all -> 0x0859 }
        L_0x06fa:
            r37 = move-exception
            r6 = r37
            r10 = r28
            r9 = r26
            r7 = r18
            r5 = r17
            r40 = r16
            r8 = r22
            r39 = r15
            r37 = r14
            goto L_0x0290
        L_0x070f:
            r37 = move-exception
            r6 = r37
            r10 = r28
            r9 = r26
            r7 = r18
            r5 = r17
            r40 = r16
            r8 = r22
            r39 = r15
            r37 = r14
        L_0x0722:
            r6.printStackTrace()     // Catch:{ all -> 0x02e9 }
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ all -> 0x02e9 }
            java.lang.String r12 = "App version: "
            r11.<init>(r12)     // Catch:{ all -> 0x02e9 }
            r12 = 2131230721(0x7f080001, float:1.8077503E38)
            r0 = r36
            r1 = r12
            java.lang.String r12 = r0.getString(r1)     // Catch:{ all -> 0x02e9 }
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ all -> 0x02e9 }
            java.lang.String r11 = r11.toString()     // Catch:{ all -> 0x02e9 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r11)     // Catch:{ all -> 0x02e9 }
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ all -> 0x02e9 }
            java.lang.String r12 = "Count: "
            r11.<init>(r12)     // Catch:{ all -> 0x02e9 }
            java.lang.StringBuilder r11 = r11.append(r10)     // Catch:{ all -> 0x02e9 }
            java.lang.String r11 = r11.toString()     // Catch:{ all -> 0x02e9 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r11)     // Catch:{ all -> 0x02e9 }
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ all -> 0x02e9 }
            java.lang.String r12 = "IOException: "
            r11.<init>(r12)     // Catch:{ all -> 0x02e9 }
            java.lang.String r12 = r6.getMessage()     // Catch:{ all -> 0x02e9 }
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ all -> 0x02e9 }
            java.lang.String r11 = r11.toString()     // Catch:{ all -> 0x02e9 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r11)     // Catch:{ all -> 0x02e9 }
            com.riteshsahu.SMSBackupRestoreBase.CustomException r11 = new com.riteshsahu.SMSBackupRestoreBase.CustomException     // Catch:{ all -> 0x02e9 }
            r12 = 2131230725(0x7f080005, float:1.807751E38)
            r0 = r36
            r1 = r12
            java.lang.String r36 = r0.getString(r1)     // Catch:{ all -> 0x02e9 }
            r12 = 1
            java.lang.Object[] r12 = new java.lang.Object[r12]     // Catch:{ all -> 0x02e9 }
            r13 = 0
            java.lang.String r6 = r6.getMessage()     // Catch:{ all -> 0x02e9 }
            r12[r13] = r6     // Catch:{ all -> 0x02e9 }
            r0 = r36
            r1 = r12
            java.lang.String r36 = java.lang.String.format(r0, r1)     // Catch:{ all -> 0x02e9 }
            r0 = r11
            r1 = r36
            r0.<init>(r1)     // Catch:{ all -> 0x02e9 }
            throw r11     // Catch:{ all -> 0x02e9 }
        L_0x078d:
            r37 = move-exception
            r6 = r37
            r10 = r28
            r9 = r26
            r7 = r18
            r5 = r17
            r40 = r16
            r8 = r22
            r39 = r15
            r37 = r14
        L_0x07a0:
            r6.printStackTrace()     // Catch:{ all -> 0x02e9 }
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ all -> 0x02e9 }
            java.lang.String r12 = "App version: "
            r11.<init>(r12)     // Catch:{ all -> 0x02e9 }
            r12 = 2131230721(0x7f080001, float:1.8077503E38)
            r0 = r36
            r1 = r12
            java.lang.String r36 = r0.getString(r1)     // Catch:{ all -> 0x02e9 }
            r0 = r11
            r1 = r36
            java.lang.StringBuilder r36 = r0.append(r1)     // Catch:{ all -> 0x02e9 }
            java.lang.String r36 = r36.toString()     // Catch:{ all -> 0x02e9 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r36)     // Catch:{ all -> 0x02e9 }
            java.lang.StringBuilder r36 = new java.lang.StringBuilder     // Catch:{ all -> 0x02e9 }
            java.lang.String r11 = "Count: "
            r0 = r36
            r1 = r11
            r0.<init>(r1)     // Catch:{ all -> 0x02e9 }
            r0 = r36
            r1 = r10
            java.lang.StringBuilder r36 = r0.append(r1)     // Catch:{ all -> 0x02e9 }
            java.lang.String r36 = r36.toString()     // Catch:{ all -> 0x02e9 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r36)     // Catch:{ all -> 0x02e9 }
            throw r6     // Catch:{ all -> 0x02e9 }
        L_0x07db:
            r37 = move-exception
            r6 = r37
            r10 = r28
            r9 = r26
            r7 = r18
            r5 = r17
            r40 = r16
            r8 = r22
            r39 = r15
            r37 = r14
        L_0x07ee:
            r6.printStackTrace()     // Catch:{ all -> 0x02e9 }
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ all -> 0x02e9 }
            java.lang.String r12 = "App version: "
            r11.<init>(r12)     // Catch:{ all -> 0x02e9 }
            r12 = 2131230721(0x7f080001, float:1.8077503E38)
            r0 = r36
            r1 = r12
            java.lang.String r12 = r0.getString(r1)     // Catch:{ all -> 0x02e9 }
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ all -> 0x02e9 }
            java.lang.String r11 = r11.toString()     // Catch:{ all -> 0x02e9 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r11)     // Catch:{ all -> 0x02e9 }
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ all -> 0x02e9 }
            java.lang.String r12 = "Count: "
            r11.<init>(r12)     // Catch:{ all -> 0x02e9 }
            java.lang.StringBuilder r11 = r11.append(r10)     // Catch:{ all -> 0x02e9 }
            java.lang.String r11 = r11.toString()     // Catch:{ all -> 0x02e9 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r11)     // Catch:{ all -> 0x02e9 }
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ all -> 0x02e9 }
            java.lang.String r12 = "Error occurred during backup: "
            r11.<init>(r12)     // Catch:{ all -> 0x02e9 }
            java.lang.String r12 = r6.getMessage()     // Catch:{ all -> 0x02e9 }
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ all -> 0x02e9 }
            java.lang.String r11 = r11.toString()     // Catch:{ all -> 0x02e9 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r11)     // Catch:{ all -> 0x02e9 }
            com.riteshsahu.SMSBackupRestoreBase.CustomException r11 = new com.riteshsahu.SMSBackupRestoreBase.CustomException     // Catch:{ all -> 0x02e9 }
            r12 = 2131230725(0x7f080005, float:1.807751E38)
            r0 = r36
            r1 = r12
            java.lang.String r36 = r0.getString(r1)     // Catch:{ all -> 0x02e9 }
            r12 = 1
            java.lang.Object[] r12 = new java.lang.Object[r12]     // Catch:{ all -> 0x02e9 }
            r13 = 0
            java.lang.String r6 = r6.getMessage()     // Catch:{ all -> 0x02e9 }
            r12[r13] = r6     // Catch:{ all -> 0x02e9 }
            r0 = r36
            r1 = r12
            java.lang.String r36 = java.lang.String.format(r0, r1)     // Catch:{ all -> 0x02e9 }
            r0 = r11
            r1 = r36
            r0.<init>(r1)     // Catch:{ all -> 0x02e9 }
            throw r11     // Catch:{ all -> 0x02e9 }
        L_0x0859:
            r36 = move-exception
            r9 = r36
            r8 = r28
            r7 = r26
            r5 = r18
            r40 = r17
            r39 = r16
            r6 = r22
            r37 = r15
            r36 = r14
            goto L_0x02fb
        L_0x086e:
            r36 = move-exception
            r8 = r28
            r7 = r26
            r5 = r18
            r40 = r17
            r39 = r16
            r6 = r22
            r37 = r15
            r35 = r36
            r36 = r9
            r9 = r35
            goto L_0x02fb
        L_0x0885:
            r36 = move-exception
            r8 = r28
            r7 = r26
            r40 = r17
            r39 = r5
            r6 = r22
            r37 = r15
            r5 = r18
            r35 = r9
            r9 = r36
            r36 = r35
            goto L_0x02fb
        L_0x089c:
            r36 = move-exception
            r8 = r28
            r7 = r26
            r5 = r18
            r40 = r16
            r39 = r14
            r6 = r22
            r37 = r15
            r35 = r36
            r36 = r9
            r9 = r35
            goto L_0x02fb
        L_0x08b3:
            r36 = move-exception
            r8 = r28
            r7 = r26
            r5 = r18
            r40 = r16
            r39 = r14
            r6 = r22
            r37 = r10
            r35 = r36
            r36 = r9
            r9 = r35
            goto L_0x02fb
        L_0x08ca:
            r36 = move-exception
            r8 = r28
            r5 = r18
            r40 = r16
            r39 = r14
            r6 = r7
            r37 = r10
            r7 = r26
            r35 = r9
            r9 = r36
            r36 = r35
            goto L_0x02fb
        L_0x08e0:
            r36 = move-exception
            r8 = r28
            r7 = r26
            r5 = r18
            r40 = r16
            r39 = r14
            r6 = r19
            r37 = r10
            r35 = r36
            r36 = r9
            r9 = r35
            goto L_0x02fb
        L_0x08f7:
            r36 = move-exception
            r8 = r21
            r7 = r20
            r5 = r11
            r40 = r16
            r39 = r14
            r6 = r19
            r37 = r10
            r35 = r36
            r36 = r9
            r9 = r35
            goto L_0x02fb
        L_0x090d:
            r36 = move-exception
            r5 = r7
            r40 = r16
            r39 = r14
            r6 = r19
            r37 = r10
            r7 = r8
            r8 = r21
            r35 = r36
            r36 = r9
            r9 = r35
            goto L_0x02fb
        L_0x0922:
            r36 = move-exception
            r9 = r36
            r8 = r21
            r5 = r11
            r40 = r16
            r39 = r14
            r6 = r19
            r37 = r10
            r36 = r7
            r7 = r20
            goto L_0x02fb
        L_0x0936:
            r37 = move-exception
            r6 = r37
            r10 = r28
            r7 = r18
            r5 = r17
            r40 = r16
            r8 = r22
            r39 = r15
            r37 = r9
            r9 = r26
            goto L_0x07ee
        L_0x094b:
            r37 = move-exception
            r6 = r37
            r10 = r28
            r7 = r18
            r40 = r5
            r8 = r22
            r39 = r15
            r37 = r9
            r5 = r17
            r9 = r26
            goto L_0x07ee
        L_0x0960:
            r37 = move-exception
            r6 = r37
            r10 = r28
            r7 = r18
            r5 = r16
            r40 = r14
            r8 = r22
            r39 = r15
            r37 = r9
            r9 = r26
            goto L_0x07ee
        L_0x0975:
            r37 = move-exception
            r6 = r37
            r7 = r18
            r5 = r16
            r40 = r14
            r8 = r22
            r39 = r10
            r10 = r28
            r37 = r9
            r9 = r26
            goto L_0x07ee
        L_0x098a:
            r37 = move-exception
            r6 = r37
            r5 = r16
            r40 = r14
            r8 = r7
            r39 = r10
            r10 = r28
            r7 = r18
            r37 = r9
            r9 = r26
            goto L_0x07ee
        L_0x099e:
            r37 = move-exception
            r6 = r37
            r7 = r18
            r5 = r16
            r40 = r14
            r8 = r19
            r39 = r10
            r10 = r28
            r37 = r9
            r9 = r26
            goto L_0x07ee
        L_0x09b3:
            r37 = move-exception
            r6 = r37
            r7 = r11
            r5 = r16
            r40 = r14
            r8 = r19
            r39 = r10
            r10 = r21
            r37 = r9
            r9 = r20
            goto L_0x07ee
        L_0x09c7:
            r37 = move-exception
            r6 = r37
            r5 = r16
            r40 = r14
            r39 = r10
            r10 = r21
            r37 = r9
            r9 = r8
            r8 = r19
            goto L_0x07ee
        L_0x09d9:
            r37 = move-exception
            r6 = r37
            r9 = r20
            r5 = r16
            r40 = r14
            r8 = r19
            r39 = r10
            r37 = r7
            r10 = r21
            r7 = r11
            goto L_0x07ee
        L_0x09ed:
            r37 = move-exception
            r6 = r37
            r10 = r28
            r7 = r18
            r5 = r17
            r40 = r16
            r8 = r22
            r39 = r15
            r37 = r9
            r9 = r26
            goto L_0x07a0
        L_0x0a02:
            r37 = move-exception
            r6 = r37
            r10 = r28
            r7 = r18
            r40 = r5
            r8 = r22
            r39 = r15
            r37 = r9
            r5 = r17
            r9 = r26
            goto L_0x07a0
        L_0x0a17:
            r37 = move-exception
            r6 = r37
            r10 = r28
            r7 = r18
            r5 = r16
            r40 = r14
            r8 = r22
            r39 = r15
            r37 = r9
            r9 = r26
            goto L_0x07a0
        L_0x0a2c:
            r37 = move-exception
            r6 = r37
            r7 = r18
            r5 = r16
            r40 = r14
            r8 = r22
            r39 = r10
            r10 = r28
            r37 = r9
            r9 = r26
            goto L_0x07a0
        L_0x0a41:
            r37 = move-exception
            r6 = r37
            r5 = r16
            r40 = r14
            r8 = r7
            r39 = r10
            r10 = r28
            r7 = r18
            r37 = r9
            r9 = r26
            goto L_0x07a0
        L_0x0a55:
            r37 = move-exception
            r6 = r37
            r7 = r18
            r5 = r16
            r40 = r14
            r8 = r19
            r39 = r10
            r10 = r28
            r37 = r9
            r9 = r26
            goto L_0x07a0
        L_0x0a6a:
            r37 = move-exception
            r6 = r37
            r7 = r11
            r5 = r16
            r40 = r14
            r8 = r19
            r39 = r10
            r10 = r21
            r37 = r9
            r9 = r20
            goto L_0x07a0
        L_0x0a7e:
            r37 = move-exception
            r6 = r37
            r5 = r16
            r40 = r14
            r39 = r10
            r10 = r21
            r37 = r9
            r9 = r8
            r8 = r19
            goto L_0x07a0
        L_0x0a90:
            r37 = move-exception
            r6 = r37
            r9 = r20
            r5 = r16
            r40 = r14
            r8 = r19
            r39 = r10
            r37 = r7
            r10 = r21
            r7 = r11
            goto L_0x07a0
        L_0x0aa4:
            r37 = move-exception
            r6 = r37
            r10 = r28
            r7 = r18
            r5 = r17
            r40 = r16
            r8 = r22
            r39 = r15
            r37 = r9
            r9 = r26
            goto L_0x0722
        L_0x0ab9:
            r37 = move-exception
            r6 = r37
            r10 = r28
            r7 = r18
            r40 = r5
            r8 = r22
            r39 = r15
            r37 = r9
            r5 = r17
            r9 = r26
            goto L_0x0722
        L_0x0ace:
            r37 = move-exception
            r6 = r37
            r10 = r28
            r7 = r18
            r5 = r16
            r40 = r14
            r8 = r22
            r39 = r15
            r37 = r9
            r9 = r26
            goto L_0x0722
        L_0x0ae3:
            r37 = move-exception
            r6 = r37
            r7 = r18
            r5 = r16
            r40 = r14
            r8 = r22
            r39 = r10
            r10 = r28
            r37 = r9
            r9 = r26
            goto L_0x0722
        L_0x0af8:
            r37 = move-exception
            r6 = r37
            r5 = r16
            r40 = r14
            r8 = r7
            r39 = r10
            r10 = r28
            r7 = r18
            r37 = r9
            r9 = r26
            goto L_0x0722
        L_0x0b0c:
            r37 = move-exception
            r6 = r37
            r7 = r18
            r5 = r16
            r40 = r14
            r8 = r19
            r39 = r10
            r10 = r28
            r37 = r9
            r9 = r26
            goto L_0x0722
        L_0x0b21:
            r37 = move-exception
            r6 = r37
            r7 = r11
            r5 = r16
            r40 = r14
            r8 = r19
            r39 = r10
            r10 = r21
            r37 = r9
            r9 = r20
            goto L_0x0722
        L_0x0b35:
            r37 = move-exception
            r6 = r37
            r5 = r16
            r40 = r14
            r39 = r10
            r10 = r21
            r37 = r9
            r9 = r8
            r8 = r19
            goto L_0x0722
        L_0x0b47:
            r37 = move-exception
            r6 = r37
            r9 = r20
            r5 = r16
            r40 = r14
            r8 = r19
            r39 = r10
            r37 = r7
            r10 = r21
            r7 = r11
            goto L_0x0722
        L_0x0b5b:
            r37 = move-exception
            r6 = r37
            r10 = r28
            r7 = r18
            r40 = r5
            r8 = r22
            r39 = r15
            r37 = r9
            r5 = r17
            r9 = r26
            goto L_0x0290
        L_0x0b70:
            r37 = move-exception
            r6 = r37
            r10 = r28
            r7 = r18
            r5 = r16
            r40 = r14
            r8 = r22
            r39 = r15
            r37 = r9
            r9 = r26
            goto L_0x0290
        L_0x0b85:
            r37 = move-exception
            r6 = r37
            r7 = r18
            r5 = r16
            r40 = r14
            r8 = r22
            r39 = r10
            r10 = r28
            r37 = r9
            r9 = r26
            goto L_0x0290
        L_0x0b9a:
            r37 = move-exception
            r6 = r37
            r5 = r16
            r40 = r14
            r8 = r7
            r39 = r10
            r10 = r28
            r7 = r18
            r37 = r9
            r9 = r26
            goto L_0x0290
        L_0x0bae:
            r37 = move-exception
            r6 = r37
            r7 = r18
            r5 = r16
            r40 = r14
            r8 = r19
            r39 = r10
            r10 = r28
            r37 = r9
            r9 = r26
            goto L_0x0290
        L_0x0bc3:
            r37 = move-exception
            r6 = r37
            r7 = r11
            r5 = r16
            r40 = r14
            r8 = r19
            r39 = r10
            r10 = r21
            r37 = r9
            r9 = r20
            goto L_0x0290
        L_0x0bd7:
            r37 = move-exception
            r6 = r37
            r5 = r16
            r40 = r14
            r39 = r10
            r10 = r21
            r37 = r9
            r9 = r8
            r8 = r19
            goto L_0x0290
        L_0x0be9:
            r39 = r20
            goto L_0x0651
        L_0x0bed:
            r7 = r8
            goto L_0x0581
        L_0x0bf0:
            r7 = r8
            goto L_0x0581
        L_0x0bf3:
            r37 = r11
            r19 = r7
            goto L_0x021b
        L_0x0bf9:
            r37 = r11
            r19 = r22
            goto L_0x021b
        L_0x0bff:
            r5 = r12
            goto L_0x0202
        L_0x0c02:
            r5 = r17
            r6 = r19
            goto L_0x0316
        L_0x0c08:
            r6 = r5
            r5 = r17
            goto L_0x0316
        L_0x0c0d:
            r6 = r8
            r5 = r7
            goto L_0x0402
        L_0x0c11:
            r23 = r6
            r17 = r19
            r6 = r37
            goto L_0x01c1
        L_0x0c19:
            r14 = r16
            r16 = r17
            goto L_0x0143
        L_0x0c1f:
            r20 = r5
            r21 = r6
            goto L_0x0095
        */
        throw new UnsupportedOperationException("Method not decompiled: com.riteshsahu.SMSBackupRestoreBase.SmsBackupProcessor.saveXmlIncremental(android.content.Context, com.riteshsahu.SMSBackupRestoreBase.BackupFile, java.lang.Boolean, android.app.ProgressDialog, android.os.Handler):com.riteshsahu.SMSBackupRestoreBase.OperationResult");
    }

    private static boolean setSerializerAttribute(KXmlSerializer serializer, String columnName, Cursor cursor) throws IllegalArgumentException, IllegalStateException, IOException {
        String value = getColumnValue(cursor, columnName);
        try {
            serializer.attribute("", columnName, value);
            if (value.equalsIgnoreCase(Common.NullString)) {
                for (String mandatoryColumn : Common.MandatoryColumnNames) {
                    if (mandatoryColumn.equalsIgnoreCase(columnName)) {
                        return false;
                    }
                }
            }
            return true;
        } catch (IllegalArgumentException e) {
            IllegalArgumentException ex = e;
            Common.logDebug("Error in column: " + columnName + ", value: " + value);
            throw ex;
        }
    }

    /* JADX INFO: Multiple debug info for r9v44 java.lang.String: [D('totalCount' int), D('columnName' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r12v21 'totalCount'  int: [D('name' java.lang.String), D('totalCount' int)] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0095 A[SYNTHETIC, Splitter:B:34:0x0095] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0109 A[SYNTHETIC, Splitter:B:47:0x0109] */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0174 A[SYNTHETIC, Splitter:B:57:0x0174] */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x01df A[SYNTHETIC, Splitter:B:67:0x01df] */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x0207 A[SYNTHETIC, Splitter:B:75:0x0207] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:44:0x00c3=Splitter:B:44:0x00c3, B:64:0x0199=Splitter:B:64:0x0199, B:54:0x012e=Splitter:B:54:0x012e} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean verifyXml(android.content.Context r8, java.lang.String r9, int r10, android.app.ProgressDialog r11, android.os.Handler r12) {
        /*
            r0 = 2131230839(0x7f080077, float:1.8077742E38)
            com.riteshsahu.SMSBackupRestoreBase.Common.resetProgressHandler(r11, r12, r0, r10)
            if (r11 == 0) goto L_0x000c
            r10 = 0
            r11.setProgress(r10)
        L_0x000c:
            r2 = 0
            com.riteshsahu.SMSBackupRestoreBase.KXmlParser r1 = new com.riteshsahu.SMSBackupRestoreBase.KXmlParser
            r1.<init>()
            r12 = 0
            r10 = r9
            java.io.File r9 = new java.io.File
            java.lang.String r0 = com.riteshsahu.SMSBackupRestoreBase.Common.getBackupFilePath(r8)
            r9.<init>(r0)
            boolean r9 = r9.canRead()
            if (r9 == 0) goto L_0x023c
            java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x02a0, XmlPullParserException -> 0x00c0, IOException -> 0x012b, Exception -> 0x0196, all -> 0x0201 }
            r0.<init>(r10)     // Catch:{ FileNotFoundException -> 0x02a0, XmlPullParserException -> 0x00c0, IOException -> 0x012b, Exception -> 0x0196, all -> 0x0201 }
            r9 = 0
            r1.setInput(r0, r9)     // Catch:{ FileNotFoundException -> 0x0065, XmlPullParserException -> 0x0296, IOException -> 0x028c, Exception -> 0x0282, all -> 0x026d }
            int r9 = r1.getEventType()     // Catch:{ FileNotFoundException -> 0x0065, XmlPullParserException -> 0x0296, IOException -> 0x028c, Exception -> 0x0282, all -> 0x026d }
        L_0x0030:
            r12 = 1
            if (r9 != r12) goto L_0x003d
            if (r0 == 0) goto L_0x0038
            r0.close()     // Catch:{ IOException -> 0x0223 }
        L_0x0038:
            r8 = 1
            r9 = r2
            r10 = r8
            r8 = r0
        L_0x003c:
            return r10
        L_0x003d:
            r12 = 0
            switch(r9) {
                case 2: goto L_0x0049;
                default: goto L_0x0041;
            }
        L_0x0041:
            r9 = r12
            r12 = r2
            int r9 = r1.next()     // Catch:{ FileNotFoundException -> 0x02a5, XmlPullParserException -> 0x029b, IOException -> 0x0291, Exception -> 0x0287, all -> 0x0272 }
            r2 = r12
            goto L_0x0030
        L_0x0049:
            java.lang.String r12 = r1.getName()     // Catch:{ FileNotFoundException -> 0x0065, XmlPullParserException -> 0x0296, IOException -> 0x028c, Exception -> 0x0282, all -> 0x026d }
            java.lang.String r9 = "sms"
            boolean r9 = r12.equalsIgnoreCase(r9)     // Catch:{ FileNotFoundException -> 0x0065, XmlPullParserException -> 0x0296, IOException -> 0x028c, Exception -> 0x0282, all -> 0x026d }
            if (r9 == 0) goto L_0x0041
            int r2 = r2 + 1
            java.lang.String[] r3 = com.riteshsahu.SMSBackupRestoreBase.Common.ColumnNames     // Catch:{ FileNotFoundException -> 0x0065, XmlPullParserException -> 0x0296, IOException -> 0x028c, Exception -> 0x0282, all -> 0x026d }
            int r4 = r3.length     // Catch:{ FileNotFoundException -> 0x0065, XmlPullParserException -> 0x0296, IOException -> 0x028c, Exception -> 0x0282, all -> 0x026d }
            r9 = 0
            r5 = r9
        L_0x005c:
            if (r5 < r4) goto L_0x009d
            if (r11 == 0) goto L_0x0041
            r9 = 1
            r11.incrementProgressBy(r9)     // Catch:{ FileNotFoundException -> 0x0065, XmlPullParserException -> 0x0296, IOException -> 0x028c, Exception -> 0x0282, all -> 0x026d }
            goto L_0x0041
        L_0x0065:
            r9 = move-exception
            r11 = r0
            r12 = r2
        L_0x0068:
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ all -> 0x0277 }
            java.lang.String r0 = "App version: "
            r9.<init>(r0)     // Catch:{ all -> 0x0277 }
            r0 = 2131230721(0x7f080001, float:1.8077503E38)
            java.lang.String r8 = r8.getString(r0)     // Catch:{ all -> 0x0277 }
            java.lang.StringBuilder r8 = r9.append(r8)     // Catch:{ all -> 0x0277 }
            java.lang.String r8 = r8.toString()     // Catch:{ all -> 0x0277 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r8)     // Catch:{ all -> 0x0277 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ all -> 0x0277 }
            java.lang.String r9 = "Could not find file - "
            r8.<init>(r9)     // Catch:{ all -> 0x0277 }
            java.lang.StringBuilder r8 = r8.append(r10)     // Catch:{ all -> 0x0277 }
            java.lang.String r8 = r8.toString()     // Catch:{ all -> 0x0277 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r8)     // Catch:{ all -> 0x0277 }
            if (r11 == 0) goto L_0x0098
            r11.close()     // Catch:{ IOException -> 0x00a8 }
        L_0x0098:
            r8 = 0
            r9 = r12
            r10 = r8
            r8 = r11
            goto L_0x003c
        L_0x009d:
            r9 = r3[r5]     // Catch:{ FileNotFoundException -> 0x0065, XmlPullParserException -> 0x0296, IOException -> 0x028c, Exception -> 0x0282, all -> 0x026d }
            java.lang.String r6 = ""
            r1.getAttributeValue(r6, r9)     // Catch:{ FileNotFoundException -> 0x0065, XmlPullParserException -> 0x0296, IOException -> 0x028c, Exception -> 0x0282, all -> 0x026d }
            int r9 = r5 + 1
            r5 = r9
            goto L_0x005c
        L_0x00a8:
            r8 = move-exception
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            java.lang.String r10 = "IOException "
            r9.<init>(r10)
            java.lang.String r8 = r8.getMessage()
            java.lang.StringBuilder r8 = r9.append(r8)
            java.lang.String r8 = r8.toString()
            com.riteshsahu.SMSBackupRestoreBase.Common.logDebug(r8)
            goto L_0x0098
        L_0x00c0:
            r9 = move-exception
            r10 = r12
            r11 = r2
        L_0x00c3:
            r9.printStackTrace()     // Catch:{ all -> 0x027c }
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ all -> 0x027c }
            java.lang.String r0 = "App version: "
            r12.<init>(r0)     // Catch:{ all -> 0x027c }
            r0 = 2131230721(0x7f080001, float:1.8077503E38)
            java.lang.String r8 = r8.getString(r0)     // Catch:{ all -> 0x027c }
            java.lang.StringBuilder r8 = r12.append(r8)     // Catch:{ all -> 0x027c }
            java.lang.String r8 = r8.toString()     // Catch:{ all -> 0x027c }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r8)     // Catch:{ all -> 0x027c }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ all -> 0x027c }
            java.lang.String r12 = "Count: "
            r8.<init>(r12)     // Catch:{ all -> 0x027c }
            java.lang.StringBuilder r8 = r8.append(r11)     // Catch:{ all -> 0x027c }
            java.lang.String r8 = r8.toString()     // Catch:{ all -> 0x027c }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r8)     // Catch:{ all -> 0x027c }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ all -> 0x027c }
            java.lang.String r12 = "Error Parsing xml - "
            r8.<init>(r12)     // Catch:{ all -> 0x027c }
            java.lang.String r9 = r9.getMessage()     // Catch:{ all -> 0x027c }
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ all -> 0x027c }
            java.lang.String r8 = r8.toString()     // Catch:{ all -> 0x027c }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r8)     // Catch:{ all -> 0x027c }
            if (r10 == 0) goto L_0x010c
            r10.close()     // Catch:{ IOException -> 0x0113 }
        L_0x010c:
            r8 = 0
            r9 = r11
            r7 = r10
            r10 = r8
            r8 = r7
            goto L_0x003c
        L_0x0113:
            r8 = move-exception
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            java.lang.String r12 = "IOException "
            r9.<init>(r12)
            java.lang.String r8 = r8.getMessage()
            java.lang.StringBuilder r8 = r9.append(r8)
            java.lang.String r8 = r8.toString()
            com.riteshsahu.SMSBackupRestoreBase.Common.logDebug(r8)
            goto L_0x010c
        L_0x012b:
            r9 = move-exception
            r10 = r12
            r11 = r2
        L_0x012e:
            r9.printStackTrace()     // Catch:{ all -> 0x027c }
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ all -> 0x027c }
            java.lang.String r0 = "App version: "
            r12.<init>(r0)     // Catch:{ all -> 0x027c }
            r0 = 2131230721(0x7f080001, float:1.8077503E38)
            java.lang.String r8 = r8.getString(r0)     // Catch:{ all -> 0x027c }
            java.lang.StringBuilder r8 = r12.append(r8)     // Catch:{ all -> 0x027c }
            java.lang.String r8 = r8.toString()     // Catch:{ all -> 0x027c }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r8)     // Catch:{ all -> 0x027c }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ all -> 0x027c }
            java.lang.String r12 = "Count: "
            r8.<init>(r12)     // Catch:{ all -> 0x027c }
            java.lang.StringBuilder r8 = r8.append(r11)     // Catch:{ all -> 0x027c }
            java.lang.String r8 = r8.toString()     // Catch:{ all -> 0x027c }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r8)     // Catch:{ all -> 0x027c }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ all -> 0x027c }
            java.lang.String r12 = "IOException - "
            r8.<init>(r12)     // Catch:{ all -> 0x027c }
            java.lang.String r9 = r9.getMessage()     // Catch:{ all -> 0x027c }
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ all -> 0x027c }
            java.lang.String r8 = r8.toString()     // Catch:{ all -> 0x027c }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r8)     // Catch:{ all -> 0x027c }
            if (r10 == 0) goto L_0x0177
            r10.close()     // Catch:{ IOException -> 0x017e }
        L_0x0177:
            r8 = 0
            r9 = r11
            r7 = r10
            r10 = r8
            r8 = r7
            goto L_0x003c
        L_0x017e:
            r8 = move-exception
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            java.lang.String r12 = "IOException "
            r9.<init>(r12)
            java.lang.String r8 = r8.getMessage()
            java.lang.StringBuilder r8 = r9.append(r8)
            java.lang.String r8 = r8.toString()
            com.riteshsahu.SMSBackupRestoreBase.Common.logDebug(r8)
            goto L_0x0177
        L_0x0196:
            r9 = move-exception
            r10 = r12
            r11 = r2
        L_0x0199:
            r9.printStackTrace()     // Catch:{ all -> 0x027c }
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ all -> 0x027c }
            java.lang.String r0 = "App version: "
            r12.<init>(r0)     // Catch:{ all -> 0x027c }
            r0 = 2131230721(0x7f080001, float:1.8077503E38)
            java.lang.String r8 = r8.getString(r0)     // Catch:{ all -> 0x027c }
            java.lang.StringBuilder r8 = r12.append(r8)     // Catch:{ all -> 0x027c }
            java.lang.String r8 = r8.toString()     // Catch:{ all -> 0x027c }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r8)     // Catch:{ all -> 0x027c }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ all -> 0x027c }
            java.lang.String r12 = "Count: "
            r8.<init>(r12)     // Catch:{ all -> 0x027c }
            java.lang.StringBuilder r8 = r8.append(r11)     // Catch:{ all -> 0x027c }
            java.lang.String r8 = r8.toString()     // Catch:{ all -> 0x027c }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r8)     // Catch:{ all -> 0x027c }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ all -> 0x027c }
            java.lang.String r12 = "Exception - "
            r8.<init>(r12)     // Catch:{ all -> 0x027c }
            java.lang.String r9 = r9.getMessage()     // Catch:{ all -> 0x027c }
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ all -> 0x027c }
            java.lang.String r8 = r8.toString()     // Catch:{ all -> 0x027c }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r8)     // Catch:{ all -> 0x027c }
            if (r10 == 0) goto L_0x01e2
            r10.close()     // Catch:{ IOException -> 0x01e9 }
        L_0x01e2:
            r8 = 0
            r9 = r11
            r7 = r10
            r10 = r8
            r8 = r7
            goto L_0x003c
        L_0x01e9:
            r8 = move-exception
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            java.lang.String r12 = "IOException "
            r9.<init>(r12)
            java.lang.String r8 = r8.getMessage()
            java.lang.StringBuilder r8 = r9.append(r8)
            java.lang.String r8 = r8.toString()
            com.riteshsahu.SMSBackupRestoreBase.Common.logDebug(r8)
            goto L_0x01e2
        L_0x0201:
            r8 = move-exception
            r10 = r8
            r9 = r2
            r8 = r12
        L_0x0205:
            if (r8 == 0) goto L_0x020a
            r8.close()     // Catch:{ IOException -> 0x020b }
        L_0x020a:
            throw r10
        L_0x020b:
            r8 = move-exception
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            java.lang.String r11 = "IOException "
            r9.<init>(r11)
            java.lang.String r8 = r8.getMessage()
            java.lang.StringBuilder r8 = r9.append(r8)
            java.lang.String r8 = r8.toString()
            com.riteshsahu.SMSBackupRestoreBase.Common.logDebug(r8)
            goto L_0x020a
        L_0x0223:
            r8 = move-exception
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            java.lang.String r10 = "IOException "
            r9.<init>(r10)
            java.lang.String r8 = r8.getMessage()
            java.lang.StringBuilder r8 = r9.append(r8)
            java.lang.String r8 = r8.toString()
            com.riteshsahu.SMSBackupRestoreBase.Common.logDebug(r8)
            goto L_0x0038
        L_0x023c:
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            java.lang.String r11 = "App version: "
            r9.<init>(r11)
            r11 = 2131230721(0x7f080001, float:1.8077503E38)
            java.lang.String r8 = r8.getString(r11)
            java.lang.StringBuilder r8 = r9.append(r8)
            java.lang.String r8 = r8.toString()
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r8)
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            java.lang.String r9 = "Could not read file: "
            r8.<init>(r9)
            java.lang.StringBuilder r8 = r8.append(r10)
            java.lang.String r8 = r8.toString()
            com.riteshsahu.SMSBackupRestoreBase.Common.logDebug(r8)
            r8 = 0
            r9 = r2
            r10 = r8
            r8 = r12
            goto L_0x003c
        L_0x026d:
            r8 = move-exception
            r10 = r8
            r9 = r2
            r8 = r0
            goto L_0x0205
        L_0x0272:
            r8 = move-exception
            r10 = r8
            r9 = r12
            r8 = r0
            goto L_0x0205
        L_0x0277:
            r8 = move-exception
            r10 = r8
            r9 = r12
            r8 = r11
            goto L_0x0205
        L_0x027c:
            r8 = move-exception
            r9 = r11
            r7 = r10
            r10 = r8
            r8 = r7
            goto L_0x0205
        L_0x0282:
            r9 = move-exception
            r10 = r0
            r11 = r2
            goto L_0x0199
        L_0x0287:
            r9 = move-exception
            r10 = r0
            r11 = r12
            goto L_0x0199
        L_0x028c:
            r9 = move-exception
            r10 = r0
            r11 = r2
            goto L_0x012e
        L_0x0291:
            r9 = move-exception
            r10 = r0
            r11 = r12
            goto L_0x012e
        L_0x0296:
            r9 = move-exception
            r10 = r0
            r11 = r2
            goto L_0x00c3
        L_0x029b:
            r9 = move-exception
            r10 = r0
            r11 = r12
            goto L_0x00c3
        L_0x02a0:
            r9 = move-exception
            r11 = r12
            r12 = r2
            goto L_0x0068
        L_0x02a5:
            r9 = move-exception
            r11 = r0
            goto L_0x0068
        */
        throw new UnsupportedOperationException("Method not decompiled: com.riteshsahu.SMSBackupRestoreBase.SmsBackupProcessor.verifyXml(android.content.Context, java.lang.String, int, android.app.ProgressDialog, android.os.Handler):boolean");
    }
}
