package com.riteshsahu.SMSBackupRestoreBase;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import java.util.GregorianCalendar;

public class AlarmProcessor extends BroadcastReceiver {
    private static final String mAlarmActionName = "com.riteshsahu.SMSBackupRestore.Backup";

    /* access modifiers changed from: protected */
    public Intent createAlarmProcessorIntent(Context context) {
        return new Intent(context, AlarmProcessor.class);
    }

    public void UpdateAlarm(Context context) {
        Intent intent = createAlarmProcessorIntent(context);
        intent.setAction(mAlarmActionName);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService("alarm");
        alarmManager.cancel(pendingIntent);
        if (Common.getBooleanPreference(context, PreferenceKeys.UseScheduledBackups).booleanValue()) {
            long repeatInterval = 0;
            String repeatString = Common.getStringPreference(context, PreferenceKeys.ScheduleRepeatEvery);
            if (repeatString.length() == 0) {
                repeatString = Common.MESSAGE_TYPE_INBOX;
            }
            int repeatCount = Integer.parseInt(repeatString);
            switch (Common.getIntPreference(context, PreferenceKeys.ScheduleRepeatType)) {
                case 0:
                    repeatInterval = 3600000 * ((long) repeatCount);
                    break;
                case 1:
                    repeatInterval = 86400000 * ((long) repeatCount);
                    break;
                case 2:
                    repeatInterval = 60000 * ((long) repeatCount);
                    break;
            }
            long lastBackupDate = Common.getLongPreference(context, PreferenceKeys.LastScheduleDate);
            GregorianCalendar scheduleStart = new GregorianCalendar();
            if (lastBackupDate == 0) {
                String time = Common.getStringPreference(context, PreferenceKeys.ScheduleTimeofBackup);
                if (time.length() == 0) {
                    time = Common.MESSAGE_TYPE_ALL;
                }
                scheduleStart.set(11, Integer.parseInt(time));
                if (new GregorianCalendar().after(scheduleStart)) {
                    scheduleStart.add(5, 1);
                }
            } else {
                scheduleStart.setTimeInMillis(lastBackupDate + repeatInterval);
            }
            scheduleStart.set(12, 0);
            scheduleStart.set(13, 0);
            alarmManager.setRepeating(0, scheduleStart.getTimeInMillis(), repeatInterval, pendingIntent);
        }
    }

    public void onReceive(Context context, Intent intent) {
        if (intent != null) {
            Common.logDebug("AlarmProcessor.onReceive called with intentAction: " + intent.getAction());
        } else {
            Common.logDebug("AlarmProcessor.onReceive called with null intent.");
        }
        if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction())) {
            UpdateAlarm(context);
        }
        if (mAlarmActionName.equals(intent.getAction())) {
            WakeLocker.acquireLock(context);
            startAlarmProcessorService(context);
            Common.setLongPreference(context, PreferenceKeys.LastScheduleDate, System.currentTimeMillis());
        }
    }

    /* access modifiers changed from: protected */
    public void startAlarmProcessorService(Context context) {
        context.startService(new Intent(context, AlarmProcessorService.class));
    }
}
