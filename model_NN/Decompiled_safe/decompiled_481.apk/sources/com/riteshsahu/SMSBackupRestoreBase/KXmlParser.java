package com.riteshsahu.SMSBackupRestoreBase;

import java.io.IOException;
import java.io.Reader;
import java.util.Hashtable;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class KXmlParser implements XmlPullParser {
    private static final String ILLEGAL_TYPE = "Wrong event type";
    private static final int LEGACY = 999;
    private static final String UNEXPECTED_EOF = "Unexpected EOF";
    private static final int XML_DECL = 998;
    private int attributeCount;
    private String[] attributes = new String[16];
    private int column;
    private boolean degenerated;
    private int depth;
    private String[] elementStack = new String[16];
    private String encoding;
    private Hashtable entityMap;
    private String error;
    private boolean isWhitespace;
    private int line;
    private Object location;
    private String name;
    private String namespace;
    private int[] nspCounts = new int[4];
    private String[] nspStack = new String[8];
    private int[] peek = new int[2];
    private int peekCount;
    private String prefix;
    private boolean processNsp;
    private Reader reader;
    private boolean relaxed = true;
    private char[] srcBuf;
    private int srcCount;
    private int srcPos;
    private int stackMismatch = 0;
    private Boolean standalone;
    private boolean token;
    private char[] txtBuf = new char[128];
    private int txtPos;
    private int type;
    private boolean unresolved;
    private String version;
    private boolean wasCR;

    public KXmlParser() {
        this.srcBuf = new char[(Runtime.getRuntime().freeMemory() >= 1048576 ? 8192 : 128)];
    }

    private final boolean isProp(String n1, boolean prop, String n2) {
        if (!n1.startsWith("http://xmlpull.org/v1/doc/")) {
            return false;
        }
        if (prop) {
            return n1.substring(42).equals(n2);
        }
        return n1.substring(40).equals(n2);
    }

    private final boolean adjustNsp() throws XmlPullParserException {
        String prefix2;
        String attrName;
        boolean any = false;
        int i = 0;
        while (i < (this.attributeCount << 2)) {
            String attrName2 = this.attributes[i + 2];
            int cut = attrName2.indexOf(58);
            if (cut != -1) {
                prefix2 = attrName2.substring(0, cut);
                attrName = attrName2.substring(cut + 1);
            } else if (attrName2.equals("xmlns")) {
                prefix2 = attrName2;
                attrName = null;
            } else {
                i += 4;
            }
            if (!prefix2.equals("xmlns")) {
                any = true;
            } else {
                int[] iArr = this.nspCounts;
                int i2 = this.depth;
                int i3 = iArr[i2];
                iArr[i2] = i3 + 1;
                int j = i3 << 1;
                this.nspStack = ensureCapacity(this.nspStack, j + 2);
                this.nspStack[j] = attrName;
                this.nspStack[j + 1] = this.attributes[i + 3];
                if (attrName != null && this.attributes[i + 3].equals("")) {
                    error("illegal empty namespace");
                }
                String[] strArr = this.attributes;
                int i4 = this.attributeCount - 1;
                this.attributeCount = i4;
                System.arraycopy(this.attributes, i + 4, strArr, i, (i4 << 2) - i);
                i -= 4;
            }
            i += 4;
        }
        if (any) {
            int i5 = (this.attributeCount << 2) - 4;
            while (i5 >= 0) {
                String attrName3 = this.attributes[i5 + 2];
                int cut2 = attrName3.indexOf(58);
                if (cut2 != 0 || this.relaxed) {
                    if (cut2 != -1) {
                        String attrPrefix = attrName3.substring(0, cut2);
                        String attrName4 = attrName3.substring(cut2 + 1);
                        String attrNs = getNamespace(attrPrefix);
                        if (attrNs != null || this.relaxed) {
                            this.attributes[i5] = attrNs;
                            this.attributes[i5 + 1] = attrPrefix;
                            this.attributes[i5 + 2] = attrName4;
                        } else {
                            throw new RuntimeException("Undefined Prefix: " + attrPrefix + " in " + this);
                        }
                    }
                    i5 -= 4;
                } else {
                    throw new RuntimeException("illegal attribute name: " + attrName3 + " at " + this);
                }
            }
        }
        int cut3 = this.name.indexOf(58);
        if (cut3 == 0) {
            error("illegal tag name: " + this.name);
        }
        if (cut3 != -1) {
            this.prefix = this.name.substring(0, cut3);
            this.name = this.name.substring(cut3 + 1);
        }
        this.namespace = getNamespace(this.prefix);
        if (this.namespace == null) {
            if (this.prefix != null) {
                error("undefined prefix: " + this.prefix);
            }
            this.namespace = "";
        }
        return any;
    }

    private final String[] ensureCapacity(String[] arr, int required) {
        if (arr.length >= required) {
            return arr;
        }
        String[] bigger = new String[(required + 16)];
        System.arraycopy(arr, 0, bigger, 0, arr.length);
        return bigger;
    }

    private final void error(String desc) throws XmlPullParserException {
        if (!this.relaxed) {
            exception(desc);
        } else if (this.error == null) {
            this.error = "ERR: " + desc;
        }
    }

    private final void exception(String desc) throws XmlPullParserException {
        throw new XmlPullParserException(desc.length() < 100 ? desc : String.valueOf(desc.substring(0, 100)) + "\n", this, null);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x001d A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0022  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void nextImpl() throws java.io.IOException, org.xmlpull.v1.XmlPullParserException {
        /*
            r9 = this;
            r8 = -1
            r7 = 3
            r6 = 0
            r5 = 0
            r4 = 1
            java.io.Reader r2 = r9.reader
            if (r2 != 0) goto L_0x000e
            java.lang.String r2 = "No Input specified"
            r9.exception(r2)
        L_0x000e:
            int r2 = r9.type
            if (r2 != r7) goto L_0x0017
            int r2 = r9.depth
            int r2 = r2 - r4
            r9.depth = r2
        L_0x0017:
            r9.attributeCount = r8
            boolean r2 = r9.degenerated
            if (r2 == 0) goto L_0x0022
            r9.degenerated = r5
            r9.type = r7
        L_0x0021:
            return
        L_0x0022:
            java.lang.String r2 = r9.error
            if (r2 == 0) goto L_0x0042
            r0 = 0
        L_0x0027:
            java.lang.String r2 = r9.error
            int r2 = r2.length()
            if (r0 < r2) goto L_0x0036
            r9.error = r6
            r2 = 9
            r9.type = r2
            goto L_0x0021
        L_0x0036:
            java.lang.String r2 = r9.error
            char r2 = r2.charAt(r0)
            r9.push(r2)
            int r0 = r0 + 1
            goto L_0x0027
        L_0x0042:
            boolean r2 = r9.relaxed
            if (r2 == 0) goto L_0x0098
            int r2 = r9.stackMismatch
            if (r2 > 0) goto L_0x0054
            int r2 = r9.peek(r5)
            if (r2 != r8) goto L_0x0098
            int r2 = r9.depth
            if (r2 <= 0) goto L_0x0098
        L_0x0054:
            int r2 = r9.depth
            int r2 = r2 - r4
            int r1 = r2 << 2
            r9.type = r7
            java.lang.String[] r2 = r9.elementStack
            r2 = r2[r1]
            r9.namespace = r2
            java.lang.String[] r2 = r9.elementStack
            int r3 = r1 + 1
            r2 = r2[r3]
            r9.prefix = r2
            java.lang.String[] r2 = r9.elementStack
            int r3 = r1 + 2
            r2 = r2[r3]
            r9.name = r2
            int r2 = r9.stackMismatch
            if (r2 == r4) goto L_0x008e
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "missing end tag /"
            r2.<init>(r3)
            java.lang.String r3 = r9.name
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = " inserted"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r9.error = r2
        L_0x008e:
            int r2 = r9.stackMismatch
            if (r2 <= 0) goto L_0x0021
            int r2 = r9.stackMismatch
            int r2 = r2 - r4
            r9.stackMismatch = r2
            goto L_0x0021
        L_0x0098:
            r9.prefix = r6
            r9.name = r6
            r9.namespace = r6
            int r2 = r9.peekType()
            r9.type = r2
            int r2 = r9.type
            switch(r2) {
                case 1: goto L_0x0021;
                case 2: goto L_0x00be;
                case 3: goto L_0x00c3;
                case 4: goto L_0x00c8;
                case 5: goto L_0x00a9;
                case 6: goto L_0x00b9;
                default: goto L_0x00a9;
            }
        L_0x00a9:
            boolean r2 = r9.token
            int r2 = r9.parseLegacy(r2)
            r9.type = r2
            int r2 = r9.type
            r3 = 998(0x3e6, float:1.398E-42)
            if (r2 == r3) goto L_0x0017
            goto L_0x0021
        L_0x00b9:
            r9.pushEntity()
            goto L_0x0021
        L_0x00be:
            r9.parseStartTag(r5)
            goto L_0x0021
        L_0x00c3:
            r9.parseEndTag()
            goto L_0x0021
        L_0x00c8:
            r2 = 60
            boolean r3 = r9.token
            if (r3 == 0) goto L_0x00df
            r3 = r5
        L_0x00cf:
            r9.pushText(r2, r3)
            int r2 = r9.depth
            if (r2 != 0) goto L_0x0021
            boolean r2 = r9.isWhitespace
            if (r2 == 0) goto L_0x0021
            r2 = 7
            r9.type = r2
            goto L_0x0021
        L_0x00df:
            r3 = r4
            goto L_0x00cf
        */
        throw new UnsupportedOperationException("Method not decompiled: com.riteshsahu.SMSBackupRestoreBase.KXmlParser.nextImpl():void");
    }

    private final int parseLegacy(boolean push) throws IOException, XmlPullParserException {
        int result;
        int term;
        String req = "";
        int prev = 0;
        read();
        int c = read();
        if (c == 63) {
            if ((peek(0) == 120 || peek(0) == 88) && (peek(1) == 109 || peek(1) == 77)) {
                if (push) {
                    push(peek(0));
                    push(peek(1));
                }
                read();
                read();
                if ((peek(0) == 108 || peek(0) == 76) && peek(1) <= 32) {
                    if (this.line != 1 || this.column > 4) {
                        error("PI must not start with xml");
                    }
                    parseStartTag(true);
                    if (this.attributeCount < 1 || !"version".equals(this.attributes[2])) {
                        error("version expected");
                    }
                    this.version = this.attributes[3];
                    int pos = 1;
                    if (1 < this.attributeCount && "encoding".equals(this.attributes[6])) {
                        this.encoding = this.attributes[7];
                        pos = 1 + 1;
                    }
                    if (pos < this.attributeCount && "standalone".equals(this.attributes[(pos * 4) + 2])) {
                        String st = this.attributes[(pos * 4) + 3];
                        if ("yes".equals(st)) {
                            this.standalone = new Boolean(true);
                        } else if ("no".equals(st)) {
                            this.standalone = new Boolean(false);
                        } else {
                            error("illegal standalone value: " + st);
                        }
                        pos++;
                    }
                    if (pos != this.attributeCount) {
                        error("illegal xmldecl");
                    }
                    this.isWhitespace = true;
                    this.txtPos = 0;
                    return XML_DECL;
                }
            }
            term = 63;
            result = 8;
        } else if (c != 33) {
            error("illegal: <" + c);
            return 9;
        } else if (peek(0) == 45) {
            result = 9;
            req = "--";
            term = 45;
        } else if (peek(0) == 91) {
            result = 5;
            req = "[CDATA[";
            term = 93;
            push = true;
        } else {
            result = 10;
            req = "DOCTYPE";
            term = -1;
        }
        for (int i = 0; i < req.length(); i++) {
            read(req.charAt(i));
        }
        if (result == 10) {
            parseDoctype(push);
        } else {
            while (true) {
                int c2 = read();
                if (c2 == -1) {
                    error(UNEXPECTED_EOF);
                    return 9;
                }
                if (push) {
                    push(c2);
                }
                if ((term == 63 || c2 == term) && peek(0) == term && peek(1) == 62) {
                    if (term == 45 && prev == 45) {
                        error("illegal comment delimiter: --->");
                    }
                    read();
                    read();
                    if (push && term != 63) {
                        this.txtPos--;
                    }
                } else {
                    prev = c2;
                }
            }
        }
        return result;
    }

    private final void parseDoctype(boolean push) throws IOException, XmlPullParserException {
        int nesting = 1;
        boolean quoted = false;
        while (true) {
            int i = read();
            switch (i) {
                case -1:
                    error(UNEXPECTED_EOF);
                    return;
                case 39:
                    if (!quoted) {
                        quoted = true;
                        break;
                    } else {
                        quoted = false;
                        break;
                    }
                case 60:
                    if (!quoted) {
                        nesting++;
                        break;
                    }
                    break;
                case 62:
                    if (!quoted && nesting - 1 == 0) {
                        return;
                    }
            }
            if (push) {
                push(i);
            }
        }
    }

    private final void parseEndTag() throws IOException, XmlPullParserException {
        read();
        read();
        this.name = readName();
        skip();
        read('>');
        int sp = (this.depth - 1) << 2;
        if (this.depth == 0) {
            error("element stack empty");
            this.type = 9;
            return;
        }
        if (!this.name.equals(this.elementStack[sp + 3])) {
            error("expected: /" + this.elementStack[sp + 3] + " read: " + this.name);
            int probe = sp;
            while (probe >= 0 && !this.name.toLowerCase().equals(this.elementStack[probe + 3].toLowerCase())) {
                this.stackMismatch++;
                probe -= 4;
            }
            if (probe < 0) {
                this.stackMismatch = 0;
                this.type = 9;
                return;
            }
        }
        this.namespace = this.elementStack[sp];
        this.prefix = this.elementStack[sp + 1];
        this.name = this.elementStack[sp + 2];
    }

    private final int peekType() throws IOException {
        switch (peek(0)) {
            case -1:
                return 1;
            case 38:
                return 6;
            case 60:
                switch (peek(1)) {
                    case 33:
                    case 63:
                        return LEGACY;
                    case 47:
                        return 3;
                    default:
                        return 2;
                }
            default:
                return 4;
        }
    }

    private final String get(int pos) {
        return new String(this.txtBuf, pos, this.txtPos - pos);
    }

    private final void push(int c) {
        this.isWhitespace &= c <= 32;
        if (this.txtPos == this.txtBuf.length) {
            char[] bigger = new char[(((this.txtPos * 4) / 3) + 4)];
            System.arraycopy(this.txtBuf, 0, bigger, 0, this.txtPos);
            this.txtBuf = bigger;
        }
        char[] cArr = this.txtBuf;
        int i = this.txtPos;
        this.txtPos = i + 1;
        cArr[i] = (char) c;
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x00a3  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x009d A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void parseStartTag(boolean r15) throws java.io.IOException, org.xmlpull.v1.XmlPullParserException {
        /*
            r14 = this;
            r13 = 61
            r10 = 62
            r12 = 1
            r11 = 0
            if (r15 != 0) goto L_0x000b
            r14.read()
        L_0x000b:
            java.lang.String r8 = r14.readName()
            r14.name = r8
            r14.attributeCount = r11
        L_0x0013:
            r14.skip()
            int r2 = r14.peek(r11)
            if (r15 == 0) goto L_0x0027
            r8 = 63
            if (r2 != r8) goto L_0x009a
            r14.read()
            r14.read(r10)
        L_0x0026:
            return
        L_0x0027:
            r8 = 47
            if (r2 != r8) goto L_0x0092
            r14.degenerated = r12
            r14.read()
            r14.skip()
            r14.read(r10)
        L_0x0036:
            int r8 = r14.depth
            int r9 = r8 + 1
            r14.depth = r9
            int r7 = r8 << 2
            java.lang.String[] r8 = r14.elementStack
            int r9 = r7 + 4
            java.lang.String[] r8 = r14.ensureCapacity(r8, r9)
            r14.elementStack = r8
            java.lang.String[] r8 = r14.elementStack
            int r9 = r7 + 3
            java.lang.String r10 = r14.name
            r8[r9] = r10
            int r8 = r14.depth
            int[] r9 = r14.nspCounts
            int r9 = r9.length
            if (r8 < r9) goto L_0x0067
            int r8 = r14.depth
            int r8 = r8 + 4
            int[] r1 = new int[r8]
            int[] r8 = r14.nspCounts
            int[] r9 = r14.nspCounts
            int r9 = r9.length
            java.lang.System.arraycopy(r8, r11, r1, r11, r9)
            r14.nspCounts = r1
        L_0x0067:
            int[] r8 = r14.nspCounts
            int r9 = r14.depth
            int[] r10 = r14.nspCounts
            int r11 = r14.depth
            int r11 = r11 - r12
            r10 = r10[r11]
            r8[r9] = r10
            boolean r8 = r14.processNsp
            if (r8 == 0) goto L_0x0132
            r14.adjustNsp()
        L_0x007b:
            java.lang.String[] r8 = r14.elementStack
            java.lang.String r9 = r14.namespace
            r8[r7] = r9
            java.lang.String[] r8 = r14.elementStack
            int r9 = r7 + 1
            java.lang.String r10 = r14.prefix
            r8[r9] = r10
            java.lang.String[] r8 = r14.elementStack
            int r9 = r7 + 2
            java.lang.String r10 = r14.name
            r8[r9] = r10
            goto L_0x0026
        L_0x0092:
            if (r2 != r10) goto L_0x009a
            if (r15 != 0) goto L_0x009a
            r14.read()
            goto L_0x0036
        L_0x009a:
            r8 = -1
            if (r2 != r8) goto L_0x00a3
            java.lang.String r8 = "Unexpected EOF"
            r14.error(r8)
            goto L_0x0026
        L_0x00a3:
            java.lang.String r0 = r14.readName()
            int r8 = r0.length()
            if (r8 != 0) goto L_0x00b3
            java.lang.String r8 = "attr name expected"
            r14.error(r8)
            goto L_0x0036
        L_0x00b3:
            int r8 = r14.attributeCount
            int r9 = r8 + 1
            r14.attributeCount = r9
            int r4 = r8 << 2
            java.lang.String[] r8 = r14.attributes
            int r9 = r4 + 4
            java.lang.String[] r8 = r14.ensureCapacity(r8, r9)
            r14.attributes = r8
            java.lang.String[] r8 = r14.attributes
            int r5 = r4 + 1
            java.lang.String r9 = ""
            r8[r4] = r9
            java.lang.String[] r8 = r14.attributes
            int r4 = r5 + 1
            r9 = 0
            r8[r5] = r9
            java.lang.String[] r8 = r14.attributes
            int r5 = r4 + 1
            r8[r4] = r0
            r14.skip()
            int r8 = r14.peek(r11)
            if (r8 == r13) goto L_0x00fd
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            java.lang.String r9 = "Attr.value missing f. "
            r8.<init>(r9)
            java.lang.StringBuilder r8 = r8.append(r0)
            java.lang.String r8 = r8.toString()
            r14.error(r8)
            java.lang.String[] r8 = r14.attributes
            java.lang.String r9 = "1"
            r8[r5] = r9
            goto L_0x0013
        L_0x00fd:
            r14.read(r13)
            r14.skip()
            int r3 = r14.peek(r11)
            r8 = 39
            if (r3 == r8) goto L_0x012e
            r8 = 34
            if (r3 == r8) goto L_0x012e
            java.lang.String r8 = "attr value delimiter missing!"
            r14.error(r8)
            r3 = 32
        L_0x0116:
            int r6 = r14.txtPos
            r14.pushText(r3, r12)
            java.lang.String[] r8 = r14.attributes
            java.lang.String r9 = r14.get(r6)
            r8[r5] = r9
            r14.txtPos = r6
            r8 = 32
            if (r3 == r8) goto L_0x0013
            r14.read()
            goto L_0x0013
        L_0x012e:
            r14.read()
            goto L_0x0116
        L_0x0132:
            java.lang.String r8 = ""
            r14.namespace = r8
            goto L_0x007b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.riteshsahu.SMSBackupRestoreBase.KXmlParser.parseStartTag(boolean):void");
    }

    private final void pushEntity() throws IOException, XmlPullParserException {
        boolean z;
        int c;
        push(read());
        int pos = this.txtPos;
        while (true) {
            int c2 = read();
            if (c2 == 59) {
                String code = get(pos);
                this.txtPos = pos - 1;
                if (this.token && this.type == 6) {
                    this.name = code;
                }
                if (code.charAt(0) == '#') {
                    if (code.charAt(1) == 'x') {
                        c = Integer.parseInt(code.substring(2), 16);
                    } else {
                        c = Integer.parseInt(code.substring(1));
                    }
                    push(c);
                    return;
                }
                String result = (String) this.entityMap.get(code);
                if (result == null) {
                    z = true;
                } else {
                    z = false;
                }
                this.unresolved = z;
                if (!this.unresolved) {
                    for (int i = 0; i < result.length(); i++) {
                        push(result.charAt(i));
                    }
                    return;
                } else if (!this.token) {
                    error("unresolved: &" + code + ";");
                    return;
                } else {
                    return;
                }
            } else if (c2 >= 128 || ((c2 >= 48 && c2 <= 57) || ((c2 >= 97 && c2 <= 122) || ((c2 >= 65 && c2 <= 90) || c2 == 95 || c2 == 45 || c2 == 35)))) {
                push(c2);
            } else {
                if (!this.relaxed) {
                    error("unterminated entity ref");
                }
                if (c2 != -1) {
                    push(c2);
                    return;
                }
                return;
            }
        }
    }

    private final void pushText(int delimiter, boolean resolveEntities) throws IOException, XmlPullParserException {
        int next = peek(0);
        int cbrCount = 0;
        while (next != -1 && next != delimiter) {
            if (delimiter != 32 || (next > 32 && next != 62)) {
                if (next == 38) {
                    if (resolveEntities) {
                        pushEntity();
                    } else {
                        return;
                    }
                } else if (next == 10 && this.type == 2) {
                    read();
                    push(32);
                } else {
                    push(read());
                }
                if (next == 62 && cbrCount >= 2 && delimiter != 93) {
                    error("Illegal: ]]>");
                }
                if (next == 93) {
                    cbrCount++;
                } else {
                    cbrCount = 0;
                }
                next = peek(0);
            } else {
                return;
            }
        }
    }

    private final void read(char c) throws IOException, XmlPullParserException {
        int a = read();
        if (a != c) {
            error("expected: '" + c + "' actual: '" + ((char) a) + "'");
        }
    }

    private final int read() throws IOException {
        int result;
        if (this.peekCount == 0) {
            result = peek(0);
        } else {
            result = this.peek[0];
            this.peek[0] = this.peek[1];
        }
        this.peekCount--;
        this.column++;
        if (result == 10) {
            this.line++;
            this.column = 1;
        }
        return result;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    private final int peek(int pos) throws IOException {
        int nw;
        int nw2;
        while (pos >= this.peekCount) {
            if (this.srcBuf.length <= 1) {
                nw2 = this.reader.read();
            } else if (this.srcPos < this.srcCount) {
                char[] cArr = this.srcBuf;
                int i = this.srcPos;
                this.srcPos = i + 1;
                nw2 = cArr[i];
            } else {
                this.srcCount = this.reader.read(this.srcBuf, 0, this.srcBuf.length);
                if (this.srcCount <= 0) {
                    nw = -1;
                } else {
                    nw = this.srcBuf[0];
                }
                this.srcPos = 1;
                nw2 = nw;
            }
            if (nw2 == 13) {
                this.wasCR = true;
                int[] iArr = this.peek;
                int i2 = this.peekCount;
                this.peekCount = i2 + 1;
                iArr[i2] = 10;
            } else {
                if (nw2 != 10) {
                    int[] iArr2 = this.peek;
                    int i3 = this.peekCount;
                    this.peekCount = i3 + 1;
                    iArr2[i3] = nw2;
                } else if (!this.wasCR) {
                    int[] iArr3 = this.peek;
                    int i4 = this.peekCount;
                    this.peekCount = i4 + 1;
                    iArr3[i4] = 10;
                }
                this.wasCR = false;
            }
        }
        return this.peek[pos];
    }

    private final String readName() throws IOException, XmlPullParserException {
        int pos = this.txtPos;
        int c = peek(0);
        if ((c < 97 || c > 122) && ((c < 65 || c > 90) && c != 95 && c != 58 && c < 192 && !this.relaxed)) {
            error("name expected");
        }
        while (true) {
            push(read());
            int c2 = peek(0);
            if ((c2 < 97 || c2 > 122) && ((c2 < 65 || c2 > 90) && !((c2 >= 48 && c2 <= 57) || c2 == 95 || c2 == 45 || c2 == 58 || c2 == 46 || c2 >= 183))) {
                String result = get(pos);
                this.txtPos = pos;
                return result;
            }
        }
    }

    private final void skip() throws IOException {
        while (true) {
            int c = peek(0);
            if (c <= 32 && c != -1) {
                read();
            } else {
                return;
            }
        }
    }

    public void setInput(Reader reader2) throws XmlPullParserException {
        this.reader = reader2;
        this.line = 1;
        this.column = 0;
        this.type = 0;
        this.name = null;
        this.namespace = null;
        this.degenerated = false;
        this.attributeCount = -1;
        this.encoding = null;
        this.version = null;
        this.standalone = null;
        if (reader2 != null) {
            this.srcPos = 0;
            this.srcCount = 0;
            this.peekCount = 0;
            this.depth = 0;
            this.entityMap = new Hashtable();
            this.entityMap.put("amp", "&");
            this.entityMap.put("apos", "'");
            this.entityMap.put("gt", ">");
            this.entityMap.put("lt", "<");
            this.entityMap.put("quot", "\"");
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0033 A[Catch:{ Exception -> 0x00a9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x01a6 A[Catch:{ Exception -> 0x00a9 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setInput(java.io.InputStream r20, java.lang.String r21) throws org.xmlpull.v1.XmlPullParserException {
        /*
            r19 = this;
            r14 = 0
            r0 = r14
            r1 = r19
            r1.srcPos = r0
            r14 = 0
            r0 = r14
            r1 = r19
            r1.srcCount = r0
            r7 = r21
            if (r20 != 0) goto L_0x0016
            java.lang.IllegalArgumentException r14 = new java.lang.IllegalArgumentException
            r14.<init>()
            throw r14
        L_0x0016:
            if (r7 != 0) goto L_0x0060
            r4 = 0
        L_0x0019:
            r0 = r19
            int r0 = r0.srcCount     // Catch:{ Exception -> 0x00a9 }
            r14 = r0
            r15 = 4
            if (r14 < r15) goto L_0x0084
        L_0x0021:
            r0 = r19
            int r0 = r0.srcCount     // Catch:{ Exception -> 0x00a9 }
            r14 = r0
            r15 = 4
            if (r14 != r15) goto L_0x0060
            switch(r4) {
                case -131072: goto L_0x00d2;
                case 60: goto L_0x00db;
                case 65279: goto L_0x00c9;
                case 3932223: goto L_0x0103;
                case 1006632960: goto L_0x00ef;
                case 1006649088: goto L_0x0121;
                case 1010792557: goto L_0x013f;
                default: goto L_0x002c;
            }     // Catch:{ Exception -> 0x00a9 }
        L_0x002c:
            r14 = -65536(0xffffffffffff0000, float:NaN)
            r14 = r14 & r4
            r15 = -16842752(0xfffffffffeff0000, float:-1.6947657E38)
            if (r14 != r15) goto L_0x01a6
            java.lang.String r7 = "UTF-16BE"
            r0 = r19
            char[] r0 = r0.srcBuf     // Catch:{ Exception -> 0x00a9 }
            r14 = r0
            r15 = 0
            r0 = r19
            char[] r0 = r0.srcBuf     // Catch:{ Exception -> 0x00a9 }
            r16 = r0
            r17 = 2
            char r16 = r16[r17]     // Catch:{ Exception -> 0x00a9 }
            int r16 = r16 << 8
            r0 = r19
            char[] r0 = r0.srcBuf     // Catch:{ Exception -> 0x00a9 }
            r17 = r0
            r18 = 3
            char r17 = r17[r18]     // Catch:{ Exception -> 0x00a9 }
            r16 = r16 | r17
            r0 = r16
            char r0 = (char) r0     // Catch:{ Exception -> 0x00a9 }
            r16 = r0
            r14[r15] = r16     // Catch:{ Exception -> 0x00a9 }
            r14 = 1
            r0 = r14
            r1 = r19
            r1.srcCount = r0     // Catch:{ Exception -> 0x00a9 }
        L_0x0060:
            if (r7 != 0) goto L_0x0064
            java.lang.String r7 = "UTF-8"
        L_0x0064:
            r0 = r19
            int r0 = r0.srcCount     // Catch:{ Exception -> 0x00a9 }
            r13 = r0
            java.io.InputStreamReader r14 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x00a9 }
            r0 = r14
            r1 = r20
            r2 = r7
            r0.<init>(r1, r2)     // Catch:{ Exception -> 0x00a9 }
            r0 = r19
            r1 = r14
            r0.setInput(r1)     // Catch:{ Exception -> 0x00a9 }
            r0 = r21
            r1 = r19
            r1.encoding = r0     // Catch:{ Exception -> 0x00a9 }
            r0 = r13
            r1 = r19
            r1.srcCount = r0     // Catch:{ Exception -> 0x00a9 }
            return
        L_0x0084:
            int r8 = r20.read()     // Catch:{ Exception -> 0x00a9 }
            r14 = -1
            if (r8 == r14) goto L_0x0021
            int r14 = r4 << 8
            r4 = r14 | r8
            r0 = r19
            char[] r0 = r0.srcBuf     // Catch:{ Exception -> 0x00a9 }
            r14 = r0
            r0 = r19
            int r0 = r0.srcCount     // Catch:{ Exception -> 0x00a9 }
            r15 = r0
            int r16 = r15 + 1
            r0 = r16
            r1 = r19
            r1.srcCount = r0     // Catch:{ Exception -> 0x00a9 }
            r0 = r8
            char r0 = (char) r0     // Catch:{ Exception -> 0x00a9 }
            r16 = r0
            r14[r15] = r16     // Catch:{ Exception -> 0x00a9 }
            goto L_0x0019
        L_0x00a9:
            r14 = move-exception
            r6 = r14
            org.xmlpull.v1.XmlPullParserException r14 = new org.xmlpull.v1.XmlPullParserException
            java.lang.StringBuilder r15 = new java.lang.StringBuilder
            java.lang.String r16 = "Invalid stream or encoding: "
            r15.<init>(r16)
            java.lang.String r16 = r6.toString()
            java.lang.StringBuilder r15 = r15.append(r16)
            java.lang.String r15 = r15.toString()
            r0 = r14
            r1 = r15
            r2 = r19
            r3 = r6
            r0.<init>(r1, r2, r3)
            throw r14
        L_0x00c9:
            java.lang.String r7 = "UTF-32BE"
            r14 = 0
            r0 = r14
            r1 = r19
            r1.srcCount = r0     // Catch:{ Exception -> 0x00a9 }
            goto L_0x0060
        L_0x00d2:
            java.lang.String r7 = "UTF-32LE"
            r14 = 0
            r0 = r14
            r1 = r19
            r1.srcCount = r0     // Catch:{ Exception -> 0x00a9 }
            goto L_0x0060
        L_0x00db:
            java.lang.String r7 = "UTF-32BE"
            r0 = r19
            char[] r0 = r0.srcBuf     // Catch:{ Exception -> 0x00a9 }
            r14 = r0
            r15 = 0
            r16 = 60
            r14[r15] = r16     // Catch:{ Exception -> 0x00a9 }
            r14 = 1
            r0 = r14
            r1 = r19
            r1.srcCount = r0     // Catch:{ Exception -> 0x00a9 }
            goto L_0x0060
        L_0x00ef:
            java.lang.String r7 = "UTF-32LE"
            r0 = r19
            char[] r0 = r0.srcBuf     // Catch:{ Exception -> 0x00a9 }
            r14 = r0
            r15 = 0
            r16 = 60
            r14[r15] = r16     // Catch:{ Exception -> 0x00a9 }
            r14 = 1
            r0 = r14
            r1 = r19
            r1.srcCount = r0     // Catch:{ Exception -> 0x00a9 }
            goto L_0x0060
        L_0x0103:
            java.lang.String r7 = "UTF-16BE"
            r0 = r19
            char[] r0 = r0.srcBuf     // Catch:{ Exception -> 0x00a9 }
            r14 = r0
            r15 = 0
            r16 = 60
            r14[r15] = r16     // Catch:{ Exception -> 0x00a9 }
            r0 = r19
            char[] r0 = r0.srcBuf     // Catch:{ Exception -> 0x00a9 }
            r14 = r0
            r15 = 1
            r16 = 63
            r14[r15] = r16     // Catch:{ Exception -> 0x00a9 }
            r14 = 2
            r0 = r14
            r1 = r19
            r1.srcCount = r0     // Catch:{ Exception -> 0x00a9 }
            goto L_0x0060
        L_0x0121:
            java.lang.String r7 = "UTF-16LE"
            r0 = r19
            char[] r0 = r0.srcBuf     // Catch:{ Exception -> 0x00a9 }
            r14 = r0
            r15 = 0
            r16 = 60
            r14[r15] = r16     // Catch:{ Exception -> 0x00a9 }
            r0 = r19
            char[] r0 = r0.srcBuf     // Catch:{ Exception -> 0x00a9 }
            r14 = r0
            r15 = 1
            r16 = 63
            r14[r15] = r16     // Catch:{ Exception -> 0x00a9 }
            r14 = 2
            r0 = r14
            r1 = r19
            r1.srcCount = r0     // Catch:{ Exception -> 0x00a9 }
            goto L_0x0060
        L_0x013f:
            int r8 = r20.read()     // Catch:{ Exception -> 0x00a9 }
            r14 = -1
            if (r8 == r14) goto L_0x002c
            r0 = r19
            char[] r0 = r0.srcBuf     // Catch:{ Exception -> 0x00a9 }
            r14 = r0
            r0 = r19
            int r0 = r0.srcCount     // Catch:{ Exception -> 0x00a9 }
            r15 = r0
            int r16 = r15 + 1
            r0 = r16
            r1 = r19
            r1.srcCount = r0     // Catch:{ Exception -> 0x00a9 }
            r0 = r8
            char r0 = (char) r0     // Catch:{ Exception -> 0x00a9 }
            r16 = r0
            r14[r15] = r16     // Catch:{ Exception -> 0x00a9 }
            r14 = 62
            if (r8 != r14) goto L_0x013f
            java.lang.String r12 = new java.lang.String     // Catch:{ Exception -> 0x00a9 }
            r0 = r19
            char[] r0 = r0.srcBuf     // Catch:{ Exception -> 0x00a9 }
            r14 = r0
            r15 = 0
            r0 = r19
            int r0 = r0.srcCount     // Catch:{ Exception -> 0x00a9 }
            r16 = r0
            r0 = r12
            r1 = r14
            r2 = r15
            r3 = r16
            r0.<init>(r1, r2, r3)     // Catch:{ Exception -> 0x00a9 }
            java.lang.String r14 = "encoding"
            int r9 = r12.indexOf(r14)     // Catch:{ Exception -> 0x00a9 }
            r14 = -1
            if (r9 == r14) goto L_0x002c
            r10 = r9
        L_0x0182:
            char r14 = r12.charAt(r10)     // Catch:{ Exception -> 0x00a9 }
            r15 = 34
            if (r14 == r15) goto L_0x0192
            char r14 = r12.charAt(r10)     // Catch:{ Exception -> 0x00a9 }
            r15 = 39
            if (r14 != r15) goto L_0x01a2
        L_0x0192:
            int r9 = r10 + 1
            char r5 = r12.charAt(r10)     // Catch:{ Exception -> 0x00a9 }
            int r11 = r12.indexOf(r5, r9)     // Catch:{ Exception -> 0x00a9 }
            java.lang.String r7 = r12.substring(r9, r11)     // Catch:{ Exception -> 0x00a9 }
            goto L_0x002c
        L_0x01a2:
            int r9 = r10 + 1
            r10 = r9
            goto L_0x0182
        L_0x01a6:
            r14 = -65536(0xffffffffffff0000, float:NaN)
            r14 = r14 & r4
            r15 = -131072(0xfffffffffffe0000, float:NaN)
            if (r14 != r15) goto L_0x01dc
            java.lang.String r7 = "UTF-16LE"
            r0 = r19
            char[] r0 = r0.srcBuf     // Catch:{ Exception -> 0x00a9 }
            r14 = r0
            r15 = 0
            r0 = r19
            char[] r0 = r0.srcBuf     // Catch:{ Exception -> 0x00a9 }
            r16 = r0
            r17 = 3
            char r16 = r16[r17]     // Catch:{ Exception -> 0x00a9 }
            int r16 = r16 << 8
            r0 = r19
            char[] r0 = r0.srcBuf     // Catch:{ Exception -> 0x00a9 }
            r17 = r0
            r18 = 2
            char r17 = r17[r18]     // Catch:{ Exception -> 0x00a9 }
            r16 = r16 | r17
            r0 = r16
            char r0 = (char) r0     // Catch:{ Exception -> 0x00a9 }
            r16 = r0
            r14[r15] = r16     // Catch:{ Exception -> 0x00a9 }
            r14 = 1
            r0 = r14
            r1 = r19
            r1.srcCount = r0     // Catch:{ Exception -> 0x00a9 }
            goto L_0x0060
        L_0x01dc:
            r14 = r4 & -256(0xffffffffffffff00, float:NaN)
            r15 = -272908544(0xffffffffefbbbf00, float:-1.162092E29)
            if (r14 != r15) goto L_0x0060
            java.lang.String r7 = "UTF-8"
            r0 = r19
            char[] r0 = r0.srcBuf     // Catch:{ Exception -> 0x00a9 }
            r14 = r0
            r15 = 0
            r0 = r19
            char[] r0 = r0.srcBuf     // Catch:{ Exception -> 0x00a9 }
            r16 = r0
            r17 = 3
            char r16 = r16[r17]     // Catch:{ Exception -> 0x00a9 }
            r14[r15] = r16     // Catch:{ Exception -> 0x00a9 }
            r14 = 1
            r0 = r14
            r1 = r19
            r1.srcCount = r0     // Catch:{ Exception -> 0x00a9 }
            goto L_0x0060
        */
        throw new UnsupportedOperationException("Method not decompiled: com.riteshsahu.SMSBackupRestoreBase.KXmlParser.setInput(java.io.InputStream, java.lang.String):void");
    }

    public boolean getFeature(String feature) {
        if ("http://xmlpull.org/v1/doc/features.html#process-namespaces".equals(feature)) {
            return this.processNsp;
        }
        if (isProp(feature, false, "relaxed")) {
            return this.relaxed;
        }
        return false;
    }

    public String getInputEncoding() {
        return this.encoding;
    }

    public void defineEntityReplacementText(String entity, String value) throws XmlPullParserException {
        if (this.entityMap == null) {
            throw new RuntimeException("entity replacement text must be defined after setInput!");
        }
        this.entityMap.put(entity, value);
    }

    public Object getProperty(String property) {
        if (isProp(property, true, "xmldecl-version")) {
            return this.version;
        }
        if (isProp(property, true, "xmldecl-standalone")) {
            return this.standalone;
        }
        if (isProp(property, true, "location")) {
            return this.location != null ? this.location : this.reader.toString();
        }
        return null;
    }

    public int getNamespaceCount(int depth2) {
        if (depth2 <= this.depth) {
            return this.nspCounts[depth2];
        }
        throw new IndexOutOfBoundsException();
    }

    public String getNamespacePrefix(int pos) {
        return this.nspStack[pos << 1];
    }

    public String getNamespaceUri(int pos) {
        return this.nspStack[(pos << 1) + 1];
    }

    public String getNamespace(String prefix2) {
        if ("xml".equals(prefix2)) {
            return "http://www.w3.org/XML/1998/namespace";
        }
        if ("xmlns".equals(prefix2)) {
            return "http://www.w3.org/2000/xmlns/";
        }
        for (int i = (getNamespaceCount(this.depth) << 1) - 2; i >= 0; i -= 2) {
            if (prefix2 == null) {
                if (this.nspStack[i] == null) {
                    return this.nspStack[i + 1];
                }
            } else if (prefix2.equals(this.nspStack[i])) {
                return this.nspStack[i + 1];
            }
        }
        return null;
    }

    public int getDepth() {
        return this.depth;
    }

    public String getPositionDescription() {
        StringBuffer buf = new StringBuffer(this.type < TYPES.length ? TYPES[this.type] : "unknown");
        buf.append(' ');
        if (this.type == 2 || this.type == 3) {
            if (this.degenerated) {
                buf.append("(empty) ");
            }
            buf.append('<');
            if (this.type == 3) {
                buf.append('/');
            }
            if (this.prefix != null) {
                buf.append("{" + this.namespace + "}" + this.prefix + ":");
            }
            buf.append(this.name);
            int cnt = this.attributeCount << 2;
            for (int i = 0; i < cnt; i += 4) {
                buf.append(' ');
                if (this.attributes[i + 1] != null) {
                    buf.append("{" + this.attributes[i] + "}" + this.attributes[i + 1] + ":");
                }
                buf.append(String.valueOf(this.attributes[i + 2]) + "='" + this.attributes[i + 3] + "'");
            }
            buf.append('>');
        } else if (this.type != 7) {
            if (this.type != 4) {
                buf.append(getText());
            } else if (this.isWhitespace) {
                buf.append("(whitespace)");
            } else {
                String text = getText();
                if (text.length() > 16) {
                    text = String.valueOf(text.substring(0, 16)) + "...";
                }
                buf.append(text);
            }
        }
        buf.append("@" + this.line + ":" + this.column);
        if (this.location != null) {
            buf.append(" in ");
            buf.append(this.location);
        } else if (this.reader != null) {
            buf.append(" in ");
            buf.append(this.reader.toString());
        }
        return buf.toString();
    }

    public int getLineNumber() {
        return this.line;
    }

    public int getColumnNumber() {
        return this.column;
    }

    public boolean isWhitespace() throws XmlPullParserException {
        if (!(this.type == 4 || this.type == 7 || this.type == 5)) {
            exception(ILLEGAL_TYPE);
        }
        return this.isWhitespace;
    }

    public String getText() {
        if (this.type < 4 || (this.type == 6 && this.unresolved)) {
            return null;
        }
        return get(0);
    }

    public char[] getTextCharacters(int[] poslen) {
        if (this.type < 4) {
            poslen[0] = -1;
            poslen[1] = -1;
            return null;
        } else if (this.type == 6) {
            poslen[0] = 0;
            poslen[1] = this.name.length();
            return this.name.toCharArray();
        } else {
            poslen[0] = 0;
            poslen[1] = this.txtPos;
            return this.txtBuf;
        }
    }

    public String getNamespace() {
        return this.namespace;
    }

    public String getName() {
        return this.name;
    }

    public String getPrefix() {
        return this.prefix;
    }

    public boolean isEmptyElementTag() throws XmlPullParserException {
        if (this.type != 2) {
            exception(ILLEGAL_TYPE);
        }
        return this.degenerated;
    }

    public int getAttributeCount() {
        return this.attributeCount;
    }

    public String getAttributeType(int index) {
        return "CDATA";
    }

    public boolean isAttributeDefault(int index) {
        return false;
    }

    public String getAttributeNamespace(int index) {
        if (index < this.attributeCount) {
            return this.attributes[index << 2];
        }
        throw new IndexOutOfBoundsException();
    }

    public String getAttributeName(int index) {
        if (index < this.attributeCount) {
            return this.attributes[(index << 2) + 2];
        }
        throw new IndexOutOfBoundsException();
    }

    public String getAttributePrefix(int index) {
        if (index < this.attributeCount) {
            return this.attributes[(index << 2) + 1];
        }
        throw new IndexOutOfBoundsException();
    }

    public String getAttributeValue(int index) {
        if (index < this.attributeCount) {
            return this.attributes[(index << 2) + 3];
        }
        throw new IndexOutOfBoundsException();
    }

    public String getAttributeValue(String namespace2, String name2) {
        for (int i = (this.attributeCount << 2) - 4; i >= 0; i -= 4) {
            if (this.attributes[i + 2].equals(name2) && (namespace2 == null || this.attributes[i].equals(namespace2))) {
                return this.attributes[i + 3];
            }
        }
        return null;
    }

    public int getEventType() throws XmlPullParserException {
        return this.type;
    }

    public int next() throws XmlPullParserException, IOException {
        this.txtPos = 0;
        this.isWhitespace = true;
        int minType = 9999;
        this.token = false;
        while (true) {
            nextImpl();
            if (this.type < minType) {
                minType = this.type;
            }
            if (minType > 6 || (minType >= 4 && peekType() >= 4)) {
            }
        }
        this.type = minType;
        if (this.type > 4) {
            this.type = 4;
        }
        return this.type;
    }

    public int nextToken() throws XmlPullParserException, IOException {
        this.isWhitespace = true;
        this.txtPos = 0;
        this.token = true;
        nextImpl();
        return this.type;
    }

    public int nextTag() throws XmlPullParserException, IOException {
        next();
        if (this.type == 4 && this.isWhitespace) {
            next();
        }
        if (!(this.type == 3 || this.type == 2)) {
            exception("unexpected type");
        }
        return this.type;
    }

    public void require(int type2, String namespace2, String name2) throws XmlPullParserException, IOException {
        if (type2 != this.type || ((namespace2 != null && !namespace2.equals(getNamespace())) || (name2 != null && !name2.equals(getName())))) {
            exception("expected: " + TYPES[type2] + " {" + namespace2 + "}" + name2);
        }
    }

    public String nextText() throws XmlPullParserException, IOException {
        String result;
        if (this.type != 2) {
            exception("precondition: START_TAG");
        }
        next();
        if (this.type == 4) {
            result = getText();
            next();
        } else {
            result = "";
        }
        if (this.type != 3) {
            exception("END_TAG expected");
        }
        return result;
    }

    public void setFeature(String feature, boolean value) throws XmlPullParserException {
        if ("http://xmlpull.org/v1/doc/features.html#process-namespaces".equals(feature)) {
            this.processNsp = value;
        } else if (isProp(feature, false, "relaxed")) {
            this.relaxed = value;
        } else {
            exception("unsupported feature: " + feature);
        }
    }

    public void setProperty(String property, Object value) throws XmlPullParserException {
        if (isProp(property, true, "location")) {
            this.location = value;
            return;
        }
        throw new XmlPullParserException("unsupported property: " + property);
    }

    public void skipSubTree() throws XmlPullParserException, IOException {
        require(2, null, null);
        int level = 1;
        while (level > 0) {
            int eventType = next();
            if (eventType == 3) {
                level--;
            } else if (eventType == 2) {
                level++;
            }
        }
    }
}
