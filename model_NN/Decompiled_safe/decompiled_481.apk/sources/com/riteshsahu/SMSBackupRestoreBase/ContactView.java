package com.riteshsahu.SMSBackupRestoreBase;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.riteshsahu.SMSBackupRestore.R;
import java.util.ArrayList;
import java.util.Collections;

public class ContactView extends ListActivity {
    /* access modifiers changed from: private */
    public static String mErrorMessage;
    final ActivityHelper mActivityHelper = ActivityHelper.createInstance(this);
    /* access modifiers changed from: private */
    public ContactAdapter mAdapter;
    private final View.OnCreateContextMenuListener mContactListOnCreateContextMenuListener = new View.OnCreateContextMenuListener() {
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            if (((AdapterView.AdapterContextMenuInfo) menuInfo).position > -1) {
                menu.setHeaderTitle((int) R.string.conversation_options);
                menu.add(0, (int) R.string.menu_restore_conversation, 0, (int) R.string.menu_restore_conversation);
            }
        }
    };
    /* access modifiers changed from: private */
    public ArrayList<ContactNumbers> mContacts = null;
    /* access modifiers changed from: private */
    public ProgressDialog mProgressDialog = null;
    private Runnable returnRes = new Runnable() {
        public void run() {
            if (ContactView.mErrorMessage == null || ContactView.mErrorMessage.length() == 0) {
                if (ContactView.this.mContacts != null && ContactView.this.mContacts.size() > 0) {
                    Collections.sort(ContactView.this.mContacts);
                    for (int i = 0; i < ContactView.this.mContacts.size(); i++) {
                        ContactView.this.mAdapter.add((ContactNumbers) ContactView.this.mContacts.get(i));
                    }
                    ContactView.this.mAdapter.notifyDataSetChanged();
                }
                ContactView.this.mProgressDialog.dismiss();
                ContactView.this.mAdapter.notifyDataSetChanged();
                return;
            }
            new AlertDialog.Builder(ContactView.this).setIcon((int) R.drawable.icon).setTitle(ContactView.this.getText(R.string.app_name)).setMessage(String.format(ContactView.this.getString(R.string.error_message), ContactView.mErrorMessage)).setPositiveButton((int) R.string.button_close, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    ContactView.this.finish();
                }
            }).create().show();
            ContactView.this.mProgressDialog.dismiss();
        }
    };
    private Runnable viewContacts;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mErrorMessage = "";
        setContentView((int) R.layout.contact_list);
        this.mActivityHelper.setupActionBar(getString(R.string.select_number), 0);
        addAdditionalControls();
        this.mContacts = new ArrayList<>();
        this.mAdapter = new ContactAdapter(this, R.layout.contact_row, this.mContacts);
        setListAdapter(this.mAdapter);
        this.viewContacts = new Runnable() {
            public void run() {
                ContactView.this.getContacts();
            }
        };
        new Thread(null, this.viewContacts, "ContactViewBackground").start();
        this.mProgressDialog = ProgressDialog.show(this, getString(R.string.please_wait), getString(R.string.analysing_file), true);
        getListView().setOnCreateContextMenuListener(this.mContactListOnCreateContextMenuListener);
    }

    /* access modifiers changed from: protected */
    public void addAdditionalControls() {
    }

    public boolean onContextItemSelected(MenuItem item) {
        try {
            ContactNumbers contactNumbers = (ContactNumbers) getListView().getItemAtPosition(((AdapterView.AdapterContextMenuInfo) item.getMenuInfo()).position);
            switch (item.getItemId()) {
                case R.string.menu_restore_conversation:
                    new TaskRunner().performAction(this, ActionMode.RestoreConversation, contactNumbers, true, false);
                    break;
            }
            return super.onContextItemSelected(item);
        } catch (ClassCastException e) {
            Common.logError("Bad menuInfo", e);
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        openMessages((ContactNumbers) getListView().getItemAtPosition(position));
    }

    /* access modifiers changed from: private */
    public void getContacts() {
        try {
            this.mContacts = (ArrayList) SmsRestoreProcessor.getContactList(this, Common.getCurrentBackupFile());
            Common.logDebug("Contacts loaded: " + this.mContacts.size());
        } catch (Exception e) {
            mErrorMessage = e.getMessage();
        }
        runOnUiThread(this.returnRes);
    }

    private class ContactAdapter extends ArrayAdapter<ContactNumbers> {
        private ArrayList<ContactNumbers> items;

        public ContactAdapter(Context context, int textViewResourceId, ArrayList<ContactNumbers> items2) {
            super(context, textViewResourceId, items2);
            this.items = items2;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            if (v == null) {
                v = ((LayoutInflater) ContactView.this.getSystemService("layout_inflater")).inflate((int) R.layout.contact_row, (ViewGroup) null);
            }
            ContactNumbers contactNumbers = this.items.get(position);
            if (contactNumbers != null) {
                ((TextView) v.findViewById(R.id.contact_message)).setText(contactNumbers.getBody());
                ((TextView) v.findViewById(R.id.contact_name)).setText(contactNumbers.getNameAndNumber());
                ((TextView) v.findViewById(R.id.contact_message_count)).setText(new StringBuilder(String.valueOf(contactNumbers.getCount())).toString());
            }
            return v;
        }
    }

    private void openMessages(ContactNumbers contactNumbers) {
        MessageView.setAddressFilter(contactNumbers);
        openMessageView();
    }

    /* access modifiers changed from: protected */
    public void openMessageView() {
        try {
            startActivity(new Intent(this, MessageView.class));
        } catch (Exception e) {
            Log.e("", e.getMessage());
        }
    }
}
