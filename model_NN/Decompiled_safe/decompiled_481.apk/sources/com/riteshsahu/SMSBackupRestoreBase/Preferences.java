package com.riteshsahu.SMSBackupRestoreBase;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.widget.EditText;
import com.riteshsahu.SMSBackupRestore.R;

public class Preferences extends PreferenceActivity {
    static final int FolderRequestCode = 1;
    final ActivityHelper mActivityHelper = ActivityHelper.createInstance(this);

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.layout.preferences);
        setContentView((int) R.layout.preferences_wrapper);
        this.mActivityHelper.setupActionBar(getString(R.string.preferences), 0);
        Preference backupFolder = findPreference(PreferenceKeys.BackupFolder);
        backupFolder.setSummary(String.format(getString(R.string.backup_folder_description, new Object[]{Common.getStringPreference(this, PreferenceKeys.BackupFolder)}), new Object[0]));
        backupFolder.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                if (!preference.getKey().equalsIgnoreCase(PreferenceKeys.BackupFolder)) {
                    return false;
                }
                Context context = preference.getContext();
                Preferences.this.showBackupFolderDialog(context, Common.getStringPreference(context, PreferenceKeys.BackupFolder), preference);
                return true;
            }
        });
        findPreference(PreferenceKeys.Disclaimer).setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference pref) {
                if (!pref.getKey().equalsIgnoreCase(PreferenceKeys.Disclaimer)) {
                    return false;
                }
                Common.showDisclaimer(pref.getContext(), false, null);
                return true;
            }
        });
        addAdditionalPreferences();
    }

    /* access modifiers changed from: private */
    public void showBackupFolderDialog(final Context context, final String textToDisplay, final Preference preference) {
        AlertDialog alert = new CustomDialog(context, R.style.ClassicDialog);
        alert.setTitle((int) R.string.pref_backup_folder);
        alert.setMessage(getText(R.string.backup_folder_dialog_message));
        alert.setIcon((int) R.drawable.icon);
        final EditText input = new EditText(context);
        input.setText(textToDisplay);
        input.setMinLines(2);
        alert.setView(input);
        alert.setButton(-1, getText(R.string.button_ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                Common.setStringPreference(context, PreferenceKeys.BackupFolder, input.getText().toString());
                preference.setSummary(String.format(Preferences.this.getString(R.string.backup_folder_description, new Object[]{input.getText().toString()}), new Object[0]));
            }
        });
        alert.setButton(-2, getText(R.string.button_browse), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                Preferences.this.browseFolder(textToDisplay);
            }
        });
        alert.setButton(-3, getText(R.string.button_default), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                Preferences.this.showBackupFolderDialog(context, Common.getDefaultBackupFolder(), preference);
            }
        });
        alert.show();
    }

    /* access modifiers changed from: private */
    public void browseFolder(final String initialFolder) {
        Intent intent = new Intent("com.estrongs.action.PICK_DIRECTORY");
        Uri startFolder = Uri.parse("file://" + initialFolder);
        intent.setData(startFolder);
        intent.putExtra("com.estrongs.intent.extra.BUTTON_TITLE", getString(R.string.use_folder));
        intent.putExtra("android.intent.extra.TITLE", getString(R.string.select_a_folder));
        intent.addFlags(8388608);
        try {
            startActivityForResult(intent, 1);
        } catch (ActivityNotFoundException e) {
            intent.setAction("org.openintents.action.PICK_DIRECTORY");
            intent.putExtra("org.openintents.extra.BUTTON_TEXT", getString(R.string.use_folder));
            try {
                startActivityForResult(intent, 1);
            } catch (ActivityNotFoundException e2) {
                intent.setAction("android.intent.action.PICK");
                intent.setDataAndType(startFolder, "vnd.android.cursor.dir/lysesoft.andexplorer.directory");
                intent.putExtra("explorer_title", getString(R.string.select_a_folder));
                try {
                    startActivityForResult(intent, 1);
                } catch (ActivityNotFoundException e3) {
                    new AlertDialog.Builder(this).setIcon((int) R.drawable.icon).setTitle(getText(R.string.app_name)).setMessage((int) R.string.browser_app_not_found).setPositiveButton((int) R.string.button_ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            Preferences.this.showBackupFolderDialog(Preferences.this, initialFolder, Preferences.this.findPreference(PreferenceKeys.BackupFolder));
                        }
                    }).create().show();
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == 1 && resultCode == -1 && intent.getData() != null) {
            showBackupFolderDialog(this, intent.getData().getPath(), findPreference(PreferenceKeys.BackupFolder));
        }
    }

    /* access modifiers changed from: protected */
    public void addAdditionalPreferences() {
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        Common.setLoggingEnabled(Common.getBooleanPreference(this, PreferenceKeys.EnableLogging).booleanValue());
    }
}
