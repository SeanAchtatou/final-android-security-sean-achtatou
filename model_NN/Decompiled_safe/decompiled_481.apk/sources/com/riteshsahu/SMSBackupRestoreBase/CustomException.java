package com.riteshsahu.SMSBackupRestoreBase;

public class CustomException extends Exception {
    private static final long serialVersionUID = 1167341606425922350L;

    public CustomException(String detailMessage) {
        super(detailMessage);
    }
}
