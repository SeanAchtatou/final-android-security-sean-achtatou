package com.riteshsahu.SMSBackupRestoreBase;

public class OperationResult {
    private int mFailed;
    private String mMessage;
    private int mSuccessful;
    private int mTotal;

    public int getFailed() {
        return this.mFailed;
    }

    public String getMessage() {
        return this.mMessage;
    }

    public int getSuccessful() {
        return this.mSuccessful;
    }

    public int getTotal() {
        return this.mTotal;
    }

    public void setFailed(int failed) {
        this.mFailed = failed;
    }

    public void setMessage(String message) {
        this.mMessage = message;
    }

    public void setSuccessful(int successful) {
        this.mSuccessful = successful;
    }

    public void setTotal(int total) {
        this.mTotal = total;
    }
}
