package com.riteshsahu.SMSBackupRestoreBase;

import java.util.Comparator;

public class ContactIdComparer implements Comparator<Contact> {
    public int compare(Contact first, Contact second) {
        return first.getId().compareTo(second.getId());
    }
}
