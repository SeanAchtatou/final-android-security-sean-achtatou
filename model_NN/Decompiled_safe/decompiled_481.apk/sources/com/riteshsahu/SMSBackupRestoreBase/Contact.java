package com.riteshsahu.SMSBackupRestoreBase;

public class Contact implements Comparable<Contact> {
    private String mAddress;
    private String mBody;
    private int mCount;
    private long mDate;
    private String mId;
    private String mName;

    public Contact() {
    }

    public Contact(String address) {
        this.mAddress = address;
    }

    public Contact(String name, String address, String body) {
        this.mName = name;
        this.mAddress = address;
        this.mBody = body;
    }

    public int compareTo(Contact other) {
        return this.mAddress.compareToIgnoreCase(other.mAddress);
    }

    public String getAddress() {
        return this.mAddress;
    }

    public int getCount() {
        return this.mCount;
    }

    public Long getDate() {
        return Long.valueOf(this.mDate);
    }

    public String getId() {
        return this.mId;
    }

    public String getName() {
        return this.mName;
    }

    public String getNameOrAddress() {
        if (this.mName == null || this.mName.length() == 0) {
            return this.mAddress;
        }
        return this.mName;
    }

    public void updateDateAndIncrementCount(long date, String body) {
        if (this.mDate < date) {
            this.mDate = date;
            this.mBody = body;
        }
        this.mCount++;
    }

    public void setAddress(String address) {
        this.mAddress = address;
    }

    public void setCount(int count) {
        this.mCount = count;
    }

    public void setDate(long date) {
        this.mDate = date;
    }

    public void setId(String id) {
        this.mId = id;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public void setBody(String body) {
        this.mBody = body;
    }

    public String getBody() {
        return this.mBody;
    }
}
