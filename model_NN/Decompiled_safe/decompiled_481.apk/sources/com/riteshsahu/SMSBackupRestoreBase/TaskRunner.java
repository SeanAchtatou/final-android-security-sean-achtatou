package com.riteshsahu.SMSBackupRestoreBase;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import com.riteshsahu.SMSBackupRestore.R;

public class TaskRunner implements Runnable {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$riteshsahu$SMSBackupRestoreBase$ActionMode;
    /* access modifiers changed from: private */
    public ActionMode mActionMode;
    private boolean mCheckDuplicatesDuringRestore;
    /* access modifiers changed from: private */
    public boolean mCloseActivityWhenDone;
    private ContactNumbers mContactNumbers;
    /* access modifiers changed from: private */
    public Activity mContextActivity;
    private Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            if (!TaskRunner.this.mContextActivity.isFinishing()) {
                if (msg.what == 0) {
                    if (!TaskRunner.this.mSuccess) {
                        new AlertDialog.Builder(TaskRunner.this.mContextActivity).setIcon((int) R.drawable.icon).setTitle(TaskRunner.this.mContextActivity.getText(R.string.app_name)).setMessage(TaskRunner.this.mResult).setPositiveButton((int) R.string.button_ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                            }
                        }).setNegativeButton((int) R.string.button_help, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                Common.openHelp(TaskRunner.this.mContextActivity);
                            }
                        }).create().show();
                    } else if (TaskRunner.this.mActionMode == ActionMode.Restore) {
                        new AlertDialog.Builder(TaskRunner.this.mContextActivity).setIcon((int) R.drawable.icon).setTitle(TaskRunner.this.mContextActivity.getText(R.string.app_name)).setMessage(TaskRunner.this.mResult).setPositiveButton((int) R.string.button_close, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                TaskRunner.this.askForChangingBackupFolder();
                            }
                        }).create().show();
                    } else {
                        new AlertDialog.Builder(TaskRunner.this.mContextActivity).setIcon((int) R.drawable.icon).setTitle(TaskRunner.this.mContextActivity.getText(R.string.app_name)).setMessage(TaskRunner.this.mResult).setPositiveButton((int) R.string.button_close, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                if (TaskRunner.this.mCloseActivityWhenDone) {
                                    TaskRunner.this.mContextActivity.finish();
                                }
                            }
                        }).create().show();
                    }
                    if (!Common.getBooleanPreference(TaskRunner.this.mContextActivity, PreferenceKeys.DisableVibration).booleanValue()) {
                        ((Vibrator) TaskRunner.this.mContextActivity.getSystemService("vibrator")).vibrate(100);
                    }
                    TaskRunner.this.mActionMode = ActionMode.None;
                    return;
                }
                TaskRunner.this.mProgressDialog.setMessage(TaskRunner.this.mContextActivity.getString(msg.what));
            }
        }
    };
    /* access modifiers changed from: private */
    public ProgressDialog mProgressDialog;
    /* access modifiers changed from: private */
    public String mResult;
    /* access modifiers changed from: private */
    public boolean mSuccess;

    static /* synthetic */ int[] $SWITCH_TABLE$com$riteshsahu$SMSBackupRestoreBase$ActionMode() {
        int[] iArr = $SWITCH_TABLE$com$riteshsahu$SMSBackupRestoreBase$ActionMode;
        if (iArr == null) {
            iArr = new int[ActionMode.values().length];
            try {
                iArr[ActionMode.Backup.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[ActionMode.Delete.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[ActionMode.DeleteBackups.ordinal()] = 5;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[ActionMode.None.ordinal()] = 1;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[ActionMode.Restore.ordinal()] = 4;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[ActionMode.RestoreConversation.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            $SWITCH_TABLE$com$riteshsahu$SMSBackupRestoreBase$ActionMode = iArr;
        }
        return iArr;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x011c A[Catch:{ CustomException -> 0x00d8, all -> 0x0133 }] */
    /* JADX WARNING: Removed duplicated region for block: B:5:0x0018 A[Catch:{ CustomException -> 0x00d8, all -> 0x0133 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r11 = this;
            r0 = 1
            r10 = 0
            r11.mSuccess = r0
            java.lang.String r8 = ""
            r9 = 0
            int[] r0 = $SWITCH_TABLE$com$riteshsahu$SMSBackupRestoreBase$ActionMode()     // Catch:{ CustomException -> 0x00d8 }
            com.riteshsahu.SMSBackupRestoreBase.ActionMode r1 = r11.mActionMode     // Catch:{ CustomException -> 0x00d8 }
            int r1 = r1.ordinal()     // Catch:{ CustomException -> 0x00d8 }
            r0 = r0[r1]     // Catch:{ CustomException -> 0x00d8 }
            switch(r0) {
                case 2: goto L_0x006b;
                case 3: goto L_0x00c8;
                case 4: goto L_0x0088;
                case 5: goto L_0x0107;
                case 6: goto L_0x00a5;
                default: goto L_0x0016;
            }     // Catch:{ CustomException -> 0x00d8 }
        L_0x0016:
            if (r9 == 0) goto L_0x011c
            java.lang.String r0 = r9.getMessage()     // Catch:{ CustomException -> 0x00d8 }
            r11.mResult = r0     // Catch:{ CustomException -> 0x00d8 }
            java.lang.String r0 = r11.mResult     // Catch:{ CustomException -> 0x00d8 }
            if (r0 == 0) goto L_0x002a
            java.lang.String r0 = r11.mResult     // Catch:{ CustomException -> 0x00d8 }
            int r0 = r0.length()     // Catch:{ CustomException -> 0x00d8 }
            if (r0 != 0) goto L_0x0060
        L_0x002a:
            android.app.Activity r0 = r11.mContextActivity     // Catch:{ CustomException -> 0x00d8 }
            r1 = 2131230737(0x7f080011, float:1.8077535E38)
            java.lang.String r0 = r0.getString(r1)     // Catch:{ CustomException -> 0x00d8 }
            r1 = 4
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ CustomException -> 0x00d8 }
            r2 = 0
            r1[r2] = r8     // Catch:{ CustomException -> 0x00d8 }
            r2 = 1
            int r3 = r9.getSuccessful()     // Catch:{ CustomException -> 0x00d8 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ CustomException -> 0x00d8 }
            r1[r2] = r3     // Catch:{ CustomException -> 0x00d8 }
            r2 = 2
            int r3 = r9.getFailed()     // Catch:{ CustomException -> 0x00d8 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ CustomException -> 0x00d8 }
            r1[r2] = r3     // Catch:{ CustomException -> 0x00d8 }
            r2 = 3
            int r3 = r9.getTotal()     // Catch:{ CustomException -> 0x00d8 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ CustomException -> 0x00d8 }
            r1[r2] = r3     // Catch:{ CustomException -> 0x00d8 }
            java.lang.String r0 = java.lang.String.format(r0, r1)     // Catch:{ CustomException -> 0x00d8 }
            r11.mResult = r0     // Catch:{ CustomException -> 0x00d8 }
        L_0x0060:
            android.app.ProgressDialog r0 = r11.mProgressDialog
            r0.dismiss()
        L_0x0065:
            android.os.Handler r0 = r11.mHandler
            r0.sendEmptyMessage(r10)
            return
        L_0x006b:
            android.app.Activity r0 = r11.mContextActivity     // Catch:{ CustomException -> 0x00d8 }
            r1 = 2131230740(0x7f080014, float:1.8077541E38)
            java.lang.String r8 = r0.getString(r1)     // Catch:{ CustomException -> 0x00d8 }
            android.app.Activity r0 = r11.mContextActivity     // Catch:{ CustomException -> 0x00d8 }
            com.riteshsahu.SMSBackupRestoreBase.BackupFile r1 = com.riteshsahu.SMSBackupRestoreBase.Common.getCurrentBackupFile()     // Catch:{ CustomException -> 0x00d8 }
            r2 = 1
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)     // Catch:{ CustomException -> 0x00d8 }
            android.app.ProgressDialog r3 = r11.mProgressDialog     // Catch:{ CustomException -> 0x00d8 }
            android.os.Handler r4 = r11.mHandler     // Catch:{ CustomException -> 0x00d8 }
            com.riteshsahu.SMSBackupRestoreBase.OperationResult r9 = com.riteshsahu.SMSBackupRestoreBase.SmsBackupProcessor.createBackup(r0, r1, r2, r3, r4)     // Catch:{ CustomException -> 0x00d8 }
            goto L_0x0016
        L_0x0088:
            android.app.Activity r0 = r11.mContextActivity     // Catch:{ CustomException -> 0x00d8 }
            r1 = 2131230741(0x7f080015, float:1.8077543E38)
            java.lang.String r8 = r0.getString(r1)     // Catch:{ CustomException -> 0x00d8 }
            android.app.Activity r0 = r11.mContextActivity     // Catch:{ CustomException -> 0x00d8 }
            com.riteshsahu.SMSBackupRestoreBase.BackupFile r1 = com.riteshsahu.SMSBackupRestoreBase.Common.getCurrentBackupFile()     // Catch:{ CustomException -> 0x00d8 }
            boolean r2 = r11.mCheckDuplicatesDuringRestore     // Catch:{ CustomException -> 0x00d8 }
            android.app.ProgressDialog r3 = r11.mProgressDialog     // Catch:{ CustomException -> 0x00d8 }
            android.os.Handler r4 = r11.mHandler     // Catch:{ CustomException -> 0x00d8 }
            r5 = 0
            r6 = 0
            com.riteshsahu.SMSBackupRestoreBase.OperationResult r9 = com.riteshsahu.SMSBackupRestoreBase.SmsRestoreProcessor.loadXml(r0, r1, r2, r3, r4, r5, r6)     // Catch:{ CustomException -> 0x00d8 }
            goto L_0x0016
        L_0x00a5:
            android.app.Activity r0 = r11.mContextActivity     // Catch:{ CustomException -> 0x00d8 }
            r1 = 2131230742(0x7f080016, float:1.8077545E38)
            java.lang.String r8 = r0.getString(r1)     // Catch:{ CustomException -> 0x00d8 }
            android.app.Activity r0 = r11.mContextActivity     // Catch:{ CustomException -> 0x00d8 }
            com.riteshsahu.SMSBackupRestoreBase.BackupFile r1 = com.riteshsahu.SMSBackupRestoreBase.Common.getCurrentBackupFile()     // Catch:{ CustomException -> 0x00d8 }
            boolean r2 = r11.mCheckDuplicatesDuringRestore     // Catch:{ CustomException -> 0x00d8 }
            android.app.ProgressDialog r3 = r11.mProgressDialog     // Catch:{ CustomException -> 0x00d8 }
            android.os.Handler r4 = r11.mHandler     // Catch:{ CustomException -> 0x00d8 }
            com.riteshsahu.SMSBackupRestoreBase.ContactNumbers r5 = r11.mContactNumbers     // Catch:{ CustomException -> 0x00d8 }
            com.riteshsahu.SMSBackupRestoreBase.ContactNumbers r6 = r11.mContactNumbers     // Catch:{ CustomException -> 0x00d8 }
            int r6 = r6.getCount()     // Catch:{ CustomException -> 0x00d8 }
            com.riteshsahu.SMSBackupRestoreBase.OperationResult r9 = com.riteshsahu.SMSBackupRestoreBase.SmsRestoreProcessor.loadXml(r0, r1, r2, r3, r4, r5, r6)     // Catch:{ CustomException -> 0x00d8 }
            goto L_0x0016
        L_0x00c8:
            android.app.Activity r0 = r11.mContextActivity     // Catch:{ CustomException -> 0x00d8 }
            r1 = 2131230743(0x7f080017, float:1.8077547E38)
            java.lang.String r8 = r0.getString(r1)     // Catch:{ CustomException -> 0x00d8 }
            android.app.Activity r0 = r11.mContextActivity     // Catch:{ CustomException -> 0x00d8 }
            com.riteshsahu.SMSBackupRestoreBase.Common.deleteAllMessages(r0)     // Catch:{ CustomException -> 0x00d8 }
            goto L_0x0016
        L_0x00d8:
            r0 = move-exception
            r7 = r0
            java.lang.String r0 = r7.getMessage()     // Catch:{ all -> 0x0133 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logDebug(r0)     // Catch:{ all -> 0x0133 }
            android.app.Activity r0 = r11.mContextActivity     // Catch:{ all -> 0x0133 }
            r1 = 2131230739(0x7f080013, float:1.807754E38)
            java.lang.String r0 = r0.getString(r1)     // Catch:{ all -> 0x0133 }
            r1 = 2
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ all -> 0x0133 }
            r2 = 0
            r1[r2] = r8     // Catch:{ all -> 0x0133 }
            r2 = 1
            java.lang.String r3 = r7.getMessage()     // Catch:{ all -> 0x0133 }
            r1[r2] = r3     // Catch:{ all -> 0x0133 }
            java.lang.String r0 = java.lang.String.format(r0, r1)     // Catch:{ all -> 0x0133 }
            r11.mResult = r0     // Catch:{ all -> 0x0133 }
            r0 = 0
            r11.mSuccess = r0     // Catch:{ all -> 0x0133 }
            android.app.ProgressDialog r0 = r11.mProgressDialog
            r0.dismiss()
            goto L_0x0065
        L_0x0107:
            android.app.Activity r0 = r11.mContextActivity     // Catch:{ CustomException -> 0x00d8 }
            r1 = 2131230744(0x7f080018, float:1.807755E38)
            java.lang.String r8 = r0.getString(r1)     // Catch:{ CustomException -> 0x00d8 }
            java.util.List r0 = com.riteshsahu.SMSBackupRestoreBase.Common.getSelectedFiles()     // Catch:{ CustomException -> 0x00d8 }
            android.app.Activity r1 = r11.mContextActivity     // Catch:{ CustomException -> 0x00d8 }
            com.riteshsahu.SMSBackupRestoreBase.OperationResult r9 = com.riteshsahu.SMSBackupRestoreBase.Common.deleteFiles(r0, r1)     // Catch:{ CustomException -> 0x00d8 }
            goto L_0x0016
        L_0x011c:
            android.app.Activity r0 = r11.mContextActivity     // Catch:{ CustomException -> 0x00d8 }
            r1 = 2131230738(0x7f080012, float:1.8077537E38)
            java.lang.String r0 = r0.getString(r1)     // Catch:{ CustomException -> 0x00d8 }
            r1 = 1
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ CustomException -> 0x00d8 }
            r2 = 0
            r1[r2] = r8     // Catch:{ CustomException -> 0x00d8 }
            java.lang.String r0 = java.lang.String.format(r0, r1)     // Catch:{ CustomException -> 0x00d8 }
            r11.mResult = r0     // Catch:{ CustomException -> 0x00d8 }
            goto L_0x0060
        L_0x0133:
            r0 = move-exception
            android.app.ProgressDialog r1 = r11.mProgressDialog
            r1.dismiss()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.riteshsahu.SMSBackupRestoreBase.TaskRunner.run():void");
    }

    public void performAction(Activity context, ActionMode actionMode, boolean closeActivityWhenDone) {
        performAction(context, actionMode, null, Main.getCheckDuplicatesDuringRestore(), closeActivityWhenDone);
    }

    public void performAction(Activity context, ActionMode actionMode, ContactNumbers contactNumbers, boolean checkDuplicatesDuringRestore, boolean closeActivityWhenDone) {
        this.mContextActivity = context;
        this.mActionMode = actionMode;
        this.mContactNumbers = contactNumbers;
        this.mCheckDuplicatesDuringRestore = checkDuplicatesDuringRestore;
        this.mCloseActivityWhenDone = closeActivityWhenDone;
        String message = "";
        this.mProgressDialog = new ProgressDialog(this.mContextActivity);
        switch ($SWITCH_TABLE$com$riteshsahu$SMSBackupRestoreBase$ActionMode()[this.mActionMode.ordinal()]) {
            case 2:
                message = this.mContextActivity.getString(R.string.backing);
                this.mProgressDialog.setProgressStyle(1);
                break;
            case 3:
                message = this.mContextActivity.getString(R.string.deleting_messages);
                break;
            case 4:
                message = this.mContextActivity.getString(R.string.restoring);
                this.mProgressDialog.setProgressStyle(1);
                break;
            case 5:
                message = this.mContextActivity.getString(R.string.deleting_files);
                break;
            case 6:
                message = this.mContextActivity.getString(R.string.restoring_conversation);
                this.mProgressDialog.setProgressStyle(1);
                break;
        }
        this.mProgressDialog.setMessage(message);
        this.mProgressDialog.setIcon((int) R.drawable.icon);
        this.mProgressDialog.setCancelable(false);
        this.mProgressDialog.show();
        new Thread(this).start();
    }

    /* access modifiers changed from: private */
    public void askForChangingBackupFolder() {
        final String restoreFolder = Common.getCurrentBackupFile().getFolder();
        if (!restoreFolder.equalsIgnoreCase(Common.getBackupFilePath(this.mContextActivity))) {
            new AlertDialog.Builder(this.mContextActivity).setIcon((int) R.drawable.icon).setTitle(this.mContextActivity.getText(R.string.app_name)).setMessage((int) R.string.restored_from_non_default_folder).setPositiveButton((int) R.string.button_yes, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    Common.setStringPreference(TaskRunner.this.mContextActivity, PreferenceKeys.BackupFolder, restoreFolder);
                }
            }).setNegativeButton((int) R.string.button_no, (DialogInterface.OnClickListener) null).create().show();
        }
    }
}
