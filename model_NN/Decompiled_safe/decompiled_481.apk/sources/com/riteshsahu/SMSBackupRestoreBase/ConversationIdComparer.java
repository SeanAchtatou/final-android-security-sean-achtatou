package com.riteshsahu.SMSBackupRestoreBase;

import java.util.Comparator;

public class ConversationIdComparer implements Comparator<Conversation> {
    public int compare(Conversation first, Conversation second) {
        return first.getId().compareTo(second.getId());
    }
}
