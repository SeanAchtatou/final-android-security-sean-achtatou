package com.riteshsahu.SMSBackupRestoreBase;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class Message implements Comparable<Message> {
    private static DateFormat mDateFormat = SimpleDateFormat.getDateTimeInstance();
    private String mBody;
    private long mDate;
    private Boolean mIncoming;

    public int compareTo(Message other) {
        return other.getDate().compareTo(getDate());
    }

    public String getBody() {
        return this.mBody;
    }

    public Long getDate() {
        return Long.valueOf(this.mDate);
    }

    public String getDateText() {
        return mDateFormat.format(Long.valueOf(this.mDate));
    }

    public Boolean getIncoming() {
        return this.mIncoming;
    }

    public void setBody(String body) {
        this.mBody = body;
    }

    public void setDate(long date) {
        this.mDate = date;
    }

    public void setIncoming(Boolean incoming) {
        this.mIncoming = incoming;
    }
}
