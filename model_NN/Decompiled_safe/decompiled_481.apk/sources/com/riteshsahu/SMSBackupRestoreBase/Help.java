package com.riteshsahu.SMSBackupRestoreBase;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;
import com.riteshsahu.SMSBackupRestore.R;

public class Help extends Activity {
    final ActivityHelper mActivityHelper = ActivityHelper.createInstance(this);

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.help);
        this.mActivityHelper.setupActionBar(String.format(getString(R.string.help_title), getString(R.string.app_version_name)), 0);
        ((WebView) findViewById(R.id.help_webview)).loadUrl("file:///android_asset/Help.html");
        showAds();
    }

    /* access modifiers changed from: protected */
    public void showAds() {
    }
}
