package com.riteshsahu.SMSBackupRestoreBase;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.riteshsahu.SMSBackupRestore.R;
import java.util.List;

public class BackupFileListAdapter extends ArrayAdapter<BackupFile> {
    private LayoutInflater mInflater;
    private List<BackupFile> mItems;
    private int mViewResourceId;

    public BackupFileListAdapter(Context context, int textViewResourceId, List<BackupFile> items) {
        super(context, textViewResourceId, items);
        this.mInflater = LayoutInflater.from(context);
        this.mItems = items;
        this.mViewResourceId = textViewResourceId;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            v = this.mInflater.inflate(this.mViewResourceId, (ViewGroup) null);
        }
        BackupFile backupFile = this.mItems.get(position);
        if (backupFile != null) {
            ((TextView) v.findViewById(R.id.dialog_list_item_title)).setText(backupFile.getFileName());
        }
        return v;
    }
}
