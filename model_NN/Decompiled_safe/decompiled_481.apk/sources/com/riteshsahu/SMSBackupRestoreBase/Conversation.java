package com.riteshsahu.SMSBackupRestoreBase;

public class Conversation extends Contact {
    private String mBody;
    private Boolean mSelected = false;
    private long mThreadId;

    public Conversation() {
    }

    public Conversation(Contact contact, String body, long threadId, boolean selected) {
        setName(contact.getName());
        setAddress(contact.getAddress());
        setId(contact.getId());
        this.mBody = body;
        this.mThreadId = threadId;
        this.mSelected = Boolean.valueOf(selected);
    }

    public void setThreadId(long threadId) {
        this.mThreadId = threadId;
    }

    public long getThreadId() {
        return this.mThreadId;
    }

    public void setBody(String body) {
        this.mBody = body;
    }

    public String getBody() {
        return this.mBody;
    }

    public void setSelected(Boolean selected) {
        this.mSelected = selected;
    }

    public Boolean getSelected() {
        return this.mSelected;
    }
}
