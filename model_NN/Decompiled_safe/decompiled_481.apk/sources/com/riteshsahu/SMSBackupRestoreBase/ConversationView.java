package com.riteshsahu.SMSBackupRestoreBase;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import com.riteshsahu.SMSBackupRestore.R;
import java.util.ArrayList;

public class ConversationView extends ListActivity {
    /* access modifiers changed from: private */
    public static String mErrorConversation;
    final ActivityHelper mActivityHelper = ActivityHelper.createInstance(this);
    /* access modifiers changed from: private */
    public ConversationAdapter mAdapter;
    /* access modifiers changed from: private */
    public ArrayList<Conversation> mConversations = null;
    /* access modifiers changed from: private */
    public ProgressDialog mProgressDialog = null;
    private Runnable returnRes = new Runnable() {
        public void run() {
            if (ConversationView.mErrorConversation == null || ConversationView.mErrorConversation.length() == 0) {
                if (ConversationView.this.mConversations != null && ConversationView.this.mConversations.size() > 0) {
                    ConversationView.this.mAdapter.notifyDataSetChanged();
                    for (int i = ConversationView.this.mConversations.size() - 1; i >= 0; i--) {
                        ConversationView.this.mAdapter.add((Conversation) ConversationView.this.mConversations.get(i));
                    }
                }
                ConversationView.this.mProgressDialog.dismiss();
                ConversationView.this.mAdapter.notifyDataSetChanged();
                return;
            }
            new AlertDialog.Builder(ConversationView.this).setIcon((int) R.drawable.icon).setTitle(ConversationView.this.getText(R.string.app_name)).setMessage(String.format(ConversationView.this.getString(R.string.error_message), ConversationView.mErrorConversation)).setPositiveButton((int) R.string.button_close, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    ConversationView.this.finish();
                }
            }).create().show();
            ConversationView.this.mProgressDialog.dismiss();
        }
    };
    private Runnable viewConversations;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mErrorConversation = "";
        setContentView((int) R.layout.conversation_list);
        this.mActivityHelper.setupActionBar(getString(R.string.select_conversations), 0);
        this.mConversations = new ArrayList<>();
        this.mAdapter = new ConversationAdapter(this, R.layout.conversation_row, this.mConversations);
        setListAdapter(this.mAdapter);
        this.viewConversations = new Runnable() {
            public void run() {
                ConversationView.this.getConversations();
            }
        };
        new Thread(null, this.viewConversations, "ConversationViewBackground").start();
        this.mProgressDialog = ProgressDialog.show(this, getString(R.string.please_wait), getString(R.string.retrieving_conversations), true);
    }

    public void onPause() {
        super.onPause();
        StringBuilder selectedConversations = new StringBuilder();
        if (this.mConversations != null) {
            for (int x = 0; x < this.mConversations.size(); x++) {
                Conversation selectedConversation = this.mConversations.get(x);
                if (selectedConversation.getSelected().booleanValue()) {
                    if (selectedConversations.length() > 0) {
                        selectedConversations.append(",");
                    }
                    selectedConversations.append(selectedConversation.getThreadId());
                }
            }
        }
        Common.setStringPreference(this, PreferenceKeys.SelectedConversations, selectedConversations.toString());
    }

    /* access modifiers changed from: private */
    public void getConversations() {
        try {
            this.mConversations = ConversationProcessor.getConversationList(this);
            Common.logDebug("Conversations loaded: " + this.mConversations.size());
        } catch (Exception e) {
            mErrorConversation = e.getMessage();
        }
        runOnUiThread(this.returnRes);
    }

    private class ConversationAdapter extends ArrayAdapter<Conversation> {
        private ArrayList<Conversation> items;

        public ConversationAdapter(Context context, int textViewResourceId, ArrayList<Conversation> items2) {
            super(context, textViewResourceId, items2);
            this.items = items2;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View currentView = convertView;
            if (currentView == null) {
                currentView = ((LayoutInflater) ConversationView.this.getSystemService("layout_inflater")).inflate((int) R.layout.conversation_row, (ViewGroup) null);
            }
            Conversation conversation = this.items.get(position);
            if (conversation != null) {
                ((TextView) currentView.findViewById(R.id.conversation_body)).setText(conversation.getBody());
                ((TextView) currentView.findViewById(R.id.conversation_name)).setText(conversation.getName());
                ((TextView) currentView.findViewById(R.id.conversation_number)).setText(conversation.getAddress());
                CheckBox selectedCheckBox = (CheckBox) currentView.findViewById(R.id.conversation_selected);
                selectedCheckBox.setChecked(conversation.getSelected().booleanValue());
                selectedCheckBox.setOnClickListener(new OnItemClickListener(conversation));
            }
            return currentView;
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, (int) R.string.menu_select_all, 0, (int) R.string.menu_select_all).setIcon((int) R.drawable.icon_check_on);
        menu.add(0, (int) R.string.menu_unselect_all, 1, (int) R.string.menu_unselect_all).setIcon((int) R.drawable.icon_check_off);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.string.menu_select_all:
                for (int i = 0; i < this.mConversations.size(); i++) {
                    this.mConversations.get(i).setSelected(true);
                }
                this.mAdapter.notifyDataSetChanged();
                break;
            case R.string.menu_unselect_all:
                for (int i2 = 0; i2 < this.mConversations.size(); i2++) {
                    this.mConversations.get(i2).setSelected(false);
                }
                this.mAdapter.notifyDataSetChanged();
                break;
        }
        return true;
    }

    private class OnItemClickListener implements View.OnClickListener {
        private Conversation mConversation;

        OnItemClickListener(Conversation conversation) {
            this.mConversation = conversation;
        }

        public void onClick(View arg0) {
            this.mConversation.setSelected(Boolean.valueOf(!this.mConversation.getSelected().booleanValue()));
        }
    }
}
