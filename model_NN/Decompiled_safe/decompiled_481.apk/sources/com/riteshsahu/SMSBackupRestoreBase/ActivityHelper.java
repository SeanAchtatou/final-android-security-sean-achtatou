package com.riteshsahu.SMSBackupRestoreBase;

import android.app.Activity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.riteshsahu.SMSBackupRestore.R;

public class ActivityHelper {
    protected Activity mActivity;

    public static ActivityHelper createInstance(Activity activity) {
        return new ActivityHelper(activity);
    }

    protected ActivityHelper(Activity activity) {
        this.mActivity = activity;
    }

    private void addActionBarButtons() {
        SimpleMenu menu = new SimpleMenu(this.mActivity);
        this.mActivity.onCreatePanelMenu(0, menu);
        for (int i = 0; i < menu.size(); i++) {
            addActionButtonCompatFromMenuItem(menu.getItem(i));
        }
    }

    public void setupHomeActivity() {
    }

    public void setupSubActivity() {
    }

    public void setupActionBar(CharSequence title, int color) {
        ViewGroup actionBarCompat = getActionBarCompat();
        if (actionBarCompat != null) {
            LinearLayout.LayoutParams springLayoutParams = new LinearLayout.LayoutParams(0, -1);
            springLayoutParams.weight = 1.0f;
            TextView titleText = new TextView(this.mActivity, null, R.attr.actionbarCompatTextStyle);
            titleText.setLayoutParams(springLayoutParams);
            titleText.setText(title);
            actionBarCompat.addView(titleText);
            addActionBarButtons();
        }
    }

    public void setActionBarTitle(CharSequence title) {
        TextView titleText;
        ViewGroup actionBar = getActionBarCompat();
        if (actionBar != null && (titleText = (TextView) actionBar.findViewById(R.id.actionbar_compat_text)) != null) {
            titleText.setText(title);
        }
    }

    public ViewGroup getActionBarCompat() {
        return (ViewGroup) this.mActivity.findViewById(R.id.actionbar_compat);
    }

    public View addActionButtonCompatFromMenuItem(final MenuItem item) {
        ViewGroup actionBar = getActionBarCompat();
        if (actionBar == null) {
            return null;
        }
        ImageView separator = new ImageView(this.mActivity, null, R.attr.actionbarCompatSeparatorStyle);
        separator.setLayoutParams(new ViewGroup.LayoutParams(2, -1));
        ImageButton actionButton = new ImageButton(this.mActivity, null, R.attr.actionbarCompatButtonStyle);
        actionButton.setId(item.getItemId());
        actionButton.setLayoutParams(new ViewGroup.LayoutParams((int) this.mActivity.getResources().getDimension(R.dimen.actionbar_compat_height), -1));
        actionButton.setImageDrawable(item.getIcon());
        actionButton.setScaleType(ImageView.ScaleType.CENTER);
        actionButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                ActivityHelper.this.mActivity.onMenuItemSelected(0, item);
            }
        });
        actionBar.addView(separator);
        actionBar.addView(actionButton);
        return actionButton;
    }
}
