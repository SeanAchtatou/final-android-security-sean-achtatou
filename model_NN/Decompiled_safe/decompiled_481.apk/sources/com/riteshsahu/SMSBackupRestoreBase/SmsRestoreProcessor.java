package com.riteshsahu.SMSBackupRestoreBase;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import com.riteshsahu.SMSBackupRestore.R;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class SmsRestoreProcessor {
    private static HashMap<String, Long> mAddresses;
    private static List<String> mColumnList;
    private static HashMap<String, String> mContacts;

    private static void addAddressToList(String address, Long threadId) {
        if (threadId.longValue() >= 0 && !mAddresses.containsKey(address)) {
            mAddresses.put(address, threadId);
        }
    }

    private static void clearColumnList() {
        if (mColumnList != null) {
            mColumnList.clear();
            mColumnList = null;
        }
    }

    private static boolean contactHasNumber(ContactNumbers contact, String number) {
        List<String> numbers = contact.getNumbers();
        for (int x = 0; x < numbers.size(); x++) {
            if (number.equalsIgnoreCase(numbers.get(x))) {
                return true;
            }
        }
        return false;
    }

    /* JADX INFO: Multiple debug info for r13v46 java.lang.String: [D('eventType' int), D('name' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r0v56 java.util.TimeZone: [D('timeZone' java.util.TimeZone), D('timeZoneAdjust' int)] */
    /* JADX INFO: Multiple debug info for r0v57 int: [D('timeZone' java.util.TimeZone), D('timeZoneAdjust' int)] */
    /* JADX WARN: Type inference failed for: r13v8 */
    /* JADX WARN: Type inference failed for: r13v21, types: [java.io.FileInputStream] */
    /* JADX WARN: Type inference failed for: r13v25 */
    /* JADX WARN: Type inference failed for: r7v0, types: [java.io.FileInputStream, java.io.InputStream] */
    /* JADX WARN: Type inference failed for: r13v37 */
    /* JADX WARN: Type inference failed for: r13v45 */
    /* JADX WARN: Type inference failed for: r13v52 */
    /* JADX WARN: Type inference failed for: r13v58 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x010f A[SYNTHETIC, Splitter:B:53:0x010f] */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x0276  */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:60:0x011c=Splitter:B:60:0x011c, B:47:0x00df=Splitter:B:47:0x00df} */
    /* JADX WARNING: Unknown variable types count: 2 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.List<com.riteshsahu.SMSBackupRestoreBase.ContactNumbers> getContactList(android.content.ContextWrapper r12, com.riteshsahu.SMSBackupRestoreBase.BackupFile r13) throws com.riteshsahu.SMSBackupRestoreBase.CustomException {
        /*
            com.riteshsahu.SMSBackupRestoreBase.Common.setupContactNameSettings()
            com.riteshsahu.SMSBackupRestoreBase.KXmlParser r8 = new com.riteshsahu.SMSBackupRestoreBase.KXmlParser
            r8.<init>()
            r2 = 0
            java.lang.String r6 = r13.getFullPath()
            r3 = 0
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            r0 = 0
            java.lang.String r4 = com.riteshsahu.SMSBackupRestoreBase.PreferenceKeys.AdjustTimezoneOnView
            java.lang.Boolean r4 = com.riteshsahu.SMSBackupRestoreBase.Common.getBooleanPreference(r12, r4)
            boolean r4 = r4.booleanValue()
            if (r4 == 0) goto L_0x0311
            java.util.TimeZone r0 = java.util.TimeZone.getDefault()
            int r0 = r0.getRawOffset()
            r9 = r0
        L_0x0029:
            java.io.File r0 = new java.io.File
            java.lang.String r13 = r13.getFolder()
            r0.<init>(r13)
            boolean r13 = r0.canRead()
            if (r13 == 0) goto L_0x027b
            com.riteshsahu.SMSBackupRestoreBase.WakeLocker.acquireLock(r12)     // Catch:{ FileNotFoundException -> 0x00dc, XmlPullParserException -> 0x0119, IOException -> 0x017b, Exception -> 0x01dd, all -> 0x02a4 }
            java.io.FileInputStream r7 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x00dc, XmlPullParserException -> 0x0119, IOException -> 0x017b, Exception -> 0x01dd, all -> 0x02a4 }
            r7.<init>(r6)     // Catch:{ FileNotFoundException -> 0x00dc, XmlPullParserException -> 0x0119, IOException -> 0x017b, Exception -> 0x01dd, all -> 0x02a4 }
            r13 = 0
            r8.setInput(r7, r13)     // Catch:{ FileNotFoundException -> 0x02fb, XmlPullParserException -> 0x02e8, IOException -> 0x02d5, Exception -> 0x02c2, all -> 0x02ab }
            int r13 = r8.getEventType()     // Catch:{ FileNotFoundException -> 0x02fb, XmlPullParserException -> 0x02e8, IOException -> 0x02d5, Exception -> 0x02c2, all -> 0x02ab }
            r2 = r3
        L_0x0049:
            r0 = 1
            if (r13 != r0) goto L_0x005c
            java.util.List r13 = mergeContacts(r1)     // Catch:{ FileNotFoundException -> 0x0300, XmlPullParserException -> 0x02ed, IOException -> 0x02da, Exception -> 0x02c7, all -> 0x02b1 }
            if (r7 == 0) goto L_0x00d7
            r7.close()     // Catch:{ IOException -> 0x00a0 }
            com.riteshsahu.SMSBackupRestoreBase.WakeLocker.releaseLock()
        L_0x0058:
            r1.clear()
            return r13
        L_0x005c:
            r0 = 0
            switch(r13) {
                case 2: goto L_0x0068;
                default: goto L_0x0060;
            }
        L_0x0060:
            r13 = r0
            r0 = r2
        L_0x0062:
            int r13 = r8.next()     // Catch:{ FileNotFoundException -> 0x0309, XmlPullParserException -> 0x02f6, IOException -> 0x02e3, Exception -> 0x02d0, all -> 0x02bd }
            r2 = r0
            goto L_0x0049
        L_0x0068:
            java.lang.String r13 = r8.getName()     // Catch:{ FileNotFoundException -> 0x0300, XmlPullParserException -> 0x02ed, IOException -> 0x02da, Exception -> 0x02c7, all -> 0x02b1 }
            java.lang.String r0 = "sms"
            boolean r0 = r13.equalsIgnoreCase(r0)     // Catch:{ FileNotFoundException -> 0x0300, XmlPullParserException -> 0x02ed, IOException -> 0x02da, Exception -> 0x02c7, all -> 0x02b1 }
            if (r0 == 0) goto L_0x030e
            int r10 = r2 + 1
            java.lang.String r0 = ""
            java.lang.String r2 = "date"
            java.lang.String r0 = r8.getAttributeValue(r0, r2)     // Catch:{ FileNotFoundException -> 0x0304, XmlPullParserException -> 0x02f1, IOException -> 0x02de, Exception -> 0x02cb, all -> 0x02b7 }
            long r4 = java.lang.Long.parseLong(r0)     // Catch:{ FileNotFoundException -> 0x0304, XmlPullParserException -> 0x02f1, IOException -> 0x02de, Exception -> 0x02cb, all -> 0x02b7 }
            boolean r0 = isIncomingMessage(r8)     // Catch:{ FileNotFoundException -> 0x0304, XmlPullParserException -> 0x02f1, IOException -> 0x02de, Exception -> 0x02cb, all -> 0x02b7 }
            if (r0 == 0) goto L_0x008a
            long r2 = (long) r9     // Catch:{ FileNotFoundException -> 0x0304, XmlPullParserException -> 0x02f1, IOException -> 0x02de, Exception -> 0x02cb, all -> 0x02b7 }
            long r4 = r4 - r2
        L_0x008a:
            java.lang.String r0 = ""
            java.lang.String r2 = "body"
            java.lang.String r2 = r8.getAttributeValue(r0, r2)     // Catch:{ FileNotFoundException -> 0x0304, XmlPullParserException -> 0x02f1, IOException -> 0x02de, Exception -> 0x02cb, all -> 0x02b7 }
            java.lang.String r0 = ""
            java.lang.String r3 = "address"
            java.lang.String r3 = r8.getAttributeValue(r0, r3)     // Catch:{ FileNotFoundException -> 0x0304, XmlPullParserException -> 0x02f1, IOException -> 0x02de, Exception -> 0x02cb, all -> 0x02b7 }
            r0 = r12
            updateContactList(r0, r1, r2, r3, r4)     // Catch:{ FileNotFoundException -> 0x0304, XmlPullParserException -> 0x02f1, IOException -> 0x02de, Exception -> 0x02cb, all -> 0x02b7 }
            r0 = r10
            goto L_0x0062
        L_0x00a0:
            r13 = move-exception
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x00d2 }
            java.lang.String r1 = "IOException "
            r0.<init>(r1)     // Catch:{ all -> 0x00d2 }
            java.lang.String r1 = r13.getMessage()     // Catch:{ all -> 0x00d2 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x00d2 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00d2 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logDebug(r0)     // Catch:{ all -> 0x00d2 }
            com.riteshsahu.SMSBackupRestoreBase.CustomException r0 = new com.riteshsahu.SMSBackupRestoreBase.CustomException     // Catch:{ all -> 0x00d2 }
            r1 = 2131230726(0x7f080006, float:1.8077513E38)
            java.lang.String r12 = r12.getString(r1)     // Catch:{ all -> 0x00d2 }
            r1 = 1
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ all -> 0x00d2 }
            r2 = 0
            java.lang.String r13 = r13.getMessage()     // Catch:{ all -> 0x00d2 }
            r1[r2] = r13     // Catch:{ all -> 0x00d2 }
            java.lang.String r12 = java.lang.String.format(r12, r1)     // Catch:{ all -> 0x00d2 }
            r0.<init>(r12)     // Catch:{ all -> 0x00d2 }
            throw r0     // Catch:{ all -> 0x00d2 }
        L_0x00d2:
            r12 = move-exception
            com.riteshsahu.SMSBackupRestoreBase.WakeLocker.releaseLock()
            throw r12
        L_0x00d7:
            com.riteshsahu.SMSBackupRestoreBase.WakeLocker.releaseLock()
            goto L_0x0058
        L_0x00dc:
            r13 = move-exception
            r0 = r2
            r2 = r3
        L_0x00df:
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ all -> 0x0108 }
            java.lang.String r3 = "Could not find file - "
            r13.<init>(r3)     // Catch:{ all -> 0x0108 }
            java.lang.StringBuilder r13 = r13.append(r6)     // Catch:{ all -> 0x0108 }
            java.lang.String r13 = r13.toString()     // Catch:{ all -> 0x0108 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r13)     // Catch:{ all -> 0x0108 }
            com.riteshsahu.SMSBackupRestoreBase.CustomException r13 = new com.riteshsahu.SMSBackupRestoreBase.CustomException     // Catch:{ all -> 0x0108 }
            r3 = 2131230730(0x7f08000a, float:1.8077521E38)
            java.lang.String r3 = r12.getString(r3)     // Catch:{ all -> 0x0108 }
            r4 = 1
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x0108 }
            r5 = 0
            r4[r5] = r6     // Catch:{ all -> 0x0108 }
            java.lang.String r3 = java.lang.String.format(r3, r4)     // Catch:{ all -> 0x0108 }
            r13.<init>(r3)     // Catch:{ all -> 0x0108 }
            throw r13     // Catch:{ all -> 0x0108 }
        L_0x0108:
            r13 = move-exception
            r11 = r13
            r13 = r0
            r0 = r2
            r2 = r11
        L_0x010d:
            if (r13 == 0) goto L_0x0276
            r13.close()     // Catch:{ IOException -> 0x023f }
            com.riteshsahu.SMSBackupRestoreBase.WakeLocker.releaseLock()
        L_0x0115:
            r1.clear()
            throw r2
        L_0x0119:
            r13 = move-exception
            r0 = r2
            r2 = r3
        L_0x011c:
            r13.printStackTrace()     // Catch:{ all -> 0x0108 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0108 }
            java.lang.String r4 = "App version: "
            r3.<init>(r4)     // Catch:{ all -> 0x0108 }
            r4 = 2131230721(0x7f080001, float:1.8077503E38)
            java.lang.String r4 = r12.getString(r4)     // Catch:{ all -> 0x0108 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0108 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0108 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r3)     // Catch:{ all -> 0x0108 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0108 }
            java.lang.String r4 = "Count: "
            r3.<init>(r4)     // Catch:{ all -> 0x0108 }
            java.lang.StringBuilder r3 = r3.append(r2)     // Catch:{ all -> 0x0108 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0108 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r3)     // Catch:{ all -> 0x0108 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0108 }
            java.lang.String r4 = "Error Parsing xml - "
            r3.<init>(r4)     // Catch:{ all -> 0x0108 }
            java.lang.String r4 = r13.getMessage()     // Catch:{ all -> 0x0108 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0108 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0108 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r3)     // Catch:{ all -> 0x0108 }
            com.riteshsahu.SMSBackupRestoreBase.CustomException r3 = new com.riteshsahu.SMSBackupRestoreBase.CustomException     // Catch:{ all -> 0x0108 }
            r4 = 2131230889(0x7f0800a9, float:1.8077844E38)
            java.lang.String r4 = r12.getString(r4)     // Catch:{ all -> 0x0108 }
            r5 = 1
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ all -> 0x0108 }
            r6 = 0
            java.lang.String r13 = r13.getMessage()     // Catch:{ all -> 0x0108 }
            r5[r6] = r13     // Catch:{ all -> 0x0108 }
            java.lang.String r13 = java.lang.String.format(r4, r5)     // Catch:{ all -> 0x0108 }
            r3.<init>(r13)     // Catch:{ all -> 0x0108 }
            throw r3     // Catch:{ all -> 0x0108 }
        L_0x017b:
            r13 = move-exception
            r0 = r2
            r2 = r3
        L_0x017e:
            r13.printStackTrace()     // Catch:{ all -> 0x0108 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0108 }
            java.lang.String r4 = "App version: "
            r3.<init>(r4)     // Catch:{ all -> 0x0108 }
            r4 = 2131230721(0x7f080001, float:1.8077503E38)
            java.lang.String r4 = r12.getString(r4)     // Catch:{ all -> 0x0108 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0108 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0108 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r3)     // Catch:{ all -> 0x0108 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0108 }
            java.lang.String r4 = "Count: "
            r3.<init>(r4)     // Catch:{ all -> 0x0108 }
            java.lang.StringBuilder r3 = r3.append(r2)     // Catch:{ all -> 0x0108 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0108 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r3)     // Catch:{ all -> 0x0108 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0108 }
            java.lang.String r4 = "IOException - "
            r3.<init>(r4)     // Catch:{ all -> 0x0108 }
            java.lang.String r4 = r13.getMessage()     // Catch:{ all -> 0x0108 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0108 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0108 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r3)     // Catch:{ all -> 0x0108 }
            com.riteshsahu.SMSBackupRestoreBase.CustomException r3 = new com.riteshsahu.SMSBackupRestoreBase.CustomException     // Catch:{ all -> 0x0108 }
            r4 = 2131230726(0x7f080006, float:1.8077513E38)
            java.lang.String r4 = r12.getString(r4)     // Catch:{ all -> 0x0108 }
            r5 = 1
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ all -> 0x0108 }
            r6 = 0
            java.lang.String r13 = r13.getMessage()     // Catch:{ all -> 0x0108 }
            r5[r6] = r13     // Catch:{ all -> 0x0108 }
            java.lang.String r13 = java.lang.String.format(r4, r5)     // Catch:{ all -> 0x0108 }
            r3.<init>(r13)     // Catch:{ all -> 0x0108 }
            throw r3     // Catch:{ all -> 0x0108 }
        L_0x01dd:
            r13 = move-exception
            r0 = r2
            r2 = r3
        L_0x01e0:
            r13.printStackTrace()     // Catch:{ all -> 0x0108 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0108 }
            java.lang.String r4 = "App version: "
            r3.<init>(r4)     // Catch:{ all -> 0x0108 }
            r4 = 2131230721(0x7f080001, float:1.8077503E38)
            java.lang.String r4 = r12.getString(r4)     // Catch:{ all -> 0x0108 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0108 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0108 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r3)     // Catch:{ all -> 0x0108 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0108 }
            java.lang.String r4 = "Count: "
            r3.<init>(r4)     // Catch:{ all -> 0x0108 }
            java.lang.StringBuilder r3 = r3.append(r2)     // Catch:{ all -> 0x0108 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0108 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r3)     // Catch:{ all -> 0x0108 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0108 }
            java.lang.String r4 = "Exception - "
            r3.<init>(r4)     // Catch:{ all -> 0x0108 }
            java.lang.String r4 = r13.getMessage()     // Catch:{ all -> 0x0108 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0108 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0108 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r3)     // Catch:{ all -> 0x0108 }
            com.riteshsahu.SMSBackupRestoreBase.CustomException r3 = new com.riteshsahu.SMSBackupRestoreBase.CustomException     // Catch:{ all -> 0x0108 }
            r4 = 2131230726(0x7f080006, float:1.8077513E38)
            java.lang.String r4 = r12.getString(r4)     // Catch:{ all -> 0x0108 }
            r5 = 1
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ all -> 0x0108 }
            r6 = 0
            java.lang.String r13 = r13.getMessage()     // Catch:{ all -> 0x0108 }
            r5[r6] = r13     // Catch:{ all -> 0x0108 }
            java.lang.String r13 = java.lang.String.format(r4, r5)     // Catch:{ all -> 0x0108 }
            r3.<init>(r13)     // Catch:{ all -> 0x0108 }
            throw r3     // Catch:{ all -> 0x0108 }
        L_0x023f:
            r13 = move-exception
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x0271 }
            java.lang.String r1 = "IOException "
            r0.<init>(r1)     // Catch:{ all -> 0x0271 }
            java.lang.String r1 = r13.getMessage()     // Catch:{ all -> 0x0271 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x0271 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0271 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logDebug(r0)     // Catch:{ all -> 0x0271 }
            com.riteshsahu.SMSBackupRestoreBase.CustomException r0 = new com.riteshsahu.SMSBackupRestoreBase.CustomException     // Catch:{ all -> 0x0271 }
            r1 = 2131230726(0x7f080006, float:1.8077513E38)
            java.lang.String r12 = r12.getString(r1)     // Catch:{ all -> 0x0271 }
            r1 = 1
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ all -> 0x0271 }
            r2 = 0
            java.lang.String r13 = r13.getMessage()     // Catch:{ all -> 0x0271 }
            r1[r2] = r13     // Catch:{ all -> 0x0271 }
            java.lang.String r12 = java.lang.String.format(r12, r1)     // Catch:{ all -> 0x0271 }
            r0.<init>(r12)     // Catch:{ all -> 0x0271 }
            throw r0     // Catch:{ all -> 0x0271 }
        L_0x0271:
            r12 = move-exception
            com.riteshsahu.SMSBackupRestoreBase.WakeLocker.releaseLock()
            throw r12
        L_0x0276:
            com.riteshsahu.SMSBackupRestoreBase.WakeLocker.releaseLock()
            goto L_0x0115
        L_0x027b:
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            java.lang.String r0 = "Could not read file: "
            r13.<init>(r0)
            java.lang.StringBuilder r13 = r13.append(r6)
            java.lang.String r13 = r13.toString()
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r13)
            com.riteshsahu.SMSBackupRestoreBase.CustomException r13 = new com.riteshsahu.SMSBackupRestoreBase.CustomException
            r0 = 2131230736(0x7f080010, float:1.8077533E38)
            java.lang.String r12 = r12.getString(r0)
            r0 = 1
            java.lang.Object[] r0 = new java.lang.Object[r0]
            r1 = 0
            r0[r1] = r6
            java.lang.String r12 = java.lang.String.format(r12, r0)
            r13.<init>(r12)
            throw r13
        L_0x02a4:
            r13 = move-exception
            r0 = r3
            r11 = r13
            r13 = r2
            r2 = r11
            goto L_0x010d
        L_0x02ab:
            r13 = move-exception
            r2 = r13
            r0 = r3
            r13 = r7
            goto L_0x010d
        L_0x02b1:
            r13 = move-exception
            r0 = r2
            r2 = r13
            r13 = r7
            goto L_0x010d
        L_0x02b7:
            r13 = move-exception
            r2 = r13
            r0 = r10
            r13 = r7
            goto L_0x010d
        L_0x02bd:
            r13 = move-exception
            r2 = r13
            r13 = r7
            goto L_0x010d
        L_0x02c2:
            r13 = move-exception
            r2 = r3
            r0 = r7
            goto L_0x01e0
        L_0x02c7:
            r13 = move-exception
            r0 = r7
            goto L_0x01e0
        L_0x02cb:
            r13 = move-exception
            r2 = r10
            r0 = r7
            goto L_0x01e0
        L_0x02d0:
            r13 = move-exception
            r2 = r0
            r0 = r7
            goto L_0x01e0
        L_0x02d5:
            r13 = move-exception
            r2 = r3
            r0 = r7
            goto L_0x017e
        L_0x02da:
            r13 = move-exception
            r0 = r7
            goto L_0x017e
        L_0x02de:
            r13 = move-exception
            r2 = r10
            r0 = r7
            goto L_0x017e
        L_0x02e3:
            r13 = move-exception
            r2 = r0
            r0 = r7
            goto L_0x017e
        L_0x02e8:
            r13 = move-exception
            r2 = r3
            r0 = r7
            goto L_0x011c
        L_0x02ed:
            r13 = move-exception
            r0 = r7
            goto L_0x011c
        L_0x02f1:
            r13 = move-exception
            r2 = r10
            r0 = r7
            goto L_0x011c
        L_0x02f6:
            r13 = move-exception
            r2 = r0
            r0 = r7
            goto L_0x011c
        L_0x02fb:
            r13 = move-exception
            r2 = r3
            r0 = r7
            goto L_0x00df
        L_0x0300:
            r13 = move-exception
            r0 = r7
            goto L_0x00df
        L_0x0304:
            r13 = move-exception
            r2 = r10
            r0 = r7
            goto L_0x00df
        L_0x0309:
            r13 = move-exception
            r2 = r0
            r0 = r7
            goto L_0x00df
        L_0x030e:
            r0 = r2
            goto L_0x0062
        L_0x0311:
            r9 = r0
            goto L_0x0029
        */
        throw new UnsupportedOperationException("Method not decompiled: com.riteshsahu.SMSBackupRestoreBase.SmsRestoreProcessor.getContactList(android.content.ContextWrapper, com.riteshsahu.SMSBackupRestoreBase.BackupFile):java.util.List");
    }

    private static long getMessageId(Context context, String address, String date, String protocol) {
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(Common.SmsContentUri, new String[]{Common.IdColumnName}, "address = ? AND date = ? AND protocol = ?", new String[]{address, date, protocol}, null);
            if (cursor == null || cursor.getCount() <= 0) {
                if (cursor != null) {
                    cursor.close();
                }
                return -1;
            }
            cursor.moveToNext();
            long j = cursor.getLong(0);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:5:0x003c */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:51:0x0117 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:38:0x00d8 */
    /* JADX INFO: Multiple debug info for r0v21 ?: [D('inputStream' java.io.FileInputStream), D('folder' java.io.File)] */
    /* JADX INFO: Multiple debug info for r2v29 int: [D('message' com.riteshsahu.SMSBackupRestoreBase.Message), D('totalCount' int)] */
    /* JADX INFO: Multiple debug info for r0v42 java.util.TimeZone: [D('timeZone' java.util.TimeZone), D('timeZoneAdjust' int)] */
    /* JADX INFO: Multiple debug info for r0v43 int: [D('timeZone' java.util.TimeZone), D('timeZoneAdjust' int)] */
    /* JADX WARN: Type inference failed for: r0v2, types: [java.io.File] */
    /* JADX WARN: Type inference failed for: r0v4 */
    /* JADX WARN: Type inference failed for: r13v9 */
    /* JADX WARN: Type inference failed for: r0v10, types: [int] */
    /* JADX WARN: Type inference failed for: r0v11 */
    /* JADX WARN: Type inference failed for: r13v26, types: [java.io.FileInputStream] */
    /* JADX WARN: Type inference failed for: r0v18 */
    /* JADX WARN: Type inference failed for: r13v30 */
    /* JADX WARN: Type inference failed for: r0v20 */
    /* JADX WARN: Type inference failed for: r0v21, types: [java.io.FileInputStream, java.io.InputStream] */
    /* JADX WARN: Type inference failed for: r0v22 */
    /* JADX WARN: Type inference failed for: r0v23 */
    /* JADX WARN: Type inference failed for: r13v41 */
    /* JADX WARN: Type inference failed for: r0v32 */
    /* JADX WARN: Type inference failed for: r0v33 */
    /* JADX WARN: Type inference failed for: r13v51 */
    /* JADX WARN: Type inference failed for: r0v37 */
    /* JADX WARN: Type inference failed for: r13v61 */
    /* JADX WARN: Type inference failed for: r0v41 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0108 A[SYNTHETIC, Splitter:B:44:0x0108] */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x0250  */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:51:0x0117=Splitter:B:51:0x0117, B:38:0x00d8=Splitter:B:38:0x00d8} */
    /* JADX WARNING: Unknown variable types count: 4 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.ArrayList<com.riteshsahu.SMSBackupRestoreBase.Message> getMessageList(android.content.ContextWrapper r12, com.riteshsahu.SMSBackupRestoreBase.BackupFile r13, com.riteshsahu.SMSBackupRestoreBase.ContactNumbers r14) throws com.riteshsahu.SMSBackupRestoreBase.CustomException {
        /*
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>()
            com.riteshsahu.SMSBackupRestoreBase.KXmlParser r5 = new com.riteshsahu.SMSBackupRestoreBase.KXmlParser
            r5.<init>()
            r2 = 0
            java.lang.String r1 = r13.getFullPath()
            r4 = 0
            java.util.HashMap r0 = new java.util.HashMap
            r0.<init>()
            com.riteshsahu.SMSBackupRestoreBase.SmsRestoreProcessor.mContacts = r0
            r0 = 0
            java.lang.String r6 = com.riteshsahu.SMSBackupRestoreBase.PreferenceKeys.AdjustTimezoneOnView
            java.lang.Boolean r6 = com.riteshsahu.SMSBackupRestoreBase.Common.getBooleanPreference(r12, r6)
            boolean r6 = r6.booleanValue()
            if (r6 == 0) goto L_0x0311
            java.util.TimeZone r0 = java.util.TimeZone.getDefault()
            int r0 = r0.getRawOffset()
            r6 = r0
        L_0x002d:
            java.io.File r0 = new java.io.File
            java.lang.String r13 = r13.getFolder()
            r0.<init>(r13)
            boolean r13 = r0.canRead()
            if (r13 == 0) goto L_0x0291
            com.riteshsahu.SMSBackupRestoreBase.WakeLocker.acquireLock(r12)     // Catch:{ FileNotFoundException -> 0x0302, XmlPullParserException -> 0x0114, IOException -> 0x015d, Exception -> 0x01bb, all -> 0x02ba }
            java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x0302, XmlPullParserException -> 0x0114, IOException -> 0x015d, Exception -> 0x01bb, all -> 0x02ba }
            r0.<init>(r1)     // Catch:{ FileNotFoundException -> 0x0302, XmlPullParserException -> 0x0114, IOException -> 0x015d, Exception -> 0x01bb, all -> 0x02ba }
            r13 = 0
            r5.setInput(r0, r13)     // Catch:{ FileNotFoundException -> 0x0307, XmlPullParserException -> 0x02f3, IOException -> 0x02e4, Exception -> 0x02d5, all -> 0x02c0 }
            int r13 = r5.getEventType()     // Catch:{ FileNotFoundException -> 0x0307, XmlPullParserException -> 0x02f3, IOException -> 0x02e4, Exception -> 0x02d5, all -> 0x02c0 }
            r7 = r4
        L_0x004d:
            r2 = 1
            if (r13 != r2) goto L_0x005e
            if (r0 == 0) goto L_0x028c
            r0.close()     // Catch:{ IOException -> 0x0255 }
            com.riteshsahu.SMSBackupRestoreBase.WakeLocker.releaseLock()
        L_0x0058:
            java.util.HashMap<java.lang.String, java.lang.String> r12 = com.riteshsahu.SMSBackupRestoreBase.SmsRestoreProcessor.mContacts
            r12.clear()
            return r3
        L_0x005e:
            r2 = 0
            r4 = 0
            switch(r13) {
                case 2: goto L_0x006c;
                default: goto L_0x0063;
            }
        L_0x0063:
            r13 = r2
            r2 = r4
            r4 = r7
        L_0x0066:
            int r13 = r5.next()     // Catch:{ FileNotFoundException -> 0x0307, XmlPullParserException -> 0x02f3, IOException -> 0x02e4, Exception -> 0x02d5, all -> 0x02c0 }
            r7 = r4
            goto L_0x004d
        L_0x006c:
            java.lang.String r4 = r5.getName()     // Catch:{ FileNotFoundException -> 0x030c, XmlPullParserException -> 0x02f8, IOException -> 0x02e9, Exception -> 0x02da, all -> 0x02c7 }
            java.lang.String r13 = "sms"
            boolean r13 = r4.equalsIgnoreCase(r13)     // Catch:{ FileNotFoundException -> 0x030c, XmlPullParserException -> 0x02f8, IOException -> 0x02e9, Exception -> 0x02da, all -> 0x02c7 }
            if (r13 == 0) goto L_0x0063
            java.lang.String r13 = ""
            java.lang.String r8 = "address"
            java.lang.String r13 = r5.getAttributeValue(r13, r8)     // Catch:{ FileNotFoundException -> 0x030c, XmlPullParserException -> 0x02f8, IOException -> 0x02e9, Exception -> 0x02da, all -> 0x02c7 }
            boolean r13 = contactHasNumber(r14, r13)     // Catch:{ FileNotFoundException -> 0x030c, XmlPullParserException -> 0x02f8, IOException -> 0x02e9, Exception -> 0x02da, all -> 0x02c7 }
            if (r13 == 0) goto L_0x0063
            int r2 = r7 + 1
            com.riteshsahu.SMSBackupRestoreBase.Message r13 = new com.riteshsahu.SMSBackupRestoreBase.Message     // Catch:{ FileNotFoundException -> 0x00d5, XmlPullParserException -> 0x02fd, IOException -> 0x02ee, Exception -> 0x02df, all -> 0x02ce }
            r13.<init>()     // Catch:{ FileNotFoundException -> 0x00d5, XmlPullParserException -> 0x02fd, IOException -> 0x02ee, Exception -> 0x02df, all -> 0x02ce }
            java.lang.String r7 = ""
            java.lang.String r8 = "body"
            java.lang.String r7 = r5.getAttributeValue(r7, r8)     // Catch:{ FileNotFoundException -> 0x00d5, XmlPullParserException -> 0x02fd, IOException -> 0x02ee, Exception -> 0x02df, all -> 0x02ce }
            r13.setBody(r7)     // Catch:{ FileNotFoundException -> 0x00d5, XmlPullParserException -> 0x02fd, IOException -> 0x02ee, Exception -> 0x02df, all -> 0x02ce }
            boolean r7 = isIncomingMessage(r5)     // Catch:{ FileNotFoundException -> 0x00d5, XmlPullParserException -> 0x02fd, IOException -> 0x02ee, Exception -> 0x02df, all -> 0x02ce }
            java.lang.Boolean r7 = java.lang.Boolean.valueOf(r7)     // Catch:{ FileNotFoundException -> 0x00d5, XmlPullParserException -> 0x02fd, IOException -> 0x02ee, Exception -> 0x02df, all -> 0x02ce }
            r13.setIncoming(r7)     // Catch:{ FileNotFoundException -> 0x00d5, XmlPullParserException -> 0x02fd, IOException -> 0x02ee, Exception -> 0x02df, all -> 0x02ce }
            java.lang.Boolean r7 = r13.getIncoming()     // Catch:{ FileNotFoundException -> 0x00d5, XmlPullParserException -> 0x02fd, IOException -> 0x02ee, Exception -> 0x02df, all -> 0x02ce }
            boolean r7 = r7.booleanValue()     // Catch:{ FileNotFoundException -> 0x00d5, XmlPullParserException -> 0x02fd, IOException -> 0x02ee, Exception -> 0x02df, all -> 0x02ce }
            if (r7 == 0) goto L_0x00c5
            java.lang.String r7 = ""
            java.lang.String r8 = "date"
            java.lang.String r7 = r5.getAttributeValue(r7, r8)     // Catch:{ FileNotFoundException -> 0x00d5, XmlPullParserException -> 0x02fd, IOException -> 0x02ee, Exception -> 0x02df, all -> 0x02ce }
            long r7 = java.lang.Long.parseLong(r7)     // Catch:{ FileNotFoundException -> 0x00d5, XmlPullParserException -> 0x02fd, IOException -> 0x02ee, Exception -> 0x02df, all -> 0x02ce }
            long r9 = (long) r6     // Catch:{ FileNotFoundException -> 0x00d5, XmlPullParserException -> 0x02fd, IOException -> 0x02ee, Exception -> 0x02df, all -> 0x02ce }
            long r7 = r7 - r9
            r13.setDate(r7)     // Catch:{ FileNotFoundException -> 0x00d5, XmlPullParserException -> 0x02fd, IOException -> 0x02ee, Exception -> 0x02df, all -> 0x02ce }
        L_0x00be:
            r3.add(r13)     // Catch:{ FileNotFoundException -> 0x00d5, XmlPullParserException -> 0x02fd, IOException -> 0x02ee, Exception -> 0x02df, all -> 0x02ce }
            r11 = r4
            r4 = r2
            r2 = r11
            goto L_0x0066
        L_0x00c5:
            java.lang.String r7 = ""
            java.lang.String r8 = "date"
            java.lang.String r7 = r5.getAttributeValue(r7, r8)     // Catch:{ FileNotFoundException -> 0x00d5, XmlPullParserException -> 0x02fd, IOException -> 0x02ee, Exception -> 0x02df, all -> 0x02ce }
            long r7 = java.lang.Long.parseLong(r7)     // Catch:{ FileNotFoundException -> 0x00d5, XmlPullParserException -> 0x02fd, IOException -> 0x02ee, Exception -> 0x02df, all -> 0x02ce }
            r13.setDate(r7)     // Catch:{ FileNotFoundException -> 0x00d5, XmlPullParserException -> 0x02fd, IOException -> 0x02ee, Exception -> 0x02df, all -> 0x02ce }
            goto L_0x00be
        L_0x00d5:
            r13 = move-exception
            r14 = r0
            r0 = r2
        L_0x00d8:
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ all -> 0x0101 }
            java.lang.String r2 = "Could not find file - "
            r13.<init>(r2)     // Catch:{ all -> 0x0101 }
            java.lang.StringBuilder r13 = r13.append(r1)     // Catch:{ all -> 0x0101 }
            java.lang.String r13 = r13.toString()     // Catch:{ all -> 0x0101 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r13)     // Catch:{ all -> 0x0101 }
            com.riteshsahu.SMSBackupRestoreBase.CustomException r13 = new com.riteshsahu.SMSBackupRestoreBase.CustomException     // Catch:{ all -> 0x0101 }
            r2 = 2131230730(0x7f08000a, float:1.8077521E38)
            java.lang.String r2 = r12.getString(r2)     // Catch:{ all -> 0x0101 }
            r3 = 1
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ all -> 0x0101 }
            r4 = 0
            r3[r4] = r1     // Catch:{ all -> 0x0101 }
            java.lang.String r1 = java.lang.String.format(r2, r3)     // Catch:{ all -> 0x0101 }
            r13.<init>(r1)     // Catch:{ all -> 0x0101 }
            throw r13     // Catch:{ all -> 0x0101 }
        L_0x0101:
            r13 = move-exception
            r11 = r13
            r13 = r14
            r14 = r0
            r0 = r11
        L_0x0106:
            if (r13 == 0) goto L_0x0250
            r13.close()     // Catch:{ IOException -> 0x0219 }
            com.riteshsahu.SMSBackupRestoreBase.WakeLocker.releaseLock()
        L_0x010e:
            java.util.HashMap<java.lang.String, java.lang.String> r12 = com.riteshsahu.SMSBackupRestoreBase.SmsRestoreProcessor.mContacts
            r12.clear()
            throw r0
        L_0x0114:
            r13 = move-exception
            r0 = r4
            r14 = r2
        L_0x0117:
            r13.printStackTrace()     // Catch:{ all -> 0x0101 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0101 }
            java.lang.String r2 = "Count: "
            r1.<init>(r2)     // Catch:{ all -> 0x0101 }
            java.lang.StringBuilder r1 = r1.append(r0)     // Catch:{ all -> 0x0101 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0101 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r1)     // Catch:{ all -> 0x0101 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0101 }
            java.lang.String r2 = "Error Parsing xml - "
            r1.<init>(r2)     // Catch:{ all -> 0x0101 }
            java.lang.String r2 = r13.getMessage()     // Catch:{ all -> 0x0101 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0101 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0101 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r1)     // Catch:{ all -> 0x0101 }
            com.riteshsahu.SMSBackupRestoreBase.CustomException r1 = new com.riteshsahu.SMSBackupRestoreBase.CustomException     // Catch:{ all -> 0x0101 }
            r2 = 2131230889(0x7f0800a9, float:1.8077844E38)
            java.lang.String r2 = r12.getString(r2)     // Catch:{ all -> 0x0101 }
            r3 = 1
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ all -> 0x0101 }
            r4 = 0
            java.lang.String r13 = r13.getMessage()     // Catch:{ all -> 0x0101 }
            r3[r4] = r13     // Catch:{ all -> 0x0101 }
            java.lang.String r13 = java.lang.String.format(r2, r3)     // Catch:{ all -> 0x0101 }
            r1.<init>(r13)     // Catch:{ all -> 0x0101 }
            throw r1     // Catch:{ all -> 0x0101 }
        L_0x015d:
            r13 = move-exception
            r0 = r4
            r14 = r2
        L_0x0160:
            r13.printStackTrace()     // Catch:{ all -> 0x0101 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0101 }
            java.lang.String r3 = "App version: "
            r2.<init>(r3)     // Catch:{ all -> 0x0101 }
            r3 = 2131230721(0x7f080001, float:1.8077503E38)
            java.lang.String r3 = r12.getString(r3)     // Catch:{ all -> 0x0101 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0101 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0101 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r2)     // Catch:{ all -> 0x0101 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0101 }
            java.lang.String r3 = "Count: "
            r2.<init>(r3)     // Catch:{ all -> 0x0101 }
            java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ all -> 0x0101 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0101 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r2)     // Catch:{ all -> 0x0101 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0101 }
            java.lang.String r3 = "IOException - "
            r2.<init>(r3)     // Catch:{ all -> 0x0101 }
            java.lang.String r13 = r13.getMessage()     // Catch:{ all -> 0x0101 }
            java.lang.StringBuilder r13 = r2.append(r13)     // Catch:{ all -> 0x0101 }
            java.lang.String r13 = r13.toString()     // Catch:{ all -> 0x0101 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r13)     // Catch:{ all -> 0x0101 }
            com.riteshsahu.SMSBackupRestoreBase.CustomException r13 = new com.riteshsahu.SMSBackupRestoreBase.CustomException     // Catch:{ all -> 0x0101 }
            r2 = 2131230736(0x7f080010, float:1.8077533E38)
            java.lang.String r2 = r12.getString(r2)     // Catch:{ all -> 0x0101 }
            r3 = 1
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ all -> 0x0101 }
            r4 = 0
            r3[r4] = r1     // Catch:{ all -> 0x0101 }
            java.lang.String r1 = java.lang.String.format(r2, r3)     // Catch:{ all -> 0x0101 }
            r13.<init>(r1)     // Catch:{ all -> 0x0101 }
            throw r13     // Catch:{ all -> 0x0101 }
        L_0x01bb:
            r13 = move-exception
            r0 = r4
            r14 = r2
        L_0x01be:
            r13.printStackTrace()     // Catch:{ all -> 0x0101 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0101 }
            java.lang.String r3 = "App version: "
            r2.<init>(r3)     // Catch:{ all -> 0x0101 }
            r3 = 2131230721(0x7f080001, float:1.8077503E38)
            java.lang.String r3 = r12.getString(r3)     // Catch:{ all -> 0x0101 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0101 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0101 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r2)     // Catch:{ all -> 0x0101 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0101 }
            java.lang.String r3 = "Count: "
            r2.<init>(r3)     // Catch:{ all -> 0x0101 }
            java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ all -> 0x0101 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0101 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r2)     // Catch:{ all -> 0x0101 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0101 }
            java.lang.String r3 = "Exception - "
            r2.<init>(r3)     // Catch:{ all -> 0x0101 }
            java.lang.String r13 = r13.getMessage()     // Catch:{ all -> 0x0101 }
            java.lang.StringBuilder r13 = r2.append(r13)     // Catch:{ all -> 0x0101 }
            java.lang.String r13 = r13.toString()     // Catch:{ all -> 0x0101 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r13)     // Catch:{ all -> 0x0101 }
            com.riteshsahu.SMSBackupRestoreBase.CustomException r13 = new com.riteshsahu.SMSBackupRestoreBase.CustomException     // Catch:{ all -> 0x0101 }
            r2 = 2131230736(0x7f080010, float:1.8077533E38)
            java.lang.String r2 = r12.getString(r2)     // Catch:{ all -> 0x0101 }
            r3 = 1
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ all -> 0x0101 }
            r4 = 0
            r3[r4] = r1     // Catch:{ all -> 0x0101 }
            java.lang.String r1 = java.lang.String.format(r2, r3)     // Catch:{ all -> 0x0101 }
            r13.<init>(r1)     // Catch:{ all -> 0x0101 }
            throw r13     // Catch:{ all -> 0x0101 }
        L_0x0219:
            r13 = move-exception
            java.lang.StringBuilder r14 = new java.lang.StringBuilder     // Catch:{ all -> 0x024b }
            java.lang.String r0 = "IOException "
            r14.<init>(r0)     // Catch:{ all -> 0x024b }
            java.lang.String r0 = r13.getMessage()     // Catch:{ all -> 0x024b }
            java.lang.StringBuilder r14 = r14.append(r0)     // Catch:{ all -> 0x024b }
            java.lang.String r14 = r14.toString()     // Catch:{ all -> 0x024b }
            com.riteshsahu.SMSBackupRestoreBase.Common.logDebug(r14)     // Catch:{ all -> 0x024b }
            com.riteshsahu.SMSBackupRestoreBase.CustomException r14 = new com.riteshsahu.SMSBackupRestoreBase.CustomException     // Catch:{ all -> 0x024b }
            r0 = 2131230726(0x7f080006, float:1.8077513E38)
            java.lang.String r12 = r12.getString(r0)     // Catch:{ all -> 0x024b }
            r0 = 1
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ all -> 0x024b }
            r1 = 0
            java.lang.String r13 = r13.getMessage()     // Catch:{ all -> 0x024b }
            r0[r1] = r13     // Catch:{ all -> 0x024b }
            java.lang.String r12 = java.lang.String.format(r12, r0)     // Catch:{ all -> 0x024b }
            r14.<init>(r12)     // Catch:{ all -> 0x024b }
            throw r14     // Catch:{ all -> 0x024b }
        L_0x024b:
            r12 = move-exception
            com.riteshsahu.SMSBackupRestoreBase.WakeLocker.releaseLock()
            throw r12
        L_0x0250:
            com.riteshsahu.SMSBackupRestoreBase.WakeLocker.releaseLock()
            goto L_0x010e
        L_0x0255:
            r13 = move-exception
            java.lang.StringBuilder r14 = new java.lang.StringBuilder     // Catch:{ all -> 0x0287 }
            java.lang.String r0 = "IOException "
            r14.<init>(r0)     // Catch:{ all -> 0x0287 }
            java.lang.String r0 = r13.getMessage()     // Catch:{ all -> 0x0287 }
            java.lang.StringBuilder r14 = r14.append(r0)     // Catch:{ all -> 0x0287 }
            java.lang.String r14 = r14.toString()     // Catch:{ all -> 0x0287 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logDebug(r14)     // Catch:{ all -> 0x0287 }
            com.riteshsahu.SMSBackupRestoreBase.CustomException r14 = new com.riteshsahu.SMSBackupRestoreBase.CustomException     // Catch:{ all -> 0x0287 }
            r0 = 2131230726(0x7f080006, float:1.8077513E38)
            java.lang.String r12 = r12.getString(r0)     // Catch:{ all -> 0x0287 }
            r0 = 1
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ all -> 0x0287 }
            r1 = 0
            java.lang.String r13 = r13.getMessage()     // Catch:{ all -> 0x0287 }
            r0[r1] = r13     // Catch:{ all -> 0x0287 }
            java.lang.String r12 = java.lang.String.format(r12, r0)     // Catch:{ all -> 0x0287 }
            r14.<init>(r12)     // Catch:{ all -> 0x0287 }
            throw r14     // Catch:{ all -> 0x0287 }
        L_0x0287:
            r12 = move-exception
            com.riteshsahu.SMSBackupRestoreBase.WakeLocker.releaseLock()
            throw r12
        L_0x028c:
            com.riteshsahu.SMSBackupRestoreBase.WakeLocker.releaseLock()
            goto L_0x0058
        L_0x0291:
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            java.lang.String r14 = "Could not read file: "
            r13.<init>(r14)
            java.lang.StringBuilder r13 = r13.append(r1)
            java.lang.String r13 = r13.toString()
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r13)
            com.riteshsahu.SMSBackupRestoreBase.CustomException r13 = new com.riteshsahu.SMSBackupRestoreBase.CustomException
            r14 = 2131230736(0x7f080010, float:1.8077533E38)
            java.lang.String r12 = r12.getString(r14)
            r14 = 1
            java.lang.Object[] r14 = new java.lang.Object[r14]
            r0 = 0
            r14[r0] = r1
            java.lang.String r12 = java.lang.String.format(r12, r14)
            r13.<init>(r12)
            throw r13
        L_0x02ba:
            r13 = move-exception
            r0 = r13
            r14 = r4
            r13 = r2
            goto L_0x0106
        L_0x02c0:
            r13 = move-exception
            r14 = r4
            r11 = r13
            r13 = r0
            r0 = r11
            goto L_0x0106
        L_0x02c7:
            r13 = move-exception
            r14 = r7
            r11 = r13
            r13 = r0
            r0 = r11
            goto L_0x0106
        L_0x02ce:
            r13 = move-exception
            r14 = r2
            r11 = r13
            r13 = r0
            r0 = r11
            goto L_0x0106
        L_0x02d5:
            r13 = move-exception
            r14 = r0
            r0 = r4
            goto L_0x01be
        L_0x02da:
            r13 = move-exception
            r14 = r0
            r0 = r7
            goto L_0x01be
        L_0x02df:
            r13 = move-exception
            r14 = r0
            r0 = r2
            goto L_0x01be
        L_0x02e4:
            r13 = move-exception
            r14 = r0
            r0 = r4
            goto L_0x0160
        L_0x02e9:
            r13 = move-exception
            r14 = r0
            r0 = r7
            goto L_0x0160
        L_0x02ee:
            r13 = move-exception
            r14 = r0
            r0 = r2
            goto L_0x0160
        L_0x02f3:
            r13 = move-exception
            r14 = r0
            r0 = r4
            goto L_0x0117
        L_0x02f8:
            r13 = move-exception
            r14 = r0
            r0 = r7
            goto L_0x0117
        L_0x02fd:
            r13 = move-exception
            r14 = r0
            r0 = r2
            goto L_0x0117
        L_0x0302:
            r13 = move-exception
            r0 = r4
            r14 = r2
            goto L_0x00d8
        L_0x0307:
            r13 = move-exception
            r14 = r0
            r0 = r4
            goto L_0x00d8
        L_0x030c:
            r13 = move-exception
            r14 = r0
            r0 = r7
            goto L_0x00d8
        L_0x0311:
            r6 = r0
            goto L_0x002d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.riteshsahu.SMSBackupRestoreBase.SmsRestoreProcessor.getMessageList(android.content.ContextWrapper, com.riteshsahu.SMSBackupRestoreBase.BackupFile, com.riteshsahu.SMSBackupRestoreBase.ContactNumbers):java.util.ArrayList");
    }

    private static long getMessageThreadId(Context context, Uri messageUri) {
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(messageUri, new String[]{Common.ThreadIdColumnName}, null, null, null);
            if (cursor == null || cursor.getCount() <= 0) {
                if (cursor != null) {
                    cursor.close();
                }
                return -1;
            }
            cursor.moveToNext();
            long j = cursor.getLong(0);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    private static Long getThreadIdForAddress(String address) {
        if (mAddresses.containsKey(address)) {
            return mAddresses.get(address);
        }
        return -1L;
    }

    private static Long insertMessage(ContentResolver contentResolver, ContentValues contentValueToSave, Context context, Long threadId) {
        if (threadId.longValue() >= 0) {
            contentValueToSave.put(Common.ThreadIdColumnName, threadId);
        }
        Uri messageUri = contentResolver.insert(Common.SmsContentUri, contentValueToSave);
        if (threadId.longValue() < 0) {
            return Long.valueOf(getMessageThreadId(context, messageUri));
        }
        return threadId;
    }

    private static void loadColumnList(Context context) throws CustomException {
        if (mColumnList == null) {
            Cursor cursor = null;
            try {
                cursor = context.getContentResolver().query(Common.SmsContentUri, null, "1 = 2", null, null);
                if (cursor == null) {
                    throw new CustomException(context.getString(R.string.error_could_not_load_columns));
                }
                mColumnList = new ArrayList();
                int columnCount = cursor.getColumnCount();
                for (int counter = 0; counter < columnCount; counter++) {
                    mColumnList.add(cursor.getColumnName(counter));
                }
                if (cursor != null) {
                    cursor.close();
                }
            } catch (Exception e) {
                Exception ex = e;
                ex.printStackTrace();
                throw new CustomException(String.valueOf(context.getString(R.string.error_could_not_load_columns)) + ": " + ex.getMessage());
            } catch (Throwable th) {
                if (cursor != null) {
                    cursor.close();
                }
                throw th;
            }
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX INFO: Multiple debug info for r8v2 java.lang.Boolean: [D('name' java.lang.String), D('foundValidValues' java.lang.Boolean)] */
    /* JADX INFO: Multiple debug info for r23v66 java.lang.String: [D('foundValidValues' java.lang.Boolean), D('columnName' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r23v87 ?: [D('address' java.lang.String), D('successfulCount' int)] */
    /* JADX INFO: Multiple debug info for r23v103 int: [D('eventType' int), D('contentValueToSave' android.content.ContentValues)] */
    /* JADX INFO: Multiple debug info for r7v22 java.util.TimeZone: [D('timeZone' java.util.TimeZone), D('timeZoneAdjust' int)] */
    /* JADX INFO: Multiple debug info for r7v23 int: [D('timeZone' java.util.TimeZone), D('timeZoneAdjust' int)] */
    /* JADX WARN: Type inference failed for: r24v9 */
    /* JADX WARN: Type inference failed for: r1v29, types: [int] */
    /* JADX WARN: Type inference failed for: r24v14, types: [java.io.FileInputStream] */
    /* JADX WARN: Type inference failed for: r24v20 */
    /* JADX WARN: Type inference failed for: r10v3, types: [java.io.FileInputStream] */
    /* JADX WARN: Type inference failed for: r24v26 */
    /* JADX WARN: Type inference failed for: r24v36 */
    /* JADX WARN: Type inference failed for: r24v40 */
    /* JADX WARN: Type inference failed for: r23v71, types: [int] */
    /* JADX WARN: Type inference failed for: r24v51 */
    /* JADX WARN: Type inference failed for: r23v87, types: [int] */
    /* JADX WARN: Type inference failed for: r23v96, types: [int] */
    /* JADX WARN: Type inference failed for: r24v56 */
    /* JADX WARN: Type inference failed for: r16v2 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:126:0x04f7  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x0286 A[SYNTHETIC, Splitter:B:78:0x0286] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:72:0x0217=Splitter:B:72:0x0217, B:108:0x0328=Splitter:B:108:0x0328} */
    /* JADX WARNING: Unknown variable types count: 3 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.riteshsahu.SMSBackupRestoreBase.OperationResult loadXml(android.content.Context r22, com.riteshsahu.SMSBackupRestoreBase.BackupFile r23, boolean r24, android.app.ProgressDialog r25, android.os.Handler r26, com.riteshsahu.SMSBackupRestoreBase.ContactNumbers r27, int r28) throws com.riteshsahu.SMSBackupRestoreBase.CustomException {
        /*
            com.riteshsahu.SMSBackupRestoreBase.OperationResult r15 = new com.riteshsahu.SMSBackupRestoreBase.OperationResult
            r15.<init>()
            r6 = 0
            r11 = 0
            r12 = 0
            com.riteshsahu.SMSBackupRestoreBase.KXmlParser r14 = new com.riteshsahu.SMSBackupRestoreBase.KXmlParser
            r14.<init>()
            r8 = 0
            java.lang.String r9 = r23.getFullPath()
            java.util.HashMap r4 = new java.util.HashMap
            r4.<init>()
            com.riteshsahu.SMSBackupRestoreBase.SmsRestoreProcessor.mAddresses = r4
            android.content.ContentResolver r5 = r22.getContentResolver()
            r7 = 0
            r4 = 0
            java.lang.String r10 = com.riteshsahu.SMSBackupRestoreBase.PreferenceKeys.AdjustTimezoneOnRestore
            r0 = r22
            r1 = r10
            java.lang.Boolean r10 = com.riteshsahu.SMSBackupRestoreBase.Common.getBooleanPreference(r0, r1)
            boolean r10 = r10.booleanValue()
            if (r10 == 0) goto L_0x06e3
            r4 = 1
            java.util.TimeZone r7 = java.util.TimeZone.getDefault()
            int r7 = r7.getRawOffset()
            r17 = r7
        L_0x0039:
            java.io.File r7 = new java.io.File
            java.lang.String r23 = r23.getFolder()
            r0 = r7
            r1 = r23
            r0.<init>(r1)
            boolean r23 = r7.canRead()
            if (r23 == 0) goto L_0x054a
            com.riteshsahu.SMSBackupRestoreBase.WakeLocker.acquireLock(r22)     // Catch:{ FileNotFoundException -> 0x06a2, XmlPullParserException -> 0x031f, IOException -> 0x03a4, Exception -> 0x0429, all -> 0x05a5 }
            java.io.FileInputStream r10 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x06a2, XmlPullParserException -> 0x031f, IOException -> 0x03a4, Exception -> 0x0429, all -> 0x05a5 }
            r10.<init>(r9)     // Catch:{ FileNotFoundException -> 0x06a2, XmlPullParserException -> 0x031f, IOException -> 0x03a4, Exception -> 0x0429, all -> 0x05a5 }
            r23 = 0
            r0 = r14
            r1 = r10
            r2 = r23
            r0.setInput(r1, r2)     // Catch:{ FileNotFoundException -> 0x06ad, XmlPullParserException -> 0x0667, IOException -> 0x062c, Exception -> 0x05f1, all -> 0x05b2 }
            int r23 = r14.getEventType()     // Catch:{ FileNotFoundException -> 0x06ad, XmlPullParserException -> 0x0667, IOException -> 0x062c, Exception -> 0x05f1, all -> 0x05b2 }
            loadColumnList(r22)     // Catch:{ FileNotFoundException -> 0x06ad, XmlPullParserException -> 0x0667, IOException -> 0x062c, Exception -> 0x05f1, all -> 0x05b2 }
            r16 = r11
            r7 = r6
            r6 = r23
            r11 = r12
        L_0x0069:
            r23 = 1
            r0 = r6
            r1 = r23
            if (r0 != r1) goto L_0x00aa
            java.lang.String r23 = "Start updating conversation threads..."
            com.riteshsahu.SMSBackupRestoreBase.Common.logDebug(r23)     // Catch:{ FileNotFoundException -> 0x06b8, XmlPullParserException -> 0x0672, IOException -> 0x0637, Exception -> 0x05fc, all -> 0x05bf }
            r0 = r5
            r1 = r22
            r2 = r25
            r3 = r26
            updateThreads(r0, r1, r2, r3)     // Catch:{ FileNotFoundException -> 0x06b8, XmlPullParserException -> 0x0672, IOException -> 0x0637, Exception -> 0x05fc, all -> 0x05bf }
            java.lang.String r23 = "Conversation threads updated."
            com.riteshsahu.SMSBackupRestoreBase.Common.logDebug(r23)     // Catch:{ FileNotFoundException -> 0x06b8, XmlPullParserException -> 0x0672, IOException -> 0x0637, Exception -> 0x05fc, all -> 0x05bf }
            android.net.Uri r23 = com.riteshsahu.SMSBackupRestoreBase.Common.SmsContentUri     // Catch:{ FileNotFoundException -> 0x06b8, XmlPullParserException -> 0x0672, IOException -> 0x0637, Exception -> 0x05fc, all -> 0x05bf }
            r24 = 0
            r0 = r5
            r1 = r23
            r2 = r24
            r0.notifyChange(r1, r2)     // Catch:{ FileNotFoundException -> 0x06b8, XmlPullParserException -> 0x0672, IOException -> 0x0637, Exception -> 0x05fc, all -> 0x05bf }
            if (r10 == 0) goto L_0x0545
            r10.close()     // Catch:{ IOException -> 0x04fc }
            com.riteshsahu.SMSBackupRestoreBase.WakeLocker.releaseLock()
        L_0x0098:
            java.util.HashMap<java.lang.String, java.lang.Long> r22 = com.riteshsahu.SMSBackupRestoreBase.SmsRestoreProcessor.mAddresses
            r22.clear()
            clearColumnList()
            r15.setFailed(r7)
            r15.setSuccessful(r16)
            r15.setTotal(r11)
            return r15
        L_0x00aa:
            r23 = 0
            r8 = 0
            switch(r6) {
                case 2: goto L_0x00be;
                default: goto L_0x00b0;
            }
        L_0x00b0:
            r6 = r7
            r7 = r8
            r8 = r16
        L_0x00b4:
            int r23 = r14.next()     // Catch:{ FileNotFoundException -> 0x06d2, XmlPullParserException -> 0x0697, IOException -> 0x065c, Exception -> 0x0621, all -> 0x05e4 }
            r16 = r8
            r7 = r6
            r6 = r23
            goto L_0x0069
        L_0x00be:
            r6 = 1
            java.lang.Boolean r8 = java.lang.Boolean.valueOf(r6)     // Catch:{ FileNotFoundException -> 0x06b8, XmlPullParserException -> 0x0672, IOException -> 0x0637, Exception -> 0x05fc, all -> 0x05bf }
            java.lang.String r13 = r14.getName()     // Catch:{ FileNotFoundException -> 0x06b8, XmlPullParserException -> 0x0672, IOException -> 0x0637, Exception -> 0x05fc, all -> 0x05bf }
            java.lang.String r6 = "call"
            boolean r6 = r13.equalsIgnoreCase(r6)     // Catch:{ FileNotFoundException -> 0x06b8, XmlPullParserException -> 0x0672, IOException -> 0x0637, Exception -> 0x05fc, all -> 0x05bf }
            if (r6 != 0) goto L_0x00d7
            java.lang.String r6 = "sms"
            boolean r6 = r13.equalsIgnoreCase(r6)     // Catch:{ FileNotFoundException -> 0x06b8, XmlPullParserException -> 0x0672, IOException -> 0x0637, Exception -> 0x05fc, all -> 0x05bf }
            if (r6 == 0) goto L_0x02a0
        L_0x00d7:
            if (r27 == 0) goto L_0x00ec
            if (r27 == 0) goto L_0x0319
            java.lang.String r6 = ""
            java.lang.String r12 = "address"
            java.lang.String r6 = r14.getAttributeValue(r6, r12)     // Catch:{ FileNotFoundException -> 0x06b8, XmlPullParserException -> 0x0672, IOException -> 0x0637, Exception -> 0x05fc, all -> 0x05bf }
            r0 = r27
            r1 = r6
            boolean r6 = contactHasNumber(r0, r1)     // Catch:{ FileNotFoundException -> 0x06b8, XmlPullParserException -> 0x0672, IOException -> 0x0637, Exception -> 0x05fc, all -> 0x05bf }
            if (r6 == 0) goto L_0x0319
        L_0x00ec:
            int r18 = r11 + 1
            android.content.ContentValues r6 = new android.content.ContentValues     // Catch:{ FileNotFoundException -> 0x020e, XmlPullParserException -> 0x067d, IOException -> 0x0642, Exception -> 0x0607, all -> 0x05cc }
            r6.<init>()     // Catch:{ FileNotFoundException -> 0x020e, XmlPullParserException -> 0x067d, IOException -> 0x0642, Exception -> 0x0607, all -> 0x05cc }
            java.lang.String[] r11 = com.riteshsahu.SMSBackupRestoreBase.Common.ColumnNames     // Catch:{ FileNotFoundException -> 0x020e, XmlPullParserException -> 0x067d, IOException -> 0x0642, Exception -> 0x0607, all -> 0x05cc }
            int r12 = r11.length     // Catch:{ FileNotFoundException -> 0x020e, XmlPullParserException -> 0x067d, IOException -> 0x0642, Exception -> 0x0607, all -> 0x05cc }
            r23 = 0
            r19 = r23
            r23 = r8
        L_0x00fc:
            r0 = r19
            r1 = r12
            if (r0 < r1) goto L_0x013d
        L_0x0101:
            boolean r23 = r23.booleanValue()     // Catch:{ FileNotFoundException -> 0x020e, XmlPullParserException -> 0x067d, IOException -> 0x0642, Exception -> 0x0607, all -> 0x05cc }
            if (r23 == 0) goto L_0x06dd
            int r23 = r6.size()     // Catch:{ FileNotFoundException -> 0x020e, XmlPullParserException -> 0x067d, IOException -> 0x0642, Exception -> 0x0607, all -> 0x05cc }
            if (r23 != 0) goto L_0x0174
            java.lang.StringBuilder r23 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x020e, XmlPullParserException -> 0x067d, IOException -> 0x0642, Exception -> 0x0607, all -> 0x05cc }
            java.lang.String r8 = "Not adding sms because the value set is empty in message no: "
            r0 = r23
            r1 = r8
            r0.<init>(r1)     // Catch:{ FileNotFoundException -> 0x020e, XmlPullParserException -> 0x067d, IOException -> 0x0642, Exception -> 0x0607, all -> 0x05cc }
            r0 = r23
            r1 = r18
            java.lang.StringBuilder r23 = r0.append(r1)     // Catch:{ FileNotFoundException -> 0x020e, XmlPullParserException -> 0x067d, IOException -> 0x0642, Exception -> 0x0607, all -> 0x05cc }
            java.lang.String r23 = r23.toString()     // Catch:{ FileNotFoundException -> 0x020e, XmlPullParserException -> 0x067d, IOException -> 0x0642, Exception -> 0x0607, all -> 0x05cc }
            com.riteshsahu.SMSBackupRestoreBase.Common.logDebug(r23)     // Catch:{ FileNotFoundException -> 0x020e, XmlPullParserException -> 0x067d, IOException -> 0x0642, Exception -> 0x0607, all -> 0x05cc }
            int r23 = r7 + 1
            r7 = r16
        L_0x012a:
            r8 = 1
            r0 = r25
            r1 = r8
            r0.incrementProgressBy(r1)     // Catch:{ FileNotFoundException -> 0x06c3, XmlPullParserException -> 0x0688, IOException -> 0x064d, Exception -> 0x0612, all -> 0x05d9 }
            r11 = r18
            r8 = r7
            r7 = r13
            r21 = r6
            r6 = r23
            r23 = r21
            goto L_0x00b4
        L_0x013d:
            r23 = r11[r19]     // Catch:{ FileNotFoundException -> 0x020e, XmlPullParserException -> 0x067d, IOException -> 0x0642, Exception -> 0x0607, all -> 0x05cc }
            r0 = r6
            r1 = r23
            r2 = r14
            java.lang.Boolean r8 = setContentValue(r0, r1, r2)     // Catch:{ FileNotFoundException -> 0x020e, XmlPullParserException -> 0x067d, IOException -> 0x0642, Exception -> 0x0607, all -> 0x05cc }
            boolean r23 = r8.booleanValue()     // Catch:{ FileNotFoundException -> 0x020e, XmlPullParserException -> 0x067d, IOException -> 0x0642, Exception -> 0x0607, all -> 0x05cc }
            if (r23 != 0) goto L_0x016d
            java.lang.StringBuilder r23 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x020e, XmlPullParserException -> 0x067d, IOException -> 0x0642, Exception -> 0x0607, all -> 0x05cc }
            java.lang.String r11 = "Missing essential data in message no: "
            r0 = r23
            r1 = r11
            r0.<init>(r1)     // Catch:{ FileNotFoundException -> 0x020e, XmlPullParserException -> 0x067d, IOException -> 0x0642, Exception -> 0x0607, all -> 0x05cc }
            r0 = r23
            r1 = r18
            java.lang.StringBuilder r23 = r0.append(r1)     // Catch:{ FileNotFoundException -> 0x020e, XmlPullParserException -> 0x067d, IOException -> 0x0642, Exception -> 0x0607, all -> 0x05cc }
            java.lang.String r23 = r23.toString()     // Catch:{ FileNotFoundException -> 0x020e, XmlPullParserException -> 0x067d, IOException -> 0x0642, Exception -> 0x0607, all -> 0x05cc }
            com.riteshsahu.SMSBackupRestoreBase.Common.logDebug(r23)     // Catch:{ FileNotFoundException -> 0x020e, XmlPullParserException -> 0x067d, IOException -> 0x0642, Exception -> 0x0607, all -> 0x05cc }
            int r23 = r7 + 1
            r7 = r23
            r23 = r8
            goto L_0x0101
        L_0x016d:
            int r23 = r19 + 1
            r19 = r23
            r23 = r8
            goto L_0x00fc
        L_0x0174:
            adjustTypeToStopResending(r6)     // Catch:{ FileNotFoundException -> 0x020e, XmlPullParserException -> 0x067d, IOException -> 0x0642, Exception -> 0x0607, all -> 0x05cc }
            if (r4 == 0) goto L_0x01ab
            java.lang.String r23 = "type"
            r0 = r6
            r1 = r23
            java.lang.String r23 = r0.getAsString(r1)     // Catch:{ FileNotFoundException -> 0x020e, XmlPullParserException -> 0x067d, IOException -> 0x0642, Exception -> 0x0607, all -> 0x05cc }
            java.lang.String r8 = "1"
            r0 = r23
            r1 = r8
            boolean r23 = r0.equalsIgnoreCase(r1)     // Catch:{ FileNotFoundException -> 0x020e, XmlPullParserException -> 0x067d, IOException -> 0x0642, Exception -> 0x0607, all -> 0x05cc }
            if (r23 == 0) goto L_0x01ab
            java.lang.String r23 = "date"
            java.lang.String r8 = "date"
            java.lang.String r8 = r6.getAsString(r8)     // Catch:{ FileNotFoundException -> 0x020e, XmlPullParserException -> 0x067d, IOException -> 0x0642, Exception -> 0x0607, all -> 0x05cc }
            long r11 = java.lang.Long.parseLong(r8)     // Catch:{ FileNotFoundException -> 0x020e, XmlPullParserException -> 0x067d, IOException -> 0x0642, Exception -> 0x0607, all -> 0x05cc }
            r0 = r17
            long r0 = (long) r0     // Catch:{ FileNotFoundException -> 0x020e, XmlPullParserException -> 0x067d, IOException -> 0x0642, Exception -> 0x0607, all -> 0x05cc }
            r19 = r0
            long r11 = r11 - r19
            java.lang.Long r8 = java.lang.Long.valueOf(r11)     // Catch:{ FileNotFoundException -> 0x020e, XmlPullParserException -> 0x067d, IOException -> 0x0642, Exception -> 0x0607, all -> 0x05cc }
            r0 = r6
            r1 = r23
            r2 = r8
            r0.put(r1, r2)     // Catch:{ FileNotFoundException -> 0x020e, XmlPullParserException -> 0x067d, IOException -> 0x0642, Exception -> 0x0607, all -> 0x05cc }
        L_0x01ab:
            java.lang.String r23 = "address"
            r0 = r6
            r1 = r23
            java.lang.String r23 = r0.getAsString(r1)     // Catch:{ FileNotFoundException -> 0x020e, XmlPullParserException -> 0x067d, IOException -> 0x0642, Exception -> 0x0607, all -> 0x05cc }
            java.lang.Long r8 = getThreadIdForAddress(r23)     // Catch:{ FileNotFoundException -> 0x020e, XmlPullParserException -> 0x067d, IOException -> 0x0642, Exception -> 0x0607, all -> 0x05cc }
            if (r24 == 0) goto L_0x0295
            java.lang.String r11 = "address"
            java.lang.String r11 = r6.getAsString(r11)     // Catch:{ FileNotFoundException -> 0x020e, XmlPullParserException -> 0x067d, IOException -> 0x0642, Exception -> 0x0607, all -> 0x05cc }
            java.lang.String r12 = "date"
            java.lang.String r12 = r6.getAsString(r12)     // Catch:{ FileNotFoundException -> 0x020e, XmlPullParserException -> 0x067d, IOException -> 0x0642, Exception -> 0x0607, all -> 0x05cc }
            java.lang.String r19 = "protocol"
            r0 = r6
            r1 = r19
            java.lang.String r19 = r0.getAsString(r1)     // Catch:{ FileNotFoundException -> 0x020e, XmlPullParserException -> 0x067d, IOException -> 0x0642, Exception -> 0x0607, all -> 0x05cc }
            r0 = r22
            r1 = r11
            r2 = r12
            r3 = r19
            long r11 = getMessageId(r0, r1, r2, r3)     // Catch:{ FileNotFoundException -> 0x020e, XmlPullParserException -> 0x067d, IOException -> 0x0642, Exception -> 0x0607, all -> 0x05cc }
            r19 = 0
            int r11 = (r11 > r19 ? 1 : (r11 == r19 ? 0 : -1))
            if (r11 >= 0) goto L_0x01f8
            r0 = r5
            r1 = r6
            r2 = r22
            r3 = r8
            java.lang.Long r8 = insertMessage(r0, r1, r2, r3)     // Catch:{ FileNotFoundException -> 0x020e, XmlPullParserException -> 0x067d, IOException -> 0x0642, Exception -> 0x0607, all -> 0x05cc }
        L_0x01e8:
            r0 = r23
            r1 = r8
            addAddressToList(r0, r1)     // Catch:{ FileNotFoundException -> 0x020e, XmlPullParserException -> 0x067d, IOException -> 0x0642, Exception -> 0x0607, all -> 0x05cc }
            int r23 = r16 + 1
            r21 = r23
            r23 = r7
            r7 = r21
            goto L_0x012a
        L_0x01f8:
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x020e, XmlPullParserException -> 0x067d, IOException -> 0x0642, Exception -> 0x0607, all -> 0x05cc }
            java.lang.String r12 = "Ignoring duplicate message no: "
            r11.<init>(r12)     // Catch:{ FileNotFoundException -> 0x020e, XmlPullParserException -> 0x067d, IOException -> 0x0642, Exception -> 0x0607, all -> 0x05cc }
            r0 = r11
            r1 = r18
            java.lang.StringBuilder r11 = r0.append(r1)     // Catch:{ FileNotFoundException -> 0x020e, XmlPullParserException -> 0x067d, IOException -> 0x0642, Exception -> 0x0607, all -> 0x05cc }
            java.lang.String r11 = r11.toString()     // Catch:{ FileNotFoundException -> 0x020e, XmlPullParserException -> 0x067d, IOException -> 0x0642, Exception -> 0x0607, all -> 0x05cc }
            com.riteshsahu.SMSBackupRestoreBase.Common.logDebug(r11)     // Catch:{ FileNotFoundException -> 0x020e, XmlPullParserException -> 0x067d, IOException -> 0x0642, Exception -> 0x0607, all -> 0x05cc }
            goto L_0x01e8
        L_0x020e:
            r23 = move-exception
            r25 = r10
            r27 = r18
            r26 = r16
            r24 = r7
        L_0x0217:
            java.lang.StringBuilder r23 = new java.lang.StringBuilder     // Catch:{ all -> 0x0277 }
            java.lang.String r28 = "App version: "
            r0 = r23
            r1 = r28
            r0.<init>(r1)     // Catch:{ all -> 0x0277 }
            r28 = 2131230721(0x7f080001, float:1.8077503E38)
            r0 = r22
            r1 = r28
            java.lang.String r28 = r0.getString(r1)     // Catch:{ all -> 0x0277 }
            r0 = r23
            r1 = r28
            java.lang.StringBuilder r23 = r0.append(r1)     // Catch:{ all -> 0x0277 }
            java.lang.String r23 = r23.toString()     // Catch:{ all -> 0x0277 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r23)     // Catch:{ all -> 0x0277 }
            java.lang.StringBuilder r23 = new java.lang.StringBuilder     // Catch:{ all -> 0x0277 }
            java.lang.String r28 = "Could not find file - "
            r0 = r23
            r1 = r28
            r0.<init>(r1)     // Catch:{ all -> 0x0277 }
            r0 = r23
            r1 = r9
            java.lang.StringBuilder r23 = r0.append(r1)     // Catch:{ all -> 0x0277 }
            java.lang.String r23 = r23.toString()     // Catch:{ all -> 0x0277 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r23)     // Catch:{ all -> 0x0277 }
            com.riteshsahu.SMSBackupRestoreBase.CustomException r23 = new com.riteshsahu.SMSBackupRestoreBase.CustomException     // Catch:{ all -> 0x0277 }
            r28 = 2131230730(0x7f08000a, float:1.8077521E38)
            r0 = r22
            r1 = r28
            java.lang.String r28 = r0.getString(r1)     // Catch:{ all -> 0x0277 }
            r4 = 1
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x0277 }
            r5 = 0
            r4[r5] = r9     // Catch:{ all -> 0x0277 }
            r0 = r28
            r1 = r4
            java.lang.String r28 = java.lang.String.format(r0, r1)     // Catch:{ all -> 0x0277 }
            r0 = r23
            r1 = r28
            r0.<init>(r1)     // Catch:{ all -> 0x0277 }
            throw r23     // Catch:{ all -> 0x0277 }
        L_0x0277:
            r23 = move-exception
            r21 = r23
            r23 = r24
            r24 = r25
            r25 = r26
            r26 = r27
            r27 = r21
        L_0x0284:
            if (r24 == 0) goto L_0x04f7
            r24.close()     // Catch:{ IOException -> 0x04ae }
            com.riteshsahu.SMSBackupRestoreBase.WakeLocker.releaseLock()
        L_0x028c:
            java.util.HashMap<java.lang.String, java.lang.Long> r22 = com.riteshsahu.SMSBackupRestoreBase.SmsRestoreProcessor.mAddresses
            r22.clear()
            clearColumnList()
            throw r27
        L_0x0295:
            r0 = r5
            r1 = r6
            r2 = r22
            r3 = r8
            java.lang.Long r8 = insertMessage(r0, r1, r2, r3)     // Catch:{ FileNotFoundException -> 0x020e, XmlPullParserException -> 0x067d, IOException -> 0x0642, Exception -> 0x0607, all -> 0x05cc }
            goto L_0x01e8
        L_0x02a0:
            java.lang.String r6 = "smses"
            boolean r6 = r13.equalsIgnoreCase(r6)     // Catch:{ FileNotFoundException -> 0x06b8, XmlPullParserException -> 0x0672, IOException -> 0x0637, Exception -> 0x05fc, all -> 0x05bf }
            if (r6 == 0) goto L_0x0319
            if (r27 == 0) goto L_0x02bc
            java.lang.String r6 = "Restoring specified conversation, not loading count from file."
            com.riteshsahu.SMSBackupRestoreBase.Common.logDebug(r6)     // Catch:{ FileNotFoundException -> 0x06b8, XmlPullParserException -> 0x0672, IOException -> 0x0637, Exception -> 0x05fc, all -> 0x05bf }
            r0 = r25
            r1 = r28
            r0.setMax(r1)     // Catch:{ FileNotFoundException -> 0x06b8, XmlPullParserException -> 0x0672, IOException -> 0x0637, Exception -> 0x05fc, all -> 0x05bf }
            r8 = r16
            r6 = r7
            r7 = r13
            goto L_0x00b4
        L_0x02bc:
            java.lang.String r6 = "Trying to load message count in file..."
            com.riteshsahu.SMSBackupRestoreBase.Common.logDebug(r6)     // Catch:{ FileNotFoundException -> 0x06b8, XmlPullParserException -> 0x0672, IOException -> 0x0637, Exception -> 0x05fc, all -> 0x05bf }
            java.lang.String r6 = ""
            java.lang.String r8 = "count"
            java.lang.String r6 = r14.getAttributeValue(r6, r8)     // Catch:{ FileNotFoundException -> 0x06b8, XmlPullParserException -> 0x0672, IOException -> 0x0637, Exception -> 0x05fc, all -> 0x05bf }
            if (r6 == 0) goto L_0x0306
            java.lang.String r8 = ""
            if (r6 == r8) goto L_0x0306
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x06b8, XmlPullParserException -> 0x0672, IOException -> 0x0637, Exception -> 0x05fc, all -> 0x05bf }
            java.lang.String r12 = "Found count string: "
            r8.<init>(r12)     // Catch:{ FileNotFoundException -> 0x06b8, XmlPullParserException -> 0x0672, IOException -> 0x0637, Exception -> 0x05fc, all -> 0x05bf }
            java.lang.StringBuilder r8 = r8.append(r6)     // Catch:{ FileNotFoundException -> 0x06b8, XmlPullParserException -> 0x0672, IOException -> 0x0637, Exception -> 0x05fc, all -> 0x05bf }
            java.lang.String r8 = r8.toString()     // Catch:{ FileNotFoundException -> 0x06b8, XmlPullParserException -> 0x0672, IOException -> 0x0637, Exception -> 0x05fc, all -> 0x05bf }
            com.riteshsahu.SMSBackupRestoreBase.Common.logDebug(r8)     // Catch:{ FileNotFoundException -> 0x06b8, XmlPullParserException -> 0x0672, IOException -> 0x0637, Exception -> 0x05fc, all -> 0x05bf }
            int r6 = java.lang.Integer.parseInt(r6)     // Catch:{ NumberFormatException -> 0x02f1 }
            r0 = r25
            r1 = r6
            r0.setMax(r1)     // Catch:{ NumberFormatException -> 0x02f1 }
            r8 = r16
            r6 = r7
            r7 = r13
            goto L_0x00b4
        L_0x02f1:
            r6 = move-exception
            r6 = 2131230815(0x7f08005f, float:1.8077693E38)
            r8 = 10000(0x2710, float:1.4013E-41)
            r0 = r25
            r1 = r26
            r2 = r6
            r3 = r8
            com.riteshsahu.SMSBackupRestoreBase.Common.resetProgressHandler(r0, r1, r2, r3)     // Catch:{ FileNotFoundException -> 0x06b8, XmlPullParserException -> 0x0672, IOException -> 0x0637, Exception -> 0x05fc, all -> 0x05bf }
            r8 = r16
            r6 = r7
            r7 = r13
            goto L_0x00b4
        L_0x0306:
            java.lang.String r6 = "Count not found."
            com.riteshsahu.SMSBackupRestoreBase.Common.logDebug(r6)     // Catch:{ FileNotFoundException -> 0x06b8, XmlPullParserException -> 0x0672, IOException -> 0x0637, Exception -> 0x05fc, all -> 0x05bf }
            r6 = 2131230815(0x7f08005f, float:1.8077693E38)
            r8 = 10000(0x2710, float:1.4013E-41)
            r0 = r25
            r1 = r26
            r2 = r6
            r3 = r8
            com.riteshsahu.SMSBackupRestoreBase.Common.resetProgressHandler(r0, r1, r2, r3)     // Catch:{ FileNotFoundException -> 0x06b8, XmlPullParserException -> 0x0672, IOException -> 0x0637, Exception -> 0x05fc, all -> 0x05bf }
        L_0x0319:
            r8 = r16
            r6 = r7
            r7 = r13
            goto L_0x00b4
        L_0x031f:
            r23 = move-exception
            r25 = r8
            r27 = r12
            r26 = r11
            r24 = r6
        L_0x0328:
            r23.printStackTrace()     // Catch:{ all -> 0x0277 }
            java.lang.StringBuilder r28 = new java.lang.StringBuilder     // Catch:{ all -> 0x0277 }
            java.lang.String r4 = "App version: "
            r0 = r28
            r1 = r4
            r0.<init>(r1)     // Catch:{ all -> 0x0277 }
            r4 = 2131230721(0x7f080001, float:1.8077503E38)
            r0 = r22
            r1 = r4
            java.lang.String r4 = r0.getString(r1)     // Catch:{ all -> 0x0277 }
            r0 = r28
            r1 = r4
            java.lang.StringBuilder r28 = r0.append(r1)     // Catch:{ all -> 0x0277 }
            java.lang.String r28 = r28.toString()     // Catch:{ all -> 0x0277 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r28)     // Catch:{ all -> 0x0277 }
            java.lang.StringBuilder r28 = new java.lang.StringBuilder     // Catch:{ all -> 0x0277 }
            java.lang.String r4 = "Count: "
            r0 = r28
            r1 = r4
            r0.<init>(r1)     // Catch:{ all -> 0x0277 }
            r0 = r28
            r1 = r27
            java.lang.StringBuilder r28 = r0.append(r1)     // Catch:{ all -> 0x0277 }
            java.lang.String r28 = r28.toString()     // Catch:{ all -> 0x0277 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r28)     // Catch:{ all -> 0x0277 }
            java.lang.StringBuilder r28 = new java.lang.StringBuilder     // Catch:{ all -> 0x0277 }
            java.lang.String r4 = "Error Parsing xml - "
            r0 = r28
            r1 = r4
            r0.<init>(r1)     // Catch:{ all -> 0x0277 }
            java.lang.String r4 = r23.getMessage()     // Catch:{ all -> 0x0277 }
            r0 = r28
            r1 = r4
            java.lang.StringBuilder r28 = r0.append(r1)     // Catch:{ all -> 0x0277 }
            java.lang.String r28 = r28.toString()     // Catch:{ all -> 0x0277 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r28)     // Catch:{ all -> 0x0277 }
            com.riteshsahu.SMSBackupRestoreBase.CustomException r28 = new com.riteshsahu.SMSBackupRestoreBase.CustomException     // Catch:{ all -> 0x0277 }
            r4 = 2131230889(0x7f0800a9, float:1.8077844E38)
            r0 = r22
            r1 = r4
            java.lang.String r4 = r0.getString(r1)     // Catch:{ all -> 0x0277 }
            r5 = 1
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ all -> 0x0277 }
            r6 = 0
            java.lang.String r23 = r23.getMessage()     // Catch:{ all -> 0x0277 }
            r5[r6] = r23     // Catch:{ all -> 0x0277 }
            java.lang.String r23 = java.lang.String.format(r4, r5)     // Catch:{ all -> 0x0277 }
            r0 = r28
            r1 = r23
            r0.<init>(r1)     // Catch:{ all -> 0x0277 }
            throw r28     // Catch:{ all -> 0x0277 }
        L_0x03a4:
            r23 = move-exception
            r25 = r8
            r27 = r12
            r26 = r11
            r24 = r6
        L_0x03ad:
            r23.printStackTrace()     // Catch:{ all -> 0x0277 }
            java.lang.StringBuilder r28 = new java.lang.StringBuilder     // Catch:{ all -> 0x0277 }
            java.lang.String r4 = "App version: "
            r0 = r28
            r1 = r4
            r0.<init>(r1)     // Catch:{ all -> 0x0277 }
            r4 = 2131230721(0x7f080001, float:1.8077503E38)
            r0 = r22
            r1 = r4
            java.lang.String r4 = r0.getString(r1)     // Catch:{ all -> 0x0277 }
            r0 = r28
            r1 = r4
            java.lang.StringBuilder r28 = r0.append(r1)     // Catch:{ all -> 0x0277 }
            java.lang.String r28 = r28.toString()     // Catch:{ all -> 0x0277 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r28)     // Catch:{ all -> 0x0277 }
            java.lang.StringBuilder r28 = new java.lang.StringBuilder     // Catch:{ all -> 0x0277 }
            java.lang.String r4 = "Count: "
            r0 = r28
            r1 = r4
            r0.<init>(r1)     // Catch:{ all -> 0x0277 }
            r0 = r28
            r1 = r27
            java.lang.StringBuilder r28 = r0.append(r1)     // Catch:{ all -> 0x0277 }
            java.lang.String r28 = r28.toString()     // Catch:{ all -> 0x0277 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r28)     // Catch:{ all -> 0x0277 }
            java.lang.StringBuilder r28 = new java.lang.StringBuilder     // Catch:{ all -> 0x0277 }
            java.lang.String r4 = "IOException - "
            r0 = r28
            r1 = r4
            r0.<init>(r1)     // Catch:{ all -> 0x0277 }
            java.lang.String r4 = r23.getMessage()     // Catch:{ all -> 0x0277 }
            r0 = r28
            r1 = r4
            java.lang.StringBuilder r28 = r0.append(r1)     // Catch:{ all -> 0x0277 }
            java.lang.String r28 = r28.toString()     // Catch:{ all -> 0x0277 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r28)     // Catch:{ all -> 0x0277 }
            com.riteshsahu.SMSBackupRestoreBase.CustomException r28 = new com.riteshsahu.SMSBackupRestoreBase.CustomException     // Catch:{ all -> 0x0277 }
            r4 = 2131230726(0x7f080006, float:1.8077513E38)
            r0 = r22
            r1 = r4
            java.lang.String r4 = r0.getString(r1)     // Catch:{ all -> 0x0277 }
            r5 = 1
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ all -> 0x0277 }
            r6 = 0
            java.lang.String r23 = r23.getMessage()     // Catch:{ all -> 0x0277 }
            r5[r6] = r23     // Catch:{ all -> 0x0277 }
            java.lang.String r23 = java.lang.String.format(r4, r5)     // Catch:{ all -> 0x0277 }
            r0 = r28
            r1 = r23
            r0.<init>(r1)     // Catch:{ all -> 0x0277 }
            throw r28     // Catch:{ all -> 0x0277 }
        L_0x0429:
            r23 = move-exception
            r25 = r8
            r27 = r12
            r26 = r11
            r24 = r6
        L_0x0432:
            r23.printStackTrace()     // Catch:{ all -> 0x0277 }
            java.lang.StringBuilder r28 = new java.lang.StringBuilder     // Catch:{ all -> 0x0277 }
            java.lang.String r4 = "App version: "
            r0 = r28
            r1 = r4
            r0.<init>(r1)     // Catch:{ all -> 0x0277 }
            r4 = 2131230721(0x7f080001, float:1.8077503E38)
            r0 = r22
            r1 = r4
            java.lang.String r4 = r0.getString(r1)     // Catch:{ all -> 0x0277 }
            r0 = r28
            r1 = r4
            java.lang.StringBuilder r28 = r0.append(r1)     // Catch:{ all -> 0x0277 }
            java.lang.String r28 = r28.toString()     // Catch:{ all -> 0x0277 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r28)     // Catch:{ all -> 0x0277 }
            java.lang.StringBuilder r28 = new java.lang.StringBuilder     // Catch:{ all -> 0x0277 }
            java.lang.String r4 = "Count: "
            r0 = r28
            r1 = r4
            r0.<init>(r1)     // Catch:{ all -> 0x0277 }
            r0 = r28
            r1 = r27
            java.lang.StringBuilder r28 = r0.append(r1)     // Catch:{ all -> 0x0277 }
            java.lang.String r28 = r28.toString()     // Catch:{ all -> 0x0277 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r28)     // Catch:{ all -> 0x0277 }
            java.lang.StringBuilder r28 = new java.lang.StringBuilder     // Catch:{ all -> 0x0277 }
            java.lang.String r4 = "Exception - "
            r0 = r28
            r1 = r4
            r0.<init>(r1)     // Catch:{ all -> 0x0277 }
            java.lang.String r4 = r23.getMessage()     // Catch:{ all -> 0x0277 }
            r0 = r28
            r1 = r4
            java.lang.StringBuilder r28 = r0.append(r1)     // Catch:{ all -> 0x0277 }
            java.lang.String r28 = r28.toString()     // Catch:{ all -> 0x0277 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r28)     // Catch:{ all -> 0x0277 }
            com.riteshsahu.SMSBackupRestoreBase.CustomException r28 = new com.riteshsahu.SMSBackupRestoreBase.CustomException     // Catch:{ all -> 0x0277 }
            r4 = 2131230726(0x7f080006, float:1.8077513E38)
            r0 = r22
            r1 = r4
            java.lang.String r4 = r0.getString(r1)     // Catch:{ all -> 0x0277 }
            r5 = 1
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ all -> 0x0277 }
            r6 = 0
            java.lang.String r23 = r23.getMessage()     // Catch:{ all -> 0x0277 }
            r5[r6] = r23     // Catch:{ all -> 0x0277 }
            java.lang.String r23 = java.lang.String.format(r4, r5)     // Catch:{ all -> 0x0277 }
            r0 = r28
            r1 = r23
            r0.<init>(r1)     // Catch:{ all -> 0x0277 }
            throw r28     // Catch:{ all -> 0x0277 }
        L_0x04ae:
            r23 = move-exception
            java.lang.StringBuilder r24 = new java.lang.StringBuilder     // Catch:{ all -> 0x04f2 }
            java.lang.String r25 = "IOException "
            r24.<init>(r25)     // Catch:{ all -> 0x04f2 }
            java.lang.String r25 = r23.getMessage()     // Catch:{ all -> 0x04f2 }
            java.lang.StringBuilder r24 = r24.append(r25)     // Catch:{ all -> 0x04f2 }
            java.lang.String r24 = r24.toString()     // Catch:{ all -> 0x04f2 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logDebug(r24)     // Catch:{ all -> 0x04f2 }
            com.riteshsahu.SMSBackupRestoreBase.CustomException r24 = new com.riteshsahu.SMSBackupRestoreBase.CustomException     // Catch:{ all -> 0x04f2 }
            r25 = 2131230726(0x7f080006, float:1.8077513E38)
            r0 = r22
            r1 = r25
            java.lang.String r22 = r0.getString(r1)     // Catch:{ all -> 0x04f2 }
            r25 = 1
            r0 = r25
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ all -> 0x04f2 }
            r25 = r0
            r26 = 0
            java.lang.String r23 = r23.getMessage()     // Catch:{ all -> 0x04f2 }
            r25[r26] = r23     // Catch:{ all -> 0x04f2 }
            r0 = r22
            r1 = r25
            java.lang.String r22 = java.lang.String.format(r0, r1)     // Catch:{ all -> 0x04f2 }
            r0 = r24
            r1 = r22
            r0.<init>(r1)     // Catch:{ all -> 0x04f2 }
            throw r24     // Catch:{ all -> 0x04f2 }
        L_0x04f2:
            r22 = move-exception
            com.riteshsahu.SMSBackupRestoreBase.WakeLocker.releaseLock()
            throw r22
        L_0x04f7:
            com.riteshsahu.SMSBackupRestoreBase.WakeLocker.releaseLock()
            goto L_0x028c
        L_0x04fc:
            r23 = move-exception
            java.lang.StringBuilder r24 = new java.lang.StringBuilder     // Catch:{ all -> 0x0540 }
            java.lang.String r25 = "IOException "
            r24.<init>(r25)     // Catch:{ all -> 0x0540 }
            java.lang.String r25 = r23.getMessage()     // Catch:{ all -> 0x0540 }
            java.lang.StringBuilder r24 = r24.append(r25)     // Catch:{ all -> 0x0540 }
            java.lang.String r24 = r24.toString()     // Catch:{ all -> 0x0540 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logDebug(r24)     // Catch:{ all -> 0x0540 }
            com.riteshsahu.SMSBackupRestoreBase.CustomException r24 = new com.riteshsahu.SMSBackupRestoreBase.CustomException     // Catch:{ all -> 0x0540 }
            r25 = 2131230726(0x7f080006, float:1.8077513E38)
            r0 = r22
            r1 = r25
            java.lang.String r22 = r0.getString(r1)     // Catch:{ all -> 0x0540 }
            r25 = 1
            r0 = r25
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ all -> 0x0540 }
            r25 = r0
            r26 = 0
            java.lang.String r23 = r23.getMessage()     // Catch:{ all -> 0x0540 }
            r25[r26] = r23     // Catch:{ all -> 0x0540 }
            r0 = r22
            r1 = r25
            java.lang.String r22 = java.lang.String.format(r0, r1)     // Catch:{ all -> 0x0540 }
            r0 = r24
            r1 = r22
            r0.<init>(r1)     // Catch:{ all -> 0x0540 }
            throw r24     // Catch:{ all -> 0x0540 }
        L_0x0540:
            r22 = move-exception
            com.riteshsahu.SMSBackupRestoreBase.WakeLocker.releaseLock()
            throw r22
        L_0x0545:
            com.riteshsahu.SMSBackupRestoreBase.WakeLocker.releaseLock()
            goto L_0x0098
        L_0x054a:
            java.lang.StringBuilder r23 = new java.lang.StringBuilder
            java.lang.String r24 = "App version: "
            r23.<init>(r24)
            r24 = 2131230721(0x7f080001, float:1.8077503E38)
            r0 = r22
            r1 = r24
            java.lang.String r24 = r0.getString(r1)
            java.lang.StringBuilder r23 = r23.append(r24)
            java.lang.String r23 = r23.toString()
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r23)
            java.lang.StringBuilder r23 = new java.lang.StringBuilder
            java.lang.String r24 = "Could not read file: "
            r23.<init>(r24)
            r0 = r23
            r1 = r9
            java.lang.StringBuilder r23 = r0.append(r1)
            java.lang.String r23 = r23.toString()
            com.riteshsahu.SMSBackupRestoreBase.Common.logDebug(r23)
            com.riteshsahu.SMSBackupRestoreBase.CustomException r23 = new com.riteshsahu.SMSBackupRestoreBase.CustomException
            r24 = 2131230736(0x7f080010, float:1.8077533E38)
            r0 = r22
            r1 = r24
            java.lang.String r22 = r0.getString(r1)
            r24 = 1
            r0 = r24
            java.lang.Object[] r0 = new java.lang.Object[r0]
            r24 = r0
            r25 = 0
            r24[r25] = r9
            r0 = r22
            r1 = r24
            java.lang.String r22 = java.lang.String.format(r0, r1)
            r0 = r23
            r1 = r22
            r0.<init>(r1)
            throw r23
        L_0x05a5:
            r23 = move-exception
            r27 = r23
            r24 = r8
            r26 = r12
            r25 = r11
            r23 = r6
            goto L_0x0284
        L_0x05b2:
            r23 = move-exception
            r27 = r23
            r24 = r10
            r26 = r12
            r25 = r11
            r23 = r6
            goto L_0x0284
        L_0x05bf:
            r23 = move-exception
            r27 = r23
            r24 = r10
            r26 = r11
            r25 = r16
            r23 = r7
            goto L_0x0284
        L_0x05cc:
            r23 = move-exception
            r27 = r23
            r24 = r10
            r26 = r18
            r25 = r16
            r23 = r7
            goto L_0x0284
        L_0x05d9:
            r24 = move-exception
            r27 = r24
            r26 = r18
            r25 = r7
            r24 = r10
            goto L_0x0284
        L_0x05e4:
            r23 = move-exception
            r27 = r23
            r24 = r10
            r26 = r11
            r25 = r8
            r23 = r6
            goto L_0x0284
        L_0x05f1:
            r23 = move-exception
            r25 = r10
            r27 = r12
            r26 = r11
            r24 = r6
            goto L_0x0432
        L_0x05fc:
            r23 = move-exception
            r25 = r10
            r27 = r11
            r26 = r16
            r24 = r7
            goto L_0x0432
        L_0x0607:
            r23 = move-exception
            r25 = r10
            r27 = r18
            r26 = r16
            r24 = r7
            goto L_0x0432
        L_0x0612:
            r24 = move-exception
            r25 = r10
            r27 = r18
            r26 = r7
            r21 = r24
            r24 = r23
            r23 = r21
            goto L_0x0432
        L_0x0621:
            r23 = move-exception
            r25 = r10
            r27 = r11
            r26 = r8
            r24 = r6
            goto L_0x0432
        L_0x062c:
            r23 = move-exception
            r25 = r10
            r27 = r12
            r26 = r11
            r24 = r6
            goto L_0x03ad
        L_0x0637:
            r23 = move-exception
            r25 = r10
            r27 = r11
            r26 = r16
            r24 = r7
            goto L_0x03ad
        L_0x0642:
            r23 = move-exception
            r25 = r10
            r27 = r18
            r26 = r16
            r24 = r7
            goto L_0x03ad
        L_0x064d:
            r24 = move-exception
            r25 = r10
            r27 = r18
            r26 = r7
            r21 = r24
            r24 = r23
            r23 = r21
            goto L_0x03ad
        L_0x065c:
            r23 = move-exception
            r25 = r10
            r27 = r11
            r26 = r8
            r24 = r6
            goto L_0x03ad
        L_0x0667:
            r23 = move-exception
            r25 = r10
            r27 = r12
            r26 = r11
            r24 = r6
            goto L_0x0328
        L_0x0672:
            r23 = move-exception
            r25 = r10
            r27 = r11
            r26 = r16
            r24 = r7
            goto L_0x0328
        L_0x067d:
            r23 = move-exception
            r25 = r10
            r27 = r18
            r26 = r16
            r24 = r7
            goto L_0x0328
        L_0x0688:
            r24 = move-exception
            r25 = r10
            r27 = r18
            r26 = r7
            r21 = r24
            r24 = r23
            r23 = r21
            goto L_0x0328
        L_0x0697:
            r23 = move-exception
            r25 = r10
            r27 = r11
            r26 = r8
            r24 = r6
            goto L_0x0328
        L_0x06a2:
            r23 = move-exception
            r25 = r8
            r27 = r12
            r26 = r11
            r24 = r6
            goto L_0x0217
        L_0x06ad:
            r23 = move-exception
            r25 = r10
            r27 = r12
            r26 = r11
            r24 = r6
            goto L_0x0217
        L_0x06b8:
            r23 = move-exception
            r25 = r10
            r27 = r11
            r26 = r16
            r24 = r7
            goto L_0x0217
        L_0x06c3:
            r24 = move-exception
            r25 = r10
            r27 = r18
            r26 = r7
            r21 = r24
            r24 = r23
            r23 = r21
            goto L_0x0217
        L_0x06d2:
            r23 = move-exception
            r25 = r10
            r27 = r11
            r26 = r8
            r24 = r6
            goto L_0x0217
        L_0x06dd:
            r23 = r7
            r7 = r16
            goto L_0x012a
        L_0x06e3:
            r17 = r7
            goto L_0x0039
        */
        throw new UnsupportedOperationException("Method not decompiled: com.riteshsahu.SMSBackupRestoreBase.SmsRestoreProcessor.loadXml(android.content.Context, com.riteshsahu.SMSBackupRestoreBase.BackupFile, boolean, android.app.ProgressDialog, android.os.Handler, com.riteshsahu.SMSBackupRestoreBase.ContactNumbers, int):com.riteshsahu.SMSBackupRestoreBase.OperationResult");
    }

    private static void adjustTypeToStopResending(ContentValues contentValueToSave) {
        String messageType = contentValueToSave.getAsString(Common.TypeAttributeName);
        if (messageType.equalsIgnoreCase(Common.MESSAGE_TYPE_OUTBOX) || messageType.equalsIgnoreCase(Common.MESSAGE_TYPE_QUEUED)) {
            Common.logDebug("Changing Type from: " + messageType + " to Sent");
            contentValueToSave.put(Common.TypeAttributeName, Common.MESSAGE_TYPE_SENT);
        }
    }

    private static List<ContactNumbers> mergeContacts(List<Contact> contacts) {
        if (contacts == null || contacts.size() == 0) {
            return null;
        }
        Collections.sort(contacts, new ContactIdComparer());
        List<ContactNumbers> mergedContacts = new ArrayList<>();
        Contact contact = contacts.get(0);
        ContactNumbers cn = new ContactNumbers(contact.getName(), contact.getAddress(), contact.getBody(), contact.getCount(), contact.getDate().longValue());
        mergedContacts.add(cn);
        String id = contact.getId();
        for (int x = 1; x < contacts.size(); x++) {
            Contact contact2 = contacts.get(x);
            if (id.equalsIgnoreCase(contact2.getId())) {
                cn.addNumber(contact2.getAddress(), contact2.getBody(), contact2.getCount(), contact2.getDate().longValue());
            } else {
                cn = new ContactNumbers(contact2.getName(), contact2.getAddress(), contact2.getBody(), contact2.getCount(), contact2.getDate().longValue());
                id = contact2.getId();
                mergedContacts.add(cn);
            }
        }
        return mergedContacts;
    }

    private static Boolean setContentValue(ContentValues contentValue, String columnName, KXmlParser parser) {
        if (mColumnList.contains(columnName)) {
            String value = parser.getAttributeValue("", columnName);
            if (value == null || value.equalsIgnoreCase(Common.NullString)) {
                for (String mandatoryColumn : Common.MandatoryColumnNames) {
                    if (mandatoryColumn.equalsIgnoreCase(columnName)) {
                        return false;
                    }
                }
            } else {
                contentValue.put(columnName, value);
                return true;
            }
        }
        return true;
    }

    private static boolean isIncomingMessage(KXmlParser parser) {
        return parser.getAttributeValue("", Common.TypeAttributeName).equalsIgnoreCase(Common.MESSAGE_TYPE_INBOX);
    }

    private static void updateContactList(Context context, List<Contact> contacts, String body, String number, long date) {
        int index = Collections.binarySearch(contacts, new Contact(number));
        if (index < 0) {
            Contact contact = Common.getContactForNumber(context, number);
            contact.setDate(date);
            contact.setBody(body);
            contacts.add(contact);
            Collections.sort(contacts);
            return;
        }
        contacts.get(index).updateDateAndIncrementCount(date, body);
    }

    private static void updateThreads(ContentResolver contentResolver, Context context, ProgressDialog progressDialog, Handler handler) {
        long currentDate = System.currentTimeMillis();
        Common.resetProgressHandler(progressDialog, handler, R.string.updating_threads, mAddresses.size());
        progressDialog.setProgress(0);
        for (String address : mAddresses.keySet()) {
            ContentValues contentValueToSave = new ContentValues();
            contentValueToSave.put(Common.AddressAttributeName, address);
            contentValueToSave.put(Common.DateAttributeName, Long.valueOf(currentDate));
            contentValueToSave.put(Common.TypeAttributeName, Common.MESSAGE_TYPE_SENT);
            contentValueToSave.put(Common.ReadAttributeName, Common.MESSAGE_TYPE_INBOX);
            contentValueToSave.put(Common.ThreadIdColumnName, getThreadIdForAddress(address));
            contentValueToSave.put(Common.BodyAttributeName, Common.TempMessage);
            contentResolver.delete(contentResolver.insert(Common.SmsContentUri, contentValueToSave), null, null);
            progressDialog.incrementProgressBy(1);
        }
    }
}
