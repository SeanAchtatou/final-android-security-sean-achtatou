package com.riteshsahu.SMSBackupRestoreBase;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.ClipboardManager;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.riteshsahu.SMSBackupRestore.R;
import java.util.ArrayList;
import java.util.Collections;

public class MessageView extends ListActivity {
    private static ContactNumbers mAddressFilter = null;
    /* access modifiers changed from: private */
    public static String mErrorMessage;
    final ActivityHelper mActivityHelper = ActivityHelper.createInstance(this);
    private MessageAdapter mAdapter;
    private final View.OnCreateContextMenuListener mConvListOnCreateContextMenuListener = new View.OnCreateContextMenuListener() {
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            if (((AdapterView.AdapterContextMenuInfo) menuInfo).position > -1) {
                menu.setHeaderTitle((int) R.string.message_options);
                menu.add(0, (int) R.string.menu_forward, 0, (int) R.string.menu_forward);
                menu.add(0, (int) R.string.menu_clipboard, 0, (int) R.string.menu_clipboard);
            }
        }
    };
    /* access modifiers changed from: private */
    public String mMe;
    private ArrayList<Message> mMessages = null;
    /* access modifiers changed from: private */
    public String mOtherPartyName;
    /* access modifiers changed from: private */
    public ProgressDialog mProgressDialog = null;
    private Runnable returnRes = new Runnable() {
        public void run() {
            if (MessageView.mErrorMessage == null || MessageView.mErrorMessage.length() == 0) {
                MessageView.this.updateAdapter();
                MessageView.this.mProgressDialog.dismiss();
                return;
            }
            new AlertDialog.Builder(MessageView.this).setIcon((int) R.drawable.icon).setTitle(MessageView.this.getText(R.string.app_name)).setMessage(String.format(MessageView.this.getString(R.string.error_message), MessageView.mErrorMessage)).setPositiveButton((int) R.string.button_close, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    MessageView.this.finish();
                }
            }).create().show();
            MessageView.this.mProgressDialog.dismiss();
        }
    };
    private Runnable viewMessages;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mErrorMessage = "";
        setContentView((int) R.layout.message_list);
        if (mAddressFilter != null) {
            this.mMessages = new ArrayList<>();
            this.mAdapter = new MessageAdapter(this, R.layout.message_row, this.mMessages);
            setListAdapter(this.mAdapter);
            this.mOtherPartyName = mAddressFilter.getNameOrNumber();
            this.mMe = getString(R.string.me);
            this.mActivityHelper.setupActionBar(String.format(getString(R.string.conversation_with), mAddressFilter.getNameAndNumber()), 0);
            addAdditionalControls();
            this.viewMessages = new Runnable() {
                public void run() {
                    MessageView.this.getMessages();
                }
            };
            new Thread(null, this.viewMessages, "MessageViewBackground").start();
            this.mProgressDialog = ProgressDialog.show(this, getString(R.string.please_wait), getString(R.string.analysing_file), true);
        }
        getListView().setOnCreateContextMenuListener(this.mConvListOnCreateContextMenuListener);
    }

    /* access modifiers changed from: protected */
    public void addAdditionalControls() {
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, (int) R.string.menu_newest_first, 0, (int) R.string.menu_newest_first).setIcon(17301661);
        return true;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        if (Common.getBooleanPreference(this, PreferenceKeys.SortMessageAscending).booleanValue()) {
            menu.add(0, (int) R.string.menu_newest_first, 0, (int) R.string.menu_newest_first);
        } else {
            menu.add(0, (int) R.string.menu_newest_first, 0, (int) R.string.menu_oldest_first);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.string.menu_newest_first:
                changeSortOrder(!Common.getBooleanPreference(this, PreferenceKeys.SortMessageAscending).booleanValue());
                break;
        }
        return true;
    }

    private void changeSortOrder(boolean ascending) {
        Common.setBooleanPreference(this, PreferenceKeys.SortMessageAscending, Boolean.valueOf(ascending));
        Toast.makeText(this, ascending ? R.string.showing_oldest_first : R.string.showing_newest_first, 0).show();
        updateAdapter();
    }

    public boolean onContextItemSelected(MenuItem item) {
        try {
            Message message = (Message) getListView().getItemAtPosition(((AdapterView.AdapterContextMenuInfo) item.getMenuInfo()).position);
            switch (item.getItemId()) {
                case R.string.menu_clipboard:
                    ((ClipboardManager) getSystemService("clipboard")).setText(message.getBody());
                    Toast.makeText(this, getResources().getText(R.string.message_copied_clipboard), 0).show();
                    break;
                case R.string.menu_forward:
                    Intent sendIntent = new Intent("android.intent.action.SEND");
                    sendIntent.putExtra("android.intent.extra.TEXT", message.getBody());
                    sendIntent.setType("text/plain");
                    startActivity(Intent.createChooser(sendIntent, getText(R.string.menu_forward)));
                    break;
            }
            return super.onContextItemSelected(item);
        } catch (ClassCastException e) {
            Common.logError("Bad menuInfo", e);
            return false;
        }
    }

    /* access modifiers changed from: private */
    public void updateAdapter() {
        if (this.mMessages != null && this.mMessages.size() > 0) {
            if (Common.getBooleanPreference(this, PreferenceKeys.SortMessageAscending).booleanValue()) {
                Collections.sort(this.mMessages, new MessageComparer());
            } else {
                Collections.sort(this.mMessages);
            }
            this.mAdapter.clear();
            int size = this.mMessages.size();
            for (int i = 0; i < size; i++) {
                this.mAdapter.add(this.mMessages.get(i));
            }
        }
        if (this.mAdapter != null) {
            this.mAdapter.notifyDataSetChanged();
        }
    }

    /* access modifiers changed from: private */
    public void getMessages() {
        try {
            this.mMessages = SmsRestoreProcessor.getMessageList(this, Common.getCurrentBackupFile(), mAddressFilter);
            Common.logDebug("Messages loaded: " + this.mMessages.size());
        } catch (Exception e) {
            mErrorMessage = e.getMessage();
        }
        runOnUiThread(this.returnRes);
    }

    public static void setAddressFilter(ContactNumbers addressFilter) {
        mAddressFilter = addressFilter;
    }

    private class MessageAdapter extends ArrayAdapter<Message> {
        private ArrayList<Message> items;

        public MessageAdapter(Context context, int textViewResourceId, ArrayList<Message> items2) {
            super(context, textViewResourceId, items2);
            this.items = items2;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            if (v == null) {
                v = ((LayoutInflater) MessageView.this.getSystemService("layout_inflater")).inflate((int) R.layout.message_row, (ViewGroup) null);
                v.setBackgroundColor(17170447);
            }
            Message message = this.items.get(position);
            if (message != null) {
                ((TextView) v.findViewById(R.id.message_body)).setText(message.getBody());
                ((TextView) v.findViewById(R.id.message_date)).setText(message.getDateText());
                if (message.getIncoming().booleanValue()) {
                    ((TextView) v.findViewById(R.id.message_sender)).setText(String.valueOf(MessageView.this.mOtherPartyName) + ": ");
                } else {
                    ((TextView) v.findViewById(R.id.message_sender)).setText(String.valueOf(MessageView.this.mMe) + ": ");
                }
            }
            return v;
        }
    }
}
