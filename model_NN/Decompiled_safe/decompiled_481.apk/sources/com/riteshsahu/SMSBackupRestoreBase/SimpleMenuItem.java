package com.riteshsahu.SMSBackupRestoreBase;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;

public class SimpleMenuItem implements MenuItem {
    private boolean mEnabled = true;
    private Drawable mIconDrawable;
    private int mIconResId = 0;
    private final int mId;
    private SimpleMenu mMenu;
    private final int mOrder;
    private CharSequence mTitle;
    private CharSequence mTitleCondensed;

    public SimpleMenuItem(SimpleMenu menu, int id, int order, CharSequence title) {
        this.mMenu = menu;
        this.mId = id;
        this.mOrder = order;
        this.mTitle = title;
    }

    public int getItemId() {
        return this.mId;
    }

    public int getOrder() {
        return this.mOrder;
    }

    public MenuItem setTitle(CharSequence title) {
        this.mTitle = title;
        return this;
    }

    public MenuItem setTitle(int titleRes) {
        return setTitle(this.mMenu.getContext().getString(titleRes));
    }

    public CharSequence getTitle() {
        return this.mTitle;
    }

    public MenuItem setTitleCondensed(CharSequence title) {
        this.mTitleCondensed = title;
        return this;
    }

    public CharSequence getTitleCondensed() {
        return this.mTitleCondensed != null ? this.mTitleCondensed : this.mTitle;
    }

    public MenuItem setIcon(Drawable icon) {
        this.mIconResId = 0;
        this.mIconDrawable = icon;
        return this;
    }

    public MenuItem setIcon(int iconResId) {
        this.mIconDrawable = null;
        this.mIconResId = iconResId;
        return this;
    }

    public Drawable getIcon() {
        if (this.mIconDrawable != null) {
            return this.mIconDrawable;
        }
        if (this.mIconResId != 0) {
            return this.mMenu.getResources().getDrawable(this.mIconResId);
        }
        return null;
    }

    public MenuItem setEnabled(boolean enabled) {
        this.mEnabled = enabled;
        return this;
    }

    public boolean isEnabled() {
        return this.mEnabled;
    }

    public int getGroupId() {
        return 0;
    }

    public View getActionView() {
        return null;
    }

    public MenuItem setIntent(Intent intent) {
        return this;
    }

    public Intent getIntent() {
        return null;
    }

    public MenuItem setShortcut(char c, char c1) {
        return this;
    }

    public MenuItem setNumericShortcut(char c) {
        return this;
    }

    public char getNumericShortcut() {
        return 0;
    }

    public MenuItem setAlphabeticShortcut(char c) {
        return this;
    }

    public char getAlphabeticShortcut() {
        return 0;
    }

    public MenuItem setCheckable(boolean b) {
        return this;
    }

    public boolean isCheckable() {
        return false;
    }

    public MenuItem setChecked(boolean b) {
        return this;
    }

    public boolean isChecked() {
        return false;
    }

    public MenuItem setVisible(boolean b) {
        return this;
    }

    public boolean isVisible() {
        return true;
    }

    public boolean hasSubMenu() {
        return false;
    }

    public SubMenu getSubMenu() {
        return null;
    }

    public MenuItem setOnMenuItemClickListener(MenuItem.OnMenuItemClickListener onMenuItemClickListener) {
        return this;
    }

    public ContextMenu.ContextMenuInfo getMenuInfo() {
        return null;
    }

    public void setShowAsAction(int i) {
    }

    public MenuItem setActionView(View view) {
        return this;
    }

    public MenuItem setActionView(int i) {
        return this;
    }
}
