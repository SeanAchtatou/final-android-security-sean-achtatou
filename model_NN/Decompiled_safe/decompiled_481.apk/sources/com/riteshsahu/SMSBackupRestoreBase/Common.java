package com.riteshsahu.SMSBackupRestoreBase;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import com.riteshsahu.SMSBackupRestore.R;
import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class Common {
    public static final String AddressAttributeName = "address";
    public static final String ApplicationName = "SMSBackupRestore";
    public static final String BodyAttributeName = "body";
    public static final String[] ColumnNames = {ProtocolAttributeName, AddressAttributeName, DateAttributeName, TypeAttributeName, SubjectAttributeName, BodyAttributeName, ToaAttributeName, SCToaAttributeName, ServiceCenterAttributeName, ReadAttributeName, StatusAttributeName, LockedAttributeName};
    public static String ContactIdColumnName = null;
    public static String ContactNameColumnName = null;
    public static Uri ContactUri = null;
    public static final String CountAttributeName = "count";
    public static final String DateAttributeName = "date";
    public static final String FileNamePrefix = "sms-";
    public static final String FileNameSuffix = ".xml";
    public static final String IdColumnName = "_id";
    public static final String LockedAttributeName = "locked";
    public static final String LogElementName = "call";
    public static final String MESSAGE_TYPE_ALL = "0";
    public static final String MESSAGE_TYPE_DRAFT = "3";
    public static final String MESSAGE_TYPE_FAILED = "5";
    public static final String MESSAGE_TYPE_INBOX = "1";
    public static final String MESSAGE_TYPE_OUTBOX = "4";
    public static final String MESSAGE_TYPE_QUEUED = "6";
    public static final String MESSAGE_TYPE_SENT = "2";
    public static String[] MandatoryColumnNames = {ProtocolAttributeName, AddressAttributeName, DateAttributeName, TypeAttributeName, ReadAttributeName, StatusAttributeName};
    public static final String NullString = "null";
    public static final String ProtocolAttributeName = "protocol";
    public static final String ReadAttributeName = "read";
    public static final String RootElementName = "smses";
    public static final String SCToaAttributeName = "sc_toa";
    public static final String ServiceCenterAttributeName = "service_center";
    public static final Uri SmsContentUri = Uri.parse("content://sms");
    public static final String SmsElementName = "sms";
    public static final String StatusAttributeName = "status";
    public static final String SubjectAttributeName = "subject";
    public static final String TempMessage = "SMSBackupRestore Temp Message, Please delete.";
    public static final String ThreadIdColumnName = "thread_id";
    public static final String ToaAttributeName = "toa";
    public static final String TypeAttributeName = "type";
    public static final String UnknownContactName = "(Unknown)";
    private static BackupFile mCurrentBackupFile;
    private static boolean mLoggingEnabled;
    private static int mSdkVersion;
    private static List<BackupFile> mSelectedFiles;

    public static List<BackupFile> getSelectedFiles() {
        return mSelectedFiles;
    }

    public static void setSelectedFiles(List<BackupFile> files) {
        mSelectedFiles = files;
    }

    public static BackupFile getCurrentBackupFile() {
        return mCurrentBackupFile;
    }

    public static void setCurrentBackupFile(BackupFile backupFile) {
        mCurrentBackupFile = backupFile;
    }

    public static boolean backupExists(BackupFile backupFile) {
        return new File(backupFile.getFullPath()).exists();
    }

    public static void openHelp(Context context) {
        try {
            context.startActivity(new Intent(context, Help.class));
        } catch (Exception e) {
            Log.e("", e.getMessage());
        }
    }

    public static int getSdkVersion() {
        if (mSdkVersion <= 0) {
            try {
                mSdkVersion = Integer.parseInt(Build.VERSION.SDK);
            } catch (Exception e) {
                mSdkVersion = 3;
            }
        }
        return mSdkVersion;
    }

    public static void deleteAllMessages(Context context) throws CustomException {
        try {
            ContentResolver contentResolver = context.getContentResolver();
            contentResolver.delete(SmsContentUri, null, null);
            contentResolver.notifyChange(SmsContentUri, null);
        } catch (Exception e) {
            Exception ex = e;
            ex.printStackTrace();
            logError("App version: " + context.getString(R.string.app_version_name), ex);
            logInfo("Error occurred during deleting all messages: " + ex.getMessage());
            throw new CustomException(String.format(context.getString(R.string.error_during_delete_all), ex.getMessage()));
        }
    }

    public static OperationResult deleteFiles(List<BackupFile> backupFiles, Context context) throws CustomException {
        OperationResult result = new OperationResult();
        int failedCount = 0;
        int successfulCount = 0;
        int totalCount = 0;
        for (BackupFile backupFile : backupFiles) {
            totalCount++;
            if (new File(backupFile.getFullPath()).delete()) {
                successfulCount++;
            } else {
                failedCount++;
            }
        }
        result.setFailed(failedCount);
        result.setSuccessful(successfulCount);
        result.setTotal(totalCount);
        return result;
    }

    public static Integer deleteOldFiles(final long deleteFilesBeforeDate, Context context) throws CustomException {
        File[] files = new File(getBackupFilePath(context)).listFiles(new FileFilter() {
            public boolean accept(File currentFile) {
                return currentFile.lastModified() < deleteFilesBeforeDate && currentFile.getName().endsWith(Common.FileNameSuffix) && !currentFile.getName().startsWith("_") && currentFile.isFile();
            }
        });
        Integer fileCount = 0;
        if (files != null && files.length > 0) {
            File folder = new File(getBackupFilePath(context));
            if (folder.canWrite()) {
                for (File fileToDelete : files) {
                    fileToDelete.delete();
                    fileCount = Integer.valueOf(fileCount.intValue() + 1);
                }
            } else {
                logDebug("No Write Access to folder: " + folder);
                throw new CustomException(String.format(context.getString(R.string.no_write_access_to_folder), folder));
            }
        }
        return fileCount;
    }

    private static String[] getBackupFilesInFolder(File folder) {
        return folder.list(new FilenameFilter() {
            public boolean accept(File dir, String filename) {
                return filename.endsWith(Common.FileNameSuffix);
            }
        });
    }

    public static void showDisclaimer(final Context context, Boolean showCancelButton, final Main main) {
        AlertDialog alert = new CustomDialog(context, R.style.ClassicDialog);
        alert.setTitle((int) R.string.disclaimer_title);
        alert.setMessage(context.getText(R.string.disclaimer_text));
        alert.setIcon((int) R.drawable.icon);
        alert.setButton(-1, context.getText(R.string.button_ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                Common.setBooleanPreference(context, PreferenceKeys.DisclaimerShown, true);
                dialog.dismiss();
            }
        });
        if (showCancelButton.booleanValue()) {
            alert.setButton(-2, context.getText(R.string.button_cancel), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    dialog.dismiss();
                    if (Main.this != null) {
                        Main.this.finish();
                    }
                }
            });
        }
        alert.show();
    }

    public static List<BackupFile> getBackupFilesInDefaultFolder(Context context) {
        List<BackupFile> listToReturn = new ArrayList<>();
        String folder = getBackupFilePath(context);
        String[] fileNames = getBackupFilesInFolder(new File(folder));
        if (fileNames != null && fileNames.length > 0) {
            Arrays.sort(fileNames, Collections.reverseOrder());
            for (String backupFile : fileNames) {
                listToReturn.add(new BackupFile(folder, backupFile));
            }
        }
        return listToReturn;
    }

    public static List<BackupFile> getBackupFilesInOtherFolders() {
        List<BackupFile> listToReturn = new ArrayList<>();
        List<File> folders = getProbableFoldersWithBackups();
        if (folders.size() > 0) {
            for (int x = 0; x < folders.size(); x++) {
                String folder = folders.get(x).getAbsolutePath();
                String[] fileNames = getBackupFilesInFolder(folders.get(x));
                if (fileNames != null && fileNames.length > 0) {
                    for (String backupFile : fileNames) {
                        listToReturn.add(new BackupFile(folder, backupFile));
                    }
                }
            }
        }
        Collections.sort(listToReturn, Collections.reverseOrder());
        return listToReturn;
    }

    private static List<File> getProbableFoldersWithBackups() {
        final List<File> folderList = new ArrayList<>();
        File[] folders = new File(Environment.getExternalStorageDirectory().getAbsolutePath()).listFiles(new FileFilter() {
            public boolean accept(File pathname) {
                return pathname.isDirectory();
            }
        });
        if (folders != null && folders.length > 0) {
            FileFilter filter = new FileFilter() {
                public boolean accept(File pathname) {
                    if (!pathname.isDirectory() || !pathname.getName().equalsIgnoreCase(Common.ApplicationName)) {
                        return false;
                    }
                    folderList.add(pathname);
                    return true;
                }
            };
            for (File currentFolder : folders) {
                currentFolder.listFiles(filter);
            }
        }
        return folderList;
    }

    public static String getBackupFilePath(Context context) {
        String folder = getStringPreference(context, PreferenceKeys.BackupFolder);
        if (folder == null || folder.length() <= 0) {
            return getDefaultBackupFolder();
        }
        return folder;
    }

    public static final Boolean getBooleanPreference(Context context, String name) {
        return Boolean.valueOf(PreferenceManager.getDefaultSharedPreferences(context).getBoolean(name, false));
    }

    public static Contact getContactForNumber(Context context, String number) {
        Throwable th;
        Cursor cursor;
        logDebug("Looking up contact for Number: " + number);
        Contact contact = new Contact();
        contact.setAddress(number);
        if (number == null || number.length() <= 0) {
            contact.setId(UnknownContactName);
            contact.setName(UnknownContactName);
        } else {
            String numberToCheck = PhoneNumberUtils.stripSeparators(number);
            if (numberToCheck == null || numberToCheck.length() <= 0) {
                contact.setId(UnknownContactName + number);
                contact.setName(UnknownContactName);
            } else {
                try {
                    if (getSdkVersion() >= 5) {
                        cursor = context.getContentResolver().query(ContactUri, new String[]{ContactNameColumnName, ContactIdColumnName}, "PHONE_NUMBERS_EQUAL(data1,?) AND mimetype='vnd.android.cursor.item/phone_v2' AND raw_contact_id IN (SELECT raw_contact_id  FROM phone_lookup WHERE normalized_number GLOB('+*'))".replace("+", PhoneNumberUtils.toCallerIDMinMatch(number)), new String[]{numberToCheck}, null);
                    } else {
                        cursor = context.getContentResolver().query(Uri.withAppendedPath(ContactUri, Uri.encode(numberToCheck)), new String[]{ContactNameColumnName, ContactIdColumnName}, null, null, null);
                    }
                    try {
                        if (cursor.moveToFirst()) {
                            contact.setName(cursor.getString(0));
                            contact.setId(cursor.getString(1));
                        } else {
                            contact.setId(UnknownContactName + number);
                            contact.setName(UnknownContactName);
                        }
                        if (cursor != null) {
                            cursor.close();
                        }
                    } catch (Throwable th2) {
                        th = th2;
                    }
                } catch (Throwable th3) {
                    th = th3;
                    cursor = null;
                }
            }
        }
        contact.setCount(1);
        return contact;
        if (cursor != null) {
            cursor.close();
        }
        throw th;
    }

    public static String getDefaultBackupFolder() {
        return String.valueOf(Environment.getExternalStorageDirectory().getAbsolutePath()) + "/" + ApplicationName + "/";
    }

    public static void resetProgressHandler(ProgressDialog progressDialog, Handler handler, int messageResourceId, int maxValue) {
        if (handler != null) {
            handler.sendEmptyMessage(messageResourceId);
        }
        if (progressDialog != null) {
            progressDialog.setMax(maxValue);
            progressDialog.setProgress(0);
        }
    }

    public static final int getIntPreference(Context context, String name) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt(name, 0);
    }

    public static final boolean getLoggingEnabled() {
        return mLoggingEnabled;
    }

    public static final long getLongPreference(Context context, String name) {
        return PreferenceManager.getDefaultSharedPreferences(context).getLong(name, 0);
    }

    public static String getNewBackupFileName(Context context) {
        if (getBooleanPreference(context, PreferenceKeys.UseArchiveMode).booleanValue()) {
            return getStringPreference(context, PreferenceKeys.ArchiveFilename, R.string.archive_default_filename);
        }
        String dateFormat = getStringPreference(context, PreferenceKeys.FilenameDateFormat);
        if (dateFormat == "") {
            dateFormat = context.getString(R.string.default_date_format);
        }
        return FileNamePrefix + new SimpleDateFormat(dateFormat).format(new Date()) + FileNameSuffix;
    }

    public static final String getStringPreference(Context context, String name) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(name, "");
    }

    public static final String getStringPreference(Context context, String name, int defaultValueResourceId) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(name, context.getString(defaultValueResourceId));
    }

    public static final void logDebug(String message) {
        if (mLoggingEnabled) {
            Log.d(ApplicationName, message);
        }
    }

    public static final void logError(String message, Exception ex) {
        Log.e(ApplicationName, message, ex);
    }

    public static final void logInfo(String message) {
        Log.i(ApplicationName, message);
    }

    public static final void setBooleanPreference(Context context, String name, Boolean value) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.putBoolean(name, value.booleanValue());
        editor.commit();
    }

    public static final void setIntPreference(Context context, String name, int value) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.putInt(name, value);
        editor.commit();
    }

    public static final void setLoggingEnabled(boolean enable) {
        mLoggingEnabled = enable;
    }

    public static final void setLongPreference(Context context, String name, long value) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.putLong(name, value);
        editor.commit();
    }

    public static void setStringPreference(Context context, String name, String value) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.putString(name, value);
        editor.commit();
    }

    public static void setupContactNameSettings() {
        if (getSdkVersion() >= 5) {
            ContactIdColumnName = IdColumnName;
            ContactNameColumnName = "display_name";
            ContactUri = Uri.parse("content://com.android.contacts/data");
            return;
        }
        ContactIdColumnName = "person";
        ContactNameColumnName = "display_name";
        ContactUri = Uri.parse("content://contacts/phones/filter");
    }
}
