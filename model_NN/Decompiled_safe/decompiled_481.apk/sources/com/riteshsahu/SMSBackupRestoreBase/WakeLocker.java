package com.riteshsahu.SMSBackupRestoreBase;

import android.content.Context;
import android.os.PowerManager;

class WakeLocker {
    private static PowerManager.WakeLock mCpuWakeLock;

    WakeLocker() {
    }

    static void acquireLock(Context context) {
        Common.logDebug("Acquiring cpu wake lock");
        if (mCpuWakeLock == null || !mCpuWakeLock.isHeld()) {
            mCpuWakeLock = ((PowerManager) context.getSystemService("power")).newWakeLock(1, Common.ApplicationName);
            mCpuWakeLock.acquire();
            return;
        }
        Common.logDebug("Lock already exists, not acquing a new one!");
    }

    static void releaseLock() {
        Common.logDebug("Releasing cpu wake lock");
        if (mCpuWakeLock != null && mCpuWakeLock.isHeld()) {
            mCpuWakeLock.release();
            mCpuWakeLock = null;
        }
    }
}
