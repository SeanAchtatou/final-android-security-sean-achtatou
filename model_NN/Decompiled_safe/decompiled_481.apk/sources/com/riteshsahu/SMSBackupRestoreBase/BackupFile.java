package com.riteshsahu.SMSBackupRestoreBase;

public class BackupFile implements Comparable<BackupFile> {
    private String mFileName;
    private String mFolder;

    public BackupFile() {
    }

    public BackupFile(String folder, String fileName) {
        this.mFolder = folder;
        this.mFileName = fileName;
    }

    public String getFullPath() {
        return String.valueOf(this.mFolder) + "/" + this.mFileName;
    }

    /* access modifiers changed from: package-private */
    public void setFileName(String fileName) {
        this.mFileName = fileName;
    }

    /* access modifiers changed from: package-private */
    public String getFileName() {
        return this.mFileName;
    }

    /* access modifiers changed from: package-private */
    public void setFolder(String folder) {
        this.mFolder = folder;
    }

    /* access modifiers changed from: package-private */
    public String getFolder() {
        return this.mFolder;
    }

    public int compareTo(BackupFile other) {
        int result = this.mFolder.compareTo(other.mFolder);
        if (result == 0) {
            return this.mFileName.compareTo(other.mFileName);
        }
        return result;
    }
}
