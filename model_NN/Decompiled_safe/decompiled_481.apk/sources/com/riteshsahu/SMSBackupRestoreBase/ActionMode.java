package com.riteshsahu.SMSBackupRestoreBase;

public enum ActionMode {
    None,
    Backup,
    Delete,
    Restore,
    DeleteBackups,
    RestoreConversation
}
