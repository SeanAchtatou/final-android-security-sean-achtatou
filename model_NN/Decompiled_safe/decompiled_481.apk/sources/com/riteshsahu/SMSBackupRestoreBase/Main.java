package com.riteshsahu.SMSBackupRestoreBase;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.riteshsahu.SMSBackupRestore.R;
import java.io.File;
import java.util.List;

public class Main extends Activity {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$riteshsahu$SMSBackupRestoreBase$Main$FileOperation;
    /* access modifiers changed from: private */
    public static boolean mCheckDuplicatesDuringRestore;
    final ActivityHelper mActivityHelper = ActivityHelper.createInstance(this);

    private enum FileOperation {
        Email,
        Restore,
        View
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$riteshsahu$SMSBackupRestoreBase$Main$FileOperation() {
        int[] iArr = $SWITCH_TABLE$com$riteshsahu$SMSBackupRestoreBase$Main$FileOperation;
        if (iArr == null) {
            iArr = new int[FileOperation.values().length];
            try {
                iArr[FileOperation.Email.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[FileOperation.Restore.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[FileOperation.View.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$com$riteshsahu$SMSBackupRestoreBase$Main$FileOperation = iArr;
        }
        return iArr;
    }

    public static boolean getCheckDuplicatesDuringRestore() {
        return mCheckDuplicatesDuringRestore;
    }

    /* access modifiers changed from: private */
    public void performAction(ActionMode actionMode) {
        new TaskRunner().performAction(this, actionMode, false);
    }

    /* access modifiers changed from: private */
    public void askForBackupFileNameAndProcess() {
        if (Common.getBooleanPreference(this, PreferenceKeys.UseArchiveMode).booleanValue()) {
            Common.setCurrentBackupFile(new BackupFile(Common.getBackupFilePath(this), Common.getNewBackupFileName(this)));
            performAction(ActionMode.Backup);
            return;
        }
        AlertDialog alert = new CustomDialog(this, R.style.ClassicDialog);
        alert.setTitle((int) R.string.app_name);
        alert.setMessage(getText(R.string.enter_backup_filename));
        alert.setIcon((int) R.drawable.icon);
        final EditText input = new EditText(this);
        input.setText(Common.getNewBackupFileName(this));
        alert.setView(input);
        alert.setButton(-1, getText(R.string.button_ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                Common.setCurrentBackupFile(new BackupFile(Common.getBackupFilePath(Main.this), input.getText().toString()));
                BackupFile currentBackupFile = Common.getCurrentBackupFile();
                if (!currentBackupFile.getFileName().endsWith(Common.FileNameSuffix)) {
                    currentBackupFile.setFileName(String.valueOf(currentBackupFile.getFileName()) + Common.FileNameSuffix);
                }
                if (Common.backupExists(currentBackupFile)) {
                    new AlertDialog.Builder(Main.this).setIcon((int) R.drawable.icon).setTitle(Main.this.getText(R.string.app_name)).setMessage(String.format(Main.this.getString(R.string.backup_confirm_text), currentBackupFile.getFileName())).setPositiveButton((int) R.string.button_yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            Main.this.performAction(ActionMode.Backup);
                        }
                    }).setNegativeButton((int) R.string.button_no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                        }
                    }).create().show();
                    return;
                }
                Main.this.performAction(ActionMode.Backup);
            }
        });
        alert.setButton(-3, getText(R.string.button_cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        alert.show();
    }

    /* access modifiers changed from: private */
    public void askForDuplicatesOnRestore() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setIcon((int) R.drawable.icon);
        dialog.setTitle((int) R.string.app_name);
        dialog.setMessage(String.format(getString(R.string.restore_duplicates_confirm_text), Common.getCurrentBackupFile().getFileName()));
        dialog.setPositiveButton((int) R.string.button_yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                Main.mCheckDuplicatesDuringRestore = true;
                Main.this.performAction(ActionMode.Restore);
            }
        });
        dialog.setNegativeButton((int) R.string.button_no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                Main.mCheckDuplicatesDuringRestore = false;
                Main.this.performAction(ActionMode.Restore);
            }
        });
        dialog.create().show();
    }

    /* access modifiers changed from: private */
    public void showFileSelectorDialog(final FileOperation operation, final List<BackupFile> backupFiles) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setIcon((int) R.drawable.icon);
        switch ($SWITCH_TABLE$com$riteshsahu$SMSBackupRestoreBase$Main$FileOperation()[operation.ordinal()]) {
            case 1:
                dialog.setTitle((int) R.string.select_filename_email);
                break;
            case 2:
                dialog.setTitle((int) R.string.select_filename_restore);
                break;
            case 3:
                dialog.setTitle((int) R.string.select_filename_view);
                break;
        }
        Common.setCurrentBackupFile(backupFiles.get(0));
        dialog.setSingleChoiceItems(new BackupFileListAdapter(this, R.layout.list_item_single, backupFiles), 0, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                Common.setCurrentBackupFile((BackupFile) backupFiles.get(whichButton));
            }
        });
        dialog.setPositiveButton((int) R.string.button_ok, new DialogInterface.OnClickListener() {
            private static /* synthetic */ int[] $SWITCH_TABLE$com$riteshsahu$SMSBackupRestoreBase$Main$FileOperation;

            static /* synthetic */ int[] $SWITCH_TABLE$com$riteshsahu$SMSBackupRestoreBase$Main$FileOperation() {
                int[] iArr = $SWITCH_TABLE$com$riteshsahu$SMSBackupRestoreBase$Main$FileOperation;
                if (iArr == null) {
                    iArr = new int[FileOperation.values().length];
                    try {
                        iArr[FileOperation.Email.ordinal()] = 1;
                    } catch (NoSuchFieldError e) {
                    }
                    try {
                        iArr[FileOperation.Restore.ordinal()] = 2;
                    } catch (NoSuchFieldError e2) {
                    }
                    try {
                        iArr[FileOperation.View.ordinal()] = 3;
                    } catch (NoSuchFieldError e3) {
                    }
                    $SWITCH_TABLE$com$riteshsahu$SMSBackupRestoreBase$Main$FileOperation = iArr;
                }
                return iArr;
            }

            public void onClick(DialogInterface dialog, int whichButton) {
                switch ($SWITCH_TABLE$com$riteshsahu$SMSBackupRestoreBase$Main$FileOperation()[operation.ordinal()]) {
                    case 1:
                        Main.this.sendEmail();
                        return;
                    case 2:
                        Main.this.askForDuplicatesOnRestore();
                        return;
                    case 3:
                        Main.this.startViewer();
                        return;
                    default:
                        return;
                }
            }
        });
        dialog.setNegativeButton((int) R.string.button_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        dialog.create().show();
    }

    /* access modifiers changed from: protected */
    public void startViewer() {
        try {
            startActivity(new Intent(this, ContactView.class));
        } catch (Exception e) {
            Common.logError("Could not open ContactView", e);
        }
    }

    /* access modifiers changed from: private */
    public void askForFileNameToLoad(final FileOperation operation) {
        final List<BackupFile> fileNames = Common.getBackupFilesInDefaultFolder(this);
        if (fileNames.size() == 0) {
            new AlertDialog.Builder(this).setIcon((int) R.drawable.icon).setTitle(getText(R.string.app_name)).setMessage(String.format(getString(R.string.backup_file_notfound_search_more), Common.getBackupFilePath(this))).setPositiveButton((int) R.string.button_yes, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    dialog.dismiss();
                    fileNames.addAll(Common.getBackupFilesInOtherFolders());
                    if (fileNames.size() == 0) {
                        Main.this.showNoBackupsFoundDialog();
                    } else {
                        Main.this.showFileSelectorDialog(operation, fileNames);
                    }
                }
            }).setNegativeButton((int) R.string.button_no, (DialogInterface.OnClickListener) null).create().show();
            return;
        }
        showFileSelectorDialog(operation, fileNames);
    }

    /* access modifiers changed from: private */
    public void showNoBackupsFoundDialog() {
        new AlertDialog.Builder(this).setIcon((int) R.drawable.icon).setTitle(getText(R.string.app_name)).setMessage(getString(R.string.backup_file_notfound)).setPositiveButton((int) R.string.button_close, (DialogInterface.OnClickListener) null).create().show();
    }

    /* access modifiers changed from: protected */
    public void UpdateAlarm() {
        new AlarmProcessor().UpdateAlarm(this);
    }

    /* access modifiers changed from: protected */
    public void checkForEvenMoreMessagesToDisplay() {
        if (!Common.getBooleanPreference(this, PreferenceKeys.DisclaimerShown).booleanValue()) {
            Common.showDisclaimer(this, true, this);
        }
    }

    private void checkForMessagesToDisplay() {
        if (!Common.getStringPreference(this, PreferenceKeys.Version).equalsIgnoreCase(getString(R.string.app_version_name))) {
            UpdateAlarm();
            Common.setStringPreference(this, PreferenceKeys.Version, getString(R.string.app_version_name));
            Common.setStringPreference(this, PreferenceKeys.BackupFolder, Common.getBackupFilePath(this));
            checkForMoreMessagesToDisplay();
            return;
        }
        if (Common.getBooleanPreference(this, PreferenceKeys.ScheduledBackupStarted).booleanValue()) {
            new AlertDialog.Builder(this).setIcon((int) R.drawable.icon).setTitle(getText(R.string.app_name)).setMessage((int) R.string.scheduled_backup_did_not_complete).setPositiveButton((int) R.string.button_ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                }
            }).setNegativeButton((int) R.string.button_help, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    Common.openHelp(Main.this);
                }
            }).create().show();
            Common.setBooleanPreference(this, PreferenceKeys.ScheduledBackupStarted, false);
        }
        checkForEvenMoreMessagesToDisplay();
    }

    /* access modifiers changed from: protected */
    public void checkForMoreMessagesToDisplay() {
    }

    /* access modifiers changed from: private */
    public void deleteOldBackups() {
        try {
            startActivity(new Intent(this, DeleteFilesView.class));
        } catch (Exception e) {
            Common.logError("Could not open ContactView", e);
        }
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setupView();
    }

    public void onCreate(Bundle savedInstanceState) {
        PreferenceManager.setDefaultValues(this, R.layout.preferences, false);
        Common.setLoggingEnabled(Common.getBooleanPreference(this, PreferenceKeys.EnableLogging).booleanValue());
        setupView();
        checkForMessagesToDisplay();
        super.onCreate(savedInstanceState);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, (int) R.string.menu_help, 0, (int) R.string.menu_help).setIcon(17301568);
        menu.add(0, (int) R.string.menu_preferences, 2, (int) R.string.menu_preferences).setIcon(17301577);
        menu.add(0, (int) R.string.menu_email, 3, (int) R.string.menu_email).setIcon(17301584);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.string.menu_email:
                askForFileNameToLoad(FileOperation.Email);
                return true;
            case R.string.menu_help:
                Common.openHelp(this);
                return true;
            case R.string.menu_more:
            default:
                return false;
            case R.string.menu_preferences:
                openPreferences();
                return true;
        }
    }

    /* access modifiers changed from: protected */
    public void openPreferences() {
        try {
            startActivity(new Intent(this, Preferences.class));
        } catch (Exception e) {
            Log.e("", e.getMessage());
        }
    }

    /* access modifiers changed from: private */
    public void sendEmail() {
        Intent sendIntent = new Intent("android.intent.action.SEND");
        sendIntent.putExtra("android.intent.extra.SUBJECT", ((Object) getText(R.string.app_name)) + getString(R.string.backup));
        String backupFileName = Common.getCurrentBackupFile().getFullPath();
        if (!new File(backupFileName).canRead()) {
            new AlertDialog.Builder(this).setIcon((int) R.drawable.icon).setTitle(getText(R.string.app_name)).setMessage(getText(R.string.email_read_failed)).setPositiveButton((int) R.string.button_close, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                }
            }).create().show();
            return;
        }
        sendIntent.putExtra("android.intent.extra.STREAM", Uri.parse("file://" + backupFileName));
        sendIntent.setType("text/xml");
        startActivity(Intent.createChooser(sendIntent, getText(R.string.menu_email)));
    }

    private void setupView() {
        setContentView((int) R.layout.main);
        this.mActivityHelper.setupActionBar(getString(R.string.app_name), 0);
        ((Button) findViewById(R.id.mBackupButton)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Main.this.askForBackupFileNameAndProcess();
            }
        });
        ((Button) findViewById(R.id.mRestoreButton)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Main.this.askForFileNameToLoad(FileOperation.Restore);
            }
        });
        ((Button) findViewById(R.id.mDeleteButton)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(Main.this);
                dialog.setIcon((int) R.drawable.icon);
                dialog.setTitle((int) R.string.app_name);
                dialog.setMessage((int) R.string.delete_confirm_text);
                dialog.setPositiveButton((int) R.string.button_yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        Main.this.performAction(ActionMode.Delete);
                    }
                });
                dialog.setNegativeButton((int) R.string.button_no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                });
                dialog.create().show();
            }
        });
        ((Button) findViewById(R.id.mDeleteBackupsButton)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Main.this.deleteOldBackups();
            }
        });
        ((Button) findViewById(R.id.mViewButton)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Main.this.askForFileNameToLoad(FileOperation.View);
            }
        });
        addAdditionalControls();
    }

    /* access modifiers changed from: protected */
    public void addAdditionalControls() {
    }
}
