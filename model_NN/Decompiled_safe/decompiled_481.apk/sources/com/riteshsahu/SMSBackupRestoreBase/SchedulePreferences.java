package com.riteshsahu.SMSBackupRestoreBase;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Spinner;
import com.riteshsahu.SMSBackupRestore.R;

public class SchedulePreferences extends Activity {
    final ActivityHelper mActivityHelper = ActivityHelper.createInstance(this);

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.scheduler_preference);
        this.mActivityHelper.setupActionBar(getString(R.string.schedule_settings), 0);
        ((CheckBox) findViewById(R.id.schedule_checkbox)).setChecked(Common.getBooleanPreference(this, PreferenceKeys.UseScheduledBackups).booleanValue());
        ((Spinner) findViewById(R.id.schedule_start_time_spinner)).setSelection(getArrayIndex(getResources().getStringArray(R.array.schedule_hour), Common.getStringPreference(this, PreferenceKeys.ScheduleTimeofBackup)));
        ((Spinner) findViewById(R.id.schedule_repeat_spinner)).setSelection(getArrayIndex(getResources().getStringArray(R.array.schedule_interval), Common.getStringPreference(this, PreferenceKeys.ScheduleRepeatEvery)));
        ((Spinner) findViewById(R.id.schedule_repeat_type_spinner)).setSelection(Common.getIntPreference(this, PreferenceKeys.ScheduleRepeatType));
        ((Spinner) findViewById(R.id.schedule_backups_to_keep_spinner)).setSelection(getArrayIndex(getResources().getStringArray(R.array.backups_to_keep), Common.getStringPreference(this, PreferenceKeys.ScheduleBackupsToKeep)));
        performExtraOnCreateProcessing();
        ((Button) findViewById(R.id.schedule_save_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SchedulePreferences.this.savePreferences();
                SchedulePreferences.this.finish();
            }
        });
        ((Button) findViewById(R.id.schedule_cancel_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SchedulePreferences.this.finish();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void performExtraOnCreateProcessing() {
        CheckBox notificationCheckBox = (CheckBox) findViewById(R.id.schedule_disable_notification_checkbox);
        notificationCheckBox.setEnabled(true);
        notificationCheckBox.setChecked(Common.getBooleanPreference(this, PreferenceKeys.DisableNotifications).booleanValue());
    }

    private int getArrayIndex(String[] array, String itemToFind) {
        for (int i = 0; i < array.length; i++) {
            if (array[i].equalsIgnoreCase(itemToFind)) {
                return i;
            }
        }
        return 0;
    }

    /* access modifiers changed from: private */
    public void savePreferences() {
        Common.setBooleanPreference(this, PreferenceKeys.UseScheduledBackups, Boolean.valueOf(((CheckBox) findViewById(R.id.schedule_checkbox)).isChecked()));
        Common.setStringPreference(this, PreferenceKeys.ScheduleTimeofBackup, getResources().getStringArray(R.array.schedule_hour)[((Spinner) findViewById(R.id.schedule_start_time_spinner)).getSelectedItemPosition()]);
        Common.setStringPreference(this, PreferenceKeys.ScheduleRepeatEvery, (String) ((Spinner) findViewById(R.id.schedule_repeat_spinner)).getSelectedItem());
        Common.setIntPreference(this, PreferenceKeys.ScheduleRepeatType, ((Spinner) findViewById(R.id.schedule_repeat_type_spinner)).getSelectedItemPosition());
        Common.setStringPreference(this, PreferenceKeys.ScheduleBackupsToKeep, getResources().getStringArray(R.array.backups_to_keep)[((Spinner) findViewById(R.id.schedule_backups_to_keep_spinner)).getSelectedItemPosition()]);
        Common.setBooleanPreference(this, PreferenceKeys.DisableNotifications, Boolean.valueOf(((CheckBox) findViewById(R.id.schedule_disable_notification_checkbox)).isChecked()));
        Common.setLongPreference(this, PreferenceKeys.LastScheduleDate, 0);
        UpdateAlarm();
    }

    /* access modifiers changed from: protected */
    public void UpdateAlarm() {
        new AlarmProcessor().UpdateAlarm(this);
    }
}
