package com.riteshsahu.SMSBackupRestoreBase;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import com.riteshsahu.SMSBackupRestore.R;

public class ConversationProcessor {
    private static final String AddressColumnName = "address";
    private static final String BodyColumnName = "body";
    private static final Uri ConversationUri = Uri.parse("content://mms-sms/conversations/");
    private static final String ThreadIdColumnName = "thread_id";

    private static String getColumnValue(Cursor cursor, int columnIndex) {
        String value = cursor.getString(columnIndex);
        if (value == null) {
            return Common.NullString;
        }
        return value;
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x00ae  */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:37:0x00c7=Splitter:B:37:0x00c7, B:23:0x008b=Splitter:B:23:0x008b} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.ArrayList<com.riteshsahu.SMSBackupRestoreBase.Conversation> getConversationList(android.content.Context r13) throws com.riteshsahu.SMSBackupRestoreBase.CustomException {
        /*
            com.riteshsahu.SMSBackupRestoreBase.Common.setupContactNameSettings()
            java.util.ArrayList r8 = new java.util.ArrayList
            r8.<init>()
            r6 = 0
            android.content.ContentResolver r0 = r13.getContentResolver()     // Catch:{ CustomException -> 0x0120, Exception -> 0x011c, all -> 0x0114 }
            android.net.Uri r1 = com.riteshsahu.SMSBackupRestoreBase.ConversationProcessor.ConversationUri     // Catch:{ CustomException -> 0x0120, Exception -> 0x011c, all -> 0x0114 }
            r2 = 3
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ CustomException -> 0x0120, Exception -> 0x011c, all -> 0x0114 }
            r3 = 0
            java.lang.String r4 = "body"
            r2[r3] = r4     // Catch:{ CustomException -> 0x0120, Exception -> 0x011c, all -> 0x0114 }
            r3 = 1
            java.lang.String r4 = "thread_id"
            r2[r3] = r4     // Catch:{ CustomException -> 0x0120, Exception -> 0x011c, all -> 0x0114 }
            r3 = 2
            java.lang.String r4 = "address"
            r2[r3] = r4     // Catch:{ CustomException -> 0x0120, Exception -> 0x011c, all -> 0x0114 }
            r3 = 0
            r4 = 0
            java.lang.String r5 = "date ASC"
            android.database.Cursor r9 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ CustomException -> 0x0120, Exception -> 0x011c, all -> 0x0114 }
            r7 = 0
            r11 = 1
            r6 = 2
            if (r9 == 0) goto L_0x00b2
            java.lang.String r0 = com.riteshsahu.SMSBackupRestoreBase.PreferenceKeys.SelectedConversations     // Catch:{ CustomException -> 0x0088, Exception -> 0x00c4, all -> 0x0118 }
            java.lang.String r0 = com.riteshsahu.SMSBackupRestoreBase.Common.getStringPreference(r13, r0)     // Catch:{ CustomException -> 0x0088, Exception -> 0x00c4, all -> 0x0118 }
            java.util.ArrayList r10 = new java.util.ArrayList     // Catch:{ CustomException -> 0x0088, Exception -> 0x00c4, all -> 0x0118 }
            r10.<init>()     // Catch:{ CustomException -> 0x0088, Exception -> 0x00c4, all -> 0x0118 }
            if (r0 == 0) goto L_0x004b
            int r1 = r0.length()     // Catch:{ CustomException -> 0x0088, Exception -> 0x00c4, all -> 0x0118 }
            if (r1 <= 0) goto L_0x004b
            java.lang.String r1 = ","
            java.lang.String[] r1 = r0.split(r1)     // Catch:{ CustomException -> 0x0088, Exception -> 0x00c4, all -> 0x0118 }
            r0 = 0
        L_0x0048:
            int r2 = r1.length     // Catch:{ CustomException -> 0x0088, Exception -> 0x00c4, all -> 0x0118 }
            if (r0 < r2) goto L_0x0057
        L_0x004b:
            boolean r0 = r9.moveToNext()     // Catch:{ CustomException -> 0x0088, Exception -> 0x00c4, all -> 0x0118 }
            if (r0 != 0) goto L_0x0067
            if (r9 == 0) goto L_0x0056
            r9.close()
        L_0x0056:
            return r8
        L_0x0057:
            r2 = r1[r0]     // Catch:{ CustomException -> 0x0088, Exception -> 0x00c4, all -> 0x0118 }
            long r2 = java.lang.Long.parseLong(r2)     // Catch:{ CustomException -> 0x0088, Exception -> 0x00c4, all -> 0x0118 }
            java.lang.Long r2 = java.lang.Long.valueOf(r2)     // Catch:{ CustomException -> 0x0088, Exception -> 0x00c4, all -> 0x0118 }
            r10.add(r2)     // Catch:{ CustomException -> 0x0088, Exception -> 0x00c4, all -> 0x0118 }
            int r0 = r0 + 1
            goto L_0x0048
        L_0x0067:
            long r3 = r9.getLong(r11)     // Catch:{ CustomException -> 0x0088, Exception -> 0x00c4, all -> 0x0118 }
            com.riteshsahu.SMSBackupRestoreBase.Conversation r0 = new com.riteshsahu.SMSBackupRestoreBase.Conversation     // Catch:{ CustomException -> 0x0088, Exception -> 0x00c4, all -> 0x0118 }
            java.lang.String r1 = getColumnValue(r9, r6)     // Catch:{ CustomException -> 0x0088, Exception -> 0x00c4, all -> 0x0118 }
            com.riteshsahu.SMSBackupRestoreBase.Contact r1 = com.riteshsahu.SMSBackupRestoreBase.Common.getContactForNumber(r13, r1)     // Catch:{ CustomException -> 0x0088, Exception -> 0x00c4, all -> 0x0118 }
            java.lang.String r2 = getColumnValue(r9, r7)     // Catch:{ CustomException -> 0x0088, Exception -> 0x00c4, all -> 0x0118 }
            java.lang.Long r5 = java.lang.Long.valueOf(r3)     // Catch:{ CustomException -> 0x0088, Exception -> 0x00c4, all -> 0x0118 }
            boolean r5 = r10.contains(r5)     // Catch:{ CustomException -> 0x0088, Exception -> 0x00c4, all -> 0x0118 }
            r0.<init>(r1, r2, r3, r5)     // Catch:{ CustomException -> 0x0088, Exception -> 0x00c4, all -> 0x0118 }
            r8.add(r0)     // Catch:{ CustomException -> 0x0088, Exception -> 0x00c4, all -> 0x0118 }
            goto L_0x004b
        L_0x0088:
            r0 = move-exception
            r1 = r0
            r0 = r9
        L_0x008b:
            r1.printStackTrace()     // Catch:{ all -> 0x00a8 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x00a8 }
            java.lang.String r3 = "App version: "
            r2.<init>(r3)     // Catch:{ all -> 0x00a8 }
            r3 = 2131230721(0x7f080001, float:1.8077503E38)
            java.lang.String r13 = r13.getString(r3)     // Catch:{ all -> 0x00a8 }
            java.lang.StringBuilder r13 = r2.append(r13)     // Catch:{ all -> 0x00a8 }
            java.lang.String r13 = r13.toString()     // Catch:{ all -> 0x00a8 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r13)     // Catch:{ all -> 0x00a8 }
            throw r1     // Catch:{ all -> 0x00a8 }
        L_0x00a8:
            r13 = move-exception
            r12 = r13
            r13 = r0
            r0 = r12
        L_0x00ac:
            if (r13 == 0) goto L_0x00b1
            r13.close()
        L_0x00b1:
            throw r0
        L_0x00b2:
            java.lang.String r0 = "Could not find any records."
            com.riteshsahu.SMSBackupRestoreBase.Common.logDebug(r0)     // Catch:{ CustomException -> 0x0088, Exception -> 0x00c4, all -> 0x0118 }
            com.riteshsahu.SMSBackupRestoreBase.CustomException r0 = new com.riteshsahu.SMSBackupRestoreBase.CustomException     // Catch:{ CustomException -> 0x0088, Exception -> 0x00c4, all -> 0x0118 }
            r1 = 2131230735(0x7f08000f, float:1.8077531E38)
            java.lang.String r1 = r13.getString(r1)     // Catch:{ CustomException -> 0x0088, Exception -> 0x00c4, all -> 0x0118 }
            r0.<init>(r1)     // Catch:{ CustomException -> 0x0088, Exception -> 0x00c4, all -> 0x0118 }
            throw r0     // Catch:{ CustomException -> 0x0088, Exception -> 0x00c4, all -> 0x0118 }
        L_0x00c4:
            r0 = move-exception
            r1 = r0
            r0 = r9
        L_0x00c7:
            r1.printStackTrace()     // Catch:{ all -> 0x00a8 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x00a8 }
            java.lang.String r3 = "App version: "
            r2.<init>(r3)     // Catch:{ all -> 0x00a8 }
            r3 = 2131230721(0x7f080001, float:1.8077503E38)
            java.lang.String r3 = r13.getString(r3)     // Catch:{ all -> 0x00a8 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x00a8 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x00a8 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r2)     // Catch:{ all -> 0x00a8 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x00a8 }
            java.lang.String r3 = "Error occurred during backup: "
            r2.<init>(r3)     // Catch:{ all -> 0x00a8 }
            java.lang.String r3 = r1.getMessage()     // Catch:{ all -> 0x00a8 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x00a8 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x00a8 }
            com.riteshsahu.SMSBackupRestoreBase.Common.logInfo(r2)     // Catch:{ all -> 0x00a8 }
            com.riteshsahu.SMSBackupRestoreBase.CustomException r2 = new com.riteshsahu.SMSBackupRestoreBase.CustomException     // Catch:{ all -> 0x00a8 }
            r3 = 2131230725(0x7f080005, float:1.807751E38)
            java.lang.String r13 = r13.getString(r3)     // Catch:{ all -> 0x00a8 }
            r3 = 1
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ all -> 0x00a8 }
            r4 = 0
            java.lang.String r1 = r1.getMessage()     // Catch:{ all -> 0x00a8 }
            r3[r4] = r1     // Catch:{ all -> 0x00a8 }
            java.lang.String r13 = java.lang.String.format(r13, r3)     // Catch:{ all -> 0x00a8 }
            r2.<init>(r13)     // Catch:{ all -> 0x00a8 }
            throw r2     // Catch:{ all -> 0x00a8 }
        L_0x0114:
            r13 = move-exception
            r0 = r13
            r13 = r6
            goto L_0x00ac
        L_0x0118:
            r13 = move-exception
            r0 = r13
            r13 = r9
            goto L_0x00ac
        L_0x011c:
            r0 = move-exception
            r1 = r0
            r0 = r6
            goto L_0x00c7
        L_0x0120:
            r0 = move-exception
            r1 = r0
            r0 = r6
            goto L_0x008b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.riteshsahu.SMSBackupRestoreBase.ConversationProcessor.getConversationList(android.content.Context):java.util.ArrayList");
    }

    public static String getConversationFilter(Context context) throws CustomException {
        if (!Common.getBooleanPreference(context, PreferenceKeys.BackupSelectedConversationsOnly).booleanValue()) {
            return null;
        }
        String selectedConversationsPreference = Common.getStringPreference(context, PreferenceKeys.SelectedConversations);
        if (selectedConversationsPreference != null && selectedConversationsPreference.length() > 0) {
            return "thread_id IN (" + selectedConversationsPreference + ")";
        }
        throw new CustomException(context.getString(R.string.no_conversation_selected_error));
    }
}
