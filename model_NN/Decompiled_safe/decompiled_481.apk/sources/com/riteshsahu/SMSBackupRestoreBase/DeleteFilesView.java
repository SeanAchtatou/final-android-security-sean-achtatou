package com.riteshsahu.SMSBackupRestoreBase;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import com.riteshsahu.SMSBackupRestore.R;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DeleteFilesView extends ListActivity {
    /* access modifiers changed from: private */
    public static String mErrorMessage;
    final ActivityHelper mActivityHelper = ActivityHelper.createInstance(this);
    private BackupFileListAdapter mAdapter;
    /* access modifiers changed from: private */
    public List<BackupFile> mBackupFiles = null;
    /* access modifiers changed from: private */
    public ProgressDialog mProgressDialog = null;
    private Runnable returnRes = new Runnable() {
        public void run() {
            if (DeleteFilesView.mErrorMessage == null || DeleteFilesView.mErrorMessage.length() == 0) {
                DeleteFilesView.this.updateAdapter();
                DeleteFilesView.this.mProgressDialog.dismiss();
                return;
            }
            new AlertDialog.Builder(DeleteFilesView.this).setIcon((int) R.drawable.icon).setTitle(DeleteFilesView.this.getText(R.string.app_name)).setMessage(String.format(DeleteFilesView.this.getString(R.string.error_message), DeleteFilesView.mErrorMessage)).setPositiveButton((int) R.string.button_close, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    DeleteFilesView.this.finish();
                }
            }).create().show();
            DeleteFilesView.this.mProgressDialog.dismiss();
        }
    };
    private Runnable viewFiles;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mErrorMessage = "";
        setContentView((int) R.layout.file_list);
        this.mBackupFiles = new ArrayList();
        this.mAdapter = new BackupFileListAdapter(this, R.layout.list_item_multiple, this.mBackupFiles);
        setListAdapter(this.mAdapter);
        this.mActivityHelper.setupActionBar(getText(R.string.select_filename_delete), 0);
        this.viewFiles = new Runnable() {
            public void run() {
                DeleteFilesView.this.getFilesList();
            }
        };
        new Thread(null, this.viewFiles, "MessageViewBackground").start();
        this.mProgressDialog = ProgressDialog.show(this, getString(R.string.please_wait), getString(R.string.analysing_file), true);
        final ListView listView = getListView();
        listView.setChoiceMode(2);
        ((Button) findViewById(R.id.delete_files_ok)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SparseBooleanArray checkedItems = listView.getCheckedItemPositions();
                if (checkedItems.size() > 0) {
                    List<BackupFile> selectedFiles = new ArrayList<>();
                    for (int i = 0; i < checkedItems.size(); i++) {
                        if (checkedItems.valueAt(i)) {
                            selectedFiles.add((BackupFile) DeleteFilesView.this.mBackupFiles.get(checkedItems.keyAt(i)));
                        }
                    }
                    if (selectedFiles.size() > 0) {
                        Common.setSelectedFiles(selectedFiles);
                        new TaskRunner().performAction(DeleteFilesView.this, ActionMode.DeleteBackups, true);
                        return;
                    }
                    return;
                }
                new AlertDialog.Builder(DeleteFilesView.this).setIcon((int) R.drawable.icon).setTitle(DeleteFilesView.this.getText(R.string.app_name)).setMessage((int) R.string.select_file_to_be_deleted).setPositiveButton((int) R.string.button_close, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                }).create().show();
            }
        });
        ((Button) findViewById(R.id.delete_files_cancel)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                DeleteFilesView.this.finish();
            }
        });
    }

    /* access modifiers changed from: private */
    public void updateAdapter() {
        if (this.mBackupFiles != null && this.mBackupFiles.size() > 0) {
            Collections.sort(this.mBackupFiles);
            this.mAdapter.clear();
            int size = this.mBackupFiles.size();
            for (int i = 0; i < size; i++) {
                this.mAdapter.add(this.mBackupFiles.get(i));
            }
        }
        this.mAdapter.notifyDataSetChanged();
    }

    /* access modifiers changed from: private */
    public void getFilesList() {
        try {
            this.mBackupFiles = Common.getBackupFilesInDefaultFolder(this);
            Common.logDebug("Backup Files loaded: " + this.mBackupFiles.size());
        } catch (Exception e) {
            mErrorMessage = e.getMessage();
        }
        runOnUiThread(this.returnRes);
    }
}
