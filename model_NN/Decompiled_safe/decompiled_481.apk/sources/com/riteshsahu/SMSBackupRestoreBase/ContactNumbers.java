package com.riteshsahu.SMSBackupRestoreBase;

import java.util.ArrayList;
import java.util.List;

public class ContactNumbers implements Comparable<ContactNumbers> {
    private static final String UnknownContactName = "ZZZZZZZZZ";
    private String mBody;
    private int mCount;
    private long mDate;
    private String mName;
    private List<String> mNumbers;

    public ContactNumbers() {
    }

    public ContactNumbers(String name, String number, String body, int count, long date) {
        this.mName = name;
        this.mNumbers = new ArrayList();
        this.mNumbers.add(number);
        this.mCount = count;
        this.mDate = date;
        this.mBody = body;
    }

    public void addNumber(String number, String body, int count, long date) {
        if (this.mNumbers == null) {
            this.mNumbers = new ArrayList();
        }
        this.mNumbers.add(number);
        this.mCount += count;
        if (this.mDate < date) {
            this.mDate = date;
            this.mBody = body;
        }
    }

    public Long getDate() {
        return Long.valueOf(this.mDate);
    }

    private String getComparisionName() {
        if (this.mName.equalsIgnoreCase(Common.UnknownContactName)) {
            return UnknownContactName;
        }
        return this.mName;
    }

    public int compareTo(ContactNumbers another) {
        int result = another.getDate().compareTo(getDate());
        if (result != 0) {
            return result;
        }
        int result2 = getComparisionName().compareTo(another.getComparisionName());
        if (result2 == 0) {
            return getNumber().compareTo(another.getNumber());
        }
        return result2;
    }

    public int getCount() {
        return this.mCount;
    }

    public String getName() {
        if (this.mName == null) {
            return Common.UnknownContactName;
        }
        return this.mName;
    }

    public String getNameAndNumber() {
        return String.valueOf(getName()) + " <" + getNumber() + ">";
    }

    public String getNameOrNumber() {
        if (this.mName == null || this.mName.equalsIgnoreCase(Common.UnknownContactName)) {
            return getNumber();
        }
        return this.mName;
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public String getNumber() {
        if (this.mNumbers == null || this.mNumbers.size() <= 0) {
            return "";
        }
        return this.mNumbers.get(0);
    }

    public List<String> getNumbers() {
        return this.mNumbers;
    }

    public void setCount(int count) {
        this.mCount = count;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public void setNumbers(List<String> numbers) {
        this.mNumbers = numbers;
    }

    public void setBody(String body) {
        this.mBody = body;
    }

    public String getBody() {
        return this.mBody;
    }
}
