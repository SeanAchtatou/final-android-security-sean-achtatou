package com.riteshsahu.SMSBackupRestore;

import com.riteshsahu.SMSBackupRestoreBase.AlarmProcessorService;
import com.riteshsahu.SMSBackupRestoreBase.Common;

public class FreeAlarmProcessorService extends AlarmProcessorService {
    private static final int mReminderCount = 5;

    /* access modifiers changed from: protected */
    public String performExtraProcessingAfterBackup() {
        int times;
        String messageToReturn = "";
        if (!Common.getBooleanPreference(this, PreferenceKeys.DisableDonate).booleanValue()) {
            int times2 = Common.getIntPreference(this, PreferenceKeys.BackupDonateCount);
            if (times2 >= 5) {
                times = 0;
                messageToReturn = " " + getString(R.string.consider_donation);
            } else {
                times = times2 + 1;
            }
            Common.setIntPreference(this, PreferenceKeys.BackupDonateCount, times);
        }
        return messageToReturn;
    }
}
