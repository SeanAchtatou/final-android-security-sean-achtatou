package com.riteshsahu.SMSBackupRestore;

import android.content.Intent;
import android.preference.CheckBoxPreference;
import android.preference.PreferenceCategory;
import com.riteshsahu.SMSBackupRestoreBase.PreferenceKeys;
import com.riteshsahu.SMSBackupRestoreBase.Preferences;

public class FreePreferences extends Preferences {
    /* access modifiers changed from: protected */
    public void addAdditionalPreferences() {
        PreferenceCategory otherCategory = (PreferenceCategory) findPreference(PreferenceKeys.OtherCategory);
        CheckBoxPreference disableAdsPreference = new CheckBoxPreference(this);
        disableAdsPreference.setKey(PreferenceKeys.DisableAds);
        disableAdsPreference.setTitle((int) R.string.pref_disable_ads);
        disableAdsPreference.setSummary((int) R.string.disable_ads_summary);
        otherCategory.addPreference(disableAdsPreference);
        CheckBoxPreference donatedPreference = new CheckBoxPreference(this);
        donatedPreference.setKey(PreferenceKeys.DisableDonate);
        donatedPreference.setTitle((int) R.string.have_donated);
        donatedPreference.setSummary((int) R.string.disable_donate_summary);
        otherCategory.addPreference(donatedPreference);
        findPreference(PreferenceKeys.SchedulePreferencesIntent).setIntent(new Intent(this, FreeSchedulePreferences.class));
        findPreference(PreferenceKeys.SelectConversationsIntent).setIntent(new Intent(this, FreeConversationView.class));
    }
}
