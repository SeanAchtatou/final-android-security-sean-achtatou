package com.riteshsahu.SMSBackupRestore;

import android.app.Activity;
import android.content.Context;
import android.view.ViewGroup;
import android.webkit.WebViewDatabase;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import com.riteshsahu.SMSBackupRestoreBase.Common;

public class AdsHelper {
    public static void CreateAd(Context context, Activity activity, ViewGroup containerView) {
        if (!Common.getBooleanPreference(context, PreferenceKeys.DisableAds).booleanValue()) {
            try {
                if (WebViewDatabase.getInstance(context) != null) {
                    AdView adView = new AdView(activity, AdSize.BANNER, "a14af937db8cbde");
                    adView.setGravity(81);
                    adView.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
                    containerView.addView(adView);
                    adView.loadAd(new AdRequest());
                }
            } catch (Exception e) {
                Common.logError("Could not display ads!", e);
            }
        }
    }
}
