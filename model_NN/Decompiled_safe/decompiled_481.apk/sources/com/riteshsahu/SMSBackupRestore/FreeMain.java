package com.riteshsahu.SMSBackupRestore;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import com.riteshsahu.SMSBackupRestoreBase.Common;
import com.riteshsahu.SMSBackupRestoreBase.Main;

public class FreeMain extends Main {
    /* access modifiers changed from: protected */
    public void UpdateAlarm() {
        new FreeAlarmProcessor().UpdateAlarm(this);
    }

    /* access modifiers changed from: private */
    public void askForDonate() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setIcon((int) R.drawable.icon);
        dialog.setTitle((int) R.string.app_name);
        dialog.setMessage((int) R.string.donation_dialog_text);
        dialog.setPositiveButton((int) R.string.button_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                FreeMain.this.donate();
            }
        });
        dialog.setNegativeButton((int) R.string.button_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        dialog.create().show();
    }

    /* access modifiers changed from: protected */
    public void checkForMoreMessagesToDisplay() {
        if (!Common.getBooleanPreference(this, PreferenceKeys.AskedForDonation).booleanValue()) {
            new AlertDialog.Builder(this).setIcon((int) R.drawable.icon).setTitle((int) R.string.welcome).setMessage((int) R.string.welcome_text).setPositiveButton((int) R.string.button_ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    dialog.dismiss();
                    FreeMain.this.checkForEvenMoreMessagesToDisplay();
                }
            }).create().show();
            Common.setBooleanPreference(this, PreferenceKeys.AskedForDonation, true);
            return;
        }
        checkForEvenMoreMessagesToDisplay();
    }

    /* access modifiers changed from: private */
    public void donate() {
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse(getString(R.string.donation_url))));
    }

    /* access modifiers changed from: protected */
    public void openPreferences() {
        try {
            startActivity(new Intent(this, FreePreferences.class));
        } catch (Exception e) {
            Log.e("", e.getMessage());
        }
    }

    /* access modifiers changed from: protected */
    public void startViewer() {
        try {
            startActivity(new Intent(this, FreeContactView.class));
        } catch (Exception e) {
            Common.logError("Could not open ContactView", e);
        }
    }

    /* access modifiers changed from: protected */
    public void addAdditionalControls() {
        LinearLayout mainLayout = (LinearLayout) findViewById(R.id.mMainLayout);
        Button donateButton = new Button(this);
        donateButton.setText((int) R.string.button_donate);
        donateButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                FreeMain.this.askForDonate();
            }
        });
        Resources resources = getResources();
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams((int) TypedValue.applyDimension(0, (float) resources.getDimensionPixelSize(R.dimen.push_button_width), resources.getDisplayMetrics()), (int) TypedValue.applyDimension(0, (float) resources.getDimensionPixelSize(R.dimen.push_button_height), resources.getDisplayMetrics()));
        lp.gravity = 81;
        lp.bottomMargin = 10;
        mainLayout.addView(donateButton, lp);
        AdsHelper.CreateAd(this, this, mainLayout);
    }
}
