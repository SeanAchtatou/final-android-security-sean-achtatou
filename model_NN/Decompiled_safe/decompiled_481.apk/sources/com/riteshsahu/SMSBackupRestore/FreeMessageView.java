package com.riteshsahu.SMSBackupRestore;

import android.widget.LinearLayout;
import com.riteshsahu.SMSBackupRestoreBase.MessageView;

public class FreeMessageView extends MessageView {
    /* access modifiers changed from: protected */
    public void addAdditionalControls() {
        AdsHelper.CreateAd(this, this, (LinearLayout) findViewById(R.id.message_list_layout));
    }
}
