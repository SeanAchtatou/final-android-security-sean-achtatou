package com.riteshsahu.SMSBackupRestore;

import android.widget.CheckBox;
import com.riteshsahu.SMSBackupRestoreBase.Common;
import com.riteshsahu.SMSBackupRestoreBase.PreferenceKeys;
import com.riteshsahu.SMSBackupRestoreBase.SchedulePreferences;

public class FreeSchedulePreferences extends SchedulePreferences {
    /* access modifiers changed from: protected */
    public void performExtraOnCreateProcessing() {
        CheckBox notificationCheckBox = (CheckBox) findViewById(R.id.schedule_disable_notification_checkbox);
        if (!Common.getBooleanPreference(this, PreferenceKeys.DisableDonate).booleanValue()) {
            notificationCheckBox.setChecked(false);
            notificationCheckBox.setEnabled(false);
            return;
        }
        notificationCheckBox.setEnabled(true);
        notificationCheckBox.setChecked(Common.getBooleanPreference(this, PreferenceKeys.DisableNotifications).booleanValue());
    }

    /* access modifiers changed from: protected */
    public void UpdateAlarm() {
        new FreeAlarmProcessor().UpdateAlarm(this);
    }
}
