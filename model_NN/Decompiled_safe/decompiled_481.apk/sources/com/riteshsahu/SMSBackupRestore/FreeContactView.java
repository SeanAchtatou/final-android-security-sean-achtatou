package com.riteshsahu.SMSBackupRestore;

import android.content.Intent;
import android.util.Log;
import android.widget.LinearLayout;
import com.riteshsahu.SMSBackupRestoreBase.ContactView;

public class FreeContactView extends ContactView {
    /* access modifiers changed from: protected */
    public void addAdditionalControls() {
        AdsHelper.CreateAd(this, this, (LinearLayout) findViewById(R.id.contact_list_layout));
    }

    /* access modifiers changed from: protected */
    public void openMessageView() {
        try {
            startActivity(new Intent(this, FreeMessageView.class));
        } catch (Exception e) {
            Log.e("", e.getMessage());
        }
    }
}
