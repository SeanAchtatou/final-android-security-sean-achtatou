package com.riteshsahu.SMSBackupRestore;

import android.content.Context;
import android.content.Intent;
import com.riteshsahu.SMSBackupRestoreBase.AlarmProcessor;

public class FreeAlarmProcessor extends AlarmProcessor {
    /* access modifiers changed from: protected */
    public void startAlarmProcessorService(Context context) {
        context.startService(new Intent(context, FreeAlarmProcessorService.class));
    }

    /* access modifiers changed from: protected */
    public Intent createAlarmProcessorIntent(Context context) {
        return new Intent(context, FreeAlarmProcessor.class);
    }
}
