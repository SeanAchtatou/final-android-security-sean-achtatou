package com.riteshsahu.SMSBackupRestore;

public final class R {

    public static final class array {
        public static final int backups_to_keep = 2130968578;
        public static final int backups_to_keep_display = 2130968579;
        public static final int date_formats = 2130968580;
        public static final int readable_date_formats = 2130968581;
        public static final int schedule_hour = 2130968576;
        public static final int schedule_interval = 2130968583;
        public static final int schedule_time = 2130968577;
        public static final int schedule_type = 2130968582;
    }

    public static final class attr {
        public static final int actionbarCompatButtonStyle = 2130771968;
        public static final int actionbarCompatLogoStyle = 2130771971;
        public static final int actionbarCompatProgressIndicatorStyle = 2130771969;
        public static final int actionbarCompatSeparatorStyle = 2130771970;
        public static final int actionbarCompatTextStyle = 2130771972;
        public static final int listItemContainer = 2130771973;
        public static final int listItemSubtitle = 2130771975;
        public static final int listItemTitle = 2130771974;
        public static final int spinnerStyle = 2130771976;
    }

    public static final class color {
        public static final int accent_1 = 2131034132;
        public static final int actionbar_text = 2131034112;
        public static final int all_track_color = 2131034136;
        public static final int background_1 = 2131034134;
        public static final int block_column_1 = 2131034137;
        public static final int block_column_2 = 2131034138;
        public static final int block_column_3 = 2131034139;
        public static final int body_text_1 = 2131034122;
        public static final int body_text_1_inverse = 2131034130;
        public static final int body_text_2 = 2131034123;
        public static final int body_text_2_inverse = 2131034131;
        public static final int body_text_disabled = 2131034124;
        public static final int button_background = 2131034113;
        public static final int button_color = 2131034114;
        public static final int cache_color_hint = 2131034117;
        public static final int dialog_background = 2131034118;
        public static final int dialog_color = 2131034119;
        public static final int hyperlink = 2131034133;
        public static final int list_separator_background = 2131034128;
        public static final int list_separator_color = 2131034129;
        public static final int listview_text_body = 2131034127;
        public static final int listview_text_header = 2131034126;
        public static final int preference_category_background = 2131034120;
        public static final int preference_category_color = 2131034121;
        public static final int spinner_item_color = 2131034115;
        public static final int warning = 2131034125;
        public static final int whats_on_separator = 2131034135;
        public static final int window_background = 2131034116;
    }

    public static final class dimen {
        public static final int actionbar_compat_height = 2131099651;
        public static final int body_padding_large = 2131099671;
        public static final int body_padding_medium = 2131099670;
        public static final int colorstrip_height = 2131099650;
        public static final int delete_backups_button_font_size = 2131099668;
        public static final int delete_selected_button_font_size = 2131099669;
        public static final int list_item_padding = 2131099649;
        public static final int list_margin = 2131099648;
        public static final int push_button_height = 2131099663;
        public static final int push_button_margin = 2131099666;
        public static final int push_button_padding = 2131099665;
        public static final int push_button_text_size = 2131099667;
        public static final int push_button_width = 2131099664;
        public static final int schedule_header_padding = 2131099655;
        public static final int schedule_hour_height = 2131099656;
        public static final int schedule_label_header_width = 2131099657;
        public static final int schedule_label_padding = 2131099658;
        public static final int speaker_image_padding = 2131099678;
        public static final int speaker_image_size = 2131099677;
        public static final int tab_height = 2131099659;
        public static final int tab_side_padding = 2131099660;
        public static final int text_size_large = 2131099675;
        public static final int text_size_medium = 2131099674;
        public static final int text_size_small = 2131099673;
        public static final int text_size_xlarge = 2131099676;
        public static final int text_size_xsmall = 2131099672;
        public static final int track_icon_padding = 2131099662;
        public static final int track_icon_width = 2131099661;
        public static final int vendor_image_size = 2131099679;
        public static final int whats_on_button_right_padding = 2131099654;
        public static final int whats_on_height = 2131099652;
        public static final int whats_on_item_padding = 2131099653;
    }

    public static final class drawable {
        public static final int actionbar_compat_background = 2130837504;
        public static final int actionbar_compat_button = 2130837505;
        public static final int actionbar_compat_separator = 2130837506;
        public static final int btn_bg_pressed = 2130837507;
        public static final int btn_bg_selected = 2130837508;
        public static final int icon = 2130837509;
        public static final int icon_check_off = 2130837510;
        public static final int icon_check_on = 2130837511;
        public static final int notification = 2130837512;
        public static final int push_button = 2130837513;
        public static final int spinner_background = 2130837514;
    }

    public static final class id {
        public static final int MessageListLayout = 2131165216;
        public static final int actionbar_compat = 2131165184;
        public static final int actionbar_compat_container = 2131165185;
        public static final int actionbar_compat_logo = 2131165186;
        public static final int actionbar_compat_text = 2131165187;
        public static final int blocks_now = 2131165195;
        public static final int blocks_ruler = 2131165194;
        public static final int contact_list_layout = 2131165196;
        public static final int contact_message = 2131165199;
        public static final int contact_message_count = 2131165198;
        public static final int contact_name = 2131165197;
        public static final int conversation_body = 2131165203;
        public static final int conversation_list_layout = 2131165200;
        public static final int conversation_name = 2131165201;
        public static final int conversation_number = 2131165202;
        public static final int conversation_selected = 2131165204;
        public static final int delete_files_cancel = 2131165206;
        public static final int delete_files_ok = 2131165205;
        public static final int dialog_discard_confirm = 2131165193;
        public static final int dialog_list_item_title = 2131165209;
        public static final int fragment_session_detail = 2131165189;
        public static final int fragment_sessions = 2131165188;
        public static final int fragment_vendor_detail = 2131165191;
        public static final int fragment_vendors = 2131165190;
        public static final int help_layout = 2131165207;
        public static final int help_webview = 2131165208;
        public static final int mBackupButton = 2131165211;
        public static final int mDeleteBackupsButton = 2131165214;
        public static final int mDeleteButton = 2131165215;
        public static final int mMainLayout = 2131165210;
        public static final int mRestoreButton = 2131165212;
        public static final int mViewButton = 2131165213;
        public static final int menu_refresh_progress = 2131165192;
        public static final int message_body = 2131165220;
        public static final int message_date = 2131165219;
        public static final int message_list_layout = 2131165217;
        public static final int message_sender = 2131165218;
        public static final int preferences_layout = 2131165221;
        public static final int schedule_backups_to_keep_spinner = 2131165226;
        public static final int schedule_cancel_button = 2131165229;
        public static final int schedule_checkbox = 2131165222;
        public static final int schedule_disable_notification_checkbox = 2131165227;
        public static final int schedule_repeat_spinner = 2131165224;
        public static final int schedule_repeat_type_spinner = 2131165225;
        public static final int schedule_save_button = 2131165228;
        public static final int schedule_start_time_spinner = 2131165223;
        public static final int search_button = 2131165230;
    }

    public static final class layout {
        public static final int actionbar = 2130903040;
        public static final int contact_list = 2130903041;
        public static final int contact_row = 2130903042;
        public static final int conversation_list = 2130903043;
        public static final int conversation_row = 2130903044;
        public static final int file_list = 2130903045;
        public static final int help = 2130903046;
        public static final int list_item_multiple = 2130903047;
        public static final int list_item_single = 2130903048;
        public static final int main = 2130903049;
        public static final int message_list = 2130903050;
        public static final int message_row = 2130903051;
        public static final int preferences = 2130903052;
        public static final int preferences_wrapper = 2130903053;
        public static final int scheduler_preference = 2130903054;
        public static final int search = 2130903055;
    }

    public static final class string {
        public static final int add_contact_name_description = 2131230803;
        public static final int add_readable_date_description = 2131230799;
        public static final int add_xsl_description = 2131230801;
        public static final int adjust_timezone_restore_description = 2131230793;
        public static final int adjust_timezone_view_description = 2131230795;
        public static final int analysing_file = 2131230732;
        public static final int app_name = 2131230720;
        public static final int app_version_name = 2131230721;
        public static final int archive_default_filename = 2131230848;
        public static final int archive_filename_description = 2131230847;
        public static final int archive_mode_description = 2131230841;
        public static final int backing = 2131230812;
        public static final int backup = 2131230740;
        public static final int backup_complete_notification_message = 2131230723;
        public static final int backup_confirm_text = 2131230722;
        public static final int backup_failed_error = 2131230724;
        public static final int backup_file_notfound = 2131230761;
        public static final int backup_file_notfound_search_more = 2131230762;
        public static final int backup_folder_description = 2131230853;
        public static final int backup_folder_dialog_message = 2131230854;
        public static final int backup_selected_conversations_description = 2131230862;
        public static final int backup_settings = 2131230844;
        public static final int browser_app_not_found = 2131230895;
        public static final int button_backup = 2131230745;
        public static final int button_browse = 2131230787;
        public static final int button_cancel = 2131230781;
        public static final int button_close = 2131230780;
        public static final int button_default = 2131230785;
        public static final int button_delete_backups = 2131230749;
        public static final int button_delete_messages = 2131230750;
        public static final int button_delete_selected = 2131230786;
        public static final int button_donate = 2131230913;
        public static final int button_help = 2131230850;
        public static final int button_no = 2131230784;
        public static final int button_ok = 2131230782;
        public static final int button_restore = 2131230746;
        public static final int button_save = 2131230830;
        public static final int button_search = 2131230748;
        public static final int button_view = 2131230747;
        public static final int button_yes = 2131230783;
        public static final int consider_donation = 2131230908;
        public static final int conversation_options = 2131230837;
        public static final int conversation_with = 2131230766;
        public static final int could_not_find_records = 2131230735;
        public static final int could_not_read_file = 2131230736;
        public static final int date_format_description = 2131230797;
        public static final int default_date_format = 2131230788;
        public static final int delete = 2131230743;
        public static final int delete_backup_confirm_text = 2131230752;
        public static final int delete_backups = 2131230744;
        public static final int delete_confirm_text = 2131230751;
        public static final int deleting_files = 2131230814;
        public static final int deleting_messages = 2131230813;
        public static final int disable_ads_summary = 2131230912;
        public static final int disable_donate_summary = 2131230910;
        public static final int disable_verification_description = 2131230846;
        public static final int disable_vibration_description = 2131230791;
        public static final int disclaimer_text = 2131230891;
        public static final int disclaimer_title = 2131230890;
        public static final int donation_dialog_text = 2131230904;
        public static final int donation_url = 2131230907;
        public static final int email_read_failed = 2131230759;
        public static final int enable_logging_description = 2131230809;
        public static final int enter_backup_filename = 2131230753;
        public static final int error_could_not_load_columns = 2131230817;
        public static final int error_during_backup = 2131230725;
        public static final int error_during_delete_all = 2131230727;
        public static final int error_during_restore = 2131230726;
        public static final int error_message = 2131230734;
        public static final int error_parsing_xml = 2131230889;
        public static final int file_not_found = 2131230730;
        public static final int ftp_folder_description = 2131230882;
        public static final int ftp_password_description = 2131230880;
        public static final int ftp_server_description = 2131230876;
        public static final int ftp_user_description = 2131230878;
        public static final int have_donated = 2131230909;
        public static final int help_title = 2131230764;
        public static final int me = 2131230870;
        public static final int menu_clipboard = 2131230833;
        public static final int menu_donate = 2131230906;
        public static final int menu_email = 2131230774;
        public static final int menu_forward = 2131230835;
        public static final int menu_help = 2131230775;
        public static final int menu_more = 2131230776;
        public static final int menu_newest_first = 2131230871;
        public static final int menu_oldest_first = 2131230872;
        public static final int menu_preferences = 2131230777;
        public static final int menu_quit = 2131230779;
        public static final int menu_restore_conversation = 2131230773;
        public static final int menu_select_all = 2131230864;
        public static final int menu_unselect_all = 2131230865;
        public static final int menu_view = 2131230778;
        public static final int message_copied_clipboard = 2131230834;
        public static final int message_options = 2131230836;
        public static final int motorola_timestamp_fix_description = 2131230869;
        public static final int no_backup_file_to_delete = 2131230769;
        public static final int no_contacts_to_display = 2131230771;
        public static final int no_conversation_selected_error = 2131230863;
        public static final int no_conversations_to_display = 2131230770;
        public static final int no_messages_to_display = 2131230768;
        public static final int no_new_messages_to_backup = 2131230851;
        public static final int no_write_access_to_file = 2131230729;
        public static final int no_write_access_to_folder = 2131230728;
        public static final int operation_completed = 2131230737;
        public static final int operation_failed = 2131230739;
        public static final int operation_succeeded = 2131230738;
        public static final int please_wait = 2131230731;
        public static final int pref_add_contact_name = 2131230802;
        public static final int pref_add_readable_date = 2131230798;
        public static final int pref_add_xsl = 2131230800;
        public static final int pref_adjust_timezone = 2131230868;
        public static final int pref_adjust_timezone_restore = 2131230792;
        public static final int pref_adjust_timezone_view = 2131230794;
        public static final int pref_archive_filename = 2131230845;
        public static final int pref_archive_mode = 2131230842;
        public static final int pref_backup_folder = 2131230852;
        public static final int pref_backup_selected_conversations = 2131230861;
        public static final int pref_backups_to_keep = 2131230805;
        public static final int pref_category_about = 2131230856;
        public static final int pref_category_ftp = 2131230885;
        public static final int pref_category_other = 2131230855;
        public static final int pref_date_format = 2131230796;
        public static final int pref_disable_ads = 2131230911;
        public static final int pref_disable_verification = 2131230838;
        public static final int pref_disable_vibration = 2131230790;
        public static final int pref_disclaimer = 2131230857;
        public static final int pref_enable_logging = 2131230808;
        public static final int pref_ftp_folder = 2131230881;
        public static final int pref_ftp_password = 2131230879;
        public static final int pref_ftp_server = 2131230875;
        public static final int pref_ftp_user = 2131230877;
        public static final int pref_no_notification = 2131230807;
        public static final int pref_readable_date_format = 2131230866;
        public static final int pref_schedule_repeat = 2131230825;
        public static final int pref_schedule_repeat_type = 2131230826;
        public static final int pref_schedule_time = 2131230806;
        public static final int pref_select_conversations = 2131230858;
        public static final int pref_use_ftp = 2131230883;
        public static final int pref_use_schedule = 2131230804;
        public static final int preferences = 2131230767;
        public static final int processing_existing_backup = 2131230840;
        public static final int readable_date_format_description = 2131230867;
        public static final int reset_archive_history = 2131230843;
        public static final int restore = 2131230741;
        public static final int restore_confirm_text = 2131230760;
        public static final int restore_conversation = 2131230742;
        public static final int restore_duplicates_confirm_text = 2131230763;
        public static final int restored_from_non_default_folder = 2131230896;
        public static final int restoring = 2131230810;
        public static final int restoring_conversation = 2131230811;
        public static final int results_truncated = 2131230902;
        public static final int retrieving_conversations = 2131230733;
        public static final int schedule_delete_old_summary = 2131230824;
        public static final int schedule_disable_notification = 2131230831;
        public static final int schedule_disable_notification_summary = 2131230832;
        public static final int schedule_enable = 2131230827;
        public static final int schedule_every = 2131230818;
        public static final int schedule_repeat_summary = 2131230823;
        public static final int schedule_settings = 2131230820;
        public static final int schedule_settings_summary = 2131230821;
        public static final int schedule_start_summary = 2131230822;
        public static final int schedule_starting_at = 2131230819;
        public static final int schedule_summary = 2131230828;
        public static final int scheduled_backup_did_not_complete = 2131230849;
        public static final int scheduled_backups = 2131230789;
        public static final int sdcard_warning = 2131230829;
        public static final int search_for = 2131230897;
        public static final int search_messages = 2131230898;
        public static final int search_results = 2131230899;
        public static final int search_results_not_found = 2131230900;
        public static final int searching = 2131230903;
        public static final int select_a_folder = 2131230893;
        public static final int select_conversations = 2131230859;
        public static final int select_conversations_description = 2131230860;
        public static final int select_file_to_be_deleted = 2131230758;
        public static final int select_file_to_be_searched = 2131230901;
        public static final int select_filename_delete = 2131230757;
        public static final int select_filename_email = 2131230756;
        public static final int select_filename_restore = 2131230754;
        public static final int select_filename_view = 2131230755;
        public static final int select_number = 2131230765;
        public static final int showing_newest_first = 2131230873;
        public static final int showing_oldest_first = 2131230874;
        public static final int unknown_number_of_messages = 2131230815;
        public static final int updating_threads = 2131230816;
        public static final int upload_failed = 2131230887;
        public static final int uploading_file = 2131230886;
        public static final int use_folder = 2131230894;
        public static final int use_ftp_description = 2131230884;
        public static final int verification_failed = 2131230888;
        public static final int verifying_backup = 2131230839;
        public static final int waiting_for_external_storage = 2131230892;
        public static final int welcome = 2131230772;
        public static final int welcome_text = 2131230905;
    }

    public static final class style {
        public static final int ActionBarCompat = 2131296259;
        public static final int ActionBarCompatButton = 2131296260;
        public static final int ActionBarCompatSeparator = 2131296261;
        public static final int ActionBarCompatText = 2131296262;
        public static final int Classic = 2131296256;
        public static final int ClassicDialog = 2131296258;
        public static final int ClassicListItemContainer = 2131296269;
        public static final int ClassicListItemSubtitle = 2131296265;
        public static final int ClassicListItemTitle = 2131296263;
        public static final int ClassicPushButton = 2131296273;
        public static final int ListItemContainerBase = 2131296267;
        public static final int ListItemContent = 2131296271;
        public static final int PreferenceCategory = 2131296275;
        public static final int PushButton = 2131296272;
        public static final int White = 2131296257;
        public static final int WhiteListItemContainer = 2131296270;
        public static final int WhiteListItemSubtitle = 2131296266;
        public static final int WhiteListItemTitle = 2131296264;
        public static final int WhiteSimpleListItem = 2131296268;
        public static final int WhiteSpinner = 2131296274;
    }

    public static final class styleable {
        public static final int[] AppTheme = {R.attr.actionbarCompatButtonStyle, R.attr.actionbarCompatProgressIndicatorStyle, R.attr.actionbarCompatSeparatorStyle, R.attr.actionbarCompatLogoStyle, R.attr.actionbarCompatTextStyle, R.attr.listItemContainer, R.attr.listItemTitle, R.attr.listItemSubtitle, R.attr.spinnerStyle};
        public static final int AppTheme_actionbarCompatButtonStyle = 0;
        public static final int AppTheme_actionbarCompatLogoStyle = 3;
        public static final int AppTheme_actionbarCompatProgressIndicatorStyle = 1;
        public static final int AppTheme_actionbarCompatSeparatorStyle = 2;
        public static final int AppTheme_actionbarCompatTextStyle = 4;
        public static final int AppTheme_listItemContainer = 5;
        public static final int AppTheme_listItemSubtitle = 7;
        public static final int AppTheme_listItemTitle = 6;
        public static final int AppTheme_spinnerStyle = 8;
    }
}
