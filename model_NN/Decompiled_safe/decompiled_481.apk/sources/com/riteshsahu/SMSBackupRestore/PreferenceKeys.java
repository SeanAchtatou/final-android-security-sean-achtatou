package com.riteshsahu.SMSBackupRestore;

public class PreferenceKeys {
    public static String AskedForDonation = "AskedForDonation";
    public static String BackupDonateCount = "Backup Donate Count";
    public static String DisableAds = "Do not show Ads";
    public static String DisableDonate = "Disable Donate";
}
