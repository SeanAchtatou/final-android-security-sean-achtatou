package defpackage;

import android.webkit.WebView;
import com.google.ads.AdRequest;
import com.google.ads.util.a;
import com.riteshsahu.SMSBackupRestoreBase.Common;
import java.util.HashMap;

/* renamed from: o  reason: default package */
public final class o implements i {
    public final void a(d dVar, HashMap<String, String> hashMap, WebView webView) {
        a.e("Invalid " + hashMap.get(Common.TypeAttributeName) + " request error: " + hashMap.get("errors"));
        c g = dVar.g();
        if (g != null) {
            g.a(AdRequest.ErrorCode.INVALID_REQUEST);
        }
    }
}
