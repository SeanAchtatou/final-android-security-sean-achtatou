package com.dianxinos.a.a;

import android.content.Context;
import android.util.Log;
import com.dianxinos.dxservices.a.g;
import com.dianxinos.dxservices.a.j;
import com.dianxinos.dxservices.a.p;
import com.dianxinos.dxservices.a.s;
import com.dianxinos.dxservices.a.u;
import com.dianxinos.dxservices.a.x;
import com.dianxinos.dxservices.b.d;

/* compiled from: DXCore */
public class a {
    private static volatile a hK;
    private static Integer hL = 2;
    private g hM = new g(this.mContext);
    private Context mContext;

    public static a F(Context context) {
        synchronized (a.class) {
            if (hK == null) {
                hK = new a(context);
            }
        }
        return hK;
    }

    private a(Context context) {
        this.mContext = context.getApplicationContext();
        this.hM.V();
    }

    public void init() {
    }

    public void destroy() {
    }

    public boolean bB() {
        return a("start", 0, "");
    }

    public boolean a(String str, String str2, Number number) {
        return a(str, 1, d.a(str2, number));
    }

    public boolean a(String str, int i, Object obj) {
        return a(str, i, hL == null ? 1 : hL.intValue(), obj);
    }

    public boolean a(String str, int i, int i2, Object obj) {
        return a(str, i, i2, 5, obj);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.dianxinos.dxservices.a.g.a(com.dianxinos.dxservices.a.p, boolean):boolean
     arg types: [com.dianxinos.dxservices.a.p, int]
     candidates:
      com.dianxinos.dxservices.a.g.a(com.dianxinos.dxservices.a.g, com.dianxinos.dxservices.a.e):com.dianxinos.dxservices.a.e
      com.dianxinos.dxservices.a.g.a(java.lang.Number, java.lang.Number):java.lang.Number
      com.dianxinos.dxservices.a.g.a(int, java.lang.String):java.lang.Object
      com.dianxinos.dxservices.a.g.a(int, java.lang.Object):java.lang.String
      com.dianxinos.dxservices.a.g.a(com.dianxinos.dxservices.a.g, java.util.ArrayList):java.util.ArrayList
      com.dianxinos.dxservices.a.g.a(com.dianxinos.dxservices.a.g, com.dianxinos.dxservices.a.p):boolean
      com.dianxinos.dxservices.a.g.a(com.dianxinos.dxservices.a.p, int):boolean
      com.dianxinos.dxservices.a.g.a(java.lang.String, com.dianxinos.dxservices.a.p):boolean
      com.dianxinos.dxservices.a.g.a(com.dianxinos.dxservices.a.p, boolean):boolean */
    public boolean a(String str, int i, int i2, int i3, Object obj) {
        if (str == null || str.length() == 0) {
            if (Log.isLoggable("DXCore", 6)) {
                Log.e("DXCore", "Invalid key: " + str);
            }
            return false;
        } else if (!x.contains(i)) {
            if (Log.isLoggable("DXCore", 6)) {
                Log.e("DXCore", "Invalid data policy: " + i);
            }
            return false;
        } else if (!j.contains(i2)) {
            if (Log.isLoggable("DXCore", 6)) {
                Log.e("DXCore", "Invalid report policy: " + i2);
            }
            return false;
        } else if (i3 < 0 || i3 > 9) {
            if (Log.isLoggable("DXCore", 6)) {
                Log.e("DXCore", "Invalid priority which should be in range [0-9].");
            }
            return false;
        } else if (obj == null) {
            if (Log.isLoggable("DXCore", 6)) {
                Log.e("DXCore", "Invalid value which should be required.");
            }
            return false;
        } else {
            int b = u.b(i, obj);
            if (!u.contains(b)) {
                if (Log.isLoggable("DXCore", 6)) {
                    Log.e("DXCore", "Invalid data type for data policy " + i + ": " + obj.getClass().getName());
                }
                return false;
            }
            return this.hM.a(new p(i2, b, i, p.d(this.mContext, str), i3, obj, null), true);
        }
    }

    public void L(int i) {
        s.d(this.mContext, i);
    }
}
