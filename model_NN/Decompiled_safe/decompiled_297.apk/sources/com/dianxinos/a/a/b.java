package com.dianxinos.a.a;

import android.content.Context;
import android.os.SystemProperties;
import android.util.Log;
import com.dianxinos.dxservices.b.a;

/* compiled from: DXApi */
public class b {
    private static final String jq = SystemProperties.get("ro.dianxinos.feedback.url");

    public static String L(Context context) {
        try {
            return a.a(context.getApplicationContext()).getToken();
        } catch (Exception e) {
            if (Log.isLoggable("DXApi", 6)) {
                Log.e("DXApi", "Failed to get the token.", e);
            }
            return null;
        }
    }
}
