package com.dianxinos.powermanager;

import android.os.AsyncTask;
import android.widget.TextView;

/* compiled from: PowerMgrHomeActivity */
class f extends AsyncTask {
    private boolean cA;
    final /* synthetic */ PowerMgrHomeActivity cB;

    private f(PowerMgrHomeActivity powerMgrHomeActivity) {
        this.cB = powerMgrHomeActivity;
        this.cA = false;
    }

    /* synthetic */ f(PowerMgrHomeActivity powerMgrHomeActivity, p pVar) {
        this(powerMgrHomeActivity);
    }

    public void al() {
        this.cA = true;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Void doInBackground(Void... voidArr) {
        for (int i = 10; i >= 0 && !this.cA; i--) {
            publishProgress(Integer.valueOf(i));
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
            }
        }
        for (int i2 = 0; i2 < 11 && !this.cA; i2++) {
            publishProgress(Integer.valueOf(i2));
            try {
                Thread.sleep(50);
            } catch (InterruptedException e2) {
            }
        }
        this.cA = false;
        return null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onProgressUpdate(Integer... numArr) {
        int intValue = numArr[0].intValue();
        if (this.cB.du != 3) {
            this.cB.db.setTextColor(this.cB.d(this.cB.du, intValue));
        }
        ((TextView) this.cB.cQ.get(0)).setTextColor(this.cB.d(this.cB.dv, intValue));
        ((TextView) this.cB.cQ.get(1)).setTextColor(this.cB.d(this.cB.dw, intValue));
        ((TextView) this.cB.cQ.get(2)).setTextColor(this.cB.d(this.cB.dx, intValue));
        ((TextView) this.cB.cQ.get(3)).setTextColor(this.cB.d(this.cB.dy, intValue));
        ((TextView) this.cB.cQ.get(4)).setTextColor(this.cB.d(this.cB.dz, intValue));
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(Void voidR) {
        this.cB.db.setTextColor(this.cB.f0do);
    }
}
