package com.dianxinos.powermanager;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.dianxinos.powermanager.b.c;
import com.dianxinos.powermanager.b.h;
import com.dianxinos.powermanager.b.i;
import com.dianxinos.powermanager.b.j;
import com.dianxinos.powermanager.b.k;
import com.dianxinos.powermanager.b.o;
import com.dianxinos.powermanager.b.q;
import com.dianxinos.powermanager.b.s;
import com.dianxinos.powermanager.c.e;
import com.dianxinos.powermanager.ui.d;

public class HwPowerUsageDetails extends Activity implements View.OnClickListener {
    private o C;
    private com.dianxinos.powermanager.menu.o bI;
    private View bJ;
    private View bK;
    private ViewGroup bL;
    private View bM;
    private View bN;
    private ViewGroup bO;
    private View bP;
    private Button bQ;
    private ViewGroup bR;
    private Button bS;
    private int bT;
    private LayoutInflater mInflater;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.power_usage_details);
        this.bI = new com.dianxinos.powermanager.menu.o(this);
        try {
            init();
        } catch (Exception e) {
            finish();
            e.e("HwPowerUsageDetails", "Process killed??? Exception: " + e);
        }
    }

    private void init() {
        Intent intent = getIntent();
        int intExtra = intent.getIntExtra("position", 0);
        double doubleExtra = intent.getDoubleExtra("bar_percent", 0.0d);
        this.mInflater = LayoutInflater.from(this);
        this.C = o.K(this);
        j jVar = (j) this.C.ck().fC.hH.get(intExtra);
        this.bT = jVar.bT;
        View findViewById = findViewById(C0000R.id.summary_header);
        ((ImageView) findViewById.findViewById(C0000R.id.icon)).setImageResource(c.n(this.bT));
        ((TextView) findViewById.findViewById(C0000R.id.label)).setText(c.m(this.bT));
        ((TextView) findViewById.findViewById(C0000R.id.progress)).setText(String.format("%.1f%%", Double.valueOf(jVar.hw)));
        ((ImageView) findViewById.findViewById(C0000R.id.progress_image)).setImageDrawable(new d(getResources().getDrawable(C0000R.drawable.list_item_progress_bar), getResources().getDrawable(C0000R.drawable.list_item_progress_bkg), doubleExtra));
        this.bJ = findViewById(C0000R.id.data_stats_layout);
        this.bK = findViewById(C0000R.id.data_stats_switch);
        this.bK.setOnClickListener(this);
        this.bL = (ViewGroup) findViewById(C0000R.id.data_stats_group);
        this.bM = findViewById(C0000R.id.operation_layout);
        this.bN = findViewById(C0000R.id.operation_switch);
        this.bN.setOnClickListener(this);
        this.bO = (ViewGroup) findViewById(C0000R.id.operation_group);
        this.bP = findViewById(C0000R.id.children_layout);
        this.bQ = (Button) findViewById(C0000R.id.children_switch);
        this.bQ.setOnClickListener(this);
        this.bR = (ViewGroup) findViewById(C0000R.id.children_group);
        a(this.bL, jVar.hO, (int) C0000R.string.hw_detail_labels_running_time);
        if (this.bL.getChildCount() == 0) {
            this.bJ.setVisibility(8);
        }
        TextView textView = (TextView) findViewById(C0000R.id.operation_label);
        this.bS = (Button) findViewById(C0000R.id.operation_btn);
        this.bS.setOnClickListener(this);
        this.bS.setText((int) C0000R.string.hw_detail_operation_btn_common);
        if (this.bT == 2) {
            textView.setText((int) C0000R.string.hw_detail_operation_tip_wifi);
        } else if (this.bT == 1) {
            textView.setText((int) C0000R.string.hw_detail_operation_tip_screen);
        } else if (this.bT == 3) {
            textView.setText((int) C0000R.string.hw_detail_operation_tip_bluetooth);
        } else if (this.bT == 7) {
            textView.setText((int) C0000R.string.hw_detail_operation_tip_gps);
        } else {
            this.bM.setVisibility(8);
        }
        this.bQ.setText((int) C0000R.string.hw_detail_apps_usage);
        a(jVar);
    }

    private void a(ViewGroup viewGroup, long j, int i) {
        int i2 = (int) (j / 1000);
        if (i2 > 0) {
            a(viewGroup, com.dianxinos.powermanager.c.c.b(this, i2), i);
        }
    }

    private void a(ViewGroup viewGroup, String str, int i) {
        View inflate = this.mInflater.inflate((int) C0000R.layout.power_data_stats_item, (ViewGroup) null);
        viewGroup.addView(inflate);
        ((TextView) inflate.findViewById(C0000R.id.name)).setText(i);
        ((TextView) inflate.findViewById(C0000R.id.value)).setText(str);
    }

    private void a(j jVar) {
        String str;
        String str2;
        Drawable drawable;
        n nVar = new n(this);
        k H = k.H(this);
        int size = jVar.hH.size();
        int i = 0;
        while (i < size && i < 10) {
            i iVar = (i) jVar.hH.get(i);
            if (iVar instanceof q) {
                q qVar = (q) iVar;
                s d = H.d(qVar.mUid, qVar.bl);
                String str3 = d.label;
                str2 = d.kb;
                str = str3;
                drawable = d.icon;
            } else {
                h hVar = (h) iVar;
                str = hVar.hy;
                str2 = null;
                drawable = getResources().getDrawable(c.n(hVar.bT));
            }
            if (iVar.hw < 0.10000000149011612d) {
                break;
            }
            if (i > 0) {
                ImageView imageView = new ImageView(this);
                imageView.setImageResource(C0000R.drawable.horizontal_line);
                imageView.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
                this.bR.addView(imageView);
            }
            View inflate = this.mInflater.inflate((int) C0000R.layout.power_usage_sub_list_item, (ViewGroup) null);
            ((ImageView) inflate.findViewById(C0000R.id.icon)).setImageDrawable(drawable);
            ((TextView) inflate.findViewById(C0000R.id.label)).setText(str);
            ((TextView) inflate.findViewById(C0000R.id.progress)).setText(String.format("%.1f%%", Double.valueOf(iVar.hw)));
            if (str2 != null) {
                inflate.setFocusable(true);
                inflate.setClickable(true);
                inflate.setBackgroundResource(C0000R.drawable.power_usage_sub_list_item_bkg);
                inflate.setTag(str2);
                inflate.setOnClickListener(nVar);
                inflate.findViewById(C0000R.id.indicator).setVisibility(0);
            }
            this.bR.addView(inflate);
            i++;
        }
        if (this.bR.getChildCount() == 0) {
            this.bP.setVisibility(8);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.C = null;
        super.onDestroy();
    }

    public void onClick(View view) {
        boolean z;
        int i;
        boolean z2;
        int i2;
        int i3;
        int i4 = C0000R.drawable.power_usage_details_switch_down;
        if (view == this.bK) {
            boolean z3 = this.bL.getVisibility() == 0;
            View view2 = this.bK;
            if (!z3) {
                i4 = C0000R.drawable.power_usage_details_switch_up;
            }
            view2.setBackgroundResource(i4);
            ViewGroup viewGroup = this.bL;
            if (z3) {
                i3 = 8;
            } else {
                i3 = 0;
            }
            viewGroup.setVisibility(i3);
        } else if (view == this.bN) {
            if (this.bO.getVisibility() == 0) {
                z2 = true;
            } else {
                z2 = false;
            }
            View view3 = this.bN;
            if (!z2) {
                i4 = C0000R.drawable.power_usage_details_switch_up;
            }
            view3.setBackgroundResource(i4);
            ViewGroup viewGroup2 = this.bO;
            if (z2) {
                i2 = 8;
            } else {
                i2 = 0;
            }
            viewGroup2.setVisibility(i2);
        } else if (view == this.bQ) {
            if (this.bR.getVisibility() == 0) {
                z = true;
            } else {
                z = false;
            }
            Button button = this.bQ;
            if (!z) {
                i4 = C0000R.drawable.power_usage_details_switch_up;
            }
            button.setBackgroundResource(i4);
            ViewGroup viewGroup3 = this.bR;
            if (z) {
                i = 8;
            } else {
                i = 0;
            }
            viewGroup3.setVisibility(i);
        } else if (view == this.bS) {
            p(this.bT);
        }
    }

    private void p(int i) {
        if (i == 2) {
            startActivity(new Intent("android.settings.WIFI_SETTINGS"));
        } else if (i == 1) {
            startActivity(new Intent("android.settings.DISPLAY_SETTINGS"));
        } else if (i == 3) {
            startActivity(new Intent("android.settings.BLUETOOTH_SETTINGS"));
        } else if (i == 7) {
            startActivity(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"));
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return this.bI.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        return this.bI.onOptionsItemSelected(menuItem);
    }
}
