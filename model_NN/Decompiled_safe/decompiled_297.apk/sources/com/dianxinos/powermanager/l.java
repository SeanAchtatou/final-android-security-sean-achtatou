package com.dianxinos.powermanager;

import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.os.SystemClock;
import android.widget.Toast;
import com.dianxinos.powermanager.c.e;
import com.dianxinos.powermanager.menu.k;
import java.util.List;
import java.util.Timer;

/* compiled from: AutoCleanuper */
public class l {
    private static l eT;
    private k dW;
    private long eU;
    private Timer eV;
    /* access modifiers changed from: private */
    public Context mContext;

    public static l v(Context context) {
        if (eT == null) {
            eT = new l(context);
        }
        return eT;
    }

    private l(Context context) {
        this.mContext = context;
        this.dW = k.I(context);
    }

    public boolean aI() {
        return SystemClock.elapsedRealtime() - this.eU > 60000;
    }

    public int d(int i, boolean z) {
        int i2;
        int i3 = 0;
        ActivityManager activityManager = (ActivityManager) this.mContext.getSystemService("activity");
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = activityManager.getRunningAppProcesses();
        int size = runningAppProcesses.size();
        int i4 = 0;
        while (i3 < size) {
            String str = runningAppProcesses.get(i3).processName;
            try {
                ApplicationInfo applicationInfo = this.mContext.getPackageManager().getApplicationInfo(str, 8704);
                if (n(str) || (runningAppProcesses.get(i3).importance >= i && (applicationInfo.flags & 1) == 0 && !m(str))) {
                    activityManager.killBackgroundProcesses(runningAppProcesses.get(i3).processName);
                    i2 = i4 + 1;
                    i3++;
                    i4 = i2;
                }
            } catch (Exception e) {
                e.e("AutoCleanuper", "Exception caught: " + e);
            }
            i2 = i4;
            i3++;
            i4 = i2;
        }
        if (z) {
            this.eU = SystemClock.elapsedRealtime();
            this.eV = new Timer();
            this.eV.schedule(new y(this), 30000);
        }
        return i4;
    }

    public void f(int i, int i2) {
        String string;
        e.d("AutoCleanuper", "showToast, num: " + i + ", time: " + i2);
        if (i == 0) {
            string = this.mContext.getString(C0000R.string.onkey_no_needed_kill_app);
        } else {
            int i3 = i2 / 60;
            if (i3 < 3) {
                i3 = 3;
            }
            string = this.mContext.getString(C0000R.string.onkey_kill_app, Integer.valueOf(i), Integer.valueOf(i3));
        }
        Toast.makeText(this.mContext, string, 0).show();
    }

    private boolean m(String str) {
        if (str.equals("com.dianxinos.powermanager") || str.equals("com.dianxinos.battery")) {
            return true;
        }
        return this.dW.B(str);
    }

    private boolean n(String str) {
        if (str.equalsIgnoreCase("com.android.camera")) {
            return true;
        }
        return false;
    }

    public static void a(Context context, String str) {
        try {
            ((ActivityManager) context.getSystemService("activity")).killBackgroundProcesses(str);
        } catch (Exception e) {
            e.e("AutoCleanuper", "Exception caught: " + e);
        }
    }
}
