package com.dianxinos.powermanager;

import android.app.TabActivity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TextView;
import com.dianxinos.a.a.a;
import com.dianxinos.powermanager.PowerMgrMidWidgetProvider;
import com.dianxinos.powermanager.b.k;
import com.dianxinos.powermanager.menu.AppSettings;
import com.dianxinos.powermanager.menu.FeedbackDialog;
import com.dianxinos.powermanager.menu.HelperActivity;
import com.dianxinos.powermanager.menu.d;
import com.dianxinos.powermanager.mode.ModeMgrActivity;
import com.dianxinos.powermanager.update.i;

public class PowerMgrActivity extends TabActivity {
    private a by;
    private TabHost fv;
    private Intent fw;
    private Intent fx;
    private Intent fy;
    private Intent fz;

    /* JADX WARNING: Removed duplicated region for block: B:6:0x0064  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r6) {
        /*
            r5 = this;
            r4 = 1
            r2 = 2130837572(0x7f020044, float:1.7280102E38)
            r3 = 0
            super.onCreate(r6)
            r0 = 2130903084(0x7f03002c, float:1.7412976E38)
            r5.setContentView(r0)
            android.widget.TabWidget r0 = r5.getTabWidget()
            r0.setStripEnabled(r3)
            r5.aS()
            r5.aT()
            android.view.View r1 = r0.getChildAt(r3)
            r1.setBackgroundResource(r2)
            android.view.View r1 = r0.getChildAt(r4)
            r1.setBackgroundResource(r2)
            r1 = 2
            android.view.View r1 = r0.getChildAt(r1)
            r1.setBackgroundResource(r2)
            r1 = 3
            android.view.View r0 = r0.getChildAt(r1)
            r0.setBackgroundResource(r2)
            if (r6 != 0) goto L_0x0085
            com.dianxinos.powermanager.c.b r0 = new com.dianxinos.powermanager.c.b
            r0.<init>(r5)
            boolean r1 = r0.D()
            if (r1 == 0) goto L_0x0085
            com.dianxinos.powermanager.update.d r1 = new com.dianxinos.powermanager.update.d
            r1.<init>(r5)
            r2 = 2131230960(0x7f0800f0, float:1.8077988E38)
            r1.setTitle(r2)
            r2 = 2131230961(0x7f0800f1, float:1.807799E38)
            r1.B(r2)
            r2 = 0
            r1.a(r3, r2)
            r1.show()
            r0.b(r4)
            r0 = r4
        L_0x0062:
            if (r0 != 0) goto L_0x0067
            com.dianxinos.powermanager.update.i.a(r5)
        L_0x0067:
            com.dianxinos.a.a.a r0 = com.dianxinos.a.a.a.F(r5)
            r5.by = r0
            com.dianxinos.a.a.a r0 = r5.by
            r0.init()
            com.dianxinos.a.a.a r0 = r5.by
            r0.bB()
            android.content.Intent r0 = r5.getIntent()
            java.lang.String r1 = "From"
            int r0 = r0.getIntExtra(r1, r3)
            r5.D(r0)
            return
        L_0x0085:
            r0 = r3
            goto L_0x0062
        */
        throw new UnsupportedOperationException("Method not decompiled: com.dianxinos.powermanager.PowerMgrActivity.onCreate(android.os.Bundle):void");
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        setIntent(intent);
        D(intent.getIntExtra("From", 0));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.dianxinos.a.a.a.a(java.lang.String, java.lang.String, java.lang.Number):boolean
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.dianxinos.a.a.a.a(java.lang.String, int, java.lang.Object):boolean
      com.dianxinos.a.a.a.a(java.lang.String, java.lang.String, java.lang.Number):boolean */
    private void D(int i) {
        if (i == 2) {
            startService(new Intent(this, PowerMgrMidWidgetProvider.UpdataService.class));
            this.by.a("widget14", "enter", (Number) 1);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        k.H(this).clearCache();
        i.bS();
        this.by.destroy();
        super.onDestroy();
    }

    private void aS() {
        this.fw = new Intent(this, PowerMgrHomeActivity.class);
        this.fx = new Intent(this, ModeMgrActivity.class);
        this.fy = new Intent(this, AppsPowerUsage.class);
        this.fz = new Intent(this, HardwaresPowerUsageSummary.class);
    }

    private void aT() {
        this.fv = getTabHost();
        TabHost tabHost = this.fv;
        tabHost.addTab(a("tab_tag_home", C0000R.string.tab_battery, C0000R.drawable.tab_home_icon, this.fw));
        tabHost.addTab(a("tab_tag_appmgr", C0000R.string.apps_power_usage, C0000R.drawable.tab_app_icon, this.fy));
        tabHost.addTab(a("tab_tag_hardmgr", C0000R.string.hardware_power_usage, C0000R.drawable.tab_hardware_icon, this.fz));
        tabHost.addTab(a("tab_tag_mode", C0000R.string.mode_settings, C0000R.drawable.tab_mode_icon, this.fx));
    }

    private TabHost.TabSpec a(String str, int i, int i2, Intent intent) {
        return this.fv.newTabSpec(str).setIndicator(h(i, i2)).setContent(intent);
    }

    private View h(int i, int i2) {
        View inflate = getLayoutInflater().inflate((int) C0000R.layout.tab_main_indicator, (ViewGroup) null);
        ((ImageView) inflate.findViewById(C0000R.id.icon)).setImageResource(i2);
        ((TextView) inflate.findViewById(C0000R.id.title)).setText(i);
        return inflate;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater();
        menu.add(0, 101, 0, getString(C0000R.string.menu_feedback)).setIcon((int) C0000R.drawable.menu_feedback);
        menu.add(0, 102, 0, getString(C0000R.string.menu_share)).setIcon((int) C0000R.drawable.menu_share);
        menu.add(0, 103, 0, getString(C0000R.string.menu_help_about)).setIcon((int) C0000R.drawable.menu_help);
        menu.add(0, 104, 0, getString(C0000R.string.menu_update)).setIcon((int) C0000R.drawable.menu_update);
        menu.add(0, 105, 0, getString(C0000R.string.menu_settings)).setIcon((int) C0000R.drawable.menu_settings);
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.dianxinos.a.a.a.a(java.lang.String, java.lang.String, java.lang.Number):boolean
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.dianxinos.a.a.a.a(java.lang.String, int, java.lang.Object):boolean
      com.dianxinos.a.a.a.a(java.lang.String, java.lang.String, java.lang.Number):boolean */
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 101:
                startActivity(new Intent(this, FeedbackDialog.class));
                return true;
            case 102:
                new d(this).show();
                this.by.a("clicks", "share", (Number) 1);
                return true;
            case 103:
                startActivity(new Intent(this, HelperActivity.class));
                return true;
            case 104:
                i.b(this);
                this.by.a("clicks", "update", (Number) 1);
                return true;
            case 105:
                startActivity(new Intent(this, AppSettings.class));
                return true;
            default:
                return false;
        }
    }
}
