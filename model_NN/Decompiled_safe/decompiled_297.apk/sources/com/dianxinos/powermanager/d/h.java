package com.dianxinos.powermanager.d;

import android.app.ActivityManager;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import com.dianxinos.powermanager.b.m;
import com.dianxinos.powermanager.c.e;
import com.dianxinos.powermanager.mode.i;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

/* compiled from: RemainingTimeImpl */
class h {
    private a dY;
    ConnectivityManager g;
    ActivityManager kW;
    private String kX;
    private ArrayList kY = new ArrayList();
    private boolean kZ = false;
    private boolean la = false;
    private double lb;
    private double lc;
    private double ld;
    private double le;
    private double lf;
    private double lg;
    private double lh;
    private double li;
    private double lj;
    private double lk;
    private double ll;
    private double lm;
    private double ln;
    private double lo;
    private double lp;
    private double lq;
    private double lr;
    private boolean ls = true;
    private boolean lt = true;
    private double lu = 0.1d;
    private double lv = 0.1d;
    private double lw = 0.1d;
    private double lx = 0.1d;
    private double ly = 0.01d;
    private double lz = 0.01d;
    private Context mContext;

    public h(Context context) {
        this.mContext = context;
        this.dY = a.c(context);
        m mVar = new m(context);
        this.g = (ConnectivityManager) context.getSystemService("connectivity");
        this.kW = (ActivityManager) this.mContext.getSystemService("activity");
        double averagePower = (mVar.getAveragePower("battery.capacity") * 3600.0d) / 100.0d;
        this.lg = ((mVar.getAveragePower("screen.on") / 1000.0d) * 1000.0d) / averagePower;
        this.lh = mVar.getAveragePower("screen.full") / averagePower;
        this.li = mVar.getAveragePower("wifi.on") / averagePower;
        this.lj = mVar.getAveragePower("wifi.active") / averagePower;
        this.lk = mVar.getAveragePower("bluetooth.on") / averagePower;
        this.ll = mVar.getAveragePower("radio.on", 0) / averagePower;
        this.lm = mVar.getAveragePower("radio.active") / averagePower;
        this.ln = mVar.getAveragePower("dsp.audio") / averagePower;
        this.lo = mVar.getAveragePower("dsp.video") / averagePower;
        this.lp = mVar.getAveragePower("cpu.idle") / averagePower;
        this.lq = mVar.getAveragePower("cpu.active", mVar.getNumSpeedSteps() - 1) / averagePower;
        this.lr = mVar.getAveragePower("gps.on") / averagePower;
    }

    public void O() {
        i t = i.t(this.mContext);
        com.dianxinos.powermanager.mode.m aA = t.aA();
        G(t.az());
        m(0, aA.F(0));
        m(1, aA.F(1));
        m(5, aA.F(2));
        m(6, aA.F(3));
        m(4, aA.F(4));
        m(2, aA.F(10));
        m(3, aA.F(5));
        m(7, aA.F(9));
        m(8, aA.F(6));
        m(9, aA.F(7));
        m(10, aA.F(12));
        cS();
    }

    public void G(String str) {
        if (this.kZ) {
            e.e("RemainingTimeImpl", "Discard the pending mode swithcing and start a new one");
        }
        this.kX = str;
        this.kY.clear();
        this.kZ = true;
        this.la = false;
    }

    public void m(int i, int i2) {
        if (this.kZ) {
            this.kY.add(new g(i, i2));
        } else {
            e.f("RemainingTimeImpl", "Logic error! Adding settings but not in mode switching.");
        }
    }

    private boolean ae(int i) {
        return i == 1;
    }

    public void cS() {
        boolean z;
        double d;
        int i;
        int i2;
        int i3;
        if (!this.kZ) {
            e.f("RemainingTimeImpl", "Logic error! Finish but not in pending mode switching.");
            return;
        }
        this.lb = this.lp;
        this.lc = 0.0d;
        this.ld = 0.0d;
        this.le = 0.0d;
        this.lf = 0.0d;
        this.ls = false;
        this.lt = false;
        if (Settings.System.getInt(this.mContext.getContentResolver(), "airplane_mode_on", 0) == 1) {
            z = true;
        } else {
            z = false;
        }
        Iterator it = this.kY.iterator();
        int i4 = 128;
        int i5 = 1;
        int i6 = 30;
        while (it.hasNext()) {
            g gVar = (g) it.next();
            switch (gVar.mType) {
                case 0:
                    int i7 = i5;
                    i2 = i6;
                    i3 = gVar.mValue;
                    i = i7;
                    continue;
                    i4 = i3;
                    i6 = i2;
                    i5 = i;
                case 1:
                    i3 = i4;
                    int i8 = gVar.mValue;
                    i = i5;
                    i2 = i8;
                    continue;
                    i4 = i3;
                    i6 = i2;
                    i5 = i;
                case 2:
                    if (ae(gVar.mValue)) {
                        i = (int) (((double) i5) + 0.005d);
                        i2 = i6;
                        i3 = i4;
                        continue;
                        i4 = i3;
                        i6 = i2;
                        i5 = i;
                    }
                    break;
                case 3:
                    if (ae(gVar.mValue)) {
                        i = (int) (((double) i5) + 0.005d);
                        i2 = i6;
                        i3 = i4;
                        continue;
                        i4 = i3;
                        i6 = i2;
                        i5 = i;
                    }
                    break;
                case 4:
                    if (ae(gVar.mValue) && !z) {
                        this.lt = true;
                        i = i5;
                        i2 = i6;
                        i3 = i4;
                        continue;
                        i4 = i3;
                        i6 = i2;
                        i5 = i;
                    }
                case 5:
                    if (ae(gVar.mValue)) {
                        this.lb += this.li * this.lu;
                        this.lc += this.li;
                        this.ld += this.li;
                        this.le += this.li;
                        this.lf += this.li;
                        this.lt = true;
                        i = i5;
                        i2 = i6;
                        i3 = i4;
                        continue;
                        i4 = i3;
                        i6 = i2;
                        i5 = i;
                    }
                    break;
                case 6:
                    if (ae(gVar.mValue)) {
                        this.lb += this.lk * this.lw;
                        this.lc += this.lk;
                        this.ld += this.lk;
                        this.le += this.lk;
                        this.lf += this.lk;
                        i = i5;
                        i2 = i6;
                        i3 = i4;
                        continue;
                        i4 = i3;
                        i6 = i2;
                        i5 = i;
                    }
                    break;
                case 7:
                    if (ae(gVar.mValue)) {
                        double d2 = this.lr * this.lz;
                        this.lc += d2;
                        this.ld += d2;
                        this.le += d2;
                        this.lf = d2 + this.lf;
                        i = i5;
                        i2 = i6;
                        i3 = i4;
                        continue;
                        i4 = i3;
                        i6 = i2;
                        i5 = i;
                    }
                    break;
                case 8:
                    if (ae(gVar.mValue)) {
                        i = (int) (((double) i5) + 0.005d);
                        i2 = i6;
                        i3 = i4;
                        continue;
                        i4 = i3;
                        i6 = i2;
                        i5 = i;
                    }
                    break;
                case 9:
                    if (ae(gVar.mValue)) {
                        i = (int) (((double) i5) + 0.01d);
                        i2 = i6;
                        i3 = i4;
                        continue;
                        i4 = i3;
                        i6 = i2;
                        i5 = i;
                    }
                    break;
                case 10:
                    this.ls = ae(gVar.mValue);
                    if (this.ls) {
                        this.lb += this.ll;
                        this.lc += this.ll;
                        this.ld += this.ll;
                        this.le += this.ll;
                        this.lf += this.ll;
                        this.lc += this.lm;
                        i = i5;
                        i2 = i6;
                        i3 = i4;
                        continue;
                        i4 = i3;
                        i6 = i2;
                        i5 = i;
                    }
                    break;
            }
            i = i5;
            i2 = i6;
            i3 = i4;
            i4 = i3;
            i6 = i2;
            i5 = i;
        }
        double d3 = (this.ly * ((double) i6)) / 30.0d;
        this.lb += this.lg * d3;
        this.lc += this.lg * d3;
        this.ld += this.lg;
        this.le += this.lg;
        this.lf += this.lg * d3;
        if (i4 == -1) {
            d = this.lh * 0.33d;
        } else {
            d = (this.lh * ((double) i4)) / 100.0d;
        }
        this.lb += d * d3;
        this.lc += d * d3;
        this.ld += d;
        this.le += d;
        this.lf = (d3 * d) + this.lf;
        this.lf += this.ln;
        this.le += this.ln;
        this.le += this.lo;
        NetworkInfo activeNetworkInfo = this.g.getActiveNetworkInfo();
        if (activeNetworkInfo != null) {
            int type = activeNetworkInfo.getType();
            if (type == 0) {
                this.ld += this.lm * this.lv;
            } else if (type == 1) {
                this.ld += this.lj * this.lv;
            }
        }
        if (this.ls) {
            this.lc += this.lq * this.lx;
        }
        this.ld += this.lq * this.lx;
        this.le += this.lq * this.lx;
        this.lf += this.lq * this.lx;
        this.lb *= (double) i5;
        this.lc *= (double) i5;
        this.ld *= (double) i5;
        this.le *= (double) i5;
        this.lf *= (double) i5;
        this.kZ = false;
        this.la = true;
    }

    public int a(double d, int i) {
        double d2;
        if (!this.la) {
            e.f("RemainingTimeImpl", "not in a mode");
            return -1;
        }
        double d3 = this.lb;
        switch (i) {
            case 1:
                d3 = this.lc;
                if (!this.ls) {
                    return -1;
                }
                break;
            case 2:
                d3 = this.ld;
                if (!this.lt) {
                    return -1;
                }
                break;
            case 3:
                d3 = this.le;
                break;
            case 4:
                d3 = this.lf;
                break;
            case 5:
                d3 = this.lb + (this.lf * 0.01d) + (this.ld * 0.2d) + (this.le * 0.02d) + (this.lc * 0.05d) + cT();
                if (this.dY.I()) {
                    double J = this.dY.J();
                    if (J > 0.0d) {
                        d3 = (d3 * 0.4d) + (J * 0.6d);
                        break;
                    }
                }
                break;
        }
        if (d3 > 0.0d) {
            d2 = d / d3;
        } else {
            d2 = -1.0d;
        }
        return (int) d2;
    }

    private double cT() {
        Random random = new Random();
        random.setSeed(System.currentTimeMillis());
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = this.kW.getRunningAppProcesses();
        double d = 3.0E-5d;
        for (int i = 0; i < runningAppProcesses.size(); i++) {
            d += ((double) ((random.nextInt() % 3) + 1)) * 5.0E-6d;
        }
        return d;
    }
}
