package com.dianxinos.powermanager.d;

import android.content.Context;
import android.os.SystemClock;
import com.dianxinos.powermanager.c.a;
import com.dianxinos.powermanager.c.e;
import com.dianxinos.powermanager.c.i;

/* compiled from: BatteryStatsSimpleAnalyzer */
public class d implements com.dianxinos.powermanager.c.d {
    private static d ee;
    private a dY;
    private boolean dZ = false;
    private boolean ea = false;
    private int eb;
    private long ec;
    private int ed = -1;
    private Context mContext;

    public static d s(Context context) {
        synchronized (e.class) {
            if (ee == null) {
                ee = new d(context);
            }
        }
        return ee;
    }

    private d(Context context) {
        this.mContext = context.getApplicationContext();
        this.dY = a.c(context);
    }

    public void B() {
        e.d("BatteryStatsSimpleAnalyzer", "Start to work");
        a.b(this.mContext).a(this);
    }

    public void C() {
        e.d("BatteryStatsSimpleAnalyzer", "Stop to work");
        a.b(this.mContext).b(this);
    }

    public void d(i iVar) {
        if (iVar.status == 2) {
            this.ea = false;
            this.dZ = false;
            this.ed = iVar.level;
            return;
        }
        if (!this.dZ) {
            if (iVar.level >= this.ed) {
                this.ed = iVar.level;
                return;
            }
            this.dZ = true;
        }
        if (!this.ea) {
            this.ea = true;
            this.eb = (iVar.level * 100) / iVar.gP;
            this.ec = SystemClock.elapsedRealtime() / 1000;
            return;
        }
        int i = (iVar.level * 100) / iVar.gP;
        long elapsedRealtime = SystemClock.elapsedRealtime() / 1000;
        this.dY.a(this.eb - i, elapsedRealtime - this.ec);
        this.eb = i;
        this.ec = elapsedRealtime;
    }
}
