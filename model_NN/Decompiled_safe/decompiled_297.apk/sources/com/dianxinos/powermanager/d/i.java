package com.dianxinos.powermanager.d;

import android.util.SparseArray;
import com.dianxinos.powermanager.c.e;

/* compiled from: BatteryStatsSnap */
public class i {
    private SparseArray lA = new SparseArray();
    private b lB = null;
    public long lC;
    public long lD;

    public b af(int i) {
        if (i < 10000 || i > 99999) {
            if (this.lB == null) {
                this.lB = new b(0);
                this.lA.put(0, this.lB);
            }
            return this.lB;
        }
        b bVar = new b(i);
        this.lA.put(i, bVar);
        return bVar;
    }

    public SparseArray cU() {
        return this.lA;
    }

    public void ag(int i) {
        int size = this.lA.size();
        double d = 0.0d;
        for (int i2 = 0; i2 < size; i2++) {
            d += ((b) this.lA.valueAt(i2)).bq;
        }
        if (d != 0.0d) {
            for (int i3 = 0; i3 < size; i3++) {
                b bVar = (b) this.lA.valueAt(i3);
                long j = bVar.bp > bVar.bm ? bVar.bp : bVar.bm;
                e.d("BatteryStatsSnap", "time: " + j);
                if (j != 0) {
                    bVar.bs = (((((double) i) * bVar.bq) / d) / ((double) j)) * 1000.0d;
                    e.d("BatteryStatsSnap", "pps: " + bVar.bs + ", power: " + bVar.bq);
                }
            }
        }
    }

    public void cV() {
        int size = this.lA.size();
        e.d("BatteryStatsSnap", "dump snap diff, size: " + size);
        for (int i = 0; i < size; i++) {
            b bVar = (b) this.lA.valueAt(i);
            e.d("BatteryStatsSnap", "uid: " + bVar.mUid + ", app: " + bVar.bl + ", pps: " + bVar.bs);
        }
    }
}
