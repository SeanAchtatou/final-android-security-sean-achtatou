package com.dianxinos.powermanager.d;

import android.content.Context;
import com.dianxinos.powermanager.c.b;

/* compiled from: GlobalPpsMgr */
public class a {
    private static a bc;
    private double aX;
    private long aY;
    private long aZ;
    private int ba;
    private b bb;

    public static a c(Context context) {
        synchronized (a.class) {
            if (bc == null) {
                bc = new a(context.getApplicationContext());
            }
        }
        return bc;
    }

    private a(Context context) {
        this.bb = new b(context);
        long[] F = this.bb.F();
        this.aY = F[0];
        this.aZ = F[1];
        if (this.aZ > 0) {
            this.aX = ((double) this.aY) / ((double) this.aZ);
        }
    }

    public void a(int i, long j) {
        this.aY += (long) i;
        this.aZ += j;
        this.aX = ((double) this.aY) / ((double) this.aZ);
        this.ba += i;
        if (this.ba > 10 || this.aY < 20) {
            this.ba = 0;
            this.bb.a(this.aY, this.aZ);
        }
    }

    public boolean I() {
        return this.aY >= 4;
    }

    public double J() {
        return this.aX;
    }
}
