package com.dianxinos.powermanager.d;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.BatteryStats;
import android.os.Parcel;
import android.os.RemoteException;
import android.os.SystemClock;
import android.util.SparseArray;
import com.android.internal.app.IBatteryStats;
import com.android.internal.os.BatteryStatsImpl;
import com.android.internal.os.PowerProfile;
import com.dianxinos.powermanager.c.d;
import com.dianxinos.powermanager.c.i;
import java.util.Iterator;
import java.util.Map;

/* compiled from: BatteryStatsAnalyzer */
public class e implements d {
    private boolean ea;
    private int eb;
    private long ec;
    private int hj;
    private PowerProfile hk;
    private IBatteryStats hl;
    private BatteryStatsImpl hm;
    private BatteryStatsImpl hn;
    private boolean ho;
    private boolean hp;
    private int hq;
    private long hr;
    private int hs;
    private Context mContext;

    public void d(i iVar) {
        boolean z = iVar.status == 2;
        if (!this.hp) {
            if (!this.ea && !z) {
                this.hm = bu();
                this.hn = null;
                this.ea = this.hm != null;
                this.ho = false;
                this.hp = false;
                this.eb = (iVar.level * 100) / iVar.gP;
                this.hq = -1;
                this.hs = 0;
                if (this.hm != null) {
                    this.ec = this.hm.computeBatteryRealtime(SystemClock.elapsedRealtime() * 1000, this.hj);
                }
            } else if (this.ea && z) {
                this.hs++;
                if (this.hq == -1) {
                    this.hq = (iVar.level * 100) / iVar.gP;
                }
                if (this.eb - this.hq < 1) {
                    this.ea = false;
                    this.hm = null;
                } else {
                    this.hn = bu();
                    if (this.hn != null) {
                        this.ea = false;
                        this.ho = true;
                        this.hr = this.hn.computeBatteryRealtime(SystemClock.elapsedRealtime() * 1000, this.hj);
                    }
                }
            } else if (this.ho && z) {
                this.hs++;
            }
            if (this.ho && this.hs >= 0) {
                this.ho = false;
                this.hp = true;
                a(this.hm, this.ec, this.hn, this.hr, this.eb - this.hq);
            }
        }
    }

    private BatteryStatsImpl bu() {
        BatteryStatsImpl batteryStatsImpl = null;
        try {
            byte[] statistics = this.hl.getStatistics();
            Parcel obtain = Parcel.obtain();
            obtain.unmarshall(statistics, 0, statistics.length);
            obtain.setDataPosition(0);
            BatteryStatsImpl batteryStatsImpl2 = (BatteryStatsImpl) BatteryStatsImpl.CREATOR.createFromParcel(obtain);
            try {
                batteryStatsImpl2.distributeWorkLocked(this.hj);
                obtain.recycle();
                return batteryStatsImpl2;
            } catch (RemoteException e) {
                RemoteException remoteException = e;
                batteryStatsImpl = batteryStatsImpl2;
                e = remoteException;
            }
        } catch (RemoteException e2) {
            e = e2;
            com.dianxinos.powermanager.c.e.f("BatteryStatsAnalyzer", "RemoteException:" + e);
            return batteryStatsImpl;
        }
    }

    private void a(BatteryStatsImpl batteryStatsImpl, long j, BatteryStatsImpl batteryStatsImpl2, long j2, int i) {
        if (batteryStatsImpl == null || batteryStatsImpl2 == null || i < 1) {
            com.dianxinos.powermanager.c.e.f("BatteryStatsAnalyzer", "old: " + batteryStatsImpl + ", new: " + batteryStatsImpl2 + ", gap: " + i);
            return;
        }
        a(batteryStatsImpl, j);
        i a = a(batteryStatsImpl2, j2);
        a.ag(i);
        a.cV();
        this.hp = false;
    }

    private i a(BatteryStatsImpl batteryStatsImpl, long j) {
        long j2;
        String str;
        double d;
        long j3;
        double d2;
        long j4;
        double d3;
        int i;
        double d4;
        String str2;
        SensorManager sensorManager = (SensorManager) this.mContext.getSystemService("sensor");
        int i2 = this.hj;
        int numSpeedSteps = this.hk.getNumSpeedSteps();
        double[] dArr = new double[numSpeedSteps];
        for (int i3 = 0; i3 < numSpeedSteps; i3++) {
            dArr[i3] = this.hk.getAveragePower("cpu.active", i3);
        }
        double averagePower = this.hk.getAveragePower("cpu.awake");
        double averagePower2 = this.hk.getAveragePower("wifi.on");
        double averagePower3 = this.hk.getAveragePower("gps.on");
        double averagePower4 = this.hk.getAveragePower("dsp.audio");
        double averagePower5 = this.hk.getAveragePower("dsp.video");
        i iVar = new i();
        SparseArray uidStats = batteryStatsImpl.getUidStats();
        int size = uidStats.size();
        int i4 = 0;
        while (true) {
            int i5 = i4;
            if (i5 >= size) {
                break;
            }
            BatteryStats.Uid uid = (BatteryStats.Uid) uidStats.valueAt(i5);
            int uid2 = uid.getUid();
            Map processStats = uid.getProcessStats();
            if (processStats.size() != 0) {
                b af = iVar.af(uid2);
                long j5 = 0;
                if (processStats.size() > 0) {
                    double d5 = 0.0d;
                    double d6 = 0.0d;
                    String str3 = null;
                    long j6 = 0;
                    long j7 = 0;
                    for (Map.Entry entry : processStats.entrySet()) {
                        String str4 = (String) entry.getKey();
                        BatteryStats.Uid.Proc proc = (BatteryStats.Uid.Proc) entry.getValue();
                        long userTime = (proc.getUserTime(i2) + proc.getSystemTime(i2)) * 10;
                        j7 += userTime;
                        j6 += proc.getForegroundTime(i2) * 10;
                        long[] jArr = new long[numSpeedSteps];
                        int i6 = 0;
                        for (int i7 = 0; i7 < numSpeedSteps; i7++) {
                            jArr[i7] = proc.getTimeAtCpuSpeedStep(i7, i2);
                            i6 = (int) (((long) i6) + jArr[i7]);
                        }
                        if (i6 == 0) {
                            i = 1;
                        } else {
                            i = i6;
                        }
                        double d7 = 0.0d;
                        for (int i8 = 0; i8 < numSpeedSteps; i8++) {
                            d7 += (((double) jArr[i8]) / ((double) i)) * ((double) userTime) * dArr[i8];
                        }
                        d6 += d7;
                        if (str3 == null || str3.startsWith("*")) {
                            str2 = str4;
                            d4 = d7;
                        } else if (d5 >= d7 || str4.startsWith("*")) {
                            str2 = str3;
                            d4 = d5;
                        } else {
                            str2 = str4;
                            d4 = d7;
                        }
                        d5 = d4;
                        str3 = str2;
                    }
                    d = d6;
                    j2 = j6;
                    j5 = j7;
                    str = str3;
                } else {
                    j2 = 0;
                    str = null;
                    d = 0.0d;
                }
                if (j2 > j5) {
                    j5 = j2;
                }
                af.bl = str;
                double d8 = d / 1000.0d;
                af.bq += d8;
                af.bm = j5;
                af.bn = j2;
                long j8 = j5 - j2;
                af.bo = j8;
                if (j5 > 0) {
                    af.br = ((d8 * ((double) j8)) / ((double) j5)) + af.br;
                }
                long j9 = 0;
                Iterator it = uid.getWakelockStats().entrySet().iterator();
                while (true) {
                    j3 = j9;
                    if (!it.hasNext()) {
                        break;
                    }
                    BatteryStats.Timer wakeTime = ((BatteryStats.Uid.Wakelock) ((Map.Entry) it.next()).getValue()).getWakeTime(0);
                    if (wakeTime != null) {
                        j9 = wakeTime.getTotalTimeLocked(j, i2) + j3;
                    } else {
                        j9 = j3;
                    }
                }
                double d9 = (((double) (j3 / 1000)) * averagePower) / 1000.0d;
                af.bq += d9;
                af.br = d9 + af.br;
                iVar.lC += j2;
                iVar.lD += j5;
                af.bq = ((((double) (uid.getWifiRunningTime(j, i2) / 1000)) * averagePower2) / 1000.0d) + af.bq;
                double d10 = 0.0d;
                double d11 = 0.0d;
                long j10 = 0;
                for (Map.Entry value : uid.getSensorStats().entrySet()) {
                    BatteryStats.Uid.Sensor sensor = (BatteryStats.Uid.Sensor) value.getValue();
                    int handle = sensor.getHandle();
                    long totalTimeLocked = sensor.getSensorTime().getTotalTimeLocked(j, i2) / 1000;
                    if (handle == -10000) {
                        double d12 = d11;
                        j4 = j10;
                        d3 = (((double) totalTimeLocked) * averagePower3) / 1000.0d;
                        d2 = d12;
                    } else {
                        Sensor defaultSensor = sensorManager.getDefaultSensor(handle);
                        if (defaultSensor != null) {
                            d2 = ((((double) defaultSensor.getPower()) * ((double) totalTimeLocked)) / 1000.0d) + d11;
                            j4 = j10 + totalTimeLocked;
                            d3 = d10;
                        } else {
                            d2 = d11;
                            j4 = j10;
                            d3 = d10;
                        }
                    }
                    d10 = d3;
                    j10 = j4;
                    d11 = d2;
                }
                af.bq += d10;
                af.bq += d11;
                af.bq = ((((double) uid.getAudioTurnedOnTime(j, i2)) * averagePower4) / 1000.0d) + af.bq;
                af.bq = ((((double) uid.getVideoTurnedOnTime(j, i2)) * averagePower5) / 1000.0d) + af.bq;
            }
            i4 = i5 + 1;
        }
        double averagePower6 = this.hk.getAveragePower("screen.on");
        long screenOnTime = batteryStatsImpl.getScreenOnTime(j, this.hj) / 1000;
        double d13 = ((double) screenOnTime) * averagePower6;
        double averagePower7 = this.hk.getAveragePower("screen.full");
        long j11 = 0;
        double d14 = 0.0d;
        for (int i9 = 0; i9 < 5; i9++) {
            long screenBrightnessTime = batteryStatsImpl.getScreenBrightnessTime(i9, j, this.hj) / 1000;
            d14 += ((((double) (i9 + 1)) * averagePower7) / 5.0d) * ((double) screenBrightnessTime);
            j11 += screenBrightnessTime;
        }
        double d15 = d14 / ((double) j11);
        double d16 = d13 + d14;
        long j12 = iVar.lC;
        if (j12 > 0) {
            SparseArray cU = iVar.cU();
            int size2 = cU.size();
            int i10 = 0;
            while (true) {
                int i11 = i10;
                if (i11 >= size2) {
                    break;
                }
                b bVar = (b) cU.valueAt(i11);
                double d17 = ((double) bVar.bn) / ((double) j12);
                double d18 = ((double) screenOnTime) * d17;
                bVar.bq = ((((d17 * ((double) j11)) * d15) + (d18 * averagePower6)) / 1000.0d) + bVar.bq;
                bVar.bp = (long) d18;
                i10 = i11 + 1;
            }
        }
        b af2 = iVar.af(0);
        double averagePower8 = this.hk.getAveragePower("radio.active");
        af2.bq = ((((double) (batteryStatsImpl.getPhoneOnTime(j, this.hj) / 1000)) * averagePower8) / 1000.0d) + af2.bq;
        af2.bq = ((averagePower8 * ((double) batteryStatsImpl.getRadioDataUptime())) / 1000.0d) + af2.bq;
        long j13 = 0;
        double d19 = 0.0d;
        for (int i12 = 0; i12 < 5; i12++) {
            double averagePower9 = this.hk.getAveragePower("radio.on", i12);
            long phoneSignalStrengthTime = batteryStatsImpl.getPhoneSignalStrengthTime(i12, j, this.hj) / 1000;
            d19 += (averagePower9 * ((double) phoneSignalStrengthTime)) / 1000.0d;
            j13 += phoneSignalStrengthTime;
        }
        af2.bq = ((this.hk.getAveragePower("radio.scanning") * ((double) (batteryStatsImpl.getPhoneSignalScanningTime(j, this.hj) / 1000))) / 1000.0d) + d19 + af2.bq;
        af2.bq = (((((double) ((batteryStatsImpl.getWifiOnTime(j, this.hj) / 1000) * 0)) * this.hk.getAveragePower("wifi.on")) + (((double) (batteryStatsImpl.getGlobalWifiRunningTime(j, this.hj) / 1000)) * this.hk.getAveragePower("wifi.on"))) / 1000.0d) + af2.bq;
        af2.bq = ((this.hk.getAveragePower("cpu.idle") * ((double) ((j - screenOnTime) / 1000))) / 1000.0d) + af2.bq;
        af2.bq = ((this.hk.getAveragePower("bluetooth.on") * ((double) (batteryStatsImpl.getBluetoothOnTime(j, this.hj) / 1000))) / 1000.0d) + ((this.hk.getAveragePower("bluetooth.at") * ((double) batteryStatsImpl.getBluetoothPingCount())) / 1000.0d) + af2.bq;
        return iVar;
    }
}
