package com.dianxinos.powermanager.d;

import android.content.ContentResolver;
import android.content.Context;
import android.os.Build;
import android.provider.Settings;
import com.dianxinos.powermanager.c.b;
import com.dianxinos.powermanager.c.e;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/* compiled from: ChargingTimeHelper */
public class f {
    private static final String an = Build.DEVICE;
    private static f ij;
    private b bb;
    private boolean ea = false;
    private double ia;
    private double ib;
    private double ic;
    private double id;
    private Context ie;

    /* renamed from: if  reason: not valid java name */
    private int f1if = 0;
    private int ig = 100;
    private int ih = -1;
    private long ii = 0;
    private ContentResolver mContentResolver;

    public static f G(Context context) {
        synchronized (f.class) {
            if (ij == null) {
                ij = new f(context);
            }
        }
        return ij;
    }

    private f(Context context) {
        this.ie = context.getApplicationContext();
        this.mContentResolver = this.ie.getContentResolver();
        if (!bL()) {
            bN();
        }
        this.bb = new b(context);
    }

    public void a(int i, int i2, int i3, long j) {
        boolean z;
        double d;
        if (i != 0) {
            if (Settings.System.getInt(this.mContentResolver, "stay_on_while_plugged_in", 0) != 0) {
                z = true;
            } else {
                z = false;
            }
            long[] a = this.bb.a(i, z);
            long j2 = ((long) (i2 - this.ih)) + a[0];
            long j3 = (j - this.ii) + (a[1] * 1000);
            if (j2 == 0) {
                d = 0.0d;
            } else {
                d = ((double) j3) / ((double) j2);
            }
            if (this.ih != -1 && i2 > this.ih) {
                if (this.ea) {
                    if (i2 < i3) {
                        this.bb.a(i, z, j2, j3 / 1000);
                        e.d("ChargingTimeHelper", "levelGap: " + j2 + ", timeGap: " + (j3 / 1000));
                    }
                    if (i == 2) {
                        if (z) {
                            this.ia = d;
                            double d2 = this.ia;
                        } else {
                            this.ib = d;
                            double d3 = this.ib;
                        }
                    } else if (this.f1if == 1) {
                        if (z) {
                            this.ic = d;
                            double d4 = this.ic;
                        } else {
                            this.id = d;
                            double d5 = this.id;
                        }
                    }
                }
                this.ea = true;
            }
            this.f1if = i;
            this.ig = i3;
            this.ih = i2;
            if (j2 > 0) {
                this.ii = j;
            }
        }
    }

    public void bJ() {
        bM();
        this.f1if = 0;
        this.ih = -1;
        this.ii = 0;
    }

    public long bK() {
        double d;
        boolean z = Settings.System.getInt(this.mContentResolver, "stay_on_while_plugged_in", 0) != 0;
        if (this.f1if == 2) {
            if (z) {
                d = this.ia;
            } else {
                d = this.ib;
            }
        } else if (this.f1if != 1) {
            return -1;
        } else {
            if (z) {
                d = this.ic;
            } else {
                d = this.id;
            }
        }
        long j = (long) (((double) (this.ig - this.ih)) * d);
        if (j >= 0) {
            return j;
        }
        e.f("ChargingTimeHelper", "Bad state, mScale: " + this.ig + ", mLevel: " + this.ih + ", rate: " + d + ", plug: " + this.f1if);
        return -1;
    }

    private boolean bL() {
        e.d("ChargingTimeHelper", "loading values from local");
        try {
            DataInputStream dataInputStream = new DataInputStream(this.ie.openFileInput("charging_time_local"));
            double readDouble = dataInputStream.readDouble();
            double readDouble2 = dataInputStream.readDouble();
            double readDouble3 = dataInputStream.readDouble();
            double readDouble4 = dataInputStream.readDouble();
            e.d("ChargingTimeHelper", "loaded values, usbOn: " + readDouble + ", usbOff: " + readDouble2 + ", acOn: " + readDouble3 + ", acOff: " + readDouble4);
            this.ia = readDouble;
            this.ib = readDouble2;
            this.ic = readDouble3;
            this.id = readDouble4;
            return true;
        } catch (FileNotFoundException e) {
            e.c("ChargingTimeHelper", "data file not found when loading values from local");
            return false;
        } catch (Exception e2) {
            e.f("ChargingTimeHelper", "failed to load values with exception: " + e2.toString());
            return false;
        }
    }

    private void bM() {
        try {
            DataOutputStream dataOutputStream = new DataOutputStream(this.ie.openFileOutput("charging_time_local", 0));
            dataOutputStream.writeDouble(this.ia);
            dataOutputStream.writeDouble(this.ib);
            dataOutputStream.writeDouble(this.ic);
            dataOutputStream.writeDouble(this.id);
            dataOutputStream.close();
        } catch (Exception e) {
            e.f("ChargingTimeHelper", "Failed to write data with exception: " + e.toString());
        }
    }

    private void bN() {
        NodeList nodeList;
        e.d("ChargingTimeHelper", "reading values for " + an);
        Document document = null;
        try {
            document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(this.ie.getAssets().open("charging_profile.xml"));
        } catch (Exception e) {
            e.f("ChargingTimeHelper", "failed to parse with exception: " + e.toString());
        }
        Element documentElement = document.getDocumentElement();
        NodeList elementsByTagName = documentElement.getElementsByTagName(an);
        if (elementsByTagName == null || elementsByTagName.getLength() == 0) {
            nodeList = documentElement.getElementsByTagName("default_device");
            if (nodeList == null || nodeList.getLength() == 0) {
                e.f("ChargingTimeHelper", "Implement error! No default values found!");
                return;
            }
        } else {
            nodeList = elementsByTagName;
        }
        NodeList elementsByTagName2 = ((Element) nodeList.item(0)).getElementsByTagName("item");
        for (int i = 0; i < elementsByTagName2.getLength(); i++) {
            Element element = (Element) elementsByTagName2.item(i);
            String attribute = element.getAttribute("plug");
            String attribute2 = element.getAttribute("screen");
            String attribute3 = element.getAttribute("rate");
            try {
                double parseDouble = Double.parseDouble(attribute3);
                if ("ac".equals(attribute)) {
                    if ("on".equals(attribute2)) {
                        this.ic = parseDouble;
                    } else if ("off".equals(attribute2)) {
                        this.id = parseDouble;
                    } else {
                        e.f("ChargingTimeHelper", "unknown screen attribute value: " + attribute2);
                    }
                } else if (!"usb".equals(attribute)) {
                    e.f("ChargingTimeHelper", "unknown plug attribute value: " + attribute);
                } else if ("on".equals(attribute2)) {
                    this.ia = parseDouble;
                } else if ("off".equals(attribute2)) {
                    this.ib = parseDouble;
                } else {
                    e.f("ChargingTimeHelper", "unknown screen attribute value: " + attribute2);
                }
            } catch (Exception e2) {
                e.f("ChargingTimeHelper", "failed to parse rate attr: " + attribute3);
            }
        }
    }
}
