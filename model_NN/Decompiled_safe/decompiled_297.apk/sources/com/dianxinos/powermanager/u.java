package com.dianxinos.powermanager;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;

/* compiled from: ShowDialog */
public class u extends Dialog {
    LinearLayout bA = ((LinearLayout) findViewById(C0000R.id.statusitems));
    Context mContext;

    public u(Context context) {
        super(context, C0000R.style.ShowDialogStyle);
        this.mContext = context;
        setContentView((int) C0000R.layout.mode_status_show);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    public void a(View view) {
        this.bA.addView(view);
    }

    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        dismiss();
        return super.dispatchTouchEvent(motionEvent);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        dismiss();
        return true;
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        dismiss();
        return true;
    }
}
