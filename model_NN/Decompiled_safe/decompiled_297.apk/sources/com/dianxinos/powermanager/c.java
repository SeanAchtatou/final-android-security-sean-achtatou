package com.dianxinos.powermanager;

import android.database.ContentObserver;
import android.os.Handler;
import android.provider.Settings;
import com.dianxinos.powermanager.c.e;

/* compiled from: PowerMgrService */
class c extends ContentObserver {
    final /* synthetic */ PowerMgrService bB;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public c(PowerMgrService powerMgrService, Handler handler) {
        super(handler);
        this.bB = powerMgrService;
    }

    public void k() {
        this.bB.mResolver.registerContentObserver(Settings.System.getUriFor("com.dianxinos.powermanager.statusbar_notification"), false, this);
    }

    public void l() {
        this.bB.mResolver.unregisterContentObserver(this);
    }

    public void onChange(boolean z) {
        int unused = this.bB.eq = Settings.System.getInt(this.bB.mResolver, "com.dianxinos.powermanager.statusbar_notification", 1);
        e.d("PowerMgrService", "onChange mStatusBar: " + this.bB.eq);
        if (this.bB.eq == 0) {
            this.bB.em.an();
        } else {
            this.bB.em.a(this.bB.aG, this.bB.az, this.bB.aG ? this.bB.aL : this.bB.aM, this.bB.ab.az(), null);
        }
    }
}
