package com.dianxinos.powermanager;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.dianxinos.powermanager.b.d;
import com.dianxinos.powermanager.b.f;
import com.dianxinos.powermanager.b.i;
import com.dianxinos.powermanager.b.s;
import com.dianxinos.powermanager.b.t;
import java.util.ArrayList;

/* compiled from: RecentAppsListAdapter */
public class k extends BaseAdapter {
    private e eB;
    private com.dianxinos.powermanager.b.k eC;
    f eD;
    private double eE;
    private boolean eF = false;
    View.OnClickListener eG = new m(this);
    t eu;
    /* access modifiers changed from: private */
    public Context mContext;
    private LayoutInflater mInflater;
    private int o;
    private ArrayList p;

    public k(Context context, e eVar) {
        this.mContext = context;
        this.eB = eVar;
        this.mInflater = LayoutInflater.from(this.mContext);
        this.eC = com.dianxinos.powermanager.b.k.H(this.mContext);
        this.eu = t.M(this.mContext);
    }

    public void a(f fVar, boolean z) {
        double d;
        this.eD = fVar;
        this.eF = z;
        if (z) {
            this.p = this.eD.gb;
        } else {
            this.p = this.eD.hH;
        }
        this.o = this.p.size();
        if (z) {
            if (this.p.size() > 0) {
                d = ((d) this.p.get(0)).dR;
            } else {
                d = 1.0d;
            }
            this.eE = d;
        } else {
            this.eE = this.p.size() > 0 ? ((i) this.p.get(0)).hw : 1.0d;
        }
        notifyDataSetChanged();
    }

    public int getCount() {
        return this.o;
    }

    public Object getItem(int i) {
        return null;
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        o oVar;
        View view2;
        if (view == null) {
            View inflate = this.mInflater.inflate((int) C0000R.layout.recent_apps_list_item, (ViewGroup) null);
            o oVar2 = new o();
            oVar2.gY = (ImageView) inflate.findViewById(C0000R.id.icon);
            oVar2.gZ = (TextView) inflate.findViewById(C0000R.id.label);
            oVar2.ha = (ImageView) inflate.findViewById(C0000R.id.progress_image);
            oVar2.hb = (TextView) inflate.findViewById(C0000R.id.progress);
            oVar2.hc = (ImageView) inflate.findViewById(C0000R.id.close);
            oVar2.hc.setOnClickListener(this.eG);
            inflate.setTag(oVar2);
            o oVar3 = oVar2;
            view2 = inflate;
            oVar = oVar3;
        } else {
            oVar = (o) view.getTag();
            view2 = view;
        }
        d dVar = (d) this.p.get(i);
        double d = dVar.hw;
        if (this.eF) {
            d = dVar.dR;
        }
        s d2 = this.eC.d(dVar.mUid, dVar.bl);
        oVar.gY.setImageDrawable(d2.icon);
        oVar.gZ.setText(d2.label);
        oVar.hb.setText(String.format("%.1f%%", Double.valueOf(d)));
        Drawable drawable = this.mContext.getResources().getDrawable(C0000R.drawable.list_item_progress_bar);
        Drawable drawable2 = this.mContext.getResources().getDrawable(C0000R.drawable.list_item_progress_bkg);
        double d3 = d / this.eE;
        oVar.ha.setImageDrawable(new com.dianxinos.powermanager.ui.d(drawable, drawable2, d3));
        oVar.hd = d3;
        oVar.hc.setTag(Integer.valueOf(i));
        return view2;
    }

    /* access modifiers changed from: private */
    public void w(int i) {
        d dVar = (d) this.p.get(i);
        this.eu.e(dVar.mUid, dVar.bl);
        this.eD.f(i, this.eF);
        this.eD.E(this.mContext);
        this.eD.a(this.mContext, 20, 0.1d);
        this.o = this.p.size();
        s d = this.eC.d(dVar.mUid, dVar.bl);
        if (d.kb != null) {
            l.a(this.mContext, d.kb);
        }
        notifyDataSetChanged();
        this.eB.o(this.o);
        Toast.makeText(this.mContext, this.mContext.getString(C0000R.string.recent_apps_toast_close_app, d.label), 0).show();
    }
}
