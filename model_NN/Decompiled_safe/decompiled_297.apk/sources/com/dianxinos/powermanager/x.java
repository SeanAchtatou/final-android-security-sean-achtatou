package com.dianxinos.powermanager;

import android.database.ContentObserver;
import android.os.Handler;
import android.provider.Settings;
import com.dianxinos.powermanager.c.e;

/* compiled from: PowerMgrService */
class x extends ContentObserver {
    final /* synthetic */ PowerMgrService bB;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public x(PowerMgrService powerMgrService, Handler handler) {
        super(handler);
        this.bB = powerMgrService;
    }

    public void k() {
        this.bB.mResolver.registerContentObserver(Settings.System.getUriFor("com.dianxinos.powermanager.auto_cleanup"), false, this);
    }

    public void l() {
        this.bB.mResolver.unregisterContentObserver(this);
    }

    public void onChange(boolean z) {
        int unused = this.bB.ep = Settings.System.getInt(this.bB.mResolver, "com.dianxinos.powermanager.auto_cleanup", 0);
        e.d("PowerMgrService", "onChange mCleanup: " + this.bB.ep);
        if (this.bB.ep != 0) {
            int unused2 = this.bB.aM;
            this.bB.er.d(400, false);
        }
    }
}
