package com.dianxinos.powermanager;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.dianxinos.powermanager.b.e;
import com.dianxinos.powermanager.b.f;
import com.dianxinos.powermanager.b.o;
import com.dianxinos.powermanager.b.v;
import com.dianxinos.powermanager.c.c;
import com.dianxinos.powermanager.ui.b;

public class AppsPowerUsageHistory extends Activity implements View.OnClickListener, AdapterView.OnItemClickListener, v, b {
    private o C;
    private w E;
    private ListView F;
    private View G;
    private long fB;
    private int iv = 1;
    private f jY;
    private Button jZ;
    private d ka = new d(this);

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.power_usage_summary);
        this.E = new w(this);
        this.F = (ListView) findViewById(C0000R.id.list);
        this.F.setAdapter((ListAdapter) this.E);
        this.F.setEmptyView(findViewById(C0000R.id.empty));
        this.F.setOnItemClickListener(this);
        this.C = o.K(this);
        this.C.a(this);
        n();
        this.jZ = (Button) findViewById(C0000R.id.sort_by);
        this.jZ.setOnClickListener(this);
        this.jZ.setVisibility(0);
        this.G = findViewById(C0000R.id.summary_icon);
        this.G.setOnClickListener(this);
        o.K(this).cl();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.C.cm();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.C.b(this);
    }

    /* access modifiers changed from: private */
    public void n() {
        e ck = this.C.ck();
        if (ck == null) {
            this.C.cl();
            return;
        }
        this.jY = ck.eD;
        this.fB = ck.fB;
        bP();
    }

    private void bP() {
        if (this.iv == 1) {
            this.E.a(this.jY.hH);
            this.E.R(this.iv);
            int i = (int) (this.jY.bq / 3600.0d);
        } else if (this.iv == 2) {
            this.E.a(this.jY.gb);
            this.E.R(this.iv);
            int i2 = (int) (this.jY.gc / 3600.0d);
        } else {
            com.dianxinos.powermanager.c.e.f("AppsPowerUsageSummary", "Unknown sort type: " + this.iv);
        }
        ((TextView) findViewById(C0000R.id.summary_descption)).setText(getString(C0000R.string.apps_summary_descption_2, new Object[]{c.b(this, (int) (this.fB / 1000))}));
    }

    public void onItemClick(AdapterView adapterView, View view, int i, long j) {
        Intent intent = new Intent(this, AppPowerUsageDetails.class);
        intent.putExtra("position", i);
        intent.putExtra("bar_percent", ((z) view.getTag()).hd);
        intent.putExtra("bg", this.iv == 2);
        startActivity(intent);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void onClick(View view) {
        if (view == this.jZ) {
            Activity parent = getParent();
            if (parent == null) {
                parent = this;
            }
            com.dianxinos.powermanager.ui.e eVar = new com.dianxinos.powermanager.ui.e(parent);
            eVar.setTitle((int) C0000R.string.apps_power_usage_history);
            eVar.k(1, C0000R.string.apps_summary_sort_by_total);
            eVar.k(2, C0000R.string.apps_summary_sort_by_bg);
            eVar.a(this);
            eVar.U(this.iv);
            eVar.show();
        } else if (view == this.G && this.jY != null) {
            Intent intent = new Intent(this, PieChartActivity.class);
            intent.putExtra("apps", true);
            intent.putExtra("bg", this.iv == 2);
            startActivity(intent);
        }
    }

    public void o() {
        this.ka.sendEmptyMessage(1);
    }

    public void l(int i) {
        if (this.iv != i) {
            this.iv = i;
            if (i == 1) {
                this.jZ.setText((int) C0000R.string.apps_summary_sort_by_total);
            } else if (i == 2) {
                this.jZ.setText((int) C0000R.string.apps_summary_sort_by_bg);
            }
            bP();
        }
    }
}
