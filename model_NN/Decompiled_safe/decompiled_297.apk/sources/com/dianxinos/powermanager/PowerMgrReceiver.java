package com.dianxinos.powermanager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.dianxinos.powermanager.c.e;

public class PowerMgrReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        e.d("PowerMgrReceiver", "received: " + intent);
        if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction())) {
            context.startService(new Intent(context, PowerMgrService.class));
        }
    }
}
