package com.dianxinos.powermanager;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import com.dianxinos.powermanager.c.e;
import com.dianxinos.powermanager.ui.PieChart;

public class PieChartActivity extends Activity {
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.power_usage_pie_chart);
        try {
            init();
        } catch (Exception e) {
            finish();
            e.e("PieChartActivity", "Process killed??? Exception: " + e);
        }
    }

    private void init() {
        Intent intent = getIntent();
        a((PieChart) findViewById(C0000R.id.pie_chart), intent.getBooleanExtra("apps", true), intent.getBooleanExtra("recent", false), intent.getBooleanExtra("bg", false));
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:0x009b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(com.dianxinos.powermanager.ui.PieChart r13, boolean r14, boolean r15, boolean r16) {
        /*
            r12 = this;
            if (r14 == 0) goto L_0x0036
            if (r15 == 0) goto L_0x0028
            com.dianxinos.powermanager.b.t r1 = com.dianxinos.powermanager.b.t.M(r12)
            com.dianxinos.powermanager.b.f r1 = r1.cv()
        L_0x000c:
            if (r16 == 0) goto L_0x0033
            java.util.ArrayList r1 = r1.gb
        L_0x0010:
            r2 = r1
        L_0x0011:
            if (r2 == 0) goto L_0x0019
            int r1 = r2.size()
            if (r1 != 0) goto L_0x0044
        L_0x0019:
            r1 = 2131230786(0x7f080042, float:1.8077635E38)
            r2 = 0
            android.widget.Toast r1 = android.widget.Toast.makeText(r12, r1, r2)
            r1.show()
            r12.finish()
        L_0x0027:
            return
        L_0x0028:
            com.dianxinos.powermanager.b.o r1 = com.dianxinos.powermanager.b.o.K(r12)
            com.dianxinos.powermanager.b.e r1 = r1.ck()
            com.dianxinos.powermanager.b.f r1 = r1.eD
            goto L_0x000c
        L_0x0033:
            java.util.ArrayList r1 = r1.hH
            goto L_0x0010
        L_0x0036:
            com.dianxinos.powermanager.b.o r1 = com.dianxinos.powermanager.b.o.K(r12)
            com.dianxinos.powermanager.b.e r1 = r1.ck()
            com.dianxinos.powermanager.b.i r1 = r1.fC
            java.util.ArrayList r1 = r1.hH
            r2 = r1
            goto L_0x0011
        L_0x0044:
            com.dianxinos.powermanager.b.k r3 = com.dianxinos.powermanager.b.k.H(r12)
            r1 = 0
            r4 = 5
            int r5 = r2.size()
            int r4 = java.lang.Math.min(r4, r5)
            int r5 = r4 + 1
            java.lang.String[] r5 = new java.lang.String[r5]
            int r6 = r4 + 1
            int[] r6 = new int[r6]
            r7 = 0
            r8 = r1
        L_0x005c:
            if (r7 >= r4) goto L_0x0108
            java.lang.Object r15 = r2.get(r7)
            com.dianxinos.powermanager.b.i r15 = (com.dianxinos.powermanager.b.i) r15
            if (r16 == 0) goto L_0x00bd
            r0 = r15
            com.dianxinos.powermanager.b.d r0 = (com.dianxinos.powermanager.b.d) r0
            r1 = r0
            double r9 = r1.dR
        L_0x006c:
            long r9 = java.lang.Math.round(r9)
            int r1 = (int) r9
            r9 = 1
            if (r1 >= r9) goto L_0x00c0
            r1 = r7
            r3 = r8
        L_0x0076:
            if (r14 == 0) goto L_0x0106
            if (r1 <= 0) goto L_0x0106
            r4 = 1
            int r4 = r1 - r4
            java.lang.Object r15 = r2.get(r4)
            com.dianxinos.powermanager.b.d r15 = (com.dianxinos.powermanager.b.d) r15
            int r2 = r15.mUid
            r7 = -1
            if (r2 != r7) goto L_0x0106
            r2 = r6[r4]
            r7 = 100
            int r2 = r3 - r2
            int r2 = r7 - r2
            r6[r4] = r2
            r2 = 100
        L_0x0094:
            r3 = 100
            int r2 = r3 - r2
            r3 = 1
            if (r2 < r3) goto L_0x00aa
            if (r14 == 0) goto L_0x00fc
            r3 = 2131230789(0x7f080045, float:1.807764E38)
            java.lang.String r3 = r12.getString(r3)
            r5[r1] = r3
        L_0x00a6:
            r6[r1] = r2
            int r1 = r1 + 1
        L_0x00aa:
            java.lang.String[] r2 = new java.lang.String[r1]
            int[] r3 = new int[r1]
            r4 = 0
            r7 = 0
            java.lang.System.arraycopy(r5, r4, r2, r7, r1)
            r4 = 0
            r5 = 0
            java.lang.System.arraycopy(r6, r4, r3, r5, r1)
            r13.a(r2, r3)
            goto L_0x0027
        L_0x00bd:
            double r9 = r15.hw
            goto L_0x006c
        L_0x00c0:
            if (r14 == 0) goto L_0x00e8
            com.dianxinos.powermanager.b.d r15 = (com.dianxinos.powermanager.b.d) r15
            int r9 = r15.mUid
            java.lang.String r10 = r15.bl
            com.dianxinos.powermanager.b.s r9 = r3.d(r9, r10)
            java.lang.String r9 = r9.label
            r5[r7] = r9
        L_0x00d0:
            r6[r7] = r1
            int r1 = r1 + r8
            r8 = 100
            if (r1 < r8) goto L_0x00f7
            r3 = r6[r7]
            r4 = 100
            int r1 = r1 - r4
            int r1 = r3 - r1
            r6[r7] = r1
            r1 = 100
            int r3 = r7 + 1
            r11 = r3
            r3 = r1
            r1 = r11
            goto L_0x0076
        L_0x00e8:
            com.dianxinos.powermanager.b.j r15 = (com.dianxinos.powermanager.b.j) r15
            int r9 = r15.bT
            int r9 = com.dianxinos.powermanager.b.c.m(r9)
            java.lang.String r9 = r12.getString(r9)
            r5[r7] = r9
            goto L_0x00d0
        L_0x00f7:
            int r7 = r7 + 1
            r8 = r1
            goto L_0x005c
        L_0x00fc:
            r3 = 2131230790(0x7f080046, float:1.8077643E38)
            java.lang.String r3 = r12.getString(r3)
            r5[r1] = r3
            goto L_0x00a6
        L_0x0106:
            r2 = r3
            goto L_0x0094
        L_0x0108:
            r1 = r4
            r3 = r8
            goto L_0x0076
        */
        throw new UnsupportedOperationException("Method not decompiled: com.dianxinos.powermanager.PieChartActivity.a(com.dianxinos.powermanager.ui.PieChart, boolean, boolean, boolean):void");
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (isFinishing()) {
            return true;
        }
        finish();
        return true;
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (isFinishing()) {
            return true;
        }
        finish();
        return true;
    }
}
