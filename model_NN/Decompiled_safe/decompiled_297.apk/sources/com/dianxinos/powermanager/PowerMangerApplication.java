package com.dianxinos.powermanager;

import android.app.Application;
import android.content.Intent;
import android.content.res.Configuration;
import com.dianxinos.a.a.a;
import com.dianxinos.powermanager.b.k;
import com.dianxinos.powermanager.c.e;
import com.dianxinos.powermanager.mode.i;

public class PowerMangerApplication extends Application {
    private String lM;

    public void onCreate() {
        e.d("PowerMangerApplication", "app created");
        super.onCreate();
        a.F(this).L(0);
        this.lM = getResources().getConfiguration().locale.toString();
        startService(new Intent(this, PowerMgrService.class));
    }

    public void onConfigurationChanged(Configuration configuration) {
        String locale = configuration.locale.toString();
        if (!this.lM.equals(locale)) {
            e.d("PowerMangerApplication", "locale changed, oldLocale: " + this.lM + ", newLocale: " + locale);
            this.lM = locale;
            k.H(this).ao();
            i.t(this).ao();
            g.r(this).ao();
        }
    }
}
