package com.dianxinos.powermanager;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.dianxinos.a.a.a;
import com.dianxinos.powermanager.b.f;
import com.dianxinos.powermanager.b.t;
import com.dianxinos.powermanager.c.d;
import com.dianxinos.powermanager.c.e;
import com.dianxinos.powermanager.c.i;

public class AppsPowerUsageRecent extends Activity implements View.OnClickListener, AdapterView.OnItemClickListener, d, e {
    private ListView F;
    private View G;
    private a by;
    private com.dianxinos.powermanager.c.a cP;
    private boolean dd = false;
    private f eD;
    private t eu;
    /* access modifiers changed from: private */
    public boolean iA = false;
    /* access modifiers changed from: private */
    public boolean iB = false;
    private int iv = 1;
    private k iw;
    private TextView ix;
    private Button iy;
    private b iz;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.recent_apps_summary);
        this.iw = new k(this, this);
        this.F = (ListView) findViewById(C0000R.id.list);
        this.ix = (TextView) findViewById(C0000R.id.empty);
        this.F.setAdapter((ListAdapter) this.iw);
        this.F.setEmptyView(this.ix);
        this.F.setOnItemClickListener(this);
        this.iy = (Button) findViewById(C0000R.id.one_key_battery_optimizer);
        this.iy.setOnClickListener(this);
        this.G = findViewById(C0000R.id.summary_icon);
        this.G.setOnClickListener(this);
        this.cP = com.dianxinos.powermanager.c.a.b((Context) this);
        this.eu = t.M(this);
        this.iz = new b(this);
        this.by = a.F(this);
        this.by.init();
    }

    public void onResume() {
        super.onResume();
        this.cP.a(this);
        bQ();
    }

    public void onPause() {
        super.onPause();
        this.cP.b((d) this);
    }

    public void onDestroy() {
        this.by.destroy();
        super.onDestroy();
    }

    private void bP() {
        boolean z;
        this.iw.a(this.eD, this.iv == 2);
        if (this.iv == 1) {
            int i = (int) (this.eD.bq / 3600.0d);
            z = this.eD.hH.size() > 0;
        } else if (this.iv == 2) {
            int i2 = (int) (this.eD.gc / 3600.0d);
            z = this.eD.gb.size() > 0;
        } else {
            e.f("AppsPowerUsageRecent", "Unknown sort type: " + this.iv);
            z = false;
        }
        this.iy.setEnabled(z);
        this.G.setEnabled(z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void onItemClick(AdapterView adapterView, View view, int i, long j) {
        Intent intent = new Intent(this, AppPowerUsageDetails.class);
        intent.putExtra("recent", true);
        intent.putExtra("position", i);
        intent.putExtra("bar_percent", ((o) view.getTag()).hd);
        intent.putExtra("bg", this.iv == 2);
        startActivity(intent);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.dianxinos.a.a.a.a(java.lang.String, java.lang.String, java.lang.Number):boolean
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.dianxinos.a.a.a.a(java.lang.String, int, java.lang.Object):boolean
      com.dianxinos.a.a.a.a(java.lang.String, java.lang.String, java.lang.Number):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void onClick(View view) {
        boolean z;
        if (view == this.iy) {
            e.c("AppsPowerUsageRecent", " cleanup " + l.v(this).d(300, false));
            this.eu.cx();
            bP();
            this.ix.setText((int) C0000R.string.recent_apps_cleanup_no_data);
            this.iA = false;
            this.iz.sendEmptyMessageDelayed(1, 3000);
            this.by.a("recent", "clean_all", (Number) 1);
            Toast.makeText(this, (int) C0000R.string.recent_apps_toast_close_all, 0).show();
        } else if (view == this.G) {
            Intent intent = new Intent(this, PieChartActivity.class);
            intent.putExtra("recent", true);
            intent.putExtra("apps", true);
            if (this.iv == 2) {
                z = true;
            } else {
                z = false;
            }
            intent.putExtra("bg", z);
            startActivity(intent);
        }
    }

    public void d(i iVar) {
        boolean z = iVar.status == 2;
        if (this.dd != z) {
            this.dd = z;
            bQ();
        }
    }

    public void o(int i) {
        if (i == 0) {
            this.eu.cx();
            bQ();
        }
    }

    public void bQ() {
        this.iA = true;
        this.eD = this.eu.cu();
        long cw = this.eu.cw();
        int i = (int) (cw / 1000);
        if (this.dd) {
            this.ix.setText((int) C0000R.string.recent_charging_no_stats_data);
        } else if (cw >= 0) {
            this.ix.setText(getString(C0000R.string.recent_no_stats_data, new Object[]{Integer.valueOf((i + 59) / 60)}));
            if (!this.iB) {
                this.iB = true;
                this.iz.sendEmptyMessageDelayed(2, 60000);
            }
        } else {
            this.ix.setText((int) C0000R.string.no_stats_data);
        }
        bP();
    }
}
