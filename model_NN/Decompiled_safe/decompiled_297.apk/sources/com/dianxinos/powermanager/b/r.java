package com.dianxinos.powermanager.b;

import android.os.Parcel;

/* compiled from: UidStatsEntry */
public class r extends i {
    public int bT;

    public r(int i, double d) {
        this.bT = 0;
        this.hG = 1;
        this.bT = i;
        this.bq = d;
    }

    /* access modifiers changed from: protected */
    /* renamed from: cs */
    public r av() {
        return new r(this.bT, this.bq);
    }

    private r(Parcel parcel) {
        this.bT = 0;
        this.hG = 1;
        readFromParcel(parcel);
    }

    private void readFromParcel(Parcel parcel) {
        this.bT = parcel.readInt();
        this.bq = parcel.readDouble();
    }

    /* access modifiers changed from: private */
    public void writeToParcel(Parcel parcel) {
        parcel.writeInt(this.bT);
        parcel.writeDouble(this.bq);
    }
}
