package com.dianxinos.powermanager.b;

import android.content.Context;
import com.android.internal.os.PowerProfile;
import com.dianxinos.powermanager.c.e;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

/* compiled from: LocalPowerProfile */
public class m extends PowerProfile {
    private static final HashMap iD = new HashMap();

    public m(Context context) {
        super(context);
        synchronized (iD) {
            if (iD.size() == 0) {
                a(context, "data/" + new n().ch(), iD);
            }
        }
    }

    public double getAveragePower(String str) {
        if (iD.containsKey(str)) {
            Object obj = iD.get(str);
            if (!(obj instanceof Float[])) {
                return (double) ((Float) iD.get(str)).floatValue();
            }
            e.f("LocalPowerProfile", "Missing level for key: " + str);
            return (double) ((Float[]) obj)[0].floatValue();
        }
        e.f("LocalPowerProfile", "No value for key: " + str);
        return 0.0d;
    }

    public double getAveragePower(String str, int i) {
        if (iD.containsKey(str)) {
            Object obj = iD.get(str);
            if (obj instanceof Float[]) {
                Float[] fArr = (Float[]) obj;
                if (fArr.length > i && i >= 0) {
                    return (double) fArr[i].floatValue();
                }
                if (i >= 0) {
                    return (double) fArr[fArr.length - 1].floatValue();
                }
                e.f("LocalPowerProfile", "Incorrect level: " + i + ", key: " + str);
                return 0.0d;
            }
            e.f("LocalPowerProfile", "No level for key: " + str);
            return (double) ((Float) obj).floatValue();
        }
        e.f("LocalPowerProfile", "No value for key: " + str);
        return 0.0d;
    }

    public double getBatteryCapacity() {
        return getAveragePower("battery.capacity");
    }

    public int getNumSpeedSteps() {
        Object obj = iD.get("cpu.speeds");
        if (obj == null || !(obj instanceof Float[])) {
            return 1;
        }
        return ((Float[]) obj).length;
    }

    private void a(Context context, String str, HashMap hashMap) {
        try {
            a(context.getAssets().open(str), str, hashMap);
        } catch (IOException e) {
            e.f("LocalPowerProfile", "Failed to read the file: " + str + ", with exception: " + e.toString());
        }
    }

    private void a(InputStream inputStream, String str, HashMap hashMap) {
        DataInputStream dataInputStream = new DataInputStream(inputStream);
        try {
            dataInputStream.readInt();
            hashMap.put("dsp.audio", Float.valueOf(dataInputStream.readFloat()));
            hashMap.put("dsp.video", Float.valueOf(dataInputStream.readFloat()));
            hashMap.put("gps.on", Float.valueOf(dataInputStream.readFloat()));
            hashMap.put("screen.on", Float.valueOf(dataInputStream.readFloat()));
            hashMap.put("screen.full", Float.valueOf(dataInputStream.readFloat()));
            hashMap.put("bluetooth.on", Float.valueOf(dataInputStream.readFloat()));
            hashMap.put("bluetooth.active", Float.valueOf(dataInputStream.readFloat()));
            hashMap.put("bluetooth.at", Float.valueOf(dataInputStream.readFloat()));
            hashMap.put("wifi.on", Float.valueOf(dataInputStream.readFloat()));
            hashMap.put("wifi.active", Float.valueOf(dataInputStream.readFloat()));
            hashMap.put("wifi.scan", Float.valueOf(dataInputStream.readFloat()));
            int readInt = dataInputStream.readInt();
            Float[] fArr = new Float[readInt];
            for (int i = 0; i < readInt; i++) {
                fArr[i] = Float.valueOf(dataInputStream.readFloat());
            }
            hashMap.put("radio.on", fArr);
            hashMap.put("radio.active", Float.valueOf(dataInputStream.readFloat()));
            hashMap.put("radio.scanning", Float.valueOf(dataInputStream.readFloat()));
            hashMap.put("cpu.idle", Float.valueOf(dataInputStream.readFloat()));
            hashMap.put("cpu.awake", Float.valueOf(dataInputStream.readFloat()));
            int readInt2 = dataInputStream.readInt();
            Float[] fArr2 = new Float[readInt2];
            for (int i2 = 0; i2 < readInt2; i2++) {
                fArr2[i2] = Float.valueOf(dataInputStream.readFloat());
            }
            hashMap.put("cpu.speeds", fArr2);
            int readInt3 = dataInputStream.readInt();
            Float[] fArr3 = new Float[readInt3];
            for (int i3 = 0; i3 < readInt3; i3++) {
                fArr3[i3] = Float.valueOf(dataInputStream.readFloat());
            }
            hashMap.put("cpu.active", fArr3);
            hashMap.put("battery.capacity", Float.valueOf(dataInputStream.readFloat()));
            try {
                dataInputStream.close();
            } catch (IOException e) {
            }
        } catch (IOException e2) {
            e.f("LocalPowerProfile", "Failed to read the file " + str + " with exception: " + e2.toString());
            try {
                dataInputStream.close();
            } catch (IOException e3) {
            }
        } catch (Throwable th) {
            try {
                dataInputStream.close();
            } catch (IOException e4) {
            }
            throw th;
        }
    }
}
