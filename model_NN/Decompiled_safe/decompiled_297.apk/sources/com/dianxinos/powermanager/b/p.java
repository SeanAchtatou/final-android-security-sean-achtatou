package com.dianxinos.powermanager.b;

import android.content.Context;
import java.util.ListIterator;

/* compiled from: PowerStatsEntry */
class p extends i {
    p() {
    }

    public void a(Context context, int i, double d) {
        int size = this.hH.size();
        int i2 = 0;
        while (i2 < size && ((i) this.hH.get(i2)).hw >= d) {
            i2++;
        }
        while (i2 < this.hH.size()) {
            this.hH.remove(i2);
        }
    }

    public j T(int i) {
        ListIterator listIterator = this.hH.listIterator();
        while (listIterator.hasNext()) {
            j jVar = (j) listIterator.next();
            if (jVar.bT == i) {
                return jVar;
            }
        }
        j jVar2 = new j(i);
        this.hH.add(jVar2);
        return jVar2;
    }
}
