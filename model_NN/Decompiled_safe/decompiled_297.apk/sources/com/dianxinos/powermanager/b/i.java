package com.dianxinos.powermanager.b;

import android.content.Context;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.ListIterator;

/* compiled from: PowerStatsEntry */
public class i implements Comparable {
    public double bq;
    protected int hG = 2;
    public ArrayList hH = new ArrayList();
    public double hw;

    /* renamed from: by */
    public i clone() {
        i av = av();
        av.a(this);
        return av;
    }

    /* access modifiers changed from: protected */
    public i av() {
        return new i();
    }

    /* access modifiers changed from: protected */
    public void a(i iVar) {
        this.hG = iVar.hG;
        this.bq = iVar.bq;
        this.hw = iVar.hw;
        this.hH.clear();
        int size = iVar.hH.size();
        for (int i = 0; i < size; i++) {
            this.hH.add(((i) iVar.hH.get(i)).clone());
        }
    }

    public void c(i iVar) {
        this.hH.add(iVar);
    }

    public void clear() {
        this.hG = 2;
        this.bq = 0.0d;
        this.hw = 0.0d;
        Iterator it = this.hH.iterator();
        while (it.hasNext()) {
            ((i) it.next()).clear();
        }
        this.hH.clear();
    }

    public final void E(Context context) {
        au();
        a(100.0d);
    }

    public void a(Context context, int i, double d) {
    }

    /* access modifiers changed from: protected */
    public void b(i iVar) {
    }

    /* access modifiers changed from: protected */
    public void au() {
        if (this.hG != 1) {
            this.bq = 0.0d;
            ListIterator listIterator = this.hH.listIterator();
            while (listIterator.hasNext()) {
                i iVar = (i) listIterator.next();
                iVar.au();
                this.bq += iVar.bq;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(double d) {
        this.hw = d;
        if (this.hG != 1) {
            ListIterator listIterator = this.hH.listIterator();
            while (listIterator.hasNext()) {
                i iVar = (i) listIterator.next();
                iVar.a(this.bq > 0.0d ? (iVar.bq / this.bq) * this.hw : 0.0d);
            }
            Collections.sort(this.hH);
        }
    }

    /* renamed from: d */
    public int compareTo(i iVar) {
        if (iVar.hw > this.hw) {
            return 1;
        }
        return iVar.hw < this.hw ? -1 : 0;
    }
}
