package com.dianxinos.powermanager.b;

import android.os.Parcel;
import com.dianxinos.powermanager.c.e;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/* compiled from: UidStatsEntry */
public class d extends i {
    public String bl;
    public long bm;
    public long bp;
    public double br;
    public List dH = new LinkedList();
    public long dI;
    public long dJ;
    public long dK;
    public long dL;
    public long dM;
    public long dN;
    public int dO;
    public long dP;
    public long dQ;
    public double dR;
    public int mUid;

    public d(int i) {
        this.mUid = i;
    }

    public void clear() {
        super.clear();
        this.mUid = 0;
        this.bl = null;
        this.dH.clear();
    }

    /* access modifiers changed from: protected */
    /* renamed from: at */
    public d av() {
        return new d(this.mUid);
    }

    /* access modifiers changed from: protected */
    public void a(i iVar) {
        super.a(iVar);
        d dVar = (d) iVar;
        this.mUid = dVar.mUid;
        this.bl = dVar.bl;
        this.dH.clear();
        int size = dVar.dH.size();
        for (int i = 0; i < size; i++) {
            this.dH.add(dVar.dH.get(i));
        }
        this.bm = dVar.bm;
        this.dI = dVar.dI;
        this.dJ = dVar.dJ;
        this.bp = dVar.bp;
        this.dK = dVar.dK;
        this.dL = dVar.dL;
        this.dM = dVar.dM;
        this.dN = dVar.dN;
        this.dO = dVar.dO;
        this.dP = dVar.dP;
        this.dQ = dVar.dQ;
        this.br = dVar.br;
        this.dR = dVar.dR;
    }

    public void b(i iVar) {
        boolean z;
        d dVar = (d) iVar;
        this.bq += dVar.bq;
        this.hw += dVar.hw;
        this.bm += dVar.bm;
        this.dI += dVar.dI;
        this.bp += dVar.bp;
        this.dK += dVar.dK;
        this.dL += dVar.dL;
        this.dM += dVar.dM;
        this.dN += dVar.dN;
        this.dO += dVar.dO;
        this.dP += dVar.dP;
        this.dQ += dVar.dQ;
        this.br += dVar.br;
        this.dR += dVar.dR;
        this.dH.addAll(dVar.dH);
        Iterator it = dVar.hH.iterator();
        while (it.hasNext()) {
            i iVar2 = (i) it.next();
            r rVar = (r) iVar2;
            Iterator it2 = this.hH.iterator();
            while (true) {
                if (!it2.hasNext()) {
                    z = false;
                    break;
                }
                r rVar2 = (r) ((i) it2.next());
                if (rVar2.bT == rVar.bT) {
                    rVar2.bq += rVar.bq;
                    rVar2.hw += rVar.hw;
                    z = true;
                    break;
                }
            }
            if (!z) {
                this.hH.add(iVar2);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void au() {
        super.au();
        if (this.bq < this.br) {
            this.bq = this.br;
        }
    }

    public void a(d dVar) {
        if (this.mUid != dVar.mUid) {
            e.f("UidStatsEntry", "Not the same app, uid1: " + this.mUid + ", pkg1: " + this.bl + ", uid2: " + dVar.mUid + ", pkg2: " + dVar.bl);
            return;
        }
        this.bm -= dVar.bm;
        this.dI -= dVar.dI;
        this.dJ -= dVar.dJ;
        this.bp -= dVar.bp;
        this.dK -= dVar.dK;
        this.dL -= dVar.dL;
        this.dM -= dVar.dM;
        this.dN -= dVar.dN;
        this.dO -= dVar.dO;
        this.dP -= dVar.dP;
        this.dQ -= dVar.dQ;
        this.br -= dVar.br;
        if (this.br < 0.0d) {
            this.br = 0.0d;
        }
        Iterator it = dVar.hH.iterator();
        while (it.hasNext()) {
            r rVar = (r) ((i) it.next());
            Iterator it2 = this.hH.iterator();
            while (true) {
                if (!it2.hasNext()) {
                    break;
                }
                r rVar2 = (r) ((i) it2.next());
                if (rVar2.bT == rVar.bT) {
                    rVar2.bq -= rVar.bq;
                    if (rVar2.bq < 0.0d) {
                        rVar2.bq = 0.0d;
                    }
                }
            }
        }
    }

    public void writeToParcel(Parcel parcel) {
        parcel.writeInt(171543868);
        parcel.writeLong(this.bm);
        parcel.writeLong(this.dI);
        parcel.writeLong(this.dJ);
        parcel.writeLong(this.bp);
        parcel.writeLong(this.dK);
        parcel.writeLong(this.dL);
        parcel.writeLong(this.dM);
        parcel.writeLong(this.dN);
        parcel.writeInt(this.dO);
        parcel.writeLong(this.dP);
        parcel.writeLong(this.dQ);
        parcel.writeDouble(this.bq);
        parcel.writeDouble(this.br);
        parcel.writeInt(this.mUid);
        parcel.writeString(this.bl);
        parcel.writeInt(this.hH.size());
        Iterator it = this.hH.iterator();
        while (it.hasNext()) {
            ((r) ((i) it.next())).writeToParcel(parcel);
        }
        parcel.writeInt(this.dH.size());
        for (String writeString : this.dH) {
            parcel.writeString(writeString);
        }
    }

    public boolean a(Parcel parcel) {
        int readInt = parcel.readInt();
        if (readInt != 171543868) {
            e.f("UidStatsEntry", "Data corrupted with magic number: " + readInt);
            return false;
        }
        this.bm = parcel.readLong();
        this.dI = parcel.readLong();
        this.dJ = parcel.readLong();
        this.bp = parcel.readLong();
        this.dK = parcel.readLong();
        this.dL = parcel.readLong();
        this.dM = parcel.readLong();
        this.dN = parcel.readLong();
        this.dO = parcel.readInt();
        this.dP = parcel.readLong();
        this.dQ = parcel.readLong();
        this.bq = parcel.readDouble();
        this.br = parcel.readDouble();
        this.mUid = parcel.readInt();
        this.bl = parcel.readString();
        int readInt2 = parcel.readInt();
        this.hH.clear();
        for (int i = 0; i < readInt2; i++) {
            this.hH.add(new r(parcel));
        }
        int readInt3 = parcel.readInt();
        this.dH.clear();
        for (int i2 = 0; i2 < readInt3; i2++) {
            this.dH.add(parcel.readString());
        }
        return true;
    }
}
