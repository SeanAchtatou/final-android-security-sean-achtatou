package com.dianxinos.powermanager.b;

import com.android.internal.os.BatteryStatsImpl;

/* compiled from: PowerUsageStats */
public class e {
    public long aT;
    public f eD = new f();
    /* access modifiers changed from: private */
    public BatteryStatsImpl fA;
    public long fB;
    public i fC = new p();
    /* access modifiers changed from: private */
    public long fD;
    /* access modifiers changed from: private */
    public long fE;
    private long fF;
    private long fG;
    private long fH;
    private long fI;
    /* access modifiers changed from: private */
    public long fJ;
    /* access modifiers changed from: private */
    public long fK;
    /* access modifiers changed from: private */
    public long fL;
    /* access modifiers changed from: private */
    public long fM;

    static /* synthetic */ long a(e eVar, double d) {
        long j = (long) (((double) eVar.fJ) + d);
        eVar.fJ = j;
        return j;
    }

    static /* synthetic */ long a(e eVar, long j) {
        long j2 = eVar.fH + j;
        eVar.fH = j2;
        return j2;
    }

    static /* synthetic */ long b(e eVar, double d) {
        long j = (long) (((double) eVar.fK) + d);
        eVar.fK = j;
        return j;
    }

    static /* synthetic */ long b(e eVar, long j) {
        long j2 = eVar.fI + j;
        eVar.fI = j2;
        return j2;
    }

    static /* synthetic */ long c(e eVar, double d) {
        long j = (long) (((double) eVar.fL) + d);
        eVar.fL = j;
        return j;
    }

    static /* synthetic */ long c(e eVar, long j) {
        long j2 = eVar.fE + j;
        eVar.fE = j2;
        return j2;
    }

    static /* synthetic */ long d(e eVar, double d) {
        long j = (long) (((double) eVar.fM) + d);
        eVar.fM = j;
        return j;
    }

    static /* synthetic */ long d(e eVar, long j) {
        long j2 = eVar.fF + j;
        eVar.fF = j2;
        return j2;
    }

    static /* synthetic */ long e(e eVar, long j) {
        long j2 = eVar.fG + j;
        eVar.fG = j2;
        return j2;
    }

    static /* synthetic */ long f(e eVar, long j) {
        long j2 = eVar.fD + j;
        eVar.fD = j2;
        return j2;
    }
}
