package com.dianxinos.powermanager.b;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.BatteryStats;
import android.os.Parcel;
import android.os.ServiceManager;
import android.os.SystemClock;
import android.util.SparseArray;
import com.android.internal.app.IBatteryStats;
import com.android.internal.os.BatteryStatsImpl;
import com.android.internal.os.PowerProfile;
import com.dianxinos.powermanager.C0000R;
import com.dianxinos.powermanager.c.e;
import com.dianxinos.powermanager.c.j;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/* compiled from: PowerUsageStats */
public class o {
    private static o jm;
    private List aw = new LinkedList();
    private int hj = j.cc();
    private PowerProfile hk;
    private IBatteryStats hl;
    private Object jk = new Object();
    private e jl;
    /* access modifiers changed from: private */
    public boolean jn = false;
    /* access modifiers changed from: private */
    public Object jo = new Object();
    private long jp = 0;
    private Context mContext;

    public void a(v vVar) {
        synchronized (this.aw) {
            this.aw.add(vVar);
        }
    }

    public void b(v vVar) {
        synchronized (this.aw) {
            this.aw.remove(vVar);
        }
    }

    private void cj() {
        synchronized (this.aw) {
            for (v o : this.aw) {
                o.o();
            }
        }
    }

    private o(Context context) {
        this.mContext = context.getApplicationContext();
        this.hk = new m(this.mContext);
        this.hl = IBatteryStats.Stub.asInterface(ServiceManager.getService("batteryinfo"));
    }

    public static o K(Context context) {
        synchronized (o.class) {
            if (jm == null) {
                jm = new o(context);
            }
        }
        return jm;
    }

    private BatteryStatsImpl bu() {
        BatteryStatsImpl batteryStatsImpl = null;
        try {
            byte[] statistics = this.hl.getStatistics();
            Parcel obtain = Parcel.obtain();
            obtain.unmarshall(statistics, 0, statistics.length);
            obtain.setDataPosition(0);
            BatteryStatsImpl batteryStatsImpl2 = (BatteryStatsImpl) BatteryStatsImpl.CREATOR.createFromParcel(obtain);
            try {
                if (j.ce()) {
                    batteryStatsImpl2.distributeWorkLocked(this.hj);
                }
                obtain.recycle();
                return batteryStatsImpl2;
            } catch (Exception e) {
                Exception exc = e;
                batteryStatsImpl = batteryStatsImpl2;
                e = exc;
                e.f("PowerUsageStats", "Exception:" + e);
                return batteryStatsImpl;
            }
        } catch (Exception e2) {
            e = e2;
            e.f("PowerUsageStats", "Exception:" + e);
            return batteryStatsImpl;
        }
    }

    public e ck() {
        e eVar;
        synchronized (this.jk) {
            eVar = this.jl;
        }
        return eVar;
    }

    public void cl() {
        e.d("PowerUsageStats", "Request to refresh stats data");
        synchronized (this.jo) {
            if (!this.jn) {
                this.jn = true;
                this.jp = System.currentTimeMillis();
                new a(this).start();
            }
        }
    }

    public void cm() {
        if (System.currentTimeMillis() - this.jp > 300000) {
            cl();
        }
    }

    /* access modifiers changed from: private */
    public void cn() {
        e.d("PowerUsageStats", "Refreshing stats data");
        e eVar = new e();
        BatteryStatsImpl unused = eVar.fA = bu();
        if (eVar.fA != null) {
            h(eVar);
            eVar.eD.E(this.mContext);
            eVar.eD.a(this.mContext, 20, 0.1d);
            eVar.fC.E(this.mContext);
            eVar.fC.a(this.mContext, 20, 0.1d);
            synchronized (this.jk) {
                this.jl = eVar;
            }
            cj();
        }
    }

    public e co() {
        e.d("PowerUsageStats", "Snap battery stats");
        e eVar = new e();
        BatteryStatsImpl unused = eVar.fA = bu();
        if (eVar.fA == null) {
            return null;
        }
        h(eVar);
        return eVar;
    }

    private void h(e eVar) {
        long computeBatteryRealtime = eVar.fA.computeBatteryRealtime(SystemClock.elapsedRealtime() * 1000, this.hj);
        eVar.aT = System.currentTimeMillis();
        eVar.fB = computeBatteryRealtime / 1000;
        g(eVar, computeBatteryRealtime);
        h(eVar, computeBatteryRealtime);
    }

    private void g(e eVar, long j) {
        double d;
        String str;
        long j2;
        double d2;
        long j3;
        double d3;
        long j4;
        int i;
        String str2;
        SensorManager sensorManager = (SensorManager) this.mContext.getSystemService("sensor");
        int i2 = this.hj;
        int numSpeedSteps = this.hk.getNumSpeedSteps();
        double[] dArr = new double[numSpeedSteps];
        for (int i3 = 0; i3 < numSpeedSteps; i3++) {
            dArr[i3] = this.hk.getAveragePower("cpu.active", i3);
        }
        double averagePower = this.hk.getAveragePower("cpu.awake");
        double averagePower2 = this.hk.getAveragePower("wifi.on");
        double averagePower3 = this.hk.getAveragePower("gps.on");
        double averagePower4 = this.hk.getAveragePower("dsp.audio");
        double averagePower5 = this.hk.getAveragePower("dsp.video");
        f fVar = eVar.eD;
        p pVar = (p) eVar.fC;
        j T = pVar.T(2);
        j T2 = pVar.T(5);
        j T3 = pVar.T(6);
        j T4 = pVar.T(7);
        j T5 = pVar.T(8);
        j T6 = pVar.T(9);
        SparseArray uidStats = eVar.fA.getUidStats();
        int size = uidStats.size();
        int i4 = 0;
        while (true) {
            int i5 = i4;
            if (i5 < size) {
                BatteryStats.Uid uid = (BatteryStats.Uid) uidStats.valueAt(i5);
                int uid2 = uid.getUid();
                d dVar = new d(uid2);
                String str3 = null;
                Map processStats = uid.getProcessStats();
                long j5 = 0;
                long j6 = 0;
                if (processStats.size() > 0) {
                    double d4 = 0.0d;
                    double d5 = 0.0d;
                    long j7 = 0;
                    for (Map.Entry entry : processStats.entrySet()) {
                        String str4 = (String) entry.getKey();
                        BatteryStats.Uid.Proc proc = (BatteryStats.Uid.Proc) entry.getValue();
                        long userTime = (proc.getUserTime(i2) + proc.getSystemTime(i2)) * 10;
                        j7 += userTime;
                        j6 += proc.getForegroundTime(i2) * 10;
                        long[] jArr = new long[numSpeedSteps];
                        int i6 = 0;
                        for (int i7 = 0; i7 < numSpeedSteps; i7++) {
                            jArr[i7] = proc.getTimeAtCpuSpeedStep(i7, i2);
                            i6 = (int) (((long) i6) + jArr[i7]);
                        }
                        if (i6 == 0) {
                            i = 1;
                        } else {
                            i = i6;
                        }
                        double d6 = 0.0d;
                        for (int i8 = 0; i8 < numSpeedSteps; i8++) {
                            d6 += (((double) jArr[i8]) / ((double) i)) * ((double) userTime) * dArr[i8];
                        }
                        d5 += d6;
                        if (str3 == null || str3.startsWith("*")) {
                            str2 = str4;
                            d4 = d6;
                        } else if (d4 >= d6 || str4.startsWith("*")) {
                            str2 = str3;
                        } else {
                            str2 = str4;
                            d4 = d6;
                        }
                        if (d6 > 0.0d && !str4.startsWith("*")) {
                            dVar.dH.add(str4);
                        }
                        str3 = str2;
                    }
                    d = d5;
                    j5 = j7;
                    long j8 = j6;
                    str = str3;
                    j2 = j8;
                } else {
                    d = 0.0d;
                    str = null;
                    j2 = 0;
                }
                if (j2 > j5) {
                    j5 = j2;
                }
                dVar.bl = str;
                double d7 = d / 1000.0d;
                dVar.bm = j5;
                dVar.dI = j2;
                long j9 = j5 - j2;
                dVar.dJ = j9;
                if (j5 > 0) {
                    dVar.br = ((((double) j9) * d7) / ((double) j5)) + dVar.br;
                }
                long j10 = 0;
                for (Map.Entry value : uid.getWakelockStats().entrySet()) {
                    BatteryStats.Uid.Wakelock wakelock = (BatteryStats.Uid.Wakelock) value.getValue();
                    BatteryStats.Timer wakeTime = wakelock.getWakeTime(0);
                    if (wakeTime != null) {
                        j10 += wakeTime.getTotalTimeLocked(j, i2);
                    }
                    BatteryStats.Timer wakeTime2 = wakelock.getWakeTime(1);
                    if (wakeTime2 != null) {
                        e.a(eVar, wakeTime2.getTotalTimeLocked(j, i2) / 1000);
                    }
                    BatteryStats.Timer wakeTime3 = wakelock.getWakeTime(2);
                    if (wakeTime3 != null) {
                        e.b(eVar, wakeTime3.getTotalTimeLocked(j, i2) / 1000);
                    }
                }
                long j11 = j10 / 1000;
                double d8 = (((double) j11) * averagePower) / 1000.0d;
                dVar.dK = j11;
                dVar.br += d8;
                e.c(eVar, j2);
                e.d(eVar, j5);
                e.e(eVar, j11);
                dVar.c(new r(9, d7 + d8));
                T6.c(new q(uid2, str, d7 + d8));
                long tcpBytesReceived = uid.getTcpBytesReceived(this.hj);
                long tcpBytesSent = uid.getTcpBytesSent(this.hj);
                dVar.dP = tcpBytesReceived;
                dVar.dQ = tcpBytesSent;
                long j12 = 0;
                if (j.cd()) {
                    j12 = uid.getWifiRunningTime(j, i2) / 1000;
                }
                double d9 = (((double) j12) * averagePower2) / 1000.0d;
                long scanWifiLockTime = uid.getScanWifiLockTime(j, i2) / 1000;
                long fullWifiLockTime = uid.getFullWifiLockTime(j, i2) / 1000;
                e.f(eVar, j12);
                dVar.dN = j12;
                dVar.c(new r(2, d9));
                T.c(new q(uid2, str, d9));
                if (j12 > 0 || scanWifiLockTime > 0 || fullWifiLockTime > 0) {
                }
                long j13 = 0;
                long j14 = 0;
                double d10 = 0.0d;
                double d11 = 0.0d;
                for (Map.Entry value2 : uid.getSensorStats().entrySet()) {
                    BatteryStats.Uid.Sensor sensor = (BatteryStats.Uid.Sensor) value2.getValue();
                    int handle = sensor.getHandle();
                    long totalTimeLocked = sensor.getSensorTime().getTotalTimeLocked(j, i2) / 1000;
                    if (handle == -10000) {
                        j4 = totalTimeLocked;
                        long j15 = j13;
                        d3 = (((double) totalTimeLocked) * averagePower3) / 1000.0d;
                        d2 = d10;
                        j3 = j15;
                    } else {
                        Sensor defaultSensor = sensorManager.getDefaultSensor(handle);
                        if (defaultSensor != null) {
                            d2 = ((((double) defaultSensor.getPower()) * ((double) totalTimeLocked)) / 1000.0d) + d10;
                            j3 = j13 + totalTimeLocked;
                            d3 = d11;
                            j4 = j14;
                        } else {
                            d2 = d10;
                            j3 = j13;
                            d3 = d11;
                            j4 = j14;
                        }
                    }
                    j14 = j4;
                    d11 = d3;
                    j13 = j3;
                    d10 = d2;
                }
                dVar.dL = j14;
                dVar.dM = j13;
                dVar.c(new r(7, d11));
                dVar.c(new r(8, d10));
                T4.c(new q(uid.getUid(), str, d11));
                T4.hO += j14;
                T5.c(new q(uid.getUid(), str, d10));
                T5.hO += j13;
                long audioTurnedOnTime = uid.getAudioTurnedOnTime(j, i2) / 1000;
                double d12 = (((double) audioTurnedOnTime) * averagePower4) / 1000.0d;
                dVar.c(new r(5, d12));
                T2.c(new q(uid.getUid(), str, d12));
                T2.hO = audioTurnedOnTime + T2.hO;
                long videoTurnedOnTime = uid.getVideoTurnedOnTime(j, i2) / 1000;
                double d13 = (((double) videoTurnedOnTime) * averagePower5) / 1000.0d;
                dVar.c(new r(6, d13));
                T3.c(new q(uid.getUid(), str, d13));
                T3.hO = videoTurnedOnTime + T3.hO;
                int i9 = 0;
                for (Map.Entry value3 : uid.getPackageStats().entrySet()) {
                    i9 = ((BatteryStats.Uid.Pkg) value3.getValue()).getWakeups(i2) + i9;
                }
                dVar.dO = i9;
                fVar.c(dVar);
                i4 = i5 + 1;
            } else {
                return;
            }
        }
    }

    private void h(e eVar, long j) {
        i(eVar, j);
        j(eVar, j);
        k(eVar, j);
        l(eVar, j);
        m(eVar, j);
        o(eVar, j);
        n(eVar, j);
        eVar.eD.a(4, (double) eVar.fJ);
        eVar.eD.a(2, (double) eVar.fK);
        eVar.eD.a(9, (double) eVar.fL);
        eVar.eD.a(3, (double) eVar.fM);
    }

    private void i(e eVar, long j) {
        double averagePower = this.hk.getAveragePower("screen.on");
        long screenOnTime = eVar.fA.getScreenOnTime(j, this.hj) / 1000;
        double d = ((double) screenOnTime) * averagePower;
        double averagePower2 = this.hk.getAveragePower("screen.full");
        long j2 = 0;
        double d2 = 0.0d;
        for (int i = 0; i < 5; i++) {
            long screenBrightnessTime = eVar.fA.getScreenBrightnessTime(i, j, this.hj) / 1000;
            d2 += ((((double) (i + 1)) * averagePower2) / 5.0d) * ((double) screenBrightnessTime);
            j2 += screenBrightnessTime;
        }
        double d3 = d2 / ((double) j2);
        double d4 = d + d2;
        if (eVar.fE > 0) {
            j T = ((p) eVar.fC).T(1);
            T.hO = screenOnTime;
            Iterator it = eVar.eD.hH.iterator();
            while (it.hasNext()) {
                d dVar = (d) ((i) it.next());
                double f = ((double) dVar.dI) / ((double) eVar.fE);
                double d5 = ((double) screenOnTime) * f;
                double d6 = (((f * ((double) j2)) * d3) + (d5 * averagePower)) / 1000.0d;
                dVar.c(new r(1, d6));
                T.c(new q(dVar.mUid, dVar.bl, d6));
                dVar.bp = (long) d5;
            }
        }
    }

    private void j(e eVar, long j) {
        double averagePower = this.hk.getAveragePower("radio.active");
        long phoneOnTime = eVar.fA.getPhoneOnTime(j, this.hj) / 1000;
        double d = (averagePower * ((double) phoneOnTime)) / 1000.0d;
        String string = this.mContext.getString(C0000R.string.uid_label_radio_active);
        j T = ((p) eVar.fC).T(4);
        T.hO = phoneOnTime + T.hO;
        T.c(new h(4, string, d));
        e.a(eVar, d);
    }

    private void k(e eVar, long j) {
        long radioDataUptime = eVar.fA.getRadioDataUptime();
        double averagePower = (this.hk.getAveragePower("radio.active") * ((double) radioDataUptime)) / 1000.0d;
        String string = this.mContext.getString(C0000R.string.uid_label_radio_data);
        j T = ((p) eVar.fC).T(4);
        T.hO = radioDataUptime + T.hO;
        T.c(new h(4, string, averagePower));
        e.a(eVar, averagePower);
    }

    private void l(e eVar, long j) {
        double d = 0.0d;
        long j2 = 0;
        for (int i = 0; i < 5; i++) {
            double averagePower = this.hk.getAveragePower("radio.on", i);
            long phoneSignalStrengthTime = eVar.fA.getPhoneSignalStrengthTime(i, j, this.hj) / 1000;
            d += (averagePower * ((double) phoneSignalStrengthTime)) / 1000.0d;
            j2 += phoneSignalStrengthTime;
        }
        double averagePower2 = this.hk.getAveragePower("radio.scanning");
        long phoneSignalScanningTime = eVar.fA.getPhoneSignalScanningTime(j, this.hj) / 1000;
        double d2 = d + ((averagePower2 * ((double) phoneSignalScanningTime)) / 1000.0d);
        String string = this.mContext.getString(C0000R.string.uid_label_radio_idle);
        j T = ((p) eVar.fC).T(4);
        T.hO = j2 + phoneSignalScanningTime + T.hO;
        T.c(new h(4, string, d2));
        e.a(eVar, d2);
    }

    private void m(e eVar, long j) {
        long j2;
        long wifiOnTime = eVar.fA.getWifiOnTime(j, this.hj) / 1000;
        if (j.cd()) {
            j2 = eVar.fA.getGlobalWifiRunningTime(j, this.hj) / 1000;
        } else {
            try {
                j2 = ((Long) BatteryStatsImpl.class.getMethod("getWifiRunningTime", Long.TYPE, Integer.TYPE).invoke(eVar.fA, Long.valueOf(j), Integer.valueOf(this.hj))).longValue() / 1000;
            } catch (Exception e) {
                e.printStackTrace();
                j2 = 0;
            }
        }
        long g = j2 - eVar.fD;
        if (g < 0) {
            g = 0;
        }
        double averagePower = ((((double) g) * this.hk.getAveragePower("wifi.on")) + (((double) (wifiOnTime * 0)) * this.hk.getAveragePower("wifi.on"))) / 1000.0d;
        String string = this.mContext.getString(C0000R.string.uid_label_wlan_idle);
        j T = ((p) eVar.fC).T(2);
        T.hO = wifiOnTime;
        T.c(new h(2, string, averagePower));
        e.b(eVar, averagePower);
    }

    private void n(e eVar, long j) {
        double averagePower = this.hk.getAveragePower("cpu.idle");
        long screenOnTime = (j - eVar.fA.getScreenOnTime(j, this.hj)) / 1000;
        double d = (averagePower * ((double) screenOnTime)) / 1000.0d;
        String string = this.mContext.getString(C0000R.string.uid_label_cpu_idle);
        j T = ((p) eVar.fC).T(9);
        T.hO = screenOnTime;
        T.c(new h(9, string, d));
        e.c(eVar, d);
    }

    private void o(e eVar, long j) {
        double averagePower = this.hk.getAveragePower("bluetooth.on");
        double averagePower2 = this.hk.getAveragePower("bluetooth.at");
        long bluetoothOnTime = eVar.fA.getBluetoothOnTime(j, this.hj) / 1000;
        double bluetoothPingCount = ((averagePower * ((double) bluetoothOnTime)) / 1000.0d) + ((averagePower2 * ((double) eVar.fA.getBluetoothPingCount())) / 1000.0d);
        String string = this.mContext.getString(C0000R.string.uid_label_bluetooth_idle);
        j T = ((p) eVar.fC).T(3);
        T.hO = bluetoothOnTime;
        T.c(new h(3, string, bluetoothPingCount));
        e.d(eVar, bluetoothPingCount);
    }
}
