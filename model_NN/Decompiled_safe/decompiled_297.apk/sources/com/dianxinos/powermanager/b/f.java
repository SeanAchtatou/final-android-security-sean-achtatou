package com.dianxinos.powermanager.b;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.SparseArray;
import com.dianxinos.powermanager.c.e;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

/* compiled from: UidPowerStats */
public class f extends i {
    public static final Parcelable.Creator CREATOR = new u();
    public double dR;
    private d ga;
    public ArrayList gb = new ArrayList();
    public double gc;

    public void clear() {
        super.clear();
        if (this.ga != null) {
            this.ga.clear();
        }
        Iterator it = this.gb.iterator();
        while (it.hasNext()) {
            ((d) it.next()).clear();
        }
        this.gb.clear();
    }

    /* access modifiers changed from: protected */
    /* renamed from: aZ */
    public f av() {
        return new f();
    }

    /* access modifiers changed from: protected */
    public void a(i iVar) {
        super.a(iVar);
    }

    /* access modifiers changed from: protected */
    public void au() {
        super.au();
        this.gb.clear();
        this.gc = 0.0d;
        Iterator it = this.hH.iterator();
        while (it.hasNext()) {
            d dVar = (d) ((i) it.next());
            this.gc += dVar.br;
            this.gb.add(dVar);
        }
    }

    /* access modifiers changed from: protected */
    public void a(double d) {
        super.a(d);
        if (this.gc > 0.0d) {
            this.dR = d;
            Iterator it = this.gb.iterator();
            while (it.hasNext()) {
                d dVar = (d) it.next();
                dVar.dR = (dVar.br / this.gc) * d;
            }
            Collections.sort(this.gb, new l());
            return;
        }
        this.gb.clear();
    }

    public void c(i iVar) {
        d dVar = (d) iVar;
        if (dVar.mUid >= 10000 && dVar.mUid <= 99999) {
            this.hH.add(iVar);
        } else if (this.ga == null) {
            this.ga = dVar;
            this.ga.mUid = 0;
            this.hH.add(this.ga);
        } else {
            this.ga.b(dVar);
        }
    }

    public void a(int i, double d) {
        boolean z;
        if (this.ga == null) {
            e.f("UidPowerStats", "logic error! no system entry yet.");
        }
        Iterator it = this.ga.hH.iterator();
        while (true) {
            if (!it.hasNext()) {
                z = false;
                break;
            }
            r rVar = (r) ((i) it.next());
            if (rVar.bT == i) {
                rVar.bq += d;
                z = true;
                break;
            }
        }
        if (!z) {
            this.ga.c(new r(i, d));
        }
    }

    public void a(Context context, int i, double d) {
        a(context, i, d, this.hH, this.hw);
        b(context, i, d, this.gb, this.dR);
    }

    private static void a(Context context, int i, double d, ArrayList arrayList, double d2) {
        int size = arrayList.size();
        if (size < i) {
            i = size;
        }
        double d3 = 0.0d;
        int i2 = 0;
        while (i2 < i) {
            double d4 = ((i) arrayList.get(i2)).hw;
            if (d4 < 0.1d) {
                break;
            }
            d3 += d4;
            i2++;
        }
        double d5 = d2 - d3;
        d dVar = null;
        if (d3 > 0.0d && d5 >= d) {
            d dVar2 = new d(-1);
            for (int i3 = i2; i3 < size; i3++) {
                i iVar = (i) arrayList.get(i3);
                if (iVar.hw == 0.0d) {
                    break;
                }
                dVar2.b(iVar);
            }
            dVar2.hw = d5;
            dVar = dVar2;
        }
        while (i2 < arrayList.size()) {
            arrayList.remove(i2);
        }
        if (dVar != null) {
            arrayList.add(dVar);
        }
    }

    private static void b(Context context, int i, double d, ArrayList arrayList, double d2) {
        int size = arrayList.size();
        if (size < i) {
            i = size;
        }
        double d3 = 0.0d;
        int i2 = 0;
        while (i2 < i) {
            double d4 = ((d) arrayList.get(i2)).dR;
            if (d4 < 0.1d) {
                break;
            }
            d3 += d4;
            i2++;
        }
        double d5 = d2 - d3;
        d dVar = null;
        if (d3 > 0.0d && d5 >= d) {
            d dVar2 = new d(-1);
            for (int i3 = i2; i3 < size; i3++) {
                d dVar3 = (d) arrayList.get(i3);
                if (dVar3.dR == 0.0d) {
                    break;
                }
                dVar2.b(dVar3);
            }
            dVar2.dR = d5;
            dVar = dVar2;
        }
        while (i2 < arrayList.size()) {
            arrayList.remove(i2);
        }
        if (dVar != null) {
            arrayList.add(dVar);
        }
    }

    public void f(int i, boolean z) {
        int i2 = 0;
        if (z) {
            this.gb.remove(i);
            int i3 = ((d) this.gb.get(i)).mUid;
            if (i3 != -1) {
                int size = this.hH.size();
                while (i2 < size) {
                    if (((d) this.hH.get(i2)).mUid == i3) {
                        this.hH.remove(i2);
                        return;
                    }
                    i2++;
                }
                return;
            }
            return;
        }
        this.hH.remove(i);
        int i4 = ((d) this.hH.get(i)).mUid;
        if (i4 != -1) {
            int size2 = this.gb.size();
            while (i2 < size2) {
                if (((d) this.gb.get(i2)).mUid == i4) {
                    this.gb.remove(i2);
                    return;
                }
                i2++;
            }
        }
    }

    public void a(f fVar) {
        SparseArray b = b(this.hH);
        SparseArray b2 = b(fVar.hH);
        int size = b2.size();
        for (int i = 0; i < size; i++) {
            d dVar = (d) b.get(b2.keyAt(i));
            if (dVar != null) {
                dVar.a((d) b2.valueAt(i));
            }
        }
        this.hH.clear();
        this.hH = a(b);
    }

    private SparseArray b(ArrayList arrayList) {
        SparseArray sparseArray = new SparseArray();
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            d dVar = (d) ((i) it.next());
            sparseArray.put(dVar.mUid, dVar);
        }
        return sparseArray;
    }

    private ArrayList a(SparseArray sparseArray) {
        ArrayList arrayList = new ArrayList();
        int size = sparseArray.size();
        for (int i = 0; i < size; i++) {
            arrayList.add(sparseArray.valueAt(i));
        }
        return arrayList;
    }

    /* access modifiers changed from: private */
    public boolean a(Parcel parcel) {
        int readInt = parcel.readInt();
        if (readInt != -372005032) {
            e.f("UidPowerStats", "Data corrupted with magic number: " + readInt);
            return false;
        }
        parcel.readInt();
        this.hH.clear();
        int readInt2 = parcel.readInt();
        int i = 0;
        while (i < readInt2) {
            d dVar = new d(-2);
            if (dVar.a(parcel)) {
                this.hH.add(dVar);
                if (dVar.mUid == 0) {
                    this.ga = dVar;
                }
                i++;
            } else {
                e.f("UidPowerStats", "failed to read child #" + i);
                return false;
            }
        }
        return true;
    }

    private void writeToParcel(Parcel parcel) {
        parcel.writeInt(-372005032);
        parcel.writeInt(3);
        parcel.writeInt(this.hH.size());
        Iterator it = this.hH.iterator();
        while (it.hasNext()) {
            ((d) ((i) it.next())).writeToParcel(parcel);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020 A[SYNTHETIC, Splitter:B:7:0x0020] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean b(android.content.Context r8, java.lang.String r9) {
        /*
            r7 = this;
            r5 = 0
            r0 = 0
            java.io.FileInputStream r0 = r8.openFileInput(r9)     // Catch:{ IOException -> 0x0024 }
            byte[] r1 = a(r0)     // Catch:{ IOException -> 0x004e }
            android.os.Parcel r2 = android.os.Parcel.obtain()     // Catch:{ IOException -> 0x004e }
            r3 = 0
            int r4 = r1.length     // Catch:{ IOException -> 0x004e }
            r2.unmarshall(r1, r3, r4)     // Catch:{ IOException -> 0x004e }
            r1 = 0
            r2.setDataPosition(r1)     // Catch:{ IOException -> 0x004e }
            boolean r1 = r7.a(r2)     // Catch:{ IOException -> 0x004e }
            r6 = r1
            r1 = r0
            r0 = r6
        L_0x001e:
            if (r1 == 0) goto L_0x0023
            r1.close()     // Catch:{ IOException -> 0x004c }
        L_0x0023:
            return r0
        L_0x0024:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
        L_0x0028:
            java.lang.String r2 = "UidPowerStats"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Failed to read file: "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r3 = r3.append(r9)
            java.lang.String r4 = " with exception: "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r0 = r3.append(r0)
            java.lang.String r0 = r0.toString()
            com.dianxinos.powermanager.c.e.f(r2, r0)
            r0 = r5
            goto L_0x001e
        L_0x004c:
            r1 = move-exception
            goto L_0x0023
        L_0x004e:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x0028
        */
        throw new UnsupportedOperationException("Method not decompiled: com.dianxinos.powermanager.b.f.b(android.content.Context, java.lang.String):boolean");
    }

    private static byte[] a(FileInputStream fileInputStream) {
        byte[] bArr = new byte[fileInputStream.available()];
        int i = 0;
        while (true) {
            int read = fileInputStream.read(bArr, i, bArr.length - i);
            if (read <= 0) {
                return bArr;
            }
            i += read;
            int available = fileInputStream.available();
            if (available > bArr.length - i) {
                byte[] bArr2 = new byte[(available + i)];
                System.arraycopy(bArr, 0, bArr2, 0, i);
                bArr = bArr2;
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:6:0x0019 A[SYNTHETIC, Splitter:B:6:0x0019] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void c(android.content.Context r7, java.lang.String r8) {
        /*
            r6 = this;
            android.os.Parcel r0 = android.os.Parcel.obtain()
            r6.writeToParcel(r0)
            byte[] r1 = r0.marshall()
            r0.recycle()
            r0 = 0
            r2 = 0
            java.io.FileOutputStream r0 = r7.openFileOutput(r8, r2)     // Catch:{ IOException -> 0x001d }
            r0.write(r1)     // Catch:{ IOException -> 0x0047 }
        L_0x0017:
            if (r0 == 0) goto L_0x001c
            r0.close()     // Catch:{ IOException -> 0x0045 }
        L_0x001c:
            return
        L_0x001d:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x0021:
            java.lang.String r2 = "UidPowerStats"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Failed to write file: "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r3 = r3.append(r8)
            java.lang.String r4 = " with exception: "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r0 = r3.append(r0)
            java.lang.String r0 = r0.toString()
            com.dianxinos.powermanager.c.e.f(r2, r0)
            r0 = r1
            goto L_0x0017
        L_0x0045:
            r0 = move-exception
            goto L_0x001c
        L_0x0047:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0021
        */
        throw new UnsupportedOperationException("Method not decompiled: com.dianxinos.powermanager.b.f.c(android.content.Context, java.lang.String):void");
    }
}
