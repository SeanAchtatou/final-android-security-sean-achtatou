package com.dianxinos.powermanager.b;

import android.content.Context;
import com.dianxinos.powermanager.c.c;
import com.dianxinos.powermanager.c.d;
import com.dianxinos.powermanager.c.e;
import com.dianxinos.powermanager.c.i;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: PowerStatsSnapMgr */
public class t implements d {
    private static t ko;
    private boolean dd = false;
    private o kc;
    private boolean kd = false;
    private long ke;
    private int kf;
    private int kg;
    private long kh;
    private long ki;
    private long kj;
    private f kk;
    private f kl = new f();
    private long km;
    private ArrayList kn = new ArrayList();
    private Context mContext;
    private long mLastSnapTime;

    public static t M(Context context) {
        synchronized (t.class) {
            if (ko == null) {
                ko = new t(context);
            }
        }
        return ko;
    }

    private t(Context context) {
        this.mContext = context.getApplicationContext();
        this.kc = o.K(this.mContext);
        cy();
    }

    public long ct() {
        long currentTimeMillis = System.currentTimeMillis() - this.mLastSnapTime;
        if (currentTimeMillis < 0 || currentTimeMillis > 600000) {
            currentTimeMillis = 600000;
        }
        return 600000 - currentTimeMillis;
    }

    public void l(boolean z) {
        if (!z || !this.dd || this.kg != this.kf) {
            e co = this.kc.co();
            if (co == null) {
                e.e("PowerStatsSnapMgr", "Failed to get stats snap. Cannot add new snap.");
                return;
            }
            co.fC.clear();
            if (co.fB < this.kj) {
                e.d("PowerStatsSnapMgr", "System statistical data was cleared. Reset the snaps");
                cB();
            }
            long j = co.aT;
            long j2 = co.fB;
            f fVar = co.eD;
            fVar.c(this.mContext, cA());
            fVar.clear();
            synchronized (this.kn) {
                this.kn.add(new b(j, j2, this.kg));
                this.kg++;
                this.mLastSnapTime = j;
                this.kj = j2;
                if (this.kh == 0) {
                    this.kh = System.currentTimeMillis();
                    this.ki = j2;
                }
                while (this.kh + 3600000 < this.mLastSnapTime && this.kn.size() > 2) {
                    this.kn.remove(0);
                    this.mContext.deleteFile(V(this.kf));
                    b bVar = (b) this.kn.get(0);
                    this.kh = bVar.aT;
                    this.kf = bVar.aV;
                }
                cz();
            }
        }
    }

    public f cu() {
        f fVar;
        long j;
        synchronized (this.kn) {
            boolean z = false;
            b bVar = null;
            fVar = null;
            while (true) {
                if (this.kn.size() <= 0) {
                    break;
                }
                bVar = (b) this.kn.get(0);
                String V = V(bVar.aV);
                f fVar2 = new f();
                if (fVar2.b(this.mContext, V)) {
                    fVar = fVar2;
                    break;
                }
                e.f("PowerStatsSnapMgr", "Bad data file: " + V + "! remove it");
                this.mContext.deleteFile(V);
                fVar2.clear();
                this.kn.remove(0);
                z = true;
                fVar = null;
            }
            if (z) {
                cz();
            }
            if (fVar != null) {
                this.kh = bVar.aT;
                this.kf = bVar.aV;
                j = this.kh;
            } else {
                j = 0;
            }
        }
        if (fVar == null) {
            if (!this.dd) {
                l(false);
            }
            this.kl.clear();
            this.km = 0;
            return this.kl;
        }
        long currentTimeMillis = System.currentTimeMillis() - j;
        if (currentTimeMillis < 300000) {
            this.kl.clear();
            this.km = currentTimeMillis;
            return this.kl;
        }
        e co = this.kc.co();
        if (co == null) {
            e.e("PowerStatsSnapMgr", "Failed to get stats snap. Use the old one.");
            return this.kl;
        }
        co.fC.clear();
        f fVar3 = co.eD;
        if (this.kk != null) {
            this.kk.clear();
        }
        this.kk = (f) fVar3.clone();
        fVar3.a(fVar);
        long j2 = co.aT - j;
        e.d("PowerStatsSnapMgr", "Snap time gap: " + d(j2));
        fVar3.E(this.mContext);
        fVar3.a(this.mContext, 20, 0.1d);
        this.kl = fVar3;
        this.km = j2;
        return this.kl;
    }

    public f cv() {
        return this.kl;
    }

    public long cw() {
        return 300000 - this.km;
    }

    public void e(int i, String str) {
        d dVar;
        boolean z;
        int i2;
        boolean z2;
        if (i != -1) {
            d dVar2 = null;
            Iterator it = this.kk.hH.iterator();
            while (true) {
                if (!it.hasNext()) {
                    dVar = dVar2;
                    break;
                }
                dVar2 = (d) ((i) it.next());
                if (dVar2.mUid == i) {
                    dVar = dVar2;
                    break;
                }
            }
            if (dVar == null) {
                e.f("PowerStatsSnapMgr", "Not found uid: " + i + ", proc: " + str);
                return;
            }
            f fVar = new f();
            synchronized (this.kn) {
                int i3 = 0;
                boolean z3 = false;
                while (i3 < this.kn.size()) {
                    String V = V(((b) this.kn.get(i3)).aV);
                    fVar.clear();
                    if (!fVar.b(this.mContext, V)) {
                        e.f("PowerStatsSnapMgr", "Bad data file: " + V + "! remove it");
                        this.mContext.deleteFile(V);
                        this.kn.remove(i3);
                        i2 = i3 - 1;
                        z2 = true;
                    } else {
                        int size = fVar.hH.size();
                        int i4 = 0;
                        while (true) {
                            if (i4 >= size) {
                                z = false;
                                break;
                            } else if (((d) fVar.hH.get(i4)).mUid == i) {
                                fVar.hH.set(i4, dVar);
                                fVar.c(this.mContext, V);
                                z = true;
                                break;
                            } else {
                                i4++;
                            }
                        }
                        if (!z) {
                            fVar.hH.add(dVar);
                            fVar.c(this.mContext, V);
                        }
                        i2 = i3;
                        z2 = z3;
                    }
                    z3 = z2;
                    i3 = i2 + 1;
                }
                if (z3) {
                    cz();
                }
            }
            fVar.clear();
        }
    }

    public void cx() {
        cB();
        this.kl.clear();
        if (this.kk != null) {
            this.kk.clear();
        }
        if (!this.dd) {
            l(false);
        }
    }

    private void cy() {
        DataInputStream dataInputStream = null;
        try {
            DataInputStream dataInputStream2 = new DataInputStream(this.mContext.openFileInput("snaps_config"));
            try {
                int readInt = dataInputStream2.readInt();
                dataInputStream2.readInt();
                if (readInt != -245549990) {
                    cB();
                } else {
                    a(dataInputStream2);
                }
                dataInputStream = dataInputStream2;
            } catch (IOException e) {
                dataInputStream = dataInputStream2;
            }
        } catch (IOException e2) {
        }
        if (dataInputStream != null) {
            try {
                dataInputStream.close();
            } catch (IOException e3) {
            }
        }
    }

    private void a(DataInputStream dataInputStream) {
        this.kf = dataInputStream.readInt();
        this.kg = dataInputStream.readInt();
        int readInt = dataInputStream.readInt();
        this.kn.clear();
        for (int i = 0; i < readInt; i++) {
            this.kn.add(new b(dataInputStream.readLong(), dataInputStream.readLong(), dataInputStream.readInt()));
        }
        if (readInt > 0) {
            b bVar = (b) this.kn.get(0);
            b bVar2 = (b) this.kn.get(readInt - 1);
            this.kh = bVar.aT;
            this.mLastSnapTime = bVar2.aT;
            this.ki = bVar.aU;
            this.kj = bVar2.aU;
        }
    }

    private void cz() {
        DataOutputStream dataOutputStream = null;
        try {
            DataOutputStream dataOutputStream2 = new DataOutputStream(this.mContext.openFileOutput("snaps_config", 0));
            try {
                dataOutputStream2.writeInt(-245549990);
                dataOutputStream2.writeInt(2);
                dataOutputStream2.writeInt(this.kf);
                dataOutputStream2.writeInt(this.kg);
                dataOutputStream2.writeInt(this.kn.size());
                for (int i = 0; i < this.kn.size(); i++) {
                    b bVar = (b) this.kn.get(i);
                    dataOutputStream2.writeLong(bVar.aT);
                    dataOutputStream2.writeLong(bVar.aU);
                    dataOutputStream2.writeInt(bVar.aV);
                }
                dataOutputStream = dataOutputStream2;
            } catch (IOException e) {
                dataOutputStream = dataOutputStream2;
            }
        } catch (IOException e2) {
        }
        if (dataOutputStream != null) {
            try {
                dataOutputStream.close();
            } catch (IOException e3) {
            }
        }
    }

    private String cA() {
        return "snaps_stats_" + this.kg;
    }

    private String V(int i) {
        return "snaps_stats_" + i;
    }

    public void d(i iVar) {
        boolean z = iVar.status == 2;
        if (this.dd) {
            if (System.currentTimeMillis() - this.ke >= 600000) {
                cB();
                this.kd = true;
                this.ke = System.currentTimeMillis();
            }
        } else if (z) {
            this.dd = true;
            this.ke = System.currentTimeMillis();
        }
        if (!z) {
            this.dd = false;
            this.ke = 0;
            if (this.kd) {
                l(false);
                this.kd = false;
            }
        }
    }

    private void cB() {
        synchronized (this.kn) {
            for (int i = this.kf; i < this.kg; i++) {
                this.mContext.deleteFile(V(i));
            }
            this.kf = this.kg;
            this.kh = 0;
            this.mLastSnapTime = 0;
            this.ki = 0;
            this.kj = 0;
            this.kn.clear();
            cz();
        }
    }

    private String d(long j) {
        return c.c(this.mContext, (int) (j / 1000));
    }
}
