package com.dianxinos.powermanager.b;

import com.dianxinos.powermanager.C0000R;

/* compiled from: HardwareType */
public class c {
    private static final int[] bj = {C0000R.string.hw_label_unknown, C0000R.string.hw_label_screen, C0000R.string.hw_label_wlan, C0000R.string.hw_label_bluetooth, C0000R.string.hw_label_radio, C0000R.string.hw_label_audio, C0000R.string.hw_label_video, C0000R.string.hw_label_gps, C0000R.string.hw_label_sensor, C0000R.string.hw_label_cpu};
    private static final int[] bk = {C0000R.drawable.ic_hw_default, C0000R.drawable.ic_hw_screen, C0000R.drawable.ic_hw_wifi, C0000R.drawable.ic_hw_bluetooth, C0000R.drawable.ic_hw_radio, C0000R.drawable.ic_hw_audio, C0000R.drawable.ic_hw_video, C0000R.drawable.ic_hw_gps, C0000R.drawable.ic_hw_sensor, C0000R.drawable.ic_hw_cpu};

    public static int m(int i) {
        return bj[i];
    }

    public static int n(int i) {
        return bk[i];
    }
}
