package com.dianxinos.powermanager.b;

import android.content.Context;
import android.content.pm.PackageManager;
import android.util.SparseArray;
import com.dianxinos.powermanager.c.e;

/* compiled from: LabelIconHelper */
public class k {
    private static k is = null;
    private PackageManager iq;
    private SparseArray ir = new SparseArray();
    private Context mContext;

    private k(Context context) {
        this.mContext = context.getApplicationContext();
        this.iq = this.mContext.getPackageManager();
    }

    public static k H(Context context) {
        synchronized (k.class) {
            if (is == null) {
                is = new k(context);
            }
        }
        return is;
    }

    public void ao() {
        e.d("LabelIconHelper", "locale changed, clear data");
        synchronized (this.ir) {
            this.ir.clear();
        }
    }

    public void clearCache() {
        e.d("LabelIconHelper", "clear cached data");
        synchronized (this.ir) {
            this.ir.clear();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:48:?, code lost:
        r4 = r13.iq.getApplicationInfo(r1[r3], 0);
        r0.label = r4.loadLabel(r13.iq).toString();
        r0.kb = r1[r3];
        r0.icon = r4.loadIcon(r13.iq);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00d0, code lost:
        r1 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x00ff, code lost:
        r1 = r2;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.dianxinos.powermanager.b.s d(int r14, java.lang.String r15) {
        /*
            r13 = this;
            r12 = 17301651(0x1080093, float:2.4979667E-38)
            r11 = 1
            r10 = 0
            android.util.SparseArray r1 = r13.ir
            monitor-enter(r1)
            android.util.SparseArray r0 = r13.ir     // Catch:{ all -> 0x0014 }
            java.lang.Object r0 = r0.get(r14)     // Catch:{ all -> 0x0014 }
            com.dianxinos.powermanager.b.s r0 = (com.dianxinos.powermanager.b.s) r0     // Catch:{ all -> 0x0014 }
            monitor-exit(r1)     // Catch:{ all -> 0x0014 }
            if (r0 == 0) goto L_0x0017
        L_0x0013:
            return r0
        L_0x0014:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0014 }
            throw r0
        L_0x0017:
            com.dianxinos.powermanager.b.s r0 = new com.dianxinos.powermanager.b.s
            r0.<init>()
            r1 = -1
            if (r14 != r1) goto L_0x0043
            android.content.Context r1 = r13.mContext
            android.content.res.Resources r1 = r1.getResources()
            android.graphics.drawable.Drawable r1 = r1.getDrawable(r12)
            r0.icon = r1
            android.content.Context r1 = r13.mContext
            r2 = 2131230789(0x7f080045, float:1.807764E38)
            java.lang.String r1 = r1.getString(r2)
            r0.label = r1
            android.util.SparseArray r1 = r13.ir
            monitor-enter(r1)
            android.util.SparseArray r2 = r13.ir     // Catch:{ all -> 0x0040 }
            r2.put(r14, r0)     // Catch:{ all -> 0x0040 }
            monitor-exit(r1)     // Catch:{ all -> 0x0040 }
            goto L_0x0013
        L_0x0040:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0040 }
            throw r0
        L_0x0043:
            android.content.pm.PackageManager r1 = r13.iq
            java.lang.String[] r1 = r1.getPackagesForUid(r14)
            if (r1 == 0) goto L_0x004e
            int r2 = r1.length
            if (r2 != 0) goto L_0x0105
        L_0x004e:
            if (r14 != 0) goto L_0x00f2
            android.content.Context r2 = r13.mContext
            android.content.res.Resources r2 = r2.getResources()
            r3 = 2131230760(0x7f080028, float:1.8077582E38)
            java.lang.String r2 = r2.getString(r3)
            r0.label = r2
        L_0x005f:
            android.content.Context r2 = r13.mContext
            android.content.res.Resources r2 = r2.getResources()
            android.graphics.drawable.Drawable r2 = r2.getDrawable(r12)
            r0.icon = r2
            r2 = r11
        L_0x006c:
            if (r2 != 0) goto L_0x00a1
            int r3 = r1.length
            if (r3 <= r11) goto L_0x00a1
            int r3 = r1.length
            r4 = r10
        L_0x0073:
            if (r4 >= r3) goto L_0x00a1
            r5 = r1[r4]
            android.content.pm.PackageManager r6 = r13.iq     // Catch:{ NameNotFoundException -> 0x00f6 }
            r7 = 0
            android.content.pm.PackageInfo r6 = r6.getPackageInfo(r5, r7)     // Catch:{ NameNotFoundException -> 0x00f6 }
            int r7 = r6.sharedUserLabel     // Catch:{ NameNotFoundException -> 0x00f6 }
            if (r7 == 0) goto L_0x00f7
            android.content.pm.PackageManager r7 = r13.iq     // Catch:{ NameNotFoundException -> 0x00f6 }
            int r8 = r6.sharedUserLabel     // Catch:{ NameNotFoundException -> 0x00f6 }
            android.content.pm.ApplicationInfo r9 = r6.applicationInfo     // Catch:{ NameNotFoundException -> 0x00f6 }
            java.lang.CharSequence r7 = r7.getText(r5, r8, r9)     // Catch:{ NameNotFoundException -> 0x00f6 }
            if (r7 == 0) goto L_0x00f7
            java.lang.String r7 = r7.toString()     // Catch:{ NameNotFoundException -> 0x00f6 }
            r0.label = r7     // Catch:{ NameNotFoundException -> 0x00f6 }
            r0.kb = r5     // Catch:{ NameNotFoundException -> 0x00f6 }
            android.content.pm.ApplicationInfo r5 = r6.applicationInfo     // Catch:{ NameNotFoundException -> 0x00f6 }
            android.content.pm.PackageManager r6 = r13.iq     // Catch:{ NameNotFoundException -> 0x00f6 }
            android.graphics.drawable.Drawable r5 = r5.loadIcon(r6)     // Catch:{ NameNotFoundException -> 0x00f6 }
            r0.icon = r5     // Catch:{ NameNotFoundException -> 0x00f6 }
            r2 = r11
        L_0x00a1:
            if (r2 != 0) goto L_0x0103
            r3 = r10
        L_0x00a4:
            int r4 = r1.length
            if (r3 >= r4) goto L_0x0101
            r4 = r1[r3]
            boolean r4 = r4.equals(r15)
            if (r4 == 0) goto L_0x00fb
        L_0x00af:
            android.content.pm.PackageManager r4 = r13.iq     // Catch:{ NameNotFoundException -> 0x00fe }
            r5 = r1[r3]     // Catch:{ NameNotFoundException -> 0x00fe }
            r6 = 0
            android.content.pm.ApplicationInfo r4 = r4.getApplicationInfo(r5, r6)     // Catch:{ NameNotFoundException -> 0x00fe }
            android.content.pm.PackageManager r5 = r13.iq     // Catch:{ NameNotFoundException -> 0x00fe }
            java.lang.CharSequence r5 = r4.loadLabel(r5)     // Catch:{ NameNotFoundException -> 0x00fe }
            java.lang.String r5 = r5.toString()     // Catch:{ NameNotFoundException -> 0x00fe }
            r0.label = r5     // Catch:{ NameNotFoundException -> 0x00fe }
            r1 = r1[r3]     // Catch:{ NameNotFoundException -> 0x00fe }
            r0.kb = r1     // Catch:{ NameNotFoundException -> 0x00fe }
            android.content.pm.PackageManager r1 = r13.iq     // Catch:{ NameNotFoundException -> 0x00fe }
            android.graphics.drawable.Drawable r1 = r4.loadIcon(r1)     // Catch:{ NameNotFoundException -> 0x00fe }
            r0.icon = r1     // Catch:{ NameNotFoundException -> 0x00fe }
            r1 = r11
        L_0x00d1:
            if (r1 != 0) goto L_0x00e4
            r0.label = r15
            r1 = 0
            r0.kb = r1
            android.content.Context r1 = r13.mContext
            android.content.res.Resources r1 = r1.getResources()
            android.graphics.drawable.Drawable r1 = r1.getDrawable(r12)
            r0.icon = r1
        L_0x00e4:
            android.util.SparseArray r1 = r13.ir
            monitor-enter(r1)
            android.util.SparseArray r2 = r13.ir     // Catch:{ all -> 0x00ef }
            r2.put(r14, r0)     // Catch:{ all -> 0x00ef }
            monitor-exit(r1)     // Catch:{ all -> 0x00ef }
            goto L_0x0013
        L_0x00ef:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x00ef }
            throw r0
        L_0x00f2:
            r0.label = r15
            goto L_0x005f
        L_0x00f6:
            r5 = move-exception
        L_0x00f7:
            int r4 = r4 + 1
            goto L_0x0073
        L_0x00fb:
            int r3 = r3 + 1
            goto L_0x00a4
        L_0x00fe:
            r1 = move-exception
            r1 = r2
            goto L_0x00d1
        L_0x0101:
            r3 = r10
            goto L_0x00af
        L_0x0103:
            r1 = r2
            goto L_0x00d1
        L_0x0105:
            r2 = r10
            goto L_0x006c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.dianxinos.powermanager.b.k.d(int, java.lang.String):com.dianxinos.powermanager.b.s");
    }

    public String u(String str) {
        try {
            return this.iq.getApplicationInfo(str, 0).loadLabel(this.iq).toString();
        } catch (PackageManager.NameNotFoundException e) {
            return null;
        }
    }
}
