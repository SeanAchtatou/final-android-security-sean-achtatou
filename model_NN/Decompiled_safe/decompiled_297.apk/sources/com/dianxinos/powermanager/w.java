package com.dianxinos.powermanager;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.dianxinos.powermanager.b.c;
import com.dianxinos.powermanager.b.d;
import com.dianxinos.powermanager.b.i;
import com.dianxinos.powermanager.b.j;
import com.dianxinos.powermanager.b.k;
import com.dianxinos.powermanager.b.s;
import java.util.ArrayList;

/* compiled from: PowerUsageListAdapter */
public class w extends BaseAdapter {
    private k eC;
    private double eE;
    private boolean iM = false;
    private int iN;
    private Context mContext;
    private LayoutInflater mInflater;
    private int o;
    private ArrayList p;

    public w(Context context) {
        this.mContext = context;
        this.mInflater = LayoutInflater.from(this.mContext);
        this.eC = k.H(this.mContext);
    }

    public void a(ArrayList arrayList) {
        this.p = arrayList;
        this.eE = this.p.size() > 0 ? ((i) this.p.get(0)).hw : 1.0d;
        this.o = this.p.size();
        notifyDataSetChanged();
    }

    public void R(int i) {
        this.iM = true;
        this.iN = i;
        if (this.iM && this.iN == 2) {
            this.eE = this.p.size() > 0 ? ((d) this.p.get(0)).dR : 1.0d;
        }
    }

    public int getCount() {
        return this.o;
    }

    public Object getItem(int i) {
        return null;
    }

    public long getItemId(int i) {
        return 0;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        z zVar;
        View view2;
        if (view == null) {
            View inflate = this.mInflater.inflate((int) C0000R.layout.power_usage_list_item, (ViewGroup) null);
            zVar = new z();
            zVar.gY = (ImageView) inflate.findViewById(C0000R.id.icon);
            zVar.gZ = (TextView) inflate.findViewById(C0000R.id.label);
            zVar.ha = (ImageView) inflate.findViewById(C0000R.id.progress_image);
            zVar.hb = (TextView) inflate.findViewById(C0000R.id.progress);
            inflate.setTag(zVar);
            view2 = inflate;
        } else {
            zVar = (z) view.getTag();
            view2 = view;
        }
        i iVar = (i) this.p.get(i);
        double d = iVar.hw;
        if (this.iM && this.iN == 2) {
            d = ((d) iVar).dR;
        }
        if (this.iM) {
            d dVar = (d) iVar;
            s d2 = this.eC.d(dVar.mUid, dVar.bl);
            zVar.gY.setImageDrawable(d2.icon);
            zVar.gZ.setText(d2.label);
        } else {
            j jVar = (j) iVar;
            zVar.gY.setImageResource(c.n(jVar.bT));
            zVar.gZ.setText(c.m(jVar.bT));
        }
        zVar.hb.setText(String.format("%.1f%%", Double.valueOf(d)));
        Drawable drawable = this.mContext.getResources().getDrawable(C0000R.drawable.list_item_progress_bar);
        Drawable drawable2 = this.mContext.getResources().getDrawable(C0000R.drawable.list_item_progress_bkg);
        double d3 = d / this.eE;
        zVar.ha.setImageDrawable(new com.dianxinos.powermanager.ui.d(drawable, drawable2, d3));
        zVar.hd = d3;
        return view2;
    }
}
