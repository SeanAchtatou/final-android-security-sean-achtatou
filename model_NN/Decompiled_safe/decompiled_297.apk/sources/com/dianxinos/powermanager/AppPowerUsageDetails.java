package com.dianxinos.powermanager;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.dianxinos.powermanager.b.d;
import com.dianxinos.powermanager.b.f;
import com.dianxinos.powermanager.b.k;
import com.dianxinos.powermanager.b.r;
import com.dianxinos.powermanager.b.s;
import com.dianxinos.powermanager.b.t;
import com.dianxinos.powermanager.c.c;
import com.dianxinos.powermanager.c.e;
import com.dianxinos.powermanager.c.j;
import com.dianxinos.powermanager.menu.o;
import java.util.LinkedList;

public class AppPowerUsageDetails extends Activity implements View.OnClickListener {
    private o bI;
    private View bJ;
    private View bK;
    private ViewGroup bL;
    private View bM;
    private View bN;
    private ViewGroup bO;
    private Button bS;
    private String iV;
    private View iW;
    private Button iX;
    private ViewGroup iY;
    private View iZ;
    private View ja;
    private ViewGroup jb;
    private View jc;
    private View jd;
    private ViewGroup je;
    private LayoutInflater mInflater;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.power_usage_details);
        this.bI = new o(this);
        try {
            init();
        } catch (Exception e) {
            finish();
            e.e("AppPowerUsageDetails", "Process killed??? Exception: " + e);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.iV == null || !A(this.iV)) {
            this.bM.setVisibility(8);
        } else {
            this.bM.setVisibility(0);
        }
    }

    private void init() {
        f fVar;
        double d;
        d dVar;
        Intent intent = getIntent();
        boolean booleanExtra = intent.getBooleanExtra("recent", false);
        int intExtra = intent.getIntExtra("position", 0);
        double doubleExtra = intent.getDoubleExtra("bar_percent", 0.0d);
        boolean booleanExtra2 = intent.getBooleanExtra("bg", false);
        this.mInflater = LayoutInflater.from(this);
        if (booleanExtra) {
            fVar = t.M(this).cv();
        } else {
            fVar = com.dianxinos.powermanager.b.o.K(this).ck().eD;
        }
        if (booleanExtra2) {
            d dVar2 = (d) fVar.gb.get(intExtra);
            d = dVar2.dR;
            dVar = dVar2;
        } else {
            d dVar3 = (d) fVar.hH.get(intExtra);
            d = dVar3.hw;
            dVar = dVar3;
        }
        View findViewById = findViewById(C0000R.id.summary_header);
        k H = k.H(this);
        s d2 = H.d(dVar.mUid, dVar.bl);
        this.iV = d2.kb;
        ((ImageView) findViewById.findViewById(C0000R.id.icon)).setImageDrawable(d2.icon);
        ((TextView) findViewById.findViewById(C0000R.id.label)).setText(d2.label);
        ((TextView) findViewById.findViewById(C0000R.id.progress)).setText(String.format("%.1f%%", Double.valueOf(d)));
        ((ImageView) findViewById.findViewById(C0000R.id.progress_image)).setImageDrawable(new com.dianxinos.powermanager.ui.d(getResources().getDrawable(C0000R.drawable.list_item_progress_bar), getResources().getDrawable(C0000R.drawable.list_item_progress_bkg), doubleExtra));
        this.bJ = findViewById(C0000R.id.data_stats_layout);
        this.bK = findViewById(C0000R.id.data_stats_switch);
        this.bK.setOnClickListener(this);
        this.bL = (ViewGroup) findViewById(C0000R.id.data_stats_group);
        this.bM = findViewById(C0000R.id.operation_layout);
        this.bN = findViewById(C0000R.id.operation_switch);
        this.bN.setOnClickListener(this);
        this.bO = (ViewGroup) findViewById(C0000R.id.operation_group);
        this.iW = findViewById(C0000R.id.children_layout);
        this.iX = (Button) findViewById(C0000R.id.children_switch);
        this.iX.setOnClickListener(this);
        this.iY = (ViewGroup) findViewById(C0000R.id.children_group);
        this.iZ = findViewById(C0000R.id.packages_layout);
        this.ja = findViewById(C0000R.id.packages_switch);
        this.ja.setOnClickListener(this);
        this.jb = (ViewGroup) findViewById(C0000R.id.packages_group);
        this.jc = findViewById(C0000R.id.processes_layout);
        this.jd = findViewById(C0000R.id.processes_switch);
        this.jd.setOnClickListener(this);
        this.je = (ViewGroup) findViewById(C0000R.id.processes_group);
        a(this.bL, dVar.bm, (int) C0000R.string.app_detail_labels_cpu_time);
        a(this.bL, dVar.dK, (int) C0000R.string.app_detail_labels_wakelock_time);
        a(this.bL, dVar.bp, (int) C0000R.string.app_detail_labels_screen_time);
        a(this.bL, dVar.dL, (int) C0000R.string.app_detail_labels_gps_time);
        a(this.bL, dVar.dM, (int) C0000R.string.app_detail_labels_sensors_time);
        a(this.bL, dVar.dN, (int) C0000R.string.app_detail_labels_wifi_time);
        a(this.bL, dVar.dO, C0000R.string.app_detail_labels_wakeup_times, false);
        b(this.bL, dVar.dP, C0000R.string.app_detail_labels_tcp_received);
        b(this.bL, dVar.dQ, C0000R.string.app_detail_labels_tcp_sent);
        a(this.bL, String.format("%.1f%%", Double.valueOf(dVar.bq > 0.0d ? (dVar.br / dVar.bq) * 100.0d : 0.0d)), (int) C0000R.string.app_detail_labels_bg_percent);
        if (this.bL.getChildCount() == 0) {
            this.bJ.setVisibility(8);
        }
        TextView textView = (TextView) findViewById(C0000R.id.operation_label);
        this.bS = (Button) findViewById(C0000R.id.operation_btn);
        this.bS.setOnClickListener(this);
        if (this.iV == null) {
            this.bM.setVisibility(8);
        } else {
            textView.setText((int) C0000R.string.app_detail_operation_description_common);
            this.bS.setText((int) C0000R.string.app_detail_operation_btn_common);
        }
        this.iX.setText((int) C0000R.string.app_detail_hardware_usage);
        b(dVar);
        LinkedList<String> linkedList = new LinkedList<>();
        for (String u : dVar.dH) {
            String u2 = H.u(u);
            if (u2 != null) {
                linkedList.add(u2);
            }
        }
        if (linkedList.size() > 1) {
            for (String text : linkedList) {
                View inflate = this.mInflater.inflate((int) C0000R.layout.app_power_usage_package_item, (ViewGroup) null);
                this.jb.addView(inflate);
                ((TextView) inflate.findViewById(C0000R.id.name)).setText(text);
            }
            this.iZ.setVisibility(0);
        } else {
            this.iZ.setVisibility(8);
        }
        this.jc.setVisibility(8);
    }

    private void a(ViewGroup viewGroup, long j, int i) {
        int i2 = (int) (j / 1000);
        if (i2 > 0) {
            a(viewGroup, c.b(this, i2), i);
        }
    }

    private void a(ViewGroup viewGroup, int i, int i2, boolean z) {
        if (z || i > 0) {
            a(viewGroup, String.valueOf(i), i2);
        }
    }

    private void b(ViewGroup viewGroup, long j, int i) {
        if (j > 0) {
            a(viewGroup, c.a(this, j), i);
        }
    }

    private void a(ViewGroup viewGroup, String str, int i) {
        View inflate = this.mInflater.inflate((int) C0000R.layout.power_data_stats_item, (ViewGroup) null);
        viewGroup.addView(inflate);
        ((TextView) inflate.findViewById(C0000R.id.name)).setText(i);
        ((TextView) inflate.findViewById(C0000R.id.value)).setText(str);
    }

    private void b(d dVar) {
        j jVar = new j(this);
        int size = dVar.hH.size();
        for (int i = 0; i < size; i++) {
            r rVar = (r) dVar.hH.get(i);
            if (rVar.hw < 0.10000000149011612d) {
                break;
            }
            if (i > 0) {
                ImageView imageView = new ImageView(this);
                imageView.setImageResource(C0000R.drawable.horizontal_line);
                imageView.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
                this.iY.addView(imageView);
            }
            View inflate = this.mInflater.inflate((int) C0000R.layout.power_usage_sub_list_item, (ViewGroup) null);
            ((ImageView) inflate.findViewById(C0000R.id.icon)).setImageResource(com.dianxinos.powermanager.b.c.n(rVar.bT));
            ((TextView) inflate.findViewById(C0000R.id.label)).setText(com.dianxinos.powermanager.b.c.m(rVar.bT));
            ((TextView) inflate.findViewById(C0000R.id.progress)).setText(String.format("%.1f%%", Double.valueOf(rVar.hw)));
            if (S(rVar.bT)) {
                inflate.setFocusable(true);
                inflate.setClickable(true);
                inflate.setBackgroundResource(C0000R.drawable.power_usage_sub_list_item_bkg);
                inflate.setTag(Integer.valueOf(rVar.bT));
                inflate.setOnClickListener(jVar);
                inflate.findViewById(C0000R.id.indicator).setVisibility(0);
            }
            this.iY.addView(inflate);
        }
        if (this.iY.getChildCount() == 0) {
            this.iW.setVisibility(8);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    public void onClick(View view) {
        boolean z;
        int i;
        boolean z2;
        int i2;
        boolean z3;
        int i3;
        boolean z4;
        int i4;
        int i5;
        int i6 = C0000R.drawable.power_usage_details_switch_down;
        if (view == this.bK) {
            boolean z5 = this.bL.getVisibility() == 0;
            View view2 = this.bK;
            if (!z5) {
                i6 = C0000R.drawable.power_usage_details_switch_up;
            }
            view2.setBackgroundResource(i6);
            ViewGroup viewGroup = this.bL;
            if (z5) {
                i5 = 8;
            } else {
                i5 = 0;
            }
            viewGroup.setVisibility(i5);
        } else if (view == this.bN) {
            if (this.bO.getVisibility() == 0) {
                z4 = true;
            } else {
                z4 = false;
            }
            View view3 = this.bN;
            if (!z4) {
                i6 = C0000R.drawable.power_usage_details_switch_up;
            }
            view3.setBackgroundResource(i6);
            ViewGroup viewGroup2 = this.bO;
            if (z4) {
                i4 = 8;
            } else {
                i4 = 0;
            }
            viewGroup2.setVisibility(i4);
        } else if (view == this.iX) {
            if (this.iY.getVisibility() == 0) {
                z3 = true;
            } else {
                z3 = false;
            }
            Button button = this.iX;
            if (!z3) {
                i6 = C0000R.drawable.power_usage_details_switch_up;
            }
            button.setBackgroundResource(i6);
            ViewGroup viewGroup3 = this.iY;
            if (z3) {
                i3 = 8;
            } else {
                i3 = 0;
            }
            viewGroup3.setVisibility(i3);
        } else if (view == this.ja) {
            if (this.jb.getVisibility() == 0) {
                z2 = true;
            } else {
                z2 = false;
            }
            View view4 = this.ja;
            if (!z2) {
                i6 = C0000R.drawable.power_usage_details_switch_up;
            }
            view4.setBackgroundResource(i6);
            ViewGroup viewGroup4 = this.jb;
            if (z2) {
                i2 = 8;
            } else {
                i2 = 0;
            }
            viewGroup4.setVisibility(i2);
        } else if (view == this.jd) {
            if (this.je.getVisibility() == 0) {
                z = true;
            } else {
                z = false;
            }
            View view5 = this.jd;
            if (!z) {
                i6 = C0000R.drawable.power_usage_details_switch_up;
            }
            view5.setBackgroundResource(i6);
            ViewGroup viewGroup5 = this.je;
            if (z) {
                i = 8;
            } else {
                i = 0;
            }
            viewGroup5.setVisibility(i);
        } else if (view == this.bS) {
            e.d("AppPowerUsageDetails", "check details for " + this.iV);
            startActivity(j.w(this.iV));
        }
    }

    private boolean S(int i) {
        return i == 2 || i == 1 || i == 3 || i == 7;
    }

    /* access modifiers changed from: private */
    public void p(int i) {
        if (i == 2) {
            startActivity(new Intent("android.settings.WIFI_SETTINGS"));
        } else if (i == 1) {
            startActivity(new Intent("android.settings.DISPLAY_SETTINGS"));
        } else if (i == 3) {
            startActivity(new Intent("android.settings.BLUETOOTH_SETTINGS"));
        } else if (i == 7) {
            startActivity(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"));
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return this.bI.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        return this.bI.onOptionsItemSelected(menuItem);
    }

    private boolean A(String str) {
        try {
            getPackageManager().getPackageInfo(str, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }
}
