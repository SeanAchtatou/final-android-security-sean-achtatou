package com.dianxinos.powermanager;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.dianxinos.powermanager.b.e;
import com.dianxinos.powermanager.b.i;
import com.dianxinos.powermanager.b.o;
import com.dianxinos.powermanager.b.v;
import com.dianxinos.powermanager.c.c;

public class HardwaresPowerUsageSummary extends Activity implements View.OnClickListener, AdapterView.OnItemClickListener, v {
    private o C;
    private i D;
    private w E;
    private ListView F;
    private View G;
    private v H = new v(this);

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.power_usage_summary);
        this.E = new w(this);
        this.F = (ListView) findViewById(C0000R.id.list);
        this.F.setAdapter((ListAdapter) this.E);
        this.F.setEmptyView(findViewById(C0000R.id.empty));
        this.F.setOnItemClickListener(this);
        this.C = o.K(this);
        this.C.a(this);
        n();
        this.G = findViewById(C0000R.id.summary_icon);
        this.G.setOnClickListener(this);
        o.K(this).cl();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.C.cm();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.C.b(this);
    }

    /* access modifiers changed from: private */
    public void n() {
        e ck = this.C.ck();
        if (ck == null) {
            this.C.cl();
            return;
        }
        this.D = ck.fC;
        this.E.a(this.D.hH);
        String b = c.b(this, (int) (ck.fB / 1000));
        int i = (int) (this.D.bq / 3600.0d);
        ((TextView) findViewById(C0000R.id.summary_descption)).setText(getString(C0000R.string.apps_summary_descption_2, new Object[]{b}));
    }

    public void onItemClick(AdapterView adapterView, View view, int i, long j) {
        Intent intent = new Intent(this, HwPowerUsageDetails.class);
        intent.putExtra("position", i);
        intent.putExtra("bar_percent", ((z) view.getTag()).hd);
        startActivity(intent);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void onClick(View view) {
        if (view == this.G && this.D != null) {
            Intent intent = new Intent(this, PieChartActivity.class);
            intent.putExtra("apps", false);
            startActivity(intent);
        }
    }

    public void o() {
        this.H.sendEmptyMessage(1);
    }
}
