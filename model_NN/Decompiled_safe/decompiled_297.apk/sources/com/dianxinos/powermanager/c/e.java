package com.dianxinos.powermanager.c;

import android.util.Log;

/* compiled from: LogHelper */
public class e {
    public static void d(String str, String str2) {
        Log.d("DX-PowerManager", g(str, str2));
    }

    public static void c(String str, String str2) {
        Log.i("DX-PowerManager", g(str, str2));
    }

    public static void e(String str, String str2) {
        Log.w("DX-PowerManager", g(str, str2));
    }

    public static void f(String str, String str2) {
        Log.e("DX-PowerManager", g(str, str2));
    }

    private static String g(String str, String str2) {
        return "[" + str + "] " + str2;
    }
}
