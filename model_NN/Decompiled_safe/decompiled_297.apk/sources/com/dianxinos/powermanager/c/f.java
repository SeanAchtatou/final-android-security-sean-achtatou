package com.dianxinos.powermanager.c;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;

/* compiled from: BitmapScaler */
public class f {
    private Bitmap ey;

    public f(Resources resources, int i, int i2, int i3) {
        a(resources, i, a(resources, i, i2, i3));
        e(i2, i3);
    }

    public Bitmap aD() {
        return this.ey;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    private void e(int i, int i2) {
        int width = this.ey.getWidth();
        int height = this.ey.getHeight();
        Matrix matrix = new Matrix();
        matrix.postScale(((float) i) / ((float) width), ((float) i2) / ((float) height));
        this.ey = Bitmap.createBitmap(this.ey, 0, 0, width, height, matrix, true);
    }

    private void a(Resources resources, int i, k kVar) {
        new Matrix().postScale(kVar.jV, kVar.jV);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = kVar.jU;
        this.ey = BitmapFactory.decodeResource(resources, i, options);
    }

    private k a(Resources resources, int i, int i2, int i3) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(resources, i, options);
        return a(options.outWidth, options.outHeight, i2, i3);
    }

    private k a(int i, int i2, int i3, int i4) {
        k kVar = new k();
        kVar.jV = (float) (i2 / i4);
        kVar.jU = 1;
        int i5 = i2;
        int i6 = i;
        while (i5 / 2 >= i4) {
            i6 /= 2;
            i5 /= 2;
            kVar.jU *= 2;
        }
        return kVar;
    }
}
