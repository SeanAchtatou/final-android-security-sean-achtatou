package com.dianxinos.powermanager.c;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/* compiled from: BatteryInfoHelper */
class g extends BroadcastReceiver {
    final /* synthetic */ a eO;

    g(a aVar) {
        this.eO = aVar;
    }

    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action.equals("android.intent.action.BATTERY_CHANGED")) {
            this.eO.a(intent);
        } else if (action.equals("com.dianxinos.powermanager.MODECHANGE")) {
            this.eO.aq.O();
            if (this.eO.ao != null) {
                this.eO.ao.gU = true;
                this.eO.ao.gX = this.eO.aq.a((double) this.eO.ao.gV, 5);
                this.eO.a(this.eO.ao);
            }
        } else if (action.equals("com.dianxinos.powermanager.MODEMODIFIED")) {
            this.eO.aq.O();
            if (this.eO.ao != null) {
                this.eO.ao.gU = false;
                this.eO.ao.gX = this.eO.aq.a((double) this.eO.ao.gV, 5);
                this.eO.a(this.eO.ao);
            }
        } else if (action.equals("com.dianxinos.powermanager.action.RemainingTimeUpate") && this.eO.ao != null) {
            this.eO.ao.gU = false;
            this.eO.ao.gX = this.eO.aq.a((double) this.eO.ao.gV, 5);
            this.eO.a(this.eO.ao);
        }
    }
}
