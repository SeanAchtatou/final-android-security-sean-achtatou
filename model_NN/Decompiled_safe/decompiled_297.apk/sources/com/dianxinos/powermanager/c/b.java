package com.dianxinos.powermanager.c;

import android.content.Context;
import android.content.SharedPreferences;

/* compiled from: GlobalConfigMgr */
public class b {
    private Context mContext;

    public b(Context context) {
        this.mContext = context;
    }

    public boolean D() {
        return !this.mContext.getSharedPreferences("global_config", 0).getBoolean("guide_viewed", false);
    }

    public void b(boolean z) {
        SharedPreferences.Editor edit = this.mContext.getSharedPreferences("global_config", 0).edit();
        edit.putBoolean("guide_viewed", z);
        edit.commit();
    }

    public boolean E() {
        SharedPreferences sharedPreferences = this.mContext.getSharedPreferences("global_config", 0);
        boolean z = sharedPreferences.getBoolean("first_run", true);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putBoolean("first_run", false);
        edit.commit();
        return z;
    }

    public void a(long j, long j2) {
        SharedPreferences.Editor edit = this.mContext.getSharedPreferences("global_config", 0).edit();
        edit.putLong("pps_count", j);
        edit.putLong("pps_time", j2);
        edit.commit();
    }

    public long[] F() {
        SharedPreferences sharedPreferences = this.mContext.getSharedPreferences("global_config", 0);
        return new long[]{sharedPreferences.getLong("pps_count", 0), sharedPreferences.getLong("pps_time", 0)};
    }

    public void a(int i, boolean z, long j, long j2) {
        SharedPreferences.Editor edit = this.mContext.getSharedPreferences("global_config", 0).edit();
        if (i == 2) {
            if (z) {
                edit.putLong("usb_screenon_count", j);
                edit.putLong("usb_screenon_time", j2);
            } else {
                edit.putLong("usb_screenoff_count", j);
                edit.putLong("usb_screenoff_time", j2);
            }
        } else if (i == 1) {
            if (z) {
                edit.putLong("ac_screenon_count", j);
                edit.putLong("ac_screenon_time", j2);
            } else {
                edit.putLong("ac_screenoff_count", j);
                edit.putLong("ac_screenoff_time", j2);
            }
        }
        edit.commit();
    }

    public long[] a(int i, boolean z) {
        SharedPreferences sharedPreferences = this.mContext.getSharedPreferences("global_config", 0);
        long[] jArr = {0, 0};
        if (i == 2) {
            if (z) {
                jArr[0] = sharedPreferences.getLong("usb_screenon_count", 0);
                jArr[1] = sharedPreferences.getLong("usb_screenon_time", 0);
            } else {
                jArr[0] = sharedPreferences.getLong("usb_screenoff_count", 0);
                jArr[1] = sharedPreferences.getLong("usb_screenoff_time", 0);
            }
        } else if (i == 1) {
            if (z) {
                jArr[0] = sharedPreferences.getLong("ac_screenon_count", 0);
                jArr[1] = sharedPreferences.getLong("ac_screenon_time", 0);
            } else {
                jArr[0] = sharedPreferences.getLong("ac_screenoff_count", 0);
                jArr[1] = sharedPreferences.getLong("ac_screenoff_time", 0);
            }
        }
        return jArr;
    }
}
