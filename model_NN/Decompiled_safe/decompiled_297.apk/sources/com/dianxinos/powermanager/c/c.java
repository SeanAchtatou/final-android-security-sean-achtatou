package com.dianxinos.powermanager.c;

import android.content.Context;
import com.dianxinos.powermanager.C0000R;

/* compiled from: FormatHelper */
public class c {
    public static String b(Context context, int i) {
        int i2;
        int i3;
        int i4;
        int i5;
        if (i > 3600) {
            i2 = i / 3600;
            i3 = i - (i2 * 3600);
        } else {
            i2 = 0;
            i3 = i;
        }
        if (i3 > 60) {
            int i6 = i3 / 60;
            int i7 = i6;
            i4 = i3 - (i6 * 60);
            i5 = i7;
        } else {
            i4 = i3;
            i5 = 0;
        }
        StringBuilder sb = new StringBuilder();
        if (i2 > 0) {
            if (i5 == 0) {
                sb.append(context.getString(C0000R.string.battery_time_hours, Integer.valueOf(i2)));
            } else {
                sb.append(context.getString(C0000R.string.battery_time_hours_minutes, Integer.valueOf(i2), Integer.valueOf(i5)));
            }
        } else if (i5 > 0) {
            sb.append(context.getString(C0000R.string.battery_time_minutes, Integer.valueOf(i5)));
        } else {
            sb.append(context.getString(C0000R.string.battery_time_seconds, Integer.valueOf(i4)));
        }
        return sb.toString();
    }

    public static String c(Context context, int i) {
        return b(context, i);
    }

    public static String a(Context context, long j) {
        if (j > 1000000) {
            return String.format("%.2f MB", Float.valueOf(((float) (j / 1000)) / 1000.0f));
        } else if (j > 1024) {
            return String.format("%.2f KB", Float.valueOf(((float) (j / 10)) / 100.0f));
        } else {
            return String.format("%d B", Integer.valueOf((int) j));
        }
    }
}
