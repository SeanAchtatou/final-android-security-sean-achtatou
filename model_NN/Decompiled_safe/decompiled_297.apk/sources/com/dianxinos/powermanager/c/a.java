package com.dianxinos.powermanager.c;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.SystemClock;
import com.dianxinos.powermanager.C0000R;
import com.dianxinos.powermanager.d.c;
import com.dianxinos.powermanager.d.f;
import java.util.LinkedList;
import java.util.List;

/* compiled from: BatteryInfoHelper */
public class a {
    public static int[] am = {C0000R.color.info_value_low, C0000R.color.info_value_high};
    private static final String an = Build.DEVICE;
    public static long ar = -1;
    public static long as = -2;
    private static a av;
    /* access modifiers changed from: private */
    public i ao;
    private f ap;
    /* access modifiers changed from: private */
    public c aq;
    private long at = ar;
    private BroadcastReceiver au = new g(this);
    private List aw = new LinkedList();
    private Context mContext;

    public static a b(Context context) {
        synchronized (a.class) {
            if (av == null) {
                av = new a(context);
            }
        }
        return av;
    }

    private a(Context context) {
        this.mContext = context.getApplicationContext();
        this.ap = f.G(this.mContext);
        this.aq = new c(this.mContext);
        this.aq.O();
        B();
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        C();
    }

    public void a(d dVar) {
        synchronized (this.aw) {
            this.aw.add(dVar);
        }
        dVar.d(this.ao);
    }

    public void b(d dVar) {
        synchronized (this.aw) {
            this.aw.remove(dVar);
        }
    }

    public int z() {
        int i = -1;
        if (this.ao != null) {
            int i2 = this.ao.gX;
            int a = this.aq.a((double) this.ao.gV, 5);
            if (a - i2 < 300) {
                i = i2 + 300;
            } else {
                i = a;
            }
            this.ao.gX = i;
            a(this.ao);
        }
        return i;
    }

    public int A() {
        return this.ao.gX;
    }

    /* access modifiers changed from: private */
    public void a(i iVar) {
        synchronized (this.aw) {
            for (d d : this.aw) {
                d.d(iVar);
            }
        }
    }

    private void B() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.BATTERY_CHANGED");
        intentFilter.addAction("com.dianxinos.powermanager.MODECHANGE");
        intentFilter.addAction("com.dianxinos.powermanager.MODEMODIFIED");
        intentFilter.addAction("com.dianxinos.powermanager.action.RemainingTimeUpate");
        Intent registerReceiver = this.mContext.registerReceiver(this.au, intentFilter);
        if (registerReceiver != null) {
            a(registerReceiver);
        }
        e.c("BatteryInfoHelper", "BatteryInfoHelper, service started");
    }

    private void C() {
        this.mContext.unregisterReceiver(this.au);
        e.c("BatteryInfoHelper", "BatteryInfoHelper, service stopped");
    }

    /* access modifiers changed from: private */
    public void a(Intent intent) {
        i iVar = new i();
        iVar.status = intent.getIntExtra("status", 1);
        iVar.gN = intent.getIntExtra("health", 1);
        iVar.gO = intent.getBooleanExtra("present", false);
        iVar.level = intent.getIntExtra("level", 0);
        iVar.gP = intent.getIntExtra("scale", 100);
        iVar.gQ = intent.getIntExtra("plugged", 0);
        iVar.gR = intent.getIntExtra("voltage", 0);
        iVar.gS = intent.getIntExtra("temperature", 0);
        iVar.gT = intent.getStringExtra("technology");
        c(iVar);
        if (iVar.level > iVar.gP) {
            e.f("BatteryInfoHelper", "Bad phone!!! battery level: " + iVar.level + ", battery scale: " + iVar.gP);
            iVar.level = iVar.gP;
        }
        if (iVar.status == 2) {
            this.at = as;
            this.ap.a(iVar.gQ, iVar.level, iVar.gP, SystemClock.elapsedRealtime());
            iVar.gW = (int) (this.ap.bK() / 1000);
        } else if (this.ao != null && this.ao.status == 2) {
            this.at = System.currentTimeMillis();
            this.ap.bJ();
        }
        iVar.gU = false;
        iVar.gV = iVar.gP < 1 ? iVar.level : (iVar.level * 100) / iVar.gP;
        if (this.ao == null || this.ao.gV != iVar.gV) {
            iVar.gX = this.aq.a((double) iVar.gV, 5);
        } else {
            iVar.gX = this.ao.gX;
        }
        e.d("BatteryInfoHelper", "battery info updated, " + b(iVar));
        a(iVar);
        this.ao = iVar;
    }

    private static String b(i iVar) {
        return "status:" + iVar.status + ", health:" + iVar.gN + ", present:" + iVar.gO + ", level:" + iVar.level + ", scale:" + iVar.gP + ", plugType:" + iVar.gQ + ", voltage:" + iVar.gR + ", temperature:" + iVar.gS + ", technology:" + iVar.gT + ", percent: " + iVar.gV + ", chargingTime: " + iVar.gW + ", remainingBatteryTime: " + iVar.gX;
    }

    private void c(i iVar) {
        if (an.equalsIgnoreCase("SCH-i909") && Build.VERSION.SDK_INT <= 8 && !Build.VERSION.RELEASE.equals("2.2.2")) {
            iVar.gP = 1000;
        }
    }

    public static int j(int i) {
        if (i <= 20) {
            return 0;
        }
        return 1;
    }

    public static int a(Context context, int i) {
        return context.getResources().getColor(am[i]);
    }
}
