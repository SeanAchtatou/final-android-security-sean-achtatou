package com.dianxinos.powermanager.c;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;

/* compiled from: CompatibilityHelper */
public class j {
    private static final int iO = Build.VERSION.SDK_INT;

    public static int cc() {
        if (iO >= 9) {
            return 0;
        }
        return 3;
    }

    public static boolean cd() {
        return iO >= 9;
    }

    public static boolean ce() {
        return iO >= 9;
    }

    public static boolean cf() {
        return iO >= 9;
    }

    public static Intent w(String str) {
        if (iO >= 9) {
            Intent intent = new Intent("android.settings.APPLICATION_DETAILS_SETTINGS");
            intent.setData(Uri.fromParts("package", str, null));
            return intent;
        }
        Intent intent2 = new Intent("android.intent.action.VIEW");
        intent2.setClassName("com.android.settings", "com.android.settings.InstalledAppDetails");
        intent2.putExtra("pkg", str);
        return intent2;
    }
}
