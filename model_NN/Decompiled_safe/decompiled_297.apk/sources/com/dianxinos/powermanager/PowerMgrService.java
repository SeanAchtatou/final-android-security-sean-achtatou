package com.dianxinos.powermanager;

import android.app.Service;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.provider.Settings;
import com.dianxinos.powermanager.b.t;
import com.dianxinos.powermanager.c.a;
import com.dianxinos.powermanager.c.d;
import com.dianxinos.powermanager.mode.i;
import java.util.Timer;
import java.util.TimerTask;

public class PowerMgrService extends Service implements d {
    /* access modifiers changed from: private */
    public boolean aG;
    private Timer aK = new Timer();
    /* access modifiers changed from: private */
    public int aL;
    /* access modifiers changed from: private */
    public int aM;
    /* access modifiers changed from: private */
    public i ab;
    /* access modifiers changed from: private */
    public int az;
    private a cP;
    private com.dianxinos.powermanager.d.d el;
    /* access modifiers changed from: private */
    public g em;
    private x en;
    private c eo;
    /* access modifiers changed from: private */
    public int ep;
    /* access modifiers changed from: private */
    public int eq;
    /* access modifiers changed from: private */
    public l er;
    private Timer es = new Timer();
    private TimerTask et = new r(this);
    t eu;
    private Timer ev = new Timer();
    private TimerTask ew = new s(this);
    private TimerTask ex = new t(this);
    /* access modifiers changed from: private */
    public Handler mHandler = new q(this);
    /* access modifiers changed from: private */
    public ContentResolver mResolver;

    public void onCreate() {
        this.ab = i.t(this);
        this.em = g.r(this);
        this.cP = a.b((Context) this);
        this.el = com.dianxinos.powermanager.d.d.s(this);
        this.el.B();
        this.er = l.v(this);
        this.mResolver = getContentResolver();
        this.en = new x(this, new Handler());
        this.en.k();
        this.ep = Settings.System.getInt(this.mResolver, "com.dianxinos.powermanager.auto_cleanup", 0);
        this.eo = new c(this, new Handler());
        this.eo.k();
        this.eq = Settings.System.getInt(this.mResolver, "com.dianxinos.powermanager.statusbar_notification", 1);
        this.es.schedule(this.et, 1000, 600000);
        this.cP.a(this);
        this.eu = t.M(this);
        this.cP.a(this.eu);
        this.ev.schedule(this.ew, t.M(this).ct(), 600000);
        this.aK.schedule(this.ex, 10000, 10800000);
    }

    public void onDestroy() {
        this.en.l();
        this.eo.l();
        this.el.C();
        super.onDestroy();
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void d(com.dianxinos.powermanager.c.i iVar) {
        this.az = iVar.gV;
        this.aM = iVar.gX;
        this.aL = iVar.gW;
        if (this.eq == 1) {
            this.aG = iVar.status == 2;
            this.em.a(this.aG, this.az, this.aG ? this.aL : this.aM, this.ab.az(), null);
        }
    }
}
