package com.dianxinos.powermanager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.dianxinos.powermanager.c.d;

/* compiled from: WidgetUpdataServiceBase */
class i extends BroadcastReceiver {
    final /* synthetic */ WidgetUpdataServiceBase dC;

    i(WidgetUpdataServiceBase widgetUpdataServiceBase) {
        this.dC = widgetUpdataServiceBase;
    }

    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action.equals("android.intent.action.SCREEN_OFF")) {
            if (this.dC.aH) {
                this.dC.aE.b((d) this.dC);
                boolean unused = this.dC.aH = false;
            }
        } else if (action.equals("android.intent.action.SCREEN_ON") && !this.dC.aH) {
            this.dC.aE.a(this.dC);
            boolean unused2 = this.dC.aH = true;
        }
    }
}
