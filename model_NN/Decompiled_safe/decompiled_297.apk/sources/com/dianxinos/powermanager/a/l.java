package com.dianxinos.powermanager.a;

import android.database.ContentObserver;
import android.os.Handler;
import android.provider.Settings;

/* compiled from: CleanupCommand */
class l extends ContentObserver {
    final /* synthetic */ s gM;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public l(s sVar, Handler handler) {
        super(handler);
        this.gM = sVar;
    }

    public void k() {
        this.gM.mResolver.registerContentObserver(Settings.System.getUriFor("com.dianxinos.powermanager.auto_cleanup"), false, this);
    }

    public void l() {
        this.gM.mResolver.unregisterContentObserver(this);
    }

    public void onChange(boolean z) {
        int unused = this.gM.ep = Settings.System.getInt(this.gM.mResolver, "com.dianxinos.powermanager.auto_cleanup", 0);
        if (this.gM.i != null) {
            this.gM.i.a(this.gM, this.gM.ep, this.gM.ep);
        }
    }
}
