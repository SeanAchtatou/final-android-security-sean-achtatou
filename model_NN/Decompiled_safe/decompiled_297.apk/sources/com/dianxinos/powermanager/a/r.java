package com.dianxinos.powermanager.a;

import java.util.Observable;
import java.util.Observer;

/* compiled from: MobileDataCommand */
final class r implements Observer {
    final /* synthetic */ z io;

    private r(z zVar) {
        this.io = zVar;
    }

    public void update(Observable observable, Object obj) {
        int i;
        boolean unused = this.io.mEnabled = this.io.g.getMobileDataEnabled();
        if (this.io.i != null) {
            i b = this.io.i;
            z zVar = this.io;
            if (this.io.mEnabled) {
                i = 1;
            } else {
                i = 0;
            }
            b.a(zVar, i, this.io.mEnabled ? 1 : 0);
        }
    }
}
