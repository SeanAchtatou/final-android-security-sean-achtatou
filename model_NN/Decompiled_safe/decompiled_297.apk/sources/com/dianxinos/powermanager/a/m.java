package com.dianxinos.powermanager.a;

import android.content.ContentResolver;
import android.content.Context;
import android.os.Handler;
import android.provider.Settings;
import com.dianxinos.powermanager.C0000R;
import java.util.ArrayList;

/* compiled from: ScreenTimeOutCommand */
public class m implements u {
    private f he;
    private int hf;
    /* access modifiers changed from: private */
    public i i;
    private Context mContext;
    /* access modifiers changed from: private */
    public int mIndex;
    private String mName;
    /* access modifiers changed from: private */
    public ContentResolver mResolver;
    private ArrayList y = new ArrayList();

    public m(Context context) {
        this.mContext = context;
        this.mResolver = context.getContentResolver();
        this.hf = Settings.System.getInt(this.mResolver, "screen_off_timeout", 30000);
        this.he = new f(this, new Handler());
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        super.finalize();
        this.he.l();
    }

    public void a(int i2) {
        try {
            Settings.System.putInt(this.mResolver, "screen_off_timeout", K(i2));
        } catch (NumberFormatException e) {
        }
    }

    public boolean g() {
        return false;
    }

    public String getValueString() {
        this.hf = Settings.System.getInt(this.mResolver, "screen_off_timeout", 30000);
        return J(this.hf);
    }

    public ArrayList h() {
        this.y.clear();
        this.y.add(this.mContext.getString(C0000R.string.mode_sreen_timeout_15sec));
        this.y.add(this.mContext.getString(C0000R.string.mode_sreen_timeout_30sec));
        this.y.add(this.mContext.getString(C0000R.string.mode_sreen_timeout_1min));
        this.y.add(this.mContext.getString(C0000R.string.mode_sreen_timeout_2min));
        this.y.add(this.mContext.getString(C0000R.string.mode_sreen_timeout_10min));
        return this.y;
    }

    private String J(int i2) {
        this.y.clear();
        this.y.add(this.mContext.getString(C0000R.string.mode_sreen_timeout_15sec));
        this.y.add(this.mContext.getString(C0000R.string.mode_sreen_timeout_30sec));
        this.y.add(this.mContext.getString(C0000R.string.mode_sreen_timeout_1min));
        this.y.add(this.mContext.getString(C0000R.string.mode_sreen_timeout_2min));
        this.y.add(this.mContext.getString(C0000R.string.mode_sreen_timeout_10min));
        if (i2 == 15000) {
            this.mIndex = 0;
            return (String) this.y.get(0);
        } else if (i2 == 30000) {
            this.mIndex = 1;
            return (String) this.y.get(1);
        } else if (i2 == 60000) {
            this.mIndex = 2;
            return (String) this.y.get(2);
        } else if (i2 == 120000) {
            this.mIndex = 3;
            return (String) this.y.get(3);
        } else {
            this.mIndex = 4;
            return (String) this.y.get(4);
        }
    }

    private int K(int i2) {
        if (i2 == 0) {
            return 15000;
        }
        if (i2 == 1) {
            return 30000;
        }
        if (i2 == 2) {
            return 60000;
        }
        if (i2 == 3) {
            return 120000;
        }
        return i2 == 4 ? 600000 : 30000;
    }

    public int i() {
        return 5;
    }

    public int getIndex() {
        this.hf = Settings.System.getInt(this.mResolver, "screen_off_timeout", 30000);
        if (this.hf == 15000) {
            this.mIndex = 0;
        } else if (this.hf == 30000) {
            this.mIndex = 1;
        } else if (this.hf == 60000) {
            this.mIndex = 2;
        } else if (this.hf == 120000) {
            this.mIndex = 3;
        } else {
            this.mIndex = 4;
        }
        return this.mIndex;
    }

    public String getName() {
        this.mName = this.mContext.getString(C0000R.string.mode_screen_timeout_item);
        return this.mName;
    }

    public void a(i iVar) {
        this.he.k();
        this.i = iVar;
    }

    public void b(i iVar) {
        this.he.l();
        this.i = null;
    }

    public int getValue() {
        this.hf = Settings.System.getInt(this.mResolver, "screen_off_timeout", 30000);
        return this.hf / 1000;
    }

    public int b(int i2) {
        if (i2 == 0) {
            return 15;
        }
        if (i2 == 1) {
            return 30;
        }
        if (i2 == 2) {
            return 60;
        }
        if (i2 == 3) {
            return 120;
        }
        if (i2 == 4) {
            return 600;
        }
        return 0;
    }

    public boolean j() {
        return true;
    }

    public String toString() {
        return "ScreenTimeOutCommand";
    }
}
