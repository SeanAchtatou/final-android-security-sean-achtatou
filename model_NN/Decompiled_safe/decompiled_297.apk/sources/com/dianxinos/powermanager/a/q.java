package com.dianxinos.powermanager.a;

import android.content.ContentResolver;
import android.content.Context;
import android.media.AudioManager;
import android.provider.Settings;
import com.dianxinos.powermanager.C0000R;
import java.util.ArrayList;

/* compiled from: VibrateCommand */
public class q implements u {
    private AudioManager hP;
    private ContentResolver mContentResolver;
    private Context mContext;
    private boolean mEnabled;
    private String mName;

    public q(Context context) {
        this.mContext = context;
        this.mContentResolver = context.getContentResolver();
        this.hP = (AudioManager) context.getSystemService("audio");
    }

    private void j(boolean z) {
        int i;
        boolean z2;
        int i2;
        int i3;
        if (z) {
            i = 1;
            z2 = true;
        } else {
            i = 0;
            z2 = false;
        }
        if (this.hP.getRingerMode() != 2) {
            AudioManager audioManager = this.hP;
            if (z2) {
                i3 = 1;
            } else {
                i3 = 0;
            }
            audioManager.setRingerMode(i3);
        }
        ContentResolver contentResolver = this.mContentResolver;
        if (z2) {
            i2 = 1;
        } else {
            i2 = 0;
        }
        Settings.System.putInt(contentResolver, "vibrate_in_silent", i2);
        this.hP.setVibrateSetting(0, i);
    }

    public void a(boolean z) {
        j(z);
    }

    public boolean g() {
        int bD = bD();
        if (bD == 1 || bD == 3) {
            this.mEnabled = true;
            return true;
        }
        this.mEnabled = false;
        return false;
    }

    public void a(int i) {
        if (i == 0) {
            a(false);
        } else {
            a(true);
        }
    }

    public String getValueString() {
        g();
        if (this.mEnabled) {
            return this.mContext.getString(C0000R.string.mode_status_on);
        }
        return this.mContext.getString(C0000R.string.mode_status_off);
    }

    public ArrayList h() {
        return null;
    }

    public int i() {
        return 2;
    }

    public int getIndex() {
        g();
        if (this.mEnabled) {
            return 1;
        }
        return 0;
    }

    private int bD() {
        boolean z = Settings.System.getInt(this.mContentResolver, "vibrate_in_silent", 1) == 1;
        int vibrateSetting = this.hP.getVibrateSetting(0);
        if (z) {
            if (vibrateSetting == 0) {
                this.hP.setVibrateSetting(0, 2);
            }
            if (vibrateSetting == 1) {
                return 1;
            }
            return 3;
        }
        if (vibrateSetting == 2) {
            this.hP.setVibrateSetting(0, 0);
        }
        if (vibrateSetting == 1) {
            return 2;
        }
        return 0;
    }

    public String getName() {
        this.mName = this.mContext.getString(C0000R.string.mode_newmode_shake_switch);
        return this.mName;
    }

    public void a(i iVar) {
    }

    public void b(i iVar) {
    }

    public int getValue() {
        return getIndex();
    }

    public int b(int i) {
        return i;
    }

    public boolean j() {
        return true;
    }

    public String toString() {
        return "VibrateCommand";
    }
}
