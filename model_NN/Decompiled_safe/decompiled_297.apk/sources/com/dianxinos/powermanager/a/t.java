package com.dianxinos.powermanager.a;

import android.database.ContentObserver;
import android.os.Handler;
import android.provider.Settings;

/* compiled from: AirplaneCommand */
class t extends ContentObserver {
    final /* synthetic */ o it;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public t(o oVar, Handler handler) {
        super(handler);
        this.it = oVar;
    }

    public void k() {
        this.it.mContentResolver.registerContentObserver(Settings.System.getUriFor("airplane_mode_on"), false, this);
    }

    public void l() {
        this.it.mContentResolver.unregisterContentObserver(this);
    }

    public void onChange(boolean z) {
        this.it.g();
        if (this.it.i != null) {
            this.it.i.a(this.it, this.it.hA, this.it.hA);
        }
    }
}
