package com.dianxinos.powermanager.a;

import android.content.Context;
import android.net.wifi.WifiManager;
import com.dianxinos.powermanager.C0000R;
import java.util.ArrayList;

/* compiled from: WifiCommand */
public class h implements u {
    /* access modifiers changed from: private */
    public WifiManager bG;
    /* access modifiers changed from: private */
    public boolean bH;
    private Context mContext;
    private boolean mEnabled;

    public h(Context context) {
        this.bG = (WifiManager) context.getSystemService("wifi");
        this.mContext = context;
    }

    public void a(boolean z) {
        this.bH = z;
        new v(this).execute(new String[0]);
    }

    public boolean g() {
        int wifiState = this.bG.getWifiState();
        if (wifiState == 3 || wifiState == 2) {
            this.mEnabled = true;
            return true;
        }
        this.mEnabled = false;
        return false;
    }

    public void a(int i) {
        if (i == 0) {
            a(false);
        } else {
            a(true);
        }
    }

    public String getValueString() {
        g();
        if (this.mEnabled) {
            return this.mContext.getString(C0000R.string.mode_status_on);
        }
        return this.mContext.getString(C0000R.string.mode_status_off);
    }

    public ArrayList h() {
        return null;
    }

    public int i() {
        return 2;
    }

    public int getIndex() {
        g();
        if (this.mEnabled) {
            return 1;
        }
        return 0;
    }

    public String getName() {
        return this.mContext.getString(C0000R.string.mode_newmode_wifinet_switch);
    }

    public void a(i iVar) {
    }

    public void b(i iVar) {
    }

    public int getValue() {
        return getIndex();
    }

    public int b(int i) {
        return i;
    }

    public boolean j() {
        return true;
    }

    public String toString() {
        return "WifiCommand";
    }
}
