package com.dianxinos.powermanager.a;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.provider.Settings;
import java.util.ArrayList;

/* compiled from: AirplaneCommand */
public class o implements u {
    /* access modifiers changed from: private */
    public int hA = Settings.System.getInt(this.mContext.getContentResolver(), "airplane_mode_on", 0);
    private t hB;
    /* access modifiers changed from: private */
    public i i;
    /* access modifiers changed from: private */
    public ContentResolver mContentResolver;
    private Context mContext;
    private boolean mEnabled;

    public o(Context context) {
        boolean z;
        this.mContext = context;
        this.mContentResolver = context.getContentResolver();
        if (this.hA == 1) {
            z = true;
        } else {
            z = false;
        }
        this.mEnabled = z;
        this.hB = new t(this, new Handler());
    }

    public void a(boolean z) {
        Settings.System.putInt(this.mContentResolver, "airplane_mode_on", z ? 1 : 0);
        Intent intent = new Intent("android.intent.action.AIRPLANE_MODE");
        intent.addFlags(536870912);
        intent.putExtra("state", z);
        this.mContext.sendBroadcast(intent);
    }

    public boolean g() {
        boolean z;
        this.hA = Settings.System.getInt(this.mContext.getContentResolver(), "airplane_mode_on", 0);
        if (this.hA == 1) {
            z = true;
        } else {
            z = false;
        }
        this.mEnabled = z;
        return this.mEnabled;
    }

    public void a(int i2) {
        if (i2 == 0) {
            a(false);
        } else {
            a(true);
        }
    }

    public String getValueString() {
        return null;
    }

    public ArrayList h() {
        return null;
    }

    public int i() {
        return 2;
    }

    public int getIndex() {
        g();
        if (this.mEnabled) {
            return 1;
        }
        return 0;
    }

    public String getName() {
        return null;
    }

    public void a(i iVar) {
        this.hB.k();
        this.i = iVar;
    }

    public void b(i iVar) {
        this.hB.l();
        this.i = null;
    }

    public int getValue() {
        return getIndex();
    }

    public int b(int i2) {
        return i2;
    }

    public boolean j() {
        return false;
    }

    public String toString() {
        return "AirplaneCommand ";
    }
}
