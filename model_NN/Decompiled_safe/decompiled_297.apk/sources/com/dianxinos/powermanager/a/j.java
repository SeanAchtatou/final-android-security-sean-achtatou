package com.dianxinos.powermanager.a;

import android.content.Context;
import android.net.ConnectivityManager;
import com.dianxinos.powermanager.C0000R;
import java.util.ArrayList;

/* compiled from: BkDataCommand */
public class j implements u {
    private boolean fr;
    private ConnectivityManager g;
    private Context mContext;
    private String mName;

    public j(Context context) {
        this.mContext = context;
        this.g = (ConnectivityManager) context.getSystemService("connectivity");
    }

    public void a(boolean z) {
        this.g.setBackgroundDataSetting(z);
    }

    public boolean g() {
        this.fr = this.g.getBackgroundDataSetting();
        return this.fr;
    }

    public void a(int i) {
        if (i == 0) {
            a(false);
        } else {
            a(true);
        }
    }

    public String getValueString() {
        if (this.fr) {
            return this.mContext.getString(C0000R.string.mode_status_on);
        }
        return this.mContext.getString(C0000R.string.mode_status_off);
    }

    public ArrayList h() {
        return null;
    }

    public int i() {
        return 2;
    }

    public int getIndex() {
        g();
        if (this.fr) {
            return 1;
        }
        return 0;
    }

    public String getName() {
        this.mName = this.mContext.getString(C0000R.string.mode_newmode_bkdata_switch);
        return this.mName;
    }

    public void a(i iVar) {
    }

    public void b(i iVar) {
    }

    public int getValue() {
        return getIndex();
    }

    public int b(int i) {
        return i;
    }

    public boolean j() {
        return false;
    }

    public String toString() {
        return "BkDataCommand ";
    }
}
