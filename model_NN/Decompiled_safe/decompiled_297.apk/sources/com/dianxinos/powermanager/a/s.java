package com.dianxinos.powermanager.a;

import android.content.ContentResolver;
import android.content.Context;
import android.os.Handler;
import android.provider.Settings;
import com.dianxinos.powermanager.C0000R;
import java.util.ArrayList;

/* compiled from: CleanupCommand */
public class s implements u {
    private boolean U;
    /* access modifiers changed from: private */
    public int ep = Settings.System.getInt(this.mResolver, "com.dianxinos.powermanager.auto_cleanup", 0);
    /* access modifiers changed from: private */
    public i i;
    private l ip = new l(this, new Handler());
    private Context mContext;
    private String mName;
    /* access modifiers changed from: private */
    public ContentResolver mResolver;

    public s(Context context) {
        this.mContext = context;
        this.mResolver = context.getContentResolver();
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        super.finalize();
        this.ip.l();
    }

    public void a(boolean z) {
        Settings.System.putInt(this.mResolver, "com.dianxinos.powermanager.auto_cleanup", z ? 1 : 0);
    }

    public boolean g() {
        this.ep = Settings.System.getInt(this.mResolver, "com.dianxinos.powermanager.auto_cleanup", 0);
        if (this.ep == 1) {
            this.U = true;
        } else {
            this.U = false;
        }
        return this.U;
    }

    public void a(int i2) {
        if (i2 == 0) {
            a(false);
        } else {
            a(true);
        }
    }

    public String getValueString() {
        g();
        if (this.U) {
            return this.mContext.getString(C0000R.string.mode_status_on);
        }
        return this.mContext.getString(C0000R.string.mode_status_off);
    }

    public ArrayList h() {
        return null;
    }

    public int i() {
        return 2;
    }

    public int getIndex() {
        g();
        if (this.U) {
            return 1;
        }
        return 0;
    }

    public void a(i iVar) {
        this.ip.k();
        this.i = iVar;
    }

    public String getName() {
        this.mName = this.mContext.getString(C0000R.string.mode_auto_cleanup_bkapp);
        return this.mName;
    }

    public void b(i iVar) {
        this.ip.l();
        this.i = null;
    }

    public int getValue() {
        return getIndex();
    }

    public int b(int i2) {
        return i2;
    }

    public boolean j() {
        return true;
    }

    public String toString() {
        return "CleanupCommand ";
    }
}
