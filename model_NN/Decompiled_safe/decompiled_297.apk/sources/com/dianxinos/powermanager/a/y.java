package com.dianxinos.powermanager.a;

import android.database.ContentObserver;
import android.os.Handler;
import android.provider.Settings;

/* compiled from: BrightnessCommand */
class y extends ContentObserver {
    final /* synthetic */ d kV;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public y(d dVar, Handler handler) {
        super(handler);
        this.kV = dVar;
    }

    public void k() {
        this.kV.mContentResolver.registerContentObserver(Settings.System.getUriFor("screen_brightness"), false, this);
        this.kV.mContentResolver.registerContentObserver(Settings.System.getUriFor("screen_brightness_mode"), false, this);
    }

    public void l() {
        this.kV.mContentResolver.unregisterContentObserver(this);
    }

    public void onChange(boolean z) {
        this.kV.m();
        if (this.kV.i != null) {
            this.kV.i.a(this.kV, this.kV.mIndex, this.kV.getValue());
        }
    }
}
