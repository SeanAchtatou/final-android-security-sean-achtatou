package com.dianxinos.powermanager.a;

import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;
import com.dianxinos.powermanager.C0000R;
import java.util.ArrayList;

/* compiled from: GpsCommand */
public class g implements u {
    private ContentResolver mContentResolver;
    private Context mContext;
    private boolean mEnabled;
    private String mName;

    public g(Context context) {
        this.mContentResolver = context.getContentResolver();
        this.mContext = context;
    }

    public void a(boolean z) {
        Intent intent = new Intent();
        intent.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
        intent.addCategory("android.intent.category.ALTERNATIVE");
        intent.setData(Uri.parse("custom:3"));
        try {
            PendingIntent.getBroadcast(this.mContext, 0, intent, 0).send();
        } catch (PendingIntent.CanceledException e) {
            e.printStackTrace();
        }
    }

    public boolean g() {
        this.mEnabled = Settings.Secure.isLocationProviderEnabled(this.mContentResolver, "gps");
        return this.mEnabled;
    }

    public void a(int i) {
        if (i == 0) {
            a(false);
        } else {
            a(true);
        }
    }

    public String getValueString() {
        g();
        if (this.mEnabled) {
            return this.mContext.getString(C0000R.string.mode_status_on);
        }
        return this.mContext.getString(C0000R.string.mode_status_off);
    }

    public ArrayList h() {
        return null;
    }

    public int i() {
        return 2;
    }

    public int getIndex() {
        g();
        if (this.mEnabled) {
            return 1;
        }
        return 0;
    }

    public String getName() {
        this.mName = this.mContext.getString(C0000R.string.mode_newmode_gps_switch);
        return this.mName;
    }

    public void a(i iVar) {
    }

    public void b(i iVar) {
    }

    public int getValue() {
        return getIndex();
    }

    public int b(int i) {
        return i;
    }

    public boolean j() {
        return false;
    }

    public String toString() {
        return "GpsCommand ";
    }
}
