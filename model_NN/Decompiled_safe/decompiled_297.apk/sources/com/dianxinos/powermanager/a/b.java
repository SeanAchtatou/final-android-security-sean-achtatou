package com.dianxinos.powermanager.a;

import android.database.ContentObserver;
import android.os.Handler;
import android.provider.Settings;

/* compiled from: HapticFeedbackCommand */
class b extends ContentObserver {
    final /* synthetic */ x l;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public b(x xVar, Handler handler) {
        super(handler);
        this.l = xVar;
    }

    public void k() {
        this.l.mResolver.registerContentObserver(Settings.System.getUriFor("haptic_feedback_enabled"), false, this);
    }

    public void l() {
        this.l.mResolver.unregisterContentObserver(this);
    }

    public void onChange(boolean z) {
        this.l.getValueString();
        if (this.l.i != null) {
            this.l.i.a(this.l, this.l.kQ, this.l.kQ);
        }
    }
}
