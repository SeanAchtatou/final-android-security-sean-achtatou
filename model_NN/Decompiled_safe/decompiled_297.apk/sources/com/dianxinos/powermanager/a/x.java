package com.dianxinos.powermanager.a;

import android.content.ContentResolver;
import android.content.Context;
import android.os.Handler;
import android.provider.Settings;
import com.dianxinos.powermanager.C0000R;
import java.util.ArrayList;

/* compiled from: HapticFeedbackCommand */
public class x implements u {
    /* access modifiers changed from: private */
    public i i;
    /* access modifiers changed from: private */
    public int kQ = Settings.System.getInt(this.mResolver, "haptic_feedback_enabled", 0);
    private b kR = new b(this, new Handler());
    private Context mContext;
    private String mName;
    /* access modifiers changed from: private */
    public ContentResolver mResolver;

    public x(Context context) {
        this.mContext = context;
        this.mResolver = context.getContentResolver();
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        super.finalize();
        this.kR.l();
    }

    public void a(boolean z) {
        Settings.System.putInt(this.mResolver, "haptic_feedback_enabled", z ? 1 : 0);
    }

    public boolean g() {
        this.kQ = Settings.System.getInt(this.mResolver, "haptic_feedback_enabled", 0);
        if (this.kQ == 1) {
            return true;
        }
        return false;
    }

    public void a(int i2) {
        if (i2 == 0) {
            a(false);
        } else {
            a(true);
        }
    }

    public String getValueString() {
        g();
        if (this.kQ == 1) {
            return this.mContext.getString(C0000R.string.mode_status_on);
        }
        return this.mContext.getString(C0000R.string.mode_status_off);
    }

    public ArrayList h() {
        return null;
    }

    public int i() {
        return 2;
    }

    public int getIndex() {
        g();
        if (this.kQ == 1) {
            return 1;
        }
        return 0;
    }

    public String getName() {
        this.mName = this.mContext.getString(C0000R.string.mode_newmode_touchfbk_switch);
        return this.mName;
    }

    public void a(i iVar) {
        this.kR.k();
        this.i = iVar;
    }

    public void b(i iVar) {
        this.kR.l();
        this.i = null;
    }

    public int getValue() {
        return getIndex();
    }

    public int b(int i2) {
        return i2;
    }

    public boolean j() {
        return true;
    }

    public String toString() {
        return "HapticFeedbackCommand ";
    }
}
