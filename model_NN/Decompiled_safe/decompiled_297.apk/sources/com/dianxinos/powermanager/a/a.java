package com.dianxinos.powermanager.a;

import android.content.ContentResolver;
import android.content.Context;
import android.content.SyncStatusObserver;
import android.net.ConnectivityManager;
import android.os.Handler;
import com.dianxinos.powermanager.C0000R;
import java.util.ArrayList;

/* compiled from: AutoSyncCommand */
public class a implements u {
    private ConnectivityManager g;
    /* access modifiers changed from: private */
    public boolean h;
    /* access modifiers changed from: private */
    public i i;
    private Object j;
    private SyncStatusObserver k = new p(this);
    private Context mContext;
    /* access modifiers changed from: private */
    public final Handler mHandler = new Handler();
    private String mName;

    public a(Context context) {
        this.mContext = context;
        this.g = (ConnectivityManager) context.getSystemService("connectivity");
    }

    public void a(boolean z) {
        ContentResolver.setMasterSyncAutomatically(z);
    }

    public boolean g() {
        this.g.getBackgroundDataSetting();
        this.h = ContentResolver.getMasterSyncAutomatically();
        return this.h;
    }

    public void a(int i2) {
        if (i2 == 0) {
            a(false);
        } else {
            a(true);
        }
    }

    public String getValueString() {
        g();
        if (this.h) {
            return this.mContext.getString(C0000R.string.mode_status_on);
        }
        return this.mContext.getString(C0000R.string.mode_status_off);
    }

    public ArrayList h() {
        return null;
    }

    public int i() {
        return 2;
    }

    public int getIndex() {
        g();
        if (this.h) {
            return 1;
        }
        return 0;
    }

    public String getName() {
        this.mName = this.mContext.getString(C0000R.string.mode_newmode_autosync_switch);
        return this.mName;
    }

    public void a(i iVar) {
        this.j = ContentResolver.addStatusChangeListener(13, this.k);
        this.i = iVar;
    }

    public void b(i iVar) {
        ContentResolver.removeStatusChangeListener(this.j);
        this.i = null;
    }

    public int getValue() {
        return getIndex();
    }

    public int b(int i2) {
        return i2;
    }

    public boolean j() {
        return true;
    }

    public String toString() {
        return "AutoSyncCommand ";
    }
}
