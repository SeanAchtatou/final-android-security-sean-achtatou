package com.dianxinos.powermanager.a;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.dianxinos.powermanager.C0000R;
import java.util.ArrayList;

/* compiled from: Only2GCommand */
public class k implements u {
    private boolean fY;
    private BroadcastReceiver fZ = new e(this);
    private i i;
    private Context mContext;

    public k(Context context) {
        this.mContext = context;
        aY();
    }

    public void a(boolean z) {
        f(z);
    }

    public boolean g() {
        return this.fY;
    }

    public void a(int i2) {
        if (i2 == 0) {
            a(false);
        } else {
            a(true);
        }
    }

    public String getValueString() {
        g();
        if (this.fY) {
            return this.mContext.getString(C0000R.string.mode_status_on);
        }
        return this.mContext.getString(C0000R.string.mode_status_off);
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        super.finalize();
        aX();
    }

    public ArrayList h() {
        return null;
    }

    public int i() {
        return 2;
    }

    public int getIndex() {
        g();
        if (this.fY) {
            return 1;
        }
        return 0;
    }

    public String getName() {
        return this.mContext.getString(C0000R.string.mode_only2g_item);
    }

    public int getValue() {
        return getIndex();
    }

    public int b(int i2) {
        return i2;
    }

    public void a(i iVar) {
        this.i = iVar;
    }

    public void b(i iVar) {
        this.i = null;
    }

    public boolean j() {
        return false;
    }

    public String toString() {
        return "Only2GCommand";
    }

    private void f(boolean z) {
        Intent intent = new Intent("dianxinos.intent.action.ACTION_SET_PREFERRED_NETWORK");
        intent.putExtra("prefer2G", z);
        this.mContext.sendBroadcast(intent);
    }

    private void aX() {
        this.mContext.unregisterReceiver(this.fZ);
    }

    private void aY() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("dianxinos.intent.action.ACTION_PREFERRED_NETWORK_STATE");
        Intent registerReceiver = this.mContext.registerReceiver(this.fZ, intentFilter);
        if (registerReceiver != null) {
            d(registerReceiver);
        }
    }

    /* access modifiers changed from: private */
    public void d(Intent intent) {
        if (intent.getAction().equals("dianxinos.intent.action.ACTION_PREFERRED_NETWORK_STATE")) {
            if (intent.getIntExtra("prefer2G", -1) == 0) {
                this.fY = false;
            } else if (intent.getIntExtra("prefer2G", -1) == 1) {
                this.fY = true;
            }
            if (this.i == null) {
                return;
            }
            if (this.fY) {
                this.i.a(this, 1, 1);
            } else {
                this.i.a(this, 0, 0);
            }
        }
    }
}
