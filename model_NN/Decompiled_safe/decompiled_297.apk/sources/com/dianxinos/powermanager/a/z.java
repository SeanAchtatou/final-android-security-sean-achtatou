package com.dianxinos.powermanager.a;

import android.content.ContentQueryMap;
import android.content.ContentResolver;
import android.content.Context;
import android.net.ConnectivityManager;
import android.provider.Settings;
import com.dianxinos.powermanager.C0000R;
import com.dianxinos.powermanager.c.j;
import java.util.ArrayList;

/* compiled from: MobileDataCommand */
public class z implements u {
    /* access modifiers changed from: private */
    public ConnectivityManager g;
    /* access modifiers changed from: private */
    public i i;
    private ContentQueryMap lJ = new ContentQueryMap(this.mContentResolver.query(Settings.Secure.CONTENT_URI, null, "(name=?)", new String[]{"mobile_data"}, null), "name", true, null);
    private r lK = new r(this);
    private ContentResolver mContentResolver;
    private Context mContext;
    /* access modifiers changed from: private */
    public boolean mEnabled = this.g.getMobileDataEnabled();
    private String mName;

    public z(Context context) {
        this.mContext = context;
        this.g = (ConnectivityManager) context.getSystemService("connectivity");
        this.mContentResolver = context.getContentResolver();
    }

    public void a(boolean z) {
        this.g.setMobileDataEnabled(z);
    }

    public boolean g() {
        this.mEnabled = this.g.getMobileDataEnabled();
        return this.mEnabled;
    }

    public void a(int i2) {
        if (i2 == 0) {
            a(false);
        } else {
            a(true);
        }
    }

    public String getValueString() {
        this.mEnabled = this.g.getMobileDataEnabled();
        if (this.mEnabled) {
            return this.mContext.getString(C0000R.string.mode_status_on);
        }
        return this.mContext.getString(C0000R.string.mode_status_off);
    }

    public ArrayList h() {
        return null;
    }

    public int i() {
        return 2;
    }

    public int getIndex() {
        g();
        if (this.mEnabled) {
            return 1;
        }
        return 0;
    }

    public String getName() {
        this.mName = this.mContext.getString(C0000R.string.mode_newmode_mobiledata_switch);
        return this.mName;
    }

    public void a(i iVar) {
        this.lJ.addObserver(this.lK);
        this.i = iVar;
    }

    public void b(i iVar) {
        this.lJ.deleteObserver(this.lK);
        this.i = null;
    }

    public int getValue() {
        return getIndex();
    }

    public int b(int i2) {
        return i2;
    }

    public boolean j() {
        if (j.cf()) {
            return true;
        }
        return false;
    }

    public String toString() {
        return "MobileDataCommand";
    }
}
