package com.dianxinos.powermanager.a;

import android.database.ContentObserver;
import android.os.Handler;
import android.provider.Settings;

/* compiled from: ScreenTimeOutCommand */
class f extends ContentObserver {
    final /* synthetic */ m bi;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public f(m mVar, Handler handler) {
        super(handler);
        this.bi = mVar;
    }

    public void k() {
        this.bi.mResolver.registerContentObserver(Settings.System.getUriFor("screen_off_timeout"), false, this);
    }

    public void l() {
        this.bi.mResolver.unregisterContentObserver(this);
    }

    public void onChange(boolean z) {
        this.bi.getValueString();
        if (this.bi.i != null) {
            this.bi.i.a(this.bi, this.bi.mIndex, this.bi.getValue());
        }
    }
}
