package com.dianxinos.powermanager.a;

import android.content.ContentResolver;
import android.content.Context;
import android.os.Handler;
import android.os.PowerManager;
import android.provider.Settings;
import com.dianxinos.powermanager.C0000R;
import java.util.ArrayList;

/* compiled from: BrightnessCommand */
public class d implements u {
    private static final int[] z = {10, 30, 50, 100, -1};
    private boolean A;
    private int B;
    /* access modifiers changed from: private */
    public i i;
    /* access modifiers changed from: private */
    public ContentResolver mContentResolver;
    private Context mContext;
    /* access modifiers changed from: private */
    public int mIndex = 0;
    private String mName;
    private String mValue;
    private y w;
    private PowerManager x;
    private ArrayList y;

    public d(Context context) {
        this.mContext = context;
        this.mContentResolver = context.getContentResolver();
        this.x = (PowerManager) context.getSystemService("power");
        this.w = new y(this, new Handler());
        this.y = new ArrayList();
        this.y.add(context.getString(C0000R.string.mode_brightness_percent_10));
        this.y.add(context.getString(C0000R.string.mode_brightness_percent_30));
        this.y.add(context.getString(C0000R.string.mode_brightness_percent_50));
        this.y.add(context.getString(C0000R.string.mode_brightness_percent_100));
        this.y.add(context.getString(C0000R.string.mode_setting_value_auto));
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        super.finalize();
        this.w.l();
    }

    public void m() {
        this.A = false;
        this.B = -1;
        if (Settings.System.getInt(this.mContentResolver, "screen_brightness_mode", 1) == 1) {
            this.A = true;
            this.mIndex = 4;
        }
        if (!this.A) {
            this.B = Settings.System.getInt(this.mContentResolver, "screen_brightness", 255);
            f(this.B);
        }
    }

    public void a(int i2) {
        int i3;
        if (i2 == 1) {
            i3 = 30;
        } else if (i2 == 2) {
            i3 = 50;
        } else if (i2 == 3) {
            i3 = 100;
        } else {
            i3 = 0;
        }
        if (i2 == 4) {
            i3 = -1;
        }
        Settings.System.getInt(this.mContentResolver, "screen_brightness", 255);
        if (i3 == -1) {
            this.mIndex = 4;
            this.A = true;
            Settings.System.putInt(this.mContentResolver, "screen_brightness_mode", 1);
            return;
        }
        Settings.System.putInt(this.mContentResolver, "screen_brightness_mode", 0);
        Settings.System.putInt(this.mContentResolver, "screen_brightness", ((i3 * 225) / 100) + 30);
    }

    public boolean g() {
        return false;
    }

    public String getValueString() {
        m();
        if (this.A) {
            this.y.set(4, this.mContext.getString(C0000R.string.mode_setting_value_auto));
            return (String) this.y.get(4);
        }
        this.B = Settings.System.getInt(this.mContentResolver, "screen_brightness", 255);
        f(this.B);
        return this.mValue;
    }

    private void f(int i2) {
        if (this.B >= 250) {
            this.mIndex = 3;
            this.mValue = (String) this.y.get(3);
        } else if (this.B >= 120) {
            this.mIndex = 2;
            this.mValue = (String) this.y.get(2);
        } else if (this.B >= 60) {
            this.mIndex = 1;
            this.mValue = (String) this.y.get(1);
        } else {
            this.mIndex = 0;
            this.mValue = (String) this.y.get(0);
        }
    }

    public ArrayList h() {
        this.y.set(4, this.mContext.getString(C0000R.string.mode_setting_value_auto));
        return this.y;
    }

    public int i() {
        return 5;
    }

    public int getIndex() {
        m();
        if (!this.A) {
            return this.mIndex;
        }
        this.mIndex = 4;
        return 4;
    }

    public void a(i iVar) {
        this.w.k();
        this.i = iVar;
    }

    public String getName() {
        this.mName = this.mContext.getString(C0000R.string.mode_light_item);
        return this.mName;
    }

    public void b(i iVar) {
        this.w.l();
        this.i = null;
    }

    public int getValue() {
        return z[getIndex()];
    }

    public int b(int i2) {
        return z[i2];
    }

    public boolean j() {
        return true;
    }

    public String toString() {
        return "BrightnessCommand ";
    }
}
