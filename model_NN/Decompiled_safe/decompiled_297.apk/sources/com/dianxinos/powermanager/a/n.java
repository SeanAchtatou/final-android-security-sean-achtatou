package com.dianxinos.powermanager.a;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import com.dianxinos.powermanager.C0000R;
import com.dianxinos.powermanager.c.e;
import java.util.ArrayList;

/* compiled from: BluetoothCommand */
public class n implements u {
    private boolean U;
    private BluetoothAdapter ht = BluetoothAdapter.getDefaultAdapter();
    private Context mContext;
    private String mName;

    public n(Context context) {
        this.mContext = context;
        if (this.ht == null) {
            e.e("BluetoothCommand", "Running on emulator? Or your device doesn't support bluetooth?");
        }
    }

    public void a(boolean z) {
        if (z) {
            this.ht.enable();
        } else {
            this.ht.disable();
        }
    }

    public boolean g() {
        int state = this.ht.getState();
        if (state == 11 || state == 12) {
            this.U = true;
        } else {
            this.U = false;
        }
        return this.U;
    }

    public void a(int i) {
        if (i == 0) {
            a(false);
        } else {
            a(true);
        }
    }

    public String getValueString() {
        g();
        if (this.U) {
            return this.mContext.getString(C0000R.string.mode_status_on);
        }
        return this.mContext.getString(C0000R.string.mode_status_off);
    }

    public ArrayList h() {
        return null;
    }

    public int i() {
        return 2;
    }

    public int getIndex() {
        g();
        if (this.U) {
            return 1;
        }
        return 0;
    }

    public String getName() {
        this.mName = this.mContext.getString(C0000R.string.mode_newmode_bluetooth_switch);
        return this.mName;
    }

    public void a(i iVar) {
    }

    public void b(i iVar) {
    }

    public int getValue() {
        return getIndex();
    }

    public int b(int i) {
        return i;
    }

    public boolean j() {
        return true;
    }

    public String toString() {
        return "BluetoothCommand ";
    }
}
