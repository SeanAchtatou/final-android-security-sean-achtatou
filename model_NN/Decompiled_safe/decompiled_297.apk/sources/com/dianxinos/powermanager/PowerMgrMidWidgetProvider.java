package com.dianxinos.powermanager;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.widget.RemoteViews;
import android.widget.Toast;
import com.dianxinos.a.a.a;
import com.dianxinos.powermanager.c.f;

public class PowerMgrMidWidgetProvider extends AppWidgetProvider {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.dianxinos.a.a.a.a(java.lang.String, java.lang.String, java.lang.Number):boolean
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.dianxinos.a.a.a.a(java.lang.String, int, java.lang.Object):boolean
      com.dianxinos.a.a.a.a(java.lang.String, java.lang.String, java.lang.Number):boolean */
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] iArr) {
        context.startService(new Intent(context, UpdataService.class));
        a F = a.F(context);
        F.init();
        F.a("widget14", "add", (Number) 1);
        F.destroy();
    }

    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("com.dianxinos.powermanager.ONEKEY")) {
            context.startService(new Intent(context, UpdataService.class));
            u(context);
        }
        super.onReceive(context, intent);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.dianxinos.a.a.a.a(java.lang.String, java.lang.String, java.lang.Number):boolean
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.dianxinos.a.a.a.a(java.lang.String, int, java.lang.Object):boolean
      com.dianxinos.a.a.a.a(java.lang.String, java.lang.String, java.lang.Number):boolean */
    public void onDeleted(Context context, int[] iArr) {
        a F = a.F(context);
        F.init();
        F.a("widget14", "delete", (Number) 1);
        F.destroy();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.dianxinos.a.a.a.a(java.lang.String, java.lang.String, java.lang.Number):boolean
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.dianxinos.a.a.a.a(java.lang.String, int, java.lang.Object):boolean
      com.dianxinos.a.a.a.a(java.lang.String, java.lang.String, java.lang.Number):boolean */
    private void u(Context context) {
        int i;
        com.dianxinos.powermanager.c.a b = com.dianxinos.powermanager.c.a.b(context);
        int A = b.A();
        l v = l.v(context);
        if (!v.aI()) {
            Toast.makeText(context, (int) C0000R.string.onekey_not_allowed, 0).show();
            return;
        }
        int d = v.d(300, true);
        int z = b.z();
        if (A <= 0 || z <= 0) {
            i = 0;
        } else {
            i = z - A;
        }
        v.f(d, i);
        a F = a.F(context);
        F.init();
        F.a("widget14", "onekey", (Number) 1);
        F.a("clicks", "one_key", (Number) 1);
        F.destroy();
    }

    public void onDisabled(Context context) {
        context.stopService(new Intent(context, UpdataService.class));
    }

    public class UpdataService extends WidgetUpdataServiceBase {
        public void onCreate() {
            super.onCreate();
        }

        public int onStartCommand(Intent intent, int i, int i2) {
            return super.onStartCommand(intent, i, i2);
        }

        /* access modifiers changed from: protected */
        public RemoteViews H() {
            RemoteViews remoteViews = new RemoteViews(getPackageName(), (int) C0000R.layout.power_mid_widget);
            Intent intent = new Intent(this, PowerMgrActivity.class);
            intent.putExtra("From", 2);
            intent.setAction("com.dianxinos.powermanager.MidWidget");
            remoteViews.setOnClickPendingIntent(C0000R.id.battery_percent_digit, PendingIntent.getActivity(this, 0, intent, 134217728));
            remoteViews.setOnClickPendingIntent(C0000R.id.onkey_button, PendingIntent.getBroadcast(this, 0, new Intent("com.dianxinos.powermanager.ONEKEY"), 0));
            return remoteViews;
        }

        /* access modifiers changed from: protected */
        public void a(RemoteViews remoteViews) {
            int i = C0000R.drawable.widget_mid_percent_high;
            if (this.aF == 1) {
                i = C0000R.drawable.widget_mid_percent_low;
            }
            Resources resources = getResources();
            int dimension = (int) resources.getDimension(C0000R.dimen.widget_mid_height);
            int dimension2 = (((int) resources.getDimension(C0000R.dimen.widget_mid_width)) * this.az) / 100;
            if (dimension2 == 0) {
                dimension2++;
            }
            try {
                remoteViews.setImageViewBitmap(C0000R.id.battery_percent, new f(resources, i, dimension2, dimension).aD());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        /* access modifiers changed from: protected */
        public void b(RemoteViews remoteViews) {
            if (this.aG) {
                remoteViews.setImageViewResource(C0000R.id.charging_sign, C0000R.drawable.widget_charging);
                remoteViews.setViewVisibility(C0000R.id.charging_sign, 0);
                remoteViews.setViewVisibility(C0000R.id.battery_digit, 8);
                return;
            }
            remoteViews.setTextViewText(C0000R.id.battery_digit, this.az + "%");
            remoteViews.setViewVisibility(C0000R.id.battery_digit, 0);
            remoteViews.setViewVisibility(C0000R.id.charging_sign, 8);
        }

        /* access modifiers changed from: protected */
        public void d(RemoteViews remoteViews) {
            remoteViews.setImageViewResource(C0000R.id.left_sign, aP[this.aF]);
            remoteViews.setImageViewResource(C0000R.id.right_sign, aP[this.aF]);
        }

        /* access modifiers changed from: protected */
        public void e(RemoteViews remoteViews) {
            AppWidgetManager.getInstance(this).updateAppWidget(new ComponentName(this, PowerMgrMidWidgetProvider.class), remoteViews);
        }
    }
}
