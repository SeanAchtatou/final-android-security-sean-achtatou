package com.dianxinos.powermanager.update;

import android.app.NotificationManager;
import android.view.View;

/* compiled from: DownloadActivity */
class h implements View.OnClickListener {
    final /* synthetic */ DownloadActivity gE;

    h(DownloadActivity downloadActivity) {
        this.gE = downloadActivity;
    }

    public void onClick(View view) {
        this.gE.ay.ad();
        NotificationManager notificationManager = (NotificationManager) this.gE.getSystemService("notification");
        notificationManager.cancel(1);
        notificationManager.cancel(3);
        i.bR();
        this.gE.finish();
    }
}
