package com.dianxinos.powermanager.update;

import android.view.View;

/* compiled from: UpdateDialog */
class c implements View.OnClickListener {
    private View.OnClickListener aD;
    final /* synthetic */ d f;

    public c(d dVar, View.OnClickListener onClickListener) {
        this.f = dVar;
        this.aD = onClickListener;
    }

    public void onClick(View view) {
        this.f.dismiss();
        this.aD.onClick(view);
    }
}
