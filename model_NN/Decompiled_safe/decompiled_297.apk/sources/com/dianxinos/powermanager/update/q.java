package com.dianxinos.powermanager.update;

import android.os.Handler;
import android.os.Message;

/* compiled from: UpdateHelper */
class q extends Handler {
    final /* synthetic */ i ju;

    private q(i iVar) {
        this.ju = iVar;
    }

    /* synthetic */ q(i iVar, p pVar) {
        this(iVar);
    }

    public void handleMessage(Message message) {
        switch (message.what) {
            case 1:
                this.ju.ca();
                return;
            case 2:
                this.ju.cb();
                return;
            case 3:
                b bVar = (b) message.obj;
                this.ju.l(bVar.mVersionName, bVar.ae);
                return;
            case 4:
                this.ju.v(((b) message.obj).mVersionName);
                return;
            default:
                return;
        }
    }
}
