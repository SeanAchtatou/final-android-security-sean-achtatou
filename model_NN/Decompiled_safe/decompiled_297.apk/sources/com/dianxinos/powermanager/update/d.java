package com.dianxinos.powermanager.update;

import android.app.Dialog;
import android.content.Context;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.dianxinos.powermanager.C0000R;

/* compiled from: UpdateDialog */
public class d extends Dialog {
    public d(Context context) {
        super(context, C0000R.style.ShowDialogStyle);
        setContentView((int) C0000R.layout.update_dialog);
    }

    public void setTitle(int i) {
        ((TextView) findViewById(C0000R.id.title)).setText(i);
    }

    public void B(int i) {
        ((TextView) findViewById(C0000R.id.message)).setText(i);
    }

    public void setMessage(String str) {
        ((TextView) findViewById(C0000R.id.message)).setText(str);
    }

    public void a(int i, View.OnClickListener onClickListener) {
        Button button = (Button) findViewById(C0000R.id.ok);
        button.setVisibility(0);
        if (i > 0) {
            button.setText(i);
        }
        if (onClickListener != null) {
            button.setOnClickListener(new c(this, onClickListener));
        } else {
            button.setOnClickListener(new a(this));
        }
    }

    public void b(int i, View.OnClickListener onClickListener) {
        Button button = (Button) findViewById(C0000R.id.cancel);
        button.setVisibility(0);
        if (i > 0) {
            button.setText(i);
        }
        if (onClickListener != null) {
            button.setOnClickListener(new c(this, onClickListener));
        } else {
            button.setOnClickListener(new a(this));
        }
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        if (keyEvent.getKeyCode() == 84) {
            return true;
        }
        return super.dispatchKeyEvent(keyEvent);
    }
}
