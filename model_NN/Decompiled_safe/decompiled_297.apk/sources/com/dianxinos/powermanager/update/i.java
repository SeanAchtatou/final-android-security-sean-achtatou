package com.dianxinos.powermanager.update;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Message;
import android.telephony.TelephonyManager;
import com.dianxinos.appupdate.aa;
import com.dianxinos.appupdate.ab;
import com.dianxinos.appupdate.j;
import com.dianxinos.appupdate.k;
import com.dianxinos.appupdate.u;
import com.dianxinos.appupdate.w;
import com.dianxinos.powermanager.C0000R;
import com.dianxinos.powermanager.c.e;
import java.util.Map;

/* compiled from: UpdateHelper */
public class i implements com.dianxinos.appupdate.i, j {
    /* access modifiers changed from: private */
    public static boolean iK = false;
    private static boolean iL = false;
    private k ay;
    private q iG = new q(this, null);
    private Dialog iH;
    /* access modifiers changed from: private */
    public e iI;
    private boolean iJ;
    private Activity mActivity;

    private i(Activity activity) {
        this.mActivity = activity;
        this.ay = k.d(this.mActivity.getApplicationContext());
        this.iI = new e(this.mActivity);
    }

    public static void bR() {
        iL = false;
    }

    public static void a(Activity activity) {
        new i(activity).bT();
    }

    public static void bS() {
        iK = false;
    }

    public void bT() {
        if (!iL && bV() && this.iI.bh()) {
            this.iI.h(false);
            this.iJ = false;
            bY();
        }
    }

    public static void b(Activity activity) {
        new i(activity).bU();
    }

    public void bU() {
        if (iL) {
            e.e("UpdateHelper", "upgrade is in progress");
        } else if (!bV()) {
            e.c("UpdateHelper", "no network available");
            d dVar = new d(this.mActivity);
            dVar.setTitle((int) C0000R.string.update_title_tip);
            dVar.B(C0000R.string.update_msg_no_network_availabel);
            dVar.a(0, null);
            dVar.show();
        } else {
            this.iI.h(false);
            this.iJ = true;
            bY();
        }
    }

    private boolean bV() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.mActivity.getSystemService("connectivity")).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void bW() {
        ProgressDialog progressDialog = new ProgressDialog(this.mActivity);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(this.mActivity.getString(C0000R.string.update_msg_checking_updates));
        progressDialog.setOnKeyListener(new p(this));
        progressDialog.show();
        this.iH = progressDialog;
    }

    private void bX() {
        if (this.iH != null) {
            this.iH.dismiss();
        }
    }

    /* access modifiers changed from: private */
    public void bY() {
        if (!iK) {
            iK = true;
            if (this.iJ) {
                bW();
            }
            String i = i(this.mActivity);
            String x = x(this.mActivity);
            this.ay.a(this, new aa(new ab(i), new w(x)));
        }
    }

    /* access modifiers changed from: private */
    public void bZ() {
        if (!this.ay.af()) {
            this.ay.a((u) null);
        } else {
            this.ay.a((com.dianxinos.appupdate.i) this);
        }
    }

    public void K() {
        iK = false;
        if (this.iJ) {
            e.c("UpdateHelper", "Failed to check updates because of network error");
            this.iG.sendEmptyMessage(1);
        }
    }

    /* access modifiers changed from: private */
    public void ca() {
        bX();
        d dVar = new d(this.mActivity);
        dVar.setTitle((int) C0000R.string.update_title_checking_failure);
        dVar.B(C0000R.string.update_msg_network_busy);
        dVar.a(C0000R.string.update_retry, new n(this));
        dVar.b(0, null);
        dVar.show();
    }

    public void L() {
        iK = false;
        if (this.iJ) {
            e.c("UpdateHelper", "No update available");
        }
        if (this.iJ) {
            this.iG.sendEmptyMessage(2);
        }
        this.iI.g(false);
    }

    /* access modifiers changed from: private */
    public void cb() {
        bX();
        d dVar = new d(this.mActivity);
        dVar.setTitle((int) C0000R.string.update_title_update_tip);
        dVar.B(C0000R.string.update_msg_no_updates);
        dVar.a(0, null);
        dVar.show();
    }

    public void a(int i, String str, String str2, int i2, Map map) {
        bX();
        e.c("UpdateHelper", "update available, version code:" + i + ", version name:" + str + ", priority:" + i2);
        b bVar = new b(null);
        bVar.mVersionName = str;
        bVar.ae = str2;
        Message message = new Message();
        message.obj = bVar;
        if (i2 > 0 || this.iI.bg()) {
            message.what = 4;
            this.iG.sendMessage(message);
            this.iI.g(true);
            return;
        }
        message.what = 3;
        this.iG.sendMessage(message);
    }

    /* access modifiers changed from: private */
    public void l(String str, String str2) {
        if (this.mActivity.isFinishing()) {
            iK = false;
            return;
        }
        String string = this.mActivity.getString(C0000R.string.update_msg_normal_update, new Object[]{str});
        d dVar = new d(this.mActivity);
        dVar.setTitle((int) C0000R.string.update_title_update_tip);
        dVar.setMessage(string + "\n\n" + str2);
        dVar.a(C0000R.string.update_update_now, new o(this));
        dVar.b(C0000R.string.update_update_skip, new l(this));
        dVar.setOnDismissListener(new m(this));
        dVar.show();
    }

    /* access modifiers changed from: private */
    public void v(String str) {
        if (this.mActivity.isFinishing()) {
            iK = false;
            return;
        }
        String string = this.mActivity.getString(C0000R.string.update_msg_force_update, new Object[]{str});
        d dVar = new d(this.mActivity);
        dVar.setTitle((int) C0000R.string.update_title_update_tip);
        dVar.setMessage(string);
        dVar.a(C0000R.string.update_update_now, new j(this));
        dVar.setOnDismissListener(new k(this));
        dVar.show();
    }

    public void Q() {
        e.c("UpdateHelper", "start downloading...");
        iL = true;
    }

    public void R() {
        e.c("UpdateHelper", "No updates for download");
    }

    private static String i(Context context) {
        try {
            return ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        } catch (Exception e) {
            e.f("UpdateHelper", "Failed to get id");
            return null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x002c A[SYNTHETIC, Splitter:B:11:0x002c] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String x(android.content.Context r5) {
        /*
            r4 = 0
            java.io.BufferedReader r0 = new java.io.BufferedReader     // Catch:{ IOException -> 0x0030 }
            java.io.InputStreamReader r1 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x0030 }
            android.content.res.Resources r2 = r5.getResources()     // Catch:{ IOException -> 0x0030 }
            android.content.res.AssetManager r2 = r2.getAssets()     // Catch:{ IOException -> 0x0030 }
            java.lang.String r3 = "lc.txt"
            java.io.InputStream r2 = r2.open(r3)     // Catch:{ IOException -> 0x0030 }
            r1.<init>(r2)     // Catch:{ IOException -> 0x0030 }
            r0.<init>(r1)     // Catch:{ IOException -> 0x0030 }
            java.lang.String r1 = r0.readLine()     // Catch:{ IOException -> 0x003d }
            if (r1 == 0) goto L_0x002a
            java.lang.String r1 = r1.trim()     // Catch:{ IOException -> 0x0040 }
            int r2 = r1.length()     // Catch:{ IOException -> 0x0040 }
            if (r2 != 0) goto L_0x002a
            r1 = r4
        L_0x002a:
            if (r0 == 0) goto L_0x002f
            r0.close()     // Catch:{ IOException -> 0x003b }
        L_0x002f:
            return r1
        L_0x0030:
            r0 = move-exception
            r0 = r4
            r1 = r4
        L_0x0033:
            java.lang.String r2 = "UpdateHelper"
            java.lang.String r3 = "LC not found"
            com.dianxinos.powermanager.c.e.f(r2, r3)
            goto L_0x002a
        L_0x003b:
            r0 = move-exception
            goto L_0x002f
        L_0x003d:
            r1 = move-exception
            r1 = r4
            goto L_0x0033
        L_0x0040:
            r2 = move-exception
            goto L_0x0033
        */
        throw new UnsupportedOperationException("Method not decompiled: com.dianxinos.powermanager.update.i.x(android.content.Context):java.lang.String");
    }
}
