package com.dianxinos.powermanager.update;

import android.content.Context;
import android.content.SharedPreferences;

/* compiled from: UpdateConfig */
public class e {
    private Context mContext;

    public e(Context context) {
        this.mContext = context;
    }

    public boolean bg() {
        return this.mContext.getSharedPreferences("upgrade_config", 0).getBoolean("force", false);
    }

    public void g(boolean z) {
        SharedPreferences.Editor edit = this.mContext.getSharedPreferences("upgrade_config", 0).edit();
        edit.putBoolean("force", z);
        edit.commit();
    }

    public boolean bh() {
        if (bg()) {
            return true;
        }
        return System.currentTimeMillis() >= this.mContext.getSharedPreferences("upgrade_config", 0).getLong("check", 0);
    }

    public void h(boolean z) {
        long j;
        long currentTimeMillis = System.currentTimeMillis();
        if (z) {
            j = currentTimeMillis + 259200000;
        } else {
            j = currentTimeMillis + 86400000;
        }
        SharedPreferences.Editor edit = this.mContext.getSharedPreferences("upgrade_config", 0).edit();
        edit.putLong("check", j);
        edit.commit();
    }
}
