package com.dianxinos.powermanager.update;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;
import com.dianxinos.appupdate.k;
import com.dianxinos.appupdate.q;
import com.dianxinos.appupdate.u;
import com.dianxinos.powermanager.C0000R;
import com.dianxinos.powermanager.PowerMgrActivity;
import com.dianxinos.powermanager.c.e;

public class DownloadProgressListenerImpl implements q {
    private int az;
    private NotificationManager cJ;

    public void a(Context context, String str, long j, long j2) {
        int i;
        if (j2 > 0) {
            i = (int) ((100 * j) / j2);
        } else {
            i = 0;
        }
        a(context, i, false, false, 0);
    }

    public void a(Context context, long j, long j2) {
        int i;
        int i2 = this.az;
        if (j2 <= 0 || j <= 0) {
            i = i2;
        } else {
            i = (int) ((100 * j) / j2);
        }
        a(context, i, false, false, 0);
    }

    public void a(Context context, String str, boolean z, int i, String str2, int i2) {
        e.d("DownloadProgressListenerImpl", "Download complete with result: " + i2);
        if (i2 != 2) {
            a(context, this.az, true, z, i2);
            i.bR();
            return;
        }
        e(context, i2);
    }

    private void a(Context context, int i, boolean z, boolean z2, int i2) {
        if (this.cJ == null) {
            this.cJ = (NotificationManager) context.getSystemService("notification");
        }
        this.az = i;
        this.cJ.cancel(3);
        if (z) {
            if (i2 != 2) {
                this.cJ.cancel(1);
            }
            if (z2) {
                k.d(context.getApplicationContext()).a((u) null);
            } else if (i2 != 1) {
                e(context, i2);
            }
        } else {
            String string = context.getString(C0000R.string.update_download_message, context.getString(C0000R.string.app_name));
            Intent intent = new Intent(context, DownloadActivity.class);
            intent.addFlags(268435456);
            intent.putExtra("percent", i);
            RemoteViews remoteViews = new RemoteViews("com.dianxinos.powermanager", (int) C0000R.layout.statusbar_download);
            remoteViews.setImageViewResource(C0000R.id.icon, C0000R.drawable.app_icon);
            remoteViews.setTextViewText(C0000R.id.title, string);
            remoteViews.setTextViewText(C0000R.id.percent, i + "%");
            remoteViews.setProgressBar(C0000R.id.progress_bar, 100, i, false);
            PendingIntent activity = PendingIntent.getActivity(context, 0, intent, 134217728);
            Notification notification = new Notification();
            notification.icon = C0000R.drawable.app_icon;
            notification.when = 0;
            notification.flags = 2;
            notification.defaults = 0;
            notification.sound = null;
            notification.vibrate = null;
            notification.contentView = remoteViews;
            notification.contentIntent = activity;
            notification.tickerText = string;
            this.cJ.notify(1, notification);
        }
    }

    private void e(Context context, int i) {
        String string;
        if (i == 3) {
            string = context.getString(C0000R.string.update_download_error_no_network);
        } else if (i == 5 || i == 4) {
            string = context.getString(C0000R.string.udpate_downlaod_error_device_not_ready);
        } else if (i == 6) {
            string = context.getString(C0000R.string.update_download_error_insuffient_space);
        } else if (i == 7) {
            string = context.getString(C0000R.string.update_download_error_http);
        } else if (i == 9) {
            string = context.getString(C0000R.string.update_download_error_no_network);
        } else if (i == 2) {
            string = context.getString(C0000R.string.update_download_error_wait_retry);
        } else if (i == 10) {
            string = context.getString(C0000R.string.update_download_error_retry_failed);
        } else {
            string = context.getString(C0000R.string.update_download_error_unknown, Integer.valueOf(i));
        }
        e.e("DownloadProgressListenerImpl", "Download failed with error: " + i);
        Intent intent = new Intent(context, PowerMgrActivity.class);
        intent.addFlags(268435456);
        PendingIntent activity = PendingIntent.getActivity(context, 0, intent, 0);
        String string2 = context.getString(C0000R.string.update_title_download_error);
        Notification notification = new Notification(C0000R.drawable.app_icon, string2, 0);
        notification.flags = 16;
        notification.setLatestEventInfo(context, string2, string, activity);
        this.cJ.notify(3, notification);
    }
}
