package com.dianxinos.powermanager.update;

import android.os.Handler;
import android.os.Message;

/* compiled from: DownloadActivity */
class f extends Handler {
    final /* synthetic */ DownloadActivity gE;

    private f(DownloadActivity downloadActivity) {
        this.gE = downloadActivity;
    }

    /* synthetic */ f(DownloadActivity downloadActivity, g gVar) {
        this(downloadActivity);
    }

    public void handleMessage(Message message) {
        switch (message.what) {
            case 1:
                this.gE.finish();
                return;
            case 2:
                this.gE.mProgressBar.setProgress(message.arg1);
                return;
            default:
                return;
        }
    }
}
