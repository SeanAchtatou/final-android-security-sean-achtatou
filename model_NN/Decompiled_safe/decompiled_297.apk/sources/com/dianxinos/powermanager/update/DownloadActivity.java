package com.dianxinos.powermanager.update;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Message;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.dianxinos.appupdate.k;
import com.dianxinos.appupdate.q;
import com.dianxinos.powermanager.C0000R;

public class DownloadActivity extends Activity implements q {
    private f ax;
    /* access modifiers changed from: private */
    public k ay;
    private int az;
    /* access modifiers changed from: private */
    public ProgressBar mProgressBar;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.update_dialog);
        this.az = getIntent().getIntExtra("percent", 0);
        this.ax = new f(this, null);
        this.ay = k.d(getApplicationContext());
        String string = getString(C0000R.string.update_download_message, new Object[]{getString(C0000R.string.app_name)});
        ((TextView) findViewById(C0000R.id.title)).setText((int) C0000R.string.update_title_download);
        ((TextView) findViewById(C0000R.id.message)).setText(string);
        this.mProgressBar = (ProgressBar) findViewById(C0000R.id.progress_bar);
        this.mProgressBar.setMax(100);
        this.mProgressBar.setProgress(this.az);
        this.mProgressBar.setVisibility(0);
        Button button = (Button) findViewById(C0000R.id.ok);
        button.setVisibility(0);
        button.setText((int) C0000R.string.update_download_continue);
        button.setOnClickListener(new g(this));
        Button button2 = (Button) findViewById(C0000R.id.cancel);
        button2.setVisibility(0);
        button2.setText((int) C0000R.string.update_download_cancel);
        button2.setOnClickListener(new h(this));
        this.ay.a(this);
    }

    public void a(Context context, String str, long j, long j2) {
        int i = 0;
        if (j2 > 0) {
            i = (int) ((100 * j) / j2);
        }
        k(i);
    }

    public void a(Context context, long j, long j2) {
        int i = this.az;
        if (j2 > 0 && j > 0) {
            i = (int) ((100 * j) / j2);
        }
        k(i);
    }

    public void a(Context context, String str, boolean z, int i, String str2, int i2) {
        if (i2 != 2) {
            this.ax.sendEmptyMessage(1);
            this.ay.b(this);
        }
    }

    private void k(int i) {
        this.az = i;
        Message message = new Message();
        message.what = 2;
        message.arg1 = i;
        this.ax.sendMessage(message);
    }
}
