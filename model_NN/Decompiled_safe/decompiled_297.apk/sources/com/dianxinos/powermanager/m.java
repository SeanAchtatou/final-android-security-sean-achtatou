package com.dianxinos.powermanager;

import android.os.SystemClock;
import android.view.View;
import com.dianxinos.a.a.a;

/* compiled from: RecentAppsListAdapter */
class m implements View.OnClickListener {
    final /* synthetic */ k fb;

    m(k kVar) {
        this.fb = kVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.dianxinos.a.a.a.a(java.lang.String, java.lang.String, java.lang.Number):boolean
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.dianxinos.a.a.a.a(java.lang.String, int, java.lang.Object):boolean
      com.dianxinos.a.a.a.a(java.lang.String, java.lang.String, java.lang.Number):boolean */
    public void onClick(View view) {
        int intValue = ((Integer) view.getTag()).intValue();
        SystemClock.elapsedRealtime();
        this.fb.w(intValue);
        a F = a.F(this.fb.mContext);
        F.init();
        F.a("recent", "clean_item", (Number) 1);
        F.destroy();
    }
}
