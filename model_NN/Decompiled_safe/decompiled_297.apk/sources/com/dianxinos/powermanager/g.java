package com.dianxinos.powermanager;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import android.widget.RemoteViews;
import com.dianxinos.powermanager.c.c;
import com.dianxinos.powermanager.c.e;
import com.dianxinos.powermanager.mode.i;

/* compiled from: OnGoingNotification */
public class g {
    private static g cN;
    private boolean aG;
    private NotificationManager cJ;
    private int cK;
    private int cL;
    private String cM;
    private int[] cO = {C0000R.drawable.stat_battery_0, C0000R.drawable.stat_battery_1, C0000R.drawable.stat_battery_2, C0000R.drawable.stat_battery_3, C0000R.drawable.stat_battery_4, C0000R.drawable.stat_battery_5, C0000R.drawable.stat_battery_6, C0000R.drawable.stat_battery_7, C0000R.drawable.stat_battery_8, C0000R.drawable.stat_battery_9, C0000R.drawable.stat_battery_10, C0000R.drawable.stat_battery_11, C0000R.drawable.stat_battery_12, C0000R.drawable.stat_battery_13, C0000R.drawable.stat_battery_14, C0000R.drawable.stat_battery_15, C0000R.drawable.stat_battery_16, C0000R.drawable.stat_battery_17, C0000R.drawable.stat_battery_18, C0000R.drawable.stat_battery_19, C0000R.drawable.stat_battery_20, C0000R.drawable.stat_battery_21, C0000R.drawable.stat_battery_22, C0000R.drawable.stat_battery_23, C0000R.drawable.stat_battery_24, C0000R.drawable.stat_battery_25, C0000R.drawable.stat_battery_26, C0000R.drawable.stat_battery_27, C0000R.drawable.stat_battery_28, C0000R.drawable.stat_battery_29, C0000R.drawable.stat_battery_30, C0000R.drawable.stat_battery_31, C0000R.drawable.stat_battery_32, C0000R.drawable.stat_battery_33, C0000R.drawable.stat_battery_34, C0000R.drawable.stat_battery_35, C0000R.drawable.stat_battery_36, C0000R.drawable.stat_battery_37, C0000R.drawable.stat_battery_38, C0000R.drawable.stat_battery_39, C0000R.drawable.stat_battery_40, C0000R.drawable.stat_battery_41, C0000R.drawable.stat_battery_42, C0000R.drawable.stat_battery_43, C0000R.drawable.stat_battery_44, C0000R.drawable.stat_battery_45, C0000R.drawable.stat_battery_46, C0000R.drawable.stat_battery_47, C0000R.drawable.stat_battery_48, C0000R.drawable.stat_battery_49, C0000R.drawable.stat_battery_50, C0000R.drawable.stat_battery_51, C0000R.drawable.stat_battery_52, C0000R.drawable.stat_battery_53, C0000R.drawable.stat_battery_54, C0000R.drawable.stat_battery_55, C0000R.drawable.stat_battery_56, C0000R.drawable.stat_battery_57, C0000R.drawable.stat_battery_58, C0000R.drawable.stat_battery_59, C0000R.drawable.stat_battery_60, C0000R.drawable.stat_battery_61, C0000R.drawable.stat_battery_62, C0000R.drawable.stat_battery_63, C0000R.drawable.stat_battery_64, C0000R.drawable.stat_battery_65, C0000R.drawable.stat_battery_66, C0000R.drawable.stat_battery_67, C0000R.drawable.stat_battery_68, C0000R.drawable.stat_battery_69, C0000R.drawable.stat_battery_70, C0000R.drawable.stat_battery_71, C0000R.drawable.stat_battery_72, C0000R.drawable.stat_battery_73, C0000R.drawable.stat_battery_74, C0000R.drawable.stat_battery_75, C0000R.drawable.stat_battery_76, C0000R.drawable.stat_battery_77, C0000R.drawable.stat_battery_78, C0000R.drawable.stat_battery_79, C0000R.drawable.stat_battery_80, C0000R.drawable.stat_battery_81, C0000R.drawable.stat_battery_82, C0000R.drawable.stat_battery_83, C0000R.drawable.stat_battery_84, C0000R.drawable.stat_battery_85, C0000R.drawable.stat_battery_86, C0000R.drawable.stat_battery_87, C0000R.drawable.stat_battery_88, C0000R.drawable.stat_battery_89, C0000R.drawable.stat_battery_90, C0000R.drawable.stat_battery_91, C0000R.drawable.stat_battery_92, C0000R.drawable.stat_battery_93, C0000R.drawable.stat_battery_94, C0000R.drawable.stat_battery_95, C0000R.drawable.stat_battery_96, C0000R.drawable.stat_battery_97, C0000R.drawable.stat_battery_98, C0000R.drawable.stat_battery_99, C0000R.drawable.stat_battery_100};
    private Context mContext;

    public static g r(Context context) {
        if (cN == null) {
            cN = new g(context);
        }
        return cN;
    }

    private g(Context context) {
        this.mContext = context;
        this.cJ = (NotificationManager) this.mContext.getSystemService("notification");
    }

    public void a(boolean z, int i, int i2, String str, String str2) {
        this.aG = z;
        this.cK = i;
        this.cL = i2;
        this.cM = str2;
        String string = this.mContext.getString(C0000R.string.app_name);
        Intent intent = new Intent(this.mContext, PowerMgrActivity.class);
        intent.setAction("com.dianxinos.powermanager.STATUSBAR");
        intent.putExtra("From", 3);
        intent.addFlags(268435456);
        RemoteViews remoteViews = new RemoteViews("com.dianxinos.powermanager", (int) C0000R.layout.statusbar_ongoing);
        remoteViews.setImageViewResource(C0000R.id.icon, this.cO[i]);
        remoteViews.setTextViewText(C0000R.id.remaining_time, a(z, i));
        remoteViews.setTextViewText(C0000R.id.suggest, str2);
        remoteViews.setTextViewText(C0000R.id.curr_mode, string + "-" + str);
        PendingIntent activity = PendingIntent.getActivity(this.mContext, 0, intent, 134217728);
        Notification notification = new Notification();
        notification.icon = this.cO[i];
        notification.when = 0;
        notification.flags = 2;
        notification.defaults = 0;
        notification.sound = null;
        notification.vibrate = null;
        notification.contentView = remoteViews;
        notification.contentIntent = activity;
        notification.tickerText = null;
        this.cJ.notify(2, notification);
    }

    public void an() {
        this.cJ.cancel(2);
    }

    private String a(boolean z, int i) {
        int i2;
        String string;
        int i3;
        if (z) {
            if (((long) this.cL) == -1) {
                e.c("OnGoingNotification", " charging time unkonwn ");
                i3 = 0;
            } else {
                i3 = this.cL;
            }
            i2 = i3;
            string = this.mContext.getString(C0000R.string.battery_info_remaining_charging_time);
        } else {
            i2 = this.cL;
            string = this.mContext.getString(C0000R.string.battery_info_remaining_discharging_time);
        }
        if (i2 == -1) {
            return string + c.c(this.mContext, 0);
        }
        if (i2 == -2) {
            return string + this.mContext.getString(C0000R.string.battery_data_collecting);
        }
        return string + c.c(this.mContext, i2);
    }

    public void ao() {
        if (Settings.System.getInt(this.mContext.getContentResolver(), "com.dianxinos.powermanager.statusbar_notification", 1) != 0) {
            a(this.aG, this.cK, this.cL, i.t(this.mContext).az(), this.cM);
        }
    }
}
