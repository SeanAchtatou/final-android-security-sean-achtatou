package com.dianxinos.powermanager.mode;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.dianxinos.a.a.a;
import com.dianxinos.powermanager.C0000R;
import com.dianxinos.powermanager.c.e;
import com.dianxinos.powermanager.u;
import java.util.ArrayList;

public class ModeMgrActivity extends Activity implements View.OnClickListener, t {
    private LinearLayout X;
    /* access modifiers changed from: private */
    public s ac;
    /* access modifiers changed from: private */
    public a by;
    private BroadcastReceiver dB = new c(this);
    /* access modifiers changed from: private */
    public int ef = 0;
    private View fc;
    /* access modifiers changed from: private */
    public ArrayList fd;
    /* access modifiers changed from: private */
    public TextView fe;
    /* access modifiers changed from: private */
    public Dialog ff;
    private Dialog fg;
    private ArrayList fh;
    private ArrayList fi;
    /* access modifiers changed from: private */
    public int fj;
    /* access modifiers changed from: private */
    public Dialog fk;
    /* access modifiers changed from: private */
    public int fl;
    private Button fm;
    private TextView fn;
    /* access modifiers changed from: private */
    public Dialog fo;
    /* access modifiers changed from: private */
    public i fp;
    /* access modifiers changed from: private */
    public boolean fq = false;
    private LayoutInflater mInflater;
    /* access modifiers changed from: private */
    public int o = 3;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.mode_mgr);
        this.fp = i.t(this);
        this.fd = new ArrayList();
        this.fh = new ArrayList();
        this.fi = new ArrayList();
        this.X = (LinearLayout) findViewById(C0000R.id.mode_scrollview_items);
        this.o = this.fp.aw();
        this.ef = this.fp.ax();
        this.fc = findViewById(C0000R.id.mode_title);
        this.mInflater = LayoutInflater.from(this);
        aK();
        this.fe = (TextView) findViewById(C0000R.id.curr_mode_selected);
        this.fe.setText(((u) this.fd.get(this.ef)).getTitle());
        this.fn = (TextView) findViewById(C0000R.id.modifed);
        this.fm = (Button) findViewById(C0000R.id.restore_button);
        this.fm.setOnClickListener(this);
        this.ac = this.fp.aB();
        this.ac.a(this);
        as();
        this.by = a.F(this);
        this.by.init();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        unregisterReceiver(this.dB);
        this.by.destroy();
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        aN();
        this.ac.cK();
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.ac.cL();
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        boolean z;
        if (i == 1) {
            if (this.fq) {
                z(this.fl);
                this.fq = false;
            }
            switch (i2) {
                case -2:
                    setResult(-2);
                    finish();
                    return;
                case -1:
                    String string = intent.getExtras().getString("ModeName");
                    this.X.addView(aq(), this.o * 2);
                    this.X.addView(b(this.o, this.ef, string), (this.o * 2) + 1);
                    this.fp.k(string);
                    this.o++;
                    return;
                default:
                    return;
            }
        } else if (i == 2) {
            switch (i2) {
                case -2:
                    setResult(-2);
                    finish();
                    return;
                case -1:
                    Bundle extras = intent.getExtras();
                    String string2 = extras.getString("ModeName");
                    int i3 = extras.getInt("index");
                    this.fp.b(i3, string2);
                    ((u) this.fd.get(i3)).setTitle(string2);
                    if (i3 == this.ef) {
                        this.fe.setText(string2);
                        if (this.fn.getVisibility() == 0) {
                            z = true;
                        } else {
                            z = false;
                        }
                        this.ac.m(z);
                        this.ac.Z(this.ef);
                        x(this.ef);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: private */
    public void x(int i) {
        if (this.ac.Y(i)) {
            P();
        }
        this.fm.setVisibility(8);
        this.fn.setVisibility(8);
        Toast.makeText(this, getString(C0000R.string.mode_newmode_changeing), 0).show();
    }

    private void P() {
        float f;
        int intValue = ((Integer) this.ac.cF().get(0)).intValue();
        if (intValue == 4) {
            f = 0.2f;
        } else if (intValue == 2) {
            f = 0.5f;
        } else if (intValue == 1) {
            f = 0.3f;
        } else {
            f = intValue == 0 ? 0.11764706f : 1.0f;
        }
        e.d("ModeMgrActivity", " freshBright br:  " + f);
        WindowManager.LayoutParams attributes = getWindow().getAttributes();
        attributes.screenBrightness = f;
        getWindow().setAttributes(attributes);
    }

    private void aK() {
        for (int i = 0; i < 3; i++) {
            View g = g(i, this.ef);
            this.X.addView(aq());
            this.X.addView(g);
        }
        for (int i2 = 3; i2 < this.o; i2++) {
            String t = this.fp.t(i2);
            e.d("ModeMgrActivity", " Reading share preference get mode :" + t);
            this.X.addView(aq());
            this.X.addView(b(i2, this.ef, t));
        }
        this.X.addView(aq());
        this.X.addView(y(this.o));
    }

    private View b(int i, int i2, String str) {
        boolean z;
        View inflate = this.mInflater.inflate((int) C0000R.layout.mode_custom_item, (ViewGroup) null);
        ((TextView) inflate.findViewById(C0000R.id.label)).setText(str);
        ((TextView) inflate.findViewById(C0000R.id.time)).setText(getString(C0000R.string.battery_time_hours));
        if (i == i2) {
            z = true;
        } else {
            z = false;
        }
        ImageView imageView = (ImageView) inflate.findViewById(C0000R.id.edit);
        q qVar = new q(i);
        this.fh.add(qVar);
        imageView.setTag(qVar);
        imageView.setOnClickListener(this);
        ImageView imageView2 = (ImageView) inflate.findViewById(C0000R.id.delete);
        p pVar = new p(i);
        this.fi.add(pVar);
        imageView2.setTag(pVar);
        imageView2.setOnClickListener(this);
        u uVar = new u(this, str, inflate, z);
        this.fd.add(uVar);
        inflate.setTag(uVar);
        inflate.setOnClickListener(this);
        return inflate;
    }

    private View y(int i) {
        View inflate = this.mInflater.inflate((int) C0000R.layout.mode_new_item, (ViewGroup) null);
        inflate.setTag("addNew");
        inflate.setOnClickListener(this);
        ((TextView) inflate.findViewById(C0000R.id.label)).setText(getString(C0000R.string.mode_label_newmode));
        ((TextView) inflate.findViewById(C0000R.id.detail)).setText(getString(C0000R.string.mode_newmode_detail));
        return inflate;
    }

    private View g(int i, int i2) {
        View inflate = this.mInflater.inflate((int) C0000R.layout.mode_mgr_item, (ViewGroup) null);
        String t = this.fp.t(i);
        String string = getString(C0000R.string.battery_time_hours);
        String u = this.fp.u(i);
        ((TextView) inflate.findViewById(C0000R.id.label)).setText(t);
        ((TextView) inflate.findViewById(C0000R.id.time)).setText(string);
        ((TextView) inflate.findViewById(C0000R.id.detail)).setText(u);
        boolean z = false;
        if (i == i2) {
            z = true;
        }
        u uVar = new u(this, t, inflate, z);
        this.fd.add(uVar);
        inflate.setTag(uVar);
        inflate.setOnClickListener(this);
        return inflate;
    }

    private void aL() {
        String string;
        if (this.fg == null || !this.fg.isShowing()) {
            this.fg = new u(this);
            int size = this.ac.size();
            for (int i = 0; i < size; i++) {
                if (this.ac.X(a.c(i)).j()) {
                    ((u) this.fg).a(i(this.ac.X(a.c(i)).getName(), this.ac.X(a.c(i)).getValueString()));
                }
            }
            if (this.ef == 2) {
                String string2 = getString(C0000R.string.mode_comm);
                if (this.ac.cM().g()) {
                    string = getString(C0000R.string.mode_status_off);
                } else {
                    string = getString(C0000R.string.mode_status_on);
                }
                ((u) this.fg).a(i(string2, string));
            }
            this.fg.show();
        }
    }

    private void aM() {
        if (this.fk == null) {
            this.fk = new Dialog(this, C0000R.style.ShowDialogStyle);
            this.fk.setContentView((int) C0000R.layout.change_confirm_dialog);
        }
        ((TextView) this.fk.findViewById(C0000R.id.title)).setText(getString(C0000R.string.mode_change_mode));
        TextView textView = (TextView) this.fk.findViewById(C0000R.id.showinfo);
        textView.setText(getString(C0000R.string.mode_change_mode_confirm_info));
        ((Button) this.fk.findViewById(C0000R.id.cancel)).setOnClickListener(new l(this));
        ((Button) this.fk.findViewById(C0000R.id.change)).setOnClickListener(new k(this));
        Button button = (Button) this.fk.findViewById(C0000R.id.save);
        if (this.fn.getVisibility() == 0) {
            button.setVisibility(0);
            textView.setText(getString(C0000R.string.mode_change_mode_confirm_save_info));
        } else {
            button.setVisibility(8);
        }
        button.setOnClickListener(new f(this));
        this.fk.show();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.dianxinos.a.a.a.a(java.lang.String, java.lang.String, java.lang.Number):boolean
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.dianxinos.a.a.a.a(java.lang.String, int, java.lang.Object):boolean
      com.dianxinos.a.a.a.a(java.lang.String, java.lang.String, java.lang.Number):boolean */
    public void onClick(View view) {
        if (view == this.fc) {
            aL();
        } else if (view == this.fm) {
            if (this.fo == null) {
                this.fo = new Dialog(this, C0000R.style.ShowDialogStyle);
                this.fo.setContentView((int) C0000R.layout.confirm_dialog);
            }
            ((TextView) this.fo.findViewById(C0000R.id.title)).setText(getString(C0000R.string.mode_restore));
            ((TextView) this.fo.findViewById(C0000R.id.showinfo)).setText(getString(C0000R.string.mode_restore_mode_confirm_info));
            ((Button) this.fo.findViewById(C0000R.id.no)).setOnClickListener(new e(this));
            ((Button) this.fo.findViewById(C0000R.id.yes)).setOnClickListener(new h(this));
            this.fo.show();
        } else if (view.getTag().equals("addNew")) {
            Intent intent = new Intent(this, NewModeActivity.class);
            Bundle bundle = new Bundle();
            bundle.putInt("index", this.o);
            bundle.putInt("counts", this.o);
            intent.putExtras(bundle);
            startActivityForResult(intent, 1);
            this.by.a("mode", "add", (Number) 1);
        } else {
            for (int i = 3; i < this.o; i++) {
                if (view.getTag().equals(this.fh.get(i - 3))) {
                    Intent intent2 = new Intent(this, NewModeActivity.class);
                    Bundle bundle2 = new Bundle();
                    bundle2.putInt("index", i);
                    bundle2.putInt("counts", this.o);
                    intent2.putExtras(bundle2);
                    startActivityForResult(intent2, 2);
                    this.by.a("mode", "modify", (Number) 1);
                }
            }
            for (int i2 = 3; i2 < this.o; i2++) {
                if (view.getTag().equals(this.fi.get(i2 - 3))) {
                    if (this.ef == i2) {
                        Toast.makeText(this, getString(C0000R.string.mode_current_selected), 0).show();
                        return;
                    }
                    this.fj = i2;
                    if (this.ff == null) {
                        this.ff = new Dialog(this, C0000R.style.ShowDialogStyle);
                        this.ff.setContentView((int) C0000R.layout.confirm_dialog);
                    }
                    ((TextView) this.ff.findViewById(C0000R.id.title)).setText(getString(C0000R.string.mode_delete_mode));
                    ((TextView) this.ff.findViewById(C0000R.id.showinfo)).setText(getString(C0000R.string.mode_delete_mode_confirm_info));
                    ((Button) this.ff.findViewById(C0000R.id.no)).setOnClickListener(new g(this));
                    ((Button) this.ff.findViewById(C0000R.id.yes)).setOnClickListener(new d(this));
                    this.ff.show();
                }
            }
            for (int i3 = 0; i3 < this.o; i3++) {
                if (view.getTag().equals(this.fd.get(i3))) {
                    if (this.ef != i3) {
                        this.fl = i3;
                        aM();
                    } else {
                        aL();
                    }
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void z(int i) {
        ((u) this.fd.get(this.ef)).bA();
        ((u) this.fd.get(i)).bA();
        this.ef = i;
        this.fp.r(this.ef);
        this.fe.setText(((u) this.fd.get(this.ef)).getTitle());
        this.ac.m(this.fn.getVisibility() == 0);
        this.ac.Z(this.ef);
        x(i);
    }

    private View i(String str, String str2) {
        View inflate = this.mInflater.inflate((int) C0000R.layout.mode_status_show_item, (ViewGroup) null);
        ((TextView) inflate.findViewById(C0000R.id.itemname)).setText(str);
        ((TextView) inflate.findViewById(C0000R.id.value)).setText(str2);
        return inflate;
    }

    private View aq() {
        ImageView imageView = new ImageView(this);
        imageView.setImageDrawable(getResources().getDrawable(C0000R.drawable.horizontal_line));
        return imageView;
    }

    private void aN() {
        this.ac.cH();
        this.ac.Z(this.ef);
        if (!this.ac.cI()) {
            this.fm.setVisibility(8);
            this.fn.setVisibility(0);
            return;
        }
        this.fm.setVisibility(8);
        this.fn.setVisibility(8);
    }

    /* access modifiers changed from: private */
    public void A(int i) {
        if (this.ef > i) {
            this.ef--;
            this.fp.r(this.ef);
        }
        this.fi.remove(i - 3);
        this.fh.remove(i - 3);
        this.fd.remove(i);
        this.X.removeViewAt(i * 2);
        this.X.removeViewAt(i * 2);
        this.fp.s(i);
        this.o--;
    }

    public void aO() {
        if (this.fn.getVisibility() == 8) {
            this.fn.setVisibility(0);
            this.fm.setVisibility(8);
        }
    }

    public void aP() {
        if (this.fn.getVisibility() == 0) {
            this.fn.setVisibility(8);
            this.fm.setVisibility(8);
        }
    }

    private void as() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.dianxinos.powermanager.MODECHANGE");
        registerReceiver(this.dB, intentFilter);
    }
}
