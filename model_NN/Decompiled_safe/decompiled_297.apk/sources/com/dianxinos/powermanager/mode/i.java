package com.dianxinos.powermanager.mode;

import android.content.Context;
import android.content.Intent;
import com.dianxinos.powermanager.C0000R;
import java.util.ArrayList;

/* compiled from: ModeManager */
public class i {
    private static int ef = 1;
    private static ArrayList eg;
    private static ArrayList eh;
    private static i ej;
    private static int o = 3;
    private s ac;
    private o ei;
    private m ek = m.a(this.mContext, this, this.ac);
    private Context mContext;

    public static i t(Context context) {
        if (ej == null) {
            ej = new i(context);
        }
        return ej;
    }

    private i(Context context) {
        this.mContext = context;
        this.ac = s.a(context, this);
        this.ei = new o(context);
        this.ei.j(this.ac.cE(), this.ac.size());
        o = this.ei.aw();
        ef = this.ei.bF();
        eg = new ArrayList();
        eh = new ArrayList();
        eg.add(this.mContext.getString(C0000R.string.mode_label_longest_standby));
        eh.add(this.mContext.getString(C0000R.string.mode_longest_detail));
        eg.add(this.mContext.getString(C0000R.string.mode_label_balance));
        eh.add(this.mContext.getString(C0000R.string.mode_balance_detail));
        eg.add(this.mContext.getString(C0000R.string.mode_label_sleep));
        eh.add(this.mContext.getString(C0000R.string.mode_sleep_detail));
        if (o > 3) {
            for (int i = 3; i < o; i++) {
                eg.add(this.ei.t(i));
            }
        }
    }

    public void k(String str) {
        eg.add(str);
        o++;
    }

    public int aw() {
        return o;
    }

    public int ax() {
        return ef;
    }

    public void r(int i) {
        ef = i;
        this.ei.M(i);
    }

    public void s(int i) {
        eg.remove(i);
        o--;
        this.ei.i(i, o);
    }

    public ArrayList ay() {
        return eg;
    }

    public String t(int i) {
        if (i < o) {
            return (String) eg.get(i);
        }
        return null;
    }

    public void b(int i, String str) {
        eg.set(i, str);
    }

    public String u(int i) {
        if (i < o) {
            return (String) eh.get(i);
        }
        return null;
    }

    public String az() {
        return (String) eg.get(ef);
    }

    public m aA() {
        return this.ek;
    }

    public s aB() {
        return this.ac;
    }

    public void ao() {
        eg.clear();
        eh.clear();
        eg.add(this.mContext.getString(C0000R.string.mode_label_longest_standby));
        eh.add(this.mContext.getString(C0000R.string.mode_longest_detail));
        eg.add(this.mContext.getString(C0000R.string.mode_label_balance));
        eh.add(this.mContext.getString(C0000R.string.mode_balance_detail));
        eg.add(this.mContext.getString(C0000R.string.mode_label_sleep));
        eh.add(this.mContext.getString(C0000R.string.mode_sleep_detail));
        if (o > 3) {
            for (int i = 3; i < o; i++) {
                eg.add(this.ei.t(i));
            }
        }
    }

    public void v(int i) {
        Intent intent = new Intent("com.dianxinos.powermanager.MODECHANGE");
        intent.putExtra("ModeName", t(i));
        this.mContext.sendBroadcast(intent);
    }

    public void aC() {
        this.mContext.sendBroadcast(new Intent("com.dianxinos.powermanager.MODEMODIFIED"));
    }
}
