package com.dianxinos.powermanager.mode;

import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.widget.TextView;
import com.dianxinos.powermanager.C0000R;

/* compiled from: SettingItem */
public class n {
    private static int[] ad = new int[2];
    private boolean U;
    private boolean hI;
    TextView hJ;
    private Context mContext;
    private int mIndex;
    private String mValue;

    private void x() {
        Resources resources = this.mContext.getResources();
        ad[0] = resources.getColor(C0000R.color.mode_nomal_color);
        ad[1] = resources.getColor(C0000R.color.mode_back_color);
    }

    public n() {
        this.mIndex = -2;
    }

    public n(Context context, int i, boolean z, boolean z2, String str, View view) {
        this.mContext = context;
        this.mIndex = i;
        this.hI = z;
        x();
        this.U = z2;
        this.mValue = str;
        this.hJ = (TextView) view;
        if (this.hI) {
            i(z2);
        }
    }

    public boolean bz() {
        return this.hI;
    }

    public int getIndex() {
        return this.mIndex;
    }

    public void setIndex(int i) {
        this.mIndex = i;
    }

    public void i(boolean z) {
        if (this.hJ != null) {
            this.U = z;
            if (!this.U) {
                this.mIndex = 0;
                this.hJ.setBackgroundResource(C0000R.drawable.newmode_setting_switch_off);
                this.hJ.setText(this.mContext.getString(C0000R.string.mode_newmode_off));
                this.hJ.setTextColor(ad[0]);
                return;
            }
            this.mIndex = 1;
            this.hJ.setBackgroundResource(C0000R.drawable.newmode_setting_switch_on);
            this.hJ.setText(this.mContext.getString(C0000R.string.mode_newmode_on));
            this.hJ.setTextColor(ad[1]);
        }
    }

    public void bA() {
        boolean z;
        if (this.hJ != null) {
            if (this.U) {
                this.hJ.setBackgroundResource(C0000R.drawable.newmode_setting_switch_off);
                this.hJ.setText(this.mContext.getString(C0000R.string.mode_newmode_off));
                this.mIndex = 0;
                this.hJ.setTextColor(ad[0]);
            } else {
                this.hJ.setBackgroundResource(C0000R.drawable.newmode_setting_switch_on);
                this.hJ.setText(this.mContext.getString(C0000R.string.mode_newmode_on));
                this.mIndex = 1;
                this.hJ.setTextColor(ad[1]);
            }
            if (!this.U) {
                z = true;
            } else {
                z = false;
            }
            this.U = z;
        }
    }
}
