package com.dianxinos.powermanager.mode;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/* compiled from: ModeMgrActivity */
class c extends BroadcastReceiver {
    final /* synthetic */ ModeMgrActivity aA;

    c(ModeMgrActivity modeMgrActivity) {
        this.aA = modeMgrActivity;
    }

    public void onReceive(Context context, Intent intent) {
        String stringExtra = intent.getStringExtra("ModeName");
        int ax = this.aA.fp.ax();
        if (ax < this.aA.fd.size()) {
            ((u) this.aA.fd.get(this.aA.ef)).bA();
            ((u) this.aA.fd.get(ax)).bA();
            int unused = this.aA.ef = ax;
            this.aA.fe.setText(stringExtra);
        }
    }
}
