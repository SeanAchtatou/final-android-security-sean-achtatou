package com.dianxinos.powermanager.mode;

import android.content.Context;
import java.util.ArrayList;

/* compiled from: ModeSettingsMgr */
public class m {
    private static i ab;
    /* access modifiers changed from: private */
    public static s ac;
    private static m ge;
    /* access modifiers changed from: private */
    public o ei;
    private ArrayList gd = new ArrayList();
    private Context mContext;

    public static m a(Context context, i iVar, s sVar) {
        if (ge == null) {
            ge = new m(context, iVar, sVar);
        }
        return ge;
    }

    private m(Context context, i iVar, s sVar) {
        this.mContext = context;
        ab = iVar;
        ac = sVar;
        this.ei = new o(context);
    }

    public ArrayList E(int i) {
        r G = G(i);
        if (G == null) {
            return null;
        }
        return G.cr();
    }

    public int F(int i) {
        if (i == 12) {
            return 1 - ac.cM().getValue();
        }
        if (ac.cG().size() > i) {
            return ((Integer) ac.cG().get(i)).intValue();
        }
        return ac.X(i).getValue();
    }

    public r G(int i) {
        if (i >= ab.aw()) {
            return null;
        }
        if (i >= 3) {
            return new r(this, i);
        }
        if (this.gd.isEmpty()) {
            for (int i2 = 0; i2 < 3; i2++) {
                this.gd.add(new r(this, i2));
            }
        }
        return (r) this.gd.get(i);
    }
}
