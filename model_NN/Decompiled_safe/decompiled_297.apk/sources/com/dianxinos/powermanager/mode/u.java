package com.dianxinos.powermanager.mode;

import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.dianxinos.powermanager.C0000R;

/* compiled from: ModeItem */
public class u {
    private static int[] ad = new int[2];
    private boolean kS;
    private String kT;
    View kU;
    private Context mContext;

    private void x() {
        Resources resources = this.mContext.getResources();
        ad[0] = resources.getColor(C0000R.color.mode_nomal_color);
        ad[1] = resources.getColor(C0000R.color.mode_selected_color);
    }

    public u(Context context, String str, View view, boolean z) {
        this.mContext = context;
        this.kT = str;
        this.kU = view;
        this.kS = z;
        x();
        if (z) {
            ((ImageView) view.findViewById(C0000R.id.icon)).setImageDrawable(this.mContext.getResources().getDrawable(C0000R.drawable.mode_on));
            view.setBackgroundResource(C0000R.drawable.mode_list_item_selected_bkg);
            TextView textView = (TextView) this.kU.findViewById(C0000R.id.label);
            textView.setTextColor(ad[1]);
            textView.setText(str);
            TextView textView2 = (TextView) this.kU.findViewById(C0000R.id.detail);
            if (textView2 != null) {
                textView2.setTextColor(ad[1]);
            }
        }
    }

    public void setTitle(String str) {
        this.kT = str;
        ((TextView) this.kU.findViewById(C0000R.id.label)).setText(str);
    }

    public String getTitle() {
        return this.kT;
    }

    public void bA() {
        boolean z;
        if (this.kS) {
            ((ImageView) this.kU.findViewById(C0000R.id.icon)).setImageDrawable(this.mContext.getResources().getDrawable(C0000R.drawable.mode_off));
            this.kU.setBackgroundResource(C0000R.drawable.mode_list_item_bkg);
            TextView textView = (TextView) this.kU.findViewById(C0000R.id.detail);
            ((TextView) this.kU.findViewById(C0000R.id.label)).setTextColor(ad[0]);
            if (textView != null) {
                textView.setTextColor(ad[0]);
            }
        } else {
            ((ImageView) this.kU.findViewById(C0000R.id.icon)).setImageDrawable(this.mContext.getResources().getDrawable(C0000R.drawable.mode_on));
            this.kU.setBackgroundResource(C0000R.drawable.mode_list_item_selected_bkg);
            TextView textView2 = (TextView) this.kU.findViewById(C0000R.id.detail);
            ((TextView) this.kU.findViewById(C0000R.id.label)).setTextColor(ad[1]);
            if (textView2 != null) {
                textView2.setTextColor(ad[1]);
            }
        }
        if (!this.kS) {
            z = true;
        } else {
            z = false;
        }
        this.kS = z;
    }
}
