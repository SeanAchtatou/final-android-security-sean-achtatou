package com.dianxinos.powermanager.mode;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.dianxinos.a.a.a;
import com.dianxinos.powermanager.C0000R;
import java.util.ArrayList;

public class ModeSelectDialog extends Activity implements View.OnClickListener {
    private static int[] ad = new int[2];
    private LinearLayout X;
    /* access modifiers changed from: private */
    public int Y;
    private int aa;
    /* access modifiers changed from: private */
    public i ab;
    /* access modifiers changed from: private */
    public s ac;
    /* access modifiers changed from: private */
    public boolean bx;
    private a by;
    private LayoutInflater mInflater;
    private int o;
    private ArrayList y;

    private void x() {
        Resources resources = getResources();
        ad[0] = resources.getColor(C0000R.color.mode_nomal_color);
        ad[1] = resources.getColor(C0000R.color.mode_selected_color);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        x();
        setContentView((int) C0000R.layout.mode_select_dialog);
        ((TextView) findViewById(C0000R.id.settingdialog_title)).setText(getString(C0000R.string.mode_select_mode));
        this.ab = i.t(this);
        this.y = this.ab.ay();
        this.ac = this.ab.aB();
        this.ac.cK();
        this.ac.cH();
        this.ac.Z(this.ab.ax());
        this.o = this.ab.aw();
        this.Y = this.ab.ax();
        if (!this.ac.cI()) {
            this.aa = 1;
        } else {
            this.aa = 0;
        }
        this.mInflater = LayoutInflater.from(this);
        y();
        this.by = a.F(this);
        this.by.init();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.by.destroy();
        super.onDestroy();
    }

    private void y() {
        this.X = (LinearLayout) findViewById(C0000R.id.item_value_lists);
        for (int i = 0; i < this.o; i++) {
            this.X.addView(i(i));
        }
    }

    private View i(int i) {
        View inflate = this.mInflater.inflate((int) C0000R.layout.select_mode_list_item, (ViewGroup) null);
        TextView textView = (TextView) inflate.findViewById(C0000R.id.value);
        textView.setText((CharSequence) this.y.get(i));
        if (i == this.Y) {
            ((ImageView) inflate.findViewById(C0000R.id.itemimage)).setVisibility(0);
            textView.setTextColor(ad[1]);
            if (this.aa == 1) {
                ((TextView) inflate.findViewById(C0000R.id.modified)).setVisibility(0);
                this.bx = true;
            } else {
                ((TextView) inflate.findViewById(C0000R.id.modified)).setVisibility(8);
                this.bx = false;
            }
        }
        inflate.setTag(Integer.valueOf(i));
        inflate.setOnClickListener(this);
        return inflate;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.dianxinos.a.a.a.a(java.lang.String, java.lang.String, java.lang.Number):boolean
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.dianxinos.a.a.a.a(java.lang.String, int, java.lang.Object):boolean
      com.dianxinos.a.a.a.a(java.lang.String, java.lang.String, java.lang.Number):boolean */
    public void onClick(View view) {
        for (int i = 0; i < this.o; i++) {
            if (view.getTag().equals(Integer.valueOf(i))) {
                ImageView imageView = (ImageView) this.X.getChildAt(i).findViewById(C0000R.id.itemimage);
                TextView textView = (TextView) this.X.getChildAt(i).findViewById(C0000R.id.value);
                if (i != this.Y) {
                    ((ImageView) this.X.getChildAt(this.Y).findViewById(C0000R.id.itemimage)).setVisibility(4);
                    ((TextView) this.X.getChildAt(this.Y).findViewById(C0000R.id.value)).setTextColor(ad[0]);
                    ((TextView) this.X.getChildAt(this.Y).findViewById(C0000R.id.modified)).setVisibility(4);
                    imageView.setVisibility(0);
                    textView.setTextColor(ad[1]);
                    this.Y = i;
                    Toast.makeText(this, getString(C0000R.string.mode_newmode_changeing), 0).show();
                    new b(this).execute(new String[0]);
                    if (this.Y == 0) {
                        this.by.a("mode", "longest", (Number) 1);
                    } else if (this.Y == 1) {
                        this.by.a("mode", "normal", (Number) 1);
                    } else if (this.Y == 2) {
                        this.by.a("mode", "sleep", (Number) 1);
                    }
                    this.by.a("mode", "switch", (Number) 1);
                    return;
                }
                finish();
                return;
            }
        }
    }

    /* access modifiers changed from: private */
    public void P() {
        float f;
        int intValue = ((Integer) this.ac.cF().get(0)).intValue();
        if (intValue == 4) {
            f = 0.2f;
        } else if (intValue == 2) {
            f = 0.5f;
        } else if (intValue == 1) {
            f = 0.3f;
        } else {
            f = intValue == 0 ? 0.11764706f : 1.0f;
        }
        WindowManager.LayoutParams attributes = getWindow().getAttributes();
        attributes.screenBrightness = f;
        getWindow().setAttributes(attributes);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }
}
