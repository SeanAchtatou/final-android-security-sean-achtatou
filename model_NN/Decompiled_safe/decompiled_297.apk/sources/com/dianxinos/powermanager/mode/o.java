package com.dianxinos.powermanager.mode;

import android.content.Context;
import android.content.SharedPreferences;
import com.dianxinos.powermanager.c.e;
import com.dianxinos.powermanager.c.j;
import java.util.ArrayList;

/* compiled from: ModePrefs */
public class o {
    private static boolean DEBUG = false;
    private static String TAG = "ModePrefs";
    private SharedPreferences hQ = this.mContext.getSharedPreferences("mode_settings", 0);
    private Context mContext;

    public o(Context context) {
        this.mContext = context;
        bE();
    }

    private void bE() {
        if (this.hQ.getInt("Version", 0) != 1) {
            SharedPreferences.Editor edit = this.hQ.edit();
            edit.putInt("Version", 1);
            edit.commit();
        }
    }

    public void a(int i, String str, ArrayList arrayList, boolean z) {
        SharedPreferences.Editor edit = this.hQ.edit();
        String str2 = "mode_id" + i;
        int i2 = this.hQ.getInt("ModeCounter", 3);
        if (z) {
            int i3 = this.hQ.getInt("ModeCounts", 3);
            edit.putInt(str2, i2);
            edit.putInt("ModeCounter", i2 + 1);
            edit.putInt("ModeCounts", i3 + 1);
        } else {
            i2 = this.hQ.getInt(str2, 3);
        }
        int size = arrayList.size();
        for (int i4 = 0; i4 < size; i4++) {
            int intValue = ((Integer) arrayList.get(i4)).intValue();
            if (DEBUG) {
                e.d(TAG, "share prrferences index : " + i4 + " index value:" + intValue);
            }
            a(edit, i2, a.c(i4), intValue);
        }
        a(edit, i2, str);
        edit.putInt("setting_num", size);
        edit.commit();
    }

    public void i(int i, int i2) {
        SharedPreferences.Editor edit = this.hQ.edit();
        for (int i3 = i + 1; i3 <= i2; i3++) {
            edit.putInt("mode_id" + (i3 - 1), this.hQ.getInt("mode_id" + i3, 0));
        }
        edit.remove("mode_id" + i2);
        int i4 = this.hQ.getInt("mode_id" + i, 0);
        int i5 = this.hQ.getInt("setting_num", 0);
        for (int i6 = 0; i6 < i5; i6++) {
            a(edit, i4, i6);
        }
        a(edit, i4);
        edit.putInt("ModeCounts", i2);
        edit.commit();
    }

    private void a(SharedPreferences.Editor editor, int i, int i2, int i3) {
        if (i2 == 0) {
            editor.putInt("mode" + i + "-brightness", i3);
        } else if (i2 == 1) {
            editor.putInt("mode" + i + "-screen_timeout", i3);
        } else if (i2 == 2) {
            editor.putInt("mode" + i + "-wifi", i3);
        } else if (i2 == 3) {
            editor.putInt("mode" + i + "-bluetooth", i3);
        } else if (i2 == 4) {
            editor.putInt("mode" + i + "-mobile_data", i3);
        } else if (i2 == 10) {
            editor.putInt("mode" + i + "-bk_data", i3);
        } else if (i2 == 5) {
            editor.putInt("mode" + i + "-auto_sync", i3);
        } else if (i2 == 6) {
            editor.putInt("mode" + i + "-virbate", i3);
        } else if (i2 == 7) {
            editor.putInt("mode" + i + "-haptic_feedback", i3);
        } else if (i2 == 8) {
            editor.putInt("mode" + i + "-auto_clean", i3);
        } else if (i2 == 9) {
            editor.putInt("mode" + i + "-gps", i3);
        } else if (i2 == 11) {
            editor.putInt("mode" + i + "-only2g", i3);
        }
    }

    public int get(int i, int i2) {
        int i3 = this.hQ.getInt("mode_id" + i, 0);
        if (i2 == 0) {
            return this.hQ.getInt("mode" + i3 + "-brightness", 0);
        } else if (i2 == 1) {
            return this.hQ.getInt("mode" + i3 + "-screen_timeout", 0);
        } else if (i2 == 2) {
            return this.hQ.getInt("mode" + i3 + "-wifi", 0);
        } else if (i2 == 3) {
            return this.hQ.getInt("mode" + i3 + "-bluetooth", 0);
        } else if (i2 == 4) {
            return this.hQ.getInt("mode" + i3 + "-mobile_data", 0);
        } else if (i2 == 10) {
            return this.hQ.getInt("mode" + i3 + "-bk_data", 0);
        } else if (i2 == 5) {
            return this.hQ.getInt("mode" + i3 + "-auto_sync", 0);
        } else if (i2 == 6) {
            return this.hQ.getInt("mode" + i3 + "-virbate", 0);
        } else if (i2 == 7) {
            return this.hQ.getInt("mode" + i3 + "-haptic_feedback", 0);
        } else if (i2 == 8) {
            return this.hQ.getInt("mode" + i3 + "-auto_clean", 0);
        } else if (i2 == 9) {
            return this.hQ.getInt("mode" + i3 + "-gps", 0);
        } else if (i2 != 11) {
            return -1;
        } else {
            return this.hQ.getInt("mode" + i3 + "-only2g", 0);
        }
    }

    private void a(SharedPreferences.Editor editor, int i, int i2) {
        String str = "";
        if (i2 == 0) {
            str = "mode" + i + "-brightness";
        } else if (i2 == 1) {
            str = "mode" + i + "-screen_timeout";
        } else if (i2 == 2) {
            str = "mode" + i + "-wifi";
        } else if (i2 == 3) {
            str = "mode" + i + "-bluetooth";
        } else if (i2 == 4) {
            str = "mode" + i + "-mobile_data";
        } else if (i2 == 10) {
            str = "mode" + i + "-bk_data";
        } else if (i2 == 5) {
            str = "mode" + i + "-auto_sync";
        } else if (i2 == 6) {
            str = "mode" + i + "-virbate";
        } else if (i2 == 7) {
            str = "mode" + i + "-haptic_feedback";
        } else if (i2 == 8) {
            str = "mode" + i + "-auto_clean";
        } else if (i2 == 9) {
            str = "mode" + i + "-gps";
        } else if (i2 == 11) {
            str = "mode" + i + "-only2g";
        }
        editor.remove(str);
    }

    public int aw() {
        return this.hQ.getInt("ModeCounts", 3);
    }

    public int bF() {
        return this.hQ.getInt("ModeSelected", 1);
    }

    public void M(int i) {
        SharedPreferences.Editor edit = this.hQ.edit();
        edit.putInt("ModeSelected", i);
        edit.commit();
    }

    public String t(int i) {
        return this.hQ.getString("mode" + this.hQ.getInt("mode_id" + i, 0) + "-name", "");
    }

    private void a(SharedPreferences.Editor editor, int i, String str) {
        editor.putString("mode" + i + "-name", str);
    }

    private void a(SharedPreferences.Editor editor, int i) {
        editor.remove("mode" + i + "-name");
    }

    public boolean j(int i, int i2) {
        SharedPreferences sharedPreferences = this.mContext.getSharedPreferences("ModeSetting", 0);
        if (sharedPreferences.getInt("ItemCounts", 3) <= 3) {
            return false;
        }
        int i3 = sharedPreferences.getInt("ItemCounts", 3);
        int i4 = sharedPreferences.getInt("Selected", 1);
        SharedPreferences.Editor edit = this.mContext.getSharedPreferences("mode_settings", 0).edit();
        edit.putInt("ModeCounts", i3);
        edit.putInt("ModeSelected", i4);
        edit.putInt("ModeCounter", i3);
        edit.putInt("setting_num", i2);
        for (int i5 = 3; i5 < i3; i5++) {
            String string = sharedPreferences.getString("item" + i5, "");
            edit.putInt("mode_id" + i5, i5);
            a(edit, i5, string);
            for (int i6 = 0; i6 < i; i6++) {
                int i7 = sharedPreferences.getInt("item" + i5 + "-" + i6, 0);
                if (j.cf()) {
                    a(edit, i5, i6, i7);
                } else if (i6 < 4) {
                    a(edit, i5, i6, i7);
                } else {
                    a(edit, i5, i6 + 1, i7);
                }
            }
        }
        SharedPreferences.Editor edit2 = sharedPreferences.edit();
        edit2.clear();
        edit2.commit();
        edit.commit();
        return true;
    }
}
