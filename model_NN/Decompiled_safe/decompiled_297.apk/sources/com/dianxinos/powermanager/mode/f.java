package com.dianxinos.powermanager.mode;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

/* compiled from: ModeMgrActivity */
class f implements View.OnClickListener {
    final /* synthetic */ ModeMgrActivity aA;

    f(ModeMgrActivity modeMgrActivity) {
        this.aA = modeMgrActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.dianxinos.powermanager.mode.ModeMgrActivity.a(com.dianxinos.powermanager.mode.ModeMgrActivity, boolean):boolean
     arg types: [com.dianxinos.powermanager.mode.ModeMgrActivity, int]
     candidates:
      com.dianxinos.powermanager.mode.ModeMgrActivity.a(com.dianxinos.powermanager.mode.ModeMgrActivity, int):void
      com.dianxinos.powermanager.mode.ModeMgrActivity.a(com.dianxinos.powermanager.mode.ModeMgrActivity, boolean):boolean */
    public void onClick(View view) {
        if (this.aA.fk != null) {
            this.aA.fk.dismiss();
            Intent intent = new Intent(this.aA, NewModeActivity.class);
            Bundle bundle = new Bundle();
            bundle.putInt("index", this.aA.o);
            bundle.putInt("counts", this.aA.o);
            intent.putExtras(bundle);
            this.aA.startActivityForResult(intent, 1);
            boolean unused = this.aA.fq = true;
        }
    }
}
