package com.dianxinos.powermanager.mode;

import android.os.AsyncTask;

/* compiled from: ModeSelectDialog */
class b extends AsyncTask {
    final /* synthetic */ ModeSelectDialog af;

    b(ModeSelectDialog modeSelectDialog) {
        this.af = modeSelectDialog;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Void doInBackground(String... strArr) {
        this.af.ab.r(this.af.Y);
        this.af.ac.m(this.af.bx);
        this.af.ac.Z(this.af.Y);
        if (this.af.ac.Y(this.af.Y)) {
            publishProgress(1);
        }
        publishProgress(2);
        return null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onProgressUpdate(Integer... numArr) {
        int intValue = numArr[0].intValue();
        if (intValue == 1) {
            this.af.P();
        } else if (intValue == 2) {
            this.af.finish();
        }
    }
}
