package com.dianxinos.powermanager.mode;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.dianxinos.powermanager.C0000R;
import com.dianxinos.powermanager.a.u;
import java.util.ArrayList;
import java.util.Iterator;

public class NewModeActivity extends Activity implements View.OnClickListener {
    private o ei;
    private i fp;
    private int hR;
    private ArrayList hS;
    private s hT;
    private Button hU;
    private Button hV;
    private EditText hW;
    private boolean hX;
    /* access modifiers changed from: private */
    public boolean hY;
    private ArrayList hZ;
    private int mIndex;
    private LayoutInflater mInflater;

    private boolean N(int i) {
        if (((u) this.hT.cC().get(a.c(i))).i() > 2) {
            return false;
        }
        return true;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.new_mode_mgr);
        Bundle extras = getIntent().getExtras();
        this.mIndex = extras.getInt("index");
        int i = extras.getInt("counts");
        new View(this);
        this.hW = (EditText) findViewById(C0000R.id.nameedit);
        bH();
        this.fp = i.t(this);
        this.ei = new o(this);
        this.hS = new ArrayList();
        this.hZ = new ArrayList();
        if (this.mIndex == i) {
            this.hX = false;
            String t = t(this.mIndex);
            this.hW.setText(t);
            this.hW.setSelection(t.length());
        } else {
            String t2 = this.ei.t(this.mIndex);
            this.hW.setText(t2);
            this.hW.setSelection(t2.length());
            this.hX = true;
        }
        this.hU = (Button) findViewById(C0000R.id.save);
        this.hU.setOnClickListener(this);
        this.hV = (Button) findViewById(C0000R.id.cancel);
        this.hV.setOnClickListener(this);
        this.mInflater = LayoutInflater.from(this);
        this.hT = this.fp.aB();
        this.hR = this.hT.size();
        bI();
    }

    private String t(int i) {
        if (i == 3) {
            return getString(C0000R.string.mode_customed) + (i - 2);
        }
        this.hZ.clear();
        for (int i2 = 3; i2 < i; i2++) {
            this.hZ.add(this.ei.t(i2));
        }
        String str = "";
        for (int i3 = 3; i3 <= i; i3++) {
            str = getString(C0000R.string.mode_customed) + (i3 - 2);
            if (!this.hZ.contains(str)) {
                return str;
            }
        }
        return str;
    }

    private boolean bG() {
        if (!this.hY) {
            return false;
        }
        this.hZ.add(getString(C0000R.string.mode_label_longest_standby));
        this.hZ.add(getString(C0000R.string.mode_label_sleep));
        this.hZ.add(getString(C0000R.string.mode_label_balance));
        return this.hZ.contains(this.hW.getText().toString());
    }

    private void bH() {
        this.hY = false;
        this.hW.setOnEditorActionListener(new j(this));
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    public void onClick(View view) {
        if (view == this.hU) {
            if (this.hW.getText().toString().equals("")) {
                Toast.makeText(this, getString(C0000R.string.mode_nomodename_prompt), 0).show();
            } else if (bG()) {
                Toast.makeText(this, getString(C0000R.string.mode_modename_same_prompt), 0).show();
            } else {
                ArrayList arrayList = new ArrayList();
                Iterator it = this.hS.iterator();
                while (it.hasNext()) {
                    arrayList.add(Integer.valueOf(((n) it.next()).getIndex()));
                }
                this.ei.a(this.mIndex, this.hW.getText().toString(), arrayList, !this.hX);
                Intent intent = new Intent(this, ModeMgrActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("ModeName", this.hW.getText().toString());
                bundle.putInt("index", this.mIndex);
                intent.putExtras(bundle);
                setResult(-1, intent);
                finish();
            }
        } else if (view == this.hV) {
            finish();
        } else if (view != this.hW) {
            for (int i = 0; i < this.hR; i++) {
                n nVar = (n) this.hS.get(i);
                if (view.getTag().equals(nVar)) {
                    if (nVar.bz()) {
                        nVar.bA();
                    } else {
                        Intent intent2 = new Intent(this, SettingListDialog.class);
                        Bundle bundle2 = new Bundle();
                        if (i == 0) {
                            bundle2.putInt("title", 0);
                        } else if (i == 1) {
                            bundle2.putInt("title", 1);
                        }
                        bundle2.putInt("counts", this.hT.X(a.c(i)).i());
                        bundle2.putInt("Selected", ((n) this.hS.get(i)).getIndex());
                        intent2.putExtras(bundle2);
                        startActivityForResult(intent2, i + 3);
                    }
                }
            }
        }
    }

    private void bI() {
        LinearLayout linearLayout = (LinearLayout) findViewById(C0000R.id.newmode_setting_items);
        for (int i = 0; i < this.hR; i++) {
            if (!this.hT.X(a.c(i)).j()) {
                this.hS.add(new n());
            } else {
                linearLayout.addView(aq());
                if (!N(i)) {
                    linearLayout.addView(P(i));
                } else {
                    linearLayout.addView(Q(i));
                }
            }
        }
    }

    private void O(int i) {
        float f = 1.0f;
        if (i == 4) {
            f = 0.2f;
        } else if (i == 2) {
            f = 0.5f;
        } else if (i == 1) {
            f = 0.3f;
        } else if (i == 0) {
            f = 0.11764706f;
        }
        WindowManager.LayoutParams attributes = getWindow().getAttributes();
        attributes.screenBrightness = f;
        getWindow().setAttributes(attributes);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i == 3) {
            switch (i2) {
                case -1:
                    Bundle extras = intent.getExtras();
                    String string = extras.getString("SelectedValue");
                    int i3 = extras.getInt("SelectedItem");
                    O(i3);
                    ((n) this.hS.get(i - 3)).hJ.setText(string);
                    ((n) this.hS.get(i - 3)).setIndex(i3);
                    return;
                default:
                    return;
            }
        } else if (i == 4) {
            switch (i2) {
                case -1:
                    Bundle extras2 = intent.getExtras();
                    ((n) this.hS.get(i - 3)).hJ.setText(extras2.getString("SelectedValue"));
                    ((n) this.hS.get(i - 3)).setIndex(extras2.getInt("SelectedItem"));
                    return;
                default:
                    return;
            }
        }
    }

    private View P(int i) {
        int i2;
        String str;
        View inflate = this.mInflater.inflate((int) C0000R.layout.mode_normal_setting_item, (ViewGroup) null);
        ((TextView) inflate.findViewById(C0000R.id.label)).setText(a.d(i));
        TextView textView = (TextView) inflate.findViewById(C0000R.id.settingvalue);
        String valueString = this.hT.X(a.c(i)).getValueString();
        int index = this.hT.X(a.c(i)).getIndex();
        if (this.hX) {
            int i3 = this.ei.get(this.mIndex, a.c(i));
            i2 = i3;
            str = (String) this.hT.X(a.c(i)).h().get(i3);
        } else {
            i2 = index;
            str = valueString;
        }
        textView.setText(str);
        ((TextView) inflate.findViewById(C0000R.id.detail)).setText(a.e(i));
        n nVar = new n(this, i2, false, false, str, textView);
        this.hS.add(nVar);
        inflate.setTag(nVar);
        inflate.setOnClickListener(this);
        return inflate;
    }

    private View Q(int i) {
        boolean z;
        int i2;
        View inflate = this.mInflater.inflate((int) C0000R.layout.mode_switch_setting_item, (ViewGroup) null);
        ((TextView) inflate.findViewById(C0000R.id.label)).setText(a.d(i));
        TextView textView = (TextView) inflate.findViewById(C0000R.id.switchicon);
        boolean g = this.hT.X(a.c(i)).g();
        if (!this.hX) {
            z = g;
        } else if (this.ei.get(this.mIndex, a.c(i)) == 0) {
            z = false;
        } else {
            z = true;
        }
        if (z) {
            i2 = 1;
        } else {
            i2 = 0;
        }
        ((TextView) inflate.findViewById(C0000R.id.detail)).setText(a.e(i));
        n nVar = new n(this, i2, true, z, null, textView);
        this.hS.add(nVar);
        inflate.setTag(nVar);
        inflate.setOnClickListener(this);
        return inflate;
    }

    private View aq() {
        ImageView imageView = new ImageView(this);
        imageView.setImageDrawable(getResources().getDrawable(C0000R.drawable.horizontal_line));
        return imageView;
    }
}
