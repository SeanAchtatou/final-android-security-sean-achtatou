package com.dianxinos.powermanager.mode;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.dianxinos.powermanager.C0000R;
import java.util.ArrayList;

public class SettingListDialog extends Activity implements View.OnClickListener {
    private static int[] ad = new int[2];
    private LinearLayout X;
    private int Y;
    private boolean Z;
    private int aa;
    private i ab;
    private s ac;
    private LayoutInflater mInflater;
    private int o;
    private ArrayList y;

    private void x() {
        Resources resources = getResources();
        ad[0] = resources.getColor(C0000R.color.mode_nomal_color);
        ad[1] = resources.getColor(C0000R.color.mode_selected_color);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.ab = i.t(this);
        this.ac = this.ab.aB();
        x();
        Bundle extras = getIntent().getExtras();
        this.o = extras.getInt("counts");
        this.Y = extras.getInt("Selected");
        String str = "";
        int i = extras.getInt("title");
        if (i == 20) {
            str = getString(C0000R.string.mode_select_mode);
            setContentView((int) C0000R.layout.mode_select_dialog);
            this.Z = true;
            this.aa = extras.getInt("changed");
            this.y = this.ab.ay();
        } else if (i == 0) {
            String string = getString(C0000R.string.mode_newmode_light_setting);
            setContentView((int) C0000R.layout.setting_dialog);
            this.Z = false;
            this.y = this.ac.X(0).h();
            str = string;
        } else if (i == 1) {
            String string2 = getString(C0000R.string.mode_newmode_screen_timeout_setting);
            setContentView((int) C0000R.layout.setting_dialog);
            this.Z = false;
            this.y = this.ac.X(1).h();
            str = string2;
        }
        ((TextView) findViewById(C0000R.id.settingdialog_title)).setText(str);
        this.mInflater = LayoutInflater.from(this);
        y();
    }

    private void y() {
        this.X = (LinearLayout) findViewById(C0000R.id.item_value_lists);
        for (int i = 0; i < this.o; i++) {
            this.X.addView(i(i));
        }
    }

    private View i(int i) {
        View inflate;
        if (this.Z) {
            inflate = this.mInflater.inflate((int) C0000R.layout.select_mode_list_item, (ViewGroup) null);
        } else {
            inflate = this.mInflater.inflate((int) C0000R.layout.pref_setting_list_item, (ViewGroup) null);
        }
        TextView textView = (TextView) inflate.findViewById(C0000R.id.value);
        textView.setText((CharSequence) this.y.get(i));
        if (i == this.Y) {
            ((ImageView) inflate.findViewById(C0000R.id.itemimage)).setVisibility(0);
            textView.setTextColor(ad[1]);
            if (this.Z) {
                if (this.aa == 1) {
                    ((TextView) inflate.findViewById(C0000R.id.modified)).setVisibility(0);
                } else {
                    ((TextView) inflate.findViewById(C0000R.id.modified)).setVisibility(8);
                }
            }
        }
        inflate.setTag(Integer.valueOf(i));
        inflate.setOnClickListener(this);
        return inflate;
    }

    public void onClick(View view) {
        int i = 0;
        while (true) {
            if (i >= this.o) {
                break;
            } else if (view.getTag().equals(Integer.valueOf(i))) {
                ImageView imageView = (ImageView) this.X.getChildAt(i).findViewById(C0000R.id.itemimage);
                TextView textView = (TextView) this.X.getChildAt(i).findViewById(C0000R.id.value);
                if (i != this.Y) {
                    ((ImageView) this.X.getChildAt(this.Y).findViewById(C0000R.id.itemimage)).setVisibility(4);
                    ((TextView) this.X.getChildAt(this.Y).findViewById(C0000R.id.value)).setTextColor(ad[0]);
                    imageView.setVisibility(0);
                    textView.setTextColor(ad[1]);
                    this.Y = i;
                    Intent intent = new Intent(this, NewModeActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("SelectedValue", (String) this.y.get(this.Y));
                    bundle.putInt("SelectedItem", this.Y);
                    intent.putExtras(bundle);
                    setResult(-1, intent);
                }
            } else {
                i++;
            }
        }
        finish();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }
}
