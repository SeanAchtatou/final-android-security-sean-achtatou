package com.dianxinos.powermanager.mode;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.dianxinos.powermanager.a.a;
import com.dianxinos.powermanager.a.d;
import com.dianxinos.powermanager.a.g;
import com.dianxinos.powermanager.a.h;
import com.dianxinos.powermanager.a.i;
import com.dianxinos.powermanager.a.j;
import com.dianxinos.powermanager.a.k;
import com.dianxinos.powermanager.a.m;
import com.dianxinos.powermanager.a.n;
import com.dianxinos.powermanager.a.o;
import com.dianxinos.powermanager.a.q;
import com.dianxinos.powermanager.a.u;
import com.dianxinos.powermanager.a.x;
import com.dianxinos.powermanager.a.z;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Timer;

/* compiled from: ModeCommand */
public class s implements i {
    private static s kw;
    private i ab;
    private BroadcastReceiver kA = new v(this);
    private ArrayList kq;
    private ArrayList kr;
    private ArrayList ks;
    private ArrayList kt;
    private ArrayList ku;
    private t kv;
    private o kx;
    /* access modifiers changed from: private */
    public boolean ky;
    private Timer kz;
    private Context mContext;
    private int mSize;

    public static s a(Context context, i iVar) {
        if (kw == null) {
            kw = new s(context, iVar);
        }
        return kw;
    }

    private s(Context context, i iVar) {
        this.mContext = context;
        this.ab = iVar;
        cD();
        this.kx = new o(this.mContext);
        this.kr = new ArrayList();
        this.ks = new ArrayList();
        this.kt = new ArrayList();
        this.ku = new ArrayList();
        cN();
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        super.finalize();
        this.mContext.unregisterReceiver(this.kA);
    }

    public ArrayList cC() {
        return this.kq;
    }

    public u X(int i) {
        return (u) this.kq.get(i);
    }

    private void cD() {
        this.kq = new ArrayList();
        this.kq.add(new d(this.mContext));
        this.kq.add(new m(this.mContext));
        this.kq.add(new h(this.mContext));
        this.kq.add(new n(this.mContext));
        this.kq.add(new z(this.mContext));
        this.kq.add(new a(this.mContext));
        this.kq.add(new q(this.mContext));
        this.kq.add(new x(this.mContext));
        this.kq.add(new com.dianxinos.powermanager.a.s(this.mContext));
        this.kq.add(new g(this.mContext));
        this.kq.add(new j(this.mContext));
        this.kq.add(new k(this.mContext));
        this.mSize = this.kq.size();
    }

    public int size() {
        return this.mSize;
    }

    public int cE() {
        int i = 0;
        Iterator it = this.kq.iterator();
        while (it.hasNext()) {
            if (((u) it.next()).j()) {
                i++;
            }
        }
        return i;
    }

    public ArrayList cF() {
        return this.kr;
    }

    public ArrayList cG() {
        return this.kt;
    }

    public boolean Y(int i) {
        if (this.kz != null) {
            this.kz.cancel();
        }
        this.ky = true;
        this.kz = new Timer();
        this.kz.schedule(new w(this), 10000);
        boolean g = this.kx.g();
        if (i != 2 && g) {
            this.kx.a(false);
        }
        boolean z = false;
        for (int i2 = 0; i2 < this.mSize; i2++) {
            if (cJ().get(i2) != cF().get(i2)) {
                l(i, i2);
                if (i2 == 0) {
                    z = true;
                }
            }
        }
        if (i == 2 && !g) {
            this.kx.a(true);
        }
        this.ab.v(i);
        return z;
    }

    public void l(int i, int i2) {
        u uVar = (u) this.kq.get(i2);
        if (uVar.j()) {
            int intValue = ((Integer) this.kr.get(i2)).intValue();
            uVar.a(intValue);
            this.ks.set(i2, Integer.valueOf(intValue));
            this.kt.set(i2, Integer.valueOf(uVar.b(intValue)));
        }
    }

    public void Z(int i) {
        if (this.kr.size() == 0) {
            this.kr.addAll(this.ab.aA().E(i));
            return;
        }
        ArrayList E = this.ab.aA().E(i);
        for (int i2 = 0; i2 < this.mSize; i2++) {
            this.kr.set(i2, E.get(i2));
        }
    }

    public void cH() {
        if (this.ks.isEmpty()) {
            for (int i = 0; i < this.mSize; i++) {
                this.ks.add(Integer.valueOf(X(i).getIndex()));
                this.kt.add(Integer.valueOf(X(i).getValue()));
            }
        } else if (((u) this.kq.get(4)).j()) {
            this.ks.set(4, Integer.valueOf(X(4).getIndex()));
            this.kt.set(4, Integer.valueOf(X(4).getValue()));
        } else if (((u) this.kq.get(9)).j()) {
            this.ks.set(9, Integer.valueOf(X(9).getIndex()));
            this.kt.set(9, Integer.valueOf(X(9).getValue()));
        }
    }

    public boolean cI() {
        for (int i = 0; i < this.mSize; i++) {
            if (X(i).j() && !((Integer) this.ks.get(i)).equals(this.kr.get(i))) {
                return false;
            }
        }
        if (this.ab.ax() == 2 && !this.kx.g()) {
            return false;
        }
        if (this.ab.ax() == 2 || !this.kx.g()) {
            return true;
        }
        return false;
    }

    public void m(boolean z) {
        this.ku.clear();
        for (int i = 0; i < this.mSize; i++) {
            if (z) {
                this.ku.add(this.ks.get(i));
            } else {
                this.ku.add(this.kr.get(i));
            }
        }
    }

    public ArrayList cJ() {
        return this.ku;
    }

    public void a(u uVar, int i, int i2) {
        if (uVar.equals(this.kx)) {
            if (this.ab.ax() == 2 && i2 == 0) {
                if (this.kv != null) {
                    this.kv.aO();
                }
            } else if (!(this.ab.ax() == 2 || i2 != 1 || this.kv == null)) {
                this.kv.aO();
            }
            if (!this.ky) {
                this.ab.aC();
                return;
            }
            return;
        }
        int i3 = 0;
        while (true) {
            int i4 = i3;
            if (i4 >= this.mSize) {
                break;
            }
            if (uVar == this.kq.get(i4)) {
                if (uVar.j()) {
                    this.ks.set(i4, Integer.valueOf(i));
                }
                this.kt.set(i4, Integer.valueOf(i2));
                if (!((Integer) this.kr.get(i4)).equals(Integer.valueOf(i))) {
                    if (this.kv != null) {
                        this.kv.aO();
                    }
                } else if (cI() && this.kv != null) {
                    this.kv.aP();
                }
            }
            i3 = i4 + 1;
        }
        if (!this.ky) {
            this.ab.aC();
        }
    }

    public void a(t tVar) {
        this.kv = tVar;
    }

    public void cK() {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.mSize) {
                ((u) this.kq.get(i2)).a(this);
                i = i2 + 1;
            } else {
                this.kx.a(this);
                return;
            }
        }
    }

    public void cL() {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.mSize) {
                ((u) this.kq.get(i2)).b(this);
                i = i2 + 1;
            } else {
                this.kx.b(this);
                return;
            }
        }
    }

    public o cM() {
        return this.kx;
    }

    private void cN() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.net.wifi.WIFI_STATE_CHANGED");
        intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        intentFilter.addAction("android.media.VIBRATE_SETTING_CHANGED");
        intentFilter.addAction("android.bluetooth.adapter.action.STATE_CHANGED");
        intentFilter.addAction("android.location.PROVIDERS_CHANGED");
        this.mContext.registerReceiver(this.kA, intentFilter);
    }

    public void a(Context context, Intent intent) {
        String action = intent.getAction();
        if ("android.net.wifi.WIFI_STATE_CHANGED".equals(action)) {
            if (this.ks.size() >= 2 && this.kt.size() >= 2) {
                a(X(2), X(2).getIndex(), X(2).getValue());
            }
        } else if ("android.media.VIBRATE_SETTING_CHANGED".equals(action)) {
            if (this.ks.size() >= 6 && this.kt.size() >= 6) {
                a(X(6), X(6).getIndex(), X(6).getValue());
            }
        } else if ("android.bluetooth.adapter.action.STATE_CHANGED".equals(action)) {
            if (this.ks.size() >= 3 && this.kt.size() >= 3) {
                a(X(3), X(3).getIndex(), X(3).getValue());
            }
        } else if ("android.location.PROVIDERS_CHANGED".equals(action) && this.ks.size() >= 9 && this.kt.size() >= 9) {
            a(X(9), X(9).getIndex(), X(9).getValue());
        }
    }
}
