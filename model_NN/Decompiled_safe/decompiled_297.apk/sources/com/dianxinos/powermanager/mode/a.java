package com.dianxinos.powermanager.mode;

import com.dianxinos.powermanager.C0000R;

/* compiled from: ItemMap */
public class a {
    public static int[] r = {0, 1, 2, 11, 3, 4, 10, 5, 9, 6, 7, 8};
    public static int[] s = {C0000R.string.mode_newmode_light_setting, C0000R.string.mode_newmode_screen_timeout_setting, C0000R.string.mode_newmode_wifinet_switch, C0000R.string.mode_newmode_only2g_switch, C0000R.string.mode_newmode_bluetooth_switch, C0000R.string.mode_newmode_mobiledata_switch, C0000R.string.mode_newmode_bkdata_switch, C0000R.string.mode_newmode_autosync_switch, C0000R.string.mode_newmode_gps_switch, C0000R.string.mode_newmode_shake_switch, C0000R.string.mode_newmode_touchfbk_switch, C0000R.string.mode_auto_cleanup_bkapp};
    public static int[] t = {C0000R.string.mode_newmode_light_des, C0000R.string.mode_newmode_screen_timeout_des, C0000R.string.mode_newmode_wifinet_switch_des, C0000R.string.mode_newmode_only2g_switch_des, C0000R.string.mode_newmode_bluetooth_switch_des, C0000R.string.mode_newmode_mobiledata_switch_des, C0000R.string.mode_newmode_bkdata_switch, C0000R.string.mode_newmode_autosync_switch_des, C0000R.string.mode_newmode_gps_switch, C0000R.string.mode_newmode_shake_switch_des, C0000R.string.mode_newmode_touchfbk_switch_des, C0000R.string.mode_auto_cleanup_bkapp_des};

    public static int c(int i) {
        return r[i];
    }

    public static int d(int i) {
        return s[i];
    }

    public static int e(int i) {
        return t[i];
    }
}
