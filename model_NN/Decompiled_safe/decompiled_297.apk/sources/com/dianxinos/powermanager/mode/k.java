package com.dianxinos.powermanager.mode;

import android.view.View;

/* compiled from: ModeMgrActivity */
class k implements View.OnClickListener {
    final /* synthetic */ ModeMgrActivity aA;

    k(ModeMgrActivity modeMgrActivity) {
        this.aA = modeMgrActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.dianxinos.a.a.a.a(java.lang.String, java.lang.String, java.lang.Number):boolean
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.dianxinos.a.a.a.a(java.lang.String, int, java.lang.Object):boolean
      com.dianxinos.a.a.a.a(java.lang.String, java.lang.String, java.lang.Number):boolean */
    public void onClick(View view) {
        if (this.aA.fk != null) {
            this.aA.z(this.aA.fl);
            this.aA.fk.dismiss();
            if (this.aA.fl == 0) {
                this.aA.by.a("mode", "longest", (Number) 1);
            } else if (this.aA.fl == 1) {
                this.aA.by.a("mode", "normal", (Number) 1);
            } else if (this.aA.fl == 2) {
                this.aA.by.a("mode", "sleep", (Number) 1);
            }
            this.aA.by.a("mode", "switch", (Number) 1);
        }
    }
}
