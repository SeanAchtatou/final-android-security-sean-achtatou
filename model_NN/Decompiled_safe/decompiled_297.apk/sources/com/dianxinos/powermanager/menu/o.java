package com.dianxinos.powermanager.menu;

import android.app.Activity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;
import com.dianxinos.powermanager.C0000R;

/* compiled from: MenuManager */
public class o {
    private Activity mActivity;

    public o(Activity activity) {
        this.mActivity = activity;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case C0000R.id.about /*2131427474*/:
                Toast.makeText(this.mActivity, "about", 0).show();
                return true;
            case C0000R.id.exit /*2131427475*/:
                this.mActivity.setResult(-2);
                this.mActivity.finish();
                return true;
            default:
                return false;
        }
    }
}
