package com.dianxinos.powermanager.menu;

import android.app.Activity;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Proxy;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.dianxinos.powermanager.C0000R;

public class FeedbackDialog extends Activity implements View.OnClickListener {
    private Button hV;
    private EditText js;
    /* access modifiers changed from: private */
    public Button jt;
    private Handler mHandler;

    public void onCreate(Bundle bundle) {
        String cp;
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.feedback_dialog);
        String str = "";
        if (bundle != null) {
            String string = bundle.getString("content");
            if (string != null) {
                str = string;
            }
            cp = str;
        } else {
            cp = cp();
        }
        this.js = (EditText) findViewById(C0000R.id.input_editor);
        this.js.setText(cp);
        this.jt = (Button) findViewById(C0000R.id.send_btn);
        this.jt.setOnClickListener(this);
        this.hV = (Button) findViewById(C0000R.id.cancel_btn);
        this.hV.setOnClickListener(this);
        this.js.addTextChangedListener(new l(this));
        this.jt.setEnabled(cp != null && cp.length() > 0);
        HandlerThread handlerThread = new HandlerThread("feedback-send");
        handlerThread.start();
        this.mHandler = new g(this, handlerThread.getLooper());
    }

    private String cp() {
        return getSharedPreferences("feedback", 0).getString("content", "");
    }

    /* access modifiers changed from: private */
    public void E(String str) {
        SharedPreferences.Editor edit = getSharedPreferences("feedback", 0).edit();
        edit.putString("content", str);
        edit.commit();
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (this.js != null) {
            bundle.putString("content", this.js.getText().toString());
        }
    }

    public void onClick(View view) {
        if (view == this.jt) {
            String obj = this.js.getText().toString();
            if (obj.length() > 0) {
                E(obj);
                this.mHandler.sendMessage(this.mHandler.obtainMessage(1, obj));
                finish();
            }
        } else if (view == this.hV) {
            E("");
            finish();
        }
    }

    public void onBackPressed() {
        E("");
        finish();
    }

    /* access modifiers changed from: private */
    public String e() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo == null || activeNetworkInfo.getType() != 0) {
            return null;
        }
        return Proxy.getDefaultHost();
    }

    /* access modifiers changed from: private */
    public int f() {
        return Proxy.getDefaultPort();
    }
}
