package com.dianxinos.powermanager.menu;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.dianxinos.a.a.a;
import com.dianxinos.powermanager.C0000R;

public class AppSettings extends Activity implements View.OnClickListener {
    private static int[] ad = new int[2];
    private a by;
    private RelativeLayout eX;
    private int eY;
    private TextView eZ;
    private LinearLayout fa;
    private ContentResolver mResolver;

    private void x() {
        Resources resources = getResources();
        ad[0] = resources.getColor(C0000R.color.mode_nomal_color);
        ad[1] = resources.getColor(C0000R.color.mode_back_color);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.app_settings);
        x();
        ((TextView) findViewById(C0000R.id.setting_title)).setText(getString(C0000R.string.app_settings_title, new Object[]{getString(C0000R.string.app_name)}));
        this.eX = (RelativeLayout) findViewById(C0000R.id.statusbar_setting);
        this.eX.setOnClickListener(this);
        this.eZ = (TextView) findViewById(C0000R.id.switchicon);
        this.fa = (LinearLayout) findViewById(C0000R.id.whiteapp_settings);
        this.fa.setOnClickListener(this);
        this.mResolver = getContentResolver();
        this.eY = Settings.System.getInt(this.mResolver, "com.dianxinos.powermanager.statusbar_notification", 1);
        if (this.eY == 1) {
            this.eZ.setBackgroundResource(C0000R.drawable.newmode_setting_switch_on);
            this.eZ.setText(getString(C0000R.string.mode_newmode_on));
            this.eZ.setTextColor(ad[1]);
        } else {
            this.eZ.setBackgroundResource(C0000R.drawable.newmode_setting_switch_off);
            this.eZ.setText(getString(C0000R.string.mode_newmode_off));
            this.eZ.setTextColor(ad[0]);
        }
        this.by = a.F(this);
        this.by.init();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.by.destroy();
        super.onDestroy();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.dianxinos.a.a.a.a(java.lang.String, java.lang.String, java.lang.Number):boolean
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.dianxinos.a.a.a.a(java.lang.String, int, java.lang.Object):boolean
      com.dianxinos.a.a.a.a(java.lang.String, java.lang.String, java.lang.Number):boolean */
    public void onClick(View view) {
        if (view == this.eX) {
            this.eY = 1 - this.eY;
            if (this.eY == 1) {
                this.eZ.setBackgroundResource(C0000R.drawable.newmode_setting_switch_on);
                this.eZ.setText(getString(C0000R.string.mode_newmode_on));
                this.eZ.setTextColor(ad[1]);
                this.by.a("statusbar", "enable", (Number) 1);
            } else {
                this.eZ.setBackgroundResource(C0000R.drawable.newmode_setting_switch_off);
                this.eZ.setText(getString(C0000R.string.mode_newmode_off));
                this.eZ.setTextColor(ad[0]);
                this.by.a("statusbar", "disable", (Number) 1);
            }
            Settings.System.putInt(this.mResolver, "com.dianxinos.powermanager.statusbar_notification", this.eY);
        } else if (view == this.fa) {
            startActivity(new Intent(this, AppWhiteListActivity.class));
        }
    }
}
