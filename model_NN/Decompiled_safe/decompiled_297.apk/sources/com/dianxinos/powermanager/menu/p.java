package com.dianxinos.powermanager.menu;

import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import java.util.Set;

/* compiled from: AppWhiteListActivity */
public class p extends AsyncTask {
    final /* synthetic */ AppWhiteListActivity q;

    public p(AppWhiteListActivity appWhiteListActivity) {
        this.q = appWhiteListActivity;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Void doInBackground(Void... voidArr) {
        bC();
        return null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onProgressUpdate(Integer... numArr) {
        if (numArr[0].intValue() == 1) {
            this.q.lN.a(this.q.dU);
        }
    }

    private void bC() {
        this.q.dU.clear();
        Set<String> ci = this.q.dW.ci();
        PackageManager packageManager = this.q.getPackageManager();
        for (String str : ci) {
            j jVar = new j(this.q);
            String unused = jVar.ai = str;
            jVar.al = true;
            try {
                String unused2 = jVar.ah = packageManager.getApplicationLabel(packageManager.getApplicationInfo(str, 0)).toString();
                Drawable unused3 = jVar.aj = packageManager.getApplicationIcon(str);
            } catch (Exception e) {
                e.printStackTrace();
                String unused4 = jVar.ah = str;
                Drawable unused5 = jVar.aj = this.q.getResources().getDrawable(17301651);
            }
            this.q.dU.add(jVar);
        }
        publishProgress(1);
    }
}
