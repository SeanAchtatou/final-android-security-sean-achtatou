package com.dianxinos.powermanager.menu;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ResolveInfo;
import android.view.View;

/* compiled from: ShareDialog */
class n implements View.OnClickListener {
    final /* synthetic */ ResolveInfo jK;
    final /* synthetic */ d jL;

    n(d dVar, ResolveInfo resolveInfo) {
        this.jL = dVar;
        this.jK = resolveInfo;
    }

    public void onClick(View view) {
        this.jL.dismiss();
        Intent intent = new Intent(this.jL.bz);
        ActivityInfo activityInfo = this.jK.activityInfo;
        intent.setComponent(new ComponentName(activityInfo.applicationInfo.packageName, activityInfo.name));
        this.jL.mContext.startActivity(intent);
    }
}
