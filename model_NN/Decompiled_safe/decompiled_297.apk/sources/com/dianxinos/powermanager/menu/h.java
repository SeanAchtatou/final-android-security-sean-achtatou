package com.dianxinos.powermanager.menu;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.SystemClock;
import java.util.List;

/* compiled from: AppListActivity */
public class h extends AsyncTask {
    final /* synthetic */ AppListActivity ag;

    public h(AppListActivity appListActivity) {
        this.ag = appListActivity;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Void doInBackground(Void... voidArr) {
        bC();
        return null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onProgressUpdate(Integer... numArr) {
        if (numArr[0].intValue() == 1) {
            this.ag.dV.a(this.ag.dU);
        }
    }

    private void bC() {
        this.ag.dU.clear();
        PackageManager packageManager = this.ag.getPackageManager();
        List<PackageInfo> installedPackages = packageManager.getInstalledPackages(0);
        for (int i = 0; i < installedPackages.size(); i++) {
            SystemClock.elapsedRealtime();
            PackageInfo packageInfo = installedPackages.get(i);
            if (!"com.dianxinos.powermanager".equals(packageInfo.packageName)) {
                c cVar = new c(this.ag);
                if (this.ag.dW.B(packageInfo.packageName)) {
                    cVar.al = true;
                }
                try {
                    boolean unused = cVar.ak = (packageManager.getApplicationInfo(packageInfo.packageName, 8704).flags & 1) != 0;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (this.ag.dT) {
                    String unused2 = cVar.ai = packageInfo.packageName;
                    String unused3 = cVar.ah = packageInfo.applicationInfo.loadLabel(this.ag.getPackageManager()).toString();
                    Drawable unused4 = cVar.aj = packageInfo.applicationInfo.loadIcon(this.ag.getPackageManager());
                    this.ag.dU.add(cVar);
                } else if (!cVar.ak) {
                    String unused5 = cVar.ai = packageInfo.packageName;
                    String unused6 = cVar.ah = packageInfo.applicationInfo.loadLabel(this.ag.getPackageManager()).toString();
                    Drawable unused7 = cVar.aj = packageInfo.applicationInfo.loadIcon(this.ag.getPackageManager());
                    this.ag.dU.add(cVar);
                }
                if (this.ag.dU.size() % 10 == 0) {
                    publishProgress(1);
                }
            }
        }
        publishProgress(1);
    }
}
