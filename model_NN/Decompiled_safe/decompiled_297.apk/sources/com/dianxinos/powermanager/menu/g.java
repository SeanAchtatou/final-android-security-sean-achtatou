package com.dianxinos.powermanager.menu;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.widget.Toast;
import com.dianxinos.a.a.b;
import com.dianxinos.powermanager.C0000R;
import com.dianxinos.powermanager.c.e;
import java.util.ArrayList;
import org.apache.http.HttpHost;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

/* compiled from: FeedbackDialog */
class g extends Handler {
    final /* synthetic */ FeedbackDialog gw;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public g(FeedbackDialog feedbackDialog, Looper looper) {
        super(looper);
        this.gw = feedbackDialog;
    }

    public void handleMessage(Message message) {
        if (message.what == 1) {
            q((String) message.obj);
        }
    }

    private void q(String str) {
        Context applicationContext = this.gw.getApplicationContext();
        try {
            String str2 = this.gw.getPackageManager().getPackageInfo("com.dianxinos.powermanager", 0).versionName;
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost("http://pasta.dianxinos.com/feedback");
            String b = this.gw.e();
            int c = this.gw.f();
            if (!(b == null || c == -1)) {
                defaultHttpClient.getParams().setParameter("http.route.default-proxy", new HttpHost(b, c, "http"));
            }
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("token", b.L(applicationContext));
            jSONObject.put("message", str);
            jSONObject.put("appName", "com.dianxinos.powermanager");
            jSONObject.put("appVersion", str2);
            ArrayList arrayList = new ArrayList();
            arrayList.add(new BasicNameValuePair("question", jSONObject.toString()));
            httpPost.setEntity(new UrlEncodedFormEntity(arrayList, "UTF-8"));
            httpPost.getParams().setParameter("http.protocol.content-charset", "UTF-8");
            if (defaultHttpClient.execute(httpPost).getStatusLine().getStatusCode() == 200) {
                this.gw.E("");
                Toast.makeText(applicationContext, (int) C0000R.string.feedback_send_ok, 0).show();
                return;
            }
            Toast.makeText(applicationContext, (int) C0000R.string.feedback_send_fail, 0).show();
        } catch (Exception e) {
            Toast.makeText(applicationContext, (int) C0000R.string.feedback_send_fail, 0).show();
            e.f("FeedbackDialog", "Exception: " + e.toString());
        }
    }
}
