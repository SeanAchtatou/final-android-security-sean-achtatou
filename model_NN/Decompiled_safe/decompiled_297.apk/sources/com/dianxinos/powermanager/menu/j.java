package com.dianxinos.powermanager.menu;

import android.graphics.drawable.Drawable;

/* compiled from: AppWhiteListActivity */
class j {
    /* access modifiers changed from: private */
    public String ah;
    /* access modifiers changed from: private */
    public String ai;
    /* access modifiers changed from: private */
    public Drawable aj;
    private boolean ak;
    boolean al;
    final /* synthetic */ AppWhiteListActivity q;

    private j(AppWhiteListActivity appWhiteListActivity) {
        this.q = appWhiteListActivity;
        this.ah = "";
        this.ai = "";
        this.aj = null;
    }

    public String toString() {
        return "AppInfo: appname " + this.ah + " , packagename " + this.ai + ", isSys " + this.ak;
    }
}
