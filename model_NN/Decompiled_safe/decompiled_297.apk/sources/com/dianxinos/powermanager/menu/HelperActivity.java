package com.dianxinos.powermanager.menu;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;
import com.dianxinos.powermanager.C0000R;

public class HelperActivity extends Activity {
    private WebView hC;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.about_help_html);
        this.hC = (WebView) findViewById(C0000R.id.webview);
        this.hC.getSettings().setSupportZoom(false);
        this.hC.loadUrl(bw());
        super.setTitle((int) C0000R.string.menu_help_about);
    }

    public String bw() {
        return "file:///android_asset/help/help.html";
    }
}
