package com.dianxinos.powermanager.menu;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import com.dianxinos.powermanager.C0000R;
import java.util.ArrayList;

/* compiled from: AppWhiteListActivity */
public class a extends BaseAdapter {
    private Context mContext;
    private LayoutInflater mInflater = LayoutInflater.from(this.mContext);
    private int o;
    private ArrayList p;
    final /* synthetic */ AppWhiteListActivity q;

    public a(AppWhiteListActivity appWhiteListActivity, Context context) {
        this.q = appWhiteListActivity;
        this.mContext = context;
    }

    public void a(ArrayList arrayList) {
        this.p = arrayList;
        this.o = this.p.size();
        notifyDataSetChanged();
    }

    public int getCount() {
        return this.o;
    }

    public Object getItem(int i) {
        return this.q.dU.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        e eVar;
        View view2;
        if (view == null) {
            View inflate = this.mInflater.inflate((int) C0000R.layout.app_whitelist_settings_item, (ViewGroup) null);
            e eVar2 = new e(this);
            eVar2.eI = (TextView) inflate.findViewById(C0000R.id.text);
            eVar2.eJ = (ImageView) inflate.findViewById(C0000R.id.icon);
            eVar2.eK = (CheckBox) inflate.findViewById(C0000R.id.is_inwhitelist);
            inflate.setTag(eVar2);
            e eVar3 = eVar2;
            view2 = inflate;
            eVar = eVar3;
        } else {
            eVar = (e) view.getTag();
            view2 = view;
        }
        eVar.eI.setText(((j) this.p.get(i)).ah);
        eVar.eJ.setImageDrawable(((j) this.p.get(i)).aj);
        if (((j) this.q.dU.get(i)).al) {
            eVar.eK.setChecked(true);
        } else {
            eVar.eK.setChecked(false);
        }
        return view2;
    }
}
