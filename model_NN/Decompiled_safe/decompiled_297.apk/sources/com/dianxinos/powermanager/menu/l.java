package com.dianxinos.powermanager.menu;

import android.text.Editable;
import android.text.TextWatcher;

/* compiled from: FeedbackDialog */
class l implements TextWatcher {
    final /* synthetic */ FeedbackDialog gw;

    l(FeedbackDialog feedbackDialog) {
        this.gw = feedbackDialog;
    }

    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public void afterTextChanged(Editable editable) {
        this.gw.jt.setEnabled(editable.toString().trim().length() > 0);
    }
}
