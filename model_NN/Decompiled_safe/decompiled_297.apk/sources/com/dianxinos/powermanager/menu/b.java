package com.dianxinos.powermanager.menu;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import com.dianxinos.powermanager.C0000R;
import java.util.ArrayList;

/* compiled from: AppListActivity */
public class b extends BaseAdapter {
    final /* synthetic */ AppListActivity ag;
    private Context mContext;
    private LayoutInflater mInflater = LayoutInflater.from(this.mContext);
    private int o;
    private ArrayList p;

    public b(AppListActivity appListActivity, Context context) {
        this.ag = appListActivity;
        this.mContext = context;
    }

    public void a(ArrayList arrayList) {
        this.p = arrayList;
        this.o = this.p.size();
        notifyDataSetChanged();
    }

    public int getCount() {
        return this.o;
    }

    public Object getItem(int i) {
        return this.ag.dU.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        f fVar;
        View view2;
        if (view == null) {
            View inflate = this.mInflater.inflate((int) C0000R.layout.app_whitelist_settings_item, (ViewGroup) null);
            f fVar2 = new f(this);
            fVar2.eI = (TextView) inflate.findViewById(C0000R.id.text);
            fVar2.eJ = (ImageView) inflate.findViewById(C0000R.id.icon);
            fVar2.eK = (CheckBox) inflate.findViewById(C0000R.id.is_inwhitelist);
            inflate.setTag(fVar2);
            f fVar3 = fVar2;
            view2 = inflate;
            fVar = fVar3;
        } else {
            fVar = (f) view.getTag();
            view2 = view;
        }
        fVar.eI.setText(((c) this.p.get(i)).ah);
        fVar.eJ.setImageDrawable(((c) this.p.get(i)).aj);
        if (((c) this.ag.dU.get(i)).al) {
            fVar.eK.setChecked(true);
        } else {
            fVar.eK.setChecked(false);
        }
        return view2;
    }
}
