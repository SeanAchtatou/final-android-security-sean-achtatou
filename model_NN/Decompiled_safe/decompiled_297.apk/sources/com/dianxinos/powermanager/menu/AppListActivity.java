package com.dianxinos.powermanager.menu;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.dianxinos.powermanager.C0000R;
import java.util.ArrayList;

public class AppListActivity extends Activity implements View.OnClickListener, AdapterView.OnItemClickListener {
    private ListView dS;
    /* access modifiers changed from: private */
    public boolean dT;
    /* access modifiers changed from: private */
    public ArrayList dU;
    /* access modifiers changed from: private */
    public b dV;
    /* access modifiers changed from: private */
    public k dW;
    private Button dX;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.app_whitelist_settings);
        ((TextView) findViewById(C0000R.id.setting_whitelist_title)).setText((int) C0000R.string.app_settings_app_all);
        this.dX = (Button) findViewById(C0000R.id.bottom_btn_ok);
        this.dX.setOnClickListener(this);
        this.dW = k.I(this);
        this.dU = new ArrayList();
        this.dV = new b(this, this);
        this.dS = (ListView) findViewById(C0000R.id.app_white_list);
        this.dS.setAdapter((ListAdapter) this.dV);
        TextView textView = (TextView) findViewById(C0000R.id.empty);
        textView.setText((int) C0000R.string.app_settings_no_app);
        this.dS.setEmptyView(textView);
        this.dV.a(this.dU);
        this.dS.setOnItemClickListener(this);
        new h(this).execute(new Void[0]);
    }

    public void onDestroy() {
        this.dU.clear();
        super.onDestroy();
    }

    public void onItemClick(AdapterView adapterView, View view, int i, long j) {
        CheckBox checkBox = (CheckBox) view.findViewById(C0000R.id.is_inwhitelist);
        ((c) this.dU.get(i)).al = !((c) this.dU.get(i)).al;
        boolean z = ((c) this.dU.get(i)).al;
        checkBox.setChecked(z);
        if (z) {
            this.dW.C(((c) this.dU.get(i)).ai);
        } else {
            this.dW.D(((c) this.dU.get(i)).ai);
        }
    }

    public void onClick(View view) {
        if (view == this.dX) {
            finish();
        }
    }
}
