package com.dianxinos.powermanager.menu;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.dianxinos.powermanager.C0000R;
import java.util.ArrayList;

public class AppWhiteListActivity extends Activity implements View.OnClickListener, AdapterView.OnItemClickListener {
    private ListView dS;
    /* access modifiers changed from: private */
    public ArrayList dU;
    /* access modifiers changed from: private */
    public k dW;
    private Button dX;
    /* access modifiers changed from: private */
    public a lN;
    private Button lO;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.app_whitelist_settings);
        ((TextView) findViewById(C0000R.id.setting_whitelist_title)).setText((int) C0000R.string.app_settings_protected_list);
        this.lO = (Button) findViewById(C0000R.id.title_button);
        this.lO.setText((int) C0000R.string.app_settings_white_app_add);
        this.lO.setVisibility(0);
        this.lO.setOnClickListener(this);
        this.dX = (Button) findViewById(C0000R.id.bottom_btn_ok);
        this.dX.setOnClickListener(this);
        this.dU = new ArrayList();
        this.lN = new a(this, this);
        this.dS = (ListView) findViewById(C0000R.id.app_white_list);
        this.dS.setAdapter((ListAdapter) this.lN);
        TextView textView = (TextView) findViewById(C0000R.id.empty);
        textView.setText((int) C0000R.string.app_settings_no_white_app);
        this.dS.setEmptyView(textView);
        this.lN.a(this.dU);
        this.dS.setOnItemClickListener(this);
        this.dW = k.I(this);
    }

    public void onStart() {
        new p(this).execute(new Void[0]);
        super.onStart();
    }

    public void onDestroy() {
        this.dU.clear();
        super.onDestroy();
    }

    public void onItemClick(AdapterView adapterView, View view, int i, long j) {
        CheckBox checkBox = (CheckBox) view.findViewById(C0000R.id.is_inwhitelist);
        ((j) this.dU.get(i)).al = !((j) this.dU.get(i)).al;
        boolean z = ((j) this.dU.get(i)).al;
        checkBox.setChecked(z);
        if (z) {
            this.dW.C(((j) this.dU.get(i)).ai);
        } else {
            this.dW.D(((j) this.dU.get(i)).ai);
        }
    }

    public void onClick(View view) {
        if (view == this.lO) {
            startActivity(new Intent(this, AppListActivity.class));
        } else if (view == this.dX) {
            finish();
        }
    }
}
