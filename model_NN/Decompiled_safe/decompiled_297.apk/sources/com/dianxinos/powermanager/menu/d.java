package com.dianxinos.powermanager.menu;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.dianxinos.powermanager.C0000R;
import com.dianxinos.powermanager.c.e;
import java.util.List;

/* compiled from: ShareDialog */
public class d extends Dialog {
    LinearLayout bA = ((LinearLayout) findViewById(C0000R.id.share_items));
    /* access modifiers changed from: private */
    public Intent bz;
    /* access modifiers changed from: private */
    public Context mContext;
    private LayoutInflater mInflater = LayoutInflater.from(this.mContext);

    public d(Context context) {
        super(context, C0000R.style.ShowDialogStyle);
        this.mContext = context;
        setContentView((int) C0000R.layout.share_choose_dialog);
        addItem();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    private void addItem() {
        this.bz = new Intent("android.intent.action.SEND");
        this.bz.setType("text/plain");
        this.bz.putExtra("android.intent.extra.SUBJECT", this.mContext.getString(C0000R.string.menu_share));
        String string = this.mContext.getString(C0000R.string.app_name);
        String S = S();
        this.bz.putExtra("android.intent.extra.TEXT", this.mContext.getString(C0000R.string.share_description, string, S));
        PackageManager packageManager = this.mContext.getPackageManager();
        List<ResolveInfo> queryIntentActivities = packageManager.queryIntentActivities(this.bz, 0);
        int size = queryIntentActivities.size();
        for (int i = 0; i != size; i++) {
            ResolveInfo resolveInfo = queryIntentActivities.get(i);
            String obj = resolveInfo.loadLabel(packageManager).toString();
            View inflate = this.mInflater.inflate((int) C0000R.layout.share_list_item, (ViewGroup) null);
            ((TextView) inflate.findViewById(C0000R.id.label)).setText(obj);
            ((ImageView) inflate.findViewById(C0000R.id.icon)).setImageDrawable(resolveInfo.loadIcon(packageManager));
            inflate.setOnClickListener(new n(this, resolveInfo));
            this.bA.addView(inflate);
        }
    }

    private String S() {
        try {
            return String.format("http://dl.dianxinos.com/s/apk/powermgr/%d/DX-PowerManager.apk", Integer.valueOf(this.mContext.getPackageManager().getPackageInfo("com.dianxinos.powermanager", 0).versionCode));
        } catch (PackageManager.NameNotFoundException e) {
            e.f("ShareDialog", "Failed to get version for: com.dianxinos.powermanager");
            return "http://dl.dianxinos.com/s/apk/powermgr";
        }
    }
}
