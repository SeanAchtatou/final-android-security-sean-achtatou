package com.dianxinos.powermanager.menu;

import android.graphics.drawable.Drawable;

/* compiled from: AppListActivity */
class c {
    final /* synthetic */ AppListActivity ag;
    /* access modifiers changed from: private */
    public String ah;
    /* access modifiers changed from: private */
    public String ai;
    /* access modifiers changed from: private */
    public Drawable aj;
    /* access modifiers changed from: private */
    public boolean ak;
    boolean al;

    private c(AppListActivity appListActivity) {
        this.ag = appListActivity;
        this.ah = "";
        this.ai = "";
        this.aj = null;
    }

    public String toString() {
        return "AppInfo: appname " + this.ah + " , packagename " + this.ai + ", isSys " + this.ak;
    }
}
