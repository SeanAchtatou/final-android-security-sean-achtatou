package com.dianxinos.powermanager.menu;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import com.dianxinos.powermanager.c.b;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

/* compiled from: AppWhiteMgr */
public class k {
    private static k jj;
    private SharedPreferences jh;
    private HashMap ji = ((HashMap) this.jh.getAll());

    public static k I(Context context) {
        if (jj == null) {
            jj = new k(context);
        }
        return jj;
    }

    private k(Context context) {
        this.jh = context.getSharedPreferences("AppWhiteList", 0);
        if (new b(context).E()) {
            J(context);
        }
    }

    public boolean B(String str) {
        if (this.ji.containsKey(str)) {
            return true;
        }
        return false;
    }

    public void C(String str) {
        this.ji.put(str, 1);
        SharedPreferences.Editor edit = this.jh.edit();
        edit.putInt(str, 1);
        edit.commit();
    }

    public void D(String str) {
        this.ji.remove(str);
        SharedPreferences.Editor edit = this.jh.edit();
        edit.remove(str);
        edit.commit();
    }

    public Set ci() {
        return this.ji.keySet();
    }

    private void J(Context context) {
        Intent intent = new Intent("android.intent.action.MAIN", (Uri) null);
        intent.addCategory("android.intent.category.HOME");
        List<ResolveInfo> queryIntentActivities = context.getPackageManager().queryIntentActivities(intent, 0);
        for (int i = 0; i < queryIntentActivities.size(); i++) {
            C(queryIntentActivities.get(i).activityInfo.packageName);
        }
    }
}
