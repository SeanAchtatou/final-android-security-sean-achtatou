package com.dianxinos.powermanager;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ClipDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.dianxinos.a.a.a;
import com.dianxinos.powermanager.c.d;
import com.dianxinos.powermanager.d.c;
import com.dianxinos.powermanager.mode.ModeSelectDialog;
import com.dianxinos.powermanager.mode.i;
import com.dianxinos.powermanager.mode.s;
import com.dianxinos.powermanager.ui.ImageShader;
import java.util.ArrayList;

public class PowerMgrHomeActivity extends Activity implements View.OnClickListener, d {
    private static final int[] dh = {C0000R.drawable.battery_info_bkg_low, C0000R.drawable.battery_info_bkg_high};
    private static final int[] di = {C0000R.drawable.cur_mode_btn_bkg_low, C0000R.drawable.cur_mode_btn_bkg_high};
    private static final int[] dj = {C0000R.drawable.one_key_battery_optimizer_low, C0000R.drawable.one_key_battery_optimizer_high};
    private static final int[] dk = {C0000R.drawable.details_btn_bkg_low, C0000R.drawable.details_btn_bkg_high};
    private static final int[] dl = {16712194, 16719904, 16725301, 16667731, 16738151, 16418436, 16554907, 16298929, 16698572, 16704996, 16777215};
    private static final int[] dm = {327513, 2293610, 6418044, 5308042, 8317587, 8845222, 10157501, 11730893, 13434846, 14876139, 16777215};
    private int aM;
    private i ab;
    private s ac;
    private c aq;
    private a by;
    private com.dianxinos.powermanager.c.a cP;
    /* access modifiers changed from: private */
    public ArrayList cQ;
    private Button cR;
    /* access modifiers changed from: private */
    public Button cS;
    private View cT;
    private LinearLayout cU;
    private ImageView cV;
    private ImageView cW;
    private ImageView cX;
    private ImageView cY;
    private TextView cZ;
    private f dA;
    private BroadcastReceiver dB = new p(this);
    private TextView da;
    /* access modifiers changed from: private */
    public TextView db;
    private com.dianxinos.powermanager.c.i dc;
    private boolean dd;
    private int de = -1;
    private int[] df = {C0000R.drawable.scene_call, C0000R.drawable.scene_net, C0000R.drawable.scene_music, C0000R.drawable.scene_video, C0000R.drawable.scene_standby};
    private int[] dg = {C0000R.string.scene_call_label, C0000R.string.scene_net_label, C0000R.string.scene_music_label, C0000R.string.scene_video_label, C0000R.string.scene_standby_label};
    private boolean dn = false;
    /* access modifiers changed from: private */

    /* renamed from: do  reason: not valid java name */
    public int f0do;
    private int dp;
    private int dq;
    private int dr;
    private int ds;
    private int dt;
    /* access modifiers changed from: private */
    public int du;
    /* access modifiers changed from: private */
    public int dv;
    /* access modifiers changed from: private */
    public int dw;
    /* access modifiers changed from: private */
    public int dx;
    /* access modifiers changed from: private */
    public int dy;
    /* access modifiers changed from: private */
    public int dz;
    private LayoutInflater mInflater;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.power_home_layout);
        this.mInflater = LayoutInflater.from(this);
        this.cP = com.dianxinos.powermanager.c.a.b((Context) this);
        this.aq = new c(this);
        this.ab = i.t(this);
        this.ac = this.ab.aB();
        this.ac.cH();
        this.ac.Z(this.ab.ax());
        ap();
        this.cU = (LinearLayout) findViewById(C0000R.id.battery_digit_zone);
        this.cV = (ImageView) findViewById(C0000R.id.capacity_image);
        this.cW = (ImageView) findViewById(C0000R.id.bkg_image);
        this.cX = (ImageView) findViewById(C0000R.id.charging_sign);
        this.cT = findViewById(C0000R.id.battery_info_summary);
        this.cS = (Button) findViewById(C0000R.id.battery_info_current_mode);
        this.cS.setOnClickListener(this);
        this.cS.setText(this.ab.az());
        this.da = (TextView) findViewById(C0000R.id.battery_info_remaining_time_label);
        this.db = (TextView) findViewById(C0000R.id.battery_info_remaining_time_value);
        this.cR = (Button) findViewById(C0000R.id.one_key_battery_optimizer);
        this.cR.setOnClickListener(this);
        this.cY = (ImageView) findViewById(C0000R.id.details_btn);
        this.cY.setOnClickListener(this);
        this.cZ = (TextView) findViewById(C0000R.id.battery_info_level);
        as();
        this.by = a.F(this);
        this.by.init();
    }

    private void ap() {
        this.cQ = new ArrayList();
        LinearLayout linearLayout = (LinearLayout) findViewById(C0000R.id.time_items);
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < 5) {
                View inflate = this.mInflater.inflate((int) C0000R.layout.scene_time_item, (ViewGroup) null);
                ((ImageView) inflate.findViewById(C0000R.id.scene_icon)).setImageResource(this.df[i2]);
                ((TextView) inflate.findViewById(C0000R.id.scene_label)).setText(this.dg[i2]);
                this.cQ.add((TextView) inflate.findViewById(C0000R.id.time_show));
                linearLayout.addView(inflate);
                linearLayout.addView(aq());
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    private View aq() {
        ImageView imageView = new ImageView(this);
        imageView.setBackgroundResource(C0000R.drawable.scene_time_seg);
        return imageView;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.cP.a(this);
        this.dn = false;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.cP.b((d) this);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        unregisterReceiver(this.dB);
        this.by.destroy();
        super.onDestroy();
    }

    public void d(com.dianxinos.powermanager.c.i iVar) {
        this.dc = iVar;
        this.dd = this.dc.status == 2;
        b(this.dc.gV, this.dd);
    }

    private void b(int i, boolean z) {
        int j = com.dianxinos.powermanager.c.a.j(i);
        a(i, z, j);
        int a = com.dianxinos.powermanager.c.a.a(this, j);
        this.f0do = a;
        if (j != this.de) {
            this.de = j;
            ImageShader.a(this.cW, a);
            ImageShader.a(this.cX, a);
            this.cT.setBackgroundResource(dh[j]);
            this.cS.setBackgroundResource(di[j]);
            this.cS.setTextColor(a);
            this.db.setTextColor(a);
            this.cZ.setTextColor(a);
            this.cR.setBackgroundResource(dj[j]);
            this.cR.setTextColor(a);
            this.cY.setImageResource(dk[j]);
        }
        this.cZ.setText(i + "%");
        if (z) {
            this.da.setText((int) C0000R.string.battery_info_remaining_charging_time);
            int i2 = this.dc.gW;
            if (((long) i2) == -1) {
                this.db.setText((int) C0000R.string.battery_info_value_unknown);
            } else {
                String b = com.dianxinos.powermanager.c.c.b(this, i2);
                this.db.setText(getString(C0000R.string.battery_info_value_remaining_charging_time, new Object[]{b}));
            }
            this.du = 3;
        } else {
            this.da.setText((int) C0000R.string.battery_info_remaining_discharging_time);
            int i3 = this.dc.gX;
            if (i3 == -1) {
                this.db.setText((int) C0000R.string.battery_info_value_unknown);
            } else if (i3 == -2) {
                this.db.setText((int) C0000R.string.battery_data_collecting);
            } else {
                this.db.setText(com.dianxinos.powermanager.c.c.c(this, i3));
            }
            this.du = c(this.aM, i3);
            this.aM = i3;
        }
        this.aq.O();
        c(i, this.dc.gU);
        this.dc.gU = false;
    }

    private void c(int i, boolean z) {
        int a = this.aq.a((double) i, 1);
        ((TextView) this.cQ.get(0)).setText(q(a));
        this.dv = c(this.dp, a);
        this.dp = a;
        int a2 = this.aq.a((double) i, 2);
        ((TextView) this.cQ.get(1)).setText(q(a2));
        this.dw = c(this.dq, a2);
        this.dq = a2;
        int a3 = this.aq.a((double) i, 4);
        ((TextView) this.cQ.get(2)).setText(q(a3));
        this.dx = c(this.dr, a3);
        this.dr = a3;
        int a4 = this.aq.a((double) i, 3);
        ((TextView) this.cQ.get(3)).setText(q(a4));
        this.dy = c(this.ds, a4);
        this.ds = a4;
        int a5 = this.aq.a((double) i, 0);
        ((TextView) this.cQ.get(4)).setText(q(a5));
        this.dz = c(this.dt, a5);
        this.dt = a5;
        if (this.dn && z) {
            if (this.dA != null) {
                this.dA.al();
            }
            this.dA = new f(this, null);
            this.dA.execute(new Void[0]);
        }
    }

    private void b(int i, int i2) {
        this.dp = (int) (((double) this.dp) + (((double) i2) * (((double) this.dp) / ((double) i))));
        ((TextView) this.cQ.get(0)).setText(q(this.dp));
        this.dv = 1;
        this.dq = (int) (((double) this.dq) + (((double) i2) * (((double) this.dq) / ((double) i))));
        ((TextView) this.cQ.get(1)).setText(q(this.dq));
        this.dw = 1;
        this.dr = (int) (((double) this.dr) + (((double) i2) * (((double) this.dr) / ((double) i))));
        ((TextView) this.cQ.get(2)).setText(q(this.dr));
        this.dx = 1;
        this.ds = (int) (((double) this.ds) + (((double) i2) * (((double) this.ds) / ((double) i))));
        ((TextView) this.cQ.get(3)).setText(q(this.ds));
        this.dy = 1;
        this.dt = (int) (((double) this.dt) + (((double) i2) * (((double) this.dt) / ((double) i))));
        ((TextView) this.cQ.get(4)).setText(q(this.dt));
        this.dz = 1;
        if (this.dA != null) {
            this.dA.al();
        }
        this.dA = new f(this, null);
        this.dA.execute(new Void[0]);
    }

    private String q(int i) {
        if (i == -1) {
            return getString(C0000R.string.scene_time_unavailable);
        }
        if (i == -2) {
            return getString(C0000R.string.battery_data_collecting);
        }
        return com.dianxinos.powermanager.c.c.c(this, i);
    }

    private int c(int i, int i2) {
        if (i == -1 || i2 == -1 || i == i2) {
            return 3;
        }
        if (i2 > i) {
            return 1;
        }
        return 2;
    }

    /* access modifiers changed from: private */
    public int d(int i, int i2) {
        if (i == 1) {
            return dm[i2] | -16777216;
        }
        if (i == 2) {
            return dl[i2] | -16777216;
        }
        return -1;
    }

    private void a(int i, boolean z, int i2) {
        if (i2 == 0) {
            this.cV.setImageResource(C0000R.drawable.battery_low_clip);
        } else {
            this.cV.setImageResource(C0000R.drawable.battery_high_clip);
        }
        ((ClipDrawable) this.cV.getDrawable()).setLevel(i * 100);
        ImageView imageView = (ImageView) findViewById(C0000R.id.charging_sign);
        TextView textView = (TextView) findViewById(C0000R.id.number_text);
        if (z) {
            imageView.setVisibility(0);
            textView.setVisibility(8);
            return;
        }
        imageView.setVisibility(8);
        textView.setVisibility(0);
        textView.setText(i + "%");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.dianxinos.a.a.a.a(java.lang.String, java.lang.String, java.lang.Number):boolean
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.dianxinos.a.a.a.a(java.lang.String, int, java.lang.Object):boolean
      com.dianxinos.a.a.a.a(java.lang.String, java.lang.String, java.lang.Number):boolean */
    public void onClick(View view) {
        if (view == this.cR) {
            int i = this.dc.gX;
            l v = l.v(this);
            if (!v.aI()) {
                Toast.makeText(this, (int) C0000R.string.onekey_not_allowed, 0).show();
                return;
            }
            int d = v.d(300, true);
            int i2 = 180;
            if (d > 0) {
                int z = this.cP.z();
                if (z > 0 && i > 0) {
                    i2 = z - i;
                }
                b(i, i2);
            }
            v.f(d, i2);
            this.by.a("clicks", "one_key", (Number) 1);
        } else if (view == this.cS) {
            this.dn = true;
            startActivity(new Intent(this, ModeSelectDialog.class));
        } else if (view == this.cU) {
            ar();
        } else if (view == this.cY) {
            ar();
        }
    }

    private void ar() {
        startActivity(new Intent(this, BatteryInfoDetails.class));
    }

    private void as() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.dianxinos.powermanager.MODECHANGE");
        registerReceiver(this.dB, intentFilter);
    }
}
