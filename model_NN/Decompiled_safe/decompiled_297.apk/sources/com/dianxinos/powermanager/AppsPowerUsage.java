package com.dianxinos.powermanager;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;
import android.widget.TextView;

public class AppsPowerUsage extends TabActivity {
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.tab_content_2);
        String string = getString(C0000R.string.apps_power_usage_current);
        String string2 = getString(C0000R.string.apps_power_usage_history);
        TabHost tabHost = getTabHost();
        tabHost.addTab(tabHost.newTabSpec("current").setContent(new Intent(this, AppsPowerUsageRecent.class)).setIndicator(l(string)));
        tabHost.addTab(tabHost.newTabSpec("history").setContent(new Intent(this, AppsPowerUsageHistory.class)).setIndicator(l(string2)));
        tabHost.setCurrentTabByTag("history");
    }

    private View l(String str) {
        View inflate = getLayoutInflater().inflate((int) C0000R.layout.tab_indicator_2, (ViewGroup) null);
        ((TextView) inflate.findViewById(C0000R.id.title)).setText(str);
        return inflate;
    }
}
