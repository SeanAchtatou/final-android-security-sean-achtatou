package com.dianxinos.powermanager;

import android.view.View;
import com.dianxinos.powermanager.c.e;
import com.dianxinos.powermanager.c.j;

/* compiled from: HwPowerUsageDetails */
class n implements View.OnClickListener {
    final /* synthetic */ HwPowerUsageDetails gv;

    n(HwPowerUsageDetails hwPowerUsageDetails) {
        this.gv = hwPowerUsageDetails;
    }

    public void onClick(View view) {
        String str = (String) view.getTag();
        e.d("HwPowerUsageDetails", "check details for " + str);
        if (str != null) {
            this.gv.startActivity(j.w(str));
        }
    }
}
