package com.dianxinos.powermanager.ui;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.drawable.Drawable;

/* compiled from: PercentageBar */
public class d extends Drawable {
    private Drawable hu;
    private Drawable hv;
    private double hw;
    private int hx = -1;

    public d(Drawable drawable, Drawable drawable2, double d) {
        this.hu = drawable;
        this.hv = drawable2;
        this.hw = d;
    }

    public void draw(Canvas canvas) {
        this.hv.setBounds(getBounds());
        this.hv.draw(canvas);
        if (this.hx == -1) {
            this.hx = bv();
            this.hu.setBounds(0, 0, this.hx, this.hu.getIntrinsicHeight());
        }
        this.hu.draw(canvas);
    }

    public void setAlpha(int i) {
    }

    public void setColorFilter(ColorFilter colorFilter) {
    }

    public int getOpacity() {
        return -3;
    }

    private int bv() {
        return Math.max((int) (((double) getBounds().width()) * this.hw), this.hu.getIntrinsicWidth());
    }

    public int getIntrinsicHeight() {
        return this.hu.getIntrinsicHeight();
    }
}
