package com.dianxinos.powermanager.ui;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.dianxinos.powermanager.C0000R;
import java.util.ArrayList;

/* compiled from: SingleChoiceDialog */
public class e extends Dialog implements View.OnClickListener {
    private LinearLayout jv;
    private ArrayList jw = new ArrayList();
    private b jx;
    private int mCheckedItem = -1;

    public e(Context context) {
        super(context, C0000R.style.ShowDialogStyle);
        setContentView((int) C0000R.layout.single_choice_dialog);
        this.jv = (LinearLayout) findViewById(C0000R.id.choice_group);
    }

    public void setTitle(int i) {
        ((TextView) findViewById(C0000R.id.title)).setText(i);
    }

    public void k(int i, int i2) {
        LinearLayout linearLayout = (LinearLayout) LayoutInflater.from(getContext()).inflate((int) C0000R.layout.single_choice_item, (ViewGroup) null);
        ImageView imageView = (ImageView) linearLayout.findViewById(C0000R.id.checked_image);
        imageView.setImageResource(C0000R.drawable.mode_off);
        ((TextView) linearLayout.findViewById(C0000R.id.label)).setText(i2);
        a aVar = new a();
        aVar.m = imageView;
        aVar.n = i;
        linearLayout.setTag(aVar);
        linearLayout.setOnClickListener(this);
        if (this.jw.size() > 0) {
            ImageView imageView2 = new ImageView(getContext());
            imageView2.setImageResource(C0000R.drawable.seg_listbox);
            this.jv.addView(imageView2, new LinearLayout.LayoutParams(-1, -2));
        }
        this.jv.addView(linearLayout);
        this.jw.add(aVar);
    }

    public boolean U(int i) {
        if (this.mCheckedItem != -1) {
            a aVar = (a) this.jw.get(this.mCheckedItem);
            if (aVar.n == i) {
                return false;
            }
            aVar.m.setImageResource(C0000R.drawable.mode_off);
        }
        for (int i2 = 0; i2 < this.jw.size(); i2++) {
            a aVar2 = (a) this.jw.get(i2);
            if (aVar2.n == i) {
                aVar2.m.setImageResource(C0000R.drawable.mode_on);
                this.mCheckedItem = i2;
                return true;
            }
        }
        return false;
    }

    public void a(b bVar) {
        this.jx = bVar;
    }

    public void onClick(View view) {
        a aVar = (a) view.getTag();
        boolean U = U(aVar.n);
        if (this.jx != null && U) {
            this.jx.l(aVar.n);
        }
        dismiss();
    }
}
