package com.dianxinos.powermanager.ui;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.widget.ImageView;

public class ImageShader {
    private static native void native_toColor(int i, int[] iArr);

    public static Bitmap a(Bitmap bitmap, int i) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        int[] iArr = new int[(width * height)];
        bitmap.getPixels(iArr, 0, width, 0, 0, width, height);
        native_toColor(i, iArr);
        Bitmap createBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        createBitmap.setPixels(iArr, 0, width, 0, 0, width, height);
        return createBitmap;
    }

    public static void a(ImageView imageView, int i) {
        imageView.setImageBitmap(a(((BitmapDrawable) imageView.getDrawable()).getBitmap(), i));
    }

    static {
        System.loadLibrary("dxpowermgrjni");
    }
}
