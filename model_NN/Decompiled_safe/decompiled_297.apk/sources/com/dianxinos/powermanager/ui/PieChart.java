package com.dianxinos.powermanager.ui;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import com.dianxinos.powermanager.C0000R;

public class PieChart extends View {
    private static final int[] am = {-14641620, -16454913, -10176519, -3565484, -5816632, -1255107};
    private static final int[] gg = {C0000R.drawable.pie_chart_number_0, C0000R.drawable.pie_chart_number_1, C0000R.drawable.pie_chart_number_2, C0000R.drawable.pie_chart_number_3, C0000R.drawable.pie_chart_number_4, C0000R.drawable.pie_chart_number_5, C0000R.drawable.pie_chart_number_6, C0000R.drawable.pie_chart_number_7, C0000R.drawable.pie_chart_number_8, C0000R.drawable.pie_chart_number_9};
    private String[] gh;
    private int[] gi;
    private float gj;
    private float gk;
    private float gl;
    private float gm;
    private float gn;
    private float go;
    private int[] mColors;

    public PieChart(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public PieChart(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        Resources resources = this.mContext.getResources();
        this.gj = resources.getDimension(C0000R.dimen.pie_chart_rect_indent);
        this.gk = resources.getDimension(C0000R.dimen.pie_chart_label_font);
        this.gl = resources.getDimension(C0000R.dimen.pie_chart_label_horizontal_padding);
        this.gm = resources.getDimension(C0000R.dimen.pie_chart_label_percent_width);
        this.gn = resources.getDimension(C0000R.dimen.pid_chart_label_vertical_padding);
        this.go = resources.getDimension(C0000R.dimen.pie_chart_label_round_radius);
    }

    public void a(String[] strArr, int[] iArr, int[] iArr2) {
        this.gh = strArr;
        this.gi = new int[iArr.length];
        System.arraycopy(iArr, 0, this.gi, 0, iArr.length);
        this.mColors = new int[iArr2.length];
        System.arraycopy(iArr2, 0, this.mColors, 0, iArr2.length);
    }

    public void a(String[] strArr, int[] iArr) {
        a(strArr, iArr, am);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        RectF rectF = new RectF();
        RectF rectF2 = new RectF();
        a(rectF, rectF2);
        float centerX = rectF.centerX();
        float centerY = rectF.centerY();
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(0.0f);
        canvas.save();
        float f = 0.0f;
        int i = 0;
        while (true) {
            int i2 = i;
            float f2 = f;
            if (i2 >= this.gi.length) {
                break;
            }
            paint.setColor(this.mColors[i2 % this.mColors.length]);
            float f3 = ((float) (this.gi[i2] * 360)) / 100.0f;
            Path path = new Path();
            path.addArc(rectF, f2, f3);
            path.lineTo(centerX, centerY);
            canvas.clipPath(path, Region.Op.REPLACE);
            float f4 = rectF.top;
            while (true) {
                float f5 = f4;
                if (f5 > rectF.bottom) {
                    break;
                }
                canvas.drawLine(rectF.left, f5, rectF.right, f5, paint);
                f4 = 4.0f + f5;
            }
            f = f2 + f3;
            i = i2 + 1;
        }
        canvas.restore();
        Paint paint2 = new Paint();
        paint2.setAntiAlias(true);
        paint2.setStyle(Paint.Style.STROKE);
        paint2.setStrokeWidth(0.0f);
        paint2.setColor(-16711681);
        paint2.setTextAlign(Paint.Align.CENTER);
        paint2.setTextSize(30.0f);
        paint2.setTypeface(Typeface.SERIF);
        canvas.drawArc(rectF, 0.0f, 360.0f, true, paint2);
        float f6 = 0.0f;
        int i3 = 0;
        while (true) {
            int i4 = i3;
            float f7 = f6;
            if (i4 < this.gi.length) {
                float f8 = ((float) (this.gi[i4] * 360)) / 100.0f;
                canvas.drawArc(rectF, f7, f8, true, paint2);
                a(canvas, rectF, this.gi[i4], f7, f8);
                f6 = f7 + f8;
                i3 = i4 + 1;
            } else {
                a(canvas, rectF2);
                return;
            }
        }
    }

    private void a(RectF rectF, RectF rectF2) {
        Rect rect = new Rect();
        getDrawingRect(rect);
        rectF.set(rect);
        rectF2.set(rect);
        float width = rectF.width() - rectF.height();
        if (width > 0.0f) {
            rectF.right -= width;
            rectF2.left = rectF2.right - width;
        } else {
            rectF.bottom += width;
            rectF2.top = width + rectF2.bottom;
        }
        rectF.inset(this.gj, this.gj);
        rectF2.inset(this.gj, this.gj);
    }

    private PointF a(RectF rectF, float f, float f2, int i) {
        float f3 = (f2 / 2.0f) + f;
        float f4 = 0.7f;
        if (this.gh.length == 1) {
            f4 = 0.0f;
        } else if (f2 > 180.0f) {
            f4 = 0.4f;
        } else if (f2 > 150.0f) {
            f4 = 0.5f;
        } else if (f2 > 60.0f) {
            f4 = 0.6f;
        } else if (f2 < 15.0f) {
            f4 = 0.9f;
        } else if (f2 < 20.0f) {
            f4 = 0.8f;
        } else if (f2 < 40.0f && ((f3 > 30.0f && f3 < 150.0f) || (f3 > 210.0f && f3 < 330.0f))) {
            f4 = 0.8f;
        }
        float width = f4 * (rectF.width() / 2.0f);
        double d = ((double) ((f3 / 360.0f) * 2.0f)) * 3.141592653589793d;
        return new PointF((float) (((double) rectF.centerX()) + (((double) width) * Math.cos(d))), (float) (((double) rectF.centerY()) + (Math.sin(d) * ((double) width))));
    }

    private void a(Canvas canvas, RectF rectF, int i, float f, float f2) {
        Drawable drawable;
        int i2;
        Drawable drawable2;
        int i3;
        int i4;
        int i5;
        char c = i < 100 ? (char) 0 : 1;
        int i6 = (i / 10) % 10;
        int i7 = i % 10;
        if (c > 0) {
            Drawable drawable3 = this.mContext.getResources().getDrawable(gg[c]);
            drawable = drawable3;
            i2 = drawable3.getIntrinsicWidth();
        } else {
            drawable = null;
            i2 = 0;
        }
        if (c > 0 || i6 > 0) {
            Drawable drawable4 = this.mContext.getResources().getDrawable(gg[i6]);
            drawable2 = drawable4;
            i3 = drawable4.getIntrinsicWidth();
        } else {
            drawable2 = null;
            i3 = 0;
        }
        Drawable drawable5 = this.mContext.getResources().getDrawable(gg[i7]);
        int intrinsicWidth = drawable5.getIntrinsicWidth();
        Drawable drawable6 = this.mContext.getResources().getDrawable(C0000R.drawable.pie_chart_percent_sign);
        int intrinsicWidth2 = drawable6.getIntrinsicWidth();
        int i8 = i2 + i3 + intrinsicWidth + intrinsicWidth2;
        PointF a = a(rectF, f, f2, i8);
        int i9 = ((int) a.x) - (i8 / 2);
        int intrinsicHeight = ((int) a.y) - (drawable5.getIntrinsicHeight() / 2);
        if (c > 0) {
            i4 = i2 + i9;
            drawable.setBounds(i9, intrinsicHeight, i4, drawable.getIntrinsicHeight() + intrinsicHeight);
            drawable.draw(canvas);
        } else {
            i4 = i9;
        }
        if (c > 0 || i6 > 0) {
            i5 = i4 + i3;
            drawable2.setBounds(i4, intrinsicHeight, i5, drawable2.getIntrinsicHeight() + intrinsicHeight);
            drawable2.draw(canvas);
        } else {
            i5 = i4;
        }
        int i10 = i5 + intrinsicWidth;
        drawable5.setBounds(i5, intrinsicHeight, i10, drawable5.getIntrinsicHeight() + intrinsicHeight);
        drawable5.draw(canvas);
        drawable6.setBounds(i10, intrinsicHeight, i10 + intrinsicWidth2, drawable6.getIntrinsicHeight() + intrinsicHeight);
        drawable6.draw(canvas);
    }

    private void a(Canvas canvas, RectF rectF) {
        float f;
        float f2;
        float f3 = this.gk;
        float f4 = rectF.left + this.gl;
        float f5 = f4 + f3 + this.gn;
        float f6 = rectF.right - this.gl;
        float f7 = f6 - this.gm;
        float f8 = this.gn + f3;
        float f9 = rectF.top;
        RectF rectF2 = new RectF();
        float height = (rectF.height() - (((float) this.gh.length) * f8)) / ((float) (this.gh.length + 1));
        if (height > 0.0f) {
            float f10 = f9 + height;
            f = f8 + height;
            f2 = f10;
        } else {
            float f11 = f9;
            f = f8;
            f2 = f11;
        }
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setTextSize(this.gk);
        Rect rect = new Rect();
        RectF rectF3 = new RectF();
        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        float f12 = f2;
        for (int i = 0; i < this.gh.length; i++) {
            paint.setColor(this.mColors[i % this.mColors.length]);
            paint.setAlpha(153);
            rectF2.set(f4, f12, f4 + f3, f12 + f3);
            canvas.drawRoundRect(rectF2, this.go, this.go, paint);
            canvas.save();
            paint.getTextBounds(this.gh[i], 0, this.gh[i].length(), rect);
            float f13 = (((f3 - ((float) (rect.bottom - rect.top))) / 2.0f) + f12) - ((float) rect.top);
            rectF3.set(f5, f12, f7, f12 + f3);
            canvas.clipRect(rectF3, Region.Op.REPLACE);
            paint.setTextAlign(Paint.Align.LEFT);
            paint.setColor(-1);
            canvas.drawText(this.gh[i], f5, f13, paint);
            canvas.restore();
            String format = String.format(" %d%%", Integer.valueOf(this.gi[i]));
            paint.getTextBounds(format, 0, format.length(), rect);
            float f14 = (((f3 - ((float) (rect.bottom - rect.top))) / 2.0f) + f12) - ((float) rect.top);
            paint.setTextAlign(Paint.Align.RIGHT);
            paint.setColor(-16711681);
            paint.setAlpha(255);
            canvas.drawText(format, f6, f14, paint);
            f12 += f;
        }
    }
}
