package com.dianxinos.powermanager;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.widget.TextView;
import com.dianxinos.powermanager.c.a;
import com.dianxinos.powermanager.c.c;
import com.dianxinos.powermanager.c.d;
import com.dianxinos.powermanager.c.e;
import com.dianxinos.powermanager.c.i;

public class BatteryInfoDetails extends Activity implements d {
    private static int[] ad = new int[2];
    private static String hh;
    private static String hi;
    private a cP;

    private void x() {
        Resources resources = getResources();
        ad[0] = resources.getColor(C0000R.color.info_value_low);
        ad[1] = resources.getColor(C0000R.color.info_value_high);
        hh = resources.getString(C0000R.string.temperature_unit);
        hi = resources.getString(C0000R.string.voltage_unit);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.battery_info_details);
        x();
        this.cP = a.b((Context) this);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.cP.a(this);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.cP.b((d) this);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (isFinishing()) {
            return true;
        }
        finish();
        return true;
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (isFinishing()) {
            return true;
        }
        finish();
        return true;
    }

    public void d(i iVar) {
        f(iVar);
    }

    private void f(i iVar) {
        String string;
        String string2;
        String c;
        String string3;
        int i = iVar.gV;
        boolean z = iVar.status == 2;
        int j = a.j(i);
        TextView textView = (TextView) findViewById(C0000R.id.battery_info_level);
        textView.setText(i + "%");
        textView.setTextColor(ad[j]);
        int i2 = iVar.status;
        int i3 = iVar.gQ;
        if (i2 == 2) {
            if (i3 == 1) {
                string = getString(C0000R.string.battery_info_value_status_ac_charging);
            } else if (i3 == 2) {
                string = getString(C0000R.string.battery_info_value_status_usb_charging);
            } else {
                e.f("BatteryInfoDetails", "Unknown charging type: " + i3);
                string = getString(C0000R.string.battery_info_value_status_charging);
            }
        } else if (i2 == 3 || i2 == 4) {
            string = getString(C0000R.string.battery_info_value_status_discharging);
        } else if (i2 == 5) {
            string = getString(C0000R.string.battery_info_value_status_full);
        } else {
            string = getString(C0000R.string.battery_info_value_unknown);
        }
        TextView textView2 = (TextView) findViewById(C0000R.id.battery_info_status);
        textView2.setText(string);
        textView2.setTextColor(ad[j]);
        int i4 = iVar.gN;
        if (i4 == 2) {
            string2 = getString(C0000R.string.battery_info_value_health_good);
        } else if (i4 == 4) {
            string2 = getString(C0000R.string.battery_info_value_health_dead);
        } else if (i4 == 3) {
            string2 = getString(C0000R.string.battery_info_value_health_over_heat);
        } else if (i4 == 5) {
            string2 = getString(C0000R.string.battery_info_value_health_over_voltage);
        } else {
            string2 = getString(C0000R.string.battery_info_value_unknown);
        }
        TextView textView3 = (TextView) findViewById(C0000R.id.battery_info_health);
        textView3.setText(string2);
        textView3.setTextColor(ad[j]);
        TextView textView4 = (TextView) findViewById(C0000R.id.battery_info_remaining_time_label);
        if (z) {
            textView4.setText((int) C0000R.string.battery_info_remaining_charging_time);
            int i5 = iVar.gW;
            if (((long) i5) == -1) {
                string3 = getString(C0000R.string.battery_info_value_unknown);
            } else {
                string3 = getString(C0000R.string.battery_info_value_remaining_charging_time, new Object[]{c.b(this, i5)});
            }
            c = string3;
        } else {
            textView4.setText((int) C0000R.string.battery_info_remaining_discharging_time);
            long j2 = (long) iVar.gX;
            if (j2 == -1) {
                c = getString(C0000R.string.battery_info_value_unknown);
            } else if (j2 == -2) {
                c = getString(C0000R.string.battery_data_collecting);
            } else {
                c = c.c(this, (int) j2);
            }
        }
        TextView textView5 = (TextView) findViewById(C0000R.id.battery_info_remaining_time_value);
        textView5.setText(c);
        textView5.setTextColor(ad[j]);
        TextView textView6 = (TextView) findViewById(C0000R.id.battery_info_temperature);
        textView6.setText((iVar.gS / 10) + hh);
        textView6.setTextColor(ad[j]);
        TextView textView7 = (TextView) findViewById(C0000R.id.battery_info_voltage);
        textView7.setText((((float) (iVar.gR / 100)) / 10.0f) + hi);
        textView7.setTextColor(ad[j]);
        String str = iVar.gT;
        TextView textView8 = (TextView) findViewById(C0000R.id.battery_info_technology);
        textView8.setText(str);
        textView8.setTextColor(ad[j]);
    }
}
