package com.dianxinos.powermanager;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.widget.RemoteViews;
import com.dianxinos.powermanager.c.a;
import com.dianxinos.powermanager.c.d;
import com.dianxinos.powermanager.c.e;
import com.dianxinos.powermanager.mode.i;
import java.util.Timer;

public class WidgetUpdataServiceBase extends Service implements d {
    private static Timer aK;
    protected static final int[][] aN = {new int[]{C0000R.drawable.widget_high_digit_0, C0000R.drawable.widget_high_digit_1, C0000R.drawable.widget_high_digit_2, C0000R.drawable.widget_high_digit_3, C0000R.drawable.widget_high_digit_4, C0000R.drawable.widget_high_digit_5, C0000R.drawable.widget_high_digit_6, C0000R.drawable.widget_high_digit_7, C0000R.drawable.widget_high_digit_8, C0000R.drawable.widget_high_digit_9}, new int[]{C0000R.drawable.widget_low_digit_0, C0000R.drawable.widget_low_digit_1, C0000R.drawable.widget_low_digit_2, C0000R.drawable.widget_low_digit_3, C0000R.drawable.widget_low_digit_4, C0000R.drawable.widget_low_digit_5, C0000R.drawable.widget_low_digit_6, C0000R.drawable.widget_low_digit_7, C0000R.drawable.widget_low_digit_8, C0000R.drawable.widget_low_digit_9}};
    protected static final int[] aO = {C0000R.drawable.widget_high_digit_dian, C0000R.drawable.widget_low_digit_dian};
    protected static final int[] aP = {C0000R.drawable.widget_high_digit_sign, C0000R.drawable.widget_low_digit_sign};
    /* access modifiers changed from: private */
    public a aE;
    protected int aF;
    protected boolean aG;
    /* access modifiers changed from: private */
    public boolean aH;
    private int aI;
    private int aJ;
    private int aL;
    protected int aM;
    private BroadcastReceiver aQ = new i(this);
    protected i ab;
    protected int az;

    public void d(com.dianxinos.powermanager.c.i iVar) {
        if (e(iVar)) {
            G();
        }
    }

    private boolean e(com.dianxinos.powermanager.c.i iVar) {
        boolean z = iVar.status == 2;
        int ax = this.ab.ax();
        int i = iVar.gQ;
        if (this.az == iVar.gV && this.aG == z && this.aI == ax && this.aJ == i && this.aM == iVar.gX) {
            return false;
        }
        this.aG = z;
        this.aI = ax;
        this.aJ = i;
        this.az = iVar.gV;
        this.aL = iVar.gW;
        this.aM = iVar.gX;
        if (this.az > 20) {
            this.aF = 0;
        } else {
            this.aF = 1;
        }
        return true;
    }

    private void G() {
        RemoteViews H = H();
        a(H);
        b(H);
        c(H);
        d(H);
        e(H);
    }

    /* access modifiers changed from: protected */
    public RemoteViews H() {
        return null;
    }

    /* access modifiers changed from: protected */
    public void a(RemoteViews remoteViews) {
    }

    /* access modifiers changed from: protected */
    public void b(RemoteViews remoteViews) {
    }

    /* access modifiers changed from: protected */
    public void c(RemoteViews remoteViews) {
        int i;
        if (this.aG) {
            if (((long) this.aL) == -1) {
                e.c("WidgetUpdataServiceBase", " charging time unkonwn ");
                i = 0;
            } else {
                i = this.aL;
            }
            remoteViews.setImageViewResource(C0000R.id.cover, 17170445);
        } else if (this.aM == -1) {
            e.c("WidgetUpdataServiceBase", " Remaining time unavaiable ");
            remoteViews.setImageViewResource(C0000R.id.cover, 17170445);
            i = 0;
        } else if (this.aM == -2) {
            e.c("WidgetUpdataServiceBase", " Remaining time not ready ");
            remoteViews.setImageViewResource(C0000R.id.cover, C0000R.drawable.widget_cover);
            i = 0;
        } else {
            remoteViews.setImageViewResource(C0000R.id.cover, 17170445);
            i = this.aM;
        }
        int i2 = i / 60;
        int i3 = i2 % 60;
        int i4 = i2 / 60;
        if (i4 > 999) {
            i4 = 999;
        }
        e.c("WidgetUpdataServiceBase", "remainingTime : " + i + "  remainingTimeTolalMin : " + i2 + "  remainingTimeMin: " + i3 + "  remainingTimeHour: " + i4);
        int i5 = i4 / 100;
        int i6 = (i4 % 100) / 10;
        int i7 = i4 % 10;
        int i8 = i3 / 10;
        int i9 = i3 % 10;
        e.c("WidgetUpdataServiceBase", "hourHundred : " + i5 + "  hourTen :" + i6 + "  hoursingle : " + i7 + "  minTen:" + i8 + " minSingle:" + i9);
        if (i5 > 0) {
            remoteViews.setImageViewResource(C0000R.id.hour_hundred_digit, aN[this.aF][i5]);
            remoteViews.setViewVisibility(C0000R.id.hour_hundred_digit, 0);
        } else {
            remoteViews.setViewVisibility(C0000R.id.hour_hundred_digit, 8);
        }
        if (i5 > 0 || i6 > 0) {
            remoteViews.setImageViewResource(C0000R.id.hour_tens_digit, aN[this.aF][i6]);
            remoteViews.setViewVisibility(C0000R.id.hour_tens_digit, 0);
        } else {
            remoteViews.setViewVisibility(C0000R.id.hour_tens_digit, 8);
        }
        remoteViews.setImageViewResource(C0000R.id.hour_single_digit, aN[this.aF][i7]);
        remoteViews.setViewVisibility(C0000R.id.hour_single_digit, 0);
        if (i5 > 0) {
            remoteViews.setImageViewResource(C0000R.id.time_seg, C0000R.drawable.widget_time_hour);
            remoteViews.setViewVisibility(C0000R.id.min_tens_digit, 8);
            remoteViews.setViewVisibility(C0000R.id.min_single_digit, 8);
            return;
        }
        remoteViews.setImageViewResource(C0000R.id.min_tens_digit, aN[this.aF][i8]);
        remoteViews.setViewVisibility(C0000R.id.min_tens_digit, 0);
        remoteViews.setImageViewResource(C0000R.id.min_single_digit, aN[this.aF][i9]);
        remoteViews.setViewVisibility(C0000R.id.min_single_digit, 0);
        remoteViews.setImageViewResource(C0000R.id.time_seg, aO[this.aF]);
    }

    /* access modifiers changed from: protected */
    public void d(RemoteViews remoteViews) {
    }

    /* access modifiers changed from: protected */
    public void e(RemoteViews remoteViews) {
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        this.ab = i.t(this);
        this.aI = this.ab.ax();
        this.aE = a.b((Context) this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.SCREEN_OFF");
        intentFilter.addAction("android.intent.action.SCREEN_ON");
        registerReceiver(this.aQ, intentFilter);
        if (aK == null) {
            aK = new Timer();
            aK.schedule(new h(this), 10000, 10800000);
        }
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        if (!this.aH) {
            this.aE.a(this);
            this.aH = true;
        } else {
            G();
        }
        return super.onStartCommand(intent, i, i2);
    }

    public void onDestroy() {
        if (this.aH) {
            this.aE.b((d) this);
        }
        unregisterReceiver(this.aQ);
        aK.cancel();
        aK = null;
    }
}
