package com.dianxinos.appupdate;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

/* compiled from: VendorInfoBucket */
public abstract class l {
    public Map ak() {
        Object obj;
        HashMap hashMap = new HashMap();
        for (Field field : getClass().getDeclaredFields()) {
            try {
                if (!(field.getModifiers() != 1 || field.getModifiers() == 128 || (obj = field.get(this)) == null)) {
                    hashMap.put(field.getName(), obj.toString());
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return hashMap;
    }
}
