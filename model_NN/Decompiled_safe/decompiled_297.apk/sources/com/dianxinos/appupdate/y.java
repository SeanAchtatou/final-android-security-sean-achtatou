package com.dianxinos.appupdate;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;

/* compiled from: AndroidUtils */
public class y {
    private static final boolean DEBUG = x.DEBUG;

    public static int e(Context context, String str) {
        PackageManager packageManager = context.getPackageManager();
        String packageName = context.getPackageName();
        PackageInfo packageArchiveInfo = packageManager.getPackageArchiveInfo(str, 16384);
        if (packageArchiveInfo != null) {
            if (packageName.equals(packageArchiveInfo.packageName)) {
                try {
                    PackageInfo packageInfo = packageManager.getPackageInfo(packageName, 0);
                    if (packageInfo != null) {
                        if (packageInfo.versionCode < packageArchiveInfo.versionCode) {
                            if (DEBUG) {
                                Log.i("AndroidUtils", "New version name:" + packageArchiveInfo.versionName);
                            }
                            return 0;
                        }
                        if (DEBUG) {
                            Log.w("AndroidUtils", "Current version is newer, no need to upgrade");
                        }
                        return 2;
                    }
                } catch (PackageManager.NameNotFoundException e) {
                }
            } else {
                if (DEBUG) {
                    Log.w("AndroidUtils", "Package name mismatches");
                }
                return 3;
            }
        }
        return 1;
    }
}
