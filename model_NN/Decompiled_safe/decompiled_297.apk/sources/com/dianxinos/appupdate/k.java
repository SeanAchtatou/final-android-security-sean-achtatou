package com.dianxinos.appupdate;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Process;
import android.text.TextUtils;
import android.util.Log;
import java.io.File;
import java.lang.Thread;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.Callable;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: UpdateManager */
public class k {
    /* access modifiers changed from: private */
    public static final boolean DEBUG = x.DEBUG;
    private static k cf;
    private static int cm = 0;
    private static int cn = 0;
    private t S;
    private long cg;
    private long ch;
    /* access modifiers changed from: private */
    public String ci;
    /* access modifiers changed from: private */
    public e cj;
    /* access modifiers changed from: private */
    public n ck;
    /* access modifiers changed from: private */
    public Thread cl;
    /* access modifiers changed from: private */
    public String co;
    /* access modifiers changed from: private */
    public DownloadService cp;
    /* access modifiers changed from: private */
    public int cq;
    /* access modifiers changed from: private */
    public f cr;
    /* access modifiers changed from: private */
    public q cs;
    private Object ct;
    private Object cu;
    /* access modifiers changed from: private */
    public Object cv;
    /* access modifiers changed from: private */
    public z cw;
    private Map cx;
    private int cy;
    private ServiceConnection cz;
    /* access modifiers changed from: private */
    public Context mContext;

    static /* synthetic */ int aj() {
        int i = cm + 1;
        cm = i;
        return i;
    }

    public static k d(Context context) {
        if (cf == null) {
            cf = new k(context.getApplicationContext());
        }
        return cf;
    }

    private k(Context context) {
        this(context, null);
    }

    private k(Context context, e eVar) {
        this.ch = 60000;
        this.ct = new Object();
        this.cu = new Object();
        this.cv = new Object();
        this.cy = 0;
        this.S = new b(this);
        this.cz = new c(this);
        this.mContext = context;
        if (eVar == null) {
            this.cj = new e();
            this.cj.a(this.S);
        } else {
            this.cj = eVar;
        }
        ac();
        try {
            this.cq = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
            if (DEBUG) {
                Log.d("UpdateManager", "Current apk version code:" + this.cq);
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        this.cr = new f();
        this.cr.sdkVersion = Build.VERSION.SDK_INT;
        this.cr.aB = Build.MODEL;
        this.cr.device = Build.DEVICE;
        this.cr.aC = Build.FINGERPRINT;
        int myUid = Process.myUid();
        if (DEBUG) {
            Log.d("UpdateManager", "my uid:" + myUid);
        }
        if (myUid == 1000) {
            this.cy = 5;
        } else {
            this.cy = 0;
        }
        File file = new File(DownloadHelpers.f(this.mContext, 0));
        File file2 = new File(DownloadHelpers.f(this.mContext, 5));
        a(file);
        a(file2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.dianxinos.appupdate.ah.a(android.content.Context, java.lang.String, long):long
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.dianxinos.appupdate.ah.a(android.content.Context, java.lang.String, int):int
      com.dianxinos.appupdate.ah.a(android.content.Context, java.lang.String, java.lang.String):java.lang.String
      com.dianxinos.appupdate.ah.a(android.content.Context, java.lang.String, boolean):boolean
      com.dianxinos.appupdate.ah.a(android.content.Context, java.lang.String, long):long */
    private void ac() {
        this.ci = ah.f(this.mContext, "pref-filename");
        if (!s.p(this.ci)) {
            if (DEBUG) {
                Log.w("UpdateManager", "Invalid filename found in pref:" + this.ci);
            }
            this.ci = this.mContext.getPackageName() + "-update_" + System.currentTimeMillis() + ".apk";
            if (!s.p(this.ci)) {
                if (DEBUG) {
                    Log.w("UpdateManager", "Generated filename invalid:" + this.ci);
                }
                this.ci = "app-update_" + System.currentTimeMillis() + ".apk";
                if (DEBUG) {
                    Log.d("UpdateManager", "Use failback filename:" + this.ci);
                }
            } else if (DEBUG) {
                Log.d("UpdateManager", "Filename generated:" + this.ci);
            }
            this.cw = g(ah.f(this.mContext, "pref-update-info"));
        } else if (DEBUG) {
            Log.d("UpdateManager", "get filename from pref:" + this.ci);
        }
        this.ch = ah.a(this.mContext, "pref-check-interval", -1L);
        this.cg = ah.a(this.mContext, "pref-check-start", System.currentTimeMillis());
        if (this.ch > 0 && this.ch > 60000) {
            this.ch = 60000;
        }
        ah.b(this.mContext, "pref-filename", this.ci);
        String f = ah.f(this.mContext, "pref-custom-info");
        if (DEBUG) {
            Log.d("UpdateManager", "Get saved custom info from pref:" + f);
        }
        this.cx = h(f);
    }

    private boolean a(Thread thread) {
        if (thread == null) {
            return false;
        }
        if (Thread.State.TERMINATED.equals(thread.getState())) {
            return false;
        }
        return true;
    }

    public boolean a(j jVar) {
        return a(jVar, null, null, null);
    }

    public boolean a(j jVar, Map map) {
        return a(jVar, map, (aa) null);
    }

    public boolean a(j jVar, aa aaVar) {
        return a(jVar, null, aaVar, null);
    }

    public boolean a(j jVar, Map map, aa aaVar) {
        return a(jVar, map, aaVar, null);
    }

    /* access modifiers changed from: protected */
    public boolean a(j jVar, Map map, aa aaVar, Callable callable) {
        synchronized (this.ct) {
            if (a(this.ck)) {
                if (DEBUG) {
                    Log.i("UpdateManager", "A previous check update task is working, start check failed");
                }
                return false;
            }
            HashMap hashMap = new HashMap();
            if (map != null) {
                hashMap.putAll(map);
            }
            if (aaVar != null) {
                hashMap.putAll(aaVar.ak());
            }
            this.ck = new n(this, jVar, hashMap, callable);
            this.ck.start();
            return true;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.dianxinos.appupdate.k.a(com.dianxinos.appupdate.i, boolean):void
     arg types: [com.dianxinos.appupdate.i, int]
     candidates:
      com.dianxinos.appupdate.k.a(com.dianxinos.appupdate.k, com.dianxinos.appupdate.DownloadService):com.dianxinos.appupdate.DownloadService
      com.dianxinos.appupdate.k.a(com.dianxinos.appupdate.k, com.dianxinos.appupdate.n):com.dianxinos.appupdate.n
      com.dianxinos.appupdate.k.a(com.dianxinos.appupdate.k, com.dianxinos.appupdate.z):com.dianxinos.appupdate.z
      com.dianxinos.appupdate.k.a(com.dianxinos.appupdate.k, java.lang.String):java.lang.String
      com.dianxinos.appupdate.k.a(com.dianxinos.appupdate.k, java.lang.Thread):java.lang.Thread
      com.dianxinos.appupdate.k.a(com.dianxinos.appupdate.k, java.io.File):void
      com.dianxinos.appupdate.k.a(java.lang.String, com.dianxinos.appupdate.i):void
      com.dianxinos.appupdate.k.a(com.dianxinos.appupdate.j, com.dianxinos.appupdate.aa):boolean
      com.dianxinos.appupdate.k.a(com.dianxinos.appupdate.j, java.util.Map):boolean
      com.dianxinos.appupdate.k.a(com.dianxinos.appupdate.i, boolean):void */
    public void a(i iVar) {
        a(iVar, false);
    }

    /* access modifiers changed from: protected */
    public void a(i iVar, boolean z) {
        synchronized (this.cu) {
            if (TextUtils.isEmpty(this.co)) {
                if (DEBUG) {
                    Log.d("UpdateManager", "Empty download URL:" + this.co + ", check update now");
                }
                a(new h(this, iVar));
            } else {
                a(this.co, iVar);
            }
        }
        if (!z) {
            ah.b(this.mContext, "pref-retry-count", 0);
        }
    }

    private void a(File file) {
        String str = this.mContext.getPackageName() + "-update_";
        if (file.exists() && file.isDirectory()) {
            File[] listFiles = file.listFiles(new g(this, str));
            long currentTimeMillis = System.currentTimeMillis();
            for (File file2 : listFiles) {
                long lastModified = currentTimeMillis - file2.lastModified();
                if (DEBUG) {
                    Log.d("UpdateManager", "file:" + file2.getAbsolutePath() + ", last modified:" + file2.lastModified());
                }
                if (lastModified < 0 || lastModified > 172800000) {
                    boolean delete = file2.delete();
                    if (DEBUG) {
                        Log.d("UpdateManager", "Delete old updated file " + file2.getAbsolutePath() + " removed:" + delete);
                    }
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.dianxinos.appupdate.ah.b(android.content.Context, java.lang.String, boolean):void
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.dianxinos.appupdate.ah.b(android.content.Context, java.lang.String, int):void
      com.dianxinos.appupdate.ah.b(android.content.Context, java.lang.String, long):void
      com.dianxinos.appupdate.ah.b(android.content.Context, java.lang.String, java.lang.String):void
      com.dianxinos.appupdate.ah.b(android.content.Context, java.lang.String, boolean):void */
    /* access modifiers changed from: private */
    public void a(String str, i iVar) {
        if (DEBUG) {
            Log.d("UpdateManager", "Before performing download, url:" + str);
        }
        if (!TextUtils.isEmpty(str) && ac.WEB_URL.matcher(str).matches()) {
            if (iVar != null) {
                iVar.Q();
            }
            Intent intent = new Intent("com.dianxinos.appupdate.intent.DOWNLOAD");
            intent.setData(Uri.parse(this.co));
            intent.putExtra("extra-filename", this.ci);
            intent.putExtra("extra-dest", this.cy);
            intent.putExtra("extra-file-size", this.cw.iu);
            intent.putExtra("extra-checksum", this.cw.L);
            if (this.cw != null) {
                intent.putExtra("extra-dspt", this.cw.description);
                intent.putExtra("extra-pri", this.cw.priority);
                intent.putExtra("extra-extra_info", a(this.cw.J));
            } else if (DEBUG) {
                Log.w("UpdateManager", "Lastest update info is null");
            }
            intent.setPackage(this.mContext.getPackageName());
            ah.b(this.mContext, "pref-last-down-url", str);
            ah.b(this.mContext, "pref-need-redownload", true);
            this.mContext.startService(intent);
        } else if (iVar != null) {
            iVar.R();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.dianxinos.appupdate.ah.b(android.content.Context, java.lang.String, boolean):void
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.dianxinos.appupdate.ah.b(android.content.Context, java.lang.String, int):void
      com.dianxinos.appupdate.ah.b(android.content.Context, java.lang.String, long):void
      com.dianxinos.appupdate.ah.b(android.content.Context, java.lang.String, java.lang.String):void
      com.dianxinos.appupdate.ah.b(android.content.Context, java.lang.String, boolean):void */
    public void ad() {
        Intent intent = new Intent("com.dianxinos.appupdate.intent.CANCEL_DOWNLOAD");
        intent.setPackage(this.mContext.getPackageName());
        this.mContext.startService(intent);
        ah.b(this.mContext, "pref-need-redownload", false);
        PendingIntent service = PendingIntent.getService(this.mContext, 0, new Intent("com.dianxinos.appupdate.intent.DOWNLOAD_RETRY"), 536870912);
        if (service != null) {
            ((AlarmManager) this.mContext.getSystemService("alarm")).cancel(service);
        }
        if (DEBUG) {
            Log.d("UpdateManager", "Cancel download intent sent");
        }
    }

    public boolean a(u uVar) {
        synchronized (this.cv) {
            if (a(this.cl)) {
                if (DEBUG) {
                    Log.d("UpdateManager", "A previous install thread interrupted");
                }
                return false;
            }
            a aVar = new a(this, uVar);
            StringBuilder append = new StringBuilder().append("InstallThread-");
            int i = cm + 1;
            cm = i;
            this.cl = new Thread(aVar, append.append(i).toString());
            this.cl.start();
            return true;
        }
    }

    /* access modifiers changed from: private */
    public void b(File file) {
        if (DEBUG) {
            Log.i("UpdateManager", "Installing update apk:" + file.getAbsolutePath());
        }
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
        intent.addFlags(268435456);
        this.mContext.startActivity(intent);
        if (DEBUG) {
            Log.d("UpdateManager", "PackageInstaller called");
        }
    }

    /* access modifiers changed from: protected */
    public z g(String str) {
        String optString;
        if (str != null) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                int optInt = jSONObject.optInt("vc", -1);
                String optString2 = jSONObject.optString("vn");
                String optString3 = jSONObject.optString("dspt");
                int optInt2 = jSONObject.optInt("prt", 0);
                if (optInt2 > 2) {
                    optInt2 = 2;
                }
                long optLong = jSONObject.optLong("time", 0);
                if (optLong < 0) {
                    optLong = 0;
                }
                long optLong2 = jSONObject.optLong("size");
                Iterator<String> keys = jSONObject.keys();
                HashMap hashMap = new HashMap();
                while (keys.hasNext()) {
                    String obj = keys.next().toString();
                    if (!x.il.contains(obj) && (optString = jSONObject.optString(obj)) != null) {
                        hashMap.put(obj, optString);
                    }
                }
                if (optInt > 0) {
                    z zVar = new z();
                    zVar.versionCode = optInt;
                    zVar.versionName = optString2;
                    zVar.priority = optInt2;
                    zVar.description = optString3;
                    zVar.timestamp = optLong;
                    zVar.J = hashMap;
                    zVar.iu = optLong2;
                    zVar.L = jSONObject.optString("md5");
                    return zVar;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void a(z zVar) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("vc", zVar.versionCode);
            jSONObject.put("vn", zVar.versionName);
            jSONObject.put("dspt", zVar.description);
            jSONObject.put("prt", zVar.priority);
            jSONObject.put("time", zVar.timestamp);
            jSONObject.put("size", zVar.iu);
            jSONObject.put("md5", zVar.L);
            if (zVar.J != null) {
                for (String str : zVar.J.keySet()) {
                    jSONObject.put(str, (String) zVar.J.get(str));
                }
            }
            String jSONObject2 = jSONObject.toString();
            if (DEBUG) {
                Log.d("UpdateManager", "saving update info:" + jSONObject2);
            }
            ah.b(this.mContext, "pref-update-info", jSONObject2);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public String a(Map map) {
        if (map == null) {
            return null;
        }
        JSONObject jSONObject = new JSONObject();
        for (String str : map.keySet()) {
            try {
                jSONObject.put(str, (String) map.get(str));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return jSONObject.toString();
    }

    /* access modifiers changed from: protected */
    public Map h(String str) {
        if (!TextUtils.isEmpty(str)) {
            try {
                HashMap hashMap = new HashMap();
                JSONObject jSONObject = new JSONObject(str);
                Iterator<String> keys = jSONObject.keys();
                while (keys.hasNext()) {
                    String obj = keys.next().toString();
                    String optString = jSONObject.optString(obj);
                    if (optString != null) {
                        hashMap.put(obj, optString);
                    }
                }
                return hashMap;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.dianxinos.appupdate.ah.a(android.content.Context, java.lang.String, long):long
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.dianxinos.appupdate.ah.a(android.content.Context, java.lang.String, int):int
      com.dianxinos.appupdate.ah.a(android.content.Context, java.lang.String, java.lang.String):java.lang.String
      com.dianxinos.appupdate.ah.a(android.content.Context, java.lang.String, boolean):boolean
      com.dianxinos.appupdate.ah.a(android.content.Context, java.lang.String, long):long */
    public z ae() {
        String str = DownloadHelpers.f(this.mContext, this.cy) + this.ci;
        if (y.e(this.mContext, str) == 0) {
            PackageInfo packageArchiveInfo = this.mContext.getPackageManager().getPackageArchiveInfo(str, 16384);
            if (packageArchiveInfo != null) {
                z zVar = new z();
                zVar.versionCode = packageArchiveInfo.versionCode;
                zVar.versionName = packageArchiveInfo.versionName;
                zVar.priority = ah.a(this.mContext, "pref-archive-pri", 0);
                zVar.description = ah.f(this.mContext, "pref-archive-dspt");
                zVar.J = h(ah.f(this.mContext, "pref-archive-extra"));
                zVar.timestamp = ah.a(this.mContext, "pref-archive-time", 0L);
                return zVar;
            } else if (DEBUG) {
                Log.w("UpdateManager", "Cannot get archive info for apk:" + str);
            }
        }
        return null;
    }

    public boolean af() {
        z ae = ae();
        if (ae == null) {
            if (DEBUG) {
                Log.d("UpdateManager", "No local archive, need download");
            }
            return true;
        } else if (this.cw == null) {
            if (DEBUG) {
                Log.d("UpdateManager", "No latest update info found, need download");
            }
            return true;
        } else {
            if (DEBUG) {
                Log.d("UpdateManager", "Latest version:" + this.cw.versionCode + ", local archive version:" + ae.versionCode);
            }
            return this.cw.versionCode > ae.versionCode;
        }
    }

    /* access modifiers changed from: protected */
    public Map ag() {
        return this.cx;
    }

    public void a(q qVar) {
        if (qVar != null) {
            this.cs = qVar;
            if (DEBUG) {
                Log.d("UpdateManager", "Binding download service");
            }
            if (this.cp != null) {
                this.cp.a(qVar);
            } else {
                this.mContext.bindService(new Intent(this.mContext, DownloadService.class), this.cz, 1);
            }
            if (DEBUG) {
                Log.d("UpdateManager", "After bind download service");
            }
        }
    }

    public void b(q qVar) {
        if (qVar != null) {
            this.cs = null;
            if (this.cp != null) {
                this.cp.b(qVar);
                this.mContext.unbindService(this.cz);
                this.cp = null;
                if (DEBUG) {
                    Log.d("UpdateManager", "DownloadProgressListener unregistered");
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void ah() {
        if (this.cg > 0 && this.ch > 0) {
            long currentTimeMillis = System.currentTimeMillis();
            if (this.cg > currentTimeMillis) {
                currentTimeMillis = this.cg;
                if (DEBUG) {
                    Log.d("UpdateManager", "Going to schedule a further check-update task");
                }
            }
            long j = currentTimeMillis;
            Intent intent = new Intent("com.dianxinos.appupdate.intent.CHECK_UPDATE");
            intent.setPackage(this.mContext.getPackageName());
            ((AlarmManager) this.mContext.getSystemService("alarm")).set(0, this.ch + j, PendingIntent.getService(this.mContext, 0, intent, 268435456));
            if (DEBUG) {
                Log.d("UpdateManager", "Schedule update in " + (this.ch / 1000) + " seconds since " + ((j - System.currentTimeMillis()) / 1000) + " seconds from now");
            }
        } else if (DEBUG) {
            Log.w("UpdateManager", "No need to schedule update-check, start:" + this.cg + ", interval:" + this.ch);
        }
    }

    /* access modifiers changed from: private */
    public String ai() {
        return DownloadHelpers.f(this.mContext, this.cy);
    }
}
