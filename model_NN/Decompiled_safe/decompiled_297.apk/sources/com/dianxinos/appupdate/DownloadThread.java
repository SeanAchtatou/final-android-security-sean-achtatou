package com.dianxinos.appupdate;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.Thread;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;

public class DownloadThread extends Thread {
    private static final boolean DEBUG = x.DEBUG;
    private r kL;
    private af kM;
    private long kN;
    private long kO;
    private List kP = new ArrayList();
    /* access modifiers changed from: private */
    public Context mContext;

    public DownloadThread(Context context, r rVar, af afVar) {
        this.mContext = context;
        this.kL = rVar;
        this.kM = afVar;
    }

    private String cP() {
        String str = this.kM.bg;
        if (str != null) {
        }
        if (str == null) {
            return "Appupdate model";
        }
        return str;
    }

    class StopRequest extends Throwable {
        public int mFinalStatus;

        public StopRequest(int i, String str) {
            super(str);
            this.mFinalStatus = i;
        }

        public StopRequest(int i, String str, Throwable th) {
            super(str, th);
            this.mFinalStatus = i;
        }
    }

    class RetryDownload extends Throwable {
        private RetryDownload() {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.dianxinos.appupdate.DownloadThread.a(com.dianxinos.appupdate.ae, org.apache.http.client.HttpClient, org.apache.http.client.methods.HttpGet):void
     arg types: [com.dianxinos.appupdate.ae, org.apache.http.impl.client.DefaultHttpClient, org.apache.http.client.methods.HttpGet]
     candidates:
      com.dianxinos.appupdate.DownloadThread.a(com.dianxinos.appupdate.ae, com.dianxinos.appupdate.ad, int):void
      com.dianxinos.appupdate.DownloadThread.a(com.dianxinos.appupdate.ae, com.dianxinos.appupdate.ad, org.apache.http.HttpResponse):void
      com.dianxinos.appupdate.DownloadThread.a(com.dianxinos.appupdate.ae, com.dianxinos.appupdate.ad, org.apache.http.client.methods.HttpGet):void
      com.dianxinos.appupdate.DownloadThread.a(com.dianxinos.appupdate.ae, com.dianxinos.appupdate.ad, boolean):void
      com.dianxinos.appupdate.DownloadThread.a(com.dianxinos.appupdate.ae, java.lang.String, java.lang.Exception):void
      com.dianxinos.appupdate.DownloadThread.a(com.dianxinos.appupdate.ae, org.apache.http.HttpResponse, int):void
      com.dianxinos.appupdate.DownloadThread.a(com.dianxinos.appupdate.ae, byte[], int):void
      com.dianxinos.appupdate.DownloadThread.a(java.lang.String, long, long):void
      com.dianxinos.appupdate.DownloadThread.a(com.dianxinos.appupdate.ae, org.apache.http.client.HttpClient, org.apache.http.client.methods.HttpGet):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.dianxinos.appupdate.ah.b(android.content.Context, java.lang.String, boolean):void
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.dianxinos.appupdate.ah.b(android.content.Context, java.lang.String, int):void
      com.dianxinos.appupdate.ah.b(android.content.Context, java.lang.String, long):void
      com.dianxinos.appupdate.ah.b(android.content.Context, java.lang.String, java.lang.String):void
      com.dianxinos.appupdate.ah.b(android.content.Context, java.lang.String, boolean):void */
    /* JADX WARNING: Code restructure failed: missing block: B:113:0x01ed, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:114:0x01ee, code lost:
        r7 = r6;
        r6 = r1;
        r1 = r0;
        r0 = 491;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0098, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0099, code lost:
        r2 = r0;
        r0 = r1;
        r1 = 491;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x0148, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x0149, code lost:
        r10 = r1;
        r1 = r0;
        r0 = r10;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:105:0x01d2  */
    /* JADX WARNING: Removed duplicated region for block: B:108:0x01e3  */
    /* JADX WARNING: Removed duplicated region for block: B:118:0x01fe  */
    /* JADX WARNING: Removed duplicated region for block: B:121:0x022d  */
    /* JADX WARNING: Removed duplicated region for block: B:139:0x024a  */
    /* JADX WARNING: Removed duplicated region for block: B:148:0x0273  */
    /* JADX WARNING: Removed duplicated region for block: B:151:0x0284  */
    /* JADX WARNING: Removed duplicated region for block: B:229:0x03ae  */
    /* JADX WARNING: Removed duplicated region for block: B:230:0x03b9  */
    /* JADX WARNING: Removed duplicated region for block: B:234:0x03c6  */
    /* JADX WARNING: Removed duplicated region for block: B:235:0x03d1  */
    /* JADX WARNING: Removed duplicated region for block: B:259:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x0148 A[ExcHandler: Throwable (r1v83 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:5:0x0029] */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x015d  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x018c  */
    /* JADX WARNING: Removed duplicated region for block: B:96:0x01a9  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r11 = this;
            r0 = 10
            android.os.Process.setThreadPriority(r0)
            com.dianxinos.appupdate.ae r4 = new com.dianxinos.appupdate.ae
            com.dianxinos.appupdate.af r0 = r11.kM
            r4.<init>(r11, r0)
            r1 = 0
            r2 = 0
            r3 = 491(0x1eb, float:6.88E-43)
            android.content.Context r0 = r11.mContext     // Catch:{ StopRequest -> 0x03fc, Throwable -> 0x03f5, all -> 0x03d5 }
            java.lang.String r5 = "power"
            java.lang.Object r0 = r0.getSystemService(r5)     // Catch:{ StopRequest -> 0x03fc, Throwable -> 0x03f5, all -> 0x03d5 }
            android.os.PowerManager r0 = (android.os.PowerManager) r0     // Catch:{ StopRequest -> 0x03fc, Throwable -> 0x03f5, all -> 0x03d5 }
            r5 = 1
            java.lang.String r6 = "App update module"
            android.os.PowerManager$WakeLock r6 = r0.newWakeLock(r5, r6)     // Catch:{ StopRequest -> 0x03fc, Throwable -> 0x03f5, all -> 0x03d5 }
            r6.acquire()     // Catch:{ StopRequest -> 0x0402, Throwable -> 0x03f9, all -> 0x03db }
            org.apache.http.impl.client.DefaultHttpClient r0 = new org.apache.http.impl.client.DefaultHttpClient     // Catch:{ StopRequest -> 0x0402, Throwable -> 0x03f9, all -> 0x03db }
            r0.<init>()     // Catch:{ StopRequest -> 0x0402, Throwable -> 0x03f9, all -> 0x03db }
            org.apache.http.params.HttpParams r1 = r0.getParams()     // Catch:{ StopRequest -> 0x0098, Throwable -> 0x0148, all -> 0x01ed }
            r2 = 60000(0xea60, float:8.4078E-41)
            org.apache.http.params.HttpConnectionParams.setSoTimeout(r1, r2)     // Catch:{ StopRequest -> 0x0098, Throwable -> 0x0148, all -> 0x01ed }
            com.dianxinos.appupdate.r r1 = r11.kL     // Catch:{ StopRequest -> 0x0098, Throwable -> 0x0148, all -> 0x01ed }
            java.lang.String r1 = r1.e()     // Catch:{ StopRequest -> 0x0098, Throwable -> 0x0148, all -> 0x01ed }
            com.dianxinos.appupdate.r r2 = r11.kL     // Catch:{ StopRequest -> 0x0098, Throwable -> 0x0148, all -> 0x01ed }
            int r2 = r2.f()     // Catch:{ StopRequest -> 0x0098, Throwable -> 0x0148, all -> 0x01ed }
            if (r1 == 0) goto L_0x0079
            if (r2 <= 0) goto L_0x0079
            boolean r5 = com.dianxinos.appupdate.DownloadThread.DEBUG     // Catch:{ StopRequest -> 0x0098, Throwable -> 0x0148, all -> 0x01ed }
            if (r5 == 0) goto L_0x0069
            java.lang.String r5 = "DownloadThread"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ StopRequest -> 0x0098, Throwable -> 0x0148, all -> 0x01ed }
            r7.<init>()     // Catch:{ StopRequest -> 0x0098, Throwable -> 0x0148, all -> 0x01ed }
            java.lang.String r8 = "Connecting with proxy, addr:"
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ StopRequest -> 0x0098, Throwable -> 0x0148, all -> 0x01ed }
            java.lang.StringBuilder r7 = r7.append(r1)     // Catch:{ StopRequest -> 0x0098, Throwable -> 0x0148, all -> 0x01ed }
            java.lang.String r8 = ":"
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ StopRequest -> 0x0098, Throwable -> 0x0148, all -> 0x01ed }
            java.lang.StringBuilder r7 = r7.append(r2)     // Catch:{ StopRequest -> 0x0098, Throwable -> 0x0148, all -> 0x01ed }
            java.lang.String r7 = r7.toString()     // Catch:{ StopRequest -> 0x0098, Throwable -> 0x0148, all -> 0x01ed }
            android.util.Log.i(r5, r7)     // Catch:{ StopRequest -> 0x0098, Throwable -> 0x0148, all -> 0x01ed }
        L_0x0069:
            org.apache.http.HttpHost r5 = new org.apache.http.HttpHost     // Catch:{ StopRequest -> 0x0098, Throwable -> 0x0148, all -> 0x01ed }
            java.lang.String r7 = "http"
            r5.<init>(r1, r2, r7)     // Catch:{ StopRequest -> 0x0098, Throwable -> 0x0148, all -> 0x01ed }
            org.apache.http.params.HttpParams r1 = r0.getParams()     // Catch:{ StopRequest -> 0x0098, Throwable -> 0x0148, all -> 0x01ed }
            java.lang.String r2 = "http.route.default-proxy"
            r1.setParameter(r2, r5)     // Catch:{ StopRequest -> 0x0098, Throwable -> 0x0148, all -> 0x01ed }
        L_0x0079:
            r1 = 0
            r7 = 0
            r11.kO = r7     // Catch:{ StopRequest -> 0x0098, Throwable -> 0x0148, all -> 0x01ed }
        L_0x007e:
            if (r1 != 0) goto L_0x0288
            org.apache.http.client.methods.HttpGet r2 = new org.apache.http.client.methods.HttpGet     // Catch:{ StopRequest -> 0x0098, Throwable -> 0x0148, all -> 0x01ed }
            java.lang.String r5 = r4.jT     // Catch:{ StopRequest -> 0x0098, Throwable -> 0x0148, all -> 0x01ed }
            r2.<init>(r5)     // Catch:{ StopRequest -> 0x0098, Throwable -> 0x0148, all -> 0x01ed }
            java.lang.String r5 = "User-Agent"
            java.lang.String r7 = r11.cP()     // Catch:{ StopRequest -> 0x0098, Throwable -> 0x0148, all -> 0x01ed }
            r2.setHeader(r5, r7)     // Catch:{ StopRequest -> 0x0098, Throwable -> 0x0148, all -> 0x01ed }
            r11.a(r4, r0, r2)     // Catch:{ RetryDownload -> 0x0137 }
            r1 = 1
            r2.abort()     // Catch:{ StopRequest -> 0x0098, Throwable -> 0x0148, all -> 0x01ed }
            goto L_0x007e
        L_0x0098:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r3
        L_0x009c:
            int r1 = r0.mFinalStatus     // Catch:{ all -> 0x03e8 }
            r0.printStackTrace()     // Catch:{ all -> 0x03e8 }
            if (r2 == 0) goto L_0x00a3
        L_0x00a3:
            r11.a(r4, r1)
            int r0 = r11.ad(r1)
            r1 = -1
            if (r0 != r1) goto L_0x00d9
            android.content.Context r1 = r11.mContext
            java.lang.String r2 = "pref-archive-dspt"
            com.dianxinos.appupdate.af r3 = r11.kM
            java.lang.String r3 = r3.description
            com.dianxinos.appupdate.ah.b(r1, r2, r3)
            android.content.Context r1 = r11.mContext
            java.lang.String r2 = "pref-archive-extra"
            com.dianxinos.appupdate.af r3 = r11.kM
            java.lang.String r3 = r3.kI
            com.dianxinos.appupdate.ah.b(r1, r2, r3)
            android.content.Context r1 = r11.mContext
            java.lang.String r2 = "pref-archive-pri"
            com.dianxinos.appupdate.af r3 = r11.kM
            int r3 = r3.priority
            com.dianxinos.appupdate.ah.b(r1, r2, r3)
            android.content.Context r1 = r11.mContext
            java.lang.String r2 = "pref-archive-time"
            long r7 = java.lang.System.currentTimeMillis()
            com.dianxinos.appupdate.ah.b(r1, r2, r7)
        L_0x00d9:
            r1 = -1
            if (r0 == r1) goto L_0x038d
            r1 = 1
            if (r0 == r1) goto L_0x038d
            r1 = 8
            if (r0 == r1) goto L_0x038d
            r1 = 5
            if (r0 == r1) goto L_0x038d
            r1 = 6
            if (r0 == r1) goto L_0x038d
            r1 = 4
            if (r0 == r1) goto L_0x038d
            r1 = 9
            if (r0 == r1) goto L_0x038d
            r1 = 7
            if (r0 == r1) goto L_0x038d
            r1 = 6
            if (r0 == r1) goto L_0x038d
            r1 = 1
        L_0x00f7:
            if (r1 == 0) goto L_0x0396
            android.content.Context r0 = r11.mContext
            java.lang.String r1 = "pref-retry-count"
            r2 = 0
            int r0 = com.dianxinos.appupdate.ah.a(r0, r1, r2)
            int r0 = r0 + 1
            android.content.Context r1 = r11.mContext
            java.lang.String r2 = "pref-retry-count"
            com.dianxinos.appupdate.ah.b(r1, r2, r0)
            int r1 = com.dianxinos.appupdate.x.ik
            if (r0 <= r1) goto L_0x0390
            r0 = 10
        L_0x0111:
            android.content.Context r1 = r11.mContext
            java.lang.String r2 = "pref-need-redownload"
            r3 = 2
            if (r0 != r3) goto L_0x0393
            r3 = 1
        L_0x0119:
            com.dianxinos.appupdate.ah.b(r1, r2, r3)
            r5 = r0
        L_0x011d:
            java.lang.String r1 = r4.jM
            r0 = -1
            if (r5 != r0) goto L_0x03a1
            r0 = 1
            r2 = r0
        L_0x0124:
            int r3 = r4.jP
            java.lang.String r4 = r4.jR
            r0 = r11
            r0.a(r1, r2, r3, r4, r5)
            com.dianxinos.appupdate.af r0 = r11.kM
            r1 = 0
            r0.kK = r1
            if (r6 == 0) goto L_0x0136
            r6.release()
        L_0x0136:
            return
        L_0x0137:
            r5 = move-exception
            r5.printStackTrace()     // Catch:{ all -> 0x01e8 }
            com.dianxinos.appupdate.af r5 = r11.kM     // Catch:{ all -> 0x01e8 }
            int r7 = r5.kF     // Catch:{ all -> 0x01e8 }
            int r7 = r7 + 1
            r5.kF = r7     // Catch:{ all -> 0x01e8 }
            r2.abort()     // Catch:{ StopRequest -> 0x0098, Throwable -> 0x0148, all -> 0x01ed }
            goto L_0x007e
        L_0x0148:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
        L_0x014c:
            r2 = 491(0x1eb, float:6.88E-43)
            r0.printStackTrace()     // Catch:{ all -> 0x03ef }
            if (r1 == 0) goto L_0x0153
        L_0x0153:
            r11.a(r4, r2)
            int r0 = r11.ad(r2)
            r1 = -1
            if (r0 != r1) goto L_0x0189
            android.content.Context r1 = r11.mContext
            java.lang.String r2 = "pref-archive-dspt"
            com.dianxinos.appupdate.af r3 = r11.kM
            java.lang.String r3 = r3.description
            com.dianxinos.appupdate.ah.b(r1, r2, r3)
            android.content.Context r1 = r11.mContext
            java.lang.String r2 = "pref-archive-extra"
            com.dianxinos.appupdate.af r3 = r11.kM
            java.lang.String r3 = r3.kI
            com.dianxinos.appupdate.ah.b(r1, r2, r3)
            android.content.Context r1 = r11.mContext
            java.lang.String r2 = "pref-archive-pri"
            com.dianxinos.appupdate.af r3 = r11.kM
            int r3 = r3.priority
            com.dianxinos.appupdate.ah.b(r1, r2, r3)
            android.content.Context r1 = r11.mContext
            java.lang.String r2 = "pref-archive-time"
            long r7 = java.lang.System.currentTimeMillis()
            com.dianxinos.appupdate.ah.b(r1, r2, r7)
        L_0x0189:
            r1 = -1
            if (r0 == r1) goto L_0x03a5
            r1 = 1
            if (r0 == r1) goto L_0x03a5
            r1 = 8
            if (r0 == r1) goto L_0x03a5
            r1 = 5
            if (r0 == r1) goto L_0x03a5
            r1 = 6
            if (r0 == r1) goto L_0x03a5
            r1 = 4
            if (r0 == r1) goto L_0x03a5
            r1 = 9
            if (r0 == r1) goto L_0x03a5
            r1 = 7
            if (r0 == r1) goto L_0x03a5
            r1 = 6
            if (r0 == r1) goto L_0x03a5
            r1 = 1
        L_0x01a7:
            if (r1 == 0) goto L_0x03ae
            android.content.Context r0 = r11.mContext
            java.lang.String r1 = "pref-retry-count"
            r2 = 0
            int r0 = com.dianxinos.appupdate.ah.a(r0, r1, r2)
            int r0 = r0 + 1
            android.content.Context r1 = r11.mContext
            java.lang.String r2 = "pref-retry-count"
            com.dianxinos.appupdate.ah.b(r1, r2, r0)
            int r1 = com.dianxinos.appupdate.x.ik
            if (r0 <= r1) goto L_0x03a8
            r0 = 10
        L_0x01c1:
            android.content.Context r1 = r11.mContext
            java.lang.String r2 = "pref-need-redownload"
            r3 = 2
            if (r0 != r3) goto L_0x03ab
            r3 = 1
        L_0x01c9:
            com.dianxinos.appupdate.ah.b(r1, r2, r3)
            r5 = r0
        L_0x01cd:
            java.lang.String r1 = r4.jM
            r0 = -1
            if (r5 != r0) goto L_0x03b9
            r0 = 1
            r2 = r0
        L_0x01d4:
            int r3 = r4.jP
            java.lang.String r4 = r4.jR
            r0 = r11
            r0.a(r1, r2, r3, r4, r5)
            com.dianxinos.appupdate.af r0 = r11.kM
            r1 = 0
            r0.kK = r1
            if (r6 == 0) goto L_0x0136
            r6.release()
            goto L_0x0136
        L_0x01e8:
            r1 = move-exception
            r2.abort()     // Catch:{ StopRequest -> 0x0098, Throwable -> 0x0148, all -> 0x01ed }
            throw r1     // Catch:{ StopRequest -> 0x0098, Throwable -> 0x0148, all -> 0x01ed }
        L_0x01ed:
            r1 = move-exception
            r7 = r6
            r6 = r1
            r1 = r0
            r0 = r3
        L_0x01f2:
            if (r1 == 0) goto L_0x01f4
        L_0x01f4:
            r11.a(r4, r0)
            int r0 = r11.ad(r0)
            r1 = -1
            if (r0 != r1) goto L_0x022a
            android.content.Context r1 = r11.mContext
            java.lang.String r2 = "pref-archive-dspt"
            com.dianxinos.appupdate.af r3 = r11.kM
            java.lang.String r3 = r3.description
            com.dianxinos.appupdate.ah.b(r1, r2, r3)
            android.content.Context r1 = r11.mContext
            java.lang.String r2 = "pref-archive-extra"
            com.dianxinos.appupdate.af r3 = r11.kM
            java.lang.String r3 = r3.kI
            com.dianxinos.appupdate.ah.b(r1, r2, r3)
            android.content.Context r1 = r11.mContext
            java.lang.String r2 = "pref-archive-pri"
            com.dianxinos.appupdate.af r3 = r11.kM
            int r3 = r3.priority
            com.dianxinos.appupdate.ah.b(r1, r2, r3)
            android.content.Context r1 = r11.mContext
            java.lang.String r2 = "pref-archive-time"
            long r8 = java.lang.System.currentTimeMillis()
            com.dianxinos.appupdate.ah.b(r1, r2, r8)
        L_0x022a:
            r1 = -1
            if (r0 == r1) goto L_0x03bd
            r1 = 1
            if (r0 == r1) goto L_0x03bd
            r1 = 8
            if (r0 == r1) goto L_0x03bd
            r1 = 5
            if (r0 == r1) goto L_0x03bd
            r1 = 6
            if (r0 == r1) goto L_0x03bd
            r1 = 4
            if (r0 == r1) goto L_0x03bd
            r1 = 9
            if (r0 == r1) goto L_0x03bd
            r1 = 7
            if (r0 == r1) goto L_0x03bd
            r1 = 6
            if (r0 == r1) goto L_0x03bd
            r1 = 1
        L_0x0248:
            if (r1 == 0) goto L_0x03c6
            android.content.Context r0 = r11.mContext
            java.lang.String r1 = "pref-retry-count"
            r2 = 0
            int r0 = com.dianxinos.appupdate.ah.a(r0, r1, r2)
            int r0 = r0 + 1
            android.content.Context r1 = r11.mContext
            java.lang.String r2 = "pref-retry-count"
            com.dianxinos.appupdate.ah.b(r1, r2, r0)
            int r1 = com.dianxinos.appupdate.x.ik
            if (r0 <= r1) goto L_0x03c0
            r0 = 10
        L_0x0262:
            android.content.Context r1 = r11.mContext
            java.lang.String r2 = "pref-need-redownload"
            r3 = 2
            if (r0 != r3) goto L_0x03c3
            r3 = 1
        L_0x026a:
            com.dianxinos.appupdate.ah.b(r1, r2, r3)
            r5 = r0
        L_0x026e:
            java.lang.String r1 = r4.jM
            r0 = -1
            if (r5 != r0) goto L_0x03d1
            r0 = 1
            r2 = r0
        L_0x0275:
            int r3 = r4.jP
            java.lang.String r4 = r4.jR
            r0 = r11
            r0.a(r1, r2, r3, r4, r5)
            com.dianxinos.appupdate.af r0 = r11.kM
            r1 = 0
            r0.kK = r1
            if (r7 == 0) goto L_0x0287
            r7.release()
        L_0x0287:
            throw r6
        L_0x0288:
            r11.b(r4)     // Catch:{ StopRequest -> 0x0098, Throwable -> 0x0148, all -> 0x01ed }
            r1 = 200(0xc8, float:2.8E-43)
            if (r3 == r1) goto L_0x0407
            java.io.File r1 = new java.io.File     // Catch:{ StopRequest -> 0x0098, Throwable -> 0x0148, all -> 0x01ed }
            java.lang.String r2 = r4.jM     // Catch:{ StopRequest -> 0x0098, Throwable -> 0x0148, all -> 0x01ed }
            r1.<init>(r2)     // Catch:{ StopRequest -> 0x0098, Throwable -> 0x0148, all -> 0x01ed }
            r2 = 200(0xc8, float:2.8E-43)
            boolean r3 = r1.exists()     // Catch:{ StopRequest -> 0x0373, Throwable -> 0x0148, all -> 0x03e1 }
            if (r3 == 0) goto L_0x0364
            boolean r3 = r1.isFile()     // Catch:{ StopRequest -> 0x0373, Throwable -> 0x0148, all -> 0x03e1 }
            if (r3 == 0) goto L_0x0364
            com.dianxinos.appupdate.af r3 = r11.kM     // Catch:{ StopRequest -> 0x0373, Throwable -> 0x0148, all -> 0x03e1 }
            java.lang.String r3 = r3.L     // Catch:{ StopRequest -> 0x0373, Throwable -> 0x0148, all -> 0x03e1 }
            boolean r3 = android.text.TextUtils.isEmpty(r3)     // Catch:{ StopRequest -> 0x0373, Throwable -> 0x0148, all -> 0x03e1 }
            if (r3 != 0) goto L_0x0367
            java.lang.String r3 = com.dianxinos.appupdate.m.c(r1)     // Catch:{ StopRequest -> 0x0373, Throwable -> 0x0148, all -> 0x03e1 }
            com.dianxinos.appupdate.af r5 = r11.kM     // Catch:{ StopRequest -> 0x0373, Throwable -> 0x0148, all -> 0x03e1 }
            java.lang.String r5 = r5.L     // Catch:{ StopRequest -> 0x0373, Throwable -> 0x0148, all -> 0x03e1 }
            boolean r3 = r5.equals(r3)     // Catch:{ StopRequest -> 0x0373, Throwable -> 0x0148, all -> 0x03e1 }
            if (r3 != 0) goto L_0x0359
            r2 = 500(0x1f4, float:7.0E-43)
            r1.delete()     // Catch:{ StopRequest -> 0x0373, Throwable -> 0x0148, all -> 0x03e1 }
            r1 = r2
        L_0x02c2:
            if (r0 == 0) goto L_0x02c4
        L_0x02c4:
            r11.a(r4, r1)
            int r0 = r11.ad(r1)
            r1 = -1
            if (r0 != r1) goto L_0x02fa
            android.content.Context r1 = r11.mContext
            java.lang.String r2 = "pref-archive-dspt"
            com.dianxinos.appupdate.af r3 = r11.kM
            java.lang.String r3 = r3.description
            com.dianxinos.appupdate.ah.b(r1, r2, r3)
            android.content.Context r1 = r11.mContext
            java.lang.String r2 = "pref-archive-extra"
            com.dianxinos.appupdate.af r3 = r11.kM
            java.lang.String r3 = r3.kI
            com.dianxinos.appupdate.ah.b(r1, r2, r3)
            android.content.Context r1 = r11.mContext
            java.lang.String r2 = "pref-archive-pri"
            com.dianxinos.appupdate.af r3 = r11.kM
            int r3 = r3.priority
            com.dianxinos.appupdate.ah.b(r1, r2, r3)
            android.content.Context r1 = r11.mContext
            java.lang.String r2 = "pref-archive-time"
            long r7 = java.lang.System.currentTimeMillis()
            com.dianxinos.appupdate.ah.b(r1, r2, r7)
        L_0x02fa:
            r1 = -1
            if (r0 == r1) goto L_0x037a
            r1 = 1
            if (r0 == r1) goto L_0x037a
            r1 = 8
            if (r0 == r1) goto L_0x037a
            r1 = 5
            if (r0 == r1) goto L_0x037a
            r1 = 6
            if (r0 == r1) goto L_0x037a
            r1 = 4
            if (r0 == r1) goto L_0x037a
            r1 = 9
            if (r0 == r1) goto L_0x037a
            r1 = 7
            if (r0 == r1) goto L_0x037a
            r1 = 6
            if (r0 == r1) goto L_0x037a
            r1 = 1
        L_0x0318:
            if (r1 == 0) goto L_0x0380
            android.content.Context r0 = r11.mContext
            java.lang.String r1 = "pref-retry-count"
            r2 = 0
            int r0 = com.dianxinos.appupdate.ah.a(r0, r1, r2)
            int r0 = r0 + 1
            android.content.Context r1 = r11.mContext
            java.lang.String r2 = "pref-retry-count"
            com.dianxinos.appupdate.ah.b(r1, r2, r0)
            int r1 = com.dianxinos.appupdate.x.ik
            if (r0 <= r1) goto L_0x037c
            r0 = 10
        L_0x0332:
            android.content.Context r1 = r11.mContext
            java.lang.String r2 = "pref-need-redownload"
            r3 = 2
            if (r0 != r3) goto L_0x037e
            r3 = 1
        L_0x033a:
            com.dianxinos.appupdate.ah.b(r1, r2, r3)
            r5 = r0
        L_0x033e:
            java.lang.String r1 = r4.jM
            r0 = -1
            if (r5 != r0) goto L_0x038a
            r0 = 1
            r2 = r0
        L_0x0345:
            int r3 = r4.jP
            java.lang.String r4 = r4.jR
            r0 = r11
            r0.a(r1, r2, r3, r4, r5)
            com.dianxinos.appupdate.af r0 = r11.kM
            r1 = 0
            r0.kK = r1
            if (r6 == 0) goto L_0x0136
            r6.release()
            goto L_0x0136
        L_0x0359:
            boolean r1 = com.dianxinos.appupdate.DownloadThread.DEBUG     // Catch:{ StopRequest -> 0x0373, Throwable -> 0x0148, all -> 0x03e1 }
            if (r1 == 0) goto L_0x0364
            java.lang.String r1 = "DownloadThread"
            java.lang.String r3 = "MD5SUM is same"
            android.util.Log.i(r1, r3)     // Catch:{ StopRequest -> 0x0373, Throwable -> 0x0148, all -> 0x03e1 }
        L_0x0364:
            r1 = r2
            goto L_0x02c2
        L_0x0367:
            boolean r1 = com.dianxinos.appupdate.DownloadThread.DEBUG     // Catch:{ StopRequest -> 0x0373, Throwable -> 0x0148, all -> 0x03e1 }
            if (r1 == 0) goto L_0x0364
            java.lang.String r1 = "DownloadThread"
            java.lang.String r3 = "Target md5sum not specified"
            android.util.Log.w(r1, r3)     // Catch:{ StopRequest -> 0x0373, Throwable -> 0x0148, all -> 0x03e1 }
            goto L_0x0364
        L_0x0373:
            r1 = move-exception
            r10 = r1
            r1 = r2
            r2 = r0
            r0 = r10
            goto L_0x009c
        L_0x037a:
            r1 = 0
            goto L_0x0318
        L_0x037c:
            r0 = 2
            goto L_0x0332
        L_0x037e:
            r3 = 0
            goto L_0x033a
        L_0x0380:
            android.content.Context r1 = r11.mContext
            java.lang.String r2 = "pref-need-redownload"
            r3 = 0
            com.dianxinos.appupdate.ah.b(r1, r2, r3)
            r5 = r0
            goto L_0x033e
        L_0x038a:
            r0 = 0
            r2 = r0
            goto L_0x0345
        L_0x038d:
            r1 = 0
            goto L_0x00f7
        L_0x0390:
            r0 = 2
            goto L_0x0111
        L_0x0393:
            r3 = 0
            goto L_0x0119
        L_0x0396:
            android.content.Context r1 = r11.mContext
            java.lang.String r2 = "pref-need-redownload"
            r3 = 0
            com.dianxinos.appupdate.ah.b(r1, r2, r3)
            r5 = r0
            goto L_0x011d
        L_0x03a1:
            r0 = 0
            r2 = r0
            goto L_0x0124
        L_0x03a5:
            r1 = 0
            goto L_0x01a7
        L_0x03a8:
            r0 = 2
            goto L_0x01c1
        L_0x03ab:
            r3 = 0
            goto L_0x01c9
        L_0x03ae:
            android.content.Context r1 = r11.mContext
            java.lang.String r2 = "pref-need-redownload"
            r3 = 0
            com.dianxinos.appupdate.ah.b(r1, r2, r3)
            r5 = r0
            goto L_0x01cd
        L_0x03b9:
            r0 = 0
            r2 = r0
            goto L_0x01d4
        L_0x03bd:
            r1 = 0
            goto L_0x0248
        L_0x03c0:
            r0 = 2
            goto L_0x0262
        L_0x03c3:
            r3 = 0
            goto L_0x026a
        L_0x03c6:
            android.content.Context r1 = r11.mContext
            java.lang.String r2 = "pref-need-redownload"
            r3 = 0
            com.dianxinos.appupdate.ah.b(r1, r2, r3)
            r5 = r0
            goto L_0x026e
        L_0x03d1:
            r0 = 0
            r2 = r0
            goto L_0x0275
        L_0x03d5:
            r0 = move-exception
            r6 = r0
            r7 = r2
            r0 = r3
            goto L_0x01f2
        L_0x03db:
            r0 = move-exception
            r7 = r6
            r6 = r0
            r0 = r3
            goto L_0x01f2
        L_0x03e1:
            r1 = move-exception
            r7 = r6
            r6 = r1
            r1 = r0
            r0 = r2
            goto L_0x01f2
        L_0x03e8:
            r0 = move-exception
            r7 = r6
            r6 = r0
            r0 = r1
            r1 = r2
            goto L_0x01f2
        L_0x03ef:
            r0 = move-exception
            r7 = r6
            r6 = r0
            r0 = r2
            goto L_0x01f2
        L_0x03f5:
            r0 = move-exception
            r6 = r2
            goto L_0x014c
        L_0x03f9:
            r0 = move-exception
            goto L_0x014c
        L_0x03fc:
            r0 = move-exception
            r6 = r2
            r2 = r1
            r1 = r3
            goto L_0x009c
        L_0x0402:
            r0 = move-exception
            r2 = r1
            r1 = r3
            goto L_0x009c
        L_0x0407:
            r1 = r3
            goto L_0x02c2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.dianxinos.appupdate.DownloadThread.run():void");
    }

    private void a(ae aeVar, HttpClient httpClient, HttpGet httpGet) {
        ad adVar = new ad();
        byte[] bArr = new byte[4096];
        a(aeVar, adVar, httpGet);
        f(aeVar);
        a(adVar, httpGet);
        a(aeVar);
        HttpResponse b = b(aeVar, httpClient, httpGet);
        f(aeVar);
        c(aeVar, adVar, b);
        if (DEBUG) {
            Log.v("DownloadThread", "received response for " + httpGet.getURI() + ", status:" + b.getStatusLine().getStatusCode());
        }
        a(aeVar, adVar, b);
        if (this.kM.kE == 1) {
            if (DEBUG) {
                Log.i("DownloadThread", "Download paused");
            }
            throw new StopRequest(193, "Dowload paused after before start receive data");
        }
        a(aeVar.jM, (long) adVar.jy, this.kM.kG);
        a(aeVar, adVar, bArr, a(aeVar, b));
    }

    private void a(ae aeVar) {
        int cO = this.kM.cO();
        if (cO != 1) {
            int i = 195;
            if (cO == 5) {
                i = 197;
            }
            throw new StopRequest(i, this.kM.aa(cO));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.dianxinos.appupdate.DownloadThread.a(com.dianxinos.appupdate.ae, com.dianxinos.appupdate.ad, boolean):void
     arg types: [com.dianxinos.appupdate.ae, com.dianxinos.appupdate.ad, int]
     candidates:
      com.dianxinos.appupdate.DownloadThread.a(com.dianxinos.appupdate.ae, com.dianxinos.appupdate.ad, int):void
      com.dianxinos.appupdate.DownloadThread.a(com.dianxinos.appupdate.ae, com.dianxinos.appupdate.ad, org.apache.http.HttpResponse):void
      com.dianxinos.appupdate.DownloadThread.a(com.dianxinos.appupdate.ae, com.dianxinos.appupdate.ad, org.apache.http.client.methods.HttpGet):void
      com.dianxinos.appupdate.DownloadThread.a(com.dianxinos.appupdate.ae, java.lang.String, java.lang.Exception):void
      com.dianxinos.appupdate.DownloadThread.a(com.dianxinos.appupdate.ae, org.apache.http.HttpResponse, int):void
      com.dianxinos.appupdate.DownloadThread.a(com.dianxinos.appupdate.ae, org.apache.http.client.HttpClient, org.apache.http.client.methods.HttpGet):void
      com.dianxinos.appupdate.DownloadThread.a(com.dianxinos.appupdate.ae, byte[], int):void
      com.dianxinos.appupdate.DownloadThread.a(java.lang.String, long, long):void
      com.dianxinos.appupdate.DownloadThread.a(com.dianxinos.appupdate.ae, com.dianxinos.appupdate.ad, boolean):void */
    private void a(ae aeVar, ad adVar, byte[] bArr, InputStream inputStream) {
        while (true) {
            int b = b(aeVar, adVar, bArr, inputStream);
            if (b == -1) {
                a(aeVar, adVar, true);
                a(aeVar, adVar);
                return;
            }
            aeVar.jS = true;
            a(aeVar, bArr, b);
            adVar.jy += b;
            adVar.jz += (long) b;
            this.kO += (long) b;
            a(aeVar, adVar, false);
            f(aeVar);
            if (this.kM.kG > 0 && ((long) adVar.jy) > this.kM.kG) {
                if (DEBUG) {
                    Log.w("DownloadThread", "File size exceeds");
                }
                a(aeVar, 489);
                throw new RetryDownload();
            }
        }
    }

    private void b(ae aeVar) {
        if (d(aeVar)) {
            Log.e("DownloadThread", "Drm file, not supported at present");
        } else {
            c(aeVar);
        }
    }

    private void a(ae aeVar, int i) {
        e(aeVar);
        if (aeVar.jM != null && i == 489) {
            new File(aeVar.jM).delete();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException} */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x005e A[SYNTHETIC, Splitter:B:20:0x005e] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x009e A[SYNTHETIC, Splitter:B:32:0x009e] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00e0 A[SYNTHETIC, Splitter:B:44:0x00e0] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x0106 A[SYNTHETIC, Splitter:B:56:0x0106] */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x0125 A[SYNTHETIC, Splitter:B:65:0x0125] */
    /* JADX WARNING: Removed duplicated region for block: B:87:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:90:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:93:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:96:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void c(com.dianxinos.appupdate.ae r10) {
        /*
            r9 = this;
            java.lang.String r0 = "file "
            java.lang.String r7 = "exception while closing file: "
            java.lang.String r6 = "IOException while closing synced file: "
            java.lang.String r5 = "DownloadThread"
            java.io.FileOutputStream r0 = r10.jN
            if (r0 != 0) goto L_0x0015
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x0034, SyncFailedException -> 0x0074, IOException -> 0x00b6, RuntimeException -> 0x00f9, all -> 0x011f }
            java.lang.String r2 = r10.jM     // Catch:{ FileNotFoundException -> 0x0034, SyncFailedException -> 0x0074, IOException -> 0x00b6, RuntimeException -> 0x00f9, all -> 0x011f }
            r3 = 1
            r1.<init>(r2, r3)     // Catch:{ FileNotFoundException -> 0x0034, SyncFailedException -> 0x0074, IOException -> 0x00b6, RuntimeException -> 0x00f9, all -> 0x011f }
            r0 = r1
        L_0x0015:
            java.io.FileDescriptor r1 = r0.getFD()     // Catch:{ FileNotFoundException -> 0x0153, SyncFailedException -> 0x014d, IOException -> 0x0147, RuntimeException -> 0x0142, all -> 0x013b }
            r1.sync()     // Catch:{ FileNotFoundException -> 0x0153, SyncFailedException -> 0x014d, IOException -> 0x0147, RuntimeException -> 0x0142, all -> 0x013b }
            if (r0 == 0) goto L_0x0021
            r0.close()     // Catch:{ IOException -> 0x0022, RuntimeException -> 0x002b }
        L_0x0021:
            return
        L_0x0022:
            r0 = move-exception
            java.lang.String r1 = "DownloadThread"
            java.lang.String r1 = "IOException while closing synced file: "
            android.util.Log.w(r5, r6, r0)
            goto L_0x0021
        L_0x002b:
            r0 = move-exception
            java.lang.String r1 = "DownloadThread"
            java.lang.String r1 = "exception while closing file: "
            android.util.Log.w(r5, r7, r0)
            goto L_0x0021
        L_0x0034:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
        L_0x0038:
            java.lang.String r2 = "DownloadThread"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0140 }
            r3.<init>()     // Catch:{ all -> 0x0140 }
            java.lang.String r4 = "file "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0140 }
            java.lang.String r4 = r10.jM     // Catch:{ all -> 0x0140 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0140 }
            java.lang.String r4 = " not found: "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0140 }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ all -> 0x0140 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0140 }
            android.util.Log.w(r2, r0)     // Catch:{ all -> 0x0140 }
            if (r1 == 0) goto L_0x0021
            r1.close()     // Catch:{ IOException -> 0x0062, RuntimeException -> 0x006b }
            goto L_0x0021
        L_0x0062:
            r0 = move-exception
            java.lang.String r1 = "DownloadThread"
            java.lang.String r1 = "IOException while closing synced file: "
            android.util.Log.w(r5, r6, r0)
            goto L_0x0021
        L_0x006b:
            r0 = move-exception
            java.lang.String r1 = "DownloadThread"
            java.lang.String r1 = "exception while closing file: "
            android.util.Log.w(r5, r7, r0)
            goto L_0x0021
        L_0x0074:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
        L_0x0078:
            java.lang.String r2 = "DownloadThread"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0140 }
            r3.<init>()     // Catch:{ all -> 0x0140 }
            java.lang.String r4 = "file "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0140 }
            java.lang.String r4 = r10.jM     // Catch:{ all -> 0x0140 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0140 }
            java.lang.String r4 = " sync failed: "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0140 }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ all -> 0x0140 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0140 }
            android.util.Log.w(r2, r0)     // Catch:{ all -> 0x0140 }
            if (r1 == 0) goto L_0x0021
            r1.close()     // Catch:{ IOException -> 0x00a2, RuntimeException -> 0x00ac }
            goto L_0x0021
        L_0x00a2:
            r0 = move-exception
            java.lang.String r1 = "DownloadThread"
            java.lang.String r1 = "IOException while closing synced file: "
            android.util.Log.w(r5, r6, r0)
            goto L_0x0021
        L_0x00ac:
            r0 = move-exception
            java.lang.String r1 = "DownloadThread"
            java.lang.String r1 = "exception while closing file: "
            android.util.Log.w(r5, r7, r0)
            goto L_0x0021
        L_0x00b6:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
        L_0x00ba:
            java.lang.String r2 = "DownloadThread"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0140 }
            r3.<init>()     // Catch:{ all -> 0x0140 }
            java.lang.String r4 = "IOException trying to sync "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0140 }
            java.lang.String r4 = r10.jM     // Catch:{ all -> 0x0140 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0140 }
            java.lang.String r4 = ": "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0140 }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ all -> 0x0140 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0140 }
            android.util.Log.w(r2, r0)     // Catch:{ all -> 0x0140 }
            if (r1 == 0) goto L_0x0021
            r1.close()     // Catch:{ IOException -> 0x00e5, RuntimeException -> 0x00ef }
            goto L_0x0021
        L_0x00e5:
            r0 = move-exception
            java.lang.String r1 = "DownloadThread"
            java.lang.String r1 = "IOException while closing synced file: "
            android.util.Log.w(r5, r6, r0)
            goto L_0x0021
        L_0x00ef:
            r0 = move-exception
            java.lang.String r1 = "DownloadThread"
            java.lang.String r1 = "exception while closing file: "
            android.util.Log.w(r5, r7, r0)
            goto L_0x0021
        L_0x00f9:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
        L_0x00fd:
            java.lang.String r2 = "DownloadThread"
            java.lang.String r3 = "exception while syncing file: "
            android.util.Log.w(r2, r3, r0)     // Catch:{ all -> 0x0140 }
            if (r1 == 0) goto L_0x0021
            r1.close()     // Catch:{ IOException -> 0x010b, RuntimeException -> 0x0115 }
            goto L_0x0021
        L_0x010b:
            r0 = move-exception
            java.lang.String r1 = "DownloadThread"
            java.lang.String r1 = "IOException while closing synced file: "
            android.util.Log.w(r5, r6, r0)
            goto L_0x0021
        L_0x0115:
            r0 = move-exception
            java.lang.String r1 = "DownloadThread"
            java.lang.String r1 = "exception while closing file: "
            android.util.Log.w(r5, r7, r0)
            goto L_0x0021
        L_0x011f:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
        L_0x0123:
            if (r1 == 0) goto L_0x0128
            r1.close()     // Catch:{ IOException -> 0x0129, RuntimeException -> 0x0132 }
        L_0x0128:
            throw r0
        L_0x0129:
            r1 = move-exception
            java.lang.String r2 = "DownloadThread"
            java.lang.String r2 = "IOException while closing synced file: "
            android.util.Log.w(r5, r6, r1)
            goto L_0x0128
        L_0x0132:
            r1 = move-exception
            java.lang.String r2 = "DownloadThread"
            java.lang.String r2 = "exception while closing file: "
            android.util.Log.w(r5, r7, r1)
            goto L_0x0128
        L_0x013b:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x0123
        L_0x0140:
            r0 = move-exception
            goto L_0x0123
        L_0x0142:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x00fd
        L_0x0147:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x00ba
        L_0x014d:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x0078
        L_0x0153:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: com.dianxinos.appupdate.DownloadThread.c(com.dianxinos.appupdate.ae):void");
    }

    private boolean d(ae aeVar) {
        return false;
    }

    private void e(ae aeVar) {
        try {
            if (aeVar.jN != null) {
                aeVar.jN.close();
                aeVar.jN = null;
            }
        } catch (IOException e) {
            if (DEBUG) {
                Log.v("DownloadThread", "exception when closing the file after download : " + e);
            }
        }
    }

    private void f(ae aeVar) {
        synchronized (this.kM) {
            if (this.kM.kE == 1) {
                throw new StopRequest(193, "download paused by owner");
            }
        }
        if (this.kM.hA == 490) {
            throw new StopRequest(490, "download canceled");
        }
    }

    private void a(ae aeVar, ad adVar, boolean z) {
        long currentTimeMillis = this.kL.currentTimeMillis();
        this.kN = (long) adVar.jy;
        if ((adVar.jy - adVar.jF > 4096 && currentTimeMillis - adVar.jG > 1500) || z) {
            adVar.jF = adVar.jy;
            adVar.jG = currentTimeMillis;
            e((long) adVar.jy);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException} */
    private void a(ae aeVar, byte[] bArr, int i) {
        try {
            if (aeVar.jN == null) {
                aeVar.jN = new FileOutputStream(aeVar.jM, true);
            }
            aeVar.jN.write(bArr, 0, i);
            if (this.kM.cy == 0 && !d(aeVar)) {
                e(aeVar);
            }
        } catch (IOException e) {
            if (!DownloadHelpers.bO()) {
                throw new StopRequest(499, "external media not mounted while writing destination file");
            } else if (DownloadHelpers.d(DownloadHelpers.s(aeVar.jM)) < ((long) i)) {
                throw new StopRequest(498, "insufficient space while writing destination file", e);
            } else {
                throw new StopRequest(492, "while writing destination file: " + e.toString(), e);
            }
        }
    }

    private void a(ae aeVar, ad adVar) {
        boolean z;
        boolean z2 = (!TextUtils.isEmpty(adVar.jC) && adVar.jz != ((long) Integer.parseInt(adVar.jC))) || (this.kM.kG > 0 && this.kM.kG != ((long) adVar.jy));
        if (DEBUG) {
            StringBuilder append = new StringBuilder().append("handle end of stream, excepted size:").append(adVar.jC).append(", byte transferred:").append(adVar.jz).append(", total bytes:").append(this.kM.kG).append(", bytesSoFar:").append(adVar.jy).append(", matches:");
            if (!z2) {
                z = true;
            } else {
                z = false;
            }
            Log.w("DownloadThread", append.append(z).toString());
        }
        if (!z2) {
            return;
        }
        if (a(adVar)) {
            throw new StopRequest(489, "mismatched content length");
        }
        a(aeVar, "closed socket before end of file", (Exception) null);
    }

    private boolean a(ad adVar) {
        return adVar.jy > 0 && !this.kM.kC && adVar.jA == null;
    }

    private int b(ae aeVar, ad adVar, byte[] bArr, InputStream inputStream) {
        try {
            return inputStream.read(bArr);
        } catch (IOException e) {
            cQ();
            if (a(adVar)) {
                throw new StopRequest(489, "while reading response: " + e.toString() + ", can't resume interrupted download with no ETag", e);
            }
            a(aeVar, "while reading response: " + e.toString(), e);
            return -1;
        }
    }

    private InputStream a(ae aeVar, HttpResponse httpResponse) {
        try {
            return httpResponse.getEntity().getContent();
        } catch (IOException e) {
            cQ();
            a(aeVar, "while getting entity: " + e.toString(), e);
            return null;
        }
    }

    private void cQ() {
        if (DEBUG) {
            Log.i("DownloadThread", "Net " + (DownloadHelpers.a(this.kL) ? "Up" : "Down"));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException} */
    private void a(ae aeVar, ad adVar, HttpResponse httpResponse) {
        b(aeVar, adVar, httpResponse);
        try {
            switch (this.kM.cy) {
                case 0:
                    aeVar.jN = new FileOutputStream(aeVar.jM, true);
                    break;
                case 5:
                    aeVar.jN = this.mContext.openFileOutput(aeVar.ci, 32769);
                    break;
            }
            if (DEBUG) {
                Log.v("DownloadThread", "writing " + this.kM.kB + " to " + aeVar.jM);
            }
            a(aeVar);
        } catch (FileNotFoundException e) {
            throw new StopRequest(492, "while opening destination file: " + e.toString(), e);
        }
    }

    private void b(ae aeVar, ad adVar, HttpResponse httpResponse) {
        Header firstHeader;
        Header firstHeader2 = httpResponse.getFirstHeader("Content-Disposition");
        if (firstHeader2 != null) {
            adVar.jD = firstHeader2.getValue();
        }
        Header firstHeader3 = httpResponse.getFirstHeader("Content-Location");
        if (firstHeader3 != null) {
            adVar.jE = firstHeader3.getValue();
        }
        if (aeVar.mMimeType == null && (firstHeader = httpResponse.getFirstHeader("Content-Type")) != null) {
            aeVar.mMimeType = F(firstHeader.getValue());
        }
        Header firstHeader4 = httpResponse.getFirstHeader("ETag");
        if (firstHeader4 != null) {
            adVar.jA = firstHeader4.getValue();
        }
        String str = null;
        Header firstHeader5 = httpResponse.getFirstHeader("Transfer-Encoding");
        if (firstHeader5 != null) {
            str = firstHeader5.getValue();
        }
        if (str == null) {
            Header firstHeader6 = httpResponse.getFirstHeader("Content-Length");
            if (firstHeader6 != null) {
                adVar.jC = firstHeader6.getValue();
                if (this.kM.kG <= 0) {
                    this.kM.kG = Long.parseLong(adVar.jC);
                }
            }
            if (DEBUG) {
                Log.d("DownloadThread", "Content-length:" + firstHeader6.getValue());
            }
        } else if (DEBUG) {
            Log.v("DownloadThread", "ignoring content-length because of xfer-encoding");
        }
        if (DEBUG) {
            Log.v("DownloadThread", "Content-Disposition: " + adVar.jD);
            Log.v("DownloadThread", "Content-Length: " + adVar.jC);
            Log.v("DownloadThread", "Content-Location: " + adVar.jE);
            Log.v("DownloadThread", "Content-Type: " + aeVar.mMimeType);
            Log.v("DownloadThread", "ETag: " + adVar.jA);
            Log.v("DownloadThread", "Transfer-Encoding: " + str);
        }
        boolean z = adVar.jC == null && (str == null || !str.equalsIgnoreCase("chunked"));
        if (!this.kM.kC && z) {
            throw new StopRequest(495, "can't know size of download, giving up");
        }
    }

    private void c(ae aeVar, ad adVar, HttpResponse httpResponse) {
        int statusCode = httpResponse.getStatusLine().getStatusCode();
        if (statusCode == 503 && this.kM.kF < 5) {
            b(aeVar, httpResponse);
        }
        if (statusCode == 301 || statusCode == 302 || statusCode == 303 || statusCode == 307) {
            a(aeVar, httpResponse, statusCode);
        }
        if (statusCode == (adVar.jB ? 206 : 200)) {
            return;
        }
        if (statusCode == 200 || statusCode == 206 || statusCode == 416) {
            if (DEBUG) {
                Log.w("DownloadThread", "Status code:" + statusCode + ", Server does not allow to resume the download, start a new file");
            }
            boolean delete = new File(aeVar.jM).delete();
            adVar.jy = 0;
            adVar.jz = 0;
            if (DEBUG) {
                Log.d("DownloadThread", "Obsoleted file deleted, start a new file, removed:" + delete);
                return;
            }
            return;
        }
        a(aeVar, adVar, statusCode);
    }

    private void a(ae aeVar, ad adVar, int i) {
        int i2;
        if (p.isStatusError(i)) {
            i2 = i;
        } else if (i >= 300 && i < 400) {
            i2 = 493;
        } else if (!adVar.jB || i != 200) {
            i2 = 494;
        } else {
            i2 = 489;
        }
        throw new StopRequest(i2, "http error " + i);
    }

    private void a(ae aeVar, HttpResponse httpResponse, int i) {
        if (DEBUG) {
            Log.v("DownloadThread", "got HTTP redirect " + i);
        }
        if (aeVar.jQ >= 5) {
            throw new StopRequest(497, "too many redirects");
        }
        Header firstHeader = httpResponse.getFirstHeader("Location");
        if (firstHeader != null) {
            if (DEBUG) {
                Log.v("DownloadThread", "Location :" + firstHeader.getValue());
            }
            try {
                String uri = new URI(this.kM.kB).resolve(new URI(firstHeader.getValue())).toString();
                aeVar.jQ++;
                aeVar.jT = uri;
                if (i == 301 || i == 303) {
                    aeVar.jR = uri;
                }
                throw new RetryDownload();
            } catch (URISyntaxException e) {
                if (DEBUG) {
                    Log.d("DownloadThread", "Couldn't resolve redirect URI " + firstHeader.getValue() + " for " + this.kM.kB);
                }
                throw new StopRequest(495, "Couldn't resolve redirect URI");
            }
        }
    }

    private void b(ae aeVar, HttpResponse httpResponse) {
        if (DEBUG) {
            Log.v("DownloadThread", "got HTTP response code 503");
        }
        aeVar.jO = true;
        Header firstHeader = httpResponse.getFirstHeader("Retry-After");
        if (firstHeader != null) {
            try {
                if (DEBUG) {
                    Log.v("DownloadThread", "Retry-After :" + firstHeader.getValue());
                }
                aeVar.jP = Integer.parseInt(firstHeader.getValue());
                if (aeVar.jP < 0) {
                    aeVar.jP = 0;
                } else {
                    if (aeVar.jP < 30) {
                        aeVar.jP = 30;
                    } else if (aeVar.jP > 86400) {
                        aeVar.jP = 86400;
                    }
                    aeVar.jP += DownloadHelpers.im.nextInt(31);
                    aeVar.jP *= 1000;
                }
            } catch (NumberFormatException e) {
            }
        }
        throw new StopRequest(194, "got 503 Service Unavailable, will retry later");
    }

    private HttpResponse b(ae aeVar, HttpClient httpClient, HttpGet httpGet) {
        try {
            return httpClient.execute(httpGet);
        } catch (IllegalArgumentException e) {
            throw new StopRequest(495, "while trying to execute request: " + e.toString(), e);
        } catch (IOException e2) {
            cQ();
            a(aeVar, "while trying to execute request: " + e2.toString(), e2);
            return null;
        }
    }

    private int g(ae aeVar) {
        if (!DownloadHelpers.a(this.kL)) {
            return 195;
        }
        if (this.kM.kF < 5) {
            aeVar.jO = true;
            return 194;
        }
        Log.w("DownloadThread", "reached max retries for " + this.kM.kB);
        return 495;
    }

    private void a(ae aeVar, String str, Exception exc) {
        int g = g(aeVar);
        if (g == 194) {
            throw new RetryDownload();
        } else if (exc == null) {
            throw new StopRequest(g, str);
        } else {
            throw new StopRequest(g, str, exc);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException} */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00d0  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x00fe  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(com.dianxinos.appupdate.ae r13, com.dianxinos.appupdate.ad r14, org.apache.http.client.methods.HttpGet r15) {
        /*
            r12 = this;
            r10 = 492(0x1ec, float:6.9E-43)
            r9 = 1
            r7 = 0
            java.lang.String r11 = "Download already finished"
            java.lang.String r6 = "DownloadThread"
            java.lang.String r0 = r13.jM
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 != 0) goto L_0x0046
            java.lang.String r0 = r13.jM
            boolean r0 = com.dianxinos.appupdate.DownloadHelpers.t(r0)
            if (r0 != 0) goto L_0x0021
            com.dianxinos.appupdate.DownloadThread$StopRequest r0 = new com.dianxinos.appupdate.DownloadThread$StopRequest
            java.lang.String r1 = "found invalid internal destination filename"
            r0.<init>(r10, r1)
            throw r0
        L_0x0021:
            r14.jz = r7
            java.io.File r0 = new java.io.File
            java.lang.String r1 = r13.jM
            r0.<init>(r1)
            boolean r1 = r0.exists()
            if (r1 == 0) goto L_0x0147
            long r1 = r0.length()
            int r3 = (r1 > r7 ? 1 : (r1 == r7 ? 0 : -1))
            if (r3 != 0) goto L_0x005a
            r0.delete()
            boolean r0 = com.dianxinos.appupdate.DownloadThread.DEBUG
            if (r0 == 0) goto L_0x0046
            java.lang.String r0 = "DownloadThread"
            java.lang.String r0 = "Obsoleted file deleted"
            android.util.Log.w(r6, r0)
        L_0x0046:
            java.io.FileOutputStream r0 = r13.jN
            if (r0 == 0) goto L_0x0059
            com.dianxinos.appupdate.af r0 = r12.kM
            int r0 = r0.cy
            if (r0 != 0) goto L_0x0059
            boolean r0 = r12.d(r13)
            if (r0 != 0) goto L_0x0059
            r12.e(r13)
        L_0x0059:
            return
        L_0x005a:
            com.dianxinos.appupdate.af r3 = r12.kM
            java.lang.String r3 = r3.kH
            if (r3 != 0) goto L_0x007e
            com.dianxinos.appupdate.af r3 = r12.kM
            boolean r3 = r3.kC
            if (r3 != 0) goto L_0x007e
            r0.delete()
            boolean r0 = com.dianxinos.appupdate.DownloadThread.DEBUG
            if (r0 == 0) goto L_0x0074
            java.lang.String r0 = "DownloadThread"
            java.lang.String r0 = "Download cannot be resumed"
            android.util.Log.w(r6, r0)
        L_0x0074:
            com.dianxinos.appupdate.DownloadThread$StopRequest r0 = new com.dianxinos.appupdate.DownloadThread$StopRequest
            r1 = 489(0x1e9, float:6.85E-43)
            java.lang.String r2 = "Trying to resume a download that can't be resumed"
            r0.<init>(r1, r2)
            throw r0
        L_0x007e:
            com.dianxinos.appupdate.af r3 = r12.kM
            java.lang.String r3 = r3.L
            boolean r3 = android.text.TextUtils.isEmpty(r3)
            if (r3 == 0) goto L_0x008f
            boolean r3 = r0.delete()
            if (r3 == 0) goto L_0x008f
            r1 = r7
        L_0x008f:
            java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x00da }
            java.lang.String r4 = r13.jM     // Catch:{ FileNotFoundException -> 0x00da }
            r5 = 1
            r3.<init>(r4, r5)     // Catch:{ FileNotFoundException -> 0x00da }
            r13.jN = r3     // Catch:{ FileNotFoundException -> 0x00da }
            r3 = 0
            com.dianxinos.appupdate.af r4 = r12.kM
            long r4 = r4.kG
            int r4 = (r1 > r4 ? 1 : (r1 == r4 ? 0 : -1))
            if (r4 != 0) goto L_0x00fc
            com.dianxinos.appupdate.af r4 = r12.kM
            long r4 = r4.kG
            int r4 = (r4 > r7 ? 1 : (r4 == r7 ? 0 : -1))
            if (r4 <= 0) goto L_0x00fc
            com.dianxinos.appupdate.af r4 = r12.kM
            java.lang.String r4 = r4.L
            boolean r4 = android.text.TextUtils.isEmpty(r4)
            if (r4 != 0) goto L_0x00fc
            java.lang.String r0 = com.dianxinos.appupdate.m.c(r0)     // Catch:{ FileNotFoundException -> 0x00f8 }
            com.dianxinos.appupdate.af r4 = r12.kM     // Catch:{ FileNotFoundException -> 0x00f8 }
            java.lang.String r4 = r4.L     // Catch:{ FileNotFoundException -> 0x00f8 }
            boolean r0 = r4.equals(r0)     // Catch:{ FileNotFoundException -> 0x00f8 }
            if (r0 == 0) goto L_0x00fc
            boolean r0 = com.dianxinos.appupdate.DownloadThread.DEBUG     // Catch:{ FileNotFoundException -> 0x017b }
            if (r0 == 0) goto L_0x00cd
            java.lang.String r0 = "DownloadThread"
            java.lang.String r3 = "Download already finished"
            android.util.Log.i(r0, r3)     // Catch:{ FileNotFoundException -> 0x017b }
        L_0x00cd:
            r0 = r9
        L_0x00ce:
            if (r0 == 0) goto L_0x00fe
            com.dianxinos.appupdate.DownloadThread$StopRequest r0 = new com.dianxinos.appupdate.DownloadThread$StopRequest
            r1 = 200(0xc8, float:2.8E-43)
            java.lang.String r2 = "Download already finished"
            r0.<init>(r1, r11)
            throw r0
        L_0x00da:
            r0 = move-exception
            com.dianxinos.appupdate.DownloadThread$StopRequest r1 = new com.dianxinos.appupdate.DownloadThread$StopRequest
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "while opening destination for resuming: "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = r0.toString()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r1.<init>(r10, r2, r0)
            throw r1
        L_0x00f8:
            r0 = move-exception
        L_0x00f9:
            r0.printStackTrace()
        L_0x00fc:
            r0 = r3
            goto L_0x00ce
        L_0x00fe:
            int r0 = (int) r1
            r14.jy = r0
            r12.kN = r1
            boolean r0 = com.dianxinos.appupdate.DownloadThread.DEBUG
            if (r0 == 0) goto L_0x011f
            java.lang.String r0 = "DownloadThread"
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r3 = "get file length:"
            java.lang.StringBuilder r0 = r0.append(r3)
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            android.util.Log.d(r6, r0)
        L_0x011f:
            com.dianxinos.appupdate.af r0 = r12.kM
            java.lang.String r0 = r0.kH
            r14.jA = r0
            r14.jB = r9
            boolean r0 = com.dianxinos.appupdate.DownloadThread.DEBUG
            if (r0 == 0) goto L_0x0046
            java.lang.String r0 = "DownloadThread"
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "Download resumed from "
            java.lang.StringBuilder r0 = r0.append(r1)
            int r1 = r14.jy
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            android.util.Log.d(r6, r0)
            goto L_0x0046
        L_0x0147:
            java.io.File r0 = r0.getParentFile()
            if (r0 == 0) goto L_0x0046
            boolean r1 = r0.exists()
            if (r1 == 0) goto L_0x0159
            boolean r1 = r0.isDirectory()
            if (r1 != 0) goto L_0x0046
        L_0x0159:
            boolean r0 = r0.mkdirs()
            boolean r1 = com.dianxinos.appupdate.DownloadThread.DEBUG
            if (r1 == 0) goto L_0x0046
            java.lang.String r1 = "DownloadThread"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Dowload parent file created, success:"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r0 = r0.toString()
            android.util.Log.d(r6, r0)
            goto L_0x0046
        L_0x017b:
            r0 = move-exception
            r3 = r9
            goto L_0x00f9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.dianxinos.appupdate.DownloadThread.a(com.dianxinos.appupdate.ae, com.dianxinos.appupdate.ad, org.apache.http.client.methods.HttpGet):void");
    }

    private void a(ad adVar, HttpGet httpGet) {
        if (adVar.jB) {
            if (adVar.jA != null) {
                httpGet.addHeader("If-Match", adVar.jA);
            }
            httpGet.addHeader("Range", "bytes=" + adVar.jy + "-");
        }
    }

    private int ad(int i) {
        if (DEBUG) {
            Log.d("DownloadThread", "Converting download status, status:" + i);
        }
        switch (i) {
            case 193:
            case 490:
                return 1;
            case 194:
                return 2;
            case 195:
                return 3;
            case 197:
                return 9;
            case 200:
                return -1;
            case 492:
                return 4;
            case 493:
            case 494:
            case 495:
            case 497:
                return 7;
            case 498:
                return 6;
            case 499:
                return 5;
            case 500:
                return 8;
            default:
                return 0;
        }
    }

    /* access modifiers changed from: private */
    public static String F(String str) {
        try {
            String lowerCase = str.trim().toLowerCase(Locale.ENGLISH);
            int indexOf = lowerCase.indexOf(59);
            if (indexOf != -1) {
                return lowerCase.substring(0, indexOf);
            }
            return lowerCase;
        } catch (NullPointerException e) {
            return null;
        }
    }

    private void a(String str, long j, long j2) {
        synchronized (this.kP) {
            for (q a : this.kP) {
                a.a(this.mContext, str, j, j2);
            }
        }
    }

    public void cR() {
        if (DEBUG) {
            Log.d("DownloadThread", "Force finishing download");
        }
        synchronized (this.kM) {
            this.kM.kE = 1;
        }
        interrupt();
    }

    public void c(q qVar) {
        if (DEBUG) {
            Log.d("DownloadThread", "Adding progress listener");
        }
        synchronized (this.kP) {
            if (qVar != null) {
                if (!this.kP.contains(qVar)) {
                    this.kP.add(qVar);
                    if (DEBUG) {
                        Log.d("DownloadThread", "Added new progress listener, current bytes:" + this.kN);
                    }
                    e(this.kN);
                }
            }
        }
    }

    public void d(q qVar) {
        synchronized (this.kP) {
            this.kP.remove(qVar);
        }
    }

    public void e(long j) {
        if (Thread.State.TERMINATED.equals(getState())) {
            if (DEBUG) {
                Log.d("DownloadThread", "Download thread stopped, publish progress ignored");
            }
        } else if (j >= this.kN) {
            if (DEBUG) {
                Log.d("DownloadThread", "Publishing progress, bytes:" + j);
            }
            synchronized (this.kP) {
                for (q a : this.kP) {
                    a.a(this.mContext, j, this.kM.kG);
                }
            }
            this.kN = j;
        }
    }

    public void a(String str, boolean z, int i, String str2, int i2) {
        synchronized (this.kP) {
            for (q a : new ArrayList(this.kP)) {
                a.a(this.mContext, str, z, i, str2, i2);
            }
        }
    }
}
