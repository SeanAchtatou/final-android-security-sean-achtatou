package com.dianxinos.appupdate;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class PowerOnReceiver extends BroadcastReceiver {
    private static final boolean DEBUG = x.DEBUG;

    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (DEBUG) {
            Log.v("PowerOnReceiver", "Receive broadcast, action:" + action);
        }
        if ("android.intent.action.BOOT_COMPLETED".equals(action)) {
            k.d(context).ah();
            if (DEBUG) {
                Log.d("PowerOnReceiver", "Check update scheduled");
            }
        }
    }
}
