package com.dianxinos.appupdate;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Proxy;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;

/* compiled from: DownloadService */
class o implements r {
    final /* synthetic */ DownloadService eN;

    o(DownloadService downloadService) {
        this.eN = downloadService;
    }

    public long currentTimeMillis() {
        return System.currentTimeMillis();
    }

    public Integer aE() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.eN.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo == null) {
            return null;
        }
        return Integer.valueOf(activeNetworkInfo.getType());
    }

    public boolean isNetworkRoaming() {
        return ((TelephonyManager) this.eN.getSystemService("phone")).isNetworkRoaming();
    }

    public Long aF() {
        return null;
    }

    public Long aG() {
        return null;
    }

    public String e() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.eN.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo == null || activeNetworkInfo.getType() != 0) {
            return null;
        }
        return Proxy.getDefaultHost();
    }

    public int f() {
        return Proxy.getDefaultPort();
    }

    public boolean aH() {
        try {
            int i = Settings.Secure.getInt(this.eN.getApplicationContext().getContentResolver(), "data_roaming");
            if (DownloadService.DEBUG) {
                Log.d("DownloadService", "Allow roaming:" + i);
            }
            return i != 1;
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
            return true;
        }
    }
}
