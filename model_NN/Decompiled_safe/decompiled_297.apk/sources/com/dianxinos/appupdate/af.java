package com.dianxinos.appupdate;

import android.content.Context;

/* compiled from: DownloadInfo */
public class af {
    public String L;
    public String bg;
    public int cy;
    public String description;
    public int hA;
    public String kB;
    public boolean kC;
    public String kD;
    public int kE;
    public int kF;
    public long kG;
    public String kH;
    public String kI;
    public int kJ;
    public boolean kK;
    private r kL;
    private Context mContext;
    public String mMimeType;
    public int priority;

    public af(Context context, r rVar) {
        this.mContext = context;
        this.kL = rVar;
    }

    public int cO() {
        Integer aE = this.kL.aE();
        if (aE == null) {
            return 2;
        }
        if (aE.intValue() == 1) {
            return 1;
        }
        if (!this.kL.isNetworkRoaming() || !this.kL.aH()) {
            return ab(aE.intValue());
        }
        return 5;
    }

    public String aa(int i) {
        switch (i) {
            case 2:
                return "no network connection available";
            case 3:
                return "download size exceeds limit for mobile network";
            case 4:
                return "download size exceeds recommended limit for mobile network";
            case 5:
                return "download cannot use the current network connection because it is roaming";
            case 6:
                return "download was requested to not use the current network type";
            default:
                return "unknown error with network connectivity";
        }
    }

    private int ab(int i) {
        return ac(i);
    }

    private int ac(int i) {
        Long aG;
        if (this.kG <= 0) {
            return 1;
        }
        if (i == 1) {
            return 1;
        }
        Long aF = this.kL.aF();
        if (aF == null || this.kG <= aF.longValue()) {
            return (this.kJ != 0 || (aG = this.kL.aG()) == null || this.kG <= aG.longValue()) ? 1 : 4;
        }
        return 3;
    }
}
