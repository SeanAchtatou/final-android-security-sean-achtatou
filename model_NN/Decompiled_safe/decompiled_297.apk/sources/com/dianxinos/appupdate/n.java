package com.dianxinos.appupdate;

import android.util.Log;
import java.io.File;
import java.util.Map;
import java.util.concurrent.Callable;

/* compiled from: UpdateManager */
class n extends Thread {
    final /* synthetic */ k b;
    private j dD;
    private Map dE;
    private boolean dF = false;
    private Callable dG;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public n(k kVar, j jVar, Map map, Callable callable) {
        super("CheckUpdateThread-" + k.aj());
        this.b = kVar;
        this.dD = jVar;
        this.dE = map;
        this.dG = callable;
    }

    public void run() {
        if (this.dG != null) {
            try {
                this.dG.call();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        d a = this.b.cj.a(this.b.mContext.getPackageName(), this.b.cq, this.b.cr, this.dE);
        if (a == null) {
            if (k.DEBUG) {
                Log.d("UpdateManager", "No update info returned");
            }
            if (!this.dF && this.dD != null) {
                this.dD.K();
            }
        } else if (!a.available || a.I == null || a.versionCode <= this.b.cq) {
            ah.b(this.b.mContext, "pref-last-check", System.currentTimeMillis());
            ah.b(this.b.mContext, "pref-update-info", (String) null);
            if (k.DEBUG) {
                Log.d("UpdateManager", "Download url pref cleared dut to no available update");
            }
            if (!this.dF && this.dD != null) {
                this.dD.L();
            }
        } else {
            if (k.DEBUG) {
                Log.d("UpdateManager", "Update is available");
            }
            ah.b(this.b.mContext, "pref-last-check", System.currentTimeMillis());
            if (ac.WEB_URL.matcher(a.I).matches()) {
                if (!a.I.equals(ah.f(this.b.mContext, "pref-last-down-url"))) {
                    File file = new File(DownloadHelpers.f(this.b.mContext, 0) + this.b.ci);
                    if (file.delete()) {
                        Log.i("UpdateManager", "Delete obsoleted file in sdcard:" + file.getAbsolutePath());
                    }
                    File file2 = new File(DownloadHelpers.f(this.b.mContext, 5) + this.b.ci);
                    if (file2.delete()) {
                        Log.i("UpdateManager", "Delete obsoleted file in data:" + file2.getAbsolutePath());
                    }
                    ah.b(this.b.mContext, "pref-retry-count", 0);
                }
                String unused = this.b.co = a.I;
                z unused2 = this.b.cw = new z();
                z k = this.b.cw;
                k.versionCode = a.versionCode;
                k.versionName = a.versionName;
                k.description = a.description;
                k.priority = a.priority;
                k.J = a.J;
                k.timestamp = System.currentTimeMillis();
                k.iu = a.K;
                k.L = a.L;
                this.b.a(k);
                if (!this.dF && this.dD != null) {
                    this.dD.a(a.versionCode, a.versionName, a.description, a.priority, a.J);
                }
            } else {
                if (k.DEBUG) {
                    Log.w("UpdateManager", "Invalid download URL:" + a.I);
                }
                ah.b(this.b.mContext, "pref-update-info", (String) null);
                if (k.DEBUG) {
                    Log.d("UpdateManager", "Download url pref cleared due to invalid download url");
                }
                if (!this.dF && this.dD != null) {
                    this.dD.L();
                }
            }
        }
        n unused3 = this.b.ck = (n) null;
    }
}
