package com.dianxinos.appupdate;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Proxy;

/* compiled from: UpdateManager */
class b implements t {
    final /* synthetic */ k b;

    b(k kVar) {
        this.b = kVar;
    }

    public String e() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.b.mContext.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo == null || activeNetworkInfo.getType() != 0) {
            return null;
        }
        return Proxy.getDefaultHost();
    }

    public int f() {
        return Proxy.getDefaultPort();
    }
}
