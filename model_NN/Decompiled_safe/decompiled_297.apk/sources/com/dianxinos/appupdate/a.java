package com.dianxinos.appupdate;

import android.text.TextUtils;
import android.util.Log;
import java.io.File;

/* compiled from: UpdateManager */
class a implements Runnable {
    final /* synthetic */ u a;
    final /* synthetic */ k b;

    a(k kVar, u uVar) {
        this.b = kVar;
        this.a = uVar;
    }

    public void run() {
        if (k.DEBUG) {
            Log.d("UpdateManager", "Start to check archive");
        }
        String str = this.b.ai() + this.b.ci;
        if (!TextUtils.isEmpty(str)) {
            File file = new File(str);
            if (!file.exists() || !file.isFile()) {
                if (k.DEBUG) {
                    Log.w("UpdateManager", "File not found:" + str);
                }
                if (this.a != null) {
                    this.a.bi();
                }
            } else {
                int e = y.e(this.b.mContext, file.getAbsolutePath());
                if (e == 0) {
                    if (k.DEBUG) {
                        Log.d("UpdateManager", "About to install");
                    }
                    if (this.a != null) {
                        this.a.bl();
                    }
                    this.b.b(file);
                } else if (e == 3) {
                    if (k.DEBUG) {
                        Log.d("UpdateManager", "Package name mismatches");
                    }
                    if (this.a != null) {
                        this.a.bj();
                    }
                } else if (e == 2) {
                    if (k.DEBUG) {
                        Log.d("UpdateManager", "Version too old");
                    }
                    if (this.a != null) {
                        this.a.bk();
                    }
                } else {
                    if (k.DEBUG) {
                        Log.d("UpdateManager", "Invalid archive");
                    }
                    if (this.a != null) {
                        this.a.bj();
                    }
                }
            }
        } else if (this.a != null) {
            this.a.bi();
        }
        synchronized (this.b.cv) {
            Thread unused = this.b.cl = (Thread) null;
        }
    }
}
