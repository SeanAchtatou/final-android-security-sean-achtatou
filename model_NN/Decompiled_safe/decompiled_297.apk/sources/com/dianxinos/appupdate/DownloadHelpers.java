package com.dianxinos.appupdate;

import android.content.Context;
import android.os.Environment;
import android.os.StatFs;
import android.os.SystemClock;
import android.util.Log;
import java.io.File;
import java.util.Random;
import java.util.regex.Pattern;

public class DownloadHelpers {
    private static final boolean DEBUG = x.DEBUG;
    public static Random im = new Random(SystemClock.uptimeMillis());
    private static final Pattern in = Pattern.compile("attachment;\\s*filename\\s*=\\s*\"([^\"]*)\"");

    private DownloadHelpers() {
    }

    public static File s(String str) {
        File downloadCacheDirectory = Environment.getDownloadCacheDirectory();
        if (!str.startsWith(downloadCacheDirectory.getPath())) {
            downloadCacheDirectory = Environment.getExternalStorageDirectory();
            if (!str.startsWith(downloadCacheDirectory.getPath())) {
                throw new IllegalArgumentException("Cannot determine filesystem root for " + str);
            }
        }
        return downloadCacheDirectory;
    }

    public static boolean bO() {
        if (Environment.getExternalStorageState().equals("mounted")) {
            return true;
        }
        Log.d("DownloadHelpers", "no external storage");
        return false;
    }

    public static long d(File file) {
        StatFs statFs = new StatFs(file.getPath());
        return ((long) statFs.getBlockSize()) * (((long) statFs.getAvailableBlocks()) - 4);
    }

    public static boolean a(r rVar) {
        return rVar.aE() != null;
    }

    public static boolean t(String str) {
        String replaceFirst = str.replaceFirst("/+", "/");
        return replaceFirst.startsWith(Environment.getDownloadCacheDirectory().toString()) || replaceFirst.startsWith(Environment.getExternalStorageDirectory().toString()) || replaceFirst.startsWith(Environment.getDataDirectory().toString());
    }

    public static String f(Context context, int i) {
        switch (i) {
            case 0:
                return Environment.getExternalStorageDirectory().getAbsolutePath() + "/app-update" + "/";
            case 5:
                return context.getFilesDir().getAbsolutePath() + "/";
            default:
                throw new IllegalStateException("Unknown download destination:" + i);
        }
    }
}
