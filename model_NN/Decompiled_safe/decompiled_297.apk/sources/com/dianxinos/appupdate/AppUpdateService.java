package com.dianxinos.appupdate;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class AppUpdateService extends Service {
    /* access modifiers changed from: private */
    public static final boolean DEBUG = x.DEBUG;
    private boolean jI = false;
    private j jJ = new v(this);

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.dianxinos.appupdate.ah.a(android.content.Context, java.lang.String, boolean):boolean
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.dianxinos.appupdate.ah.a(android.content.Context, java.lang.String, int):int
      com.dianxinos.appupdate.ah.a(android.content.Context, java.lang.String, long):long
      com.dianxinos.appupdate.ah.a(android.content.Context, java.lang.String, java.lang.String):java.lang.String
      com.dianxinos.appupdate.ah.a(android.content.Context, java.lang.String, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.dianxinos.appupdate.k.a(com.dianxinos.appupdate.i, boolean):void
     arg types: [?[OBJECT, ARRAY], int]
     candidates:
      com.dianxinos.appupdate.k.a(com.dianxinos.appupdate.k, com.dianxinos.appupdate.DownloadService):com.dianxinos.appupdate.DownloadService
      com.dianxinos.appupdate.k.a(com.dianxinos.appupdate.k, com.dianxinos.appupdate.n):com.dianxinos.appupdate.n
      com.dianxinos.appupdate.k.a(com.dianxinos.appupdate.k, com.dianxinos.appupdate.z):com.dianxinos.appupdate.z
      com.dianxinos.appupdate.k.a(com.dianxinos.appupdate.k, java.lang.String):java.lang.String
      com.dianxinos.appupdate.k.a(com.dianxinos.appupdate.k, java.lang.Thread):java.lang.Thread
      com.dianxinos.appupdate.k.a(com.dianxinos.appupdate.k, java.io.File):void
      com.dianxinos.appupdate.k.a(java.lang.String, com.dianxinos.appupdate.i):void
      com.dianxinos.appupdate.k.a(com.dianxinos.appupdate.j, com.dianxinos.appupdate.aa):boolean
      com.dianxinos.appupdate.k.a(com.dianxinos.appupdate.j, java.util.Map):boolean
      com.dianxinos.appupdate.k.a(com.dianxinos.appupdate.i, boolean):void */
    public void onStart(Intent intent, int i) {
        super.onStart(intent, i);
        if (intent != null) {
            String action = intent.getAction();
            if ("com.dianxinos.appupdate.intent.CHECK_UPDATE".equals(action)) {
                if (DEBUG) {
                    Log.d("AppUpdateService", "Auto checking update");
                }
                k d = k.d(getApplicationContext());
                d.a(this.jJ, d.ag());
                this.jI = true;
                d.ah();
            } else if ("com.dianxinos.appupdate.intent.DOWNLOAD_RETRY".equals(action) && ah.a(getApplicationContext(), "pref-need-redownload", false)) {
                k.d(getApplicationContext()).a((i) null, true);
            }
        }
    }
}
