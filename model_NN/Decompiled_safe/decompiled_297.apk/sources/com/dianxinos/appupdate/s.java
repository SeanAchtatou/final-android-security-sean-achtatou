package com.dianxinos.appupdate;

import java.util.logging.Logger;

/* compiled from: Utils */
public class s {
    private static final boolean DEBUG = x.DEBUG;
    private static final Logger R = Logger.getLogger(s.class.getName());

    /* JADX WARNING: Unknown top exception splitter block from list: {B:33:0x00ca=Splitter:B:33:0x00ca, B:13:0x004d=Splitter:B:13:0x004d} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.Object a(java.lang.String r6, java.lang.Class r7) {
        /*
            r5 = 0
            java.lang.Class r0 = java.lang.Class.forName(r6)     // Catch:{ ClassNotFoundException -> 0x0079 }
            boolean r1 = com.dianxinos.appupdate.s.DEBUG     // Catch:{ ClassNotFoundException -> 0x0079 }
            if (r1 == 0) goto L_0x0021
            java.util.logging.Logger r1 = com.dianxinos.appupdate.s.R     // Catch:{ ClassNotFoundException -> 0x0079 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ ClassNotFoundException -> 0x0079 }
            r2.<init>()     // Catch:{ ClassNotFoundException -> 0x0079 }
            java.lang.String r3 = "Retrieve target class:"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ ClassNotFoundException -> 0x0079 }
            java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ ClassNotFoundException -> 0x0079 }
            java.lang.String r2 = r2.toString()     // Catch:{ ClassNotFoundException -> 0x0079 }
            r1.info(r2)     // Catch:{ ClassNotFoundException -> 0x0079 }
        L_0x0021:
            java.lang.reflect.Type[] r1 = r0.getGenericInterfaces()     // Catch:{ ClassNotFoundException -> 0x0079 }
            r2 = 0
        L_0x0026:
            int r3 = r1.length     // Catch:{ ClassNotFoundException -> 0x0079 }
            if (r2 >= r3) goto L_0x00a4
            r3 = r1[r2]     // Catch:{ ClassNotFoundException -> 0x0079 }
            boolean r4 = r7.equals(r3)     // Catch:{ ClassNotFoundException -> 0x0079 }
            if (r4 == 0) goto L_0x00a1
            boolean r1 = com.dianxinos.appupdate.s.DEBUG     // Catch:{ ClassNotFoundException -> 0x0079 }
            if (r1 == 0) goto L_0x004d
            java.util.logging.Logger r1 = com.dianxinos.appupdate.s.R     // Catch:{ ClassNotFoundException -> 0x0079 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ ClassNotFoundException -> 0x0079 }
            r2.<init>()     // Catch:{ ClassNotFoundException -> 0x0079 }
            java.lang.String r4 = "Get target interface:"
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ ClassNotFoundException -> 0x0079 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ ClassNotFoundException -> 0x0079 }
            java.lang.String r2 = r2.toString()     // Catch:{ ClassNotFoundException -> 0x0079 }
            r1.info(r2)     // Catch:{ ClassNotFoundException -> 0x0079 }
        L_0x004d:
            java.lang.Object r0 = r0.newInstance()     // Catch:{ IllegalAccessException -> 0x006e, InstantiationException -> 0x0074 }
            boolean r1 = com.dianxinos.appupdate.s.DEBUG     // Catch:{ IllegalAccessException -> 0x006e, InstantiationException -> 0x0074 }
            if (r1 == 0) goto L_0x006d
            java.util.logging.Logger r1 = com.dianxinos.appupdate.s.R     // Catch:{ IllegalAccessException -> 0x006e, InstantiationException -> 0x0074 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ IllegalAccessException -> 0x006e, InstantiationException -> 0x0074 }
            r2.<init>()     // Catch:{ IllegalAccessException -> 0x006e, InstantiationException -> 0x0074 }
            java.lang.String r3 = "Target class instantiated:"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ IllegalAccessException -> 0x006e, InstantiationException -> 0x0074 }
            java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ IllegalAccessException -> 0x006e, InstantiationException -> 0x0074 }
            java.lang.String r2 = r2.toString()     // Catch:{ IllegalAccessException -> 0x006e, InstantiationException -> 0x0074 }
            r1.info(r2)     // Catch:{ IllegalAccessException -> 0x006e, InstantiationException -> 0x0074 }
        L_0x006d:
            return r0
        L_0x006e:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ ClassNotFoundException -> 0x0079 }
        L_0x0072:
            r0 = r5
            goto L_0x006d
        L_0x0074:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ ClassNotFoundException -> 0x0079 }
            goto L_0x0072
        L_0x0079:
            r0 = move-exception
            r0.printStackTrace()
        L_0x007d:
            java.util.logging.Logger r0 = com.dianxinos.appupdate.s.R
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Class "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r6)
            java.lang.String r2 = " not an instance of "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r7)
            java.lang.String r1 = r1.toString()
            r0.warning(r1)
            r0 = r5
            goto L_0x006d
        L_0x00a1:
            int r2 = r2 + 1
            goto L_0x0026
        L_0x00a4:
            java.lang.reflect.Type r1 = r0.getGenericSuperclass()     // Catch:{ ClassNotFoundException -> 0x0079 }
            boolean r2 = r7.equals(r1)     // Catch:{ ClassNotFoundException -> 0x0079 }
            if (r2 == 0) goto L_0x007d
            boolean r2 = com.dianxinos.appupdate.s.DEBUG     // Catch:{ ClassNotFoundException -> 0x0079 }
            if (r2 == 0) goto L_0x00ca
            java.util.logging.Logger r2 = com.dianxinos.appupdate.s.R     // Catch:{ ClassNotFoundException -> 0x0079 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ ClassNotFoundException -> 0x0079 }
            r3.<init>()     // Catch:{ ClassNotFoundException -> 0x0079 }
            java.lang.String r4 = "Get target class:"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ ClassNotFoundException -> 0x0079 }
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ ClassNotFoundException -> 0x0079 }
            java.lang.String r1 = r1.toString()     // Catch:{ ClassNotFoundException -> 0x0079 }
            r2.info(r1)     // Catch:{ ClassNotFoundException -> 0x0079 }
        L_0x00ca:
            java.lang.Object r0 = r0.newInstance()     // Catch:{ IllegalAccessException -> 0x00eb, InstantiationException -> 0x00f0 }
            boolean r1 = com.dianxinos.appupdate.s.DEBUG     // Catch:{ IllegalAccessException -> 0x00eb, InstantiationException -> 0x00f0 }
            if (r1 == 0) goto L_0x006d
            java.util.logging.Logger r1 = com.dianxinos.appupdate.s.R     // Catch:{ IllegalAccessException -> 0x00eb, InstantiationException -> 0x00f0 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ IllegalAccessException -> 0x00eb, InstantiationException -> 0x00f0 }
            r2.<init>()     // Catch:{ IllegalAccessException -> 0x00eb, InstantiationException -> 0x00f0 }
            java.lang.String r3 = "Target interface instantiated:"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ IllegalAccessException -> 0x00eb, InstantiationException -> 0x00f0 }
            java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ IllegalAccessException -> 0x00eb, InstantiationException -> 0x00f0 }
            java.lang.String r2 = r2.toString()     // Catch:{ IllegalAccessException -> 0x00eb, InstantiationException -> 0x00f0 }
            r1.info(r2)     // Catch:{ IllegalAccessException -> 0x00eb, InstantiationException -> 0x00f0 }
            goto L_0x006d
        L_0x00eb:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ ClassNotFoundException -> 0x0079 }
            goto L_0x007d
        L_0x00f0:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ ClassNotFoundException -> 0x0079 }
            goto L_0x007d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.dianxinos.appupdate.s.a(java.lang.String, java.lang.Class):java.lang.Object");
    }

    public static boolean p(String str) {
        if (str == null || str.length() == 0) {
            return false;
        }
        return ac.jg.matcher(str).matches();
    }
}
