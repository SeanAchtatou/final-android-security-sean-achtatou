package com.dianxinos.appupdate;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/* compiled from: DigestHelper */
public class m {
    public static String c(File file) {
        if (!file.exists() || !file.isFile()) {
            throw new FileNotFoundException("File not exists");
        }
        FileInputStream fileInputStream = new FileInputStream(file);
        MessageDigest messageDigest = null;
        try {
            messageDigest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        byte[] bArr = new byte[1024];
        do {
            try {
            } catch (IOException e2) {
                e2.printStackTrace();
                try {
                    fileInputStream.close();
                } catch (IOException e3) {
                    e3.printStackTrace();
                }
            } catch (Throwable th) {
                try {
                    fileInputStream.close();
                } catch (IOException e4) {
                    e4.printStackTrace();
                }
                throw th;
            }
        } while (new DigestInputStream(fileInputStream, messageDigest).read(bArr, 0, bArr.length) > 0);
        try {
            fileInputStream.close();
        } catch (IOException e5) {
            e5.printStackTrace();
        }
        String bigInteger = new BigInteger(1, messageDigest.digest()).toString(16);
        while (bigInteger.length() < 32) {
            bigInteger = "0" + bigInteger;
        }
        return bigInteger;
    }
}
