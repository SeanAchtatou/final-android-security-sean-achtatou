package com.dianxinos.appupdate;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: RequestHelper */
public class e {
    private static final boolean DEBUG = x.DEBUG;
    public static final Logger R = Logger.getLogger(e.class.getName());
    private String Q = "http://u.dxsvr.com/api/apps";
    public t S;

    /* JADX WARNING: Removed duplicated region for block: B:35:0x00ed A[SYNTHETIC, Splitter:B:35:0x00ed] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00f9 A[SYNTHETIC, Splitter:B:43:0x00f9] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:32:0x00e8=Splitter:B:32:0x00e8, B:22:0x00d9=Splitter:B:22:0x00d9} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.dianxinos.appupdate.d a(java.lang.String r8, int r9, com.dianxinos.appupdate.f r10, java.util.Map r11) {
        /*
            r7 = this;
            r5 = 0
            java.lang.String r6 = ""
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            org.apache.http.message.BasicNameValuePair r0 = new org.apache.http.message.BasicNameValuePair
            java.lang.String r1 = "pkg"
            r0.<init>(r1, r8)
            r2.add(r0)
            org.apache.http.message.BasicNameValuePair r0 = new org.apache.http.message.BasicNameValuePair
            java.lang.String r1 = "ver"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.StringBuilder r3 = r3.append(r9)
            java.lang.String r4 = ""
            java.lang.StringBuilder r3 = r3.append(r6)
            java.lang.String r3 = r3.toString()
            r0.<init>(r1, r3)
            r2.add(r0)
            org.apache.http.message.BasicNameValuePair r0 = new org.apache.http.message.BasicNameValuePair
            java.lang.String r1 = "asv"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            int r4 = r10.sdkVersion
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r4 = ""
            java.lang.StringBuilder r3 = r3.append(r6)
            java.lang.String r3 = r3.toString()
            r0.<init>(r1, r3)
            r2.add(r0)
            org.apache.http.message.BasicNameValuePair r0 = new org.apache.http.message.BasicNameValuePair
            java.lang.String r1 = "mod"
            java.lang.String r3 = r10.aB
            r0.<init>(r1, r3)
            r2.add(r0)
            org.apache.http.message.BasicNameValuePair r0 = new org.apache.http.message.BasicNameValuePair
            java.lang.String r1 = "dev"
            java.lang.String r3 = r10.device
            r0.<init>(r1, r3)
            r2.add(r0)
            org.apache.http.message.BasicNameValuePair r0 = new org.apache.http.message.BasicNameValuePair
            java.lang.String r1 = "fig"
            java.lang.String r3 = r10.aC
            r0.<init>(r1, r3)
            r2.add(r0)
            if (r11 == 0) goto L_0x00a1
            java.util.Set r0 = r11.keySet()
            java.util.Iterator r3 = r0.iterator()
        L_0x007c:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x00a1
            java.lang.Object r0 = r3.next()
            java.lang.String r0 = (java.lang.String) r0
            java.util.Set r1 = com.dianxinos.appupdate.x.il
            boolean r1 = r1.contains(r0)
            if (r1 != 0) goto L_0x007c
            java.lang.Object r1 = r11.get(r0)
            java.lang.String r1 = (java.lang.String) r1
            if (r1 == 0) goto L_0x007c
            org.apache.http.message.BasicNameValuePair r4 = new org.apache.http.message.BasicNameValuePair
            r4.<init>(r0, r1)
            r2.add(r4)
            goto L_0x007c
        L_0x00a1:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = r7.Q
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "/check_update"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            r1 = 0
            java.io.InputStream r0 = r7.a(r0, r2, r1)
            if (r0 == 0) goto L_0x00fd
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ UnsupportedEncodingException -> 0x00d7, IOException -> 0x00e6, all -> 0x00f5 }
            java.io.InputStreamReader r2 = new java.io.InputStreamReader     // Catch:{ UnsupportedEncodingException -> 0x00d7, IOException -> 0x00e6, all -> 0x00f5 }
            java.lang.String r3 = "UTF-8"
            r2.<init>(r0, r3)     // Catch:{ UnsupportedEncodingException -> 0x00d7, IOException -> 0x00e6, all -> 0x00f5 }
            r1.<init>(r2)     // Catch:{ UnsupportedEncodingException -> 0x00d7, IOException -> 0x00e6, all -> 0x00f5 }
            java.lang.String r0 = r1.readLine()     // Catch:{ UnsupportedEncodingException -> 0x012e, IOException -> 0x012c }
            if (r1 == 0) goto L_0x00d2
            r1.close()     // Catch:{ IOException -> 0x0126 }
        L_0x00d2:
            com.dianxinos.appupdate.d r0 = r7.a(r0)
        L_0x00d6:
            return r0
        L_0x00d7:
            r0 = move-exception
            r1 = r5
        L_0x00d9:
            r0.printStackTrace()     // Catch:{ all -> 0x012a }
            if (r1 == 0) goto L_0x0130
            r1.close()     // Catch:{ IOException -> 0x00e3 }
            r0 = r5
            goto L_0x00d2
        L_0x00e3:
            r0 = move-exception
            r0 = r5
            goto L_0x00d2
        L_0x00e6:
            r0 = move-exception
            r1 = r5
        L_0x00e8:
            r0.printStackTrace()     // Catch:{ all -> 0x012a }
            if (r1 == 0) goto L_0x0130
            r1.close()     // Catch:{ IOException -> 0x00f2 }
            r0 = r5
            goto L_0x00d2
        L_0x00f2:
            r0 = move-exception
            r0 = r5
            goto L_0x00d2
        L_0x00f5:
            r0 = move-exception
            r1 = r5
        L_0x00f7:
            if (r1 == 0) goto L_0x00fc
            r1.close()     // Catch:{ IOException -> 0x0128 }
        L_0x00fc:
            throw r0
        L_0x00fd:
            boolean r0 = com.dianxinos.appupdate.e.DEBUG
            if (r0 == 0) goto L_0x0108
            java.util.logging.Logger r0 = com.dianxinos.appupdate.e.R
            java.lang.String r1 = "Network error when checking update"
            r0.warning(r1)
        L_0x0108:
            boolean r0 = com.dianxinos.appupdate.e.DEBUG
            if (r0 == 0) goto L_0x0124
            java.util.logging.Logger r0 = com.dianxinos.appupdate.e.R
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Response format error when checking update for pkg:"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r8)
            java.lang.String r1 = r1.toString()
            r0.warning(r1)
        L_0x0124:
            r0 = r5
            goto L_0x00d6
        L_0x0126:
            r1 = move-exception
            goto L_0x00d2
        L_0x0128:
            r1 = move-exception
            goto L_0x00fc
        L_0x012a:
            r0 = move-exception
            goto L_0x00f7
        L_0x012c:
            r0 = move-exception
            goto L_0x00e8
        L_0x012e:
            r0 = move-exception
            goto L_0x00d9
        L_0x0130:
            r0 = r5
            goto L_0x00d2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.dianxinos.appupdate.e.a(java.lang.String, int, com.dianxinos.appupdate.f, java.util.Map):com.dianxinos.appupdate.d");
    }

    /* access modifiers changed from: protected */
    public d a(String str) {
        String optString;
        if (str == null) {
            return null;
        }
        d dVar = new d();
        try {
            JSONObject jSONObject = new JSONObject(str);
            if (!jSONObject.optBoolean("ava", false)) {
                return dVar;
            }
            dVar.available = true;
            int optInt = jSONObject.optInt("vc", -1);
            String optString2 = jSONObject.optString("vn");
            String optString3 = jSONObject.optString("url");
            long optLong = jSONObject.optLong("size", 0);
            if (optInt <= 0 || optString2 == null || optString3 == null) {
                dVar.available = false;
                return dVar;
            }
            dVar.versionCode = optInt;
            dVar.versionName = optString2;
            dVar.I = optString3;
            dVar.description = jSONObject.optString("dspt");
            dVar.L = jSONObject.optString("md5");
            dVar.K = optLong;
            int optInt2 = jSONObject.optInt("prt", 2);
            if (optInt2 < 0 || optInt2 > 2) {
                if (DEBUG) {
                    R.warning("Illegal priority:" + optInt2);
                }
                optInt2 = 1;
            }
            dVar.priority = optInt2;
            Iterator<String> keys = jSONObject.keys();
            HashMap hashMap = new HashMap();
            while (keys.hasNext()) {
                String obj = keys.next().toString();
                if (!"ava".equals(obj) && !"vc".equals(obj) && !"vn".equals(obj) && !"url".equals(obj) && !"dspt".equals(obj) && (optString = jSONObject.optString(obj)) != null) {
                    hashMap.put(obj, optString);
                }
            }
            return dVar;
        } catch (JSONException e) {
            e.printStackTrace();
            return dVar;
        }
    }

    /* access modifiers changed from: protected */
    public InputStream a(String str, List list, boolean z) {
        HttpPost httpGet;
        if (z) {
            httpGet = new HttpPost(str);
            if (list != null) {
                try {
                    httpGet.setEntity(new UrlEncodedFormEntity(list, "UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        } else if (list != null) {
            httpGet = new HttpGet(str + "?" + URLEncodedUtils.format(list, "UTF-8"));
        } else {
            httpGet = new HttpGet(str);
        }
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        if (this.S != null) {
            String e2 = this.S.e();
            int f = this.S.f();
            if (e2 != null && f > 0) {
                defaultHttpClient.getParams().setParameter("http.route.default-proxy", new HttpHost(e2, f, "http"));
            }
        }
        httpGet.setHeader("User-Agent", "Appupdate model");
        try {
            HttpResponse execute = defaultHttpClient.execute(httpGet);
            int statusCode = execute.getStatusLine().getStatusCode();
            if (DEBUG) {
                R.info("Get response for " + httpGet.getURI() + ", status:" + statusCode);
            }
            if (statusCode == 200) {
                return execute.getEntity().getContent();
            }
        } catch (IOException e3) {
            e3.printStackTrace();
        }
        return null;
    }

    public void a(t tVar) {
        this.S = tVar;
    }
}
