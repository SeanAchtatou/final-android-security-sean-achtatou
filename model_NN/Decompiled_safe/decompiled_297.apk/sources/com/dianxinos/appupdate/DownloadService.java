package com.dianxinos.appupdate;

import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.IBinder;
import android.util.Log;
import java.lang.Thread;

public class DownloadService extends Service {
    /* access modifiers changed from: private */
    public static final boolean DEBUG = x.DEBUG;
    private DownloadThread fN;
    private q fO;
    private q fP;
    private Object fQ = new Object();
    private final ag fR = new ag(this);
    protected boolean fS = false;
    protected boolean fT = false;
    protected boolean fU = false;
    protected boolean fV = false;
    private r fW = new o(this);

    public void onCreate() {
        super.onCreate();
        if (DEBUG) {
            Log.d("DownloadService", "onCreate");
        }
        this.fV = true;
        this.fO = aV();
    }

    public IBinder onBind(Intent intent) {
        return this.fR;
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        super.onStart(intent, i2);
        this.fT = false;
        this.fS = false;
        if (intent != null) {
            String action = intent.getAction();
            if (DEBUG) {
                Log.v("DownloadService", "onStartCommand, action:" + action);
            }
            if ("com.dianxinos.appupdate.intent.DOWNLOAD".equals(action)) {
                Uri data = intent.getData();
                if (data != null) {
                    this.fS = true;
                    synchronized (this.fQ) {
                        if (this.fN == null || this.fN.getState().equals(Thread.State.TERMINATED)) {
                            af afVar = new af(getApplicationContext(), this.fW);
                            afVar.kB = data.toString();
                            afVar.kD = intent.getStringExtra("extra-filename");
                            afVar.kC = true;
                            int intExtra = intent.getIntExtra("extra-dest", -1);
                            if (intExtra == -1) {
                                Log.w("DownloadService", "Download destination not specified");
                                intExtra = 0;
                            }
                            afVar.cy = intExtra;
                            afVar.priority = intent.getIntExtra("extra-pri", 0);
                            afVar.description = intent.getStringExtra("extra-dspt");
                            afVar.kI = intent.getStringExtra("extra-extra_info");
                            afVar.L = intent.getStringExtra("extra-checksum");
                            afVar.kG = intent.getLongExtra("extra-file-size", 0);
                            this.fN = new DownloadThread(getApplicationContext(), this.fW, afVar);
                            this.fN.c(this.fO);
                            if (this.fP != null) {
                                this.fN.c(this.fP);
                            }
                            this.fN.start();
                            if (DEBUG) {
                                Log.d("DownloadService", "Download thread started, dest:" + afVar.cy + ", checksum:" + afVar.L + ", total:" + afVar.kG);
                            }
                            return 3;
                        }
                        Log.w("DownloadService", "A previous download is still executing");
                    }
                } else {
                    Log.e("DownloadService", "Receive start download command, but no uri specified");
                }
            } else if ("com.dianxinos.appupdate.intent.CANCEL_DOWNLOAD".equals(action)) {
                if (DEBUG) {
                    Log.d("DownloadService", "Canceling download action received");
                }
                this.fT = true;
                aU();
            } else if (DEBUG) {
                Log.w("DownloadService", "Unknown action:" + action);
            }
        }
        return super.onStartCommand(intent, i, i2);
    }

    public void onDestroy() {
        if (DEBUG) {
            Log.v("DownloadService", "onDestroy, tid:" + Thread.currentThread().getId());
        }
        aU();
        this.fU = true;
        super.onDestroy();
    }

    public void a(q qVar) {
        if (qVar != null) {
            this.fP = qVar;
            synchronized (this.fQ) {
                if (this.fN != null) {
                    this.fN.c(qVar);
                }
            }
        }
    }

    public void b(q qVar) {
        if (qVar != null) {
            this.fP = null;
            synchronized (this.fQ) {
                if (this.fN != null) {
                    this.fN.d(qVar);
                }
            }
        }
    }

    private void aU() {
        if (DEBUG) {
            Log.d("DownloadService", "Canceling downlaod, tid:" + Thread.currentThread().getId());
        }
        synchronized (this.fQ) {
            if (this.fN != null) {
                this.fN.cR();
                this.fN = null;
                if (DEBUG) {
                    Log.d("DownloadService", "Download canceled");
                }
            } else if (DEBUG) {
                Log.d("DownloadService", "No active download");
            }
        }
    }

    private q aV() {
        if (DEBUG) {
            Log.d("DownloadService", "Reflecting download progress listener");
        }
        Context applicationContext = getApplicationContext();
        try {
            String string = applicationContext.getPackageManager().getServiceInfo(new ComponentName(applicationContext, DownloadService.class), 128).metaData.getString("com.dianxinos.appupdate.DOWNLOAD_PROGRESS_LISTENER");
            if (string != null) {
                return (q) s.a(string, q.class);
            }
            if (DEBUG) {
                Log.w("DownloadService", "Download progress listener not specified");
            }
            return null;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }
}
