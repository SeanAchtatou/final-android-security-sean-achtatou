package com.dianxinos.appupdate;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class NetworkStatusChangedReceiver extends BroadcastReceiver {
    private static final boolean DEBUG = x.DEBUG;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.dianxinos.appupdate.ah.a(android.content.Context, java.lang.String, boolean):boolean
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.dianxinos.appupdate.ah.a(android.content.Context, java.lang.String, int):int
      com.dianxinos.appupdate.ah.a(android.content.Context, java.lang.String, long):long
      com.dianxinos.appupdate.ah.a(android.content.Context, java.lang.String, java.lang.String):java.lang.String
      com.dianxinos.appupdate.ah.a(android.content.Context, java.lang.String, boolean):boolean */
    public void onReceive(Context context, Intent intent) {
        if ("android.net.conn.CONNECTIVITY_CHANGE".equals(intent.getAction())) {
            boolean booleanExtra = intent.getBooleanExtra("isFailover", false);
            boolean booleanExtra2 = intent.getBooleanExtra("noConnectivity", false);
            if (DEBUG) {
                Log.d("NetworkStatusChangedReceiver", "Receive network status chaned broadcast, failover:" + booleanExtra + ", noNetwork:" + booleanExtra2);
            }
            if (!booleanExtra2 && ah.a(context, "pref-need-redownload", false)) {
                if (DEBUG) {
                    Log.d("NetworkStatusChangedReceiver", "Schedule re-download");
                }
                ((AlarmManager) context.getSystemService("alarm")).set(0, System.currentTimeMillis() + 5000, PendingIntent.getService(context, 0, new Intent("com.dianxinos.appupdate.intent.DOWNLOAD_RETRY"), 268435456));
            }
        }
    }
}
