package com.dianxinos.appupdate;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;

/* compiled from: UpdateManager */
class c implements ServiceConnection {
    final /* synthetic */ k b;

    c(k kVar) {
        this.b = kVar;
    }

    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        if (k.DEBUG) {
            Log.d("UpdateManager", "Service connected");
        }
        DownloadService unused = this.b.cp = ((ag) iBinder).cY();
        if (this.b.cs != null) {
            this.b.cp.a(this.b.cs);
        }
    }

    public void onServiceDisconnected(ComponentName componentName) {
        if (k.DEBUG) {
            Log.d("UpdateManager", "Service disconnected");
        }
        DownloadService unused = this.b.cp = (DownloadService) null;
    }
}
