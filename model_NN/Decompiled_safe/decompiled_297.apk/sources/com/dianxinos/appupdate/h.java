package com.dianxinos.appupdate;

import android.util.Log;
import java.util.Map;

/* compiled from: UpdateManager */
class h implements j {
    final /* synthetic */ k b;
    final /* synthetic */ i be;

    h(k kVar, i iVar) {
        this.b = kVar;
        this.be = iVar;
    }

    public void K() {
        if (this.be != null) {
            this.be.R();
        }
    }

    public void a(int i, String str, String str2, int i2, Map map) {
        if (k.DEBUG) {
            Log.d("UpdateManager", "Update found before download, url:" + this.b.co);
        }
        this.b.a(this.b.co, this.be);
    }

    public void L() {
        if (k.DEBUG) {
            Log.d("UpdateManager", "No update available");
        }
        if (this.be != null) {
            this.be.R();
        }
    }
}
