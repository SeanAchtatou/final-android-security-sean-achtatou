package com.dianxinos.appupdate;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import java.util.Map;

/* compiled from: AppUpdateService */
class v implements j {
    final /* synthetic */ AppUpdateService hg;

    v(AppUpdateService appUpdateService) {
        this.hg = appUpdateService;
    }

    public void K() {
        if (AppUpdateService.DEBUG) {
            Log.d("AppUpdateService", "Auto checking update result: network error");
        }
    }

    public void a(int i, String str, String str2, int i2, Map map) {
        if (AppUpdateService.DEBUG) {
            Log.d("AppUpdateService", "Auto checking update result: Update available");
        }
        Intent intent = new Intent("com.dianxinos.appupdate.intent.NEW_UPDATE");
        intent.setPackage(this.hg.getPackageName());
        intent.putExtra("new-vn", str);
        intent.putExtra("new_vc", i);
        intent.putExtra("update-dspt", str2);
        intent.putExtra("update-pri", i2);
        if (map != null) {
            Bundle bundle = new Bundle();
            for (String str3 : map.keySet()) {
                bundle.putString(str3, (String) map.get(str3));
            }
            intent.putExtra("update-extras", bundle);
        }
        this.hg.sendBroadcast(intent, this.hg.getPackageName() + ".permission.UPDATE");
        if (AppUpdateService.DEBUG) {
            Log.d("AppUpdateService", "Update available broadcast sent");
        }
    }

    public void L() {
        if (AppUpdateService.DEBUG) {
            Log.d("AppUpdateService", "Auto check update result: no update");
        }
    }
}
