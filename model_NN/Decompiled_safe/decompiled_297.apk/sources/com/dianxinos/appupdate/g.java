package com.dianxinos.appupdate;

import java.io.File;
import java.io.FilenameFilter;

/* compiled from: UpdateManager */
class g implements FilenameFilter {
    final /* synthetic */ k b;
    final /* synthetic */ String bd;

    g(k kVar, String str) {
        this.b = kVar;
        this.bd = str;
    }

    public boolean accept(File file, String str) {
        return str.startsWith(this.bd);
    }
}
