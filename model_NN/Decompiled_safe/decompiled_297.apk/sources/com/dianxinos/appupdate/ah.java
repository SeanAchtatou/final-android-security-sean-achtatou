package com.dianxinos.appupdate;

import android.content.Context;
import android.content.SharedPreferences;

/* compiled from: PrefsUtils */
public class ah {
    public static String f(Context context, String str) {
        return a(context, str, (String) null);
    }

    public static String a(Context context, String str, String str2) {
        return context.getSharedPreferences("app-update-prefs", 0).getString(str, str2);
    }

    public static void b(Context context, String str, String str2) {
        SharedPreferences.Editor edit = context.getSharedPreferences("app-update-prefs", 0).edit();
        edit.putString(str, str2);
        edit.commit();
    }

    public static int a(Context context, String str, int i) {
        return context.getSharedPreferences("app-update-prefs", 0).getInt(str, i);
    }

    public static void b(Context context, String str, int i) {
        SharedPreferences.Editor edit = context.getSharedPreferences("app-update-prefs", 0).edit();
        edit.putInt(str, i);
        edit.commit();
    }

    public static long a(Context context, String str, long j) {
        return context.getSharedPreferences("app-update-prefs", 0).getLong(str, j);
    }

    public static void b(Context context, String str, long j) {
        SharedPreferences.Editor edit = context.getSharedPreferences("app-update-prefs", 0).edit();
        edit.putLong(str, j);
        edit.commit();
    }

    public static boolean a(Context context, String str, boolean z) {
        return context.getSharedPreferences("app-update-prefs", 0).getBoolean(str, z);
    }

    public static void b(Context context, String str, boolean z) {
        SharedPreferences.Editor edit = context.getSharedPreferences("app-update-prefs", 0).edit();
        edit.putBoolean(str, z);
        edit.commit();
    }
}
