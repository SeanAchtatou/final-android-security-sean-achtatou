package com.dianxinos.dxservices.config.a;

import android.database.Cursor;
import android.database.CursorWrapper;
import java.util.Date;

/* compiled from: DbHelper */
public class e extends CursorWrapper {
    e(Cursor cursor) {
        super(cursor);
    }

    /* access modifiers changed from: package-private */
    public String getString(String str) {
        return getString(getColumnIndex(str));
    }

    /* access modifiers changed from: package-private */
    public long getLong(String str) {
        return getLong(getColumnIndex(str));
    }

    /* access modifiers changed from: package-private */
    public int getInt(String str) {
        return getInt(getColumnIndex(str));
    }

    /* access modifiers changed from: package-private */
    public Date r(String str) {
        return new Date(getLong(getColumnIndex(str)));
    }
}
