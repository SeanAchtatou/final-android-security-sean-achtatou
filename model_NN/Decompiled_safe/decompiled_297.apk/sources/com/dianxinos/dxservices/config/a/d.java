package com.dianxinos.dxservices.config.a;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/* compiled from: DbHelper */
public class d extends SQLiteOpenHelper {
    public d(Context context) {
        super(context, "i", (SQLiteDatabase.CursorFactory) null, 1);
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("create table if not exists entry (id integer primary key,app text,key text,value text,start numeric,end numeric,repeat text,priority integer,version integer)");
        sQLiteDatabase.execSQL("create table if not exists version (version integer primary key,time numeric)");
        sQLiteDatabase.execSQL("create table if not exists resource (id integer primary key,url text,file text,entry_id integer)");
        sQLiteDatabase.execSQL("create table if not exists update_log (id integer primary key,real_time numeric,expected_time numeric)");
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS entry");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS version");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS resource");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS update_log");
    }
}
