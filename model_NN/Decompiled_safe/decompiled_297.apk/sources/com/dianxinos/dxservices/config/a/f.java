package com.dianxinos.dxservices.config.a;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: Entry */
public class f {
    public static final String[] COLUMNS = {"id", "app", "key", "value", "start", "end", "priority", "repeat", "version"};
    private final Integer bU;
    private final Long bt;
    private final String iP;
    private final Date iQ;
    private final Date iR;
    private final String iS;
    private final Integer iT;
    private final String iU;
    private final String key;
    private final String value;

    public f(JSONObject jSONObject) {
        this.bt = Long.valueOf(jSONObject.getLong("id"));
        this.iP = x(jSONObject.getString("app"));
        this.key = x(jSONObject.getString("key"));
        this.value = x(jSONObject.optString("value"));
        this.iQ = new Date(jSONObject.optLong("start", 0));
        this.iR = new Date(jSONObject.optLong("end", 4102415999936L));
        this.iS = x(jSONObject.optString("repeat", null));
        this.iT = Integer.valueOf(jSONObject.optInt("priority", 0));
        this.bU = Integer.valueOf(jSONObject.getInt("version"));
        this.iU = z(jSONObject.optString("status"));
    }

    public f(Cursor cursor) {
        e eVar = new e(cursor);
        this.bt = Long.valueOf(eVar.getLong("id"));
        this.iP = eVar.getString("app");
        this.key = eVar.getString("key");
        this.value = eVar.getString("value");
        this.iQ = eVar.r("start");
        this.iR = eVar.r("end");
        this.iT = Integer.valueOf(eVar.getInt("priority"));
        this.iS = eVar.getString("repeat");
        this.bU = Integer.valueOf(eVar.getInt("version"));
        this.iU = null;
    }

    public String getValue() {
        return this.value;
    }

    public String cg() {
        return this.iU;
    }

    public void g(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.delete("entry", "id = ?", new String[]{this.bt.toString()});
    }

    public void b(SQLiteDatabase sQLiteDatabase) {
        if (c(sQLiteDatabase)) {
            sQLiteDatabase.update("entry", N(), "id = ?", new String[]{this.bt.toString()});
        } else if (!"D".equals(this.iU)) {
            sQLiteDatabase.insert("entry", null, N());
        }
    }

    public boolean c(SQLiteDatabase sQLiteDatabase) {
        Cursor cursor;
        Throwable th;
        boolean z;
        if (this.bt == null) {
            return false;
        }
        try {
            Cursor rawQuery = sQLiteDatabase.rawQuery("select id from entry where id = ?", new String[]{this.bt.toString()});
            try {
                if (rawQuery.getCount() != 0) {
                    z = true;
                } else {
                    z = false;
                }
                if (rawQuery != null) {
                    rawQuery.close();
                }
                return z;
            } catch (Throwable th2) {
                Throwable th3 = th2;
                cursor = rawQuery;
                th = th3;
            }
        } catch (Throwable th4) {
            Throwable th5 = th4;
            cursor = null;
            th = th5;
        }
        if (cursor != null) {
            cursor.close();
        }
        throw th;
    }

    public List h(SQLiteDatabase sQLiteDatabase) {
        Cursor cursor;
        try {
            Cursor query = sQLiteDatabase.query("resource", g.COLUMNS, "entry_id = ?", new String[]{String.valueOf(this.bt)}, null, null, null);
            try {
                ArrayList arrayList = new ArrayList(query.getCount());
                while (query.moveToNext()) {
                    arrayList.add(new g(query));
                }
                if (query != null) {
                    query.close();
                }
                return arrayList;
            } catch (Throwable th) {
                Throwable th2 = th;
                cursor = query;
                th = th2;
                if (cursor != null) {
                    cursor.close();
                }
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
        }
    }

    public ContentValues N() {
        ContentValues contentValues = new ContentValues();
        contentValues.put("id", this.bt);
        contentValues.put("app", this.iP);
        contentValues.put("key", this.key);
        contentValues.put("value", this.value);
        contentValues.put("start", Long.valueOf(this.iQ.getTime()));
        contentValues.put("end", Long.valueOf(this.iR.getTime()));
        contentValues.put("priority", this.iT);
        contentValues.put("repeat", this.iS);
        contentValues.put("version", this.bU);
        return contentValues;
    }

    private static String x(String str) {
        if (str == null) {
            return null;
        }
        return str.replaceAll("\\\\/", "/");
    }

    public String toString() {
        return "Entry [id=" + this.bt + ", app=" + this.iP + ", key=" + this.key + ", value=" + this.value + ", start=" + this.iQ + ", end=" + this.iR + ", repeat=" + this.iS + ", priority=" + this.iT + ", version=" + this.bU + ", status=" + this.iU + "]";
    }

    public static List y(String str) {
        try {
            JSONArray jSONArray = new JSONArray(str);
            if (jSONArray.length() == 0) {
                return Collections.emptyList();
            }
            return a(jSONArray);
        } catch (JSONException e) {
            return Collections.emptyList();
        }
    }

    public static List a(JSONArray jSONArray) {
        ArrayList arrayList = new ArrayList(jSONArray.length());
        for (int i = 0; i < jSONArray.length(); i++) {
            try {
                arrayList.add(new f(jSONArray.getJSONObject(i)));
            } catch (JSONException e) {
            }
        }
        return arrayList;
    }

    /* JADX WARNING: Removed duplicated region for block: B:6:0x002e A[Catch:{ all -> 0x004b }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void i(android.database.sqlite.SQLiteDatabase r15) {
        /*
            r12 = 1
            r11 = 0
            r10 = 0
            java.lang.String r14 = "entry"
            java.lang.String r13 = "end < ?"
            java.util.Date r0 = new java.util.Date
            r0.<init>()
            long r8 = r0.getTime()
            java.lang.String r1 = "entry"
            java.lang.String[] r2 = com.dianxinos.dxservices.config.a.f.COLUMNS     // Catch:{ all -> 0x0067 }
            java.lang.String r3 = "end < ?"
            r0 = 1
            java.lang.String[] r4 = new java.lang.String[r0]     // Catch:{ all -> 0x0067 }
            r0 = 0
            java.lang.String r5 = java.lang.String.valueOf(r8)     // Catch:{ all -> 0x0067 }
            r4[r0] = r5     // Catch:{ all -> 0x0067 }
            r5 = 0
            r6 = 0
            r7 = 0
            r0 = r15
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ all -> 0x0067 }
        L_0x0028:
            boolean r0 = r1.moveToNext()     // Catch:{ all -> 0x004b }
            if (r0 == 0) goto L_0x0052
            com.dianxinos.dxservices.config.a.f r0 = new com.dianxinos.dxservices.config.a.f     // Catch:{ all -> 0x004b }
            r0.<init>(r1)     // Catch:{ all -> 0x004b }
            java.util.List r0 = r0.h(r15)     // Catch:{ all -> 0x004b }
            java.util.Iterator r2 = r0.iterator()     // Catch:{ all -> 0x004b }
        L_0x003b:
            boolean r0 = r2.hasNext()     // Catch:{ all -> 0x004b }
            if (r0 == 0) goto L_0x0028
            java.lang.Object r0 = r2.next()     // Catch:{ all -> 0x004b }
            com.dianxinos.dxservices.config.a.g r0 = (com.dianxinos.dxservices.config.a.g) r0     // Catch:{ all -> 0x004b }
            r0.g(r15)     // Catch:{ all -> 0x004b }
            goto L_0x003b
        L_0x004b:
            r0 = move-exception
        L_0x004c:
            if (r1 == 0) goto L_0x0051
            r1.close()
        L_0x0051:
            throw r0
        L_0x0052:
            if (r1 == 0) goto L_0x0057
            r1.close()
        L_0x0057:
            java.lang.String r0 = "entry"
            java.lang.String r0 = "end < ?"
            java.lang.String[] r0 = new java.lang.String[r12]
            java.lang.String r1 = java.lang.String.valueOf(r8)
            r0[r11] = r1
            r15.delete(r14, r13, r0)
            return
        L_0x0067:
            r0 = move-exception
            r1 = r10
            goto L_0x004c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.dianxinos.dxservices.config.a.f.i(android.database.sqlite.SQLiteDatabase):void");
    }

    public static f a(String str, String str2, SQLiteDatabase sQLiteDatabase) {
        Cursor cursor;
        f fVar;
        SQLiteQueryBuilder sQLiteQueryBuilder = new SQLiteQueryBuilder();
        sQLiteQueryBuilder.setTables("entry");
        String valueOf = String.valueOf(new Date().getTime());
        try {
            Cursor query = sQLiteQueryBuilder.query(sQLiteDatabase, COLUMNS, "app = ? and key = ? and priority = (select max(priority) from entry where app = ? and key = ? and end > ? and start <= ? ) and end > ? and start <= ?", new String[]{str, str2, str, str2, valueOf, valueOf, valueOf, valueOf}, null, null, null);
            try {
                if (query.moveToNext()) {
                    fVar = new f(query);
                } else {
                    fVar = null;
                }
                if (query != null) {
                    query.close();
                }
                return fVar;
            } catch (Throwable th) {
                Throwable th2 = th;
                cursor = query;
                th = th2;
                if (cursor != null) {
                    cursor.close();
                }
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
        }
    }

    private static String z(String str) {
        if ("A".equals(str) || "U".equals(str) || "D".equals("D")) {
            return str;
        }
        return "A";
    }
}
