package com.dianxinos.dxservices.config.a;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import java.util.Date;

/* compiled from: UpdateLog */
public class b {
    public static final String[] COLUMNS = {"id", "real_time", "expected_time"};
    private final Long bt;
    private Date bu;
    private Date bv;

    public b(Date date) {
        this.bt = null;
        this.bu = date;
    }

    public b(Cursor cursor) {
        e eVar = new e(cursor);
        this.bt = Long.valueOf(eVar.getLong("id"));
        this.bu = eVar.r("real_time");
        this.bv = eVar.r("expected_time");
    }

    public Date M() {
        return this.bu;
    }

    public void c(Date date) {
        this.bu = date;
    }

    public ContentValues N() {
        ContentValues contentValues = new ContentValues();
        contentValues.put("id", this.bt);
        contentValues.put("real_time", this.bu == null ? null : Long.valueOf(this.bu.getTime()));
        contentValues.put("expected_time", this.bv == null ? null : Long.valueOf(this.bv.getTime()));
        return contentValues;
    }

    public void b(SQLiteDatabase sQLiteDatabase) {
        if (c(sQLiteDatabase)) {
            sQLiteDatabase.update("update_log", N(), "id = ?", new String[]{this.bt.toString()});
            return;
        }
        sQLiteDatabase.insert("update_log", null, N());
    }

    public boolean c(SQLiteDatabase sQLiteDatabase) {
        Cursor cursor;
        Throwable th;
        boolean z;
        if (this.bt == null) {
            return false;
        }
        try {
            Cursor rawQuery = sQLiteDatabase.rawQuery("select id from update_log where id = ?", new String[]{this.bt.toString()});
            try {
                if (rawQuery.getCount() != 0) {
                    z = true;
                } else {
                    z = false;
                }
                if (rawQuery != null) {
                    rawQuery.close();
                }
                return z;
            } catch (Throwable th2) {
                Throwable th3 = th2;
                cursor = rawQuery;
                th = th3;
            }
        } catch (Throwable th4) {
            Throwable th5 = th4;
            cursor = null;
            th = th5;
        }
        if (cursor != null) {
            cursor.close();
        }
        throw th;
    }

    public static b d(SQLiteDatabase sQLiteDatabase) {
        Cursor cursor;
        b bVar;
        try {
            Cursor query = sQLiteDatabase.query("update_log", COLUMNS, null, null, null, null, null);
            try {
                if (query.moveToNext()) {
                    bVar = new b(query);
                } else {
                    bVar = null;
                }
                if (query != null) {
                    query.close();
                }
                return bVar;
            } catch (Throwable th) {
                Throwable th2 = th;
                cursor = query;
                th = th2;
                if (cursor != null) {
                    cursor.close();
                }
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
        }
    }

    public static void e(SQLiteDatabase sQLiteDatabase) {
        b d = d(sQLiteDatabase);
        if (d == null) {
            new b(new Date()).b(sQLiteDatabase);
            return;
        }
        d.c(new Date());
        d.b(sQLiteDatabase);
    }

    public String toString() {
        return "UpdateLog [id=" + this.bt + ", realTime=" + this.bu + ", expectedTime=" + this.bv + "]";
    }
}
