package com.dianxinos.dxservices.config.a;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.dianxinos.dxservices.config.ConfigProvider;
import java.io.File;
import java.util.UUID;

/* compiled from: Resource */
public class g {
    public static final String[] COLUMNS = {"id", "url", "file", "entry_id"};
    public static final String lE = String.format("/data/data/%s/files", ConfigProvider.class.getPackage().getName());
    private final Long bt;
    private final String lF;
    private final Long lG;
    private final String url;

    public g(String str, Long l) {
        this(str, UUID.randomUUID().toString(), l);
    }

    public g(String str, String str2, Long l) {
        if (str == null || l == null || str2 == null) {
            throw new IllegalArgumentException("URL, entryId and file should not be null.");
        }
        this.bt = null;
        this.url = str;
        this.lF = str2;
        this.lG = l;
    }

    public g(Cursor cursor) {
        e eVar = new e(cursor);
        this.bt = Long.valueOf(eVar.getLong("id"));
        this.url = eVar.getString("url");
        this.lF = eVar.getString("file");
        this.lG = Long.valueOf(eVar.getLong("entry_id"));
    }

    public File cW() {
        cX();
        return new File(lE, this.lF);
    }

    public ContentValues N() {
        ContentValues contentValues = new ContentValues();
        contentValues.put("id", this.bt);
        contentValues.put("url", this.url);
        contentValues.put("file", this.lF);
        contentValues.put("entry_id", this.lG);
        return contentValues;
    }

    public void b(SQLiteDatabase sQLiteDatabase) {
        if (this.bt != null || !c(sQLiteDatabase)) {
            sQLiteDatabase.insert("resource", null, N());
            return;
        }
        sQLiteDatabase.update("resource", N(), "url = ? and entry_id = ?", new String[]{this.url, this.lG.toString()});
    }

    public boolean c(SQLiteDatabase sQLiteDatabase) {
        Cursor cursor;
        Throwable th;
        boolean z;
        if (this.url == null || this.lG == null) {
            return false;
        }
        try {
            Cursor rawQuery = sQLiteDatabase.rawQuery("select id from resource where url = ? and entry_id = ?", new String[]{this.url, this.lG.toString()});
            try {
                if (rawQuery.getCount() != 0) {
                    z = true;
                } else {
                    z = false;
                }
                if (rawQuery != null) {
                    rawQuery.close();
                }
                return z;
            } catch (Throwable th2) {
                Throwable th3 = th2;
                cursor = rawQuery;
                th = th3;
            }
        } catch (Throwable th4) {
            Throwable th5 = th4;
            cursor = null;
            th = th5;
        }
        if (cursor != null) {
            cursor.close();
        }
        throw th;
    }

    public void g(SQLiteDatabase sQLiteDatabase) {
        if (this.lF != null) {
            new File(this.lF).delete();
        }
        sQLiteDatabase.delete("resource", "id = ?", new String[]{this.bt.toString()});
    }

    public static void cX() {
        File file = new File(lE);
        if (!file.exists()) {
            file.mkdirs();
        }
    }

    public String toString() {
        return "Resource [id=" + this.bt + ", url=" + this.url + ", file=" + this.lF + ", entryId=" + this.lG + "]";
    }
}
