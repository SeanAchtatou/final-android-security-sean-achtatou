package com.dianxinos.dxservices.config;

import android.content.Intent;
import com.dianxinos.dxservices.b.i;

/* compiled from: ConfigProvider */
class e extends i {
    final /* synthetic */ ConfigProvider hN;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    e(ConfigProvider configProvider, String str, long j) {
        super(str, j);
        this.hN = configProvider;
    }

    public void run() {
        this.hN.getContext().startService(new Intent(this.hN.getContext(), ConfigService.class));
    }
}
