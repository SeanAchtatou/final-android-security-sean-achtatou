package com.dianxinos.dxservices.config;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.SystemProperties;
import android.text.TextUtils;
import com.dianxinos.dxservices.b.j;
import com.dianxinos.dxservices.config.a.b;
import com.dianxinos.dxservices.config.a.d;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import org.json.JSONException;

/* compiled from: SyncTask */
public class a implements Runnable {
    private static final String bZ = SystemProperties.get("ro.dianxinos.core.conurl");
    private final com.dianxinos.dxservices.b.a ca;
    /* access modifiers changed from: private */
    public final SQLiteDatabase cb;
    private c cc = new c(this);
    private f cd;
    private j ce;
    private final Context mContext;

    public a(Context context) {
        this.mContext = context;
        this.cb = new d(context).getWritableDatabase();
        this.ca = com.dianxinos.dxservices.b.a.a(context);
        this.ce = new j(this.mContext);
    }

    public void run() {
        try {
            com.dianxinos.dxservices.config.a.a aVar = new com.dianxinos.dxservices.config.a.a(this.mContext);
            try {
                b d = b.d(this.cb);
                if (d == null || aVar.a(d.M())) {
                    ab();
                }
            } catch (RuntimeException e) {
            }
        } catch (JSONException e2) {
        }
    }

    /* access modifiers changed from: package-private */
    public void ab() {
        this.ce.a(e(this.ca.getToken()), this.cc);
    }

    /* access modifiers changed from: private */
    public void a(int i, int i2) {
        this.cd = new f(this, Integer.valueOf(i2));
        this.ce.a(a(i, i2, this.ca.getToken()), this.cd);
    }

    private static String a(int i, int i2, String str) {
        try {
            return String.format(f("/configs/versions/%s-%s?token=%s"), Integer.valueOf(i), Integer.valueOf(i2), URLEncoder.encode(str, "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    private static String e(String str) {
        try {
            return String.format(f("/configs/versions/latest?token=%s"), URLEncoder.encode(str, "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    private static String f(String str) {
        String str2 = TextUtils.isEmpty(bZ) ? "http://donut.dianxinos.com" : bZ;
        if ("/".equals(str2.substring(str2.length() - 1))) {
            str2 = str2.substring(0, str2.length() - 1);
        }
        return str2 + str;
    }
}
