package com.dianxinos.dxservices.config;

import com.dianxinos.dxservices.b.g;
import com.dianxinos.dxservices.config.a.c;
import java.util.Date;
import java.util.List;

/* compiled from: SyncTask */
class f implements g {
    final /* synthetic */ a fX;
    private final Integer lL;

    public f(a aVar, Integer num) {
        this.fX = aVar;
        if (num == null) {
            throw new IllegalArgumentException("targetVersion should not be null.");
        }
        this.lL = num;
    }

    public void c(int i, String str) {
        if (i == 200) {
            try {
                List<com.dianxinos.dxservices.config.a.f> y = com.dianxinos.dxservices.config.a.f.y(str);
                this.fX.cb.beginTransaction();
                for (com.dianxinos.dxservices.config.a.f fVar : y) {
                    if ("D".equals(fVar.cg())) {
                        fVar.g(this.fX.cb);
                    } else {
                        fVar.b(this.fX.cb);
                    }
                }
                com.dianxinos.dxservices.config.a.f.i(this.fX.cb);
                new c(this.lL, new Date()).b(this.fX.cb);
                this.fX.cb.setTransactionSuccessful();
                this.fX.cb.endTransaction();
            } catch (RuntimeException e) {
            } catch (Throwable th) {
                this.fX.cb.endTransaction();
                throw th;
            }
        }
    }
}
