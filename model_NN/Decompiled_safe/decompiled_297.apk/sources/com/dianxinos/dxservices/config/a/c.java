package com.dianxinos.dxservices.config.a;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import java.util.Date;

/* compiled from: Version */
public class c {
    public static final String[] COLUMNS = {"version", "time"};
    private final Integer bU;
    private final Date bV;

    public c(Integer num, Date date) {
        this.bU = num;
        this.bV = date;
    }

    public c(Cursor cursor) {
        e eVar = new e(cursor);
        this.bU = Integer.valueOf(eVar.getInt("version"));
        this.bV = eVar.r("time");
    }

    public Integer U() {
        return this.bU;
    }

    public void b(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.delete("version", null, null);
        sQLiteDatabase.insert("version", null, N());
    }

    public ContentValues N() {
        ContentValues contentValues = new ContentValues();
        contentValues.put("version", this.bU);
        contentValues.put("time", Long.valueOf(this.bV.getTime()));
        return contentValues;
    }

    public String toString() {
        return "Version [version=" + this.bU + ", time=" + this.bV + "]";
    }

    public static c f(SQLiteDatabase sQLiteDatabase) {
        Cursor cursor;
        c cVar;
        try {
            Cursor rawQuery = sQLiteDatabase.rawQuery("select " + TextUtils.join(",", COLUMNS) + " from " + "version" + " where " + "version" + " = (select max(" + "version" + ") from " + "version" + ")", null);
            try {
                if (rawQuery.moveToNext()) {
                    cVar = new c(rawQuery);
                } else {
                    cVar = null;
                }
                if (rawQuery != null) {
                    rawQuery.close();
                }
                return cVar;
            } catch (Throwable th) {
                Throwable th2 = th;
                cursor = rawQuery;
                th = th2;
                if (cursor != null) {
                    cursor.close();
                }
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
        }
    }
}
