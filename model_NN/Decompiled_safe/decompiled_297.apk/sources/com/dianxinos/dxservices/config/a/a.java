package com.dianxinos.dxservices.config.a;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import java.util.Calendar;
import java.util.Date;
import org.json.JSONObject;

/* compiled from: SyncStrategy */
public class a {
    private boolean c;
    private int cycle;
    private boolean d;
    private d e;
    private Context mContext;
    private int mode;

    public a(Context context) {
        SQLiteDatabase sQLiteDatabase;
        Throwable th;
        this.mContext = context;
        this.e = new d(context);
        try {
            SQLiteDatabase readableDatabase = this.e.getReadableDatabase();
            try {
                f a = f.a("com.dianxinos.config", "sync", readableDatabase);
                if (readableDatabase != null) {
                    readableDatabase.close();
                }
                if (a == null) {
                    this.mode = 0;
                } else {
                    JSONObject jSONObject = new JSONObject(a.getValue());
                    this.mode = jSONObject.getInt("mode");
                    if ((this.mode & 4) != 0) {
                        this.cycle = jSONObject.getInt("cycle");
                    }
                }
                if ((this.mode & 1) != 0) {
                    this.c = true;
                }
                if ((this.mode & 2) != 0) {
                    this.d = true;
                }
            } catch (Throwable th2) {
                Throwable th3 = th2;
                sQLiteDatabase = readableDatabase;
                th = th3;
            }
        } catch (Throwable th4) {
            Throwable th5 = th4;
            sQLiteDatabase = null;
            th = th5;
            if (sQLiteDatabase != null) {
                sQLiteDatabase.close();
            }
            throw th;
        }
    }

    public boolean a(Date date) {
        if (date == null) {
            return true;
        }
        long time = new Date().getTime() - date.getTime();
        if (!b()) {
            return false;
        }
        if (time < 0) {
            return true;
        }
        if (this.c && a() && time > 3600000) {
            return true;
        }
        if (time > 604800000) {
            return true;
        }
        if (this.cycle != 0) {
            return time / 60000 > ((long) this.cycle);
        }
        if (!this.d) {
            return false;
        }
        return new Date().getTime() > ((c() % 1440) * 60000) + b(date).getTime();
    }

    private static Date b(Date date) {
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        instance.set(13, 0);
        instance.set(12, 0);
        instance.set(11, 0);
        instance.add(5, 1);
        return instance.getTime();
    }

    private boolean a() {
        ConnectivityManager connectivityManager = (ConnectivityManager) this.mContext.getSystemService("connectivity");
        if (connectivityManager == null) {
            return false;
        }
        NetworkInfo networkInfo = connectivityManager.getNetworkInfo(1);
        return networkInfo != null && networkInfo.getState() == NetworkInfo.State.CONNECTED;
    }

    private boolean b() {
        ConnectivityManager connectivityManager = (ConnectivityManager) this.mContext.getSystemService("connectivity");
        if (connectivityManager == null) {
            return false;
        }
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.getState() == NetworkInfo.State.CONNECTED;
    }

    private long c() {
        String d2 = d();
        if (TextUtils.isDigitsOnly(d2)) {
            return Long.parseLong(d2);
        }
        StringBuilder sb = new StringBuilder();
        char[] charArray = d2.toCharArray();
        int length = charArray.length;
        int i = 0;
        while (i < length) {
            char c2 = charArray[i];
            if (c2 > '9' || c2 < '0') {
                sb.append((int) c2);
                i++;
            } else {
                sb.append((int) c2);
                i++;
            }
        }
        return Long.parseLong(sb.toString());
    }

    private String d() {
        return ((TelephonyManager) this.mContext.getSystemService("phone")).getDeviceId();
    }
}
