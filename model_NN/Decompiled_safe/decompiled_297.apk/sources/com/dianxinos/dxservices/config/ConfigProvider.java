package com.dianxinos.dxservices.config;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.content.pm.PathPermission;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import com.dianxinos.dxservices.config.a.d;
import com.dianxinos.dxservices.config.a.g;
import java.io.FileNotFoundException;
import java.util.Date;

public class ConfigProvider extends ContentProvider {
    private static final UriMatcher lI = new UriMatcher(-1);
    private SQLiteDatabase cb;

    static {
        lI.addURI("com.dianxinos.dxservices.config", "configs", 1);
        lI.addURI("com.dianxinos.dxservices.config", "configs/#/resources", 2);
    }

    public ConfigProvider() {
    }

    public ConfigProvider(Context context, String str, String str2, PathPermission[] pathPermissionArr) {
        super(context, str, str2, pathPermissionArr);
    }

    public boolean onCreate() {
        this.cb = new d(getContext()).getReadableDatabase();
        return this.cb != null;
    }

    public String getType(Uri uri) {
        switch (lI.match(uri)) {
            case 1:
                return "vnd.android.cursor.item/vnd.config";
            case 2:
                return "vnd.android.cursor.dir/vnd.resource";
            default:
                return null;
        }
    }

    public Cursor query(Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        B();
        if (lI.match(uri) == 1) {
            return a(uri.getQueryParameter("app"), uri.getQueryParameter("key"), strArr);
        }
        throw new IllegalArgumentException("Invalid URI: " + uri + ", only config entry can be query");
    }

    private Cursor a(Long l, String str, String[] strArr) {
        if (l == null || str == null) {
            throw new IllegalArgumentException("The path variable entryId and parameter url must be specified in URI.");
        }
        SQLiteQueryBuilder sQLiteQueryBuilder = new SQLiteQueryBuilder();
        sQLiteQueryBuilder.setTables("resource");
        return sQLiteQueryBuilder.query(this.cb, strArr, "entry_id = ? and url = ?", new String[]{l.toString(), str}, null, null, null);
    }

    private Cursor a(String str, String str2, String[] strArr) {
        if (str == null || str2 == null) {
            throw new IllegalArgumentException("The query parameters app and key must be specified in URI.");
        }
        SQLiteQueryBuilder sQLiteQueryBuilder = new SQLiteQueryBuilder();
        sQLiteQueryBuilder.setTables("entry");
        String valueOf = String.valueOf(new Date().getTime());
        return sQLiteQueryBuilder.query(this.cb, strArr, "app = ? and key = ? and priority = (select max(priority) from entry where app = ? and key = ? and end > ? and start <= ? ) and end > ? and start <= ?", new String[]{str, str2, str, str2, valueOf, valueOf, valueOf, valueOf}, null, null, null);
    }

    public Uri insert(Uri uri, ContentValues contentValues) {
        throw new UnsupportedOperationException("Insert operation not permitted.");
    }

    public int delete(Uri uri, String str, String[] strArr) {
        throw new UnsupportedOperationException("Delete operation not permitted.");
    }

    public int update(Uri uri, ContentValues contentValues, String str, String[] strArr) {
        throw new UnsupportedOperationException("Update operation not permitted.");
    }

    public ParcelFileDescriptor openFile(Uri uri, String str) {
        Cursor cursor;
        g gVar;
        if (lI.match(uri) != 2) {
            throw new IllegalArgumentException("Invalid URI: " + uri + ", only resource can be called through openFile.");
        }
        Long valueOf = Long.valueOf(uri.getPathSegments().get(1));
        String queryParameter = uri.getQueryParameter("url");
        try {
            Cursor a = a(valueOf, queryParameter, g.COLUMNS);
            try {
                if (!a.moveToNext()) {
                    g gVar2 = new g(queryParameter, valueOf);
                    gVar2.b(this.cb);
                    gVar = gVar2;
                } else {
                    gVar = new g(a);
                }
                if (a != null) {
                    a.close();
                }
                return ParcelFileDescriptor.open(gVar.cW(), modeToMode(uri, str));
            } catch (Throwable th) {
                th = th;
                cursor = a;
            }
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
        }
        if (cursor != null) {
            cursor.close();
        }
        throw th;
    }

    private void B() {
        new e(this, "start_service_on_query", 10000).start();
    }

    private static int modeToMode(Uri uri, String str) {
        if ("r".equals(str)) {
            return 268435456;
        }
        if ("w".equals(str) || "wt".equals(str)) {
            return 738197504;
        }
        if ("wa".equals(str)) {
            return 704643072;
        }
        if ("rw".equals(str)) {
            return 939524096;
        }
        if ("rwt".equals(str)) {
            return 1006632960;
        }
        throw new FileNotFoundException("Bad mode for " + uri + ": " + str);
    }
}
