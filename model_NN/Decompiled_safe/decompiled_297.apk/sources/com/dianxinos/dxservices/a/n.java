package com.dianxinos.dxservices.a;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.NetworkInfo;
import android.os.Bundle;
import com.dianxinos.dxservices.b.d;
import java.util.Calendar;
import java.util.Date;

/* compiled from: EventReportAlarm */
final class n {
    private IntentFilter bX = null;
    private final q cE;
    private final t[] fs = {new t(this, 60, 360), new t(this, 540, 840), new t(this, 1020, 1320)};
    private boolean ft = false;
    private boolean fu = false;
    private final Context mContext;
    private BroadcastReceiver mReceiver = null;

    public n(q qVar, Context context) {
        this.cE = qVar;
        this.mContext = context;
    }

    public void V() {
        W();
        d(e(true));
    }

    private void W() {
        if (this.bX == null) {
            this.bX = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
            this.bX.addAction("com.dianxinos.dxservices.stat.INTENT_ACTIONS_SEND_OFFLINE");
        }
        if (this.mReceiver == null) {
            this.mReceiver = new o(this);
        }
        this.mContext.registerReceiver(this.mReceiver, this.bX);
    }

    private void aQ() {
        this.cE.bm();
    }

    private void C(int i) {
        this.cE.H(i);
    }

    /* access modifiers changed from: private */
    public void b(Intent intent) {
        NetworkInfo networkInfo;
        Bundle extras = intent.getExtras();
        if (extras == null) {
            networkInfo = null;
        } else {
            networkInfo = (NetworkInfo) extras.get("networkInfo");
        }
        if (networkInfo != null && networkInfo.isConnected()) {
            aQ();
            if (networkInfo.getType() == 1) {
                C(0);
                d(e(false));
            } else if (!this.ft && !this.fu) {
                long j = this.mContext.getSharedPreferences("o", 0).getLong("l", 0);
                long currentTimeMillis = j == 0 ? -1 : System.currentTimeMillis() - j;
                if (currentTimeMillis < 0 || currentTimeMillis > 86400000) {
                    int minute = getMinute();
                    d(a(new t(this, minute, minute + 10)));
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void c(Intent intent) {
        if (d.p(this.mContext)) {
            C(1);
            d(e(false));
            return;
        }
        Date c = c(true);
        if (this.ft) {
            d(c);
        } else if (d.q(this.mContext)) {
            C(1);
            d(d(false));
        } else {
            d(d(true));
        }
    }

    private void d(Date date) {
        Intent intent = new Intent();
        intent.setAction("com.dianxinos.dxservices.stat.INTENT_ACTIONS_SEND_OFFLINE");
        ((AlarmManager) this.mContext.getSystemService("alarm")).set(1, date.getTime(), PendingIntent.getBroadcast(this.mContext, 0, intent, 268435456));
    }

    private t e(int i, boolean z) {
        try {
            String string = this.mContext.getSharedPreferences("o", 0).getString(aR(), null);
            if (!(string == null || string.trim().length() == 0)) {
                String[] split = string.split(",");
                for (String split2 : split) {
                    String[] split3 = split2.split(":");
                    int parseInt = Integer.parseInt(split3[0]);
                    int parseInt2 = Integer.parseInt(split3[1]);
                    if (z && i >= parseInt && i < parseInt2) {
                        return new t(this, parseInt, parseInt2);
                    }
                    if (i < parseInt) {
                        return new t(this, parseInt, parseInt2);
                    }
                }
            }
        } catch (Exception e) {
        }
        return null;
    }

    private Date c(boolean z) {
        int minute = getMinute();
        this.ft = true;
        t e = e(minute, z);
        if (e != null) {
            return a(0, minute, e);
        }
        this.ft = false;
        return null;
    }

    private Date d(boolean z) {
        int minute = getMinute();
        this.fu = true;
        long j = this.mContext.getSharedPreferences("o", 0).getLong("l", 0);
        for (t tVar : this.fs) {
            if (j == 0 || j < getDate(tVar.gK).getTime() || j >= getDate(tVar.gL).getTime()) {
                if (z && minute >= tVar.gK && minute < tVar.gL) {
                    return a(0, minute, tVar);
                }
                if (minute < tVar.gK) {
                    return a(tVar);
                }
            }
        }
        this.fu = false;
        return a(1, this.fs[0]);
    }

    private Date getDate(int i) {
        Calendar instance = Calendar.getInstance();
        instance.set(11, i / 60);
        instance.set(12, i % 60);
        instance.set(13, 0);
        instance.set(14, 0);
        return instance.getTime();
    }

    private Date e(boolean z) {
        Date c = c(z);
        return c != null ? c : d(z);
    }

    private Date a(t tVar) {
        return a(0, tVar.gK, tVar);
    }

    private Date a(int i, t tVar) {
        return a(i, tVar.gK, tVar);
    }

    private Date a(int i, int i2, t tVar) {
        int max = Math.max(i2, tVar.gK);
        int random = max + ((int) (Math.random() * ((double) (tVar.gL - max))));
        Calendar instance = Calendar.getInstance();
        instance.add(5, i);
        instance.set(11, random / 60);
        instance.set(12, random % 60);
        return instance.getTime();
    }

    private String aR() {
        return String.valueOf(Calendar.getInstance().get(7));
    }

    private int getMinute() {
        Calendar instance = Calendar.getInstance();
        return (instance.get(11) * 60) + instance.get(12) + 1;
    }
}
