package com.dianxinos.dxservices.a;

import android.content.Context;
import android.os.Environment;
import android.os.StatFs;
import android.os.SystemProperties;
import android.util.Log;
import javax.crypto.KeyGenerator;
import org.apache.commons.codec.binary.Base64;

/* compiled from: EventConfig */
public final class s {
    private static long gF = 10485760;
    private static Double gG = null;
    private static String gH;
    private static byte[] gI;
    private static String gJ;

    static {
        try {
            if (gI == null) {
                KeyGenerator instance = KeyGenerator.getInstance("AES");
                instance.init(128);
                gI = instance.generateKey().getEncoded();
                gH = new String(Base64.encodeBase64(gI));
            }
        } catch (Exception e) {
            Log.e("stat.EventConfig", "Failed to generate the aes key.");
        }
    }

    public static void d(Context context, int i) {
        if (i == 0) {
            gJ = k.x(context);
        } else if (1 == i) {
            gJ = k.y(context);
        } else {
            gJ = null;
        }
    }

    public static String w(Context context) {
        if (gJ == null || gJ.length() == 0) {
            return k.w(context);
        }
        return gJ;
    }

    public static String z(Context context) {
        String a = a("com.dianxinos.dxservices", "public_key", context);
        if (a == null || a.trim().length() == 0) {
            return "MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAKtGrzy6i+TTp+M3H+Za7MZa89RGVkDWhpSjO2VCD2Q7n+PeTHZ5SqdHuUTAQ0OPZhCgdNkmCByrcF4PtOHMQY0CAwEAAQ==";
        }
        return a;
    }

    public static String br() {
        return "MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAKtGrzy6i+TTp+M3H+Za7MZa89RGVkDWhpSjO2VCD2Q7n+PeTHZ5SqdHuUTAQ0OPZhCgdNkmCByrcF4PtOHMQY0CAwEAAQ==";
    }

    public static String bs() {
        return gH;
    }

    public static byte[] bt() {
        return gI;
    }

    public static String A(Context context) {
        String str = SystemProperties.get("ro.dianxinos.core.staurl");
        if (str == null || str.trim().length() == 0) {
            return "http://pasta.dianxinos.com/api/data";
        }
        return str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    public static long B(Context context) {
        double d;
        String a = a("com.dianxinos.dxservices", "api_sta_max_db_size_percent", context);
        if (a != null) {
            try {
                d = Double.parseDouble(a);
            } catch (NumberFormatException e) {
                d = 0.1d;
            }
        } else {
            d = 0.1d;
        }
        if (gG == null || !gG.equals(Double.valueOf(d))) {
            StatFs statFs = new StatFs(Environment.getDataDirectory().getPath());
            long blockSize = (long) statFs.getBlockSize();
            long availableBlocks = (long) statFs.getAvailableBlocks();
            gF = Math.min(10485760L, (long) (d * ((double) (availableBlocks * blockSize))));
            gF = Math.max(gF, 1048576L);
            gF = Math.min(gF, (long) (((double) (availableBlocks * blockSize)) * 0.5d));
        }
        return gF;
    }

    public static int C(Context context) {
        String a = a("com.dianxinos.dxservices", "event_upload_priority_threshold_key", context);
        if (a == null || a.trim().length() == 0) {
            return 0;
        }
        try {
            return Integer.parseInt(a);
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    public static long D(Context context) {
        String a = a("com.dianxinos.dxservices", "event_upload_min_send_offline_interval_key", context);
        if (a == null || a.trim().length() == 0) {
            return 600000;
        }
        try {
            return Long.parseLong(a);
        } catch (NumberFormatException e) {
            return 600000;
        }
    }

    public static String a(String str, String str2, Context context) {
        return null;
    }
}
