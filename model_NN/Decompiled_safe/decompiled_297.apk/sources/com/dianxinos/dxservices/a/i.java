package com.dianxinos.dxservices.a;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/* compiled from: HwInfoService */
class i extends BroadcastReceiver {
    final /* synthetic */ f eA;

    i(f fVar) {
        this.eA = fVar;
    }

    public void onReceive(Context context, Intent intent) {
        if ("android.intent.action.SIM_STATE_CHANGED".equals(intent.getAction())) {
            this.eA.X();
        }
        if ("android.intent.action.SERVICE_STATE".equals(intent.getAction())) {
            this.eA.Y();
        } else if ("android.net.wifi.WIFI_STATE_CHANGED".equals(intent.getAction())) {
            this.eA.Z();
        }
    }
}
