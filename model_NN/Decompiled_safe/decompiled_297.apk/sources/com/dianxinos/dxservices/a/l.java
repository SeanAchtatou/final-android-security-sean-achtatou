package com.dianxinos.dxservices.a;

import android.content.Context;
import android.util.Pair;
import com.dianxinos.dxservices.b.a;
import com.dianxinos.dxservices.b.b;
import com.dianxinos.dxservices.b.d;
import java.util.ArrayList;
import java.util.Calendar;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: EventReporter */
class l {
    final /* synthetic */ q aR;
    private final a ca;
    private final Context mContext;

    public l(q qVar, Context context) {
        this.aR = qVar;
        this.ca = a.a(context);
        this.mContext = context;
    }

    public boolean h(String str, String str2) {
        String q = this.ca.q();
        if (q == null || q.trim().length() == 0) {
            return false;
        }
        if (str == null || str.trim().length() == 0) {
            return false;
        }
        String A = s.A(this.mContext);
        if (A == null || A.trim().length() == 0) {
            return false;
        }
        ArrayList arrayList = new ArrayList();
        JSONObject jSONObject = new JSONObject();
        try {
            a(jSONObject, str);
            try {
                byte[] o = k.o(jSONObject.toString());
                byte[] o2 = k.o(str2);
                byte[] bArr = new byte[(o.length + o2.length + 4)];
                System.arraycopy(intToByteArray(o.length), 0, bArr, 0, 4);
                System.arraycopy(o, 0, bArr, 4, o.length);
                System.arraycopy(o2, 0, bArr, o.length + 4, o2.length);
                arrayList.add(new Pair("data", bArr));
                b bVar = new b(this.mContext, A, "DXCoreService", "stat.EventReporter");
                a(bVar, "token", q);
                if (!bVar.a(null, arrayList)) {
                    return false;
                }
                return true;
            } catch (Exception e) {
                return false;
            }
        } catch (JSONException e2) {
            return false;
        }
    }

    private void a(b bVar, String str, String str2) {
        bVar.addHeader(str, r.a(str2, s.bt()));
    }

    private void a(JSONObject jSONObject, String str) {
        String j = r.j(s.bs(), str);
        if (!str.equals(s.br())) {
            jSONObject.put("a", str);
        }
        jSONObject.put("b", j);
        JSONObject jSONObject2 = new JSONObject();
        Calendar instance = Calendar.getInstance();
        if (instance.getTimeZone().getRawOffset() != k.eW.getRawOffset()) {
            jSONObject2.put("c", instance.getTimeZone().getID());
        }
        jSONObject2.put("d", k.b(instance.getTimeInMillis()));
        jSONObject2.put("e", k.aJ());
        if (d.p(this.mContext)) {
            jSONObject2.put("f", "wifi");
        }
        jSONObject2.put("g", s.w(this.mContext));
        jSONObject.put("c", r.a(jSONObject2.toString(), s.bt()));
    }

    private byte[] intToByteArray(int i) {
        byte[] bArr = new byte[4];
        bArr[3] = (byte) (i & 255);
        bArr[2] = (byte) ((i >> 8) & 255);
        bArr[1] = (byte) ((i >> 16) & 255);
        bArr[0] = (byte) ((i >> 24) & 255);
        return bArr;
    }
}
