package com.dianxinos.dxservices.a;

import android.content.Context;
import android.content.SharedPreferences;
import com.dianxinos.dxservices.b.d;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import org.json.JSONObject;

/* compiled from: EventDispatcher */
public final class g {
    private final f cC;
    private final c cD;
    private final q cE;
    /* access modifiers changed from: private */
    public final Object cF = new Object();
    /* access modifiers changed from: private */
    public e cG;
    /* access modifiers changed from: private */
    public ArrayList cH = new ArrayList();
    private Long cI = null;
    private final Context mContext;
    private boolean mFirst = true;

    public g(Context context) {
        this.cD = new c(context);
        this.cE = new q(context);
        this.cC = new f(context);
        this.mContext = context;
    }

    public void V() {
        this.cI = Long.valueOf(this.mContext.getSharedPreferences("o", 0).getLong("l", 0));
        this.cC.V();
        new v(this).start();
        this.cE.V();
    }

    public boolean a(p pVar, boolean z) {
        if (pVar.getPriority() <= s.C(this.mContext)) {
            return false;
        }
        if (z) {
            return a(pVar);
        }
        return b(pVar);
    }

    private boolean a(p pVar) {
        synchronized (this.cF) {
            this.cH.add(pVar);
            if (this.cH.size() > 0) {
                this.cG.sendEmptyMessage(1);
            }
        }
        return true;
    }

    /* access modifiers changed from: private */
    public boolean b(p pVar) {
        boolean z = this.mFirst;
        this.mFirst = false;
        if (2 == pVar.bb()) {
            if (z) {
                return a(pVar, 1);
            }
            return a(pVar, 0);
        } else if (3 == pVar.bb()) {
            if (!am()) {
                return a(pVar, 1);
            }
            return a(pVar, 0);
        } else if (pVar.bb() == 0) {
            return c(pVar);
        } else {
            if (1 == pVar.bb()) {
                return a(pVar, 0);
            }
            return false;
        }
    }

    private boolean am() {
        if (this.cI.longValue() == 0) {
            return false;
        }
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(this.cI.longValue());
        Calendar instance2 = Calendar.getInstance();
        return instance2.get(1) == instance.get(1) && instance2.get(2) == instance.get(2) && instance2.get(5) == instance.get(5);
    }

    private boolean a(p pVar, int i) {
        boolean a = a("o", pVar);
        if (a && ((i == 1 && d.q(this.mContext)) || (i == 0 && d.p(this.mContext)))) {
            this.cI = Long.valueOf(System.currentTimeMillis());
            SharedPreferences.Editor edit = this.mContext.getSharedPreferences("o", 0).edit();
            edit.putLong("l", this.cI.longValue());
            d.a(edit);
            this.cE.H(i);
        }
        return a;
    }

    private boolean c(p pVar) {
        boolean a = a("r", pVar);
        if (a) {
            this.cE.bm();
        }
        return a;
    }

    private Number a(Number number, Number number2) {
        if (number2 == null) {
            return null;
        }
        if (number instanceof Byte) {
            return Integer.valueOf(((Byte) number).byteValue() + ((Byte) number2).byteValue());
        }
        if (number instanceof Short) {
            return Integer.valueOf(((Short) number).shortValue() + ((Short) number2).shortValue());
        }
        if (number instanceof Integer) {
            return Integer.valueOf(((Integer) number).intValue() + ((Integer) number2).intValue());
        }
        if (number instanceof Long) {
            return Long.valueOf(((Long) number).longValue() + ((Long) number2).longValue());
        }
        if (number instanceof Float) {
            return Float.valueOf(((Float) number).floatValue() + ((Float) number2).floatValue());
        }
        if (number instanceof Double) {
            return Double.valueOf(((Double) number).doubleValue() + ((Double) number2).doubleValue());
        }
        if (number instanceof BigInteger) {
            return ((BigInteger) number).add((BigInteger) number2);
        }
        return number instanceof BigDecimal ? ((BigDecimal) number).add((BigDecimal) number2) : number;
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean a(java.lang.String r11, com.dianxinos.dxservices.a.p r12) {
        /*
            r10 = this;
            android.content.Context r0 = r10.mContext     // Catch:{ Exception -> 0x0059 }
            java.lang.String r3 = com.dianxinos.dxservices.a.s.z(r0)     // Catch:{ Exception -> 0x0059 }
            if (r3 != 0) goto L_0x000a
            r0 = 0
        L_0x0009:
            return r0
        L_0x000a:
            java.lang.String r2 = com.dianxinos.dxservices.a.s.bs()     // Catch:{ Exception -> 0x0059 }
            java.lang.String r0 = r12.getTag()     // Catch:{ Exception -> 0x0059 }
            java.lang.String r1 = com.dianxinos.dxservices.a.r.k(r0, r2)     // Catch:{ Exception -> 0x0059 }
            java.lang.String r6 = com.dianxinos.dxservices.a.r.j(r2, r3)     // Catch:{ Exception -> 0x0059 }
            r0 = 0
            r4 = 0
            r5 = 1
            int r7 = r12.bd()     // Catch:{ Exception -> 0x0059 }
            if (r5 != r7) goto L_0x0105
            r5 = 9
            int r7 = r12.bc()     // Catch:{ Exception -> 0x0059 }
            if (r5 != r7) goto L_0x0238
            java.lang.Object r0 = r12.be()     // Catch:{ Exception -> 0x0059 }
            if (r0 == 0) goto L_0x005c
            java.lang.Object r0 = r12.be()     // Catch:{ Exception -> 0x0059 }
            org.json.JSONObject r0 = (org.json.JSONObject) r0     // Catch:{ Exception -> 0x0059 }
            r5 = r0
        L_0x0038:
            org.json.JSONObject r7 = new org.json.JSONObject     // Catch:{ Exception -> 0x0059 }
            r7.<init>()     // Catch:{ Exception -> 0x0059 }
            java.util.Iterator r8 = r5.keys()     // Catch:{ Exception -> 0x0059 }
        L_0x0041:
            boolean r0 = r8.hasNext()     // Catch:{ Exception -> 0x0059 }
            if (r0 == 0) goto L_0x006c
            java.lang.Object r0 = r8.next()     // Catch:{ Exception -> 0x0059 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x0059 }
            java.lang.String r9 = com.dianxinos.dxservices.a.r.k(r0, r2)     // Catch:{ Exception -> 0x0059 }
            java.lang.Object r0 = r5.get(r0)     // Catch:{ Exception -> 0x0059 }
            r7.put(r9, r0)     // Catch:{ Exception -> 0x0059 }
            goto L_0x0041
        L_0x0059:
            r0 = move-exception
            r0 = 0
            goto L_0x0009
        L_0x005c:
            int r0 = r12.bc()     // Catch:{ Exception -> 0x0059 }
            java.lang.String r5 = r12.bf()     // Catch:{ Exception -> 0x0059 }
            java.lang.Object r0 = a(r0, r5)     // Catch:{ Exception -> 0x0059 }
            org.json.JSONObject r0 = (org.json.JSONObject) r0     // Catch:{ Exception -> 0x0059 }
            r5 = r0
            goto L_0x0038
        L_0x006c:
            r2 = r7
        L_0x006d:
            com.dianxinos.dxservices.a.c r0 = r10.cD     // Catch:{ Exception -> 0x0059 }
            com.dianxinos.dxservices.a.a r0 = r0.d(r11)     // Catch:{ Exception -> 0x0059 }
            android.content.Context r5 = r10.mContext     // Catch:{ Exception -> 0x0059 }
            r7 = 0
            android.content.SharedPreferences r5 = r5.getSharedPreferences(r11, r7)     // Catch:{ Exception -> 0x0059 }
            java.lang.String r7 = "pk"
            r8 = 0
            java.lang.String r7 = r5.getString(r7, r8)     // Catch:{ Exception -> 0x0059 }
            java.lang.String r8 = "pkv"
            r9 = 0
            int r8 = r5.getInt(r8, r9)     // Catch:{ Exception -> 0x0059 }
            r0.lock()     // Catch:{ Exception -> 0x0059 }
            if (r7 == 0) goto L_0x0096
            boolean r9 = r7.equals(r3)     // Catch:{ all -> 0x0100 }
            if (r9 != 0) goto L_0x0096
            r0.g(r8)     // Catch:{ all -> 0x0100 }
        L_0x0096:
            if (r7 == 0) goto L_0x009e
            boolean r7 = r7.equals(r3)     // Catch:{ all -> 0x0100 }
            if (r7 != 0) goto L_0x0235
        L_0x009e:
            android.content.SharedPreferences$Editor r5 = r5.edit()     // Catch:{ all -> 0x0100 }
            java.lang.String r7 = "pk"
            r5.putString(r7, r3)     // Catch:{ all -> 0x0100 }
            java.lang.String r3 = "pkv"
            int r7 = r8 + 1
            r5.putInt(r3, r7)     // Catch:{ all -> 0x0100 }
            r5.commit()     // Catch:{ all -> 0x0100 }
        L_0x00b1:
            r3 = 1
            int r5 = r12.bd()     // Catch:{ all -> 0x0100 }
            if (r3 != r5) goto L_0x0202
            java.util.Date r3 = r12.getTime()     // Catch:{ all -> 0x0100 }
            java.lang.String r3 = r0.a(r1, r3, r6)     // Catch:{ all -> 0x0100 }
            if (r3 == 0) goto L_0x01c2
            r4 = 9
            int r5 = r12.bc()     // Catch:{ all -> 0x0100 }
            if (r4 != r5) goto L_0x0190
            int r4 = r12.bc()     // Catch:{ all -> 0x0100 }
            java.lang.Object r11 = a(r4, r3)     // Catch:{ all -> 0x0100 }
            org.json.JSONObject r11 = (org.json.JSONObject) r11     // Catch:{ all -> 0x0100 }
            org.json.JSONObject r2 = (org.json.JSONObject) r2     // Catch:{ all -> 0x0100 }
            java.util.Iterator r7 = r2.keys()     // Catch:{ all -> 0x0100 }
        L_0x00da:
            boolean r3 = r7.hasNext()     // Catch:{ all -> 0x0100 }
            if (r3 == 0) goto L_0x017a
            java.lang.Object r3 = r7.next()     // Catch:{ all -> 0x0100 }
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ all -> 0x0100 }
            java.lang.Object r4 = r2.get(r3)     // Catch:{ all -> 0x0100 }
            java.lang.Number r4 = (java.lang.Number) r4     // Catch:{ all -> 0x0100 }
            boolean r5 = r11.has(r3)     // Catch:{ all -> 0x0100 }
            if (r5 == 0) goto L_0x0175
            java.lang.Object r5 = r11.get(r3)     // Catch:{ all -> 0x0100 }
            java.lang.Number r5 = (java.lang.Number) r5     // Catch:{ all -> 0x0100 }
            java.lang.Number r4 = r10.a(r5, r4)     // Catch:{ all -> 0x0100 }
            r11.put(r3, r4)     // Catch:{ all -> 0x0100 }
            goto L_0x00da
        L_0x0100:
            r1 = move-exception
            r0.unlock()     // Catch:{ Exception -> 0x0059 }
            throw r1     // Catch:{ Exception -> 0x0059 }
        L_0x0105:
            r4 = 4
            int r5 = r12.bd()     // Catch:{ Exception -> 0x0059 }
            if (r4 != r5) goto L_0x012a
            java.lang.Object r2 = r12.be()     // Catch:{ Exception -> 0x0059 }
            if (r2 == 0) goto L_0x0122
            int r2 = r12.bc()     // Catch:{ Exception -> 0x0059 }
            java.lang.Object r4 = r12.be()     // Catch:{ Exception -> 0x0059 }
            java.lang.String r2 = a(r2, r4)     // Catch:{ Exception -> 0x0059 }
            r4 = r2
            r2 = r0
            goto L_0x006d
        L_0x0122:
            java.lang.String r2 = r12.bf()     // Catch:{ Exception -> 0x0059 }
            r4 = r2
            r2 = r0
            goto L_0x006d
        L_0x012a:
            r4 = 3
            int r5 = r12.bd()     // Catch:{ Exception -> 0x0059 }
            if (r4 != r5) goto L_0x014f
            java.lang.Object r2 = r12.be()     // Catch:{ Exception -> 0x0059 }
            if (r2 == 0) goto L_0x0147
            int r2 = r12.bc()     // Catch:{ Exception -> 0x0059 }
            java.lang.Object r4 = r12.be()     // Catch:{ Exception -> 0x0059 }
            java.lang.String r2 = a(r2, r4)     // Catch:{ Exception -> 0x0059 }
            r4 = r2
            r2 = r0
            goto L_0x006d
        L_0x0147:
            java.lang.String r2 = r12.bf()     // Catch:{ Exception -> 0x0059 }
            r4 = r2
            r2 = r0
            goto L_0x006d
        L_0x014f:
            java.lang.Object r4 = r12.be()     // Catch:{ Exception -> 0x0059 }
            if (r4 == 0) goto L_0x0169
            int r4 = r12.bc()     // Catch:{ Exception -> 0x0059 }
            java.lang.Object r5 = r12.be()     // Catch:{ Exception -> 0x0059 }
            java.lang.String r4 = a(r4, r5)     // Catch:{ Exception -> 0x0059 }
            java.lang.String r2 = com.dianxinos.dxservices.a.r.k(r4, r2)     // Catch:{ Exception -> 0x0059 }
            r4 = r2
            r2 = r0
            goto L_0x006d
        L_0x0169:
            java.lang.String r4 = r12.bf()     // Catch:{ Exception -> 0x0059 }
            java.lang.String r2 = com.dianxinos.dxservices.a.r.k(r4, r2)     // Catch:{ Exception -> 0x0059 }
            r4 = r2
            r2 = r0
            goto L_0x006d
        L_0x0175:
            r11.put(r3, r4)     // Catch:{ all -> 0x0100 }
            goto L_0x00da
        L_0x017a:
            int r2 = r12.bc()     // Catch:{ all -> 0x0100 }
            java.lang.String r2 = a(r2, r11)     // Catch:{ all -> 0x0100 }
        L_0x0182:
            java.util.Date r3 = r12.getTime()     // Catch:{ all -> 0x0100 }
            boolean r1 = r0.b(r1, r2, r3, r6)     // Catch:{ all -> 0x0100 }
            r0.unlock()     // Catch:{ Exception -> 0x0059 }
            r0 = r1
            goto L_0x0009
        L_0x0190:
            int r2 = r12.bc()     // Catch:{ all -> 0x0100 }
            java.lang.Object r11 = a(r2, r3)     // Catch:{ all -> 0x0100 }
            java.lang.Number r11 = (java.lang.Number) r11     // Catch:{ all -> 0x0100 }
            java.lang.Object r2 = r12.be()     // Catch:{ all -> 0x0100 }
            if (r2 == 0) goto L_0x01b3
            java.lang.Object r2 = r12.be()     // Catch:{ all -> 0x0100 }
            java.lang.Number r2 = (java.lang.Number) r2     // Catch:{ all -> 0x0100 }
        L_0x01a6:
            java.lang.Number r2 = r10.a(r11, r2)     // Catch:{ all -> 0x0100 }
            int r3 = r12.bc()     // Catch:{ all -> 0x0100 }
            java.lang.String r2 = a(r3, r2)     // Catch:{ all -> 0x0100 }
            goto L_0x0182
        L_0x01b3:
            int r2 = r12.bc()     // Catch:{ all -> 0x0100 }
            java.lang.String r3 = r12.bf()     // Catch:{ all -> 0x0100 }
            java.lang.Object r2 = a(r2, r3)     // Catch:{ all -> 0x0100 }
            java.lang.Number r2 = (java.lang.Number) r2     // Catch:{ all -> 0x0100 }
            goto L_0x01a6
        L_0x01c2:
            r3 = 9
            int r4 = r12.bc()     // Catch:{ all -> 0x0100 }
            if (r3 != r4) goto L_0x01e9
            int r3 = r12.bc()     // Catch:{ all -> 0x0100 }
            java.lang.String r2 = a(r3, r2)     // Catch:{ all -> 0x0100 }
            r4 = r2
        L_0x01d3:
            int r2 = r12.bc()     // Catch:{ all -> 0x0100 }
            int r3 = r12.bd()     // Catch:{ all -> 0x0100 }
            java.util.Date r5 = r12.getTime()     // Catch:{ all -> 0x0100 }
            boolean r1 = r0.b(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ all -> 0x0100 }
            r0.unlock()     // Catch:{ Exception -> 0x0059 }
            r0 = r1
            goto L_0x0009
        L_0x01e9:
            java.lang.String r2 = r12.bf()     // Catch:{ all -> 0x0100 }
            if (r2 == 0) goto L_0x01f5
            java.lang.String r2 = r12.bf()     // Catch:{ all -> 0x0100 }
        L_0x01f3:
            r4 = r2
            goto L_0x01d3
        L_0x01f5:
            int r2 = r12.bc()     // Catch:{ all -> 0x0100 }
            java.lang.Object r3 = r12.be()     // Catch:{ all -> 0x0100 }
            java.lang.String r2 = a(r2, r3)     // Catch:{ all -> 0x0100 }
            goto L_0x01f3
        L_0x0202:
            r2 = 4
            int r3 = r12.bd()     // Catch:{ all -> 0x0100 }
            if (r2 != r3) goto L_0x021f
            int r2 = r12.bc()     // Catch:{ all -> 0x0100 }
            int r3 = r12.bd()     // Catch:{ all -> 0x0100 }
            java.util.Date r5 = r12.getTime()     // Catch:{ all -> 0x0100 }
            boolean r1 = r0.a(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ all -> 0x0100 }
            r0.unlock()     // Catch:{ Exception -> 0x0059 }
            r0 = r1
            goto L_0x0009
        L_0x021f:
            int r2 = r12.bc()     // Catch:{ all -> 0x0100 }
            int r3 = r12.bd()     // Catch:{ all -> 0x0100 }
            java.util.Date r5 = r12.getTime()     // Catch:{ all -> 0x0100 }
            boolean r1 = r0.b(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ all -> 0x0100 }
            r0.unlock()     // Catch:{ Exception -> 0x0059 }
            r0 = r1
            goto L_0x0009
        L_0x0235:
            r7 = r8
            goto L_0x00b1
        L_0x0238:
            r2 = r0
            goto L_0x006d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.dianxinos.dxservices.a.g.a(java.lang.String, com.dianxinos.dxservices.a.p):boolean");
    }

    private static Object a(int i, String str) {
        if (10 == i) {
            return str.getBytes();
        }
        if (9 == i) {
            return new JSONObject(str);
        }
        if (i == 0) {
            return Byte.valueOf(Byte.parseByte(str));
        }
        if (1 == i) {
            return Short.valueOf(Short.parseShort(str));
        }
        if (2 == i) {
            return Integer.valueOf(Integer.parseInt(str));
        }
        if (3 == i) {
            return Long.valueOf(Long.parseLong(str));
        }
        if (4 == i) {
            return Float.valueOf(Float.parseFloat(str));
        }
        if (5 == i) {
            return Double.valueOf(Double.parseDouble(str));
        }
        if (6 == i) {
            return new BigInteger(str);
        }
        return 7 == i ? new BigDecimal(str) : str;
    }

    private static String a(int i, Object obj) {
        if (10 == i) {
            return new String((byte[]) obj);
        }
        return obj.toString();
    }
}
