package com.dianxinos.dxservices.a;

import android.content.Context;
import android.os.FileUtils;
import android.os.SystemProperties;
import com.dianxinos.dxservices.b.d;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.TimeZone;
import java.util.zip.GZIPOutputStream;

/* compiled from: EventHelper */
final class k {
    public static final TimeZone eW = TimeZone.getTimeZone("GMT+8");

    k() {
    }

    public static long b(long j) {
        return j / 1000;
    }

    public static long c(long j) {
        return (((long) eW.getOffset(j)) + j) / 86400000;
    }

    public static String aJ() {
        try {
            return FileUtils.readTextFile(new File("/system/dxversion"), 0, null);
        } catch (IOException e) {
            return "";
        }
    }

    public static String w(Context context) {
        try {
            return SystemProperties.get("ro.dianxinos.os.lc");
        } catch (Throwable th) {
            return "";
        }
    }

    public static String x(Context context) {
        BufferedReader bufferedReader;
        BufferedReader bufferedReader2;
        try {
            bufferedReader2 = new BufferedReader(new InputStreamReader(context.getResources().getAssets().open("lc.txt")));
            try {
                String readLine = bufferedReader2.readLine();
                if (readLine != null) {
                    readLine = readLine.trim();
                }
                if (readLine.length() == 0) {
                    readLine = null;
                }
                d.a(bufferedReader2);
                return readLine;
            } catch (Throwable th) {
                Throwable th2 = th;
                bufferedReader = bufferedReader2;
                th = th2;
                d.a(bufferedReader);
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            bufferedReader = null;
            d.a(bufferedReader);
            throw th;
        }
    }

    public static String y(Context context) {
        try {
            return context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData.getString("LC");
        } catch (Throwable th) {
            return null;
        }
    }

    public static byte[] o(String str) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(byteArrayOutputStream);
        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(gZIPOutputStream, "UTF-8"));
        bufferedWriter.write(str);
        bufferedWriter.flush();
        bufferedWriter.close();
        gZIPOutputStream.finish();
        return byteArrayOutputStream.toByteArray();
    }
}
