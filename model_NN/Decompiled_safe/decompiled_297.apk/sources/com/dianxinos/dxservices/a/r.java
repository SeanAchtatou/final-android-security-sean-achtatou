package com.dianxinos.dxservices.a;

import java.security.KeyFactory;
import javax.crypto.Cipher;
import org.apache.commons.codec.binary.Hex;

/* compiled from: EncryptionUtil */
public final class r {
    private static Cipher gB;
    private static Cipher gC;
    private static KeyFactory gD;

    private static Cipher bo() {
        if (gB == null) {
            gB = Cipher.getInstance("AES");
        }
        return gB;
    }

    private static Cipher bp() {
        if (gC == null) {
            gC = Cipher.getInstance("RSA");
        }
        return gC;
    }

    private static KeyFactory bq() {
        if (gD == null) {
            gD = KeyFactory.getInstance("RSA");
        }
        return gD;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x003f, code lost:
        r1 = r4;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized java.lang.String j(java.lang.String r4, java.lang.String r5) {
        /*
            java.lang.Class<com.dianxinos.dxservices.a.r> r0 = com.dianxinos.dxservices.a.r.class
            monitor-enter(r0)
            if (r4 == 0) goto L_0x000f
            java.lang.String r1 = r4.trim()     // Catch:{ Exception -> 0x003e, all -> 0x0041 }
            int r1 = r1.length()     // Catch:{ Exception -> 0x003e, all -> 0x0041 }
            if (r1 != 0) goto L_0x0012
        L_0x000f:
            r1 = r4
        L_0x0010:
            monitor-exit(r0)
            return r1
        L_0x0012:
            java.security.spec.X509EncodedKeySpec r1 = new java.security.spec.X509EncodedKeySpec     // Catch:{ Exception -> 0x003e, all -> 0x0041 }
            byte[] r2 = r5.getBytes()     // Catch:{ Exception -> 0x003e, all -> 0x0041 }
            byte[] r2 = org.apache.commons.codec.binary.Base64.decodeBase64(r2)     // Catch:{ Exception -> 0x003e, all -> 0x0041 }
            r1.<init>(r2)     // Catch:{ Exception -> 0x003e, all -> 0x0041 }
            java.security.KeyFactory r2 = bq()     // Catch:{ Exception -> 0x003e, all -> 0x0041 }
            java.security.PublicKey r1 = r2.generatePublic(r1)     // Catch:{ Exception -> 0x003e, all -> 0x0041 }
            javax.crypto.Cipher r2 = bp()     // Catch:{ Exception -> 0x003e, all -> 0x0041 }
            r3 = 1
            r2.init(r3, r1)     // Catch:{ Exception -> 0x003e, all -> 0x0041 }
            java.lang.String r1 = "utf-8"
            byte[] r1 = r4.getBytes(r1)     // Catch:{ Exception -> 0x003e, all -> 0x0041 }
            byte[] r1 = r2.doFinal(r1)     // Catch:{ Exception -> 0x003e, all -> 0x0041 }
            java.lang.String r1 = a(r1)     // Catch:{ Exception -> 0x003e, all -> 0x0041 }
            goto L_0x0010
        L_0x003e:
            r1 = move-exception
            r1 = r4
            goto L_0x0010
        L_0x0041:
            r1 = move-exception
            monitor-exit(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.dianxinos.dxservices.a.r.j(java.lang.String, java.lang.String):java.lang.String");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0031, code lost:
        r1 = r4;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized java.lang.String a(java.lang.String r4, byte[] r5) {
        /*
            java.lang.Class<com.dianxinos.dxservices.a.r> r0 = com.dianxinos.dxservices.a.r.class
            monitor-enter(r0)
            if (r4 == 0) goto L_0x000f
            java.lang.String r1 = r4.trim()     // Catch:{ Exception -> 0x0030, all -> 0x0033 }
            int r1 = r1.length()     // Catch:{ Exception -> 0x0030, all -> 0x0033 }
            if (r1 != 0) goto L_0x0012
        L_0x000f:
            r1 = r4
        L_0x0010:
            monitor-exit(r0)
            return r1
        L_0x0012:
            javax.crypto.spec.SecretKeySpec r1 = new javax.crypto.spec.SecretKeySpec     // Catch:{ Exception -> 0x0030, all -> 0x0033 }
            java.lang.String r2 = "AES"
            r1.<init>(r5, r2)     // Catch:{ Exception -> 0x0030, all -> 0x0033 }
            javax.crypto.Cipher r2 = bo()     // Catch:{ Exception -> 0x0030, all -> 0x0033 }
            r3 = 1
            r2.init(r3, r1)     // Catch:{ Exception -> 0x0030, all -> 0x0033 }
            java.lang.String r1 = "utf-8"
            byte[] r1 = r4.getBytes(r1)     // Catch:{ Exception -> 0x0030, all -> 0x0033 }
            byte[] r1 = r2.doFinal(r1)     // Catch:{ Exception -> 0x0030, all -> 0x0033 }
            java.lang.String r1 = a(r1)     // Catch:{ Exception -> 0x0030, all -> 0x0033 }
            goto L_0x0010
        L_0x0030:
            r1 = move-exception
            r1 = r4
            goto L_0x0010
        L_0x0033:
            r1 = move-exception
            monitor-exit(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.dianxinos.dxservices.a.r.a(java.lang.String, byte[]):java.lang.String");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0039, code lost:
        r1 = r4;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized java.lang.String k(java.lang.String r4, java.lang.String r5) {
        /*
            java.lang.Class<com.dianxinos.dxservices.a.r> r0 = com.dianxinos.dxservices.a.r.class
            monitor-enter(r0)
            if (r4 == 0) goto L_0x000f
            java.lang.String r1 = r4.trim()     // Catch:{ Exception -> 0x0038, all -> 0x003b }
            int r1 = r1.length()     // Catch:{ Exception -> 0x0038, all -> 0x003b }
            if (r1 != 0) goto L_0x0012
        L_0x000f:
            r1 = r4
        L_0x0010:
            monitor-exit(r0)
            return r1
        L_0x0012:
            byte[] r1 = r5.getBytes()     // Catch:{ Exception -> 0x0038, all -> 0x003b }
            byte[] r1 = org.apache.commons.codec.binary.Base64.decodeBase64(r1)     // Catch:{ Exception -> 0x0038, all -> 0x003b }
            javax.crypto.spec.SecretKeySpec r2 = new javax.crypto.spec.SecretKeySpec     // Catch:{ Exception -> 0x0038, all -> 0x003b }
            java.lang.String r3 = "AES"
            r2.<init>(r1, r3)     // Catch:{ Exception -> 0x0038, all -> 0x003b }
            javax.crypto.Cipher r1 = bo()     // Catch:{ Exception -> 0x0038, all -> 0x003b }
            r3 = 1
            r1.init(r3, r2)     // Catch:{ Exception -> 0x0038, all -> 0x003b }
            java.lang.String r2 = "utf-8"
            byte[] r2 = r4.getBytes(r2)     // Catch:{ Exception -> 0x0038, all -> 0x003b }
            byte[] r1 = r1.doFinal(r2)     // Catch:{ Exception -> 0x0038, all -> 0x003b }
            java.lang.String r1 = a(r1)     // Catch:{ Exception -> 0x0038, all -> 0x003b }
            goto L_0x0010
        L_0x0038:
            r1 = move-exception
            r1 = r4
            goto L_0x0010
        L_0x003b:
            r1 = move-exception
            monitor-exit(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.dianxinos.dxservices.a.r.k(java.lang.String, java.lang.String):java.lang.String");
    }

    private static String a(byte[] bArr) {
        return String.valueOf(Hex.encodeHex(bArr));
    }
}
