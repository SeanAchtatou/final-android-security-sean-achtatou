package com.dianxinos.dxservices.a;

import android.content.Context;
import android.content.SharedPreferences;
import com.dianxinos.dxservices.b.d;

/* compiled from: EventReporter */
final class q {
    private final c cD;
    private Long gA = null;
    private final l gx;
    private final n gy;
    /* access modifiers changed from: private */
    public b gz;
    private final Context mContext;

    public q(Context context) {
        this.mContext = context;
        this.cD = new c(this.mContext);
        this.gx = new l(this, this.mContext);
        this.gy = new n(this, this.mContext);
    }

    public void V() {
        new w(this).start();
        this.gy.V();
    }

    public void H(int i) {
        SharedPreferences.Editor edit = this.mContext.getSharedPreferences("o", 0).edit();
        edit.putInt("t", 0);
        edit.commit();
        if (i == 0) {
            this.gz.sendEmptyMessage(1);
        } else {
            this.gz.sendEmptyMessage(2);
        }
    }

    public void bm() {
        SharedPreferences.Editor edit = this.mContext.getSharedPreferences("r", 0).edit();
        edit.putInt("t", 0);
        edit.commit();
        this.gz.sendEmptyMessage(0);
    }

    /* access modifiers changed from: private */
    public void bn() {
        int i;
        int i2;
        if (d.q(this.mContext)) {
            a d = this.cD.d("r");
            if (!d.isEmpty()) {
                SharedPreferences sharedPreferences = this.mContext.getSharedPreferences("r", 0);
                int i3 = sharedPreferences.getInt("t", 0);
                d.lock();
                try {
                    d h = d.h(1000);
                    if (h != null) {
                        boolean h2 = this.gx.h(sharedPreferences.getString("pk", null), h.getData());
                        SharedPreferences.Editor edit = sharedPreferences.edit();
                        if (h2) {
                            i2 = i3;
                            i = 0;
                        } else {
                            i = i3 + 1;
                            i2 = i;
                        }
                        edit.putInt("t", i);
                        edit.commit();
                        if (h2) {
                            d.a(h.T());
                        } else if (i2 < 3) {
                            this.gz.sendEmptyMessageDelayed(0, 600000);
                        }
                    }
                } finally {
                    d.unlock();
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void I(int i) {
        int i2;
        int i3;
        long currentTimeMillis = System.currentTimeMillis();
        long longValue = this.gA == null ? -1 : currentTimeMillis - this.gA.longValue();
        if (longValue >= 0 && longValue < s.D(this.mContext)) {
            return;
        }
        if ((i == 0 && d.p(this.mContext)) || (i == 1 && d.q(this.mContext))) {
            a d = this.cD.d("o");
            if (!d.isEmpty()) {
                SharedPreferences sharedPreferences = this.mContext.getSharedPreferences("o", 0);
                int i4 = sharedPreferences.getInt("t", 0);
                d.lock();
                try {
                    d h = d.h(1000);
                    if (h != null) {
                        boolean h2 = this.gx.h(sharedPreferences.getString("pk", null), h.getData());
                        SharedPreferences.Editor edit = sharedPreferences.edit();
                        if (h2) {
                            i3 = i4;
                            i2 = 0;
                        } else {
                            i2 = i4 + 1;
                            i3 = i2;
                        }
                        edit.putInt("t", i2);
                        edit.commit();
                        if (h2) {
                            d.a(h.T());
                            this.gA = Long.valueOf(currentTimeMillis);
                        } else if (i3 < 3) {
                            if (i == 0) {
                                this.gz.sendEmptyMessageDelayed(1, 600000);
                            } else {
                                this.gz.sendEmptyMessageDelayed(2, 600000);
                            }
                        }
                    }
                } finally {
                    d.unlock();
                }
            }
        }
    }
}
