package com.dianxinos.dxservices.a;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Handler;
import android.security.MessageDigest;
import com.dianxinos.dxservices.b.a;
import com.dianxinos.dxservices.b.c;
import com.dianxinos.dxservices.b.d;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import org.apache.commons.codec.binary.Base64;

/* compiled from: HwInfoService */
final class f {
    private final Runnable bW;
    private IntentFilter bX = null;
    /* access modifiers changed from: private */
    public int bY = 0;
    /* access modifiers changed from: private */
    public final Context mContext;
    /* access modifiers changed from: private */
    public final Handler mHandler;
    private BroadcastReceiver mReceiver = null;

    static /* synthetic */ int c(f fVar) {
        int i = fVar.bY;
        fVar.bY = i + 1;
        return i;
    }

    public f(Context context) {
        this.mContext = context;
        this.mHandler = new Handler();
        this.bW = new h(this);
    }

    public void V() {
        this.mHandler.post(this.bW);
        W();
    }

    private void W() {
        String string = this.mContext.getSharedPreferences("h", 1).getString("d", "");
        if (this.bX == null) {
            this.bX = new IntentFilter("android.intent.action.SIM_STATE_CHANGED");
            this.bX.addAction("android.intent.action.SERVICE_STATE");
            if (d.i(string)) {
                this.bX.addAction("android.net.wifi.WIFI_STATE_CHANGED");
            }
        }
        if (this.mReceiver == null) {
            this.mReceiver = new i(this);
        }
        this.mContext.registerReceiver(this.mReceiver, this.bX);
    }

    /* access modifiers changed from: private */
    public void X() {
        SharedPreferences sharedPreferences = this.mContext.getSharedPreferences("h", 1);
        String string = sharedPreferences.getString("c", "");
        String b = b(string, c.i(this.mContext));
        if (!a(string, b)) {
            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.putString("c", b);
            d.a(edit);
            if (!a(string, b)) {
                a.a(this.mContext).s();
            }
        }
    }

    /* access modifiers changed from: private */
    public void Y() {
        SharedPreferences sharedPreferences = this.mContext.getSharedPreferences("h", 1);
        String string = sharedPreferences.getString("m", "");
        String b = b(string, c.l(this.mContext));
        if (!a(string, b)) {
            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.putString("m", b);
            d.a(edit);
            a.a(this.mContext).r();
        }
    }

    /* access modifiers changed from: private */
    public void Z() {
        SharedPreferences sharedPreferences = this.mContext.getSharedPreferences("h", 1);
        String string = sharedPreferences.getString("d", "");
        String b = b(string, c.k(this.mContext));
        if (!a(string, b)) {
            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.putString("d", b);
            d.a(edit);
            a.a(this.mContext).r();
        }
    }

    /* access modifiers changed from: private */
    public void aa() {
        SharedPreferences sharedPreferences = this.mContext.getSharedPreferences("h", 1);
        String string = sharedPreferences.getString("b", "");
        String string2 = sharedPreferences.getString("c", "");
        String string3 = sharedPreferences.getString("m", "");
        String string4 = sharedPreferences.getString("a", "");
        String string5 = sharedPreferences.getString("d", "");
        String string6 = sharedPreferences.getString("k", "");
        String string7 = sharedPreferences.getString("g", "");
        String string8 = sharedPreferences.getString("h", "");
        String string9 = sharedPreferences.getString("i", "");
        String string10 = sharedPreferences.getString("j", "");
        String b = b(string, c.h(this.mContext));
        String b2 = b(string2, c.i(this.mContext));
        String b3 = b(string3, c.l(this.mContext));
        String b4 = b(string4, c.g(this.mContext));
        String b5 = b(string5, c.k(this.mContext));
        String b6 = b(string6, c.j(this.mContext));
        String b7 = b(string7, c.m(this.mContext));
        String b8 = b(string8, c.n(this.mContext));
        String b9 = b(string9, c.o(this.mContext));
        String b10 = b(string10, c.f(this.mContext));
        if (!a(string, b) || !a(string2, b2) || !a(string3, b3) || !a(string4, b4) || !a(string5, b5) || !a(string6, b6) || !a(string7, b7) || !a(string8, b8) || !a(string9, b9) || !a(string10, b10)) {
            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.putString("a", b4);
            edit.putString("b", b);
            edit.putString("c", b2);
            edit.putString("d", b5);
            edit.putString("k", b6);
            edit.putString("m", b3);
            edit.putString("g", b7);
            edit.putString("h", b8);
            edit.putString("i", b9);
            edit.putString("j", b10);
            d.a(edit);
            if (!a(string, b) || !a(string2, b2)) {
                a.a(this.mContext).s();
            }
            if (!a(string3, b3) || !a(string4, b4) || !a(string5, b5) || !a(string6, b6) || !a(string7, b7) || !a(string8, b8) || !a(string9, b9) || !a(string10, b10)) {
                a.a(this.mContext).r();
            }
        }
    }

    private boolean a(String str, String str2) {
        if (str == null) {
            return str2 == null;
        }
        return str.equals(str2);
    }

    private String b(String str, String str2) {
        if (d.j(str2)) {
            return c(str2);
        }
        return str;
    }

    private String c(String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(str.getBytes("UTF-8"));
            return new String(Base64.encodeBase64(instance.digest()), "UTF-8");
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
            return str;
        }
    }
}
