package com.dianxinos.dxservices.a;

import android.content.Context;
import java.util.HashMap;
import java.util.Map;

/* compiled from: EventStorage */
final class c {
    private final Map bf = new HashMap();
    private final Context mContext;

    public c(Context context) {
        this.mContext = context;
    }

    public a d(String str) {
        if (this.bf.containsKey(str)) {
            return (a) this.bf.get(str);
        }
        a aVar = new a(this.mContext, str);
        this.bf.put(str, aVar);
        return aVar;
    }
}
