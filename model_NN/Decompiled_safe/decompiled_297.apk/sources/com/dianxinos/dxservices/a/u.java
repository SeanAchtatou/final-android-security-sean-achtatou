package com.dianxinos.dxservices.a;

import java.math.BigDecimal;
import java.math.BigInteger;
import org.json.JSONObject;

/* compiled from: Constant */
public class u {
    public static boolean contains(int i) {
        return i == 0 || 1 == i || 2 == i || 3 == i || 4 == i || 5 == i || 6 == i || 7 == i || 8 == i || 9 == i || 10 == i;
    }

    public static int b(int i, Object obj) {
        if (1 == i && !(obj instanceof JSONObject) && !(obj instanceof Number)) {
            return -1;
        }
        if (obj instanceof String) {
            return 8;
        }
        if (obj instanceof byte[]) {
            return 10;
        }
        if (obj instanceof JSONObject) {
            return 9;
        }
        if (obj instanceof Byte) {
            return 0;
        }
        if (obj instanceof Short) {
            return 1;
        }
        if (obj instanceof Integer) {
            return 2;
        }
        if (obj instanceof Long) {
            return 3;
        }
        if (obj instanceof Float) {
            return 4;
        }
        if (obj instanceof Double) {
            return 5;
        }
        if (obj instanceof BigInteger) {
            return 6;
        }
        if (obj instanceof BigDecimal) {
            return 7;
        }
        return -1;
    }
}
