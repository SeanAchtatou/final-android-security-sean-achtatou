package com.dianxinos.dxservices.a;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/* compiled from: EventReportAlarm */
class o extends BroadcastReceiver {
    final /* synthetic */ n gp;

    o(n nVar) {
        this.gp = nVar;
    }

    public void onReceive(Context context, Intent intent) {
        if ("android.net.conn.CONNECTIVITY_CHANGE".equals(intent.getAction())) {
            this.gp.b(intent);
        } else if ("com.dianxinos.dxservices.stat.INTENT_ACTIONS_SEND_OFFLINE".equals(intent.getAction())) {
            this.gp.c(intent);
        }
    }
}
