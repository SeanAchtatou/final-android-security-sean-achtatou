package com.dianxinos.dxservices.a;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.util.Date;
import java.util.concurrent.locks.ReentrantLock;

/* compiled from: EventDatabase */
final class a extends SQLiteOpenHelper {
    private static final String[] M = {"a", "b", "c", "d", "f", "g", "h", "rowId"};
    private final String N;
    private final ReentrantLock O = new ReentrantLock();
    private Long P = null;
    private final Context mContext;
    private final String mName;

    public a(Context context, String str) {
        super(context, str, (SQLiteDatabase.CursorFactory) null, 5);
        this.mContext = context;
        this.mName = str;
        this.N = "CREATE TABLE " + str + " (" + "a" + " TEXT, " + "b" + " INTEGER, " + "c" + " INTEGER, " + "d" + " TEXT, " + "e" + " INTEGER, " + "f" + " INTEGER, " + "g" + " INTEGER, " + "h" + " TEXT, " + "i" + " INTEGER);";
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL(this.N);
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        if (Log.isLoggable("stat.EventDatabase", 4)) {
            Log.i("stat.EventDatabase", "onUpgrade from " + i + " to " + i2 + ".");
        }
        sQLiteDatabase.execSQL("DROP TABLE if exists " + this.mName);
        sQLiteDatabase.execSQL(this.N);
    }

    public boolean a(String str, int i, int i2, String str2, Date date, String str3, int i3) {
        Long a = a(str, str2, date, str3);
        if (a == null) {
            return b(str, i, i2, str2, date, str3, i3);
        }
        if (a.longValue() >= 0) {
            return a(a, str2, date);
        }
        return false;
    }

    private void a(Date date, String str) {
        long currentTimeMillis = System.currentTimeMillis();
        long longValue = this.P == null ? -1 : currentTimeMillis - this.P.longValue();
        if (longValue < 0 || longValue >= 3600000) {
            this.P = Long.valueOf(currentTimeMillis);
            String valueOf = String.valueOf(4);
            SQLiteDatabase writableDatabase = getWritableDatabase();
            writableDatabase.delete(this.mName, "c=? and h<>? and d<>'#'", new String[]{valueOf, str});
            String valueOf2 = String.valueOf((date.getTime() - 86400000) / 1000);
            writableDatabase.delete(this.mName, "c=? and h=? and d='0' and ( (g is not null and g<?) or (g is null and f<?) )", new String[]{valueOf, str, valueOf2, valueOf2});
            ContentValues contentValues = new ContentValues();
            contentValues.put("d", "#");
            writableDatabase.update(this.mName, contentValues, "c=? and h=? and d='1' and g<?", new String[]{valueOf, str, String.valueOf((date.getTime() / 1000) - 30)});
        }
    }

    private Long a(String str, String str2, Date date, String str3) {
        Cursor cursor;
        Long l;
        String str4;
        Long l2;
        a(date, str3);
        try {
            Cursor query = getReadableDatabase().query(this.mName, new String[]{"g", "d", "rowId"}, "a=? and h=? and d<>'#'", new String[]{str, str3}, null, null, "g desc, f desc");
            try {
                if (query.getCount() > 0) {
                    query.moveToFirst();
                    Long valueOf = query.isNull(0) ? null : Long.valueOf(query.getLong(0));
                    str4 = query.getString(1);
                    l2 = valueOf;
                    l = Long.valueOf(query.getLong(2));
                } else {
                    l = null;
                    str4 = null;
                    l2 = null;
                }
                if (query != null) {
                    query.close();
                }
                if (l == null) {
                    if ("0".equals(str2)) {
                        return null;
                    }
                    return -1L;
                } else if ("0".equals(str4)) {
                    if (!"0".equals(str2)) {
                        return l;
                    }
                    a(l);
                    return null;
                } else if (l2 == null || (date.getTime() / 1000) - l2.longValue() <= 30) {
                    return l;
                } else {
                    if (!"0".equals(str2)) {
                        return -1L;
                    }
                    a(l, "#", (Date) null);
                    return null;
                }
            } catch (Throwable th) {
                Throwable th2 = th;
                cursor = query;
                th = th2;
            }
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
        }
        if (cursor != null) {
            cursor.close();
        }
        throw th;
    }

    public boolean b(String str, int i, int i2, String str2, Date date, String str3, int i3) {
        SQLiteDatabase writableDatabase = getWritableDatabase();
        if (((long) a(writableDatabase)) > s.B(this.mContext)) {
            a(p());
        }
        long c = k.c(date.getTime());
        ContentValues contentValues = new ContentValues();
        contentValues.put("a", str);
        contentValues.put("b", Integer.valueOf(i));
        contentValues.put("c", Integer.valueOf(i2));
        contentValues.put("d", str2);
        contentValues.put("e", Long.valueOf(c));
        contentValues.put("f", Long.valueOf(k.b(date.getTime())));
        contentValues.put("h", str3);
        contentValues.put("i", Integer.valueOf(i3));
        return writableDatabase.insert(this.mName, null, contentValues) >= 0;
    }

    public boolean a(Long l, String str, Date date) {
        SQLiteDatabase writableDatabase = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("d", str);
        if (date != null) {
            contentValues.put("g", Long.valueOf(k.b(date.getTime())));
        }
        return ((long) writableDatabase.update(this.mName, contentValues, "rowId=?", new String[]{String.valueOf(l)})) > 0;
    }

    private int a(SQLiteDatabase sQLiteDatabase) {
        try {
            FileInputStream fileInputStream = new FileInputStream(new File(sQLiteDatabase.getPath()));
            int available = fileInputStream.available();
            fileInputStream.close();
            return available;
        } catch (Exception e) {
            return 0;
        }
    }

    public String a(String str, Date date, String str2) {
        Cursor cursor;
        String str3;
        try {
            Cursor query = getReadableDatabase().query(this.mName, new String[]{"d"}, "a=? and e=? and h=?", new String[]{str, String.valueOf(k.c(date.getTime())), str2}, null, null, null);
            try {
                if (query.getCount() > 0) {
                    query.moveToFirst();
                    str3 = query.getString(0);
                } else {
                    str3 = null;
                }
                if (query != null) {
                    query.close();
                }
                return str3;
            } catch (Throwable th) {
                Throwable th2 = th;
                cursor = query;
                th = th2;
                if (cursor != null) {
                    cursor.close();
                }
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
        }
    }

    public boolean b(String str, String str2, Date date, String str3) {
        SQLiteDatabase writableDatabase = getWritableDatabase();
        long c = k.c(date.getTime());
        ContentValues contentValues = new ContentValues();
        contentValues.put("d", str2);
        contentValues.put("g", Long.valueOf(k.b(date.getTime())));
        return ((long) writableDatabase.update(this.mName, contentValues, "a=? and e=? and h=?", new String[]{str, String.valueOf(c), str3})) > 0;
    }

    public void lock() {
        this.O.lock();
    }

    public void unlock() {
        this.O.unlock();
    }

    private void a(Long l) {
        if (l != null) {
            getWritableDatabase().delete(this.mName, "rowId=?", new String[]{String.valueOf(l)});
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0040  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.Long p() {
        /*
            r11 = this;
            r9 = 0
            android.database.sqlite.SQLiteDatabase r0 = r11.getReadableDatabase()
            java.lang.String r1 = r11.mName     // Catch:{ Exception -> 0x0033, all -> 0x003c }
            r2 = 1
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ Exception -> 0x0033, all -> 0x003c }
            r3 = 0
            java.lang.String r4 = "rowId"
            r2[r3] = r4     // Catch:{ Exception -> 0x0033, all -> 0x003c }
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            java.lang.String r8 = "1"
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5, r6, r7, r8)     // Catch:{ Exception -> 0x0033, all -> 0x003c }
            int r1 = r0.getCount()     // Catch:{ Exception -> 0x0049, all -> 0x0044 }
            if (r1 <= 0) goto L_0x004f
            r0.moveToFirst()     // Catch:{ Exception -> 0x0049, all -> 0x0044 }
            r1 = 0
            long r1 = r0.getLong(r1)     // Catch:{ Exception -> 0x0049, all -> 0x0044 }
            java.lang.Long r1 = java.lang.Long.valueOf(r1)     // Catch:{ Exception -> 0x0049, all -> 0x0044 }
        L_0x002c:
            if (r0 == 0) goto L_0x004d
            r0.close()
            r0 = r1
        L_0x0032:
            return r0
        L_0x0033:
            r0 = move-exception
            r0 = r9
        L_0x0035:
            if (r0 == 0) goto L_0x004b
            r0.close()
            r0 = r9
            goto L_0x0032
        L_0x003c:
            r0 = move-exception
            r1 = r9
        L_0x003e:
            if (r1 == 0) goto L_0x0043
            r1.close()
        L_0x0043:
            throw r0
        L_0x0044:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
            goto L_0x003e
        L_0x0049:
            r1 = move-exception
            goto L_0x0035
        L_0x004b:
            r0 = r9
            goto L_0x0032
        L_0x004d:
            r0 = r1
            goto L_0x0032
        L_0x004f:
            r1 = r9
            goto L_0x002c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.dianxinos.dxservices.a.a.p():java.lang.Long");
    }

    public void g(int i) {
        getWritableDatabase().delete(this.mName, "i=?", new String[]{String.valueOf(i)});
    }

    public void a(long j) {
        if (j <= 0) {
            a((String) null, (String[]) null);
            return;
        }
        a("rowId<=?", new String[]{String.valueOf(j)});
    }

    private void a(String str, String[] strArr) {
        String str2;
        SQLiteDatabase writableDatabase = getWritableDatabase();
        if (str == null) {
            str2 = "c<>4 or d='#'";
        } else {
            str2 = str + " and (c<>4 or d='#')";
        }
        writableDatabase.delete(this.mName, str2, strArr);
    }

    public d h(int i) {
        return a((String) null, (String[]) null, i <= 0 ? null : String.valueOf(i));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:40:?, code lost:
        a(java.lang.Long.valueOf(r8));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0103, code lost:
        r6.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0107, code lost:
        r6 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0108, code lost:
        r20 = r6;
        r6 = r5;
        r5 = r20;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0103  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0107 A[ExcHandler: all (r6v7 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:6:0x001c] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.dianxinos.dxservices.a.d a(java.lang.String r22, java.lang.String[] r23, java.lang.String r24) {
        /*
            r21 = this;
            android.database.sqlite.SQLiteDatabase r5 = r21.getReadableDatabase()
            r14 = 0
            if (r22 != 0) goto L_0x00bc
            java.lang.String r6 = "c<>4 or d='#'"
            r8 = r6
        L_0x000a:
            r0 = r21
            java.lang.String r0 = r0.mName     // Catch:{ Exception -> 0x010e, all -> 0x00ff }
            r6 = r0
            java.lang.String[] r7 = com.dianxinos.dxservices.a.a.M     // Catch:{ Exception -> 0x010e, all -> 0x00ff }
            r10 = 0
            r11 = 0
            r12 = 0
            r9 = r23
            r13 = r24
            android.database.Cursor r5 = r5.query(r6, r7, r8, r9, r10, r11, r12, r13)     // Catch:{ Exception -> 0x010e, all -> 0x00ff }
            org.json.JSONArray r6 = new org.json.JSONArray     // Catch:{ Exception -> 0x00f7, all -> 0x0107 }
            r6.<init>()     // Catch:{ Exception -> 0x00f7, all -> 0x0107 }
            r7 = 0
            r9 = 0
            int r10 = r5.getCount()     // Catch:{ Exception -> 0x00f7, all -> 0x0107 }
            if (r10 <= 0) goto L_0x00a7
            r5.moveToFirst()     // Catch:{ Exception -> 0x00f7, all -> 0x0107 }
            r7 = r9
        L_0x002e:
            r8 = 7
            long r8 = r5.getLong(r8)     // Catch:{ Exception -> 0x00f7, all -> 0x0107 }
            org.json.JSONObject r10 = new org.json.JSONObject     // Catch:{ Exception -> 0x00eb, all -> 0x0107 }
            r10.<init>()     // Catch:{ Exception -> 0x00eb, all -> 0x0107 }
            r11 = 0
            java.lang.String r11 = r5.getString(r11)     // Catch:{ Exception -> 0x00eb, all -> 0x0107 }
            r12 = 1
            int r12 = r5.getInt(r12)     // Catch:{ Exception -> 0x00eb, all -> 0x0107 }
            r13 = 2
            int r13 = r5.getInt(r13)     // Catch:{ Exception -> 0x00eb, all -> 0x0107 }
            r14 = 4
            if (r13 != r14) goto L_0x00d5
            java.lang.String r14 = ""
        L_0x004c:
            r15 = 4
            long r15 = r5.getLong(r15)     // Catch:{ Exception -> 0x00eb, all -> 0x0107 }
            r17 = 5
            r0 = r5
            r1 = r17
            boolean r17 = r0.isNull(r1)     // Catch:{ Exception -> 0x00eb, all -> 0x0107 }
            if (r17 == 0) goto L_0x00dc
            r17 = 0
        L_0x005e:
            r18 = 6
            r0 = r5
            r1 = r18
            java.lang.String r18 = r0.getString(r1)     // Catch:{ Exception -> 0x00eb, all -> 0x0107 }
            java.lang.String r19 = "a"
            r0 = r10
            r1 = r19
            r2 = r11
            r0.put(r1, r2)     // Catch:{ Exception -> 0x00eb, all -> 0x0107 }
            java.lang.String r11 = "b"
            r10.put(r11, r12)     // Catch:{ Exception -> 0x00eb, all -> 0x0107 }
            java.lang.String r11 = "c"
            r10.put(r11, r13)     // Catch:{ Exception -> 0x00eb, all -> 0x0107 }
            java.lang.String r11 = "f"
            r0 = r10
            r1 = r11
            r2 = r15
            r0.put(r1, r2)     // Catch:{ Exception -> 0x00eb, all -> 0x0107 }
            if (r17 == 0) goto L_0x008d
            java.lang.String r11 = "g"
            r0 = r10
            r1 = r11
            r2 = r17
            r0.put(r1, r2)     // Catch:{ Exception -> 0x00eb, all -> 0x0107 }
        L_0x008d:
            java.lang.String r11 = "d"
            r10.put(r11, r14)     // Catch:{ Exception -> 0x00eb, all -> 0x0107 }
            java.lang.String r11 = "h"
            r0 = r10
            r1 = r11
            r2 = r18
            r0.put(r1, r2)     // Catch:{ Exception -> 0x00eb, all -> 0x0107 }
            r6.put(r10)     // Catch:{ Exception -> 0x00eb, all -> 0x0107 }
            int r7 = r7 + 1
        L_0x00a0:
            boolean r10 = r5.moveToNext()     // Catch:{ Exception -> 0x00f7, all -> 0x0107 }
            if (r10 != 0) goto L_0x002e
            r7 = r8
        L_0x00a7:
            com.dianxinos.dxservices.a.d r9 = new com.dianxinos.dxservices.a.d     // Catch:{ Exception -> 0x00f7, all -> 0x0107 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x00f7, all -> 0x0107 }
            r0 = r9
            r1 = r21
            r2 = r6
            r3 = r7
            r0.<init>(r1, r2, r3)     // Catch:{ Exception -> 0x00f7, all -> 0x0107 }
            if (r5 == 0) goto L_0x00ba
            r5.close()
        L_0x00ba:
            r5 = r9
        L_0x00bb:
            return r5
        L_0x00bc:
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x010e, all -> 0x00ff }
            r6.<init>()     // Catch:{ Exception -> 0x010e, all -> 0x00ff }
            r0 = r6
            r1 = r22
            java.lang.StringBuilder r6 = r0.append(r1)     // Catch:{ Exception -> 0x010e, all -> 0x00ff }
            java.lang.String r7 = " and (c<>4 or d='#')"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x010e, all -> 0x00ff }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x010e, all -> 0x00ff }
            r8 = r6
            goto L_0x000a
        L_0x00d5:
            r14 = 3
            java.lang.String r14 = r5.getString(r14)     // Catch:{ Exception -> 0x00eb, all -> 0x0107 }
            goto L_0x004c
        L_0x00dc:
            r17 = 5
            r0 = r5
            r1 = r17
            long r17 = r0.getLong(r1)     // Catch:{ Exception -> 0x00eb, all -> 0x0107 }
            java.lang.Long r17 = java.lang.Long.valueOf(r17)     // Catch:{ Exception -> 0x00eb, all -> 0x0107 }
            goto L_0x005e
        L_0x00eb:
            r10 = move-exception
            java.lang.Long r10 = java.lang.Long.valueOf(r8)     // Catch:{ Exception -> 0x00f7, all -> 0x0107 }
            r0 = r21
            r1 = r10
            r0.a(r1)     // Catch:{ Exception -> 0x00f7, all -> 0x0107 }
            goto L_0x00a0
        L_0x00f7:
            r6 = move-exception
        L_0x00f8:
            if (r5 == 0) goto L_0x00fd
            r5.close()
        L_0x00fd:
            r5 = 0
            goto L_0x00bb
        L_0x00ff:
            r5 = move-exception
            r6 = r14
        L_0x0101:
            if (r6 == 0) goto L_0x0106
            r6.close()
        L_0x0106:
            throw r5
        L_0x0107:
            r6 = move-exception
            r20 = r6
            r6 = r5
            r5 = r20
            goto L_0x0101
        L_0x010e:
            r5 = move-exception
            r5 = r14
            goto L_0x00f8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.dianxinos.dxservices.a.a.a(java.lang.String, java.lang.String[], java.lang.String):com.dianxinos.dxservices.a.d");
    }

    public boolean isEmpty() {
        Cursor query = getReadableDatabase().query(this.mName, null, "c<>4 or d='#'", null, null, null, null, "1");
        int count = query.getCount();
        query.close();
        return count == 0;
    }
}
