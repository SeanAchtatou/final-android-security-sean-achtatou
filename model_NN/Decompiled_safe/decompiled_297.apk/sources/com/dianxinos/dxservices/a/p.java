package com.dianxinos.dxservices.a;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import java.util.Calendar;
import java.util.Date;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: Event */
public class p {
    private final Date bV;
    private final int gq;
    private final int gr;
    private final int gs;
    private final Object gt;
    private final String gu;
    private final int priority;
    private final String tag;

    public p(int i, int i2, int i3, String str, int i4, Object obj, String str2) {
        this.gq = i;
        this.gr = i2;
        this.gs = i3;
        this.tag = str;
        this.priority = i4;
        this.gt = obj;
        this.gu = str2;
        Calendar instance = Calendar.getInstance();
        instance.add(14, k.eW.getOffset(instance.getTimeInMillis()) - instance.getTimeZone().getOffset(instance.getTimeInMillis()));
        this.bV = instance.getTime();
    }

    public int bb() {
        return this.gq;
    }

    public int bc() {
        return this.gr;
    }

    public int bd() {
        return this.gs;
    }

    public String getTag() {
        return this.tag;
    }

    public int getPriority() {
        return this.priority;
    }

    public Object be() {
        return this.gt;
    }

    public String bf() {
        return this.gu;
    }

    public Date getTime() {
        return this.bV;
    }

    public String toString() {
        return "[" + this.gq + ":" + this.gr + ":" + this.gs + ":" + this.tag + ":" + this.bV + ":" + this.gu + ":" + this.gt + "]";
    }

    public static String d(Context context, String str) {
        String packageName = context.getPackageName();
        String str2 = null;
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(packageName, 0);
            if (packageInfo != null) {
                str2 = packageInfo.versionName;
            }
        } catch (PackageManager.NameNotFoundException e) {
        }
        return a(packageName, str2, str);
    }

    public static String a(String str, String str2, String str3) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("pkg", str);
            jSONObject.put("ver", str2);
            jSONObject.put("key", str3);
        } catch (JSONException e) {
        }
        return jSONObject.toString();
    }
}
