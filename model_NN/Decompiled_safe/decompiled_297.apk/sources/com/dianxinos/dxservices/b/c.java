package com.dianxinos.dxservices.b;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.os.SystemProperties;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import java.util.Hashtable;
import java.util.Map;

/* compiled from: HwInfoManager */
public final class c {
    public static Map e(Context context) {
        Hashtable hashtable = new Hashtable();
        String g = g(context);
        String h = h(context);
        String i = i(context);
        String k = k(context);
        String j = j(context);
        String l = l(context);
        String m = m(context);
        String n = n(context);
        String o = o(context);
        String f = f(context);
        a(hashtable, g, "a");
        a(hashtable, h, "b");
        a(hashtable, i, "c");
        a(hashtable, k, "d");
        a(hashtable, j, "k");
        a(hashtable, l, "m");
        a(hashtable, m, "g");
        a(hashtable, n, "h");
        a(hashtable, o, "i");
        a(hashtable, f, "j");
        return hashtable;
    }

    private static void a(Map map, String str, String str2) {
        if (d.j(str)) {
            map.put(str2, str);
        }
    }

    public static String f(Context context) {
        try {
            return Build.MANUFACTURER;
        } catch (Exception e) {
            return "";
        }
    }

    public static String g(Context context) {
        try {
            return Build.MODEL;
        } catch (Exception e) {
            return "";
        }
    }

    public static String h(Context context) {
        try {
            String str = SystemProperties.get("ro.serialno");
            if (str.length() > 0) {
                return str;
            }
            String str2 = SystemProperties.get("ro.hw.dxos.SN");
            if (str2.length() > 0) {
            }
            return str2;
        } catch (Exception e) {
            return "";
        }
    }

    public static String i(Context context) {
        try {
            return ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        } catch (Exception e) {
            return "";
        }
    }

    public static String j(Context context) {
        try {
            return Build.VERSION.RELEASE;
        } catch (Exception e) {
            return "";
        }
    }

    public static String k(Context context) {
        try {
            WifiInfo connectionInfo = ((WifiManager) context.getSystemService("wifi")).getConnectionInfo();
            if (connectionInfo != null) {
                return connectionInfo.getMacAddress();
            }
            return null;
        } catch (Exception e) {
            return "";
        }
    }

    public static String l(Context context) {
        try {
            return ((TelephonyManager) context.getSystemService("phone")).getNetworkOperator();
        } catch (Exception e) {
            return "";
        }
    }

    public static String m(Context context) {
        try {
            StatFs statFs = new StatFs(Environment.getDataDirectory().getPath());
            return Long.toString(((long) statFs.getBlockCount()) * ((long) statFs.getBlockSize()));
        } catch (Exception e) {
            return "";
        }
    }

    public static String n(Context context) {
        try {
            Display defaultDisplay = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
            DisplayMetrics displayMetrics = new DisplayMetrics();
            defaultDisplay.getMetrics(displayMetrics);
            return Integer.toString(displayMetrics.densityDpi);
        } catch (Exception e) {
            return "";
        }
    }

    public static String o(Context context) {
        try {
            Display defaultDisplay = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
            DisplayMetrics displayMetrics = new DisplayMetrics();
            defaultDisplay.getMetrics(displayMetrics);
            if (displayMetrics.widthPixels < displayMetrics.heightPixels) {
                return displayMetrics.widthPixels + "*" + displayMetrics.heightPixels;
            }
            return displayMetrics.heightPixels + "*" + displayMetrics.widthPixels;
        } catch (Exception e) {
            return "";
        }
    }
}
