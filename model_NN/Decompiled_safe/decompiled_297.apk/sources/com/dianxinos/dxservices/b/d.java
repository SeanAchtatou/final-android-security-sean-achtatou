package com.dianxinos.dxservices.b;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import java.io.IOException;
import java.io.Reader;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: Helper */
public class d {
    public static JSONObject a(String str, Number number) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put(str, number);
        } catch (JSONException e) {
        }
        return jSONObject;
    }

    public static boolean p(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager == null) {
            return false;
        }
        NetworkInfo networkInfo = connectivityManager.getNetworkInfo(1);
        return networkInfo != null && networkInfo.getState() == NetworkInfo.State.CONNECTED;
    }

    public static boolean q(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager == null) {
            return false;
        }
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.getState() == NetworkInfo.State.CONNECTED;
    }

    public static void a(Reader reader) {
        if (reader != null) {
            try {
                reader.close();
            } catch (IOException e) {
            }
        }
    }

    public static void a(SharedPreferences.Editor editor) {
        if (editor != null) {
            try {
                editor.commit();
            } catch (Exception e) {
            }
        }
    }

    public static boolean i(String str) {
        return str == null || str.length() == 0;
    }

    public static boolean j(String str) {
        return str != null && str.length() > 0;
    }
}
