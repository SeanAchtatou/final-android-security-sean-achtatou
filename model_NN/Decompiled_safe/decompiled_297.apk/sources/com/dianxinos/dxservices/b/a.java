package com.dianxinos.dxservices.b;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.SystemProperties;
import android.security.MessageDigest;
import com.dianxinos.dxservices.a.r;
import com.dianxinos.dxservices.a.s;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Map;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

/* compiled from: TokenManager */
public final class a {
    private static a V = null;
    private String T = "";
    private boolean U = false;
    private final long W = System.currentTimeMillis();
    private Context mContext;
    private Object mLock = new Object();

    private a(Context context) {
        this.mContext = context.getApplicationContext();
        u();
    }

    public static a a(Context context) {
        synchronized (a.class) {
            if (V == null) {
                V = new a(context);
            }
        }
        return V;
    }

    public String getToken() {
        String str;
        synchronized (this.mLock) {
            if (this.T.length() == 0) {
                this.T = t();
                if (this.T.length() != 0) {
                    v();
                }
            }
            str = this.T;
        }
        return str;
    }

    public String q() {
        String str;
        synchronized (this.mLock) {
            if (this.T.length() == 0) {
                this.T = t();
                if (this.T.length() != 0) {
                    v();
                }
            }
            if (this.T.length() != 0 && !this.U) {
                this.U = b(this.T);
                if (this.U) {
                    w();
                }
            }
            str = this.T;
        }
        return str;
    }

    public void r() {
        synchronized (this.mLock) {
            this.U = false;
            w();
        }
    }

    public void s() {
        synchronized (this.mLock) {
            this.T = t();
            v();
            this.U = false;
            w();
        }
    }

    private boolean b(String str) {
        String str2;
        if (!d.q(this.mContext)) {
            return false;
        }
        try {
            String str3 = SystemProperties.get("ro.dianxinos.core.tokurl");
            if (d.i(str3)) {
                str2 = "http://pasta.dianxinos.com/api/tokens";
            } else {
                str2 = str3;
            }
            JSONObject jSONObject = new JSONObject();
            for (Map.Entry entry : c.e(this.mContext).entrySet()) {
                jSONObject.put((String) entry.getKey(), entry.getValue());
            }
            ArrayList arrayList = new ArrayList();
            arrayList.add(new BasicNameValuePair("token", str));
            String z = s.z(this.mContext);
            String j = r.j(s.bs(), z);
            arrayList.add(new BasicNameValuePair("pu", z));
            arrayList.add(new BasicNameValuePair("ci", j));
            arrayList.add(new BasicNameValuePair("hw", r.a(jSONObject.toString(), s.bt())));
            return new b(this.mContext, str2, "DXServiceToken", "TokenManager").a(arrayList);
        } catch (Exception e) {
            return false;
        }
    }

    private String t() {
        String i = c.i(this.mContext);
        String h = c.h(this.mContext);
        if (!d.i(i)) {
            return c(i + "_" + h);
        }
        if (System.currentTimeMillis() > this.W + 600000) {
            String k = c.k(this.mContext);
            if (d.j(k)) {
                return c(k + "_" + h);
            }
        }
        return "";
    }

    private void u() {
        SharedPreferences sharedPreferences = this.mContext.getSharedPreferences("utils", 1);
        this.T = sharedPreferences.getString("tm", "");
        this.U = sharedPreferences.getBoolean("st", false);
    }

    private void v() {
        SharedPreferences.Editor edit = this.mContext.getSharedPreferences("utils", 1).edit();
        edit.putString("tm", this.T);
        d.a(edit);
    }

    private void w() {
        SharedPreferences.Editor edit = this.mContext.getSharedPreferences("utils", 1).edit();
        edit.putBoolean("st", this.U);
        d.a(edit);
    }

    private String c(String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(str.getBytes("UTF-8"));
            return new String(Base64.encodeBase64(instance.digest()), "UTF-8");
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
            return str;
        }
    }
}
