package com.dianxinos.dxservices.b;

import android.content.Context;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
import org.apache.http.client.methods.HttpPost;

/* compiled from: HttpPostHelper */
public class b {
    private String bg;
    private HttpPost bh;
    private Context mContext;
    private String mLogTag;

    public b(Context context, String str, String str2, String str3) {
        this.mContext = context;
        this.bg = str2;
        this.mLogTag = str3;
        this.bh = new HttpPost(str);
    }

    public void addHeader(String str, String str2) {
        try {
            this.bh.addHeader(str, URLEncoder.encode(str2, "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            this.bh.addHeader(str, str2);
        }
    }

    public boolean a(List list) {
        return a(list, null);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0079, code lost:
        if (r12.isEmpty() != false) goto L_0x007b;
     */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x00d6  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(java.util.List r12, java.util.List r13) {
        /*
            r11 = this;
            r9 = 0
            r8 = 1
            r7 = 0
            java.lang.String r0 = "UTF-8"
            org.apache.http.client.methods.HttpPost r0 = r11.bh     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            org.apache.http.params.HttpParams r0 = r0.getParams()     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            java.lang.String r1 = "http.protocol.content-charset"
            java.lang.String r2 = "UTF-8"
            r0.setParameter(r1, r2)     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            if (r13 != 0) goto L_0x0073
            org.apache.http.client.methods.HttpPost r0 = r11.bh     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            org.apache.http.client.entity.UrlEncodedFormEntity r1 = new org.apache.http.client.entity.UrlEncodedFormEntity     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            java.lang.String r2 = "UTF-8"
            r1.<init>(r12, r2)     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            r0.setEntity(r1)     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
        L_0x0020:
            r1 = -1
            android.content.Context r0 = r11.mContext     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            java.lang.String r2 = "connectivity"
            java.lang.Object r0 = r0.getSystemService(r2)     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            android.net.ConnectivityManager r0 = (android.net.ConnectivityManager) r0     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            if (r0 != 0) goto L_0x012a
            r0 = r9
        L_0x002e:
            if (r0 == 0) goto L_0x0157
            int r0 = r0.getType()     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            if (r0 == r8) goto L_0x0157
            android.content.Context r0 = r11.mContext     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            java.lang.String r0 = android.net.Proxy.getHost(r0)     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            android.content.Context r1 = r11.mContext     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            int r1 = android.net.Proxy.getPort(r1)     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            if (r0 == 0) goto L_0x0130
            if (r1 <= 0) goto L_0x0130
            r2 = r8
        L_0x0047:
            r10 = r2
            r2 = r0
            r0 = r10
        L_0x004a:
            if (r0 == 0) goto L_0x005a
            org.apache.http.HttpHost r0 = new org.apache.http.HttpHost     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            r0.<init>(r2, r1)     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            org.apache.http.client.methods.HttpPost r1 = r11.bh     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            org.apache.http.params.HttpParams r1 = r1.getParams()     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            org.apache.http.conn.params.ConnRouteParams.setDefaultProxy(r1, r0)     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
        L_0x005a:
            java.lang.String r0 = r11.bg     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            android.net.http.AndroidHttpClient r0 = android.net.http.AndroidHttpClient.newInstance(r0)     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            org.apache.http.client.methods.HttpPost r1 = r11.bh     // Catch:{ Exception -> 0x0154, all -> 0x014f }
            org.apache.http.HttpResponse r1 = r0.execute(r1)     // Catch:{ Exception -> 0x0154, all -> 0x014f }
            org.apache.http.StatusLine r2 = r1.getStatusLine()     // Catch:{ Exception -> 0x0154, all -> 0x014f }
            if (r2 != 0) goto L_0x0133
            if (r0 == 0) goto L_0x0071
            r0.close()
        L_0x0071:
            r0 = r7
        L_0x0072:
            return r0
        L_0x0073:
            if (r12 == 0) goto L_0x007b
            boolean r0 = r12.isEmpty()     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            if (r0 == 0) goto L_0x00a0
        L_0x007b:
            int r0 = r13.size()     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            if (r0 != r8) goto L_0x00a0
            org.apache.http.client.methods.HttpPost r1 = r11.bh     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            org.apache.http.entity.ByteArrayEntity r2 = new org.apache.http.entity.ByteArrayEntity     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            r0 = 0
            java.lang.Object r0 = r13.get(r0)     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            android.util.Pair r0 = (android.util.Pair) r0     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            java.lang.Object r0 = r0.second     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            byte[] r0 = (byte[]) r0     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            r2.<init>(r0)     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            r1.setEntity(r2)     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            goto L_0x0020
        L_0x0097:
            r0 = move-exception
            r0 = r9
        L_0x0099:
            if (r0 == 0) goto L_0x009e
            r0.close()
        L_0x009e:
            r0 = r7
            goto L_0x0072
        L_0x00a0:
            java.util.ArrayList r3 = new java.util.ArrayList     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            if (r12 != 0) goto L_0x00da
            r0 = r7
        L_0x00a5:
            int r1 = r13.size()     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            int r0 = r0 + r1
            r3.<init>(r0)     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            if (r12 == 0) goto L_0x00df
            java.util.Iterator r1 = r12.iterator()     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
        L_0x00b3:
            boolean r0 = r1.hasNext()     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            if (r0 == 0) goto L_0x00df
            java.lang.Object r0 = r1.next()     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            org.apache.http.NameValuePair r0 = (org.apache.http.NameValuePair) r0     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            com.android.internal.http.multipart.StringPart r2 = new com.android.internal.http.multipart.StringPart     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            java.lang.String r4 = r0.getName()     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            java.lang.String r0 = r0.getValue()     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            java.lang.String r5 = "UTF-8"
            r2.<init>(r4, r0, r5)     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            r3.add(r2)     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            goto L_0x00b3
        L_0x00d2:
            r0 = move-exception
            r1 = r9
        L_0x00d4:
            if (r1 == 0) goto L_0x00d9
            r1.close()
        L_0x00d9:
            throw r0
        L_0x00da:
            int r0 = r12.size()     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            goto L_0x00a5
        L_0x00df:
            java.util.Iterator r4 = r13.iterator()     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
        L_0x00e3:
            boolean r0 = r4.hasNext()     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            if (r0 == 0) goto L_0x010c
            java.lang.Object r0 = r4.next()     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            android.util.Pair r0 = (android.util.Pair) r0     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            com.android.internal.http.multipart.FilePart r5 = new com.android.internal.http.multipart.FilePart     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            java.lang.Object r1 = r0.first     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            com.android.internal.http.multipart.ByteArrayPartSource r6 = new com.android.internal.http.multipart.ByteArrayPartSource     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            java.lang.Object r2 = r0.first     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            java.lang.Object r0 = r0.second     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            byte[] r0 = (byte[]) r0     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            r6.<init>(r2, r0)     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            r0 = 0
            java.lang.String r2 = "UTF-8"
            r5.<init>(r1, r6, r0, r2)     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            r3.add(r5)     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            goto L_0x00e3
        L_0x010c:
            org.apache.http.client.methods.HttpPost r1 = r11.bh     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            com.android.internal.http.multipart.MultipartEntity r2 = new com.android.internal.http.multipart.MultipartEntity     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            int r0 = r3.size()     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            com.android.internal.http.multipart.Part[] r0 = new com.android.internal.http.multipart.Part[r0]     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            java.lang.Object[] r0 = r3.toArray(r0)     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            com.android.internal.http.multipart.Part[] r0 = (com.android.internal.http.multipart.Part[]) r0     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            org.apache.http.client.methods.HttpPost r3 = r11.bh     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            org.apache.http.params.HttpParams r3 = r3.getParams()     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            r2.<init>(r0, r3)     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            r1.setEntity(r2)     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            goto L_0x0020
        L_0x012a:
            android.net.NetworkInfo r0 = r0.getActiveNetworkInfo()     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            goto L_0x002e
        L_0x0130:
            r2 = r7
            goto L_0x0047
        L_0x0133:
            org.apache.http.StatusLine r1 = r1.getStatusLine()     // Catch:{ Exception -> 0x0154, all -> 0x014f }
            int r1 = r1.getStatusCode()     // Catch:{ Exception -> 0x0154, all -> 0x014f }
            r2 = 200(0xc8, float:2.8E-43)
            if (r1 == r2) goto L_0x0147
            if (r0 == 0) goto L_0x0144
            r0.close()
        L_0x0144:
            r0 = r7
            goto L_0x0072
        L_0x0147:
            if (r0 == 0) goto L_0x014c
            r0.close()
        L_0x014c:
            r0 = r8
            goto L_0x0072
        L_0x014f:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
            goto L_0x00d4
        L_0x0154:
            r1 = move-exception
            goto L_0x0099
        L_0x0157:
            r0 = r7
            r2 = r9
            goto L_0x004a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.dianxinos.dxservices.b.b.a(java.util.List, java.util.List):boolean");
    }
}
