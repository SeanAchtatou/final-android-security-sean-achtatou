package com.dianxinos.dxservices.b;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Proxy;
import android.net.http.AndroidHttpClient;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.params.ConnRouteParams;
import org.apache.http.message.BasicHeader;

/* compiled from: Http */
public class j {
    /* access modifiers changed from: private */
    public static final Header jH = new BasicHeader("Accept-Encoding", "gzip");
    private final Context mContext;

    public j(Context context) {
        this.mContext = context;
    }

    public void a(String str, g gVar) {
        new f(this, str, new e(this, gVar)).start();
    }

    /* access modifiers changed from: private */
    public void a(HttpUriRequest httpUriRequest) {
        String host;
        int port;
        ConnectivityManager connectivityManager = (ConnectivityManager) this.mContext.getSystemService("connectivity");
        NetworkInfo activeNetworkInfo = connectivityManager == null ? null : connectivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo != null && activeNetworkInfo.getType() != 1 && (host = Proxy.getHost(this.mContext)) != null && (port = Proxy.getPort(this.mContext)) >= 0) {
            ConnRouteParams.setDefaultProxy(httpUriRequest.getParams(), new HttpHost(host, port));
        }
    }

    /* access modifiers changed from: private */
    public static String a(HttpResponse httpResponse) {
        HttpEntity entity = httpResponse.getEntity();
        return a(a(entity.getContentEncoding(), "gzip") ? AndroidHttpClient.getUngzippedContent(entity) : entity.getContent());
    }

    private static String a(InputStream inputStream) {
        BufferedReader bufferedReader;
        Throwable th;
        try {
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            try {
                StringBuilder sb = new StringBuilder();
                while (true) {
                    String readLine = bufferedReader.readLine();
                    if (readLine == null) {
                        break;
                    }
                    if (sb.length() != 0) {
                        sb.append(13);
                    }
                    sb.append(readLine);
                }
                String sb2 = sb.toString();
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
                return sb2;
            } catch (Throwable th2) {
                th = th2;
            }
        } catch (Throwable th3) {
            Throwable th4 = th3;
            bufferedReader = null;
            th = th4;
            if (bufferedReader != null) {
                bufferedReader.close();
            }
            throw th;
        }
    }

    private static boolean a(Header header, String str) {
        if (header == null || str == null) {
            return false;
        }
        for (HeaderElement name : header.getElements()) {
            if (str.equals(name.getName())) {
                return true;
            }
        }
        return false;
    }
}
