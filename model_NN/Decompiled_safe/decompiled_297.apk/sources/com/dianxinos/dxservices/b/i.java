package com.dianxinos.dxservices.b;

import java.util.HashMap;
import java.util.Map;

/* compiled from: PeriodExecutor */
public abstract class i {
    private static Map hE = new HashMap();
    private String key;
    private long period;

    public abstract void run();

    public i(String str, long j) {
        this.key = str;
        this.period = j;
    }

    public void start() {
        long currentTimeMillis = System.currentTimeMillis();
        Long l = (Long) hE.get(this.key);
        long longValue = currentTimeMillis - (l == null ? 0 : l.longValue());
        if (longValue < 0) {
            hE.put(this.key, Long.valueOf(currentTimeMillis));
        } else if (longValue >= this.period) {
            hE.put(this.key, Long.valueOf(currentTimeMillis));
            new h(this).start();
        }
    }
}
