package com.dianxinos.dxservices.b;

import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ResponseHandler;

/* compiled from: Http */
class e implements ResponseHandler {
    final /* synthetic */ g eP;
    final /* synthetic */ j eQ;

    e(j jVar, g gVar) {
        this.eQ = jVar;
        this.eP = gVar;
    }

    public String handleResponse(HttpResponse httpResponse) {
        StatusLine statusLine = httpResponse.getStatusLine();
        if (statusLine == null) {
            return null;
        }
        int statusCode = statusLine.getStatusCode();
        String b = j.a(httpResponse);
        this.eP.c(statusCode, b);
        return b;
    }
}
