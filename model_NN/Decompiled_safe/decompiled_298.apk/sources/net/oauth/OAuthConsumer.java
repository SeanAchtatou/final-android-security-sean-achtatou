package net.oauth;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class OAuthConsumer implements Serializable {
    public static final String ACCEPT_ENCODING = "HTTP.header.Accept-Encoding";
    public static final String ACCESSOR_SECRET = "oauth_accessor_secret";
    private static final long serialVersionUID = -2258581186977818580L;
    public final String callbackURL;
    public final String consumerKey;
    public final String consumerSecret;
    private final Map<String, Object> properties = new HashMap();
    public final OAuthServiceProvider serviceProvider;

    public OAuthConsumer(String callbackURL2, String consumerKey2, String consumerSecret2, OAuthServiceProvider serviceProvider2) {
        this.callbackURL = callbackURL2;
        this.consumerKey = consumerKey2;
        this.consumerSecret = consumerSecret2;
        this.serviceProvider = serviceProvider2;
    }

    public Object getProperty(String name) {
        return this.properties.get(name);
    }

    public void setProperty(String name, Object value) {
        this.properties.put(name, value);
    }
}
