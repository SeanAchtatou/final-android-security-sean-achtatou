package net.oauth;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import net.oauth.OAuth;
import net.oauth.http.HttpMessage;
import net.oauth.signature.OAuthSignatureMethod;

public class OAuthMessage {
    private static final Pattern AUTHORIZATION = Pattern.compile("\\s*(\\w*)\\s+(.*)");
    public static final String AUTH_SCHEME = "OAuth";
    public static final String DELETE = "DELETE";
    public static final String GET = "GET";
    private static final Pattern NVP = Pattern.compile("(\\S*)\\s*\\=\\s*\"([^\"]*)\"");
    public static final String POST = "POST";
    public static final String PUT = "PUT";
    public String URL;
    private final InputStream bodyAsStream;
    private final List<Map.Entry<String, String>> headers;
    public String method;
    private Map<String, String> parameterMap;
    private final List<Map.Entry<String, String>> parameters;
    private boolean parametersAreComplete;

    public OAuthMessage(String method2, String URL2, Collection<? extends Map.Entry> parameters2) {
        this(method2, URL2, parameters2, null);
    }

    public OAuthMessage(String method2, String URL2, Collection<? extends Map.Entry> parameters2, InputStream bodyAsStream2) {
        this.parametersAreComplete = false;
        this.headers = new ArrayList();
        this.method = method2;
        this.URL = URL2;
        this.bodyAsStream = bodyAsStream2;
        if (parameters2 == null) {
            this.parameters = new ArrayList();
            return;
        }
        this.parameters = new ArrayList(parameters2.size());
        for (Map.Entry p : parameters2) {
            this.parameters.add(new OAuth.Parameter(toString(p.getKey()), toString(p.getValue())));
        }
    }

    public String toString() {
        return "OAuthMessage(" + this.method + ", " + this.URL + ", " + this.parameters + ")";
    }

    private void beforeGetParameter() throws IOException {
        if (!this.parametersAreComplete) {
            completeParameters();
            this.parametersAreComplete = true;
        }
    }

    /* access modifiers changed from: protected */
    public void completeParameters() throws IOException {
    }

    public List<Map.Entry<String, String>> getParameters() throws IOException {
        beforeGetParameter();
        return Collections.unmodifiableList(this.parameters);
    }

    public void addParameter(String key, String value) {
        addParameter(new OAuth.Parameter(key, value));
    }

    public void addParameter(Map.Entry<String, String> parameter) {
        this.parameters.add(parameter);
        this.parameterMap = null;
    }

    public void addParameters(Collection<? extends Map.Entry<String, String>> parameters2) {
        this.parameters.addAll(parameters2);
        this.parameterMap = null;
    }

    public String getParameter(String name) throws IOException {
        return getParameterMap().get(name);
    }

    public String getConsumerKey() throws IOException {
        return getParameter(OAuth.OAUTH_CONSUMER_KEY);
    }

    public String getToken() throws IOException {
        return getParameter(OAuth.OAUTH_TOKEN);
    }

    public String getSignatureMethod() throws IOException {
        return getParameter(OAuth.OAUTH_SIGNATURE_METHOD);
    }

    public String getSignature() throws IOException {
        return getParameter(OAuth.OAUTH_SIGNATURE);
    }

    /* access modifiers changed from: protected */
    public Map<String, String> getParameterMap() throws IOException {
        beforeGetParameter();
        if (this.parameterMap == null) {
            this.parameterMap = OAuth.newMap(this.parameters);
        }
        return this.parameterMap;
    }

    public String getBodyType() {
        return getHeader(HttpMessage.CONTENT_TYPE);
    }

    public String getBodyEncoding() {
        return HttpMessage.DEFAULT_CHARSET;
    }

    public final String getHeader(String name) {
        String value = null;
        for (Map.Entry<String, String> header : getHeaders()) {
            if (name.equalsIgnoreCase((String) header.getKey())) {
                value = header.getValue();
            }
        }
        return value;
    }

    public final List<Map.Entry<String, String>> getHeaders() {
        return this.headers;
    }

    public final String readBodyAsString() throws IOException {
        return readAll(getBodyAsStream(), getBodyEncoding());
    }

    public InputStream getBodyAsStream() throws IOException {
        return this.bodyAsStream;
    }

    public Map<String, Object> getDump() throws IOException {
        Map<String, Object> into = new HashMap<>();
        dump(into);
        return into;
    }

    /* access modifiers changed from: protected */
    public void dump(Map<String, Object> into) throws IOException {
        into.put(OAuthProblemException.URL, this.URL);
        if (this.parametersAreComplete) {
            try {
                into.putAll(getParameterMap());
            } catch (Exception e) {
            }
        }
    }

    public void requireParameters(String... names) throws OAuthProblemException, IOException {
        Set<String> present = getParameterMap().keySet();
        List<String> absent = new ArrayList<>();
        for (String required : names) {
            if (!present.contains(required)) {
                absent.add(required);
            }
        }
        if (!absent.isEmpty()) {
            OAuthProblemException problem = new OAuthProblemException(OAuth.Problems.PARAMETER_ABSENT);
            problem.setParameter(OAuth.Problems.OAUTH_PARAMETERS_ABSENT, OAuth.percentEncode(absent));
            throw problem;
        }
    }

    public void addRequiredParameters(OAuthAccessor accessor) throws OAuthException, IOException, URISyntaxException {
        Map<String, String> pMap = OAuth.newMap(this.parameters);
        if (pMap.get(OAuth.OAUTH_TOKEN) == null && accessor.accessToken != null) {
            addParameter(OAuth.OAUTH_TOKEN, accessor.accessToken);
        }
        OAuthConsumer consumer = accessor.consumer;
        if (pMap.get(OAuth.OAUTH_CONSUMER_KEY) == null) {
            addParameter(OAuth.OAUTH_CONSUMER_KEY, consumer.consumerKey);
        }
        if (pMap.get(OAuth.OAUTH_SIGNATURE_METHOD) == null) {
            String signatureMethod = (String) consumer.getProperty(OAuth.OAUTH_SIGNATURE_METHOD);
            if (signatureMethod == null) {
                signatureMethod = OAuth.HMAC_SHA1;
            }
            addParameter(OAuth.OAUTH_SIGNATURE_METHOD, signatureMethod);
        }
        if (pMap.get(OAuth.OAUTH_TIMESTAMP) == null) {
            addParameter(OAuth.OAUTH_TIMESTAMP, new StringBuilder(String.valueOf(System.currentTimeMillis() / 1000)).toString());
        }
        if (pMap.get(OAuth.OAUTH_NONCE) == null) {
            addParameter(OAuth.OAUTH_NONCE, new StringBuilder(String.valueOf(System.nanoTime())).toString());
        }
        if (pMap.get(OAuth.OAUTH_VERSION) == null) {
            addParameter(OAuth.OAUTH_VERSION, OAuth.VERSION_1_0);
        }
        sign(accessor);
    }

    public void sign(OAuthAccessor accessor) throws IOException, OAuthException, URISyntaxException {
        OAuthSignatureMethod.newSigner(this, accessor).sign(this);
    }

    public String getAuthorizationHeader(String realm) throws IOException {
        StringBuilder into = new StringBuilder();
        if (realm != null) {
            into.append(" realm=\"").append(OAuth.percentEncode(realm)).append('\"');
        }
        beforeGetParameter();
        if (this.parameters != null) {
            for (Map.Entry parameter : this.parameters) {
                String name = toString(parameter.getKey());
                if (name.startsWith("oauth_")) {
                    if (into.length() > 0) {
                        into.append(",");
                    }
                    into.append(" ");
                    into.append(OAuth.percentEncode(name)).append("=\"");
                    into.append(OAuth.percentEncode(toString(parameter.getValue()))).append('\"');
                }
            }
        }
        return "OAuth" + into.toString();
    }

    public static String readAll(InputStream from, String encoding) throws IOException {
        if (from == null) {
            return null;
        }
        try {
            StringBuilder into = new StringBuilder();
            Reader r = new InputStreamReader(from, encoding);
            char[] s = new char[512];
            while (true) {
                int n = r.read(s);
                if (n <= 0) {
                    return into.toString();
                }
                into.append(s, 0, n);
            }
        } finally {
            from.close();
        }
    }

    public static List<OAuth.Parameter> decodeAuthorization(String authorization) {
        List<OAuth.Parameter> into = new ArrayList<>();
        if (authorization != null) {
            Matcher m = AUTHORIZATION.matcher(authorization);
            if (m.matches() && "OAuth".equalsIgnoreCase(m.group(1))) {
                for (String nvp : m.group(2).split("\\s*,\\s*")) {
                    Matcher m2 = NVP.matcher(nvp);
                    if (m2.matches()) {
                        into.add(new OAuth.Parameter(OAuth.decodePercent(m2.group(1)), OAuth.decodePercent(m2.group(2))));
                    }
                }
            }
        }
        return into;
    }

    private static final String toString(Object from) {
        if (from == null) {
            return null;
        }
        return from.toString();
    }
}
