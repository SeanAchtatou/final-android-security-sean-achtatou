package net.oauth;

import java.io.Serializable;

public class OAuthServiceProvider implements Serializable {
    private static final long serialVersionUID = 3306534392621038574L;
    public final String accessTokenURL;
    public final String requestTokenURL;
    public final String userAuthorizationURL;

    public OAuthServiceProvider(String requestTokenURL2, String userAuthorizationURL2, String accessTokenURL2) {
        this.requestTokenURL = requestTokenURL2;
        this.userAuthorizationURL = userAuthorizationURL2;
        this.accessTokenURL = accessTokenURL2;
    }

    public int hashCode() {
        int i = 1 * 31;
        return (((((this.accessTokenURL == null ? 0 : this.accessTokenURL.hashCode()) + 31) * 31) + (this.requestTokenURL == null ? 0 : this.requestTokenURL.hashCode())) * 31) + (this.userAuthorizationURL == null ? 0 : this.userAuthorizationURL.hashCode());
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        OAuthServiceProvider other = (OAuthServiceProvider) obj;
        if (this.accessTokenURL == null) {
            if (other.accessTokenURL != null) {
                return false;
            }
        } else if (!this.accessTokenURL.equals(other.accessTokenURL)) {
            return false;
        }
        if (this.requestTokenURL == null) {
            if (other.requestTokenURL != null) {
                return false;
            }
        } else if (!this.requestTokenURL.equals(other.requestTokenURL)) {
            return false;
        }
        if (this.userAuthorizationURL == null) {
            if (other.userAuthorizationURL != null) {
                return false;
            }
        } else if (!this.userAuthorizationURL.equals(other.userAuthorizationURL)) {
            return false;
        }
        return true;
    }
}
