package net.oauth.http;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import net.oauth.OAuth;
import net.oauth.OAuthMessage;
import net.oauth.ParameterStyle;
import net.oauth.client.ExcerptInputStream;

public class HttpMessage {
    private static /* synthetic */ int[] $SWITCH_TABLE$net$oauth$ParameterStyle = null;
    public static final String ACCEPT_ENCODING = "Accept-Encoding";
    private static final Pattern CHARSET = Pattern.compile("; *charset *= *([^;\"]*|\"([^\"]|\\\\\")*\")(;|$)");
    public static final String CONTENT_ENCODING = "Content-Encoding";
    public static final String CONTENT_LENGTH = "Content-Length";
    public static final String CONTENT_TYPE = "Content-Type";
    public static final String DEFAULT_CHARSET = "ISO-8859-1";
    public static final String REQUEST = "HTTP request";
    public static final String RESPONSE = "HTTP response";
    public static final String STATUS_CODE = "HTTP status";
    protected InputStream body;
    public final List<Map.Entry<String, String>> headers;
    public String method;
    public URL url;

    static /* synthetic */ int[] $SWITCH_TABLE$net$oauth$ParameterStyle() {
        int[] iArr = $SWITCH_TABLE$net$oauth$ParameterStyle;
        if (iArr == null) {
            iArr = new int[ParameterStyle.values().length];
            try {
                iArr[ParameterStyle.AUTHORIZATION_HEADER.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[ParameterStyle.BODY.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[ParameterStyle.QUERY_STRING.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$net$oauth$ParameterStyle = iArr;
        }
        return iArr;
    }

    public HttpMessage() {
        this(null, null);
    }

    public HttpMessage(String method2, URL url2) {
        this(method2, url2, null);
    }

    public HttpMessage(String method2, URL url2, InputStream body2) {
        this.headers = new ArrayList();
        this.body = null;
        this.method = method2;
        this.url = url2;
        this.body = body2;
    }

    public final String getHeader(String name) {
        String value = null;
        for (Map.Entry<String, String> header : this.headers) {
            if (equalsIgnoreCase(name, (String) header.getKey())) {
                value = header.getValue();
            }
        }
        return value;
    }

    public String removeHeaders(String name) {
        String value = null;
        Iterator<Map.Entry<String, String>> i = this.headers.iterator();
        while (i.hasNext()) {
            Map.Entry<String, String> header = i.next();
            if (equalsIgnoreCase(name, (String) header.getKey())) {
                value = header.getValue();
                i.remove();
            }
        }
        return value;
    }

    public final String getContentCharset() {
        return getCharset(getHeader(CONTENT_TYPE));
    }

    public final InputStream getBody() throws IOException {
        InputStream raw;
        if (this.body == null && (raw = openBody()) != null) {
            this.body = new ExcerptInputStream(raw);
        }
        return this.body;
    }

    /* access modifiers changed from: protected */
    public InputStream openBody() throws IOException {
        return null;
    }

    public void dump(Map<String, Object> map) throws IOException {
    }

    public static HttpMessage newRequest(OAuthMessage from, ParameterStyle style) throws IOException {
        boolean isPost = "POST".equalsIgnoreCase(from.method);
        InputStream body2 = from.getBodyAsStream();
        if (style == ParameterStyle.BODY && (!isPost || body2 != null)) {
            style = ParameterStyle.QUERY_STRING;
        }
        String url2 = from.URL;
        List<Map.Entry<String, String>> headers2 = new ArrayList<>(from.getHeaders());
        switch ($SWITCH_TABLE$net$oauth$ParameterStyle()[style.ordinal()]) {
            case 1:
                headers2.add(new OAuth.Parameter("Authorization", from.getAuthorizationHeader(null)));
                List<Map.Entry<String, String>> others = from.getParameters();
                if (others != null && !others.isEmpty()) {
                    List<Map.Entry<String, String>> others2 = new ArrayList<>(others);
                    Iterator<Map.Entry<String, String>> p = others2.iterator();
                    while (p.hasNext()) {
                        if (((String) ((Map.Entry) p.next()).getKey()).startsWith("oauth_")) {
                            p.remove();
                        }
                    }
                    if (isPost && body2 == null) {
                        byte[] form = OAuth.formEncode(others2).getBytes(from.getBodyEncoding());
                        headers2.add(new OAuth.Parameter(CONTENT_TYPE, OAuth.FORM_ENCODED));
                        headers2.add(new OAuth.Parameter(CONTENT_LENGTH, new StringBuilder(String.valueOf(form.length)).toString()));
                        body2 = new ByteArrayInputStream(form);
                        break;
                    } else {
                        url2 = OAuth.addParameters(url2, others2);
                        break;
                    }
                }
            case 2:
                byte[] form2 = OAuth.formEncode(from.getParameters()).getBytes(from.getBodyEncoding());
                headers2.add(new OAuth.Parameter(CONTENT_TYPE, OAuth.FORM_ENCODED));
                headers2.add(new OAuth.Parameter(CONTENT_LENGTH, new StringBuilder(String.valueOf(form2.length)).toString()));
                body2 = new ByteArrayInputStream(form2);
                break;
            case 3:
                url2 = OAuth.addParameters(url2, from.getParameters());
                break;
        }
        HttpMessage httpRequest = new HttpMessage(from.method, new URL(url2), body2);
        httpRequest.headers.addAll(headers2);
        return httpRequest;
    }

    private static boolean equalsIgnoreCase(String x, String y) {
        if (x == null) {
            return y == null;
        }
        return x.equalsIgnoreCase(y);
    }

    private static final String getCharset(String mimeType) {
        if (mimeType != null) {
            Matcher m = CHARSET.matcher(mimeType);
            if (m.find()) {
                String charset = m.group(1);
                if (charset.length() >= 2 && charset.charAt(0) == '\"' && charset.charAt(charset.length() - 1) == '\"') {
                    charset = charset.substring(1, charset.length() - 1).replace("\\\"", "\"");
                }
                return charset;
            }
        }
        return DEFAULT_CHARSET;
    }
}
