package net.oauth.http;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.zip.GZIPInputStream;
import java.util.zip.InflaterInputStream;

public class HttpMessageDecoder extends HttpResponseMessage {
    static final /* synthetic */ boolean $assertionsDisabled = (!HttpMessageDecoder.class.desiredAssertionStatus() ? true : $assertionsDisabled);
    public static final String ACCEPTED = "gzip,deflate";
    public static final String DEFLATE = "deflate";
    public static final String GZIP = "gzip";
    private final HttpResponseMessage in;

    public static HttpResponseMessage decode(HttpResponseMessage message) throws IOException {
        String encoding;
        if (message == null || (encoding = getEncoding(message)) == null) {
            return message;
        }
        return new HttpMessageDecoder(message, encoding);
    }

    private static String getEncoding(HttpMessage message) {
        String encoding = message.getHeader(HttpMessage.CONTENT_ENCODING);
        if (encoding != null) {
            if (GZIP.equalsIgnoreCase(encoding) || "x-gzip".equalsIgnoreCase(encoding)) {
                return GZIP;
            }
            if (DEFLATE.equalsIgnoreCase(encoding)) {
                return DEFLATE;
            }
        }
        return null;
    }

    private HttpMessageDecoder(HttpResponseMessage in2, String encoding) throws IOException {
        super(in2.method, in2.url);
        this.headers.addAll(in2.headers);
        removeHeaders(HttpMessage.CONTENT_ENCODING);
        removeHeaders(HttpMessage.CONTENT_LENGTH);
        InputStream body = in2.getBody();
        if (body != null) {
            if (encoding == GZIP) {
                body = new GZIPInputStream(body);
            } else if (encoding == DEFLATE) {
                body = new InflaterInputStream(body);
            } else if (!$assertionsDisabled) {
                throw new AssertionError();
            }
        }
        this.body = body;
        this.in = in2;
    }

    public void dump(Map<String, Object> into) throws IOException {
        this.in.dump(into);
    }

    public int getStatusCode() throws IOException {
        return this.in.getStatusCode();
    }
}
