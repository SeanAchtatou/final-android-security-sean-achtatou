package net.oauth.signature;

import net.oauth.OAuth;
import net.oauth.OAuthException;

class PLAINTEXT extends OAuthSignatureMethod {
    private String signature = null;

    PLAINTEXT() {
    }

    public String getSignature(String baseString) {
        return getSignature();
    }

    /* access modifiers changed from: protected */
    public boolean isValid(String signature2, String baseString) throws OAuthException {
        return equals(getSignature(), signature2);
    }

    private synchronized String getSignature() {
        if (this.signature == null) {
            this.signature = String.valueOf(OAuth.percentEncode(getConsumerSecret())) + '&' + OAuth.percentEncode(getTokenSecret());
        }
        return this.signature;
    }

    public void setConsumerSecret(String consumerSecret) {
        synchronized (this) {
            this.signature = null;
        }
        super.setConsumerSecret(consumerSecret);
    }

    public void setTokenSecret(String tokenSecret) {
        synchronized (this) {
            this.signature = null;
        }
        super.setTokenSecret(tokenSecret);
    }
}
