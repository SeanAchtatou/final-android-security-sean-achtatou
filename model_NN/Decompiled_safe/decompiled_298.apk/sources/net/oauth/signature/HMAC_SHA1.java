package net.oauth.signature;

import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import net.oauth.OAuth;
import net.oauth.OAuthException;

class HMAC_SHA1 extends OAuthSignatureMethod {
    private static final String ENCODING = "UTF-8";
    private static final String MAC_NAME = "HmacSHA1";
    private SecretKey key = null;

    HMAC_SHA1() {
    }

    /* access modifiers changed from: protected */
    public String getSignature(String baseString) throws OAuthException {
        try {
            return base64Encode(computeSignature(baseString));
        } catch (GeneralSecurityException e) {
            throw new OAuthException(e);
        } catch (UnsupportedEncodingException e2) {
            throw new OAuthException(e2);
        }
    }

    /* access modifiers changed from: protected */
    public boolean isValid(String signature, String baseString) throws OAuthException {
        try {
            return equals(computeSignature(baseString), decodeBase64(signature));
        } catch (GeneralSecurityException e) {
            throw new OAuthException(e);
        } catch (UnsupportedEncodingException e2) {
            throw new OAuthException(e2);
        }
    }

    private byte[] computeSignature(String baseString) throws GeneralSecurityException, UnsupportedEncodingException {
        SecretKey key2;
        synchronized (this) {
            if (this.key == null) {
                this.key = new SecretKeySpec((String.valueOf(OAuth.percentEncode(getConsumerSecret())) + '&' + OAuth.percentEncode(getTokenSecret())).getBytes("UTF-8"), MAC_NAME);
            }
            key2 = this.key;
        }
        Mac mac = Mac.getInstance(MAC_NAME);
        mac.init(key2);
        return mac.doFinal(baseString.getBytes("UTF-8"));
    }

    public void setConsumerSecret(String consumerSecret) {
        synchronized (this) {
            this.key = null;
        }
        super.setConsumerSecret(consumerSecret);
    }

    public void setTokenSecret(String tokenSecret) {
        synchronized (this) {
            this.key = null;
        }
        super.setTokenSecret(tokenSecret);
    }
}
