package net.oauth.signature;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import net.oauth.OAuth;
import net.oauth.OAuthAccessor;
import net.oauth.OAuthConsumer;
import net.oauth.OAuthException;
import net.oauth.OAuthMessage;
import net.oauth.OAuthProblemException;

public abstract class OAuthSignatureMethod {
    private static final Base64 BASE64 = new Base64();
    private static final String BASE64_ENCODING = "ISO-8859-1";
    private static final Map<String, Class> NAME_TO_CLASS = new ConcurrentHashMap();
    public static final String _ACCESSOR = "-Accessor";
    private String consumerSecret;
    private String tokenSecret;

    /* access modifiers changed from: protected */
    public abstract String getSignature(String str) throws OAuthException;

    /* access modifiers changed from: protected */
    public abstract boolean isValid(String str, String str2) throws OAuthException;

    public void sign(OAuthMessage message) throws OAuthException, IOException, URISyntaxException {
        message.addParameter(new OAuth.Parameter(OAuth.OAUTH_SIGNATURE, getSignature(message)));
    }

    public void validate(OAuthMessage message) throws IOException, OAuthException, URISyntaxException {
        message.requireParameters(OAuth.OAUTH_SIGNATURE);
        String signature = message.getSignature();
        String baseString = getBaseString(message);
        if (!isValid(signature, baseString)) {
            OAuthProblemException problem = new OAuthProblemException(OAuth.Problems.SIGNATURE_INVALID);
            problem.setParameter(OAuth.OAUTH_SIGNATURE, signature);
            problem.setParameter("oauth_signature_base_string", baseString);
            problem.setParameter(OAuth.OAUTH_SIGNATURE_METHOD, message.getSignatureMethod());
            throw problem;
        }
    }

    /* access modifiers changed from: protected */
    public String getSignature(OAuthMessage message) throws OAuthException, IOException, URISyntaxException {
        return getSignature(getBaseString(message));
    }

    /* access modifiers changed from: protected */
    public void initialize(String name, OAuthAccessor accessor) throws OAuthException {
        String secret = accessor.consumer.consumerSecret;
        if (name.endsWith(_ACCESSOR)) {
            Object accessorSecret = accessor.getProperty(OAuthConsumer.ACCESSOR_SECRET);
            if (accessorSecret == null) {
                accessorSecret = accessor.consumer.getProperty(OAuthConsumer.ACCESSOR_SECRET);
            }
            if (accessorSecret != null) {
                secret = accessorSecret.toString();
            }
        }
        if (secret == null) {
            secret = "";
        }
        setConsumerSecret(secret);
    }

    /* access modifiers changed from: protected */
    public String getConsumerSecret() {
        return this.consumerSecret;
    }

    /* access modifiers changed from: protected */
    public void setConsumerSecret(String consumerSecret2) {
        this.consumerSecret = consumerSecret2;
    }

    public String getTokenSecret() {
        return this.tokenSecret;
    }

    public void setTokenSecret(String tokenSecret2) {
        this.tokenSecret = tokenSecret2;
    }

    public static String getBaseString(OAuthMessage message) throws IOException, URISyntaxException {
        List<Map.Entry<String, String>> parameters;
        String url = message.URL;
        int q = url.indexOf(63);
        if (q < 0) {
            parameters = message.getParameters();
        } else {
            parameters = new ArrayList<>();
            parameters.addAll(OAuth.decodeForm(message.URL.substring(q + 1)));
            parameters.addAll(message.getParameters());
            url = url.substring(0, q);
        }
        return String.valueOf(OAuth.percentEncode(message.method.toUpperCase())) + '&' + OAuth.percentEncode(normalizeUrl(url)) + '&' + OAuth.percentEncode(normalizeParameters(parameters));
    }

    protected static String normalizeUrl(String url) throws URISyntaxException {
        int index;
        URI uri = new URI(url);
        String scheme = uri.getScheme().toLowerCase();
        String authority = uri.getAuthority().toLowerCase();
        if (((scheme.equals("http") && uri.getPort() == 80) || (scheme.equals("https") && uri.getPort() == 443)) && (index = authority.lastIndexOf(":")) >= 0) {
            authority = authority.substring(0, index);
        }
        String path = uri.getRawPath();
        if (path == null || path.length() <= 0) {
            path = "/";
        }
        return String.valueOf(scheme) + "://" + authority + path;
    }

    protected static String normalizeParameters(Collection<? extends Map.Entry> parameters) throws IOException {
        if (parameters == null) {
            return "";
        }
        List<ComparableParameter> p = new ArrayList<>(parameters.size());
        for (Map.Entry parameter : parameters) {
            if (!OAuth.OAUTH_SIGNATURE.equals(parameter.getKey())) {
                p.add(new ComparableParameter(parameter));
            }
        }
        Collections.sort(p);
        return OAuth.formEncode(getParameters(p));
    }

    public static boolean equals(String x, String y) {
        int i;
        if (x == null) {
            if (y == null) {
                return true;
            }
            return false;
        } else if (y == null) {
            return false;
        } else {
            if (y.length() <= 0) {
                return x.length() <= 0;
            }
            char[] a = x.toCharArray();
            char[] b = y.toCharArray();
            if (a.length == b.length) {
                i = 0;
            } else {
                i = 1;
            }
            char diff = (char) i;
            int j = 0;
            for (char c : a) {
                diff = (char) ((c ^ b[j]) | diff);
                j = (j + 1) % b.length;
            }
            if (diff == 0) {
                return true;
            }
            return false;
        }
    }

    public static boolean equals(byte[] a, byte[] b) {
        int i;
        if (a == null) {
            if (b == null) {
                return true;
            }
            return false;
        } else if (b == null) {
            return false;
        } else {
            if (b.length <= 0) {
                return a.length <= 0;
            }
            if (a.length == b.length) {
                i = 0;
            } else {
                i = 1;
            }
            byte diff = (byte) i;
            int j = 0;
            for (byte b2 : a) {
                diff = (byte) ((b2 ^ b[j]) | diff);
                j = (j + 1) % b.length;
            }
            if (diff == 0) {
                return true;
            }
            return false;
        }
    }

    public static byte[] decodeBase64(String s) {
        byte[] b;
        try {
            b = s.getBytes("ISO-8859-1");
        } catch (UnsupportedEncodingException e) {
            System.err.println(new StringBuilder().append(e).toString());
            b = s.getBytes();
        }
        return BASE64.decode(b);
    }

    public static String base64Encode(byte[] b) {
        byte[] b2 = BASE64.encode(b);
        try {
            return new String(b2, "ISO-8859-1");
        } catch (UnsupportedEncodingException e) {
            System.err.println(new StringBuilder().append(e).toString());
            return new String(b2);
        }
    }

    static {
        registerMethodClass(OAuth.HMAC_SHA1, HMAC_SHA1.class);
        registerMethodClass("PLAINTEXT", PLAINTEXT.class);
        registerMethodClass(OAuth.RSA_SHA1, RSA_SHA1.class);
        registerMethodClass("HMAC-SHA1-Accessor", HMAC_SHA1.class);
        registerMethodClass("PLAINTEXT-Accessor", PLAINTEXT.class);
    }

    public static OAuthSignatureMethod newSigner(OAuthMessage message, OAuthAccessor accessor) throws IOException, OAuthException {
        message.requireParameters(OAuth.OAUTH_SIGNATURE_METHOD);
        OAuthSignatureMethod signer = newMethod(message.getSignatureMethod(), accessor);
        signer.setTokenSecret(accessor.tokenSecret);
        return signer;
    }

    public static OAuthSignatureMethod newMethod(String name, OAuthAccessor accessor) throws OAuthException {
        try {
            Class methodClass = NAME_TO_CLASS.get(name);
            if (methodClass != null) {
                OAuthSignatureMethod method = (OAuthSignatureMethod) methodClass.newInstance();
                method.initialize(name, accessor);
                return method;
            }
            OAuthProblemException problem = new OAuthProblemException(OAuth.Problems.SIGNATURE_METHOD_REJECTED);
            String acceptable = OAuth.percentEncode(NAME_TO_CLASS.keySet());
            if (acceptable.length() > 0) {
                problem.setParameter("oauth_acceptable_signature_methods", acceptable.toString());
            }
            throw problem;
        } catch (InstantiationException e) {
            throw new OAuthException(e);
        } catch (IllegalAccessException e2) {
            throw new OAuthException(e2);
        }
    }

    public static void registerMethodClass(String name, Class clazz) {
        if (clazz == null) {
            unregisterMethod(name);
        } else {
            NAME_TO_CLASS.put(name, clazz);
        }
    }

    public static void unregisterMethod(String name) {
        NAME_TO_CLASS.remove(name);
    }

    private static class ComparableParameter implements Comparable<ComparableParameter> {
        private final String key;
        final Map.Entry value;

        ComparableParameter(Map.Entry value2) {
            this.value = value2;
            this.key = String.valueOf(OAuth.percentEncode(toString(value2.getKey()))) + ' ' + OAuth.percentEncode(toString(value2.getValue()));
        }

        private static String toString(Object from) {
            if (from == null) {
                return null;
            }
            return from.toString();
        }

        public int compareTo(ComparableParameter that) {
            return this.key.compareTo(that.key);
        }

        public String toString() {
            return this.key;
        }
    }

    private static List<Map.Entry> getParameters(Collection<ComparableParameter> parameters) {
        if (parameters == null) {
            return null;
        }
        List<Map.Entry> list = new ArrayList<>(parameters.size());
        for (ComparableParameter parameter : parameters) {
            list.add(parameter.value);
        }
        return list;
    }
}
