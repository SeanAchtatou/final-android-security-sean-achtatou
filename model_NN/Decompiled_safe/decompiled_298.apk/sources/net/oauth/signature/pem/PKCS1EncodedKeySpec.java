package net.oauth.signature.pem;

import java.io.IOException;
import java.security.spec.RSAPrivateCrtKeySpec;

public class PKCS1EncodedKeySpec {
    private RSAPrivateCrtKeySpec keySpec;

    public PKCS1EncodedKeySpec(byte[] keyBytes) throws IOException {
        decode(keyBytes);
    }

    public RSAPrivateCrtKeySpec getKeySpec() {
        return this.keySpec;
    }

    private void decode(byte[] keyBytes) throws IOException {
        Asn1Object sequence = new DerParser(keyBytes).read();
        if (sequence.getType() != 16) {
            throw new IOException("Invalid DER: not a sequence");
        }
        DerParser parser = sequence.getParser();
        parser.read();
        this.keySpec = new RSAPrivateCrtKeySpec(parser.read().getInteger(), parser.read().getInteger(), parser.read().getInteger(), parser.read().getInteger(), parser.read().getInteger(), parser.read().getInteger(), parser.read().getInteger(), parser.read().getInteger());
    }
}
