package net.oauth.signature.pem;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import net.oauth.signature.OAuthSignatureMethod;

public class PEMReader {
    private static final String BEGIN_MARKER = "-----BEGIN ";
    public static final String CERTIFICATE_X509_MARKER = "-----BEGIN CERTIFICATE-----";
    public static final String PRIVATE_PKCS1_MARKER = "-----BEGIN RSA PRIVATE KEY-----";
    public static final String PRIVATE_PKCS8_MARKER = "-----BEGIN PRIVATE KEY-----";
    public static final String PUBLIC_X509_MARKER = "-----BEGIN PUBLIC KEY-----";
    private String beginMarker;
    private byte[] derBytes;
    private InputStream stream;

    public PEMReader(InputStream inStream) throws IOException {
        this.stream = inStream;
        readFile();
    }

    public PEMReader(byte[] buffer) throws IOException {
        this(new ByteArrayInputStream(buffer));
    }

    public PEMReader(String fileName) throws IOException {
        this(new FileInputStream(fileName));
    }

    public byte[] getDerBytes() {
        return this.derBytes;
    }

    public String getBeginMarker() {
        return this.beginMarker;
    }

    /* access modifiers changed from: protected */
    public void readFile() throws IOException {
        String line;
        BufferedReader reader = new BufferedReader(new InputStreamReader(this.stream));
        do {
            try {
                line = reader.readLine();
                if (line == null) {
                    throw new IOException("Invalid PEM file: no begin marker");
                }
            } finally {
                reader.close();
            }
        } while (line.indexOf(BEGIN_MARKER) == -1);
        this.beginMarker = line.trim();
        this.derBytes = readBytes(reader, this.beginMarker.replace("BEGIN", "END"));
    }

    private byte[] readBytes(BufferedReader reader, String endMarker) throws IOException {
        StringBuffer buf = new StringBuffer();
        while (true) {
            String line = reader.readLine();
            if (line == null) {
                throw new IOException("Invalid PEM file: No end marker");
            } else if (line.indexOf(endMarker) != -1) {
                return OAuthSignatureMethod.decodeBase64(buf.toString());
            } else {
                buf.append(line.trim());
            }
        }
    }
}
