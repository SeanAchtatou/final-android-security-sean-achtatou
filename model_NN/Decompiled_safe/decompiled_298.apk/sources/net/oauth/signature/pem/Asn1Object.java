package net.oauth.signature.pem;

import java.io.IOException;
import java.math.BigInteger;
import net.oauth.OAuth;
import net.oauth.http.HttpMessage;

class Asn1Object {
    protected final int length;
    protected final int tag;
    protected final int type;
    protected final byte[] value;

    public Asn1Object(int tag2, int length2, byte[] value2) {
        this.tag = tag2;
        this.type = tag2 & 31;
        this.length = length2;
        this.value = value2;
    }

    public int getType() {
        return this.type;
    }

    public int getLength() {
        return this.length;
    }

    public byte[] getValue() {
        return this.value;
    }

    public boolean isConstructed() {
        return (this.tag & 32) == 32;
    }

    public DerParser getParser() throws IOException {
        if (isConstructed()) {
            return new DerParser(this.value);
        }
        throw new IOException("Invalid DER: can't parse primitive entity");
    }

    public BigInteger getInteger() throws IOException {
        if (this.type == 2) {
            return new BigInteger(this.value);
        }
        throw new IOException("Invalid DER: object is not integer");
    }

    public String getString() throws IOException {
        String encoding;
        switch (this.type) {
            case DerParser.UTF8_STRING:
                encoding = OAuth.ENCODING;
                break;
            case DerParser.RELATIVE_OID:
            case 14:
            case 15:
            case DerParser.SEQUENCE:
            case DerParser.SET:
            case DerParser.T61_STRING:
            case DerParser.UTC_TIME:
            case DerParser.GENERALIZED_TIME:
            case 29:
            default:
                throw new IOException("Invalid DER: object is not a string");
            case DerParser.NUMERIC_STRING:
            case DerParser.PRINTABLE_STRING:
            case DerParser.VIDEOTEX_STRING:
            case DerParser.IA5_STRING:
            case DerParser.GRAPHIC_STRING:
            case DerParser.ISO646_STRING:
            case DerParser.GENERAL_STRING:
                encoding = HttpMessage.DEFAULT_CHARSET;
                break;
            case DerParser.UNIVERSAL_STRING:
                throw new IOException("Invalid DER: can't handle UCS-4 string");
            case DerParser.BMP_STRING:
                encoding = "UTF-16BE";
                break;
        }
        return new String(this.value, encoding);
    }
}
