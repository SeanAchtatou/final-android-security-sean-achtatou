package net.oauth.signature;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.KeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import net.oauth.OAuth;
import net.oauth.OAuthAccessor;
import net.oauth.OAuthException;
import net.oauth.signature.pem.PEMReader;
import net.oauth.signature.pem.PKCS1EncodedKeySpec;

public class RSA_SHA1 extends OAuthSignatureMethod {
    public static final String PRIVATE_KEY = "RSA-SHA1.PrivateKey";
    public static final String PUBLIC_KEY = "RSA-SHA1.PublicKey";
    public static final String X509_CERTIFICATE = "RSA-SHA1.X509Certificate";
    private PrivateKey privateKey = null;
    private PublicKey publicKey = null;

    /* access modifiers changed from: protected */
    public void initialize(String name, OAuthAccessor accessor) throws OAuthException {
        super.initialize(name, accessor);
        try {
            Object privateKeyObject = accessor.consumer.getProperty(PRIVATE_KEY);
            if (privateKeyObject != null) {
                this.privateKey = loadPrivateKey(privateKeyObject);
            }
            Object publicKeyObject = accessor.consumer.getProperty(PUBLIC_KEY);
            if (publicKeyObject != null) {
                this.publicKey = loadPublicKey(publicKeyObject, false);
                return;
            }
            Object certObject = accessor.consumer.getProperty(X509_CERTIFICATE);
            if (certObject != null) {
                this.publicKey = loadPublicKey(certObject, true);
            }
        } catch (GeneralSecurityException e) {
            throw new OAuthException(e);
        } catch (IOException e2) {
            throw new OAuthException(e2);
        }
    }

    private PublicKey getPublicKeyFromDerCert(byte[] certObject) throws GeneralSecurityException {
        return ((X509Certificate) CertificateFactory.getInstance("X509").generateCertificate(new ByteArrayInputStream(certObject))).getPublicKey();
    }

    private PublicKey getPublicKeyFromDer(byte[] publicKeyObject) throws GeneralSecurityException {
        return KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(publicKeyObject));
    }

    private PublicKey getPublicKeyFromPem(String pem) throws GeneralSecurityException, IOException {
        PEMReader reader = new PEMReader(new ByteArrayInputStream(pem.getBytes(OAuth.ENCODING)));
        byte[] bytes = reader.getDerBytes();
        if (PEMReader.PUBLIC_X509_MARKER.equals(reader.getBeginMarker())) {
            return KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(bytes));
        } else if (PEMReader.CERTIFICATE_X509_MARKER.equals(reader.getBeginMarker())) {
            return getPublicKeyFromDerCert(bytes);
        } else {
            throw new IOException("Invalid PEM fileL: Unknown marker for  public key or cert " + reader.getBeginMarker());
        }
    }

    private PrivateKey getPrivateKeyFromDer(byte[] privateKeyObject) throws GeneralSecurityException {
        return KeyFactory.getInstance("RSA").generatePrivate(new PKCS8EncodedKeySpec(privateKeyObject));
    }

    private PrivateKey getPrivateKeyFromPem(String pem) throws GeneralSecurityException, IOException {
        KeySpec keySpec;
        PEMReader reader = new PEMReader(new ByteArrayInputStream(pem.getBytes(OAuth.ENCODING)));
        byte[] bytes = reader.getDerBytes();
        if (PEMReader.PRIVATE_PKCS1_MARKER.equals(reader.getBeginMarker())) {
            keySpec = new PKCS1EncodedKeySpec(bytes).getKeySpec();
        } else if (PEMReader.PRIVATE_PKCS8_MARKER.equals(reader.getBeginMarker())) {
            keySpec = new PKCS8EncodedKeySpec(bytes);
        } else {
            throw new IOException("Invalid PEM file: Unknown marker for private key " + reader.getBeginMarker());
        }
        return KeyFactory.getInstance("RSA").generatePrivate(keySpec);
    }

    /* access modifiers changed from: protected */
    public String getSignature(String baseString) throws OAuthException {
        try {
            return base64Encode(sign(baseString.getBytes(OAuth.ENCODING)));
        } catch (UnsupportedEncodingException e) {
            throw new OAuthException(e);
        } catch (GeneralSecurityException e2) {
            throw new OAuthException(e2);
        }
    }

    /* access modifiers changed from: protected */
    public boolean isValid(String signature, String baseString) throws OAuthException {
        try {
            return verify(decodeBase64(signature), baseString.getBytes(OAuth.ENCODING));
        } catch (UnsupportedEncodingException e) {
            throw new OAuthException(e);
        } catch (GeneralSecurityException e2) {
            throw new OAuthException(e2);
        }
    }

    private byte[] sign(byte[] message) throws GeneralSecurityException {
        if (this.privateKey == null) {
            throw new IllegalStateException("need to set private key with OAuthConsumer.setProperty when generating RSA-SHA1 signatures.");
        }
        Signature signer = Signature.getInstance("SHA1withRSA");
        signer.initSign(this.privateKey);
        signer.update(message);
        return signer.sign();
    }

    private boolean verify(byte[] signature, byte[] message) throws GeneralSecurityException {
        if (this.publicKey == null) {
            throw new IllegalStateException("need to set public key with  OAuthConsumer.setProperty when verifying RSA-SHA1 signatures.");
        }
        Signature verifier = Signature.getInstance("SHA1withRSA");
        verifier.initVerify(this.publicKey);
        verifier.update(message);
        return verifier.verify(signature);
    }

    private PrivateKey loadPrivateKey(Object privateKeyObject) throws IOException, GeneralSecurityException {
        if (privateKeyObject instanceof PrivateKey) {
            return (PrivateKey) privateKeyObject;
        }
        if (privateKeyObject instanceof String) {
            try {
                return getPrivateKeyFromPem((String) privateKeyObject);
            } catch (IOException e) {
                return getPrivateKeyFromDer(decodeBase64((String) privateKeyObject));
            }
        } else if (privateKeyObject instanceof byte[]) {
            return getPrivateKeyFromDer((byte[]) privateKeyObject);
        } else {
            throw new IllegalArgumentException("Private key set through RSA_SHA1.PRIVATE_KEY must be of type PrivateKey, String or byte[] and not " + privateKeyObject.getClass().getName());
        }
    }

    private PublicKey loadPublicKey(Object publicKeyObject, boolean isCert) throws IOException, GeneralSecurityException {
        String source;
        if (publicKeyObject instanceof PublicKey) {
            return (PublicKey) publicKeyObject;
        }
        if (publicKeyObject instanceof X509Certificate) {
            return ((X509Certificate) publicKeyObject).getPublicKey();
        }
        if (publicKeyObject instanceof String) {
            try {
                return getPublicKeyFromPem((String) publicKeyObject);
            } catch (IOException e) {
                IOException e2 = e;
                if (!isCert) {
                    return getPublicKeyFromDer(decodeBase64((String) publicKeyObject));
                }
                throw e2;
            }
        } else if (!(publicKeyObject instanceof byte[])) {
            if (isCert) {
                source = "RSA_SHA1.X509_CERTIFICATE";
            } else {
                source = "RSA_SHA1.PUBLIC_KEY";
            }
            throw new IllegalArgumentException("Public key or certificate set through " + source + " must be of " + "type PublicKey, String or byte[], and not " + publicKeyObject.getClass().getName());
        } else if (isCert) {
            return getPublicKeyFromDerCert((byte[]) publicKeyObject);
        } else {
            return getPublicKeyFromDer((byte[]) publicKeyObject);
        }
    }
}
