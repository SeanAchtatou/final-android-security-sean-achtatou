package net.oauth;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OAuth {
    public static final String ENCODING = "UTF-8";
    public static final String FORM_ENCODED = "application/x-www-form-urlencoded";
    public static final String HMAC_SHA1 = "HMAC-SHA1";
    public static final String OAUTH_CALLBACK = "oauth_callback";
    public static final String OAUTH_CALLBACK_CONFIRMED = "oauth_callback_confirmed";
    public static final String OAUTH_CONSUMER_KEY = "oauth_consumer_key";
    public static final String OAUTH_NONCE = "oauth_nonce";
    public static final String OAUTH_SIGNATURE = "oauth_signature";
    public static final String OAUTH_SIGNATURE_METHOD = "oauth_signature_method";
    public static final String OAUTH_TIMESTAMP = "oauth_timestamp";
    public static final String OAUTH_TOKEN = "oauth_token";
    public static final String OAUTH_TOKEN_SECRET = "oauth_token_secret";
    public static final String OAUTH_VERIFIER = "oauth_verifier";
    public static final String OAUTH_VERSION = "oauth_version";
    public static final String RSA_SHA1 = "RSA-SHA1";
    public static final String VERSION_1_0 = "1.0";
    private static String characterEncoding = ENCODING;

    public static class Problems {
        public static final String ADDITIONAL_AUTHORIZATION_REQUIRED = "additional_authorization_required";
        public static final String CONSUMER_KEY_REFUSED = "consumer_key_refused";
        public static final String CONSUMER_KEY_REJECTED = "consumer_key_rejected";
        public static final String CONSUMER_KEY_UNKNOWN = "consumer_key_unknown";
        public static final String NONCE_USED = "nonce_used";
        public static final String OAUTH_ACCEPTABLE_TIMESTAMPS = "oauth_acceptable_timestamps";
        public static final String OAUTH_ACCEPTABLE_VERSIONS = "oauth_acceptable_versions";
        public static final String OAUTH_PARAMETERS_ABSENT = "oauth_parameters_absent";
        public static final String OAUTH_PARAMETERS_REJECTED = "oauth_parameters_rejected";
        public static final String OAUTH_PROBLEM_ADVICE = "oauth_problem_advice";
        public static final String PARAMETER_ABSENT = "parameter_absent";
        public static final String PARAMETER_REJECTED = "parameter_rejected";
        public static final String PERMISSION_DENIED = "permission_denied";
        public static final String PERMISSION_UNKNOWN = "permission_unknown";
        public static final String SIGNATURE_INVALID = "signature_invalid";
        public static final String SIGNATURE_METHOD_REJECTED = "signature_method_rejected";
        public static final String TIMESTAMP_REFUSED = "timestamp_refused";
        public static final String TOKEN_EXPIRED = "token_expired";
        public static final String TOKEN_REJECTED = "token_rejected";
        public static final String TOKEN_REVOKED = "token_revoked";
        public static final String TOKEN_USED = "token_used";
        public static final Map<String, Integer> TO_HTTP_CODE = mapToHttpCode();
        public static final String USER_REFUSED = "user_refused";
        public static final String VERSION_REJECTED = "version_rejected";

        private static Map<String, Integer> mapToHttpCode() {
            Integer badRequest = new Integer(400);
            Integer unauthorized = new Integer(401);
            Integer serviceUnavailable = new Integer(503);
            Map<String, Integer> map = new HashMap<>();
            map.put(VERSION_REJECTED, badRequest);
            map.put(PARAMETER_ABSENT, badRequest);
            map.put(PARAMETER_REJECTED, badRequest);
            map.put(TIMESTAMP_REFUSED, badRequest);
            map.put(SIGNATURE_METHOD_REJECTED, badRequest);
            map.put(NONCE_USED, unauthorized);
            map.put(TOKEN_USED, unauthorized);
            map.put(TOKEN_EXPIRED, unauthorized);
            map.put(TOKEN_REVOKED, unauthorized);
            map.put(TOKEN_REJECTED, unauthorized);
            map.put("token_not_authorized", unauthorized);
            map.put(SIGNATURE_INVALID, unauthorized);
            map.put(CONSUMER_KEY_UNKNOWN, unauthorized);
            map.put(CONSUMER_KEY_REJECTED, unauthorized);
            map.put(ADDITIONAL_AUTHORIZATION_REQUIRED, unauthorized);
            map.put(PERMISSION_UNKNOWN, unauthorized);
            map.put(PERMISSION_DENIED, unauthorized);
            map.put(USER_REFUSED, serviceUnavailable);
            map.put(CONSUMER_KEY_REFUSED, serviceUnavailable);
            return Collections.unmodifiableMap(map);
        }
    }

    public static void setCharacterEncoding(String encoding) {
        characterEncoding = encoding;
    }

    public static String decodeCharacters(byte[] from) {
        if (characterEncoding != null) {
            try {
                return new String(from, characterEncoding);
            } catch (UnsupportedEncodingException e) {
                System.err.println(new StringBuilder().append(e).toString());
            }
        }
        return new String(from);
    }

    public static byte[] encodeCharacters(String from) {
        if (characterEncoding != null) {
            try {
                return from.getBytes(characterEncoding);
            } catch (UnsupportedEncodingException e) {
                System.err.println(new StringBuilder().append(e).toString());
            }
        }
        return from.getBytes();
    }

    public static boolean isFormEncoded(String contentType) {
        if (contentType == null) {
            return false;
        }
        int semi = contentType.indexOf(";");
        if (semi >= 0) {
            contentType = contentType.substring(0, semi);
        }
        return FORM_ENCODED.equalsIgnoreCase(contentType.trim());
    }

    public static String formEncode(Iterable<? extends Map.Entry> parameters) throws IOException {
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        formEncode(parameters, b);
        return decodeCharacters(b.toByteArray());
    }

    public static void formEncode(Iterable<? extends Map.Entry> parameters, OutputStream into) throws IOException {
        if (parameters != null) {
            boolean first = true;
            for (Map.Entry parameter : parameters) {
                if (first) {
                    first = false;
                } else {
                    into.write(38);
                }
                into.write(encodeCharacters(percentEncode(toString(parameter.getKey()))));
                into.write(61);
                into.write(encodeCharacters(percentEncode(toString(parameter.getValue()))));
            }
        }
    }

    public static List<Parameter> decodeForm(String form) {
        String name;
        String value;
        List<Parameter> list = new ArrayList<>();
        if (!isEmpty(form)) {
            for (String nvp : form.split("\\&")) {
                int equals = nvp.indexOf(61);
                if (equals < 0) {
                    name = decodePercent(nvp);
                    value = null;
                } else {
                    name = decodePercent(nvp.substring(0, equals));
                    value = decodePercent(nvp.substring(equals + 1));
                }
                list.add(new Parameter(name, value));
            }
        }
        return list;
    }

    public static String percentEncode(Iterable values) {
        StringBuilder p = new StringBuilder();
        for (Object v : values) {
            if (p.length() > 0) {
                p.append("&");
            }
            p.append(percentEncode(toString(v)));
        }
        return p.toString();
    }

    public static String percentEncode(String s) {
        if (s == null) {
            return "";
        }
        try {
            return URLEncoder.encode(s, ENCODING).replace("+", "%20").replace("*", "%2A").replace("%7E", "~");
        } catch (UnsupportedEncodingException e) {
            UnsupportedEncodingException wow = e;
            throw new RuntimeException(wow.getMessage(), wow);
        }
    }

    public static String decodePercent(String s) {
        try {
            return URLDecoder.decode(s, ENCODING);
        } catch (UnsupportedEncodingException e) {
            UnsupportedEncodingException wow = e;
            throw new RuntimeException(wow.getMessage(), wow);
        }
    }

    public static Map<String, String> newMap(Iterable<? extends Map.Entry> from) {
        Map<String, String> map = new HashMap<>();
        if (from != null) {
            for (Map.Entry f : from) {
                String key = toString(f.getKey());
                if (!map.containsKey(key)) {
                    map.put(key, toString(f.getValue()));
                }
            }
        }
        return map;
    }

    public static List<Parameter> newList(String... parameters) {
        List<Parameter> list = new ArrayList<>(parameters.length / 2);
        for (int p = 0; p + 1 < parameters.length; p += 2) {
            list.add(new Parameter(parameters[p], parameters[p + 1]));
        }
        return list;
    }

    public static class Parameter implements Map.Entry<String, String> {
        private final String key;
        private String value;

        public Parameter(String key2, String value2) {
            this.key = key2;
            this.value = value2;
        }

        public String getKey() {
            return this.key;
        }

        public String getValue() {
            return this.value;
        }

        public String setValue(String value2) {
            try {
                return this.value;
            } finally {
                this.value = value2;
            }
        }

        public String toString() {
            return String.valueOf(OAuth.percentEncode(getKey())) + '=' + OAuth.percentEncode(getValue());
        }

        public int hashCode() {
            int i = 1 * 31;
            return (((this.key == null ? 0 : this.key.hashCode()) + 31) * 31) + (this.value == null ? 0 : this.value.hashCode());
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            Parameter that = (Parameter) obj;
            if (this.key == null) {
                if (that.key != null) {
                    return false;
                }
            } else if (!this.key.equals(that.key)) {
                return false;
            }
            if (this.value == null) {
                if (that.value != null) {
                    return false;
                }
            } else if (!this.value.equals(that.value)) {
                return false;
            }
            return true;
        }
    }

    private static final String toString(Object from) {
        if (from == null) {
            return null;
        }
        return from.toString();
    }

    public static String addParameters(String url, String... parameters) throws IOException {
        return addParameters(url, newList(parameters));
    }

    public static String addParameters(String url, Iterable<? extends Map.Entry<String, String>> parameters) throws IOException {
        String form = formEncode(parameters);
        if (form == null || form.length() <= 0) {
            return url;
        }
        return String.valueOf(url) + (url.indexOf("?") < 0 ? '?' : '&') + form;
    }

    public static boolean isEmpty(String str) {
        return str == null || str.length() == 0;
    }
}
