package net.oauth;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class OAuthAccessor implements Cloneable, Serializable {
    private static final long serialVersionUID = 5590788443138352999L;
    public String accessToken;
    public final OAuthConsumer consumer;
    private final Map<String, Object> properties = new HashMap();
    public String requestToken;
    public String tokenSecret;

    public OAuthAccessor(OAuthConsumer consumer2) {
        this.consumer = consumer2;
        this.requestToken = null;
        this.accessToken = null;
        this.tokenSecret = null;
    }

    public OAuthAccessor clone() {
        try {
            return (OAuthAccessor) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }

    public Object getProperty(String name) {
        return this.properties.get(name);
    }

    public void setProperty(String name, Object value) {
        this.properties.put(name, value);
    }

    public OAuthMessage newRequestMessage(String method, String url, Collection<? extends Map.Entry> parameters, InputStream body) throws OAuthException, IOException, URISyntaxException {
        if (method == null && (method = (String) getProperty("httpMethod")) == null && (method = (String) this.consumer.getProperty("httpMethod")) == null) {
            method = "GET";
        }
        OAuthMessage message = new OAuthMessage(method, url, parameters, body);
        message.addRequiredParameters(this);
        return message;
    }

    public OAuthMessage newRequestMessage(String method, String url, Collection<? extends Map.Entry> parameters) throws OAuthException, IOException, URISyntaxException {
        return newRequestMessage(method, url, parameters, null);
    }
}
