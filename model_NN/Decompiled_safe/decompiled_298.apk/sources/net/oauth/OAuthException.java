package net.oauth;

public class OAuthException extends Exception {
    private static final long serialVersionUID = 1;

    protected OAuthException() {
    }

    public OAuthException(String message) {
        super(message);
    }

    public OAuthException(Throwable cause) {
        super(cause);
    }

    public OAuthException(String message, Throwable cause) {
        super(message, cause);
    }
}
