package net.oauth.client.httpclient4;

import org.apache.http.auth.AuthScheme;
import org.apache.http.auth.AuthSchemeFactory;
import org.apache.http.params.HttpParams;

public class OAuthSchemeFactory implements AuthSchemeFactory {
    public static final String DEFAULT_REALM = "defaultRealm";
    public static final String SCHEME_NAME = "OAuth";

    public AuthScheme newInstance(HttpParams params) {
        return new OAuthScheme((String) params.getParameter(DEFAULT_REALM));
    }
}
