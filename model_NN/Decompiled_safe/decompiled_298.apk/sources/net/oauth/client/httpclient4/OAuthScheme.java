package net.oauth.client.httpclient4;

import net.oauth.OAuthAccessor;
import net.oauth.OAuthConsumer;
import net.oauth.OAuthMessage;
import org.apache.http.Header;
import org.apache.http.HttpRequest;
import org.apache.http.RequestLine;
import org.apache.http.auth.AuthenticationException;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.MalformedChallengeException;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.auth.RFC2617Scheme;
import org.apache.http.impl.client.RequestWrapper;
import org.apache.http.message.BasicHeader;

class OAuthScheme extends RFC2617Scheme {
    private boolean complete;
    private final String defaultRealm;

    OAuthScheme(String defaultRealm2) {
        this.defaultRealm = defaultRealm2;
    }

    public String getRealm() {
        String realm = OAuthScheme.super.getRealm();
        if (realm == null) {
            return this.defaultRealm;
        }
        return realm;
    }

    public String getSchemeName() {
        return "OAuth";
    }

    public boolean isComplete() {
        return this.complete;
    }

    public boolean isConnectionBased() {
        return false;
    }

    public void processChallenge(Header challenge) throws MalformedChallengeException {
        OAuthScheme.super.processChallenge(challenge);
        this.complete = true;
    }

    public Header authenticate(Credentials credentials, HttpRequest request) throws AuthenticationException {
        String uri;
        String method;
        HttpUriRequest uriRequest = getHttpUriRequest(request);
        if (uriRequest != null) {
            uri = uriRequest.getURI().toString();
            method = uriRequest.getMethod();
        } else {
            RequestLine requestLine = request.getRequestLine();
            uri = requestLine.getUri();
            method = requestLine.getMethod();
        }
        try {
            OAuthMessage message = new OAuthMessage(method, uri, null);
            message.addRequiredParameters(getAccessor(credentials));
            return new BasicHeader("Authorization", message.getAuthorizationHeader(getRealm()));
        } catch (Exception e) {
            throw new AuthenticationException((String) null, e);
        }
    }

    private HttpUriRequest getHttpUriRequest(HttpRequest request) {
        HttpRequest original;
        while ((request instanceof RequestWrapper) && (original = ((RequestWrapper) request).getOriginal()) != request) {
            request = original;
        }
        if (request instanceof HttpUriRequest) {
            return (HttpUriRequest) request;
        }
        return null;
    }

    private OAuthAccessor getAccessor(Credentials credentials) {
        if (credentials instanceof OAuthCredentials) {
            return ((OAuthCredentials) credentials).getAccessor();
        }
        return new OAuthAccessor(new OAuthConsumer(null, credentials.getUserPrincipal().getName(), credentials.getPassword(), null));
    }
}
