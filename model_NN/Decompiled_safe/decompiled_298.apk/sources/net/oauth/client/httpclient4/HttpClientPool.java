package net.oauth.client.httpclient4;

import java.net.URL;
import org.apache.http.client.HttpClient;

public interface HttpClientPool {
    HttpClient getHttpClient(URL url);
}
