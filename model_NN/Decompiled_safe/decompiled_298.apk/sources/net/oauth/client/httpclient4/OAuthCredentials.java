package net.oauth.client.httpclient4;

import net.oauth.OAuthAccessor;
import net.oauth.OAuthConsumer;
import org.apache.http.auth.UsernamePasswordCredentials;

public class OAuthCredentials extends UsernamePasswordCredentials {
    private final OAuthAccessor accessor;

    public OAuthCredentials(OAuthAccessor accessor2) {
        super(accessor2.consumer.consumerKey, accessor2.consumer.consumerSecret);
        this.accessor = accessor2;
    }

    public OAuthCredentials(String consumerKey, String consumerSecret) {
        this(new OAuthAccessor(new OAuthConsumer(null, consumerKey, consumerSecret, null)));
    }

    public OAuthAccessor getAccessor() {
        return this.accessor;
    }

    public String getPassword() {
        return getAccessor().consumer.consumerSecret;
    }

    public String getUserName() {
        return getAccessor().consumer.consumerKey;
    }
}
