package net.oauth.client.httpclient4;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import net.oauth.OAuth;
import net.oauth.client.ExcerptInputStream;
import net.oauth.http.HttpResponseMessage;
import org.apache.http.Header;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpRequestBase;

public class HttpMethodResponse extends HttpResponseMessage {
    private final HttpRequestBase httpRequest;
    private final HttpResponse httpResponse;
    private final byte[] requestBody;
    private final String requestEncoding;

    public HttpMethodResponse(HttpRequestBase request, HttpResponse response, byte[] requestBody2, String requestEncoding2) throws IOException {
        super(request.getMethod(), new URL(request.getURI().toString()));
        this.httpRequest = request;
        this.httpResponse = response;
        this.requestBody = requestBody2;
        this.requestEncoding = requestEncoding2;
        this.headers.addAll(getHeaders());
    }

    public int getStatusCode() {
        return this.httpResponse.getStatusLine().getStatusCode();
    }

    public InputStream openBody() throws IOException {
        return this.httpResponse.getEntity().getContent();
    }

    private List<Map.Entry<String, String>> getHeaders() {
        List<Map.Entry<String, String>> headers = new ArrayList<>();
        Header[] allHeaders = this.httpResponse.getAllHeaders();
        if (allHeaders != null) {
            for (Header header : allHeaders) {
                headers.add(new OAuth.Parameter(header.getName(), header.getValue()));
            }
        }
        return headers;
    }

    public void dump(Map<String, Object> into) throws IOException {
        super.dump(into);
        StringBuilder request = new StringBuilder(this.httpRequest.getMethod());
        request.append(" ").append(this.httpRequest.getURI().getPath());
        String query = this.httpRequest.getURI().getQuery();
        if (query != null && query.length() > 0) {
            request.append("?").append(query);
        }
        request.append(HttpResponseMessage.EOL);
        for (Header header : this.httpRequest.getAllHeaders()) {
            request.append(header.getName()).append(": ").append(header.getValue()).append(HttpResponseMessage.EOL);
        }
        if (this.httpRequest instanceof HttpEntityEnclosingRequest) {
            long contentLength = this.httpRequest.getEntity().getContentLength();
            if (contentLength >= 0) {
                request.append("Content-Length: ").append(contentLength).append(HttpResponseMessage.EOL);
            }
        }
        request.append(HttpResponseMessage.EOL);
        if (this.requestBody != null) {
            request.append(new String(this.requestBody, this.requestEncoding));
        }
        into.put("HTTP request", request.toString());
        StringBuilder response = new StringBuilder();
        response.append(this.httpResponse.getStatusLine().toString()).append(HttpResponseMessage.EOL);
        for (Header header2 : this.httpResponse.getAllHeaders()) {
            response.append(header2.getName()).append(": ").append(header2.getValue()).append(HttpResponseMessage.EOL);
        }
        response.append(HttpResponseMessage.EOL);
        if (this.body != null) {
            response.append(new String(((ExcerptInputStream) this.body).getExcerpt(), getContentCharset()));
        }
        into.put("HTTP response", response.toString());
    }
}
