package net.oauth.client.httpclient4;

import java.io.IOException;
import org.apache.http.HttpException;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.auth.AuthScheme;
import org.apache.http.auth.AuthSchemeRegistry;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.AuthState;
import org.apache.http.auth.Credentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.protocol.HttpContext;

public class PreemptiveAuthorizer implements HttpRequestInterceptor {
    /* Debug info: failed to restart local var, previous not found, register: 13 */
    public void process(HttpRequest request, HttpContext context) throws HttpException, IOException {
        Credentials cred;
        AuthState authState = (AuthState) context.getAttribute("http.auth.target-scope");
        if (authState == null || authState.getAuthScheme() == null) {
            HttpHost target = (HttpHost) context.getAttribute("http.target_host");
            CredentialsProvider creds = (CredentialsProvider) context.getAttribute("http.auth.credentials-provider");
            AuthSchemeRegistry schemes = (AuthSchemeRegistry) context.getAttribute("http.authscheme-registry");
            for (Object schemeName : (Iterable) context.getAttribute("http.auth.scheme-pref")) {
                AuthScheme scheme = schemes.getAuthScheme(schemeName.toString(), request.getParams());
                if (scheme != null && (cred = creds.getCredentials(new AuthScope(target.getHostName(), target.getPort(), scheme.getRealm(), scheme.getSchemeName()))) != null) {
                    authState.setAuthScheme(scheme);
                    authState.setCredentials(cred);
                    return;
                }
            }
        }
    }
}
