package net.oauth.client.httpclient4;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Map;
import net.oauth.client.ExcerptInputStream;
import net.oauth.http.HttpClient;
import net.oauth.http.HttpMessage;
import net.oauth.http.HttpResponseMessage;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.HttpParams;

public class HttpClient4 implements HttpClient {
    private static final HttpClientPool SHARED_CLIENT = new SingleClient();
    private final HttpClientPool clientPool;

    public HttpClient4() {
        this(SHARED_CLIENT);
    }

    public HttpClient4(HttpClientPool clientPool2) {
        this.clientPool = clientPool2;
    }

    public HttpResponseMessage execute(HttpMessage request, Map<String, Object> parameters) throws IOException {
        HttpRequestBase httpRequest;
        String method = request.method;
        String url = request.url.toExternalForm();
        InputStream body = request.getBody();
        boolean isDelete = "DELETE".equalsIgnoreCase(method);
        boolean isPost = "POST".equalsIgnoreCase(method);
        boolean isPut = "PUT".equalsIgnoreCase(method);
        byte[] excerpt = null;
        if (isPost || isPut) {
            HttpRequestBase httpPost = isPost ? new HttpPost(url) : new HttpPut(url);
            if (body != null) {
                ExcerptInputStream e = new ExcerptInputStream(body);
                excerpt = e.getExcerpt();
                String length = request.removeHeaders(HttpMessage.CONTENT_LENGTH);
                httpPost.setEntity(new InputStreamEntity(e, length == null ? -1 : Long.parseLong(length)));
            }
            httpRequest = httpPost;
        } else if (isDelete) {
            httpRequest = new HttpDelete(url);
        } else {
            httpRequest = new HttpGet(url);
        }
        for (Map.Entry<String, String> header : request.headers) {
            httpRequest.addHeader((String) header.getKey(), (String) header.getValue());
        }
        HttpParams params = httpRequest.getParams();
        for (Map.Entry<String, Object> p : parameters.entrySet()) {
            String name = (String) p.getKey();
            String value = p.getValue().toString();
            if (HttpClient.FOLLOW_REDIRECTS.equals(name)) {
                params.setBooleanParameter("http.protocol.handle-redirects", Boolean.parseBoolean(value));
            } else if (HttpClient.READ_TIMEOUT.equals(name)) {
                params.setIntParameter("http.socket.timeout", Integer.parseInt(value));
            } else if (HttpClient.CONNECT_TIMEOUT.equals(name)) {
                params.setIntParameter("http.connection.timeout", Integer.parseInt(value));
            }
        }
        params.setBooleanParameter("http.protocol.expect-continue", false);
        return new HttpMethodResponse(httpRequest, this.clientPool.getHttpClient(new URL(httpRequest.getURI().toString())).execute(httpRequest), excerpt, request.getContentCharset());
    }

    private static class SingleClient implements HttpClientPool {
        private final org.apache.http.client.HttpClient client;

        SingleClient() {
            org.apache.http.client.HttpClient client2 = new DefaultHttpClient();
            ClientConnectionManager mgr = client2.getConnectionManager();
            if (!(mgr instanceof ThreadSafeClientConnManager)) {
                HttpParams params = client2.getParams();
                client2 = new DefaultHttpClient(new ThreadSafeClientConnManager(params, mgr.getSchemeRegistry()), params);
            }
            this.client = client2;
        }

        public org.apache.http.client.HttpClient getHttpClient(URL server) {
            return this.client;
        }
    }
}
