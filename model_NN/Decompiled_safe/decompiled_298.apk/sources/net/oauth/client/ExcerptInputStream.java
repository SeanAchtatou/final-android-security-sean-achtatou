package net.oauth.client;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import net.oauth.OAuth;

public class ExcerptInputStream extends BufferedInputStream {
    public static final byte[] ELLIPSIS = OAuth.encodeCharacters(" ...");
    private static final int LIMIT = 1024;
    private byte[] excerpt = new byte[(ELLIPSIS.length + LIMIT)];

    public ExcerptInputStream(InputStream in) throws IOException {
        super(in);
        mark(LIMIT);
        int total = 0;
        do {
            int read = read(this.excerpt, total, LIMIT - total);
            if (read == -1) {
                break;
            }
            total += read;
        } while (total < LIMIT);
        if (total == LIMIT) {
            System.arraycopy(ELLIPSIS, 0, this.excerpt, total, ELLIPSIS.length);
        } else {
            byte[] tmp = new byte[total];
            System.arraycopy(this.excerpt, 0, tmp, 0, total);
            this.excerpt = tmp;
        }
        reset();
    }

    public byte[] getExcerpt() {
        return this.excerpt;
    }
}
