package net.oauth.client;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import net.oauth.http.HttpClient;
import net.oauth.http.HttpMessage;
import net.oauth.http.HttpResponseMessage;

public class URLConnectionClient implements HttpClient {
    private static final String EOL = "\r\n";

    /* Debug info: failed to restart local var, previous not found, register: 29 */
    public HttpResponseMessage execute(HttpMessage request, Map<String, Object> parameters) throws IOException {
        OutputStream output;
        String httpMethod = request.method;
        Collection<Map.Entry<String, String>> addHeaders = request.headers;
        URL url = request.url;
        URLConnection connection = url.openConnection();
        connection.setDoInput(true);
        if (connection instanceof HttpURLConnection) {
            HttpURLConnection http = (HttpURLConnection) connection;
            http.setRequestMethod(httpMethod);
            for (Map.Entry<String, Object> p : parameters.entrySet()) {
                String name = (String) p.getKey();
                String value = p.getValue().toString();
                if (HttpClient.FOLLOW_REDIRECTS.equals(name)) {
                    http.setInstanceFollowRedirects(Boolean.parseBoolean(value));
                } else if (HttpClient.CONNECT_TIMEOUT.equals(name)) {
                    http.setConnectTimeout(Integer.parseInt(value));
                } else if (HttpClient.READ_TIMEOUT.equals(name)) {
                    http.setReadTimeout(Integer.parseInt(value));
                }
            }
        }
        StringBuilder sb = new StringBuilder(httpMethod);
        sb.append(" ").append(url.getPath());
        String query = url.getQuery();
        if (query != null && query.length() > 0) {
            sb.append("?").append(query);
        }
        sb.append("\r\n");
        for (Map.Entry<String, List<String>> header : connection.getRequestProperties().entrySet()) {
            String key = (String) header.getKey();
            for (String value2 : (List) header.getValue()) {
                sb.append(key).append(": ").append(value2).append("\r\n");
            }
        }
        String contentLength = null;
        for (Map.Entry<String, String> header2 : addHeaders) {
            String key2 = (String) header2.getKey();
            if (!HttpMessage.CONTENT_LENGTH.equalsIgnoreCase(key2) || !(connection instanceof HttpURLConnection)) {
                connection.setRequestProperty(key2, (String) header2.getValue());
            } else {
                contentLength = header2.getValue();
            }
            sb.append(key2).append(": ").append((String) header2.getValue()).append("\r\n");
        }
        byte[] excerpt = null;
        InputStream body = request.getBody();
        if (body != null) {
            if (contentLength != null) {
                try {
                    ((HttpURLConnection) connection).setFixedLengthStreamingMode(Integer.parseInt(contentLength));
                } catch (Throwable th) {
                    body.close();
                    throw th;
                }
            }
            connection.setDoOutput(true);
            output = connection.getOutputStream();
            ExcerptInputStream ex = new ExcerptInputStream(body);
            byte[] b = new byte[1024];
            while (true) {
                int n = ex.read(b);
                if (n <= 0) {
                    break;
                }
                output.write(b, 0, n);
            }
            excerpt = ex.getExcerpt();
            output.close();
            body.close();
        }
        return new URLConnectionResponse(request, sb.toString(), excerpt, connection);
    }
}
