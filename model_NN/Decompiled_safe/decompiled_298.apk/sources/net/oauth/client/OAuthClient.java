package net.oauth.client;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.oauth.OAuth;
import net.oauth.OAuthAccessor;
import net.oauth.OAuthConsumer;
import net.oauth.OAuthException;
import net.oauth.OAuthMessage;
import net.oauth.OAuthProblemException;
import net.oauth.ParameterStyle;
import net.oauth.http.HttpClient;
import net.oauth.http.HttpMessage;
import net.oauth.http.HttpMessageDecoder;
import net.oauth.signature.OAuthSignatureMethod;

public class OAuthClient {
    @Deprecated
    public static final String ACCEPT_ENCODING = "HTTP.header.Accept-Encoding";
    protected static final String CONTENT_LENGTH = "Content-Length";
    protected static final String DELETE = "DELETE";
    public static final String PARAMETER_STYLE = "parameterStyle";
    protected static final String POST = "POST";
    protected static final String PUT = "PUT";
    private HttpClient http;
    protected final Map<String, Object> httpParameters = new HashMap();

    public OAuthClient(HttpClient http2) {
        this.http = http2;
        this.httpParameters.put(HttpClient.FOLLOW_REDIRECTS, Boolean.FALSE);
    }

    public void setHttpClient(HttpClient http2) {
        this.http = http2;
    }

    public HttpClient getHttpClient() {
        return this.http;
    }

    public Map<String, Object> getHttpParameters() {
        return this.httpParameters;
    }

    public void getRequestToken(OAuthAccessor accessor) throws IOException, OAuthException, URISyntaxException {
        getRequestToken(accessor, null);
    }

    public void getRequestToken(OAuthAccessor accessor, String httpMethod) throws IOException, OAuthException, URISyntaxException {
        getRequestToken(accessor, httpMethod, null);
    }

    public void getRequestToken(OAuthAccessor accessor, String httpMethod, Collection<? extends Map.Entry> parameters) throws IOException, OAuthException, URISyntaxException {
        getRequestTokenResponse(accessor, httpMethod, parameters);
    }

    public OAuthMessage getRequestTokenResponse(OAuthAccessor accessor, String httpMethod, Collection<? extends Map.Entry> parameters) throws IOException, OAuthException, URISyntaxException {
        List<Map.Entry> p;
        accessor.accessToken = null;
        accessor.tokenSecret = null;
        Object accessorSecret = accessor.getProperty(OAuthConsumer.ACCESSOR_SECRET);
        if (accessorSecret != null) {
            if (parameters == null) {
                p = new ArrayList<>(1);
            } else {
                p = new ArrayList<>(parameters);
            }
            p.add(new OAuth.Parameter(OAuthConsumer.ACCESSOR_SECRET, accessorSecret.toString()));
            parameters = p;
        }
        OAuthMessage response = invoke(accessor, httpMethod, accessor.consumer.serviceProvider.requestTokenURL, parameters);
        accessor.requestToken = response.getParameter(OAuth.OAUTH_TOKEN);
        accessor.tokenSecret = response.getParameter(OAuth.OAUTH_TOKEN_SECRET);
        response.requireParameters(OAuth.OAUTH_TOKEN, OAuth.OAUTH_TOKEN_SECRET);
        return response;
    }

    public OAuthMessage getAccessToken(OAuthAccessor accessor, String httpMethod, Collection<? extends Map.Entry> parameters) throws IOException, OAuthException, URISyntaxException {
        if (accessor.requestToken != null) {
            if (parameters == null) {
                parameters = OAuth.newList(OAuth.OAUTH_TOKEN, accessor.requestToken);
            } else if (!OAuth.newMap(parameters).containsKey(OAuth.OAUTH_TOKEN)) {
                List<Map.Entry> p = new ArrayList<>(parameters);
                p.add(new OAuth.Parameter(OAuth.OAUTH_TOKEN, accessor.requestToken));
                parameters = p;
            }
        }
        OAuthMessage response = invoke(accessor, httpMethod, accessor.consumer.serviceProvider.accessTokenURL, parameters);
        response.requireParameters(OAuth.OAUTH_TOKEN, OAuth.OAUTH_TOKEN_SECRET);
        accessor.accessToken = response.getParameter(OAuth.OAUTH_TOKEN);
        accessor.tokenSecret = response.getParameter(OAuth.OAUTH_TOKEN_SECRET);
        return response;
    }

    public OAuthMessage invoke(OAuthAccessor accessor, String httpMethod, String url, Collection<? extends Map.Entry> parameters) throws IOException, OAuthException, URISyntaxException {
        ParameterStyle style;
        OAuthMessage request = accessor.newRequestMessage(httpMethod, url, parameters);
        Object accepted = accessor.consumer.getProperty("HTTP.header.Accept-Encoding");
        if (accepted != null) {
            request.getHeaders().add(new OAuth.Parameter(HttpMessage.ACCEPT_ENCODING, accepted.toString()));
        }
        Object ps = accessor.consumer.getProperty(PARAMETER_STYLE);
        if (ps == null) {
            style = ParameterStyle.BODY;
        } else {
            style = (ParameterStyle) Enum.valueOf(ParameterStyle.class, ps.toString());
        }
        return invoke(request, style);
    }

    public OAuthMessage invoke(OAuthAccessor accessor, String url, Collection<? extends Map.Entry> parameters) throws IOException, OAuthException, URISyntaxException {
        return invoke(accessor, null, url, parameters);
    }

    public OAuthMessage invoke(OAuthMessage request, ParameterStyle style) throws IOException, OAuthException {
        OAuthResponseMessage response = access(request, style);
        if (response.getHttpResponse().getStatusCode() / 100 == 2) {
            return response;
        }
        OAuthProblemException problem = response.toOAuthProblemException();
        try {
            problem.setParameter(OAuthProblemException.SIGNATURE_BASE_STRING, OAuthSignatureMethod.getBaseString(request));
        } catch (Exception e) {
        }
        throw problem;
    }

    public OAuthResponseMessage access(OAuthMessage request, ParameterStyle style) throws IOException {
        return new OAuthResponseMessage(HttpMessageDecoder.decode(this.http.execute(HttpMessage.newRequest(request, style), this.httpParameters)));
    }
}
