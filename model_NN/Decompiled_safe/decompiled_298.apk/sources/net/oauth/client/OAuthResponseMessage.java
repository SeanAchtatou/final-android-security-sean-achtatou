package net.oauth.client;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import net.oauth.OAuth;
import net.oauth.OAuthMessage;
import net.oauth.OAuthProblemException;
import net.oauth.http.HttpResponseMessage;

public class OAuthResponseMessage extends OAuthMessage {
    private final HttpResponseMessage http;

    OAuthResponseMessage(HttpResponseMessage http2) throws IOException {
        super(http2.method, http2.url.toExternalForm(), null);
        this.http = http2;
        getHeaders().addAll(http2.headers);
        for (Map.Entry<String, String> header : http2.headers) {
            if ("WWW-Authenticate".equalsIgnoreCase((String) header.getKey())) {
                for (OAuth.Parameter parameter : decodeAuthorization((String) header.getValue())) {
                    if (!"realm".equalsIgnoreCase(parameter.getKey())) {
                        addParameter(parameter);
                    }
                }
            }
        }
    }

    public HttpResponseMessage getHttpResponse() {
        return this.http;
    }

    public InputStream getBodyAsStream() throws IOException {
        return this.http.getBody();
    }

    public String getBodyEncoding() {
        return this.http.getContentCharset();
    }

    public void requireParameters(String... names) throws OAuthProblemException, IOException {
        try {
            super.requireParameters(names);
        } catch (OAuthProblemException problem) {
            problem.getParameters().putAll(getDump());
            throw problem;
        }
    }

    public OAuthProblemException toOAuthProblemException() throws IOException {
        OAuthProblemException problem = new OAuthProblemException();
        try {
            getParameters();
        } catch (IOException | IllegalArgumentException e) {
        }
        problem.getParameters().putAll(getDump());
        try {
            InputStream b = getBodyAsStream();
            if (b != null) {
                b.close();
            }
        } catch (IOException e2) {
        }
        return problem;
    }

    /* access modifiers changed from: protected */
    public void completeParameters() throws IOException {
        super.completeParameters();
        String body = readBodyAsString();
        if (body != null) {
            addParameters(OAuth.decodeForm(body.trim()));
        }
    }

    /* access modifiers changed from: protected */
    public void dump(Map<String, Object> into) throws IOException {
        super.dump(into);
        this.http.dump(into);
    }
}
