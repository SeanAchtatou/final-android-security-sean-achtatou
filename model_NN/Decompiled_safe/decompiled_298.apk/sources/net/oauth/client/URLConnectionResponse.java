package net.oauth.client;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import net.oauth.OAuth;
import net.oauth.http.HttpMessage;
import net.oauth.http.HttpResponseMessage;

public class URLConnectionResponse extends HttpResponseMessage {
    private final URLConnection connection;
    private final String requestEncoding;
    private final byte[] requestExcerpt;
    private final String requestHeaders;

    public URLConnectionResponse(HttpMessage request, String requestHeaders2, byte[] requestExcerpt2, URLConnection connection2) throws IOException {
        super(request.method, request.url);
        this.requestHeaders = requestHeaders2;
        this.requestExcerpt = requestExcerpt2;
        this.requestEncoding = request.getContentCharset();
        this.connection = connection2;
        this.headers.addAll(getHeaders());
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public int getStatusCode() throws IOException {
        if (this.connection instanceof HttpURLConnection) {
            return ((HttpURLConnection) this.connection).getResponseCode();
        }
        return HttpResponseMessage.STATUS_OK;
    }

    public InputStream openBody() {
        try {
            return this.connection.getInputStream();
        } catch (IOException e) {
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public String getHeaderField(URLConnection connection2, int index) {
        try {
            return connection2.getHeaderField(index);
        } catch (NoSuchElementException e) {
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public String getHeaderFieldKey(URLConnection connection2, int index) {
        try {
            return connection2.getHeaderFieldKey(index);
        } catch (NoSuchElementException e) {
            return null;
        }
    }

    private List<Map.Entry<String, String>> getHeaders() {
        List<Map.Entry<String, String>> headers = new ArrayList<>();
        boolean foundContentType = false;
        int i = 0;
        while (true) {
            String value = getHeaderField(this.connection, i);
            if (value == null) {
                break;
            }
            String name = getHeaderFieldKey(this.connection, i);
            if (name != null) {
                headers.add(new OAuth.Parameter(name, value));
                if (HttpMessage.CONTENT_TYPE.equalsIgnoreCase(name)) {
                    foundContentType = true;
                }
            }
            i++;
        }
        if (!foundContentType) {
            headers.add(new OAuth.Parameter(HttpMessage.CONTENT_TYPE, this.connection.getContentType()));
        }
        return headers;
    }

    public void dump(Map<String, Object> into) throws IOException {
        HttpURLConnection http;
        super.dump(into);
        StringBuilder request = new StringBuilder(this.requestHeaders);
        request.append(HttpResponseMessage.EOL);
        if (this.requestExcerpt != null) {
            request.append(new String(this.requestExcerpt, this.requestEncoding));
        }
        into.put("HTTP request", request.toString());
        if (this.connection instanceof HttpURLConnection) {
            http = (HttpURLConnection) this.connection;
        } else {
            http = null;
        }
        StringBuilder response = new StringBuilder();
        int i = 0;
        while (true) {
            String value = getHeaderField(this.connection, i);
            if (value == null) {
                break;
            }
            String name = getHeaderFieldKey(this.connection, i);
            if (!(i != 0 || name == null || http == null)) {
                String firstLine = "HTTP " + getStatusCode();
                String message = http.getResponseMessage();
                if (message != null) {
                    firstLine = String.valueOf(firstLine) + " " + message;
                }
                response.append(firstLine).append(HttpResponseMessage.EOL);
            }
            if (name != null) {
                response.append(name).append(": ");
                String name2 = name.toLowerCase();
            }
            response.append(value).append(HttpResponseMessage.EOL);
            i++;
        }
        response.append(HttpResponseMessage.EOL);
        if (this.body != null) {
            response.append(new String(((ExcerptInputStream) this.body).getExcerpt(), getContentCharset()));
        }
        into.put("HTTP response", response.toString());
    }
}
