package net.oauth;

import bostone.android.hireadroid.dao.SearchItemsDbHelper;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class ConsumerProperties {
    private final Properties consumerProperties;
    private final Map<String, OAuthConsumer> pool;

    public static URL getResource(String name, ClassLoader loader) throws IOException {
        URL resource = loader.getResource(name);
        if (resource != null) {
            return resource;
        }
        throw new IOException("resource not found: " + name);
    }

    public static Properties getProperties(URL source) throws IOException {
        InputStream input = source.openStream();
        try {
            Properties p = new Properties();
            p.load(input);
            return p;
        } finally {
            input.close();
        }
    }

    public ConsumerProperties(String resourceName, ClassLoader loader) throws IOException {
        this(getProperties(getResource(resourceName, loader)));
    }

    public ConsumerProperties(Properties consumerProperties2) {
        this.pool = new HashMap();
        this.consumerProperties = consumerProperties2;
    }

    public OAuthConsumer getConsumer(String name) throws MalformedURLException {
        OAuthConsumer consumer;
        synchronized (this.pool) {
            consumer = this.pool.get(name);
        }
        if (consumer == null) {
            consumer = newConsumer(name);
        }
        synchronized (this.pool) {
            OAuthConsumer first = this.pool.get(name);
            if (first == null) {
                this.pool.put(name, consumer);
            } else {
                consumer = first;
            }
        }
        return consumer;
    }

    /* access modifiers changed from: protected */
    public OAuthConsumer newConsumer(String name) throws MalformedURLException {
        URL baseURL;
        String base = this.consumerProperties.getProperty(String.valueOf(name) + ".serviceProvider.baseURL");
        if (base == null) {
            baseURL = null;
        } else {
            baseURL = new URL(base);
        }
        OAuthConsumer consumer = new OAuthConsumer(this.consumerProperties.getProperty(String.valueOf(name) + ".callbackURL"), this.consumerProperties.getProperty(String.valueOf(name) + ".consumerKey"), this.consumerProperties.getProperty(String.valueOf(name) + ".consumerSecret"), new OAuthServiceProvider(getURL(baseURL, String.valueOf(name) + ".serviceProvider.requestTokenURL"), getURL(baseURL, String.valueOf(name) + ".serviceProvider.userAuthorizationURL"), getURL(baseURL, String.valueOf(name) + ".serviceProvider.accessTokenURL")));
        consumer.setProperty(SearchItemsDbHelper.ItemColumns.LOC_NAME, name);
        if (baseURL != null) {
            consumer.setProperty("serviceProvider.baseURL", baseURL);
        }
        for (Map.Entry prop : this.consumerProperties.entrySet()) {
            String propName = (String) prop.getKey();
            if (propName.startsWith(String.valueOf(name) + ".consumer.")) {
                consumer.setProperty(propName.substring(name.length() + 10), prop.getValue());
            }
        }
        return consumer;
    }

    private String getURL(URL base, String name) throws MalformedURLException {
        String url = this.consumerProperties.getProperty(name);
        if (base != null) {
            return new URL(base, url).toExternalForm();
        }
        return url;
    }
}
