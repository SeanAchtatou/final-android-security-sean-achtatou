package net.oauth;

import java.util.HashMap;
import java.util.Map;
import net.oauth.OAuth;
import net.oauth.http.HttpResponseMessage;

public class OAuthProblemException extends OAuthException {
    public static final String HTTP_LOCATION = "Location";
    public static final String HTTP_REQUEST = "HTTP request";
    public static final String HTTP_RESPONSE = "HTTP response";
    public static final String HTTP_STATUS_CODE = "HTTP status";
    public static final String OAUTH_PROBLEM = "oauth_problem";
    public static final String SIGNATURE_BASE_STRING = "oauth_signature base string";
    public static final String URL = "URL";
    private static final long serialVersionUID = 1;
    private final Map<String, Object> parameters = new HashMap();

    public OAuthProblemException() {
    }

    public OAuthProblemException(String problem) {
        super(problem);
        if (problem != null) {
            this.parameters.put(OAUTH_PROBLEM, problem);
        }
    }

    /* JADX INFO: Multiple debug info for r2v1 java.lang.Integer: [D('response' java.lang.Integer), D('response' java.lang.Object)] */
    public String getMessage() {
        String msg = super.getMessage();
        if (msg != null) {
            return msg;
        }
        String msg2 = getProblem();
        if (msg2 != null) {
            return msg2;
        }
        Object response = getParameters().get("HTTP response");
        if (response != null) {
            String msg3 = response.toString();
            int eol = msg3.indexOf("\n");
            if (eol < 0) {
                eol = msg3.indexOf("\r");
            }
            if (eol >= 0) {
                msg3 = msg3.substring(0, eol);
            }
            String msg4 = msg3.trim();
            if (msg4.length() > 0) {
                return msg4;
            }
        }
        Object response2 = Integer.valueOf(getHttpStatusCode());
        if (response2 != null) {
            return "HTTP status " + response2;
        }
        return null;
    }

    public void setParameter(String name, Object value) {
        getParameters().put(name, value);
    }

    public Map<String, Object> getParameters() {
        return this.parameters;
    }

    public String getProblem() {
        return (String) getParameters().get(OAUTH_PROBLEM);
    }

    public int getHttpStatusCode() {
        Object code = getParameters().get("HTTP status");
        if (code == null) {
            return HttpResponseMessage.STATUS_OK;
        }
        if (code instanceof Number) {
            return ((Number) code).intValue();
        }
        return Integer.parseInt(code.toString());
    }

    public String toString() {
        StringBuilder s = new StringBuilder(super.toString());
        try {
            String eol = System.getProperty("line.separator", "\n");
            Map<String, Object> parameters2 = getParameters();
            for (String key : new String[]{OAuth.Problems.OAUTH_PROBLEM_ADVICE, URL, SIGNATURE_BASE_STRING}) {
                Object value = parameters2.get(key);
                if (value != null) {
                    s.append(String.valueOf(eol) + key + ": " + value);
                }
            }
            Object msg = parameters2.get("HTTP request");
            if (msg != null) {
                s.append(String.valueOf(eol) + ">>>>>>>> " + "HTTP request" + ":" + eol + msg);
            }
            Object msg2 = parameters2.get("HTTP response");
            if (msg2 != null) {
                s.append(String.valueOf(eol) + "<<<<<<<< " + "HTTP response" + ":" + eol + msg2);
            } else {
                for (Map.Entry<String, Object> parameter : parameters2.entrySet()) {
                    String key2 = (String) parameter.getKey();
                    if (!OAuth.Problems.OAUTH_PROBLEM_ADVICE.equals(key2) && !URL.equals(key2) && !SIGNATURE_BASE_STRING.equals(key2) && !"HTTP request".equals(key2) && !"HTTP response".equals(key2)) {
                        s.append(String.valueOf(eol) + key2 + ": " + parameter.getValue());
                    }
                }
            }
        } catch (Exception e) {
        }
        return s.toString();
    }
}
