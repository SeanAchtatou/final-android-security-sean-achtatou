package com.flurry.android;

final class ac implements Runnable {
    private /* synthetic */ String a;
    private /* synthetic */ q b;

    ac(q qVar, String str) {
        this.b = qVar;
        this.a = str;
    }

    public final void run() {
        CallbackEvent callbackEvent = new CallbackEvent(CallbackEvent.ERROR_MARKET_LAUNCH);
        callbackEvent.setMessage(this.a);
        this.b.y.onMarketAppLaunchError(callbackEvent);
    }
}
