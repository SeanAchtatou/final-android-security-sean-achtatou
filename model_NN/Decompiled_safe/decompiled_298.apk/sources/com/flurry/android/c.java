package com.flurry.android;

import android.content.Context;

final class c implements Runnable {
    final /* synthetic */ Context a;
    final /* synthetic */ FlurryAgent b;
    private /* synthetic */ boolean c;

    c(FlurryAgent flurryAgent, boolean z, Context context) {
        this.b = flurryAgent;
        this.c = z;
        this.a = context;
    }

    public final void run() {
        FlurryAgent.c(this.b);
        this.b.i();
        if (!this.c) {
            this.b.n.postDelayed(new o(this), FlurryAgent.g);
        }
        if (FlurryAgent.l) {
            this.b.U.b();
        }
    }
}
