package com.flurry.android;

final class j {
    final byte a;
    final long b;

    j(byte b2, long j) {
        this.a = b2;
        this.b = j;
    }

    public final String toString() {
        return "[" + this.b + "] " + ((int) this.a);
    }
}
