package org.acra;

import android.util.Log;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Iterator;
import java.util.Map;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import net.oauth.OAuth;
import org.apache.http.conn.ssl.AllowAllHostnameVerifier;

class HttpUtils {
    private static final AllowAllHostnameVerifier HOSTNAME_VERIFIER = new AllowAllHostnameVerifier();
    private static final String LOG_TAG = ACRA.LOG_TAG;
    private static final int SOCKET_TIMEOUT = 3000;
    private static final TrustManager[] TRUST_MANAGER = {new NaiveTrustManager()};

    HttpUtils() {
    }

    static void doPost(Map<?, ?> parameters, URL url) throws UnsupportedEncodingException, IOException, KeyManagementException, NoSuchAlgorithmException {
        URLConnection cnx = getConnection(url);
        StringBuilder dataBfr = new StringBuilder();
        Iterator<?> iKeys = parameters.keySet().iterator();
        while (iKeys.hasNext()) {
            if (dataBfr.length() != 0) {
                dataBfr.append('&');
            }
            String key = (String) iKeys.next();
            dataBfr.append(URLEncoder.encode(key, OAuth.ENCODING)).append('=').append(URLEncoder.encode((String) parameters.get(key), OAuth.ENCODING));
        }
        cnx.setDoOutput(true);
        OutputStreamWriter wr = new OutputStreamWriter(cnx.getOutputStream());
        Log.d(LOG_TAG, "Posting crash report data");
        wr.write(dataBfr.toString());
        wr.flush();
        wr.close();
        Log.d(LOG_TAG, "Reading response");
        BufferedReader rd = new BufferedReader(new InputStreamReader(cnx.getInputStream()));
        int linecount = 0;
        while (true) {
            String line = rd.readLine();
            if (line == null) {
                rd.close();
                return;
            }
            linecount++;
            if (linecount <= 2) {
                Log.d(LOG_TAG, line);
            }
        }
    }

    private static URLConnection getConnection(URL url) throws IOException, NoSuchAlgorithmException, KeyManagementException {
        URLConnection conn = url.openConnection();
        if (conn instanceof HttpsURLConnection) {
            SSLContext context = SSLContext.getInstance("TLS");
            context.init(new KeyManager[0], TRUST_MANAGER, new SecureRandom());
            ((HttpsURLConnection) conn).setSSLSocketFactory(context.getSocketFactory());
            ((HttpsURLConnection) conn).setHostnameVerifier(HOSTNAME_VERIFIER);
        }
        conn.setConnectTimeout(SOCKET_TIMEOUT);
        conn.setReadTimeout(SOCKET_TIMEOUT);
        return conn;
    }
}
