package org.acra;

import android.app.Application;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import org.acra.annotation.ReportsCrashes;

public class ACRA {
    private static /* synthetic */ int[] $SWITCH_TABLE$org$acra$ReportingInteractionMode = null;
    protected static final String LOG_TAG = ACRA.class.getSimpleName();
    static final int NOTIF_CRASH_ID = 666;
    public static final String PREF_DISABLE_ACRA = "acra.disable";
    public static final String PREF_ENABLE_ACRA = "acra.enable";
    static final String RES_DIALOG_COMMENT_PROMPT = "RES_DIALOG_COMMENT_PROMPT";
    static final String RES_DIALOG_ICON = "RES_DIALOG_ICON";
    static final String RES_DIALOG_OK_TOAST = "RES_DIALOG_OK_TOAST";
    static final String RES_DIALOG_TEXT = "RES_DIALOG_TEXT";
    static final String RES_DIALOG_TITLE = "RES_DIALOG_TITLE";
    static final String RES_NOTIF_ICON = "RES_NOTIF_ICON";
    static final String RES_NOTIF_TEXT = "RES_NOTIF_TEXT";
    static final String RES_NOTIF_TICKER_TEXT = "RES_NOTIF_TICKER_TEXT";
    static final String RES_NOTIF_TITLE = "RES_NOTIF_TITLE";
    static final String RES_TOAST_TEXT = "RES_TOAST_TEXT";
    private static Application mApplication;
    private static Bundle mCrashResources;
    private static SharedPreferences.OnSharedPreferenceChangeListener mPrefListener;
    private static ReportsCrashes mReportsCrashes;

    static /* synthetic */ int[] $SWITCH_TABLE$org$acra$ReportingInteractionMode() {
        int[] iArr = $SWITCH_TABLE$org$acra$ReportingInteractionMode;
        if (iArr == null) {
            iArr = new int[ReportingInteractionMode.values().length];
            try {
                iArr[ReportingInteractionMode.NOTIFICATION.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[ReportingInteractionMode.SILENT.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[ReportingInteractionMode.TOAST.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$org$acra$ReportingInteractionMode = iArr;
        }
        return iArr;
    }

    public static void init(Application app) {
        boolean z;
        mApplication = app;
        mReportsCrashes = (ReportsCrashes) mApplication.getClass().getAnnotation(ReportsCrashes.class);
        if (mReportsCrashes != null) {
            SharedPreferences prefs = getACRASharedPreferences();
            Log.d(LOG_TAG, "Set OnSharedPreferenceChangeListener.");
            mPrefListener = new SharedPreferences.OnSharedPreferenceChangeListener() {
                public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
                    boolean z;
                    Log.d(ACRA.LOG_TAG, "Preferences changed, check if ACRA configuration must change.");
                    if (ACRA.PREF_DISABLE_ACRA.equals(key) || ACRA.PREF_ENABLE_ACRA.equals(key)) {
                        Boolean disableAcra = false;
                        try {
                            if (sharedPreferences.getBoolean(ACRA.PREF_ENABLE_ACRA, true)) {
                                z = false;
                            } else {
                                z = true;
                            }
                            disableAcra = Boolean.valueOf(sharedPreferences.getBoolean(ACRA.PREF_DISABLE_ACRA, z));
                        } catch (Exception e) {
                        }
                        if (disableAcra.booleanValue()) {
                            ErrorReporter.getInstance().disable();
                            return;
                        }
                        try {
                            ACRA.initAcra();
                        } catch (ACRAConfigurationException e2) {
                            Log.w(ACRA.LOG_TAG, "Error : ", e2);
                        }
                    }
                }
            };
            boolean disableAcra = false;
            try {
                if (prefs.getBoolean(PREF_ENABLE_ACRA, true)) {
                    z = false;
                } else {
                    z = true;
                }
                disableAcra = prefs.getBoolean(PREF_DISABLE_ACRA, z);
            } catch (Exception e) {
            }
            if (disableAcra) {
                Log.d(LOG_TAG, "ACRA is disabled for " + mApplication.getPackageName() + ".");
                return;
            }
            try {
                initAcra();
            } catch (ACRAConfigurationException e2) {
                Log.w(LOG_TAG, "Error : ", e2);
            }
            prefs.registerOnSharedPreferenceChangeListener(mPrefListener);
        }
    }

    /* access modifiers changed from: private */
    public static void initAcra() throws ACRAConfigurationException {
        initCrashResources();
        Log.d(LOG_TAG, "ACRA is enabled for " + mApplication.getPackageName() + ", intializing...");
        ErrorReporter errorReporter = ErrorReporter.getInstance();
        errorReporter.setFormUri(getFormUri());
        errorReporter.setReportingInteractionMode(mReportsCrashes.mode());
        errorReporter.setCrashResources(getCrashResources());
        errorReporter.init(mApplication.getApplicationContext());
        errorReporter.checkReportsOnApplicationStart();
    }

    static void initCrashResources() throws ACRAConfigurationException {
        mCrashResources = new Bundle();
        switch ($SWITCH_TABLE$org$acra$ReportingInteractionMode()[mReportsCrashes.mode().ordinal()]) {
            case 2:
                if (mReportsCrashes.resNotifTickerText() == 0 || mReportsCrashes.resNotifTitle() == 0 || mReportsCrashes.resNotifText() == 0 || mReportsCrashes.resDialogText() == 0) {
                    throw new ACRAConfigurationException("NOTIFICATION mode: you have to define at least the resNotifTickerText, resNotifTitle, resNotifText, resDialogText parameters in your application @ReportsCrashes() annotation.");
                }
                mCrashResources.putInt(RES_NOTIF_TICKER_TEXT, mReportsCrashes.resNotifTickerText());
                mCrashResources.putInt(RES_NOTIF_TITLE, mReportsCrashes.resNotifTitle());
                mCrashResources.putInt(RES_NOTIF_TEXT, mReportsCrashes.resNotifText());
                mCrashResources.putInt(RES_DIALOG_TEXT, mReportsCrashes.resDialogText());
                mCrashResources.putInt(RES_NOTIF_ICON, mReportsCrashes.resNotifIcon());
                mCrashResources.putInt(RES_DIALOG_ICON, mReportsCrashes.resDialogIcon());
                mCrashResources.putInt(RES_DIALOG_TITLE, mReportsCrashes.resDialogTitle());
                mCrashResources.putInt(RES_DIALOG_COMMENT_PROMPT, mReportsCrashes.resDialogCommentPrompt());
                mCrashResources.putInt(RES_DIALOG_OK_TOAST, mReportsCrashes.resDialogOkToast());
                return;
            case 3:
                if (mReportsCrashes.resToastText() == 0) {
                    throw new ACRAConfigurationException("TOAST mode: you have to define the resToastText parameter in your application @ReportsCrashes() annotation.");
                }
                mCrashResources.putInt(RES_TOAST_TEXT, mReportsCrashes.resToastText());
                return;
            default:
                return;
        }
    }

    static Bundle getCrashResources() {
        return mCrashResources;
    }

    private static Uri getFormUri() {
        if (mReportsCrashes.formUri().equals("")) {
            return Uri.parse("http://spreadsheets.google.com/formResponse?formkey=" + mReportsCrashes.formKey() + "&amp;ifq");
        }
        return Uri.parse(mReportsCrashes.formUri());
    }

    public static SharedPreferences getACRASharedPreferences() {
        if (!"".equals(mReportsCrashes.sharedPreferencesName())) {
            Log.d(LOG_TAG, "Retrieve SharedPreferences " + mReportsCrashes.sharedPreferencesName());
            return mApplication.getSharedPreferences(mReportsCrashes.sharedPreferencesName(), mReportsCrashes.sharedPreferencesMode());
        }
        Log.d(LOG_TAG, "Retrieve application default SharedPreferences.");
        return PreferenceManager.getDefaultSharedPreferences(mApplication);
    }
}
