package org.acra;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Looper;
import android.os.Process;
import android.os.StatFs;
import android.text.format.Time;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import android.widget.Toast;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.lang.Thread;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.TreeSet;

public class ErrorReporter implements Thread.UncaughtExceptionHandler {
    private static final String ANDROID_VERSION_KEY = "entry.4.single";
    private static final String AVAILABLE_MEM_SIZE_KEY = "entry.19.single";
    private static final String BOARD_KEY = "entry.5.single";
    private static final String BRAND_KEY = "entry.6.single";
    private static final String BUILD_DISPLAY_KEY = "entry.8.single";
    private static final String CRASH_CONFIGURATION_KEY = "entry.23.single";
    private static final String CUSTOM_DATA_KEY = "entry.20.single";
    private static final String DEVICE_KEY = "entry.7.single";
    private static final String DISPLAY_KEY = "entry.24.single";
    static final String EXTRA_REPORT_FILE_NAME = "REPORT_FILE_NAME";
    private static final String FILE_PATH_KEY = "entry.2.single";
    private static final String FINGERPRINT_KEY = "entry.9.single";
    private static final String HOST_KEY = "entry.10.single";
    private static final String ID_KEY = "entry.11.single";
    private static final String INITIAL_CONFIGURATION_KEY = "entry.22.single";
    static final String IS_SILENT_KEY = "silent";
    private static final String LOG_TAG = ACRA.LOG_TAG;
    private static final int MAX_SEND_REPORTS = 5;
    private static final String MODEL_KEY = "entry.12.single";
    private static final String PACKAGE_NAME_KEY = "entry.1.single";
    private static final String PHONE_MODEL_KEY = "entry.3.single";
    private static final String PRODUCT_KEY = "entry.13.single";
    static final String SILENT_SUFFIX = "-silent";
    private static final String STACK_TRACE_KEY = "entry.21.single";
    private static final String TAGS_KEY = "entry.14.single";
    private static final String TIME_KEY = "entry.15.single";
    private static final String TOTAL_MEM_SIZE_KEY = "entry.18.single";
    private static final String TYPE_KEY = "entry.16.single";
    private static final String USER_COMMENT_KEY = "entry.25.single";
    private static final String USER_CRASH_DATE_KEY = "entry.26.single";
    private static final String USER_KEY = "entry.17.single";
    private static final String VERSION_NAME_KEY = "entry.0.single";
    private static Uri mFormUri;
    private static ErrorReporter mInstanceSingleton;
    static String mUserComment = "";
    /* access modifiers changed from: private */
    public Context mContext;
    private Properties mCrashProperties = new Properties();
    /* access modifiers changed from: private */
    public Bundle mCrashResources = new Bundle();
    Map<String, String> mCustomParameters = new HashMap();
    private Thread.UncaughtExceptionHandler mDfltExceptionHandler;
    private String mInitialConfiguration;
    private ReportingInteractionMode mReportingInteractionMode = ReportingInteractionMode.SILENT;

    final class ReportsSenderWorker extends Thread {
        private String mReportFileName = null;
        private boolean mSendOnlySilentReports = false;

        public ReportsSenderWorker(boolean sendOnlySilentReports) {
            this.mSendOnlySilentReports = sendOnlySilentReports;
        }

        public ReportsSenderWorker() {
        }

        public void run() {
            ErrorReporter.this.checkAndSendReports(ErrorReporter.this.mContext, this.mReportFileName, this.mSendOnlySilentReports);
        }

        /* access modifiers changed from: package-private */
        public void setCommentReportFileName(String reportFileName) {
            this.mReportFileName = reportFileName;
        }
    }

    /* access modifiers changed from: package-private */
    public void setFormUri(Uri formUri) {
        mFormUri = formUri;
    }

    @Deprecated
    public void addCustomData(String key, String value) {
        this.mCustomParameters.put(key, value);
    }

    public String putCustomData(String key, String value) {
        return this.mCustomParameters.put(key, value);
    }

    public String removeCustomData(String key) {
        return this.mCustomParameters.remove(key);
    }

    public String getCustomData(String key) {
        return this.mCustomParameters.get(key);
    }

    private String createCustomInfoString() {
        String CustomInfo = "";
        for (String CurrentKey : this.mCustomParameters.keySet()) {
            CustomInfo = String.valueOf(CustomInfo) + CurrentKey + " = " + this.mCustomParameters.get(CurrentKey) + "\n";
        }
        return CustomInfo;
    }

    public static ErrorReporter getInstance() {
        if (mInstanceSingleton == null) {
            mInstanceSingleton = new ErrorReporter();
        }
        return mInstanceSingleton;
    }

    public void init(Context context) {
        if (this.mDfltExceptionHandler == null) {
            this.mDfltExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
            Thread.setDefaultUncaughtExceptionHandler(this);
            this.mContext = context;
            this.mInitialConfiguration = ConfigurationInspector.toString(this.mContext.getResources().getConfiguration());
        }
    }

    private static long getAvailableInternalMemorySize() {
        StatFs stat = new StatFs(Environment.getDataDirectory().getPath());
        return ((long) stat.getAvailableBlocks()) * ((long) stat.getBlockSize());
    }

    private static long getTotalInternalMemorySize() {
        StatFs stat = new StatFs(Environment.getDataDirectory().getPath());
        return ((long) stat.getBlockCount()) * ((long) stat.getBlockSize());
    }

    private void retrieveCrashData(Context context) {
        try {
            this.mCrashProperties.put(INITIAL_CONFIGURATION_KEY, this.mInitialConfiguration);
            this.mCrashProperties.put(CRASH_CONFIGURATION_KEY, ConfigurationInspector.toString(context.getResources().getConfiguration()));
            PackageInfo pi = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            if (pi != null) {
                this.mCrashProperties.put(VERSION_NAME_KEY, pi.versionName != null ? "'" + pi.versionName : "not set");
            } else {
                this.mCrashProperties.put(PACKAGE_NAME_KEY, "Package info unavailable");
            }
            this.mCrashProperties.put(PACKAGE_NAME_KEY, context.getPackageName());
            this.mCrashProperties.put(PHONE_MODEL_KEY, Build.MODEL);
            this.mCrashProperties.put(ANDROID_VERSION_KEY, "'" + Build.VERSION.RELEASE);
            this.mCrashProperties.put(BOARD_KEY, Build.BOARD);
            this.mCrashProperties.put(BRAND_KEY, Build.BRAND);
            this.mCrashProperties.put(DEVICE_KEY, Build.DEVICE);
            this.mCrashProperties.put(BUILD_DISPLAY_KEY, Build.DISPLAY);
            this.mCrashProperties.put(FINGERPRINT_KEY, Build.FINGERPRINT);
            this.mCrashProperties.put(HOST_KEY, Build.HOST);
            this.mCrashProperties.put(ID_KEY, Build.ID);
            this.mCrashProperties.put(MODEL_KEY, Build.MODEL);
            this.mCrashProperties.put(PRODUCT_KEY, Build.PRODUCT);
            this.mCrashProperties.put(TAGS_KEY, Build.TAGS);
            this.mCrashProperties.put(TIME_KEY, new StringBuilder().append(Build.TIME).toString());
            this.mCrashProperties.put(TYPE_KEY, Build.TYPE);
            this.mCrashProperties.put(USER_KEY, Build.USER);
            this.mCrashProperties.put(TOTAL_MEM_SIZE_KEY, new StringBuilder().append(getTotalInternalMemorySize()).toString());
            this.mCrashProperties.put(AVAILABLE_MEM_SIZE_KEY, new StringBuilder().append(getAvailableInternalMemorySize()).toString());
            this.mCrashProperties.put(FILE_PATH_KEY, context.getFilesDir().getAbsolutePath());
            this.mCrashProperties.put(DISPLAY_KEY, toString(((WindowManager) context.getSystemService("window")).getDefaultDisplay()));
            Time curDate = new Time();
            curDate.setToNow();
            this.mCrashProperties.put(USER_CRASH_DATE_KEY, curDate.format3339(false));
            this.mCrashProperties.put(CUSTOM_DATA_KEY, createCustomInfoString());
        } catch (Exception e) {
            Log.e(LOG_TAG, "Error while retrieving crash data", e);
        }
    }

    private static String toString(Display display) {
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);
        StringBuilder result = new StringBuilder();
        result.append("width=").append(display.getWidth()).append(10).append("height=").append(display.getHeight()).append(10).append("pixelFormat=").append(display.getPixelFormat()).append(10).append("refreshRate=").append(display.getRefreshRate()).append("fps").append(10).append("metrics.density=x").append(metrics.density).append(10).append("metrics.scaledDensity=x").append(metrics.scaledDensity).append(10).append("metrics.widthPixels=").append(metrics.widthPixels).append(10).append("metrics.heightPixels=").append(metrics.heightPixels).append(10).append("metrics.xdpi=").append(metrics.xdpi).append(10).append("metrics.ydpi=").append(metrics.ydpi);
        return result.toString();
    }

    public void uncaughtException(Thread t, Throwable e) {
        ReportsSenderWorker worker = handleException(e);
        if (this.mReportingInteractionMode == ReportingInteractionMode.TOAST) {
            try {
                Thread.sleep(4000);
            } catch (InterruptedException e1) {
                Log.e(LOG_TAG, "Error : ", e1);
            }
        }
        if (worker != null) {
            while (worker.isAlive()) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e12) {
                    Log.e(LOG_TAG, "Error : ", e12);
                }
            }
        }
        if (this.mReportingInteractionMode == ReportingInteractionMode.SILENT) {
            this.mDfltExceptionHandler.uncaughtException(t, e);
            return;
        }
        try {
            Log.e(LOG_TAG, ((Object) this.mContext.getPackageManager().getApplicationInfo(this.mContext.getPackageName(), 0).loadLabel(this.mContext.getPackageManager())) + " fatal error : " + e.getMessage(), e);
        } catch (PackageManager.NameNotFoundException e2) {
            Log.e(LOG_TAG, "Error : ", e2);
        } finally {
            Process.killProcess(Process.myPid());
            System.exit(10);
        }
    }

    /* access modifiers changed from: package-private */
    public ReportsSenderWorker handleException(Throwable e, ReportingInteractionMode reportingInteractionMode) {
        boolean sendOnlySilentReports = false;
        if (reportingInteractionMode == null) {
            reportingInteractionMode = this.mReportingInteractionMode;
        } else if (reportingInteractionMode == ReportingInteractionMode.SILENT && this.mReportingInteractionMode != ReportingInteractionMode.SILENT) {
            sendOnlySilentReports = true;
        }
        if (e == null) {
            e = new Exception("Report requested by developer");
        }
        if (reportingInteractionMode == ReportingInteractionMode.TOAST) {
            new Thread() {
                public void run() {
                    Looper.prepare();
                    Toast.makeText(ErrorReporter.this.mContext, ErrorReporter.this.mCrashResources.getInt("RES_TOAST_TEXT"), 1).show();
                    Looper.loop();
                }
            }.start();
        }
        retrieveCrashData(this.mContext);
        Writer result = new StringWriter();
        PrintWriter printWriter = new PrintWriter(result);
        e.printStackTrace(printWriter);
        Log.getStackTraceString(e);
        for (Throwable cause = e.getCause(); cause != null; cause = cause.getCause()) {
            cause.printStackTrace(printWriter);
        }
        this.mCrashProperties.put(STACK_TRACE_KEY, result.toString());
        printWriter.close();
        String reportFileName = saveCrashReportFile();
        if (reportingInteractionMode == ReportingInteractionMode.SILENT || reportingInteractionMode == ReportingInteractionMode.TOAST) {
            ReportsSenderWorker wk = new ReportsSenderWorker(sendOnlySilentReports);
            wk.start();
            return wk;
        }
        if (reportingInteractionMode == ReportingInteractionMode.NOTIFICATION) {
            notifySendReport(reportFileName);
        }
        return null;
    }

    public ReportsSenderWorker handleException(Throwable e) {
        return handleException(e, this.mReportingInteractionMode);
    }

    public ReportsSenderWorker handleSilentException(Throwable e) {
        this.mCrashProperties.put(IS_SILENT_KEY, "true");
        return handleException(e, ReportingInteractionMode.SILENT);
    }

    /* access modifiers changed from: package-private */
    public void notifySendReport(String reportFileName) {
        NotificationManager notificationManager = (NotificationManager) this.mContext.getSystemService("notification");
        int icon = 17301624;
        if (this.mCrashResources.containsKey("RES_NOTIF_ICON")) {
            icon = this.mCrashResources.getInt("RES_NOTIF_ICON");
        }
        Notification notification = new Notification(icon, this.mContext.getText(this.mCrashResources.getInt("RES_NOTIF_TICKER_TEXT")), System.currentTimeMillis());
        CharSequence contentTitle = this.mContext.getText(this.mCrashResources.getInt("RES_NOTIF_TITLE"));
        CharSequence contentText = this.mContext.getText(this.mCrashResources.getInt("RES_NOTIF_TEXT"));
        Intent notificationIntent = new Intent(this.mContext, CrashReportDialog.class);
        notificationIntent.putExtra(EXTRA_REPORT_FILE_NAME, reportFileName);
        notification.setLatestEventInfo(this.mContext, contentTitle, contentText, PendingIntent.getActivity(this.mContext, 0, notificationIntent, 0));
        notificationManager.notify(666, notification);
    }

    private static void sendCrashReport(Context context, Properties errorContent) throws UnsupportedEncodingException, IOException, KeyManagementException, NoSuchAlgorithmException {
        errorContent.put("pageNumber", "0");
        errorContent.put("backupCache", "");
        errorContent.put("submit", "Envoyer");
        URL reportUrl = new URL(mFormUri.toString());
        Log.d(LOG_TAG, "Connect to " + reportUrl.toString());
        HttpUtils.doPost(errorContent, reportUrl);
    }

    private String saveCrashReportFile() {
        try {
            Log.d(LOG_TAG, "Writing crash report file.");
            Time now = new Time();
            now.setToNow();
            String fileName = now.toMillis(false) + (this.mCrashProperties.getProperty(IS_SILENT_KEY) != null ? SILENT_SUFFIX : "") + ".stacktrace";
            FileOutputStream trace = this.mContext.openFileOutput(fileName, 0);
            if (storeToXML()) {
                this.mCrashProperties.storeToXML(trace, "");
            } else {
                this.mCrashProperties.store(trace, "");
            }
            trace.close();
            return fileName;
        } catch (Exception e) {
            Log.e(LOG_TAG, "An error occured while writing the report file...", e);
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public String[] getCrashReportFilesList() {
        File dir = this.mContext.getFilesDir();
        if (dir != null) {
            Log.d(LOG_TAG, "Looking for error files in " + dir.getAbsolutePath());
            return dir.list(new FilenameFilter() {
                public boolean accept(File dir, String name) {
                    return name.endsWith(".stacktrace");
                }
            });
        }
        Log.w(LOG_TAG, "Application files directory does not exist! The application may not be installed correctly. Please try reinstalling.");
        return new String[0];
    }

    /* access modifiers changed from: package-private */
    public void checkAndSendReports(Context context, String userCommentReportFileName, boolean sendOnlySilentReports) {
        String str;
        try {
            String[] reportFilesList = getCrashReportFilesList();
            TreeSet<String> sortedFiles = new TreeSet<>();
            sortedFiles.addAll(Arrays.asList(reportFilesList));
            if (reportFilesList != null && reportFilesList.length > 0) {
                Properties previousCrashReport = new Properties();
                int curIndex = 0;
                Iterator it = sortedFiles.iterator();
                while (it.hasNext()) {
                    String curFileName = (String) it.next();
                    if (!sendOnlySilentReports || (sendOnlySilentReports && isSilent(curFileName))) {
                        if (curIndex < 5) {
                            FileInputStream input = context.openFileInput(curFileName);
                            if (storeToXML()) {
                                previousCrashReport.loadFromXML(input);
                            } else {
                                previousCrashReport.load(input);
                            }
                            input.close();
                            if (0 == 0 && (curFileName.equals(userCommentReportFileName) || (curIndex == sortedFiles.size() - 1 && !"".equals(mUserComment)))) {
                                String custom = previousCrashReport.getProperty(CUSTOM_DATA_KEY);
                                if (custom != null) {
                                    String custom2 = String.valueOf(custom) + "\n";
                                }
                                previousCrashReport.put(USER_COMMENT_KEY, mUserComment);
                            }
                            sendCrashReport(context, previousCrashReport);
                            new File(context.getFilesDir(), curFileName).delete();
                        }
                        curIndex++;
                    }
                }
            }
            mUserComment = "";
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            str = "";
            mUserComment = str;
        }
    }

    private boolean storeToXML() {
        return getAPILevel() < 5;
    }

    /* access modifiers changed from: package-private */
    public void setReportingInteractionMode(ReportingInteractionMode reportingInteractionMode) {
        this.mReportingInteractionMode = reportingInteractionMode;
    }

    public void checkReportsOnApplicationStart() {
        String[] filesList = getCrashReportFilesList();
        if (filesList != null && filesList.length > 0) {
            boolean onlySilentReports = containsOnlySilentReports(filesList);
            if (this.mReportingInteractionMode == ReportingInteractionMode.SILENT || this.mReportingInteractionMode == ReportingInteractionMode.TOAST || (this.mReportingInteractionMode == ReportingInteractionMode.NOTIFICATION && onlySilentReports)) {
                if (this.mReportingInteractionMode == ReportingInteractionMode.TOAST && !onlySilentReports) {
                    Toast.makeText(this.mContext, this.mCrashResources.getInt("RES_TOAST_TEXT"), 1).show();
                }
                new ReportsSenderWorker().start();
            } else if (this.mReportingInteractionMode == ReportingInteractionMode.NOTIFICATION) {
                getInstance().notifySendReport(getLatestNonSilentReport(filesList));
            }
        }
    }

    private String getLatestNonSilentReport(String[] filesList) {
        if (filesList == null || filesList.length <= 0) {
            return null;
        }
        for (int i = filesList.length - 1; i >= 0; i--) {
            if (!isSilent(filesList[i])) {
                return filesList[i];
            }
        }
        return filesList[filesList.length - 1];
    }

    public void deletePendingReports() {
        deletePendingReports(true, true);
    }

    public void deletePendingSilentReports() {
        deletePendingReports(true, false);
    }

    public void deletePendingNonSilentReports() {
        deletePendingReports(false, true);
    }

    private void deletePendingReports(boolean deleteSilentReports, boolean deleteNonSilentReports) {
        String[] filesList = getCrashReportFilesList();
        if (filesList != null) {
            for (String fileName : filesList) {
                if ((isSilent(fileName) && deleteSilentReports) || (!isSilent(fileName) && deleteNonSilentReports)) {
                    new File(this.mContext.getFilesDir(), fileName).delete();
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void setCrashResources(Bundle crashResources) {
        this.mCrashResources = crashResources;
    }

    public void disable() {
        if (this.mContext != null) {
            Log.d(ACRA.LOG_TAG, "ACRA is disabled for " + this.mContext.getPackageName());
        } else {
            Log.d(ACRA.LOG_TAG, "ACRA is disabled.");
        }
        if (this.mDfltExceptionHandler != null) {
            Thread.setDefaultUncaughtExceptionHandler(this.mDfltExceptionHandler);
        }
    }

    private boolean containsOnlySilentReports(String[] reportFileNames) {
        for (String reportFileName : reportFileNames) {
            if (!isSilent(reportFileName)) {
                return false;
            }
        }
        return true;
    }

    private boolean isSilent(String reportFileName) {
        return reportFileName.contains(SILENT_SUFFIX);
    }

    public void setUserComment(String userComment) {
        mUserComment = userComment;
    }

    private static int getAPILevel() {
        try {
            return Build.VERSION.class.getField("SDK_INT").getInt(null);
        } catch (SecurityException e) {
            return Integer.parseInt(Build.VERSION.SDK);
        } catch (NoSuchFieldException e2) {
            return Integer.parseInt(Build.VERSION.SDK);
        } catch (IllegalArgumentException e3) {
            return Integer.parseInt(Build.VERSION.SDK);
        } catch (IllegalAccessException e4) {
            return Integer.parseInt(Build.VERSION.SDK);
        }
    }
}
