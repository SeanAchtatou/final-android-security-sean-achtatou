package bostone.android.hireadroid.engine;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;
import bostone.android.hireadroid.HireadroidException;
import bostone.android.hireadroid.R;
import bostone.android.hireadroid.model.Country;
import bostone.android.hireadroid.oauth.Connector;
import bostone.android.hireadroid.oauth.JavaOAuthConnectorImpl;
import bostone.android.hireadroid.sax.LinkedInHandler;
import bostone.android.hireadroid.utils.Utils;
import com.flurry.android.CallbackEvent;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.parsers.ParserConfigurationException;
import net.oauth.OAuth;
import net.oauth.http.HttpResponseMessage;
import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;
import org.xml.sax.SAXException;

public class LinkedInEngine extends AbstractEngine {
    static final /* synthetic */ boolean $assertionsDisabled = (!LinkedInEngine.class.desiredAssertionStatus());
    public static final String AUTH_TOKEN = "auth_token";
    public static final String AUTH_TOKEN_SECRET = "auth_token_secret";
    static final Pattern BUMP_PTR = Pattern.compile("^(.+[^_]start=)(\\d+)(.*)$");
    private static final String CO = "&country-code";
    public static String[] ISO = {"US", Country.AU, Country.AT, Country.BE, Country.BR, Country.CA, Country.CN, Country.FR, Country.DE, Country.IN, Country.IE, Country.IT, Country.JP, Country.MX, Country.NL, Country.ES, Country.CH, Country.UK};
    static final Pattern ISO_PAT = Pattern.compile("^.+?\\&country-code=(\\w\\w).+$");
    public static final int LOGO = 2130837561;
    private static final String SORT_BY_DATE = "&sort=DD";
    private static final String SORT_BY_REL = "&sort=R";
    private static final String TAG = "LinkedInEngine";
    public static final String TYPE = "LinkedIn";
    private String aSecret;
    private String aToken;
    protected Connector connector = new JavaOAuthConnectorImpl();
    /* access modifiers changed from: private */
    public SharedPreferences prefs;
    public Request request;

    public LinkedInEngine() {
        this.xmlHandler = new LinkedInHandler();
        this.name = TYPE;
        this.logo = R.drawable.linkedin_logo;
    }

    public static String getCountryIsoFromUrl(String url) {
        if (!$assertionsDisabled && url == null) {
            throw new AssertionError();
        } else if (!url.contains(CO)) {
            return "US";
        } else {
            int start = url.indexOf(CO) + CO.length();
            return url.substring(start, start + 2).toUpperCase();
        }
    }

    public static Map<String, String> parseUrl(String url) {
        HashMap<String, String> map = new HashMap<>();
        for (String part : url.split("&")) {
            String[] pair = part.split("=");
            String val = pair.length == 2 ? pair[1] : null;
            if ("keywords".equals(pair[0])) {
                map.put(EngineConstants.QUERY, val);
            } else if ("postal-code".equals(pair[0])) {
                map.put(EngineConstants.LOCATION, val);
            } else if ("country-code".equals(pair[0])) {
                if (Country.UK.equalsIgnoreCase(val)) {
                    val = Country.GB;
                }
                map.put(EngineConstants.COUNTRY, val);
            }
        }
        return map;
    }

    public static boolean supportsISO(String country) {
        for (String iso : ISO) {
            if (iso.equalsIgnoreCase(country)) {
                return true;
            }
        }
        return $assertionsDisabled;
    }

    public String buildSearchUrl(Map<String, String> params) {
        this.request = new Request(this.activity, params);
        return this.request.toString();
    }

    public String bumpPageNumber(String url) {
        Matcher m = BUMP_PTR.matcher(url);
        if (m.find()) {
            return m.replaceAll("$1" + (Integer.parseInt(m.group(2)) + 1) + "$3");
        }
        throw new HireadroidException("Failed to extract more items");
    }

    /* access modifiers changed from: protected */
    public boolean supportsCountry(String country) {
        if (!supportsISO(country)) {
            return $assertionsDisabled;
        }
        this.enabled = true;
        return true;
    }

    /* access modifiers changed from: protected */
    public String updateSearchUrl(boolean byRelevance) {
        if (byRelevance && this.searchUrl.contains(SORT_BY_DATE)) {
            this.searchUrl = this.searchUrl.replace(SORT_BY_DATE, SORT_BY_REL);
        } else if (!byRelevance && this.searchUrl.contains(SORT_BY_REL)) {
            this.searchUrl = this.searchUrl.replace(SORT_BY_REL, SORT_BY_DATE);
        }
        return this.searchUrl;
    }

    public void requestUpdate(boolean cached, boolean reset, boolean byRelevance) {
        initPrefs();
        if (this.aToken == null || this.aSecret == null) {
            new AlertDialog.Builder(this.activity).setTitle("Attention").setMessage((int) R.string.linkedin_login_msg).setPositiveButton("Login with LinkedIn", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    LinkedInEngine.this.onLoginRequest();
                }
            }).setNegativeButton("Cancel", (DialogInterface.OnClickListener) null).show();
            return;
        }
        this.connector.init(this.aToken, this.aSecret);
        super.requestUpdate(cached, reset, byRelevance);
    }

    private void initPrefs() {
        if (this.prefs == null) {
            this.prefs = PreferenceManager.getDefaultSharedPreferences(this.activity);
        }
        if (this.aToken == null || this.aSecret == null) {
            this.aToken = this.prefs.getString(AUTH_TOKEN, null);
            this.aSecret = this.prefs.getString(AUTH_TOKEN_SECRET, null);
        }
    }

    /* access modifiers changed from: private */
    public void onLoginRequest() {
        new AsyncTask<Void, Void, String>() {
            Throwable error;

            /* access modifiers changed from: protected */
            public String doInBackground(Void... params) {
                try {
                    return LinkedInEngine.this.connector.authenticate();
                } catch (Exception e) {
                    this.error = e;
                    return null;
                }
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(String url) {
                String msg;
                if (this.error != null) {
                    if (OAuth.Problems.TIMESTAMP_REFUSED.equals(this.error.getMessage())) {
                        msg = "Timestamp error";
                    } else {
                        msg = "Failed to obtain authentication URL: " + this.error.getMessage();
                    }
                    if (!LinkedInEngine.this.activity.isFinishing()) {
                        new AlertDialog.Builder(LinkedInEngine.this.activity).setTitle("Login error").setMessage(msg).setPositiveButton("OK", (DialogInterface.OnClickListener) null).show();
                        return;
                    }
                    return;
                }
                Uri uri = Uri.parse(url);
                String secret = uri.getQueryParameter(OAuth.OAUTH_TOKEN_SECRET);
                if (secret != null) {
                    LinkedInEngine.this.prefs.edit().putString(OAuth.OAUTH_TOKEN_SECRET, secret).commit();
                }
                try {
                    Intent intent = new Intent("android.intent.action.VIEW", uri);
                    intent.setFlags(67108864);
                    LinkedInEngine.this.activity.startActivity(intent);
                    LinkedInEngine.this.activity.finish();
                } catch (ActivityNotFoundException e) {
                    ActivityNotFoundException e2 = e;
                    Log.e(LinkedInEngine.TAG, "Can't load login page", e2);
                    Toast.makeText(LinkedInEngine.this.activity, "Can't load LinkedIn authentication page, " + e2.getMessage(), 1).show();
                }
            }
        }.execute(new Void[0]);
    }

    public void onLoginApproved(Uri data) {
        if (this.prefs == null) {
            this.prefs = PreferenceManager.getDefaultSharedPreferences(this.activity);
        }
        Toast.makeText(this.activity, "Welcome back. Please wait until the login is finalized", 1).show();
        String code = data.getQueryParameter(OAuth.OAUTH_VERIFIER);
        String token = data.getQueryParameter(OAuth.OAUTH_TOKEN);
        String secret = this.prefs.getString(OAuth.OAUTH_TOKEN_SECRET, null);
        List<OAuth.Parameter> params = OAuth.newList(OAuth.OAUTH_VERIFIER, code);
        params.add(new OAuth.Parameter(OAuth.OAUTH_TOKEN, token));
        params.add(new OAuth.Parameter(OAuth.OAUTH_TOKEN_SECRET, secret));
        authFinish(params);
    }

    /* access modifiers changed from: protected */
    public void authFinish(final List<OAuth.Parameter> params) {
        new AsyncTask<Void, Void, String[]>() {
            Throwable t;

            /* access modifiers changed from: protected */
            public String[] doInBackground(Void... p) {
                try {
                    return LinkedInEngine.this.connector.authorize(params);
                } catch (Exception e) {
                    this.t = e;
                    return null;
                }
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(String[] tokens) {
                if (!LinkedInEngine.this.activity.isFinishing() && LinkedInEngine.this.activity.loginProgress != null && LinkedInEngine.this.activity.loginProgress.isShowing()) {
                    LinkedInEngine.this.activity.loginProgress.cancel();
                }
                if (this.t == null && tokens != null && tokens.length == 2) {
                    LinkedInEngine.this.onAuthComplete(tokens);
                } else if (!LinkedInEngine.this.activity.isFinishing() && !LinkedInEngine.this.activity.isFinishing()) {
                    new AlertDialog.Builder(LinkedInEngine.this.activity).setTitle("Authentication failed").setMessage(Utils.unwrap(this.t)).setNegativeButton("Cancel", (DialogInterface.OnClickListener) null).show();
                }
            }
        }.execute(new Void[0]);
    }

    /* access modifiers changed from: private */
    public void onAuthComplete(String[] tokens) {
        this.prefs.edit().putString(AUTH_TOKEN, tokens[0]).putString(AUTH_TOKEN_SECRET, tokens[1]).commit();
        completeLogin();
    }

    private void completeLogin() {
        initActiveTab(this.activity.gestureListener, $assertionsDisabled);
    }

    /* Debug info: failed to restart local var, previous not found, register: 6 */
    /* access modifiers changed from: protected */
    public void requestListUpdate(String url, boolean cached) throws ClientProtocolException, IOException, ParserConfigurationException, SAXException {
        initPrefs();
        this.connector.init(this.aToken, this.aSecret);
        Connector.SiteResponse response = this.connector.get(url, this.connector.getJsonHeaders(true));
        switch (response.getCode()) {
            case HttpResponseMessage.STATUS_OK /*200*/:
            case CallbackEvent.ADS_LOADED_FROM_CACHE /*201*/:
                try {
                    ((LinkedInHandler) this.xmlHandler).parse(response.getBody());
                    return;
                } catch (JSONException e) {
                    Log.e(TAG, "Failed to parse response body", e);
                    return;
                }
            default:
                Log.e(TAG, response.getBody());
                return;
        }
    }

    public static class Request {
        private static final String FILTER = ":(jobs:(id,posting-date,description-snippet,job-poster,location-description,company,site-job-url,position:(title)))";
        public static final String ROOT_URL = "http://api.linkedin.com/v1/job-search:(jobs:(id,posting-date,description-snippet,job-poster,location-description,company,site-job-url,position:(title)))?start=0&count=20&sort=DD";
        public String country;
        public String distance;
        public String keyword;
        public String location;

        Request(Context context, Map<String, String> params) {
            String str;
            this.location = params.get(EngineConstants.LOCATION);
            this.keyword = params.get(EngineConstants.QUERY);
            String iso = params.get(EngineConstants.COUNTRY);
            this.distance = params.get(EngineConstants.MILES);
            if (TextUtils.isEmpty(iso)) {
                str = "US";
            } else {
                str = iso;
            }
            this.country = str;
        }

        public String toString() {
            String encode;
            StringBuilder sb = new StringBuilder(ROOT_URL);
            try {
                StringBuilder append = sb.append("&keywords=");
                if (this.keyword == null) {
                    encode = "";
                } else {
                    encode = URLEncoder.encode(this.keyword, "utf-8");
                }
                append.append(encode);
                if (!TextUtils.isEmpty(this.country)) {
                    if (this.country != null && this.country.equalsIgnoreCase(Country.UK)) {
                        this.country = Country.GB;
                    }
                    sb.append("&country-code=").append(this.country.toLowerCase());
                }
                if (!TextUtils.isEmpty(this.location)) {
                    sb.append("&postal-code=").append(URLEncoder.encode(this.location, "utf-8"));
                    sb.append("&distance=").append(this.distance);
                }
                return sb.toString();
            } catch (UnsupportedEncodingException e) {
                UnsupportedEncodingException e2 = e;
                Log.e(LinkedInEngine.TAG, "Failed to encode search criteria", e2);
                throw new HireadroidException(e2);
            }
        }
    }
}
