package bostone.android.hireadroid.engine;

import android.text.TextUtils;
import android.util.Log;
import bostone.android.hireadroid.HireadroidException;
import bostone.android.hireadroid.R;
import bostone.android.hireadroid.model.Country;
import bostone.android.hireadroid.sax.CareerBuilderHandler;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CareerBuilderEngine extends AbstractEngine {
    static final /* synthetic */ boolean $assertionsDisabled = (!CareerBuilderEngine.class.desiredAssertionStatus());
    static final Pattern BUMP_PTR = Pattern.compile("^(.+[^_]PageNumber=)(\\d+)(.*)$");
    private static final String CO = "&CountryCode=";
    public static String[] ISO = {"US", Country.AU, Country.AT, Country.BE, Country.BR, Country.CA, Country.CN, Country.FR, Country.DE, Country.IN, Country.IE, Country.IT, Country.JP, Country.MX, Country.NL, Country.ES, Country.CH, Country.UK};
    static final Pattern ISO_PAT = Pattern.compile("^.+?\\&CountryCode=(\\w\\w).+$");
    public static final int LOGO = 2130837512;
    static final int[] RADIUS = {5, 10, 20, 30, 50, 100, 150};
    private static final String SORT_BY_DATE = "&OrderBy=Date";
    private static final String SORT_BY_REL = "&OrderBy=Relevance";
    public static final String TAG = "CareerBuilderEngine";
    public static final String TYPE = "CareerBuilder";
    private static final long serialVersionUID = 8390251996555675027L;
    public Request request;

    public CareerBuilderEngine() {
        this.xmlHandler = new CareerBuilderHandler();
        this.name = TYPE;
        this.logo = R.drawable.careerbuilder_logo_small;
    }

    public String buildSearchUrl(Map<String, String> params) {
        this.request = new Request(params);
        return this.request.toString();
    }

    public static Map<String, String> parseUrl(String url) {
        HashMap<String, String> map = new HashMap<>();
        for (String part : url.split("&")) {
            String[] pair = part.split("=");
            String val = pair.length == 2 ? pair[1] : null;
            if ("Keywords".equals(pair[0])) {
                map.put(EngineConstants.QUERY, val);
            } else if ("Location".equals(pair[0])) {
                map.put(EngineConstants.LOCATION, val);
            } else if ("Radius".equals(pair[0])) {
                map.put(EngineConstants.MILES, val);
            } else if ("CountryCode".equals(pair[0])) {
                map.put(EngineConstants.COUNTRY, val);
            }
        }
        return map;
    }

    public String bumpPageNumber(String url) {
        Matcher m = BUMP_PTR.matcher(url);
        if (m.find()) {
            return m.replaceAll("$1" + (Integer.parseInt(m.group(2)) + 1) + "$3");
        }
        throw new HireadroidException("Failed to extract more items");
    }

    public static String getCountryIsoFromUrl(String url) {
        if (!$assertionsDisabled && url == null) {
            throw new AssertionError();
        } else if (!url.contains(CO)) {
            return "US";
        } else {
            int start = url.indexOf(CO) + CO.length();
            return url.substring(start, start + 2).toUpperCase();
        }
    }

    public static class Request {
        public static final String ROOT_URL = "http://api.careerbuilder.com/v1/jobsearch?DeveloperKey=WDhf29W6P4NR5KPP6DVF&PageNumber=1&PerPage=20&ExcludeNational=true&OrderBy=Date";
        public static final String SEARCH_KEY = "WDhf29W6P4NR5KPP6DVF";
        public String country;
        public String distance;
        public String keyword;
        public String location;

        Request(Map<String, String> params) {
            String str;
            this.location = params.get(EngineConstants.LOCATION);
            this.keyword = params.get(EngineConstants.QUERY);
            this.distance = params.get(EngineConstants.MILES);
            String iso = params.get(EngineConstants.COUNTRY);
            if (TextUtils.isEmpty(iso)) {
                str = "US";
            } else {
                str = iso;
            }
            this.country = str;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder(ROOT_URL);
            try {
                sb.append("&Keywords=").append(this.keyword == null ? "" : URLEncoder.encode(this.keyword, "utf-8"));
                if (!TextUtils.isEmpty(this.location)) {
                    sb.append("&Location=").append(URLEncoder.encode(this.location, "utf-8"));
                    sb.append("&Radius=").append(roundUp(this.distance));
                }
                sb.append(CareerBuilderEngine.CO).append(this.country == null ? "US" : this.country);
                return sb.toString();
            } catch (UnsupportedEncodingException e) {
                UnsupportedEncodingException e2 = e;
                Log.e(CareerBuilderEngine.TAG, "Failed to encode search criteria", e2);
                throw new HireadroidException(e2);
            }
        }

        private int roundUp(String distance2) {
            try {
                int d = Integer.parseInt(distance2);
                if (d >= 100) {
                    return 100;
                }
                for (int i = 0; i < CareerBuilderEngine.RADIUS.length; i++) {
                    if (d == CareerBuilderEngine.RADIUS[i]) {
                        return d;
                    }
                    if (d > CareerBuilderEngine.RADIUS[i] && d < CareerBuilderEngine.RADIUS[i + 1]) {
                        return CareerBuilderEngine.RADIUS[i + 1];
                    }
                }
                return 20;
            } catch (NumberFormatException e) {
                Log.e(CareerBuilderEngine.TAG, "Exception rounding up distance " + distance2, e);
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean supportsCountry(String country) {
        if (!supportsISO(country)) {
            return $assertionsDisabled;
        }
        this.enabled = true;
        return true;
    }

    public static boolean supportsISO(String country) {
        for (String iso : ISO) {
            if (iso.equalsIgnoreCase(country)) {
                return true;
            }
        }
        return $assertionsDisabled;
    }

    /* access modifiers changed from: protected */
    public String updateSearchUrl(boolean byRelevance) {
        if (byRelevance && this.searchUrl.contains(SORT_BY_DATE)) {
            this.searchUrl = this.searchUrl.replace(SORT_BY_DATE, SORT_BY_REL);
        } else if (!byRelevance && this.searchUrl.contains(SORT_BY_REL)) {
            this.searchUrl = this.searchUrl.replace(SORT_BY_REL, SORT_BY_DATE);
        }
        return this.searchUrl;
    }
}
