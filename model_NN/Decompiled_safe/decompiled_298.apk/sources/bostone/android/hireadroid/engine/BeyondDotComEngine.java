package bostone.android.hireadroid.engine;

import android.text.TextUtils;
import android.util.Log;
import bostone.android.hireadroid.HireadroidException;
import bostone.android.hireadroid.R;
import bostone.android.hireadroid.model.Country;
import bostone.android.hireadroid.sax.BeyondDotComHandler;
import bostone.android.hireadroid.utils.Utils;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BeyondDotComEngine extends AbstractEngine {
    static final /* synthetic */ boolean $assertionsDisabled = (!BeyondDotComEngine.class.desiredAssertionStatus());
    static final Pattern BUMP_PTR = Pattern.compile("^(.+[^_]PN=)(\\d+)(.*)$");
    private static final String CO = "&CT=";
    public static final String EMAIL = "beyond_email";
    public static String[] ISO = {"US", Country.AU, Country.AT, Country.BE, Country.BR, Country.CA, Country.CN, Country.FR, Country.DE, Country.IN, Country.IE, Country.IT, Country.JP, Country.MX, Country.NL, Country.ES, Country.CH, Country.UK};
    static final Pattern ISO_PAT = Pattern.compile("^.+?\\&CT=(\\w\\w).+$");
    public static final int LOGO = 2130837506;
    public static final String PASSWORD = "beyond_password";
    private static final String SORT_BY_DATE = "&ORD=M";
    private static final String SORT_BY_REL = "&ORD=D";
    private static final String TAG = "BeyondDotComEngine";
    public static final String TYPE = "Beyond.com";
    private static final long serialVersionUID = 1005919756304076643L;
    public Request request;

    public BeyondDotComEngine() {
        this.xmlHandler = new BeyondDotComHandler();
        this.name = TYPE;
        this.logo = R.drawable.beyond_logo_small;
    }

    public static String getCountryIsoFromUrl(String url) {
        if (!$assertionsDisabled && url == null) {
            throw new AssertionError();
        } else if (!url.contains(CO)) {
            return "US";
        } else {
            int start = url.indexOf(CO) + CO.length();
            return url.substring(start, start + 2).toUpperCase();
        }
    }

    public static Map<String, String> parseUrl(String url) {
        HashMap<String, String> map = new HashMap<>();
        for (String part : url.split("&")) {
            String[] pair = part.split("=");
            String key = pair[0];
            String val = pair.length == 2 ? pair[1] : null;
            if ("kw".equalsIgnoreCase(key)) {
                map.put(EngineConstants.QUERY, val);
            } else if ("zc".equalsIgnoreCase(key) || "fwhere".equals(key)) {
                map.put(EngineConstants.LOCATION, val);
            } else if ("dst".equalsIgnoreCase(key)) {
                map.put(EngineConstants.MILES, val);
            } else if ("ct".equalsIgnoreCase(key)) {
                if (Country.UK.equalsIgnoreCase(val)) {
                    val = Country.GB;
                }
                map.put(EngineConstants.COUNTRY, val);
            }
        }
        return map;
    }

    public static boolean supportsISO(String country) {
        for (String iso : ISO) {
            if (iso.equalsIgnoreCase(country)) {
                return true;
            }
        }
        return $assertionsDisabled;
    }

    public String buildSearchUrl(Map<String, String> params) {
        this.request = new Request(params);
        return this.request.toString();
    }

    public String bumpPageNumber(String url) {
        Matcher m = BUMP_PTR.matcher(url);
        if (m.find()) {
            return m.replaceAll("$1" + (Integer.parseInt(m.group(2)) + 1) + "$3");
        }
        throw new HireadroidException("Failed to extract more items");
    }

    /* access modifiers changed from: protected */
    public boolean supportsCountry(String country) {
        if (!supportsISO(country)) {
            return $assertionsDisabled;
        }
        this.enabled = true;
        return true;
    }

    /* access modifiers changed from: protected */
    public String updateSearchUrl(boolean byRelevance) {
        if (byRelevance && this.searchUrl.contains(SORT_BY_DATE)) {
            this.searchUrl = this.searchUrl.replace(SORT_BY_DATE, SORT_BY_REL);
        } else if (!byRelevance && this.searchUrl.contains(SORT_BY_REL)) {
            this.searchUrl = this.searchUrl.replace(SORT_BY_REL, SORT_BY_DATE);
        }
        return this.searchUrl;
    }

    public static class Request {
        public static final String API_KEY = "E69981C0-33F7-40D6-BC6B-4100B452E05B";
        public static final String LOGIN_URL = "http://www.beyond.com/common/services/jobseeker/jobseeker.login/?apikey=E69981C0-33F7-40D6-BC6B-4100B452E05B";
        public static final String REGISTER_URL = "http://www.beyond.com/common/services/jobseeker/jobseeker.signup/?apikey=E69981C0-33F7-40D6-BC6B-4100B452E05B";
        public static final String ROOT_URL = "http://www.beyond.com/common/services/job/search/default.asp?aff=E69981C0-33F7-40D6-BC6B-4100B452E05B&PN=1&MX=20&NW=E&ORD=M";
        private static final Pattern zipPtr = Pattern.compile("\\d{5}([\\-]\\d{4})?");
        public String country;
        private final String distance;
        public String keyword;
        public String location;

        Request(Map<String, String> params) {
            String str;
            this.location = params.get(EngineConstants.LOCATION);
            this.keyword = params.get(EngineConstants.QUERY);
            String iso = params.get(EngineConstants.COUNTRY);
            if (TextUtils.isEmpty(iso)) {
                str = "US";
            } else {
                str = iso;
            }
            this.country = str;
            this.distance = params.get(EngineConstants.MILES);
        }

        public String toString() {
            String cntr;
            StringBuilder sb = new StringBuilder(ROOT_URL);
            try {
                sb.append("&kw=").append(this.keyword == null ? "" : URLEncoder.encode(this.keyword, "utf-8"));
                if (!TextUtils.isEmpty(this.location)) {
                    if (zipPtr.matcher(this.location).find()) {
                        sb.append("&zc=").append(URLEncoder.encode(this.location, "utf-8"));
                    } else {
                        sb.append("&fwhere=").append(URLEncoder.encode(this.location, "utf-8"));
                    }
                    if (Utils.isNotEmpty(this.distance)) {
                        sb.append("&dst=").append(this.distance);
                    }
                }
                if (this.country == null) {
                    cntr = "US";
                } else {
                    cntr = this.country.equalsIgnoreCase(Country.UK) ? Country.GB : this.country;
                }
                sb.append(BeyondDotComEngine.CO).append(cntr);
                return sb.toString();
            } catch (UnsupportedEncodingException e) {
                UnsupportedEncodingException e2 = e;
                Log.e(BeyondDotComEngine.TAG, "Failed to encode search criteria", e2);
                throw new HireadroidException(e2);
            }
        }
    }
}
