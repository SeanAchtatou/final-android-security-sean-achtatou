package bostone.android.hireadroid.engine;

import android.util.Log;
import bostone.android.hireadroid.HireadroidException;
import bostone.android.hireadroid.R;
import bostone.android.hireadroid.model.Country;
import bostone.android.hireadroid.sax.IndeedComHandler;
import bostone.android.hireadroid.utils.Utils;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IndeedDotComEngine extends AbstractEngine {
    static final /* synthetic */ boolean $assertionsDisabled = (!IndeedDotComEngine.class.desiredAssertionStatus());
    static final Pattern BUMP_PTR = Pattern.compile("^(.+start=)(\\d+)(.*)$");
    private static final String CO = "&co=";
    public static String[] ISO = {"US", Country.AU, Country.AT, Country.BE, Country.BR, Country.CA, Country.FR, Country.DE, Country.IN, Country.IE, Country.IT, Country.MX, Country.NL, Country.ES, Country.CH, Country.UK};
    static final Pattern ISO_PAT = Pattern.compile("^.+?\\&co=(\\w\\w).+$");
    public static final int LOGO = 2130837558;
    static final String[] SEARCH_KEYS = {"date", "relevance"};
    static final String[] SEARCH_VALUES = {"Date", "Relevance"};
    private static final String SORT_BY_DATE = "&sort=date";
    private static final String SORT_BY_REL = "&sort=relevance";
    public static final String TAG = "IndeedDotComEngine";
    public static final String TYPE = "Indeed.com";
    private static final long serialVersionUID = 7721912953252597133L;
    public Request request;

    public IndeedDotComEngine() {
        this.xmlHandler = new IndeedComHandler();
        this.name = TYPE;
        this.logo = R.drawable.indeedcom_onblack_small;
    }

    public String buildSearchUrl(Map<String, String> params) {
        this.request = new Request(params);
        return this.request.toString();
    }

    public static Map<String, String> parseUrl(String url) {
        HashMap<String, String> map = new HashMap<>();
        for (String part : url.split("&")) {
            String[] pair = part.split("=");
            String val = pair.length == 2 ? pair[1] : null;
            if (EngineConstants.QUERY.equals(pair[0])) {
                map.put(EngineConstants.QUERY, val);
            } else if (EngineConstants.LOCATION.equals(pair[0])) {
                map.put(EngineConstants.LOCATION, val);
            } else if ("radius".equals(pair[0])) {
                map.put(EngineConstants.MILES, val);
            } else if ("co".equals(pair[0])) {
                map.put(EngineConstants.COUNTRY, val);
            }
        }
        return map;
    }

    public static class Request {
        public static final String[] JOB_TYPES = {"fulltime", "parttime", "contract", "internship", "temporary"};
        public static final String PUBLISHER = "4503489749735822";
        public static final String ROOT_URL = "http://api.indeed.com/ads/apisearch?start=0&limit=20&publisher=4503489749735822";
        public String chnl = "hireadroid";
        public String co = "us";
        public int filter = 1;
        public int fromage = 30;
        public int highlight = 0;
        public String jt;
        public String l;
        public int latlong = 0;
        public int limit = 10;
        public String q;
        public int radius;
        public String sort;
        public String st;
        public int start = 0;
        public String useragent;
        public String userip;

        Request(Map<String, String> params) {
            this.l = params.get(EngineConstants.LOCATION);
            this.q = params.get(EngineConstants.QUERY);
            this.sort = params.get(EngineConstants.ORDER);
            this.radius = Integer.valueOf(params.get(EngineConstants.MILES)).intValue();
            this.co = params.get(EngineConstants.COUNTRY).toLowerCase();
            if (Country.GB.equalsIgnoreCase(this.co)) {
                this.co = Country.UK.toLowerCase();
            }
            this.userip = Utils.getLocalIpAddress();
        }

        public String toString() {
            StringBuilder sb = new StringBuilder(ROOT_URL);
            try {
                sb.append("&q=").append(this.q == null ? "" : URLEncoder.encode(this.q, "utf-8"));
                sb.append("&l=").append(this.l == null ? "" : URLEncoder.encode(this.l, "utf-8"));
                sb.append("&sort=").append(this.sort == null ? "date" : this.sort);
                sb.append("&radius=").append(this.l == null ? "" : Integer.valueOf(this.radius));
                sb.append(IndeedDotComEngine.CO).append(this.co == null ? "us" : this.co);
                sb.append("&userip=").append(this.userip == null ? "1.2.3.4" : this.userip);
                sb.append("&st=&jobTitle=&fromage=&filter=&latlong=1&chnl=hireadroid&useragent=Mozilla%2F5.0%20(Linux%3B%20U%3B%20Android%200.5%3B%20en-us)%20AppleWebKit%2F522%2B%20(KHTML%2C%20like%20Gecko)%20Safari%2F419.3%20");
                return sb.toString();
            } catch (UnsupportedEncodingException e) {
                UnsupportedEncodingException e2 = e;
                Log.e(IndeedDotComEngine.TAG, "Failed to encode search criteria", e2);
                throw new HireadroidException(e2);
            }
        }
    }

    public String bumpPageNumber(String url) {
        Matcher m = BUMP_PTR.matcher(url);
        if (!m.find()) {
            return String.valueOf(url) + "&start=10";
        }
        return m.replaceAll("$1" + (Integer.parseInt(m.group(2)) + 20) + "$3");
    }

    public static String getCountryIsoFromUrl(String url) {
        if (!$assertionsDisabled && url == null) {
            throw new AssertionError();
        } else if (url.contains(CO)) {
            int start = url.indexOf(CO) + CO.length();
            return url.substring(start, start + 2).toUpperCase();
        } else if (url.contains(String.valueOf('=') + Country.GB.toLowerCase())) {
            return Country.UK;
        } else {
            return "US";
        }
    }

    /* access modifiers changed from: protected */
    public boolean supportsCountry(String country) {
        for (String iso : ISO) {
            if (iso.equalsIgnoreCase(country)) {
                this.enabled = true;
                return true;
            }
        }
        return $assertionsDisabled;
    }

    public static boolean supportsISO(String country) {
        for (String iso : ISO) {
            if (iso.equalsIgnoreCase(country)) {
                return true;
            }
        }
        return $assertionsDisabled;
    }

    /* access modifiers changed from: protected */
    public String updateSearchUrl(boolean byRelevance) {
        if (byRelevance && this.searchUrl.contains(SORT_BY_DATE)) {
            this.searchUrl = this.searchUrl.replace(SORT_BY_DATE, SORT_BY_REL);
        } else if (!byRelevance && this.searchUrl.contains(SORT_BY_REL)) {
            this.searchUrl = this.searchUrl.replace(SORT_BY_REL, SORT_BY_DATE);
        }
        return this.searchUrl;
    }
}
