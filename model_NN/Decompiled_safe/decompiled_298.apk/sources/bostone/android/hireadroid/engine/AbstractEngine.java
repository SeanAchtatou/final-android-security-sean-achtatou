package bostone.android.hireadroid.engine;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SpinnerAdapter;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;
import bostone.android.hireadroid.HireadroidException;
import bostone.android.hireadroid.R;
import bostone.android.hireadroid.activity.MonitoredActivity;
import bostone.android.hireadroid.activity.SearchResultsActivity;
import bostone.android.hireadroid.dao.SearchItemsDbHelper;
import bostone.android.hireadroid.engine.BeyondDotComEngine;
import bostone.android.hireadroid.engine.CareerBuilderEngine;
import bostone.android.hireadroid.engine.IndeedDotComEngine;
import bostone.android.hireadroid.engine.LinkUpEngine;
import bostone.android.hireadroid.engine.SimplyHiredEngine;
import bostone.android.hireadroid.model.Location;
import bostone.android.hireadroid.model.SearchHeader;
import bostone.android.hireadroid.model.SearchItem;
import bostone.android.hireadroid.net.RequestHandler;
import bostone.android.hireadroid.provider.SearchItemsProvider;
import bostone.android.hireadroid.sax.AbstractHandler;
import bostone.android.hireadroid.sax.ParserAction;
import bostone.android.hireadroid.ui.JobSearchResultsAdapter;
import bostone.android.hireadroid.ui.PaginatableListCallback;
import bostone.android.hireadroid.ui.PaginatableListView;
import bostone.android.hireadroid.utils.SavedSearchRepo;
import bostone.android.hireadroid.utils.Utils;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.http.client.ClientProtocolException;
import org.xml.sax.SAXException;

public abstract class AbstractEngine implements EngineConstants, PaginatableListCallback {
    public static final String SORT_BY_DATE = "Sort By Date";
    public static final String SORT_BY_RELEVANCE = "Sort By Relevance";
    private static final String TAG = "AbstractEngine";
    protected SearchResultsActivity activity;
    private JobSearchResultsAdapter adapter;
    public boolean done;
    public boolean enabled;
    protected ViewFlipper flipper;
    /* access modifiers changed from: private */
    public boolean isUpdating;
    /* access modifiers changed from: private */
    public PaginatableListView list;
    public int logo;
    public String name;
    private Map<String, String> params;
    /* access modifiers changed from: private */
    public String rawUrl;
    private TableRow row;
    protected SearchAdapter searchOrderAdapter;
    protected String searchUrl;
    protected View tab;
    public int tabId;
    public boolean tabSelected;
    public ImageButton trigger;
    protected AbstractHandler xmlHandler;

    public abstract String buildSearchUrl(Map<String, String> map);

    public abstract String bumpPageNumber(String str);

    /* access modifiers changed from: protected */
    public abstract boolean supportsCountry(String str);

    /* access modifiers changed from: protected */
    public abstract String updateSearchUrl(boolean z);

    public static AbstractEngine getInstance(String name2, SearchResultsActivity activity2, int tabId2, ViewFlipper flipper2, TableRow row2) {
        AbstractEngine eng;
        if (SimplyHiredEngine.TYPE.equals(name2)) {
            eng = new SimplyHiredEngine();
        } else if (IndeedDotComEngine.TYPE.equals(name2)) {
            eng = new IndeedDotComEngine();
        } else if ("LinkUp.com".equals(name2)) {
            eng = new LinkUpEngine();
        } else if (CareerBuilderEngine.TYPE.equals(name2)) {
            eng = new CareerBuilderEngine();
        } else if (BeyondDotComEngine.TYPE.equals(name2)) {
            eng = new BeyondDotComEngine();
        } else if (LinkedInEngine.TYPE.equals(name2)) {
            eng = new LinkedInEngine();
        } else {
            throw new UnsupportedOperationException("Unsupported engine type: " + name2);
        }
        eng.tabId = tabId2;
        eng.flipper = flipper2;
        eng.activity = activity2;
        eng.row = row2;
        eng.adapter = new JobSearchResultsAdapter(activity2, name2);
        return eng;
    }

    public Dialog buildSearchInfoDlg(final int id) {
        String str;
        String decode;
        if (this.adapter.header == null) {
            return new AlertDialog.Builder(this.activity).setTitle("Warning").setMessage("Failed to get search information").setNegativeButton("OK", (DialogInterface.OnClickListener) null).create();
        }
        final Dialog d = new Dialog(this.activity);
        d.setContentView((int) R.layout.results_info);
        SearchHeader h = this.adapter.header;
        ((TextView) d.findViewById(R.id.infoTotal)).setText(Integer.toString(h.numTotalResults));
        ((TextView) d.findViewById(R.id.infoViewable)).setText(Integer.toString(h.numViewableResults));
        Map<String, String> map = Utils.parseSearchParams(this.searchUrl);
        String loc = map == null ? null : map.get(EngineConstants.LOCATION);
        TextView textView = (TextView) d.findViewById(R.id.infoLocation);
        if (TextUtils.isEmpty(loc)) {
            str = "N/A";
        } else {
            str = loc;
        }
        textView.setText(str);
        String country = map == null ? "US" : map.get(EngineConstants.COUNTRY);
        if (TextUtils.isEmpty(country)) {
            country = "US";
        }
        ((TextView) d.findViewById(R.id.infoCountry)).setText(country.toUpperCase());
        if (!TextUtils.isEmpty(loc)) {
            ((TextView) d.findViewById(R.id.infoRadius)).setText(map.get(EngineConstants.MILES));
        }
        String keyword = map == null ? null : map.get(EngineConstants.QUERY);
        if (TextUtils.isEmpty(keyword)) {
            keyword = "N/A";
        }
        TextView textView2 = (TextView) d.findViewById(R.id.infoCriteria);
        if (TextUtils.isEmpty(keyword)) {
            decode = "";
        } else {
            decode = URLDecoder.decode(keyword);
        }
        textView2.setText(decode);
        d.setTitle(TextUtils.isEmpty(h.title) ? String.valueOf(keyword) + " jobs" : h.title);
        d.findViewById(R.id.infoDlgTbl).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                d.cancel();
                AbstractEngine.this.activity.removeDialog(id);
            }
        });
        ((ImageView) d.findViewById(R.id.engineLogoSmall)).setImageResource(this.logo);
        return d;
    }

    public Dialog buildSearchSaveDlg(final int id) {
        Location loc;
        if (this.adapter == null || this.adapter.header == null) {
            return new AlertDialog.Builder(this.activity).setMessage("Unable to obtain search criteria").setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                    AbstractEngine.this.activity.removeDialog(id);
                }
            }).create();
        }
        final EditText edit = new EditText(this.activity);
        edit.setId(id);
        String val = this.adapter.header.title;
        if (!(this.adapter.items == null || this.adapter.items.isEmpty() || this.adapter.items.get(0) == null || (loc = this.adapter.items.get(0).location) == null || loc.country == null)) {
            val = String.valueOf(val) + " - " + loc.country.toUpperCase();
        }
        edit.setText(val);
        return new AlertDialog.Builder(this.activity).setMessage((int) R.string.save_search_confirm).setPositiveButton("Save", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dlg, int which) {
                try {
                    SavedSearchRepo.put(edit.getText().toString(), AbstractEngine.this.rawUrl, AbstractEngine.this.activity);
                } catch (Exception e) {
                    Exception e2 = e;
                    Toast.makeText(AbstractEngine.this.activity, "Failed to save search\n" + Utils.formatError(e2), 1).show();
                    Log.e("SearchActivity", "onClickSearch", e2);
                }
                dlg.cancel();
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        }).setTitle("Save search").setView(edit).create();
    }

    public SpinnerAdapter getSearchOrderAdapter() {
        return this.searchOrderAdapter;
    }

    public String getSearchOrderByPosition(int position) {
        return this.searchOrderAdapter.keys[position];
    }

    public void initActiveTab(View.OnTouchListener gestureListener, boolean reload) {
        View view;
        if (!this.tabSelected || reload) {
            if (this.tab == null) {
                View s = this.flipper.findViewById(getStubByTabId(this.tabId, this.flipper));
                if (ViewStub.class.isInstance(s)) {
                    view = ((ViewStub) s).inflate();
                } else {
                    view = s;
                }
                this.tab = view;
                if (this.list == null) {
                    this.list = (PaginatableListView) this.tab.findViewById(R.id.tabResults);
                    this.list.setItemsCanFocus(true);
                    this.list.setOnTouchListener(gestureListener);
                    this.list.setAdapter((ListAdapter) this.adapter);
                }
            }
            this.tabSelected = true;
            this.activity.currentTab = this.tabId;
            this.activity.toggleArrowsBasedOnTabId(this.tabId);
            this.trigger.setBackgroundResource(R.drawable.tab_bgr_selected);
            this.flipper.setDisplayedChild(this.tabId);
            boolean byRelevance = Utils.getPrefs(this.activity).getBoolean(EngineConstants.ORDER + this.name, false);
            if (!(this.activity == null || this.activity.sortMenuItem == null)) {
                this.activity.sortMenuItem.setTitle(byRelevance ? SORT_BY_DATE : SORT_BY_RELEVANCE);
            }
            if (this.done) {
                updateWindowTitle();
                broadcastOnClickEvent();
                return;
            }
            this.list.setOnScrollCallback(this);
            if (!this.list.filledUp) {
                requestUpdate(true, false, byRelevance);
            }
            broadcastOnClickEvent();
        }
    }

    private void broadcastOnClickEvent() {
        Intent intent = new Intent(EngineConstants.CLICK_ACTION);
        intent.putExtra("ID", this.tabId);
        this.activity.sendBroadcast(intent);
    }

    public boolean init(String theUrl) {
        this.params = Utils.parseSearchParams(theUrl);
        String iso = this.params.get(EngineConstants.COUNTRY);
        if (iso == null || !supportsCountry(iso)) {
            return false;
        }
        this.rawUrl = theUrl;
        this.searchUrl = buildSearchUrl(Utils.parseSearchParams(theUrl));
        return true;
    }

    public void onLastItemDisplayed(ListView l) {
        Log.d(TAG, "Last list item detected");
        if (!this.done) {
            Log.d(TAG, "Not done with the previous update - ignoring");
            return;
        }
        boolean byRelevance = Utils.getPrefs(this.activity).getBoolean(EngineConstants.ORDER + this.name, false);
        if (!this.list.filledUp) {
            this.done = false;
            Log.d(TAG, "Done is false, requesting update");
            requestUpdate(false, false, byRelevance);
            return;
        }
        Log.d(TAG, "List is filled, skipping update");
    }

    public void onScrollChanged(int state, ListView list2) {
    }

    public boolean isDone() {
        return this.done;
    }

    /* access modifiers changed from: private */
    public void onSearchRequest(String url, boolean cached, boolean reset) {
        this.done = true;
        if (cached && !reset) {
            String xml = Utils.getStringVal(EngineConstants.SEARCH + url, this.activity);
            if (Utils.isNotEmpty(xml) && !xml.equals(EngineConstants.UPDATE_FAILED)) {
                try {
                    new ParserAction(xml, this.xmlHandler).parse();
                    processResults(url, reset);
                    return;
                } catch (Exception e) {
                    Log.e(TAG, "Failed process saved html for: " + url, e);
                    this.xmlHandler.reset();
                    return;
                }
            }
        }
        try {
            new SearchRequestTask(this, cached, reset, null).execute(url);
        } catch (Exception e2) {
            Log.e(TAG, "Failed to request update for: " + url, e2);
            this.done = true;
        }
    }

    /* access modifiers changed from: private */
    public void processResults(String url, boolean reset) {
        Log.d(TAG, "Got search results from: " + url);
        this.done = true;
        MonitoredActivity.event("Load search result", "Engine", this.name);
        if (reset) {
            this.adapter.reset();
        }
        if (this.xmlHandler.result.items.size() < 20) {
            this.list.filledUp = true;
            if (this.xmlHandler.result.items.isEmpty()) {
                this.list.footer.showInfoMessage("Search produced no results", false);
            } else {
                this.list.footer.done(this.activity.getResources().getString(R.string.end_of_list));
            }
        }
        if (!this.xmlHandler.result.items.isEmpty()) {
            onResultsEmpty();
        }
        if (!(this.adapter.header == null || this.adapter.header.messages == null || this.adapter.header.messages.length <= 0)) {
            String msg = "";
            for (String m : this.adapter.header.messages) {
                if (Utils.isEmpty(m)) {
                    break;
                }
                msg = (String.valueOf(msg) + m + "\n").replaceAll("_", " ");
            }
            if (Utils.isNotEmpty(msg)) {
                Toast.makeText(this.activity, msg, 1).show();
            }
        }
        updateWindowTitle();
        this.xmlHandler.result.header.url = url;
    }

    private void onResultsEmpty() {
        StringBuilder sb = null;
        for (SearchItem item : this.xmlHandler.result.items) {
            if (sb == null) {
                sb = new StringBuilder();
            } else {
                sb.append(',');
            }
            sb.append('\'').append(item.jobId).append('\'');
        }
        Cursor c = this.activity.managedQuery(SearchItemsProvider.CONTENT_URI, new String[]{"_id", SearchItemsDbHelper.ItemColumns.JOB_ID, SearchItemsDbHelper.ItemColumns.READ_TS}, "jobId IN(" + ((Object) sb) + ")", null, null);
        while (c.moveToNext()) {
            long id = c.getLong(0);
            String jId = c.getString(1);
            long ts = c.getLong(2);
            for (SearchItem item2 : this.xmlHandler.result.items) {
                if (item2.jobId.equals(jId)) {
                    item2.id = id;
                    item2.tsRead = ts;
                }
            }
        }
        this.adapter.addResult(this.xmlHandler.result);
    }

    private void updateWindowTitle() {
        this.activity.title.setText("HireADroid | " + this.name);
    }

    /* access modifiers changed from: protected */
    public void requestListUpdate(String url, boolean cached) throws ClientProtocolException, IOException, ParserConfigurationException, SAXException {
        this.xmlHandler.result.reset();
        String xml = getRequestHandler().service(url, this.xmlHandler);
        if (xml != null && cached) {
            updateLastUrl(url, xml);
        }
    }

    /* access modifiers changed from: protected */
    public RequestHandler getRequestHandler() {
        return new RequestHandler(this.activity);
    }

    public void requestUpdate(boolean cached, boolean reset, boolean byRelevance) {
        if (this.searchUrl != null && !this.isUpdating) {
            String url = updateSearchUrl(byRelevance);
            if (this.adapter == null || this.adapter.header == null || this.adapter.header.url == null || reset) {
                Log.i(TAG, "Initial request or reload for URL " + url);
            } else {
                url = bumpPageNumber(this.adapter.header.url);
                Log.d(TAG, "Bumped URL " + url);
            }
            onSearchRequest(url, cached, reset);
        }
    }

    private void updateLastUrl(String url, String xml) {
        Utils.setString(EngineConstants.SEARCH + url, xml, this.activity);
        String prevUrl = Utils.getStringVal(EngineConstants.CURR_SUMMARY_URL, this.activity);
        if (Utils.isNotEmpty(prevUrl)) {
            Utils.getPrefs(this.activity).edit().remove(prevUrl).commit();
        }
        Utils.getPrefs(this.activity).edit().putString(EngineConstants.CURR_SUMMARY_URL, EngineConstants.SEARCH + url).commit();
    }

    public static final class SearchAdapter extends ArrayAdapter<String> {
        String[] keys;

        public SearchAdapter(Context context, String[] keys2, String[] values) {
            super(context, 17367048, values);
            this.keys = keys2;
            setDropDownViewResource(17367049);
        }
    }

    private final class SearchRequestTask extends AsyncTask<String, Integer, Void> {
        private final boolean cached;
        private Exception error;
        private final boolean reset;
        /* access modifiers changed from: private */
        public String url;

        /* synthetic */ SearchRequestTask(AbstractEngine abstractEngine, boolean z, boolean z2, SearchRequestTask searchRequestTask) {
            this(z, z2);
        }

        private SearchRequestTask(boolean cached2, boolean reset2) {
            this.cached = cached2;
            this.reset = reset2;
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(String... params) {
            try {
                this.url = params[0];
                AbstractEngine.this.requestListUpdate(this.url, this.cached);
                SearchHeader header = AbstractEngine.this.xmlHandler.result.header;
                if (header.messages == null || header.messages.length <= 0 || AbstractEngine.this.xmlHandler.result.items.size() >= 1) {
                    return null;
                }
                this.error = new HireadroidException(TextUtils.join("\n", header.messages));
                return null;
            } catch (Exception e) {
                this.error = e;
                return null;
            }
        }

        /* access modifiers changed from: protected */
        public void onCancelled() {
            AbstractEngine.this.isUpdating = false;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Void v) {
            AbstractEngine.this.isUpdating = false;
            if (this.error != null) {
                AbstractEngine.this.list.footer.showError(Utils.unwrap(this.error), new View.OnClickListener() {
                    public void onClick(View v) {
                        AbstractEngine.this.onSearchRequest(SearchRequestTask.this.url, false, false);
                    }
                });
            } else {
                AbstractEngine.this.processResults(this.url, this.reset);
            }
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            AbstractEngine.this.isUpdating = true;
            AbstractEngine.this.list.footer.showInfoMessage("Please wait", true);
        }
    }

    public static String[] pickSupportingURLs(String theUrl) {
        Map<String, String> map = Utils.parseSearchParams(theUrl);
        String country = map.get(EngineConstants.COUNTRY);
        List<String> urls = new ArrayList<>();
        if (LinkUpEngine.supportsISO(country)) {
            urls.add(new LinkUpEngine.Request(map).toString());
        }
        if (SimplyHiredEngine.supportsISO(country)) {
            urls.add(new SimplyHiredEngine.Request(map).toString());
        }
        if (IndeedDotComEngine.supportsISO(country)) {
            urls.add(new IndeedDotComEngine.Request(map).toString());
        }
        if (CareerBuilderEngine.supportsISO(country)) {
            urls.add(new CareerBuilderEngine.Request(map).toString());
        }
        if (BeyondDotComEngine.supportsISO(country)) {
            urls.add(new BeyondDotComEngine.Request(map).toString());
        }
        return (String[]) urls.toArray(new String[0]);
    }

    public void initTabImage(int tabId2, final View.OnTouchListener gestureListener) {
        this.tabId = tabId2;
        if (this.trigger == null) {
            this.trigger = (ImageButton) ((ViewStub) this.row.findViewById(getStubByTabId(tabId2, this.row))).inflate().findViewById(R.id.tabBtt);
            this.trigger.setEnabled(true);
            this.trigger.setImageBitmap(BitmapFactory.decodeResource(this.activity.getResources(), this.logo));
            this.trigger.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    AbstractEngine.this.initActiveTab(gestureListener, false);
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    public int getStubByTabId(int tabId2, ViewGroup parent) {
        return parent.getChildAt(tabId2).getId();
    }
}
