package bostone.android.hireadroid.engine;

import android.util.Log;
import bostone.android.hireadroid.HireadroidException;
import bostone.android.hireadroid.R;
import bostone.android.hireadroid.dao.SearchItemsDbHelper;
import bostone.android.hireadroid.model.Country;
import bostone.android.hireadroid.sax.LinkUpHandler;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LinkUpEngine extends AbstractEngine {
    static final /* synthetic */ boolean $assertionsDisabled = (!LinkUpEngine.class.desiredAssertionStatus());
    static final Pattern BUMP_PTR = Pattern.compile("^(.+[^_]page=)(\\d+)(.*)$");
    private static final String CO = "&country=";
    public static final String[] ISO = {"US", Country.CA};
    static final Pattern ISO_PAT = Pattern.compile("^.+?\\&country=(\\w\\w).+$");
    public static final int LOGO = 2130837562;
    static final String[] SEARCH_KEYS = {"d", "r"};
    static final String[] SEARCH_VALUES = {"Date", "Relevance"};
    private static final String SORT_BY_DATE = "&sort=d";
    private static final String SORT_BY_REL = "&sort=r";
    public static final String TAG = "LinkUpEngine";
    public static final String TYPE = "LinkUp.com";
    private static final long serialVersionUID = 8511249281473274590L;
    public Request request;

    public LinkUpEngine() {
        this.xmlHandler = new LinkUpHandler();
        this.name = "LinkUp.com";
        this.logo = R.drawable.linkup_logo_small;
    }

    public String buildSearchUrl(Map<String, String> params) {
        this.request = new Request(params);
        return this.request.toString();
    }

    public static Map<String, String> parseUrl(String url) {
        HashMap<String, String> map = new HashMap<>();
        for (String part : url.split("&")) {
            String[] pair = part.split("=");
            String val = pair.length == 2 ? pair[1] : null;
            if ("keyword".equals(pair[0])) {
                map.put(EngineConstants.QUERY, val);
            } else if ("location".equals(pair[0])) {
                map.put(EngineConstants.LOCATION, val);
            } else if ("distance".equals(pair[0])) {
                map.put(EngineConstants.MILES, val);
            } else if (SearchItemsDbHelper.ItemColumns.LOC_COUNTRY.equals(pair[0])) {
                map.put(EngineConstants.COUNTRY, val);
            }
        }
        return map;
    }

    public String bumpPageNumber(String url) {
        Matcher m = BUMP_PTR.matcher(url);
        if (m.find()) {
            return m.replaceAll("$1" + (Integer.parseInt(m.group(2)) + 1) + "$3");
        }
        throw new HireadroidException("Failed to extract more items");
    }

    public static String getCountryIsoFromUrl(String url) {
        if (!$assertionsDisabled && url == null) {
            throw new AssertionError();
        } else if (!url.contains(CO)) {
            return "US";
        } else {
            int start = url.indexOf(CO) + CO.length();
            return url.substring(start, start + 2).toUpperCase();
        }
    }

    public static class Request {
        public static final String ROOT_URL = "http://www.linkup.com/developers/v-1/search-handler.js?api_key=9AA3DB5BF6049DB6672752C17B641B1A&per_page=20&page=1&embedded_search_key=2211148CC06978F33F2229941D25D85C";
        public static final String SEARCH_KEY = "2211148CC06978F33F2229941D25D85C";
        public String company;
        public String country;
        public String distance;
        public String keyword;
        public String location;
        public String method;
        public String page;
        public String perPage;
        public String sort;
        public String tags;
        public String timeFrame;

        Request(Map<String, String> params) {
            this.location = params.get(EngineConstants.LOCATION);
            this.keyword = params.get(EngineConstants.QUERY);
            this.sort = params.get(EngineConstants.ORDER);
            this.distance = params.get(EngineConstants.MILES);
            String iso = params.get(EngineConstants.COUNTRY);
            if (iso != null) {
                this.country = iso.toLowerCase();
            }
        }

        public String toString() {
            StringBuilder sb = new StringBuilder(ROOT_URL);
            try {
                sb.append("&keyword=").append(this.keyword == null ? "" : URLEncoder.encode(this.keyword, "utf-8"));
                sb.append("&location=").append(this.location == null ? "" : URLEncoder.encode(this.location, "utf-8"));
                sb.append("&sort=").append(this.sort == null ? "d" : this.sort);
                sb.append("&distance=").append(this.distance == null ? "" : this.distance);
                sb.append(LinkUpEngine.CO).append(this.country == null ? "us" : this.country);
                return sb.toString();
            } catch (UnsupportedEncodingException e) {
                UnsupportedEncodingException e2 = e;
                Log.e(LinkUpEngine.TAG, "Failed to encode search criteria", e2);
                throw new HireadroidException(e2);
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean supportsCountry(String country) {
        if (!supportsISO(country)) {
            return $assertionsDisabled;
        }
        this.enabled = true;
        return true;
    }

    public static boolean supportsISO(String country) {
        for (String iso : ISO) {
            if (iso.equalsIgnoreCase(country)) {
                return true;
            }
        }
        return $assertionsDisabled;
    }

    /* access modifiers changed from: protected */
    public String updateSearchUrl(boolean byRelevance) {
        if (byRelevance && this.searchUrl.contains(SORT_BY_DATE)) {
            this.searchUrl = this.searchUrl.replace(SORT_BY_DATE, SORT_BY_REL);
        } else if (!byRelevance && this.searchUrl.contains(SORT_BY_REL)) {
            this.searchUrl = this.searchUrl.replace(SORT_BY_REL, SORT_BY_DATE);
        }
        return this.searchUrl;
    }
}
