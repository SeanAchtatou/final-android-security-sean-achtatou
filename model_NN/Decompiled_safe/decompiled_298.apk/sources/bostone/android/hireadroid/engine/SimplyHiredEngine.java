package bostone.android.hireadroid.engine;

import android.text.TextUtils;
import android.util.Log;
import bostone.android.hireadroid.R;
import bostone.android.hireadroid.model.Country;
import bostone.android.hireadroid.sax.SimplyHiredHandler;
import bostone.android.hireadroid.utils.Utils;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SimplyHiredEngine extends AbstractEngine {
    static final /* synthetic */ boolean $assertionsDisabled = (!SimplyHiredEngine.class.desiredAssertionStatus());
    public static final Pattern CRITERIA_PTT = Pattern.compile("\\/q-(.*?)\\?");
    public static String[] ISO = {"US", Country.AU, Country.BE, Country.BR, Country.CA, Country.CN, Country.FR, Country.DE, Country.IN, Country.IE, Country.IT, Country.JP, Country.MX, Country.NL, Country.ES, Country.CH, Country.UK};
    static final String[] KEYS = {EngineConstants.QUERY, EngineConstants.LOCATION, EngineConstants.MILES, SORTED_BY, WINDOW_SIZE, PAGE_NUM};
    public static final int LOGO = 2130837569;
    public static final String PAGE_NUM = "pn";
    static final String[] SEARCH_KEYS = {"dd", "da", "rd", "ra", "td", "ta", "cd", "ca", "ld", "la"};
    static final String[] SEARCH_VALUES = {"Date (desc)", "Date (asc)", "Relevance (desc)", "Relevance (asc)", "Title (desc)", "Title (asc)", "Company (desc)", "Company (asc)", "Location (desc)", "Location (asc)"};
    public static final String SORTED_BY = "sb";
    public static final String SORT_BY_DATE = "/sb-dd";
    public static final String SORT_BY_REL = "/sb-rd";
    private static final String TAG = "SimplyHiredEngine";
    public static final String TYPE = "SimplyHired";
    public static final String WINDOW_SIZE = "ws";
    private static final long serialVersionUID = 7038646063962741318L;

    public SimplyHiredEngine() {
        this.xmlHandler = new SimplyHiredHandler();
        this.name = TYPE;
        this.logo = R.drawable.simplyhired_small;
    }

    public static Map<String, String> parseUrl(String url) {
        HashMap<String, String> map = new HashMap<>();
        String country = getCountryIsoFromUrl(url);
        if (!TextUtils.isEmpty(country)) {
            map.put(EngineConstants.COUNTRY, country);
        }
        String url2 = url.substring(url.indexOf("/pn-"), url.indexOf("?"));
        for (String pair : url2.split("/")) {
            if (Utils.isNotEmpty(pair) && pair.contains("-")) {
                String[] strArr = KEYS;
                int length = strArr.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        break;
                    }
                    String key = strArr[i];
                    if (pair.contains(key)) {
                        map.put(key, pair.substring(pair.indexOf(key) + key.length() + 1));
                        break;
                    }
                    i++;
                }
            }
        }
        Matcher m = CRITERIA_PTT.matcher(url2);
        if (m.find()) {
            map.put(EngineConstants.QUERY, m.group(1));
        }
        return map;
    }

    public String buildSearchUrl(Map<String, String> params) {
        return new Request(params).toString();
    }

    private static class BaseUrl implements Serializable {
        private static final long serialVersionUID = -5510836676025250726L;
        /* access modifiers changed from: private */
        public final String base;
        /* access modifiers changed from: private */
        public final String query;

        /* synthetic */ BaseUrl(String str, String str2, BaseUrl baseUrl) {
            this(str, str2);
        }

        private BaseUrl(String base2, String query2) {
            this.base = base2;
            this.query = query2;
        }
    }

    public String bumpPageNumber(String url) {
        int pos = url.indexOf("/pn-");
        return String.valueOf(url.substring(0, pos + 4)) + (Character.getNumericValue(url.charAt(pos + 4)) + 1) + url.substring(pos + 5);
    }

    public static String getCountryIsoFromUrl(String url) {
        if (!$assertionsDisabled && url == null) {
            throw new AssertionError();
        } else if (!url.contains("api.simplyhired.")) {
            return "US";
        } else {
            int end = url.indexOf(47, 9);
            String c = url.substring(end - 2, end);
            return "om".equalsIgnoreCase(c) ? "US" : c.toUpperCase();
        }
    }

    /* access modifiers changed from: protected */
    public boolean supportsCountry(String country) {
        for (String iso : ISO) {
            if (iso.equalsIgnoreCase(country)) {
                this.enabled = true;
                return true;
            }
        }
        return $assertionsDisabled;
    }

    public static boolean supportsISO(String country) {
        for (String iso : ISO) {
            if (iso.equalsIgnoreCase(country)) {
                return true;
            }
        }
        return $assertionsDisabled;
    }

    public static class Request {
        final Map<String, String> params;
        private final HashMap<String, BaseUrl> urls = new HashMap<>(17);

        private BaseUrl getUrlByIso(String iso) {
            return this.urls.get(iso);
        }

        public Request(Map<String, String> params2) {
            this.params = params2;
            this.urls.put("US", new BaseUrl("http://api.simplyhired.com/a/jobs-api/xml-v2/ws-20/pn-1", "?pshid=17415&ssty=2&cflg=r&jbd=hireadroid.jobamatic.com&clip=", null));
            this.urls.put(Country.AU, new BaseUrl("http://api.simplyhired.com.au/a/jobs-api/xml-v2/ws-20/pn-1", "?pshid=17415&ssty=3&cflg=r&clip=", null));
            this.urls.put(Country.BE, new BaseUrl("http://api.simplyhired.be/a/jobs-api/xml-v2/ws-20/pn-1", "?pshid=17415&ssty=3&cflg=r&clip=", null));
            this.urls.put(Country.BR, new BaseUrl("http://api.simplyhired.com.br/a/jobs-api/xml-v2/ws-20/pn-1", "?pshid=17415&ssty=3&cflg=r&clip=", null));
            this.urls.put(Country.CA, new BaseUrl("http://api.simplyhired.ca/a/jobs-api/xml-v2/ws-20/pn-1", "?pshid=17415&ssty=3&cflg=r&clip=", null));
            this.urls.put(Country.CN, new BaseUrl("http://api.simplyhired.cn/a/jobs-api/xml-v2/ws-20/pn-1", "?pshid=17415&ssty=3&cflg=r&clip=", null));
            this.urls.put(Country.FR, new BaseUrl("http://api.simplyhired.fr/a/jobs-api/xml-v2/ws-20/pn-1", "?pshid=17415&ssty=3&cflg=r&clip=", null));
            this.urls.put(Country.DE, new BaseUrl("http://api.simplyhired.de/a/jobs-api/xml-v2/ws-20/pn-1", "?pshid=17415&ssty=3&cflg=r&clip=", null));
            this.urls.put(Country.IN, new BaseUrl("http://api.simplyhired.co.in/a/jobs-api/xml-v2/ws-20/pn-1", "?pshid=17415&ssty=3&cflg=r&clip=", null));
            this.urls.put(Country.IE, new BaseUrl("http://api.simplyhired.ie/a/jobs-api/xml-v2/ws-20/pn-1", "?pshid=17415&ssty=3&cflg=r&clip=", null));
            this.urls.put(Country.IT, new BaseUrl("http://api.simplyhired.it/a/jobs-api/xml-v2/ws-20/pn-1", "?pshid=17415&ssty=3&cflg=r&clip=", null));
            this.urls.put(Country.JP, new BaseUrl("http://api.simplyhired.jp/a/jobs-api/xml-v2/ws-20/pn-1", "?pshid=17415&ssty=3&cflg=r&clip=", null));
            this.urls.put(Country.MX, new BaseUrl("http://api.simplyhired.mx/a/jobs-api/xml-v2/ws-20/pn-1", "?pshid=17415&ssty=3&cflg=r&clip=", null));
            this.urls.put(Country.NL, new BaseUrl("http://api.simplyhired.nl/a/jobs-api/xml-v2/ws-20/pn-1", "?pshid=17415&ssty=3&cflg=r&clip=", null));
            this.urls.put(Country.ES, new BaseUrl("http://api.simplyhired.es/a/jobs-api/xml-v2/ws-20/pn-1", "?pshid=17415&ssty=3&cflg=r&clip=", null));
            this.urls.put(Country.CH, new BaseUrl("http://api.simplyhired.ch/a/jobs-api/xml-v2/ws-20/pn-1", "?pshid=17415&ssty=3&cflg=r&clip=", null));
            this.urls.put(Country.UK, new BaseUrl("http://api.simplyhired.co.uk/a/jobs-api/xml-v2/ws-20/pn-1", "?pshid=17415&ssty=3&cflg=r&clip=", null));
        }

        private String encode(String string) {
            try {
                return URLEncoder.encode(string, "utf-8");
            } catch (UnsupportedEncodingException e) {
                Log.e(SimplyHiredEngine.TAG, "SimplyHiredEngine - encoding error", e);
                return string;
            }
        }

        /* Debug info: failed to restart local var, previous not found, register: 9 */
        public String toString() {
            StringBuilder sb = new StringBuilder();
            BaseUrl url = getUrlByIso(this.params.get(EngineConstants.COUNTRY));
            sb.append(url.base);
            String z = this.params.get(EngineConstants.LOCATION);
            if (Utils.isNotEmpty(z)) {
                sb.append("/l-").append(encode(z));
                int r = Integer.parseInt(this.params.get(EngineConstants.MILES));
                if (r == 0) {
                    sb.append("/mi-exact");
                } else if (r > 0 && r < 101) {
                    sb.append("/mi-").append(r);
                }
            }
            String so = this.params.get(EngineConstants.ORDER);
            if (Utils.isNotEmpty(so)) {
                sb.append("/sb-").append(so);
            } else {
                sb.append(SimplyHiredEngine.SORT_BY_DATE);
            }
            sb.append("/q-");
            String q = this.params.get(EngineConstants.QUERY);
            if (Utils.isNotEmpty(q)) {
                sb.append(encode(q));
            } else {
                sb.append("");
            }
            sb.append(url.query).append(this.params.get(EngineConstants.IP));
            return sb.toString();
        }
    }

    /* access modifiers changed from: protected */
    public String updateSearchUrl(boolean byRelevance) {
        if (byRelevance && this.searchUrl.contains(SORT_BY_DATE)) {
            this.searchUrl = this.searchUrl.replace(SORT_BY_DATE, SORT_BY_REL);
        } else if (!byRelevance && this.searchUrl.contains(SORT_BY_REL)) {
            this.searchUrl = this.searchUrl.replace(SORT_BY_REL, SORT_BY_DATE);
        }
        return this.searchUrl;
    }
}
