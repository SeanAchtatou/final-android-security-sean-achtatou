package bostone.android.hireadroid.engine;

public interface EngineConstants {
    public static final String AGENT = "Mozilla%2F5.0%20(Linux%3B%20U%3B%20Android%200.5%3B%20en-us)%20AppleWebKit%2F522%2B%20(KHTML%2C%20like%20Gecko)%20Safari%2F419.3%20";
    public static final String CLICK_ACTION = "bostone.android.hreadroid.TRIGGER_CLICK";
    public static final String COUNTRY = "COUNTRY";
    public static final String COUNTRY_ID = "COUNTRY_ID";
    public static final int COUNTRY_NOT_FOUND = -1;
    public static final String CURR_SUMMARY_URL = "CURR_SUMMARY_URL";
    public static final String DEFAULT_COUNTRY = "US";
    public static final String DEFAULT_TYPE = "LinkUp.com";
    public static final String ENGINES = "ENGINES";
    public static final String ENGINES_URL = "http://hireadroid.com/app/engines";
    public static final String[] ENGINE_NAMES = {BeyondDotComEngine.TYPE, "LinkUp.com", LinkedInEngine.TYPE, SimplyHiredEngine.TYPE, IndeedDotComEngine.TYPE, CareerBuilderEngine.TYPE};
    public static final String EXACT = "exact";
    public static final String IP = "ip";
    public static final String LAST_SEARCH = "LAST_SEARCH";
    public static final String LOCATION = "l";
    public static final String MILES = "mi";
    public static final String NAME = "ENGINE_NAME";
    public static final String ORDER = "so";
    public static final String QUERY = "q";
    public static final String SEARCH = "SEARCH_";
    public static final String UPDATE_FAILED = "UPDATE_FAILED";
}
