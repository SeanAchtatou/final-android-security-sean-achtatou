package bostone.android.hireadroid.utils;

import android.os.Environment;
import android.util.Log;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.NotActiveException;

public class SDCardRepo {
    private static final String TAG = "SDCardRepo";
    public static SDCardRepo repo = new SDCardRepo();
    String path = "/hireadroid";
    private File root;

    static {
        if (repo == null) {
        }
    }

    private SDCardRepo() {
        try {
            this.root = new File(String.valueOf(Environment.getExternalStorageDirectory().getPath()) + this.path);
            if (!this.root.exists() && !this.root.mkdir()) {
                this.root.createNewFile();
            }
        } catch (Exception e) {
            Log.e(TAG, "SDCardRepo#SDCardRepo:37-40", e);
            this.root = null;
        }
    }

    public static boolean isAvailable() {
        return (repo == null || repo.root == null || !repo.root.canWrite()) ? false : true;
    }

    public static void put(String url, String xml) throws IOException {
        repo._put(url, xml);
    }

    public static String get(String url) throws IOException {
        return repo._get(url);
    }

    public static void remove(String url) throws IOException {
        repo._remove(url);
    }

    public static void clear() throws IOException {
        repo._clear();
    }

    private void _clear() throws IOException {
        if (!isAvailable()) {
            throw new NotActiveException();
        }
        File[] files = this.root.listFiles();
        if (files != null) {
            for (File f : files) {
                f.delete();
            }
        }
    }

    private void _put(String url, String xml) throws IOException {
        if (!isAvailable()) {
            throw new NotActiveException();
        }
        File file = _getFile(url);
        if (!file.exists()) {
            file.createNewFile();
        }
        BufferedWriter out = new BufferedWriter(new FileWriter(file), 1024);
        if (xml != null) {
            out.write(xml);
            out.close();
            return;
        }
        throw new IOException("Cannot cache empty content");
    }

    private String _get(String url) throws IOException {
        if (!isAvailable()) {
            throw new NotActiveException();
        }
        File file = _getFile(url);
        if (!file.exists()) {
            return null;
        }
        BufferedReader in = new BufferedReader(new FileReader(file), 1024);
        StringBuilder sb = new StringBuilder();
        while (true) {
            String line = in.readLine();
            if (line == null) {
                return sb.toString();
            }
            sb.append(line);
        }
    }

    private void _remove(String url) throws IOException {
        if (!isAvailable()) {
            throw new NotActiveException();
        }
        _getFile(url).delete();
    }

    private File _getFile(String url) {
        return new File(this.root, Utils.compress(url));
    }
}
