package bostone.android.hireadroid.utils;

import android.content.Context;
import android.content.SharedPreferences;
import java.util.Iterator;
import java.util.Map;

public class SavedSearchRepo {
    static final /* synthetic */ boolean $assertionsDisabled = (!SavedSearchRepo.class.desiredAssertionStatus() ? true : $assertionsDisabled);
    public static final String SEARCH_REPO = "SEARCH_REPO";
    static final String TOGGLE = "TOGGLE_";

    public static Map<String, String> getSavedSearches(Context context) {
        Map all = getPrefs(context).getAll();
        Iterator<String> iter = all.keySet().iterator();
        while (iter.hasNext()) {
            if (((String) iter.next()).startsWith(TOGGLE)) {
                iter.remove();
            }
        }
        return all;
    }

    public static void put(String key, String url, Context context) {
        if (!$assertionsDisabled && key == null) {
            throw new AssertionError();
        } else if ($assertionsDisabled || url != null) {
            getPrefs(context).edit().putString(key, url).commit();
        } else {
            throw new AssertionError();
        }
    }

    public static SharedPreferences getPrefs(Context context) {
        return context.getSharedPreferences(SEARCH_REPO, 0);
    }

    public static void remove(String key, Context context) {
        getPrefs(context).edit().remove(key).commit();
    }

    public static void toggleUpdatesNotify(Context context, int id, boolean checked) {
        String key = TOGGLE + id;
        if (checked) {
            getPrefs(context).edit().putInt(key, countUpdateNotify(context, id) + 1).commit();
            return;
        }
        getPrefs(context).edit().remove(key).commit();
    }

    public static int countUpdateNotify(Context context, int id) {
        return getPrefs(context).getInt(TOGGLE + id, 0);
    }
}
