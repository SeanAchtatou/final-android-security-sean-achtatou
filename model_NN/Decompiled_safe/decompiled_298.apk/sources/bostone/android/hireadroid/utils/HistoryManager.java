package bostone.android.hireadroid.utils;

import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import bostone.android.hireadroid.dao.ItemDao;
import bostone.android.hireadroid.dao.SearchItemsDbHelper;
import bostone.android.hireadroid.engine.EngineConstants;
import bostone.android.hireadroid.net.RequestHandler;
import java.io.File;
import java.util.Date;

public class HistoryManager {
    public static final int AUTO = 0;
    public static final String LAST_HISTORY_CLEANUP = "LAST_HISTORY_CLEANUP";
    public static final String MAINTANACE_TYPE = "MAINTANACE_TYPE";
    private static final long MAX_DB_SIZE = 1048576;
    public static final int NONE = -1;
    private static final long ONE_DAY = 86400;
    private static final String TAG = "HistoryManager";
    private boolean auto;
    /* access modifiers changed from: private */
    public final Context context;
    /* access modifiers changed from: private */
    public final SharedPreferences prefs;
    private Thread t;

    public HistoryManager(Context context2) {
        this.context = context2;
        this.prefs = PreferenceManager.getDefaultSharedPreferences(context2);
    }

    public void start() {
        if (this.t != null && this.t.isAlive()) {
            Log.i(TAG, "History cleanup is currently running, skipping.");
        } else if (isTimeToClean()) {
            this.t = new Thread(new Runnable() {
                public void run() {
                    HistoryManager.this.cleanUp();
                }
            }, getClass().getSimpleName());
            this.t.start();
        }
    }

    public void refreshEngineList() {
        new AsyncTask<Void, Void, Void>() {
            /* access modifiers changed from: protected */
            public Void doInBackground(Void... params) {
                try {
                    String names = new RequestHandler(HistoryManager.this.context).get("http://droidin.net/engines")[1];
                    if (names == null || !Utils.isNotEmpty(names) || !names.contains(",")) {
                        return null;
                    }
                    HistoryManager.this.prefs.edit().putString(EngineConstants.ENGINES, names).commit();
                    return null;
                } catch (Throwable th) {
                    Log.e(HistoryManager.TAG, "failed to capture remote list name", th);
                    return null;
                }
            }
        }.execute(new Void[0]);
    }

    /* access modifiers changed from: protected */
    public void cleanUp() {
        if (this.auto) {
            autoCleanUp();
        } else {
            timelyCleanUp();
        }
    }

    private void timelyCleanUp() {
        int limit = this.prefs.getInt(MAINTANACE_TYPE, 0);
        long now = new Date().getTime();
        long ts = now - (((long) limit) * ONE_DAY);
        if (ts > 0) {
            try {
                ItemDao.delete(this.context.getContentResolver(), "postedDate<?", new String[]{Long.toString(ts)});
                this.prefs.edit().putLong(LAST_HISTORY_CLEANUP, now).commit();
            } catch (Exception e) {
                Log.e(TAG, "Failed to clean old history", e);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void autoCleanUp() {
        long ts = 0;
        ContentResolver resolver = this.context.getContentResolver();
        do {
            Cursor c = null;
            try {
                Cursor c2 = ItemDao.query(resolver, new String[]{"MIN(postedDate) as ts"}, null, null, null);
                if (c2.moveToFirst()) {
                    ts = c2.getLong(0);
                }
                if (c2 != null && !c2.isClosed()) {
                    try {
                        c2.close();
                    } catch (Exception e) {
                        Log.e(TAG, "Can't close cursor", e);
                    }
                }
            } catch (Exception e2) {
                Log.e(TAG, "Can't query oldest ts", e2);
                if (c != null && !c.isClosed()) {
                    try {
                        c.close();
                    } catch (Exception e3) {
                        Log.e(TAG, "Can't close cursor", e3);
                    }
                }
            } catch (Throwable th) {
                if (c != null && !c.isClosed()) {
                    try {
                        c.close();
                    } catch (Exception e4) {
                        Log.e(TAG, "Can't close cursor", e4);
                    }
                }
                throw th;
            }
            if (ts > 0) {
                ItemDao.delete(resolver, "postedDate<?", new String[]{Long.toString(ONE_DAY + ts)});
            }
        } while (isTimeToAutoClean());
    }

    private boolean isTimeToClean() {
        int type = this.prefs.getInt(MAINTANACE_TYPE, 0);
        switch (type) {
            case -1:
                return false;
            case 0:
                this.auto = true;
                return isTimeToAutoClean();
            default:
                return isTimeToTimelyClean(type);
        }
    }

    private boolean isTimeToTimelyClean(int limit) {
        long last = this.prefs.getLong(LAST_HISTORY_CLEANUP, 0);
        if (last != 0) {
            return new Date().getTime() - (((long) limit) * ONE_DAY) > last;
        }
        timelyCleanUp();
        return false;
    }

    private boolean isTimeToAutoClean() {
        File f = new File(String.valueOf(Environment.getDataDirectory().getPath()) + "/data/" + this.context.getPackageName() + "/databases/" + SearchItemsDbHelper.DB_NAME);
        if (!f.exists()) {
            return false;
        }
        return f.length() > MAX_DB_SIZE;
    }
}
