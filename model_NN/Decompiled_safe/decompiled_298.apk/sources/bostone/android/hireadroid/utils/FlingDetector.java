package bostone.android.hireadroid.utils;

import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;

public abstract class FlingDetector extends GestureDetector.SimpleOnGestureListener {
    private static final int SWIPE_MAX_OFF_PATH = 250;
    private static final int SWIPE_MIN_DISTANCE = 120;
    private static final int SWIPE_THRESHOLD_VELOCITY = 200;
    private static final String TAG = "FlingDetector";

    public abstract boolean onLeftToRightFling(int i, int i2);

    public abstract boolean onRightToLeftFling(int i, int i2);

    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        try {
            if (Math.abs(e1.getY() - e2.getY()) > 250.0f) {
                return false;
            }
            if (e1.getX() - e2.getX() > 120.0f && Math.abs(velocityX) > 200.0f) {
                return onRightToLeftFling((int) e1.getX(), (int) e1.getY());
            }
            if (e2.getX() - e1.getX() > 120.0f && Math.abs(velocityX) > 200.0f) {
                return onLeftToRightFling((int) e1.getX(), (int) e1.getY());
            }
            return false;
        } catch (Exception e) {
            Log.e(TAG, "Failed fling gesture", e);
        }
    }
}
