package bostone.android.hireadroid.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import bostone.android.hireadroid.R;
import bostone.android.hireadroid.activity.MonitoredActivity;
import bostone.android.hireadroid.dao.SearchItemsDbHelper;
import bostone.android.hireadroid.engine.BeyondDotComEngine;
import bostone.android.hireadroid.engine.CareerBuilderEngine;
import bostone.android.hireadroid.engine.EngineConstants;
import bostone.android.hireadroid.engine.IndeedDotComEngine;
import bostone.android.hireadroid.engine.LinkUpEngine;
import bostone.android.hireadroid.engine.LinkedInEngine;
import bostone.android.hireadroid.engine.SimplyHiredEngine;
import bostone.android.hireadroid.model.SearchItem;
import bostone.android.hireadroid.net.RequestHandler;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.NoRouteToHostException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.HttpHostConnectException;
import org.json.JSONException;
import org.json.JSONObject;

public class Utils {
    /* access modifiers changed from: private */
    public static final String[] BITLY = {"http://api.bit.ly/shorten?version=2.0.1&longUrl=", "&login=droid&apiKey=R_4795dd5b6796e5a481268a76a49b4d13"};
    static final Map<String, SimpleDateFormat> DATE_FORMATS = new HashMap();
    public static final String[] MONTHS = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    private static final SimpleDateFormat SDF = new SimpleDateFormat("MMM d, y");
    private static final String TAG = "Utils";

    static {
        DATE_FORMATS.put("LinkUp.com", SDF);
        DATE_FORMATS.put(BeyondDotComEngine.TYPE, new SimpleDateFormat("y/M/d h:m:s"));
        DATE_FORMATS.put(LinkedInEngine.TYPE, new SimpleDateFormat("y/M/d h:m:s"));
        DATE_FORMATS.put(SimplyHiredEngine.TYPE, new SimpleDateFormat("y-M-d'T'h:m:s'Z'"));
        DATE_FORMATS.put(IndeedDotComEngine.TYPE, new SimpleDateFormat("E, d MMMM y h:m:s Z"));
        DATE_FORMATS.put(CareerBuilderEngine.TYPE, new SimpleDateFormat("M/d/y"));
    }

    public static String getMonth(String string) {
        if (isEmpty(string)) {
            return null;
        }
        return MONTHS[Integer.parseInt(string) - 1];
    }

    public static String getStringVal(int id, Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(context.getString(id), null);
    }

    public static String getStringVal(String name, Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(name, null);
    }

    public static String getStringVal(String name, Context context, String defValue) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(name, defValue);
    }

    public static void setString(int id, String value, Context context) {
        getPrefs(context).edit().putString(context.getString(id), value).commit();
    }

    public static void setString(String name, String value, Context context) {
        getPrefs(context).edit().putString(name, value).commit();
    }

    public static void remove(int id, Context context) {
        getPrefs(context).edit().remove(context.getString(id)).commit();
    }

    public static void remove(String name, Context context) {
        getPrefs(context).edit().remove(name).commit();
    }

    public static SharedPreferences getPrefs(Context ctx) {
        return PreferenceManager.getDefaultSharedPreferences(ctx);
    }

    public static void showAlertAndDismiss(String title, String message, String okBtt, final Activity activity, boolean withCancel) {
        if (activity != null) {
            try {
                if (!activity.isFinishing()) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(activity).setMessage(message).setTitle(title).setPositiveButton(okBtt, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            activity.finish();
                        }
                    }).setNeutralButton("Retry", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            activity.startActivity(activity.getIntent());
                            activity.finish();
                        }
                    });
                    if (withCancel) {
                        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                    }
                    alert.show();
                }
            } catch (Exception e) {
                Log.e(TAG, "Unable to display alert", e);
            }
        }
    }

    public static int countPositives(SparseBooleanArray items, long[] checked) {
        int counter = 0;
        for (long j : checked) {
            if (items.get((int) j)) {
                counter++;
            }
        }
        return counter;
    }

    public static Dialog buildInfoDialog(Context context, int titleId, int txtId) {
        final Dialog dlg = new Dialog(context);
        View.OnClickListener listener = new View.OnClickListener() {
            public void onClick(View v) {
                dlg.dismiss();
            }
        };
        dlg.setContentView((int) R.layout.info_dlg);
        dlg.setTitle(titleId);
        TextView txt = (TextView) dlg.findViewById(R.id.infoView);
        txt.setText(txtId);
        txt.setOnClickListener(listener);
        return dlg;
    }

    public static Dialog buildInfoDialog(Context context, String title, String text) {
        final Dialog dlg = new Dialog(context);
        View.OnClickListener listener = new View.OnClickListener() {
            public void onClick(View v) {
                dlg.dismiss();
            }
        };
        dlg.setContentView((int) R.layout.info_dlg);
        dlg.setTitle(title);
        TextView txt = (TextView) dlg.findViewById(R.id.infoView);
        txt.setText(text);
        txt.setOnClickListener(listener);
        return dlg;
    }

    public static String parseBitly(String value) throws JSONException {
        JSONObject json = new JSONObject(value);
        if (!json.has("statusCode") || !"OK".equals(json.get("statusCode")) || !json.has("results")) {
            return null;
        }
        JSONObject res = json.getJSONObject("results");
        return res.getJSONObject(res.keys().next()).getString(SearchItemsDbHelper.ItemColumns.SHORT_URL);
    }

    public static long[] getCheckItemIds(ListView lv) {
        ListAdapter adapter = lv.getAdapter();
        if (lv.getChoiceMode() == 0 || adapter == null) {
            return new long[0];
        }
        SparseBooleanArray states = lv.getCheckedItemPositions();
        int count = states.size();
        long[] ids = new long[count];
        for (int i = 0; i < count; i++) {
            ids[i] = adapter.getItemId(states.keyAt(i));
        }
        return ids;
    }

    public static String getUrl(Context context, String key) {
        return SavedSearchRepo.getSavedSearches(context).get(key);
    }

    public static boolean isEmpty(String string) {
        return string == null || string.trim().length() < 1;
    }

    public static boolean isNotEmpty(String string) {
        return !isEmpty(string);
    }

    public static String formatError(String error) {
        if (isEmpty(error)) {
            return "Unknown Error";
        }
        int idx = error.indexOf("Exception:");
        return idx != -1 ? error.substring(idx + 10) : error;
    }

    public static String formatError(Throwable t) {
        String cause = t == null ? "Unknown error" : t.getMessage();
        while (isEmpty(cause) && (t = t.getCause()) != null) {
            cause = t.getMessage();
        }
        return formatError(cause);
    }

    public static String getLocalIpAddress() {
        try {
            Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces();
            while (en.hasMoreElements()) {
                Enumeration<InetAddress> enumIpAddr = en.nextElement().getInetAddresses();
                while (true) {
                    if (enumIpAddr.hasMoreElements()) {
                        InetAddress ip = enumIpAddr.nextElement();
                        if (!ip.isLoopbackAddress() && !ip.isLinkLocalAddress() && !ip.isAnyLocalAddress()) {
                            return ip.getHostAddress().toString();
                        }
                    }
                }
            }
        } catch (SocketException e) {
            Log.e(TAG, "Utils#getLocalIpAddress:220-225", e);
        }
        return "157.166.226.26";
    }

    public static Map<String, String> parseSearchParams(String url) {
        Map<String, String> map = new HashMap<>();
        if (TextUtils.isEmpty(url)) {
            return map;
        }
        if (url.startsWith("hireadroid:")) {
            for (String val : url.substring(url.indexOf(63) + 1).split("&")) {
                String[] tuple = val.split("=");
                if (tuple != null) {
                    map.put(tuple[0], tuple.length > 1 ? tuple[1] : "");
                }
            }
            return map;
        }
        String name = getNameFromUrl(url);
        if ("LinkUp.com".equals(name)) {
            return LinkUpEngine.parseUrl(url);
        }
        if (BeyondDotComEngine.TYPE.equals(name)) {
            return BeyondDotComEngine.parseUrl(url);
        }
        if (SimplyHiredEngine.TYPE.equals(name)) {
            return SimplyHiredEngine.parseUrl(url);
        }
        if (IndeedDotComEngine.TYPE.equals(name)) {
            return IndeedDotComEngine.parseUrl(url);
        }
        if (CareerBuilderEngine.TYPE.equals(name)) {
            return CareerBuilderEngine.parseUrl(url);
        }
        if (LinkedInEngine.TYPE.equals(name)) {
            return LinkedInEngine.parseUrl(url);
        }
        return map;
    }

    public static String getNameFromUrl(String url) {
        if (url.contains("simplyhired") || url.contains("jobamatic")) {
            return SimplyHiredEngine.TYPE;
        }
        if (url.contains("indeed.")) {
            return IndeedDotComEngine.TYPE;
        }
        if (url.contains("linkup.com")) {
            return "LinkUp.com";
        }
        if (url.contains("linkedin.com")) {
            return LinkedInEngine.TYPE;
        }
        if (url.contains("careerbuilder.")) {
            return CareerBuilderEngine.TYPE;
        }
        if (url.contains("beyond.")) {
            return BeyondDotComEngine.TYPE;
        }
        throw new IllegalArgumentException("Unsupported URL " + url);
    }

    public static String unwrap(Throwable t) {
        if ((t instanceof UnknownHostException) || (t instanceof SocketException) || (t instanceof ConnectTimeoutException) || (t instanceof NoRouteToHostException) || (t instanceof SocketTimeoutException) || (t instanceof HttpHostConnectException)) {
            return "<font color='red'>Network connection problem</font>";
        }
        Throwable temp = t;
        StringBuilder sb = new StringBuilder();
        while (true) {
            String msg = temp.getMessage();
            if (msg == null) {
                break;
            }
            if (sb.length() == 0) {
                sb.append("<font color='red'>");
            }
            sb.append(msg);
            temp = temp.getCause();
            if (temp == null || !isNotEmpty(t.getMessage())) {
                break;
            }
            sb.append("->");
        }
        if (sb.length() == 0) {
            return "<font color='red'>" + t.getClass().getSimpleName() + "</font>";
        }
        sb.append("</font>");
        return sb.toString();
    }

    public static String extract(String xml, Pattern pattern) {
        if (xml == null) {
            return null;
        }
        Matcher m = pattern.matcher(xml);
        if (m.find()) {
            return m.group(1);
        }
        return null;
    }

    public static List<String> fromCursor(Cursor c, int colIdx) {
        List<String> list = new ArrayList<>();
        while (c.moveToNext()) {
            list.add(c.getString(colIdx));
        }
        return list;
    }

    public static void fetchBitlyUrl(final Context context, final SearchItem item) {
        new AsyncTask<Void, Void, String>() {
            /* access modifiers changed from: protected */
            public String doInBackground(Void... params) {
                RequestHandler handler = new RequestHandler(context);
                String u = String.valueOf(Utils.BITLY[0]) + Uri.encode(item.listingUrl) + Utils.BITLY[1];
                Log.d("Utils.fetchBitlyUrl", u);
                try {
                    return handler.get(u)[1];
                } catch (Exception e) {
                    Log.e("bitlyHandler", "Failed to get short URL from Bit.ly", e);
                    return null;
                }
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(String result) {
                if (Utils.isNotEmpty(result)) {
                    try {
                        item.shortUrl = Utils.parseBitly(result);
                    } catch (Exception e) {
                        Log.e("Utils.fetchBitlyUrl", "Failed to parse", e);
                    }
                }
            }
        }.execute(new Void[0]);
    }

    public static void share(Activity activity, SearchItem item) {
        if (isEmpty(item.jobTitle)) {
            item.jobTitle = "HireADroid - job search on Android";
        }
        shareTextBy(activity, String.valueOf(item.jobTitle.length() > 95 ? item.jobTitle.substring(0, 95) : item.jobTitle) + ": " + (isEmpty(item.shortUrl) ? item.listingUrl : item.shortUrl) + ". More jobs at hireadroid.com");
        MonitoredActivity.event("Job Action", "Share by", "Shared from seach results");
    }

    public static void shareTextBy(Activity activity, String text) {
        Intent i = new Intent("android.intent.action.SEND");
        i.setType("text/plain");
        i.putExtra("android.intent.extra.SUBJECT", "HireADroid - job search on your 'Droid");
        i.putExtra("android.intent.extra.TEXT", text);
        try {
            activity.startActivity(Intent.createChooser(i, "Send this job by"));
        } catch (Exception e) {
            Exception e2 = e;
            if (!activity.isFinishing()) {
                Toast.makeText(activity, "Failed to share\n" + unwrap(e2), 1).show();
            }
        }
    }

    public static String compress(String input) {
        try {
            String md5 = new BigInteger(1, MessageDigest.getInstance("MD5").digest(input.getBytes())).toString(16);
            while (md5.length() < 32) {
                md5 = "0" + md5;
            }
            return md5;
        } catch (NoSuchAlgorithmException e) {
            Log.e(TAG, "SDCardRepo#shorten:133-138", e);
            return null;
        }
    }

    public static CharSequence prettyDate(long ts) {
        Date today = new Date();
        Date date = new Date(ts);
        if (date.getYear() == today.getYear() && date.getMonth() == today.getMonth()) {
            int t = today.getDate();
            int d = date.getDate();
            if (d == t) {
                return "Today";
            }
            if (t - d == 1) {
                return "Yesterday";
            }
        }
        return SDF.format(date);
    }

    public static long convertToTs(String date, String engine) {
        SimpleDateFormat sdf = DATE_FORMATS.get(engine);
        if (sdf != null) {
            try {
                return sdf.parse(date).getTime();
            } catch (ParseException e) {
                Log.e(TAG, "Failed to parse date: " + date + ", for " + engine);
            }
        }
        return 0;
    }

    public static String[] getEngineNames(Context context) {
        String[] names = null;
        String allnames = getPrefs(context).getString(EngineConstants.ENGINES, null);
        if (allnames != null && isNotEmpty(allnames)) {
            names = allnames.split(",");
        }
        return (names == null || names.length < 1) ? EngineConstants.ENGINE_NAMES : names;
    }
}
