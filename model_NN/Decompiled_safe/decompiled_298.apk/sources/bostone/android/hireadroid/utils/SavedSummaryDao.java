package bostone.android.hireadroid.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import java.io.IOException;
import java.util.Map;

public class SavedSummaryDao {
    static final /* synthetic */ boolean $assertionsDisabled = (!SavedSummaryDao.class.desiredAssertionStatus() ? true : $assertionsDisabled);
    public static final String SUMMARY_REPO = "SUMMARY_REPO";
    private static final String TAG = "SavedSummaryDao";

    public static void put(String url, String html, Context context) {
        if (!$assertionsDisabled && url == null) {
            throw new AssertionError();
        } else if ($assertionsDisabled || html != null) {
            if (SDCardRepo.isAvailable()) {
                try {
                    SDCardRepo.put(url, html);
                    return;
                } catch (IOException e) {
                    Log.e(TAG, "SavedSummaryDao#put:26", e);
                    SDCardRepo.repo = null;
                }
            }
            save(url, html, context);
        } else {
            throw new AssertionError();
        }
    }

    private static void save(String url, String html, Context context) {
        try {
            getPrefs(context).edit().putString(url, html).commit();
        } catch (Exception e) {
            Exception e2 = e;
            Log.e(TAG, "Failed to save html for url:" + url, e2);
            Log.e(TAG, "SavedSummaryDao: Failed to save html for url:" + url, e2);
        }
    }

    public static String getSummary(Context context, String url) {
        if (SDCardRepo.isAvailable()) {
            try {
                return SDCardRepo.get(url);
            } catch (IOException e) {
                Log.e(TAG, "SavedSummaryDao#getSummary:55", e);
            }
        }
        return getAll(context).get(url);
    }

    public static void remove(String key, Context context) {
        if (SDCardRepo.isAvailable()) {
            try {
                SDCardRepo.remove(key);
                return;
            } catch (IOException e) {
                Log.e(TAG, "SavedSummaryDao#remove:66", e);
            }
        }
        getPrefs(context).edit().remove(key).commit();
    }

    public static void clear(Context context) {
        if (SDCardRepo.isAvailable()) {
            try {
                SDCardRepo.clear();
                return;
            } catch (IOException e) {
                Log.e(TAG, "SavedSummaryDao#clear:78", e);
            }
        }
        getPrefs(context).edit().clear().commit();
    }

    private static SharedPreferences getPrefs(Context context) {
        return context.getSharedPreferences(SUMMARY_REPO, 0);
    }

    private static Map<String, String> getAll(Context context) {
        return getPrefs(context).getAll();
    }
}
