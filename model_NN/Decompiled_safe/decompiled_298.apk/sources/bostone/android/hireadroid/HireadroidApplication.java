package bostone.android.hireadroid;

import android.app.Application;
import android.util.Log;
import bostone.android.hireadroid.utils.HistoryManager;
import org.acra.ACRA;
import org.acra.annotation.ReportsCrashes;

@ReportsCrashes(formKey = "dDdId3ByU1BiUE9sZTB4NlBhWWdDZXc6MQ")
public class HireadroidApplication extends Application {
    public void onCreate() {
        ACRA.init(this);
        super.onCreate();
        try {
            HistoryManager historyManager = new HistoryManager(this);
            historyManager.refreshEngineList();
            historyManager.start();
        } catch (Throwable th) {
            Log.e("HireadroidApplication", "Failed to run HistoryManager", th);
        }
    }
}
