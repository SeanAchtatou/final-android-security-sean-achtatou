package bostone.android.hireadroid.dao;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

public class SearchItemsDbHelper extends SQLiteOpenHelper {
    public static final String DB_NAME = "hireadroid.db";
    private static final int DB_VERSION = 2;
    public static final String ITEMS_TBL = "items";

    public interface ItemColumns extends BaseColumns {
        public static final String COMPANY = "company";
        public static final String DATE = "lastSeeingDate";
        public static final String ENG = "engine";
        public static final String FAV = "favorite";
        public static final String JOB_ID = "jobId";
        public static final String LOC_CITY = "city";
        public static final String LOC_COUNTRY = "country";
        public static final String LOC_LATITUDE = "latitude";
        public static final String LOC_LONGTITUDE = "longtitude";
        public static final String LOC_NAME = "name";
        public static final String LOC_POSTAL = "postal";
        public static final String LOC_REGION = "region";
        public static final String LOC_ST = "st";
        public static final String NOTE = "note";
        public static final String POST_DATE = "postedDate";
        public static final String PREFERRED = "preferred";
        public static final String READ_TS = "tsRead";
        public static final String SHORT_URL = "shortUrl";
        public static final String SNIPPET = "snippet";
        public static final String SOURCE = "listingSource";
        public static final String TITLE = "jobTitle";
        public static final String TYPE = "listingType";
        public static final String URL = "listingUrl";
        public static final String VIEWED = "viewed";
    }

    public SearchItemsDbHelper(Context context) {
        super(context, DB_NAME, (SQLiteDatabase.CursorFactory) null, 2);
    }

    public void onCreate(SQLiteDatabase db) {
        createItemsTable(db);
    }

    private void createItemsTable(SQLiteDatabase db) {
        Cursor c = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table' AND name=?", new String[]{ITEMS_TBL});
        try {
            if (c.getCount() == 0) {
                db.execSQL("CREATE TABLE items (_id INTEGER PRIMARY KEY AUTOINCREMENT, jobId TEXT NOT NULL UNIQUE, jobTitle TEXT, company TEXT, listingUrl TEXT, shortUrl TEXT, listingSource TEXT, listingType TEXT, lastSeeingDate TEXT, postedDate TEXT, snippet TEXT, engine TEXT, preferred INT, favorite TEXT, name TEXT, city TEXT, st TEXT, postal TEXT, country TEXT, region TEXT, longtitude LONG, latitude LONG, note TEXT, viewed INT, tsRead TIMESTAMP);");
            }
        } finally {
            c.close();
        }
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS items");
        onCreate(db);
    }
}
