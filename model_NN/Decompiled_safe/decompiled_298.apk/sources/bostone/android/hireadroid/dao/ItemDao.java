package bostone.android.hireadroid.dao;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import bostone.android.hireadroid.dao.SearchItemsDbHelper;
import bostone.android.hireadroid.model.SearchItem;
import bostone.android.hireadroid.provider.SearchItemsProvider;
import java.util.Date;

public class ItemDao {
    public static long toggleFavorite(ContentResolver resolver, SearchItem item) {
        return (long) resolver.update(SearchItemsProvider.CONTENT_URI, item.toContentValues(), null, null);
    }

    public static void update(ContentResolver resolver, String jobId, ContentValues values) {
        resolver.update(SearchItemsProvider.CONTENT_URI, values, "jobId=?", new String[]{jobId});
    }

    public static void delete(ContentResolver resolver, String where, String[] selectionArgs) {
        resolver.delete(SearchItemsProvider.CONTENT_URI, where, selectionArgs);
    }

    public static void markRead(ContentResolver resolver, SearchItem item) {
        ContentValues values = item.toContentValues();
        long t = new Date().getTime();
        values.put(SearchItemsDbHelper.ItemColumns.READ_TS, Long.valueOf(t));
        item.tsRead = t;
        resolver.update(SearchItemsProvider.CONTENT_URI, values, null, null);
    }

    public static Cursor query(ContentResolver resolver, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        return resolver.query(SearchItemsProvider.CONTENT_URI, projection, selection, selectionArgs, sortOrder);
    }
}
