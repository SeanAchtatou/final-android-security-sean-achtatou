package bostone.android.hireadroid;

public class HireadroidException extends RuntimeException {
    private static final long serialVersionUID = -3678499147337273540L;

    public HireadroidException() {
    }

    public HireadroidException(String message) {
        super(message);
    }

    public HireadroidException(Throwable cause) {
        super(cause);
    }

    public HireadroidException(String message, Throwable cause) {
        super(message, cause);
    }
}
