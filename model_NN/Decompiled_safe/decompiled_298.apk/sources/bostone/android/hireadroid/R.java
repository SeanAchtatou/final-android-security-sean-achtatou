package bostone.android.hireadroid;

public final class R {

    public static final class anim {
        public static final int slide_in_left = 2130968576;
        public static final int slide_in_right = 2130968577;
        public static final int slide_out_left = 2130968578;
        public static final int slide_out_right = 2130968579;
    }

    public static final class array {
        public static final int searchRangeLabels = 2131099648;
        public static final int searchRangeValues = 2131099649;
        public static final int viewed = 2131099650;
        public static final int viewed_values = 2131099651;
    }

    public static final class attr {
        public static final int backgroundColor = 2130771968;
        public static final int keywords = 2130771971;
        public static final int primaryTextColor = 2130771969;
        public static final int refreshInterval = 2130771972;
        public static final int secondaryTextColor = 2130771970;
    }

    public static final class color {
        public static final int black = 2131165188;
        public static final int blue = 2131165192;
        public static final int dark_blue = 2131165193;
        public static final int dark_green = 2131165196;
        public static final int darkest_green = 2131165197;
        public static final int darkest_green_transparent = 2131165198;
        public static final int green = 2131165195;
        public static final int grey = 2131165190;
        public static final int item_body = 2131165185;
        public static final int item_highlight = 2131165187;
        public static final int item_txt = 2131165186;
        public static final int light_green = 2131165199;
        public static final int lightest_green = 2131165200;
        public static final int list_disabled = 2131165203;
        public static final int list_focus = 2131165201;
        public static final int list_pressed = 2131165202;
        public static final int red = 2131165194;
        public static final int tabs_header = 2131165184;
        public static final int transparent = 2131165204;
        public static final int white = 2131165189;
        public static final int yellow = 2131165191;
    }

    public static final class drawable {
        public static final int arrow_left = 2130837504;
        public static final int arrow_right = 2130837505;
        public static final int beyond_logo_small = 2130837506;
        public static final int btt_bgr = 2130837507;
        public static final int btt_bgr_focused = 2130837508;
        public static final int btt_bgr_focused_pressed = 2130837509;
        public static final int btt_bgr_normal = 2130837510;
        public static final int btt_bgr_pressed = 2130837511;
        public static final int careerbuilder_logo_small = 2130837512;
        public static final int fav_item_bgr = 2130837513;
        public static final int flag_at = 2130837514;
        public static final int flag_au = 2130837515;
        public static final int flag_be = 2130837516;
        public static final int flag_br = 2130837517;
        public static final int flag_ca = 2130837518;
        public static final int flag_ch = 2130837519;
        public static final int flag_cn = 2130837520;
        public static final int flag_de = 2130837521;
        public static final int flag_es = 2130837522;
        public static final int flag_fr = 2130837523;
        public static final int flag_ie = 2130837524;
        public static final int flag_in = 2130837525;
        public static final int flag_it = 2130837526;
        public static final int flag_jp = 2130837527;
        public static final int flag_mx = 2130837528;
        public static final int flag_nl = 2130837529;
        public static final int flag_uk = 2130837530;
        public static final int flag_us = 2130837531;
        public static final int ic_btt_favorite = 2130837532;
        public static final int ic_btt_favorite_white = 2130837533;
        public static final int ic_btt_help = 2130837534;
        public static final int ic_btt_load = 2130837535;
        public static final int ic_btt_pinmap = 2130837536;
        public static final int ic_btt_reload = 2130837537;
        public static final int ic_btt_reload_white = 2130837538;
        public static final int ic_btt_save = 2130837539;
        public static final int ic_btt_search = 2130837540;
        public static final int ic_btt_share_white = 2130837541;
        public static final int ic_btt_target = 2130837542;
        public static final int ic_btt_tools = 2130837543;
        public static final int ic_btt_undo = 2130837544;
        public static final int ic_btt_view_white = 2130837545;
        public static final int ic_hireadroid = 2130837546;
        public static final int ic_launcher = 2130837547;
        public static final int ic_menu_browser = 2130837548;
        public static final int ic_menu_help = 2130837549;
        public static final int ic_menu_info = 2130837550;
        public static final int ic_menu_mail = 2130837551;
        public static final int ic_menu_post = 2130837552;
        public static final int ic_menu_save = 2130837553;
        public static final int ic_menu_settings = 2130837554;
        public static final int ic_menu_share = 2130837555;
        public static final int ic_menu_sort = 2130837556;
        public static final int ic_menu_trash = 2130837557;
        public static final int indeedcom_onblack_small = 2130837558;
        public static final int item_details_bgr_favorite = 2130837559;
        public static final int item_details_bgr_normal = 2130837560;
        public static final int linkedin_logo = 2130837561;
        public static final int linkup_logo_small = 2130837562;
        public static final int list_background_preferred = 2130837563;
        public static final int list_selector_background_focus = 2130837564;
        public static final int n_toolbar_bgr = 2130837565;
        public static final int newjob = 2130837566;
        public static final int rate_star_small_on = 2130837567;
        public static final int recent_bgr = 2130837568;
        public static final int simplyhired_small = 2130837569;
        public static final int tab_bgr_focused = 2130837570;
        public static final int tab_bgr_focused_pressed = 2130837571;
        public static final int tab_bgr_normal = 2130837572;
        public static final int tab_bgr_pressed = 2130837573;
        public static final int tab_bgr_selected = 2130837574;
        public static final int tab_normal = 2130837575;
        public static final int widget_bg = 2130837576;
    }

    public static final class id {
        public static final int LinearLayout01 = 2131361812;
        public static final int LinearLayout05 = 2131361872;
        public static final int LinearLayout07 = 2131361870;
        public static final int LinearLayout08 = 2131361865;
        public static final int TableRow01 = 2131361801;
        public static final int TableRow02 = 2131361794;
        public static final int TableRow03 = 2131361797;
        public static final int TableRow04 = 2131361805;
        public static final int TableRow05 = 2131361863;
        public static final int TableRow06 = 2131361860;
        public static final int TextView01 = 2131361802;
        public static final int TextView02 = 2131361795;
        public static final int TextView03 = 2131361798;
        public static final int TextView04 = 2131361806;
        public static final int TextView05 = 2131361800;
        public static final int ad = 2131361900;
        public static final int arrowLeft = 2131361813;
        public static final int arrowRight = 2131361823;
        public static final int beyondEmail = 2131361796;
        public static final int beyondName = 2131361803;
        public static final int beyondPhone = 2131361804;
        public static final int beyondPwd = 2131361799;
        public static final int beyondZip = 2131361807;
        public static final int beyond_root = 2131361792;
        public static final int beyond_table = 2131361793;
        public static final int browser = 2131361899;
        public static final int bttAND = 2131361876;
        public static final int bttCOMP = 2131361879;
        public static final int bttLoad = 2131361897;
        public static final int bttNOT = 2131361878;
        public static final int bttONET = 2131361881;
        public static final int bttOR = 2131361877;
        public static final int bttPalette = 2131361895;
        public static final int bttReset = 2131361898;
        public static final int bttSave = 2131361896;
        public static final int bttSearch = 2131361874;
        public static final int bttShare = 2131361837;
        public static final int bttTITLE = 2131361880;
        public static final int bttView = 2131361839;
        public static final int chkFav = 2131361835;
        public static final int cn = 2131361843;
        public static final int controls = 2131361832;
        public static final int countryList = 2131361883;
        public static final int countryName = 2131361811;
        public static final int dp = 2131361842;
        public static final int e = 2131361833;
        public static final int engineLogoSmall = 2131361866;
        public static final int engines = 2131361855;
        public static final int favComment = 2131361848;
        public static final int favsEmpty = 2131361829;
        public static final int favsList = 2131361828;
        public static final int flagImg = 2131361884;
        public static final int footerView = 2131361893;
        public static final int infoBtn = 2131361810;
        public static final int infoCountry = 2131361861;
        public static final int infoCriteria = 2131361859;
        public static final int infoDlgTbl = 2131361856;
        public static final int infoLocation = 2131361862;
        public static final int infoProgress = 2131361808;
        public static final int infoRadius = 2131361864;
        public static final int infoTotal = 2131361857;
        public static final int infoTxt = 2131361809;
        public static final int infoView = 2131361831;
        public static final int infoViewable = 2131361858;
        public static final int info_menu = 2131361927;
        public static final int itemBody = 2131361845;
        public static final int jobSrc = 2131361846;
        public static final int jt = 2131361840;
        public static final int loadDlgCancel = 2131361853;
        public static final int loadDlgDelete = 2131361851;
        public static final int loadDlgError = 2131361849;
        public static final int loadDlgLoad = 2131361852;
        public static final int loc = 2131361844;
        public static final int locDetect = 2131361886;
        public static final int location = 2131361885;
        public static final int locationControls = 2131361882;
        public static final int logo = 2131361903;
        public static final int logoImg = 2131361904;
        public static final int open_browser_menu = 2131361930;
        public static final int paletteView = 2131361875;
        public static final int postAJob = 2131361922;
        public static final int progress = 2131361901;
        public static final int radius = 2131361888;
        public static final int radiusField = 2131361887;
        public static final int recentList = 2131361892;
        public static final int recentTxt = 2131361891;
        public static final int recentView = 2131361890;
        public static final int resultsFlipper = 2131361824;
        public static final int resultsTabsRow = 2131361816;
        public static final int row = 2131361834;
        public static final int save_menu = 2131361928;
        public static final int savedSearchesList = 2131361850;
        public static final int search = 2131361873;
        public static final int searchFeedback = 2131361924;
        public static final int searchForm = 2131361871;
        public static final int searchHelp = 2131361923;
        public static final int searchRadiusVal = 2131361889;
        public static final int send_menu = 2131361929;
        public static final int settings = 2131361925;
        public static final int sort_by_menu = 2131361926;
        public static final int src = 2131361847;
        public static final int stab0 = 2131361817;
        public static final int stab1 = 2131361818;
        public static final int stab2 = 2131361819;
        public static final int stab3 = 2131361820;
        public static final int stab4 = 2131361821;
        public static final int stab5 = 2131361822;
        public static final int star = 2131361841;
        public static final int summary = 2131361830;
        public static final int switcher = 2131361826;
        public static final int tab = 2131361868;
        public static final int tabBtt = 2131361867;
        public static final int tabResults = 2131361869;
        public static final int tableRow1 = 2131361894;
        public static final int tabsButtonView = 2131361814;
        public static final int tabsContainer = 2131361815;
        public static final int title = 2131361905;
        public static final int titleBtn1 = 2131361906;
        public static final int titleBtn2 = 2131361907;
        public static final int titlebar = 2131361825;
        public static final int toolbar = 2131361854;
        public static final int updatesTitleBar = 2131361902;
        public static final int view0 = 2131361827;
        public static final int view1 = 2131361836;
        public static final int view2 = 2131361838;
        public static final int widgetBody = 2131361911;
        public static final int widgetContent = 2131361909;
        public static final int widgetHead = 2131361910;
        public static final int widgetLoc = 2131361913;
        public static final int widgetPopUpAddBtn = 2131361919;
        public static final int widgetPopUpCancelBtn = 2131361920;
        public static final int widgetPopUpHelp = 2131361921;
        public static final int widgetPopUpInfo = 2131361917;
        public static final int widgetPopUpList = 2131361918;
        public static final int widgetProg = 2131361916;
        public static final int widgetProgress = 2131361915;
        public static final int widgetSrc = 2131361912;
        public static final int widgetUpdated = 2131361914;
        public static final int widgetlayout = 2131361908;
    }

    public static final class layout {
        public static final int ad_banner = 2130903040;
        public static final int beyond_login_dlg = 2130903041;
        public static final int beyond_register_dlg = 2130903042;
        public static final int common_header = 2130903043;
        public static final int country_row = 2130903044;
        public static final int engines = 2130903045;
        public static final int favorites = 2130903046;
        public static final int info_dlg = 2130903047;
        public static final int item_controls = 2130903048;
        public static final int job_search_item = 2130903049;
        public static final int load_saved = 2130903050;
        public static final int recent_search_item = 2130903051;
        public static final int results = 2130903052;
        public static final int results_info = 2130903053;
        public static final int results_tab = 2130903054;
        public static final int resuts_tab_body = 2130903055;
        public static final int search = 2130903056;
        public static final int search_form_panel = 2130903057;
        public static final int summary = 2130903058;
        public static final int title_bar = 2130903059;
        public static final int widget_layout = 2130903060;
        public static final int widget_popup = 2130903061;
    }

    public static final class menu {
        public static final int search_opts_menu = 2131296256;
        public static final int search_results_menu = 2131296257;
        public static final int search_summary_menu = 2131296258;
    }

    public static final class string {
        public static final int about_hint = 2131230739;
        public static final int about_title = 2131230738;
        public static final int app_name = 2131230726;
        public static final int end_of_list = 2131230744;
        public static final int err_search_criteria = 2131230722;
        public static final int failedGPS = 2131230720;
        public static final int failed_widget_updates = 2131230750;
        public static final int help_hint = 2131230742;
        public static final int help_title = 2131230741;
        public static final int itemUrl = 2131230730;
        public static final int linkedin_login_msg = 2131230748;
        public static final int load_jobs_dlg_title = 2131230736;
        public static final int load_long_click_hint = 2131230740;
        public static final int loading_wait = 2131230721;
        public static final int location_hint = 2131230724;
        public static final int location_wait_hint = 2131230725;
        public static final int no_saved_searches = 2131230731;
        public static final int out_of_mem_error = 2131230747;
        public static final int powered_by = 2131230737;
        public static final int quick_search_hint = 2131230735;
        public static final int quick_search_title = 2131230734;
        public static final int save_search_confirm = 2131230743;
        public static final int sd_warning = 2131230746;
        public static final int sd_warning_title = 2131230745;
        public static final int search_help_title = 2131230732;
        public static final int search_hint = 2131230733;
        public static final int search_results = 2131230727;
        public static final int search_title = 2131230728;
        public static final int sort_help_title = 2131230723;
        public static final int summary_title = 2131230729;
        public static final int warn_favs_empty = 2131230749;
        public static final int widget_empty = 2131230752;
        public static final int widget_name = 2131230751;
        public static final int widget_popup_help = 2131230757;
        public static final int widget_popup_help_title = 2131230756;
        public static final int widget_popup_jobs = 2131230755;
        public static final int widget_popup_nojob = 2131230754;
        public static final int widget_popup_title = 2131230753;
    }

    public static final class styleable {
        public static final int[] com_admob_android_ads_AdView = {R.attr.backgroundColor, R.attr.primaryTextColor, R.attr.secondaryTextColor, R.attr.keywords, R.attr.refreshInterval};
        public static final int com_admob_android_ads_AdView_backgroundColor = 0;
        public static final int com_admob_android_ads_AdView_keywords = 3;
        public static final int com_admob_android_ads_AdView_primaryTextColor = 1;
        public static final int com_admob_android_ads_AdView_refreshInterval = 4;
        public static final int com_admob_android_ads_AdView_secondaryTextColor = 2;
    }

    public static final class xml {
        public static final int preferences = 2131034112;
        public static final int searchable = 2131034113;
        public static final int widget = 2131034114;
    }
}
