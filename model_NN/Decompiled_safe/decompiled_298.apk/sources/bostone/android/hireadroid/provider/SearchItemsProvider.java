package bostone.android.hireadroid.provider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.text.TextUtils;
import bostone.android.hireadroid.dao.SearchItemsDbHelper;

public class SearchItemsProvider extends ContentProvider {
    static final /* synthetic */ boolean $assertionsDisabled = (!SearchItemsProvider.class.desiredAssertionStatus() ? true : $assertionsDisabled);
    public static final String AUTHORITY = "bostone.android.hireadroid.provider.searchitemsprovider";
    public static final Uri CONTENT_URI = Uri.parse("content://bostone.android.hireadroid.provider.searchitemsprovider");
    public static final String MIME_DIR = "vnd.android.cursor.dir/vnd.hireadroid.search";
    public static final String MIME_ITEM = "vnd.android.cursor.item/vnd.hireadroid.search";
    SQLiteDatabase db;

    public boolean onCreate() {
        this.db = new SearchItemsDbHelper(getContext()).getWritableDatabase();
        if (this.db != null) {
            return true;
        }
        return $assertionsDisabled;
    }

    public Uri insert(Uri uri, ContentValues values) {
        if (!$assertionsDisabled && values == null) {
            throw new AssertionError();
        } else if ($assertionsDisabled || uri != null) {
            long id = this.db.insert(SearchItemsDbHelper.ITEMS_TBL, SearchItemsDbHelper.ItemColumns.SHORT_URL, values);
            if (id <= 0) {
                return null;
            }
            Uri u = ContentUris.withAppendedId(CONTENT_URI, id);
            getContext().getContentResolver().notifyChange(u, null);
            return u;
        } else {
            throw new AssertionError();
        }
    }

    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        if (!$assertionsDisabled && values == null) {
            throw new AssertionError();
        } else if ($assertionsDisabled || uri != null) {
            long id = ContentUris.parseId(uri);
            if (id != -1) {
                return this.db.update(SearchItemsDbHelper.ITEMS_TBL, values, "_id=?", new String[]{Long.toString(id)});
            } else if (values.containsKey(SearchItemsDbHelper.ItemColumns.JOB_ID)) {
                return (int) this.db.replace(SearchItemsDbHelper.ITEMS_TBL, SearchItemsDbHelper.ItemColumns.SHORT_URL, values);
            } else {
                return this.db.update(SearchItemsDbHelper.ITEMS_TBL, values, selection, selectionArgs);
            }
        } else {
            throw new AssertionError();
        }
    }

    public int delete(Uri uri, String selection, String[] selectionArgs) {
        if ($assertionsDisabled || uri != null) {
            long id = ContentUris.parseId(uri);
            if (id == -1) {
                return this.db.delete(SearchItemsDbHelper.ITEMS_TBL, selection, selectionArgs);
            }
            return this.db.delete(SearchItemsDbHelper.ITEMS_TBL, "_id=?", new String[]{Long.toString(id)});
        }
        throw new AssertionError();
    }

    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        if ($assertionsDisabled || uri != null) {
            long id = ContentUris.parseId(uri);
            if (id == -1) {
                return this.db.query(SearchItemsDbHelper.ITEMS_TBL, projection, selection, selectionArgs, null, null, sortOrder);
            }
            return this.db.query(SearchItemsDbHelper.ITEMS_TBL, projection, "_id=?", new String[]{Long.toString(id)}, null, null, sortOrder);
        }
        throw new AssertionError();
    }

    public String getType(Uri uri) {
        String segment;
        if (uri == null || !AUTHORITY.equals(uri.getAuthority()) || (segment = uri.getLastPathSegment()) == null) {
            return null;
        }
        if (TextUtils.isDigitsOnly(segment)) {
            return MIME_ITEM;
        }
        return MIME_DIR;
    }
}
