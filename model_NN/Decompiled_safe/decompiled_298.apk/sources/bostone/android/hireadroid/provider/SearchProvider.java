package bostone.android.hireadroid.provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.util.Log;
import bostone.android.hireadroid.dao.SearchItemsDbHelper;
import bostone.android.hireadroid.engine.AbstractEngine;
import bostone.android.hireadroid.model.SearchItem;
import bostone.android.hireadroid.net.RequestHandler;
import bostone.android.hireadroid.sax.AbstractHandler;
import bostone.android.hireadroid.utils.SavedSearchRepo;
import bostone.android.hireadroid.utils.Utils;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.http.client.ClientProtocolException;
import org.xml.sax.SAXException;

public class SearchProvider extends ContentProvider {
    public static final String APP_VERSION = "APP_VERSION";
    public static final String DEFAULT_APP_VERSION = "1.5.0";
    public static final String LATEST_UPDATE = "LATEST_UPDATE";
    public static final String SAVED_SEARCHES = "SAVED_SEARCHES";
    public static final String[] SAVED_SEARCH_COLS = {SearchItemsDbHelper.ItemColumns.LOC_NAME, SearchItem.LISTING_URL};
    private static final String TAG = "SearchProvider";

    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    public String getType(Uri uri) {
        return "hireadroid";
    }

    public Uri insert(Uri uri, ContentValues values) {
        return null;
    }

    public boolean onCreate() {
        return true;
    }

    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        if (SAVED_SEARCHES.equals(selection)) {
            return getSavedSearches();
        }
        if (LATEST_UPDATE.equals(selection)) {
            return getLatestUpdates(AbstractEngine.pickSupportingURLs(selectionArgs[0]), Integer.parseInt(selectionArgs[1]), Integer.parseInt(selectionArgs[2]));
        }
        if (APP_VERSION.equals(selection)) {
            return getAppVersion();
        }
        return null;
    }

    private MatrixCursor getAppVersion() {
        MatrixCursor cursor = new MatrixCursor(new String[]{APP_VERSION});
        String version = DEFAULT_APP_VERSION;
        try {
            version = getContext().getPackageManager().getPackageInfo("bostone.android.hireadroid", 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, "Failed to obtain version name", e);
        }
        cursor.addRow(new Object[]{version});
        return cursor;
    }

    private MatrixCursor getLatestUpdates(String[] urls, int step, int widgetId) {
        MatrixCursor cursor = new MatrixCursor(new String[]{"TYPE", SearchItem.JOB_TITLE, SearchItem.SNIPPET, SearchItem.LISTING_SOURCE, SearchItem.LOCATION, SearchItem.DATE_POSTED, "LINK"});
        MatrixCursor errCursor = new MatrixCursor(new String[]{"TYPE", "CAUSE", "MESSAGE"});
        AbstractHandler handler = null;
        for (String url : urls) {
            handler = addSearchToCursor(step, widgetId, cursor, errCursor, handler, url);
        }
        return cursor;
    }

    /* access modifiers changed from: protected */
    public AbstractHandler addSearchToCursor(int step, int widgetId, MatrixCursor cursor, MatrixCursor errCursor, AbstractHandler handler, String url) {
        String[] error = null;
        try {
            handler = AbstractHandler.instanceOf(url);
            if (noResults(url, handler) && step < 2) {
                errCursor.addRow(new Object[]{0, "Search Error", "Your query produced no items"});
            }
        } catch (ParserConfigurationException e) {
            ParserConfigurationException e2 = e;
            Log.e(TAG, "Parser configuration error", e2);
            error = new String[]{"Parser configuration error", e2.getMessage()};
        } catch (SAXException e3) {
            SAXException e4 = e3;
            Log.e(TAG, "Parser error", e4);
            error = new String[]{"Content process error", e4.getMessage()};
        } catch (IOException e5) {
            IOException e6 = e5;
            Log.e(TAG, "Network error", e6);
            error = new String[]{"Network error", e6.getMessage()};
        } catch (Exception e7) {
            Exception e8 = e7;
            Log.e(TAG, "System error", e8);
            error = new String[]{"System error", e8.getMessage()};
        }
        if (error == null) {
            processUpdate(handler, step, widgetId, cursor);
        } else if (step < 2) {
            errCursor.addRow(new Object[]{0, error[0], error[1]});
        }
        return handler;
    }

    private boolean noResults(String url, AbstractHandler handler) throws ClientProtocolException, IOException, ParserConfigurationException, SAXException {
        return Utils.isEmpty(new RequestHandler(getContext()).service(url, handler)) || handler == null || handler.result.items.size() < 1;
    }

    /* Debug info: failed to restart local var, previous not found, register: 6 */
    private void processUpdate(AbstractHandler handler, int step, int widgetId, MatrixCursor cursor) {
        if (handler != null && handler.result.items.size() >= 1) {
            List<SearchItem> results = handler.result.items;
            SearchItem result = (results == null || results.isEmpty()) ? null : results.get(0);
            if (result != null) {
                cursor.addRow(new Object[]{1, result.jobTitle, result.snippet, result.listingSource, result.location.toString(), Long.valueOf(result.postedDate), result.listingUrl});
            }
        }
    }

    private MatrixCursor getSavedSearches() {
        MatrixCursor cursor = new MatrixCursor(SAVED_SEARCH_COLS);
        for (Map.Entry<String, String> entry : SavedSearchRepo.getSavedSearches(getContext()).entrySet()) {
            cursor.addRow(new String[]{(String) entry.getKey(), (String) entry.getValue()});
        }
        return cursor;
    }

    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }
}
