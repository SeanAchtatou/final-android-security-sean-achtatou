package bostone.android.hireadroid.provider;

import android.content.SearchRecentSuggestionsProvider;
import android.net.Uri;

public class SearchHistorySuggestionsProvider extends SearchRecentSuggestionsProvider {
    public static final String AUTHORITY = "bostone.android.hireadroid.provider.SearchHistorySuggestionsProvider";
    public static final Uri CONTENT_URI = Uri.parse("content://bostone.android.hireadroid.provider.SearchHistorySuggestionsProvider/suggestions");
    public static final int MODE = 1;

    public SearchHistorySuggestionsProvider() {
        setupSuggestions(AUTHORITY, 1);
    }
}
