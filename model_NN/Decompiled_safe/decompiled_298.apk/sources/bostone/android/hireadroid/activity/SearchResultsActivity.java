package bostone.android.hireadroid.activity;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.provider.SearchRecentSuggestions;
import android.text.TextUtils;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.HorizontalScrollView;
import android.widget.TableRow;
import android.widget.Toast;
import bostone.android.hireadroid.R;
import bostone.android.hireadroid.engine.AbstractEngine;
import bostone.android.hireadroid.engine.EngineConstants;
import bostone.android.hireadroid.engine.LinkedInEngine;
import bostone.android.hireadroid.provider.SearchHistorySuggestionsProvider;
import bostone.android.hireadroid.ui.EngineFlipper;
import bostone.android.hireadroid.utils.FlingDetector;
import bostone.android.hireadroid.utils.Utils;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class SearchResultsActivity extends SummaryActivity implements EngineConstants {
    public static final int NO_WIDGET = -1;
    private static final int SEARCH_INFO_DLG = 0;
    private static final int SEARCH_SAVE_DLG = 1;
    private static final String TAG = "SearchResultsView";
    public int currentTab;
    /* access modifiers changed from: private */
    public List<AbstractEngine> engines;
    private EngineFlipper flipper;
    public View.OnTouchListener gestureListener;
    private String lastSearchUrl;
    private View leftArrow;
    public ProgressDialog loginProgress;
    private EngineClickReceiver receiver;
    private View rightArrow;
    private TableRow row;
    boolean scrollable;
    public MenuItem sortMenuItem;
    private HorizontalScrollView tabButtons;
    private View tabContainer;
    int widgetId;

    public void onCreate(Bundle bundle) {
        String url;
        super.onCreate(bundle);
        setContentView((int) R.layout.results);
        findViewById(R.id.titleBtn1).setVisibility(0);
        this.row = (TableRow) findViewById(R.id.resultsTabsRow);
        Intent intent = getIntent();
        String query = intent.getStringExtra("query");
        if (!TextUtils.isEmpty(query)) {
            query = query.trim();
            new SearchRecentSuggestions(this, SearchHistorySuggestionsProvider.AUTHORITY, 1).saveRecentQuery(query, null);
        }
        if ("android.intent.action.SEARCH".equals(intent.getAction()) && (url = Utils.getStringVal((String) EngineConstants.LAST_SEARCH, this)) != null && url.contains("&q=") && url.contains("&ip=")) {
            this.lastSearchUrl = String.valueOf(url.substring(0, url.indexOf("&q=") + 3)) + query + url.substring(url.indexOf("&ip="));
            Log.d(TAG, "Searching for: " + this.lastSearchUrl);
        }
        this.flipper = (EngineFlipper) findViewById(R.id.resultsFlipper);
        this.tabButtons = (HorizontalScrollView) findViewById(R.id.tabsButtonView);
        this.tabContainer = findViewById(R.id.tabsContainer);
        this.leftArrow = findViewById(R.id.arrowLeft);
        this.rightArrow = findViewById(R.id.arrowRight);
        this.gestureListener = new View.OnTouchListener() {
            final GestureDetector gestureDetector = new GestureDetector(new FlingDetector() {
                public boolean onLeftToRightFling(int x, int y) {
                    return SearchResultsActivity.this.onTheFling(x, y, false);
                }

                public boolean onRightToLeftFling(int x, int y) {
                    return SearchResultsActivity.this.onTheFling(x, y, true);
                }
            });

            public boolean onTouch(View v, MotionEvent event) {
                if (this.gestureDetector.onTouchEvent(event)) {
                    return true;
                }
                return false;
            }
        };
        initSummary();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        String url;
        super.onStart();
        try {
            Uri uri = getIntent().getData();
            if (uri == null) {
                url = this.lastSearchUrl;
            } else if (!uri.getScheme().equals("hireadroid") || !"login".equals(uri.getAuthority())) {
                url = uri.toString();
            } else {
                showLinkedInLoginProgress();
                this.engines = initEngines(Utils.getStringVal(EngineConstants.LAST_SEARCH, this));
                getLinkedInEngine().onLoginApproved(uri);
                return;
            }
            if (TextUtils.isEmpty(url)) {
                Toast.makeText(this, "Search criteria is not supplied", 1).show();
                finish();
                return;
            }
            this.widgetId = getIntent().getIntExtra("appWidgetId", -1);
            if (this.engines == null) {
                this.engines = initEngines(url);
            }
        } catch (Exception e) {
            Exception e2 = e;
            Log.e(TAG, "Failed to process search: " + e2.getMessage(), e2);
            Toast.makeText(this, "Failed to display final listing: " + e2.getMessage(), 1).show();
            finish();
        } catch (OutOfMemoryError e3) {
            System.gc();
            Toast.makeText(this, (int) R.string.out_of_mem_error, 1).show();
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 5 */
    public LinkedInEngine getLinkedInEngine() {
        if (this.engines == null || this.engines.isEmpty()) {
            return (LinkedInEngine) AbstractEngine.getInstance(LinkedInEngine.TYPE, this, 0, this.flipper, this.row);
        }
        for (AbstractEngine e : this.engines) {
            if (e instanceof LinkedInEngine) {
                return (LinkedInEngine) e;
            }
        }
        return null;
    }

    private void showLinkedInLoginProgress() {
        this.loginProgress = new ProgressDialog(this);
        this.loginProgress.setProgressStyle(0);
        this.loginProgress.setIndeterminate(true);
        this.loginProgress.setMessage("Finalizing LinkedIn login, please wait...");
        this.loginProgress.setCancelable(false);
        this.loginProgress.show();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        if (this.receiver == null) {
            this.receiver = new EngineClickReceiver();
        }
        registerReceiver(this.receiver, new IntentFilter(EngineConstants.CLICK_ACTION));
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        if (this.receiver != null) {
            unregisterReceiver(this.receiver);
        }
        super.onPause();
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Log.d(TAG, "onConfigurationChanged#post: " + computeIfScrollable());
        this.scrollable = !computeIfScrollable();
        if (this.scrollable) {
            toggleArrowsBasedOnTabId(this.currentTab);
        } else {
            toggleArrows(false, false);
        }
    }

    private boolean computeIfScrollable() {
        if (this.tabContainer == null || this.tabButtons == null) {
            return false;
        }
        return this.tabContainer.getWidth() > this.tabButtons.getWidth();
    }

    /* access modifiers changed from: private */
    public boolean onTheFling(int x, int y, boolean leftToRight) {
        if (leftToRight) {
            this.flipper.setInAnimation(AnimationUtils.loadAnimation(this, R.anim.slide_in_left));
            this.flipper.setOutAnimation(AnimationUtils.loadAnimation(this, R.anim.slide_out_right));
        } else {
            this.flipper.setInAnimation(AnimationUtils.loadAnimation(this, R.anim.slide_in_right));
            this.flipper.setOutAnimation(AnimationUtils.loadAnimation(this, R.anim.slide_out_left));
        }
        Iterator<AbstractEngine> it = this.engines.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            AbstractEngine eng = it.next();
            if (eng.tabSelected) {
                int id = eng.tabId;
                do {
                    id = calculateNext(id, leftToRight);
                } while (!this.engines.get(id).enabled);
                if (eng.tabId != id) {
                    this.engines.get(id).initActiveTab(this.gestureListener, false);
                    this.tabButtons.smoothScrollTo(id * 70, 0);
                    toggleArrowsBasedOnTabId(id);
                }
            }
        }
        return false;
    }

    public void toggleArrowsBasedOnTabId(int id) {
        if (!this.scrollable) {
            toggleArrows(false, false);
        } else if (id == 0) {
            toggleArrows(false, true);
        } else if (id == this.engines.size() - 1) {
            toggleArrows(true, false);
        } else {
            toggleArrows(true, true);
        }
    }

    private void toggleArrows(boolean isLeftVisible, boolean isRightVisible) {
        int i;
        int i2;
        View view = this.leftArrow;
        if (isLeftVisible) {
            i = 0;
        } else {
            i = 8;
        }
        view.setVisibility(i);
        View view2 = this.rightArrow;
        if (isRightVisible) {
            i2 = 0;
        } else {
            i2 = 8;
        }
        view2.setVisibility(i2);
    }

    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        Log.d(TAG, "onWindowFocusChanged: " + computeIfScrollable());
        this.scrollable = computeIfScrollable();
        if (this.scrollable) {
            toggleArrows(false, true);
        } else {
            toggleArrows(false, false);
        }
    }

    private int calculateNext(int id, boolean leftToRight) {
        int numTabs = this.engines.size();
        if (!leftToRight) {
            return id == 0 ? numTabs - 1 : id - 1;
        }
        if (id >= numTabs - 1) {
            return 0;
        }
        return id + 1;
    }

    private List<AbstractEngine> initEngines(String url) {
        int trueTabId;
        List<AbstractEngine> engs = new ArrayList<>();
        boolean defaultIsEnabled = false;
        int i = 0;
        String[] names = Utils.getEngineNames(this);
        int length = names.length;
        int i2 = 0;
        int trueTabId2 = 0;
        int tabId = 0;
        while (i2 < length) {
            int tabId2 = tabId + 1;
            AbstractEngine eng = AbstractEngine.getInstance(names[i2], this, tabId, this.flipper, this.row);
            if (!eng.init(url) || !eng.enabled) {
                trueTabId = trueTabId2;
            } else {
                trueTabId = trueTabId2 + 1;
                eng.initTabImage(trueTabId2, this.gestureListener);
                if (!defaultIsEnabled) {
                    eng.initActiveTab(this.gestureListener, false);
                    defaultIsEnabled = true;
                }
                engs.add(eng);
            }
            i++;
            i2++;
            trueTabId2 = trueTabId;
            tabId = tabId2;
        }
        return engs;
    }

    private AbstractEngine getActiveEngine() {
        for (AbstractEngine eng : this.engines) {
            if (eng.tabSelected) {
                this.currentTab = eng.tabId;
                return eng;
            }
        }
        AbstractEngine eng2 = this.engines.get(0);
        eng2.initActiveTab(this.gestureListener, false);
        return eng2;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        new MenuInflater(getApplication()).inflate(R.menu.search_results_menu, menu);
        boolean byRel = Utils.getPrefs(this).getBoolean(getSortKey(getActiveEngine()), false);
        this.sortMenuItem = menu.getItem(0);
        this.sortMenuItem.setTitle(byRel ? AbstractEngine.SORT_BY_DATE : AbstractEngine.SORT_BY_RELEVANCE);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        boolean z;
        boolean z2;
        AbstractEngine eng = getActiveEngine();
        if (eng == null) {
            Toast.makeText(this, "Failed to detect search engine", 1).show();
            return false;
        }
        SharedPreferences prefs = Utils.getPrefs(this);
        switch (item.getItemId()) {
            case R.id.sort_by_menu:
                boolean byRel = prefs.getBoolean(getSortKey(eng), false);
                item.setTitle(byRel ? AbstractEngine.SORT_BY_RELEVANCE : AbstractEngine.SORT_BY_DATE);
                SharedPreferences.Editor edit = prefs.edit();
                String sortKey = getSortKey(eng);
                if (byRel) {
                    z = false;
                } else {
                    z = true;
                }
                edit.putBoolean(sortKey, z).commit();
                if (byRel) {
                    z2 = false;
                } else {
                    z2 = true;
                }
                eng.requestUpdate(false, true, z2);
                break;
            case R.id.info_menu:
                if (!isFinishing() && eng != null) {
                    eng.buildSearchInfoDlg(0).show();
                    break;
                }
            case R.id.save_menu:
                if (!isFinishing() && eng != null) {
                    eng.buildSearchSaveDlg(1).show();
                    break;
                }
        }
        return super.onMenuItemSelected(featureId, item);
    }

    public String getSortKey(AbstractEngine activeEngine) {
        return EngineConstants.ORDER + activeEngine.name;
    }

    public class EngineClickReceiver extends BroadcastReceiver {
        public EngineClickReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            int id = intent.getIntExtra("ID", -1);
            Log.d(SearchResultsActivity.TAG, " origin: " + id);
            for (AbstractEngine eng : SearchResultsActivity.this.engines) {
                if (id != eng.tabId && eng.trigger.isEnabled()) {
                    eng.trigger.setBackgroundResource(R.drawable.tab_normal);
                    eng.tabSelected = false;
                }
            }
        }
    }

    public boolean trackRead() {
        return true;
    }
}
