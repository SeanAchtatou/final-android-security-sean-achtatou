package bostone.android.hireadroid.activity;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;
import bostone.android.hireadroid.R;
import com.flurry.android.FlurryAgent;
import java.util.HashMap;

public class MonitoredActivity extends Activity {
    public static final String CLIENT_ID = "ca-mb-app-pub-1319224076176423";
    public static final String KEY = "TFVQU6C9EETZUHCKKM7V";
    public static boolean PRODUCTION_MODE = true;
    private static final String TAG = "MonitoredActivity";
    protected boolean adSupported = true;
    public TextView title;

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        checkIfAdSupported();
        FlurryAgent.onStartSession(this, KEY);
        initToolbar();
        FlurryAgent.onPageView();
        event("Page load", "Type", getClass().getSimpleName());
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void initToolbar() {
        this.title = (TextView) findViewById(R.id.title);
        findViewById(R.id.logo).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!(MonitoredActivity.this instanceof SearchActivity)) {
                    MonitoredActivity.this.startActivity(new Intent(MonitoredActivity.this, SearchActivity.class));
                    MonitoredActivity.this.finish();
                }
            }
        });
        findViewById(R.id.titleBtn1).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MonitoredActivity.this.refresh();
            }
        });
        if (!(this instanceof FavoritesActivity)) {
            View btn = findViewById(R.id.titleBtn2);
            btn.setVisibility(0);
            btn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    MonitoredActivity.this.startActivity(new Intent(MonitoredActivity.this, FavoritesActivity.class));
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        if (PRODUCTION_MODE) {
            FlurryAgent.onEndSession(this);
        }
    }

    public static void event(String name, String key, String value) {
        if (PRODUCTION_MODE) {
            HashMap<String, String> map = new HashMap<>(1);
            map.put(key, value);
            FlurryAgent.onEvent(name, map);
        }
    }

    private void checkIfAdSupported() {
        this.adSupported = true;
    }

    /* access modifiers changed from: protected */
    public void refresh() {
        startActivity(getIntent());
        finish();
    }
}
