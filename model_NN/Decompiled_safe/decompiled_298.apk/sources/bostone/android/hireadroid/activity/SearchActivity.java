package bostone.android.hireadroid.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;
import bostone.android.hireadroid.R;
import bostone.android.hireadroid.engine.EngineConstants;
import bostone.android.hireadroid.model.Country;
import bostone.android.hireadroid.model.SearchItem;
import bostone.android.hireadroid.provider.SearchHistorySuggestionsProvider;
import bostone.android.hireadroid.provider.SearchItemsProvider;
import bostone.android.hireadroid.ui.CountryListAdapter;
import bostone.android.hireadroid.ui.FavoritesSupport;
import bostone.android.hireadroid.ui.ItemHolder;
import bostone.android.hireadroid.utils.SavedSearchRepo;
import bostone.android.hireadroid.utils.SavedSummaryDao;
import bostone.android.hireadroid.utils.Utils;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class SearchActivity extends SummaryActivity {
    private static final int ABOUT_DIG = 3;
    private static final int CONFIRM_SAVE_SEARCH_DLG = 2;
    private static final int FEEDBACK_DLG = 5;
    private static final int LOAD_JOBS_DLG = 1;
    private static final int ONET_CATEGORY_DLG = 4;
    public static final String PROVIDER_GPS = "gps";
    private static final int SEARCH_HELP_DLG = 0;
    public static final String SHOW_PALETTE = "SHOW_PALETTE";
    /* access modifiers changed from: private */
    public static final String TAG = SearchActivity.class.getSimpleName();
    /* access modifiers changed from: private */
    public Spinner countries;
    private CountryListAdapter countryAdapter;
    /* access modifiers changed from: private */
    public int countryId;
    private ImageView flagImg;
    /* access modifiers changed from: private */
    public AutoCompleteTextView keyword;
    private ListView list;
    /* access modifiers changed from: private */
    public EditText location;
    private View palette;
    private ImageButton paletteBtt;
    private SeekBar radius;
    /* access modifiers changed from: private */
    public View radiusField;
    /* access modifiers changed from: private */
    public ImageButton resetBtt;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.search);
        if (movedToSD()) {
            Utils.buildInfoDialog(this, (int) R.string.sd_warning_title, (int) R.string.sd_warning).show();
        }
        findViewById(R.id.titleBtn1).setVisibility(8);
        this.resetBtt = (ImageButton) findViewById(R.id.bttReset);
        this.resetBtt.setEnabled(false);
        final ImageButton saveBtt = (ImageButton) findViewById(R.id.bttSave);
        saveBtt.setEnabled(false);
        this.location = (EditText) findViewById(R.id.location);
        this.location.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                if (Utils.isNotEmpty(s.toString())) {
                    SearchActivity.this.resetBtt.setEnabled(true);
                    saveBtt.setEnabled(true);
                } else if (Utils.isEmpty(SearchActivity.this.keyword.getText().toString())) {
                    saveBtt.setEnabled(false);
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });
        this.keyword = (AutoCompleteTextView) findViewById(R.id.search);
        this.keyword.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                Log.d(SearchActivity.TAG, "Text changes: " + ((Object) s));
            }

            public void afterTextChanged(Editable s) {
                if (Utils.isNotEmpty(s.toString())) {
                    SearchActivity.this.resetBtt.setEnabled(true);
                    saveBtt.setEnabled(true);
                } else if (Utils.isEmpty(SearchActivity.this.location.getText().toString())) {
                    saveBtt.setEnabled(false);
                }
            }
        });
        this.keyword.setThreshold(1);
        this.radius = (SeekBar) findViewById(R.id.radius);
        this.radiusField = findViewById(R.id.radiusField);
        this.radiusField.setVisibility(8);
        this.location.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int arg1, int arg2, int arg3) {
            }

            public void beforeTextChanged(CharSequence s, int arg1, int arg2, int arg3) {
            }

            public void afterTextChanged(Editable text) {
                boolean notEmpty = Utils.isNotEmpty(text.toString());
                SearchActivity.this.radiusField.setVisibility(notEmpty ? 0 : 8);
                if (notEmpty) {
                    SearchActivity.this.resetBtt.setEnabled(true);
                }
            }
        });
        final TextView rad = (TextView) findViewById(R.id.searchRadiusVal);
        this.radius.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onStopTrackingTouch(SeekBar bar) {
            }

            public void onStartTrackingTouch(SeekBar bar) {
            }

            public void onProgressChanged(SeekBar bar, int progress, boolean fromUser) {
                rad.setText(Integer.toString(progress));
            }
        });
        this.palette = findViewById(R.id.paletteView);
        this.paletteBtt = (ImageButton) findViewById(R.id.bttPalette);
        SavedSummaryDao.clear(this);
        initOnClick(this.paletteBtt, "onPaletteFlipClick");
        initOnClick(findViewById(R.id.bttAND), "onAddKeywordClick");
        initOnClick(findViewById(R.id.bttOR), "onAddKeywordClick");
        initOnClick(findViewById(R.id.bttNOT), "onAddKeywordClick");
        initOnClick(findViewById(R.id.bttCOMP), "onAddKeywordClick");
        initOnClick(findViewById(R.id.bttTITLE), "onAddKeywordClick");
        initOnClick(findViewById(R.id.bttONET), "onCategoryClick");
        initOnClick(findViewById(R.id.locDetect), "onClickMyLocation");
        initOnClick(findViewById(R.id.bttLoad), "onClickLoad");
        initOnClick(findViewById(R.id.bttSave), "onClickSave");
        initOnClick(findViewById(R.id.bttReset), "onClickReset");
        initOnClick(findViewById(R.id.bttSearch), "onClickSearch");
        this.palette.setVisibility(Utils.getPrefs(this).getInt(SHOW_PALETTE, 8));
        this.flagImg = (ImageView) findViewById(R.id.flagImg);
        initCountryList(true);
        this.list = (ListView) findViewById(R.id.recentList);
        initSummary();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        final FavoritesSupport favSupport = new FavoritesSupport() {
            public void remove(SearchItem item) {
            }

            public boolean hasId(SearchItem item) {
                return item.favorite;
            }

            public void setNote(SearchItem item) {
            }

            public String getNote(SearchItem item) {
                return null;
            }

            public void add(SearchItem item) {
            }
        };
        CursorAdapter adapter = new CursorAdapter(this, managedQuery(SearchItemsProvider.CONTENT_URI, null, null, null, "lastSeeingDate DESC")) {
            public View newView(Context context, Cursor cursor, ViewGroup parent) {
                View view = SearchActivity.this.getLayoutInflater().inflate((int) R.layout.recent_search_item, (ViewGroup) null);
                ItemHolder holder = new ItemHolder(SearchActivity.this, view, favSupport, SearchActivity.this);
                holder.reset(SearchItem.fillFromCursor(cursor, holder.item));
                view.setTag(holder);
                SearchActivity.this.setItemBackground(view, holder);
                return view;
            }

            public void bindView(View view, Context context, Cursor cursor) {
                ItemHolder holder = (ItemHolder) view.getTag();
                if (holder != null) {
                    holder.reset(SearchItem.fillFromCursor(cursor, holder.item));
                }
                SearchActivity.this.setItemBackground(view, holder);
            }
        };
        this.list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                ItemHolder h = (ItemHolder) view.getTag();
                if (h != null) {
                    MonitoredActivity.event("View job", "Engine", h.item.engine);
                    h.summaryHandler.viewSummary(h.item);
                }
            }
        });
        this.list.setAdapter((ListAdapter) adapter);
    }

    private boolean movedToSD() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void setItemBackground(View view, ItemHolder holder) {
        if (holder.item.preferred) {
            view.setBackgroundResource(R.drawable.list_background_preferred);
        } else {
            view.setBackgroundResource(17301602);
        }
    }

    private void initOnClick(View button, final String name) {
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Class<SearchActivity> cls = SearchActivity.class;
                try {
                    cls.getMethod(name, View.class).invoke(SearchActivity.this, v);
                } catch (Exception e) {
                    Log.e(SearchActivity.TAG, "initOnClick", e);
                }
            }
        });
    }

    private void initCountryList(boolean firstTime) {
        this.countryId = getPreferences(0).getInt(EngineConstants.COUNTRY_ID, 0);
        if (firstTime) {
            this.countries = (Spinner) findViewById(R.id.countryList);
            this.countries.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                    if (SearchActivity.this.countryId != position) {
                        SearchActivity.this.setCountrySelectedId(position);
                        SearchActivity.this.resetLocation();
                    }
                }

                public void onNothingSelected(AdapterView<?> adapterView) {
                }
            });
        }
        try {
            if (this.countryAdapter == null) {
                this.countryAdapter = new CountryListAdapter(this);
            }
            this.countries.setAdapter((SpinnerAdapter) this.countryAdapter);
            setCountrySelectedId(this.countryId);
        } catch (Exception e) {
            Toast.makeText(this, "Error selecting country. Changing to default (US)", 1).show();
            setCountrySelectedId(Country.getCountryPositionByCode("US"));
            Log.w(TAG, "Failed to select country: " + this.countryId, e);
        }
    }

    public void onClickSearch(View view) {
        try {
            search(buildSearchUrl(getSearchParamsMap()));
            event("Action", "Search", "Direct");
        } catch (UnsupportedEncodingException e) {
            Toast.makeText(this, "Unsupported search format", 1).show();
            Log.e(TAG, "onClickSearch", e);
        } catch (Exception e2) {
            Exception e3 = e2;
            Toast.makeText(this, e3.getMessage(), 1).show();
            Log.e(TAG, "onClickSearch", e3);
        }
    }

    /* access modifiers changed from: private */
    public void search(String url) {
        Uri uri = Uri.parse(url);
        Utils.remove(EngineConstants.LAST_SEARCH, this);
        Utils.setString(EngineConstants.LAST_SEARCH, url, this);
        Intent i = new Intent("search", uri, this, SearchResultsActivity.class);
        Editable txt = this.keyword.getText();
        if (txt != null) {
            i.putExtra("query", txt.toString());
        }
        startActivity(i);
    }

    public void onClickReset(View view) {
        this.keyword.setText((CharSequence) null);
        this.location.setText((CharSequence) null);
        this.radiusField.setVisibility(8);
        this.resetBtt.setEnabled(false);
    }

    public void onClickLoad(View view) {
        if (!isFinishing()) {
            showDialog(1);
        }
    }

    public void onPaletteFlipClick(View view) {
        toggleSearchPalette(view);
    }

    public void onAddKeywordClick(View view) {
        String val = this.keyword.getText().toString();
        boolean shift = false;
        switch (view.getId()) {
            case R.id.bttAND:
                val = String.valueOf(val) + " AND ";
                break;
            case R.id.bttOR:
                val = String.valueOf(val) + " OR ";
                break;
            case R.id.bttNOT:
                val = String.valueOf(val) + " NOT ";
                break;
            case R.id.bttCOMP:
                val = String.valueOf(val) + " company:() ";
                shift = true;
                break;
            case R.id.bttTITLE:
                shift = true;
                val = String.valueOf(val) + " title:() ";
                break;
        }
        this.keyword.setText(val);
        this.keyword.requestFocus();
        if (shift) {
            this.keyword.setSelection(val.length() - 2);
        } else {
            this.keyword.setSelection(val.length());
        }
        ((InputMethodManager) getSystemService("input_method")).showSoftInput(this.keyword, 1);
    }

    public void onCategoryClick(View view) {
        if (!isFinishing()) {
            showDialog(4);
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 13 */
    /* access modifiers changed from: protected */
    public void onPrepareDialog(int id, Dialog d) {
        switch (id) {
            case 1:
                ListView lv = (ListView) d.findViewById(R.id.savedSearchesList);
                ArrayAdapter<String> a = (ArrayAdapter) lv.getAdapter();
                Map<String, String> map = SavedSearchRepo.getSavedSearches(this);
                if (map.size() > a.getCount()) {
                    for (int i = 0; i < a.getCount(); i++) {
                        String key = (String) a.getItem(i);
                        if (map.containsKey(key)) {
                            map.remove(key);
                        }
                    }
                    for (String key2 : map.keySet()) {
                        a.add(key2);
                    }
                }
                ((Button) d.findViewById(R.id.loadDlgDelete)).setEnabled(false);
                ((Button) d.findViewById(R.id.loadDlgLoad)).setEnabled(false);
                if (lv.getAdapter().isEmpty()) {
                    lv.setVisibility(8);
                    d.findViewById(R.id.loadDlgError).setVisibility(0);
                    return;
                }
                lv.setVisibility(0);
                d.findViewById(R.id.loadDlgError).setVisibility(8);
                lv.clearChoices();
                return;
            case 2:
                String kwd = this.keyword.getText().toString();
                if (Utils.isEmpty(kwd)) {
                    kwd = "*";
                }
                String loc = this.location.getText().toString();
                if (Utils.isNotEmpty(loc)) {
                    String kwd2 = String.valueOf(kwd) + " - " + loc;
                    kwd = String.valueOf(kwd2) + " [" + this.radius.getProgress() + " ml]";
                }
                EditText edit = (EditText) d.findViewById(2);
                edit.setText(String.valueOf(kwd) + " " + Country.getCountryIsoByPosition(this.countryId));
                ((View) edit.getParent()).setPadding(5, 5, 5, 5);
                return;
            default:
                return;
        }
    }

    public void onClickMyLocation(View view) {
        new MyLocationTask(this, null).execute(new Void[0]);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.countries.setSelection(this.countryId);
        Cursor c = managedQuery(SearchHistorySuggestionsProvider.CONTENT_URI, null, null, null, null);
        List<String> suggestions = Utils.fromCursor(c, 2);
        startManagingCursor(c);
        this.keyword.setAdapter(new ArrayAdapter(this, 17367043, 16908308, suggestions));
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 0:
                try {
                    return buildSearchHelpDialog();
                } catch (Exception e) {
                    Log.e(TAG, "onCreateDialog", e);
                    break;
                }
            case 1:
                return buildLoadJobsDialog();
            case 2:
                return buildSaveSearchDlg();
            case 3:
                return buildAboutDlg();
            case 5:
                return buildFeedbackDlg();
        }
        return super.onCreateDialog(id);
    }

    private Dialog buildFeedbackDlg() {
        final EditText input = new EditText(this);
        input.setLines(3);
        input.setGravity(48);
        input.setHint("Enter your description here. No device info is required");
        return new AlertDialog.Builder(this).setTitle("Send feedback").setView(input).setNegativeButton("Cancel", (DialogInterface.OnClickListener) null).setPositiveButton("Send", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (TextUtils.isEmpty(input.getText())) {
                    Toast.makeText(SearchActivity.this, "Cannot send. Feedback is empty", 1).show();
                } else {
                    SearchActivity.this.onFeedbackSubmit(input.getText());
                }
            }
        }).create();
    }

    /* access modifiers changed from: protected */
    public void onFeedbackSubmit(CharSequence text) {
        Intent emailIntent = new Intent("android.intent.action.SEND");
        emailIntent.setType("plain/text");
        emailIntent.putExtra("android.intent.extra.EMAIL", new String[]{"HireADroid support <support@hireadroid.com>"});
        emailIntent.putExtra("android.intent.extra.SUBJECT", "HireADroid feedback");
        emailIntent.putExtra("android.intent.extra.TEXT", ((Object) text) + "\n\nHelpful info about your device (Optional)" + getDeviceInfo());
        startActivity(Intent.createChooser(emailIntent, "HireADroid feedback..."));
    }

    private String getDeviceInfo() {
        StringBuilder sb = new StringBuilder("\n\n---\n");
        try {
            sb.append("Version: ").append(getPackageManager().getPackageInfo(getPackageName(), 0).versionName).append(10);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, "failed to get app version", e);
        }
        sb.append("Model: ").append(Build.MODEL).append(10);
        sb.append("Brand: ").append(Build.BRAND).append(10);
        sb.append("Device: ").append(Build.DEVICE).append(10);
        sb.append("Display: ").append(Build.DISPLAY).append(10);
        sb.append("Host: ").append(Build.HOST).append(10);
        sb.append("Release: ").append(Build.VERSION.RELEASE).append(10);
        sb.append("Board: ").append(Build.BOARD).append(10);
        sb.append("Product: ").append(Build.PRODUCT).append(10);
        return sb.toString();
    }

    private Dialog buildAboutDlg() {
        return Utils.buildInfoDialog(this, (int) R.string.about_title, (int) R.string.about_hint);
    }

    private Dialog buildSaveSearchDlg() {
        final EditText edit = new EditText(this);
        edit.setId(2);
        return new AlertDialog.Builder(this).setMessage((int) R.string.save_search_confirm).setPositiveButton("Save", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dlg, int which) {
                try {
                    SavedSearchRepo.put(edit.getText().toString(), SearchActivity.this.buildSearchUrl(SearchActivity.this.getSearchParamsMap()), SearchActivity.this);
                } catch (UnsupportedEncodingException e) {
                    Toast.makeText(SearchActivity.this, "Unsupported search format", 1).show();
                    Log.e(SearchActivity.TAG, "onClickSearch", e);
                } catch (Exception e2) {
                    Exception e3 = e2;
                    Toast.makeText(SearchActivity.this, e3.getMessage(), 1).show();
                    Log.e(SearchActivity.TAG, "onClickSearch", e3);
                }
                dlg.cancel();
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        }).setTitle("Save search").setView(edit).create();
    }

    /* access modifiers changed from: protected */
    public String buildSearchUrl(Map<String, String> searchParamsMap) {
        StringBuilder sb = new StringBuilder();
        sb.append("hireadroid:/search?");
        for (Map.Entry<String, String> entry : searchParamsMap.entrySet()) {
            sb.append((String) entry.getKey()).append('=').append((String) entry.getValue()).append('&');
        }
        return sb.toString();
    }

    /* access modifiers changed from: private */
    public Map<String, String> getSearchParamsMap() throws Exception {
        String z = this.location.getText().toString();
        String q = this.keyword.getText().toString();
        if (!Utils.isEmpty(z) || !Utils.isEmpty(q)) {
            Map<String, String> params = new HashMap<>();
            params.put(EngineConstants.QUERY, q == null ? null : q.trim());
            params.put(EngineConstants.COUNTRY, Country.getCountryIsoByPosition(this.countryId));
            params.put(EngineConstants.LOCATION, z);
            params.put(EngineConstants.IP, Utils.getLocalIpAddress());
            params.put(EngineConstants.MILES, Integer.toString(this.radius.getProgress()));
            return params;
        }
        throw new Exception("Incomplete search criteria\nPlease tell us either a Keyword or a Location to search!");
    }

    private Dialog buildLoadJobsDialog() {
        final Dialog d = new Dialog(this);
        d.setContentView((int) R.layout.load_saved);
        d.setTitle((int) R.string.load_jobs_dlg_title);
        ((Button) d.findViewById(R.id.loadDlgCancel)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                d.cancel();
            }
        });
        final ListView lv = (ListView) d.findViewById(R.id.savedSearchesList);
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, 17367059, new ArrayList<>(SavedSearchRepo.getSavedSearches(this).keySet())) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                ((CheckedTextView) view).setTextColor(-1);
                return view;
            }
        };
        lv.setAdapter((ListAdapter) adapter);
        final Button deleteBtn = (Button) d.findViewById(R.id.loadDlgDelete);
        final Button loadBtn = (Button) d.findViewById(R.id.loadDlgLoad);
        deleteBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!SearchActivity.this.isFinishing()) {
                    AlertDialog.Builder message = new AlertDialog.Builder(SearchActivity.this).setTitle("Warning").setMessage("This action is permanent, please confirm");
                    final Dialog dialog = d;
                    final ListView listView = lv;
                    final ArrayAdapter arrayAdapter = adapter;
                    final Button button = deleteBtn;
                    final Button button2 = loadBtn;
                    AlertDialog.Builder positiveButton = message.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dlg, int arg1) {
                            dlg.dismiss();
                            SearchActivity.this.onSavedSearchesDelete(dialog, listView, arrayAdapter, button, button2);
                        }
                    });
                    final Dialog dialog2 = d;
                    positiveButton.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dlg, int arg1) {
                            dlg.dismiss();
                            dialog2.dismiss();
                        }
                    }).show();
                }
            }
        });
        loadBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ListAdapter a = lv.getAdapter();
                Object o = null;
                long[] checked = Utils.getCheckItemIds(lv);
                SparseBooleanArray items = lv.getCheckedItemPositions();
                int length = checked.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        break;
                    }
                    long key = checked[i];
                    if (items.get((int) key)) {
                        o = a.getItem((int) key);
                        break;
                    }
                    i++;
                }
                if (o != null) {
                    try {
                        SearchActivity.this.loadSelectedSearch(d, o.toString());
                    } catch (Exception e) {
                        Exception e2 = e;
                        Log.e(SearchActivity.TAG, "Failed to process search: " + e2.getMessage(), e2);
                        Toast.makeText(SearchActivity.this, "Search error: " + e2.getMessage(), 1).show();
                    }
                }
            }
        });
        lv.setChoiceMode(2);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                switch (Utils.countPositives(lv.getCheckedItemPositions(), Utils.getCheckItemIds(lv))) {
                    case 0:
                        deleteBtn.setEnabled(false);
                        loadBtn.setEnabled(false);
                        return;
                    case 1:
                        deleteBtn.setEnabled(true);
                        loadBtn.setEnabled(true);
                        return;
                    default:
                        deleteBtn.setEnabled(true);
                        loadBtn.setEnabled(false);
                        return;
                }
            }
        });
        lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id) {
                SearchActivity.this.search(Utils.getUrl(SearchActivity.this, ((CheckedTextView) view).getText().toString()));
                SearchActivity.event("Action", "Search", "Saved search (long click)");
                if (!d.isShowing()) {
                    return true;
                }
                d.dismiss();
                return true;
            }
        });
        return d;
    }

    private Dialog buildSearchHelpDialog() {
        return Utils.buildInfoDialog(this, (int) R.string.search_help_title, (int) R.string.search_hint);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        new MenuInflater(getApplication()).inflate(R.menu.search_opts_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        switch (item.getItemId()) {
            case R.id.postAJob:
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://jobs.hireadroid.com/a/jbb/post-job")));
                break;
            case R.id.searchHelp:
                if (!isFinishing()) {
                    showDialog(0);
                    break;
                }
                break;
            case R.id.searchFeedback:
                if (!isFinishing()) {
                    showDialog(5);
                    break;
                }
                break;
            case R.id.settings:
                if (!isFinishing()) {
                    startActivity(new Intent(this, HireadroidPreferenceActivity.class));
                    break;
                }
                break;
        }
        return super.onMenuItemSelected(featureId, item);
    }

    private void toggleSearchPalette(View view) {
        int visible;
        if (this.palette.getVisibility() == 8) {
            visible = 0;
        } else {
            visible = 8;
        }
        this.palette.setVisibility(visible);
        Utils.getPrefs(this).edit().putInt(SHOW_PALETTE, visible).commit();
    }

    public void onClickSave(View view) {
        if (!isFinishing()) {
            showDialog(2);
        }
    }

    /* access modifiers changed from: private */
    public void loadSelectedSearch(Dialog d, String key) {
        String theUrl = Utils.getUrl(this, key);
        if (Utils.isNotEmpty(theUrl)) {
            Map<String, String> map = Utils.parseSearchParams(theUrl);
            String kword = map.get(EngineConstants.QUERY);
            String loc = map.get(EngineConstants.LOCATION);
            if (!TextUtils.isEmpty(kword) || !TextUtils.isEmpty(loc)) {
                this.keyword.setText(kword == null ? null : URLDecoder.decode(kword));
                if (!TextUtils.isEmpty(loc)) {
                    loc = URLDecoder.decode(loc);
                }
                this.location.setText(loc);
                String p = map.get(EngineConstants.MILES);
                if (EngineConstants.EXACT.equals(p)) {
                    p = "0";
                }
                setCountrySelectedId(Country.getCountryPositionByCode(map.get(EngineConstants.COUNTRY)));
                this.radius.setProgress(TextUtils.isEmpty(p) ? 25 : Integer.parseInt(p));
            } else {
                Toast.makeText(this, "Failed to load search items: empty keyword and location", 1).show();
                return;
            }
        }
        d.cancel();
    }

    private final class MyLocationTask extends AsyncTask<Void, String, Address> {
        private Exception error;

        private MyLocationTask() {
        }

        /* synthetic */ MyLocationTask(SearchActivity searchActivity, MyLocationTask myLocationTask) {
            this();
        }

        /* Debug info: failed to restart local var, previous not found, register: 13 */
        /* access modifiers changed from: protected */
        public Address doInBackground(Void... params) {
            try {
                LocationManager lm = (LocationManager) SearchActivity.this.getSystemService("location");
                Criteria criteria = new Criteria();
                criteria.setAccuracy(1);
                Location loc = lm.getLastKnownLocation(lm.getProvider(SearchActivity.PROVIDER_GPS) == null ? lm.getBestProvider(criteria, true) : SearchActivity.PROVIDER_GPS);
                if (loc == null) {
                    loc = lm.getLastKnownLocation("network");
                }
                if (loc != null) {
                    return new Geocoder(SearchActivity.this).getFromLocation(loc.getLatitude(), loc.getLongitude(), 1).get(0);
                }
                return null;
            } catch (Exception e) {
                Exception e2 = e;
                Log.e(SearchActivity.TAG, "onClickMyLocation", e2);
                this.error = e2;
                return null;
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Address address) {
            SearchActivity.this.location.setHint((int) R.string.location_hint);
            if (this.error != null) {
                SearchActivity.this.handleLocationError(Utils.formatError(this.error));
            } else if (address != null) {
                String z = address.getPostalCode();
                if (Utils.isEmpty(z)) {
                    z = address.getAddressLine(0);
                }
                SearchActivity.this.location.setText(z);
                int pos = Country.getCountryPositionByCode(address.getCountryCode());
                if (pos != SearchActivity.this.countryId) {
                    if (pos == -1) {
                        pos = 0;
                        Toast.makeText(SearchActivity.this, "Country at your location [" + address.getCountryName() + "] is not supported, setting to default (USA)", 1).show();
                    }
                    SearchActivity.this.setCountrySelectedId(pos);
                    SearchActivity.this.countries.setSelection(pos, true);
                }
            } else {
                SearchActivity.this.handleLocationError("Service returned invalid location");
            }
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            SearchActivity.this.location.setText("");
            SearchActivity.this.location.setHint((int) R.string.location_wait_hint);
        }

        /* access modifiers changed from: protected */
        public void onProgressUpdate(String... values) {
            if (values != null && Utils.isNotEmpty(values[0])) {
                Toast toast = Toast.makeText(SearchActivity.this, values[0], 1);
                toast.setGravity(17, 0, -20);
                toast.show();
            }
        }
    }

    /* access modifiers changed from: private */
    public void handleLocationError(String error) {
        try {
            if (!isFinishing()) {
                new AlertDialog.Builder(this).setTitle("Warning!").setMessage((int) R.string.failedGPS).setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dlg, int arg1) {
                        dlg.dismiss();
                        SearchActivity.this.onClickMyLocation(null);
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dlg, int arg1) {
                        dlg.dismiss();
                    }
                }).show();
            }
        } catch (Exception e) {
            Log.e("SearchActivity", "handleLocationError", e);
        }
    }

    /* access modifiers changed from: private */
    public void resetLocation() {
        Log.d(TAG, "Resetting location");
        this.location.setText((CharSequence) null);
        this.radius.setProgress(25);
        this.radiusField.setVisibility(8);
    }

    /* access modifiers changed from: private */
    public void setCountrySelectedId(int position) {
        this.countryId = position;
        getPreferences(2).edit().putInt(EngineConstants.COUNTRY_ID, position).commit();
        this.countries.setSelection(this.countryId);
        String iso = Country.getCountryIsoByPosition(position);
        this.flagImg.setImageDrawable(getResources().getDrawable(Country.getByIso(iso).icon));
        event("Action", "Select country", iso);
    }

    /* access modifiers changed from: private */
    public void onSavedSearchesDelete(Dialog d, ListView lv, ArrayAdapter<String> adapter, Button deleteBtn, Button loadBtn) {
        long[] ids = Utils.getCheckItemIds(lv);
        if (ids.length > 0) {
            SparseBooleanArray positions = lv.getCheckedItemPositions();
            ArrayList<String> keys = new ArrayList<>();
            for (long id : ids) {
                if (positions.get((int) id)) {
                    keys.add(adapter.getItem((int) id));
                }
            }
            lv.clearChoices();
            deleteBtn.setEnabled(false);
            loadBtn.setEnabled(false);
            Iterator it = keys.iterator();
            while (it.hasNext()) {
                String key = (String) it.next();
                adapter.remove(key);
                SavedSearchRepo.remove(key, this);
            }
            if (adapter.isEmpty()) {
                lv.setVisibility(8);
                d.findViewById(R.id.loadDlgError).setVisibility(0);
            }
        }
    }
}
