package bostone.android.hireadroid.activity;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.provider.SearchRecentSuggestions;
import android.widget.Toast;
import bostone.android.hireadroid.R;
import bostone.android.hireadroid.dao.SearchItemsDbHelper;
import bostone.android.hireadroid.engine.LinkedInEngine;
import bostone.android.hireadroid.oauth.JavaOAuthConnectorImpl;
import bostone.android.hireadroid.provider.SearchHistorySuggestionsProvider;
import bostone.android.hireadroid.provider.SearchItemsProvider;
import bostone.android.hireadroid.utils.HistoryManager;
import bostone.android.hireadroid.utils.Utils;

public class HireadroidPreferenceActivity extends PreferenceActivity {
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        addPreferencesFromResource(R.xml.preferences);
        findPreference("about").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                Utils.buildInfoDialog(HireadroidPreferenceActivity.this, (int) R.string.about_title, (int) R.string.about_hint).show();
                return true;
            }
        });
        final Preference liLogoutPref = findPreference("liLogout");
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        final String token = prefs.getString(LinkedInEngine.AUTH_TOKEN, null);
        final String secret = prefs.getString(LinkedInEngine.AUTH_TOKEN_SECRET, null);
        if (token == null || secret == null) {
            liLogoutPref.setEnabled(false);
        } else {
            liLogoutPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                public boolean onPreferenceClick(Preference preference) {
                    final String str = token;
                    final String str2 = secret;
                    final SharedPreferences sharedPreferences = prefs;
                    final Preference preference2 = liLogoutPref;
                    new AsyncTask<Void, Void, Void>() {
                        Throwable error;

                        /* access modifiers changed from: protected */
                        public Void doInBackground(Void... params) {
                            try {
                                new JavaOAuthConnectorImpl().init(str, str2);
                                return null;
                            } catch (Throwable th) {
                                this.error = th;
                                return null;
                            }
                        }

                        /* access modifiers changed from: protected */
                        public void onPostExecute(Void result) {
                            if (this.error == null) {
                                sharedPreferences.edit().remove(LinkedInEngine.AUTH_TOKEN).commit();
                                sharedPreferences.edit().remove(LinkedInEngine.AUTH_TOKEN_SECRET).commit();
                                preference2.setEnabled(false);
                                return;
                            }
                            Toast.makeText(HireadroidPreferenceActivity.this, "Failed to logout from LinkedIn", 1).show();
                        }
                    }.execute(new Void[0]);
                    return true;
                }
            });
        }
        findPreference("cleark").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                try {
                    new SearchRecentSuggestions(HireadroidPreferenceActivity.this, SearchHistorySuggestionsProvider.AUTHORITY, 1).clearHistory();
                    preference.setEnabled(false);
                    Toast.makeText(HireadroidPreferenceActivity.this, "Search suggestions removed", 1).show();
                } catch (Exception e) {
                    Toast.makeText(HireadroidPreferenceActivity.this, "Failed to clear suggestions:\n" + Utils.unwrap(e), 1).show();
                }
                return true;
            }
        });
        findPreference("clearf").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(final Preference preference) {
                try {
                    if (!HireadroidPreferenceActivity.this.isFinishing()) {
                        new AlertDialog.Builder(HireadroidPreferenceActivity.this).setTitle("Attention").setMessage("This action will unset all favorites, please confirm!").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                HireadroidPreferenceActivity.this.onClearFavorites(preference);
                            }
                        }).setNegativeButton("Cancel", (DialogInterface.OnClickListener) null).show();
                    }
                } catch (Exception e) {
                    Toast.makeText(HireadroidPreferenceActivity.this, "Failed to clear suggestions:\n" + Utils.unwrap(e), 1).show();
                }
                return true;
            }
        });
        findPreference("clearh").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(final Preference preference) {
                try {
                    if (!HireadroidPreferenceActivity.this.isFinishing()) {
                        new AlertDialog.Builder(HireadroidPreferenceActivity.this).setTitle("Attention").setMessage("This action will remove all history including favorites, please confirm!").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                HireadroidPreferenceActivity.this.onClearHistory(preference);
                            }
                        }).setNegativeButton("Cancel", (DialogInterface.OnClickListener) null).show();
                    }
                } catch (Exception e) {
                    Toast.makeText(HireadroidPreferenceActivity.this, "Failed to clear suggestions:\n" + Utils.unwrap(e), 1).show();
                }
                return true;
            }
        });
        ((ListPreference) findPreference("clearh_rate")).setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                prefs.edit().putInt(HistoryManager.MAINTANACE_TYPE, Integer.parseInt((String) newValue)).commit();
                return true;
            }
        });
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* access modifiers changed from: private */
    public void onClearFavorites(Preference preference) {
        ContentValues values = new ContentValues(1);
        values.put(SearchItemsDbHelper.ItemColumns.FAV, (Integer) 0);
        getContentResolver().update(SearchItemsProvider.CONTENT_URI, values, "favorite='1'", null);
        preference.setEnabled(false);
        Toast.makeText(this, "All favorites removed", 1).show();
    }

    /* access modifiers changed from: private */
    public void onClearHistory(Preference preference) {
        getContentResolver().delete(SearchItemsProvider.CONTENT_URI, null, null);
        preference.setEnabled(false);
        Toast.makeText(this, "All history removed", 1).show();
    }
}
