package bostone.android.hireadroid.activity;

import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.webkit.WebViewDatabase;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.widget.ViewSwitcher;
import bostone.android.hireadroid.R;
import bostone.android.hireadroid.model.SearchItem;
import bostone.android.hireadroid.ui.SummarySupport;
import bostone.android.hireadroid.utils.Utils;
import com.google.ads.AdRequest;
import com.google.ads.AdView;

public abstract class SummaryActivity extends MonitoredActivity implements SummarySupport {
    private static final String TAG = "SummaryActivity";
    protected AdView adView;
    protected WebView browser;
    protected String currentUrl;
    protected SearchItem item;
    protected ProgressBar progress;
    public View shareBtt;
    protected ViewSwitcher switcher;

    /* access modifiers changed from: protected */
    public boolean initSummary() {
        WebViewDatabase webViewDB = null;
        try {
            webViewDB = WebViewDatabase.getInstance(this);
        } catch (Exception e) {
            Log.e(TAG, "Failed to init browser database", e);
        }
        if (webViewDB == null) {
            return false;
        }
        this.switcher = (ViewSwitcher) findViewById(R.id.switcher);
        this.browser = (WebView) findViewById(R.id.browser);
        this.progress = (ProgressBar) findViewById(R.id.progress);
        WebSettings settings = this.browser.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(false);
        settings.setPluginsEnabled(true);
        settings.setLoadsImagesAutomatically(true);
        settings.setBuiltInZoomControls(true);
        settings.setSupportZoom(true);
        settings.setSavePassword(false);
        this.browser.setWebChromeClient(new WebChromeClient() {
            final Window win;

            {
                this.win = SummaryActivity.this.getWindow();
            }

            public void onProgressChanged(WebView view, int i) {
                this.win.setFeatureInt(5, -4);
                if (i > 50) {
                    SummaryActivity.this.showProgress(false);
                }
                if (i < 100) {
                    this.win.setFeatureInt(2, i * 100);
                } else {
                    this.win.setFeatureInt(2, -2);
                }
            }
        });
        this.browser.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return super.shouldOverrideUrlLoading(view, url);
            }
        });
        if (this.adView == null) {
            this.adView = (AdView) findViewById(R.id.ad);
        }
        if (this.adView != null) {
            if (this.adSupported) {
                this.adView.setVisibility(0);
                this.adView.loadAd(new AdRequest());
            } else {
                this.adView.setVisibility(8);
            }
        }
        this.shareBtt = findViewById(R.id.bttShare);
        this.shareBtt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SummaryActivity.this.onClickSummaryShare();
            }
        });
        return true;
    }

    /* access modifiers changed from: private */
    public void showProgress(boolean show) {
        this.progress.setVisibility(show ? 0 : 4);
    }

    public void onDestroy() {
        if (this.adView != null && this.adSupported) {
            this.adView.destroy();
        }
        super.onDestroy();
    }

    public void preloadSummary(String url) {
        if (this.browser != null) {
            this.browser.loadUrl("about:blank");
            this.currentUrl = url;
            this.browser.loadUrl(url);
        }
        if (this.adView != null && this.adSupported) {
            this.adView.loadAd(new AdRequest());
        }
    }

    public void viewSummary(SearchItem item2) {
        this.item = item2;
        if (item2 == null || item2.listingUrl == null) {
            loadErrorPage();
        } else if (!item2.listingUrl.equals(this.currentUrl)) {
            try {
                preloadSummary(item2.listingUrl);
            } catch (Throwable th) {
                Log.e(TAG, "Failed to load");
                loadErrorPage();
            }
        }
        this.switcher.setDisplayedChild(1);
    }

    /* access modifiers changed from: protected */
    public void loadErrorPage() {
        if (this.browser != null) {
            this.browser.loadUrl("file:///android_asset/error.html");
        }
    }

    public boolean trackRead() {
        return false;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4 && event.getRepeatCount() == 0 && this.switcher != null && this.switcher.getDisplayedChild() == 1) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode != 4 || this.switcher == null || this.switcher.getDisplayedChild() != 1) {
            return super.onKeyUp(keyCode, event);
        }
        this.switcher.setDisplayedChild(0);
        return true;
    }

    /* access modifiers changed from: protected */
    public void refresh() {
        if (this.currentUrl == null || this.switcher.getDisplayedChild() != 1) {
            super.refresh();
        } else {
            this.browser.loadUrl(this.currentUrl);
        }
    }

    /* access modifiers changed from: protected */
    public void onClickSummaryShare() {
        if (this.item != null) {
            Utils.share(this, this.item);
        } else {
            Toast.makeText(this, "Sorry, unable to generate share listing", 1).show();
        }
    }
}
