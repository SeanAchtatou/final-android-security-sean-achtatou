package bostone.android.hireadroid.activity;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import bostone.android.hireadroid.R;
import bostone.android.hireadroid.dao.ItemDao;
import bostone.android.hireadroid.dao.SearchItemsDbHelper;
import bostone.android.hireadroid.model.SearchItem;
import bostone.android.hireadroid.provider.SearchItemsProvider;
import bostone.android.hireadroid.ui.FavoritesSupport;
import bostone.android.hireadroid.ui.ItemHolder;
import bostone.android.hireadroid.utils.Utils;
import java.util.ArrayList;
import java.util.List;

public class FavoritesActivity extends SummaryActivity {
    private ListView list;
    List<Long> viewedItems = new ArrayList();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void} */
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.favorites);
        findViewById(R.id.titleBtn1).setVisibility(0);
        ImageButton bttSendBy = (ImageButton) findViewById(R.id.titleBtn2);
        bttSendBy.setVisibility(0);
        bttSendBy.setImageResource(R.drawable.ic_btt_share_white);
        bttSendBy.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                FavoritesActivity.this.onClickBttSendBy(v);
            }
        });
        ContentValues values = new ContentValues(1);
        values.put(SearchItemsDbHelper.ItemColumns.VIEWED, (Boolean) false);
        getContentResolver().update(SearchItemsProvider.CONTENT_URI, values, null, null);
        initSummary();
    }

    /* access modifiers changed from: protected */
    public void onClickBttSendBy(View v) {
        if (this.list == null || this.list.getCount() == 0) {
            Toast.makeText(this, (int) R.string.warn_favs_empty, 1).show();
        } else if (this.switcher.getDisplayedChild() == 1) {
            onClickSummaryShare();
        } else {
            onClickShareAll();
        }
    }

    /* access modifiers changed from: protected */
    public void onClickShareAll() {
        StringBuilder sb = new StringBuilder();
        Cursor c = ((CursorAdapter) this.list.getAdapter()).getCursor();
        if (c.moveToFirst()) {
            do {
                String url = c.getString(c.getColumnIndex(SearchItemsDbHelper.ItemColumns.SHORT_URL));
                if (Utils.isEmpty(url)) {
                    url = c.getString(c.getColumnIndex(SearchItemsDbHelper.ItemColumns.URL));
                }
                String title = c.getString(c.getColumnIndex(SearchItemsDbHelper.ItemColumns.TITLE));
                if (Utils.isNotEmpty(url)) {
                    sb.append(title).append(" [").append(url).append("]\n\n");
                }
            } while (c.moveToNext());
        }
        Utils.shareTextBy(this, sb.toString());
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        this.list = (ListView) findViewById(R.id.favsList);
        final Cursor c = managedQuery(SearchItemsProvider.CONTENT_URI, null, "favorite=?", new String[]{"1"}, "lastSeeingDate DESC");
        CursorAdapter adapter = new CursorAdapter(this, c) {
            public View newView(Context context, Cursor cursor, ViewGroup parent) {
                View view = FavoritesActivity.this.getLayoutInflater().inflate((int) R.layout.job_search_item, (ViewGroup) null);
                FavoritesActivity favoritesActivity = FavoritesActivity.this;
                final Cursor cursor2 = c;
                ItemHolder holder = new ItemHolder(favoritesActivity, view, new FavoritesSupport() {
                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
                     arg types: [java.lang.String, int]
                     candidates:
                      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
                      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
                      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
                      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
                      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
                      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
                      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
                      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
                      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
                    public void remove(SearchItem item) {
                        ContentValues values = new ContentValues(1);
                        values.put(SearchItemsDbHelper.ItemColumns.FAV, (Integer) 0);
                        FavoritesActivity.this.getContentResolver().update(SearchItemsProvider.CONTENT_URI, values, "jobId=?", new String[]{item.jobId});
                        cursor2.requery();
                    }

                    public boolean hasId(SearchItem item) {
                        return true;
                    }

                    public void setNote(SearchItem item) {
                        ContentValues values = new ContentValues(1);
                        values.put(SearchItemsDbHelper.ItemColumns.NOTE, item.comment);
                        ItemDao.update(FavoritesActivity.this.getContentResolver(), item.jobId, values);
                        cursor2.requery();
                    }

                    public String getNote(SearchItem item) {
                        return null;
                    }

                    public void add(SearchItem item) {
                    }
                }, FavoritesActivity.this);
                holder.reset(SearchItem.fillFromCursor(cursor, holder.item));
                view.setTag(holder);
                FavoritesActivity.this.setItemBackground(view, holder);
                return view;
            }

            public void bindView(View view, Context context, Cursor cursor) {
                ItemHolder holder = (ItemHolder) view.getTag();
                if (holder != null) {
                    holder.reset(SearchItem.fillFromCursor(cursor, holder.item));
                }
                FavoritesActivity.this.setItemBackground(view, holder);
            }
        };
        this.list.setEmptyView((TextView) findViewById(R.id.favsEmpty));
        this.list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                ItemHolder h = (ItemHolder) view.getTag();
                if (h != null) {
                    h.flipBody();
                    ContentValues values = new ContentValues(1);
                    values.put(SearchItemsDbHelper.ItemColumns.VIEWED, Boolean.valueOf(h.item.viewed));
                    FavoritesActivity.this.getContentResolver().update(SearchItemsProvider.CONTENT_URI, values, "jobId=?", new String[]{h.item.jobId});
                    if (c != null) {
                        c.requery();
                    }
                }
            }
        });
        this.list.setAdapter((ListAdapter) adapter);
    }

    /* access modifiers changed from: protected */
    public boolean initSummary() {
        boolean ok = super.initSummary();
        if (this.shareBtt != null) {
            this.shareBtt.setVisibility(8);
        }
        return ok;
    }

    /* access modifiers changed from: protected */
    public void setItemBackground(View view, ItemHolder holder) {
        if (holder.item.preferred) {
            view.setBackgroundResource(R.drawable.list_background_preferred);
        } else {
            view.setBackgroundResource(17301602);
        }
    }

    public boolean trackRead() {
        return false;
    }
}
