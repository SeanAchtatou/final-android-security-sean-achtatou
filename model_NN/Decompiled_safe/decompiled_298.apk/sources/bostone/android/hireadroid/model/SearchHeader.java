package bostone.android.hireadroid.model;

import java.io.Serializable;

public class SearchHeader implements Serializable {
    public static final String DATE = "dt";
    public static final String NUM_RETURNED_RESULTS = "rpd";
    public static final String NUM_TOTAL_RESULTS = "tr";
    public static final String NUM_VIEWABLE_RESULTS = "tv";
    public static final String REQUEST = "rq";
    public static final String START_INDEX = "si";
    public static final String TITLE = "t";
    private static final long serialVersionUID = -8797147949654200765L;
    public String date;
    public String[] messages;
    public int numReturnedResults;
    public int numTotalResults;
    public int numViewableResults;
    public int startIndex;
    public String title;
    public String url;

    public void set(String key, String value) {
        if (TITLE.equals(key)) {
            this.title = value;
        } else if (DATE.equals(key)) {
            this.date = value;
        } else if (START_INDEX.equals(key)) {
            this.startIndex = Integer.parseInt(value);
        } else if (NUM_RETURNED_RESULTS.equals(key)) {
            this.numReturnedResults = Integer.parseInt(value);
        } else if (NUM_TOTAL_RESULTS.equals(key)) {
            this.numTotalResults = Integer.parseInt(value);
        } else if (NUM_VIEWABLE_RESULTS.equals(key)) {
            this.numViewableResults = Integer.parseInt(value);
        }
    }

    public String toString() {
        return "[" + this.title + "," + this.date + "," + this.startIndex + "," + this.numReturnedResults + "," + this.numTotalResults + "," + this.numViewableResults + "]";
    }

    public void reset() {
        this.date = null;
        this.messages = null;
        this.numReturnedResults = 0;
        this.numTotalResults = 0;
        this.numViewableResults = 0;
        this.startIndex = 0;
        this.title = null;
        this.url = null;
    }
}
