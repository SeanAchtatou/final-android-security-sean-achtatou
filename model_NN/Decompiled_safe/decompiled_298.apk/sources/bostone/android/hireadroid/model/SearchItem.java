package bostone.android.hireadroid.model;

import android.content.ContentValues;
import android.database.Cursor;
import bostone.android.hireadroid.dao.SearchItemsDbHelper;
import bostone.android.hireadroid.utils.Utils;
import java.io.Serializable;

public class SearchItem implements Serializable {
    public static final String COMPANY = "cn";
    public static final String DATE_LAST_SEEING = "ls";
    public static final String DATE_POSTED = "dp";
    public static final String JOB_ID = "jid";
    public static final String JOB_TITLE = "jt";
    public static final String LISTING_SOURCE = "src";
    public static final String LISTING_TYPE = "ty";
    public static final String LISTING_URL = "url";
    public static final String LOCATION = "loc";
    public static final String SNIPPET = "e";
    private static final long serialVersionUID = 2129334420298774209L;
    public String comment;
    public String company;
    public String engine;
    public boolean favorite;
    public long id;
    public String jobId;
    public String jobTitle;
    public long lastSeeingDate;
    public String listingSource;
    public String listingType;
    public String listingUrl;
    public Location location = new Location();
    public long postedDate;
    public boolean preferred;
    public String shortUrl;
    public String snippet;
    public long tsRead;
    public transient boolean viewed;

    public void set(String key, String value) {
        if (JOB_TITLE.equals(key)) {
            this.jobTitle = value;
        } else if (COMPANY.equals(key)) {
            this.company = value;
        } else if (LISTING_SOURCE.equals(key)) {
            this.listingSource = value;
        } else if (LISTING_TYPE.equals(key)) {
            this.listingType = value;
        } else if (LISTING_URL.equals(key)) {
            this.listingUrl = value;
        } else if (SNIPPET.equals(key)) {
            this.snippet = value;
        } else if (DATE_LAST_SEEING.equals(key)) {
            this.lastSeeingDate = Utils.convertToTs(value, this.engine);
        } else if (DATE_POSTED.equals(key)) {
            this.postedDate = Utils.convertToTs(value, this.engine);
        } else if (LOCATION.equals(key)) {
            this.location.name = value;
        } else if (JOB_ID.equals(key)) {
            this.jobId = value;
        }
    }

    public boolean isSame(SearchItem item) {
        if (item == null) {
            return false;
        }
        return this.jobId.equals(item.jobId);
    }

    public String toString() {
        return "SearchItem [jobTitle=" + this.jobTitle + ", company=" + this.company + ", listingSource=" + this.listingSource + ", location=" + this.location + ", postedDate=" + this.postedDate + ", engine=" + this.engine + "]";
    }

    public static SearchItem fillFromCursor(Cursor c, SearchItem item) {
        boolean z;
        boolean z2;
        if (item == null) {
            item = new SearchItem();
        }
        item.id = c.getLong(0);
        item.jobId = c.getString(1);
        item.jobTitle = c.getString(2);
        item.company = c.getString(3);
        item.listingUrl = c.getString(4);
        item.shortUrl = c.getString(5);
        item.listingSource = c.getString(6);
        item.listingType = c.getString(7);
        item.lastSeeingDate = c.getLong(8);
        item.postedDate = c.getLong(9);
        item.snippet = c.getString(10);
        item.engine = c.getString(11);
        item.preferred = c.getInt(12) == 1;
        if (c.getInt(13) == 1) {
            z = true;
        } else {
            z = false;
        }
        item.favorite = z;
        item.location.name = c.getString(14);
        item.location.city = c.getString(15);
        item.location.st = c.getString(16);
        item.location.postal = c.getString(17);
        item.location.country = c.getString(18);
        item.location.region = c.getString(19);
        item.location.longtitude = c.getDouble(20);
        item.location.latitude = c.getDouble(21);
        item.comment = c.getString(22);
        if (c.getInt(23) == 1) {
            z2 = true;
        } else {
            z2 = false;
        }
        item.viewed = z2;
        return item;
    }

    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();
        if (this.id > 0) {
            values.put("_id", Long.valueOf(this.id));
        }
        values.put(SearchItemsDbHelper.ItemColumns.JOB_ID, this.jobId);
        values.put(SearchItemsDbHelper.ItemColumns.TITLE, this.jobTitle);
        values.put(SearchItemsDbHelper.ItemColumns.COMPANY, this.company);
        values.put(SearchItemsDbHelper.ItemColumns.URL, this.listingUrl);
        values.put(SearchItemsDbHelper.ItemColumns.SHORT_URL, this.shortUrl);
        values.put(SearchItemsDbHelper.ItemColumns.SOURCE, this.listingSource);
        values.put(SearchItemsDbHelper.ItemColumns.TYPE, this.listingType);
        values.put(SearchItemsDbHelper.ItemColumns.DATE, Long.valueOf(this.lastSeeingDate == 0 ? this.postedDate : this.lastSeeingDate));
        values.put(SearchItemsDbHelper.ItemColumns.POST_DATE, Long.valueOf(this.postedDate));
        values.put(SearchItemsDbHelper.ItemColumns.SNIPPET, this.snippet);
        values.put(SearchItemsDbHelper.ItemColumns.ENG, this.engine);
        values.put(SearchItemsDbHelper.ItemColumns.PREFERRED, Boolean.valueOf(this.preferred));
        values.put(SearchItemsDbHelper.ItemColumns.FAV, Boolean.valueOf(this.favorite));
        values.put(SearchItemsDbHelper.ItemColumns.LOC_NAME, this.location.name);
        values.put(SearchItemsDbHelper.ItemColumns.LOC_CITY, this.location.city);
        values.put(SearchItemsDbHelper.ItemColumns.LOC_ST, this.location.st);
        values.put(SearchItemsDbHelper.ItemColumns.LOC_POSTAL, this.location.postal);
        values.put(SearchItemsDbHelper.ItemColumns.LOC_COUNTRY, this.location.country);
        values.put(SearchItemsDbHelper.ItemColumns.LOC_REGION, this.location.region);
        values.put(SearchItemsDbHelper.ItemColumns.LOC_LONGTITUDE, Double.valueOf(this.location.longtitude));
        values.put(SearchItemsDbHelper.ItemColumns.LOC_LATITUDE, Double.valueOf(this.location.latitude));
        values.put(SearchItemsDbHelper.ItemColumns.NOTE, this.comment);
        values.put(SearchItemsDbHelper.ItemColumns.VIEWED, Boolean.valueOf(this.viewed));
        values.put(SearchItemsDbHelper.ItemColumns.READ_TS, Long.valueOf(this.tsRead));
        return values;
    }
}
