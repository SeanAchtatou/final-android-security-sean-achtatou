package bostone.android.hireadroid.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SearchResult implements Serializable {
    private static final long serialVersionUID = 346241017139507046L;
    public SearchHeader header = new SearchHeader();
    public List<SearchItem> items = new ArrayList();

    public void reset() {
        this.items.clear();
        this.header.reset();
    }
}
