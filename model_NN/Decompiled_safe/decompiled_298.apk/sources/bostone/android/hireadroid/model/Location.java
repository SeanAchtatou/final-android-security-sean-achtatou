package bostone.android.hireadroid.model;

import bostone.android.hireadroid.dao.SearchItemsDbHelper;
import bostone.android.hireadroid.utils.Utils;
import java.io.Serializable;
import org.xml.sax.Attributes;

public class Location implements Serializable {
    private static final long serialVersionUID = 5742635419654729866L;
    public String city = "";
    public String country = "US";
    public String county = "";
    public double latitude;
    public double longtitude;
    public String name = "";
    public String postal = "";
    public String region = "";
    public String st = "";

    public void set(Attributes attributes) {
        this.city = attributes.getValue(SearchItemsDbHelper.ItemColumns.LOC_CITY);
        this.st = attributes.getValue(SearchItemsDbHelper.ItemColumns.LOC_ST);
        this.postal = attributes.getValue(SearchItemsDbHelper.ItemColumns.LOC_POSTAL);
        this.county = attributes.getValue("county");
        this.region = attributes.getValue(SearchItemsDbHelper.ItemColumns.LOC_REGION);
        this.country = attributes.getValue(SearchItemsDbHelper.ItemColumns.LOC_COUNTRY);
    }

    public String toString() {
        if (Utils.isNotEmpty(this.name)) {
            return this.name;
        }
        StringBuilder sb = new StringBuilder();
        if (this.city != null) {
            sb.append(this.city);
        } else if (this.postal != null) {
            sb.append(this.postal);
        }
        if (this.country != null) {
            if (sb.length() > 0) {
                sb.append(", ");
            }
            sb.append(this.country);
        }
        if (sb.length() < 1) {
            sb.append("Undetermined location");
        }
        return sb.toString();
    }
}
