package bostone.android.hireadroid.model;

import bostone.android.hireadroid.R;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

public class Country implements Serializable {
    public static final String AT = "AT";
    public static final String AU = "AU";
    public static final String BE = "BE";
    public static final String BR = "BR";
    public static final String CA = "CA";
    public static final String CH = "CH";
    public static final String CN = "CN";
    public static final Map<String, Country> COUNTRIES = new LinkedHashMap();
    public static final String DE = "DE";
    public static final String ES = "ES";
    public static final String FR = "FR";
    public static final String GB = "GB";
    public static final int[] ICONS = new int[COUNTRIES.size()];
    public static final String IE = "IE";
    public static final String IN = "IN";
    public static final String[] ISO_CODES = new String[COUNTRIES.size()];
    public static final String IT = "IT";
    public static final String JP = "JP";
    public static final String MX = "MX";
    public static final String NL = "NL";
    public static final String UK = "UK";
    public static final String US = "US";
    public int icon;
    public String iso;
    public String name;

    static {
        COUNTRIES.put("US", new Country("US", "United States", R.drawable.flag_us));
        COUNTRIES.put(AU, new Country(AU, "Australia", R.drawable.flag_au));
        COUNTRIES.put(AT, new Country(AT, "Austria", R.drawable.flag_at));
        COUNTRIES.put(BE, new Country(BE, "Belgium", R.drawable.flag_be));
        COUNTRIES.put(BR, new Country(BR, "Brazil", R.drawable.flag_br));
        COUNTRIES.put(CA, new Country(CA, "Canada", R.drawable.flag_ca));
        COUNTRIES.put(CN, new Country(CN, "China", R.drawable.flag_cn));
        COUNTRIES.put(FR, new Country(FR, "France", R.drawable.flag_fr));
        COUNTRIES.put(DE, new Country(DE, "Germany", R.drawable.flag_de));
        COUNTRIES.put(IN, new Country(IN, "India", R.drawable.flag_in));
        COUNTRIES.put(IE, new Country(IE, "Ireland", R.drawable.flag_ie));
        COUNTRIES.put(IT, new Country(IT, "Italy", R.drawable.flag_it));
        COUNTRIES.put(JP, new Country(JP, "Japan", R.drawable.flag_jp));
        COUNTRIES.put(MX, new Country(MX, "Mexico", R.drawable.flag_mx));
        COUNTRIES.put(NL, new Country(NL, "Netherlands", R.drawable.flag_nl));
        COUNTRIES.put(ES, new Country(ES, "Spain", R.drawable.flag_es));
        COUNTRIES.put(CH, new Country(CH, "Switzerland", R.drawable.flag_ch));
        COUNTRIES.put(UK, new Country(UK, "United Kingdom", R.drawable.flag_uk));
        int i = 0;
        for (Map.Entry<String, Country> entry : COUNTRIES.entrySet()) {
            ISO_CODES[i] = (String) entry.getKey();
            ICONS[i] = ((Country) entry.getValue()).icon;
            i++;
        }
    }

    public static Country getByIso(String iso2) {
        return COUNTRIES.get(iso2);
    }

    public static String getCountryIsoByPosition(int position) {
        return ISO_CODES[position];
    }

    public static int getCountryPositionByCode(String code) {
        for (int i = 0; i < ISO_CODES.length; i++) {
            if (ISO_CODES[i].equalsIgnoreCase(code)) {
                return i;
            }
        }
        return -1;
    }

    public Country(String iso2, String name2, int icon2) {
        this.iso = iso2;
        this.name = name2;
        this.icon = icon2;
    }

    public int hashCode() {
        int i = 1 * 31;
        return (this.iso == null ? 0 : this.iso.hashCode()) + 31;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Country other = (Country) obj;
        if (this.iso == null) {
            if (other.iso != null) {
                return false;
            }
        } else if (!this.iso.equals(other.iso)) {
            return false;
        }
        return true;
    }
}
