package bostone.android.hireadroid;

public class HireadroidAuthException extends HireadroidException {
    public HireadroidAuthException() {
    }

    public HireadroidAuthException(String detailMessage) {
        super(detailMessage);
    }

    public HireadroidAuthException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public HireadroidAuthException(Throwable throwable) {
        super(throwable);
    }
}
