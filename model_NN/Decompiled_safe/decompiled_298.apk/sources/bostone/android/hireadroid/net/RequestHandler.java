package bostone.android.hireadroid.net;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Looper;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewDatabase;
import bostone.android.hireadroid.HireadroidException;
import bostone.android.hireadroid.utils.Utils;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import javax.net.ssl.SSLHandshakeException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import net.oauth.OAuth;
import org.apache.http.HttpEntity;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.NoHttpResponseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class RequestHandler {
    /* access modifiers changed from: private */
    public static boolean AGENT_SET = false;
    private static final String TAG = "RequestHandler";
    public static String USER_AGENT = "Mozilla/5.0 (Linux; U; Android 1.5; en-us;dream) AppleWebKit/525.10+ (KHTML, like Gecko) Version/3.0.4 Mobile Safari/523.12.2";
    private static DefaultHttpClient _client;
    public final DefaultHttpClient client;

    public RequestHandler(Context context) {
        if (_client == null) {
            setUserAgent(context);
            _client = getThreadSafeHttpClient();
        }
        this.client = _client;
    }

    private void setUserAgent(final Context context) {
        if (context != null && !AGENT_SET) {
            try {
                if (WebViewDatabase.getInstance(context) != null) {
                    new AsyncTask<Void, Void, String>() {
                        /* access modifiers changed from: protected */
                        public String doInBackground(Void... params) {
                            if (Looper.myLooper() == null) {
                                Looper.prepare();
                            }
                            return new WebView(context).getSettings().getUserAgentString();
                        }

                        /* access modifiers changed from: protected */
                        public void onPostExecute(String result) {
                            if (result != null) {
                                RequestHandler.USER_AGENT = result;
                                RequestHandler.AGENT_SET = true;
                            }
                        }
                    }.execute(new Void[0]);
                }
            } catch (Throwable th) {
                Log.e(TAG, "Can't obtain real USER AGENT", th);
            }
        }
    }

    private DefaultHttpClient getThreadSafeHttpClient() {
        HttpParams params = new BasicHttpParams();
        params.setParameter("http.useragent", USER_AGENT);
        params.setBooleanParameter("http.protocol.allow-circular-redirects", true);
        params.setParameter("http.protocol.cookie-policy", "compatibility");
        HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
        HttpProtocolParams.setContentCharset(params, OAuth.ENCODING);
        SchemeRegistry registry = new SchemeRegistry();
        registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
        SSLSocketFactory sslSocketFactory = SSLSocketFactory.getSocketFactory();
        sslSocketFactory.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
        registry.register(new Scheme("https", sslSocketFactory, 443));
        DefaultHttpClient httpclient = new DefaultHttpClient(new ThreadSafeClientConnManager(params, registry), params);
        httpclient.setHttpRequestRetryHandler(new HttpRequestRetryHandler() {
            public boolean retryRequest(IOException exception, int executionCount, HttpContext context) {
                boolean idempotent;
                if (executionCount >= 5) {
                    return false;
                }
                if (exception instanceof NoHttpResponseException) {
                    return true;
                }
                if (exception instanceof SSLHandshakeException) {
                    return false;
                }
                if (((HttpRequest) context.getAttribute("http.request")) instanceof HttpEntityEnclosingRequest) {
                    idempotent = false;
                } else {
                    idempotent = true;
                }
                if (idempotent) {
                    return true;
                }
                return false;
            }
        });
        return httpclient;
    }

    public String post(String url, Map<String, String> params) throws ClientProtocolException, IOException {
        HttpPost post = new HttpPost(url);
        if (params != null) {
            try {
                if (!params.isEmpty()) {
                    List<NameValuePair> nvps = new ArrayList<>();
                    for (Map.Entry<String, String> pair : params.entrySet()) {
                        nvps.add(new BasicNameValuePair((String) pair.getKey(), (String) pair.getValue()));
                    }
                    post.setEntity(new UrlEncodedFormEntity(nvps, OAuth.ENCODING));
                }
            } catch (Throwable th) {
                post.abort();
                throw th;
            }
        }
        String body = new BasicResponseHandler().handleResponse(this.client.execute(post));
        post.abort();
        return body;
    }

    public String service(String url, DefaultHandler handler) throws ClientProtocolException, IOException, ParserConfigurationException, SAXException {
        HttpGet get = new HttpGet(url);
        try {
            String body = new BasicResponseHandler().handleResponse(this.client.execute(get));
            if (!Utils.isEmpty(body) && !body.contains("<sherror>")) {
                SAXParserFactory.newInstance().newSAXParser().parse(new InputSource(new StringReader(body)), handler);
                get.abort();
                this.client.getConnectionManager().closeIdleConnections(15, TimeUnit.SECONDS);
                return body;
            } else if (body.contains("<option url")) {
                throw new HireadroidException("Multiple destinations returned, please refine your locaton criteria");
            } else {
                get.abort();
                this.client.getConnectionManager().closeIdleConnections(15, TimeUnit.SECONDS);
                return null;
            }
        } catch (OutOfMemoryError e) {
            OutOfMemoryError e2 = e;
            System.gc();
            throw new HireadroidException("Unable to load. Insufficient memory", e2);
        } catch (Throwable th) {
            get.abort();
            this.client.getConnectionManager().closeIdleConnections(15, TimeUnit.SECONDS);
            throw th;
        }
    }

    public String[] get(String url) throws ClientProtocolException, IOException {
        HttpEntity entity;
        HttpEntity entity2;
        HttpGet get = new HttpGet(url);
        HttpContext localContext = new BasicHttpContext();
        HttpResponse response = null;
        try {
            response = this.client.execute(get, localContext);
            String[] strArr = {((HttpHost) localContext.getAttribute("http.target_host")).toURI(), new BasicResponseHandler().handleResponse(response)};
            if (!(response == null || (entity2 = response.getEntity()) == null)) {
                try {
                    entity2.consumeContent();
                } catch (IOException e) {
                    Log.e(TAG, "RequestHandler#get:146", e);
                }
            }
            get.abort();
            this.client.getConnectionManager().closeIdleConnections(15, TimeUnit.SECONDS);
            return strArr;
        } catch (OutOfMemoryError e2) {
            OutOfMemoryError e3 = e2;
            System.gc();
            throw new HireadroidException(e3);
        } catch (Throwable th) {
            if (!(response == null || (entity = response.getEntity()) == null)) {
                try {
                    entity.consumeContent();
                } catch (IOException e4) {
                    Log.e(TAG, "RequestHandler#get:146", e4);
                }
            }
            get.abort();
            this.client.getConnectionManager().closeIdleConnections(15, TimeUnit.SECONDS);
            throw th;
        }
    }
}
