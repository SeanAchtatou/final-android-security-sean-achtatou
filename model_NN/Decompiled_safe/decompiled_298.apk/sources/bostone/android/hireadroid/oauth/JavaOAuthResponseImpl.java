package bostone.android.hireadroid.oauth;

import bostone.android.hireadroid.HireadroidAuthException;
import bostone.android.hireadroid.oauth.Connector;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.oauth.OAuthMessage;
import net.oauth.client.OAuthResponseMessage;

public class JavaOAuthResponseImpl implements Connector.SiteResponse {
    OAuthMessage result;

    public JavaOAuthResponseImpl(OAuthMessage result2) {
        this.result = result2;
    }

    public String getBody() {
        try {
            return this.result.readBodyAsString();
        } catch (IOException e) {
            throw new HireadroidAuthException(e);
        }
    }

    public int getCode() {
        try {
            return ((OAuthResponseMessage) this.result).getHttpResponse().getStatusCode();
        } catch (IOException e) {
            throw new HireadroidAuthException(e);
        }
    }

    public String getHeader(String name) {
        return this.result.getHeader(name);
    }

    public Map<String, String> getHeaders() {
        List<Map.Entry<String, String>> list = this.result.getHeaders();
        Map<String, String> map = new HashMap<>(list.size());
        for (Map.Entry<String, String> entry : list) {
            map.put((String) entry.getKey(), (String) entry.getValue());
        }
        return map;
    }

    public InputStream getStream() {
        try {
            return this.result.getBodyAsStream();
        } catch (IOException e) {
            throw new HireadroidAuthException(e);
        }
    }
}
