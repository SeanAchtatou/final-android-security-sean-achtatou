package bostone.android.hireadroid.oauth;

public class ConnectorException extends RuntimeException {
    private static final long serialVersionUID = 3175341251824722867L;

    public ConnectorException() {
    }

    public ConnectorException(String detailMessage) {
        super(detailMessage);
    }

    public ConnectorException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public ConnectorException(Throwable throwable) {
        super(throwable);
    }
}
