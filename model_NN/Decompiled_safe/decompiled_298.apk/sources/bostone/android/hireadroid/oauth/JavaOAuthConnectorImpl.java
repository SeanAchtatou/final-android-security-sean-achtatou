package bostone.android.hireadroid.oauth;

import bostone.android.hireadroid.HireadroidAuthException;
import bostone.android.hireadroid.oauth.Connector;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import net.oauth.OAuth;
import net.oauth.OAuthAccessor;
import net.oauth.OAuthConsumer;
import net.oauth.OAuthException;
import net.oauth.OAuthMessage;
import net.oauth.OAuthProblemException;
import net.oauth.OAuthServiceProvider;
import net.oauth.ParameterStyle;
import net.oauth.client.OAuthClient;
import net.oauth.client.httpclient4.HttpClient4;
import net.oauth.http.HttpMessage;
import net.oauth.http.HttpMessageDecoder;
import org.apache.http.client.ClientProtocolException;

public class JavaOAuthConnectorImpl implements Connector {
    private final OAuthAccessor accessor;
    private final OAuthClient client;
    private final Properties props = new Properties();

    public JavaOAuthConnectorImpl() {
        this.props.put("authorizationUrl", Connector.AUTHORIZE_URL);
        this.props.put("requestUrl", Connector.REQUEST_TOKEN_URL);
        this.props.put("accessUrl", Connector.ACCESS_TOKEN_URL);
        this.props.put("consumerKey", Connector.CONSUMER_KEY);
        this.props.put("consumerSecret", Connector.CONSUMER_SECRET);
        this.accessor = new OAuthAccessor(new OAuthConsumer(null, Connector.CONSUMER_KEY, Connector.CONSUMER_SECRET, new OAuthServiceProvider(Connector.REQUEST_TOKEN_URL, Connector.AUTHORIZE_URL, Connector.ACCESS_TOKEN_URL)));
        this.client = new OAuthClient(new HttpClient4());
    }

    public String authenticate() {
        try {
            this.client.getRequestToken(this.accessor, "POST");
            this.props.setProperty("requestToken", this.accessor.requestToken);
            this.props.setProperty("tokenSecret", this.accessor.tokenSecret);
            return String.valueOf(invoke("GET", this.accessor.consumer.serviceProvider.userAuthorizationURL, OAuth.newList(OAuth.OAUTH_TOKEN, this.accessor.requestToken), null, false, true, false, false).URL) + "&" + OAuth.OAUTH_TOKEN_SECRET + "=" + this.accessor.tokenSecret;
        } catch (IOException e) {
            throw new HireadroidAuthException(e);
        } catch (URISyntaxException e2) {
            throw new HireadroidAuthException(e2);
        } catch (OAuthProblemException e3) {
            throw new DroidinOAuthProblemException(e3);
        } catch (OAuthException e4) {
            throw new HireadroidAuthException(e4);
        } catch (Exception e5) {
            throw new HireadroidAuthException(e5);
        }
    }

    public String[] authorize(List<OAuth.Parameter> params) {
        try {
            for (OAuth.Parameter parameter : params) {
                if (OAuth.OAUTH_TOKEN.equals(parameter.getKey())) {
                    this.accessor.requestToken = parameter.getValue();
                }
                if (OAuth.OAUTH_TOKEN_SECRET.equals(parameter.getKey())) {
                    this.accessor.tokenSecret = parameter.getValue();
                }
            }
            OAuthMessage message = this.client.getAccessToken(this.accessor, "GET", params);
            if (this.accessor.accessToken == null) {
                OAuthProblemException e = new OAuthProblemException(OAuth.Problems.PARAMETER_ABSENT);
                e.setParameter(OAuth.Problems.OAUTH_PARAMETERS_ABSENT, OAuth.OAUTH_TOKEN);
                e.getParameters().putAll(message.getDump());
                throw new HireadroidAuthException(e);
            }
            return new String[]{this.accessor.accessToken, this.accessor.tokenSecret};
        } catch (IOException e2) {
            throw new HireadroidAuthException(e2);
        } catch (OAuthProblemException e3) {
            throw new DroidinOAuthProblemException(e3);
        } catch (OAuthException e4) {
            throw new HireadroidAuthException(e4);
        } catch (URISyntaxException e5) {
            throw new HireadroidAuthException(e5);
        } catch (Exception e6) {
            throw new HireadroidAuthException(e6);
        }
    }

    public void init(String authToken, String authSecret) {
        this.accessor.accessToken = authToken;
        this.accessor.tokenSecret = authSecret;
    }

    public Connector.SiteResponse post(String url, String payload, boolean asJson, boolean zipped) {
        return new JavaOAuthResponseImpl(invoke("POST", url, OAuth.newList(new String[0]), payload, true, true, asJson, zipped));
    }

    public Connector.SiteResponse put(String url, String payload, boolean asJson, boolean zipped) {
        return new JavaOAuthResponseImpl(invoke("PUT", url, OAuth.newList(new String[0]), payload, true, true, asJson, zipped));
    }

    public int signOut() {
        return 0;
    }

    public Connector.SiteResponse get(String url, List<OAuth.Parameter> headers) {
        try {
            OAuthMessage request = this.accessor.newRequestMessage("GET", url, OAuth.newList(new String[0]));
            Object accepted = this.accessor.consumer.getProperty("HTTP.header.Accept-Encoding");
            if (accepted != null) {
                request.getHeaders().add(new OAuth.Parameter(HttpMessage.ACCEPT_ENCODING, accepted.toString()));
            }
            if (headers != null) {
                for (OAuth.Parameter parameter : headers) {
                    request.getHeaders().add(parameter);
                }
            }
            return new JavaOAuthResponseImpl(this.client.invoke(request, ParameterStyle.BODY));
        } catch (IOException e) {
            throw new HireadroidAuthException(e);
        } catch (OAuthProblemException e2) {
            throw new DroidinOAuthProblemException(e2);
        } catch (OAuthException e3) {
            throw new HireadroidAuthException(e3);
        } catch (URISyntaxException e4) {
            throw new HireadroidAuthException(e4);
        } catch (OutOfMemoryError e5) {
            throw new HireadroidAuthException("Not enough memory", e5);
        }
    }

    private OAuthMessage invoke(String method, String url, List<OAuth.Parameter> params, String payload, boolean doInvoke, boolean retry, boolean asJson, boolean zipped) {
        OAuthMessage request;
        if (payload == null) {
            try {
                request = this.accessor.newRequestMessage(method, url, params);
            } catch (ClientProtocolException e) {
                ClientProtocolException e2 = e;
                if (retry) {
                    return invoke(method, url, params, payload, doInvoke, false, asJson, zipped);
                }
                throw new DroidinOAuthProblemException(e2);
            } catch (IOException e3) {
                throw new DroidinOAuthProblemException(e3);
            } catch (URISyntaxException e4) {
                throw new DroidinOAuthProblemException(e4);
            } catch (OAuthProblemException e5) {
                throw new DroidinOAuthProblemException(e5);
            } catch (OAuthException e6) {
                throw new DroidinOAuthProblemException(e6);
            } catch (Exception e7) {
                throw new DroidinOAuthProblemException(e7);
            }
        } else {
            request = this.accessor.newRequestMessage(method, url, params, new ByteArrayInputStream(payload.getBytes(OAuth.ENCODING)));
        }
        Object accepted = this.accessor.consumer.getProperty("HTTP.header.Accept-Encoding");
        List<Map.Entry<String, String>> headers = request.getHeaders();
        if (asJson) {
            headers.addAll(getJsonHeaders(zipped));
        }
        if (accepted != null) {
            headers.add(new OAuth.Parameter(HttpMessage.ACCEPT_ENCODING, accepted.toString()));
        }
        return doInvoke ? this.client.invoke(request, ParameterStyle.BODY) : this.client.access(request, ParameterStyle.BODY);
    }

    public List<OAuth.Parameter> getJsonHeaders(boolean zipped) {
        List<OAuth.Parameter> headers = OAuth.newList(new String[0]);
        headers.add(new OAuth.Parameter("x-li-format", "json"));
        headers.add(new OAuth.Parameter(HttpMessage.CONTENT_TYPE, "application/json"));
        if (zipped) {
            headers.add(new OAuth.Parameter(HttpMessage.ACCEPT_ENCODING, HttpMessageDecoder.GZIP));
        }
        return headers;
    }
}
