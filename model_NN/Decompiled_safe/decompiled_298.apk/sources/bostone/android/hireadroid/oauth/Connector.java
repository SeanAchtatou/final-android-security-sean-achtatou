package bostone.android.hireadroid.oauth;

import java.io.InputStream;
import java.util.List;
import java.util.Map;
import net.oauth.OAuth;

public interface Connector {
    public static final String ACCESS_TOKEN_URL = "https://api.linkedin.com/uas/oauth/accessToken";
    public static final String AUTHORIZE_URL = "https://api.linkedin.com/uas/oauth/authorize";
    public static final String CALLBACK = "http://hireadroid.com/login";
    public static final String CONSUMER_KEY = "AX95WHD6b76XJz9DuSs9HzAx55jpHjVnQOxUuA1AK5RWPmV5tboFT6BE8RqiMezC";
    public static final String CONSUMER_SECRET = "Qs-GvcJmJumwkpIsvqMd8c09a66y2f1ufHSYExv1ZseY-HyGGLp-Aj9YwZvGlgA6";
    public static final String REQUEST_TOKEN_URL = "https://api.linkedin.com/uas/oauth/requestToken";
    public static final String SIGN_OUT_URL = "https://api.linkedin.com/uas/oauth/invalidateToken";

    public interface SiteResponse {
        String getBody();

        int getCode();

        String getHeader(String str);

        Map<String, String> getHeaders();

        InputStream getStream();
    }

    String authenticate();

    String[] authorize(List<OAuth.Parameter> list);

    SiteResponse get(String str, List<OAuth.Parameter> list);

    List<OAuth.Parameter> getJsonHeaders(boolean z);

    void init(String str, String str2);

    SiteResponse post(String str, String str2, boolean z, boolean z2);

    SiteResponse put(String str, String str2, boolean z, boolean z2);

    int signOut();
}
