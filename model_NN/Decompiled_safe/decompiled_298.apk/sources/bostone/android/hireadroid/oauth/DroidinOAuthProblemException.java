package bostone.android.hireadroid.oauth;

import bostone.android.hireadroid.HireadroidAuthException;
import bostone.android.hireadroid.utils.Utils;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DroidinOAuthProblemException extends HireadroidAuthException {
    private static final Pattern pttr = Pattern.compile(".*<status>(\\d\\d\\d)</status>.*<message>(.+?)</message>.*", 32);
    private static final long serialVersionUID = 8953463856902803989L;
    public int code;
    public String message;

    public DroidinOAuthProblemException(Throwable throwable) {
        super(throwable);
        Matcher m = pttr.matcher(throwable.toString());
        if (m.find()) {
            try {
                this.code = Integer.parseInt(m.group(1));
            } catch (NumberFormatException e) {
                this.code = 0;
            }
            this.message = m.group(2);
        }
        if (Utils.isEmpty(this.message)) {
            this.message = throwable.getMessage();
        }
    }

    public String getMessage() {
        if (Utils.isEmpty(this.message)) {
            return super.getMessage();
        }
        return this.message;
    }
}
