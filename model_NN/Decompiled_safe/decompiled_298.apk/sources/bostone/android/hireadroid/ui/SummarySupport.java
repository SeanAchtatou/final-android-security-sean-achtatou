package bostone.android.hireadroid.ui;

import bostone.android.hireadroid.model.SearchItem;

public interface SummarySupport {
    void preloadSummary(String str);

    boolean trackRead();

    void viewSummary(SearchItem searchItem);
}
