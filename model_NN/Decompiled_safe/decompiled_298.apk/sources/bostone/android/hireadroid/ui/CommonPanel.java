package bostone.android.hireadroid.ui;

import android.content.Context;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import bostone.android.hireadroid.R;

public class CommonPanel {
    Button button;
    final View container;
    final Context context;
    ProgressBar spinner;
    public TextView text;

    public CommonPanel(View container2, Context context2) {
        this.container = container2;
        this.text = (TextView) container2.findViewById(R.id.infoTxt);
        this.button = (Button) container2.findViewById(R.id.infoBtn);
        this.spinner = (ProgressBar) container2.findViewById(R.id.infoProgress);
        this.context = context2;
    }

    public void showError(CharSequence msg, View.OnClickListener listener) {
        setVisibility(0);
        this.text.setText(Html.fromHtml((String) msg));
        this.button.setVisibility(0);
        this.button.setOnClickListener(listener);
        this.spinner.setVisibility(8);
    }

    public void showInfoMessage(CharSequence msg, boolean showSpinner) {
        int i = 0;
        setVisibility(0);
        this.text.setVisibility(0);
        this.text.setText(msg);
        ProgressBar progressBar = this.spinner;
        if (!showSpinner) {
            i = 8;
        }
        progressBar.setVisibility(i);
        this.button.setVisibility(8);
    }

    public void done() {
        this.button.setVisibility(8);
        this.spinner.setVisibility(8);
    }

    public void done(CharSequence msg) {
        done();
        this.text.setText(msg);
    }

    public int getVisibility() {
        return this.container.getVisibility();
    }

    public void setVisibility(int visibility) {
        this.text.setVisibility(0);
        this.container.setVisibility(visibility);
    }

    public boolean isShown() {
        return this.container.isShown();
    }
}
