package bostone.android.hireadroid.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;

public class JobSearchListView extends PaginatableListView {
    public JobSearchListView(Context context) {
        super(context);
    }

    public JobSearchListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public JobSearchListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void init(Context context) {
        super.init(context);
        setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                ItemHolder h = (ItemHolder) view.getTag();
                if (h != null) {
                    h.flipBody();
                }
            }
        });
    }
}
