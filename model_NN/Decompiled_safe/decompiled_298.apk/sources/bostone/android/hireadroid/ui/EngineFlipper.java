package bostone.android.hireadroid.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ViewFlipper;

public class EngineFlipper extends ViewFlipper {
    public EngineFlipper(Context context) {
        super(context);
    }

    public EngineFlipper(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        try {
            super.onDetachedFromWindow();
        } catch (IllegalArgumentException e) {
            stopFlipping();
        }
    }
}
