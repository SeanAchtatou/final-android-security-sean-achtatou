package bostone.android.hireadroid.ui;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import bostone.android.hireadroid.R;
import bostone.android.hireadroid.dao.ItemDao;
import bostone.android.hireadroid.dao.SearchItemsDbHelper;
import bostone.android.hireadroid.model.SearchHeader;
import bostone.android.hireadroid.model.SearchItem;
import bostone.android.hireadroid.model.SearchResult;
import bostone.android.hireadroid.provider.SearchItemsProvider;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JobSearchResultsAdapter extends BaseAdapter {
    final Activity activity;
    final String engine;
    final Map<String, String> favorites = new HashMap();
    public SearchHeader header;
    public List<SearchItem> items;

    public JobSearchResultsAdapter(Activity activity2, String engine2) {
        this.activity = activity2;
        this.engine = engine2;
        Cursor cursor = activity2.managedQuery(SearchItemsProvider.CONTENT_URI, new String[]{SearchItemsDbHelper.ItemColumns.JOB_ID, SearchItemsDbHelper.ItemColumns.NOTE}, "engine=? AND favorite=?", new String[]{engine2, "1"}, null);
        while (cursor.moveToNext()) {
            this.favorites.put(cursor.getString(0), cursor.getString(1));
        }
    }

    public int getCount() {
        if (this.items == null) {
            return 0;
        }
        return this.items.size();
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public SearchItem getItem(int position) {
        if (this.items == null) {
            return null;
        }
        return this.items.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    /* Debug info: failed to restart local var, previous not found, register: 5 */
    public View getView(int position, View view, ViewGroup parent) {
        ItemHolder h;
        if (view == null) {
            view = this.activity.getLayoutInflater().inflate((int) R.layout.job_search_item, (ViewGroup) null);
            h = new ItemHolder(this.activity, view, new FavoritesSupport() {
                public void remove(SearchItem item) {
                    item.favorite = false;
                    ItemDao.toggleFavorite(JobSearchResultsAdapter.this.activity.getContentResolver(), item);
                    JobSearchResultsAdapter.this.favorites.remove(item.jobId);
                }

                public boolean hasId(SearchItem item) {
                    return JobSearchResultsAdapter.this.favorites.containsKey(item.jobId);
                }

                public void add(SearchItem item) {
                    item.favorite = true;
                    ItemDao.toggleFavorite(JobSearchResultsAdapter.this.activity.getContentResolver(), item);
                    JobSearchResultsAdapter.this.favorites.put(item.jobId, null);
                }

                public void setNote(SearchItem item) {
                    ContentValues values = new ContentValues(1);
                    values.put(SearchItemsDbHelper.ItemColumns.NOTE, item.comment);
                    ItemDao.update(JobSearchResultsAdapter.this.activity.getContentResolver(), item.jobId, values);
                    JobSearchResultsAdapter.this.favorites.put(item.jobId, item.comment);
                }

                /* Debug info: failed to restart local var, previous not found, register: 2 */
                public String getNote(SearchItem item) {
                    if (JobSearchResultsAdapter.this.favorites.containsKey(item.jobId)) {
                        return JobSearchResultsAdapter.this.favorites.get(item.jobId);
                    }
                    return null;
                }
            }, (SummarySupport) this.activity);
            view.setTag(h);
        } else {
            h = (ItemHolder) view.getTag();
        }
        SearchItem item = getItem(position);
        if (item != null) {
            if (this.favorites.containsKey(item.jobId)) {
                item.favorite = true;
                item.comment = this.favorites.get(item.jobId);
            }
            if (item.preferred) {
                view.setBackgroundResource(R.drawable.list_background_preferred);
            } else {
                view.setBackgroundResource(17301602);
            }
        }
        h.reset(item);
        return view;
    }

    public void addResult(SearchResult result) {
        this.header = result.header;
        if (this.items == null) {
            this.items = new ArrayList();
        }
        this.items.addAll(result.items);
        notifyDataSetChanged();
    }

    public void reset() {
        if (this.header != null) {
            this.header.reset();
        }
        if (this.items != null) {
            this.items.clear();
        }
    }
}
