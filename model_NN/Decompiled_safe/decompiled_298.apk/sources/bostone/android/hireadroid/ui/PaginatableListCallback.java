package bostone.android.hireadroid.ui;

import android.widget.ListView;

public interface PaginatableListCallback {
    public static final String END_OF_LIST = "End of list";

    boolean isDone();

    void onLastItemDisplayed(ListView listView);

    void onScrollChanged(int i, ListView listView);
}
