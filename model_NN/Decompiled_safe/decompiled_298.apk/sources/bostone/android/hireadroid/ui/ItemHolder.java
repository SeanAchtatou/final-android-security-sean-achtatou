package bostone.android.hireadroid.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import bostone.android.hireadroid.R;
import bostone.android.hireadroid.activity.MonitoredActivity;
import bostone.android.hireadroid.dao.ItemDao;
import bostone.android.hireadroid.dao.SearchItemsDbHelper;
import bostone.android.hireadroid.model.SearchItem;
import bostone.android.hireadroid.utils.Utils;
import java.util.Date;

public class ItemHolder {
    private static final String TAG = "ItemHolder";
    public RelativeLayout body;
    CheckBox chkFav;
    TextView cn;
    TextView comment;
    final Context context;
    View controls;
    TextView dp;
    TextView e;
    final FavoritesSupport favSupport;
    public SearchItem item;
    View.OnClickListener itemListener;
    TextView jobSrc;
    TextView jt;
    TextView loc;
    public View shareBtt;
    TextView src;
    ImageView star;
    public final SummarySupport summaryHandler;
    private final boolean trackRead;
    View view;

    public ItemHolder(Context context2, View view2, FavoritesSupport favHandler, SummarySupport summaryHandler2) {
        this.context = context2;
        this.view = view2;
        this.favSupport = favHandler;
        this.summaryHandler = summaryHandler2;
        this.trackRead = summaryHandler2.trackRead();
        this.jt = (TextView) view2.findViewById(R.id.jt);
        this.dp = (TextView) view2.findViewById(R.id.dp);
        this.cn = (TextView) view2.findViewById(R.id.cn);
        this.loc = (TextView) view2.findViewById(R.id.loc);
        this.star = (ImageView) view2.findViewById(R.id.star);
        this.body = (RelativeLayout) view2.findViewById(R.id.itemBody);
        if (this.body != null) {
            this.e = (TextView) this.body.findViewById(R.id.e);
            this.jobSrc = (TextView) this.body.findViewById(R.id.jobSrc);
            this.src = (TextView) this.body.findViewById(R.id.src);
            this.controls = this.body.findViewById(R.id.controls);
            this.comment = (TextView) this.body.findViewById(R.id.favComment);
            setControlsListeners();
        }
    }

    /* access modifiers changed from: package-private */
    public void setControlsListeners() {
        this.controls.findViewById(R.id.bttView).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MonitoredActivity.event("View job", "Engine", ItemHolder.this.item.engine);
                ItemHolder.this.summaryHandler.viewSummary(ItemHolder.this.item);
            }
        });
        View.OnClickListener onShareListener = new View.OnClickListener() {
            public void onClick(View v) {
                Utils.share((Activity) ItemHolder.this.context, ItemHolder.this.item);
            }
        };
        this.shareBtt = this.controls.findViewById(R.id.bttShare);
        this.shareBtt.setOnClickListener(onShareListener);
        this.chkFav = (CheckBox) this.controls.findViewById(R.id.chkFav);
        this.chkFav.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton btt, boolean checked) {
                if (checked != ItemHolder.this.item.favorite) {
                    ItemHolder.this.onToggleFavorite(checked);
                }
            }
        });
        this.comment.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ItemHolder.this.onEditNote();
            }
        });
    }

    public void reset(SearchItem item2) {
        int i;
        this.item = item2;
        this.jt.setText(item2.jobTitle);
        this.dp.setText(Utils.prettyDate(item2.postedDate));
        this.cn.setText(item2.company);
        this.loc.setText(item2.location.name);
        if (!item2.favorite) {
            item2.favorite = this.favSupport.hasId(item2);
        }
        this.star.setVisibility(item2.favorite ? 0 : 8);
        if (this.body != null) {
            TextView textView = this.comment;
            if (item2.favorite) {
                i = 0;
            } else {
                i = 8;
            }
            textView.setVisibility(i);
            this.comment.setText((CharSequence) null);
            this.src.setText(item2.listingSource);
            this.e.setText(Html.fromHtml(item2.snippet));
            if (this.chkFav.isChecked() != item2.favorite) {
                this.chkFav.setChecked(item2.favorite);
            }
            if (this.body != null) {
                if (item2.favorite) {
                    this.body.setBackgroundResource(R.drawable.item_details_bgr_favorite);
                } else {
                    this.body.setBackgroundResource(R.drawable.item_details_bgr_normal);
                }
            }
            if (item2.favorite) {
                if (Utils.isEmpty(item2.comment)) {
                    item2.comment = this.favSupport.getNote(item2);
                }
                this.comment.setText(item2.comment);
            }
            this.view.setPadding(4, 4, 4, 4);
            if (item2.tsRead <= 0 || !this.trackRead) {
                this.jt.setTypeface(Typeface.DEFAULT_BOLD);
            } else {
                this.jt.setTypeface(Typeface.DEFAULT);
            }
            if (item2.viewed) {
                showBody();
            } else {
                hideBody();
            }
        }
    }

    /* access modifiers changed from: private */
    public void onEditNote() {
        final EditText input = new EditText(this.context);
        input.setText(this.comment.getText());
        if ((this.context instanceof Activity) && !((Activity) this.context).isFinishing()) {
            new AlertDialog.Builder(this.context).setTitle("Add profile note").setView(input).setNegativeButton("Cancel", (DialogInterface.OnClickListener) null).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    ItemHolder.this.onSaveNote(input.getText());
                }
            }).show();
        }
        input.requestFocus();
    }

    /* access modifiers changed from: package-private */
    public void onSaveNote(Editable text) {
        ContentValues values = new ContentValues(1);
        String note = text == null ? null : text.toString();
        values.put(SearchItemsDbHelper.ItemColumns.NOTE, note);
        this.item.comment = note;
        try {
            ItemDao.update(this.context.getContentResolver(), this.item.jobId, values);
            this.favSupport.setNote(this.item);
        } catch (Exception e2) {
            Exception e3 = e2;
            Log.e(TAG, "Failed to update favorite note: " + ((Object) text), e3);
            Toast.makeText(this.context, "Failed to update favorite note:\n" + Utils.unwrap(e3), 1).show();
        }
    }

    public void flipBody() {
        if (this.item.viewed) {
            hideBody();
        } else {
            showBody();
        }
        this.view.setPadding(4, 4, 4, 4);
    }

    /* access modifiers changed from: package-private */
    public void showBody() {
        this.e.setMaxLines(20);
        this.e.setSingleLine(false);
        this.e.setEllipsize(null);
        this.jobSrc.setVisibility(0);
        this.src.setVisibility(0);
        this.controls.setVisibility(0);
        this.item.viewed = true;
        this.view.setClickable(true);
        this.view.setFocusable(true);
        this.view.setOnClickListener(getItemListener());
        if (this.item.preferred) {
            this.view.setBackgroundResource(R.drawable.list_background_preferred);
        } else {
            this.view.setBackgroundResource(17301602);
        }
        this.summaryHandler.preloadSummary(this.item.listingUrl);
        if (Utils.isEmpty(this.item.shortUrl)) {
            Utils.fetchBitlyUrl(this.context, this.item);
        }
        if (this.item.favorite) {
            this.comment.setVisibility(0);
            this.comment.setClickable(true);
            this.comment.setFocusable(true);
        } else {
            this.comment.setVisibility(8);
        }
        if (this.item.tsRead == 0) {
            this.item.tsRead = new Date().getTime();
            ItemDao.markRead(this.context.getContentResolver(), this.item);
        }
        if (this.trackRead) {
            this.jt.setTypeface(Typeface.DEFAULT);
        }
    }

    /* access modifiers changed from: package-private */
    public void hideBody() {
        this.e.setMaxLines(1);
        this.e.setSingleLine();
        this.e.setEllipsize(TextUtils.TruncateAt.END);
        this.jobSrc.setVisibility(8);
        this.src.setVisibility(8);
        this.controls.setVisibility(8);
        this.item.viewed = false;
        this.view.setClickable(false);
        this.view.setFocusable(false);
        if (!this.item.favorite || !Utils.isNotEmpty(this.item.comment)) {
            this.comment.setText((CharSequence) null);
            this.comment.setVisibility(8);
        } else {
            this.comment.setText(this.item.comment);
            this.comment.setVisibility(0);
        }
        this.comment.setClickable(false);
        this.comment.setFocusable(false);
    }

    /* access modifiers changed from: package-private */
    public View.OnClickListener getItemListener() {
        if (this.itemListener == null) {
            this.itemListener = new View.OnClickListener() {
                public void onClick(View view) {
                    ItemHolder.this.flipBody();
                }
            };
        }
        return this.itemListener;
    }

    /* access modifiers changed from: private */
    public void onToggleFavorite(boolean checked) {
        int i;
        int i2;
        int i3;
        this.item.favorite = checked;
        MonitoredActivity.event("Job Action", "Favorite", checked ? "Selected" : "Unselected");
        if (checked) {
            try {
                this.favSupport.add(this.item);
            } catch (Exception e2) {
                Exception e3 = e2;
                Toast.makeText(this.context, "Failed to toggle favorite:\n" + Utils.unwrap(e3), 1).show();
                Log.e(TAG, "Failed to toggle favorite", e3);
                this.chkFav.setChecked(!checked);
                return;
            }
        } else {
            this.favSupport.remove(this.item);
        }
        ImageView imageView = this.star;
        if (checked) {
            i = 0;
        } else {
            i = 8;
        }
        imageView.setVisibility(i);
        TextView textView = this.comment;
        if (checked) {
            i2 = 0;
        } else {
            i2 = 8;
        }
        textView.setVisibility(i2);
        this.comment.setClickable(checked);
        if (this.body != null) {
            RelativeLayout relativeLayout = this.body;
            if (checked) {
                i3 = R.drawable.item_details_bgr_favorite;
            } else {
                i3 = R.drawable.item_details_bgr_normal;
            }
            relativeLayout.setBackgroundResource(i3);
        }
    }
}
