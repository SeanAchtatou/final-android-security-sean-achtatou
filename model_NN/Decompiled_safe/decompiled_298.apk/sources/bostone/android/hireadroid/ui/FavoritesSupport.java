package bostone.android.hireadroid.ui;

import bostone.android.hireadroid.model.SearchItem;

public interface FavoritesSupport {
    void add(SearchItem searchItem);

    String getNote(SearchItem searchItem);

    boolean hasId(SearchItem searchItem);

    void remove(SearchItem searchItem);

    void setNote(SearchItem searchItem);
}
