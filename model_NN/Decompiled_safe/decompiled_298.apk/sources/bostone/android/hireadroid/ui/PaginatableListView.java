package bostone.android.hireadroid.ui;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.Toast;
import bostone.android.hireadroid.R;

public class PaginatableListView extends ListView {
    private static final String TAG = "PaginatableListView";
    private int attempts;
    /* access modifiers changed from: private */
    public String endOfList;
    public boolean filledUp;
    public CommonPanel footer;

    public PaginatableListView(Context context) {
        super(context);
        init(context);
    }

    public PaginatableListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    public PaginatableListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public void setOnScrollCallback(PaginatableListCallback callback) {
        setOnScrollListener(new ScrollListener(callback));
    }

    public void init(Context context) {
        try {
            View v = LayoutInflater.from(context).inflate((int) R.layout.common_header, (ViewGroup) null);
            this.footer = new CommonPanel(v, context);
            this.endOfList = context.getResources().getString(R.string.end_of_list);
            addFooterView(v);
        } catch (OutOfMemoryError e) {
            Log.e(TAG, "Not enough memory on device", e);
            System.gc();
            int i = this.attempts + 1;
            this.attempts = i;
            if (i < 5) {
                init(context);
                return;
            }
            Toast.makeText(context, "Unable to display results, not enough memory left on the device", 1).show();
            ((Activity) context).finish();
        }
    }

    public class ScrollListener implements AbsListView.OnScrollListener {
        private final PaginatableListCallback callback;

        public ScrollListener(PaginatableListCallback callback2) {
            this.callback = callback2;
        }

        public void onScroll(AbsListView view, int first, int visible, int total) {
            boolean done = this.callback.isDone();
            if (!done || PaginatableListView.this.footer == null || visible >= total) {
                Log.d(PaginatableListView.TAG, "Will not scroll. Done: " + done + ", footer: " + PaginatableListView.this.footer + ", visible: " + visible + ", total: " + total);
                return;
            }
            if (PaginatableListView.this.footer.getVisibility() == 8) {
                PaginatableListView.this.footer.setVisibility(0);
            }
            if (PaginatableListView.this.footer.isShown()) {
                Log.d(PaginatableListView.TAG, "Footer detected");
                if (!PaginatableListView.this.filledUp) {
                    PaginatableListView.this.footer.showInfoMessage("Please wait...", true);
                } else if (!PaginatableListView.this.endOfList.equals(PaginatableListView.this.footer.text.getText())) {
                    PaginatableListView.this.footer.done(PaginatableListView.this.endOfList);
                }
                if (!PaginatableListCallback.END_OF_LIST.equals(PaginatableListView.this.footer.text)) {
                    this.callback.onLastItemDisplayed(PaginatableListView.this);
                }
            }
        }

        public void onScrollStateChanged(AbsListView view, int scrollState) {
            this.callback.onScrollChanged(scrollState, PaginatableListView.this);
        }
    }
}
