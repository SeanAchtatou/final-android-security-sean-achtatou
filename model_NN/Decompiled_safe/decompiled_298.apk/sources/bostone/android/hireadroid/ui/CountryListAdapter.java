package bostone.android.hireadroid.ui;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import bostone.android.hireadroid.R;
import bostone.android.hireadroid.model.Country;

public class CountryListAdapter extends ArrayAdapter<String> {
    public CountryListAdapter(Context context) {
        super(context, (int) R.layout.country_row, Country.ISO_CODES);
    }

    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        TextView view = (TextView) super.getDropDownView(position, convertView, parent);
        view.setCompoundDrawablesWithIntrinsicBounds(0, 0, Country.ICONS[position], 0);
        return view;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        TextView view;
        try {
            view = (TextView) super.getView(position, convertView, parent);
        } catch (Exception e) {
            if (convertView != null) {
                view = (TextView) convertView;
            } else {
                view = new TextView(getContext());
            }
        }
        view.setText((CharSequence) null);
        return view;
    }
}
