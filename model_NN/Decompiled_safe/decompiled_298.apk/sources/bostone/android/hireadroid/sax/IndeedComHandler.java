package bostone.android.hireadroid.sax;

import android.text.TextUtils;
import android.util.Log;
import bostone.android.hireadroid.dao.SearchItemsDbHelper;
import bostone.android.hireadroid.engine.IndeedDotComEngine;
import bostone.android.hireadroid.model.SearchHeader;
import bostone.android.hireadroid.model.SearchItem;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

public class IndeedComHandler extends AbstractHandler {
    static final Pattern CLICK_PTR = Pattern.compile(".+\\'(.+?)\\'");
    static final HashMap<String, String> HEADER_MAPPER = new HashMap<>(3);
    static final HashMap<String, String> ITEM_MAPPER = new HashMap<>(8);
    static final String RESULT = "result";
    private static final String TAG = "IndeedComHandler";
    static final List<String> entryKeys = Arrays.asList("jobtitle", SearchItemsDbHelper.ItemColumns.COMPANY, SearchItemsDbHelper.ItemColumns.LOC_CITY, "state", SearchItemsDbHelper.ItemColumns.LOC_COUNTRY, "formattedLocation", "source", "date", SearchItemsDbHelper.ItemColumns.SNIPPET, SearchItem.LISTING_URL, "onmousedown", SearchItemsDbHelper.ItemColumns.LOC_LATITUDE, SearchItemsDbHelper.ItemColumns.LOC_LONGTITUDE, "jobkey");
    static final List<String> headerKeys = Arrays.asList("query", "location", "dupefilter", "highlight", "totalresults", "start", "end");
    private StringBuffer collector;

    static {
        HEADER_MAPPER.put("totalresults", SearchHeader.NUM_TOTAL_RESULTS);
        HEADER_MAPPER.put("start", SearchHeader.START_INDEX);
        HEADER_MAPPER.put("query", SearchHeader.TITLE);
        ITEM_MAPPER.put(SearchItemsDbHelper.ItemColumns.COMPANY, SearchItem.COMPANY);
        ITEM_MAPPER.put("date", SearchItem.DATE_POSTED);
        ITEM_MAPPER.put("jobtitle", SearchItem.JOB_TITLE);
        ITEM_MAPPER.put("jobkey", SearchItem.JOB_ID);
        ITEM_MAPPER.put("source", SearchItem.LISTING_SOURCE);
        ITEM_MAPPER.put(SearchItem.LISTING_URL, SearchItem.LISTING_URL);
        ITEM_MAPPER.put("formattedLocation", SearchItem.LOCATION);
        ITEM_MAPPER.put(SearchItemsDbHelper.ItemColumns.SNIPPET, SearchItem.SNIPPET);
    }

    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (headerKeys.contains(localName) || entryKeys.contains(localName)) {
            this.key = localName;
            this.collector = new StringBuffer();
        } else if (RESULT.equals(localName)) {
            this.item = new SearchItem();
            this.item.listingSource = IndeedDotComEngine.TYPE;
            this.item.engine = IndeedDotComEngine.TYPE;
        }
    }

    public void characters(char[] ch, int start, int length) throws SAXException {
        if (this.key != null && ch != null) {
            String value = new String(ch, start, length);
            if (!TextUtils.isEmpty(value)) {
                this.collector.append(value.trim());
            }
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 4 */
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (RESULT.equals(localName) && verify(this.item)) {
            if (this.item.jobId == null) {
                this.item.jobId = generateJobId(this.item);
            }
            this.result.items.add(this.item);
        } else if (HEADER_MAPPER.containsKey(this.key)) {
            this.result.header.set(HEADER_MAPPER.get(this.key), this.collector.toString());
        } else if (ITEM_MAPPER.containsKey(this.key)) {
            this.item.set(ITEM_MAPPER.get(this.key), this.collector.toString());
        } else if (this.key != null && this.key.equals("onmousedown")) {
            addClickInfoToUrl(this.collector.toString());
        }
    }

    private boolean verify(SearchItem item) {
        boolean valid = !TextUtils.isEmpty(item.jobTitle) && !TextUtils.isEmpty(item.snippet) && !TextUtils.isEmpty(item.listingSource) && !TextUtils.isEmpty(item.listingUrl) && item.postedDate > 0 && item.location != null && !TextUtils.isEmpty(item.location.name);
        if (!valid) {
            Log.d(TAG, "Invalid item " + item);
        }
        return valid;
    }

    private void addClickInfoToUrl(String val) {
        if (this.item != null && this.item.listingUrl != null && this.item.listingUrl.indexOf("&jsa=") <= 0) {
            Matcher m = CLICK_PTR.matcher(val);
            if (m.find()) {
                SearchItem searchItem = this.item;
                searchItem.listingUrl = String.valueOf(searchItem.listingUrl) + "&jsa=" + m.group(1);
            }
            if (this.item.listingUrl.indexOf("&inchal") == -1) {
                SearchItem searchItem2 = this.item;
                searchItem2.listingUrl = String.valueOf(searchItem2.listingUrl) + "&inchal=apiresults";
            }
        }
    }

    public void endDocument() throws SAXException {
        super.endDocument();
        this.result.header.numViewableResults = this.result.header.numTotalResults;
        this.result.header.title = String.valueOf(this.result.header.title) + " Jobs";
    }
}
