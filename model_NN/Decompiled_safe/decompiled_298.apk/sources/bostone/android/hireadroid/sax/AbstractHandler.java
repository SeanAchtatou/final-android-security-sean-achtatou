package bostone.android.hireadroid.sax;

import bostone.android.hireadroid.model.SearchItem;
import bostone.android.hireadroid.model.SearchResult;
import bostone.android.hireadroid.utils.Utils;
import org.xml.sax.helpers.DefaultHandler;

public abstract class AbstractHandler extends DefaultHandler {
    protected SearchItem item;
    protected String key;
    public SearchResult result = new SearchResult();

    protected AbstractHandler() {
    }

    public void reset() {
        this.result.reset();
    }

    public static AbstractHandler instanceOf(String url) {
        if (url == null) {
            throw new IllegalArgumentException("URL wasn't provided");
        } else if (url.contains("simplyhired.")) {
            return new SimplyHiredHandler();
        } else {
            if (url.contains("indeed.")) {
                return new IndeedComHandler();
            }
            if (url.contains("linkup.")) {
                return new LinkUpHandler();
            }
            if (url.contains("linkedin.")) {
                return new LinkedInHandler();
            }
            if (url.contains("careerbuilder.")) {
                return new CareerBuilderHandler();
            }
            if (url.contains("beyond.")) {
                return new BeyondDotComHandler();
            }
            throw new IllegalArgumentException("Unsupported URL " + url);
        }
    }

    /* access modifiers changed from: protected */
    public String generateJobId(SearchItem item2) {
        return Utils.compress(item2.toString());
    }
}
