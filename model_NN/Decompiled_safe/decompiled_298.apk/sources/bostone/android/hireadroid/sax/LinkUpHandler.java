package bostone.android.hireadroid.sax;

import android.text.TextUtils;
import android.util.Log;
import bostone.android.hireadroid.dao.SearchItemsDbHelper;
import bostone.android.hireadroid.model.SearchHeader;
import bostone.android.hireadroid.model.SearchItem;
import bostone.android.hireadroid.utils.Utils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

public class LinkUpHandler extends AbstractHandler {
    static final Pattern CLICK_PTR = Pattern.compile(".+\\'(.+?)\\'");
    static final Pattern DATE_PTR = Pattern.compile("\\w+, (.+\\d\\d\\d\\d) \\d\\d");
    static final HashMap<String, String> HEADER_MAPPER = new HashMap<>(3);
    static final HashMap<String, String> ITEM_MAPPER = new HashMap<>(8);
    static final String JOB = "job";
    private static final String TAG = "LinkUpHandler";
    static final List<String> entryKeys = Arrays.asList("job_title", "job_title_link", "job_company", "job_tag", "job_location", "job_zip", "job_date_added", "job_description", "job_hash");
    static final List<String> headerKeys = Arrays.asList("title", "total_jobs", "page", "keyword", "location", SearchItemsDbHelper.ItemColumns.COMPANY, "tags", "distance", "per_page", "time_frame", "sort", "message");
    private StringBuffer collector;
    private boolean preferred;
    private int preferredIndex;
    private List<String> warnings;

    static {
        HEADER_MAPPER.put("total_jobs", SearchHeader.NUM_TOTAL_RESULTS);
        HEADER_MAPPER.put("page", SearchHeader.START_INDEX);
        HEADER_MAPPER.put("title", SearchHeader.TITLE);
        ITEM_MAPPER.put("job_company", SearchItem.COMPANY);
        ITEM_MAPPER.put("job_date_added", SearchItem.DATE_POSTED);
        ITEM_MAPPER.put("job_title", SearchItem.JOB_TITLE);
        ITEM_MAPPER.put("job_hash", SearchItem.JOB_ID);
        ITEM_MAPPER.put("job_title_link", SearchItem.LISTING_URL);
        ITEM_MAPPER.put("job_location", SearchItem.LOCATION);
        ITEM_MAPPER.put("job_description", SearchItem.SNIPPET);
    }

    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (headerKeys.contains(localName) || entryKeys.contains(localName)) {
            this.key = localName;
            this.collector = new StringBuffer();
        } else if (JOB.equals(localName)) {
            this.item = new SearchItem();
            this.item.listingSource = "LinkUp.com";
            this.item.engine = "LinkUp.com";
        } else if ("warnings".equals(localName)) {
            this.warnings = new ArrayList();
        } else if ("sponsored_listings".equals(localName)) {
            this.preferred = true;
        }
    }

    public void characters(char[] ch, int start, int length) throws SAXException {
        if (this.key != null && ch != null) {
            String value = new String(ch, start, length);
            if (!TextUtils.isEmpty(value)) {
                this.collector.append(value.trim());
            }
        }
    }

    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (!JOB.equals(localName) || !verify(this.item)) {
            String m = this.collector.toString();
            if (HEADER_MAPPER.containsKey(this.key)) {
                this.result.header.set(HEADER_MAPPER.get(this.key), m);
            } else if (ITEM_MAPPER.containsKey(this.key)) {
                this.item.set(ITEM_MAPPER.get(this.key), m);
            } else if ("message".equals(localName)) {
                if (Utils.isNotEmpty(m) && !m.equals("No sponsored_listings to display") && !m.equals("No jobs to display")) {
                    this.warnings.add(m);
                }
            } else if ("warnings".equals(localName)) {
                this.result.header.messages = (String[]) this.warnings.toArray(new String[0]);
                this.warnings = null;
            } else if ("sponsored_listings".equals(localName)) {
                this.preferred = false;
            }
        } else {
            if (this.item.jobId == null) {
                this.item.jobId = generateJobId(this.item);
            }
            this.item.preferred = this.preferred;
            if (this.preferred) {
                List<SearchItem> list = this.result.items;
                int i = this.preferredIndex;
                this.preferredIndex = i + 1;
                list.add(i, this.item);
            } else {
                this.result.items.add(this.item);
            }
        }
        this.key = null;
        this.collector = new StringBuffer();
    }

    private boolean verify(SearchItem item) {
        boolean valid = !TextUtils.isEmpty(item.jobTitle) && !TextUtils.isEmpty(item.snippet) && !TextUtils.isEmpty(item.listingSource) && !TextUtils.isEmpty(item.listingUrl) && item.postedDate > 0 && item.location != null && !TextUtils.isEmpty(item.location.name);
        if (!valid) {
            Log.d(TAG, "Invalid item " + item);
        }
        return valid;
    }

    public void endDocument() throws SAXException {
        super.endDocument();
        this.result.header.numViewableResults = this.result.header.numTotalResults;
        this.result.header.startIndex = (this.result.header.startIndex * 20) - 20;
        this.preferredIndex = 0;
    }
}
