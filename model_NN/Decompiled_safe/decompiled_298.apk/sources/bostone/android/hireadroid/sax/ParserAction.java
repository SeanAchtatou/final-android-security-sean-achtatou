package bostone.android.hireadroid.sax;

import java.io.IOException;
import java.io.StringReader;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class ParserAction {
    DefaultHandler handler;
    String xml;

    public ParserAction(String xml2, DefaultHandler handler2) {
        this.xml = xml2;
        this.handler = handler2;
    }

    public void parse() throws ParserConfigurationException, SAXException, FactoryConfigurationError, IOException {
        SAXParserFactory.newInstance().newSAXParser().parse(new InputSource(new StringReader(this.xml)), this.handler);
    }
}
