package bostone.android.hireadroid.sax;

import bostone.android.hireadroid.dao.SearchItemsDbHelper;
import bostone.android.hireadroid.engine.LinkedInEngine;
import bostone.android.hireadroid.model.SearchItem;
import java.util.Date;
import java.util.GregorianCalendar;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class LinkedInHandler extends AbstractHandler {
    public void parse(String rawJson) throws JSONException {
        JSONObject json = new JSONObject(rawJson);
        if (json.has("jobs")) {
            JSONObject jobs = json.getJSONObject("jobs");
            if (jobs.has("_total")) {
                this.result.header.numTotalResults = jobs.getInt("_total");
            }
            if (jobs.has("_count")) {
                this.result.header.numReturnedResults = jobs.getInt("_count");
                this.result.header.numViewableResults = this.result.header.numReturnedResults;
            }
            if (jobs.has("_start")) {
                this.result.header.startIndex = jobs.getInt("_start");
            }
            if (this.result.header.numTotalResults > 0 && jobs.has("values")) {
                JSONArray ja = jobs.getJSONArray("values");
                for (int i = 0; i < ja.length(); i++) {
                    parseJob(ja.getJSONObject(i));
                }
            }
        }
    }

    private void parseJob(JSONObject job) throws JSONException {
        SearchItem item = new SearchItem();
        item.engine = LinkedInEngine.TYPE;
        item.listingSource = LinkedInEngine.TYPE;
        if (job.has("id")) {
            item.jobId = job.getString("id");
        }
        if (job.has("siteJobUrl")) {
            item.listingUrl = job.getString("siteJobUrl");
        }
        if (job.has("position")) {
            JSONObject position = job.getJSONObject("position");
            if (position.has("title")) {
                item.jobTitle = position.getString("title");
            }
        }
        if (job.has(SearchItemsDbHelper.ItemColumns.COMPANY)) {
            JSONObject company = job.getJSONObject(SearchItemsDbHelper.ItemColumns.COMPANY);
            if (company.has(SearchItemsDbHelper.ItemColumns.LOC_NAME)) {
                item.company = company.getString(SearchItemsDbHelper.ItemColumns.LOC_NAME);
            } else {
                item.company = "";
            }
        }
        item.listingSource = LinkedInEngine.TYPE;
        if (job.has("locationDescription")) {
            item.location.name = job.getString("locationDescription");
        }
        if (job.has("postingDate")) {
            JSONObject d = job.getJSONObject("postingDate");
            int day = d.getInt("day");
            item.postedDate = new GregorianCalendar(d.getInt("year"), d.getInt("month") - 1, day).getTimeInMillis();
            item.lastSeeingDate = new Date().getTime();
        }
        if (job.has("descriptionSnippet")) {
            item.snippet = job.getString("descriptionSnippet");
        }
        this.result.items.add(item);
    }
}
