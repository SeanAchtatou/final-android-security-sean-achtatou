package bostone.android.hireadroid.sax;

import bostone.android.hireadroid.engine.SimplyHiredEngine;
import bostone.android.hireadroid.model.Location;
import bostone.android.hireadroid.model.SearchHeader;
import bostone.android.hireadroid.model.SearchItem;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

public class SimplyHiredHandler extends AbstractHandler {
    static final Pattern JOB_ID_PTR = Pattern.compile(".*jobkey-(.+?)\\/.*");
    static List<String> entryKeys = Arrays.asList(SearchItem.JOB_TITLE, SearchItem.COMPANY, SearchItem.LISTING_SOURCE, SearchItem.LISTING_TYPE, SearchItem.LOCATION, SearchItem.DATE_LAST_SEEING, SearchItem.DATE_POSTED, SearchItem.SNIPPET, SearchItem.LISTING_TYPE);
    static List<String> headerKeys = Arrays.asList(SearchHeader.REQUEST, SearchHeader.TITLE, SearchHeader.DATE, SearchHeader.START_INDEX, SearchHeader.NUM_RETURNED_RESULTS, SearchHeader.NUM_TOTAL_RESULTS, SearchHeader.NUM_VIEWABLE_RESULTS);

    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (headerKeys.contains(localName)) {
            this.key = localName;
        } else if ("r".equals(localName)) {
            this.item = new SearchItem();
            this.item.engine = SimplyHiredEngine.TYPE;
        } else if (entryKeys.contains(localName)) {
            if (SearchItem.LOCATION.equals(localName)) {
                if (this.item.location == null) {
                    this.item.location = new Location();
                }
                this.item.location.set(attributes);
            } else if (SearchItem.LISTING_SOURCE.equals(localName)) {
                this.item.listingUrl = attributes.getValue(SearchItem.LISTING_URL);
                if (this.item.listingUrl.contains("?")) {
                    SearchItem searchItem = this.item;
                    searchItem.listingUrl = String.valueOf(searchItem.listingUrl) + "&uck=y";
                } else {
                    SearchItem searchItem2 = this.item;
                    searchItem2.listingUrl = String.valueOf(searchItem2.listingUrl) + "?uck=y";
                }
                Matcher m = JOB_ID_PTR.matcher(this.item.listingUrl);
                if (m.find()) {
                    this.item.jobId = m.group(1);
                }
            }
            this.key = localName;
        }
    }

    public void characters(char[] ch, int start, int length) throws SAXException {
        String value;
        if (this.key != null && ch != null && (value = new String(ch, start, length)) != null && value.trim().length() > 0) {
            if (headerKeys.contains(this.key)) {
                this.result.header.set(this.key, value);
            } else if (!entryKeys.contains(this.key)) {
            } else {
                if (this.key.equals(SearchItem.LISTING_TYPE)) {
                    this.item.preferred = "sponsored".equalsIgnoreCase(value);
                } else {
                    this.item.set(this.key, value);
                }
            }
        }
    }

    public void endElement(String uri, String localName, String qName) throws SAXException {
        if ("r".equals(localName)) {
            if (this.item.jobId == null) {
                this.item.jobId = generateJobId(this.item);
            }
            this.result.items.add(this.item);
        } else if (entryKeys.contains(localName) || headerKeys.contains(localName)) {
            this.key = null;
        }
    }
}
