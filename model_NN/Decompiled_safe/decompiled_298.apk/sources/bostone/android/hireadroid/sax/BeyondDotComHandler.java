package bostone.android.hireadroid.sax;

import android.text.TextUtils;
import android.util.Log;
import bostone.android.hireadroid.engine.BeyondDotComEngine;
import bostone.android.hireadroid.model.SearchHeader;
import bostone.android.hireadroid.model.SearchItem;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

public class BeyondDotComHandler extends AbstractHandler {
    static final HashMap<String, String> HEADER_MAPPER = new HashMap<>(1);
    static final HashMap<String, String> ITEM_MAPPER = new HashMap<>(8);
    static final String JOB = "Item";
    private static final Pattern JOB_ID_PTR = Pattern.compile(".+id=(.*?)\\&.*");
    private static final String MODIFIED = "Modified";
    private static final String TAG = "BeyondDotComHandler";
    static final List<String> entryKeys = Arrays.asList("Title", "ShortDescription", "Location", "CompanyName", "ApplyURL", MODIFIED);
    static final List<String> headerKeys = Arrays.asList("ResultCount");
    private StringBuffer collector;
    private List<String> warnings;

    static {
        HEADER_MAPPER.put("ResultCount", SearchHeader.NUM_TOTAL_RESULTS);
        ITEM_MAPPER.put("CompanyName", SearchItem.COMPANY);
        ITEM_MAPPER.put(MODIFIED, SearchItem.DATE_POSTED);
        ITEM_MAPPER.put("Title", SearchItem.JOB_TITLE);
        ITEM_MAPPER.put("ApplyURL", SearchItem.LISTING_URL);
        ITEM_MAPPER.put("Location", SearchItem.LOCATION);
        ITEM_MAPPER.put("ShortDescription", SearchItem.SNIPPET);
    }

    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (headerKeys.contains(localName) || entryKeys.contains(localName)) {
            this.key = localName;
            this.collector = new StringBuffer();
        } else if (JOB.equals(localName)) {
            this.item = new SearchItem();
            this.item.listingSource = BeyondDotComEngine.TYPE;
            this.item.engine = BeyondDotComEngine.TYPE;
        } else if ("Errors".equals(localName)) {
            this.warnings = new ArrayList();
        }
    }

    public void characters(char[] ch, int start, int length) throws SAXException {
        if (this.key != null && ch != null) {
            String value = new String(ch, start, length);
            if (!TextUtils.isEmpty(value)) {
                this.collector.append(value.trim());
            }
        }
    }

    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (JOB.equals(localName) && verify(this.item)) {
            if (this.item.jobId == null) {
                this.item.jobId = generateJobId(this.item);
            }
            this.result.items.add(this.item);
        } else if (HEADER_MAPPER.containsKey(this.key)) {
            this.result.header.set(HEADER_MAPPER.get(this.key), this.collector.toString());
        } else if (ITEM_MAPPER.containsKey(this.key)) {
            String val = this.collector.toString();
            this.item.set(ITEM_MAPPER.get(this.key), val);
            if (this.key.equals("ApplyURL")) {
                Matcher m = JOB_ID_PTR.matcher(val);
                if (m.find()) {
                    this.item.jobId = m.group(1);
                }
            }
        } else if ("Error".equals(localName)) {
            this.warnings.add(this.collector.toString());
        } else if ("Errors".equals(localName)) {
            this.result.header.messages = (String[]) this.warnings.toArray(new String[0]);
            this.warnings = null;
        }
        this.key = null;
        this.collector = new StringBuffer();
    }

    private boolean verify(SearchItem item) {
        boolean valid = !TextUtils.isEmpty(item.jobTitle) && !TextUtils.isEmpty(item.snippet) && !TextUtils.isEmpty(item.listingUrl) && item.postedDate > 0 && item.location != null && !TextUtils.isEmpty(item.location.name);
        if (!valid) {
            Log.d(TAG, "Invalid item " + item);
        }
        return valid;
    }

    public void endDocument() throws SAXException {
        super.endDocument();
        this.result.header.numViewableResults = this.result.header.numTotalResults;
    }
}
