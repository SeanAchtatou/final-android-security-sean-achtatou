package bostone.android.hireadroid.widget;

import android.content.Context;
import android.content.SharedPreferences;

public class WidgetPrefs {
    private static final String JT = "JT";
    private static final String KEY = WidgetMini.class.getName();
    private static final String LOC = "LOC";
    private static final String PREFIX = "searchwidget_";
    private static final String Q = "Q";
    private static final String SRC = "SRC";

    public static String getUrl(Context context, int id) {
        return getPrefs(context).getString(PREFIX + id, null);
    }

    private static SharedPreferences getPrefs(Context context) {
        return context.getSharedPreferences(KEY, 0);
    }

    public static void addUrl(Context context, int id, String url) {
        getPrefs(context).edit().putString(PREFIX + id, url).commit();
    }

    public static void removeUrl(Context context, int id) {
        getPrefs(context).edit().remove(PREFIX + id).commit();
    }

    public static String[] getCurrent(Context context, int id) {
        SharedPreferences prefs = getPrefs(context);
        String key = PREFIX + id + "_";
        return new String[]{prefs.getString(String.valueOf(key) + ((String) JT), null), prefs.getString(String.valueOf(key) + ((String) Q), null), prefs.getString(String.valueOf(key) + ((String) SRC), null), prefs.getString(String.valueOf(key) + ((String) LOC), null)};
    }

    public static void removeCurrent(Context context, int id) {
        String key = PREFIX + id + "_";
        getPrefs(context).edit().remove(String.valueOf(key) + JT).remove(String.valueOf(key) + Q).remove(String.valueOf(key) + SRC).remove(String.valueOf(key) + LOC).commit();
    }

    public static void setCurrent(Context context, int id, String jt, String q, String src, String loc) {
        String key = PREFIX + id + "_";
        getPrefs(context).edit().putString(String.valueOf(key) + JT, jt).putString(String.valueOf(key) + Q, q).putString(String.valueOf(key) + SRC, src).putString(String.valueOf(key) + LOC, loc).commit();
    }
}
