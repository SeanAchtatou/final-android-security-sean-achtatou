package bostone.android.hireadroid.widget;

import android.app.IntentService;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;
import android.widget.RemoteViews;
import bostone.android.hireadroid.R;
import bostone.android.hireadroid.provider.SearchProvider;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import net.oauth.OAuthProblemException;

public class WidgetIntentService extends IntentService {
    private static final int ERROR = 0;
    private static final int OK_UPDATE = 1;
    private static final String TAG = "WidgetIntentService";
    private static final SimpleDateFormat sdf = new SimpleDateFormat("MMMMMMMMMM d, yyyy", Locale.US);
    private static final SimpleDateFormat sdf1 = new SimpleDateFormat("d MMMMMMMMMM yyyy", Locale.US);
    private static final SimpleDateFormat sdf2 = new SimpleDateFormat("MM/dd/yyyy", Locale.US);
    private static final SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy/MM/dd", Locale.US);

    public WidgetIntentService() {
        super(TAG);
    }

    /* access modifiers changed from: protected */
    public void onHandleIntent(Intent intent) {
        Uri data = intent.getData();
        if (data != null && data.getFragment() != null) {
            int widgetId = Integer.parseInt(data.getFragment());
            String url = intent.getStringExtra(OAuthProblemException.URL);
            AppWidgetManager mgr = AppWidgetManager.getInstance(this);
            RemoteViews views = new RemoteViews(getPackageName(), (int) R.layout.widget_layout);
            views.setViewVisibility(R.id.widgetUpdated, 8);
            views.setViewVisibility(R.id.widgetProgress, 0);
            if (url != null) {
                int step = intent.getIntExtra("step", 0);
                switch (step) {
                    case 0:
                        WidgetMini.setWidgetText(views, "", getString(R.string.loading_wait), "", "");
                        mgr.updateAppWidget(widgetId, views);
                        break;
                }
                Intent settingsIntent = new Intent(this, WidgetConfigureActivity.class);
                settingsIntent.setData(Uri.fromParts("settings", ":", Integer.toString(widgetId)));
                settingsIntent.putExtra("appWidgetId", widgetId);
                Intent resultsIntent = new Intent("bostone.android.hireadroid.RESULTS");
                resultsIntent.setFlags(268435456);
                resultsIntent.putExtra("appWidgetId", widgetId);
                resultsIntent.setData(Uri.parse(url));
                views.setOnClickPendingIntent(R.id.widgetContent, PendingIntent.getActivity(this, 0, resultsIntent, 0));
                processUpdate(widgetId, url, mgr, views, step);
            } else {
                Log.d(TAG, "No updates to process due to null URL");
                WidgetMini.setWidgetText(views, "", getString(R.string.loading_wait), "", "");
            }
            views.setViewVisibility(R.id.widgetProgress, 8);
            mgr.updateAppWidget(widgetId, views);
        }
    }

    private void processUpdate(int widgetId, String url, AppWidgetManager mgr, RemoteViews views, int step) {
        Cursor cursor = null;
        try {
            cursor = getContentResolver().query(Uri.parse("content://bostone.android.hireadroid.provider.SearchProvider?type=update"), null, SearchProvider.LATEST_UPDATE, new String[]{url, Integer.toString(step), Integer.toString(widgetId)}, null);
            Log.d(TAG, "Cursor: " + cursor);
            if (cursor != null && cursor.getColumnCount() >= 1) {
                if (cursor.moveToFirst()) {
                    int type = cursor.getInt(0);
                    switch (type) {
                        case 0:
                            WidgetMini.setWidgetText(views, "Error", cursor.getString(1), cursor.getString(2), "");
                            mgr.updateAppWidget(widgetId, views);
                            break;
                        case 1:
                            String[] update = getUpdateFromCursor(cursor);
                            if (update != null && update.length == 4) {
                                WidgetMini.setWidgetText(views, update[0], update[1], update[2], update[3]);
                                mgr.updateAppWidget(widgetId, views);
                                break;
                            }
                        default:
                            Log.e(TAG, "Invalid index type for widget update: " + type);
                            break;
                    }
                }
                if (cursor != null && !cursor.isClosed()) {
                    cursor.close();
                }
            }
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
    }

    private String[] getUpdateFromCursor(Cursor cursor) {
        String[] update = null;
        long ts = 0;
        do {
            String date = cursor.getString(5);
            long l = date.matches("\\d+") ? Long.parseLong(date) : parseDate(date);
            if (l > ts) {
                update = new String[]{cursor.getString(1), cursor.getString(2), "Source: " + cursor.getString(3), cursor.getString(4)};
                ts = l;
            }
        } while (cursor.moveToNext());
        return update;
    }

    private long parseDate(String date) {
        try {
            return sdf.parse(date).getTime();
        } catch (ParseException e) {
            try {
                return sdf1.parse(date).getTime();
            } catch (ParseException e2) {
                try {
                    return sdf2.parse(date).getTime();
                } catch (ParseException e3) {
                    try {
                        return sdf3.parse(date).getTime();
                    } catch (ParseException e4) {
                        Log.d(TAG, "Failed to parse date: " + date);
                        return 1;
                    }
                }
            }
        }
    }
}
