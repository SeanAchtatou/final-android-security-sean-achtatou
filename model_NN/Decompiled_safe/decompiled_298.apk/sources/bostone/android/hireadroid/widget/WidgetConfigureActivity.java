package bostone.android.hireadroid.widget;

import android.app.Activity;
import android.app.AlertDialog;
import android.appwidget.AppWidgetManager;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import bostone.android.hireadroid.R;
import bostone.android.hireadroid.activity.MonitoredActivity;
import bostone.android.hireadroid.provider.SearchProvider;
import com.flurry.android.FlurryAgent;
import java.util.HashMap;
import net.oauth.OAuthProblemException;

public class WidgetConfigureActivity extends Activity {
    private static final String TAG = WidgetConfigureActivity.class.getName();
    private TextView info = null;
    private ListView lv = null;
    private HashMap<String, String> opts;
    private final int rate = WidgetMini.UPDATE_RATE_SEC;
    private String url;
    private int widgetId;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.widget_popup);
        setResult(0);
        this.info = (TextView) findViewById(R.id.widgetPopUpInfo);
        this.lv = (ListView) findViewById(R.id.widgetPopUpList);
        this.lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                WidgetConfigureActivity.this.onJobSearchSelected(view);
            }
        });
        findViewById(R.id.widgetPopUpHelp).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!WidgetConfigureActivity.this.isFinishing()) {
                    new AlertDialog.Builder(WidgetConfigureActivity.this).setTitle((int) R.string.widget_popup_help_title).setMessage((int) R.string.widget_popup_help).setCancelable(true).setNeutralButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).create().show();
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this, MonitoredActivity.KEY);
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            this.widgetId = extras.getInt("appWidgetId", 0);
        } else {
            Uri data = intent.getData();
            if (data != null) {
                this.widgetId = Integer.parseInt(data.getFragment());
            }
        }
        findViewById(R.id.widgetPopUpAddBtn).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                WidgetConfigureActivity.this.onAddSearchesClick();
            }
        });
        findViewById(R.id.widgetPopUpCancelBtn).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                WidgetConfigureActivity.this.finish();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        FlurryAgent.onEndSession(this);
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onAddSearchesClick() {
        try {
            startActivity(new Intent("bostone.android.hireadroid.SEARCH"));
        } catch (ActivityNotFoundException e) {
            Log.e(TAG, "Detected no main app", e);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        Cursor cursor = managedQuery(Uri.parse("content://bostone.android.hireadroid.provider.SearchProvider?type=searches"), null, SearchProvider.SAVED_SEARCHES, null, null);
        if (cursor == null || !cursor.moveToFirst()) {
            this.info.setText((int) R.string.widget_popup_nojob);
        } else {
            initJobSearchList(cursor);
        }
    }

    private void initJobSearchList(Cursor cursor) {
        this.info.setText((int) R.string.widget_popup_jobs);
        ArrayAdapter<String> a = new ArrayAdapter<>(this, 17367043);
        this.lv.setAdapter((ListAdapter) a);
        this.opts = new HashMap<>();
        do {
            String key = cursor.getString(0);
            a.add(key);
            this.opts.put(key, cursor.getString(1));
        } while (cursor.moveToNext());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: private */
    public void onJobSearchSelected(View view) {
        String key = ((TextView) view).getText().toString();
        if (key != null) {
            this.url = this.opts.get(key);
            AppWidgetManager mgr = AppWidgetManager.getInstance(this);
            WidgetPrefs.addUrl(this, this.widgetId, this.url);
            WidgetMini.processUpdate(this, mgr, this.widgetId, 1);
            WidgetMini.startAlarm(this, this.rate);
            Intent intent = new Intent();
            intent.putExtra("appWidgetId", this.widgetId);
            intent.putExtra(OAuthProblemException.URL, this.url);
            intent.putExtra("FROM_WIDGET", true);
            setResult(-1, intent);
        }
        finish();
    }
}
