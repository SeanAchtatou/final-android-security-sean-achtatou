package bostone.android.hireadroid.widget;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.SystemClock;
import android.util.Log;
import android.widget.RemoteViews;
import bostone.android.hireadroid.R;
import net.oauth.OAuthProblemException;

public class WidgetMini extends AppWidgetProvider {
    public static final int STEP_CHANGE = 1;
    public static final int STEP_INIT = 0;
    public static final int STEP_UPDATE = 2;
    protected static final String TAG = WidgetMini.class.getSimpleName();
    public static int UPDATE_RATE_SEC = 3600;
    private static final String WIDGET_UPDATE = "bostone.android.hireadroid.UPDATE_WIDGET";

    public void onDisabled(Context context) {
        Log.d(TAG, "Removing widget Alarms");
        ((AlarmManager) context.getSystemService("alarm")).cancel(PendingIntent.getBroadcast(context, 0, new Intent(WIDGET_UPDATE), 134217728));
    }

    public void onEnabled(Context context) {
        context.getPackageManager().setComponentEnabledSetting(new ComponentName("bostone.android.hireadroid", ".widget.TopJobWidgetAlarmReceiver"), 0, 1);
        Log.d(TAG, "onEnabled");
    }

    public static void startAlarm(Context context, int rate) {
        startAlarm(context, rate, false);
    }

    public static void startAlarm(Context context, int rate, boolean delay) {
        long triggerAt;
        Intent widgetUpdate = new Intent(context, WidgetAlarmReceiver.class);
        widgetUpdate.setAction(WIDGET_UPDATE);
        PendingIntent newPending = PendingIntent.getBroadcast(context, 0, widgetUpdate, 134217728);
        AlarmManager alarms = (AlarmManager) context.getSystemService("alarm");
        if (delay) {
            triggerAt = SystemClock.elapsedRealtime() + 3600000;
        } else {
            triggerAt = SystemClock.elapsedRealtime();
        }
        alarms.setRepeating(2, triggerAt, (long) (rate * 1000), newPending);
        Log.d(TAG, "Started Alarm with refresh rate of " + rate + " sec and delay of " + triggerAt);
    }

    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        for (int id : appWidgetIds) {
            processUpdate(context, appWidgetManager, id, 0);
        }
    }

    public void onDeleted(Context context, int[] appWidgetIds) {
        for (int id : appWidgetIds) {
            WidgetPrefs.removeUrl(context, id);
            WidgetPrefs.removeCurrent(context, id);
        }
    }

    public static void setWidgetText(RemoteViews views, String jt, String e, String source, String loc) {
        setText(views, R.id.widgetHead, jt);
        setText(views, R.id.widgetBody, e);
        setText(views, R.id.widgetLoc, loc);
        setText(views, R.id.widgetSrc, source);
    }

    public static void setText(RemoteViews views, int id, String text) {
        if (text != null) {
            views.setTextViewText(id, text);
        }
    }

    public static void processUpdate(Context context, AppWidgetManager appWidgetManager, int widgetId, int step) {
        String url = WidgetPrefs.getUrl(context, widgetId);
        Log.d(TAG, "Processing widget update [id=" + widgetId + ", url=[" + url + "]");
        Intent ui = new Intent(context, WidgetIntentService.class);
        ui.setData(Uri.fromParts("hireadroid", ":", Integer.toString(widgetId)));
        ui.putExtra("step", step);
        ui.putExtra(OAuthProblemException.URL, url);
        context.startService(ui);
    }
}
