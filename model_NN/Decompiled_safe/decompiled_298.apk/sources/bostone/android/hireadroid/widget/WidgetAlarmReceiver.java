package bostone.android.hireadroid.widget;

import android.appwidget.AppWidgetManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class WidgetAlarmReceiver extends BroadcastReceiver {
    public static final String TAG = "WidgetAlarmReceiver";

    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "intent=" + intent);
        AppWidgetManager mgr = AppWidgetManager.getInstance(context);
        for (int id : mgr.getAppWidgetIds(new ComponentName(context, WidgetMini.class))) {
            try {
                WidgetMini.processUpdate(context, mgr, id, 2);
            } catch (Exception e) {
                Log.e(TAG, "Failed to process widget id=" + id, e);
            }
        }
    }
}
