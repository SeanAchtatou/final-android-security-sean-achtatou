package softkos.rotateme;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FileWR {
    static FileWR instance = null;
    String FILENAME = "rotateme2";
    int file_pos = 0;

    public static final byte[] intToByteArray(int value) {
        return new byte[]{(byte) (value >>> 24), (byte) (value >>> 16), (byte) (value >>> 8), (byte) value};
    }

    public static final int byteArrayToInt(byte[] b) {
        return (b[0] << 24) + ((b[1] & 255) << 16) + ((b[2] & 255) << 8) + (b[3] & 255);
    }

    public void writeInt(FileOutputStream fos, int val) {
        try {
            fos.write(intToByteArray(val));
        } catch (Exception e) {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, ?[OBJECT, ARRAY], java.io.FileNotFoundException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, ?[OBJECT, ARRAY], java.io.IOException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    public void saveGame() {
        int val;
        try {
            FileOutputStream fos = null;
            try {
                fos = MainActivity.getInstance().getApplicationContext().openFileOutput(this.FILENAME, 0);
            } catch (FileNotFoundException e) {
                Logger.getLogger(FileWR.class.getName()).log(Level.SEVERE, (String) null, (Throwable) e);
            }
            try {
                writeInt(fos, Vars.getInstance().getLevel());
                for (int i = 0; i < 1000; i++) {
                    if (Levels.getInstance().isLevelSolved(i)) {
                        val = 1;
                    } else {
                        val = 0;
                    }
                    writeInt(fos, val);
                }
                fos.close();
            } catch (IOException e2) {
                Logger.getLogger(FileWR.class.getName()).log(Level.SEVERE, (String) null, (Throwable) e2);
            }
        } catch (Exception e3) {
        }
    }

    public int getInt(byte[] buffer) {
        try {
            byte[] int_buf = {buffer[this.file_pos], buffer[this.file_pos + 1], buffer[this.file_pos + 2], buffer[this.file_pos + 3]};
            this.file_pos += 4;
            return byteArrayToInt(int_buf);
        } catch (Exception e) {
            return 0;
        }
    }

    public String getString(byte[] buffer, int len) {
        byte[] tmp = new byte[len];
        for (int i = 0; i < len; i++) {
            tmp[i] = buffer[this.file_pos + i];
        }
        this.file_pos += len;
        return new String(tmp);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, ?[OBJECT, ARRAY], java.io.FileNotFoundException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, ?[OBJECT, ARRAY], java.io.IOException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    public boolean loadGame() {
        try {
            try {
                FileInputStream fis = MainActivity.getInstance().getApplicationContext().openFileInput(this.FILENAME);
                try {
                    byte[] buffer = new byte[fis.available()];
                    fis.read(buffer);
                    this.file_pos = 0;
                    Vars.getInstance().setLevel(getInt(buffer));
                    for (int i = 0; i < 1000; i++) {
                        if (getInt(buffer) > 0) {
                            Levels.getInstance().setSolved(i);
                        }
                    }
                    fis.close();
                    return true;
                } catch (IOException e) {
                    Logger.getLogger(FileWR.class.getName()).log(Level.SEVERE, (String) null, (Throwable) e);
                    return false;
                }
            } catch (FileNotFoundException e2) {
                Logger.getLogger(FileWR.class.getName()).log(Level.SEVERE, (String) null, (Throwable) e2);
                return false;
            }
        } catch (Exception e3) {
            return false;
        }
    }

    public static FileWR getInstance() {
        if (instance == null) {
            instance = new FileWR();
        }
        return instance;
    }
}
