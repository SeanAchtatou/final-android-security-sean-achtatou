package softkos.rotateme;

public class Element extends GameObject {
    boolean can_click = false;
    int id = 0;
    int type;

    public void setType(int t) {
        this.type = t;
    }

    public int getType() {
        return this.type;
    }

    public boolean canClick() {
        return this.can_click;
    }

    public Element(boolean cc) {
        this.can_click = cc;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int i) {
        this.id = i;
    }

    public boolean move() {
        if (getPx() == getDestPx() && getPy() == getDestPy()) {
            return false;
        }
        if (Math.abs(getPx() - getDestPx()) < 8) {
            SetPos(getDestPx(), getPy());
        }
        if (Math.abs(getPy() - getDestPy()) < 8) {
            SetPos(getPx(), getDestPy());
        }
        if (getPx() < getDestPx()) {
            SetPos(getPx() + 8, getPy());
        }
        if (getPx() > getDestPx()) {
            SetPos(getPx() - 8, getPy());
        }
        if (getPy() < getDestPy()) {
            SetPos(getPx(), getPy() + 8);
        }
        if (getPy() > getDestPy()) {
            SetPos(getPx(), getPy() - 8);
        }
        return true;
    }
}
