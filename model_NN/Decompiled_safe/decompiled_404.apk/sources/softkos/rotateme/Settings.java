package softkos.rotateme;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Settings {
    static Settings instance = null;
    public int SOUND_DISABLED = 0;
    public int VIBRATION_DISABLED = 1;
    int file_pos = 0;
    String filename = "moveme_settings";
    int[] settings = new int[1000];

    public Settings() {
        loadSettings();
    }

    public void toggleSettings(int set) {
        if (this.settings[set] != 0) {
            this.settings[set] = 0;
        } else {
            this.settings[set] = 1;
        }
    }

    public void loadSettings() {
        this.file_pos = 0;
        try {
            FileInputStream fis = MainActivity.getInstance().getApplicationContext().openFileInput(this.filename);
            try {
                byte[] buffer = new byte[fis.available()];
                fis.read(buffer);
                for (int i = 0; i < 1000; i++) {
                    this.settings[i] = getInt(buffer);
                }
                fis.close();
            } catch (IOException e) {
            }
        } catch (FileNotFoundException e2) {
            FileNotFoundException fileNotFoundException = e2;
        }
    }

    public int getIntSettings(int set) {
        return this.settings[set];
    }

    public void setIntSettings(int set, int val) {
        this.settings[set] = val;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, ?[OBJECT, ARRAY], java.io.IOException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, ?[OBJECT, ARRAY], java.io.FileNotFoundException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    public void saveSettings() {
        try {
            FileOutputStream fos = MainActivity.getInstance().getApplicationContext().openFileOutput(this.filename, 0);
            for (int i = 0; i < 1000; i++) {
                writeInt(fos, this.settings[i]);
            }
            try {
                fos.close();
            } catch (IOException e) {
                Logger.getLogger(FileWR.class.getName()).log(Level.SEVERE, (String) null, (Throwable) e);
            }
        } catch (FileNotFoundException e2) {
            Logger.getLogger(FileWR.class.getName()).log(Level.SEVERE, (String) null, (Throwable) e2);
        }
    }

    public static Settings getInstnace() {
        if (instance == null) {
            instance = new Settings();
        }
        return instance;
    }

    public static final byte[] intToByteArray(int value) {
        return new byte[]{(byte) (value >>> 24), (byte) (value >>> 16), (byte) (value >>> 8), (byte) value};
    }

    public static final int byteArrayToInt(byte[] b) {
        return (b[0] << 24) + ((b[1] & 255) << 16) + ((b[2] & 255) << 8) + (b[3] & 255);
    }

    public void writeInt(FileOutputStream fos, int val) {
        try {
            fos.write(intToByteArray(val));
        } catch (Exception e) {
        }
    }

    private int getInt(byte[] buffer) {
        if (this.file_pos + 3 >= buffer.length) {
            return 0;
        }
        byte[] int_buf = {buffer[this.file_pos], buffer[this.file_pos + 1], buffer[this.file_pos + 2], buffer[this.file_pos + 3]};
        this.file_pos += 4;
        return byteArrayToInt(int_buf);
    }

    private String getString(byte[] buffer, int len) {
        byte[] tmp = new byte[len];
        for (int i = 0; i < len; i++) {
            tmp[i] = buffer[this.file_pos + i];
        }
        this.file_pos += len;
        return new String(tmp);
    }
}
