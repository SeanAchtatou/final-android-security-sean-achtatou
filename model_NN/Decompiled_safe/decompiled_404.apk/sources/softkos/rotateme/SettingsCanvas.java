package softkos.rotateme;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.View;

public class SettingsCanvas extends View {
    static SettingsCanvas instance = null;
    gButton[] otherAppsBtn;
    Paint paint = new Paint();
    PaintManager paintMgr;
    gButton resetAllLevelsBtn = null;
    gButton soundSettingsBtn = null;
    Vars vars;
    gButton vibrationBtn = null;

    public SettingsCanvas(Context c) {
        super(c);
        instance = this;
        this.vars = Vars.getInstance();
        this.paintMgr = PaintManager.getInstance();
        initUI();
    }

    public void showUI(boolean show) {
        for (int i = 0; i < this.vars.getButtonListSize(); i++) {
            this.vars.getButton(i).hide();
        }
        if (show) {
            this.resetAllLevelsBtn.show();
            this.soundSettingsBtn.show();
            this.vibrationBtn.show();
        }
    }

    public void initUI() {
        int btn_w = 260;
        int btn_h = 50;
        if (this.vars.getScreenWidth() > 400) {
            btn_w = 380;
            btn_h = 70;
        }
        this.soundSettingsBtn = new gButton();
        this.soundSettingsBtn.setSize(btn_w, btn_h);
        this.soundSettingsBtn.setId(Vars.getInstance().SOUND_SETTINGS);
        this.soundSettingsBtn.show();
        this.soundSettingsBtn.setBackImages("emptybtn", "240x50on");
        Vars.getInstance().addButton(this.soundSettingsBtn);
        this.vibrationBtn = new gButton();
        this.vibrationBtn.setSize(btn_w, btn_h);
        this.vibrationBtn.setId(Vars.getInstance().VIBRATION_SETTINGS);
        this.vibrationBtn.show();
        this.vibrationBtn.setBackImages("emptybtn", "240x50on");
        Vars.getInstance().addButton(this.vibrationBtn);
        this.resetAllLevelsBtn = new gButton();
        this.resetAllLevelsBtn.setSize(btn_w, btn_h);
        this.resetAllLevelsBtn.setId(Vars.getInstance().RESET_ALL_LEVELS);
        this.resetAllLevelsBtn.show();
        this.resetAllLevelsBtn.setText("Reset all levels");
        this.resetAllLevelsBtn.setBackImages("emptybtn", "240x50on");
        Vars.getInstance().addButton(this.resetAllLevelsBtn);
        updateButtonText();
    }

    public void updateButtonText() {
        String s;
        String s2;
        if (Settings.getInstnace().getIntSettings(Settings.getInstnace().SOUND_DISABLED) != 0) {
            s = String.valueOf("Sound: ") + "OFF";
        } else {
            s = String.valueOf("Sound: ") + "ON";
        }
        this.soundSettingsBtn.setText(s);
        if (Settings.getInstnace().getIntSettings(Settings.getInstnace().VIBRATION_DISABLED) != 0) {
            s2 = String.valueOf("Vibration: ") + "OFF";
        } else {
            s2 = String.valueOf("Vibration: ") + "ON";
        }
        this.vibrationBtn.setText(s2);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        layoutUI();
    }

    public void layoutUI() {
        int xs = (getWidth() / 2) - (this.soundSettingsBtn.getWidth() / 2);
        int ys = getHeight() / 4;
        int spacing = (int) (((double) this.soundSettingsBtn.getHeight()) * 1.1d);
        this.soundSettingsBtn.setPosition(xs, ys);
        this.vibrationBtn.setPosition(xs, ys + spacing);
        this.resetAllLevelsBtn.setPosition(xs, (spacing * 2) + ys);
    }

    public void mouseDown(int x, int y) {
        int pressed = 0;
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            pressed |= Vars.getInstance().getButton(i).mouseDown(x, y);
            if (pressed != 0) {
                invalidate();
                return;
            }
        }
    }

    public void mouseUp(int x, int y) {
        int pressed = 0;
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            pressed |= Vars.getInstance().getButton(i).mouseUp(x, y);
            if (pressed != 0) {
                handleCommand(Vars.getInstance().getButton(i).getId());
                invalidate();
                return;
            }
        }
    }

    public void mouseDrag(int x, int y) {
        int pressed = 0;
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            pressed |= Vars.getInstance().getButton(i).mouseDrag(x, y);
            if (pressed != 0) {
                invalidate();
                return;
            }
        }
    }

    public void handleCommand(int id) {
        if (id == Vars.getInstance().SOUND_SETTINGS) {
            Settings.getInstnace().toggleSettings(Settings.getInstnace().SOUND_DISABLED);
            updateButtonText();
        }
        if (id == Vars.getInstance().VIBRATION_SETTINGS) {
            Settings.getInstnace().toggleSettings(Settings.getInstnace().VIBRATION_DISABLED);
            updateButtonText();
        } else if (id == Vars.getInstance().RESET_ALL_LEVELS) {
            MainActivity.getInstance().resetAllLevels();
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        int x = (int) event.getX();
        int y = (int) event.getY();
        if (event.getAction() == 0) {
            mouseDown(x, y);
        } else if (2 == event.getAction()) {
            mouseDrag(x, y);
        } else if (1 == event.getAction()) {
            mouseUp(x, y);
        }
        invalidate();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        this.paint.setAntiAlias(true);
        this.paintMgr.drawImage(canvas, "gameback", 0, 0, getWidth(), getHeight());
        this.paintMgr.setColor(-256);
        this.paintMgr.setTextSize(60);
        this.paintMgr.drawString(canvas, "Settings", 0, 20, getWidth(), 70, PaintManager.STR_CENTER);
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            Vars.getInstance().getButton(i).draw(canvas, this.paint);
        }
    }

    public static SettingsCanvas getInstance() {
        return instance;
    }
}
