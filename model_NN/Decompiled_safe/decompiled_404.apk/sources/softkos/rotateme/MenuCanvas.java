package softkos.rotateme;

import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.net.Uri;
import android.view.MotionEvent;
import android.view.View;

public class MenuCanvas extends View {
    static MenuCanvas instance = null;
    gButton howPlayBtn = null;
    gButton mobilsoftBtn = null;
    gButton otherAppBtn = null;
    Paint paint = new Paint();
    PaintManager paintMgr;
    gButton startGame = null;
    Vars vars;

    public MenuCanvas(Context c) {
        super(c);
        instance = this;
        this.vars = Vars.getInstance();
        this.paintMgr = PaintManager.getInstance();
        initUI();
    }

    public void showUI(boolean show) {
        for (int i = 0; i < this.vars.getButtonListSize(); i++) {
            this.vars.getButton(i).hide();
        }
        if (show) {
            this.startGame.show();
            this.mobilsoftBtn.show();
            this.howPlayBtn.show();
            this.otherAppBtn.show();
        }
    }

    public void initUI() {
        int btn_w = 280;
        int btn_h = 50;
        if (this.vars.getScreenWidth() > 400) {
            btn_w = 380;
            btn_h = 70;
        }
        this.startGame = new gButton();
        this.startGame.setSize(btn_w, btn_h);
        this.startGame.setId(Vars.getInstance().START_GAME);
        this.startGame.show();
        this.startGame.setText("Play");
        this.startGame.setBackImages("button_off", "button_on");
        Vars.getInstance().addButton(this.startGame);
        this.otherAppBtn = new gButton();
        this.otherAppBtn.setSize(btn_w, btn_h);
        this.otherAppBtn.setId(Vars.getInstance().OTHERAPP);
        this.otherAppBtn.show();
        this.otherAppBtn.setBackImages("button_off", "button_on");
        Vars.getInstance().addButton(this.otherAppBtn);
        this.howPlayBtn = new gButton();
        this.howPlayBtn.setSize(btn_w, btn_h);
        this.howPlayBtn.setId(Vars.getInstance().HOWTOPLAY);
        this.howPlayBtn.show();
        this.howPlayBtn.setText("Instructions");
        this.howPlayBtn.setBackImages("button_off", "button_on");
        Vars.getInstance().addButton(this.howPlayBtn);
        this.mobilsoftBtn = new gButton();
        this.mobilsoftBtn.setSize(btn_w, btn_h);
        this.mobilsoftBtn.setId(Vars.getInstance().GO_TO_MOBILSOFT);
        this.mobilsoftBtn.show();
        this.mobilsoftBtn.setText("More free games");
        this.mobilsoftBtn.setBackImages("button_off", "button_on");
        Vars.getInstance().addButton(this.mobilsoftBtn);
    }

    public void updateButtonText() {
        this.otherAppBtn.setText(OtherApp.getInstance().button_text[OtherApp.getInstance().current]);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        layoutUI();
    }

    public void layoutUI() {
        this.mobilsoftBtn.setPosition((getWidth() / 2) - (this.startGame.getWidth() / 2), getHeight() - ((int) (((double) this.startGame.getHeight()) * 1.05d)));
        this.otherAppBtn.setPosition((getWidth() / 2) - (this.startGame.getWidth() / 2), getHeight() - ((int) (((double) this.startGame.getHeight()) * 2.1d)));
        this.howPlayBtn.setPosition((getWidth() / 2) - (this.startGame.getWidth() / 2), getHeight() - ((int) (((double) this.startGame.getHeight()) * 3.15d)));
        this.startGame.setPosition((getWidth() / 2) - (this.startGame.getWidth() / 2), getHeight() - ((int) (((double) this.startGame.getHeight()) * 4.2d)));
    }

    public void mouseDown(int x, int y) {
        int pressed = 0;
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            pressed |= Vars.getInstance().getButton(i).mouseDown(x, y);
            if (pressed != 0) {
                invalidate();
                return;
            }
        }
    }

    public void mouseUp(int x, int y) {
        int pressed = 0;
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            pressed |= Vars.getInstance().getButton(i).mouseUp(x, y);
            if (pressed != 0) {
                handleCommand(Vars.getInstance().getButton(i).getId());
                invalidate();
                return;
            }
        }
    }

    public void mouseDrag(int x, int y) {
        int pressed = 0;
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            pressed |= Vars.getInstance().getButton(i).mouseDrag(x, y);
            if (pressed != 0) {
                invalidate();
                return;
            }
        }
    }

    public void handleCommand(int id) {
        if (id == Vars.getInstance().START_GAME) {
            MainActivity.getInstance().showGame();
        } else if (id == Vars.getInstance().GO_TO_MOBILSOFT) {
            MainActivity.getInstance().startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://www.mobilsoft.pl")));
        } else if (id == Vars.getInstance().HOWTOPLAY) {
            MainActivity.getInstance().showHowPlay();
        } else if (id == Vars.getInstance().OTHERAPP) {
            MainActivity.getInstance().startActivity(new Intent("android.intent.action.VIEW", Uri.parse(OtherApp.getInstance().urls[OtherApp.getInstance().current])));
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        int x = (int) event.getX();
        int y = (int) event.getY();
        if (event.getAction() == 0) {
            mouseDown(x, y);
        } else if (2 == event.getAction()) {
            mouseDrag(x, y);
        } else if (1 == event.getAction()) {
            mouseUp(x, y);
        }
        invalidate();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        this.paint.setAntiAlias(true);
        canvas.drawColor(-1);
        this.paintMgr.drawImage(canvas, "gameback", 0, 0, getWidth(), getHeight());
        this.paintMgr.drawImage(canvas, "gamename", 0, 0, getWidth(), getHeight());
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            Vars.getInstance().getButton(i).draw(canvas, this.paint);
        }
        String img = OtherApp.getInstance().images[OtherApp.getInstance().current];
        this.paintMgr.drawImage(canvas, img, this.otherAppBtn.getPx() + 7, this.otherAppBtn.getPy() + 6, this.otherAppBtn.getHeight() - 12, this.otherAppBtn.getHeight() - 12);
    }

    public static MenuCanvas getInstance() {
        return instance;
    }
}
