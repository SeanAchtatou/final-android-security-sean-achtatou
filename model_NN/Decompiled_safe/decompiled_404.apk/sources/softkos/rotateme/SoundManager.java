package softkos.rotateme;

import android.media.AudioManager;
import android.media.SoundPool;
import java.util.HashMap;

public class SoundManager {
    public static final int SOUND_ROTATE = 1;
    static SoundManager instance = null;
    private SoundPool soundPool;
    private HashMap<Integer, Integer> soundPoolMap;

    public SoundManager() {
        instance = this;
        initSounds();
    }

    private void initSounds() {
        try {
            this.soundPool = new SoundPool(3, 3, 0);
            if (this.soundPool != null) {
                this.soundPoolMap = new HashMap<>();
                this.soundPoolMap.put(1, Integer.valueOf(this.soundPool.load(MainActivity.getInstance().getBaseContext(), R.raw.rotate, 1)));
                return;
            }
            System.out.println("Sound pool is null");
        } catch (Exception e) {
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 9 */
    public void playSound(int sound) {
        if (Settings.getInstnace().getIntSettings(Settings.getInstnace().SOUND_DISABLED) == 0) {
            try {
                int streamVolume = ((AudioManager) MainActivity.getInstance().getSystemService("audio")).getStreamVolume(3);
                this.soundPool.play(this.soundPoolMap.get(Integer.valueOf(sound)).intValue(), (float) streamVolume, (float) streamVolume, 1, 0, 1.0f);
            } catch (Exception e) {
            }
        }
    }

    public static SoundManager getInstance() {
        if (instance == null) {
            instance = new SoundManager();
        }
        return instance;
    }
}
