package softkos.rotateme;

import android.graphics.drawable.Drawable;

public class ImageLoader {
    static ImageLoader instance = null;
    String[] image_names = {"gameback", "gamename", "emptybtn", "button_off", "button_on", "playoffbtn", "playonbtn", "moreappsoffbtn", "moreappsonbtn", "howplayoffbtn", "howplayonbtn", "playnextoffbtn", "playnextonbtn", "retryoffbtn", "retryonbtn", "otherapp_off", "otherapp_on", "howplay1", "howplay2", "endlevelpassed", "circle", "arrow_left", "arrow_right", "element0", "element1", "element2", "element3", "element4", "boxit_ad", "cubix_ad", "frogs_jump_ad", "turnmeon_ad", "untangle_ad", "ume_ad", "tripeaks_ad"};
    Drawable[] images;
    int[] img_id = {R.drawable.gameback, R.drawable.gamename, R.drawable.emptybtn, R.drawable.button_off, R.drawable.button_on, R.drawable.playoff, R.drawable.playon, R.drawable.moreappsoff, R.drawable.moreappson, R.drawable.howplayoff, R.drawable.howplayon, R.drawable.playnextoffbtn, R.drawable.playnextonbtn, R.drawable.retryoffbtn, R.drawable.retryonbtn, R.drawable.otherapp_off, R.drawable.otherapp_on, R.drawable.howplay1, R.drawable.howplay2, R.drawable.end_passed, R.drawable.circle, R.drawable.arrow_left, R.drawable.arrow_right, R.drawable.element1, R.drawable.element2, R.drawable.element3, R.drawable.element4, R.drawable.element5, R.drawable.ad_boxit, R.drawable.ad_cubix, R.drawable.ad_frogs_jump, R.drawable.ad_turnmeon, R.drawable.ad_untangle, R.drawable.ume_ad, R.drawable.tripeaks_ad};

    public static ImageLoader getInstance() {
        if (instance == null) {
            instance = new ImageLoader();
        }
        return instance;
    }

    public void LoadImages() {
        int len = this.img_id.length;
        this.images = new Drawable[this.image_names.length];
        for (int i = 0; i < len; i++) {
            this.images[i] = MainActivity.getInstance().getResources().getDrawable(this.img_id[i]);
        }
    }

    public Drawable getImage(String img) {
        for (int i = 0; i < this.image_names.length; i++) {
            if (img.equals(this.image_names[i])) {
                return this.images[i];
            }
        }
        return null;
    }
}
