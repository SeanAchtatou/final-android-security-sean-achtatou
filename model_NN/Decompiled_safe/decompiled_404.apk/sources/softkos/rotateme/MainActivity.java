package softkos.rotateme;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import softkos.rotateme.Vars;

public class MainActivity extends Activity {
    static MainActivity instance;
    GameCanvas gameCanvas = null;
    RelativeLayout gameLayout = null;
    HowPlayCanvas howPlayCanvas = null;
    Vars.GameState lastState;
    MenuCanvas menuCanvas = null;
    SettingsCanvas settingsCanvas = null;
    boolean skipInitNewGame = false;

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        instance = this;
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        Vars.getInstance().screenSize(dm.widthPixels, dm.heightPixels);
        ImageLoader.getInstance().LoadImages();
        getWindow().setFlags(1024, 1024);
        requestWindowFeature(1);
        FontManager.getInstance();
        FileWR.getInstance().loadGame();
        showMenu();
    }

    public void showSettings() {
        this.lastState = Vars.getInstance().gameState;
        Vars.getInstance().gameState = Vars.GameState.Settings;
        if (this.settingsCanvas == null) {
            this.settingsCanvas = new SettingsCanvas(this);
        }
        this.settingsCanvas.showUI(true);
        this.settingsCanvas.layoutUI();
        setContentView(this.settingsCanvas);
    }

    public void vibrate(int ms) {
        if (Settings.getInstnace().getIntSettings(Settings.getInstnace().VIBRATION_DISABLED) == 0) {
            try {
                ((Vibrator) getSystemService("vibrator")).vibrate((long) ms);
            } catch (Exception e) {
            }
        }
    }

    public void returnFromSettings() {
        Settings.getInstnace().saveSettings();
        if (this.lastState == Vars.GameState.Game) {
            this.skipInitNewGame = true;
            showGame();
        }
        if (this.lastState == Vars.GameState.Menu) {
            showMenu();
        }
        if (this.lastState == Vars.GameState.HowPlay) {
            showHowPlay();
        }
    }

    public void showMenu() {
        Vars.getInstance().gameState = Vars.GameState.Menu;
        if (this.menuCanvas == null) {
            this.menuCanvas = new MenuCanvas(this);
        }
        if (this.gameCanvas != null) {
            this.gameCanvas.showUI(false);
        }
        OtherApp.getInstance().random();
        this.menuCanvas.updateButtonText();
        this.menuCanvas.showUI(true);
        setContentView(this.menuCanvas);
    }

    public void showHowPlay() {
        Vars.getInstance().gameState = Vars.GameState.HowPlay;
        if (this.howPlayCanvas == null) {
            this.howPlayCanvas = new HowPlayCanvas(this);
        }
        if (this.menuCanvas != null) {
            this.menuCanvas.showUI(false);
        }
        setContentView(this.howPlayCanvas);
    }

    public void showHighscores() {
    }

    public void showGame() {
        if (this.menuCanvas != null) {
            this.menuCanvas.showUI(false);
        }
        if (this.gameCanvas == null) {
            this.gameLayout = new RelativeLayout(this);
            this.gameLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
            this.gameCanvas = new GameCanvas(this);
            RelativeLayout.LayoutParams lparams = new RelativeLayout.LayoutParams(-1, -2);
            lparams.addRule(12);
            this.gameLayout.addView(this.gameCanvas);
            try {
                AdView adView = new AdView(this, AdSize.BANNER, "a14cd2db2ae25ce");
                adView.setLayoutParams(lparams);
                this.gameLayout.addView(adView);
                adView.loadAd(new AdRequest());
                adView.bringToFront();
            } catch (Exception e) {
            }
        }
        Vars.getInstance().gameState = Vars.GameState.Game;
        Vars.getInstance().newGame();
        this.gameCanvas.showUI(true);
        setContentView(this.gameLayout);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4 && (Vars.getInstance().gameState == Vars.GameState.Game || Vars.getInstance().gameState == Vars.GameState.Highscores || Vars.getInstance().gameState == Vars.GameState.HowPlay || Vars.getInstance().gameState == Vars.GameState.Settings)) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyUp(keyCode, event);
        }
        if (Vars.getInstance().gameState == Vars.GameState.Game || Vars.getInstance().gameState == Vars.GameState.Highscores || Vars.getInstance().gameState == Vars.GameState.HowPlay) {
            if (Vars.getInstance().gameState != Vars.GameState.Game || GameCanvas.getInstance() == null || GameCanvas.getInstance().getLevelSelectionDialog() == null) {
                showMenu();
            } else {
                GameCanvas.getInstance().dismissDialogs();
                return true;
            }
        }
        if (Vars.getInstance().gameState == Vars.GameState.Settings) {
            returnFromSettings();
        }
        return true;
    }

    public static MainActivity getInstance() {
        return instance;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 2004, 0, "Prev level").setIcon((int) R.drawable.arrow_left);
        menu.add(0, 2005, 0, "Next level").setIcon((int) R.drawable.arrow_right);
        MenuItem add = menu.add(0, 2000, 0, "Reset level");
        MenuItem add2 = menu.add(0, 2006, 0, "Select level");
        MenuItem add3 = menu.add(0, 2003, 0, "Settings");
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 2000:
                if (Vars.getInstance().gameState == Vars.GameState.Game) {
                    Vars.getInstance().resetLevel();
                }
                return true;
            case 2001:
            case 2002:
            default:
                return false;
            case 2003:
                showSettings();
                return true;
            case 2004:
                Vars.getInstance().prevLevel();
                return true;
            case 2005:
                Vars.getInstance().nextLevel();
                return true;
            case 2006:
                if (GameCanvas.getInstance() != null) {
                    GameCanvas.getInstance().selectLevelDialog();
                }
                return true;
        }
    }

    public void resetAllLevels() {
        AlertDialog.Builder alertbox = new AlertDialog.Builder(this);
        alertbox.setTitle("Reset all levels");
        alertbox.setMessage("Do you want reset all solved levels?");
        alertbox.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                Levels.getInstance().resetAllLevels();
            }
        });
        alertbox.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
            }
        });
        alertbox.show();
    }
}
