package softkos.rotateme;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.View;

public class GameCanvas extends View {
    static GameCanvas instance = null;
    AlertDialog lvlSelectDlg = null;
    int mouseDownX = 0;
    int mouseDownY = 0;
    int mouseX = -1;
    int mouseY = -1;
    Paint paint;
    PaintManager paintMgr;
    gButton playNextBtn = null;
    Vars vars;
    int vpx;
    int vpy;

    public GameCanvas(Context context) {
        super(context);
        instance = this;
        this.paint = new Paint();
        this.vars = Vars.getInstance();
        this.paintMgr = PaintManager.getInstance();
        initUI();
    }

    public void initUI() {
        int btn_w = 280;
        int btn_h = 50;
        if (this.vars.getScreenWidth() > 400) {
            btn_w = 380;
            btn_h = 70;
        }
        this.playNextBtn = new gButton();
        this.playNextBtn.setSize(btn_w, btn_h);
        this.playNextBtn.setId(Vars.getInstance().PLAY_NEXT);
        this.playNextBtn.setText("Next level");
        this.playNextBtn.setBackImages("button_off", "button_on");
        this.playNextBtn.hide();
        this.vars.addButton(this.playNextBtn);
    }

    public void showUI(boolean show) {
        this.vars.hideAllButtons();
    }

    public void layoutUI() {
        if (this.playNextBtn != null) {
            this.playNextBtn.setPosition((getWidth() / 2) - (this.playNextBtn.getWidth() / 2), ((getHeight() / 2) + (getWidth() / 2)) - ((int) (((double) this.playNextBtn.getHeight()) * 1.2d)));
        }
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        layoutUI();
        invalidate();
    }

    public void drawGame(Canvas canvas) {
        this.paintMgr.drawImage(canvas, "gameback", 0, 0, getWidth(), getHeight());
        int xoff = this.vars.getBoardPosX();
        int yoff = this.vars.getBoardPosY();
        int size = this.vars.getBoardHeight();
        int spacing = (int) (((double) size) * 0.05d);
        this.paintMgr.drawImage(canvas, "circle", xoff - spacing, yoff - spacing, size + (spacing * 2), size + (spacing * 2));
        if (this.vars.getElement(0) != null && this.vars.getElement(21) != null) {
            int size2 = (int) (((double) this.vars.getElement(21).getWidth()) * 6.5d);
            this.paintMgr.drawImage(canvas, "circle", 4, 31, size2, size2);
            for (int i = 0; i < this.vars.getElementList().size(); i++) {
                Element e = this.vars.getElement(i);
                if (e.getType() == 0) {
                    this.paintMgr.setColor(-7829368);
                }
                if (e.getType() == 1) {
                    this.paintMgr.setColor(-16711936);
                }
                if (e.getType() == 2) {
                    this.paintMgr.setColor(-65536);
                }
                if (e.getType() == 3) {
                    this.paintMgr.setColor(-16776961);
                }
                if (e.getType() == 4) {
                    this.paintMgr.setColor(-1);
                }
                this.paintMgr.drawImage(canvas, "element" + e.getType(), e.getPx(), e.getPy(), e.getWidth(), e.getHeight());
            }
            String str1 = "";
            String str2 = "";
            int[] slev = Levels.getInstance().getSolvedLevel(this.vars.getLevel());
            for (int i2 = 0; i2 < this.vars.getCurrLevel().length; i2++) {
                str1 = String.valueOf(str1) + this.vars.getCurrLevelAt(i2);
                str2 = String.valueOf(str2) + slev[i2];
            }
            this.paintMgr.setColor(-256);
            this.paintMgr.setTextSize(32);
            this.paintMgr.drawString(canvas, "Level " + (this.vars.getLevel() + 1), getWidth() / 2, 50, getWidth() / 2, 30, PaintManager.STR_CENTER);
            this.paintMgr.setTextSize(16);
            this.paintMgr.drawString(canvas, "Press menu for options", 3, 15, getWidth(), 20, PaintManager.STR_LEFT);
            if (this.playNextBtn != null) {
                this.playNextBtn.hide();
            }
            if (this.vars.isEndLevel() || Levels.getInstance().isLevelSolved(this.vars.getLevel())) {
                this.paintMgr.drawImage(canvas, "endlevelpassed", 0, (getHeight() / 2) - (getWidth() / 2), getWidth(), getWidth());
                this.playNextBtn.show();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        if (this.paint == null) {
            this.paint = new Paint();
        }
        if (canvas != null) {
            drawGame(canvas);
            for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
                Vars.getInstance().getButton(i).draw(canvas, this.paint);
            }
        }
    }

    public void mouseDown(int x, int y) {
        this.mouseDownX = x;
        this.mouseDownY = y;
        int pressed = 0;
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            pressed |= Vars.getInstance().getButton(i).mouseDown(x, y);
            if (pressed != 0) {
                invalidate();
                return;
            }
        }
    }

    public void mouseDrag(int x, int y) {
        this.mouseX = x;
        this.mouseY = y;
        int pressed = 0;
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            pressed |= Vars.getInstance().getButton(i).mouseDrag(x, y);
            if (pressed != 0) {
                invalidate();
                return;
            }
        }
    }

    public void mouseUp(int x, int y) {
        this.mouseX = -1;
        this.mouseY = -1;
        int pressed = 0;
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            pressed |= Vars.getInstance().getButton(i).mouseUp(x, y);
            if (pressed != 0) {
                handleCommand(Vars.getInstance().getButton(i).getId());
                invalidate();
                return;
            }
        }
        if (this.vars.canRotate() && !this.vars.isEndLevel()) {
            this.vars.rotateAt(x, y);
            String str = "";
            for (int i2 = 0; i2 < this.vars.getCurrLevel().length; i2++) {
                str = String.valueOf(str) + this.vars.getCurrLevel()[i2] + ",";
            }
            System.out.println(str);
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        int x = (int) event.getX();
        int y = (int) event.getY();
        if (event.getAction() == 0) {
            mouseDown(x, y);
        } else if (2 == event.getAction()) {
            mouseDrag(x, y);
        } else if (1 == event.getAction()) {
            mouseUp(x, y);
        }
        invalidate();
        return true;
    }

    public void handleCommand(int id) {
        if (id == this.vars.PLAY_NEXT) {
            this.vars.nextLevel();
            showUI(false);
        }
    }

    public static GameCanvas getInstance() {
        return instance;
    }

    public AlertDialog getLevelSelectionDialog() {
        return this.lvlSelectDlg;
    }

    public void dismissDialogs() {
        this.lvlSelectDlg = null;
    }

    /* access modifiers changed from: package-private */
    public void selectLevelDialog() {
        String[] items = new String[Levels.getInstance().getLevelCount()];
        for (int i = 0; i < Levels.getInstance().getLevelCount(); i++) {
            items[i] = "Level " + (i + 1);
            if (Levels.getInstance().isLevelSolved(i)) {
                items[i] = String.valueOf(items[i]) + ": solved";
            }
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.getInstance());
        builder.setTitle("Select level");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                Vars.getInstance().setLevel(item);
                Vars.getInstance().initLevel();
                GameCanvas.this.lvlSelectDlg.dismiss();
                GameCanvas.this.lvlSelectDlg = null;
            }
        });
        this.lvlSelectDlg = builder.create();
        this.lvlSelectDlg.show();
    }
}
