package com.openfeint.internal.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.widget.Toast;
import com.openfeint.api.OpenFeint;
import com.openfeint.internal.OpenFeintInternal;
import com.openfeint.internal.request.IRawRequestDelegate;
import com.openfeint.internal.ui.WebNav;
import java.util.List;
import java.util.Map;
import sudoku.challenge.lt.R;

public class Settings extends WebNav {
    String mOldUserId;

    public static void open() {
        Context currentActivity = OpenFeintInternal.getInstance().getContext();
        Intent intent = new Intent(currentActivity, Settings.class);
        intent.addFlags(268435456);
        currentActivity.startActivity(intent);
    }

    public void onResume() {
        if (this.mOldUserId == null) {
            this.mOldUserId = OpenFeint.getCurrentUser().resourceID();
        } else if (!this.mOldUserId.equals(OpenFeint.getCurrentUser().resourceID())) {
            new AlertDialog.Builder(this).setTitle(OpenFeintInternal.getRString(R.string.of_switched_accounts)).setMessage(String.format(OpenFeintInternal.getRString(R.string.of_now_logged_in_as_format), OpenFeint.getCurrentUser().name)).setNegativeButton(OpenFeintInternal.getRString(R.string.of_ok), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Settings.this.finish();
                }
            }).show();
        }
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public String initialContentPath() {
        return "settings/index";
    }

    /* access modifiers changed from: protected */
    public WebNav.ActionHandler createActionHandler(WebNav webNav) {
        return new SettingsActionHandler(webNav);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 10009 && data != null) {
            Toast.makeText(this, OpenFeintInternal.getRString(R.string.of_profile_pic_changed), 0).show();
        }
    }

    private class SettingsActionHandler extends WebNav.ActionHandler {
        public SettingsActionHandler(WebNav webNav) {
            super(webNav);
        }

        /* access modifiers changed from: protected */
        public void populateActionList(List<String> actionList) {
            super.populateActionList(actionList);
            actionList.add("logout");
            actionList.add("introFlow");
        }

        public void apiRequest(Map<String, String> options) {
            super.apiRequest(options);
            OpenFeint.getCurrentUser().load(null);
        }

        public final void logout(Map<String, String> map) {
            OpenFeintInternal.getInstance().logoutUser(new IRawRequestDelegate() {
                public void onResponse(int responseCode, String responseBody) {
                    Settings.this.finish();
                }
            });
        }

        public final void introFlow(Map<String, String> map) {
            Settings.this.startActivity(new Intent(Settings.this, IntroFlow.class).putExtra("content_name", "login?mode=switch"));
        }
    }
}
