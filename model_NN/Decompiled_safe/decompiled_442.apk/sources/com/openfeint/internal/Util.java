package com.openfeint.internal;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import com.openfeint.api.OpenFeintSettings;
import com.openfeint.internal.resource.ResourceClass;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonParseException;

public class Util {
    private static final String TAG = "Util";
    public static final int VERSION = Integer.valueOf(Build.VERSION.SDK).intValue();

    public static boolean isPadVersion() {
        return VERSION >= 11;
    }

    public static void setOrientation(Activity act) {
        Integer orientation = (Integer) OpenFeintInternal.getInstance().getSettings().get(OpenFeintSettings.RequestedOrientation);
        if (orientation != null) {
            act.setRequestedOrientation(orientation.intValue());
        }
    }

    public static final byte[] toByteArray(InputStream is) throws IOException {
        byte[] readBuffer = new byte[4096];
        ByteArrayOutputStream accumulator = new ByteArrayOutputStream();
        while (true) {
            int count = is.read(readBuffer);
            if (count <= 0) {
                accumulator.close();
                return accumulator.toByteArray();
            }
            accumulator.write(readBuffer, 0, count);
        }
    }

    public static void deleteFiles(File path) {
        if (path.isDirectory()) {
            for (String name : path.list()) {
                deleteFiles(new File(path, name));
            }
        }
        path.delete();
    }

    public static void copyDirectory(File srcDir, File dstDir) throws IOException {
        if (srcDir.isDirectory()) {
            if (!dstDir.exists()) {
                dstDir.mkdir();
            }
            String[] children = srcDir.list();
            for (int i = 0; i < children.length; i++) {
                copyDirectory(new File(srcDir, children[i]), new File(dstDir, children[i]));
            }
            return;
        }
        copyFile(srcDir, dstDir);
    }

    public static void copyFile(File src, File dst) throws IOException {
        copyStream(new FileInputStream(src), new FileOutputStream(dst));
    }

    public static void copyStreamAndLeaveInputOpen(InputStream in, OutputStream out) throws IOException {
        byte[] copyBuffer = new byte[16384];
        while (true) {
            int len = in.read(copyBuffer);
            if (len <= 0) {
                out.close();
                return;
            }
            out.write(copyBuffer, 0, len);
        }
    }

    public static void copyStream(InputStream in, OutputStream out) throws IOException {
        copyStreamAndLeaveInputOpen(in, out);
        in.close();
    }

    public static void saveFile(byte[] in, String path) throws IOException {
        File file = new File(path);
        file.getParentFile().mkdirs();
        FileOutputStream out = new FileOutputStream(file);
        out.write(in);
        out.close();
    }

    public static void saveStreamAndLeaveInputOpen(InputStream in, String path) throws IOException {
        File file = new File(path);
        file.getParentFile().mkdirs();
        copyStreamAndLeaveInputOpen(in, new FileOutputStream(file));
    }

    public static void saveStream(InputStream in, String path) throws IOException {
        saveStreamAndLeaveInputOpen(in, path);
        in.close();
    }

    public static DisplayMetrics getDisplayMetrics() {
        DisplayMetrics metrics = new DisplayMetrics();
        ((WindowManager) OpenFeintInternal.getInstance().getContext().getSystemService("window")).getDefaultDisplay().getMetrics(metrics);
        return metrics;
    }

    public static void run(String cmd) {
        try {
            Runtime.getRuntime().exec(cmd);
            OpenFeintInternal.log(TAG, cmd);
        } catch (Exception e) {
            OpenFeintInternal.log(TAG, e.getMessage());
        }
    }

    public static void createSymbolic(String dst, String src) {
        run("ln -s " + dst + " " + src);
    }

    public static boolean isSymblic(File f) {
        try {
            return !f.getCanonicalPath().equals(f.getAbsolutePath());
        } catch (IOException e) {
            return false;
        }
    }

    public static boolean sdcardReady(Context ctx) {
        if (!noSdcardPermission(ctx)) {
            return false;
        }
        if (!"mounted".equals(Environment.getExternalStorageState())) {
            return false;
        }
        return true;
    }

    public static void moveWebCache(Context ctx) {
        if (noSdcardPermission(ctx)) {
            File cache = new File(ctx.getCacheDir(), "webviewCache");
            if (!isSymblic(cache) && "mounted".equals(Environment.getExternalStorageState())) {
                File sdcard = new File(Environment.getExternalStorageDirectory(), "openfeint/cache");
                if (!sdcard.exists()) {
                    sdcard.mkdirs();
                }
                deleteFiles(cache);
                createSymbolic(sdcard.getAbsolutePath(), cache.getAbsolutePath());
            }
        }
    }

    public static boolean noPermission(String permission, Context ctx) {
        return -1 == ctx.getPackageManager().checkPermission(permission, ctx.getPackageName());
    }

    public static boolean noSdcardPermission() {
        return noSdcardPermission(OpenFeintInternal.getInstance().getContext());
    }

    public static boolean noSdcardPermission(Context ctx) {
        return noPermission("android.permission.WRITE_EXTERNAL_STORAGE", ctx);
    }

    public static byte[] readWholeFile(String path) throws IOException {
        File f = new File(path);
        InputStream in = new FileInputStream(f);
        byte[] b = new byte[((int) f.length())];
        in.read(b);
        in.close();
        return b;
    }

    public static Object getObjFromJsonFile(String path) {
        try {
            return getObjFromJsonStream(new FileInputStream(new File(path)));
        } catch (Exception e) {
            return null;
        }
    }

    public static Object getObjFromJsonStream(InputStream in) throws JsonParseException, IOException {
        Object obj = new JsonResourceParser(new JsonFactory().createJsonParser(in)).parse();
        in.close();
        return obj;
    }

    public static Object getObjFromJson(byte[] json) {
        try {
            return new JsonResourceParser(new JsonFactory().createJsonParser(json)).parse();
        } catch (Exception e) {
            OpenFeintInternal.log(TAG, e.getMessage());
            return null;
        }
    }

    public static Object getObjFromJson(byte[] json, ResourceClass resourceClass) {
        JsonFactory jsonFactory = new JsonFactory();
        OpenFeintInternal.log(TAG, new String(json));
        try {
            return new JsonResourceParser(jsonFactory.createJsonParser(json)).parse(resourceClass);
        } catch (Exception e) {
            OpenFeintInternal.log(TAG, String.valueOf(e.getMessage()) + "json error");
            return null;
        }
    }

    public static String getDpiName(Context ctx) {
        DisplayMetrics metrics = new DisplayMetrics();
        ((WindowManager) ctx.getSystemService("window")).getDefaultDisplay().getMetrics(metrics);
        if (metrics.density >= 2.0f) {
            return "udpi";
        }
        if (((double) metrics.density) >= 1.5d) {
            return "hdpi";
        }
        return "mdpi";
    }
}
