package com.openfeint.api;

public final class R {

    public static final class anim {
        public static final int appear = 2130968576;
        public static final int disappear = 2130968577;
        public static final int left_to_right = 2130968578;
        public static final int right_to_left = 2130968579;
    }

    public static final class attr {
        public static final int backgroundColor = 2130771968;
        public static final int keywords = 2130771971;
        public static final int primaryTextColor = 2130771969;
        public static final int refreshInterval = 2130771972;
        public static final int secondaryTextColor = 2130771970;
    }

    public static final class color {
        public static final int black = 2131034114;
        public static final int blue = 2131034121;
        public static final int blue_1 = 2131034133;
        public static final int blue_2 = 2131034134;
        public static final int blue_3 = 2131034135;
        public static final int blue_dark = 2131034136;
        public static final int cyan = 2131034125;
        public static final int darkblue = 2131034122;
        public static final int darkgray = 2131034113;
        public static final int gray = 2131034123;
        public static final int green = 2131034116;
        public static final int green_1 = 2131034130;
        public static final int green_2 = 2131034131;
        public static final int green_dark = 2131034132;
        public static final int lightblue = 2131034118;
        public static final int lightgray = 2131034117;
        public static final int magenta = 2131034124;
        public static final int of_transparent = 2131034112;
        public static final int opaque_red = 2131034126;
        public static final int red = 2131034115;
        public static final int translucent_red = 2131034127;
        public static final int transparent = 2131034129;
        public static final int transparent_white = 2131034128;
        public static final int white = 2131034119;
        public static final int yellow = 2131034120;
        public static final int yellow_1 = 2131034137;
        public static final int yellow_2 = 2131034138;
    }

    public static final class drawable {
        public static final int arrow_down = 2130837504;
        public static final int arrow_up = 2130837505;
        public static final int arrowleft = 2130837506;
        public static final int arrowright = 2130837507;
        public static final int bg_mappa = 2130837508;
        public static final int bg_mappa_blue = 2130837509;
        public static final int blue_button = 2130837510;
        public static final int bookmark = 2130837511;
        public static final int buttongreen = 2130837512;
        public static final int buttonwithe = 2130837513;
        public static final int buttonyellow = 2130837514;
        public static final int frame_layout_shape = 2130837515;
        public static final int green_button = 2130837516;
        public static final int icon = 2130837517;
        public static final int light = 2130837518;
        public static final int light_menu = 2130837519;
        public static final int light_off = 2130837520;
        public static final int lightblue_button = 2130837521;
        public static final int market = 2130837522;
        public static final int more = 2130837523;
        public static final int no_play = 2130837524;
        public static final int of_achievement_icon_frame = 2130837525;
        public static final int of_achievement_icon_locked = 2130837526;
        public static final int of_achievement_icon_unlocked = 2130837527;
        public static final int of_achievement_notification_bkg = 2130837528;
        public static final int of_achievement_notification_locked = 2130837529;
        public static final int of_feint_points_white = 2130837530;
        public static final int of_icon_dashboard_exit = 2130837531;
        public static final int of_icon_dashboard_home = 2130837532;
        public static final int of_icon_dashboard_settings = 2130837533;
        public static final int of_icon_highscore_notification = 2130837534;
        public static final int of_leaf = 2130837535;
        public static final int of_ll_logo = 2130837536;
        public static final int of_logo = 2130837537;
        public static final int of_native_loader = 2130837538;
        public static final int of_native_loader_frame = 2130837539;
        public static final int of_native_loader_leaf = 2130837540;
        public static final int of_native_loader_progress = 2130837541;
        public static final int of_native_loader_progress_01 = 2130837542;
        public static final int of_native_loader_progress_02 = 2130837543;
        public static final int of_native_loader_progress_03 = 2130837544;
        public static final int of_native_loader_progress_04 = 2130837545;
        public static final int of_native_loader_progress_05 = 2130837546;
        public static final int of_native_loader_progress_06 = 2130837547;
        public static final int of_native_loader_progress_07 = 2130837548;
        public static final int of_native_loader_progress_08 = 2130837549;
        public static final int of_native_loader_progress_09 = 2130837550;
        public static final int of_native_loader_progress_10 = 2130837551;
        public static final int of_native_loader_progress_11 = 2130837552;
        public static final int of_native_loader_progress_12 = 2130837553;
        public static final int of_notification_bkg = 2130837554;
        public static final int panel = 2130837555;
        public static final int pause = 2130837556;
        public static final int play = 2130837557;
        public static final int rect_blue = 2130837558;
        public static final int rect_green = 2130837559;
        public static final int rect_red = 2130837560;
        public static final int restore = 2130837561;
        public static final int sad = 2130837562;
        public static final int sad2 = 2130837563;
        public static final int sel_blue = 2130837564;
        public static final int sel_green = 2130837565;
        public static final int sel_red = 2130837566;
        public static final int settings = 2130837567;
        public static final int small_tiles = 2130837568;
        public static final int star = 2130837569;
        public static final int undo = 2130837570;
    }

    public static final class id {
        public static final int MainActivity = 2131296286;
        public static final int SchemaActivity = 2131296316;
        public static final int ad = 2131296292;
        public static final int btn1 = 2131296334;
        public static final int btn2 = 2131296335;
        public static final int btn3 = 2131296336;
        public static final int btnAutoHint = 2131296266;
        public static final int btnBig1 = 2131296257;
        public static final int btnBig2 = 2131296258;
        public static final int btnBig3 = 2131296259;
        public static final int btnBig4 = 2131296260;
        public static final int btnBig5 = 2131296261;
        public static final int btnBig6 = 2131296262;
        public static final int btnBig7 = 2131296263;
        public static final int btnBig8 = 2131296264;
        public static final int btnBig9 = 2131296265;
        public static final int btnChoose = 2131296289;
        public static final int btnDeleteAll = 2131296294;
        public static final int btnDeleteHints = 2131296295;
        public static final int btnHideMenu = 2131296296;
        public static final int btnMarket = 2131296291;
        public static final int btnOpenAch = 2131296284;
        public static final int btnOpenLeader = 2131296283;
        public static final int btnPlay = 2131296288;
        public static final int btnRestoreBookmark = 2131296298;
        public static final int btnSaveBookmark = 2131296297;
        public static final int btnScore = 2131296290;
        public static final int btnShowMenu = 2131296268;
        public static final int btnSmall1 = 2131296269;
        public static final int btnSmall2 = 2131296270;
        public static final int btnSmall3 = 2131296271;
        public static final int btnSmall4 = 2131296272;
        public static final int btnSmall5 = 2131296273;
        public static final int btnSmall6 = 2131296274;
        public static final int btnSmall7 = 2131296275;
        public static final int btnSmall8 = 2131296276;
        public static final int btnSmall9 = 2131296277;
        public static final int btnUndo = 2131296267;
        public static final int buttons = 2131296318;
        public static final int dialog_button = 2131296285;
        public static final int end_dialog = 2131296278;
        public static final int exit_feint = 2131296339;
        public static final int frameLayout = 2131296308;
        public static final int home = 2131296337;
        public static final int labName = 2131296256;
        public static final int llButton = 2131296287;
        public static final int lvSudokuList = 2131296315;
        public static final int menuGroup_Main = 2131296340;
        public static final int menu_autohint = 2131296342;
        public static final int menu_clear = 2131296341;
        public static final int menu_scema = 2131296321;
        public static final int nested_window_root = 2131296307;
        public static final int of_achievement_icon = 2131296300;
        public static final int of_achievement_icon_frame = 2131296301;
        public static final int of_achievement_notification = 2131296299;
        public static final int of_achievement_progress_icon = 2131296305;
        public static final int of_achievement_score = 2131296303;
        public static final int of_achievement_score_icon = 2131296304;
        public static final int of_achievement_text = 2131296302;
        public static final int of_icon = 2131296311;
        public static final int of_ll_logo_image = 2131296310;
        public static final int of_text = 2131296312;
        public static final int of_text1 = 2131296313;
        public static final int of_text2 = 2131296314;
        public static final int preview = 2131296330;
        public static final int progress = 2131296306;
        public static final int relative_easy = 2131296293;
        public static final int relative_epic = 2131296324;
        public static final int relative_hard = 2131296323;
        public static final int relative_medium = 2131296322;
        public static final int relative_special = 2131296325;
        public static final int rowID = 2131296326;
        public static final int rowLevel = 2131296329;
        public static final int rowName = 2131296328;
        public static final int rowStart = 2131296331;
        public static final int rowStop = 2131296332;
        public static final int sel_h = 2131296319;
        public static final int sel_v = 2131296320;
        public static final int settings = 2131296338;
        public static final int status = 2131296327;
        public static final int sudoku_view = 2131296317;
        public static final int text = 2131296333;
        public static final int txtCompleted = 2131296279;
        public static final int txtSchemaName = 2131296280;
        public static final int txtSchemaPoints = 2131296281;
        public static final int txtTotalScore = 2131296282;
        public static final int web_view = 2131296309;
    }

    public static final class layout {
        public static final int buttons = 2130903040;
        public static final int end_dialog = 2130903041;
        public static final int main = 2130903042;
        public static final int menu_schema = 2130903043;
        public static final int of_achievement_notification = 2130903044;
        public static final int of_native_loader = 2130903045;
        public static final int of_nested_window = 2130903046;
        public static final int of_simple_notification = 2130903047;
        public static final int of_two_line_notification = 2130903048;
        public static final int page = 2130903049;
        public static final int schema = 2130903050;
        public static final int sudokulist = 2130903051;
        public static final int sudokulist_item = 2130903052;
        public static final int table_cell = 2130903053;
        public static final int table_row = 2130903054;
    }

    public static final class menu {
        public static final int of_dashboard = 2131230720;
        public static final int schema_menu = 2131230721;
    }

    public static final class string {
        public static final int Achievements = 2131099722;
        public static final int AutoHintDisable = 2131099692;
        public static final int AutoHintEnable = 2131099691;
        public static final int ClearAll = 2131099693;
        public static final int ClearHints = 2131099694;
        public static final int Clues = 2131099711;
        public static final int Easy = 2131099698;
        public static final int Epic = 2131099701;
        public static final int Hard = 2131099700;
        public static final int Leaderboard = 2131099721;
        public static final int Medium = 2131099699;
        public static final int OnlineScore = 2131099713;
        public static final int Points = 2131099712;
        public static final int RestoreBookmark = 2131099720;
        public static final int SaveBookmark = 2131099719;
        public static final int Strange = 2131099702;
        public static final int TotalOnlineScore = 2131099718;
        public static final int app_name = 2131099696;
        public static final int btnChoose = 2131099697;
        public static final int btnOnlineScore = 2131099715;
        public static final int btnPlay = 2131099714;
        public static final int completed = 2131099717;
        public static final int congratulations = 2131099716;
        public static final int hello = 2131099695;
        public static final int of_achievement_load_null = 2131099656;
        public static final int of_achievement_unlock_null = 2131099655;
        public static final int of_achievement_unlocked = 2131099671;
        public static final int of_banned_dialog = 2131099684;
        public static final int of_bitmap_decode_error = 2131099673;
        public static final int of_cancel = 2131099667;
        public static final int of_cant_compress_blob = 2131099669;
        public static final int of_crash_report_query = 2131099681;
        public static final int of_device = 2131099662;
        public static final int of_error_parsing_error_message = 2131099675;
        public static final int of_exit_feint = 2131099686;
        public static final int of_file_not_found = 2131099674;
        public static final int of_home = 2131099683;
        public static final int of_id_cannot_be_null = 2131099650;
        public static final int of_io_exception_on_download = 2131099668;
        public static final int of_ioexception_reading_body = 2131099677;
        public static final int of_key_cannot_be_null = 2131099648;
        public static final int of_loading_feint = 2131099663;
        public static final int of_low_memory_profile_pic = 2131099658;
        public static final int of_malformed_request_error = 2131099690;
        public static final int of_name_cannot_be_null = 2131099651;
        public static final int of_no = 2131099664;
        public static final int of_no_blob = 2131099670;
        public static final int of_no_video = 2131099682;
        public static final int of_nodisk = 2131099660;
        public static final int of_now_logged_in_as_format = 2131099679;
        public static final int of_null_icon_url = 2131099654;
        public static final int of_offline_notification = 2131099687;
        public static final int of_offline_notification_line2 = 2131099688;
        public static final int of_ok = 2131099666;
        public static final int of_profile_pic_changed = 2131099680;
        public static final int of_profile_picture_download_failed = 2131099659;
        public static final int of_profile_url_null = 2131099657;
        public static final int of_score_submitted_notification = 2131099689;
        public static final int of_sdcard = 2131099661;
        public static final int of_secret_cannot_be_null = 2131099649;
        public static final int of_server_error_code_format = 2131099676;
        public static final int of_settings = 2131099685;
        public static final int of_switched_accounts = 2131099678;
        public static final int of_timeout = 2131099672;
        public static final int of_unexpected_response_format = 2131099652;
        public static final int of_unknown_server_error = 2131099653;
        public static final int of_yes = 2131099665;
        public static final int txtBack = 2131099707;
        public static final int txtHistory = 2131099704;
        public static final int txtNotStarted = 2131099710;
        public static final int txtPlay = 2131099703;
        public static final int txtScore = 2131099705;
        public static final int txtSettings = 2131099706;
        public static final int txtStartAt = 2131099708;
        public static final int txtStopAt = 2131099709;
    }

    public static final class style {
        public static final int OFLoading = 2131165184;
        public static final int OFNestedWindow = 2131165185;
    }

    public static final class styleable {
        public static final int[] com_admob_android_ads_AdView = {sudoku.challenge.lt.R.attr.backgroundColor, sudoku.challenge.lt.R.attr.primaryTextColor, sudoku.challenge.lt.R.attr.secondaryTextColor, sudoku.challenge.lt.R.attr.keywords, sudoku.challenge.lt.R.attr.refreshInterval};
        public static final int com_admob_android_ads_AdView_backgroundColor = 0;
        public static final int com_admob_android_ads_AdView_keywords = 3;
        public static final int com_admob_android_ads_AdView_primaryTextColor = 1;
        public static final int com_admob_android_ads_AdView_refreshInterval = 4;
        public static final int com_admob_android_ads_AdView_secondaryTextColor = 2;
    }
}
