package com.openfeint.api.resource;

import android.graphics.Bitmap;
import android.net.Uri;
import com.openfeint.internal.APICallback;
import com.openfeint.internal.OpenFeintInternal;
import com.openfeint.internal.request.BitmapRequest;
import com.openfeint.internal.request.JSONRequest;
import com.openfeint.internal.request.OrderedArgList;
import com.openfeint.internal.resource.BooleanResourceProperty;
import com.openfeint.internal.resource.DoubleResourceProperty;
import com.openfeint.internal.resource.IntResourceProperty;
import com.openfeint.internal.resource.Resource;
import com.openfeint.internal.resource.ResourceClass;
import com.openfeint.internal.resource.StringResourceProperty;
import java.util.List;
import sudoku.challenge.lt.R;

public class User extends Resource {
    public boolean followedByLocalUser;
    public boolean followsLocalUser;
    public int gamerScore;
    public String lastPlayedGameId;
    public String lastPlayedGameName;
    public double latitude;
    public double longitude;
    public String name;
    public boolean online;
    public String profilePictureSource;
    public String profilePictureUrl;
    public boolean usesFacebookProfilePicture;

    public static abstract class DownloadProfilePictureCB extends APICallback {
        public abstract void onSuccess(Bitmap bitmap);
    }

    public static abstract class FindCB extends APICallback {
        public abstract void onSuccess(User user);
    }

    public static abstract class GetFriendsCB extends APICallback {
        public abstract void onSuccess(List<User> list);
    }

    public static abstract class LoadCB extends APICallback {
        public abstract void onSuccess();
    }

    public User(String resourceID) {
        setResourceID(resourceID);
    }

    public String userID() {
        return resourceID();
    }

    public void load(final LoadCB cb) {
        new JSONRequest() {
            public String method() {
                return "GET";
            }

            public String path() {
                return "/xp/users/" + User.this.resourceID();
            }

            public void onSuccess(Object responseBody) {
                User.this.shallowCopyAncestorType((User) responseBody);
                if (cb != null) {
                    cb.onSuccess();
                }
            }

            public void onFailure(String exceptionMessage) {
                super.onFailure(exceptionMessage);
                if (cb != null) {
                    cb.onFailure(exceptionMessage);
                }
            }
        }.launch();
    }

    public static void findByName(final String name2, final FindCB cb) {
        new JSONRequest() {
            public String method() {
                return "GET";
            }

            public String path() {
                return "/xp/users/" + Uri.encode(name2);
            }

            public void onSuccess(Object responseBody) {
                if (cb != null) {
                    cb.onSuccess((User) responseBody);
                }
            }

            public void onFailure(String exceptionMessage) {
                super.onFailure(exceptionMessage);
                if (cb != null) {
                    cb.onFailure(exceptionMessage);
                }
            }
        }.launch();
    }

    public static void findByID(final String resourceID, final FindCB cb) {
        new JSONRequest() {
            public String method() {
                return "GET";
            }

            public String path() {
                return "/xp/users/" + resourceID;
            }

            public void onSuccess(Object responseBody) {
                if (cb != null) {
                    cb.onSuccess((User) responseBody);
                }
            }

            public void onFailure(String exceptionMessage) {
                super.onFailure(exceptionMessage);
                if (cb != null) {
                    cb.onFailure(exceptionMessage);
                }
            }
        }.launch();
    }

    public void getFriends(final GetFriendsCB cb) {
        OrderedArgList args = new OrderedArgList();
        args.put("user_id", resourceID());
        new JSONRequest(args) {
            public boolean wantsLogin() {
                return true;
            }

            public String method() {
                return "GET";
            }

            public String path() {
                return "/xp/friends";
            }

            public void onSuccess(Object responseBody) {
                if (cb != null) {
                    try {
                        cb.onSuccess((List) responseBody);
                    } catch (Exception e) {
                        onFailure(OpenFeintInternal.getRString(R.string.of_unexpected_response_format));
                    }
                }
            }

            public void onFailure(String exceptionMessage) {
                super.onFailure(exceptionMessage);
                if (cb != null) {
                    cb.onFailure(exceptionMessage);
                }
            }
        }.launch();
    }

    public void downloadProfilePicture(final DownloadProfilePictureCB cb) {
        if (this.profilePictureUrl != null) {
            new BitmapRequest() {
                public String url() {
                    return User.this.profilePictureUrl;
                }

                public String path() {
                    return "";
                }

                public void onSuccess(Bitmap responseBody) {
                    if (cb != null) {
                        cb.onSuccess(responseBody);
                    }
                }

                public void onFailure(String exceptionMessage) {
                    if (cb != null) {
                        cb.onFailure(exceptionMessage);
                    }
                }
            }.launch();
        } else if (cb != null) {
            cb.onFailure(OpenFeintInternal.getRString(R.string.of_profile_url_null));
        }
    }

    public User() {
    }

    public static ResourceClass getResourceClass() {
        ResourceClass klass = new ResourceClass(User.class, "user") {
            public Resource factory() {
                return new User();
            }
        };
        klass.mProperties.put("name", new StringResourceProperty() {
            public String get(Resource obj) {
                return ((User) obj).name;
            }

            public void set(Resource obj, String val) {
                ((User) obj).name = val;
            }
        });
        klass.mProperties.put("profile_picture_url", new StringResourceProperty() {
            public String get(Resource obj) {
                return ((User) obj).profilePictureUrl;
            }

            public void set(Resource obj, String val) {
                ((User) obj).profilePictureUrl = val;
            }
        });
        klass.mProperties.put("profile_picture_source", new StringResourceProperty() {
            public String get(Resource obj) {
                return ((User) obj).profilePictureSource;
            }

            public void set(Resource obj, String val) {
                ((User) obj).profilePictureSource = val;
            }
        });
        klass.mProperties.put("uses_facebook_profile_picture", new BooleanResourceProperty() {
            public boolean get(Resource obj) {
                return ((User) obj).usesFacebookProfilePicture;
            }

            public void set(Resource obj, boolean val) {
                ((User) obj).usesFacebookProfilePicture = val;
            }
        });
        klass.mProperties.put("last_played_game_id", new StringResourceProperty() {
            public String get(Resource obj) {
                return ((User) obj).lastPlayedGameId;
            }

            public void set(Resource obj, String val) {
                ((User) obj).lastPlayedGameId = val;
            }
        });
        klass.mProperties.put("last_played_game_name", new StringResourceProperty() {
            public String get(Resource obj) {
                return ((User) obj).lastPlayedGameName;
            }

            public void set(Resource obj, String val) {
                ((User) obj).lastPlayedGameName = val;
            }
        });
        klass.mProperties.put("gamer_score", new IntResourceProperty() {
            public int get(Resource obj) {
                return ((User) obj).gamerScore;
            }

            public void set(Resource obj, int val) {
                ((User) obj).gamerScore = val;
            }
        });
        klass.mProperties.put("following_local_user", new BooleanResourceProperty() {
            public boolean get(Resource obj) {
                return ((User) obj).followsLocalUser;
            }

            public void set(Resource obj, boolean val) {
                ((User) obj).followsLocalUser = val;
            }
        });
        klass.mProperties.put("followed_by_local_user", new BooleanResourceProperty() {
            public boolean get(Resource obj) {
                return ((User) obj).followedByLocalUser;
            }

            public void set(Resource obj, boolean val) {
                ((User) obj).followedByLocalUser = val;
            }
        });
        klass.mProperties.put("online", new BooleanResourceProperty() {
            public boolean get(Resource obj) {
                return ((User) obj).online;
            }

            public void set(Resource obj, boolean val) {
                ((User) obj).online = val;
            }
        });
        klass.mProperties.put("lat", new DoubleResourceProperty() {
            public double get(Resource obj) {
                return ((User) obj).latitude;
            }

            public void set(Resource obj, double val) {
                ((User) obj).latitude = val;
            }
        });
        klass.mProperties.put("lng", new DoubleResourceProperty() {
            public double get(Resource obj) {
                return ((User) obj).longitude;
            }

            public void set(Resource obj, double val) {
                ((User) obj).longitude = val;
            }
        });
        return klass;
    }
}
