package sudoku.challenge.lt;

import android.app.TabActivity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TabHost;
import java.util.ArrayList;

public class SudokuListActivity extends TabActivity {
    private Integer[] CurPage;
    /* access modifiers changed from: private */
    public Integer CurTab;
    private Integer[] MaxPage;
    SQLSchemaManager SchemaManager;
    SudokuListItemAdapter aaCurrent;
    View.OnTouchListener gestureListener;
    private ListView[] lvSudokuList;
    /* access modifiers changed from: private */
    public TabHost mTabHost;
    private int record_per_page;
    private View[] vPageList;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.sudokulist);
        this.SchemaManager = new SQLSchemaManager(getApplicationContext());
        this.CurPage = new Integer[5];
        this.MaxPage = new Integer[5];
        for (int i = 0; i < 5; i++) {
            this.CurPage[i] = 0;
            this.MaxPage[i] = 1;
        }
        this.CurTab = 0;
        this.mTabHost = getTabHost();
        this.mTabHost.addTab(this.mTabHost.newTabSpec("tab_0").setIndicator(getString(R.string.Easy)).setContent((int) R.id.relative_easy));
        this.mTabHost.addTab(this.mTabHost.newTabSpec("tab_1").setIndicator(getString(R.string.Medium)).setContent((int) R.id.relative_medium));
        this.mTabHost.addTab(this.mTabHost.newTabSpec("tab_2").setIndicator(getString(R.string.Hard)).setContent((int) R.id.relative_hard));
        this.mTabHost.addTab(this.mTabHost.newTabSpec("tab_3").setIndicator(getString(R.string.Epic)).setContent((int) R.id.relative_epic));
        this.mTabHost.addTab(this.mTabHost.newTabSpec("tab_4").setIndicator(getString(R.string.Strange)).setContent((int) R.id.relative_special));
        this.mTabHost.setCurrentTab(0);
        this.vPageList = new View[5];
        this.vPageList[0] = this.mTabHost.findViewById(R.id.relative_easy);
        this.vPageList[1] = this.mTabHost.findViewById(R.id.relative_medium);
        this.vPageList[2] = this.mTabHost.findViewById(R.id.relative_hard);
        this.vPageList[3] = this.mTabHost.findViewById(R.id.relative_epic);
        this.vPageList[4] = this.mTabHost.findViewById(R.id.relative_special);
        this.lvSudokuList = new ListView[5];
        for (int i2 = 0; i2 < 5; i2++) {
            this.lvSudokuList[i2] = (ListView) this.vPageList[i2].findViewById(R.id.lvSudokuList);
            this.lvSudokuList[i2].setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {
                    Intent myIntent = new Intent(view.getContext(), SchemaActivity.class);
                    int rowId = ((SchemaSmall) SudokuListActivity.this.aaCurrent.getItem(pos)).ID;
                    Bundle bundle = new Bundle();
                    bundle.putInt("SchemaID", rowId);
                    myIntent.putExtras(bundle);
                    SudokuListActivity.this.startActivityForResult(myIntent, 0);
                }
            });
        }
        this.mTabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            public void onTabChanged(String tabId) {
                SudokuListActivity.this.CurTab = Integer.valueOf(SudokuListActivity.this.mTabHost.getCurrentTab());
                if (tabId.equals("tab_0")) {
                    SudokuListActivity.this.CurTab = 0;
                    SudokuListActivity.this.UpdatePageList();
                }
                if (tabId.equals("tab_1")) {
                    SudokuListActivity.this.CurTab = 1;
                    SudokuListActivity.this.UpdatePageList();
                }
                if (tabId.equals("tab_2")) {
                    SudokuListActivity.this.CurTab = 2;
                    SudokuListActivity.this.UpdatePageList();
                }
                if (tabId.equals("tab_3")) {
                    SudokuListActivity.this.CurTab = 3;
                    SudokuListActivity.this.UpdatePageList();
                }
                if (tabId.equals("tab_4")) {
                    SudokuListActivity.this.CurTab = 4;
                    SudokuListActivity.this.UpdatePageList();
                }
            }
        });
        UpdatePageList();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        UpdatePageList();
    }

    /* access modifiers changed from: private */
    public void UpdatePageList() {
        String Level_from = "0";
        String Level_to = "1";
        String Special = "";
        ArrayList<SchemaSmall> lstAvailableSudoku = new ArrayList<>();
        if (this.CurTab.intValue() == 0) {
            Level_from = "0";
            Level_to = "400";
            Special = "0";
        }
        if (this.CurTab.intValue() == 1) {
            Level_from = "400";
            Level_to = "900";
            Special = "0";
        }
        if (this.CurTab.intValue() == 2) {
            Level_from = "900";
            Level_to = "4000";
            Special = "0";
        }
        if (this.CurTab.intValue() == 3) {
            Level_from = "4000";
            Level_to = "200000";
            Special = "0";
        }
        if (this.CurTab.intValue() == 4) {
            Level_from = "0";
            Level_to = "200000";
            Special = "1";
        }
        Cursor C = this.SchemaManager.CreateCursor("SELECT * FROM History WHERE Points>=" + Level_from + " AND Points<" + Level_to + " AND Strange=" + Special + " ORDER BY Points ;");
        if (C != null) {
            this.record_per_page = 4000;
            try {
                int n = C.getCount();
                if (n != 0) {
                    this.MaxPage[this.CurTab.intValue()] = Integer.valueOf(n / this.record_per_page);
                    for (int i = this.CurPage[this.CurTab.intValue()].intValue() * this.record_per_page; i < (this.CurPage[this.CurTab.intValue()].intValue() + 1) * this.record_per_page; i++) {
                        C.moveToPosition(i);
                        SchemaSmall s = new SchemaSmall();
                        this.SchemaManager.GetSchemaSmallFromCursor(s, C);
                        lstAvailableSudoku.add(s);
                        if (i >= n - 1) {
                            break;
                        }
                    }
                    this.aaCurrent = new SudokuListItemAdapter(this, R.layout.sudokulist_item, lstAvailableSudoku);
                    this.lvSudokuList[this.CurTab.intValue()].setAdapter((ListAdapter) this.aaCurrent);
                    C.close();
                }
            } catch (Exception e) {
                Log.d("Ex", e.getMessage());
            }
        }
    }
}
