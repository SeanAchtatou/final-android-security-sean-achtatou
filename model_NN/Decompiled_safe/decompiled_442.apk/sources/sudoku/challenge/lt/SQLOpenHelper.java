package sudoku.challenge.lt;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class SQLOpenHelper extends SQLiteOpenHelper {
    private static String DB_NAME = "NoName";
    private static String DB_PATH = "/data/data/sudoku.challenge.lt/databases/";
    private final Context myContext;
    public SQLiteDatabase myDataBase;

    public SQLOpenHelper(Context context, String name) {
        super(context, name, (SQLiteDatabase.CursorFactory) null, 1);
        DB_NAME = name;
        this.myContext = context;
    }

    public void createDataBase() {
        if (!checkDataBase()) {
            getReadableDatabase();
            copyDataBase();
        }
    }

    private boolean checkDataBase() {
        SQLiteDatabase checkDB = null;
        try {
            checkDB = SQLiteDatabase.openDatabase(String.valueOf(DB_PATH) + DB_NAME, null, 0);
        } catch (SQLiteException e) {
        }
        if (checkDB != null) {
            checkDB.close();
        }
        return checkDB != null;
    }

    private void copyDataBase() {
        try {
            InputStream myInput = this.myContext.getAssets().open(DB_NAME);
            OutputStream myOutput = new FileOutputStream(String.valueOf(DB_PATH) + DB_NAME);
            byte[] buffer = new byte[1024];
            while (true) {
                int length = myInput.read(buffer);
                if (length <= 0) {
                    myOutput.flush();
                    myOutput.close();
                    myInput.close();
                    return;
                }
                myOutput.write(buffer, 0, length);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void openDataBase() {
        try {
            this.myDataBase = SQLiteDatabase.openDatabase(String.valueOf(DB_PATH) + DB_NAME, null, 0);
        } catch (Exception e) {
            Log.d("DB", e.getMessage());
        }
    }

    public synchronized void close() {
        if (this.myDataBase != null) {
            this.myDataBase.close();
        }
        super.close();
    }

    public void onCreate(SQLiteDatabase db) {
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }
}
