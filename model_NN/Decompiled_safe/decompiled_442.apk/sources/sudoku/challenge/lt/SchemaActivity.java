package sudoku.challenge.lt;

import android.app.Activity;
import android.app.Dialog;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import com.openfeint.api.OpenFeint;
import com.openfeint.api.OpenFeintDelegate;
import com.openfeint.api.OpenFeintSettings;
import com.openfeint.api.resource.Achievement;
import com.openfeint.api.resource.Leaderboard;
import com.openfeint.api.resource.Score;
import com.openfeint.api.ui.Dashboard;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import sudoku.challenge.lt.map.SudokuCell;
import sudoku.challenge.lt.map.UndoMove;

public class SchemaActivity extends Activity {
    public boolean AutoHintEnabled;
    private Button[] BigButtons;
    private Integer CompletedEasy;
    private Integer CompletedEpic;
    private Integer CompletedHard;
    private Integer CompletedMedium;
    private Integer CompletedSpecial;
    public Schema CurrentSchema;
    private Date DateNull;
    private Button DeleteAll;
    private Button DeleteHints;
    private ImageButton HideMenu;
    public boolean IsCurrentSchemaPresent;
    private View PanelMenu;
    private Button RestoreSnapshot;
    public SchemaView SV;
    private Button SaveSnapshot;
    private SQLSchemaManager SchemaManager;
    private ImageButton ShowMenu;
    private Button[] SmallButtons;
    private Integer SudokuDelete;
    public SimpleDateFormat TimeFormatter;
    private Integer TotalEasy;
    private Integer TotalEpic;
    private Integer TotalHard;
    private Integer TotalMedium;
    private Integer TotalSpecial;
    private ArrayList<UndoMove> UndoList;
    private AdView adView;
    private ImageButton btnAutoHint;
    private ImageButton btnUndo;
    private MenuItem itAutoHint;
    private Integer nCells;
    private TextView tvName;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        Log.d("SUDOKU", "Sudoku Activity start");
        try {
            setContentView(R.layout.schema);
        } catch (Exception e) {
            e.getMessage();
        }
        this.adView = new AdView(this, AdSize.BANNER, "a14d604712e804b");
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(12, -1);
        ((RelativeLayout) findViewById(R.id.SchemaActivity)).addView(this.adView, layoutParams);
        this.adView.loadAd(new AdRequest());
        OpenFeint.initialize(this, new OpenFeintSettings("Sudoku challenge", "QAALk8LPZioOXlYe8szIA", "7ichMe7ZsCxqqPXHbpMRf709uTKnSmiESn8zLOXq0", "193182"), new OpenFeintDelegate() {
        });
        this.TimeFormatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        try {
            this.DateNull = this.TimeFormatter.parse("2010/01/01 00:00:00");
        } catch (ParseException e2) {
        }
        this.SV = (SchemaView) findViewById(R.id.sudoku_view);
        this.SV.sel_h = (ImageView) findViewById(R.id.sel_h);
        this.SV.sel_v = (ImageView) findViewById(R.id.sel_v);
        this.btnAutoHint = (ImageButton) findViewById(R.id.btnAutoHint);
        this.btnAutoHint.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (!SchemaActivity.this.CurrentSchema.isEnded()) {
                    SchemaActivity.this.AutoHintEnabled = !SchemaActivity.this.AutoHintEnabled;
                    SchemaActivity.this.UpdateAutoHintView();
                    if (SchemaActivity.this.AutoHintEnabled) {
                        SchemaActivity.this.CurrentSchema.CalcHints();
                        SchemaActivity.this.SV.DrawCurrentSchema();
                        SchemaActivity.this.SV.invalidate();
                        SchemaActivity.this.UpdateTime();
                        SchemaActivity.this.UpdateButtons();
                        SchemaActivity.this.SaveSchema();
                    }
                }
            }
        });
        this.PanelMenu = findViewById(R.id.menu_scema);
        this.ShowMenu = (ImageButton) findViewById(R.id.btnShowMenu);
        this.ShowMenu.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SchemaActivity.this.ShowMenu(true);
            }
        });
        this.HideMenu = (ImageButton) this.PanelMenu.findViewById(R.id.btnHideMenu);
        this.HideMenu.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SchemaActivity.this.ShowMenu(false);
            }
        });
        this.DeleteAll = (Button) this.PanelMenu.findViewById(R.id.btnDeleteAll);
        this.DeleteAll.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (!SchemaActivity.this.CurrentSchema.isEnded() && SchemaActivity.this.AreYouSure("Text")) {
                    SchemaActivity.this.AutoHintEnabled = false;
                    SchemaActivity.this.UpdateAutoHintView();
                    SchemaActivity.this.CurrentSchema.ClearUserData();
                    SchemaActivity.this.SV.SelectedNumber = 0;
                    SchemaActivity.this.SV.DrawCurrentSchema();
                    SchemaActivity.this.SV.invalidate();
                    SchemaActivity.this.UpdateButtons();
                    SchemaActivity.this.UpdateTime();
                    SchemaActivity.this.SaveSchema();
                    SchemaActivity.this.ShowMenu(false);
                    boolean unused = SchemaActivity.this.AddAchievement("SudokuDelete", 1);
                }
            }
        });
        this.DeleteHints = (Button) this.PanelMenu.findViewById(R.id.btnDeleteHints);
        this.DeleteHints.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (!SchemaActivity.this.CurrentSchema.isEnded() && SchemaActivity.this.AreYouSure("Text")) {
                    SchemaActivity.this.AutoHintEnabled = false;
                    SchemaActivity.this.UpdateAutoHintView();
                    SchemaActivity.this.CurrentSchema.ClearUserHints();
                    SchemaActivity.this.SV.DrawCurrentSchema();
                    SchemaActivity.this.SV.invalidate();
                    SchemaActivity.this.UpdateButtons();
                    SchemaActivity.this.UpdateTime();
                    SchemaActivity.this.SaveSchema();
                    SchemaActivity.this.ShowMenu(false);
                }
            }
        });
        this.SaveSnapshot = (Button) this.PanelMenu.findViewById(R.id.btnSaveBookmark);
        this.SaveSnapshot.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (!SchemaActivity.this.CurrentSchema.isEnded()) {
                    SchemaActivity.this.CurrentSchema.SaveSnapshot();
                    SchemaActivity.this.SaveSchema();
                    SchemaActivity.this.ShowMenu(false);
                }
            }
        });
        this.RestoreSnapshot = (Button) this.PanelMenu.findViewById(R.id.btnRestoreBookmark);
        this.RestoreSnapshot.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SchemaActivity.this.ClearUndo();
                if (!SchemaActivity.this.CurrentSchema.isEnded()) {
                    SchemaActivity.this.CurrentSchema.RestoreSnapshot();
                    if (SchemaActivity.this.AutoHintEnabled) {
                        SchemaActivity.this.CurrentSchema.CalcHints();
                    }
                    SchemaActivity.this.CurrentSchema.CheckSchemaValues();
                    SchemaActivity.this.SV.SelectedNumber = 0;
                    SchemaActivity.this.SV.DrawCurrentSchema();
                    SchemaActivity.this.SV.invalidate();
                    SchemaActivity.this.UpdateButtons();
                    SchemaActivity.this.UpdateTime();
                    SchemaActivity.this.SaveSchema();
                }
            }
        });
        this.btnUndo = (ImageButton) findViewById(R.id.btnUndo);
        this.btnUndo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SchemaActivity.this.PopUndo();
                SchemaActivity.this.SV.DrawCurrentSchema();
                SchemaActivity.this.SV.invalidate();
                SchemaActivity.this.UpdateButtons();
                SchemaActivity.this.UpdateTime();
                SchemaActivity.this.SaveSchema();
            }
        });
        this.BigButtons = new Button[9];
        this.SmallButtons = new Button[9];
        this.BigButtons[0] = (Button) findViewById(R.id.btnBig1);
        this.BigButtons[1] = (Button) findViewById(R.id.btnBig2);
        this.BigButtons[2] = (Button) findViewById(R.id.btnBig3);
        this.BigButtons[3] = (Button) findViewById(R.id.btnBig4);
        this.BigButtons[4] = (Button) findViewById(R.id.btnBig5);
        this.BigButtons[5] = (Button) findViewById(R.id.btnBig6);
        this.BigButtons[6] = (Button) findViewById(R.id.btnBig7);
        this.BigButtons[7] = (Button) findViewById(R.id.btnBig8);
        this.BigButtons[8] = (Button) findViewById(R.id.btnBig9);
        this.SmallButtons[0] = (Button) findViewById(R.id.btnSmall1);
        this.SmallButtons[1] = (Button) findViewById(R.id.btnSmall2);
        this.SmallButtons[2] = (Button) findViewById(R.id.btnSmall3);
        this.SmallButtons[3] = (Button) findViewById(R.id.btnSmall4);
        this.SmallButtons[4] = (Button) findViewById(R.id.btnSmall5);
        this.SmallButtons[5] = (Button) findViewById(R.id.btnSmall6);
        this.SmallButtons[6] = (Button) findViewById(R.id.btnSmall7);
        this.SmallButtons[7] = (Button) findViewById(R.id.btnSmall8);
        this.SmallButtons[8] = (Button) findViewById(R.id.btnSmall9);
        for (int i = 0; i < 9; i++) {
            this.BigButtons[i].setTag(Integer.valueOf(i + 1));
            this.BigButtons[i].setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    if (SchemaActivity.this.SV.SelectedCell.x >= 0 && SchemaActivity.this.SV.SelectedCell.x <= 8 && SchemaActivity.this.SV.SelectedCell.y >= 0 && SchemaActivity.this.SV.SelectedCell.y <= 8) {
                        UndoMove um = new UndoMove();
                        um.Col = Integer.valueOf(SchemaActivity.this.SV.SelectedCell.x);
                        um.Row = Integer.valueOf(SchemaActivity.this.SV.SelectedCell.y);
                        um.From = SchemaActivity.this.CurrentSchema.Mappa[um.Row.intValue()][um.Col.intValue()].valoreSchema;
                        SchemaActivity.this.SetValue(SchemaActivity.this.SV.SelectedCell, new Integer(view.getTag().toString()).intValue());
                        um.To = SchemaActivity.this.CurrentSchema.Mappa[um.Row.intValue()][um.Col.intValue()].valoreSchema;
                        if (um.From != um.To) {
                            SchemaActivity.this.PushUndo(um);
                        }
                    }
                }
            });
            this.SmallButtons[i].setTag(Integer.valueOf(i + 1));
            this.SmallButtons[i].setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    if (SchemaActivity.this.SV.SelectedCell.x >= 0 && SchemaActivity.this.SV.SelectedCell.x <= 8 && SchemaActivity.this.SV.SelectedCell.y >= 0 && SchemaActivity.this.SV.SelectedCell.y <= 8) {
                        UndoMove um = new UndoMove();
                        um.Col = Integer.valueOf(SchemaActivity.this.SV.SelectedCell.x);
                        um.Row = Integer.valueOf(SchemaActivity.this.SV.SelectedCell.y);
                        um.From = SchemaActivity.this.CurrentSchema.Mappa[um.Row.intValue()][um.Col.intValue()].valoreSchema;
                        SchemaActivity.this.SetOptionValue(SchemaActivity.this.SV.SelectedCell, new Integer(view.getTag().toString()).intValue());
                        um.To = SchemaActivity.this.CurrentSchema.Mappa[um.Row.intValue()][um.Col.intValue()].valoreSchema;
                        if (um.From != um.To) {
                            SchemaActivity.this.PushUndo(um);
                        }
                    }
                }
            });
        }
        Log.d("SUDOKU", "Sudoku SQL Database");
        this.SchemaManager = new SQLSchemaManager(getApplicationContext());
        Log.d("SUDOKU", "Sudoku Assegna mappa");
        this.CurrentSchema = new Schema(this);
        int ID_S = getIntent().getExtras().getInt("SchemaID");
        this.IsCurrentSchemaPresent = this.SchemaManager.GetSchemaByID(this.CurrentSchema, ID_S);
        Display display = getWindowManager().getDefaultDisplay();
        this.SV.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                int actionCode = event.getAction();
                int x = SchemaActivity.this.SV.getLeft();
                int y = SchemaActivity.this.SV.getTop();
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(SchemaActivity.this.SV.getLayoutParams());
                layoutParams.setMargins(x, y, 0, 0);
                layoutParams.height = SchemaActivity.this.SV.CellWidth;
                SchemaActivity.this.SV.sel_h.setLayoutParams(layoutParams);
                RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(SchemaActivity.this.SV.getLayoutParams());
                layoutParams2.setMargins(x, y, 0, 0);
                layoutParams2.width = SchemaActivity.this.SV.CellWidth;
                SchemaActivity.this.SV.sel_v.setLayoutParams(layoutParams2);
                if (actionCode == 1) {
                    int c = (int) (event.getX() / ((float) SchemaActivity.this.SV.CellWidth));
                    int r = (int) (event.getY() / ((float) SchemaActivity.this.SV.CellWidth));
                    if (r < 0) {
                        return true;
                    }
                    if (c < 0) {
                        return true;
                    }
                    if (r > 8) {
                        return true;
                    }
                    if (c > 8) {
                        return true;
                    }
                    SchemaActivity.this.SV.SelectCell(r, c);
                    SchemaActivity.this.SV.DrawCurrentSchema();
                    SchemaActivity.this.UpdateButtons();
                    SchemaActivity.this.SV.invalidate();
                }
                return false;
            }
        });
        ViewGroup.LayoutParams l = this.SV.getLayoutParams();
        this.SV.GridRect.right = display.getWidth() - 4;
        this.SV.GridRect.bottom = display.getHeight() - 175;
        this.SV.GridRect.right = (this.SV.GridRect.right / 9) * 9;
        this.SV.GridRect.bottom = (this.SV.GridRect.bottom / 9) * 9;
        if (this.SV.GridRect.right > this.SV.GridRect.bottom) {
            this.SV.GridRect.right = this.SV.GridRect.bottom;
        } else {
            this.SV.GridRect.bottom = this.SV.GridRect.right;
        }
        l.width = this.SV.GridRect.right;
        l.height = this.SV.GridRect.right;
        this.SV.setLayoutParams(l);
        ViewGroup.LayoutParams lp = this.SV.sel_h.getLayoutParams();
        lp.width = this.SV.GridRect.width();
        lp.height = this.SV.GridRect.height() / 9;
        this.SV.sel_h.setLayoutParams(lp);
        ViewGroup.LayoutParams lp2 = this.SV.sel_v.getLayoutParams();
        lp2.height = this.SV.GridRect.height();
        lp2.width = this.SV.GridRect.width() / 9;
        this.SV.sel_v.setLayoutParams(lp2);
        this.SV.CurSchema = this.CurrentSchema;
        this.SV.ResizeSchema();
        Log.d("SUDOKU", "Sudoku Draw");
        this.SV.CurSchema.CheckSchemaValues();
        this.SV.DrawCurrentSchema();
        this.SV.invalidate();
        this.tvName = (TextView) findViewById(R.id.labName);
        this.tvName.setText(String.valueOf(ID_S) + " - " + this.CurrentSchema.Name + ": " + this.CurrentSchema.Points + " " + getString(R.string.Points));
        this.AutoHintEnabled = false;
        UpdateAutoHintView();
        this.CompletedEasy = Integer.valueOf(Integer.parseInt(GetPreference("CompletedEasy", "0")));
        this.CompletedMedium = Integer.valueOf(Integer.parseInt(GetPreference("CompletedMedium", "0")));
        this.CompletedHard = Integer.valueOf(Integer.parseInt(GetPreference("CompletedHard", "0")));
        this.CompletedEpic = Integer.valueOf(Integer.parseInt(GetPreference("CompletedEpic", "0")));
        this.CompletedSpecial = Integer.valueOf(Integer.parseInt(GetPreference("CompletedSpecial", "0")));
        this.nCells = Integer.valueOf(Integer.parseInt(GetPreference("nCells", "0")));
        this.TotalEasy = Integer.valueOf(Integer.parseInt(GetPreference("TotalEasy", "0")));
        this.TotalMedium = Integer.valueOf(Integer.parseInt(GetPreference("TotalMedium", "0")));
        this.TotalHard = Integer.valueOf(Integer.parseInt(GetPreference("TotalHard", "0")));
        this.TotalEpic = Integer.valueOf(Integer.parseInt(GetPreference("TotalEpic", "0")));
        this.TotalSpecial = Integer.valueOf(Integer.parseInt(GetPreference("TotalSpecial", "0")));
        String sTmp = GetPreference("SudokuMaster", "0");
        String sTmp2 = GetPreference("SudokuGrandMaster", "0");
        this.SudokuDelete = Integer.valueOf(Integer.parseInt(GetPreference("SudokuDelete", "0")));
        this.UndoList = new ArrayList<>();
        Log.d("SUDOKU", "Sudoku Activity end OnCreate");
    }

    /* access modifiers changed from: protected */
    public void ShowMenu(boolean vis) {
        boolean z;
        boolean z2;
        this.PanelMenu.setEnabled(vis);
        for (int i = 0; i < 9; i++) {
            Button button = this.SmallButtons[i];
            if (vis) {
                z = false;
            } else {
                z = true;
            }
            button.setEnabled(z);
            Button button2 = this.BigButtons[i];
            if (vis) {
                z2 = false;
            } else {
                z2 = true;
            }
            button2.setEnabled(z2);
        }
        if (vis) {
            this.PanelMenu.setVisibility(0);
        } else {
            this.PanelMenu.setVisibility(4);
        }
    }

    /* access modifiers changed from: private */
    public void UpdateAutoHintView() {
        if (this.AutoHintEnabled) {
            this.btnAutoHint.setImageResource(R.drawable.light);
            this.btnAutoHint.setBackgroundResource(R.drawable.lightblue_button);
            return;
        }
        this.btnAutoHint.setImageResource(R.drawable.light_off);
        this.btnAutoHint.setBackgroundResource(R.drawable.blue_button);
    }

    /* access modifiers changed from: private */
    public void SetValue(Point cell, int Value) {
        if (cell.x >= 0 && cell.y >= 0 && cell.x <= 8 && cell.y <= 8 && !this.CurrentSchema.isEnded()) {
            int res = this.CurrentSchema.SetValue(cell, Value);
            if (this.AutoHintEnabled) {
                this.CurrentSchema.CalcHints();
            }
            if (res > 0) {
                this.CurrentSchema.CheckCompleted();
                if (this.CurrentSchema.isEnded()) {
                    OnSchemaCompleted();
                }
                AddAchievement("nCells", 1);
            }
            this.SV.SelectCell(cell.y, cell.x);
            UpdateButtons();
            this.SV.DrawCurrentSchema();
            this.SV.invalidate();
            UpdateTime();
            SaveSchema();
        }
    }

    /* access modifiers changed from: private */
    public void UpdateButtons() {
        if (this.SV.SelectedCell.y >= 0 && this.SV.SelectedCell.x >= 0) {
            SudokuCell SelC = this.CurrentSchema.Mappa[this.SV.SelectedCell.y][this.SV.SelectedCell.x];
            for (int i = 0; i < 9; i++) {
                if (SelC.valoreSchema != i + 1 || !SelC.Editable) {
                    this.BigButtons[i].setBackgroundResource(R.drawable.blue_button);
                } else {
                    this.BigButtons[i].setBackgroundResource(R.drawable.lightblue_button);
                }
                if (SelC.opzioni[i] <= 0 || !SelC.Editable) {
                    this.SmallButtons[i].setBackgroundResource(R.drawable.blue_button);
                } else {
                    this.SmallButtons[i].setBackgroundResource(R.drawable.lightblue_button);
                }
            }
        }
    }

    private void OnSchemaCompleted() {
        int l = this.CurrentSchema.GetSchemaLevel();
        if (l == 0) {
            this.TotalEasy = Integer.valueOf(this.TotalEasy.intValue() + this.CurrentSchema.Points);
            AddLeaderboard("TotalEasy", this.TotalEasy);
            AddAchievement("CompletedEasy", 1);
        } else if (l == 1) {
            this.TotalMedium = Integer.valueOf(this.TotalMedium.intValue() + this.CurrentSchema.Points);
            AddLeaderboard("TotalMedium", this.TotalMedium);
            AddAchievement("CompletedMedium", 1);
        } else if (l == 2) {
            this.TotalHard = Integer.valueOf(this.TotalHard.intValue() + this.CurrentSchema.Points);
            AddLeaderboard("TotalHard", this.TotalHard);
            AddAchievement("CompletedHard", 1);
        } else if (l == 3) {
            this.TotalEpic = Integer.valueOf(this.TotalEpic.intValue() + this.CurrentSchema.Points);
            AddLeaderboard("TotalEpic", this.TotalEpic);
            AddAchievement("CompletedEpic", 1);
        } else if (l == 4) {
            this.TotalSpecial = Integer.valueOf(this.TotalSpecial.intValue() + this.CurrentSchema.Points);
            AddLeaderboard("TotalSpecial", this.TotalSpecial);
            AddAchievement("CompletedSpecial", 1);
        }
        if (this.CurrentSchema.ID == 1107) {
            AddAchievement("SudokuMaster", 1);
        }
        if (this.CurrentSchema.ID == 1108) {
            AddAchievement("SudokuGrandMaster", 1);
        }
        Dialog d = new Dialog(this);
        d.getWindow().setFlags(4, 4);
        d.setContentView((int) R.layout.end_dialog);
        d.setTitle((int) R.string.congratulations);
        ((TextView) d.findViewById(R.id.txtSchemaName)).setText(String.valueOf(this.CurrentSchema.ID) + " - " + this.CurrentSchema.Name);
        ((TextView) d.findViewById(R.id.txtSchemaPoints)).setText(String.valueOf(getString(R.string.Points)) + ": +" + this.CurrentSchema.Points);
        Integer n = Integer.valueOf(this.CurrentSchema.GetSchemaLevel());
        String Sc = "";
        if (n.intValue() == 0) {
            Sc = this.TotalEasy.toString();
        }
        if (n.intValue() == 1) {
            Sc = this.TotalMedium.toString();
        }
        if (n.intValue() == 2) {
            Sc = this.TotalHard.toString();
        }
        if (n.intValue() == 3) {
            Sc = this.TotalEpic.toString();
        }
        if (n.intValue() == 4) {
            Sc = this.TotalSpecial.toString();
        }
        ((TextView) d.findViewById(R.id.txtTotalScore)).setText(String.valueOf(getString(R.string.TotalOnlineScore)) + " " + Sc);
        d.show();
        ((Button) d.findViewById(R.id.dialog_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                SchemaActivity.this.finish();
            }
        });
        ((Button) d.findViewById(R.id.btnOpenLeader)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Dashboard.openLeaderboard("545414");
                SchemaActivity.this.finish();
            }
        });
        ((Button) d.findViewById(R.id.btnOpenAch)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Dashboard.openAchievements();
                SchemaActivity.this.finish();
            }
        });
    }

    /* access modifiers changed from: private */
    public void SaveSchema() {
        this.SchemaManager.UpdateSchema(this.CurrentSchema);
    }

    /* access modifiers changed from: private */
    public void SetOptionValue(Point cell, int Value) {
        if (cell.x >= 0 && cell.y >= 0 && cell.x <= 8 && cell.y <= 8 && !this.CurrentSchema.isEnded()) {
            SudokuCell sc = this.CurrentSchema.Mappa[cell.y][cell.x];
            if (sc.Editable) {
                if (sc.valoreSchema != 0) {
                    for (int i = 0; i < 9; i++) {
                        sc.removeOption(i);
                    }
                    sc.opzioni[Value - 1] = Value;
                    sc.valoreSchema = 0;
                } else if (sc.opzioni[Value - 1] == Value) {
                    sc.opzioni[Value - 1] = 0;
                } else {
                    sc.opzioni[Value - 1] = Value;
                }
                UpdateButtons();
                this.SV.DrawCurrentSchema();
                this.SV.invalidate();
                UpdateTime();
                SaveSchema();
            }
        }
    }

    /* access modifiers changed from: private */
    public void UpdateTime() {
        Date Adesso = new Date();
        if (this.CurrentSchema.Start.before(this.DateNull)) {
            this.CurrentSchema.Start = Adesso;
        }
        this.CurrentSchema.LastUpdate = Adesso;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int iid = item.getItemId();
        if (iid == R.id.menu_clear) {
            if (AreYouSure("Text")) {
                this.CurrentSchema.ClearUserData();
                this.SV.DrawCurrentSchema();
                this.SV.invalidate();
                UpdateTime();
                SaveSchema();
            }
        } else if (iid == R.id.menu_autohint) {
            this.AutoHintEnabled = !this.AutoHintEnabled;
            if (this.AutoHintEnabled) {
                SetPreference("AutoHintEnabled", "true");
                this.itAutoHint.setTitle(getString(R.string.AutoHintDisable));
            } else {
                SetPreference("AutoHintEnabled", "false");
                this.itAutoHint.setTitle(getString(R.string.AutoHintEnable));
            }
            UpdateAutoHintView();
            this.itAutoHint.setChecked(this.AutoHintEnabled);
        }
        return true;
    }

    /* access modifiers changed from: private */
    public boolean AreYouSure(String title) {
        return true;
    }

    private String GetPreference(String Name, String Default) {
        return getSharedPreferences("Sudoku", 0).getString(Name, Default);
    }

    private boolean SetPreference(String Name, String NewValue) {
        SharedPreferences.Editor editor = getSharedPreferences("Sudoku", 0).edit();
        editor.putString(Name, NewValue);
        return editor.commit();
    }

    /* access modifiers changed from: private */
    public boolean AddAchievement(String Name, int Value) {
        String Res = "";
        if (Name.equalsIgnoreCase("CompletedEasy")) {
            this.CompletedEasy = Integer.valueOf(this.CompletedEasy.intValue() + 1);
            SetPreference("CompletedEasy", this.CompletedEasy.toString());
            if (this.CompletedEasy.intValue() == 1) {
                Res = "680272";
            }
            if (this.CompletedEasy.intValue() == 10) {
                Res = "707782";
            }
            if (this.CompletedEasy.intValue() == 50) {
                Res = "680272";
            }
        } else if (Name.equalsIgnoreCase("CompletedMedium")) {
            this.CompletedMedium = Integer.valueOf(this.CompletedMedium.intValue() + 1);
            SetPreference("CompletedMedium", this.CompletedMedium.toString());
            if (this.CompletedMedium.intValue() == 1) {
                Res = "680282";
            }
            if (this.CompletedMedium.intValue() == 10) {
                Res = "707792";
            }
            if (this.CompletedMedium.intValue() == 50) {
                Res = "707922";
            }
        } else if (Name.equalsIgnoreCase("CompletedHard")) {
            this.CompletedHard = Integer.valueOf(this.CompletedHard.intValue() + 1);
            SetPreference("CompletedHard", this.CompletedHard.toString());
            if (this.CompletedHard.intValue() == 1) {
                Res = "707422";
            }
            if (this.CompletedHard.intValue() == 10) {
                Res = "707802";
            }
            if (this.CompletedHard.intValue() == 50) {
                Res = "707932";
            }
        } else if (Name.equalsIgnoreCase("CompletedEpic")) {
            this.CompletedEpic = Integer.valueOf(this.CompletedEpic.intValue() + 1);
            SetPreference("CompletedEpic", this.CompletedEpic.toString());
            if (this.CompletedEpic.intValue() == 1) {
                Res = "707432";
            }
            if (this.CompletedEpic.intValue() == 2) {
                Res = "707812";
            }
            if (this.CompletedEpic.intValue() == 10) {
                Res = "707992";
            }
            if (this.CompletedEpic.intValue() == 50) {
                Res = "707942";
            }
        } else if (Name.equalsIgnoreCase("CompletedSpecial")) {
            this.CompletedSpecial = Integer.valueOf(this.CompletedSpecial.intValue() + 1);
            SetPreference("CompletedSpecial", this.CompletedSpecial.toString());
            if (this.CompletedSpecial.intValue() == 1) {
                Res = "707442";
            }
            if (this.CompletedSpecial.intValue() == 5) {
                Res = "707822";
            }
            if (this.CompletedSpecial.intValue() == 10) {
                Res = "708012";
            }
            if (this.CompletedSpecial.intValue() == 50) {
                Res = "707972";
            }
        } else if (Name.equalsIgnoreCase("nCells")) {
            this.nCells = Integer.valueOf(this.nCells.intValue() + 1);
            SetPreference("nCells", this.nCells.toString());
            if (this.nCells.intValue() == 100) {
                Res = "680242";
            }
            if (this.nCells.intValue() == 500) {
                Res = "680252";
            }
            if (this.nCells.intValue() == 1000) {
                Res = "680262";
            }
        } else if (Name.equalsIgnoreCase("SudokuMaster")) {
            SetPreference("SudokuMaster", "1");
            Res = "718952";
        } else if (Name.equalsIgnoreCase("SudokuGrandMaster")) {
            SetPreference("SudokuGrandMaster", "1");
            Res = "718962";
        } else if (Name.equalsIgnoreCase("SudokuDelete") && this.SudokuDelete.intValue() == 0) {
            this.SudokuDelete = 1;
            SetPreference("SudokuDelete", "1");
            Res = "708002";
        }
        if (Res == "") {
            return true;
        }
        new Achievement(Res).unlock(new Achievement.UnlockCB() {
            public void onSuccess(boolean newUnlock) {
            }

            public void onFailure(String exceptionMessage) {
            }
        });
        return true;
    }

    private boolean AddLeaderboard(String lName, Integer lValue) {
        String Res = "";
        if (lName.equalsIgnoreCase("TotalEasy")) {
            Res = "545384";
            SetPreference("TotalEasy", lValue.toString());
        }
        if (lName.equalsIgnoreCase("TotalMedium")) {
            Res = "545374";
            SetPreference("TotalMedium", lValue.toString());
        }
        if (lName.equalsIgnoreCase("TotalHard")) {
            Res = "545364";
            SetPreference("TotalHard", lValue.toString());
        }
        if (lName.equalsIgnoreCase("TotalEpic")) {
            Res = "545394";
            SetPreference("TotalEpic", lValue.toString());
        }
        if (lName.equalsIgnoreCase("TotalSpecial")) {
            Res = "545404";
            SetPreference("TotalSpecial", lValue.toString());
        }
        if (Res == "") {
            return false;
        }
        new Score((long) lValue.intValue(), String.valueOf(getString(R.string.OnlineScore)) + " " + lValue.toString()).submitTo(new Leaderboard(Res), new Score.SubmitToCB() {
            public void onSuccess(boolean newHighScore) {
            }

            public void onFailure(String exceptionMessage) {
            }
        });
        return true;
    }

    public void onResume() {
        super.onResume();
    }

    public void onStart() {
        super.onStart();
    }

    /* access modifiers changed from: private */
    public void PopUndo() {
        if (this.UndoList.size() > 0) {
            UndoMove um = this.UndoList.get(this.UndoList.size() - 1);
            this.UndoList.remove(this.UndoList.size() - 1);
            Point p = new Point();
            p.x = um.Col.intValue();
            p.y = um.Row.intValue();
            SetValue(p, um.From);
        }
    }

    /* access modifiers changed from: private */
    public void PushUndo(UndoMove NewMove) {
        this.UndoList.add(NewMove);
    }

    /* access modifiers changed from: private */
    public void ClearUndo() {
        this.UndoList.clear();
    }
}
