package sudoku.challenge.lt.map;

public class SudokuCell {
    public boolean Editable;
    public boolean IsPossible;
    public boolean Selected;
    public int Snapshot;
    public int colonna;
    public int[] opzioni = new int[9];
    public int riga;
    public int riquadro;
    public int valoreSchema;
    public int valoreSoluzione;

    public SudokuCell() {
        reset();
    }

    public void reset() {
        this.valoreSoluzione = 0;
        this.valoreSchema = 0;
        for (int i = 0; i < 9; i++) {
            this.opzioni[i] = 0;
        }
        this.IsPossible = true;
        this.Selected = false;
    }

    public int getOption() {
        boolean no_options = true;
        int i = 0;
        while (true) {
            if (i >= 9) {
                break;
            } else if (this.opzioni[i] != 0) {
                no_options = false;
                break;
            } else {
                i++;
            }
        }
        if (no_options) {
            return 0;
        }
        return 1;
    }

    public int GetOptionsCount() {
        int no_options = 0;
        for (int i = 0; i < 9; i++) {
            if (this.opzioni[i] != 0) {
                no_options++;
            }
        }
        return no_options;
    }

    public int verifyOption(int lastOpt) {
        for (int i = 0; i < 9; i++) {
            if (this.opzioni[i] != 0 && i > lastOpt) {
                return this.opzioni[i];
            }
        }
        return -1;
    }

    public void removeOption(int valore) {
        this.opzioni[valore] = 0;
    }

    public void setOpzioni(int[] opz) {
        for (int i = 0; i < 9; i++) {
            this.opzioni[i] = opz[i];
        }
    }

    public void EnableAllHints() {
        for (int i = 0; i < 9; i++) {
            this.opzioni[i] = 1;
        }
    }
}
