package sudoku.challenge.lt.map;

public class SudokuConfiguration {
    public static final boolean DEBUG = true;
    public static final int DIMENSION = 9;

    public enum SudokuDifficulty {
        EASY,
        MEDIUM,
        HARD,
        HELL
    }

    public enum SudokuPhase {
        CREATION,
        CHECK
    }

    public enum SudokuType {
        TRADITIONAL
    }
}
