package sudoku.challenge.lt;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import sudoku.challenge.lt.map.SudokuCell;

public class SchemaView extends View {
    public int BigFontSize;
    public Bitmap BufferBitmap;
    public Canvas BufferCanvas;
    public int CellWidth;
    public Schema CurSchema;
    public Rect GridRect;
    public Point SelectedCell;
    public int SelectedNumber;
    public int SmallFontSize;
    private int anim_lastx;
    private int anim_lasty;
    private int colBackground;
    private int colEditable;
    private int colFixed;
    private int colForeground;
    private int colHints;
    private int colImpossible;
    private int colSelected;
    private int colSelectedNumber;
    public ImageView sel_h;
    public ImageView sel_v;

    public SchemaView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFocusable(true);
        setFocusableInTouchMode(true);
        setClickable(true);
        setEnabled(true);
        this.anim_lasty = -100;
        this.anim_lastx = -100;
        this.SelectedCell = new Point(-1, -1);
        this.SelectedNumber = 0;
        this.GridRect = new Rect(0, 0, 1, 1);
        this.colBackground = Color.parseColor("#FFFFFFFF");
        this.colForeground = -16777216;
        this.colSelected = getResources().getColor(R.color.lightblue);
        this.colSelectedNumber = getResources().getColor(R.color.blue);
        this.colEditable = Color.parseColor("#377437");
        this.colHints = -12303292;
        this.colFixed = Color.parseColor("#174417");
        this.colFixed = -16777216;
        this.colImpossible = -65536;
        this.SmallFontSize = 12;
        this.BigFontSize = 26;
        this.CellWidth = 20;
        this.CurSchema = new Schema(getContext());
        this.BufferBitmap = Bitmap.createBitmap(100, 100, Bitmap.Config.ARGB_8888);
        this.BufferCanvas = new Canvas(this.BufferBitmap);
    }

    public void SelectCell(int r, int c) {
        boolean sel = !this.CurSchema.Mappa[r][c].Selected;
        this.CurSchema.DeselectAll();
        this.CurSchema.Mappa[r][c].Selected = sel;
        this.SelectedNumber = this.CurSchema.Mappa[r][c].valoreSchema;
        this.SelectedCell.x = c;
        this.SelectedCell.y = r;
        TranslateAnimation anim_v = new TranslateAnimation(0, 0.0f, 0, 0.0f, 0, (float) this.anim_lasty, 0, (float) ((this.CellWidth * r) + 1));
        anim_v.setDuration(500);
        anim_v.willChangeBounds();
        anim_v.setInterpolator(new DecelerateInterpolator());
        anim_v.setRepeatCount(0);
        anim_v.setFillAfter(true);
        this.sel_h.startAnimation(anim_v);
        TranslateAnimation anim_h = new TranslateAnimation(0, (float) this.anim_lastx, 0, (float) ((this.CellWidth * c) + 1), 0, 0.0f, 0, 0.0f);
        anim_h.setDuration(500);
        anim_h.willChangeBounds();
        anim_h.setInterpolator(new DecelerateInterpolator());
        anim_h.setRepeatCount(0);
        anim_h.setFillAfter(true);
        this.sel_v.startAnimation(anim_h);
        this.anim_lasty = (this.CellWidth * r) + 1;
        this.anim_lastx = (this.CellWidth * c) + 1;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        canvas.drawBitmap(this.BufferBitmap, 0.0f, 0.0f, paint);
        Log.d("View", "redraw ");
    }

    public void DrawCurrentSchema() {
        Paint paint = new Paint();
        paint.setColor(this.colBackground);
        paint.setStyle(Paint.Style.FILL);
        this.BufferCanvas.drawRect(this.GridRect, paint);
        paint.setColor(this.colForeground);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(3.0f);
        this.CellWidth = this.GridRect.width() / 9;
        this.SmallFontSize = (int) (((double) this.CellWidth) * 0.3d);
        this.BigFontSize = (int) (((double) this.CellWidth) * 0.8d);
        paint.setStrokeWidth(3.0f);
        for (int i = 0; i <= 9; i++) {
            if (i % 3 == 0) {
                this.BufferCanvas.drawLine((float) this.GridRect.left, (float) (this.GridRect.top + (this.CellWidth * i)), (float) (this.GridRect.left + (this.CellWidth * 9)), (float) (this.GridRect.top + (this.CellWidth * i)), paint);
                this.BufferCanvas.drawLine((float) (this.GridRect.left + (this.CellWidth * i)), (float) this.GridRect.top, (float) (this.GridRect.left + (this.CellWidth * i)), (float) (this.GridRect.left + (this.CellWidth * 9)), paint);
            }
        }
        paint.setColor(this.colForeground);
        paint.setStyle(Paint.Style.FILL);
        paint.setTextSize((float) this.BigFontSize);
        paint.setTypeface(Typeface.DEFAULT_BOLD);
        paint.setAntiAlias(true);
        Rect cellRect = new Rect();
        for (int r = 0; r < 9; r++) {
            for (int c = 0; c < 9; c++) {
                SudokuCell CurCel = this.CurSchema.Mappa[r][c];
                cellRect.left = this.CellWidth * c;
                cellRect.right = cellRect.left + this.CellWidth;
                cellRect.top = this.CellWidth * r;
                cellRect.bottom = cellRect.top + this.CellWidth;
                DrawCell(CurCel, paint, cellRect);
            }
        }
        paint.setAntiAlias(false);
        paint.setStrokeWidth(2.0f);
        paint.setColor(this.colForeground);
        paint.setStyle(Paint.Style.FILL);
        for (int i2 = 1; i2 < 9; i2++) {
            if (i2 % 3 == 0) {
                this.BufferCanvas.drawLine((float) this.GridRect.left, (float) (this.GridRect.top + (this.CellWidth * i2)), (float) (this.GridRect.left + (this.CellWidth * 9)), (float) (this.GridRect.top + (this.CellWidth * i2)), paint);
                this.BufferCanvas.drawLine((float) (this.GridRect.left + (this.CellWidth * i2)), (float) this.GridRect.top, (float) (this.GridRect.left + (this.CellWidth * i2)), (float) (this.GridRect.left + (this.CellWidth * 9)), paint);
            }
        }
        paint.setStyle(Paint.Style.STROKE);
        this.BufferCanvas.drawRect((float) this.GridRect.left, (float) this.GridRect.top, (float) ((this.GridRect.left + (this.CellWidth * 9)) - 1), (float) ((this.GridRect.top + (this.CellWidth * 9)) - 1), paint);
    }

    private void DrawCell(SudokuCell CurCel, Paint paint, Rect cellRect) {
        int colTmp;
        paint.setAntiAlias(false);
        if (this.SelectedCell.x >= 0 && CurCel.colonna / 3 == this.SelectedCell.x / 3 && CurCel.riga / 3 == this.SelectedCell.y / 3) {
            paint.setColor(this.colSelected);
            paint.setStyle(Paint.Style.FILL);
            this.BufferCanvas.drawRect(cellRect, paint);
        }
        paint.setColor(this.colForeground);
        paint.setStrokeWidth(1.0f);
        paint.setStyle(Paint.Style.STROKE);
        this.BufferCanvas.drawRect(cellRect, paint);
        paint.setFakeBoldText(false);
        if (CurCel.valoreSchema > 0) {
            paint.setAntiAlias(true);
            paint.setTextSize((float) this.BigFontSize);
            paint.setTypeface(Typeface.DEFAULT_BOLD);
            paint.setStyle(Paint.Style.FILL);
            int colTmp2 = this.colFixed;
            if (CurCel.Editable) {
                colTmp2 = this.colEditable;
            }
            if (CurCel.valoreSchema == this.SelectedNumber) {
                colTmp2 = this.colSelectedNumber;
            }
            if (!CurCel.Editable) {
                if (CurCel.valoreSchema == this.SelectedNumber) {
                    colTmp = this.colSelectedNumber;
                } else {
                    colTmp = this.colFixed;
                }
                paint.setFakeBoldText(true);
            }
            if (!CurCel.IsPossible && CurCel.Editable) {
                colTmp = this.colImpossible;
            }
            paint.setColor(colTmp);
            this.BufferCanvas.drawText(new Integer(CurCel.valoreSchema).toString(), (float) ((cellRect.left + (cellRect.width() / 2)) - 6), (float) (cellRect.top + ((cellRect.height() / 4) * 3) + 4), paint);
        } else if (CurCel.Editable && CurCel.valoreSchema <= 0) {
            paint.setAntiAlias(true);
            paint.setTextSize((float) this.SmallFontSize);
            paint.setTypeface(Typeface.DEFAULT);
            paint.setColor(this.colHints);
            paint.setStyle(Paint.Style.FILL);
            for (int o = 0; o < 9; o++) {
                if (CurCel.opzioni[o] != 0) {
                    if (o == 0) {
                        this.BufferCanvas.drawText("1", (float) (cellRect.left + 3), ((float) cellRect.top) + 10.0f + 2.0f, paint);
                    }
                    if (o == 1) {
                        this.BufferCanvas.drawText("2", (((float) (cellRect.left + (cellRect.width() / 2))) - (10.0f / 2.0f)) + 2.0f, ((float) cellRect.top) + 10.0f + 2.0f, paint);
                    }
                    if (o == 2) {
                        this.BufferCanvas.drawText("3", ((float) (cellRect.left + cellRect.width())) - 10.0f, ((float) cellRect.top) + 10.0f + 2.0f, paint);
                    }
                    if (o == 3) {
                        this.BufferCanvas.drawText("4", (float) (cellRect.left + 3), ((float) (cellRect.top + (cellRect.height() / 2))) + (10.0f / 2.0f), paint);
                    }
                    if (o == 4) {
                        this.BufferCanvas.drawText("5", (((float) (cellRect.left + (cellRect.width() / 2))) - (10.0f / 2.0f)) + 2.0f, ((float) (cellRect.top + (cellRect.height() / 2))) + (10.0f / 2.0f), paint);
                    }
                    if (o == 5) {
                        this.BufferCanvas.drawText("6", ((float) (cellRect.left + cellRect.width())) - 10.0f, ((float) (cellRect.top + (cellRect.height() / 2))) + (10.0f / 2.0f), paint);
                    }
                    if (o == 6) {
                        this.BufferCanvas.drawText("7", (float) (cellRect.left + 3), (float) ((cellRect.top + cellRect.height()) - 2), paint);
                    }
                    if (o == 7) {
                        this.BufferCanvas.drawText("8", (((float) (cellRect.left + (cellRect.width() / 2))) - (10.0f / 2.0f)) + 2.0f, (float) ((cellRect.top + cellRect.height()) - 2), paint);
                    }
                    if (o == 8) {
                        this.BufferCanvas.drawText("9", ((float) (cellRect.left + cellRect.width())) - 10.0f, (float) ((cellRect.top + cellRect.height()) - 2), paint);
                    }
                }
            }
        }
    }

    public void ResizeSchema() {
        this.BufferBitmap = Bitmap.createBitmap(this.GridRect.width(), this.GridRect.height(), Bitmap.Config.ARGB_8888);
        this.BufferCanvas = new Canvas(this.BufferBitmap);
    }
}
