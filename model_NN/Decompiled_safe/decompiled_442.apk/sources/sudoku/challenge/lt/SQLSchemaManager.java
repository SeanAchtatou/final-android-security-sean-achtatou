package sudoku.challenge.lt;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class SQLSchemaManager {
    private SQLOpenHelper OpenHelper = new SQLOpenHelper(this.sudoku_context, "sudokus_ad.db");
    public SimpleDateFormat TimeFormatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    public SQLiteDatabase db;
    private Context sudoku_context;

    public SQLSchemaManager(Context ctx) {
        this.sudoku_context = ctx;
        this.OpenHelper.createDataBase();
        this.OpenHelper.openDataBase();
        this.db = this.OpenHelper.myDataBase;
    }

    public boolean GetCurrentSchema(Schema curSchema) {
        curSchema.clear();
        try {
            Cursor Result = this.db.rawQuery("SELECT * FROM History WHERE History.LastUpdate>'2000/01/01 00:00:00' AND History.Stop<='2000/01/01 00:00:00' ORDER BY LastUpdate Desc", null);
            if (Result.getCount() <= 0) {
                Result.close();
                return false;
            }
            Result.moveToFirst();
            GetSchemaFromCursor(curSchema, Result);
            Result.close();
            return true;
        } catch (Exception e) {
            Log.d("DB", e.getMessage());
            return false;
        }
    }

    public boolean GetSchemaByID(Schema curSchema, int ID_S) {
        curSchema.clear();
        try {
            Cursor Result = this.db.rawQuery("SELECT * FROM History WHERE ID_S = " + ID_S, null);
            if (Result.getCount() <= 0) {
                Result.close();
                return false;
            }
            Result.moveToFirst();
            GetSchemaFromCursor(curSchema, Result);
            Result.close();
            return true;
        } catch (Exception e) {
            Log.d("DB", e.getMessage());
            return false;
        }
    }

    public void AddSchema(Schema cur) {
    }

    public void UpdateSchema(Schema cur) {
        ContentValues newValues = new ContentValues();
        newValues.put("LastUpdate", this.TimeFormatter.format(cur.LastUpdate));
        newValues.put("Start", this.TimeFormatter.format(cur.Start));
        newValues.put("Stop", this.TimeFormatter.format(cur.Stop));
        newValues.put("User", cur.GetUserString());
        newValues.put("Hints", cur.GetHintString());
        newValues.put("Snapshot", cur.GetSnapshotString());
        int n = 0;
        try {
            n = this.db.update("History", newValues, "ID_S = " + String.valueOf(cur.ID), null);
        } catch (Exception e) {
            Log.d("DB", e.getMessage());
        }
        if (n == 0) {
            Log.d("DB", "No row updated");
        }
    }

    public boolean GetSchemaFromCursor(Schema sch, Cursor cur) {
        try {
            sch.ID = cur.getInt(cur.getColumnIndex("ID_S"));
            sch.Name = cur.getString(cur.getColumnIndex("Name"));
            sch.Points = cur.getInt(cur.getColumnIndex("Points"));
            sch.IsSpecial = cur.getInt(cur.getColumnIndex("Strange")) == 1;
            String tmpDate = cur.getString(cur.getColumnIndex("Start"));
            if (tmpDate != null && tmpDate.length() > 4) {
                sch.Start = this.TimeFormatter.parse(tmpDate);
            }
            String tmpDate2 = cur.getString(cur.getColumnIndex("Stop"));
            if (tmpDate2 != null && tmpDate2.length() > 4) {
                sch.Stop = this.TimeFormatter.parse(tmpDate2);
            }
            String tmpDate3 = cur.getString(cur.getColumnIndex("LastUpdate"));
            if (tmpDate3 != null && tmpDate3.length() > 4) {
                sch.LastUpdate = this.TimeFormatter.parse(tmpDate3);
            }
            String S = cur.getString(cur.getColumnIndex("Solution"));
            String F = cur.getString(cur.getColumnIndex("Fixed"));
            String U = cur.getString(cur.getColumnIndex("User"));
            String H = cur.getString(cur.getColumnIndex("Hints"));
            String Snp = cur.getString(cur.getColumnIndex("Snapshot"));
            if (Snp == null || Snp.length() != 81) {
                Snp = F;
            }
            sch.GetValues(S, F, U, H, Snp);
            return true;
        } catch (ParseException e) {
            Log.d("DB", e.getMessage());
            return false;
        } catch (Exception e2) {
            Log.d("DB", e2.getMessage());
            return false;
        }
    }

    public int GetCurrentSchemaID() {
        try {
            Cursor Result = this.db.rawQuery("SELECT ID_S FROM History WHERE History.LastUpdate>'2010/01/01 00:00:00' AND History.Stop<'2010/01/01 00:00:00' ORDER BY LastUpdate Desc;", null);
            if (Result.getCount() <= 0) {
                Result.close();
                return -1;
            }
            Result.moveToFirst();
            int r = Result.getInt(Result.getColumnIndex("ID_S"));
            Result.close();
            return r;
        } catch (Exception e) {
            Log.d("DB", e.getMessage());
            return -1;
        }
    }

    public Cursor CreateCursor(String SQL) {
        try {
            Cursor Result = this.db.rawQuery(SQL, null);
            if (Result.getCount() > 0) {
                Result.moveToFirst();
            }
            return Result;
        } catch (Exception e) {
            Log.d("DB", e.getMessage());
            return null;
        }
    }

    public boolean GetSchemaSmallFromCursor(SchemaSmall sch, Cursor cur) {
        try {
            sch.ID = cur.getInt(cur.getColumnIndex("ID_S"));
            sch.Name = cur.getString(cur.getColumnIndex("Name"));
            sch.Points = cur.getInt(cur.getColumnIndex("Points"));
            sch.IsSpecial = cur.getInt(cur.getColumnIndex("Strange")) == 1;
            sch.Start = cur.getString(cur.getColumnIndex("Start"));
            sch.Stop = cur.getString(cur.getColumnIndex("Stop"));
            sch.LastUpdate = cur.getString(cur.getColumnIndex("LastUpdate"));
            sch.Fixed = cur.getString(cur.getColumnIndex("Fixed"));
            return true;
        } catch (Exception e) {
            Log.d("DB", e.getMessage());
            return false;
        }
    }
}
