package sudoku.challenge.lt;

import android.content.Context;
import android.graphics.Point;
import java.lang.reflect.Array;
import java.util.Date;
import sudoku.challenge.lt.map.SudokuCell;

public class Schema {
    public int ID;
    public boolean IsSpecial;
    public Date LastUpdate;
    SudokuCell[][] Mappa = ((SudokuCell[][]) Array.newInstance(SudokuCell.class, 9, 9));
    public String Name;
    public int Points = 0;
    public Date Start;
    public Date Stop;
    Context context;

    public Schema(Context curContext) {
        this.context = curContext;
        for (int r = 0; r < 9; r++) {
            for (int c = 0; c < 9; c++) {
                this.Mappa[r][c] = new SudokuCell();
                this.Mappa[r][c].riga = r;
                this.Mappa[r][c].colonna = c;
            }
        }
        clear();
    }

    public void clear() {
        for (int r = 0; r < 9; r++) {
            for (int c = 0; c < 9; c++) {
                this.Mappa[r][c].reset();
            }
        }
        this.Points = 0;
        this.Name = "";
        this.IsSpecial = false;
        this.ID = -1;
    }

    public void DeselectAll() {
        for (int r = 0; r < 9; r++) {
            for (int c = 0; c < 9; c++) {
                this.Mappa[r][c].Selected = false;
            }
        }
    }

    public void GetValues(String Slz, String Fxd, String Usr, String Hnt, String snp) {
        int snpLen;
        int ul = 0;
        int hl = 0;
        if (Usr != null) {
            ul = Usr.length();
        }
        if (Hnt != null) {
            hl = Hnt.length();
        }
        try {
            snpLen = snp.length();
        } catch (Exception e) {
            snpLen = 0;
        }
        for (int r = 0; r < 9; r++) {
            for (int c = 0; c < 9; c++) {
                int n = (r * 9) + c;
                this.Mappa[r][c].valoreSoluzione = Integer.parseInt(String.valueOf(Slz.charAt(n)));
                if (n < snpLen) {
                    this.Mappa[r][c].Snapshot = Integer.parseInt(String.valueOf(snp.charAt(n)));
                }
                this.Mappa[r][c].Editable = Fxd.charAt(n) == '0';
                if (!this.Mappa[r][c].Editable) {
                    this.Mappa[r][c].valoreSchema = this.Mappa[r][c].valoreSoluzione;
                } else if (ul > n) {
                    this.Mappa[r][c].valoreSchema = Integer.parseInt(String.valueOf(Usr.charAt(n)));
                }
                int m = (r * 81) + (c * 9);
                for (int h = 0; h < 9; h++) {
                    if (hl > m + h) {
                        this.Mappa[r][c].opzioni[h] = Integer.parseInt(String.valueOf(Hnt.charAt(m + h)));
                    }
                }
            }
        }
    }

    public String getStatus() {
        String Res;
        new String();
        String Res2 = this.context.getString(R.string.txtNotStarted);
        try {
            if (!isStarted()) {
                return Res2;
            }
            String Res3 = String.valueOf(this.context.getString(R.string.txtStartAt)) + ": ";
            if (!isEnded()) {
                return Res3;
            }
            Res = String.valueOf(this.context.getString(R.string.txtStopAt)) + ": " + " - " + this.context.getString(R.string.txtScore) + ": " + new Integer(this.Points).toString();
            return Res;
        } catch (Exception e) {
            Res = "??" + e.getMessage();
        }
    }

    public boolean isStarted() {
        if (this.Start == null || this.Start.getYear() + 1900 <= 2001) {
            return false;
        }
        return true;
    }

    public boolean isEnded() {
        if (this.Start == null || this.Stop == null || this.Start.getYear() + 1900 <= 2001 || this.Stop.getYear() + 1900 <= 2001) {
            return false;
        }
        return true;
    }

    public int SetValue(Point cell, int value) {
        int res;
        SudokuCell sc = this.Mappa[cell.y][cell.x];
        if (!sc.Editable) {
            return 0;
        }
        if (sc.valoreSchema == value) {
            sc.valoreSchema = 0;
            res = 0;
        } else {
            sc.valoreSchema = value;
            res = value;
        }
        CheckSchemaValues();
        return res;
    }

    private boolean CheckValue(int vr, int vc, int value) {
        if (value == 0) {
            return true;
        }
        for (int r = 0; r < 9; r++) {
            if (r != vr && this.Mappa[r][vc].valoreSchema == value) {
                return false;
            }
        }
        for (int c = 0; c < 9; c++) {
            if (c != vc && this.Mappa[vr][c].valoreSchema == value) {
                return false;
            }
        }
        int startr = (vr / 3) * 3;
        int startc = (vc / 3) * 3;
        for (int r2 = startr; r2 < startr + 3; r2++) {
            for (int c2 = startc; c2 < startc + 3; c2++) {
                if ((c2 != vc || r2 != vr) && this.Mappa[r2][c2].valoreSchema == value) {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean CheckHint(int vr, int vc, int value) {
        if (value == 0) {
            return true;
        }
        for (int r = 0; r < 9; r++) {
            if (this.Mappa[r][vc].valoreSchema == value) {
                return false;
            }
        }
        for (int c = 0; c < 9; c++) {
            if (this.Mappa[vr][c].valoreSchema == value) {
                return false;
            }
        }
        int startr = (vr / 3) * 3;
        int startc = (vc / 3) * 3;
        for (int r2 = startr; r2 < startr + 3; r2++) {
            for (int c2 = startc; c2 < startc + 3; c2++) {
                if (this.Mappa[r2][c2].valoreSchema == value) {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean CheckSchemaValues() {
        for (int r = 0; r < 9; r++) {
            for (int c = 0; c < 9; c++) {
                this.Mappa[r][c].IsPossible = CheckValue(r, c, this.Mappa[r][c].valoreSchema);
            }
        }
        return true;
    }

    public void CalcHints() {
        for (int r = 0; r < 9; r++) {
            for (int c = 0; c < 9; c++) {
                this.Mappa[r][c].EnableAllHints();
            }
        }
        for (int r2 = 0; r2 < 9; r2++) {
            for (int c2 = 0; c2 < 9; c2++) {
                for (int h = 0; h < 9; h++) {
                    if (!CheckHint(r2, c2, h + 1)) {
                        this.Mappa[r2][c2].opzioni[h] = 0;
                    }
                }
            }
        }
    }

    public String GetUserString() {
        String Res = "";
        for (int r = 0; r < 9; r++) {
            for (int c = 0; c < 9; c++) {
                Res = String.valueOf(Res) + this.Mappa[r][c].valoreSchema;
            }
        }
        return Res;
    }

    public String GetHintString() {
        String Res = "";
        for (int r = 0; r < 9; r++) {
            for (int c = 0; c < 9; c++) {
                for (int h = 0; h < 9; h++) {
                    Res = String.valueOf(Res) + this.Mappa[r][c].opzioni[h];
                }
            }
        }
        return Res;
    }

    public String GetSnapshotString() {
        String Res = "";
        for (int r = 0; r < 9; r++) {
            for (int c = 0; c < 9; c++) {
                Res = String.valueOf(Res) + this.Mappa[r][c].Snapshot;
            }
        }
        return Res;
    }

    public void ClearUserData() {
        for (int r = 0; r < 9; r++) {
            for (int c = 0; c < 9; c++) {
                this.Mappa[r][c].IsPossible = true;
                this.Mappa[r][c].Selected = false;
                if (this.Mappa[r][c].Editable) {
                    this.Mappa[r][c].valoreSchema = 0;
                    for (int h = 0; h < 9; h++) {
                        this.Mappa[r][c].opzioni[h] = 0;
                    }
                }
            }
        }
    }

    public void ClearUserHints() {
        for (int r = 0; r < 9; r++) {
            for (int c = 0; c < 9; c++) {
                if (this.Mappa[r][c].Editable) {
                    for (int h = 0; h < 9; h++) {
                        this.Mappa[r][c].opzioni[h] = 0;
                    }
                }
            }
        }
    }

    public boolean CheckCompleted() {
        if (isEnded()) {
            return true;
        }
        for (int r = 0; r < 9; r++) {
            for (int c = 0; c < 9; c++) {
                if (this.Mappa[r][c].Editable && this.Mappa[r][c].valoreSchema != this.Mappa[r][c].valoreSoluzione) {
                    return false;
                }
            }
        }
        this.Stop = new Date();
        return true;
    }

    public int GetSchemaLevel() {
        if (this.IsSpecial) {
            return 4;
        }
        if (this.Points >= 0 && this.Points < 400) {
            return 0;
        }
        if (this.Points >= 400 && this.Points < 900) {
            return 1;
        }
        if (this.Points < 900 || this.Points >= 4000) {
            return (this.Points < 4000 || this.Points >= 200000) ? 0 : 3;
        }
        return 2;
    }

    public void SaveSnapshot() {
        if (!isEnded()) {
            for (int r = 0; r < 9; r++) {
                for (int c = 0; c < 9; c++) {
                    this.Mappa[r][c].Snapshot = this.Mappa[r][c].valoreSchema;
                }
            }
        }
    }

    public void RestoreSnapshot() {
        if (!isEnded()) {
            for (int r = 0; r < 9; r++) {
                for (int c = 0; c < 9; c++) {
                    int n = this.Mappa[r][c].valoreSchema;
                    this.Mappa[r][c].valoreSchema = this.Mappa[r][c].Snapshot;
                    this.Mappa[r][c].Snapshot = n;
                }
            }
        }
    }
}
