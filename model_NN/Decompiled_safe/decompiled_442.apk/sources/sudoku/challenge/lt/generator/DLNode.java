package sudoku.challenge.lt.generator;

public class DLNode {
    public DLNode Down;
    public DLNode Header;
    public char IDName;
    public int IDNum;
    public DLNode Left;
    public DLNode Right;
    public DLNode Up;
}
