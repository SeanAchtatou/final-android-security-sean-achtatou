package sudoku.challenge.lt;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.RelativeLayout;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import com.openfeint.api.OpenFeint;
import com.openfeint.api.OpenFeintDelegate;
import com.openfeint.api.OpenFeintSettings;
import com.openfeint.api.ui.Dashboard;

public class MainActivity extends Activity {
    public int CurSchemaID;
    public Schema CurrentSchema;
    public boolean IsCurrentSchemaPresent;
    SQLSchemaManager SchemaManager;
    private AdView adView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        this.adView = new AdView(this, AdSize.BANNER, "a14d604712e804b");
        ((RelativeLayout) findViewById(R.id.MainActivity)).addView(this.adView);
        this.adView.loadAd(new AdRequest());
        this.SchemaManager = new SQLSchemaManager(getApplicationContext());
        this.CurrentSchema = new Schema(this);
        this.CurSchemaID = this.SchemaManager.GetCurrentSchemaID();
        this.IsCurrentSchemaPresent = this.CurSchemaID >= 0;
        ((Button) findViewById(R.id.btnChoose)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                MainActivity.this.startActivityForResult(new Intent(view.getContext(), SudokuListActivity.class), 0);
            }
        });
        this.IsCurrentSchemaPresent = true;
        ((Button) findViewById(R.id.btnPlay)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                boolean z;
                MainActivity.this.CurSchemaID = MainActivity.this.SchemaManager.GetCurrentSchemaID();
                MainActivity mainActivity = MainActivity.this;
                if (MainActivity.this.CurSchemaID >= 0) {
                    z = true;
                } else {
                    z = false;
                }
                mainActivity.IsCurrentSchemaPresent = z;
                if (MainActivity.this.IsCurrentSchemaPresent) {
                    Intent myIntent = new Intent(view.getContext(), SchemaActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putInt("SchemaID", MainActivity.this.CurSchemaID);
                    myIntent.putExtras(bundle);
                    MainActivity.this.startActivityForResult(myIntent, 0);
                    return;
                }
                MainActivity.this.startActivityForResult(new Intent(view.getContext(), SudokuListActivity.class), 0);
            }
        });
        ((Button) findViewById(R.id.btnScore)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                OpenFeint.initialize(view.getContext(), new OpenFeintSettings("Sudoku challenge", "QAALk8LPZioOXlYe8szIA", "7ichMe7ZsCxqqPXHbpMRf709uTKnSmiESn8zLOXq0", "193182"), new OpenFeintDelegate() {
                });
                Dashboard.open();
            }
        });
        ((Button) findViewById(R.id.btnMarket)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MainActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=sudoku.challenge.lt")));
            }
        });
        ((Button) findViewById(R.id.btnPlay)).startAnimation(AnimationUtils.loadAnimation(this, R.anim.left_to_right));
        ((Button) findViewById(R.id.btnChoose)).startAnimation(AnimationUtils.loadAnimation(this, R.anim.right_to_left));
        ((Button) findViewById(R.id.btnScore)).startAnimation(AnimationUtils.loadAnimation(this, R.anim.left_to_right));
    }

    public void onResume() {
        super.onResume();
    }
}
