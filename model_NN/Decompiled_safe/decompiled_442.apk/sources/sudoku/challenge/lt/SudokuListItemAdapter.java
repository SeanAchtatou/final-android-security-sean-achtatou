package sudoku.challenge.lt;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.List;

public class SudokuListItemAdapter extends ArrayAdapter<SchemaSmall> {
    Context cntx;
    int resource;

    public SudokuListItemAdapter(Context _context, int _resource, List<SchemaSmall> _items) {
        super(_context, _resource, _items);
        this.cntx = _context;
        this.resource = _resource;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.widget.LinearLayout, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int position, View convertView, ViewGroup parent) {
        LinearLayout todoView;
        SchemaSmall item = (SchemaSmall) getItem(position);
        if (convertView == null) {
            todoView = new LinearLayout(getContext());
            ((LayoutInflater) getContext().getSystemService("layout_inflater")).inflate(this.resource, (ViewGroup) todoView, true);
        } else {
            todoView = (LinearLayout) convertView;
        }
        ImageView imgStatus = (ImageView) todoView.findViewById(R.id.status);
        ImageView imgSchema = (ImageView) todoView.findViewById(R.id.preview);
        ((TextView) todoView.findViewById(R.id.rowLevel)).setText(String.valueOf(this.cntx.getString(R.string.Points)) + ": " + new Integer(item.Points).toString());
        ((TextView) todoView.findViewById(R.id.rowID)).setText(String.valueOf(new Integer(item.ID).toString()) + " - " + item.Name);
        if (item.isEnded()) {
            imgStatus.setImageResource(R.drawable.buttongreen);
        } else if (item.isStarted()) {
            imgStatus.setImageResource(R.drawable.buttonyellow);
        } else {
            imgStatus.setImageResource(R.drawable.buttonwithe);
        }
        TextView tvStart = (TextView) todoView.findViewById(R.id.rowStart);
        if (item.Start.equals("2000/01/01 00:00:00")) {
            tvStart.setText("-");
        } else {
            tvStart.setText(item.Start);
        }
        TextView tvStop = (TextView) todoView.findViewById(R.id.rowStop);
        if (item.Stop.equals("2000/01/01 00:00:00")) {
            tvStop.setText("-");
        } else {
            tvStop.setText(item.Stop);
        }
        DrawSchema(imgSchema, item);
        return todoView;
    }

    private void DrawSchema(ImageView imgSchema, SchemaSmall item) {
        Rect GridRect = new Rect(0, 0, 64, 64);
        Bitmap BufferBitmap = Bitmap.createBitmap(GridRect.width(), GridRect.height(), Bitmap.Config.ARGB_4444);
        Canvas canvas = new Canvas(BufferBitmap);
        Paint paint = new Paint();
        paint.setColor(-1);
        paint.setStyle(Paint.Style.FILL);
        canvas.drawRect(GridRect, paint);
        int CellWidth = GridRect.width() / 9;
        Rect cellRect = new Rect();
        int n = 0;
        paint.setColor(-7829368);
        paint.setStrokeWidth(1.0f);
        for (int r = 0; r < 9; r++) {
            for (int c = 0; c < 9; c++) {
                if (item.Fixed.charAt(n) != '0') {
                    cellRect.left = c * CellWidth;
                    cellRect.right = cellRect.left + CellWidth;
                    cellRect.top = r * CellWidth;
                    cellRect.bottom = cellRect.top + CellWidth;
                    paint.setStyle(Paint.Style.FILL);
                    canvas.drawRect(cellRect, paint);
                }
                n++;
            }
        }
        paint.setStrokeWidth(1.0f);
        paint.setColor(-7829368);
        paint.setStyle(Paint.Style.FILL);
        for (int i = 1; i <= 8; i++) {
            if (i % 3 != 0) {
                canvas.drawLine((float) GridRect.left, (float) (GridRect.top + (CellWidth * i)), (float) (GridRect.left + (CellWidth * 9)), (float) (GridRect.top + (CellWidth * i)), paint);
                canvas.drawLine((float) (GridRect.left + (CellWidth * i)), (float) GridRect.top, (float) (GridRect.left + (CellWidth * i)), (float) (GridRect.left + (CellWidth * 9)), paint);
            }
        }
        paint.setStrokeWidth(2.0f);
        paint.setColor(-16777216);
        paint.setStyle(Paint.Style.FILL);
        for (int i2 = 1; i2 <= 8; i2++) {
            if (i2 % 3 == 0) {
                canvas.drawLine((float) GridRect.left, (float) (GridRect.top + (CellWidth * i2)), (float) (GridRect.left + (CellWidth * 9)), (float) (GridRect.top + (CellWidth * i2)), paint);
                canvas.drawLine((float) (GridRect.left + (CellWidth * i2)), (float) GridRect.top, (float) (GridRect.left + (CellWidth * i2)), (float) (GridRect.left + (CellWidth * 9)), paint);
            }
        }
        paint.setStyle(Paint.Style.STROKE);
        canvas.drawRect((float) GridRect.left, (float) GridRect.top, (float) ((GridRect.left + (CellWidth * 9)) - 1), (float) ((GridRect.top + (CellWidth * 9)) - 1), paint);
        imgSchema.setImageBitmap(BufferBitmap);
    }
}
