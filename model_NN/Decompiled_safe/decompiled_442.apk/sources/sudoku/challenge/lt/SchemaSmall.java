package sudoku.challenge.lt;

public class SchemaSmall {
    public String Fixed;
    public int ID;
    public boolean IsSpecial;
    public String LastUpdate;
    public String Name;
    public int Points;
    public String Start;
    public String Stop;

    public SchemaSmall() {
        clear();
    }

    public void clear() {
        this.Points = 0;
        this.Name = "";
        this.Fixed = "";
        this.IsSpecial = false;
        this.ID = -1;
    }

    public boolean isStarted() {
        if (this.Start == null || this.Start.equals("2000/01/01 00:00:00")) {
            return false;
        }
        return true;
    }

    public boolean isEnded() {
        if (this.Start == null || this.Stop == null || this.Start.equals("2000/01/01 00:00:00") || this.Stop.equals("2000/01/01 00:00:00")) {
            return false;
        }
        return true;
    }
}
