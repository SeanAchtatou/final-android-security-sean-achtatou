package org.codehaus.jackson.impl;

/* compiled from: JsonWriteContext */
final class ArrayWContext extends JsonWriteContext {
    public ArrayWContext(JsonWriteContext parent) {
        super(1, parent);
    }

    public String getCurrentName() {
        return null;
    }

    public int writeFieldName(String name) {
        return 4;
    }

    public int writeValue() {
        int ix = this._index;
        this._index++;
        return ix < 0 ? 0 : 1;
    }

    /* access modifiers changed from: protected */
    public void appendDesc(StringBuilder sb) {
        sb.append('[');
        sb.append(getCurrentIndex());
        sb.append(']');
    }
}
