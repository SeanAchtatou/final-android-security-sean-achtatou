package barry.stlviewer2;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class IconifiedTextView extends LinearLayout {
    private ImageView mIcon;
    private TextView mText;

    public IconifiedTextView(Context context, IconifiedText aIconifiedText) {
        super(context);
        setOrientation(0);
        setGravity(16);
        this.mIcon = new ImageView(context);
        this.mIcon.setImageDrawable(aIconifiedText.getIcon());
        this.mIcon.setPadding(4, 6, 8, 6);
        addView(this.mIcon, new LinearLayout.LayoutParams(-2, -2));
        this.mText = new TextView(context);
        this.mText.setText(aIconifiedText.getText());
        this.mText.setTextSize(20.0f);
        this.mText.setTextColor(Color.rgb(35, 207, 52));
        addView(this.mText, new LinearLayout.LayoutParams(-2, -2));
    }

    public void setText(String words) {
        this.mText.setText(words);
    }

    public void setIcon(Drawable bullet) {
        this.mIcon.setImageDrawable(bullet);
    }
}
