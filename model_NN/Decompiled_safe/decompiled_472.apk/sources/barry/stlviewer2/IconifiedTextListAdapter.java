package barry.stlviewer2;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import java.util.ArrayList;
import java.util.List;

public class IconifiedTextListAdapter extends BaseAdapter {
    private Context mContext;
    private List<IconifiedText> mItems = new ArrayList();

    public IconifiedTextListAdapter(Context context) {
        this.mContext = context;
    }

    public void addItem(IconifiedText it) {
        this.mItems.add(it);
    }

    public void setListItems(List<IconifiedText> lit) {
        this.mItems = lit;
    }

    public int getCount() {
        return this.mItems.size();
    }

    public Object getItem(int position) {
        return this.mItems.get(position);
    }

    public boolean areAllItemsSelectable() {
        return false;
    }

    public boolean isSelectable(int position) {
        try {
            return this.mItems.get(position).isSelectable();
        } catch (IndexOutOfBoundsException e) {
            return false;
        }
    }

    public long getItemId(int position) {
        return (long) position;
    }

    /* Debug info: failed to restart local var, previous not found, register: 4 */
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            return new IconifiedTextView(this.mContext, this.mItems.get(position));
        }
        IconifiedTextView btv = (IconifiedTextView) convertView;
        btv.setText(this.mItems.get(position).getText());
        btv.setIcon(this.mItems.get(position).getIcon());
        return btv;
    }
}
