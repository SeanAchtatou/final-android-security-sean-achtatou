package barry.stlviewer2;

import android.app.ListActivity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class stlviewer2 extends ListActivity {
    private File currentDirectory = new File("/");
    private List<IconifiedText> directoryEntries = new ArrayList();

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        browseToRoot();
    }

    private void browseToRoot() {
        browseTo(new File("/"));
    }

    private void upOneLevel() {
        if (this.currentDirectory.getParent() != null) {
            browseTo(this.currentDirectory.getParentFile());
        }
    }

    private void browseTo(File aDirectory) {
        if (aDirectory.isDirectory()) {
            this.currentDirectory = aDirectory;
            fill(this.currentDirectory.listFiles());
            return;
        }
        openFile(aDirectory);
    }

    private void openFile(File f) {
        Global.file = f;
        Global.bLaunchedByMe = true;
        startActivity(new Intent(this, ShowSTL.class));
    }

    public boolean onCreateOptionsMenu(Menu menu2) {
        getMenuInflater().inflate(R.menu.menu2, menu2);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settings:
                startActivity(new Intent(this, Settings.class));
                return true;
            case R.id.info:
                startActivity(new Intent(this, Info.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void fill(File[] files) {
        this.directoryEntries.clear();
        this.directoryEntries.add(new IconifiedText(getString(R.string.current_dir), getResources().getDrawable(R.drawable.foldericon)));
        if (this.currentDirectory.getParent() != null) {
            this.directoryEntries.add(new IconifiedText(getString(R.string.up_one_level), getResources().getDrawable(R.drawable.upicon)));
        }
        Arrays.sort(files);
        Drawable currentIcon = null;
        for (File file : files) {
            String end = file.getName().substring(file.getName().lastIndexOf(".") + 1, file.getName().length()).toLowerCase();
            if (file.canRead() && (file.isDirectory() || end.equals("stl"))) {
                if (file.isDirectory()) {
                    currentIcon = getResources().getDrawable(R.drawable.foldericon);
                }
                if (end.equals("stl")) {
                    currentIcon = getResources().getDrawable(R.drawable.modelicon);
                }
                this.directoryEntries.add(new IconifiedText(file.getPath(), currentIcon));
            }
        }
        IconifiedTextListAdapter itla = new IconifiedTextListAdapter(this);
        itla.setListItems(this.directoryEntries);
        setListAdapter(itla);
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        String selectedFileString = this.directoryEntries.get(position).getText();
        if (selectedFileString.equals(getString(R.string.current_dir))) {
            browseTo(this.currentDirectory);
        } else if (selectedFileString.equals(getString(R.string.up_one_level))) {
            upOneLevel();
        } else {
            File clickedFile = new File(this.directoryEntries.get(position).getText());
            if (clickedFile != null && clickedFile.canRead()) {
                browseTo(clickedFile);
            }
        }
    }
}
