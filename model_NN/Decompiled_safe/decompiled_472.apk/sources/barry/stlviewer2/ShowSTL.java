package barry.stlviewer2;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.opengl.GLSurfaceView;
import android.opengl.GLU;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import java.io.File;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class ShowSTL extends Activity {
    float f1;
    float f2;
    float fDotProduct;
    float fSphereRad;
    float[] fV1 = new float[3];
    float[] fV2 = new float[3];
    float fX1;
    float fX2;
    float fY1;
    float fY2;
    float fdist;
    /* access modifiers changed from: private */
    public GLSurfaceView mGLView;
    float scale;
    float x1;
    float x2;
    float y1;
    float y2;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Global.bInvert = false;
        Global.mThisTransform = new float[16];
        Global.MatrixIdentity(Global.mThisTransform);
        Global.mTotalTransform = new float[16];
        Global.MatrixIdentity(Global.mTotalTransform);
        Global.m = new float[16];
        Global.fScale = 1.01f;
        Global.iState = Global.iSTATIC;
        Global.fThisQuat = new float[4];
        Global.fThisQuat[0] = 0.0f;
        Global.fThisQuat[1] = 0.0f;
        Global.fThisQuat[2] = 0.0f;
        Global.fThisQuat[3] = 1.0f;
        Global.fTotalQuat = new float[4];
        Global.fTotalQuat[0] = 0.0f;
        Global.fTotalQuat[1] = 0.0f;
        Global.fTotalQuat[2] = 0.0f;
        Global.fTotalQuat[3] = 1.0f;
        Global.fQuat = new float[4];
        Global.fCrossProduct = new float[3];
        Global.fCrossProduct[0] = 0.0f;
        Global.fCrossProduct[1] = 0.0f;
        Global.fCrossProduct[2] = 0.0f;
        Global.fDragStartX = 0.0f;
        Global.fDragStartY = 0.0f;
        Global.fDragEndX = 0.0f;
        Global.fDragEndY = 0.0f;
        if (!Global.bLaunchedByMe) {
            Global.file = new File(getIntent().getData().toString());
        }
        if (Global.GetObject(Global.file, Global.al, getApplicationContext()).booleanValue()) {
            requestWindowFeature(1);
            getWindow().setFlags(1024, 1024);
            this.mGLView = new GLSurfaceView(this);
            this.mGLView.setRenderer(new ClearRenderer());
            setContentView(this.mGLView);
            Global.bLaunchedByMe = false;
            return;
        }
        finish();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        String stats;
        switch (item.getItemId()) {
            case R.id.settings:
                startActivity(new Intent(this, Settings.class));
                return true;
            case R.id.info:
                startActivity(new Intent(this, Info.class));
                return true;
            case R.id.stats:
                String stats2 = String.valueOf("Name: ") + Global.file;
                if (Global.bBinary) {
                    stats = String.valueOf(stats2) + "\nFormat: Binary";
                } else {
                    stats = String.valueOf(stats2) + "\nFormat: Ascii";
                }
                String stats3 = String.valueOf(String.valueOf(stats) + "\nFacets: ") + Global.iFacetCount;
                AlertDialog.Builder statsbox = new AlertDialog.Builder(this);
                statsbox.setTitle("STL file stats");
                statsbox.setMessage(String.valueOf(stats3) + String.format("\nX width: %,.3f\nY height: %,.3f\nZ depth: %,.3f\nVolume: %,.3f", Float.valueOf(Global.fWidthX), Float.valueOf(Global.fWidthY), Float.valueOf(Global.fWidthZ), Float.valueOf(Global.fVolume)));
                statsbox.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                });
                statsbox.show();
                return true;
            case R.id.invert:
                if (Global.bInvert) {
                    Global.bInvert = false;
                } else {
                    Global.bInvert = true;
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == 0) {
            Global.fDragStartX = event.getX();
            Global.fDragStartY = event.getY();
            Global.fDragEndX = event.getX();
            Global.fDragEndY = event.getY();
            if (Global.iState == Global.iSTATIC) {
                Global.iState = Global.iDRAG;
            }
        }
        if ((event.getAction() & 255) == 5) {
            Global.i = event.findPointerIndex(1);
            this.f1 = event.getX(Global.i);
            this.f2 = event.getY(Global.i);
            Global.fZoomDist = Global.GetDistanceBetweenPoints(Global.fDragStartX, Global.fDragStartY, this.f1, this.f2);
            Global.iState = Global.iZOOM;
        }
        if (event.getAction() == 1 || event.getAction() == 3) {
            Global.fDragStartX = event.getX();
            Global.fDragStartY = event.getY();
            Global.iState = Global.iSTATIC;
        }
        if (event.getAction() == 6) {
            Global.fDragStartX = event.getX();
            Global.fDragStartY = event.getY();
            Global.iState = Global.iDRAG;
        }
        if (event.getAction() == 2) {
            if (event.getPointerCount() > 1) {
                Global.i = event.findPointerIndex(0);
                this.fX1 = event.getX(Global.i);
                this.fY1 = event.getY(Global.i);
                Global.i2 = event.findPointerIndex(1);
                this.fX2 = event.getX(Global.i2);
                this.fY2 = event.getY(Global.i2);
                this.fdist = Global.GetDistanceBetweenPoints(this.fX1, this.fY1, this.fX2, this.fY2);
                if (Global.fZoomDist == 0.0f) {
                    Global.fZoomDist = this.fdist;
                }
                this.scale = this.fdist / Global.fZoomDist;
                if (this.scale > 1.0f) {
                    Global.fScale *= 1.03f;
                } else if (this.scale < 1.0f) {
                    Global.fScale *= 0.98f;
                }
                if (Global.fScale > 4.0f) {
                    Global.fScale = 4.0f;
                }
                if (Global.fScale < 0.5f) {
                    Global.fScale = 0.5f;
                }
            } else if (Global.iState == Global.iDRAG) {
                Global.fDragEndX = event.getX();
                Global.fDragEndY = event.getY();
                if (Global.fDragEndX == Global.fDragStartX && Global.fDragEndY == Global.fDragStartY) {
                    return true;
                }
                this.x1 = Global.fDragStartX - (((float) Global.iWidth) / 2.0f);
                this.y1 = (((float) Global.iHeight) / 2.0f) - Global.fDragStartY;
                this.x2 = Global.fDragEndX - (((float) Global.iWidth) / 2.0f);
                this.y2 = (((float) Global.iHeight) / 2.0f) - Global.fDragEndY;
                this.f1 = Global.GetDistanceBetweenPoints(this.x1, this.y1, 0.0f, 0.0f);
                this.f2 = Global.GetDistanceBetweenPoints(this.x2, this.y2, 0.0f, 0.0f);
                this.fSphereRad = ((float) Math.sqrt((double) ((Global.iWidth * Global.iWidth) + (Global.iHeight * Global.iHeight)))) / 1.9f;
                this.fV1[0] = this.x1;
                this.fV1[1] = this.y1;
                this.fV1[2] = (float) Math.sqrt((double) ((this.fSphereRad * this.fSphereRad) - (this.f1 * this.f1)));
                this.fV2[0] = this.x2;
                this.fV2[1] = this.y2;
                this.fV2[2] = (float) Math.sqrt((double) ((this.fSphereRad * this.fSphereRad) - (this.f2 * this.f2)));
                Global.Normalise(this.fV1);
                Global.Normalise(this.fV2);
                Global.CrossProduct(this.fV2, this.fV1);
                Global.Normalise(Global.fCrossProduct);
                this.fDotProduct = Global.DotProduct(this.fV1, this.fV2);
                this.fDotProduct = (float) Math.cos(2.5d * ((double) ((float) Math.acos((double) this.fDotProduct))));
                float fsina = (float) Math.sqrt((1.0d - ((double) this.fDotProduct)) * 0.5d);
                float fcosa = (float) Math.sqrt((1.0d + ((double) this.fDotProduct)) * 0.5d);
                Global.fThisQuat[0] = Global.fCrossProduct[0] * fsina;
                Global.fThisQuat[1] = Global.fCrossProduct[1] * fsina;
                Global.fThisQuat[2] = Global.fCrossProduct[2] * fsina;
                Global.fThisQuat[3] = fcosa;
                Global.NormaliseQuat(Global.fThisQuat);
                Global.QuatMultiply(Global.fTotalQuat, Global.fThisQuat);
                Global.QuatToMatrix(Global.fTotalQuat);
                Global.fDragStartX = Global.fDragEndX;
                Global.fDragStartY = Global.fDragEndY;
            }
        }
        return true;
    }

    class ClearRenderer implements GLSurfaceView.Renderer {
        ClearRenderer() {
        }

        public void onSurfaceCreated(GL10 gl, EGLConfig config) {
            Global.fDotFrameRate = 100.0f;
            Global.fDotVelocityStart = 40.0f;
            Global.fDotTheta = 35.0f;
            Global.fDotYChange = (float) Math.cos((double) Global.fDotTheta);
            gl.glClearColor(Global.fBackR, Global.fBackG, Global.fBackB, 1.0f);
            gl.glClearDepthf(1.0f);
            gl.glEnable(2929);
            gl.glDepthFunc(515);
            gl.glShadeModel(7425);
            Global.iWidth = ShowSTL.this.mGLView.getWidth();
            Global.iHeight = ShowSTL.this.mGLView.getHeight();
            gl.glHint(3152, 4354);
            Global.fWidthX = Global.fmaxx + (0.0f - Global.fminx);
            Global.fWidthY = Global.fmaxy + (0.0f - Global.fminy);
            Global.fWidthZ = Global.fmaxz + (0.0f - Global.fminz);
            Global.fFirstShiftZ = Global.fWidthX;
            if (Global.fWidthY > Global.fFirstShiftZ) {
                Global.fFirstShiftZ = Global.fWidthY;
            }
            if (Global.fWidthZ > Global.fFirstShiftZ) {
                Global.fFirstShiftZ = Global.fWidthZ;
            }
            gl.glEnable(2896);
            gl.glEnable(2896);
            float[] ambientLight = {0.0f, 0.0f, 0.0f, 1.0f};
            float[] diffuseLight = {0.6f, 0.6f, 0.6f, 1.0f};
            float[] position = {10.0f * Global.fFirstShiftZ, 10.0f * Global.fFirstShiftZ, 10.0f * Global.fFirstShiftZ, 0.0f};
            ByteBuffer byteBuf = ByteBuffer.allocateDirect(ambientLight.length * 4);
            byteBuf.order(ByteOrder.nativeOrder());
            Global.lightAmbientBuffer = byteBuf.asFloatBuffer();
            Global.lightAmbientBuffer.put(ambientLight);
            Global.lightAmbientBuffer.position(0);
            ByteBuffer byteBuf2 = ByteBuffer.allocateDirect(diffuseLight.length * 4);
            byteBuf2.order(ByteOrder.nativeOrder());
            Global.lightDiffuseBuffer = byteBuf2.asFloatBuffer();
            Global.lightDiffuseBuffer.put(diffuseLight);
            Global.lightDiffuseBuffer.position(0);
            ByteBuffer byteBuf3 = ByteBuffer.allocateDirect(position.length * 4);
            byteBuf3.order(ByteOrder.nativeOrder());
            Global.lightPositionBuffer = byteBuf3.asFloatBuffer();
            Global.lightPositionBuffer.put(position);
            Global.lightPositionBuffer.position(0);
            gl.glLightfv(16384, 4608, Global.lightAmbientBuffer);
            gl.glLightfv(16384, 4609, Global.lightDiffuseBuffer);
            gl.glLightfv(16384, 4611, Global.lightPositionBuffer);
            gl.glEnable(16384);
            float[] ambientLight2 = {0.0f, 0.0f, 0.0f, 1.0f};
            float[] diffuseLight2 = {0.5f, 0.5f, 0.5f, 1.0f};
            float[] position2 = {-10.0f * Global.fFirstShiftZ, 10.0f * Global.fFirstShiftZ, 10.0f * Global.fFirstShiftZ, 0.0f};
            ByteBuffer byteBuf4 = ByteBuffer.allocateDirect(ambientLight2.length * 4);
            byteBuf4.order(ByteOrder.nativeOrder());
            Global.lightAmbientBuffer2 = byteBuf4.asFloatBuffer();
            Global.lightAmbientBuffer2.put(ambientLight2);
            Global.lightAmbientBuffer2.position(0);
            ByteBuffer byteBuf5 = ByteBuffer.allocateDirect(diffuseLight2.length * 4);
            byteBuf5.order(ByteOrder.nativeOrder());
            Global.lightDiffuseBuffer2 = byteBuf5.asFloatBuffer();
            Global.lightDiffuseBuffer2.put(diffuseLight2);
            Global.lightDiffuseBuffer2.position(0);
            ByteBuffer byteBuf6 = ByteBuffer.allocateDirect(position2.length * 4);
            byteBuf6.order(ByteOrder.nativeOrder());
            Global.lightPositionBuffer2 = byteBuf6.asFloatBuffer();
            Global.lightPositionBuffer2.put(position2);
            Global.lightPositionBuffer2.position(0);
            gl.glLightfv(16385, 4608, Global.lightAmbientBuffer2);
            gl.glLightfv(16385, 4609, Global.lightDiffuseBuffer2);
            gl.glLightfv(16385, 4611, Global.lightPositionBuffer2);
            gl.glEnable(16385);
            gl.glShadeModel(7424);
            switch (Global.iModelColor) {
                case 0:
                    Global.mcolor[0] = 1.0f;
                    Global.mcolor[1] = 0.066f;
                    Global.mcolor[2] = 0.066f;
                    Global.mcolor[3] = Global.fAlpha;
                    break;
                case 1:
                    Global.mcolor[0] = 0.13f;
                    Global.mcolor[1] = 0.7666f;
                    Global.mcolor[2] = 0.1928f;
                    Global.mcolor[3] = Global.fAlpha;
                    break;
                case 2:
                    Global.mcolor[0] = 0.0f;
                    Global.mcolor[1] = 0.5f;
                    Global.mcolor[2] = 1.0f;
                    Global.mcolor[3] = Global.fAlpha;
                    break;
                case 3:
                    Global.mcolor[0] = 0.75f;
                    Global.mcolor[1] = 0.75f;
                    Global.mcolor[2] = 0.75f;
                    Global.mcolor[3] = Global.fAlpha;
                    break;
            }
            ByteBuffer byteBuf7 = ByteBuffer.allocateDirect(Global.mcolor.length * 4);
            byteBuf7.order(ByteOrder.nativeOrder());
            Global.matBuffer = byteBuf7.asFloatBuffer();
            Global.matBuffer.put(Global.mcolor);
            Global.matBuffer.position(0);
            Global.mVcolor = new float[4];
            Global.mVcolor[0] = 1.0f;
            Global.mVcolor[1] = 1.0f;
            Global.mVcolor[2] = 1.0f;
            Global.mVcolor[3] = 1.0f;
            ByteBuffer byteBufV = ByteBuffer.allocateDirect(Global.mVcolor.length * 4);
            byteBufV.order(ByteOrder.nativeOrder());
            Global.matVBuffer = byteBufV.asFloatBuffer();
            Global.matVBuffer.put(Global.mVcolor);
            Global.matVBuffer.position(0);
            float[] fBoxCoords = {(-Global.fWidthX) / 2.0f, Global.fWidthY / 2.0f, Global.fWidthZ / 2.0f, Global.fWidthX / 2.0f, Global.fWidthY / 2.0f, Global.fWidthZ / 2.0f, Global.fWidthX / 2.0f, (-Global.fWidthY) / 2.0f, Global.fWidthZ / 2.0f, (-Global.fWidthX) / 2.0f, (-Global.fWidthY) / 2.0f, Global.fWidthZ / 2.0f, (-Global.fWidthX) / 2.0f, Global.fWidthY / 2.0f, (-Global.fWidthZ) / 2.0f, Global.fWidthX / 2.0f, Global.fWidthY / 2.0f, (-Global.fWidthZ) / 2.0f, Global.fWidthX / 2.0f, (-Global.fWidthY) / 2.0f, (-Global.fWidthZ) / 2.0f, (-Global.fWidthX) / 2.0f, (-Global.fWidthY) / 2.0f, (-Global.fWidthZ) / 2.0f};
            short[] sBoxLines = new short[24];
            sBoxLines[1] = 1;
            sBoxLines[2] = 1;
            sBoxLines[3] = 2;
            sBoxLines[4] = 2;
            sBoxLines[5] = 3;
            sBoxLines[6] = 3;
            sBoxLines[9] = 4;
            sBoxLines[10] = 4;
            sBoxLines[11] = 5;
            sBoxLines[12] = 5;
            sBoxLines[13] = 1;
            sBoxLines[14] = 5;
            sBoxLines[15] = 6;
            sBoxLines[16] = 6;
            sBoxLines[17] = 2;
            sBoxLines[18] = 6;
            sBoxLines[19] = 7;
            sBoxLines[20] = 7;
            sBoxLines[21] = 3;
            sBoxLines[22] = 7;
            sBoxLines[23] = 4;
            ByteBuffer ibb = ByteBuffer.allocateDirect(48);
            ibb.order(ByteOrder.nativeOrder());
            Global.BoxIndexBuffer = ibb.asShortBuffer();
            Global.BoxIndexBuffer.put(sBoxLines);
            Global.BoxIndexBuffer.position(0);
            ByteBuffer BoxBuf = ByteBuffer.allocateDirect(fBoxCoords.length * 4);
            BoxBuf.order(ByteOrder.nativeOrder());
            Global.BoxCoordBuf = BoxBuf.asFloatBuffer();
            Global.BoxCoordBuf.put(fBoxCoords);
            Global.BoxCoordBuf.position(0);
            float[] boxcolor = {0.5f, 0.5f, 0.5f, 1.0f};
            ByteBuffer BoxcolorBuf = ByteBuffer.allocateDirect(boxcolor.length * 4);
            BoxcolorBuf.order(ByteOrder.nativeOrder());
            Global.matBoxBuffer = BoxcolorBuf.asFloatBuffer();
            Global.matBoxBuffer.put(boxcolor);
            Global.matBoxBuffer.position(0);
        }

        public void onSurfaceChanged(GL10 gl, int w, int h) {
            if (h == 0) {
                h = 1;
            }
            Global.iWidth = w;
            Global.iHeight = h;
            gl.glViewport(0, 0, w, h);
            gl.glMatrixMode(5889);
            gl.glLoadIdentity();
            GLU.gluPerspective(gl, Global.fLens, ((float) w) / ((float) h), Global.fFirstShiftZ, 10.0f * Global.fFirstShiftZ);
            gl.glTranslatef(0.0f, 0.0f, 0.0f - (4.0f * Global.fFirstShiftZ));
            gl.glMatrixMode(5888);
            gl.glLoadIdentity();
        }

        public void onDrawFrame(GL10 gl) {
            gl.glClearColor(Global.fBackR, Global.fBackG, Global.fBackB, 1.0f);
            gl.glClear(16640);
            ByteBuffer bb = ByteBuffer.allocateDirect(Global.mTotalTransform.length * 4);
            bb.order(ByteOrder.nativeOrder());
            Global.fb = bb.asFloatBuffer();
            Global.fb.put(Global.mTotalTransform);
            Global.fb.position(0);
            gl.glPushMatrix();
            gl.glMultMatrixf(Global.fb);
            gl.glScalef(Global.fScale, Global.fScale, Global.fScale);
            if (Global.bDrawTriangles) {
                Global.DrawMesh(gl);
            }
            if (Global.bDrawVertices) {
                Global.DrawVertices(gl);
            }
            if (Global.bDrawAxes) {
                Global.DrawAxes(gl);
            }
            if (Global.bDrawBBox) {
                Global.DrawBBox(gl);
            }
            gl.glPopMatrix();
            Global.MatrixIdentity(Global.mThisTransform);
            Global.QuatIdentity(Global.fThisQuat);
        }
    }
}
