package barry.stlviewer2;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.RadioButton;

public class Settings extends Activity {
    View.OnClickListener checkboxhandler = new View.OnClickListener() {
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.checkboxfacets:
                    if (Global.bDrawTriangles) {
                        Global.bDrawTriangles = false;
                        return;
                    } else {
                        Global.bDrawTriangles = true;
                        return;
                    }
                case R.id.checkboxvertices:
                    if (Global.bDrawVertices) {
                        Global.bDrawVertices = false;
                        return;
                    } else {
                        Global.bDrawVertices = true;
                        return;
                    }
                case R.id.checkboxaxes:
                    if (Global.bDrawAxes) {
                        Global.bDrawAxes = false;
                        return;
                    } else {
                        Global.bDrawAxes = true;
                        return;
                    }
                case R.id.checkboxbbox:
                    if (Global.bDrawBBox) {
                        Global.bDrawBBox = false;
                        return;
                    } else {
                        Global.bDrawBBox = true;
                        return;
                    }
                default:
                    return;
            }
        }
    };
    View.OnClickListener radio_listener = new View.OnClickListener() {
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.radio_red:
                    Global.mcolor[0] = 1.0f;
                    Global.mcolor[1] = 0.066f;
                    Global.mcolor[2] = 0.066f;
                    Global.mcolor[3] = 1.0f;
                    Global.iModelColor = 0;
                    return;
                case R.id.radio_green:
                    Global.mcolor[0] = 0.13f;
                    Global.mcolor[1] = 0.7666f;
                    Global.mcolor[2] = 0.1928f;
                    Global.mcolor[3] = 1.0f;
                    Global.iModelColor = 1;
                    return;
                case R.id.radio_blue:
                    Global.mcolor[0] = 0.0f;
                    Global.mcolor[1] = 0.5f;
                    Global.mcolor[2] = 1.0f;
                    Global.mcolor[3] = 1.0f;
                    Global.iModelColor = 2;
                    return;
                case R.id.radio_grey:
                    Global.mcolor[0] = 0.75f;
                    Global.mcolor[1] = 0.75f;
                    Global.mcolor[2] = 0.75f;
                    Global.mcolor[3] = 1.0f;
                    Global.iModelColor = 3;
                    return;
                default:
                    return;
            }
        }
    };

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.settings);
        CheckBox cbfacets = (CheckBox) findViewById(R.id.checkboxfacets);
        CheckBox cbvertices = (CheckBox) findViewById(R.id.checkboxvertices);
        CheckBox cbaxes = (CheckBox) findViewById(R.id.checkboxaxes);
        CheckBox cbbbox = (CheckBox) findViewById(R.id.checkboxbbox);
        if (Global.bDrawTriangles) {
            cbfacets.setChecked(true);
        }
        if (Global.bDrawVertices) {
            cbvertices.setChecked(true);
        }
        if (Global.bDrawAxes) {
            cbaxes.setChecked(true);
        }
        if (Global.bDrawBBox) {
            cbbbox.setChecked(true);
        }
        cbfacets.setOnClickListener(this.checkboxhandler);
        cbvertices.setOnClickListener(this.checkboxhandler);
        cbaxes.setOnClickListener(this.checkboxhandler);
        cbbbox.setOnClickListener(this.checkboxhandler);
        RadioButton radio_red = (RadioButton) findViewById(R.id.radio_red);
        RadioButton radio_green = (RadioButton) findViewById(R.id.radio_green);
        RadioButton radio_blue = (RadioButton) findViewById(R.id.radio_blue);
        RadioButton radio_grey = (RadioButton) findViewById(R.id.radio_grey);
        radio_red.setOnClickListener(this.radio_listener);
        radio_green.setOnClickListener(this.radio_listener);
        radio_blue.setOnClickListener(this.radio_listener);
        radio_grey.setOnClickListener(this.radio_listener);
        switch (Global.iModelColor) {
            case 0:
                radio_red.setChecked(true);
                return;
            case 1:
                radio_green.setChecked(true);
                return;
            case 2:
                radio_blue.setChecked(true);
                return;
            case 3:
                radio_grey.setChecked(true);
                return;
            default:
                return;
        }
    }
}
