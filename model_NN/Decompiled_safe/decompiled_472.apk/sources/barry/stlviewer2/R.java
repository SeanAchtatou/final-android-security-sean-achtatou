package barry.stlviewer2;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int foldericon = 2130837504;
        public static final int icon = 2130837505;
        public static final int m_blank = 2130837506;
        public static final int m_info = 2130837507;
        public static final int m_invert = 2130837508;
        public static final int m_settings = 2130837509;
        public static final int m_stats = 2130837510;
        public static final int modelicon = 2130837511;
        public static final int pspbrwse = 2130837512;
        public static final int upicon = 2130837513;
    }

    public static final class id {
        public static final int ABOUT_URL = 2131099650;
        public static final int bimage = 2131099662;
        public static final int btitle = 2131099663;
        public static final int checkboxaxes = 2131099668;
        public static final int checkboxbbox = 2131099669;
        public static final int checkboxfacets = 2131099666;
        public static final int checkboxvertices = 2131099667;
        public static final int info = 2131099676;
        public static final int info1 = 2131099649;
        public static final int info10 = 2131099659;
        public static final int info2 = 2131099651;
        public static final int info3 = 2131099652;
        public static final int info4 = 2131099653;
        public static final int info5 = 2131099654;
        public static final int info6 = 2131099655;
        public static final int info7 = 2131099656;
        public static final int info8 = 2131099657;
        public static final int info9 = 2131099658;
        public static final int infoversion = 2131099661;
        public static final int invert = 2131099678;
        public static final int listView = 2131099674;
        public static final int radio_blue = 2131099672;
        public static final int radio_green = 2131099671;
        public static final int radio_grey = 2131099673;
        public static final int radio_red = 2131099670;
        public static final int scroll = 2131099648;
        public static final int settings = 2131099675;
        public static final int stats = 2131099677;
        public static final int textView1 = 2131099664;
        public static final int textView2 = 2131099665;
        public static final int versions = 2131099660;
    }

    public static final class layout {
        public static final int info = 2130903040;
        public static final int listitem = 2130903041;
        public static final int listitem2 = 2130903042;
        public static final int main = 2130903043;
        public static final int settings = 2130903044;
        public static final int stlviewer2 = 2130903045;
    }

    public static final class menu {
        public static final int menu = 2131034112;
        public static final int menu2 = 2131034113;
    }

    public static final class string {
        public static final int app_name = 2130968576;
        public static final int current_dir = 2130968578;
        public static final int info1 = 2130968579;
        public static final int info10 = 2130968588;
        public static final int info2 = 2130968580;
        public static final int info3 = 2130968581;
        public static final int info4 = 2130968582;
        public static final int info5 = 2130968583;
        public static final int info6 = 2130968584;
        public static final int info7 = 2130968585;
        public static final int info8 = 2130968586;
        public static final int info9 = 2130968587;
        public static final int infoversion = 2130968589;
        public static final int settings = 2130968590;
        public static final int up_one_level = 2130968577;
    }
}
