package barry.stlviewer2;

import android.content.Context;
import android.widget.Toast;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;
import java.util.Random;
import javax.microedition.khronos.opengles.GL10;

public class Global {
    public static FloatBuffer BoxCoordBuf;
    public static ShortBuffer BoxIndexBuffer;
    public static float[] GLDot;
    public static float[] GLnormals;
    public static float[] GLverts;
    public static int GLvertsLength = 0;
    public static ArrayList al = new ArrayList();
    public static FloatBuffer axisBuffer;
    public static FloatBuffer axisBufferX;
    public static FloatBuffer axisBufferY;
    public static FloatBuffer axisBufferZ;
    public static ShortBuffer axisindexBuffer;
    public static boolean bBinary = false;
    public static boolean bDebug = true;
    public static boolean bDrawAxes = false;
    public static boolean bDrawBBox = false;
    public static boolean bDrawTriangles = true;
    public static boolean bDrawVertices = false;
    public static boolean bInvert = false;
    public static boolean bLaunchedByMe = false;
    public static boolean bZoom = false;
    public static FloatBuffer dotBuffer;
    public static float f1;
    public static float f1nx;
    public static float f1ny;
    public static float f1nz;
    public static float f1x;
    public static float f1y;
    public static float f1z;
    public static float f2;
    public static float f2x;
    public static float f2y;
    public static float f2z;
    public static float f3x;
    public static float f3y;
    public static float f3z;
    public static float fAlpha = 1.0f;
    public static float fBackB = 0.0f;
    public static float fBackG = 0.0f;
    public static float fBackR = 0.0f;
    public static float[] fCrossProduct;
    public static float fDotFrameRate;
    public static float fDotScaleFactor;
    public static float fDotTheta;
    public static float fDotVelocityStart;
    public static float fDotX = 0.0f;
    public static float fDotY = 0.001f;
    public static float fDotYChange;
    public static float fDragEndX;
    public static float fDragEndY;
    public static float fDragStartX;
    public static float fDragStartY;
    public static float fFirstShiftX;
    public static float fFirstShiftY;
    public static float fFirstShiftZ;
    public static float fLens = 25.0f;
    public static float[] fQuat;
    public static float fScale = 1.0f;
    public static float fSecondShiftX;
    public static float fSecondShiftY;
    public static float fSecondShiftZ;
    public static float[] fThisQuat;
    public static float[] fTotalQuat;
    public static float fVolume;
    public static float fWidthX;
    public static float fWidthY;
    public static float fWidthZ;
    public static float fX1;
    public static float fX2;
    public static float fY1;
    public static float fY2;
    public static float fZoomBegin;
    public static float fZoomDist;
    public static float fZoomEnd;
    public static float fZoomEndX;
    public static float fZoomEndY;
    public static FloatBuffer fb;
    public static File file;
    public static float fmaxx;
    public static float fmaxy;
    public static float fmaxz;
    public static float fminx;
    public static float fminy;
    public static float fminz;
    public static byte[] fourbytes = new byte[4];
    public static float fxr;
    public static float fyr;
    public static float fzr;
    public static Random generator = new Random();
    public static int i = 0;
    public static int i2;
    public static int iAsciiFacetCount = 0;
    public static int iBinaryCounter;
    public static int iBinaryFacetCount = 0;
    public static int iDRAG = 1;
    public static int iEventID0;
    public static int iEventID1;
    public static int iFacetCount = 0;
    public static int iHeight;
    public static int iLoadMaximum;
    public static int iMaxFacets = 150000;
    public static int iModelColor = 1;
    public static int iSTATIC = 0;
    public static int iState = 0;
    public static int iValueCount = 0;
    public static int iVertCount = 0;
    public static int iVertIndex;
    public static int iWidth;
    public static int iZOOM = 2;
    public static ShortBuffer indexBuffer;
    public static FloatBuffer lightAmbientBuffer;
    public static FloatBuffer lightAmbientBuffer2;
    public static FloatBuffer lightDiffuseBuffer;
    public static FloatBuffer lightDiffuseBuffer2;
    public static FloatBuffer lightPositionBuffer;
    public static FloatBuffer lightPositionBuffer2;
    public static float[] m;
    public static float[] mThisTransform;
    public static float[] mTotalTransform;
    public static float[] mVcolor;
    public static float[] mX;
    public static float[] mY;
    public static float[] mZ;
    public static FloatBuffer matBoxBuffer;
    public static FloatBuffer matBuffer;
    public static FloatBuffer matBufferX;
    public static FloatBuffer matBufferY;
    public static FloatBuffer matBufferZ;
    public static FloatBuffer matVBuffer;
    public static float[] mcolor = new float[4];
    public static ByteBuffer nbb;
    public static FloatBuffer normalBuffer;
    public static ByteBuffer vbb;
    public static FloatBuffer vertexBuffer;
    public static float vol123;
    public static float vol132;
    public static float vol213;
    public static float vol231;
    public static float vol312;
    public static float vol321;

    public static void ToastMemoryShort(Context context) {
        Toast.makeText(context, "Sorry, can't open this file, your device is running out of memory", 1).show();
    }

    public static void DrawMesh(GL10 gl) {
        matBuffer.put(mcolor);
        matBuffer.position(0);
        gl.glMaterialfv(1032, 5634, matBuffer);
        gl.glEnableClientState(32884);
        gl.glEnableClientState(32885);
        gl.glVertexPointer(3, 5126, 0, vertexBuffer);
        gl.glNormalPointer(5126, 0, normalBuffer);
        if (bInvert) {
            gl.glFrontFace(2304);
        } else {
            gl.glFrontFace(2305);
        }
        gl.glEnable(2884);
        gl.glCullFace(1029);
        gl.glEnable(32826);
        if (bDrawTriangles) {
            gl.glDrawArrays(4, 0, GLvertsLength / 3);
        }
        gl.glDisableClientState(32884);
        gl.glDisableClientState(32885);
        gl.glDisable(2884);
    }

    public static void DrawDot(GL10 gl) {
        GLDot = new float[3];
        ByteBuffer bb = ByteBuffer.allocateDirect(12);
        if (fDotY >= 0.0f) {
            fDotY += fDotYChange;
            fDotYChange = (float) (((double) fDotYChange) - (9.8d / ((double) fDotFrameRate)));
            fDotX += (fDotVelocityStart * ((float) Math.cos((double) fDotTheta))) / fDotFrameRate;
            GLDot[0] = (fDotX * fWidthY) / fDotVelocityStart;
            GLDot[1] = (fDotY * fWidthY) / fDotVelocityStart;
            GLDot[2] = 0.0f;
            bb.order(ByteOrder.nativeOrder());
            dotBuffer = bb.asFloatBuffer();
            dotBuffer.put(GLDot);
            dotBuffer.position(0);
            gl.glMaterialfv(1032, 5634, matVBuffer);
            gl.glEnableClientState(32884);
            gl.glVertexPointer(3, 5126, 0, dotBuffer);
            gl.glEnable(2832);
            gl.glPointSize(fScale * 12.0f);
            gl.glDisable(2896);
            gl.glDrawArrays(0, 0, 1);
            gl.glEnable(2896);
            gl.glDisableClientState(32884);
            return;
        }
        fDotY = 0.001f;
        fDotX = 0.0f;
    }

    public static void DrawVertices(GL10 gl) {
        gl.glMaterialfv(1032, 5634, matVBuffer);
        gl.glEnableClientState(32884);
        gl.glVertexPointer(3, 5126, 0, vertexBuffer);
        gl.glEnable(2832);
        gl.glPointSize(fScale * 2.0f);
        gl.glDisable(2896);
        gl.glDrawArrays(0, 0, GLvertsLength / 3);
        gl.glEnable(2896);
        gl.glDisableClientState(32884);
    }

    public static void DrawAxes(GL10 gl) {
        float fHalfAxis = fWidthX;
        if (fHalfAxis < fWidthY) {
            fHalfAxis = fWidthY;
        }
        if (fHalfAxis < fWidthZ) {
            fHalfAxis = fWidthZ;
        }
        float fHalfAxis2 = fHalfAxis * 0.6f;
        float[] axisfX = {-fHalfAxis2, 0.0f, 0.0f, fHalfAxis2, 0.0f, 0.0f};
        float[] axisfY = {0.0f, -fHalfAxis2, 0.0f, 0.0f, fHalfAxis2, 0.0f};
        float[] axisfZ = {0.0f, 0.0f, -fHalfAxis2, 0.0f, 0.0f, fHalfAxis2};
        short[] axiss = new short[2];
        axiss[1] = 1;
        ByteBuffer ibb = ByteBuffer.allocateDirect(4);
        ibb.order(ByteOrder.nativeOrder());
        axisindexBuffer = ibb.asShortBuffer();
        axisindexBuffer.put(axiss);
        axisindexBuffer.position(0);
        ByteBuffer axisXBuf = ByteBuffer.allocateDirect(axisfX.length * 4);
        axisXBuf.order(ByteOrder.nativeOrder());
        axisBufferX = axisXBuf.asFloatBuffer();
        axisBufferX.put(axisfX);
        axisBufferX.position(0);
        ByteBuffer axisYBuf = ByteBuffer.allocateDirect(axisfX.length * 4);
        axisYBuf.order(ByteOrder.nativeOrder());
        axisBufferY = axisYBuf.asFloatBuffer();
        axisBufferY.put(axisfY);
        axisBufferY.position(0);
        ByteBuffer axisZBuf = ByteBuffer.allocateDirect(axisfX.length * 4);
        axisZBuf.order(ByteOrder.nativeOrder());
        axisBufferZ = axisZBuf.asFloatBuffer();
        axisBufferZ.put(axisfZ);
        axisBufferZ.position(0);
        float[] mcolorX = {1.0f, 0.0f, 0.0f, 1.0f};
        ByteBuffer colorBufX = ByteBuffer.allocateDirect(mcolorX.length * 4);
        colorBufX.order(ByteOrder.nativeOrder());
        matBufferX = colorBufX.asFloatBuffer();
        matBufferX.put(mcolorX);
        matBufferX.position(0);
        float[] mcolorY = {0.0f, 1.0f, 0.0f, 1.0f};
        ByteBuffer colorBufY = ByteBuffer.allocateDirect(mcolorY.length * 4);
        colorBufY.order(ByteOrder.nativeOrder());
        matBufferY = colorBufY.asFloatBuffer();
        matBufferY.put(mcolorY);
        matBufferY.position(0);
        float[] mcolorZ = {0.0f, 0.0f, 1.0f, 1.0f};
        ByteBuffer colorBufZ = ByteBuffer.allocateDirect(mcolorZ.length * 4);
        colorBufZ.order(ByteOrder.nativeOrder());
        matBufferZ = colorBufZ.asFloatBuffer();
        matBufferZ.put(mcolorZ);
        matBufferZ.position(0);
        gl.glLineWidth(15.0f * fScale);
        gl.glEnable(2848);
        gl.glEnableClientState(32884);
        gl.glMaterialfv(1032, 5634, matBufferX);
        gl.glVertexPointer(3, 5126, 0, axisBufferX);
        gl.glDrawElements(1, axiss.length, 5123, axisindexBuffer);
        gl.glMaterialfv(1032, 5634, matBufferY);
        gl.glVertexPointer(3, 5126, 0, axisBufferY);
        gl.glDrawElements(1, axiss.length, 5123, axisindexBuffer);
        gl.glMaterialfv(1032, 5634, matBufferZ);
        gl.glVertexPointer(3, 5126, 0, axisBufferZ);
        gl.glDrawElements(1, axiss.length, 5123, axisindexBuffer);
        gl.glDisableClientState(32884);
    }

    public static void DrawBBox(GL10 gl) {
        gl.glMaterialfv(1032, 5634, matBoxBuffer);
        gl.glLineWidth(0.25f);
        gl.glEnable(2848);
        gl.glEnableClientState(32884);
        gl.glVertexPointer(3, 5126, 0, BoxCoordBuf);
        gl.glDisable(2896);
        gl.glDrawElements(1, 24, 5123, BoxIndexBuffer);
        gl.glEnable(2896);
        gl.glDisableClientState(32884);
    }

    public static float DotProduct(float[] v0, float[] v1) {
        return (v0[0] * v1[0]) + (v0[1] * v1[1]) + (v0[2] * v1[2]);
    }

    public static void CrossProduct(float[] v0, float[] v1) {
        fCrossProduct[0] = (v0[1] * v1[2]) - (v0[2] * v1[1]);
        fCrossProduct[1] = (v0[2] * v1[0]) - (v0[0] * v1[2]);
        fCrossProduct[2] = (v0[0] * v1[1]) - (v0[1] * v1[0]);
    }

    public static void Normalise(float[] v) {
        float d = (float) Math.sqrt((double) ((v[0] * v[0]) + (v[1] * v[1]) + (v[2] * v[2])));
        v[0] = v[0] / d;
        v[1] = v[1] / d;
        v[2] = v[2] / d;
    }

    public static void NormaliseQuat(float[] q) {
        float d = (float) Math.sqrt((double) ((q[0] * q[0]) + (q[1] * q[1]) + (q[2] * q[2]) + (q[3] * q[3])));
        q[0] = q[0] / d;
        q[1] = q[1] / d;
        q[2] = q[2] / d;
        q[3] = q[3] / d;
    }

    public static void QuatToMatrix(float[] q) {
        float x = q[0];
        float y = q[1];
        float z = q[2];
        float w = q[3];
        float xsq = x * x;
        float ysq = y * y;
        float zsq = z * z;
        float wsq = w * w;
        mTotalTransform[0] = ((wsq + xsq) - ysq) - zsq;
        mTotalTransform[1] = ((2.0f * x) * y) - ((2.0f * w) * z);
        mTotalTransform[2] = (2.0f * x * z) + (2.0f * w * y);
        mTotalTransform[3] = 0.0f;
        mTotalTransform[4] = (2.0f * x * y) + (2.0f * w * z);
        mTotalTransform[5] = ((wsq - xsq) + ysq) - zsq;
        mTotalTransform[6] = ((2.0f * y) * z) - ((2.0f * w) * x);
        mTotalTransform[7] = 0.0f;
        mTotalTransform[8] = ((2.0f * x) * z) - ((2.0f * w) * y);
        mTotalTransform[9] = (2.0f * y * z) + (2.0f * w * x);
        mTotalTransform[10] = ((wsq - xsq) - ysq) + zsq;
        mTotalTransform[11] = 0.0f;
        mTotalTransform[12] = 0.0f;
        mTotalTransform[13] = 0.0f;
        mTotalTransform[14] = 0.0f;
        mTotalTransform[15] = wsq + xsq + ysq + zsq;
    }

    public static void MatrixIdentity(float[] m2) {
        m2[0] = 1.0f;
        m2[4] = 0.0f;
        m2[8] = 0.0f;
        m2[12] = 0.0f;
        m2[1] = 0.0f;
        m2[5] = 1.0f;
        m2[9] = 0.0f;
        m2[13] = 0.0f;
        m2[2] = 0.0f;
        m2[6] = 0.0f;
        m2[10] = 1.0f;
        m2[14] = 0.0f;
        m2[3] = 0.0f;
        m2[7] = 0.0f;
        m2[11] = 0.0f;
        m2[15] = 1.0f;
    }

    public static void QuatIdentity(float[] q) {
        q[0] = 0.0f;
        q[1] = 0.0f;
        q[2] = 0.0f;
        q[3] = 1.0f;
    }

    public static void MatrixCopy(float[] m1, float[] m2) {
        for (int i3 = 0; i3 < 16; i3++) {
            m2[i3] = m1[i3];
        }
    }

    public static void QuatCopy(float[] q1, float[] q2) {
        q2[0] = q1[0];
        q2[1] = q1[1];
        q2[2] = q1[2];
        q2[3] = q1[3];
    }

    public static void QuatMultiply(float[] q1, float[] q2) {
        fQuat[0] = (((q1[3] * q2[0]) + (q1[0] * q2[3])) + (q1[1] * q2[2])) - (q1[2] * q2[1]);
        fQuat[1] = ((q1[3] * q2[1]) - (q1[0] * q2[2])) + (q1[1] * q2[3]) + (q1[2] * q2[0]);
        fQuat[2] = (((q1[3] * q2[2]) + (q1[0] * q2[1])) - (q1[1] * q2[0])) + (q1[2] * q2[3]);
        fQuat[3] = (((q1[3] * q2[3]) - (q1[0] * q2[0])) - (q1[1] * q2[1])) - (q1[2] * q2[2]);
        QuatCopy(fQuat, fTotalQuat);
    }

    public static void MatrixMultiply(float[] left, float[] right, float[] dest) {
        dest[0] = (left[0] * right[0]) + (left[1] * right[4]) + (left[2] * right[8]);
        dest[1] = (left[0] * right[1]) + (left[1] * right[5]) + (left[2] * right[9]);
        dest[2] = (left[0] * right[2]) + (left[1] * right[6]) + (left[2] * right[10]);
        dest[3] = 0.0f;
        dest[4] = (left[4] * right[0]) + (left[5] * right[4]) + (left[6] * right[8]);
        dest[5] = (left[4] * right[1]) + (left[5] * right[5]) + (left[6] * right[9]);
        dest[6] = (left[4] * right[2]) + (left[5] * right[6]) + (left[6] * right[10]);
        dest[7] = 0.0f;
        dest[8] = (left[8] * right[0]) + (left[9] * right[4]) + (left[10] * right[8]);
        dest[9] = (left[8] * right[1]) + (left[9] * right[5]) + (left[10] * right[9]);
        dest[10] = (left[8] * right[2]) + (left[9] * right[6]) + (left[10] * right[10]);
        dest[11] = 0.0f;
        dest[12] = 0.0f;
        dest[13] = 0.0f;
        dest[14] = 0.0f;
        dest[15] = 1.0f;
    }

    public static int swap(int value) {
        return (((value >> 0) & 255) << 24) | (((value >> 8) & 255) << 16) | (((value >> 16) & 255) << 8) | (((value >> 24) & 255) << 0);
    }

    public static float swap(float value) {
        return Float.intBitsToFloat(swap(Float.floatToIntBits(value)));
    }

    public static float GetDistanceBetweenPoints(float fX12, float fY12, float fX22, float fY22) {
        return (float) Math.sqrt((double) (((fX22 - fX12) * (fX22 - fX12)) + ((fY22 - fY12) * (fY22 - fY12))));
    }

    public static float GetTheta(float f12, float f22) {
        float fReturnTheta = 0.0f;
        float fHalfWidth = ((float) iWidth) / 2.0f;
        float fHalfHeight = ((float) iHeight) / 2.0f;
        if (f12 < 0.0f) {
            fReturnTheta = 180.0f;
        }
        if (f12 > 0.0f && f22 < 0.0f) {
            fReturnTheta = 360.0f;
        }
        float fReturnTheta2 = fReturnTheta + (57.295776f * ((float) Math.atan((double) ((f22 - fHalfHeight) / (f12 - fHalfWidth)))));
        if (fReturnTheta2 < -180.0f) {
            fReturnTheta2 += 360.0f;
        }
        if (fReturnTheta2 > 180.0f) {
            return fReturnTheta2 - 360.0f;
        }
        return fReturnTheta2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:117:0x0240 A[SYNTHETIC, Splitter:B:117:0x0240] */
    /* JADX WARNING: Removed duplicated region for block: B:154:0x02cb A[LOOP:4: B:94:0x018d->B:154:0x02cb, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:159:0x030c  */
    /* JADX WARNING: Removed duplicated region for block: B:163:0x0487 A[LOOP:6: B:105:0x01dc->B:163:0x0487, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0069  */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x0145 A[SYNTHETIC, Splitter:B:87:0x0145] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.Boolean GetObject(java.io.File r10, java.util.ArrayList r11, android.content.Context r12) {
        /*
            r0 = 0
            java.lang.Boolean r9 = java.lang.Boolean.valueOf(r0)
            java.lang.String r1 = " "
            r11.clear()
            r0 = 0
            barry.stlviewer2.Global.iVertCount = r0
            r0 = 0
            barry.stlviewer2.Global.iFacetCount = r0
            r0 = 0
            barry.stlviewer2.Global.iValueCount = r0
            r0 = 0
            barry.stlviewer2.Global.iFacetCount = r0
            r0 = 0
            barry.stlviewer2.Global.iBinaryFacetCount = r0
            r0 = 1259902592(0x4b189680, float:1.0E7)
            barry.stlviewer2.Global.fminz = r0
            barry.stlviewer2.Global.fminy = r0
            barry.stlviewer2.Global.fminx = r0
            r0 = -887581056(0xffffffffcb189680, float:-1.0E7)
            barry.stlviewer2.Global.fmaxz = r0
            barry.stlviewer2.Global.fmaxy = r0
            barry.stlviewer2.Global.fmaxx = r0
            java.io.BufferedReader r0 = new java.io.BufferedReader     // Catch:{ FileNotFoundException -> 0x00b8 }
            java.io.FileReader r2 = new java.io.FileReader     // Catch:{ FileNotFoundException -> 0x00b8 }
            r2.<init>(r10)     // Catch:{ FileNotFoundException -> 0x00b8 }
            r0.<init>(r2)     // Catch:{ FileNotFoundException -> 0x00b8 }
            r2 = 1
            barry.stlviewer2.Global.bBinary = r2
            java.lang.String r3 = r0.readLine()     // Catch:{ IOException -> 0x00d6 }
            if (r3 == 0) goto L_0x0049
            java.lang.String[] r4 = r3.split(r1)     // Catch:{ IOException -> 0x00d6 }
            int r2 = r4.length     // Catch:{ IOException -> 0x00d6 }
            if (r2 <= 0) goto L_0x0049
            r2 = 0
        L_0x0046:
            int r5 = r4.length     // Catch:{ IOException -> 0x00d6 }
            if (r2 < r5) goto L_0x00bb
        L_0x0049:
            boolean r2 = barry.stlviewer2.Global.bBinary
            if (r2 != 0) goto L_0x04ef
            r2 = 1
            barry.stlviewer2.Global.bBinary = r2
            java.lang.String r3 = r0.readLine()     // Catch:{ IOException -> 0x00fd }
            if (r3 == 0) goto L_0x04ef
            java.lang.String[] r4 = r3.split(r1)     // Catch:{ IOException -> 0x00fd }
            int r2 = r4.length     // Catch:{ IOException -> 0x00fd }
            if (r2 <= 0) goto L_0x04ef
            r2 = 0
        L_0x005e:
            int r5 = r4.length     // Catch:{ IOException -> 0x00fd }
            if (r2 < r5) goto L_0x00e1
            r5 = r3
        L_0x0062:
            r0.close()     // Catch:{ IOException -> 0x0109 }
        L_0x0065:
            boolean r2 = barry.stlviewer2.Global.bBinary
            if (r2 == 0) goto L_0x0240
            r1 = 0
            java.io.DataInputStream r2 = new java.io.DataInputStream     // Catch:{ FileNotFoundException -> 0x010f }
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x010f }
            r3.<init>(r10)     // Catch:{ FileNotFoundException -> 0x010f }
            r2.<init>(r3)     // Catch:{ FileNotFoundException -> 0x010f }
            r4 = r2
        L_0x0075:
            r10 = 80
            r4.skipBytes(r10)     // Catch:{ IOException -> 0x0116 }
        L_0x007a:
            int r10 = r4.readUnsignedByte()     // Catch:{ IOException -> 0x011c }
            int r1 = r4.readUnsignedByte()     // Catch:{ IOException -> 0x011c }
            int r2 = r4.readUnsignedByte()     // Catch:{ IOException -> 0x011c }
            int r3 = r4.readUnsignedByte()     // Catch:{ IOException -> 0x011c }
            barry.stlviewer2.Global.iBinaryFacetCount = r3     // Catch:{ IOException -> 0x011c }
            int r3 = barry.stlviewer2.Global.iBinaryFacetCount     // Catch:{ IOException -> 0x011c }
            int r3 = r3 << 8
            barry.stlviewer2.Global.iBinaryFacetCount = r3     // Catch:{ IOException -> 0x011c }
            int r3 = barry.stlviewer2.Global.iBinaryFacetCount     // Catch:{ IOException -> 0x011c }
            int r2 = r2 + r3
            barry.stlviewer2.Global.iBinaryFacetCount = r2     // Catch:{ IOException -> 0x011c }
            int r2 = barry.stlviewer2.Global.iBinaryFacetCount     // Catch:{ IOException -> 0x011c }
            int r2 = r2 << 8
            barry.stlviewer2.Global.iBinaryFacetCount = r2     // Catch:{ IOException -> 0x011c }
            int r2 = barry.stlviewer2.Global.iBinaryFacetCount     // Catch:{ IOException -> 0x011c }
            int r1 = r1 + r2
            barry.stlviewer2.Global.iBinaryFacetCount = r1     // Catch:{ IOException -> 0x011c }
            int r1 = barry.stlviewer2.Global.iBinaryFacetCount     // Catch:{ IOException -> 0x011c }
            int r1 = r1 << 8
            barry.stlviewer2.Global.iBinaryFacetCount = r1     // Catch:{ IOException -> 0x011c }
            int r1 = barry.stlviewer2.Global.iBinaryFacetCount     // Catch:{ IOException -> 0x011c }
            int r10 = r10 + r1
            barry.stlviewer2.Global.iBinaryFacetCount = r10     // Catch:{ IOException -> 0x011c }
        L_0x00ad:
            int r10 = barry.stlviewer2.Global.iBinaryFacetCount
            int r1 = barry.stlviewer2.Global.iMaxFacets
            if (r10 <= r1) goto L_0x0121
            ToastMemoryShort(r12)
            r10 = r9
        L_0x00b7:
            return r10
        L_0x00b8:
            r10 = move-exception
            r10 = r9
            goto L_0x00b7
        L_0x00bb:
            r5 = r4[r2]     // Catch:{ IOException -> 0x00d6 }
            int r5 = r5.length()     // Catch:{ IOException -> 0x00d6 }
            if (r5 == 0) goto L_0x00cd
            r5 = r4[r2]     // Catch:{ IOException -> 0x00d6 }
            java.lang.String r6 = "solid"
            int r5 = r5.compareTo(r6)     // Catch:{ IOException -> 0x00d6 }
            if (r5 == 0) goto L_0x00d1
        L_0x00cd:
            int r2 = r2 + 1
            goto L_0x0046
        L_0x00d1:
            r2 = 0
            barry.stlviewer2.Global.bBinary = r2     // Catch:{ IOException -> 0x00d6 }
            goto L_0x0049
        L_0x00d6:
            r10 = move-exception
            r0.close()     // Catch:{ IOException -> 0x00dc }
        L_0x00da:
            r10 = r9
            goto L_0x00b7
        L_0x00dc:
            r10 = move-exception
            r10.printStackTrace()
            goto L_0x00da
        L_0x00e1:
            r5 = r4[r2]     // Catch:{ IOException -> 0x00fd }
            int r5 = r5.length()     // Catch:{ IOException -> 0x00fd }
            if (r5 == 0) goto L_0x00f3
            r5 = r4[r2]     // Catch:{ IOException -> 0x00fd }
            java.lang.String r6 = "facet"
            int r5 = r5.compareTo(r6)     // Catch:{ IOException -> 0x00fd }
            if (r5 == 0) goto L_0x00f7
        L_0x00f3:
            int r2 = r2 + 1
            goto L_0x005e
        L_0x00f7:
            r2 = 0
            barry.stlviewer2.Global.bBinary = r2     // Catch:{ IOException -> 0x00fd }
            r5 = r3
            goto L_0x0062
        L_0x00fd:
            r10 = move-exception
            r11 = r3
            r0.close()     // Catch:{ IOException -> 0x0104 }
        L_0x0102:
            r10 = r9
            goto L_0x00b7
        L_0x0104:
            r10 = move-exception
            r10.printStackTrace()
            goto L_0x0102
        L_0x0109:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0065
        L_0x010f:
            r10 = move-exception
            r10.printStackTrace()
            r4 = r1
            goto L_0x0075
        L_0x0116:
            r10 = move-exception
            r10.printStackTrace()
            goto L_0x007a
        L_0x011c:
            r10 = move-exception
            r10.printStackTrace()
            goto L_0x00ad
        L_0x0121:
            int r10 = barry.stlviewer2.Global.iBinaryFacetCount
            barry.stlviewer2.Global.iFacetCount = r10
            int r10 = barry.stlviewer2.Global.iBinaryFacetCount     // Catch:{ Exception -> 0x0221 }
            int r10 = r10 * 9
            float[] r10 = new float[r10]     // Catch:{ Exception -> 0x0221 }
            barry.stlviewer2.Global.GLverts = r10     // Catch:{ Exception -> 0x0221 }
            r10 = 0
            barry.stlviewer2.Global.iBinaryCounter = r10
            r10 = 0
            barry.stlviewer2.Global.iVertIndex = r10
        L_0x0133:
            int r10 = barry.stlviewer2.Global.iBinaryCounter
            int r1 = barry.stlviewer2.Global.iBinaryFacetCount
            if (r10 < r1) goto L_0x022e
            r10 = r0
            r0 = r5
        L_0x013b:
            int r10 = barry.stlviewer2.Global.iVertCount
            int r10 = r10 * 3
            barry.stlviewer2.Global.iValueCount = r10
            boolean r10 = barry.stlviewer2.Global.bBinary
            if (r10 != 0) goto L_0x0157
            int r10 = barry.stlviewer2.Global.iValueCount     // Catch:{ Exception -> 0x02a4 }
            float[] r10 = new float[r10]     // Catch:{ Exception -> 0x02a4 }
            barry.stlviewer2.Global.GLverts = r10     // Catch:{ Exception -> 0x02a4 }
            r10 = 0
            barry.stlviewer2.Global.i = r10
        L_0x014e:
            int r10 = barry.stlviewer2.Global.i
            int r0 = barry.stlviewer2.Global.iValueCount
            if (r10 < r0) goto L_0x02b1
            r11.clear()
        L_0x0157:
            float r10 = barry.stlviewer2.Global.fmaxx
            float r11 = barry.stlviewer2.Global.fminx
            float r10 = r10 - r11
            barry.stlviewer2.Global.fWidthX = r10
            float r10 = barry.stlviewer2.Global.fmaxy
            float r11 = barry.stlviewer2.Global.fminy
            float r10 = r10 - r11
            barry.stlviewer2.Global.fWidthY = r10
            float r10 = barry.stlviewer2.Global.fmaxz
            float r11 = barry.stlviewer2.Global.fminz
            float r10 = r10 - r11
            barry.stlviewer2.Global.fWidthZ = r10
            r10 = 0
            float r11 = barry.stlviewer2.Global.fminx
            float r0 = barry.stlviewer2.Global.fWidthX
            r1 = 1073741824(0x40000000, float:2.0)
            float r0 = r0 / r1
            float r11 = r11 + r0
            float r10 = r10 - r11
            r11 = 0
            float r0 = barry.stlviewer2.Global.fminy
            float r1 = barry.stlviewer2.Global.fWidthY
            r2 = 1073741824(0x40000000, float:2.0)
            float r1 = r1 / r2
            float r0 = r0 + r1
            float r11 = r11 - r0
            r0 = 0
            float r1 = barry.stlviewer2.Global.fminz
            float r2 = barry.stlviewer2.Global.fWidthZ
            r3 = 1073741824(0x40000000, float:2.0)
            float r2 = r2 / r3
            float r1 = r1 + r2
            float r0 = r0 - r1
            r1 = 0
            barry.stlviewer2.Global.i = r1
        L_0x018d:
            int r1 = barry.stlviewer2.Global.i
            int r2 = barry.stlviewer2.Global.iValueCount
            if (r1 < r2) goto L_0x02cb
            float[] r10 = barry.stlviewer2.Global.GLverts
            int r10 = r10.length
            barry.stlviewer2.Global.GLvertsLength = r10
            int r10 = barry.stlviewer2.Global.iFacetCount     // Catch:{ Exception -> 0x02f2 }
            int r10 = r10 * 9
            float[] r10 = new float[r10]     // Catch:{ Exception -> 0x02f2 }
            barry.stlviewer2.Global.GLnormals = r10     // Catch:{ Exception -> 0x02f2 }
            float[] r10 = barry.stlviewer2.Global.GLverts     // Catch:{ Exception -> 0x02ff }
            int r10 = r10.length     // Catch:{ Exception -> 0x02ff }
            int r10 = r10 * 4
            java.nio.ByteBuffer r10 = java.nio.ByteBuffer.allocateDirect(r10)     // Catch:{ Exception -> 0x02ff }
            barry.stlviewer2.Global.vbb = r10     // Catch:{ Exception -> 0x02ff }
            java.nio.ByteBuffer r10 = barry.stlviewer2.Global.vbb
            java.nio.ByteOrder r11 = java.nio.ByteOrder.nativeOrder()
            r10.order(r11)
            java.nio.ByteBuffer r10 = barry.stlviewer2.Global.vbb
            java.nio.FloatBuffer r10 = r10.asFloatBuffer()
            barry.stlviewer2.Global.vertexBuffer = r10
            java.nio.FloatBuffer r10 = barry.stlviewer2.Global.vertexBuffer
            float[] r11 = barry.stlviewer2.Global.GLverts
            r10.put(r11)
            java.nio.FloatBuffer r10 = barry.stlviewer2.Global.vertexBuffer
            r11 = 0
            r10.position(r11)
            r10 = 0
            barry.stlviewer2.Global.i = r10
            r10 = 0
            barry.stlviewer2.Global.i2 = r10
        L_0x01cf:
            int r10 = barry.stlviewer2.Global.i
            float[] r11 = barry.stlviewer2.Global.GLverts
            int r11 = r11.length
            if (r10 < r11) goto L_0x030c
            r10 = 0
            barry.stlviewer2.Global.fVolume = r10
            r10 = 0
            barry.stlviewer2.Global.i = r10
        L_0x01dc:
            int r10 = barry.stlviewer2.Global.i
            float[] r11 = barry.stlviewer2.Global.GLverts
            int r11 = r11.length
            if (r10 < r11) goto L_0x0487
            float r10 = barry.stlviewer2.Global.fVolume
            float r10 = java.lang.Math.abs(r10)
            barry.stlviewer2.Global.fVolume = r10
            r10 = 0
            barry.stlviewer2.Global.GLverts = r10
            float[] r10 = barry.stlviewer2.Global.GLnormals     // Catch:{ Exception -> 0x04de }
            int r10 = r10.length     // Catch:{ Exception -> 0x04de }
            int r10 = r10 * 4
            java.nio.ByteBuffer r10 = java.nio.ByteBuffer.allocateDirect(r10)     // Catch:{ Exception -> 0x04de }
            barry.stlviewer2.Global.nbb = r10     // Catch:{ Exception -> 0x04de }
            java.nio.ByteBuffer r10 = barry.stlviewer2.Global.nbb
            java.nio.ByteOrder r11 = java.nio.ByteOrder.nativeOrder()
            r10.order(r11)
            java.nio.ByteBuffer r10 = barry.stlviewer2.Global.nbb
            java.nio.FloatBuffer r10 = r10.asFloatBuffer()
            barry.stlviewer2.Global.normalBuffer = r10
            java.nio.FloatBuffer r10 = barry.stlviewer2.Global.normalBuffer
            float[] r11 = barry.stlviewer2.Global.GLnormals
            r10.put(r11)
            java.nio.FloatBuffer r10 = barry.stlviewer2.Global.normalBuffer
            r11 = 0
            r10.position(r11)
            r10 = 0
            barry.stlviewer2.Global.GLnormals = r10
            r10 = 1
            java.lang.Boolean r10 = java.lang.Boolean.valueOf(r10)
            goto L_0x00b7
        L_0x0221:
            r10 = move-exception
            ToastMemoryShort(r12)
            r11.clear()
            r10 = 0
            barry.stlviewer2.Global.GLverts = r10
            r10 = r9
            goto L_0x00b7
        L_0x022e:
            int r10 = GetFacetBinary(r4, r11, r12)
            r1 = -1
            if (r10 != r1) goto L_0x0238
            r10 = r9
            goto L_0x00b7
        L_0x0238:
            int r10 = barry.stlviewer2.Global.iBinaryCounter
            int r10 = r10 + 1
            barry.stlviewer2.Global.iBinaryCounter = r10
            goto L_0x0133
        L_0x0240:
            java.io.BufferedReader r0 = new java.io.BufferedReader     // Catch:{ FileNotFoundException -> 0x026e }
            java.io.FileReader r2 = new java.io.FileReader     // Catch:{ FileNotFoundException -> 0x026e }
            r2.<init>(r10)     // Catch:{ FileNotFoundException -> 0x026e }
            r0.<init>(r2)     // Catch:{ FileNotFoundException -> 0x026e }
            java.lang.String r2 = r0.readLine()     // Catch:{ IOException -> 0x028a }
            if (r2 == 0) goto L_0x025b
            java.lang.String[] r3 = r2.split(r1)     // Catch:{ IOException -> 0x04eb }
            int r1 = r3.length     // Catch:{ IOException -> 0x04eb }
            if (r1 <= 0) goto L_0x025b
            r1 = 0
        L_0x0258:
            int r4 = r3.length     // Catch:{ IOException -> 0x04eb }
            if (r1 < r4) goto L_0x0272
        L_0x025b:
            r1 = 0
            barry.stlviewer2.Global.iAsciiFacetCount = r1
        L_0x025e:
            int r1 = GetFacetAscii(r10, r0, r11, r12)
            int r3 = barry.stlviewer2.Global.iAsciiFacetCount
            int r3 = r3 + 1
            barry.stlviewer2.Global.iAsciiFacetCount = r3
            r3 = -1
            if (r1 != r3) goto L_0x028f
            r10 = r9
            goto L_0x00b7
        L_0x026e:
            r10 = move-exception
            r10 = r9
            goto L_0x00b7
        L_0x0272:
            r4 = r3[r1]     // Catch:{ IOException -> 0x04eb }
            int r4 = r4.length()     // Catch:{ IOException -> 0x04eb }
            if (r4 == 0) goto L_0x0287
            r1 = r3[r1]     // Catch:{ IOException -> 0x04eb }
            java.lang.String r3 = "solid"
            int r1 = r1.compareTo(r3)     // Catch:{ IOException -> 0x04eb }
            if (r1 == 0) goto L_0x025b
            r10 = r9
            goto L_0x00b7
        L_0x0287:
            int r1 = r1 + 1
            goto L_0x0258
        L_0x028a:
            r10 = move-exception
            r11 = r5
        L_0x028c:
            r10 = r9
            goto L_0x00b7
        L_0x028f:
            if (r1 != 0) goto L_0x0295
            r10 = r0
            r0 = r2
            goto L_0x013b
        L_0x0295:
            r3 = 1
            if (r1 != r3) goto L_0x0298
        L_0x0298:
            int r1 = barry.stlviewer2.Global.iAsciiFacetCount
            int r3 = barry.stlviewer2.Global.iMaxFacets
            if (r1 <= r3) goto L_0x025e
            ToastMemoryShort(r12)
            r10 = r9
            goto L_0x00b7
        L_0x02a4:
            r10 = move-exception
            ToastMemoryShort(r12)
            r11.clear()
            r10 = 0
            barry.stlviewer2.Global.GLverts = r10
            r10 = r9
            goto L_0x00b7
        L_0x02b1:
            int r10 = barry.stlviewer2.Global.i
            java.lang.Object r10 = r11.get(r10)
            java.lang.Float r10 = (java.lang.Float) r10
            float r10 = r10.floatValue()
            float[] r0 = barry.stlviewer2.Global.GLverts
            int r1 = barry.stlviewer2.Global.i
            r0[r1] = r10
            int r10 = barry.stlviewer2.Global.i
            int r10 = r10 + 1
            barry.stlviewer2.Global.i = r10
            goto L_0x014e
        L_0x02cb:
            float[] r1 = barry.stlviewer2.Global.GLverts
            int r2 = barry.stlviewer2.Global.i
            r3 = r1[r2]
            float r3 = r3 + r10
            r1[r2] = r3
            float[] r1 = barry.stlviewer2.Global.GLverts
            int r2 = barry.stlviewer2.Global.i
            int r2 = r2 + 1
            r3 = r1[r2]
            float r3 = r3 + r11
            r1[r2] = r3
            float[] r1 = barry.stlviewer2.Global.GLverts
            int r2 = barry.stlviewer2.Global.i
            int r2 = r2 + 2
            r3 = r1[r2]
            float r3 = r3 + r0
            r1[r2] = r3
            int r1 = barry.stlviewer2.Global.i
            int r1 = r1 + 3
            barry.stlviewer2.Global.i = r1
            goto L_0x018d
        L_0x02f2:
            r10 = move-exception
            ToastMemoryShort(r12)
            r10 = 0
            barry.stlviewer2.Global.GLverts = r10
            r10 = 0
            barry.stlviewer2.Global.GLnormals = r10
            r10 = r9
            goto L_0x00b7
        L_0x02ff:
            r10 = move-exception
            ToastMemoryShort(r12)
            r10 = 0
            barry.stlviewer2.Global.GLverts = r10
            r10 = 0
            barry.stlviewer2.Global.GLnormals = r10
            r10 = r9
            goto L_0x00b7
        L_0x030c:
            float[] r10 = barry.stlviewer2.Global.GLverts
            int r11 = barry.stlviewer2.Global.i
            r10 = r10[r11]
            float[] r11 = barry.stlviewer2.Global.GLverts
            int r0 = barry.stlviewer2.Global.i
            int r0 = r0 + 3
            r11 = r11[r0]
            float r10 = r10 - r11
            float[] r11 = barry.stlviewer2.Global.GLverts
            int r0 = barry.stlviewer2.Global.i
            int r0 = r0 + 1
            r11 = r11[r0]
            float[] r0 = barry.stlviewer2.Global.GLverts
            int r1 = barry.stlviewer2.Global.i
            int r1 = r1 + 4
            r0 = r0[r1]
            float r11 = r11 - r0
            float[] r0 = barry.stlviewer2.Global.GLverts
            int r1 = barry.stlviewer2.Global.i
            int r1 = r1 + 2
            r0 = r0[r1]
            float[] r1 = barry.stlviewer2.Global.GLverts
            int r2 = barry.stlviewer2.Global.i
            int r2 = r2 + 5
            r1 = r1[r2]
            float r0 = r0 - r1
            float[] r1 = barry.stlviewer2.Global.GLverts
            int r2 = barry.stlviewer2.Global.i
            int r2 = r2 + 3
            r1 = r1[r2]
            float[] r2 = barry.stlviewer2.Global.GLverts
            int r3 = barry.stlviewer2.Global.i
            int r3 = r3 + 6
            r2 = r2[r3]
            float r1 = r1 - r2
            float[] r2 = barry.stlviewer2.Global.GLverts
            int r3 = barry.stlviewer2.Global.i
            int r3 = r3 + 4
            r2 = r2[r3]
            float[] r3 = barry.stlviewer2.Global.GLverts
            int r4 = barry.stlviewer2.Global.i
            int r4 = r4 + 7
            r3 = r3[r4]
            float r2 = r2 - r3
            float[] r3 = barry.stlviewer2.Global.GLverts
            int r4 = barry.stlviewer2.Global.i
            int r4 = r4 + 5
            r3 = r3[r4]
            float[] r4 = barry.stlviewer2.Global.GLverts
            int r5 = barry.stlviewer2.Global.i
            int r5 = r5 + 8
            r4 = r4[r5]
            float r3 = r3 - r4
            float[] r4 = barry.stlviewer2.Global.GLnormals
            int r5 = barry.stlviewer2.Global.i
            float r6 = r11 * r3
            float r7 = r0 * r2
            float r6 = r6 - r7
            r4[r5] = r6
            float[] r4 = barry.stlviewer2.Global.GLnormals
            int r5 = barry.stlviewer2.Global.i
            int r5 = r5 + 1
            float r6 = r0 * r1
            float r7 = r10 * r3
            float r6 = r6 - r7
            r4[r5] = r6
            float[] r4 = barry.stlviewer2.Global.GLnormals
            int r5 = barry.stlviewer2.Global.i
            int r5 = r5 + 2
            float r6 = r10 * r2
            float r7 = r11 * r1
            float r6 = r6 - r7
            r4[r5] = r6
            float[] r4 = barry.stlviewer2.Global.GLnormals
            int r5 = barry.stlviewer2.Global.i
            int r5 = r5 + 3
            float r6 = r11 * r3
            float r7 = r0 * r2
            float r6 = r6 - r7
            r4[r5] = r6
            float[] r4 = barry.stlviewer2.Global.GLnormals
            int r5 = barry.stlviewer2.Global.i
            int r5 = r5 + 4
            float r6 = r0 * r1
            float r7 = r10 * r3
            float r6 = r6 - r7
            r4[r5] = r6
            float[] r4 = barry.stlviewer2.Global.GLnormals
            int r5 = barry.stlviewer2.Global.i
            int r5 = r5 + 5
            float r6 = r10 * r2
            float r7 = r11 * r1
            float r6 = r6 - r7
            r4[r5] = r6
            float[] r4 = barry.stlviewer2.Global.GLnormals
            int r5 = barry.stlviewer2.Global.i
            int r5 = r5 + 6
            float r6 = r11 * r3
            float r7 = r0 * r2
            float r6 = r6 - r7
            r4[r5] = r6
            float[] r4 = barry.stlviewer2.Global.GLnormals
            int r5 = barry.stlviewer2.Global.i
            int r5 = r5 + 7
            float r0 = r0 * r1
            float r3 = r3 * r10
            float r0 = r0 - r3
            r4[r5] = r0
            float[] r0 = barry.stlviewer2.Global.GLnormals
            int r3 = barry.stlviewer2.Global.i
            int r3 = r3 + 8
            float r10 = r10 * r2
            float r11 = r11 * r1
            float r10 = r10 - r11
            r0[r3] = r10
            float[] r10 = barry.stlviewer2.Global.GLnormals
            int r11 = barry.stlviewer2.Global.i
            r10 = r10[r11]
            float[] r11 = barry.stlviewer2.Global.GLnormals
            int r0 = barry.stlviewer2.Global.i
            r11 = r11[r0]
            float r10 = r10 * r11
            float[] r11 = barry.stlviewer2.Global.GLnormals
            int r0 = barry.stlviewer2.Global.i
            int r0 = r0 + 1
            r11 = r11[r0]
            float[] r0 = barry.stlviewer2.Global.GLnormals
            int r1 = barry.stlviewer2.Global.i
            int r1 = r1 + 1
            r0 = r0[r1]
            float r11 = r11 * r0
            float r10 = r10 + r11
            float[] r11 = barry.stlviewer2.Global.GLnormals
            int r0 = barry.stlviewer2.Global.i
            int r0 = r0 + 2
            r11 = r11[r0]
            float[] r0 = barry.stlviewer2.Global.GLnormals
            int r1 = barry.stlviewer2.Global.i
            int r1 = r1 + 2
            r0 = r0[r1]
            float r11 = r11 * r0
            float r10 = r10 + r11
            double r10 = (double) r10
            double r10 = java.lang.Math.sqrt(r10)
            float r10 = (float) r10
            r11 = 0
            int r11 = (r10 > r11 ? 1 : (r10 == r11 ? 0 : -1))
            if (r11 != 0) goto L_0x041d
            r10 = 1065353216(0x3f800000, float:1.0)
        L_0x041d:
            float[] r11 = barry.stlviewer2.Global.GLnormals
            int r0 = barry.stlviewer2.Global.i
            r1 = r11[r0]
            float r1 = r1 / r10
            r11[r0] = r1
            float[] r11 = barry.stlviewer2.Global.GLnormals
            int r0 = barry.stlviewer2.Global.i
            int r0 = r0 + 1
            r1 = r11[r0]
            float r1 = r1 / r10
            r11[r0] = r1
            float[] r11 = barry.stlviewer2.Global.GLnormals
            int r0 = barry.stlviewer2.Global.i
            int r0 = r0 + 2
            r1 = r11[r0]
            float r1 = r1 / r10
            r11[r0] = r1
            float[] r11 = barry.stlviewer2.Global.GLnormals
            int r0 = barry.stlviewer2.Global.i
            int r0 = r0 + 3
            r1 = r11[r0]
            float r1 = r1 / r10
            r11[r0] = r1
            float[] r11 = barry.stlviewer2.Global.GLnormals
            int r0 = barry.stlviewer2.Global.i
            int r0 = r0 + 4
            r1 = r11[r0]
            float r1 = r1 / r10
            r11[r0] = r1
            float[] r11 = barry.stlviewer2.Global.GLnormals
            int r0 = barry.stlviewer2.Global.i
            int r0 = r0 + 5
            r1 = r11[r0]
            float r1 = r1 / r10
            r11[r0] = r1
            float[] r11 = barry.stlviewer2.Global.GLnormals
            int r0 = barry.stlviewer2.Global.i
            int r0 = r0 + 6
            r1 = r11[r0]
            float r1 = r1 / r10
            r11[r0] = r1
            float[] r11 = barry.stlviewer2.Global.GLnormals
            int r0 = barry.stlviewer2.Global.i
            int r0 = r0 + 7
            r1 = r11[r0]
            float r1 = r1 / r10
            r11[r0] = r1
            float[] r11 = barry.stlviewer2.Global.GLnormals
            int r0 = barry.stlviewer2.Global.i
            int r0 = r0 + 8
            r1 = r11[r0]
            float r10 = r1 / r10
            r11[r0] = r10
            int r10 = barry.stlviewer2.Global.i
            int r10 = r10 + 9
            barry.stlviewer2.Global.i = r10
            goto L_0x01cf
        L_0x0487:
            float r10 = barry.stlviewer2.Global.fVolume
            float[] r11 = barry.stlviewer2.Global.GLverts
            int r0 = barry.stlviewer2.Global.i
            r0 = r11[r0]
            float[] r11 = barry.stlviewer2.Global.GLverts
            int r1 = barry.stlviewer2.Global.i
            int r1 = r1 + 1
            r1 = r11[r1]
            float[] r11 = barry.stlviewer2.Global.GLverts
            int r2 = barry.stlviewer2.Global.i
            int r2 = r2 + 2
            r2 = r11[r2]
            float[] r11 = barry.stlviewer2.Global.GLverts
            int r3 = barry.stlviewer2.Global.i
            int r3 = r3 + 3
            r3 = r11[r3]
            float[] r11 = barry.stlviewer2.Global.GLverts
            int r4 = barry.stlviewer2.Global.i
            int r4 = r4 + 4
            r4 = r11[r4]
            float[] r11 = barry.stlviewer2.Global.GLverts
            int r5 = barry.stlviewer2.Global.i
            int r5 = r5 + 5
            r5 = r11[r5]
            float[] r11 = barry.stlviewer2.Global.GLverts
            int r6 = barry.stlviewer2.Global.i
            int r6 = r6 + 6
            r6 = r11[r6]
            float[] r11 = barry.stlviewer2.Global.GLverts
            int r7 = barry.stlviewer2.Global.i
            int r7 = r7 + 7
            r7 = r11[r7]
            float[] r11 = barry.stlviewer2.Global.GLverts
            int r8 = barry.stlviewer2.Global.i
            int r8 = r8 + 8
            r8 = r11[r8]
            float r11 = SignedVolumeOfTriangle(r0, r1, r2, r3, r4, r5, r6, r7, r8)
            float r10 = r10 + r11
            barry.stlviewer2.Global.fVolume = r10
            int r10 = barry.stlviewer2.Global.i
            int r10 = r10 + 9
            barry.stlviewer2.Global.i = r10
            goto L_0x01dc
        L_0x04de:
            r10 = move-exception
            ToastMemoryShort(r12)
            r10 = 0
            barry.stlviewer2.Global.vbb = r10
            r10 = 0
            barry.stlviewer2.Global.GLnormals = r10
            r10 = r9
            goto L_0x00b7
        L_0x04eb:
            r10 = move-exception
            r11 = r2
            goto L_0x028c
        L_0x04ef:
            r5 = r3
            goto L_0x0062
        */
        throw new UnsupportedOperationException("Method not decompiled: barry.stlviewer2.Global.GetObject(java.io.File, java.util.ArrayList, android.content.Context):java.lang.Boolean");
    }

    public static int GetFacetBinary(DataInputStream in, ArrayList al2, Context context) {
        try {
            in.skipBytes(12);
            in.readFully(fourbytes);
            f1x = Float.intBitsToFloat(((fourbytes[3] & 255) << 24) | ((fourbytes[2] & 255) << 16) | ((fourbytes[1] & 255) << 8) | ((fourbytes[0] & 255) << 0));
            in.readFully(fourbytes);
            f1y = Float.intBitsToFloat(((fourbytes[3] & 255) << 24) | ((fourbytes[2] & 255) << 16) | ((fourbytes[1] & 255) << 8) | ((fourbytes[0] & 255) << 0));
            in.readFully(fourbytes);
            f1z = Float.intBitsToFloat(((fourbytes[3] & 255) << 24) | ((fourbytes[2] & 255) << 16) | ((fourbytes[1] & 255) << 8) | ((fourbytes[0] & 255) << 0));
            GLverts[iVertIndex] = f1x;
            iVertIndex++;
            if (fminx > f1x) {
                fminx = f1x;
            }
            if (fmaxx < f1x) {
                fmaxx = f1x;
            }
            GLverts[iVertIndex] = f1y;
            iVertIndex++;
            if (fminy > f1y) {
                fminy = f1y;
            }
            if (fmaxy < f1y) {
                fmaxy = f1y;
            }
            GLverts[iVertIndex] = f1z;
            iVertIndex++;
            if (fminz > f1z) {
                fminz = f1z;
            }
            if (fmaxz < f1z) {
                fmaxz = f1z;
            }
            iVertCount++;
            in.readFully(fourbytes);
            f2x = Float.intBitsToFloat(((fourbytes[3] & 255) << 24) | ((fourbytes[2] & 255) << 16) | ((fourbytes[1] & 255) << 8) | ((fourbytes[0] & 255) << 0));
            in.readFully(fourbytes);
            f2y = Float.intBitsToFloat(((fourbytes[3] & 255) << 24) | ((fourbytes[2] & 255) << 16) | ((fourbytes[1] & 255) << 8) | ((fourbytes[0] & 255) << 0));
            in.readFully(fourbytes);
            f2z = Float.intBitsToFloat(((fourbytes[3] & 255) << 24) | ((fourbytes[2] & 255) << 16) | ((fourbytes[1] & 255) << 8) | ((fourbytes[0] & 255) << 0));
            GLverts[iVertIndex] = f2x;
            iVertIndex++;
            if (fminx > f2x) {
                fminx = f2x;
            }
            if (fmaxx < f2x) {
                fmaxx = f2x;
            }
            GLverts[iVertIndex] = f2y;
            iVertIndex++;
            if (fminy > f2y) {
                fminy = f2y;
            }
            if (fmaxy < f2y) {
                fmaxy = f2y;
            }
            GLverts[iVertIndex] = f2z;
            iVertIndex++;
            if (fminz > f2z) {
                fminz = f2z;
            }
            if (fmaxz < f2z) {
                fmaxz = f2z;
            }
            iVertCount++;
            in.readFully(fourbytes);
            f3x = Float.intBitsToFloat(((fourbytes[3] & 255) << 24) | ((fourbytes[2] & 255) << 16) | ((fourbytes[1] & 255) << 8) | ((fourbytes[0] & 255) << 0));
            in.readFully(fourbytes);
            f3y = Float.intBitsToFloat(((fourbytes[3] & 255) << 24) | ((fourbytes[2] & 255) << 16) | ((fourbytes[1] & 255) << 8) | ((fourbytes[0] & 255) << 0));
            in.readFully(fourbytes);
            f3z = Float.intBitsToFloat(((fourbytes[3] & 255) << 24) | ((fourbytes[2] & 255) << 16) | ((fourbytes[1] & 255) << 8) | ((fourbytes[0] & 255) << 0));
            GLverts[iVertIndex] = f3x;
            iVertIndex++;
            if (fminx > f3x) {
                fminx = f3x;
            }
            if (fmaxx < f3x) {
                fmaxx = f3x;
            }
            GLverts[iVertIndex] = f3y;
            iVertIndex++;
            if (fminy > f3y) {
                fminy = f3y;
            }
            if (fmaxy < f3y) {
                fmaxy = f3y;
            }
            GLverts[iVertIndex] = f3z;
            iVertIndex++;
            if (fminz > f3z) {
                fminz = f3z;
            }
            if (fmaxz < f3z) {
                fmaxz = f3z;
            }
            iVertCount++;
            in.skipBytes(2);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return iBinaryFacetCount;
    }

    public static int GetFacetAscii(File file2, BufferedReader br, ArrayList al2, Context context) {
        try {
            String line = br.readLine();
            if (line != null) {
                String[] tokens = line.split(" ");
                if (tokens.length > 0) {
                    int iCount = 0;
                    while (true) {
                        if (iCount < tokens.length) {
                            if (tokens[iCount].length() == 0) {
                                iCount++;
                            } else if (tokens[iCount].compareTo("facet") != 0) {
                                return tokens[iCount].compareTo("endsolid") == 0 ? 0 : -1;
                            }
                        }
                    }
                }
            }
            try {
                String line2 = br.readLine();
                if (line2 != null) {
                    String[] tokens2 = line2.split(" ");
                    if (tokens2.length > 0) {
                        int iCount2 = 0;
                        while (true) {
                            if (iCount2 < tokens2.length) {
                                if (tokens2[iCount2].length() == 0) {
                                    iCount2++;
                                } else if (tokens2[iCount2].compareTo("outer") != 0) {
                                    return -1;
                                }
                            }
                        }
                    }
                }
                try {
                    String line3 = br.readLine();
                    if (line3 != null) {
                        String[] tokens3 = line3.split(" ");
                        if (tokens3.length > 0) {
                            int iCount3 = 0;
                            while (true) {
                                if (iCount3 >= tokens3.length) {
                                    break;
                                } else if (tokens3[iCount3].length() == 0) {
                                    iCount3++;
                                } else if (tokens3[iCount3].compareTo("vertex") != 0) {
                                    return -1;
                                }
                            }
                            int iCount4 = iCount3 + 1;
                            while (tokens3[iCount4].length() == 0) {
                                iCount4++;
                            }
                            f1x = Float.valueOf(tokens3[iCount4]).floatValue();
                            if (fminx > f1x) {
                                fminx = f1x;
                            }
                            if (fmaxx < f1x) {
                                fmaxx = f1x;
                            }
                            f1y = Float.valueOf(tokens3[iCount4 + 1]).floatValue();
                            if (fminy > f1y) {
                                fminy = f1y;
                            }
                            if (fmaxy < f1y) {
                                fmaxy = f1y;
                            }
                            f1z = Float.valueOf(tokens3[iCount4 + 2]).floatValue();
                            if (fminz > f1z) {
                                fminz = f1z;
                            }
                            if (fmaxz < f1z) {
                                fmaxz = f1z;
                            }
                            try {
                                al2.add(Float.valueOf(f1x));
                                try {
                                    al2.add(Float.valueOf(f1y));
                                    try {
                                        al2.add(Float.valueOf(f1z));
                                        iVertCount++;
                                    } catch (Exception e) {
                                        Exception exc = e;
                                        ToastMemoryShort(context);
                                        return -1;
                                    }
                                } catch (Exception e2) {
                                    Exception exc2 = e2;
                                    ToastMemoryShort(context);
                                    return -1;
                                }
                            } catch (Exception e3) {
                                Exception exc3 = e3;
                                ToastMemoryShort(context);
                                return -1;
                            }
                        }
                    }
                    try {
                        String line4 = br.readLine();
                        if (line4 != null) {
                            String[] tokens4 = line4.split(" ");
                            if (tokens4.length > 0) {
                                int iCount5 = 0;
                                while (true) {
                                    if (iCount5 >= tokens4.length) {
                                        break;
                                    } else if (tokens4[iCount5].length() == 0) {
                                        iCount5++;
                                    } else if (tokens4[iCount5].compareTo("vertex") != 0) {
                                        return -1;
                                    }
                                }
                                int iCount6 = iCount5 + 1;
                                while (tokens4[iCount6].length() == 0) {
                                    iCount6++;
                                }
                                f1x = Float.valueOf(tokens4[iCount6]).floatValue();
                                if (fminx > f1x) {
                                    fminx = f1x;
                                }
                                if (fmaxx < f1x) {
                                    fmaxx = f1x;
                                }
                                f1y = Float.valueOf(tokens4[iCount6 + 1]).floatValue();
                                if (fminy > f1y) {
                                    fminy = f1y;
                                }
                                if (fmaxy < f1y) {
                                    fmaxy = f1y;
                                }
                                f1z = Float.valueOf(tokens4[iCount6 + 2]).floatValue();
                                if (fminz > f1z) {
                                    fminz = f1z;
                                }
                                if (fmaxz < f1z) {
                                    fmaxz = f1z;
                                }
                                try {
                                    al2.add(Float.valueOf(f1x));
                                    try {
                                        al2.add(Float.valueOf(f1y));
                                        try {
                                            al2.add(Float.valueOf(f1z));
                                            iVertCount++;
                                        } catch (Exception e4) {
                                            Exception exc4 = e4;
                                            ToastMemoryShort(context);
                                            return -1;
                                        }
                                    } catch (Exception e5) {
                                        Exception exc5 = e5;
                                        ToastMemoryShort(context);
                                        return -1;
                                    }
                                } catch (Exception e6) {
                                    Exception exc6 = e6;
                                    ToastMemoryShort(context);
                                    return -1;
                                }
                            }
                        }
                        try {
                            String line5 = br.readLine();
                            if (line5 != null) {
                                String[] tokens5 = line5.split(" ");
                                if (tokens5.length > 0) {
                                    int iCount7 = 0;
                                    while (true) {
                                        if (iCount7 >= tokens5.length) {
                                            break;
                                        } else if (tokens5[iCount7].length() == 0) {
                                            iCount7++;
                                        } else if (tokens5[iCount7].compareTo("vertex") != 0) {
                                            return -1;
                                        }
                                    }
                                    int iCount8 = iCount7 + 1;
                                    while (tokens5[iCount8].length() == 0) {
                                        iCount8++;
                                    }
                                    f1x = Float.valueOf(tokens5[iCount8]).floatValue();
                                    if (fminx > f1x) {
                                        fminx = f1x;
                                    }
                                    if (fmaxx < f1x) {
                                        fmaxx = f1x;
                                    }
                                    f1y = Float.valueOf(tokens5[iCount8 + 1]).floatValue();
                                    if (fminy > f1y) {
                                        fminy = f1y;
                                    }
                                    if (fmaxy < f1y) {
                                        fmaxy = f1y;
                                    }
                                    f1z = Float.valueOf(tokens5[iCount8 + 2]).floatValue();
                                    if (fminz > f1z) {
                                        fminz = f1z;
                                    }
                                    if (fmaxz < f1z) {
                                        fmaxz = f1z;
                                    }
                                    try {
                                        al2.add(Float.valueOf(f1x));
                                        try {
                                            al2.add(Float.valueOf(f1y));
                                            try {
                                                al2.add(Float.valueOf(f1z));
                                                iVertCount++;
                                            } catch (Exception e7) {
                                                Exception exc7 = e7;
                                                ToastMemoryShort(context);
                                                return -1;
                                            }
                                        } catch (Exception e8) {
                                            Exception exc8 = e8;
                                            ToastMemoryShort(context);
                                            return -1;
                                        }
                                    } catch (Exception e9) {
                                        Exception exc9 = e9;
                                        ToastMemoryShort(context);
                                        return -1;
                                    }
                                }
                            }
                            try {
                                String line6 = br.readLine();
                                if (line6 != null) {
                                    String[] tokens6 = line6.split(" ");
                                    if (tokens6.length > 0) {
                                        int iCount9 = 0;
                                        while (true) {
                                            if (iCount9 < tokens6.length) {
                                                if (tokens6[iCount9].length() == 0) {
                                                    iCount9++;
                                                } else if (tokens6[iCount9].compareTo("endloop") != 0) {
                                                    return -1;
                                                }
                                            }
                                        }
                                    }
                                }
                                try {
                                    String line7 = br.readLine();
                                    if (line7 != null) {
                                        String[] tokens7 = line7.split(" ");
                                        if (tokens7.length > 0) {
                                            int iCount10 = 0;
                                            while (true) {
                                                if (iCount10 >= tokens7.length) {
                                                    break;
                                                } else if (tokens7[iCount10].length() == 0) {
                                                    iCount10++;
                                                } else if (tokens7[iCount10].compareTo("endfacet") != 0) {
                                                    return -1;
                                                }
                                            }
                                        }
                                    }
                                    iFacetCount++;
                                    return 1;
                                } catch (IOException e10) {
                                    return -1;
                                }
                            } catch (IOException e11) {
                                return -1;
                            }
                        } catch (IOException e12) {
                            return -1;
                        }
                    } catch (IOException e13) {
                        return -1;
                    }
                } catch (IOException e14) {
                    return -1;
                }
            } catch (IOException e15) {
                return -1;
            }
        } catch (IOException e16) {
            return -1;
        }
    }

    public static float SignedVolumeOfTriangle(float x1, float y1, float z1, float x2, float y2, float z2, float x3, float y3, float z3) {
        vol321 = x3 * y2 * z1;
        vol231 = x2 * y3 * z1;
        vol312 = x3 * y1 * z2;
        vol132 = x1 * y3 * z2;
        vol213 = x2 * y1 * z3;
        vol123 = x1 * y2 * z3;
        return 0.16666667f * ((((((-vol321) + vol231) + vol312) - vol132) - vol213) + vol123);
    }
}
