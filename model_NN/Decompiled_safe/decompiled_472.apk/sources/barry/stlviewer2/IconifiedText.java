package barry.stlviewer2;

import android.graphics.drawable.Drawable;

public class IconifiedText implements Comparable<IconifiedText> {
    private Drawable mIcon;
    private boolean mSelectable = true;
    private String mText = "";

    public IconifiedText(String text, Drawable bullet) {
        this.mIcon = bullet;
        this.mText = text;
    }

    public boolean isSelectable() {
        return this.mSelectable;
    }

    public void setSelectable(boolean selectable) {
        this.mSelectable = selectable;
    }

    public String getText() {
        return this.mText;
    }

    public void setText(String text) {
        this.mText = text;
    }

    public void setIcon(Drawable icon) {
        this.mIcon = icon;
    }

    public Drawable getIcon() {
        return this.mIcon;
    }

    public int compareTo(IconifiedText other) {
        if (this.mText != null) {
            return this.mText.compareTo(other.getText());
        }
        throw new IllegalArgumentException();
    }
}
