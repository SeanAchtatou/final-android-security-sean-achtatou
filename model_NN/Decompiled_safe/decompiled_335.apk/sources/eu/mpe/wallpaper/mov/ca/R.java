package eu.mpe.wallpaper.mov.ca;

public final class R {

    public static final class attr {
        public static final int adSize = 2130771968;
        public static final int adUnitId = 2130771969;
    }

    public static final class drawable {
        public static final int ic_launcher = 2130837504;
        public static final int icon = 2130837505;
        public static final int p1 = 2130837506;
        public static final int p10 = 2130837507;
        public static final int p11 = 2130837508;
        public static final int p12 = 2130837509;
        public static final int p13 = 2130837510;
        public static final int p14 = 2130837511;
        public static final int p15 = 2130837512;
        public static final int p16 = 2130837513;
        public static final int p17 = 2130837514;
        public static final int p18 = 2130837515;
        public static final int p19 = 2130837516;
        public static final int p2 = 2130837517;
        public static final int p20 = 2130837518;
        public static final int p21 = 2130837519;
        public static final int p22 = 2130837520;
        public static final int p3 = 2130837521;
        public static final int p4 = 2130837522;
        public static final int p5 = 2130837523;
        public static final int p6 = 2130837524;
        public static final int p7 = 2130837525;
        public static final int p8 = 2130837526;
        public static final int p9 = 2130837527;
        public static final int pp1 = 2130837528;
        public static final int pp10 = 2130837529;
        public static final int pp11 = 2130837530;
        public static final int pp12 = 2130837531;
        public static final int pp13 = 2130837532;
        public static final int pp14 = 2130837533;
        public static final int pp15 = 2130837534;
        public static final int pp16 = 2130837535;
        public static final int pp17 = 2130837536;
        public static final int pp18 = 2130837537;
        public static final int pp19 = 2130837538;
        public static final int pp2 = 2130837539;
        public static final int pp20 = 2130837540;
        public static final int pp21 = 2130837541;
        public static final int pp22 = 2130837542;
        public static final int pp3 = 2130837543;
        public static final int pp4 = 2130837544;
        public static final int pp5 = 2130837545;
        public static final int pp6 = 2130837546;
        public static final int pp7 = 2130837547;
        public static final int pp8 = 2130837548;
        public static final int pp9 = 2130837549;
    }

    public static final class id {
        public static final int BANNER = 2130968576;
        public static final int IAB_BANNER = 2130968578;
        public static final int IAB_LEADERBOARD = 2130968579;
        public static final int IAB_MRECT = 2130968577;
        public static final int adView = 2130968584;
        public static final int button1 = 2130968582;
        public static final int gallery1 = 2130968583;
        public static final int preview = 2130968581;
        public static final int rlayout = 2130968580;
    }

    public static final class layout {
        public static final int main = 2130903040;
    }

    public static final class string {
        public static final int app_name = 2131034113;
        public static final int hello = 2131034112;
    }

    public static final class styleable {
        public static final int[] com_google_ads_AdView = {R.attr.adSize, R.attr.adUnitId};
        public static final int com_google_ads_AdView_adSize = 0;
        public static final int com_google_ads_AdView_adUnitId = 1;
    }
}
