package eu.mpe.wallpaper.mov.ca;

import android.app.Activity;
import android.app.ProgressDialog;
import android.app.WallpaperManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.SpinnerAdapter;
import android.widget.Toast;
import com.google.ads.AdRequest;
import com.google.ads.AdView;
import java.io.IOException;

public class Wallpaper extends Activity {
    public int accImag = 0;
    Bitmap bitmap;
    public int dh;
    public ProgressDialog dialog;
    public DisplayMetrics displaymetrics;
    public int dw;
    public ImageView imagePreview;
    int lastImageRef;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        ((AdView) findViewById(R.id.adView)).loadAd(new AdRequest());
        this.displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(this.displaymetrics);
        this.dh = this.displaymetrics.heightPixels;
        this.dw = this.displaymetrics.widthPixels;
        this.imagePreview = (ImageView) findViewById(R.id.preview);
        this.imagePreview.setImageResource(R.drawable.p1);
        Gallery gallery = (Gallery) findViewById(R.id.gallery1);
        gallery.setAdapter((SpinnerAdapter) new ImageAdapter(this));
        this.imagePreview.getLayoutParams().width = this.dw;
        this.imagePreview.getLayoutParams().height = (this.dh / 4) * 2;
        this.accImag = R.drawable.p1;
        gallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View v, int position, long id) {
                if (position == 0) {
                    Wallpaper.this.imagePreview.setImageResource(R.drawable.p1);
                    Wallpaper.this.accImag = R.drawable.p1;
                }
                if (position == 1) {
                    Wallpaper.this.imagePreview.setImageResource(R.drawable.p2);
                    Wallpaper.this.accImag = R.drawable.p2;
                }
                if (position == 2) {
                    Wallpaper.this.imagePreview.setImageResource(R.drawable.p3);
                    Wallpaper.this.accImag = R.drawable.p3;
                }
                if (position == 3) {
                    Wallpaper.this.imagePreview.setImageResource(R.drawable.p4);
                    Wallpaper.this.accImag = R.drawable.p4;
                }
                if (position == 4) {
                    Wallpaper.this.imagePreview.setImageResource(R.drawable.p5);
                    Wallpaper.this.accImag = R.drawable.p5;
                }
                if (position == 5) {
                    Wallpaper.this.imagePreview.setImageResource(R.drawable.p6);
                    Wallpaper.this.accImag = R.drawable.p6;
                }
                if (position == 6) {
                    Wallpaper.this.imagePreview.setImageResource(R.drawable.p7);
                    Wallpaper.this.accImag = R.drawable.p7;
                }
                if (position == 7) {
                    Wallpaper.this.imagePreview.setImageResource(R.drawable.p8);
                    Wallpaper.this.accImag = R.drawable.p8;
                }
                if (position == 8) {
                    Wallpaper.this.imagePreview.setImageResource(R.drawable.p9);
                    Wallpaper.this.accImag = R.drawable.p9;
                }
                if (position == 9) {
                    Wallpaper.this.imagePreview.setImageResource(R.drawable.p10);
                    Wallpaper.this.accImag = R.drawable.p10;
                }
                if (position == 10) {
                    Wallpaper.this.imagePreview.setImageResource(R.drawable.p11);
                    Wallpaper.this.accImag = R.drawable.p11;
                }
                if (position == 11) {
                    Wallpaper.this.imagePreview.setImageResource(R.drawable.p12);
                    Wallpaper.this.accImag = R.drawable.p12;
                }
                if (position == 12) {
                    Wallpaper.this.imagePreview.setImageResource(R.drawable.p13);
                    Wallpaper.this.accImag = R.drawable.p13;
                }
                if (position == 13) {
                    Wallpaper.this.imagePreview.setImageResource(R.drawable.p14);
                    Wallpaper.this.accImag = R.drawable.p14;
                }
                if (position == 14) {
                    Wallpaper.this.imagePreview.setImageResource(R.drawable.p15);
                    Wallpaper.this.accImag = R.drawable.p15;
                }
                if (position == 15) {
                    Wallpaper.this.imagePreview.setImageResource(R.drawable.p16);
                    Wallpaper.this.accImag = R.drawable.p16;
                }
                if (position == 16) {
                    Wallpaper.this.imagePreview.setImageResource(R.drawable.p17);
                    Wallpaper.this.accImag = R.drawable.p17;
                }
                if (position == 17) {
                    Wallpaper.this.imagePreview.setImageResource(R.drawable.p18);
                    Wallpaper.this.accImag = R.drawable.p18;
                }
                if (position == 18) {
                    Wallpaper.this.imagePreview.setImageResource(R.drawable.p19);
                    Wallpaper.this.accImag = R.drawable.p19;
                }
                if (position == 19) {
                    Wallpaper.this.imagePreview.setImageResource(R.drawable.p20);
                    Wallpaper.this.accImag = R.drawable.p20;
                }
                if (position == 20) {
                    Wallpaper.this.imagePreview.setImageResource(R.drawable.p21);
                    Wallpaper.this.accImag = R.drawable.p21;
                }
                if (position == 21) {
                    Wallpaper.this.imagePreview.setImageResource(R.drawable.p22);
                    Wallpaper.this.accImag = R.drawable.p22;
                }
            }
        });
        ((Button) findViewById(R.id.button1)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Wallpaper.this.SetAsWallpaper();
            }
        });
    }

    public class ImageAdapter extends BaseAdapter {
        private Context mContext;
        int mGalleryItemBackground;
        private Integer[] mImageIds = {Integer.valueOf((int) R.drawable.pp1), Integer.valueOf((int) R.drawable.pp2), Integer.valueOf((int) R.drawable.pp3), Integer.valueOf((int) R.drawable.pp4), Integer.valueOf((int) R.drawable.pp5), Integer.valueOf((int) R.drawable.pp6), Integer.valueOf((int) R.drawable.pp7), Integer.valueOf((int) R.drawable.pp8), Integer.valueOf((int) R.drawable.pp9), Integer.valueOf((int) R.drawable.pp10), Integer.valueOf((int) R.drawable.pp11), Integer.valueOf((int) R.drawable.pp12), Integer.valueOf((int) R.drawable.pp13), Integer.valueOf((int) R.drawable.pp14), Integer.valueOf((int) R.drawable.pp15), Integer.valueOf((int) R.drawable.pp16), Integer.valueOf((int) R.drawable.pp17), Integer.valueOf((int) R.drawable.pp18), Integer.valueOf((int) R.drawable.pp19), Integer.valueOf((int) R.drawable.pp20), Integer.valueOf((int) R.drawable.pp21), Integer.valueOf((int) R.drawable.pp22)};

        public ImageAdapter(Context c) {
            this.mContext = c;
        }

        public int getCount() {
            return this.mImageIds.length;
        }

        public Object getItem(int position) {
            return Integer.valueOf(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ImageView imageView = new ImageView(this.mContext);
            imageView.setImageResource(this.mImageIds[position].intValue());
            imageView.setLayoutParams(new Gallery.LayoutParams(Wallpaper.this.dw / 4, (int) (((double) (Wallpaper.this.dw / 4)) / 1.2d)));
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            imageView.setBackgroundResource(this.mGalleryItemBackground);
            return imageView;
        }
    }

    /* access modifiers changed from: private */
    public void SetAsWallpaper() {
        new DownloadFileAsync().execute(new String[0]);
    }

    class DownloadFileAsync extends AsyncTask<String, String, String> {
        DownloadFileAsync() {
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            Wallpaper.this.dialog = ProgressDialog.show(Wallpaper.this, "", "Creating wallpaper...", true);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
         arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
         candidates:
          ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
          ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
        /* access modifiers changed from: protected */
        public String doInBackground(String... aurl) {
            Bitmap bitmapOrg = BitmapFactory.decodeResource(Wallpaper.this.getResources(), Wallpaper.this.accImag);
            int width = bitmapOrg.getWidth();
            int height = bitmapOrg.getHeight();
            float scaleWidth = (((float) Wallpaper.this.dw) * 2.0f) / ((float) width);
            float scaleHeight = ((float) Wallpaper.this.dh) / ((float) height);
            Log.e("imageW", String.valueOf(width));
            Log.e("imageH", String.valueOf(height));
            Log.e("DW", String.valueOf(Wallpaper.this.dw));
            Log.e("DH", String.valueOf(Wallpaper.this.dh));
            Log.e("scaleW", String.valueOf(scaleWidth));
            Log.e("scaleH", String.valueOf(scaleHeight));
            Matrix matrix = new Matrix();
            matrix.postScale(scaleWidth, scaleHeight);
            try {
                WallpaperManager.getInstance(Wallpaper.this.getApplicationContext()).setBitmap(Bitmap.createBitmap(bitmapOrg, 0, 0, width, height, matrix, true));
                return null;
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }

        /* access modifiers changed from: protected */
        public void onProgressUpdate(String... progress) {
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String unused) {
            Wallpaper.this.dialog.dismiss();
            Toast.makeText(Wallpaper.this.getApplicationContext(), "Success", 0).show();
        }
    }
}
