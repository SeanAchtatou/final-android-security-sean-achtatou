package com.skyd.bestpuzzle;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.widget.Toast;
import com.scoreloop.client.android.core.model.Score;
import com.scoreloop.client.android.ui.ScoreloopManagerSingleton;
import com.skyd.bestpuzzle.n1622.R;
import com.skyd.core.android.game.GameImageSpirit;
import com.skyd.core.android.game.GameMaster;
import com.skyd.core.android.game.GameObject;
import com.skyd.core.android.game.GameScene;
import com.skyd.core.android.game.GameSpirit;
import com.skyd.core.draw.DrawHelper;
import com.skyd.core.vector.Vector2DF;
import com.skyd.core.vector.VectorRect2DF;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

public class PuzzleScene extends GameScene {
    public Controller Controller;
    public Desktop Desktop;
    boolean IsDrawingPuzzle;
    boolean IsDrawnSelectRect;
    public GameImageSpirit MoveButton;
    public UI MoveButtonSlot;
    public OriginalImage OriginalImage;
    public long PastTime;
    public long StartTime;
    public UI SubmitScoreButton;
    /* access modifiers changed from: private */
    public RectF SubmitScoreButtonRect;
    public GameSpirit Time;
    public UI ViewFullButton;
    /* access modifiers changed from: private */
    public RectF ViewFullButtonRect;
    public UI ZoomInButton;
    public UI ZoomOutButton;
    private boolean _IsFinished = false;
    private ArrayList<OnIsFinishedChangedListener> _IsFinishedChangedListenerList = null;
    long savetime = Long.MIN_VALUE;
    RectF selectRect;

    public interface OnIsFinishedChangedListener {
        void OnIsFinishedChangedEvent(Object obj, boolean z);
    }

    public PuzzleScene() {
        setMaxDrawCacheLayer(25.0f);
        this.StartTime = new Date().getTime();
        loadGameState();
    }

    public void loadGameState() {
        Center c = (Center) GameMaster.getContext().getApplicationContext();
        this.PastTime = c.getPastTime().longValue();
        this._IsFinished = c.getIsFinished().booleanValue();
    }

    public void saveGameState() {
        Center c = (Center) GameMaster.getContext().getApplicationContext();
        if (getIsFinished()) {
            c.setPastTime(Long.valueOf(this.PastTime));
        } else {
            c.setPastTime(Long.valueOf(this.PastTime + (new Date().getTime() - this.StartTime)));
        }
        c.setIsFinished(Boolean.valueOf(getIsFinished()));
    }

    public void setFinished() {
        if (!getIsFinished()) {
            this.SubmitScoreButton.show();
        }
        setIsFinished(true);
        this.PastTime += new Date().getTime() - this.StartTime;
    }

    public boolean getIsFinished() {
        return this._IsFinished;
    }

    private void setIsFinished(boolean value) {
        onIsFinishedChanged(value);
        this._IsFinished = value;
    }

    private void setIsFinishedToDefault() {
        setIsFinished(false);
    }

    public boolean addOnIsFinishedChangedListener(OnIsFinishedChangedListener listener) {
        if (this._IsFinishedChangedListenerList == null) {
            this._IsFinishedChangedListenerList = new ArrayList<>();
        } else if (this._IsFinishedChangedListenerList.contains(listener)) {
            return false;
        }
        this._IsFinishedChangedListenerList.add(listener);
        return true;
    }

    public boolean removeOnIsFinishedChangedListener(OnIsFinishedChangedListener listener) {
        if (this._IsFinishedChangedListenerList == null || !this._IsFinishedChangedListenerList.contains(listener)) {
            return false;
        }
        this._IsFinishedChangedListenerList.remove(listener);
        return true;
    }

    public void clearOnIsFinishedChangedListeners() {
        if (this._IsFinishedChangedListenerList != null) {
            this._IsFinishedChangedListenerList.clear();
        }
    }

    /* access modifiers changed from: protected */
    public void onIsFinishedChanged(boolean newValue) {
        if (this._IsFinishedChangedListenerList != null) {
            Iterator<OnIsFinishedChangedListener> it = this._IsFinishedChangedListenerList.iterator();
            while (it.hasNext()) {
                it.next().OnIsFinishedChangedEvent(this, newValue);
            }
        }
    }

    public int getTargetCacheID() {
        return 1;
    }

    /* access modifiers changed from: protected */
    public void drawChilds(Canvas c, Rect drawArea) {
        this.IsDrawingPuzzle = false;
        this.IsDrawnSelectRect = false;
        super.drawChilds(c, drawArea);
    }

    /* access modifiers changed from: protected */
    public boolean onDrawingChild(GameObject child, Canvas c, Rect drawArea) {
        float lvl = ((GameSpirit) child).getLevel();
        if (!this.IsDrawingPuzzle && lvl <= 10.0f) {
            this.IsDrawingPuzzle = true;
            c.setMatrix(this.Desktop.getMatrix());
        } else if (lvl > 10.0f && this.IsDrawingPuzzle) {
            this.IsDrawingPuzzle = false;
            c.setMatrix(new Matrix());
        }
        if (lvl > 25.0f && this.selectRect != null && !this.IsDrawnSelectRect) {
            this.IsDrawnSelectRect = true;
            Paint p = new Paint();
            p.setARGB(100, 180, 225, 255);
            c.drawRect(this.selectRect, p);
        }
        return super.onDrawingChild(child, c, drawArea);
    }

    public void load(Context c) {
        loadOriginalImage(c);
        loadDesktop(c);
        loadInterface(c);
        loadPuzzle(c);
        loadPuzzleState();
        this.Desktop.updateCurrentObserveAreaRectF();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.skyd.core.draw.DrawHelper.calculateScaleSize(float, float, float, float, boolean):com.skyd.core.vector.Vector2DF
     arg types: [int, int, float, float, int]
     candidates:
      com.skyd.core.draw.DrawHelper.calculateScaleSize(double, double, double, double, boolean):com.skyd.core.vector.Vector2D
      com.skyd.core.draw.DrawHelper.calculateScaleSize(float, float, float, float, boolean):com.skyd.core.vector.Vector2DF */
    public void loadOriginalImage(Context c) {
        this.OriginalImage = new OriginalImage() {
            /* access modifiers changed from: protected */
            public boolean onDrawing(Canvas c, Rect drawArea) {
                c.drawARGB(180, 0, 0, 0);
                return super.onDrawing(c, drawArea);
            }
        };
        this.OriginalImage.setLevel(50.0f);
        this.OriginalImage.setName("OriginalImage");
        this.OriginalImage.getSize().resetWith(DrawHelper.calculateScaleSize(480.0f, 240.0f, (float) (GameMaster.getScreenWidth() - 40), (float) (GameMaster.getScreenHeight() - 40), true));
        this.OriginalImage.setTotalSeparateColumn(12);
        this.OriginalImage.setTotalSeparateRow(6);
        this.OriginalImage.getOriginalSize().reset(1333.0f, 666.0f);
        this.OriginalImage.hide();
        this.OriginalImage.setIsUseAbsolutePosition(true);
        this.OriginalImage.setIsUseAbsoluteSize(true);
        this.OriginalImage.getImage().setIsUseAbsolutePosition(true);
        this.OriginalImage.getImage().setIsUseAbsoluteSize(true);
        this.OriginalImage.getPosition().reset(((float) (GameMaster.getScreenWidth() / 2)) - (this.OriginalImage.getSize().getX() / 2.0f), ((float) (GameMaster.getScreenHeight() / 2)) - (this.OriginalImage.getSize().getY() / 2.0f));
        getSpiritList().add(this.OriginalImage);
    }

    public void loadDesktop(Context c) {
        this.Desktop = new Desktop();
        this.Desktop.setLevel(-9999.0f);
        this.Desktop.setName("Desktop");
        this.Desktop.getSize().reset(2666.0f, 1332.0f);
        this.Desktop.getCurrentObservePosition().reset(1333.0f, 666.0f);
        this.Desktop.setOriginalImage(this.OriginalImage);
        this.Desktop.setIsUseAbsolutePosition(true);
        this.Desktop.setIsUseAbsoluteSize(true);
        this.Desktop.ColorAPaint = new Paint();
        this.Desktop.ColorAPaint.setColor(Color.argb(255, 58, 54, 37));
        this.Desktop.ColorBPaint = new Paint();
        this.Desktop.ColorBPaint.setColor(Color.argb(255, 52, 48, 31));
        getSpiritList().add(this.Desktop);
    }

    public void loadPuzzle(Context c) {
        this.Controller = new Controller();
        this.Controller.setLevel(10.0f);
        this.Controller.setName("Controller");
        this.Controller.setDesktop(this.Desktop);
        this.Controller.setIsUseAbsolutePosition(true);
        this.Controller.setIsUseAbsoluteSize(true);
        getSpiritList().add(this.Controller);
        Puzzle.getRealitySize().reset(111.0833f, 111.0f);
        Puzzle.getMaxRadius().reset(85.14167f, 85.1f);
        c2017383204(c);
        c1793810707(c);
        c39635291(c);
        c146079436(c);
        c704674678(c);
        c1029183600(c);
        c1229511719(c);
        c2010462150(c);
        c1439712182(c);
        c1535984674(c);
        c121050773(c);
        c1792926864(c);
        c1114616165(c);
        c1629257215(c);
        c1504832738(c);
        c169466133(c);
        c1550625862(c);
        c1659168475(c);
        c1746440748(c);
        c1962491871(c);
        c1918314562(c);
        c1028647396(c);
        c480267172(c);
        c2063534525(c);
        c1806710122(c);
        c233452626(c);
        c1578016639(c);
        c2111421574(c);
        c1011078854(c);
        c440604811(c);
        c707993294(c);
        c1365809361(c);
        c1086466190(c);
        c677124579(c);
        c1976495085(c);
        c1337293698(c);
        c1809581665(c);
        c1544933686(c);
        c1363659385(c);
        c126487040(c);
        c1461794885(c);
        c1794224488(c);
        c1278619394(c);
        c305531379(c);
        c1965247119(c);
        c704666089(c);
        c889907541(c);
        c996557555(c);
        c405078516(c);
        c1092698069(c);
        c1584592799(c);
        c1401309947(c);
        c1967921646(c);
        c1770746245(c);
        c1199201345(c);
        c1721237390(c);
        c1438994516(c);
        c1924795414(c);
        c1527842742(c);
        c735389401(c);
        c556392346(c);
        c1044904642(c);
        c1390734376(c);
        c776822006(c);
        c666701507(c);
        c537266955(c);
        c1051015167(c);
        c1793640042(c);
        c1558039989(c);
        c269806753(c);
        c1870290361(c);
        c1240996084(c);
    }

    public void loadInterface(Context c) {
        this.MoveButton = new GameImageSpirit();
        this.MoveButton.setLevel(22.0f);
        this.MoveButton.getImage().loadImageFromResource(c, R.drawable.movebutton);
        this.MoveButton.getSize().reset(155.0f, 155.0f);
        this.MoveButton.setIsUseAbsolutePosition(true);
        getSpiritList().add(this.MoveButton);
        final Vector2DF mvds = this.MoveButton.getDisplaySize();
        mvds.scale(0.5f);
        this.MoveButton.getPosition().reset(5.0f + mvds.getX(), ((float) (GameMaster.getScreenHeight() - 5)) - mvds.getY());
        this.MoveButton.getPositionOffset().resetWith(mvds.negateNew());
        this.MoveButtonSlot = new UI() {
            Vector2DF movevector;
            boolean needmove;

            public void executive(Vector2DF point) {
                this.movevector = point.minusNew(getPosition()).restrainLength(mvds.getX() * 0.4f);
                this.needmove = true;
            }

            /* access modifiers changed from: protected */
            public void updateSelf() {
                if (this.needmove) {
                    PuzzleScene.this.MoveButton.getPosition().resetWith(getPosition().plusNew(this.movevector));
                    PuzzleScene.this.Desktop.getCurrentObservePosition().plus(this.movevector);
                    PuzzleScene.this.refreshDrawCacheBitmap();
                }
                super.updateSelf();
            }

            public boolean isInArea(Vector2DF point) {
                return point.minusNew(getPosition()).getLength() < mvds.getX() + 5.0f;
            }

            public void reset() {
                this.needmove = false;
                PuzzleScene.this.MoveButton.getPosition().resetWith(getPosition());
                PuzzleScene.this.refreshDrawCacheBitmap();
            }
        };
        this.MoveButtonSlot.setLevel(21.0f);
        this.MoveButtonSlot.getImage().loadImageFromResource(c, R.drawable.movebuttonslot);
        this.MoveButtonSlot.getSize().reset(155.0f, 155.0f);
        this.MoveButtonSlot.setIsUseAbsolutePosition(true);
        getSpiritList().add(this.MoveButtonSlot);
        this.MoveButtonSlot.getPosition().resetWith(this.MoveButton.getPosition());
        this.MoveButtonSlot.getPositionOffset().resetWith(this.MoveButton.getPositionOffset());
        this.ZoomInButton = new UI() {
            boolean zoom;

            public void executive(Vector2DF point) {
                this.zoom = true;
            }

            public boolean isInArea(Vector2DF point) {
                return point.minusNew(getPosition()).getLength() < getDisplaySize().getX() / 2.0f;
            }

            public void reset() {
                this.zoom = false;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{java.lang.Math.min(float, float):float}
             arg types: [int, float]
             candidates:
              ClspMth{java.lang.Math.min(double, double):double}
              ClspMth{java.lang.Math.min(long, long):long}
              ClspMth{java.lang.Math.min(int, int):int}
              ClspMth{java.lang.Math.min(float, float):float} */
            /* access modifiers changed from: protected */
            public void updateSelf() {
                if (this.zoom) {
                    PuzzleScene.this.Desktop.setZoom(Math.min(1.3f, PuzzleScene.this.Desktop.getZoom() * 1.05f));
                    PuzzleScene.this.refreshDrawCacheBitmap();
                }
                super.updateSelf();
            }
        };
        this.ZoomInButton.setLevel(21.0f);
        this.ZoomInButton.getImage().loadImageFromResource(c, R.drawable.zoombutton1);
        this.ZoomInButton.getSize().reset(77.0f, 77.0f);
        this.ZoomInButton.setIsUseAbsolutePosition(true);
        getSpiritList().add(this.ZoomInButton);
        Vector2DF zibds = this.ZoomInButton.getDisplaySize().scale(0.5f);
        this.ZoomInButton.getPosition().reset(((float) (GameMaster.getScreenWidth() - 50)) - zibds.getX(), ((float) (GameMaster.getScreenHeight() - 10)) - zibds.getY());
        this.ZoomInButton.getPositionOffset().resetWith(zibds.negateNew());
        this.ZoomOutButton = new UI() {
            boolean zoom;

            public void executive(Vector2DF point) {
                this.zoom = true;
            }

            public boolean isInArea(Vector2DF point) {
                return point.minusNew(getPosition()).getLength() < getDisplaySize().getX() / 2.0f;
            }

            public void reset() {
                this.zoom = false;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{java.lang.Math.max(float, float):float}
             arg types: [int, float]
             candidates:
              ClspMth{java.lang.Math.max(double, double):double}
              ClspMth{java.lang.Math.max(int, int):int}
              ClspMth{java.lang.Math.max(long, long):long}
              ClspMth{java.lang.Math.max(float, float):float} */
            /* access modifiers changed from: protected */
            public void updateSelf() {
                if (this.zoom) {
                    PuzzleScene.this.Desktop.setZoom(Math.max(0.3f, PuzzleScene.this.Desktop.getZoom() * 0.95f));
                    PuzzleScene.this.refreshDrawCacheBitmap();
                    GameMaster.log(this, Float.valueOf(PuzzleScene.this.Desktop.getCurrentObserveAreaRectF().width()));
                }
                super.updateSelf();
            }
        };
        this.ZoomOutButton.setLevel(21.0f);
        this.ZoomOutButton.getImage().loadImageFromResource(c, R.drawable.zoombutton2);
        this.ZoomOutButton.getSize().reset(77.0f, 77.0f);
        this.ZoomOutButton.setIsUseAbsolutePosition(true);
        getSpiritList().add(this.ZoomOutButton);
        Vector2DF zobds = this.ZoomOutButton.getDisplaySize().scale(0.5f);
        this.ZoomOutButton.getPosition().reset((((float) (GameMaster.getScreenWidth() - 100)) - zobds.getX()) - (zibds.getX() * 2.0f), ((float) (GameMaster.getScreenHeight() - 10)) - zobds.getY());
        this.ZoomOutButton.getPositionOffset().resetWith(zobds.negateNew());
        this.ViewFullButton = new UI() {
            public void executive(Vector2DF point) {
                if (PuzzleScene.this.OriginalImage.getImage().getImage() == null) {
                    PuzzleScene.this.OriginalImage.getImage().loadImageFromResource(GameMaster.getContext(), R.drawable.oim);
                }
                PuzzleScene.this.OriginalImage.show();
            }

            public boolean isInArea(Vector2DF point) {
                return point.isIn(PuzzleScene.this.ViewFullButtonRect);
            }

            public void reset() {
                PuzzleScene.this.OriginalImage.hide();
            }
        };
        this.ViewFullButton.getImage().loadImageFromResource(c, R.drawable.viewfull);
        this.ViewFullButton.setLevel(21.0f);
        this.ViewFullButton.setIsUseAbsolutePosition(true);
        this.ViewFullButton.getSize().reset(83.0f, 38.0f);
        getSpiritList().add(this.ViewFullButton);
        this.ViewFullButton.getPosition().reset(((float) (GameMaster.getScreenWidth() - 15)) - this.ViewFullButton.getDisplaySize().getX(), 15.0f);
        this.ViewFullButtonRect = new VectorRect2DF(this.ViewFullButton.getPosition(), this.ViewFullButton.getDisplaySize()).getRectF();
        this.SubmitScoreButton = new UI() {
            public void executive(Vector2DF point) {
                Score score = new Score(Double.valueOf(Math.ceil((double) (PuzzleScene.this.PastTime / 1000))), null);
                score.setMode(34);
                ScoreloopManagerSingleton.get().onGamePlayEnded(score);
                hide();
                Toast.makeText(GameMaster.getContext(), (int) R.string.Submitting, 1).show();
            }

            public boolean isInArea(Vector2DF point) {
                return getVisibleOriginalValue() && point.isIn(PuzzleScene.this.SubmitScoreButtonRect);
            }

            public void reset() {
            }
        };
        this.SubmitScoreButton.getImage().loadImageFromResource(c, R.drawable.submitscore);
        this.SubmitScoreButton.setLevel(21.0f);
        this.SubmitScoreButton.setIsUseAbsolutePosition(true);
        this.SubmitScoreButton.getSize().reset(114.0f, 40.0f);
        getSpiritList().add(this.SubmitScoreButton);
        this.SubmitScoreButton.getPosition().reset(15.0f, 60.0f);
        this.SubmitScoreButtonRect = new VectorRect2DF(this.SubmitScoreButton.getPosition(), this.SubmitScoreButton.getDisplaySize()).getRectF();
        this.SubmitScoreButton.hide();
        this.Time = new GameSpirit() {
            DecimalFormat FMT = new DecimalFormat("00");
            Paint p1;
            Paint p2;

            public GameObject getDisplayContentChild() {
                return null;
            }

            /* access modifiers changed from: protected */
            public void drawSelf(Canvas c, Rect drawArea) {
                if (this.p1 == null) {
                    this.p1 = new Paint();
                    this.p1.setARGB(160, 255, 255, 255);
                    this.p1.setAntiAlias(true);
                    this.p1.setTextSize(28.0f);
                    this.p1.setTypeface(Typeface.createFromAsset(GameMaster.getContext().getAssets(), "fonts/font.ttf"));
                }
                if (this.p2 == null) {
                    this.p2 = new Paint();
                    this.p2.setARGB(100, 0, 0, 0);
                    this.p2.setAntiAlias(true);
                    this.p2.setTextSize(28.0f);
                    this.p2.setTypeface(Typeface.createFromAsset(GameMaster.getContext().getAssets(), "fonts/font.ttf"));
                }
                long t = PuzzleScene.this.getIsFinished() ? PuzzleScene.this.PastTime : PuzzleScene.this.PastTime + (new Date().getTime() - PuzzleScene.this.StartTime);
                long h = t / 3600000;
                long m = (t - (((1000 * h) * 60) * 60)) / 60000;
                String str = String.valueOf(this.FMT.format(h)) + ":" + this.FMT.format(m) + ":" + this.FMT.format(((t - (((1000 * h) * 60) * 60)) - ((1000 * m) * 60)) / 1000) + (PuzzleScene.this.getIsFinished() ? " Finished" : "");
                c.drawText(str, 16.0f, 44.0f, this.p2);
                c.drawText(str, 15.0f, 43.0f, this.p1);
                super.drawSelf(c, drawArea);
            }
        };
        this.Time.setLevel(40.0f);
        getSpiritList().add(this.Time);
    }

    public float getMaxPuzzleLevel() {
        float maxlvl = 0.0f;
        Iterator<Puzzle> it = this.Desktop.getPuzzleList().iterator();
        while (it.hasNext()) {
            Puzzle f = it.next();
            if (f.getLevel() <= 10.0f) {
                maxlvl = Math.max(f.getLevel(), maxlvl);
            }
        }
        return maxlvl;
    }

    public Puzzle getTouchPuzzle(Vector2DF v) {
        Vector2DF rv = this.Desktop.reMapPoint(v);
        Puzzle o = null;
        float olen = 99999.0f;
        for (int i = getSpiritList().size() - 1; i >= 0; i--) {
            if (getSpiritList().get(i) instanceof Puzzle) {
                Puzzle p = (Puzzle) getSpiritList().get(i);
                float len = p.getPositionInDesktop().minusNew(rv).getLength();
                if (len <= Puzzle.getRealitySize().getX() * 0.4f) {
                    return p;
                }
                if (len <= Puzzle.getRealitySize().getX() && olen > len) {
                    o = p;
                    olen = len;
                }
            }
        }
        return o;
    }

    public void startDrawSelectRect(Vector2DF startDragPoint, Vector2DF v) {
        this.selectRect = new VectorRect2DF(startDragPoint, v.minusNew(startDragPoint)).getFixedRectF();
    }

    public void stopDrawSelectRect() {
        this.selectRect = null;
    }

    public ArrayList<Puzzle> getSelectPuzzle(Vector2DF startDragPoint, Vector2DF v) {
        Vector2DF rs = this.Desktop.reMapPoint(startDragPoint);
        ArrayList<Puzzle> l = new ArrayList<>();
        RectF rect = new VectorRect2DF(rs, this.Desktop.reMapPoint(v).minusNew(rs)).getFixedRectF();
        float xo = Puzzle.getRealitySize().getX() / 3.0f;
        float yo = Puzzle.getRealitySize().getY() / 3.0f;
        RectF sr = new RectF(rect.left - xo, rect.top - yo, rect.right + xo, rect.bottom + yo);
        Iterator<Puzzle> it = this.Desktop.getPuzzleList().iterator();
        while (it.hasNext()) {
            Puzzle f = it.next();
            if (f.getPositionInDesktop().isIn(sr)) {
                l.add(f);
            }
        }
        return l;
    }

    public void loadPuzzleState() {
        Center c = (Center) GameMaster.getContext().getApplicationContext();
        SharedPreferences s = c.getSharedPreferences();
        Iterator<Puzzle> it = this.Desktop.getPuzzleList().iterator();
        while (it.hasNext()) {
            it.next().load(c, s);
        }
    }

    public void savePuzzleState() {
        Center c = (Center) GameMaster.getContext().getApplicationContext();
        SharedPreferences.Editor e = c.getSharedPreferences().edit();
        Iterator<Puzzle> it = this.Desktop.getPuzzleList().iterator();
        while (it.hasNext()) {
            it.next().save(c, e);
        }
        e.commit();
    }

    public void reset() {
        this.Controller.reset();
        Iterator<Puzzle> it = this.Desktop.getPuzzleList().iterator();
        while (it.hasNext()) {
            Puzzle f = it.next();
            f.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
            this.Desktop.RandomlyPlaced(f);
        }
        refreshDrawCacheBitmap();
        setIsFinishedToDefault();
        this.StartTime = new Date().getTime();
        this.PastTime = 0;
        this.SubmitScoreButton.hide();
        saveGameState();
        savePuzzleState();
    }

    /* access modifiers changed from: package-private */
    public void c2017383204(Context c) {
        Puzzle p2017383204 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load2017383204(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save2017383204(editor, this);
            }
        };
        p2017383204.setID(2017383204);
        p2017383204.setName("2017383204");
        p2017383204.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p2017383204);
        this.Desktop.RandomlyPlaced(p2017383204);
        p2017383204.setTopEdgeType(EdgeType.Flat);
        p2017383204.setBottomEdgeType(EdgeType.Concave);
        p2017383204.setLeftEdgeType(EdgeType.Flat);
        p2017383204.setRightEdgeType(EdgeType.Concave);
        p2017383204.setTopExactPuzzleID(-1);
        p2017383204.setBottomExactPuzzleID(1793810707);
        p2017383204.setLeftExactPuzzleID(-1);
        p2017383204.setRightExactPuzzleID(1229511719);
        p2017383204.getDisplayImage().loadImageFromResource(c, R.drawable.p2017383204h);
        p2017383204.setExactRow(0);
        p2017383204.setExactColumn(0);
        p2017383204.getSize().reset(111.0833f, 111.0f);
        p2017383204.getPositionOffset().reset(-55.54167f, -55.5f);
        p2017383204.setIsUseAbsolutePosition(true);
        p2017383204.setIsUseAbsoluteSize(true);
        p2017383204.getImage().setIsUseAbsolutePosition(true);
        p2017383204.getImage().setIsUseAbsoluteSize(true);
        p2017383204.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p2017383204.resetPosition();
        getSpiritList().add(p2017383204);
    }

    /* access modifiers changed from: package-private */
    public void c1793810707(Context c) {
        Puzzle p1793810707 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1793810707(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1793810707(editor, this);
            }
        };
        p1793810707.setID(1793810707);
        p1793810707.setName("1793810707");
        p1793810707.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1793810707);
        this.Desktop.RandomlyPlaced(p1793810707);
        p1793810707.setTopEdgeType(EdgeType.Convex);
        p1793810707.setBottomEdgeType(EdgeType.Concave);
        p1793810707.setLeftEdgeType(EdgeType.Flat);
        p1793810707.setRightEdgeType(EdgeType.Convex);
        p1793810707.setTopExactPuzzleID(2017383204);
        p1793810707.setBottomExactPuzzleID(39635291);
        p1793810707.setLeftExactPuzzleID(-1);
        p1793810707.setRightExactPuzzleID(2010462150);
        p1793810707.getDisplayImage().loadImageFromResource(c, R.drawable.p1793810707h);
        p1793810707.setExactRow(1);
        p1793810707.setExactColumn(0);
        p1793810707.getSize().reset(140.6833f, 140.6f);
        p1793810707.getPositionOffset().reset(-55.54167f, -85.1f);
        p1793810707.setIsUseAbsolutePosition(true);
        p1793810707.setIsUseAbsoluteSize(true);
        p1793810707.getImage().setIsUseAbsolutePosition(true);
        p1793810707.getImage().setIsUseAbsoluteSize(true);
        p1793810707.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1793810707.resetPosition();
        getSpiritList().add(p1793810707);
    }

    /* access modifiers changed from: package-private */
    public void c39635291(Context c) {
        Puzzle p39635291 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load39635291(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save39635291(editor, this);
            }
        };
        p39635291.setID(39635291);
        p39635291.setName("39635291");
        p39635291.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p39635291);
        this.Desktop.RandomlyPlaced(p39635291);
        p39635291.setTopEdgeType(EdgeType.Convex);
        p39635291.setBottomEdgeType(EdgeType.Convex);
        p39635291.setLeftEdgeType(EdgeType.Flat);
        p39635291.setRightEdgeType(EdgeType.Convex);
        p39635291.setTopExactPuzzleID(1793810707);
        p39635291.setBottomExactPuzzleID(146079436);
        p39635291.setLeftExactPuzzleID(-1);
        p39635291.setRightExactPuzzleID(1439712182);
        p39635291.getDisplayImage().loadImageFromResource(c, R.drawable.p39635291h);
        p39635291.setExactRow(2);
        p39635291.setExactColumn(0);
        p39635291.getSize().reset(140.6833f, 170.2f);
        p39635291.getPositionOffset().reset(-55.54167f, -85.1f);
        p39635291.setIsUseAbsolutePosition(true);
        p39635291.setIsUseAbsoluteSize(true);
        p39635291.getImage().setIsUseAbsolutePosition(true);
        p39635291.getImage().setIsUseAbsoluteSize(true);
        p39635291.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p39635291.resetPosition();
        getSpiritList().add(p39635291);
    }

    /* access modifiers changed from: package-private */
    public void c146079436(Context c) {
        Puzzle p146079436 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load146079436(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save146079436(editor, this);
            }
        };
        p146079436.setID(146079436);
        p146079436.setName("146079436");
        p146079436.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p146079436);
        this.Desktop.RandomlyPlaced(p146079436);
        p146079436.setTopEdgeType(EdgeType.Concave);
        p146079436.setBottomEdgeType(EdgeType.Convex);
        p146079436.setLeftEdgeType(EdgeType.Flat);
        p146079436.setRightEdgeType(EdgeType.Convex);
        p146079436.setTopExactPuzzleID(39635291);
        p146079436.setBottomExactPuzzleID(704674678);
        p146079436.setLeftExactPuzzleID(-1);
        p146079436.setRightExactPuzzleID(1535984674);
        p146079436.getDisplayImage().loadImageFromResource(c, R.drawable.p146079436h);
        p146079436.setExactRow(3);
        p146079436.setExactColumn(0);
        p146079436.getSize().reset(140.6833f, 140.6f);
        p146079436.getPositionOffset().reset(-55.54167f, -55.5f);
        p146079436.setIsUseAbsolutePosition(true);
        p146079436.setIsUseAbsoluteSize(true);
        p146079436.getImage().setIsUseAbsolutePosition(true);
        p146079436.getImage().setIsUseAbsoluteSize(true);
        p146079436.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p146079436.resetPosition();
        getSpiritList().add(p146079436);
    }

    /* access modifiers changed from: package-private */
    public void c704674678(Context c) {
        Puzzle p704674678 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load704674678(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save704674678(editor, this);
            }
        };
        p704674678.setID(704674678);
        p704674678.setName("704674678");
        p704674678.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p704674678);
        this.Desktop.RandomlyPlaced(p704674678);
        p704674678.setTopEdgeType(EdgeType.Concave);
        p704674678.setBottomEdgeType(EdgeType.Concave);
        p704674678.setLeftEdgeType(EdgeType.Flat);
        p704674678.setRightEdgeType(EdgeType.Concave);
        p704674678.setTopExactPuzzleID(146079436);
        p704674678.setBottomExactPuzzleID(1029183600);
        p704674678.setLeftExactPuzzleID(-1);
        p704674678.setRightExactPuzzleID(121050773);
        p704674678.getDisplayImage().loadImageFromResource(c, R.drawable.p704674678h);
        p704674678.setExactRow(4);
        p704674678.setExactColumn(0);
        p704674678.getSize().reset(111.0833f, 111.0f);
        p704674678.getPositionOffset().reset(-55.54167f, -55.5f);
        p704674678.setIsUseAbsolutePosition(true);
        p704674678.setIsUseAbsoluteSize(true);
        p704674678.getImage().setIsUseAbsolutePosition(true);
        p704674678.getImage().setIsUseAbsoluteSize(true);
        p704674678.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p704674678.resetPosition();
        getSpiritList().add(p704674678);
    }

    /* access modifiers changed from: package-private */
    public void c1029183600(Context c) {
        Puzzle p1029183600 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1029183600(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1029183600(editor, this);
            }
        };
        p1029183600.setID(1029183600);
        p1029183600.setName("1029183600");
        p1029183600.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1029183600);
        this.Desktop.RandomlyPlaced(p1029183600);
        p1029183600.setTopEdgeType(EdgeType.Convex);
        p1029183600.setBottomEdgeType(EdgeType.Flat);
        p1029183600.setLeftEdgeType(EdgeType.Flat);
        p1029183600.setRightEdgeType(EdgeType.Convex);
        p1029183600.setTopExactPuzzleID(704674678);
        p1029183600.setBottomExactPuzzleID(-1);
        p1029183600.setLeftExactPuzzleID(-1);
        p1029183600.setRightExactPuzzleID(1792926864);
        p1029183600.getDisplayImage().loadImageFromResource(c, R.drawable.p1029183600h);
        p1029183600.setExactRow(5);
        p1029183600.setExactColumn(0);
        p1029183600.getSize().reset(140.6833f, 140.6f);
        p1029183600.getPositionOffset().reset(-55.54167f, -85.1f);
        p1029183600.setIsUseAbsolutePosition(true);
        p1029183600.setIsUseAbsoluteSize(true);
        p1029183600.getImage().setIsUseAbsolutePosition(true);
        p1029183600.getImage().setIsUseAbsoluteSize(true);
        p1029183600.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1029183600.resetPosition();
        getSpiritList().add(p1029183600);
    }

    /* access modifiers changed from: package-private */
    public void c1229511719(Context c) {
        Puzzle p1229511719 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1229511719(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1229511719(editor, this);
            }
        };
        p1229511719.setID(1229511719);
        p1229511719.setName("1229511719");
        p1229511719.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1229511719);
        this.Desktop.RandomlyPlaced(p1229511719);
        p1229511719.setTopEdgeType(EdgeType.Flat);
        p1229511719.setBottomEdgeType(EdgeType.Concave);
        p1229511719.setLeftEdgeType(EdgeType.Convex);
        p1229511719.setRightEdgeType(EdgeType.Concave);
        p1229511719.setTopExactPuzzleID(-1);
        p1229511719.setBottomExactPuzzleID(2010462150);
        p1229511719.setLeftExactPuzzleID(2017383204);
        p1229511719.setRightExactPuzzleID(1114616165);
        p1229511719.getDisplayImage().loadImageFromResource(c, R.drawable.p1229511719h);
        p1229511719.setExactRow(0);
        p1229511719.setExactColumn(1);
        p1229511719.getSize().reset(140.6833f, 111.0f);
        p1229511719.getPositionOffset().reset(-85.14167f, -55.5f);
        p1229511719.setIsUseAbsolutePosition(true);
        p1229511719.setIsUseAbsoluteSize(true);
        p1229511719.getImage().setIsUseAbsolutePosition(true);
        p1229511719.getImage().setIsUseAbsoluteSize(true);
        p1229511719.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1229511719.resetPosition();
        getSpiritList().add(p1229511719);
    }

    /* access modifiers changed from: package-private */
    public void c2010462150(Context c) {
        Puzzle p2010462150 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load2010462150(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save2010462150(editor, this);
            }
        };
        p2010462150.setID(2010462150);
        p2010462150.setName("2010462150");
        p2010462150.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p2010462150);
        this.Desktop.RandomlyPlaced(p2010462150);
        p2010462150.setTopEdgeType(EdgeType.Convex);
        p2010462150.setBottomEdgeType(EdgeType.Convex);
        p2010462150.setLeftEdgeType(EdgeType.Concave);
        p2010462150.setRightEdgeType(EdgeType.Convex);
        p2010462150.setTopExactPuzzleID(1229511719);
        p2010462150.setBottomExactPuzzleID(1439712182);
        p2010462150.setLeftExactPuzzleID(1793810707);
        p2010462150.setRightExactPuzzleID(1629257215);
        p2010462150.getDisplayImage().loadImageFromResource(c, R.drawable.p2010462150h);
        p2010462150.setExactRow(1);
        p2010462150.setExactColumn(1);
        p2010462150.getSize().reset(140.6833f, 170.2f);
        p2010462150.getPositionOffset().reset(-55.54167f, -85.1f);
        p2010462150.setIsUseAbsolutePosition(true);
        p2010462150.setIsUseAbsoluteSize(true);
        p2010462150.getImage().setIsUseAbsolutePosition(true);
        p2010462150.getImage().setIsUseAbsoluteSize(true);
        p2010462150.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p2010462150.resetPosition();
        getSpiritList().add(p2010462150);
    }

    /* access modifiers changed from: package-private */
    public void c1439712182(Context c) {
        Puzzle p1439712182 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1439712182(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1439712182(editor, this);
            }
        };
        p1439712182.setID(1439712182);
        p1439712182.setName("1439712182");
        p1439712182.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1439712182);
        this.Desktop.RandomlyPlaced(p1439712182);
        p1439712182.setTopEdgeType(EdgeType.Concave);
        p1439712182.setBottomEdgeType(EdgeType.Convex);
        p1439712182.setLeftEdgeType(EdgeType.Concave);
        p1439712182.setRightEdgeType(EdgeType.Convex);
        p1439712182.setTopExactPuzzleID(2010462150);
        p1439712182.setBottomExactPuzzleID(1535984674);
        p1439712182.setLeftExactPuzzleID(39635291);
        p1439712182.setRightExactPuzzleID(1504832738);
        p1439712182.getDisplayImage().loadImageFromResource(c, R.drawable.p1439712182h);
        p1439712182.setExactRow(2);
        p1439712182.setExactColumn(1);
        p1439712182.getSize().reset(140.6833f, 140.6f);
        p1439712182.getPositionOffset().reset(-55.54167f, -55.5f);
        p1439712182.setIsUseAbsolutePosition(true);
        p1439712182.setIsUseAbsoluteSize(true);
        p1439712182.getImage().setIsUseAbsolutePosition(true);
        p1439712182.getImage().setIsUseAbsoluteSize(true);
        p1439712182.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1439712182.resetPosition();
        getSpiritList().add(p1439712182);
    }

    /* access modifiers changed from: package-private */
    public void c1535984674(Context c) {
        Puzzle p1535984674 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1535984674(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1535984674(editor, this);
            }
        };
        p1535984674.setID(1535984674);
        p1535984674.setName("1535984674");
        p1535984674.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1535984674);
        this.Desktop.RandomlyPlaced(p1535984674);
        p1535984674.setTopEdgeType(EdgeType.Concave);
        p1535984674.setBottomEdgeType(EdgeType.Convex);
        p1535984674.setLeftEdgeType(EdgeType.Concave);
        p1535984674.setRightEdgeType(EdgeType.Convex);
        p1535984674.setTopExactPuzzleID(1439712182);
        p1535984674.setBottomExactPuzzleID(121050773);
        p1535984674.setLeftExactPuzzleID(146079436);
        p1535984674.setRightExactPuzzleID(169466133);
        p1535984674.getDisplayImage().loadImageFromResource(c, R.drawable.p1535984674h);
        p1535984674.setExactRow(3);
        p1535984674.setExactColumn(1);
        p1535984674.getSize().reset(140.6833f, 140.6f);
        p1535984674.getPositionOffset().reset(-55.54167f, -55.5f);
        p1535984674.setIsUseAbsolutePosition(true);
        p1535984674.setIsUseAbsoluteSize(true);
        p1535984674.getImage().setIsUseAbsolutePosition(true);
        p1535984674.getImage().setIsUseAbsoluteSize(true);
        p1535984674.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1535984674.resetPosition();
        getSpiritList().add(p1535984674);
    }

    /* access modifiers changed from: package-private */
    public void c121050773(Context c) {
        Puzzle p121050773 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load121050773(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save121050773(editor, this);
            }
        };
        p121050773.setID(121050773);
        p121050773.setName("121050773");
        p121050773.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p121050773);
        this.Desktop.RandomlyPlaced(p121050773);
        p121050773.setTopEdgeType(EdgeType.Concave);
        p121050773.setBottomEdgeType(EdgeType.Convex);
        p121050773.setLeftEdgeType(EdgeType.Convex);
        p121050773.setRightEdgeType(EdgeType.Concave);
        p121050773.setTopExactPuzzleID(1535984674);
        p121050773.setBottomExactPuzzleID(1792926864);
        p121050773.setLeftExactPuzzleID(704674678);
        p121050773.setRightExactPuzzleID(1550625862);
        p121050773.getDisplayImage().loadImageFromResource(c, R.drawable.p121050773h);
        p121050773.setExactRow(4);
        p121050773.setExactColumn(1);
        p121050773.getSize().reset(140.6833f, 140.6f);
        p121050773.getPositionOffset().reset(-85.14167f, -55.5f);
        p121050773.setIsUseAbsolutePosition(true);
        p121050773.setIsUseAbsoluteSize(true);
        p121050773.getImage().setIsUseAbsolutePosition(true);
        p121050773.getImage().setIsUseAbsoluteSize(true);
        p121050773.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p121050773.resetPosition();
        getSpiritList().add(p121050773);
    }

    /* access modifiers changed from: package-private */
    public void c1792926864(Context c) {
        Puzzle p1792926864 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1792926864(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1792926864(editor, this);
            }
        };
        p1792926864.setID(1792926864);
        p1792926864.setName("1792926864");
        p1792926864.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1792926864);
        this.Desktop.RandomlyPlaced(p1792926864);
        p1792926864.setTopEdgeType(EdgeType.Concave);
        p1792926864.setBottomEdgeType(EdgeType.Flat);
        p1792926864.setLeftEdgeType(EdgeType.Concave);
        p1792926864.setRightEdgeType(EdgeType.Convex);
        p1792926864.setTopExactPuzzleID(121050773);
        p1792926864.setBottomExactPuzzleID(-1);
        p1792926864.setLeftExactPuzzleID(1029183600);
        p1792926864.setRightExactPuzzleID(1659168475);
        p1792926864.getDisplayImage().loadImageFromResource(c, R.drawable.p1792926864h);
        p1792926864.setExactRow(5);
        p1792926864.setExactColumn(1);
        p1792926864.getSize().reset(140.6833f, 111.0f);
        p1792926864.getPositionOffset().reset(-55.54167f, -55.5f);
        p1792926864.setIsUseAbsolutePosition(true);
        p1792926864.setIsUseAbsoluteSize(true);
        p1792926864.getImage().setIsUseAbsolutePosition(true);
        p1792926864.getImage().setIsUseAbsoluteSize(true);
        p1792926864.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1792926864.resetPosition();
        getSpiritList().add(p1792926864);
    }

    /* access modifiers changed from: package-private */
    public void c1114616165(Context c) {
        Puzzle p1114616165 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1114616165(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1114616165(editor, this);
            }
        };
        p1114616165.setID(1114616165);
        p1114616165.setName("1114616165");
        p1114616165.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1114616165);
        this.Desktop.RandomlyPlaced(p1114616165);
        p1114616165.setTopEdgeType(EdgeType.Flat);
        p1114616165.setBottomEdgeType(EdgeType.Concave);
        p1114616165.setLeftEdgeType(EdgeType.Convex);
        p1114616165.setRightEdgeType(EdgeType.Concave);
        p1114616165.setTopExactPuzzleID(-1);
        p1114616165.setBottomExactPuzzleID(1629257215);
        p1114616165.setLeftExactPuzzleID(1229511719);
        p1114616165.setRightExactPuzzleID(1746440748);
        p1114616165.getDisplayImage().loadImageFromResource(c, R.drawable.p1114616165h);
        p1114616165.setExactRow(0);
        p1114616165.setExactColumn(2);
        p1114616165.getSize().reset(140.6833f, 111.0f);
        p1114616165.getPositionOffset().reset(-85.14167f, -55.5f);
        p1114616165.setIsUseAbsolutePosition(true);
        p1114616165.setIsUseAbsoluteSize(true);
        p1114616165.getImage().setIsUseAbsolutePosition(true);
        p1114616165.getImage().setIsUseAbsoluteSize(true);
        p1114616165.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1114616165.resetPosition();
        getSpiritList().add(p1114616165);
    }

    /* access modifiers changed from: package-private */
    public void c1629257215(Context c) {
        Puzzle p1629257215 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1629257215(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1629257215(editor, this);
            }
        };
        p1629257215.setID(1629257215);
        p1629257215.setName("1629257215");
        p1629257215.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1629257215);
        this.Desktop.RandomlyPlaced(p1629257215);
        p1629257215.setTopEdgeType(EdgeType.Convex);
        p1629257215.setBottomEdgeType(EdgeType.Convex);
        p1629257215.setLeftEdgeType(EdgeType.Concave);
        p1629257215.setRightEdgeType(EdgeType.Concave);
        p1629257215.setTopExactPuzzleID(1114616165);
        p1629257215.setBottomExactPuzzleID(1504832738);
        p1629257215.setLeftExactPuzzleID(2010462150);
        p1629257215.setRightExactPuzzleID(1962491871);
        p1629257215.getDisplayImage().loadImageFromResource(c, R.drawable.p1629257215h);
        p1629257215.setExactRow(1);
        p1629257215.setExactColumn(2);
        p1629257215.getSize().reset(111.0833f, 170.2f);
        p1629257215.getPositionOffset().reset(-55.54167f, -85.1f);
        p1629257215.setIsUseAbsolutePosition(true);
        p1629257215.setIsUseAbsoluteSize(true);
        p1629257215.getImage().setIsUseAbsolutePosition(true);
        p1629257215.getImage().setIsUseAbsoluteSize(true);
        p1629257215.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1629257215.resetPosition();
        getSpiritList().add(p1629257215);
    }

    /* access modifiers changed from: package-private */
    public void c1504832738(Context c) {
        Puzzle p1504832738 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1504832738(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1504832738(editor, this);
            }
        };
        p1504832738.setID(1504832738);
        p1504832738.setName("1504832738");
        p1504832738.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1504832738);
        this.Desktop.RandomlyPlaced(p1504832738);
        p1504832738.setTopEdgeType(EdgeType.Concave);
        p1504832738.setBottomEdgeType(EdgeType.Concave);
        p1504832738.setLeftEdgeType(EdgeType.Concave);
        p1504832738.setRightEdgeType(EdgeType.Concave);
        p1504832738.setTopExactPuzzleID(1629257215);
        p1504832738.setBottomExactPuzzleID(169466133);
        p1504832738.setLeftExactPuzzleID(1439712182);
        p1504832738.setRightExactPuzzleID(1918314562);
        p1504832738.getDisplayImage().loadImageFromResource(c, R.drawable.p1504832738h);
        p1504832738.setExactRow(2);
        p1504832738.setExactColumn(2);
        p1504832738.getSize().reset(111.0833f, 111.0f);
        p1504832738.getPositionOffset().reset(-55.54167f, -55.5f);
        p1504832738.setIsUseAbsolutePosition(true);
        p1504832738.setIsUseAbsoluteSize(true);
        p1504832738.getImage().setIsUseAbsolutePosition(true);
        p1504832738.getImage().setIsUseAbsoluteSize(true);
        p1504832738.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1504832738.resetPosition();
        getSpiritList().add(p1504832738);
    }

    /* access modifiers changed from: package-private */
    public void c169466133(Context c) {
        Puzzle p169466133 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load169466133(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save169466133(editor, this);
            }
        };
        p169466133.setID(169466133);
        p169466133.setName("169466133");
        p169466133.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p169466133);
        this.Desktop.RandomlyPlaced(p169466133);
        p169466133.setTopEdgeType(EdgeType.Convex);
        p169466133.setBottomEdgeType(EdgeType.Convex);
        p169466133.setLeftEdgeType(EdgeType.Concave);
        p169466133.setRightEdgeType(EdgeType.Convex);
        p169466133.setTopExactPuzzleID(1504832738);
        p169466133.setBottomExactPuzzleID(1550625862);
        p169466133.setLeftExactPuzzleID(1535984674);
        p169466133.setRightExactPuzzleID(1028647396);
        p169466133.getDisplayImage().loadImageFromResource(c, R.drawable.p169466133h);
        p169466133.setExactRow(3);
        p169466133.setExactColumn(2);
        p169466133.getSize().reset(140.6833f, 170.2f);
        p169466133.getPositionOffset().reset(-55.54167f, -85.1f);
        p169466133.setIsUseAbsolutePosition(true);
        p169466133.setIsUseAbsoluteSize(true);
        p169466133.getImage().setIsUseAbsolutePosition(true);
        p169466133.getImage().setIsUseAbsoluteSize(true);
        p169466133.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p169466133.resetPosition();
        getSpiritList().add(p169466133);
    }

    /* access modifiers changed from: package-private */
    public void c1550625862(Context c) {
        Puzzle p1550625862 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1550625862(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1550625862(editor, this);
            }
        };
        p1550625862.setID(1550625862);
        p1550625862.setName("1550625862");
        p1550625862.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1550625862);
        this.Desktop.RandomlyPlaced(p1550625862);
        p1550625862.setTopEdgeType(EdgeType.Concave);
        p1550625862.setBottomEdgeType(EdgeType.Convex);
        p1550625862.setLeftEdgeType(EdgeType.Convex);
        p1550625862.setRightEdgeType(EdgeType.Concave);
        p1550625862.setTopExactPuzzleID(169466133);
        p1550625862.setBottomExactPuzzleID(1659168475);
        p1550625862.setLeftExactPuzzleID(121050773);
        p1550625862.setRightExactPuzzleID(480267172);
        p1550625862.getDisplayImage().loadImageFromResource(c, R.drawable.p1550625862h);
        p1550625862.setExactRow(4);
        p1550625862.setExactColumn(2);
        p1550625862.getSize().reset(140.6833f, 140.6f);
        p1550625862.getPositionOffset().reset(-85.14167f, -55.5f);
        p1550625862.setIsUseAbsolutePosition(true);
        p1550625862.setIsUseAbsoluteSize(true);
        p1550625862.getImage().setIsUseAbsolutePosition(true);
        p1550625862.getImage().setIsUseAbsoluteSize(true);
        p1550625862.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1550625862.resetPosition();
        getSpiritList().add(p1550625862);
    }

    /* access modifiers changed from: package-private */
    public void c1659168475(Context c) {
        Puzzle p1659168475 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1659168475(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1659168475(editor, this);
            }
        };
        p1659168475.setID(1659168475);
        p1659168475.setName("1659168475");
        p1659168475.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1659168475);
        this.Desktop.RandomlyPlaced(p1659168475);
        p1659168475.setTopEdgeType(EdgeType.Concave);
        p1659168475.setBottomEdgeType(EdgeType.Flat);
        p1659168475.setLeftEdgeType(EdgeType.Concave);
        p1659168475.setRightEdgeType(EdgeType.Concave);
        p1659168475.setTopExactPuzzleID(1550625862);
        p1659168475.setBottomExactPuzzleID(-1);
        p1659168475.setLeftExactPuzzleID(1792926864);
        p1659168475.setRightExactPuzzleID(2063534525);
        p1659168475.getDisplayImage().loadImageFromResource(c, R.drawable.p1659168475h);
        p1659168475.setExactRow(5);
        p1659168475.setExactColumn(2);
        p1659168475.getSize().reset(111.0833f, 111.0f);
        p1659168475.getPositionOffset().reset(-55.54167f, -55.5f);
        p1659168475.setIsUseAbsolutePosition(true);
        p1659168475.setIsUseAbsoluteSize(true);
        p1659168475.getImage().setIsUseAbsolutePosition(true);
        p1659168475.getImage().setIsUseAbsoluteSize(true);
        p1659168475.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1659168475.resetPosition();
        getSpiritList().add(p1659168475);
    }

    /* access modifiers changed from: package-private */
    public void c1746440748(Context c) {
        Puzzle p1746440748 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1746440748(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1746440748(editor, this);
            }
        };
        p1746440748.setID(1746440748);
        p1746440748.setName("1746440748");
        p1746440748.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1746440748);
        this.Desktop.RandomlyPlaced(p1746440748);
        p1746440748.setTopEdgeType(EdgeType.Flat);
        p1746440748.setBottomEdgeType(EdgeType.Concave);
        p1746440748.setLeftEdgeType(EdgeType.Convex);
        p1746440748.setRightEdgeType(EdgeType.Convex);
        p1746440748.setTopExactPuzzleID(-1);
        p1746440748.setBottomExactPuzzleID(1962491871);
        p1746440748.setLeftExactPuzzleID(1114616165);
        p1746440748.setRightExactPuzzleID(1806710122);
        p1746440748.getDisplayImage().loadImageFromResource(c, R.drawable.p1746440748h);
        p1746440748.setExactRow(0);
        p1746440748.setExactColumn(3);
        p1746440748.getSize().reset(170.2833f, 111.0f);
        p1746440748.getPositionOffset().reset(-85.14167f, -55.5f);
        p1746440748.setIsUseAbsolutePosition(true);
        p1746440748.setIsUseAbsoluteSize(true);
        p1746440748.getImage().setIsUseAbsolutePosition(true);
        p1746440748.getImage().setIsUseAbsoluteSize(true);
        p1746440748.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1746440748.resetPosition();
        getSpiritList().add(p1746440748);
    }

    /* access modifiers changed from: package-private */
    public void c1962491871(Context c) {
        Puzzle p1962491871 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1962491871(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1962491871(editor, this);
            }
        };
        p1962491871.setID(1962491871);
        p1962491871.setName("1962491871");
        p1962491871.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1962491871);
        this.Desktop.RandomlyPlaced(p1962491871);
        p1962491871.setTopEdgeType(EdgeType.Convex);
        p1962491871.setBottomEdgeType(EdgeType.Convex);
        p1962491871.setLeftEdgeType(EdgeType.Convex);
        p1962491871.setRightEdgeType(EdgeType.Concave);
        p1962491871.setTopExactPuzzleID(1746440748);
        p1962491871.setBottomExactPuzzleID(1918314562);
        p1962491871.setLeftExactPuzzleID(1629257215);
        p1962491871.setRightExactPuzzleID(233452626);
        p1962491871.getDisplayImage().loadImageFromResource(c, R.drawable.p1962491871h);
        p1962491871.setExactRow(1);
        p1962491871.setExactColumn(3);
        p1962491871.getSize().reset(140.6833f, 170.2f);
        p1962491871.getPositionOffset().reset(-85.14167f, -85.1f);
        p1962491871.setIsUseAbsolutePosition(true);
        p1962491871.setIsUseAbsoluteSize(true);
        p1962491871.getImage().setIsUseAbsolutePosition(true);
        p1962491871.getImage().setIsUseAbsoluteSize(true);
        p1962491871.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1962491871.resetPosition();
        getSpiritList().add(p1962491871);
    }

    /* access modifiers changed from: package-private */
    public void c1918314562(Context c) {
        Puzzle p1918314562 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1918314562(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1918314562(editor, this);
            }
        };
        p1918314562.setID(1918314562);
        p1918314562.setName("1918314562");
        p1918314562.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1918314562);
        this.Desktop.RandomlyPlaced(p1918314562);
        p1918314562.setTopEdgeType(EdgeType.Concave);
        p1918314562.setBottomEdgeType(EdgeType.Convex);
        p1918314562.setLeftEdgeType(EdgeType.Convex);
        p1918314562.setRightEdgeType(EdgeType.Convex);
        p1918314562.setTopExactPuzzleID(1962491871);
        p1918314562.setBottomExactPuzzleID(1028647396);
        p1918314562.setLeftExactPuzzleID(1504832738);
        p1918314562.setRightExactPuzzleID(1578016639);
        p1918314562.getDisplayImage().loadImageFromResource(c, R.drawable.p1918314562h);
        p1918314562.setExactRow(2);
        p1918314562.setExactColumn(3);
        p1918314562.getSize().reset(170.2833f, 140.6f);
        p1918314562.getPositionOffset().reset(-85.14167f, -55.5f);
        p1918314562.setIsUseAbsolutePosition(true);
        p1918314562.setIsUseAbsoluteSize(true);
        p1918314562.getImage().setIsUseAbsolutePosition(true);
        p1918314562.getImage().setIsUseAbsoluteSize(true);
        p1918314562.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1918314562.resetPosition();
        getSpiritList().add(p1918314562);
    }

    /* access modifiers changed from: package-private */
    public void c1028647396(Context c) {
        Puzzle p1028647396 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1028647396(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1028647396(editor, this);
            }
        };
        p1028647396.setID(1028647396);
        p1028647396.setName("1028647396");
        p1028647396.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1028647396);
        this.Desktop.RandomlyPlaced(p1028647396);
        p1028647396.setTopEdgeType(EdgeType.Concave);
        p1028647396.setBottomEdgeType(EdgeType.Convex);
        p1028647396.setLeftEdgeType(EdgeType.Concave);
        p1028647396.setRightEdgeType(EdgeType.Concave);
        p1028647396.setTopExactPuzzleID(1918314562);
        p1028647396.setBottomExactPuzzleID(480267172);
        p1028647396.setLeftExactPuzzleID(169466133);
        p1028647396.setRightExactPuzzleID(2111421574);
        p1028647396.getDisplayImage().loadImageFromResource(c, R.drawable.p1028647396h);
        p1028647396.setExactRow(3);
        p1028647396.setExactColumn(3);
        p1028647396.getSize().reset(111.0833f, 140.6f);
        p1028647396.getPositionOffset().reset(-55.54167f, -55.5f);
        p1028647396.setIsUseAbsolutePosition(true);
        p1028647396.setIsUseAbsoluteSize(true);
        p1028647396.getImage().setIsUseAbsolutePosition(true);
        p1028647396.getImage().setIsUseAbsoluteSize(true);
        p1028647396.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1028647396.resetPosition();
        getSpiritList().add(p1028647396);
    }

    /* access modifiers changed from: package-private */
    public void c480267172(Context c) {
        Puzzle p480267172 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load480267172(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save480267172(editor, this);
            }
        };
        p480267172.setID(480267172);
        p480267172.setName("480267172");
        p480267172.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p480267172);
        this.Desktop.RandomlyPlaced(p480267172);
        p480267172.setTopEdgeType(EdgeType.Concave);
        p480267172.setBottomEdgeType(EdgeType.Concave);
        p480267172.setLeftEdgeType(EdgeType.Convex);
        p480267172.setRightEdgeType(EdgeType.Convex);
        p480267172.setTopExactPuzzleID(1028647396);
        p480267172.setBottomExactPuzzleID(2063534525);
        p480267172.setLeftExactPuzzleID(1550625862);
        p480267172.setRightExactPuzzleID(1011078854);
        p480267172.getDisplayImage().loadImageFromResource(c, R.drawable.p480267172h);
        p480267172.setExactRow(4);
        p480267172.setExactColumn(3);
        p480267172.getSize().reset(170.2833f, 111.0f);
        p480267172.getPositionOffset().reset(-85.14167f, -55.5f);
        p480267172.setIsUseAbsolutePosition(true);
        p480267172.setIsUseAbsoluteSize(true);
        p480267172.getImage().setIsUseAbsolutePosition(true);
        p480267172.getImage().setIsUseAbsoluteSize(true);
        p480267172.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p480267172.resetPosition();
        getSpiritList().add(p480267172);
    }

    /* access modifiers changed from: package-private */
    public void c2063534525(Context c) {
        Puzzle p2063534525 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load2063534525(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save2063534525(editor, this);
            }
        };
        p2063534525.setID(2063534525);
        p2063534525.setName("2063534525");
        p2063534525.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p2063534525);
        this.Desktop.RandomlyPlaced(p2063534525);
        p2063534525.setTopEdgeType(EdgeType.Convex);
        p2063534525.setBottomEdgeType(EdgeType.Flat);
        p2063534525.setLeftEdgeType(EdgeType.Convex);
        p2063534525.setRightEdgeType(EdgeType.Concave);
        p2063534525.setTopExactPuzzleID(480267172);
        p2063534525.setBottomExactPuzzleID(-1);
        p2063534525.setLeftExactPuzzleID(1659168475);
        p2063534525.setRightExactPuzzleID(440604811);
        p2063534525.getDisplayImage().loadImageFromResource(c, R.drawable.p2063534525h);
        p2063534525.setExactRow(5);
        p2063534525.setExactColumn(3);
        p2063534525.getSize().reset(140.6833f, 140.6f);
        p2063534525.getPositionOffset().reset(-85.14167f, -85.1f);
        p2063534525.setIsUseAbsolutePosition(true);
        p2063534525.setIsUseAbsoluteSize(true);
        p2063534525.getImage().setIsUseAbsolutePosition(true);
        p2063534525.getImage().setIsUseAbsoluteSize(true);
        p2063534525.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p2063534525.resetPosition();
        getSpiritList().add(p2063534525);
    }

    /* access modifiers changed from: package-private */
    public void c1806710122(Context c) {
        Puzzle p1806710122 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1806710122(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1806710122(editor, this);
            }
        };
        p1806710122.setID(1806710122);
        p1806710122.setName("1806710122");
        p1806710122.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1806710122);
        this.Desktop.RandomlyPlaced(p1806710122);
        p1806710122.setTopEdgeType(EdgeType.Flat);
        p1806710122.setBottomEdgeType(EdgeType.Concave);
        p1806710122.setLeftEdgeType(EdgeType.Concave);
        p1806710122.setRightEdgeType(EdgeType.Concave);
        p1806710122.setTopExactPuzzleID(-1);
        p1806710122.setBottomExactPuzzleID(233452626);
        p1806710122.setLeftExactPuzzleID(1746440748);
        p1806710122.setRightExactPuzzleID(707993294);
        p1806710122.getDisplayImage().loadImageFromResource(c, R.drawable.p1806710122h);
        p1806710122.setExactRow(0);
        p1806710122.setExactColumn(4);
        p1806710122.getSize().reset(111.0833f, 111.0f);
        p1806710122.getPositionOffset().reset(-55.54167f, -55.5f);
        p1806710122.setIsUseAbsolutePosition(true);
        p1806710122.setIsUseAbsoluteSize(true);
        p1806710122.getImage().setIsUseAbsolutePosition(true);
        p1806710122.getImage().setIsUseAbsoluteSize(true);
        p1806710122.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1806710122.resetPosition();
        getSpiritList().add(p1806710122);
    }

    /* access modifiers changed from: package-private */
    public void c233452626(Context c) {
        Puzzle p233452626 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load233452626(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save233452626(editor, this);
            }
        };
        p233452626.setID(233452626);
        p233452626.setName("233452626");
        p233452626.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p233452626);
        this.Desktop.RandomlyPlaced(p233452626);
        p233452626.setTopEdgeType(EdgeType.Convex);
        p233452626.setBottomEdgeType(EdgeType.Convex);
        p233452626.setLeftEdgeType(EdgeType.Convex);
        p233452626.setRightEdgeType(EdgeType.Concave);
        p233452626.setTopExactPuzzleID(1806710122);
        p233452626.setBottomExactPuzzleID(1578016639);
        p233452626.setLeftExactPuzzleID(1962491871);
        p233452626.setRightExactPuzzleID(1365809361);
        p233452626.getDisplayImage().loadImageFromResource(c, R.drawable.p233452626h);
        p233452626.setExactRow(1);
        p233452626.setExactColumn(4);
        p233452626.getSize().reset(140.6833f, 170.2f);
        p233452626.getPositionOffset().reset(-85.14167f, -85.1f);
        p233452626.setIsUseAbsolutePosition(true);
        p233452626.setIsUseAbsoluteSize(true);
        p233452626.getImage().setIsUseAbsolutePosition(true);
        p233452626.getImage().setIsUseAbsoluteSize(true);
        p233452626.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p233452626.resetPosition();
        getSpiritList().add(p233452626);
    }

    /* access modifiers changed from: package-private */
    public void c1578016639(Context c) {
        Puzzle p1578016639 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1578016639(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1578016639(editor, this);
            }
        };
        p1578016639.setID(1578016639);
        p1578016639.setName("1578016639");
        p1578016639.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1578016639);
        this.Desktop.RandomlyPlaced(p1578016639);
        p1578016639.setTopEdgeType(EdgeType.Concave);
        p1578016639.setBottomEdgeType(EdgeType.Concave);
        p1578016639.setLeftEdgeType(EdgeType.Concave);
        p1578016639.setRightEdgeType(EdgeType.Concave);
        p1578016639.setTopExactPuzzleID(233452626);
        p1578016639.setBottomExactPuzzleID(2111421574);
        p1578016639.setLeftExactPuzzleID(1918314562);
        p1578016639.setRightExactPuzzleID(1086466190);
        p1578016639.getDisplayImage().loadImageFromResource(c, R.drawable.p1578016639h);
        p1578016639.setExactRow(2);
        p1578016639.setExactColumn(4);
        p1578016639.getSize().reset(111.0833f, 111.0f);
        p1578016639.getPositionOffset().reset(-55.54167f, -55.5f);
        p1578016639.setIsUseAbsolutePosition(true);
        p1578016639.setIsUseAbsoluteSize(true);
        p1578016639.getImage().setIsUseAbsolutePosition(true);
        p1578016639.getImage().setIsUseAbsoluteSize(true);
        p1578016639.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1578016639.resetPosition();
        getSpiritList().add(p1578016639);
    }

    /* access modifiers changed from: package-private */
    public void c2111421574(Context c) {
        Puzzle p2111421574 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load2111421574(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save2111421574(editor, this);
            }
        };
        p2111421574.setID(2111421574);
        p2111421574.setName("2111421574");
        p2111421574.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p2111421574);
        this.Desktop.RandomlyPlaced(p2111421574);
        p2111421574.setTopEdgeType(EdgeType.Convex);
        p2111421574.setBottomEdgeType(EdgeType.Convex);
        p2111421574.setLeftEdgeType(EdgeType.Convex);
        p2111421574.setRightEdgeType(EdgeType.Concave);
        p2111421574.setTopExactPuzzleID(1578016639);
        p2111421574.setBottomExactPuzzleID(1011078854);
        p2111421574.setLeftExactPuzzleID(1028647396);
        p2111421574.setRightExactPuzzleID(677124579);
        p2111421574.getDisplayImage().loadImageFromResource(c, R.drawable.p2111421574h);
        p2111421574.setExactRow(3);
        p2111421574.setExactColumn(4);
        p2111421574.getSize().reset(140.6833f, 170.2f);
        p2111421574.getPositionOffset().reset(-85.14167f, -85.1f);
        p2111421574.setIsUseAbsolutePosition(true);
        p2111421574.setIsUseAbsoluteSize(true);
        p2111421574.getImage().setIsUseAbsolutePosition(true);
        p2111421574.getImage().setIsUseAbsoluteSize(true);
        p2111421574.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p2111421574.resetPosition();
        getSpiritList().add(p2111421574);
    }

    /* access modifiers changed from: package-private */
    public void c1011078854(Context c) {
        Puzzle p1011078854 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1011078854(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1011078854(editor, this);
            }
        };
        p1011078854.setID(1011078854);
        p1011078854.setName("1011078854");
        p1011078854.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1011078854);
        this.Desktop.RandomlyPlaced(p1011078854);
        p1011078854.setTopEdgeType(EdgeType.Concave);
        p1011078854.setBottomEdgeType(EdgeType.Concave);
        p1011078854.setLeftEdgeType(EdgeType.Concave);
        p1011078854.setRightEdgeType(EdgeType.Convex);
        p1011078854.setTopExactPuzzleID(2111421574);
        p1011078854.setBottomExactPuzzleID(440604811);
        p1011078854.setLeftExactPuzzleID(480267172);
        p1011078854.setRightExactPuzzleID(1976495085);
        p1011078854.getDisplayImage().loadImageFromResource(c, R.drawable.p1011078854h);
        p1011078854.setExactRow(4);
        p1011078854.setExactColumn(4);
        p1011078854.getSize().reset(140.6833f, 111.0f);
        p1011078854.getPositionOffset().reset(-55.54167f, -55.5f);
        p1011078854.setIsUseAbsolutePosition(true);
        p1011078854.setIsUseAbsoluteSize(true);
        p1011078854.getImage().setIsUseAbsolutePosition(true);
        p1011078854.getImage().setIsUseAbsoluteSize(true);
        p1011078854.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1011078854.resetPosition();
        getSpiritList().add(p1011078854);
    }

    /* access modifiers changed from: package-private */
    public void c440604811(Context c) {
        Puzzle p440604811 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load440604811(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save440604811(editor, this);
            }
        };
        p440604811.setID(440604811);
        p440604811.setName("440604811");
        p440604811.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p440604811);
        this.Desktop.RandomlyPlaced(p440604811);
        p440604811.setTopEdgeType(EdgeType.Convex);
        p440604811.setBottomEdgeType(EdgeType.Flat);
        p440604811.setLeftEdgeType(EdgeType.Convex);
        p440604811.setRightEdgeType(EdgeType.Convex);
        p440604811.setTopExactPuzzleID(1011078854);
        p440604811.setBottomExactPuzzleID(-1);
        p440604811.setLeftExactPuzzleID(2063534525);
        p440604811.setRightExactPuzzleID(1337293698);
        p440604811.getDisplayImage().loadImageFromResource(c, R.drawable.p440604811h);
        p440604811.setExactRow(5);
        p440604811.setExactColumn(4);
        p440604811.getSize().reset(170.2833f, 140.6f);
        p440604811.getPositionOffset().reset(-85.14167f, -85.1f);
        p440604811.setIsUseAbsolutePosition(true);
        p440604811.setIsUseAbsoluteSize(true);
        p440604811.getImage().setIsUseAbsolutePosition(true);
        p440604811.getImage().setIsUseAbsoluteSize(true);
        p440604811.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p440604811.resetPosition();
        getSpiritList().add(p440604811);
    }

    /* access modifiers changed from: package-private */
    public void c707993294(Context c) {
        Puzzle p707993294 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load707993294(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save707993294(editor, this);
            }
        };
        p707993294.setID(707993294);
        p707993294.setName("707993294");
        p707993294.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p707993294);
        this.Desktop.RandomlyPlaced(p707993294);
        p707993294.setTopEdgeType(EdgeType.Flat);
        p707993294.setBottomEdgeType(EdgeType.Convex);
        p707993294.setLeftEdgeType(EdgeType.Convex);
        p707993294.setRightEdgeType(EdgeType.Convex);
        p707993294.setTopExactPuzzleID(-1);
        p707993294.setBottomExactPuzzleID(1365809361);
        p707993294.setLeftExactPuzzleID(1806710122);
        p707993294.setRightExactPuzzleID(1809581665);
        p707993294.getDisplayImage().loadImageFromResource(c, R.drawable.p707993294h);
        p707993294.setExactRow(0);
        p707993294.setExactColumn(5);
        p707993294.getSize().reset(170.2833f, 140.6f);
        p707993294.getPositionOffset().reset(-85.14167f, -55.5f);
        p707993294.setIsUseAbsolutePosition(true);
        p707993294.setIsUseAbsoluteSize(true);
        p707993294.getImage().setIsUseAbsolutePosition(true);
        p707993294.getImage().setIsUseAbsoluteSize(true);
        p707993294.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p707993294.resetPosition();
        getSpiritList().add(p707993294);
    }

    /* access modifiers changed from: package-private */
    public void c1365809361(Context c) {
        Puzzle p1365809361 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1365809361(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1365809361(editor, this);
            }
        };
        p1365809361.setID(1365809361);
        p1365809361.setName("1365809361");
        p1365809361.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1365809361);
        this.Desktop.RandomlyPlaced(p1365809361);
        p1365809361.setTopEdgeType(EdgeType.Concave);
        p1365809361.setBottomEdgeType(EdgeType.Convex);
        p1365809361.setLeftEdgeType(EdgeType.Convex);
        p1365809361.setRightEdgeType(EdgeType.Concave);
        p1365809361.setTopExactPuzzleID(707993294);
        p1365809361.setBottomExactPuzzleID(1086466190);
        p1365809361.setLeftExactPuzzleID(233452626);
        p1365809361.setRightExactPuzzleID(1544933686);
        p1365809361.getDisplayImage().loadImageFromResource(c, R.drawable.p1365809361h);
        p1365809361.setExactRow(1);
        p1365809361.setExactColumn(5);
        p1365809361.getSize().reset(140.6833f, 140.6f);
        p1365809361.getPositionOffset().reset(-85.14167f, -55.5f);
        p1365809361.setIsUseAbsolutePosition(true);
        p1365809361.setIsUseAbsoluteSize(true);
        p1365809361.getImage().setIsUseAbsolutePosition(true);
        p1365809361.getImage().setIsUseAbsoluteSize(true);
        p1365809361.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1365809361.resetPosition();
        getSpiritList().add(p1365809361);
    }

    /* access modifiers changed from: package-private */
    public void c1086466190(Context c) {
        Puzzle p1086466190 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1086466190(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1086466190(editor, this);
            }
        };
        p1086466190.setID(1086466190);
        p1086466190.setName("1086466190");
        p1086466190.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1086466190);
        this.Desktop.RandomlyPlaced(p1086466190);
        p1086466190.setTopEdgeType(EdgeType.Concave);
        p1086466190.setBottomEdgeType(EdgeType.Concave);
        p1086466190.setLeftEdgeType(EdgeType.Convex);
        p1086466190.setRightEdgeType(EdgeType.Convex);
        p1086466190.setTopExactPuzzleID(1365809361);
        p1086466190.setBottomExactPuzzleID(677124579);
        p1086466190.setLeftExactPuzzleID(1578016639);
        p1086466190.setRightExactPuzzleID(1363659385);
        p1086466190.getDisplayImage().loadImageFromResource(c, R.drawable.p1086466190h);
        p1086466190.setExactRow(2);
        p1086466190.setExactColumn(5);
        p1086466190.getSize().reset(170.2833f, 111.0f);
        p1086466190.getPositionOffset().reset(-85.14167f, -55.5f);
        p1086466190.setIsUseAbsolutePosition(true);
        p1086466190.setIsUseAbsoluteSize(true);
        p1086466190.getImage().setIsUseAbsolutePosition(true);
        p1086466190.getImage().setIsUseAbsoluteSize(true);
        p1086466190.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1086466190.resetPosition();
        getSpiritList().add(p1086466190);
    }

    /* access modifiers changed from: package-private */
    public void c677124579(Context c) {
        Puzzle p677124579 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load677124579(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save677124579(editor, this);
            }
        };
        p677124579.setID(677124579);
        p677124579.setName("677124579");
        p677124579.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p677124579);
        this.Desktop.RandomlyPlaced(p677124579);
        p677124579.setTopEdgeType(EdgeType.Convex);
        p677124579.setBottomEdgeType(EdgeType.Concave);
        p677124579.setLeftEdgeType(EdgeType.Convex);
        p677124579.setRightEdgeType(EdgeType.Convex);
        p677124579.setTopExactPuzzleID(1086466190);
        p677124579.setBottomExactPuzzleID(1976495085);
        p677124579.setLeftExactPuzzleID(2111421574);
        p677124579.setRightExactPuzzleID(126487040);
        p677124579.getDisplayImage().loadImageFromResource(c, R.drawable.p677124579h);
        p677124579.setExactRow(3);
        p677124579.setExactColumn(5);
        p677124579.getSize().reset(170.2833f, 140.6f);
        p677124579.getPositionOffset().reset(-85.14167f, -85.1f);
        p677124579.setIsUseAbsolutePosition(true);
        p677124579.setIsUseAbsoluteSize(true);
        p677124579.getImage().setIsUseAbsolutePosition(true);
        p677124579.getImage().setIsUseAbsoluteSize(true);
        p677124579.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p677124579.resetPosition();
        getSpiritList().add(p677124579);
    }

    /* access modifiers changed from: package-private */
    public void c1976495085(Context c) {
        Puzzle p1976495085 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1976495085(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1976495085(editor, this);
            }
        };
        p1976495085.setID(1976495085);
        p1976495085.setName("1976495085");
        p1976495085.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1976495085);
        this.Desktop.RandomlyPlaced(p1976495085);
        p1976495085.setTopEdgeType(EdgeType.Convex);
        p1976495085.setBottomEdgeType(EdgeType.Convex);
        p1976495085.setLeftEdgeType(EdgeType.Concave);
        p1976495085.setRightEdgeType(EdgeType.Concave);
        p1976495085.setTopExactPuzzleID(677124579);
        p1976495085.setBottomExactPuzzleID(1337293698);
        p1976495085.setLeftExactPuzzleID(1011078854);
        p1976495085.setRightExactPuzzleID(1461794885);
        p1976495085.getDisplayImage().loadImageFromResource(c, R.drawable.p1976495085h);
        p1976495085.setExactRow(4);
        p1976495085.setExactColumn(5);
        p1976495085.getSize().reset(111.0833f, 170.2f);
        p1976495085.getPositionOffset().reset(-55.54167f, -85.1f);
        p1976495085.setIsUseAbsolutePosition(true);
        p1976495085.setIsUseAbsoluteSize(true);
        p1976495085.getImage().setIsUseAbsolutePosition(true);
        p1976495085.getImage().setIsUseAbsoluteSize(true);
        p1976495085.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1976495085.resetPosition();
        getSpiritList().add(p1976495085);
    }

    /* access modifiers changed from: package-private */
    public void c1337293698(Context c) {
        Puzzle p1337293698 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1337293698(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1337293698(editor, this);
            }
        };
        p1337293698.setID(1337293698);
        p1337293698.setName("1337293698");
        p1337293698.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1337293698);
        this.Desktop.RandomlyPlaced(p1337293698);
        p1337293698.setTopEdgeType(EdgeType.Concave);
        p1337293698.setBottomEdgeType(EdgeType.Flat);
        p1337293698.setLeftEdgeType(EdgeType.Concave);
        p1337293698.setRightEdgeType(EdgeType.Convex);
        p1337293698.setTopExactPuzzleID(1976495085);
        p1337293698.setBottomExactPuzzleID(-1);
        p1337293698.setLeftExactPuzzleID(440604811);
        p1337293698.setRightExactPuzzleID(1794224488);
        p1337293698.getDisplayImage().loadImageFromResource(c, R.drawable.p1337293698h);
        p1337293698.setExactRow(5);
        p1337293698.setExactColumn(5);
        p1337293698.getSize().reset(140.6833f, 111.0f);
        p1337293698.getPositionOffset().reset(-55.54167f, -55.5f);
        p1337293698.setIsUseAbsolutePosition(true);
        p1337293698.setIsUseAbsoluteSize(true);
        p1337293698.getImage().setIsUseAbsolutePosition(true);
        p1337293698.getImage().setIsUseAbsoluteSize(true);
        p1337293698.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1337293698.resetPosition();
        getSpiritList().add(p1337293698);
    }

    /* access modifiers changed from: package-private */
    public void c1809581665(Context c) {
        Puzzle p1809581665 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1809581665(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1809581665(editor, this);
            }
        };
        p1809581665.setID(1809581665);
        p1809581665.setName("1809581665");
        p1809581665.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1809581665);
        this.Desktop.RandomlyPlaced(p1809581665);
        p1809581665.setTopEdgeType(EdgeType.Flat);
        p1809581665.setBottomEdgeType(EdgeType.Concave);
        p1809581665.setLeftEdgeType(EdgeType.Concave);
        p1809581665.setRightEdgeType(EdgeType.Concave);
        p1809581665.setTopExactPuzzleID(-1);
        p1809581665.setBottomExactPuzzleID(1544933686);
        p1809581665.setLeftExactPuzzleID(707993294);
        p1809581665.setRightExactPuzzleID(1278619394);
        p1809581665.getDisplayImage().loadImageFromResource(c, R.drawable.p1809581665h);
        p1809581665.setExactRow(0);
        p1809581665.setExactColumn(6);
        p1809581665.getSize().reset(111.0833f, 111.0f);
        p1809581665.getPositionOffset().reset(-55.54167f, -55.5f);
        p1809581665.setIsUseAbsolutePosition(true);
        p1809581665.setIsUseAbsoluteSize(true);
        p1809581665.getImage().setIsUseAbsolutePosition(true);
        p1809581665.getImage().setIsUseAbsoluteSize(true);
        p1809581665.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1809581665.resetPosition();
        getSpiritList().add(p1809581665);
    }

    /* access modifiers changed from: package-private */
    public void c1544933686(Context c) {
        Puzzle p1544933686 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1544933686(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1544933686(editor, this);
            }
        };
        p1544933686.setID(1544933686);
        p1544933686.setName("1544933686");
        p1544933686.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1544933686);
        this.Desktop.RandomlyPlaced(p1544933686);
        p1544933686.setTopEdgeType(EdgeType.Convex);
        p1544933686.setBottomEdgeType(EdgeType.Concave);
        p1544933686.setLeftEdgeType(EdgeType.Convex);
        p1544933686.setRightEdgeType(EdgeType.Convex);
        p1544933686.setTopExactPuzzleID(1809581665);
        p1544933686.setBottomExactPuzzleID(1363659385);
        p1544933686.setLeftExactPuzzleID(1365809361);
        p1544933686.setRightExactPuzzleID(305531379);
        p1544933686.getDisplayImage().loadImageFromResource(c, R.drawable.p1544933686h);
        p1544933686.setExactRow(1);
        p1544933686.setExactColumn(6);
        p1544933686.getSize().reset(170.2833f, 140.6f);
        p1544933686.getPositionOffset().reset(-85.14167f, -85.1f);
        p1544933686.setIsUseAbsolutePosition(true);
        p1544933686.setIsUseAbsoluteSize(true);
        p1544933686.getImage().setIsUseAbsolutePosition(true);
        p1544933686.getImage().setIsUseAbsoluteSize(true);
        p1544933686.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1544933686.resetPosition();
        getSpiritList().add(p1544933686);
    }

    /* access modifiers changed from: package-private */
    public void c1363659385(Context c) {
        Puzzle p1363659385 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1363659385(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1363659385(editor, this);
            }
        };
        p1363659385.setID(1363659385);
        p1363659385.setName("1363659385");
        p1363659385.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1363659385);
        this.Desktop.RandomlyPlaced(p1363659385);
        p1363659385.setTopEdgeType(EdgeType.Convex);
        p1363659385.setBottomEdgeType(EdgeType.Convex);
        p1363659385.setLeftEdgeType(EdgeType.Concave);
        p1363659385.setRightEdgeType(EdgeType.Concave);
        p1363659385.setTopExactPuzzleID(1544933686);
        p1363659385.setBottomExactPuzzleID(126487040);
        p1363659385.setLeftExactPuzzleID(1086466190);
        p1363659385.setRightExactPuzzleID(1965247119);
        p1363659385.getDisplayImage().loadImageFromResource(c, R.drawable.p1363659385h);
        p1363659385.setExactRow(2);
        p1363659385.setExactColumn(6);
        p1363659385.getSize().reset(111.0833f, 170.2f);
        p1363659385.getPositionOffset().reset(-55.54167f, -85.1f);
        p1363659385.setIsUseAbsolutePosition(true);
        p1363659385.setIsUseAbsoluteSize(true);
        p1363659385.getImage().setIsUseAbsolutePosition(true);
        p1363659385.getImage().setIsUseAbsoluteSize(true);
        p1363659385.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1363659385.resetPosition();
        getSpiritList().add(p1363659385);
    }

    /* access modifiers changed from: package-private */
    public void c126487040(Context c) {
        Puzzle p126487040 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load126487040(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save126487040(editor, this);
            }
        };
        p126487040.setID(126487040);
        p126487040.setName("126487040");
        p126487040.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p126487040);
        this.Desktop.RandomlyPlaced(p126487040);
        p126487040.setTopEdgeType(EdgeType.Concave);
        p126487040.setBottomEdgeType(EdgeType.Convex);
        p126487040.setLeftEdgeType(EdgeType.Concave);
        p126487040.setRightEdgeType(EdgeType.Convex);
        p126487040.setTopExactPuzzleID(1363659385);
        p126487040.setBottomExactPuzzleID(1461794885);
        p126487040.setLeftExactPuzzleID(677124579);
        p126487040.setRightExactPuzzleID(704666089);
        p126487040.getDisplayImage().loadImageFromResource(c, R.drawable.p126487040h);
        p126487040.setExactRow(3);
        p126487040.setExactColumn(6);
        p126487040.getSize().reset(140.6833f, 140.6f);
        p126487040.getPositionOffset().reset(-55.54167f, -55.5f);
        p126487040.setIsUseAbsolutePosition(true);
        p126487040.setIsUseAbsoluteSize(true);
        p126487040.getImage().setIsUseAbsolutePosition(true);
        p126487040.getImage().setIsUseAbsoluteSize(true);
        p126487040.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p126487040.resetPosition();
        getSpiritList().add(p126487040);
    }

    /* access modifiers changed from: package-private */
    public void c1461794885(Context c) {
        Puzzle p1461794885 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1461794885(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1461794885(editor, this);
            }
        };
        p1461794885.setID(1461794885);
        p1461794885.setName("1461794885");
        p1461794885.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1461794885);
        this.Desktop.RandomlyPlaced(p1461794885);
        p1461794885.setTopEdgeType(EdgeType.Concave);
        p1461794885.setBottomEdgeType(EdgeType.Concave);
        p1461794885.setLeftEdgeType(EdgeType.Convex);
        p1461794885.setRightEdgeType(EdgeType.Concave);
        p1461794885.setTopExactPuzzleID(126487040);
        p1461794885.setBottomExactPuzzleID(1794224488);
        p1461794885.setLeftExactPuzzleID(1976495085);
        p1461794885.setRightExactPuzzleID(889907541);
        p1461794885.getDisplayImage().loadImageFromResource(c, R.drawable.p1461794885h);
        p1461794885.setExactRow(4);
        p1461794885.setExactColumn(6);
        p1461794885.getSize().reset(140.6833f, 111.0f);
        p1461794885.getPositionOffset().reset(-85.14167f, -55.5f);
        p1461794885.setIsUseAbsolutePosition(true);
        p1461794885.setIsUseAbsoluteSize(true);
        p1461794885.getImage().setIsUseAbsolutePosition(true);
        p1461794885.getImage().setIsUseAbsoluteSize(true);
        p1461794885.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1461794885.resetPosition();
        getSpiritList().add(p1461794885);
    }

    /* access modifiers changed from: package-private */
    public void c1794224488(Context c) {
        Puzzle p1794224488 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1794224488(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1794224488(editor, this);
            }
        };
        p1794224488.setID(1794224488);
        p1794224488.setName("1794224488");
        p1794224488.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1794224488);
        this.Desktop.RandomlyPlaced(p1794224488);
        p1794224488.setTopEdgeType(EdgeType.Convex);
        p1794224488.setBottomEdgeType(EdgeType.Flat);
        p1794224488.setLeftEdgeType(EdgeType.Concave);
        p1794224488.setRightEdgeType(EdgeType.Convex);
        p1794224488.setTopExactPuzzleID(1461794885);
        p1794224488.setBottomExactPuzzleID(-1);
        p1794224488.setLeftExactPuzzleID(1337293698);
        p1794224488.setRightExactPuzzleID(996557555);
        p1794224488.getDisplayImage().loadImageFromResource(c, R.drawable.p1794224488h);
        p1794224488.setExactRow(5);
        p1794224488.setExactColumn(6);
        p1794224488.getSize().reset(140.6833f, 140.6f);
        p1794224488.getPositionOffset().reset(-55.54167f, -85.1f);
        p1794224488.setIsUseAbsolutePosition(true);
        p1794224488.setIsUseAbsoluteSize(true);
        p1794224488.getImage().setIsUseAbsolutePosition(true);
        p1794224488.getImage().setIsUseAbsoluteSize(true);
        p1794224488.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1794224488.resetPosition();
        getSpiritList().add(p1794224488);
    }

    /* access modifiers changed from: package-private */
    public void c1278619394(Context c) {
        Puzzle p1278619394 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1278619394(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1278619394(editor, this);
            }
        };
        p1278619394.setID(1278619394);
        p1278619394.setName("1278619394");
        p1278619394.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1278619394);
        this.Desktop.RandomlyPlaced(p1278619394);
        p1278619394.setTopEdgeType(EdgeType.Flat);
        p1278619394.setBottomEdgeType(EdgeType.Concave);
        p1278619394.setLeftEdgeType(EdgeType.Convex);
        p1278619394.setRightEdgeType(EdgeType.Concave);
        p1278619394.setTopExactPuzzleID(-1);
        p1278619394.setBottomExactPuzzleID(305531379);
        p1278619394.setLeftExactPuzzleID(1809581665);
        p1278619394.setRightExactPuzzleID(405078516);
        p1278619394.getDisplayImage().loadImageFromResource(c, R.drawable.p1278619394h);
        p1278619394.setExactRow(0);
        p1278619394.setExactColumn(7);
        p1278619394.getSize().reset(140.6833f, 111.0f);
        p1278619394.getPositionOffset().reset(-85.14167f, -55.5f);
        p1278619394.setIsUseAbsolutePosition(true);
        p1278619394.setIsUseAbsoluteSize(true);
        p1278619394.getImage().setIsUseAbsolutePosition(true);
        p1278619394.getImage().setIsUseAbsoluteSize(true);
        p1278619394.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1278619394.resetPosition();
        getSpiritList().add(p1278619394);
    }

    /* access modifiers changed from: package-private */
    public void c305531379(Context c) {
        Puzzle p305531379 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load305531379(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save305531379(editor, this);
            }
        };
        p305531379.setID(305531379);
        p305531379.setName("305531379");
        p305531379.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p305531379);
        this.Desktop.RandomlyPlaced(p305531379);
        p305531379.setTopEdgeType(EdgeType.Convex);
        p305531379.setBottomEdgeType(EdgeType.Convex);
        p305531379.setLeftEdgeType(EdgeType.Concave);
        p305531379.setRightEdgeType(EdgeType.Concave);
        p305531379.setTopExactPuzzleID(1278619394);
        p305531379.setBottomExactPuzzleID(1965247119);
        p305531379.setLeftExactPuzzleID(1544933686);
        p305531379.setRightExactPuzzleID(1092698069);
        p305531379.getDisplayImage().loadImageFromResource(c, R.drawable.p305531379h);
        p305531379.setExactRow(1);
        p305531379.setExactColumn(7);
        p305531379.getSize().reset(111.0833f, 170.2f);
        p305531379.getPositionOffset().reset(-55.54167f, -85.1f);
        p305531379.setIsUseAbsolutePosition(true);
        p305531379.setIsUseAbsoluteSize(true);
        p305531379.getImage().setIsUseAbsolutePosition(true);
        p305531379.getImage().setIsUseAbsoluteSize(true);
        p305531379.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p305531379.resetPosition();
        getSpiritList().add(p305531379);
    }

    /* access modifiers changed from: package-private */
    public void c1965247119(Context c) {
        Puzzle p1965247119 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1965247119(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1965247119(editor, this);
            }
        };
        p1965247119.setID(1965247119);
        p1965247119.setName("1965247119");
        p1965247119.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1965247119);
        this.Desktop.RandomlyPlaced(p1965247119);
        p1965247119.setTopEdgeType(EdgeType.Concave);
        p1965247119.setBottomEdgeType(EdgeType.Convex);
        p1965247119.setLeftEdgeType(EdgeType.Convex);
        p1965247119.setRightEdgeType(EdgeType.Concave);
        p1965247119.setTopExactPuzzleID(305531379);
        p1965247119.setBottomExactPuzzleID(704666089);
        p1965247119.setLeftExactPuzzleID(1363659385);
        p1965247119.setRightExactPuzzleID(1584592799);
        p1965247119.getDisplayImage().loadImageFromResource(c, R.drawable.p1965247119h);
        p1965247119.setExactRow(2);
        p1965247119.setExactColumn(7);
        p1965247119.getSize().reset(140.6833f, 140.6f);
        p1965247119.getPositionOffset().reset(-85.14167f, -55.5f);
        p1965247119.setIsUseAbsolutePosition(true);
        p1965247119.setIsUseAbsoluteSize(true);
        p1965247119.getImage().setIsUseAbsolutePosition(true);
        p1965247119.getImage().setIsUseAbsoluteSize(true);
        p1965247119.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1965247119.resetPosition();
        getSpiritList().add(p1965247119);
    }

    /* access modifiers changed from: package-private */
    public void c704666089(Context c) {
        Puzzle p704666089 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load704666089(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save704666089(editor, this);
            }
        };
        p704666089.setID(704666089);
        p704666089.setName("704666089");
        p704666089.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p704666089);
        this.Desktop.RandomlyPlaced(p704666089);
        p704666089.setTopEdgeType(EdgeType.Concave);
        p704666089.setBottomEdgeType(EdgeType.Convex);
        p704666089.setLeftEdgeType(EdgeType.Concave);
        p704666089.setRightEdgeType(EdgeType.Convex);
        p704666089.setTopExactPuzzleID(1965247119);
        p704666089.setBottomExactPuzzleID(889907541);
        p704666089.setLeftExactPuzzleID(126487040);
        p704666089.setRightExactPuzzleID(1401309947);
        p704666089.getDisplayImage().loadImageFromResource(c, R.drawable.p704666089h);
        p704666089.setExactRow(3);
        p704666089.setExactColumn(7);
        p704666089.getSize().reset(140.6833f, 140.6f);
        p704666089.getPositionOffset().reset(-55.54167f, -55.5f);
        p704666089.setIsUseAbsolutePosition(true);
        p704666089.setIsUseAbsoluteSize(true);
        p704666089.getImage().setIsUseAbsolutePosition(true);
        p704666089.getImage().setIsUseAbsoluteSize(true);
        p704666089.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p704666089.resetPosition();
        getSpiritList().add(p704666089);
    }

    /* access modifiers changed from: package-private */
    public void c889907541(Context c) {
        Puzzle p889907541 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load889907541(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save889907541(editor, this);
            }
        };
        p889907541.setID(889907541);
        p889907541.setName("889907541");
        p889907541.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p889907541);
        this.Desktop.RandomlyPlaced(p889907541);
        p889907541.setTopEdgeType(EdgeType.Concave);
        p889907541.setBottomEdgeType(EdgeType.Convex);
        p889907541.setLeftEdgeType(EdgeType.Convex);
        p889907541.setRightEdgeType(EdgeType.Convex);
        p889907541.setTopExactPuzzleID(704666089);
        p889907541.setBottomExactPuzzleID(996557555);
        p889907541.setLeftExactPuzzleID(1461794885);
        p889907541.setRightExactPuzzleID(1967921646);
        p889907541.getDisplayImage().loadImageFromResource(c, R.drawable.p889907541h);
        p889907541.setExactRow(4);
        p889907541.setExactColumn(7);
        p889907541.getSize().reset(170.2833f, 140.6f);
        p889907541.getPositionOffset().reset(-85.14167f, -55.5f);
        p889907541.setIsUseAbsolutePosition(true);
        p889907541.setIsUseAbsoluteSize(true);
        p889907541.getImage().setIsUseAbsolutePosition(true);
        p889907541.getImage().setIsUseAbsoluteSize(true);
        p889907541.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p889907541.resetPosition();
        getSpiritList().add(p889907541);
    }

    /* access modifiers changed from: package-private */
    public void c996557555(Context c) {
        Puzzle p996557555 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load996557555(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save996557555(editor, this);
            }
        };
        p996557555.setID(996557555);
        p996557555.setName("996557555");
        p996557555.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p996557555);
        this.Desktop.RandomlyPlaced(p996557555);
        p996557555.setTopEdgeType(EdgeType.Concave);
        p996557555.setBottomEdgeType(EdgeType.Flat);
        p996557555.setLeftEdgeType(EdgeType.Concave);
        p996557555.setRightEdgeType(EdgeType.Concave);
        p996557555.setTopExactPuzzleID(889907541);
        p996557555.setBottomExactPuzzleID(-1);
        p996557555.setLeftExactPuzzleID(1794224488);
        p996557555.setRightExactPuzzleID(1770746245);
        p996557555.getDisplayImage().loadImageFromResource(c, R.drawable.p996557555h);
        p996557555.setExactRow(5);
        p996557555.setExactColumn(7);
        p996557555.getSize().reset(111.0833f, 111.0f);
        p996557555.getPositionOffset().reset(-55.54167f, -55.5f);
        p996557555.setIsUseAbsolutePosition(true);
        p996557555.setIsUseAbsoluteSize(true);
        p996557555.getImage().setIsUseAbsolutePosition(true);
        p996557555.getImage().setIsUseAbsoluteSize(true);
        p996557555.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p996557555.resetPosition();
        getSpiritList().add(p996557555);
    }

    /* access modifiers changed from: package-private */
    public void c405078516(Context c) {
        Puzzle p405078516 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load405078516(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save405078516(editor, this);
            }
        };
        p405078516.setID(405078516);
        p405078516.setName("405078516");
        p405078516.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p405078516);
        this.Desktop.RandomlyPlaced(p405078516);
        p405078516.setTopEdgeType(EdgeType.Flat);
        p405078516.setBottomEdgeType(EdgeType.Concave);
        p405078516.setLeftEdgeType(EdgeType.Convex);
        p405078516.setRightEdgeType(EdgeType.Convex);
        p405078516.setTopExactPuzzleID(-1);
        p405078516.setBottomExactPuzzleID(1092698069);
        p405078516.setLeftExactPuzzleID(1278619394);
        p405078516.setRightExactPuzzleID(1199201345);
        p405078516.getDisplayImage().loadImageFromResource(c, R.drawable.p405078516h);
        p405078516.setExactRow(0);
        p405078516.setExactColumn(8);
        p405078516.getSize().reset(170.2833f, 111.0f);
        p405078516.getPositionOffset().reset(-85.14167f, -55.5f);
        p405078516.setIsUseAbsolutePosition(true);
        p405078516.setIsUseAbsoluteSize(true);
        p405078516.getImage().setIsUseAbsolutePosition(true);
        p405078516.getImage().setIsUseAbsoluteSize(true);
        p405078516.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p405078516.resetPosition();
        getSpiritList().add(p405078516);
    }

    /* access modifiers changed from: package-private */
    public void c1092698069(Context c) {
        Puzzle p1092698069 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1092698069(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1092698069(editor, this);
            }
        };
        p1092698069.setID(1092698069);
        p1092698069.setName("1092698069");
        p1092698069.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1092698069);
        this.Desktop.RandomlyPlaced(p1092698069);
        p1092698069.setTopEdgeType(EdgeType.Convex);
        p1092698069.setBottomEdgeType(EdgeType.Convex);
        p1092698069.setLeftEdgeType(EdgeType.Convex);
        p1092698069.setRightEdgeType(EdgeType.Convex);
        p1092698069.setTopExactPuzzleID(405078516);
        p1092698069.setBottomExactPuzzleID(1584592799);
        p1092698069.setLeftExactPuzzleID(305531379);
        p1092698069.setRightExactPuzzleID(1721237390);
        p1092698069.getDisplayImage().loadImageFromResource(c, R.drawable.p1092698069h);
        p1092698069.setExactRow(1);
        p1092698069.setExactColumn(8);
        p1092698069.getSize().reset(170.2833f, 170.2f);
        p1092698069.getPositionOffset().reset(-85.14167f, -85.1f);
        p1092698069.setIsUseAbsolutePosition(true);
        p1092698069.setIsUseAbsoluteSize(true);
        p1092698069.getImage().setIsUseAbsolutePosition(true);
        p1092698069.getImage().setIsUseAbsoluteSize(true);
        p1092698069.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1092698069.resetPosition();
        getSpiritList().add(p1092698069);
    }

    /* access modifiers changed from: package-private */
    public void c1584592799(Context c) {
        Puzzle p1584592799 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1584592799(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1584592799(editor, this);
            }
        };
        p1584592799.setID(1584592799);
        p1584592799.setName("1584592799");
        p1584592799.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1584592799);
        this.Desktop.RandomlyPlaced(p1584592799);
        p1584592799.setTopEdgeType(EdgeType.Concave);
        p1584592799.setBottomEdgeType(EdgeType.Concave);
        p1584592799.setLeftEdgeType(EdgeType.Convex);
        p1584592799.setRightEdgeType(EdgeType.Convex);
        p1584592799.setTopExactPuzzleID(1092698069);
        p1584592799.setBottomExactPuzzleID(1401309947);
        p1584592799.setLeftExactPuzzleID(1965247119);
        p1584592799.setRightExactPuzzleID(1438994516);
        p1584592799.getDisplayImage().loadImageFromResource(c, R.drawable.p1584592799h);
        p1584592799.setExactRow(2);
        p1584592799.setExactColumn(8);
        p1584592799.getSize().reset(170.2833f, 111.0f);
        p1584592799.getPositionOffset().reset(-85.14167f, -55.5f);
        p1584592799.setIsUseAbsolutePosition(true);
        p1584592799.setIsUseAbsoluteSize(true);
        p1584592799.getImage().setIsUseAbsolutePosition(true);
        p1584592799.getImage().setIsUseAbsoluteSize(true);
        p1584592799.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1584592799.resetPosition();
        getSpiritList().add(p1584592799);
    }

    /* access modifiers changed from: package-private */
    public void c1401309947(Context c) {
        Puzzle p1401309947 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1401309947(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1401309947(editor, this);
            }
        };
        p1401309947.setID(1401309947);
        p1401309947.setName("1401309947");
        p1401309947.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1401309947);
        this.Desktop.RandomlyPlaced(p1401309947);
        p1401309947.setTopEdgeType(EdgeType.Convex);
        p1401309947.setBottomEdgeType(EdgeType.Concave);
        p1401309947.setLeftEdgeType(EdgeType.Concave);
        p1401309947.setRightEdgeType(EdgeType.Convex);
        p1401309947.setTopExactPuzzleID(1584592799);
        p1401309947.setBottomExactPuzzleID(1967921646);
        p1401309947.setLeftExactPuzzleID(704666089);
        p1401309947.setRightExactPuzzleID(1924795414);
        p1401309947.getDisplayImage().loadImageFromResource(c, R.drawable.p1401309947h);
        p1401309947.setExactRow(3);
        p1401309947.setExactColumn(8);
        p1401309947.getSize().reset(140.6833f, 140.6f);
        p1401309947.getPositionOffset().reset(-55.54167f, -85.1f);
        p1401309947.setIsUseAbsolutePosition(true);
        p1401309947.setIsUseAbsoluteSize(true);
        p1401309947.getImage().setIsUseAbsolutePosition(true);
        p1401309947.getImage().setIsUseAbsoluteSize(true);
        p1401309947.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1401309947.resetPosition();
        getSpiritList().add(p1401309947);
    }

    /* access modifiers changed from: package-private */
    public void c1967921646(Context c) {
        Puzzle p1967921646 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1967921646(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1967921646(editor, this);
            }
        };
        p1967921646.setID(1967921646);
        p1967921646.setName("1967921646");
        p1967921646.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1967921646);
        this.Desktop.RandomlyPlaced(p1967921646);
        p1967921646.setTopEdgeType(EdgeType.Convex);
        p1967921646.setBottomEdgeType(EdgeType.Convex);
        p1967921646.setLeftEdgeType(EdgeType.Concave);
        p1967921646.setRightEdgeType(EdgeType.Concave);
        p1967921646.setTopExactPuzzleID(1401309947);
        p1967921646.setBottomExactPuzzleID(1770746245);
        p1967921646.setLeftExactPuzzleID(889907541);
        p1967921646.setRightExactPuzzleID(1527842742);
        p1967921646.getDisplayImage().loadImageFromResource(c, R.drawable.p1967921646h);
        p1967921646.setExactRow(4);
        p1967921646.setExactColumn(8);
        p1967921646.getSize().reset(111.0833f, 170.2f);
        p1967921646.getPositionOffset().reset(-55.54167f, -85.1f);
        p1967921646.setIsUseAbsolutePosition(true);
        p1967921646.setIsUseAbsoluteSize(true);
        p1967921646.getImage().setIsUseAbsolutePosition(true);
        p1967921646.getImage().setIsUseAbsoluteSize(true);
        p1967921646.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1967921646.resetPosition();
        getSpiritList().add(p1967921646);
    }

    /* access modifiers changed from: package-private */
    public void c1770746245(Context c) {
        Puzzle p1770746245 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1770746245(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1770746245(editor, this);
            }
        };
        p1770746245.setID(1770746245);
        p1770746245.setName("1770746245");
        p1770746245.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1770746245);
        this.Desktop.RandomlyPlaced(p1770746245);
        p1770746245.setTopEdgeType(EdgeType.Concave);
        p1770746245.setBottomEdgeType(EdgeType.Flat);
        p1770746245.setLeftEdgeType(EdgeType.Convex);
        p1770746245.setRightEdgeType(EdgeType.Concave);
        p1770746245.setTopExactPuzzleID(1967921646);
        p1770746245.setBottomExactPuzzleID(-1);
        p1770746245.setLeftExactPuzzleID(996557555);
        p1770746245.setRightExactPuzzleID(735389401);
        p1770746245.getDisplayImage().loadImageFromResource(c, R.drawable.p1770746245h);
        p1770746245.setExactRow(5);
        p1770746245.setExactColumn(8);
        p1770746245.getSize().reset(140.6833f, 111.0f);
        p1770746245.getPositionOffset().reset(-85.14167f, -55.5f);
        p1770746245.setIsUseAbsolutePosition(true);
        p1770746245.setIsUseAbsoluteSize(true);
        p1770746245.getImage().setIsUseAbsolutePosition(true);
        p1770746245.getImage().setIsUseAbsoluteSize(true);
        p1770746245.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1770746245.resetPosition();
        getSpiritList().add(p1770746245);
    }

    /* access modifiers changed from: package-private */
    public void c1199201345(Context c) {
        Puzzle p1199201345 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1199201345(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1199201345(editor, this);
            }
        };
        p1199201345.setID(1199201345);
        p1199201345.setName("1199201345");
        p1199201345.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1199201345);
        this.Desktop.RandomlyPlaced(p1199201345);
        p1199201345.setTopEdgeType(EdgeType.Flat);
        p1199201345.setBottomEdgeType(EdgeType.Concave);
        p1199201345.setLeftEdgeType(EdgeType.Concave);
        p1199201345.setRightEdgeType(EdgeType.Convex);
        p1199201345.setTopExactPuzzleID(-1);
        p1199201345.setBottomExactPuzzleID(1721237390);
        p1199201345.setLeftExactPuzzleID(405078516);
        p1199201345.setRightExactPuzzleID(556392346);
        p1199201345.getDisplayImage().loadImageFromResource(c, R.drawable.p1199201345h);
        p1199201345.setExactRow(0);
        p1199201345.setExactColumn(9);
        p1199201345.getSize().reset(140.6833f, 111.0f);
        p1199201345.getPositionOffset().reset(-55.54167f, -55.5f);
        p1199201345.setIsUseAbsolutePosition(true);
        p1199201345.setIsUseAbsoluteSize(true);
        p1199201345.getImage().setIsUseAbsolutePosition(true);
        p1199201345.getImage().setIsUseAbsoluteSize(true);
        p1199201345.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1199201345.resetPosition();
        getSpiritList().add(p1199201345);
    }

    /* access modifiers changed from: package-private */
    public void c1721237390(Context c) {
        Puzzle p1721237390 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1721237390(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1721237390(editor, this);
            }
        };
        p1721237390.setID(1721237390);
        p1721237390.setName("1721237390");
        p1721237390.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1721237390);
        this.Desktop.RandomlyPlaced(p1721237390);
        p1721237390.setTopEdgeType(EdgeType.Convex);
        p1721237390.setBottomEdgeType(EdgeType.Convex);
        p1721237390.setLeftEdgeType(EdgeType.Concave);
        p1721237390.setRightEdgeType(EdgeType.Concave);
        p1721237390.setTopExactPuzzleID(1199201345);
        p1721237390.setBottomExactPuzzleID(1438994516);
        p1721237390.setLeftExactPuzzleID(1092698069);
        p1721237390.setRightExactPuzzleID(1044904642);
        p1721237390.getDisplayImage().loadImageFromResource(c, R.drawable.p1721237390h);
        p1721237390.setExactRow(1);
        p1721237390.setExactColumn(9);
        p1721237390.getSize().reset(111.0833f, 170.2f);
        p1721237390.getPositionOffset().reset(-55.54167f, -85.1f);
        p1721237390.setIsUseAbsolutePosition(true);
        p1721237390.setIsUseAbsoluteSize(true);
        p1721237390.getImage().setIsUseAbsolutePosition(true);
        p1721237390.getImage().setIsUseAbsoluteSize(true);
        p1721237390.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1721237390.resetPosition();
        getSpiritList().add(p1721237390);
    }

    /* access modifiers changed from: package-private */
    public void c1438994516(Context c) {
        Puzzle p1438994516 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1438994516(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1438994516(editor, this);
            }
        };
        p1438994516.setID(1438994516);
        p1438994516.setName("1438994516");
        p1438994516.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1438994516);
        this.Desktop.RandomlyPlaced(p1438994516);
        p1438994516.setTopEdgeType(EdgeType.Concave);
        p1438994516.setBottomEdgeType(EdgeType.Convex);
        p1438994516.setLeftEdgeType(EdgeType.Concave);
        p1438994516.setRightEdgeType(EdgeType.Convex);
        p1438994516.setTopExactPuzzleID(1721237390);
        p1438994516.setBottomExactPuzzleID(1924795414);
        p1438994516.setLeftExactPuzzleID(1584592799);
        p1438994516.setRightExactPuzzleID(1390734376);
        p1438994516.getDisplayImage().loadImageFromResource(c, R.drawable.p1438994516h);
        p1438994516.setExactRow(2);
        p1438994516.setExactColumn(9);
        p1438994516.getSize().reset(140.6833f, 140.6f);
        p1438994516.getPositionOffset().reset(-55.54167f, -55.5f);
        p1438994516.setIsUseAbsolutePosition(true);
        p1438994516.setIsUseAbsoluteSize(true);
        p1438994516.getImage().setIsUseAbsolutePosition(true);
        p1438994516.getImage().setIsUseAbsoluteSize(true);
        p1438994516.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1438994516.resetPosition();
        getSpiritList().add(p1438994516);
    }

    /* access modifiers changed from: package-private */
    public void c1924795414(Context c) {
        Puzzle p1924795414 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1924795414(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1924795414(editor, this);
            }
        };
        p1924795414.setID(1924795414);
        p1924795414.setName("1924795414");
        p1924795414.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1924795414);
        this.Desktop.RandomlyPlaced(p1924795414);
        p1924795414.setTopEdgeType(EdgeType.Concave);
        p1924795414.setBottomEdgeType(EdgeType.Concave);
        p1924795414.setLeftEdgeType(EdgeType.Concave);
        p1924795414.setRightEdgeType(EdgeType.Concave);
        p1924795414.setTopExactPuzzleID(1438994516);
        p1924795414.setBottomExactPuzzleID(1527842742);
        p1924795414.setLeftExactPuzzleID(1401309947);
        p1924795414.setRightExactPuzzleID(776822006);
        p1924795414.getDisplayImage().loadImageFromResource(c, R.drawable.p1924795414h);
        p1924795414.setExactRow(3);
        p1924795414.setExactColumn(9);
        p1924795414.getSize().reset(111.0833f, 111.0f);
        p1924795414.getPositionOffset().reset(-55.54167f, -55.5f);
        p1924795414.setIsUseAbsolutePosition(true);
        p1924795414.setIsUseAbsoluteSize(true);
        p1924795414.getImage().setIsUseAbsolutePosition(true);
        p1924795414.getImage().setIsUseAbsoluteSize(true);
        p1924795414.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1924795414.resetPosition();
        getSpiritList().add(p1924795414);
    }

    /* access modifiers changed from: package-private */
    public void c1527842742(Context c) {
        Puzzle p1527842742 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1527842742(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1527842742(editor, this);
            }
        };
        p1527842742.setID(1527842742);
        p1527842742.setName("1527842742");
        p1527842742.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1527842742);
        this.Desktop.RandomlyPlaced(p1527842742);
        p1527842742.setTopEdgeType(EdgeType.Convex);
        p1527842742.setBottomEdgeType(EdgeType.Concave);
        p1527842742.setLeftEdgeType(EdgeType.Convex);
        p1527842742.setRightEdgeType(EdgeType.Concave);
        p1527842742.setTopExactPuzzleID(1924795414);
        p1527842742.setBottomExactPuzzleID(735389401);
        p1527842742.setLeftExactPuzzleID(1967921646);
        p1527842742.setRightExactPuzzleID(666701507);
        p1527842742.getDisplayImage().loadImageFromResource(c, R.drawable.p1527842742h);
        p1527842742.setExactRow(4);
        p1527842742.setExactColumn(9);
        p1527842742.getSize().reset(140.6833f, 140.6f);
        p1527842742.getPositionOffset().reset(-85.14167f, -85.1f);
        p1527842742.setIsUseAbsolutePosition(true);
        p1527842742.setIsUseAbsoluteSize(true);
        p1527842742.getImage().setIsUseAbsolutePosition(true);
        p1527842742.getImage().setIsUseAbsoluteSize(true);
        p1527842742.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1527842742.resetPosition();
        getSpiritList().add(p1527842742);
    }

    /* access modifiers changed from: package-private */
    public void c735389401(Context c) {
        Puzzle p735389401 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load735389401(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save735389401(editor, this);
            }
        };
        p735389401.setID(735389401);
        p735389401.setName("735389401");
        p735389401.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p735389401);
        this.Desktop.RandomlyPlaced(p735389401);
        p735389401.setTopEdgeType(EdgeType.Convex);
        p735389401.setBottomEdgeType(EdgeType.Flat);
        p735389401.setLeftEdgeType(EdgeType.Convex);
        p735389401.setRightEdgeType(EdgeType.Convex);
        p735389401.setTopExactPuzzleID(1527842742);
        p735389401.setBottomExactPuzzleID(-1);
        p735389401.setLeftExactPuzzleID(1770746245);
        p735389401.setRightExactPuzzleID(537266955);
        p735389401.getDisplayImage().loadImageFromResource(c, R.drawable.p735389401h);
        p735389401.setExactRow(5);
        p735389401.setExactColumn(9);
        p735389401.getSize().reset(170.2833f, 140.6f);
        p735389401.getPositionOffset().reset(-85.14167f, -85.1f);
        p735389401.setIsUseAbsolutePosition(true);
        p735389401.setIsUseAbsoluteSize(true);
        p735389401.getImage().setIsUseAbsolutePosition(true);
        p735389401.getImage().setIsUseAbsoluteSize(true);
        p735389401.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p735389401.resetPosition();
        getSpiritList().add(p735389401);
    }

    /* access modifiers changed from: package-private */
    public void c556392346(Context c) {
        Puzzle p556392346 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load556392346(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save556392346(editor, this);
            }
        };
        p556392346.setID(556392346);
        p556392346.setName("556392346");
        p556392346.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p556392346);
        this.Desktop.RandomlyPlaced(p556392346);
        p556392346.setTopEdgeType(EdgeType.Flat);
        p556392346.setBottomEdgeType(EdgeType.Convex);
        p556392346.setLeftEdgeType(EdgeType.Concave);
        p556392346.setRightEdgeType(EdgeType.Concave);
        p556392346.setTopExactPuzzleID(-1);
        p556392346.setBottomExactPuzzleID(1044904642);
        p556392346.setLeftExactPuzzleID(1199201345);
        p556392346.setRightExactPuzzleID(1051015167);
        p556392346.getDisplayImage().loadImageFromResource(c, R.drawable.p556392346h);
        p556392346.setExactRow(0);
        p556392346.setExactColumn(10);
        p556392346.getSize().reset(111.0833f, 140.6f);
        p556392346.getPositionOffset().reset(-55.54167f, -55.5f);
        p556392346.setIsUseAbsolutePosition(true);
        p556392346.setIsUseAbsoluteSize(true);
        p556392346.getImage().setIsUseAbsolutePosition(true);
        p556392346.getImage().setIsUseAbsoluteSize(true);
        p556392346.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p556392346.resetPosition();
        getSpiritList().add(p556392346);
    }

    /* access modifiers changed from: package-private */
    public void c1044904642(Context c) {
        Puzzle p1044904642 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1044904642(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1044904642(editor, this);
            }
        };
        p1044904642.setID(1044904642);
        p1044904642.setName("1044904642");
        p1044904642.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1044904642);
        this.Desktop.RandomlyPlaced(p1044904642);
        p1044904642.setTopEdgeType(EdgeType.Concave);
        p1044904642.setBottomEdgeType(EdgeType.Convex);
        p1044904642.setLeftEdgeType(EdgeType.Convex);
        p1044904642.setRightEdgeType(EdgeType.Concave);
        p1044904642.setTopExactPuzzleID(556392346);
        p1044904642.setBottomExactPuzzleID(1390734376);
        p1044904642.setLeftExactPuzzleID(1721237390);
        p1044904642.setRightExactPuzzleID(1793640042);
        p1044904642.getDisplayImage().loadImageFromResource(c, R.drawable.p1044904642h);
        p1044904642.setExactRow(1);
        p1044904642.setExactColumn(10);
        p1044904642.getSize().reset(140.6833f, 140.6f);
        p1044904642.getPositionOffset().reset(-85.14167f, -55.5f);
        p1044904642.setIsUseAbsolutePosition(true);
        p1044904642.setIsUseAbsoluteSize(true);
        p1044904642.getImage().setIsUseAbsolutePosition(true);
        p1044904642.getImage().setIsUseAbsoluteSize(true);
        p1044904642.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1044904642.resetPosition();
        getSpiritList().add(p1044904642);
    }

    /* access modifiers changed from: package-private */
    public void c1390734376(Context c) {
        Puzzle p1390734376 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1390734376(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1390734376(editor, this);
            }
        };
        p1390734376.setID(1390734376);
        p1390734376.setName("1390734376");
        p1390734376.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1390734376);
        this.Desktop.RandomlyPlaced(p1390734376);
        p1390734376.setTopEdgeType(EdgeType.Concave);
        p1390734376.setBottomEdgeType(EdgeType.Convex);
        p1390734376.setLeftEdgeType(EdgeType.Concave);
        p1390734376.setRightEdgeType(EdgeType.Convex);
        p1390734376.setTopExactPuzzleID(1044904642);
        p1390734376.setBottomExactPuzzleID(776822006);
        p1390734376.setLeftExactPuzzleID(1438994516);
        p1390734376.setRightExactPuzzleID(1558039989);
        p1390734376.getDisplayImage().loadImageFromResource(c, R.drawable.p1390734376h);
        p1390734376.setExactRow(2);
        p1390734376.setExactColumn(10);
        p1390734376.getSize().reset(140.6833f, 140.6f);
        p1390734376.getPositionOffset().reset(-55.54167f, -55.5f);
        p1390734376.setIsUseAbsolutePosition(true);
        p1390734376.setIsUseAbsoluteSize(true);
        p1390734376.getImage().setIsUseAbsolutePosition(true);
        p1390734376.getImage().setIsUseAbsoluteSize(true);
        p1390734376.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1390734376.resetPosition();
        getSpiritList().add(p1390734376);
    }

    /* access modifiers changed from: package-private */
    public void c776822006(Context c) {
        Puzzle p776822006 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load776822006(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save776822006(editor, this);
            }
        };
        p776822006.setID(776822006);
        p776822006.setName("776822006");
        p776822006.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p776822006);
        this.Desktop.RandomlyPlaced(p776822006);
        p776822006.setTopEdgeType(EdgeType.Concave);
        p776822006.setBottomEdgeType(EdgeType.Concave);
        p776822006.setLeftEdgeType(EdgeType.Convex);
        p776822006.setRightEdgeType(EdgeType.Concave);
        p776822006.setTopExactPuzzleID(1390734376);
        p776822006.setBottomExactPuzzleID(666701507);
        p776822006.setLeftExactPuzzleID(1924795414);
        p776822006.setRightExactPuzzleID(269806753);
        p776822006.getDisplayImage().loadImageFromResource(c, R.drawable.p776822006h);
        p776822006.setExactRow(3);
        p776822006.setExactColumn(10);
        p776822006.getSize().reset(140.6833f, 111.0f);
        p776822006.getPositionOffset().reset(-85.14167f, -55.5f);
        p776822006.setIsUseAbsolutePosition(true);
        p776822006.setIsUseAbsoluteSize(true);
        p776822006.getImage().setIsUseAbsolutePosition(true);
        p776822006.getImage().setIsUseAbsoluteSize(true);
        p776822006.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p776822006.resetPosition();
        getSpiritList().add(p776822006);
    }

    /* access modifiers changed from: package-private */
    public void c666701507(Context c) {
        Puzzle p666701507 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load666701507(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save666701507(editor, this);
            }
        };
        p666701507.setID(666701507);
        p666701507.setName("666701507");
        p666701507.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p666701507);
        this.Desktop.RandomlyPlaced(p666701507);
        p666701507.setTopEdgeType(EdgeType.Convex);
        p666701507.setBottomEdgeType(EdgeType.Convex);
        p666701507.setLeftEdgeType(EdgeType.Convex);
        p666701507.setRightEdgeType(EdgeType.Convex);
        p666701507.setTopExactPuzzleID(776822006);
        p666701507.setBottomExactPuzzleID(537266955);
        p666701507.setLeftExactPuzzleID(1527842742);
        p666701507.setRightExactPuzzleID(1870290361);
        p666701507.getDisplayImage().loadImageFromResource(c, R.drawable.p666701507h);
        p666701507.setExactRow(4);
        p666701507.setExactColumn(10);
        p666701507.getSize().reset(170.2833f, 170.2f);
        p666701507.getPositionOffset().reset(-85.14167f, -85.1f);
        p666701507.setIsUseAbsolutePosition(true);
        p666701507.setIsUseAbsoluteSize(true);
        p666701507.getImage().setIsUseAbsolutePosition(true);
        p666701507.getImage().setIsUseAbsoluteSize(true);
        p666701507.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p666701507.resetPosition();
        getSpiritList().add(p666701507);
    }

    /* access modifiers changed from: package-private */
    public void c537266955(Context c) {
        Puzzle p537266955 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load537266955(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save537266955(editor, this);
            }
        };
        p537266955.setID(537266955);
        p537266955.setName("537266955");
        p537266955.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p537266955);
        this.Desktop.RandomlyPlaced(p537266955);
        p537266955.setTopEdgeType(EdgeType.Concave);
        p537266955.setBottomEdgeType(EdgeType.Flat);
        p537266955.setLeftEdgeType(EdgeType.Concave);
        p537266955.setRightEdgeType(EdgeType.Convex);
        p537266955.setTopExactPuzzleID(666701507);
        p537266955.setBottomExactPuzzleID(-1);
        p537266955.setLeftExactPuzzleID(735389401);
        p537266955.setRightExactPuzzleID(1240996084);
        p537266955.getDisplayImage().loadImageFromResource(c, R.drawable.p537266955h);
        p537266955.setExactRow(5);
        p537266955.setExactColumn(10);
        p537266955.getSize().reset(140.6833f, 111.0f);
        p537266955.getPositionOffset().reset(-55.54167f, -55.5f);
        p537266955.setIsUseAbsolutePosition(true);
        p537266955.setIsUseAbsoluteSize(true);
        p537266955.getImage().setIsUseAbsolutePosition(true);
        p537266955.getImage().setIsUseAbsoluteSize(true);
        p537266955.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p537266955.resetPosition();
        getSpiritList().add(p537266955);
    }

    /* access modifiers changed from: package-private */
    public void c1051015167(Context c) {
        Puzzle p1051015167 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1051015167(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1051015167(editor, this);
            }
        };
        p1051015167.setID(1051015167);
        p1051015167.setName("1051015167");
        p1051015167.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1051015167);
        this.Desktop.RandomlyPlaced(p1051015167);
        p1051015167.setTopEdgeType(EdgeType.Flat);
        p1051015167.setBottomEdgeType(EdgeType.Convex);
        p1051015167.setLeftEdgeType(EdgeType.Convex);
        p1051015167.setRightEdgeType(EdgeType.Flat);
        p1051015167.setTopExactPuzzleID(-1);
        p1051015167.setBottomExactPuzzleID(1793640042);
        p1051015167.setLeftExactPuzzleID(556392346);
        p1051015167.setRightExactPuzzleID(-1);
        p1051015167.getDisplayImage().loadImageFromResource(c, R.drawable.p1051015167h);
        p1051015167.setExactRow(0);
        p1051015167.setExactColumn(11);
        p1051015167.getSize().reset(140.6833f, 140.6f);
        p1051015167.getPositionOffset().reset(-85.14167f, -55.5f);
        p1051015167.setIsUseAbsolutePosition(true);
        p1051015167.setIsUseAbsoluteSize(true);
        p1051015167.getImage().setIsUseAbsolutePosition(true);
        p1051015167.getImage().setIsUseAbsoluteSize(true);
        p1051015167.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1051015167.resetPosition();
        getSpiritList().add(p1051015167);
    }

    /* access modifiers changed from: package-private */
    public void c1793640042(Context c) {
        Puzzle p1793640042 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1793640042(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1793640042(editor, this);
            }
        };
        p1793640042.setID(1793640042);
        p1793640042.setName("1793640042");
        p1793640042.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1793640042);
        this.Desktop.RandomlyPlaced(p1793640042);
        p1793640042.setTopEdgeType(EdgeType.Concave);
        p1793640042.setBottomEdgeType(EdgeType.Convex);
        p1793640042.setLeftEdgeType(EdgeType.Convex);
        p1793640042.setRightEdgeType(EdgeType.Flat);
        p1793640042.setTopExactPuzzleID(1051015167);
        p1793640042.setBottomExactPuzzleID(1558039989);
        p1793640042.setLeftExactPuzzleID(1044904642);
        p1793640042.setRightExactPuzzleID(-1);
        p1793640042.getDisplayImage().loadImageFromResource(c, R.drawable.p1793640042h);
        p1793640042.setExactRow(1);
        p1793640042.setExactColumn(11);
        p1793640042.getSize().reset(140.6833f, 140.6f);
        p1793640042.getPositionOffset().reset(-85.14167f, -55.5f);
        p1793640042.setIsUseAbsolutePosition(true);
        p1793640042.setIsUseAbsoluteSize(true);
        p1793640042.getImage().setIsUseAbsolutePosition(true);
        p1793640042.getImage().setIsUseAbsoluteSize(true);
        p1793640042.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1793640042.resetPosition();
        getSpiritList().add(p1793640042);
    }

    /* access modifiers changed from: package-private */
    public void c1558039989(Context c) {
        Puzzle p1558039989 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1558039989(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1558039989(editor, this);
            }
        };
        p1558039989.setID(1558039989);
        p1558039989.setName("1558039989");
        p1558039989.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1558039989);
        this.Desktop.RandomlyPlaced(p1558039989);
        p1558039989.setTopEdgeType(EdgeType.Concave);
        p1558039989.setBottomEdgeType(EdgeType.Concave);
        p1558039989.setLeftEdgeType(EdgeType.Concave);
        p1558039989.setRightEdgeType(EdgeType.Flat);
        p1558039989.setTopExactPuzzleID(1793640042);
        p1558039989.setBottomExactPuzzleID(269806753);
        p1558039989.setLeftExactPuzzleID(1390734376);
        p1558039989.setRightExactPuzzleID(-1);
        p1558039989.getDisplayImage().loadImageFromResource(c, R.drawable.p1558039989h);
        p1558039989.setExactRow(2);
        p1558039989.setExactColumn(11);
        p1558039989.getSize().reset(111.0833f, 111.0f);
        p1558039989.getPositionOffset().reset(-55.54167f, -55.5f);
        p1558039989.setIsUseAbsolutePosition(true);
        p1558039989.setIsUseAbsoluteSize(true);
        p1558039989.getImage().setIsUseAbsolutePosition(true);
        p1558039989.getImage().setIsUseAbsoluteSize(true);
        p1558039989.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1558039989.resetPosition();
        getSpiritList().add(p1558039989);
    }

    /* access modifiers changed from: package-private */
    public void c269806753(Context c) {
        Puzzle p269806753 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load269806753(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save269806753(editor, this);
            }
        };
        p269806753.setID(269806753);
        p269806753.setName("269806753");
        p269806753.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p269806753);
        this.Desktop.RandomlyPlaced(p269806753);
        p269806753.setTopEdgeType(EdgeType.Convex);
        p269806753.setBottomEdgeType(EdgeType.Convex);
        p269806753.setLeftEdgeType(EdgeType.Convex);
        p269806753.setRightEdgeType(EdgeType.Flat);
        p269806753.setTopExactPuzzleID(1558039989);
        p269806753.setBottomExactPuzzleID(1870290361);
        p269806753.setLeftExactPuzzleID(776822006);
        p269806753.setRightExactPuzzleID(-1);
        p269806753.getDisplayImage().loadImageFromResource(c, R.drawable.p269806753h);
        p269806753.setExactRow(3);
        p269806753.setExactColumn(11);
        p269806753.getSize().reset(140.6833f, 170.2f);
        p269806753.getPositionOffset().reset(-85.14167f, -85.1f);
        p269806753.setIsUseAbsolutePosition(true);
        p269806753.setIsUseAbsoluteSize(true);
        p269806753.getImage().setIsUseAbsolutePosition(true);
        p269806753.getImage().setIsUseAbsoluteSize(true);
        p269806753.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p269806753.resetPosition();
        getSpiritList().add(p269806753);
    }

    /* access modifiers changed from: package-private */
    public void c1870290361(Context c) {
        Puzzle p1870290361 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1870290361(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1870290361(editor, this);
            }
        };
        p1870290361.setID(1870290361);
        p1870290361.setName("1870290361");
        p1870290361.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1870290361);
        this.Desktop.RandomlyPlaced(p1870290361);
        p1870290361.setTopEdgeType(EdgeType.Concave);
        p1870290361.setBottomEdgeType(EdgeType.Concave);
        p1870290361.setLeftEdgeType(EdgeType.Concave);
        p1870290361.setRightEdgeType(EdgeType.Flat);
        p1870290361.setTopExactPuzzleID(269806753);
        p1870290361.setBottomExactPuzzleID(1240996084);
        p1870290361.setLeftExactPuzzleID(666701507);
        p1870290361.setRightExactPuzzleID(-1);
        p1870290361.getDisplayImage().loadImageFromResource(c, R.drawable.p1870290361h);
        p1870290361.setExactRow(4);
        p1870290361.setExactColumn(11);
        p1870290361.getSize().reset(111.0833f, 111.0f);
        p1870290361.getPositionOffset().reset(-55.54167f, -55.5f);
        p1870290361.setIsUseAbsolutePosition(true);
        p1870290361.setIsUseAbsoluteSize(true);
        p1870290361.getImage().setIsUseAbsolutePosition(true);
        p1870290361.getImage().setIsUseAbsoluteSize(true);
        p1870290361.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1870290361.resetPosition();
        getSpiritList().add(p1870290361);
    }

    /* access modifiers changed from: package-private */
    public void c1240996084(Context c) {
        Puzzle p1240996084 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1240996084(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1240996084(editor, this);
            }
        };
        p1240996084.setID(1240996084);
        p1240996084.setName("1240996084");
        p1240996084.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1240996084);
        this.Desktop.RandomlyPlaced(p1240996084);
        p1240996084.setTopEdgeType(EdgeType.Convex);
        p1240996084.setBottomEdgeType(EdgeType.Flat);
        p1240996084.setLeftEdgeType(EdgeType.Concave);
        p1240996084.setRightEdgeType(EdgeType.Flat);
        p1240996084.setTopExactPuzzleID(1870290361);
        p1240996084.setBottomExactPuzzleID(-1);
        p1240996084.setLeftExactPuzzleID(537266955);
        p1240996084.setRightExactPuzzleID(-1);
        p1240996084.getDisplayImage().loadImageFromResource(c, R.drawable.p1240996084h);
        p1240996084.setExactRow(5);
        p1240996084.setExactColumn(11);
        p1240996084.getSize().reset(111.0833f, 140.6f);
        p1240996084.getPositionOffset().reset(-55.54167f, -85.1f);
        p1240996084.setIsUseAbsolutePosition(true);
        p1240996084.setIsUseAbsoluteSize(true);
        p1240996084.getImage().setIsUseAbsolutePosition(true);
        p1240996084.getImage().setIsUseAbsoluteSize(true);
        p1240996084.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1240996084.resetPosition();
        getSpiritList().add(p1240996084);
    }
}
