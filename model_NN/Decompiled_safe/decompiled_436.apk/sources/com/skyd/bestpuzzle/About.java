package com.skyd.bestpuzzle;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.skyd.bestpuzzle.n1622.R;
import com.skyd.core.android.CommonActivity;

public class About extends CommonActivity {
    private Button _Button01 = null;

    public Button getButton01() {
        if (this._Button01 == null) {
            this._Button01 = (Button) findViewById(R.id.Button01);
        }
        return this._Button01;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.about);
        getButton01().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                About.this.finish();
            }
        });
    }
}
