package com.skyd.core.android.game.crosswisewar;

import com.skyd.core.game.crosswisewar.IBuff;
import com.skyd.core.game.crosswisewar.IEntity;
import com.skyd.core.game.crosswisewar.IWarrior;

public abstract class Buff extends Warrior implements IBuff {
    private long _CreateTime;
    private int _KeepTime;

    public Buff() {
        this._CreateTime = 0;
        this._KeepTime = 0;
        this._CreateTime = getCurrentFrameNumber();
    }

    public long getCreateTime() {
        return this._CreateTime;
    }

    public int getKeepTime() {
        return this._KeepTime;
    }

    public void setKeepTime(int value) {
        this._KeepTime = value;
    }

    public void setKeepTimeToDefault() {
        setKeepTime(0);
    }

    public void remove() {
        IEntity buff = getBuff();
        getBuffTarget().setBuff(buff);
        if (buff != null) {
            buff.setBuffTarget(getBuffTarget());
        }
    }

    public void setToTarget(IWarrior value) {
        setBuffTarget(value.getBuffed());
        value.getBuffed().setBuff(this);
    }

    public boolean getIsCanMoveAttack() {
        return ((IWarrior) getBuffTarget()).getIsCanMoveAttack();
    }

    public int getNeedPay() {
        return ((IWarrior) getBuffTarget()).getNeedPay();
    }

    /* access modifiers changed from: protected */
    public void updateSelf() {
        super.updateSelf();
        if (getKeepTime() > 0 && getCurrentFrameNumber() - getCreateTime() >= ((long) getKeepTime())) {
            remove();
        }
    }
}
