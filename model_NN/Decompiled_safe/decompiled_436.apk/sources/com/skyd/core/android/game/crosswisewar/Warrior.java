package com.skyd.core.android.game.crosswisewar;

import com.skyd.core.game.crosswisewar.ICooling;
import com.skyd.core.game.crosswisewar.IEntity;
import com.skyd.core.game.crosswisewar.IWarrior;

public abstract class Warrior extends Entity implements IWarrior {
    private int _Speed = 1;

    public boolean move() {
        getPositionInScene().setX(getPositionInScene().getX() + ((float) (getNation().getIsRighteous() ? getSpeed() : 0 - getSpeed())));
        return true;
    }

    /* access modifiers changed from: protected */
    public void updateSelf() {
        super.updateSelf();
        IEntity[] target = getEncounterEntity();
        if (target == null || target.length == 0) {
            move();
            return;
        }
        attack(target);
        if (getIsCanMoveAttack()) {
            move();
        }
    }

    public static boolean hitTest(float position1, int width1, float position2, int width2) {
        return Math.abs(position1 - position2) <= ((float) Math.max(width1, width2));
    }

    public ICooling getCooling() {
        return CoolingMaster.getCooling(getClass());
    }

    public int getSpeed() {
        return this._Speed;
    }

    public void setSpeed(int value) {
        this._Speed = value;
    }

    public void setSpeedToDefault() {
        setSpeed(1);
    }
}
