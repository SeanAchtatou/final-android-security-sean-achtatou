package com.skyd.core.android.game;

import android.graphics.Canvas;
import android.graphics.Matrix;
import java.util.ArrayList;
import java.util.Iterator;

public abstract class GameSpirit extends GameNodeObject implements IGameVerticalCapsuleHitTest, IGameRectHitTest, ICurrentFrameNumberSupport {
    private float _Level = 0.0f;
    private ArrayList<OnLevelChangedListener> _LevelChangedListenerList = null;
    private String _Name = null;

    public interface OnLevelChangedListener {
        void OnLevelChangedEvent(Object obj, float f);
    }

    public String getName() {
        return this._Name;
    }

    public void setName(String value) {
        this._Name = value;
    }

    public void setNameToDefault() {
        setName(null);
    }

    public float getLevel() {
        return this._Level;
    }

    public void setLevel(float value) {
        this._Level = value;
        onLevelChanged(value);
    }

    public void setLevelAbove(GameSpirit target) {
        setLevel(target.getLevel() + 1.0E-8f);
    }

    public void setLevelBelow(GameSpirit target) {
        setLevel(target.getLevel() - 1.0E-8f);
    }

    public void setLevelAbove(float target) {
        setLevel(1.0E-8f + target);
    }

    public void setLevelBelow(float target) {
        setLevel(target - 1.0E-8f);
    }

    public void setLevelToDefault() {
        setLevel(0.0f);
    }

    public boolean addOnLevelChangedListener(OnLevelChangedListener listener) {
        if (this._LevelChangedListenerList == null) {
            this._LevelChangedListenerList = new ArrayList<>();
        } else if (this._LevelChangedListenerList.contains(listener)) {
            return false;
        }
        this._LevelChangedListenerList.add(listener);
        return true;
    }

    public boolean removeOnLevelChangedListener(OnLevelChangedListener listener) {
        if (this._LevelChangedListenerList == null || !this._LevelChangedListenerList.contains(listener)) {
            return false;
        }
        this._LevelChangedListenerList.remove(listener);
        return true;
    }

    public void clearOnLevelChangedListeners() {
        if (this._LevelChangedListenerList != null) {
            this._LevelChangedListenerList.clear();
        }
    }

    /* access modifiers changed from: protected */
    public void onLevelChanged(float value) {
        if (this._LevelChangedListenerList != null) {
            Iterator<OnLevelChangedListener> it = this._LevelChangedListenerList.iterator();
            while (it.hasNext()) {
                it.next().OnLevelChangedEvent(this, value);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void operateCanvasForParentAbsoluteSize(Canvas c) {
        if (!(getParent() instanceof GameRootObject)) {
            super.operateCanvasForParentAbsoluteSize(c);
        }
    }

    /* access modifiers changed from: protected */
    public void operateMatrixForParentAbsoluteSize(Matrix m) {
        if (!(getParent() instanceof GameRootObject)) {
            super.operateMatrixForParentAbsoluteSize(m);
        }
    }

    public float getVerticalCapsuleHitTestHeight() {
        float v = getSize().getY();
        return Math.max(v / 2.0f, v - getVerticalCapsuleHitTestRadius());
    }

    public float getVerticalCapsuleHitTestRadius() {
        return getSize().getX() / 2.0f;
    }

    public float getVerticalCapsuleHitTestX() {
        return getPosition().getX() + getPositionOffset().getX() + (getSize().getX() / 2.0f);
    }

    public float getVerticalCapsuleHitTestY() {
        float v = getPosition().getY() + getPositionOffset().getY();
        return Math.min((getSize().getY() / 2.0f) + v, getVerticalCapsuleHitTestRadius() + v);
    }

    public float getRectHitTestBottom() {
        return getRectHitTestTop() + getSize().getY();
    }

    public float getRectHitTestLeft() {
        return getPosition().getX() + getPositionOffset().getX();
    }

    public float getRectHitTestRight() {
        return getRectHitTestLeft() + getSize().getX();
    }

    public float getRectHitTestTop() {
        return getPosition().getY() + getPositionOffset().getY();
    }

    public long getCurrentFrameNumber() {
        return ((ICurrentFrameNumberSupport) getParent()).getCurrentFrameNumber();
    }
}
