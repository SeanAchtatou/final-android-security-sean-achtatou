package com.skyd.core.android.game;

import android.os.Bundle;

public class GameException extends Exception {
    private static final long serialVersionUID = -87766899045279275L;
    private Bundle _Args = null;
    private Object _Sender = null;

    public GameException(Object sender, String message, Bundle args) {
        super(message);
        setSender(sender);
        setArgs(args);
    }

    public Object getSender() {
        return this._Sender;
    }

    public void setSender(Object value) {
        this._Sender = value;
    }

    public void setSenderToDefault() {
        setSender(null);
    }

    public Bundle getArgs() {
        return this._Args;
    }

    public void setArgs(Bundle value) {
        this._Args = value;
    }

    public void setArgsToDefault() {
        setArgs(null);
    }
}
