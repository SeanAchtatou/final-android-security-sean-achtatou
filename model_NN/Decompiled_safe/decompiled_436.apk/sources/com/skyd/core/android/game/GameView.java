package com.skyd.core.android.game;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import com.skyd.core.vector.Vector2DF;
import java.util.Date;

public abstract class GameView extends SurfaceView implements SurfaceHolder.Callback, IGameDisplayBase {
    private int _FPS = 60;
    private int _FPSDrawColor = -65536;
    private Vector2DF _FPSDrawPosition = new Vector2DF(10.0f, 10.0f);
    private int _FPSDrawTextSize = 22;
    private Boolean _IsFirstShow = true;
    private Boolean _IsInitialized = false;
    /* access modifiers changed from: private */
    public boolean _IsShowFPS = false;
    /* access modifiers changed from: private */
    public boolean _Runnable = false;
    int fps;
    int fpscount = 0;
    long fpsstart;
    int frameRate;
    SurfaceHolder holder = getHolder();
    PaintThread pThread;
    UpdateThread uThread;

    /* access modifiers changed from: protected */
    public abstract void PaintInThread(Canvas canvas);

    /* access modifiers changed from: protected */
    public abstract void UpdateInThread();

    public abstract void init(Boolean bool);

    public Boolean getIsFirstShow() {
        return this._IsFirstShow;
    }

    public boolean getRunnable() {
        return this._Runnable;
    }

    public void setRunnable(boolean value) {
        this._Runnable = value;
    }

    public void setRunnableToDefault() {
        setRunnable(false);
    }

    public int getFPS() {
        return this._FPS;
    }

    public void setFPS(int value) {
        this._FPS = value;
        updateFrameRate(value);
    }

    public void setFPSToDefault() {
        setFPS(60);
    }

    /* access modifiers changed from: protected */
    public void updateFrameRate(int FPS) {
        this.frameRate = 1000 / FPS;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    /* access modifiers changed from: protected */
    public long calculateSleepTime(Date beginTime) {
        return Math.max(0L, ((long) this.frameRate) - (new Date().getTime() - beginTime.getTime()));
    }

    public GameView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.holder.addCallback(this);
        setFPSToDefault();
    }

    public Boolean getIsInitialized() {
        return this._IsInitialized;
    }

    public void setIsInitialized(Boolean value) {
        this._IsInitialized = value;
    }

    public void setIsInitializedToDefault() {
        setIsInitialized(false);
    }

    public void surfaceCreated(SurfaceHolder arg0) {
        this.uThread = new UpdateThread();
        this.pThread = new PaintThread();
        setRunnable(true);
        this.uThread.start();
        this.pThread.start();
        init(this._IsFirstShow);
        this._IsFirstShow = false;
        setIsInitialized(true);
    }

    public void surfaceDestroyed(SurfaceHolder arg0) {
        setRunnable(false);
        setIsInitialized(false);
    }

    public void surfaceChanged(SurfaceHolder holder2, int format, int width, int height) {
    }

    class UpdateThread extends Thread {
        Date beginTime;

        UpdateThread() {
        }

        public void run() {
            while (GameView.this._Runnable) {
                this.beginTime = new Date();
                GameView.this.UpdateInThread();
                try {
                    Thread.sleep(GameView.this.calculateSleepTime(this.beginTime));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    class PaintThread extends Thread {
        Date beginTime;

        PaintThread() {
        }

        public void run() {
            while (GameView.this._Runnable) {
                this.beginTime = new Date();
                Canvas c = GameView.this.getCanvas(GameView.this.holder);
                GameView.this.PaintInThread(c);
                if (GameView.this._IsShowFPS) {
                    GameView.this.processingFPS(c, this.beginTime);
                }
                if (c != null) {
                    GameView.this.holder.unlockCanvasAndPost(c);
                }
                try {
                    Thread.sleep(GameView.this.calculateSleepTime(this.beginTime));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public Canvas getCanvas(SurfaceHolder holder2) {
        return holder2.lockCanvas();
    }

    public void processingFPS(Canvas c, Date beginTime) {
        if (this.fpscount == 0) {
            this.fpsstart = beginTime.getTime();
        }
        this.fpscount++;
        if (beginTime.getTime() - this.fpsstart > 1000) {
            this.fps = this.fpscount - 1;
            this.fpscount = 0;
        }
        Paint paint = new Paint();
        paint.setColor(getFPSDrawColor());
        paint.setTextSize((float) getFPSDrawTextSize());
        c.drawText("FPS:" + this.fps, getFPSDrawPosition().getX(), getFPSDrawPosition().getY(), paint);
    }

    public boolean getIsShowFPS() {
        return this._IsShowFPS;
    }

    private void setIsShowFPS(boolean value) {
        this._IsShowFPS = value;
    }

    public void showFPS(int x, int y, int size, int color) {
        setFPSDrawTextSize(size);
        setFPSDrawColor(color);
        showFPS(x, y);
    }

    public void showFPS(int x, int y) {
        getFPSDrawPosition().reset((float) x, (float) y);
        showFPS();
    }

    public void showFPS() {
        setIsShowFPS(true);
    }

    public void hideFPS() {
        setIsShowFPS(false);
    }

    public Vector2DF getFPSDrawPosition() {
        return this._FPSDrawPosition;
    }

    public int getFPSDrawTextSize() {
        return this._FPSDrawTextSize;
    }

    public void setFPSDrawTextSize(int value) {
        this._FPSDrawTextSize = value;
    }

    public void setFPSDrawTextSizeToDefault() {
        setFPSDrawTextSize(22);
    }

    public int getFPSDrawColor() {
        return this._FPSDrawColor;
    }

    public void setFPSDrawColor(int value) {
        this._FPSDrawColor = value;
    }

    public void setFPSDrawColorToDefault() {
        setFPSDrawColor(-65536);
    }
}
