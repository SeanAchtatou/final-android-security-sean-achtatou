package com.skyd.core.android.game;

public class GameAnimationFrame extends GameNodeObject implements IGameImageHolder {
    private int _Duration = 20;
    private GameImage _Image = null;

    public GameAnimationFrame() {
    }

    public GameAnimationFrame(GameImage img) {
        try {
            img.setParent(this);
        } catch (GameException e) {
            e.printStackTrace();
        }
        setImage(img);
    }

    public int getDuration() {
        return this._Duration;
    }

    public void setDuration(int value) {
        this._Duration = value;
    }

    public void setDurationToDefault() {
        setDuration(20);
    }

    public GameImage getImage() {
        return this._Image;
    }

    public void setImage(GameImage value) {
        try {
            value.setParent(this);
        } catch (GameException e) {
            e.printStackTrace();
        }
        this._Image = value;
    }

    public void setImageToDefault() {
        if (this._Image != null) {
            try {
                this._Image.setParentToDefault();
            } catch (GameException e) {
                e.printStackTrace();
            }
            setImage(null);
        }
    }

    public GameObject getDisplayContentChild() {
        return getImage();
    }

    public GameImage getDisplayImage() {
        return getImage();
    }
}
