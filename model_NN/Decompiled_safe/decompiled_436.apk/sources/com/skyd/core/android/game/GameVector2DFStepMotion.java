package com.skyd.core.android.game;

import com.skyd.core.vector.Vector2DF;

public class GameVector2DFStepMotion extends GameMotion {
    private float _StepLength = 0.0f;
    private Vector2DF _TargetPosition = null;
    private float _Tolerance = 0.01f;
    private IGameReadonlyValueDuct<GameObject, Vector2DF> _ValueDuct = null;

    public GameVector2DFStepMotion(float stepLength, Vector2DF targetPosition, IGameReadonlyValueDuct<GameObject, Vector2DF> valueDuct) {
        setTargetPosition(targetPosition);
        setStepLength(stepLength);
        setValueDuct(valueDuct);
    }

    public IGameReadonlyValueDuct<GameObject, Vector2DF> getValueDuct() {
        return this._ValueDuct;
    }

    public void setValueDuct(IGameReadonlyValueDuct<GameObject, Vector2DF> value) {
        this._ValueDuct = value;
    }

    public void setValueDuctToDefault() {
        setValueDuct(null);
    }

    /* access modifiers changed from: protected */
    public void restart(GameObject obj) {
    }

    public float getTolerance() {
        return this._Tolerance;
    }

    public void setTolerance(float value) {
        this._Tolerance = value;
    }

    public void setToleranceToDefault() {
        setTolerance(0.01f);
    }

    /* access modifiers changed from: protected */
    public void updateSelf(GameObject obj) {
    }

    public float getStepLength() {
        return this._StepLength;
    }

    public void setStepLength(float value) {
        this._StepLength = value;
    }

    public void setStepLengthToDefault() {
        setStepLength(0.0f);
    }

    /* access modifiers changed from: protected */
    public boolean getIsComplete(GameObject obj) {
        return checkComplete(getValueDuct().getValueFrom(obj));
    }

    /* access modifiers changed from: protected */
    public void updateTarget(GameObject obj) {
        changePosition(getValueDuct().getValueFrom(obj));
    }

    public Vector2DF getTargetPosition() {
        return this._TargetPosition;
    }

    public void setTargetPosition(Vector2DF value) {
        this._TargetPosition = value;
    }

    public void setTargetPositionToDefault() {
        setTargetPosition(null);
    }

    /* access modifiers changed from: protected */
    public boolean checkComplete(Vector2DF nowPosition) {
        return nowPosition.minusNew(getTargetPosition()).getLength() <= getTolerance();
    }

    /* access modifiers changed from: protected */
    public void changePosition(Vector2DF nowPosition) {
        Vector2DF v = getRelativePosition(nowPosition);
        if (v.getLength() <= getStepLength()) {
            nowPosition.resetWith(getTargetPosition());
        } else {
            nowPosition.plus(getStepVector(v.getAngle()));
        }
    }

    /* access modifiers changed from: protected */
    public Vector2DF getStepVector(float angle) {
        Vector2DF n = new Vector2DF(0.0f, 0.0f);
        n.setLength(getStepLength());
        n.setAngle(angle);
        return n;
    }

    /* access modifiers changed from: protected */
    public Vector2DF getRelativePosition(Vector2DF nowPosition) {
        return getTargetPosition().minusNew(nowPosition);
    }
}
