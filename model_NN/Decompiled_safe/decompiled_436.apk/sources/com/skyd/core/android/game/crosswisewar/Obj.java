package com.skyd.core.android.game.crosswisewar;

import android.graphics.Canvas;
import com.skyd.core.android.game.GameMultifunctionSpirit;
import com.skyd.core.game.crosswisewar.INation;
import com.skyd.core.game.crosswisewar.IObj;
import com.skyd.core.game.crosswisewar.IScene;
import com.skyd.core.vector.Vector2DF;

public abstract class Obj extends GameMultifunctionSpirit implements IObj {
    private INation _Nation = null;
    private IScene _ParentScene = null;

    public void addToScene(Scene scene) {
        scene.getSpiritList().add(this);
        setParentScene(scene);
    }

    public void removeFromScene() {
        ((Scene) getParentScene()).getSpiritList().remove(this);
        setParentSceneToDefault();
    }

    public INation getNation() {
        return this._Nation;
    }

    public void setNation(INation value) {
        this._Nation = value;
    }

    public void setNationToDefault() {
        setNation(null);
    }

    public IScene getParentScene() {
        return this._ParentScene;
    }

    public void setParentScene(IScene value) {
        this._ParentScene = value;
    }

    public void setParentSceneToDefault() {
        setParentScene(null);
    }

    public Vector2DF getPositionInScene() {
        return getPosition();
    }

    public void setPositionInScene(Vector2DF value) {
        setPosition(value);
    }

    /* access modifiers changed from: protected */
    public void operateCanvas(Canvas c) {
        super.operateCanvas(c);
        if (!getNation().getIsRighteous()) {
            c.scale(-1.0f, 1.0f);
        }
    }
}
