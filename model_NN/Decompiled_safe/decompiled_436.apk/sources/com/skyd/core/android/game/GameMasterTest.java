package com.skyd.core.android.game;

import com.skyd.core.vector.Vector2DF;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class GameMasterTest {
    private IGameVerticalCapsuleHitTest IGameVerticalCapsuleHitTestObj1;

    @Before
    public void setUp() throws Exception {
        this.IGameVerticalCapsuleHitTestObj1 = new IGameVerticalCapsuleHitTest() {
            public float getVerticalCapsuleHitTestX() {
                return 20.0f;
            }

            public float getVerticalCapsuleHitTestY() {
                return 30.0f;
            }

            public float getVerticalCapsuleHitTestRadius() {
                return 10.0f;
            }

            public float getVerticalCapsuleHitTestHeight() {
                return 50.0f;
            }
        };
    }

    @Test
    public void testGetVerticalCapsuleLeftBound() {
        boolean z;
        boolean z2;
        float r = this.IGameVerticalCapsuleHitTestObj1.getVerticalCapsuleHitTestRadius();
        float x = this.IGameVerticalCapsuleHitTestObj1.getVerticalCapsuleHitTestX();
        float t = this.IGameVerticalCapsuleHitTestObj1.getVerticalCapsuleHitTestY();
        float b = t + this.IGameVerticalCapsuleHitTestObj1.getVerticalCapsuleHitTestHeight();
        Assert.assertEquals((double) (x - r), (double) GameMaster.getVerticalCapsuleLeftBound(this.IGameVerticalCapsuleHitTestObj1, 30.0f), 9.999999747378752E-5d);
        Assert.assertEquals((double) (x - r), (double) GameMaster.getVerticalCapsuleLeftBound(this.IGameVerticalCapsuleHitTestObj1, 50.0f), 9.999999747378752E-5d);
        Assert.assertEquals((double) (x - r), (double) GameMaster.getVerticalCapsuleLeftBound(this.IGameVerticalCapsuleHitTestObj1, 80.0f), 9.999999747378752E-5d);
        Assert.assertEquals((double) x, (double) GameMaster.getVerticalCapsuleLeftBound(this.IGameVerticalCapsuleHitTestObj1, t - r), 9.999999747378752E-5d);
        Assert.assertEquals((double) x, (double) GameMaster.getVerticalCapsuleLeftBound(this.IGameVerticalCapsuleHitTestObj1, b + r), 9.999999747378752E-5d);
        float v = GameMaster.getVerticalCapsuleLeftBound(this.IGameVerticalCapsuleHitTestObj1, 24.0f);
        Assert.assertEquals((double) (x - 8.0f), (double) v, 9.999999747378752E-5d);
        if (GameMaster.getVerticalCapsuleLeftBound(this.IGameVerticalCapsuleHitTestObj1, 22.0f) > v) {
            z = true;
        } else {
            z = false;
        }
        Assert.assertTrue(z);
        if (GameMaster.getVerticalCapsuleLeftBound(this.IGameVerticalCapsuleHitTestObj1, 26.0f) < v) {
            z2 = true;
        } else {
            z2 = false;
        }
        Assert.assertTrue(z2);
        int c = 0;
        while (true) {
            int c2 = c;
            c = c2 + 1;
            if (c2 < 5000) {
                GameMaster.getVerticalCapsuleLeftBound(this.IGameVerticalCapsuleHitTestObj1, getRandom(1000.0f));
            } else {
                return;
            }
        }
    }

    @Test
    public void testGetVerticalCapsuleRightBound() {
        boolean z;
        boolean z2;
        float r = this.IGameVerticalCapsuleHitTestObj1.getVerticalCapsuleHitTestRadius();
        float x = this.IGameVerticalCapsuleHitTestObj1.getVerticalCapsuleHitTestX();
        float t = this.IGameVerticalCapsuleHitTestObj1.getVerticalCapsuleHitTestY();
        float b = t + this.IGameVerticalCapsuleHitTestObj1.getVerticalCapsuleHitTestHeight();
        Assert.assertEquals((double) (x + r), (double) GameMaster.getVerticalCapsuleRightBound(this.IGameVerticalCapsuleHitTestObj1, 30.0f), 9.999999747378752E-5d);
        Assert.assertEquals((double) (x + r), (double) GameMaster.getVerticalCapsuleRightBound(this.IGameVerticalCapsuleHitTestObj1, 50.0f), 9.999999747378752E-5d);
        Assert.assertEquals((double) (x + r), (double) GameMaster.getVerticalCapsuleRightBound(this.IGameVerticalCapsuleHitTestObj1, 80.0f), 9.999999747378752E-5d);
        Assert.assertEquals((double) x, (double) GameMaster.getVerticalCapsuleRightBound(this.IGameVerticalCapsuleHitTestObj1, t - r), 9.999999747378752E-5d);
        Assert.assertEquals((double) x, (double) GameMaster.getVerticalCapsuleRightBound(this.IGameVerticalCapsuleHitTestObj1, b + r), 9.999999747378752E-5d);
        float v = GameMaster.getVerticalCapsuleRightBound(this.IGameVerticalCapsuleHitTestObj1, 24.0f);
        Assert.assertEquals((double) (8.0f + x), (double) v, 9.999999747378752E-5d);
        if (GameMaster.getVerticalCapsuleRightBound(this.IGameVerticalCapsuleHitTestObj1, 22.0f) < v) {
            z = true;
        } else {
            z = false;
        }
        Assert.assertTrue(z);
        if (GameMaster.getVerticalCapsuleRightBound(this.IGameVerticalCapsuleHitTestObj1, 26.0f) > v) {
            z2 = true;
        } else {
            z2 = false;
        }
        Assert.assertTrue(z2);
        int c = 0;
        while (true) {
            int c2 = c;
            c = c2 + 1;
            if (c2 < 5000) {
                GameMaster.getVerticalCapsuleRightBound(this.IGameVerticalCapsuleHitTestObj1, getRandom(1000.0f));
            } else {
                return;
            }
        }
    }

    @Test
    public void testGetVerticalCapsuleTopBound() {
        boolean z;
        boolean z2;
        float r = this.IGameVerticalCapsuleHitTestObj1.getVerticalCapsuleHitTestRadius();
        float x = this.IGameVerticalCapsuleHitTestObj1.getVerticalCapsuleHitTestX();
        float t = this.IGameVerticalCapsuleHitTestObj1.getVerticalCapsuleHitTestY();
        Assert.assertEquals((double) (t - r), (double) GameMaster.getVerticalCapsuleTopBound(this.IGameVerticalCapsuleHitTestObj1, x), 9.999999747378752E-5d);
        Assert.assertEquals((double) t, (double) GameMaster.getVerticalCapsuleTopBound(this.IGameVerticalCapsuleHitTestObj1, x - r), 9.999999747378752E-5d);
        Assert.assertEquals((double) t, (double) GameMaster.getVerticalCapsuleTopBound(this.IGameVerticalCapsuleHitTestObj1, x + r), 9.999999747378752E-5d);
        float v = GameMaster.getVerticalCapsuleTopBound(this.IGameVerticalCapsuleHitTestObj1, 14.0f);
        Assert.assertEquals((double) (t - 8.0f), (double) v, 9.999999747378752E-5d);
        if (GameMaster.getVerticalCapsuleTopBound(this.IGameVerticalCapsuleHitTestObj1, 12.0f) > v) {
            z = true;
        } else {
            z = false;
        }
        Assert.assertTrue(z);
        if (GameMaster.getVerticalCapsuleTopBound(this.IGameVerticalCapsuleHitTestObj1, 16.0f) < v) {
            z2 = true;
        } else {
            z2 = false;
        }
        Assert.assertTrue(z2);
        int c = 0;
        while (true) {
            int c2 = c;
            c = c2 + 1;
            if (c2 < 5000) {
                GameMaster.getVerticalCapsuleTopBound(this.IGameVerticalCapsuleHitTestObj1, getRandom(1000.0f));
            } else {
                return;
            }
        }
    }

    @Test
    public void testGetVerticalCapsuleBottomBound() {
        boolean z;
        boolean z2;
        float r = this.IGameVerticalCapsuleHitTestObj1.getVerticalCapsuleHitTestRadius();
        float x = this.IGameVerticalCapsuleHitTestObj1.getVerticalCapsuleHitTestX();
        float b = this.IGameVerticalCapsuleHitTestObj1.getVerticalCapsuleHitTestY() + this.IGameVerticalCapsuleHitTestObj1.getVerticalCapsuleHitTestHeight();
        Assert.assertEquals((double) (b + r), (double) GameMaster.getVerticalCapsuleBottomBound(this.IGameVerticalCapsuleHitTestObj1, x), 9.999999747378752E-5d);
        Assert.assertEquals((double) b, (double) GameMaster.getVerticalCapsuleBottomBound(this.IGameVerticalCapsuleHitTestObj1, x - r), 9.999999747378752E-5d);
        Assert.assertEquals((double) b, (double) GameMaster.getVerticalCapsuleBottomBound(this.IGameVerticalCapsuleHitTestObj1, x + r), 9.999999747378752E-5d);
        float v = GameMaster.getVerticalCapsuleBottomBound(this.IGameVerticalCapsuleHitTestObj1, 14.0f);
        Assert.assertEquals((double) (8.0f + b), (double) v, 9.999999747378752E-5d);
        if (GameMaster.getVerticalCapsuleBottomBound(this.IGameVerticalCapsuleHitTestObj1, 12.0f) < v) {
            z = true;
        } else {
            z = false;
        }
        Assert.assertTrue(z);
        if (GameMaster.getVerticalCapsuleBottomBound(this.IGameVerticalCapsuleHitTestObj1, 16.0f) > v) {
            z2 = true;
        } else {
            z2 = false;
        }
        Assert.assertTrue(z2);
        int c = 0;
        while (true) {
            int c2 = c;
            c = c2 + 1;
            if (c2 < 5000) {
                GameMaster.getVerticalCapsuleBottomBound(this.IGameVerticalCapsuleHitTestObj1, getRandom(1000.0f));
            } else {
                return;
            }
        }
    }

    @Test
    public void testVerticalCapsuleHitTest() {
        Assert.assertEquals(Boolean.valueOf(GameMaster.executeVerticalCapsuleHitTest(this.IGameVerticalCapsuleHitTestObj1, new Vector2DF(0.0f, 0.0f))), false);
        Assert.assertEquals(Boolean.valueOf(GameMaster.executeVerticalCapsuleHitTest(this.IGameVerticalCapsuleHitTestObj1, new Vector2DF(this.IGameVerticalCapsuleHitTestObj1.getVerticalCapsuleHitTestX(), this.IGameVerticalCapsuleHitTestObj1.getVerticalCapsuleHitTestY()))), true);
        Assert.assertEquals(Boolean.valueOf(GameMaster.executeVerticalCapsuleHitTest(this.IGameVerticalCapsuleHitTestObj1, new Vector2DF(this.IGameVerticalCapsuleHitTestObj1.getVerticalCapsuleHitTestX(), this.IGameVerticalCapsuleHitTestObj1.getVerticalCapsuleHitTestY() + this.IGameVerticalCapsuleHitTestObj1.getVerticalCapsuleHitTestHeight()))), true);
        Assert.assertEquals(Boolean.valueOf(GameMaster.executeVerticalCapsuleHitTest(this.IGameVerticalCapsuleHitTestObj1, new Vector2DF(10.0f, 40.0f))), true);
        Assert.assertEquals(Boolean.valueOf(GameMaster.executeVerticalCapsuleHitTest(this.IGameVerticalCapsuleHitTestObj1, new Vector2DF(90.0f, 40.0f))), false);
        Assert.assertEquals(Boolean.valueOf(GameMaster.executeVerticalCapsuleHitTest(this.IGameVerticalCapsuleHitTestObj1, new Vector2DF(10.0f, 20.0f))), false);
        Assert.assertEquals(Boolean.valueOf(GameMaster.executeVerticalCapsuleHitTest(this.IGameVerticalCapsuleHitTestObj1, new Vector2DF(15.0f, 25.0f))), true);
        Assert.assertEquals(Boolean.valueOf(GameMaster.executeVerticalCapsuleHitTest(this.IGameVerticalCapsuleHitTestObj1, new Vector2DF(25.0f, 85.0f))), true);
    }

    @Test
    public void testVerticalCapsuleHitTest2() {
        Vector2DF t = new Vector2DF(this.IGameVerticalCapsuleHitTestObj1.getVerticalCapsuleHitTestX(), this.IGameVerticalCapsuleHitTestObj1.getVerticalCapsuleHitTestY());
        Vector2DF d = t.plusNew(new Vector2DF(0.0f, this.IGameVerticalCapsuleHitTestObj1.getVerticalCapsuleHitTestHeight()));
        float r = this.IGameVerticalCapsuleHitTestObj1.getVerticalCapsuleHitTestRadius();
        Assert.assertEquals(Boolean.valueOf(GameMaster.executeVerticalCapsuleHitTest(this.IGameVerticalCapsuleHitTestObj1, new Vector2DF(25.0f, 86.0f))), true);
        Assert.assertEquals(Boolean.valueOf(GameMaster.executeVerticalCapsuleHitTest(this.IGameVerticalCapsuleHitTestObj1, new Vector2DF(27.0f, 87.0f))), true);
        Assert.assertEquals(Boolean.valueOf(GameMaster.executeVerticalCapsuleHitTest(this.IGameVerticalCapsuleHitTestObj1, new Vector2DF(27.0f, 88.0f))), false);
        int x = 0;
        while (true) {
            int x2 = x;
            x = x2 + 1;
            if (x2 >= 3500) {
                Assert.assertEquals(Boolean.valueOf(GameMaster.executeVerticalCapsuleHitTest(this.IGameVerticalCapsuleHitTestObj1, t.plusNew(new Vector2DF(1.0E-4f + r, 0.0f)))), false);
                return;
            }
            Assert.assertTrue(GameMaster.executeVerticalCapsuleHitTest(this.IGameVerticalCapsuleHitTestObj1, t.plusNew(new Vector2DF(getRandom(r), 0.0f).rotate(getRandom(360.0f)))));
            Assert.assertTrue(GameMaster.executeVerticalCapsuleHitTest(this.IGameVerticalCapsuleHitTestObj1, d.plusNew(new Vector2DF(getRandom(r), 0.0f).rotate(getRandom(360.0f)))));
            Assert.assertFalse(GameMaster.executeVerticalCapsuleHitTest(this.IGameVerticalCapsuleHitTestObj1, t.plusNew(new Vector2DF(r + 0.01f, 0.0f).rotate(-getRandom(180.0f)))));
            Assert.assertFalse(GameMaster.executeVerticalCapsuleHitTest(this.IGameVerticalCapsuleHitTestObj1, d.plusNew(new Vector2DF(r + 0.01f, 0.0f).rotate(getRandom(180.0f)))));
            Assert.assertTrue(GameMaster.executeVerticalCapsuleHitTest(this.IGameVerticalCapsuleHitTestObj1, new Vector2DF((t.getX() - r) + getRandom(2.0f * r), t.getY() + getRandom(this.IGameVerticalCapsuleHitTestObj1.getVerticalCapsuleHitTestHeight()))));
        }
    }

    /* access modifiers changed from: package-private */
    public float getRandom(float max) {
        float v = GameMaster.getRandom().nextFloat() * max;
        System.out.println("RANDOM:" + v);
        return v;
    }
}
