package com.skyd.core.android.game;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import com.skyd.core.android.game.GameObjectList;
import com.skyd.core.android.game.GameSpirit;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;

public class GameScene extends GameRootObject implements ICurrentFrameNumberSupport {
    final int NeedsToRedrawColorSign = -16244687;
    GameSpirit.OnLevelChangedListener Spirit_OnLevelChangedListener = new GameSpirit.OnLevelChangedListener() {
        public void OnLevelChangedEvent(Object sender, float value) {
            GameScene.this.sortSpiritList();
        }
    };
    private long _CurrentFrameNumber = 0;
    private ConcurrentHashMap<Integer, Bitmap> _DrawCacheMap = new ConcurrentHashMap<>();
    private float _MaxDrawCacheLayer = 0.0f;
    private String _Name = null;
    private GameObjectList<GameSpirit> _SpiritList = new GameObjectList<>(this);
    long refreshTime;

    public GameScene() {
        this._SpiritList.addOnAddItemListener(new GameObjectList.OnAddItemListener<GameSpirit>() {
            public void OnAddItemEvent(Object sender, GameSpirit newItem) {
                newItem.addOnLevelChangedListener(GameScene.this.Spirit_OnLevelChangedListener);
            }
        });
        this._SpiritList.addOnRemoveItemListener(new GameObjectList.OnRemoveItemListener<GameSpirit>() {
            public void OnRemoveItemEvent(Object sender, GameSpirit oldItem) {
                oldItem.removeOnLevelChangedListener(GameScene.this.Spirit_OnLevelChangedListener);
            }
        });
        this._SpiritList.addOnChangedListener(new GameObjectList.OnChangedListener() {
            public void OnChangedEvent(Object sender, int newSize) {
                GameScene.this.sortSpiritList();
            }
        });
    }

    public GameObjectList<GameSpirit> getSpiritList() {
        return this._SpiritList;
    }

    public void setSpiritList(GameObjectList<GameSpirit> value) {
        this._SpiritList = value;
    }

    public void setSpiritListToDefault() {
        setSpiritList(new GameObjectList(this));
    }

    public String getName() {
        return this._Name;
    }

    public void setName(String value) {
        this._Name = value;
    }

    public void setNameToDefault() {
        setName(null);
    }

    public Vector<GameSpirit> getSpiritsByName(String name) {
        Vector<GameSpirit> l = new Vector<>();
        Iterator<GameSpirit> it = this._SpiritList.iterator();
        while (it.hasNext()) {
            GameSpirit f = it.next();
            if (f.getName() == name) {
                l.add(f);
            }
        }
        return l;
    }

    /* access modifiers changed from: protected */
    public void sortSpiritList() {
        this._SpiritList.sort(new Comparator<GameSpirit>() {
            public int compare(GameSpirit arg0, GameSpirit arg1) {
                if (arg0.getLevel() == arg1.getLevel()) {
                    return 0;
                }
                if (arg0.getLevel() > arg1.getLevel()) {
                    return 1;
                }
                return -1;
            }
        });
    }

    public void updateChilds() {
        Iterator<GameSpirit> it = this._SpiritList.iterator();
        while (it.hasNext()) {
            it.next().update();
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0068, code lost:
        r6 = r11._SpiritList.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0072, code lost:
        if (r6.hasNext() == false) goto L_0x0009;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0074, code lost:
        r3 = r6.next();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0082, code lost:
        if (r3.getLevel() <= r11._MaxDrawCacheLayer) goto L_0x006e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0084, code lost:
        drawChild(r3, r12, r13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void drawChilds(android.graphics.Canvas r12, android.graphics.Rect r13) {
        /*
            r11 = this;
            int r4 = r11.getTargetCacheID()
            if (r4 != 0) goto L_0x000a
            r11.normalDrawing(r12, r13)
        L_0x0009:
            return
        L_0x000a:
            java.util.concurrent.ConcurrentHashMap<java.lang.Integer, android.graphics.Bitmap> r6 = r11._DrawCacheMap
            monitor-enter(r6)
            r1 = 0
            android.graphics.Bitmap r1 = r11.getDrawCacheBitmap(r4)     // Catch:{ Exception -> 0x009c }
            android.graphics.Paint r5 = new android.graphics.Paint     // Catch:{ Exception -> 0x009c }
            r5.<init>()     // Catch:{ Exception -> 0x009c }
            if (r1 == 0) goto L_0x0024
            r7 = 0
            r8 = 0
            int r7 = r1.getPixel(r7, r8)     // Catch:{ Exception -> 0x009c }
            r8 = -16244687(0xffffffffff082031, float:-1.8094215E38)
            if (r7 != r8) goto L_0x0062
        L_0x0024:
            java.util.Date r7 = new java.util.Date     // Catch:{ Exception -> 0x009c }
            r7.<init>()     // Catch:{ Exception -> 0x009c }
            long r7 = r7.getTime()     // Catch:{ Exception -> 0x009c }
            long r9 = r11.refreshTime     // Catch:{ Exception -> 0x009c }
            long r7 = r7 - r9
            r9 = 80
            int r7 = (r7 > r9 ? 1 : (r7 == r9 ? 0 : -1))
            if (r7 >= 0) goto L_0x003e
            r11.normalDrawing(r12, r13)     // Catch:{ Exception -> 0x009c }
            monitor-exit(r6)     // Catch:{ all -> 0x003b }
            goto L_0x0009
        L_0x003b:
            r7 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x003b }
            throw r7
        L_0x003e:
            if (r1 != 0) goto L_0x004e
            int r7 = r12.getWidth()     // Catch:{ Exception -> 0x009c }
            int r8 = r12.getHeight()     // Catch:{ Exception -> 0x009c }
            android.graphics.Bitmap$Config r9 = android.graphics.Bitmap.Config.RGB_565     // Catch:{ Exception -> 0x009c }
            android.graphics.Bitmap r1 = android.graphics.Bitmap.createBitmap(r7, r8, r9)     // Catch:{ Exception -> 0x009c }
        L_0x004e:
            android.graphics.Canvas r0 = new android.graphics.Canvas     // Catch:{ Exception -> 0x009c }
            r0.<init>(r1)     // Catch:{ Exception -> 0x009c }
            com.skyd.core.android.game.GameObjectList<com.skyd.core.android.game.GameSpirit> r7 = r11._SpiritList     // Catch:{ Exception -> 0x009c }
            java.util.Iterator r7 = r7.iterator()     // Catch:{ Exception -> 0x009c }
        L_0x0059:
            boolean r8 = r7.hasNext()     // Catch:{ Exception -> 0x009c }
            if (r8 != 0) goto L_0x0088
            r11.addDrawCacheBitmap(r4, r1)     // Catch:{ Exception -> 0x009c }
        L_0x0062:
            r7 = 0
            r8 = 0
            r12.drawBitmap(r1, r7, r8, r5)     // Catch:{ Exception -> 0x009c }
            monitor-exit(r6)     // Catch:{ all -> 0x003b }
            com.skyd.core.android.game.GameObjectList<com.skyd.core.android.game.GameSpirit> r6 = r11._SpiritList
            java.util.Iterator r6 = r6.iterator()
        L_0x006e:
            boolean r7 = r6.hasNext()
            if (r7 == 0) goto L_0x0009
            java.lang.Object r3 = r6.next()
            com.skyd.core.android.game.GameSpirit r3 = (com.skyd.core.android.game.GameSpirit) r3
            float r7 = r3.getLevel()
            float r8 = r11._MaxDrawCacheLayer
            int r7 = (r7 > r8 ? 1 : (r7 == r8 ? 0 : -1))
            if (r7 <= 0) goto L_0x006e
            r11.drawChild(r3, r12, r13)
            goto L_0x006e
        L_0x0088:
            java.lang.Object r3 = r7.next()     // Catch:{ Exception -> 0x009c }
            com.skyd.core.android.game.GameSpirit r3 = (com.skyd.core.android.game.GameSpirit) r3     // Catch:{ Exception -> 0x009c }
            float r8 = r3.getLevel()     // Catch:{ Exception -> 0x009c }
            float r9 = r11._MaxDrawCacheLayer     // Catch:{ Exception -> 0x009c }
            int r8 = (r8 > r9 ? 1 : (r8 == r9 ? 0 : -1))
            if (r8 > 0) goto L_0x0059
            r11.drawChild(r3, r0, r13)     // Catch:{ Exception -> 0x009c }
            goto L_0x0059
        L_0x009c:
            r7 = move-exception
            r2 = r7
            java.lang.String r7 = "绘制缓存失败，可能缓存对象已被释放"
            com.skyd.core.android.game.GameMaster.log(r11, r7)     // Catch:{ all -> 0x003b }
            r11.normalDrawing(r12, r13)     // Catch:{ all -> 0x003b }
            monitor-exit(r6)     // Catch:{ all -> 0x003b }
            goto L_0x0009
        */
        throw new UnsupportedOperationException("Method not decompiled: com.skyd.core.android.game.GameScene.drawChilds(android.graphics.Canvas, android.graphics.Rect):void");
    }

    private void normalDrawing(Canvas c, Rect drawArea) {
        Iterator<GameSpirit> it = this._SpiritList.iterator();
        while (it.hasNext()) {
            drawChild(it.next(), c, drawArea);
        }
    }

    private void addDrawCacheBitmap(int id, Bitmap bmp) {
        if (this._DrawCacheMap.containsKey(Integer.valueOf(id))) {
            this._DrawCacheMap.remove(Integer.valueOf(id));
        }
        this._DrawCacheMap.put(Integer.valueOf(id), bmp);
    }

    /* access modifiers changed from: protected */
    public void drawSelf(Canvas c, Rect drawArea) {
    }

    /* access modifiers changed from: protected */
    public void updateSelf() {
        this._CurrentFrameNumber++;
    }

    public void clearAllDrawCacheBitmap() {
        for (Integer f : this._DrawCacheMap.keySet()) {
            clearDrawCacheBitmap(f.intValue());
        }
    }

    public void clearAllDrawCacheBitmapWithout(int CacheID) {
        for (Integer f : this._DrawCacheMap.keySet()) {
            if (f.intValue() != CacheID) {
                clearDrawCacheBitmap(f.intValue());
            }
        }
    }

    public void clearAllDrawCacheBitmapWithoutCurrent() {
        clearAllDrawCacheBitmapWithout(getTargetCacheID());
    }

    public void clearDrawCacheBitmap() {
        clearDrawCacheBitmap(getTargetCacheID());
    }

    public void clearDrawCacheBitmap(int CacheID) {
        synchronized (this._DrawCacheMap) {
            Bitmap b = getDrawCacheBitmap(CacheID);
            if (b != null) {
                this._DrawCacheMap.remove(Integer.valueOf(CacheID));
                b.recycle();
            }
        }
    }

    public void refreshAllDrawCacheBitmap() {
        for (Integer f : this._DrawCacheMap.keySet()) {
            refreshDrawCacheBitmap(f.intValue());
        }
    }

    public void refreshAllDrawCacheBitmapWithout(int CacheID) {
        for (Integer f : this._DrawCacheMap.keySet()) {
            if (f.intValue() != CacheID) {
                refreshDrawCacheBitmap(f.intValue());
            }
        }
    }

    public void refreshAllDrawCacheBitmapWithoutCurrent() {
        refreshAllDrawCacheBitmapWithout(getTargetCacheID());
    }

    public void refreshDrawCacheBitmap() {
        refreshDrawCacheBitmap(getTargetCacheID());
    }

    public void refreshDrawCacheBitmap(int CacheID) {
        Bitmap b = getDrawCacheBitmap(CacheID);
        if (b != null) {
            this.refreshTime = new Date().getTime();
            b.setPixel(0, 0, -16244687);
        }
    }

    public Bitmap getDrawCacheBitmap() {
        return getDrawCacheBitmap(getTargetCacheID());
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public Bitmap getDrawCacheBitmap(int CacheID) {
        if (this._DrawCacheMap.containsKey(Integer.valueOf(CacheID))) {
            return this._DrawCacheMap.get(Integer.valueOf(CacheID));
        }
        return null;
    }

    public ConcurrentHashMap<Integer, Bitmap> getDrawCacheMap() {
        return this._DrawCacheMap;
    }

    public void setDrawCacheMap(ConcurrentHashMap<Integer, Bitmap> value) {
        this._DrawCacheMap = value;
    }

    public void setDrawCacheMapToDefault() {
        setDrawCacheMap(new ConcurrentHashMap());
    }

    public int getTargetCacheID() {
        return 0;
    }

    public float getMaxDrawCacheLayer() {
        return this._MaxDrawCacheLayer;
    }

    public void setMaxDrawCacheLayer(float value) {
        this._MaxDrawCacheLayer = value;
    }

    public void setMaxDrawCacheLayerToDefault() {
        setMaxDrawCacheLayer(0.0f);
    }

    public long getCurrentFrameNumber() {
        return this._CurrentFrameNumber;
    }
}
