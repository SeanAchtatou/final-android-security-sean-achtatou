package com.skyd.core.game.crosswisewar;

public interface IBuff extends IWarrior {
    long getCreateTime();

    int getKeepTime();

    void remove();

    void setKeepTime(int i);

    void setToTarget(IWarrior iWarrior);
}
