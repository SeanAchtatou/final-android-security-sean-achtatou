package com.scoreloop.client.android.ui.component.achievement;

import android.content.Intent;
import android.os.Bundle;
import com.scoreloop.client.android.core.controller.AchievementsController;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.model.Achievement;
import com.scoreloop.client.android.ui.component.base.ComponentListActivity;
import com.scoreloop.client.android.ui.component.post.PostOverlayActivity;
import com.scoreloop.client.android.ui.framework.BaseListAdapter;

public class AchievementListActivity extends ComponentListActivity<AchievementListItem> {
    private AchievementsController _achievementsController;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setListAdapter(new BaseListAdapter(this));
        this._achievementsController = new AchievementsController(getRequestControllerObserver());
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        if (isSessionUser()) {
            showSpinner();
            getManager().submitAchievements(new Runnable() {
                public void run() {
                    AchievementListActivity.this.hideSpinner();
                    AchievementListActivity.this.setNeedsRefresh();
                }
            });
            return;
        }
        setNeedsRefresh();
    }

    public void onListItemClick(AchievementListItem item) {
        Achievement achievement = item.getAchievement();
        if (item.isEnabled() && !PostOverlayActivity.isPosted(getApplicationContext(), achievement)) {
            Intent intent = new Intent(this, PostOverlayActivity.class);
            PostOverlayActivity.setMessageTarget(achievement);
            startActivity(intent);
        }
    }

    public void onRefresh(int flags) {
        showSpinnerFor(this._achievementsController);
        this._achievementsController.setUser(getUser());
        this._achievementsController.loadAchievements();
    }

    public void requestControllerDidReceiveResponseSafe(RequestController aRequestController) {
        BaseListAdapter<AchievementListItem> adapter = getBaseListAdapter();
        adapter.clear();
        boolean isSessionUser = isSessionUser();
        for (Achievement achievement : this._achievementsController.getAchievements()) {
            adapter.add(new AchievementListItem(this, achievement, isSessionUser));
        }
    }
}
