package com.scoreloop.client.android.ui.component.challenge;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.scoreloop.client.android.core.model.Challenge;
import com.scoreloop.client.android.ui.component.base.ComponentActivity;
import com.scoreloop.client.android.ui.component.base.StringFormatter;
import com.scoreloop.client.android.ui.framework.BaseListItem;
import com.skyd.bestpuzzle.n1622.R;

class ChallengeSettingsListItem extends BaseListItem {
    private final Challenge _challenge;

    public ChallengeSettingsListItem(ComponentActivity context, Challenge challenge) {
        super(context, null, null);
        this._challenge = challenge;
    }

    /* access modifiers changed from: protected */
    public ComponentActivity getComponentActivity() {
        return (ComponentActivity) getContext();
    }

    public int getType() {
        return 9;
    }

    public View getView(View view, ViewGroup parent) {
        if (view == null) {
            view = getLayoutInflater().inflate((int) R.layout.sl_list_item_challenge_settings, (ViewGroup) null);
        }
        prepareView(view);
        return view;
    }

    public boolean isEnabled() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void prepareView(View view) {
        ((TextView) view.findViewById(R.id.stake)).setText(String.format(getContext().getResources().getString(R.string.sl_format_stake), StringFormatter.formatMoney(this._challenge.getStake(), getComponentActivity().getConfiguration())));
        TextView mode = (TextView) view.findViewById(R.id.mode);
        if (getComponentActivity().getGame().hasModes()) {
            mode.setVisibility(0);
            mode.setText(getComponentActivity().getModeString(this._challenge.getMode().intValue()));
            return;
        }
        mode.setVisibility(8);
    }
}
