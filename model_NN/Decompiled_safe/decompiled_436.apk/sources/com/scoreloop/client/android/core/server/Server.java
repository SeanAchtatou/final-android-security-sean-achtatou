package com.scoreloop.client.android.core.server;

import android.os.Handler;
import android.os.Message;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.util.Base64;
import com.scoreloop.client.android.core.util.Cache;
import com.scoreloop.client.android.core.util.Logger;
import java.net.URL;
import java.nio.channels.IllegalSelectorException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.LinkedList;
import org.json.JSONException;
import org.json.JSONObject;

public class Server {
    static final /* synthetic */ boolean a = (!Server.class.desiredAssertionStatus());
    private final URL b;
    private final g c = new g(this.d);
    private final a d = new a();
    private a e;
    /* access modifiers changed from: private */
    public Request f;
    private final LinkedList<Request> g = new LinkedList<>();
    /* access modifiers changed from: private */
    public final LinkedList<Request> h = new LinkedList<>();
    /* access modifiers changed from: private */
    public final Cache<String, Request> i = new Cache<>();
    private boolean j = true;

    private class a extends Handler {
        private a() {
        }

        public void handleMessage(Message message) {
            boolean z;
            String r;
            Request a2 = Server.this.f;
            Request unused = Server.this.f = null;
            if (a2.k() != Request.State.CANCELLED) {
                switch (message.what) {
                    case 1:
                        Response response = (Response) message.obj;
                        Integer b = response.b();
                        if (b != null && b.intValue() == a2.i()) {
                            a2.a(response);
                            break;
                        } else {
                            a2.a(new Exception("Invalid response ID, expected:" + a2.i() + ", but was:" + b));
                            break;
                        }
                    case 2:
                        a2.a((Exception) message.obj);
                        break;
                    case 3:
                        if (!a2.s()) {
                            a2.a((Exception) message.obj);
                            break;
                        }
                        break;
                    default:
                        throw new IllegalStateException("Unknown message type");
                }
                if (message.what == 1 || !a2.s()) {
                    z = true;
                } else {
                    z = a2.t() == 1;
                    Server.this.h.add(a2);
                }
                if (z) {
                    a2.f().a(a2);
                    if (message.what == 1 && a2.u() > 0 && (r = a2.r()) != null) {
                        Server.this.i.a(r, a2, a2.u());
                    }
                }
            }
            if (Server.this.f == null && !a2.n()) {
                Server.this.d();
            }
        }
    }

    public Server(URL url) {
        this.b = url;
        this.c.setPriority(1);
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA1");
            instance.reset();
            instance.update("https://api.scoreloop.com/bayeux/android/v2".getBytes());
            byte[] digest = instance.digest();
            instance.reset();
            instance.update("https://www.scoreloop.com/android/updates".getBytes());
            byte[] digest2 = instance.digest();
            byte[] bArr = new byte[16];
            for (int i2 = 0; i2 < bArr.length; i2++) {
                bArr[i2] = (byte) ((digest[(i2 + 6) % digest.length] ^ digest2[(i2 + 3) % digest2.length]) ^ 62);
            }
            this.e = new a(b(), this.c, bArr);
            for (int i3 = 0; i3 < digest.length; i3++) {
                digest[i3] = (byte) (digest[i3] ^ 26);
            }
            this.e.b(Base64.a(digest));
            for (int i4 = 0; i4 < digest2.length; i4++) {
                digest2[i4] = (byte) (digest2[i4] ^ 53);
            }
            this.e.a(Base64.a(digest2));
            this.c.a(this.e);
            this.c.start();
        } catch (NoSuchAlgorithmException e2) {
            throw new IllegalStateException();
        }
    }

    private Request c(Request request) {
        String r = request.r();
        if (r != null) {
            return this.i.a(r);
        }
        return null;
    }

    private void c() {
        if (this.f != null) {
            Logger.a("Server", "doCancelCurrentRequest canceling request: ", this.f);
            this.f.o();
            this.f.f().a(this.f);
            this.c.b();
        }
    }

    /* access modifiers changed from: private */
    public void d() {
        Request poll;
        Request poll2;
        if (a || this.f == null) {
            if (!this.h.isEmpty()) {
                do {
                    poll2 = this.h.poll();
                    if (poll2 != null) {
                        this.g.add(poll2);
                        continue;
                    }
                } while (poll2 != null);
            }
            do {
                poll = this.g.poll();
                if (poll != null && !poll.m()) {
                    try {
                        d(poll);
                        return;
                    } catch (Exception e2) {
                        Logger.a("Server", "startNextQueuedRequest: caught runtime exception\n" + e2);
                        return;
                    }
                }
            } while (poll != null);
            return;
        }
        throw new AssertionError();
    }

    private void d(Request request) {
        Logger.a("Server", "startProcessingRequest: " + request.toString());
        if (!a && request == null) {
            throw new AssertionError();
        } else if (a || this.f == null) {
            this.f = request;
            this.f.f().b(this.f);
            this.f.q();
            this.c.a(this.f);
        } else {
            throw new AssertionError();
        }
    }

    public void a() {
        this.c.a();
    }

    public void a(final Request request) {
        Logger.a("Server", "addRequest: ", request);
        if (request.k() == Request.State.ENQUEUED || request.k() == Request.State.EXECUTING) {
            throw new IllegalStateException("Request already enqueued or executing");
        } else if (request.a() == null) {
            throw new IllegalStateException("Request channel is not set");
        } else if (request.c() == null) {
            throw new IllegalStateException("Request method is not set");
        } else {
            if (request.h() == null) {
                request.a(new JSONObject());
            }
            try {
                request.h().put("method", request.c().toString());
                Request request2 = null;
                if (request.u() <= 0 || !this.j) {
                    this.i.b();
                } else {
                    request2 = c(request);
                }
                if (request2 != null) {
                    request.a(request2.j());
                    this.d.post(new Runnable() {
                        public void run() {
                            request.f().a(request);
                        }
                    });
                } else if (this.f != null || !this.g.isEmpty() || !this.h.isEmpty()) {
                    request.p();
                    this.g.add(request);
                    if (!this.h.isEmpty()) {
                        d();
                    }
                } else {
                    d(request);
                }
            } catch (JSONException e2) {
                throw new IllegalSelectorException();
            }
        }
    }

    public void a(String str) {
        this.e.c(str);
    }

    public void a(JSONObject jSONObject) {
        this.c.a(jSONObject);
    }

    /* access modifiers changed from: package-private */
    public URL b() {
        return this.b;
    }

    public void b(Request request) {
        Logger.a("Server", "cancelRequest: ", request);
        if (this.f == request) {
            c();
            return;
        }
        request.o();
        request.f().a(request);
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        a();
        super.finalize();
    }
}
