package com.scoreloop.client.android.core.model;

import com.scoreloop.client.android.core.PublishedFor__1_0_0;
import com.scoreloop.client.android.core.util.JSONUtils;
import com.scoreloop.client.android.core.util.SetterIntent;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class Score extends BaseEntity implements MessageTargetInterface {
    public static String a = "score";
    private String c;
    private Integer d;
    private Double e;
    private Integer f;
    private Double g;
    private Integer h;
    private User i;
    private Map<String, Object> j;

    @PublishedFor__1_0_0
    public Score(Double d2, Map<String, Object> map) {
        this.g = d2;
        if (map != null) {
            this.j = new HashMap(map);
            this.e = (Double) this.j.get(Game.CONTEXT_KEY_MINOR_RESULT);
            this.d = (Integer) this.j.get(Game.CONTEXT_KEY_LEVEL);
            this.f = (Integer) this.j.get(Game.CONTEXT_KEY_MODE);
            this.j.remove(Game.CONTEXT_KEY_MINOR_RESULT);
            this.j.remove(Game.CONTEXT_KEY_LEVEL);
            this.j.remove(Game.CONTEXT_KEY_MODE);
        }
    }

    public Score(JSONObject jSONObject) throws JSONException {
        a(jSONObject);
    }

    @PublishedFor__1_0_0
    public static boolean areModesEqual(Score score, Score score2) {
        if (score != null && score2 != null) {
            return areModesEqual(score.getMode(), score2.getMode());
        }
        throw new IllegalArgumentException();
    }

    @PublishedFor__1_0_0
    public static boolean areModesEqual(Integer num, Score score) {
        if (score != null) {
            return areModesEqual(num, score.getMode());
        }
        throw new IllegalArgumentException();
    }

    @PublishedFor__1_0_0
    public static boolean areModesEqual(Integer num, Integer num2) {
        if (num != null) {
            return num.equals(num2);
        }
        throw new IllegalArgumentException();
    }

    public String a() {
        return a;
    }

    public void a(User user) {
        this.i = user;
    }

    public void a(Integer num) {
        this.h = num;
    }

    public void a(JSONObject jSONObject) throws JSONException {
        super.a(jSONObject);
        SetterIntent setterIntent = new SetterIntent();
        if (setterIntent.h(jSONObject, "device_id", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.c = (String) setterIntent.a();
        }
        if (setterIntent.c(jSONObject, "result", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.g = (Double) setterIntent.a();
        }
        if (setterIntent.c(jSONObject, "minor_result", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.e = (Double) setterIntent.a();
        }
        if (setterIntent.d(jSONObject, "level", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.d = (Integer) setterIntent.a();
        }
        if (setterIntent.d(jSONObject, "mode", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.f = (Integer) setterIntent.a();
        }
        if (setterIntent.f(jSONObject, User.a, SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.i = new User((JSONObject) setterIntent.a());
        }
        if (setterIntent.f(jSONObject, "context", SetterIntent.ValueMode.ALLOWS_NULL_VALUE)) {
            this.j = setterIntent.b().booleanValue() ? null : JSONUtils.a((JSONObject) setterIntent.a());
        }
    }

    public JSONObject d() throws JSONException {
        JSONObject d2 = super.d();
        if (this.c == null && this.i != null) {
            this.c = this.i.e();
        }
        d2.put("device_id", this.c);
        d2.put("result", JSONUtils.a(this.g));
        d2.put("level", this.d);
        if (this.i != null) {
            d2.put("user_id", this.i.getIdentifier());
        }
        d2.put("mode", this.f);
        d2.put("minor_result", JSONUtils.a(this.e));
        if (this.j != null) {
            d2.put("context", JSONUtils.a(this.j));
        }
        return d2;
    }

    @PublishedFor__1_0_0
    public Map<String, Object> getContext() {
        return this.j;
    }

    @PublishedFor__1_0_0
    public Integer getLevel() {
        return this.d;
    }

    @PublishedFor__1_0_0
    public Double getMinorResult() {
        return this.e;
    }

    @PublishedFor__1_0_0
    public Integer getMode() {
        return this.f;
    }

    @PublishedFor__1_0_0
    public Integer getRank() {
        return this.h;
    }

    @PublishedFor__1_0_0
    public Double getResult() {
        return this.g;
    }

    @PublishedFor__1_0_0
    public User getUser() {
        return this.i;
    }

    @PublishedFor__1_0_0
    public void setContext(Map<String, Object> map) {
        this.j = map;
    }

    @PublishedFor__1_0_0
    public void setLevel(Integer num) {
        this.d = num;
    }

    @PublishedFor__1_0_0
    public void setMinorResult(Double d2) {
        this.e = d2;
    }

    @PublishedFor__1_0_0
    public void setMode(Integer num) {
        this.f = num;
    }

    @PublishedFor__1_0_0
    public void setResult(Double d2) {
        this.g = d2;
    }
}
