package com.scoreloop.client.android.ui.component.score;

import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import com.scoreloop.client.android.core.controller.RankingController;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.ScoresController;
import com.scoreloop.client.android.core.model.Ranking;
import com.scoreloop.client.android.core.model.Score;
import com.scoreloop.client.android.core.model.SearchList;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.ui.component.base.ComponentListActivity;
import com.scoreloop.client.android.ui.component.base.Constant;
import com.scoreloop.client.android.ui.component.base.EmptyListItem;
import com.scoreloop.client.android.ui.component.base.Factory;
import com.scoreloop.client.android.ui.framework.BaseListItem;
import com.scoreloop.client.android.ui.framework.PagingDirection;
import com.scoreloop.client.android.ui.framework.PagingListAdapter;
import com.scoreloop.client.android.ui.framework.ValueStore;
import com.skyd.bestpuzzle.n1622.R;
import java.util.List;

public class ScoreListActivity extends ComponentListActivity<ScoreListItem> implements ValueStore.Observer, PagingListAdapter.OnListItemClickListener<ScoreListItem> {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$scoreloop$client$android$ui$framework$PagingDirection = null;
    private static final String RECENT_TOP_RANK = "recentTopRank";
    private int _cachedVerticalCenterOffset = -1;
    /* access modifiers changed from: private */
    public int _highlightedPosition;
    /* access modifiers changed from: private */
    public PagingDirection _pagingDirection;
    private Ranking _ranking;
    private RankingController _rankingController;
    private ScoresController _scoresController;

    static /* synthetic */ int[] $SWITCH_TABLE$com$scoreloop$client$android$ui$framework$PagingDirection() {
        int[] iArr = $SWITCH_TABLE$com$scoreloop$client$android$ui$framework$PagingDirection;
        if (iArr == null) {
            iArr = new int[PagingDirection.values().length];
            try {
                iArr[PagingDirection.PAGE_TO_NEXT.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[PagingDirection.PAGE_TO_OWN.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[PagingDirection.PAGE_TO_PREV.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[PagingDirection.PAGE_TO_RECENT.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[PagingDirection.PAGE_TO_TOP.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            $SWITCH_TABLE$com$scoreloop$client$android$ui$framework$PagingDirection = iArr;
        }
        return iArr;
    }

    private PagingListAdapter<ScoreListItem> getPagingListAdapter() {
        return (PagingListAdapter) getBaseListAdapter();
    }

    /* access modifiers changed from: private */
    public int getVerticalCenterOffset() {
        if (this._cachedVerticalCenterOffset == -1) {
            View itemView = getPagingListAdapter().getContentItem(this._highlightedPosition).getView(null, null);
            itemView.measure(0, 0);
            this._cachedVerticalCenterOffset = (getListView().getHeight() - itemView.getMeasuredHeight()) / 2;
        }
        return this._cachedVerticalCenterOffset;
    }

    private boolean isHighlightedScore(Score score) {
        return getSession().isOwnedByUser(score.getUser());
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setListAdapter(new PagingListAdapter(this));
        SearchList searchList = (SearchList) getActivityArguments().getValue(Constant.SEARCH_LIST, SearchList.getDefaultScoreSearchList());
        addObservedKeys("mode");
        if (searchList.equals(SearchList.getBuddiesScoreSearchList())) {
            addObservedKeys(Constant.NUMBER_BUDDIES);
        }
        this._scoresController = new ScoresController(getRequestControllerObserver());
        this._scoresController.setRangeLength(Constant.getOptimalRangeLength(getListView(), new ScoreListItem(this, new Score(Double.valueOf(0.0d), null))));
        this._scoresController.setSearchList(searchList);
        this._rankingController = new RankingController(getRequestControllerObserver());
        this._rankingController.setSearchList(searchList);
    }

    /* access modifiers changed from: protected */
    public void onFooterItemClick(BaseListItem footerItem) {
        if (footerItem.getType() == 21) {
            setNeedsRefresh(PagingDirection.PAGE_TO_OWN);
        }
    }

    public void onListItemClick(ScoreListItem item) {
        Factory factory = getFactory();
        User user = ((Score) item.getTarget()).getUser();
        if (getSession().isOwnedByUser(user)) {
            display(factory.createProfileSettingsScreenDescription(user));
        } else {
            display(factory.createUserDetailScreenDescription(user, true));
        }
    }

    public void onPagingListItemClick(PagingDirection pagingDirection) {
        setNeedsRefresh(pagingDirection);
    }

    private void onRanking() {
        final PagingListAdapter<ScoreListItem> adapter = getPagingListAdapter();
        this._ranking = this._rankingController.getRanking();
        if (this._ranking.getRank() == null) {
            showFooter(new ScoreExcludedListItem(this));
        } else if (this._highlightedPosition != -1) {
            ((ScoreHighlightedListItem) adapter.getContentItem(this._highlightedPosition)).setRanking(this._ranking);
            adapter.notifyDataSetChanged();
        } else {
            Score score = this._ranking.getScore();
            if (score != null) {
                showFooter(new ScoreHighlightedListItem(this, score, this._ranking));
            }
        }
        getListView().post(new Runnable() {
            public void run() {
                ListView listView = ScoreListActivity.this.getListView();
                if (ScoreListActivity.this._highlightedPosition != -1) {
                    listView.setSelectionFromTop(ScoreListActivity.this._highlightedPosition + adapter.getFirstContentPosition(), ScoreListActivity.this.getVerticalCenterOffset());
                } else if (ScoreListActivity.this._pagingDirection == PagingDirection.PAGE_TO_TOP) {
                    listView.setSelection(0);
                } else if (ScoreListActivity.this._pagingDirection == PagingDirection.PAGE_TO_NEXT) {
                    listView.setSelection(adapter.getFirstContentPosition());
                } else if (ScoreListActivity.this._pagingDirection == PagingDirection.PAGE_TO_PREV) {
                    listView.setSelectionFromTop(adapter.getLastContentPosition() + 1, listView.getHeight());
                }
            }
        });
    }

    public void onRefresh(int flags) {
        showSpinnerFor(this._scoresController);
        this._scoresController.setMode((Integer) getScreenValues().getValue("mode"));
        switch ($SWITCH_TABLE$com$scoreloop$client$android$ui$framework$PagingDirection()[this._pagingDirection.ordinal()]) {
            case 1:
                this._scoresController.loadNextRange();
                return;
            case 2:
                this._scoresController.loadRangeForUser(getSessionUser());
                return;
            case 3:
                this._scoresController.loadPreviousRange();
                return;
            case 4:
                this._scoresController.loadRangeAtRank(((Integer) getActivityArguments().getValue(RECENT_TOP_RANK, 1)).intValue());
                return;
            case 5:
                this._scoresController.loadRangeAtRank(1);
                return;
            default:
                return;
        }
    }

    private void onScores() {
        int recentTopRank;
        PagingListAdapter<ScoreListItem> adapter = getPagingListAdapter();
        adapter.clear();
        List<Score> scores = this._scoresController.getScores();
        int scoreCount = scores.size();
        for (int i = 0; i < scoreCount; i++) {
            Score score = scores.get(i);
            if (isHighlightedScore(score)) {
                this._highlightedPosition = i;
                adapter.add(new ScoreHighlightedListItem(this, score, null));
            } else {
                adapter.add(new ScoreListItem(this, score));
            }
        }
        if (scoreCount == 0) {
            adapter.add(new EmptyListItem(this, getString(R.string.sl_no_scores)));
            recentTopRank = 1;
        } else {
            recentTopRank = scores.get(0).getRank();
        }
        getActivityArguments().putValue(RECENT_TOP_RANK, recentTopRank);
        boolean hasPreviousRange = this._scoresController.hasPreviousRange();
        adapter.addPagingItems(hasPreviousRange, hasPreviousRange, this._scoresController.hasNextRange());
        this._rankingController.loadRankingForUserInGameMode(getUser(), (Integer) getScreenValues().getValue("mode"));
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        setNeedsRefresh(PagingDirection.PAGE_TO_RECENT);
    }

    public void onValueChanged(ValueStore valueStore, String key, Object oldValue, Object newValue) {
        if (isValueChangedFor(key, "mode", oldValue, newValue)) {
            if (oldValue == null) {
                setNeedsRefresh(PagingDirection.PAGE_TO_RECENT);
            } else {
                setNeedsRefresh(PagingDirection.PAGE_TO_TOP);
            }
        } else if (isValueChangedFor(key, Constant.NUMBER_BUDDIES, oldValue, newValue)) {
            setNeedsRefresh(PagingDirection.PAGE_TO_TOP);
        }
    }

    public void requestControllerDidReceiveResponseSafe(RequestController aRequestController) {
        if (aRequestController == this._scoresController) {
            onScores();
        } else if (aRequestController == this._rankingController) {
            onRanking();
        }
    }

    private void setNeedsRefresh(PagingDirection pagingDirection) {
        this._highlightedPosition = -1;
        this._pagingDirection = pagingDirection;
        hideFooter();
        setNeedsRefresh();
    }
}
