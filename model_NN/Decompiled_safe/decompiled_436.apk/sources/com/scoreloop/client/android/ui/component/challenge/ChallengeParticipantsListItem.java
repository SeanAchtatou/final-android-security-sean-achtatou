package com.scoreloop.client.android.ui.component.challenge;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.ui.framework.BaseListItem;
import com.scoreloop.client.android.ui.util.ImageDownloader;
import com.skyd.bestpuzzle.n1622.R;

class ChallengeParticipantsListItem extends BaseListItem {
    private final User _contender;
    private String _contenderStats;
    private final User _contestant;
    private String _contestantStats;

    public ChallengeParticipantsListItem(Context context, User contender, User contestant) {
        super(context, null, null);
        this._contender = contender;
        this._contestant = contestant;
    }

    public int getType() {
        return 7;
    }

    public View getView(View view, ViewGroup parent) {
        if (view == null) {
            view = getLayoutInflater().inflate((int) R.layout.sl_list_item_challenge_participants, (ViewGroup) null);
        }
        prepareView(view);
        return view;
    }

    public boolean isEnabled() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void prepareView(View view) {
        ImageView contenderIconView = (ImageView) view.findViewById(R.id.contender_icon);
        String contenderImageUrl = this._contender.getImageUrl();
        if (contenderImageUrl != null) {
            ImageDownloader.downloadImage(contenderImageUrl, getDrawableLoading(), contenderIconView, null);
        }
        ((TextView) view.findViewById(R.id.contender_name)).setText(this._contender.getDisplayName());
        ((TextView) view.findViewById(R.id.contender_stats)).setText(this._contenderStats);
        ImageView contestantIconView = (ImageView) view.findViewById(R.id.contestant_icon);
        if (this._contestant != null) {
            String contestantImageUrl = this._contestant.getImageUrl();
            if (contestantImageUrl != null) {
                ImageDownloader.downloadImage(contestantImageUrl, getDrawableLoading(), contestantIconView, null);
            }
        } else {
            contestantIconView.setImageDrawable(getContext().getResources().getDrawable(R.drawable.sl_icon_challenge_anyone));
        }
        ((TextView) view.findViewById(R.id.contestant_name)).setText(this._contestant != null ? this._contestant.getDisplayName() : getContext().getResources().getString(R.string.sl_anyone));
        ((TextView) view.findViewById(R.id.contestant_stats)).setText(this._contestantStats);
    }

    private Drawable getDrawableLoading() {
        return getContext().getResources().getDrawable(R.drawable.sl_icon_games_loading);
    }

    public void setContenderStats(String contenderStats) {
        this._contenderStats = contenderStats;
    }

    public void setContestantStats(String contestantStats) {
        this._contestantStats = contestantStats;
    }
}
