package com.scoreloop.client.android.ui.component.base;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.Session;

public class PackageManager {
    private static final String[] SCORELOOP_APP_PACKAGE_NAMES = {"com.scoreloop.android.slapp"};

    private static void download(Context context, String downloadUrl) {
        if (downloadUrl != null) {
            context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(downloadUrl)));
        }
    }

    public static void installScoreloopApp(Context context) {
        download(context, Session.getCurrentSession().getScoreloopAppDownloadUrl());
    }

    public static void installGame(Context context, Game game) {
        download(context, game.getDownloadUrl());
    }

    private static Intent getLaunchIntentForPackage(Context context, String[] packageNames) {
        android.content.pm.PackageManager pm = context.getPackageManager();
        int i = 0;
        while (i < packageNames.length) {
            try {
                if (pm.getPackageInfo(packageNames[i], 0) != null) {
                    return pm.getLaunchIntentForPackage(packageNames[i]);
                }
                i++;
            } catch (PackageManager.NameNotFoundException e) {
            }
        }
        return null;
    }

    public static void launchGame(Context context, Game game) {
        Intent intent = getLaunchIntentForPackage(context, game.getPackageNames());
        if (intent != null) {
            context.startActivity(intent);
        }
    }

    public static boolean isScoreloopAppInstalled(Context context) {
        return getLaunchIntentForPackage(context, SCORELOOP_APP_PACKAGE_NAMES) != null;
    }

    public static boolean isGameInstalled(Context context, Game game) {
        return getLaunchIntentForPackage(context, game.getPackageNames()) != null;
    }
}
