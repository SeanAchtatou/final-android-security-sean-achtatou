package com.scoreloop.client.android.core.model;

import com.scoreloop.client.android.core.PublishedFor__1_0_0;
import com.scoreloop.client.android.core.PublishedFor__1_1_0;
import com.scoreloop.client.android.core.PublishedFor__2_1_0;
import com.scoreloop.client.android.core.util.SetterIntent;
import org.json.JSONException;
import org.json.JSONObject;

public class Ranking {
    private Integer a;
    private Score b;
    private Integer c;

    public void a(JSONObject jSONObject) throws JSONException {
        SetterIntent setterIntent = new SetterIntent();
        this.a = setterIntent.a(jSONObject, "rank", SetterIntent.KeyMode.THROWS_WHEN_NO_KEY, SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE);
        this.c = setterIntent.a(jSONObject, "total", SetterIntent.KeyMode.THROWS_WHEN_NO_KEY, SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE);
        if (this.a.intValue() == 0) {
            this.a = null;
        }
        if (setterIntent.f(jSONObject, "score", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.b = new Score((JSONObject) setterIntent.a());
            this.b.a(this.a);
        }
    }

    @PublishedFor__1_0_0
    public Integer getRank() {
        return this.a;
    }

    @PublishedFor__2_1_0
    public Score getScore() {
        return this.b;
    }

    @PublishedFor__1_1_0
    public Integer getTotal() {
        return this.c;
    }
}
