package com.scoreloop.client.android.core.util;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class JSONUtils {
    private static Object a(Object obj) throws JSONException {
        if (JSONObject.NULL.equals(obj)) {
            return null;
        }
        return obj instanceof JSONArray ? a((JSONArray) obj) : obj instanceof JSONObject ? b((JSONObject) obj) : obj;
    }

    public static String a(Double d) {
        if (d == null) {
            return null;
        }
        NumberFormat instance = NumberFormat.getInstance(Locale.US);
        if (instance instanceof DecimalFormat) {
            ((DecimalFormat) instance).applyPattern("##0.######");
        }
        return instance.format(d);
    }

    private static List<Object> a(JSONArray jSONArray) throws JSONException {
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < jSONArray.length(); i++) {
            arrayList.add(a(jSONArray.opt(i)));
        }
        return arrayList;
    }

    public static Map<String, Object> a(JSONObject jSONObject) throws JSONException {
        return b(jSONObject);
    }

    public static JSONObject a(Map<String, Object> map) {
        return map != null ? new JSONObject(map) : new JSONObject();
    }

    private static Map<String, Object> b(JSONObject jSONObject) throws JSONException {
        HashMap hashMap = new HashMap();
        Iterator<String> keys = jSONObject.keys();
        while (keys.hasNext()) {
            String next = keys.next();
            hashMap.put(next, a(jSONObject.opt(next)));
        }
        return hashMap;
    }
}
