package com.scoreloop.client.android.core.controller;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.RequestCompletionCallback;
import com.scoreloop.client.android.core.server.Response;
import com.scoreloop.client.android.core.util.Logger;
import com.scoreloop.client.android.core.util.SetterIntent;
import com.scoreloop.client.android.ui.component.base.TrackerEvents;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class RequestController {
    static final /* synthetic */ boolean a = (!RequestController.class.desiredAssertionStatus());
    private final b b;
    private Exception c;
    private final RequestControllerObserver d;
    /* access modifiers changed from: private */
    public Request e;
    private final Session f;
    /* access modifiers changed from: private */
    public f g;
    private boolean h;

    private class a extends Handler {
        private final Exception b;
        private final boolean c;
        private final RequestControllerObserver d;

        public a(RequestControllerObserver requestControllerObserver, boolean z, Exception exc) {
            this.d = requestControllerObserver;
            this.b = exc;
            this.c = z;
        }

        public void handleMessage(Message message) {
            if (this.c) {
                this.d.requestControllerDidFail(RequestController.this, this.b);
            } else {
                this.d.requestControllerDidReceiveResponse(RequestController.this);
            }
        }
    }

    private class b implements RequestCompletionCallback {
        private b() {
        }

        public void a(Request request) {
            RequestController.this.c((Exception) null);
            switch (request.k()) {
                case RETRY:
                    Logger.a("RequestController", "RequestCallback.onRequestCompleted: request will be retried: ", request);
                    RequestController.this.c();
                    return;
                case COMPLETED:
                    Logger.a("RequestController", "RequestCallback.onRequestCompleted: request completed: ", request);
                    try {
                        if (RequestController.this.a(request, request.j())) {
                            RequestController.this.c();
                            return;
                        }
                        return;
                    } catch (Exception e) {
                        RequestController.this.d(e);
                        return;
                    }
                case FAILED:
                    Logger.a("RequestController", "RequestCallback.onRequestCompleted: request failed: ", request);
                    RequestController.this.d(request.g());
                    return;
                case CANCELLED:
                    Logger.a("RequestController", "RequestCallback.onRequestCompleted: request cancelled: ", request);
                    RequestController.this.d(new RequestCancelledException());
                    return;
                default:
                    throw new IllegalStateException("onRequestCompleted called for not completed request");
            }
        }

        public void b(Request request) {
        }
    }

    private class c implements RequestControllerObserver {
        private c() {
        }

        public void requestControllerDidFail(RequestController requestController, Exception exc) {
            Logger.a("RequestController", "Session authentication failed, failing _request");
            if (!RequestController.this.e.s()) {
                RequestController.this.e.a(exc);
                RequestController.this.e.f().a(RequestController.this.e);
            }
            f unused = RequestController.this.g = (f) null;
        }

        public void requestControllerDidReceiveResponse(RequestController requestController) {
            f unused = RequestController.this.g = (f) null;
        }
    }

    RequestController(Session session, RequestControllerObserver requestControllerObserver, boolean z) {
        if (requestControllerObserver == null) {
            throw new IllegalArgumentException("observer parameter cannot be null");
        }
        if (session == null) {
            this.f = Session.getCurrentSession();
        } else {
            this.f = session;
        }
        if (a || this.f != null) {
            this.d = requestControllerObserver;
            this.b = new b();
            this.h = z;
            j();
            return;
        }
        throw new AssertionError();
    }

    static Integer a(JSONObject jSONObject) throws JSONException {
        SetterIntent setterIntent = new SetterIntent();
        if (setterIntent.f(jSONObject, TrackerEvents.LABEL_ERROR, SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            return setterIntent.a((JSONObject) setterIntent.a(), "code", SetterIntent.KeyMode.USE_NULL_WHEN_NO_KEY, SetterIntent.ValueMode.ALLOWS_NULL_VALUE);
        }
        return null;
    }

    private Request b() {
        return this.e;
    }

    /* access modifiers changed from: private */
    public void c() {
        Logger.a("RequestController.invokeDidReceiveResponse", " observer = ", this.d);
        this.d.requestControllerDidReceiveResponse(this);
    }

    private void c(Request request) {
        this.e = request;
    }

    /* access modifiers changed from: private */
    public void d(Exception exc) {
        Logger.a("onRequestCompleted", "failed with exception: ", exc);
        c(exc);
        this.d.requestControllerDidFail(this, exc);
    }

    /* access modifiers changed from: package-private */
    public void a(Request request) {
        c(request);
        this.f.b().a(request);
    }

    /* access modifiers changed from: package-private */
    public void a(Exception exc) {
        Request b2 = b();
        if (b2 != null) {
            b2.a(exc);
            d(exc);
        }
    }

    /* access modifiers changed from: package-private */
    public abstract boolean a(Request request, Response response) throws Exception;

    /* access modifiers changed from: package-private */
    public void a_() {
        c((Exception) null);
        if (this.e != null) {
            if (!this.e.m() && !this.e.n()) {
                this.f.b().b(this.e);
            }
            this.e = null;
        }
    }

    /* access modifiers changed from: package-private */
    public void b(Request request) {
        if (Thread.currentThread() != Looper.getMainLooper().getThread()) {
            throw new IllegalStateException("you are not calling from the main thread context");
        }
        i();
        request.a(this.h);
        c(request);
        this.f.e();
        this.f.b().a(request);
    }

    /* access modifiers changed from: protected */
    public void b(Exception exc) {
        Logger.a("RequestController.invokeDelayedDidReceiveResponse", " observer = ", this.d);
        new a(this.d, true, exc).obtainMessage().sendToTarget();
    }

    /* access modifiers changed from: protected */
    public void c(Exception exc) {
        this.c = exc;
    }

    /* access modifiers changed from: package-private */
    public RequestControllerObserver d() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public b e() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public final Session f() {
        return this.f;
    }

    /* access modifiers changed from: package-private */
    public final User g() {
        return f().getUser();
    }

    /* access modifiers changed from: package-private */
    public Game getGame() {
        return f().getGame();
    }

    /* access modifiers changed from: protected */
    public void h() {
        Logger.a("RequestController.invokeDelayedDidReceiveResponse", " observer = ", this.d);
        new a(this.d, false, null).obtainMessage().sendToTarget();
    }

    /* access modifiers changed from: package-private */
    public void i() {
        Session.State c2 = f().c();
        if (c2 != Session.State.AUTHENTICATED && c2 != Session.State.AUTHENTICATING) {
            if (this.g == null) {
                this.g = new f(f(), new c());
            }
            this.g.b();
        }
    }

    /* access modifiers changed from: package-private */
    public void j() {
        if (getGame() == null) {
            throw new IllegalStateException("we do not allow game id to be null at all, please initialize Client with valid game id and secret");
        }
    }
}
