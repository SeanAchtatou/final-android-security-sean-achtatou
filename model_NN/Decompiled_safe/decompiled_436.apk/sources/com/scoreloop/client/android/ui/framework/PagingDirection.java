package com.scoreloop.client.android.ui.framework;

public enum PagingDirection {
    PAGE_TO_NEXT(2),
    PAGE_TO_OWN(3),
    PAGE_TO_PREV(1),
    PAGE_TO_RECENT(4),
    PAGE_TO_TOP(0);
    
    private int _flag;

    private PagingDirection(int shift) {
        this._flag = 1 << shift;
    }

    /* access modifiers changed from: package-private */
    public int combine(int flags) {
        return this._flag | flags;
    }

    /* access modifiers changed from: package-private */
    public boolean isPresentIn(int flags) {
        return (this._flag & flags) != 0;
    }
}
