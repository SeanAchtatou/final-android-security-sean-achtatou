package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.PublishedFor__1_1_0;
import com.scoreloop.client.android.core.PublishedFor__2_2_0;
import com.scoreloop.client.android.core.model.Achievement;
import com.scoreloop.client.android.core.model.AchievementsStore;
import com.scoreloop.client.android.core.model.Award;
import com.scoreloop.client.android.core.model.AwardList;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.SharedPreferencesAchievementsStore;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.RequestCompletionCallback;
import com.scoreloop.client.android.core.server.RequestMethod;
import com.scoreloop.client.android.core.server.Response;
import com.scoreloop.client.android.core.util.Logger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AchievementsController extends RequestController {
    private static String b = null;
    private List<Achievement> c;
    private AchievementsStore d;
    private AwardList e;
    private boolean f;
    private User g;

    private static final class a extends Request {
        private final AwardList a;
        private final Game b;
        private final User c;

        public a(RequestCompletionCallback requestCompletionCallback, User user, Game game, AwardList awardList) {
            super(requestCompletionCallback);
            this.c = user;
            this.b = game;
            this.a = awardList;
        }

        public String a() {
            return String.format("/service/games/%s/achievements", this.b.getIdentifier());
        }

        public JSONObject b() {
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put("user_id", this.c.getIdentifier());
                jSONObject.put("achievable_list_id", this.a != null ? this.a.a() : null);
                return jSONObject;
            } catch (JSONException e) {
                throw new IllegalStateException("Invalid achievement request", e);
            }
        }

        public RequestMethod c() {
            return RequestMethod.GET;
        }
    }

    @PublishedFor__1_1_0
    public AchievementsController(RequestControllerObserver requestControllerObserver) {
        this(null, requestControllerObserver);
    }

    @PublishedFor__1_1_0
    public AchievementsController(Session session, RequestControllerObserver requestControllerObserver) {
        super(session, requestControllerObserver, false);
        this.c = Collections.emptyList();
        this.d = null;
        this.e = null;
        this.f = true;
        this.g = null;
        this.g = g();
    }

    private void b() {
        if (getAwardList() == null) {
            throw new IllegalStateException("no SLAwards.bundle found in the assets");
        }
    }

    private void b(List<Achievement> list) throws JSONException {
        for (Achievement next : list) {
            getAchievementForAwardIdentifier(next.getAward().getIdentifier()).a(next, false);
        }
        a().b();
    }

    private void c() {
        ArrayList arrayList = new ArrayList();
        AwardList awardList = getAwardList();
        AchievementsStore a2 = a();
        for (Award next : awardList.getAwards()) {
            try {
                arrayList.add(new Achievement(awardList, next.getIdentifier(), a2));
            } catch (Exception e2) {
                e2.printStackTrace();
                Logger.b("can't create achievement from local store for award: " + next);
            }
        }
        a(arrayList);
    }

    private void k() {
        a aVar = new a(e(), getUser(), getGame(), getAwardList());
        aVar.a(120000);
        b(aVar);
    }

    /* access modifiers changed from: package-private */
    public AchievementsStore a() {
        if (this.d == null) {
            this.d = new SharedPreferencesAchievementsStore(f().d(), getGame().getIdentifier());
        }
        return this.d;
    }

    /* access modifiers changed from: protected */
    public void a(List<Achievement> list) {
        this.c = Collections.unmodifiableList(list);
    }

    /* access modifiers changed from: package-private */
    public boolean a(Request request, Response response) throws Exception {
        try {
            if (response.f() != 200) {
                throw new IllegalStateException("invalid response status: " + response.f());
            }
            HashMap hashMap = new HashMap();
            JSONArray jSONArray = response.e().getJSONArray("achievements");
            AwardList awardList = getAwardList();
            int length = jSONArray.length();
            for (int i = 0; i < length; i++) {
                Achievement achievement = new Achievement(awardList, jSONArray.getJSONObject(i).getJSONObject(Achievement.a));
                hashMap.put(achievement.getAward().getIdentifier(), achievement);
            }
            ArrayList arrayList = new ArrayList();
            for (Award next : awardList.getAwards()) {
                Achievement achievement2 = (Achievement) hashMap.get(next.getIdentifier());
                if (achievement2 == null) {
                    achievement2 = new Achievement(next);
                }
                arrayList.add(achievement2);
            }
            if (f().isOwnedByUser(getUser())) {
                c();
                b(arrayList);
                return true;
            }
            a(arrayList);
            return true;
        } catch (Exception e2) {
            throw new IllegalStateException(e2);
        }
    }

    @PublishedFor__1_1_0
    public int countAchievedAwards() {
        int i = 0;
        for (Achievement isAchieved : getAchievements()) {
            if (isAchieved.isAchieved()) {
                i++;
            }
        }
        return i;
    }

    @PublishedFor__1_1_0
    public Achievement getAchievementForAwardIdentifier(String str) {
        b();
        Award awardWithIdentifier = getAwardList().getAwardWithIdentifier(str);
        for (Achievement next : getAchievements()) {
            if (awardWithIdentifier.equals(next.getAward())) {
                return next;
            }
        }
        return null;
    }

    @PublishedFor__1_1_0
    public List<Achievement> getAchievements() {
        return this.c;
    }

    @PublishedFor__1_1_0
    public AwardList getAwardList() {
        if (this.e != null) {
            return this.e;
        }
        if (this.e == null && b == null) {
            this.e = getGame().e();
        }
        if (this.e == null) {
            this.e = AwardList.a(f().d(), getGame().getIdentifier(), b);
            if (b == null) {
                getGame().a(this.e);
            }
        }
        return this.e;
    }

    @PublishedFor__2_2_0
    public boolean getForceInitialSync() {
        return this.f;
    }

    @PublishedFor__1_1_0
    public User getUser() {
        return this.g;
    }

    @PublishedFor__2_2_0
    public boolean hadInitialSync() {
        return a().a();
    }

    @PublishedFor__1_1_0
    public void loadAchievements() {
        b();
        a_();
        if (!f().isOwnedByUser(getUser()) || (this.f && !a().a())) {
            k();
            return;
        }
        c();
        h();
    }

    @PublishedFor__2_2_0
    public void setForceInitialSync(boolean z) {
        this.f = z;
    }

    @PublishedFor__1_1_0
    public void setUser(User user) {
        if (user == null) {
            throw new IllegalArgumentException("user must not be null");
        }
        this.g = user;
    }
}
