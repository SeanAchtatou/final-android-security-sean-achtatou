package com.scoreloop.client.android.ui.framework;

import android.app.Activity;
import android.app.ActivityGroup;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.scoreloop.client.android.ui.framework.NavigationIntent;
import com.scoreloop.client.android.ui.framework.ScreenDescription;
import com.skyd.bestpuzzle.n1622.R;

public class ScreenActivity extends ActivityGroup implements ScreenActivityProtocol, View.OnClickListener {
    private static final String REGION_BODY = "body";
    private static final String REGION_HEADER = "header";
    private static final String STACK_ENTRY_REFERENCE_KEY = "stackEntryReference";
    public static final String TAG = "ScoreloopUI";

    public ScreenActivity() {
        super(false);
    }

    public void cleanOutSubactivities() {
        getLocalActivityManager().removeAllActivities();
    }

    /* access modifiers changed from: protected */
    public void display(ScreenDescription description, Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            ScreenManagerSingleton.get().displayInScreen(description, this, true);
        }
    }

    /* access modifiers changed from: protected */
    public void displayPreviousDescription() {
        ScreenManagerSingleton.get().displayPreviousDescription();
    }

    /* access modifiers changed from: protected */
    public void finishDisplay() {
        ScreenManagerSingleton.get().finishDisplay();
    }

    public Activity getActivity() {
        return this;
    }

    public boolean isNavigationAllowed(NavigationIntent navigationIntent) {
        boolean navigationAllowed = true;
        Activity activity = getLocalActivityManager().getActivity(REGION_HEADER);
        if (activity != null && (activity instanceof BaseActivity)) {
            navigationAllowed = true & ((BaseActivity) activity).isNavigationAllowed(navigationIntent);
        }
        Activity activity2 = getLocalActivityManager().getActivity(REGION_BODY);
        if (activity2 instanceof BaseActivity) {
            return navigationAllowed & ((BaseActivity) activity2).isNavigationAllowed(navigationIntent);
        }
        if (activity2 instanceof TabsActivity) {
            return navigationAllowed & ((TabsActivity) activity2).isNavigationAllowed(navigationIntent);
        }
        return navigationAllowed;
    }

    public void onClick(View view) {
        if (view == findViewById(R.id.sl_status_close_button)) {
            onStatusCloseClick(view);
        } else if (view == findViewById(R.id.sl_shortcuts)) {
            onShortcutClick((ShortcutView) view);
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.sl_screen);
        ImageView closeButton = (ImageView) findViewById(R.id.sl_status_close_button);
        if (closeButton != null) {
            closeButton.setEnabled(true);
            closeButton.setOnClickListener(this);
        }
        ((ShortcutView) findViewById(R.id.sl_shortcuts)).setOnSegmentClickListener(this);
        if (savedInstanceState == null) {
            ScreenManagerSingleton.get().displayStoredDescriptionInScreen(this);
            return;
        }
        int stackEntryReference = savedInstanceState.getInt(STACK_ENTRY_REFERENCE_KEY);
        int currentStackEntryReference = ScreenManagerSingleton.get().getCurrentStackEntryReference();
        if (stackEntryReference != currentStackEntryReference) {
            Log.w(TAG, String.format("onCreate with savedInstanceState: contains wrong stackEntryReference %s and current stack depth is %s", Integer.valueOf(stackEntryReference), Integer.valueOf(currentStackEntryReference)));
            ScreenManagerSingleton.get().finishDisplay();
            finish();
            return;
        }
        ScreenManagerSingleton.get().displayReferencedStackEntryInScreen(stackEntryReference, this);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || event.getRepeatCount() != 0) {
            return super.onKeyDown(keyCode, event);
        }
        displayPreviousDescription();
        return true;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        boolean result = super.onCreateOptionsMenu(menu);
        Activity bodyActivity = getLocalActivityManager().getActivity(REGION_BODY);
        if (bodyActivity != null && (bodyActivity instanceof OptionsMenuForActivityGroup)) {
            result |= ((OptionsMenuForActivityGroup) bodyActivity).onCreateOptionsMenuForActivityGroup(menu);
        }
        Activity headerActivity = getLocalActivityManager().getActivity(REGION_HEADER);
        if (headerActivity == null || !(headerActivity instanceof OptionsMenuForActivityGroup)) {
            return result;
        }
        return result | ((OptionsMenuForActivityGroup) headerActivity).onCreateOptionsMenuForActivityGroup(menu);
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean result = super.onPrepareOptionsMenu(menu);
        Activity headerActivity = getLocalActivityManager().getActivity(REGION_HEADER);
        if (headerActivity != null && (headerActivity instanceof OptionsMenuForActivityGroup)) {
            result |= ((OptionsMenuForActivityGroup) headerActivity).onPrepareOptionsMenuForActivityGroup(menu);
        }
        Activity bodyActivity = getLocalActivityManager().getActivity(REGION_BODY);
        if (bodyActivity != null && (bodyActivity instanceof OptionsMenuForActivityGroup)) {
            result |= ((OptionsMenuForActivityGroup) bodyActivity).onPrepareOptionsMenuForActivityGroup(menu);
        }
        ScreenManagerSingleton.get().onWillShowOptionsMenu();
        return result;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        Activity activity;
        Activity activity2;
        boolean consumed = super.onOptionsItemSelected(item);
        if (!consumed && (activity2 = getLocalActivityManager().getActivity(REGION_HEADER)) != null && (activity2 instanceof OptionsMenuForActivityGroup)) {
            consumed = ((OptionsMenuForActivityGroup) activity2).onOptionsItemSelectedForActivityGroup(item);
        }
        if (consumed || (activity = getLocalActivityManager().getActivity(REGION_BODY)) == null || !(activity instanceof OptionsMenuForActivityGroup)) {
            return consumed;
        }
        return ((OptionsMenuForActivityGroup) activity).onOptionsItemSelectedForActivityGroup(item);
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(STACK_ENTRY_REFERENCE_KEY, ScreenManagerSingleton.get().getCurrentStackEntryReference());
    }

    private void onShortcutClick(ShortcutView shortcutView) {
        final int selection = shortcutView.getSelectedSegment();
        NavigationIntent navigationIntent = new NavigationIntent(NavigationIntent.Type.SHORTCUT, new Runnable() {
            public void run() {
                ScreenDescription screenDescription = ScreenManagerSingleton.get().getCurrentDescription();
                ScreenDescription.ShortcutObserver observer = screenDescription.getShortcutObserver();
                if (observer != null && selection != -1) {
                    observer.onShortcut(screenDescription.getShortcutDescriptions().get(selection).getTextId());
                }
            }
        });
        if (!isNavigationAllowed(navigationIntent)) {
            shortcutView.switchToSegment(shortcutView.oldSelectedSegment);
        } else {
            navigationIntent.execute();
        }
    }

    private void onStatusCloseClick(View view) {
        finishDisplay();
    }

    public void setShortcuts(ScreenDescription description) {
        ShortcutView shortcutView = (ShortcutView) findViewById(R.id.sl_shortcuts);
        shortcutView.setDescriptions(this, description.getShortcutDescriptions());
        shortcutView.switchToSegment(description.getShortcutSelectionIndex());
    }

    public void startBody(ActivityDescription description, int anim) {
        ActivityHelper.startLocalActivity(this, description.getIntent(), REGION_BODY, R.id.sl_body, anim);
    }

    public void startEmptyBody() {
        ((ViewGroup) findViewById(R.id.sl_body)).removeAllViews();
    }

    public void startHeader(ActivityDescription description, int anim) {
        ActivityHelper.startLocalActivity(this, description.getIntent(), REGION_HEADER, R.id.sl_header, anim);
    }

    public void startNewScreen() {
        startActivity(new Intent(this, ScreenActivity.class));
    }

    public void startTabBody(ScreenDescription description, int anim) {
        Intent intent = new Intent(this, TabsActivity.class);
        intent.addFlags(536870912);
        ActivityHelper.startLocalActivity(this, intent, REGION_BODY, R.id.sl_body, anim);
    }
}
