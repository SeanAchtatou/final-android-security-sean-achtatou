package com.scoreloop.client.android.ui.component.agent;

import com.scoreloop.client.android.core.addon.RSSFeed;
import com.scoreloop.client.android.core.addon.RSSItem;
import com.scoreloop.client.android.ui.component.base.Constant;
import com.scoreloop.client.android.ui.framework.ValueStore;
import java.util.Collections;
import java.util.List;
import java.util.Set;

public class NewsAgent implements ValueStore.ValueSource {
    public static final String[] SUPPORTED_KEYS = {Constant.NEWS_NUMBER_UNREAD_ITEMS, Constant.NEWS_FEED};
    private RSSFeed _feed;

    public boolean isRetrieving() {
        if (this._feed != null) {
            return this._feed.getState() == RSSFeed.State.PENDING;
        }
        return false;
    }

    public void retrieve(final ValueStore valueStore) {
        if (this._feed == null) {
            this._feed = new RSSFeed(null);
        }
        this._feed.reloadOnNextRequest();
        this._feed.requestAllItems(new RSSFeed.Continuation() {
            public void withLoadedFeed(List<RSSItem> feed, Exception failure) {
                if (feed != null) {
                    valueStore.putValue(Constant.NEWS_FEED, feed);
                    int numberUnread = 0;
                    for (RSSItem item : feed) {
                        if (!item.hasPersistentReadFlag()) {
                            numberUnread++;
                        }
                    }
                    valueStore.putValue(Constant.NEWS_NUMBER_UNREAD_ITEMS, Integer.valueOf(numberUnread));
                }
            }
        }, false, null);
    }

    public void supportedKeys(Set<String> keys) {
        Collections.addAll(keys, SUPPORTED_KEYS);
    }
}
