package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.model.Device;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.Money;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.SocialProvider;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.RequestCompletionCallback;
import com.scoreloop.client.android.core.server.RequestMethod;
import com.scoreloop.client.android.core.server.Response;
import com.scoreloop.client.android.ui.component.base.Constant;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

class f extends RequestController {
    /* access modifiers changed from: private */
    public b b;
    private final RequestControllerObserver c = new a();

    private final class a implements RequestControllerObserver {
        static final /* synthetic */ boolean a = (!f.class.desiredAssertionStatus());

        private a() {
        }

        public void requestControllerDidFail(RequestController requestController, Exception exc) {
            Session f = f.this.f();
            if (a || f.c() == Session.State.AUTHENTICATING) {
                f.a(Session.State.FAILED);
                f.this.a(exc);
                b unused = f.this.b = (b) null;
                return;
            }
            throw new AssertionError();
        }

        public void requestControllerDidReceiveResponse(RequestController requestController) {
            Session f = f.this.f();
            if (!a && f.c() != Session.State.AUTHENTICATING) {
                throw new AssertionError();
            } else if (f.a().getIdentifier() != null) {
                f.getUser().a(f.a().getIdentifier());
                b unused = f.this.b = (b) null;
            } else {
                throw new IllegalStateException();
            }
        }
    }

    private static class b extends Request {
        private final Device a;
        private final Game b;

        public b(RequestCompletionCallback requestCompletionCallback, Game game, Device device) {
            super(requestCompletionCallback);
            this.b = game;
            this.a = device;
        }

        public String a() {
            if (this.b == null) {
                return "/service/session";
            }
            return String.format("/service/games/%s/session", this.b.getIdentifier());
        }

        public JSONObject b() {
            JSONObject jSONObject = new JSONObject();
            JSONObject jSONObject2 = new JSONObject();
            try {
                jSONObject2.put("device_id", this.a.getIdentifier());
                jSONObject.put(Constant.USER, jSONObject2);
                return jSONObject;
            } catch (JSONException e) {
                throw new IllegalStateException("Invalid device data");
            }
        }

        public RequestMethod c() {
            return RequestMethod.POST;
        }
    }

    f(Session session, RequestControllerObserver requestControllerObserver) {
        super(session, requestControllerObserver, false);
    }

    private List<Money> a(Money money) {
        String d = money.d();
        return money.compareTo(new Money(d, new BigDecimal(10000))) < 0 ? a(d) : money.compareTo(new Money(d, new BigDecimal(100000))) < 0 ? c(d) : b(d);
    }

    private List<Money> a(String str) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new Money(str, new BigDecimal(100)));
        arrayList.add(new Money(str, new BigDecimal(200)));
        arrayList.add(new Money(str, new BigDecimal(500)));
        arrayList.add(new Money(str, new BigDecimal(1000)));
        arrayList.add(new Money(str, new BigDecimal(2000)));
        arrayList.add(new Money(str, new BigDecimal(5000)));
        return arrayList;
    }

    private List<Money> b(String str) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new Money(str, new BigDecimal(1000)));
        arrayList.add(new Money(str, new BigDecimal(2500)));
        arrayList.add(new Money(str, new BigDecimal(5000)));
        arrayList.add(new Money(str, new BigDecimal(10000)));
        arrayList.add(new Money(str, new BigDecimal(50000)));
        arrayList.add(new Money(str, new BigDecimal(100000)));
        return arrayList;
    }

    private List<Money> c(String str) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new Money(str, new BigDecimal(500)));
        arrayList.add(new Money(str, new BigDecimal(1000)));
        arrayList.add(new Money(str, new BigDecimal(2500)));
        arrayList.add(new Money(str, new BigDecimal(5000)));
        arrayList.add(new Money(str, new BigDecimal(10000)));
        arrayList.add(new Money(str, new BigDecimal(20000)));
        return arrayList;
    }

    /* access modifiers changed from: package-private */
    public boolean a(Request request, Response response) throws Exception {
        Session f = f();
        int f2 = response.f();
        JSONObject e = response.e();
        JSONObject optJSONObject = e.optJSONObject(User.a);
        if ((f2 == 200 || f2 == 201) && optJSONObject != null) {
            User user = f.getUser();
            user.a(optJSONObject);
            user.a(true);
            SocialProvider.a(user, optJSONObject);
            f.a(a(user.c()));
            f.a(e);
            f.a(Session.State.AUTHENTICATED);
            return true;
        }
        f.a(Session.State.FAILED);
        throw new Exception("Session authentication request failed with status: " + f2);
    }

    /* access modifiers changed from: package-private */
    public void b() {
        Session f = f();
        Device a2 = f.a();
        if (f.c() == Session.State.FAILED) {
            a2.b(null);
        }
        b bVar = new b(e(), getGame(), a2);
        if (a2.getIdentifier() == null && this.b == null) {
            this.b = new b(f(), this.c);
        }
        a_();
        f.a(Session.State.AUTHENTICATING);
        if (a2.getIdentifier() == null) {
            this.b.c();
        }
        a(bVar);
    }
}
