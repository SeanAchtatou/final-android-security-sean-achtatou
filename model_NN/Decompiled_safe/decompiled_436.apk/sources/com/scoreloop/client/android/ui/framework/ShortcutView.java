package com.scoreloop.client.android.ui.framework;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.skyd.bestpuzzle.n1622.R;
import java.util.List;

public class ShortcutView extends SegmentedView {
    private List<ShortcutDescription> _shortcutDescriptions = null;

    public ShortcutView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void setDescriptions(Activity activity, List<ShortcutDescription> shortcutDescriptions) {
        LinearLayout.LayoutParams lp;
        if (this._shortcutDescriptions == null || !this._shortcutDescriptions.equals(shortcutDescriptions)) {
            removeAllViews();
            this._shortcutDescriptions = shortcutDescriptions;
            Display display = activity.getWindowManager().getDefaultDisplay();
            for (ShortcutDescription shortcutDescription : shortcutDescriptions) {
                ViewGroup viewGroup = (ViewGroup) activity.getLayoutInflater().inflate((int) R.layout.sl_tab_shortcut, (ViewGroup) null);
                int margin = (int) getResources().getDimension(R.dimen.sl_margin_shortcut);
                DisplayMetrics metrics = new DisplayMetrics();
                display.getMetrics(metrics);
                int rotation = display.getOrientation();
                if (metrics.widthPixels > metrics.heightPixels || rotation == 3 || rotation == 1) {
                    lp = new LinearLayout.LayoutParams(-1, 0, 1.0f);
                    lp.gravity = 17;
                    lp.leftMargin = margin;
                    lp.rightMargin = margin;
                } else {
                    lp = new LinearLayout.LayoutParams(0, -2, 1.0f);
                    lp.gravity = 17;
                    lp.bottomMargin = margin;
                    lp.topMargin = margin;
                }
                viewGroup.setLayoutParams(lp);
                viewGroup.setId(shortcutDescription.getTextId());
                ((ImageView) viewGroup.findViewById(R.id.sl_image_tab_view)).setImageResource(shortcutDescription.getImageId());
                addView(viewGroup);
            }
            prepareUsage();
            return;
        }
        this._shortcutDescriptions = shortcutDescriptions;
    }

    /* Debug info: failed to restart local var, previous not found, register: 4 */
    /* access modifiers changed from: protected */
    public void setSegmentEnabled(int segment, boolean enabled) {
        View view = getChildAt(segment);
        ShortcutDescription shortcutDescription = this._shortcutDescriptions.get(segment);
        if (enabled) {
            ((ImageView) view.findViewById(R.id.sl_image_tab_view)).setImageResource(shortcutDescription.getActiveImageId());
            view.setBackgroundResource(R.drawable.sl_shortcut_highlight);
            return;
        }
        ((ImageView) view.findViewById(R.id.sl_image_tab_view)).setImageResource(shortcutDescription.getImageId());
        view.setBackgroundDrawable(null);
    }
}
