package com.scoreloop.client.android.ui.component.market;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import com.scoreloop.client.android.core.controller.GamesController;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.ui.component.base.ComponentHeaderActivity;
import com.scoreloop.client.android.ui.component.base.Constant;
import com.scoreloop.client.android.ui.component.base.TrackerEvents;
import com.scoreloop.client.android.ui.framework.ValueStore;
import com.scoreloop.client.android.ui.util.ImageDownloader;
import com.skyd.bestpuzzle.n1622.R;
import java.util.List;

public class MarketHeaderActivity extends ComponentHeaderActivity implements View.OnClickListener, ValueStore.Observer {
    private GamesController _gamesController;

    public void onClick(View view) {
        Game featuredGame = (Game) getScreenValues().getValue(Constant.FEATURED_GAME);
        if (featuredGame != null) {
            getTracker().trackEvent(TrackerEvents.CAT_NAVI, TrackerEvents.NAVI_HEADER_GAME_FEATURED, featuredGame.getName(), 0);
            display(getFactory().createGameDetailScreenDescription(featuredGame));
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.sl_header_market);
        setTitle(getString(R.string.sl_market));
        setSubTitle(getString(R.string.sl_market_description));
        getImageView().setImageResource(R.drawable.sl_header_icon_market);
        addObservedKeys(Constant.FEATURED_GAME, Constant.FEATURED_GAME_NAME, Constant.FEATURED_GAME_IMAGE_URL, Constant.FEATURED_GAME_PUBLISHER);
        this._gamesController = new GamesController(getRequestControllerObserver());
        this._gamesController.setRangeLength(1);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this._gamesController.loadRangeForFeatured();
    }

    public void onValueChanged(ValueStore valueStore, String key, Object oldValue, Object newValue) {
        if (oldValue == newValue) {
            return;
        }
        if (key.equals(Constant.FEATURED_GAME_IMAGE_URL)) {
            ImageDownloader.downloadImage((String) newValue, getResources().getDrawable(R.drawable.sl_header_icon_market), getImageView(), null);
        } else if (key.equals(Constant.FEATURED_GAME_NAME)) {
            setTitle((String) newValue);
        } else if (key.equals(Constant.FEATURED_GAME_PUBLISHER)) {
            setSubTitle((String) newValue);
        }
    }

    public void onValueSetDirty(ValueStore valueStore, String key) {
        if (Constant.FEATURED_GAME.equals(key)) {
            getScreenValues().retrieveValue(key, ValueStore.RetrievalMode.NOT_DIRTY, null);
        } else if (Constant.FEATURED_GAME_IMAGE_URL.equals(key)) {
            getScreenValues().retrieveValue(key, ValueStore.RetrievalMode.NOT_DIRTY, null);
        } else if (Constant.FEATURED_GAME_NAME.equals(key)) {
            getScreenValues().retrieveValue(key, ValueStore.RetrievalMode.NOT_DIRTY, null);
        } else if (Constant.FEATURED_GAME_PUBLISHER.equals(key)) {
            getScreenValues().retrieveValue(key, ValueStore.RetrievalMode.NOT_DIRTY, null);
        }
    }

    public void requestControllerDidReceiveResponseSafe(RequestController aRequestController) {
        List<Game> featuredGames = this._gamesController.getGames();
        if (featuredGames.size() > 0) {
            ValueStore store = getScreenValues();
            Game game = featuredGames.get(0);
            store.putValue(Constant.FEATURED_GAME, game);
            store.putValue(Constant.FEATURED_GAME_NAME, game.getName());
            store.putValue(Constant.FEATURED_GAME_PUBLISHER, game.getPublisherName());
            store.putValue(Constant.FEATURED_GAME_IMAGE_URL, game.getImageUrl());
            showControlIcon(R.drawable.sl_button_arrow);
        }
    }

    private void showControlIcon(int resId) {
        ((ImageView) findViewById(R.id.sl_control_icon)).setImageResource(resId);
        findViewById(R.id.sl_header_layout).setOnClickListener(this);
    }
}
