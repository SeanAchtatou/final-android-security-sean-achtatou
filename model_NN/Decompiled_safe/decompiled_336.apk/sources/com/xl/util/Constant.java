package com.xl.util;

public class Constant {
    public static final String APPROOT = "/sdcard/acebook/";
    public static final String BMPATH = "bmpath";
    public static final String BOOKAUTHOR = "author";
    public static final String BOOKENC = "enc";
    public static final String BOOKMARK = "bm.xml";
    public static final String BOOKNAME = "name";
    public static final String BOOKSIZE = "fsize";
    public static final String CHAPTER = "chapter";
    public static final String FONTSIZE = "fontsize";
    public static final String MYBOOK = "mybook/";
    public static final String PAGESIZE = "pagesize";
    public static final String TMP = "tmp/";
    public static final String YPOSITION = "yposition";
}
