package com.xl.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class FileReaderLocalFile {
    private boolean bEncoded = false;
    private boolean bFirst = false;
    private boolean bLast = false;
    private boolean bValid = false;
    private byte[] btPage = null;
    private String filePath = "";
    private FileInputStream fs = null;
    private long iCurrPosition = 0;
    private int iOnePage = 100;
    private long iPrevPosition = 0;
    private int iShift = 0;
    private long lFileLength = -1;
    private String strEncode = "";

    public FileReaderLocalFile(String filePath2, int iOnePage2, String encode, boolean bEnc) {
        this.strEncode = encode;
        this.filePath = filePath2;
        this.iOnePage = iOnePage2;
        this.bEncoded = bEnc;
        this.btPage = new byte[iOnePage2];
        this.iShift = 0;
        this.bValid = init();
        this.bFirst = true;
        this.iCurrPosition = 0;
    }

    private boolean init() {
        try {
            this.fs = new FileInputStream(this.filePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (this.fs != null) {
            return true;
        }
        System.out.println("is is null");
        return false;
    }

    public long getSize() {
        this.lFileLength = new File(this.filePath).length();
        return this.lFileLength;
    }

    public long getSizeOld() {
        if (this.lFileLength > 0) {
            return this.lFileLength;
        }
        long lSize = 0;
        try {
            lSize = this.fs.skip(1000000000000L);
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.lFileLength = lSize;
        try {
            this.fs = new FileInputStream(this.filePath);
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        return this.lFileLength;
    }

    public String getNextPage() {
        IOException e;
        String strPageOut;
        int iOldShift = this.iShift;
        int i = 0;
        while (i < this.iShift) {
            try {
                this.btPage[i] = this.btPage[(this.iOnePage - this.iShift) + i];
                i++;
            } catch (IOException e2) {
                e = e2;
                e.printStackTrace();
                return "";
            }
        }
        int ipage = this.fs.read(this.btPage, this.iShift, this.iOnePage - this.iShift);
        GenUtil.systemPrintln("xxllxl:" + String.valueOf(ipage) + "," + String.valueOf(this.iOnePage));
        if (ipage <= 0) {
            this.bLast = true;
            return "";
        }
        if (this.iCurrPosition > 0) {
            this.bFirst = false;
        } else {
            this.bFirst = true;
        }
        byte[] btTmp = new byte[(this.iShift + ipage)];
        for (int i2 = 0; i2 < this.iShift + ipage; i2++) {
            btTmp[i2] = this.btPage[i2];
        }
        if (this.bEncoded) {
            byte[] btBuffers = XMEnDecrypt.XMDecryptString(btTmp);
            int iStartOffset = 0;
            if ((btBuffers[0] & 224) != 224 && (btBuffers[0] & 128) == 128) {
                iStartOffset = ((btBuffers[1] & 224) == 224 || (btBuffers[1] & 128) != 128) ? 0 + 1 : 0 + 2;
            }
            if ((btBuffers[btBuffers.length - 2] & 224) == 224 && (btBuffers[btBuffers.length - 1] & 128) == 128) {
                btBuffers[btBuffers.length - 2] = 0;
                btBuffers[btBuffers.length - 1] = 0;
                ipage -= 2;
                GenUtil.systemPrintln("ipage-2");
                this.iShift = 2;
            } else if ((btBuffers[btBuffers.length - 1] & 224) == 224) {
                GenUtil.systemPrintln("utf8");
                btBuffers[btBuffers.length - 1] = 0;
                ipage--;
                GenUtil.systemPrintln("ipage-1");
                this.iShift = 1;
            } else {
                this.iShift = 0;
            }
            String strPageOut2 = new String(btBuffers, iStartOffset, (ipage + iOldShift) - iStartOffset, this.strEncode);
            try {
                GenUtil.systemPrintln("content: " + strPageOut2);
                strPageOut = strPageOut2;
            } catch (IOException e3) {
                e = e3;
                e.printStackTrace();
                return "";
            }
        } else {
            strPageOut = new String(this.btPage, this.strEncode);
        }
        this.iPrevPosition = this.iCurrPosition;
        this.iCurrPosition += (long) (ipage + iOldShift);
        return strPageOut;
    }

    public String getPrePage() {
        this.bLast = false;
        long lPosition = this.iCurrPosition - ((long) (this.iOnePage * 2));
        GenUtil.systemPrintln("iCurrPosition = " + this.iCurrPosition);
        GenUtil.systemPrintln("iOnePage = " + this.iOnePage);
        GenUtil.systemPrintln("lPosition = " + lPosition);
        this.iShift = 0;
        return getPage(lPosition);
    }

    public String getPage(long lPosition) {
        if (this.iCurrPosition != 0) {
            try {
                this.fs = new FileInputStream(this.filePath);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        long lSkip = lPosition;
        if (lPosition < 0) {
            lSkip = 0;
            this.bFirst = true;
        }
        this.iCurrPosition = lSkip;
        try {
            this.fs.skip(lSkip);
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        return getNextPage();
    }

    public int getPageStatus() {
        if (this.bLast) {
            return 1;
        }
        if (this.bFirst) {
            return 2;
        }
        return 0;
    }

    public boolean getStatus() {
        return this.bValid;
    }

    public long getCurrPosition() {
        return this.iCurrPosition;
    }

    public long getPrevPosition() {
        return this.iPrevPosition;
    }
}
