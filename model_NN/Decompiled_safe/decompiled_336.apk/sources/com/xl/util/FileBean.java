package com.xl.util;

import java.io.Serializable;

public class FileBean implements Serializable {
    private String name;
    private String signiture;
    private String size;
    private String subject;
    private String time;
    private String type;
    private String url;
    private String ziped;

    public String getSubject() {
        return this.subject;
    }

    public void setSubject(String subject2) {
        this.subject = subject2;
    }

    public String getSigniture() {
        return this.signiture;
    }

    public void setSigniture(String signiture2) {
        this.signiture = signiture2;
    }

    public String getTime() {
        return this.time;
    }

    public void setTime(String time2) {
        this.time = time2;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url2) {
        this.url = url2;
    }

    public String getSize() {
        return this.size;
    }

    public void setSize(String size2) {
        this.size = size2;
    }

    public String getZiped() {
        return this.ziped;
    }

    public void setZiped(String ziped2) {
        this.ziped = ziped2;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type2) {
        this.type = type2;
    }
}
