package com.skyd.core.android;

import android.app.Activity;
import android.content.ContextWrapper;
import android.content.Intent;
import java.util.ArrayList;

public class VoiceInput {
    public static void Call(Activity a, String tip, int maxResult, int requestCode) {
        Intent intent = new Intent("android.speech.action.RECOGNIZE_SPEECH");
        intent.putExtra("android.speech.extra.LANGUAGE_MODEL", "free_form");
        intent.putExtra("android.speech.extra.PROMPT", tip);
        intent.putExtra("android.speech.extra.MAX_RESULTS", maxResult);
        a.startActivityForResult(intent, requestCode);
    }

    public static void Call(Activity a, int tipResID, int maxResult, int requestCode) {
        Call(a, a.getResources().getString(tipResID), maxResult, requestCode);
    }

    public static ArrayList<String> GetInputStrings(Intent data) {
        return data.getStringArrayListExtra("android.speech.extra.RESULTS");
    }

    public static boolean IsSupport(ContextWrapper c) {
        return c.getPackageManager().queryIntentActivities(new Intent("android.speech.action.RECOGNIZE_SPEECH"), 0).size() != 0;
    }
}
