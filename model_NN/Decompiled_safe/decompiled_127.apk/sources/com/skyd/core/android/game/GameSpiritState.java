package com.skyd.core.android.game;

import com.skyd.core.vector.Vector2DF;

public class GameSpiritState extends GameNodeObject implements IGameImageHolder {
    private GameObjectList<GameAnimation> _AnimationList = new GameObjectList<>(this);
    private int _DisplayAnimationIndex = 0;
    private String _Name = null;
    private Vector2DF _PositionOffset = new Vector2DF(0.0f, 0.0f);

    public GameSpiritState() {
    }

    public GameSpiritState(GameAnimation... animations) {
        for (GameAnimation a : animations) {
            try {
                a.setParent(this);
            } catch (GameException e) {
                e.printStackTrace();
            }
            getAnimationList().add(a);
        }
    }

    public String getName() {
        return this._Name;
    }

    public void setName(String value) {
        this._Name = value;
    }

    public void setNameToDefault() {
        setName(null);
    }

    public Vector2DF getPositionOffset() {
        return this._PositionOffset;
    }

    public void setPositionOffset(Vector2DF value) {
        this._PositionOffset = value;
    }

    public void setPositionOffsetToDefault() {
        setPositionOffset(new Vector2DF(0.0f, 0.0f));
    }

    public GameObjectList<GameAnimation> getAnimationList() {
        return this._AnimationList;
    }

    public void setAnimationList(GameObjectList<GameAnimation> value) {
        this._AnimationList = value;
    }

    public void setAnimationListToDefault() {
        setAnimationList(new GameObjectList(this));
    }

    public int getDisplayAnimationIndex() {
        return this._DisplayAnimationIndex;
    }

    public void setDisplayAnimationIndex(int value) {
        this._DisplayAnimationIndex = value;
    }

    public void setDisplayAnimationIndexToDefault() {
        setDisplayAnimationIndex(0);
    }

    public void toNextAnimation() {
        int i = this._DisplayAnimationIndex + 1;
        this._DisplayAnimationIndex = i;
        if (i >= this._AnimationList.size()) {
            this._DisplayAnimationIndex = 0;
        }
    }

    public void toPrevAnimation() {
        int i = this._DisplayAnimationIndex - 1;
        this._DisplayAnimationIndex = i;
        if (i < 0) {
            this._DisplayAnimationIndex = this._AnimationList.size() - 1;
        }
    }

    public void toRandomAnimation() {
        this._DisplayAnimationIndex = GameMaster.getRandom().nextInt(this._AnimationList.size());
    }

    public GameAnimation getDisplayAnimation() {
        return this._AnimationList.get(this._DisplayAnimationIndex);
    }

    public GameImage getDisplayImage() {
        return getDisplayAnimation().getDisplayImage();
    }

    public GameObject getDisplayContentChild() {
        return getDisplayAnimation();
    }

    public void updateChilds() {
        if (this._AnimationList.size() > 0) {
            getDisplayAnimation().update();
        }
    }
}
