package com.skyd.core.android.game;

import android.graphics.Canvas;
import android.graphics.Matrix;
import com.skyd.core.vector.Vector2DF;

public abstract class GameRootObject extends GameObject {
    private Vector2DF _DisplayAreaFixScaleReferenceValue = new Vector2DF(1.0f, 1.0f);
    private Vector2DF _DisplayAreaSize = new Vector2DF(0.0f, 0.0f);
    private boolean _IsFillParentDisplayArea = true;
    private boolean _IsMatchHeight = true;
    private boolean _IsMatchWidth = true;
    private IGameDisplayArea _ParentDisplayArea = null;
    private Vector2DF _StandardDisplayAreaSize = new Vector2DF(480.0f, 800.0f);

    public GameRootObject() {
        getSize().reset(100.0f, 100.0f);
        setVisible(false);
    }

    public boolean getIsFillParentDisplayArea() {
        return this._IsFillParentDisplayArea;
    }

    public void setIsFillParentDisplayArea(boolean value) {
        this._IsFillParentDisplayArea = value;
    }

    public void setIsFillParentDisplayAreaToDefault() {
        setIsFillParentDisplayArea(true);
    }

    public IGameDisplayArea getParentDisplayArea() {
        return this._ParentDisplayArea;
    }

    /* access modifiers changed from: protected */
    public void setParentDisplayArea(IGameDisplayArea value) {
        this._ParentDisplayArea = value;
    }

    /* access modifiers changed from: protected */
    public void setParentDisplayAreaToDefault() {
        setParentDisplayArea(null);
    }

    public GameObject getParent() {
        return null;
    }

    public void setParent(GameObject value) throws GameException {
        throw new GameException(this, "此为根对象，不能设置父对象", null);
    }

    public GameObject getDisplayContentChild() {
        return null;
    }

    public Vector2DF getDisplayAreaSize() {
        return this._DisplayAreaSize;
    }

    /* access modifiers changed from: protected */
    public void setDisplayAreaSize(Vector2DF value) {
        this._DisplayAreaSize = value;
    }

    /* access modifiers changed from: protected */
    public void setDisplayAreaSizeToDefault() {
        setDisplayAreaSize(new Vector2DF(0.0f, 0.0f));
    }

    public Vector2DF getStandardDisplayAreaSize() {
        return this._StandardDisplayAreaSize;
    }

    public void setStandardDisplayAreaSize(Vector2DF value) {
        this._StandardDisplayAreaSize = value;
    }

    public void setStandardDisplayAreaSizeToDefault() {
        setStandardDisplayAreaSize(new Vector2DF(480.0f, 800.0f));
    }

    public Vector2DF getDisplayAreaFixScaleReferenceValue() {
        return this._DisplayAreaFixScaleReferenceValue;
    }

    /* access modifiers changed from: protected */
    public void setDisplayAreaFixScaleReferenceValue(Vector2DF value) {
        this._DisplayAreaFixScaleReferenceValue = value;
    }

    /* access modifiers changed from: protected */
    public void setDisplayAreaFixScaleReferenceValueToDefault() {
        setDisplayAreaFixScaleReferenceValue(new Vector2DF(1.0f, 1.0f));
    }

    public boolean onUpdating() {
        updateDisplayAreaSize();
        updateDisplayAreaScaleReferenceValue();
        return super.onUpdating();
    }

    /* access modifiers changed from: protected */
    public void updateDisplayAreaScaleReferenceValue() {
        Vector2DF d = this._DisplayAreaSize;
        Vector2DF s = this._StandardDisplayAreaSize;
        float xs = (d.getX() * 1.0f) / s.getX();
        float ys = (d.getY() * 1.0f) / s.getY();
        if (this._IsFillParentDisplayArea) {
            this._DisplayAreaFixScaleReferenceValue.reset(xs, ys);
        } else if (this._IsMatchHeight && this._IsMatchWidth) {
            float min = Math.min(xs, ys);
            this._DisplayAreaFixScaleReferenceValue.reset(min, min);
        } else if (this._IsMatchHeight) {
            this._DisplayAreaFixScaleReferenceValue.reset(ys, ys);
        } else {
            this._DisplayAreaFixScaleReferenceValue.reset(xs, xs);
        }
    }

    /* access modifiers changed from: protected */
    public void updateDisplayAreaSize() {
        this._DisplayAreaSize.setX((float) getParentDisplayArea().getWidth());
        this._DisplayAreaSize.setY((float) getParentDisplayArea().getHeight());
    }

    public boolean getIsMatchWidth() {
        return this._IsMatchWidth;
    }

    public void setIsMatchWidth(boolean value) {
        this._IsMatchWidth = value;
    }

    public void setIsMatchWidthToDefault() {
        setIsMatchWidth(true);
    }

    public boolean getIsMatchHeight() {
        return this._IsMatchHeight;
    }

    public void setIsMatchHeight(boolean value) {
        this._IsMatchHeight = value;
    }

    public void setIsMatchHeightToDefault() {
        setIsMatchHeight(true);
    }

    public Vector2DF transitionPosition(Vector2DF v) {
        return v.getClone();
    }

    public GameRootObject getRoot() {
        return this;
    }

    /* access modifiers changed from: protected */
    public void operateCanvas(Canvas c) {
    }

    /* access modifiers changed from: protected */
    public void operateCanvasForParentAbsoluteSize(Canvas c) {
    }

    /* access modifiers changed from: protected */
    public void operateMatrixForParentAbsoluteSize(Matrix m) {
    }
}
