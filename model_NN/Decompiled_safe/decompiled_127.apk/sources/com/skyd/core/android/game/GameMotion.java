package com.skyd.core.android.game;

import java.util.ArrayList;
import java.util.Iterator;

public abstract class GameMotion {
    OnIsRunningChangedListener BeforeTransformationRunningChangeListener = new OnIsRunningChangedListener() {
        public void OnIsRunningChangedEvent(Object sender, boolean newValue) {
            if (!newValue) {
                GameMotion.this.setIsBeforeTransformationCompleted(true);
            }
        }
    };
    private GameMotion _BeforeTransformation = null;
    private ArrayList<OnCompleteListener> _CompleteListenerList = null;
    private int _Duration = 0;
    private int _FrameCounting = 0;
    private boolean _IsBeforeTransformationCompleted = false;
    private boolean _IsRunning = false;
    private ArrayList<OnIsRunningChangedListener> _IsRunningChangedListenerList = null;
    private int _LoopCounting = 0;
    private int _LoopTime = 0;

    public interface OnCompleteListener {
        void OnCompleteEvent(Object obj, GameObject gameObject);
    }

    public interface OnIsRunningChangedListener {
        void OnIsRunningChangedEvent(Object obj, boolean z);
    }

    /* access modifiers changed from: protected */
    public abstract void restart(GameObject gameObject);

    /* access modifiers changed from: protected */
    public abstract void updateSelf(GameObject gameObject);

    /* access modifiers changed from: protected */
    public abstract void updateTarget(GameObject gameObject);

    public GameMotion() {
    }

    public GameMotion(int duration) {
        setDuration(duration);
    }

    public int getDuration() {
        return this._Duration;
    }

    public void setDuration(int value) {
        this._Duration = value;
    }

    public void setDurationToDefault() {
        setDuration(0);
    }

    public int getFrameCounting() {
        return this._FrameCounting;
    }

    /* access modifiers changed from: protected */
    public void setFrameCounting(int value) {
        this._FrameCounting = value;
    }

    /* access modifiers changed from: protected */
    public void setFrameCountingToDefault() {
        setFrameCounting(0);
    }

    public int getLoopCounting() {
        return this._LoopCounting;
    }

    /* access modifiers changed from: protected */
    public void setLoopCounting(int value) {
        this._LoopCounting = value;
    }

    /* access modifiers changed from: protected */
    public void setLoopCountingToDefault() {
        setLoopCounting(0);
    }

    public int getLoopTime() {
        return this._LoopTime;
    }

    public void setLoopTime(int value) {
        this._LoopTime = value;
    }

    public void setLoopTimeToDefault() {
        setLoopTime(0);
    }

    /* access modifiers changed from: protected */
    public boolean getIsComplete(GameObject obj) {
        return getFrameCounting() >= getDuration();
    }

    public final void update(GameObject obj) {
        if (!getIsRunning()) {
            return;
        }
        if (getBeforeTransformation() == null || getIsBeforeTransformationCompleted()) {
            updateSelf(obj);
            updateTarget(obj);
            setFrameCounting(getFrameCounting() + 1);
            if (getIsComplete(obj)) {
                onComplete(obj);
                setLoopCounting(getLoopCounting() + 1);
                if (getLoopTime() < 0) {
                    return;
                }
                if (getLoopCounting() > getLoopTime()) {
                    stop();
                    return;
                }
                setFrameCounting(0);
                restart(obj);
            }
        }
    }

    public void start() {
        setIsRunning(true);
    }

    public void stop() {
        setIsRunning(false);
    }

    public boolean getIsRunning() {
        return this._IsRunning;
    }

    /* access modifiers changed from: protected */
    public void setIsRunning(boolean value) {
        if (this._IsRunning != value) {
            onIsRunningChanged(value);
        }
        this._IsRunning = value;
    }

    /* access modifiers changed from: protected */
    public void setIsRunningToDefault() {
        setIsRunning(false);
    }

    public boolean addOnCompleteListener(OnCompleteListener listener) {
        if (this._CompleteListenerList == null) {
            this._CompleteListenerList = new ArrayList<>();
        } else if (this._CompleteListenerList.contains(listener)) {
            return false;
        }
        this._CompleteListenerList.add(listener);
        return true;
    }

    public boolean removeOnCompleteListener(OnCompleteListener listener) {
        if (this._CompleteListenerList == null || !this._CompleteListenerList.contains(listener)) {
            return false;
        }
        this._CompleteListenerList.remove(listener);
        return true;
    }

    public void clearOnCompleteListeners() {
        if (this._CompleteListenerList != null) {
            this._CompleteListenerList.clear();
        }
    }

    /* access modifiers changed from: protected */
    public void onComplete(GameObject targetObj) {
        if (this._CompleteListenerList != null) {
            Iterator<OnCompleteListener> it = this._CompleteListenerList.iterator();
            while (it.hasNext()) {
                it.next().OnCompleteEvent(this, targetObj);
            }
        }
    }

    public boolean addOnIsRunningChangedListener(OnIsRunningChangedListener listener) {
        if (this._IsRunningChangedListenerList == null) {
            this._IsRunningChangedListenerList = new ArrayList<>();
        } else if (this._IsRunningChangedListenerList.contains(listener)) {
            return false;
        }
        this._IsRunningChangedListenerList.add(listener);
        return true;
    }

    public boolean removeOnIsRunningChangedListener(OnIsRunningChangedListener listener) {
        if (this._IsRunningChangedListenerList == null || !this._IsRunningChangedListenerList.contains(listener)) {
            return false;
        }
        this._IsRunningChangedListenerList.remove(listener);
        return true;
    }

    public void clearOnIsRunningChangedListeners() {
        if (this._IsRunningChangedListenerList != null) {
            this._IsRunningChangedListenerList.clear();
        }
    }

    /* access modifiers changed from: protected */
    public void onIsRunningChanged(boolean newValue) {
        if (this._IsRunningChangedListenerList != null) {
            Iterator<OnIsRunningChangedListener> it = this._IsRunningChangedListenerList.iterator();
            while (it.hasNext()) {
                it.next().OnIsRunningChangedEvent(this, newValue);
            }
        }
    }

    public GameMotion getBeforeTransformation() {
        return this._BeforeTransformation;
    }

    public void setBeforeTransformation(GameMotion value) {
        if (value != null) {
            value.addOnIsRunningChangedListener(this.BeforeTransformationRunningChangeListener);
        }
        if (this._BeforeTransformation != null) {
            this._BeforeTransformation.removeOnIsRunningChangedListener(this.BeforeTransformationRunningChangeListener);
        }
        this._BeforeTransformation = value;
    }

    public void setBeforeTransformationToDefault() {
        setBeforeTransformation(null);
    }

    public boolean getIsBeforeTransformationCompleted() {
        return this._IsBeforeTransformationCompleted;
    }

    /* access modifiers changed from: protected */
    public void setIsBeforeTransformationCompleted(boolean value) {
        this._IsBeforeTransformationCompleted = value;
    }

    /* access modifiers changed from: protected */
    public void setIsBeforeTransformationCompletedToDefault() {
        setIsBeforeTransformationCompleted(false);
    }
}
