package com.skyd.core.game.crosswisewar;

public interface IObj {
    IEntity getEncounterEntity();

    INation getNation();

    IScene getParentScene();

    float getPositionInScene();

    void setNation(INation iNation);

    void setParentScene(IScene iScene);

    void setPositionInScene(float f);

    void update();
}
