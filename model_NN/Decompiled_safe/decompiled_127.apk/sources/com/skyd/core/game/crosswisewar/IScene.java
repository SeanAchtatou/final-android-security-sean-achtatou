package com.skyd.core.game.crosswisewar;

import java.util.ArrayList;

public interface IScene {
    float getCameraOffset();

    ArrayList<IObj> getChildrenList();

    float getHorizonPoint();

    float getLeftEnterPoint();

    float getRightEnterPoint();

    int getTotalWidth();
}
