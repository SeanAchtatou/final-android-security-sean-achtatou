package com.skyd.bestpuzzle;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.widget.Toast;
import com.scoreloop.client.android.core.model.Score;
import com.scoreloop.client.android.ui.ScoreloopManagerSingleton;
import com.skyd.bestpuzzle.n1663.R;
import com.skyd.core.android.game.GameImageSpirit;
import com.skyd.core.android.game.GameMaster;
import com.skyd.core.android.game.GameObject;
import com.skyd.core.android.game.GameScene;
import com.skyd.core.android.game.GameSpirit;
import com.skyd.core.draw.DrawHelper;
import com.skyd.core.vector.Vector2DF;
import com.skyd.core.vector.VectorRect2DF;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

public class PuzzleScene extends GameScene {
    public Controller Controller;
    public Desktop Desktop;
    boolean IsDrawingPuzzle;
    boolean IsDrawnSelectRect;
    public GameImageSpirit MoveButton;
    public UI MoveButtonSlot;
    public OriginalImage OriginalImage;
    public long PastTime;
    public long StartTime;
    public UI SubmitScoreButton;
    /* access modifiers changed from: private */
    public RectF SubmitScoreButtonRect;
    public GameSpirit Time;
    public UI ViewFullButton;
    /* access modifiers changed from: private */
    public RectF ViewFullButtonRect;
    public UI ZoomInButton;
    public UI ZoomOutButton;
    private boolean _IsFinished = false;
    private ArrayList<OnIsFinishedChangedListener> _IsFinishedChangedListenerList = null;
    long savetime = Long.MIN_VALUE;
    RectF selectRect;

    public interface OnIsFinishedChangedListener {
        void OnIsFinishedChangedEvent(Object obj, boolean z);
    }

    public PuzzleScene() {
        setMaxDrawCacheLayer(25.0f);
        this.StartTime = new Date().getTime();
        loadGameState();
    }

    public void loadGameState() {
        Center c = (Center) GameMaster.getContext().getApplicationContext();
        this.PastTime = c.getPastTime().longValue();
        this._IsFinished = c.getIsFinished().booleanValue();
    }

    public void saveGameState() {
        Center c = (Center) GameMaster.getContext().getApplicationContext();
        if (getIsFinished()) {
            c.setPastTime(Long.valueOf(this.PastTime));
        } else {
            c.setPastTime(Long.valueOf(this.PastTime + (new Date().getTime() - this.StartTime)));
        }
        c.setIsFinished(Boolean.valueOf(getIsFinished()));
    }

    public void setFinished() {
        if (!getIsFinished()) {
            this.SubmitScoreButton.show();
        }
        setIsFinished(true);
        this.PastTime += new Date().getTime() - this.StartTime;
    }

    public boolean getIsFinished() {
        return this._IsFinished;
    }

    private void setIsFinished(boolean value) {
        onIsFinishedChanged(value);
        this._IsFinished = value;
    }

    private void setIsFinishedToDefault() {
        setIsFinished(false);
    }

    public boolean addOnIsFinishedChangedListener(OnIsFinishedChangedListener listener) {
        if (this._IsFinishedChangedListenerList == null) {
            this._IsFinishedChangedListenerList = new ArrayList<>();
        } else if (this._IsFinishedChangedListenerList.contains(listener)) {
            return false;
        }
        this._IsFinishedChangedListenerList.add(listener);
        return true;
    }

    public boolean removeOnIsFinishedChangedListener(OnIsFinishedChangedListener listener) {
        if (this._IsFinishedChangedListenerList == null || !this._IsFinishedChangedListenerList.contains(listener)) {
            return false;
        }
        this._IsFinishedChangedListenerList.remove(listener);
        return true;
    }

    public void clearOnIsFinishedChangedListeners() {
        if (this._IsFinishedChangedListenerList != null) {
            this._IsFinishedChangedListenerList.clear();
        }
    }

    /* access modifiers changed from: protected */
    public void onIsFinishedChanged(boolean newValue) {
        if (this._IsFinishedChangedListenerList != null) {
            Iterator<OnIsFinishedChangedListener> it = this._IsFinishedChangedListenerList.iterator();
            while (it.hasNext()) {
                it.next().OnIsFinishedChangedEvent(this, newValue);
            }
        }
    }

    public int getTargetCacheID() {
        return 1;
    }

    /* access modifiers changed from: protected */
    public void drawChilds(Canvas c, Rect drawArea) {
        this.IsDrawingPuzzle = false;
        this.IsDrawnSelectRect = false;
        super.drawChilds(c, drawArea);
    }

    /* access modifiers changed from: protected */
    public boolean onDrawingChild(GameObject child, Canvas c, Rect drawArea) {
        float lvl = ((GameSpirit) child).getLevel();
        if (!this.IsDrawingPuzzle && lvl <= 10.0f) {
            this.IsDrawingPuzzle = true;
            c.setMatrix(this.Desktop.getMatrix());
        } else if (lvl > 10.0f && this.IsDrawingPuzzle) {
            this.IsDrawingPuzzle = false;
            c.setMatrix(new Matrix());
        }
        if (lvl > 25.0f && this.selectRect != null && !this.IsDrawnSelectRect) {
            this.IsDrawnSelectRect = true;
            Paint p = new Paint();
            p.setARGB(100, 180, 225, 255);
            c.drawRect(this.selectRect, p);
        }
        return super.onDrawingChild(child, c, drawArea);
    }

    public void load(Context c) {
        loadOriginalImage(c);
        loadDesktop(c);
        loadInterface(c);
        loadPuzzle(c);
        loadPuzzleState();
        this.Desktop.updateCurrentObserveAreaRectF();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.skyd.core.draw.DrawHelper.calculateScaleSize(float, float, float, float, boolean):com.skyd.core.vector.Vector2DF
     arg types: [int, int, float, float, int]
     candidates:
      com.skyd.core.draw.DrawHelper.calculateScaleSize(double, double, double, double, boolean):com.skyd.core.vector.Vector2D
      com.skyd.core.draw.DrawHelper.calculateScaleSize(float, float, float, float, boolean):com.skyd.core.vector.Vector2DF */
    public void loadOriginalImage(Context c) {
        this.OriginalImage = new OriginalImage() {
            /* access modifiers changed from: protected */
            public boolean onDrawing(Canvas c, Rect drawArea) {
                c.drawARGB(180, 0, 0, 0);
                return super.onDrawing(c, drawArea);
            }
        };
        this.OriginalImage.setLevel(50.0f);
        this.OriginalImage.setName("OriginalImage");
        this.OriginalImage.getSize().resetWith(DrawHelper.calculateScaleSize(320.0f, 240.0f, (float) (GameMaster.getScreenWidth() - 40), (float) (GameMaster.getScreenHeight() - 40), true));
        this.OriginalImage.setTotalSeparateColumn(8);
        this.OriginalImage.setTotalSeparateRow(6);
        this.OriginalImage.getOriginalSize().reset(800.0f, 600.0f);
        this.OriginalImage.hide();
        this.OriginalImage.setIsUseAbsolutePosition(true);
        this.OriginalImage.setIsUseAbsoluteSize(true);
        this.OriginalImage.getImage().setIsUseAbsolutePosition(true);
        this.OriginalImage.getImage().setIsUseAbsoluteSize(true);
        this.OriginalImage.getPosition().reset(((float) (GameMaster.getScreenWidth() / 2)) - (this.OriginalImage.getSize().getX() / 2.0f), ((float) (GameMaster.getScreenHeight() / 2)) - (this.OriginalImage.getSize().getY() / 2.0f));
        getSpiritList().add(this.OriginalImage);
    }

    public void loadDesktop(Context c) {
        this.Desktop = new Desktop();
        this.Desktop.setLevel(-9999.0f);
        this.Desktop.setName("Desktop");
        this.Desktop.getSize().reset(1600.0f, 1200.0f);
        this.Desktop.getCurrentObservePosition().reset(800.0f, 600.0f);
        this.Desktop.setOriginalImage(this.OriginalImage);
        this.Desktop.setIsUseAbsolutePosition(true);
        this.Desktop.setIsUseAbsoluteSize(true);
        this.Desktop.ColorAPaint = new Paint();
        this.Desktop.ColorAPaint.setColor(Color.argb(255, 32, 30, 43));
        this.Desktop.ColorBPaint = new Paint();
        this.Desktop.ColorBPaint.setColor(Color.argb(255, 26, 24, 37));
        getSpiritList().add(this.Desktop);
    }

    public void loadPuzzle(Context c) {
        this.Controller = new Controller();
        this.Controller.setLevel(10.0f);
        this.Controller.setName("Controller");
        this.Controller.setDesktop(this.Desktop);
        this.Controller.setIsUseAbsolutePosition(true);
        this.Controller.setIsUseAbsoluteSize(true);
        getSpiritList().add(this.Controller);
        Puzzle.getRealitySize().reset(100.0f, 100.0f);
        Puzzle.getMaxRadius().reset(76.66666f, 76.66666f);
        c2093412775(c);
        c1591657332(c);
        c350496937(c);
        c537769616(c);
        c808482080(c);
        c486379817(c);
        c1846479498(c);
        c1735686180(c);
        c2054284449(c);
        c947922052(c);
        c1428805518(c);
        c893325049(c);
        c2059957329(c);
        c1656706169(c);
        c685727458(c);
        c1367960567(c);
        c1954697477(c);
        c384695074(c);
        c1923403094(c);
        c1740885567(c);
        c1221132387(c);
        c2104337530(c);
        c273912212(c);
        c2089983398(c);
        c1878338777(c);
        c1826916394(c);
        c330885544(c);
        c1305250614(c);
        c1279035711(c);
        c1735140811(c);
        c1234932860(c);
        c813043902(c);
        c382685693(c);
        c1781484329(c);
        c483678178(c);
        c663555648(c);
        c1397996497(c);
        c152087478(c);
        c1914205414(c);
        c134771039(c);
        c1772970260(c);
        c790487535(c);
        c824110566(c);
        c1635278835(c);
        c1736476003(c);
        c222475235(c);
        c298225912(c);
        c1702896116(c);
    }

    public void loadInterface(Context c) {
        this.MoveButton = new GameImageSpirit();
        this.MoveButton.setLevel(22.0f);
        this.MoveButton.getImage().loadImageFromResource(c, R.drawable.movebutton);
        this.MoveButton.getSize().reset(155.0f, 155.0f);
        this.MoveButton.setIsUseAbsolutePosition(true);
        getSpiritList().add(this.MoveButton);
        final Vector2DF mvds = this.MoveButton.getDisplaySize();
        mvds.scale(0.5f);
        this.MoveButton.getPosition().reset(5.0f + mvds.getX(), ((float) (GameMaster.getScreenHeight() - 5)) - mvds.getY());
        this.MoveButton.getPositionOffset().resetWith(mvds.negateNew());
        this.MoveButtonSlot = new UI() {
            Vector2DF movevector;
            boolean needmove;

            public void executive(Vector2DF point) {
                this.movevector = point.minusNew(getPosition()).restrainLength(mvds.getX() * 0.4f);
                this.needmove = true;
            }

            /* access modifiers changed from: protected */
            public void updateSelf() {
                if (this.needmove) {
                    PuzzleScene.this.MoveButton.getPosition().resetWith(getPosition().plusNew(this.movevector));
                    PuzzleScene.this.Desktop.getCurrentObservePosition().plus(this.movevector);
                    PuzzleScene.this.refreshDrawCacheBitmap();
                }
                super.updateSelf();
            }

            public boolean isInArea(Vector2DF point) {
                return point.minusNew(getPosition()).getLength() < mvds.getX() + 5.0f;
            }

            public void reset() {
                this.needmove = false;
                PuzzleScene.this.MoveButton.getPosition().resetWith(getPosition());
                PuzzleScene.this.refreshDrawCacheBitmap();
            }
        };
        this.MoveButtonSlot.setLevel(21.0f);
        this.MoveButtonSlot.getImage().loadImageFromResource(c, R.drawable.movebuttonslot);
        this.MoveButtonSlot.getSize().reset(155.0f, 155.0f);
        this.MoveButtonSlot.setIsUseAbsolutePosition(true);
        getSpiritList().add(this.MoveButtonSlot);
        this.MoveButtonSlot.getPosition().resetWith(this.MoveButton.getPosition());
        this.MoveButtonSlot.getPositionOffset().resetWith(this.MoveButton.getPositionOffset());
        this.ZoomInButton = new UI() {
            boolean zoom;

            public void executive(Vector2DF point) {
                this.zoom = true;
            }

            public boolean isInArea(Vector2DF point) {
                return point.minusNew(getPosition()).getLength() < getDisplaySize().getX() / 2.0f;
            }

            public void reset() {
                this.zoom = false;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{java.lang.Math.min(float, float):float}
             arg types: [int, float]
             candidates:
              ClspMth{java.lang.Math.min(double, double):double}
              ClspMth{java.lang.Math.min(long, long):long}
              ClspMth{java.lang.Math.min(int, int):int}
              ClspMth{java.lang.Math.min(float, float):float} */
            /* access modifiers changed from: protected */
            public void updateSelf() {
                if (this.zoom) {
                    PuzzleScene.this.Desktop.setZoom(Math.min(1.3f, PuzzleScene.this.Desktop.getZoom() * 1.05f));
                    PuzzleScene.this.refreshDrawCacheBitmap();
                }
                super.updateSelf();
            }
        };
        this.ZoomInButton.setLevel(21.0f);
        this.ZoomInButton.getImage().loadImageFromResource(c, R.drawable.zoombutton1);
        this.ZoomInButton.getSize().reset(77.0f, 77.0f);
        this.ZoomInButton.setIsUseAbsolutePosition(true);
        getSpiritList().add(this.ZoomInButton);
        Vector2DF zibds = this.ZoomInButton.getDisplaySize().scale(0.5f);
        this.ZoomInButton.getPosition().reset(((float) (GameMaster.getScreenWidth() - 50)) - zibds.getX(), ((float) (GameMaster.getScreenHeight() - 10)) - zibds.getY());
        this.ZoomInButton.getPositionOffset().resetWith(zibds.negateNew());
        this.ZoomOutButton = new UI() {
            boolean zoom;

            public void executive(Vector2DF point) {
                this.zoom = true;
            }

            public boolean isInArea(Vector2DF point) {
                return point.minusNew(getPosition()).getLength() < getDisplaySize().getX() / 2.0f;
            }

            public void reset() {
                this.zoom = false;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{java.lang.Math.max(float, float):float}
             arg types: [int, float]
             candidates:
              ClspMth{java.lang.Math.max(double, double):double}
              ClspMth{java.lang.Math.max(int, int):int}
              ClspMth{java.lang.Math.max(long, long):long}
              ClspMth{java.lang.Math.max(float, float):float} */
            /* access modifiers changed from: protected */
            public void updateSelf() {
                if (this.zoom) {
                    PuzzleScene.this.Desktop.setZoom(Math.max(0.3f, PuzzleScene.this.Desktop.getZoom() * 0.95f));
                    PuzzleScene.this.refreshDrawCacheBitmap();
                    GameMaster.log(this, Float.valueOf(PuzzleScene.this.Desktop.getCurrentObserveAreaRectF().width()));
                }
                super.updateSelf();
            }
        };
        this.ZoomOutButton.setLevel(21.0f);
        this.ZoomOutButton.getImage().loadImageFromResource(c, R.drawable.zoombutton2);
        this.ZoomOutButton.getSize().reset(77.0f, 77.0f);
        this.ZoomOutButton.setIsUseAbsolutePosition(true);
        getSpiritList().add(this.ZoomOutButton);
        Vector2DF zobds = this.ZoomOutButton.getDisplaySize().scale(0.5f);
        this.ZoomOutButton.getPosition().reset((((float) (GameMaster.getScreenWidth() - 100)) - zobds.getX()) - (zibds.getX() * 2.0f), ((float) (GameMaster.getScreenHeight() - 10)) - zobds.getY());
        this.ZoomOutButton.getPositionOffset().resetWith(zobds.negateNew());
        this.ViewFullButton = new UI() {
            public void executive(Vector2DF point) {
                if (PuzzleScene.this.OriginalImage.getImage().getImage() == null) {
                    PuzzleScene.this.OriginalImage.getImage().loadImageFromResource(GameMaster.getContext(), R.drawable.oim);
                }
                PuzzleScene.this.OriginalImage.show();
            }

            public boolean isInArea(Vector2DF point) {
                return point.isIn(PuzzleScene.this.ViewFullButtonRect);
            }

            public void reset() {
                PuzzleScene.this.OriginalImage.hide();
            }
        };
        this.ViewFullButton.getImage().loadImageFromResource(c, R.drawable.viewfull);
        this.ViewFullButton.setLevel(21.0f);
        this.ViewFullButton.setIsUseAbsolutePosition(true);
        this.ViewFullButton.getSize().reset(83.0f, 38.0f);
        getSpiritList().add(this.ViewFullButton);
        this.ViewFullButton.getPosition().reset(((float) (GameMaster.getScreenWidth() - 15)) - this.ViewFullButton.getDisplaySize().getX(), 15.0f);
        this.ViewFullButtonRect = new VectorRect2DF(this.ViewFullButton.getPosition(), this.ViewFullButton.getDisplaySize()).getRectF();
        this.SubmitScoreButton = new UI() {
            public void executive(Vector2DF point) {
                Score score = new Score(Double.valueOf(Math.ceil((double) (PuzzleScene.this.PastTime / 1000))), null);
                score.setMode(21);
                ScoreloopManagerSingleton.get().onGamePlayEnded(score);
                hide();
                Toast.makeText(GameMaster.getContext(), (int) R.string.Submitting, 1).show();
            }

            public boolean isInArea(Vector2DF point) {
                return getVisibleOriginalValue() && point.isIn(PuzzleScene.this.SubmitScoreButtonRect);
            }

            public void reset() {
            }
        };
        this.SubmitScoreButton.getImage().loadImageFromResource(c, R.drawable.submitscore);
        this.SubmitScoreButton.setLevel(21.0f);
        this.SubmitScoreButton.setIsUseAbsolutePosition(true);
        this.SubmitScoreButton.getSize().reset(114.0f, 40.0f);
        getSpiritList().add(this.SubmitScoreButton);
        this.SubmitScoreButton.getPosition().reset(15.0f, 60.0f);
        this.SubmitScoreButtonRect = new VectorRect2DF(this.SubmitScoreButton.getPosition(), this.SubmitScoreButton.getDisplaySize()).getRectF();
        this.SubmitScoreButton.hide();
        this.Time = new GameSpirit() {
            DecimalFormat FMT = new DecimalFormat("00");
            Paint p1;
            Paint p2;

            public GameObject getDisplayContentChild() {
                return null;
            }

            /* access modifiers changed from: protected */
            public void drawSelf(Canvas c, Rect drawArea) {
                if (this.p1 == null) {
                    this.p1 = new Paint();
                    this.p1.setARGB(160, 255, 255, 255);
                    this.p1.setAntiAlias(true);
                    this.p1.setTextSize(28.0f);
                    this.p1.setTypeface(Typeface.createFromAsset(GameMaster.getContext().getAssets(), "fonts/font.ttf"));
                }
                if (this.p2 == null) {
                    this.p2 = new Paint();
                    this.p2.setARGB(100, 0, 0, 0);
                    this.p2.setAntiAlias(true);
                    this.p2.setTextSize(28.0f);
                    this.p2.setTypeface(Typeface.createFromAsset(GameMaster.getContext().getAssets(), "fonts/font.ttf"));
                }
                long t = PuzzleScene.this.getIsFinished() ? PuzzleScene.this.PastTime : PuzzleScene.this.PastTime + (new Date().getTime() - PuzzleScene.this.StartTime);
                long h = t / 3600000;
                long m = (t - (((1000 * h) * 60) * 60)) / 60000;
                String str = String.valueOf(this.FMT.format(h)) + ":" + this.FMT.format(m) + ":" + this.FMT.format(((t - (((1000 * h) * 60) * 60)) - ((1000 * m) * 60)) / 1000) + (PuzzleScene.this.getIsFinished() ? " Finished" : "");
                c.drawText(str, 16.0f, 44.0f, this.p2);
                c.drawText(str, 15.0f, 43.0f, this.p1);
                super.drawSelf(c, drawArea);
            }
        };
        this.Time.setLevel(40.0f);
        getSpiritList().add(this.Time);
    }

    public float getMaxPuzzleLevel() {
        float maxlvl = 0.0f;
        Iterator<Puzzle> it = this.Desktop.getPuzzleList().iterator();
        while (it.hasNext()) {
            Puzzle f = it.next();
            if (f.getLevel() <= 10.0f) {
                maxlvl = Math.max(f.getLevel(), maxlvl);
            }
        }
        return maxlvl;
    }

    public Puzzle getTouchPuzzle(Vector2DF v) {
        Vector2DF rv = this.Desktop.reMapPoint(v);
        Puzzle o = null;
        float olen = 99999.0f;
        for (int i = getSpiritList().size() - 1; i >= 0; i--) {
            if (getSpiritList().get(i) instanceof Puzzle) {
                Puzzle p = (Puzzle) getSpiritList().get(i);
                float len = p.getPositionInDesktop().minusNew(rv).getLength();
                if (len <= Puzzle.getRealitySize().getX() * 0.4f) {
                    return p;
                }
                if (len <= Puzzle.getRealitySize().getX() && olen > len) {
                    o = p;
                    olen = len;
                }
            }
        }
        return o;
    }

    public void startDrawSelectRect(Vector2DF startDragPoint, Vector2DF v) {
        this.selectRect = new VectorRect2DF(startDragPoint, v.minusNew(startDragPoint)).getFixedRectF();
    }

    public void stopDrawSelectRect() {
        this.selectRect = null;
    }

    public ArrayList<Puzzle> getSelectPuzzle(Vector2DF startDragPoint, Vector2DF v) {
        Vector2DF rs = this.Desktop.reMapPoint(startDragPoint);
        ArrayList<Puzzle> l = new ArrayList<>();
        RectF rect = new VectorRect2DF(rs, this.Desktop.reMapPoint(v).minusNew(rs)).getFixedRectF();
        float xo = Puzzle.getRealitySize().getX() / 3.0f;
        float yo = Puzzle.getRealitySize().getY() / 3.0f;
        RectF sr = new RectF(rect.left - xo, rect.top - yo, rect.right + xo, rect.bottom + yo);
        Iterator<Puzzle> it = this.Desktop.getPuzzleList().iterator();
        while (it.hasNext()) {
            Puzzle f = it.next();
            if (f.getPositionInDesktop().isIn(sr)) {
                l.add(f);
            }
        }
        return l;
    }

    public void loadPuzzleState() {
        Center c = (Center) GameMaster.getContext().getApplicationContext();
        SharedPreferences s = c.getSharedPreferences();
        Iterator<Puzzle> it = this.Desktop.getPuzzleList().iterator();
        while (it.hasNext()) {
            it.next().load(c, s);
        }
    }

    public void savePuzzleState() {
        Center c = (Center) GameMaster.getContext().getApplicationContext();
        SharedPreferences.Editor e = c.getSharedPreferences().edit();
        Iterator<Puzzle> it = this.Desktop.getPuzzleList().iterator();
        while (it.hasNext()) {
            it.next().save(c, e);
        }
        e.commit();
    }

    public void reset() {
        this.Controller.reset();
        Iterator<Puzzle> it = this.Desktop.getPuzzleList().iterator();
        while (it.hasNext()) {
            Puzzle f = it.next();
            f.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
            this.Desktop.RandomlyPlaced(f);
        }
        refreshDrawCacheBitmap();
        setIsFinishedToDefault();
        this.StartTime = new Date().getTime();
        this.PastTime = 0;
        this.SubmitScoreButton.hide();
        saveGameState();
        savePuzzleState();
    }

    /* access modifiers changed from: package-private */
    public void c2093412775(Context c) {
        Puzzle p2093412775 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load2093412775(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save2093412775(editor, this);
            }
        };
        p2093412775.setID(2093412775);
        p2093412775.setName("2093412775");
        p2093412775.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p2093412775);
        this.Desktop.RandomlyPlaced(p2093412775);
        p2093412775.setTopEdgeType(EdgeType.Flat);
        p2093412775.setBottomEdgeType(EdgeType.Concave);
        p2093412775.setLeftEdgeType(EdgeType.Flat);
        p2093412775.setRightEdgeType(EdgeType.Concave);
        p2093412775.setTopExactPuzzleID(-1);
        p2093412775.setBottomExactPuzzleID(1591657332);
        p2093412775.setLeftExactPuzzleID(-1);
        p2093412775.setRightExactPuzzleID(1846479498);
        p2093412775.getDisplayImage().loadImageFromResource(c, R.drawable.p2093412775h);
        p2093412775.setExactRow(0);
        p2093412775.setExactColumn(0);
        p2093412775.getSize().reset(100.0f, 100.0f);
        p2093412775.getPositionOffset().reset(-50.0f, -50.0f);
        p2093412775.setIsUseAbsolutePosition(true);
        p2093412775.setIsUseAbsoluteSize(true);
        p2093412775.getImage().setIsUseAbsolutePosition(true);
        p2093412775.getImage().setIsUseAbsoluteSize(true);
        p2093412775.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p2093412775.resetPosition();
        getSpiritList().add(p2093412775);
    }

    /* access modifiers changed from: package-private */
    public void c1591657332(Context c) {
        Puzzle p1591657332 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1591657332(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1591657332(editor, this);
            }
        };
        p1591657332.setID(1591657332);
        p1591657332.setName("1591657332");
        p1591657332.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1591657332);
        this.Desktop.RandomlyPlaced(p1591657332);
        p1591657332.setTopEdgeType(EdgeType.Convex);
        p1591657332.setBottomEdgeType(EdgeType.Concave);
        p1591657332.setLeftEdgeType(EdgeType.Flat);
        p1591657332.setRightEdgeType(EdgeType.Concave);
        p1591657332.setTopExactPuzzleID(2093412775);
        p1591657332.setBottomExactPuzzleID(350496937);
        p1591657332.setLeftExactPuzzleID(-1);
        p1591657332.setRightExactPuzzleID(1735686180);
        p1591657332.getDisplayImage().loadImageFromResource(c, R.drawable.p1591657332h);
        p1591657332.setExactRow(1);
        p1591657332.setExactColumn(0);
        p1591657332.getSize().reset(100.0f, 126.6667f);
        p1591657332.getPositionOffset().reset(-50.0f, -76.66666f);
        p1591657332.setIsUseAbsolutePosition(true);
        p1591657332.setIsUseAbsoluteSize(true);
        p1591657332.getImage().setIsUseAbsolutePosition(true);
        p1591657332.getImage().setIsUseAbsoluteSize(true);
        p1591657332.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1591657332.resetPosition();
        getSpiritList().add(p1591657332);
    }

    /* access modifiers changed from: package-private */
    public void c350496937(Context c) {
        Puzzle p350496937 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load350496937(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save350496937(editor, this);
            }
        };
        p350496937.setID(350496937);
        p350496937.setName("350496937");
        p350496937.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p350496937);
        this.Desktop.RandomlyPlaced(p350496937);
        p350496937.setTopEdgeType(EdgeType.Convex);
        p350496937.setBottomEdgeType(EdgeType.Convex);
        p350496937.setLeftEdgeType(EdgeType.Flat);
        p350496937.setRightEdgeType(EdgeType.Concave);
        p350496937.setTopExactPuzzleID(1591657332);
        p350496937.setBottomExactPuzzleID(537769616);
        p350496937.setLeftExactPuzzleID(-1);
        p350496937.setRightExactPuzzleID(2054284449);
        p350496937.getDisplayImage().loadImageFromResource(c, R.drawable.p350496937h);
        p350496937.setExactRow(2);
        p350496937.setExactColumn(0);
        p350496937.getSize().reset(100.0f, 153.3333f);
        p350496937.getPositionOffset().reset(-50.0f, -76.66666f);
        p350496937.setIsUseAbsolutePosition(true);
        p350496937.setIsUseAbsoluteSize(true);
        p350496937.getImage().setIsUseAbsolutePosition(true);
        p350496937.getImage().setIsUseAbsoluteSize(true);
        p350496937.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p350496937.resetPosition();
        getSpiritList().add(p350496937);
    }

    /* access modifiers changed from: package-private */
    public void c537769616(Context c) {
        Puzzle p537769616 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load537769616(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save537769616(editor, this);
            }
        };
        p537769616.setID(537769616);
        p537769616.setName("537769616");
        p537769616.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p537769616);
        this.Desktop.RandomlyPlaced(p537769616);
        p537769616.setTopEdgeType(EdgeType.Concave);
        p537769616.setBottomEdgeType(EdgeType.Concave);
        p537769616.setLeftEdgeType(EdgeType.Flat);
        p537769616.setRightEdgeType(EdgeType.Convex);
        p537769616.setTopExactPuzzleID(350496937);
        p537769616.setBottomExactPuzzleID(808482080);
        p537769616.setLeftExactPuzzleID(-1);
        p537769616.setRightExactPuzzleID(947922052);
        p537769616.getDisplayImage().loadImageFromResource(c, R.drawable.p537769616h);
        p537769616.setExactRow(3);
        p537769616.setExactColumn(0);
        p537769616.getSize().reset(126.6667f, 100.0f);
        p537769616.getPositionOffset().reset(-50.0f, -50.0f);
        p537769616.setIsUseAbsolutePosition(true);
        p537769616.setIsUseAbsoluteSize(true);
        p537769616.getImage().setIsUseAbsolutePosition(true);
        p537769616.getImage().setIsUseAbsoluteSize(true);
        p537769616.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p537769616.resetPosition();
        getSpiritList().add(p537769616);
    }

    /* access modifiers changed from: package-private */
    public void c808482080(Context c) {
        Puzzle p808482080 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load808482080(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save808482080(editor, this);
            }
        };
        p808482080.setID(808482080);
        p808482080.setName("808482080");
        p808482080.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p808482080);
        this.Desktop.RandomlyPlaced(p808482080);
        p808482080.setTopEdgeType(EdgeType.Convex);
        p808482080.setBottomEdgeType(EdgeType.Convex);
        p808482080.setLeftEdgeType(EdgeType.Flat);
        p808482080.setRightEdgeType(EdgeType.Convex);
        p808482080.setTopExactPuzzleID(537769616);
        p808482080.setBottomExactPuzzleID(486379817);
        p808482080.setLeftExactPuzzleID(-1);
        p808482080.setRightExactPuzzleID(1428805518);
        p808482080.getDisplayImage().loadImageFromResource(c, R.drawable.p808482080h);
        p808482080.setExactRow(4);
        p808482080.setExactColumn(0);
        p808482080.getSize().reset(126.6667f, 153.3333f);
        p808482080.getPositionOffset().reset(-50.0f, -76.66666f);
        p808482080.setIsUseAbsolutePosition(true);
        p808482080.setIsUseAbsoluteSize(true);
        p808482080.getImage().setIsUseAbsolutePosition(true);
        p808482080.getImage().setIsUseAbsoluteSize(true);
        p808482080.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p808482080.resetPosition();
        getSpiritList().add(p808482080);
    }

    /* access modifiers changed from: package-private */
    public void c486379817(Context c) {
        Puzzle p486379817 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load486379817(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save486379817(editor, this);
            }
        };
        p486379817.setID(486379817);
        p486379817.setName("486379817");
        p486379817.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p486379817);
        this.Desktop.RandomlyPlaced(p486379817);
        p486379817.setTopEdgeType(EdgeType.Concave);
        p486379817.setBottomEdgeType(EdgeType.Flat);
        p486379817.setLeftEdgeType(EdgeType.Flat);
        p486379817.setRightEdgeType(EdgeType.Concave);
        p486379817.setTopExactPuzzleID(808482080);
        p486379817.setBottomExactPuzzleID(-1);
        p486379817.setLeftExactPuzzleID(-1);
        p486379817.setRightExactPuzzleID(893325049);
        p486379817.getDisplayImage().loadImageFromResource(c, R.drawable.p486379817h);
        p486379817.setExactRow(5);
        p486379817.setExactColumn(0);
        p486379817.getSize().reset(100.0f, 100.0f);
        p486379817.getPositionOffset().reset(-50.0f, -50.0f);
        p486379817.setIsUseAbsolutePosition(true);
        p486379817.setIsUseAbsoluteSize(true);
        p486379817.getImage().setIsUseAbsolutePosition(true);
        p486379817.getImage().setIsUseAbsoluteSize(true);
        p486379817.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p486379817.resetPosition();
        getSpiritList().add(p486379817);
    }

    /* access modifiers changed from: package-private */
    public void c1846479498(Context c) {
        Puzzle p1846479498 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1846479498(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1846479498(editor, this);
            }
        };
        p1846479498.setID(1846479498);
        p1846479498.setName("1846479498");
        p1846479498.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1846479498);
        this.Desktop.RandomlyPlaced(p1846479498);
        p1846479498.setTopEdgeType(EdgeType.Flat);
        p1846479498.setBottomEdgeType(EdgeType.Concave);
        p1846479498.setLeftEdgeType(EdgeType.Convex);
        p1846479498.setRightEdgeType(EdgeType.Convex);
        p1846479498.setTopExactPuzzleID(-1);
        p1846479498.setBottomExactPuzzleID(1735686180);
        p1846479498.setLeftExactPuzzleID(2093412775);
        p1846479498.setRightExactPuzzleID(2059957329);
        p1846479498.getDisplayImage().loadImageFromResource(c, R.drawable.p1846479498h);
        p1846479498.setExactRow(0);
        p1846479498.setExactColumn(1);
        p1846479498.getSize().reset(153.3333f, 100.0f);
        p1846479498.getPositionOffset().reset(-76.66666f, -50.0f);
        p1846479498.setIsUseAbsolutePosition(true);
        p1846479498.setIsUseAbsoluteSize(true);
        p1846479498.getImage().setIsUseAbsolutePosition(true);
        p1846479498.getImage().setIsUseAbsoluteSize(true);
        p1846479498.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1846479498.resetPosition();
        getSpiritList().add(p1846479498);
    }

    /* access modifiers changed from: package-private */
    public void c1735686180(Context c) {
        Puzzle p1735686180 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1735686180(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1735686180(editor, this);
            }
        };
        p1735686180.setID(1735686180);
        p1735686180.setName("1735686180");
        p1735686180.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1735686180);
        this.Desktop.RandomlyPlaced(p1735686180);
        p1735686180.setTopEdgeType(EdgeType.Convex);
        p1735686180.setBottomEdgeType(EdgeType.Convex);
        p1735686180.setLeftEdgeType(EdgeType.Convex);
        p1735686180.setRightEdgeType(EdgeType.Convex);
        p1735686180.setTopExactPuzzleID(1846479498);
        p1735686180.setBottomExactPuzzleID(2054284449);
        p1735686180.setLeftExactPuzzleID(1591657332);
        p1735686180.setRightExactPuzzleID(1656706169);
        p1735686180.getDisplayImage().loadImageFromResource(c, R.drawable.p1735686180h);
        p1735686180.setExactRow(1);
        p1735686180.setExactColumn(1);
        p1735686180.getSize().reset(153.3333f, 153.3333f);
        p1735686180.getPositionOffset().reset(-76.66666f, -76.66666f);
        p1735686180.setIsUseAbsolutePosition(true);
        p1735686180.setIsUseAbsoluteSize(true);
        p1735686180.getImage().setIsUseAbsolutePosition(true);
        p1735686180.getImage().setIsUseAbsoluteSize(true);
        p1735686180.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1735686180.resetPosition();
        getSpiritList().add(p1735686180);
    }

    /* access modifiers changed from: package-private */
    public void c2054284449(Context c) {
        Puzzle p2054284449 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load2054284449(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save2054284449(editor, this);
            }
        };
        p2054284449.setID(2054284449);
        p2054284449.setName("2054284449");
        p2054284449.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p2054284449);
        this.Desktop.RandomlyPlaced(p2054284449);
        p2054284449.setTopEdgeType(EdgeType.Concave);
        p2054284449.setBottomEdgeType(EdgeType.Convex);
        p2054284449.setLeftEdgeType(EdgeType.Convex);
        p2054284449.setRightEdgeType(EdgeType.Convex);
        p2054284449.setTopExactPuzzleID(1735686180);
        p2054284449.setBottomExactPuzzleID(947922052);
        p2054284449.setLeftExactPuzzleID(350496937);
        p2054284449.setRightExactPuzzleID(685727458);
        p2054284449.getDisplayImage().loadImageFromResource(c, R.drawable.p2054284449h);
        p2054284449.setExactRow(2);
        p2054284449.setExactColumn(1);
        p2054284449.getSize().reset(153.3333f, 126.6667f);
        p2054284449.getPositionOffset().reset(-76.66666f, -50.0f);
        p2054284449.setIsUseAbsolutePosition(true);
        p2054284449.setIsUseAbsoluteSize(true);
        p2054284449.getImage().setIsUseAbsolutePosition(true);
        p2054284449.getImage().setIsUseAbsoluteSize(true);
        p2054284449.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p2054284449.resetPosition();
        getSpiritList().add(p2054284449);
    }

    /* access modifiers changed from: package-private */
    public void c947922052(Context c) {
        Puzzle p947922052 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load947922052(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save947922052(editor, this);
            }
        };
        p947922052.setID(947922052);
        p947922052.setName("947922052");
        p947922052.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p947922052);
        this.Desktop.RandomlyPlaced(p947922052);
        p947922052.setTopEdgeType(EdgeType.Concave);
        p947922052.setBottomEdgeType(EdgeType.Concave);
        p947922052.setLeftEdgeType(EdgeType.Concave);
        p947922052.setRightEdgeType(EdgeType.Concave);
        p947922052.setTopExactPuzzleID(2054284449);
        p947922052.setBottomExactPuzzleID(1428805518);
        p947922052.setLeftExactPuzzleID(537769616);
        p947922052.setRightExactPuzzleID(1367960567);
        p947922052.getDisplayImage().loadImageFromResource(c, R.drawable.p947922052h);
        p947922052.setExactRow(3);
        p947922052.setExactColumn(1);
        p947922052.getSize().reset(100.0f, 100.0f);
        p947922052.getPositionOffset().reset(-50.0f, -50.0f);
        p947922052.setIsUseAbsolutePosition(true);
        p947922052.setIsUseAbsoluteSize(true);
        p947922052.getImage().setIsUseAbsolutePosition(true);
        p947922052.getImage().setIsUseAbsoluteSize(true);
        p947922052.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p947922052.resetPosition();
        getSpiritList().add(p947922052);
    }

    /* access modifiers changed from: package-private */
    public void c1428805518(Context c) {
        Puzzle p1428805518 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1428805518(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1428805518(editor, this);
            }
        };
        p1428805518.setID(1428805518);
        p1428805518.setName("1428805518");
        p1428805518.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1428805518);
        this.Desktop.RandomlyPlaced(p1428805518);
        p1428805518.setTopEdgeType(EdgeType.Convex);
        p1428805518.setBottomEdgeType(EdgeType.Concave);
        p1428805518.setLeftEdgeType(EdgeType.Concave);
        p1428805518.setRightEdgeType(EdgeType.Concave);
        p1428805518.setTopExactPuzzleID(947922052);
        p1428805518.setBottomExactPuzzleID(893325049);
        p1428805518.setLeftExactPuzzleID(808482080);
        p1428805518.setRightExactPuzzleID(1954697477);
        p1428805518.getDisplayImage().loadImageFromResource(c, R.drawable.p1428805518h);
        p1428805518.setExactRow(4);
        p1428805518.setExactColumn(1);
        p1428805518.getSize().reset(100.0f, 126.6667f);
        p1428805518.getPositionOffset().reset(-50.0f, -76.66666f);
        p1428805518.setIsUseAbsolutePosition(true);
        p1428805518.setIsUseAbsoluteSize(true);
        p1428805518.getImage().setIsUseAbsolutePosition(true);
        p1428805518.getImage().setIsUseAbsoluteSize(true);
        p1428805518.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1428805518.resetPosition();
        getSpiritList().add(p1428805518);
    }

    /* access modifiers changed from: package-private */
    public void c893325049(Context c) {
        Puzzle p893325049 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load893325049(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save893325049(editor, this);
            }
        };
        p893325049.setID(893325049);
        p893325049.setName("893325049");
        p893325049.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p893325049);
        this.Desktop.RandomlyPlaced(p893325049);
        p893325049.setTopEdgeType(EdgeType.Convex);
        p893325049.setBottomEdgeType(EdgeType.Flat);
        p893325049.setLeftEdgeType(EdgeType.Convex);
        p893325049.setRightEdgeType(EdgeType.Convex);
        p893325049.setTopExactPuzzleID(1428805518);
        p893325049.setBottomExactPuzzleID(-1);
        p893325049.setLeftExactPuzzleID(486379817);
        p893325049.setRightExactPuzzleID(384695074);
        p893325049.getDisplayImage().loadImageFromResource(c, R.drawable.p893325049h);
        p893325049.setExactRow(5);
        p893325049.setExactColumn(1);
        p893325049.getSize().reset(153.3333f, 126.6667f);
        p893325049.getPositionOffset().reset(-76.66666f, -76.66666f);
        p893325049.setIsUseAbsolutePosition(true);
        p893325049.setIsUseAbsoluteSize(true);
        p893325049.getImage().setIsUseAbsolutePosition(true);
        p893325049.getImage().setIsUseAbsoluteSize(true);
        p893325049.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p893325049.resetPosition();
        getSpiritList().add(p893325049);
    }

    /* access modifiers changed from: package-private */
    public void c2059957329(Context c) {
        Puzzle p2059957329 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load2059957329(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save2059957329(editor, this);
            }
        };
        p2059957329.setID(2059957329);
        p2059957329.setName("2059957329");
        p2059957329.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p2059957329);
        this.Desktop.RandomlyPlaced(p2059957329);
        p2059957329.setTopEdgeType(EdgeType.Flat);
        p2059957329.setBottomEdgeType(EdgeType.Convex);
        p2059957329.setLeftEdgeType(EdgeType.Concave);
        p2059957329.setRightEdgeType(EdgeType.Convex);
        p2059957329.setTopExactPuzzleID(-1);
        p2059957329.setBottomExactPuzzleID(1656706169);
        p2059957329.setLeftExactPuzzleID(1846479498);
        p2059957329.setRightExactPuzzleID(1923403094);
        p2059957329.getDisplayImage().loadImageFromResource(c, R.drawable.p2059957329h);
        p2059957329.setExactRow(0);
        p2059957329.setExactColumn(2);
        p2059957329.getSize().reset(126.6667f, 126.6667f);
        p2059957329.getPositionOffset().reset(-50.0f, -50.0f);
        p2059957329.setIsUseAbsolutePosition(true);
        p2059957329.setIsUseAbsoluteSize(true);
        p2059957329.getImage().setIsUseAbsolutePosition(true);
        p2059957329.getImage().setIsUseAbsoluteSize(true);
        p2059957329.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p2059957329.resetPosition();
        getSpiritList().add(p2059957329);
    }

    /* access modifiers changed from: package-private */
    public void c1656706169(Context c) {
        Puzzle p1656706169 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1656706169(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1656706169(editor, this);
            }
        };
        p1656706169.setID(1656706169);
        p1656706169.setName("1656706169");
        p1656706169.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1656706169);
        this.Desktop.RandomlyPlaced(p1656706169);
        p1656706169.setTopEdgeType(EdgeType.Concave);
        p1656706169.setBottomEdgeType(EdgeType.Concave);
        p1656706169.setLeftEdgeType(EdgeType.Concave);
        p1656706169.setRightEdgeType(EdgeType.Concave);
        p1656706169.setTopExactPuzzleID(2059957329);
        p1656706169.setBottomExactPuzzleID(685727458);
        p1656706169.setLeftExactPuzzleID(1735686180);
        p1656706169.setRightExactPuzzleID(1740885567);
        p1656706169.getDisplayImage().loadImageFromResource(c, R.drawable.p1656706169h);
        p1656706169.setExactRow(1);
        p1656706169.setExactColumn(2);
        p1656706169.getSize().reset(100.0f, 100.0f);
        p1656706169.getPositionOffset().reset(-50.0f, -50.0f);
        p1656706169.setIsUseAbsolutePosition(true);
        p1656706169.setIsUseAbsoluteSize(true);
        p1656706169.getImage().setIsUseAbsolutePosition(true);
        p1656706169.getImage().setIsUseAbsoluteSize(true);
        p1656706169.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1656706169.resetPosition();
        getSpiritList().add(p1656706169);
    }

    /* access modifiers changed from: package-private */
    public void c685727458(Context c) {
        Puzzle p685727458 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load685727458(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save685727458(editor, this);
            }
        };
        p685727458.setID(685727458);
        p685727458.setName("685727458");
        p685727458.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p685727458);
        this.Desktop.RandomlyPlaced(p685727458);
        p685727458.setTopEdgeType(EdgeType.Convex);
        p685727458.setBottomEdgeType(EdgeType.Concave);
        p685727458.setLeftEdgeType(EdgeType.Concave);
        p685727458.setRightEdgeType(EdgeType.Convex);
        p685727458.setTopExactPuzzleID(1656706169);
        p685727458.setBottomExactPuzzleID(1367960567);
        p685727458.setLeftExactPuzzleID(2054284449);
        p685727458.setRightExactPuzzleID(1221132387);
        p685727458.getDisplayImage().loadImageFromResource(c, R.drawable.p685727458h);
        p685727458.setExactRow(2);
        p685727458.setExactColumn(2);
        p685727458.getSize().reset(126.6667f, 126.6667f);
        p685727458.getPositionOffset().reset(-50.0f, -76.66666f);
        p685727458.setIsUseAbsolutePosition(true);
        p685727458.setIsUseAbsoluteSize(true);
        p685727458.getImage().setIsUseAbsolutePosition(true);
        p685727458.getImage().setIsUseAbsoluteSize(true);
        p685727458.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p685727458.resetPosition();
        getSpiritList().add(p685727458);
    }

    /* access modifiers changed from: package-private */
    public void c1367960567(Context c) {
        Puzzle p1367960567 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1367960567(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1367960567(editor, this);
            }
        };
        p1367960567.setID(1367960567);
        p1367960567.setName("1367960567");
        p1367960567.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1367960567);
        this.Desktop.RandomlyPlaced(p1367960567);
        p1367960567.setTopEdgeType(EdgeType.Convex);
        p1367960567.setBottomEdgeType(EdgeType.Convex);
        p1367960567.setLeftEdgeType(EdgeType.Convex);
        p1367960567.setRightEdgeType(EdgeType.Convex);
        p1367960567.setTopExactPuzzleID(685727458);
        p1367960567.setBottomExactPuzzleID(1954697477);
        p1367960567.setLeftExactPuzzleID(947922052);
        p1367960567.setRightExactPuzzleID(2104337530);
        p1367960567.getDisplayImage().loadImageFromResource(c, R.drawable.p1367960567h);
        p1367960567.setExactRow(3);
        p1367960567.setExactColumn(2);
        p1367960567.getSize().reset(153.3333f, 153.3333f);
        p1367960567.getPositionOffset().reset(-76.66666f, -76.66666f);
        p1367960567.setIsUseAbsolutePosition(true);
        p1367960567.setIsUseAbsoluteSize(true);
        p1367960567.getImage().setIsUseAbsolutePosition(true);
        p1367960567.getImage().setIsUseAbsoluteSize(true);
        p1367960567.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1367960567.resetPosition();
        getSpiritList().add(p1367960567);
    }

    /* access modifiers changed from: package-private */
    public void c1954697477(Context c) {
        Puzzle p1954697477 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1954697477(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1954697477(editor, this);
            }
        };
        p1954697477.setID(1954697477);
        p1954697477.setName("1954697477");
        p1954697477.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1954697477);
        this.Desktop.RandomlyPlaced(p1954697477);
        p1954697477.setTopEdgeType(EdgeType.Concave);
        p1954697477.setBottomEdgeType(EdgeType.Convex);
        p1954697477.setLeftEdgeType(EdgeType.Convex);
        p1954697477.setRightEdgeType(EdgeType.Convex);
        p1954697477.setTopExactPuzzleID(1367960567);
        p1954697477.setBottomExactPuzzleID(384695074);
        p1954697477.setLeftExactPuzzleID(1428805518);
        p1954697477.setRightExactPuzzleID(273912212);
        p1954697477.getDisplayImage().loadImageFromResource(c, R.drawable.p1954697477h);
        p1954697477.setExactRow(4);
        p1954697477.setExactColumn(2);
        p1954697477.getSize().reset(153.3333f, 126.6667f);
        p1954697477.getPositionOffset().reset(-76.66666f, -50.0f);
        p1954697477.setIsUseAbsolutePosition(true);
        p1954697477.setIsUseAbsoluteSize(true);
        p1954697477.getImage().setIsUseAbsolutePosition(true);
        p1954697477.getImage().setIsUseAbsoluteSize(true);
        p1954697477.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1954697477.resetPosition();
        getSpiritList().add(p1954697477);
    }

    /* access modifiers changed from: package-private */
    public void c384695074(Context c) {
        Puzzle p384695074 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load384695074(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save384695074(editor, this);
            }
        };
        p384695074.setID(384695074);
        p384695074.setName("384695074");
        p384695074.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p384695074);
        this.Desktop.RandomlyPlaced(p384695074);
        p384695074.setTopEdgeType(EdgeType.Concave);
        p384695074.setBottomEdgeType(EdgeType.Flat);
        p384695074.setLeftEdgeType(EdgeType.Concave);
        p384695074.setRightEdgeType(EdgeType.Concave);
        p384695074.setTopExactPuzzleID(1954697477);
        p384695074.setBottomExactPuzzleID(-1);
        p384695074.setLeftExactPuzzleID(893325049);
        p384695074.setRightExactPuzzleID(2089983398);
        p384695074.getDisplayImage().loadImageFromResource(c, R.drawable.p384695074h);
        p384695074.setExactRow(5);
        p384695074.setExactColumn(2);
        p384695074.getSize().reset(100.0f, 100.0f);
        p384695074.getPositionOffset().reset(-50.0f, -50.0f);
        p384695074.setIsUseAbsolutePosition(true);
        p384695074.setIsUseAbsoluteSize(true);
        p384695074.getImage().setIsUseAbsolutePosition(true);
        p384695074.getImage().setIsUseAbsoluteSize(true);
        p384695074.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p384695074.resetPosition();
        getSpiritList().add(p384695074);
    }

    /* access modifiers changed from: package-private */
    public void c1923403094(Context c) {
        Puzzle p1923403094 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1923403094(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1923403094(editor, this);
            }
        };
        p1923403094.setID(1923403094);
        p1923403094.setName("1923403094");
        p1923403094.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1923403094);
        this.Desktop.RandomlyPlaced(p1923403094);
        p1923403094.setTopEdgeType(EdgeType.Flat);
        p1923403094.setBottomEdgeType(EdgeType.Concave);
        p1923403094.setLeftEdgeType(EdgeType.Concave);
        p1923403094.setRightEdgeType(EdgeType.Convex);
        p1923403094.setTopExactPuzzleID(-1);
        p1923403094.setBottomExactPuzzleID(1740885567);
        p1923403094.setLeftExactPuzzleID(2059957329);
        p1923403094.setRightExactPuzzleID(1878338777);
        p1923403094.getDisplayImage().loadImageFromResource(c, R.drawable.p1923403094h);
        p1923403094.setExactRow(0);
        p1923403094.setExactColumn(3);
        p1923403094.getSize().reset(126.6667f, 100.0f);
        p1923403094.getPositionOffset().reset(-50.0f, -50.0f);
        p1923403094.setIsUseAbsolutePosition(true);
        p1923403094.setIsUseAbsoluteSize(true);
        p1923403094.getImage().setIsUseAbsolutePosition(true);
        p1923403094.getImage().setIsUseAbsoluteSize(true);
        p1923403094.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1923403094.resetPosition();
        getSpiritList().add(p1923403094);
    }

    /* access modifiers changed from: package-private */
    public void c1740885567(Context c) {
        Puzzle p1740885567 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1740885567(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1740885567(editor, this);
            }
        };
        p1740885567.setID(1740885567);
        p1740885567.setName("1740885567");
        p1740885567.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1740885567);
        this.Desktop.RandomlyPlaced(p1740885567);
        p1740885567.setTopEdgeType(EdgeType.Convex);
        p1740885567.setBottomEdgeType(EdgeType.Convex);
        p1740885567.setLeftEdgeType(EdgeType.Convex);
        p1740885567.setRightEdgeType(EdgeType.Convex);
        p1740885567.setTopExactPuzzleID(1923403094);
        p1740885567.setBottomExactPuzzleID(1221132387);
        p1740885567.setLeftExactPuzzleID(1656706169);
        p1740885567.setRightExactPuzzleID(1826916394);
        p1740885567.getDisplayImage().loadImageFromResource(c, R.drawable.p1740885567h);
        p1740885567.setExactRow(1);
        p1740885567.setExactColumn(3);
        p1740885567.getSize().reset(153.3333f, 153.3333f);
        p1740885567.getPositionOffset().reset(-76.66666f, -76.66666f);
        p1740885567.setIsUseAbsolutePosition(true);
        p1740885567.setIsUseAbsoluteSize(true);
        p1740885567.getImage().setIsUseAbsolutePosition(true);
        p1740885567.getImage().setIsUseAbsoluteSize(true);
        p1740885567.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1740885567.resetPosition();
        getSpiritList().add(p1740885567);
    }

    /* access modifiers changed from: package-private */
    public void c1221132387(Context c) {
        Puzzle p1221132387 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1221132387(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1221132387(editor, this);
            }
        };
        p1221132387.setID(1221132387);
        p1221132387.setName("1221132387");
        p1221132387.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1221132387);
        this.Desktop.RandomlyPlaced(p1221132387);
        p1221132387.setTopEdgeType(EdgeType.Concave);
        p1221132387.setBottomEdgeType(EdgeType.Concave);
        p1221132387.setLeftEdgeType(EdgeType.Concave);
        p1221132387.setRightEdgeType(EdgeType.Concave);
        p1221132387.setTopExactPuzzleID(1740885567);
        p1221132387.setBottomExactPuzzleID(2104337530);
        p1221132387.setLeftExactPuzzleID(685727458);
        p1221132387.setRightExactPuzzleID(330885544);
        p1221132387.getDisplayImage().loadImageFromResource(c, R.drawable.p1221132387h);
        p1221132387.setExactRow(2);
        p1221132387.setExactColumn(3);
        p1221132387.getSize().reset(100.0f, 100.0f);
        p1221132387.getPositionOffset().reset(-50.0f, -50.0f);
        p1221132387.setIsUseAbsolutePosition(true);
        p1221132387.setIsUseAbsoluteSize(true);
        p1221132387.getImage().setIsUseAbsolutePosition(true);
        p1221132387.getImage().setIsUseAbsoluteSize(true);
        p1221132387.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1221132387.resetPosition();
        getSpiritList().add(p1221132387);
    }

    /* access modifiers changed from: package-private */
    public void c2104337530(Context c) {
        Puzzle p2104337530 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load2104337530(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save2104337530(editor, this);
            }
        };
        p2104337530.setID(2104337530);
        p2104337530.setName("2104337530");
        p2104337530.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p2104337530);
        this.Desktop.RandomlyPlaced(p2104337530);
        p2104337530.setTopEdgeType(EdgeType.Convex);
        p2104337530.setBottomEdgeType(EdgeType.Convex);
        p2104337530.setLeftEdgeType(EdgeType.Concave);
        p2104337530.setRightEdgeType(EdgeType.Convex);
        p2104337530.setTopExactPuzzleID(1221132387);
        p2104337530.setBottomExactPuzzleID(273912212);
        p2104337530.setLeftExactPuzzleID(1367960567);
        p2104337530.setRightExactPuzzleID(1305250614);
        p2104337530.getDisplayImage().loadImageFromResource(c, R.drawable.p2104337530h);
        p2104337530.setExactRow(3);
        p2104337530.setExactColumn(3);
        p2104337530.getSize().reset(126.6667f, 153.3333f);
        p2104337530.getPositionOffset().reset(-50.0f, -76.66666f);
        p2104337530.setIsUseAbsolutePosition(true);
        p2104337530.setIsUseAbsoluteSize(true);
        p2104337530.getImage().setIsUseAbsolutePosition(true);
        p2104337530.getImage().setIsUseAbsoluteSize(true);
        p2104337530.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p2104337530.resetPosition();
        getSpiritList().add(p2104337530);
    }

    /* access modifiers changed from: package-private */
    public void c273912212(Context c) {
        Puzzle p273912212 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load273912212(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save273912212(editor, this);
            }
        };
        p273912212.setID(273912212);
        p273912212.setName("273912212");
        p273912212.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p273912212);
        this.Desktop.RandomlyPlaced(p273912212);
        p273912212.setTopEdgeType(EdgeType.Concave);
        p273912212.setBottomEdgeType(EdgeType.Concave);
        p273912212.setLeftEdgeType(EdgeType.Concave);
        p273912212.setRightEdgeType(EdgeType.Convex);
        p273912212.setTopExactPuzzleID(2104337530);
        p273912212.setBottomExactPuzzleID(2089983398);
        p273912212.setLeftExactPuzzleID(1954697477);
        p273912212.setRightExactPuzzleID(1279035711);
        p273912212.getDisplayImage().loadImageFromResource(c, R.drawable.p273912212h);
        p273912212.setExactRow(4);
        p273912212.setExactColumn(3);
        p273912212.getSize().reset(126.6667f, 100.0f);
        p273912212.getPositionOffset().reset(-50.0f, -50.0f);
        p273912212.setIsUseAbsolutePosition(true);
        p273912212.setIsUseAbsoluteSize(true);
        p273912212.getImage().setIsUseAbsolutePosition(true);
        p273912212.getImage().setIsUseAbsoluteSize(true);
        p273912212.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p273912212.resetPosition();
        getSpiritList().add(p273912212);
    }

    /* access modifiers changed from: package-private */
    public void c2089983398(Context c) {
        Puzzle p2089983398 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load2089983398(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save2089983398(editor, this);
            }
        };
        p2089983398.setID(2089983398);
        p2089983398.setName("2089983398");
        p2089983398.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p2089983398);
        this.Desktop.RandomlyPlaced(p2089983398);
        p2089983398.setTopEdgeType(EdgeType.Convex);
        p2089983398.setBottomEdgeType(EdgeType.Flat);
        p2089983398.setLeftEdgeType(EdgeType.Convex);
        p2089983398.setRightEdgeType(EdgeType.Concave);
        p2089983398.setTopExactPuzzleID(273912212);
        p2089983398.setBottomExactPuzzleID(-1);
        p2089983398.setLeftExactPuzzleID(384695074);
        p2089983398.setRightExactPuzzleID(1735140811);
        p2089983398.getDisplayImage().loadImageFromResource(c, R.drawable.p2089983398h);
        p2089983398.setExactRow(5);
        p2089983398.setExactColumn(3);
        p2089983398.getSize().reset(126.6667f, 126.6667f);
        p2089983398.getPositionOffset().reset(-76.66666f, -76.66666f);
        p2089983398.setIsUseAbsolutePosition(true);
        p2089983398.setIsUseAbsoluteSize(true);
        p2089983398.getImage().setIsUseAbsolutePosition(true);
        p2089983398.getImage().setIsUseAbsoluteSize(true);
        p2089983398.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p2089983398.resetPosition();
        getSpiritList().add(p2089983398);
    }

    /* access modifiers changed from: package-private */
    public void c1878338777(Context c) {
        Puzzle p1878338777 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1878338777(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1878338777(editor, this);
            }
        };
        p1878338777.setID(1878338777);
        p1878338777.setName("1878338777");
        p1878338777.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1878338777);
        this.Desktop.RandomlyPlaced(p1878338777);
        p1878338777.setTopEdgeType(EdgeType.Flat);
        p1878338777.setBottomEdgeType(EdgeType.Convex);
        p1878338777.setLeftEdgeType(EdgeType.Concave);
        p1878338777.setRightEdgeType(EdgeType.Concave);
        p1878338777.setTopExactPuzzleID(-1);
        p1878338777.setBottomExactPuzzleID(1826916394);
        p1878338777.setLeftExactPuzzleID(1923403094);
        p1878338777.setRightExactPuzzleID(1234932860);
        p1878338777.getDisplayImage().loadImageFromResource(c, R.drawable.p1878338777h);
        p1878338777.setExactRow(0);
        p1878338777.setExactColumn(4);
        p1878338777.getSize().reset(100.0f, 126.6667f);
        p1878338777.getPositionOffset().reset(-50.0f, -50.0f);
        p1878338777.setIsUseAbsolutePosition(true);
        p1878338777.setIsUseAbsoluteSize(true);
        p1878338777.getImage().setIsUseAbsolutePosition(true);
        p1878338777.getImage().setIsUseAbsoluteSize(true);
        p1878338777.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1878338777.resetPosition();
        getSpiritList().add(p1878338777);
    }

    /* access modifiers changed from: package-private */
    public void c1826916394(Context c) {
        Puzzle p1826916394 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1826916394(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1826916394(editor, this);
            }
        };
        p1826916394.setID(1826916394);
        p1826916394.setName("1826916394");
        p1826916394.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1826916394);
        this.Desktop.RandomlyPlaced(p1826916394);
        p1826916394.setTopEdgeType(EdgeType.Concave);
        p1826916394.setBottomEdgeType(EdgeType.Concave);
        p1826916394.setLeftEdgeType(EdgeType.Concave);
        p1826916394.setRightEdgeType(EdgeType.Convex);
        p1826916394.setTopExactPuzzleID(1878338777);
        p1826916394.setBottomExactPuzzleID(330885544);
        p1826916394.setLeftExactPuzzleID(1740885567);
        p1826916394.setRightExactPuzzleID(813043902);
        p1826916394.getDisplayImage().loadImageFromResource(c, R.drawable.p1826916394h);
        p1826916394.setExactRow(1);
        p1826916394.setExactColumn(4);
        p1826916394.getSize().reset(126.6667f, 100.0f);
        p1826916394.getPositionOffset().reset(-50.0f, -50.0f);
        p1826916394.setIsUseAbsolutePosition(true);
        p1826916394.setIsUseAbsoluteSize(true);
        p1826916394.getImage().setIsUseAbsolutePosition(true);
        p1826916394.getImage().setIsUseAbsoluteSize(true);
        p1826916394.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1826916394.resetPosition();
        getSpiritList().add(p1826916394);
    }

    /* access modifiers changed from: package-private */
    public void c330885544(Context c) {
        Puzzle p330885544 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load330885544(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save330885544(editor, this);
            }
        };
        p330885544.setID(330885544);
        p330885544.setName("330885544");
        p330885544.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p330885544);
        this.Desktop.RandomlyPlaced(p330885544);
        p330885544.setTopEdgeType(EdgeType.Convex);
        p330885544.setBottomEdgeType(EdgeType.Convex);
        p330885544.setLeftEdgeType(EdgeType.Convex);
        p330885544.setRightEdgeType(EdgeType.Concave);
        p330885544.setTopExactPuzzleID(1826916394);
        p330885544.setBottomExactPuzzleID(1305250614);
        p330885544.setLeftExactPuzzleID(1221132387);
        p330885544.setRightExactPuzzleID(382685693);
        p330885544.getDisplayImage().loadImageFromResource(c, R.drawable.p330885544h);
        p330885544.setExactRow(2);
        p330885544.setExactColumn(4);
        p330885544.getSize().reset(126.6667f, 153.3333f);
        p330885544.getPositionOffset().reset(-76.66666f, -76.66666f);
        p330885544.setIsUseAbsolutePosition(true);
        p330885544.setIsUseAbsoluteSize(true);
        p330885544.getImage().setIsUseAbsolutePosition(true);
        p330885544.getImage().setIsUseAbsoluteSize(true);
        p330885544.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p330885544.resetPosition();
        getSpiritList().add(p330885544);
    }

    /* access modifiers changed from: package-private */
    public void c1305250614(Context c) {
        Puzzle p1305250614 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1305250614(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1305250614(editor, this);
            }
        };
        p1305250614.setID(1305250614);
        p1305250614.setName("1305250614");
        p1305250614.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1305250614);
        this.Desktop.RandomlyPlaced(p1305250614);
        p1305250614.setTopEdgeType(EdgeType.Concave);
        p1305250614.setBottomEdgeType(EdgeType.Concave);
        p1305250614.setLeftEdgeType(EdgeType.Concave);
        p1305250614.setRightEdgeType(EdgeType.Convex);
        p1305250614.setTopExactPuzzleID(330885544);
        p1305250614.setBottomExactPuzzleID(1279035711);
        p1305250614.setLeftExactPuzzleID(2104337530);
        p1305250614.setRightExactPuzzleID(1781484329);
        p1305250614.getDisplayImage().loadImageFromResource(c, R.drawable.p1305250614h);
        p1305250614.setExactRow(3);
        p1305250614.setExactColumn(4);
        p1305250614.getSize().reset(126.6667f, 100.0f);
        p1305250614.getPositionOffset().reset(-50.0f, -50.0f);
        p1305250614.setIsUseAbsolutePosition(true);
        p1305250614.setIsUseAbsoluteSize(true);
        p1305250614.getImage().setIsUseAbsolutePosition(true);
        p1305250614.getImage().setIsUseAbsoluteSize(true);
        p1305250614.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1305250614.resetPosition();
        getSpiritList().add(p1305250614);
    }

    /* access modifiers changed from: package-private */
    public void c1279035711(Context c) {
        Puzzle p1279035711 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1279035711(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1279035711(editor, this);
            }
        };
        p1279035711.setID(1279035711);
        p1279035711.setName("1279035711");
        p1279035711.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1279035711);
        this.Desktop.RandomlyPlaced(p1279035711);
        p1279035711.setTopEdgeType(EdgeType.Convex);
        p1279035711.setBottomEdgeType(EdgeType.Concave);
        p1279035711.setLeftEdgeType(EdgeType.Concave);
        p1279035711.setRightEdgeType(EdgeType.Concave);
        p1279035711.setTopExactPuzzleID(1305250614);
        p1279035711.setBottomExactPuzzleID(1735140811);
        p1279035711.setLeftExactPuzzleID(273912212);
        p1279035711.setRightExactPuzzleID(483678178);
        p1279035711.getDisplayImage().loadImageFromResource(c, R.drawable.p1279035711h);
        p1279035711.setExactRow(4);
        p1279035711.setExactColumn(4);
        p1279035711.getSize().reset(100.0f, 126.6667f);
        p1279035711.getPositionOffset().reset(-50.0f, -76.66666f);
        p1279035711.setIsUseAbsolutePosition(true);
        p1279035711.setIsUseAbsoluteSize(true);
        p1279035711.getImage().setIsUseAbsolutePosition(true);
        p1279035711.getImage().setIsUseAbsoluteSize(true);
        p1279035711.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1279035711.resetPosition();
        getSpiritList().add(p1279035711);
    }

    /* access modifiers changed from: package-private */
    public void c1735140811(Context c) {
        Puzzle p1735140811 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1735140811(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1735140811(editor, this);
            }
        };
        p1735140811.setID(1735140811);
        p1735140811.setName("1735140811");
        p1735140811.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1735140811);
        this.Desktop.RandomlyPlaced(p1735140811);
        p1735140811.setTopEdgeType(EdgeType.Convex);
        p1735140811.setBottomEdgeType(EdgeType.Flat);
        p1735140811.setLeftEdgeType(EdgeType.Convex);
        p1735140811.setRightEdgeType(EdgeType.Concave);
        p1735140811.setTopExactPuzzleID(1279035711);
        p1735140811.setBottomExactPuzzleID(-1);
        p1735140811.setLeftExactPuzzleID(2089983398);
        p1735140811.setRightExactPuzzleID(663555648);
        p1735140811.getDisplayImage().loadImageFromResource(c, R.drawable.p1735140811h);
        p1735140811.setExactRow(5);
        p1735140811.setExactColumn(4);
        p1735140811.getSize().reset(126.6667f, 126.6667f);
        p1735140811.getPositionOffset().reset(-76.66666f, -76.66666f);
        p1735140811.setIsUseAbsolutePosition(true);
        p1735140811.setIsUseAbsoluteSize(true);
        p1735140811.getImage().setIsUseAbsolutePosition(true);
        p1735140811.getImage().setIsUseAbsoluteSize(true);
        p1735140811.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1735140811.resetPosition();
        getSpiritList().add(p1735140811);
    }

    /* access modifiers changed from: package-private */
    public void c1234932860(Context c) {
        Puzzle p1234932860 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1234932860(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1234932860(editor, this);
            }
        };
        p1234932860.setID(1234932860);
        p1234932860.setName("1234932860");
        p1234932860.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1234932860);
        this.Desktop.RandomlyPlaced(p1234932860);
        p1234932860.setTopEdgeType(EdgeType.Flat);
        p1234932860.setBottomEdgeType(EdgeType.Concave);
        p1234932860.setLeftEdgeType(EdgeType.Convex);
        p1234932860.setRightEdgeType(EdgeType.Convex);
        p1234932860.setTopExactPuzzleID(-1);
        p1234932860.setBottomExactPuzzleID(813043902);
        p1234932860.setLeftExactPuzzleID(1878338777);
        p1234932860.setRightExactPuzzleID(1397996497);
        p1234932860.getDisplayImage().loadImageFromResource(c, R.drawable.p1234932860h);
        p1234932860.setExactRow(0);
        p1234932860.setExactColumn(5);
        p1234932860.getSize().reset(153.3333f, 100.0f);
        p1234932860.getPositionOffset().reset(-76.66666f, -50.0f);
        p1234932860.setIsUseAbsolutePosition(true);
        p1234932860.setIsUseAbsoluteSize(true);
        p1234932860.getImage().setIsUseAbsolutePosition(true);
        p1234932860.getImage().setIsUseAbsoluteSize(true);
        p1234932860.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1234932860.resetPosition();
        getSpiritList().add(p1234932860);
    }

    /* access modifiers changed from: package-private */
    public void c813043902(Context c) {
        Puzzle p813043902 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load813043902(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save813043902(editor, this);
            }
        };
        p813043902.setID(813043902);
        p813043902.setName("813043902");
        p813043902.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p813043902);
        this.Desktop.RandomlyPlaced(p813043902);
        p813043902.setTopEdgeType(EdgeType.Convex);
        p813043902.setBottomEdgeType(EdgeType.Convex);
        p813043902.setLeftEdgeType(EdgeType.Concave);
        p813043902.setRightEdgeType(EdgeType.Convex);
        p813043902.setTopExactPuzzleID(1234932860);
        p813043902.setBottomExactPuzzleID(382685693);
        p813043902.setLeftExactPuzzleID(1826916394);
        p813043902.setRightExactPuzzleID(152087478);
        p813043902.getDisplayImage().loadImageFromResource(c, R.drawable.p813043902h);
        p813043902.setExactRow(1);
        p813043902.setExactColumn(5);
        p813043902.getSize().reset(126.6667f, 153.3333f);
        p813043902.getPositionOffset().reset(-50.0f, -76.66666f);
        p813043902.setIsUseAbsolutePosition(true);
        p813043902.setIsUseAbsoluteSize(true);
        p813043902.getImage().setIsUseAbsolutePosition(true);
        p813043902.getImage().setIsUseAbsoluteSize(true);
        p813043902.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p813043902.resetPosition();
        getSpiritList().add(p813043902);
    }

    /* access modifiers changed from: package-private */
    public void c382685693(Context c) {
        Puzzle p382685693 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load382685693(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save382685693(editor, this);
            }
        };
        p382685693.setID(382685693);
        p382685693.setName("382685693");
        p382685693.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p382685693);
        this.Desktop.RandomlyPlaced(p382685693);
        p382685693.setTopEdgeType(EdgeType.Concave);
        p382685693.setBottomEdgeType(EdgeType.Convex);
        p382685693.setLeftEdgeType(EdgeType.Convex);
        p382685693.setRightEdgeType(EdgeType.Convex);
        p382685693.setTopExactPuzzleID(813043902);
        p382685693.setBottomExactPuzzleID(1781484329);
        p382685693.setLeftExactPuzzleID(330885544);
        p382685693.setRightExactPuzzleID(1914205414);
        p382685693.getDisplayImage().loadImageFromResource(c, R.drawable.p382685693h);
        p382685693.setExactRow(2);
        p382685693.setExactColumn(5);
        p382685693.getSize().reset(153.3333f, 126.6667f);
        p382685693.getPositionOffset().reset(-76.66666f, -50.0f);
        p382685693.setIsUseAbsolutePosition(true);
        p382685693.setIsUseAbsoluteSize(true);
        p382685693.getImage().setIsUseAbsolutePosition(true);
        p382685693.getImage().setIsUseAbsoluteSize(true);
        p382685693.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p382685693.resetPosition();
        getSpiritList().add(p382685693);
    }

    /* access modifiers changed from: package-private */
    public void c1781484329(Context c) {
        Puzzle p1781484329 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1781484329(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1781484329(editor, this);
            }
        };
        p1781484329.setID(1781484329);
        p1781484329.setName("1781484329");
        p1781484329.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1781484329);
        this.Desktop.RandomlyPlaced(p1781484329);
        p1781484329.setTopEdgeType(EdgeType.Concave);
        p1781484329.setBottomEdgeType(EdgeType.Concave);
        p1781484329.setLeftEdgeType(EdgeType.Concave);
        p1781484329.setRightEdgeType(EdgeType.Concave);
        p1781484329.setTopExactPuzzleID(382685693);
        p1781484329.setBottomExactPuzzleID(483678178);
        p1781484329.setLeftExactPuzzleID(1305250614);
        p1781484329.setRightExactPuzzleID(134771039);
        p1781484329.getDisplayImage().loadImageFromResource(c, R.drawable.p1781484329h);
        p1781484329.setExactRow(3);
        p1781484329.setExactColumn(5);
        p1781484329.getSize().reset(100.0f, 100.0f);
        p1781484329.getPositionOffset().reset(-50.0f, -50.0f);
        p1781484329.setIsUseAbsolutePosition(true);
        p1781484329.setIsUseAbsoluteSize(true);
        p1781484329.getImage().setIsUseAbsolutePosition(true);
        p1781484329.getImage().setIsUseAbsoluteSize(true);
        p1781484329.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1781484329.resetPosition();
        getSpiritList().add(p1781484329);
    }

    /* access modifiers changed from: package-private */
    public void c483678178(Context c) {
        Puzzle p483678178 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load483678178(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save483678178(editor, this);
            }
        };
        p483678178.setID(483678178);
        p483678178.setName("483678178");
        p483678178.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p483678178);
        this.Desktop.RandomlyPlaced(p483678178);
        p483678178.setTopEdgeType(EdgeType.Convex);
        p483678178.setBottomEdgeType(EdgeType.Concave);
        p483678178.setLeftEdgeType(EdgeType.Convex);
        p483678178.setRightEdgeType(EdgeType.Convex);
        p483678178.setTopExactPuzzleID(1781484329);
        p483678178.setBottomExactPuzzleID(663555648);
        p483678178.setLeftExactPuzzleID(1279035711);
        p483678178.setRightExactPuzzleID(1772970260);
        p483678178.getDisplayImage().loadImageFromResource(c, R.drawable.p483678178h);
        p483678178.setExactRow(4);
        p483678178.setExactColumn(5);
        p483678178.getSize().reset(153.3333f, 126.6667f);
        p483678178.getPositionOffset().reset(-76.66666f, -76.66666f);
        p483678178.setIsUseAbsolutePosition(true);
        p483678178.setIsUseAbsoluteSize(true);
        p483678178.getImage().setIsUseAbsolutePosition(true);
        p483678178.getImage().setIsUseAbsoluteSize(true);
        p483678178.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p483678178.resetPosition();
        getSpiritList().add(p483678178);
    }

    /* access modifiers changed from: package-private */
    public void c663555648(Context c) {
        Puzzle p663555648 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load663555648(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save663555648(editor, this);
            }
        };
        p663555648.setID(663555648);
        p663555648.setName("663555648");
        p663555648.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p663555648);
        this.Desktop.RandomlyPlaced(p663555648);
        p663555648.setTopEdgeType(EdgeType.Convex);
        p663555648.setBottomEdgeType(EdgeType.Flat);
        p663555648.setLeftEdgeType(EdgeType.Convex);
        p663555648.setRightEdgeType(EdgeType.Concave);
        p663555648.setTopExactPuzzleID(483678178);
        p663555648.setBottomExactPuzzleID(-1);
        p663555648.setLeftExactPuzzleID(1735140811);
        p663555648.setRightExactPuzzleID(790487535);
        p663555648.getDisplayImage().loadImageFromResource(c, R.drawable.p663555648h);
        p663555648.setExactRow(5);
        p663555648.setExactColumn(5);
        p663555648.getSize().reset(126.6667f, 126.6667f);
        p663555648.getPositionOffset().reset(-76.66666f, -76.66666f);
        p663555648.setIsUseAbsolutePosition(true);
        p663555648.setIsUseAbsoluteSize(true);
        p663555648.getImage().setIsUseAbsolutePosition(true);
        p663555648.getImage().setIsUseAbsoluteSize(true);
        p663555648.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p663555648.resetPosition();
        getSpiritList().add(p663555648);
    }

    /* access modifiers changed from: package-private */
    public void c1397996497(Context c) {
        Puzzle p1397996497 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1397996497(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1397996497(editor, this);
            }
        };
        p1397996497.setID(1397996497);
        p1397996497.setName("1397996497");
        p1397996497.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1397996497);
        this.Desktop.RandomlyPlaced(p1397996497);
        p1397996497.setTopEdgeType(EdgeType.Flat);
        p1397996497.setBottomEdgeType(EdgeType.Concave);
        p1397996497.setLeftEdgeType(EdgeType.Concave);
        p1397996497.setRightEdgeType(EdgeType.Convex);
        p1397996497.setTopExactPuzzleID(-1);
        p1397996497.setBottomExactPuzzleID(152087478);
        p1397996497.setLeftExactPuzzleID(1234932860);
        p1397996497.setRightExactPuzzleID(824110566);
        p1397996497.getDisplayImage().loadImageFromResource(c, R.drawable.p1397996497h);
        p1397996497.setExactRow(0);
        p1397996497.setExactColumn(6);
        p1397996497.getSize().reset(126.6667f, 100.0f);
        p1397996497.getPositionOffset().reset(-50.0f, -50.0f);
        p1397996497.setIsUseAbsolutePosition(true);
        p1397996497.setIsUseAbsoluteSize(true);
        p1397996497.getImage().setIsUseAbsolutePosition(true);
        p1397996497.getImage().setIsUseAbsoluteSize(true);
        p1397996497.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1397996497.resetPosition();
        getSpiritList().add(p1397996497);
    }

    /* access modifiers changed from: package-private */
    public void c152087478(Context c) {
        Puzzle p152087478 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load152087478(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save152087478(editor, this);
            }
        };
        p152087478.setID(152087478);
        p152087478.setName("152087478");
        p152087478.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p152087478);
        this.Desktop.RandomlyPlaced(p152087478);
        p152087478.setTopEdgeType(EdgeType.Convex);
        p152087478.setBottomEdgeType(EdgeType.Concave);
        p152087478.setLeftEdgeType(EdgeType.Concave);
        p152087478.setRightEdgeType(EdgeType.Concave);
        p152087478.setTopExactPuzzleID(1397996497);
        p152087478.setBottomExactPuzzleID(1914205414);
        p152087478.setLeftExactPuzzleID(813043902);
        p152087478.setRightExactPuzzleID(1635278835);
        p152087478.getDisplayImage().loadImageFromResource(c, R.drawable.p152087478h);
        p152087478.setExactRow(1);
        p152087478.setExactColumn(6);
        p152087478.getSize().reset(100.0f, 126.6667f);
        p152087478.getPositionOffset().reset(-50.0f, -76.66666f);
        p152087478.setIsUseAbsolutePosition(true);
        p152087478.setIsUseAbsoluteSize(true);
        p152087478.getImage().setIsUseAbsolutePosition(true);
        p152087478.getImage().setIsUseAbsoluteSize(true);
        p152087478.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p152087478.resetPosition();
        getSpiritList().add(p152087478);
    }

    /* access modifiers changed from: package-private */
    public void c1914205414(Context c) {
        Puzzle p1914205414 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1914205414(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1914205414(editor, this);
            }
        };
        p1914205414.setID(1914205414);
        p1914205414.setName("1914205414");
        p1914205414.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1914205414);
        this.Desktop.RandomlyPlaced(p1914205414);
        p1914205414.setTopEdgeType(EdgeType.Convex);
        p1914205414.setBottomEdgeType(EdgeType.Concave);
        p1914205414.setLeftEdgeType(EdgeType.Concave);
        p1914205414.setRightEdgeType(EdgeType.Convex);
        p1914205414.setTopExactPuzzleID(152087478);
        p1914205414.setBottomExactPuzzleID(134771039);
        p1914205414.setLeftExactPuzzleID(382685693);
        p1914205414.setRightExactPuzzleID(1736476003);
        p1914205414.getDisplayImage().loadImageFromResource(c, R.drawable.p1914205414h);
        p1914205414.setExactRow(2);
        p1914205414.setExactColumn(6);
        p1914205414.getSize().reset(126.6667f, 126.6667f);
        p1914205414.getPositionOffset().reset(-50.0f, -76.66666f);
        p1914205414.setIsUseAbsolutePosition(true);
        p1914205414.setIsUseAbsoluteSize(true);
        p1914205414.getImage().setIsUseAbsolutePosition(true);
        p1914205414.getImage().setIsUseAbsoluteSize(true);
        p1914205414.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1914205414.resetPosition();
        getSpiritList().add(p1914205414);
    }

    /* access modifiers changed from: package-private */
    public void c134771039(Context c) {
        Puzzle p134771039 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load134771039(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save134771039(editor, this);
            }
        };
        p134771039.setID(134771039);
        p134771039.setName("134771039");
        p134771039.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p134771039);
        this.Desktop.RandomlyPlaced(p134771039);
        p134771039.setTopEdgeType(EdgeType.Convex);
        p134771039.setBottomEdgeType(EdgeType.Convex);
        p134771039.setLeftEdgeType(EdgeType.Convex);
        p134771039.setRightEdgeType(EdgeType.Convex);
        p134771039.setTopExactPuzzleID(1914205414);
        p134771039.setBottomExactPuzzleID(1772970260);
        p134771039.setLeftExactPuzzleID(1781484329);
        p134771039.setRightExactPuzzleID(222475235);
        p134771039.getDisplayImage().loadImageFromResource(c, R.drawable.p134771039h);
        p134771039.setExactRow(3);
        p134771039.setExactColumn(6);
        p134771039.getSize().reset(153.3333f, 153.3333f);
        p134771039.getPositionOffset().reset(-76.66666f, -76.66666f);
        p134771039.setIsUseAbsolutePosition(true);
        p134771039.setIsUseAbsoluteSize(true);
        p134771039.getImage().setIsUseAbsolutePosition(true);
        p134771039.getImage().setIsUseAbsoluteSize(true);
        p134771039.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p134771039.resetPosition();
        getSpiritList().add(p134771039);
    }

    /* access modifiers changed from: package-private */
    public void c1772970260(Context c) {
        Puzzle p1772970260 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1772970260(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1772970260(editor, this);
            }
        };
        p1772970260.setID(1772970260);
        p1772970260.setName("1772970260");
        p1772970260.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1772970260);
        this.Desktop.RandomlyPlaced(p1772970260);
        p1772970260.setTopEdgeType(EdgeType.Concave);
        p1772970260.setBottomEdgeType(EdgeType.Convex);
        p1772970260.setLeftEdgeType(EdgeType.Concave);
        p1772970260.setRightEdgeType(EdgeType.Concave);
        p1772970260.setTopExactPuzzleID(134771039);
        p1772970260.setBottomExactPuzzleID(790487535);
        p1772970260.setLeftExactPuzzleID(483678178);
        p1772970260.setRightExactPuzzleID(298225912);
        p1772970260.getDisplayImage().loadImageFromResource(c, R.drawable.p1772970260h);
        p1772970260.setExactRow(4);
        p1772970260.setExactColumn(6);
        p1772970260.getSize().reset(100.0f, 126.6667f);
        p1772970260.getPositionOffset().reset(-50.0f, -50.0f);
        p1772970260.setIsUseAbsolutePosition(true);
        p1772970260.setIsUseAbsoluteSize(true);
        p1772970260.getImage().setIsUseAbsolutePosition(true);
        p1772970260.getImage().setIsUseAbsoluteSize(true);
        p1772970260.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1772970260.resetPosition();
        getSpiritList().add(p1772970260);
    }

    /* access modifiers changed from: package-private */
    public void c790487535(Context c) {
        Puzzle p790487535 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load790487535(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save790487535(editor, this);
            }
        };
        p790487535.setID(790487535);
        p790487535.setName("790487535");
        p790487535.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p790487535);
        this.Desktop.RandomlyPlaced(p790487535);
        p790487535.setTopEdgeType(EdgeType.Concave);
        p790487535.setBottomEdgeType(EdgeType.Flat);
        p790487535.setLeftEdgeType(EdgeType.Convex);
        p790487535.setRightEdgeType(EdgeType.Convex);
        p790487535.setTopExactPuzzleID(1772970260);
        p790487535.setBottomExactPuzzleID(-1);
        p790487535.setLeftExactPuzzleID(663555648);
        p790487535.setRightExactPuzzleID(1702896116);
        p790487535.getDisplayImage().loadImageFromResource(c, R.drawable.p790487535h);
        p790487535.setExactRow(5);
        p790487535.setExactColumn(6);
        p790487535.getSize().reset(153.3333f, 100.0f);
        p790487535.getPositionOffset().reset(-76.66666f, -50.0f);
        p790487535.setIsUseAbsolutePosition(true);
        p790487535.setIsUseAbsoluteSize(true);
        p790487535.getImage().setIsUseAbsolutePosition(true);
        p790487535.getImage().setIsUseAbsoluteSize(true);
        p790487535.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p790487535.resetPosition();
        getSpiritList().add(p790487535);
    }

    /* access modifiers changed from: package-private */
    public void c824110566(Context c) {
        Puzzle p824110566 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load824110566(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save824110566(editor, this);
            }
        };
        p824110566.setID(824110566);
        p824110566.setName("824110566");
        p824110566.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p824110566);
        this.Desktop.RandomlyPlaced(p824110566);
        p824110566.setTopEdgeType(EdgeType.Flat);
        p824110566.setBottomEdgeType(EdgeType.Convex);
        p824110566.setLeftEdgeType(EdgeType.Concave);
        p824110566.setRightEdgeType(EdgeType.Flat);
        p824110566.setTopExactPuzzleID(-1);
        p824110566.setBottomExactPuzzleID(1635278835);
        p824110566.setLeftExactPuzzleID(1397996497);
        p824110566.setRightExactPuzzleID(-1);
        p824110566.getDisplayImage().loadImageFromResource(c, R.drawable.p824110566h);
        p824110566.setExactRow(0);
        p824110566.setExactColumn(7);
        p824110566.getSize().reset(100.0f, 126.6667f);
        p824110566.getPositionOffset().reset(-50.0f, -50.0f);
        p824110566.setIsUseAbsolutePosition(true);
        p824110566.setIsUseAbsoluteSize(true);
        p824110566.getImage().setIsUseAbsolutePosition(true);
        p824110566.getImage().setIsUseAbsoluteSize(true);
        p824110566.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p824110566.resetPosition();
        getSpiritList().add(p824110566);
    }

    /* access modifiers changed from: package-private */
    public void c1635278835(Context c) {
        Puzzle p1635278835 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1635278835(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1635278835(editor, this);
            }
        };
        p1635278835.setID(1635278835);
        p1635278835.setName("1635278835");
        p1635278835.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1635278835);
        this.Desktop.RandomlyPlaced(p1635278835);
        p1635278835.setTopEdgeType(EdgeType.Concave);
        p1635278835.setBottomEdgeType(EdgeType.Convex);
        p1635278835.setLeftEdgeType(EdgeType.Convex);
        p1635278835.setRightEdgeType(EdgeType.Flat);
        p1635278835.setTopExactPuzzleID(824110566);
        p1635278835.setBottomExactPuzzleID(1736476003);
        p1635278835.setLeftExactPuzzleID(152087478);
        p1635278835.setRightExactPuzzleID(-1);
        p1635278835.getDisplayImage().loadImageFromResource(c, R.drawable.p1635278835h);
        p1635278835.setExactRow(1);
        p1635278835.setExactColumn(7);
        p1635278835.getSize().reset(126.6667f, 126.6667f);
        p1635278835.getPositionOffset().reset(-76.66666f, -50.0f);
        p1635278835.setIsUseAbsolutePosition(true);
        p1635278835.setIsUseAbsoluteSize(true);
        p1635278835.getImage().setIsUseAbsolutePosition(true);
        p1635278835.getImage().setIsUseAbsoluteSize(true);
        p1635278835.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1635278835.resetPosition();
        getSpiritList().add(p1635278835);
    }

    /* access modifiers changed from: package-private */
    public void c1736476003(Context c) {
        Puzzle p1736476003 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1736476003(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1736476003(editor, this);
            }
        };
        p1736476003.setID(1736476003);
        p1736476003.setName("1736476003");
        p1736476003.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1736476003);
        this.Desktop.RandomlyPlaced(p1736476003);
        p1736476003.setTopEdgeType(EdgeType.Concave);
        p1736476003.setBottomEdgeType(EdgeType.Convex);
        p1736476003.setLeftEdgeType(EdgeType.Concave);
        p1736476003.setRightEdgeType(EdgeType.Flat);
        p1736476003.setTopExactPuzzleID(1635278835);
        p1736476003.setBottomExactPuzzleID(222475235);
        p1736476003.setLeftExactPuzzleID(1914205414);
        p1736476003.setRightExactPuzzleID(-1);
        p1736476003.getDisplayImage().loadImageFromResource(c, R.drawable.p1736476003h);
        p1736476003.setExactRow(2);
        p1736476003.setExactColumn(7);
        p1736476003.getSize().reset(100.0f, 126.6667f);
        p1736476003.getPositionOffset().reset(-50.0f, -50.0f);
        p1736476003.setIsUseAbsolutePosition(true);
        p1736476003.setIsUseAbsoluteSize(true);
        p1736476003.getImage().setIsUseAbsolutePosition(true);
        p1736476003.getImage().setIsUseAbsoluteSize(true);
        p1736476003.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1736476003.resetPosition();
        getSpiritList().add(p1736476003);
    }

    /* access modifiers changed from: package-private */
    public void c222475235(Context c) {
        Puzzle p222475235 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load222475235(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save222475235(editor, this);
            }
        };
        p222475235.setID(222475235);
        p222475235.setName("222475235");
        p222475235.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p222475235);
        this.Desktop.RandomlyPlaced(p222475235);
        p222475235.setTopEdgeType(EdgeType.Concave);
        p222475235.setBottomEdgeType(EdgeType.Concave);
        p222475235.setLeftEdgeType(EdgeType.Concave);
        p222475235.setRightEdgeType(EdgeType.Flat);
        p222475235.setTopExactPuzzleID(1736476003);
        p222475235.setBottomExactPuzzleID(298225912);
        p222475235.setLeftExactPuzzleID(134771039);
        p222475235.setRightExactPuzzleID(-1);
        p222475235.getDisplayImage().loadImageFromResource(c, R.drawable.p222475235h);
        p222475235.setExactRow(3);
        p222475235.setExactColumn(7);
        p222475235.getSize().reset(100.0f, 100.0f);
        p222475235.getPositionOffset().reset(-50.0f, -50.0f);
        p222475235.setIsUseAbsolutePosition(true);
        p222475235.setIsUseAbsoluteSize(true);
        p222475235.getImage().setIsUseAbsolutePosition(true);
        p222475235.getImage().setIsUseAbsoluteSize(true);
        p222475235.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p222475235.resetPosition();
        getSpiritList().add(p222475235);
    }

    /* access modifiers changed from: package-private */
    public void c298225912(Context c) {
        Puzzle p298225912 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load298225912(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save298225912(editor, this);
            }
        };
        p298225912.setID(298225912);
        p298225912.setName("298225912");
        p298225912.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p298225912);
        this.Desktop.RandomlyPlaced(p298225912);
        p298225912.setTopEdgeType(EdgeType.Convex);
        p298225912.setBottomEdgeType(EdgeType.Convex);
        p298225912.setLeftEdgeType(EdgeType.Convex);
        p298225912.setRightEdgeType(EdgeType.Flat);
        p298225912.setTopExactPuzzleID(222475235);
        p298225912.setBottomExactPuzzleID(1702896116);
        p298225912.setLeftExactPuzzleID(1772970260);
        p298225912.setRightExactPuzzleID(-1);
        p298225912.getDisplayImage().loadImageFromResource(c, R.drawable.p298225912h);
        p298225912.setExactRow(4);
        p298225912.setExactColumn(7);
        p298225912.getSize().reset(126.6667f, 153.3333f);
        p298225912.getPositionOffset().reset(-76.66666f, -76.66666f);
        p298225912.setIsUseAbsolutePosition(true);
        p298225912.setIsUseAbsoluteSize(true);
        p298225912.getImage().setIsUseAbsolutePosition(true);
        p298225912.getImage().setIsUseAbsoluteSize(true);
        p298225912.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p298225912.resetPosition();
        getSpiritList().add(p298225912);
    }

    /* access modifiers changed from: package-private */
    public void c1702896116(Context c) {
        Puzzle p1702896116 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1702896116(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1702896116(editor, this);
            }
        };
        p1702896116.setID(1702896116);
        p1702896116.setName("1702896116");
        p1702896116.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1702896116);
        this.Desktop.RandomlyPlaced(p1702896116);
        p1702896116.setTopEdgeType(EdgeType.Concave);
        p1702896116.setBottomEdgeType(EdgeType.Flat);
        p1702896116.setLeftEdgeType(EdgeType.Concave);
        p1702896116.setRightEdgeType(EdgeType.Flat);
        p1702896116.setTopExactPuzzleID(298225912);
        p1702896116.setBottomExactPuzzleID(-1);
        p1702896116.setLeftExactPuzzleID(790487535);
        p1702896116.setRightExactPuzzleID(-1);
        p1702896116.getDisplayImage().loadImageFromResource(c, R.drawable.p1702896116h);
        p1702896116.setExactRow(5);
        p1702896116.setExactColumn(7);
        p1702896116.getSize().reset(100.0f, 100.0f);
        p1702896116.getPositionOffset().reset(-50.0f, -50.0f);
        p1702896116.setIsUseAbsolutePosition(true);
        p1702896116.setIsUseAbsoluteSize(true);
        p1702896116.getImage().setIsUseAbsolutePosition(true);
        p1702896116.getImage().setIsUseAbsoluteSize(true);
        p1702896116.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1702896116.resetPosition();
        getSpiritList().add(p1702896116);
    }
}
