package com.scoreloop.client.android.ui.component.challenge;

import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.scoreloop.client.android.core.model.Challenge;
import com.scoreloop.client.android.ui.component.base.ComponentActivity;
import com.scoreloop.client.android.ui.framework.BaseListItem;
import com.skyd.bestpuzzle.n1663.R;

class ChallengeControlsListItem extends BaseListItem {
    private final Challenge _challenge;
    /* access modifiers changed from: private */
    public boolean _controlsEnabled = true;
    /* access modifiers changed from: private */
    public OnControlObserver _onControlObserver;

    interface OnControlObserver {
        void onControl1();

        void onControl2();
    }

    public ChallengeControlsListItem(ComponentActivity context, Challenge challenge, OnControlObserver observer) {
        super(context, null, null);
        this._challenge = challenge;
        this._onControlObserver = observer;
    }

    public int getType() {
        return 3;
    }

    public View getView(View view, ViewGroup parent) {
        if (view == null) {
            view = getLayoutInflater().inflate((int) R.layout.sl_list_item_challenge_controls, (ViewGroup) null);
        }
        prepareView(view);
        return view;
    }

    public boolean isEnabled() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void prepareView(View view) {
        Button control1 = (Button) view.findViewById(R.id.control1);
        control1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (ChallengeControlsListItem.this._controlsEnabled) {
                    ChallengeControlsListItem.this._controlsEnabled = false;
                    ChallengeControlsListItem.this._onControlObserver.onControl1();
                }
            }
        });
        Button control2 = (Button) view.findViewById(R.id.control2);
        control2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (ChallengeControlsListItem.this._controlsEnabled) {
                    ChallengeControlsListItem.this._controlsEnabled = false;
                    ChallengeControlsListItem.this._onControlObserver.onControl2();
                }
            }
        });
        if (this._challenge == null) {
            control1.setText(getContext().getResources().getString(R.string.sl_create_challenge));
            control2.setVisibility(8);
            return;
        }
        control1.setText(getContext().getResources().getString(R.string.sl_accept_start_challenge));
        if (this._challenge.isAssigned()) {
            control2.setText(getContext().getResources().getString(R.string.sl_reject_challenge));
            control2.setVisibility(0);
            return;
        }
        control2.setVisibility(8);
    }
}
