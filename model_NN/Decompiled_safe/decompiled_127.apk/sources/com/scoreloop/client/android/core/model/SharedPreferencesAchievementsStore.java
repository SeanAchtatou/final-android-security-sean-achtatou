package com.scoreloop.client.android.core.model;

import android.content.Context;
import android.content.SharedPreferences;
import org.json.JSONException;
import org.json.JSONObject;

public class SharedPreferencesAchievementsStore implements AchievementsStore {
    private final Context a;
    private final String b;

    public SharedPreferencesAchievementsStore(Context context, String str) {
        this.a = context;
        this.b = str;
    }

    private String d() {
        return e() + "_data";
    }

    private String e() {
        return "com.scoreloop.achievements.store_" + c();
    }

    public JSONObject a(String str) {
        String string = this.a.getSharedPreferences(d(), 0).getString(str, null);
        if (string == null) {
            return null;
        }
        try {
            return new JSONObject(string);
        } catch (JSONException e) {
            return null;
        }
    }

    public void a(String str, JSONObject jSONObject) {
        SharedPreferences.Editor edit = this.a.getSharedPreferences(d(), 0).edit();
        edit.putString(str, jSONObject.toString());
        edit.commit();
    }

    public boolean a() {
        return this.a.getSharedPreferences(e(), 0).getBoolean("did_query_server", false);
    }

    public void b() {
        SharedPreferences.Editor edit = this.a.getSharedPreferences(e(), 0).edit();
        edit.putBoolean("did_query_server", true);
        edit.commit();
    }

    public String c() {
        return this.b;
    }
}
