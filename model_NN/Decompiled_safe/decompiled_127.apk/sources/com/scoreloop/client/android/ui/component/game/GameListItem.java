package com.scoreloop.client.android.ui.component.game;

import android.graphics.drawable.Drawable;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.ui.component.base.ComponentActivity;
import com.scoreloop.client.android.ui.component.base.StandardListItem;
import com.skyd.bestpuzzle.n1663.R;

public class GameListItem extends StandardListItem<Game> {
    public GameListItem(ComponentActivity context, Drawable drawable, Game game) {
        super(context, drawable, game.getName(), game.getPublisherName(), game);
    }

    /* access modifiers changed from: protected */
    public String getImageUrl() {
        return ((Game) getTarget()).getImageUrl();
    }

    public int getType() {
        return 12;
    }

    public Drawable getDrawable() {
        return getContext().getResources().getDrawable(R.drawable.sl_icon_games);
    }
}
