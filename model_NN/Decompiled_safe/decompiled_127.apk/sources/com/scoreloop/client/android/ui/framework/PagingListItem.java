package com.scoreloop.client.android.ui.framework;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.skyd.bestpuzzle.n1663.R;

public class PagingListItem extends BaseListItem {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$scoreloop$client$android$ui$framework$PagingDirection;
    private final PagingDirection _pagingDirection;

    static /* synthetic */ int[] $SWITCH_TABLE$com$scoreloop$client$android$ui$framework$PagingDirection() {
        int[] iArr = $SWITCH_TABLE$com$scoreloop$client$android$ui$framework$PagingDirection;
        if (iArr == null) {
            iArr = new int[PagingDirection.values().length];
            try {
                iArr[PagingDirection.PAGE_TO_NEXT.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[PagingDirection.PAGE_TO_OWN.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[PagingDirection.PAGE_TO_PREV.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[PagingDirection.PAGE_TO_RECENT.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[PagingDirection.PAGE_TO_TOP.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            $SWITCH_TABLE$com$scoreloop$client$android$ui$framework$PagingDirection = iArr;
        }
        return iArr;
    }

    private static Drawable getDrawable(Context context, PagingDirection pagingDirection) {
        Resources resources = context.getResources();
        switch ($SWITCH_TABLE$com$scoreloop$client$android$ui$framework$PagingDirection()[pagingDirection.ordinal()]) {
            case 1:
                return resources.getDrawable(R.drawable.sl_icon_next);
            case 2:
            case 4:
            default:
                return null;
            case 3:
                return resources.getDrawable(R.drawable.sl_icon_previous);
            case 5:
                return resources.getDrawable(R.drawable.sl_icon_top);
        }
    }

    public static String getTitle(Context context, PagingDirection pagingDirection) {
        switch ($SWITCH_TABLE$com$scoreloop$client$android$ui$framework$PagingDirection()[pagingDirection.ordinal()]) {
            case 1:
                return context.getString(R.string.sl_next);
            case 2:
            case 4:
            default:
                return null;
            case 3:
                return context.getString(R.string.sl_previous);
            case 5:
                return context.getString(R.string.sl_top);
        }
    }

    public PagingListItem(Context context, PagingDirection pagingDirection) {
        super(context, getDrawable(context, pagingDirection), getTitle(context, pagingDirection));
        this._pagingDirection = pagingDirection;
    }

    public PagingDirection getPagingDirection() {
        return this._pagingDirection;
    }

    public int getType() {
        return 0;
    }

    public View getView(View view, ViewGroup parent) {
        if (view == null) {
            view = getLayoutInflater().inflate((int) R.layout.sl_list_item_icon_title_small, (ViewGroup) null);
        }
        ((ImageView) view.findViewById(R.id.sl_icon)).setImageDrawable(getDrawable());
        ((TextView) view.findViewById(R.id.sl_title)).setText(getTitle());
        return view;
    }

    public boolean isEnabled() {
        return true;
    }
}
