package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.PublishedFor__1_0_0;

public interface UserControllerObserver extends RequestControllerObserver {
    @PublishedFor__1_0_0
    void userControllerDidFailOnEmailAlreadyTaken(UserController userController);

    @PublishedFor__1_0_0
    void userControllerDidFailOnInvalidEmailFormat(UserController userController);

    @PublishedFor__1_0_0
    void userControllerDidFailOnUsernameAlreadyTaken(UserController userController);
}
