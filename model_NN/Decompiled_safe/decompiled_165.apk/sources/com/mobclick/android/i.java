package com.mobclick.android;

import android.content.Context;
import android.util.Log;

final class i extends Thread {
    private static final Object a = new Object();
    private Context b;
    private int c;
    private String d;
    private String e;
    private String f;
    private String g;
    private int h;

    i(Context context, int i) {
        this.b = context;
        this.c = i;
    }

    i(Context context, String str, int i) {
        this.b = context;
        this.c = i;
        this.d = str;
    }

    i(Context context, String str, String str2, int i) {
        this.b = context;
        this.c = i;
        this.d = str;
        this.e = str2;
    }

    i(Context context, String str, String str2, String str3, int i, int i2) {
        this.b = context;
        this.d = str;
        this.f = str2;
        this.g = str3;
        this.h = i;
        this.c = i2;
    }

    public void run() {
        try {
            synchronized (a) {
                if (this.c == 0) {
                    try {
                        if (this.b == null) {
                            Log.e("MobclickAgent", "unexpected null context");
                            return;
                        }
                        MobclickAgent.a.e(this.b);
                    } catch (Exception e2) {
                        Log.e("MobclickAgent", "Exception occurred in Mobclick.onRause(). ");
                    }
                } else if (this.c == 1) {
                    MobclickAgent.a.a(this.b, this.d, this.e);
                } else if (this.c == 2) {
                    MobclickAgent.a.b(this.b, this.d);
                } else if (this.c == 3) {
                    MobclickAgent.a.a(this.b, this.d, this.f, this.g, this.h);
                }
            }
        } catch (Exception e3) {
            Log.e("MobclickAgent", "Exception occurred when recording usage.");
        }
    }
}
