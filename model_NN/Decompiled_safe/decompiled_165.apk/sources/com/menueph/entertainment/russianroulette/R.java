package com.menueph.entertainment.russianroulette;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int btn_questionmarkred_36x36 = 2130837504;
        public static final int ic_menu_notifications = 2130837505;
        public static final int icon = 2130837506;
        public static final int rriclauncher = 2130837507;
        public static final int rrimgbullet = 2130837508;
        public static final int rrscrempty = 2130837509;
        public static final int rrscrnormal = 2130837510;
        public static final int rrscrreloadspin1 = 2130837511;
        public static final int rrscrreloadspin2 = 2130837512;
        public static final int rrscrreloadspin3 = 2130837513;
    }

    public static final class id {
        public static final int SurfaceView01 = 2131099648;
        public static final int button1 = 2131099649;
        public static final int imageView1 = 2131099650;
        public static final int imageView2 = 2131099651;
    }

    public static final class layout {
        public static final int main = 2130903040;
    }

    public static final class raw {
        public static final int click = 2130968576;
        public static final int reload = 2130968577;
        public static final int rollclose = 2130968578;
        public static final int shoot = 2130968579;
        public static final int spinchamber = 2130968580;
        public static final int unload = 2130968581;
    }

    public static final class string {
        public static final int app_name = 2131034113;
        public static final int fdbk_msg = 2131034115;
        public static final int fdbk_title = 2131034114;
        public static final int hello = 2131034112;
        public static final int help_msg = 2131034117;
        public static final int help_title = 2131034116;
    }
}
