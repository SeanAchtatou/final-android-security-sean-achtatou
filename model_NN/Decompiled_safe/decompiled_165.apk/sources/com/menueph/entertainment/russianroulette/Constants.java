package com.menueph.entertainment.russianroulette;

public class Constants {
    public static final int SFX_CHAMBER = 4;
    public static final int SFX_RELOAD = 0;
    public static final int SFX_SHOT = 1;
    public static final int SFX_SHOT_FAIL = 2;
    public static final int SFX_SPIN = 3;
    public static final int STATE_FIRE = 8;
    public static final int STATE_FIRED = 9;
    public static final int STATE_NORMAL = 0;
    public static final int STATE_NORMAL_WITH_BULLET = 3;
    public static final int STATE_READY_TO_FIRE = 7;
    public static final int STATE_RELOAD_NO_BULLET = 1;
    public static final int STATE_RELOAD_WITH_BULLET = 2;
    public static final int STATE_SPIN = 10;
    public static final int STATE_SPIN1 = 4;
    public static final int STATE_SPIN2 = 5;
    public static final int STATE_SPIN3 = 6;
    public static final int STATE_SPIN_DONE = 12;
    public static final int STATE_SPIN_WAIT = 11;
}
