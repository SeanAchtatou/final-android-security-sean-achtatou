package com.menueph.entertainment.russianroulette;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class Panel extends SurfaceView implements SurfaceHolder.Callback {
    private CanvasThread canvasthread = new CanvasThread(getHolder(), this);
    private Bitmap m_currIMG;
    private Bitmap m_gunEmptyBMP;
    private Bitmap m_gunNormalBMP;
    private Bitmap m_gunSpin1;
    private Bitmap m_gunSpin2;
    private Bitmap m_gunSpin3;

    public Panel(Context context, AttributeSet attrs) {
        super(context, attrs);
        Log.i("STARTED HERE", "1");
        getHolder().addCallback(this);
        setFocusable(true);
        this.m_gunNormalBMP = BitmapFactory.decodeResource(getResources(), R.drawable.rrscrnormal);
        this.m_gunEmptyBMP = BitmapFactory.decodeResource(getResources(), R.drawable.rrscrempty);
        this.m_gunSpin1 = BitmapFactory.decodeResource(getResources(), R.drawable.rrscrreloadspin1);
        this.m_gunSpin2 = BitmapFactory.decodeResource(getResources(), R.drawable.rrscrreloadspin2);
        this.m_gunSpin3 = BitmapFactory.decodeResource(getResources(), R.drawable.rrscrreloadspin3);
        this.m_currIMG = this.m_gunNormalBMP;
    }

    private Bitmap ChangeGunByState() {
        Bitmap tmp = this.m_currIMG;
        switch (main.state) {
            case 0:
                this.m_currIMG = this.m_gunNormalBMP;
                break;
            case 1:
                this.m_currIMG = this.m_gunNormalBMP;
                break;
            case 2:
                this.m_currIMG = this.m_gunEmptyBMP;
                break;
            case 3:
                this.m_currIMG = this.m_gunSpin1;
                break;
            case Constants.STATE_READY_TO_FIRE:
                this.m_currIMG = this.m_gunSpin3;
                break;
            case Constants.STATE_FIRE:
                this.m_currIMG = this.m_gunNormalBMP;
                break;
            case Constants.STATE_FIRED:
                this.m_currIMG = this.m_gunNormalBMP;
                break;
            case Constants.STATE_SPIN:
                switch (main.spinState) {
                    case 4:
                        this.m_currIMG = this.m_gunSpin1;
                        break;
                    case 5:
                        this.m_currIMG = this.m_gunSpin2;
                        break;
                    case Constants.STATE_SPIN3:
                        this.m_currIMG = this.m_gunSpin3;
                        break;
                }
        }
        return tmp;
    }

    public void onDraw(Canvas canvas) {
        if (canvas != null) {
            canvas.drawColor(-65281);
            Rect rct = new Rect();
            rct.set(0, 0, getWidth(), getHeight());
            canvas.drawBitmap(ChangeGunByState(), (Rect) null, rct, (Paint) null);
        }
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }

    public void surfaceCreated(SurfaceHolder holder) {
        if (this.canvasthread == null) {
            Log.i("LOG", "THERE IS NO CANVAS THREAD CREATING CANVAS");
            this.canvasthread = new CanvasThread(getHolder(), this);
            this.canvasthread.setRunning(true);
            this.canvasthread.start();
            return;
        }
        Log.i("LOG", "THERE IS A CANVAS CREATED");
        this.canvasthread.setRunning(true);
        this.canvasthread.start();
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        boolean retry = true;
        this.canvasthread.setRunning(false);
        while (retry) {
            try {
                this.canvasthread.join();
                retry = false;
            } catch (InterruptedException e) {
            }
        }
        if (this.canvasthread != null) {
            CanvasThread dummy = this.canvasthread;
            this.canvasthread = null;
            dummy.interrupt();
        }
    }
}
