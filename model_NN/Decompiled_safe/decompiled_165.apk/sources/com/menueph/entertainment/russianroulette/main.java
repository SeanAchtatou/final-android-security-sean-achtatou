package com.menueph.entertainment.russianroulette;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import com.mobclick.android.MobclickAgent;
import java.util.Timer;
import java.util.TimerTask;

public class main extends Activity {
    public static int spinState;
    public static int state;
    private SensorEventListener accelerationListener = new SensorEventListener() {
        public void onAccuracyChanged(Sensor sensor, int acc) {
        }

        public void onSensorChanged(SensorEvent event) {
            switch (main.state) {
                case 1:
                    if (event.values[0] > 11.0f) {
                        main.this.m_soundManager.playSound(4);
                        main.state = 2;
                        return;
                    }
                    return;
                case Constants.STATE_READY_TO_FIRE:
                    if (event.values[0] < -11.0f) {
                        main.this.m_chamberBullet = (int) (Math.random() * 6.0d);
                        Log.i("CHAMBER", Integer.toString(main.this.m_chamberBullet));
                        main.this.m_soundManager.playSound(4);
                        main.state = 8;
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    };
    /* access modifiers changed from: private */
    public int m_chamberBullet;
    private FixedUpdate m_fixedUpdate = new FixedUpdate();
    /* access modifiers changed from: private */
    public float m_recordX;
    private Sensor m_sensor;
    private SensorManager m_sensorManager;
    /* access modifiers changed from: private */
    public SoundManager m_soundManager;
    /* access modifiers changed from: private */
    public int m_spinCount;
    /* access modifiers changed from: private */
    public View mainCanvas;

    /* access modifiers changed from: private */
    public void Spin() {
        Log.i("SPIN", Integer.toString(spinState));
        this.m_spinCount++;
        switch (spinState) {
            case 4:
                spinState = 5;
                return;
            case 5:
                spinState = 6;
                return;
            case Constants.STATE_SPIN3:
                if (this.m_spinCount >= 15) {
                    state = 7;
                }
                spinState = 4;
                return;
            default:
                return;
        }
    }

    class FixedUpdate extends TimerTask {
        FixedUpdate() {
        }

        public void run() {
            switch (main.state) {
                case Constants.STATE_SPIN:
                    main.this.Spin();
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        state = 1;
        spinState = 4;
        this.m_chamberBullet = 0;
        this.m_soundManager = new SoundManager();
        this.m_soundManager.initSounds(getBaseContext());
        this.m_soundManager.addSound(1, R.raw.shoot);
        this.m_soundManager.addSound(2, R.raw.click);
        this.m_soundManager.addSound(0, R.raw.reload);
        this.m_soundManager.addSound(3, R.raw.spinchamber);
        this.m_soundManager.addSound(4, R.raw.unload);
        new Timer().scheduleAtFixedRate(this.m_fixedUpdate, 0, 100);
        this.m_sensorManager = (SensorManager) getSystemService("sensor");
        this.m_sensor = this.m_sensorManager.getSensorList(1).get(0);
        this.mainCanvas = findViewById(R.id.SurfaceView01);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.m_sensorManager.registerListener(this.accelerationListener, this.m_sensor, 1);
        MobclickAgent.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        this.m_sensorManager.unregisterListener(this.accelerationListener);
        super.onStop();
    }

    public void LoadBullet(View v) {
        switch (state) {
            case 2:
                this.m_soundManager.playSound(0);
                this.mainCanvas.setOnTouchListener(new View.OnTouchListener() {
                    public boolean onTouch(View v, MotionEvent e) {
                        Log.i("ONTOUCH", Float.toString(e.getX()));
                        switch (e.getAction()) {
                            case 0:
                                main.this.m_recordX = e.getX();
                                return true;
                            case 1:
                                Log.i(Float.toString(main.this.m_recordX), Float.toString(e.getX()));
                                if (Math.abs(main.this.m_recordX - e.getX()) <= 3.0f) {
                                    return true;
                                }
                                main.this.m_soundManager.playSound(3);
                                main.this.m_spinCount = 0;
                                main.this.mainCanvas.setOnTouchListener(null);
                                main.state = 10;
                                return true;
                            case 2:
                            default:
                                return true;
                        }
                    }
                });
                state = 3;
                return;
            default:
                return;
        }
    }

    public void ChangeBG(View v) {
        Log.i("STATE", Integer.toString(state));
        switch (state) {
            case 0:
                state = 1;
                return;
            case 2:
            default:
                return;
            case Constants.STATE_FIRE:
                if (this.m_chamberBullet != 0) {
                    this.m_soundManager.playSound(2);
                } else {
                    this.m_soundManager.playSound(1);
                    state = 1;
                }
                this.m_chamberBullet--;
                return;
        }
    }

    public void launchFeedBackMsg(View target) {
        new AlertDialog.Builder(this).setTitle((int) R.string.fdbk_title).setMessage((int) R.string.fdbk_msg).setPositiveButton("Send Feedback email", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                main.this.startActivity(new Intent("android.intent.action.SENDTO", Uri.fromParts("mailto", "russian@mymenue.com", null)));
            }
        }).setNegativeButton("Close", (DialogInterface.OnClickListener) null).show();
    }

    public void launchInstructionsMsg(View target) {
        new AlertDialog.Builder(this).setTitle((int) R.string.help_title).setMessage((int) R.string.help_msg).setNegativeButton("Close", (DialogInterface.OnClickListener) null).show();
    }
}
