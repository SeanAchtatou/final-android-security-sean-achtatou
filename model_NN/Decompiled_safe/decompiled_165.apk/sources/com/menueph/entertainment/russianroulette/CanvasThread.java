package com.menueph.entertainment.russianroulette;

import android.graphics.Canvas;
import android.view.SurfaceHolder;

public class CanvasThread extends Thread {
    private Panel _panel;
    private boolean _run = false;
    private SurfaceHolder _surfaceHolder;

    public CanvasThread(SurfaceHolder surfaceHolder, Panel panel) {
        this._surfaceHolder = surfaceHolder;
        this._panel = panel;
    }

    public void setRunning(boolean run) {
        this._run = run;
    }

    public boolean isRunning() {
        return this._run;
    }

    public void run() {
        while (this._run) {
            Canvas c = null;
            try {
                c = this._surfaceHolder.lockCanvas(null);
                synchronized (this._surfaceHolder) {
                    this._panel.onDraw(c);
                }
            } finally {
                if (c != null) {
                    this._surfaceHolder.unlockCanvasAndPost(c);
                }
            }
        }
    }
}
