Group: Mate in 1 move
A1: Mate in 1 move
    kc6 qc5 kc8
    qc5-f8#
A2: Mate in 1 move
    ke3 qf4 rd2 bb3 nc6 ke6 ra3 bc1 bc4 ne7
    nc6-d8#
A3: Mate in 1 move
    kd4 ra8 rc1 bg1 bh1 nc7 a4 kb6 nb4 f6
    a4-a5#
A4: Mate in 1 move
    kb5 ra5 ba1 ng3 e3 f5 ke5 bc3 nc6 f6
    kb5-c6#
A5: Mate in 1 move
    kg6 qc1 ba8 bh8 ne6 ng4 e2 ke4 rc6 bh3 nc7
    qc1-c4#
A6: Mate in 1 move
    kg1 ra6 bb8 be4 nc5 nd2 c3 e3 g3 ke5 rg7 bd4 be2 nb3 nc7 f4
    ra6-e6#
A7: Mate in 1 move
    kc8 qf8 re1 rh6 ne7 nf6 kd6
    nf6-d7#
A8: Mate in 1 move
    kg6 qe8 ra5 rh5 bc5 bg2 nd2 e3 f5 ke5 bd3 bf8 nc2 ne6 c4 d4 f4 g7
    bc5-f8#
A9: Mate in 1 move
    kg8 qa1 rb3 rd5 bh1 ne2 kh2 bb6 bc4
    qa1-h8#
A10: Mate in 1 move
    ke1 rb6 rf8 bb2 bg2 nd4 ng5 ke5 ra3 bc8
    nd4-b3#
A11: Mate in 1 move 
    kh2 qg6 ra1 rh8 nb5 nd7 ka8 qa2 rg8 nc4
    qg6-g2#
A12: Mate in 1 move
    kh8 qd1 ra6 ra8 ba4 ng8 g7 kf7 qh3 nh7
    ba4-e8#
A13: Mate in 1 move
    ke3 qe5 a7 b7 c7 d7 e7 f7 g7 h7 ra8 rh8 rg8 ke1
    qe5-a1#
A14: Mate in 1 move
    ke3 qe5 a7 b7 c7 d7 e7 f7 g7 h7 ra8 rh8 rg8 ke1
    qe5-a1#

Group: Mate in 2 moves
B1: Mate in 2 moves
    kh6 ne7 g5 kh8 bg8 h7
    g5-g6 h7xg6 ne7xg6#
    g5-g6 bg8-f7 g6-g7#
B2: Mate in 2 moves
    kg2 qc2 rf1 ne7 a2 b2 f2 g3 kh8 qb8 rd8 rf8 a7 b7 f7 g7 h7
    qc2xh7 kh8xh7 rf1-h1#
B3: Mate in 2 moves
    kc5 bg2 nd7 ka8 ne4 f5
    kc5-b6 f5-f4 bg2xe4
B4: Mate in 2 moves
    ka8 rd1 bd2 kd8 rf7 bc8 c6
    bd2-g5+ kd8-e8 rd1-d8#
    bd2-g5+ kd8-c7 bg5-d8#
B5: Mate in 2 moves
    ke1 qe2 ra1 bc1 bg2 nh8 a2 b2 c2 g3 h2 kd8 qf5 ra8 bc8 bd4 a7 b7 c7 d7 f7 g7 h7
    bc1-g5+ qf5xg5 nh8xf7#
    bc1-g5+ qf5-f6 nh8xf7#
    bc1-g5+ bd4-f6 nh8xf7#
    bc1-g5+  f7-f6 nh8xf7#
B6: Mate in 2 moves
    kh1 qe6 ba3 kh7 rh8 bg8 nf7
    ba3-f8 nf7-g5 qe6-h6#

Group: Mate in 3 moves
C1: Mate in 3 moves
    kg1 qf4 rd1 rf1 ne4 a2 g2 h2 kg8 qg4 ra8 rf8 bc8 a7 f7 g7 h7
    qf4xf7+ rf8xf7 rd1-d8+ rf7-f8 rd8xf8#
    qf4xf7+ rf8xf7 rd1-d8+ rf7-f8 rf1xf8#
C2: Mate in 3 moves
    kg1 qc1 rd1 rf7 ne5 nf6 a2 b2 g2 h2 kh8 qb6 ra8 re8 bf8 nd4 a7 b5 c6 g7 h7
    ne5-g6+ h7xg6 qc1-h6+ g7xh6 rf7-h7#
C3: Mate in 3 moves
    kd4 rc1 ra8 bg1 bh1 nc7 a4 kb6 nb4 f6
    a4-a5
C4: Mate in 3 moves
    kb5 ra5 ba1 ng3 e3 f5 ke5 bc3 nc6 f6
    kb5-c6
C5: Mate in 3 moves
    kg6 qc1 ba8 bh8 ne6 ng4 e2 ke4 rc6 bh3 nc7
    qc1-c4
C6: Mate in 3 moves
    kg1 ra6 bb8 be4 nc5 nd2 c3 e3 g3 ke5 rg7 bd4 be2 nb3 nc7 f4
    ra6-e6

Group: Mate in 4 moves
D1: Mate in 4 moves
    ke3 qe5 a7 b7 c7 d7 e7 f7 g7 h7 ra8 rh8 rg8 ke1
    qe5-a1
D2: Mate in 4 moves
    kc6 qc5 kc8
    qc5-f8
D3: Mate in 4 moves
    ke3 qf4 rd2 bb3 nc6 Ke6 ra3 bc1 bc4 ne7
    nc6-d8
D4: Mate in 4 moves
    kd4 rc1 ra8 bg1 bh1 nc7 a4 kb6 nb4 f6
    a4-a5
D5: Mate in 4 moves
    kb5 ra5 ba1 ng3 e3 f5 ke5 bc3 nc6 f6
    kb5-c6
D6: Mate in 4 moves
    kg6 qc1 ba8 bh8 ne6 ng4 e2 ke4 rc6 bh3 nc7
    qc1-c4
D7: Mate in 4 moves
    kg1 ra6 bb8 be4 nc5 nd2 c3 e3 g3 ke5 rg7 bd4 be2 nb3 nc7 f4
    ra6-e6

Group: Make best move
E1: Make the best move
    kh1 qf5 ra6 h3 kh5 qe3 h6 g5 g3
    qe3-e1
E2: Make the best move
    kh1 qf5 ra6 h3 kh5 qe2 h6 g5
    qe2-e5
E3: Make the best move
    ka4 qd7 a3 b4 kb6 qc4 rh3 a6
    qd7-d4
E4: Make the best move
    kh1 qf5 ra6 h3 kh5 qe3 h6 g5 g3
    qe3-e1
E5: Make the best move
    kh1 qf5 ra6 h3 kh5 qe2 h6 g5
    qe2-e5
E6: Make the best move
    ka4 qd7 a3 b4 kb6 qc4 rh3 a6
    qd7-d4
F7: Make the best move (errors!)
    kg1 re1 qc4 f2 g2 h3 kg8 qd5 rd8 re8 f7 g7
    re1xe8 kg8-h7 qc4-h4+ kh7-g6 qh4-d8
    re1xe8 rd8xe8 qc4xd5
F11: Make the best move
    kh3 qa8 qe4 ng3 g2 h2 kh7 qe1 qf6 rc3 f7 g6 h5
    qa8-g8+ kh7xg8 qe4-e8+ kg8-h7 qe8-g8+ kh7-h6 qg8-h7+ k6xh7

Group: Make a draw
F1: Make a draw
    kh1 qf5 ra6 h3 kh5 qe3 h6 g5 g3
    qe3-e1
F2: Make a draw
    kh1 qf5 ra6 h3 kh5 qe2 h6 g5
    qe2-e5
F3: Make a draw
    ka4 qd7 a3 b4 kb6 qc4 rh3 a6
    qd7-d4
F4: Make a draw
    kh1 qf5 ra6 h3 kh5 qe3 h6 g5 g3
    qe3-e1
F5: Make a draw
    kh1 qf5 ra6 h3 kh5 qe2 h6 g5
    qe2-e5
F6: Make a draw
    ka4 qd7 a3 b4 kb6 qc4 rh3 a6
    qd7-d4
F12: Make a draw
    kg1 rg7 kh3 re8 g2 g4
    rg7-h7+ kh3-g3 rh7-e7 re8-a8 re7-a7

Group: Cooperative mate
G1: Cooperative mate
    kb5 ra4 rd2 nd1 nd7 bg2 b6 f5 kd4 qd3 rb3 re2 ba3 bf1 nb2 nb4 b7 c4
    ... re2-e5+ nd7-c5 nb4-d5 nc5-b3#
G2: Cooperative mate
    kh3 qg3 ka4 qb5 rb4 rd3 bd4 nd1 nd5 b2 c4 f4
    ... nd5-e3 qg3-g8 c4-c3 qg8-a2#
G3: Cooperative mate
    ka6 qb6 kh5 qg4 rg5 re6 ne4 ne8 be5 c5 f5 g7 
    ... be5-d6 qb6-b2 g7-g6 qb2-h8#
G4: Cooperative mate
    kc5 rc6 bc2 be3 b3 d2 kc1 qg2 rd8 rg4 bb2 bf1 b5 b4 c7 d5 g3 g5
    ... bf1-c4 kc5xb4 bc4-e2 d2-d4#
G5: Cooperative mate
    kc5 rc6 bc2 be3 b3 d2 kc1 qg5 rd8 rg4 bb2 bf1 b5 b4 c7 d5 g3
    ... rg4-c4+ kc5xb5 rc4-f4 d2-d3#
G6: Cooperative mate
    ke1 qg1 ra1 c3 e2 g5 kh3 rf6 rh1 be6 nf5 a4 d5 d6 d7 f7 g6
    ... rh1xg1 ke1-f2 rg1-g4 ra1-h1#
G7: Cooperative mate
    kc3 qb2 bd1 c6 ka4 ba1 a5 b3 c4
    ... ba1xb2 kc3xc4 bb2-a3 bd1xb3#

End: