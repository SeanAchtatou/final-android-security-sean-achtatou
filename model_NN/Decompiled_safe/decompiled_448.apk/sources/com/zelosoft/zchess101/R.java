package com.zelosoft.zchess101;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int bb = 2130837504;
        public static final int bb1 = 2130837505;
        public static final int bb2 = 2130837506;
        public static final int bf = 2130837507;
        public static final int bk = 2130837508;
        public static final int bk1 = 2130837509;
        public static final int bk2 = 2130837510;
        public static final int bk3 = 2130837511;
        public static final int bn = 2130837512;
        public static final int bn1 = 2130837513;
        public static final int bn2 = 2130837514;
        public static final int board = 2130837515;
        public static final int board0 = 2130837516;
        public static final int board1 = 2130837517;
        public static final int board10 = 2130837518;
        public static final int board10down = 2130837519;
        public static final int bp = 2130837520;
        public static final int bp1 = 2130837521;
        public static final int bp2 = 2130837522;
        public static final int bq = 2130837523;
        public static final int bq1 = 2130837524;
        public static final int bq2 = 2130837525;
        public static final int br = 2130837526;
        public static final int br1 = 2130837527;
        public static final int br2 = 2130837528;
        public static final int butt = 2130837529;
        public static final int buttdown = 2130837530;
        public static final int icon = 2130837531;
        public static final int mainyy = 2130837532;
        public static final int nextdown = 2130837533;
        public static final int nextup = 2130837534;
        public static final int prevdown = 2130837535;
        public static final int prevup = 2130837536;
        public static final int qmark = 2130837537;
        public static final int qmark1 = 2130837538;
        public static final int showdown = 2130837539;
        public static final int showdown0 = 2130837540;
        public static final int showup = 2130837541;
        public static final int showup0 = 2130837542;
        public static final int sound0 = 2130837543;
        public static final int swapdown = 2130837544;
        public static final int swapup = 2130837545;
        public static final int tmp = 2130837546;
        public static final int videoup = 2130837547;
        public static final int wb = 2130837548;
        public static final int wb1 = 2130837549;
        public static final int wb2 = 2130837550;
        public static final int wf = 2130837551;
        public static final int wk = 2130837552;
        public static final int wk1 = 2130837553;
        public static final int wk2 = 2130837554;
        public static final int wn = 2130837555;
        public static final int wn1 = 2130837556;
        public static final int wn2 = 2130837557;
        public static final int woodmark = 2130837558;
        public static final int wp = 2130837559;
        public static final int wp1 = 2130837560;
        public static final int wp2 = 2130837561;
        public static final int wq = 2130837562;
        public static final int wq1 = 2130837563;
        public static final int wq2 = 2130837564;
        public static final int wr = 2130837565;
        public static final int wr1 = 2130837566;
        public static final int wr2 = 2130837567;
        public static final int zschess8 = 2130837568;
        public static final int zslogo = 2130837569;
    }

    public static final class id {
        public static final int mainLayout = 2131099648;
    }

    public static final class layout {
        public static final int main = 2130903040;
    }

    public static final class raw {
        public static final int taskset0 = 2130968576;
        public static final int taskset1 = 2130968577;
    }

    public static final class string {
        public static final int app_name = 2131034112;
        public static final int company = 2131034113;
        public static final int ctx_menu_back = 2131034120;
        public static final int ctx_menu_quit = 2131034119;
        public static final int level_butt_basename = 2131034118;
        public static final int main_menu_options = 2131034116;
        public static final int main_menu_resume = 2131034117;
        public static final int main_menu_start = 2131034115;
        public static final int off_txt = 2131034123;
        public static final int ok_txt = 2131034121;
        public static final int on_txt = 2131034122;
        public static final int prefs_anima = 2131034126;
        public static final int prefs_theme = 2131034125;
        public static final int prefs_title = 2131034124;
        public static final int prefs_vibro = 2131034127;
        public static final int presents = 2131034114;
    }
}
