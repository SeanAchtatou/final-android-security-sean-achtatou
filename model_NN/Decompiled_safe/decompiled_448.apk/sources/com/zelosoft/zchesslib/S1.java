package com.zelosoft.zchesslib;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

public class S1 extends Activity {
    static final int FontColor1 = -11193583;
    static final int FontColor2 = -10662531;
    static final int FontColor3 = -16777216;
    static final int ShadColor1 = -8947849;
    static final int ShadColor2 = -1;
    static final int ShadColor3 = -3372988;
    static final int ShadColor4 = -21897;
    static final DisplayMetrics metrics = new DisplayMetrics();
    static int s1Idx = 0;
    static String[] tags;
    static final Typeface tf = Typeface.defaultFromStyle(1);
    private float FSZ;
    private int HSZ;
    private int VSZ;
    private RelativeLayout layout;

    static int getGrIdx() {
        return s1Idx;
    }

    static void setGrIdx(int idx) {
        s1Idx = idx;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        if (savedInstanceState == null) {
            S1Init();
        }
    }

    private void S1Init() {
        this.HSZ = ZChess.getHSize();
        this.VSZ = ZChess.getVSize();
        this.FSZ = ZChess.getFontSize();
        this.layout = new RelativeLayout(this);
        ScrollView scrView = new ScrollView(this);
        scrView.addView(this.layout);
        PrefElem.readPrefs(this);
        PrefElem.setTheme(this, scrView);
        int GAP = this.VSZ / 40;
        int x = this.HSZ / 6;
        int y = GAP * 6;
        int w = this.HSZ - (x * 2);
        int h = (this.VSZ - (GAP * 18)) / 7;
        setHeaderView(this.layout, "Select Level", 1.0f * this.FSZ, x / 2, this.VSZ / 32, w + x, h);
        tags = PzGroup.getPzGroupTags(this);
        for (int idx = 0; idx < 7; idx++) {
            setButtonAttrs(new S1Button(this, idx), x, y, w, h, tags[idx], FontColor3);
            y += h + GAP;
        }
        setContentView(scrView);
    }

    /* access modifiers changed from: package-private */
    public void setHeaderView(RelativeLayout lout, String txt, float fsz, int x, int y, int w, int h) {
        TextView textView = new TextView(this);
        ViewGroup.LayoutParams lpar = ZChess.setLoutParams(textView, x, y, w, h);
        ZChess.setTextAttrs(textView, txt, fsz, FontColor1, ShadColor4);
        textView.setSingleLine(false);
        textView.setMaxEms(this.HSZ);
        textView.setHorizontallyScrolling(false);
        textView.setGravity(17);
        lout.addView(textView, lpar);
    }

    /* access modifiers changed from: package-private */
    public void setTextAttrs(TextView tv, String str, int color) {
        float shadR = (0.07f * this.FSZ) / ZChess.dpiCoeff();
        tv.setText(str);
        tv.setTextSize(this.FSZ);
        tv.setTextColor(color);
        tv.setTypeface(tf);
        tv.setShadowLayer(shadR, shadR * 2.0f, shadR * 2.0f, ShadColor1);
    }

    /* access modifiers changed from: package-private */
    public ViewGroup.LayoutParams setLoutParams(View view, int x, int y, int w, int h) {
        ViewGroup.LayoutParams lpar = ZChess.setLoutParams(view, x, y, w, h);
        view.setLayoutParams(lpar);
        return lpar;
    }

    /* access modifiers changed from: package-private */
    public void setButtonAttrs(S1Button butt, int x, int y, int w, int h, String txt, int color) {
        ZChess.setTextAttrs(butt, txt, this.FSZ * 0.7f, color, ShadColor3);
        butt.setGravity(17);
        this.layout.addView(butt, ZChess.setLoutParams(butt, x, y, w, h));
    }

    /* access modifiers changed from: package-private */
    public void startActivity(Class<? extends Object> clazz) {
        try {
            startActivity(new Intent(this, clazz));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void finish() {
        ZChess.vibrate(this, 20);
        super.finish();
    }
}
