package com.zelosoft.zchesslib;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.View;

/* compiled from: ChessBoard */
class ChN8n extends View {
    private final Board board;
    private final int fldSz;
    private final Paint paint = new Paint();
    private final String str;
    private final int[] x;
    private final int[] y;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, int):void}
     arg types: [int, int, int, int]
     candidates:
      ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, long):void}
      ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, int):void} */
    public ChN8n(Context ctx, Board board2, String str2, int fldSz2, boolean vert) {
        super(ctx);
        int i;
        int i2;
        this.board = board2;
        this.str = str2;
        this.fldSz = fldSz2;
        this.paint.setTextSize((float) (fldSz2 / 2));
        this.paint.setColor(-13426159);
        this.paint.setShadowLayer(2.0f, 2.0f, 2.0f, -21897);
        int len = str2.length();
        this.x = new int[len];
        this.y = new int[len];
        for (int n = 0; n < len; n++) {
            int[] iArr = this.x;
            if (vert) {
                i = 0;
            } else {
                i = n;
            }
            iArr[n] = i * fldSz2;
            int[] iArr2 = this.y;
            if (vert) {
                i2 = n;
            } else {
                i2 = 0;
            }
            iArr2[n] = (i2 * fldSz2) + fldSz2;
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        int n;
        Rect r = new Rect();
        int len = this.str.length();
        for (int i = 0; i < len; i++) {
            if (this.board.revers) {
                n = (len - 1) - i;
            } else {
                n = i;
            }
            String s = this.str.substring(n, n + 1);
            this.paint.getTextBounds(s, 0, 1, r);
            canvas.drawText(s, (float) (this.x[i] + (((this.fldSz - r.right) + r.left) / 2)), (float) (this.y[i] - (((this.fldSz + r.top) - r.bottom) / 2)), this.paint);
        }
    }
}
