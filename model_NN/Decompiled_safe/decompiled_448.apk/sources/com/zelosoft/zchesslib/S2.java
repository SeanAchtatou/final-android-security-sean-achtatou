package com.zelosoft.zchesslib;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class S2 extends Activity {
    static float FSZ = 0.0f;
    static final int FontColor1 = -11193583;
    static final int FontColor2 = -13410509;
    static final int FontColor3 = -16777216;
    static int GAP = 0;
    static int HSZ = 0;
    static final int ShadColor1 = -8947849;
    static final int ShadColor2 = -1;
    static final int ShadColor3 = -3372988;
    static final int ShadColor4 = -21897;
    static int VSZ;
    static int a;
    static int b;
    static int c;
    static int idx0 = 0;
    static final DisplayMetrics metrics = new DisplayMetrics();
    static Puzzle[] puzzles;
    static int pzIdx = 0;
    static final Typeface tf = Typeface.defaultFromStyle(1);
    private RelativeLayout layout;

    static int getPzIdx() {
        return pzIdx;
    }

    static void setPzIdx(int idx) {
        pzIdx = idx;
    }

    static void setIdx0(int val) {
        idx0 = val;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        idx0 = 0;
        if (savedInstanceState == null) {
            S2Init22();
        }
    }

    /* access modifiers changed from: package-private */
    public void S2Init22() {
        HSZ = ZChess.getHSize();
        VSZ = ZChess.getVSize();
        FSZ = ZChess.getFontSize();
        this.layout = new RelativeLayout(this);
        PrefElem.readPrefs(this);
        PrefElem.setTheme(this, this.layout);
        b = HSZ / 20;
        c = VSZ / 16;
        a = (HSZ - (b * 3)) / 2;
        GAP = c;
        int[] xa = {b, a + b + b};
        int grIdx = S1.getGrIdx();
        setHdrView(this.layout, S1.tags[grIdx], 1.0f * FSZ, b * 2, c / 3, HSZ - (b * 4), (c * 3) / 2);
        puzzles = PzGroup.getPuzzles(this, grIdx);
        S2Button.S2ButtonInit(puzzles.length, 4);
        setS2Buttons(grIdx, xa);
        setContentView(this.layout);
    }

    /* access modifiers changed from: package-private */
    public void S2Init23() {
        HSZ = ZChess.getHSize();
        VSZ = ZChess.getVSize();
        FSZ = ZChess.getFontSize();
        this.layout = new RelativeLayout(this);
        PrefElem.readPrefs(this);
        PrefElem.setTheme(this, this.layout);
        c = VSZ / 16;
        a = (VSZ - (c * 2)) / 4;
        b = ((HSZ - (a * 2)) - c) / 3;
        GAP = c / 2;
        int[] xa = {(c / 2) + b, (c / 2) + a + b + b};
        int grIdx = S1.getGrIdx();
        String hdrStr = S1.tags[grIdx];
        setHdrView(this.layout, hdrStr, 1.0f * FSZ, c, VSZ / 44, HSZ - (c * 2), b + c);
        puzzles = PzGroup.getPuzzles(this, grIdx);
        S2Button.S2ButtonInit(puzzles.length, 6);
        setS2Buttons(grIdx, xa);
        setContentView(this.layout);
    }

    /* access modifiers changed from: package-private */
    public void setS2Buttons(int grIdx, int[] xa) {
        int y = (c * 5) / 2;
        int w = a;
        int h = a;
        int i = 0;
        while (i < S2Button.mod && idx0 + i < puzzles.length) {
            int pzIdx2 = idx0 + i;
            int x = xa[pzIdx2 % 2];
            Puzzle pzl = puzzles[pzIdx2];
            setTagView(this.layout, pzl.pzTag, 0.6f * FSZ, pzl, x, y - ((c * 2) / 3), w, (c * 2) / 3, FontColor1);
            setButton(new S2Button(this, pzIdx2, pzl), x, y, w, h);
            if (pzIdx2 % 2 == 1) {
                y += GAP + h;
            }
            i++;
        }
        int d = (b * 2) / 3;
        int w2 = (HSZ - d) / 4;
        ArrowButt arrowButt = new ArrowButt(this, ShadColor2);
        ArrowButt arrowButt2 = new ArrowButt(this, 1);
        setButton(arrowButt, (w2 * 1) + 0, VSZ - (c * 2), w2, (c * 3) / 4);
        setButton(arrowButt2, (w2 * 2) + d, VSZ - (c * 2), w2, (c * 3) / 4);
    }

    /* access modifiers changed from: package-private */
    public void setHdrView(RelativeLayout lout, String txt, float fsz, int x, int y, int w, int h) {
        TextView textView = new TextView(this);
        ViewGroup.LayoutParams lpar = setLoutParams(textView, x, y, w, h);
        ZChess.setTextAttrs(textView, txt, fsz, FontColor1, ShadColor4);
        textView.setSingleLine(false);
        textView.setMaxEms(HSZ);
        textView.setHorizontallyScrolling(false);
        textView.setGravity(17);
        lout.addView(textView, lpar);
    }

    /* access modifiers changed from: package-private */
    public View setTagView(RelativeLayout lout, String txt, float fsz, Puzzle pzl, int x, int y, int w, int h, int color) {
        TagView tagView = new TagView(this, pzl);
        ViewGroup.LayoutParams lpar = setLoutParams(tagView, x, y, w, h);
        ZChess.setTextAttrs(tagView, txt, fsz, color, ShadColor4);
        tagView.setGravity(17);
        lout.addView(tagView, lpar);
        return tagView;
    }

    /* access modifiers changed from: package-private */
    public void setTextAttrs(TextView tv, String str, int color) {
        float shadR = (0.07f * FSZ) / ZChess.dpiCoeff();
        tv.setText(str);
        tv.setTextSize(FSZ);
        tv.setTextColor(color);
        tv.setTypeface(tf);
        tv.setShadowLayer(shadR, shadR * 2.0f, shadR * 2.0f, ShadColor1);
    }

    /* access modifiers changed from: package-private */
    public ViewGroup.LayoutParams setLoutParams(View view, int x, int y, int w, int h) {
        ViewGroup.LayoutParams lpar = ZChess.setLoutParams(view, x, y, w, h);
        view.setLayoutParams(lpar);
        return lpar;
    }

    /* access modifiers changed from: package-private */
    public void setButton(Button butt, int x, int y, int w, int h) {
        ZChess.setTextAttrs(butt, "", (float) (h / 3), FontColor3, ShadColor3);
        butt.setGravity(17);
        this.layout.addView(butt, ZChess.setLoutParams(butt, x, y, w, h));
    }

    public void resetPuzzleButtons(int dir) {
        int mod = S2Button.mod;
        int cnt = (((S2Button.cnt + mod) - 1) / mod) * mod;
        setIdx0(((idx0 + cnt) + (dir * mod)) % cnt);
        S2Init22();
    }

    /* access modifiers changed from: package-private */
    public void startActivity(Class<? extends Object> clazz) {
        try {
            startActivity(new Intent(this, clazz));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void finish() {
        ZChess.vibrate(this, 20);
        super.finish();
    }
}
