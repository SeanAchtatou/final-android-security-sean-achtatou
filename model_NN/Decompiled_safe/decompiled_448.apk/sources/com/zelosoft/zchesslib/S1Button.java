package com.zelosoft.zchesslib;

import android.graphics.Canvas;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import com.zelosoft.zchess101.R;

/* compiled from: S1 */
class S1Button extends Button implements View.OnTouchListener {
    static final int CNT = 7;
    static final int PHASE_MOD = 3;
    static final int ResIdDown = 2130837530;
    static final int ResIdUp = 2130837529;
    private final int idx;
    private int phase = 0;

    public S1Button(S1 par, int idx2) {
        super(par);
        this.idx = idx2;
        setBackgroundResource(R.drawable.butt);
        setOnTouchListener(this);
    }

    public void onDraw(Canvas canvas) {
        invalidate();
        super.onDraw(canvas);
        if (this.phase != 0) {
            this.phase = (this.phase + 1) % PHASE_MOD;
            if (this.phase == 0) {
                setBackgroundResource(R.drawable.butt);
                setPadding(0, 0, 0, 0);
                ((S1) getContext()).startActivity(S2.class);
                setEnabled(true);
            }
        }
    }

    public boolean onTouch(View arg0, MotionEvent event) {
        if (event.getAction() == 0) {
            ZChess.vibrate(getContext(), 20);
            setEnabled(false);
            S1.setGrIdx(this.idx);
            this.phase = 1;
            setBackgroundResource(R.drawable.buttdown);
            setPadding(0, getHeight() / 10, 0, 0);
        }
        return true;
    }
}
