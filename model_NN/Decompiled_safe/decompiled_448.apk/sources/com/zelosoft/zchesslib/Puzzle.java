package com.zelosoft.zchesslib;

import java.util.Vector;

/* compiled from: ChessBoard */
class Puzzle {
    final String desc;
    final String pzTag;
    final String spec;
    final Vector<String> vars;

    Puzzle(String pzTag2, String desc2, String spec2, Vector<String> vars2) {
        this.desc = desc2;
        this.pzTag = pzTag2;
        this.spec = spec2;
        this.vars = vars2;
    }
}
