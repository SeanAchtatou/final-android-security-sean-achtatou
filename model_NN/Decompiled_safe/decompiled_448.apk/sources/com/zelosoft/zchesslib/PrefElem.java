package com.zelosoft.zchesslib;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.view.View;
import com.zelosoft.zchess101.R;

/* compiled from: Prefs */
class PrefElem {
    static final int ANIMA = 1;
    static final int CNT = 3;
    static String Off = null;
    static String On = null;
    static final int THEME = 0;
    static final int VIBRO = 2;
    public static PrefElem[] elems;
    static String[][] prefArrs;
    public String[] choices;
    public String curChoice;
    public int id;
    public String name;
    public String text;

    public PrefElem(Context ctx, int id2, String name2, int strId) {
        String text2 = ctx.getString(strId);
        this.id = id2;
        this.name = name2;
        this.text = text2;
    }

    static void initPrefElems(Context ctx) {
        if (elems == null) {
            On = ctx.getString(R.string.on_txt);
            Off = ctx.getString(R.string.off_txt);
            String[][] locPrefArrs = new String[CNT][];
            String[] strArr = new String[VIBRO];
            strArr[THEME] = On;
            strArr[ANIMA] = Off;
            locPrefArrs[THEME] = strArr;
            String[] strArr2 = new String[VIBRO];
            strArr2[THEME] = On;
            strArr2[ANIMA] = Off;
            locPrefArrs[ANIMA] = strArr2;
            String[] strArr3 = new String[VIBRO];
            strArr3[THEME] = On;
            strArr3[ANIMA] = Off;
            locPrefArrs[VIBRO] = strArr3;
            PrefElem[] locElems = new PrefElem[CNT];
            locElems[THEME] = new PrefElem(ctx, THEME, "ThemeMode", R.string.prefs_theme);
            locElems[ANIMA] = new PrefElem(ctx, ANIMA, "AnimaMode", R.string.prefs_anima);
            locElems[VIBRO] = new PrefElem(ctx, VIBRO, "VibroMode", R.string.prefs_vibro);
            prefArrs = locPrefArrs;
            elems = locElems;
        }
    }

    static String getPref(Context ctx, int prefId) {
        initPrefElems(ctx);
        elems[prefId].readPref(ctx);
        return elems[prefId].curChoice;
    }

    static void readPrefs(Context ctx) {
        initPrefElems(ctx);
        for (int id2 = THEME; id2 < CNT; id2 += ANIMA) {
            elems[id2].readPref(ctx);
        }
    }

    static void writePrefs(Context ctx) {
        for (int id2 = THEME; id2 < CNT; id2 += ANIMA) {
            elems[id2].writePref(ctx);
        }
    }

    static void setTheme(Context ctx, View view) {
        initPrefElems(ctx);
        int idx = elems[THEME].curChoiceIdx();
        Resources rsrc = ctx.getResources();
        if (elems[THEME].curChoice.equals(On)) {
            view.setBackgroundDrawable(rsrc.getDrawable(Theme.images[idx]));
        } else {
            view.setBackgroundColor(Theme.colors[idx]);
        }
    }

    /* access modifiers changed from: package-private */
    public int curChoiceIdx() {
        for (int idx = THEME; idx < this.choices.length; idx += ANIMA) {
            if (this.choices[idx].equals(this.curChoice)) {
                return idx;
            }
        }
        return THEME;
    }

    /* access modifiers changed from: package-private */
    public void setCurChoice(int choice) {
        this.curChoice = this.choices[choice];
    }

    /* access modifiers changed from: package-private */
    public void toggleChoice() {
        setCurChoice((curChoiceIdx() + ANIMA) % this.choices.length);
    }

    /* access modifiers changed from: package-private */
    public void readPref(Context ctx) {
        SharedPreferences prefs = ctx.getSharedPreferences(Prefs.PREFS_NAME, THEME);
        this.choices = prefArrs[this.id];
        this.curChoice = prefs.getString(this.name, this.choices[THEME]);
    }

    /* access modifiers changed from: package-private */
    public void writePref(Context ctx) {
        SharedPreferences.Editor editor = ctx.getSharedPreferences(Prefs.PREFS_NAME, THEME).edit();
        editor.putString(this.name, this.curChoice);
        editor.commit();
    }
}
