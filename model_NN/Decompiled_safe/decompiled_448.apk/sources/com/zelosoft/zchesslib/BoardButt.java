package com.zelosoft.zchesslib;

import android.graphics.Canvas;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import com.zelosoft.zchess101.R;

/* compiled from: ChessBoard */
class BoardButt extends Button implements View.OnTouchListener {
    static final int BUTT_NEXT = 3;
    static final int BUTT_PREV = 2;
    static final int BUTT_SHOW = 0;
    static final int BUTT_SWAP = 1;
    static final int BUTT_VRNT = 4;
    static final int PHASE_MOD = 3;
    private int idx;
    private int phase;
    private int resIdDown = R.drawable.buttdown;
    private int resIdUp = R.drawable.butt;
    private String tag = null;

    public BoardButt(ChessBoard par, int idx2) {
        super(par);
        this.idx = idx2;
        this.phase = BUTT_SHOW;
        if (idx2 == BUTT_SWAP) {
            this.tag = "Swap";
        }
        if (idx2 == 0) {
            this.tag = "Show";
        }
        if (idx2 == BUTT_PREV) {
            this.resIdUp = R.drawable.prevup;
            this.resIdDown = R.drawable.prevdown;
        }
        if (idx2 == 3) {
            this.resIdUp = R.drawable.nextup;
            this.resIdDown = R.drawable.nextdown;
        }
        if (idx2 == BUTT_VRNT) {
            this.tag = "Variant";
        }
        if (this.tag != null) {
            par.setTextAttrs(this, this.tag);
        }
        setBackgroundResource(this.resIdUp);
        setPadding(BUTT_SHOW, (-getHeight()) / 10, BUTT_SHOW, BUTT_SHOW);
        setOnTouchListener(this);
    }

    /* access modifiers changed from: package-private */
    public void buttonAction() {
        ChessBoard par = (ChessBoard) getContext();
        if (this.idx == BUTT_SWAP) {
            par.doSwap();
        }
        if (this.idx == 0) {
            par.toggleSolutionShow();
        }
        if (this.idx == BUTT_PREV) {
            par.doPrevStep();
        }
        if (this.idx == 3) {
            par.doNextStep();
        }
        if (this.idx == BUTT_VRNT) {
            par.doNextVariant();
        }
    }

    public void onDraw(Canvas canvas) {
        invalidate();
        super.onDraw(canvas);
        if (this.phase != 0) {
            this.phase = (this.phase + BUTT_SWAP) % 3;
            if (this.phase == 0) {
                if (this.resIdUp != 0) {
                    setBackgroundResource(this.resIdUp);
                    setPadding(BUTT_SHOW, (-getHeight()) / 10, BUTT_SHOW, BUTT_SHOW);
                }
                buttonAction();
                setEnabled(true);
            }
        }
    }

    public boolean onTouch(View arg0, MotionEvent event) {
        if (event.getAction() == 0) {
            ZChess.vibrate(getContext(), 20);
            setEnabled(false);
            this.phase = BUTT_SWAP;
            if (this.resIdDown != 0) {
                setBackgroundResource(this.resIdDown);
                setPadding(BUTT_SHOW, getHeight() / 10, BUTT_SHOW, BUTT_SHOW);
            }
        }
        return true;
    }
}
