package com.zelosoft.zchesslib;

public class Chess {
    static final int B_B = 3;
    static final int B_F = 0;
    static final int B_K = 6;
    static final int B_N = 2;
    static final int B_P = 1;
    static final int B_Q = 5;
    static final int B_R = 4;
    static final String S12345678 = "12345678";
    static final String Sabcdefgh = "abcdefgh";
    static final int W_B = 11;
    static final int W_F = 8;
    static final int W_K = 14;
    static final int W_N = 10;
    static final int W_P = 9;
    static final int W_Q = 13;
    static final int W_R = 12;
    static final int W_SHIFT = 8;
    private int answer_from = -1;
    private int answer_to = -1;
    private int bKing;
    private final int[] desk = new int[64];
    private String errStr = null;
    private int wKing;
    private boolean white_move;

    private void setErrStr(int i, String s) {
        this.errStr = "Chess:  setting: (ind=" + i + ") '" + s + "'";
    }

    public String getErrStr() {
        return this.errStr;
    }

    Chess(String s) {
        setDesk(s, true);
    }

    public static int getPic(char ch, boolean isWhite) {
        int pic;
        switch (ch) {
            case 'B':
            case 'b':
                pic = W_B;
                break;
            case 'K':
            case 'k':
                pic = W_K;
                break;
            case 'N':
            case 'n':
                pic = W_N;
                break;
            case 'P':
            case 'p':
                pic = W_P;
                break;
            case 'Q':
            case 'q':
                pic = W_Q;
                break;
            case 'R':
            case 'r':
                pic = W_R;
                break;
            default:
                pic = W_P;
                break;
        }
        if (!isWhite) {
            return pic - 8;
        }
        return pic;
    }

    public boolean setDesk(String s, boolean wmove) {
        int qCnt = B_F;
        int rCnt = B_F;
        int bCnt = B_F;
        int nCnt = B_F;
        int pCnt = B_F;
        this.errStr = null;
        this.white_move = wmove;
        this.wKing = -1;
        this.bKing = -1;
        for (int i = B_F; i < this.desk.length; i += B_P) {
            this.desk[i] = B_F;
        }
        int i2 = B_F;
        while (true) {
            if (i2 < s.length()) {
                while (i2 < s.length() && ((ch = s.charAt(i2)) == ' ' || ch == W_P || ch == ',' || ch == W_N)) {
                    i2 += B_P;
                }
                if (s.length() >= i2 + B_N) {
                    char c = s.charAt(i2 + B_P);
                    int pic = W_P;
                    if (c < '1' || c > '8') {
                        switch (s.charAt(i2)) {
                            case 'B':
                            case 'b':
                                bCnt += B_P;
                                pic = W_B;
                                break;
                            case 'K':
                            case 'k':
                                pic = W_K;
                                break;
                            case 'N':
                            case 'n':
                                nCnt += B_P;
                                pic = W_N;
                                break;
                            case 'Q':
                            case 'q':
                                qCnt += B_P;
                                pic = W_Q;
                                break;
                            case 'R':
                            case 'r':
                                rCnt += B_P;
                                pic = W_R;
                                break;
                            default:
                                setErrStr(i2, s);
                                return false;
                        }
                        i2 += B_P;
                        if (s.length() < i2 + B_N) {
                            setErrStr(i2, s);
                            return false;
                        }
                    } else {
                        pCnt += B_P;
                    }
                    int fld = strToFld(s.substring(i2));
                    if (fld < 0) {
                        setErrStr(i2, s);
                        return false;
                    } else if (this.desk[fld] != 0) {
                        setErrStr(i2, s);
                        return false;
                    } else {
                        int i3 = i2 + B_N;
                        if (pic == W_K) {
                            if (this.wKing == -1) {
                                this.wKing = fld;
                            } else if (this.bKing != -1) {
                                setErrStr(i3, s);
                                return false;
                            } else if (!isOkPicsCnt(qCnt, rCnt, bCnt, nCnt, pCnt)) {
                                return false;
                            } else {
                                pCnt = B_F;
                                nCnt = B_F;
                                bCnt = B_F;
                                rCnt = B_F;
                                qCnt = B_F;
                                this.bKing = fld;
                            }
                        }
                        if (this.bKing != -1) {
                            pic -= 8;
                        }
                        this.desk[fld] = pic;
                        i2 = i3 + B_P;
                    }
                } else if (s.length() != i2) {
                    setErrStr(i2, s);
                    return false;
                }
            }
        }
        if (!isOkPicsCnt(qCnt, rCnt, bCnt, nCnt, pCnt)) {
            return false;
        }
        return true;
    }

    private boolean isOkPicsCnt(int qCnt, int rCnt, int bCnt, int nCnt, int pCnt) {
        if (pCnt > 8) {
            this.errStr = "wrong count of pawns";
            return B_F;
        } else if (qCnt > W_P - pCnt) {
            this.errStr = "wrong count of queens";
            return B_F;
        } else if (rCnt > W_N - pCnt) {
            this.errStr = "wrong count of rooks";
            return B_F;
        } else if (bCnt > W_N - pCnt) {
            this.errStr = "wrong count of bishops";
            return B_F;
        } else if (nCnt <= W_N - pCnt) {
            return true;
        } else {
            this.errStr = "wrong count of knights";
            return B_F;
        }
    }

    public static int strToFld(String s) {
        int fld;
        char c = s.charAt(B_F);
        if (c >= 'A' && c <= 'H') {
            fld = c - 'A';
        } else if (c < 'a' || c > 'h') {
            return -1;
        } else {
            fld = c - 'a';
        }
        char c2 = s.charAt(B_P);
        if (c2 < '1' || c2 > '8') {
            return -1;
        }
        return fld + ((c2 - '1') * 8);
    }

    public void setAnswer(String s) {
        this.answer_from = strToFld(s);
        this.answer_to = strToFld(s.substring(B_B));
    }

    public boolean canBeMoved(int n) {
        int pic = this.desk[n];
        if (pic == 0 || this.white_move != isWhite(pic)) {
            return false;
        }
        return true;
    }

    public boolean makeMove(int from, int to) {
        if (from == -1) {
            return B_F;
        }
        if (from != this.answer_from || to != this.answer_to) {
            return B_F;
        }
        this.desk[to] = this.desk[from];
        this.desk[from] = B_F;
        return true;
    }

    public int get_fld(int n) {
        return this.desk[n];
    }

    public String getPices(boolean white) {
        int[] pices = new int[32];
        int lastPic = B_F;
        for (int i = B_F; i < this.desk.length; i += B_P) {
            if (this.desk[i] != 0 && isWhite(this.desk[i]) == white) {
                pices[lastPic] = i;
                lastPic += B_P;
            }
        }
        for (int i2 = B_F; i2 < lastPic; i2 += B_P) {
            for (int j = i2 + B_P; j < lastPic; j += B_P) {
                if (this.desk[pices[i2]] < this.desk[pices[j]]) {
                    int n = pices[i2];
                    pices[i2] = pices[j];
                    pices[j] = n;
                }
            }
        }
        String s = null;
        for (int i3 = B_F; i3 < lastPic; i3 += B_P) {
            String s1 = picToString(pices[i3]);
            if (s == null) {
                s = s1;
            } else {
                s = String.valueOf(s) + ", " + s1;
            }
        }
        if (s == null) {
            return "???";
        }
        return s;
    }

    public String picToString(int fld) {
        String s;
        int pic = this.desk[fld];
        if (pic == 0) {
            return "???";
        }
        switch (pic) {
            case B_N /*2*/:
            case W_N /*10*/:
                s = "N";
                break;
            case B_B /*3*/:
            case W_B /*11*/:
                s = "B";
                break;
            case B_R /*4*/:
            case W_R /*12*/:
                s = "R";
                break;
            case B_Q /*5*/:
            case W_Q /*13*/:
                s = "Q";
                break;
            case B_K /*6*/:
            case W_K /*14*/:
                s = "K";
                break;
            case 7:
            case 8:
            case W_P /*9*/:
            default:
                s = "";
                break;
        }
        int x = fld % 8;
        int y = fld / 8;
        return String.valueOf(s) + Sabcdefgh.substring(x, x + B_P) + S12345678.substring(y, y + B_P);
    }

    public static boolean isWhiteFld(int n) {
        if (((n / 8) + n) % B_N == B_P) {
            return B_P;
        }
        return false;
    }

    public static boolean isWhite(int pic) {
        return pic > 8;
    }
}
