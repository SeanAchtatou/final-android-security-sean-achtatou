package com.zelosoft.zchesslib;

import android.graphics.Canvas;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import com.zelosoft.zchess101.R;

/* compiled from: ZChess */
class ZButton extends Button implements View.OnTouchListener {
    static final int CNT = 3;
    static final int OPTIONS_BUTT = 1;
    static final int PHASE_MOD = 3;
    static final int RESUME_BUTT = 2;
    static final int ResIdDown = 2130837530;
    static final int ResIdUp = 2130837529;
    static final int START_BUTT = 0;
    private final int idx;
    private int phase = 0;

    public ZButton(ZChess par, int idx2) {
        super(par);
        this.idx = idx2;
        setBackgroundResource(R.drawable.butt);
        setOnTouchListener(this);
    }

    /* access modifiers changed from: package-private */
    public void buttonAction() {
        ZChess par = (ZChess) getContext();
        if (this.idx == 0) {
            par.startActivity(S1.class, 0);
        } else if (this.idx == RESUME_BUTT) {
            par.startActivity(ChessBoard.class, OPTIONS_BUTT);
        } else if (this.idx == OPTIONS_BUTT) {
            par.startActivity(Prefs.class, 0);
        }
    }

    public void onDraw(Canvas canvas) {
        invalidate();
        super.onDraw(canvas);
        if (this.phase != 0) {
            this.phase = (this.phase + OPTIONS_BUTT) % 3;
            if (this.phase == 0) {
                setBackgroundResource(R.drawable.butt);
                setPadding(0, 0, 0, 0);
                buttonAction();
                setEnabled(true);
            }
        }
    }

    public boolean onTouch(View arg0, MotionEvent event) {
        if (event.getAction() == 0) {
            ZChess.vibrate(getContext(), 20);
            setEnabled(false);
            this.phase = OPTIONS_BUTT;
            setBackgroundResource(R.drawable.buttdown);
            setPadding(0, getHeight() / 10, 0, 0);
        }
        return true;
    }
}
