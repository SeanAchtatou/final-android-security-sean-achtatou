package com.zelosoft.zchesslib;

import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

/* compiled from: ChessBoard */
class StepView extends TextView implements View.OnTouchListener {
    private final int idx;

    public StepView(ChessBoard par, int idx2) {
        super(par);
        this.idx = idx2;
        setOnTouchListener(this);
    }

    public boolean onTouch(View arg0, MotionEvent event) {
        if (event.getAction() != 0) {
            return true;
        }
        ChessBoard par = (ChessBoard) getContext();
        ZChess.vibrate(par, 20);
        par.doTouchStep(this.idx);
        return true;
    }
}
