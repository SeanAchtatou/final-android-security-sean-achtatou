package com.zelosoft.zchesslib;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.View;
import com.zelosoft.zchess101.R;

/* compiled from: ChessBoard */
class Board extends View {
    private String[] animSteps;
    private Chess chess;
    private final int fldSz;
    private final Paint gPaint = new Paint();
    private int lastTouch = -1;
    private int mateKingIdx = -1;
    private String nSpec;
    private final Paint paint = new Paint();
    private int phase = 0;
    private final Bitmap[] pics2;
    private final Paint rPaint = new Paint();
    private final Rect[] rect;
    boolean revers = false;

    public void setBoard(Puzzle puzzle) {
        this.chess = new Chess(puzzle.spec);
    }

    public void setBoard(String spec) {
        this.chess = new Chess(spec);
    }

    public void setAnimSteps(String[] animSteps2, String nSpec2) {
        this.animSteps = animSteps2;
        this.nSpec = nSpec2;
        this.phase = 0;
    }

    public Board(Context par, int fldSz2, Puzzle puzzle) {
        super(par);
        this.fldSz = fldSz2;
        this.lastTouch = -1;
        this.mateKingIdx = -1;
        setBoard(puzzle.spec);
        this.gPaint.setColor(-16711936);
        this.gPaint.setStyle(Paint.Style.STROKE);
        this.rPaint.setColor(-65536);
        this.rPaint.setStyle(Paint.Style.STROKE);
        this.rect = new Rect[64];
        int shift = (fldSz2 * 3) / 4;
        for (int n = 0; n < this.rect.length; n++) {
            int i = shift + ((n % 8) * fldSz2);
            int j = shift + ((7 - (n / 8)) * fldSz2);
            Rect r = new Rect();
            r.set(i, j, i + fldSz2, j + fldSz2);
            this.rect[n] = r;
        }
        this.pics2 = ChessBoard.setPics2();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        int i;
        invalidate();
        String animFig = null;
        int animFld = -1;
        boolean isWhite = true;
        if (this.animSteps != null) {
            String animCmd = this.animSteps[this.phase / 4];
            animFld = Chess.strToFld(animCmd.substring(1));
            animFig = animCmd.substring(0, 1);
            isWhite = this.animSteps[3].equals("white");
            if (this.phase / 4 == 1) {
                if (isWhite) {
                    isWhite = false;
                } else {
                    isWhite = true;
                }
            }
            this.phase = (this.phase + 1) % (4 * 3);
            if (this.phase == 4 * 2) {
                this.chess = new Chess(this.nSpec);
            }
            if (this.phase == 0) {
                this.animSteps = null;
            }
        }
        canvas.drawBitmap(ChessBoard.getImage(R.drawable.board0), (Rect) null, new Rect(0, 0, (this.fldSz * 19) / 2, (this.fldSz * 19) / 2), (Paint) null);
        int len = this.rect.length;
        for (int i2 = 0; i2 < len; i2++) {
            int pic = this.chess.get_fld(i2);
            if (pic != 0) {
                Rect[] rectArr = this.rect;
                if (this.revers) {
                    i = (((7 - (i2 / 8)) * 8) + 7) - (i2 % 8);
                } else {
                    i = i2;
                }
                Rect rt = rectArr[i];
                Bitmap bm = this.pics2[pic];
                if (animFld == i2) {
                    if (!animFig.equals("Z")) {
                        bm = this.pics2[Chess.getPic(animFig.charAt(0), isWhite)];
                        rt = new Rect(rt.left - 2, rt.top - 2, rt.right + 2, rt.bottom + 2);
                    }
                }
                canvas.drawBitmap(bm, (Rect) null, rt, this.paint);
                if (this.lastTouch == i2) {
                    drawRect(canvas, this.gPaint, rt, 3);
                }
                if (this.mateKingIdx == i2 && this.phase == 0) {
                    drawLine((float) rt.left, (float) rt.top, (float) rt.right, (float) rt.bottom, canvas, this.rPaint);
                    drawLine((float) rt.left, (float) rt.bottom, (float) rt.right, (float) rt.top, canvas, this.rPaint);
                }
            }
        }
    }

    private void drawRect(Canvas canvas, Paint paint2, Rect r, int cnt) {
        if (cnt > r.right - r.left) {
            cnt = r.right - r.left;
        }
        if (cnt > r.bottom - r.top) {
            cnt = r.bottom - r.top;
        }
        for (int i = 0; i < cnt; i++) {
            canvas.drawRect((float) (r.left + i), (float) (r.top + i), (float) (r.right - i), (float) (r.bottom - i), paint2);
        }
    }

    private void drawLine(float x0, float y0, float x1, float y1, Canvas canvas, Paint paint2) {
        for (int i = -1; i < 2; i++) {
            canvas.drawLine(x0 + ((float) i), y0, x1 + ((float) i), y1, paint2);
        }
    }

    public String getErrStr() {
        return this.chess.getErrStr();
    }

    public String getPices(boolean white) {
        return this.chess.getPices(white);
    }

    /* access modifiers changed from: package-private */
    public void setMateToKing(int val) {
        this.mateKingIdx = val;
    }
}
