package com.zelosoft.zchesslib;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.zelosoft.zchess101.R;

public class ZChess extends Activity {
    static final int FontColor1 = -11193583;
    static final int FontColor2 = -7846878;
    static final int FontColor3 = -13426159;
    static final int MENU_QUIT = 0;
    static final int NO_REQUEST_CODE = -1;
    static final int NO_RESULT_CODE = 0;
    static final int PREFS_REQUEST_CODE = 1;
    static final int START_MODE_CONT = 1;
    static final int START_MODE_NEW = 0;
    static final int ShadColor1 = -8947849;
    static final int ShadColor2 = -1;
    static final int ShadColor3 = -3372988;
    static final int ShadColor4 = -21897;
    static final DisplayMetrics metrics = new DisplayMetrics();
    static final Typeface tf = Typeface.defaultFromStyle(1);
    private static Vibrator vibrator = null;
    private static ZChess zChess = null;
    private RelativeLayout layout = null;
    private int startMode = 0;

    static ZChess getZChess() {
        return zChess;
    }

    static int getDpi() {
        return metrics.densityDpi;
    }

    static float dpiCoeff() {
        return 240.0f / ((float) getDpi());
    }

    static void vibrate(Context ctx, int ms) {
        if (PrefElem.getPref(ctx, 2).equals(ctx.getString(R.string.on_txt))) {
            vibrator.vibrate((long) ms);
        }
    }

    static int getHSize() {
        int w = metrics.widthPixels;
        int h = metrics.heightPixels;
        return w < h ? w : h;
    }

    static int getVSize() {
        int w = metrics.widthPixels;
        int h = metrics.heightPixels;
        return w > h ? w : h;
    }

    static float getFontSize() {
        int vsz = getVSize();
        return dpiCoeff() * ((float) (vsz / 30)) * (vsz == 320 ? 1.2f : 1.0f);
    }

    /* access modifiers changed from: package-private */
    public int getGameMode() {
        return this.startMode;
    }

    /* access modifiers changed from: package-private */
    public View getLayout() {
        return this.layout;
    }

    /* access modifiers changed from: package-private */
    public String getVersionName() {
        try {
            return getPackageManager().getPackageInfo(getPackageName(), 128).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            return "v0";
        }
    }

    public boolean isLiteVersion() {
        return getPackageName().toLowerCase().contains("lite");
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        zChess = this;
        requestWindowFeature(1);
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        vibrator = (Vibrator) getSystemService("vibrator");
        setContentView((int) R.layout.main);
        if (savedInstanceState == null) {
            ZChessInit();
        }
    }

    /* access modifiers changed from: package-private */
    public void ZChessInit() {
        int HSZ = getHSize();
        int VSZ = getVSize();
        int GAPX = HSZ / 20;
        int GAPY = VSZ / 28;
        int x = GAPX * 4;
        int y = GAPY * 14;
        int w = HSZ - (x * 2);
        int h = ((VSZ - y) - ((GAPY * 2) * 3)) / 3;
        this.layout = (RelativeLayout) findViewById(R.id.mainLayout);
        PrefElem.readPrefs(this);
        PrefElem.setTheme(this, this.layout);
        setLogo(GAPX * 1, GAPY * 1, GAPX * 6, GAPY * 6);
        String[] tags = {getString(R.string.main_menu_start), getString(R.string.main_menu_options), getString(R.string.main_menu_resume)};
        for (int idx = 0; idx < 2; idx++) {
            setButton(tags[idx], idx, x, y, w, h);
            y += h + GAPY;
        }
    }

    static void setTextAttrs(TextView tv, String str, float fsz, int color, int shadColor) {
        float shadR = (0.07f * fsz) / dpiCoeff();
        tv.setText(str);
        tv.setTextSize(fsz);
        tv.setTextColor(color);
        tv.setTypeface(tf);
        tv.setShadowLayer(shadR, shadR * 2.0f, shadR * 2.0f, shadColor);
    }

    static RelativeLayout.LayoutParams setLoutParams(View view, int x, int y, int w, int h) {
        RelativeLayout.LayoutParams lpar = new RelativeLayout.LayoutParams(w, h);
        lpar.leftMargin = x;
        lpar.topMargin = y;
        view.setLayoutParams(lpar);
        return lpar;
    }

    /* access modifiers changed from: package-private */
    public void setTextView(int x, int y, int w, int h, int color, String str, float fsz) {
        TextView tv = new TextView(this);
        setTextAttrs(tv, str, fsz, color, ShadColor4);
        RelativeLayout.LayoutParams lpar = setLoutParams(tv, x, y, w, h);
        tv.setGravity(17);
        this.layout.addView(tv, lpar);
    }

    /* access modifiers changed from: package-private */
    public void setLogo(int x, int y, int w, int h) {
        int HRZ = getHSize();
        float fsz = getFontSize();
        String Company = getString(R.string.company);
        String Presents = getString(R.string.presents);
        ImageView imageView = new ImageView(this);
        ImageView imageView2 = new ImageView(this);
        ImageView name = new ImageView(this);
        setTextView(((w * 4) / 4) + (x * 1), y + ((h * 3) / 8), (HRZ - w) - (x * 4), h / 2, FontColor3, Company, fsz * 1.4f);
        setTextView((x * 1) + ((w * 1) / 2), y + ((h * 6) / 8), (HRZ - w) - (x * 2), h / 2, FontColor2, Presents, fsz * 1.1f);
        RelativeLayout.LayoutParams lpar1 = setLoutParams(imageView2, x, (y * 3) / 2, w, h);
        RelativeLayout.LayoutParams lpar2 = setLoutParams(name, x, (h * 3) / 2, w * 3, (h * 3) / 5);
        RelativeLayout.LayoutParams lpar3 = setLoutParams(imageView, (x * 2) + ((w * 4) / 4), (h / 6) + y, h / 3, h / 3);
        imageView.setImageResource(R.drawable.bk3);
        imageView2.setImageResource(R.drawable.zslogo);
        name.setImageResource(R.drawable.zschess8);
        this.layout.addView(imageView2, lpar1);
        this.layout.addView(name, lpar2);
        this.layout.addView(imageView, lpar3);
    }

    /* access modifiers changed from: package-private */
    public void setButton(String txt, int idx, int x, int y, int w, int h) {
        ZButton butt = new ZButton(this, idx);
        setTextAttrs(butt, txt, getFontSize(), FontColor3, ShadColor3);
        butt.setGravity(17);
        RelativeLayout.LayoutParams lpar = setLoutParams(butt, x, y, w, h);
        butt.setBackgroundResource(R.drawable.butt);
        butt.setOnTouchListener(butt);
        this.layout.addView(butt, lpar);
    }

    /* access modifiers changed from: package-private */
    public void startActivity(Class<? extends Object> clazz, int mode) {
        int reqCode;
        if (clazz == Prefs.class) {
            reqCode = 1;
        } else {
            reqCode = -1;
        }
        this.startMode = mode;
        try {
            startActivityForResult(new Intent(this, clazz), reqCode);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int reqCode, int resCode, Intent in) {
        if (reqCode == 1 && resCode != 0) {
            this.layout = (RelativeLayout) findViewById(R.id.mainLayout);
            PrefElem.setTheme(this, this.layout);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 0, 0, getString(R.string.ctx_menu_quit));
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 0:
                finish();
                return true;
            default:
                return false;
        }
    }

    public void finish() {
        vibrate(this, 20);
        super.finish();
    }
}
