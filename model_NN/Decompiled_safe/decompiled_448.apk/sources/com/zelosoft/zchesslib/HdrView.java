package com.zelosoft.zchesslib;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.widget.TextView;
import com.zelosoft.zchess101.R;

/* compiled from: ChessBoard */
class HdrView extends TextView {
    public HdrView(ChessBoard par) {
        super(par);
    }

    public void onDraw(Canvas canvas) {
        invalidate();
        super.onDraw(canvas);
        Paint paint = new Paint();
        String val = Prefs.readShPref(getContext(), ChessBoard.getPuzzle().pzTag);
        int w = getWidth();
        int h = getHeight();
        if (val.equals("1")) {
            canvas.drawBitmap(ChessBoard.getImage(R.drawable.woodmark), (Rect) null, new RectF((float) ((w * 0) / 28), 0.0f, (float) ((w * 2) / 28), (float) ((h * 2) / 3)), paint);
        }
    }
}
