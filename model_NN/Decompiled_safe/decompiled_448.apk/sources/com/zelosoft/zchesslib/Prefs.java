package com.zelosoft.zchesslib;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.zelosoft.zchess101.R;

public class Prefs extends Activity implements View.OnClickListener {
    static final int FontColor1 = -16776961;
    static final int FontColor2 = -16777216;
    static final int MENU_BACK = 0;
    static String PREFS_NAME = null;
    static final int ShadColor1 = -8947849;
    static final int ShadColor2 = -1;
    static final int ShadColor3 = -4491469;
    static final int ShadColor4 = -21931;
    static final Typeface tf = Typeface.defaultFromStyle(1);
    private float fsz;
    private int hsz;
    RelativeLayout layout;
    private int vsz;

    static String readShPref(Context ctx, String key) {
        if (PREFS_NAME == null) {
            PREFS_NAME = String.valueOf(ctx.getString(R.string.app_name)) + "Prefs";
        }
        return ctx.getSharedPreferences(PREFS_NAME, MENU_BACK).getString(key, "0");
    }

    static void writeShPref(Context ctx, String key, String val) {
        if (PREFS_NAME == null) {
            PREFS_NAME = String.valueOf(ctx.getString(R.string.app_name)) + "Prefs";
        }
        SharedPreferences.Editor editor = ctx.getSharedPreferences(PREFS_NAME, MENU_BACK).edit();
        editor.putString(key, val);
        editor.commit();
    }

    /* access modifiers changed from: package-private */
    public String getAppName() {
        return getString(R.string.app_name);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        this.hsz = ZChess.getHSize();
        this.vsz = ZChess.getVSize();
        this.fsz = ZChess.getFontSize();
        this.layout = new RelativeLayout(this);
        PREFS_NAME = String.valueOf(getAppName()) + "Prefs";
        PrefElem.readPrefs(this);
        PrefElem.setTheme(this, this.layout);
        setContentView(this.layout);
        int h = this.vsz / 12;
        setHeader(getString(R.string.prefs_title), h);
        int dy = (this.vsz * 2) / 25;
        int y = (dy * 9) / 4;
        for (int idx = 1; idx < 3; idx++) {
            setPrefView(idx, y, h);
            y += dy;
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add((int) MENU_BACK, (int) MENU_BACK, (int) MENU_BACK, getString(R.string.ctx_menu_back));
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case MENU_BACK /*0*/:
                finish();
                return true;
            default:
                return false;
        }
    }

    /* access modifiers changed from: package-private */
    public void setHeader(String str, int h) {
        TextView head = new TextView(this);
        RelativeLayout.LayoutParams lpar = ZChess.setLoutParams(head, this.hsz / 4, this.vsz / 25, this.hsz / 2, h * 2);
        ZChess.setTextAttrs(head, str, this.fsz * 1.2f, FontColor2, ShadColor4);
        this.layout.addView(head, lpar);
    }

    /* access modifiers changed from: package-private */
    public void setPrefView(int idx, int y, int h) {
        RelativeLayout lout = new RelativeLayout(this);
        RelativeLayout.LayoutParams lpar = ZChess.setLoutParams(lout, MENU_BACK, y, this.hsz, h);
        setCheckBox(lout, idx, (this.hsz * 1) / 20, (this.hsz * 2) / 3, h * 2, 3);
        this.layout.addView(lout, lpar);
    }

    /* access modifiers changed from: package-private */
    public void setCheckBox(RelativeLayout lout, int idx, int x, int w, int h, int gravity) {
        PrefElem prefElem = PrefElem.elems[idx];
        CheckBox chkBox = new CheckBox(this);
        RelativeLayout.LayoutParams lpar = ZChess.setLoutParams(chkBox, x, MENU_BACK, w, h);
        ZChess.setTextAttrs(chkBox, prefElem.text, (4.0f * this.fsz) / 5.0f, FontColor2, ShadColor4);
        chkBox.setGravity(gravity);
        chkBox.setPadding(w / 5, h / 30, MENU_BACK, MENU_BACK);
        if (prefElem.curChoice.equals(PrefElem.On)) {
            chkBox.toggle();
        }
        chkBox.setOnClickListener(this);
        lout.addView(chkBox, lpar);
    }

    public void finish() {
        PrefElem.writePrefs(this);
        PrefElem.setTheme(this, this.layout);
        setResult(1);
        super.finish();
    }

    public void onClick(View view) {
        ZChess.vibrate(this, 20);
        String txt = ((CheckBox) view).getText().toString();
        for (int idx = MENU_BACK; idx < PrefElem.elems.length; idx++) {
            PrefElem pref = PrefElem.elems[idx];
            if (txt.equals(pref.text)) {
                pref.toggleChoice();
                return;
            }
        }
    }
}
