package com.zelosoft.zchesslib;

import android.graphics.Canvas;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import com.zelosoft.zchess101.R;

/* compiled from: S2 */
class ArrowButt extends Button implements View.OnTouchListener {
    static final int DIR_NEXT = 1;
    static final int DIR_NONE = 0;
    static final int DIR_PREV = -1;
    static final int PHASE_MOD = 3;
    private final int dir;
    int phase = DIR_NONE;
    int resIdDown;
    int resIdUp;

    public ArrowButt(S2 par, int dir2) {
        super(par);
        this.dir = dir2;
        if (dir2 == DIR_PREV) {
            this.resIdUp = R.drawable.prevup;
            this.resIdDown = R.drawable.prevdown;
        } else {
            this.resIdUp = R.drawable.nextup;
            this.resIdDown = R.drawable.nextdown;
        }
        setBackgroundResource(this.resIdUp);
        setOnTouchListener(this);
    }

    public void onDraw(Canvas canvas) {
        invalidate();
        super.onDraw(canvas);
        if (this.phase != 0) {
            this.phase = (this.phase + DIR_NEXT) % PHASE_MOD;
            if (this.phase == 0) {
                setBackgroundResource(this.resIdUp);
                ((S2) getContext()).resetPuzzleButtons(this.dir);
                setEnabled(true);
            }
        }
    }

    public boolean onTouch(View arg0, MotionEvent event) {
        if (event.getAction() == 0) {
            ZChess.vibrate(getContext(), 20);
            setEnabled(false);
            this.phase = DIR_NEXT;
            setBackgroundResource(this.resIdDown);
        }
        return true;
    }
}
