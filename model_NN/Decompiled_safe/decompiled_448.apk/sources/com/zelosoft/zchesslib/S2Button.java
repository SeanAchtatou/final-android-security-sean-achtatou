package com.zelosoft.zchesslib;

import android.graphics.Canvas;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import com.zelosoft.zchess101.R;

/* compiled from: S2 */
class S2Button extends Button implements View.OnTouchListener {
    static final int PHASE_MOD = 3;
    static final int ResIdDown = 2130837519;
    static final int ResIdUp = 2130837518;
    static int cnt;
    static int mod;
    private final int idx;
    private int phase = 0;
    private final Puzzle puzzle;

    static void S2ButtonInit(int arg1, int arg2) {
        cnt = arg1;
        mod = arg2;
    }

    public S2Button(S2 par, int idx2, Puzzle puzzle2) {
        super(par);
        this.idx = idx2;
        this.puzzle = puzzle2;
        setBackgroundResource(R.drawable.board10);
        setOnTouchListener(this);
    }

    public void onDraw(Canvas canvas) {
        float w = (float) getWidth();
        float w2 = w - (w / 48.0f);
        invalidate();
        super.onDraw(canvas);
        if (this.phase == 0) {
            ChessBoard.drawPuzzle(canvas, this.puzzle.spec, (float) ((int) (0.075f * w2)), (float) ((int) (0.85f * w2)));
            return;
        }
        this.phase = (this.phase + 1) % PHASE_MOD;
        if (this.phase == 0) {
            setBackgroundResource(R.drawable.board10);
            ChessBoard.drawPuzzle(canvas, this.puzzle.spec, (float) ((int) (0.075f * w2)), (float) ((int) (0.85f * w2)));
            ((S2) getContext()).startActivity(ChessBoard.class);
            setEnabled(true);
            return;
        }
        ChessBoard.drawPuzzle(canvas, this.puzzle.spec, (float) ((int) (0.075f * w2)), (float) ((int) (0.85f * w2)));
    }

    public boolean onTouch(View arg0, MotionEvent event) {
        if (event.getAction() == 0) {
            ZChess.vibrate(getContext(), 20);
            setEnabled(false);
            this.phase = 1;
            S2.setPzIdx(this.idx);
            setBackgroundResource(R.drawable.board10down);
        }
        return true;
    }
}
