package com.zelosoft.zchesslib;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.widget.TextView;
import com.zelosoft.zchess101.R;

/* compiled from: S2 */
class TagView extends TextView {
    private final Puzzle pzl;

    public TagView(S2 par, Puzzle pzl2) {
        super(par);
        this.pzl = pzl2;
    }

    public void onDraw(Canvas canvas) {
        invalidate();
        super.onDraw(canvas);
        Paint paint = new Paint();
        String val = Prefs.readShPref(getContext(), this.pzl.pzTag);
        int w = getWidth();
        int h = getHeight();
        if (val.equals("1")) {
            canvas.drawBitmap(ChessBoard.getImage(R.drawable.woodmark), (Rect) null, new RectF((float) ((w * 7) / 24), (float) (h / 4), (float) ((w * 10) / 24), (float) h), paint);
        }
    }
}
