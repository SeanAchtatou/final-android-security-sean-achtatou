package com.zelosoft.zchesslib;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.zelosoft.zchess101.R;

public class ChessBoard extends Activity {
    static final int BKG_HILIGHT = 3342336;
    static final int[] CH_PICS1;
    static final int[] CH_PICS2;
    static final int FontColor1 = -13426159;
    static final int FontColor2 = -7846878;
    static final int FontColor3 = -16777216;
    static int MaxRows = 4;
    static final int SEL_HILIGHT = 2011028787;
    static final int ShadColor1 = -8947849;
    static final int ShadColor2 = -1;
    static final int ShadColor3 = -3372988;
    static final int ShadColor4 = -21897;
    static final int TRG_HILIGHT = 2006515712;
    private static Bitmap[] pics1;
    private static Puzzle puzzle;
    private static PzGroup[] pzGroups;
    static final Typeface tf = Typeface.defaultFromStyle(1);
    private int FSZ = 0;
    private int GAPX = 0;
    private int GAPY = 0;
    private Board board;
    private ChN8n[] chN8n;
    private int fldSz;
    private int grIdx = 0;
    private RelativeLayout layout;
    private DisplayMetrics metrics;
    private TextView[] numViews;
    private int pzIdx = 0;
    private int relRow = 0;
    private int stepId = 0;
    private TextView[] stepViews;
    private String[] steps;
    private TextView text;
    private int varId = 0;
    private TextView varView;
    private RelativeLayout varsLL;

    static {
        int[] iArr = new int[16];
        iArr[1] = R.drawable.bp1;
        iArr[2] = R.drawable.bn1;
        iArr[3] = R.drawable.bb1;
        iArr[4] = R.drawable.br1;
        iArr[5] = R.drawable.bq1;
        iArr[6] = R.drawable.bk1;
        iArr[9] = R.drawable.wp1;
        iArr[10] = R.drawable.wn1;
        iArr[11] = R.drawable.wb1;
        iArr[12] = R.drawable.wr1;
        iArr[13] = R.drawable.wq1;
        iArr[14] = R.drawable.wk1;
        CH_PICS1 = iArr;
        int[] iArr2 = new int[16];
        iArr2[1] = R.drawable.bp2;
        iArr2[2] = R.drawable.bn2;
        iArr2[3] = R.drawable.bb2;
        iArr2[4] = R.drawable.br2;
        iArr2[5] = R.drawable.bq2;
        iArr2[6] = R.drawable.bk2;
        iArr2[9] = R.drawable.wp2;
        iArr2[10] = R.drawable.wn2;
        iArr2[11] = R.drawable.wb2;
        iArr2[12] = R.drawable.wr2;
        iArr2[13] = R.drawable.wq2;
        iArr2[14] = R.drawable.wk2;
        CH_PICS2 = iArr2;
    }

    static Bitmap getImage(int id) {
        return BitmapFactory.decodeResource(ZChess.getZChess().getResources(), id);
    }

    static Bitmap[] setPics1() {
        Bitmap[] pics = new Bitmap[15];
        for (int i = 1; i < 7; i++) {
            int j = i + 8;
            pics[i] = getImage(CH_PICS1[i]);
            pics[j] = getImage(CH_PICS1[j]);
        }
        return pics;
    }

    static Bitmap[] setPics2() {
        Bitmap[] pics = new Bitmap[15];
        for (int i = 1; i < 7; i++) {
            int j = i + 8;
            pics[i] = getImage(CH_PICS2[i]);
            pics[j] = getImage(CH_PICS2[j]);
        }
        return pics;
    }

    static void drawPuzzle(Canvas canvas, String pzl, float offs, float fldSz2) {
        Chess chess = new Chess(pzl);
        Paint paint = new Paint();
        float fldSz3 = fldSz2 / 8.0f;
        if (pics1 == null) {
            pics1 = setPics1();
        }
        for (int i = 0; i < 64; i++) {
            int id = chess.get_fld(i);
            Rect rect = new Rect((int) ((((float) (i % 8)) * fldSz3) + offs), (int) ((((float) (7 - (i / 8))) * fldSz3) + offs), (int) ((((float) ((i % 8) + 1)) * fldSz3) + offs), (int) ((((float) ((7 - (i / 8)) + 1)) * fldSz3) + offs));
            if (id != 0) {
                canvas.drawBitmap(pics1[id], (Rect) null, rect, paint);
            }
        }
    }

    static void initPuzzleGroups(Context ctx) {
        if (pzGroups == null) {
            pzGroups = PzGroup.getPzGroups(ctx);
        }
    }

    /* access modifiers changed from: package-private */
    public boolean animOn() {
        return PrefElem.getPref(this, 1).equals(getString(R.string.on_txt));
    }

    static Puzzle getPuzzle() {
        return puzzle;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.main);
        if (savedInstanceState == null) {
            this.metrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(this.metrics);
            this.layout = (RelativeLayout) findViewById(R.id.mainLayout);
            PrefElem.setTheme(this, this.layout);
            initPuzzleGroups(this);
            this.grIdx = S1.getGrIdx();
            this.pzIdx = S2.getPzIdx();
            puzzle = pzGroups[this.grIdx].getPuzzles()[this.pzIdx];
            this.chN8n = new ChN8n[4];
            ChessBoardInit();
        }
    }

    /* access modifiers changed from: package-private */
    public void ChessBoardInit() {
        int VSZ = getVSize();
        int HSZ = getHSize();
        this.GAPX = HSZ / 32;
        this.GAPY = VSZ <= 320 ? 0 : VSZ <= 640 ? 2 : 8;
        int x = (this.GAPX * 9) / 4;
        int y = (this.GAPY * 4) / 3;
        int h = VSZ / 16;
        int H = h + this.GAPY;
        int i = ((HSZ - (x * 10)) - this.GAPX) / 2;
        MaxRows = VSZ < 854 ? 3 : 4;
        this.FSZ = VSZ <= 320 ? 12 : VSZ < 800 ? 16 : 20;
        this.fldSz = ((getHSize() - (this.GAPX * 4)) * 2) / 19;
        setTxtView(this.layout, new TextView(this), "Z-CHESS PUZZLES", ((float) this.FSZ) * 1.2f, HSZ / 6, this.GAPY * 3, (HSZ * 2) / 3, h);
        int y2 = y + (this.GAPY * 4) + H;
        this.text = setTagView(this.layout, puzzle, "", 1.1f * ((float) this.FSZ), HSZ / 16, y2, (HSZ * 5) / 6, H);
        int y3 = y2 + (((H * 5) / 6) - (VSZ / 96));
        this.board = new Board(this, this.fldSz, puzzle);
        setLayout(this.board, x, y3, (this.fldSz * 19) / 2, (this.fldSz * 19) / 2);
        this.chN8n[0] = new ChN8n(this, this.board, "abcdefgh", this.fldSz, false);
        this.chN8n[1] = new ChN8n(this, this.board, "abcdefgh", this.fldSz, false);
        this.chN8n[2] = new ChN8n(this, this.board, "87654321", this.fldSz, true);
        this.chN8n[3] = new ChN8n(this, this.board, "87654321", this.fldSz, true);
        int b = this.fldSz / 8;
        setLayout(this.chN8n[0], (((this.fldSz * 3) / 4) + x) - 0, (((this.fldSz * 0) / 4) + y3) - b, this.fldSz * 8, (this.fldSz * 3) / 4);
        setLayout(this.chN8n[1], (((this.fldSz * 3) / 4) + x) - 0, (((this.fldSz * 35) / 4) + y3) - b, this.fldSz * 8, (this.fldSz * 3) / 4);
        setLayout(this.chN8n[2], (((this.fldSz * 0) / 4) + x) - b, (((this.fldSz * 3) / 4) + y3) - 0, (this.fldSz * 3) / 4, this.fldSz * 8);
        setLayout(this.chN8n[3], (((this.fldSz * 35) / 4) + x) - b, (((this.fldSz * 3) / 4) + y3) - 0, (this.fldSz * 3) / 4, this.fldSz * 8);
        this.varsLL = new RelativeLayout(this);
        setLayout(this.varsLL, this.fldSz / 4, (VSZ / 96) + ((this.fldSz * 19) / 2) + y3, HSZ - (this.fldSz / 2), H * 3);
        int w = ((HSZ / 2) - this.GAPX) / 2;
        BoardButt boardButt = new BoardButt(this, 0);
        BoardButt boardButt2 = new BoardButt(this, 1);
        setLayout(boardButt, ((this.GAPX + w) * 0) + (HSZ / 4), VSZ - ((H * 13) / 8), w, (VSZ * 3) / 64);
        setLayout(boardButt2, (HSZ / 4) + ((this.GAPX + w) * 1), VSZ - ((H * 13) / 8), w, (VSZ * 3) / 64);
    }

    /* access modifiers changed from: package-private */
    public TextView setTxtView(RelativeLayout lout, TextView tv, String txt, float fsz, int x, int y, int w, int h) {
        RelativeLayout.LayoutParams lpar = ZChess.setLoutParams(tv, x, y, w, h);
        ZChess.setTextAttrs(tv, txt, fsz, FontColor1, ShadColor4);
        tv.setSingleLine(true);
        tv.setHorizontallyScrolling(true);
        tv.setGravity(fsz > 1.15f * ((float) this.FSZ) ? 17 : 3);
        lout.addView(tv, lpar);
        return tv;
    }

    /* access modifiers changed from: package-private */
    public TextView setTagView(RelativeLayout lout, Puzzle pzl, String txt, float fsz, int x, int y, int w, int h) {
        TextView tv = new HdrView(this);
        if (pzl != null) {
            txt = "   " + pzl.pzTag + ":" + pzl.desc;
        }
        setTxtView(lout, tv, txt, fsz, x, y, w, h);
        return tv;
    }

    /* access modifiers changed from: package-private */
    public void setTextAttrs(Button butt, String tag) {
        ZChess.setTextAttrs(butt, tag, ((float) this.FSZ) * 0.8f, FontColor1, ShadColor3);
        butt.setGravity(17);
    }

    /* access modifiers changed from: package-private */
    public void showPosition() {
        if (!(this.board == null || this.board.getErrStr() == null)) {
        }
    }

    private void setLayout(RelativeLayout layout2, View view, int x, int y, int w, int h) {
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(w, h);
        lp.leftMargin = x;
        lp.topMargin = y;
        view.setLayoutParams(lp);
        layout2.addView(view, lp);
    }

    private void setLayout(View view, int x, int y, int w, int h) {
        setLayout(this.layout, view, x, y, w, h);
    }

    public int getHSize() {
        int width = this.metrics.widthPixels;
        int height = this.metrics.heightPixels;
        return width < height ? width : height;
    }

    public int getVSize() {
        return this.metrics.heightPixels;
    }

    /* access modifiers changed from: package-private */
    public void setPuzzle(Puzzle pzl) {
        puzzle = pzl;
        this.text.setText("   " + pzl.pzTag + ":" + pzl.desc);
        this.board.setBoard(puzzle.spec);
        this.board.invalidate();
    }

    /* access modifiers changed from: package-private */
    public void updateShPref() {
        String key = puzzle.pzTag;
        String readShPref = Prefs.readShPref(this, key);
        Prefs.writeShPref(this, key, "1");
    }

    /* access modifiers changed from: package-private */
    public String varToUserMode(String var) {
        String str = "";
        String[] tokens = var.trim().split("\\s+");
        for (int i = 0; i < tokens.length; i++) {
            if (i % 2 == 0) {
                str = String.valueOf(str) + "  " + ((i / 2) + 1) + ".";
            }
            str = String.valueOf(str) + " " + tokens[i] + " ";
        }
        return str;
    }

    /* access modifiers changed from: package-private */
    public void doSwap() {
        this.board.revers = !this.board.revers;
        this.board.invalidate();
    }

    /* access modifiers changed from: package-private */
    public void setStepViews(String var, int vcnt, int llw, int llh) {
        int x;
        this.steps = var.trim().split("\\s+");
        int w = (llw * 1) / 5;
        int w1 = (w * 6) / 5;
        int x0 = w * 1;
        int x1 = (w * 1) + ((w * 1) / 4);
        int x2 = ((w * 1) + (w * 2)) - ((w * 2) / 6);
        int h = (llh * 3) / 20;
        int y0 = (llh * 3) / 11;
        int dy = (llh * 8) / 45;
        int scnt = this.steps.length;
        this.relRow = 0;
        setLayout(this.varsLL, new BoardButt(this, 4), x1, (llh * 1) / 32, w1, (llh * 2) / 9);
        this.varView = setTxtView(this.varsLL, new StepView(this, 0), this.varId + "/" + vcnt, ((float) this.FSZ) * 0.9f, x2, llh / 13, w1, h);
        this.varView.setBackgroundColor(SEL_HILIGHT);
        this.varView.setGravity(17);
        this.stepViews = new TextView[scnt];
        int numCnt = (scnt + 1) / 2;
        if (numCnt >= MaxRows) {
            numCnt = MaxRows;
        }
        this.numViews = new TextView[numCnt];
        int sid = 0;
        while (sid < scnt) {
            int row = sid / 2;
            if (row < numCnt) {
                if (sid % 2 == 0) {
                    TextView[] textViewArr = this.numViews;
                    TextView num = setTxtView(this.varsLL, new TextView(this), (row + 1) + ". ", ((float) this.FSZ) * 0.8f, x0, y0 + (row * dy), w / 3, h);
                    textViewArr[row] = num;
                    num.setBackgroundColor(0);
                }
                if (sid % 2 == 0) {
                    x = x1;
                } else {
                    x = x2;
                }
                TextView[] textViewArr2 = this.stepViews;
                TextView tv = setTxtView(this.varsLL, new StepView(this, sid + 1), this.steps[sid], ((float) this.FSZ) * 0.8f, x, y0 + (row * dy), w1, h);
                textViewArr2[sid] = tv;
                tv.setBackgroundColor(BKG_HILIGHT);
                tv.setGravity(17);
                sid++;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void doNextVariant() {
        int llw = this.varsLL.getWidth();
        int llh = this.varsLL.getHeight();
        int cnt = puzzle.vars.size();
        RelativeLayout varsLL0 = this.varsLL;
        this.varsLL = new RelativeLayout(this);
        this.varsLL.setLayoutParams(varsLL0.getLayoutParams());
        this.varId = (this.varId + 1) % (cnt + 1);
        if (this.varId == 0) {
            this.varId = this.varId + 1;
        }
        this.stepId = 0;
        this.board.setBoard(puzzle.spec);
        this.board.setMateToKing(ShadColor2);
        BoardButt boardButt = new BoardButt(this, 2);
        BoardButt boardButt2 = new BoardButt(this, 3);
        int w = (llw * 9) / 10;
        int h = llh / 5;
        setLayout(this.varsLL, boardButt, w / 32, (h * 1) + (h / 4), w / 6, h * 1);
        setLayout(this.varsLL, boardButt2, (llw - (w / 6)) - (w / 32), (h / 4) + (h * 1), w / 6, h * 1);
        setStepViews(puzzle.vars.get(this.varId - 1), cnt, llw, llh);
        this.layout.removeView(varsLL0);
        this.layout.addView(this.varsLL);
        this.varsLL.invalidate();
    }

    /* access modifiers changed from: package-private */
    public void toggleSolutionShow() {
        this.board.setMateToKing(ShadColor2);
        if (this.varId == 0) {
            updateShPref();
            doNextVariant();
            return;
        }
        this.varId = 0;
        this.varsLL.removeAllViews();
        this.varsLL.invalidate();
        this.board.setBoard(puzzle.spec);
    }

    /* access modifiers changed from: package-private */
    public String updFigForLastPawnLine(String step, String nFloc, boolean whiteStep) {
        int i;
        StringBuilder sb = new StringBuilder();
        if (whiteStep) {
            i = 8;
        } else {
            i = 1;
        }
        String lastLine = sb.append(i).toString();
        if (step.length() <= 2 || !nFloc.substring(1).equals(lastLine)) {
            return nFloc;
        }
        String fig = "Q";
        String fg = step.substring(5, 6).toUpperCase();
        if (fg.equals("R") || fg.equals("B") || fg.equals("N")) {
            fig = fg;
        }
        return String.valueOf(fig) + nFloc;
    }

    /* access modifiers changed from: package-private */
    public String stepForSpec(String oSpec, String step, boolean whiteStep, boolean doAnim) {
        String fig0;
        String loc;
        String nSpec = "";
        String[] flocs = oSpec.trim().split("\\s+");
        String step2 = step.trim();
        char ch = step2.charAt(2);
        int dash = (ch == '-' || ch == 'x') ? 2 : 3;
        String fig = step2.substring(0, dash - 2);
        String loc0 = step2.substring(dash - 2, dash);
        String loc1 = step2.substring(dash + 1, dash + 3);
        if (dash == 2) {
            fig0 = "P";
        } else {
            fig0 = fig;
        }
        String fig1 = "Z";
        String fig2 = fig0;
        for (int i = 0; i < flocs.length; i++) {
            String floc = flocs[i];
            if (floc.length() == 3) {
                loc = floc.substring(1);
            } else {
                loc = floc;
            }
            if (floc.equals(String.valueOf(fig) + loc0)) {
                String nFloc = String.valueOf(fig) + loc1;
                if (dash == 2) {
                    nFloc = updFigForLastPawnLine(step2, nFloc, whiteStep);
                    if (nFloc.length() == 3) {
                        fig2 = nFloc.substring(0, 1);
                    }
                }
                nSpec = String.valueOf(nSpec) + nFloc + " ";
            } else if (!loc.equals(loc1)) {
                nSpec = String.valueOf(nSpec) + floc + " ";
            } else {
                fig1 = floc.length() == 2 ? "P" : floc.substring(0, 1);
            }
        }
        String nSpec2 = nSpec.trim();
        if (doAnim) {
            String[] animSteps = new String[4];
            animSteps[0] = String.valueOf(fig0) + loc0;
            animSteps[1] = String.valueOf(fig1) + loc1;
            animSteps[2] = String.valueOf(fig2) + loc1;
            animSteps[3] = whiteStep ? "white" : "black";
            this.board.setAnimSteps(animSteps, nSpec2);
        } else {
            this.board.setBoard(nSpec2);
        }
        return nSpec2;
    }

    /* access modifiers changed from: package-private */
    public void shiftStepViews() {
        int i;
        int lastRow = (this.steps.length + 1) / 2;
        int absRow = (this.stepId + 1) / 2;
        if (absRow < MaxRows) {
            i = absRow;
        } else if (absRow == lastRow) {
            i = MaxRows;
        } else {
            i = MaxRows - 1;
        }
        this.relRow = i;
        int shift = absRow - this.relRow;
        int i2 = 0;
        while (i2 < MaxRows && i2 + shift < lastRow) {
            int j = i2 + shift;
            this.numViews[i2].setText(new StringBuilder().append(j + 1).toString());
            this.stepViews[i2 * 2].setText(this.steps[j * 2]);
            this.stepViews[(i2 * 2) + 1].setText((j * 2) + 1 < this.steps.length ? this.steps[(j * 2) + 1] : "");
            i2++;
        }
    }

    /* access modifiers changed from: package-private */
    public void doStep(int delta) {
        int idx;
        boolean doAnim;
        boolean z;
        int i;
        int scnt = this.steps.length;
        (this.stepId == 0 ? this.varView : this.stepViews[(this.stepId - ((((this.stepId + 1) / 2) - this.relRow) * 2)) - 1]).setBackgroundColor(BKG_HILIGHT);
        this.stepId = (((scnt + 1) + this.stepId) + delta) % (scnt + 1);
        if (this.stepId == 1 && this.steps[0].equals("...")) {
            if (delta > 0) {
                i = 2;
            } else {
                i = 0;
            }
            this.stepId = i;
        }
        this.relRow = (this.stepId + 1) / 2;
        if ((scnt + 1) / 2 > MaxRows) {
            shiftStepViews();
        }
        (this.stepId == 0 ? this.varView : this.stepViews[(this.stepId - ((((this.stepId + 1) / 2) - this.relRow) * 2)) - 1]).setBackgroundColor(SEL_HILIGHT);
        String nSpec = puzzle.spec;
        for (int sid = 0; sid < this.stepId; sid++) {
            if (sid != 0 || !this.steps[sid].equals("...")) {
                if (!animOn() || delta != 1) {
                    doAnim = false;
                } else {
                    doAnim = true;
                }
                String str = this.steps[sid];
                if (sid % 2 == 0) {
                    z = true;
                } else {
                    z = false;
                }
                nSpec = stepForSpec(nSpec, str, z, doAnim);
            }
        }
        if (this.stepId == 0) {
            this.board.setBoard(nSpec);
        }
        String step = this.stepId == 0 ? null : this.steps[this.stepId - 1];
        if (this.stepId == scnt && step.charAt(step.length() - 1) == '#') {
            if (this.stepId % 2 == 1) {
                idx = nSpec.lastIndexOf(75);
            } else {
                idx = nSpec.indexOf(75);
            }
            this.board.setMateToKing(Chess.strToFld(nSpec.substring(idx + 1, idx + 3)));
            return;
        }
        this.board.setMateToKing(ShadColor2);
    }

    /* access modifiers changed from: package-private */
    public void doTouchStep(int touchStep) {
        doStep((((((this.stepId + 1) / 2) - this.relRow) * 2) + touchStep) - this.stepId);
    }

    /* access modifiers changed from: package-private */
    public void doPrevStep() {
        doStep(ShadColor2);
    }

    /* access modifiers changed from: package-private */
    public void doNextStep() {
        doStep(1);
    }

    /* access modifiers changed from: package-private */
    public void doNextPuzzle() {
        Puzzle[] puzzles = pzGroups[this.grIdx].getPuzzles();
        this.pzIdx = (this.pzIdx + 1) % puzzles.length;
        setPuzzle(puzzles[this.pzIdx]);
    }

    /* access modifiers changed from: package-private */
    public void doPrevPuzzle() {
        Puzzle[] puzzles = pzGroups[this.grIdx].getPuzzles();
        this.pzIdx = ((this.pzIdx + puzzles.length) - 1) % puzzles.length;
        setPuzzle(puzzles[this.pzIdx]);
    }

    public void finish() {
        ZChess.vibrate(this, 20);
        super.finish();
    }
}
