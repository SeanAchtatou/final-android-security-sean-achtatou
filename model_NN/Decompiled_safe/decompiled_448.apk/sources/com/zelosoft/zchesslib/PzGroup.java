package com.zelosoft.zchesslib;

import android.content.Context;
import com.zelosoft.zchess101.R;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Vector;

/* compiled from: ChessBoard */
class PzGroup {
    static final int GrMarkLen = GroupMark.length();
    static final String GroupMark = "Group: ";
    static PzGroup[] pzGroups = null;
    final String grTag;
    final Puzzle[] puzzles;

    static String spec2Upcase(String spec) {
        String nSpec = "";
        String[] items = spec.trim().split("\\s+");
        for (int i = GrMarkLen; i < items.length; i++) {
            String item = items[i];
            if (item.length() > 2 && Character.isDigit(item.charAt(2))) {
                item = String.valueOf(item.substring(GrMarkLen, 1).toUpperCase()) + item.substring(1);
            }
            nSpec = String.valueOf(nSpec) + item + (i + 1 < items.length ? " " : "");
        }
        return nSpec;
    }

    /* JADX WARNING: CFG modification limit reached, blocks count: 133 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static com.zelosoft.zchesslib.Puzzle readOnePuzzleFromFile(java.io.BufferedReader r13, java.lang.String r14) throws java.io.IOException {
        /*
            r12 = 0
            r11 = 256(0x100, float:3.59E-43)
            r10 = 0
            r2 = 0
            boolean r8 = r13.markSupported()
            if (r8 != 0) goto L_0x001b
            java.io.IOException r8 = new java.io.IOException
            java.lang.String r9 = "markSupported() returns false!"
            r8.<init>(r9)
            throw r8
        L_0x0013:
            java.lang.String r8 = ""
            boolean r8 = r5.equals(r8)
            if (r8 == 0) goto L_0x0049
        L_0x001b:
            r13.mark(r11)
            java.lang.String r5 = r13.readLine()
            if (r5 == 0) goto L_0x002c
            java.lang.String r8 = "End:"
            boolean r8 = r5.equals(r8)
            if (r8 == 0) goto L_0x002e
        L_0x002c:
            r8 = r12
        L_0x002d:
            return r8
        L_0x002e:
            int r8 = r5.length()
            int r9 = com.zelosoft.zchesslib.PzGroup.GrMarkLen
            if (r8 <= r9) goto L_0x0013
            int r8 = com.zelosoft.zchesslib.PzGroup.GrMarkLen
            java.lang.String r8 = r5.substring(r10, r8)
            java.lang.String r9 = "Group: "
            boolean r8 = r8.equals(r9)
            if (r8 == 0) goto L_0x0013
            r13.reset()
            r8 = r12
            goto L_0x002d
        L_0x0049:
            r8 = 58
            int r1 = r5.indexOf(r8)
            r8 = -1
            if (r1 == r8) goto L_0x001b
            java.lang.String r3 = r5.substring(r10, r1)
            int r8 = r1 + 1
            java.lang.String r0 = r5.substring(r8)
            java.lang.String r8 = r13.readLine()
            java.lang.String r4 = spec2Upcase(r8)
            java.util.Vector r7 = new java.util.Vector
            r7.<init>()
            r6 = 0
        L_0x006a:
            java.lang.String r6 = r13.readLine()
            if (r6 == 0) goto L_0x007e
            int r8 = r6.length()
            if (r8 <= 0) goto L_0x007e
            char r8 = r6.charAt(r10)
            r9 = 32
            if (r8 == r9) goto L_0x0088
        L_0x007e:
            r13.reset()
            com.zelosoft.zchesslib.Puzzle r2 = new com.zelosoft.zchesslib.Puzzle
            r2.<init>(r3, r0, r4, r7)
            r8 = r2
            goto L_0x002d
        L_0x0088:
            r13.mark(r11)
            java.lang.String r8 = spec2Upcase(r6)
            r7.add(r8)
            goto L_0x006a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.zelosoft.zchesslib.PzGroup.readOnePuzzleFromFile(java.io.BufferedReader, java.lang.String):com.zelosoft.zchesslib.Puzzle");
    }

    static PzGroup readOnePzGroupFromFile(BufferedReader br) throws IOException {
        String grTag2 = null;
        Vector<Puzzle> pzVect = new Vector<>();
        while (true) {
            String str = br.readLine();
            if (str != null && !str.equals("End:")) {
                if (!str.equals("") && str.substring(GrMarkLen, GrMarkLen).equals(GroupMark)) {
                    grTag2 = str.substring(GrMarkLen, str.length());
                    while (true) {
                        Puzzle pzl = readOnePuzzleFromFile(br, grTag2);
                        if (pzl == null) {
                            break;
                        }
                        pzVect.add(pzl);
                    }
                }
            } else {
                break;
            }
        }
        if (pzVect.size() == 0) {
            return null;
        }
        Puzzle[] puzzles2 = new Puzzle[pzVect.size()];
        for (int idx = GrMarkLen; idx < puzzles2.length; idx++) {
            puzzles2[idx] = (Puzzle) pzVect.get(idx);
        }
        return new PzGroup(grTag2, puzzles2);
    }

    static PzGroup[] readAllPzGroupsFromFile(Context ctx) {
        InputStream is = ctx.getResources().openRawResource(R.raw.taskset1);
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        Vector<PzGroup> grpVect = new Vector<>(6);
        while (true) {
            try {
                PzGroup pzGroup = readOnePzGroupFromFile(br);
                if (pzGroup == null) {
                    break;
                }
                grpVect.add(pzGroup);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        is.close();
        PzGroup[] pzGroups2 = new PzGroup[grpVect.size()];
        for (int idx = GrMarkLen; idx < pzGroups2.length; idx++) {
            pzGroups2[idx] = (PzGroup) grpVect.get(idx);
        }
        return pzGroups2;
    }

    PzGroup(String grTag2, Puzzle[] puzzles2) {
        this.grTag = grTag2;
        this.puzzles = puzzles2;
    }

    /* access modifiers changed from: package-private */
    public Puzzle[] getPuzzles() {
        return this.puzzles;
    }

    /* access modifiers changed from: package-private */
    public Puzzle getPuzzle(int idx) {
        return this.puzzles[idx];
    }

    static PzGroup[] getPzGroups(Context ctx) {
        if (pzGroups == null) {
            pzGroups = readAllPzGroupsFromFile(ctx);
        }
        return pzGroups;
    }

    static Puzzle[] getPuzzles(Context ctx, int grpIdx) {
        if (pzGroups == null) {
            pzGroups = readAllPzGroupsFromFile(ctx);
        }
        return pzGroups[grpIdx % pzGroups.length].getPuzzles();
    }

    static String[] getPzGroupTags(Context ctx) {
        if (pzGroups == null) {
            pzGroups = readAllPzGroupsFromFile(ctx);
        }
        String[] tags = new String[pzGroups.length];
        for (int idx = GrMarkLen; idx < tags.length; idx++) {
            tags[idx] = pzGroups[idx].grTag;
        }
        return tags;
    }
}
