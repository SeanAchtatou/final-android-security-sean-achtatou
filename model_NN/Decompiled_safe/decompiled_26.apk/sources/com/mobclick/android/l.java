package com.mobclick.android;

import android.content.Context;
import org.json.JSONObject;

final class l implements Runnable {
    private static final Object a = new Object();
    private MobclickAgent b = MobclickAgent.a;
    private Context c;
    private JSONObject d;

    l(MobclickAgent mobclickAgent, Context context, JSONObject jSONObject) {
        this.c = context;
        this.d = jSONObject;
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r4 = this;
            java.lang.String r0 = "type"
            org.json.JSONObject r0 = r4.d     // Catch:{ Exception -> 0x0036 }
            java.lang.String r1 = "type"
            java.lang.String r0 = r0.getString(r1)     // Catch:{ Exception -> 0x0036 }
            java.lang.String r1 = "update"
            boolean r0 = r0.equals(r1)     // Catch:{ Exception -> 0x0036 }
            if (r0 == 0) goto L_0x001c
            com.mobclick.android.MobclickAgent r0 = r4.b     // Catch:{ Exception -> 0x0036 }
            android.content.Context r1 = r4.c     // Catch:{ Exception -> 0x0036 }
            org.json.JSONObject r2 = r4.d     // Catch:{ Exception -> 0x0036 }
            r0.c(r1, r2)     // Catch:{ Exception -> 0x0036 }
        L_0x001b:
            return
        L_0x001c:
            org.json.JSONObject r0 = r4.d     // Catch:{ Exception -> 0x0036 }
            java.lang.String r1 = "type"
            java.lang.String r0 = r0.getString(r1)     // Catch:{ Exception -> 0x0036 }
            java.lang.String r1 = "online_config"
            boolean r0 = r0.equals(r1)     // Catch:{ Exception -> 0x0036 }
            if (r0 == 0) goto L_0x0042
            com.mobclick.android.MobclickAgent r0 = r4.b     // Catch:{ Exception -> 0x0036 }
            android.content.Context r1 = r4.c     // Catch:{ Exception -> 0x0036 }
            org.json.JSONObject r2 = r4.d     // Catch:{ Exception -> 0x0036 }
            r0.h(r1, r2)     // Catch:{ Exception -> 0x0036 }
            goto L_0x001b
        L_0x0036:
            r0 = move-exception
            java.lang.String r1 = "MobclickAgent"
            java.lang.String r2 = "Exception occurred when sending message."
            android.util.Log.e(r1, r2)
            r0.printStackTrace()
            goto L_0x001b
        L_0x0042:
            java.lang.Object r0 = com.mobclick.android.l.a     // Catch:{ Exception -> 0x0036 }
            monitor-enter(r0)     // Catch:{ Exception -> 0x0036 }
            com.mobclick.android.MobclickAgent r1 = r4.b     // Catch:{ all -> 0x0050 }
            android.content.Context r2 = r4.c     // Catch:{ all -> 0x0050 }
            org.json.JSONObject r3 = r4.d     // Catch:{ all -> 0x0050 }
            r1.e(r2, r3)     // Catch:{ all -> 0x0050 }
            monitor-exit(r0)     // Catch:{ all -> 0x0050 }
            goto L_0x001b
        L_0x0050:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0050 }
            throw r1     // Catch:{ Exception -> 0x0036 }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobclick.android.l.run():void");
    }
}
