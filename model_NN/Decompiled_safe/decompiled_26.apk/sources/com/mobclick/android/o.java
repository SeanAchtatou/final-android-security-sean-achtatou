package com.mobclick.android;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.util.Log;
import android.widget.Toast;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;
import javax.microedition.khronos.opengles.GL10;

public class o {
    static String a = "utf-8";
    public static int b;

    /* JADX WARNING: Removed duplicated region for block: B:8:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a() {
        /*
            java.lang.String r4 = "MobclickAgent"
            r0 = 0
            java.io.FileReader r1 = new java.io.FileReader     // Catch:{ FileNotFoundException -> 0x003b }
            java.lang.String r2 = "/proc/cpuinfo"
            r1.<init>(r2)     // Catch:{ FileNotFoundException -> 0x003b }
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ IOException -> 0x002e }
            r3 = 1024(0x400, float:1.435E-42)
            r2.<init>(r1, r3)     // Catch:{ IOException -> 0x002e }
            java.lang.String r0 = r2.readLine()     // Catch:{ IOException -> 0x002e }
            r2.close()     // Catch:{ IOException -> 0x004f, FileNotFoundException -> 0x0048 }
            r1.close()     // Catch:{ IOException -> 0x004f, FileNotFoundException -> 0x0048 }
        L_0x001b:
            if (r0 == 0) goto L_0x0029
            r1 = 58
            int r1 = r0.indexOf(r1)
            int r1 = r1 + 1
            java.lang.String r0 = r0.substring(r1)
        L_0x0029:
            java.lang.String r0 = r0.trim()
            return r0
        L_0x002e:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x0032:
            java.lang.String r2 = "MobclickAgent"
            java.lang.String r3 = "Could not read from file /proc/cpuinfo"
            android.util.Log.e(r2, r3, r0)     // Catch:{ FileNotFoundException -> 0x004d }
            r0 = r1
            goto L_0x001b
        L_0x003b:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x003f:
            java.lang.String r2 = "MobclickAgent"
            java.lang.String r2 = "Could not open file /proc/cpuinfo"
            android.util.Log.e(r4, r2, r0)
            r0 = r1
            goto L_0x001b
        L_0x0048:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x003f
        L_0x004d:
            r0 = move-exception
            goto L_0x003f
        L_0x004f:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0032
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobclick.android.o.a():java.lang.String");
    }

    public static String a(String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(str.getBytes());
            byte[] digest = instance.digest();
            StringBuffer stringBuffer = new StringBuffer();
            for (byte b2 : digest) {
                stringBuffer.append(Integer.toHexString(b2 & 255));
            }
            return stringBuffer.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String a(byte[] bArr, int i) {
        try {
            Inflater inflater = new Inflater();
            inflater.setInput(bArr, 0, i);
            byte[] bArr2 = new byte[100];
            int inflate = inflater.inflate(bArr2);
            inflater.end();
            try {
                return new String(bArr2, 0, inflate, "utf-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                return null;
            }
        } catch (DataFormatException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public static void a(Context context) {
        Toast.makeText(context, n.o(context), 0).show();
    }

    public static boolean a(Context context, String str) {
        return context.getPackageManager().checkPermission(str, context.getPackageName()) == 0;
    }

    public static String[] a(GL10 gl10) {
        try {
            return new String[]{gl10.glGetString(7936), gl10.glGetString(7937)};
        } catch (Exception e) {
            Log.e(n.c, "Could not read gpu infor:", e);
            return new String[0];
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0029  */
    /* JADX WARNING: Removed duplicated region for block: B:17:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String b(android.content.Context r5) {
        /*
            java.lang.String r4 = "No IMEI."
            java.lang.String r3 = "MobclickAgent"
            java.lang.String r0 = "phone"
            java.lang.Object r0 = r5.getSystemService(r0)
            android.telephony.TelephonyManager r0 = (android.telephony.TelephonyManager) r0
            if (r0 != 0) goto L_0x0015
            java.lang.String r1 = "MobclickAgent"
            java.lang.String r1 = "No IMEI."
            android.util.Log.w(r3, r4)
        L_0x0015:
            java.lang.String r1 = ""
            java.lang.String r2 = "android.permission.READ_PHONE_STATE"
            boolean r2 = a(r5, r2)     // Catch:{ Exception -> 0x003f }
            if (r2 == 0) goto L_0x0043
            java.lang.String r0 = r0.getDeviceId()     // Catch:{ Exception -> 0x003f }
        L_0x0023:
            boolean r1 = android.text.TextUtils.isEmpty(r0)
            if (r1 == 0) goto L_0x003e
            java.lang.String r0 = "MobclickAgent"
            java.lang.String r0 = "No IMEI."
            android.util.Log.w(r3, r4)
            java.lang.String r0 = c(r5)
            if (r0 != 0) goto L_0x003e
            java.lang.String r0 = "MobclickAgent"
            java.lang.String r0 = "Failed to take mac as IMEI."
            android.util.Log.w(r3, r0)
            r0 = 0
        L_0x003e:
            return r0
        L_0x003f:
            r0 = move-exception
            r0.printStackTrace()
        L_0x0043:
            r0 = r1
            goto L_0x0023
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobclick.android.o.b(android.content.Context):java.lang.String");
    }

    public static byte[] b(String str) {
        b = 0;
        byte[] bytes = str.getBytes(a);
        Deflater deflater = new Deflater();
        deflater.setInput(bytes);
        deflater.finish();
        byte[] bArr = new byte[8192];
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        while (!deflater.finished()) {
            int deflate = deflater.deflate(bArr);
            b += deflate;
            byteArrayOutputStream.write(bArr, 0, deflate);
        }
        deflater.end();
        return byteArrayOutputStream.toByteArray();
    }

    private static String c(Context context) {
        try {
            return ((WifiManager) context.getSystemService("wifi")).getConnectionInfo().getMacAddress();
        } catch (Exception e) {
            Log.i(n.c, "Could not read MAC, forget to include ACCESS_WIFI_STATE permission?", e);
            return null;
        }
    }
}
