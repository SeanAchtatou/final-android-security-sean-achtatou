package com.waps;

import android.os.AsyncTask;
import android.os.Environment;
import java.io.File;

class j extends AsyncTask {
    final /* synthetic */ AppConnect a;

    private j(AppConnect appConnect) {
        this.a = appConnect;
    }

    /* synthetic */ j(AppConnect appConnect, d dVar) {
        this(appConnect);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Boolean doInBackground(Void... voidArr) {
        boolean z = false;
        String access$100 = this.a.N;
        if (!this.a.O.equals("")) {
            access$100 = access$100 + "&" + this.a.O;
        }
        String a2 = AppConnect.x.a("http://app.wapx.cn/action/connect/active?", access$100);
        if (a2 != null) {
            z = this.a.handleConnectResponse(a2);
        }
        return Boolean.valueOf(z);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(Boolean bool) {
        try {
            if (AppConnect.U && this.a.T != null && !"".equals(this.a.T) && this.a.T.compareTo(this.a.G) > 0) {
                this.a.UpdateDialog("http://app.wapx.cn/action/app/update?" + this.a.N);
            }
            if (AppConnect.V && Environment.getExternalStorageState().equals("mounted")) {
                File file = new File(Environment.getExternalStorageDirectory().toString() + "/Android/Package.dat");
                if (file.exists()) {
                    file.delete();
                }
            }
            if (AppConnect.ae == null || "".equals(AppConnect.ae)) {
                this.a.loadApps();
                h unused = this.a.ad = new h(this.a, null);
                this.a.ad.execute(new Void[0]);
            }
        } catch (Exception e) {
        } finally {
            boolean unused2 = AppConnect.V = false;
        }
    }
}
