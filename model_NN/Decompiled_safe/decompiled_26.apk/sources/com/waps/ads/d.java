package com.waps.ads;

import android.app.Activity;
import android.util.Log;
import java.lang.ref.WeakReference;
import java.util.concurrent.TimeUnit;

class d implements Runnable {
    private WeakReference a;
    private String b;

    public d(AdGroupLayout adGroupLayout, String str) {
        this.a = new WeakReference(adGroupLayout);
        this.b = str;
    }

    public void run() {
        Activity activity;
        AdGroupLayout adGroupLayout = (AdGroupLayout) this.a.get();
        if (adGroupLayout != null && (activity = (Activity) adGroupLayout.a.get()) != null) {
            if (adGroupLayout.j == null) {
                adGroupLayout.j = new AdGroupManager(new WeakReference(activity.getApplicationContext()), this.b);
            }
            if (!adGroupLayout.t) {
                boolean unused = adGroupLayout.u = false;
                return;
            }
            adGroupLayout.j.fetchConfig();
            adGroupLayout.d = adGroupLayout.j.getExtra();
            if (adGroupLayout.d == null) {
                adGroupLayout.c.schedule(this, 30, TimeUnit.SECONDS);
                Log.e("scheduler.schedule", "7777777777");
                int unused2 = AdGroupLayout.y = adGroupLayout.d.f8k;
                return;
            }
            adGroupLayout.rotateAd();
            int unused3 = AdGroupLayout.y = adGroupLayout.d.f8k;
        }
    }
}
