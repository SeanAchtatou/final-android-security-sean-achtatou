package com.waps.ads;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import com.waps.AppConnect;
import com.waps.ads.b.a;
import com.waps.ads.b.b;
import com.waps.ads.b.c;
import com.waps.n;
import java.lang.ref.WeakReference;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.apache.http.HttpHost;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;

public class AdGroupLayout extends RelativeLayout {

    /* renamed from: k  reason: collision with root package name */
    public static String f7k = "";
    public static String l = "";
    static long o = 0;
    static int p = 30;
    /* access modifiers changed from: private */
    public static Activity x;
    /* access modifiers changed from: private */
    public static int y;
    public WeakReference a;
    public final Handler b = new Handler();
    public final ScheduledExecutorService c = Executors.newScheduledThreadPool(1);
    public b d;
    public a e;
    public WeakReference f;
    public c g;
    public c h;
    public b i;
    public AdGroupManager j;
    long m = 0;
    long n = 0;
    String q;
    String r;
    private String s;
    /* access modifiers changed from: private */
    public boolean t;
    /* access modifiers changed from: private */
    public boolean u;
    private int v;
    private String w = "";
    private int z;

    public AdGroupLayout(Activity activity) {
        super(activity);
        String adGroupKey = getAdGroupKey(activity);
        if (Looper.myLooper() == null) {
            Looper.prepare();
        }
        init(activity, adGroupKey);
        x = activity;
    }

    public AdGroupLayout(Activity activity, String str) {
        super(activity);
        init(activity, str);
    }

    public AdGroupLayout(Activity activity, int[] iArr) {
        super(activity);
        init(activity, getAdGroupKey(activity));
        x = activity;
    }

    public AdGroupLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init((Activity) context, getAdGroupKey(context));
    }

    private void DisplayAd(int i2, String str) {
        o = System.currentTimeMillis();
        if (p < 20) {
            p = 20;
        }
        this.r = str;
        new a(this).start();
    }

    private void countClick() {
        if (this.g != null) {
            this.w = "nid=" + this.g.a + "&";
            this.w += new AppConnect().getParams((Context) this.a.get());
            StringBuilder append = new StringBuilder().append(this.w);
            String str = "&ad_type=" + this.g.b;
            l = str;
            this.w = append.append(str).toString();
            String str2 = "http://ads.waps.cn/action/adgroup/ad_click?" + this.w;
            Log.i("AdGroup", "countClick: " + str2);
            this.c.schedule(new e(str2), 0, TimeUnit.SECONDS);
            Log.e("scheduler.schedule", "6666666666");
        }
    }

    private void countImpression() {
        Log.i("AdGroup", "activeRation: " + this.g);
        if (this.g != null) {
            this.w = "nid=" + this.g.a + "&";
            this.w += new AppConnect().getParams((Context) this.a.get());
            StringBuilder append = new StringBuilder().append(this.w);
            String str = "&ad_type=" + this.g.b;
            l = str;
            this.w = append.append(str).toString();
            String str2 = "http://ads.waps.cn/action/adgroup/ad_impression?" + this.w;
            Log.i("AdGroup", "countImpression: " + str2);
            this.c.schedule(new e(str2), 0, TimeUnit.SECONDS);
            Log.e("scheduler.schedule", "5555555555++" + str2);
        }
        this.n = this.m;
    }

    /* access modifiers changed from: private */
    public void handleAd() {
        if (this.h == null) {
            Log.e("AdGroup_SDK", "nextRation is null!");
            rotateThreadedDelayed();
            return;
        }
        Log.d("AdGroup_SDK", String.format("Showing ad:\n\tnid: %s\n\tname: %s\n\ttype: %d\n\tkey: %s\n\tkey2: %s", this.h.a, this.h.c, Integer.valueOf(this.h.b), this.h.e, this.h.f));
        try {
            com.waps.ads.a.a.handle(this, this.h);
        } catch (Throwable th) {
            Log.w("AdGroup_SDK", "Caught an exception in adapter:", th);
            rollover();
        }
    }

    /* access modifiers changed from: private */
    public void rotateAd() {
        if (!this.t) {
            this.u = false;
            return;
        }
        Log.i("AdGroup_SDK", "Rotating Ad");
        this.h = this.j.getRation();
        this.b.post(new c(this));
    }

    /* access modifiers changed from: private */
    public void sendUrl(String str) {
        try {
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
            n nVar = new n();
            Activity activity = x;
            Activity activity2 = x;
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) activity.getSystemService("connectivity")).getActiveNetworkInfo();
            if (!(activeNetworkInfo == null || activeNetworkInfo.getExtraInfo() == null || !activeNetworkInfo.getExtraInfo().equals("cmwap"))) {
                nVar.a(true);
            }
            if (!nVar.a()) {
                defaultHttpClient.execute(new HttpGet(str));
                return;
            }
            HttpHost httpHost = new HttpHost("10.0.0.172", 80, "http");
            HttpHost httpHost2 = new HttpHost("ads.wapx.cn", 80, "http");
            HttpGet httpGet = new HttpGet(str.replaceAll(" ", "%20").replaceFirst("http://ads.wapx.cn", ""));
            BasicHttpParams basicHttpParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(basicHttpParams, 15000);
            HttpConnectionParams.setSoTimeout(basicHttpParams, 30000);
            DefaultHttpClient defaultHttpClient2 = new DefaultHttpClient(basicHttpParams);
            defaultHttpClient2.getParams().setParameter("http.route.default-proxy", httpHost);
            defaultHttpClient2.execute(httpHost2, httpGet);
        } catch (ClientProtocolException e2) {
            Log.e("AdGroup_SDK", "Caught ClientProtocolException in fetchConfig()", e2);
        } catch (Exception e3) {
            Log.e("AdGroup_SDK", "Caught IOException in fetchConfig()", e3);
        }
    }

    /* access modifiers changed from: protected */
    public String getAdGroupKey(Context context) {
        String packageName = context.getPackageName();
        String name = context.getClass().getName();
        PackageManager packageManager = context.getPackageManager();
        try {
            Bundle bundle = packageManager.getActivityInfo(new ComponentName(packageName, name), 128).metaData;
            if (bundle != null) {
                return bundle.getString("WAPS_ID");
            }
            try {
                Bundle bundle2 = packageManager.getApplicationInfo(packageName, 128).metaData;
                if (bundle2 != null) {
                    return bundle2.getString("WAPS_ID");
                }
                return null;
            } catch (PackageManager.NameNotFoundException e2) {
                return null;
            }
        } catch (PackageManager.NameNotFoundException e3) {
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void init(Activity activity, String str) {
        this.a = new WeakReference(activity);
        this.f = new WeakReference(this);
        this.s = str;
        this.t = true;
        this.u = true;
        this.c.schedule(new d(this, str), 0, TimeUnit.SECONDS);
        Log.e("scheduler.schedule", "1111111111");
        setHorizontalScrollBarEnabled(false);
        setVerticalScrollBarEnabled(false);
        this.v = 0;
        this.z = 0;
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                Log.d("AdGroup_SDK", "Intercepted ACTION_DOWN event");
                if (this.g != null) {
                    countClick();
                    if (this.g.b == 9) {
                        if (this.e != null && this.e.b != null) {
                            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(this.e.b));
                            intent.addFlags(268435456);
                            try {
                                if (this.a != null) {
                                    Activity activity = (Activity) this.a.get();
                                    if (activity != null) {
                                        activity.startActivity(intent);
                                        break;
                                    } else {
                                        return false;
                                    }
                                } else {
                                    return false;
                                }
                            } catch (Exception e2) {
                                Log.w("AdGroup_SDK", "Could not handle click to " + this.e.b, e2);
                                break;
                            }
                        } else {
                            Log.w("AdGroup_SDK", "In onInterceptTouchEvent(), but custom or custom.link is null");
                            break;
                        }
                    }
                }
                break;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        super.onMeasure((this.v <= 0 || View.MeasureSpec.getSize(i2) <= this.v) ? i2 : View.MeasureSpec.makeMeasureSpec(this.v, Integer.MIN_VALUE), (this.z <= 0 || View.MeasureSpec.getSize(i3) <= this.z) ? i3 : View.MeasureSpec.makeMeasureSpec(this.z, Integer.MIN_VALUE));
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int i2) {
        if (i2 == 0) {
            this.t = true;
            if (!this.u) {
                this.u = true;
                if (this.d != null) {
                    rotateThreadedNow();
                    return;
                }
                this.c.schedule(new d(this, this.s), 0, TimeUnit.SECONDS);
                Log.e("scheduler.schedule", "2222222222");
                return;
            }
            return;
        }
        this.t = false;
    }

    public void pushSubView(ViewGroup viewGroup) {
        RelativeLayout relativeLayout = (RelativeLayout) this.f.get();
        if (relativeLayout != null) {
            relativeLayout.removeAllViews();
            relativeLayout.clearAnimation();
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams.addRule(13);
            relativeLayout.addView(viewGroup, layoutParams);
            Log.d("AdGroup_SDK", "Added subview");
            this.g = this.h;
            countImpression();
        }
    }

    public void rollover() {
        this.h = this.j.getRollover();
        this.b.post(new c(this));
    }

    public void rotateThreadedDelayed() {
        this.m = System.currentTimeMillis();
        Log.d("AdGroup_SDK", "Will call rotateAd() in " + this.d.i + " seconds");
        this.c.schedule(new f(this), (long) this.d.i, TimeUnit.SECONDS);
        Log.e("scheduler.schedule", "4444444444++");
    }

    public void rotateThreadedNow() {
        this.c.schedule(new f(this), 0, TimeUnit.SECONDS);
        Log.e("scheduler.schedule", "3333333333");
    }

    public void setAdGroupInterface(b bVar) {
        this.i = bVar;
    }

    public void setMaxHeight(int i2) {
        this.z = i2;
    }

    public void setMaxWidth(int i2) {
        this.v = i2;
    }

    public void updateResultsInUi(View view) {
        RelativeLayout relativeLayout = (RelativeLayout) this.f.get();
        if (relativeLayout != null) {
            relativeLayout.removeAllViews();
            relativeLayout.clearAnimation();
            ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
            double d2 = ((double) layoutParams.width) / ((double) layoutParams.height);
            int width = getRootView().getWidth();
            int height = getRootView().getHeight();
            if (height != 0 && width > height) {
                width = height;
            }
            RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(width, (int) (((double) width) / d2));
            layoutParams2.addRule(13);
            relativeLayout.addView(view, layoutParams2);
            Log.d("AdGroup_SDK", "update image view");
            this.g = this.h;
            countImpression();
        }
    }
}
