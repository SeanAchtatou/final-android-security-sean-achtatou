package com.waps;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class OffersWebView extends Activity {
    String a = "";
    k b;
    /* access modifiers changed from: private */
    public WebView c = null;
    private String d = null;
    /* access modifiers changed from: private */
    public ProgressBar e;
    private Dialog f = null;
    private String g = "";
    private String h = "";
    private String i = "";
    private String j = "";
    /* access modifiers changed from: private */

    /* renamed from: k  reason: collision with root package name */
    public String f6k = "false";
    /* access modifiers changed from: private */
    public String l;
    /* access modifiers changed from: private */
    public boolean m = true;

    private void initMetaData(Bundle bundle) {
        if (bundle != null) {
            if (bundle.getString("Offers_URL") != null) {
                this.h = bundle.getString("Offers_URL");
            }
            if (bundle.getString("URL") != null) {
                this.h = bundle.getString("URL");
            }
            this.i = bundle.getString("URL_PARAMS");
            this.g = bundle.getString("CLIENT_PACKAGE");
            this.j = bundle.getString("USER_ID");
            this.f6k = bundle.getString("isFinshClose");
            this.i += "&publisher_user_id=" + this.j;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        initMetaData(getIntent().getExtras());
        if (this.h.indexOf("?") > -1) {
            this.d = this.h + this.i;
        } else {
            this.d = this.h + "?a=1" + this.i;
        }
        this.d = this.d.replaceAll(" ", "%20");
        super.onCreate(bundle);
        requestWindowFeature(1);
        int width = getWindowManager().getDefaultDisplay().getWidth();
        int height = getWindowManager().getDefaultDisplay().getHeight();
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setOrientation(1);
        linearLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        RelativeLayout relativeLayout = new RelativeLayout(this);
        relativeLayout.setGravity(17);
        this.c = new WebView(this);
        this.e = new ProgressBar(this);
        this.e.setEnabled(true);
        this.e.setVisibility(0);
        this.e.setPadding((width / 2) - 20, (height / 2) - 30, 0, 0);
        relativeLayout.addView(this.c, layoutParams);
        relativeLayout.addView(this.e, new ViewGroup.LayoutParams(-2, -2));
        linearLayout.addView(relativeLayout, new ViewGroup.LayoutParams(-1, -1));
        setContentView(linearLayout);
        this.c.setWebViewClient(new r(this, null));
        this.c.getSettings().setJavaScriptEnabled(true);
        WebView webView = this.c;
        WebView.enablePlatformNotifications();
        this.c.setHttpAuthUsernamePassword("10.0.0.172", "", "", "");
        if (this.f6k != null && "true".equals(this.f6k)) {
            Toast.makeText(this, "加载中,请稍候...", 0).show();
        }
        this.c.loadUrl(this.d);
        this.c.setDownloadListener(new q(this));
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.c.destroy();
        finish();
        super.onDestroy();
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4 || !this.c.canGoBack()) {
            return super.onKeyDown(i2, keyEvent);
        }
        if (!this.m) {
            finish();
            this.m = true;
        }
        this.c.goBack();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        if (!(this.d == null || this.c == null)) {
            this.c.loadUrl(this.d);
        }
        super.onResume();
    }
}
