package k.microcells.androidwallpaper;

public final class R {

    public static final class array {
        public static final int bj_name = 2131034118;
        public static final int bj_value = 2131034119;
        public static final int entries = 2131034112;
        public static final int textsize = 2131034116;
        public static final int textsize_value = 2131034117;
        public static final int textspeed = 2131034114;
        public static final int textspeed_value = 2131034115;
        public static final int values = 2131034113;
    }

    public static final class attr {
    }

    public static final class drawable {
        public static final int a1 = 2130837504;
        public static final int a4 = 2130837505;
        public static final int a8 = 2130837506;
        public static final int a9 = 2130837507;
        public static final int b1 = 2130837508;
        public static final int b4 = 2130837509;
        public static final int b8 = 2130837510;
        public static final int b9 = 2130837511;
        public static final int bg1 = 2130837512;
        public static final int bg2 = 2130837513;
        public static final int bg3 = 2130837514;
        public static final int bg4 = 2130837515;
        public static final int bg5 = 2130837516;
        public static final int c1 = 2130837517;
        public static final int c4 = 2130837518;
        public static final int c8 = 2130837519;
        public static final int c9 = 2130837520;
        public static final int d1 = 2130837521;
        public static final int d4 = 2130837522;
        public static final int d8 = 2130837523;
        public static final int d9 = 2130837524;
        public static final int droid = 2130837525;
        public static final int droid_icon = 2130837526;
        public static final int droid_icon_1 = 2130837527;
        public static final int icon = 2130837528;
        public static final int smalllogo = 2130837529;
    }

    public static final class id {
        public static final int LinearLayout01 = 2131165184;
        public static final int OfferProgressBar = 2131165187;
        public static final int RelativeLayout01 = 2131165185;
        public static final int ScrollView = 2131165196;
        public static final int app = 2131165189;
        public static final int appIcon = 2131165190;
        public static final int description = 2131165194;
        public static final int feedback_age_spinner = 2131165202;
        public static final int feedback_content = 2131165201;
        public static final int feedback_gender_spinner = 2131165203;
        public static final int feedback_submit = 2131165204;
        public static final int feedback_title = 2131165200;
        public static final int feedback_umeng_title = 2131165199;
        public static final int notification = 2131165192;
        public static final int offersWebView = 2131165186;
        public static final int proTV = 2131165188;
        public static final int progress_bar = 2131165195;
        public static final int progress_text = 2131165191;
        public static final int rootId = 2131165197;
        public static final int title = 2131165193;
        public static final int umengBannerTop = 2131165198;
    }

    public static final class layout {
        public static final int main = 2130903040;
        public static final int offers_web_view = 2130903041;
        public static final int umeng_download_notification = 2130903042;
        public static final int umeng_feedback = 2130903043;
    }

    public static final class string {
        public static final int action_setting = 2131099655;
        public static final int action_setting_summary = 2131099656;
        public static final int app_name = 2131099649;
        public static final int bj_setting = 2131099660;
        public static final int close = 2131099659;
        public static final int description = 2131099654;
        public static final int hello = 2131099648;
        public static final int open = 2131099658;
        public static final int preference_key = 2131099653;
        public static final int settings = 2131099650;
        public static final int settings_summary = 2131099652;
        public static final int settings_title = 2131099651;
        public static final int snow_size_setting_summary = 2131099661;
        public static final int wallpaper_settings = 2131099657;
    }

    public static final class xml {
        public static final int livewallpaper = 2130968576;
        public static final int settings = 2130968577;
    }
}
