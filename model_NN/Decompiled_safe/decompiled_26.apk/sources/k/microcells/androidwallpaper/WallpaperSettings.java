package k.microcells.androidwallpaper;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.widget.Toast;
import com.mobclick.android.MobclickAgent;
import com.waps.AppConnect;
import com.waps.UpdatePointsNotifier;

public class WallpaperSettings extends PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener, UpdatePointsNotifier {
    SharedPreferences ActiveSignal;
    CheckBoxPreference checkBoxPreference;
    int getPoint;
    int isActive;
    String key;
    boolean needKey;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getPreferenceManager().setSharedPreferencesName(MyAndroidWallpaper.SHARED_PREFS_NAME);
        addPreferencesFromResource(R.xml.settings);
        getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
        MobclickAgent.update(this);
        MobclickAgent.setUpdateOnlyWifi(false);
        MobclickAgent.setUpdateOnlyWifi(false);
        AppConnect.getInstance(this);
        AppConnect.getInstance(this).getPoints(this);
        MobclickAgent.updateOnlineConfig(this);
        this.needKey = true;
        getKey(this.needKey);
        this.ActiveSignal = getSharedPreferences("SETTING_INFOS", 0);
        this.isActive = this.ActiveSignal.getInt("ActiveSignal", 0);
        this.checkBoxPreference = (CheckBoxPreference) findPreference("mode");
        this.checkBoxPreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference arg0) {
                WallpaperSettings.this.getKey(WallpaperSettings.this.needKey);
                if (WallpaperSettings.this.needKey) {
                    if (WallpaperSettings.this.key.equals("1") && WallpaperSettings.this.isActive == 0) {
                        WallpaperSettings.this.activeAPP();
                        WallpaperSettings.this.checkBoxPreference.setChecked(false);
                    }
                } else if (WallpaperSettings.this.isActive == 0) {
                    WallpaperSettings.this.activeAPP();
                    WallpaperSettings.this.checkBoxPreference.setChecked(false);
                }
                return false;
            }
        });
    }

    public void getKey(boolean NeedKey) {
        if (NeedKey) {
            this.key = MobclickAgent.getConfigParams(this, "active");
        }
    }

    /* access modifiers changed from: private */
    public void activeAPP() {
        try {
            AppConnect.getInstance(this).getPoints(this);
            new AlertDialog.Builder(this).setTitle("温馨提示").setMessage("亲爱的朋友，你好，本软件有效期已过，需要重新激活后才能继续使用，激活软件需100积分，只需激活一次，请按“免费获取积分”按钮进入精品应用推荐，下载精品应用后使用1次即可获得对应的积分奖励，快来激活吧！\n(你现在拥有积分:" + this.getPoint + ",激活后可永久免费使用，如果因网络延时引起积分显示不正确，可以点击“激活软件”后，稍候再试即可！)").setPositiveButton("激活软件", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    WallpaperSettings.this.getActive();
                }
            }).setNegativeButton("获取积分", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    WallpaperSettings.this.showApplist();
                }
            }).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showApplist() {
        AppConnect.getInstance(this).showOffers(this);
    }

    public void getActive() {
        if (this.getPoint >= 100) {
            MobclickAgent.onEvent(this, "active");
            this.isActive = 1;
            AppConnect.getInstance(this).spendPoints(100, this);
            this.ActiveSignal.edit().putInt("ActiveSignal", this.isActive).commit();
            Toast.makeText(this, "软件激活成功!", 1).show();
            return;
        }
        Toast.makeText(this, "积分不够，激活软件失败，请重新使用免费获取积分!", 1).show();
        activeAPP();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
        super.onDestroy();
        AppConnect.getInstance(this).finalize();
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key2) {
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

    public void onResume() {
        super.onResume();
        MobclickAgent.setUpdateOnlyWifi(false);
        MobclickAgent.setUpdateOnlyWifi(false);
        AppConnect.getInstance(this).getPoints(this);
        MobclickAgent.onResume(this);
    }

    public void getUpdatePoints(String arg0, int arg1) {
        this.getPoint = arg1;
    }

    public void getUpdatePointsFailed(String arg0) {
    }
}
