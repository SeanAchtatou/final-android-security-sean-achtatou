package k.microcells.androidwallpaper;

import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Handler;
import android.service.wallpaper.WallpaperService;
import android.util.Log;
import android.view.SurfaceHolder;

public class MyAndroidWallpaper extends WallpaperService {
    public static final String SHARED_PREFS_NAME = "k.microcells.androidwallpaper";

    public void onCreate() {
        super.onCreate();
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public WallpaperService.Engine onCreateEngine() {
        return new WallpaperEngine(getResources());
    }

    public class WallpaperEngine extends WallpaperService.Engine implements SharedPreferences.OnSharedPreferenceChangeListener {
        private Bitmap background;
        private Bitmap[] bgImage = new Bitmap[10];
        int bgShow;
        private int bg_height;
        private int bg_width;
        private final Runnable drawThread = new Runnable() {
            public void run() {
                WallpaperEngine.this.drawFrame();
            }
        };
        private final Handler handler = new Handler();
        private int height;
        private Bitmap image;
        private Bitmap image01;
        private Bitmap image02;
        private Bitmap image03;
        private Bitmap image04;
        private Bitmap image05;
        private Bitmap image06;
        private Bitmap image07;
        private Bitmap image08;
        private Bitmap[] imageIds;
        private int image_dir = 0;
        private int image_id = 0;
        private int image_load = 0;
        Boolean mode;
        int people;
        SharedPreferences prefs;
        private int px = 0;
        private int py = 0;
        private int ramdon;
        int ramdonText;
        int showAction;
        int showText;
        private int speedLevel = 200;
        private int step;
        String[] str = new String[40];
        int textLen;
        int textSize = 20;
        int text_timer;
        int text_x;
        int textspeed = 150;
        int timer;
        private int totalstep;
        private boolean visible;
        private int vx = 10;
        private int vy = 10;
        private int width;

        public WallpaperEngine(Resources r) {
            super(MyAndroidWallpaper.this);
            this.prefs = MyAndroidWallpaper.this.getSharedPreferences(MyAndroidWallpaper.SHARED_PREFS_NAME, 0);
            this.prefs.registerOnSharedPreferenceChangeListener(this);
            onSharedPreferenceChanged(this.prefs, null);
            Bitmap[] imageIds2 = {this.image01, this.image02, this.image03, this.image04, this.image05, this.image06, this.image07, this.image08};
            Integer[] photoIds = {Integer.valueOf((int) R.drawable.a4), Integer.valueOf((int) R.drawable.a8), Integer.valueOf((int) R.drawable.b4), Integer.valueOf((int) R.drawable.b8), Integer.valueOf((int) R.drawable.c4), Integer.valueOf((int) R.drawable.c8), Integer.valueOf((int) R.drawable.d4), Integer.valueOf((int) R.drawable.d8)};
            setImageIds(imageIds2);
            this.str[0] = "下雨了，快收衣服啦!";
            this.str[1] = "好困啊!";
            this.str[2] = "早上好!";
            this.str[3] = "你吃饭了吗?";
            this.str[4] = "你信不信？反正我是信了!";
            this.str[5] = "给力啊!";
            this.str[6] = "我是小蘑菇点点";
            this.str[7] = "我的地盘我做主";
            this.str[8] = "累了，休息一下咯";
            this.str[9] = "心情真好,有木有";
            this.str[10] = "给你唱个歌吧";
            for (int i = 0; i < photoIds.length; i++) {
                imageIds2[i] = BitmapFactory.decodeResource(r, photoIds[i].intValue());
            }
            this.bgImage[0] = BitmapFactory.decodeResource(r, R.drawable.bg1);
            this.bgImage[1] = BitmapFactory.decodeResource(r, R.drawable.bg2);
            this.bgImage[2] = BitmapFactory.decodeResource(r, R.drawable.bg3);
            this.bgImage[3] = BitmapFactory.decodeResource(r, R.drawable.bg4);
            this.bgImage[4] = BitmapFactory.decodeResource(r, R.drawable.bg5);
            this.ramdon = Integer.parseInt(String.valueOf(Math.round((Math.random() * 15.0d) + 2.0d)));
            this.totalstep = this.ramdon;
            this.image = BitmapFactory.decodeResource(r, R.drawable.a1);
            this.background = BitmapFactory.decodeResource(r, R.drawable.bg1);
            this.px = this.image.getWidth() / 2;
            this.py = this.image.getHeight() / 2;
            this.bg_width = this.background.getWidth();
            this.bg_height = this.background.getHeight();
        }

        public void onCreate(SurfaceHolder surfaceHolder) {
            super.onCreate(surfaceHolder);
        }

        public void onDestroy() {
            super.onDestroy();
            this.handler.removeCallbacks(this.drawThread);
        }

        public void onVisibilityChanged(boolean visible2) {
            this.visible = visible2;
            if (visible2) {
                drawFrame();
            } else {
                this.handler.removeCallbacks(this.drawThread);
            }
        }

        public void onSurfaceChanged(SurfaceHolder holder, int format, int width2, int height2) {
            super.onSurfaceChanged(holder, format, width2, height2);
            this.width = width2;
            this.height = height2;
            drawFrame();
        }

        public void onSurfaceCreated(SurfaceHolder holder) {
            super.onSurfaceCreated(holder);
        }

        public void onSurfaceDestroyed(SurfaceHolder holder) {
            super.onSurfaceDestroyed(holder);
            this.visible = false;
            this.handler.removeCallbacks(this.drawThread);
        }

        public void onOffsetsChanged(float xOffset, float yOffset, float xStep, float yStep, int xPixels, int yPixels) {
            drawFrame();
        }

        /* access modifiers changed from: private */
        public void drawFrame() {
            SurfaceHolder holder = getSurfaceHolder();
            Canvas c = holder.lockCanvas();
            if (this.mode.booleanValue()) {
                c.drawBitmap(this.bgImage[this.bgShow], 0.0f, 0.0f, (Paint) null);
                this.timer += this.speedLevel / 100;
                if (this.timer > 400) {
                    this.timer = 0;
                    this.showAction = Integer.parseInt(String.valueOf(Math.round((Math.random() * 2.0d) + 1.0d))) - 1;
                }
                this.text_timer += this.speedLevel / 100;
                if (this.text_timer > this.textspeed) {
                    if (this.showText == 0) {
                        this.showText = 1;
                        this.ramdonText = Integer.parseInt(String.valueOf(Math.round(Math.random() * 9.0d)));
                    } else {
                        this.showText = 0;
                    }
                    this.text_timer = 0;
                }
                Paint paint = new Paint();
                paint.setAntiAlias(true);
                paint.setTextSize((float) this.textSize);
                paint.setColor(-16777216);
                changeAction(1);
                if (this.showText == 1) {
                    this.textLen = (int) paint.measureText(this.str[this.ramdonText]);
                    this.text_x = this.px - (this.textLen / 2);
                    if (this.text_x < 0) {
                        this.text_x = 0;
                    }
                    if (this.px + (this.textLen / 2) > this.width) {
                        this.text_x = this.width - this.textLen;
                    }
                    c.drawText(this.str[this.ramdonText], (float) this.text_x, (float) ((this.py - (this.image.getHeight() / 2)) - 10), paint);
                }
                c.drawBitmap(this.imageIds[this.image_load], (float) (this.px - (this.image.getWidth() / 2)), (float) (this.py - (this.image.getHeight() / 2)), (Paint) null);
            } else {
                c.drawBitmap(this.bgImage[0], 0.0f, 0.0f, (Paint) null);
            }
            holder.unlockCanvasAndPost(c);
            Log.e("TEST", "=" + this.image_load);
            this.handler.removeCallbacks(this.drawThread);
            if (this.visible) {
                this.handler.postDelayed(this.drawThread, (long) this.speedLevel);
            }
        }

        public void changeAction(int action) {
            if (this.step <= this.totalstep) {
                this.step++;
            } else {
                this.step = 0;
                this.totalstep = Integer.parseInt(String.valueOf(Math.round((Math.random() * 5.0d) + 5.0d)));
                this.ramdon = Integer.parseInt(String.valueOf(Math.round((Math.random() * 2.0d) + 1.0d)));
                if (this.ramdon == 1) {
                    this.vx = -this.vx;
                }
                this.ramdon = Integer.parseInt(String.valueOf(Math.round((Math.random() * 2.0d) + 1.0d)));
                if (this.ramdon == 1) {
                    this.vy = -this.vy;
                }
            }
            if (this.image_id == 1) {
                this.image_id = 0;
            } else {
                this.image_id = 1;
            }
            if (this.px < this.image.getWidth()) {
                this.vx = 10;
            }
            if (this.width - this.image.getWidth() < this.px) {
                this.vx = -10;
            }
            if (this.py < this.image.getHeight()) {
                this.vy = 10;
            }
            if (this.height - this.image.getHeight() < this.py) {
                this.vy = -10;
            }
            this.px += this.vx;
            this.py += this.vy;
            if (this.vx > 0 && this.vy > 0) {
                this.image_dir = 6;
            }
            if (this.vx > 0 && this.vy < 0) {
                this.image_dir = 4;
            }
            if (this.vx < 0 && this.vy < 0) {
                this.image_dir = 2;
            }
            if (this.vx < 0 && this.vy > 0) {
                this.image_dir = 0;
            }
            this.image_load = this.image_dir + this.image_id;
        }

        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            this.speedLevel = Integer.parseInt(this.prefs.getString(MyAndroidWallpaper.this.getResources().getString(R.string.preference_key), "200"));
            this.mode = Boolean.valueOf(this.prefs.getBoolean("mode", false));
            this.bgShow = Integer.parseInt(this.prefs.getString("bj", "1"));
            this.textSize = Integer.parseInt(this.prefs.getString("textsize", "20"));
            this.textspeed = Integer.parseInt(this.prefs.getString("textspeed", "150"));
        }

        public void setImageIds(Bitmap[] imageIds2) {
            this.imageIds = imageIds2;
        }

        public Bitmap[] getImageIds() {
            return this.imageIds;
        }
    }
}
