package com.waps;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.view.View;
import android.widget.Toast;
import java.io.File;

public class l {
    private Notification a;
    private NotificationManager b;
    private Context c;
    private String d;

    public l(Context context) {
        this.c = context;
    }

    public void a(int i, String str) {
        File fileStreamPath;
        this.b.cancel(i);
        if (!Environment.getExternalStorageState().equals("mounted") && (fileStreamPath = this.c.getFileStreamPath(str)) != null) {
            fileStreamPath.delete();
            Toast.makeText(this.c, str + "已经被删除", 1).show();
        }
    }

    public void a(View view, String str, int i, String str2) {
        this.a = new Notification();
        this.a.icon = 17301633;
        this.a.tickerText = "正在下载";
        this.a.when = System.currentTimeMillis();
        this.a.flags = 16;
        this.a.setLatestEventInfo(this.c, str, "正在下载，已完成  " + str2 + "", PendingIntent.getActivity(this.c, 100, new Intent(), 0));
        this.b = (NotificationManager) this.c.getSystemService("notification");
        this.b.notify(i, this.a);
    }

    public void a(View view, String str, int i, String str2, String str3) {
        this.d = str2;
        this.a = new Notification();
        this.a.icon = 17301634;
        this.a.tickerText = "";
        this.a.when = System.currentTimeMillis();
        this.a.defaults = 1;
        this.a.flags = 16;
        Intent intent = new Intent();
        intent.setAction("android.intent.action.VIEW");
        if (Environment.getExternalStorageState().equals("mounted")) {
            intent.setDataAndType(Uri.fromFile(new File("/sdcard/download/" + str)), "application/vnd.android.package-archive");
        } else {
            intent.setDataAndType(Uri.fromFile(this.c.getFileStreamPath(str)), "application/vnd.android.package-archive");
        }
        this.a.setLatestEventInfo(this.c, str, str3, PendingIntent.getActivity(this.c, 100, intent, 0));
        this.b = (NotificationManager) this.c.getSystemService("notification");
        this.b.notify(i, this.a);
    }
}
