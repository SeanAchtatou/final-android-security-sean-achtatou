package com.waps;

import android.content.Context;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.view.View;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import org.apache.http.HttpHost;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;

public class k extends AsyncTask {
    private static Context m;
    l a;
    m b;
    int c;
    n d;
    String e = "";
    String f = "";
    float g = 0.0f;
    float h = 0.0f;
    NumberFormat i = new DecimalFormat("#0");
    float j;
    private String k;
    private View l;

    public k(Context context, View view, String str) {
        m = context;
        this.l = view;
        this.k = str;
        this.e = str.substring(str.indexOf("http://") + 7, str.indexOf("/", str.indexOf("http://") + 8));
        this.f = str.substring(0, str.indexOf("/", str.indexOf("http://") + 8));
        this.a = new l(m);
        this.d = new n();
        Context context2 = m;
        Context context3 = m;
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context2.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo != null && activeNetworkInfo.getExtraInfo() != null && activeNetworkInfo.getExtraInfo().equals("cmwap")) {
            this.d.a(true);
        }
    }

    private String a(String str) {
        return str.substring(str.lastIndexOf("/") + 1);
    }

    private long b(String str) {
        if (!this.d.a()) {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
            httpURLConnection.setConnectTimeout(5000);
            httpURLConnection.setRequestMethod("GET");
            return (long) httpURLConnection.getContentLength();
        }
        HttpHost httpHost = new HttpHost("10.0.0.172", 80, "http");
        HttpHost httpHost2 = new HttpHost(this.e, 80, "http");
        HttpGet httpGet = new HttpGet(str.replaceAll(" ", "%20").replaceFirst(this.f, ""));
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, 15000);
        HttpConnectionParams.setSoTimeout(basicHttpParams, 30000);
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient(basicHttpParams);
        defaultHttpClient.getParams().setParameter("http.route.default-proxy", httpHost);
        return defaultHttpClient.execute(httpHost2, httpGet).getEntity().getContentLength();
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x01fc A[SYNTHETIC, Splitter:B:62:0x01fc] */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x0201 A[Catch:{ Exception -> 0x0205 }] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String doInBackground(java.lang.String... r14) {
        /*
            r13 = this;
            r8 = 0
            r12 = -1
            long r0 = java.lang.System.currentTimeMillis()
            int r0 = (int) r0
            r13.c = r0
            com.waps.l r0 = r13.a
            android.view.View r1 = r13.l
            java.lang.String r2 = "正在获取文件名..."
            int r3 = r13.c
            java.lang.String r4 = "0 %"
            r0.a(r1, r2, r3, r4)
            java.lang.String r0 = r13.k     // Catch:{ Exception -> 0x0211, all -> 0x01f7 }
            java.lang.String r2 = r13.a(r0)     // Catch:{ Exception -> 0x0211, all -> 0x01f7 }
            java.lang.String r6 = "/sdcard/download/"
            java.lang.String r0 = r13.k     // Catch:{ Exception -> 0x0211, all -> 0x01f7 }
            long r0 = r13.b(r0)     // Catch:{ Exception -> 0x0211, all -> 0x01f7 }
            float r0 = (float) r0     // Catch:{ Exception -> 0x0211, all -> 0x01f7 }
            r13.g = r0     // Catch:{ Exception -> 0x0211, all -> 0x01f7 }
            com.waps.n r0 = r13.d     // Catch:{ Exception -> 0x0211, all -> 0x01f7 }
            boolean r0 = r0.a()     // Catch:{ Exception -> 0x0211, all -> 0x01f7 }
            if (r0 != 0) goto L_0x0126
            org.apache.http.impl.client.DefaultHttpClient r0 = new org.apache.http.impl.client.DefaultHttpClient     // Catch:{ Exception -> 0x0211, all -> 0x01f7 }
            r0.<init>()     // Catch:{ Exception -> 0x0211, all -> 0x01f7 }
            org.apache.http.client.methods.HttpGet r1 = new org.apache.http.client.methods.HttpGet     // Catch:{ Exception -> 0x0211, all -> 0x01f7 }
            r3 = 0
            r3 = r14[r3]     // Catch:{ Exception -> 0x0211, all -> 0x01f7 }
            java.lang.String r4 = " "
            java.lang.String r5 = "%20"
            java.lang.String r3 = r3.replaceAll(r4, r5)     // Catch:{ Exception -> 0x0211, all -> 0x01f7 }
            r1.<init>(r3)     // Catch:{ Exception -> 0x0211, all -> 0x01f7 }
            org.apache.http.HttpResponse r0 = r0.execute(r1)     // Catch:{ Exception -> 0x0211, all -> 0x01f7 }
        L_0x0048:
            org.apache.http.HttpEntity r0 = r0.getEntity()     // Catch:{ Exception -> 0x0211, all -> 0x01f7 }
            java.io.InputStream r7 = r0.getContent()     // Catch:{ Exception -> 0x0211, all -> 0x01f7 }
            java.lang.String r0 = android.os.Environment.getExternalStorageState()     // Catch:{ Exception -> 0x0215, all -> 0x0207 }
            java.lang.String r1 = "mounted"
            boolean r0 = r0.equals(r1)     // Catch:{ Exception -> 0x0215, all -> 0x0207 }
            if (r0 == 0) goto L_0x0177
            java.io.File r0 = new java.io.File     // Catch:{ Exception -> 0x0215, all -> 0x0207 }
            r0.<init>(r6)     // Catch:{ Exception -> 0x0215, all -> 0x0207 }
            java.io.File r1 = new java.io.File     // Catch:{ Exception -> 0x0215, all -> 0x0207 }
            r1.<init>(r6, r2)     // Catch:{ Exception -> 0x0215, all -> 0x0207 }
            boolean r3 = r0.exists()     // Catch:{ Exception -> 0x0215, all -> 0x0207 }
            if (r3 != 0) goto L_0x006f
            r0.mkdir()     // Catch:{ Exception -> 0x0215, all -> 0x0207 }
        L_0x006f:
            boolean r0 = r1.exists()     // Catch:{ Exception -> 0x0215, all -> 0x0207 }
            if (r0 != 0) goto L_0x0078
            r1.createNewFile()     // Catch:{ Exception -> 0x0215, all -> 0x0207 }
        L_0x0078:
            java.io.FileOutputStream r0 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0215, all -> 0x0207 }
            r0.<init>(r1)     // Catch:{ Exception -> 0x0215, all -> 0x0207 }
            r8 = r0
        L_0x007e:
            if (r7 == 0) goto L_0x0119
            r0 = 51200(0xc800, float:7.1746E-41)
            byte[] r9 = new byte[r0]     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
        L_0x0085:
            int r0 = r7.read(r9)     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            if (r0 == r12) goto L_0x00b8
            java.text.NumberFormat r1 = r13.i     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            float r3 = r13.j     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            double r3 = (double) r3     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            java.lang.String r1 = r1.format(r3)     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            int r1 = java.lang.Integer.parseInt(r1)     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            r3 = 100
            if (r1 <= r3) goto L_0x0181
            com.waps.l r0 = r13.a     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            android.view.View r1 = r13.l     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            int r3 = r13.c     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            r4.<init>()     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            java.lang.StringBuilder r4 = r4.append(r2)     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            java.lang.String r5 = "下载失败，请重新下载"
            r0.a(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
        L_0x00b8:
            int r0 = r7.read(r9)     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            r3 = 1000(0x3e8, double:4.94E-321)
            java.lang.Thread.sleep(r3)     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            if (r0 != r12) goto L_0x0119
            java.lang.String r0 = ""
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            r0.<init>()     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            java.lang.StringBuilder r0 = r0.append(r6)     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            java.lang.String r4 = r0.toString()     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            java.lang.String r0 = android.os.Environment.getExternalStorageState()     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            java.lang.String r1 = "mounted"
            boolean r0 = r0.equals(r1)     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            if (r0 == 0) goto L_0x01ee
            java.io.File r0 = new java.io.File     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            r0.<init>(r4)     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            r6 = r0
        L_0x00e8:
            com.waps.l r0 = r13.a     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            android.view.View r1 = r13.l     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            int r3 = r13.c     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            java.lang.String r5 = "下载完成,点击安装"
            r0.a(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            android.content.Intent r0 = new android.content.Intent     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            r0.<init>()     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            java.lang.String r1 = "android.intent.action.VIEW"
            r0.setAction(r1)     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            android.net.Uri r1 = android.net.Uri.fromFile(r6)     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            java.lang.String r3 = "application/vnd.android.package-archive"
            r0.setDataAndType(r1, r3)     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            android.content.Context r1 = com.waps.k.m     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            r1.startActivity(r0)     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            com.waps.m r0 = new com.waps.m     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            com.waps.l r1 = r13.a     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            int r3 = r13.c     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            r0.<init>(r1, r3, r2)     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            r13.b = r0     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            r13.a()     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
        L_0x0119:
            if (r7 == 0) goto L_0x011e
            r7.close()     // Catch:{ Exception -> 0x0219 }
        L_0x011e:
            if (r8 == 0) goto L_0x0123
            r8.close()     // Catch:{ Exception -> 0x0219 }
        L_0x0123:
            java.lang.String r0 = ""
            return r0
        L_0x0126:
            r0 = 0
            r0 = r14[r0]     // Catch:{ Exception -> 0x0211, all -> 0x01f7 }
            org.apache.http.HttpHost r1 = new org.apache.http.HttpHost     // Catch:{ Exception -> 0x0211, all -> 0x01f7 }
            java.lang.String r3 = "10.0.0.172"
            r4 = 80
            java.lang.String r5 = "http"
            r1.<init>(r3, r4, r5)     // Catch:{ Exception -> 0x0211, all -> 0x01f7 }
            org.apache.http.HttpHost r3 = new org.apache.http.HttpHost     // Catch:{ Exception -> 0x0211, all -> 0x01f7 }
            java.lang.String r4 = r13.e     // Catch:{ Exception -> 0x0211, all -> 0x01f7 }
            r5 = 80
            java.lang.String r7 = "http"
            r3.<init>(r4, r5, r7)     // Catch:{ Exception -> 0x0211, all -> 0x01f7 }
            java.lang.String r4 = " "
            java.lang.String r5 = "%20"
            java.lang.String r0 = r0.replaceAll(r4, r5)     // Catch:{ Exception -> 0x0211, all -> 0x01f7 }
            java.lang.String r4 = r13.f     // Catch:{ Exception -> 0x0211, all -> 0x01f7 }
            java.lang.String r5 = ""
            java.lang.String r0 = r0.replaceFirst(r4, r5)     // Catch:{ Exception -> 0x0211, all -> 0x01f7 }
            org.apache.http.client.methods.HttpGet r4 = new org.apache.http.client.methods.HttpGet     // Catch:{ Exception -> 0x0211, all -> 0x01f7 }
            r4.<init>(r0)     // Catch:{ Exception -> 0x0211, all -> 0x01f7 }
            org.apache.http.params.BasicHttpParams r0 = new org.apache.http.params.BasicHttpParams     // Catch:{ Exception -> 0x0211, all -> 0x01f7 }
            r0.<init>()     // Catch:{ Exception -> 0x0211, all -> 0x01f7 }
            r5 = 15000(0x3a98, float:2.102E-41)
            org.apache.http.params.HttpConnectionParams.setConnectionTimeout(r0, r5)     // Catch:{ Exception -> 0x0211, all -> 0x01f7 }
            r5 = 30000(0x7530, float:4.2039E-41)
            org.apache.http.params.HttpConnectionParams.setSoTimeout(r0, r5)     // Catch:{ Exception -> 0x0211, all -> 0x01f7 }
            org.apache.http.impl.client.DefaultHttpClient r5 = new org.apache.http.impl.client.DefaultHttpClient     // Catch:{ Exception -> 0x0211, all -> 0x01f7 }
            r5.<init>(r0)     // Catch:{ Exception -> 0x0211, all -> 0x01f7 }
            org.apache.http.params.HttpParams r0 = r5.getParams()     // Catch:{ Exception -> 0x0211, all -> 0x01f7 }
            java.lang.String r7 = "http.route.default-proxy"
            r0.setParameter(r7, r1)     // Catch:{ Exception -> 0x0211, all -> 0x01f7 }
            org.apache.http.HttpResponse r0 = r5.execute(r3, r4)     // Catch:{ Exception -> 0x0211, all -> 0x01f7 }
            goto L_0x0048
        L_0x0177:
            android.content.Context r0 = com.waps.k.m     // Catch:{ Exception -> 0x0215, all -> 0x0207 }
            r1 = 3
            java.io.FileOutputStream r0 = r0.openFileOutput(r2, r1)     // Catch:{ Exception -> 0x0215, all -> 0x0207 }
            r8 = r0
            goto L_0x007e
        L_0x0181:
            r1 = 0
            r8.write(r9, r1, r0)     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            float r1 = r13.h     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            float r0 = (float) r0     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            float r0 = r0 + r1
            r13.h = r0     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            float r0 = r13.h     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            float r1 = r13.g     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            float r0 = r0 / r1
            r1 = 1120403456(0x42c80000, float:100.0)
            float r0 = r0 * r1
            r13.j = r0     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            r0 = 1
            java.lang.Integer[] r0 = new java.lang.Integer[r0]     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            r1 = 0
            float r3 = r13.h     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            float r4 = r13.g     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            float r3 = r3 / r4
            int r3 = (int) r3     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            int r3 = r3 * 100
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            r0[r1] = r3     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            r13.publishProgress(r0)     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            java.text.NumberFormat r0 = r13.i     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            float r1 = r13.j     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            double r3 = (double) r1     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            r0.format(r3)     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            com.waps.l r0 = r13.a     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            android.view.View r1 = r13.l     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            int r3 = r13.c     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            r4.<init>()     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            java.text.NumberFormat r5 = r13.i     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            float r10 = r13.j     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            double r10 = (double) r10     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            java.lang.String r5 = r5.format(r10)     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            java.lang.String r5 = " %"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            r0.a(r1, r2, r3, r4)     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            goto L_0x0085
        L_0x01d9:
            r0 = move-exception
            r1 = r8
            r2 = r7
        L_0x01dc:
            r0.printStackTrace()     // Catch:{ all -> 0x020f }
            if (r2 == 0) goto L_0x01e4
            r2.close()     // Catch:{ Exception -> 0x01eb }
        L_0x01e4:
            if (r1 == 0) goto L_0x0123
            r1.close()     // Catch:{ Exception -> 0x01eb }
            goto L_0x0123
        L_0x01eb:
            r0 = move-exception
            goto L_0x0123
        L_0x01ee:
            android.content.Context r0 = com.waps.k.m     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            java.io.File r0 = r0.getFileStreamPath(r2)     // Catch:{ Exception -> 0x01d9, all -> 0x020b }
            r6 = r0
            goto L_0x00e8
        L_0x01f7:
            r0 = move-exception
            r1 = r8
            r2 = r8
        L_0x01fa:
            if (r2 == 0) goto L_0x01ff
            r2.close()     // Catch:{ Exception -> 0x0205 }
        L_0x01ff:
            if (r1 == 0) goto L_0x0204
            r1.close()     // Catch:{ Exception -> 0x0205 }
        L_0x0204:
            throw r0
        L_0x0205:
            r1 = move-exception
            goto L_0x0204
        L_0x0207:
            r0 = move-exception
            r1 = r8
            r2 = r7
            goto L_0x01fa
        L_0x020b:
            r0 = move-exception
            r1 = r8
            r2 = r7
            goto L_0x01fa
        L_0x020f:
            r0 = move-exception
            goto L_0x01fa
        L_0x0211:
            r0 = move-exception
            r1 = r8
            r2 = r8
            goto L_0x01dc
        L_0x0215:
            r0 = move-exception
            r1 = r8
            r2 = r7
            goto L_0x01dc
        L_0x0219:
            r0 = move-exception
            goto L_0x0123
        */
        throw new UnsupportedOperationException("Method not decompiled: com.waps.k.doInBackground(java.lang.String[]):java.lang.String");
    }

    /* access modifiers changed from: protected */
    public void a() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.PACKAGE_ADDED");
        intentFilter.addDataScheme("package");
        m.registerReceiver(this.b, intentFilter);
    }
}
