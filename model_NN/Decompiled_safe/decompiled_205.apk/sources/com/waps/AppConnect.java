package com.waps;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.os.Looper;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.util.List;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public final class AppConnect {
    public static final String LIBRARY_VERSION_NUMBER = "1.4.3";
    /* access modifiers changed from: private */
    public static boolean U = true;
    /* access modifiers changed from: private */
    public static boolean V = false;
    private static String W = null;
    /* access modifiers changed from: private */
    public static UpdatePointsNotifier Z;
    /* access modifiers changed from: private */
    public static String ae = "";
    /* access modifiers changed from: private */
    public static String af = "receiver/install?";
    /* access modifiers changed from: private */
    public static String ag = "install";
    private static AppConnect w = null;
    /* access modifiers changed from: private */
    public static n x = null;
    private static DisplayAd y = null;
    private String A = "";
    private String B = "";
    private String C = "";
    private String D = "";
    private String E = "";
    private String F = "";
    /* access modifiers changed from: private */
    public String G = "";
    private String H = "";
    private String I = "";
    /* access modifiers changed from: private */
    public String J = "";
    private String K = "http://app.wapx.cn/action/account/offerlist?";
    private String L = "http://app.wapx.cn/action/account/ownslist?";
    /* access modifiers changed from: private */
    public String M = "";
    /* access modifiers changed from: private */
    public String N = "";
    /* access modifiers changed from: private */
    public String O = "";
    /* access modifiers changed from: private */
    public String P = "";
    private String Q = "";
    private int R = 0;
    private int S = 0;
    /* access modifiers changed from: private */
    public String T = "";
    private String X = "";
    private String Y = "";
    final String a = "net";
    private f aa = null;
    private i ab = null;
    private g ac = null;
    /* access modifiers changed from: private */
    public h ad = null;
    private int ah;
    final String b = "imsi";
    final String c = "udid";
    final String d = "device_name";
    final String e = "device_type";
    final String f = "os_version";
    final String g = "country_code";
    final String h = "language";
    final String i = "app_id";
    final String j = "app_version";
    final String k = "sdk_version";
    final String l = "act";
    final String m = "userid";
    final String n = "channel";
    final String o = "points";
    final String p = "install";
    final String q = "uninstall";
    final String r = "load";
    final String s = "device_width";
    final String t = "device_height";
    private j u = null;
    /* access modifiers changed from: private */
    public Context v = null;
    /* access modifiers changed from: private */
    public String z = "";

    public AppConnect() {
    }

    private AppConnect(Context context) {
        this.N = getParams(context);
        if (Looper.myLooper() == null) {
            Looper.prepare();
        }
        this.u = new j(this, null);
        this.u.execute(new Void[0]);
    }

    public AppConnect(Context context, int i2) {
        this.N = getParams(context);
    }

    private AppConnect(Context context, String str) {
        this.v = context;
        this.N = getParams(context);
        this.N += "&userid=" + str;
        if (Looper.myLooper() == null) {
            Looper.prepare();
        }
        this.u = new j(this, null);
        this.u.execute(new Void[0]);
    }

    /* access modifiers changed from: private */
    public void UpdateDialog(String str) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(this.v);
            builder.setTitle("新版提示");
            builder.setMessage("有新版本(" + this.T + "),是否下载?");
            builder.setPositiveButton("下载", new d(this, str));
            builder.setNegativeButton("下次再说", new e(this));
            builder.show();
        } catch (Exception e2) {
        }
    }

    private Document buildDocument(String str) {
        try {
            DocumentBuilderFactory newInstance = DocumentBuilderFactory.newInstance();
            return newInstance.newDocumentBuilder().parse(new ByteArrayInputStream(str.getBytes("UTF-8")));
        } catch (Exception e2) {
            return null;
        }
    }

    public static String getArea() {
        int i2 = 0;
        while (W == null && i2 < 20) {
            try {
                Thread.sleep(100);
                i2++;
            } catch (InterruptedException e2) {
            }
        }
        return W;
    }

    public static AppConnect getInstance(Context context) {
        if (x == null) {
            x = new n();
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            if (!(activeNetworkInfo == null || activeNetworkInfo.getExtraInfo() == null || !activeNetworkInfo.getExtraInfo().equals("cmwap"))) {
                x.a(true);
            }
        }
        if (w == null) {
            w = new AppConnect(context);
        }
        if (y == null) {
            y = new DisplayAd(context);
        }
        return w;
    }

    public static AppConnect getInstance(Context context, int i2) {
        if (x == null) {
            x = new n();
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            if (!(activeNetworkInfo == null || activeNetworkInfo.getExtraInfo() == null || !activeNetworkInfo.getExtraInfo().equals("cmwap"))) {
                x.a(true);
            }
        }
        if (w == null) {
            w = new AppConnect(context, i2);
        }
        if (y == null) {
            y = new DisplayAd(context);
        }
        return w;
    }

    public static AppConnect getInstance(Context context, String str) {
        if (x == null) {
            x = new n();
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            if (!(activeNetworkInfo == null || activeNetworkInfo.getExtraInfo() == null || !activeNetworkInfo.getExtraInfo().equals("cmwap"))) {
                x.a(true);
            }
        }
        if (w == null) {
            w = new AppConnect(context, str);
        }
        if (y == null) {
            y = new DisplayAd(context);
        }
        return w;
    }

    public static AppConnect getInstanceNoConnect(Context context) {
        if (x == null) {
            x = new n();
        }
        if (w == null) {
            w = new AppConnect(context, 0);
        }
        if (y == null) {
            y = new DisplayAd(context);
        }
        return w;
    }

    private String getNodeTrimValue(NodeList nodeList) {
        Element element = (Element) nodeList.item(0);
        if (element == null) {
            return null;
        }
        NodeList childNodes = element.getChildNodes();
        int length = childNodes.getLength();
        String str = "";
        for (int i2 = 0; i2 < length; i2++) {
            Node item = childNodes.item(i2);
            if (item != null) {
                str = str + item.getNodeValue();
            }
        }
        if (str == null || str.equals("")) {
            return null;
        }
        return str.trim();
    }

    private void getPointsHelper() {
        this.aa = new f(this, null);
        this.aa.execute(new Void[0]);
    }

    /* access modifiers changed from: private */
    public boolean handleConnectResponse(String str) {
        try {
            Document buildDocument = buildDocument(str);
            if (!(buildDocument == null || buildDocument.getElementsByTagName("Version") == null)) {
                String nodeTrimValue = getNodeTrimValue(buildDocument.getElementsByTagName("Success"));
                String nodeTrimValue2 = getNodeTrimValue(buildDocument.getElementsByTagName("Version"));
                String nodeTrimValue3 = getNodeTrimValue(buildDocument.getElementsByTagName("Clear"));
                String nodeTrimValue4 = getNodeTrimValue(buildDocument.getElementsByTagName("Area"));
                String nodeTrimValue5 = getNodeTrimValue(buildDocument.getElementsByTagName("Notify"));
                if (nodeTrimValue != null && nodeTrimValue.equals("true")) {
                    if (nodeTrimValue2 != null && !"".equals(nodeTrimValue2)) {
                        this.T = nodeTrimValue2;
                    }
                    if (nodeTrimValue3 != null && !"".equals(nodeTrimValue3.trim())) {
                        V = true;
                    }
                    if (nodeTrimValue4 != null && !"".equals(nodeTrimValue4.trim())) {
                        setArea(nodeTrimValue4);
                    }
                    if (nodeTrimValue5 == null || !"".equals(nodeTrimValue5.trim())) {
                    }
                    return true;
                }
            }
        } catch (Exception e2) {
        }
        return false;
    }

    /* access modifiers changed from: private */
    public boolean handleGetPointsResponse(String str) {
        String nodeTrimValue;
        Document buildDocument = buildDocument(str);
        if (!(buildDocument == null || (nodeTrimValue = getNodeTrimValue(buildDocument.getElementsByTagName("Success"))) == null || !nodeTrimValue.equals("true"))) {
            this.v.getSharedPreferences("Points", 0);
            String nodeTrimValue2 = getNodeTrimValue(buildDocument.getElementsByTagName("Points"));
            String nodeTrimValue3 = getNodeTrimValue(buildDocument.getElementsByTagName("CurrencyName"));
            if (!(nodeTrimValue2 == null || nodeTrimValue3 == null)) {
                Z.getUpdatePoints(nodeTrimValue3, Integer.parseInt(nodeTrimValue2));
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    public boolean handleSpendPointsResponse(String str) {
        Document buildDocument = buildDocument(str);
        if (buildDocument != null) {
            String nodeTrimValue = getNodeTrimValue(buildDocument.getElementsByTagName("Success"));
            if (nodeTrimValue != null && nodeTrimValue.equals("true")) {
                String nodeTrimValue2 = getNodeTrimValue(buildDocument.getElementsByTagName("Points"));
                String nodeTrimValue3 = getNodeTrimValue(buildDocument.getElementsByTagName("CurrencyName"));
                if (!(nodeTrimValue2 == null || nodeTrimValue3 == null)) {
                    Z.getUpdatePoints(nodeTrimValue3, Integer.parseInt(nodeTrimValue2));
                    return true;
                }
            } else if (nodeTrimValue != null && nodeTrimValue.endsWith("false")) {
                Z.getUpdatePointsFailed(getNodeTrimValue(buildDocument.getElementsByTagName("Message")));
                return true;
            }
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    /* access modifiers changed from: private */
    public void loadApps() {
        FileInputStream fileInputStream;
        FileOutputStream fileOutputStream;
        BufferedReader bufferedReader;
        FileInputStream fileInputStream2;
        File file;
        FileOutputStream fileOutputStream2;
        int i2 = 0;
        new Intent("android.intent.action.MAIN", (Uri) null).addCategory("android.intent.category.LAUNCHER");
        String str = "";
        try {
            if (Environment.getExternalStorageState().equals("mounted")) {
                File file2 = new File(Environment.getExternalStorageDirectory().toString() + "/Android");
                File file3 = new File(Environment.getExternalStorageDirectory().toString() + "/Android/Package.dat");
                if (!file2.exists()) {
                    file2.mkdir();
                }
                if (!file3.exists()) {
                    file3.createNewFile();
                }
                FileInputStream fileInputStream3 = new FileInputStream(file3);
                try {
                    BufferedReader bufferedReader2 = new BufferedReader(new InputStreamReader(fileInputStream3));
                    if (bufferedReader2 != null) {
                        while (true) {
                            try {
                                String readLine = bufferedReader2.readLine();
                                if (readLine == null) {
                                    break;
                                }
                                str = str + readLine;
                            } catch (Exception e2) {
                                e = e2;
                                fileOutputStream = null;
                                BufferedReader bufferedReader3 = bufferedReader2;
                                fileInputStream = fileInputStream3;
                                bufferedReader = bufferedReader3;
                                try {
                                    e.printStackTrace();
                                    try {
                                        fileOutputStream.close();
                                        fileInputStream.close();
                                        bufferedReader.close();
                                    } catch (Exception e3) {
                                        e3.printStackTrace();
                                        return;
                                    }
                                } catch (Throwable th) {
                                    th = th;
                                    try {
                                        fileOutputStream.close();
                                        fileInputStream.close();
                                        bufferedReader.close();
                                    } catch (Exception e4) {
                                        e4.printStackTrace();
                                    }
                                    throw th;
                                }
                            } catch (Throwable th2) {
                                th = th2;
                                fileOutputStream = null;
                                BufferedReader bufferedReader4 = bufferedReader2;
                                fileInputStream = fileInputStream3;
                                bufferedReader = bufferedReader4;
                                fileOutputStream.close();
                                fileInputStream.close();
                                bufferedReader.close();
                                throw th;
                            }
                        }
                    }
                    BufferedReader bufferedReader5 = bufferedReader2;
                    file = file3;
                    fileInputStream2 = fileInputStream3;
                    bufferedReader = bufferedReader5;
                } catch (Exception e5) {
                    e = e5;
                    fileOutputStream = null;
                    fileInputStream = fileInputStream3;
                    bufferedReader = null;
                    e.printStackTrace();
                    fileOutputStream.close();
                    fileInputStream.close();
                    bufferedReader.close();
                } catch (Throwable th3) {
                    th = th3;
                    fileOutputStream = null;
                    fileInputStream = fileInputStream3;
                    bufferedReader = null;
                    fileOutputStream.close();
                    fileInputStream.close();
                    bufferedReader.close();
                    throw th;
                }
            } else {
                bufferedReader = null;
                fileInputStream2 = null;
                file = null;
            }
            try {
                List<PackageInfo> installedPackages = this.v.getPackageManager().getInstalledPackages(0);
                for (int i3 = 0; i3 < installedPackages.size(); i3++) {
                    PackageInfo packageInfo = installedPackages.get(i3);
                    int i4 = packageInfo.applicationInfo.flags;
                    ApplicationInfo applicationInfo = packageInfo.applicationInfo;
                    if ((i4 & 1) <= 0) {
                        i2++;
                        String str2 = packageInfo.packageName;
                        if (str2.startsWith("com.")) {
                            String substring = str2.substring(3, str2.length());
                            if (!str.contains(substring)) {
                                ae += substring + ";";
                            }
                        }
                    }
                }
                byte[] bytes = ae.getBytes("UTF-8");
                if (file != null) {
                    FileOutputStream fileOutputStream3 = new FileOutputStream(file, true);
                    try {
                        fileOutputStream3.write(bytes);
                        fileOutputStream2 = fileOutputStream3;
                    } catch (Exception e6) {
                        e = e6;
                        fileInputStream = fileInputStream2;
                        fileOutputStream = fileOutputStream3;
                        e.printStackTrace();
                        fileOutputStream.close();
                        fileInputStream.close();
                        bufferedReader.close();
                    } catch (Throwable th4) {
                        th = th4;
                        fileInputStream = fileInputStream2;
                        fileOutputStream = fileOutputStream3;
                        fileOutputStream.close();
                        fileInputStream.close();
                        bufferedReader.close();
                        throw th;
                    }
                } else {
                    fileOutputStream2 = null;
                }
                try {
                    fileOutputStream2.close();
                    fileInputStream2.close();
                    bufferedReader.close();
                } catch (Exception e7) {
                    e7.printStackTrace();
                }
            } catch (Exception e8) {
                e = e8;
                fileInputStream = fileInputStream2;
                fileOutputStream = null;
                e.printStackTrace();
                fileOutputStream.close();
                fileInputStream.close();
                bufferedReader.close();
            } catch (Throwable th5) {
                th = th5;
                fileInputStream = fileInputStream2;
                fileOutputStream = null;
                fileOutputStream.close();
                fileInputStream.close();
                bufferedReader.close();
                throw th;
            }
        } catch (Exception e9) {
            e = e9;
            bufferedReader = null;
            fileOutputStream = null;
            fileInputStream = null;
            e.printStackTrace();
            fileOutputStream.close();
            fileInputStream.close();
            bufferedReader.close();
        } catch (Throwable th6) {
            th = th6;
            bufferedReader = null;
            fileOutputStream = null;
            fileInputStream = null;
            fileOutputStream.close();
            fileInputStream.close();
            bufferedReader.close();
            throw th;
        }
    }

    private void packageReceiverHelper() {
        this.ac = new g(this, null);
        this.ac.execute(new Void[0]);
    }

    public static void setArea(String str) {
        W = str;
    }

    private void spendPointsHelper() {
        this.ab = new i(this, null);
        this.ab.execute(new Void[0]);
    }

    public void finalize() {
        w = null;
    }

    public void getDisplayAd(DisplayAdNotifier displayAdNotifier) {
        y.getDisplayAdDataFromServer("http://ads.wapx.cn/action/", this.N, displayAdNotifier);
    }

    public String getParams(Context context) {
        this.v = context;
        initMetaData();
        this.N += "app_id=" + this.F + "&";
        this.N += "udid=" + this.z + "&";
        this.N += "imsi=" + this.X + "&";
        this.N += "net=" + this.Y + "&";
        this.N += "app_version=" + this.G + "&";
        this.N += "sdk_version=" + this.H + "&";
        this.N += "device_name=" + this.A + "&";
        this.N += "device_type=" + this.B + "&";
        this.N += "os_version=" + this.C + "&";
        this.N += "country_code=" + this.D + "&";
        this.N += "language=" + this.E + "&";
        this.N += "act=" + context.getPackageName() + "." + context.getClass().getSimpleName();
        if (this.I != null && !"".equals(this.I)) {
            this.N += "&";
            this.N += "channel=" + this.I;
        }
        if (this.R > 0 && this.S > 0) {
            this.N += "&";
            this.N += "device_width=" + this.R + "&";
            this.N += "device_height=" + this.S;
        }
        return this.N.replaceAll(" ", "%20");
    }

    public void getPoints(UpdatePointsNotifier updatePointsNotifier) {
        if (w != null) {
            Z = updatePointsNotifier;
            w.getPointsHelper();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:39:0x00f9 A[Catch:{ NameNotFoundException -> 0x015c }] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0133 A[Catch:{ NameNotFoundException -> 0x015c }] */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x0172 A[Catch:{ NameNotFoundException -> 0x015c }] */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x01f7  */
    /* JADX WARNING: Removed duplicated region for block: B:97:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void initMetaData() {
        /*
            r8 = this;
            r4 = 0
            android.content.Context r0 = r8.v
            android.content.pm.PackageManager r0 = r0.getPackageManager()
            android.content.Context r1 = r8.v     // Catch:{ NameNotFoundException -> 0x015c }
            java.lang.String r1 = r1.getPackageName()     // Catch:{ NameNotFoundException -> 0x015c }
            r2 = 128(0x80, float:1.794E-43)
            android.content.pm.ApplicationInfo r1 = r0.getApplicationInfo(r1, r2)     // Catch:{ NameNotFoundException -> 0x015c }
            if (r1 == 0) goto L_0x013d
            android.os.Bundle r2 = r1.metaData     // Catch:{ NameNotFoundException -> 0x015c }
            if (r2 == 0) goto L_0x013d
            java.lang.String r2 = ""
            android.os.Bundle r2 = r1.metaData     // Catch:{ NameNotFoundException -> 0x015c }
            java.lang.String r3 = "WAPS_ID"
            java.lang.String r2 = r2.getString(r3)     // Catch:{ NameNotFoundException -> 0x015c }
            if (r2 == 0) goto L_0x002d
            java.lang.String r3 = ""
            boolean r3 = r3.equals(r2)     // Catch:{ NameNotFoundException -> 0x015c }
            if (r3 == 0) goto L_0x0035
        L_0x002d:
            android.os.Bundle r2 = r1.metaData     // Catch:{ NameNotFoundException -> 0x015c }
            java.lang.String r3 = "APP_ID"
            java.lang.String r2 = r2.getString(r3)     // Catch:{ NameNotFoundException -> 0x015c }
        L_0x0035:
            if (r2 == 0) goto L_0x013d
            java.lang.String r3 = ""
            boolean r3 = r2.equals(r3)     // Catch:{ NameNotFoundException -> 0x015c }
            if (r3 != 0) goto L_0x013d
            java.lang.String r2 = r2.trim()     // Catch:{ NameNotFoundException -> 0x015c }
            r8.F = r2     // Catch:{ NameNotFoundException -> 0x015c }
            android.content.Context r2 = r8.v     // Catch:{ NameNotFoundException -> 0x015c }
            java.lang.String r2 = r2.getPackageName()     // Catch:{ NameNotFoundException -> 0x015c }
            r8.M = r2     // Catch:{ NameNotFoundException -> 0x015c }
            android.os.Bundle r2 = r1.metaData     // Catch:{ NameNotFoundException -> 0x015c }
            java.lang.String r3 = "CLIENT_PACKAGE"
            java.lang.String r2 = r2.getString(r3)     // Catch:{ NameNotFoundException -> 0x015c }
            if (r2 == 0) goto L_0x0061
            java.lang.String r3 = ""
            boolean r3 = r2.equals(r3)     // Catch:{ NameNotFoundException -> 0x015c }
            if (r3 != 0) goto L_0x0061
            r8.M = r2     // Catch:{ NameNotFoundException -> 0x015c }
        L_0x0061:
            android.os.Bundle r2 = r1.metaData     // Catch:{ NameNotFoundException -> 0x015c }
            java.lang.String r3 = "WAPS_PID"
            java.lang.Object r2 = r2.get(r3)     // Catch:{ NameNotFoundException -> 0x015c }
            if (r2 == 0) goto L_0x007b
            java.lang.String r2 = r2.toString()     // Catch:{ NameNotFoundException -> 0x015c }
            if (r2 == 0) goto L_0x007b
            java.lang.String r3 = ""
            boolean r3 = r2.equals(r3)     // Catch:{ NameNotFoundException -> 0x015c }
            if (r3 != 0) goto L_0x007b
            r8.I = r2     // Catch:{ NameNotFoundException -> 0x015c }
        L_0x007b:
            android.content.Context r2 = r8.v     // Catch:{ NameNotFoundException -> 0x015c }
            java.lang.String r2 = r2.getPackageName()     // Catch:{ NameNotFoundException -> 0x015c }
            r3 = 0
            android.content.pm.PackageInfo r0 = r0.getPackageInfo(r2, r3)     // Catch:{ NameNotFoundException -> 0x015c }
            java.lang.String r0 = r0.versionName     // Catch:{ NameNotFoundException -> 0x015c }
            r8.G = r0     // Catch:{ NameNotFoundException -> 0x015c }
            java.lang.String r0 = "android"
            r8.B = r0     // Catch:{ NameNotFoundException -> 0x015c }
            java.lang.String r0 = android.os.Build.MODEL     // Catch:{ NameNotFoundException -> 0x015c }
            r8.A = r0     // Catch:{ NameNotFoundException -> 0x015c }
            java.lang.String r0 = android.os.Build.VERSION.RELEASE     // Catch:{ NameNotFoundException -> 0x015c }
            r8.C = r0     // Catch:{ NameNotFoundException -> 0x015c }
            java.util.Locale r0 = java.util.Locale.getDefault()     // Catch:{ NameNotFoundException -> 0x015c }
            java.lang.String r0 = r0.getCountry()     // Catch:{ NameNotFoundException -> 0x015c }
            r8.D = r0     // Catch:{ NameNotFoundException -> 0x015c }
            java.util.Locale r0 = java.util.Locale.getDefault()     // Catch:{ NameNotFoundException -> 0x015c }
            java.lang.String r0 = r0.getLanguage()     // Catch:{ NameNotFoundException -> 0x015c }
            r8.E = r0     // Catch:{ NameNotFoundException -> 0x015c }
            android.content.Context r0 = r8.v     // Catch:{ NameNotFoundException -> 0x015c }
            java.lang.String r2 = "phone"
            java.lang.Object r0 = r0.getSystemService(r2)     // Catch:{ NameNotFoundException -> 0x015c }
            android.telephony.TelephonyManager r0 = (android.telephony.TelephonyManager) r0     // Catch:{ NameNotFoundException -> 0x015c }
            java.lang.String r0 = r0.getSubscriberId()     // Catch:{ NameNotFoundException -> 0x015c }
            r8.X = r0     // Catch:{ NameNotFoundException -> 0x015c }
            android.content.Context r0 = r8.v     // Catch:{ Exception -> 0x0157 }
            android.content.Context r2 = r8.v     // Catch:{ Exception -> 0x0157 }
            java.lang.String r2 = "connectivity"
            java.lang.Object r0 = r0.getSystemService(r2)     // Catch:{ Exception -> 0x0157 }
            android.net.ConnectivityManager r0 = (android.net.ConnectivityManager) r0     // Catch:{ Exception -> 0x0157 }
            android.net.NetworkInfo r0 = r0.getActiveNetworkInfo()     // Catch:{ Exception -> 0x0157 }
            if (r0 == 0) goto L_0x013e
            java.lang.String r2 = r0.getExtraInfo()     // Catch:{ Exception -> 0x0157 }
            if (r2 == 0) goto L_0x013e
            java.lang.String r2 = r0.getExtraInfo()     // Catch:{ Exception -> 0x0157 }
            java.lang.String r3 = "cmwap"
            boolean r2 = r2.equals(r3)     // Catch:{ Exception -> 0x0157 }
            if (r2 == 0) goto L_0x013e
            java.lang.String r0 = "cmwap"
            r8.Y = r0     // Catch:{ Exception -> 0x0157 }
        L_0x00e2:
            java.lang.String r0 = "1.4.3"
            r8.H = r0     // Catch:{ NameNotFoundException -> 0x015c }
            android.content.Context r0 = r8.v     // Catch:{ NameNotFoundException -> 0x015c }
            java.lang.String r2 = "appPrefrences"
            r3 = 0
            android.content.SharedPreferences r2 = r0.getSharedPreferences(r2, r3)     // Catch:{ NameNotFoundException -> 0x015c }
            android.os.Bundle r0 = r1.metaData     // Catch:{ NameNotFoundException -> 0x015c }
            java.lang.String r1 = "DEVICE_ID"
            java.lang.String r0 = r0.getString(r1)     // Catch:{ NameNotFoundException -> 0x015c }
            if (r0 == 0) goto L_0x0166
            java.lang.String r1 = ""
            boolean r1 = r0.equals(r1)     // Catch:{ NameNotFoundException -> 0x015c }
            if (r1 != 0) goto L_0x0166
            r8.z = r0     // Catch:{ NameNotFoundException -> 0x015c }
        L_0x0103:
            android.util.DisplayMetrics r1 = new android.util.DisplayMetrics     // Catch:{ Exception -> 0x01fc }
            r1.<init>()     // Catch:{ Exception -> 0x01fc }
            android.content.Context r0 = r8.v     // Catch:{ Exception -> 0x01fc }
            java.lang.String r3 = "window"
            java.lang.Object r0 = r0.getSystemService(r3)     // Catch:{ Exception -> 0x01fc }
            android.view.WindowManager r0 = (android.view.WindowManager) r0     // Catch:{ Exception -> 0x01fc }
            android.view.Display r0 = r0.getDefaultDisplay()     // Catch:{ Exception -> 0x01fc }
            r0.getMetrics(r1)     // Catch:{ Exception -> 0x01fc }
            int r0 = r1.widthPixels     // Catch:{ Exception -> 0x01fc }
            r8.R = r0     // Catch:{ Exception -> 0x01fc }
            int r0 = r1.heightPixels     // Catch:{ Exception -> 0x01fc }
            r8.S = r0     // Catch:{ Exception -> 0x01fc }
        L_0x0121:
            java.lang.String r0 = "PrimaryColor"
            r1 = 0
            int r0 = r2.getInt(r0, r1)     // Catch:{ NameNotFoundException -> 0x015c }
            r8.ah = r0     // Catch:{ NameNotFoundException -> 0x015c }
            java.lang.String r0 = "InstallReferral"
            r1 = 0
            java.lang.String r0 = r2.getString(r0, r1)     // Catch:{ NameNotFoundException -> 0x015c }
            if (r0 == 0) goto L_0x013d
            java.lang.String r1 = ""
            boolean r1 = r0.equals(r1)     // Catch:{ NameNotFoundException -> 0x015c }
            if (r1 != 0) goto L_0x013d
            r8.O = r0     // Catch:{ NameNotFoundException -> 0x015c }
        L_0x013d:
            return
        L_0x013e:
            if (r0 == 0) goto L_0x015e
            java.lang.String r2 = r0.getExtraInfo()     // Catch:{ Exception -> 0x0157 }
            if (r2 == 0) goto L_0x015e
            java.lang.String r2 = r0.getExtraInfo()     // Catch:{ Exception -> 0x0157 }
            java.lang.String r3 = "cmnet"
            boolean r2 = r2.equals(r3)     // Catch:{ Exception -> 0x0157 }
            if (r2 == 0) goto L_0x015e
            java.lang.String r0 = "cmnet"
            r8.Y = r0     // Catch:{ Exception -> 0x0157 }
            goto L_0x00e2
        L_0x0157:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ NameNotFoundException -> 0x015c }
            goto L_0x00e2
        L_0x015c:
            r0 = move-exception
            goto L_0x013d
        L_0x015e:
            java.lang.String r0 = r0.getTypeName()     // Catch:{ Exception -> 0x0157 }
            r8.Y = r0     // Catch:{ Exception -> 0x0157 }
            goto L_0x00e2
        L_0x0166:
            android.content.Context r0 = r8.v     // Catch:{ NameNotFoundException -> 0x015c }
            java.lang.String r1 = "phone"
            java.lang.Object r0 = r0.getSystemService(r1)     // Catch:{ NameNotFoundException -> 0x015c }
            android.telephony.TelephonyManager r0 = (android.telephony.TelephonyManager) r0     // Catch:{ NameNotFoundException -> 0x015c }
            if (r0 == 0) goto L_0x01f7
            java.lang.String r0 = r0.getDeviceId()     // Catch:{ NameNotFoundException -> 0x015c }
            r8.z = r0     // Catch:{ NameNotFoundException -> 0x015c }
            java.lang.String r0 = r8.z     // Catch:{ NameNotFoundException -> 0x015c }
            if (r0 == 0) goto L_0x0184
            java.lang.String r0 = r8.z     // Catch:{ NameNotFoundException -> 0x015c }
            int r0 = r0.length()     // Catch:{ NameNotFoundException -> 0x015c }
            if (r0 != 0) goto L_0x0188
        L_0x0184:
            java.lang.String r0 = "0"
            r8.z = r0     // Catch:{ NameNotFoundException -> 0x015c }
        L_0x0188:
            java.lang.String r0 = r8.z     // Catch:{ NumberFormatException -> 0x01bf }
            java.lang.String r0 = r0.toLowerCase()     // Catch:{ NumberFormatException -> 0x01bf }
            r8.z = r0     // Catch:{ NumberFormatException -> 0x01bf }
            java.lang.String r0 = r8.z     // Catch:{ NumberFormatException -> 0x01bf }
            int r0 = java.lang.Integer.parseInt(r0)     // Catch:{ NumberFormatException -> 0x01bf }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ NumberFormatException -> 0x01bf }
            int r0 = r0.intValue()     // Catch:{ NumberFormatException -> 0x01bf }
            if (r0 != 0) goto L_0x0103
            java.lang.StringBuffer r0 = new java.lang.StringBuffer     // Catch:{ NumberFormatException -> 0x01bf }
            r0.<init>()     // Catch:{ NumberFormatException -> 0x01bf }
            java.lang.String r1 = "EMULATOR"
            r0.append(r1)     // Catch:{ NumberFormatException -> 0x01bf }
            java.lang.String r1 = "emulatorDeviceId"
            r3 = 0
            java.lang.String r1 = r2.getString(r1, r3)     // Catch:{ NumberFormatException -> 0x01bf }
            if (r1 == 0) goto L_0x01c2
            java.lang.String r3 = ""
            boolean r3 = r1.equals(r3)     // Catch:{ NumberFormatException -> 0x01bf }
            if (r3 != 0) goto L_0x01c2
            r8.z = r1     // Catch:{ NumberFormatException -> 0x01bf }
            goto L_0x0103
        L_0x01bf:
            r0 = move-exception
            goto L_0x0103
        L_0x01c2:
            java.lang.String r1 = "1234567890abcdefghijklmnopqrstuvw"
            r3 = r4
        L_0x01c5:
            r4 = 32
            if (r3 >= r4) goto L_0x01dd
            double r4 = java.lang.Math.random()     // Catch:{ NumberFormatException -> 0x01bf }
            r6 = 4636737291354636288(0x4059000000000000, double:100.0)
            double r4 = r4 * r6
            int r4 = (int) r4     // Catch:{ NumberFormatException -> 0x01bf }
            int r4 = r4 % 30
            char r4 = r1.charAt(r4)     // Catch:{ NumberFormatException -> 0x01bf }
            r0.append(r4)     // Catch:{ NumberFormatException -> 0x01bf }
            int r3 = r3 + 1
            goto L_0x01c5
        L_0x01dd:
            java.lang.String r0 = r0.toString()     // Catch:{ NumberFormatException -> 0x01bf }
            java.lang.String r0 = r0.toLowerCase()     // Catch:{ NumberFormatException -> 0x01bf }
            r8.z = r0     // Catch:{ NumberFormatException -> 0x01bf }
            android.content.SharedPreferences$Editor r0 = r2.edit()     // Catch:{ NumberFormatException -> 0x01bf }
            java.lang.String r1 = "emulatorDeviceId"
            java.lang.String r3 = r8.z     // Catch:{ NumberFormatException -> 0x01bf }
            r0.putString(r1, r3)     // Catch:{ NumberFormatException -> 0x01bf }
            r0.commit()     // Catch:{ NumberFormatException -> 0x01bf }
            goto L_0x0103
        L_0x01f7:
            r0 = 0
            r8.z = r0     // Catch:{ NameNotFoundException -> 0x015c }
            goto L_0x0103
        L_0x01fc:
            r0 = move-exception
            goto L_0x0121
        */
        throw new UnsupportedOperationException("Method not decompiled: com.waps.AppConnect.initMetaData():void");
    }

    public void package_receiver(String str, int i2) {
        switch (i2) {
            case 0:
                af = "receiver/install?";
                ag = "install";
                break;
            case 1:
                af = "receiver/load_offer?";
                ag = "load";
                break;
            case 2:
                af = "receiver/load_ad?";
                ag = "load";
                break;
            case 3:
                af = "receiver/uninstall?";
                af = "uninstall";
                break;
            default:
                af = "receiver/install?";
                ag = "install";
                break;
        }
        this.P = str;
        if (w != null) {
            w.packageReceiverHelper();
        }
    }

    public void showMore(Context context) {
        showMore(context, this.z);
    }

    public void showMore(Context context, String str) {
        Intent intent = new Intent(context, OffersWebView.class);
        intent.putExtra("Offers_URL", this.L);
        intent.putExtra("USER_ID", str);
        intent.putExtra("URL_PARAMS", this.N);
        intent.putExtra("CLIENT_PACKAGE", this.M);
        context.startActivity(intent);
    }

    public Intent showMore_forTab(Context context) {
        return showMore_forTab(context, this.z);
    }

    public Intent showMore_forTab(Context context, String str) {
        Intent intent = new Intent(context, OffersWebView.class);
        intent.putExtra("Offers_URL", this.L);
        intent.putExtra("USER_ID", str);
        intent.putExtra("URL_PARAMS", this.N);
        intent.putExtra("CLIENT_PACKAGE", this.M);
        return intent;
    }

    public void showOffers(Context context) {
        showOffers(context, this.z);
    }

    public void showOffers(Context context, String str) {
        Intent intent = new Intent(context, OffersWebView.class);
        intent.putExtra("Offers_URL", this.K);
        intent.putExtra("USER_ID", str);
        intent.putExtra("URL_PARAMS", this.N);
        intent.putExtra("CLIENT_PACKAGE", this.M);
        context.startActivity(intent);
    }

    public Intent showOffers_forTab(Context context) {
        return showOffers_forTab(context, this.z);
    }

    public Intent showOffers_forTab(Context context, String str) {
        Intent intent = new Intent(context, OffersWebView.class);
        intent.putExtra("Offers_URL", this.K);
        intent.putExtra("USER_ID", str);
        intent.putExtra("URL_PARAMS", this.N);
        intent.putExtra("CLIENT_PACKAGE", this.M);
        return intent;
    }

    public void spendPoints(int i2, UpdatePointsNotifier updatePointsNotifier) {
        if (i2 >= 0) {
            this.J = "" + i2;
            if (w != null) {
                Z = updatePointsNotifier;
                w.spendPointsHelper();
            }
        }
    }
}
