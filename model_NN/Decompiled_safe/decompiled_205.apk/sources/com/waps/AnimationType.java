package com.waps;

import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class AnimationType {
    public static final int ALPHA = 5;
    public static final int NONE = -1;
    public static final int RANDOM = 0;
    public static final int ROTATE = 4;
    public static final int SCALE_CENTER = 1;
    public static final int TRANSLATE_FROM_LEFT = 7;
    public static final int TRANSLATE_FROM_RIGHT = 6;
    private int[] a;
    private int b;

    public AnimationType(int i) {
        this.b = i;
    }

    public AnimationType(int[] iArr) {
        this.a = iArr;
    }

    private static Map getAnimation(View view) {
        float width = ((float) view.getWidth()) / 2.0f;
        float height = ((float) view.getHeight()) / 2.0f;
        HashMap hashMap = new HashMap();
        hashMap.put("1", new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, width, height));
        hashMap.put("4", new c(270.0f, 360.0f, width, height, 0.0f, true));
        hashMap.put("5", new AlphaAnimation(0.0f, 1.0f));
        hashMap.put("6", new TranslateAnimation((float) view.getWidth(), 0.0f, 0.0f, 0.0f));
        hashMap.put("7", new TranslateAnimation((float) (-view.getWidth()), 0.0f, 0.0f, 0.0f));
        return hashMap;
    }

    private List getAnimationList(View view) {
        Map animation = getAnimation(view);
        ArrayList arrayList = new ArrayList();
        String str = "";
        for (int i = 0; i < this.a.length; i++) {
            str = str + this.a[i];
        }
        if (str.contains("0")) {
            arrayList.add(animation.get("1"));
            arrayList.add(animation.get("4"));
            arrayList.add(animation.get("5"));
            arrayList.add(animation.get("6"));
            arrayList.add(animation.get("7"));
            return arrayList;
        } else if (str.contains("-1")) {
            return arrayList;
        } else {
            for (int i2 = 0; i2 < this.a.length; i2++) {
                if (this.a[i2] >= 9 || this.a.length > 7) {
                    arrayList.add(animation.get("1"));
                    arrayList.add(animation.get("4"));
                    arrayList.add(animation.get("5"));
                    arrayList.add(animation.get("6"));
                    arrayList.add(animation.get("7"));
                    return arrayList;
                }
                arrayList.add(animation.get(this.a[i2] + ""));
            }
            return arrayList;
        }
    }

    private List getAnimationList2(View view) {
        Map animation = getAnimation(view);
        ArrayList arrayList = new ArrayList();
        switch (this.b) {
            case NONE /*-1*/:
                return arrayList;
            case 0:
                arrayList.add(animation.get("1"));
                arrayList.add(animation.get("4"));
                arrayList.add(animation.get("5"));
                arrayList.add(animation.get("6"));
                arrayList.add(animation.get("7"));
                return arrayList;
            case 1:
                arrayList.add(animation.get("1"));
                return arrayList;
            case 2:
            case 3:
            default:
                return arrayList;
            case 4:
                arrayList.add(animation.get("4"));
                return arrayList;
            case 5:
                arrayList.add(animation.get("5"));
                return arrayList;
            case TRANSLATE_FROM_RIGHT /*6*/:
                arrayList.add(animation.get("6"));
                return arrayList;
            case TRANSLATE_FROM_LEFT /*7*/:
                arrayList.add(animation.get("7"));
                return arrayList;
        }
    }

    private static void startRotation(float f, float f2, View view) {
        c cVar = new c(f, f2, ((float) view.getWidth()) / 2.0f, ((float) view.getHeight()) / 2.0f, 0.0f, true);
        cVar.setDuration(2000);
        cVar.setFillAfter(true);
        cVar.setInterpolator(new DecelerateInterpolator());
        view.startAnimation(cVar);
    }

    public void startAnimation(View view) {
        List animationList2 = this.a == null ? getAnimationList2(view) : getAnimationList(view);
        Random random = new Random();
        if (animationList2.size() != 0) {
            Animation animation = (Animation) animationList2.get(random.nextInt(animationList2.size()));
            animation.setDuration(1000);
            animation.setInterpolator(new DecelerateInterpolator());
            view.startAnimation(animation);
        }
    }
}
