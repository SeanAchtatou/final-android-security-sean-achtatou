package net.yebaihe.sdk;

import java.security.Key;
import java.security.SecureRandom;
import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.IvParameterSpec;

public class Des2 {
    public static final String ALGORITHM_DES = "DES/CBC/PKCS5Padding";

    public static String encode(String key, String data) throws Exception {
        return encode(key, data.getBytes());
    }

    public static String encode(String key, byte[] data) throws Exception {
        try {
            Key secretKey = SecretKeyFactory.getInstance("DES").generateSecret(new DESKeySpec(key.getBytes()));
            Cipher cipher = Cipher.getInstance(ALGORITHM_DES);
            cipher.init(1, secretKey, new IvParameterSpec(key.getBytes()));
            return Base64.encodeBytes(cipher.doFinal(data));
        } catch (Exception e) {
            throw new Exception(e);
        }
    }

    public static byte[] decode(String key, byte[] data) throws Exception {
        try {
            new SecureRandom();
            Key secretKey = SecretKeyFactory.getInstance("DES").generateSecret(new DESKeySpec(key.getBytes()));
            Cipher cipher = Cipher.getInstance(ALGORITHM_DES);
            cipher.init(2, secretKey, new IvParameterSpec("12345678".getBytes()));
            return cipher.doFinal(data);
        } catch (Exception e) {
            throw new Exception(e);
        }
    }

    public static String decodeValue(String key, String data) {
        byte[] datas;
        try {
            if (System.getProperty("os.name") == null || (!System.getProperty("os.name").equalsIgnoreCase("sunos") && !System.getProperty("os.name").equalsIgnoreCase("linux"))) {
                datas = decode(key, Base64.decode(data));
            } else {
                datas = decode(key, Base64.decode(data));
            }
            return new String(datas);
        } catch (Exception e) {
            return "";
        }
    }

    public static void main(String[] args) throws Exception {
        System.out.println("明：abc ；密：" + encode("lilysdk@2", "abc"));
    }
}
