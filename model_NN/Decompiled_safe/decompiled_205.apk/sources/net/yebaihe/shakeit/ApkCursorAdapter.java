package net.yebaihe.shakeit;

import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.ArrayList;

public class ApkCursorAdapter extends SimpleCursorAdapter {
    protected ArrayList<String> apkValueList = new ArrayList<>();
    /* access modifiers changed from: private */
    public AdaptSelChangeNotify mNotify;
    private PackageManager pm;
    protected long totalSize;

    class ApkInfo {
        String path;
        /* access modifiers changed from: private */
        public long size;

        public ApkInfo(String string) {
            this.path = string;
            this.size = new File(string).length();
        }
    }

    public ApkCursorAdapter(Context context, int layout, Cursor c, String[] from, int[] to) {
        super(context, layout, c, from, to);
        this.pm = context.getPackageManager();
    }

    public void bindView(View view, Context context, Cursor cursor) {
        ((TextView) view.findViewById(R.id.rowtitle)).setText(baseName(cursor.getString(1)));
        ImageView i = (ImageView) view.findViewById(R.id.idicon);
        byte[] iconbytes = cursor.getBlob(4);
        if (iconbytes == null || iconbytes.length <= 1) {
            i.setImageResource(R.drawable.apk);
        } else {
            i.setImageDrawable(Drawable.createFromStream(new ByteArrayInputStream(iconbytes), baseName(cursor.getString(1))));
        }
        CheckBox c = (CheckBox) view.findViewById(R.id.idcheckbox);
        c.setTag(new ApkInfo(cursor.getString(1)));
        c.setChecked(this.apkValueList.indexOf(cursor.getString(1)) >= 0);
        c.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                ApkInfo info = (ApkInfo) buttonView.getTag();
                if (!isChecked) {
                    int idx = ApkCursorAdapter.this.apkValueList.indexOf(info.path);
                    if (idx >= 0) {
                        ApkCursorAdapter.this.apkValueList.remove(idx);
                        ApkCursorAdapter.this.totalSize -= info.size;
                    }
                } else if (ApkCursorAdapter.this.apkValueList.indexOf(info.path) < 0) {
                    ApkCursorAdapter.this.apkValueList.add(info.path);
                    ApkCursorAdapter.this.totalSize += info.size;
                }
                if (ApkCursorAdapter.this.mNotify != null) {
                    ApkCursorAdapter.this.mNotify.onAdapeSelectionChange("");
                }
            }
        });
        super.bindView(view, context, cursor);
    }

    private String baseName(String path) {
        return path.substring(path.lastIndexOf("/") + 1);
    }

    public void setOnSelectChange(AdaptSelChangeNotify notify) {
        this.mNotify = notify;
    }
}
