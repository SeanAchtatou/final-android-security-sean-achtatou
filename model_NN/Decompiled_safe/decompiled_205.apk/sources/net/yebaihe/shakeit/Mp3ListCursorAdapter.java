package net.yebaihe.shakeit;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.SimpleCursorAdapter;
import java.util.ArrayList;

public class Mp3ListCursorAdapter extends SimpleCursorAdapter {
    /* access modifiers changed from: private */
    public AdaptSelChangeNotify mNotify;
    protected ArrayList<Integer> mp3ValueList = new ArrayList<>();
    protected long totalSize = 0;

    class Mp3Info {
        int id;
        long size;

        public Mp3Info(int id2, long s) {
            this.id = id2;
            this.size = s;
        }
    }

    public Mp3ListCursorAdapter(Context context, int layout, Cursor c, String[] from, int[] to) {
        super(context, layout, c, from, to);
    }

    public void bindView(View view, Context context, Cursor cursor) {
        int id = cursor.getInt(cursor.getColumnIndexOrThrow("_id"));
        CheckBox c = (CheckBox) view.findViewById(R.id.idcheckbox);
        c.setTag(new Mp3Info(id, cursor.getLong(cursor.getColumnIndexOrThrow("_size"))));
        c.setChecked(this.mp3ValueList.indexOf(Integer.valueOf(id)) >= 0);
        ((ImageView) view.findViewById(R.id.idmp3img)).setImageResource(R.drawable.mp3);
        c.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Mp3Info info = (Mp3Info) buttonView.getTag();
                if (!isChecked) {
                    int idx = Mp3ListCursorAdapter.this.mp3ValueList.indexOf(Integer.valueOf(info.id));
                    if (idx >= 0) {
                        Mp3ListCursorAdapter.this.mp3ValueList.remove(idx);
                        Mp3ListCursorAdapter.this.totalSize -= info.size;
                    }
                } else if (Mp3ListCursorAdapter.this.mp3ValueList.indexOf(Integer.valueOf(info.id)) < 0) {
                    Mp3ListCursorAdapter.this.mp3ValueList.add(Integer.valueOf(info.id));
                    Mp3ListCursorAdapter.this.totalSize += info.size;
                }
                if (Mp3ListCursorAdapter.this.mNotify != null) {
                    Mp3ListCursorAdapter.this.mNotify.onAdapeSelectionChange("");
                }
            }
        });
        super.bindView(view, context, cursor);
    }

    public void setOnSelectChange(AdaptSelChangeNotify notify) {
        this.mNotify = notify;
    }
}
