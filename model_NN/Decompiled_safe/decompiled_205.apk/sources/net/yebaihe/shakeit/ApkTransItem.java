package net.yebaihe.shakeit;

import android.app.Activity;
import android.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class ApkTransItem extends TransItem {
    private int curPacket = 0;
    private String mPath;

    public ApkTransItem(Activity ctx, String path) {
        super(ctx);
        this.mPath = path;
        this.path = path;
        this.cntType = 4;
        if (path != null) {
            this.size = new File(path).length();
        }
    }

    public String getName() {
        if (this.path == null) {
            return this.name;
        }
        return this.path.substring(this.path.lastIndexOf("/") + 1);
    }

    public int getPacketNum() {
        int num = (int) (this.size / 800);
        if (((long) (num * 800)) < this.size) {
            return num + 1;
        }
        return num;
    }

    public int getPacketLength(int curPacketIdx) {
        if (curPacketIdx < getPacketNum() - 1) {
            return 800;
        }
        return (int) (this.size - ((long) (curPacketIdx * 800)));
    }

    public byte[] getPacket(int curPacketIdx) throws IOException {
        FileInputStream in = new FileInputStream(this.path);
        in.skip((long) (curPacketIdx * 800));
        byte[] bytes = new byte[getPacketLength(curPacketIdx)];
        in.read(bytes);
        return bytes;
    }

    public boolean fillPacket(int packetidx, int packetTotal, byte[] dst) throws IOException {
        boolean z;
        if (packetidx != this.curPacket) {
            Log.d("myown", "package idx does not match");
            return false;
        }
        this.curPacket = packetidx;
        if (packetidx != 0) {
            z = true;
        } else {
            z = false;
        }
        FileOutputStream out = getOutputStream(z);
        out.write(dst);
        out.flush();
        out.close();
        this.curPacket++;
        return true;
    }
}
