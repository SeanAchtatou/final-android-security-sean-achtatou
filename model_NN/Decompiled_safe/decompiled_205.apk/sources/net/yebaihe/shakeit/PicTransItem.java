package net.yebaihe.shakeit;

import android.app.Activity;
import android.database.Cursor;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;

public class PicTransItem extends TransItem {
    private int curPacket = 0;
    private Integer mPicId;
    /* access modifiers changed from: private */
    public MediaScannerConnection m_pScanner;

    public PicTransItem(Activity ctx, Integer picid) {
        super(ctx);
        this.mPicId = picid;
        this.cntType = 1;
        if (picid.intValue() != -1) {
            Uri images = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
            Cursor cursor = this.mctx.managedQuery(images, new String[]{"_id", "_size", "_data", "datetaken"}, "_id=" + this.mPicId, null, "");
            cursor.moveToFirst();
            this.name = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(Long.valueOf(cursor.getLong(cursor.getColumnIndexOrThrow("datetaken"))));
            this.size = cursor.getLong(cursor.getColumnIndexOrThrow("_size"));
            this.path = cursor.getString(cursor.getColumnIndexOrThrow("_data"));
            Log.d("myown", this.path);
            cursor.close();
        }
    }

    public String getName() {
        if (this.mPicId.intValue() == -1) {
            return this.name;
        }
        return this.path.substring(this.path.lastIndexOf("/") + 1);
    }

    public int getPacketNum() {
        int num = (int) (this.size / 800);
        if (((long) (num * 800)) < this.size) {
            return num + 1;
        }
        return num;
    }

    public int getPacketLength(int curPacketIdx) {
        if (curPacketIdx < getPacketNum() - 1) {
            return 800;
        }
        return (int) (this.size - ((long) (curPacketIdx * 800)));
    }

    public byte[] getPacket(int curPacketIdx) throws IOException {
        FileInputStream in = new FileInputStream(this.path);
        in.skip((long) (curPacketIdx * 800));
        byte[] bytes = new byte[getPacketLength(curPacketIdx)];
        in.read(bytes);
        return bytes;
    }

    public boolean fillPacket(int packetidx, int packetTotal, byte[] dst) throws IOException {
        boolean z;
        if (packetidx != this.curPacket) {
            Log.d("myown", "package idx does not match");
            return false;
        }
        this.curPacket = packetidx;
        if (packetidx != 0) {
            z = true;
        } else {
            z = false;
        }
        FileOutputStream out = getOutputStream(z);
        out.write(dst);
        out.flush();
        out.close();
        this.curPacket++;
        if (this.curPacket == packetTotal) {
            this.mctx.runOnUiThread(new Runnable() {
                public void run() {
                    PicTransItem.this.scanfile(PicTransItem.this.getOutputPath());
                }
            });
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void scanfile(String outputPath) {
        final String szFile = outputPath;
        this.m_pScanner = new MediaScannerConnection(this.mctx, new MediaScannerConnection.MediaScannerConnectionClient() {
            public void onMediaScannerConnected() {
                PicTransItem.this.m_pScanner.scanFile(szFile, null);
            }

            public void onScanCompleted(String path, Uri uri) {
                if (path.equals(szFile)) {
                    PicTransItem.this.m_pScanner.disconnect();
                }
            }
        });
        this.m_pScanner.connect();
    }
}
