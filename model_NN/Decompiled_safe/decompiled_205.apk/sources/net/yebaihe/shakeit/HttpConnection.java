package net.yebaihe.shakeit;

import android.os.Handler;
import android.os.Message;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import org.apache.http.HttpEntity;
import org.apache.http.client.HttpClient;
import org.apache.http.entity.BufferedHttpEntity;

public class HttpConnection implements Runnable {
    private static final int BIGFILE = 4;
    private static final int DELETE = 3;
    public static final int DID_ERROR = 1;
    public static final int DID_PROGRESS = 3;
    public static final int DID_START = 0;
    public static final int DID_SUCCEED = 2;
    private static final int GET = 0;
    private static final int POST = 1;
    private static final int PUT = 2;
    private String data;
    private Handler handler;
    private HttpClient httpClient;
    private int method;
    public File outTmpFile;
    private String url;

    public HttpConnection() {
        this(new Handler());
    }

    public HttpConnection(Handler _handler) {
        this.handler = _handler;
    }

    public void create(int method2, String url2, String data2) {
        this.method = method2;
        this.url = url2;
        this.data = data2;
        ConnectionManager.getInstance().push(this);
    }

    public void get(String url2) {
        create(0, url2, null);
    }

    public void post(String url2, String data2) {
        create(1, url2, data2);
    }

    public void put(String url2, String data2) {
        create(2, url2, data2);
    }

    public void delete(String url2) {
        create(3, url2, null);
    }

    public void file(String url2) {
        create(4, url2, null);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0115 A[Catch:{ Exception -> 0x01df }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r22 = this;
            r0 = r22
            int r0 = r0.method
            r17 = r0
            r18 = 4
            r0 = r17
            r1 = r18
            if (r0 != r1) goto L_0x00ce
            java.net.URL r4 = new java.net.URL     // Catch:{ MalformedURLException -> 0x00c0, IOException -> 0x00c7 }
            r0 = r22
            java.lang.String r0 = r0.url     // Catch:{ MalformedURLException -> 0x00c0, IOException -> 0x00c7 }
            r17 = r0
            r0 = r4
            r1 = r17
            r0.<init>(r1)     // Catch:{ MalformedURLException -> 0x00c0, IOException -> 0x00c7 }
            java.net.URLConnection r16 = r4.openConnection()     // Catch:{ MalformedURLException -> 0x00c0, IOException -> 0x00c7 }
            java.net.HttpURLConnection r16 = (java.net.HttpURLConnection) r16     // Catch:{ MalformedURLException -> 0x00c0, IOException -> 0x00c7 }
            java.lang.String r17 = "GET"
            r16.setRequestMethod(r17)     // Catch:{ MalformedURLException -> 0x00c0, IOException -> 0x00c7 }
            r17 = 1
            r16.setDoOutput(r17)     // Catch:{ MalformedURLException -> 0x00c0, IOException -> 0x00c7 }
            r16.connect()     // Catch:{ MalformedURLException -> 0x00c0, IOException -> 0x00c7 }
            java.io.InputStream r13 = r16.getInputStream()     // Catch:{ MalformedURLException -> 0x00c0, IOException -> 0x00c7 }
            int r15 = r16.getContentLength()     // Catch:{ MalformedURLException -> 0x00c0, IOException -> 0x00c7 }
            r7 = 0
            java.lang.String r17 = "mediaplayertmp"
            java.lang.String r18 = "dat"
            java.io.File r19 = new java.io.File     // Catch:{ MalformedURLException -> 0x00c0, IOException -> 0x00c7 }
            java.lang.StringBuilder r20 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x00c0, IOException -> 0x00c7 }
            r20.<init>()     // Catch:{ MalformedURLException -> 0x00c0, IOException -> 0x00c7 }
            java.io.File r21 = android.os.Environment.getExternalStorageDirectory()     // Catch:{ MalformedURLException -> 0x00c0, IOException -> 0x00c7 }
            java.lang.StringBuilder r20 = r20.append(r21)     // Catch:{ MalformedURLException -> 0x00c0, IOException -> 0x00c7 }
            java.lang.String r20 = r20.toString()     // Catch:{ MalformedURLException -> 0x00c0, IOException -> 0x00c7 }
            r19.<init>(r20)     // Catch:{ MalformedURLException -> 0x00c0, IOException -> 0x00c7 }
            java.io.File r17 = java.io.File.createTempFile(r17, r18, r19)     // Catch:{ MalformedURLException -> 0x00c0, IOException -> 0x00c7 }
            r0 = r17
            r1 = r22
            r1.outTmpFile = r0     // Catch:{ MalformedURLException -> 0x00c0, IOException -> 0x00c7 }
            java.io.FileOutputStream r10 = new java.io.FileOutputStream     // Catch:{ MalformedURLException -> 0x00c0, IOException -> 0x00c7 }
            r0 = r22
            java.io.File r0 = r0.outTmpFile     // Catch:{ MalformedURLException -> 0x00c0, IOException -> 0x00c7 }
            r17 = r0
            r0 = r10
            r1 = r17
            r0.<init>(r1)     // Catch:{ MalformedURLException -> 0x00c0, IOException -> 0x00c7 }
            r17 = 1024(0x400, float:1.435E-42)
            r0 = r17
            byte[] r0 = new byte[r0]     // Catch:{ MalformedURLException -> 0x00c0, IOException -> 0x00c7 }
            r5 = r0
            r6 = 0
        L_0x0072:
            int r6 = r13.read(r5)     // Catch:{ MalformedURLException -> 0x00c0, IOException -> 0x00c7 }
            if (r6 > 0) goto L_0x0097
            r10.close()     // Catch:{ MalformedURLException -> 0x00c0, IOException -> 0x00c7 }
            r0 = r22
            android.os.Handler r0 = r0.handler     // Catch:{ MalformedURLException -> 0x00c0, IOException -> 0x00c7 }
            r17 = r0
            r0 = r22
            android.os.Handler r0 = r0.handler     // Catch:{ MalformedURLException -> 0x00c0, IOException -> 0x00c7 }
            r18 = r0
            r19 = 2
            r0 = r22
            java.io.File r0 = r0.outTmpFile     // Catch:{ MalformedURLException -> 0x00c0, IOException -> 0x00c7 }
            r20 = r0
            android.os.Message r18 = android.os.Message.obtain(r18, r19, r20)     // Catch:{ MalformedURLException -> 0x00c0, IOException -> 0x00c7 }
            r17.sendMessage(r18)     // Catch:{ MalformedURLException -> 0x00c0, IOException -> 0x00c7 }
        L_0x0096:
            return
        L_0x0097:
            r17 = 0
            r0 = r10
            r1 = r5
            r2 = r17
            r3 = r6
            r0.write(r1, r2, r3)     // Catch:{ MalformedURLException -> 0x00c0, IOException -> 0x00c7 }
            int r7 = r7 + r6
            r0 = r22
            android.os.Handler r0 = r0.handler     // Catch:{ MalformedURLException -> 0x00c0, IOException -> 0x00c7 }
            r17 = r0
            r0 = r22
            android.os.Handler r0 = r0.handler     // Catch:{ MalformedURLException -> 0x00c0, IOException -> 0x00c7 }
            r18 = r0
            r19 = 3
            int r20 = r7 * 100
            int r20 = r20 / r15
            java.lang.Integer r20 = java.lang.Integer.valueOf(r20)     // Catch:{ MalformedURLException -> 0x00c0, IOException -> 0x00c7 }
            android.os.Message r18 = android.os.Message.obtain(r18, r19, r20)     // Catch:{ MalformedURLException -> 0x00c0, IOException -> 0x00c7 }
            r17.sendMessage(r18)     // Catch:{ MalformedURLException -> 0x00c0, IOException -> 0x00c7 }
            goto L_0x0072
        L_0x00c0:
            r17 = move-exception
            r8 = r17
            r8.printStackTrace()
            goto L_0x0096
        L_0x00c7:
            r17 = move-exception
            r8 = r17
            r8.printStackTrace()
            goto L_0x0096
        L_0x00ce:
            r0 = r22
            android.os.Handler r0 = r0.handler
            r17 = r0
            r0 = r22
            android.os.Handler r0 = r0.handler
            r18 = r0
            r19 = 0
            android.os.Message r18 = android.os.Message.obtain(r18, r19)
            r17.sendMessage(r18)
            org.apache.http.impl.client.DefaultHttpClient r17 = new org.apache.http.impl.client.DefaultHttpClient
            r17.<init>()
            r0 = r17
            r1 = r22
            r1.httpClient = r0
            r0 = r22
            org.apache.http.client.HttpClient r0 = r0.httpClient
            r17 = r0
            org.apache.http.params.HttpParams r17 = r17.getParams()
            r18 = 10000(0x2710, float:1.4013E-41)
            org.apache.http.params.HttpConnectionParams.setConnectionTimeout(r17, r18)
            r14 = 0
            r0 = r22
            int r0 = r0.method     // Catch:{ Exception -> 0x01df }
            r17 = r0
            switch(r17) {
                case 0: goto L_0x012d;
                case 1: goto L_0x0143;
                case 2: goto L_0x0178;
                case 3: goto L_0x01a6;
                case 4: goto L_0x01bd;
                default: goto L_0x0107;
            }     // Catch:{ Exception -> 0x01df }
        L_0x0107:
            r0 = r22
            int r0 = r0.method     // Catch:{ Exception -> 0x01df }
            r17 = r0
            r18 = 4
            r0 = r17
            r1 = r18
            if (r0 >= r1) goto L_0x0120
            org.apache.http.HttpEntity r17 = r14.getEntity()     // Catch:{ Exception -> 0x01df }
            r0 = r22
            r1 = r17
            r0.processEntity(r1)     // Catch:{ Exception -> 0x01df }
        L_0x0120:
            net.yebaihe.shakeit.ConnectionManager r17 = net.yebaihe.shakeit.ConnectionManager.getInstance()
            r0 = r17
            r1 = r22
            r0.didComplete(r1)
            goto L_0x0096
        L_0x012d:
            r0 = r22
            org.apache.http.client.HttpClient r0 = r0.httpClient     // Catch:{ Exception -> 0x01df }
            r17 = r0
            org.apache.http.client.methods.HttpGet r18 = new org.apache.http.client.methods.HttpGet     // Catch:{ Exception -> 0x01df }
            r0 = r22
            java.lang.String r0 = r0.url     // Catch:{ Exception -> 0x01df }
            r19 = r0
            r18.<init>(r19)     // Catch:{ Exception -> 0x01df }
            org.apache.http.HttpResponse r14 = r17.execute(r18)     // Catch:{ Exception -> 0x01df }
            goto L_0x0107
        L_0x0143:
            org.apache.http.client.methods.HttpPost r11 = new org.apache.http.client.methods.HttpPost     // Catch:{ Exception -> 0x01df }
            r0 = r22
            java.lang.String r0 = r0.url     // Catch:{ Exception -> 0x01df }
            r17 = r0
            r0 = r11
            r1 = r17
            r0.<init>(r1)     // Catch:{ Exception -> 0x01df }
            org.apache.http.entity.StringEntity r9 = new org.apache.http.entity.StringEntity     // Catch:{ Exception -> 0x01df }
            r0 = r22
            java.lang.String r0 = r0.data     // Catch:{ Exception -> 0x01df }
            r17 = r0
            r0 = r9
            r1 = r17
            r0.<init>(r1)     // Catch:{ Exception -> 0x01df }
            java.lang.String r17 = "application/x-www-form-urlencoded"
            r0 = r9
            r1 = r17
            r0.setContentType(r1)     // Catch:{ Exception -> 0x01df }
            r11.setEntity(r9)     // Catch:{ Exception -> 0x01df }
            r0 = r22
            org.apache.http.client.HttpClient r0 = r0.httpClient     // Catch:{ Exception -> 0x01df }
            r17 = r0
            r0 = r17
            r1 = r11
            org.apache.http.HttpResponse r14 = r0.execute(r1)     // Catch:{ Exception -> 0x01df }
            goto L_0x0107
        L_0x0178:
            org.apache.http.client.methods.HttpPut r12 = new org.apache.http.client.methods.HttpPut     // Catch:{ Exception -> 0x01df }
            r0 = r22
            java.lang.String r0 = r0.url     // Catch:{ Exception -> 0x01df }
            r17 = r0
            r0 = r12
            r1 = r17
            r0.<init>(r1)     // Catch:{ Exception -> 0x01df }
            org.apache.http.entity.StringEntity r17 = new org.apache.http.entity.StringEntity     // Catch:{ Exception -> 0x01df }
            r0 = r22
            java.lang.String r0 = r0.data     // Catch:{ Exception -> 0x01df }
            r18 = r0
            r17.<init>(r18)     // Catch:{ Exception -> 0x01df }
            r0 = r12
            r1 = r17
            r0.setEntity(r1)     // Catch:{ Exception -> 0x01df }
            r0 = r22
            org.apache.http.client.HttpClient r0 = r0.httpClient     // Catch:{ Exception -> 0x01df }
            r17 = r0
            r0 = r17
            r1 = r12
            org.apache.http.HttpResponse r14 = r0.execute(r1)     // Catch:{ Exception -> 0x01df }
            goto L_0x0107
        L_0x01a6:
            r0 = r22
            org.apache.http.client.HttpClient r0 = r0.httpClient     // Catch:{ Exception -> 0x01df }
            r17 = r0
            org.apache.http.client.methods.HttpDelete r18 = new org.apache.http.client.methods.HttpDelete     // Catch:{ Exception -> 0x01df }
            r0 = r22
            java.lang.String r0 = r0.url     // Catch:{ Exception -> 0x01df }
            r19 = r0
            r18.<init>(r19)     // Catch:{ Exception -> 0x01df }
            org.apache.http.HttpResponse r14 = r17.execute(r18)     // Catch:{ Exception -> 0x01df }
            goto L_0x0107
        L_0x01bd:
            r0 = r22
            org.apache.http.client.HttpClient r0 = r0.httpClient     // Catch:{ Exception -> 0x01df }
            r17 = r0
            org.apache.http.client.methods.HttpGet r18 = new org.apache.http.client.methods.HttpGet     // Catch:{ Exception -> 0x01df }
            r0 = r22
            java.lang.String r0 = r0.url     // Catch:{ Exception -> 0x01df }
            r19 = r0
            r18.<init>(r19)     // Catch:{ Exception -> 0x01df }
            org.apache.http.HttpResponse r14 = r17.execute(r18)     // Catch:{ Exception -> 0x01df }
            org.apache.http.HttpEntity r17 = r14.getEntity()     // Catch:{ Exception -> 0x01df }
            r0 = r22
            r1 = r17
            r0.processFileEntity(r1)     // Catch:{ Exception -> 0x01df }
            goto L_0x0107
        L_0x01df:
            r17 = move-exception
            r8 = r17
            r0 = r22
            android.os.Handler r0 = r0.handler
            r17 = r0
            r0 = r22
            android.os.Handler r0 = r0.handler
            r18 = r0
            r19 = 1
            r0 = r18
            r1 = r19
            r2 = r8
            android.os.Message r18 = android.os.Message.obtain(r0, r1, r2)
            r17.sendMessage(r18)
            goto L_0x0120
        */
        throw new UnsupportedOperationException("Method not decompiled: net.yebaihe.shakeit.HttpConnection.run():void");
    }

    private void processEntity(HttpEntity entity) throws IllegalStateException, IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(entity.getContent()));
        String result = "";
        while (true) {
            String line = br.readLine();
            if (line == null) {
                this.handler.sendMessage(Message.obtain(this.handler, 2, result));
                return;
            }
            result = String.valueOf(result) + line + "\n";
        }
    }

    private void processFileEntity(HttpEntity entity) throws IOException {
        BufferedHttpEntity bufHttpEntity = new BufferedHttpEntity(entity);
        File file = File.createTempFile("mediaplayertmp", "dat");
        FileOutputStream fouts = new FileOutputStream(file);
        InputStream ins = bufHttpEntity.getContent();
        byte[] b = new byte[5000];
        int total = 0;
        while (true) {
            int i = ins.read(b);
            if (i == -1) {
                fouts.flush();
                fouts.close();
                ins.close();
                this.handler.sendMessage(Message.obtain(this.handler, 2, file));
                return;
            }
            fouts.write(b, 0, i);
            total += i;
            this.handler.sendMessage(Message.obtain(this.handler, 3, Integer.valueOf(total)));
        }
    }
}
