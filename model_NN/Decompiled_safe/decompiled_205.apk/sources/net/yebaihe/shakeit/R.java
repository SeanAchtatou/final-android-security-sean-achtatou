package net.yebaihe.shakeit;

public final class R {

    public static final class anim {
        public static final int bottom_in = 2130968576;
        public static final int fade_in = 2130968577;
        public static final int fade_out = 2130968578;
        public static final int left_in = 2130968579;
        public static final int left_out = 2130968580;
        public static final int right_in = 2130968581;
        public static final int right_out = 2130968582;
    }

    public static final class attr {
    }

    public static final class drawable {
        public static final int apk = 2130837504;
        public static final int bg = 2130837505;
        public static final int c1a = 2130837506;
        public static final int c1b = 2130837507;
        public static final int c2a = 2130837508;
        public static final int c2b = 2130837509;
        public static final int c3a = 2130837510;
        public static final int c3b = 2130837511;
        public static final int c4a = 2130837512;
        public static final int c4b = 2130837513;
        public static final int c5a = 2130837514;
        public static final int c5b = 2130837515;
        public static final int dc = 2130837516;
        public static final int down = 2130837517;
        public static final int download = 2130837518;
        public static final int dx = 2130837519;
        public static final int file = 2130837520;
        public static final int folder = 2130837521;
        public static final int gd = 2130837522;
        public static final int ic_pulltorefresh_arrow = 2130837523;
        public static final int icon = 2130837524;
        public static final int mp3 = 2130837525;
        public static final int new1 = 2130837526;
        public static final int pull_to_refresh_header_background = 2130837527;
        public static final int top = 2130837528;
        public static final int webviewbg = 2130837529;
        public static final int ybh = 2130837530;
    }

    public static final class id {
        public static final int apklist = 2131099649;
        public static final int btnimgfile = 2131099648;
        public static final int filelist = 2131099654;
        public static final int flayout = 2131099656;
        public static final int idProgressTxt = 2131099679;
        public static final int idcheckbox = 2131099652;
        public static final int idhint = 2131099669;
        public static final int idicon = 2131099650;
        public static final int idmp3img = 2131099672;
        public static final int idtitletext = 2131099684;
        public static final int ilogo = 2131099661;
        public static final int imgbtnapk = 2131099653;
        public static final int imgbtnmusic = 2131099670;
        public static final int imgbtnpic = 2131099677;
        public static final int imgbtnvideo = 2131099685;
        public static final int imgid = 2131099675;
        public static final int inewimage = 2131099659;
        public static final int lbutton = 2131099658;
        public static final int linearLayout1 = 2131099663;
        public static final int lprogressBar = 2131099660;
        public static final int lwebview = 2131099657;
        public static final int mp3list = 2131099671;
        public static final int mp3title = 2131099673;
        public static final int nametitle = 2131099655;
        public static final int piclist = 2131099674;
        public static final int picpath = 2131099676;
        public static final int progressBar1 = 2131099678;
        public static final int pull_to_refresh_image = 2131099681;
        public static final int pull_to_refresh_progress = 2131099680;
        public static final int pull_to_refresh_text = 2131099682;
        public static final int pull_to_refresh_updated_at = 2131099683;
        public static final int rowtitle = 2131099651;
        public static final int size = 2131099687;
        public static final int tab1 = 2131099664;
        public static final int tab2 = 2131099665;
        public static final int tab3 = 2131099666;
        public static final int tab4 = 2131099667;
        public static final int tab5 = 2131099668;
        public static final int tabhost = 2131099662;
        public static final int videolist = 2131099686;
    }

    public static final class layout {
        public static final int anything = 2130903040;
        public static final int apklists = 2130903041;
        public static final int apkrow = 2130903042;
        public static final int apks = 2130903043;
        public static final int filelists = 2130903044;
        public static final int filerow = 2130903045;
        public static final int lilysdkmain = 2130903046;
        public static final int main = 2130903047;
        public static final int mp3 = 2130903048;
        public static final int mp3lists = 2130903049;
        public static final int mp3row = 2130903050;
        public static final int piclists = 2130903051;
        public static final int picrow = 2130903052;
        public static final int picvideo = 2130903053;
        public static final int progress = 2130903054;
        public static final int pull_to_refresh_header = 2130903055;
        public static final int titlebar = 2130903056;
        public static final int video = 2130903057;
        public static final int videolists = 2130903058;
        public static final int videorow = 2130903059;
    }

    public static final class string {
        public static final int COMMENT = 2131034136;
        public static final int COMM_ERROR = 2131034139;
        public static final int GET_JIFEN = 2131034131;
        public static final int GET_JIFEN_SUCC = 2131034133;
        public static final int JIFEN_HINT = 2131034134;
        public static final int KOU_JIFEN = 2131034130;
        public static final int LilySdkTurnTo = 2131034114;
        public static final int LookingForPeer = 2131034120;
        public static final int MORE = 2131034137;
        public static final int MustChooseSomething = 2131034119;
        public static final int NEED_JIFEN = 2131034132;
        public static final int NoPeerFoound = 2131034124;
        public static final int Receiving = 2131034126;
        public static final int Sel_Hint = 2131034129;
        public static final int Sending = 2131034125;
        public static final int SetNameDialogMsg = 2131034123;
        public static final int SetNameDialogTitle = 2131034122;
        public static final int ShakePeerHint = 2131034121;
        public static final int StoreHint = 2131034127;
        public static final int YEBAIHE = 2131034135;
        public static final int app_name = 2131034113;
        public static final int copyright = 2131034138;
        public static final int hello = 2131034112;
        public static final int nowifi = 2131034128;
        public static final int pull_to_refresh_pull_label = 2131034115;
        public static final int pull_to_refresh_refreshing_label = 2131034117;
        public static final int pull_to_refresh_release_label = 2131034116;
        public static final int pull_to_refresh_tap_label = 2131034118;
    }
}
