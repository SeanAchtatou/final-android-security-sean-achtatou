package net.yebaihe.shakeit;

import android.app.Activity;
import android.os.Environment;
import android.util.Log;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public abstract class TransItem {
    protected static final int CNT_APK = 4;
    protected static final int CNT_FILE = 5;
    protected static final int CNT_MP3 = 2;
    protected static final int CNT_PIC = 1;
    protected static final int CNT_Video = 3;
    protected static final int DATA_IN_EACH_PACKET = 800;
    public int cntType = 0;
    protected Activity mctx;
    protected String name;
    protected String path;
    protected long size;

    public abstract boolean fillPacket(int i, int i2, byte[] bArr) throws IOException;

    public abstract String getName();

    public abstract byte[] getPacket(int i) throws IOException;

    public abstract int getPacketLength(int i);

    public abstract int getPacketNum();

    public TransItem(Activity ctx) {
        this.mctx = ctx;
    }

    /* access modifiers changed from: protected */
    public FileOutputStream getOutputStream(boolean append) throws FileNotFoundException {
        new File(getOutputDir()).mkdirs();
        return new FileOutputStream(getOutputPath(), append);
    }

    private String getOutputDir() {
        String base = Environment.getExternalStorageDirectory() + "/shakeit/";
        switch (this.cntType) {
            case 1:
                return String.valueOf(base) + "pic/";
            case 2:
                return String.valueOf(base) + "mp3/";
            case 3:
                return String.valueOf(base) + "video/";
            case 4:
                return String.valueOf(base) + "apk/";
            case 5:
                return String.valueOf(base) + "file/";
            default:
                return base;
        }
    }

    /* access modifiers changed from: package-private */
    public String getOutputPath() {
        return String.valueOf(getOutputDir()) + getName();
    }

    public void cancelTrans() {
        try {
            new File(getOutputPath()).delete();
        } catch (Exception e) {
            Log.d("myown", "exception on delete ");
        }
    }
}
