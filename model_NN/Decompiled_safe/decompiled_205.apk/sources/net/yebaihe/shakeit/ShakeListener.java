package net.yebaihe.shakeit;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

public class ShakeListener implements SensorEventListener {
    private static final int FORCE_THRESHOLD = 350;
    private static final int SHAKE_COUNT = 3;
    private static final int SHAKE_DURATION = 1000;
    private static final int SHAKE_TIMEOUT = 500;
    private static final int TIME_THRESHOLD = 100;
    private Context mContext;
    private long mLastForce;
    private long mLastShake;
    private long mLastTime;
    private float mLastX = -1.0f;
    private float mLastY = -1.0f;
    private float mLastZ = -1.0f;
    private SensorManager mSensorMgr;
    private int mShakeCount = 0;
    private OnShakeListener mShakeListener;

    public interface OnShakeListener {
        void onShake();
    }

    public ShakeListener(Context context) {
        this.mContext = context;
        resume();
    }

    public void setOnShakeListener(OnShakeListener listener) {
        this.mShakeListener = listener;
    }

    public void resume() {
        this.mSensorMgr = (SensorManager) this.mContext.getSystemService("sensor");
        if (this.mSensorMgr == null) {
            throw new UnsupportedOperationException("Sensors not supported");
        } else if (!this.mSensorMgr.registerListener(this, this.mSensorMgr.getDefaultSensor(1), 2)) {
            this.mSensorMgr.unregisterListener(this);
            throw new UnsupportedOperationException("Accelerometer not supported");
        }
    }

    public void pause() {
        if (this.mSensorMgr != null) {
            this.mSensorMgr.unregisterListener(this);
            this.mSensorMgr = null;
        }
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == 1) {
            long now = System.currentTimeMillis();
            if (now - this.mLastForce > 500) {
                this.mShakeCount = 0;
            }
            if (now - this.mLastTime > 100) {
                if ((Math.abs(((((event.values[0] + event.values[1]) + event.values[2]) - this.mLastX) - this.mLastY) - this.mLastZ) / ((float) (now - this.mLastTime))) * 10000.0f > 350.0f) {
                    int i = this.mShakeCount + 1;
                    this.mShakeCount = i;
                    if (i >= 3 && now - this.mLastShake > 1000) {
                        this.mLastShake = now;
                        this.mShakeCount = 0;
                        if (this.mShakeListener != null) {
                            this.mShakeListener.onShake();
                        }
                    }
                    this.mLastForce = now;
                }
                this.mLastTime = now;
                this.mLastX = event.values[0];
                this.mLastY = event.values[1];
                this.mLastZ = event.values[2];
            }
        }
    }
}
