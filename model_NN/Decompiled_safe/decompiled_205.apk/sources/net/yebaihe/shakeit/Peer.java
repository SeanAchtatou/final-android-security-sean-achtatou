package net.yebaihe.shakeit;

import android.util.Log;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class Peer {
    static final int SVR_PORT = 2208;
    private InetAddress client;
    private long mMyid;
    private DatagramSocket mSocket;
    String mip;
    String mname;

    public Peer(String ip, String name, DatagramSocket s, long id) {
        this.mip = ip;
        this.mname = name;
        this.mSocket = s;
        this.mMyid = id;
        Log.d("myown", "new Peer:" + ip + " " + name);
        try {
            this.client = InetAddress.getByName(this.mip);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    public void tryShakeHands(int total, String myname) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        DataOutputStream dos = new DataOutputStream(bos);
        try {
            dos.writeInt(65281);
            dos.writeLong(this.mMyid);
            dos.writeInt(total);
            byte[] bytes = myname.getBytes("UTF-8");
            dos.writeInt(bytes.length);
            dos.write(bytes);
            dos.flush();
            byte[] bytes2 = bos.toByteArray();
            try {
                this.mSocket.send(new DatagramPacket(bytes2, bytes2.length, this.client, (int) SVR_PORT));
            } catch (IOException e) {
                IOException e2 = e;
                Log.d("myown", "exception on try shake hands");
                e2.printStackTrace();
            }
        } catch (IOException e3) {
            Log.d("myown", "exception on try shake hands 2");
            e3.printStackTrace();
        }
    }
}
