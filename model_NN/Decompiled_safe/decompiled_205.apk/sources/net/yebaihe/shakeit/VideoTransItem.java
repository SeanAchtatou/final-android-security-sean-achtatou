package net.yebaihe.shakeit;

import android.app.Activity;
import android.database.Cursor;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class VideoTransItem extends TransItem {
    private int curPacket = 0;
    private Integer mVideoId;
    /* access modifiers changed from: private */
    public MediaScannerConnection m_pScanner;

    public VideoTransItem(Activity ctx, Integer videoid) {
        super(ctx);
        this.mVideoId = videoid;
        this.cntType = 3;
        if (this.mVideoId.intValue() != -1) {
            Uri uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
            Cursor cursor = this.mctx.managedQuery(uri, new String[]{"_id", "_size", "_data"}, "_id=" + this.mVideoId, null, "");
            cursor.moveToFirst();
            this.size = cursor.getLong(cursor.getColumnIndexOrThrow("_size"));
            this.path = cursor.getString(cursor.getColumnIndexOrThrow("_data"));
            Log.d("myown", this.path);
            cursor.close();
        }
    }

    public String getName() {
        if (this.mVideoId.intValue() == -1) {
            return this.name;
        }
        return this.path.substring(this.path.lastIndexOf("/") + 1);
    }

    public int getPacketNum() {
        int num = (int) (this.size / 800);
        if (((long) (num * 800)) < this.size) {
            return num + 1;
        }
        return num;
    }

    public int getPacketLength(int curPacketIdx) {
        if (curPacketIdx < getPacketNum() - 1) {
            return 800;
        }
        return (int) (this.size - ((long) (curPacketIdx * 800)));
    }

    public byte[] getPacket(int curPacketIdx) throws IOException {
        FileInputStream in = new FileInputStream(this.path);
        in.skip((long) (curPacketIdx * 800));
        byte[] bytes = new byte[getPacketLength(curPacketIdx)];
        in.read(bytes);
        return bytes;
    }

    public boolean fillPacket(int packetidx, int packetTotal, byte[] dst) throws IOException {
        boolean z;
        if (packetidx != this.curPacket) {
            Log.d("myown", "package idx does not match");
            return false;
        }
        this.curPacket = packetidx;
        if (packetidx != 0) {
            z = true;
        } else {
            z = false;
        }
        FileOutputStream out = getOutputStream(z);
        out.write(dst);
        out.flush();
        out.close();
        this.curPacket++;
        if (this.curPacket == packetTotal) {
            this.mctx.runOnUiThread(new Runnable() {
                public void run() {
                    VideoTransItem.this.scanfile(VideoTransItem.this.getOutputPath());
                }
            });
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void scanfile(String outputPath) {
        final String szFile = outputPath;
        this.m_pScanner = new MediaScannerConnection(this.mctx, new MediaScannerConnection.MediaScannerConnectionClient() {
            public void onMediaScannerConnected() {
                VideoTransItem.this.m_pScanner.scanFile(szFile, null);
            }

            public void onScanCompleted(String path, Uri uri) {
                if (path.equals(szFile)) {
                    VideoTransItem.this.m_pScanner.disconnect();
                }
            }
        });
        this.m_pScanner.connect();
    }
}
