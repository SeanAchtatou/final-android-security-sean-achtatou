package net.yebaihe.shakeit;

import android.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

public class ProgressDialog {
    private AlertDialog mDialog;
    private ProgressBar mProgress;
    private TextView mText;

    public ProgressDialog(main ctx) {
        View textEntryView = LayoutInflater.from(ctx).inflate((int) R.layout.progress, (ViewGroup) null);
        this.mProgress = (ProgressBar) textEntryView.findViewById(R.id.progressBar1);
        this.mText = (TextView) textEntryView.findViewById(R.id.idProgressTxt);
        this.mDialog = new AlertDialog.Builder(ctx).setIcon((int) R.drawable.icon).setOnCancelListener(ctx).setView(textEntryView).create();
    }

    public void updateValue(String hint, int total, int cur) {
        this.mDialog.setTitle(hint);
        this.mProgress.setMax(total);
        this.mProgress.setProgress(cur);
        this.mText.setText(String.format("%d/%d", Integer.valueOf(cur), Integer.valueOf(total)));
        if (!this.mDialog.isShowing()) {
            this.mDialog.show();
        }
    }

    public void close() {
        this.mDialog.hide();
    }
}
