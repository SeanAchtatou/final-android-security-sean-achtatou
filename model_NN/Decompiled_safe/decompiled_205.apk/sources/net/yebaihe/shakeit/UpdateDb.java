package net.yebaihe.shakeit;

import android.app.Service;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.os.IBinder;
import java.io.File;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;
import java.util.zip.ZipException;

public class UpdateDb extends Service {
    static final int CONST_APK = 5;
    static final int CONST_DIR = 2;
    static final int CONST_MP3 = 4;
    static final int CONST_OTHER = 0;
    static final int CONST_PIC_VIDEO = 3;
    private static final long INTERVAL = 3600000;
    private SQLiteDatabase db;
    private MyDbHelper m_Helper;
    private Timer timer = new Timer();

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
        this.m_Helper = new MyDbHelper(this, MyDbHelper.DB_NAME, null, 2);
        startservice();
    }

    public void onDestroy() {
        stopservice();
    }

    private void stopservice() {
        if (this.timer != null) {
            this.timer.cancel();
        }
    }

    private void startservice() {
        this.timer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                UpdateDb.this.updatedb();
            }
        }, 0, (long) INTERVAL);
    }

    /* access modifiers changed from: protected */
    public void updatedb() {
        this.db = this.m_Helper.getWritableDatabase();
        try {
            listDir(Environment.getExternalStorageDirectory());
        } catch (ZipException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        this.db.close();
    }

    private void listDir(File file) throws ZipException, IOException {
        File[] files;
        if (!noChangeInDB(file)) {
            if (file.isDirectory() && (files = file.listFiles()) != null) {
                for (File listDir : files) {
                    listDir(listDir);
                }
            }
            insertEntryIntoDB(file);
        }
    }

    private boolean noChangeInDB(File file) {
        long time = file.lastModified();
        Cursor result = this.db.rawQuery("select * from files where path='" + safeWay(file.getAbsolutePath()) + "' limit 1", null);
        if (!result.moveToFirst() || result.getLong(3) != time) {
            result.close();
            return false;
        }
        result.close();
        return true;
    }

    private String safeWay(String p) {
        return p.replace("'", "|");
    }

    private void insertEntryIntoDB(File file) throws ZipException, IOException {
        int type = 2;
        byte[] bytes = null;
        if (!file.isDirectory()) {
            String lower = file.getAbsolutePath().toLowerCase();
            if (lower.endsWith(".jpg") || lower.endsWith(".gif") || lower.endsWith(".png") || lower.endsWith(".bmp") || lower.endsWith(".3gp") || lower.endsWith(".mp4") || lower.endsWith(".rmvb") || lower.endsWith(".avi") || lower.endsWith(".mkv") || lower.endsWith(".mpg")) {
                type = 3;
            } else if (lower.endsWith(".mp3")) {
                type = 4;
            } else if (lower.endsWith(".apk")) {
                type = 5;
            } else {
                type = 0;
            }
        }
        this.db.execSQL("insert into files (path,type,modifiedat,icon) values ('" + safeWay(file.getAbsolutePath()) + "'," + type + "," + file.lastModified() + ",?)", new Object[]{bytes});
    }
}
