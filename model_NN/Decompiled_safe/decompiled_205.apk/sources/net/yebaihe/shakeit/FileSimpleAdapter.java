package net.yebaihe.shakeit;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class FileSimpleAdapter extends SimpleAdapter {
    ArrayList<File> fileValueList = new ArrayList<>();
    private List<? extends Map<String, ?>> mData;
    private LayoutInflater mInflater;
    /* access modifiers changed from: private */
    public AdaptSelChangeNotify mNotify;
    private int mResource;
    protected long totalSize = 0;

    public FileSimpleAdapter(Context context, List<? extends Map<String, ?>> data, int resource, String[] from, int[] to) {
        super(context, data, resource, from, to);
        this.mData = data;
        this.mResource = resource;
        this.mInflater = (LayoutInflater) context.getSystemService("layout_inflater");
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        return createViewFromResource(position, convertView, parent, this.mResource);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private View createViewFromResource(int position, View convertView, ViewGroup parent, int resource) {
        View v;
        if (convertView == null) {
            v = this.mInflater.inflate(resource, parent, false);
        } else {
            v = convertView;
        }
        bindView(position, v);
        return v;
    }

    private void bindView(int position, View view) {
        boolean z;
        Map map = (Map) this.mData.get(position);
        if (map != null) {
            File f = (File) map.get("file");
            ((TextView) view.findViewById(R.id.nametitle)).setText(f.getName());
            ImageView i = (ImageView) view.findViewById(R.id.idicon);
            CheckBox c = (CheckBox) view.findViewById(R.id.idcheckbox);
            if (f.isDirectory()) {
                c.setVisibility(4);
                i.setImageResource(R.drawable.folder);
                c.setTag(f.getAbsoluteFile());
                return;
            }
            c.setVisibility(0);
            i.setImageResource(R.drawable.file);
            c.setTag(f.getAbsoluteFile());
            if (this.fileValueList.indexOf(f) >= 0) {
                z = true;
            } else {
                z = false;
            }
            c.setChecked(z);
            c.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    File info = (File) buttonView.getTag();
                    if (!isChecked) {
                        int idx = FileSimpleAdapter.this.fileValueList.indexOf(info);
                        if (idx >= 0) {
                            FileSimpleAdapter.this.fileValueList.remove(idx);
                            FileSimpleAdapter.this.totalSize -= info.length();
                        }
                    } else if (FileSimpleAdapter.this.fileValueList.indexOf(info) < 0) {
                        FileSimpleAdapter.this.fileValueList.add(info);
                        FileSimpleAdapter.this.totalSize += info.length();
                    }
                    if (FileSimpleAdapter.this.mNotify != null) {
                        FileSimpleAdapter.this.mNotify.onAdapeSelectionChange("");
                    }
                }
            });
        }
    }

    public void setOnSelectChange(AdaptSelChangeNotify notify) {
        this.mNotify = notify;
    }
}
