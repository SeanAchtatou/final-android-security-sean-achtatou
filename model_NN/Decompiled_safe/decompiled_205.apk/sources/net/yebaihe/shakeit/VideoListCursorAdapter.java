package net.yebaihe.shakeit;

import android.content.Context;
import android.database.Cursor;
import android.provider.MediaStore;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import java.util.ArrayList;

public class VideoListCursorAdapter extends SimpleCursorAdapter {
    /* access modifiers changed from: private */
    public AdaptSelChangeNotify mNotify;
    protected long totalSize = 0;
    protected ArrayList<Integer> videoValueList = new ArrayList<>();

    class VideoInfo {
        int id;
        long size;

        public VideoInfo(int id2, long s) {
            this.id = id2;
            this.size = s;
        }
    }

    public VideoListCursorAdapter(Context context, int layout, Cursor c, String[] from, int[] to) {
        super(context, layout, c, from, to);
    }

    public void bindView(View view, Context context, Cursor cursor) {
        int id = cursor.getInt(cursor.getColumnIndexOrThrow("_id"));
        ((ImageView) view.findViewById(R.id.imgid)).setImageBitmap(MediaStore.Video.Thumbnails.getThumbnail(context.getContentResolver(), (long) id, 3, null));
        long size = cursor.getLong(cursor.getColumnIndexOrThrow("_size"));
        ((TextView) view.findViewById(R.id.size)).setText(String.format("%.2fM", Double.valueOf((((double) size) / 1024.0d) / 1024.0d)));
        CheckBox c = (CheckBox) view.findViewById(R.id.idcheckbox);
        c.setTag(new VideoInfo(id, size));
        c.setChecked(this.videoValueList.indexOf(Integer.valueOf(id)) >= 0);
        c.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                VideoInfo info = (VideoInfo) buttonView.getTag();
                if (!isChecked) {
                    int idx = VideoListCursorAdapter.this.videoValueList.indexOf(Integer.valueOf(info.id));
                    if (idx >= 0) {
                        VideoListCursorAdapter.this.videoValueList.remove(idx);
                        VideoListCursorAdapter.this.totalSize -= info.size;
                    }
                } else if (VideoListCursorAdapter.this.videoValueList.indexOf(Integer.valueOf(info.id)) < 0) {
                    VideoListCursorAdapter.this.videoValueList.add(Integer.valueOf(info.id));
                    VideoListCursorAdapter.this.totalSize += info.size;
                }
                if (VideoListCursorAdapter.this.mNotify != null) {
                    VideoListCursorAdapter.this.mNotify.onAdapeSelectionChange("");
                }
            }
        });
        super.bindView(view, context, cursor);
    }

    public void setOnSelectChange(AdaptSelChangeNotify notify) {
        this.mNotify = notify;
    }
}
