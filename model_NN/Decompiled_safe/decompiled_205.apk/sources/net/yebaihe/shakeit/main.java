package net.yebaihe.shakeit;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.os.Vibrator;
import android.provider.MediaStore;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;
import com.waps.AppConnect;
import com.waps.UpdatePointsNotifier;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import net.yebaihe.shakeit.ApkCursorAdapter;
import net.yebaihe.shakeit.Mp3ListCursorAdapter;
import net.yebaihe.shakeit.PicListCursorAdapter;
import net.yebaihe.shakeit.ShakeListener;
import net.yebaihe.shakeit.VideoListCursorAdapter;

public class main extends Activity implements AdaptSelChangeNotify, UpdatePointsNotifier, DialogInterface.OnCancelListener {
    private static final String PREFS_NAME = "SHAKEIT";
    private static final int UNLOCK_NEED_TOTAL = 200;
    private ApkCursorAdapter apkadapter;
    private ListView apklistview;
    /* access modifiers changed from: private */
    public String baseDir = (String.valueOf(Environment.getExternalStorageDirectory().getAbsolutePath()) + "/shakeit");
    /* access modifiers changed from: private */
    public ImageView btnImgApk;
    /* access modifiers changed from: private */
    public ImageView btnImgFile;
    /* access modifiers changed from: private */
    public ImageView btnImgMusic;
    /* access modifiers changed from: private */
    public ImageView btnImgPic;
    /* access modifiers changed from: private */
    public ImageView btnImgVideo;
    private byte[] bytes = new byte[1024];
    private Handler cancelHandler = new Handler();
    private Runnable cancelRunable = new Runnable() {
        public void run() {
            main.this.sendUserCancelRequest();
        }
    };
    private Runnable confirmFailRunable = new Runnable() {
        public void run() {
            main.this.sendContent();
        }
    };
    /* access modifiers changed from: private */
    public int curPacketIdx;
    protected String curPeer;
    /* access modifiers changed from: private */
    public String curPeerName = "";
    protected String curTab = "pic";
    /* access modifiers changed from: private */
    public int curTransIdx;
    /* access modifiers changed from: private */
    public TransItem curTransItem;
    private SQLiteDatabase db;
    private FileSimpleAdapter fileadapter;
    private ListView filelistview;
    private Handler handleLogin = new Handler() {
        public void handleMessage(Message message) {
            switch (message.what) {
                case 2:
                    String response = (String) message.obj;
                    main.this.logined = true;
                    if (response.trim().length() > 0) {
                        main.this.hintText = String.valueOf(main.this.getString(R.string.app_name)) + ":" + response.trim();
                        main.this.showHint(main.this.hintText);
                        return;
                    }
                    return;
                case 3:
                default:
                    return;
            }
        }
    };
    private Handler handlePeer = new Handler() {
        public void handleMessage(Message message) {
            switch (message.what) {
                case 2:
                    String response = (String) message.obj;
                    Log.d("myown", "peer:" + response);
                    String[] ret = response.trim().split("\n");
                    main.this.peers.clear();
                    for (String peer : ret) {
                        String[] peerinfo = peer.trim().split(" ", 2);
                        if (peerinfo.length > 1) {
                            main.this.addPeer(peerinfo[0], peerinfo[1]);
                        } else if (peerinfo.length == 1) {
                            try {
                                main.this.mUsedTimes = Integer.parseInt(peerinfo[0]);
                            } catch (Exception e) {
                                main.this.mUsedTimes = 0;
                            }
                            SharedPreferences.Editor editor = main.this.getSharedPreferences(main.PREFS_NAME, 0).edit();
                            editor.putInt("usedtimes", main.this.mUsedTimes);
                            editor.commit();
                        }
                    }
                    if (main.this.mUsedTimes < 5 || main.this.mUnlocked) {
                        main.this.doTransFer();
                        return;
                    } else {
                        main.this.warnJifen();
                        return;
                    }
                case 3:
                default:
                    return;
            }
        }
    };
    protected String hintText = "";
    private TextView hintview;
    /* access modifiers changed from: private */
    public Intent intent;
    /* access modifiers changed from: private */
    public boolean isTransfering = false;
    /* access modifiers changed from: private */
    public boolean logined = false;
    private String mLocalIP;
    private long mMyId;
    /* access modifiers changed from: private */
    public String mName = "";
    /* access modifiers changed from: private */
    public DatagramPacket mPacket = new DatagramPacket(this.bytes, this.bytes.length);
    /* access modifiers changed from: private */
    public ProgressDialog mProgressDialog = null;
    private ShakeListener mShake;
    /* access modifiers changed from: private */
    public DatagramSocket mSocket;
    /* access modifiers changed from: private */
    public ArrayList<TransItem> mTransItems;
    /* access modifiers changed from: private */
    public boolean mUnlocked = false;
    /* access modifiers changed from: private */
    public int mUsedTimes = 0;
    private boolean mUserCanceled = false;
    private MyDbHelper m_Helper;
    /* access modifiers changed from: private */
    public int moneyMode = -1;
    private Mp3ListCursorAdapter mp3adapter;
    private ListView mp3listview;
    private boolean noWifi = false;
    private long peerId;
    /* access modifiers changed from: private */
    public ArrayList<Peer> peers = new ArrayList<>();
    private int peertotal;
    private PicListCursorAdapter picadapter;
    private ListView piclistview;
    private Handler shakeHandHandler = new Handler();
    private Runnable shakeHandRunable = new Runnable() {
        public void run() {
            Toast.makeText(main.this, (int) R.string.NoPeerFoound, 1).show();
        }
    };
    private int totalMoney = -1;
    private Vibrator vibrator;
    private VideoListCursorAdapter videoadapter;
    private ListView videolistview;
    private Handler waitConfirmHandler = new Handler();
    private boolean waitingShakeConfirm = false;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppConnect.getInstance(this);
        checkWifi();
        getLocalIP();
        prepareDb();
        setupLocalServer();
        startUpdateDbService();
        checkPref();
        registerSenser();
        doLayout();
        login();
    }

    /* access modifiers changed from: protected */
    public void sendUserCancelRequest() {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        DataOutputStream dos = new DataOutputStream(bos);
        try {
            dos.writeInt(65286);
            dos.writeLong(this.mMyId);
            dos.writeLong(this.peerId);
            dos.flush();
            byte[] bytes2 = bos.toByteArray();
            try {
                this.mSocket.send(new DatagramPacket(bytes2, bytes2.length, InetAddress.getByName(this.curPeer), 2208));
                this.cancelHandler.postDelayed(this.cancelRunable, 1);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void warnJifen() {
        this.moneyMode = 0;
        AppConnect.getInstance(this).getPoints(this);
    }

    private void checkWifi() {
        if (!((ConnectivityManager) getSystemService("connectivity")).getNetworkInfo(1).isConnected()) {
            this.noWifi = true;
            Toast.makeText(this, (int) R.string.nowifi, 1).show();
        }
    }

    private void setupLocalServer() {
        this.mMyId = SystemClock.elapsedRealtime();
        try {
            this.mSocket = new DatagramSocket(2208);
            startReceiveThread();
        } catch (SocketException e) {
            Log.d("myown", "create server socket error:");
            e.printStackTrace();
        }
    }

    private void startReceiveThread() {
        new Thread(new Runnable() {
            public void run() {
                while (true) {
                    try {
                        main.this.mSocket.receive(main.this.mPacket);
                        DataInputStream in = new DataInputStream(new ByteArrayInputStream(main.this.mPacket.getData()));
                        main.this.curPeer = main.this.mPacket.getAddress().toString().substring(1);
                        if (main.this.curPeerName.length() <= 0) {
                            main.this.updateCurPeerName();
                        }
                        main.this.processInPacket(in);
                    } catch (Exception e) {
                        Log.d("myown", "receive thread exit because of exception");
                        e.printStackTrace();
                        return;
                    }
                }
            }
        }).start();
    }

    /* access modifiers changed from: protected */
    public void updateCurPeerName() {
        for (int i = 0; i < this.peers.size(); i++) {
            if (this.peers.get(i).mip.equals(this.curPeer)) {
                this.curPeerName = this.peers.get(i).mname;
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void processInPacket(DataInputStream in) throws IOException {
        switch (in.readInt()) {
            case 65281:
                this.mUserCanceled = false;
                processShakeRequest(in);
                return;
            case 65282:
                this.mUserCanceled = false;
                this.shakeHandHandler.removeCallbacks(this.shakeHandRunable);
                this.isTransfering = true;
                processShakeResponse(in);
                return;
            case 65283:
                processDataRequest(in);
                return;
            case 65284:
                processDataResponse(in);
                return;
            case 65285:
                processDataFinish(in);
                return;
            case 65286:
                processUserCancel(in);
                return;
            case 65287:
                this.cancelHandler.removeCallbacks(this.cancelRunable);
                return;
            default:
                return;
        }
    }

    private void processUserCancel(DataInputStream in) {
        this.mUserCanceled = true;
        cancelTrans();
        sendCancelConfirm();
    }

    private void sendCancelConfirm() {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        DataOutputStream dos = new DataOutputStream(bos);
        try {
            dos.writeInt(65287);
            dos.writeLong(this.mMyId);
            dos.writeLong(this.peerId);
            dos.flush();
            byte[] bytes2 = bos.toByteArray();
            try {
                this.mSocket.send(new DatagramPacket(bytes2, bytes2.length, InetAddress.getByName(this.curPeer), 2208));
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    private void processDataFinish(DataInputStream in) throws IOException {
        if (in.readLong() != this.peerId) {
            Log.d("myown", "peerId not valid");
        } else if (in.readLong() != this.mMyId) {
            Log.d("myown", "MyId not valid");
        } else {
            this.isTransfering = false;
            runOnUiThread(new Runnable() {
                public void run() {
                    main.this.mProgressDialog.close();
                    main.this.mProgressDialog = null;
                    main.this.getWindow().clearFlags(128);
                    Toast.makeText(main.this, (int) R.string.StoreHint, 1).show();
                }
            });
        }
    }

    private void processDataResponse(DataInputStream in) throws IOException {
        this.waitConfirmHandler.removeCallbacks(this.confirmFailRunable);
        if (this.mUserCanceled) {
            cancelTrans();
        } else if (in.readLong() != this.peerId) {
            Log.d("myown", "peerId not valid");
        } else if (in.readLong() != this.mMyId) {
            Log.d("myown", "MyId not valid");
        } else if (in.readInt() != this.curPacketIdx) {
            Log.d("myown", "packetIdx not valid!");
        } else {
            this.curPacketIdx++;
            if (this.curPacketIdx == this.mTransItems.get(this.curTransIdx).getPacketNum()) {
                this.curPacketIdx = 0;
                this.curTransIdx++;
                if (this.curTransIdx == this.mTransItems.size()) {
                    Log.d("myown", "trans finish");
                    this.isTransfering = false;
                    sendContentComplete();
                    runOnUiThread(new Runnable() {
                        public void run() {
                            main.this.mProgressDialog.close();
                            main.this.mProgressDialog = null;
                            main.this.getWindow().clearFlags(128);
                            main.this.updateServerUsedTimers();
                        }
                    });
                    return;
                }
            }
            sendContent();
        }
    }

    /* access modifiers changed from: private */
    public void updateServerUsedTimers() {
        new HttpConnection(new Handler()).get("http://lily.newim.net/appstore/shakeuptimes.php?imei=" + ((TelephonyManager) getSystemService("phone")).getDeviceId());
    }

    private void sendContentComplete() {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        DataOutputStream dos = new DataOutputStream(bos);
        try {
            dos.writeInt(65285);
            dos.writeLong(this.mMyId);
            dos.writeLong(this.peerId);
            dos.flush();
            byte[] bytes2 = bos.toByteArray();
            try {
                this.mSocket.send(new DatagramPacket(bytes2, bytes2.length, InetAddress.getByName(this.curPeer), 2208));
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    private void processDataRequest(DataInputStream in) throws IOException {
        if (this.mUserCanceled) {
            cancelTrans();
            return;
        }
        this.isTransfering = true;
        if (in.readLong() != this.peerId) {
            Log.d("myown", "peerId not valid");
        } else if (in.readLong() != this.mMyId) {
            Log.d("myown", "MyId not valid");
        } else {
            int cntType = in.readInt();
            final int packetidx = in.readInt();
            byte[] dst = new byte[in.readInt()];
            in.readFully(dst);
            String name = new String(dst, "UTF-8");
            if (packetidx == 0) {
                switch (cntType) {
                    case 1:
                        this.curTransItem = new PicTransItem(this, -1);
                        this.curTransItem.name = name;
                        break;
                    case 2:
                        this.curTransItem = new Mp3TransItem(this, -1);
                        this.curTransItem.name = name;
                        break;
                    case 3:
                        this.curTransItem = new VideoTransItem(this, -1);
                        this.curTransItem.name = name;
                        break;
                    case 4:
                        this.curTransItem = new ApkTransItem(this, null);
                        this.curTransItem.name = name;
                        break;
                    case 5:
                        this.curTransItem = new FileTransItem(this, null);
                        this.curTransItem.name = name;
                        break;
                }
            }
            final int packetTotal = in.readInt();
            runOnUiThread(new Runnable() {
                public void run() {
                    main.this.updateProgressDialog(String.format(main.this.getString(R.string.Receiving), main.this.curPeerName, main.this.curTransItem.getName()), packetTotal, packetidx);
                }
            });
            byte[] dst2 = new byte[in.readInt()];
            in.readFully(dst2);
            this.curTransItem.fillPacket(packetidx, packetTotal, dst2);
            sendDataConfirm(packetidx);
        }
    }

    private void sendDataConfirm(int packetidx) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        DataOutputStream dos = new DataOutputStream(bos);
        try {
            dos.writeInt(65284);
            dos.writeLong(this.mMyId);
            dos.writeLong(this.peerId);
            dos.writeInt(packetidx);
            dos.flush();
            byte[] bytes2 = bos.toByteArray();
            try {
                this.mSocket.send(new DatagramPacket(bytes2, bytes2.length, InetAddress.getByName(this.curPeer), 2208));
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    private void processShakeResponse(DataInputStream in) throws IOException {
        Log.d("myown", "shake response");
        this.peerId = in.readLong();
        if (in.readLong() == this.mMyId) {
            this.curTransIdx = 0;
            this.curPacketIdx = 0;
            sendContent();
        }
    }

    /* access modifiers changed from: private */
    public void sendContent() {
        if (this.mUserCanceled) {
            cancelTrans();
            return;
        }
        TransItem item = this.mTransItems.get(this.curTransIdx);
        runOnUiThread(new Runnable() {
            public void run() {
                if (main.this.curTransIdx >= 0 && main.this.curTransIdx < main.this.mTransItems.size()) {
                    TransItem item = (TransItem) main.this.mTransItems.get(main.this.curTransIdx);
                    main.this.updateProgressDialog(String.format(main.this.getString(R.string.Sending), main.this.curPeerName, item.getName()), item.getPacketNum(), main.this.curPacketIdx);
                }
            }
        });
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        DataOutputStream dos = new DataOutputStream(bos);
        try {
            dos.writeInt(65283);
            dos.writeLong(this.mMyId);
            dos.writeLong(this.peerId);
            dos.writeInt(item.cntType);
            dos.writeInt(this.curPacketIdx);
            byte[] bytes2 = item.getName().getBytes("UTF-8");
            dos.writeInt(bytes2.length);
            dos.write(bytes2);
            dos.writeInt(item.getPacketNum());
            byte[] bytes3 = item.getPacket(this.curPacketIdx);
            dos.writeInt(bytes3.length);
            dos.write(bytes3);
            dos.flush();
            byte[] bytes4 = bos.toByteArray();
            try {
                this.mSocket.send(new DatagramPacket(bytes4, bytes4.length, InetAddress.getByName(this.curPeer), 2208));
            } catch (IOException e) {
                IOException e2 = e;
                Log.d("myown", "exception on send content to peer!");
                e2.printStackTrace();
            }
            this.waitConfirmHandler.postDelayed(this.confirmFailRunable, 3000);
        } catch (IOException e3) {
            e3.printStackTrace();
        }
    }

    private void cancelTrans() {
        this.mUserCanceled = true;
        runOnUiThread(new Runnable() {
            public void run() {
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        if (main.this.mProgressDialog != null) {
                            Log.d("myown", "need close progress dialog");
                            main.this.mProgressDialog.close();
                            main.this.mProgressDialog = null;
                            main.this.isTransfering = false;
                            if (main.this.curTransItem != null) {
                                main.this.curTransItem.cancelTrans();
                            }
                        }
                    }
                }, 1000);
            }
        });
    }

    /* access modifiers changed from: private */
    public void updateProgressDialog(String hint, int total, int cur) {
        if (this.mUserCanceled) {
            cancelTrans();
            return;
        }
        if (this.mProgressDialog == null) {
            Log.d("myown", "need create progress dialog");
            this.mProgressDialog = new ProgressDialog(this);
            getWindow().setFlags(128, 128);
        }
        this.mProgressDialog.updateValue(hint, total, cur);
    }

    private void processShakeRequest(DataInputStream in) throws IOException {
        Log.d("myown", "shake request");
        this.peerId = in.readLong();
        this.peertotal = in.readInt();
        byte[] b = new byte[in.readInt()];
        in.readFully(b);
        this.curPeerName = new String(b, "UTF-8");
        if (this.peertotal == 1) {
            sendShakeResponse();
            doShake();
            return;
        }
        waitShakeConfirm();
    }

    private void waitShakeConfirm() {
        this.waitingShakeConfirm = true;
    }

    private void sendShakeResponse() {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        DataOutputStream dos = new DataOutputStream(bos);
        try {
            dos.writeInt(65282);
            dos.writeLong(this.mMyId);
            dos.writeLong(this.peerId);
            dos.flush();
            byte[] bytes2 = bos.toByteArray();
            try {
                this.mSocket.send(new DatagramPacket(bytes2, bytes2.length, InetAddress.getByName(this.curPeer), 2208));
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    public void onBackPressed() {
        if (this.curTab.equals("anything")) {
            String up = upDir(this.baseDir);
            if (!up.equals(this.baseDir)) {
                this.baseDir = up;
                refreshFileList();
                return;
            }
        }
        this.mSocket.close();
        this.mShake.pause();
        AppConnect.getInstance(this);
        finish();
    }

    private String upDir(String base) {
        int idx = base.lastIndexOf("/");
        if (idx <= 0) {
            return base;
        }
        String base2 = base.substring(0, idx);
        if (base2.length() < Environment.getExternalStorageDirectory().getAbsolutePath().length()) {
            return Environment.getExternalStorageDirectory().getAbsolutePath();
        }
        return base2;
    }

    /* access modifiers changed from: protected */
    public void doTransFer() {
        for (int i = 0; i < this.peers.size(); i++) {
            this.peers.get(i).tryShakeHands(this.peers.size(), this.mName);
        }
        this.shakeHandHandler.postDelayed(this.shakeHandRunable, 5000);
        if (this.peers.size() > 1) {
            Toast.makeText(this, (int) R.string.ShakePeerHint, 1).show();
        } else if (this.peers.size() == 0) {
            Toast.makeText(this, (int) R.string.NoPeerFoound, 1).show();
        }
    }

    /* access modifiers changed from: protected */
    public void addPeer(String ip, String name) {
        if (ip.trim().length() > 0) {
            this.peers.add(new Peer(ip.trim(), name.trim(), this.mSocket, this.mMyId));
        }
    }

    private void registerSenser() {
        this.mShake = new ShakeListener(this);
        this.mShake.setOnShakeListener(new ShakeListener.OnShakeListener() {
            public void onShake() {
                main.this.onShake();
            }
        });
        this.vibrator = (Vibrator) getSystemService("vibrator");
    }

    private void prepareDb() {
        this.m_Helper = new MyDbHelper(this, MyDbHelper.DB_NAME, null, 2);
        this.db = this.m_Helper.getWritableDatabase();
    }

    private void startUpdateDbService() {
        this.intent = new Intent(this, UpdateDb.class);
        startService(this.intent);
    }

    public static String getAppVersionName(Context context) {
        String versionName = "";
        try {
            versionName = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
            if (versionName == null || versionName.length() <= 0) {
                return "";
            }
        } catch (Exception e) {
        }
        return versionName;
    }

    private void doLayout() {
        boolean customTitleSupported = requestWindowFeature(7);
        setContentView(R.layout.main);
        if (customTitleSupported) {
            getWindow().setFeatureInt(7, R.layout.titlebar);
            TextView myTitleText = (TextView) findViewById(R.id.idtitletext);
            if (myTitleText != null) {
                myTitleText.setText(String.valueOf(getString(R.string.app_name)) + " " + getString(R.string.copyright) + getAppVersionName(this));
            }
        }
        this.hintview = (TextView) findViewById(R.id.idhint);
        RelativeLayout picbtn = (RelativeLayout) LayoutInflater.from(this).inflate((int) R.layout.picvideo, (ViewGroup) null);
        this.btnImgPic = (ImageView) picbtn.findViewById(R.id.imgbtnpic);
        RelativeLayout mp3btn = (RelativeLayout) LayoutInflater.from(this).inflate((int) R.layout.mp3, (ViewGroup) null);
        this.btnImgMusic = (ImageView) mp3btn.findViewById(R.id.imgbtnmusic);
        RelativeLayout apkbtn = (RelativeLayout) LayoutInflater.from(this).inflate((int) R.layout.apks, (ViewGroup) null);
        this.btnImgApk = (ImageView) apkbtn.findViewById(R.id.imgbtnapk);
        RelativeLayout videobtn = (RelativeLayout) LayoutInflater.from(this).inflate((int) R.layout.video, (ViewGroup) null);
        this.btnImgVideo = (ImageView) videobtn.findViewById(R.id.imgbtnvideo);
        RelativeLayout filebtn = (RelativeLayout) LayoutInflater.from(this).inflate((int) R.layout.anything, (ViewGroup) null);
        this.btnImgFile = (ImageView) filebtn.findViewById(R.id.btnimgfile);
        LinearLayout piclist = (LinearLayout) LayoutInflater.from(this).inflate((int) R.layout.piclists, (ViewGroup) null);
        this.piclistview = (ListView) piclist.findViewById(R.id.piclist);
        this.piclistview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int arg2, long arg3) {
                Intent intent = new Intent();
                intent.setAction("android.intent.action.VIEW");
                intent.setDataAndType(Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new StringBuilder().append(((PicListCursorAdapter.PicInfo) ((CheckBox) view.findViewById(R.id.idcheckbox)).getTag()).id).toString()), "image/*");
                main.this.startActivity(intent);
            }
        });
        refreshPicList();
        ((LinearLayout) findViewById(R.id.tab1)).addView(piclist, new LinearLayout.LayoutParams(-1, -2));
        LinearLayout mp3list = (LinearLayout) LayoutInflater.from(this).inflate((int) R.layout.mp3lists, (ViewGroup) null);
        this.mp3listview = (ListView) mp3list.findViewById(R.id.mp3list);
        this.mp3listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int arg2, long arg3) {
                Intent intent = new Intent();
                intent.setAction("android.intent.action.VIEW");
                intent.setDataAndType(Uri.withAppendedPath(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, new StringBuilder().append(((Mp3ListCursorAdapter.Mp3Info) ((CheckBox) view.findViewById(R.id.idcheckbox)).getTag()).id).toString()), "audio/*");
                main.this.startActivity(intent);
            }
        });
        ((LinearLayout) findViewById(R.id.tab2)).addView(mp3list, new LinearLayout.LayoutParams(-1, -2));
        LinearLayout videolist = (LinearLayout) LayoutInflater.from(this).inflate((int) R.layout.videolists, (ViewGroup) null);
        this.videolistview = (ListView) videolist.findViewById(R.id.videolist);
        this.videolistview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int arg2, long arg3) {
                Intent intent = new Intent();
                intent.setAction("android.intent.action.VIEW");
                intent.setDataAndType(Uri.withAppendedPath(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, new StringBuilder().append(((VideoListCursorAdapter.VideoInfo) ((CheckBox) view.findViewById(R.id.idcheckbox)).getTag()).id).toString()), "video/*");
                main.this.startActivity(intent);
            }
        });
        ((LinearLayout) findViewById(R.id.tab3)).addView(videolist, new LinearLayout.LayoutParams(-1, -2));
        LinearLayout apklist = (LinearLayout) LayoutInflater.from(this).inflate((int) R.layout.apklists, (ViewGroup) null);
        this.apklistview = (ListView) apklist.findViewById(R.id.apklist);
        this.apklistview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int arg2, long arg3) {
                main.this.intent = new Intent("android.intent.action.VIEW");
                main.this.intent.setDataAndType(Uri.fromFile(new File(((ApkCursorAdapter.ApkInfo) ((CheckBox) view.findViewById(R.id.idcheckbox)).getTag()).path)), "application/vnd.android.package-archive");
                main.this.startActivity(main.this.intent);
            }
        });
        ((LinearLayout) findViewById(R.id.tab4)).addView(apklist, new LinearLayout.LayoutParams(-1, -2));
        LinearLayout filelist = (LinearLayout) LayoutInflater.from(this).inflate((int) R.layout.filelists, (ViewGroup) null);
        this.filelistview = (ListView) filelist.findViewById(R.id.filelist);
        this.filelistview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int arg2, long arg3) {
                File info = (File) ((CheckBox) view.findViewById(R.id.idcheckbox)).getTag();
                if (info.isDirectory()) {
                    main main = main.this;
                    main.baseDir = String.valueOf(main.baseDir) + "/" + ((Object) ((TextView) view.findViewById(R.id.nametitle)).getText());
                    main.this.refreshFileList();
                    return;
                }
                try {
                    String mime = MimeTypeMap.getSingleton().getMimeTypeFromExtension(main.this.getFileExt(info.getAbsolutePath()));
                    if (mime != null && mime.length() > 0) {
                        main.this.intent = new Intent("android.intent.action.VIEW");
                        main.this.intent.setDataAndType(Uri.fromFile(info), mime);
                        main.this.startActivity(main.this.intent);
                    }
                } catch (Exception e) {
                }
            }
        });
        ((LinearLayout) findViewById(R.id.tab5)).addView(filelist, new LinearLayout.LayoutParams(-1, -2));
        TabHost tabHost = (TabHost) findViewById(R.id.tabhost);
        tabHost.setup();
        tabHost.addTab(tabHost.newTabSpec("pic").setContent((int) R.id.tab1).setIndicator(picbtn));
        tabHost.addTab(tabHost.newTabSpec("mp3").setContent((int) R.id.tab2).setIndicator(mp3btn));
        tabHost.addTab(tabHost.newTabSpec("video").setContent((int) R.id.tab3).setIndicator(videobtn));
        tabHost.addTab(tabHost.newTabSpec("apks").setContent((int) R.id.tab4).setIndicator(apkbtn));
        tabHost.addTab(tabHost.newTabSpec("anything").setContent((int) R.id.tab5).setIndicator(filebtn));
        tabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            public void onTabChanged(String tabId) {
                main.this.curTab = tabId;
                main.this.btnImgPic.setImageResource(R.drawable.c1a);
                main.this.btnImgMusic.setImageResource(R.drawable.c2a);
                main.this.btnImgVideo.setImageResource(R.drawable.c4a);
                main.this.btnImgApk.setImageResource(R.drawable.c3a);
                main.this.btnImgFile.setImageResource(R.drawable.c5a);
                if (tabId.equals("pic")) {
                    main.this.btnImgPic.setImageResource(R.drawable.c1b);
                    main.this.refreshPicList();
                } else if (tabId.equals("mp3")) {
                    main.this.btnImgMusic.setImageResource(R.drawable.c2b);
                    main.this.refreshMp3List();
                } else if (tabId.equals("video")) {
                    main.this.btnImgVideo.setImageResource(R.drawable.c4b);
                    main.this.refreshVideoList();
                } else if (tabId.equals("apks")) {
                    main.this.btnImgApk.setImageResource(R.drawable.c3b);
                    main.this.refreshApkList();
                } else {
                    main.this.btnImgFile.setImageResource(R.drawable.c5b);
                    main.this.refreshFileList();
                }
                main.this.onAdapeSelectionChange("");
            }
        });
        tabHost.setCurrentTab(0);
    }

    /* access modifiers changed from: protected */
    public String getFileExt(String path) {
        return path.substring(path.lastIndexOf(".") + 1);
    }

    /* access modifiers changed from: private */
    public void refreshFileList() {
        this.fileadapter = new FileSimpleAdapter(this, getFiles(), R.layout.filerow, new String[0], new int[0]);
        this.fileadapter.setOnSelectChange(this);
        this.filelistview.setAdapter((ListAdapter) this.fileadapter);
    }

    private List<Map<String, Object>> getFiles() {
        File base = new File(this.baseDir);
        Log.d("myown", this.baseDir);
        List<Map<String, Object>> contents = new ArrayList<>();
        File[] files = base.listFiles();
        if (files != null) {
            for (File f : files) {
                if (f.isDirectory()) {
                    Map<String, Object> map = new HashMap<>();
                    map.put("name", f.getPath());
                    map.put("file", f);
                    contents.add(map);
                }
            }
            for (File f2 : files) {
                if (!f2.isDirectory()) {
                    Map<String, Object> map2 = new HashMap<>();
                    map2.put("name", f2.getPath());
                    map2.put("file", f2);
                    contents.add(map2);
                }
            }
        }
        return contents;
    }

    /* access modifiers changed from: private */
    public void refreshApkList() {
        this.apkadapter = new ApkCursorAdapter(this, R.layout.apkrow, this.db.rawQuery("select * from files where type=5", null), new String[0], new int[0]);
        this.apkadapter.setOnSelectChange(this);
        this.apklistview.setAdapter((ListAdapter) this.apkadapter);
    }

    /* access modifiers changed from: private */
    public void refreshVideoList() {
        this.videoadapter = new VideoListCursorAdapter(this, R.layout.videorow, managedQuery(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, new String[]{"_id", "datetaken", "_size"}, "_size>500000", null, ""), new String[0], new int[0]);
        this.videoadapter.setOnSelectChange(this);
        this.videolistview.setAdapter((ListAdapter) this.videoadapter);
    }

    /* access modifiers changed from: private */
    public void refreshMp3List() {
        this.mp3adapter = new Mp3ListCursorAdapter(this, R.layout.mp3row, managedQuery(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, new String[]{"_id", "title", "_size"}, "", null, ""), new String[]{"title"}, new int[]{R.id.mp3title});
        this.mp3adapter.setOnSelectChange(this);
        this.mp3listview.setAdapter((ListAdapter) this.mp3adapter);
    }

    /* access modifiers changed from: private */
    public void refreshPicList() {
        this.picadapter = new PicListCursorAdapter(this, R.layout.picrow, managedQuery(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new String[]{"_id", "bucket_display_name", "datetaken", "_size", "_data"}, "_size>100000", null, ""), new String[0], new int[0]);
        this.picadapter.setOnSelectChange(this);
        this.piclistview.setAdapter((ListAdapter) this.picadapter);
    }

    /* access modifiers changed from: private */
    public void login() {
        if (!this.mName.equals("")) {
            String url = "http://lily.newim.net/appstore/shakelogin.php?imei=" + ((TelephonyManager) getSystemService("phone")).getDeviceId() + "&ip=" + this.mLocalIP + "&name=" + this.mName + "&locale=" + (String.valueOf(Locale.getDefault().getLanguage()) + Locale.getDefault().getCountry());
            Log.d("myown", url);
            new HttpConnection(this.handleLogin).get(url);
        }
    }

    private void showHint(int stringid) {
        this.hintview.setText(getString(stringid));
    }

    /* access modifiers changed from: private */
    public void checkPref() {
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        this.mName = settings.getString("name", "");
        this.mUnlocked = settings.getBoolean("unlocked", false);
        this.mUsedTimes = settings.getInt("usedtimes", 0);
        if (this.mName.equals("")) {
            final EditText input = new EditText(this);
            new AlertDialog.Builder(this).setTitle((int) R.string.SetNameDialogTitle).setMessage((int) R.string.SetNameDialogMsg).setView(input).setPositiveButton(17039370, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    main.this.mName = input.getText().toString();
                    SharedPreferences.Editor editor = main.this.getSharedPreferences(main.PREFS_NAME, 0).edit();
                    editor.putString("name", main.this.mName);
                    editor.commit();
                    if (main.this.mName.length() > 0) {
                        main.this.login();
                    } else {
                        main.this.checkPref();
                    }
                }
            }).setCancelable(false).show();
        }
    }

    private void getLocalIP() {
        try {
            Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces();
            while (en.hasMoreElements()) {
                Enumeration<InetAddress> enumIpAddr = en.nextElement().getInetAddresses();
                while (enumIpAddr.hasMoreElements()) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        this.mLocalIP = inetAddress.toString().substring(1);
                        Log.d("myown", this.mLocalIP);
                    }
                }
            }
        } catch (SocketException e) {
            Log.e("WifiPreference IpAddress", e.toString());
        }
    }

    /* access modifiers changed from: private */
    public void onShake() {
        if (!this.isTransfering) {
            if (this.noWifi) {
                Toast.makeText(this, (int) R.string.nowifi, 1).show();
            } else if (this.waitingShakeConfirm) {
                sendShakeResponse();
                doShake();
                this.waitingShakeConfirm = false;
            } else {
                this.mTransItems = getTransItems();
                if (this.mTransItems.size() == 0) {
                    Toast.makeText(this, (int) R.string.MustChooseSomething, 1).show();
                    return;
                }
                doShake();
                Toast.makeText(this, (int) R.string.LookingForPeer, 1).show();
                String imei = ((TelephonyManager) getSystemService("phone")).getDeviceId();
                this.mUserCanceled = false;
                new HttpConnection(this.handlePeer).get("http://lily.newim.net/appstore/shakepeer.php?imei=" + imei + "&ip=" + this.mLocalIP);
            }
        }
    }

    private void doShake() {
        this.vibrator.vibrate(new long[]{10, 500}, -1);
    }

    private ArrayList<TransItem> getTransItems() {
        ArrayList<TransItem> items = new ArrayList<>();
        if (this.curTab.equals("pic")) {
            for (int i = 0; i < this.picadapter.picValueList.size(); i++) {
                items.add(new PicTransItem(this, this.picadapter.picValueList.get(i)));
            }
            this.picadapter.picValueList.clear();
            refreshPicList();
        } else if (this.curTab.equals("mp3")) {
            for (int i2 = 0; i2 < this.mp3adapter.mp3ValueList.size(); i2++) {
                items.add(new Mp3TransItem(this, this.mp3adapter.mp3ValueList.get(i2)));
            }
            this.mp3adapter.mp3ValueList.clear();
            refreshMp3List();
        } else if (this.curTab.equals("video")) {
            for (int i3 = 0; i3 < this.videoadapter.videoValueList.size(); i3++) {
                items.add(new VideoTransItem(this, this.videoadapter.videoValueList.get(i3)));
            }
            this.videoadapter.videoValueList.clear();
            refreshVideoList();
        } else if (this.curTab.equals("apks")) {
            for (int i4 = 0; i4 < this.apkadapter.apkValueList.size(); i4++) {
                items.add(new ApkTransItem(this, this.apkadapter.apkValueList.get(i4)));
            }
            this.apkadapter.apkValueList.clear();
            refreshApkList();
        } else {
            for (int i5 = 0; i5 < this.fileadapter.fileValueList.size(); i5++) {
                items.add(new FileTransItem(this, this.fileadapter.fileValueList.get(i5)));
            }
            this.fileadapter.fileValueList.clear();
            refreshFileList();
        }
        onAdapeSelectionChange("");
        return items;
    }

    private String getName() {
        return Build.MODEL;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        if (this.mUsedTimes < 5) {
            return true;
        }
        menu.clear();
        menu.add(0, 2, 1, (int) R.string.YEBAIHE).setIcon((int) R.drawable.ybh);
        menu.add(0, 3, 1, (int) R.string.COMMENT).setIcon((int) R.drawable.dc);
        menu.add(0, 4, 1, (int) R.string.MORE).setIcon((int) R.drawable.gd);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 2:
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://wap.newim.net")));
                return true;
            case 3:
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://lily.newim.net/appstore/singlecommet.php?id=480")));
                return true;
            case 4:
                AppConnect.getInstance(this).showOffers(this);
                return true;
            default:
                return true;
        }
    }

    public void onAdapeSelectionChange(String hint) {
        int count;
        long size;
        if (this.curTab.equals("pic")) {
            count = this.picadapter.picValueList.size();
            size = this.picadapter.totalSize;
        } else if (this.curTab.equals("mp3")) {
            count = this.mp3adapter.mp3ValueList.size();
            size = this.mp3adapter.totalSize;
        } else if (this.curTab.equals("video")) {
            count = this.videoadapter.videoValueList.size();
            size = this.videoadapter.totalSize;
        } else if (this.curTab.equals("apks")) {
            count = this.apkadapter.apkValueList.size();
            size = this.apkadapter.totalSize;
        } else {
            count = this.fileadapter.fileValueList.size();
            size = this.fileadapter.totalSize;
        }
        String hint2 = String.format(getString(R.string.Sel_Hint), Integer.valueOf(count), Double.valueOf((((double) size) / 1024.0d) / 1024.0d));
        if (count > 0) {
            showHint(hint2);
        } else {
            showHint(this.hintText);
        }
    }

    /* access modifiers changed from: private */
    public void showHint(String hint) {
        this.hintview.setText(hint);
    }

    public void getUpdatePoints(String arg0, int total) {
        this.totalMoney = total;
        runOnUiThread(new Runnable() {
            public void run() {
                switch (main.this.moneyMode) {
                    case 0:
                        main.this.koujifen();
                        return;
                    case 1:
                        main.this.koujifenchenggong();
                        return;
                    default:
                        return;
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void koujifenchenggong() {
        new AlertDialog.Builder(this).setIcon((int) R.drawable.icon).setTitle((int) R.string.GET_JIFEN_SUCC).setMessage(String.format(getString(R.string.JIFEN_HINT), Integer.valueOf(this.totalMoney))).setNeutralButton(17039370, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                main.this.mUnlocked = true;
                SharedPreferences.Editor editor = main.this.getSharedPreferences(main.PREFS_NAME, 0).edit();
                editor.putBoolean("unlocked", main.this.mUnlocked);
                editor.commit();
            }
        }).show();
    }

    /* access modifiers changed from: protected */
    public void koujifen() {
        if (this.totalMoney >= UNLOCK_NEED_TOTAL) {
            new AlertDialog.Builder(this).setIcon((int) R.drawable.icon).setTitle(17039380).setMessage(String.format(getString(R.string.KOU_JIFEN), Integer.valueOf(this.totalMoney), Integer.valueOf((int) UNLOCK_NEED_TOTAL))).setPositiveButton(17039370, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    main.this.moneyMode = 1;
                    new Handler().postDelayed(new Runnable() {
                        public void run() {
                            AppConnect.getInstance(main.this).spendPoints(main.UNLOCK_NEED_TOTAL, main.this);
                        }
                    }, 500);
                }
            }).setCancelable(false).setNeutralButton(17039360, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                }
            }).show();
            return;
        }
        new AlertDialog.Builder(this).setIcon((int) R.drawable.icon).setTitle(17039380).setCancelable(false).setNeutralButton(17039360, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        }).setPositiveButton((int) R.string.GET_JIFEN, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                AppConnect.getInstance(main.this).showOffers(main.this);
            }
        }).setMessage(String.format(getString(R.string.NEED_JIFEN), Integer.valueOf((int) UNLOCK_NEED_TOTAL), Integer.valueOf(this.totalMoney))).show();
    }

    public void getUpdatePointsFailed(String arg0) {
        this.totalMoney = -1;
        runOnUiThread(new Runnable() {
            public void run() {
                new AlertDialog.Builder(main.this).setIcon((int) R.drawable.icon).setTitle(17039380).setMessage((int) R.string.COMM_ERROR).show();
            }
        });
    }

    public void onCancel(DialogInterface dialog) {
        this.mUserCanceled = true;
        Log.d("myown", "user canceld trnas");
        cancelTrans();
        sendUserCancelRequest();
    }
}
