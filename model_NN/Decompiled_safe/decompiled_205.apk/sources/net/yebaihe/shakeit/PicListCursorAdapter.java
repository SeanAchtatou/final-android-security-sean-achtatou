package net.yebaihe.shakeit;

import android.content.Context;
import android.database.Cursor;
import android.provider.MediaStore;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import java.util.ArrayList;

public class PicListCursorAdapter extends SimpleCursorAdapter {
    private Context mCtx;
    /* access modifiers changed from: private */
    public AdaptSelChangeNotify mNotify;
    ArrayList<Integer> picValueList = new ArrayList<>();
    protected long totalSize;

    class PicInfo {
        int id;
        long size;

        public PicInfo(int id2, long s) {
            this.id = id2;
            this.size = s;
        }
    }

    public PicListCursorAdapter(Context context, int layout, Cursor c, String[] from, int[] to) {
        super(context, layout, c, from, to);
        this.mCtx = context;
    }

    private String baseName(String path) {
        return path.substring(path.lastIndexOf("/") + 1);
    }

    public void bindView(View view, Context context, Cursor cursor) {
        int id = cursor.getInt(cursor.getColumnIndexOrThrow("_id"));
        ((ImageView) view.findViewById(R.id.imgid)).setImageBitmap(MediaStore.Images.Thumbnails.getThumbnail(context.getContentResolver(), (long) id, 3, null));
        long size = cursor.getLong(cursor.getColumnIndexOrThrow("_size"));
        ((TextView) view.findViewById(R.id.picpath)).setText(String.valueOf(baseName(cursor.getString(cursor.getColumnIndexOrThrow("_data")))) + String.format(" %.2fM", Float.valueOf((((float) size) / 1024.0f) / 1024.0f)));
        CheckBox c = (CheckBox) view.findViewById(R.id.idcheckbox);
        c.setTag(new PicInfo(id, size));
        c.setChecked(this.picValueList.indexOf(Integer.valueOf(id)) >= 0);
        c.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    PicInfo info = (PicInfo) buttonView.getTag();
                    if (PicListCursorAdapter.this.picValueList.indexOf(Integer.valueOf(info.id)) < 0) {
                        PicListCursorAdapter.this.picValueList.add(Integer.valueOf(info.id));
                        PicListCursorAdapter.this.totalSize += info.size;
                    }
                } else {
                    PicInfo info2 = (PicInfo) buttonView.getTag();
                    int idx = PicListCursorAdapter.this.picValueList.indexOf(Integer.valueOf(info2.id));
                    if (idx >= 0) {
                        PicListCursorAdapter.this.picValueList.remove(idx);
                        PicListCursorAdapter.this.totalSize -= info2.size;
                    }
                }
                if (PicListCursorAdapter.this.mNotify != null) {
                    PicListCursorAdapter.this.mNotify.onAdapeSelectionChange("");
                }
            }
        });
        super.bindView(view, context, cursor);
    }

    public void setOnSelectChange(AdaptSelChangeNotify notify) {
        this.mNotify = notify;
    }
}
