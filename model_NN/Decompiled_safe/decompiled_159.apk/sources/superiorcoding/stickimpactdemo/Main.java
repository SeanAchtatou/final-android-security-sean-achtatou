package superiorcoding.stickimpactdemo;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import java.io.IOException;
import superiorcoding.stickimpactdemo.game.GamePanel;
import superiorcoding.stickimpactdemo.game.GameView;
import superiorcoding.stickimpactdemo.highscore.Highscore;
import superiorcoding.stickimpactdemo.highscore.HighscorePanel;
import superiorcoding.stickimpactdemo.menu.MenuPanel;
import superiorcoding.stickimpactdemo.notice.NoticeView;
import superiorcoding.stickimpactdemo.tutorial.TutorialPanel;

public class Main extends Activity {
    public static Bitmap ANDREJ = null;
    public static Bitmap ANDREJ_TUTORIAL = null;
    public static Bitmap BACKGROUND = null;
    public static int DPI = 0;
    public static int DRAW_SETTING = 0;
    public static final int DRAW_SETTING_LARGE = 3;
    public static final int DRAW_SETTING_MEDIUM = 2;
    public static final int DRAW_SETTING_SMALL = 1;
    public static Typeface FONT = null;
    public static int HEIGHT = 0;
    public static Highscore HIGHSCORE = null;
    public static final String NAME = "Stick Impact";
    public static final int VIEW_GAME = 2;
    public static final int VIEW_HIGHSCORE = 3;
    public static final int VIEW_MENU = 1;
    public static final int VIEW_TUTORIAL = 6;
    public static int WIDTH;
    private int VIEW;
    private RelativeLayout currentView;
    private RelativeLayout gamePanel;
    private RelativeLayout menuPanel;
    private RelativeLayout tutorialPanel;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getScreenDimension();
        getDrawSetting();
        loadRessources();
        getFontTypeFace();
        createHighscoreObject();
        createViews();
        this.currentView = this.menuPanel;
        this.VIEW = 1;
        startActivity(new Intent(this, NoticeView.class));
        setContentView(this.currentView);
    }

    public void onBackPressed() {
        if (this.VIEW == 1) {
            finish();
        }
        if (this.VIEW == 2 && !GameView.GAMEOVER) {
            GameView.GAME_PAUSED = true;
        }
        switchView(1);
    }

    public void onRestart() {
        super.onRestart();
        setContentView(this.currentView);
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    private void createHighscoreObject() {
        HIGHSCORE = new Highscore(this);
        if (!HIGHSCORE.exist()) {
            HIGHSCORE.saveHighscore();
        } else {
            HIGHSCORE.loadHighscore();
        }
    }

    private void getFontTypeFace() {
        FONT = Typeface.createFromAsset(getAssets(), "lhandw.tff");
    }

    private void createViews() {
        this.gamePanel = new GamePanel(this);
        this.menuPanel = new MenuPanel(this);
        this.tutorialPanel = new TutorialPanel(this);
    }

    public void switchView(int VIEW2) {
        this.VIEW = VIEW2;
        switch (VIEW2) {
            case 1:
                ((MenuPanel) this.menuPanel).updateGameState();
                switchView(this.menuPanel);
                return;
            case 2:
                if (GameView.GAMEOVER) {
                    ((GamePanel) this.gamePanel).resetGame();
                }
                switchView(this.gamePanel);
                return;
            case 3:
                switchView(new HighscorePanel(this));
                return;
            case 4:
            case 5:
            default:
                return;
            case VIEW_TUTORIAL /*6*/:
                switchView(this.tutorialPanel);
                return;
        }
    }

    private void switchView(RelativeLayout view) {
        this.currentView = view;
        setContentView(this.currentView);
    }

    private void getScreenDimension() {
        Display display = ((WindowManager) getSystemService("window")).getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        WIDTH = display.getWidth();
        HEIGHT = display.getHeight();
        DPI = metrics.densityDpi;
    }

    private void loadRessources() {
        try {
            Bitmap piece = BitmapFactory.decodeStream(getAssets().open("background_piece.png"));
            ANDREJ = BitmapFactory.decodeStream(getAssets().open(DRAW_SETTING + "/andrej.png"));
            ANDREJ_TUTORIAL = BitmapFactory.decodeStream(getAssets().open(DRAW_SETTING + "/andrej_tutorial.png"));
            BACKGROUND = Bitmap.createBitmap(WIDTH, HEIGHT, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(BACKGROUND);
            int bHeight = piece.getHeight();
            int bWidth = piece.getWidth();
            for (int y = 0; y < HEIGHT; y += bHeight - 1) {
                for (int x = 0; x < WIDTH; x += bWidth - 1) {
                    canvas.drawBitmap(piece, (float) x, (float) y, (Paint) null);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void getDrawSetting() {
        if (HEIGHT == 240 && WIDTH == 400 && DPI == 120) {
            DRAW_SETTING = 1;
        } else if (HEIGHT == 240 && WIDTH == 432 && DPI == 120) {
            DRAW_SETTING = 1;
        } else if (HEIGHT == 240 && WIDTH == 320 && DPI == 120) {
            DRAW_SETTING = 1;
        } else if (HEIGHT == 320 && WIDTH == 480 && DPI == 160) {
            DRAW_SETTING = 2;
        } else if (HEIGHT == 480 && WIDTH == 640 && DPI == 240) {
            DRAW_SETTING = 3;
        } else if (HEIGHT == 480 && WIDTH == 800) {
            DRAW_SETTING = 3;
        } else if (HEIGHT == 480 && WIDTH == 854) {
            DRAW_SETTING = 3;
        } else {
            DRAW_SETTING = 2;
        }
    }
}
