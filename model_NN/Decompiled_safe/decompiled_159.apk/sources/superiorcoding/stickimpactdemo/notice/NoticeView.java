package superiorcoding.stickimpactdemo.notice;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import superiorcoding.stickimpactdemo.Main;
import superiorcoding.stickimpactdemo.menu.MenuText;

public class NoticeView extends Activity implements View.OnClickListener {
    private MenuText continueB;
    private LinearLayout mainPanel;
    private LinearLayout optionPanel;
    private NoticeText site;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createLayout();
        setContentView(this.mainPanel);
    }

    private void createLayout() {
        LinearLayout.LayoutParams layoutItem = new LinearLayout.LayoutParams(-2, -2);
        LinearLayout.LayoutParams mainItem = new LinearLayout.LayoutParams(-1, -2);
        mainItem.weight = 1.0f;
        this.mainPanel = new LinearLayout(this);
        this.mainPanel.setLayoutParams(mainItem);
        this.mainPanel.setGravity(5);
        this.mainPanel.setOrientation(1);
        this.mainPanel.setBackgroundDrawable(new BitmapDrawable(Main.BACKGROUND));
        this.mainPanel.setPadding(0, 2, 0, 0);
        LinearLayout.LayoutParams scrollItem = new LinearLayout.LayoutParams(-1, -2);
        scrollItem.weight = 1.0f;
        scrollItem.setMargins(2, 2, 2, 2);
        ScrollView scrollView = new ScrollView(this);
        scrollView.setLayoutParams(scrollItem);
        scrollView.setVerticalScrollBarEnabled(false);
        scrollView.setFadingEdgeLength(30);
        scrollView.setOnClickListener(this);
        LinearLayout noticePanel = new LinearLayout(this);
        noticePanel.setOrientation(1);
        noticePanel.addView(new NoticeText("Stick Impact goes online!", 25, this));
        noticePanel.addView(new NoticeText("Check out the Pro Version and accept the challenge!\n", 15, this));
        noticePanel.addView(new NoticeText("Dear Customer", 25, this));
        noticePanel.addView(new NoticeText("We really appreciate comments, ratings and likes!\nIf you have improvement suggestions simply let us know!\n", 15, this));
        noticePanel.addView(new NoticeText("Support", 25, this));
        noticePanel.addView(new NoticeText("If you like our work please support us by buying our paid apps! It helps to provide new releases and updates. Thank you very much!", 15, this));
        this.site = new NoticeText("", 15, this);
        this.site.setText(Html.fromHtml("<a href=\"\">Apps of SuperiorCoding</a><br>"));
        this.site.setOnClickListener(this);
        noticePanel.addView(this.site);
        noticePanel.addView(new NoticeText("Contact", 25, this));
        NoticeText contact = new NoticeText("", 15, this);
        contact.setMovementMethod(LinkMovementMethod.getInstance());
        contact.setText(Html.fromHtml("<a href=\"http://www.facebook.com/SuperiorCoding\">www.facebook.com/SuperiorCoding</a>"));
        noticePanel.addView(contact);
        NoticeText mail = new NoticeText("", 15, this);
        mail.setMovementMethod(LinkMovementMethod.getInstance());
        mail.setText(Html.fromHtml("<a href=\"mailto:superiorcoding@gmx.net\">superiorcoding@gmx.net</a>"));
        noticePanel.addView(mail);
        scrollView.addView(noticePanel);
        this.optionPanel = new LinearLayout(this);
        this.optionPanel.setLayoutParams(layoutItem);
        this.continueB = new MenuText(this, "continue ");
        this.continueB.setTextSize(1, 25.0f);
        this.continueB.setOnClickListener(this);
        this.optionPanel.setMinimumHeight(this.continueB.getHeight() + 1);
        this.optionPanel.addView(this.continueB);
        this.mainPanel.addView(scrollView);
        this.mainPanel.addView(this.optionPanel);
    }

    public void onClick(View view) {
        if (view == this.continueB) {
            finish();
        }
        if (view == this.site) {
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setData(Uri.parse("market://search?q=pub:SuperiorCoding"));
            startActivity(intent);
        }
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }
}
