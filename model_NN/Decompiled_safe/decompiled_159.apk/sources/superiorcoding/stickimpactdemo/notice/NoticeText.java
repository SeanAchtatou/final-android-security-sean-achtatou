package superiorcoding.stickimpactdemo.notice;

import android.content.Context;
import android.widget.RelativeLayout;
import android.widget.TextView;
import superiorcoding.stickimpactdemo.Main;

public class NoticeText extends TextView {
    public NoticeText(String text, int textSize, Context context) {
        super(context);
        setText(text);
        setTypeface(Main.FONT);
        setTextColor(-16777216);
        setTextSize(1, (float) textSize);
        setLayoutParams(new RelativeLayout.LayoutParams(-2, -2));
    }
}
