package superiorcoding.stickimpactdemo.game;

import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import superiorcoding.stickimpactdemo.Main;
import superiorcoding.stickimpactdemo.menu.MenuText;

public class GamePanel extends RelativeLayout implements View.OnClickListener {
    private static final int INFO_SIZE = 20;
    private static final int MENU_SIZE = 25;
    private MenuText back;
    private Context context;
    private RelativeLayout continueView;
    private GameView gameView;
    private MenuText header;
    private RelativeLayout infoView;
    private MenuText level;
    private final Handler mHandler = new Handler();
    private RelativeLayout menuView;
    private MenuText restart;
    private MenuText score;

    public GamePanel(Context context2) {
        super(context2);
        this.context = context2;
        generateGameView();
        generateInfoOverlay();
        generateMenuOverlay();
        generateContinueOverlay();
        addView(this.gameView);
        gameRunningView();
    }

    private void generateGameView() {
        this.gameView = new GameView(this.context, this);
    }

    private void generateInfoOverlay() {
        LinearLayout.LayoutParams layoutMain = new LinearLayout.LayoutParams(-1, -1);
        LinearLayout.LayoutParams layoutPanel = new LinearLayout.LayoutParams(-2, -1);
        layoutPanel.weight = 1.0f;
        this.infoView = new RelativeLayout(this.context);
        this.infoView.setLayoutParams(layoutMain);
        LinearLayout mainPanel = new LinearLayout(this.context);
        mainPanel.setLayoutParams(layoutMain);
        mainPanel.setWeightSum(2.0f);
        LinearLayout levelPanel = new LinearLayout(this.context);
        levelPanel.setLayoutParams(layoutPanel);
        levelPanel.setGravity(51);
        this.level = new MenuText(this.context, "Level 0");
        this.level.setTextSize(1, 20.0f);
        levelPanel.addView(this.level);
        LinearLayout scorePanel = new LinearLayout(this.context);
        scorePanel.setLayoutParams(layoutPanel);
        scorePanel.setGravity(53);
        this.score = new MenuText(this.context, "0 Sticks");
        this.score.setTextSize(1, 20.0f);
        scorePanel.addView(this.score);
        mainPanel.addView(levelPanel);
        mainPanel.addView(scorePanel);
        this.infoView.addView(mainPanel);
    }

    private void generateMenuOverlay() {
        LinearLayout.LayoutParams layoutMain = new LinearLayout.LayoutParams(-1, -1);
        LinearLayout.LayoutParams layoutPanel = new LinearLayout.LayoutParams(-2, -2);
        layoutPanel.weight = 1.0f;
        this.menuView = new RelativeLayout(this.context);
        this.menuView.setLayoutParams(layoutMain);
        LinearLayout mainPanel = new LinearLayout(this.context);
        mainPanel.setLayoutParams(layoutMain);
        mainPanel.setOrientation(1);
        mainPanel.setGravity(17);
        mainPanel.setWeightSum(3.0f);
        this.header = new MenuText(this.context, "You survived 0 sticks\nDemo is limited to 250");
        this.header.setLayoutParams(layoutPanel);
        this.header.setGravity(17);
        this.header.setTextSize(1, 25.0f);
        this.restart = new MenuText(this.context, "Play again");
        this.restart.setLayoutParams(layoutPanel);
        this.restart.setGravity(17);
        this.restart.setTextSize(1, 20.0f);
        this.restart.setOnClickListener(this);
        this.back = new MenuText(this.context, "Back");
        this.back.setLayoutParams(layoutPanel);
        this.back.setGravity(17);
        this.back.setTextSize(1, 20.0f);
        this.back.setOnClickListener(this);
        mainPanel.addView(this.header);
        mainPanel.addView(this.restart);
        mainPanel.addView(this.back);
        this.menuView.addView(mainPanel);
    }

    private void generateContinueOverlay() {
        this.continueView = new RelativeLayout(this.context);
        this.continueView.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        this.continueView.setGravity(17);
        MenuText continueT = new MenuText(this.context, "tap to continue");
        continueT.setTextSize(1, 20.0f);
        this.continueView.addView(continueT);
    }

    public void toggleContinueView() {
        if (!GameView.GAME_PAUSED) {
            removeView(this.continueView);
        } else if (this.continueView.getParent() == null) {
            addView(this.continueView);
        }
    }

    public void gameRunningView() {
        removeView(this.menuView);
        addView(this.infoView);
    }

    public void gameOverView() {
        removeView(this.infoView);
        this.header.setText("You survived " + this.gameView.getCurrentScore() + " sticks!\n[Demo is limited to 250]");
        if (this.gameView.getCurrentScore() > 0) {
            Main.HIGHSCORE.addEntry(this.gameView.getCurrentScore());
        }
        addView(this.menuView);
    }

    public void resetGame() {
        removeView(this.menuView);
        addView(this.infoView);
        this.gameView.resetGame();
    }

    public void updateInfoPanel() {
        this.level.setText("Level " + this.gameView.getCurrentLevel());
        this.score.setText(this.gameView.getCurrentScore() + " Sticks");
    }

    public Handler getHandler() {
        return this.mHandler;
    }

    public void onClick(View v) {
        if (v instanceof MenuText) {
            if (v == this.restart) {
                this.gameView.resetGame();
                gameRunningView();
            }
            if (v == this.back) {
                ((Main) this.context).switchView(1);
            }
        }
    }
}
