package superiorcoding.stickimpactdemo.game;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import java.lang.Thread;
import superiorcoding.stickimpactdemo.Main;
import superiorcoding.stickimpactdemo.engine.level.LevelEngine;
import superiorcoding.stickimpactdemo.engine.player.Player;
import superiorcoding.stickimpactdemo.engine.stick.StickEngine;
import superiorcoding.stickimpactdemo.menu.MenuText;

public class GameView extends ImpactPanel implements SurfaceHolder.Callback {
    public static boolean GAMEOVER;
    public static boolean GAME_PAUSED;
    public static MenuText LEVEL_TEXT;
    public static MenuText SCORE_TEXT;
    private Bitmap background;
    private int currentScore;
    final Runnable gameOverView = new Runnable() {
        public void run() {
            GameView.this.panel.gameOverView();
        }
    };
    private LevelEngine levelEngine;
    private MainThread mainThread;
    /* access modifiers changed from: private */
    public GamePanel panel;
    private Player player;
    private boolean registeredDeath;
    private StickEngine stickEngine;
    final Runnable toggleContinueView = new Runnable() {
        public void run() {
            GameView.this.panel.toggleContinueView();
        }
    };
    final Runnable updateInfoPanel = new Runnable() {
        public void run() {
            GameView.this.panel.updateInfoPanel();
        }
    };

    public GameView(Context context, GamePanel panel2) {
        super(context);
        this.panel = panel2;
        getHolder().addCallback(this);
        this.mainThread = new MainThread(getHolder(), this);
        GAMEOVER = false;
        this.background = Main.BACKGROUND;
        this.levelEngine = new LevelEngine();
        this.stickEngine = new StickEngine(context, this.VIEW);
        this.stickEngine.setSpawnPoolCount(6);
        transferLevelSettings();
        this.stickEngine.generateWave();
        this.player = new Player(context);
        GAME_PAUSED = false;
        this.registeredDeath = false;
        panel2.getHandler().post(this.updateInfoPanel);
        setKeepScreenOn(true);
        setFocusable(true);
    }

    public void render(Canvas canvas) {
        canvas.drawBitmap(this.background, 0.0f, 0.0f, (Paint) null);
        this.player.render(canvas);
        this.stickEngine.render(canvas);
        if (this.player.isRunning()) {
            this.player.renderTarget(canvas);
        }
    }

    public void update() {
        if (!GAME_PAUSED) {
            this.player.update(System.currentTimeMillis());
            this.stickEngine.update(System.currentTimeMillis());
            if (!GAMEOVER) {
                if (this.stickEngine.checkForCollision(this.player.getBitmap(), this.player.getPlayerHitbox(), this.player.getCurrentColor())) {
                    this.player.death();
                    GAMEOVER = true;
                }
                if (this.stickEngine.newStickScore()) {
                    this.currentScore = this.stickEngine.getStickScore();
                    this.panel.getHandler().post(this.updateInfoPanel);
                    if (this.levelEngine.updateLevelEngine(this.currentScore)) {
                        transferLevelSettings();
                        this.panel.getHandler().post(this.updateInfoPanel);
                    }
                    if (this.currentScore >= 250) {
                        this.player.death();
                        GAMEOVER = true;
                    }
                }
            } else if (!this.registeredDeath) {
                this.stickEngine.setSpawnPoolWaves(0);
                this.panel.getHandler().post(this.gameOverView);
                this.registeredDeath = true;
            }
        }
    }

    public void resetGame() {
        this.player.reset();
        this.levelEngine.reset();
        this.currentScore = 0;
        this.registeredDeath = false;
        GAMEOVER = false;
        GAME_PAUSED = false;
        this.stickEngine.restartEngine();
        this.stickEngine.setSpawnPoolCount(6);
        transferLevelSettings();
        this.stickEngine.generateWave();
        this.panel.getHandler().post(this.updateInfoPanel);
    }

    private void transferLevelSettings() {
        this.stickEngine.setHandicapPerWave(this.levelEngine.getHandicapPerWave());
        this.stickEngine.setSpawnPoolWaves(this.levelEngine.getSpawnPoolWaves());
        this.stickEngine.setStickSpeed(this.levelEngine.getStickSpeed());
    }

    public int getCurrentLevel() {
        return this.levelEngine.getLevel();
    }

    public int getCurrentScore() {
        return this.currentScore;
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (GAME_PAUSED) {
            GAME_PAUSED = false;
            this.panel.getHandler().post(this.toggleContinueView);
            this.player.restoreChangeTime();
            return true;
        }
        if (event.getAction() == 0 && !this.player.isDeath()) {
            this.player.moveTo(event.getRawX(), event.getRawY());
        }
        return true;
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }

    public void surfaceCreated(SurfaceHolder holder) {
        if (GAME_PAUSED) {
            this.panel.getHandler().post(this.toggleContinueView);
        }
        if (this.mainThread.getState() == Thread.State.TERMINATED) {
            this.mainThread = new MainThread(getHolder(), this);
        }
        if (this.mainThread.getState() == Thread.State.NEW) {
            this.mainThread.setRunning(true);
            this.mainThread.start();
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        if (!this.player.isDeath()) {
            GAME_PAUSED = true;
            this.player.saveChangeTime();
        }
        boolean retry = true;
        while (retry) {
            try {
                this.mainThread.setRunning(false);
                this.mainThread.join();
                retry = false;
            } catch (InterruptedException e) {
            }
        }
    }
}
