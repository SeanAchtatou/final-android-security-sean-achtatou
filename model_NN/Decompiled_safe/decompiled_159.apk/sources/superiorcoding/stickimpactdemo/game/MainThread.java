package superiorcoding.stickimpactdemo.game;

import android.graphics.Canvas;
import android.view.SurfaceHolder;

public class MainThread extends Thread {
    private static final int FRAME_PERIOD = 20;
    public static final int MAX_FPS = 50;
    private static final int MAX_FRAME_SKIPS = 0;
    private Canvas canvas;
    private ImpactPanel currentPanel;
    private boolean isPaused;
    private boolean isRunning;
    private SurfaceHolder surfaceHolder;

    public MainThread(SurfaceHolder surfaceHolder2, ImpactPanel currentPanel2) {
        this.currentPanel = currentPanel2;
        this.surfaceHolder = surfaceHolder2;
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: CFG modification limit reached, blocks count: 141 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r9 = this;
            r3 = 0
        L_0x0001:
            boolean r6 = r9.isRunning
            if (r6 != 0) goto L_0x0045
            return
        L_0x0006:
            r6 = 0
            r9.canvas = r6
            android.view.SurfaceHolder r6 = r9.surfaceHolder     // Catch:{ all -> 0x005b }
            android.graphics.Canvas r6 = r6.lockCanvas()     // Catch:{ all -> 0x005b }
            r9.canvas = r6     // Catch:{ all -> 0x005b }
            android.view.SurfaceHolder r6 = r9.surfaceHolder     // Catch:{ all -> 0x005b }
            monitor-enter(r6)     // Catch:{ all -> 0x005b }
            long r0 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0058 }
            r2 = 0
            superiorcoding.stickimpactdemo.game.ImpactPanel r7 = r9.currentPanel     // Catch:{ all -> 0x0058 }
            r7.update()     // Catch:{ all -> 0x0058 }
            superiorcoding.stickimpactdemo.game.ImpactPanel r7 = r9.currentPanel     // Catch:{ all -> 0x0058 }
            android.graphics.Canvas r8 = r9.canvas     // Catch:{ all -> 0x0058 }
            r7.render(r8)     // Catch:{ all -> 0x0058 }
            long r7 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0058 }
            long r4 = r7 - r0
            r7 = 20
            long r7 = r7 - r4
            int r3 = (int) r7
            if (r3 <= 0) goto L_0x0035
            long r7 = (long) r3
            java.lang.Thread.sleep(r7)     // Catch:{ InterruptedException -> 0x0068 }
        L_0x0035:
            if (r3 >= 0) goto L_0x0039
            if (r2 < 0) goto L_0x004e
        L_0x0039:
            monitor-exit(r6)     // Catch:{ all -> 0x0058 }
            android.graphics.Canvas r6 = r9.canvas
            if (r6 == 0) goto L_0x0045
            android.view.SurfaceHolder r6 = r9.surfaceHolder
            android.graphics.Canvas r7 = r9.canvas
            r6.unlockCanvasAndPost(r7)
        L_0x0045:
            boolean r6 = r9.isRunning
            if (r6 == 0) goto L_0x0001
            boolean r6 = r9.isPaused
            if (r6 == 0) goto L_0x0006
            goto L_0x0001
        L_0x004e:
            superiorcoding.stickimpactdemo.game.ImpactPanel r7 = r9.currentPanel     // Catch:{ all -> 0x0058 }
            r7.update()     // Catch:{ all -> 0x0058 }
            int r3 = r3 + 20
            int r2 = r2 + 1
            goto L_0x0035
        L_0x0058:
            r7 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x0058 }
            throw r7     // Catch:{ all -> 0x005b }
        L_0x005b:
            r6 = move-exception
            android.graphics.Canvas r7 = r9.canvas
            if (r7 == 0) goto L_0x0067
            android.view.SurfaceHolder r7 = r9.surfaceHolder
            android.graphics.Canvas r8 = r9.canvas
            r7.unlockCanvasAndPost(r8)
        L_0x0067:
            throw r6
        L_0x0068:
            r7 = move-exception
            goto L_0x0035
        */
        throw new UnsupportedOperationException("Method not decompiled: superiorcoding.stickimpactdemo.game.MainThread.run():void");
    }

    public void setRunning(boolean isRunning2) {
        this.isRunning = isRunning2;
    }

    public void setPause(boolean isPaused2) {
        this.isPaused = isPaused2;
    }

    public boolean isRunning() {
        return this.isRunning;
    }
}
