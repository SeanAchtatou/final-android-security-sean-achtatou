package superiorcoding.stickimpactdemo.game;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.view.SurfaceView;
import superiorcoding.stickimpactdemo.Main;

public abstract class ImpactPanel extends SurfaceView {
    protected Rect VIEW = new Rect(0, 0, Main.WIDTH, Main.HEIGHT);

    public abstract void render(Canvas canvas);

    public abstract void update();

    public ImpactPanel(Context context) {
        super(context);
    }
}
