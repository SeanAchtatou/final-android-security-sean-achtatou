package superiorcoding.stickimpactdemo.engine.level;

public class LevelEngine {
    private final int HANDICAPE_DELIMITER = 6;
    private final int PRESET_MULTIPLIER = 10;
    private final int STICKSPEED_DELIMITER = 3;
    private int crossfoot;
    private int handicapPerWave;
    private int level;
    private int nextLevel;
    private int spawnPoolIndex;
    private int[] spawnPoolWaves = {3, 4, 5};
    private int stickSpeed;

    public LevelEngine() {
        presetVariables();
    }

    private void presetVariables() {
        this.stickSpeed = 1;
        this.spawnPoolIndex = 0;
        this.level = 1;
        this.handicapPerWave = 2;
        this.crossfoot = calculateCrossfoot();
        this.nextLevel = calculateNextLevel();
    }

    private int calculateCrossfoot() {
        return this.spawnPoolWaves[this.spawnPoolIndex] + this.stickSpeed + this.handicapPerWave;
    }

    private int calculateNextLevel() {
        return this.crossfoot * this.level * 10;
    }

    public boolean updateLevelEngine(int stickScore) {
        if (stickScore < this.nextLevel) {
            return false;
        }
        if (this.level % 3 == 0) {
            this.stickSpeed++;
        }
        if (this.level % 6 == 0 && this.handicapPerWave < 3) {
            this.handicapPerWave++;
        }
        this.level++;
        this.spawnPoolIndex++;
        if (this.spawnPoolIndex >= this.spawnPoolWaves.length) {
            this.spawnPoolIndex = 0;
        }
        this.crossfoot = calculateCrossfoot();
        this.nextLevel = calculateNextLevel();
        return true;
    }

    public void reset() {
        presetVariables();
    }

    public int getLevel() {
        return this.level;
    }

    public int getStickSpeed() {
        return this.stickSpeed;
    }

    public int getHandicapPerWave() {
        return this.handicapPerWave;
    }

    public int getSpawnPoolWaves() {
        return this.spawnPoolWaves[this.spawnPoolIndex];
    }
}
