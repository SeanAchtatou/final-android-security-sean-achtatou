package superiorcoding.stickimpactdemo.engine.player;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

public class PlayerAnimation {
    protected Bitmap bitmap;
    private int currentFrame = 0;
    private int frameNr;
    private int framePeriod;
    private long frameTicker;
    private boolean isPause;
    protected Rect sourceRect = new Rect(0, 0, this.spriteWidth, this.spriteHeight);
    protected int spriteHeight;
    protected int spriteWidth;
    private int standFrame;
    private int x;
    private int y;

    public PlayerAnimation(Bitmap bitmap2, int x2, int y2, int width, int height, int frameCount, int fps) {
        this.bitmap = bitmap2;
        this.x = x2;
        this.y = y2;
        this.spriteWidth = bitmap2.getWidth() / frameCount;
        this.spriteHeight = bitmap2.getHeight();
        this.frameNr = frameCount;
        this.frameTicker = 0;
        this.framePeriod = 1000 / fps;
        this.standFrame = 0;
    }

    public void update(long gameTime) {
        if (gameTime > this.frameTicker + ((long) this.framePeriod) && !this.isPause) {
            this.frameTicker = gameTime;
            this.currentFrame++;
            if (this.currentFrame >= this.frameNr) {
                this.currentFrame = 0;
            }
            this.sourceRect.left = this.currentFrame * this.spriteWidth;
            this.sourceRect.right = this.sourceRect.left + this.spriteWidth;
        }
    }

    public void render(Canvas canvas) {
        canvas.drawBitmap(this.bitmap, this.sourceRect, new Rect(this.x, this.y, this.x + this.spriteWidth, this.y + this.spriteHeight), (Paint) null);
    }

    public void setPause(boolean isPause2) {
        this.isPause = isPause2;
        if (isPause2) {
            this.sourceRect.left = this.standFrame * this.spriteWidth;
            this.sourceRect.right = this.sourceRect.left + this.spriteWidth;
            return;
        }
        this.currentFrame = 0;
    }

    public void setStandFrame(int standFrame2) {
        this.standFrame = standFrame2;
    }

    public Bitmap getCurrentBitmap() {
        return Bitmap.createBitmap(this.bitmap, this.sourceRect.left, 0, this.spriteWidth, this.spriteHeight);
    }

    public void setCurrentbitmap(Bitmap bitmap2) {
        this.bitmap = bitmap2;
    }

    public int getX() {
        return this.x;
    }

    public void setX(int x2) {
        this.x = x2;
    }

    public int getY() {
        return this.y;
    }

    public void setY(int y2) {
        this.y = y2;
    }

    public Rect getDestination() {
        return new Rect(this.x, this.y, this.x + this.spriteWidth, this.y + this.spriteHeight);
    }
}
