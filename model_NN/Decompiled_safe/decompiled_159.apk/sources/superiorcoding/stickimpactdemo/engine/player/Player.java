package superiorcoding.stickimpactdemo.engine.player;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import java.io.IOException;
import java.util.Random;
import superiorcoding.stickimpactdemo.Main;
import superiorcoding.stickimpactdemo.engine.stick.StickEngine;

public class Player {
    public static int SIZE;
    private final int MAX_COLOR_TIME = 30;
    private final int MIN_COLOR_TIME = 10;
    private final float PRESET_SPEED = 3.0f;
    private PlayerAnimation animation;
    private int colorChangeTime;
    private int currentColor;
    private Bitmap[] deathBitmap = new Bitmap[4];
    private float distance;
    private int framePeriod;
    private long frameTicker;
    private boolean isDeath;
    private boolean isRunning;
    private Bitmap[] playerBitmap = new Bitmap[4];
    private int playerHalf;
    private float posX;
    private float posY;
    private Random random;
    private int savedChangeTime;
    private float speed;
    private float speedX;
    private float speedY;
    private StopWatch stopWatch;
    private float targetDistance;
    private Paint targetPaint;
    private Rect targetRect;

    public Player(Context context) {
        loadBitmapsIntoMemory(context);
        initColorChange();
        generateAnimation();
    }

    private void initTargetTracking() {
        this.playerHalf = SIZE / 2;
        this.targetPaint = new Paint();
        this.targetPaint.setAntiAlias(false);
        this.targetPaint.setStyle(Paint.Style.STROKE);
    }

    private void initColorChange() {
        this.random = new Random();
        this.stopWatch = new StopWatch();
        generateRandomTime();
    }

    private void loadBitmapsIntoMemory(Context context) {
        try {
            this.playerBitmap[0] = BitmapFactory.decodeStream(context.getAssets().open(Main.DRAW_SETTING + "/andrej_running_gray.png"));
            this.playerBitmap[1] = BitmapFactory.decodeStream(context.getAssets().open(Main.DRAW_SETTING + "/andrej_running_red.png"));
            this.playerBitmap[2] = BitmapFactory.decodeStream(context.getAssets().open(Main.DRAW_SETTING + "/andrej_running_blue.png"));
            this.playerBitmap[3] = BitmapFactory.decodeStream(context.getAssets().open(Main.DRAW_SETTING + "/andrej_running_black.png"));
            this.deathBitmap[0] = BitmapFactory.decodeStream(context.getAssets().open(Main.DRAW_SETTING + "/andrej_death_gray.png"));
            this.deathBitmap[1] = BitmapFactory.decodeStream(context.getAssets().open(Main.DRAW_SETTING + "/andrej_death_red.png"));
            this.deathBitmap[2] = BitmapFactory.decodeStream(context.getAssets().open(Main.DRAW_SETTING + "/andrej_death_blue.png"));
            this.deathBitmap[3] = BitmapFactory.decodeStream(context.getAssets().open(Main.DRAW_SETTING + "/andrej_death_black.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void generateAnimation() {
        SIZE = this.deathBitmap[0].getWidth();
        initTargetTracking();
        this.posX = (float) ((Main.WIDTH - SIZE) / 2);
        this.posY = (float) ((Main.HEIGHT - SIZE) / 2);
        this.speedX = 0.0f;
        this.speedY = 0.0f;
        this.frameTicker = 0;
        this.framePeriod = 20;
        this.speed = 3.0f;
        this.animation = new PlayerAnimation(this.playerBitmap[this.currentColor], Math.round(this.posX), Math.round(this.posY), this.playerBitmap[0].getWidth(), this.playerBitmap[0].getHeight(), 8, 8);
        this.animation.setStandFrame(2);
        this.animation.setPause(true);
        this.isDeath = false;
        this.isRunning = false;
        switchColor();
    }

    public void moveTo(float newX, float newY) {
        float signX = 1.0f;
        float signY = 1.0f;
        float dX = newX - getCenterX();
        float dY = newY - getCenterY();
        this.distance = 0.0f;
        this.targetDistance = (float) Math.sqrt((double) ((dX * dX) + (dY * dY)));
        if (dX < 0.0f) {
            signX = -1.0f;
        }
        if (dY < 0.0f) {
            signY = -1.0f;
        }
        float dX2 = Math.abs(dX);
        float dY2 = Math.abs(dY);
        float scale = this.speed / this.targetDistance;
        this.speedY = dY2 * scale;
        this.speedX = dX2 * scale;
        this.speedY *= signY;
        this.speedX *= signX;
        this.isRunning = true;
        this.animation.setPause(false);
        this.targetRect = new Rect(((int) newX) - this.playerHalf, ((int) newY) - this.playerHalf, ((int) newX) + this.playerHalf, ((int) newY) + this.playerHalf);
        this.targetPaint.setColor(StickEngine.COLOR_CODES[this.currentColor]);
    }

    private void switchColor() {
        this.currentColor = this.random.nextInt(4);
        this.animation.setCurrentbitmap(this.playerBitmap[this.currentColor]);
        this.targetPaint.setColor(StickEngine.COLOR_CODES[this.currentColor]);
    }

    private void generateRandomTime() {
        this.colorChangeTime = this.random.nextInt(20) + 10;
        this.savedChangeTime = this.colorChangeTime;
    }

    public void update(long gameTime) {
        if (gameTime > this.frameTicker + ((long) this.framePeriod)) {
            this.frameTicker = gameTime;
            if (!this.isDeath) {
                if (this.stopWatch.getElapsedTimeSecs() >= ((long) this.colorChangeTime)) {
                    this.stopWatch.start();
                    generateRandomTime();
                    switchColor();
                }
                if (this.animation != null) {
                    this.animation.update(gameTime);
                }
                if (this.isRunning) {
                    this.posX += this.speedX;
                    this.posY += this.speedY;
                    this.animation.setX(Math.round(this.posX));
                    this.animation.setY(Math.round(this.posY));
                    this.distance += this.speed;
                    if (isTargetReached()) {
                        this.isRunning = false;
                        this.animation.setPause(true);
                    }
                }
            }
        }
    }

    public void render(Canvas canvas) {
        if (!this.isDeath) {
            this.animation.render(canvas);
        } else {
            canvas.drawBitmap(this.deathBitmap[this.currentColor], this.posX, this.posY, (Paint) null);
        }
    }

    public void renderTarget(Canvas canvas) {
        canvas.drawRect(this.targetRect, this.targetPaint);
    }

    public boolean isRunning() {
        return this.isRunning;
    }

    public Bitmap getBitmap() {
        return this.animation.getCurrentBitmap();
    }

    public void reset() {
        initColorChange();
        generateAnimation();
    }

    public float getCenterX() {
        return this.posX + ((float) (SIZE / 2));
    }

    public float getCenterY() {
        return this.posY + ((float) (SIZE / 2));
    }

    private boolean isTargetReached() {
        if (this.distance >= this.targetDistance) {
            return true;
        }
        return false;
    }

    public Rect getPlayerHitbox() {
        return this.animation.getDestination();
    }

    public void setPosX(float newX) {
        this.isRunning = false;
        this.animation.setPause(true);
        this.posX = newX;
        this.animation.setX(Math.round(newX));
    }

    public void death() {
        this.isDeath = true;
    }

    public boolean isDeath() {
        return this.isDeath;
    }

    public int getCurrentColor() {
        return this.currentColor;
    }

    public void saveChangeTime() {
        this.savedChangeTime = this.colorChangeTime - ((int) this.stopWatch.getElapsedTimeSecs());
    }

    public void restoreChangeTime() {
        this.colorChangeTime = this.savedChangeTime;
        this.stopWatch.start();
    }
}
