package superiorcoding.stickimpactdemo.engine.stick;

public class GrayStick extends Stick {
    private final int COLOR = 0;

    public boolean performAction(int PLAYER_COLOR) {
        if (PLAYER_COLOR != 0) {
            return true;
        }
        setWasHit(true);
        remove();
        return false;
    }
}
