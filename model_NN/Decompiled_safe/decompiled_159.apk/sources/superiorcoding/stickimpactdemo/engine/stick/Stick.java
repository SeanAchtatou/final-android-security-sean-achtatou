package superiorcoding.stickimpactdemo.engine.stick;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

public abstract class Stick {
    private Bitmap bitmap;
    private int endOfField;
    private Rect fieldView;
    public int height;
    private boolean removed = false;
    private int rotation;
    private boolean wasHit;
    public int width;
    private int x;
    private int y;

    public abstract boolean performAction(int i);

    public void render(Canvas canvas) {
        canvas.drawBitmap(this.bitmap, (float) this.x, (float) this.y, (Paint) null);
    }

    public boolean isVisible() {
        return !this.wasHit;
    }

    public boolean hasReachedEnd() {
        if (this.y < this.endOfField || this.removed) {
            return false;
        }
        return true;
    }

    public void move(int speed) {
        this.y += speed;
    }

    public void remove() {
        this.removed = true;
    }

    public boolean isRemoved() {
        return this.removed;
    }

    public void setBitmap(Bitmap bitmap2) {
        this.bitmap = bitmap2;
        this.width = bitmap2.getWidth();
        this.height = bitmap2.getHeight();
    }

    public int getY() {
        return this.y;
    }

    public void setY(int y2) {
        this.y = y2;
    }

    public void setX(int x2) {
        this.x = x2;
    }

    public int getX() {
        return this.x;
    }

    public int getPixel(int x2, int y2) {
        return this.bitmap.getPixel(x2, y2);
    }

    public void setRotation(int rotation2) {
        this.rotation = rotation2;
    }

    public int getRotation() {
        return this.rotation;
    }

    public void setFieldView(Rect fieldView2) {
        this.fieldView = fieldView2;
        this.endOfField = fieldView2.bottom;
    }

    public Rect getFieldView() {
        return this.fieldView;
    }

    public Rect getStickHitbox() {
        return new Rect(this.x, this.y, this.x + this.width, this.y + this.height);
    }

    public void setWasHit(boolean wasHit2) {
        this.wasHit = wasHit2;
    }

    public boolean wasHit() {
        return this.wasHit;
    }
}
