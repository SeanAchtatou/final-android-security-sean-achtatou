package superiorcoding.stickimpactdemo.engine.stick;

public class BlueStick extends Stick {
    private final int COLOR = 2;

    public boolean performAction(int PLAYER_COLOR) {
        if (PLAYER_COLOR != 2) {
            return true;
        }
        setWasHit(true);
        remove();
        return false;
    }
}
