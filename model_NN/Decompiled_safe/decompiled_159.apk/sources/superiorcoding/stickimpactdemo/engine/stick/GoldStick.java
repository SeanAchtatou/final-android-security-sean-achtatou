package superiorcoding.stickimpactdemo.engine.stick;

import android.graphics.Bitmap;

public class GoldStick extends Stick {
    private Bitmap changeBitmap;

    public boolean isVisible() {
        return true;
    }

    public boolean performAction(int PLAYER_COLOR) {
        setWasHit(true);
        setBitmap(this.changeBitmap);
        return false;
    }

    public void setChangeBitmap(Bitmap changeBitmap2) {
        this.changeBitmap = changeBitmap2;
    }
}
