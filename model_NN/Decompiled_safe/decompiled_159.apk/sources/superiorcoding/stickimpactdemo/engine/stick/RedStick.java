package superiorcoding.stickimpactdemo.engine.stick;

public class RedStick extends Stick {
    private final int COLOR = 1;

    public boolean performAction(int PLAYER_COLOR) {
        if (PLAYER_COLOR != 1) {
            return true;
        }
        setWasHit(true);
        remove();
        return false;
    }
}
