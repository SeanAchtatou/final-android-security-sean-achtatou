package superiorcoding.stickimpactdemo.engine.stick;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Random;
import superiorcoding.stickimpactdemo.Main;
import superiorcoding.stickimpactdemo.engine.player.Player;

public class StickEngine {
    public static final int COLOR_BLACK = 3;
    public static final int COLOR_BLUE = 2;
    public static final int[] COLOR_CODES = {Color.parseColor("#7e7e7e"), Color.parseColor("#bf0000"), Color.parseColor("#1f75bf"), Color.parseColor("#000000")};
    public static final int COLOR_GOLD = 4;
    public static final int COLOR_GRAY = 0;
    public static final int COLOR_RED = 1;
    private static final int GOLD_VALUE = 20;
    private static final int HANDICAP_PER_WAVE = 2;
    private static final int HANDICAP_PROB = 6;
    public static final int PLAYER_COLOR_COUNT = 4;
    private static final int SPAWNPOOL_COUNT = 4;
    private static final int SPAWNPOOL_WAVES = 5;
    public static final int STICK_COLOR_COUNT = 3;
    private static final int STICK_SPEED = 1;
    private static Bitmap[] blueBitmap = new Bitmap[3];
    private static Bitmap[] goldBitmap = new Bitmap[3];
    private static Bitmap[] grayBitmap = new Bitmap[3];
    private static Bitmap[] redBitmap = new Bitmap[3];
    private static Bitmap[] transBitmap = new Bitmap[3];
    private final int MAX_ROTATE = 180;
    private final int MAX_STICK_LENGTH = 10;
    private final int MIN_STICK_LENGTH = 3;
    private int SIZE;
    private Rect fieldView;
    private int framePeriod;
    private long frameTicker;
    private int handicapCount;
    private int handicapPerWave;
    private int lastGold;
    private Random random;
    private int spawnPoolCount;
    private int spawnPoolIndex;
    private int spawnPoolPoint;
    private int spawnPoolWaves;
    private int spawnPoolWidth;
    private int spawnWaveCount;
    private LinkedList<Stick> stickList;
    private int stickScore;
    private int stickSpeed;
    private Stick tempStick;
    private boolean updateAvailable;
    private int waveProgress;

    public StickEngine(Context context, Rect fieldView2) {
        this.fieldView = fieldView2;
        createStickBitmaps(context);
        presetConfigVariables();
        initEngine();
    }

    private void presetConfigVariables() {
        this.stickSpeed = 1;
        this.spawnPoolWaves = SPAWNPOOL_WAVES;
        this.handicapPerWave = 2;
        this.spawnPoolCount = 4;
        this.spawnPoolWidth = (int) (((float) this.fieldView.right) / 4.0f);
        this.spawnPoolPoint = (int) (((float) this.fieldView.bottom) / 5.0f);
    }

    private void initEngine() {
        this.stickScore = 0;
        this.handicapCount = 0;
        this.frameTicker = 0;
        this.framePeriod = GOLD_VALUE;
        this.waveProgress = 0;
        this.spawnPoolIndex = 0;
        this.spawnWaveCount = 1;
        this.lastGold = -1;
        this.random = new Random();
        this.stickList = new LinkedList<>();
    }

    public void restartEngine() {
        initEngine();
    }

    private void createStickBitmaps(Context context) {
        try {
            grayBitmap[0] = BitmapFactory.decodeStream(context.getAssets().open(Main.DRAW_SETTING + "/stick_gray_left.png"));
            grayBitmap[1] = BitmapFactory.decodeStream(context.getAssets().open(Main.DRAW_SETTING + "/stick_gray_middle.png"));
            grayBitmap[2] = BitmapFactory.decodeStream(context.getAssets().open(Main.DRAW_SETTING + "/stick_gray_right.png"));
            redBitmap[0] = BitmapFactory.decodeStream(context.getAssets().open(Main.DRAW_SETTING + "/stick_red_left.png"));
            redBitmap[1] = BitmapFactory.decodeStream(context.getAssets().open(Main.DRAW_SETTING + "/stick_red_middle.png"));
            redBitmap[2] = BitmapFactory.decodeStream(context.getAssets().open(Main.DRAW_SETTING + "/stick_red_right.png"));
            blueBitmap[0] = BitmapFactory.decodeStream(context.getAssets().open(Main.DRAW_SETTING + "/stick_blue_left.png"));
            blueBitmap[1] = BitmapFactory.decodeStream(context.getAssets().open(Main.DRAW_SETTING + "/stick_blue_middle.png"));
            blueBitmap[2] = BitmapFactory.decodeStream(context.getAssets().open(Main.DRAW_SETTING + "/stick_blue_right.png"));
            goldBitmap[0] = BitmapFactory.decodeStream(context.getAssets().open(Main.DRAW_SETTING + "/stick_gold_left.png"));
            goldBitmap[1] = BitmapFactory.decodeStream(context.getAssets().open(Main.DRAW_SETTING + "/stick_gold_middle.png"));
            goldBitmap[2] = BitmapFactory.decodeStream(context.getAssets().open(Main.DRAW_SETTING + "/stick_gold_right.png"));
            transBitmap[0] = BitmapFactory.decodeStream(context.getAssets().open(Main.DRAW_SETTING + "/stick_trans_left.png"));
            transBitmap[1] = BitmapFactory.decodeStream(context.getAssets().open(Main.DRAW_SETTING + "/stick_trans_middle.png"));
            transBitmap[2] = BitmapFactory.decodeStream(context.getAssets().open(Main.DRAW_SETTING + "/stick_trans_right.png"));
            this.SIZE = grayBitmap[0].getWidth();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Stick generateStick() {
        Stick tempStick2 = null;
        if (this.stickScore / GOLD_VALUE > this.lastGold && this.random.nextInt(this.spawnPoolCount) == 0) {
            this.lastGold = this.stickScore / GOLD_VALUE;
            tempStick2 = new GoldStick();
        }
        if (tempStick2 == null) {
            switch (this.random.nextInt(3)) {
                case COLOR_GRAY /*0*/:
                    tempStick2 = new GrayStick();
                    break;
                case 1:
                    tempStick2 = new RedStick();
                    break;
                case 2:
                    tempStick2 = new BlueStick();
                    break;
            }
        }
        tempStick2.setFieldView(this.fieldView);
        createStickBitmap(tempStick2);
        generateLocation(tempStick2);
        return tempStick2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    private void createStickBitmap(Stick tempStick2) {
        Bitmap tempGold = null;
        int length = (this.random.nextInt(7) + 3) * this.SIZE;
        Bitmap tempBitmap = Bitmap.createBitmap((this.SIZE * 2) + length, this.SIZE, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(tempBitmap);
        if (tempStick2 instanceof GrayStick) {
            canvas.drawBitmap(grayBitmap[0], 0.0f, 0.0f, (Paint) null);
            for (int i = 0; i < length; i += this.SIZE) {
                canvas.drawBitmap(grayBitmap[1], (float) (this.SIZE + i), 0.0f, (Paint) null);
            }
            canvas.drawBitmap(grayBitmap[2], (float) (this.SIZE + length), 0.0f, (Paint) null);
        }
        if (tempStick2 instanceof RedStick) {
            canvas.drawBitmap(redBitmap[0], 0.0f, 0.0f, (Paint) null);
            for (int i2 = 0; i2 < length; i2 += this.SIZE) {
                canvas.drawBitmap(redBitmap[1], (float) (this.SIZE + i2), 0.0f, (Paint) null);
            }
            canvas.drawBitmap(redBitmap[2], (float) (this.SIZE + length), 0.0f, (Paint) null);
        }
        if (tempStick2 instanceof BlueStick) {
            canvas.drawBitmap(blueBitmap[0], 0.0f, 0.0f, (Paint) null);
            for (int i3 = 0; i3 < length; i3 += this.SIZE) {
                canvas.drawBitmap(blueBitmap[1], (float) (this.SIZE + i3), 0.0f, (Paint) null);
            }
            canvas.drawBitmap(blueBitmap[2], (float) (this.SIZE + length), 0.0f, (Paint) null);
        }
        if (tempStick2 instanceof GoldStick) {
            canvas.drawBitmap(transBitmap[0], 0.0f, 0.0f, (Paint) null);
            for (int i4 = 0; i4 < length; i4 += this.SIZE) {
                canvas.drawBitmap(transBitmap[1], (float) (this.SIZE + i4), 0.0f, (Paint) null);
            }
            canvas.drawBitmap(transBitmap[2], (float) (this.SIZE + length), 0.0f, (Paint) null);
            tempGold = Bitmap.createBitmap((this.SIZE * 2) + length, this.SIZE, Bitmap.Config.ARGB_8888);
            canvas.setBitmap(tempGold);
            canvas.drawBitmap(goldBitmap[0], 0.0f, 0.0f, (Paint) null);
            for (int i5 = 0; i5 < length; i5 += this.SIZE) {
                canvas.drawBitmap(goldBitmap[1], (float) (this.SIZE + i5), 0.0f, (Paint) null);
            }
            canvas.drawBitmap(goldBitmap[2], (float) (this.SIZE + length), 0.0f, (Paint) null);
        }
        Matrix matrix = new Matrix();
        if (this.handicapCount >= this.handicapPerWave) {
            tempStick2.setRotation(this.random.nextInt(180));
            matrix.postRotate((float) tempStick2.getRotation(), (float) (tempBitmap.getWidth() / 2), (float) (tempBitmap.getHeight() / 2));
        } else if (this.random.nextInt(6) == 0) {
            tempStick2.setRotation(0);
            this.handicapCount = this.handicapCount + 1;
        } else {
            tempStick2.setRotation(this.random.nextInt(180));
            matrix.postRotate((float) tempStick2.getRotation(), (float) (tempBitmap.getWidth() / 2), (float) (tempBitmap.getHeight() / 2));
        }
        tempStick2.setBitmap(Bitmap.createBitmap(tempBitmap, 0, 0, tempBitmap.getWidth(), tempBitmap.getHeight(), matrix, true));
        if (tempStick2 instanceof GoldStick) {
            ((GoldStick) tempStick2).setChangeBitmap(Bitmap.createBitmap(tempGold, 0, 0, tempGold.getWidth(), tempGold.getHeight(), matrix, true));
        }
    }

    public void generateLocation(Stick tempStick2) {
        if (this.spawnPoolIndex >= this.spawnPoolCount) {
            this.spawnPoolIndex = 0;
        }
        int tempX = this.spawnPoolIndex * this.spawnPoolWidth;
        if (this.spawnWaveCount % 2 == 0) {
            if (this.random.nextInt(1) == 0) {
                tempX += this.spawnPoolWidth - tempStick2.width;
            }
        } else if (this.random.nextInt(1) == 0 && this.spawnPoolWidth - tempStick2.width > 0) {
            tempX += this.random.nextInt(this.spawnPoolWidth - tempStick2.width);
        }
        if (tempStick2.width + tempX > this.fieldView.width()) {
            tempX -= (tempStick2.width + tempX) - this.fieldView.width();
        }
        tempStick2.setX(tempX);
        tempStick2.setY(0 - tempStick2.height);
        this.spawnPoolIndex++;
    }

    public void generateWave() {
        for (int x = 0; x < this.spawnPoolCount; x++) {
            this.stickList.add(generateStick());
        }
    }

    public void update(long gameTime) {
        if (gameTime > this.frameTicker + ((long) this.framePeriod)) {
            this.frameTicker = gameTime;
            for (int i = 0; i < this.stickList.size(); i++) {
                this.tempStick = this.stickList.get(i);
                this.tempStick.move(this.stickSpeed);
                if (this.tempStick.hasReachedEnd()) {
                    if (!this.tempStick.wasHit()) {
                        this.tempStick.remove();
                        updateScore(1);
                    } else if (this.tempStick instanceof GoldStick) {
                        this.tempStick.remove();
                        updateScore(SPAWNPOOL_WAVES);
                    }
                }
            }
            this.waveProgress += this.stickSpeed;
            if (this.waveProgress >= this.spawnPoolPoint * this.spawnWaveCount) {
                generateWave();
                this.spawnWaveCount++;
                this.handicapCount = 0;
            }
            if (this.waveProgress >= this.fieldView.bottom) {
                this.waveProgress = 0;
                this.spawnWaveCount = 1;
                for (int x = 0; x < this.stickList.size(); x++) {
                    if (this.stickList.get(x).isRemoved()) {
                        this.stickList.remove(x);
                    }
                }
            }
        }
    }

    public void render(Canvas canvas) {
        for (int i = 0; i < this.stickList.size(); i++) {
            Stick actStick = this.stickList.get(i);
            if (actStick.isVisible()) {
                actStick.render(canvas);
            }
        }
    }

    public boolean checkForCollision(Bitmap playerBitmap, Rect playerHitbox, int PLAYER_COLOR) {
        int end = this.stickList.size();
        for (int i = 0; i < end; i++) {
            Stick actStick = this.stickList.get(i);
            if (actStick.isVisible()) {
                Rect stickHitbox = actStick.getStickHitbox();
                Rect bitmapHitbox = new Rect(stickHitbox);
                if (stickHitbox.intersect(playerHitbox)) {
                    int xB = stickHitbox.left - bitmapHitbox.left;
                    int yB = stickHitbox.top - bitmapHitbox.top;
                    int xP = stickHitbox.left - playerHitbox.left;
                    int yCP = stickHitbox.top - playerHitbox.top;
                    int yCB = yB;
                    while (yCP < Player.SIZE) {
                        int xCP = xP;
                        int xCB = xB;
                        while (xCP < Player.SIZE) {
                            if (xCB >= 0 && xCB < actStick.width && yCB >= 0 && yCB < actStick.height && actStick.getPixel(xCB, yCB) != 0 && playerBitmap.getPixel(xCP, yCP) != 0) {
                                return actStick.performAction(PLAYER_COLOR);
                            }
                            xCP++;
                            xCB++;
                        }
                        yCP++;
                        yCB++;
                    }
                    continue;
                } else {
                    continue;
                }
            }
        }
        return false;
    }

    private void updateScore(int score) {
        this.stickScore += score;
        this.updateAvailable = true;
    }

    public boolean newStickScore() {
        return this.updateAvailable;
    }

    public int getStickScore() {
        this.updateAvailable = false;
        return this.stickScore;
    }

    public void setStickSpeed(int stickSpeed2) {
        this.stickSpeed = stickSpeed2;
    }

    public int getStickSpeed() {
        return this.stickSpeed;
    }

    public void setSpawnPoolWaves(int spawnPoolWaves2) {
        this.spawnPoolWaves = spawnPoolWaves2;
        if (spawnPoolWaves2 > 0) {
            this.spawnPoolPoint = this.fieldView.bottom / spawnPoolWaves2;
        } else {
            this.spawnPoolCount = 0;
        }
    }

    public int getSpawnPoolWaves() {
        return this.spawnPoolWaves;
    }

    public void setHandicapPerWave(int handicapPerWave2) {
        this.handicapPerWave = handicapPerWave2;
    }

    public int getHandicapPerWave() {
        return this.handicapPerWave;
    }

    public void setSpawnPoolCount(int spawnPoolCount2) {
        this.spawnPoolCount = spawnPoolCount2;
        if (spawnPoolCount2 > 0) {
            this.spawnPoolWidth = this.fieldView.right / spawnPoolCount2;
        } else {
            this.spawnPoolWidth = this.fieldView.right;
        }
    }

    public int getSpawnPoolCount() {
        return this.spawnPoolCount;
    }

    public void setStickScore(int stickScore2) {
        this.stickScore = stickScore2;
    }
}
