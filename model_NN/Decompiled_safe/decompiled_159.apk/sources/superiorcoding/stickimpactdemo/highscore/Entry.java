package superiorcoding.stickimpactdemo.highscore;

public class Entry {
    private String checksum;
    private String playerName;
    private int score;

    public Entry(String playerName2, int score2, String checksum2) {
        this.score = score2;
        this.playerName = playerName2;
        this.checksum = checksum2;
    }

    public void setScore(int score2) {
        this.score = score2;
    }

    public int getScore() {
        return this.score;
    }

    public void setPlayerName(String playerName2) {
        this.playerName = playerName2;
    }

    public String getPlayerName() {
        return this.playerName;
    }

    public void setChecksum(String checksum2) {
        this.checksum = checksum2;
    }

    public String getChecksum() {
        return this.checksum;
    }
}
