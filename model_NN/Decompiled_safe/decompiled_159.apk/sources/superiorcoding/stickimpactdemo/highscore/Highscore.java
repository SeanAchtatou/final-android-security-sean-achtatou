package superiorcoding.stickimpactdemo.highscore;

import android.content.Context;
import android.util.Log;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

public class Highscore {
    private static final String FILENAME = "highscore.lst";
    public static final int MAX_ENTRIES = 99;
    private Context context;
    private FileInputStream fInput;
    private FileOutputStream fOutput;
    private ArrayList<Entry> highscoreList = new ArrayList<>();
    private BufferedReader inputS;
    private OutputStreamWriter outputS;
    private String playerName = "Nameless";

    public Highscore(Context context2) {
        this.context = context2;
    }

    public boolean exist() {
        try {
            this.fInput = this.context.openFileInput(FILENAME);
            try {
                if (this.fInput != null) {
                    this.fInput.close();
                }
                if (this.inputS != null) {
                    this.inputS.close();
                }
            } catch (IOException e) {
                Log.d("Highscore", "Couldn't close streams: " + e.getMessage());
            }
            return true;
        } catch (IOException e2) {
            try {
                if (this.fInput != null) {
                    this.fInput.close();
                }
                if (this.inputS != null) {
                    this.inputS.close();
                }
            } catch (IOException e3) {
                Log.d("Highscore", "Couldn't close streams: " + e3.getMessage());
            }
            return false;
        } catch (Throwable th) {
            try {
                if (this.fInput != null) {
                    this.fInput.close();
                }
                if (this.inputS != null) {
                    this.inputS.close();
                }
            } catch (IOException e4) {
                Log.d("Highscore", "Couldn't close streams: " + e4.getMessage());
            }
            throw th;
        }
    }

    public void loadHighscore() {
        try {
            this.fInput = this.context.openFileInput(FILENAME);
            this.inputS = new BufferedReader(new InputStreamReader(this.fInput), 1024);
            this.playerName = this.inputS.readLine().toString();
            while (true) {
                String line = this.inputS.readLine();
                if (line == null) {
                    try {
                        break;
                    } catch (IOException e) {
                        Log.d("Highscore", "Couldn't close streams: " + e.getMessage());
                        return;
                    }
                } else {
                    String[] temp = line.split(":");
                    if (temp.length == 3) {
                        this.highscoreList.add(new Entry(temp[0], Integer.parseInt(temp[1]), temp[2]));
                    }
                }
            }
            if (this.fInput != null) {
                this.fInput.close();
            }
            if (this.inputS != null) {
                this.inputS.close();
            }
        } catch (IOException e2) {
            Log.d("Highscore", "Couldn't load the file: " + e2.getMessage());
            try {
                if (this.fInput != null) {
                    this.fInput.close();
                }
                if (this.inputS != null) {
                    this.inputS.close();
                }
            } catch (IOException e3) {
                Log.d("Highscore", "Couldn't close streams: " + e3.getMessage());
            }
        } catch (Throwable th) {
            try {
                if (this.fInput != null) {
                    this.fInput.close();
                }
                if (this.inputS != null) {
                    this.inputS.close();
                }
            } catch (IOException e4) {
                Log.d("Highscore", "Couldn't close streams: " + e4.getMessage());
            }
            throw th;
        }
    }

    public void saveHighscore() {
        try {
            this.fOutput = this.context.openFileOutput(FILENAME, 0);
            OutputStreamWriter outputS2 = new OutputStreamWriter(this.fOutput);
            outputS2.write(String.valueOf(this.playerName) + "\n");
            int i = 0;
            while (i < getHighscoreList().size() && i < 99) {
                Entry entry = getHighscoreList().get(i);
                outputS2.write(String.valueOf(entry.getPlayerName()) + ":" + entry.getScore() + ":" + entry.getChecksum() + "\n");
                i++;
            }
            outputS2.flush();
            try {
                if (this.fOutput != null) {
                    this.fOutput.close();
                }
                if (this.outputS != null) {
                    this.outputS.close();
                }
            } catch (IOException e) {
                Log.d("Highscore", "Couldn't close streams: " + e.getMessage());
            }
        } catch (IOException e2) {
            Log.d("Highscore", "Couldn't save the file: " + e2.getMessage());
            try {
                if (this.fOutput != null) {
                    this.fOutput.close();
                }
                if (this.outputS != null) {
                    this.outputS.close();
                }
            } catch (IOException e3) {
                Log.d("Highscore", "Couldn't close streams: " + e3.getMessage());
            }
        } catch (Throwable th) {
            try {
                if (this.fOutput != null) {
                    this.fOutput.close();
                }
                if (this.outputS != null) {
                    this.outputS.close();
                }
            } catch (IOException e4) {
                Log.d("Highscore", "Couldn't close streams: " + e4.getMessage());
            }
            throw th;
        }
    }

    public void addEntry(int stickScore) {
        boolean highscoreSet = false;
        String checksum = Encryption.encryptString(this.playerName, stickScore);
        if (this.highscoreList.isEmpty()) {
            this.highscoreList.add(new Entry(this.playerName, stickScore, checksum));
        } else {
            int i = 0;
            while (true) {
                if (i >= 99 || i >= this.highscoreList.size()) {
                    break;
                } else if (stickScore >= this.highscoreList.get(i).getScore()) {
                    this.highscoreList.add(i, new Entry(this.playerName, stickScore, checksum));
                    highscoreSet = true;
                    break;
                } else {
                    i++;
                }
            }
            if (!highscoreSet) {
                this.highscoreList.add(new Entry(this.playerName, stickScore, checksum));
            }
        }
        saveHighscore();
    }

    public void clearHighscoreList() {
        this.highscoreList.clear();
    }

    public void resetHighscore() {
        this.highscoreList.clear();
        saveHighscore();
    }

    public void setPlayerName(String playerName2) {
        this.playerName = playerName2;
        saveHighscore();
    }

    public String getPlayerName() {
        return this.playerName;
    }

    public void setHighscoreList(ArrayList<Entry> highscoreList2) {
        this.highscoreList = highscoreList2;
    }

    public ArrayList<Entry> getHighscoreList() {
        return this.highscoreList;
    }
}
