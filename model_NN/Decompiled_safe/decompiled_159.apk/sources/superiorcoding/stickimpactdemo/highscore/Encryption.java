package superiorcoding.stickimpactdemo.highscore;

public class Encryption {
    private static final String key = "SuperiorCoding";

    public static String encryptString(String str, int highscore) {
        String str2 = String.valueOf(str) + highscore;
        StringBuffer sb = new StringBuffer(str2);
        int lenStr = str2.length();
        int lenKey = key.length();
        int i = 0;
        int j = 0;
        while (i < lenStr) {
            if (j >= lenKey) {
                j = 0;
            }
            sb.setCharAt(i, (char) (str2.charAt(i) ^ key.charAt(j)));
            i++;
            j++;
        }
        return sb.toString();
    }

    public static boolean checkEncryption(String str, int highscore, String checksum) {
        if (encryptString(str, highscore).equals(checksum)) {
            return true;
        }
        return false;
    }
}
