package superiorcoding.stickimpactdemo.highscore;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import java.util.ArrayList;
import superiorcoding.stickimpactdemo.Main;
import superiorcoding.stickimpactdemo.menu.MenuText;

public class HighscorePanel extends RelativeLayout implements View.OnClickListener {
    private Bitmap andrej = Main.ANDREJ;
    private MenuText back;
    private LinearLayout backPanel;
    private Context context;
    private LinearLayout entryPanel;
    private LinearLayout menuAndrej;
    private LinearLayout menuItems;
    private LinearLayout menuLayout;
    private LinearLayout nameEntryPanel;
    private LinearLayout placeEntryPanel;
    private LinearLayout scoreEntryPanel;
    private ScrollView scrollView;
    private LinearLayout titlePanel;

    public HighscorePanel(Context context2) {
        super(context2);
        this.context = context2;
        generateHighscorePanel();
        createEntries();
        setBackgroundDrawable(new BitmapDrawable(Main.BACKGROUND));
        addView(this.menuLayout);
    }

    private void createEntries() {
        Main.HIGHSCORE.clearHighscoreList();
        Main.HIGHSCORE.loadHighscore();
        int textSize = 26;
        ArrayList<Entry> list = Main.HIGHSCORE.getHighscoreList();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            if (i < 3) {
                textSize -= 2;
            } else {
                textSize = 15;
            }
            MenuText entry = new MenuText(this.context, "");
            entry.setTextSize(1, (float) textSize);
            entry.setText((i + 1) + ". ");
            this.placeEntryPanel.addView(entry);
            MenuText entry2 = new MenuText(this.context, "");
            entry2.setTextSize(1, (float) textSize);
            entry2.setText(list.get(i).getPlayerName());
            this.nameEntryPanel.addView(entry2);
            MenuText entry3 = new MenuText(this.context, "");
            entry3.setTextSize(1, (float) textSize);
            entry3.setText(list.get(i).getScore() + " ");
            this.scoreEntryPanel.addView(entry3);
        }
    }

    private void generateHighscorePanel() {
        LinearLayout.LayoutParams layoutMain = new LinearLayout.LayoutParams(-1, -1);
        LinearLayout.LayoutParams layoutPanel = new LinearLayout.LayoutParams(-2, -1);
        layoutPanel.weight = 1.0f;
        LinearLayout.LayoutParams layoutItem = new LinearLayout.LayoutParams(-2, -2);
        LinearLayout.LayoutParams scrollItem = new LinearLayout.LayoutParams(-1, -2);
        scrollItem.weight = 1.0f;
        setLayoutParams(layoutMain);
        this.menuLayout = new LinearLayout(this.context);
        this.menuLayout.setLayoutParams(layoutMain);
        this.menuLayout.setWeightSum(2.0f);
        this.menuItems = new LinearLayout(this.context);
        this.menuItems.setLayoutParams(layoutPanel);
        this.menuItems.setOrientation(1);
        this.menuItems.setMinimumWidth(Main.WIDTH - this.andrej.getWidth());
        this.titlePanel = new LinearLayout(this.context);
        this.titlePanel.setLayoutParams(layoutItem);
        this.titlePanel.setGravity(3);
        MenuText title = new MenuText(this.context, "Highscore");
        title.setTextSize(1, 45.0f);
        this.titlePanel.addView(title);
        this.titlePanel.setMinimumHeight((int) title.getTextSize());
        this.scrollView = new ScrollView(this.context);
        this.scrollView.setVerticalScrollBarEnabled(false);
        this.scrollView.setHorizontalScrollBarEnabled(false);
        this.scrollView.setLayoutParams(scrollItem);
        this.entryPanel = new LinearLayout(this.context);
        this.entryPanel.setLayoutParams(layoutItem);
        this.entryPanel.setGravity(1);
        this.entryPanel.setOrientation(0);
        this.entryPanel.setWeightSum(3.0f);
        this.placeEntryPanel = new LinearLayout(this.context);
        this.placeEntryPanel.setLayoutParams(scrollItem);
        this.placeEntryPanel.setGravity(5);
        this.placeEntryPanel.setOrientation(1);
        this.placeEntryPanel.setMinimumWidth((Main.WIDTH - this.andrej.getWidth()) / 6);
        this.nameEntryPanel = new LinearLayout(this.context);
        this.nameEntryPanel.setLayoutParams(scrollItem);
        this.nameEntryPanel.setGravity(3);
        this.nameEntryPanel.setOrientation(1);
        this.nameEntryPanel.setMinimumWidth((Main.WIDTH - this.andrej.getWidth()) / 2);
        this.scoreEntryPanel = new LinearLayout(this.context);
        this.scoreEntryPanel.setLayoutParams(scrollItem);
        this.scoreEntryPanel.setGravity(5);
        this.scoreEntryPanel.setOrientation(1);
        this.scoreEntryPanel.setMinimumWidth((Main.WIDTH - this.andrej.getWidth()) / 3);
        this.entryPanel.addView(this.placeEntryPanel);
        this.entryPanel.addView(this.nameEntryPanel);
        this.entryPanel.addView(this.scoreEntryPanel);
        this.scrollView.addView(this.entryPanel);
        this.backPanel = new LinearLayout(this.context);
        this.backPanel.setLayoutParams(layoutItem);
        this.backPanel.setGravity(3);
        this.back = new MenuText(this.context, "Back");
        this.back.setTextSize(1, 25.0f);
        this.back.setOnClickListener(this);
        this.backPanel.addView(this.back);
        this.backPanel.setMinimumHeight((int) this.back.getTextSize());
        this.menuItems.addView(this.titlePanel);
        this.menuItems.addView(this.scrollView);
        this.menuItems.addView(this.backPanel);
        this.menuAndrej = new LinearLayout(this.context);
        this.menuAndrej.setGravity(85);
        this.menuAndrej.setLayoutParams(layoutPanel);
        this.menuAndrej.setMinimumWidth(this.andrej.getWidth());
        this.menuAndrej.setMinimumHeight(this.andrej.getHeight());
        ImageView image = new ImageView(this.context);
        image.setImageBitmap(this.andrej);
        this.menuAndrej.addView(image);
        this.menuLayout.addView(this.menuItems);
        this.menuLayout.addView(this.menuAndrej);
    }

    public void onClick(View v) {
        if ((v instanceof MenuText) && v == this.back) {
            ((Main) this.context).switchView(1);
        }
    }
}
