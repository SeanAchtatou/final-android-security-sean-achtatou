package superiorcoding.stickimpactdemo.tutorial;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import superiorcoding.stickimpactdemo.Main;
import superiorcoding.stickimpactdemo.menu.MenuText;

public class TutorialPanel extends RelativeLayout implements View.OnClickListener {
    private static final String HEAD_1 = "First Steps";
    private static final String HEAD_2 = "Color Shifting";
    private static final String HEAD_3 = "Bonus Stick";
    private static final String HEAD_4 = "Live or Die";
    public static final String TEXT_1 = "To show your character where to go, simply tap at the desired destination.\nIt'll stop when the tapped point is reached.\n\nOf course you can also interrupt a current movement by just tapping another destination.\n Your character will instantly change the direction.\n\nTry not to stay close by the upper or lower screen border.\nYou should especially consider it in levels with a high stick speed.\n";
    public static final String TEXT_2 = "While gaming your character will change its color.\n\nYou should be extremely careful because it will happen in a random period of time!\n\nThere are 4 colors (blue,red,gray and black) your character can take on.\n";
    private static final String TEXT_3 = "Use colorless sticks to your advantage.\nIf you pass through these special sticks, they change colour and count extra 4 sticks!\n\nTry to hit as many as possible to progress through levels faster.\n";
    public static final String TEXT_4 = "You can pass through sticks that have the same color as your character.\nYou'll die if you try walking through a different colored stick.\n\nKeep in mind that you get points for sticks you avoid!\nIf you pass through a stick, it'll disappear and you'll accordingly get no point for it!\n";
    private Bitmap andrej = Main.ANDREJ;
    private MenuText back;
    private LinearLayout backPanel;
    private Context context;
    private LinearLayout entryPanel;
    private LinearLayout menuAndrej;
    private LinearLayout menuItems;
    private LinearLayout menuLayout;
    private ScrollView scrollView;
    private LinearLayout titlePanel;

    public TutorialPanel(Context context2) {
        super(context2);
        this.context = context2;
        generateTutorialPanel();
        setBackgroundDrawable(new BitmapDrawable(Main.BACKGROUND));
        addView(this.menuLayout);
    }

    private void generateTutorialPanel() {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -1);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-2, -1);
        layoutParams2.weight = 1.0f;
        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(-2, -2);
        LinearLayout.LayoutParams layoutParams4 = new LinearLayout.LayoutParams(-1, -2);
        layoutParams4.weight = 1.0f;
        setLayoutParams(layoutParams);
        this.menuLayout = new LinearLayout(this.context);
        this.menuLayout.setLayoutParams(layoutParams);
        this.menuLayout.setWeightSum(2.0f);
        this.menuItems = new LinearLayout(this.context);
        this.menuItems.setLayoutParams(layoutParams2);
        this.menuItems.setOrientation(1);
        this.menuItems.setMinimumWidth(Main.WIDTH - this.andrej.getWidth());
        this.titlePanel = new LinearLayout(this.context);
        this.titlePanel.setLayoutParams(layoutParams3);
        this.titlePanel.setGravity(3);
        MenuText title = new MenuText(this.context, "Tutorial");
        title.setTextSize(1, 45.0f);
        this.titlePanel.addView(title);
        this.titlePanel.setMinimumHeight((int) title.getTextSize());
        this.scrollView = new ScrollView(this.context);
        this.scrollView.setVerticalScrollBarEnabled(false);
        this.scrollView.setHorizontalScrollBarEnabled(false);
        this.scrollView.setLayoutParams(layoutParams4);
        this.entryPanel = new LinearLayout(this.context);
        this.entryPanel.setLayoutParams(layoutParams4);
        this.entryPanel.setGravity(3);
        this.entryPanel.setOrientation(1);
        MenuText menuText = new MenuText(this.context, HEAD_1);
        menuText.setTextSize(1, 25.0f);
        MenuText menuText2 = new MenuText(this.context, TEXT_1);
        menuText2.setTextSize(1, 15.0f);
        MenuText menuText3 = new MenuText(this.context, HEAD_2);
        menuText3.setTextSize(1, 25.0f);
        MenuText menuText4 = new MenuText(this.context, TEXT_2);
        menuText4.setTextSize(1, 15.0f);
        MenuText menuText5 = new MenuText(this.context, HEAD_3);
        menuText5.setTextSize(1, 25.0f);
        MenuText menuText6 = new MenuText(this.context, TEXT_3);
        menuText6.setTextSize(1, 15.0f);
        new MenuText(this.context, HEAD_4).setTextSize(1, 25.0f);
        new MenuText(this.context, TEXT_4).setTextSize(1, 15.0f);
        this.entryPanel.addView(menuText);
        this.entryPanel.addView(menuText2);
        this.entryPanel.addView(menuText3);
        this.entryPanel.addView(menuText4);
        this.entryPanel.addView(menuText5);
        this.entryPanel.addView(menuText6);
        this.scrollView.addView(this.entryPanel);
        this.backPanel = new LinearLayout(this.context);
        this.backPanel.setLayoutParams(layoutParams3);
        this.backPanel.setGravity(3);
        this.back = new MenuText(this.context, "Back");
        this.back.setTextSize(1, 25.0f);
        this.back.setOnClickListener(this);
        this.backPanel.addView(this.back);
        this.backPanel.setMinimumHeight((int) this.back.getTextSize());
        this.menuItems.addView(this.titlePanel);
        this.menuItems.addView(this.scrollView);
        this.menuItems.addView(this.backPanel);
        this.menuAndrej = new LinearLayout(this.context);
        this.menuAndrej.setGravity(85);
        this.menuAndrej.setLayoutParams(layoutParams2);
        this.menuAndrej.setMinimumWidth(this.andrej.getWidth());
        this.menuAndrej.setMinimumHeight(this.andrej.getHeight());
        ImageView imageView = new ImageView(this.context);
        imageView.setImageBitmap(this.andrej);
        this.menuAndrej.addView(imageView);
        this.menuLayout.addView(this.menuItems);
        this.menuLayout.addView(this.menuAndrej);
    }

    public void onClick(View v) {
        if ((v instanceof MenuText) && v == this.back) {
            ((Main) this.context).switchView(1);
        }
    }
}
