package superiorcoding.stickimpactdemo.menu;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;
import superiorcoding.stickimpactdemo.Main;
import superiorcoding.stickimpactdemo.game.GameView;

public class MenuPanel extends RelativeLayout implements View.OnClickListener {
    public static final int ITEM_SIZE = 25;
    public static final int PLAYER_SIZE = 15;
    public static final int TITLE_SIZE = 45;
    private MenuText achievements;
    private Bitmap andrej = Main.ANDREJ_TUTORIAL;
    private MenuAnimation animation;
    private RelativeLayout animationPanel;
    private Context context;
    private LinearLayout entryPanel;
    private MenuText exit;
    private MenuText highscore;
    private LinearLayout menuAndrej;
    private LinearLayout menuItems;
    private LinearLayout menuLayout;
    private RelativeLayout menuPanel;
    private AlertDialog.Builder nameDialog;
    /* access modifiers changed from: private */
    public MenuText playerName;
    private LinearLayout playerPanel;
    private MenuText resumeGame;
    private MenuText settings;
    private MenuText startGame;
    private LinearLayout titlePanel;
    private ImageView tutorialImage;

    public MenuPanel(Context context2) {
        super(context2);
        this.context = context2;
        this.animation = new MenuAnimation(context2, Main.WIDTH - this.andrej.getWidth());
        createAnimationPanel(context2);
        createMenuPanel(context2);
        addView(this.animationPanel);
        addView(this.menuPanel);
    }

    private void createAnimationPanel(Context context2) {
        this.animationPanel = new RelativeLayout(context2);
        this.animationPanel.addView(this.animation);
    }

    public void updateGameState() {
        if (GameView.GAME_PAUSED) {
            this.startGame.setVisibility(8);
            this.resumeGame.setVisibility(0);
            return;
        }
        this.startGame.setVisibility(0);
        this.resumeGame.setVisibility(8);
    }

    private void createMenuPanel(Context context2) {
        LinearLayout.LayoutParams layoutMain = new LinearLayout.LayoutParams(-1, -1);
        LinearLayout.LayoutParams layoutPanel = new LinearLayout.LayoutParams(-2, -1);
        layoutPanel.weight = 1.0f;
        LinearLayout.LayoutParams layoutItem = new LinearLayout.LayoutParams(-2, -2);
        layoutItem.weight = 1.0f;
        this.menuPanel = new RelativeLayout(context2);
        this.menuPanel.setLayoutParams(layoutMain);
        this.menuLayout = new LinearLayout(context2);
        this.menuLayout.setLayoutParams(layoutMain);
        this.menuLayout.setOrientation(0);
        this.menuLayout.setWeightSum(2.0f);
        this.menuItems = new LinearLayout(context2);
        this.menuItems.setLayoutParams(layoutPanel);
        this.menuItems.setOrientation(1);
        this.menuItems.setWeightSum(3.0f);
        this.titlePanel = new LinearLayout(context2);
        this.titlePanel.setLayoutParams(layoutItem);
        this.titlePanel.setGravity(48);
        MenuText title = new MenuText(context2, Main.NAME);
        title.setTextSize(1, 45.0f);
        this.titlePanel.addView(title);
        this.entryPanel = new LinearLayout(context2);
        this.entryPanel.setLayoutParams(layoutItem);
        this.entryPanel.setGravity(16);
        this.entryPanel.setOrientation(1);
        this.entryPanel.setWeightSum(6.0f);
        this.startGame = new MenuText(context2, "Start game");
        this.startGame.setLayoutParams(layoutItem);
        this.startGame.setOnClickListener(this);
        this.startGame.setTextSize(1, 25.0f);
        this.resumeGame = new MenuText(context2, "Resume game");
        this.resumeGame.setLayoutParams(layoutItem);
        this.resumeGame.setOnClickListener(this);
        this.resumeGame.setVisibility(8);
        this.resumeGame.setTextSize(1, 25.0f);
        this.highscore = new MenuText(context2, "Highscore");
        this.highscore.setLayoutParams(layoutItem);
        this.highscore.setOnClickListener(this);
        this.highscore.setTextSize(1, 25.0f);
        this.achievements = new MenuText(context2, "Achievements");
        this.achievements.setLayoutParams(layoutItem);
        this.achievements.setOnClickListener(this);
        this.achievements.setTextSize(1, 25.0f);
        this.settings = new MenuText(context2, "Settings");
        this.settings.setLayoutParams(layoutItem);
        this.settings.setOnClickListener(this);
        this.settings.setTextSize(1, 25.0f);
        this.exit = new MenuText(context2, "Exit");
        this.exit.setLayoutParams(layoutItem);
        this.exit.setOnClickListener(this);
        this.exit.setTextSize(1, 25.0f);
        this.entryPanel.addView(this.startGame);
        this.entryPanel.addView(this.resumeGame);
        this.entryPanel.addView(this.highscore);
        this.entryPanel.addView(this.achievements);
        this.entryPanel.addView(this.settings);
        this.entryPanel.addView(this.exit);
        this.playerPanel = new LinearLayout(context2);
        this.playerPanel.setLayoutParams(layoutItem);
        this.playerPanel.setGravity(80);
        this.playerName = new MenuText(context2, "Playing as " + Main.HIGHSCORE.getPlayerName());
        this.playerName.setTextSize(1, 15.0f);
        this.playerName.setOnClickListener(this);
        this.playerPanel.addView(this.playerName);
        this.menuItems.addView(this.titlePanel);
        this.menuItems.addView(this.entryPanel);
        this.menuItems.addView(this.playerPanel);
        this.menuAndrej = new LinearLayout(context2);
        this.menuAndrej.setGravity(85);
        this.menuAndrej.setLayoutParams(layoutPanel);
        this.tutorialImage = new ImageView(context2);
        this.tutorialImage.setImageBitmap(this.andrej);
        this.tutorialImage.setOnClickListener(this);
        this.menuAndrej.addView(this.tutorialImage);
        this.menuLayout.addView(this.menuItems);
        this.menuLayout.addView(this.menuAndrej);
        this.menuPanel.addView(this.menuLayout);
    }

    /* Debug info: failed to restart local var, previous not found, register: 5 */
    public void onClick(View v) {
        if (v instanceof MenuText) {
            if (v == this.startGame) {
                ((Main) this.context).switchView(2);
            } else if (v == this.resumeGame) {
                ((Main) this.context).switchView(2);
            } else if (v == this.highscore) {
                ((Main) this.context).switchView(3);
            } else if (v == this.achievements) {
                Toast.makeText(this.context.getApplicationContext(), "Only available in the pro version", 0).show();
            } else if (v == this.settings) {
                Toast.makeText(this.context.getApplicationContext(), "Only available in the pro version", 0).show();
            } else if (v == this.exit) {
                ((Main) this.context).finish();
            } else if (v == this.playerName) {
                createNameDialog();
                this.nameDialog.create().show();
            }
        } else if (v == this.tutorialImage) {
            ((Main) this.context).switchView(6);
        }
    }

    private void createNameDialog() {
        this.nameDialog = new AlertDialog.Builder(this.context);
        final EditText nameInput = new EditText(this.context);
        this.nameDialog.setMessage("Play as ...     ").setView(nameInput).setCancelable(false).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                String userInput = nameInput.getText().toString();
                if (userInput.length() == 0) {
                    userInput = "Nameless";
                } else {
                    userInput.replace(":", "|");
                }
                Main.HIGHSCORE.setPlayerName(userInput);
                MenuPanel.this.playerName.setText("Playing as " + userInput);
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
    }
}
