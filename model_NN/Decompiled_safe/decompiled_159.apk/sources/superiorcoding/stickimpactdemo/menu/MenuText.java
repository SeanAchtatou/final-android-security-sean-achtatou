package superiorcoding.stickimpactdemo.menu;

import android.content.Context;
import android.widget.RelativeLayout;
import android.widget.TextView;
import superiorcoding.stickimpactdemo.Main;

public class MenuText extends TextView {
    public MenuText(Context context, String text) {
        super(context);
        setText(text);
        setTypeface(Main.FONT);
        setTextColor(-16777216);
        setLayoutParams(new RelativeLayout.LayoutParams(-2, -2));
    }
}
