package superiorcoding.stickimpactdemo.menu;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.SurfaceHolder;
import java.lang.Thread;
import superiorcoding.stickimpactdemo.Main;
import superiorcoding.stickimpactdemo.engine.stick.StickEngine;
import superiorcoding.stickimpactdemo.game.ImpactPanel;
import superiorcoding.stickimpactdemo.game.MainThread;

public class MenuAnimation extends ImpactPanel implements SurfaceHolder.Callback {
    private Bitmap background = Main.BACKGROUND;
    private MainThread mainThread = new MainThread(getHolder(), this);
    private StickEngine stickEngine;

    public MenuAnimation(Context context, int width) {
        super(context);
        getHolder().addCallback(this);
        this.stickEngine = new StickEngine(context, new Rect(0, 0, width, Main.HEIGHT));
        this.stickEngine.generateWave();
        setKeepScreenOn(true);
        setFocusable(true);
    }

    public void render(Canvas canvas) {
        canvas.drawBitmap(this.background, 0.0f, 0.0f, (Paint) null);
        this.stickEngine.render(canvas);
    }

    public void update() {
        this.stickEngine.update(System.currentTimeMillis());
    }

    public void surfaceCreated(SurfaceHolder holder) {
        if (this.mainThread.getState() == Thread.State.TERMINATED) {
            this.mainThread = new MainThread(getHolder(), this);
        }
        this.mainThread.setRunning(true);
        this.mainThread.start();
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        boolean retry = true;
        while (retry) {
            try {
                this.mainThread.setRunning(false);
                this.mainThread.join();
                retry = false;
            } catch (InterruptedException e) {
            }
        }
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }
}
