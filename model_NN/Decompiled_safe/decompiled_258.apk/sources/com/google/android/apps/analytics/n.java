package com.google.android.apps.analytics;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

class n extends Handler {
    final /* synthetic */ b a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public n(b bVar, Looper looper) {
        super(looper);
        this.a = bVar;
    }

    public void handleMessage(Message message) {
        if (message.what == 13651479) {
            this.a.c();
        } else if (message.what == 6178583) {
            this.a.a(((Long) message.obj).longValue());
        }
    }
}
