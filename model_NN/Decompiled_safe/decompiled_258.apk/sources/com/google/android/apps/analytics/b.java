package com.google.android.apps.analytics;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.util.Log;

public class b {
    private static final b a = new b();
    private String b;
    private Context c;
    private ConnectivityManager d;
    private int e;
    private k f;
    private a g;
    private boolean h;
    private int i;
    private int j;
    private boolean k;
    private Handler l;
    private Runnable m = new f(this);

    private b() {
    }

    public static b a() {
        return a;
    }

    private void a(String str, String str2, String str3, String str4, int i2) {
        this.f.a(new m(this.f.c(), str, str2, str3, str4, i2, this.c.getResources().getDisplayMetrics().widthPixels, this.c.getResources().getDisplayMetrics().heightPixels));
        g();
    }

    private void e() {
        if (this.e >= 0 && this.l.postDelayed(this.m, (long) (this.e * 1000))) {
        }
    }

    private void f() {
        this.l.removeCallbacks(this.m);
    }

    private void g() {
        if (this.h) {
            this.h = false;
            e();
        }
    }

    public void a(int i2) {
        int i3 = this.e;
        this.e = i2;
        if (i3 <= 0) {
            e();
        } else if (i3 > 0) {
            f();
            e();
        }
    }

    /* access modifiers changed from: package-private */
    public void a(long j2) {
        this.f.a(j2);
        this.j++;
    }

    public void a(String str) {
        a(this.b, "__##GOOGLEPAGEVIEW##__", str, (String) null, -1);
    }

    public void a(String str, int i2, Context context) {
        a(str, i2, context, this.f == null ? new g(context) : this.f, this.g == null ? new l() : this.g);
    }

    /* access modifiers changed from: package-private */
    public void a(String str, int i2, Context context, k kVar, a aVar) {
        this.b = str;
        this.c = context;
        this.f = kVar;
        this.f.d();
        this.g = aVar;
        this.g.a(new n(this, this.c.getMainLooper()), this.f.e());
        this.i = 0;
        this.k = false;
        if (this.d == null) {
            this.d = (ConnectivityManager) this.c.getSystemService("connectivity");
        }
        if (this.l == null) {
            this.l = new Handler(context.getMainLooper());
        } else {
            f();
        }
        a(i2);
    }

    public void a(String str, String str2, String str3, int i2) {
        a(this.b, str, str2, str3, i2);
    }

    public boolean b() {
        if (this.k) {
            e();
            return false;
        }
        NetworkInfo activeNetworkInfo = this.d.getActiveNetworkInfo();
        if (activeNetworkInfo == null || !activeNetworkInfo.isAvailable()) {
            e();
            return false;
        }
        this.i = this.f.b();
        if (this.i != 0) {
            this.j = 0;
            this.g.a(this.f.a());
            this.k = true;
            e();
            return true;
        }
        this.h = true;
        return false;
    }

    /* access modifiers changed from: package-private */
    public void c() {
        int i2 = this.i - this.j;
        if (i2 != 0) {
            Log.w("googleanalytics", "Dispatcher thinks it finished, but there were " + i2 + " failed events");
        }
        this.i = 0;
        this.k = false;
    }

    public void d() {
        this.g.a();
        f();
    }
}
