package com.mobfox.sdk;

import android.content.Context;
import android.content.Intent;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.telephony.TelephonyManager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.RelativeLayout;
import android.widget.ViewFlipper;
import com.mobfox.sdk.a.a;
import com.mobfox.sdk.a.b;
import com.mobfox.sdk.a.d;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.Timer;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;

public class MobFoxView extends RelativeLayout {
    boolean a = false;
    final Handler b = new Handler();
    final Runnable c = new i(this);
    LocationListener d = new h(this);
    int e = 0;
    private boolean f = false;
    private m g;
    private String h;
    private boolean i;
    private Timer j;
    /* access modifiers changed from: private */
    public d k;
    private Animation l = null;
    private Animation m = null;
    private WebSettings n;
    /* access modifiers changed from: private */
    public a o;
    private LocationManager p;
    /* access modifiers changed from: private */
    public double q = 0.0d;
    /* access modifiers changed from: private */
    public double r = 0.0d;
    private WebView s;
    private WebView t;
    private ViewFlipper u;
    /* access modifiers changed from: private */
    public d v;
    /* access modifiers changed from: private */
    public boolean w;
    /* access modifiers changed from: private */
    public Thread x;
    private View.OnTouchListener y = new g(this);

    public MobFoxView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        if (attributeSet != null) {
            this.h = attributeSet.getAttributeValue(null, "publisherId");
            String attributeValue = attributeSet.getAttributeValue(null, "mode");
            if (attributeValue != null && attributeValue.equalsIgnoreCase("test")) {
                this.g = m.TEST;
            }
            String attributeValue2 = attributeSet.getAttributeValue(null, "animation");
            if (attributeValue2 == null || !attributeValue2.equals("false")) {
                this.i = true;
            } else {
                this.i = false;
            }
            this.f = attributeSet.getAttributeBooleanValue(null, "includeLocation", false);
        }
        a(context);
    }

    private void a(Context context) {
        this.s = b(context);
        this.t = b(context);
        if (Log.isLoggable("MOBFOX", 3)) {
            Log.d("MOBFOX", "Create view flipper");
        }
        this.u = new f(this, getContext());
        this.u.addView(this.s);
        this.u.addView(this.t);
        addView(this.u);
        this.s.setOnTouchListener(this.y);
        this.t.setOnTouchListener(this.y);
        if (Log.isLoggable("MOBFOX", 3)) {
            Log.d("MOBFOX", "animation: " + this.i);
        }
        if (this.i) {
            this.l = new TranslateAnimation(2, 0.0f, 2, 0.0f, 2, 1.0f, 2, 0.0f);
            this.l.setDuration(1000);
            this.m = new TranslateAnimation(2, 0.0f, 2, 0.0f, 2, 0.0f, 2, -1.0f);
            this.m.setDuration(1000);
            this.u.setInAnimation(this.l);
            this.u.setOutAnimation(this.m);
        }
    }

    private void a(String str) {
        if (this.k.f() == null || !this.k.f().equals(b.INAPP)) {
            getContext().startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
            return;
        }
        Intent intent = new Intent(getContext(), InAppWebView.class);
        intent.putExtra("REDIRECT_URI", this.k.g());
        getContext().startActivity(intent);
    }

    private WebView b(Context context) {
        WebView webView = new WebView(getContext());
        this.n = webView.getSettings();
        this.n.setJavaScriptEnabled(true);
        webView.setBackgroundColor(0);
        webView.setWebViewClient(new a(this, context));
        webView.setVerticalScrollBarEnabled(false);
        webView.setHorizontalScrollBarEnabled(false);
        return webView;
    }

    /* access modifiers changed from: private */
    public a e() {
        if (this.o == null) {
            this.o = new a();
            this.o.b(((TelephonyManager) getContext().getSystemService("phone")).getDeviceId());
            this.o.a(this.g);
            this.o.c(this.h);
            this.o.a(this.n.getUserAgentString());
            this.o.b(this.r);
            this.o.a(this.q);
        }
        return this.o;
    }

    private void f() {
        if (Log.isLoggable("MOBFOX", 3)) {
            Log.d("MOBFOX", "load content");
        }
        if (this.x == null) {
            this.x = new Thread(new j(this));
            this.x.start();
        }
    }

    /* access modifiers changed from: private */
    public void g() {
        WebView webView = this.u.getCurrentView() == this.s ? this.t : this.s;
        if (this.k.a() == l.IMAGE) {
            String format = MessageFormat.format("<body style='\"'margin: 0px; padding: 0px; text-align:center;'\"'><img src='\"'{0}'\"' width='\"'{1}'dp\"' height='\"'{2}'dp\"'/></body>", this.k.d(), Integer.valueOf(this.k.b()), Integer.valueOf(this.k.c()));
            if (Log.isLoggable("MOBFOX", 3)) {
                Log.d("MOBFOX", "set image: " + format);
            }
            webView.loadData(Uri.encode("<style>* { -webkit-tap-highlight-color: rgba(0,0,0,0) }</style>" + format), "text/html", "UTF-8");
            if (this.v != null) {
                Log.d("MOBFOX", "notify bannerListener of load succeeded: " + this.v.getClass().getName());
                this.v.b();
            }
        } else if (this.k.a() == l.TEXT) {
            String encode = Uri.encode("<style>* { -webkit-tap-highlight-color: rgba(0,0,0,0) }</style>" + this.k.e());
            if (Log.isLoggable("MOBFOX", 3)) {
                Log.d("MOBFOX", "set text: " + encode);
            }
            webView.loadData(encode, "text/html", "UTF-8");
            if (this.v != null) {
                Log.d("MOBFOX", "notify bannerListener of load succeeded: " + this.v.getClass().getName());
                this.v.b();
            }
        } else {
            if (Log.isLoggable("MOBFOX", 3)) {
                Log.d("MOBFOX", "No Ad");
            }
            if (this.v != null) {
                this.v.a();
                return;
            }
            return;
        }
        if (this.u.getCurrentView() == this.s) {
            if (Log.isLoggable("MOBFOX", 3)) {
                Log.d("MOBFOX", "show next");
            }
            this.u.showNext();
        } else {
            if (Log.isLoggable("MOBFOX", 3)) {
                Log.d("MOBFOX", "show previous");
            }
            this.u.showPrevious();
        }
        h();
    }

    private void h() {
        if (Log.isLoggable("MOBFOX", 3)) {
            Log.d("MOBFOX", "start reload timer");
        }
        if (this.j != null) {
            int h2 = this.k.h() * 1000;
            if (Log.isLoggable("MOBFOX", 3)) {
                Log.d("MOBFOX", "set timer: " + h2);
            }
            this.j.schedule(new c(this), (long) h2);
        }
    }

    private void i() {
        if (getContext().checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION") == 0) {
            this.p = (LocationManager) getContext().getSystemService("location");
            if (this.p.isProviderEnabled("gps")) {
                this.p.requestLocationUpdates("gps", 0, 0.0f, this.d, Looper.getMainLooper());
            }
        }
    }

    public void a() {
        if (this.k != null && this.k.g() != null) {
            if (this.k.i()) {
                a(this.k.g());
                return;
            }
            if (Log.isLoggable("MOBFOX", 3)) {
                Log.d("MOBFOX", "prefetch url: " + this.k.g());
            }
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet(this.k.g());
            BasicHttpContext basicHttpContext = new BasicHttpContext();
            try {
                HttpResponse execute = defaultHttpClient.execute(httpGet, basicHttpContext);
                if (execute.getStatusLine().getStatusCode() != 200) {
                    throw new IOException(execute.getStatusLine().toString());
                }
                a(((HttpHost) basicHttpContext.getAttribute("http.target_host")).toURI() + ((HttpUriRequest) basicHttpContext.getAttribute("http.request")).getURI());
            } catch (ClientProtocolException e2) {
                Log.e("MOBFOX", "Error in HTTP request", e2);
            } catch (IOException e3) {
                Log.e("MOBFOX", "Error in HTTP request", e3);
            }
        }
    }

    public void b() {
        if (Log.isLoggable("MOBFOX", 3)) {
            Log.d("MOBFOX", "load next ad");
        }
        f();
    }

    public void c() {
        if (this.j != null) {
            try {
                if (Log.isLoggable("MOBFOX", 3)) {
                    Log.d("MOBFOX", "cancel reload timer");
                }
                this.j.cancel();
                this.j = null;
            } catch (Exception e2) {
                Log.e("MOBFOX", "unable to cancel reloadTimer", e2);
            }
        }
    }

    public void d() {
        if (this.j != null) {
            this.j.cancel();
            this.j = null;
        }
        this.j = new Timer();
        if (Log.isLoggable("MOBFOX", 3)) {
            Log.d("MOBFOX", "response: " + this.k);
        }
        if (this.k == null || this.k.h() <= 0) {
            f();
        } else {
            h();
        }
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (Log.isLoggable("MOBFOX", 3)) {
            Log.d("MOBFOX", "onDetachedFromWindow");
        }
        if (this.f) {
            i();
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        if (Log.isLoggable("MOBFOX", 3)) {
            Log.d("MOBFOX", "onDetachedFromWindow");
        }
        if (this.p != null && this.d != null) {
            this.p.removeUpdates(this.d);
        }
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int i2) {
        super.onWindowVisibilityChanged(i2);
        if (i2 == 0) {
            d();
        } else {
            c();
        }
        if (Log.isLoggable("MOBFOX", 3)) {
            Log.d("MOBFOX", "onWindowVisibilityChanged: " + i2);
        }
    }
}
