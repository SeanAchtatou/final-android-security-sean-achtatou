package com.mobfox.sdk.a;

public enum b {
    INAPP,
    BROWSER;

    public static b a(String str) {
        for (b bVar : values()) {
            if (bVar.name().equalsIgnoreCase(str)) {
                return bVar;
            }
        }
        return null;
    }
}
