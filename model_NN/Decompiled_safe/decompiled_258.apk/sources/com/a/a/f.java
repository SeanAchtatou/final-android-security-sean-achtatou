package com.a.a;

import android.content.Context;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.Log;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

public final class f {
    /* access modifiers changed from: private */
    public static boolean h = false;
    private static boolean i = false;
    private static int j = 15000;
    private String a;
    private String b = null;
    private String c = null;
    private String d;
    private String e;
    private Context f;
    private boolean g = false;
    private Runnable k = new g(this);

    public f(Context context, String str) {
        this.f = context;
        this.e = str;
        this.a = context.getFilesDir() + "/" + "localytics" + "/" + this.e + "/";
        try {
            if (new File(String.valueOf(this.a) + "opted_out").exists()) {
                i = false;
            } else {
                i = true;
            }
        } catch (Exception e2) {
            Log.v("Localytics_Session", "Swallowing exception: " + e2.getMessage());
        }
    }

    private File a(String str, String str2) {
        File file = new File(String.valueOf(str2) + str);
        if (file.exists()) {
            return file;
        }
        new File(str2).mkdirs();
        try {
            if (!file.createNewFile()) {
                return null;
            }
            return file;
        } catch (IOException e2) {
            Log.v("Localytics_Session", "Unable to get or create file: " + str + " in path: " + str2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    private static void a(File file, String str) {
        if (file != null) {
            try {
                synchronized (f.class) {
                    FileOutputStream fileOutputStream = new FileOutputStream(file, true);
                    fileOutputStream.write(str.getBytes());
                    fileOutputStream.close();
                }
            } catch (IOException e2) {
                Log.v("Localytics_Session", "AppendDataToFile failed with IO Exception: " + e2.getMessage());
            }
        }
    }

    private File b(String str) {
        return a(str, this.a);
    }

    private static void b(File file, String str) {
        if (file != null) {
            try {
                FileWriter fileWriter = new FileWriter(file);
                fileWriter.write(str);
                fileWriter.flush();
                fileWriter.close();
            } catch (IOException e2) {
                Log.v("Localytics_Session", "Ovewriting file failed with IO Exception: " + e2.getMessage());
            }
        }
    }

    private String d() {
        StringBuffer stringBuffer = new StringBuffer();
        TelephonyManager telephonyManager = (TelephonyManager) this.f.getSystemService("phone");
        Locale locale = Locale.getDefault();
        stringBuffer.append("- c: se\n");
        stringBuffer.append("  a: c\n");
        stringBuffer.append("  se:\n");
        stringBuffer.append(b.a("u", this.d, 3));
        stringBuffer.append(b.a("au", this.e, 3));
        stringBuffer.append(b.a("av", b.b(this.f), 3));
        stringBuffer.append(b.a("lv", "1.5", 3));
        stringBuffer.append(b.a("ct", b.a(), 3));
        stringBuffer.append(b.a("du", e(), 3));
        stringBuffer.append(b.a("dp", "Android", 3));
        stringBuffer.append(b.a("dov", Build.ID, 3));
        stringBuffer.append(b.a("dmo", Build.MODEL, 3));
        stringBuffer.append(b.a("dll", locale.getLanguage(), 3));
        stringBuffer.append(b.a("dlc", locale.getCountry(), 3));
        stringBuffer.append(b.a("dc", telephonyManager.getSimCountryIso(), 3));
        stringBuffer.append(b.a("nca", telephonyManager.getNetworkOperatorName(), 3));
        stringBuffer.append(b.a("nc", telephonyManager.getNetworkCountryIso(), 3));
        stringBuffer.append(b.a("dac", b.a(this.f, telephonyManager), 3));
        return stringBuffer.toString();
    }

    private String e() {
        String a2 = b.a(this.f);
        return a2 == null ? f() : a2;
    }

    private String f() {
        String str;
        FileNotFoundException fileNotFoundException;
        String str2;
        IOException iOException;
        File a2 = a("device_id", this.f.getFilesDir() + "/" + "localytics" + "/");
        if (a2.length() == 0) {
            String uuid = UUID.randomUUID().toString();
            a(a2, uuid);
            return uuid;
        }
        try {
            char[] cArr = new char[100];
            BufferedReader bufferedReader = new BufferedReader(new FileReader(a2), 100);
            String copyValueOf = String.copyValueOf(cArr, 0, bufferedReader.read(cArr));
            try {
                bufferedReader.close();
                return copyValueOf;
            } catch (FileNotFoundException e2) {
                FileNotFoundException fileNotFoundException2 = e2;
                str = copyValueOf;
                fileNotFoundException = fileNotFoundException2;
            } catch (IOException e3) {
                IOException iOException2 = e3;
                str2 = copyValueOf;
                iOException = iOException2;
                Log.v("Localytics_Session", "GetLocalDeviceId Failed with IO Exception: " + iOException.getMessage());
                return str2;
            }
        } catch (FileNotFoundException e4) {
            FileNotFoundException fileNotFoundException3 = e4;
            str = null;
            fileNotFoundException = fileNotFoundException3;
        } catch (IOException e5) {
            IOException iOException3 = e5;
            str2 = null;
            iOException = iOException3;
            Log.v("Localytics_Session", "GetLocalDeviceId Failed with IO Exception: " + iOException.getMessage());
            return str2;
        }
        Log.v("Localytics_Session", "GetLocalDeviceID failed with FNF: " + fileNotFoundException.getMessage());
        return str;
    }

    private String g() {
        File file = new File(String.valueOf(this.a) + "last_session_id");
        if (file.exists()) {
            try {
                BufferedReader bufferedReader = new BufferedReader(new FileReader(file), 100);
                String readLine = bufferedReader.readLine();
                String readLine2 = bufferedReader.readLine();
                bufferedReader.close();
                if (readLine2 != null) {
                    if (((long) j) > System.currentTimeMillis() - Long.parseLong(readLine2)) {
                        return readLine;
                    }
                }
            } catch (FileNotFoundException e2) {
                Log.v("Localytics_Session", "File Not Found opening stored session");
                return null;
            } catch (IOException e3) {
                Log.v("Localytics_Session", "IO Exception getting stored session: " + e3.getMessage());
                return null;
            }
        }
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:?, code lost:
        r5.d = g();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0020, code lost:
        if (r5.d == null) goto L_0x006d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0022, code lost:
        r5.b = "s_" + r5.d;
        r5.c = "c_" + r5.d;
        android.util.Log.v("Localytics_Session", "Reconnected to existing session");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0050, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0051, code lost:
        android.util.Log.v("Localytics_Session", "Swallowing exception: " + r0.getMessage());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        r0 = new java.io.File(r5.a);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0078, code lost:
        if (r0.exists() == false) goto L_0x0093;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0086, code lost:
        if (r0.list(new com.a.a.h(r5)).length < 10) goto L_0x0093;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0088, code lost:
        r5.g = false;
        android.util.Log.v("Localytics_Session", "Queue full, session not created");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0093, code lost:
        r5.d = java.util.UUID.randomUUID().toString();
        r5.b = "s_" + r5.d;
        r5.c = "c_" + r5.d;
        r0 = b(r5.b);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00c9, code lost:
        if (r0 != null) goto L_0x00d0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00cb, code lost:
        r5.g = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00d8, code lost:
        if (r0.length() == 0) goto L_0x00e3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00da, code lost:
        android.util.Log.v("Localytics_Session", "Session already opened");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00e3, code lost:
        a(r0, d());
        android.util.Log.v("Localytics_Session", "Session opened");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:?, code lost:
        return;
     */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a() {
        /*
            r5 = this;
            java.lang.Class<com.a.a.f> r0 = com.a.a.f.class
            monitor-enter(r0)
            boolean r1 = com.a.a.f.i     // Catch:{ all -> 0x006a }
            if (r1 == 0) goto L_0x000b
            boolean r1 = r5.g     // Catch:{ all -> 0x006a }
            if (r1 == 0) goto L_0x0014
        L_0x000b:
            java.lang.String r1 = "Localytics_Session"
            java.lang.String r2 = "Session not opened"
            android.util.Log.v(r1, r2)     // Catch:{ all -> 0x006a }
            monitor-exit(r0)     // Catch:{ all -> 0x006a }
        L_0x0013:
            return
        L_0x0014:
            r1 = 1
            r5.g = r1     // Catch:{ all -> 0x006a }
            monitor-exit(r0)     // Catch:{ all -> 0x006a }
            java.lang.String r0 = r5.g()     // Catch:{ Exception -> 0x0050 }
            r5.d = r0     // Catch:{ Exception -> 0x0050 }
            java.lang.String r0 = r5.d     // Catch:{ Exception -> 0x0050 }
            if (r0 == 0) goto L_0x006d
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0050 }
            java.lang.String r1 = "s_"
            r0.<init>(r1)     // Catch:{ Exception -> 0x0050 }
            java.lang.String r1 = r5.d     // Catch:{ Exception -> 0x0050 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x0050 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0050 }
            r5.b = r0     // Catch:{ Exception -> 0x0050 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0050 }
            java.lang.String r1 = "c_"
            r0.<init>(r1)     // Catch:{ Exception -> 0x0050 }
            java.lang.String r1 = r5.d     // Catch:{ Exception -> 0x0050 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x0050 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0050 }
            r5.c = r0     // Catch:{ Exception -> 0x0050 }
            java.lang.String r0 = "Localytics_Session"
            java.lang.String r1 = "Reconnected to existing session"
            android.util.Log.v(r0, r1)     // Catch:{ Exception -> 0x0050 }
            goto L_0x0013
        L_0x0050:
            r0 = move-exception
            java.lang.String r1 = "Localytics_Session"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "Swallowing exception: "
            r2.<init>(r3)
            java.lang.String r0 = r0.getMessage()
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            android.util.Log.v(r1, r0)
            goto L_0x0013
        L_0x006a:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x006a }
            throw r1
        L_0x006d:
            java.io.File r0 = new java.io.File     // Catch:{ Exception -> 0x0050 }
            java.lang.String r1 = r5.a     // Catch:{ Exception -> 0x0050 }
            r0.<init>(r1)     // Catch:{ Exception -> 0x0050 }
            boolean r1 = r0.exists()     // Catch:{ Exception -> 0x0050 }
            if (r1 == 0) goto L_0x0093
            com.a.a.h r1 = new com.a.a.h     // Catch:{ Exception -> 0x0050 }
            r1.<init>(r5)     // Catch:{ Exception -> 0x0050 }
            java.lang.String[] r0 = r0.list(r1)     // Catch:{ Exception -> 0x0050 }
            int r0 = r0.length     // Catch:{ Exception -> 0x0050 }
            r1 = 10
            if (r0 < r1) goto L_0x0093
            r0 = 0
            r5.g = r0     // Catch:{ Exception -> 0x0050 }
            java.lang.String r0 = "Localytics_Session"
            java.lang.String r1 = "Queue full, session not created"
            android.util.Log.v(r0, r1)     // Catch:{ Exception -> 0x0050 }
            goto L_0x0013
        L_0x0093:
            java.util.UUID r0 = java.util.UUID.randomUUID()     // Catch:{ Exception -> 0x0050 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0050 }
            r5.d = r0     // Catch:{ Exception -> 0x0050 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0050 }
            java.lang.String r1 = "s_"
            r0.<init>(r1)     // Catch:{ Exception -> 0x0050 }
            java.lang.String r1 = r5.d     // Catch:{ Exception -> 0x0050 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x0050 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0050 }
            r5.b = r0     // Catch:{ Exception -> 0x0050 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0050 }
            java.lang.String r1 = "c_"
            r0.<init>(r1)     // Catch:{ Exception -> 0x0050 }
            java.lang.String r1 = r5.d     // Catch:{ Exception -> 0x0050 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x0050 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0050 }
            r5.c = r0     // Catch:{ Exception -> 0x0050 }
            java.lang.String r0 = r5.b     // Catch:{ Exception -> 0x0050 }
            java.io.File r0 = r5.b(r0)     // Catch:{ Exception -> 0x0050 }
            if (r0 != 0) goto L_0x00d0
            r0 = 0
            r5.g = r0     // Catch:{ Exception -> 0x0050 }
            goto L_0x0013
        L_0x00d0:
            long r1 = r0.length()     // Catch:{ Exception -> 0x0050 }
            r3 = 0
            int r1 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r1 == 0) goto L_0x00e3
            java.lang.String r0 = "Localytics_Session"
            java.lang.String r1 = "Session already opened"
            android.util.Log.v(r0, r1)     // Catch:{ Exception -> 0x0050 }
            goto L_0x0013
        L_0x00e3:
            java.lang.String r1 = r5.d()     // Catch:{ Exception -> 0x0050 }
            a(r0, r1)     // Catch:{ Exception -> 0x0050 }
            java.lang.String r0 = "Localytics_Session"
            java.lang.String r1 = "Session opened"
            android.util.Log.v(r0, r1)     // Catch:{ Exception -> 0x0050 }
            goto L_0x0013
        */
        throw new UnsupportedOperationException("Method not decompiled: com.a.a.f.a():void");
    }

    public void a(String str) {
        a(str, (Map) null);
    }

    public void a(String str, Map map) {
        if (!i || !this.g) {
            Log.v("Localytics_Session", "Tag not written");
            return;
        }
        try {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("- c: ev\n");
            stringBuffer.append("  a: c\n");
            stringBuffer.append("  ev:\n");
            stringBuffer.append(b.a("au", this.e, 3));
            stringBuffer.append(b.a("u", UUID.randomUUID().toString(), 3));
            stringBuffer.append(b.a("su", this.d, 3));
            stringBuffer.append(b.a("ct", b.a(), 3));
            stringBuffer.append(b.a("n", str, 3));
            if (map != null) {
                stringBuffer.append("   attrs:\n");
                Iterator it = map.keySet().iterator();
                int i2 = 0;
                while (true) {
                    int i3 = i2;
                    if (!it.hasNext() || i3 >= 10) {
                        break;
                    }
                    String str2 = (String) it.next();
                    stringBuffer.append(b.a(str2, (String) map.get(str2), 4));
                    i2 = i3 + 1;
                }
            }
            a(b(this.b), stringBuffer.toString());
            Log.v("Localytics_Session", "Tag written.");
        } catch (Exception e2) {
            Log.v("Localytics_Session", "Swallowing exception: " + e2.getMessage());
        }
    }

    public void b() {
        if (!i || !this.g) {
            Log.v("Localytics_Session", "Session not closed.");
            return;
        }
        try {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("- c: se\n");
            stringBuffer.append("  a: u\n");
            stringBuffer.append(b.a("u", this.d, 2));
            stringBuffer.append("  se:\n");
            stringBuffer.append(b.a("au", this.e, 3));
            stringBuffer.append(b.a("ctc", b.a(), 3));
            b(b(this.c), stringBuffer.toString());
            b(b("last_session_id"), String.valueOf(this.d) + "\n" + Long.toString(System.currentTimeMillis()));
            Log.v("Localytics_Session", "Close event written.");
        } catch (Exception e2) {
            Log.v("Localytics_Session", "Swallowing exception: " + e2.getMessage());
        }
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void c() {
        /*
            r6 = this;
            java.lang.Class<com.a.a.f> r0 = com.a.a.f.class
            monitor-enter(r0)
            boolean r1 = com.a.a.f.h     // Catch:{ all -> 0x003f }
            if (r1 == 0) goto L_0x0009
            monitor-exit(r0)     // Catch:{ all -> 0x003f }
        L_0x0008:
            return
        L_0x0009:
            r1 = 1
            com.a.a.f.h = r1     // Catch:{ all -> 0x003f }
            monitor-exit(r0)     // Catch:{ all -> 0x003f }
            java.io.File r1 = new java.io.File     // Catch:{ Exception -> 0x0025 }
            java.lang.String r0 = r6.a     // Catch:{ Exception -> 0x0025 }
            r1.<init>(r0)     // Catch:{ Exception -> 0x0025 }
            com.a.a.a r0 = new com.a.a.a     // Catch:{ Exception -> 0x0025 }
            java.lang.String r2 = "s_"
            java.lang.String r3 = "u_"
            java.lang.String r4 = "c_"
            java.lang.Runnable r5 = r6.k     // Catch:{ Exception -> 0x0025 }
            r0.<init>(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x0025 }
            r0.start()     // Catch:{ Exception -> 0x0025 }
            goto L_0x0008
        L_0x0025:
            r0 = move-exception
            java.lang.String r1 = "Localytics_Session"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "Swallowing exception: "
            r2.<init>(r3)
            java.lang.String r0 = r0.getMessage()
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            android.util.Log.v(r1, r0)
            goto L_0x0008
        L_0x003f:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x003f }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.a.a.f.c():void");
    }
}
