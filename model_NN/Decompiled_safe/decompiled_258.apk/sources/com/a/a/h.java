package com.a.a;

import java.io.File;
import java.io.FilenameFilter;

class h implements FilenameFilter {
    final /* synthetic */ f a;

    h(f fVar) {
        this.a = fVar;
    }

    public boolean accept(File file, String str) {
        return str.startsWith("s_") || str.startsWith("u_");
    }
}
