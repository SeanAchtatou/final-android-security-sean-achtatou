package com.a.a;

import android.content.Context;
import android.content.pm.PackageManager;
import android.net.wifi.WifiManager;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public final class b {
    private b() {
    }

    public static String a() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss-00:00");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return simpleDateFormat.format(new Date());
    }

    public static String a(Context context) {
        String string = Settings.System.getString(context.getContentResolver(), "android_id");
        if (string == null) {
            return null;
        }
        try {
            return new String(new BigInteger(1, MessageDigest.getInstance("SHA-256").digest(string.getBytes())).toString(16));
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
    }

    public static String a(Context context, TelephonyManager telephonyManager) {
        try {
            if (((WifiManager) context.getSystemService("wifi")).isWifiEnabled()) {
                return "wifi";
            }
        } catch (Exception e) {
        }
        switch (telephonyManager.getNetworkType()) {
            case 0:
                return "unknown";
            case 1:
                return "GPRS";
            case 2:
                return "edge";
            case 3:
                return "UMTS";
            default:
                return "none";
        }
    }

    private static String a(String str) {
        int i = 0;
        StringBuffer stringBuffer = new StringBuffer("\"");
        int length = str.length();
        if (str == null) {
            return "";
        }
        int i2 = 0;
        while (i < length) {
            if (str.charAt(i) == '\"' || str.charAt(i) == '\\') {
                stringBuffer.append(str.substring(i2, i));
                stringBuffer.append('\\');
                i2 = i;
            } else if (str.charAt(i) == 0) {
                stringBuffer.append(str.substring(i2, i));
                i2 = i + 1;
            }
            i++;
        }
        stringBuffer.append(str.substring(i2, i));
        stringBuffer.append('\"');
        return stringBuffer.toString();
    }

    public static String a(String str, String str2, int i) {
        String str3;
        String str4;
        if (str.length() > 128) {
            Log.v("(DatapointHelper) ", "Parameter name exceeds 128 character limit.  Truncating.");
            str3 = str.substring(0, 128);
        } else {
            str3 = str;
        }
        if (str2.length() > 128) {
            Log.v("(DatapointHelper) ", "Parameter value exceeds 128 character limit.  Truncating.");
            str4 = str2.substring(0, 128);
        } else {
            str4 = str2;
        }
        StringBuffer stringBuffer = new StringBuffer();
        for (int i2 = 0; i2 < i; i2++) {
            stringBuffer.append(" ");
        }
        stringBuffer.append(a(str3));
        stringBuffer.append(": ");
        stringBuffer.append(a(str4));
        stringBuffer.append("\n");
        return stringBuffer.toString();
    }

    public static String b(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            return "unknown";
        }
    }
}
