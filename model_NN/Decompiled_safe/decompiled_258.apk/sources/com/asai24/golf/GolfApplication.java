package com.asai24.golf;

import android.app.Application;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.preference.PreferenceManager;
import android.util.Log;
import com.a.a.f;
import com.asai24.golf.d.g;
import com.google.android.apps.analytics.b;

public class GolfApplication extends Application {
    private static GolfApplication a;
    private b b;
    private f c;

    public static String d() {
        return PreferenceManager.getDefaultSharedPreferences(a).getString(a.getString(R.string.key_oob_username), "");
    }

    public static String e() {
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(a);
        Boolean valueOf = Boolean.valueOf(defaultSharedPreferences.getBoolean(a.getString(R.string.key_oob_encrypted), false));
        String string = defaultSharedPreferences.getString(a.getString(R.string.key_oob_password), "");
        if (!valueOf.booleanValue()) {
            return string;
        }
        try {
            return g.b(string);
        } catch (Exception e) {
            Log.e("GolfApplication", e.getMessage());
            return "";
        }
    }

    public static boolean f() {
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(a);
        Resources resources = a.getResources();
        String string = defaultSharedPreferences.getString(a.getString(R.string.key_owner_gender), "");
        return string == "" || string.equals(resources.getStringArray(R.array.gender_entryvalues)[0]);
    }

    private void g() {
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor edit = defaultSharedPreferences.edit();
        if (defaultSharedPreferences.getString(getString(R.string.key_owner_name), "") == "") {
            edit.putString(getString(R.string.key_owner_name), "me");
            com.asai24.golf.a.b.a(this).b("me");
        }
        edit.commit();
    }

    public void a() {
        if (!b.a.booleanValue()) {
            this.b.d();
        }
    }

    public void a(String str) {
        if (!b.a.booleanValue()) {
            this.b.a(str);
        }
    }

    public void a(String str, String str2, String str3, int i) {
        if (!b.a.booleanValue()) {
            this.b.a(str, str2, str3, i);
        }
    }

    public void b() {
        if (!b.b.booleanValue()) {
            this.c.b();
        }
    }

    public void b(String str) {
        if (!b.b.booleanValue()) {
            this.c.a(str);
        }
    }

    public void c() {
        if (!b.b.booleanValue()) {
            this.c.c();
        }
    }

    public void onCreate() {
        super.onCreate();
        if (a == null) {
            a = this;
            g();
        }
        if (!b.a.booleanValue()) {
            this.b = b.a();
            this.b.a("UA-12579430-1", 60, this);
        }
        if (!b.b.booleanValue()) {
            this.c = new f(getApplicationContext(), "925e0f57a4226b7dc294867-d6ac65ac-3e4b-11df-165d-004a79fcdfe1");
            this.c.a();
            this.c.c();
        }
    }

    public void onLowMemory() {
        super.onLowMemory();
    }

    public void onTerminate() {
        super.onTerminate();
    }
}
