package com.asai24.golf.d;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class m {
    public static double a(double d, int i) {
        return new BigDecimal(String.valueOf(d)).setScale(i, RoundingMode.HALF_UP).doubleValue();
    }

    public static long a(double d) {
        return new BigDecimal(String.valueOf(d)).setScale(0, RoundingMode.HALF_UP).longValue();
    }
}
