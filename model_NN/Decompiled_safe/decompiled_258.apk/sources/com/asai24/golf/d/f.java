package com.asai24.golf.d;

import android.content.Context;
import java.io.File;
import java.io.IOException;

public class f {
    public static void a(Context context, String str, String str2) {
        a(context, str, str2, 1);
    }

    /* JADX WARNING: Removed duplicated region for block: B:61:0x010a A[SYNTHETIC, Splitter:B:61:0x010a] */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x010f A[SYNTHETIC, Splitter:B:64:0x010f] */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x0114 A[SYNTHETIC, Splitter:B:67:0x0114] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void a(android.content.Context r12, java.lang.String r13, java.lang.String r14, int r15) {
        /*
            r9 = 0
            java.io.File r0 = new java.io.File
            r0.<init>(r13)
            java.io.File r1 = new java.io.File
            r1.<init>(r14)
            r2 = 0
            r3 = 0
            r4 = 0
            java.lang.String r5 = r1.getParent()
            r6 = 8
            byte[] r6 = new byte[r6]     // Catch:{ Exception -> 0x01bc, all -> 0x019f }
            r6 = {57, 78, -94, 24, 121, 34, 16, -2} // fill-array     // Catch:{ Exception -> 0x01bc, all -> 0x019f }
            r7 = 10
            javax.crypto.spec.PBEParameterSpec r8 = new javax.crypto.spec.PBEParameterSpec     // Catch:{ Exception -> 0x01bc, all -> 0x019f }
            r8.<init>(r6, r7)     // Catch:{ Exception -> 0x01bc, all -> 0x019f }
            javax.crypto.spec.PBEKeySpec r6 = new javax.crypto.spec.PBEKeySpec     // Catch:{ Exception -> 0x01bc, all -> 0x019f }
            java.lang.String r7 = "my password"
            char[] r7 = r7.toCharArray()     // Catch:{ Exception -> 0x01bc, all -> 0x019f }
            r6.<init>(r7)     // Catch:{ Exception -> 0x01bc, all -> 0x019f }
            java.lang.String r7 = "PBEWithMD5AndDES"
            javax.crypto.SecretKeyFactory r7 = javax.crypto.SecretKeyFactory.getInstance(r7)     // Catch:{ Exception -> 0x01bc, all -> 0x019f }
            javax.crypto.SecretKey r6 = r7.generateSecret(r6)     // Catch:{ Exception -> 0x01bc, all -> 0x019f }
            java.lang.String r7 = "PBEWithMD5AndDES"
            javax.crypto.Cipher r7 = javax.crypto.Cipher.getInstance(r7)     // Catch:{ Exception -> 0x01bc, all -> 0x019f }
            r7.init(r15, r6, r8)     // Catch:{ Exception -> 0x01bc, all -> 0x019f }
            if (r5 == 0) goto L_0x004b
            java.lang.String r6 = ""
            boolean r6 = r5.equals(r6)     // Catch:{ Exception -> 0x01bc, all -> 0x019f }
            if (r6 != 0) goto L_0x004b
            a(r5)     // Catch:{ IOException -> 0x007e }
        L_0x004b:
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ Exception -> 0x01bc, all -> 0x019f }
            r2.<init>(r0)     // Catch:{ Exception -> 0x01bc, all -> 0x019f }
            javax.crypto.CipherInputStream r0 = new javax.crypto.CipherInputStream     // Catch:{ Exception -> 0x01c2, all -> 0x01a5 }
            r0.<init>(r2, r7)     // Catch:{ Exception -> 0x01c2, all -> 0x01a5 }
            boolean r3 = r1.exists()     // Catch:{ Exception -> 0x01c8, all -> 0x01ab }
            if (r3 != 0) goto L_0x005e
            r1.createNewFile()     // Catch:{ Exception -> 0x01c8, all -> 0x01ab }
        L_0x005e:
            java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x01c8, all -> 0x01ab }
            r3.<init>(r1)     // Catch:{ Exception -> 0x01c8, all -> 0x01ab }
            r1 = 4096(0x1000, float:5.74E-42)
            byte[] r1 = new byte[r1]     // Catch:{ Exception -> 0x00eb, all -> 0x01b3 }
        L_0x0067:
            r4 = -1
            int r5 = r0.read(r1)     // Catch:{ Exception -> 0x00eb, all -> 0x01b3 }
            if (r4 != r5) goto L_0x00e5
            if (r2 == 0) goto L_0x0073
            r2.close()     // Catch:{ IOException -> 0x015a }
        L_0x0073:
            if (r3 == 0) goto L_0x0078
            r3.close()     // Catch:{ IOException -> 0x0171 }
        L_0x0078:
            if (r0 == 0) goto L_0x007d
            r0.close()     // Catch:{ IOException -> 0x0188 }
        L_0x007d:
            return
        L_0x007e:
            r0 = move-exception
            java.lang.String r1 = "FileUtil"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01bc, all -> 0x019f }
            java.lang.String r6 = "Error: "
            r5.<init>(r6)     // Catch:{ Exception -> 0x01bc, all -> 0x019f }
            java.lang.StringBuilder r0 = r5.append(r0)     // Catch:{ Exception -> 0x01bc, all -> 0x019f }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x01bc, all -> 0x019f }
            android.util.Log.e(r1, r0)     // Catch:{ Exception -> 0x01bc, all -> 0x019f }
            if (r9 == 0) goto L_0x0098
            r2.close()     // Catch:{ IOException -> 0x00b9 }
        L_0x0098:
            if (r9 == 0) goto L_0x009d
            r3.close()     // Catch:{ IOException -> 0x00cf }
        L_0x009d:
            if (r9 == 0) goto L_0x007d
            r4.close()     // Catch:{ IOException -> 0x00a3 }
            goto L_0x007d
        L_0x00a3:
            r0 = move-exception
            java.lang.String r1 = "FileUtil"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "Error: "
            r2.<init>(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            android.util.Log.e(r1, r0)
            goto L_0x007d
        L_0x00b9:
            r0 = move-exception
            java.lang.String r1 = "FileUtil"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r5 = "Error: "
            r2.<init>(r5)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            android.util.Log.e(r1, r0)
            goto L_0x0098
        L_0x00cf:
            r0 = move-exception
            java.lang.String r1 = "FileUtil"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "Error: "
            r2.<init>(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            android.util.Log.e(r1, r0)
            goto L_0x009d
        L_0x00e5:
            r4 = 0
            r3.write(r1, r4, r5)     // Catch:{ Exception -> 0x00eb, all -> 0x01b3 }
            goto L_0x0067
        L_0x00eb:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
            r11 = r3
            r3 = r2
            r2 = r11
        L_0x00f2:
            java.lang.String r4 = "FileUtil"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x0107 }
            java.lang.String r6 = "Error: "
            r5.<init>(r6)     // Catch:{ all -> 0x0107 }
            java.lang.StringBuilder r5 = r5.append(r0)     // Catch:{ all -> 0x0107 }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x0107 }
            android.util.Log.e(r4, r5)     // Catch:{ all -> 0x0107 }
            throw r0     // Catch:{ all -> 0x0107 }
        L_0x0107:
            r0 = move-exception
        L_0x0108:
            if (r3 == 0) goto L_0x010d
            r3.close()     // Catch:{ IOException -> 0x0118 }
        L_0x010d:
            if (r2 == 0) goto L_0x0112
            r2.close()     // Catch:{ IOException -> 0x012e }
        L_0x0112:
            if (r1 == 0) goto L_0x0117
            r1.close()     // Catch:{ IOException -> 0x0144 }
        L_0x0117:
            throw r0
        L_0x0118:
            r3 = move-exception
            java.lang.String r4 = "FileUtil"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            java.lang.String r6 = "Error: "
            r5.<init>(r6)
            java.lang.StringBuilder r3 = r5.append(r3)
            java.lang.String r3 = r3.toString()
            android.util.Log.e(r4, r3)
            goto L_0x010d
        L_0x012e:
            r2 = move-exception
            java.lang.String r3 = "FileUtil"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r5 = "Error: "
            r4.<init>(r5)
            java.lang.StringBuilder r2 = r4.append(r2)
            java.lang.String r2 = r2.toString()
            android.util.Log.e(r3, r2)
            goto L_0x0112
        L_0x0144:
            r1 = move-exception
            java.lang.String r2 = "FileUtil"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "Error: "
            r3.<init>(r4)
            java.lang.StringBuilder r1 = r3.append(r1)
            java.lang.String r1 = r1.toString()
            android.util.Log.e(r2, r1)
            goto L_0x0117
        L_0x015a:
            r1 = move-exception
            java.lang.String r2 = "FileUtil"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r5 = "Error: "
            r4.<init>(r5)
            java.lang.StringBuilder r1 = r4.append(r1)
            java.lang.String r1 = r1.toString()
            android.util.Log.e(r2, r1)
            goto L_0x0073
        L_0x0171:
            r1 = move-exception
            java.lang.String r2 = "FileUtil"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "Error: "
            r3.<init>(r4)
            java.lang.StringBuilder r1 = r3.append(r1)
            java.lang.String r1 = r1.toString()
            android.util.Log.e(r2, r1)
            goto L_0x0078
        L_0x0188:
            r0 = move-exception
            java.lang.String r1 = "FileUtil"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "Error: "
            r2.<init>(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            android.util.Log.e(r1, r0)
            goto L_0x007d
        L_0x019f:
            r0 = move-exception
            r1 = r9
            r2 = r9
            r3 = r9
            goto L_0x0108
        L_0x01a5:
            r0 = move-exception
            r1 = r9
            r3 = r2
            r2 = r9
            goto L_0x0108
        L_0x01ab:
            r1 = move-exception
            r3 = r2
            r2 = r9
            r10 = r0
            r0 = r1
            r1 = r10
            goto L_0x0108
        L_0x01b3:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
            r11 = r3
            r3 = r2
            r2 = r11
            goto L_0x0108
        L_0x01bc:
            r0 = move-exception
            r1 = r9
            r2 = r9
            r3 = r9
            goto L_0x00f2
        L_0x01c2:
            r0 = move-exception
            r1 = r9
            r3 = r2
            r2 = r9
            goto L_0x00f2
        L_0x01c8:
            r1 = move-exception
            r3 = r2
            r2 = r9
            r10 = r0
            r0 = r1
            r1 = r10
            goto L_0x00f2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.asai24.golf.d.f.a(android.content.Context, java.lang.String, java.lang.String, int):void");
    }

    public static boolean a(File file) {
        if (!file.exists()) {
            if (file.mkdirs()) {
                return true;
            }
            throw new IOException("File.mkdirs() failed.");
        } else if (file.isDirectory()) {
            return false;
        } else {
            throw new IOException("Cannot create path. " + file.toString() + " already exists and is not a directory.");
        }
    }

    public static boolean a(String str) {
        return a(new File(str));
    }
}
