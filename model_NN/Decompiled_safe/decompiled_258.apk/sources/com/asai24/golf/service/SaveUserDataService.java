package com.asai24.golf.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import com.asai24.golf.c.j;
import java.util.Timer;

public class SaveUserDataService extends Service {
    private Timer a = new Timer();

    private void a() {
        this.a.scheduleAtFixedRate(new a(this, new j(this)), 0, 86400000);
        Log.i(getClass().getSimpleName(), "Timer started!!!");
    }

    private void b() {
        if (this.a != null) {
            this.a.cancel();
        }
        Log.i(getClass().getSimpleName(), "Timer stopped!!!");
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
        a();
    }

    public void onDestroy() {
        super.onDestroy();
        b();
    }
}
