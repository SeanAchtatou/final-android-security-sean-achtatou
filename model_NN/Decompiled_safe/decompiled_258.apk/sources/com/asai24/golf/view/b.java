package com.asai24.golf.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.location.Location;
import com.asai24.golf.R;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;
import com.google.android.maps.MyLocationOverlay;
import com.google.android.maps.Projection;

public class b extends MyLocationOverlay {
    private boolean a = false;
    private Paint b;
    private Point c;
    private Point d;
    private Drawable e;
    private int f;
    private int g;

    public b(Context context, MapView mapView) {
        super(context, mapView);
    }

    /* access modifiers changed from: protected */
    public void drawMyLocation(Canvas canvas, MapView mapView, Location location, GeoPoint geoPoint, long j) {
        if (!this.a) {
            try {
                b.super.drawMyLocation(canvas, mapView, location, geoPoint, j);
            } catch (Exception e2) {
                this.a = true;
            }
        }
        if (this.a) {
            if (this.e == null) {
                this.b = new Paint();
                this.b.setAntiAlias(true);
                this.b.setStrokeWidth(2.0f);
                this.e = mapView.getContext().getResources().getDrawable(R.drawable.mylocation);
                this.f = this.e.getIntrinsicWidth();
                this.g = this.e.getIntrinsicHeight();
                this.c = new Point();
                this.d = new Point();
            }
            Projection projection = mapView.getProjection();
            double latitude = location.getLatitude();
            double longitude = location.getLongitude();
            float accuracy = location.getAccuracy();
            float[] fArr = new float[1];
            Location.distanceBetween(latitude, longitude, latitude, longitude + 1.0d, fArr);
            projection.toPixels(new GeoPoint((int) (latitude * 1000000.0d), (int) ((longitude - ((double) (accuracy / fArr[0]))) * 1000000.0d)), this.d);
            projection.toPixels(geoPoint, this.c);
            int i = this.c.x - this.d.x;
            this.b.setColor(-10066177);
            this.b.setStyle(Paint.Style.STROKE);
            canvas.drawCircle((float) this.c.x, (float) this.c.y, (float) i, this.b);
            this.b.setColor(409364223);
            this.b.setStyle(Paint.Style.FILL);
            canvas.drawCircle((float) this.c.x, (float) this.c.y, (float) i, this.b);
            this.e.setBounds(this.c.x - (this.f / 2), this.c.y - (this.g / 2), this.c.x + (this.f / 2), this.c.y + (this.g / 2));
            this.e.draw(canvas);
        }
    }
}
