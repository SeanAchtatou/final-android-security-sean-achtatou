package com.asai24.golf.view;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.preference.DialogPreference;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.asai24.golf.R;
import java.util.ArrayList;
import java.util.Iterator;

public class ClubSettingPreference extends DialogPreference implements DialogInterface.OnClickListener, View.OnClickListener {
    private ArrayList a;
    private Resources b;

    public ClubSettingPreference(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    private String a(Button button) {
        return button.length() < 2 ? button.getTag().toString() : ((Object[]) button.getTag())[0].toString();
    }

    private void a(View view, Context context) {
        this.b = context.getResources();
        ((Button) view.findViewById(R.id.club_pt_btn)).setVisibility(4);
        ((Button) view.findViewById(R.id.club_cancel_btn)).setVisibility(4);
        ((RelativeLayout) view.findViewById(R.id.club_gps_status_layout)).removeAllViewsInLayout();
        TextView textView = new TextView(context);
        textView.setText(context.getString(R.string.club_setting_text));
        textView.setTextSize(13.0f);
        textView.setTypeface(Typeface.DEFAULT_BOLD);
        textView.setTextColor(-1);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.setMargins(5, 0, 0, 0);
        ((RelativeLayout) view.findViewById(R.id.club_gps_status_layout)).addView(textView, layoutParams);
        this.a = new ArrayList();
        this.a.add((Button) view.findViewById(R.id.club_dr_btn));
        this.a.add((Button) view.findViewById(R.id.club_3w_btn));
        this.a.add((Button) view.findViewById(R.id.club_4w_btn));
        this.a.add((Button) view.findViewById(R.id.club_5w_btn));
        this.a.add((Button) view.findViewById(R.id.club_7w_btn));
        this.a.add((Button) view.findViewById(R.id.club_hy_btn));
        this.a.add((Button) view.findViewById(R.id.club_2i_btn));
        this.a.add((Button) view.findViewById(R.id.club_3i_btn));
        this.a.add((Button) view.findViewById(R.id.club_4i_btn));
        this.a.add((Button) view.findViewById(R.id.club_5i_btn));
        this.a.add((Button) view.findViewById(R.id.club_6i_btn));
        this.a.add((Button) view.findViewById(R.id.club_7i_btn));
        this.a.add((Button) view.findViewById(R.id.club_8i_btn));
        this.a.add((Button) view.findViewById(R.id.club_9i_btn));
        this.a.add((Button) view.findViewById(R.id.club_pw_btn));
        this.a.add((Button) view.findViewById(R.id.club_gw_btn));
        this.a.add((Button) view.findViewById(R.id.club_sw_btn));
        this.a.add((Button) view.findViewById(R.id.club_lw_btn));
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Iterator it = this.a.iterator();
        while (it.hasNext()) {
            Button button = (Button) it.next();
            button.setOnClickListener(this);
            if (!defaultSharedPreferences.getBoolean(button.getTag().toString(), true)) {
                button.setBackgroundResource(R.drawable.square_btn2_disabled);
                button.setTextColor(-12303292);
                button.setTag(new Object[]{button.getTag(), false});
            } else {
                button.setTag(new Object[]{button.getTag(), true});
            }
        }
    }

    private Boolean b(Button button) {
        if (button.length() > 1) {
            return (Boolean) ((Object[]) button.getTag())[1];
        }
        return null;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        switch (i) {
            case -2:
            default:
                return;
            case -1:
                SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(getContext()).edit();
                Iterator it = this.a.iterator();
                while (it.hasNext()) {
                    Button button = (Button) it.next();
                    edit.putBoolean(a(button), b(button).booleanValue());
                    edit.commit();
                }
                return;
        }
    }

    public void onClick(View view) {
        Button button = (Button) view;
        if (!Boolean.valueOf(!b(button).booleanValue()).booleanValue()) {
            button.setBackgroundResource(R.drawable.square_btn2_disabled);
            button.setTextColor(-12303292);
            button.setTag(new Object[]{a(button), false});
            return;
        }
        button.setBackgroundResource(R.drawable.square_btn2_selector);
        button.setTextColor(this.b.getColor(R.color.dark_green));
        button.setTag(new Object[]{a(button), true});
    }

    /* access modifiers changed from: protected */
    public View onCreateDialogView() {
        View inflate = LayoutInflater.from(getContext()).inflate(getDialogLayoutResource(), (ViewGroup) null);
        a(inflate, getContext());
        return inflate;
    }
}
