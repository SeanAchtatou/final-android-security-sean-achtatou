package com.asai24.golf.view;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.preference.EditTextPreference;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import com.asai24.golf.R;
import com.asai24.golf.d.g;

public class PasswordTextPreference extends EditTextPreference implements DialogInterface.OnClickListener {
    private Resources a;
    private String b;
    private String c;
    private String d;
    private EditText e;
    private Boolean f;
    private String g;

    public PasswordTextPreference(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.a = context.getResources();
        a();
    }

    private String a(String str) {
        if (str == null || str.equals("")) {
            return this.d;
        }
        char[] cArr = new char[str.length()];
        for (int i = 0; i < cArr.length; i++) {
            cArr[i] = '*';
        }
        return new String(cArr);
    }

    private void a() {
        this.b = getKey();
        this.e = getEditText();
        this.g = "";
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        if (this.b.equals(this.a.getString(R.string.key_oob_password))) {
            this.c = this.a.getString(R.string.key_oob_encrypted);
            this.d = this.a.getString(R.string.setting_summary_password);
        }
        this.f = Boolean.valueOf(defaultSharedPreferences.getBoolean(this.c, false));
        this.g = defaultSharedPreferences.getString(this.b, "");
        if (this.g != null && !this.g.equals("") && this.f.booleanValue()) {
            try {
                this.g = g.b(this.g);
            } catch (Exception e2) {
                Log.e("PasswordTextPreference", e2.getMessage());
            }
        }
        setSummary(a(this.g));
    }

    /* access modifiers changed from: protected */
    public void onBindDialogView(View view) {
        super.onBindDialogView(view);
        if (this.g == null || this.g.equals("")) {
            this.e.setText("");
        } else {
            this.e.setText(this.g);
        }
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        switch (i) {
            case -2:
            default:
                return;
            case -1:
                String editable = this.e.getText().toString();
                if (this.g == null || !this.g.equals(editable)) {
                    this.g = editable;
                    if (editable == null || editable.equals("")) {
                        SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(getContext()).edit();
                        edit.putString(this.b, "");
                        edit.putBoolean(this.c, false);
                        edit.commit();
                    } else {
                        try {
                            SharedPreferences.Editor edit2 = PreferenceManager.getDefaultSharedPreferences(getContext()).edit();
                            edit2.putString(this.b, g.a(editable));
                            edit2.putBoolean(this.c, true);
                            edit2.commit();
                        } catch (Exception e2) {
                            Log.e("PasswordTextPreference", e2.getMessage());
                            Toast.makeText(getContext(), "データの保存に失敗しました・・・", 0).show();
                        }
                    }
                    setSummary(a(this.g));
                    return;
                }
                return;
        }
    }
}
