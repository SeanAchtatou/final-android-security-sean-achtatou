package com.asai24.golf.view;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.RectF;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class a extends Overlay {
    private Paint a;
    private Paint b;
    private Paint c;
    private PointF d;
    private ArrayList e = new ArrayList();
    private ArrayList f = new ArrayList();
    private float g;
    private boolean h = false;

    public a() {
        a();
    }

    private void a() {
        this.g = 0.0f;
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        this.a = new Paint(paint);
        this.a.setColor(-16776961);
        this.a.setAlpha(200);
        this.a.setStyle(Paint.Style.STROKE);
        this.b = new Paint(paint);
        this.b.setColor(-16776961);
        this.b.setTextSize(12.0f);
        this.c = new Paint(paint);
        this.c.setColor(-1);
        this.c.setAlpha(255);
    }

    public void a(float f2) {
        this.g = f2;
    }

    public void a(PointF pointF, ArrayList arrayList, ArrayList arrayList2) {
        this.d = pointF;
        this.e = arrayList;
        this.f = arrayList2;
    }

    public void a(boolean z) {
        this.h = z;
    }

    public void draw(Canvas canvas, MapView mapView, boolean z) {
        a.super.draw(canvas, mapView, z);
        if (!z && this.h) {
            if (this.d != null) {
                canvas.rotate(this.g, this.d.x, this.d.y);
            }
            for (int i = 0; i < this.e.size(); i++) {
                PointF pointF = (PointF) this.e.get(i);
                double sqrt = Math.sqrt((double) (((pointF.x - this.d.x) * (pointF.x - this.d.x)) + ((pointF.y - this.d.y) * (pointF.y - this.d.y))));
                canvas.drawCircle(this.d.x, this.d.y, (float) sqrt, this.a);
                if (i % 2 == 0) {
                    String format = new DecimalFormat("#0.#").format(this.f.get(i));
                    float measureText = this.b.measureText(format);
                    Paint.FontMetrics fontMetrics = this.b.getFontMetrics();
                    float f2 = fontMetrics.descent - fontMetrics.ascent;
                    float f3 = this.d.x;
                    float f4 = (float) (((double) this.d.y) - sqrt);
                    canvas.drawRoundRect(new RectF((f3 - (measureText / 2.0f)) - 2.0f, f4 - f2, (measureText / 2.0f) + f3 + 2.0f, f4), 5.0f, 5.0f, this.c);
                    canvas.drawText(format, f3 - (measureText / 2.0f), f4 - 2.0f, this.b);
                    float f5 = (float) (((double) this.d.y) + sqrt);
                    canvas.drawRoundRect(new RectF((f3 - (measureText / 2.0f)) - 2.0f, f5, (measureText / 2.0f) + f3 + 2.0f, f5 + f2), 5.0f, 5.0f, this.c);
                    canvas.drawText(format, f3 - (measureText / 2.0f), (f5 + f2) - 2.0f, this.b);
                    float f6 = (float) (((double) this.d.x) - sqrt);
                    float f7 = this.d.y;
                    canvas.drawRoundRect(new RectF((f6 - (measureText / 2.0f)) - 2.0f, (f7 - f2) + 2.0f, (measureText / 2.0f) + f6 + 2.0f, 2.0f + f7), 5.0f, 5.0f, this.c);
                    canvas.drawText(format, f6 - (measureText / 2.0f), f7, this.b);
                    float f8 = (float) (sqrt + ((double) this.d.x));
                    canvas.drawRoundRect(new RectF((f8 - (measureText / 2.0f)) - 2.0f, (f7 - f2) + 2.0f, (measureText / 2.0f) + f8 + 2.0f, 2.0f + f7), 5.0f, 5.0f, this.c);
                    canvas.drawText(format, f8 - (measureText / 2.0f), f7, this.b);
                }
            }
            if (this.d != null) {
                canvas.rotate(-this.g, this.d.x, this.d.y);
            }
        }
    }
}
