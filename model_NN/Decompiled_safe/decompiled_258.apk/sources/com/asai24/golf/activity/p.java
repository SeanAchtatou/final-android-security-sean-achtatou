package com.asai24.golf.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import com.asai24.golf.R;

class p implements DialogInterface.OnClickListener {
    final /* synthetic */ PlayHistories a;
    private final /* synthetic */ Context b;

    p(PlayHistories playHistories, Context context) {
        this.a = playHistories;
        this.b = context;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        Intent intent = new Intent(this.b, GolfSettingsDetail.class);
        intent.putExtra(this.a.getString(R.string.setting_detail_mode), this.a.getString(R.string.setting_oobgolf_key));
        this.a.startActivity(intent);
    }
}
