package com.asai24.golf.activity;

import android.content.Context;
import android.database.Cursor;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import com.asai24.golf.R;
import com.asai24.golf.a.b;
import com.asai24.golf.a.s;
import com.asai24.golf.f.a;

public class al extends CursorAdapter {
    private View.OnClickListener a;
    private boolean b;

    public al(Context context, s sVar, View.OnClickListener onClickListener) {
        super(context, sVar);
        this.a = onClickListener;
        if (PreferenceManager.getDefaultSharedPreferences(context).getString(context.getString(R.string.key_owner_measure_unit), "yard").equals("yard")) {
            this.b = true;
        } else {
            this.b = false;
        }
    }

    public boolean areAllItemsEnabled() {
        return false;
    }

    public void bindView(View view, Context context, Cursor cursor) {
        s sVar = (s) cursor;
        long b2 = sVar.b();
        String sb = new StringBuilder().append(sVar.d()).toString();
        String sb2 = new StringBuilder().append(sVar.e()).toString();
        String str = sVar.k();
        ((TextView) view.findViewById(R.id.hole_number)).setText(sb);
        TextView textView = (TextView) view.findViewById(R.id.hole_yard);
        TextView textView2 = (TextView) view.findViewById(R.id.hole_yard_label);
        int l = sVar.l();
        if (this.b) {
            textView.setText(new StringBuilder().append(l).toString());
            textView2.setText(" yards");
        } else {
            textView.setText(new StringBuilder().append(a.b(l)).toString());
            textView2.setText(" meters");
        }
        ((TextView) view.findViewById(R.id.hole_par)).setText(sb2);
        ((TextView) view.findViewById(R.id.hole_handicap)).setText(str);
        ImageButton imageButton = (ImageButton) view.findViewById(R.id.hole_back);
        imageButton.setOnClickListener(this.a);
        ImageButton imageButton2 = (ImageButton) view.findViewById(R.id.hole_next);
        imageButton2.setOnClickListener(this.a);
        b a2 = b.a(context);
        s d = a2.d(b2);
        if (d.getCount() == 0) {
            imageButton.setBackgroundResource(R.drawable.hole_back_btn_disabled);
        } else {
            imageButton.setBackgroundResource(R.drawable.hole_back_btn_selector);
        }
        d.close();
        s c = a2.c(b2);
        if (c.getCount() == 0) {
            imageButton2.setBackgroundResource(R.drawable.hole_next_btn_disabled);
        } else {
            imageButton2.setBackgroundResource(R.drawable.hole_next_btn_selector);
        }
        c.close();
    }

    public boolean isEnabled(int i) {
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        return LayoutInflater.from(context).inflate((int) R.layout.hole_info_item, viewGroup, false);
    }
}
