package com.asai24.golf.activity;

import com.asai24.golf.R;

class df implements Runnable {
    final /* synthetic */ HoleMap a;

    df(HoleMap holeMap) {
        this.a = holeMap;
    }

    public void run() {
        if (!this.a.t) {
            this.a.s.dismiss();
            this.a.e.removeUpdates(this.a.f);
            this.a.a(this.a.d.getString(R.string.location_undetermined_message));
        }
    }
}
