package com.asai24.golf.activity;

import android.content.DialogInterface;
import com.asai24.golf.R;

class fb implements DialogInterface.OnClickListener {
    final /* synthetic */ TotalAnalysis a;

    fb(TotalAnalysis totalAnalysis) {
        this.a = totalAnalysis;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        this.a.m = this.a.n;
        switch (this.a.m) {
            case 0:
                this.a.e.setText(this.a.getString(R.string.three_month));
                break;
            case 1:
                this.a.e.setText(this.a.getString(R.string.six_month));
                break;
            case 2:
                this.a.e.setText(this.a.getString(R.string.all));
                break;
        }
        this.a.f();
    }
}
