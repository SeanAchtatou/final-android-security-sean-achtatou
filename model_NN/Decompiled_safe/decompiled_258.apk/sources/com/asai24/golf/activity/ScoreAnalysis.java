package com.asai24.golf.activity;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.asai24.golf.R;
import com.asai24.golf.a.b;
import com.asai24.golf.a.d;
import com.asai24.golf.a.g;
import com.asai24.golf.a.i;
import com.asai24.golf.a.y;
import com.asai24.golf.d.m;
import com.asai24.golf.e.a;
import java.util.ArrayList;

public class ScoreAnalysis extends GolfActivity implements View.OnClickListener {
    private b b;
    private Resources c;
    private long[] d;
    private long e;
    private long f;
    private ArrayList g;
    private ArrayList h;
    private ArrayList i;
    private ArrayList j;
    private ArrayList k;
    private Button l;
    private Button m;
    private Button n;

    private int a(long j2) {
        g m2 = this.b.m(j2);
        a aVar = new a(this);
        int i2 = 0;
        for (int i3 = 0; i3 < m2.getCount(); i3++) {
            m2.moveToPosition(i3);
            if (aVar.a(m2.e()).equals("pt")) {
                i2++;
            }
        }
        m2.close();
        return i2;
    }

    private void d() {
        this.g = new ArrayList();
        this.h = new ArrayList();
        this.i = new ArrayList();
        this.j = new ArrayList();
        this.k = new ArrayList();
        for (int i2 = 1; i2 <= 6; i2++) {
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            ArrayList arrayList3 = new ArrayList();
            ArrayList arrayList4 = new ArrayList();
            for (int i3 = 1; i3 <= 4; i3++) {
                if (i2 == 1) {
                    this.g.add((TextView) findViewById(this.c.getIdentifier("score_analysis_label_player" + i3, "id", getPackageName())));
                }
                if (i2 <= 4) {
                    arrayList.add((TextView) findViewById(this.c.getIdentifier("score_analysis_stroke" + i2 + "_player" + i3, "id", getPackageName())));
                }
                if (i2 <= 6) {
                    arrayList2.add((TextView) findViewById(this.c.getIdentifier("score_analysis_score" + i2 + "_player" + i3, "id", getPackageName())));
                }
                if (i2 <= 2) {
                    arrayList3.add((TextView) findViewById(this.c.getIdentifier("score_analysis_putt" + i2 + "_player" + i3, "id", getPackageName())));
                }
                if (i2 <= 2) {
                    arrayList4.add((TextView) findViewById(this.c.getIdentifier("score_analysis_gir" + i2 + "_player" + i3, "id", getPackageName())));
                }
            }
            if (i2 <= 4) {
                this.h.add(arrayList);
            }
            if (i2 <= 6) {
                this.i.add(arrayList2);
            }
            if (i2 <= 2) {
                this.j.add(arrayList3);
            }
            if (i2 <= 2) {
                this.k.add(arrayList4);
            }
        }
        this.l = (Button) findViewById(R.id.putt_analysis_graph);
        this.m = (Button) findViewById(R.id.score_analysis_graph);
        this.n = (Button) findViewById(R.id.stroke_analysis_graph);
        this.l.setOnClickListener(this);
        this.m.setOnClickListener(this);
        this.n.setOnClickListener(this);
    }

    private void e() {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < this.d.length) {
                d b2 = this.b.b(this.d[i3]);
                String c2 = b2.getCount() == 0 ? "  " : b2.c();
                b2.close();
                ((TextView) this.g.get(i3)).setText(c2);
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    private void f() {
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        int i11;
        int i12 = 0;
        while (true) {
            int i13 = i12;
            if (i13 < this.d.length) {
                i b2 = this.b.b(this.e, this.d[i13]);
                ((TextView) ((ArrayList) this.h.get(0)).get(i13)).setText(new StringBuilder().append(b2.b()).toString());
                b2.close();
                int i14 = 0;
                ArrayList arrayList = new ArrayList();
                for (int i15 = 0; i15 < 6; i15++) {
                    arrayList.add(null);
                }
                y c2 = this.b.c(this.e, this.d[i13], this.f);
                c2.moveToFirst();
                int i16 = 0;
                int i17 = 0;
                int i18 = 0;
                int i19 = 0;
                int i20 = 0;
                int i21 = 0;
                int i22 = 0;
                int i23 = 0;
                int i24 = 0;
                while (!c2.isAfterLast()) {
                    int b3 = c2.b();
                    if (b3 != 0) {
                        int i25 = i21 + 1;
                        int d2 = c2.d();
                        int a = a(c2.e());
                        if (d2 == 3) {
                            i14 += b3;
                            i24++;
                        } else if (d2 == 4) {
                            i22 += b3;
                            i23++;
                        } else {
                            i19 += b3;
                            i20++;
                        }
                        if (b3 <= d2 - 2) {
                            arrayList.set(0, Integer.valueOf(((Integer) arrayList.get(0)).intValue() + 1));
                        } else if (b3 == d2 - 1) {
                            arrayList.set(1, Integer.valueOf(((Integer) arrayList.get(1)).intValue() + 1));
                        } else if (b3 == d2) {
                            arrayList.set(2, Integer.valueOf(((Integer) arrayList.get(2)).intValue() + 1));
                        } else if (b3 == d2 + 1) {
                            arrayList.set(3, Integer.valueOf(((Integer) arrayList.get(3)).intValue() + 1));
                        } else if (b3 == d2 + 2) {
                            arrayList.set(4, Integer.valueOf(((Integer) arrayList.get(4)).intValue() + 1));
                        } else if (b3 >= d2 + 3) {
                            arrayList.set(5, Integer.valueOf(((Integer) arrayList.get(5)).intValue() + 1));
                        }
                        int i26 = i18 + a;
                        if (b3 - a <= d2 - 2) {
                            i5 = i19;
                            i6 = i20;
                            i7 = i22;
                            i8 = i23;
                            i9 = i14;
                            i10 = i24;
                            i11 = i25;
                            int i27 = i17 + 1;
                            i4 = i26;
                            i2 = i16;
                            i3 = i27;
                        } else {
                            if (b3 - a == d2 - 1) {
                                i5 = i19;
                                i6 = i20;
                                i7 = i22;
                                i8 = i23;
                                i9 = i14;
                                i10 = i24;
                                i11 = i25;
                                int i28 = i17;
                                i4 = i26;
                                i2 = i16 + 1;
                                i3 = i28;
                            } else {
                                i5 = i19;
                                i6 = i20;
                                i7 = i22;
                                i8 = i23;
                                i9 = i14;
                                i10 = i24;
                                i11 = i25;
                                int i29 = i17;
                                i4 = i26;
                                i2 = i16;
                                i3 = i29;
                            }
                        }
                    } else {
                        i2 = i16;
                        i3 = i17;
                        i4 = i18;
                        i5 = i19;
                        i6 = i20;
                        i7 = i22;
                        i8 = i23;
                        i9 = i14;
                        i10 = i24;
                        i11 = i21;
                    }
                    c2.moveToNext();
                    i21 = i11;
                    i24 = i10;
                    i14 = i9;
                    i23 = i8;
                    i22 = i7;
                    i20 = i6;
                    i19 = i5;
                    i18 = i4;
                    i17 = i3;
                    i16 = i2;
                }
                c2.close();
                if (i21 > 0) {
                    if (i24 != 0) {
                        ((TextView) ((ArrayList) this.h.get(1)).get(i13)).setText(new StringBuilder().append(m.a(((double) i14) / ((double) i24), 3)).toString());
                    }
                    if (i23 != 0) {
                        ((TextView) ((ArrayList) this.h.get(2)).get(i13)).setText(new StringBuilder().append(m.a(((double) i22) / ((double) i23), 3)).toString());
                    }
                    if (i20 != 0) {
                        ((TextView) ((ArrayList) this.h.get(3)).get(i13)).setText(new StringBuilder().append(m.a(((double) i19) / ((double) i20), 3)).toString());
                    }
                    ((TextView) ((ArrayList) this.i.get(0)).get(i13)).setText(String.valueOf(m.a((((double) ((Integer) arrayList.get(0)).intValue()) / ((double) i21)) * 100.0d)) + "%");
                    ((TextView) ((ArrayList) this.i.get(1)).get(i13)).setText(String.valueOf(m.a((((double) ((Integer) arrayList.get(1)).intValue()) / ((double) i21)) * 100.0d)) + "%");
                    ((TextView) ((ArrayList) this.i.get(2)).get(i13)).setText(String.valueOf(m.a((((double) ((Integer) arrayList.get(2)).intValue()) / ((double) i21)) * 100.0d)) + "%");
                    ((TextView) ((ArrayList) this.i.get(3)).get(i13)).setText(String.valueOf(m.a((((double) ((Integer) arrayList.get(3)).intValue()) / ((double) i21)) * 100.0d)) + "%");
                    ((TextView) ((ArrayList) this.i.get(4)).get(i13)).setText(String.valueOf(m.a((((double) ((Integer) arrayList.get(4)).intValue()) / ((double) i21)) * 100.0d)) + "%");
                    ((TextView) ((ArrayList) this.i.get(5)).get(i13)).setText(String.valueOf(m.a((((double) ((Integer) arrayList.get(5)).intValue()) / ((double) i21)) * 100.0d)) + "%");
                    ((TextView) ((ArrayList) this.j.get(0)).get(i13)).setText(new StringBuilder().append(i18).toString());
                    ((TextView) ((ArrayList) this.j.get(1)).get(i13)).setText(new StringBuilder().append(m.a(((double) i18) / ((double) i21), 3)).toString());
                    ((TextView) ((ArrayList) this.k.get(0)).get(i13)).setText(String.valueOf(m.a((((double) i17) / ((double) i21)) * 100.0d)) + "%");
                    ((TextView) ((ArrayList) this.k.get(1)).get(i13)).setText(String.valueOf(m.a((((double) i16) / ((double) i21)) * 100.0d)) + "%");
                }
                i12 = i13 + 1;
            } else {
                return;
            }
        }
    }

    private ArrayList g() {
        ArrayList arrayList = new ArrayList();
        for (int i2 = 0; i2 < 4; i2++) {
            arrayList.add(String.valueOf(new StringBuilder().append((Object) ((TextView) ((ArrayList) this.h.get(0)).get(i2)).getText()).toString().replace("-", "0")) + "," + new StringBuilder().append((Object) ((TextView) ((ArrayList) this.j.get(0)).get(i2)).getText()).toString().replace("-", "0"));
        }
        return arrayList;
    }

    private ArrayList h() {
        ArrayList arrayList = new ArrayList();
        for (int i2 = 0; i2 < 4; i2++) {
            arrayList.add(String.valueOf(new StringBuilder().append((Object) ((TextView) ((ArrayList) this.i.get(0)).get(i2)).getText()).toString().replace("-", "0").replace("%", "")) + "," + new StringBuilder().append((Object) ((TextView) ((ArrayList) this.i.get(1)).get(i2)).getText()).toString().replace("-", "0").replace("%", "") + "," + new StringBuilder().append((Object) ((TextView) ((ArrayList) this.i.get(2)).get(i2)).getText()).toString().replace("-", "0").replace("%", "") + "," + new StringBuilder().append((Object) ((TextView) ((ArrayList) this.i.get(3)).get(i2)).getText()).toString().replace("-", "0").replace("%", "") + "," + new StringBuilder().append((Object) ((TextView) ((ArrayList) this.i.get(4)).get(i2)).getText()).toString().replace("-", "0").replace("%", "") + "," + new StringBuilder().append((Object) ((TextView) ((ArrayList) this.i.get(5)).get(i2)).getText()).toString().replace("-", "0").replace("%", ""));
        }
        return arrayList;
    }

    private ArrayList i() {
        ArrayList arrayList = new ArrayList();
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= 4) {
                return arrayList;
            }
            arrayList.add(String.valueOf(new StringBuilder().append((Object) ((TextView) ((ArrayList) this.h.get(1)).get(i3)).getText()).toString().replace("-", "0")) + "," + new StringBuilder().append((Object) ((TextView) ((ArrayList) this.h.get(2)).get(i3)).getText()).toString().replace("-", "0") + "," + new StringBuilder().append((Object) ((TextView) ((ArrayList) this.h.get(3)).get(i3)).getText()).toString().replace("-", "0"));
            i2 = i3 + 1;
        }
    }

    public void onClick(View view) {
        Intent intent = new Intent(this, ScoreAnalysisGraph.class);
        String[] strArr = new String[this.g.size()];
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.g.size()) {
                break;
            }
            strArr[i3] = new StringBuilder().append((Object) ((TextView) this.g.get(i3)).getText()).toString();
            i2 = i3 + 1;
        }
        intent.putExtra(getString(R.string.intent_graph_players), strArr);
        switch (view.getId()) {
            case R.id.putt_analysis_graph /*2131427510*/:
                intent.putExtra(getString(R.string.intent_graph_mode), 1);
                intent.putExtra(getString(R.string.intent_graph_value), g());
                break;
            case R.id.score_analysis_graph /*2131427524*/:
                intent.putExtra(getString(R.string.intent_graph_mode), 2);
                intent.putExtra(getString(R.string.intent_graph_value), h());
                break;
            case R.id.stroke_analysis_graph /*2131427563*/:
                intent.putExtra(getString(R.string.intent_graph_mode), 3);
                intent.putExtra(getString(R.string.intent_graph_value), i());
                break;
        }
        startActivity(intent);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.score_analysis);
        this.b = b.a(this);
        this.c = getResources();
        Bundle extras = getIntent().getExtras();
        this.d = extras.getLongArray("player_ids");
        this.e = extras.getLong("playing_round_id");
        this.f = extras.getLong("playing_tee_id");
        d();
        e();
        f();
        c();
    }

    public void onDestroy() {
        com.asai24.golf.d.d.a(findViewById(R.id.score_analysis));
        super.onDestroy();
        System.gc();
    }
}
