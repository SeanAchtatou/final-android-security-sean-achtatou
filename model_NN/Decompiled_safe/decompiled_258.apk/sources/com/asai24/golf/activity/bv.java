package com.asai24.golf.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import com.asai24.golf.R;
import com.asai24.golf.a.m;
import java.net.URLEncoder;

class bv implements DialogInterface.OnClickListener {
    final /* synthetic */ ScoreEntryWithMap a;

    bv(ScoreEntryWithMap scoreEntryWithMap) {
        this.a = scoreEntryWithMap;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        m a2 = this.a.b.a(this.a.w);
        String str = "http://m.foursquare.com/search?q=" + URLEncoder.encode(a2.e());
        a2.close();
        try {
            this.a.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
        } catch (Exception e) {
            this.a.c(this.a.getString(R.string.no_foursquare_app));
        }
    }
}
