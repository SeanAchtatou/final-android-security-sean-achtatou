package com.asai24.golf.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.asai24.golf.R;
import com.asai24.golf.a.b;
import com.asai24.golf.a.d;
import com.asai24.golf.a.g;
import com.asai24.golf.a.p;
import com.asai24.golf.a.y;
import com.asai24.golf.d.m;
import com.asai24.golf.e.a;
import java.util.ArrayList;
import java.util.Calendar;

public class TotalAnalysis extends GolfActivity implements View.OnClickListener {
    private b b;
    private Resources c;
    private long d;
    /* access modifiers changed from: private */
    public Button e;
    private TextView f;
    private ArrayList g;
    private ArrayList h;
    private ArrayList i;
    private ArrayList j;
    private ArrayList k;
    /* access modifiers changed from: private */
    public ProgressDialog l;
    /* access modifiers changed from: private */
    public int m;
    /* access modifiers changed from: private */
    public int n;
    /* access modifiers changed from: private */
    public Handler o = new es(this);

    private int a(long j2) {
        g m2 = this.b.m(j2);
        a aVar = new a(this);
        int i2 = 0;
        for (int i3 = 0; i3 < m2.getCount(); i3++) {
            m2.moveToPosition(i3);
            if (aVar.a(m2.e()).equals("pt")) {
                i2++;
            }
        }
        m2.close();
        return i2;
    }

    private void d() {
        this.e = (Button) findViewById(R.id.analysis_term_btn);
        this.e.setOnClickListener(this);
        this.f = (TextView) findViewById(R.id.score_analysis_label_player1);
        this.g = new ArrayList();
        this.i = new ArrayList();
        this.j = new ArrayList();
        this.k = new ArrayList();
        this.h = new ArrayList();
        for (int i2 = 1; i2 <= 7; i2++) {
            if (i2 <= 2) {
                this.g.add((TextView) findViewById(this.c.getIdentifier("score_analysis_target" + i2 + "_player1", "id", getPackageName())));
            }
            if (i2 <= 3) {
                this.i.add((TextView) findViewById(this.c.getIdentifier("score_analysis_stroke" + i2 + "_player1", "id", getPackageName())));
            }
            if (i2 <= 7) {
                this.j.add((TextView) findViewById(this.c.getIdentifier("score_analysis_score" + i2 + "_player1", "id", getPackageName())));
            }
            if (i2 <= 1) {
                this.k.add((TextView) findViewById(this.c.getIdentifier("score_analysis_putt" + i2 + "_player1", "id", getPackageName())));
            }
            if (i2 <= 2) {
                this.h.add((TextView) findViewById(this.c.getIdentifier("score_analysis_gir" + i2 + "_player1", "id", getPackageName())));
            }
        }
    }

    private void e() {
        d d2 = this.b.d();
        this.f.setText(d2.c());
        this.d = d2.b();
        d2.close();
    }

    /* access modifiers changed from: private */
    public void f() {
        this.l = new ProgressDialog(this);
        this.l.setProgressStyle(0);
        this.l.setTitle(this.c.getString(R.string.sa_progress_title));
        this.l.setMessage(this.c.getString(R.string.sa_progress_message));
        this.l.setCancelable(false);
        this.l.show();
        new ep(this).start();
    }

    /* access modifiers changed from: private */
    public void g() {
        int i2;
        p n2;
        int i3;
        int i4;
        ArrayList arrayList = new ArrayList();
        for (int i5 = 0; i5 < 7; i5++) {
            arrayList.add(0);
        }
        switch (this.m) {
            case 0:
                i2 = 3;
                break;
            case 1:
                i2 = 6;
                break;
            case 2:
                i2 = 0;
                break;
            default:
                i2 = 3;
                break;
        }
        if (i2 == 0) {
            n2 = this.b.c();
        } else {
            Calendar instance = Calendar.getInstance();
            instance.add(2, -i2);
            instance.set(11, 0);
            instance.set(12, 0);
            instance.set(13, 0);
            instance.set(14, 0);
            n2 = this.b.n(instance.getTimeInMillis());
        }
        n2.moveToFirst();
        int i6 = 0;
        int i7 = 0;
        int i8 = 0;
        int i9 = 0;
        int i10 = 0;
        int i11 = 0;
        int i12 = 0;
        int i13 = 0;
        int i14 = 0;
        int i15 = 0;
        int i16 = 0;
        int i17 = 0;
        int i18 = 0;
        while (!n2.isAfterLast()) {
            y c2 = this.b.c(n2.b(), this.d, n2.d());
            c2.moveToFirst();
            int i19 = 0;
            int i20 = 0;
            int i21 = i15;
            int i22 = i16;
            int i23 = i13;
            int i24 = i18;
            int i25 = i6;
            int i26 = i7;
            int i27 = i8;
            int i28 = i9;
            int i29 = i10;
            int i30 = i11;
            while (!c2.isAfterLast()) {
                int b2 = c2.b();
                if (b2 != 0) {
                    i24++;
                    i20++;
                    int i31 = i19 + b2;
                    int d2 = c2.d();
                    int a = a(c2.e());
                    if (d2 == 3) {
                        i29 += b2;
                        i30++;
                    } else if (d2 == 4) {
                        i27 += b2;
                        i28++;
                    } else {
                        i25 += b2;
                        i26++;
                    }
                    if (b2 <= d2 - 2) {
                        arrayList.set(1, Integer.valueOf(((Integer) arrayList.get(1)).intValue() + 1));
                    } else if (b2 == d2 - 1) {
                        arrayList.set(2, Integer.valueOf(((Integer) arrayList.get(2)).intValue() + 1));
                    } else if (b2 == d2) {
                        arrayList.set(3, Integer.valueOf(((Integer) arrayList.get(3)).intValue() + 1));
                    } else if (b2 == d2 + 1) {
                        arrayList.set(4, Integer.valueOf(((Integer) arrayList.get(4)).intValue() + 1));
                    } else if (b2 == d2 + 2) {
                        arrayList.set(5, Integer.valueOf(((Integer) arrayList.get(5)).intValue() + 1));
                    } else if (b2 >= d2 + 3) {
                        arrayList.set(6, Integer.valueOf(((Integer) arrayList.get(6)).intValue() + 1));
                    }
                    int i32 = i23 + a;
                    if (b2 - a <= d2 - 2) {
                        i22++;
                        i23 = i32;
                        i19 = i31;
                    } else {
                        if (b2 - a == d2 - 1) {
                            i21++;
                            i23 = i32;
                            i19 = i31;
                        } else {
                            i23 = i32;
                            i19 = i31;
                        }
                    }
                }
                c2.moveToNext();
            }
            c2.close();
            int i33 = i18 != i24 ? i12 + 1 : i12;
            if (i20 == 18) {
                i4 = i14 + 1;
                i3 = i19 + i17;
            } else {
                i3 = i17;
                i4 = i14;
            }
            n2.moveToNext();
            i17 = i3;
            i14 = i4;
            i18 = i24;
            i9 = i28;
            i10 = i29;
            i11 = i30;
            i12 = i33;
            i7 = i26;
            i8 = i27;
            i6 = i25;
            i13 = i23;
            i15 = i21;
            i16 = i22;
        }
        n2.close();
        ((TextView) this.g.get(0)).setText(new StringBuilder().append(i12).toString());
        ((TextView) this.g.get(1)).setText(new StringBuilder().append(i18).toString());
        if (i18 > 0) {
            if (i11 != 0) {
                ((TextView) this.i.get(0)).setText(new StringBuilder().append(m.a(((double) i10) / ((double) i11), 3)).toString());
            }
            if (i9 != 0) {
                ((TextView) this.i.get(1)).setText(new StringBuilder().append(m.a(((double) i8) / ((double) i9), 3)).toString());
            }
            if (i7 != 0) {
                ((TextView) this.i.get(2)).setText(new StringBuilder().append(m.a(((double) i6) / ((double) i7), 3)).toString());
            }
            if (i14 > 0) {
                ((TextView) this.j.get(0)).setText(new StringBuilder(String.valueOf(m.a(((double) i17) / ((double) i14), 1))).toString());
            }
            ((TextView) this.j.get(1)).setText(String.valueOf(m.a((((double) ((Integer) arrayList.get(1)).intValue()) / ((double) i18)) * 100.0d, 1)) + "%");
            ((TextView) this.j.get(2)).setText(String.valueOf(m.a((((double) ((Integer) arrayList.get(2)).intValue()) / ((double) i18)) * 100.0d, 1)) + "%");
            ((TextView) this.j.get(3)).setText(String.valueOf(m.a((((double) ((Integer) arrayList.get(3)).intValue()) / ((double) i18)) * 100.0d, 1)) + "%");
            ((TextView) this.j.get(4)).setText(String.valueOf(m.a((((double) ((Integer) arrayList.get(4)).intValue()) / ((double) i18)) * 100.0d, 1)) + "%");
            ((TextView) this.j.get(5)).setText(String.valueOf(m.a((((double) ((Integer) arrayList.get(5)).intValue()) / ((double) i18)) * 100.0d, 1)) + "%");
            ((TextView) this.j.get(6)).setText(String.valueOf(m.a((((double) ((Integer) arrayList.get(6)).intValue()) / ((double) i18)) * 100.0d, 1)) + "%");
            ((TextView) this.k.get(0)).setText(new StringBuilder().append(m.a(((double) i13) / ((double) i18), 3)).toString());
            ((TextView) this.h.get(0)).setText(String.valueOf(m.a((((double) i16) / ((double) i18)) * 100.0d)) + "%");
            ((TextView) this.h.get(1)).setText(String.valueOf(m.a((((double) i15) / ((double) i18)) * 100.0d)) + "%");
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.analysis_term_btn /*2131428175*/:
                showDialog(1);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.total_analysis);
        this.b = b.a(this);
        this.c = getResources();
        this.m = 0;
        this.n = 0;
        d();
        e();
        f();
        c();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i2) {
        switch (i2) {
            case 1:
                return new AlertDialog.Builder(this).setIcon((int) R.drawable.alert_dialog_icon).setTitle((int) R.string.menu_specify_term_title).setSingleChoiceItems((int) R.array.analysis_term, this.m, new er(this)).setPositiveButton((int) R.string.ok, new fb(this)).setNegativeButton((int) R.string.cancel, new fd(this)).create();
            default:
                return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        com.asai24.golf.d.d.a(findViewById(R.id.total_analysis));
        super.onDestroy();
    }
}
