package com.asai24.golf.activity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.asai24.golf.R;
import java.util.List;

class y extends ArrayAdapter {
    LayoutInflater a;
    final /* synthetic */ ScoreEntryGroup b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public y(ScoreEntryGroup scoreEntryGroup, Context context, List list) {
        super(context, 0, list);
        this.b = scoreEntryGroup;
        this.a = LayoutInflater.from(context);
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        View inflate = view == null ? this.a.inflate((int) R.layout.score_entry_group_item, (ViewGroup) null) : view;
        cm cmVar = (cm) getItem(i);
        inflate.setId(i);
        inflate.setTag(Integer.valueOf(cmVar.b()));
        ((TextView) inflate.findViewById(R.id.score_entry_group_item1)).setVisibility(4);
        ((TextView) inflate.findViewById(R.id.score_entry_group_item2)).setText(new StringBuilder().append(cmVar.a()).toString());
        if (this.b.x == i) {
            inflate.setBackgroundResource(R.drawable.gradation_lightyellow);
        } else {
            inflate.setBackgroundResource(R.drawable.gradation_yellowgreen);
        }
        inflate.setOnClickListener(cmVar);
        return inflate;
    }
}
