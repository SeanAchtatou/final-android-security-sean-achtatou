package com.asai24.golf.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import com.asai24.golf.R;
import com.asai24.golf.a.e;

class w extends AsyncTask {
    final /* synthetic */ CourseDetail a;

    private w(CourseDetail courseDetail) {
        this.a = courseDetail;
    }

    /* synthetic */ w(CourseDetail courseDetail, w wVar) {
        this(courseDetail);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Long doInBackground(String... strArr) {
        return Long.valueOf(this.a.b.a(this.a.n, this.a.m.a(), this.a.o));
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(Long l) {
        e a2 = this.a.b.a(l.longValue(), this.a.m.a());
        long c = a2.c();
        a2.close();
        this.a.c.a("/add_your_friends");
        this.a.c.b("/add_your_friends");
        Intent intent = new Intent(this.a, SelectPlayers.class);
        intent.putExtra("playing_course_id", l);
        intent.putExtra("playing_tee_id", c);
        this.a.startActivityForResult(intent, 0);
        this.a.l.dismiss();
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        CourseDetail courseDetail = this.a;
        Resources resources = courseDetail.getResources();
        this.a.l = new ProgressDialog(courseDetail);
        this.a.l.setIndeterminate(true);
        this.a.l.setMessage(resources.getString(R.string.msg_now_loading));
        if (!this.a.isFinishing()) {
            this.a.l.show();
        }
    }
}
