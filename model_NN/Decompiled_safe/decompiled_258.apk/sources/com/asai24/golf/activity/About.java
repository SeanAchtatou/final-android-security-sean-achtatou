package com.asai24.golf.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.asai24.golf.GolfApplication;
import com.asai24.golf.R;
import com.asai24.golf.d.d;
import java.util.ArrayList;

public class About extends GolfActivity implements View.OnClickListener, AdapterView.OnItemClickListener {
    /* access modifiers changed from: private */
    public Resources b;
    private GolfApplication c;
    private PackageManager d;
    private PackageInfo e;
    private ListView f;
    private ListView g;
    private ListView h;
    private ListView i;
    private TextView j;

    private void d() {
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        ArrayList arrayList4 = new ArrayList();
        bk bkVar = new bk(this, R.drawable.setting_favicon, R.string.setting_title);
        bkVar.a(this);
        bkVar.a(2);
        arrayList.add(bkVar);
        bk bkVar2 = new bk(this, R.drawable.data_transfer, R.string.data_management_title);
        bkVar2.a(this);
        bkVar2.a(3);
        arrayList2.add(bkVar2);
        bk bkVar3 = new bk(this, R.drawable.oob_favicon, R.string.oobgolf_title);
        bkVar3.a(this);
        bkVar3.a(6);
        arrayList3.add(bkVar3);
        bk bkVar4 = new bk(this, R.drawable.facebook_icon, R.string.facebook_title);
        bkVar4.a(this);
        bkVar4.a(7);
        arrayList3.add(bkVar4);
        bk bkVar5 = new bk(this, R.drawable.user_guide_favicon, R.string.user_guide);
        bkVar5.a(this);
        bkVar5.a(4);
        arrayList4.add(bkVar5);
        bk bkVar6 = new bk(this, R.drawable.contact_us, R.string.contact_us_title);
        bkVar6.a(this);
        bkVar6.a(5);
        arrayList4.add(bkVar6);
        this.f.setAdapter((ListAdapter) new eh(this, this, arrayList));
        this.f.setOnItemClickListener(this);
        this.g.setAdapter((ListAdapter) new eh(this, this, arrayList2));
        this.g.setOnItemClickListener(this);
        this.h.setAdapter((ListAdapter) new eh(this, this, arrayList3));
        this.h.setOnItemClickListener(this);
        this.i.setAdapter((ListAdapter) new eh(this, this, arrayList4));
        this.i.setOnItemClickListener(this);
    }

    private void e() {
        try {
            this.e = this.d.getPackageInfo(getPackageName(), 16384);
            this.j.setText("version : " + this.e.versionName);
        } catch (PackageManager.NameNotFoundException e2) {
            Log.e("About", "package info lost ", e2);
            this.j.setVisibility(8);
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.facebook /*2131427344*/:
                a(this.b.getString(R.string.facebook_url));
                return;
            case R.id.twitter /*2131427345*/:
                a(this.b.getString(R.string.twitter_your_golf));
                return;
            case R.id.email /*2131427346*/:
                b(this.b.getString(R.string.mail_your_golf));
                return;
            case R.id.close /*2131427347*/:
                dismissDialog(5);
                return;
            default:
                switch (Integer.parseInt(new StringBuilder().append(view.getTag()).toString())) {
                    case 2:
                        this.c.a("/settings");
                        this.c.b("/settings");
                        startActivity(new Intent(this, GolfSettings.class));
                        return;
                    case 3:
                        String string = PreferenceManager.getDefaultSharedPreferences(this).getString(getString(R.string.yourgolf_account_auth_token_key), "");
                        if (string == null || string.equals("")) {
                            showDialog(1);
                            return;
                        } else {
                            startActivity(new Intent(this, YourGolfDataManagement.class));
                            return;
                        }
                    case 4:
                        a(this.b.getString(R.string.user_guide_url));
                        return;
                    case 5:
                        showDialog(5);
                        return;
                    case 6:
                        showDialog(6);
                        return;
                    case 7:
                        showDialog(7);
                        return;
                    case 8:
                        showDialog(8);
                        return;
                    default:
                        return;
                }
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.about);
        this.c = (GolfApplication) getApplication();
        this.b = getResources();
        this.d = getPackageManager();
        this.f = (ListView) findViewById(R.id.setting_list);
        this.g = (ListView) findViewById(R.id.data_management_list);
        this.h = (ListView) findViewById(R.id.about_list);
        this.i = (ListView) findViewById(R.id.support_list);
        this.j = (TextView) findViewById(R.id.version);
        d();
        e();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i2) {
        switch (i2) {
            case 1:
                return new AlertDialog.Builder(this).setIcon(17301543).setTitle((int) R.string.data_management_no_account_title).setMessage((int) R.string.data_management_no_account_msg).setPositiveButton((int) R.string.setting_title, new au(this)).setNegativeButton((int) R.string.cancel, new as(this)).create();
            case 2:
            case 3:
            case 4:
            default:
                return null;
            case 5:
                View inflate = LayoutInflater.from(this).inflate((int) R.layout.alert_dialog_contact_us, (ViewGroup) null);
                ((TextView) inflate.findViewById(R.id.description_text)).setText((int) R.string.contact_us_description);
                ((ImageButton) inflate.findViewById(R.id.facebook)).setOnClickListener(this);
                ((ImageButton) inflate.findViewById(R.id.twitter)).setOnClickListener(this);
                ((ImageButton) inflate.findViewById(R.id.email)).setOnClickListener(this);
                ((ImageButton) inflate.findViewById(R.id.close)).setOnClickListener(this);
                return new AlertDialog.Builder(this).setIcon((int) R.drawable.contact_us).setTitle((int) R.string.contact_us_title).setView(inflate).create();
            case 6:
                return new AlertDialog.Builder(this).setIcon((int) R.drawable.oob_favicon).setTitle((int) R.string.oobgolf_title).setMessage(this.b.getString(R.string.oobgolf_description)).setPositiveButton(this.b.getString(R.string.open_site), new aw(this)).setNegativeButton((int) R.string.cancel, new av(this)).create();
            case 7:
                return new AlertDialog.Builder(this).setIcon((int) R.drawable.facebook_icon).setTitle((int) R.string.facebook_title).setMessage(this.b.getString(R.string.facebook_description)).setPositiveButton(this.b.getString(R.string.open_site), new ay(this)).setNegativeButton((int) R.string.cancel, new ax(this)).create();
            case 8:
                return new AlertDialog.Builder(this).setIcon((int) R.drawable.asai_favicon).setTitle((int) R.string.asai_title).setMessage(this.b.getString(R.string.asai_description)).setPositiveButton(this.b.getString(R.string.open_site), new an(this)).setNegativeButton((int) R.string.cancel, new am(this)).create();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.c.a();
        this.c.c();
        d.a(findViewById(R.id.about));
        super.onDestroy();
    }

    public void onItemClick(AdapterView adapterView, View view, int i2, long j2) {
        ((bk) adapterView.getAdapter().getItem(i2)).onClick(view);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.c.b();
        super.onPause();
    }
}
