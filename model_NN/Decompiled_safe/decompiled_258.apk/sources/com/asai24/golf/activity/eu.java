package com.asai24.golf.activity;

import android.os.AsyncTask;

public class eu extends AsyncTask {
    final /* synthetic */ SearchCourse a;

    protected eu(SearchCourse searchCourse) {
        this.a = searchCourse;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Integer doInBackground(String... strArr) {
        if (!this.a.a()) {
            return -1;
        }
        this.a.A.a();
        return 0;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(Integer num) {
        String b;
        if (num.intValue() == 0 && (b = this.a.A.b()) != null && !b.trim().equals("")) {
            this.a.B.setText(b);
            this.a.B.setVisibility(0);
        }
    }
}
