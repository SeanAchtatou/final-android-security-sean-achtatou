package com.asai24.golf.activity;

import android.content.Intent;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceScreen;
import com.asai24.golf.R;

public class GolfSettings extends PreferenceActivity implements Preference.OnPreferenceClickListener {
    private PreferenceScreen a;
    private PreferenceScreen b;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        getWindow().setBackgroundDrawableResource(R.drawable.yourgolf_bg_image2);
        getListView().setCacheColorHint(0);
        getListView().setDivider(null);
        getListView().setDividerHeight(3);
        addPreferencesFromResource(R.xml.settings);
        this.a = (PreferenceScreen) findPreference(getString(R.string.setting_yourgolf_key));
        this.b = (PreferenceScreen) findPreference(getString(R.string.setting_oobgolf_key));
        this.a.setOnPreferenceClickListener(this);
        this.b.setOnPreferenceClickListener(this);
    }

    public boolean onPreferenceClick(Preference preference) {
        Intent intent = new Intent(this, GolfSettingsDetail.class);
        intent.putExtra(getString(R.string.setting_detail_mode), preference.getKey());
        startActivity(intent);
        return false;
    }
}
