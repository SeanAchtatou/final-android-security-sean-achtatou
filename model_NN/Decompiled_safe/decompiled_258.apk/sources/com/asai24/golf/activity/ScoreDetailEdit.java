package com.asai24.golf.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.asai24.golf.R;
import com.asai24.golf.a.b;
import com.asai24.golf.a.g;
import com.asai24.golf.d.d;
import com.asai24.golf.d.m;
import java.util.ArrayList;

public class ScoreDetailEdit extends GolfActivity implements View.OnClickListener {
    private b b;
    private Boolean c;
    private Resources d;
    private long e;
    private int f;
    private TextView g;
    private TextView h;
    private TextView i;
    /* access modifiers changed from: private */
    public TextView j;
    private TextView k;
    private TextView l;
    private Button m;
    private Button n;
    private Button o;
    private Button p;
    private Button q;
    private Button r;
    private ArrayList s;
    private ArrayList t;
    private ArrayList u;
    private Boolean v = false;
    /* access modifiers changed from: private */
    public String w;

    private void a(View view) {
        g m2 = this.b.m(this.e);
        int count = m2.getCount();
        m2.close();
        this.s = new ArrayList();
        for (int i2 = 0; i2 < 15; i2++) {
            Button button = (Button) view.findViewById(this.d.getIdentifier("shot_no_" + (i2 + 1) + "_btn", "id", getPackageName()));
            if (this.i.getText() != null && !this.i.getText().equals("") && i2 == Integer.valueOf(new StringBuilder().append((Object) this.i.getText()).toString()).intValue() - 1) {
                button.setEnabled(false);
                button.setBackgroundResource(R.drawable.circle_btn_clicked);
            }
            if (i2 <= count) {
                button.setOnClickListener(this);
                this.s.add(button);
            } else {
                button.setVisibility(4);
            }
        }
    }

    private void b(View view) {
        ((RelativeLayout) view.findViewById(R.id.club_gps_status_layout)).removeAllViews();
        ((Button) view.findViewById(R.id.club_cancel_btn)).setVisibility(4);
        this.t = new ArrayList();
        String[] stringArray = this.d.getStringArray(R.array.dialogue_club_id);
        for (String identifier : stringArray) {
            Button button = (Button) view.findViewById(this.d.getIdentifier(identifier, "id", getPackageName()));
            button.setOnClickListener(this);
            String sb = new StringBuilder().append(button.getTag()).toString();
            if (sb.equals(getString(R.string.club_ptt))) {
                if (sb.equals(this.w) || this.w.equals(getString(R.string.club_pt))) {
                    button.setEnabled(false);
                    button.setBackgroundResource(R.drawable.square_btn2_clicked);
                }
            } else if (sb.equals(this.w)) {
                button.setEnabled(false);
                button.setBackgroundResource(R.drawable.square_btn2_clicked);
            }
            this.t.add(button);
        }
    }

    private void c(View view) {
        this.u = new ArrayList();
        String[] stringArray = this.d.getStringArray(R.array.dialogue_shot_result_id);
        for (String identifier : stringArray) {
            Button button = (Button) view.findViewById(this.d.getIdentifier(identifier, "id", getPackageName()));
            button.setOnClickListener(this);
            if (button.getText().equals(this.k.getText())) {
                button.setEnabled(false);
                button.setBackgroundResource(R.drawable.square_btn1_clicked);
            }
            this.u.add(button);
        }
    }

    private void d() {
        this.w = "";
        if (!this.c.booleanValue()) {
            g v2 = this.b.v((long) this.f);
            this.h.setText(new StringBuilder().append(v2.d()).toString());
            RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.score_detail_shot_no_layout);
            ((LinearLayout) relativeLayout.getParent()).removeView(relativeLayout);
            this.w = v2.e();
            if (this.w.equals(getString(R.string.club_ptt))) {
                this.j.setText(getString(R.string.club_pt));
            } else {
                this.j.setText(this.w);
            }
            this.k.setText(v2.f());
            double a = m.a(v2.g(), 4);
            double a2 = m.a(v2.h(), 4);
            v2.close();
            if (a == 0.0d || a2 == 0.0d) {
                RelativeLayout relativeLayout2 = (RelativeLayout) findViewById(R.id.score_detail_shot_loc_layout);
                ((LinearLayout) relativeLayout2.getParent()).removeView(relativeLayout2);
                return;
            }
            this.l.setText(String.valueOf(this.d.getString(R.string.shot_loc_lat)) + a + "\n" + this.d.getString(R.string.shot_loc_lon) + a2);
            return;
        }
        this.g.setText(this.d.getString(R.string.shot_detail_add_new));
        RelativeLayout relativeLayout3 = (RelativeLayout) findViewById(R.id.score_detail_shot_loc_layout);
        ((LinearLayout) relativeLayout3.getParent()).removeView(relativeLayout3);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.asai24.golf.a.b.a(long, double, double):void
     arg types: [long, int, int]
     candidates:
      com.asai24.golf.a.b.a(long, int, java.lang.String):long
      com.asai24.golf.a.b.a(long, long, java.util.List):long
      com.asai24.golf.a.b.a(com.asai24.golf.e.d, long, int):long
      com.asai24.golf.a.b.a(long, long, long[]):com.asai24.golf.a.aa
      com.asai24.golf.a.b.a(long, long, long):com.asai24.golf.a.w
      com.asai24.golf.a.b.a(long, long, java.lang.String):com.asai24.golf.a.z
      com.asai24.golf.a.b.a(long, java.lang.Integer, java.lang.Integer):void
      com.asai24.golf.a.b.a(long, java.lang.String, java.lang.String):void
      com.asai24.golf.a.b.a(long, double, double):void */
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.club_dr_btn /*2131427358*/:
            case R.id.club_3w_btn /*2131427359*/:
            case R.id.club_4w_btn /*2131427360*/:
            case R.id.club_5w_btn /*2131427361*/:
            case R.id.club_7w_btn /*2131427362*/:
            case R.id.club_hy_btn /*2131427363*/:
            case R.id.club_2i_btn /*2131427364*/:
            case R.id.club_3i_btn /*2131427365*/:
            case R.id.club_4i_btn /*2131427366*/:
            case R.id.club_5i_btn /*2131427367*/:
            case R.id.club_6i_btn /*2131427368*/:
            case R.id.club_7i_btn /*2131427369*/:
            case R.id.club_8i_btn /*2131427370*/:
            case R.id.club_9i_btn /*2131427371*/:
            case R.id.club_pw_btn /*2131427372*/:
            case R.id.club_gw_btn /*2131427373*/:
            case R.id.club_sw_btn /*2131427374*/:
            case R.id.club_lw_btn /*2131427375*/:
                dismissDialog(2);
                this.w = new StringBuilder().append(view.getTag()).toString();
                this.j.setText(this.w);
                return;
            case R.id.club_pt_btn /*2131427376*/:
                dismissDialog(2);
                showDialog(4);
                return;
            case R.id.shot_result_fairway_btn /*2131427381*/:
            case R.id.shot_result_rough_btn /*2131427382*/:
            case R.id.shot_result_green_btn /*2131427383*/:
            case R.id.shot_result_bunker_btn /*2131427384*/:
            case R.id.shot_result_hole_btn /*2131427385*/:
            case R.id.shot_result_water_btn /*2131427386*/:
            case R.id.shot_result_ob_btn /*2131427387*/:
            case R.id.shot_result_penalty_btn /*2131427388*/:
                dismissDialog(3);
                this.k.setText(((Button) view).getText());
                return;
            case R.id.shot_no_1_btn /*2131427390*/:
            case R.id.shot_no_2_btn /*2131427391*/:
            case R.id.shot_no_3_btn /*2131427392*/:
            case R.id.shot_no_4_btn /*2131427393*/:
            case R.id.shot_no_5_btn /*2131427394*/:
            case R.id.shot_no_6_btn /*2131427395*/:
            case R.id.shot_no_7_btn /*2131427396*/:
            case R.id.shot_no_8_btn /*2131427397*/:
            case R.id.shot_no_9_btn /*2131427398*/:
            case R.id.shot_no_10_btn /*2131427399*/:
            case R.id.shot_no_11_btn /*2131427400*/:
            case R.id.shot_no_12_btn /*2131427401*/:
            case R.id.shot_no_13_btn /*2131427402*/:
            case R.id.shot_no_14_btn /*2131427403*/:
            case R.id.shot_no_15_btn /*2131427404*/:
                dismissDialog(1);
                this.i.setText(((Button) view).getText());
                return;
            case R.id.score_detail_shot_no_btn /*2131428081*/:
                showDialog(1);
                return;
            case R.id.score_detail_club_btn /*2131428084*/:
                showDialog(2);
                return;
            case R.id.score_detail_result_btn /*2131428087*/:
                showDialog(3);
                return;
            case R.id.score_detail_shot_loc_btn /*2131428091*/:
                this.v = true;
                if (this.v.booleanValue()) {
                    this.l.setVisibility(4);
                    return;
                }
                return;
            case R.id.ok /*2131428092*/:
                String str = (String) this.k.getText();
                if (this.c.booleanValue()) {
                    String str2 = (String) this.i.getText();
                    if (str2 == null || str2.equals("")) {
                        c(this.d.getString(R.string.warning_shot_no_empty));
                        return;
                    } else if (!this.b.a(this.e, Integer.valueOf(str2).intValue(), this.w, str).booleanValue()) {
                        c(this.d.getString(R.string.warning_max_shot));
                        return;
                    }
                } else {
                    this.b.b((long) this.f, this.w, str);
                    if (this.v.booleanValue()) {
                        this.b.a((long) this.f, 0.0d, 0.0d);
                    }
                }
                finish();
                return;
            case R.id.cancel /*2131428093*/:
                finish();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.score_detail_edit);
        this.d = getResources();
        this.b = b.a(this);
        Bundle extras = getIntent().getExtras();
        this.c = Boolean.valueOf(extras.getBoolean("new_record_flg"));
        if (this.c.booleanValue()) {
            this.e = extras.getLong("score_id");
        } else {
            this.f = extras.getInt("score_detail_id");
        }
        this.g = (TextView) findViewById(R.id.score_detail_mode);
        this.h = (TextView) findViewById(R.id.score_detail_edit_shot_id);
        this.i = (TextView) findViewById(R.id.score_detail_shot_no_txt);
        this.j = (TextView) findViewById(R.id.score_detail_club_txt);
        this.k = (TextView) findViewById(R.id.score_detail_result_txt);
        this.l = (TextView) findViewById(R.id.score_detail_shot_loc_txt);
        this.m = (Button) findViewById(R.id.score_detail_shot_no_btn);
        this.n = (Button) findViewById(R.id.score_detail_club_btn);
        this.o = (Button) findViewById(R.id.score_detail_result_btn);
        this.p = (Button) findViewById(R.id.score_detail_shot_loc_btn);
        this.q = (Button) findViewById(R.id.ok);
        this.r = (Button) findViewById(R.id.cancel);
        this.m.setOnClickListener(this);
        this.n.setOnClickListener(this);
        this.o.setOnClickListener(this);
        this.p.setOnClickListener(this);
        this.q.setOnClickListener(this);
        this.r.setOnClickListener(this);
        d();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i2) {
        LayoutInflater from = LayoutInflater.from(this);
        switch (i2) {
            case 1:
                View inflate = from.inflate((int) R.layout.alert_dialog_select_shot_no, (ViewGroup) null);
                a(inflate);
                return new AlertDialog.Builder(this).setIcon((int) R.drawable.alert_dialog_icon).setTitle(this.d.getString(R.string.dialog_shot_no_select_title)).setView(inflate).setNegativeButton((int) R.string.cancel, new eb(this)).create();
            case 2:
                View inflate2 = from.inflate((int) R.layout.alert_dialog_select_club, (ViewGroup) null);
                b(inflate2);
                return new AlertDialog.Builder(this).setIcon((int) R.drawable.alert_dialog_icon).setTitle(this.d.getString(R.string.dialog_club_select_title)).setView(inflate2).setNegativeButton((int) R.string.cancel, new ea(this)).create();
            case 3:
                View inflate3 = from.inflate((int) R.layout.alert_dialog_select_result, (ViewGroup) null);
                c(inflate3);
                return new AlertDialog.Builder(this).setIcon((int) R.drawable.alert_dialog_icon).setTitle(this.d.getString(R.string.dialog_result_select_title)).setView(inflate3).setNegativeButton((int) R.string.cancel, new ef(this)).create();
            case 4:
                return new AlertDialog.Builder(this).setIcon((int) R.drawable.alert_dialog_icon).setTitle((int) R.string.confirmation).setMessage((int) R.string.dialog_putt_warning).setPositiveButton((int) R.string.yes, new ed(this)).setNegativeButton((int) R.string.no, new ec(this)).create();
            default:
                return super.onCreateDialog(i2);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        d.a(findViewById(R.id.score_detail_edit));
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onPrepareDialog(int i2, Dialog dialog) {
        switch (i2) {
            case 1:
                if (this.s != null) {
                    for (int i3 = 0; i3 < this.s.size(); i3++) {
                        ((Button) this.s.get(i3)).setEnabled(true);
                        ((Button) this.s.get(i3)).setBackgroundResource(R.drawable.circle_btn_selector);
                        if (this.i.getText() != null && !this.i.getText().equals("") && i3 == Integer.valueOf(new StringBuilder().append((Object) this.i.getText()).toString()).intValue() - 1) {
                            ((Button) this.s.get(i3)).setEnabled(false);
                            ((Button) this.s.get(i3)).setBackgroundResource(R.drawable.circle_btn_clicked);
                        }
                    }
                    return;
                }
                return;
            case 2:
                if (this.t != null) {
                    for (int i4 = 0; i4 < this.t.size(); i4++) {
                        ((Button) this.t.get(i4)).setOnClickListener(this);
                        String sb = new StringBuilder().append(((Button) this.t.get(i4)).getTag()).toString();
                        if (sb.equals(getString(R.string.club_ptt))) {
                            if (sb.equals(this.w) || this.w.equals(getString(R.string.club_pt))) {
                                ((Button) this.t.get(i4)).setEnabled(false);
                                ((Button) this.t.get(i4)).setBackgroundResource(R.drawable.square_btn2_clicked);
                            } else {
                                ((Button) this.t.get(i4)).setEnabled(true);
                                ((Button) this.t.get(i4)).setBackgroundResource(R.drawable.square_btn2_selector);
                            }
                        } else if (sb.equals(this.w)) {
                            ((Button) this.t.get(i4)).setEnabled(false);
                            ((Button) this.t.get(i4)).setBackgroundResource(R.drawable.square_btn2_clicked);
                        } else {
                            ((Button) this.t.get(i4)).setEnabled(true);
                            ((Button) this.t.get(i4)).setBackgroundResource(R.drawable.square_btn2_selector);
                        }
                    }
                    return;
                }
                return;
            case 3:
                if (this.u != null) {
                    for (int i5 = 0; i5 < this.u.size(); i5++) {
                        ((Button) this.u.get(i5)).setOnClickListener(this);
                        if (((Button) this.u.get(i5)).getText().equals(this.k.getText())) {
                            ((Button) this.u.get(i5)).setEnabled(false);
                            ((Button) this.u.get(i5)).setBackgroundResource(R.drawable.square_btn1_clicked);
                        } else {
                            ((Button) this.u.get(i5)).setEnabled(true);
                            ((Button) this.u.get(i5)).setBackgroundResource(R.drawable.square_btn1_selector);
                        }
                    }
                    return;
                }
                return;
            default:
                return;
        }
    }
}
