package com.asai24.golf.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;
import com.asai24.golf.R;
import com.asai24.golf.a;
import com.asai24.golf.c.h;

public class b extends AsyncTask {
    private static /* synthetic */ int[] c;
    final /* synthetic */ PlayHistories a;
    private Context b;

    public b(PlayHistories playHistories, Context context) {
        this.a = playHistories;
        this.b = context;
    }

    static /* synthetic */ int[] a() {
        int[] iArr = c;
        if (iArr == null) {
            iArr = new int[a.values().length];
            try {
                iArr[a.NONE.ordinal()] = 12;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[a.OOB_ERROR.ordinal()] = 6;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[a.OOB_INVALID_SESSION.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[a.OOB_NO_PERMISSION.ordinal()] = 5;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[a.OOB_NO_SETTINGS.ordinal()] = 2;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[a.OOB_SUCCESS.ordinal()] = 1;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[a.OOB_ZERO_SCORE.ordinal()] = 4;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[a.YOURGOLF_ERROR.ordinal()] = 11;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[a.YOURGOLF_INVALID_TOKEN.ordinal()] = 9;
            } catch (NoSuchFieldError e9) {
            }
            try {
                iArr[a.YOURGOLF_NO_SETTINGS.ordinal()] = 8;
            } catch (NoSuchFieldError e10) {
            }
            try {
                iArr[a.YOURGOLF_SUCCESS.ordinal()] = 7;
            } catch (NoSuchFieldError e11) {
            }
            try {
                iArr[a.YOURGOLF_UNEXPECTED_ERROR.ordinal()] = 10;
            } catch (NoSuchFieldError e12) {
            }
            c = iArr;
        }
        return iArr;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public a doInBackground(String... strArr) {
        try {
            return new h(this.b).a(this.a.l);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(a aVar) {
        super.onPostExecute(aVar);
        if (this.a.q) {
            this.a.s = aVar;
            if (this.a.r != a.NONE) {
                this.a.h();
                this.a.j.dismiss();
                return;
            }
            return;
        }
        switch (a()[aVar.ordinal()]) {
            case 8:
                this.a.showDialog(7);
                break;
            case 9:
                this.a.showDialog(8);
                break;
            case 10:
                this.a.showDialog(9);
                break;
            case 11:
                Toast.makeText(this.a, (int) R.string.status_send_error, 1).show();
                break;
            default:
                Toast.makeText(this.a, (int) R.string.send_success, 1).show();
                break;
        }
        this.a.j.dismiss();
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        super.onPreExecute();
        if (!this.a.q) {
            this.a.j = new ProgressDialog(this.a);
            this.a.j.setIndeterminate(true);
            this.a.j.setCancelable(false);
            this.a.j.setMessage(this.a.k.getString(R.string.msg_now_loading));
            if (!this.a.isFinishing()) {
                this.a.j.show();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onProgressUpdate(Object... objArr) {
        super.onProgressUpdate(objArr);
    }
}
