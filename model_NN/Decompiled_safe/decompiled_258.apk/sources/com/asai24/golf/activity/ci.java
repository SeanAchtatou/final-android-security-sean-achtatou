package com.asai24.golf.activity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.asai24.golf.R;
import java.util.List;

public class ci extends ArrayAdapter {
    final /* synthetic */ YourGolfAccountUpdate a;
    private final LayoutInflater b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ci(YourGolfAccountUpdate yourGolfAccountUpdate, Context context, List list) {
        super(context, 0, list);
        this.a = yourGolfAccountUpdate;
        this.b = LayoutInflater.from(context);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i, View view, ViewGroup viewGroup) {
        View inflate = view == null ? this.b.inflate((int) R.layout.yourgolf_account_list_item, viewGroup, false) : view;
        dq dqVar = (dq) getItem(i);
        ((TextView) inflate.findViewById(R.id.ya_title)).setText(dqVar.a());
        ((TextView) inflate.findViewById(R.id.ya_summary)).setText(dqVar.b());
        return inflate;
    }
}
