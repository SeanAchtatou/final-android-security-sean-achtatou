package com.asai24.golf.activity;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;
import com.asai24.golf.R;
import com.asai24.golf.a.m;
import com.asai24.golf.a.p;
import java.text.SimpleDateFormat;
import java.util.Date;

class cu extends CursorAdapter {
    final /* synthetic */ PlayHistories a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public cu(PlayHistories playHistories, Context context, Cursor cursor) {
        super(context, cursor);
        this.a = playHistories;
    }

    public void bindView(View view, Context context, Cursor cursor) {
        p pVar = (p) cursor;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        long b = pVar.b();
        String g = pVar.g();
        String format = simpleDateFormat.format(new Date(pVar.h()));
        ((TextView) view.findViewById(R.id.course_name)).setText(g);
        ((TextView) view.findViewById(R.id.play_date)).setText(format);
        if (this.a.p) {
            TextView textView = (TextView) view.findViewById(R.id.delete_btn);
            TextView textView2 = (TextView) view.findViewById(R.id.edit_play_date);
            textView.setOnClickListener(this.a);
            textView2.setOnClickListener(this.a);
            textView.setTag(Long.valueOf(b));
            textView2.setTag(Long.valueOf(b));
            return;
        }
        TextView textView3 = (TextView) view.findViewById(R.id.upload_scores);
        m a2 = this.a.d.a(pVar.c());
        String f = a2.f();
        view.setTag("");
        textView3.setBackgroundResource(R.drawable.upload_btn_selector);
        textView3.setText((int) R.string.menu_send_server);
        textView3.setTextColor(this.a.k.getColor(R.color.history_btn_text));
        textView3.setOnClickListener(this.a);
        textView3.setTag(Long.valueOf(b));
        if (f != null && !f.equals("") && !this.a.o.contains(f)) {
            if (this.a.n >= 10) {
                view.setTag("OOB_INVISIBLE");
            } else {
                this.a.o.add(f);
            }
            PlayHistories playHistories = this.a;
            playHistories.n = playHistories.n + 1;
        }
        a2.close();
        view.setId((int) b);
        view.setClickable(false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        LayoutInflater from = LayoutInflater.from(context);
        return this.a.p ? from.inflate((int) R.layout.play_history_item_edit, viewGroup, false) : from.inflate((int) R.layout.play_history_item, viewGroup, false);
    }
}
