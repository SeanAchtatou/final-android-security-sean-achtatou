package com.asai24.golf.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

class ac extends Handler {
    final /* synthetic */ GolfSettingsDetail a;

    ac(GolfSettingsDetail golfSettingsDetail) {
        this.a = golfSettingsDetail;
    }

    public void handleMessage(Message message) {
        this.a.k.dismiss();
        this.a.k = (ProgressDialog) null;
        Bundle data = message.getData();
        this.a.a(data.getString("msg"), data.getBoolean("result"));
    }
}
