package com.asai24.golf.activity;

import android.app.DatePickerDialog;
import android.widget.DatePicker;
import java.util.Date;

class bb implements DatePickerDialog.OnDateSetListener {
    final /* synthetic */ PlayHistories a;

    bb(PlayHistories playHistories) {
        this.a = playHistories;
    }

    public void onDateSet(DatePicker datePicker, int i, int i2, int i3) {
        this.a.d.c(this.a.l, new Date(i - 1900, i2, i3).getTime());
        this.a.h.requery();
        this.a.i.notifyDataSetChanged();
    }
}
