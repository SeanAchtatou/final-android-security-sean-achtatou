package com.asai24.golf.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.text.InputFilter;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.asai24.golf.GolfApplication;
import com.asai24.golf.R;
import com.asai24.golf.c.a;
import com.asai24.golf.d.d;
import com.asai24.golf.d.i;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class YourGolfAccountUpdate extends GolfActivity implements View.OnClickListener, AdapterView.OnItemClickListener, Runnable {
    /* access modifiers changed from: private */
    public boolean A;
    private Handler B = new e(this);
    /* access modifiers changed from: private */
    public Resources b;
    private GolfApplication c;
    private RelativeLayout d;
    private TextView e;
    private TextView f;
    private TextView g;
    private TextView h;
    private TextView i;
    /* access modifiers changed from: private */
    public TextView j;
    private TextView k;
    private TextView l;
    private ListView m;
    private Button n;
    private Button o;
    /* access modifiers changed from: private */
    public EditText p;
    /* access modifiers changed from: private */
    public EditText q;
    private EditText r;
    /* access modifiers changed from: private */
    public String s;
    private a t;
    private ArrayAdapter u;
    /* access modifiers changed from: private */
    public ProgressDialog v;
    private String w;
    private String x;
    /* access modifiers changed from: private */
    public int y;
    /* access modifiers changed from: private */
    public int z;

    private String a(String str, String str2) {
        return (str == null || str.equals("")) ? str2 : str;
    }

    private void a(int i2, String str) {
        String b2;
        switch (this.z) {
            case 0:
                b2 = a(str, i2 == 0 ? this.b.getString(R.string.yourgolf_account_username_summary) : this.b.getString(R.string.yourgolf_account_username_confirm_summary));
                break;
            case 1:
            case 2:
                b2 = b(str, i2 == 0 ? this.b.getString(R.string.yourgolf_account_password_summary) : this.b.getString(R.string.yourgolf_account_password_confirm_summary));
                break;
            default:
                b2 = str;
                break;
        }
        ((dq) this.u.getItem(i2)).a(b2);
        this.u.notifyDataSetChanged();
    }

    /* access modifiers changed from: private */
    public void a(String str, boolean z2) {
        if (z2) {
            this.y = 1;
        } else {
            this.y = 2;
        }
        d(str);
    }

    /* access modifiers changed from: private */
    public String b(String str, String str2) {
        if (str == null || str.equals("")) {
            return str2;
        }
        char[] cArr = new char[str.length()];
        for (int i2 = 0; i2 < cArr.length; i2++) {
            cArr[i2] = '*';
        }
        return new String(cArr);
    }

    /* access modifiers changed from: private */
    public void b(int i2) {
        String str;
        String editable;
        if (i2 == 0) {
            String editable2 = this.q.getText().toString();
            str = editable2;
            editable = this.r.getText().toString();
        } else {
            String editable3 = this.r.getText().toString();
            str = editable3;
            editable = this.q.getText().toString();
        }
        if (!Pattern.compile("[\\w\\.\\-]+@(?:[\\w\\-]+\\.)+[\\w\\-]+").matcher(str).matches()) {
            d(this.b.getString(R.string.yourgolf_account_invalid_email));
        } else if (editable != null && !editable.equals("") && !str.equals(editable)) {
            d(this.b.getString(R.string.yourgolf_account_email_unmatch));
        }
        a(i2, str);
        i();
    }

    /* access modifiers changed from: private */
    public void c(int i2) {
        String str;
        String editable;
        if (i2 == 0) {
            String editable2 = this.q.getText().toString();
            str = editable2;
            editable = this.r.getText().toString();
        } else {
            String editable3 = this.r.getText().toString();
            str = editable3;
            editable = this.q.getText().toString();
        }
        if (!Pattern.compile("[a-zA-Z0-9_.-]{6,}").matcher(str).matches()) {
            d(this.b.getString(R.string.yourgolf_account_invalid_password));
        } else if (editable != null && !editable.equals("") && !str.equals(editable)) {
            d(this.b.getString(R.string.yourgolf_account_password_unmatch));
        }
        a(i2, str);
        i();
    }

    private void d() {
        this.e = (TextView) findViewById(R.id.yourgolf_account_current_email_label);
        this.f = (TextView) findViewById(R.id.yourgolf_account_current_email_title);
        this.g = (TextView) findViewById(R.id.yourgolf_account_current_email_summary);
        this.k = (TextView) findViewById(R.id.yourgolf_account_new_label);
        this.m = (ListView) findViewById(R.id.yourgolf_account_new_list);
        this.h = (TextView) findViewById(R.id.yourgolf_account_current_label);
        this.d = (RelativeLayout) findViewById(R.id.yourgolf_account_current_layout);
        if (this.z == 2) {
            this.h.setVisibility(8);
            this.d.setVisibility(8);
            this.A = true;
        } else {
            this.i = (TextView) findViewById(R.id.yourgolf_account_current_title);
            this.j = (TextView) findViewById(R.id.yourgolf_account_current_summary);
            this.d.setOnClickListener(this);
            this.A = false;
            this.p = new EditText(this);
        }
        this.l = (TextView) findViewById(R.id.yourgolf_account_note);
        this.n = (Button) findViewById(R.id.yourgolf_account_ok);
        this.o = (Button) findViewById(R.id.yourgolf_account_cancel);
        this.n.setOnClickListener(this);
        this.o.setOnClickListener(this);
        this.q = new EditText(this);
        this.r = new EditText(this);
    }

    /* access modifiers changed from: private */
    public void d(String str) {
        this.w = str;
        switch (this.y) {
            case 0:
                this.x = this.b.getString(R.string.warning);
                break;
            case 1:
                this.x = this.b.getString(R.string.success);
                break;
            case 2:
                this.x = this.b.getString(R.string.error);
                break;
        }
        showDialog(3);
    }

    private void e() {
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        switch (this.z) {
            case 0:
                this.e.setText((int) R.string.yourgolf_account_update_email_label_email);
                this.h.setText((int) R.string.yourgolf_account_update_email_current_item_label);
                this.i.setText((int) R.string.yourgolf_account_password);
                this.j.setText((int) R.string.yourgolf_account_password_summary);
                this.k.setText((int) R.string.yourgolf_account_update_email_new_item_label);
                this.l.setText((int) R.string.yourgolf_account_update_email_note);
                break;
            case 1:
                this.e.setText((int) R.string.yourgolf_account_username);
                this.h.setText((int) R.string.yourgolf_account_update_password_current_item_label);
                this.i.setText((int) R.string.yourgolf_account_password);
                this.j.setText((int) R.string.yourgolf_account_password_summary);
                this.k.setText((int) R.string.yourgolf_account_update_password_new_item_label);
                this.l.setText((int) R.string.yourgolf_account_update_password_note);
                break;
            case 2:
                this.e.setText((int) R.string.yourgolf_account_username);
                this.k.setText((int) R.string.yourgolf_account_overwirte_password_new_item_label);
                this.l.setText((int) R.string.yourgolf_account_overwirte_password_note);
                break;
        }
        this.f.setText((int) R.string.yourgolf_account_username);
        this.g.setText(defaultSharedPreferences.getString(this.b.getString(R.string.yourgolf_account_username_key), ""));
    }

    private void f() {
        this.s = PreferenceManager.getDefaultSharedPreferences(this).getString(this.b.getString(R.string.yourgolf_account_password_key), "");
    }

    private void g() {
        ArrayList arrayList = new ArrayList();
        switch (this.z) {
            case 0:
                arrayList.add(new dq(this, R.string.yourgolf_account_username, R.string.yourgolf_account_username_summary));
                arrayList.add(new dq(this, R.string.yourgolf_account_username_confirm, R.string.yourgolf_account_username_confirm_summary));
                break;
            case 1:
            case 2:
                arrayList.add(new dq(this, R.string.yourgolf_account_password, R.string.yourgolf_account_password_summary));
                arrayList.add(new dq(this, R.string.yourgolf_account_password_confirm, R.string.yourgolf_account_password_confirm_summary));
                break;
        }
        this.u = new ci(this, this, arrayList);
        this.m.setAdapter((ListAdapter) this.u);
        this.m.setOnItemClickListener(this);
    }

    private void h() {
        this.v = new ProgressDialog(this);
        this.v.setIndeterminate(true);
        this.v.setCancelable(false);
        this.v.setMessage(this.b.getString(R.string.msg_now_loading));
        this.v.show();
        new Thread(this).start();
    }

    /* access modifiers changed from: private */
    public void i() {
        switch (this.z) {
            case 0:
                if (!j() || !this.A) {
                    this.n.setBackgroundResource(R.drawable.square_btn1_disabled);
                    this.n.setClickable(false);
                    return;
                }
                this.n.setBackgroundResource(R.drawable.square_btn1_selector);
                this.n.setClickable(true);
                return;
            case 1:
            case 2:
                if (!k() || !this.A) {
                    this.n.setBackgroundResource(R.drawable.square_btn1_disabled);
                    this.n.setClickable(false);
                    return;
                }
                this.n.setBackgroundResource(R.drawable.square_btn1_selector);
                this.n.setClickable(true);
                return;
            default:
                return;
        }
    }

    private boolean j() {
        String editable = this.q.getText().toString();
        String editable2 = this.r.getText().toString();
        Pattern compile = Pattern.compile("[\\w\\.\\-]+@(?:[\\w\\-]+\\.)+[\\w\\-]+");
        Matcher matcher = compile.matcher(editable);
        Matcher matcher2 = compile.matcher(editable2);
        if (!matcher.matches() || !matcher2.matches()) {
            return false;
        }
        return editable.equals(editable2);
    }

    private boolean k() {
        String editable = this.q.getText().toString();
        String editable2 = this.r.getText().toString();
        Pattern compile = Pattern.compile("[a-zA-Z0-9_.-]{6,}");
        Matcher matcher = compile.matcher(editable);
        Matcher matcher2 = compile.matcher(editable2);
        if (!matcher.matches() || !matcher2.matches()) {
            return false;
        }
        return editable.equals(editable2);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.yourgolf_account_cancel /*2131428198*/:
                finish();
                return;
            case R.id.yourgolf_account_current_layout /*2131428209*/:
                showDialog(2);
                return;
            case R.id.yourgolf_account_ok /*2131428212*/:
                h();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.yourgolf_account_update);
        this.z = getIntent().getExtras().getInt(getString(R.string.yourgolf_account_update_mode));
        this.c = (GolfApplication) getApplication();
        this.b = getResources();
        this.t = new a(this);
        d();
        e();
        this.y = 0;
        this.x = "";
        f();
        g();
        i();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i2) {
        InputFilter[] inputFilterArr = new InputFilter[2];
        switch (this.z) {
            case 0:
                switch (i2) {
                    case 0:
                        inputFilterArr[0] = new InputFilter.LengthFilter(100);
                        inputFilterArr[1] = new i();
                        this.q.setFilters(inputFilterArr);
                        this.q.setInputType(32);
                        return new AlertDialog.Builder(this).setIcon((int) R.drawable.alert_dialog_icon).setTitle((int) R.string.yourgolf_account_username).setView(this.q).setPositiveButton((int) R.string.ok, new f(this)).setNegativeButton((int) R.string.cancel, new j(this)).create();
                    case 1:
                        inputFilterArr[0] = new InputFilter.LengthFilter(100);
                        inputFilterArr[1] = new i();
                        this.r.setFilters(inputFilterArr);
                        this.r.setInputType(32);
                        return new AlertDialog.Builder(this).setIcon((int) R.drawable.alert_dialog_icon).setTitle((int) R.string.yourgolf_account_username_confirm).setView(this.r).setPositiveButton((int) R.string.ok, new k(this)).setNegativeButton((int) R.string.cancel, new l(this)).create();
                    case 2:
                        inputFilterArr[0] = new InputFilter.LengthFilter(32);
                        inputFilterArr[1] = new i();
                        this.p.setFilters(inputFilterArr);
                        this.p.setInputType(128);
                        this.p.setTransformationMethod(PasswordTransformationMethod.getInstance());
                        return new AlertDialog.Builder(this).setIcon((int) R.drawable.alert_dialog_icon).setTitle((int) R.string.yourgolf_account_password).setView(this.p).setPositiveButton((int) R.string.ok, new m(this)).setNegativeButton((int) R.string.cancel, new g(this)).create();
                    case 3:
                        return new AlertDialog.Builder(this).setIcon((int) R.drawable.alert_dialog_icon).setTitle(this.x).setMessage(this.w).setPositiveButton((int) R.string.ok, new h(this)).create();
                }
            case 1:
            case 2:
                switch (i2) {
                    case 0:
                        inputFilterArr[0] = new InputFilter.LengthFilter(32);
                        inputFilterArr[1] = new i();
                        this.q.setFilters(inputFilterArr);
                        this.q.setInputType(128);
                        this.q.setTransformationMethod(PasswordTransformationMethod.getInstance());
                        return new AlertDialog.Builder(this).setIcon((int) R.drawable.alert_dialog_icon).setTitle((int) R.string.yourgolf_account_password).setView(this.q).setPositiveButton((int) R.string.ok, new i(this)).setNegativeButton((int) R.string.cancel, new co(this)).create();
                    case 1:
                        inputFilterArr[0] = new InputFilter.LengthFilter(32);
                        inputFilterArr[1] = new i();
                        this.r.setFilters(inputFilterArr);
                        this.r.setInputType(128);
                        this.r.setTransformationMethod(PasswordTransformationMethod.getInstance());
                        return new AlertDialog.Builder(this).setIcon((int) R.drawable.alert_dialog_icon).setTitle((int) R.string.yourgolf_account_password_confirm).setView(this.r).setPositiveButton((int) R.string.ok, new cp(this)).setNegativeButton((int) R.string.cancel, new cs(this)).create();
                    case 2:
                        inputFilterArr[0] = new InputFilter.LengthFilter(32);
                        inputFilterArr[1] = new i();
                        this.p.setFilters(inputFilterArr);
                        this.p.setInputType(128);
                        this.p.setTransformationMethod(PasswordTransformationMethod.getInstance());
                        return new AlertDialog.Builder(this).setIcon((int) R.drawable.alert_dialog_icon).setTitle((int) R.string.yourgolf_account_password).setView(this.p).setPositiveButton((int) R.string.ok, new ct(this)).setNegativeButton((int) R.string.cancel, new cq(this)).create();
                    case 3:
                        return new AlertDialog.Builder(this).setIcon((int) R.drawable.alert_dialog_icon).setTitle(this.x).setMessage(this.w).setPositiveButton((int) R.string.ok, new cr(this)).create();
                }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.c.a();
        this.c.c();
        d.a(findViewById(R.id.yourgolf_account_update));
        super.onDestroy();
    }

    public void onItemClick(AdapterView adapterView, View view, int i2, long j2) {
        showDialog(i2);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.c.b();
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onPrepareDialog(int i2, Dialog dialog) {
        switch (i2) {
            case 3:
                ((AlertDialog) dialog).setTitle(this.x);
                ((AlertDialog) dialog).setMessage(this.w);
                break;
        }
        super.onPrepareDialog(i2, dialog);
    }

    public void onWindowFocusChanged(boolean z2) {
        int i2 = 0;
        if (z2) {
            int i3 = 0;
            while (true) {
                View childAt = this.m.getChildAt(i2);
                if (childAt == null) {
                    break;
                }
                i3 += childAt.getHeight();
                i2++;
            }
            int dimensionPixelSize = this.b.getDimensionPixelSize(R.dimen.yourgolf_account_list_margin) + i3;
            if (this.m.getHeight() != dimensionPixelSize) {
                this.m.getLayoutParams().height = dimensionPixelSize;
                this.m.requestLayout();
            }
        }
    }

    public void run() {
        String str = "";
        switch (this.z) {
            case 0:
                str = this.t.a(this.q.getText().toString(), this.s, 1);
                break;
            case 1:
                String charSequence = this.g.getText().toString();
                String editable = this.q.getText().toString();
                String a = this.t.a(charSequence, editable, 1);
                if (!this.t.a()) {
                    str = a;
                    break;
                } else {
                    str = this.t.a(charSequence, editable, 3);
                    break;
                }
            case 2:
                str = this.t.a(this.g.getText().toString(), this.q.getText().toString(), 2);
                break;
        }
        Message message = new Message();
        Bundle bundle = new Bundle();
        bundle.putString("msg", str);
        bundle.putBoolean("result", this.t.a());
        message.setData(bundle);
        this.B.sendMessage(message);
    }
}
