package com.asai24.golf.activity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.asai24.golf.GolfApplication;
import com.asai24.golf.R;
import com.asai24.golf.a;
import com.asai24.golf.a.b;
import com.asai24.golf.a.m;
import com.asai24.golf.a.p;
import com.asai24.golf.a.s;
import com.asai24.golf.c.l;
import com.asai24.golf.d.d;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class PlayHistories extends GolfActivity implements View.OnClickListener, AdapterView.OnItemClickListener {
    private static /* synthetic */ int[] w;
    long[] b;
    /* access modifiers changed from: private */
    public GolfApplication c;
    /* access modifiers changed from: private */
    public b d;
    private ListView e;
    private Button f;
    private TextView g;
    /* access modifiers changed from: private */
    public p h;
    /* access modifiers changed from: private */
    public cu i;
    /* access modifiers changed from: private */
    public ProgressDialog j;
    /* access modifiers changed from: private */
    public Resources k;
    /* access modifiers changed from: private */
    public long l;
    /* access modifiers changed from: private */
    public boolean m;
    /* access modifiers changed from: private */
    public int n;
    /* access modifiers changed from: private */
    public ArrayList o;
    /* access modifiers changed from: private */
    public boolean p;
    /* access modifiers changed from: private */
    public boolean q;
    /* access modifiers changed from: private */
    public a r;
    /* access modifiers changed from: private */
    public a s;
    private String t;
    /* access modifiers changed from: private */
    public l u;
    /* access modifiers changed from: private */
    public TextView v;

    static /* synthetic */ int[] d() {
        int[] iArr = w;
        if (iArr == null) {
            iArr = new int[a.values().length];
            try {
                iArr[a.NONE.ordinal()] = 12;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[a.OOB_ERROR.ordinal()] = 6;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[a.OOB_INVALID_SESSION.ordinal()] = 3;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[a.OOB_NO_PERMISSION.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[a.OOB_NO_SETTINGS.ordinal()] = 2;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[a.OOB_SUCCESS.ordinal()] = 1;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[a.OOB_ZERO_SCORE.ordinal()] = 4;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[a.YOURGOLF_ERROR.ordinal()] = 11;
            } catch (NoSuchFieldError e9) {
            }
            try {
                iArr[a.YOURGOLF_INVALID_TOKEN.ordinal()] = 9;
            } catch (NoSuchFieldError e10) {
            }
            try {
                iArr[a.YOURGOLF_NO_SETTINGS.ordinal()] = 8;
            } catch (NoSuchFieldError e11) {
            }
            try {
                iArr[a.YOURGOLF_SUCCESS.ordinal()] = 7;
            } catch (NoSuchFieldError e12) {
            }
            try {
                iArr[a.YOURGOLF_UNEXPECTED_ERROR.ordinal()] = 10;
            } catch (NoSuchFieldError e13) {
            }
            w = iArr;
        }
        return iArr;
    }

    private void e() {
        this.e.setDivider(null);
        this.e.setDividerHeight(3);
        this.e.setCacheColorHint(this.k.getColor(R.color.list_bg_cache));
        f();
    }

    private void f() {
        if (this.p) {
            this.e.setOnItemClickListener(null);
        } else {
            this.e.setOnItemClickListener(this);
        }
    }

    private int g() {
        boolean z = (GolfApplication.d().length() == 0 || GolfApplication.e().length() == 0) ? false : true;
        String string = PreferenceManager.getDefaultSharedPreferences(this).getString(getString(R.string.yourgolf_account_auth_token_key), "");
        boolean z2 = string != null && !string.equals("");
        return !z ? z2 ? 2 : 1 : z2 ? 0 : 3;
    }

    /* access modifiers changed from: private */
    public void h() {
        String string;
        String string2;
        switch (d()[this.r.ordinal()]) {
            case 1:
                string = getString(R.string.send_success);
                break;
            case 2:
            default:
                string = getString(R.string.status_send_error);
                break;
            case 3:
                string = getString(R.string.invalid_session);
                break;
            case 4:
                string = getString(R.string.status_send_with_zero_score);
                break;
            case 5:
                string = getString(R.string.msg_score_alredy_deleted);
                break;
            case 6:
                string = getString(R.string.status_send_error);
                break;
        }
        switch (d()[this.s.ordinal()]) {
            case 7:
                string2 = getString(R.string.send_success);
                break;
            case 8:
            default:
                string2 = getString(R.string.status_send_error);
                break;
            case 9:
                string2 = getString(R.string.backup_error_e0105);
                break;
            case 10:
                string2 = getString(R.string.yourolf_upload_unexpected_error);
                break;
            case 11:
                string2 = getString(R.string.status_send_error);
                break;
        }
        this.t = String.valueOf(getString(R.string.upload_to_oobgolf)) + "\n" + string + "\n\n" + getString(R.string.upload_to_yourgolf) + "\n" + string2;
        showDialog(10);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.history_edit /*2131427462*/:
                this.p = !this.p;
                if (this.p) {
                    ((TextView) view).setText(getString(R.string.done));
                    this.g.setText(getString(R.string.history_editing));
                } else {
                    ((TextView) view).setText(getString(R.string.edit));
                    this.g.setText(getString(R.string.history_edit_note));
                }
                onResume();
                return;
            case R.id.upload_scores /*2131427467*/:
                this.m = false;
                this.q = false;
                this.r = a.NONE;
                this.s = a.NONE;
                this.l = Long.parseLong(new StringBuilder().append(view.getTag()).toString());
                if (!a()) {
                    showDialog(9);
                    return;
                }
                m a = this.d.a(this.d.o(this.l).c());
                String f2 = a.f();
                a.close();
                if (f2 == null || f2.equals("")) {
                    new b(this, this).execute(new String[0]);
                    return;
                }
                switch (g()) {
                    case 0:
                        this.c.a("History", "Upload", "Upload", 1);
                        this.q = true;
                        new a(this, this).execute(new String[0]);
                        new b(this, this).execute(new String[0]);
                        return;
                    case 1:
                        showDialog(3);
                        return;
                    case 2:
                        showDialog(1);
                        return;
                    case 3:
                        showDialog(2);
                        return;
                    default:
                        return;
                }
            case R.id.delete_btn /*2131427470*/:
                this.l = Long.parseLong(new StringBuilder().append(view.getTag()).toString());
                showDialog(0);
                return;
            case R.id.edit_play_date /*2131427472*/:
                this.l = Long.parseLong(new StringBuilder().append(view.getTag()).toString());
                removeDialog(6);
                showDialog(6);
                return;
            default:
                return;
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.play_history);
        this.c = (GolfApplication) getApplication();
        this.d = b.a(this);
        this.k = getResources();
        this.p = false;
        c();
        this.v = (TextView) findViewById(R.id.warning_text);
        this.u = new l(this);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i2) {
        switch (i2) {
            case 0:
                return new AlertDialog.Builder(this).setIcon((int) R.drawable.alert_dialog_icon).setTitle((int) R.string.delete_confirmation_title).setMessage((int) R.string.delete_confirmation_message).setPositiveButton((int) R.string.ok, new r(this)).setNegativeButton((int) R.string.cancel, new q(this)).create();
            case 1:
                return new AlertDialog.Builder(this).setIcon((int) R.drawable.alert_dialog_icon).setTitle((int) R.string.oops).setMessage((int) R.string.oobgolf_upload_no_oobgolf_account).setPositiveButton((int) R.string.setting, new p(this, this)).setNeutralButton((int) R.string.visit_oob_golf, new o(this)).setNegativeButton((int) R.string.cancel, new n(this)).create();
            case 2:
                return new AlertDialog.Builder(this).setIcon((int) R.drawable.alert_dialog_icon).setTitle((int) R.string.oops).setMessage((int) R.string.oobgolf_upload_no_yourgolf_account).setPositiveButton((int) R.string.continues, new v(this)).setNeutralButton((int) R.string.setting, new u(this, this)).setNegativeButton((int) R.string.cancel, new t(this)).create();
            case 3:
                return new AlertDialog.Builder(this).setIcon((int) R.drawable.alert_dialog_icon).setTitle((int) R.string.oops).setMessage((int) R.string.oobgolf_upload_no_accounts).setPositiveButton((int) R.string.setting, new s(this, this)).setNeutralButton((int) R.string.visit_oob_golf, new cg(this)).setNegativeButton((int) R.string.cancel, new ce(this)).create();
            case 4:
                return new AlertDialog.Builder(this).setIcon((int) R.drawable.alert_dialog_icon).setTitle((int) R.string.oops).setMessage((int) R.string.msg_score_alredy_deleted).setPositiveButton((int) R.string.send, new cc(this)).setNegativeButton((int) R.string.cancel, new az(this)).create();
            case 5:
                return new AlertDialog.Builder(this).setIcon((int) R.drawable.alert_dialog_icon).setTitle((int) R.string.sorry).setMessage((int) R.string.oob_course_limit_over).setPositiveButton((int) R.string.ok, new ba(this)).create();
            case 6:
                p o2 = this.d.o(this.l);
                Calendar instance = Calendar.getInstance();
                instance.setTimeInMillis(o2.h());
                o2.close();
                return new DatePickerDialog(this, new bb(this), instance.get(1), instance.get(2), instance.get(5));
            case 7:
                return new AlertDialog.Builder(this).setIcon((int) R.drawable.alert_dialog_icon).setTitle((int) R.string.oops).setMessage((int) R.string.yourgolf_upload_no_yourgolf_account).setPositiveButton((int) R.string.setting, new bc(this, this)).setNegativeButton((int) R.string.cancel, new be(this)).create();
            case 8:
                return new AlertDialog.Builder(this).setIcon((int) R.drawable.alert_dialog_icon).setTitle((int) R.string.oops).setMessage((int) R.string.backup_error_e0105).setPositiveButton((int) R.string.ok, new bg(this)).create();
            case 9:
                return new AlertDialog.Builder(this).setIcon((int) R.drawable.alert_dialog_icon).setTitle((int) R.string.oops).setMessage((int) R.string.yourolf_upload_unexpected_error).setPositiveButton((int) R.string.ok, new bi(this)).create();
            case 10:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setIcon((int) R.drawable.alert_dialog_icon);
                builder.setTitle((int) R.string.upload_result);
                builder.setMessage(this.t);
                if (this.r == a.OOB_NO_PERMISSION) {
                    builder.setPositiveButton((int) R.string.send, new bs(this));
                }
                builder.setNegativeButton((int) R.string.close, new br(this));
                return builder.create();
            default:
                return super.onCreateDialog(i2);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.c.a();
        this.c.c();
        d.a(findViewById(R.id.play_history));
        super.onDestroy();
    }

    public void onItemClick(AdapterView adapterView, View view, int i2, long j2) {
        if (!this.p) {
            Object tag = view.getTag();
            if (tag == null || tag.equals("")) {
                this.c.a("/score_entry");
                this.c.b("/score_entry");
                this.c.a("History", "Select", "Select", 1);
                p o2 = this.d.o(j2);
                long c2 = o2.c();
                long d2 = o2.d();
                s a = this.d.a(d2, 1);
                long b2 = a.b();
                this.b = this.d.u(j2);
                Intent intent = new Intent(this, ScoreEntryWithMap.class);
                intent.putExtra("playing_course_id", c2);
                intent.putExtra("playing_tee_id", d2);
                intent.putExtra("playing_round_id", j2);
                intent.putExtra("playing_hole_id", b2);
                intent.putExtra("player_ids", this.b);
                o2.close();
                a.close();
                startActivity(intent);
                return;
            }
            showDialog(5);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.p = false;
        this.o.clear();
        this.o = null;
        this.h.close();
        this.h = null;
        this.c.b();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.n = 0;
        this.o = new ArrayList();
        this.h = this.d.c();
        if (this.i == null) {
            this.i = new cu(this, this, this.h);
        } else {
            this.i.changeCursor(this.h);
        }
        this.e = (ListView) findViewById(R.id.history_list);
        this.e.setAdapter((ListAdapter) this.i);
        this.f = (Button) findViewById(R.id.history_edit);
        this.f.setOnClickListener(this);
        this.g = (TextView) findViewById(R.id.history_edit_note);
        e();
        Locale locale = Locale.getDefault();
        if (locale != null && locale.equals(Locale.JAPAN)) {
            new ds(this).execute(new String[0]);
        }
    }
}
