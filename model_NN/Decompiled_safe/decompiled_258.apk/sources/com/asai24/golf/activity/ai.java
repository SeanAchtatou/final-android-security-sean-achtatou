package com.asai24.golf.activity;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import com.asai24.golf.R;
import com.asai24.golf.c.c;
import com.asai24.golf.c.f;
import com.asai24.golf.c.i;
import com.asai24.golf.e.d;
import java.util.HashMap;
import java.util.List;

class ai extends AsyncTask {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ SearchCourse a;
    private int b;
    private ProgressDialog c;

    private ai(SearchCourse searchCourse) {
        this.a = searchCourse;
    }

    /* synthetic */ ai(SearchCourse searchCourse, ai aiVar) {
        this(searchCourse);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public f doInBackground(Integer... numArr) {
        this.b = numArr[0].intValue();
        switch (this.b) {
            case 0:
                this.a.n = new HashMap();
                this.a.n.put("keywords", this.a.e.getText().toString().trim());
                break;
            case 1:
                this.a.n = new HashMap();
                this.a.n.put("radius", "30");
                this.a.n.put("lat", new StringBuilder(String.valueOf(this.a.r.getLatitude())).toString());
                this.a.n.put("lng", new StringBuilder(String.valueOf(this.a.r.getLongitude())).toString());
                break;
            case 2:
                this.a.n.put("page", new StringBuilder().append(numArr[1].intValue()).toString());
                break;
        }
        this.a.f();
        if (this.a.x == 1) {
            this.a.o = new i(this.a).a(this.a.n);
        } else {
            this.a.o = new c(this.a).a(this.a.n);
        }
        return this.a.o;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(f fVar) {
        if (!this.a.z) {
            List<d> a2 = fVar.a();
            int b2 = fVar.b();
            this.a.l.setVisibility(8);
            if (this.b == 0 || this.b == 1) {
                this.a.m.clear();
            }
            for (d add : a2) {
                this.a.m.add(add);
            }
            if (fVar.c()) {
                if (this.a.h.getFooterViewsCount() == 0) {
                    this.a.h.addFooterView(this.a.j);
                }
                this.a.h.setFooterDividersEnabled(true);
                this.a.j.setOnClickListener(new ee(this, b2));
                this.a.j.setVisibility(0);
            } else {
                this.a.h.setFooterDividersEnabled(false);
                this.a.h.removeFooterView(this.a.j);
                this.a.j.setVisibility(8);
            }
            this.a.m.notifyDataSetChanged();
            this.c.dismiss();
        }
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        this.c = new ProgressDialog(this.a);
        this.c.setIndeterminate(true);
        this.c.setMessage(this.a.d.getString(R.string.msg_now_loading));
        if (!this.a.isFinishing()) {
            this.c.show();
        }
    }
}
