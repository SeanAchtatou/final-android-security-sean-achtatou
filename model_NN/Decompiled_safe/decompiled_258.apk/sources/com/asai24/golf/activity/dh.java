package com.asai24.golf.activity;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import com.asai24.golf.R;
import com.asai24.golf.d.e;
import com.google.android.maps.GeoPoint;

class dh implements LocationListener {
    final /* synthetic */ HoleMap a;

    dh(HoleMap holeMap) {
        this.a = holeMap;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.asai24.golf.activity.HoleMap.a(com.asai24.golf.activity.HoleMap, boolean):void
     arg types: [com.asai24.golf.activity.HoleMap, int]
     candidates:
      com.asai24.golf.activity.HoleMap.a(com.asai24.golf.activity.HoleMap, int):void
      com.asai24.golf.activity.HoleMap.a(com.asai24.golf.activity.HoleMap, com.google.android.maps.GeoPoint):void
      com.asai24.golf.activity.HoleMap.a(com.asai24.golf.activity.HoleMap, java.lang.String):void
      com.asai24.golf.activity.HoleMap.a(com.asai24.golf.activity.HoleMap, boolean):void */
    public void onLocationChanged(Location location) {
        GeoPoint geoPoint = new GeoPoint((int) (location.getLatitude() * 1000000.0d), (int) (location.getLongitude() * 1000000.0d));
        if (!this.a.t) {
            this.a.t = true;
            this.a.s.dismiss();
            this.a.n.getController().setCenter(geoPoint);
            this.a.a(this.a.d.getString(R.string.show_your_current_location));
        } else if (this.a.z == null) {
            this.a.y = true;
            this.a.h.a(true);
        } else if (e.a(this.a.z, geoPoint) > 3000.0d) {
            this.a.y = false;
            this.a.h.a(false);
        } else {
            this.a.y = true;
            this.a.h.a(true);
        }
        if (!this.a.B) {
            this.a.y = false;
            this.a.h.a(false);
        }
        if (this.a.y) {
            this.a.a(geoPoint);
            this.a.n.invalidate();
        }
    }

    public void onProviderDisabled(String str) {
    }

    public void onProviderEnabled(String str) {
    }

    public void onStatusChanged(String str, int i, Bundle bundle) {
    }
}
