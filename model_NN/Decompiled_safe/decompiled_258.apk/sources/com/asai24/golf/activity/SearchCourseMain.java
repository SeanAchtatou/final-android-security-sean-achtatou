package com.asai24.golf.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.asai24.golf.GolfApplication;
import com.asai24.golf.R;
import com.asai24.golf.d.d;
import java.util.Locale;

public class SearchCourseMain extends GolfActivity implements View.OnClickListener {
    private static String b = "SearchCouseMain";
    private GolfApplication c;
    private Button d;
    private Button e;
    private Button f;
    private Button g;

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i2 == -1) {
            finish();
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.course_oobgolf /*2131428140*/:
                this.c.a("/search");
                this.c.b("/search");
                this.c.a("SearchMain", "SeachMainOobgolf", "SeachMainOobgolf", 1);
                Intent intent = new Intent(this, SearchCourse.class);
                intent.putExtra(getString(R.string.intent_course_mode), 1);
                startActivityForResult(intent, 0);
                return;
            case R.id.course_yourgolf /*2131428141*/:
                this.c.a("/search");
                this.c.b("/search");
                this.c.a("SearchMain", "SeachMainYourgolf", "SeachMainYourgolf", 1);
                Intent intent2 = new Intent(this, SearchCourse.class);
                intent2.putExtra(getString(R.string.intent_course_mode), 2);
                startActivityForResult(intent2, 0);
                return;
            case R.id.course_history /*2131428142*/:
                this.c.a("/search");
                this.c.b("/search");
                this.c.a("SearchMain", "SeachMainHistory", "SeachMainHistory", 1);
                Intent intent3 = new Intent(this, SearchCourse.class);
                intent3.putExtra(getString(R.string.intent_course_mode), 3);
                startActivityForResult(intent3, 0);
                return;
            case R.id.course_golf_navi /*2131428143*/:
                try {
                    Intent intent4 = new Intent("android.intent.action.VIEW");
                    intent4.setClassName("com.asai24.golf_navi", "com.asai24.golf_navi.activity.GolfNaviTop");
                    intent4.setFlags(268435456);
                    startActivity(intent4);
                    return;
                } catch (Exception e2) {
                    showDialog(1);
                    return;
                }
            default:
                return;
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.c = (GolfApplication) getApplication();
        setContentView((int) R.layout.search_course_main);
        this.d = (Button) findViewById(R.id.course_oobgolf);
        this.e = (Button) findViewById(R.id.course_yourgolf);
        this.f = (Button) findViewById(R.id.course_history);
        this.g = (Button) findViewById(R.id.course_golf_navi);
        this.d.setOnClickListener(this);
        this.e.setOnClickListener(this);
        this.f.setOnClickListener(this);
        Locale locale = Locale.getDefault();
        if (locale != null && locale.equals(Locale.JAPAN)) {
            this.g.setVisibility(0);
            this.g.setOnClickListener(this);
        }
        c();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i) {
        switch (i) {
            case 1:
                return new AlertDialog.Builder(this).setIcon(17301659).setTitle((int) R.string.golfnavi_title).setMessage((int) R.string.golfnavi_msg).setPositiveButton((int) R.string.golfnavi_market, new et(this)).setNegativeButton((int) R.string.close, new eq(this)).create();
            default:
                return super.onCreateDialog(i);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.c.a();
        this.c.c();
        d.a(findViewById(R.id.search_course_main));
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.c.b();
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }
}
