package com.asai24.golf.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.LinearLayout;
import com.asai24.golf.GolfApplication;
import com.asai24.golf.R;
import com.asai24.golf.c.j;
import com.asai24.golf.c.m;
import com.asai24.golf.d.c;
import com.asai24.golf.d.d;

public class YourGolfDataManagement extends GolfActivity implements View.OnClickListener, Runnable {
    private GolfApplication b;
    /* access modifiers changed from: private */
    public ProgressDialog c;
    /* access modifiers changed from: private */
    public int d;
    private String e;
    private String f;
    private boolean g;
    private Handler h = new dt(this);

    /* access modifiers changed from: private */
    public void d() {
        this.c = new ProgressDialog(this);
        this.c.setIndeterminate(true);
        this.c.setCancelable(false);
        this.c.setMessage(getString(R.string.msg_now_loading));
        this.c.show();
        new Thread(this).start();
    }

    /* access modifiers changed from: private */
    public void d(String str) {
        this.e = str;
        if (this.g) {
            this.f = getString(R.string.success);
        } else {
            this.f = getString(R.string.error);
        }
        showDialog(3);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.yourgolf_data_management_backup_layout /*2131428218*/:
                showDialog(1);
                return;
            case R.id.yourgolf_data_management_restore_layout /*2131428219*/:
                showDialog(2);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.yourgolf_data_management);
        this.b = (GolfApplication) getApplication();
        ((LinearLayout) findViewById(R.id.yourgolf_data_management_backup_layout)).setOnClickListener(this);
        ((LinearLayout) findViewById(R.id.yourgolf_data_management_restore_layout)).setOnClickListener(this);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i) {
        switch (i) {
            case 1:
                return new AlertDialog.Builder(this).setIcon(17301543).setTitle((int) R.string.yourgolf_data_management_dialog_title).setMessage((int) R.string.yourgolf_data_management_backup_dialog).setPositiveButton((int) R.string.yourgolf_data_management_backup, new dv(this)).setNegativeButton((int) R.string.cancel, new du(this)).create();
            case 2:
                return new AlertDialog.Builder(this).setIcon(17301543).setTitle((int) R.string.yourgolf_data_management_dialog_title).setMessage((int) R.string.yourgolf_data_management_restore_dialog).setPositiveButton((int) R.string.yourgolf_data_management_restore, new dx(this)).setNegativeButton((int) R.string.cancel, new dw(this)).create();
            case 3:
                return new AlertDialog.Builder(this).setIcon((int) R.drawable.alert_dialog_icon).setTitle(this.f).setMessage(this.e).setPositiveButton((int) R.string.ok, new dy(this)).create();
            default:
                return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.b.a();
        this.b.c();
        d.a(findViewById(R.id.yourgolf_data_management));
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.b.b();
        super.onPause();
    }

    public void run() {
        String str = "";
        this.g = false;
        switch (this.d) {
            case 1:
                String string = PreferenceManager.getDefaultSharedPreferences(this).getString(getString(R.string.yourgolf_account_username_key), "");
                j jVar = new j(this);
                try {
                    str = jVar.a(string);
                    this.g = jVar.a();
                    break;
                } catch (Exception e2) {
                    c.a(e2);
                    str = getString(R.string.backup_error_unkonwn);
                    break;
                }
            case 2:
                m mVar = new m(this);
                try {
                    String a = mVar.a();
                    this.g = mVar.b();
                    str = a;
                    break;
                } catch (Exception e3) {
                    c.a(e3);
                    str = getString(R.string.restore_error_unkonwn);
                    break;
                }
        }
        Message message = new Message();
        Bundle bundle = new Bundle();
        bundle.putString("msg", str);
        message.setData(bundle);
        this.h.sendMessage(message);
    }
}
