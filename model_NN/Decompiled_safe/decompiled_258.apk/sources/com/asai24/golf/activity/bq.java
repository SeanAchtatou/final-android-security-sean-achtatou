package com.asai24.golf.activity;

import android.content.DialogInterface;
import android.widget.Toast;
import com.asai24.golf.R;

class bq implements DialogInterface.OnClickListener {
    final /* synthetic */ SelectPlayers a;
    private final /* synthetic */ String[] b;

    bq(SelectPlayers selectPlayers, String[] strArr) {
        this.a = selectPlayers;
        this.b = strArr;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        String str;
        Boolean bool;
        String str2 = this.b[i];
        boolean z = false;
        if (str2.length() > 10) {
            str2 = str2.substring(0, 10);
            z = true;
        }
        while (true) {
            Boolean bool2 = z;
            str = str2;
            bool = bool2;
            if (str.getBytes().length <= 18) {
                break;
            }
            str2 = str.substring(0, str.length() - 1);
            z = true;
        }
        if (bool.booleanValue()) {
            Toast.makeText(this.a, this.a.e.getString(R.string.max_length_warning), 1).show();
        }
        this.a.o.add(Long.valueOf(this.a.g.a(str)));
        this.a.d();
    }
}
