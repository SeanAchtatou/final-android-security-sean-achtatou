package com.asai24.golf.activity;

import android.os.Bundle;
import android.view.KeyEvent;
import android.webkit.WebView;
import android.widget.ProgressBar;
import com.asai24.golf.R;

public class BrowserActivity extends GolfActivity {
    private WebView b;
    /* access modifiers changed from: private */
    public ProgressBar c;

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        if (keyEvent.getAction() == 0) {
            switch (keyEvent.getKeyCode()) {
                case 4:
                    if (this.b.canGoBack()) {
                        this.b.goBack();
                        return true;
                    }
                    break;
            }
        }
        return super.dispatchKeyEvent(keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.browser);
        this.b = (WebView) findViewById(R.id.webview);
        this.c = (ProgressBar) findViewById(R.id.progress);
        String string = getIntent().getExtras().getString("URL");
        if (string == null || string.equals("")) {
            finish();
            return;
        }
        this.b.loadUrl(string);
        this.b.getSettings().setBuiltInZoomControls(true);
        this.b.getSettings().setUseWideViewPort(true);
        this.b.getSettings().setJavaScriptEnabled(true);
        this.b.setWebChromeClient(new cy(this));
        this.b.setWebViewClient(new cz(this));
    }
}
