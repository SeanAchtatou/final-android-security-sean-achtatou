package com.asai24.golf.activity;

import android.os.AsyncTask;

public class ds extends AsyncTask {
    final /* synthetic */ PlayHistories a;

    protected ds(PlayHistories playHistories) {
        this.a = playHistories;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Integer doInBackground(String... strArr) {
        if (!this.a.a()) {
            return -1;
        }
        this.a.u.a();
        return 0;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(Integer num) {
        String b;
        if (num.intValue() == 0 && (b = this.a.u.b()) != null && !b.trim().equals("")) {
            this.a.v.setText(b);
            this.a.v.setVisibility(0);
        }
    }
}
