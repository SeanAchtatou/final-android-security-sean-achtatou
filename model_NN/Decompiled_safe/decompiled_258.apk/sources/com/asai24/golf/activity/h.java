package com.asai24.golf.activity;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.asai24.golf.R;

class h implements DialogInterface.OnClickListener {
    final /* synthetic */ YourGolfAccountUpdate a;

    h(YourGolfAccountUpdate yourGolfAccountUpdate) {
        this.a = yourGolfAccountUpdate;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        if (this.a.y == 1) {
            SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(this.a).edit();
            String editable = this.a.q.getText().toString();
            edit.putString(this.a.b.getString(R.string.yourgolf_account_username_key), editable);
            edit.putString(this.a.b.getString(R.string.yourgolf_account_username_confirm_key), editable);
            edit.putString(this.a.b.getString(R.string.yourgolf_account_auth_token_key), "");
            edit.putBoolean(this.a.b.getString(R.string.yourgolf_account_check_activation_key), true);
            edit.commit();
            this.a.setResult(-1);
            this.a.finish();
        }
    }
}
