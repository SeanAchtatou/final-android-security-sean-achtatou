package com.asai24.golf.activity;

import android.content.res.Resources;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.asai24.golf.R;
import com.asai24.golf.d.d;
import com.asai24.graph.BarGraphView;
import com.asai24.graph.a;
import java.lang.reflect.Array;
import java.util.ArrayList;

public class ScoreAnalysisGraph extends GolfActivity {
    private String[] b;

    private void a(ArrayList arrayList) {
        String packageName = getPackageName();
        Resources resources = getResources();
        for (int i = 1; i <= 4; i++) {
            String trim = this.b[i - 1].trim();
            if (trim == null || trim.equals("")) {
                ((LinearLayout) findViewById(resources.getIdentifier("player" + i + "_layout", "id", packageName))).setVisibility(8);
            } else {
                String[] split = ((String) arrayList.get(i - 1)).split(",");
                SpannableString spannableString = new SpannableString(trim);
                spannableString.setSpan(new UnderlineSpan(), 0, spannableString.length(), 0);
                ((TextView) findViewById(resources.getIdentifier("player_label" + i, "id", packageName))).setText(spannableString);
                ((TextView) findViewById(resources.getIdentifier("player_name" + i, "id", packageName))).setText(trim);
                ((TextView) findViewById(resources.getIdentifier("total_stroke" + i, "id", packageName))).setText(split[0]);
                ((TextView) findViewById(resources.getIdentifier("total_putt" + i, "id", packageName))).setText(split[1]);
            }
        }
        String str = (String) arrayList.get(0);
        for (int i2 = 1; i2 < arrayList.size(); i2++) {
            str = String.valueOf(str) + "," + ((String) arrayList.get(i2));
        }
        a(str.split(","));
    }

    private void a(String[] strArr) {
        int[] iArr = new int[strArr.length];
        for (int i = 0; i < strArr.length; i++) {
            iArr[i] = Integer.parseInt(strArr[i]);
        }
        int[] iArr2 = {-16776961, -16711936, -16776961, -16711936, -16776961, -16711936, -16776961, -16711936};
        ArrayList arrayList = new ArrayList(0);
        int i2 = 0;
        for (int i3 = 0; i3 < strArr.length; i3++) {
            a aVar = new a();
            aVar.a(iArr[i3]);
            aVar.b(iArr2[i3]);
            arrayList.add(aVar);
            i2 += iArr[i3];
        }
        int dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.total_bar_graph_size);
        BarGraphView barGraphView = (BarGraphView) findViewById(R.id.total_bar_graph);
        barGraphView.a(dimensionPixelSize, dimensionPixelSize, arrayList.size());
        barGraphView.a(arrayList, 1);
        barGraphView.invalidate();
    }

    private void b(ArrayList arrayList) {
        String packageName = getPackageName();
        Resources resources = getResources();
        for (int i = 1; i <= this.b.length; i++) {
            String trim = this.b[i - 1].trim();
            if (trim == null || trim.equals("")) {
                ((TextView) findViewById(resources.getIdentifier("score_eagle_under_player_name" + i, "id", packageName))).setVisibility(4);
                ((TextView) findViewById(resources.getIdentifier("score_eagle_under_player" + i + "_name", "id", packageName))).setVisibility(8);
                ((TextView) findViewById(resources.getIdentifier("score_birdie_player_name" + i, "id", packageName))).setVisibility(4);
                ((TextView) findViewById(resources.getIdentifier("score_birdie_player" + i + "_name", "id", packageName))).setVisibility(8);
                ((TextView) findViewById(resources.getIdentifier("score_par_player_name" + i, "id", packageName))).setVisibility(4);
                ((TextView) findViewById(resources.getIdentifier("score_par_player" + i + "_name", "id", packageName))).setVisibility(8);
                ((TextView) findViewById(resources.getIdentifier("score_bogey_player_name" + i, "id", packageName))).setVisibility(4);
                ((TextView) findViewById(resources.getIdentifier("score_bogey_player" + i + "_name", "id", packageName))).setVisibility(8);
                ((TextView) findViewById(resources.getIdentifier("score_double_bogey_player_name" + i, "id", packageName))).setVisibility(4);
                ((TextView) findViewById(resources.getIdentifier("score_double_bogey_player" + i + "_name", "id", packageName))).setVisibility(8);
                ((TextView) findViewById(resources.getIdentifier("score_triple_bogey_player_name" + i, "id", packageName))).setVisibility(4);
                ((TextView) findViewById(resources.getIdentifier("score_triple_bogey_player" + i + "_name", "id", packageName))).setVisibility(8);
                ((TextView) findViewById(resources.getIdentifier("score_eagle_under_player" + i + "_value", "id", packageName))).setVisibility(8);
                ((TextView) findViewById(resources.getIdentifier("score_birdie_player" + i + "_value", "id", packageName))).setVisibility(8);
                ((TextView) findViewById(resources.getIdentifier("score_par_player" + i + "_value", "id", packageName))).setVisibility(8);
                ((TextView) findViewById(resources.getIdentifier("score_bogey_player" + i + "_value", "id", packageName))).setVisibility(8);
                ((TextView) findViewById(resources.getIdentifier("score_double_bogey_player" + i + "_value", "id", packageName))).setVisibility(8);
                ((TextView) findViewById(resources.getIdentifier("score_triple_bogey_player" + i + "_value", "id", packageName))).setVisibility(8);
            } else {
                String[] split = ((String) arrayList.get(i - 1)).split(",");
                ((TextView) findViewById(resources.getIdentifier("score_eagle_under_player_name" + i, "id", packageName))).setText(trim);
                ((TextView) findViewById(resources.getIdentifier("score_eagle_under_player" + i + "_name", "id", packageName))).setText(" " + trim);
                ((TextView) findViewById(resources.getIdentifier("score_birdie_player_name" + i, "id", packageName))).setText(trim);
                ((TextView) findViewById(resources.getIdentifier("score_birdie_player" + i + "_name", "id", packageName))).setText(" " + trim);
                ((TextView) findViewById(resources.getIdentifier("score_par_player_name" + i, "id", packageName))).setText(trim);
                ((TextView) findViewById(resources.getIdentifier("score_par_player" + i + "_name", "id", packageName))).setText(" " + trim);
                ((TextView) findViewById(resources.getIdentifier("score_bogey_player_name" + i, "id", packageName))).setText(trim);
                ((TextView) findViewById(resources.getIdentifier("score_bogey_player" + i + "_name", "id", packageName))).setText(" " + trim);
                ((TextView) findViewById(resources.getIdentifier("score_double_bogey_player_name" + i, "id", packageName))).setText(trim);
                ((TextView) findViewById(resources.getIdentifier("score_double_bogey_player" + i + "_name", "id", packageName))).setText(" " + trim);
                ((TextView) findViewById(resources.getIdentifier("score_triple_bogey_player_name" + i, "id", packageName))).setText(trim);
                ((TextView) findViewById(resources.getIdentifier("score_triple_bogey_player" + i + "_name", "id", packageName))).setText(" " + trim);
                ((TextView) findViewById(resources.getIdentifier("score_eagle_under_player" + i + "_value", "id", packageName))).setText(String.valueOf(split[0]) + "%");
                ((TextView) findViewById(resources.getIdentifier("score_birdie_player" + i + "_value", "id", packageName))).setText(String.valueOf(split[1]) + "%");
                ((TextView) findViewById(resources.getIdentifier("score_par_player" + i + "_value", "id", packageName))).setText(String.valueOf(split[2]) + "%");
                ((TextView) findViewById(resources.getIdentifier("score_bogey_player" + i + "_value", "id", packageName))).setText(String.valueOf(split[3]) + "%");
                ((TextView) findViewById(resources.getIdentifier("score_double_bogey_player" + i + "_value", "id", packageName))).setText(String.valueOf(split[4]) + "%");
                ((TextView) findViewById(resources.getIdentifier("score_triple_bogey_player" + i + "_value", "id", packageName))).setText(String.valueOf(split[5]) + "%");
            }
        }
        c(arrayList);
    }

    private void c(ArrayList arrayList) {
        String packageName = getPackageName();
        Resources resources = getResources();
        int size = arrayList.size();
        int length = ((String) arrayList.get(0)).split(",").length;
        int[] iArr = {-16776961, -16711936, -65281, -65536};
        int[][] iArr2 = (int[][]) Array.newInstance(Integer.TYPE, length, size);
        for (int i = 0; i < 4; i++) {
            String[] split = ((String) arrayList.get(i)).split(",");
            for (int i2 = 0; i2 < length; i2++) {
                iArr2[i2][i] = Integer.parseInt(split[i2].replace("%", ""));
            }
        }
        String[] strArr = {"eagle_under", "birdie", "par", "bogey", "double_bogey", "triple_bogey"};
        int dimensionPixelSize = resources.getDimensionPixelSize(R.dimen.total_bar_graph_size);
        for (int i3 = 0; i3 < length; i3++) {
            ArrayList arrayList2 = new ArrayList(0);
            int i4 = 0;
            for (int i5 = 0; i5 < 4; i5++) {
                a aVar = new a();
                aVar.a(iArr2[i3][i5]);
                aVar.b(iArr[i5]);
                arrayList2.add(aVar);
                i4 += iArr2[i3][i5];
            }
            BarGraphView barGraphView = (BarGraphView) findViewById(resources.getIdentifier("score_" + strArr[i3] + "_bar_graph", "id", packageName));
            barGraphView.a(dimensionPixelSize, dimensionPixelSize, 4);
            barGraphView.a(arrayList2, 3);
            barGraphView.invalidate();
        }
    }

    private void d(ArrayList arrayList) {
        String packageName = getPackageName();
        Resources resources = getResources();
        for (int i = 1; i <= this.b.length; i++) {
            String trim = this.b[i - 1].trim();
            if (trim == null || trim.equals("")) {
                ((TextView) findViewById(resources.getIdentifier("stroke_par3_player_name" + i, "id", packageName))).setVisibility(4);
                ((TextView) findViewById(resources.getIdentifier("stroke_par3_player" + i + "_name", "id", packageName))).setVisibility(8);
                ((TextView) findViewById(resources.getIdentifier("stroke_par4_player_name" + i, "id", packageName))).setVisibility(4);
                ((TextView) findViewById(resources.getIdentifier("stroke_par4_player" + i + "_name", "id", packageName))).setVisibility(8);
                ((TextView) findViewById(resources.getIdentifier("stroke_par5_player_name" + i, "id", packageName))).setVisibility(4);
                ((TextView) findViewById(resources.getIdentifier("stroke_par5_player" + i + "_name", "id", packageName))).setVisibility(8);
                ((TextView) findViewById(resources.getIdentifier("stroke_par3_player" + i + "_value", "id", packageName))).setVisibility(8);
                ((TextView) findViewById(resources.getIdentifier("stroke_par4_player" + i + "_value", "id", packageName))).setVisibility(8);
                ((TextView) findViewById(resources.getIdentifier("stroke_par5_player" + i + "_value", "id", packageName))).setVisibility(8);
            } else {
                String[] split = ((String) arrayList.get(i - 1)).split(",");
                ((TextView) findViewById(resources.getIdentifier("stroke_par3_player_name" + i, "id", packageName))).setText(trim);
                ((TextView) findViewById(resources.getIdentifier("stroke_par3_player" + i + "_name", "id", packageName))).setText(" " + trim);
                ((TextView) findViewById(resources.getIdentifier("stroke_par4_player_name" + i, "id", packageName))).setText(trim);
                ((TextView) findViewById(resources.getIdentifier("stroke_par4_player" + i + "_name", "id", packageName))).setText(" " + trim);
                ((TextView) findViewById(resources.getIdentifier("stroke_par5_player_name" + i, "id", packageName))).setText(trim);
                ((TextView) findViewById(resources.getIdentifier("stroke_par5_player" + i + "_name", "id", packageName))).setText(" " + trim);
                ((TextView) findViewById(resources.getIdentifier("stroke_par3_player" + i + "_value", "id", packageName))).setText(split[0]);
                ((TextView) findViewById(resources.getIdentifier("stroke_par4_player" + i + "_value", "id", packageName))).setText(split[1]);
                ((TextView) findViewById(resources.getIdentifier("stroke_par5_player" + i + "_value", "id", packageName))).setText(split[2]);
            }
        }
        e(arrayList);
    }

    private void e(ArrayList arrayList) {
        String packageName = getPackageName();
        Resources resources = getResources();
        int size = arrayList.size();
        int length = ((String) arrayList.get(0)).split(",").length;
        int[] iArr = {-16776961, -16711936, -65281, -65536};
        int[][] iArr2 = (int[][]) Array.newInstance(Integer.TYPE, length, size);
        for (int i = 0; i < 4; i++) {
            String[] split = ((String) arrayList.get(i)).split(",");
            for (int i2 = 0; i2 < length; i2++) {
                iArr2[i2][i] = (int) (Float.parseFloat(split[i2]) * 10.0f);
            }
        }
        int dimensionPixelSize = resources.getDimensionPixelSize(R.dimen.total_bar_graph_size);
        for (int i3 = 0; i3 < length; i3++) {
            ArrayList arrayList2 = new ArrayList(0);
            int i4 = 0;
            for (int i5 = 0; i5 < 4; i5++) {
                a aVar = new a();
                aVar.a(iArr2[i3][i5]);
                aVar.b(iArr[i5]);
                arrayList2.add(aVar);
                i4 += iArr2[i3][i5];
            }
            BarGraphView barGraphView = (BarGraphView) findViewById(resources.getIdentifier("stroke_par" + (i3 + 3) + "_bar_graph", "id", packageName));
            barGraphView.a(dimensionPixelSize, dimensionPixelSize, 4);
            barGraphView.a(arrayList2, 2);
            barGraphView.invalidate();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Bundle extras = getIntent().getExtras();
        int i = extras.getInt(getString(R.string.intent_graph_mode), -1);
        ArrayList<String> stringArrayList = extras.getStringArrayList(getString(R.string.intent_graph_value));
        this.b = extras.getStringArray(getString(R.string.intent_graph_players));
        switch (i) {
            case 1:
                setContentView((int) R.layout.score_analysis_graph_total);
                a(stringArrayList);
                break;
            case 2:
                setContentView((int) R.layout.score_analysis_graph_score);
                b(stringArrayList);
                break;
            case 3:
                setContentView((int) R.layout.score_analysis_graph_stroke);
                d(stringArrayList);
                break;
            default:
                finish();
                break;
        }
        c();
    }

    public void onDestroy() {
        d.a(findViewById(R.id.score_analysis_graph));
        super.onDestroy();
        System.gc();
    }
}
