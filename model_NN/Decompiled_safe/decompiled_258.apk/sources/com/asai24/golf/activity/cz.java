package com.asai24.golf.activity;

import android.webkit.CookieManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;

class cz extends WebViewClient {
    String a = "";
    final /* synthetic */ BrowserActivity b;

    cz(BrowserActivity browserActivity) {
        this.b = browserActivity;
    }

    public void onLoadResource(WebView webView, String str) {
        this.a = CookieManager.getInstance().getCookie(str);
    }

    public void onPageFinished(WebView webView, String str) {
        CookieManager.getInstance().setCookie(str, this.a);
    }
}
