package com.asai24.golf.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import com.asai24.golf.R;

class aa implements DialogInterface.OnClickListener {
    final /* synthetic */ GolfSettingsDetail a;

    aa(GolfSettingsDetail golfSettingsDetail) {
        this.a = golfSettingsDetail;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        this.a.k = new ProgressDialog(this.a);
        this.a.k.setIndeterminate(true);
        this.a.k.setCancelable(false);
        this.a.k.setMessage(this.a.getString(R.string.msg_now_loading));
        this.a.k.show();
        new Thread(this.a).start();
    }
}
