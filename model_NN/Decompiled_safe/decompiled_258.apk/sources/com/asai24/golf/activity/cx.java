package com.asai24.golf.activity;

import android.app.ProgressDialog;
import com.asai24.golf.R;

class cx implements Runnable {
    final /* synthetic */ ej a;

    cx(ej ejVar) {
        this.a = ejVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.asai24.golf.activity.SearchCourse.a(com.asai24.golf.activity.SearchCourse, boolean):void
     arg types: [com.asai24.golf.activity.SearchCourse, int]
     candidates:
      com.asai24.golf.activity.SearchCourse.a(com.asai24.golf.activity.SearchCourse, long):void
      com.asai24.golf.activity.SearchCourse.a(com.asai24.golf.activity.SearchCourse, android.app.ProgressDialog):void
      com.asai24.golf.activity.SearchCourse.a(com.asai24.golf.activity.SearchCourse, android.location.Location):void
      com.asai24.golf.activity.SearchCourse.a(com.asai24.golf.activity.SearchCourse, com.asai24.golf.c.f):void
      com.asai24.golf.activity.SearchCourse.a(com.asai24.golf.activity.SearchCourse, com.asai24.golf.e.d):void
      com.asai24.golf.activity.SearchCourse.a(com.asai24.golf.activity.SearchCourse, java.util.HashMap):void
      com.asai24.golf.activity.SearchCourse.a(com.asai24.golf.activity.SearchCourse, boolean):void */
    public void run() {
        if (this.a.a.t >= 30000) {
            if (this.a.a.v != null && this.a.a.v.isShowing()) {
                this.a.a.v.dismiss();
                this.a.a.v = (ProgressDialog) null;
            }
            this.a.a.h();
            if (this.a.a.u) {
                this.a.a.c(this.a.a.getString(R.string.location_undetermined_message));
            }
            this.a.a.u = false;
        }
        SearchCourse a2 = this.a.a;
        a2.t = a2.t + 1000;
    }
}
