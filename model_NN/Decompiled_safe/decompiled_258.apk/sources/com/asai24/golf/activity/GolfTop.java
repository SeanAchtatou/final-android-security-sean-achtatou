package com.asai24.golf.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.Resources;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.asai24.golf.GolfApplication;
import com.asai24.golf.R;
import com.asai24.golf.d.c;
import com.asai24.golf.d.d;

public class GolfTop extends GolfActivity implements View.OnClickListener {
    private static String b = "GolfTop";
    private static String c;
    private Button d;
    private Button e;
    private Button f;
    private Button g;
    private TextView h;
    private GolfApplication i;
    private Resources j;

    private void d() {
        if (!((LocationManager) getSystemService("location")).isProviderEnabled("gps")) {
            showDialog(1);
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.about /*2131427328*/:
                this.i.a("/about");
                this.i.b("/about");
                startActivity(new Intent(this, About.class));
                return;
            case R.id.play /*2131427453*/:
                this.i.a("/search_main");
                this.i.b("/search_main");
                startActivity(new Intent(this, SearchCourseMain.class));
                return;
            case R.id.play_histories /*2131427455*/:
                this.i.a("/history");
                this.i.b("/history");
                startActivity(new Intent(this, PlayHistories.class));
                return;
            case R.id.analysis /*2131427456*/:
                this.i.a("/total_analysis");
                this.i.b("/total_analysis");
                startActivity(new Intent(this, TotalAnalysis.class));
                return;
            case R.id.information /*2131427458*/:
                a(getString(R.string.facebook_url));
                return;
            default:
                return;
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        c.a(getApplicationContext());
        this.i = (GolfApplication) getApplication();
        this.i.a("/top");
        this.i.b("/top");
        setContentView((int) R.layout.main);
        this.j = getResources();
        this.d = (Button) findViewById(R.id.play);
        this.e = (Button) findViewById(R.id.play_histories);
        this.f = (Button) findViewById(R.id.analysis);
        this.g = (Button) findViewById(R.id.about);
        this.h = (TextView) findViewById(R.id.information);
        this.d.setOnClickListener(this);
        this.e.setOnClickListener(this);
        this.f.setOnClickListener(this);
        this.g.setOnClickListener(this);
        this.h.setOnClickListener(this);
        if (c != "CALLED") {
            c = "CALLED";
            d();
        }
        c();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i2) {
        switch (i2) {
            case 1:
                return new AlertDialog.Builder(this).setIcon((int) R.drawable.ic_dialog_menu_generic).setTitle((int) R.string.check_gps_title).setMessage((int) R.string.check_gps_message).setPositiveButton((int) R.string.yes, new cl(this)).setNegativeButton((int) R.string.no, new ck(this)).create();
            default:
                return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.i.a();
        this.i.c();
        d.a(findViewById(R.id.main));
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        if (isFinishing()) {
            c = null;
        }
        this.i.b();
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }
}
