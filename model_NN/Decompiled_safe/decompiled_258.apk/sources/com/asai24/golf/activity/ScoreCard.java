package com.asai24.golf.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.asai24.golf.GolfApplication;
import com.asai24.golf.R;
import com.asai24.golf.a.b;
import com.asai24.golf.a.d;
import com.asai24.golf.a.g;
import com.asai24.golf.a.p;
import com.asai24.golf.a.y;
import com.asai24.golf.e.a;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class ScoreCard extends GolfActivity implements View.OnClickListener {
    private boolean A;
    private boolean B;
    private int b;
    private int c;
    private ArrayList d;
    private ArrayList e;
    private ArrayList f;
    private ArrayList g;
    private ArrayList h;
    private ArrayList i;
    private ArrayList j;
    private final int k = 11;
    private long[] l;
    private long m;
    private long n;
    private Boolean o;
    private b p;
    private GolfApplication q;
    private Resources r;
    private d s;
    private Button t;
    private TextView u;
    private TextView v;
    private Button w;
    private TextView x;
    private boolean y;
    private boolean z;

    private float a(String str, float f2, int i2) {
        switch (i2) {
            case 1:
                return f2 - ((float) ((str.length() - 1) * 4));
            case 5:
                return f2 - ((float) ((str.length() - 1) * 4));
            case 9:
                return f2 - ((float) ((str.length() - 1) * 5));
            case 10:
                return f2 - ((float) ((str.length() - 1) * 5));
            case 17:
                return f2 - ((float) ((str.length() - 1) * 5));
            default:
                return f2;
        }
    }

    private int a(long j2) {
        g m2 = this.p.m(j2);
        a aVar = new a(this);
        int i2 = 0;
        for (int i3 = 0; i3 < m2.getCount(); i3++) {
            m2.moveToPosition(i3);
            if (aVar.a(m2.e()).equals("pt")) {
                i2++;
            }
        }
        m2.close();
        return i2;
    }

    private TextView a(int i2, int i3, int i4) {
        int i5 = 0;
        switch (i2) {
            case 0:
                i5 = this.r.getIdentifier("player" + i3 + "_name", "id", getPackageName());
                break;
            case 1:
                i5 = this.r.getIdentifier("yard" + i4, "id", getPackageName());
                break;
            case 2:
                if (i4 != -1) {
                    i5 = R.id.yard_out;
                    break;
                } else {
                    i5 = R.id.yard_out2;
                    break;
                }
            case 3:
                i5 = R.id.yard_in;
                break;
            case 4:
                i5 = R.id.yard_total;
                break;
            case 5:
                i5 = this.r.getIdentifier("par" + i4, "id", getPackageName());
                break;
            case 6:
                if (i4 != -1) {
                    i5 = R.id.par_out;
                    break;
                } else {
                    i5 = R.id.par_out2;
                    break;
                }
            case 7:
                i5 = R.id.par_in;
                break;
            case 8:
                i5 = R.id.par_total;
                break;
            case 9:
                i5 = this.r.getIdentifier("player" + i3 + "_s_" + i4, "id", getPackageName());
                break;
            case 10:
                i5 = this.r.getIdentifier("player" + i3 + "_p_" + i4, "id", getPackageName());
                break;
            case 11:
                if (i4 != -1) {
                    i5 = this.r.getIdentifier("player" + i3 + "_s_out", "id", getPackageName());
                    break;
                } else {
                    i5 = this.r.getIdentifier("player" + i3 + "_s_out2", "id", getPackageName());
                    break;
                }
            case 12:
                if (i4 != -1) {
                    i5 = this.r.getIdentifier("player" + i3 + "_p_out", "id", getPackageName());
                    break;
                } else {
                    i5 = this.r.getIdentifier("player" + i3 + "_p_out2", "id", getPackageName());
                    break;
                }
            case 13:
                i5 = this.r.getIdentifier("player" + i3 + "_s_in", "id", getPackageName());
                break;
            case 14:
                i5 = this.r.getIdentifier("player" + i3 + "_p_in", "id", getPackageName());
                break;
            case 15:
                i5 = this.r.getIdentifier("player" + i3 + "_s_total", "id", getPackageName());
                break;
            case 16:
                i5 = this.r.getIdentifier("player" + i3 + "_p_total", "id", getPackageName());
                break;
            case 17:
                i5 = this.r.getIdentifier("extra" + i3 + "_" + i4, "id", getPackageName());
                break;
            case 18:
                if (i4 != -1) {
                    i5 = this.r.getIdentifier("extra" + i3 + "_out", "id", getPackageName());
                    break;
                } else {
                    i5 = this.r.getIdentifier("extra" + i3 + "_out2", "id", getPackageName());
                    break;
                }
            case 19:
                i5 = this.r.getIdentifier("extra" + i3 + "_in", "id", getPackageName());
                break;
            case 20:
                i5 = this.r.getIdentifier("extra" + i3 + "_total", "id", getPackageName());
                break;
        }
        TextView textView = (TextView) findViewById(i5);
        if (i2 == 9 || i2 == 10) {
            textView.setOnClickListener(this);
        }
        return textView;
    }

    private void a(long j2, int i2) {
        this.s = this.p.b(j2);
        String c2 = this.s.getCount() == 0 ? "  " : this.s.c();
        this.s.close();
        String str = c2.length() > 11 ? ((Object) c2.subSequence(0, 11)) + ".." : c2;
        if (i2 >= 0 && i2 <= 3) {
            ((TextView) this.f.get(i2)).setText(str);
            this.j.add(c2);
        }
    }

    private void a(long j2, boolean z2) {
        Intent intent = new Intent(this, ScoreEntryWithMap.class);
        intent.putExtra("playing_hole_id", j2);
        intent.putExtra("SingleFlg", z2);
        setResult(-1, intent);
        finish();
    }

    private void a(y yVar, int i2) {
        int i3;
        int i4;
        int i5;
        int i6 = 0;
        int i7 = 0;
        int i8 = 0;
        int i9 = 0;
        int i10 = 0;
        int i11 = 0;
        int i12 = 0;
        int i13 = 0;
        int i14 = 0;
        int i15 = 0;
        int i16 = 0;
        while (i15 < yVar.getCount()) {
            yVar.moveToPosition(i15);
            int c2 = yVar.c();
            int c3 = c2 < 10 ? yVar.c() - 1 : c2;
            if (i2 == 0) {
                int f2 = yVar.f();
                int b2 = !this.y ? com.asai24.golf.f.a.b(f2) : f2;
                ((TextView) this.d.get(c3)).setText(new StringBuilder().append(b2).toString());
                ((TextView) this.e.get(c3)).setText(new StringBuilder().append(yVar.d()).toString());
                i16 += yVar.d();
                i6 += b2;
            }
            int b3 = yVar.b();
            int a = a(yVar.e());
            if (b3 == 0) {
                ((TextView) ((ArrayList) this.g.get(i2)).get(c3)).setText(" ");
            } else {
                ((TextView) ((ArrayList) this.g.get(i2)).get(c3)).setText(new StringBuilder().append(b3).toString());
                if (b3 < yVar.d()) {
                    ((TextView) ((ArrayList) this.g.get(i2)).get(c3)).setTextColor(this.r.getColor(R.color.scorecard_text_blue));
                } else if (b3 == yVar.d()) {
                    ((TextView) ((ArrayList) this.g.get(i2)).get(c3)).setTextColor(-16777216);
                } else {
                    ((TextView) ((ArrayList) this.g.get(i2)).get(c3)).setTextColor(this.r.getColor(R.color.scorecard_text_pink));
                }
                ArrayList arrayList = new ArrayList();
                arrayList.add(Integer.valueOf(i2));
                arrayList.add(Long.valueOf(yVar.a()));
                ((TextView) ((ArrayList) this.g.get(i2)).get(c3)).setTag(arrayList);
                ((TextView) ((ArrayList) this.h.get(i2)).get(c3)).setTag(arrayList);
            }
            if (b3 != 0) {
                ((TextView) ((ArrayList) this.h.get(i2)).get(c3)).setText(new StringBuilder().append(a).toString());
            } else {
                ((TextView) ((ArrayList) this.h.get(i2)).get(c3)).setText(" ");
            }
            i7 += b3;
            i14 += a;
            if (this.B) {
                int g2 = yVar.g();
                ((TextView) ((ArrayList) this.i.get(i2)).get(c3)).setText(new StringBuilder().append(g2).toString());
                i3 = i9 + g2;
            } else {
                i3 = i9;
            }
            if (c3 < 10) {
                if (i2 == 0) {
                    i4 = i16;
                    i5 = i6;
                } else {
                    i4 = i11;
                    i5 = i12;
                }
                if (this.B) {
                    i8 = i3;
                    i11 = i4;
                    i12 = i5;
                    i13 = i14;
                    i10 = i7;
                } else {
                    i11 = i4;
                    i12 = i5;
                    i13 = i14;
                    i10 = i7;
                }
            }
            i15++;
            i9 = i3;
        }
        if (i2 == 0) {
            if (!this.o.booleanValue()) {
                ((TextView) this.d.get(19)).setText(new StringBuilder().append(i6 - i12).toString());
                ((TextView) this.d.get(20)).setText(new StringBuilder().append(i12).toString());
                ((TextView) this.e.get(19)).setText(new StringBuilder().append(i16 - i11).toString());
                ((TextView) this.e.get(20)).setText(new StringBuilder().append(i11).toString());
            }
            ((TextView) this.d.get(9)).setText(new StringBuilder().append(i12).toString());
            ((TextView) this.d.get(this.b - 1)).setText(new StringBuilder().append(i6).toString());
            ((TextView) this.e.get(9)).setText(new StringBuilder().append(i11).toString());
            ((TextView) this.e.get(this.b - 1)).setText(new StringBuilder().append(i16).toString());
        }
        if (!this.o.booleanValue()) {
            ((TextView) ((ArrayList) this.g.get(i2)).get(19)).setText(new StringBuilder().append(i7 - i10).toString());
            ((TextView) ((ArrayList) this.g.get(i2)).get(20)).setText(new StringBuilder().append(i10).toString());
            ((TextView) ((ArrayList) this.h.get(i2)).get(19)).setText(new StringBuilder().append(i14 - i13).toString());
            ((TextView) ((ArrayList) this.h.get(i2)).get(20)).setText(new StringBuilder().append(i13).toString());
            if (this.B) {
                ((TextView) ((ArrayList) this.i.get(i2)).get(19)).setText(new StringBuilder().append(i9 - i8).toString());
                ((TextView) ((ArrayList) this.i.get(i2)).get(20)).setText(new StringBuilder().append(i8).toString());
            }
        }
        ((TextView) ((ArrayList) this.g.get(i2)).get(9)).setText(new StringBuilder().append(i10).toString());
        ((TextView) ((ArrayList) this.g.get(i2)).get(this.b - 1)).setText(new StringBuilder().append(i7).toString());
        ((TextView) ((ArrayList) this.h.get(i2)).get(9)).setText(new StringBuilder().append(i13).toString());
        ((TextView) ((ArrayList) this.h.get(i2)).get(this.b - 1)).setText(new StringBuilder().append(i14).toString());
        if (this.B) {
            ((TextView) ((ArrayList) this.i.get(i2)).get(9)).setText(new StringBuilder().append(i8).toString());
            ((TextView) ((ArrayList) this.i.get(i2)).get(this.b - 1)).setText(new StringBuilder().append(i9).toString());
        }
    }

    private void b(y yVar, int i2) {
        int i3;
        int i4;
        int i5;
        int i6;
        int[] iArr = new int[7];
        iArr[0] = 6;
        iArr[1] = 5;
        iArr[2] = 4;
        iArr[3] = 3;
        iArr[4] = 2;
        iArr[5] = 1;
        int[] iArr2 = new int[6];
        iArr2[0] = 8;
        iArr2[1] = 5;
        iArr2[2] = 2;
        iArr2[4] = -1;
        iArr2[5] = -3;
        int i7 = 0;
        int i8 = 0;
        int i9 = 0;
        int i10 = 0;
        int i11 = 0;
        int i12 = 0;
        int i13 = 0;
        int i14 = 0;
        int i15 = 0;
        while (i14 < yVar.getCount()) {
            yVar.moveToPosition(i14);
            int c2 = yVar.c();
            int c3 = c2 < 10 ? yVar.c() - 1 : c2;
            if (i2 == 0) {
                int f2 = yVar.f();
                int b2 = !this.y ? com.asai24.golf.f.a.b(f2) : f2;
                ((TextView) this.d.get(c3)).setText(new StringBuilder().append(b2).toString());
                ((TextView) this.e.get(c3)).setText(new StringBuilder().append(yVar.d()).toString());
                i15 += yVar.d();
                i7 += b2;
            }
            int b3 = yVar.b();
            if (b3 <= 0) {
                i3 = b3;
            } else if (this.A) {
                int b4 = (yVar.b() - yVar.d()) + 3;
                i3 = b4 < 0 ? 8 : b4 > 5 ? -3 : iArr2[b4];
            } else {
                int b5 = (yVar.b() - yVar.d()) + 4;
                i3 = b5 < 0 ? 6 : b5 > 6 ? 0 : iArr[b5];
            }
            if (b3 == 0) {
                ((TextView) ((ArrayList) this.g.get(i2)).get(c3)).setText(" ");
            } else {
                ((TextView) ((ArrayList) this.g.get(i2)).get(c3)).setText(new StringBuilder().append(i3).toString());
                if (b3 < yVar.d()) {
                    ((TextView) ((ArrayList) this.g.get(i2)).get(c3)).setTextColor(this.r.getColor(R.color.scorecard_text_blue));
                } else if (b3 == yVar.d()) {
                    ((TextView) ((ArrayList) this.g.get(i2)).get(c3)).setTextColor(-16777216);
                } else {
                    ((TextView) ((ArrayList) this.g.get(i2)).get(c3)).setTextColor(this.r.getColor(R.color.scorecard_text_pink));
                }
                ArrayList arrayList = new ArrayList();
                arrayList.add(Integer.valueOf(i2));
                arrayList.add(Long.valueOf(yVar.a()));
                ((TextView) ((ArrayList) this.g.get(i2)).get(c3)).setTag(arrayList);
                ((TextView) ((ArrayList) this.h.get(i2)).get(c3)).setTag(arrayList);
            }
            ((TextView) ((ArrayList) this.h.get(i2)).get(c3)).setText(" ");
            i8 += i3;
            if (this.B) {
                int g2 = yVar.g();
                ((TextView) ((ArrayList) this.i.get(i2)).get(c3)).setText(new StringBuilder().append(g2).toString());
                i4 = i13 + g2;
            } else {
                i4 = i13;
            }
            if (c3 < 10) {
                if (i2 == 0) {
                    i5 = i15;
                    i6 = i7;
                } else {
                    i5 = i11;
                    i6 = i10;
                }
                if (this.B) {
                    i9 = i4;
                    i10 = i6;
                    i11 = i5;
                    i12 = i8;
                } else {
                    i10 = i6;
                    i11 = i5;
                    i12 = i8;
                }
            }
            i14++;
            i13 = i4;
        }
        if (i2 == 0) {
            if (!this.o.booleanValue()) {
                ((TextView) this.d.get(19)).setText(new StringBuilder().append(i7 - i10).toString());
                ((TextView) this.d.get(20)).setText(new StringBuilder().append(i10).toString());
                ((TextView) this.e.get(19)).setText(new StringBuilder().append(i15 - i11).toString());
                ((TextView) this.e.get(20)).setText(new StringBuilder().append(i11).toString());
            }
            ((TextView) this.d.get(9)).setText(new StringBuilder().append(i10).toString());
            ((TextView) this.d.get(this.b - 1)).setText(new StringBuilder().append(i7).toString());
            ((TextView) this.e.get(9)).setText(new StringBuilder().append(i11).toString());
            ((TextView) this.e.get(this.b - 1)).setText(new StringBuilder().append(i15).toString());
        }
        if (!this.o.booleanValue()) {
            ((TextView) ((ArrayList) this.g.get(i2)).get(19)).setText(new StringBuilder().append(i8 - i12).toString());
            ((TextView) ((ArrayList) this.g.get(i2)).get(20)).setText(new StringBuilder().append(i12).toString());
            ((TextView) ((ArrayList) this.h.get(i2)).get(19)).setText("");
            ((TextView) ((ArrayList) this.h.get(i2)).get(20)).setText("");
            if (this.B) {
                ((TextView) ((ArrayList) this.i.get(i2)).get(19)).setText(new StringBuilder().append(i13 - i9).toString());
                ((TextView) ((ArrayList) this.i.get(i2)).get(20)).setText(new StringBuilder().append(i9).toString());
            }
        }
        ((TextView) ((ArrayList) this.g.get(i2)).get(9)).setText(new StringBuilder().append(i12).toString());
        ((TextView) ((ArrayList) this.g.get(i2)).get(this.b - 1)).setText(new StringBuilder().append(i8).toString());
        ((TextView) ((ArrayList) this.h.get(i2)).get(9)).setText("");
        ((TextView) ((ArrayList) this.h.get(i2)).get(this.b - 1)).setText("");
        if (this.B) {
            ((TextView) ((ArrayList) this.i.get(i2)).get(9)).setText(new StringBuilder().append(i9).toString());
            ((TextView) ((ArrayList) this.i.get(i2)).get(this.b - 1)).setText(new StringBuilder().append(i13).toString());
        }
    }

    private void d() {
        this.d = new ArrayList();
        this.e = new ArrayList();
        this.f = new ArrayList();
        this.g = new ArrayList();
        this.h = new ArrayList();
        this.i = new ArrayList();
        for (int i2 = 1; i2 <= 4; i2++) {
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            ArrayList arrayList3 = new ArrayList();
            this.f.add(a(0, i2, -1));
            for (int i3 = 1; i3 <= this.c; i3++) {
                if (i2 == 1) {
                    if (i3 == 10) {
                        this.d.add(a(2, -1, i3));
                        this.e.add(a(6, -1, i3));
                    }
                    this.d.add(a(1, -1, i3));
                    this.e.add(a(5, -1, i3));
                }
                if (i3 == 10) {
                    arrayList.add(a(11, i2, i3));
                    arrayList2.add(a(12, i2, i3));
                    arrayList3.add(a(18, i2, i3));
                }
                arrayList.add(a(9, i2, i3));
                arrayList2.add(a(10, i2, i3));
                arrayList3.add(a(17, i2, i3));
            }
            if (this.o.booleanValue()) {
                arrayList.add(a(11, i2, 10));
                arrayList2.add(a(12, i2, 10));
                arrayList3.add(a(18, i2, 10));
            } else {
                arrayList.add(a(13, i2, -1));
                arrayList2.add(a(14, i2, -1));
                arrayList3.add(a(19, i2, -1));
                arrayList.add(a(11, i2, -1));
                arrayList2.add(a(12, i2, -1));
                arrayList3.add(a(18, i2, -1));
            }
            arrayList.add(a(15, i2, -1));
            arrayList2.add(a(16, i2, -1));
            arrayList3.add(a(20, i2, -1));
            this.g.add(arrayList);
            this.h.add(arrayList2);
            this.i.add(arrayList3);
        }
        if (this.o.booleanValue()) {
            this.d.add(a(2, -1, 10));
            this.e.add(a(6, -1, 10));
        } else {
            this.d.add(a(3, -1, -1));
            this.d.add(a(2, -1, -1));
            this.e.add(a(7, -1, -1));
            this.e.add(a(6, -1, -1));
        }
        this.d.add(a(4, -1, -1));
        this.e.add(a(8, -1, -1));
    }

    private void e() {
        p o2 = this.p.o(this.m);
        this.u.setText(o2.g());
        this.v.setText(new SimpleDateFormat("yyyy-MM-dd").format(new Date(o2.h())));
        o2.close();
        if (this.z) {
            long[] jArr = this.l;
            int length = jArr.length;
            int i2 = 0;
            int i3 = 0;
            while (i2 < length) {
                long j2 = jArr[i2];
                y c2 = this.p.c(this.m, j2, this.n);
                a(j2, i3);
                b(c2, i3);
                c2.close();
                i2++;
                i3++;
            }
            return;
        }
        long[] jArr2 = this.l;
        int length2 = jArr2.length;
        int i4 = 0;
        int i5 = 0;
        while (i4 < length2) {
            long j3 = jArr2[i4];
            y c3 = this.p.c(this.m, j3, this.n);
            a(j3, i5);
            a(c3, i5);
            c3.close();
            i4++;
            i5++;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x00ad A[SYNTHETIC, Splitter:B:21:0x00ad] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00bd A[SYNTHETIC, Splitter:B:30:0x00bd] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00ca A[SYNTHETIC, Splitter:B:37:0x00ca] */
    /* JADX WARNING: Removed duplicated region for block: B:54:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:56:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:18:0x00a8=Splitter:B:18:0x00a8, B:27:0x00b8=Splitter:B:27:0x00b8} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void f() {
        /*
            r6 = this;
            android.content.res.Resources r0 = r6.r
            r1 = 2131230740(0x7f080014, float:1.8077541E38)
            java.lang.String r0 = r0.getString(r1)
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = java.lang.String.valueOf(r0)
            r1.<init>(r2)
            java.lang.String r2 = "yourgolf_score_card.jpg"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            java.io.File r2 = new java.io.File
            r2.<init>(r1)
            com.asai24.golf.d.f.a(r0)     // Catch:{ IOException -> 0x00a1 }
            android.graphics.Bitmap r0 = r6.g()
            r3 = 0
            boolean r4 = r2.exists()     // Catch:{ FileNotFoundException -> 0x00a6, IOException -> 0x00b6, all -> 0x00c6 }
            if (r4 != 0) goto L_0x0032
            r2.createNewFile()     // Catch:{ FileNotFoundException -> 0x00a6, IOException -> 0x00b6, all -> 0x00c6 }
        L_0x0032:
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x00a6, IOException -> 0x00b6, all -> 0x00c6 }
            r2.<init>(r1)     // Catch:{ FileNotFoundException -> 0x00a6, IOException -> 0x00b6, all -> 0x00c6 }
            android.graphics.Bitmap$CompressFormat r3 = android.graphics.Bitmap.CompressFormat.JPEG     // Catch:{ FileNotFoundException -> 0x00e0, IOException -> 0x00dd, all -> 0x00d8 }
            r4 = 90
            r0.compress(r3, r4, r2)     // Catch:{ FileNotFoundException -> 0x00e0, IOException -> 0x00dd, all -> 0x00d8 }
            r0.recycle()     // Catch:{ FileNotFoundException -> 0x00e0, IOException -> 0x00dd, all -> 0x00d8 }
            android.content.Intent r0 = new android.content.Intent     // Catch:{ FileNotFoundException -> 0x00e0, IOException -> 0x00dd, all -> 0x00d8 }
            r0.<init>()     // Catch:{ FileNotFoundException -> 0x00e0, IOException -> 0x00dd, all -> 0x00d8 }
            java.lang.String r3 = "android.intent.action.SEND"
            r0.setAction(r3)     // Catch:{ FileNotFoundException -> 0x00e0, IOException -> 0x00dd, all -> 0x00d8 }
            android.content.res.Resources r3 = r6.r     // Catch:{ FileNotFoundException -> 0x00e0, IOException -> 0x00dd, all -> 0x00d8 }
            r4 = 2131230997(0x7f080115, float:1.8078063E38)
            java.lang.String r3 = r3.getString(r4)     // Catch:{ FileNotFoundException -> 0x00e0, IOException -> 0x00dd, all -> 0x00d8 }
            r0.setType(r3)     // Catch:{ FileNotFoundException -> 0x00e0, IOException -> 0x00dd, all -> 0x00d8 }
            java.lang.String r3 = "android.intent.extra.SUBJECT"
            android.content.res.Resources r4 = r6.r     // Catch:{ FileNotFoundException -> 0x00e0, IOException -> 0x00dd, all -> 0x00d8 }
            r5 = 2131230998(0x7f080116, float:1.8078065E38)
            java.lang.String r4 = r4.getString(r5)     // Catch:{ FileNotFoundException -> 0x00e0, IOException -> 0x00dd, all -> 0x00d8 }
            r0.putExtra(r3, r4)     // Catch:{ FileNotFoundException -> 0x00e0, IOException -> 0x00dd, all -> 0x00d8 }
            java.lang.String r3 = "android.intent.extra.STREAM"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x00e0, IOException -> 0x00dd, all -> 0x00d8 }
            java.lang.String r5 = "file://"
            r4.<init>(r5)     // Catch:{ FileNotFoundException -> 0x00e0, IOException -> 0x00dd, all -> 0x00d8 }
            java.lang.StringBuilder r1 = r4.append(r1)     // Catch:{ FileNotFoundException -> 0x00e0, IOException -> 0x00dd, all -> 0x00d8 }
            java.lang.String r1 = r1.toString()     // Catch:{ FileNotFoundException -> 0x00e0, IOException -> 0x00dd, all -> 0x00d8 }
            android.net.Uri r1 = android.net.Uri.parse(r1)     // Catch:{ FileNotFoundException -> 0x00e0, IOException -> 0x00dd, all -> 0x00d8 }
            r0.putExtra(r3, r1)     // Catch:{ FileNotFoundException -> 0x00e0, IOException -> 0x00dd, all -> 0x00d8 }
            java.lang.String r1 = "android.intent.extra.TEXT"
            android.content.res.Resources r3 = r6.r     // Catch:{ FileNotFoundException -> 0x00e0, IOException -> 0x00dd, all -> 0x00d8 }
            r4 = 2131230999(0x7f080117, float:1.8078067E38)
            java.lang.String r3 = r3.getString(r4)     // Catch:{ FileNotFoundException -> 0x00e0, IOException -> 0x00dd, all -> 0x00d8 }
            r0.putExtra(r1, r3)     // Catch:{ FileNotFoundException -> 0x00e0, IOException -> 0x00dd, all -> 0x00d8 }
            android.content.res.Resources r1 = r6.r     // Catch:{ FileNotFoundException -> 0x00e0, IOException -> 0x00dd, all -> 0x00d8 }
            r3 = 2131230996(0x7f080114, float:1.807806E38)
            java.lang.String r1 = r1.getString(r3)     // Catch:{ FileNotFoundException -> 0x00e0, IOException -> 0x00dd, all -> 0x00d8 }
            android.content.Intent r0 = android.content.Intent.createChooser(r0, r1)     // Catch:{ FileNotFoundException -> 0x00e0, IOException -> 0x00dd, all -> 0x00d8 }
            r6.startActivity(r0)     // Catch:{ FileNotFoundException -> 0x00e0, IOException -> 0x00dd, all -> 0x00d8 }
            if (r2 == 0) goto L_0x00a0
            r2.close()     // Catch:{ IOException -> 0x00d3 }
        L_0x00a0:
            return
        L_0x00a1:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00a0
        L_0x00a6:
            r0 = move-exception
            r1 = r3
        L_0x00a8:
            r0.printStackTrace()     // Catch:{ all -> 0x00db }
            if (r1 == 0) goto L_0x00a0
            r1.close()     // Catch:{ IOException -> 0x00b1 }
            goto L_0x00a0
        L_0x00b1:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00a0
        L_0x00b6:
            r0 = move-exception
            r1 = r3
        L_0x00b8:
            r0.printStackTrace()     // Catch:{ all -> 0x00db }
            if (r1 == 0) goto L_0x00a0
            r1.close()     // Catch:{ IOException -> 0x00c1 }
            goto L_0x00a0
        L_0x00c1:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00a0
        L_0x00c6:
            r0 = move-exception
            r1 = r3
        L_0x00c8:
            if (r1 == 0) goto L_0x00cd
            r1.close()     // Catch:{ IOException -> 0x00ce }
        L_0x00cd:
            throw r0
        L_0x00ce:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00cd
        L_0x00d3:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00a0
        L_0x00d8:
            r0 = move-exception
            r1 = r2
            goto L_0x00c8
        L_0x00db:
            r0 = move-exception
            goto L_0x00c8
        L_0x00dd:
            r0 = move-exception
            r1 = r2
            goto L_0x00b8
        L_0x00e0:
            r0 = move-exception
            r1 = r2
            goto L_0x00a8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.asai24.golf.activity.ScoreCard.f():void");
    }

    private Bitmap g() {
        Bitmap decodeResource = Integer.parseInt(Build.VERSION.SDK) < 4 ? this.o.booleanValue() ? BitmapFactory.decodeResource(this.r, R.drawable.sc_email_apk3_9) : BitmapFactory.decodeResource(this.r, R.drawable.sc_email_apk3_18) : this.o.booleanValue() ? BitmapFactory.decodeResource(this.r, R.drawable.sc_email_apk4_9) : BitmapFactory.decodeResource(this.r, R.drawable.sc_email_apk4_18);
        Bitmap createBitmap = Bitmap.createBitmap(decodeResource.getWidth(), decodeResource.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        canvas.drawBitmap(decodeResource, 0.0f, 0.0f, (Paint) null);
        decodeResource.recycle();
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(-16777216);
        paint.setTextSize(22.0f);
        paint.setTypeface(Typeface.DEFAULT_BOLD);
        Paint paint2 = new Paint(paint);
        paint2.setTextSize(16.0f);
        paint2.setColor(this.r.getColor(R.color.scorecard_text_dark));
        Paint paint3 = new Paint(paint);
        paint3.setTextSize(14.0f);
        paint3.setTypeface(Typeface.DEFAULT);
        paint3.setColor(this.r.getColor(R.color.scorecard_text_dark));
        Paint paint4 = new Paint(paint);
        paint4.setTextSize(12.0f);
        paint4.setColor(this.r.getColor(R.color.scorecard_text_dark));
        Paint paint5 = new Paint(paint4);
        paint5.setUnderlineText(true);
        Paint paint6 = new Paint(paint);
        paint6.setColor(this.r.getColor(R.color.scorecard_text_green));
        paint6.setTextSize(14.0f);
        Paint paint7 = new Paint(paint);
        paint7.setTextSize(14.0f);
        Paint paint8 = new Paint(paint);
        paint8.setTextSize(14.0f);
        Paint paint9 = new Paint(paint);
        Paint paint10 = new Paint(paint);
        paint10.setTextSize(12.0f);
        Paint paint11 = new Paint(paint);
        paint11.setTextSize(11.0f);
        canvas.drawText(new StringBuilder().append((Object) this.u.getText()).toString(), 172.0f, 23.0f, paint2);
        canvas.drawText(new StringBuilder().append((Object) this.v.getText()).toString(), 185.0f, 41.0f, paint3);
        canvas.drawText(this.r.getString(R.string.email_info), 20.0f, 315.0f, paint4);
        canvas.drawText(this.r.getString(R.string.email_url), 20.0f, 335.0f, paint5);
        canvas.drawText("Yard", 5.0f, 88.0f, paint6);
        canvas.drawText("Par", 5.0f, 107.0f, paint6);
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.j.size()) {
                return createBitmap;
            }
            String str = (String) this.j.get(i3);
            float f2 = 140.0f + (((float) i3) * 45.2f);
            if (str.getBytes().length > 10) {
                int i4 = 0;
                while (true) {
                    if (i4 < str.length()) {
                        if (str.substring(0, i4 + 1).getBytes().length > 10 && i4 > 5) {
                            canvas.drawText(str.substring(0, i4), 5.0f, f2 - 13.0f, paint8);
                            str = str.substring(i4);
                            break;
                        }
                        i4++;
                    } else {
                        break;
                    }
                }
            }
            if (str.getBytes().length > 10 && str.length() > 5) {
                String str2 = str;
                for (int i5 = 0; i5 < str2.length(); i5++) {
                    if (str2.substring(0, i5 + 1).getBytes().length > 10 && i5 > 5) {
                        str2 = String.valueOf(str2.substring(0, i5 - 2)) + "..";
                    }
                }
                str = str2;
            }
            canvas.drawText(str, 5.0f, f2, paint8);
            int i6 = 0;
            while (true) {
                int i7 = i6;
                if (i7 >= this.b) {
                    break;
                }
                int i8 = this.o.booleanValue() ? 9 : 19;
                String str3 = "";
                if (i3 < this.g.size() && i7 < ((ArrayList) this.g.get(i3)).size()) {
                    str3 = String.valueOf(str3) + ((Object) ((TextView) ((ArrayList) this.g.get(i3)).get(i7)).getText());
                }
                if (str3.trim().equals("") || i7 >= i8 || i7 == 9) {
                    paint9.setColor(-16777216);
                } else {
                    int intValue = Integer.valueOf(str3.trim()).intValue();
                    int intValue2 = Integer.valueOf(new StringBuilder().append((Object) ((TextView) this.e.get(i7)).getText()).toString()).intValue();
                    if (this.z) {
                        intValue2 = this.A ? 0 : 2;
                    }
                    if (intValue < intValue2) {
                        paint9.setColor(this.r.getColor(R.color.scorecard_text_blue));
                    } else if (intValue == intValue2) {
                        paint9.setColor(-16777216);
                    } else {
                        paint9.setColor(this.r.getColor(R.color.scorecard_text_pink));
                    }
                }
                canvas.drawText(str3, a(str3, 107.0f + (((float) i7) * 44.0f), 9), 140.0f + (((float) i3) * 45.2f), paint9);
                String sb = new StringBuilder().append((Object) ((TextView) ((ArrayList) this.h.get(i3)).get(i7)).getText()).toString();
                canvas.drawText(sb, a(sb, 125.0f + (((float) i7) * 44.0f), 10), 123.0f + (((float) i3) * 45.2f), paint10);
                String sb2 = new StringBuilder().append((Object) ((TextView) ((ArrayList) this.i.get(i3)).get(i7)).getText()).toString();
                canvas.drawText(sb2, a(sb2, 112.0f + (((float) i7) * 44.0f), 17), 154.0f + (((float) i3) * 45.2f), paint11);
                if (i3 == 0) {
                    String sb3 = new StringBuilder().append((Object) ((TextView) this.d.get(i7)).getText()).toString();
                    String sb4 = new StringBuilder().append((Object) ((TextView) this.e.get(i7)).getText()).toString();
                    if (i7 >= i8 || i7 == 9) {
                        canvas.drawText(sb3, a(sb3, 110.0f + (((float) i7) * 44.0f), 1), 88.0f, paint7);
                        canvas.drawText(sb4, a(sb4, 110.0f + (((float) i7) * 44.0f), 5), 107.0f, paint7);
                    } else {
                        canvas.drawText(sb3, a(sb3, 110.0f + (((float) i7) * 44.0f), 1), 88.0f, paint6);
                        canvas.drawText(sb4, a(sb4, 110.0f + (((float) i7) * 44.0f), 5), 107.0f, paint6);
                    }
                }
                i6 = i7 + 1;
            }
            i2 = i3 + 1;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.asai24.golf.activity.ScoreCard.a(long, boolean):void
     arg types: [long, int]
     candidates:
      com.asai24.golf.activity.ScoreCard.a(long, int):void
      com.asai24.golf.activity.ScoreCard.a(com.asai24.golf.a.y, int):void
      com.asai24.golf.activity.ScoreCard.a(long, boolean):void */
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.change_sf_btn /*2131427749*/:
                this.z = !this.z;
                if (this.z) {
                    this.t.setText(getString(R.string.score_card_stableford));
                } else {
                    this.t.setText(getString(R.string.score_card_standard));
                }
                SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(this).edit();
                edit.putBoolean(getString(R.string.pref_stableford), this.z);
                edit.commit();
                onResume();
                break;
            case R.id.email_scorecard /*2131427755*/:
                this.q.a("Scorecard", "Email", "Email", 1);
                f();
                break;
        }
        if (view.getTag() != null) {
            a(Long.valueOf(new StringBuilder().append(((ArrayList) view.getTag()).get(1)).toString()).longValue(), false);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Bundle extras = getIntent().getExtras();
        this.l = extras.getLongArray("player_ids");
        this.m = extras.getLong("playing_round_id");
        this.n = extras.getLong("playing_tee_id");
        this.o = Boolean.valueOf(extras.getBoolean("play_nine"));
        if (this.o.booleanValue()) {
            setContentView((int) R.layout.score_card_9);
            this.b = 11;
            this.c = 9;
        } else {
            setContentView((int) R.layout.score_card_18);
            this.b = 22;
            this.c = 18;
        }
        this.p = b.a(this);
        this.q = (GolfApplication) getApplication();
        this.r = getResources();
        this.t = (Button) findViewById(R.id.change_sf_btn);
        this.u = (TextView) findViewById(R.id.club_name_txt);
        this.v = (TextView) findViewById(R.id.play_date_txt);
        this.x = (TextView) findViewById(R.id.yard_unit);
        this.w = (Button) findViewById(R.id.email_scorecard);
        this.t.setOnClickListener(this);
        this.w.setOnClickListener(this);
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        if (defaultSharedPreferences.getString(getString(R.string.key_owner_measure_unit), "yard").equals("yard")) {
            this.y = true;
            this.x.setText("Yard");
        } else {
            this.y = false;
            this.x.setText("Meter");
        }
        this.j = new ArrayList();
        this.z = defaultSharedPreferences.getBoolean(getString(R.string.pref_stableford), false);
        this.A = defaultSharedPreferences.getBoolean(getString(R.string.key_owner_stableford), false);
        this.B = defaultSharedPreferences.getBoolean(getString(R.string.key_owner_point), false);
        if (this.z) {
            this.t.setText(getString(R.string.score_card_stableford));
        } else {
            this.t.setText(getString(R.string.score_card_standard));
        }
        d();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.q.a();
        if (this.o.booleanValue()) {
            com.asai24.golf.d.d.a(findViewById(R.id.score_card_9));
        } else {
            com.asai24.golf.d.d.a(findViewById(R.id.score_card_18));
        }
        System.gc();
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.j.clear();
        e();
    }
}
