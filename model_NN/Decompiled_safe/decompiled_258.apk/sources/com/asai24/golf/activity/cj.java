package com.asai24.golf.activity;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import com.asai24.golf.R;
import com.asai24.golf.a.d;

class cj extends CursorAdapter {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ SelectPlayers a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public cj(SelectPlayers selectPlayers, Context context, d dVar) {
        super(context, dVar);
        this.a = selectPlayers;
    }

    public boolean areAllItemsEnabled() {
        return false;
    }

    public void bindView(View view, Context context, Cursor cursor) {
        long b = ((d) cursor).b();
        String c = ((d) cursor).c();
        TextView textView = (TextView) view.findViewById(R.id.entry_player_name);
        textView.setText(c);
        textView.setOnClickListener(new eg(this, b, c));
        ((ImageButton) view.findViewById(R.id.cross_btn)).setOnClickListener(new ek(this, b));
    }

    public boolean isEnabled(int i) {
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        return LayoutInflater.from(context).inflate((int) R.layout.select_player_item, viewGroup, false);
    }
}
