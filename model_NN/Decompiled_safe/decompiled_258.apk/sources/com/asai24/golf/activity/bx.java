package com.asai24.golf.activity;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import com.asai24.golf.R;
import java.util.List;

class bx extends ArrayAdapter {
    final /* synthetic */ CourseDetail a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    bx(CourseDetail courseDetail, Context context, int i, List list) {
        super(context, i, list);
        this.a = courseDetail;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        CheckedTextView checkedTextView = (CheckedTextView) super.getView(i, view, viewGroup);
        checkedTextView.setBackgroundColor(this.a.d.getColor(R.color.list_bg));
        checkedTextView.setTextColor(this.a.d.getColor(R.color.tee_select_list_text));
        return checkedTextView;
    }
}
