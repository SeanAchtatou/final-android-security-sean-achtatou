package com.asai24.golf.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.widget.Toast;
import com.asai24.golf.R;
import com.asai24.golf.c.b;
import com.asai24.golf.e.d;

class cv extends AsyncTask {
    final /* synthetic */ SearchCourse a;
    private ProgressDialog b;

    private cv(SearchCourse searchCourse) {
        this.a = searchCourse;
    }

    /* synthetic */ cv(SearchCourse searchCourse, cv cvVar) {
        this(searchCourse);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public d doInBackground(d... dVarArr) {
        d dVar = dVarArr[0];
        dVar.a(this.a.x == 1 ? new com.asai24.golf.c.d(this.a).a(dVar.c()) : new b(this.a).a(dVar.d()));
        return dVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(d dVar) {
        if (!this.a.z) {
            this.b.dismiss();
            if (dVar.k().size() == 0) {
                Toast.makeText(this.a, (int) R.string.no_tees_message, 5000).show();
                return;
            }
            this.a.c.a("/choose_tee");
            this.a.c.b("/choose_tee");
            Intent intent = new Intent(this.a, CourseDetail.class);
            intent.putExtra(this.a.d.getString(R.string.intent_course), dVar);
            intent.putExtra(this.a.d.getString(R.string.intent_course_mode), this.a.x);
            this.a.startActivityForResult(intent, 0);
        }
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        SearchCourse searchCourse = this.a;
        Resources resources = searchCourse.getResources();
        this.b = new ProgressDialog(searchCourse);
        this.b.setIndeterminate(true);
        this.b.setMessage(resources.getString(R.string.msg_now_loading));
        if (!this.a.isFinishing()) {
            this.b.show();
        }
    }
}
