package com.asai24.golf.activity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.asai24.golf.R;
import com.asai24.golf.a.m;
import com.asai24.golf.e.d;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

class c extends ArrayAdapter {
    final /* synthetic */ SearchCourse a;
    private final LayoutInflater b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public c(SearchCourse searchCourse, Context context, List list) {
        super(context, 0, list);
        this.a = searchCourse;
        this.b = LayoutInflater.from(context);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i, View view, ViewGroup viewGroup) {
        dn dnVar;
        View view2;
        View view3;
        if (view == null) {
            dnVar = new dn();
            switch (this.a.x) {
                case 1:
                    View inflate = this.b.inflate((int) R.layout.search_course_oobgolf_item, viewGroup, false);
                    dnVar.a = (TextView) inflate.findViewById(R.id.oob_course_name);
                    dnVar.b = (TextView) inflate.findViewById(R.id.oob_course_address);
                    view3 = inflate;
                    break;
                case 2:
                    View inflate2 = this.b.inflate((int) R.layout.search_course_yourgolf_item, viewGroup, false);
                    dnVar.a = (TextView) inflate2.findViewById(R.id.yourgolf_course_name);
                    dnVar.b = (TextView) inflate2.findViewById(R.id.yourgolf_course_address);
                    view3 = inflate2;
                    break;
                case 3:
                    View inflate3 = this.b.inflate((int) R.layout.search_course_history_item, viewGroup, false);
                    dnVar.a = (TextView) inflate3.findViewById(R.id.history_course_name);
                    dnVar.b = (TextView) inflate3.findViewById(R.id.history_course_downloaded_date);
                    view3 = inflate3;
                    break;
                default:
                    view3 = view;
                    break;
            }
            view3.setTag(dnVar);
            view2 = view3;
        } else {
            dnVar = (dn) view.getTag();
            view2 = view;
        }
        d dVar = (d) getItem(i);
        String a2 = dVar.a();
        String str = String.valueOf(dVar.g()) + ", " + dVar.h();
        if (this.a.x == 3) {
            m a3 = this.a.b.a(dVar.b());
            str = String.valueOf(this.a.d.getString(R.string.course_last_downloaded)) + " " + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(a3.j()));
            a3.close();
        }
        dnVar.a.setText(a2);
        dnVar.b.setText(str);
        return view2;
    }
}
