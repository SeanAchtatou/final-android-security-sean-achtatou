package com.asai24.golf.activity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import com.asai24.golf.R;
import java.util.List;

class da extends ArrayAdapter {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ ScoreEntryWithMap a;
    private final LayoutInflater b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public da(ScoreEntryWithMap scoreEntryWithMap, Context context, List list) {
        super(context, 0, list);
        this.a = scoreEntryWithMap;
        this.b = LayoutInflater.from(context);
    }

    public boolean areAllItemsEnabled() {
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i, View view, ViewGroup viewGroup) {
        View inflate;
        dz dzVar = (dz) getItem(i);
        if (this.a.z.booleanValue()) {
            inflate = this.b.inflate((int) R.layout.player_info_item, viewGroup, false);
            ((ImageButton) inflate.findViewById(R.id.player_back)).setOnClickListener(this.a);
        } else {
            if (this.a.F == 0) {
                inflate = this.b.inflate((int) R.layout.player_group_items, viewGroup, false);
                ImageButton imageButton = (ImageButton) inflate.findViewById(R.id.plus_stroke);
                imageButton.setOnClickListener(this.a);
                if (dzVar.f.booleanValue()) {
                    imageButton.setTag(1);
                } else {
                    imageButton.setTag(0);
                }
                ((ImageButton) inflate.findViewById(R.id.minus_stroke)).setOnClickListener(this.a);
            } else {
                inflate = this.b.inflate((int) R.layout.player_group_items_with_game, viewGroup, false);
                Button button = (Button) inflate.findViewById(R.id.player_game_score_btn);
                if (this.a.G) {
                    button.setText("Point: " + dzVar.d);
                    button.setOnClickListener(this.a);
                } else {
                    button.setVisibility(4);
                }
            }
            inflate.setId((int) dzVar.a);
            ImageButton imageButton2 = (ImageButton) inflate.findViewById(R.id.player_next);
            if (dzVar.f.booleanValue()) {
                imageButton2.setVisibility(0);
                imageButton2.setOnClickListener(this.a);
            } else {
                imageButton2.setVisibility(4);
            }
        }
        TextView textView = (TextView) inflate.findViewById(R.id.player_name);
        textView.setText(dzVar.b);
        textView.setOnClickListener(new db(this, dzVar));
        ((TextView) inflate.findViewById(R.id.player_total_score)).setText(dzVar.g + " (" + dzVar.a() + ")" + " - P" + dzVar.i);
        Button button2 = (Button) inflate.findViewById(R.id.hole_player_strokes);
        button2.setText(new StringBuilder().append(dzVar.c).toString());
        button2.setOnClickListener(this.a);
        TextView textView2 = (TextView) inflate.findViewById(R.id.hole_player_putts);
        if (dzVar.e != null) {
            textView2.setText(dzVar.e);
        }
        return inflate;
    }

    public boolean isEnabled(int i) {
        return false;
    }
}
