package com.asai24.golf.activity;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

class ca implements LocationListener {
    final /* synthetic */ ScoreEntryWithMap a;

    ca(ScoreEntryWithMap scoreEntryWithMap) {
        this.a = scoreEntryWithMap;
    }

    public void onLocationChanged(Location location) {
        this.a.f = location;
    }

    public void onProviderDisabled(String str) {
    }

    public void onProviderEnabled(String str) {
    }

    public void onStatusChanged(String str, int i, Bundle bundle) {
    }
}
