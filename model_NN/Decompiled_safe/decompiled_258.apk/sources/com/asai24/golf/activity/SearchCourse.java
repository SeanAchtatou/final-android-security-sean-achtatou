package com.asai24.golf.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.text.method.LinkMovementMethod;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.asai24.golf.GolfApplication;
import com.asai24.golf.R;
import com.asai24.golf.a.b;
import com.asai24.golf.a.m;
import com.asai24.golf.c.f;
import com.asai24.golf.c.l;
import com.asai24.golf.e.d;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Timer;

public class SearchCourse extends GolfActivity implements View.OnClickListener, View.OnKeyListener {
    /* access modifiers changed from: private */
    public l A;
    /* access modifiers changed from: private */
    public TextView B;
    /* access modifiers changed from: private */
    public b b;
    /* access modifiers changed from: private */
    public GolfApplication c;
    /* access modifiers changed from: private */
    public Resources d;
    /* access modifiers changed from: private */
    public EditText e;
    private Button f;
    private Button g;
    /* access modifiers changed from: private */
    public ListView h;
    private TextView i;
    /* access modifiers changed from: private */
    public View j;
    /* access modifiers changed from: private */
    public ImageView k;
    /* access modifiers changed from: private */
    public ViewGroup l;
    /* access modifiers changed from: private */
    public c m;
    /* access modifiers changed from: private */
    public HashMap n;
    /* access modifiers changed from: private */
    public f o;
    private LocationListener p;
    private LocationManager q;
    /* access modifiers changed from: private */
    public Location r;
    private Timer s;
    /* access modifiers changed from: private */
    public long t;
    /* access modifiers changed from: private */
    public boolean u;
    /* access modifiers changed from: private */
    public ProgressDialog v;
    /* access modifiers changed from: private */
    public d w;
    /* access modifiers changed from: private */
    public int x;
    private boolean y;
    /* access modifiers changed from: private */
    public boolean z;

    /* access modifiers changed from: private */
    public void a(Location location) {
        if (this.v != null && this.v.isShowing()) {
            this.v.dismiss();
            this.v = null;
        }
        h();
        this.r = location;
        if (this.u) {
            new ai(this, null).execute(1);
        }
        this.u = false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.widget.ListView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void d() {
        boolean z2;
        if (this.x == 3) {
            this.h = (ListView) findViewById(R.id.history_courses);
            this.m = new c(this, this, new ArrayList());
            m a = this.b.a();
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            while (!a.isAfterLast()) {
                if (!a.c().equals("Your Golf Course(Sample)")) {
                    String f2 = a.f();
                    String g2 = a.g();
                    if (arrayList.size() == 0 && arrayList2.size() == 0) {
                        z2 = false;
                    } else {
                        z2 = arrayList.contains(f2);
                        if (arrayList2.contains(g2)) {
                            z2 = true;
                        }
                    }
                    if (!z2) {
                        d dVar = new d();
                        dVar.a(a.b());
                        dVar.b(a.d());
                        dVar.a(a.c());
                        if (f2 == null || f2.equals("")) {
                            dVar.b(0);
                        } else {
                            arrayList.add(f2);
                            dVar.b(Long.parseLong(f2));
                        }
                        if (g2 == null || g2.equals("")) {
                            dVar.c(0);
                        } else {
                            arrayList2.add(g2);
                            dVar.c(Long.parseLong(g2));
                        }
                        dVar.c("");
                        dVar.d("");
                        dVar.f("");
                        dVar.e("");
                        this.m.add(dVar);
                    }
                }
                a.moveToNext();
            }
            a.close();
            this.h.setAdapter((ListAdapter) this.m);
            this.h.setOnItemClickListener(new ex(this));
            return;
        }
        if (this.x == 1) {
            this.e = (EditText) findViewById(R.id.oob_keyword);
            this.e.setOnKeyListener(this);
            this.f = (Button) findViewById(R.id.oob_search);
            this.g = (Button) findViewById(R.id.oob_search_nearby);
            this.h = (ListView) findViewById(R.id.oob_courses);
            this.l = (ViewGroup) findViewById(R.id.oob_credit_and_links);
            this.i = (TextView) findViewById(R.id.oob_credit);
            this.k = (ImageView) findViewById(R.id.oob_logo);
            this.k.setOnTouchListener(new ey(this));
            this.k.setOnClickListener(new ev(this));
        } else {
            this.e = (EditText) findViewById(R.id.yourgolf_keyword);
            this.e.setOnKeyListener(this);
            this.f = (Button) findViewById(R.id.yourgolf_search);
            this.g = (Button) findViewById(R.id.yourgolf_search_nearby);
            this.h = (ListView) findViewById(R.id.yourgolf_courses);
            this.l = (ViewGroup) findViewById(R.id.yourgolf_credit_and_links);
            this.i = (TextView) findViewById(R.id.yourgolf_credit);
            this.k = (ImageView) findViewById(R.id.yourgolf_logo);
            this.B = (TextView) findViewById(R.id.warning_text);
            this.A = new l(this);
        }
        this.j = ((LayoutInflater) getSystemService("layout_inflater")).inflate(17367044, (ViewGroup) this.h, false);
        TextView textView = (TextView) this.j.findViewById(16908309);
        textView.setText((int) R.string.more_results);
        textView.setTextColor(this.d.getColor(R.color.search_text));
        this.j.setVisibility(8);
        this.i.setMovementMethod(LinkMovementMethod.getInstance());
        this.f.setOnClickListener(this);
        this.g.setOnClickListener(this);
        this.m = new c(this, this, new ArrayList());
        this.h.addFooterView(this.j);
        this.h.setAdapter((ListAdapter) this.m);
        this.h.setOnItemClickListener(new ew(this));
    }

    private void e() {
        String editable;
        if (this.e != null && (editable = this.e.getText().toString()) != null && !editable.equals("")) {
            if (this.x == 1) {
                this.c.a("Search", "SeachOobgolf", "SeachOobgolf", 1);
            } else {
                this.c.a("Search", "SeachYourgolf", "SeachYourgolf", 1);
            }
            if (!a()) {
                showDialog(9);
            } else {
                new ai(this, null).execute(0);
            }
        }
    }

    /* access modifiers changed from: private */
    public void f() {
        if (this.o != null) {
            List a = this.o.a();
            if (a != null) {
                for (int i2 = 0; i2 < a.size(); i2++) {
                    List k2 = ((d) a.get(i2)).k();
                    if (k2 != null) {
                        for (int i3 = 0; i3 < k2.size(); i3++) {
                            List c2 = ((com.asai24.golf.e.b) k2.get(i3)).c();
                            if (c2 != null) {
                                c2.clear();
                            }
                        }
                        k2.clear();
                    }
                }
                a.clear();
            }
            this.o = null;
        }
    }

    private void g() {
        h();
        this.q = (LocationManager) getSystemService("location");
        if (this.q != null) {
            Location lastKnownLocation = this.q.getLastKnownLocation("gps");
            if (lastKnownLocation == null || new Date().getTime() - lastKnownLocation.getTime() > 180000) {
                this.s = new Timer(true);
                this.t = 0;
                this.s.scheduleAtFixedRate(new ej(this, new Handler()), 0, 1000);
                this.p = new bl(this);
                this.q.requestLocationUpdates("gps", 60000, 0.0f, this.p);
                return;
            }
            a(lastKnownLocation);
        }
    }

    /* access modifiers changed from: private */
    public void h() {
        if (this.s != null) {
            this.s.cancel();
            this.s.purge();
            this.s = null;
        }
        if (this.q != null) {
            if (this.p != null) {
                this.q.removeUpdates(this.p);
                this.p = null;
            }
            this.q = null;
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i3 == -1) {
            setResult(-1);
            finish();
        }
    }

    public void onClick(View view) {
        if (this.v == null || !this.v.isShowing()) {
            switch (view.getId()) {
                case R.id.oob_search /*2131428146*/:
                case R.id.yourgolf_search /*2131428156*/:
                    ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(view.getWindowToken(), 0);
                    e();
                    return;
                case R.id.oob_search_nearby /*2131428147*/:
                case R.id.yourgolf_search_nearby /*2131428157*/:
                    if (this.x == 1) {
                        this.c.a("Search", "SeachNearbyOobgolf", "SeachNearbyOobgolf", 1);
                    } else {
                        this.c.a("Search", "SeachNearbyYourgolf", "SeachNearbyYourgolf", 1);
                    }
                    if (!a()) {
                        showDialog(9);
                        return;
                    } else if (this.r != null) {
                        new ai(this, null).execute(1);
                        return;
                    } else if (b()) {
                        this.u = true;
                        this.v = new ProgressDialog(this);
                        this.v.setIndeterminate(true);
                        this.v.setMessage(getString(R.string.msg_now_get_location));
                        this.v.show();
                        g();
                        return;
                    } else {
                        return;
                    }
                default:
                    return;
            }
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Bundle extras = getIntent().getExtras();
        if (extras.getString("APP_NAME") != null) {
            this.x = extras.getInt("COURSE_MODE");
            if (this.x == 0) {
                this.x = 2;
            }
            if (this.x == 2) {
                setContentView((int) R.layout.search_course_yourgolf);
            } else {
                setContentView((int) R.layout.search_course_oobgolf);
            }
            this.y = true;
        } else {
            this.x = extras.getInt(getString(R.string.intent_course_mode));
            switch (this.x) {
                case 1:
                    setContentView((int) R.layout.search_course_oobgolf);
                    break;
                case 2:
                    setContentView((int) R.layout.search_course_yourgolf);
                    break;
                case 3:
                    setContentView((int) R.layout.search_course_history);
                    break;
            }
            this.y = false;
        }
        this.b = b.a(this);
        this.c = (GolfApplication) getApplication();
        this.d = getResources();
        this.z = false;
        d();
        if (this.y) {
            this.y = false;
            getWindow().setSoftInputMode(3);
            if (extras.getInt("MODE") == 0) {
                this.e.setText(extras.getString("CLUB_NAME"));
                new ai(this, null).execute(0);
            } else {
                d dVar = new d();
                dVar.c(extras.getLong("yourgolfId"));
                dVar.a(extras.getString("clubName"));
                dVar.b(extras.getString("courseName"));
                dVar.c(extras.getString("address"));
                dVar.d(extras.getString("country"));
                dVar.e(extras.getString("url"));
                dVar.f(extras.getString("phoneNumber"));
                this.e.setText(dVar.a());
                new cv(this, null).execute(dVar);
            }
        }
        c();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i2) {
        switch (i2) {
            case 0:
                return new AlertDialog.Builder(this).setTitle((int) R.string.course_search_help_title).setMessage((int) R.string.course_search_help_message).setPositiveButton((int) R.string.ok, new fc(this)).setNeutralButton((int) R.string.visit_oob_golf, new fe(this)).create();
            case 1:
                return new AlertDialog.Builder(this).setTitle((int) R.string.confirmation).setMessage((int) R.string.dialog_course_history_update_msg).setPositiveButton((int) R.string.ok, new ez(this)).setNeutralButton((int) R.string.download, new fa(this)).create();
            default:
                return super.onCreateDialog(i2);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        if (this.x == 1) {
            menu.add(0, 9, 0, (int) R.string.menu_help).setIcon((int) R.drawable.ic_menu_upload);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        f();
        if (this.m != null) {
            this.m.clear();
        }
        this.c.a();
        this.c.c();
        this.c = null;
        switch (this.x) {
            case 1:
                com.asai24.golf.d.d.a(findViewById(R.id.search_course_oobgolf));
                break;
            case 2:
                com.asai24.golf.d.d.a(findViewById(R.id.search_course_yourgolf));
                break;
            case 3:
                com.asai24.golf.d.d.a(findViewById(R.id.search_course_history));
                break;
        }
        super.onDestroy();
    }

    public boolean onKey(View view, int i2, KeyEvent keyEvent) {
        if (keyEvent.getAction() != 1 || i2 != 66) {
            return false;
        }
        ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(view.getWindowToken(), 0);
        e();
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 9:
                showDialog(0);
                break;
        }
        super.onOptionsItemSelected(menuItem);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        if (this.x != 3) {
            this.r = null;
            h();
        }
        this.c.b();
        this.z = true;
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        Locale locale;
        if (this.x != 3) {
            this.u = false;
            g();
            if (this.x == 2 && (locale = Locale.getDefault()) != null && locale.equals(Locale.JAPAN)) {
                new eu(this).execute(new String[0]);
            }
        }
        this.z = false;
        super.onResume();
    }
}
