package com.asai24.golf.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.asai24.golf.GolfApplication;
import com.asai24.golf.R;
import com.asai24.golf.a.b;
import com.asai24.golf.e.d;
import java.util.List;

public class CourseDetail extends GolfActivity implements View.OnClickListener {
    /* access modifiers changed from: private */
    public b b;
    /* access modifiers changed from: private */
    public GolfApplication c;
    /* access modifiers changed from: private */
    public Resources d;
    private TextView e;
    private TextView f;
    private TextView g;
    private TextView h;
    private Button i;
    private ListView j;
    private Button k;
    /* access modifiers changed from: private */
    public ProgressDialog l;
    /* access modifiers changed from: private */
    public com.asai24.golf.e.b m;
    /* access modifiers changed from: private */
    public d n;
    /* access modifiers changed from: private */
    public int o;
    private ArrayAdapter p;

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i3 == -1) {
            setResult(-1);
            finish();
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.more_details /*2131427416*/:
                try {
                    if (this.n.i().length() != 0) {
                        a(this.n.i());
                        return;
                    } else {
                        c(getString(R.string.more_info_none));
                        return;
                    }
                } catch (Exception e2) {
                    Log.e("CourseDetail", "The page to show more detail for the course could not be opened.");
                    c(getString(R.string.more_info_none));
                    return;
                }
            case R.id.msg_select_tee /*2131427417*/:
            case R.id.tees_choices /*2131427418*/:
            default:
                return;
            case R.id.download_course /*2131427419*/:
                this.m = (com.asai24.golf.e.b) this.j.getItemAtPosition(this.j.getCheckedItemPosition());
                if (this.m == null) {
                    c(getResources().getString(R.string.msg_select_tee));
                    return;
                } else if (this.m.d()) {
                    showDialog(1);
                    return;
                } else {
                    new w(this, null).execute("");
                    return;
                }
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.course_detail);
        this.d = getResources();
        this.c = (GolfApplication) getApplication();
        this.b = b.a(this);
        Bundle extras = getIntent().getExtras();
        this.n = (d) extras.getSerializable(this.d.getString(R.string.intent_course));
        this.o = extras.getInt(this.d.getString(R.string.intent_course_mode));
        this.e = (TextView) findViewById(R.id.course_name);
        this.f = (TextView) findViewById(R.id.club_name);
        this.g = (TextView) findViewById(R.id.address_value);
        this.h = (TextView) findViewById(R.id.phone_value);
        this.i = (Button) findViewById(R.id.more_details);
        this.j = (ListView) findViewById(R.id.tees_choices);
        this.k = (Button) findViewById(R.id.download_course);
        this.e.setText(this.n.f());
        this.f.setText(this.n.e());
        this.g.setText(this.n.g());
        this.h.setText(this.n.j());
        this.i.setOnClickListener(this);
        this.p = new bx(this, this, 17367055, this.n.k());
        this.j.setAdapter((ListAdapter) this.p);
        this.j.setItemsCanFocus(false);
        this.j.setChoiceMode(1);
        this.k.setOnClickListener(this);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i2) {
        switch (i2) {
            case 1:
                return new AlertDialog.Builder(this).setIcon((int) R.drawable.ic_dialog_menu_generic).setTitle((int) R.string.tee_contains_temp_data_alert_title).setMessage((int) R.string.tee_contains_temp_data_alert).setPositiveButton((int) R.string.ok, new bz(this)).setNegativeButton((int) R.string.cancel, new by(this)).create();
            default:
                return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        List k2;
        this.j.setAdapter((ListAdapter) null);
        this.p.clear();
        this.p = null;
        this.d = null;
        if (!(this.n.k() == null || (k2 = this.n.k()) == null)) {
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 >= k2.size()) {
                    break;
                }
                List c2 = ((com.asai24.golf.e.b) k2.get(i3)).c();
                if (c2 != null) {
                    c2.clear();
                }
                i2 = i3 + 1;
            }
            k2.clear();
        }
        this.n = null;
        if (this.m != null) {
            this.m.c().clear();
        }
        this.m = null;
        com.asai24.golf.d.d.a(findViewById(R.id.course_detail));
        this.c.a();
        this.c.c();
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.c.b();
        super.onPause();
    }
}
