package com.asai24.golf.activity;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.widget.CompoundButton;
import com.asai24.golf.R;

class bd implements CompoundButton.OnCheckedChangeListener {
    final /* synthetic */ ScoreEntryGroup a;

    bd(ScoreEntryGroup scoreEntryGroup) {
        this.a = scoreEntryGroup;
    }

    public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
        SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(this.a).edit();
        edit.putBoolean(this.a.c.getString(R.string.preference_note_checked), z);
        edit.commit();
    }
}
