package com.asai24.golf.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.asai24.golf.GolfApplication;
import com.asai24.golf.R;
import com.asai24.golf.d.d;
import java.util.ArrayList;

public class YourGolfAccountExist extends GolfActivity implements AdapterView.OnItemClickListener {
    private Resources b;
    private GolfApplication c;
    private ListView d;
    private ListView e;
    private ArrayAdapter f;
    private ArrayAdapter g;

    private void a(ListView listView) {
        int i = 0;
        int i2 = 0;
        while (true) {
            View childAt = listView.getChildAt(i);
            if (childAt == null) {
                break;
            }
            i2 += childAt.getHeight();
            i++;
        }
        int dimensionPixelSize = this.b.getDimensionPixelSize(R.dimen.yourgolf_account_list_margin) + i2;
        if (listView.getHeight() != dimensionPixelSize) {
            listView.getLayoutParams().height = dimensionPixelSize;
            listView.requestLayout();
        }
    }

    private void d() {
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        arrayList.add(new em(this, R.string.yourgolf_account_update_email, R.string.yourgolf_account_update_email_summary));
        arrayList.add(new em(this, R.string.yourgolf_account_update_password, R.string.yourgolf_account_update_password_summary));
        arrayList.add(new em(this, R.string.yourgolf_account_overwrite_password, R.string.yourgolf_account_overwrite_password_summary));
        arrayList2.add(new em(this, R.string.yourgolf_account_clear_data, R.string.yourgolf_account_clear_data_summary));
        this.f = new de(this, this, arrayList);
        this.g = new cn(this, this, arrayList2);
        this.d.setAdapter((ListAdapter) this.f);
        this.e.setAdapter((ListAdapter) this.g);
        this.d.setOnItemClickListener(this);
        this.e.setOnItemClickListener(this);
    }

    /* access modifiers changed from: private */
    public void e() {
        SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(this).edit();
        edit.putString(getString(R.string.yourgolf_account_auth_token_key), "");
        edit.putString(getString(R.string.yourgolf_account_username_key), "");
        edit.putString(getString(R.string.yourgolf_account_username_confirm_key), "");
        edit.putString(getString(R.string.yourgolf_account_password_key), "");
        edit.putString(getString(R.string.yourgolf_account_password_confirm_key), "");
        edit.commit();
        c(getString(R.string.yourgolf_account_clear_data_complete));
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i2 == -1) {
            setResult(-1);
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.yourgolf_account_exist);
        this.b = getResources();
        this.c = (GolfApplication) getApplication();
        this.d = (ListView) findViewById(R.id.yourgolf_account_update_list);
        this.e = (ListView) findViewById(R.id.yourgolf_account_clear_list);
        d();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i) {
        switch (i) {
            case 0:
                return new AlertDialog.Builder(this).setIcon((int) R.drawable.alert_dialog_icon).setTitle((int) R.string.yourgolf_account_clear_dialog_title).setMessage((int) R.string.yourgolf_account_clear_dialog_msg).setPositiveButton((int) R.string.delete, new dm(this)).setNegativeButton((int) R.string.cancel, new dl(this)).create();
            default:
                return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.c.a();
        this.c.c();
        d.a(findViewById(R.id.yourgolf_account_exist));
        super.onDestroy();
    }

    public void onItemClick(AdapterView adapterView, View view, int i, long j) {
        String obj = view.getTag().toString();
        if (obj != null && !obj.equals("")) {
            if (obj.equals("UPDATE")) {
                switch (i) {
                    case 0:
                    case 1:
                    case 2:
                        Intent intent = new Intent(this, YourGolfAccountUpdate.class);
                        intent.putExtra(getString(R.string.yourgolf_account_update_mode), i);
                        startActivityForResult(intent, 0);
                        return;
                    default:
                        return;
                }
            } else if (obj.equals("CLEAR")) {
                showDialog(0);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.c.b();
        super.onPause();
    }

    public void onWindowFocusChanged(boolean z) {
        if (z) {
            a(this.d);
            a(this.e);
        }
    }
}
