package com.asai24.golf.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.asai24.golf.R;
import com.asai24.golf.a.b;
import com.asai24.golf.a.d;
import com.asai24.golf.a.g;
import com.asai24.golf.a.s;
import com.asai24.golf.a.w;
import com.asai24.golf.e.a;
import java.util.ArrayList;

public class ScoreEntryGroup extends GolfActivity implements View.OnClickListener {
    private b b;
    /* access modifiers changed from: private */
    public Resources c;
    private long d;
    private long e;
    private long f;
    private long g;
    private ListView h;
    private ListView i;
    private ListView j;
    private TextView k;
    private ImageButton l;
    private ImageButton m;
    private ImageButton n;
    private s o;
    private x p;
    private ArrayAdapter q;
    private ArrayAdapter r;
    /* access modifiers changed from: private */
    public String[] s;
    /* access modifiers changed from: private */
    public int t;
    private int u;
    private int v;
    /* access modifiers changed from: private */
    public int w;
    /* access modifiers changed from: private */
    public int x;

    private int a(long j2) {
        g m2 = this.b.m(j2);
        a aVar = new a(this);
        int i2 = 0;
        for (int i3 = 0; i3 < m2.getCount(); i3++) {
            m2.moveToPosition(i3);
            if (aVar.a(m2.e()).equals("pt")) {
                i2++;
            }
        }
        m2.close();
        return i2;
    }

    private void e() {
        this.h = (ListView) findViewById(R.id.hole_summary);
        this.i = (ListView) findViewById(R.id.strokes_picker);
        this.j = (ListView) findViewById(R.id.putts_picker);
        this.i.setChoiceMode(1);
        this.j.setChoiceMode(1);
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        for (int i2 = 0; i2 <= 15; i2++) {
            cm cmVar = new cm(this, i2, 1);
            cm cmVar2 = new cm(this, i2, 2);
            cmVar.a(this);
            cmVar2.a(this);
            arrayList.add(cmVar);
            arrayList2.add(cmVar2);
        }
        this.q = new ei(this, this, arrayList);
        this.i.setAdapter((ListAdapter) this.q);
        this.r = new y(this, this, arrayList2);
        this.j.setAdapter((ListAdapter) this.r);
    }

    private boolean f() {
        return (this.u == this.w && this.v == this.x) ? false : true;
    }

    private boolean g() {
        if (this.w >= this.x) {
            return true;
        }
        c(getString(R.string.invalid_scores));
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.asai24.golf.a.b.a(long, double, double):void
     arg types: [long, int, int]
     candidates:
      com.asai24.golf.a.b.a(long, int, java.lang.String):long
      com.asai24.golf.a.b.a(long, long, java.util.List):long
      com.asai24.golf.a.b.a(com.asai24.golf.e.d, long, int):long
      com.asai24.golf.a.b.a(long, long, long[]):com.asai24.golf.a.aa
      com.asai24.golf.a.b.a(long, long, long):com.asai24.golf.a.w
      com.asai24.golf.a.b.a(long, long, java.lang.String):com.asai24.golf.a.z
      com.asai24.golf.a.b.a(long, java.lang.Integer, java.lang.Integer):void
      com.asai24.golf.a.b.a(long, java.lang.String, java.lang.String):void
      com.asai24.golf.a.b.a(long, double, double):void */
    /* access modifiers changed from: private */
    public void h() {
        int i2 = 0;
        this.b.l(this.d);
        this.b.b(this.d, 0);
        for (int i3 = 0; i3 < this.w; i3++) {
            this.b.i(this.d);
        }
        g m2 = this.b.m(this.d);
        m2.moveToLast();
        while (!m2.isBeforeFirst() && i2 < this.x) {
            this.b.b(m2.b(), getString(R.string.club_pt), "");
            this.b.a(m2.b(), 0.0d, 0.0d);
            m2.moveToPrevious();
            i2++;
        }
        m2.close();
        this.u = this.w;
        this.v = this.x;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: private */
    public void i() {
        Intent intent = new Intent(this, ScoreEntryWithMap.class);
        intent.putExtra("playing_hole_id", this.f);
        intent.putExtra("SingleFlg", false);
        setResult(-1, intent);
        finish();
    }

    private void j() {
        Intent intent = new Intent(this, ScoreEdit.class);
        intent.putExtra("score_id", this.d);
        startActivity(intent);
    }

    /* access modifiers changed from: protected */
    public void d() {
        if (this.o != null && !this.o.isClosed()) {
            this.o.close();
        }
        this.o = this.b.e(this.f);
        if (this.p == null) {
            this.p = new x(this, this.o, this);
        } else {
            this.p.changeCursor(this.o);
        }
        this.h.setAdapter((ListAdapter) this.p);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.group_entry_close /*2131428113*/:
                finish();
                return;
            case R.id.group_entry_save /*2131428114*/:
                if (!f()) {
                    i();
                    return;
                } else if (!g()) {
                    return;
                } else {
                    if (!PreferenceManager.getDefaultSharedPreferences(this).getBoolean(getString(R.string.preference_note_checked), false)) {
                        showDialog(1);
                        return;
                    }
                    h();
                    i();
                    return;
                }
            case R.id.group_entry_detail /*2131428115*/:
                j();
                return;
            default:
                switch (Integer.parseInt(new StringBuilder().append(view.getTag()).toString())) {
                    case 1:
                        this.w = view.getId();
                        if (this.w == 0) {
                            this.x = 0;
                            this.r.notifyDataSetChanged();
                            this.j.setSelection(0);
                        }
                        this.q.notifyDataSetChanged();
                        return;
                    case 2:
                        this.x = view.getId();
                        this.r.notifyDataSetChanged();
                        return;
                    default:
                        return;
                }
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.score_entry_group);
        this.b = b.a(this);
        this.c = getResources();
        this.d = getIntent().getExtras().getLong("score_id");
        w g2 = this.b.g(this.d);
        this.e = g2.f();
        this.f = g2.d();
        this.g = g2.e();
        g2.close();
        this.s = this.c.getStringArray(R.array.score_names);
        s e2 = this.b.e(this.f);
        this.t = e2.e();
        e2.close();
        this.k = (TextView) findViewById(R.id.group_entry_player);
        this.l = (ImageButton) findViewById(R.id.group_entry_detail);
        this.m = (ImageButton) findViewById(R.id.group_entry_close);
        this.n = (ImageButton) findViewById(R.id.group_entry_save);
        this.l.setOnClickListener(this);
        this.m.setOnClickListener(this);
        this.n.setOnClickListener(this);
        d b2 = this.b.b(this.e);
        this.k.setText(b2.c());
        b2.close();
        e();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i2) {
        View inflate = LayoutInflater.from(this).inflate((int) R.layout.alert_dialog_group_entry_note, (ViewGroup) null);
        ((CheckBox) inflate.findViewById(R.id.group_entry_note_msg_check)).setOnCheckedChangeListener(new bd(this));
        switch (i2) {
            case 1:
                return new AlertDialog.Builder(this).setIcon((int) R.drawable.alert_dialog_icon).setTitle((int) R.string.confirmation).setView(inflate).setPositiveButton((int) R.string.save, new bf(this)).setNegativeButton((int) R.string.cancel, new bh(this)).create();
            default:
                return super.onCreateDialog(i2);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        com.asai24.golf.d.d.a(findViewById(R.id.score_entry_group));
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.o != null && !this.o.isClosed()) {
            this.o.close();
        }
    }

    /* access modifiers changed from: protected */
    public void onPrepareDialog(int i2, Dialog dialog) {
        super.onPrepareDialog(i2, dialog);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        w wVar;
        boolean z;
        super.onResume();
        if (this.d == -1) {
            wVar = this.b.b(this.g, this.f, this.e);
            if (wVar.getCount() == 0) {
                wVar.close();
                this.d = this.b.a(this.g, this.f, this.e, 0, 0);
                wVar = this.b.g(this.d);
                wVar.moveToFirst();
                z = false;
            } else {
                this.d = wVar.b();
                if (wVar.c() == 0) {
                    z = false;
                }
                z = true;
            }
        } else {
            wVar = this.b.g(this.d);
            if (wVar.c() == 0) {
                z = false;
            }
            z = true;
        }
        this.u = wVar.c();
        this.v = a(this.d);
        wVar.close();
        if (z) {
            this.w = this.u;
            this.x = this.v;
        } else {
            this.w = this.t;
            this.x = 2;
        }
        this.i.setSelection(this.w);
        this.j.setSelection(this.x);
        this.q.notifyDataSetChanged();
        this.r.notifyDataSetChanged();
        d();
    }
}
