package com.asai24.golf.activity;

import android.view.MotionEvent;
import android.view.View;
import com.asai24.golf.R;

class ey implements View.OnTouchListener {
    final /* synthetic */ SearchCourse a;

    ey(SearchCourse searchCourse) {
        this.a = searchCourse;
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                this.a.k.setImageResource(R.drawable.oob_logo_explained);
                return false;
            case 1:
                this.a.k.setImageResource(R.drawable.oob_logo);
                return false;
            default:
                return false;
        }
    }
}
