package com.asai24.golf.activity;

import android.content.DialogInterface;
import android.widget.Toast;
import com.asai24.golf.R;

class bj implements DialogInterface.OnClickListener {
    final /* synthetic */ SelectPlayers a;
    private final /* synthetic */ Long[] b;
    private final /* synthetic */ String[] c;

    bj(SelectPlayers selectPlayers, Long[] lArr, String[] strArr) {
        this.a = selectPlayers;
        this.b = lArr;
        this.c = strArr;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        String str;
        Boolean bool;
        Long l = this.b[i];
        String str2 = this.c[i];
        boolean z = false;
        if (str2.length() > 10) {
            str2 = str2.substring(0, 10);
            z = true;
        }
        while (true) {
            Boolean bool2 = z;
            str = str2;
            bool = bool2;
            if (str.getBytes().length <= 18) {
                break;
            }
            str2 = str.substring(0, str.length() - 1);
            z = true;
        }
        if (bool.booleanValue()) {
            Toast.makeText(this.a, this.a.e.getString(R.string.max_length_warning), 1).show();
            this.a.g.b(l.longValue(), str);
        }
        this.a.o.add(l);
        this.a.p.add(l);
        this.a.d();
    }
}
