package com.asai24.golf.c;

import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.util.Log;
import android.util.Xml;
import com.asai24.golf.R;
import com.asai24.golf.e.d;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.xmlpull.v1.XmlPullParser;

public class i extends e {
    private Context a;

    public i(Context context) {
        this.a = context;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.asai24.golf.c.e.a(org.apache.http.client.HttpClient, android.content.Context):void
     arg types: [org.apache.http.impl.client.DefaultHttpClient, android.content.Context]
     candidates:
      com.asai24.golf.c.e.a(java.lang.String, java.lang.Object[]):void
      com.asai24.golf.c.e.a(org.apache.http.client.HttpClient, android.content.Context):void */
    private InputStream b(HashMap hashMap) {
        Resources resources = this.a.getResources();
        hashMap.put("dev", resources.getString(R.string.oob_api_key));
        hashMap.put("page_size", "20");
        Uri.Builder buildUpon = Uri.parse(resources.getString(R.string.oob_course_search_api)).buildUpon();
        Iterator it = hashMap.keySet().iterator();
        while (true) {
            Uri.Builder builder = buildUpon;
            if (!it.hasNext()) {
                a("OobCourseSearchAPI", builder.toString());
                HttpGet httpGet = new HttpGet(builder.toString());
                DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
                a((HttpClient) defaultHttpClient, this.a);
                return defaultHttpClient.execute(httpGet).getEntity().getContent();
            }
            String str = (String) it.next();
            buildUpon = builder.appendQueryParameter(str, (String) hashMap.get(str));
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public f a(HashMap hashMap) {
        boolean z;
        boolean z2;
        d dVar;
        XmlPullParser newPullParser = Xml.newPullParser();
        ArrayList arrayList = new ArrayList();
        d dVar2 = new d();
        f fVar = new f();
        try {
            InputStream b = b(hashMap);
            a("OobCourseSearchAPI", "input stream:" + b.toString());
            newPullParser.setInput(b, null);
            boolean z3 = false;
            d dVar3 = dVar2;
            boolean z4 = false;
            for (int eventType = newPullParser.getEventType(); eventType != 1; eventType = newPullParser.next()) {
                switch (eventType) {
                    case 2:
                        String name = newPullParser.getName();
                        if (name.equals("clubs")) {
                            int parseInt = Integer.parseInt(newPullParser.getAttributeValue(null, "page"));
                            int parseInt2 = Integer.parseInt(newPullParser.getAttributeValue(null, "total"));
                            fVar.a(parseInt);
                            fVar.b(parseInt2);
                            z = z4;
                            z2 = z3;
                            dVar = dVar3;
                            break;
                        } else if (name.equals("club")) {
                            dVar = new d();
                            z = z4;
                            z2 = true;
                            break;
                        } else if (name.equals("course")) {
                            z = true;
                            z2 = z3;
                            dVar = dVar3;
                            break;
                        } else if (name.equals("id")) {
                            if (z4) {
                                dVar3.b(new Long(newPullParser.nextText()).longValue());
                                z = z4;
                                z2 = z3;
                                dVar = dVar3;
                                break;
                            }
                            z = z4;
                            z2 = z3;
                            dVar = dVar3;
                            break;
                        } else if (name.equals("name")) {
                            if (z3 && !z4) {
                                dVar3.a(newPullParser.nextText());
                                z = z4;
                                z2 = z3;
                                dVar = dVar3;
                                break;
                            } else {
                                dVar3.b(newPullParser.nextText());
                                z = z4;
                                z2 = z3;
                                dVar = dVar3;
                                break;
                            }
                        } else if (name.equals("address")) {
                            dVar3.c(newPullParser.nextText());
                            z = z4;
                            z2 = z3;
                            dVar = dVar3;
                            break;
                        } else if (name.equals("country")) {
                            dVar3.d(newPullParser.nextText());
                            z = z4;
                            z2 = z3;
                            dVar = dVar3;
                            break;
                        } else if (name.equals("phone")) {
                            dVar3.f(newPullParser.nextText());
                            z = z4;
                            z2 = z3;
                            dVar = dVar3;
                            break;
                        } else {
                            if (name.equals("url")) {
                                dVar3.e(newPullParser.nextText());
                                z = z4;
                                z2 = z3;
                                dVar = dVar3;
                                break;
                            }
                            z = z4;
                            z2 = z3;
                            dVar = dVar3;
                        }
                    case 3:
                        String name2 = newPullParser.getName();
                        if (!name2.equals("club")) {
                            if (name2.equals("course")) {
                                arrayList.add(dVar3.clone());
                                z = false;
                                z2 = z3;
                                dVar = dVar3;
                                break;
                            }
                            z = z4;
                            z2 = z3;
                            dVar = dVar3;
                            break;
                        } else {
                            z = z4;
                            dVar = dVar3;
                            z2 = false;
                            break;
                        }
                    default:
                        z = z4;
                        z2 = z3;
                        dVar = dVar3;
                        break;
                }
                dVar3 = dVar;
                z3 = z2;
                z4 = z;
            }
            a("OobCourseSearchAPI", "course count: " + arrayList.size());
        } catch (Exception e) {
            Log.e("OobCourseSearchAPI", "failed to get oob course finder results", e);
        }
        fVar.a(arrayList);
        return fVar;
    }
}
