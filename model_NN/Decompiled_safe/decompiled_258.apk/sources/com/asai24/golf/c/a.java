package com.asai24.golf.c;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.asai24.golf.R;
import com.asai24.golf.b.d;
import com.asai24.golf.d.c;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.xmlpull.v1.XmlPullParser;

public class a {
    private Context a;
    private String b;
    private boolean c;
    private String d;

    public a(Context context) {
        this.a = context;
    }

    /* JADX WARNING: Removed duplicated region for block: B:108:0x0127 A[SYNTHETIC, Splitter:B:108:0x0127] */
    /* JADX WARNING: Removed duplicated region for block: B:111:0x012c A[SYNTHETIC, Splitter:B:111:0x012c] */
    /* JADX WARNING: Removed duplicated region for block: B:114:0x0131 A[SYNTHETIC, Splitter:B:114:0x0131] */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:127:0x0151 A[SYNTHETIC, Splitter:B:127:0x0151] */
    /* JADX WARNING: Removed duplicated region for block: B:130:0x0156 A[SYNTHETIC, Splitter:B:130:0x0156] */
    /* JADX WARNING: Removed duplicated region for block: B:133:0x015b A[SYNTHETIC, Splitter:B:133:0x015b] */
    /* JADX WARNING: Removed duplicated region for block: B:146:0x017b A[SYNTHETIC, Splitter:B:146:0x017b] */
    /* JADX WARNING: Removed duplicated region for block: B:149:0x0180 A[SYNTHETIC, Splitter:B:149:0x0180] */
    /* JADX WARNING: Removed duplicated region for block: B:152:0x0185 A[SYNTHETIC, Splitter:B:152:0x0185] */
    /* JADX WARNING: Removed duplicated region for block: B:163:0x01a0 A[SYNTHETIC, Splitter:B:163:0x01a0] */
    /* JADX WARNING: Removed duplicated region for block: B:166:0x01a5 A[SYNTHETIC, Splitter:B:166:0x01a5] */
    /* JADX WARNING: Removed duplicated region for block: B:169:0x01aa A[SYNTHETIC, Splitter:B:169:0x01aa] */
    /* JADX WARNING: Removed duplicated region for block: B:214:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:216:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:218:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x005b  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x007a A[SYNTHETIC, Splitter:B:29:0x007a] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x009d A[SYNTHETIC, Splitter:B:46:0x009d] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x00c0 A[SYNTHETIC, Splitter:B:63:0x00c0] */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x00e5 A[SYNTHETIC, Splitter:B:80:0x00e5] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:105:0x0120=Splitter:B:105:0x0120, B:143:0x0174=Splitter:B:143:0x0174, B:124:0x014a=Splitter:B:124:0x014a} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String a(org.apache.http.HttpResponse r8, int r9) {
        /*
            r7 = this;
            r3 = 204(0xcc, float:2.86E-43)
            r2 = 200(0xc8, float:2.8E-43)
            r4 = 0
            r1 = 1
            org.apache.http.StatusLine r0 = r8.getStatusLine()
            int r0 = r0.getStatusCode()
            switch(r9) {
                case 0: goto L_0x004b;
                case 1: goto L_0x0059;
                case 2: goto L_0x0067;
                case 3: goto L_0x0075;
                default: goto L_0x0011;
            }
        L_0x0011:
            org.apache.http.HttpEntity r1 = r8.getEntity()     // Catch:{ IllegalStateException -> 0x011c, XmlPullParserException -> 0x0146, IOException -> 0x0170, all -> 0x019a }
            java.io.InputStream r1 = r1.getContent()     // Catch:{ IllegalStateException -> 0x011c, XmlPullParserException -> 0x0146, IOException -> 0x0170, all -> 0x019a }
            java.io.InputStreamReader r2 = new java.io.InputStreamReader     // Catch:{ IllegalStateException -> 0x01ec, XmlPullParserException -> 0x01db, IOException -> 0x01cd, all -> 0x01bd }
            r2.<init>(r1)     // Catch:{ IllegalStateException -> 0x01ec, XmlPullParserException -> 0x01db, IOException -> 0x01cd, all -> 0x01bd }
            java.io.BufferedReader r3 = new java.io.BufferedReader     // Catch:{ IllegalStateException -> 0x01f2, XmlPullParserException -> 0x01e1, IOException -> 0x01d2, all -> 0x01c2 }
            r3.<init>(r2)     // Catch:{ IllegalStateException -> 0x01f2, XmlPullParserException -> 0x01e1, IOException -> 0x01d2, all -> 0x01c2 }
            org.xmlpull.v1.XmlPullParserFactory r4 = org.xmlpull.v1.XmlPullParserFactory.newInstance()     // Catch:{ IllegalStateException -> 0x01f7, XmlPullParserException -> 0x01e6, IOException -> 0x01d6, all -> 0x01c6 }
            r5 = 1
            r4.setNamespaceAware(r5)     // Catch:{ IllegalStateException -> 0x01f7, XmlPullParserException -> 0x01e6, IOException -> 0x01d6, all -> 0x01c6 }
            org.xmlpull.v1.XmlPullParser r4 = r4.newPullParser()     // Catch:{ IllegalStateException -> 0x01f7, XmlPullParserException -> 0x01e6, IOException -> 0x01d6, all -> 0x01c6 }
            r4.setInput(r3)     // Catch:{ IllegalStateException -> 0x01f7, XmlPullParserException -> 0x01e6, IOException -> 0x01d6, all -> 0x01c6 }
            int r5 = r4.getEventType()     // Catch:{ IllegalStateException -> 0x01f7, XmlPullParserException -> 0x01e6, IOException -> 0x01d6, all -> 0x01c6 }
            switch(r9) {
                case 0: goto L_0x007a;
                case 1: goto L_0x009d;
                case 2: goto L_0x00c0;
                case 3: goto L_0x00e5;
                default: goto L_0x0039;
            }
        L_0x0039:
            if (r3 == 0) goto L_0x003e
            r3.close()     // Catch:{ IOException -> 0x010a }
        L_0x003e:
            if (r2 == 0) goto L_0x0043
            r2.close()     // Catch:{ IOException -> 0x0110 }
        L_0x0043:
            if (r1 == 0) goto L_0x0048
            r1.close()     // Catch:{ IOException -> 0x0116 }
        L_0x0048:
            java.lang.String r0 = ""
        L_0x004a:
            return r0
        L_0x004b:
            if (r0 != r2) goto L_0x0059
            r7.c = r1
            android.content.Context r0 = r7.a
            r1 = 2131231191(0x7f0801d7, float:1.8078456E38)
            java.lang.String r0 = r0.getString(r1)
            goto L_0x004a
        L_0x0059:
            if (r0 != r3) goto L_0x0011
            r7.c = r1
            android.content.Context r0 = r7.a
            r1 = 2131231194(0x7f0801da, float:1.8078462E38)
            java.lang.String r0 = r0.getString(r1)
            goto L_0x004a
        L_0x0067:
            if (r0 != r3) goto L_0x0011
            r7.c = r1
            android.content.Context r0 = r7.a
            r1 = 2131231198(0x7f0801de, float:1.807847E38)
            java.lang.String r0 = r0.getString(r1)
            goto L_0x004a
        L_0x0075:
            if (r0 != r2) goto L_0x0011
            r7.c = r1
            goto L_0x0011
        L_0x007a:
            java.lang.String r0 = r7.a(r4, r5, r0)     // Catch:{ IllegalStateException -> 0x01f7, XmlPullParserException -> 0x01e6, IOException -> 0x01d6, all -> 0x01c6 }
            if (r3 == 0) goto L_0x0083
            r3.close()     // Catch:{ IOException -> 0x0093 }
        L_0x0083:
            if (r2 == 0) goto L_0x0088
            r2.close()     // Catch:{ IOException -> 0x0098 }
        L_0x0088:
            if (r1 == 0) goto L_0x004a
            r1.close()     // Catch:{ IOException -> 0x008e }
            goto L_0x004a
        L_0x008e:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x004a
        L_0x0093:
            r3 = move-exception
            r3.printStackTrace()
            goto L_0x0083
        L_0x0098:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0088
        L_0x009d:
            java.lang.String r0 = r7.b(r4, r5, r0)     // Catch:{ IllegalStateException -> 0x01f7, XmlPullParserException -> 0x01e6, IOException -> 0x01d6, all -> 0x01c6 }
            if (r3 == 0) goto L_0x00a6
            r3.close()     // Catch:{ IOException -> 0x00b6 }
        L_0x00a6:
            if (r2 == 0) goto L_0x00ab
            r2.close()     // Catch:{ IOException -> 0x00bb }
        L_0x00ab:
            if (r1 == 0) goto L_0x004a
            r1.close()     // Catch:{ IOException -> 0x00b1 }
            goto L_0x004a
        L_0x00b1:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x004a
        L_0x00b6:
            r3 = move-exception
            r3.printStackTrace()
            goto L_0x00a6
        L_0x00bb:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x00ab
        L_0x00c0:
            java.lang.String r0 = r7.c(r4, r5, r0)     // Catch:{ IllegalStateException -> 0x01f7, XmlPullParserException -> 0x01e6, IOException -> 0x01d6, all -> 0x01c6 }
            if (r3 == 0) goto L_0x00c9
            r3.close()     // Catch:{ IOException -> 0x00db }
        L_0x00c9:
            if (r2 == 0) goto L_0x00ce
            r2.close()     // Catch:{ IOException -> 0x00e0 }
        L_0x00ce:
            if (r1 == 0) goto L_0x004a
            r1.close()     // Catch:{ IOException -> 0x00d5 }
            goto L_0x004a
        L_0x00d5:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x004a
        L_0x00db:
            r3 = move-exception
            r3.printStackTrace()
            goto L_0x00c9
        L_0x00e0:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x00ce
        L_0x00e5:
            java.lang.String r0 = r7.d(r4, r5, r0)     // Catch:{ IllegalStateException -> 0x01f7, XmlPullParserException -> 0x01e6, IOException -> 0x01d6, all -> 0x01c6 }
            if (r3 == 0) goto L_0x00ee
            r3.close()     // Catch:{ IOException -> 0x0100 }
        L_0x00ee:
            if (r2 == 0) goto L_0x00f3
            r2.close()     // Catch:{ IOException -> 0x0105 }
        L_0x00f3:
            if (r1 == 0) goto L_0x004a
            r1.close()     // Catch:{ IOException -> 0x00fa }
            goto L_0x004a
        L_0x00fa:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x004a
        L_0x0100:
            r3 = move-exception
            r3.printStackTrace()
            goto L_0x00ee
        L_0x0105:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x00f3
        L_0x010a:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x003e
        L_0x0110:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0043
        L_0x0116:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0048
        L_0x011c:
            r0 = move-exception
            r1 = r4
            r2 = r4
            r3 = r4
        L_0x0120:
            com.asai24.golf.d.c.a(r0)     // Catch:{ all -> 0x01cb }
            java.lang.String r0 = r7.b     // Catch:{ all -> 0x01cb }
            if (r1 == 0) goto L_0x012a
            r1.close()     // Catch:{ IOException -> 0x013c }
        L_0x012a:
            if (r2 == 0) goto L_0x012f
            r2.close()     // Catch:{ IOException -> 0x0141 }
        L_0x012f:
            if (r3 == 0) goto L_0x004a
            r3.close()     // Catch:{ IOException -> 0x0136 }
            goto L_0x004a
        L_0x0136:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x004a
        L_0x013c:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x012a
        L_0x0141:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x012f
        L_0x0146:
            r0 = move-exception
            r1 = r4
            r2 = r4
            r3 = r4
        L_0x014a:
            com.asai24.golf.d.c.a(r0)     // Catch:{ all -> 0x01cb }
            java.lang.String r0 = r7.b     // Catch:{ all -> 0x01cb }
            if (r1 == 0) goto L_0x0154
            r1.close()     // Catch:{ IOException -> 0x0166 }
        L_0x0154:
            if (r2 == 0) goto L_0x0159
            r2.close()     // Catch:{ IOException -> 0x016b }
        L_0x0159:
            if (r3 == 0) goto L_0x004a
            r3.close()     // Catch:{ IOException -> 0x0160 }
            goto L_0x004a
        L_0x0160:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x004a
        L_0x0166:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0154
        L_0x016b:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0159
        L_0x0170:
            r0 = move-exception
            r1 = r4
            r2 = r4
            r3 = r4
        L_0x0174:
            com.asai24.golf.d.c.a(r0)     // Catch:{ all -> 0x01cb }
            java.lang.String r0 = r7.b     // Catch:{ all -> 0x01cb }
            if (r1 == 0) goto L_0x017e
            r1.close()     // Catch:{ IOException -> 0x0190 }
        L_0x017e:
            if (r2 == 0) goto L_0x0183
            r2.close()     // Catch:{ IOException -> 0x0195 }
        L_0x0183:
            if (r3 == 0) goto L_0x004a
            r3.close()     // Catch:{ IOException -> 0x018a }
            goto L_0x004a
        L_0x018a:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x004a
        L_0x0190:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x017e
        L_0x0195:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0183
        L_0x019a:
            r0 = move-exception
            r1 = r4
            r2 = r4
            r3 = r4
        L_0x019e:
            if (r1 == 0) goto L_0x01a3
            r1.close()     // Catch:{ IOException -> 0x01ae }
        L_0x01a3:
            if (r2 == 0) goto L_0x01a8
            r2.close()     // Catch:{ IOException -> 0x01b3 }
        L_0x01a8:
            if (r3 == 0) goto L_0x01ad
            r3.close()     // Catch:{ IOException -> 0x01b8 }
        L_0x01ad:
            throw r0
        L_0x01ae:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x01a3
        L_0x01b3:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x01a8
        L_0x01b8:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x01ad
        L_0x01bd:
            r0 = move-exception
            r2 = r4
            r3 = r1
            r1 = r4
            goto L_0x019e
        L_0x01c2:
            r0 = move-exception
            r3 = r1
            r1 = r4
            goto L_0x019e
        L_0x01c6:
            r0 = move-exception
            r6 = r3
            r3 = r1
            r1 = r6
            goto L_0x019e
        L_0x01cb:
            r0 = move-exception
            goto L_0x019e
        L_0x01cd:
            r0 = move-exception
            r2 = r4
            r3 = r1
            r1 = r4
            goto L_0x0174
        L_0x01d2:
            r0 = move-exception
            r3 = r1
            r1 = r4
            goto L_0x0174
        L_0x01d6:
            r0 = move-exception
            r6 = r3
            r3 = r1
            r1 = r6
            goto L_0x0174
        L_0x01db:
            r0 = move-exception
            r2 = r4
            r3 = r1
            r1 = r4
            goto L_0x014a
        L_0x01e1:
            r0 = move-exception
            r3 = r1
            r1 = r4
            goto L_0x014a
        L_0x01e6:
            r0 = move-exception
            r6 = r3
            r3 = r1
            r1 = r6
            goto L_0x014a
        L_0x01ec:
            r0 = move-exception
            r2 = r4
            r3 = r1
            r1 = r4
            goto L_0x0120
        L_0x01f2:
            r0 = move-exception
            r3 = r1
            r1 = r4
            goto L_0x0120
        L_0x01f7:
            r0 = move-exception
            r6 = r3
            r3 = r1
            r1 = r6
            goto L_0x0120
        */
        throw new UnsupportedOperationException("Method not decompiled: com.asai24.golf.c.a.a(org.apache.http.HttpResponse, int):java.lang.String");
    }

    private String a(XmlPullParser xmlPullParser, int i, int i2) {
        String str = "";
        int i3 = i;
        String str2 = "";
        boolean z = false;
        while (i3 != 1) {
            if (i3 != 0) {
                if (i3 == 2) {
                    if (xmlPullParser.getName().equals("code")) {
                        z = true;
                    } else if (xmlPullParser.getName().equals("message")) {
                        z = true;
                    }
                } else if (i3 != 3 && i3 == 4) {
                    switch (z) {
                        case true:
                            str = xmlPullParser.getText();
                            continue;
                        case true:
                            str2 = xmlPullParser.getText();
                            continue;
                    }
                }
            }
            i3 = xmlPullParser.next();
        }
        switch (z) {
            case false:
                c.a(new d("Response content is an empty at " + this.d + " transaction."));
                return this.b;
            case true:
            case true:
                return str.equals("E0101") ? this.a.getString(R.string.yourgolf_account_registration_e0101) : str2;
            default:
                c.a(new d("Unknown error has occured at " + this.d + " transaction."));
                return this.b;
        }
    }

    private String b(XmlPullParser xmlPullParser, int i, int i2) {
        String str = "";
        int i3 = i;
        String str2 = "";
        boolean z = false;
        while (i3 != 1) {
            if (i3 != 0) {
                if (i3 == 2) {
                    if (xmlPullParser.getName().equals("code")) {
                        z = true;
                    } else if (xmlPullParser.getName().equals("message")) {
                        z = true;
                    }
                } else if (i3 != 3 && i3 == 4) {
                    switch (z) {
                        case true:
                            str = xmlPullParser.getText();
                            continue;
                        case true:
                            str2 = xmlPullParser.getText();
                            continue;
                    }
                }
            }
            i3 = xmlPullParser.next();
        }
        switch (z) {
            case false:
                c.a(new d("Response content is an empty at " + this.d + " transaction."));
                return this.b;
            case true:
            case true:
                return str.equals("E0101") ? this.a.getString(R.string.yourgolf_account_update_e0101) : str.equals("E0105") ? this.a.getString(R.string.yourgolf_account_update_e0105) : str2;
            default:
                c.a(new d("Unknown error has occured at " + this.d + " transaction."));
                return this.b;
        }
    }

    private String c(XmlPullParser xmlPullParser, int i, int i2) {
        String str = "";
        int i3 = i;
        String str2 = "";
        boolean z = false;
        while (i3 != 1) {
            if (i3 != 0) {
                if (i3 == 2) {
                    if (xmlPullParser.getName().equals("code")) {
                        z = true;
                    } else if (xmlPullParser.getName().equals("message")) {
                        z = true;
                    }
                } else if (i3 != 3 && i3 == 4) {
                    switch (z) {
                        case true:
                            str = xmlPullParser.getText();
                            continue;
                        case true:
                            str2 = xmlPullParser.getText();
                            continue;
                    }
                }
            }
            i3 = xmlPullParser.next();
        }
        switch (z) {
            case false:
                c.a(new d("Response content is an empty at " + this.d + " transaction."));
                return this.b;
            case true:
            case true:
                return str.equals("E0101") ? this.a.getString(R.string.yourgolf_account_lost_password_e0101) : str.equals("E0102") ? this.a.getString(R.string.yourgolf_account_lost_password_e0102) : str.equals("E0104") ? this.a.getString(R.string.yourgolf_account_lost_password_e0104) : str2;
            default:
                c.a(new d("Unknown error has occured at " + this.d + " transaction."));
                return this.b;
        }
    }

    private String d(XmlPullParser xmlPullParser, int i, int i2) {
        String str = "";
        int i3 = i;
        String str2 = "";
        boolean z = false;
        while (i3 != 1) {
            if (i3 != 0) {
                if (i3 == 2) {
                    if (xmlPullParser.getName().equals("token")) {
                        z = true;
                    } else if (xmlPullParser.getName().equals("code")) {
                        z = true;
                    } else if (xmlPullParser.getName().equals("message")) {
                        z = true;
                    }
                } else if (i3 != 3 && i3 == 4) {
                    switch (z) {
                        case true:
                            str = String.valueOf(str) + xmlPullParser.getText();
                            continue;
                        case true:
                            str = String.valueOf(str) + xmlPullParser.getText().trim();
                            continue;
                        case true:
                            str2 = String.valueOf(str2) + xmlPullParser.getText();
                            continue;
                    }
                }
            }
            i3 = xmlPullParser.next();
        }
        switch (z) {
            case false:
                c.a(new d("Response content is an empty at " + this.d + " transaction."));
                return this.b;
            case true:
            default:
                if (this.c) {
                    SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(this.a).edit();
                    edit.putString(this.a.getString(R.string.yourgolf_account_auth_token_key), str);
                    edit.commit();
                    return this.a.getString(R.string.yourgolf_account_auth_token_success);
                }
                c.a(new d("Unknown error has occured at " + this.d + " transaction."));
                return this.b;
            case true:
            case true:
                return str.equals("E0103") ? this.a.getString(R.string.yourgolf_account_auth_token_e0103) : str.equals("E0104") ? this.a.getString(R.string.yourgolf_account_auth_token_e0104) : str2;
        }
    }

    public String a(String str, String str2, int i) {
        this.c = false;
        String str3 = "https://golfuser.yourgolf-online.com/";
        switch (i) {
            case 0:
                str3 = String.valueOf(str3) + "1a/user/register.xml";
                this.b = this.a.getString(R.string.yourgolf_account_registration_error);
                this.d = "Registration";
                break;
            case 1:
                str3 = String.valueOf(str3) + "user/update.xml";
                this.b = this.a.getString(R.string.yourgolf_account_update_error);
                this.d = "Update";
                break;
            case 2:
                str3 = String.valueOf(str3) + "user/lost_password.xml";
                this.b = this.a.getString(R.string.yourgolf_account_lost_password_error);
                this.d = "Lost Password";
                break;
            case 3:
                str3 = String.valueOf(str3) + "user/auth_token.xml";
                this.b = this.a.getString(R.string.yourgolf_account_auth_token_error);
                this.d = "Authorization Token";
                break;
        }
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, 5000);
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient(basicHttpParams);
        HttpPost httpPost = new HttpPost(str3);
        httpPost.setHeader("Accept-Language", this.a.getString(R.string.yourgolf_account_language));
        ArrayList arrayList = new ArrayList(2);
        arrayList.add(new BasicNameValuePair("email", str));
        arrayList.add(new BasicNameValuePair("password", str2));
        if (i == 1) {
            arrayList.add(new BasicNameValuePair("auth_token", PreferenceManager.getDefaultSharedPreferences(this.a).getString(this.a.getString(R.string.yourgolf_account_auth_token_key), "")));
        }
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(arrayList, "UTF-8"));
            HttpResponse execute = defaultHttpClient.execute(httpPost);
            if (execute != null) {
                return a(execute, i);
            }
            c.a(new d("No Response data from server at " + this.d + " transaction."));
            return this.b;
        } catch (UnsupportedEncodingException e) {
            c.a(e);
            return this.b;
        } catch (ClientProtocolException e2) {
            c.a((Throwable) e2);
            return this.b;
        } catch (IOException e3) {
            c.a(e3);
            return this.b;
        }
    }

    public boolean a() {
        return this.c;
    }
}
