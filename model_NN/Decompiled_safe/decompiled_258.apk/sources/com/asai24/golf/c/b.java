package com.asai24.golf.c;

import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.util.Log;
import android.util.Xml;
import com.asai24.golf.R;
import com.asai24.golf.e.c;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.xmlpull.v1.XmlPullParser;

public class b extends e {
    private Context a;

    public b(Context context) {
        this.a = context;
    }

    private List a(List list) {
        boolean z;
        boolean z2;
        ArrayList arrayList = new ArrayList();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            com.asai24.golf.e.b bVar = (com.asai24.golf.e.b) it.next();
            int size = bVar.c().size();
            if (size < 9) {
                z = false;
                z2 = false;
            } else if (size == 9) {
                z = true;
                z2 = true;
            } else {
                c cVar = (c) bVar.c().get(9);
                if (((c) bVar.c().get(0)).g() || !cVar.g()) {
                    z = false;
                    z2 = true;
                } else {
                    z = true;
                    z2 = true;
                }
            }
            if (z2) {
                if (z) {
                    ArrayList arrayList2 = new ArrayList();
                    for (int i = 0; i < 9; i++) {
                        arrayList2.add((c) bVar.c().get(i));
                    }
                    bVar.a(arrayList2);
                }
                for (c cVar2 : bVar.c()) {
                    if (cVar2.b() == -1 && cVar2.d() == -1) {
                        bVar.a(true);
                        cVar2.b(4);
                        cVar2.d(4);
                    } else if (cVar2.b() != -1 && cVar2.d() == -1) {
                        cVar2.d(cVar2.b());
                    } else if (cVar2.b() == -1 && cVar2.d() != -1) {
                        cVar2.b(cVar2.d());
                    }
                    if (!(cVar2.f() == -1 && cVar2.e() == -1)) {
                        if (cVar2.f() != -1 && cVar2.e() == -1) {
                            cVar2.e(cVar2.f());
                        } else if (cVar2.f() == -1 && cVar2.e() != -1) {
                            cVar2.f(cVar2.e());
                        }
                    }
                    if (cVar2.c() == -1) {
                        bVar.a(true);
                        cVar2.c(0);
                    }
                }
                arrayList.add(bVar);
            }
        }
        return arrayList;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.asai24.golf.c.e.a(org.apache.http.client.HttpClient, android.content.Context):void
     arg types: [org.apache.http.impl.client.DefaultHttpClient, android.content.Context]
     candidates:
      com.asai24.golf.c.e.a(java.lang.String, java.lang.Object[]):void
      com.asai24.golf.c.e.a(org.apache.http.client.HttpClient, android.content.Context):void */
    private InputStream b(long j) {
        Resources resources = this.a.getResources();
        HashMap hashMap = new HashMap();
        hashMap.put("course_id", new StringBuilder().append(j).toString());
        Uri.Builder buildUpon = Uri.parse(resources.getString(R.string.yourgolf_course_yardage_api)).buildUpon();
        Iterator it = hashMap.keySet().iterator();
        while (true) {
            Uri.Builder builder = buildUpon;
            if (!it.hasNext()) {
                a("YourGolfTeeDetailsAPI", builder.toString());
                HttpGet httpGet = new HttpGet(builder.toString());
                DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
                a((HttpClient) defaultHttpClient, this.a);
                return defaultHttpClient.execute(httpGet).getEntity().getContent();
            }
            String str = (String) it.next();
            buildUpon = builder.appendQueryParameter(str, (String) hashMap.get(str));
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public List a(long j) {
        boolean z;
        c cVar;
        com.asai24.golf.e.b bVar;
        XmlPullParser newPullParser = Xml.newPullParser();
        ArrayList arrayList = new ArrayList();
        com.asai24.golf.e.b bVar2 = new com.asai24.golf.e.b();
        c cVar2 = new c();
        try {
            newPullParser.setInput(b(j), null);
            com.asai24.golf.e.b bVar3 = bVar2;
            c cVar3 = cVar2;
            boolean z2 = false;
            for (int eventType = newPullParser.getEventType(); eventType != 1; eventType = newPullParser.next()) {
                switch (eventType) {
                    case 2:
                        String name = newPullParser.getName();
                        if (!name.equals("tee")) {
                            if (!name.equals("hole")) {
                                if (!name.equals("id")) {
                                    if (!name.equals("name")) {
                                        if (!name.equals("num")) {
                                            if (!name.equals("men-handicap")) {
                                                if (!name.equals("women-handicap")) {
                                                    if (!name.equals("men-par")) {
                                                        if (!name.equals("women-par")) {
                                                            if (name.equals("distance") && z2) {
                                                                String nextText = newPullParser.nextText();
                                                                if (!nextText.equals("")) {
                                                                    cVar3.c(Integer.parseInt(nextText));
                                                                    z = z2;
                                                                    cVar = cVar3;
                                                                    bVar = bVar3;
                                                                    break;
                                                                } else {
                                                                    cVar3.c(-1);
                                                                    z = z2;
                                                                    cVar = cVar3;
                                                                    bVar = bVar3;
                                                                    break;
                                                                }
                                                            }
                                                            z = z2;
                                                            cVar = cVar3;
                                                            bVar = bVar3;
                                                            break;
                                                        } else {
                                                            String nextText2 = newPullParser.nextText();
                                                            if (!nextText2.equals("")) {
                                                                cVar3.d(Integer.parseInt(nextText2));
                                                                z = z2;
                                                                cVar = cVar3;
                                                                bVar = bVar3;
                                                                break;
                                                            } else {
                                                                cVar3.d(-1);
                                                                z = z2;
                                                                cVar = cVar3;
                                                                bVar = bVar3;
                                                                break;
                                                            }
                                                        }
                                                    } else {
                                                        String nextText3 = newPullParser.nextText();
                                                        if (!nextText3.equals("")) {
                                                            cVar3.b(Integer.parseInt(nextText3));
                                                            z = z2;
                                                            cVar = cVar3;
                                                            bVar = bVar3;
                                                            break;
                                                        } else {
                                                            cVar3.b(-1);
                                                            z = z2;
                                                            cVar = cVar3;
                                                            bVar = bVar3;
                                                            break;
                                                        }
                                                    }
                                                } else {
                                                    String nextText4 = newPullParser.nextText();
                                                    if (!nextText4.equals("")) {
                                                        cVar3.e(Integer.parseInt(nextText4));
                                                        z = z2;
                                                        cVar = cVar3;
                                                        bVar = bVar3;
                                                        break;
                                                    } else {
                                                        cVar3.e(-1);
                                                        z = z2;
                                                        cVar = cVar3;
                                                        bVar = bVar3;
                                                        break;
                                                    }
                                                }
                                            } else {
                                                String nextText5 = newPullParser.nextText();
                                                if (!nextText5.equals("")) {
                                                    cVar3.f(Integer.parseInt(nextText5));
                                                    z = z2;
                                                    cVar = cVar3;
                                                    bVar = bVar3;
                                                    break;
                                                } else {
                                                    cVar3.f(-1);
                                                    z = z2;
                                                    cVar = cVar3;
                                                    bVar = bVar3;
                                                    break;
                                                }
                                            }
                                        } else {
                                            cVar3.a(Integer.parseInt(newPullParser.nextText()));
                                            z = z2;
                                            cVar = cVar3;
                                            bVar = bVar3;
                                            break;
                                        }
                                    } else {
                                        bVar3.a(newPullParser.nextText());
                                        z = z2;
                                        cVar = cVar3;
                                        bVar = bVar3;
                                        break;
                                    }
                                } else {
                                    bVar3.a(new Long(newPullParser.nextText()).longValue());
                                    z = z2;
                                    cVar = cVar3;
                                    bVar = bVar3;
                                    break;
                                }
                            } else {
                                cVar = new c();
                                bVar = bVar3;
                                z = true;
                                break;
                            }
                        } else {
                            boolean z3 = z2;
                            cVar = cVar3;
                            bVar = new com.asai24.golf.e.b();
                            z = z3;
                            break;
                        }
                    case 3:
                        String name2 = newPullParser.getName();
                        if (!name2.equals("tee")) {
                            if (name2.equals("hole")) {
                                bVar3.a(cVar3);
                                z = false;
                                cVar = cVar3;
                                bVar = bVar3;
                                break;
                            }
                            z = z2;
                            cVar = cVar3;
                            bVar = bVar3;
                            break;
                        } else {
                            arrayList.add(bVar3);
                            z = z2;
                            cVar = cVar3;
                            bVar = bVar3;
                            break;
                        }
                    default:
                        z = z2;
                        cVar = cVar3;
                        bVar = bVar3;
                        break;
                }
                bVar3 = bVar;
                cVar3 = cVar;
                z2 = z;
            }
        } catch (Exception e) {
            Log.e("YourGolfTeeDetailsAPI", "failed to get tee details", e);
        }
        return a(arrayList);
    }
}
