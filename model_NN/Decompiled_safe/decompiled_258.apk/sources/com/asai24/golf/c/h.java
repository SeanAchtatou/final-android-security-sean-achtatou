package com.asai24.golf.c;

import android.content.Context;
import com.asai24.golf.a;
import com.asai24.golf.a.b;
import com.asai24.golf.a.p;

public class h {
    private b a;
    private Context b;
    private String c;

    public h(Context context) {
        this.b = context;
        this.a = b.a(context);
    }

    public a a(long j) {
        p o = this.a.o(j);
        boolean z = false;
        String k = o.k();
        if (k == null || k.equals("")) {
            z = true;
        }
        o.close();
        return a(j, z);
    }

    /* JADX WARNING: Removed duplicated region for block: B:103:0x0772  */
    /* JADX WARNING: Removed duplicated region for block: B:105:0x0777  */
    /* JADX WARNING: Removed duplicated region for block: B:247:0x0b0a A[SYNTHETIC, Splitter:B:247:0x0b0a] */
    /* JADX WARNING: Removed duplicated region for block: B:250:0x0b0f A[SYNTHETIC, Splitter:B:250:0x0b0f] */
    /* JADX WARNING: Removed duplicated region for block: B:253:0x0b14 A[SYNTHETIC, Splitter:B:253:0x0b14] */
    /* JADX WARNING: Removed duplicated region for block: B:257:0x0b28  */
    /* JADX WARNING: Removed duplicated region for block: B:259:0x0b2d  */
    /* JADX WARNING: Removed duplicated region for block: B:317:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x0754 A[SYNTHETIC, Splitter:B:93:0x0754] */
    /* JADX WARNING: Removed duplicated region for block: B:96:0x0759 A[SYNTHETIC, Splitter:B:96:0x0759] */
    /* JADX WARNING: Removed duplicated region for block: B:99:0x075e A[SYNTHETIC, Splitter:B:99:0x075e] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.asai24.golf.a a(long r27, boolean r29) {
        /*
            r26 = this;
            r0 = r26
            android.content.Context r0 = r0.b
            r4 = r0
            android.content.SharedPreferences r4 = android.preference.PreferenceManager.getDefaultSharedPreferences(r4)
            r0 = r26
            android.content.Context r0 = r0.b
            r5 = r0
            r6 = 2131231202(0x7f0801e2, float:1.8078478E38)
            java.lang.String r5 = r5.getString(r6)
            java.lang.String r6 = ""
            java.lang.String r4 = r4.getString(r5, r6)
            r0 = r4
            r1 = r26
            r1.c = r0
            r0 = r26
            java.lang.String r0 = r0.c
            r4 = r0
            if (r4 == 0) goto L_0x0034
            r0 = r26
            java.lang.String r0 = r0.c
            r4 = r0
            java.lang.String r5 = ""
            boolean r4 = r4.equals(r5)
            if (r4 == 0) goto L_0x0037
        L_0x0034:
            com.asai24.golf.a r4 = com.asai24.golf.a.YOURGOLF_NO_SETTINGS
        L_0x0036:
            return r4
        L_0x0037:
            r0 = r26
            android.content.Context r0 = r0.b
            r4 = r0
            java.lang.String r5 = "phone"
            java.lang.Object r29 = r4.getSystemService(r5)
            android.telephony.TelephonyManager r29 = (android.telephony.TelephonyManager) r29
            java.lang.String r4 = r29.getDeviceId()
            if (r4 != 0) goto L_0x0052
            java.lang.RuntimeException r4 = new java.lang.RuntimeException
            java.lang.String r5 = "imei is null"
            r4.<init>(r5)
            throw r4
        L_0x0052:
            r0 = r26
            com.asai24.golf.a.b r0 = r0.a
            r5 = r0
            r0 = r5
            r1 = r27
            com.asai24.golf.a.p r5 = r0.o(r1)
            r0 = r26
            com.asai24.golf.a.b r0 = r0.a
            r6 = r0
            long r7 = r5.c()
            com.asai24.golf.a.m r6 = r6.a(r7)
            r0 = r26
            com.asai24.golf.a.b r0 = r0.a
            r7 = r0
            long r8 = r5.d()
            com.asai24.golf.a.e r7 = r7.s(r8)
            r0 = r26
            com.asai24.golf.a.b r0 = r0.a
            r8 = r0
            long r9 = r7.c()
            com.asai24.golf.a.s r8 = r8.f(r9)
            r0 = r26
            com.asai24.golf.a.b r0 = r0.a
            r9 = r0
            r0 = r9
            r1 = r27
            com.asai24.golf.a.w r9 = r0.h(r1)
            r10 = 0
            r11 = 0
            java.io.ByteArrayOutputStream r12 = new java.io.ByteArrayOutputStream
            r12.<init>()
            java.io.OutputStreamWriter r13 = new java.io.OutputStreamWriter
            r13.<init>(r12)
            r14 = 0
            r15 = 0
            r16 = 0
            org.xmlpull.v1.XmlSerializer r17 = android.util.Xml.newSerializer()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r0 = r17
            r1 = r13
            r0.setOutput(r1)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r18 = "UTF-8"
            r19 = 1
            java.lang.Boolean r19 = java.lang.Boolean.valueOf(r19)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r17.startDocument(r18, r19)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r18 = ""
            java.lang.String r19 = "db"
            r17.startTag(r18, r19)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r18 = ""
            java.lang.String r19 = "deviceId"
            r0 = r17
            r1 = r18
            r2 = r19
            r3 = r4
            r0.attribute(r1, r2, r3)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r4 = ""
            java.lang.String r18 = "table"
            r0 = r17
            r1 = r4
            r2 = r18
            r0.startTag(r1, r2)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r4 = ""
            java.lang.String r18 = "name"
            java.lang.String r19 = "courses"
            r0 = r17
            r1 = r4
            r2 = r18
            r3 = r19
            r0.attribute(r1, r2, r3)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r4 = 0
        L_0x00e8:
            int r18 = r6.getCount()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r0 = r4
            r1 = r18
            if (r0 < r1) goto L_0x0325
            java.lang.String r4 = ""
            java.lang.String r18 = "table"
            r0 = r17
            r1 = r4
            r2 = r18
            r0.endTag(r1, r2)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r4 = ""
            java.lang.String r18 = "table"
            r0 = r17
            r1 = r4
            r2 = r18
            r0.startTag(r1, r2)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r4 = ""
            java.lang.String r18 = "name"
            java.lang.String r19 = "tees"
            r0 = r17
            r1 = r4
            r2 = r18
            r3 = r19
            r0.attribute(r1, r2, r3)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r4 = 0
        L_0x011a:
            int r18 = r7.getCount()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r0 = r4
            r1 = r18
            if (r0 < r1) goto L_0x03bb
            java.lang.String r4 = ""
            java.lang.String r18 = "table"
            r0 = r17
            r1 = r4
            r2 = r18
            r0.endTag(r1, r2)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r4 = ""
            java.lang.String r18 = "table"
            r0 = r17
            r1 = r4
            r2 = r18
            r0.startTag(r1, r2)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r4 = ""
            java.lang.String r18 = "name"
            java.lang.String r19 = "holes"
            r0 = r17
            r1 = r4
            r2 = r18
            r3 = r19
            r0.attribute(r1, r2, r3)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r4 = 0
        L_0x014c:
            int r18 = r8.getCount()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r0 = r4
            r1 = r18
            if (r0 < r1) goto L_0x0451
            java.lang.String r4 = ""
            java.lang.String r18 = "table"
            r0 = r17
            r1 = r4
            r2 = r18
            r0.endTag(r1, r2)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r4 = ""
            java.lang.String r18 = "table"
            r0 = r17
            r1 = r4
            r2 = r18
            r0.startTag(r1, r2)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r4 = ""
            java.lang.String r18 = "name"
            java.lang.String r19 = "rounds"
            r0 = r17
            r1 = r4
            r2 = r18
            r3 = r19
            r0.attribute(r1, r2, r3)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r4 = 0
        L_0x017e:
            int r18 = r5.getCount()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r0 = r4
            r1 = r18
            if (r0 < r1) goto L_0x0584
            java.lang.String r4 = ""
            java.lang.String r18 = "table"
            r0 = r17
            r1 = r4
            r2 = r18
            r0.endTag(r1, r2)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r4 = ""
            java.lang.String r18 = "table"
            r0 = r17
            r1 = r4
            r2 = r18
            r0.startTag(r1, r2)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r4 = ""
            java.lang.String r18 = "name"
            java.lang.String r19 = "scores"
            r0 = r17
            r1 = r4
            r2 = r18
            r3 = r19
            r0.attribute(r1, r2, r3)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r4 = 0
        L_0x01b0:
            int r18 = r9.getCount()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r0 = r4
            r1 = r18
            if (r0 < r1) goto L_0x0647
            java.lang.String r4 = ""
            java.lang.String r18 = "table"
            r0 = r17
            r1 = r4
            r2 = r18
            r0.endTag(r1, r2)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            com.asai24.golf.e.a r4 = new com.asai24.golf.e.a     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r0 = r26
            android.content.Context r0 = r0.b     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r18 = r0
            r0 = r4
            r1 = r18
            r0.<init>(r1)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r18 = ""
            java.lang.String r19 = "table"
            r17.startTag(r18, r19)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r18 = ""
            java.lang.String r19 = "name"
            java.lang.String r20 = "score_details"
            r17.attribute(r18, r19, r20)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r9.moveToFirst()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
        L_0x01e6:
            boolean r18 = r9.isAfterLast()     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            if (r18 == 0) goto L_0x071c
            java.lang.String r4 = ""
            java.lang.String r18 = "table"
            r0 = r17
            r1 = r4
            r2 = r18
            r0.endTag(r1, r2)     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            java.lang.String r4 = ""
            java.lang.String r18 = "table"
            r0 = r17
            r1 = r4
            r2 = r18
            r0.startTag(r1, r2)     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            java.lang.String r4 = ""
            java.lang.String r18 = "name"
            java.lang.String r19 = "players"
            r0 = r17
            r1 = r4
            r2 = r18
            r3 = r19
            r0.attribute(r1, r2, r3)     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            r0 = r26
            com.asai24.golf.a.b r0 = r0.a     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            r4 = r0
            r0 = r4
            r1 = r27
            long[] r4 = r0.u(r1)     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            r18 = 0
            r24 = r18
            r18 = r11
            r11 = r24
        L_0x0228:
            r0 = r4
            int r0 = r0.length     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            r19 = r0
            r0 = r11
            r1 = r19
            if (r0 < r1) goto L_0x08ba
            java.lang.String r4 = ""
            java.lang.String r11 = "table"
            r0 = r17
            r1 = r4
            r2 = r11
            r0.endTag(r1, r2)     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            java.lang.String r4 = ""
            java.lang.String r11 = "db"
            r0 = r17
            r1 = r4
            r2 = r11
            r0.endTag(r1, r2)     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            r17.endDocument()     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            r13.flush()     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            org.apache.http.params.BasicHttpParams r4 = new org.apache.http.params.BasicHttpParams     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            r4.<init>()     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            r11 = 5000(0x1388, float:7.006E-42)
            org.apache.http.params.HttpConnectionParams.setConnectionTimeout(r4, r11)     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            org.apache.http.impl.client.DefaultHttpClient r11 = new org.apache.http.impl.client.DefaultHttpClient     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            r11.<init>(r4)     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            org.apache.http.client.methods.HttpPost r4 = new org.apache.http.client.methods.HttpPost     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            java.lang.StringBuilder r17 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            java.lang.String r19 = "https://golfuser.yourgolf-online.com/userData/scoreUpload.xml?auth_token="
            r0 = r17
            r1 = r19
            r0.<init>(r1)     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            r0 = r26
            java.lang.String r0 = r0.c     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            r19 = r0
            java.lang.String r19 = java.net.URLEncoder.encode(r19)     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            r0 = r17
            r1 = r19
            java.lang.StringBuilder r17 = r0.append(r1)     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            java.lang.String r17 = r17.toString()     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            r0 = r4
            r1 = r17
            r0.<init>(r1)     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            org.apache.http.entity.ByteArrayEntity r17 = new org.apache.http.entity.ByteArrayEntity     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            byte[] r19 = r12.toByteArray()     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            r0 = r17
            r1 = r19
            r0.<init>(r1)     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            r0 = r4
            r1 = r17
            r0.setEntity(r1)     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            org.apache.http.HttpResponse r4 = r11.execute(r4)     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            org.apache.http.StatusLine r11 = r4.getStatusLine()     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            int r11 = r11.getStatusCode()     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            r17 = 200(0xc8, float:2.8E-43)
            r0 = r11
            r1 = r17
            if (r0 != r1) goto L_0x0a0f
            org.apache.http.HttpEntity r4 = r4.getEntity()     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            java.io.InputStream r4 = r4.getContent()     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            java.io.InputStreamReader r11 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0b9e, all -> 0x0b5a }
            r11.<init>(r4)     // Catch:{ Exception -> 0x0b9e, all -> 0x0b5a }
            java.io.BufferedReader r14 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0bac, all -> 0x0b67 }
            r14.<init>(r11)     // Catch:{ Exception -> 0x0bac, all -> 0x0b67 }
            org.xmlpull.v1.XmlPullParserFactory r15 = org.xmlpull.v1.XmlPullParserFactory.newInstance()     // Catch:{ Exception -> 0x0bbc, all -> 0x0b76 }
            r16 = 1
            r15.setNamespaceAware(r16)     // Catch:{ Exception -> 0x0bbc, all -> 0x0b76 }
            org.xmlpull.v1.XmlPullParser r15 = r15.newPullParser()     // Catch:{ Exception -> 0x0bbc, all -> 0x0b76 }
            r15.setInput(r14)     // Catch:{ Exception -> 0x0bbc, all -> 0x0b76 }
            int r16 = r15.getEventType()     // Catch:{ Exception -> 0x0bbc, all -> 0x0b76 }
            r17 = 0
            r19 = 0
            r24 = r19
            r19 = r16
            r16 = r24
        L_0x02db:
            r20 = 1
            r0 = r19
            r1 = r20
            if (r0 != r1) goto L_0x097a
            if (r16 == 0) goto L_0x09c3
            r0 = r26
            com.asai24.golf.a.b r0 = r0.a     // Catch:{ Exception -> 0x0bbc, all -> 0x0b76 }
            r15 = r0
            r0 = r15
            r1 = r27
            r3 = r16
            r0.c(r1, r3)     // Catch:{ Exception -> 0x0bbc, all -> 0x0b76 }
            com.asai24.golf.a r15 = com.asai24.golf.a.YOURGOLF_SUCCESS     // Catch:{ Exception -> 0x0bbc, all -> 0x0b76 }
            r12.close()     // Catch:{ IOException -> 0x0bd2 }
            r13.close()     // Catch:{ IOException -> 0x0bd2 }
        L_0x02fa:
            if (r14 == 0) goto L_0x02ff
            r14.close()     // Catch:{ IOException -> 0x09b1 }
        L_0x02ff:
            if (r11 == 0) goto L_0x0304
            r11.close()     // Catch:{ IOException -> 0x09b7 }
        L_0x0304:
            if (r4 == 0) goto L_0x0309
            r4.close()     // Catch:{ IOException -> 0x09bd }
        L_0x0309:
            r6.close()
            r7.close()
            r8.close()
            r5.close()
            r9.close()
            if (r10 == 0) goto L_0x031d
            r10.close()
        L_0x031d:
            if (r18 == 0) goto L_0x0322
            r18.close()
        L_0x0322:
            r4 = r15
            goto L_0x0036
        L_0x0325:
            r6.moveToPosition(r4)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r18 = ""
            java.lang.String r19 = "record"
            r17.startTag(r18, r19)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r18 = ""
            java.lang.String r19 = "id"
            java.lang.StringBuilder r20 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            long r21 = r6.b()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r21 = java.lang.String.valueOf(r21)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r20.<init>(r21)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r20 = r20.toString()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r17.attribute(r18, r19, r20)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r18 = ""
            java.lang.String r19 = "course_name"
            java.lang.String r20 = r6.d()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r17.attribute(r18, r19, r20)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r18 = ""
            java.lang.String r19 = "club_name"
            java.lang.String r20 = r6.c()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r17.attribute(r18, r19, r20)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r18 = ""
            java.lang.String r19 = "ext_id"
            java.lang.String r20 = r6.h()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r17.attribute(r18, r19, r20)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r18 = ""
            java.lang.String r19 = "ext_type"
            java.lang.StringBuilder r20 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r21 = r6.i()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r21 = java.lang.String.valueOf(r21)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r20.<init>(r21)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r20 = r20.toString()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r17.attribute(r18, r19, r20)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r18 = ""
            java.lang.String r19 = "created"
            java.lang.StringBuilder r20 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            long r21 = r6.j()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r21 = java.lang.String.valueOf(r21)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r20.<init>(r21)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r20 = r20.toString()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r17.attribute(r18, r19, r20)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r18 = ""
            java.lang.String r19 = "modified"
            java.lang.StringBuilder r20 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            long r21 = r6.k()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r21 = java.lang.String.valueOf(r21)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r20.<init>(r21)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r20 = r20.toString()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r17.attribute(r18, r19, r20)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r18 = ""
            java.lang.String r19 = "record"
            r17.endTag(r18, r19)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            int r4 = r4 + 1
            goto L_0x00e8
        L_0x03bb:
            r7.moveToPosition(r4)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r18 = ""
            java.lang.String r19 = "record"
            r17.startTag(r18, r19)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r18 = ""
            java.lang.String r19 = "id"
            java.lang.StringBuilder r20 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            long r21 = r7.c()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r21 = java.lang.String.valueOf(r21)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r20.<init>(r21)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r20 = r20.toString()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r17.attribute(r18, r19, r20)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r18 = ""
            java.lang.String r19 = "name"
            java.lang.String r20 = r7.d()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r17.attribute(r18, r19, r20)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r18 = ""
            java.lang.String r19 = "ext_id"
            java.lang.String r20 = r7.f()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r17.attribute(r18, r19, r20)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r18 = ""
            java.lang.String r19 = "ext_type"
            java.lang.String r20 = r7.g()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r17.attribute(r18, r19, r20)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r18 = ""
            java.lang.String r19 = "course_id"
            java.lang.StringBuilder r20 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            long r21 = r7.h()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r21 = java.lang.String.valueOf(r21)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r20.<init>(r21)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r20 = r20.toString()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r17.attribute(r18, r19, r20)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r18 = ""
            java.lang.String r19 = "created"
            java.lang.StringBuilder r20 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r21 = r7.i()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r21 = java.lang.String.valueOf(r21)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r20.<init>(r21)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r20 = r20.toString()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r17.attribute(r18, r19, r20)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r18 = ""
            java.lang.String r19 = "modified"
            java.lang.StringBuilder r20 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            long r21 = r7.j()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r21 = java.lang.String.valueOf(r21)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r20.<init>(r21)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r20 = r20.toString()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r17.attribute(r18, r19, r20)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r18 = ""
            java.lang.String r19 = "record"
            r17.endTag(r18, r19)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            int r4 = r4 + 1
            goto L_0x011a
        L_0x0451:
            r8.moveToPosition(r4)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r18 = ""
            java.lang.String r19 = "record"
            r17.startTag(r18, r19)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r18 = ""
            java.lang.String r19 = "id"
            java.lang.StringBuilder r20 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            long r21 = r8.b()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r21 = java.lang.String.valueOf(r21)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r20.<init>(r21)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r20 = r20.toString()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r17.attribute(r18, r19, r20)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r18 = ""
            java.lang.String r19 = "hole_number"
            java.lang.StringBuilder r20 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            int r21 = r8.d()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r21 = java.lang.String.valueOf(r21)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r20.<init>(r21)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r20 = r20.toString()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r17.attribute(r18, r19, r20)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r18 = ""
            java.lang.String r19 = "yard"
            java.lang.StringBuilder r20 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            int r21 = r8.l()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r21 = java.lang.String.valueOf(r21)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r20.<init>(r21)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r20 = r20.toString()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r17.attribute(r18, r19, r20)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r18 = ""
            java.lang.String r19 = "par"
            java.lang.StringBuilder r20 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            int r21 = r8.e()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r21 = java.lang.String.valueOf(r21)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r20.<init>(r21)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r20 = r20.toString()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r17.attribute(r18, r19, r20)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r18 = ""
            java.lang.String r19 = "women_par"
            java.lang.String r20 = "women_par"
            r0 = r8
            r1 = r20
            int r20 = r0.getColumnIndexOrThrow(r1)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r0 = r8
            r1 = r20
            java.lang.String r20 = r0.getString(r1)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r17.attribute(r18, r19, r20)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r18 = ""
            java.lang.String r19 = "handicap"
            java.lang.StringBuilder r20 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            int r21 = r8.h()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r21 = java.lang.String.valueOf(r21)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r20.<init>(r21)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r20 = r20.toString()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r17.attribute(r18, r19, r20)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r18 = ""
            java.lang.String r19 = "women_handicap"
            java.lang.String r20 = "women_handicap"
            r0 = r8
            r1 = r20
            int r20 = r0.getColumnIndexOrThrow(r1)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r0 = r8
            r1 = r20
            java.lang.String r20 = r0.getString(r1)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r17.attribute(r18, r19, r20)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r18 = ""
            java.lang.String r19 = "lat"
            java.lang.StringBuilder r20 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            double r21 = r8.m()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r21 = java.lang.String.valueOf(r21)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r20.<init>(r21)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r20 = r20.toString()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r17.attribute(r18, r19, r20)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r18 = ""
            java.lang.String r19 = "lng"
            java.lang.StringBuilder r20 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            double r21 = r8.n()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r21 = java.lang.String.valueOf(r21)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r20.<init>(r21)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r20 = r20.toString()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r17.attribute(r18, r19, r20)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r18 = ""
            java.lang.String r19 = "tee_id"
            java.lang.StringBuilder r20 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            long r21 = r8.c()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r21 = java.lang.String.valueOf(r21)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r20.<init>(r21)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r20 = r20.toString()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r17.attribute(r18, r19, r20)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r18 = ""
            java.lang.String r19 = "created"
            java.lang.StringBuilder r20 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            long r21 = r8.o()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r21 = java.lang.String.valueOf(r21)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r20.<init>(r21)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r20 = r20.toString()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r17.attribute(r18, r19, r20)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r18 = ""
            java.lang.String r19 = "modified"
            java.lang.StringBuilder r20 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            long r21 = r8.p()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r21 = java.lang.String.valueOf(r21)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r20.<init>(r21)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r20 = r20.toString()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r17.attribute(r18, r19, r20)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r18 = ""
            java.lang.String r19 = "record"
            r17.endTag(r18, r19)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            int r4 = r4 + 1
            goto L_0x014c
        L_0x0584:
            r5.moveToPosition(r4)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r18 = ""
            java.lang.String r19 = "record"
            r17.startTag(r18, r19)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r18 = ""
            java.lang.String r19 = "id"
            java.lang.StringBuilder r20 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            long r21 = r5.b()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r21 = java.lang.String.valueOf(r21)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r20.<init>(r21)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r20 = r20.toString()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r17.attribute(r18, r19, r20)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r18 = ""
            java.lang.String r19 = "tee_id"
            java.lang.StringBuilder r20 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            long r21 = r5.d()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r21 = java.lang.String.valueOf(r21)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r20.<init>(r21)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r20 = r20.toString()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r17.attribute(r18, r19, r20)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r18 = ""
            java.lang.String r19 = "result_id"
            java.lang.StringBuilder r20 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            long r21 = r5.j()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r21 = java.lang.String.valueOf(r21)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r20.<init>(r21)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r20 = r20.toString()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r17.attribute(r18, r19, r20)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r18 = r5.k()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            if (r18 == 0) goto L_0x060c
            java.lang.String r19 = ""
            boolean r19 = r18.equals(r19)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            if (r19 != 0) goto L_0x060c
            java.lang.String r19 = "null"
            boolean r19 = r18.equals(r19)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            if (r19 != 0) goto L_0x060c
            java.lang.String r19 = ""
            java.lang.String r20 = "yourgolf_id"
            java.lang.StringBuilder r21 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r18 = java.lang.String.valueOf(r18)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r0 = r21
            r1 = r18
            r0.<init>(r1)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r18 = r21.toString()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r0 = r17
            r1 = r19
            r2 = r20
            r3 = r18
            r0.attribute(r1, r2, r3)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
        L_0x060c:
            java.lang.String r18 = ""
            java.lang.String r19 = "created"
            java.lang.StringBuilder r20 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            long r21 = r5.h()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r21 = java.lang.String.valueOf(r21)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r20.<init>(r21)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r20 = r20.toString()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r17.attribute(r18, r19, r20)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r18 = ""
            java.lang.String r19 = "modified"
            java.lang.StringBuilder r20 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            long r21 = r5.i()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r21 = java.lang.String.valueOf(r21)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r20.<init>(r21)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r20 = r20.toString()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r17.attribute(r18, r19, r20)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r18 = ""
            java.lang.String r19 = "record"
            r17.endTag(r18, r19)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            int r4 = r4 + 1
            goto L_0x017e
        L_0x0647:
            r9.moveToPosition(r4)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r18 = ""
            java.lang.String r19 = "record"
            r17.startTag(r18, r19)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r18 = ""
            java.lang.String r19 = "id"
            java.lang.StringBuilder r20 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            long r21 = r9.b()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r21 = java.lang.String.valueOf(r21)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r20.<init>(r21)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r20 = r20.toString()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r17.attribute(r18, r19, r20)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r18 = ""
            java.lang.String r19 = "hole_score"
            java.lang.StringBuilder r20 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            int r21 = r9.c()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r21 = java.lang.String.valueOf(r21)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r20.<init>(r21)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r20 = r20.toString()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r17.attribute(r18, r19, r20)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r18 = ""
            java.lang.String r19 = "round_id"
            java.lang.StringBuilder r20 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            long r21 = r9.e()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r21 = java.lang.String.valueOf(r21)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r20.<init>(r21)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r20 = r20.toString()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r17.attribute(r18, r19, r20)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r18 = ""
            java.lang.String r19 = "hole_id"
            java.lang.StringBuilder r20 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            long r21 = r9.d()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r21 = java.lang.String.valueOf(r21)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r20.<init>(r21)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r20 = r20.toString()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r17.attribute(r18, r19, r20)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r18 = ""
            java.lang.String r19 = "player_id"
            java.lang.StringBuilder r20 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            long r21 = r9.f()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r21 = java.lang.String.valueOf(r21)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r20.<init>(r21)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r20 = r20.toString()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r17.attribute(r18, r19, r20)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r18 = ""
            java.lang.String r19 = "game_score"
            java.lang.StringBuilder r20 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            int r21 = r9.g()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r21 = java.lang.String.valueOf(r21)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r20.<init>(r21)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r20 = r20.toString()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r17.attribute(r18, r19, r20)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r18 = ""
            java.lang.String r19 = "created"
            java.lang.StringBuilder r20 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            long r21 = r9.h()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r21 = java.lang.String.valueOf(r21)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r20.<init>(r21)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r20 = r20.toString()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r17.attribute(r18, r19, r20)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r18 = ""
            java.lang.String r19 = "modified"
            java.lang.StringBuilder r20 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            long r21 = r9.i()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r21 = java.lang.String.valueOf(r21)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r20.<init>(r21)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r20 = r20.toString()     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            r17.attribute(r18, r19, r20)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            java.lang.String r18 = ""
            java.lang.String r19 = "record"
            r17.endTag(r18, r19)     // Catch:{ Exception -> 0x0b84, all -> 0x0af6 }
            int r4 = r4 + 1
            goto L_0x01b0
        L_0x071c:
            r0 = r26
            com.asai24.golf.a.b r0 = r0.a     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            r18 = r0
            long r19 = r9.b()     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            com.asai24.golf.a.g r10 = r18.m(r19)     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            r18 = 0
        L_0x072c:
            int r19 = r10.getCount()     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            r0 = r18
            r1 = r19
            if (r0 < r1) goto L_0x077c
            r9.moveToNext()     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            goto L_0x01e6
        L_0x073b:
            r4 = move-exception
            r24 = r16
            r16 = r10
            r10 = r24
            r25 = r11
            r11 = r15
            r15 = r25
        L_0x0747:
            com.asai24.golf.d.c.a(r4)     // Catch:{ all -> 0x0b7f }
            com.asai24.golf.a r4 = com.asai24.golf.a.YOURGOLF_ERROR     // Catch:{ all -> 0x0b7f }
            r12.close()     // Catch:{ IOException -> 0x0b81 }
            r13.close()     // Catch:{ IOException -> 0x0b81 }
        L_0x0752:
            if (r10 == 0) goto L_0x0757
            r10.close()     // Catch:{ IOException -> 0x0ae4 }
        L_0x0757:
            if (r11 == 0) goto L_0x075c
            r11.close()     // Catch:{ IOException -> 0x0aea }
        L_0x075c:
            if (r14 == 0) goto L_0x0761
            r14.close()     // Catch:{ IOException -> 0x0af0 }
        L_0x0761:
            r6.close()
            r7.close()
            r8.close()
            r5.close()
            r9.close()
            if (r16 == 0) goto L_0x0775
            r16.close()
        L_0x0775:
            if (r15 == 0) goto L_0x0036
            r15.close()
            goto L_0x0036
        L_0x077c:
            r0 = r10
            r1 = r18
            r0.moveToPosition(r1)     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            java.lang.String r19 = ""
            java.lang.String r20 = "record"
            r0 = r17
            r1 = r19
            r2 = r20
            r0.startTag(r1, r2)     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            java.lang.String r19 = ""
            java.lang.String r20 = "id"
            java.lang.StringBuilder r21 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            long r22 = r10.b()     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            java.lang.String r22 = java.lang.String.valueOf(r22)     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            r21.<init>(r22)     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            java.lang.String r21 = r21.toString()     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            r0 = r17
            r1 = r19
            r2 = r20
            r3 = r21
            r0.attribute(r1, r2, r3)     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            java.lang.String r19 = ""
            java.lang.String r20 = "shot_number"
            java.lang.StringBuilder r21 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            int r22 = r10.d()     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            java.lang.String r22 = java.lang.String.valueOf(r22)     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            r21.<init>(r22)     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            java.lang.String r21 = r21.toString()     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            r0 = r17
            r1 = r19
            r2 = r20
            r3 = r21
            r0.attribute(r1, r2, r3)     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            java.lang.String r19 = ""
            java.lang.String r20 = "lat"
            java.lang.StringBuilder r21 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            double r22 = r10.g()     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            java.lang.String r22 = java.lang.String.valueOf(r22)     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            r21.<init>(r22)     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            java.lang.String r21 = r21.toString()     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            r0 = r17
            r1 = r19
            r2 = r20
            r3 = r21
            r0.attribute(r1, r2, r3)     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            java.lang.String r19 = ""
            java.lang.String r20 = "lng"
            java.lang.StringBuilder r21 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            double r22 = r10.h()     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            java.lang.String r22 = java.lang.String.valueOf(r22)     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            r21.<init>(r22)     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            java.lang.String r21 = r21.toString()     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            r0 = r17
            r1 = r19
            r2 = r20
            r3 = r21
            r0.attribute(r1, r2, r3)     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            java.lang.String r19 = ""
            java.lang.String r20 = "club"
            java.lang.String r21 = r10.e()     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            r0 = r4
            r1 = r21
            java.lang.String r21 = r0.a(r1)     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            r0 = r17
            r1 = r19
            r2 = r20
            r3 = r21
            r0.attribute(r1, r2, r3)     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            java.lang.String r19 = ""
            java.lang.String r20 = "shot_result"
            java.lang.StringBuilder r21 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            java.lang.String r22 = r10.f()     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            java.lang.String r22 = java.lang.String.valueOf(r22)     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            r21.<init>(r22)     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            java.lang.String r21 = r21.toString()     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            r0 = r17
            r1 = r19
            r2 = r20
            r3 = r21
            r0.attribute(r1, r2, r3)     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            java.lang.String r19 = ""
            java.lang.String r20 = "score_id"
            java.lang.StringBuilder r21 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            long r22 = r10.c()     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            java.lang.String r22 = java.lang.String.valueOf(r22)     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            r21.<init>(r22)     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            java.lang.String r21 = r21.toString()     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            r0 = r17
            r1 = r19
            r2 = r20
            r3 = r21
            r0.attribute(r1, r2, r3)     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            java.lang.String r19 = ""
            java.lang.String r20 = "created"
            java.lang.StringBuilder r21 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            long r22 = r10.i()     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            java.lang.String r22 = java.lang.String.valueOf(r22)     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            r21.<init>(r22)     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            java.lang.String r21 = r21.toString()     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            r0 = r17
            r1 = r19
            r2 = r20
            r3 = r21
            r0.attribute(r1, r2, r3)     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            java.lang.String r19 = ""
            java.lang.String r20 = "modified"
            java.lang.StringBuilder r21 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            long r22 = r10.j()     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            java.lang.String r22 = java.lang.String.valueOf(r22)     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            r21.<init>(r22)     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            java.lang.String r21 = r21.toString()     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            r0 = r17
            r1 = r19
            r2 = r20
            r3 = r21
            r0.attribute(r1, r2, r3)     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            java.lang.String r19 = ""
            java.lang.String r20 = "record"
            r0 = r17
            r1 = r19
            r2 = r20
            r0.endTag(r1, r2)     // Catch:{ Exception -> 0x073b, all -> 0x0b42 }
            int r18 = r18 + 1
            goto L_0x072c
        L_0x08ba:
            r0 = r26
            com.asai24.golf.a.b r0 = r0.a     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            r19 = r0
            r20 = r4[r11]     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            com.asai24.golf.a.d r18 = r19.b(r20)     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            java.lang.String r19 = ""
            java.lang.String r20 = "record"
            r0 = r17
            r1 = r19
            r2 = r20
            r0.startTag(r1, r2)     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            java.lang.String r19 = ""
            java.lang.String r20 = "id"
            java.lang.StringBuilder r21 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            long r22 = r18.b()     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            java.lang.String r22 = java.lang.String.valueOf(r22)     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            r21.<init>(r22)     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            java.lang.String r21 = r21.toString()     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            r0 = r17
            r1 = r19
            r2 = r20
            r3 = r21
            r0.attribute(r1, r2, r3)     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            java.lang.String r19 = ""
            java.lang.String r20 = "name"
            java.lang.String r21 = r18.c()     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            r0 = r17
            r1 = r19
            r2 = r20
            r3 = r21
            r0.attribute(r1, r2, r3)     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            java.lang.String r19 = ""
            java.lang.String r20 = "ownner_flag"
            java.lang.StringBuilder r21 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            long r22 = r18.d()     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            java.lang.String r22 = java.lang.String.valueOf(r22)     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            r21.<init>(r22)     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            java.lang.String r21 = r21.toString()     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            r0 = r17
            r1 = r19
            r2 = r20
            r3 = r21
            r0.attribute(r1, r2, r3)     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            java.lang.String r19 = ""
            java.lang.String r20 = "created"
            java.lang.StringBuilder r21 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            java.lang.String r22 = r18.e()     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            java.lang.String r22 = java.lang.String.valueOf(r22)     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            r21.<init>(r22)     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            java.lang.String r21 = r21.toString()     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            r0 = r17
            r1 = r19
            r2 = r20
            r3 = r21
            r0.attribute(r1, r2, r3)     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            java.lang.String r19 = ""
            java.lang.String r20 = "modified"
            java.lang.StringBuilder r21 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            long r22 = r18.f()     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            java.lang.String r22 = java.lang.String.valueOf(r22)     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            r21.<init>(r22)     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            java.lang.String r21 = r21.toString()     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            r0 = r17
            r1 = r19
            r2 = r20
            r3 = r21
            r0.attribute(r1, r2, r3)     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            java.lang.String r19 = ""
            java.lang.String r20 = "record"
            r0 = r17
            r1 = r19
            r2 = r20
            r0.endTag(r1, r2)     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            r18.close()     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            int r11 = r11 + 1
            goto L_0x0228
        L_0x097a:
            if (r19 == 0) goto L_0x0992
            r20 = 2
            r0 = r19
            r1 = r20
            if (r0 != r1) goto L_0x0998
            java.lang.String r19 = r15.getName()     // Catch:{ Exception -> 0x0bbc, all -> 0x0b76 }
            java.lang.String r20 = "id"
            boolean r19 = r19.equals(r20)     // Catch:{ Exception -> 0x0bbc, all -> 0x0b76 }
            if (r19 == 0) goto L_0x0992
            r17 = 1
        L_0x0992:
            int r19 = r15.next()     // Catch:{ Exception -> 0x0bbc, all -> 0x0b76 }
            goto L_0x02db
        L_0x0998:
            r20 = 3
            r0 = r19
            r1 = r20
            if (r0 == r1) goto L_0x0992
            r20 = 4
            r0 = r19
            r1 = r20
            if (r0 != r1) goto L_0x0992
            switch(r17) {
                case 1: goto L_0x09ac;
                default: goto L_0x09ab;
            }     // Catch:{ Exception -> 0x0bbc, all -> 0x0b76 }
        L_0x09ab:
            goto L_0x0992
        L_0x09ac:
            java.lang.String r16 = r15.getText()     // Catch:{ Exception -> 0x0bbc, all -> 0x0b76 }
            goto L_0x0992
        L_0x09b1:
            r12 = move-exception
            r12.printStackTrace()
            goto L_0x02ff
        L_0x09b7:
            r11 = move-exception
            r11.printStackTrace()
            goto L_0x0304
        L_0x09bd:
            r4 = move-exception
            r4.printStackTrace()
            goto L_0x0309
        L_0x09c3:
            com.asai24.golf.b.d r15 = new com.asai24.golf.b.d     // Catch:{ Exception -> 0x0bbc, all -> 0x0b76 }
            java.lang.String r16 = "Unknown error has occurred at Upload Transaction."
            r15.<init>(r16)     // Catch:{ Exception -> 0x0bbc, all -> 0x0b76 }
            com.asai24.golf.d.c.a(r15)     // Catch:{ Exception -> 0x0bbc, all -> 0x0b76 }
            com.asai24.golf.a r15 = com.asai24.golf.a.YOURGOLF_ERROR     // Catch:{ Exception -> 0x0bbc, all -> 0x0b76 }
            r12.close()     // Catch:{ IOException -> 0x0bcf }
            r13.close()     // Catch:{ IOException -> 0x0bcf }
        L_0x09d5:
            if (r14 == 0) goto L_0x09da
            r14.close()     // Catch:{ IOException -> 0x0a00 }
        L_0x09da:
            if (r11 == 0) goto L_0x09df
            r11.close()     // Catch:{ IOException -> 0x0a05 }
        L_0x09df:
            if (r4 == 0) goto L_0x09e4
            r4.close()     // Catch:{ IOException -> 0x0a0a }
        L_0x09e4:
            r6.close()
            r7.close()
            r8.close()
            r5.close()
            r9.close()
            if (r10 == 0) goto L_0x09f8
            r10.close()
        L_0x09f8:
            if (r18 == 0) goto L_0x09fd
            r18.close()
        L_0x09fd:
            r4 = r15
            goto L_0x0036
        L_0x0a00:
            r12 = move-exception
            r12.printStackTrace()
            goto L_0x09da
        L_0x0a05:
            r11 = move-exception
            r11.printStackTrace()
            goto L_0x09df
        L_0x0a0a:
            r4 = move-exception
            r4.printStackTrace()
            goto L_0x09e4
        L_0x0a0f:
            r4 = 401(0x191, float:5.62E-43)
            if (r11 != r4) goto L_0x0a54
            com.asai24.golf.a r4 = com.asai24.golf.a.YOURGOLF_INVALID_TOKEN     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            r12.close()     // Catch:{ IOException -> 0x0bcc }
            r13.close()     // Catch:{ IOException -> 0x0bcc }
        L_0x0a1b:
            if (r16 == 0) goto L_0x0a20
            r16.close()     // Catch:{ IOException -> 0x0a45 }
        L_0x0a20:
            if (r15 == 0) goto L_0x0a25
            r15.close()     // Catch:{ IOException -> 0x0a4a }
        L_0x0a25:
            if (r14 == 0) goto L_0x0a2a
            r14.close()     // Catch:{ IOException -> 0x0a4f }
        L_0x0a2a:
            r6.close()
            r7.close()
            r8.close()
            r5.close()
            r9.close()
            if (r10 == 0) goto L_0x0a3e
            r10.close()
        L_0x0a3e:
            if (r18 == 0) goto L_0x0036
            r18.close()
            goto L_0x0036
        L_0x0a45:
            r11 = move-exception
            r11.printStackTrace()
            goto L_0x0a20
        L_0x0a4a:
            r11 = move-exception
            r11.printStackTrace()
            goto L_0x0a25
        L_0x0a4f:
            r11 = move-exception
            r11.printStackTrace()
            goto L_0x0a2a
        L_0x0a54:
            r4 = 500(0x1f4, float:7.0E-43)
            if (r11 != r4) goto L_0x0aa3
            com.asai24.golf.b.d r4 = new com.asai24.golf.b.d     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            java.lang.String r11 = "Unknown error has occurred at YourGolf-Online server transaction."
            r4.<init>(r11)     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            com.asai24.golf.d.c.a(r4)     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            com.asai24.golf.a r4 = com.asai24.golf.a.YOURGOLF_UNEXPECTED_ERROR     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            r12.close()     // Catch:{ IOException -> 0x0bc9 }
            r13.close()     // Catch:{ IOException -> 0x0bc9 }
        L_0x0a6a:
            if (r16 == 0) goto L_0x0a6f
            r16.close()     // Catch:{ IOException -> 0x0a94 }
        L_0x0a6f:
            if (r15 == 0) goto L_0x0a74
            r15.close()     // Catch:{ IOException -> 0x0a99 }
        L_0x0a74:
            if (r14 == 0) goto L_0x0a79
            r14.close()     // Catch:{ IOException -> 0x0a9e }
        L_0x0a79:
            r6.close()
            r7.close()
            r8.close()
            r5.close()
            r9.close()
            if (r10 == 0) goto L_0x0a8d
            r10.close()
        L_0x0a8d:
            if (r18 == 0) goto L_0x0036
            r18.close()
            goto L_0x0036
        L_0x0a94:
            r11 = move-exception
            r11.printStackTrace()
            goto L_0x0a6f
        L_0x0a99:
            r11 = move-exception
            r11.printStackTrace()
            goto L_0x0a74
        L_0x0a9e:
            r11 = move-exception
            r11.printStackTrace()
            goto L_0x0a79
        L_0x0aa3:
            com.asai24.golf.a r4 = com.asai24.golf.a.YOURGOLF_ERROR     // Catch:{ Exception -> 0x0b92, all -> 0x0b4f }
            r12.close()     // Catch:{ IOException -> 0x0bc6 }
            r13.close()     // Catch:{ IOException -> 0x0bc6 }
        L_0x0aab:
            if (r16 == 0) goto L_0x0ab0
            r16.close()     // Catch:{ IOException -> 0x0ad5 }
        L_0x0ab0:
            if (r15 == 0) goto L_0x0ab5
            r15.close()     // Catch:{ IOException -> 0x0ada }
        L_0x0ab5:
            if (r14 == 0) goto L_0x0aba
            r14.close()     // Catch:{ IOException -> 0x0adf }
        L_0x0aba:
            r6.close()
            r7.close()
            r8.close()
            r5.close()
            r9.close()
            if (r10 == 0) goto L_0x0ace
            r10.close()
        L_0x0ace:
            if (r18 == 0) goto L_0x0036
            r18.close()
            goto L_0x0036
        L_0x0ad5:
            r11 = move-exception
            r11.printStackTrace()
            goto L_0x0ab0
        L_0x0ada:
            r11 = move-exception
            r11.printStackTrace()
            goto L_0x0ab5
        L_0x0adf:
            r11 = move-exception
            r11.printStackTrace()
            goto L_0x0aba
        L_0x0ae4:
            r10 = move-exception
            r10.printStackTrace()
            goto L_0x0757
        L_0x0aea:
            r10 = move-exception
            r10.printStackTrace()
            goto L_0x075c
        L_0x0af0:
            r10 = move-exception
            r10.printStackTrace()
            goto L_0x0761
        L_0x0af6:
            r4 = move-exception
            r24 = r16
            r16 = r10
            r10 = r24
            r25 = r11
            r11 = r15
            r15 = r25
        L_0x0b02:
            r12.close()     // Catch:{ IOException -> 0x0b40 }
            r13.close()     // Catch:{ IOException -> 0x0b40 }
        L_0x0b08:
            if (r10 == 0) goto L_0x0b0d
            r10.close()     // Catch:{ IOException -> 0x0b31 }
        L_0x0b0d:
            if (r11 == 0) goto L_0x0b12
            r11.close()     // Catch:{ IOException -> 0x0b36 }
        L_0x0b12:
            if (r14 == 0) goto L_0x0b17
            r14.close()     // Catch:{ IOException -> 0x0b3b }
        L_0x0b17:
            r6.close()
            r7.close()
            r8.close()
            r5.close()
            r9.close()
            if (r16 == 0) goto L_0x0b2b
            r16.close()
        L_0x0b2b:
            if (r15 == 0) goto L_0x0b30
            r15.close()
        L_0x0b30:
            throw r4
        L_0x0b31:
            r10 = move-exception
            r10.printStackTrace()
            goto L_0x0b0d
        L_0x0b36:
            r10 = move-exception
            r10.printStackTrace()
            goto L_0x0b12
        L_0x0b3b:
            r10 = move-exception
            r10.printStackTrace()
            goto L_0x0b17
        L_0x0b40:
            r12 = move-exception
            goto L_0x0b08
        L_0x0b42:
            r4 = move-exception
            r24 = r16
            r16 = r10
            r10 = r24
            r25 = r11
            r11 = r15
            r15 = r25
            goto L_0x0b02
        L_0x0b4f:
            r4 = move-exception
            r11 = r15
            r15 = r18
            r24 = r16
            r16 = r10
            r10 = r24
            goto L_0x0b02
        L_0x0b5a:
            r11 = move-exception
            r14 = r4
            r4 = r11
            r11 = r15
            r15 = r18
            r24 = r16
            r16 = r10
            r10 = r24
            goto L_0x0b02
        L_0x0b67:
            r14 = move-exception
            r15 = r18
            r24 = r16
            r16 = r10
            r10 = r24
            r25 = r14
            r14 = r4
            r4 = r25
            goto L_0x0b02
        L_0x0b76:
            r15 = move-exception
            r16 = r10
            r10 = r14
            r14 = r4
            r4 = r15
            r15 = r18
            goto L_0x0b02
        L_0x0b7f:
            r4 = move-exception
            goto L_0x0b02
        L_0x0b81:
            r12 = move-exception
            goto L_0x0752
        L_0x0b84:
            r4 = move-exception
            r24 = r16
            r16 = r10
            r10 = r24
            r25 = r11
            r11 = r15
            r15 = r25
            goto L_0x0747
        L_0x0b92:
            r4 = move-exception
            r11 = r15
            r15 = r18
            r24 = r16
            r16 = r10
            r10 = r24
            goto L_0x0747
        L_0x0b9e:
            r11 = move-exception
            r14 = r4
            r4 = r11
            r11 = r15
            r15 = r18
            r24 = r16
            r16 = r10
            r10 = r24
            goto L_0x0747
        L_0x0bac:
            r14 = move-exception
            r15 = r18
            r24 = r16
            r16 = r10
            r10 = r24
            r25 = r14
            r14 = r4
            r4 = r25
            goto L_0x0747
        L_0x0bbc:
            r15 = move-exception
            r16 = r10
            r10 = r14
            r14 = r4
            r4 = r15
            r15 = r18
            goto L_0x0747
        L_0x0bc6:
            r11 = move-exception
            goto L_0x0aab
        L_0x0bc9:
            r11 = move-exception
            goto L_0x0a6a
        L_0x0bcc:
            r11 = move-exception
            goto L_0x0a1b
        L_0x0bcf:
            r12 = move-exception
            goto L_0x09d5
        L_0x0bd2:
            r12 = move-exception
            goto L_0x02fa
        */
        throw new UnsupportedOperationException("Method not decompiled: com.asai24.golf.c.h.a(long, boolean):com.asai24.golf.a");
    }
}
