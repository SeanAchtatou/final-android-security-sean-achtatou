package com.asai24.golf.c;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.provider.Settings;
import com.asai24.golf.R;
import com.asai24.golf.a.b;
import com.asai24.golf.a.e;
import com.asai24.golf.a.g;
import com.asai24.golf.a.p;
import com.asai24.golf.a.s;
import com.asai24.golf.a.y;
import com.asai24.golf.b.a;
import com.asai24.golf.b.d;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Calendar;
import java.util.TimeZone;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

public class k extends e {
    private b a;
    private Context b;
    private String c;
    private String d;
    private String e;
    private String f;

    public k(Context context) {
        this.b = context;
        this.a = b.a(context);
    }

    private int a(long j, String str) {
        g m = this.a.m(j);
        int i = 0;
        for (int i2 = 0; i2 < m.getCount(); i2++) {
            m.moveToPosition(i2);
            String f2 = m.f();
            if (f2 != null && f2.equals(str)) {
                i++;
            }
        }
        m.close();
        return i;
    }

    private int a(Reader reader) {
        int i;
        int i2;
        boolean z;
        boolean z2;
        XmlPullParserFactory newInstance = XmlPullParserFactory.newInstance();
        newInstance.setNamespaceAware(true);
        XmlPullParser newPullParser = newInstance.newPullParser();
        newPullParser.setInput(reader);
        boolean z3 = false;
        int i3 = 0;
        int i4 = 0;
        int i5 = 0;
        int i6 = 0;
        int i7 = 0;
        int i8 = 0;
        int i9 = 0;
        int i10 = -1;
        int i11 = 0;
        boolean z4 = false;
        for (int eventType = newPullParser.getEventType(); eventType != 1; eventType = newPullParser.next()) {
            if (eventType == 0) {
                i8++;
                i = i10;
                i2 = i4;
                z = z4;
                z2 = z3;
            } else {
                if (eventType == 2) {
                    i7++;
                    if (newPullParser.getName().equals("score")) {
                        i6++;
                        i = i10;
                        i2 = i4;
                        z = z4;
                        z2 = z3;
                    } else if (newPullParser.getName().equals("error")) {
                        i9++;
                        z2 = z3;
                        z = true;
                        i = i10;
                        i2 = Integer.parseInt(newPullParser.getAttributeValue(0));
                    } else if (newPullParser.getName().equals("id")) {
                        i5++;
                        int i12 = i10;
                        i2 = i4;
                        z = z4;
                        z2 = true;
                        i = i12;
                    }
                } else if (eventType == 3) {
                    i11++;
                    i = i10;
                    i2 = i4;
                    z = z4;
                    z2 = z3;
                } else if (eventType == 4) {
                    i3++;
                    if (z3) {
                        i = Integer.parseInt(newPullParser.getText());
                        int i13 = i4;
                        z = z4;
                        z2 = !z3;
                        i2 = i13;
                    }
                }
                i = i10;
                i2 = i4;
                z = z4;
                z2 = z3;
            }
            z3 = z2;
            z4 = z;
            i4 = i2;
            i10 = i;
        }
        String str = String.valueOf(z4) + "," + i10 + ". " + i8 + "," + i7 + "," + i6 + "," + i9 + "," + i5 + "," + i11 + "," + i3 + ". ";
        if (!z4 && i10 != -1) {
            return i10;
        }
        switch (i4) {
            case 1004:
                throw new a(i4);
        }
        throw new d(i4, String.valueOf(this.d) + this.e + this.f + str + this.c);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.asai24.golf.c.e.a(org.apache.http.client.HttpClient, android.content.Context):void
     arg types: [org.apache.http.impl.client.DefaultHttpClient, android.content.Context]
     candidates:
      com.asai24.golf.c.k.a(long, java.lang.String):int
      com.asai24.golf.c.k.a(long, boolean):com.asai24.golf.a
      com.asai24.golf.c.e.a(java.lang.String, java.lang.Object[]):void
      com.asai24.golf.c.e.a(org.apache.http.client.HttpClient, android.content.Context):void */
    private Reader a(String str) {
        HttpGet httpGet = new HttpGet(str);
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        a((HttpClient) defaultHttpClient, this.b);
        HttpResponse execute = defaultHttpClient.execute(httpGet);
        Header[] allHeaders = execute.getAllHeaders();
        this.d = "";
        for (Header header : allHeaders) {
            this.d = String.valueOf(this.d) + "Header: " + header.getName() + ", " + header.getValue() + ". ";
        }
        this.e = "Header length: " + allHeaders.length + ". ";
        this.f = "status line: " + execute.getStatusLine().toString() + ". ";
        return new BufferedReader(new InputStreamReader(execute.getEntity().getContent()));
    }

    private int b(long j) {
        int i = 0;
        g m = this.a.m(j);
        com.asai24.golf.e.a aVar = new com.asai24.golf.e.a(this.b);
        for (int i2 = 0; i2 < m.getCount(); i2++) {
            m.moveToPosition(i2);
            if (aVar.a(m.e()).equals("pt")) {
                i++;
            }
        }
        m.close();
        return i;
    }

    private String b(long j, boolean z) {
        Resources resources = this.b.getResources();
        p o = this.a.o(j);
        e r = this.a.r(o.d());
        s a2 = this.a.a(r.c(), 0);
        y c2 = this.a.c(o.b(), 1, r.c());
        com.asai24.golf.e.a aVar = new com.asai24.golf.e.a(this.b);
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(resources.getString(R.string.oob_save_score_hbh_api));
        stringBuffer.append("?dev=" + resources.getString(R.string.oob_api_key));
        if (!z) {
            stringBuffer.append("&id=" + o.j());
        }
        stringBuffer.append("&play_time=" + c(o.h()));
        stringBuffer.append("&tee_id=" + r.e());
        stringBuffer.append("&session=" + new g(this.b).a());
        switch (this.a.C(j)) {
            case 0:
                stringBuffer.append("&nine=0");
                break;
            case 1:
                stringBuffer.append("&nine=1");
                break;
            case 2:
                stringBuffer.append("&nine=1");
                stringBuffer.append("&back=1");
                break;
            case 3:
                throw new com.asai24.golf.b.b();
        }
        double[] dArr = new double[2];
        double[] dArr2 = new double[2];
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= c2.getCount()) {
                o.close();
                r.close();
                a2.close();
                c2.close();
                return stringBuffer.toString();
            }
            c2.moveToPosition(i2);
            long e2 = c2.e();
            int c3 = c2.c();
            stringBuffer.append("&h" + c3 + "_score=" + c2.b());
            stringBuffer.append("&h" + c3 + "_putts=" + b(e2));
            stringBuffer.append("&h" + c3 + "_penalties=" + a(e2, resources.getString(R.string.result_penalty)));
            stringBuffer.append("&h" + c3 + "_sand_shots=" + a(e2, resources.getString(R.string.result_bunker)));
            g m = this.a.m(e2);
            int i3 = 0;
            while (true) {
                if (i3 < m.getCount()) {
                    m.moveToPosition(i3);
                    if (m.d() == 1) {
                        String e3 = m.e();
                        if (e3 != null && !e3.trim().equals("") && !e3.trim().equals(resources.getString(R.string.club_pt))) {
                            stringBuffer.append("&h" + c3 + "_tee_club=" + aVar.a(e3.trim()));
                        }
                        dArr[0] = m.g();
                        dArr[1] = m.h();
                        String f2 = m.f();
                        if (f2 != null && !f2.trim().equals("")) {
                            if (f2.trim().equals(resources.getString(R.string.result_fairway))) {
                                stringBuffer.append("&h" + c3 + "_fairway=1");
                            } else {
                                stringBuffer.append("&h" + c3 + "_fairway=0");
                            }
                        }
                        i3++;
                    } else if (m.d() == 2) {
                        dArr2[0] = m.g();
                        dArr2[1] = m.h();
                        if (!((dArr[0] == 0.0d && dArr[1] == 0.0d) || (dArr2[0] == 0.0d && dArr2[1] == 0.0d))) {
                            stringBuffer.append("&h" + c3 + "_tee_dist=" + new StringBuilder().append(com.asai24.golf.f.a.a(com.asai24.golf.d.e.a(dArr[0], dArr[1], dArr2[0], dArr2[1]))).toString());
                        }
                    }
                }
            }
            m.close();
            i = i2 + 1;
        }
    }

    private int c(long j) {
        TimeZone timeZone = TimeZone.getTimeZone("America/New_York");
        Configuration configuration = new Configuration();
        Settings.System.getConfiguration(this.b.getContentResolver(), configuration);
        return (int) (com.asai24.golf.d.b.a(j, Calendar.getInstance(configuration.locale).getTimeZone(), timeZone) / 1000);
    }

    public com.asai24.golf.a a(long j) {
        p o = this.a.o(j);
        boolean z = o.j() == 0;
        o.close();
        return a(j, z);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:?, code lost:
        r1 = com.asai24.golf.a.OOB_NO_SETTINGS;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0047, code lost:
        if (r0 != null) goto L_0x0049;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        r1 = com.asai24.golf.a.OOB_INVALID_SESSION;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0051, code lost:
        if (r0 != null) goto L_0x0053;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
        r1 = com.asai24.golf.a.OOB_ZERO_SCORE;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x005b, code lost:
        if (r0 != null) goto L_0x005d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:?, code lost:
        r1 = com.asai24.golf.a.OOB_NO_PERMISSION;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0065, code lost:
        if (r0 != null) goto L_0x0067;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:?, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x009a, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x009b, code lost:
        r5 = r1;
        r1 = r0;
        r0 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x00a1, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x00a2, code lost:
        r5 = r1;
        r1 = r0;
        r0 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:?, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:?, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:?, code lost:
        return r0;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x004e A[ExcHandler: e (e com.asai24.golf.b.e), Splitter:B:1:0x0001] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0058 A[ExcHandler: b (e com.asai24.golf.b.b), Splitter:B:1:0x0001] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0062 A[ExcHandler: a (e com.asai24.golf.b.a), Splitter:B:1:0x0001] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x007e A[SYNTHETIC, Splitter:B:43:0x007e] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x008a A[SYNTHETIC, Splitter:B:49:0x008a] */
    /* JADX WARNING: Removed duplicated region for block: B:68:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0044 A[ExcHandler: c (e com.asai24.golf.b.c), Splitter:B:1:0x0001] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.asai24.golf.a a(long r7, boolean r9) {
        /*
            r6 = this;
            r0 = 0
            java.lang.String r1 = r6.b(r7, r9)     // Catch:{ c -> 0x0044, e -> 0x004e, b -> 0x0058, a -> 0x0062, Exception -> 0x006c, all -> 0x0084 }
            java.lang.String r2 = "&"
            int r2 = r1.indexOf(r2)     // Catch:{ c -> 0x0044, e -> 0x004e, b -> 0x0058, a -> 0x0062, Exception -> 0x006c, all -> 0x0084 }
            java.lang.String r3 = ""
            r6.c = r3     // Catch:{ c -> 0x0044, e -> 0x004e, b -> 0x0058, a -> 0x0062, Exception -> 0x006c, all -> 0x0084 }
            java.lang.String r3 = r6.c     // Catch:{ c -> 0x0044, e -> 0x004e, b -> 0x0058, a -> 0x0062, Exception -> 0x006c, all -> 0x0084 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ c -> 0x0044, e -> 0x004e, b -> 0x0058, a -> 0x0062, Exception -> 0x006c, all -> 0x0084 }
            java.lang.String r3 = java.lang.String.valueOf(r3)     // Catch:{ c -> 0x0044, e -> 0x004e, b -> 0x0058, a -> 0x0062, Exception -> 0x006c, all -> 0x0084 }
            r4.<init>(r3)     // Catch:{ c -> 0x0044, e -> 0x004e, b -> 0x0058, a -> 0x0062, Exception -> 0x006c, all -> 0x0084 }
            int r2 = r2 + 1
            int r3 = r1.length()     // Catch:{ c -> 0x0044, e -> 0x004e, b -> 0x0058, a -> 0x0062, Exception -> 0x006c, all -> 0x0084 }
            java.lang.String r2 = r1.substring(r2, r3)     // Catch:{ c -> 0x0044, e -> 0x004e, b -> 0x0058, a -> 0x0062, Exception -> 0x006c, all -> 0x0084 }
            java.lang.StringBuilder r2 = r4.append(r2)     // Catch:{ c -> 0x0044, e -> 0x004e, b -> 0x0058, a -> 0x0062, Exception -> 0x006c, all -> 0x0084 }
            java.lang.String r2 = r2.toString()     // Catch:{ c -> 0x0044, e -> 0x004e, b -> 0x0058, a -> 0x0062, Exception -> 0x006c, all -> 0x0084 }
            r6.c = r2     // Catch:{ c -> 0x0044, e -> 0x004e, b -> 0x0058, a -> 0x0062, Exception -> 0x006c, all -> 0x0084 }
            java.io.Reader r0 = r6.a(r1)     // Catch:{ c -> 0x0044, e -> 0x004e, b -> 0x0058, a -> 0x0062, Exception -> 0x006c, all -> 0x0084 }
            int r1 = r6.a(r0)     // Catch:{ c -> 0x0044, e -> 0x004e, b -> 0x0058, a -> 0x0062, Exception -> 0x00a1 }
            com.asai24.golf.a.b r2 = r6.a     // Catch:{ c -> 0x0044, e -> 0x004e, b -> 0x0058, a -> 0x0062, Exception -> 0x00a1 }
            r2.f(r7, r1)     // Catch:{ c -> 0x0044, e -> 0x004e, b -> 0x0058, a -> 0x0062, Exception -> 0x00a1 }
            com.asai24.golf.a r1 = com.asai24.golf.a.OOB_SUCCESS     // Catch:{ c -> 0x0044, e -> 0x004e, b -> 0x0058, a -> 0x0062, Exception -> 0x00a1 }
            if (r0 == 0) goto L_0x0042
            r0.close()     // Catch:{ IOException -> 0x008e }
        L_0x0042:
            r0 = r1
        L_0x0043:
            return r0
        L_0x0044:
            r1 = move-exception
            com.asai24.golf.a r1 = com.asai24.golf.a.OOB_NO_SETTINGS     // Catch:{ all -> 0x009a }
            if (r0 == 0) goto L_0x004c
            r0.close()     // Catch:{ IOException -> 0x0090 }
        L_0x004c:
            r0 = r1
            goto L_0x0043
        L_0x004e:
            r1 = move-exception
            com.asai24.golf.a r1 = com.asai24.golf.a.OOB_INVALID_SESSION     // Catch:{ all -> 0x009a }
            if (r0 == 0) goto L_0x0056
            r0.close()     // Catch:{ IOException -> 0x0092 }
        L_0x0056:
            r0 = r1
            goto L_0x0043
        L_0x0058:
            r1 = move-exception
            com.asai24.golf.a r1 = com.asai24.golf.a.OOB_ZERO_SCORE     // Catch:{ all -> 0x009a }
            if (r0 == 0) goto L_0x0060
            r0.close()     // Catch:{ IOException -> 0x0094 }
        L_0x0060:
            r0 = r1
            goto L_0x0043
        L_0x0062:
            r1 = move-exception
            com.asai24.golf.a r1 = com.asai24.golf.a.OOB_NO_PERMISSION     // Catch:{ all -> 0x009a }
            if (r0 == 0) goto L_0x006a
            r0.close()     // Catch:{ IOException -> 0x0096 }
        L_0x006a:
            r0 = r1
            goto L_0x0043
        L_0x006c:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x0070:
            java.lang.String r2 = "OobSaveHoleByHoleScoreAPI"
            java.lang.String r3 = "unexpected exception occured at saveHoleByHoleScore: "
            android.util.Log.e(r2, r3, r0)     // Catch:{ all -> 0x009f }
            com.asai24.golf.d.c.a(r0)     // Catch:{ all -> 0x009f }
            com.asai24.golf.a r0 = com.asai24.golf.a.OOB_ERROR     // Catch:{ all -> 0x009f }
            if (r1 == 0) goto L_0x0043
            r1.close()     // Catch:{ IOException -> 0x0082 }
            goto L_0x0043
        L_0x0082:
            r1 = move-exception
            goto L_0x0043
        L_0x0084:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x0088:
            if (r1 == 0) goto L_0x008d
            r1.close()     // Catch:{ IOException -> 0x0098 }
        L_0x008d:
            throw r0
        L_0x008e:
            r0 = move-exception
            goto L_0x0042
        L_0x0090:
            r0 = move-exception
            goto L_0x004c
        L_0x0092:
            r0 = move-exception
            goto L_0x0056
        L_0x0094:
            r0 = move-exception
            goto L_0x0060
        L_0x0096:
            r0 = move-exception
            goto L_0x006a
        L_0x0098:
            r1 = move-exception
            goto L_0x008d
        L_0x009a:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0088
        L_0x009f:
            r0 = move-exception
            goto L_0x0088
        L_0x00a1:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0070
        */
        throw new UnsupportedOperationException("Method not decompiled: com.asai24.golf.c.k.a(long, boolean):com.asai24.golf.a");
    }
}
