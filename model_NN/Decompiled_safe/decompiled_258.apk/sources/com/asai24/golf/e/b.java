package com.asai24.golf.e;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class b implements Serializable {
    private long a;
    private String b;
    private List c = new ArrayList();
    private boolean d = false;

    public long a() {
        return this.a;
    }

    public void a(long j) {
        this.a = j;
    }

    public void a(c cVar) {
        this.c.add(cVar);
    }

    public void a(String str) {
        this.b = str;
    }

    public void a(List list) {
        this.c = list;
    }

    public void a(boolean z) {
        this.d = z;
    }

    public String b() {
        return this.b;
    }

    public List c() {
        return this.c;
    }

    public boolean d() {
        return this.d;
    }

    public String toString() {
        return this.b;
    }
}
