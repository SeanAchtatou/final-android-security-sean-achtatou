package com.asai24.golf.e;

import java.io.Serializable;
import java.util.List;

public class d implements Serializable, Cloneable {
    private long a;
    private long b;
    private long c;
    private String d;
    private String e;
    private String f;
    private String g;
    private String h;
    private String i;
    private List j;

    public String a() {
        return (this.e == null || this.e.length() == 0) ? this.d : String.valueOf(this.d) + " - " + this.e;
    }

    public void a(long j2) {
        this.a = j2;
    }

    public void a(String str) {
        this.d = str;
    }

    public void a(List list) {
        this.j = list;
    }

    public long b() {
        return this.a;
    }

    public void b(long j2) {
        this.b = j2;
    }

    public void b(String str) {
        this.e = str;
    }

    public long c() {
        return this.b;
    }

    public void c(long j2) {
        this.c = j2;
    }

    public void c(String str) {
        this.f = str;
    }

    public long d() {
        return this.c;
    }

    public void d(String str) {
        this.g = str;
    }

    public String e() {
        return this.d;
    }

    public void e(String str) {
        this.h = str;
    }

    public String f() {
        return this.e;
    }

    public void f(String str) {
        this.i = str;
    }

    public String g() {
        return this.f;
    }

    public String h() {
        return this.g;
    }

    public String i() {
        return this.h;
    }

    public String j() {
        return this.i;
    }

    public List k() {
        return this.j;
    }

    /* renamed from: l */
    public d clone() {
        return (d) super.clone();
    }
}
