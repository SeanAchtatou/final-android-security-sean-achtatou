package com.asai24.golf.e;

import java.io.Serializable;

public class c implements Serializable {
    private int a;
    private int b;
    private int c;
    private int d;
    private int e;
    private int f;
    private double g;
    private double h;

    public int a() {
        return this.a;
    }

    public void a(double d2) {
        this.g = d2;
    }

    public void a(int i) {
        this.a = i;
    }

    public int b() {
        return this.b;
    }

    public void b(double d2) {
        this.h = d2;
    }

    public void b(int i) {
        this.b = i;
    }

    public int c() {
        return this.d;
    }

    public void c(int i) {
        this.d = i;
    }

    public int d() {
        return this.c;
    }

    public void d(int i) {
        this.c = i;
    }

    public int e() {
        return this.f;
    }

    public void e(int i) {
        this.f = i;
    }

    public int f() {
        return this.e;
    }

    public void f(int i) {
        this.e = i;
    }

    public boolean g() {
        return this.e == -1 && this.f == -1 && this.b == -1 && this.c == -1 && this.d == -1;
    }
}
