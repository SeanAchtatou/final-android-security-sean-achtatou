package com.asai24.golf.a;

import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteCursorDriver;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQuery;
import android.database.sqlite.SQLiteQueryBuilder;
import java.util.HashMap;

public class z extends SQLiteCursor {
    private static HashMap a = new HashMap();

    static {
        a.put("club", "COUNT(" + "sd.club" + ") AS " + "club");
    }

    public z(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery) {
        super(sQLiteDatabase, sQLiteCursorDriver, str, sQLiteQuery);
    }

    public static SQLiteQueryBuilder a(long j, long j2, String str) {
        SQLiteQueryBuilder sQLiteQueryBuilder = new SQLiteQueryBuilder();
        sQLiteQueryBuilder.setProjectionMap(a);
        sQLiteQueryBuilder.setTables("scores sc JOIN score_details sd ON sc._id = sd.score_id");
        sQLiteQueryBuilder.appendWhere("sc.round_id = " + j + " AND sc." + "player_id" + "=" + j2 + " " + " AND sd." + "club" + " = '" + str + "' ");
        sQLiteQueryBuilder.setCursorFactory(new o(null));
        return sQLiteQueryBuilder;
    }

    public int a() {
        return getInt(getColumnIndexOrThrow("club"));
    }
}
