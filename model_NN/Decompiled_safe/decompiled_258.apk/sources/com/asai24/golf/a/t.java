package com.asai24.golf.a;

import android.database.Cursor;
import android.database.sqlite.SQLiteCursorDriver;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQuery;

class t implements SQLiteDatabase.CursorFactory {
    private t() {
    }

    /* synthetic */ t(t tVar) {
        this();
    }

    public Cursor newCursor(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery) {
        return new d(sQLiteDatabase, sQLiteCursorDriver, str, sQLiteQuery);
    }
}
