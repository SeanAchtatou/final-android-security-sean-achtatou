package com.asai24.golf.a;

import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteCursorDriver;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQuery;
import android.database.sqlite.SQLiteQueryBuilder;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.HashMap;

public class aa extends SQLiteCursor {
    private static HashMap a = new HashMap();

    static {
        a.put("_id", "p._id");
        a.put("name", "p.name");
        a.put("hole_score", "s.hole_score");
        a.put("game_score", "s.game_score");
    }

    public aa(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery) {
        super(sQLiteDatabase, sQLiteCursorDriver, str, sQLiteQuery);
    }

    public static SQLiteQueryBuilder a(long j, long j2, long[] jArr) {
        SQLiteQueryBuilder sQLiteQueryBuilder = new SQLiteQueryBuilder();
        sQLiteQueryBuilder.setProjectionMap(a);
        sQLiteQueryBuilder.setTables("players p LEFT JOIN scores s ON p._id = s.player_id AND s.round_id = " + j + " AND s." + "hole_id" + " = " + j2);
        if (jArr != null) {
            ArrayList arrayList = new ArrayList();
            for (long l : jArr) {
                arrayList.add(Long.toString(l));
            }
            sQLiteQueryBuilder.appendWhere("p._id IN (");
            sQLiteQueryBuilder.appendWhere(TextUtils.join(",", arrayList));
            sQLiteQueryBuilder.appendWhere(")");
        }
        sQLiteQueryBuilder.setCursorFactory(new n(null));
        return sQLiteQueryBuilder;
    }

    public String a() {
        return getString(getColumnIndexOrThrow("name"));
    }

    public int b() {
        return getInt(getColumnIndexOrThrow("hole_score"));
    }

    public int c() {
        return getInt(getColumnIndexOrThrow("game_score"));
    }
}
