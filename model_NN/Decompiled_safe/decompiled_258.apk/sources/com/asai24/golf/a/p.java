package com.asai24.golf.a;

import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteCursorDriver;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQuery;
import android.database.sqlite.SQLiteQueryBuilder;
import java.util.HashMap;

public class p extends SQLiteCursor {
    private static HashMap a = new HashMap();

    static {
        a.put("_id", "r1._id AS _id");
        a.put("course_id", "c1._id AS course_id");
        a.put("tee_id", "t1._id AS tee_id");
        a.put("club_name", "c1.club_name AS club_name");
        a.put("course_name", "c1.course_name AS course_name");
        a.put("tee_name", "t1.name AS tee_name");
        a.put("result_id", "r1.result_id AS result_id");
        a.put("yourgolf_id", "r1.yourgolf_id AS yourgolf_id");
        a.put("created_date", "r1.created AS created_date");
        a.put("modified_date", "r1.modified AS modified_date");
    }

    public p(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery) {
        super(sQLiteDatabase, sQLiteCursorDriver, str, sQLiteQuery);
    }

    public static SQLiteQueryBuilder a() {
        SQLiteQueryBuilder sQLiteQueryBuilder = new SQLiteQueryBuilder();
        sQLiteQueryBuilder.setProjectionMap(a);
        sQLiteQueryBuilder.setTables("rounds r1,courses c1,tees t1");
        sQLiteQueryBuilder.appendWhere("r1.course_id = c1._id");
        sQLiteQueryBuilder.appendWhere(" AND r1.tee_id = t1._id");
        sQLiteQueryBuilder.setCursorFactory(new h(null));
        return sQLiteQueryBuilder;
    }

    public long b() {
        return getLong(getColumnIndexOrThrow("_id"));
    }

    public long c() {
        return getLong(getColumnIndexOrThrow("course_id"));
    }

    public long d() {
        return getLong(getColumnIndexOrThrow("tee_id"));
    }

    public String e() {
        return getString(getColumnIndexOrThrow("club_name"));
    }

    public String f() {
        return getString(getColumnIndexOrThrow("course_name"));
    }

    public String g() {
        String e = e();
        String f = f();
        return (f == null || f.equals("")) ? e : String.valueOf(e) + " - " + f;
    }

    public long h() {
        return getLong(getColumnIndexOrThrow("created_date"));
    }

    public long i() {
        return getLong(getColumnIndexOrThrow("modified_date"));
    }

    public long j() {
        return getLong(getColumnIndexOrThrow("result_id"));
    }

    public String k() {
        return getString(getColumnIndexOrThrow("yourgolf_id"));
    }
}
