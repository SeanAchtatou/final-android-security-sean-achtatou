package com.asai24.golf.a;

import android.database.Cursor;
import android.database.sqlite.SQLiteCursorDriver;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQuery;

class q implements SQLiteDatabase.CursorFactory {
    private q() {
    }

    /* synthetic */ q(q qVar) {
        this();
    }

    public Cursor newCursor(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery) {
        return new m(sQLiteDatabase, sQLiteCursorDriver, str, sQLiteQuery, null);
    }
}
