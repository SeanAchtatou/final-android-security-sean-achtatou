package com.asai24.golf.a;

import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteCursorDriver;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQuery;
import android.database.sqlite.SQLiteQueryBuilder;
import java.util.HashMap;

public class d extends SQLiteCursor {
    private static HashMap a = new HashMap();

    static {
        a.put("_id", "_id");
        a.put("name", "name");
        a.put("ownner_flag", "ownner_flag");
        a.put("created", "created");
        a.put("modified", "modified");
    }

    public d(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery) {
        super(sQLiteDatabase, sQLiteCursorDriver, str, sQLiteQuery);
    }

    public static SQLiteQueryBuilder a() {
        SQLiteQueryBuilder sQLiteQueryBuilder = new SQLiteQueryBuilder();
        sQLiteQueryBuilder.setProjectionMap(a);
        sQLiteQueryBuilder.setTables("players");
        sQLiteQueryBuilder.setCursorFactory(new t(null));
        return sQLiteQueryBuilder;
    }

    public long b() {
        return getLong(getColumnIndexOrThrow("_id"));
    }

    public String c() {
        return getString(getColumnIndexOrThrow("name"));
    }

    public long d() {
        return getLong(getColumnIndexOrThrow("ownner_flag"));
    }

    public String e() {
        return getString(getColumnIndexOrThrow("created"));
    }

    public long f() {
        return getLong(getColumnIndexOrThrow("modified"));
    }
}
