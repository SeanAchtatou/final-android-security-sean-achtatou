package com.asai24.golf.a;

import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteCursorDriver;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQuery;
import android.database.sqlite.SQLiteQueryBuilder;
import java.util.HashMap;

public class g extends SQLiteCursor {
    private static HashMap a = new HashMap();

    static {
        a.put("_id", "_id");
        a.put("score_id", "score_id");
        a.put("shot_number", "shot_number");
        a.put("club", "club");
        a.put("shot_result", "shot_result");
        a.put("gps_latitude", "gps_latitude");
        a.put("gps_longitude", "gps_longitude");
        a.put("created", "created");
        a.put("modified", "modified");
    }

    public g(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery) {
        super(sQLiteDatabase, sQLiteCursorDriver, str, sQLiteQuery);
    }

    public static SQLiteQueryBuilder a() {
        SQLiteQueryBuilder sQLiteQueryBuilder = new SQLiteQueryBuilder();
        sQLiteQueryBuilder.setProjectionMap(a);
        sQLiteQueryBuilder.setTables("score_details");
        sQLiteQueryBuilder.setCursorFactory(new f(null));
        return sQLiteQueryBuilder;
    }

    public long b() {
        return getLong(getColumnIndexOrThrow("_id"));
    }

    public long c() {
        return getLong(getColumnIndexOrThrow("score_id"));
    }

    public int d() {
        return getInt(getColumnIndexOrThrow("shot_number"));
    }

    public String e() {
        String string = getString(getColumnIndexOrThrow("club"));
        return string == null ? "" : string;
    }

    public String f() {
        String string = getString(getColumnIndexOrThrow("shot_result"));
        return string == null ? "" : string;
    }

    public double g() {
        return getDouble(getColumnIndexOrThrow("gps_latitude"));
    }

    public double h() {
        return getDouble(getColumnIndexOrThrow("gps_longitude"));
    }

    public long i() {
        return getLong(getColumnIndexOrThrow("created"));
    }

    public long j() {
        return getLong(getColumnIndexOrThrow("modified"));
    }
}
