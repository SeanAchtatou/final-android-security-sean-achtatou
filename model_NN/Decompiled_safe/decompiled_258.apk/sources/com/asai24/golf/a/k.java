package com.asai24.golf.a;

public enum k {
    OobGolf("oobgolf"),
    YourGolf("yourgolf");
    
    private String c;

    private k(String str) {
        this.c = str;
    }

    public String toString() {
        return this.c;
    }
}
