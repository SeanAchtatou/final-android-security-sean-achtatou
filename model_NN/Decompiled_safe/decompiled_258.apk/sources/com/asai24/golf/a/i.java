package com.asai24.golf.a;

import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteCursorDriver;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQuery;
import android.database.sqlite.SQLiteQueryBuilder;
import com.asai24.golf.GolfApplication;
import java.util.HashMap;

public class i extends SQLiteCursor {
    private static HashMap a = new HashMap();

    static {
        String str = GolfApplication.f() ? String.valueOf("s1.hole_score - h1.") + "par" : String.valueOf("s1.hole_score - h1.") + "women_par";
        a.put("player_id", String.valueOf("s1.player_id") + " AS " + "player_id");
        a.put("total_strokes", "SUM(" + "s1.hole_score" + ") AS " + "total_strokes");
        a.put("total_score", "SUM(" + str + ") AS " + "total_score");
    }

    public i(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery) {
        super(sQLiteDatabase, sQLiteCursorDriver, str, sQLiteQuery);
    }

    public static SQLiteQueryBuilder a(long j) {
        SQLiteQueryBuilder sQLiteQueryBuilder = new SQLiteQueryBuilder();
        sQLiteQueryBuilder.setProjectionMap(a);
        sQLiteQueryBuilder.setTables("scores s1 JOIN holes h1 ON s1.hole_id = h1._id");
        sQLiteQueryBuilder.appendWhere("s1.round_id = " + j);
        sQLiteQueryBuilder.setCursorFactory(new u(null));
        return sQLiteQueryBuilder;
    }

    public static SQLiteQueryBuilder a(long j, long j2) {
        SQLiteQueryBuilder sQLiteQueryBuilder = new SQLiteQueryBuilder();
        sQLiteQueryBuilder.setProjectionMap(a);
        sQLiteQueryBuilder.setTables("scores s1 JOIN holes h1 ON s1.hole_id = h1._id");
        sQLiteQueryBuilder.appendWhere("s1.round_id = " + j + " AND s1." + "player_id" + "=" + j2 + " ");
        sQLiteQueryBuilder.setCursorFactory(new u(null));
        return sQLiteQueryBuilder;
    }

    public long a() {
        return getLong(getColumnIndexOrThrow("player_id"));
    }

    public int b() {
        return getInt(getColumnIndexOrThrow("total_strokes"));
    }

    public int c() {
        return getInt(getColumnIndexOrThrow("total_score"));
    }
}
