package com.asai24.golf.a;

import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteCursorDriver;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQuery;
import android.database.sqlite.SQLiteQueryBuilder;
import java.util.HashMap;

public class w extends SQLiteCursor {
    private static HashMap a = new HashMap();

    static {
        a.put("_id", "_id");
        a.put("hole_score", "hole_score");
        a.put("hole_id", "hole_id");
        a.put("player_id", "player_id");
        a.put("round_id", "round_id");
        a.put("game_score", "game_score");
        a.put("created", "created");
        a.put("modified", "modified");
    }

    public w(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery) {
        super(sQLiteDatabase, sQLiteCursorDriver, str, sQLiteQuery);
    }

    public static SQLiteQueryBuilder a() {
        SQLiteQueryBuilder sQLiteQueryBuilder = new SQLiteQueryBuilder();
        sQLiteQueryBuilder.setProjectionMap(a);
        sQLiteQueryBuilder.setTables("scores");
        sQLiteQueryBuilder.setCursorFactory(new x(null));
        return sQLiteQueryBuilder;
    }

    public long b() {
        return getLong(getColumnIndexOrThrow("_id"));
    }

    public int c() {
        return getInt(getColumnIndexOrThrow("hole_score"));
    }

    public long d() {
        return getLong(getColumnIndexOrThrow("hole_id"));
    }

    public long e() {
        return getLong(getColumnIndexOrThrow("round_id"));
    }

    public long f() {
        return getLong(getColumnIndexOrThrow("player_id"));
    }

    public int g() {
        return getInt(getColumnIndexOrThrow("game_score"));
    }

    public long h() {
        return getLong(getColumnIndexOrThrow("created"));
    }

    public long i() {
        return getLong(getColumnIndexOrThrow("modified"));
    }
}
