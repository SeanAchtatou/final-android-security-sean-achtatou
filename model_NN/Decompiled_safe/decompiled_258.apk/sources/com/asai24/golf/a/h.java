package com.asai24.golf.a;

import android.database.Cursor;
import android.database.sqlite.SQLiteCursorDriver;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQuery;

class h implements SQLiteDatabase.CursorFactory {
    private h() {
    }

    /* synthetic */ h(h hVar) {
        this();
    }

    public Cursor newCursor(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery) {
        return new p(sQLiteDatabase, sQLiteCursorDriver, str, sQLiteQuery);
    }
}
