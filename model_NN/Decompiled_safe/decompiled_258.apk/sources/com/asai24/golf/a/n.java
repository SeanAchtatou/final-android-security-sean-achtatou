package com.asai24.golf.a;

import android.database.Cursor;
import android.database.sqlite.SQLiteCursorDriver;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQuery;

class n implements SQLiteDatabase.CursorFactory {
    private n() {
    }

    /* synthetic */ n(n nVar) {
        this();
    }

    public Cursor newCursor(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery) {
        return new aa(sQLiteDatabase, sQLiteCursorDriver, str, sQLiteQuery);
    }
}
