package com.asai24.golf.a;

import android.database.Cursor;
import android.database.sqlite.SQLiteCursorDriver;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQuery;

class c implements SQLiteDatabase.CursorFactory {
    private c() {
    }

    /* synthetic */ c(c cVar) {
        this();
    }

    public Cursor newCursor(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery) {
        return new e(sQLiteDatabase, sQLiteCursorDriver, str, sQLiteQuery, null);
    }
}
