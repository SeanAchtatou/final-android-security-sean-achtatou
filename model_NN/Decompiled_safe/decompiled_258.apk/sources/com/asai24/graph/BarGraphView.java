package com.asai24.graph;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.EmbossMaskFilter;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.view.View;
import com.asai24.golf.R;
import java.util.ArrayList;
import java.util.List;

public class BarGraphView extends View {
    private Context a;
    private int b;
    private int c;
    private int d;
    private int e;
    private int f;
    private int g;
    private ArrayList h;
    private float i;
    private int j;
    private int k;
    private int l;
    private ArrayList m;
    private Paint n;
    private Paint o;
    private Paint p;
    private Paint q;
    private boolean r;
    private ArrayList s;
    private ArrayList t;

    public BarGraphView(Context context) {
        super(context);
        this.a = context;
        a();
    }

    public BarGraphView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.a = context;
        a();
    }

    private void a() {
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        this.n = new Paint(paint);
        this.n.setMaskFilter(new EmbossMaskFilter(new float[]{5.0f, 1.0f, 5.0f}, 0.5f, 10.0f, 3.0f));
        this.o = new Paint(paint);
        this.o.setColor(-16777216);
        this.o.setStrokeWidth(5.0f);
        this.p = new Paint(paint);
        this.p.setColor(-16777216);
        this.p.setStrokeWidth(1.0f);
        this.q = new Paint(paint);
        this.q.setColor(-16777216);
        this.q.setTextAlign(Paint.Align.LEFT);
        this.q.setTextSize(this.a.getResources().getDimension(R.dimen.bar_graph_text_size));
        this.q.setAntiAlias(true);
    }

    public void a(int i2, int i3, int i4) {
        this.c = i2;
        this.d = i3;
        this.b = i4;
        this.e = i2 / (i4 + 2);
        this.f = this.e / (i4 + 1);
        this.g = this.f;
    }

    public void a(List list, int i2) {
        this.r = false;
        this.l = i2;
        this.m = new ArrayList();
        int i3 = 0;
        int i4 = 0;
        while (i3 < this.b) {
            this.m.add(Integer.valueOf(((a) list.get(i3)).b()));
            i3++;
            i4 = Math.max(i4, ((a) list.get(i3)).a());
        }
        int i5 = i4 < 10 ? 1 : i4 < 20 ? 2 : i4 < 50 ? 5 : 10;
        int i6 = i5;
        while (i4 > i6) {
            i6 += i5;
        }
        this.h = new ArrayList();
        for (int i7 = 0; i7 < this.b; i7++) {
            this.h.add(Float.valueOf(((float) ((a) list.get(i7)).a()) / ((float) i6)));
        }
        int i8 = this.d - (this.g * 2);
        this.j = (int) Math.ceil(((double) i6) / ((double) i5));
        this.k = i5;
        while (this.j > 10) {
            this.j = (int) Math.ceil(((double) this.j) / 2.0d);
            this.k *= 2;
        }
        this.i = ((float) i8) / ((float) i6);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Resources resources = getResources();
        int measureText = (int) this.q.measureText("1.00");
        int i2 = this.d - (this.g * 2);
        int i3 = 0;
        if (this.r) {
            int i4 = 0;
            int i5 = 0;
            while (i4 < this.s.size()) {
                List list = (List) this.s.get(i4);
                List list2 = (List) this.t.get(i4);
                int i6 = this.f + i5;
                int i7 = i4 == 0 ? i6 + measureText : i6;
                int i8 = i7 + this.e;
                int i9 = 0;
                int i10 = this.d - this.g;
                while (i9 < list.size()) {
                    int floatValue = i10 - ((int) (((Float) list.get(i9)).floatValue() * ((float) i2)));
                    this.n.setColor(((Integer) list2.get(i9)).intValue());
                    canvas.drawRect((float) i7, (float) floatValue, (float) i8, (float) i10, this.n);
                    i9++;
                    i10 = floatValue;
                }
                i4++;
                i5 = i8;
            }
        } else {
            int i11 = this.d - this.g;
            int i12 = 0;
            while (i12 < this.b) {
                int i13 = i3 + this.f;
                if (i12 == 0) {
                    i13 += measureText;
                }
                int i14 = i13;
                int i15 = i14 + this.e;
                int floatValue2 = i11 - ((int) (((Float) this.h.get(i12)).floatValue() * ((float) i2)));
                this.n.setShader(new LinearGradient((float) (i15 - (this.e / 2)), (float) i11, (float) (i15 - (this.e / 2)), (float) floatValue2, resources.getColor(R.color.black_back_transparent), ((Integer) this.m.get(i12)).intValue(), Shader.TileMode.CLAMP));
                canvas.drawRect((float) i14, (float) floatValue2, (float) i15, (float) i11, this.n);
                i12++;
                i3 = i15;
            }
        }
        int i16 = this.d - this.g;
        canvas.drawLine((float) measureText, (float) i16, (float) this.c, (float) i16, this.o);
        int dimensionPixelSize = this.a.getResources().getDimensionPixelSize(R.dimen.bar_graph_text_size) / 2;
        int i17 = 1;
        while (true) {
            int i18 = i17;
            if (i18 < this.j) {
                int i19 = (int) (((float) i16) - (this.i * ((float) (this.k * i18))));
                canvas.drawLine((float) measureText, (float) i19, (float) this.c, (float) i19, this.p);
                switch (this.l) {
                    case 1:
                        canvas.drawText(new StringBuilder(String.valueOf(this.k * i18)).toString(), 0.0f, (float) (i19 + dimensionPixelSize), this.q);
                        break;
                    case 2:
                        canvas.drawText(new StringBuilder(String.valueOf((((float) i18) * ((float) this.k)) / 10.0f)).toString(), 0.0f, (float) (i19 + dimensionPixelSize), this.q);
                        break;
                    case 3:
                        canvas.drawText(String.valueOf(this.k * i18) + "%", 0.0f, (float) (i19 + dimensionPixelSize), this.q);
                        break;
                }
                i17 = i18 + 1;
            } else {
                return;
            }
        }
    }
}
