@除手掌足底外，全身皮肤均有毛发生长，其中硬毛长、粗、硬而色浓，软（毳）毛短、细、软而色淡，毛发分布、类型因性别、年龄而异。毛发异常是指毛发的分布、数量、颜色和结构上的改变，临床上毛发脱落（秃发）较为常见。#
@突然发病，为圆形或椭圆形、境界清楚、皮肤光滑的脱发区，表面无炎症现象，在脱发区的边缘处头发易拔脱，脱发区数目不定，大小不一，病程慢性，一般数月后头发可自然再生，再生的新发开始呈绒毛状，逐渐变粗变黑而恢复正常#斑秃，俗称“鬼剃头”，头发全部脱落称全秃，腋毛、阴毛、胡须、眉毛全部脱落者称普秃
@儿童时期发病，病程缓慢，不能自愈，可形成永久性脱发，以黄癣痂、脱发、瘢痕为特点。黄癣痂开始于毛囊口，为炎症性丘疹或小脓疱，脓疱干涸后变成黄色痂皮，为圆形或不规则形，逐渐变厚呈碟状，直径可达1cm，有特殊臭味，痂中央有头发贯穿，头发干燥，失去光泽，变脆易折断，形成长短不齐的脱发区，头皮可发生萎缩性瘢痕#黄癣
@头一个月发展快（主要见于城市学龄儿童），以后静止，青春期可自愈，以白色鳞屑斑和病发周围有白色菌鞘为特征，开始为群集的毛囊性丘疹，很快扩展成片，境界清楚，表面有白色鳞屑，继续扩大，四周可出现小的卫星状损害，以后又可互相融合成更大斑片；病发失去光泽，发周有灰白色套状鳞屑包绕，病发常在长出头皮5mm左右就容易折断，残发易拔出#白癣
@病程慢性（多见于城市儿童），无静止期，可延长到成人期而不愈；头发刚露出头皮即折断呈黑点状，典型损害为散在性斑片，炎症较明显，边缘可有小水疱及少量痂皮#黑点癣，简称黑癣
@继发于头皮感染、物理性损伤、炎症或肿瘤性皮肤病，破坏毛囊而引起#疤痕性秃发，可由毛囊炎、疖、麻风、梅毒、带状疱疹、放射线照射、烫伤、烧伤、慢性盘状红斑狼疮、局限性硬皮病、扁平苔癣、皮脂腺痣、基底细胞癌等病症引起
