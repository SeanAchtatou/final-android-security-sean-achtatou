@休克是一种急性组织细胞灌流量和功能不足而引起的临床综合症。主要临床表现有血压下降，收缩压降至10.6kPa以下，呼吸短促、皮肤冰凉湿冷、脉搏细速、尿量减少、头昏、虚弱和晕厥。　　它是一个严重的、变化多端的动态过程，抢救措施越早越好。重点是支持治疗和呼吸循环维持；缺氧或重度呼吸困难时，给予吸氧；建立足够的静脉通路；排除心源性休克时，快速补液纠正低血压。#
@使用药物（青霉素、氯化喹啉、疫苗、血清、普鲁卡因、碘剂等），或被蛇、蝎等叮咬后立即发生皮肤潮红、荨麻疹和喉头紧缩等症状#过敏性休克，危急！立即送医院抢救
@40岁以上男性，且胸骨后痛，钝痛，重压感，向胸部两侧或左上肢放射痛#急性心肌梗塞，危急！立即送医院抢救
@撕裂样痛向胸腹部放射，有突然用力史#夹层主动脉瘤，危急！立即送医院抢救
@心率超过180次/分钟，或有心脏病史者，心率低于40次/分钟，伴脉压差变小，四肢湿冷#心律失常导致心源性休克，危急！立即送医院抢救
@心前区刺痛，可放射至胸部两侧；咳嗽；改变体重时加重；颈静脉怒张；心音减弱#急性心包填塞，危急！立即送医院抢救
@长期卧床、手术或分娩后，突发呼吸困难、胸骨后痛、咳嗽、咯血或发绀等症状#肺动脉栓塞，立即送医院抢救
@已知有糖尿病，伴饿感、头晕、心悸、手抖、出汗、软弱、乏力、脸色苍白，口服或静脉葡萄糖可立即缓解#低血糖（糖尿病人发生休克最常见的原因），立即送医院诊治
@长期使用肾上腺皮质激素#急性肾上腺皮质功能不全，应立即去医院诊治
