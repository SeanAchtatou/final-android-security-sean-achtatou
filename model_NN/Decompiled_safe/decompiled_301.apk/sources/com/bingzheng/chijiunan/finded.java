package com.bingzheng.chijiunan;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.HashMap;

public class finded extends Activity {
    private TextView mTextview;
    boolean yanzheng = true;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.sousujieguo);
        setTitle("搜索结果");
        this.mTextview = (TextView) findViewById(R.id.Textview1);
        String[] lists = getIntent().getStringArrayExtra("sousu");
        final int[] lists1 = getIntent().getIntArrayExtra("sousu1");
        String jieguoname = getIntent().getStringExtra("jieguo");
        ListView list = (ListView) findViewById(R.id.listview4);
        if (lists.length > 0) {
            this.mTextview.setText("请在搜索结果中选择病症(第二步):\n搜索“" + jieguoname + "”(" + lists.length + "个结果)的结果:");
            ArrayList<HashMap<String, Object>> listItem = new ArrayList<>();
            for (int i = 0; i < lists.length; i++) {
                HashMap<String, Object> map = new HashMap<>();
                map.put("title", String.valueOf(i + 1) + "." + lists[i]);
                listItem.add(map);
            }
            list.setAdapter((ListAdapter) new SimpleAdapter(this, listItem, R.layout.findlist, new String[]{"title"}, new int[]{R.id.title4}));
        } else {
            this.yanzheng = false;
            ArrayList<HashMap<String, Object>> listItem2 = new ArrayList<>();
            HashMap<String, Object> map2 = new HashMap<>();
            HashMap<String, Object> map1 = new HashMap<>();
            map2.put("title", "无搜索结果...");
            listItem2.add(map2);
            map1.put("title", "搜索请尽量简短...\n如：牙痛，搜索“牙”或“痛”即可。");
            listItem2.add(map1);
            list.setAdapter((ListAdapter) new SimpleAdapter(this, listItem2, R.layout.findlist, new String[]{"title"}, new int[]{R.id.title4}));
        }
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                finded.this.yanzheng = true;
                if (1 != 0) {
                    Intent intent = new Intent();
                    intent.putExtra("arg2", String.valueOf(arg2));
                    intent.putExtra("chuandi1", lists1);
                    intent.setClass(finded.this, kanbing.class);
                    finded.this.startActivity(intent);
                }
            }
        });
    }
}
