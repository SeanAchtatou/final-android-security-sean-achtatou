package com.bingzheng.chijiunan;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.HashMap;

public class adlist extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.ad);
        setTitle("chijiunan原创软件");
        ListView list = (ListView) findViewById(R.id.adlistview);
        final String[] dizhi = {"http://3g.gfan.com/index.php?/detail/index/165948", "http://binpda.cn/bbs/upload/2011/08/15/124037846.apk", "http://3g.gfan.com/index.php?/detail/index/163879", "http://3g.gfan.com/index.php?/detail/index/163023", "http://3g.gfan.com/index.php?/detail/index/162466", "http://3g.gfan.com/index.php?/detail/index/161791", "http://3g.gfan.com/index.php?/detail/index/161355", "http://binpda.cn/bbs/upload/2011/08/09/17920950.apk", "http://3g.gfan.com/index.php?/detail/index/160088", "http://www.eoemarket.com/apps/29400", "http://3g.gfan.com/index.php?/detail/index/156130"};
        String[] title = {"更懂外语的翻译软件", "安卓版网页刷新器", "12生肖搞笑证件制作", "3G挂QQ软件", "QQ日志查看软件", "免费帮你写情书", "免费艺术签名设计", "安卓版短信炸弹升级版", "超能力测试", "旧版短信炸弹", "性能力测试软件"};
        String[] content = {"翻译准确，软件简单易用！", "简单使用，上手快，刷刷更快乐！！", "制作属于自己的生肖搞笑证件！", "省流量，省心，安全，免费挂QQ！", "可以查看非QQ好友的日志", "如果您恋爱了，或许这软件对您有用", "设计属于自己的个性艺术签名", "【推荐】炸人不用花费，不显示自己的号码", "想看看自己有神马超能力？赶快下载试试", "炸人不用花费，不显示自己的号码", "可以测试您的性能力有多强"};
        ArrayList<HashMap<String, Object>> listItem = new ArrayList<>();
        for (int i = 0; i < title.length; i++) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("download", "【免费下载】");
            map.put("title", title[i].toString());
            map.put("content", content[i].toString());
            listItem.add(map);
        }
        list.setAdapter((ListAdapter) new SimpleAdapter(this, listItem, R.layout.adlist, new String[]{"download", "title", "content"}, new int[]{R.id.download, R.id.myadtexttitle, R.id.myadtextcontent}));
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                String dz = dizhi[arg2].toString();
                try {
                    Toast.makeText(adlist.this, "正在启动浏览器...", 1).show();
                    adlist.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(dz)));
                } catch (Exception e) {
                    Toast.makeText(adlist.this, "启动浏览器失败！", 1).show();
                }
            }
        });
    }
}
