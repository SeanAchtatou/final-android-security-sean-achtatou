package com.bingzheng.chijiunan;

import android.content.Context;
import android.os.Environment;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

public class function {
    private String SDPATH = (Environment.getExternalStorageDirectory() + "/");
    int downloadfilesize;

    public String yzzf(String content, String s, int i) {
        Matcher ma = Pattern.compile(s).matcher(content);
        if (ma.find()) {
            return ma.group(i);
        }
        return "";
    }

    /* Debug info: failed to restart local var, previous not found, register: 7 */
    public String[] yzzffindall(String content, String s) {
        ArrayList<String> list = new ArrayList<>();
        Matcher matcher = Pattern.compile(s).matcher(content);
        while (matcher.find()) {
            if (matcher.group(1) != null) {
                list.add(matcher.group(1));
            }
        }
        String[] lists = new String[list.size()];
        for (int i = 0; i < list.size(); i++) {
            lists[i] = (String) list.get(i);
        }
        return lists;
    }

    public String geturl(String urls) {
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(((HttpURLConnection) new URL(urls).openConnection()).getInputStream()));
            String s = "";
            while (true) {
                String line = br.readLine();
                if (line == null) {
                    return s;
                }
                s = String.valueOf(s) + line + "\n";
            }
        } catch (IOException e) {
            return "error";
        }
    }

    public String getpost(String url, String[] content, String[] con) {
        HttpPost hp = new HttpPost(url);
        List<NameValuePair> params = new ArrayList<>();
        for (int i = 0; i < content.length; i++) {
            params.add(new BasicNameValuePair(content[i], con[i]));
        }
        try {
            hp.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
            HttpResponse hr = new DefaultHttpClient().execute(hp);
            if (hr.getStatusLine().getStatusCode() == 200) {
                return EntityUtils.toString(hr.getEntity());
            }
            return "error";
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return "error";
        } catch (ClientProtocolException e2) {
            e2.printStackTrace();
            return "error";
        } catch (IOException e3) {
            e3.printStackTrace();
            return "error";
        }
    }

    public String getSDPATH() {
        return this.SDPATH;
    }

    public File creatSDFile(String fileName) throws IOException {
        File file = new File(String.valueOf(this.SDPATH) + fileName);
        file.createNewFile();
        return file;
    }

    public File creatSDDir(String dirName) {
        File dir = new File(String.valueOf(this.SDPATH) + dirName);
        dir.mkdirs();
        return dir;
    }

    public void download(String url, String path) throws IOException {
        URLConnection conn = new URL(url).openConnection();
        conn.connect();
        InputStream is = conn.getInputStream();
        if (conn.getContentLength() <= 0) {
            System.out.println("11");
        }
        FileOutputStream fos = new FileOutputStream(String.valueOf(path) + "temp.jpg");
        byte[] buffer = new byte[1024];
        this.downloadfilesize = 0;
        while (true) {
            int numread = is.read(buffer);
            if (numread == -1) {
                try {
                    is.close();
                    return;
                } catch (Exception e) {
                    e.printStackTrace();
                    return;
                }
            } else {
                fos.write(buffer, 0, numread);
                this.downloadfilesize += numread;
            }
        }
    }

    public boolean isFileExist(String fileName) {
        return new File(String.valueOf(this.SDPATH) + fileName).exists();
    }

    public class FileIO {
        private static final int MODE_WORLD_READABLE = 0;
        private Context context;

        public FileIO(Context context2) {
            this.context = context2;
        }

        public void savefilename(String filename, String content) throws Exception {
            FileOutputStream outStream = this.context.openFileOutput(filename, 0);
            outStream.write(content.getBytes());
            outStream.close();
        }

        public String readfilename(String filename) throws Exception {
            FileInputStream inStream = this.context.openFileInput(filename);
            byte[] buffer = new byte[1024];
            ByteArrayOutputStream outStream = new ByteArrayOutputStream();
            while (true) {
                int len = inStream.read(buffer);
                if (len == -1) {
                    byte[] data = outStream.toByteArray();
                    inStream.close();
                    outStream.close();
                    return new String(data);
                }
                outStream.write(buffer, 0, len);
            }
        }

        public void savafile(String path, String content) throws Exception {
            File file = new File(path.substring(0, path.lastIndexOf("/")), path.substring(path.lastIndexOf("/") + 1));
            file.createNewFile();
            FileOutputStream outStream = new FileOutputStream(file);
            outStream.write(content.getBytes());
            outStream.close();
        }

        public String readfile(String path) throws Exception {
            FileInputStream inStream = new FileInputStream(new File(path));
            byte[] buffer = new byte[1024];
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            while (true) {
                int len = inStream.read(buffer);
                if (len == -1) {
                    byte[] data = outputStream.toByteArray();
                    inStream.close();
                    outputStream.close();
                    return new String(data);
                }
                outputStream.write(buffer, 0, len);
            }
        }
    }
}
