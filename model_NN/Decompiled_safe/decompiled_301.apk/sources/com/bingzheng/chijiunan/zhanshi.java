package com.bingzheng.chijiunan;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

public class zhanshi extends Activity {
    String chuandi;
    int[] listshouye = {R.raw.shouye1, R.raw.shouye2, R.raw.shouye3, R.raw.shouye4, R.raw.shouye5, R.raw.shouye6, R.raw.shouye7};

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main1);
        String arg2 = getIntent().getStringExtra("arg2");
        this.chuandi = arg2;
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(getResources().openRawResource(this.listshouye[Integer.valueOf(arg2).intValue()])));
        String s1 = "";
        while (true) {
            try {
                String s = bufferedReader.readLine();
                if (s == null) {
                    break;
                }
                s1 = String.valueOf(s1) + s + "\n";
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        String[] rawid = new function().yzzffindall(s1, "@(.*?)#");
        ListView list = (ListView) findViewById(R.id.listview2);
        ArrayList<HashMap<String, Object>> listItem = new ArrayList<>();
        for (int i = 0; i < rawid.length; i++) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("title", String.valueOf(i + 1) + "." + rawid[i]);
            listItem.add(map);
        }
        list.setAdapter((ListAdapter) new SimpleAdapter(this, listItem, R.layout.list2, new String[]{"title"}, new int[]{R.id.title2}));
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                Intent intent = new Intent();
                intent.putExtra("arg2", String.valueOf(arg2));
                intent.putExtra("chuandi", zhanshi.this.chuandi);
                intent.setClass(zhanshi.this, kanbing.class);
                zhanshi.this.startActivity(intent);
            }
        });
    }

    public void MessageBox(Activity ac, String text) {
        AlertDialog.Builder ab = new AlertDialog.Builder(ac);
        ab.setTitle("温馨提示:");
        ab.setMessage(text);
        ab.setPositiveButton("确定", (DialogInterface.OnClickListener) null);
        ab.show();
    }
}
