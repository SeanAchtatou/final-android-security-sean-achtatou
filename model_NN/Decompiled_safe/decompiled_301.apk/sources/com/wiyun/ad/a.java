package com.wiyun.ad;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.CornerPathEffect;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Environment;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.Transformation;
import android.view.animation.TranslateAnimation;
import android.webkit.DownloadListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.wiyun.ad.AdView;
import java.io.File;
import java.util.LinkedList;
import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpProtocolParams;

class a extends View implements DownloadListener {
    private static int a = 40;
    private static final Typeface b = Typeface.create(Typeface.SANS_SERIF, 0);
    private int c = -16777216;
    private int d = -1;
    private boolean e;
    private long f;
    private int g;
    private Animation h;
    private Transformation i = new Transformation();
    private Bitmap j;
    private Paint k;
    /* access modifiers changed from: private */
    public final i l;
    private String[] m;
    private int n;
    private long o;
    private boolean p;
    private boolean q;
    private q r;
    private long s;
    private AdView.AdListener t;

    /* renamed from: com.wiyun.ad.a$a  reason: collision with other inner class name */
    private final class C0000a extends WebViewClient {
        private View b;

        public C0000a(View view) {
            this.b = view;
        }

        public void onPageFinished(WebView webView, String str) {
            super.onPageFinished(webView, str);
            this.b.setVisibility(4);
        }

        public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
            super.onPageStarted(webView, str, bitmap);
            this.b.setVisibility(0);
        }
    }

    private final class b extends Thread {
        private String b;
        /* access modifiers changed from: private */
        public String c = f.a(this.b);
        /* access modifiers changed from: private */
        public int d;
        private int e;

        b(String str) {
            this.b = str;
            setDaemon(true);
        }

        private DefaultHttpClient a() {
            HttpHost d2;
            BasicHttpParams basicHttpParams = new BasicHttpParams();
            HttpProtocolParams.setVersion(basicHttpParams, HttpVersion.HTTP_1_0);
            HttpProtocolParams.setContentCharset(basicHttpParams, "UTF-8");
            HttpProtocolParams.setUseExpectContinue(basicHttpParams, false);
            HttpConnectionParams.setConnectionTimeout(basicHttpParams, 10000);
            HttpConnectionParams.setSoTimeout(basicHttpParams, 10000);
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient(basicHttpParams);
            if (!l.b() && l.c() && (d2 = l.d()) != null) {
                defaultHttpClient.getParams().setParameter("http.route.default-proxy", d2);
            }
            return defaultHttpClient;
        }

        private void a(final int i) {
            ((Activity) a.this.getContext()).runOnUiThread(new Runnable() {
                public void run() {
                    ((AdView) a.this.getParent()).a(i);
                }
            });
        }

        /* JADX WARNING: Removed duplicated region for block: B:35:0x0068 A[SYNTHETIC, Splitter:B:35:0x0068] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private boolean a(org.apache.http.HttpResponse r7, java.io.File r8) throws java.io.IOException {
            /*
                r6 = this;
                r5 = 0
                r0 = 4096(0x1000, float:5.74E-42)
                byte[] r0 = new byte[r0]
                org.apache.http.HttpEntity r1 = r7.getEntity()
                java.io.InputStream r1 = r1.getContent()
                if (r1 != 0) goto L_0x0011
                r0 = r5
            L_0x0010:
                return r0
            L_0x0011:
                boolean r2 = r8.exists()
                if (r2 == 0) goto L_0x001f
                boolean r2 = r8.delete()
                if (r2 != 0) goto L_0x001f
                r0 = r5
                goto L_0x0010
            L_0x001f:
                java.io.File r2 = r8.getParentFile()
                boolean r3 = r2.exists()
                if (r3 != 0) goto L_0x0031
                boolean r2 = r2.mkdirs()
                if (r2 != 0) goto L_0x0031
                r0 = r5
                goto L_0x0010
            L_0x0031:
                r2 = 0
                java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0058, all -> 0x0064 }
                r3.<init>(r8)     // Catch:{ Exception -> 0x0058, all -> 0x0064 }
                r2 = r5
            L_0x0038:
                r4 = -1
                if (r2 != r4) goto L_0x0045
                if (r3 == 0) goto L_0x0043
                r3.flush()     // Catch:{ IOException -> 0x006f }
                r3.close()     // Catch:{ IOException -> 0x006f }
            L_0x0043:
                r0 = 1
                goto L_0x0010
            L_0x0045:
                r4 = 0
                r3.write(r0, r4, r2)     // Catch:{ Exception -> 0x0078, all -> 0x0073 }
                int r4 = r6.e     // Catch:{ Exception -> 0x0078, all -> 0x0073 }
                int r2 = r2 + r4
                r6.e = r2     // Catch:{ Exception -> 0x0078, all -> 0x0073 }
                int r2 = r6.e     // Catch:{ Exception -> 0x0078, all -> 0x0073 }
                r6.a(r2)     // Catch:{ Exception -> 0x0078, all -> 0x0073 }
                int r2 = r1.read(r0)     // Catch:{ Exception -> 0x0078, all -> 0x0073 }
                goto L_0x0038
            L_0x0058:
                r0 = move-exception
                r0 = r2
            L_0x005a:
                if (r0 == 0) goto L_0x0062
                r0.flush()     // Catch:{ IOException -> 0x0076 }
                r0.close()     // Catch:{ IOException -> 0x0076 }
            L_0x0062:
                r0 = r5
                goto L_0x0010
            L_0x0064:
                r0 = move-exception
                r1 = r2
            L_0x0066:
                if (r1 == 0) goto L_0x006e
                r1.flush()     // Catch:{ IOException -> 0x0071 }
                r1.close()     // Catch:{ IOException -> 0x0071 }
            L_0x006e:
                throw r0
            L_0x006f:
                r0 = move-exception
                goto L_0x0043
            L_0x0071:
                r1 = move-exception
                goto L_0x006e
            L_0x0073:
                r0 = move-exception
                r1 = r3
                goto L_0x0066
            L_0x0076:
                r0 = move-exception
                goto L_0x0062
            L_0x0078:
                r0 = move-exception
                r0 = r3
                goto L_0x005a
            */
            throw new UnsupportedOperationException("Method not decompiled: com.wiyun.ad.a.b.a(org.apache.http.HttpResponse, java.io.File):boolean");
        }

        public void run() {
            Header firstHeader;
            DefaultHttpClient a2 = a();
            try {
                HttpResponse execute = a2.execute(new HttpGet(f.b(this.b)));
                if (execute.getStatusLine().getStatusCode() < 300 && (firstHeader = execute.getFirstHeader("Content-Length")) != null) {
                    this.d = f.c(firstHeader.getValue());
                    Activity activity = (Activity) a.this.getContext();
                    File a3 = a.this.a(this.c);
                    if (a3 == null) {
                        activity.runOnUiThread(new Runnable() {
                            public void run() {
                                Toast.makeText(a.this.getContext(), s.a("no_sd_card"), 0).show();
                            }
                        });
                        if (a2 != null) {
                            a2.getConnectionManager().shutdown();
                        }
                        ((Activity) a.this.getContext()).runOnUiThread(new Runnable() {
                            public void run() {
                                ((AdView) a.this.getParent()).a();
                            }
                        });
                        return;
                    }
                    activity.runOnUiThread(new Runnable() {
                        public void run() {
                            ((AdView) a.this.getParent()).a(b.this.d, b.this.c);
                        }
                    });
                    if (a(execute, a3)) {
                        activity.runOnUiThread(new Runnable() {
                            public void run() {
                                ((AdView) a.this.getParent()).a();
                            }
                        });
                        Intent intent = new Intent("android.intent.action.VIEW");
                        intent.setDataAndType(Uri.fromFile(a3), "application/vnd.android.package-archive");
                        a.this.getContext().startActivity(intent);
                        new Thread() {
                            public void run() {
                                b.a(a.this.getContext(), a.this.l, "download_complete");
                            }
                        }.start();
                    }
                }
                if (a2 != null) {
                    a2.getConnectionManager().shutdown();
                }
                ((Activity) a.this.getContext()).runOnUiThread(new Runnable() {
                    public void run() {
                        ((AdView) a.this.getParent()).a();
                    }
                });
            } catch (Exception e2) {
                Log.w("WiYun", "failed to download apk: " + this.c);
                if (a2 != null) {
                    a2.getConnectionManager().shutdown();
                }
                ((Activity) a.this.getContext()).runOnUiThread(new Runnable() {
                    public void run() {
                        ((AdView) a.this.getParent()).a();
                    }
                });
            } catch (Throwable th) {
                Throwable th2 = th;
                if (a2 != null) {
                    a2.getConnectionManager().shutdown();
                }
                ((Activity) a.this.getContext()).runOnUiThread(new Runnable() {
                    public void run() {
                        ((AdView) a.this.getParent()).a();
                    }
                });
                throw th2;
            }
        }
    }

    private static final class c implements Interpolator {
        private float a;

        public c(int i) {
            this.a = 1.0f / ((float) i);
        }

        public float getInterpolation(float f) {
            return ((float) ((int) (f / this.a))) * this.a;
        }
    }

    public a(Context context, i iVar, AdView.AdListener adListener) {
        super(context);
        this.l = iVar;
        this.t = adListener;
        if (iVar != null) {
            setFocusable(true);
            setClickable(true);
        }
        this.k = new Paint();
        this.k.setAntiAlias(true);
        this.k.setTypeface(b);
        this.k.setTextSize(b());
    }

    /* access modifiers changed from: private */
    public File a(String str) {
        if (!f.a() || getContext().checkCallingOrSelfPermission("android.permission.WRITE_EXTERNAL_STORAGE") != 0) {
            return null;
        }
        return new File(Environment.getExternalStorageDirectory(), ".wiad_cache/" + str);
    }

    private static String a(String str, boolean z) {
        if (!z) {
            return str.trim();
        }
        int length = str.length() - 1;
        while (length >= 0 && str.charAt(length) <= ' ') {
            length--;
        }
        return length <= 0 ? str : str.substring(0, length + 1);
    }

    private void a(Canvas canvas) {
        Bitmap bitmap = this.l.s;
        if (bitmap != null) {
            float width = (float) bitmap.getWidth();
            float height = (float) bitmap.getHeight();
            float width2 = (float) getWidth();
            float height2 = (float) getHeight();
            float max = Math.max(height / height2, width / width2);
            float f2 = width / max;
            float f3 = height / max;
            int i2 = (int) ((width2 - f2) / 2.0f);
            int i3 = (int) ((height2 - f3) / 2.0f);
            a(canvas, new Rect(i2, i3, ((int) f2) + i2, ((int) f3) + i3), bitmap);
        }
    }

    private static void a(Canvas canvas, Rect rect) {
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(-1147097);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(2.0f);
        paint.setPathEffect(new CornerPathEffect(3.0f));
        Path path = new Path();
        path.addRoundRect(new RectF(rect), 3.0f, 3.0f, Path.Direction.CW);
        canvas.drawPath(path, paint);
    }

    private static void a(Canvas canvas, Rect rect, int i2) {
        Paint paint = new Paint();
        paint.setColor(i2);
        paint.setAntiAlias(true);
        canvas.drawRect(rect, paint);
        paint.setShader(new LinearGradient((float) rect.left, (float) rect.top, (float) rect.left, (float) rect.height(), 1728053247, 0, Shader.TileMode.MIRROR));
        canvas.drawRect(rect, paint);
        paint.setShader(null);
    }

    private static void a(Canvas canvas, Rect rect, Bitmap bitmap) {
        Paint paint = new Paint();
        paint.setFilterBitmap(true);
        canvas.drawBitmap(bitmap, new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight()), rect, paint);
    }

    private static String[] a(Paint paint, String str, float f2) {
        String[] strArr = null;
        if (str == null) {
            return strArr;
        }
        LinkedList linkedList = new LinkedList();
        int i2 = 0;
        float measureText = f2 - paint.measureText("0");
        char[] charArray = str.toCharArray();
        boolean z = true;
        int i3 = 0;
        int i4 = 0;
        for (int i5 = 0; i5 < charArray.length; i5++) {
            boolean z2 = charArray[i5] == '-' || charArray[i5] == '/';
            boolean z3 = charArray[i5] == ' ';
            boolean z4 = charArray[i5] == 10;
            boolean z5 = z4 || (charArray[i5] == 13);
            float measureText2 = paint.measureText(charArray, i4, i5 - i4);
            if (z5 || measureText2 > measureText) {
                int i6 = z5 ? i5 : i2 > i4 ? i2 : i5 - 1;
                linkedList.add(a(str.substring(i4, i6), z));
                if (z5) {
                    i4 = i6 + 1;
                    if (z4) {
                        linkedList.add(null);
                    }
                    z = true;
                } else {
                    z = false;
                    i4 = i6;
                }
            }
            if (z2) {
                i2 = i5 + 1;
            }
            if (z3) {
                i2 = i3 + 1;
            } else {
                i3 = i5;
            }
        }
        linkedList.add(a(str.substring(i4), z));
        String[] strArr2 = new String[linkedList.size()];
        linkedList.toArray(strArr2);
        return strArr2;
    }

    private float b() {
        if (this.l == null || this.l.d != 1) {
            return 14.0f;
        }
        return this.l.i;
    }

    private int b(int i2) {
        return View.MeasureSpec.getSize(i2);
    }

    private void b(Canvas canvas) {
        int width = getWidth();
        int height = getHeight();
        a(canvas);
        if (isPressed()) {
            canvas.drawColor(872415231);
        }
        Bitmap a2 = s.a(n.a, 0, n.a.length);
        if (a2 != null) {
            int width2 = a2.getWidth();
            if (width2 > 25) {
                width2 = 23;
            }
            int height2 = a2.getHeight();
            if (height2 > 25) {
                height2 = 23;
            }
            int i2 = ((height - 25) - ((25 - height2) / 2)) - height2;
            int i3 = (25 - width2) / 2;
            a(canvas, new Rect(i3, i2, width2 + i3, height2 + i2), a2);
            a2.recycle();
        }
        if (hasFocus()) {
            a(canvas, new Rect(0, 1, width, height));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    private void b(Canvas canvas, Rect rect, int i2) {
        String str = this.l.f;
        if (str != null) {
            this.k.setColor(i2);
            if (this.m == null) {
                this.m = a(this.k, str, (float) (rect.width() - (getHeight() / 2)));
                this.n = 0;
                this.o = System.currentTimeMillis();
            }
            Paint.FontMetrics fontMetrics = this.k.getFontMetrics();
            float f2 = fontMetrics.descent - fontMetrics.ascent;
            if (this.m.length < 3) {
                float height = (((((float) rect.height()) - f2) - f2) / 2.0f) + ((float) (-((int) this.k.ascent())));
                float f3 = height;
                for (String drawText : this.m) {
                    canvas.drawText(drawText, (float) rect.left, f3, this.k);
                    f3 += f2;
                }
                return;
            }
            long currentTimeMillis = System.currentTimeMillis();
            if (!this.p && currentTimeMillis > this.o + 5000) {
                this.o = currentTimeMillis;
                this.p = true;
                this.s = currentTimeMillis;
            }
            if (this.p) {
                float max = Math.max(0.0f, 1.0f - (((float) (currentTimeMillis - this.s)) / 1000.0f));
                this.k.setAlpha((int) (255.0f * max));
                int min = Math.min(2, this.m.length - this.n);
                float height2 = ((((float) rect.height()) - (((float) min) * f2)) / 2.0f) + (((float) min) * f2);
                canvas.drawText(this.m[this.n], (float) rect.left, ((height2 * max) - (((float) min) * f2)) - fontMetrics.ascent, this.k);
                if (min > 1) {
                    canvas.drawText(this.m[this.n + 1], (float) rect.left, (height2 * max) - fontMetrics.descent, this.k);
                }
                int i3 = 0;
                if (this.m.length - this.n >= 2) {
                    i3 = (this.n + 2) % this.m.length;
                }
                int min2 = Math.min(2, this.m.length - i3);
                float height3 = (((float) rect.height()) - (((float) min2) * f2)) / 2.0f;
                this.k.setAlpha(255 - this.k.getAlpha());
                canvas.drawText(this.m[i3], (float) rect.left, (((((float) rect.height()) - height3) * max) + height3) - fontMetrics.ascent, this.k);
                if (min2 > 1) {
                    canvas.drawText(this.m[i3 + 1], (float) rect.left, ((f2 + height3) + ((((float) rect.height()) - height3) * max)) - fontMetrics.ascent, this.k);
                }
                if (max == 0.0f) {
                    this.p = false;
                    postInvalidateDelayed(3000);
                    this.n = i3;
                    this.o = currentTimeMillis;
                    return;
                }
                postInvalidateDelayed(50);
                return;
            }
            this.k.setAlpha(255);
            int min3 = Math.min(2, this.m.length - this.n);
            float height4 = ((((float) rect.height()) - (((float) min3) * f2)) / 2.0f) + ((float) (-((int) this.k.ascent())));
            for (int i4 = 0; i4 < min3; i4++) {
                canvas.drawText(this.m[this.n + i4], (float) rect.left, height4, this.k);
                height4 += f2;
            }
            postInvalidateDelayed(3000);
        }
    }

    private float c() {
        Paint.FontMetrics fontMetrics = this.k.getFontMetrics();
        return fontMetrics.descent - fontMetrics.ascent;
    }

    private int c(int i2) {
        int applyDimension;
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        switch (mode) {
            case Integer.MIN_VALUE:
                if (this.l.d != 3) {
                    if (this.l.d != 1) {
                        applyDimension = (int) TypedValue.applyDimension(1, 50.0f, getContext().getResources().getDisplayMetrics());
                        break;
                    } else {
                        this.k.setTextSize(b());
                        applyDimension = Math.min((int) (c() + 4.0f + 4.0f), size);
                        break;
                    }
                } else {
                    applyDimension = size;
                    break;
                }
            case 1073741824:
                if (this.l.d != 2) {
                    applyDimension = size;
                    break;
                } else {
                    applyDimension = (int) TypedValue.applyDimension(1, 50.0f, getContext().getResources().getDisplayMetrics());
                    break;
                }
            default:
                if (this.l.d != 3) {
                    if (this.l.d != 1) {
                        applyDimension = (int) TypedValue.applyDimension(1, 50.0f, getContext().getResources().getDisplayMetrics());
                        break;
                    } else {
                        applyDimension = (int) (c() + 4.0f + 4.0f);
                        break;
                    }
                } else {
                    applyDimension = size;
                    break;
                }
        }
        if (this.l.d == 2) {
            a = (int) (((float) applyDimension) - ((10.0f * ((float) applyDimension)) / 50.0f));
            d(applyDimension);
        }
        return applyDimension;
    }

    private void c(Canvas canvas) {
        Rect rect = new Rect(0, 0, getWidth(), getHeight());
        if (this.m == null) {
            this.m = a(this.k, this.l.f, (((float) (rect.width() - 25)) - 4.0f) - 4.0f);
            this.n = 0;
            this.o = System.currentTimeMillis();
        }
        if (isPressed()) {
            a(canvas, rect, -19456);
            d(canvas);
            return;
        }
        a(canvas, rect, this.c);
        d(canvas);
        if (hasFocus()) {
            rect.top++;
            a(canvas, rect);
        }
    }

    private void d() {
        if (this.l != null) {
            if (isPressed()) {
                setPressed(false);
            }
            e();
        }
    }

    private void d(int i2) {
        int i3 = ((int) (((float) i2) - 16.0f)) / 2;
        this.k.setTextSize((float) i3);
        int i4 = i3;
        while (c() > ((float) i3)) {
            i4 -= 2;
            this.k.setTextSize((float) i4);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    private void d(Canvas canvas) {
        long currentTimeMillis = System.currentTimeMillis();
        if (!this.p && currentTimeMillis > this.o + 5000) {
            this.o = currentTimeMillis;
            if (this.m.length > 1) {
                this.p = true;
                this.s = currentTimeMillis;
            }
        }
        String str = this.m[this.n];
        String str2 = this.m[this.n >= this.m.length - 1 ? 0 : this.n + 1];
        Paint.FontMetrics fontMetrics = this.k.getFontMetrics();
        float f2 = fontMetrics.descent - fontMetrics.ascent;
        float height = (((float) getHeight()) - f2) / 2.0f;
        float f3 = f2 + height;
        this.k.setAlpha(255);
        this.k.setColor(this.d);
        if (this.p) {
            float max = Math.max(0.0f, 1.0f - (((float) (currentTimeMillis - this.s)) / 1000.0f));
            this.k.setAlpha((int) (255.0f * max));
            canvas.drawText(str, 4.0f, (f3 * max) - fontMetrics.descent, this.k);
            this.k.setAlpha(255 - this.k.getAlpha());
            canvas.drawText(str2, 4.0f, (((((float) getHeight()) - height) * max) + height) - fontMetrics.ascent, this.k);
            if (max == 0.0f) {
                this.p = false;
                postInvalidateDelayed(3000);
                this.n = (this.n + 1) % this.m.length;
                this.o = currentTimeMillis;
                return;
            }
            postInvalidateDelayed(50);
            return;
        }
        canvas.drawText(str, 4.0f, height - fontMetrics.ascent, this.k);
        postInvalidateDelayed(3000);
    }

    private int e(int i2) {
        return (int) TypedValue.applyDimension(1, (float) i2, getResources().getDisplayMetrics());
    }

    private void e() {
        Uri parse;
        String substring;
        String substring2;
        boolean equals = "application/x-search".equals(this.l.r);
        final Context applicationContext = getContext().getApplicationContext();
        if (!equals) {
            new Thread() {
                public void run() {
                    b.a(applicationContext, a.this.l);
                }
            }.start();
        }
        if (equals) {
            ((AdView) getParent()).b();
        } else if ("text/html".equals(this.l.r) || "application/x-app-store".equals(this.l.r)) {
            Uri parse2 = Uri.parse(this.l.q);
            boolean parseBoolean = Boolean.parseBoolean(parse2.getQueryParameter("mini"));
            if (!parseBoolean) {
                parseBoolean = this.l.q.startsWith("http://d.wiyun.com/adv/s?");
            }
            if (parseBoolean) {
                g();
            } else if ("apk".equalsIgnoreCase(f.d(this.l.q))) {
                new b(this.l.q).start();
            } else {
                Intent intent = new Intent("android.intent.action.VIEW", parse2);
                intent.addFlags(268435456);
                try {
                    applicationContext.startActivity(intent);
                } catch (Exception e2) {
                    Log.e("WiYun", "Could not open viewer on ad click to " + this.l.q, e2);
                }
            }
        } else if ("text/x-phone-number".equals(this.l.r)) {
            Intent intent2 = getContext().checkCallingOrSelfPermission("android.permission.CALL_PHONE") == 0 ? new Intent("android.intent.action.CALL") : new Intent("android.intent.action.DIAL");
            intent2.addFlags(268435456);
            intent2.setData(Uri.parse("tel:" + this.l.q));
            try {
                applicationContext.startActivity(intent2);
            } catch (Exception e3) {
                Log.e("WiYun", "Could not call phone number " + this.l.q, e3);
            }
        } else if ("text/x-sms-number".equals(this.l.r)) {
            Intent intent3 = new Intent("android.intent.action.SENDTO");
            intent3.addFlags(268435456);
            intent3.setData(Uri.parse("smsto:" + this.l.q));
            if (!TextUtils.isEmpty(this.l.j)) {
                intent3.putExtra("sms_body", this.l.j);
            }
            try {
                applicationContext.startActivity(intent3);
            } catch (Exception e4) {
                Log.e("WiYun", "Could not send sms to number " + this.l.q, e4);
            }
        } else if ("audio/mp3".equals(this.l.r) || "video/3gpp".equals(this.l.r) || "video/mp4".equals(this.l.r)) {
            Intent intent4 = new Intent("android.intent.action.VIEW");
            intent4.addFlags(268435456);
            intent4.setDataAndType(Uri.parse(this.l.q), this.l.r);
            try {
                applicationContext.startActivity(intent4);
            } catch (Exception e5) {
                Log.e("WiYun", "Could not open browser on ad click to " + this.l.q, e5);
            }
        } else if ("application/x-map".equals(this.l.r)) {
            if (this.l.q.startsWith("addr://")) {
                parse = Uri.parse(String.format("geo:0,0?q=%s", this.l.q.substring("addr://".length())));
            } else if (this.l.q.startsWith("loc://")) {
                int lastIndexOf = this.l.q.lastIndexOf(64);
                if (lastIndexOf == -1) {
                    substring = this.l.q.substring("loc://".length());
                    substring2 = null;
                } else {
                    substring = this.l.q.substring("loc://".length(), lastIndexOf);
                    substring2 = this.l.q.substring(lastIndexOf + 1);
                }
                StringBuilder sb = new StringBuilder();
                sb.append("http://maps.google.com/maps?q=");
                sb.append(substring);
                if (!TextUtils.isEmpty(substring2)) {
                    sb.append('(').append(substring2).append(')');
                }
                parse = Uri.parse(sb.toString());
            } else {
                parse = this.l.q.startsWith("http://") ? Uri.parse(this.l.q) : null;
            }
            if (parse != null) {
                Intent intent5 = new Intent("android.intent.action.VIEW", parse);
                try {
                    if (getContext().getPackageManager().getPackageInfo("com.google.android.apps.maps", 0) != null) {
                        intent5.setComponent(new ComponentName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity"));
                    }
                } catch (PackageManager.NameNotFoundException e6) {
                }
                intent5.addFlags(268435456);
                try {
                    applicationContext.startActivity(intent5);
                } catch (Exception e7) {
                    Log.e("WiYun", "Could not open google map on ad click to " + this.l.q, e7);
                }
            }
        }
        if (!equals && this.t != null) {
            this.t.onAdClicked();
        }
        if (!equals) {
            h();
        }
    }

    private void e(Canvas canvas) {
        Rect rect;
        Rect rect2;
        int i2;
        int i3;
        Rect rect3 = new Rect(0, 0, getWidth(), getHeight());
        Bitmap bitmap = this.l.t;
        Bitmap bitmap2 = this.l.s;
        int height = rect3.height();
        if (height > 0) {
            if (bitmap != null) {
                int width = bitmap.getWidth();
                int height2 = bitmap.getHeight();
                if (width == height2) {
                    if (width > a || (width * 100) / height < 67) {
                        width = a;
                    }
                    int i4 = (height - width) / 2;
                    Rect rect4 = new Rect(rect3.left + i4, rect3.top + i4, rect3.left + i4 + width, width + rect3.top + i4);
                    rect2 = new Rect(i4 + rect4.right, rect3.top, rect3.right - 8, rect3.bottom);
                    rect = rect4;
                } else {
                    boolean z = width > height2;
                    int i5 = width > height2 ? width : height2;
                    if (i5 > a || (i5 * 100) / height < 67) {
                        int i6 = a;
                        if (z) {
                            i3 = (height2 * i6) / width;
                            i2 = i6;
                        } else {
                            i2 = (width * i6) / height2;
                            i3 = i6;
                        }
                    } else {
                        int i7 = height2;
                        i2 = width;
                        i3 = i7;
                    }
                    int i8 = (height - i2) / 2;
                    int i9 = (height - i3) / 2;
                    Rect rect5 = new Rect(rect3.left + i8, rect3.top + i9, i2 + rect3.left + i8, i3 + i9 + rect3.top);
                    rect2 = new Rect(rect5.right + i8, rect3.top, rect3.right - 8, rect3.bottom);
                    rect = rect5;
                }
            } else {
                rect = null;
                rect2 = new Rect(rect3.left + 8, rect3.top, rect3.right - 8, rect3.bottom);
            }
            if (isPressed()) {
                a(canvas, rect3, -19456);
                if (bitmap2 != null) {
                    a(canvas);
                } else {
                    b(canvas, rect2, -16777216);
                }
            } else {
                a(canvas, rect3, this.c);
                if (bitmap2 != null) {
                    a(canvas);
                } else {
                    b(canvas, rect2, this.d);
                }
                if (hasFocus()) {
                    rect3.top++;
                    a(canvas, rect3);
                    rect3.top--;
                }
            }
            boolean z2 = false;
            if (bitmap != null) {
                if (this.e && SystemClock.uptimeMillis() - this.f > 4200) {
                    this.h = null;
                    this.e = false;
                    setClickable(true);
                }
                if (this.e) {
                    if (!this.h.isInitialized()) {
                        this.h.initialize(a, a, a, height);
                    }
                    z2 = this.h.getTransformation(SystemClock.uptimeMillis(), this.i);
                    if (z2) {
                        Matrix matrix = canvas.getMatrix();
                        canvas.save();
                        matrix.preConcat(this.i.getMatrix());
                        canvas.setMatrix(matrix);
                        a(canvas, rect, this.g == 0 ? bitmap : this.j);
                        canvas.restore();
                    }
                } else {
                    a(canvas, rect, bitmap);
                }
            }
            if (bitmap2 != null && isPressed()) {
                canvas.drawColor(872415231);
            }
            Bitmap a2 = s.a(n.a, 0, n.a.length);
            if (a2 != null) {
                a(canvas, new Rect((rect3.right - 2) - a2.getWidth(), (rect3.height() - a2.getHeight()) / 2, rect3.right - 2, ((rect3.height() - a2.getHeight()) / 2) + a2.getHeight()), a2);
                a2.recycle();
            }
            if (bitmap != null && this.e) {
                if (!z2) {
                    switch (this.g) {
                        case 0:
                            if (this.j == null) {
                                try {
                                    this.j = s.a(c.d, 0, c.d.length);
                                } catch (OutOfMemoryError e2) {
                                }
                            }
                            if (this.j != null) {
                                RotateAnimation rotateAnimation = new RotateAnimation(0.0f, 1080.0f, (float) (getHeight() / 2), (float) (getHeight() / 2));
                                rotateAnimation.setDuration(3600);
                                rotateAnimation.setInterpolator(new c(36));
                                rotateAnimation.setRepeatMode(-1);
                                rotateAnimation.setStartTime(-1);
                                this.h = rotateAnimation;
                                this.g++;
                                break;
                            } else {
                                this.h = null;
                                this.e = false;
                                setClickable(true);
                                break;
                            }
                        case 1:
                            this.h = null;
                            this.e = false;
                            setClickable(true);
                            break;
                    }
                }
                invalidate();
            }
        }
    }

    /* access modifiers changed from: private */
    public void f() {
        if (this.r != null) {
            ((ViewGroup) this.r.getParent()).removeView(this.r);
            this.r = null;
            this.q = false;
        }
    }

    private void g() {
        boolean z;
        View view;
        if (!this.q) {
            this.q = true;
            Activity activity = (Activity) getContext();
            this.r = new q(activity);
            View findViewById = activity.findViewById(16908290);
            if (findViewById == null) {
                view = activity.getWindow().getDecorView();
                z = true;
            } else {
                View view2 = findViewById;
                z = false;
                view = view2;
            }
            int width = view.getWidth();
            int height = view.getHeight();
            int e2 = width - e(20);
            int e3 = height - e((z ? 50 : 0) + 20);
            int e4 = e2 - e(20);
            int e5 = e3 - e(20);
            FrameLayout frameLayout = new FrameLayout(activity);
            frameLayout.setBackgroundDrawable(s.b());
            frameLayout.setPadding(e(10), e(10), e(10), e(10));
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(e2, e3);
            layoutParams.gravity = 1;
            this.r.addView(frameLayout, layoutParams);
            WebView webView = new WebView(activity);
            webView.setScrollBarStyle(33554432);
            webView.setMapTrackballToArrowKeys(false);
            webView.getSettings().setJavaScriptEnabled(true);
            webView.getSettings().setBuiltInZoomControls(true);
            webView.setDownloadListener(this);
            LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-1, -1);
            layoutParams2.weight = 1.0f;
            frameLayout.addView(webView, layoutParams2);
            LinearLayout linearLayout = new LinearLayout(activity);
            linearLayout.setId(4096);
            linearLayout.setOrientation(1);
            linearLayout.setGravity(17);
            linearLayout.setClickable(true);
            linearLayout.setBackgroundDrawable(s.c());
            linearLayout.setPadding(e(30), e(30), e(30), e(30));
            ProgressBar progressBar = new ProgressBar(activity);
            progressBar.setIndeterminate(true);
            linearLayout.addView(progressBar, new LinearLayout.LayoutParams(e(48), e(48)));
            FrameLayout.LayoutParams layoutParams3 = new FrameLayout.LayoutParams(-1, -1);
            layoutParams3.gravity = 17;
            frameLayout.addView(linearLayout, layoutParams3);
            Button button = new Button(activity);
            button.setBackgroundDrawable(s.d());
            FrameLayout.LayoutParams layoutParams4 = new FrameLayout.LayoutParams(e(29), e(29));
            layoutParams4.gravity = 53;
            this.r.addView(button, layoutParams4);
            button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    a.this.f();
                }
            });
            this.r.setFocusable(true);
            this.r.setFocusableInTouchMode(true);
            this.r.setOnKeyListener(new View.OnKeyListener() {
                public boolean onKey(View view, int i, KeyEvent keyEvent) {
                    switch (keyEvent.getAction()) {
                        case 0:
                            if (i == 4) {
                                return true;
                            }
                            break;
                        case 1:
                            if (i == 4) {
                                a.this.f();
                                break;
                            }
                            break;
                    }
                    return false;
                }
            });
            this.r.requestFocus();
            FrameLayout.LayoutParams layoutParams5 = new FrameLayout.LayoutParams(-2, -2);
            layoutParams5.gravity = 17;
            TranslateAnimation translateAnimation = new TranslateAnimation(1, 0.0f, 1, 0.0f, 2, 1.0f, 1, 0.0f);
            translateAnimation.setDuration(500);
            translateAnimation.setInterpolator(new DecelerateInterpolator());
            activity.addContentView(this.r, layoutParams5);
            this.r.startAnimation(translateAnimation);
            webView.setWebViewClient(new C0000a(linearLayout));
            StringBuffer stringBuffer = new StringBuffer(this.l.q);
            stringBuffer.append("&embed=true&width=").append(e4).append("&height=").append(e5);
            webView.loadUrl(stringBuffer.toString());
        }
    }

    private void h() {
        if (this.l.d == 2 && this.l.t != null) {
            this.e = true;
            setClickable(false);
            this.g = 0;
            AnimationSet animationSet = new AnimationSet(getContext(), null);
            ScaleAnimation scaleAnimation = new ScaleAnimation(1.0f, 1.2f, 1.0f, 1.2f, (float) (a / 2), (float) (a / 2));
            scaleAnimation.setDuration(200);
            scaleAnimation.setInterpolator(new DecelerateInterpolator());
            animationSet.addAnimation(scaleAnimation);
            ScaleAnimation scaleAnimation2 = new ScaleAnimation(1.2f, 0.0f, 1.2f, 0.0f, (float) (a / 2), (float) (a / 2));
            scaleAnimation2.setDuration(400);
            scaleAnimation2.setInterpolator(new AccelerateInterpolator());
            scaleAnimation2.setStartOffset(200);
            animationSet.addAnimation(scaleAnimation2);
            animationSet.setStartTime(-1);
            this.h = animationSet;
            this.f = SystemClock.uptimeMillis();
            invalidate();
        }
    }

    /* access modifiers changed from: protected */
    public i a() {
        return this.l;
    }

    public void a(int i2) {
        this.d = i2;
        postInvalidate();
    }

    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (action == 0) {
            setPressed(true);
            return true;
        } else if (action == 2) {
            float x = motionEvent.getX();
            float y = motionEvent.getY();
            int left = getLeft();
            int top = getTop();
            int right = getRight();
            int bottom = getBottom();
            if (x < ((float) left) || x > ((float) right) || y < ((float) top) || y > ((float) bottom)) {
                setPressed(false);
            } else {
                setPressed(true);
            }
            return true;
        } else if (action != 1) {
            return super.dispatchTouchEvent(motionEvent);
        } else {
            if (isPressed()) {
                d();
            }
            setPressed(false);
            return true;
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        this.e = false;
        if (this.j != null && !this.j.isRecycled()) {
            this.j.recycle();
        }
        this.j = null;
        super.onDetachedFromWindow();
    }

    public void onDownloadStart(String str, String str2, String str3, String str4, long j2) {
        new b(str).start();
        f();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        try {
            super.onDraw(canvas);
            if (this.l != null) {
                switch (this.l.d) {
                    case 1:
                        c(canvas);
                        return;
                    case 2:
                        e(canvas);
                        return;
                    case 3:
                        b(canvas);
                        return;
                    default:
                        return;
                }
                Log.e("WiYun", "Exception raised during onDraw.", e);
            }
        } catch (Exception e2) {
            Log.e("WiYun", "Exception raised during onDraw.", e2);
        }
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 66 || i2 == 23) {
            setPressed(true);
        }
        return super.onKeyDown(i2, keyEvent);
    }

    public boolean onKeyUp(int i2, KeyEvent keyEvent) {
        setPressed(false);
        if (i2 == 66 || i2 == 23) {
            d();
        }
        return super.onKeyUp(i2, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        int i4 = 0;
        int c2 = c(i3);
        if (c2 != 0) {
            i4 = b(i2);
        }
        setMeasuredDimension(i4, c2);
        if (Log.isLoggable("WiYun", 3)) {
            Log.d("WiYun", "AdContainer.onMeasure() determined the ad to be " + i4 + "x" + c2 + " pixels.");
        }
    }

    public void setBackgroundColor(int i2) {
        this.c = i2;
        postInvalidate();
    }

    public void setPressed(boolean z) {
        if (isPressed() != z) {
            super.setPressed(z);
            invalidate();
        }
    }
}
