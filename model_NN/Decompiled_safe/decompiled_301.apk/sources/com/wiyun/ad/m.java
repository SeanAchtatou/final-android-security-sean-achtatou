package com.wiyun.ad;

import android.content.Context;
import android.content.res.Configuration;
import android.hardware.SensorManager;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import java.util.Locale;

class m {
    private static String a;
    private static String b;
    private static String c;
    private static int d = -1;

    m() {
    }

    static String a() {
        if (a == null) {
            StringBuffer stringBuffer = new StringBuffer();
            String str = Build.VERSION.RELEASE;
            if (str.length() > 0) {
                stringBuffer.append(str);
            } else {
                stringBuffer.append("1.0");
            }
            stringBuffer.append("; ");
            Locale locale = Locale.getDefault();
            String language = locale.getLanguage();
            if (language != null) {
                stringBuffer.append(language.toLowerCase());
                String country = locale.getCountry();
                if (country != null) {
                    stringBuffer.append("-");
                    stringBuffer.append(country.toLowerCase());
                }
            } else {
                stringBuffer.append("en");
            }
            String str2 = Build.DEVICE;
            if (str2.length() > 0) {
                stringBuffer.append("; ");
                stringBuffer.append(str2);
            }
            a = String.format("Mozilla/5.0 (Linux; U; Android %s) AppleWebKit/525.10+ (KHTML, like Gecko) Version/3.0.4 Mobile Safari/523.12.2", stringBuffer);
        }
        return a;
    }

    static String a(Context context) {
        if (c == null) {
            if (b(context)) {
                c = "000000000000000";
            } else if (context == null) {
                a("Context is not set");
            } else if (context.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") == -1) {
                a("Cannot get a device ID.  Open manifest.xml and just before the final </manifest> tag add:  <uses-permission android:name=\"android.permission.READ_PHONE_STATE\" />");
            } else {
                TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
                if (telephonyManager != null) {
                    c = telephonyManager.getDeviceId();
                    if (!b(c)) {
                        c = "000000000000000";
                    }
                } else {
                    c = "000000000000000";
                    Log.w("WiYun", "No device ID available.");
                }
            }
        }
        return c;
    }

    protected static void a(String str) {
        Log.e("WiYun", str);
        throw new IllegalArgumentException(str);
    }

    static boolean b(Context context) {
        if (d == -1) {
            String f = f(context);
            boolean startsWith = Build.FINGERPRINT.startsWith("generic");
            boolean equalsIgnoreCase = "sdk".equalsIgnoreCase(Build.MODEL);
            boolean g = g(context);
            int i = (TextUtils.isEmpty(f) || !b(f)) ? 0 + 30 : 0;
            if (startsWith) {
                i += 30;
            }
            if (equalsIgnoreCase) {
                i += 20;
            }
            if (!g) {
                i += 50;
            }
            d = i >= 50 ? 1 : 0;
        }
        return d != 0;
    }

    private static boolean b(String str) {
        if (str == null) {
            return false;
        }
        int length = str.length();
        for (int i = 0; i < length; i++) {
            if (str.charAt(i) != '0') {
                return true;
            }
        }
        return false;
    }

    public static int c(Context context) {
        if (d(context)) {
            return 4;
        }
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        if (telephonyManager == null) {
            return 0;
        }
        int networkType = telephonyManager.getNetworkType();
        return (networkType == 1 || networkType == 2 || networkType == 0) ? 2 : 3;
    }

    static boolean d(Context context) {
        if (context.checkCallingOrSelfPermission("android.permission.ACCESS_WIFI_STATE") != 0) {
            return false;
        }
        WifiManager wifiManager = (WifiManager) context.getSystemService("wifi");
        if (wifiManager == null) {
            return false;
        }
        WifiInfo connectionInfo = wifiManager.getConnectionInfo();
        return connectionInfo != null && connectionInfo.getSupplicantState() == SupplicantState.COMPLETED;
    }

    static String e(Context context) {
        if (TextUtils.isEmpty(b)) {
            Configuration configuration = context.getResources().getConfiguration();
            b = String.valueOf(configuration.mnc + (configuration.mcc * 100));
        }
        return b;
    }

    private static String f(Context context) {
        if (context == null) {
            a("Context is not set");
            return null;
        } else if (context.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") == -1) {
            a("Cannot get a device ID.  Open manifest.xml and just before the final </manifest> tag add:  <uses-permission android:name=\"android.permission.READ_PHONE_STATE\" />");
            return null;
        } else {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
            if (telephonyManager != null) {
                return telephonyManager.getDeviceId();
            }
            return null;
        }
    }

    private static boolean g(Context context) {
        if (context == null) {
            return false;
        }
        SensorManager sensorManager = (SensorManager) context.getSystemService("sensor");
        if (sensorManager == null) {
            return false;
        }
        return sensorManager.getDefaultSensor(2) != null;
    }
}
