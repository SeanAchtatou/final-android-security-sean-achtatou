package com.admogo.util;

import android.app.Activity;
import android.util.DisplayMetrics;

public class AdMogoUtil extends Activity {
    public static final String ADMOGO = "AdsMOGO SDK";
    public static final int CUSTOM_TYPE_BANNER = 1;
    public static final int CUSTOM_TYPE_ICON = 2;
    public static final int MOGO_TYPE_BANNER = 1;
    public static final int MOGO_TYPE_ICON = 2;
    public static final int NETWORK_TYPE_4THSCREEN = 13;
    public static final int NETWORK_TYPE_ADCHINA = 21;
    public static final int NETWORK_TYPE_ADMOB = 1;
    public static final int NETWORK_TYPE_ADMOGO = 10;
    public static final int NETWORK_TYPE_ADSENSE = 14;
    public static final int NETWORK_TYPE_ADTOUCH = 28;
    public static final int NETWORK_TYPE_ADWO = 33;
    public static final int NETWORK_TYPE_AIRAD = 32;
    public static final int NETWORK_TYPE_APPMEDIA = 36;
    public static final int NETWORK_TYPE_CASEE = 25;
    public static final int NETWORK_TYPE_CUSTOM = 9;
    public static final int NETWORK_TYPE_DOMOB = 29;
    public static final int NETWORK_TYPE_DOUBLECLICK = 15;
    public static final int NETWORK_TYPE_EVENT = 17;
    public static final int NETWORK_TYPE_GENERIC = 16;
    public static final int NETWORK_TYPE_GREYSTRIP = 7;
    public static final int NETWORK_TYPE_INMOBI = 18;
    public static final int NETWORK_TYPE_IZP = 40;
    public static final int NETWORK_TYPE_JUMPTAP = 2;
    public static final int NETWORK_TYPE_LIVERAIL = 5;
    public static final int NETWORK_TYPE_LSENSE = 34;
    public static final int NETWORK_TYPE_MDOTM = 12;
    public static final int NETWORK_TYPE_MEDIALETS = 4;
    public static final int NETWORK_TYPE_MILLENNIAL = 6;
    public static final int NETWORK_TYPE_MOBCLIX = 11;
    public static final int NETWORK_TYPE_MOGO = 27;
    public static final int NETWORK_TYPE_QUATTRO = 8;
    public static final int NETWORK_TYPE_SMAATO = 35;
    public static final int NETWORK_TYPE_SMART = 26;
    public static final int NETWORK_TYPE_VIDEOEGG = 3;
    public static final int NETWORK_TYPE_VPON = 30;
    public static final int NETWORK_TYPE_WINAD = 37;
    public static final int NETWORK_TYPE_WIYUN = 22;
    public static final int NETWORK_TYPE_WOOBOO = 23;
    public static final int NETWORK_TYPE_YOUMI = 24;
    public static final int NETWORK_TYPE_ZESTADZ = 20;
    public static final String VER = "1.0.4";
    public static final int VERSION = 276;
    private static double density = -1.0d;

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v1, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v3, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v4, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String convertToHex(byte[] r4) {
        /*
            java.lang.StringBuffer r0 = new java.lang.StringBuffer
            r0.<init>()
            r2 = 0
        L_0x0006:
            int r3 = r4.length
            if (r2 < r3) goto L_0x000e
            java.lang.String r3 = r0.toString()
            return r3
        L_0x000e:
            byte r1 = r4[r2]
            if (r1 >= 0) goto L_0x0014
            int r1 = r1 + 256
        L_0x0014:
            r3 = 16
            if (r1 >= r3) goto L_0x001d
            java.lang.String r3 = "0"
            r0.append(r3)
        L_0x001d:
            java.lang.String r3 = java.lang.Integer.toHexString(r1)
            r0.append(r3)
            int r2 = r2 + 1
            goto L_0x0006
        */
        throw new UnsupportedOperationException("Method not decompiled: com.admogo.util.AdMogoUtil.convertToHex(byte[]):java.lang.String");
    }

    public static String convertToHexDeviceID(byte[] data) {
        StringBuffer buf = new StringBuffer();
        for (byte element : data) {
            int halfbyte = (element >>> 4) & 15;
            int two_halfs = 0;
            while (true) {
                if (halfbyte < 0 || halfbyte > 9) {
                    buf.append((char) ((halfbyte - 10) + 97));
                } else {
                    buf.append((char) (halfbyte + 48));
                }
                halfbyte = element & 15;
                int two_halfs2 = two_halfs + 1;
                if (two_halfs >= 1) {
                    break;
                }
                two_halfs = two_halfs2;
            }
        }
        return buf.toString();
    }

    public static double getDensity(Activity activity) {
        if (density == -1.0d) {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            density = (double) displayMetrics.density;
        }
        return density;
    }

    public static int convertToScreenPixels(int dipPixels, double density2) {
        return (int) convertToScreenPixels((double) dipPixels, density2);
    }

    public static double convertToScreenPixels(double dipPixels, double density2) {
        return density2 > 0.0d ? dipPixels * density2 : dipPixels;
    }
}
