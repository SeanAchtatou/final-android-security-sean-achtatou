package com.admogo;

import android.app.ActivityManager;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

public class GetUserInfo {
    public static String getDeviceID(Context context) {
        return new UUID((long) (Settings.Secure.getString(context.getContentResolver(), "android_id")).hashCode(), ((long) (((TelephonyManager) context.getSystemService("phone")).getDeviceId()).hashCode()) << 32).toString();
    }

    public static String getNetworkType(Context context) {
        NetworkInfo networkinfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (networkinfo == null) {
            return "0";
        }
        String networkType = networkinfo.getTypeName();
        if (networkType.equalsIgnoreCase("mobile")) {
            return "1";
        }
        if (networkType.equalsIgnoreCase("wifi")) {
            return "2";
        }
        return networkType;
    }

    public static String getOperators(Context cx) {
        String mno = ((TelephonyManager) cx.getSystemService("phone")).getSimOperator();
        if (mno.equals("") || mno == null) {
            mno = "00000";
        }
        return mno;
    }

    public static String GetCPUInfo() {
        try {
            String[] cpuTotalInfo = run(new String[]{"/system/bin/cat", "/proc/cpuinfo"}, "/system/bin/").split("\n");
            int cpuProcessorCount = 0;
            String cpuMIPS = "";
            for (String split : cpuTotalInfo) {
                String[] cpuInfo = split.split("\t: ");
                if (cpuInfo[0].equals("processor")) {
                    cpuProcessorCount = Integer.parseInt(cpuInfo[1]);
                }
                if (cpuInfo[0].equals("BogoMIPS")) {
                    cpuMIPS = cpuInfo[1];
                }
            }
            return "&cc=" + (cpuProcessorCount + 1) + "&cm=" + cpuMIPS;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getMemoryInfo(Context context) {
        ((ActivityManager) context.getSystemService("activity")).getMemoryInfo(new ActivityManager.MemoryInfo());
        try {
            return run(new String[]{"/system/bin/cat", "/proc/meminfo"}, "/system/bin/").split("\n")[0].split(":")[1].replace("\t", "").replace(" ", "").toLowerCase();
        } catch (IOException e) {
            Log.i("fetch_process_info", "ex=" + e.toString());
            return "";
        }
    }

    public static synchronized String run(String[] cmd, String workdirectory) throws IOException {
        String result;
        synchronized (GetUserInfo.class) {
            result = "";
            try {
                ProcessBuilder builder = new ProcessBuilder(cmd);
                if (workdirectory != null) {
                    builder.directory(new File(workdirectory));
                }
                builder.redirectErrorStream(true);
                InputStream in = builder.start().getInputStream();
                byte[] re = new byte[1024];
                while (in.read(re) != -1) {
                    result = String.valueOf(result) + new String(re);
                }
                in.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }
}
