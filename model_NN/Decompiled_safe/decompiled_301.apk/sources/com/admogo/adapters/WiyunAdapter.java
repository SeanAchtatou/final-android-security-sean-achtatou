package com.admogo.adapters;

import android.app.Activity;
import android.graphics.Color;
import android.util.Log;
import android.view.ViewGroup;
import com.admogo.AdMogoLayout;
import com.admogo.obj.Extra;
import com.admogo.obj.Ration;
import com.admogo.util.AdMogoUtil;
import com.wiyun.ad.AdView;

public class WiyunAdapter extends AdMogoAdapter implements AdView.AdListener {
    private Activity activity;
    private AdView adView;

    public WiyunAdapter(AdMogoLayout adMogoLayout, Ration ration) {
        super(adMogoLayout, ration);
    }

    public void handle() {
        AdMogoLayout adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get();
        if (adMogoLayout != null) {
            this.activity = adMogoLayout.activityReference.get();
            if (this.activity != null) {
                this.adView = new AdView(this.activity);
                try {
                    this.adView.setResId(this.ration.key);
                    this.adView.setListener(this);
                    this.adView.setTestMode(this.ration.testmodel);
                    this.adView.setTestAdType(2);
                    Extra extra = adMogoLayout.extra;
                    int bgColor = Color.rgb(extra.bgRed, extra.bgGreen, extra.bgBlue);
                    int fgColor = Color.rgb(extra.fgRed, extra.fgGreen, extra.fgBlue);
                    this.adView.setBackgroundColor(bgColor);
                    this.adView.setTextColor(fgColor);
                    adMogoLayout.addView(this.adView, new ViewGroup.LayoutParams(-2, -2));
                    this.adView.requestAd();
                } catch (IllegalArgumentException e) {
                    adMogoLayout.rollover();
                }
            }
        }
    }

    public void onAdClicked() {
    }

    public void onAdLoadFailed() {
        AdMogoLayout adMogoLayout;
        Log.d(AdMogoUtil.ADMOGO, "WiYun failure");
        this.adView.setListener(null);
        if (!this.activity.isFinishing() && (adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get()) != null) {
            adMogoLayout.rollover();
        }
    }

    public void onAdLoaded() {
        AdMogoLayout adMogoLayout;
        Log.d(AdMogoUtil.ADMOGO, "Wiyun success");
        this.adView.setListener(null);
        if (!this.activity.isFinishing() && (adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get()) != null) {
            adMogoLayout.adMogoManager.resetRollover();
            adMogoLayout.handler.post(new AdMogoLayout.ViewAdRunnable(adMogoLayout, this.adView, 22));
            adMogoLayout.rotateThreadedDelayed();
        }
    }

    public void onExitButtonClicked() {
        Log.d(AdMogoUtil.ADMOGO, "WiYun onExitButtonClicked");
    }

    public void finish() {
        Log.d(AdMogoUtil.ADMOGO, "Wiyun Finished");
    }
}
