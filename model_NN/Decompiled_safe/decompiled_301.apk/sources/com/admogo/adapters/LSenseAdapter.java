package com.admogo.adapters;

import android.app.Activity;
import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import com.admogo.AdMogoLayout;
import com.admogo.obj.Extra;
import com.admogo.obj.Ration;
import com.admogo.util.AdMogoUtil;
import com.l.adlib_android.AdListenerEx;
import com.l.adlib_android.AdView;
import com.l.adlib_android.AdViewFull;

public class LSenseAdapter extends AdMogoAdapter implements AdListenerEx {
    private Activity activity;
    private AdView adView;
    private AdViewFull adViewFull;

    public LSenseAdapter(AdMogoLayout adMogoLayout, Ration ration) {
        super(adMogoLayout, ration);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: SimpleMethodDetails{com.admogo.AdMogoLayout.addView(android.view.View, android.view.ViewGroup$LayoutParams):void}
     arg types: [com.l.adlib_android.AdView, android.view.ViewGroup$LayoutParams]
     candidates:
      ClspMth{android.view.ViewGroup.addView(android.view.View, int):void}
      SimpleMethodDetails{com.admogo.AdMogoLayout.addView(android.view.View, android.view.ViewGroup$LayoutParams):void} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: SimpleMethodDetails{com.admogo.AdMogoLayout.addView(android.view.View, android.view.ViewGroup$LayoutParams):void}
     arg types: [com.l.adlib_android.AdViewFull, android.view.ViewGroup$LayoutParams]
     candidates:
      ClspMth{android.view.ViewGroup.addView(android.view.View, int):void}
      SimpleMethodDetails{com.admogo.AdMogoLayout.addView(android.view.View, android.view.ViewGroup$LayoutParams):void} */
    public void handle() {
        AdMogoLayout adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get();
        if (adMogoLayout != null) {
            this.activity = adMogoLayout.activityReference.get();
            if (this.activity != null) {
                Extra extra = adMogoLayout.extra;
                int bgColor = Color.rgb(extra.bgRed, extra.bgGreen, extra.bgBlue);
                int fgColor = Color.rgb(extra.fgRed, extra.fgGreen, extra.fgBlue);
                try {
                    if (adMogoLayout.getAdType() == 1) {
                        this.adView = new AdView(this.activity, Integer.parseInt(this.ration.key), Color.rgb(65, 65, 65), bgColor, fgColor, 255, false);
                        this.adView.setOnAdListenerEx(this);
                        adMogoLayout.addView((View) this.adView, new ViewGroup.LayoutParams(-2, -2));
                    } else if (adMogoLayout.getAdType() == 6) {
                        this.adViewFull = new AdViewFull(this.activity);
                        adMogoLayout.addView((View) this.adViewFull, new ViewGroup.LayoutParams(-1, -1));
                        this.adViewFull.GetFullAd();
                    }
                } catch (Exception e) {
                    adMogoLayout.rollover();
                }
            }
        }
    }

    public void OnAcceptAd(int arg0) {
        AdMogoLayout adMogoLayout;
        Log.d(AdMogoUtil.ADMOGO, "LSense success");
        this.adView.setOnAdListenerEx((AdListenerEx) null);
        if (!this.activity.isFinishing() && (adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get()) != null) {
            adMogoLayout.adMogoManager.resetRollover();
            adMogoLayout.handler.post(new AdMogoLayout.ViewAdRunnable(adMogoLayout, this.adView, 34));
            adMogoLayout.rotateThreadedDelayed();
        }
    }

    public void OnConnectFailed(String arg0) {
        AdMogoLayout adMogoLayout;
        Log.d(AdMogoUtil.ADMOGO, "LSense failure");
        this.adView.setOnAdListenerEx((AdListenerEx) null);
        if (!this.activity.isFinishing() && (adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get()) != null) {
            adMogoLayout.rollover();
        }
    }

    public void finish() {
        Log.d(AdMogoUtil.ADMOGO, "LSense Finished");
    }
}
