package com.admogo.adapters;

import android.app.Activity;
import android.graphics.Color;
import android.util.Log;
import com.admogo.AdMogoLayout;
import com.admogo.obj.Extra;
import com.admogo.obj.Ration;
import com.admogo.util.AdMogoUtil;
import com.adwo.adsdk.AdListener;
import com.adwo.adsdk.AdwoAdView;

public class AdwoAdapter extends AdMogoAdapter implements AdListener {
    private Activity activity;
    private AdMogoLayout adMogoLayout;
    private AdwoAdView adView;

    public AdwoAdapter(AdMogoLayout adMogoLayout2, Ration ration) {
        super(adMogoLayout2, ration);
    }

    public void handle() {
        this.adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get();
        if (this.adMogoLayout != null) {
            this.activity = this.adMogoLayout.activityReference.get();
            if (this.activity != null) {
                try {
                    if (this.adMogoLayout.getAdType() == 1) {
                        Extra extra = new Extra();
                        this.adView = new AdwoAdView(this.activity.getApplicationContext(), this.ration.key, 4194432, Color.rgb(extra.bgRed, extra.bgGreen, extra.bgBlue), this.ration.testmodel, 600);
                        this.adView.setListener(this);
                        this.adMogoLayout.addView(this.adView);
                    }
                } catch (Exception e) {
                    this.adMogoLayout.rollover();
                }
            }
        }
    }

    public void onFailedToReceiveAd(AdwoAdView arg0) {
        AdMogoLayout adMogoLayout2;
        Log.d(AdMogoUtil.ADMOGO, "AdWo failure");
        this.adView.setListener((AdListener) null);
        if (!this.activity.isFinishing() && (adMogoLayout2 = (AdMogoLayout) this.adMogoLayoutReference.get()) != null) {
            adMogoLayout2.rollover();
        }
    }

    public void onFailedToReceiveRefreshedAd(AdwoAdView arg0) {
    }

    public void onReceiveAd(AdwoAdView adView2) {
        AdMogoLayout adMogoLayout2;
        Log.d(AdMogoUtil.ADMOGO, "AdWo success");
        adView2.setListener((AdListener) null);
        if (!this.activity.isFinishing() && (adMogoLayout2 = (AdMogoLayout) this.adMogoLayoutReference.get()) != null) {
            adMogoLayout2.adMogoManager.resetRollover();
            adView2.finalize();
            adMogoLayout2.handler.post(new AdMogoLayout.ViewAdRunnable(adMogoLayout2, adView2, 33));
            adMogoLayout2.rotateThreadedDelayed();
        }
    }

    public void finish() {
        Log.d(AdMogoUtil.ADMOGO, "Adwo Finished");
    }
}
