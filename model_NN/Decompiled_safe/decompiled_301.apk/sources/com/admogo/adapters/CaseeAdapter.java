package com.admogo.adapters;

import android.app.Activity;
import android.graphics.Color;
import android.util.AttributeSet;
import android.util.Log;
import com.admogo.AdMogoLayout;
import com.admogo.obj.Extra;
import com.admogo.obj.Ration;
import com.admogo.util.AdMogoUtil;
import com.casee.adsdk.CaseeAdView;

public class CaseeAdapter extends AdMogoAdapter implements CaseeAdView.AdListener {
    private Activity activity;
    private CaseeAdView adView;

    public CaseeAdapter(AdMogoLayout adMogoLayout, Ration ration) {
        super(adMogoLayout, ration);
    }

    public void handle() {
        AdMogoLayout adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get();
        if (adMogoLayout != null) {
            this.activity = adMogoLayout.activityReference.get();
            if (this.activity != null) {
                Extra extra = adMogoLayout.extra;
                int bgColor = Color.rgb(extra.bgRed, extra.bgGreen, extra.bgBlue);
                int fgColor = Color.rgb(extra.fgRed, extra.fgGreen, extra.fgBlue);
                try {
                    if (this.ration.testmodel) {
                        this.ration.key = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
                    }
                    this.adView = new CaseeAdView(this.activity, (AttributeSet) null, 0, this.ration.key, this.ration.testmodel, 800000, bgColor, fgColor, false);
                    this.adView.setListener(this);
                } catch (IllegalArgumentException e) {
                    adMogoLayout.rollover();
                }
            }
        }
    }

    public void onFailedToReceiveAd(CaseeAdView adView2) {
        Log.d(AdMogoUtil.ADMOGO, "Casee Failed To Receive Ad");
        adView2.setListener((CaseeAdView.AdListener) null);
        if (!this.activity.isFinishing()) {
            adView2.setListener((CaseeAdView.AdListener) null);
            AdMogoLayout adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get();
            if (adMogoLayout != null) {
                adMogoLayout.rollover();
            }
        }
    }

    public void onFailedToReceiveRefreshAd(CaseeAdView AdView) {
        Log.d(AdMogoUtil.ADMOGO, "Casee Failed To Receive Refresh Ad");
    }

    public void onReceiveAd(CaseeAdView adView2) {
        AdMogoLayout adMogoLayout;
        Log.d(AdMogoUtil.ADMOGO, "Casee success");
        adView2.setListener((CaseeAdView.AdListener) null);
        if (!this.activity.isFinishing() && (adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get()) != null) {
            adMogoLayout.adMogoManager.resetRollover();
            adMogoLayout.handler.post(new AdMogoLayout.ViewAdRunnable(adMogoLayout, adView2, 25));
            adMogoLayout.rotateThreadedDelayed();
        }
    }

    public void onReceiveRefreshAd(CaseeAdView adView2) {
        Log.d(AdMogoUtil.ADMOGO, "Casee Receive Refresh Ad");
    }

    public void finish() {
        Log.d(AdMogoUtil.ADMOGO, "Casee Finished");
    }
}
