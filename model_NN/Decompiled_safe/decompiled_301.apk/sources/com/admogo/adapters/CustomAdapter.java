package com.admogo.adapters;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.GradientDrawable;
import android.util.Log;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.admogo.AdMogoLayout;
import com.admogo.obj.Ration;
import com.admogo.util.AdMogoUtil;
import com.madhouse.android.ads.AdView;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.TimeUnit;

public class CustomAdapter extends AdMogoAdapter {
    public CustomAdapter(AdMogoLayout adMogoLayout, Ration ration) {
        super(adMogoLayout, ration);
    }

    public void handle() {
        AdMogoLayout adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get();
        if (adMogoLayout != null) {
            adMogoLayout.scheduler.schedule(new FetchCustomRunnable(this), 0, TimeUnit.SECONDS);
        }
    }

    public void displayCustom() {
        Activity activity;
        AdMogoLayout adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get();
        if (adMogoLayout != null && (activity = adMogoLayout.activityReference.get()) != null) {
            switch (adMogoLayout.custom.type) {
                case 1:
                    Log.d(AdMogoUtil.ADMOGO, "Serving custom type: banner");
                    RelativeLayout bannerView = new RelativeLayout(activity);
                    if (adMogoLayout.custom.image != null) {
                        ImageView bannerImageView = new ImageView(activity);
                        bannerImageView.setImageDrawable(adMogoLayout.custom.image);
                        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
                        layoutParams.addRule(13);
                        bannerView.addView(bannerImageView, layoutParams);
                        adMogoLayout.pushSubView(bannerView, 9);
                        break;
                    } else {
                        adMogoLayout.rotateThreadedNow();
                        return;
                    }
                case 2:
                    Log.d(AdMogoUtil.ADMOGO, "Serving custom type: icon");
                    RelativeLayout relativeLayout = new RelativeLayout(activity);
                    if (adMogoLayout.custom.image != null) {
                        double density = AdMogoUtil.getDensity(activity);
                        double px320 = (double) AdMogoUtil.convertToScreenPixels((int) AdView.AD_MEASURE_320, density);
                        double px50 = (double) AdMogoUtil.convertToScreenPixels(50, density);
                        double px4 = (double) AdMogoUtil.convertToScreenPixels(4, density);
                        double px6 = (double) AdMogoUtil.convertToScreenPixels(6, density);
                        relativeLayout.setLayoutParams(new FrameLayout.LayoutParams((int) px320, (int) px50));
                        ImageView blendView = new ImageView(activity);
                        int backgroundColor = Color.rgb(adMogoLayout.extra.bgRed, adMogoLayout.extra.bgGreen, adMogoLayout.extra.bgBlue);
                        blendView.setBackgroundDrawable(new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-1, backgroundColor, backgroundColor, backgroundColor}));
                        relativeLayout.addView(blendView, new RelativeLayout.LayoutParams(-1, -1));
                        ImageView imageView = new ImageView(activity);
                        imageView.setImageDrawable(adMogoLayout.custom.image);
                        imageView.setId(10);
                        imageView.setPadding((int) px4, 0, (int) px6, 0);
                        imageView.setScaleType(ImageView.ScaleType.CENTER);
                        relativeLayout.addView(imageView, new RelativeLayout.LayoutParams(-2, -1));
                        ImageView imageView2 = new ImageView(activity);
                        InputStream drawableStream = getClass().getResourceAsStream("/com/admogo/assets/ad_frame.gif");
                        new BitmapDrawable(drawableStream);
                        imageView2.setImageDrawable(null);
                        imageView2.setPadding((int) px4, 0, (int) px6, 0);
                        imageView2.setScaleType(ImageView.ScaleType.CENTER);
                        relativeLayout.addView(imageView2, new RelativeLayout.LayoutParams(-2, -1));
                        TextView textView = new TextView(activity);
                        try {
                            if (adMogoLayout.custom.description == null) {
                            }
                        } catch (Exception e) {
                            adMogoLayout.custom.description = "Haven't description!";
                        }
                        textView.setText(adMogoLayout.custom.description);
                        textView.setTypeface(Typeface.DEFAULT_BOLD, 1);
                        textView.setTextColor(Color.rgb(adMogoLayout.extra.fgRed, adMogoLayout.extra.fgGreen, adMogoLayout.extra.fgBlue));
                        RelativeLayout.LayoutParams textViewParams = new RelativeLayout.LayoutParams(-1, -1);
                        textViewParams.addRule(1, imageView.getId());
                        textViewParams.addRule(10);
                        textViewParams.addRule(12);
                        textViewParams.addRule(15);
                        textViewParams.addRule(13);
                        textView.setGravity(16);
                        relativeLayout.addView(textView, textViewParams);
                        adMogoLayout.pushSubView(relativeLayout, 9);
                        try {
                            drawableStream.close();
                            break;
                        } catch (IOException e2) {
                            Log.e(AdMogoUtil.ADMOGO, e2.toString());
                            break;
                        }
                    } else {
                        adMogoLayout.rotateThreadedNow();
                        return;
                    }
                default:
                    Log.w(AdMogoUtil.ADMOGO, "Unknown custom type!");
                    adMogoLayout.rotateThreadedNow();
                    return;
            }
            adMogoLayout.custom.image = null;
            adMogoLayout.adMogoManager.resetRollover();
            adMogoLayout.rotateThreadedDelayed();
        }
    }

    private static class FetchCustomRunnable implements Runnable {
        private CustomAdapter customAdapter;

        public FetchCustomRunnable(CustomAdapter customAdapter2) {
            this.customAdapter = customAdapter2;
        }

        public void run() {
            AdMogoLayout adMogoLayout = (AdMogoLayout) this.customAdapter.adMogoLayoutReference.get();
            if (adMogoLayout != null) {
                adMogoLayout.custom = adMogoLayout.adMogoManager.getCustom(this.customAdapter.ration.nid);
                if (adMogoLayout.custom == null) {
                    adMogoLayout.rotateThreadedNow();
                } else {
                    adMogoLayout.handler.post(new DisplayCustomRunnable(this.customAdapter));
                }
            }
        }
    }

    private static class DisplayCustomRunnable implements Runnable {
        private CustomAdapter customAdapter;

        public DisplayCustomRunnable(CustomAdapter customAdapter2) {
            this.customAdapter = customAdapter2;
        }

        public void run() {
            this.customAdapter.displayCustom();
        }
    }

    public void finish() {
        Log.d(AdMogoUtil.ADMOGO, "Custom Finished");
    }
}
