package net.youmi.android;

import android.app.Activity;

class y {
    y() {
    }

    static void a(Activity activity, String str) {
        if (str != null) {
            try {
                String trim = str.trim();
                if (trim.length() > 0 && trim.indexOf("sms:") == 0 && trim.length() > "sms:".length()) {
                    String[] split = trim.substring("sms:".length()).split("&");
                    if (split.length > 0) {
                        bd.a(activity, split[0], split.length > 1 ? split[1] : "");
                    }
                }
            } catch (Exception e) {
            }
        }
    }

    static boolean b(Activity activity, String str) {
        if (str == null) {
            return false;
        }
        try {
            String trim = str.trim();
            if (trim.indexOf("mailto:") == 0 && trim.length() > "mailto:".length()) {
                b bVar = new b(trim.substring("mailto:".length()).split("&"));
                String str2 = (String) bVar.a(0, null);
                String str3 = (String) bVar.a(1, null);
                String str4 = (String) bVar.a(2, null);
                String str5 = (String) bVar.a(3, null);
                String str6 = (String) bVar.a(4, null);
                String str7 = (String) bVar.a(5, "0");
                if (str2 == null) {
                    return false;
                }
                String[] a = j.a(str2, ",");
                String[] a2 = j.a(str3, ",");
                String[] a3 = j.a(str4, ",");
                if (str5 != null) {
                    str5 = ao.a(str5);
                }
                if (str6 != null) {
                    str6 = ao.a(str6);
                }
                if (str7.equals("0")) {
                    bd.a(activity, a, a2, a3, str5, str6);
                } else {
                    bd.b(activity, a, a2, a3, str5, str6);
                }
                return true;
            }
        } catch (Exception e) {
        }
        return false;
    }

    static boolean c(Activity activity, String str) {
        if (str == null) {
            return false;
        }
        try {
            String trim = str.trim();
            if (trim.indexOf("loc:") == 0 && trim.length() > "loc:".length()) {
                b bVar = new b(j.a(trim.substring("loc:".length()), "&"));
                String str2 = (String) bVar.a(0, null);
                String str3 = (String) bVar.a(1, null);
                String str4 = (String) bVar.a(2, "A");
                String str5 = (String) bVar.a(3, "");
                if (str2 == null) {
                    return false;
                }
                if (str3 == null) {
                    return false;
                }
                bd.a(activity, str3, str2, str4, str5);
            }
        } catch (Exception e) {
        }
        return false;
    }
}
