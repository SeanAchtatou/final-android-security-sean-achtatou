package net.youmi.android;

import android.content.Context;
import android.graphics.Bitmap;
import android.widget.ImageView;

class bw extends ImageView {
    private cq a;
    private ei b;
    private Bitmap c;
    private boolean d = false;

    public bw(Context context, int i, int i2, int i3) {
        super(context);
        setScaleType(ImageView.ScaleType.FIT_XY);
    }

    private void b() {
        try {
            if (this.b != null) {
                c();
            }
            if (this.a != null) {
                this.b = new ei(this);
                this.b.execute(this.a);
            }
        } catch (Exception e) {
        }
    }

    private void c() {
        try {
            if (this.b != null) {
                this.b.a();
            }
            this.b = null;
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.d = false;
        c();
    }

    /* access modifiers changed from: package-private */
    public void a(Bitmap bitmap) {
        if (bitmap != null) {
            try {
                if (!bitmap.isRecycled()) {
                    Bitmap bitmap2 = this.c;
                    this.c = bitmap;
                    setImageBitmap(bitmap);
                    if (bitmap2 != this.c) {
                        bitmap2.recycle();
                    }
                }
            } catch (Exception e) {
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(cq cqVar) {
        if (cqVar != null) {
            try {
                if (cqVar.c() > 0) {
                    this.a = cqVar;
                    this.d = true;
                    b();
                }
            } catch (Exception e) {
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        try {
            c();
        } catch (Exception e) {
        }
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        if (z) {
            try {
                if (this.d) {
                    b();
                }
            } catch (Exception e) {
            }
        } else {
            c();
        }
    }
}
