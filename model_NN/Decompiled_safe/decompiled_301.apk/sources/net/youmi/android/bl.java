package net.youmi.android;

import android.app.Activity;
import android.view.animation.Animation;
import android.widget.RelativeLayout;

class bl extends RelativeLayout implements ba {
    final /* synthetic */ cw a;
    private av b = new av(this.c, null);
    private Activity c;
    private AdView d;
    private ek e;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bl(cw cwVar, Activity activity, AdView adView) {
        super(activity);
        this.a = cwVar;
        this.c = activity;
        this.d = adView;
        RelativeLayout.LayoutParams a2 = h.a(this.d.getAdWidth(), this.d.getAdHeight());
        this.b.setClickable(false);
        this.b.setScrollContainer(false);
        addView(this.b, a2);
    }

    public void a() {
        setVisibility(8);
    }

    public void a(Animation animation) {
    }

    public boolean a(ct ctVar) {
        if (ctVar != null) {
            try {
                ek j = ctVar.j();
                if (j != null) {
                    this.e = j;
                    return true;
                }
            } catch (Exception e2) {
                f.a(e2);
            }
        }
        return false;
    }

    public void b() {
        this.b.a(this.e);
        setVisibility(0);
    }

    public void c() {
        setVisibility(0);
    }

    public void d() {
        this.b.a(this.e);
        setVisibility(0);
    }
}
