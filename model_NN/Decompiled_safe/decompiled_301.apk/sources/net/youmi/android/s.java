package net.youmi.android;

import android.content.Intent;
import android.net.Uri;
import android.view.View;
import java.io.File;

class s implements View.OnClickListener {
    final /* synthetic */ ce a;

    s(ce ceVar) {
        this.a = ceVar;
    }

    public void onClick(View view) {
        try {
            if (this.a.o != null) {
                File file = new File(this.a.o.a);
                if (file.exists()) {
                    Intent intent = new Intent("android.intent.action.SEND");
                    intent.putExtra("android.intent.extra.STREAM", Uri.fromFile(file));
                    if (this.a.o.d != null) {
                        intent.setType(this.a.o.d);
                    } else {
                        intent.setType("image/jpeg");
                    }
                    if (this.a.f != null) {
                        intent.putExtra("android.intent.extra.SUBJECT", this.a.f);
                        if (this.a.g != null) {
                            intent.putExtra("android.intent.extra.TEXT", String.valueOf(this.a.f) + " " + this.a.g);
                        } else {
                            intent.putExtra("android.intent.extra.TEXT", this.a.f);
                        }
                    }
                    this.a.a.startActivity(Intent.createChooser(intent, "分享图片"));
                }
            }
        } catch (Exception e) {
        }
    }
}
