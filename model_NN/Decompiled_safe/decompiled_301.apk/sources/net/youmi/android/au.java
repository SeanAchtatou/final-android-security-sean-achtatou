package net.youmi.android;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

class au {
    au() {
    }

    static ct a(Context context, String str, long j, AdView adView) {
        String a;
        String a2;
        JSONObject a3;
        String str2;
        String str3;
        ArrayList arrayList;
        ArrayList arrayList2;
        try {
            JSONObject jSONObject = new JSONObject(str);
            int a4 = cx.a(jSONObject, "c", -999);
            if (a4 >= 0) {
                String a5 = cx.a(jSONObject, "rsd", (String) null);
                if (a5 != null) {
                    bj.a(cx.a(jSONObject, "rt", 30));
                    String a6 = cx.a(jSONObject, "cc", (String) null);
                    JSONObject a7 = cx.a(jSONObject, "d", (JSONObject) null);
                    if (!(a7 == null || (a = cx.a(a7, "id", (String) null)) == null)) {
                        int a8 = cx.a(a7, "t", -1);
                        if (a8 > -1) {
                            int a9 = cx.a(a7, "ot", -1);
                            if (!(a9 <= -1 || (a2 = cx.a(a7, "e", (String) null)) == null || (a3 = cx.a(a7, "src", (JSONObject) null)) == null)) {
                                String a10 = cx.a(a3, "iu", (String) null);
                                String a11 = cx.a(a3, "mu", (String) null);
                                String a12 = cx.a(a3, "cu", (String) null);
                                String a13 = cx.a(a3, "st", (String) null);
                                String a14 = cx.a(a3, "su", (String) null);
                                JSONObject a15 = cx.a(a3, "text", (JSONObject) null);
                                if (a15 != null) {
                                    String a16 = cx.a(a15, "tt", (String) null);
                                    str2 = cx.a(a15, "st", (String) null);
                                    str3 = a16;
                                } else {
                                    str2 = null;
                                    str3 = null;
                                }
                                JSONObject a17 = cx.a(a7, "dest", (JSONObject) null);
                                if (a17 != null) {
                                    String a18 = cx.a(a17, "js", (String) null);
                                    if (a18 != null) {
                                        a18 = a18.trim();
                                        if (a18.length() > 0) {
                                            aj.a(a18);
                                        }
                                    }
                                    String str4 = a18;
                                    String a19 = cx.a(a17, "tu", (String) null);
                                    if (a19 != null) {
                                        JSONArray a20 = cx.a(a17, "imgs", (JSONArray) null);
                                        if (a20 != null) {
                                            if (a20.length() > 0) {
                                                ArrayList arrayList3 = null;
                                                int i = 0;
                                                while (i < a20.length()) {
                                                    try {
                                                        if (arrayList3 == null) {
                                                            arrayList3 = new ArrayList(a20.length());
                                                        }
                                                        arrayList3.add(a20.getString(i));
                                                        i++;
                                                    } catch (Exception e) {
                                                        arrayList2 = arrayList3;
                                                    }
                                                }
                                                arrayList2 = arrayList3;
                                            } else {
                                                arrayList2 = null;
                                            }
                                            arrayList = arrayList2;
                                        } else {
                                            arrayList = null;
                                        }
                                        return ct.a(context, aj.a() != null, a, a5, a2, a8, a9, a6, str3, str2, a10, a11, a12, a13, a14, a19, arrayList, cx.a(a17, "ic", 0) > 0, cx.a(a3, "il", 1) != 0, str4, j);
                                    }
                                }
                            }
                        }
                    }
                }
                return null;
            }
            if (a4 == -999) {
                f.b("Unable to connect to the server, please check your network configuration!");
            } else {
                f.b("Request Ad Error Code : " + a4);
            }
            if (adView != null) {
                adView.h();
            }
            return null;
        } catch (Exception e2) {
        }
    }

    static ct a(Context context, AdView adView) {
        int i;
        try {
            n.a();
            ef.e();
            long currentTimeMillis = System.currentTimeMillis() / 1000;
            StringBuffer stringBuffer = new StringBuffer(512);
            String a = cp.a(4);
            stringBuffer.append(x.a());
            stringBuffer.append("00000");
            stringBuffer.append(bp.b());
            stringBuffer.append(eo.b());
            stringBuffer.append(a);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(512);
            ak.a(eo.a(context), 2, byteArrayOutputStream);
            int i2 = bu.a(context) ? 1 | 2 : 0;
            try {
                String a2 = r.a(context);
                i = (a2.equals("3gnet") || a2.equals("3gwap") || a2.equals("wifi")) ? 8 | i2 : i2;
            } catch (Exception e) {
                i = i2;
            }
            try {
                if (eg.a(context) != null) {
                    i |= 4;
                }
            } catch (Exception e2) {
            }
            ak.a(i | 16, 2, byteArrayOutputStream);
            ak.a(adView.b().a().a(), 2, byteArrayOutputStream);
            ak.a(adView.b().a().b().a(), 2, byteArrayOutputStream);
            ak.a(eo.g(), 2, byteArrayOutputStream);
            ak.a(bp.a(), 1, byteArrayOutputStream);
            ak.a(ef.f(), 2, byteArrayOutputStream);
            ak.a(currentTimeMillis, 4, byteArrayOutputStream);
            ak.a(bp.h(), 2, byteArrayOutputStream);
            ak.a(bp.e(), 2, byteArrayOutputStream);
            ak.a(ef.e(context) ? 1 : 0, 1, byteArrayOutputStream);
            ak.a(bp.c(), 1, byteArrayOutputStream);
            ak.a(r.a(context), byteArrayOutputStream);
            byteArrayOutputStream.write(38);
            ej a3 = ef.a(context);
            ak.a(a3.c(), byteArrayOutputStream);
            byteArrayOutputStream.write(38);
            ak.a(a3.b(), byteArrayOutputStream);
            byteArrayOutputStream.write(38);
            ak.a(a3.a(), byteArrayOutputStream);
            byteArrayOutputStream.write(38);
            ak.a(ef.d(context), byteArrayOutputStream);
            byteArrayOutputStream.write(38);
            ak.a(ef.b(), byteArrayOutputStream);
            byteArrayOutputStream.write(38);
            ak.a(ef.d(), byteArrayOutputStream);
            byteArrayOutputStream.write(38);
            ak.a(a3.e(), byteArrayOutputStream);
            byteArrayOutputStream.write(38);
            ak.a("", byteArrayOutputStream);
            byteArrayOutputStream.write(38);
            ak.a(context.getPackageName(), byteArrayOutputStream);
            byteArrayOutputStream.write(38);
            ak.a(context.getApplicationInfo().name, byteArrayOutputStream);
            byteArrayOutputStream.write(38);
            ak.a(ef.c(context), byteArrayOutputStream);
            byteArrayOutputStream.write(38);
            ak.a(ef.c(), byteArrayOutputStream);
            byteArrayOutputStream.write(38);
            ak.a("", byteArrayOutputStream);
            byteArrayOutputStream.write(38);
            ak.a(a3.d(), byteArrayOutputStream);
            byteArrayOutputStream.write(38);
            ak.a(ef.f(context), byteArrayOutputStream);
            byteArrayOutputStream.write(38);
            byteArrayOutputStream.write(38);
            byteArrayOutputStream.write(38);
            byteArrayOutputStream.write(38);
            byteArrayOutputStream.write(38);
            byteArrayOutputStream.write(38);
            byteArrayOutputStream.write(38);
            byteArrayOutputStream.write(38);
            byteArrayOutputStream.write(38);
            try {
                stringBuffer.append(cp.a(byteArrayOutputStream.toByteArray(), cp.b(String.valueOf(bp.d()) + eo.d() + a)));
            } catch (Exception e3) {
            }
            n.a("req ps");
            String stringBuffer2 = stringBuffer.toString();
            bk bkVar = new bk();
            if (bkVar.a(context, stringBuffer2) == 6) {
                n.a("req net");
                return a(context, bkVar.c(), currentTimeMillis, adView);
            }
            f.b("Unable to connect to the server, please check your network configuration!");
            adView.h();
            return null;
        } catch (Exception e4) {
            f.a(e4);
        }
    }

    static dk a(Context context) {
        JSONObject a;
        int a2;
        String a3;
        String a4;
        try {
            StringBuffer stringBuffer = new StringBuffer(512);
            String a5 = cp.a(4);
            stringBuffer.append(x.e());
            stringBuffer.append("00000");
            stringBuffer.append(bp.b());
            stringBuffer.append(eo.b());
            stringBuffer.append(a5);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(512);
            ak.a(ef.a(context).a(), byteArrayOutputStream);
            byteArrayOutputStream.write(38);
            a(byteArrayOutputStream);
            try {
                stringBuffer.append(cp.a(byteArrayOutputStream.toByteArray(), cp.b(String.valueOf(bp.d()) + eo.d() + a5)));
            } catch (Exception e) {
            }
            String stringBuffer2 = stringBuffer.toString();
            bk bkVar = new bk();
            if (bkVar.a(context, stringBuffer2) == 6 && (a = cx.a(bkVar.c())) != null && cx.a(a, "c", -999) == 0 && cx.a(a, "n", 0) == 1 && (a2 = cx.a(a, "v", -1)) > 0 && (a3 = cx.a(a, "pn", (String) null)) != null) {
                String trim = a3.trim();
                if (trim.length() > 0 && trim.equals(context.getPackageName()) && a2 > context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode && (a4 = cx.a(a, "u", (String) null)) != null) {
                    String trim2 = a4.trim();
                    if (trim2.length() > 0) {
                        String a6 = cx.a(a, "vn", (String) null);
                        String a7 = cx.a(a, "m", (String) null);
                        dk dkVar = new dk();
                        dkVar.d = trim2;
                        dkVar.c = trim;
                        dkVar.a = a2;
                        dkVar.b = a6;
                        dkVar.e = a7;
                        return dkVar;
                    }
                }
            }
        } catch (Exception e2) {
        }
        return null;
    }

    static void a(Activity activity, AdView adView, ct ctVar) {
        JSONObject a;
        if (ctVar != null && !ctVar.q() && !ef.e(activity) && ctVar.l() != null) {
            try {
                ds dsVar = an.a;
                if (!dsVar.b() || !ctVar.l().equals("00000000")) {
                    StringBuilder a2 = j.a(ctVar.l(), 32);
                    if (a2 != null) {
                        long currentTimeMillis = System.currentTimeMillis() / 1000;
                        StringBuffer stringBuffer = new StringBuffer(512);
                        String a3 = cp.a(4);
                        stringBuffer.append(x.d());
                        stringBuffer.append("00000");
                        stringBuffer.append(bp.b());
                        stringBuffer.append(eo.b());
                        stringBuffer.append(a3);
                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(512);
                        if (a2.charAt(31) == '1') {
                            ak.a(eo.a(activity), 2, byteArrayOutputStream);
                        }
                        if (a2.charAt(30) == '1') {
                            ak.a(eo.g(), 1, byteArrayOutputStream);
                        }
                        if (!dsVar.b()) {
                            if (a2.charAt(29) == '0') {
                                a2 = a2.replace(29, 30, "1");
                            }
                            if (a2.charAt(28) == '0') {
                                a2 = a2.replace(28, 29, "1");
                            }
                            dsVar.a();
                        }
                        int i = dsVar.a;
                        int i2 = dsVar.b;
                        if (a2.charAt(29) == '1') {
                            ak.a(i, 4, byteArrayOutputStream);
                        }
                        if (a2.charAt(28) == '1') {
                            ak.a(i2 - i, 2, byteArrayOutputStream);
                        }
                        if (a2.charAt(27) == '1') {
                            ak.a(currentTimeMillis, 4, byteArrayOutputStream);
                        }
                        if (a2.charAt(26) == '1') {
                            ak.a(adView.b().h(), 2, byteArrayOutputStream);
                        }
                        if (a2.charAt(25) == '1') {
                            ak.a(bp.e(), 2, byteArrayOutputStream);
                        }
                        if (a2.charAt(24) == '1') {
                            ak.a(adView.b().g(), 2, byteArrayOutputStream);
                        }
                        if (a2.charAt(23) == '1') {
                            ak.a(bp.c(), 1, byteArrayOutputStream);
                        }
                        boolean z = false;
                        if (a2.charAt(22) == '1') {
                            ak.a(r.a(activity), byteArrayOutputStream);
                            z = true;
                        }
                        ej a4 = ef.a(activity);
                        if (a2.charAt(21) == '1') {
                            if (z) {
                                byteArrayOutputStream.write(38);
                            } else {
                                z = true;
                            }
                            ak.a(a4.c(), byteArrayOutputStream);
                        }
                        if (a2.charAt(20) == '1') {
                            if (z) {
                                byteArrayOutputStream.write(38);
                            } else {
                                z = true;
                            }
                            ak.a(a4.a(), byteArrayOutputStream);
                        }
                        if (a2.charAt(19) == '1') {
                            if (z) {
                                byteArrayOutputStream.write(38);
                            } else {
                                z = true;
                            }
                            ak.a(ef.d(activity), byteArrayOutputStream);
                        }
                        if (a2.charAt(18) == '1') {
                            if (z) {
                                byteArrayOutputStream.write(38);
                            } else {
                                z = true;
                            }
                            ak.a(ef.b(), byteArrayOutputStream);
                        }
                        if (a2.charAt(17) == '1') {
                            if (z) {
                                byteArrayOutputStream.write(38);
                            } else {
                                z = true;
                            }
                            ak.a(ef.d(), byteArrayOutputStream);
                        }
                        if (a2.charAt(16) == '1') {
                            if (z) {
                                byteArrayOutputStream.write(38);
                            } else {
                                z = true;
                            }
                            ak.a(a4.e(), byteArrayOutputStream);
                        }
                        Location a5 = eg.a(activity);
                        if (a2.charAt(15) == '1') {
                            if (z) {
                                byteArrayOutputStream.write(38);
                            } else {
                                z = true;
                            }
                            if (a5 != null) {
                                ak.a(new StringBuilder(String.valueOf(a5.getLatitude())).toString(), byteArrayOutputStream);
                            }
                        }
                        if (a2.charAt(14) == '1') {
                            if (z) {
                                byteArrayOutputStream.write(38);
                            } else {
                                z = true;
                            }
                            if (a5 != null) {
                                ak.a(new StringBuilder(String.valueOf(a5.getLongitude())).toString(), byteArrayOutputStream);
                            }
                        }
                        if (a2.charAt(13) == '1') {
                            if (z) {
                                byteArrayOutputStream.write(38);
                            } else {
                                z = true;
                            }
                            ak.a(ef.c(), byteArrayOutputStream);
                        }
                        if (a2.charAt(12) == '1') {
                            if (z) {
                                byteArrayOutputStream.write(38);
                            } else {
                                z = true;
                            }
                        }
                        if (a2.charAt(11) == '1') {
                            if (z) {
                                byteArrayOutputStream.write(38);
                            } else {
                                z = true;
                            }
                            ak.a(a4.d(), byteArrayOutputStream);
                        }
                        if (a2.charAt(10) == '1') {
                            if (z) {
                                byteArrayOutputStream.write(38);
                            } else {
                                z = true;
                            }
                            ak.a("21", byteArrayOutputStream);
                        }
                        if (a2.charAt(9) == '1') {
                            if (z) {
                                byteArrayOutputStream.write(38);
                            } else {
                                z = true;
                            }
                            ak.a("22", byteArrayOutputStream);
                        }
                        if (a2.charAt(8) == '1') {
                            if (z) {
                                byteArrayOutputStream.write(38);
                            } else {
                                z = true;
                            }
                            ak.a("23", byteArrayOutputStream);
                        }
                        if (a2.charAt(7) == '1') {
                            if (z) {
                                byteArrayOutputStream.write(38);
                            } else {
                                z = true;
                            }
                            ak.a("24", byteArrayOutputStream);
                        }
                        if (a2.charAt(6) == '1') {
                            if (z) {
                                byteArrayOutputStream.write(38);
                            } else {
                                z = true;
                            }
                            ak.a("25", byteArrayOutputStream);
                        }
                        if (a2.charAt(5) == '1') {
                            if (z) {
                                byteArrayOutputStream.write(38);
                            } else {
                                z = true;
                            }
                            ak.a("26", byteArrayOutputStream);
                        }
                        if (a2.charAt(4) == '1') {
                            if (z) {
                                byteArrayOutputStream.write(38);
                            } else {
                                z = true;
                            }
                            ak.a("27", byteArrayOutputStream);
                        }
                        if (a2.charAt(3) == '1') {
                            if (z) {
                                byteArrayOutputStream.write(38);
                            } else {
                                z = true;
                            }
                            ak.a("28", byteArrayOutputStream);
                        }
                        if (a2.charAt(2) == '1') {
                            if (z) {
                                byteArrayOutputStream.write(38);
                            } else {
                                z = true;
                            }
                            ak.a("29", byteArrayOutputStream);
                        }
                        if (a2.charAt(1) == '1') {
                            if (z) {
                                byteArrayOutputStream.write(38);
                            } else {
                                z = true;
                            }
                            ak.a("30", byteArrayOutputStream);
                        }
                        if (a2.charAt(0) == '1') {
                            if (z) {
                                byteArrayOutputStream.write(38);
                            }
                            ak.a("31", byteArrayOutputStream);
                        }
                        try {
                            stringBuffer.append(cp.a(byteArrayOutputStream.toByteArray(), cp.b(String.valueOf(bp.d()) + eo.d() + a3)));
                        } catch (Exception e) {
                        }
                        stringBuffer.append(',');
                        stringBuffer.append(j.a(a2, 8).toUpperCase());
                        String stringBuffer2 = stringBuffer.toString();
                        bk bkVar = new bk();
                        if (bkVar.a(activity, stringBuffer2) == 6 && (a = cx.a(bkVar.c())) != null) {
                            cx.a(a, "c", -999);
                        }
                    }
                    ctVar.r();
                }
            } catch (Exception e2) {
            }
        }
    }

    static void a(Activity activity, ct ctVar) {
        if (ctVar != null) {
            try {
                if (!ctVar.t()) {
                    long B = ctVar.B();
                    StringBuffer stringBuffer = new StringBuffer(512);
                    stringBuffer.append(x.b());
                    stringBuffer.append("00000");
                    stringBuffer.append(bp.b());
                    stringBuffer.append(eo.b());
                    stringBuffer.append(ctVar.o());
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(512);
                    ak.a(bp.a(), 1, byteArrayOutputStream);
                    ak.a(B, 4, byteArrayOutputStream);
                    ak.a(bp.c(), 1, byteArrayOutputStream);
                    a(byteArrayOutputStream);
                    try {
                        stringBuffer.append(cp.a(byteArrayOutputStream.toByteArray(), cp.b(String.valueOf(bp.d()) + eo.d() + ctVar.o())));
                    } catch (Exception e) {
                    }
                    stringBuffer.append(',');
                    stringBuffer.append(ctVar.k());
                    if (new bk().a(activity, stringBuffer.toString()) == 6) {
                    }
                    ctVar.w();
                }
            } catch (Exception e2) {
            }
        }
    }

    static void a(Context context, int i, int i2, int i3, int i4, int i5, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8) {
        try {
            StringBuffer stringBuffer = new StringBuffer(512);
            String a = cp.a(4);
            stringBuffer.append("00000");
            stringBuffer.append(bp.b());
            stringBuffer.append(eo.b());
            stringBuffer.append(a);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(512);
            ak.a(i, 1, byteArrayOutputStream);
            ak.a(i2, 1, byteArrayOutputStream);
            ak.a(i3, 4, byteArrayOutputStream);
            ak.a(i4, 4, byteArrayOutputStream);
            ak.a(i5, 4, byteArrayOutputStream);
            ak.a(ef.a(context).a(), byteArrayOutputStream);
            byteArrayOutputStream.write(38);
            ak.a(str != null ? str : "", byteArrayOutputStream);
            byteArrayOutputStream.write(38);
            ak.a(str2 != null ? str2 : "", byteArrayOutputStream);
            byteArrayOutputStream.write(38);
            ak.a(str3 != null ? str3 : "", byteArrayOutputStream);
            byteArrayOutputStream.write(38);
            ak.a(str4 != null ? str4 : "", byteArrayOutputStream);
            byteArrayOutputStream.write(38);
            ak.a(str5 != null ? str5 : "", byteArrayOutputStream);
            byteArrayOutputStream.write(38);
            ak.a(str6 != null ? cp.a(str6) : "", byteArrayOutputStream);
            byteArrayOutputStream.write(38);
            ak.a(str6 != null ? cp.a(str7) : "", byteArrayOutputStream);
            byteArrayOutputStream.write(38);
            ak.a(str8 != null ? str8 : "", byteArrayOutputStream);
            byteArrayOutputStream.write(38);
            a(byteArrayOutputStream);
            try {
                stringBuffer.append(cp.a(byteArrayOutputStream.toByteArray(), cp.b(String.valueOf(bp.d()) + eo.d() + a)));
            } catch (Exception e) {
            }
            String f = x.f();
            ArrayList arrayList = new ArrayList(2);
            arrayList.add(new BasicNameValuePair("s", stringBuffer.toString()));
            ap apVar = new ap(context, f);
            if (apVar.a(arrayList) == 3) {
                f.c("di:" + apVar.a());
            }
        } catch (Exception e2) {
            f.a(e2);
        }
    }

    static void a(Context context, long j, String str, String str2, long j2, long j3, long j4) {
        try {
            StringBuffer stringBuffer = new StringBuffer(512);
            stringBuffer.append(x.c());
            stringBuffer.append("00000");
            stringBuffer.append(bp.b());
            stringBuffer.append(eo.b());
            stringBuffer.append(str);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(512);
            ak.a(j2, 4, byteArrayOutputStream);
            ak.a(j3, 4, byteArrayOutputStream);
            ak.a(j4, 4, byteArrayOutputStream);
            ak.a(bp.a(), 1, byteArrayOutputStream);
            ak.a(j, 4, byteArrayOutputStream);
            ak.a(bp.c(), 1, byteArrayOutputStream);
            a(byteArrayOutputStream);
            try {
                stringBuffer.append(cp.a(byteArrayOutputStream.toByteArray(), cp.b(String.valueOf(bp.d()) + eo.d() + str)));
            } catch (Exception e) {
            }
            stringBuffer.append(',');
            stringBuffer.append(str2);
            if (new bk().a(context, stringBuffer.toString()) == 6) {
            }
        } catch (Exception e2) {
        }
    }

    static void a(ByteArrayOutputStream byteArrayOutputStream) {
        byteArrayOutputStream.write(38);
        byteArrayOutputStream.write(38);
        byteArrayOutputStream.write(38);
        byteArrayOutputStream.write(38);
        byteArrayOutputStream.write(38);
        byteArrayOutputStream.write(38);
        byteArrayOutputStream.write(38);
        byteArrayOutputStream.write(38);
        byteArrayOutputStream.write(38);
    }
}
