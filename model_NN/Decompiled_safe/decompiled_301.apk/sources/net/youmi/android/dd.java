package net.youmi.android;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.PathShape;
import android.view.View;
import com.madhouse.android.ads.AdView;

class dd extends View {
    float A = 1.0f;
    float B = 0.0f;
    int C = 0;
    Paint D = new Paint();
    Path E;
    private int F = 0;
    float a;
    int b;
    float c = 7.0f;
    int d = 0;
    int e = 0;
    int f;
    int g;
    float h = 1.0f;
    float i = 12.0f;
    int j = Color.rgb(135, 206, 235);
    int k = Color.rgb((int) AdView.AD_MEASURE_240, 248, 255);
    int l = Color.rgb(65, 105, 225);
    int m = Color.argb((int) AdView.RETRUNCODE_OK, 0, 0, 0);
    LinearGradient n;
    LinearGradient o;
    float p = 3.5f;
    float q = 3.5f;
    float r = 1.5f;
    float s = 1.0f;
    ShapeDrawable t;
    Rect u = new Rect();
    RectF v = new RectF();
    RectF w = new RectF();
    int x = 0;
    int y = 0;
    float z = 1.0f;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, int):void}
     arg types: [float, int, int, int]
     candidates:
      ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, long):void}
      ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, int):void} */
    public dd(Context context, int i2, int i3, bz bzVar) {
        super(context);
        this.a = (float) i2;
        this.b = i3;
        if (!bzVar.c()) {
            switch (bzVar.e()) {
                case 120:
                    this.i = 10.0f;
                    break;
                case 160:
                    this.i = 12.0f;
                    break;
                case AdView.AD_MEASURE_240:
                    this.i = 17.0f;
                    break;
                case AdView.AD_MEASURE_320:
                    this.i = 19.0f;
                    break;
                default:
                    this.i = 12.0f;
                    break;
            }
        } else {
            this.i = 12.0f;
        }
        this.D.setTextSize(this.i);
        this.D.setTextAlign(Paint.Align.CENTER);
        Rect rect = new Rect();
        this.D.getTextBounds("179%", 0, 3, rect);
        this.D.setColor(-1);
        this.D.setAntiAlias(true);
        this.h = bzVar.a(this.h);
        this.f = rect.width();
        this.g = rect.height();
        float width = ((float) rect.width()) * 1.6f;
        float height = ((float) rect.height()) * 2.2f;
        float f2 = 1.5f * height;
        this.x = Math.round(width);
        this.y = Math.round(f2);
        this.c = bzVar.a(this.c);
        this.s = this.a / 100.0f;
        this.d = (int) ((((float) this.b) - this.c) - ((float) this.y));
        this.E = new Path();
        this.E.addRoundRect(new RectF(0.0f, 0.0f, width, height), this.p, this.p, Path.Direction.CCW);
        this.E.moveTo(0.25f * width, height);
        float f3 = 0.28f * width;
        this.C = Math.round(f3);
        this.E.lineTo(f3, f2);
        this.E.lineTo(width * 0.55f, height);
        this.E.close();
        this.t = new ShapeDrawable(new PathShape(this.E, (float) this.x, (float) this.y));
        this.v.set(0.0f, ((float) i3) - this.c, (float) i2, (float) i3);
        this.z = bzVar.a(this.z);
        this.A = bzVar.a(this.A);
        this.q = bzVar.a(this.q);
        this.r = bzVar.a(this.r);
        Paint paint = this.t.getPaint();
        paint.reset();
        paint.setColor(this.m);
        paint.setShadowLayer(bzVar.a(2.0f), 0.0f, 0.0f, Color.rgb(245, 245, 245));
        paint.setAntiAlias(true);
    }

    private void a(Canvas canvas) {
        this.t.draw(canvas);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
     arg types: [int, float, int, float, int[], ?[OBJECT, ARRAY], android.graphics.Shader$TileMode]
     candidates:
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void} */
    private void b(Canvas canvas) {
        this.D.reset();
        this.D.setStrokeWidth(this.z);
        if (this.n == null) {
            this.n = new LinearGradient(0.0f, this.v.top, 0.0f, this.v.bottom, new int[]{-16777216, -7829368, -12303292, -16777216}, (float[]) null, Shader.TileMode.CLAMP);
        }
        this.D.setShader(this.n);
        canvas.drawRoundRect(this.v, this.q, this.q, this.D);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
     arg types: [int, float, int, float, int, int, android.graphics.Shader$TileMode]
     candidates:
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void} */
    private void c(Canvas canvas) {
        this.D.reset();
        this.w.set(this.z, (((float) this.b) - this.c) + this.z, this.B - this.z, ((float) this.b) - this.z);
        if (this.o == null) {
            this.o = new LinearGradient(0.0f, this.w.top, 0.0f, this.w.bottom, this.k, this.l, Shader.TileMode.CLAMP);
        }
        this.D.setShader(this.o);
        this.D.setAntiAlias(true);
        canvas.drawRoundRect(this.w, this.r, this.r, this.D);
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.F = 0;
    }

    /* access modifiers changed from: package-private */
    public void a(int i2) {
        if (i2 != this.F && i2 >= 0 && i2 <= 100) {
            this.F = i2;
            if (this.F == 100) {
                setVisibility(4);
            } else if (getVisibility() != 0) {
                setVisibility(0);
            } else {
                invalidate();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        try {
            this.B = this.s * ((float) this.F);
            this.e = Math.round(this.B - ((float) this.C));
            this.u.set(this.e, this.d, this.x + this.e, this.y + this.d);
            this.t.setBounds(this.u);
            a(canvas);
            this.D.reset();
            this.D.setTextAlign(Paint.Align.CENTER);
            this.D.setAntiAlias(true);
            this.D.setColor(this.j);
            this.D.setTextSize(this.i);
            canvas.drawText(new StringBuilder(String.valueOf(this.F)).toString(), (float) this.u.centerX(), (float) this.u.centerY(), this.D);
            b(canvas);
            c(canvas);
        } catch (Exception e2) {
        }
    }
}
