package net.youmi.android;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.Toast;
import com.admogo.util.AdMogoUtil;
import java.io.File;

class fb extends FrameLayout implements dj, dz {
    cw a;
    at b;
    boolean c = false;
    dd d;
    AdView e;
    int f = 255;
    Activity g;
    dw h;
    Animation i = new AlphaAnimation(0.0f, 1.0f);
    Animation j = new AlphaAnimation(1.0f, 0.0f);

    fb(Activity activity, AdView adView, bz bzVar, int i2, int i3, int i4) {
        super(activity);
        this.g = activity;
        this.e = adView;
        this.f = i4;
        int adWidth = this.e.getAdWidth();
        int adHeight = this.e.getAdHeight();
        c(i2);
        el elVar = new el(adWidth, adHeight, bzVar);
        this.b = new at(activity, adView, this, adView.getAdWidth(), adView.getAdHeight(), bzVar, elVar);
        int adWidth2 = this.e.getAdWidth() - elVar.a();
        int b2 = bzVar.a().d().b() + (bzVar.a().d().a() * 2);
        this.a = new cw(activity, adView, this, bzVar, i3, ((this.e.getAdWidth() - (adWidth2 < b2 ? b2 : adWidth2)) - bzVar.a().c().b()) - (bzVar.a().c().a() * 2));
        addView(this.a, new FrameLayout.LayoutParams(adWidth, adHeight));
        addView(this.b, new FrameLayout.LayoutParams(this.e.getAdWidth(), this.e.getAdHeight()));
        this.b.bringToFront();
        this.d = new dd(activity, adWidth, adHeight, bzVar);
        addView(this.d, new FrameLayout.LayoutParams(adWidth, adHeight));
        this.d.bringToFront();
        this.d.setVisibility(8);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-2, -2);
        layoutParams.gravity = 85;
        layoutParams.rightMargin = bzVar.a(bzVar.a().d().a());
        layoutParams.bottomMargin = bzVar.a(bzVar.a().d().a());
        this.i.setDuration(800);
        this.j.setDuration(800);
        setFocusable(true);
        setClickable(true);
        setVisibility(8);
        b();
    }

    public void a() {
        this.e.a().c = false;
        try {
            this.d.setVisibility(8);
            this.d.a();
        } catch (Exception e2) {
        }
        try {
            Toast.makeText(this.g, "无法连接服务器,请稍后重试!", 0).show();
        } catch (Exception e3) {
        }
    }

    public void a(int i2) {
        try {
            this.d.a(i2);
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: package-private */
    public void a(ct ctVar) {
        if (ctVar != null && ctVar.i() != null) {
            dm.a = ctVar;
            AdActivity.a(this.g);
        }
    }

    public void a(du duVar, String[] strArr) {
        try {
            this.e.a().c = false;
            if (strArr == null) {
                e();
                return;
            }
            try {
                new Thread(new bq(this, duVar)).start();
            } catch (Exception e2) {
            }
            try {
                AdActivity.a(this.g, strArr, duVar.e(), duVar.f());
            } catch (Exception e3) {
            }
            try {
                this.d.setVisibility(8);
                this.d.a();
            } catch (Exception e4) {
            }
        } catch (Exception e5) {
        }
    }

    public void a(i iVar) {
        this.e.a().c = false;
        this.e.a().c = false;
        try {
            this.d.setVisibility(8);
            this.d.a();
        } catch (Exception e2) {
        }
        q.c(this.g);
        ax.a(this.g, "开始下载");
        try {
            bv bvVar = new bv();
            bvVar.a = iVar.c();
            bvVar.c = iVar.a();
            bvVar.d = iVar.b();
            k.a(this.g, bvVar, 1);
        } catch (Exception e3) {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: net.youmi.android.bd.b(android.content.Context, java.lang.String):void
     arg types: [android.app.Activity, java.lang.String]
     candidates:
      net.youmi.android.bd.b(android.app.Activity, java.lang.String):void
      net.youmi.android.bd.b(android.content.Context, java.lang.String):void */
    public void a(i iVar, File file, String str) {
        q.d(this.g);
        if (file != null && file.exists()) {
            ax.a(this.g, "下载成功");
            try {
                bv a2 = k.a(this.g, file.getPath(), str, iVar.a(), iVar.b(), iVar.c());
                k.a(this.g, a2, 2);
                k.a(this.g, a2, 3);
            } catch (Exception e2) {
                f.a(e2);
            }
            bd.b((Context) this.g, file.getPath());
        }
    }

    public void a(i iVar, ek ekVar) {
        try {
            this.e.a().c = false;
            if (ekVar == null) {
                e();
                return;
            }
            try {
                ct c2 = this.e.c();
                if (c2 == null) {
                    e();
                    return;
                }
                ekVar.a(az.a(ekVar.b(), c2.z(), c2.o(), c2.k()));
                c2.a(ekVar);
                try {
                    new Thread(new dq(this, c2, iVar)).start();
                } catch (Exception e2) {
                }
                a(c2);
                try {
                    this.d.setVisibility(8);
                    this.d.a();
                } catch (Exception e3) {
                }
            } catch (Exception e4) {
            }
        } catch (Exception e5) {
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
    }

    public void b(int i2) {
        try {
            this.d.a(i2);
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: package-private */
    public void b(ct ctVar) {
        try {
            this.a.a(ctVar);
            this.b.a(ctVar);
        } catch (Exception e2) {
            f.a(e2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: net.youmi.android.bd.b(android.content.Context, java.lang.String):void
     arg types: [android.app.Activity, java.lang.String]
     candidates:
      net.youmi.android.bd.b(android.app.Activity, java.lang.String):void
      net.youmi.android.bd.b(android.content.Context, java.lang.String):void */
    public void b(i iVar, File file, String str) {
        this.e.a().c = false;
        try {
            this.d.setVisibility(8);
            this.d.a();
        } catch (Exception e2) {
        }
        if (file != null && file.exists()) {
            ax.a(this.g, "下载成功");
            try {
                k.a(this.g, k.a(this.g, file.getPath(), str, iVar.a(), iVar.b(), iVar.c()), 3);
            } catch (Exception e3) {
                f.a(e3);
            }
            bd.b((Context) this.g, file.getPath());
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: package-private */
    public void c() {
        try {
            Toast.makeText(this.g, "正在加载", 0).show();
            ct c2 = this.e.c();
            if (c2 != null) {
                switch (c2.a()) {
                    case -1:
                        if (c2.i() != null) {
                            try {
                                WebView webView = new WebView(this.g);
                                webView.loadDataWithBaseURL(null, c2.i().b(), "text/html", "utf-8", null);
                                new AlertDialog.Builder(this.g).setTitle("有米广告SDK配置简易教程").setView(webView).create().show();
                                return;
                            } catch (Exception e2) {
                                f.a(e2);
                                return;
                            }
                        } else {
                            return;
                        }
                    case AdMogoUtil.NETWORK_TYPE_MOBCLIX:
                        if (c2.i() != null) {
                            a(c2);
                            return;
                        }
                        this.e.a().c = true;
                        new i(this.g, this).execute(c2.p());
                        return;
                    case AdMogoUtil.NETWORK_TYPE_ADCHINA:
                        try {
                            this.h = new dw(this.g, c2, System.currentTimeMillis() - c2.A(), 0);
                            bd.c(this.g, c2.p());
                            return;
                        } catch (Exception e3) {
                            return;
                        }
                    case AdMogoUtil.NETWORK_TYPE_WIYUN:
                        try {
                            this.e.a().c = true;
                            new i(this.g, this, c2.f()).execute(c2.p());
                        } catch (Exception e4) {
                        }
                        try {
                            aj.a(this.g, c2);
                            return;
                        } catch (Exception e5) {
                            return;
                        }
                    case 31:
                        try {
                            String[] x = c2.x();
                            if (x != null) {
                                this.e.a().c = true;
                                new du(this.g, this, c2).execute(x);
                                return;
                            }
                            return;
                        } catch (Exception e6) {
                            return;
                        }
                    case AdMogoUtil.NETWORK_TYPE_AIRAD:
                    default:
                        return;
                    case 41:
                        try {
                            this.h = new dw(this.g, c2, System.currentTimeMillis() - c2.A(), 0);
                            bd.a(this.g, Uri.parse(c2.p()));
                            return;
                        } catch (Exception e7) {
                            return;
                        }
                    case 42:
                        try {
                            this.h = new dw(this.g, c2, System.currentTimeMillis() - c2.A(), 0);
                            y.a(this.g, c2.p());
                            return;
                        } catch (Exception e8) {
                            return;
                        }
                    case 43:
                        try {
                            this.h = new dw(this.g, c2, System.currentTimeMillis() - c2.A(), 0);
                            y.c(this.g, c2.p());
                            return;
                        } catch (Exception e9) {
                            return;
                        }
                    case 44:
                        try {
                            this.h = new dw(this.g, c2, System.currentTimeMillis() - c2.A(), 0);
                            y.b(this.g, c2.p());
                            return;
                        } catch (Exception e10) {
                            return;
                        }
                }
            }
        } catch (Exception e11) {
        }
    }

    /* access modifiers changed from: package-private */
    public void c(int i2) {
        int i3 = this.f;
        int red = Color.red(i2);
        int green = Color.green(i2);
        int blue = Color.blue(i2);
        setBackgroundDrawable(new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{Color.argb(i3, red + 60 < 255 ? red + 60 : 255, green + 60 < 255 ? green + 60 : 255, blue + 60 < 255 ? blue + 60 : 255), Color.argb(i3, red - 60 > 0 ? red - 60 : 0, green - 60 > 0 ? green - 60 : 0, blue - 60 > 0 ? blue - 60 : 0)}));
    }

    public void e() {
        this.e.a().c = false;
        try {
            this.d.setVisibility(8);
            this.d.a();
        } catch (Exception e2) {
        }
        try {
            Toast.makeText(this.g, "无法连接服务器,请稍后重试!", 0).show();
        } catch (Exception e3) {
        }
    }

    public void f() {
        this.e.a().c = false;
        ax.a(this.g, "正在下载");
        try {
            this.d.setVisibility(8);
            this.d.a();
        } catch (Exception e2) {
        }
    }

    public void g() {
        this.e.a().c = false;
        ax.a(this.g, "存储卡不可用,请启用存储卡", 1);
        try {
            this.d.setVisibility(8);
            this.d.a();
        } catch (Exception e2) {
        }
    }

    public void h() {
        q.d(this.g);
        ax.a(this.g, "下载失败");
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        try {
            if (this.h != null) {
                this.h.a();
            }
        } catch (Exception e2) {
        }
    }
}
