package net.youmi.android;

import android.app.Activity;
import android.widget.FrameLayout;

class dr extends FrameLayout {
    static int e = -1;
    Activity a;
    bz b;
    g c;
    ca d;
    protected int f;
    private int g;
    private int h = -1;

    public dr(Activity activity, cn cnVar, bz bzVar) {
        super(activity);
        int i = e + 1;
        e = i;
        this.f = i;
        this.a = activity;
        this.b = bzVar;
        this.c = new g(this.a, cnVar, this.b);
        addView(this.c);
        this.d = new ca(this.a);
        addView(this.d);
    }

    /* access modifiers changed from: package-private */
    public void a(int i) {
        this.h = i;
    }

    /* access modifiers changed from: package-private */
    public void a(ch chVar) {
        this.g = 0;
        if (this.d != null) {
            this.d.setVisibility(8);
        }
        this.c.a(chVar);
    }

    /* access modifiers changed from: package-private */
    public void a(m mVar) {
        this.g = 1;
        if (this.c != null) {
            this.c.setVisibility(8);
        }
        this.d.a(mVar);
    }
}
