package net.youmi.android;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.os.Build;
import java.lang.reflect.Field;

/* renamed from: net.youmi.android.do  reason: invalid class name */
class Cdo {
    Cdo() {
    }

    static int a(Context context) {
        try {
            if (Integer.parseInt(Build.VERSION.SDK) < 4) {
                return 1;
            }
            ApplicationInfo applicationInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).applicationInfo;
            Field field = ApplicationInfo.class.getField("targetSdkVersion");
            if (applicationInfo == null || field == null) {
                return 1;
            }
            return field.getInt(applicationInfo);
        } catch (Exception e) {
            return 1;
        }
    }

    static boolean a(Context context, String str) {
        try {
            return context.checkCallingOrSelfPermission(str) != -1;
        } catch (Exception e) {
        }
    }

    static boolean b(Context context) {
        try {
            if (a(context) >= 4) {
                return a(context, "android.permission.WRITE_EXTERNAL_STORAGE");
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    static boolean c(Context context) {
        return a(context, "android.permission.INTERNET");
    }

    static boolean d(Context context) {
        return a(context, "android.permission.READ_PHONE_STATE");
    }

    static boolean e(Context context) {
        return a(context, "android.permission.ACCESS_NETWORK_STATE");
    }

    static boolean f(Context context) {
        return a(context, "android.permission.ACCESS_FINE_LOCATION");
    }

    static boolean g(Context context) {
        return a(context, "android.permission.ACCESS_COARSE_LOCATION");
    }
}
