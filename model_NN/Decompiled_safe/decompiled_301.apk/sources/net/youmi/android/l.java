package net.youmi.android;

import android.app.Activity;
import android.view.animation.Animation;
import android.widget.RelativeLayout;

class l extends RelativeLayout implements ba {
    cq a;
    bw b;
    final /* synthetic */ cw c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public l(cw cwVar, Activity activity, int i) {
        super(activity);
        this.c = cwVar;
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(cwVar.l, cwVar.m);
        this.b = new bw(activity, cwVar.l, cwVar.m, i);
        addView(this.b, layoutParams);
    }

    public void a() {
        this.b.a();
        setVisibility(8);
    }

    public void a(Animation animation) {
    }

    public boolean a(ct ctVar) {
        if (ctVar == null) {
            return false;
        }
        try {
            this.a = null;
            if (ctVar.m() == null) {
                return false;
            }
            this.a = ctVar.m();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public void b() {
        try {
            this.b.a(this.a);
        } catch (Exception e) {
        }
    }

    public void c() {
        setVisibility(0);
    }

    public void d() {
        try {
            this.b.a(this.a);
        } catch (Exception e) {
        }
    }
}
