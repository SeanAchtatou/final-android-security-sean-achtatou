package net.youmi.android;

import org.json.JSONArray;
import org.json.JSONObject;

class cx {
    cx() {
    }

    static int a(JSONObject jSONObject, String str, int i) {
        if (jSONObject != null) {
            try {
                if (!jSONObject.isNull(str)) {
                    return jSONObject.getInt(str);
                }
            } catch (Exception e) {
            }
        }
        return i;
    }

    static String a(JSONObject jSONObject, String str, String str2) {
        String string;
        if (jSONObject != null) {
            try {
                if (!jSONObject.isNull(str) && (string = jSONObject.getString(str)) != null) {
                    return string.trim();
                }
            } catch (Exception e) {
            }
        }
        return str2;
    }

    static JSONArray a(JSONObject jSONObject, String str, JSONArray jSONArray) {
        if (jSONObject != null) {
            try {
                if (!jSONObject.isNull(str)) {
                    return jSONObject.getJSONArray(str);
                }
            } catch (Exception e) {
            }
        }
        return jSONArray;
    }

    static JSONObject a(String str) {
        try {
            return new JSONObject(str);
        } catch (Exception e) {
            return null;
        }
    }

    static JSONObject a(JSONObject jSONObject, String str, JSONObject jSONObject2) {
        if (jSONObject != null) {
            try {
                if (!jSONObject.isNull(str)) {
                    return jSONObject.getJSONObject(str);
                }
            } catch (Exception e) {
            }
        }
        return jSONObject2;
    }
}
