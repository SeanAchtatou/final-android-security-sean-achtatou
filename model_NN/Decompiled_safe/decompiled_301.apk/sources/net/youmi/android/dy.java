package net.youmi.android;

import android.app.Activity;
import android.view.animation.Animation;
import android.widget.FrameLayout;
import android.widget.ImageView;

class dy extends FrameLayout implements ba {
    ImageView a;
    ImageView b;
    ImageView c;
    Animation d;
    Animation e;
    final /* synthetic */ cw f;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public dy(cw cwVar, Activity activity, bz bzVar, boolean z) {
        super(activity);
        int i;
        int i2;
        this.f = cwVar;
        this.a = new ImageView(activity);
        this.b = new ImageView(activity);
        this.a.setScaleType(ImageView.ScaleType.FIT_XY);
        this.b.setScaleType(ImageView.ScaleType.FIT_XY);
        this.a.setVisibility(8);
        this.b.setVisibility(8);
        int a2 = cwVar.l;
        int b2 = cwVar.m;
        if (z) {
            int b3 = bzVar.a().c().b();
            i = b3;
            i2 = bzVar.a().c().b();
        } else {
            int i3 = b2;
            i = a2;
            i2 = i3;
        }
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(i, i2);
        addView(this.a, layoutParams);
        addView(this.b, layoutParams);
        if (z) {
            this.d = ai.c(bzVar);
            this.e = ai.d(bzVar);
            return;
        }
        this.d = ai.a(bzVar);
        this.e = ai.b(bzVar);
    }

    public void a() {
        setVisibility(8);
    }

    public void a(Animation animation) {
        try {
            startAnimation(animation);
        } catch (Exception e2) {
        }
    }

    public boolean a(ct ctVar) {
        if (ctVar == null) {
            return false;
        }
        try {
            if (ctVar.e() == null) {
                return false;
            }
            ImageView e2 = e();
            if (e2 == null) {
                return false;
            }
            try {
                e2.setImageBitmap(ctVar.e());
            } catch (Exception e3) {
            }
            return true;
        } catch (Exception e4) {
            return false;
        }
    }

    public void b() {
        try {
            ImageView e2 = e();
            if (e2 != null) {
                if (this.c != null) {
                    this.c.setVisibility(8);
                }
                e2.setVisibility(0);
                this.c = e2;
            }
        } catch (Exception e3) {
            f.a(e3);
        }
    }

    public void c() {
        setVisibility(0);
    }

    public void d() {
        ImageView e2 = e();
        if (e2 != null) {
            if (this.c != null) {
                this.c.startAnimation(this.e);
                this.c.setVisibility(8);
            }
            e2.setVisibility(0);
            e2.startAnimation(this.d);
            this.c = e2;
        }
    }

    /* access modifiers changed from: package-private */
    public ImageView e() {
        return this.a == this.c ? this.b : this.a;
    }
}
