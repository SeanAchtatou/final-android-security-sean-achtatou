package net.youmi.android;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.Html;
import java.net.URLDecoder;

class bd {
    bd() {
    }

    static void a(Activity activity) {
        try {
            AdActivity.b(activity);
        } catch (Exception e) {
            f.a(e);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: net.youmi.android.bd.a(android.content.Context, java.lang.String):void
     arg types: [android.app.Activity, java.lang.String]
     candidates:
      net.youmi.android.bd.a(android.app.Activity, java.lang.String):void
      net.youmi.android.bd.a(android.app.Activity, android.net.Uri):boolean
      net.youmi.android.bd.a(android.content.Context, java.lang.String):void */
    static void a(Activity activity, String str) {
        try {
            if (str.toLowerCase().indexOf(".apk") > -1) {
                a((Context) activity, str);
            }
        } catch (Exception e) {
        }
    }

    static void a(Activity activity, String str, String str2, String str3, String str4) {
        try {
            b(activity, "http://ditu.google.com/maps?q=" + str + "," + str2 + "(" + str3 + " " + str4 + ")&z=16");
        } catch (Exception e) {
        }
    }

    static void a(Activity activity, String[] strArr, String[] strArr2, String[] strArr3, String str, String str2) {
        try {
            Intent intent = new Intent("android.intent.action.SEND");
            intent.putExtra("android.intent.extra.EMAIL", strArr);
            if (strArr2 != null && strArr2.length > 0) {
                intent.putExtra("android.intent.extra.CC", strArr2);
            }
            if (strArr3 != null && strArr3.length > 0) {
                intent.putExtra("android.intent.extra.BCC", strArr3);
            }
            intent.putExtra("android.intent.extra.SUBJECT", str);
            intent.putExtra("android.intent.extra.TEXT", str2);
            intent.setType("message/rfc822");
            activity.startActivity(intent);
        } catch (Exception e) {
        }
    }

    static void a(Context context, String str) {
        try {
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setDataAndType(Uri.parse(str), "application/vnd.android.package-archive");
            context.startActivity(intent);
        } catch (Exception e) {
            f.a(e);
        }
    }

    static boolean a(Activity activity, Uri uri) {
        if (uri != null) {
            try {
                Intent intent = new Intent("android.intent.action.DIAL", uri);
                intent.addFlags(268435456);
                activity.startActivity(intent);
                return true;
            } catch (Exception e) {
            }
        }
        return false;
    }

    static boolean a(Activity activity, String str, String str2) {
        if (str != null) {
            try {
                Intent intent = new Intent("android.intent.action.SENDTO", Uri.parse("smsto:" + str));
                if (str2 != null) {
                    intent.putExtra("sms_body", str2);
                }
                activity.startActivity(intent);
                return true;
            } catch (Exception e) {
            }
        }
        return false;
    }

    static void b(Activity activity, String str) {
        if (str != null) {
            String trim = str.trim();
            if (trim.length() != 0) {
                try {
                    Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(trim));
                    intent.addFlags(268435456);
                    intent.setComponent(new ComponentName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity"));
                    activity.startActivity(intent);
                } catch (Exception e) {
                    try {
                        Intent intent2 = new Intent("android.intent.action.VIEW", Uri.parse(trim));
                        intent2.addFlags(268435456);
                        activity.startActivity(intent2);
                    } catch (Exception e2) {
                    }
                }
            }
        }
    }

    static void b(Activity activity, String[] strArr, String[] strArr2, String[] strArr3, String str, String str2) {
        try {
            Intent intent = new Intent("android.intent.action.SEND");
            intent.putExtra("android.intent.extra.EMAIL", strArr);
            if (strArr2 != null && strArr2.length > 0) {
                intent.putExtra("android.intent.extra.CC", strArr2);
            }
            if (strArr3 != null && strArr3.length > 0) {
                intent.putExtra("android.intent.extra.BCC", strArr3);
            }
            intent.putExtra("android.intent.extra.SUBJECT", str);
            intent.putExtra("android.intent.extra.TEXT", Html.fromHtml(str2));
            intent.setType("text/html");
            activity.startActivity(intent);
        } catch (Exception e) {
        }
    }

    static void b(Context context, String str) {
        a(context, "file://" + str);
    }

    static void c(Activity activity, String str) {
        if (str != null) {
            String trim = str.trim();
            if (trim.length() != 0) {
                try {
                    Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(trim));
                    intent.addFlags(268435456);
                    activity.startActivity(intent);
                } catch (Exception e) {
                }
            }
        }
    }

    static boolean d(Activity activity, String str) {
        if (str == null) {
            return false;
        }
        try {
            if (str.indexOf("geo:") == 0) {
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
                intent.addFlags(268435456);
                activity.startActivity(intent);
                return true;
            }
        } catch (Exception e) {
        }
        return false;
    }

    static boolean e(Activity activity, String str) {
        String str2;
        if (str == null) {
            return false;
        }
        try {
            String str3 = new String(str);
            int indexOf = str3.indexOf("?body=");
            if (indexOf > -1) {
                str2 = str3.substring("?body=".length() + indexOf);
                str3 = str3.substring(0, indexOf);
            } else {
                str2 = null;
            }
            String substring = str3.indexOf("sms:") == 0 ? str3.substring("sms:".length()) : str3.indexOf("smsto:") == 0 ? str3.substring("smsto:".length()) : null;
            if (substring != null) {
                Intent intent = new Intent("android.intent.action.SENDTO", Uri.parse("smsto:" + substring));
                if (str2 != null) {
                    intent.putExtra("sms_body", URLDecoder.decode(str2));
                }
                activity.startActivity(intent);
                return true;
            }
        } catch (Exception e) {
        }
        return false;
    }

    static boolean f(Activity activity, String str) {
        if (str == null) {
            return false;
        }
        String str2 = null;
        try {
            if (str.indexOf("wtai://wp/mc;") == 0) {
                str2 = str.substring("wtai://wp/mc;".length());
            } else if (str.indexOf("tel:") == 0) {
                str2 = str.substring("tel:".length());
            }
            if (str2 != null) {
                Intent intent = new Intent("android.intent.action.DIAL", Uri.parse("tel:" + str2));
                intent.addFlags(268435456);
                activity.startActivity(intent);
                return true;
            }
        } catch (Exception e) {
        }
        return false;
    }
}
