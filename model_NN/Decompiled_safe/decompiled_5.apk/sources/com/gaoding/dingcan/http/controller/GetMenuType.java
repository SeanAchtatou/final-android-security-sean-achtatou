package com.gaoding.dingcan.http.controller;

import android.content.Context;
import com.gaoding.dingcan.AppConfig;
import com.gaoding.dingcan.database.DataStore;
import com.gaoding.dingcan.database.bean.MenuTypeBean;
import com.gaoding.dingcan.http.HttpResult;
import com.gaoding.dingcan.http.ProxyFactory;
import com.gaoding.dingcan.util.LogUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GetMenuType {
    private String ACTION = "getMenuType";
    public int code;
    public List<MenuTypeBean> list = new ArrayList();
    public HashMap<String, String> mHashMapParameter = new HashMap<>();
    public String mRestaurantId;
    public String message;
    public String returnAction;
    public boolean status = false;

    public GetMenuType(Context context) {
    }

    public void close() {
    }

    public boolean GetList() {
        return requestHttp();
    }

    private HashMap<String, String> putParameter() {
        HashMap<String, String> mHashMapParameter2 = new HashMap<>();
        mHashMapParameter2.put("action", this.ACTION);
        mHashMapParameter2.put("restaurantId", this.mRestaurantId);
        LogUtils.logDebug(mHashMapParameter2.toString());
        return mHashMapParameter2;
    }

    private boolean parserJson(String retSrc) {
        LogUtils.logDebug("parserJson:" + retSrc);
        try {
            JSONObject result = new JSONObject(retSrc);
            if (result.has(DataStore.UserInfoTable.USER_STATUS)) {
                this.status = result.getBoolean(DataStore.UserInfoTable.USER_STATUS);
            }
            if (this.status) {
                LogUtils.logDebug("register status = true");
                JSONArray jsonArrayMessage = result.getJSONArray("restaurantList");
                for (int i = 0; i < jsonArrayMessage.length(); i++) {
                    JSONObject jsonObjectMessage = (JSONObject) jsonArrayMessage.get(i);
                    MenuTypeBean item = new MenuTypeBean();
                    if (result.has("restaurantId")) {
                        item.mId = jsonObjectMessage.getString("restaurantId");
                    }
                    if (result.has("restaurantName")) {
                        item.mName = jsonObjectMessage.getString("restaurantName");
                    }
                    LogUtils.logDebug("item.mId=" + item.mId);
                    LogUtils.logDebug("item.mName" + item.mName);
                    this.list.add(item);
                }
                return true;
            }
            LogUtils.logDebug("city status = false");
            if (result.has("code")) {
                this.code = result.getInt("code");
            }
            if (result.has("message")) {
                this.message = result.getString("message");
            }
            return false;
        } catch (JSONException ex) {
            throw new RuntimeException(ex);
        }
    }

    public boolean requestHttp() {
        try {
            HttpResult<String> response = ProxyFactory.getDefaultProxy().get(AppConfig.MENUTYPE_URL, putParameter());
            if (response.getCode() != 200) {
                LogUtils.logDebug("http return false, code=" + response.getCode());
                return false;
            } else if (parserJson(response.getValue())) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
