package com.gaoding.dingcan.http;

public class ProxyFactory {
    public static Proxy getDefaultProxy() {
        return HttpClientProxy.getInstance();
    }
}
