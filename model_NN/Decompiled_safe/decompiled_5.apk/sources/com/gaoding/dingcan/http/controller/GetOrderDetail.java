package com.gaoding.dingcan.http.controller;

import android.content.Context;
import com.gaoding.dingcan.AppConfig;
import com.gaoding.dingcan.database.DataStore;
import com.gaoding.dingcan.database.bean.ShopBean;
import com.gaoding.dingcan.http.HttpResult;
import com.gaoding.dingcan.http.ProxyFactory;
import com.gaoding.dingcan.util.LogUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GetOrderDetail {
    private String ACTION = "getOrderDetail";
    public int code;
    public List<ShopBean> list = new ArrayList();
    public HashMap<String, String> mHashMapParameter = new HashMap<>();
    public String mOrderId;
    public String message;
    public String returnAction;
    public boolean status = false;

    public GetOrderDetail(Context context) {
    }

    public void close() {
    }

    public boolean GetInfo() {
        return requestHttp();
    }

    private HashMap<String, String> putParameter() {
        HashMap<String, String> mHashMapParameter2 = new HashMap<>();
        mHashMapParameter2.put("action", this.ACTION);
        mHashMapParameter2.put("orderId", this.mOrderId);
        return mHashMapParameter2;
    }

    private boolean parserJson(String retSrc) {
        LogUtils.logDebug("parserJson:" + retSrc);
        try {
            JSONObject result = new JSONObject(retSrc);
            if (result.has(DataStore.UserInfoTable.USER_STATUS)) {
                this.status = result.getBoolean(DataStore.UserInfoTable.USER_STATUS);
            }
            if (this.status) {
                LogUtils.logDebug("status = true");
                this.list.clear();
                JSONArray jsonArrayMessage = result.getJSONArray("orderMenuListAll");
                for (int i = 0; i < jsonArrayMessage.length(); i++) {
                    JSONObject jsonObjectMessage = (JSONObject) jsonArrayMessage.get(i);
                    ShopBean item = new ShopBean();
                    if (jsonObjectMessage.has("orderMenuId")) {
                        item.mId = jsonObjectMessage.getString("orderMenuId");
                    }
                    if (jsonObjectMessage.has("menuname")) {
                        item.mName = jsonObjectMessage.getString("menuname");
                    }
                    if (jsonObjectMessage.has("menuPrice")) {
                        item.mPrice = jsonObjectMessage.getString("menuPrice");
                    }
                    if (jsonObjectMessage.has("orderMenuAmount")) {
                        item.mCount = jsonObjectMessage.getString("orderMenuAmount");
                    }
                    if (jsonObjectMessage.has("orderMenuTotalMoney")) {
                        item.mPrice = jsonObjectMessage.getString("orderMenuTotalMoney");
                    }
                    this.list.add(item);
                }
                return true;
            }
            LogUtils.logDebug("status = false");
            if (result.has("code")) {
                this.code = result.getInt("code");
            }
            if (result.has("message")) {
                this.message = result.getString("message");
            }
            return false;
        } catch (JSONException ex) {
            throw new RuntimeException(ex);
        }
    }

    public boolean requestHttp() {
        try {
            HttpResult<String> response = ProxyFactory.getDefaultProxy().get(AppConfig.ORDER_DETAIL_URL, putParameter());
            if (response.getCode() != 200) {
                LogUtils.logDebug("http return false, code=" + response.getCode());
                return false;
            } else if (parserJson(response.getValue())) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
