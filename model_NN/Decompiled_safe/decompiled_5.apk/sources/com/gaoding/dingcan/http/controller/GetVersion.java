package com.gaoding.dingcan.http.controller;

import android.content.Context;
import com.gaoding.dingcan.AppConfig;
import com.gaoding.dingcan.database.DataStore;
import com.gaoding.dingcan.http.HttpResult;
import com.gaoding.dingcan.http.ProxyFactory;
import com.gaoding.dingcan.util.LogUtils;
import com.gaoding.dingcan.util.Utils;
import java.util.HashMap;
import org.json.JSONException;
import org.json.JSONObject;

public class GetVersion {
    private String ACTION = "getVersion";
    public int code;
    private Context context;
    public String downURL;
    public HashMap<String, String> mHashMapParameter = new HashMap<>();
    public String message;
    public String returnAction;
    public String serverVersion;
    public boolean status = false;

    public GetVersion(Context context2) {
        this.context = context2;
    }

    public void close() {
    }

    public boolean GetNewVersion() {
        boolean result = requestHttp();
        if (result) {
            this.serverVersion.compareTo(Utils.getAPKVersionName(this.context));
        }
        return result;
    }

    private HashMap<String, String> putParameter() {
        HashMap<String, String> mHashMapParameter2 = new HashMap<>();
        mHashMapParameter2.put("action", this.ACTION);
        return mHashMapParameter2;
    }

    private boolean parserJson(String retSrc) {
        LogUtils.logDebug("parserJson:" + retSrc);
        try {
            JSONObject result = new JSONObject(retSrc);
            if (result.has(DataStore.UserInfoTable.USER_STATUS)) {
                this.status = result.getBoolean(DataStore.UserInfoTable.USER_STATUS);
            }
            if (this.status) {
                if (result.has("version")) {
                    this.serverVersion = result.getString("version");
                }
                if (result.has("url")) {
                    this.downURL = result.getString("url");
                }
                return true;
            }
            if (result.has("code")) {
                this.code = result.getInt("code");
                LogUtils.logDebug("code=" + this.code);
            }
            if (result.has("message")) {
                this.message = result.getString("message");
                LogUtils.logDebug("message=" + this.message);
            }
            return false;
        } catch (JSONException ex) {
            throw new RuntimeException(ex);
        }
    }

    public boolean requestHttp() {
        try {
            HttpResult<String> response = ProxyFactory.getDefaultProxy().post(AppConfig.VERSION_URL, putParameter());
            if (response.getCode() != 200) {
                LogUtils.logDebug("http return false, code=" + response.getCode());
                return false;
            } else if (parserJson(response.getValue())) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
