package com.gaoding.dingcan.http.controller;

import android.content.Context;
import com.gaoding.dingcan.AppConfig;
import com.gaoding.dingcan.database.DataStore;
import com.gaoding.dingcan.database.controller.Shop;
import com.gaoding.dingcan.database.controller.UserInfo;
import com.gaoding.dingcan.http.HttpResult;
import com.gaoding.dingcan.http.ProxyFactory;
import com.gaoding.dingcan.util.LogUtils;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SendShopping {
    private String ACTION = "sendOrder";
    public String address;
    public int code;
    public String desc;
    public String message;
    public String phone;
    public String restaurantId;
    public String returnAction;
    public Shop shop;
    public boolean status = false;
    public UserInfo userInfo;

    public SendShopping(Context context) {
        this.userInfo = new UserInfo(context);
        this.shop = new Shop(context);
        this.userInfo.getFromDatabase();
        this.shop.getFromDatabase();
    }

    public void close() {
        this.userInfo.close();
        this.shop.close();
    }

    public boolean send(String restaurantId2, String address2, String phone2, String desc2) {
        this.restaurantId = restaurantId2;
        this.address = address2;
        this.phone = phone2;
        this.desc = desc2;
        boolean result = requestHttp();
        if (result) {
            this.shop.clean();
        }
        return result;
    }

    private HashMap<String, String> putParameter() {
        HashMap<String, String> mHashMapParameter = new HashMap<>();
        mHashMapParameter.put("action", this.ACTION);
        mHashMapParameter.put("uid", this.userInfo.Item.mUid);
        mHashMapParameter.put("restaurantId", this.restaurantId);
        mHashMapParameter.put("address", this.address);
        mHashMapParameter.put(DataStore.UserInfoTable.USER_PHONE, this.phone);
        mHashMapParameter.put("orderDesc", this.desc);
        String orderList = "'[";
        for (int i = 0; i < this.shop.list.size(); i++) {
            orderList = String.valueOf(String.valueOf(String.valueOf(String.valueOf(orderList) + "{") + "\"rmenuId\":\"" + this.shop.list.get(i).mMenuId + "\"") + ",\"count\":\"" + this.shop.list.get(i).mCount + "\"") + "},";
        }
        String orderList2 = String.valueOf(orderList) + "]'";
        LogUtils.logDebug("action=" + this.ACTION);
        LogUtils.logDebug("uid=" + this.userInfo.Item.mUid);
        LogUtils.logDebug("restaurantId=" + this.restaurantId);
        LogUtils.logDebug("address=" + this.address);
        LogUtils.logDebug("telephone=" + this.phone);
        LogUtils.logDebug("orderDesc=" + this.desc);
        LogUtils.logDebug("orderList=" + orderList2);
        mHashMapParameter.put("orderList", orderList2);
        return mHashMapParameter;
    }

    private JSONObject putJson() {
        try {
            JSONObject param = new JSONObject();
            param.put("action", this.ACTION);
            param.put("uid", this.userInfo.Item.mUid);
            param.put("restaurantId", this.restaurantId);
            param.put("address", this.address);
            param.put(DataStore.UserInfoTable.USER_PHONE, this.phone);
            param.put("orderDesc", this.desc);
            JSONArray jsonArray = new JSONArray();
            for (int i = 0; i < this.shop.list.size(); i++) {
                JSONObject jsonItem = new JSONObject();
                jsonItem.put("rmenuId", this.shop.list.get(i).mMenuId);
                jsonItem.put("count", this.shop.list.get(i).mCount);
                jsonArray.put(jsonItem);
            }
            param.put("orderList", jsonArray);
            LogUtils.logDebug("SendOrder json=" + param.toString());
            return param;
        } catch (JSONException ex) {
            throw new RuntimeException(ex);
        }
    }

    private String putJsonString() {
        StringBuilder strResult = new StringBuilder();
        strResult.append("{");
        strResult.append("\"action\":\"sendOrder\"");
        strResult.append(",\"uid\":\"" + this.userInfo.Item.mUid + "\"");
        strResult.append(",\"restaurantId\":\"" + this.restaurantId + "\"");
        strResult.append(",\"address\":\"" + this.address + "\"");
        strResult.append(",\"telephone\":\"" + this.phone + "\"");
        String orderList = "'[";
        for (int i = 0; i < this.shop.list.size(); i++) {
            orderList = String.valueOf(String.valueOf(String.valueOf(String.valueOf(orderList) + "{") + "\"rmenuId\":\"" + this.shop.list.get(i).mMenuId + "\"") + ",\"count\":\"" + this.shop.list.get(i).mCount + "\"") + "},";
        }
        strResult.append(",\"orderList\":" + (String.valueOf(orderList) + "]'"));
        strResult.append("}");
        return strResult.toString();
    }

    private boolean parserJson(String retSrc) {
        LogUtils.logDebug("parserJson:" + retSrc);
        try {
            JSONObject result = new JSONObject(retSrc);
            if (result.has(DataStore.UserInfoTable.USER_STATUS)) {
                this.status = result.getBoolean(DataStore.UserInfoTable.USER_STATUS);
            }
            if (this.status) {
                LogUtils.logDebug("register status = true");
                return true;
            }
            LogUtils.logDebug("register status = false");
            if (result.has("code")) {
                this.code = result.getInt("code");
            }
            if (result.has("message")) {
                this.message = result.getString("message");
            }
            return false;
        } catch (JSONException ex) {
            throw new RuntimeException(ex);
        }
    }

    public boolean requestHttp() {
        try {
            HttpResult<String> response = ProxyFactory.getDefaultProxy().post(AppConfig.SEND_ORDER_URL, putParameter());
            if (response.getCode() != 200) {
                LogUtils.logDebug("http return false, code=" + response.getCode());
                return false;
            } else if (parserJson(response.getValue())) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
