package com.gaoding.dingcan.http.controller;

import android.content.Context;
import com.gaoding.dingcan.AppConfig;
import com.gaoding.dingcan.database.DataStore;
import com.gaoding.dingcan.database.bean.MenuBean;
import com.gaoding.dingcan.database.bean.MenuTypeBean;
import com.gaoding.dingcan.http.HttpResult;
import com.gaoding.dingcan.http.ProxyFactory;
import com.gaoding.dingcan.util.LogUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GetMenu {
    private String ACTION = "getMenu";
    public int code;
    public List<MenuBean> listMenu = new ArrayList();
    public List<MenuTypeBean> listType = new ArrayList();
    public HashMap<String, String> mHashMapParameter = new HashMap<>();
    public String mRestaurantId;
    public String message;
    public String returnAction;
    public boolean status = false;

    public GetMenu(Context context) {
    }

    public void close() {
    }

    public boolean GetList() {
        boolean result = requestHttp();
        if (result) {
            return true;
        }
        return result;
    }

    private HashMap<String, String> putParameter() {
        HashMap<String, String> mHashMapParameter2 = new HashMap<>();
        mHashMapParameter2.put("action", this.ACTION);
        mHashMapParameter2.put("restaurantId", this.mRestaurantId);
        return mHashMapParameter2;
    }

    private HashMap<String, String> putMenuParameter(String mMenuTypeId) {
        HashMap<String, String> mHashMapParameter2 = new HashMap<>();
        mHashMapParameter2.put("action", this.ACTION);
        mHashMapParameter2.put("rmenuTypeId", mMenuTypeId);
        return mHashMapParameter2;
    }

    private boolean parserTypeJson(String retSrc) {
        LogUtils.logDebug("parserTypeJson:" + retSrc);
        try {
            JSONObject result = new JSONObject(retSrc);
            if (result.has(DataStore.UserInfoTable.USER_STATUS)) {
                this.status = result.getBoolean(DataStore.UserInfoTable.USER_STATUS);
            }
            if (this.status) {
                LogUtils.logDebug("register status = true");
                JSONArray jsonArrayMessage = result.getJSONArray("restaurantMenuTypeList");
                for (int i = 0; i < jsonArrayMessage.length(); i++) {
                    JSONObject jsonObjectMessage = (JSONObject) jsonArrayMessage.get(i);
                    MenuTypeBean item = new MenuTypeBean();
                    if (jsonObjectMessage.has("rmenuTypeId")) {
                        item.mId = jsonObjectMessage.getString("rmenuTypeId");
                    }
                    if (jsonObjectMessage.has("rmenuTypeName")) {
                        item.mName = jsonObjectMessage.getString("rmenuTypeName");
                    }
                    if (jsonObjectMessage.has("rmenuTypeDesc")) {
                        item.mDesc = jsonObjectMessage.getString("rmenuTypeDesc");
                    }
                    this.listType.add(item);
                }
                return true;
            }
            LogUtils.logDebug("type status = false");
            if (result.has("code")) {
                this.code = result.getInt("code");
            }
            if (result.has("message")) {
                this.message = result.getString("message");
            }
            return false;
        } catch (JSONException ex) {
            throw new RuntimeException(ex);
        }
    }

    private boolean parserMenuJson(String retSrc, String TypeId) {
        LogUtils.logDebug("parserMenuJson:" + retSrc);
        try {
            JSONObject result = new JSONObject(retSrc);
            if (result.has(DataStore.UserInfoTable.USER_STATUS)) {
                this.status = result.getBoolean(DataStore.UserInfoTable.USER_STATUS);
            }
            if (this.status) {
                LogUtils.logDebug("register status = true");
                JSONArray jsonArrayMessage = result.getJSONArray("restaurantMenuList");
                for (int i = 0; i < jsonArrayMessage.length(); i++) {
                    JSONObject jsonObjectMessage = (JSONObject) jsonArrayMessage.get(i);
                    MenuBean item = new MenuBean();
                    if (jsonObjectMessage.has("rmenuId")) {
                        item.mId = jsonObjectMessage.getString("rmenuId");
                    }
                    if (jsonObjectMessage.has("rmenuName")) {
                        item.mName = jsonObjectMessage.getString("rmenuName");
                    }
                    if (jsonObjectMessage.has("rmenuPrice")) {
                        item.mPrice = jsonObjectMessage.getString("rmenuPrice");
                    }
                    item.mTypeId = TypeId;
                    this.listMenu.add(item);
                }
                return true;
            }
            LogUtils.logDebug("menu status = false");
            if (result.has("code")) {
                this.code = result.getInt("code");
            }
            if (result.has("message")) {
                this.message = result.getString("message");
            }
            return false;
        } catch (JSONException ex) {
            throw new RuntimeException(ex);
        }
    }

    public boolean requestHttp() {
        try {
            HttpResult<String> response = ProxyFactory.getDefaultProxy().get(AppConfig.MENUTYPE_URL, putParameter());
            if (response.getCode() != 200) {
                LogUtils.logDebug("http return false, code=" + response.getCode());
                return false;
            } else if (!parserTypeJson(response.getValue())) {
                return false;
            } else {
                for (int i = 0; i < this.listType.size(); i++) {
                    HttpResult<String> response2 = ProxyFactory.getDefaultProxy().get(AppConfig.MENU_URL, putMenuParameter(this.listType.get(i).mId));
                    if (response2.getCode() == 200 && parserMenuJson(response2.getValue(), this.listType.get(i).mId)) {
                        LogUtils.logDebug("menu type =" + this.listType.get(i).mId);
                    }
                }
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
