package com.gaoding.dingcan.http.controller;

import android.content.Context;
import com.gaoding.dingcan.AppConfig;
import com.gaoding.dingcan.database.DataStore;
import com.gaoding.dingcan.database.bean.RestaurantBean;
import com.gaoding.dingcan.http.HttpResult;
import com.gaoding.dingcan.http.ProxyFactory;
import com.gaoding.dingcan.util.LogUtils;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GetRestaurantInfo {
    public static final String RES_STATE_CLOSE = "0";
    public static final String RES_STATE_OPEN = "1";
    private String ACTION = "getRestaurant";
    public int code;
    public RestaurantBean item = new RestaurantBean();
    public HashMap<String, String> mHashMapParameter = new HashMap<>();
    public String mResId;
    public String message;
    public String returnAction;
    public boolean status = false;

    public GetRestaurantInfo(Context context) {
    }

    public void close() {
    }

    public boolean GetInfo() {
        return requestHttp();
    }

    private HashMap<String, String> putParameter() {
        HashMap<String, String> mHashMapParameter2 = new HashMap<>();
        mHashMapParameter2.put("action", this.ACTION);
        mHashMapParameter2.put("restaurantId", this.mResId);
        return mHashMapParameter2;
    }

    private boolean parserJson(String retSrc) {
        LogUtils.logDebug("parserJson:" + retSrc);
        try {
            JSONObject result = new JSONObject(retSrc);
            if (result.has(DataStore.UserInfoTable.USER_STATUS)) {
                this.status = result.getBoolean(DataStore.UserInfoTable.USER_STATUS);
            }
            if (this.status) {
                LogUtils.logDebug("GetRestaurantInfo status = true");
                JSONArray jsonArrayMessage = result.getJSONArray("restaurantDetailList");
                for (int i = 0; i < jsonArrayMessage.length(); i++) {
                    JSONObject jsonObjectMessage = (JSONObject) jsonArrayMessage.get(i);
                    if (jsonObjectMessage.has("restaurantLogo")) {
                        this.item.mLogo = jsonObjectMessage.getString("restaurantLogo");
                    }
                    if (jsonObjectMessage.has("restaurantNotice")) {
                        this.item.mNotice = jsonObjectMessage.getString("restaurantNotice");
                    }
                    if (jsonObjectMessage.has("restaurantStartPrice")) {
                        this.item.mStartPrice = jsonObjectMessage.getString("restaurantStartPrice");
                    }
                    if (jsonObjectMessage.has("restaurantSendTime")) {
                        this.item.mSendTime = jsonObjectMessage.getString("restaurantSendTime");
                    }
                    if (jsonObjectMessage.has("restaurantSendDesc")) {
                        this.item.mSendDesc = jsonObjectMessage.getString("restaurantSendDesc");
                    }
                    if (jsonObjectMessage.has("restaurantState")) {
                        this.item.mState = jsonObjectMessage.getString("restaurantState");
                    }
                    if (jsonObjectMessage.has("restaurantOpen")) {
                        this.item.mOpenTime = jsonObjectMessage.getString("restaurantOpen");
                    }
                    if (jsonObjectMessage.has("SendFoodSpeed")) {
                        this.item.mSpeed = jsonObjectMessage.getString("SendFoodSpeed");
                    }
                    if (jsonObjectMessage.has("restaurantAddress")) {
                        this.item.mAddress = jsonObjectMessage.getString("restaurantAddress");
                    }
                    if (jsonObjectMessage.has("restaurantAreaTo")) {
                        this.item.mSendArea = jsonObjectMessage.getString("restaurantAreaTo");
                    }
                    if (jsonObjectMessage.has("restaurantNotice")) {
                        this.item.mNotice = jsonObjectMessage.getString("restaurantNotice");
                    }
                    LogUtils.logDebug("item.mOpenTime=" + this.item.mOpenTime);
                }
                return true;
            }
            LogUtils.logDebug("status = false");
            if (result.has("code")) {
                this.code = result.getInt("code");
            }
            if (result.has("message")) {
                this.message = result.getString("message");
            }
            return false;
        } catch (JSONException ex) {
            throw new RuntimeException(ex);
        }
    }

    public boolean requestHttp() {
        try {
            HttpResult<String> response = ProxyFactory.getDefaultProxy().get(AppConfig.R_DETAIL_URL, putParameter());
            if (response.getCode() != 200) {
                LogUtils.logDebug("http return false, code=" + response.getCode());
                return false;
            } else if (parserJson(response.getValue())) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
