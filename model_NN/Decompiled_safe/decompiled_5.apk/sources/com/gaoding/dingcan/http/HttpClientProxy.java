package com.gaoding.dingcan.http;

import com.gaoding.dingcan.util.LogUtils;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;
import org.apache.http.HttpEntity;
import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.ProtocolException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.RedirectHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.conn.params.ConnPerRouteBean;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.FileEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;

public class HttpClientProxy implements Proxy {
    private static final int CONCURRENCY = 10;
    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int SOCKET_TIMEOUT = 15000;
    private static final String TAG = "HttpClientProxy";
    private static HttpClientProxy instance;
    /* access modifiers changed from: private */
    public DefaultHttpClient client;

    interface Handler<T> {
        T handleResponse(HttpResponse httpResponse) throws ResponseException;
    }

    public static HttpClientProxy getInstance() {
        if (instance == null) {
            instance = new HttpClientProxy();
        }
        return instance;
    }

    private HttpClientProxy() {
        HttpParams params = new BasicHttpParams();
        ConnManagerParams.setMaxConnectionsPerRoute(params, new ConnPerRouteBean(10));
        ConnManagerParams.setMaxTotalConnections(params, 10);
        HttpConnectionParams.setConnectionTimeout(params, 10000);
        HttpConnectionParams.setSoTimeout(params, 15000);
        SchemeRegistry schemeRegistry = new SchemeRegistry();
        try {
            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            trustStore.load(null, null);
            SSLSocketFactory sf = new SSLSocketFactoryEx(trustStore);
            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
            HttpProtocolParams.setContentCharset(params, "UTF-8");
            schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            schemeRegistry.register(new Scheme("https", sf, 443));
        } catch (Exception e) {
            e.printStackTrace();
        }
        HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
        this.client = new DefaultHttpClient(new ThreadSafeClientConnManager(params, schemeRegistry), params);
        this.client.addRequestInterceptor(new HttpRequestInterceptor() {
            public void process(HttpRequest request, HttpContext context) throws HttpException, IOException {
                if (!request.containsHeader("Accept-Encoding")) {
                    request.addHeader("Accept-Encoding", "gzip");
                }
            }
        });
        this.client.getParams().setBooleanParameter("http.protocol.handle-redirects", false);
        this.client.setRedirectHandler(new RedirectHandler() {
            public URI getLocationURI(HttpResponse response, HttpContext context) throws ProtocolException {
                return null;
            }

            public boolean isRedirectRequested(HttpResponse response, HttpContext context) {
                if (response.getStatusLine().getStatusCode() == 301 || response.getStatusLine().getStatusCode() == 302) {
                }
                return false;
            }
        });
    }

    public HttpResult<String> get(String url, Map<String, String> parameters) throws ConnectionException, Exception {
        return new RequestExecutor<>(new HttpGet(queryString(url, parameters)), new StringHandler()).execute();
    }

    public HttpResult<String> post(String url, Map<String, String> parameters) throws ConnectionException, Exception {
        LogUtils.logDebug("post url=" + url);
        HttpPost post = new HttpPost(url);
        post.setEntity(encodedFormEntity(parameters));
        return new RequestExecutor<>(post, new StringHandler()).execute();
    }

    public HttpResult<String> post(String url, Map<String, String> map, HttpEntity entity) throws ConnectionException, Exception {
        HttpPost post = new HttpPost(url);
        post.setEntity(entity);
        return new RequestExecutor<>(post, new StringHandler()).execute();
    }

    public HttpResult<Asset> download(String url, Map<String, String> parameters, ProgressCallback callback) throws ConnectionException, Exception {
        return download(url, parameters, 0, 0, callback);
    }

    public HttpResult<String> upload(String url, File file, Map<String, String> parameters, ProgressCallback callback) throws ConnectionException, Exception {
        HttpPost post = new HttpPost(queryString(url, parameters));
        post.addHeader("Connection", "Keep-Alive");
        post.addHeader("Charset", "UTF-8");
        try {
            post.setEntity(new FileEntity(file, "binary/octet-stream"));
            System.out.println("end setEntity");
            HttpResponse response = this.client.execute(post);
            return new HttpResult<>(response.getStatusLine().getStatusCode(), EntityUtils.toString(response.getEntity()));
        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
    }

    public HttpResult<Asset> download(String url, Map<String, String> parameters, long startByte, long endByte, ProgressCallback callback) throws ConnectionException, Exception {
        return new RequestExecutor(new HttpGet(queryString(url, parameters)), new AssetHandler(callback)).execute();
    }

    class RequestExecutor<T> {
        private Handler<T> handler;
        private HttpUriRequest request;

        public RequestExecutor(HttpUriRequest request2, Handler<T> handler2) {
            this.request = request2;
            this.handler = handler2;
        }

        public HttpResult<T> execute() throws ConnectionException, ResponseException, Exception {
            try {
                this.request.addHeader("Connection", "Keep-Alive");
                this.request.addHeader("Charset", "UTF-8");
                for (Object obj : HttpClientProxy.this.client.getCookieStore().getCookies().toArray()) {
                    Object obj2 = (Cookie) obj;
                }
                HttpResponse response = HttpClientProxy.this.client.execute(this.request);
                return new HttpResult<>(response.getStatusLine().getStatusCode(), this.handler.handleResponse(response));
            } catch (ClientProtocolException e) {
                throw new ConnectionException((Exception) e);
            } catch (IOException e2) {
                throw new ResponseException(e2);
            } catch (Exception e3) {
                throw new ConnectionException(e3);
            }
        }
    }

    class StringHandler implements Handler<String> {
        StringBuilder buffer = new StringBuilder();

        StringHandler() {
        }

        public String handleResponse(HttpResponse response) throws ResponseException {
            BufferedReader input;
            HttpEntity entity = response.getEntity();
            InputStream stream = null;
            try {
                stream = entity.getContent();
                boolean gzip = false;
                if (response.containsHeader("Content-Encoding") && response.getFirstHeader("Content-Encoding").getValue().contains("gzip")) {
                    gzip = true;
                }
                if (gzip) {
                    input = new BufferedReader(new InputStreamReader(new GZIPInputStream(stream)), 8192);
                } else {
                    input = new BufferedReader(new InputStreamReader(stream), 8192);
                }
                while (true) {
                    String line = input.readLine();
                    if (line == null) {
                        break;
                    }
                    this.buffer.append(line);
                    this.buffer.append("\n");
                }
                String body = this.buffer.toString().trim();
                LogUtils.logDebug("response =" + body);
                String body2 = body.substring(1, body.length() - 1);
                LogUtils.logDebug("body substring=" + body2);
                if (entity != null) {
                    try {
                        entity.consumeContent();
                    } catch (Exception e) {
                    }
                }
                if (stream != null) {
                    stream.close();
                }
                if (this.buffer != null && this.buffer.length() > 0) {
                    this.buffer.delete(0, this.buffer.length());
                }
                return body2;
            } catch (IOException e2) {
                throw new ResponseException(e2);
            } catch (Throwable th) {
                if (entity != null) {
                    try {
                        entity.consumeContent();
                    } catch (Exception e3) {
                        throw th;
                    }
                }
                if (stream != null) {
                    stream.close();
                }
                if (this.buffer != null && this.buffer.length() > 0) {
                    this.buffer.delete(0, this.buffer.length());
                }
                throw th;
            }
        }
    }

    class AssetHandler implements Handler<Asset> {
        ProgressCallback progressCallback;

        public AssetHandler(ProgressCallback progressCallback2) {
            this.progressCallback = progressCallback2;
        }

        /* JADX WARNING: Removed duplicated region for block: B:25:0x00af A[SYNTHETIC, Splitter:B:25:0x00af] */
        /* JADX WARNING: Removed duplicated region for block: B:28:0x00b4 A[Catch:{ Exception -> 0x01e4 }] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public com.gaoding.dingcan.http.Asset handleResponse(org.apache.http.HttpResponse r27) throws com.gaoding.dingcan.http.ResponseException {
            /*
                r26 = this;
                org.apache.http.HttpEntity r12 = r27.getEntity()
                r18 = 0
                r5 = 0
                java.io.InputStream r18 = r12.getContent()     // Catch:{ IOException -> 0x01e7 }
                r14 = 0
                com.gaoding.dingcan.http.Asset r6 = new com.gaoding.dingcan.http.Asset     // Catch:{ IOException -> 0x01e7 }
                r6.<init>()     // Catch:{ IOException -> 0x01e7 }
                r13 = 0
                java.lang.String r21 = "Content-Encoding"
                r0 = r27
                r1 = r21
                boolean r21 = r0.containsHeader(r1)     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                if (r21 == 0) goto L_0x0035
                java.lang.String r21 = "Content-Encoding"
                r0 = r27
                r1 = r21
                org.apache.http.Header r11 = r0.getFirstHeader(r1)     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                java.lang.String r21 = r11.getValue()     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                java.lang.String r22 = "gzip"
                boolean r21 = r21.contains(r22)     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                if (r21 == 0) goto L_0x0035
                r13 = 1
            L_0x0035:
                if (r13 == 0) goto L_0x00b8
                java.io.BufferedInputStream r14 = new java.io.BufferedInputStream     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                java.util.zip.GZIPInputStream r21 = new java.util.zip.GZIPInputStream     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                com.gaoding.dingcan.http.InputStreamProxy r22 = new com.gaoding.dingcan.http.InputStreamProxy     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                r0 = r26
                com.gaoding.dingcan.http.ProgressCallback r0 = r0.progressCallback     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                r23 = r0
                long r24 = r12.getContentLength()     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                r0 = r22
                r1 = r18
                r2 = r23
                r3 = r24
                r0.<init>(r1, r2, r3)     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                r21.<init>(r22)     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                r22 = 8192(0x2000, float:1.14794E-41)
                r0 = r21
                r1 = r22
                r14.<init>(r0, r1)     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
            L_0x005e:
                java.lang.String r21 = "Content-Range"
                r0 = r27
                r1 = r21
                boolean r21 = r0.containsHeader(r1)     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                if (r21 == 0) goto L_0x019e
                java.lang.String r21 = "Content-Range"
                r0 = r27
                r1 = r21
                org.apache.http.Header r16 = r0.getFirstHeader(r1)     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                java.lang.String r21 = r16.getValue()     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                java.lang.String r22 = "[\\s-/]"
                java.lang.String[] r15 = r21.split(r22)     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                int r0 = r15.length     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                r21 = r0
                r22 = 4
                r0 = r21
                r1 = r22
                if (r0 == r1) goto L_0x00c4
                com.gaoding.dingcan.http.ResponseException r21 = new com.gaoding.dingcan.http.ResponseException     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                java.lang.StringBuilder r22 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                java.lang.String r23 = "Invalid Content-Range header: "
                r22.<init>(r23)     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                java.lang.String r23 = r16.getValue()     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                java.lang.StringBuilder r22 = r22.append(r23)     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                java.lang.String r22 = r22.toString()     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                r21.<init>(r22)     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                throw r21     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
            L_0x00a2:
                r10 = move-exception
                r5 = r6
            L_0x00a4:
                com.gaoding.dingcan.http.ResponseException r21 = new com.gaoding.dingcan.http.ResponseException     // Catch:{ all -> 0x00ac }
                r0 = r21
                r0.<init>(r10)     // Catch:{ all -> 0x00ac }
                throw r21     // Catch:{ all -> 0x00ac }
            L_0x00ac:
                r21 = move-exception
            L_0x00ad:
                if (r18 == 0) goto L_0x00b2
                r18.close()     // Catch:{ Exception -> 0x01e4 }
            L_0x00b2:
                if (r12 == 0) goto L_0x00b7
                r12.consumeContent()     // Catch:{ Exception -> 0x01e4 }
            L_0x00b7:
                throw r21
            L_0x00b8:
                java.io.BufferedInputStream r14 = new java.io.BufferedInputStream     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                r21 = 8192(0x2000, float:1.14794E-41)
                r0 = r18
                r1 = r21
                r14.<init>(r0, r1)     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                goto L_0x005e
            L_0x00c4:
                r21 = 3
                r21 = r15[r21]     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                long r21 = java.lang.Long.parseLong(r21)     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                r0 = r21
                r6.setTotalSize(r0)     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                r21 = 2
                r21 = r15[r21]     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                long r21 = java.lang.Long.parseLong(r21)     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                r0 = r21
                r6.setEndByte(r0)     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                r21 = 1
                r21 = r15[r21]     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                long r21 = java.lang.Long.parseLong(r21)     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                r0 = r21
                r6.setStartByte(r0)     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
            L_0x00eb:
                java.lang.String r21 = ""
                r0 = r21
                r6.setFileName(r0)     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                java.lang.String r21 = "Content-Disposition"
                r0 = r27
                r1 = r21
                boolean r21 = r0.containsHeader(r1)     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                if (r21 == 0) goto L_0x0136
                java.lang.String r21 = "Content-Disposition"
                r0 = r27
                r1 = r21
                org.apache.http.Header r9 = r0.getFirstHeader(r1)     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                java.lang.String r21 = r9.getValue()     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                java.lang.String r22 = "="
                java.lang.String[] r15 = r21.split(r22)     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                int r0 = r15.length     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                r21 = r0
                r22 = 2
                r0 = r21
                r1 = r22
                if (r0 != r1) goto L_0x0136
                r21 = 1
                r21 = r15[r21]     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                r22 = 1
                r23 = 1
                r23 = r15[r23]     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                int r23 = r23.length()     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                int r23 = r23 + -1
                java.lang.String r21 = r21.substring(r22, r23)     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                r0 = r21
                r6.setFileName(r0)     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
            L_0x0136:
                long r21 = r6.getEndByte()     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                long r23 = r6.getStartByte()     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                long r21 = r21 - r23
                r23 = 1
                long r21 = r21 + r23
                r0 = r21
                int r7 = (int) r0     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                if (r7 >= 0) goto L_0x014a
                r7 = 0
            L_0x014a:
                byte[] r8 = new byte[r7]     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                r17 = 0
                r20 = 0
                org.apache.http.StatusLine r21 = r27.getStatusLine()     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                int r21 = r21.getStatusCode()     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                r22 = 200(0xc8, float:2.8E-43)
                r0 = r21
                r1 = r22
                if (r0 == r1) goto L_0x01c1
                org.apache.http.StatusLine r21 = r27.getStatusLine()     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                int r21 = r21.getStatusCode()     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                r22 = 206(0xce, float:2.89E-43)
                r0 = r21
                r1 = r22
                if (r0 == r1) goto L_0x01c1
                r19 = 0
            L_0x0172:
                int r0 = r8.length     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                r21 = r0
                r0 = r20
                r1 = r21
                if (r0 >= r1) goto L_0x0190
                int r0 = r8.length     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                r21 = r0
                int r21 = r21 - r20
                r0 = r20
                r1 = r21
                int r17 = r14.read(r8, r0, r1)     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                r21 = -1
                r0 = r17
                r1 = r21
                if (r0 != r1) goto L_0x01c4
            L_0x0190:
                r6.setData(r8)     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                if (r18 == 0) goto L_0x0198
                r18.close()     // Catch:{ Exception -> 0x01e2 }
            L_0x0198:
                if (r12 == 0) goto L_0x019d
                r12.consumeContent()     // Catch:{ Exception -> 0x01e2 }
            L_0x019d:
                return r6
            L_0x019e:
                long r21 = r12.getContentLength()     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                r0 = r21
                r6.setTotalSize(r0)     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                r21 = 0
                r0 = r21
                r6.setStartByte(r0)     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                long r21 = r6.getTotalSize()     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                r23 = 1
                long r21 = r21 - r23
                r0 = r21
                r6.setEndByte(r0)     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                goto L_0x00eb
            L_0x01bd:
                r21 = move-exception
                r5 = r6
                goto L_0x00ad
            L_0x01c1:
                r19 = 1
                goto L_0x0172
            L_0x01c4:
                int r20 = r20 + r17
                if (r19 == 0) goto L_0x0172
                r0 = r26
                com.gaoding.dingcan.http.ProgressCallback r0 = r0.progressCallback     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                r21 = r0
                if (r21 == 0) goto L_0x0172
                r0 = r26
                com.gaoding.dingcan.http.ProgressCallback r0 = r0.progressCallback     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                r21 = r0
                r0 = r20
                long r0 = (long) r0     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                r22 = r0
                long r0 = (long) r7     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                r24 = r0
                r21.transferred(r22, r24)     // Catch:{ IOException -> 0x00a2, all -> 0x01bd }
                goto L_0x0172
            L_0x01e2:
                r21 = move-exception
                goto L_0x019d
            L_0x01e4:
                r22 = move-exception
                goto L_0x00b7
            L_0x01e7:
                r10 = move-exception
                goto L_0x00a4
            */
            throw new UnsupportedOperationException("Method not decompiled: com.gaoding.dingcan.http.HttpClientProxy.AssetHandler.handleResponse(org.apache.http.HttpResponse):com.gaoding.dingcan.http.Asset");
        }
    }

    public String getCookie(String key) {
        for (Cookie cookie : this.client.getCookieStore().getCookies()) {
            if (cookie.getName().equalsIgnoreCase(key)) {
                return cookie.getValue();
            }
        }
        return null;
    }

    public void setCookie(String key, String value, String domain, String path, Date expiryDate) {
        CookieStore cookieStore = this.client.getCookieStore();
        BasicClientCookie cookie = new BasicClientCookie(key, value);
        if (domain != null) {
            cookie.setDomain(domain);
        }
        if (path != null) {
            cookie.setPath(path);
        }
        if (expiryDate != null) {
            cookie.setExpiryDate(expiryDate);
        }
        cookieStore.addCookie(cookie);
    }

    public void clearCookies() {
        this.client.getCookieStore().clear();
    }

    private String queryString(String url, Map<String, String> parameters) {
        StringBuilder fullUrl = new StringBuilder(url);
        if (parameters != null && parameters.keySet().size() > 0) {
            if (!url.contains("?")) {
                fullUrl.append("?");
            }
            int count = 0;
            for (String key : parameters.keySet()) {
                if (parameters.get(key) != null) {
                    fullUrl.append(URLEncoder.encode(key));
                    fullUrl.append("=");
                    fullUrl.append(URLEncoder.encode(parameters.get(key)));
                    count++;
                    if (count < parameters.keySet().size()) {
                        fullUrl.append("&");
                    }
                }
            }
        }
        return fullUrl.toString();
    }

    private HttpEntity encodedFormEntity(Map<String, String> parameters) {
        List<NameValuePair> pairs = new ArrayList<>();
        if (parameters == null) {
            return null;
        }
        for (String key : parameters.keySet()) {
            pairs.add(new BasicNameValuePair(key, parameters.get(key)));
        }
        try {
            return new UrlEncodedFormEntity(pairs, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return null;
        }
    }

    public HttpResult<String> post(String url, HttpEntity httpEntity) throws ConnectionException, Exception {
        LogUtils.logDebug("***url=" + url);
        LogUtils.logDebug(httpEntity.getContent());
        HttpPost post = new HttpPost(url);
        post.setEntity(httpEntity);
        return new RequestExecutor<>(post, new StringHandler()).execute();
    }

    public HttpResponse httpPost(String url, HttpEntity httpEntity) throws ConnectionException, Exception {
        LogUtils.logDebug("***url=" + url);
        LogUtils.logDebug(httpEntity.getContent());
        HttpPost post = new HttpPost(url);
        post.setEntity(httpEntity);
        return new DefaultHttpClient().execute(post);
    }

    public static StringEntity buildStringEntity(String xml) {
        try {
            StringEntity reqEntity = new StringEntity(xml, "UTF-8");
            reqEntity.setContentType("application/xml");
            reqEntity.setContentEncoding("UTF-8");
            return reqEntity;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
