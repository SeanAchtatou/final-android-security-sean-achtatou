package com.gaoding.dingcan.http;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import org.json.JSONObject;

public class SimpleHttpClient {
    private static final int CONCURRENCY = 10;
    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int SOCKET_TIMEOUT = 15000;
    private static final String TAG = "SimpleHttpClient";

    public static String httpUrlConnection(String path, JSONObject json) {
        try {
            HttpURLConnection httpConn = (HttpURLConnection) new URL(path).openConnection();
            httpConn.setRequestMethod("POST");
            httpConn.setDoOutput(true);
            httpConn.setDoInput(true);
            httpConn.setRequestProperty("Content-Type", "application/json");
            httpConn.setRequestProperty("Charset", "UTF-8");
            httpConn.connect();
            DataOutputStream out = new DataOutputStream(httpConn.getOutputStream());
            out.writeBytes(json.toString());
            out.flush();
            out.close();
            if (200 == httpConn.getResponseCode()) {
                StringBuffer sb = new StringBuffer();
                BufferedReader responseReader = new BufferedReader(new InputStreamReader(httpConn.getInputStream(), "UTF-8"));
                while (true) {
                    String readLine = responseReader.readLine();
                    if (readLine == null) {
                        responseReader.close();
                        httpConn.disconnect();
                        return sb.toString();
                    }
                    sb.append(readLine).append("\n");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
