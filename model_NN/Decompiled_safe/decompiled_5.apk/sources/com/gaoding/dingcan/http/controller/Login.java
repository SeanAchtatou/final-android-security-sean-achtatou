package com.gaoding.dingcan.http.controller;

import android.content.Context;
import com.gaoding.dingcan.AppConfig;
import com.gaoding.dingcan.database.DataStore;
import com.gaoding.dingcan.database.controller.UserAccount;
import com.gaoding.dingcan.database.controller.UserInfo;
import com.gaoding.dingcan.http.HttpResult;
import com.gaoding.dingcan.http.ProxyFactory;
import com.gaoding.dingcan.util.LogUtils;
import java.util.HashMap;
import org.json.JSONException;
import org.json.JSONObject;

public class Login {
    private String ACTION = "login";
    public int code;
    public String message;
    public String returnAction;
    public boolean status = false;
    public UserAccount userAccount;
    public UserInfo userInfo;

    public Login(Context context) {
        this.userAccount = new UserAccount(context);
        this.userInfo = new UserInfo(context);
        this.userAccount.getFromDatabase();
        this.userInfo.getFromDatabase();
    }

    public void close() {
        this.userAccount.close();
        this.userInfo.close();
    }

    public boolean UserAutoLogin() {
        this.userAccount.getFromDatabase();
        return UserLogin(this.userAccount.Item.mAccount, this.userAccount.Item.mPassword);
    }

    public boolean UserLogin(String account, String password) {
        this.userAccount.Item.mAccount = account;
        this.userAccount.Item.mPassword = password;
        boolean result = requestHttp();
        if (result) {
            this.userAccount.setToDatabase();
            this.userInfo.setToDatabase();
        }
        return result;
    }

    private HashMap<String, String> putParameter() {
        HashMap<String, String> mHashMapParameter = new HashMap<>();
        mHashMapParameter.put("action", this.ACTION);
        mHashMapParameter.put(DataStore.UserTable.USER_ACCOUNT, this.userAccount.Item.mAccount);
        mHashMapParameter.put(DataStore.UserTable.USER_PASSWORD, this.userAccount.Item.mPassword);
        return mHashMapParameter;
    }

    private boolean parserJson(String retSrc) {
        LogUtils.logDebug("parserJson:" + retSrc);
        try {
            JSONObject result = new JSONObject(retSrc);
            if (result.has(DataStore.UserInfoTable.USER_STATUS)) {
                this.status = result.getBoolean(DataStore.UserInfoTable.USER_STATUS);
            }
            if (this.status) {
                JSONObject jsonObjectMessage = (JSONObject) result.getJSONArray("memberInfo").get(0);
                LogUtils.logDebug("register status = true");
                if (jsonObjectMessage.has("memberId")) {
                    this.userInfo.Item.mUid = jsonObjectMessage.getString("memberId");
                }
                if (jsonObjectMessage.has("memberEmail")) {
                    this.userInfo.Item.mEmail = jsonObjectMessage.getString("memberEmail");
                }
                if (jsonObjectMessage.has("memberPhone")) {
                    this.userInfo.Item.mPhone = jsonObjectMessage.getString("memberPhone");
                }
                if (jsonObjectMessage.has("memberNickName")) {
                    this.userInfo.Item.mNickName = jsonObjectMessage.getString("memberNickName");
                }
                if (jsonObjectMessage.has("memberIntegration")) {
                    this.userInfo.Item.mIntegration = jsonObjectMessage.getString("memberIntegration");
                }
                if (jsonObjectMessage.has("memberSECode")) {
                    this.userInfo.Item.mSecode = jsonObjectMessage.getString("memberSECode");
                }
                if (jsonObjectMessage.has("memberCategory")) {
                    this.userInfo.Item.mCategory = jsonObjectMessage.getString("memberCategory");
                }
                if (jsonObjectMessage.has("memberStatus")) {
                    this.userInfo.Item.mCategory = jsonObjectMessage.getString("memberStatus");
                }
                if (jsonObjectMessage.has("memberHeadPhoto")) {
                    this.userInfo.Item.mStatus = jsonObjectMessage.getString("memberHeadPhoto");
                }
                if (jsonObjectMessage.has("memberSex")) {
                    this.userInfo.Item.mSex = jsonObjectMessage.getString("memberSex");
                }
                if (jsonObjectMessage.has("memberRegisterDate")) {
                    this.userInfo.Item.mDetetime = jsonObjectMessage.getString("memberRegisterDate");
                }
                return true;
            }
            LogUtils.logDebug("register status = false");
            if (result.has("code")) {
                this.code = result.getInt("code");
            }
            if (!result.has("message")) {
                return false;
            }
            this.message = result.getString("message");
            return false;
        } catch (JSONException ex) {
            throw new RuntimeException(ex);
        }
    }

    public boolean requestHttp() {
        try {
            HttpResult<String> response = ProxyFactory.getDefaultProxy().post(AppConfig.LOGIN_URL, putParameter());
            if (response.getCode() != 200) {
                LogUtils.logDebug("http return false, code=" + response.getCode());
                return false;
            } else if (parserJson(response.getValue())) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
