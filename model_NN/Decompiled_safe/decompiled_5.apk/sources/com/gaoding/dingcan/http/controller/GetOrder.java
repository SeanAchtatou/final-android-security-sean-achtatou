package com.gaoding.dingcan.http.controller;

import android.content.Context;
import com.gaoding.dingcan.AppConfig;
import com.gaoding.dingcan.database.DataStore;
import com.gaoding.dingcan.database.bean.OrderBean;
import com.gaoding.dingcan.http.HttpResult;
import com.gaoding.dingcan.http.ProxyFactory;
import com.gaoding.dingcan.util.LogUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GetOrder {
    private String ACTION = "getOrder";
    public int code;
    public List<OrderBean> list = new ArrayList();
    public HashMap<String, String> mHashMapParameter = new HashMap<>();
    public String mMemberId;
    public String message;
    public String returnAction;
    public boolean status = false;

    public GetOrder(Context context) {
    }

    public void close() {
    }

    public boolean GetTodayList() {
        return requestTodayHttp();
    }

    public boolean GetAllList() {
        return requestAllHttp();
    }

    private HashMap<String, String> putParameter() {
        HashMap<String, String> mHashMapParameter2 = new HashMap<>();
        mHashMapParameter2.put("action", this.ACTION);
        mHashMapParameter2.put("memberId", this.mMemberId);
        return mHashMapParameter2;
    }

    private boolean parserTodayJson(String retSrc) {
        LogUtils.logDebug("parserJson:" + retSrc);
        try {
            JSONObject result = new JSONObject(retSrc);
            if (result.has(DataStore.UserInfoTable.USER_STATUS)) {
                this.status = result.getBoolean(DataStore.UserInfoTable.USER_STATUS);
            }
            if (this.status) {
                LogUtils.logDebug("status = true");
                this.list.clear();
                JSONArray jsonArrayMessage = result.getJSONArray("orderListToday");
                for (int i = 0; i < jsonArrayMessage.length(); i++) {
                    JSONObject jsonObjectMessage = (JSONObject) jsonArrayMessage.get(i);
                    OrderBean item = new OrderBean();
                    if (jsonObjectMessage.has("ordersId")) {
                        item.mId = jsonObjectMessage.getString("ordersId");
                    }
                    if (jsonObjectMessage.has("ordersTime")) {
                        item.mTime = jsonObjectMessage.getString("ordersTime");
                    }
                    if (jsonObjectMessage.has("orderAddress")) {
                        item.mAddress = jsonObjectMessage.getString("orderAddress");
                    }
                    if (jsonObjectMessage.has("ordersState")) {
                        item.mState = jsonObjectMessage.getString("ordersState");
                    }
                    if (jsonObjectMessage.has("orderDesc")) {
                        item.mDesc = jsonObjectMessage.getString("orderDesc");
                    }
                    if (jsonObjectMessage.has("orderPhone")) {
                        item.mPhone = jsonObjectMessage.getString("orderPhone");
                    }
                    if (jsonObjectMessage.has("orderPrice")) {
                        item.mPrice = jsonObjectMessage.getString("orderPrice");
                    }
                    if (jsonObjectMessage.has("orderTotalCount")) {
                        item.mTotal = jsonObjectMessage.getString("orderTotalCount");
                    }
                    if (jsonObjectMessage.has("restaurantName")) {
                        item.mRestaurantId = jsonObjectMessage.getString("restaurantName");
                    }
                    if (jsonObjectMessage.has("orderNum")) {
                        item.mNum = jsonObjectMessage.getString("orderNum");
                    }
                    if (jsonObjectMessage.has("orderTypeFlag")) {
                        item.mType = jsonObjectMessage.getString("orderTypeFlag");
                    }
                    if (jsonObjectMessage.has("RestaurantActiveId")) {
                        item.mActiveId = jsonObjectMessage.getString("RestaurantActiveId");
                    }
                    this.list.add(item);
                }
                return true;
            }
            LogUtils.logDebug("status = false");
            if (result.has("code")) {
                this.code = result.getInt("code");
            }
            if (result.has("message")) {
                this.message = result.getString("message");
            }
            return false;
        } catch (JSONException ex) {
            throw new RuntimeException(ex);
        }
    }

    private boolean parserAllJson(String retSrc) {
        LogUtils.logDebug("parserJson:" + retSrc);
        try {
            JSONObject result = new JSONObject(retSrc);
            if (result.has(DataStore.UserInfoTable.USER_STATUS)) {
                this.status = result.getBoolean(DataStore.UserInfoTable.USER_STATUS);
            }
            if (this.status) {
                LogUtils.logDebug("status = true");
                this.list.clear();
                JSONArray jsonArrayMessage = result.getJSONArray("orderListAll");
                for (int i = 0; i < jsonArrayMessage.length(); i++) {
                    JSONObject jsonObjectMessage = (JSONObject) jsonArrayMessage.get(i);
                    OrderBean item = new OrderBean();
                    if (jsonObjectMessage.has("ordersId")) {
                        item.mId = jsonObjectMessage.getString("ordersId");
                    }
                    if (jsonObjectMessage.has("ordersTime")) {
                        item.mTime = jsonObjectMessage.getString("ordersTime");
                    }
                    if (jsonObjectMessage.has("orderAddress")) {
                        item.mAddress = jsonObjectMessage.getString("orderAddress");
                    }
                    if (jsonObjectMessage.has("ordersState")) {
                        item.mState = jsonObjectMessage.getString("ordersState");
                    }
                    if (jsonObjectMessage.has("orderDesc")) {
                        item.mDesc = jsonObjectMessage.getString("orderDesc");
                    }
                    if (jsonObjectMessage.has("orderPhone")) {
                        item.mPhone = jsonObjectMessage.getString("orderPhone");
                    }
                    if (jsonObjectMessage.has("orderPrice")) {
                        item.mPrice = jsonObjectMessage.getString("orderPrice");
                    }
                    if (jsonObjectMessage.has("orderTotalCount")) {
                        item.mTotal = jsonObjectMessage.getString("orderTotalCount");
                    }
                    if (jsonObjectMessage.has("restaurantName")) {
                        item.mRestaurantId = jsonObjectMessage.getString("restaurantName");
                    }
                    if (jsonObjectMessage.has("orderNum")) {
                        item.mNum = jsonObjectMessage.getString("orderNum");
                    }
                    if (jsonObjectMessage.has("orderTypeFlag")) {
                        item.mType = jsonObjectMessage.getString("orderTypeFlag");
                    }
                    if (jsonObjectMessage.has("RestaurantActiveId")) {
                        item.mActiveId = jsonObjectMessage.getString("RestaurantActiveId");
                    }
                    this.list.add(item);
                }
                return true;
            }
            LogUtils.logDebug("status = false");
            if (result.has("code")) {
                this.code = result.getInt("code");
            }
            if (result.has("message")) {
                this.message = result.getString("message");
            }
            return false;
        } catch (JSONException ex) {
            throw new RuntimeException(ex);
        }
    }

    public boolean requestTodayHttp() {
        try {
            HttpResult<String> response = ProxyFactory.getDefaultProxy().get(AppConfig.TODAY_ORDER_URL, putParameter());
            if (response.getCode() != 200) {
                LogUtils.logDebug("http return false, code=" + response.getCode());
                return false;
            } else if (parserTodayJson(response.getValue())) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean requestAllHttp() {
        try {
            HttpResult<String> response = ProxyFactory.getDefaultProxy().get(AppConfig.ALL_ORDER_URL, putParameter());
            if (response.getCode() != 200) {
                LogUtils.logDebug("http return false, code=" + response.getCode());
                return false;
            } else if (parserAllJson(response.getValue())) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
