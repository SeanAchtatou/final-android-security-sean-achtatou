package com.gaoding.dingcan.http.controller;

import android.content.Context;
import com.gaoding.dingcan.AppConfig;
import com.gaoding.dingcan.database.DataStore;
import com.gaoding.dingcan.database.bean.UnitBean;
import com.gaoding.dingcan.database.controller.Unit;
import com.gaoding.dingcan.http.HttpResult;
import com.gaoding.dingcan.http.ProxyFactory;
import com.gaoding.dingcan.util.LogUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GetUnit {
    private String ACTION = "getUnit";
    public int code;
    public List<UnitBean> list;
    public String mAreaId;
    public HashMap<String, String> mHashMapParameter = new HashMap<>();
    public String message;
    public String returnAction;
    public boolean status = false;
    public Unit unit;

    public GetUnit(Context context) {
        this.unit = new Unit(context);
        this.unit.getFromDatabase();
        this.list = new ArrayList();
    }

    public void close() {
        this.unit.close();
    }

    public boolean GetList() {
        boolean result = requestHttp();
        if (result) {
            this.unit.syncToDatabase(this.list);
        }
        return result;
    }

    private HashMap<String, String> putParameter() {
        HashMap<String, String> mHashMapParameter2 = new HashMap<>();
        mHashMapParameter2.put("action", this.ACTION);
        mHashMapParameter2.put("areaId", this.mAreaId);
        return mHashMapParameter2;
    }

    private boolean parserJson(String retSrc) {
        LogUtils.logDebug("parserJson:" + retSrc);
        try {
            JSONObject result = new JSONObject(retSrc);
            if (result.has(DataStore.UserInfoTable.USER_STATUS)) {
                this.status = result.getBoolean(DataStore.UserInfoTable.USER_STATUS);
            }
            if (this.status) {
                LogUtils.logDebug("register status = true");
                JSONArray jsonArrayMessage = result.getJSONArray("areaUnitList");
                for (int i = 0; i < jsonArrayMessage.length(); i++) {
                    JSONObject jsonObjectMessage = (JSONObject) jsonArrayMessage.get(i);
                    UnitBean item = new UnitBean();
                    if (jsonObjectMessage.has("areaUnitId")) {
                        item.mId = jsonObjectMessage.getString("areaUnitId");
                    }
                    if (jsonObjectMessage.has("areaUnitName")) {
                        item.mName = jsonObjectMessage.getString("areaUnitName");
                    }
                    if (jsonObjectMessage.has("areaUnitDesc")) {
                        item.mDesc = jsonObjectMessage.getString("areaUnitDesc");
                    }
                    if (jsonObjectMessage.has("areaUnitXSize")) {
                        item.mX = jsonObjectMessage.getString("areaUnitXSize");
                    }
                    if (jsonObjectMessage.has("areaUnitYSize")) {
                        item.mY = jsonObjectMessage.getString("areaUnitYSize");
                    }
                    this.list.add(item);
                }
                return true;
            }
            LogUtils.logDebug("city status = false");
            if (result.has("code")) {
                this.code = result.getInt("code");
            }
            if (result.has("message")) {
                this.message = result.getString("message");
            }
            return false;
        } catch (JSONException ex) {
            throw new RuntimeException(ex);
        }
    }

    public boolean requestHttp() {
        try {
            HttpResult<String> response = ProxyFactory.getDefaultProxy().post(AppConfig.UNIT_URL, putParameter());
            if (response.getCode() != 200) {
                LogUtils.logDebug("http return false, code=" + response.getCode());
                return false;
            } else if (parserJson(response.getValue())) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
