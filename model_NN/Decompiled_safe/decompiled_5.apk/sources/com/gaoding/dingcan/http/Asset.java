package com.gaoding.dingcan.http;

public class Asset {
    private byte[] data;
    private long endByte;
    private String fileName;
    private long startByte;
    private long totalSize;

    public Asset() {
    }

    public Asset(byte[] data2, long totalSize2, long startByte2, long endByte2, String fileName2) {
        this.data = data2;
        this.totalSize = totalSize2;
        this.startByte = startByte2;
        this.endByte = endByte2;
        this.fileName = fileName2;
    }

    public byte[] getData() {
        return this.data;
    }

    public long getTotalSize() {
        return this.totalSize;
    }

    public long getStartByte() {
        return this.startByte;
    }

    public long getEndByte() {
        return this.endByte;
    }

    public String getFileName() {
        return this.fileName;
    }

    /* access modifiers changed from: protected */
    public void setData(byte[] data2) {
        this.data = data2;
    }

    /* access modifiers changed from: protected */
    public void setTotalSize(long totalSize2) {
        this.totalSize = totalSize2;
    }

    /* access modifiers changed from: protected */
    public void setStartByte(long startByte2) {
        this.startByte = startByte2;
    }

    /* access modifiers changed from: protected */
    public void setEndByte(long endByte2) {
        this.endByte = endByte2;
    }

    /* access modifiers changed from: protected */
    public void setFileName(String fileName2) {
        this.fileName = fileName2;
    }
}
