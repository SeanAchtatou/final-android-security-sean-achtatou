package com.gaoding.dingcan.http.controller;

import android.content.Context;
import com.gaoding.dingcan.AppConfig;
import com.gaoding.dingcan.database.DataStore;
import com.gaoding.dingcan.database.bean.AreaBean;
import com.gaoding.dingcan.database.bean.UnitBean;
import com.gaoding.dingcan.http.HttpResult;
import com.gaoding.dingcan.http.ProxyFactory;
import com.gaoding.dingcan.util.LogUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GetAreaUnit {
    private String ACTION = "getAreaUnit";
    public int code;
    public List<AreaBean> listArea = new ArrayList();
    public List<UnitBean> listUnit = new ArrayList();
    public String mCityId;
    public HashMap<String, String> mHashMapParameter;
    public String message;
    public String returnAction;
    public boolean status = false;

    public GetAreaUnit(Context context) {
    }

    public void close() {
    }

    public boolean GetList() {
        return requestHttp();
    }

    private HashMap<String, String> putParameter() {
        HashMap<String, String> mHashMapParameter2 = new HashMap<>();
        mHashMapParameter2.put("action", this.ACTION);
        mHashMapParameter2.put("cityId", this.mCityId);
        return mHashMapParameter2;
    }

    private HashMap<String, String> putParameter(String mAreaId) {
        HashMap<String, String> mHashMapParameter2 = new HashMap<>();
        mHashMapParameter2.put("action", this.ACTION);
        mHashMapParameter2.put("areaId", mAreaId);
        return mHashMapParameter2;
    }

    private boolean parserAreaJson(String retSrc) {
        LogUtils.logDebug("parserJson:" + retSrc);
        try {
            JSONObject result = new JSONObject(retSrc);
            if (result.has(DataStore.UserInfoTable.USER_STATUS)) {
                this.status = result.getBoolean(DataStore.UserInfoTable.USER_STATUS);
            }
            if (this.status) {
                LogUtils.logDebug("register status = true");
                JSONArray jsonArrayMessage = result.getJSONArray("areaList");
                for (int i = 0; i < jsonArrayMessage.length(); i++) {
                    JSONObject jsonObjectMessage = (JSONObject) jsonArrayMessage.get(i);
                    AreaBean item = new AreaBean();
                    if (jsonObjectMessage.has("areaId")) {
                        item.mId = jsonObjectMessage.getString("areaId");
                    }
                    if (jsonObjectMessage.has("areaName")) {
                        item.mName = jsonObjectMessage.getString("areaName");
                    }
                    if (jsonObjectMessage.has("areaDesc")) {
                        item.mDesc = jsonObjectMessage.getString("areaDesc");
                    }
                    if (jsonObjectMessage.has("areaXSize")) {
                        item.mX = jsonObjectMessage.getString("areaXSize");
                    }
                    if (jsonObjectMessage.has("areaYSize")) {
                        item.mY = jsonObjectMessage.getString("areaYSize");
                    }
                    if (jsonObjectMessage.has("areaIsNew")) {
                        item.mIsNew = jsonObjectMessage.getString("areaIsNew");
                    }
                    LogUtils.logDebug("mId=" + item.mId);
                    LogUtils.logDebug("mName=" + item.mName);
                    LogUtils.logDebug("mDesc=" + item.mDesc);
                    LogUtils.logDebug("mX=" + item.mX);
                    LogUtils.logDebug("mY=" + item.mY);
                    this.listArea.add(item);
                }
                return true;
            }
            LogUtils.logDebug("city status = false");
            if (result.has("code")) {
                this.code = result.getInt("code");
            }
            if (result.has("message")) {
                this.message = result.getString("message");
            }
            return false;
        } catch (JSONException ex) {
            throw new RuntimeException(ex);
        }
    }

    private boolean parserUnitJson(String retSrc) {
        LogUtils.logDebug("parserJson:" + retSrc);
        try {
            JSONObject result = new JSONObject(retSrc);
            if (result.has(DataStore.UserInfoTable.USER_STATUS)) {
                this.status = result.getBoolean(DataStore.UserInfoTable.USER_STATUS);
            }
            if (this.status) {
                LogUtils.logDebug("register status = true");
                JSONArray jsonArrayMessage = result.getJSONArray("areaUnitList");
                for (int i = 0; i < jsonArrayMessage.length(); i++) {
                    JSONObject jsonObjectMessage = (JSONObject) jsonArrayMessage.get(i);
                    UnitBean item = new UnitBean();
                    if (jsonObjectMessage.has("areaUnitId")) {
                        item.mId = jsonObjectMessage.getString("areaUnitId");
                    }
                    if (jsonObjectMessage.has("areaUnitName")) {
                        item.mName = jsonObjectMessage.getString("areaUnitName");
                    }
                    if (jsonObjectMessage.has("areaUnitDesc")) {
                        item.mDesc = jsonObjectMessage.getString("areaUnitDesc");
                    }
                    if (jsonObjectMessage.has("areaUnitXSize")) {
                        item.mX = jsonObjectMessage.getString("areaUnitXSize");
                    }
                    if (jsonObjectMessage.has("areaUnitYSize")) {
                        item.mY = jsonObjectMessage.getString("areaUnitYSize");
                    }
                    this.listUnit.add(item);
                }
                return true;
            }
            LogUtils.logDebug("city status = false");
            if (result.has("code")) {
                this.code = result.getInt("code");
            }
            if (result.has("message")) {
                this.message = result.getString("message");
            }
            return false;
        } catch (JSONException ex) {
            throw new RuntimeException(ex);
        }
    }

    public boolean requestHttp() {
        try {
            HttpResult<String> response = ProxyFactory.getDefaultProxy().post(AppConfig.AREA_URL, putParameter());
            if (response.getCode() != 200) {
                LogUtils.logDebug("http return false, code=" + response.getCode());
                return false;
            } else if (!parserAreaJson(response.getValue())) {
                return false;
            } else {
                for (int i = 0; i < this.listArea.size(); i++) {
                    HttpResult<String> response2 = ProxyFactory.getDefaultProxy().get(AppConfig.UNIT_URL, putParameter(this.listArea.get(i).mId));
                    if (response2.getCode() == 200) {
                        parserUnitJson(response2.getValue());
                    }
                }
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
