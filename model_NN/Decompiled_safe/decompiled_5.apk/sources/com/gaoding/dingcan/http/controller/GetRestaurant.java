package com.gaoding.dingcan.http.controller;

import android.content.Context;
import com.gaoding.dingcan.AppConfig;
import com.gaoding.dingcan.database.DataStore;
import com.gaoding.dingcan.database.bean.RestaurantBean;
import com.gaoding.dingcan.database.controller.Restaurant;
import com.gaoding.dingcan.http.HttpResult;
import com.gaoding.dingcan.http.ProxyFactory;
import com.gaoding.dingcan.util.LogUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GetRestaurant {
    private String ACTION = "getRestaurant";
    public int code;
    public List<RestaurantBean> list;
    public HashMap<String, String> mHashMapParameter = new HashMap<>();
    public String mUnitId;
    public String message;
    public Restaurant restaurant;
    public String returnAction;
    public boolean status = false;

    public GetRestaurant(Context context) {
        this.restaurant = new Restaurant(context);
        this.restaurant.getFromDatabase();
        this.list = new ArrayList();
    }

    public void close() {
        this.restaurant.close();
    }

    public boolean GetList() {
        boolean result = requestHttp();
        if (result) {
            this.restaurant.syncToDatabase(this.list);
        }
        return result;
    }

    private HashMap<String, String> putParameter() {
        HashMap<String, String> mHashMapParameter2 = new HashMap<>();
        mHashMapParameter2.put("action", this.ACTION);
        mHashMapParameter2.put("areaUnitId", this.mUnitId);
        return mHashMapParameter2;
    }

    private boolean parserJson(String retSrc) {
        LogUtils.logDebug("parserJson:" + retSrc);
        try {
            JSONObject result = new JSONObject(retSrc);
            if (result.has(DataStore.UserInfoTable.USER_STATUS)) {
                this.status = result.getBoolean(DataStore.UserInfoTable.USER_STATUS);
            }
            if (this.status) {
                LogUtils.logDebug("register status = true");
                JSONArray jsonArrayMessage = result.getJSONArray("restaurantList");
                for (int i = 0; i < jsonArrayMessage.length(); i++) {
                    JSONObject jsonObjectMessage = (JSONObject) jsonArrayMessage.get(i);
                    RestaurantBean item = new RestaurantBean();
                    if (jsonObjectMessage.has("restaurantId")) {
                        item.mId = jsonObjectMessage.getString("restaurantId");
                    }
                    if (jsonObjectMessage.has("restaurantName")) {
                        item.mName = jsonObjectMessage.getString("restaurantName");
                    }
                    if (jsonObjectMessage.has("restaurantAddress")) {
                        item.mAddress = jsonObjectMessage.getString("restaurantAddress");
                    }
                    if (jsonObjectMessage.has("restaurantLogo")) {
                        item.mLogo = jsonObjectMessage.getString("restaurantLogo");
                    }
                    if (jsonObjectMessage.has("restaurantState")) {
                        item.mState = jsonObjectMessage.getString("restaurantState");
                    }
                    if (jsonObjectMessage.has("restaurantHot")) {
                        item.mHot = jsonObjectMessage.getString("restaurantHot");
                    }
                    if (jsonObjectMessage.has("SendFoodSpeed")) {
                        item.mSpeed = jsonObjectMessage.getString("SendFoodSpeed");
                    }
                    this.list.add(item);
                }
                return true;
            }
            LogUtils.logDebug("status = false");
            if (result.has("code")) {
                this.code = result.getInt("code");
            }
            if (result.has("message")) {
                this.message = result.getString("message");
            }
            return false;
        } catch (JSONException ex) {
            throw new RuntimeException(ex);
        }
    }

    public boolean requestHttp() {
        try {
            HttpResult<String> response = ProxyFactory.getDefaultProxy().get(AppConfig.RESTAURANT_URL, putParameter());
            if (response.getCode() != 200) {
                LogUtils.logDebug("http return false, code=" + response.getCode());
                return false;
            } else if (parserJson(response.getValue())) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
