package com.gaoding.dingcan.http;

public class ResponseException extends ConnectionException {
    public ResponseException(String message) {
        super(message);
    }

    public ResponseException(Exception exception) {
        super(exception);
    }

    public ResponseException() {
    }
}
