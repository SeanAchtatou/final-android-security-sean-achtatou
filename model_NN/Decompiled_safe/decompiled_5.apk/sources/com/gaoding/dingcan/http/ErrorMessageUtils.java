package com.gaoding.dingcan.http;

import android.content.Context;
import com.gaoding.dingcan.R;

public class ErrorMessageUtils {
    public static String getErrorMessage(Context context, String errorCode) {
        try {
            return getErrorMessage(context, Integer.parseInt(errorCode));
        } catch (Exception e) {
            e.printStackTrace();
            return context.getResources().getString(R.string.error_code_message_unknown);
        }
    }

    public static String getErrorMessage(Context context, int errorCode) {
        String errorMessage = context.getResources().getString(R.string.error_code_message_unknown);
        switch (errorCode) {
            case -1:
                return context.getResources().getString(R.string.error_code_message_no_response);
            default:
                try {
                    return context.getResources().getString(R.string.error_code_message_unknown);
                } catch (Exception e) {
                    e.printStackTrace();
                    return errorMessage;
                }
        }
    }
}
