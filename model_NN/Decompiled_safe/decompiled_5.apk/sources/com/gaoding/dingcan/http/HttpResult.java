package com.gaoding.dingcan.http;

public class HttpResult<T> {
    private int code;
    private T value;

    public HttpResult(int code2, T value2) {
        this.code = code2;
        this.value = value2;
    }

    public int getCode() {
        return this.code;
    }

    public T getValue() {
        return this.value;
    }
}
