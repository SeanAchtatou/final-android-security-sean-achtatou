package com.gaoding.dingcan.http;

import java.io.File;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.Date;
import java.util.Map;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.conn.ConnectTimeoutException;

public interface Proxy {
    void clearCookies();

    HttpResult<Asset> download(String str, Map<String, String> map, long j, long j2, ProgressCallback progressCallback) throws ConnectionException, Exception;

    HttpResult<Asset> download(String str, Map<String, String> map, ProgressCallback progressCallback) throws ConnectionException, Exception;

    HttpResult<String> get(String str, Map<String, String> map) throws ConnectionException, Exception;

    String getCookie(String str);

    HttpResponse httpPost(String str, HttpEntity httpEntity) throws ConnectionException, SocketTimeoutException, SocketException, ConnectTimeoutException, Exception;

    HttpResult<String> post(String str, Map<String, String> map) throws ConnectionException, Exception;

    HttpResult<String> post(String str, Map<String, String> map, HttpEntity httpEntity) throws ConnectionException, Exception;

    HttpResult<String> post(String str, HttpEntity httpEntity) throws ConnectionException, SocketTimeoutException, SocketException, ConnectTimeoutException, Exception;

    void setCookie(String str, String str2, String str3, String str4, Date date);

    HttpResult<String> upload(String str, File file, Map<String, String> map, ProgressCallback progressCallback) throws ConnectionException, Exception;
}
