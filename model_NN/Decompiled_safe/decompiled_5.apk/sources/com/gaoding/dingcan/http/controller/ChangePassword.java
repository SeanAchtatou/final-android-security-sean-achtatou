package com.gaoding.dingcan.http.controller;

import android.content.Context;
import com.gaoding.dingcan.AppConfig;
import com.gaoding.dingcan.database.DataStore;
import com.gaoding.dingcan.database.controller.UserInfo;
import com.gaoding.dingcan.http.HttpResult;
import com.gaoding.dingcan.http.ProxyFactory;
import com.gaoding.dingcan.util.LogUtils;
import java.util.HashMap;
import org.json.JSONException;
import org.json.JSONObject;

public class ChangePassword {
    private String ACTION = "ChangePassword";
    public int code;
    public String message;
    public String returnAction;
    public boolean status = false;
    public String strNewPassword;
    public String strOldPassword;
    public UserInfo userInfo;

    public ChangePassword(Context context) {
        this.userInfo = new UserInfo(context);
        this.userInfo.getFromDatabase();
    }

    public void close() {
        this.userInfo.close();
    }

    public boolean changePsw() {
        return requestHttp();
    }

    private HashMap<String, String> putParameter() {
        HashMap<String, String> mHashMapParameter = new HashMap<>();
        mHashMapParameter.put("action", this.ACTION);
        mHashMapParameter.put("uid", this.userInfo.Item.mUid);
        mHashMapParameter.put("oldPassword", this.strOldPassword);
        mHashMapParameter.put("newPassword", this.strNewPassword);
        return mHashMapParameter;
    }

    private boolean parserJson(String retSrc) {
        LogUtils.logDebug("parserJson:" + retSrc);
        try {
            JSONObject result = new JSONObject(retSrc);
            if (result.has(DataStore.UserInfoTable.USER_STATUS)) {
                this.status = result.getBoolean(DataStore.UserInfoTable.USER_STATUS);
            }
            if (this.status) {
                LogUtils.logDebug("register status = true");
                return true;
            }
            LogUtils.logDebug("register status = false");
            if (result.has("code")) {
                this.code = result.getInt("code");
            }
            if (result.has("message")) {
                this.message = result.getString("message");
            }
            return false;
        } catch (JSONException ex) {
            throw new RuntimeException(ex);
        }
    }

    public boolean requestHttp() {
        try {
            HttpResult<String> response = ProxyFactory.getDefaultProxy().post(AppConfig.CHANGE_PSW_URL, putParameter());
            if (response.getCode() != 200) {
                LogUtils.logDebug("http return false, code=" + response.getCode());
                return false;
            } else if (parserJson(response.getValue())) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
