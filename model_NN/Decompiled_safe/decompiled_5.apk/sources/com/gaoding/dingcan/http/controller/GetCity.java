package com.gaoding.dingcan.http.controller;

import android.content.Context;
import com.gaoding.dingcan.AppConfig;
import com.gaoding.dingcan.database.DataStore;
import com.gaoding.dingcan.database.bean.CityBean;
import com.gaoding.dingcan.database.controller.City;
import com.gaoding.dingcan.http.HttpResult;
import com.gaoding.dingcan.http.ProxyFactory;
import com.gaoding.dingcan.util.LogUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GetCity {
    private String ACTION = "getCity";
    public City city;
    public int code;
    public List<CityBean> list;
    public HashMap<String, String> mHashMapParameter = new HashMap<>();
    public String message;
    public String returnAction;
    public boolean status = false;

    public GetCity(Context context) {
        this.city = new City(context);
        this.city.getFromDatabase();
        this.list = new ArrayList();
    }

    public void close() {
        this.city.close();
    }

    public boolean GetList() {
        boolean result = requestHttp();
        if (result) {
            this.city.syncToDatabase(this.list);
        }
        return result;
    }

    private HashMap<String, String> putParameter() {
        HashMap<String, String> mHashMapParameter2 = new HashMap<>();
        mHashMapParameter2.put("action", this.ACTION);
        return mHashMapParameter2;
    }

    private boolean parserJson(String retSrc) {
        LogUtils.logDebug("parserJson:" + retSrc);
        try {
            JSONObject result = new JSONObject(retSrc);
            if (result.has(DataStore.UserInfoTable.USER_STATUS)) {
                this.status = result.getBoolean(DataStore.UserInfoTable.USER_STATUS);
            }
            if (this.status) {
                LogUtils.logDebug("register status = true");
                JSONArray jsonArrayMessage = result.getJSONArray("cityList");
                for (int i = 0; i < jsonArrayMessage.length(); i++) {
                    JSONObject jsonObjectMessage = (JSONObject) jsonArrayMessage.get(i);
                    CityBean item = new CityBean();
                    if (jsonObjectMessage.has("cityId")) {
                        item.mId = jsonObjectMessage.getString("cityId");
                    }
                    if (jsonObjectMessage.has("cityName")) {
                        item.mName = jsonObjectMessage.getString("cityName");
                    }
                    if (jsonObjectMessage.has("cityDesc")) {
                        item.mDesc = jsonObjectMessage.getString("cityDesc");
                    }
                    if (jsonObjectMessage.has("cityXSize")) {
                        item.mX = jsonObjectMessage.getString("cityXSize");
                    }
                    if (jsonObjectMessage.has("cityYSize")) {
                        item.mY = jsonObjectMessage.getString("cityYSize");
                    }
                    this.list.add(item);
                }
                return true;
            }
            LogUtils.logDebug("city status = false");
            if (result.has("code")) {
                this.code = result.getInt("code");
            }
            if (result.has("message")) {
                this.message = result.getString("message");
            }
            return false;
        } catch (JSONException ex) {
            throw new RuntimeException(ex);
        }
    }

    public boolean requestHttp() {
        try {
            HttpResult<String> response = ProxyFactory.getDefaultProxy().post(AppConfig.CITY_URL, putParameter());
            if (response.getCode() != 200) {
                LogUtils.logDebug("http return false, code=" + response.getCode());
                return false;
            } else if (parserJson(response.getValue())) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
