package com.gaoding.dingcan.http;

import java.io.IOException;
import java.io.InputStream;

/* compiled from: HttpClientProxy */
class InputStreamProxy extends InputStream {
    private static final int GZIP_PROGRESS_INCREMENT = 4096;
    private InputStream inputStream;
    private ProgressCallback progressCallback;
    private int read = 0;
    private long total;

    public InputStreamProxy(InputStream inputStream2, ProgressCallback progressCallback2, long total2) {
        this.inputStream = inputStream2;
        this.progressCallback = progressCallback2;
        this.total = total2;
    }

    public int read() throws IOException {
        this.read++;
        if (this.read > 0 && (this.read % 4096 == 0 || ((long) this.read) == this.total)) {
            this.progressCallback.transferred((long) this.read, this.total);
        }
        return this.inputStream.read();
    }
}
