package com.gaoding.dingcan.http.controller;

import android.content.Context;
import com.gaoding.dingcan.AppConfig;
import com.gaoding.dingcan.database.DataStore;
import com.gaoding.dingcan.database.controller.UserAccount;
import com.gaoding.dingcan.database.controller.UserInfo;
import com.gaoding.dingcan.http.HttpResult;
import com.gaoding.dingcan.http.ProxyFactory;
import com.gaoding.dingcan.util.LogUtils;
import java.util.HashMap;
import org.json.JSONException;
import org.json.JSONObject;

public class Register {
    private String ACTION = "register";
    public int code;
    public HashMap<String, String> mHashMapParameter = new HashMap<>();
    public String message;
    public String returnAction;
    public boolean status = false;
    public UserAccount userAccount;
    public UserInfo userInfo;

    public Register(Context context) {
        this.userAccount = new UserAccount(context);
        this.userInfo = new UserInfo(context);
        this.userAccount.getFromDatabase();
        this.userInfo.getFromDatabase();
    }

    public void close() {
        this.userAccount.close();
        this.userInfo.close();
    }

    public boolean UserRegister(String account, String password, String email, String phone) {
        this.userAccount.Item.mAccount = account;
        this.userAccount.Item.mPassword = password;
        this.userInfo.Item.mEmail = email;
        this.userInfo.Item.mPhone = phone;
        this.userInfo.Item.mSex = GetRestaurantInfo.RES_STATE_CLOSE;
        boolean result = requestHttp();
        if (result) {
            this.userAccount.setToDatabase();
            this.userInfo.setToDatabase();
        }
        return result;
    }

    private HashMap<String, String> putParameter() {
        HashMap<String, String> mHashMapParameter2 = new HashMap<>();
        mHashMapParameter2.put("action", this.ACTION);
        mHashMapParameter2.put(DataStore.UserTable.USER_ACCOUNT, this.userAccount.Item.mAccount);
        mHashMapParameter2.put(DataStore.UserTable.USER_PASSWORD, this.userAccount.Item.mPassword);
        mHashMapParameter2.put(DataStore.UserInfoTable.USER_EMAIL, this.userInfo.Item.mEmail);
        mHashMapParameter2.put(DataStore.UserInfoTable.USER_PHONE, this.userInfo.Item.mPhone);
        mHashMapParameter2.put(DataStore.UserInfoTable.USER_SEX, this.userInfo.Item.mSex);
        return mHashMapParameter2;
    }

    private boolean parserJson(String retSrc) {
        LogUtils.logDebug("parserJson:" + retSrc);
        try {
            JSONObject result = new JSONObject(retSrc);
            if (result.has(DataStore.UserInfoTable.USER_STATUS)) {
                this.status = result.getBoolean(DataStore.UserInfoTable.USER_STATUS);
            }
            if (this.status) {
                LogUtils.logDebug("register status = true");
                if (result.has("useId")) {
                    String uid = result.getString("useId");
                    this.userAccount.Item.mUid = uid;
                    this.userInfo.Item.mUid = uid;
                }
                return true;
            }
            LogUtils.logDebug("register status = false");
            if (result.has("code")) {
                this.code = result.getInt("code");
            }
            if (result.has("message")) {
                this.message = result.getString("message");
            }
            return false;
        } catch (JSONException ex) {
            throw new RuntimeException(ex);
        }
    }

    public boolean requestHttp() {
        try {
            HttpResult<String> response = ProxyFactory.getDefaultProxy().post(AppConfig.REGISTER_URL, putParameter());
            if (response.getCode() != 200) {
                LogUtils.logDebug("register http return false, code=" + response.getCode());
                return false;
            } else if (parserJson(response.getValue())) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
