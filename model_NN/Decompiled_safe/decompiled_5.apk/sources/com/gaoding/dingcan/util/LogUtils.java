package com.gaoding.dingcan.util;

import android.util.Log;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class LogUtils {
    public static final boolean DEBUG = true;
    private static final String DEBUG_TAG = "GaoDing";

    public static void logDebug(String tag, String msg) {
        Log.d(tag, msg);
    }

    public static void logDebug(String msg) {
        Log.d(DEBUG_TAG, msg);
    }

    public static void logDebug(InputStream inputStream) {
        StringBuffer buffer = new StringBuffer();
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
            while (true) {
                String line = in.readLine();
                if (line == null) {
                    break;
                }
                buffer.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.d(DEBUG_TAG, buffer.toString());
    }
}
