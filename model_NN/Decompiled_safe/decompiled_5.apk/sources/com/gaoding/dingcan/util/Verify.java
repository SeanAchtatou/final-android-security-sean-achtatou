package com.gaoding.dingcan.util;

public class Verify {
    public static boolean isNull(String mEditText) {
        if (mEditText == null) {
            return false;
        }
        return mEditText.equals("");
    }
}
