package com.gaoding.dingcan.util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.view.Display;
import android.view.View;
import android.widget.Toast;
import com.gaoding.dingcan.AppConfig;
import com.gaoding.dingcan.http.controller.GetRestaurantInfo;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

public class Utils {
    private static String FOLDER = "/.gaoding";
    public static String HAD_LOGIN_ERROR_CODE = "44";
    public static final int IO_BUFFER_SIZE = 8192;
    public static boolean NET_WORK_STATE = true;
    public static String SESSION_TIMEOUT_ERROR_CODE = "3";
    private static String STORE_CACHE_PATH = AppConfig.STORE_CACHE_PATH;
    static Toast mOwnToast;
    static Toast mToast;
    private static String uniqueName = "Picture";

    public static String getApVersionName(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static boolean isNetWorkConnect(Context context) {
        boolean isConnect = false;
        try {
            NetworkInfo networkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            if (networkInfo != null) {
                if (!networkInfo.isAvailable() || !networkInfo.isConnected()) {
                    isConnect = false;
                } else {
                    isConnect = true;
                }
            }
            LogUtils.logDebug("****net work is connect=" + isConnect);
            if (!isConnect || !NET_WORK_STATE) {
                return false;
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean getNetWorkStatus(Context context) {
        boolean netSataus = false;
        NetworkInfo networkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (networkInfo != null) {
            netSataus = networkInfo.isAvailable() && networkInfo.isConnected();
        }
        LogUtils.logDebug("***the net status is" + netSataus);
        return netSataus;
    }

    public static boolean pingNetwork() {
        try {
            LogUtils.logDebug("*****ping net work begin");
            String result = executeCmd("ping -c 1 -w 1 baidu.com", false);
            LogUtils.logDebug("*******ping wifi result***********" + result);
            if (result.contains("rtt min/avg/max/mdev")) {
                NET_WORK_STATE = true;
                return true;
            }
            NET_WORK_STATE = false;
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static String executeCmd(String cmd, boolean sudo) {
        Process p;
        if (!sudo) {
            try {
                p = Runtime.getRuntime().exec(cmd);
            } catch (Exception e) {
                e.printStackTrace();
                return "";
            }
        } else {
            p = Runtime.getRuntime().exec(new String[]{"su", "-c", cmd});
        }
        BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
        String res = "";
        while (true) {
            String s = stdInput.readLine();
            if (s == null) {
                p.destroy();
                return res;
            }
            res = String.valueOf(res) + s + "\n";
        }
    }

    public static boolean pingNetworkHttp() {
        boolean result = false;
        try {
            HttpGet request = new HttpGet("http://www.baidu.com");
            HttpParams httpParameters = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParameters, 3000);
            int status = new DefaultHttpClient(httpParameters).execute(request).getStatusLine().getStatusCode();
            LogUtils.logDebug("*****http status=" + status);
            if (status == 200) {
                result = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            result = false;
        }
        NET_WORK_STATE = result;
        return result;
    }

    public static boolean getGpsStatus(Context context) {
        return ((LocationManager) context.getSystemService("location")).isProviderEnabled("gps");
    }

    public static long getLongDateFromStr(String timeStr) {
        Date date = null;
        try {
            date = new SimpleDateFormat("yyyy-MM-dd").parse(timeStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date.getTime();
    }

    public static String getDateFromStr(String timeStr) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        new Date();
        String dateStr = null;
        try {
            try {
                dateStr = sdf.format(new Date(Long.parseLong(timeStr)));
            } catch (Exception e) {
                e = e;
                e.printStackTrace();
                return dateStr;
            }
        } catch (Exception e2) {
            e = e2;
            e.printStackTrace();
            return dateStr;
        }
        return dateStr;
    }

    public static String getDateFromStr2(String timeStr) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            return sdf.format(sdf.parse(timeStr));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getDateFormat(long timeMillsecondes) {
        try {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date(timeMillsecondes));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getDateFormat3(long timeMillsecondes) {
        try {
            return new SimpleDateFormat("yyyy-MM-dd").format(new Date(timeMillsecondes));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getDateFormat4(long timeMillsecondes) {
        try {
            return new SimpleDateFormat("yyyy/MM/dd-HH:mm").format(new Date(timeMillsecondes));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getDateFormat6(long timeMillsecondes) {
        try {
            return new SimpleDateFormat("M月d日").format(new Date(timeMillsecondes));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getTimeFormat(long timeMillsecondes) {
        try {
            return new SimpleDateFormat("aHH:mm").format(new Date(timeMillsecondes));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getTimeFormat2(long timeMillsecondes) {
        try {
            return new SimpleDateFormat("HH:mm").format(new Date(timeMillsecondes));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Bitmap convertViewToBitmap(View view) {
        view.buildDrawingCache();
        view.setDrawingCacheEnabled(true);
        return view.getDrawingCache();
    }

    public static boolean hasMountStorage() {
        if ("mounted".equals(Environment.getExternalStorageState())) {
            return true;
        }
        return false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:35:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String saveBitmapToSdcard(java.lang.String r14, android.graphics.Bitmap r15) {
        /*
            r9 = 0
            boolean r10 = hasMountStorage()
            if (r10 != 0) goto L_0x0009
            r6 = r9
        L_0x0008:
            return r6
        L_0x0009:
            java.io.File r8 = android.os.Environment.getExternalStorageDirectory()
            r6 = 0
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            java.lang.String r11 = r8.getAbsolutePath()
            java.lang.String r11 = java.lang.String.valueOf(r11)
            r10.<init>(r11)
            java.lang.String r11 = com.gaoding.dingcan.util.Utils.FOLDER
            java.lang.StringBuilder r10 = r10.append(r11)
            java.lang.String r7 = r10.toString()
            java.io.File r1 = new java.io.File
            r1.<init>(r7)
            r4 = 0
            boolean r10 = r1.exists()     // Catch:{ Exception -> 0x0086 }
            if (r10 != 0) goto L_0x0034
            r1.mkdirs()     // Catch:{ Exception -> 0x0086 }
        L_0x0034:
            java.io.File r5 = new java.io.File     // Catch:{ Exception -> 0x0086 }
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0086 }
            java.lang.String r11 = java.lang.String.valueOf(r7)     // Catch:{ Exception -> 0x0086 }
            r10.<init>(r11)     // Catch:{ Exception -> 0x0086 }
            java.lang.String r11 = "/"
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Exception -> 0x0086 }
            java.lang.StringBuilder r10 = r10.append(r14)     // Catch:{ Exception -> 0x0086 }
            java.lang.String r11 = ".png"
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Exception -> 0x0086 }
            java.lang.String r10 = r10.toString()     // Catch:{ Exception -> 0x0086 }
            r5.<init>(r10)     // Catch:{ Exception -> 0x0086 }
            boolean r10 = r5.exists()     // Catch:{ Exception -> 0x009a }
            if (r10 == 0) goto L_0x005f
            r5.delete()     // Catch:{ Exception -> 0x009a }
        L_0x005f:
            r5.createNewFile()     // Catch:{ Exception -> 0x009a }
            r4 = r5
        L_0x0063:
            if (r4 == 0) goto L_0x0008
            r2 = 0
            java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0091 }
            r3.<init>(r4)     // Catch:{ Exception -> 0x0091 }
            android.graphics.Bitmap$CompressFormat r10 = android.graphics.Bitmap.CompressFormat.PNG     // Catch:{ Exception -> 0x0097 }
            r11 = 100
            r15.compress(r10, r11, r3)     // Catch:{ Exception -> 0x0097 }
            r3.flush()     // Catch:{ Exception -> 0x0097 }
            r3.close()     // Catch:{ Exception -> 0x0097 }
            long r10 = getUsableSpace(r8)     // Catch:{ Exception -> 0x0097 }
            long r12 = r4.getTotalSpace()     // Catch:{ Exception -> 0x0097 }
            int r10 = (r10 > r12 ? 1 : (r10 == r12 ? 0 : -1))
            if (r10 >= 0) goto L_0x008b
            r6 = r9
            goto L_0x0008
        L_0x0086:
            r0 = move-exception
        L_0x0087:
            r0.printStackTrace()
            goto L_0x0063
        L_0x008b:
            java.lang.String r6 = r4.getPath()     // Catch:{ Exception -> 0x0097 }
            goto L_0x0008
        L_0x0091:
            r0 = move-exception
        L_0x0092:
            r0.printStackTrace()
            goto L_0x0008
        L_0x0097:
            r0 = move-exception
            r2 = r3
            goto L_0x0092
        L_0x009a:
            r0 = move-exception
            r4 = r5
            goto L_0x0087
        */
        throw new UnsupportedOperationException("Method not decompiled: com.gaoding.dingcan.util.Utils.saveBitmapToSdcard(java.lang.String, android.graphics.Bitmap):java.lang.String");
    }

    public static String getDistance(double distance, int accuracy) {
        try {
            LogUtils.logDebug("****distance=" + distance);
            String format = "";
            if (accuracy >= 0) {
                format = String.valueOf(format) + ".";
                for (int i = 0; i < accuracy; i++) {
                    format = String.valueOf(format) + "#";
                }
            }
            String st = new DecimalFormat(format).format(distance);
            if (st.endsWith(".")) {
                return st.substring(0, st.lastIndexOf("."));
            }
            return st;
        } catch (Exception e) {
            return GetRestaurantInfo.RES_STATE_CLOSE;
        }
    }

    public static void showToast(Context context, String message) {
        if (mOwnToast == null) {
            mOwnToast = Toast.makeText(context.getApplicationContext(), message, 1);
        }
        mOwnToast.setText(message);
        mOwnToast.show();
    }

    public static String createSerialNumber(String phoneNumber) {
        StringBuilder buffer = new StringBuilder();
        try {
            buffer.append(phoneNumber).append("-").append(new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return buffer.toString();
    }

    public static int getBitmapSize(Bitmap bitmap) {
        int i = Build.VERSION.SDK_INT;
        return bitmap.getRowBytes() * bitmap.getHeight();
    }

    @SuppressLint({"NewApi"})
    public static boolean isExternalStorageRemovable() {
        if (Build.VERSION.SDK_INT >= 9) {
            return Environment.isExternalStorageRemovable();
        }
        return true;
    }

    public static File getExternalCacheDir(Context context) {
        return new File(String.valueOf(Environment.getExternalStorageDirectory().getPath()) + STORE_CACHE_PATH);
    }

    public static File getExternalCacheDir() {
        return new File(String.valueOf(Environment.getExternalStorageDirectory().getPath()) + STORE_CACHE_PATH);
    }

    public static File getDiskCacheDir(Context context, String uniqueName2) {
        String cachePath;
        if (Environment.getExternalStorageState() == "mounted" || !isExternalStorageRemovable()) {
            cachePath = getExternalCacheDir(context).getPath();
        } else {
            cachePath = getExternalCacheDir(context).getPath();
        }
        File file = new File(cachePath);
        if (!file.exists()) {
            file.mkdirs();
        }
        return new File(String.valueOf(cachePath) + File.separator + uniqueName2);
    }

    public static File getDiskCacheDir(Context context) {
        String cachePath;
        if (Environment.getExternalStorageState() == "mounted" || !isExternalStorageRemovable()) {
            cachePath = getExternalCacheDir(context).getPath();
        } else {
            cachePath = getExternalCacheDir(context).getPath();
        }
        File file = new File(String.valueOf(cachePath) + File.separator + uniqueName);
        if (!file.exists()) {
            file.mkdirs();
        }
        return file;
    }

    public static void createFilePath(Context context, String uniqueName2) {
        File f = getDiskCacheDir(context, uniqueName2);
        if (!f.exists()) {
            f.mkdirs();
        }
    }

    public static void createFilePath(Context context) {
        File f = getDiskCacheDir(context);
        if (!f.exists()) {
            f.mkdirs();
        }
    }

    public static String getStorePath(Context context) {
        File f = getDiskCacheDir(context);
        if (f.exists()) {
            return f.getAbsolutePath();
        }
        return "";
    }

    @SuppressLint({"NewApi"})
    public static long getUsableSpace(File path) {
        if (Build.VERSION.SDK_INT >= 9) {
            return path.getUsableSpace();
        }
        StatFs stats = new StatFs(path.getPath());
        return ((long) stats.getBlockSize()) * ((long) stats.getAvailableBlocks());
    }

    public static int getMemoryClass(Context context) {
        return ((ActivityManager) context.getSystemService("activity")).getMemoryClass();
    }

    public static boolean hasHttpConnectionBug() {
        return Build.VERSION.SDK_INT < 8;
    }

    public static boolean hasExternalCacheDir() {
        return Build.VERSION.SDK_INT >= 8;
    }

    public static boolean hasActionBar() {
        return Build.VERSION.SDK_INT >= 11;
    }

    public static String getAPKVersionName(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (Exception e) {
            return null;
        }
    }

    public static int getAPKVersionCode(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:35:0x007c A[SYNTHETIC, Splitter:B:35:0x007c] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void writeToSdcard(java.io.InputStream r8, java.lang.String r9, java.lang.String r10) {
        /*
            r4 = 0
            if (r8 == 0) goto L_0x003c
            java.io.File r3 = new java.io.File     // Catch:{ Exception -> 0x008d }
            r3.<init>(r9)     // Catch:{ Exception -> 0x008d }
            r3.mkdirs()     // Catch:{ Exception -> 0x008d }
            java.io.File r3 = new java.io.File     // Catch:{ Exception -> 0x008d }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x008d }
            java.lang.String r7 = java.lang.String.valueOf(r9)     // Catch:{ Exception -> 0x008d }
            r6.<init>(r7)     // Catch:{ Exception -> 0x008d }
            java.lang.StringBuilder r6 = r6.append(r10)     // Catch:{ Exception -> 0x008d }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x008d }
            r3.<init>(r6)     // Catch:{ Exception -> 0x008d }
            boolean r6 = r3.exists()     // Catch:{ Exception -> 0x008d }
            if (r6 == 0) goto L_0x002a
            r3.delete()     // Catch:{ Exception -> 0x008d }
        L_0x002a:
            java.io.FileOutputStream r5 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x008d }
            r5.<init>(r3)     // Catch:{ Exception -> 0x008d }
            r6 = 1024(0x400, float:1.435E-42)
            byte[] r0 = new byte[r6]     // Catch:{ Exception -> 0x004a, all -> 0x008a }
            r1 = -1
        L_0x0034:
            int r1 = r8.read(r0)     // Catch:{ Exception -> 0x004a, all -> 0x008a }
            r6 = -1
            if (r1 != r6) goto L_0x0045
            r4 = r5
        L_0x003c:
            r4.flush()     // Catch:{ Exception -> 0x008d }
            if (r4 == 0) goto L_0x0044
            r4.close()     // Catch:{ IOException -> 0x0085 }
        L_0x0044:
            return
        L_0x0045:
            r6 = 0
            r5.write(r0, r6, r1)     // Catch:{ Exception -> 0x004a, all -> 0x008a }
            goto L_0x0034
        L_0x004a:
            r2 = move-exception
            r4 = r5
        L_0x004c:
            r2.printStackTrace()     // Catch:{ all -> 0x0079 }
            java.io.File r3 = new java.io.File     // Catch:{ all -> 0x0079 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x0079 }
            java.lang.String r7 = java.lang.String.valueOf(r9)     // Catch:{ all -> 0x0079 }
            r6.<init>(r7)     // Catch:{ all -> 0x0079 }
            java.lang.StringBuilder r6 = r6.append(r10)     // Catch:{ all -> 0x0079 }
            java.lang.String r6 = r6.toString()     // Catch:{ all -> 0x0079 }
            r3.<init>(r6)     // Catch:{ all -> 0x0079 }
            boolean r6 = r3.exists()     // Catch:{ all -> 0x0079 }
            if (r6 == 0) goto L_0x006e
            r3.delete()     // Catch:{ all -> 0x0079 }
        L_0x006e:
            if (r4 == 0) goto L_0x0044
            r4.close()     // Catch:{ IOException -> 0x0074 }
            goto L_0x0044
        L_0x0074:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0044
        L_0x0079:
            r6 = move-exception
        L_0x007a:
            if (r4 == 0) goto L_0x007f
            r4.close()     // Catch:{ IOException -> 0x0080 }
        L_0x007f:
            throw r6
        L_0x0080:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x007f
        L_0x0085:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0044
        L_0x008a:
            r6 = move-exception
            r4 = r5
            goto L_0x007a
        L_0x008d:
            r2 = move-exception
            goto L_0x004c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.gaoding.dingcan.util.Utils.writeToSdcard(java.io.InputStream, java.lang.String, java.lang.String):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:35:0x0065 A[SYNTHETIC, Splitter:B:35:0x0065] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void writeToSdcard(java.io.InputStream r8, java.lang.String r9) {
        /*
            r4 = 0
            if (r8 == 0) goto L_0x0036
            java.io.File r3 = new java.io.File     // Catch:{ Exception -> 0x0076 }
            r6 = 0
            java.lang.String r7 = "/"
            int r7 = r9.lastIndexOf(r7)     // Catch:{ Exception -> 0x0076 }
            java.lang.String r6 = r9.substring(r6, r7)     // Catch:{ Exception -> 0x0076 }
            r3.<init>(r6)     // Catch:{ Exception -> 0x0076 }
            r3.mkdirs()     // Catch:{ Exception -> 0x0076 }
            java.io.File r3 = new java.io.File     // Catch:{ Exception -> 0x0076 }
            r3.<init>(r9)     // Catch:{ Exception -> 0x0076 }
            boolean r6 = r3.exists()     // Catch:{ Exception -> 0x0076 }
            if (r6 == 0) goto L_0x0024
            r3.delete()     // Catch:{ Exception -> 0x0076 }
        L_0x0024:
            java.io.FileOutputStream r5 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0076 }
            r5.<init>(r3)     // Catch:{ Exception -> 0x0076 }
            r6 = 1024(0x400, float:1.435E-42)
            byte[] r0 = new byte[r6]     // Catch:{ Exception -> 0x0044, all -> 0x0073 }
            r1 = -1
        L_0x002e:
            int r1 = r8.read(r0)     // Catch:{ Exception -> 0x0044, all -> 0x0073 }
            r6 = -1
            if (r1 != r6) goto L_0x003f
            r4 = r5
        L_0x0036:
            r4.flush()     // Catch:{ Exception -> 0x0076 }
            if (r4 == 0) goto L_0x003e
            r4.close()     // Catch:{ IOException -> 0x006e }
        L_0x003e:
            return
        L_0x003f:
            r6 = 0
            r5.write(r0, r6, r1)     // Catch:{ Exception -> 0x0044, all -> 0x0073 }
            goto L_0x002e
        L_0x0044:
            r2 = move-exception
            r4 = r5
        L_0x0046:
            r2.printStackTrace()     // Catch:{ all -> 0x0062 }
            java.io.File r3 = new java.io.File     // Catch:{ all -> 0x0062 }
            r3.<init>(r9)     // Catch:{ all -> 0x0062 }
            boolean r6 = r3.exists()     // Catch:{ all -> 0x0062 }
            if (r6 == 0) goto L_0x0057
            r3.delete()     // Catch:{ all -> 0x0062 }
        L_0x0057:
            if (r4 == 0) goto L_0x003e
            r4.close()     // Catch:{ IOException -> 0x005d }
            goto L_0x003e
        L_0x005d:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x003e
        L_0x0062:
            r6 = move-exception
        L_0x0063:
            if (r4 == 0) goto L_0x0068
            r4.close()     // Catch:{ IOException -> 0x0069 }
        L_0x0068:
            throw r6
        L_0x0069:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0068
        L_0x006e:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x003e
        L_0x0073:
            r6 = move-exception
            r4 = r5
            goto L_0x0063
        L_0x0076:
            r2 = move-exception
            goto L_0x0046
        */
        throw new UnsupportedOperationException("Method not decompiled: com.gaoding.dingcan.util.Utils.writeToSdcard(java.io.InputStream, java.lang.String):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x0062 A[SYNTHETIC, Splitter:B:31:0x0062] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String getFileContent(java.lang.String r9) {
        /*
            r4 = 0
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.io.File r3 = new java.io.File     // Catch:{ Exception -> 0x0074 }
            r3.<init>(r9)     // Catch:{ Exception -> 0x0074 }
            boolean r7 = r3.exists()     // Catch:{ Exception -> 0x0074 }
            if (r7 != 0) goto L_0x0029
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0074 }
            java.lang.String r8 = "***file "
            r7.<init>(r8)     // Catch:{ Exception -> 0x0074 }
            java.lang.StringBuilder r7 = r7.append(r9)     // Catch:{ Exception -> 0x0074 }
            java.lang.String r8 = " not exit"
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Exception -> 0x0074 }
            java.lang.String r7 = r7.toString()     // Catch:{ Exception -> 0x0074 }
            com.gaoding.dingcan.util.LogUtils.logDebug(r7)     // Catch:{ Exception -> 0x0074 }
        L_0x0029:
            java.io.FileInputStream r5 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0074 }
            r5.<init>(r3)     // Catch:{ Exception -> 0x0074 }
            r7 = 1024(0x400, float:1.435E-42)
            byte[] r0 = new byte[r7]     // Catch:{ Exception -> 0x004f, all -> 0x0071 }
            r1 = -1
        L_0x0033:
            int r1 = r5.read(r0)     // Catch:{ Exception -> 0x004f, all -> 0x0071 }
            r7 = -1
            if (r1 != r7) goto L_0x0045
            if (r5 == 0) goto L_0x006f
            r5.close()     // Catch:{ IOException -> 0x006b }
            r4 = r5
        L_0x0040:
            java.lang.String r7 = r6.toString()
            return r7
        L_0x0045:
            java.lang.String r7 = new java.lang.String     // Catch:{ Exception -> 0x004f, all -> 0x0071 }
            r8 = 0
            r7.<init>(r0, r8, r1)     // Catch:{ Exception -> 0x004f, all -> 0x0071 }
            r6.append(r7)     // Catch:{ Exception -> 0x004f, all -> 0x0071 }
            goto L_0x0033
        L_0x004f:
            r2 = move-exception
            r4 = r5
        L_0x0051:
            r2.printStackTrace()     // Catch:{ all -> 0x005f }
            if (r4 == 0) goto L_0x0040
            r4.close()     // Catch:{ IOException -> 0x005a }
            goto L_0x0040
        L_0x005a:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0040
        L_0x005f:
            r7 = move-exception
        L_0x0060:
            if (r4 == 0) goto L_0x0065
            r4.close()     // Catch:{ IOException -> 0x0066 }
        L_0x0065:
            throw r7
        L_0x0066:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0065
        L_0x006b:
            r2 = move-exception
            r2.printStackTrace()
        L_0x006f:
            r4 = r5
            goto L_0x0040
        L_0x0071:
            r7 = move-exception
            r4 = r5
            goto L_0x0060
        L_0x0074:
            r2 = move-exception
            goto L_0x0051
        */
        throw new UnsupportedOperationException("Method not decompiled: com.gaoding.dingcan.util.Utils.getFileContent(java.lang.String):java.lang.String");
    }

    public static int[] getWidthHeight(Activity activity) {
        int[] params = new int[2];
        try {
            Display display = activity.getWindowManager().getDefaultDisplay();
            params[1] = display.getHeight();
            params[0] = display.getWidth();
            LogUtils.logDebug("****width=" + params[0] + ",height=" + params[1]);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return params;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v23, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: java.lang.String} */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0045, code lost:
        if ("".equals(r7) != false) goto L_0x0047;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String getSerialNumber(android.content.Context r14) {
        /*
            r7 = 0
            java.lang.String r10 = "android.os.SystemProperties"
            java.lang.Class r1 = java.lang.Class.forName(r10)     // Catch:{ Exception -> 0x00c8 }
            java.lang.String r10 = "get"
            r11 = 1
            java.lang.Class[] r11 = new java.lang.Class[r11]     // Catch:{ Exception -> 0x00c8 }
            r12 = 0
            java.lang.Class<java.lang.String> r13 = java.lang.String.class
            r11[r12] = r13     // Catch:{ Exception -> 0x00c8 }
            java.lang.reflect.Method r4 = r1.getMethod(r10, r11)     // Catch:{ Exception -> 0x00c8 }
            r10 = 1
            java.lang.Object[] r10 = new java.lang.Object[r10]     // Catch:{ Exception -> 0x00c8 }
            r11 = 0
            java.lang.String r12 = "ro.serialno"
            r10[r11] = r12     // Catch:{ Exception -> 0x00c8 }
            java.lang.Object r10 = r4.invoke(r1, r10)     // Catch:{ Exception -> 0x00c8 }
            r0 = r10
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x00c8 }
            r7 = r0
            if (r7 == 0) goto L_0x002b
            java.lang.String r7 = r7.trim()     // Catch:{ Exception -> 0x00c8 }
        L_0x002b:
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00c8 }
            java.lang.String r11 = "***serialNumber:"
            r10.<init>(r11)     // Catch:{ Exception -> 0x00c8 }
            java.lang.StringBuilder r10 = r10.append(r7)     // Catch:{ Exception -> 0x00c8 }
            java.lang.String r10 = r10.toString()     // Catch:{ Exception -> 0x00c8 }
            com.gaoding.dingcan.util.LogUtils.logDebug(r10)     // Catch:{ Exception -> 0x00c8 }
        L_0x003d:
            if (r7 == 0) goto L_0x0047
            java.lang.String r10 = ""
            boolean r10 = r10.equals(r7)     // Catch:{ Exception -> 0x00ce }
            if (r10 == 0) goto L_0x009b
        L_0x0047:
            java.lang.String r10 = "wifi"
            java.lang.Object r9 = r14.getSystemService(r10)     // Catch:{ Exception -> 0x00ce }
            android.net.wifi.WifiManager r9 = (android.net.wifi.WifiManager) r9     // Catch:{ Exception -> 0x00ce }
            android.net.wifi.WifiInfo r5 = r9.getConnectionInfo()     // Catch:{ Exception -> 0x00ce }
            java.lang.String r7 = r5.getMacAddress()     // Catch:{ Exception -> 0x00ce }
            if (r7 == 0) goto L_0x0061
            java.lang.String r10 = ":"
            java.lang.String r11 = ""
            java.lang.String r7 = r7.replace(r10, r11)     // Catch:{ Exception -> 0x00ce }
        L_0x0061:
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00ce }
            java.lang.String r11 = "***serialnumber-->getMacAddress:"
            r10.<init>(r11)     // Catch:{ Exception -> 0x00ce }
            java.lang.StringBuilder r10 = r10.append(r7)     // Catch:{ Exception -> 0x00ce }
            java.lang.String r10 = r10.toString()     // Catch:{ Exception -> 0x00ce }
            com.gaoding.dingcan.util.LogUtils.logDebug(r10)     // Catch:{ Exception -> 0x00ce }
            if (r7 != 0) goto L_0x009b
            java.lang.String r10 = ""
            boolean r10 = r10.equals(r7)     // Catch:{ Exception -> 0x00ce }
            if (r10 == 0) goto L_0x009b
            java.lang.String r10 = "phone"
            java.lang.Object r8 = r14.getSystemService(r10)     // Catch:{ Exception -> 0x00ce }
            android.telephony.TelephonyManager r8 = (android.telephony.TelephonyManager) r8     // Catch:{ Exception -> 0x00ce }
            java.lang.String r7 = r8.getDeviceId()     // Catch:{ Exception -> 0x00ce }
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00ce }
            java.lang.String r11 = "*****serialnumber-->getDeviceId:"
            r10.<init>(r11)     // Catch:{ Exception -> 0x00ce }
            java.lang.StringBuilder r10 = r10.append(r7)     // Catch:{ Exception -> 0x00ce }
            java.lang.String r10 = r10.toString()     // Catch:{ Exception -> 0x00ce }
            com.gaoding.dingcan.util.LogUtils.logDebug(r10)     // Catch:{ Exception -> 0x00ce }
        L_0x009b:
            if (r7 == 0) goto L_0x00a5
            java.lang.String r10 = ""
            boolean r10 = r10.equals(r7)
            if (r10 == 0) goto L_0x00b5
        L_0x00a5:
            java.text.SimpleDateFormat r6 = new java.text.SimpleDateFormat
            java.lang.String r10 = "yyyyMMddHHmmssSSS"
            r6.<init>(r10)
            java.util.Date r2 = new java.util.Date
            r2.<init>()
            java.lang.String r7 = r6.format(r2)
        L_0x00b5:
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            java.lang.String r11 = "*****serialnumber:"
            r10.<init>(r11)
            java.lang.StringBuilder r10 = r10.append(r7)
            java.lang.String r10 = r10.toString()
            com.gaoding.dingcan.util.LogUtils.logDebug(r10)
            return r7
        L_0x00c8:
            r3 = move-exception
            r3.printStackTrace()
            goto L_0x003d
        L_0x00ce:
            r3 = move-exception
            r3.printStackTrace()
            goto L_0x009b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.gaoding.dingcan.util.Utils.getSerialNumber(android.content.Context):java.lang.String");
    }

    public static int getAge(String birthday) {
        try {
            Date birthDate = new SimpleDateFormat("yyyy-MM-dd").parse(birthday);
            Calendar cal = Calendar.getInstance();
            if (cal.before(birthDate)) {
                return 0;
            }
            int yearNow = cal.get(1);
            int monthNow = cal.get(2);
            int dayOfMonthNow = cal.get(5);
            cal.setTime(birthDate);
            int yearBirth = cal.get(1);
            int monthBirth = cal.get(2);
            int dayOfMonthBirth = cal.get(5);
            int age = yearNow - yearBirth;
            if (monthNow > monthBirth) {
                return age;
            }
            if (monthNow != monthBirth) {
                return age - 1;
            }
            if (dayOfMonthNow < dayOfMonthBirth) {
                return age - 1;
            }
            return age;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static void disableConnectionReuseIfNecessary() {
        if (hasHttpConnectionBug()) {
            System.setProperty("http.keepAlive", "false");
        }
    }

    public static ProgressDialog getProgressDialog(Activity activity, String message) {
        ProgressDialog mProgressDialog = new ProgressDialog(activity);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setCancelable(false);
        if (message != null && !"".equals(message)) {
            mProgressDialog.setMessage(message);
        }
        return mProgressDialog;
    }

    public static double getDoubleAccuracy(double double1, int scale) {
        try {
            return new BigDecimal(double1).setScale(scale, 4).doubleValue();
        } catch (Exception e) {
            e.printStackTrace();
            return 0.0d;
        }
    }

    public static float getFloatAccuracy(float f, int scale) {
        try {
            return new BigDecimal((double) f).setScale(scale, 4).floatValue();
        } catch (Exception e) {
            e.printStackTrace();
            return 0.0f;
        }
    }

    public static int getDateGap(long time) {
        try {
            Calendar calendarNow = Calendar.getInstance();
            Calendar calendarTime = Calendar.getInstance();
            calendarTime.setTimeInMillis(time);
            calendarNow.set(10, 12);
            calendarNow.set(12, 0);
            calendarNow.set(13, 0);
            calendarNow.set(14, 0);
            calendarTime.set(10, 12);
            calendarTime.set(12, 0);
            calendarTime.set(13, 0);
            calendarTime.set(14, 0);
            return (int) ((calendarTime.getTimeInMillis() - calendarNow.getTimeInMillis()) / 86400000);
        } catch (Exception e) {
            e.printStackTrace();
            return -10;
        }
    }

    public static Bitmap drawableToBitmap(Drawable drawable) {
        int width = drawable.getIntrinsicWidth();
        int height = drawable.getIntrinsicHeight();
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, width, height);
        drawable.draw(canvas);
        return bitmap;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static Drawable zoomDrawable(Drawable drawable, int w, int h) {
        float scale;
        int width = drawable.getIntrinsicWidth();
        int height = drawable.getIntrinsicHeight();
        Bitmap oldbmp = drawableToBitmap(drawable);
        Matrix matrix = new Matrix();
        float scaleWidth = ((float) w) / ((float) width);
        float scaleHeight = ((float) h) / ((float) height);
        if (scaleHeight >= 1.0f && scaleWidth >= 1.0f) {
            scale = Math.min(scaleHeight, scaleWidth);
        } else if (scaleHeight >= 1.0f || scaleWidth >= 1.0f) {
            scale = Math.min(scaleHeight, scaleWidth);
        } else {
            scale = Math.min(scaleHeight, scaleWidth);
        }
        matrix.postScale(scale, scale);
        Bitmap.Config config = Bitmap.Config.RGB_565;
        Bitmap newbmp = Bitmap.createBitmap(oldbmp, 0, 0, width, height, matrix, true);
        newbmp.setDensity(240);
        BitmapDrawable bitmapDrawable = new BitmapDrawable(newbmp);
        bitmapDrawable.setTargetDensity(240);
        System.out.println("**screen width=" + w + ",height=" + h + ",old pic width=" + width + ",heigh=" + height + ",new drawable width=" + bitmapDrawable.getIntrinsicWidth() + ",heith=" + bitmapDrawable.getIntrinsicHeight());
        return bitmapDrawable;
    }

    public static Bitmap drawableToBitmapNotCompress(Drawable drawable) {
        int width = drawable.getIntrinsicWidth();
        int height = drawable.getIntrinsicHeight();
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, width, height);
        drawable.draw(canvas);
        return bitmap;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static Drawable zoomDrawableNotCompress(Drawable drawable, int w, int h) {
        float scale;
        int width = drawable.getIntrinsicWidth();
        int height = drawable.getIntrinsicHeight();
        Bitmap oldbmp = drawableToBitmapNotCompress(drawable);
        Matrix matrix = new Matrix();
        float scaleWidth = ((float) w) / ((float) width);
        float scaleHeight = ((float) h) / ((float) height);
        if (scaleHeight >= 1.0f && scaleWidth >= 1.0f) {
            scale = Math.min(scaleHeight, scaleWidth);
        } else if (scaleHeight >= 1.0f || scaleWidth >= 1.0f) {
            scale = Math.min(scaleHeight, scaleWidth);
        } else {
            scale = Math.min(scaleHeight, scaleWidth);
        }
        matrix.postScale(scale, scale);
        Bitmap newbmp = Bitmap.createBitmap(oldbmp, 0, 0, width, height, matrix, true);
        newbmp.setDensity(240);
        BitmapDrawable bitmapDrawable = new BitmapDrawable(newbmp);
        bitmapDrawable.setTargetDensity(240);
        System.out.println("**screen width=" + w + ",height=" + h + ",old pic width=" + width + ",heigh=" + height + ",new drawable width=" + bitmapDrawable.getIntrinsicWidth() + ",heith=" + bitmapDrawable.getIntrinsicHeight());
        return bitmapDrawable;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static Drawable zoomDrawable(Bitmap bitmap, float w, float h) {
        float scale;
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        Matrix matrix = new Matrix();
        float scaleWidth = w / ((float) width);
        float scaleHeight = h / ((float) height);
        if (scaleHeight >= 1.0f && scaleWidth >= 1.0f) {
            scale = Math.min(scaleHeight, scaleWidth);
        } else if (scaleHeight >= 1.0f || scaleWidth >= 1.0f) {
            scale = Math.min(scaleHeight, scaleWidth);
        } else {
            scale = Math.min(scaleHeight, scaleWidth);
        }
        matrix.postScale(scale, scale);
        return new BitmapDrawable(Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true));
    }
}
