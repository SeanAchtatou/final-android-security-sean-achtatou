package com.gaoding.dingcan.util;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.lang.ref.SoftReference;
import java.net.MalformedURLException;
import java.util.HashMap;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public class AsyncImageLoader {
    public static String ACTION_ASYNC_DOWNLOAD_END = "com.dream.baijishihui.action_async_download_end";
    public static String STORE_CACHE_IMAGE_PATH = (String.valueOf(STORE_CACHE_PATH) + "/.images/");
    public static String STORE_CACHE_PATH = (String.valueOf(Environment.getExternalStorageDirectory().toString()) + "/gaoding/storecache/");
    /* access modifiers changed from: private */
    public HashMap<String, SoftReference<Bitmap>> imageCache = new HashMap<>();
    /* access modifiers changed from: private */
    public Context mContext;

    public interface ImageCallback {
        void imageLoaded(Bitmap bitmap, String str);
    }

    public AsyncImageLoader(Context mContext2) {
        this.mContext = mContext2;
    }

    public Bitmap loadDrawable(final String imageUrl, final ImageCallback imageCallback) {
        Bitmap bitmap;
        if (this.imageCache.containsKey(imageUrl) && (bitmap = (Bitmap) this.imageCache.get(imageUrl).get()) != null) {
            return bitmap;
        }
        File file = new File(STORE_CACHE_IMAGE_PATH, imageUrl.substring(imageUrl.lastIndexOf("/") + 1));
        if (file.exists()) {
            return getBitmapFromFile(file.getAbsolutePath());
        }
        final Handler handler = new Handler() {
            public void handleMessage(Message message) {
                AsyncImageLoader.this.mContext.sendBroadcast(new Intent(AsyncImageLoader.ACTION_ASYNC_DOWNLOAD_END));
                imageCallback.imageLoaded((Bitmap) message.obj, imageUrl);
            }
        };
        new Thread() {
            public void run() {
                Bitmap bitmap = AsyncImageLoader.loadImageFromUrl(imageUrl);
                AsyncImageLoader.this.imageCache.put(imageUrl, new SoftReference(bitmap));
                handler.sendMessage(handler.obtainMessage(0, bitmap));
            }
        }.start();
        return null;
    }

    public static Bitmap loadImageFromUrl(String url) {
        LogUtils.logDebug("********download url=" + url);
        InputStream is = null;
        long total = 0;
        try {
            HttpEntity entity = new DefaultHttpClient().execute(new HttpGet(url)).getEntity();
            total = entity.getContentLength();
            is = entity.getContent();
            String fileName = url.substring(url.lastIndexOf("/") + 1);
            Utils.writeToSdcard(is, STORE_CACHE_IMAGE_PATH, fileName);
            is = new FileInputStream(String.valueOf(STORE_CACHE_IMAGE_PATH) + fileName);
        } catch (MalformedURLException e1) {
            e1.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 1;
        while (total / ((long) (options.inSampleSize * options.inSampleSize)) > 51200) {
            options.inSampleSize++;
        }
        Log.d("dream", "*****in sample size=" + options.inSampleSize + ",total=" + total + ",option in just decode bounds=" + options.inJustDecodeBounds);
        return BitmapFactory.decodeStream(is, null, options);
    }

    public void clearImageCache() {
        if (this.imageCache != null) {
            this.imageCache.clear();
        }
    }

    private static Bitmap getBitmapFromFile(String filePath) {
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(filePath, options);
            if (options.outHeight <= 0 || options.outWidth <= 0) {
                return null;
            }
            options.inJustDecodeBounds = false;
            options.inSampleSize = 2;
            while (options.outWidth / options.inSampleSize > 320 && options.outHeight / options.inSampleSize > 200) {
                options.inSampleSize++;
            }
            options.inSampleSize--;
            return BitmapFactory.decodeFile(filePath, options);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Bitmap loadDrawable(final String imageUrl, final String fileName, final ImageCallback imageCallback) {
        Bitmap bitmap;
        if (this.imageCache.containsKey(imageUrl) && (bitmap = (Bitmap) this.imageCache.get(imageUrl).get()) != null) {
            return bitmap;
        }
        File file = new File(fileName);
        if (file.exists()) {
            return getBitmapFromFile(file.getAbsolutePath());
        }
        final Handler handler = new Handler() {
            public void handleMessage(Message message) {
                AsyncImageLoader.this.mContext.sendBroadcast(new Intent(AsyncImageLoader.ACTION_ASYNC_DOWNLOAD_END));
                imageCallback.imageLoaded((Bitmap) message.obj, imageUrl);
            }
        };
        new Thread() {
            public void run() {
                Bitmap bitmap = AsyncImageLoader.loadImageFromUrl(imageUrl, fileName);
                AsyncImageLoader.this.imageCache.put(imageUrl, new SoftReference(bitmap));
                handler.sendMessage(handler.obtainMessage(0, bitmap));
            }
        }.start();
        return null;
    }

    public static Bitmap loadImageFromUrl(String url, String fileName) {
        LogUtils.logDebug("********download url=" + url);
        InputStream is = null;
        long total = 0;
        try {
            HttpEntity entity = new DefaultHttpClient().execute(new HttpGet(url)).getEntity();
            total = entity.getContentLength();
            is = entity.getContent();
            Utils.writeToSdcard(is, fileName);
            is = new FileInputStream(fileName);
        } catch (MalformedURLException e1) {
            e1.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 1;
        while (total / ((long) (options.inSampleSize * options.inSampleSize)) > 51200) {
            options.inSampleSize++;
        }
        Log.d("dream", "*****in sample size=" + options.inSampleSize + ",total=" + total + ",option in just decode bounds=" + options.inJustDecodeBounds);
        return BitmapFactory.decodeStream(is, null, options);
    }
}
