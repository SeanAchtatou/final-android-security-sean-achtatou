package com.gaoding.dingcan.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "gaoding.db";
    private static final int DATABASE_VERSION = 6;
    private final String CREATE_AREA_TABLE = "CREATE TABLE IF NOT EXISTS area(_id INTEGER PRIMARY KEY AUTOINCREMENT,cityid TEXT,id TEXT,name TEXT,x TEXT,y TEXT,desc TEXT,new TEXT);";
    private final String CREATE_CITY_TABLE = "CREATE TABLE IF NOT EXISTS city(_id INTEGER PRIMARY KEY AUTOINCREMENT,id TEXT,name TEXT,desc TEXT,x TEXT,y TEXT);";
    private final String CREATE_INFO_TABLE = "CREATE TABLE IF NOT EXISTS info(_id INTEGER PRIMARY KEY AUTOINCREMENT,uid TEXT,email TEXT,telephone TEXT,name TEXT,integration TEXT,secode TEXT,category TEXT,status TEXT,sex TEXT,address TEXT,type TEXT);";
    private final String CREATE_ORDER_TABLE = "CREATE TABLE IF NOT EXISTS menuorder(_id INTEGER PRIMARY KEY AUTOINCREMENT,id TEXT,memberid TEXT,time TEXT,state TEXT,address TEXT,phone TEXT,desc TEXT,price TEXT,total TEXT,restaurantid TEXT,num TEXT,cactivtyid TEXT,type TEXT,activeid TEXT);";
    private final String CREATE_RESTAURANT_TABLE = "CREATE TABLE IF NOT EXISTS restaurant(_id INTEGER PRIMARY KEY AUTOINCREMENT,id TEXT,unitid TEXT,name TEXT,logo TEXT,address TEXT,notice TEXT,startprice TEXT,sendtime TEXT,senddesc TEXT,state TEXT,opentime TEXT,sendarea TEXT,foodspeed TEXT,hot TEXT,orderscount TEXT);";
    private final String CREATE_SHOP_CART_TABLE = "CREATE TABLE IF NOT EXISTS shoppingcart(_id INTEGER PRIMARY KEY AUTOINCREMENT,restaurantid TEXT,sendprice TEXT,unitname TEXT,state TEXT);";
    private final String CREATE_SHOP_TABLE = "CREATE TABLE IF NOT EXISTS shopping(_id INTEGER PRIMARY KEY AUTOINCREMENT,id TEXT,name TEXT,price TEXT,menuid TEXT,count TEXT);";
    private final String CREATE_UNIT_TABLE = "CREATE TABLE IF NOT EXISTS unit(_id INTEGER PRIMARY KEY AUTOINCREMENT,id TEXT,areaid TEXT,name TEXT,x TEXT,y TEXT,desc TEXT);";
    private final String CREATE_USER_TABLE = "CREATE TABLE IF NOT EXISTS user(_id INTEGER PRIMARY KEY AUTOINCREMENT,uid TEXT,account TEXT,password TEXT,state INTEGER,type INTEGER);";
    private Context mContext = null;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, (SQLiteDatabase.CursorFactory) null, 6);
        this.mContext = context;
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS user(_id INTEGER PRIMARY KEY AUTOINCREMENT,uid TEXT,account TEXT,password TEXT,state INTEGER,type INTEGER);");
        db.execSQL("CREATE TABLE IF NOT EXISTS info(_id INTEGER PRIMARY KEY AUTOINCREMENT,uid TEXT,email TEXT,telephone TEXT,name TEXT,integration TEXT,secode TEXT,category TEXT,status TEXT,sex TEXT,address TEXT,type TEXT);");
        db.execSQL("CREATE TABLE IF NOT EXISTS city(_id INTEGER PRIMARY KEY AUTOINCREMENT,id TEXT,name TEXT,desc TEXT,x TEXT,y TEXT);");
        db.execSQL("CREATE TABLE IF NOT EXISTS area(_id INTEGER PRIMARY KEY AUTOINCREMENT,cityid TEXT,id TEXT,name TEXT,x TEXT,y TEXT,desc TEXT,new TEXT);");
        db.execSQL("CREATE TABLE IF NOT EXISTS unit(_id INTEGER PRIMARY KEY AUTOINCREMENT,id TEXT,areaid TEXT,name TEXT,x TEXT,y TEXT,desc TEXT);");
        db.execSQL("CREATE TABLE IF NOT EXISTS restaurant(_id INTEGER PRIMARY KEY AUTOINCREMENT,id TEXT,unitid TEXT,name TEXT,logo TEXT,address TEXT,notice TEXT,startprice TEXT,sendtime TEXT,senddesc TEXT,state TEXT,opentime TEXT,sendarea TEXT,foodspeed TEXT,hot TEXT,orderscount TEXT);");
        db.execSQL("CREATE TABLE IF NOT EXISTS shopping(_id INTEGER PRIMARY KEY AUTOINCREMENT,id TEXT,name TEXT,price TEXT,menuid TEXT,count TEXT);");
        db.execSQL("CREATE TABLE IF NOT EXISTS shoppingcart(_id INTEGER PRIMARY KEY AUTOINCREMENT,restaurantid TEXT,sendprice TEXT,unitname TEXT,state TEXT);");
        db.execSQL("CREATE TABLE IF NOT EXISTS menuorder(_id INTEGER PRIMARY KEY AUTOINCREMENT,id TEXT,memberid TEXT,time TEXT,state TEXT,address TEXT,phone TEXT,desc TEXT,price TEXT,total TEXT,restaurantid TEXT,num TEXT,cactivtyid TEXT,type TEXT,activeid TEXT);");
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS user");
        db.execSQL("DROP TABLE IF EXISTS info");
        db.execSQL("DROP TABLE IF EXISTS city");
        db.execSQL("DROP TABLE IF EXISTS area");
        db.execSQL("DROP TABLE IF EXISTS unit");
        db.execSQL("DROP TABLE IF EXISTS restaurant");
        db.execSQL("DROP TABLE IF EXISTS shopping");
        db.execSQL("DROP TABLE IF EXISTS shoppingcart");
        db.execSQL("DROP TABLE IF EXISTS menuorder");
        onCreate(db);
    }

    public boolean deleteDatabase() {
        SQLiteDatabase database = getWritableDatabase();
        if (database != null) {
            database.close();
        }
        return this.mContext.deleteDatabase(DATABASE_NAME);
    }
}
