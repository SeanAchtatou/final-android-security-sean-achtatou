package com.gaoding.dingcan.database.controller;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.gaoding.dingcan.database.DataStore;
import com.gaoding.dingcan.database.DatabaseHelper;
import com.gaoding.dingcan.database.bean.ShopCartBean;

public class ShopCart {
    private DatabaseHelper dbHelper = null;
    public ShopCartBean item = new ShopCartBean();

    public ShopCart(Context context) {
        openDatabase(context);
    }

    public boolean getFromDatabase() {
        if (query() > 0) {
            return true;
        }
        return false;
    }

    public boolean setToDatabase() {
        if (getCount() > 0) {
            update(this.item);
        } else {
            insert(this.item);
        }
        return getFromDatabase();
    }

    private void openDatabase(Context context) {
        this.dbHelper = new DatabaseHelper(context);
    }

    public void close() {
        this.dbHelper.close();
    }

    public int query() {
        int result = 0;
        try {
            SQLiteDatabase db = this.dbHelper.getReadableDatabase();
            Cursor cursor = db.query(DataStore.ShoppingCartTable.TABLE_NAME, null, null, null, null, null, null);
            while (cursor.moveToNext()) {
                this.item._id = cursor.getInt(cursor.getColumnIndex("_id"));
                this.item.mRestaurantId = cursor.getString(cursor.getColumnIndex("restaurantid"));
                this.item.mStartPrice = cursor.getString(cursor.getColumnIndex(DataStore.ShoppingCartTable.SEND_PRICE));
                this.item.mState = cursor.getString(cursor.getColumnIndex("state"));
                this.item.mUnitName = cursor.getString(cursor.getColumnIndex(DataStore.ShoppingCartTable.UNIT_NAME));
                result++;
            }
            cursor.close();
            db.close();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    public boolean insert(ShopCartBean item2) {
        try {
            SQLiteDatabase db = this.dbHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("restaurantid", item2.mRestaurantId);
            values.put(DataStore.ShoppingCartTable.SEND_PRICE, item2.mStartPrice);
            values.put("state", item2.mState);
            values.put(DataStore.ShoppingCartTable.UNIT_NAME, item2.mUnitName);
            db.insert(DataStore.ShoppingCartTable.TABLE_NAME, null, values);
            db.close();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean update(ShopCartBean item2) {
        try {
            SQLiteDatabase db = this.dbHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("restaurantid", item2.mRestaurantId);
            values.put(DataStore.ShoppingCartTable.SEND_PRICE, item2.mStartPrice);
            values.put("state", item2.mState);
            values.put(DataStore.ShoppingCartTable.UNIT_NAME, item2.mUnitName);
            db.update(DataStore.ShoppingCartTable.TABLE_NAME, values, "_id = ?", new String[]{String.valueOf(item2._id)});
            db.close();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean delete(int _id) {
        try {
            SQLiteDatabase db = this.dbHelper.getWritableDatabase();
            db.delete(DataStore.ShoppingCartTable.TABLE_NAME, "_id = ?", new String[]{String.valueOf(_id)});
            db.close();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean clean() {
        try {
            SQLiteDatabase db = this.dbHelper.getWritableDatabase();
            db.delete(DataStore.ShoppingCartTable.TABLE_NAME, null, null);
            db.close();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public int getCount() {
        int result = 0;
        try {
            SQLiteDatabase db = this.dbHelper.getReadableDatabase();
            Cursor cursor = db.query(DataStore.ShoppingCartTable.TABLE_NAME, null, null, null, null, null, null);
            if (cursor != null) {
                result = cursor.getCount();
            }
            cursor.close();
            db.close();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }
}
