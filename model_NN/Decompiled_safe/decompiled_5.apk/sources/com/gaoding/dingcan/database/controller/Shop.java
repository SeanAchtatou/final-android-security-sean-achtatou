package com.gaoding.dingcan.database.controller;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.gaoding.dingcan.database.DataStore;
import com.gaoding.dingcan.database.DatabaseHelper;
import com.gaoding.dingcan.database.bean.ShopBean;
import java.util.ArrayList;
import java.util.List;

public class Shop {
    private DatabaseHelper dbHelper = null;
    public List<ShopBean> list = new ArrayList();

    public Shop(Context context) {
        openDatabase(context);
    }

    public boolean getFromDatabase() {
        if (query() > 0) {
            return true;
        }
        return false;
    }

    public boolean syncToDatabase(List<ShopBean> list2) {
        clean();
        for (int i = 0; i < list2.size(); i++) {
            insert(list2.get(i));
        }
        query();
        return false;
    }

    public boolean addToDatabase(List<ShopBean> list2) {
        for (int i = 0; i < list2.size(); i++) {
            insert(list2.get(i));
        }
        query();
        return false;
    }

    public boolean setToDatabase(ShopBean item) {
        boolean update = false;
        int i = 0;
        while (true) {
            if (i >= this.list.size()) {
                break;
            } else if (this.list.get(i).mMenuId.equals(item.mMenuId)) {
                item._id = this.list.get(i)._id;
                this.list.get(i).mName = item.mName;
                this.list.get(i).mPrice = item.mPrice;
                this.list.get(i).mCount = item.mCount;
                update = true;
                update(item);
                break;
            } else {
                i++;
            }
        }
        if (update) {
            return true;
        }
        insert(item);
        query();
        return true;
    }

    public int addToDatabase(ShopBean item) {
        int result = 0;
        boolean update = false;
        int i = 0;
        while (true) {
            if (i >= this.list.size()) {
                break;
            } else if (this.list.get(i).mMenuId.equals(item.mMenuId)) {
                item._id = this.list.get(i)._id;
                this.list.get(i).mName = item.mName;
                this.list.get(i).mPrice = item.mPrice;
                int count = Integer.parseInt(this.list.get(i).mCount) + 1;
                result = count;
                item.mCount = new StringBuilder().append(count).toString();
                this.list.get(i).mCount = item.mCount;
                update = true;
                update(item);
                break;
            } else {
                i++;
            }
        }
        if (update) {
            return result;
        }
        insert(item);
        query();
        return 1;
    }

    public int reduceToDatabase(ShopBean item) {
        int i = 0;
        while (i < this.list.size()) {
            if (this.list.get(i).mMenuId.equals(item.mMenuId)) {
                item._id = this.list.get(i)._id;
                this.list.get(i).mName = item.mName;
                this.list.get(i).mPrice = item.mPrice;
                int count = Integer.parseInt(this.list.get(i).mCount);
                if (count > 1) {
                    int count2 = count - 1;
                    int result = count2;
                    item.mCount = new StringBuilder().append(count2).toString();
                    this.list.get(i).mCount = item.mCount;
                    update(item);
                    return result;
                } else if (count != 1) {
                    return 0;
                } else {
                    this.list.remove(i);
                    delete(item._id);
                    return 0;
                }
            } else {
                i++;
            }
        }
        return 0;
    }

    public int getItemCount(String strMenuId) {
        for (int i = 0; i < this.list.size(); i++) {
            if (this.list.get(i).mMenuId.equals(strMenuId)) {
                return Integer.parseInt(this.list.get(i).mCount);
            }
        }
        return 0;
    }

    private void openDatabase(Context context) {
        this.dbHelper = new DatabaseHelper(context);
    }

    public void close() {
        this.dbHelper.close();
    }

    public int query() {
        this.list.clear();
        int result = 0;
        try {
            SQLiteDatabase db = this.dbHelper.getReadableDatabase();
            Cursor cursor = db.query(DataStore.ShoppingTable.TABLE_NAME, null, null, null, null, null, null);
            while (cursor.moveToNext()) {
                ShopBean item = new ShopBean();
                item._id = cursor.getInt(cursor.getColumnIndex("_id"));
                item.mId = cursor.getString(cursor.getColumnIndex("id"));
                item.mMenuId = cursor.getString(cursor.getColumnIndex("menuid"));
                item.mName = cursor.getString(cursor.getColumnIndex("name"));
                item.mPrice = cursor.getString(cursor.getColumnIndex("price"));
                item.mCount = cursor.getString(cursor.getColumnIndex("count"));
                result++;
                this.list.add(item);
            }
            cursor.close();
            db.close();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    public boolean insert(ShopBean item) {
        try {
            SQLiteDatabase db = this.dbHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("id", item.mId);
            values.put("menuid", item.mMenuId);
            values.put("name", item.mName);
            values.put("price", item.mPrice);
            values.put("count", item.mCount);
            db.insert(DataStore.ShoppingTable.TABLE_NAME, null, values);
            db.close();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean update(ShopBean item) {
        try {
            SQLiteDatabase db = this.dbHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("id", item.mId);
            values.put("menuid", item.mMenuId);
            values.put("name", item.mName);
            values.put("price", item.mPrice);
            values.put("count", item.mCount);
            db.update(DataStore.ShoppingTable.TABLE_NAME, values, "_id = ?", new String[]{String.valueOf(item._id)});
            db.close();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean delete(int _id) {
        try {
            SQLiteDatabase db = this.dbHelper.getWritableDatabase();
            db.delete(DataStore.ShoppingTable.TABLE_NAME, "_id = ?", new String[]{String.valueOf(_id)});
            db.close();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean delete(String strMenuId) {
        try {
            SQLiteDatabase db = this.dbHelper.getWritableDatabase();
            db.delete(DataStore.ShoppingTable.TABLE_NAME, "menuid = ?", new String[]{String.valueOf(strMenuId)});
            db.close();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean clean() {
        try {
            SQLiteDatabase db = this.dbHelper.getWritableDatabase();
            db.delete(DataStore.ShoppingTable.TABLE_NAME, null, null);
            db.close();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public int getCount() {
        int result = 0;
        try {
            SQLiteDatabase db = this.dbHelper.getReadableDatabase();
            Cursor cursor = db.query(DataStore.ShoppingTable.TABLE_NAME, null, null, null, null, null, null);
            if (cursor != null) {
                result = cursor.getCount();
            }
            cursor.close();
            db.close();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }
}
