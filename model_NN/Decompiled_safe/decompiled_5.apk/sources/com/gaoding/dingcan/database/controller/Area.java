package com.gaoding.dingcan.database.controller;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.gaoding.dingcan.database.DataStore;
import com.gaoding.dingcan.database.DatabaseHelper;
import com.gaoding.dingcan.database.bean.AreaBean;
import java.util.ArrayList;
import java.util.List;

public class Area {
    private DatabaseHelper dbHelper = null;
    public List<AreaBean> list = new ArrayList();

    public Area(Context context) {
        openDatabase(context);
    }

    public boolean getFromDatabase() {
        if (query() > 0) {
            return true;
        }
        return false;
    }

    public boolean syncToDatabase(List<AreaBean> list2) {
        clean();
        for (int i = 0; i < list2.size(); i++) {
            insert(list2.get(i));
        }
        query();
        return false;
    }

    public boolean setToDatabase(AreaBean item) {
        boolean update = false;
        int i = 0;
        while (true) {
            if (i >= this.list.size()) {
                break;
            } else if (this.list.get(i).mId.equals(item.mId)) {
                item._id = this.list.get(i)._id;
                this.list.get(i).mName = item.mName;
                this.list.get(i).mDesc = item.mDesc;
                this.list.get(i).mX = item.mX;
                this.list.get(i).mY = item.mY;
                update = true;
                update(item);
                break;
            } else {
                i++;
            }
        }
        if (update) {
            return true;
        }
        insert(item);
        boolean result = getFromDatabase();
        return true;
    }

    private void openDatabase(Context context) {
        this.dbHelper = new DatabaseHelper(context);
    }

    public void close() {
        this.dbHelper.close();
    }

    public int query() {
        this.list.clear();
        int result = 0;
        try {
            SQLiteDatabase db = this.dbHelper.getReadableDatabase();
            Cursor cursor = db.query(DataStore.AreaTable.TABLE_NAME, null, null, null, null, null, null);
            while (cursor.moveToNext()) {
                AreaBean item = new AreaBean();
                item._id = cursor.getInt(cursor.getColumnIndex("_id"));
                item.mId = cursor.getString(cursor.getColumnIndex("id"));
                item.mCityId = cursor.getString(cursor.getColumnIndex(DataStore.AreaTable.CITY_ID));
                item.mName = cursor.getString(cursor.getColumnIndex("name"));
                item.mDesc = cursor.getString(cursor.getColumnIndex("desc"));
                item.mX = cursor.getString(cursor.getColumnIndex("x"));
                item.mY = cursor.getString(cursor.getColumnIndex("y"));
                item.mIsNew = cursor.getString(cursor.getColumnIndex(DataStore.AreaTable.AREA_NEW));
                result++;
                this.list.add(item);
            }
            cursor.close();
            db.close();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    public boolean insert(AreaBean item) {
        try {
            SQLiteDatabase db = this.dbHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("id", item.mId);
            values.put(DataStore.AreaTable.CITY_ID, item.mCityId);
            values.put("name", item.mName);
            values.put("desc", item.mDesc);
            values.put("x", item.mX);
            values.put("y", item.mY);
            values.put(DataStore.AreaTable.AREA_NEW, item.mIsNew);
            db.insert(DataStore.AreaTable.TABLE_NAME, null, values);
            db.close();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean update(AreaBean item) {
        try {
            SQLiteDatabase db = this.dbHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("id", item.mId);
            values.put(DataStore.AreaTable.CITY_ID, item.mCityId);
            values.put("name", item.mName);
            values.put("desc", item.mDesc);
            values.put("x", item.mX);
            values.put("y", item.mY);
            values.put(DataStore.AreaTable.AREA_NEW, item.mIsNew);
            db.update(DataStore.AreaTable.TABLE_NAME, values, "_id = ?", new String[]{String.valueOf(item._id)});
            db.close();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean delete(int _id) {
        try {
            SQLiteDatabase db = this.dbHelper.getWritableDatabase();
            db.delete(DataStore.AreaTable.TABLE_NAME, "_id = ?", new String[]{String.valueOf(_id)});
            db.close();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private boolean clean() {
        try {
            SQLiteDatabase db = this.dbHelper.getWritableDatabase();
            db.delete(DataStore.AreaTable.TABLE_NAME, null, null);
            db.close();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public int getCount() {
        int result = 0;
        try {
            SQLiteDatabase db = this.dbHelper.getReadableDatabase();
            Cursor cursor = db.query(DataStore.AreaTable.TABLE_NAME, null, null, null, null, null, null);
            if (cursor != null) {
                result = cursor.getCount();
            }
            cursor.close();
            db.close();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }
}
