package com.gaoding.dingcan.database.bean;

public class OrderBean {
    public int _id;
    public String mActiveId;
    public String mAddress;
    public String mCactivtyId;
    public String mDesc;
    public String mId;
    public String mMemberId;
    public String mNum;
    public String mPhone;
    public String mPrice;
    public String mRestaurantId;
    public String mState;
    public String mTime;
    public String mTotal;
    public String mType;

    public OrderBean() {
        this._id = 0;
        this.mId = "";
        this.mMemberId = "";
        this.mTime = "";
        this.mState = "";
        this.mAddress = "";
        this.mPhone = "";
        this.mDesc = "";
        this.mPrice = "";
        this.mTotal = "";
        this.mRestaurantId = "";
        this.mNum = "";
        this.mCactivtyId = "";
        this.mType = "";
        this.mActiveId = "";
        this.mDesc = "";
    }
}
