package com.gaoding.dingcan.database.controller;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.gaoding.dingcan.database.DataStore;
import com.gaoding.dingcan.database.DatabaseHelper;
import com.gaoding.dingcan.database.bean.AccountBean;

public class UserAccount {
    public static final int STATE_HAS_LOGIN = 1;
    public static final int STATE_LOGIN_FAILED = 2;
    public static final int STATE_NEW_ACCOUNT = 0;
    public AccountBean Item = new AccountBean();
    private DatabaseHelper dbHelper = null;

    public UserAccount(Context context) {
        openDatabase(context);
    }

    public boolean getFromDatabase() {
        if (query() > 0) {
            return true;
        }
        return false;
    }

    public boolean setToDatabase() {
        if (getCount() > 0) {
            update(this.Item);
        } else {
            insert(this.Item);
        }
        return getFromDatabase();
    }

    private void openDatabase(Context context) {
        this.dbHelper = new DatabaseHelper(context);
    }

    public void close() {
        this.dbHelper.close();
    }

    public int query() {
        int result = 0;
        try {
            SQLiteDatabase db = this.dbHelper.getReadableDatabase();
            Cursor cursor = db.query(DataStore.UserTable.TABLE_NAME, null, null, null, null, null, null);
            while (cursor.moveToNext()) {
                this.Item._id = cursor.getInt(cursor.getColumnIndex("_id"));
                this.Item.mUid = cursor.getString(cursor.getColumnIndex("uid"));
                this.Item.mAccount = cursor.getString(cursor.getColumnIndex(DataStore.UserTable.USER_ACCOUNT));
                this.Item.mPassword = cursor.getString(cursor.getColumnIndex(DataStore.UserTable.USER_PASSWORD));
                this.Item.mState = cursor.getInt(cursor.getColumnIndex("state"));
                this.Item.mType = cursor.getInt(cursor.getColumnIndex("type"));
                result++;
            }
            cursor.close();
            db.close();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    public boolean insert(AccountBean account) {
        try {
            SQLiteDatabase db = this.dbHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("uid", account.mUid);
            values.put(DataStore.UserTable.USER_ACCOUNT, account.mAccount);
            values.put(DataStore.UserTable.USER_PASSWORD, account.mPassword);
            values.put("state", Integer.valueOf(account.mState));
            values.put("type", Integer.valueOf(account.mType));
            db.insert(DataStore.UserTable.TABLE_NAME, null, values);
            db.close();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean update(AccountBean account) {
        try {
            SQLiteDatabase db = this.dbHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("uid", account.mUid);
            values.put(DataStore.UserTable.USER_ACCOUNT, account.mAccount);
            values.put(DataStore.UserTable.USER_PASSWORD, account.mPassword);
            values.put("state", Integer.valueOf(account.mState));
            values.put("type", Integer.valueOf(account.mType));
            db.update(DataStore.UserTable.TABLE_NAME, values, "_id = ?", new String[]{String.valueOf(account._id)});
            db.close();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private boolean delete(int _id) {
        try {
            SQLiteDatabase db = this.dbHelper.getWritableDatabase();
            db.delete(DataStore.UserTable.TABLE_NAME, "_id = ?", new String[]{String.valueOf(_id)});
            db.close();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean delete() {
        if (this.Item._id != 0) {
            return delete(this.Item._id);
        }
        return false;
    }

    public int getCount() {
        int result = 0;
        try {
            SQLiteDatabase db = this.dbHelper.getReadableDatabase();
            Cursor cursor = db.query(DataStore.UserTable.TABLE_NAME, null, null, null, null, null, null);
            if (cursor != null) {
                result = cursor.getCount();
            }
            cursor.close();
            db.close();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }
}
