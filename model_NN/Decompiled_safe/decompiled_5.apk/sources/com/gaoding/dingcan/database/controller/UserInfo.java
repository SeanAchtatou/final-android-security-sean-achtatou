package com.gaoding.dingcan.database.controller;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.gaoding.dingcan.database.DataStore;
import com.gaoding.dingcan.database.DatabaseHelper;
import com.gaoding.dingcan.database.bean.InfoBean;

public class UserInfo {
    public static final int STATE_HAS_LOGIN = 1;
    public static final int STATE_LOGIN_FAILED = 2;
    public static final int STATE_NEW_ACCOUNT = 0;
    public InfoBean Item = new InfoBean();
    private DatabaseHelper dbHelper = null;

    public UserInfo(Context context) {
        openDatabase(context);
    }

    public boolean getFromDatabase() {
        if (query() > 0) {
            return true;
        }
        return false;
    }

    public boolean setToDatabase() {
        if (getCount() > 0) {
            update(this.Item);
        } else {
            insert(this.Item);
        }
        return getFromDatabase();
    }

    private void openDatabase(Context context) {
        this.dbHelper = new DatabaseHelper(context);
    }

    public void close() {
        this.dbHelper.close();
    }

    public int query() {
        int result = 0;
        try {
            SQLiteDatabase db = this.dbHelper.getReadableDatabase();
            Cursor cursor = db.query(DataStore.UserInfoTable.TABLE_NAME, null, null, null, null, null, null);
            while (cursor.moveToNext()) {
                this.Item._id = cursor.getInt(cursor.getColumnIndex("_id"));
                this.Item.mUid = cursor.getString(cursor.getColumnIndex("uid"));
                this.Item.mEmail = cursor.getString(cursor.getColumnIndex(DataStore.UserInfoTable.USER_EMAIL));
                this.Item.mPhone = cursor.getString(cursor.getColumnIndex(DataStore.UserInfoTable.USER_PHONE));
                this.Item.mNickName = cursor.getString(cursor.getColumnIndex("name"));
                this.Item.mIntegration = cursor.getString(cursor.getColumnIndex(DataStore.UserInfoTable.USER_INTEGRATION));
                this.Item.mSecode = cursor.getString(cursor.getColumnIndex(DataStore.UserInfoTable.USER_SECODE));
                this.Item.mCategory = cursor.getString(cursor.getColumnIndex(DataStore.UserInfoTable.USER_CATEGORY));
                this.Item.mStatus = cursor.getString(cursor.getColumnIndex(DataStore.UserInfoTable.USER_STATUS));
                this.Item.mSex = cursor.getString(cursor.getColumnIndex(DataStore.UserInfoTable.USER_SEX));
                this.Item.mAddress = cursor.getString(cursor.getColumnIndex("address"));
                this.Item.mType = cursor.getString(cursor.getColumnIndex("type"));
                result++;
            }
            cursor.close();
            db.close();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    public boolean insert(InfoBean info) {
        try {
            SQLiteDatabase db = this.dbHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("uid", info.mUid);
            values.put(DataStore.UserInfoTable.USER_EMAIL, info.mEmail);
            values.put(DataStore.UserInfoTable.USER_PHONE, info.mPhone);
            values.put("name", info.mNickName);
            values.put(DataStore.UserInfoTable.USER_INTEGRATION, info.mIntegration);
            values.put(DataStore.UserInfoTable.USER_SECODE, info.mSecode);
            values.put(DataStore.UserInfoTable.USER_CATEGORY, info.mCategory);
            values.put(DataStore.UserInfoTable.USER_STATUS, info.mStatus);
            values.put(DataStore.UserInfoTable.USER_SEX, info.mSex);
            values.put("address", info.mAddress);
            values.put("type", info.mType);
            db.insert(DataStore.UserInfoTable.TABLE_NAME, null, values);
            db.close();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean update(InfoBean info) {
        try {
            SQLiteDatabase db = this.dbHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("uid", info.mUid);
            values.put(DataStore.UserInfoTable.USER_EMAIL, info.mEmail);
            values.put(DataStore.UserInfoTable.USER_PHONE, info.mPhone);
            values.put("name", info.mNickName);
            values.put(DataStore.UserInfoTable.USER_INTEGRATION, info.mIntegration);
            values.put(DataStore.UserInfoTable.USER_SECODE, info.mSecode);
            values.put(DataStore.UserInfoTable.USER_CATEGORY, info.mCategory);
            values.put(DataStore.UserInfoTable.USER_STATUS, info.mStatus);
            values.put(DataStore.UserInfoTable.USER_SEX, info.mSex);
            values.put("address", info.mAddress);
            values.put("type", info.mType);
            db.update(DataStore.UserInfoTable.TABLE_NAME, values, "_id = ?", new String[]{String.valueOf(info._id)});
            db.close();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private boolean delete(int _id) {
        try {
            SQLiteDatabase db = this.dbHelper.getWritableDatabase();
            db.delete(DataStore.UserInfoTable.TABLE_NAME, "_id = ?", new String[]{String.valueOf(_id)});
            db.close();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean delete() {
        if (this.Item._id != 0) {
            return delete(this.Item._id);
        }
        return false;
    }

    public int getCount() {
        int result = 0;
        try {
            SQLiteDatabase db = this.dbHelper.getReadableDatabase();
            Cursor cursor = db.query(DataStore.UserInfoTable.TABLE_NAME, null, null, null, null, null, null);
            if (cursor != null) {
                result = cursor.getCount();
            }
            cursor.close();
            db.close();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }
}
