package com.gaoding.dingcan.database.bean;

public class InfoBean {
    public int _id = 0;
    public String mAddress = "";
    public String mCategory = "";
    public String mDetetime = "";
    public String mEmail = "";
    public String mIntegration = "";
    public String mNickName = "";
    public String mPhone = "";
    public String mSecode = "";
    public String mSex = "";
    public String mStatus = "";
    public String mType = "";
    public String mUid = "";
}
