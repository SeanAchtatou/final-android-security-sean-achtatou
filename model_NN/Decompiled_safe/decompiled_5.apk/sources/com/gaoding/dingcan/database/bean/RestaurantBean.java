package com.gaoding.dingcan.database.bean;

public class RestaurantBean {
    public int _id = 0;
    public String mAddress = "";
    public String mHot = "";
    public String mId = "";
    public String mLogo = "";
    public String mName = "";
    public String mNotice = "";
    public String mOpenTime = "";
    public String mOrdersCount = "";
    public String mSendArea = "";
    public String mSendDesc = "";
    public String mSendTime = "";
    public String mSpeed = "";
    public String mStartPrice = "";
    public String mState = "";
    public String mUnitId = "";
}
