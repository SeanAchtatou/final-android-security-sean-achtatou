package com.gaoding.dingcan.database.controller;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.gaoding.dingcan.database.DataStore;
import com.gaoding.dingcan.database.DatabaseHelper;
import com.gaoding.dingcan.database.bean.OrderBean;
import java.util.ArrayList;
import java.util.List;

public class Order {
    private DatabaseHelper dbHelper = null;
    public List<OrderBean> list = new ArrayList();

    public Order(Context context) {
        openDatabase(context);
    }

    public boolean getFromDatabase() {
        if (query() > 0) {
            return true;
        }
        return false;
    }

    public boolean syncToDatabase(List<OrderBean> list2) {
        clean();
        for (int i = 0; i < list2.size(); i++) {
            insert(list2.get(i));
        }
        query();
        return false;
    }

    public boolean addToDatabase(List<OrderBean> list2) {
        for (int i = 0; i < list2.size(); i++) {
            insert(list2.get(i));
        }
        query();
        return false;
    }

    public boolean setToDatabase(OrderBean item) {
        boolean update = false;
        int i = 0;
        while (true) {
            if (i >= this.list.size()) {
                break;
            } else if (this.list.get(i).mId.equals(item.mId)) {
                item._id = this.list.get(i)._id;
                this.list.get(i).mMemberId = item.mMemberId;
                this.list.get(i).mTime = item.mTime;
                this.list.get(i).mState = item.mState;
                this.list.get(i).mAddress = item.mAddress;
                this.list.get(i).mPhone = item.mPhone;
                this.list.get(i).mDesc = item.mDesc;
                this.list.get(i).mPrice = item.mPrice;
                this.list.get(i).mTotal = item.mTotal;
                this.list.get(i).mRestaurantId = item.mRestaurantId;
                this.list.get(i).mNum = item.mNum;
                this.list.get(i).mCactivtyId = item.mCactivtyId;
                this.list.get(i).mType = item.mType;
                this.list.get(i).mActiveId = item.mActiveId;
                update = true;
                update(item);
                break;
            } else {
                i++;
            }
        }
        if (update) {
            return true;
        }
        insert(item);
        query();
        return true;
    }

    private void openDatabase(Context context) {
        this.dbHelper = new DatabaseHelper(context);
    }

    public void close() {
        this.dbHelper.close();
    }

    public int query() {
        this.list.clear();
        int result = 0;
        try {
            SQLiteDatabase db = this.dbHelper.getReadableDatabase();
            Cursor cursor = db.query(DataStore.OrderTable.TABLE_NAME, null, null, null, null, null, null);
            while (cursor.moveToNext()) {
                OrderBean item = new OrderBean();
                item._id = cursor.getInt(cursor.getColumnIndex("_id"));
                item.mId = cursor.getString(cursor.getColumnIndex("id"));
                item.mMemberId = cursor.getString(cursor.getColumnIndex(DataStore.OrderTable.MEMBER_ID));
                item.mTime = cursor.getString(cursor.getColumnIndex(DataStore.OrderTable.TIME));
                item.mState = cursor.getString(cursor.getColumnIndex("state"));
                item.mAddress = cursor.getString(cursor.getColumnIndex("address"));
                item.mPhone = cursor.getString(cursor.getColumnIndex(DataStore.OrderTable.PHONE));
                item.mDesc = cursor.getString(cursor.getColumnIndex("desc"));
                item.mPrice = cursor.getString(cursor.getColumnIndex("price"));
                item.mTotal = cursor.getString(cursor.getColumnIndex(DataStore.OrderTable.TOTAL));
                item.mRestaurantId = cursor.getString(cursor.getColumnIndex("restaurantid"));
                item.mNum = cursor.getString(cursor.getColumnIndex(DataStore.OrderTable.NUM));
                item.mCactivtyId = cursor.getString(cursor.getColumnIndex(DataStore.OrderTable.CACTIVTY_ID));
                item.mType = cursor.getString(cursor.getColumnIndex("type"));
                item.mActiveId = cursor.getString(cursor.getColumnIndex(DataStore.OrderTable.ACTIVE_ID));
                result++;
                this.list.add(item);
            }
            cursor.close();
            db.close();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    public boolean insert(OrderBean item) {
        try {
            SQLiteDatabase db = this.dbHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("id", item.mId);
            values.put(DataStore.OrderTable.MEMBER_ID, item.mMemberId);
            values.put(DataStore.OrderTable.TIME, item.mTime);
            values.put("state", item.mState);
            values.put("address", item.mAddress);
            values.put(DataStore.OrderTable.PHONE, item.mPhone);
            values.put("desc", item.mDesc);
            values.put("price", item.mPrice);
            values.put(DataStore.OrderTable.TOTAL, item.mTotal);
            values.put("restaurantid", item.mRestaurantId);
            values.put(DataStore.OrderTable.NUM, item.mNum);
            values.put(DataStore.OrderTable.CACTIVTY_ID, item.mCactivtyId);
            values.put("type", item.mType);
            values.put(DataStore.OrderTable.ACTIVE_ID, item.mActiveId);
            db.insert(DataStore.OrderTable.TABLE_NAME, null, values);
            db.close();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean update(OrderBean item) {
        try {
            SQLiteDatabase db = this.dbHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("id", item.mId);
            values.put(DataStore.OrderTable.MEMBER_ID, item.mMemberId);
            values.put(DataStore.OrderTable.TIME, item.mTime);
            values.put("state", item.mState);
            values.put("address", item.mAddress);
            values.put(DataStore.OrderTable.PHONE, item.mPhone);
            values.put("desc", item.mDesc);
            values.put("price", item.mPrice);
            values.put(DataStore.OrderTable.TOTAL, item.mTotal);
            values.put("restaurantid", item.mRestaurantId);
            values.put(DataStore.OrderTable.NUM, item.mNum);
            values.put(DataStore.OrderTable.CACTIVTY_ID, item.mCactivtyId);
            values.put("type", item.mType);
            values.put(DataStore.OrderTable.ACTIVE_ID, item.mActiveId);
            db.update(DataStore.OrderTable.TABLE_NAME, values, "_id = ?", new String[]{String.valueOf(item._id)});
            db.close();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean delete(int _id) {
        try {
            SQLiteDatabase db = this.dbHelper.getWritableDatabase();
            db.delete(DataStore.OrderTable.TABLE_NAME, "_id = ?", new String[]{String.valueOf(_id)});
            db.close();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private boolean clean() {
        try {
            SQLiteDatabase db = this.dbHelper.getWritableDatabase();
            db.delete(DataStore.OrderTable.TABLE_NAME, null, null);
            db.close();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public int getCount() {
        int result = 0;
        try {
            SQLiteDatabase db = this.dbHelper.getReadableDatabase();
            Cursor cursor = db.query(DataStore.OrderTable.TABLE_NAME, null, null, null, null, null, null);
            if (cursor != null) {
                result = cursor.getCount();
            }
            cursor.close();
            db.close();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }
}
