package com.gaoding.dingcan.database;

import android.provider.BaseColumns;

public class DataStore {
    public static final String AUTHORITY = "com.gaoding.dingcan";
    public static final String BASE_CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.gaoding.";
    public static final String BASE_CONTENT_TYPE = "vnd.android.cursor.dir/vnd.gaoding.";
    public static final String BASE_CONTENT_URI = "content://com.gaoding.dingcan/";

    public static class AreaTable implements BaseColumns {
        public static final String AREA_DESC = "desc";
        public static final String AREA_ID = "id";
        public static final String AREA_NAME = "name";
        public static final String AREA_NEW = "new";
        public static final String AREA_X = "x";
        public static final String AREA_Y = "y";
        public static final String CITY_ID = "cityid";
        public static final String TABLE_NAME = "area";
    }

    public static class CityTable implements BaseColumns {
        public static final String CITY_DESC = "desc";
        public static final String CITY_ID = "id";
        public static final String CITY_NAME = "name";
        public static final String CITY_X = "x";
        public static final String CITY_Y = "y";
        public static final String TABLE_NAME = "city";
    }

    public static class MenuTable implements BaseColumns {
        public static final String ID = "id";
        public static final String IS_NEW = "isnew";
        public static final String NAME = "name";
        public static final String PIC = "pic";
        public static final String PRICE = "price";
        public static final String RESTAURANT_ID = "restaurantid";
        public static final String TABLE_NAME = "menu";
        public static final String TYPE_ID = "typeid";
    }

    public static class MenuTypeTable implements BaseColumns {
        public static final String DESC = "desc";
        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String RESTAURANT_ID = "restaurantid";
        public static final String TABLE_NAME = "menutype";
    }

    public static class OrderMenuTable implements BaseColumns {
        public static final String COUNT = "count";
        public static final String ID = "id";
        public static final String MENU_ID = "menuid";
        public static final String NAME = "name";
        public static final String PRICE = "price";
        public static final String TABLE_NAME = "ordermenu";
        public static final String TOTAL_MONEY = "totalmoney";
    }

    public static class OrderTable implements BaseColumns {
        public static final String ACTIVE_ID = "activeid";
        public static final String ADDRESS = "address";
        public static final String CACTIVTY_ID = "cactivtyid";
        public static final String DESC = "desc";
        public static final String ID = "id";
        public static final String MEMBER_ID = "memberid";
        public static final String NUM = "num";
        public static final String PHONE = "phone";
        public static final String PRICE = "price";
        public static final String RESTAURANT_ID = "restaurantid";
        public static final String STATE = "state";
        public static final String TABLE_NAME = "menuorder";
        public static final String TIME = "time";
        public static final String TOTAL = "total";
        public static final String TYPE = "type";
    }

    public static class RestaurantTable implements BaseColumns {
        public static final String ADDRESS = "address";
        public static final String FOOD_SPEED = "foodspeed";
        public static final String HOT = "hot";
        public static final String ID = "id";
        public static final String LOGO = "logo";
        public static final String NAME = "name";
        public static final String NOTICE = "notice";
        public static final String OPEN_TIME = "opentime";
        public static final String ORDERS_COUNT = "orderscount";
        public static final String SEND_AREA = "sendarea";
        public static final String SEND_DESC = "senddesc";
        public static final String SEND_TIME = "sendtime";
        public static final String START_PRICE = "startprice";
        public static final String STATE = "state";
        public static final String TABLE_NAME = "restaurant";
        public static final String UNIT_ID = "unitid";
    }

    public static class ShoppingCartTable implements BaseColumns {
        public static final String RESTAURANT_ID = "restaurantid";
        public static final String SEND_PRICE = "sendprice";
        public static final String STATE = "state";
        public static final String TABLE_NAME = "shoppingcart";
        public static final String UNIT_NAME = "unitname";
    }

    public static class ShoppingTable implements BaseColumns {
        public static final String COUNT = "count";
        public static final String ID = "id";
        public static final String MENU_ID = "menuid";
        public static final String NAME = "name";
        public static final String PRICE = "price";
        public static final String TABLE_NAME = "shopping";
    }

    public static class UnitTable implements BaseColumns {
        public static final String AREA_ID = "areaid";
        public static final String TABLE_NAME = "unit";
        public static final String UNIT_DESC = "desc";
        public static final String UNIT_ID = "id";
        public static final String UNIT_NAME = "name";
        public static final String UNIT_X = "x";
        public static final String UNIT_Y = "y";
    }

    public static class UserInfoTable implements BaseColumns {
        public static final String TABLE_NAME = "info";
        public static final String USER_ADDRESS = "address";
        public static final String USER_CATEGORY = "category";
        public static final String USER_EMAIL = "email";
        public static final String USER_ID = "uid";
        public static final String USER_INTEGRATION = "integration";
        public static final String USER_NAME = "name";
        public static final String USER_PHONE = "telephone";
        public static final String USER_SECODE = "secode";
        public static final String USER_SEX = "sex";
        public static final String USER_STATUS = "status";
        public static final String USER_TYPE = "type";
    }

    public static class UserTable implements BaseColumns {
        public static final String TABLE_NAME = "user";
        public static final String USER_ACCOUNT = "account";
        public static final String USER_ID = "uid";
        public static final String USER_PASSWORD = "password";
        public static final String USER_STATE = "state";
        public static final String USER_TYPE = "type";
    }
}
