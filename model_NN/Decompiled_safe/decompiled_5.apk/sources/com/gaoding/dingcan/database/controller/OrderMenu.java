package com.gaoding.dingcan.database.controller;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.gaoding.dingcan.database.DataStore;
import com.gaoding.dingcan.database.DatabaseHelper;
import com.gaoding.dingcan.database.bean.OrderMenuBean;
import java.util.ArrayList;
import java.util.List;

public class OrderMenu {
    private DatabaseHelper dbHelper = null;
    public List<OrderMenuBean> list = new ArrayList();

    public OrderMenu(Context context) {
        openDatabase(context);
    }

    public boolean getFromDatabase() {
        if (query() > 0) {
            return true;
        }
        return false;
    }

    public boolean syncToDatabase(List<OrderMenuBean> list2) {
        clean();
        for (int i = 0; i < list2.size(); i++) {
            insert(list2.get(i));
        }
        query();
        return false;
    }

    public boolean addToDatabase(List<OrderMenuBean> list2) {
        for (int i = 0; i < list2.size(); i++) {
            insert(list2.get(i));
        }
        query();
        return false;
    }

    public boolean setToDatabase(OrderMenuBean item) {
        boolean update = false;
        int i = 0;
        while (true) {
            if (i >= this.list.size()) {
                break;
            } else if (this.list.get(i).mId.equals(item.mId)) {
                item._id = this.list.get(i)._id;
                this.list.get(i).mMenuId = item.mMenuId;
                this.list.get(i).mName = item.mName;
                this.list.get(i).mPrice = item.mPrice;
                this.list.get(i).mCount = item.mCount;
                this.list.get(i).mTotalMoney = item.mTotalMoney;
                update = true;
                update(item);
                break;
            } else {
                i++;
            }
        }
        if (update) {
            return true;
        }
        insert(item);
        query();
        return true;
    }

    private void openDatabase(Context context) {
        this.dbHelper = new DatabaseHelper(context);
    }

    public void close() {
        this.dbHelper.close();
    }

    public int query() {
        this.list.clear();
        int result = 0;
        try {
            SQLiteDatabase db = this.dbHelper.getReadableDatabase();
            Cursor cursor = db.query(DataStore.OrderMenuTable.TABLE_NAME, null, null, null, null, null, null);
            while (cursor.moveToNext()) {
                OrderMenuBean item = new OrderMenuBean();
                item._id = cursor.getInt(cursor.getColumnIndex("_id"));
                item.mId = cursor.getString(cursor.getColumnIndex("id"));
                item.mMenuId = cursor.getString(cursor.getColumnIndex("menuid"));
                item.mName = cursor.getString(cursor.getColumnIndex("name"));
                item.mPrice = cursor.getString(cursor.getColumnIndex("price"));
                item.mCount = cursor.getString(cursor.getColumnIndex("count"));
                item.mTotalMoney = cursor.getString(cursor.getColumnIndex(DataStore.OrderMenuTable.TOTAL_MONEY));
                result++;
                this.list.add(item);
            }
            cursor.close();
            db.close();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    public boolean insert(OrderMenuBean item) {
        try {
            SQLiteDatabase db = this.dbHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("id", item.mId);
            values.put("menuid", item.mMenuId);
            values.put("name", item.mName);
            values.put("price", item.mPrice);
            values.put("count", item.mCount);
            values.put(DataStore.OrderMenuTable.TOTAL_MONEY, item.mTotalMoney);
            db.insert(DataStore.OrderMenuTable.TABLE_NAME, null, values);
            db.close();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean update(OrderMenuBean item) {
        try {
            SQLiteDatabase db = this.dbHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("id", item.mId);
            values.put("menuid", item.mMenuId);
            values.put("name", item.mName);
            values.put("price", item.mPrice);
            values.put("count", item.mCount);
            values.put(DataStore.OrderMenuTable.TOTAL_MONEY, item.mTotalMoney);
            db.update(DataStore.OrderMenuTable.TABLE_NAME, values, "_id = ?", new String[]{String.valueOf(item._id)});
            db.close();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean delete(int _id) {
        try {
            SQLiteDatabase db = this.dbHelper.getWritableDatabase();
            db.delete(DataStore.OrderMenuTable.TABLE_NAME, "_id = ?", new String[]{String.valueOf(_id)});
            db.close();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private boolean clean() {
        try {
            SQLiteDatabase db = this.dbHelper.getWritableDatabase();
            db.delete(DataStore.OrderMenuTable.TABLE_NAME, null, null);
            db.close();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public int getCount() {
        int result = 0;
        try {
            SQLiteDatabase db = this.dbHelper.getReadableDatabase();
            Cursor cursor = db.query(DataStore.OrderMenuTable.TABLE_NAME, null, null, null, null, null, null);
            if (cursor != null) {
                result = cursor.getCount();
            }
            cursor.close();
            db.close();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }
}
