package com.gaoding.dingcan.database.controller;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.gaoding.dingcan.database.DataStore;
import com.gaoding.dingcan.database.DatabaseHelper;
import com.gaoding.dingcan.database.bean.RestaurantBean;
import java.util.ArrayList;
import java.util.List;

public class Restaurant {
    private DatabaseHelper dbHelper = null;
    public List<RestaurantBean> list = new ArrayList();

    public Restaurant(Context context) {
        openDatabase(context);
    }

    public boolean getFromDatabase() {
        if (query() > 0) {
            return true;
        }
        return false;
    }

    public boolean syncToDatabase(List<RestaurantBean> list2) {
        clean();
        for (int i = 0; i < list2.size(); i++) {
            insert(list2.get(i));
        }
        query();
        return false;
    }

    public boolean addToDatabase(List<RestaurantBean> list2) {
        for (int i = 0; i < list2.size(); i++) {
            insert(list2.get(i));
        }
        query();
        return false;
    }

    public boolean setToDatabase(RestaurantBean item) {
        boolean update = false;
        int i = 0;
        while (true) {
            if (i >= this.list.size()) {
                break;
            } else if (this.list.get(i).mId.equals(item.mId)) {
                item._id = this.list.get(i)._id;
                this.list.get(i).mUnitId = item.mUnitId;
                this.list.get(i).mName = item.mName;
                this.list.get(i).mLogo = item.mLogo;
                this.list.get(i).mAddress = item.mAddress;
                this.list.get(i).mNotice = item.mNotice;
                this.list.get(i).mStartPrice = item.mStartPrice;
                this.list.get(i).mSendTime = item.mSendTime;
                this.list.get(i).mSendDesc = item.mSendDesc;
                this.list.get(i).mState = item.mState;
                this.list.get(i).mOpenTime = item.mOpenTime;
                this.list.get(i).mSendArea = item.mSendArea;
                this.list.get(i).mSpeed = item.mSpeed;
                this.list.get(i).mHot = item.mHot;
                this.list.get(i).mOrdersCount = item.mOrdersCount;
                update = true;
                update(item);
                break;
            } else {
                i++;
            }
        }
        if (update) {
            return true;
        }
        insert(item);
        query();
        return true;
    }

    private void openDatabase(Context context) {
        this.dbHelper = new DatabaseHelper(context);
    }

    public void close() {
        this.dbHelper.close();
    }

    public int query() {
        this.list.clear();
        int result = 0;
        try {
            SQLiteDatabase db = this.dbHelper.getReadableDatabase();
            Cursor cursor = db.query(DataStore.RestaurantTable.TABLE_NAME, null, null, null, null, null, null);
            while (cursor.moveToNext()) {
                RestaurantBean item = new RestaurantBean();
                item._id = cursor.getInt(cursor.getColumnIndex("_id"));
                item.mId = cursor.getString(cursor.getColumnIndex("id"));
                item.mUnitId = cursor.getString(cursor.getColumnIndex(DataStore.RestaurantTable.UNIT_ID));
                item.mName = cursor.getString(cursor.getColumnIndex("name"));
                item.mLogo = cursor.getString(cursor.getColumnIndex(DataStore.RestaurantTable.LOGO));
                item.mAddress = cursor.getString(cursor.getColumnIndex("address"));
                item.mNotice = cursor.getString(cursor.getColumnIndex(DataStore.RestaurantTable.NOTICE));
                item.mStartPrice = cursor.getString(cursor.getColumnIndex(DataStore.RestaurantTable.START_PRICE));
                item.mSendTime = cursor.getString(cursor.getColumnIndex(DataStore.RestaurantTable.SEND_TIME));
                item.mSendDesc = cursor.getString(cursor.getColumnIndex(DataStore.RestaurantTable.SEND_DESC));
                item.mState = cursor.getString(cursor.getColumnIndex("state"));
                item.mOpenTime = cursor.getString(cursor.getColumnIndex(DataStore.RestaurantTable.OPEN_TIME));
                item.mSendArea = cursor.getString(cursor.getColumnIndex(DataStore.RestaurantTable.SEND_AREA));
                item.mSpeed = cursor.getString(cursor.getColumnIndex(DataStore.RestaurantTable.FOOD_SPEED));
                item.mHot = cursor.getString(cursor.getColumnIndex(DataStore.RestaurantTable.HOT));
                item.mOrdersCount = cursor.getString(cursor.getColumnIndex(DataStore.RestaurantTable.ORDERS_COUNT));
                result++;
                this.list.add(item);
            }
            cursor.close();
            db.close();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    public boolean insert(RestaurantBean item) {
        try {
            SQLiteDatabase db = this.dbHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("id", item.mId);
            values.put(DataStore.RestaurantTable.UNIT_ID, item.mUnitId);
            values.put("name", item.mName);
            values.put(DataStore.RestaurantTable.LOGO, item.mLogo);
            values.put("address", item.mAddress);
            values.put(DataStore.RestaurantTable.NOTICE, item.mNotice);
            values.put(DataStore.RestaurantTable.START_PRICE, item.mStartPrice);
            values.put(DataStore.RestaurantTable.SEND_TIME, item.mSendTime);
            values.put(DataStore.RestaurantTable.SEND_DESC, item.mSendDesc);
            values.put("state", item.mState);
            values.put(DataStore.RestaurantTable.OPEN_TIME, item.mOpenTime);
            values.put(DataStore.RestaurantTable.SEND_AREA, item.mSendArea);
            values.put(DataStore.RestaurantTable.FOOD_SPEED, item.mSpeed);
            values.put(DataStore.RestaurantTable.HOT, item.mHot);
            values.put(DataStore.RestaurantTable.ORDERS_COUNT, item.mOrdersCount);
            db.insert(DataStore.RestaurantTable.TABLE_NAME, null, values);
            db.close();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean update(RestaurantBean item) {
        try {
            SQLiteDatabase db = this.dbHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("id", item.mId);
            values.put(DataStore.RestaurantTable.UNIT_ID, item.mUnitId);
            values.put("name", item.mName);
            values.put(DataStore.RestaurantTable.LOGO, item.mLogo);
            values.put("address", item.mAddress);
            values.put(DataStore.RestaurantTable.NOTICE, item.mNotice);
            values.put(DataStore.RestaurantTable.START_PRICE, item.mStartPrice);
            values.put(DataStore.RestaurantTable.SEND_TIME, item.mSendTime);
            values.put(DataStore.RestaurantTable.SEND_DESC, item.mSendDesc);
            values.put("state", item.mState);
            values.put(DataStore.RestaurantTable.OPEN_TIME, item.mOpenTime);
            values.put(DataStore.RestaurantTable.SEND_AREA, item.mSendArea);
            values.put(DataStore.RestaurantTable.FOOD_SPEED, item.mSpeed);
            values.put(DataStore.RestaurantTable.HOT, item.mHot);
            values.put(DataStore.RestaurantTable.ORDERS_COUNT, item.mOrdersCount);
            db.update(DataStore.RestaurantTable.TABLE_NAME, values, "_id = ?", new String[]{String.valueOf(item._id)});
            db.close();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean delete(int _id) {
        try {
            SQLiteDatabase db = this.dbHelper.getWritableDatabase();
            db.delete(DataStore.RestaurantTable.TABLE_NAME, "_id = ?", new String[]{String.valueOf(_id)});
            db.close();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private boolean clean() {
        try {
            SQLiteDatabase db = this.dbHelper.getWritableDatabase();
            db.delete(DataStore.RestaurantTable.TABLE_NAME, null, null);
            db.close();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public int getCount() {
        int result = 0;
        try {
            SQLiteDatabase db = this.dbHelper.getReadableDatabase();
            Cursor cursor = db.query(DataStore.RestaurantTable.TABLE_NAME, null, null, null, null, null, null);
            if (cursor != null) {
                result = cursor.getCount();
            }
            cursor.close();
            db.close();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }
}
