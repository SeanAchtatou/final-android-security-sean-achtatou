package com.gaoding.dingcan.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.database.DataSetObserver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.MotionEventCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.gaoding.dingcan.R;
import com.gaoding.dingcan.database.bean.RestaurantBean;
import com.gaoding.dingcan.database.controller.Restaurant;
import com.gaoding.dingcan.http.Asset;
import com.gaoding.dingcan.http.HttpResult;
import com.gaoding.dingcan.http.ProxyFactory;
import com.gaoding.dingcan.http.controller.GetRestaurant;
import com.gaoding.dingcan.http.controller.GetRestaurantInfo;
import com.gaoding.dingcan.util.AsyncImageLoader;
import com.gaoding.dingcan.util.LogUtils;
import com.gaoding.dingcan.util.Utils;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;

public class RestaurantListActivity extends Activity implements View.OnClickListener {
    private static final int DISMISS_PROGRESS_DIALOG = 3;
    private static final int REFRESH_VIEW = 1;
    private static final int SHOW_PROGRESS_DIALOG = 2;
    private static final int SHOW_TOAST_MESSAGE = 4;
    private String UnitId;
    /* access modifiers changed from: private */
    public String UnitName;
    private AsyncImageLoader asyncImageLoader;
    private ImageView btnBack;
    /* access modifiers changed from: private */
    public MessageAdapter listAdapter;
    private ListView listView;
    private LinearLayout mLayoutProgress;
    /* access modifiers changed from: private */
    public ProgressDialog mProgressDialog;
    /* access modifiers changed from: private */
    public Handler myHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    try {
                        RestaurantListActivity.this.restaurant.query();
                        RestaurantListActivity.this.listAdapter.notifyDataSetChanged();
                        return;
                    } catch (Exception e) {
                        e.printStackTrace();
                        return;
                    }
                case 2:
                    try {
                        if (RestaurantListActivity.this.mProgressDialog != null) {
                            if (RestaurantListActivity.this.mProgressDialog.isShowing()) {
                                RestaurantListActivity.this.mProgressDialog.dismiss();
                            }
                            RestaurantListActivity.this.mProgressDialog = null;
                        }
                        RestaurantListActivity.this.mProgressDialog = Utils.getProgressDialog(RestaurantListActivity.this, (String) msg.obj);
                        RestaurantListActivity.this.mProgressDialog.show();
                        return;
                    } catch (Exception e2) {
                        e2.printStackTrace();
                        return;
                    }
                case 3:
                    try {
                        if (RestaurantListActivity.this.mProgressDialog != null && RestaurantListActivity.this.mProgressDialog.isShowing()) {
                            RestaurantListActivity.this.mProgressDialog.dismiss();
                        }
                        if (RestaurantListActivity.this.restaurant.list.size() == 0) {
                            RestaurantListActivity.this.tvNoInfo.setVisibility(0);
                        } else {
                            RestaurantListActivity.this.tvNoInfo.setVisibility(8);
                        }
                        RestaurantListActivity.this.listAdapter.notifyDataSetChanged();
                        return;
                    } catch (Exception e3) {
                        e3.printStackTrace();
                        return;
                    }
                default:
                    return;
            }
        }
    };
    /* access modifiers changed from: private */
    public Resources resources;
    /* access modifiers changed from: private */
    public Restaurant restaurant;
    /* access modifiers changed from: private */
    public TextView tvNoInfo;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LogUtils.logDebug("AreaListActivity");
        setContentView((int) R.layout.activity_restaurant);
        this.resources = getResources();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            this.UnitId = bundle.getString("UnitId");
            this.UnitName = bundle.getString("UnitName");
        }
        initViews();
        load();
        this.restaurant = new Restaurant(this);
        this.restaurant.query();
        this.listAdapter = new MessageAdapter(this, null);
        this.listView.setAdapter((ListAdapter) this.listAdapter);
        this.asyncImageLoader = new AsyncImageLoader(this);
    }

    private void initViews() {
        this.btnBack = (ImageView) findViewById(R.id.btnBack);
        this.btnBack.setOnClickListener(this);
        this.listView = (ListView) findViewById(R.id.listview_area);
        this.tvNoInfo = (TextView) findViewById(R.id.tv_no_info);
        this.mLayoutProgress = (LinearLayout) findViewById(R.id.linearlayout_progress);
        this.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                RestaurantBean bean = RestaurantListActivity.this.restaurant.list.get(arg2);
                Intent intent = new Intent();
                intent.setClass(RestaurantListActivity.this, MenuListTabActivity.class);
                intent.putExtra("RestaurantId", bean.mId);
                intent.putExtra("RestaurantName", bean.mName);
                intent.putExtra("UnitName", RestaurantListActivity.this.UnitName);
                RestaurantListActivity.this.startActivity(intent);
            }
        });
    }

    private void load() {
        if (Utils.isNetWorkConnect(this)) {
            this.tvNoInfo.setVisibility(8);
            new GetThread(this.UnitId).start();
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnBack:
                finish();
                return;
            default:
                return;
        }
    }

    private class MessageAdapter extends BaseAdapter {
        private MessageAdapter() {
        }

        /* synthetic */ MessageAdapter(RestaurantListActivity restaurantListActivity, MessageAdapter messageAdapter) {
            this();
        }

        public int getCount() {
            if (RestaurantListActivity.this.restaurant.list == null) {
                return 0;
            }
            return RestaurantListActivity.this.restaurant.list.size();
        }

        public Object getItem(int position) {
            return Integer.valueOf(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public void unregisterDataSetObserver(DataSetObserver observer) {
            if (observer != null) {
                super.unregisterDataSetObserver(observer);
            }
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            Drawable drawable;
            int mPosition = position;
            View view = ((LayoutInflater) RestaurantListActivity.this.getSystemService("layout_inflater")).inflate((int) R.layout.restaurant_item, (ViewGroup) null);
            try {
                HolderViewTopic holderView = new HolderViewTopic(RestaurantListActivity.this, null);
                holderView.mTvTitle = (TextView) view.findViewById(R.id.tv_item_title);
                holderView.mTvAddress = (TextView) view.findViewById(R.id.tv_item_address);
                holderView.mTvSpeed = (TextView) view.findViewById(R.id.tv_item_speed);
                holderView.mImage = (ImageView) view.findViewById(R.id.tv_item_image);
                holderView.mHot = (ImageView) view.findViewById(R.id.tv_item_hot);
                holderView.mOpen = (ImageView) view.findViewById(R.id.tv_item_open);
                try {
                    RestaurantBean bean = RestaurantListActivity.this.restaurant.list.get(mPosition);
                    String str = bean.mName;
                    holderView.mTvTitle.setText(bean.mName);
                    holderView.mTvAddress.setText(bean.mAddress);
                    if (bean.mHot.equals(GetRestaurantInfo.RES_STATE_CLOSE)) {
                        holderView.mHot.setVisibility(8);
                    }
                    if (bean.mState.equals(GetRestaurantInfo.RES_STATE_OPEN)) {
                        holderView.mOpen.setImageResource(R.drawable.open);
                    } else {
                        holderView.mOpen.setImageResource(R.drawable.close);
                    }
                    String logo = bean.mLogo;
                    if (logo != null && !"".equals(logo)) {
                        File file = new File(String.valueOf(AsyncImageLoader.STORE_CACHE_IMAGE_PATH) + logo);
                        if (file.exists()) {
                            try {
                                holderView.mImage.setImageDrawable(Utils.zoomDrawable(new BitmapDrawable(BitmapFactory.decodeFile(file.getAbsolutePath())), 60, 60));
                            } catch (Exception e) {
                                e.printStackTrace();
                                try {
                                    file.delete();
                                } catch (Exception e2) {
                                    e2.printStackTrace();
                                }
                            }
                        }
                    }
                    if (bean.mSpeed == null || bean.mSpeed.equals("")) {
                        bean.mSpeed = GetRestaurantInfo.RES_STATE_CLOSE;
                    }
                    switch (Integer.parseInt(bean.mSpeed)) {
                        case 0:
                            drawable = RestaurantListActivity.this.resources.getDrawable(R.drawable.star6);
                            break;
                        case 1:
                            drawable = RestaurantListActivity.this.resources.getDrawable(R.drawable.star6);
                            break;
                        case 2:
                            drawable = RestaurantListActivity.this.resources.getDrawable(R.drawable.star6);
                            break;
                        case 3:
                            drawable = RestaurantListActivity.this.resources.getDrawable(R.drawable.star6);
                            break;
                        case 4:
                            drawable = RestaurantListActivity.this.resources.getDrawable(R.drawable.star6);
                            break;
                        case 5:
                            drawable = RestaurantListActivity.this.resources.getDrawable(R.drawable.star6);
                            break;
                        case 6:
                            drawable = RestaurantListActivity.this.resources.getDrawable(R.drawable.star6);
                            break;
                        case MotionEventCompat.ACTION_HOVER_MOVE:
                            drawable = RestaurantListActivity.this.resources.getDrawable(R.drawable.star7);
                            break;
                        case 8:
                            drawable = RestaurantListActivity.this.resources.getDrawable(R.drawable.star8);
                            break;
                        case MotionEventCompat.ACTION_HOVER_ENTER:
                            drawable = RestaurantListActivity.this.resources.getDrawable(R.drawable.star9);
                            break;
                        case MotionEventCompat.ACTION_HOVER_EXIT:
                            drawable = RestaurantListActivity.this.resources.getDrawable(R.drawable.star10);
                            break;
                        default:
                            drawable = RestaurantListActivity.this.resources.getDrawable(R.drawable.star6);
                            break;
                    }
                    holderView.mTvSpeed.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, drawable, (Drawable) null);
                } catch (Exception e3) {
                    e3.printStackTrace();
                }
            } catch (Exception e4) {
                e4.printStackTrace();
            }
            return view;
        }
    }

    private class HolderViewTopic {
        /* access modifiers changed from: private */
        public ImageView mHot;
        /* access modifiers changed from: private */
        public ImageView mImage;
        /* access modifiers changed from: private */
        public ImageView mOpen;
        /* access modifiers changed from: private */
        public TextView mTvAddress;
        /* access modifiers changed from: private */
        public TextView mTvSpeed;
        /* access modifiers changed from: private */
        public TextView mTvTitle;

        private HolderViewTopic() {
        }

        /* synthetic */ HolderViewTopic(RestaurantListActivity restaurantListActivity, HolderViewTopic holderViewTopic) {
            this();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.restaurant.close();
        super.onDestroy();
    }

    private class GetThread extends Thread {
        public String mUnitId;

        public GetThread(String mUnitId2) {
            this.mUnitId = mUnitId2;
        }

        public void run() {
            HttpResult<Asset> httpResult;
            super.run();
            Message msg = new Message();
            msg.what = 2;
            msg.obj = RestaurantListActivity.this.resources.getString(R.string.please_wait);
            RestaurantListActivity.this.myHandler.sendMessage(msg);
            try {
                GetRestaurant get = new GetRestaurant(RestaurantListActivity.this);
                get.mUnitId = this.mUnitId;
                if (get.GetList()) {
                    RestaurantListActivity.this.restaurant = get.restaurant;
                    for (int i = 0; i < RestaurantListActivity.this.restaurant.list.size(); i++) {
                        String logo = RestaurantListActivity.this.restaurant.list.get(i).mLogo;
                        if (!(logo == null || logo.equals("") || new File(String.valueOf(AsyncImageLoader.STORE_CACHE_IMAGE_PATH) + logo).exists() || (httpResult = ProxyFactory.getDefaultProxy().download(String.valueOf("http://www.gaoding100.com/Images/resImg/") + logo, null, null)) == null || httpResult.getValue() == null)) {
                            Bitmap mBitmap = RestaurantListActivity.this.Bytes2Bimap(httpResult.getValue().getData());
                            File file = new File(AsyncImageLoader.STORE_CACHE_IMAGE_PATH);
                            if (!file.exists()) {
                                file.mkdirs();
                            }
                            mBitmap.compress(Bitmap.CompressFormat.JPEG, 100, new FileOutputStream(String.valueOf(AsyncImageLoader.STORE_CACHE_IMAGE_PATH) + logo));
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            RestaurantListActivity.this.myHandler.sendEmptyMessage(3);
        }
    }

    private byte[] Bitmap2Bytes(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.PNG, 100, baos);
        return baos.toByteArray();
    }

    public Bitmap Bytes2Bimap(byte[] b) {
        if (b.length != 0) {
            return BitmapFactory.decodeByteArray(b, 0, b.length);
        }
        return null;
    }
}
