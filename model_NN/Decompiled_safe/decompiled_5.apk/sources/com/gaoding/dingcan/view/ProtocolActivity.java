package com.gaoding.dingcan.view;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.gaoding.dingcan.R;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ProtocolActivity extends Activity {
    private ImageView btnBack;
    /* access modifiers changed from: private */
    public Handler mHanlder = new Handler() {
        public void handleMessage(Message msg) {
            if (msg.what == 0) {
                ProtocolActivity.this.mTextViewProtocol.setText((String) msg.obj);
            }
        }
    };
    /* access modifiers changed from: private */
    public TextView mTextViewProtocol;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.protocol_activity);
        this.btnBack = (ImageView) findViewById(R.id.btnBack);
        this.mTextViewProtocol = (TextView) findViewById(R.id.protocol_tv);
        this.btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ProtocolActivity.this.finish();
            }
        });
        new Thread() {
            public void run() {
                super.run();
                String text = ProtocolActivity.this.getProtocolContext();
                Message msg = new Message();
                msg.obj = text;
                ProtocolActivity.this.mHanlder.sendMessage(msg);
            }
        }.start();
    }

    /* access modifiers changed from: private */
    public String getProtocolContext() {
        String mProtocolString = "";
        try {
            BufferedReader mBuffered = new BufferedReader(new InputStreamReader(getBaseContext().getResources().openRawResource(R.raw.protocol)));
            while (true) {
                String temp = mBuffered.readLine();
                if (temp == null) {
                    return mProtocolString;
                }
                mProtocolString = String.valueOf(mProtocolString) + temp + "\n";
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "error read";
        }
    }
}
