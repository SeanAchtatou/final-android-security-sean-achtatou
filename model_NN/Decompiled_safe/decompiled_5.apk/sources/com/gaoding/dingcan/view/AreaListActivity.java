package com.gaoding.dingcan.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.gaoding.dingcan.R;
import com.gaoding.dingcan.database.bean.AreaBean;
import com.gaoding.dingcan.database.controller.Area;
import com.gaoding.dingcan.http.controller.GetArea;
import com.gaoding.dingcan.http.controller.GetRestaurantInfo;
import com.gaoding.dingcan.util.LogUtils;
import com.gaoding.dingcan.util.Utils;

public class AreaListActivity extends Activity implements View.OnClickListener {
    private static final int DISMISS_PROGRESS_DIALOG = 3;
    private static final int REFRESH_VIEW = 1;
    private static final int SHOW_PROGRESS_DIALOG = 2;
    private static final int SHOW_TOAST_MESSAGE = 4;
    private String CityId;
    /* access modifiers changed from: private */
    public Area area;
    private ImageView btnBack;
    /* access modifiers changed from: private */
    public MessageAdapter listAdapter;
    private ListView listView;
    private LinearLayout mLayoutProgress;
    /* access modifiers changed from: private */
    public ProgressDialog mProgressDialog;
    /* access modifiers changed from: private */
    public Handler myHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    try {
                        AreaListActivity.this.area.query();
                        AreaListActivity.this.listAdapter.notifyDataSetChanged();
                        return;
                    } catch (Exception e) {
                        e.printStackTrace();
                        return;
                    }
                case 2:
                    try {
                        if (AreaListActivity.this.mProgressDialog != null) {
                            if (AreaListActivity.this.mProgressDialog.isShowing()) {
                                AreaListActivity.this.mProgressDialog.dismiss();
                            }
                            AreaListActivity.this.mProgressDialog = null;
                        }
                        AreaListActivity.this.mProgressDialog = Utils.getProgressDialog(AreaListActivity.this, (String) msg.obj);
                        AreaListActivity.this.mProgressDialog.show();
                        return;
                    } catch (Exception e2) {
                        e2.printStackTrace();
                        return;
                    }
                case 3:
                    try {
                        if (AreaListActivity.this.mProgressDialog != null && AreaListActivity.this.mProgressDialog.isShowing()) {
                            AreaListActivity.this.mProgressDialog.dismiss();
                        }
                        if (AreaListActivity.this.area.list.size() == 0) {
                            AreaListActivity.this.tvNoInfo.setVisibility(0);
                        } else {
                            AreaListActivity.this.tvNoInfo.setVisibility(8);
                        }
                        AreaListActivity.this.listAdapter.notifyDataSetChanged();
                        return;
                    } catch (Exception e3) {
                        e3.printStackTrace();
                        return;
                    }
                default:
                    return;
            }
        }
    };
    /* access modifiers changed from: private */
    public Resources resources;
    /* access modifiers changed from: private */
    public TextView tvNoInfo;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LogUtils.logDebug("AreaListActivity");
        setContentView((int) R.layout.activity_arealist);
        this.resources = getResources();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            this.CityId = bundle.getString("CityId");
        } else {
            this.CityId = GetRestaurantInfo.RES_STATE_OPEN;
        }
        initViews();
        loadAreas();
        this.area = new Area(this);
        this.area.query();
        this.listAdapter = new MessageAdapter(this, null);
        this.listView.setAdapter((ListAdapter) this.listAdapter);
    }

    private void initViews() {
        this.btnBack = (ImageView) findViewById(R.id.btnBack);
        this.btnBack.setOnClickListener(this);
        this.listView = (ListView) findViewById(R.id.listview_area);
        this.tvNoInfo = (TextView) findViewById(R.id.tv_no_info);
        this.mLayoutProgress = (LinearLayout) findViewById(R.id.linearlayout_progress);
        this.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                AreaBean bean = AreaListActivity.this.area.list.get(arg2);
                Intent intent = new Intent();
                intent.setClass(AreaListActivity.this, UnitListActivity.class);
                intent.putExtra("AreaId", bean.mId);
                LogUtils.logDebug("AreaId = " + bean.mId);
                AreaListActivity.this.startActivity(intent);
            }
        });
    }

    private void loadAreas() {
        if (Utils.isNetWorkConnect(this)) {
            this.tvNoInfo.setVisibility(8);
            new GetAreaThread(this.CityId).start();
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnBack:
                finish();
                return;
            default:
                return;
        }
    }

    private class MessageAdapter extends BaseAdapter {
        private MessageAdapter() {
        }

        /* synthetic */ MessageAdapter(AreaListActivity areaListActivity, MessageAdapter messageAdapter) {
            this();
        }

        public int getCount() {
            if (AreaListActivity.this.area.list == null) {
                return 0;
            }
            return AreaListActivity.this.area.list.size();
        }

        public Object getItem(int position) {
            return Integer.valueOf(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public void unregisterDataSetObserver(DataSetObserver observer) {
            if (observer != null) {
                super.unregisterDataSetObserver(observer);
            }
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            int mPosition = position;
            View view = ((LayoutInflater) AreaListActivity.this.getSystemService("layout_inflater")).inflate((int) R.layout.msg_item, (ViewGroup) null);
            try {
                HolderViewTopic holderView = new HolderViewTopic(AreaListActivity.this, null);
                holderView.mTvTitle = (TextView) view.findViewById(R.id.tv_item_title);
                try {
                    holderView.mTvTitle.setText(AreaListActivity.this.area.list.get(mPosition).mName);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            return view;
        }
    }

    private class HolderViewTopic {
        /* access modifiers changed from: private */
        public TextView mTvTitle;

        private HolderViewTopic() {
        }

        /* synthetic */ HolderViewTopic(AreaListActivity areaListActivity, HolderViewTopic holderViewTopic) {
            this();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.area.close();
        super.onDestroy();
    }

    private class GetAreaThread extends Thread {
        public String mCityId;

        public GetAreaThread(String mCityId2) {
            this.mCityId = mCityId2;
        }

        public void run() {
            super.run();
            Message msg = new Message();
            msg.what = 2;
            msg.obj = AreaListActivity.this.resources.getString(R.string.please_wait);
            AreaListActivity.this.myHandler.sendMessage(msg);
            try {
                GetArea get = new GetArea(AreaListActivity.this);
                get.mCityId = this.mCityId;
                if (get.GetList()) {
                    AreaListActivity.this.area = get.area;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            AreaListActivity.this.myHandler.sendEmptyMessage(3);
        }
    }
}
