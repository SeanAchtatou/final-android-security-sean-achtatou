package com.gaoding.dingcan.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import com.gaoding.dingcan.R;
import com.gaoding.dingcan.database.controller.UserInfo;
import com.gaoding.dingcan.util.LogUtils;
import com.gaoding.dingcan.util.Utils;

public class UserInfoActivity extends Activity implements View.OnClickListener {
    private static final int DISMISS_PROGRESS_DIALOG = 3;
    private static final int REFRESH_VIEW = 1;
    private static final int SHOW_PROGRESS_DIALOG = 2;
    private static final int SHOW_TOAST_MESSAGE = 4;
    private Button btConfirm;
    private ImageView btnBack;
    private EditText etAddress;
    private EditText etPhone;
    private UserInfo info;
    /* access modifiers changed from: private */
    public ProgressDialog mProgressDialog;
    private Handler myHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                default:
                    return;
                case 2:
                    try {
                        if (UserInfoActivity.this.mProgressDialog != null) {
                            if (UserInfoActivity.this.mProgressDialog.isShowing()) {
                                UserInfoActivity.this.mProgressDialog.dismiss();
                            }
                            UserInfoActivity.this.mProgressDialog = null;
                        }
                        UserInfoActivity.this.mProgressDialog = Utils.getProgressDialog(UserInfoActivity.this, (String) msg.obj);
                        UserInfoActivity.this.mProgressDialog.show();
                        return;
                    } catch (Exception e) {
                        e.printStackTrace();
                        return;
                    }
                case 3:
                    try {
                        if (UserInfoActivity.this.mProgressDialog != null && UserInfoActivity.this.mProgressDialog.isShowing()) {
                            UserInfoActivity.this.mProgressDialog.dismiss();
                            return;
                        }
                        return;
                    } catch (Exception e2) {
                        e2.printStackTrace();
                        return;
                    }
            }
        }
    };
    private Resources resources;
    private String strAddress;
    private String strPhone;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LogUtils.logDebug("MenuListActivity");
        setContentView((int) R.layout.activity_user_info);
        this.resources = getResources();
        Bundle extras = getIntent().getExtras();
        initViews();
        this.info = new UserInfo(this);
        this.info.query();
        this.etAddress.setText(this.info.Item.mAddress);
        this.etPhone.setText(this.info.Item.mPhone);
    }

    private void initViews() {
        this.btnBack = (ImageView) findViewById(R.id.btnBack);
        this.btnBack.setOnClickListener(this);
        this.btConfirm = (Button) findViewById(R.id.userConfirm);
        this.btConfirm.setOnClickListener(this);
        this.etAddress = (EditText) findViewById(R.id.userAddress);
        this.etPhone = (EditText) findViewById(R.id.userPhone);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnBack:
                finish();
                return;
            case R.id.userConfirm:
                this.strAddress = this.etAddress.getText().toString();
                this.strPhone = this.etPhone.getText().toString();
                this.info.Item.mAddress = this.strAddress;
                this.info.Item.mPhone = this.strPhone;
                this.info.setToDatabase();
                finish();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.info.close();
        super.onDestroy();
    }
}
