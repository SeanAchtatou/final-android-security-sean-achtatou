package com.gaoding.dingcan.view;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.gaoding.dingcan.R;
import com.gaoding.dingcan.database.bean.ShopBean;
import com.gaoding.dingcan.database.controller.Shop;

public class ShoppingChangeActivity extends Activity implements View.OnClickListener {
    private Button btCancel;
    private Button btDelete;
    private Button btOK;
    private ImageView btnBack;
    private EditText etCount;
    private String strCount;
    private String strMenuId;
    private String strMenuName;
    private String strMenuPrice;
    private TextView tvName;
    private TextView tvPrice;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_shopping_change);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            this.strMenuId = bundle.getString("MenuId");
            this.strMenuName = bundle.getString("MenuName");
            this.strCount = bundle.getString("MenuCount");
            this.strMenuPrice = bundle.getString("MenuPrice");
        }
        initViews();
        this.tvName.setText(this.strMenuName);
        this.tvPrice.setText("单价: ￥" + this.strMenuPrice);
        this.etCount.setText(this.strCount);
    }

    private void initViews() {
        this.btnBack = (ImageView) findViewById(R.id.btnBack);
        this.btnBack.setOnClickListener(this);
        this.tvName = (TextView) findViewById(R.id.shop_name);
        this.tvPrice = (TextView) findViewById(R.id.shop_price);
        this.etCount = (EditText) findViewById(R.id.shop_count);
        this.btOK = (Button) findViewById(R.id.shop_ok);
        this.btCancel = (Button) findViewById(R.id.shop_cancel);
        this.btDelete = (Button) findViewById(R.id.shop_delete);
        this.btOK.setOnClickListener(this);
        this.btCancel.setOnClickListener(this);
        this.btDelete.setOnClickListener(this);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnBack:
                finish();
                return;
            case R.id.shop_ok:
                if (Integer.parseInt(this.etCount.getText().toString()) > 0) {
                    Shop shop = new Shop(this);
                    shop.getFromDatabase();
                    ShopBean bean = new ShopBean();
                    bean.mCount = this.etCount.getText().toString();
                    bean.mMenuId = this.strMenuId;
                    bean.mName = this.strMenuName;
                    bean.mPrice = this.strMenuPrice;
                    shop.setToDatabase(bean);
                    shop.close();
                    finish();
                    return;
                }
                return;
            case R.id.shop_cancel:
                finish();
                return;
            case R.id.shop_delete:
                Shop shop2 = new Shop(this);
                shop2.getFromDatabase();
                shop2.delete(this.strMenuId);
                shop2.close();
                finish();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }
}
