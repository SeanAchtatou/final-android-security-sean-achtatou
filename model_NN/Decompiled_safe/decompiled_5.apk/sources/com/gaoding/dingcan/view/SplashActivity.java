package com.gaoding.dingcan.view;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import com.gaoding.dingcan.R;
import com.gaoding.dingcan.database.controller.UserAccount;
import com.gaoding.dingcan.http.controller.Login;
import com.gaoding.dingcan.util.Utils;

public class SplashActivity extends Activity {
    private static final int AUTO_LOGING_FAILED = 2;
    private static final int AUTO_LOGING_SUCCESFUL = 1;
    Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    SplashActivity.this.startActivity(new Intent(SplashActivity.this, AreaUnitListActivity.class));
                    SplashActivity.this.finish();
                    return;
                case 2:
                    SplashActivity.this.gotoLoginPage();
                    return;
                default:
                    return;
            }
        }
    };

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences sharedPreferences = getSharedPreferences("AppGuide", 0);
        if (sharedPreferences.getBoolean("isstartonce", false)) {
            setContentView((int) R.layout.activity_splash);
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    boolean unused = SplashActivity.this.autoLogin();
                }
            }, 1300);
            return;
        }
        sharedPreferences.edit().putBoolean("isstartonce", true).commit();
        Intent intent = new Intent(getApplicationContext(), AppGuide.class);
        intent.putExtra("autoLogin", true);
        startActivity(intent);
        finish();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: private */
    public void gotoLoginPage() {
        new Handler().postDelayed(new Runnable() {
            public void run() {
                SplashActivity.this.startActivity(new Intent(SplashActivity.this, UserLoginActivity.class));
                SplashActivity.this.finish();
            }
        }, 1300);
    }

    public class LoginThread extends Thread {
        public LoginThread() {
        }

        public void run() {
            super.run();
            if (new Login(SplashActivity.this).UserAutoLogin()) {
                SplashActivity.this.mHandler.sendEmptyMessage(1);
            } else {
                SplashActivity.this.mHandler.sendEmptyMessage(2);
            }
        }
    }

    /* access modifiers changed from: private */
    public boolean autoLogin() {
        if (Utils.isNetWorkConnect(this)) {
            UserAccount account = new UserAccount(this);
            if (account.getCount() > 0) {
                account.close();
                new LoginThread().start();
                return true;
            }
            account.close();
            gotoLoginPage();
            return false;
        }
        gotoLoginPage();
        return false;
    }

    private class VersionUpdateThread extends Thread {
        private VersionUpdateThread() {
        }

        public void run() {
            super.run();
        }
    }
}
