package com.gaoding.dingcan.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.gaoding.dingcan.R;
import com.gaoding.dingcan.database.bean.UnitBean;
import com.gaoding.dingcan.http.controller.GetAreaUnit;
import com.gaoding.dingcan.http.controller.GetRestaurantInfo;
import com.gaoding.dingcan.util.LogUtils;
import com.gaoding.dingcan.util.Utils;
import java.util.List;

public class AreaUnitListActivity extends Activity implements View.OnClickListener {
    private static final int DISMISS_PROGRESS_DIALOG = 3;
    private static final int REFRESH_VIEW = 1;
    private static final int REQUEST_MY_PAGE = 101;
    private static final int SHOW_PROGRESS_DIALOG = 2;
    private static final int SHOW_TOAST_MESSAGE = 4;
    private String CityId;
    private Button btMyPage;
    private ImageView btnBack;
    /* access modifiers changed from: private */
    public MessageAdapter listAdapter;
    /* access modifiers changed from: private */
    public List<UnitBean> listUnit;
    private ListView listView;
    /* access modifiers changed from: private */
    public ProgressDialog mProgressDialog;
    /* access modifiers changed from: private */
    public Handler myHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    try {
                        AreaUnitListActivity.this.listAdapter.notifyDataSetChanged();
                        return;
                    } catch (Exception e) {
                        e.printStackTrace();
                        return;
                    }
                case 2:
                    try {
                        if (AreaUnitListActivity.this.mProgressDialog != null) {
                            if (AreaUnitListActivity.this.mProgressDialog.isShowing()) {
                                AreaUnitListActivity.this.mProgressDialog.dismiss();
                            }
                            AreaUnitListActivity.this.mProgressDialog = null;
                        }
                        AreaUnitListActivity.this.mProgressDialog = Utils.getProgressDialog(AreaUnitListActivity.this, (String) msg.obj);
                        AreaUnitListActivity.this.mProgressDialog.show();
                        return;
                    } catch (Exception e2) {
                        e2.printStackTrace();
                        return;
                    }
                case 3:
                    try {
                        if (AreaUnitListActivity.this.mProgressDialog != null && AreaUnitListActivity.this.mProgressDialog.isShowing()) {
                            AreaUnitListActivity.this.mProgressDialog.dismiss();
                        }
                        if (AreaUnitListActivity.this.listUnit.size() == 0) {
                            AreaUnitListActivity.this.tvNoInfo.setVisibility(0);
                        } else {
                            AreaUnitListActivity.this.tvNoInfo.setVisibility(8);
                        }
                        AreaUnitListActivity.this.listAdapter.notifyDataSetChanged();
                        return;
                    } catch (Exception e3) {
                        e3.printStackTrace();
                        return;
                    }
                default:
                    return;
            }
        }
    };
    /* access modifiers changed from: private */
    public Resources resources;
    /* access modifiers changed from: private */
    public TextView tvNoInfo;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LogUtils.logDebug("AreaListActivity");
        setContentView((int) R.layout.activity_areaunitlist);
        this.resources = getResources();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            this.CityId = bundle.getString("CityId");
        } else {
            this.CityId = GetRestaurantInfo.RES_STATE_OPEN;
        }
        initViews();
        loadAreaUnit();
        this.listAdapter = new MessageAdapter(this, null);
        this.listView.setAdapter((ListAdapter) this.listAdapter);
    }

    private void initViews() {
        this.btnBack = (ImageView) findViewById(R.id.btnBack);
        this.btnBack.setOnClickListener(this);
        this.btMyPage = (Button) findViewById(R.id.userPage);
        this.listView = (ListView) findViewById(R.id.listview_area);
        this.tvNoInfo = (TextView) findViewById(R.id.tv_no_info);
        this.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                UnitBean bean = (UnitBean) AreaUnitListActivity.this.listUnit.get(arg2);
                Intent intent = new Intent();
                intent.setClass(AreaUnitListActivity.this, RestaurantListActivity.class);
                intent.putExtra("UnitId", bean.mId);
                intent.putExtra("UnitName", bean.mName);
                AreaUnitListActivity.this.startActivity(intent);
            }
        });
        this.btMyPage.setOnClickListener(this);
    }

    private void loadAreaUnit() {
        if (Utils.isNetWorkConnect(this)) {
            this.tvNoInfo.setVisibility(8);
            new GetAreaThread(this.CityId).start();
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnBack:
                finish();
                return;
            case R.id.userPage:
                Intent intent = new Intent();
                intent.setClass(this, MyPageActivity.class);
                startActivityForResult(intent, REQUEST_MY_PAGE);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == -1) {
            switch (requestCode) {
                case REQUEST_MY_PAGE /*101*/:
                    finish();
                    break;
            }
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private class MessageAdapter extends BaseAdapter {
        private MessageAdapter() {
        }

        /* synthetic */ MessageAdapter(AreaUnitListActivity areaUnitListActivity, MessageAdapter messageAdapter) {
            this();
        }

        public int getCount() {
            if (AreaUnitListActivity.this.listUnit == null) {
                return 0;
            }
            return AreaUnitListActivity.this.listUnit.size();
        }

        public Object getItem(int position) {
            return Integer.valueOf(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public void unregisterDataSetObserver(DataSetObserver observer) {
            if (observer != null) {
                super.unregisterDataSetObserver(observer);
            }
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            int mPosition = position;
            View view = ((LayoutInflater) AreaUnitListActivity.this.getSystemService("layout_inflater")).inflate((int) R.layout.msg_item, (ViewGroup) null);
            try {
                HolderViewTopic holderView = new HolderViewTopic(AreaUnitListActivity.this, null);
                holderView.mTvTitle = (TextView) view.findViewById(R.id.tv_item_title);
                try {
                    holderView.mTvTitle.setText(((UnitBean) AreaUnitListActivity.this.listUnit.get(mPosition)).mName);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            return view;
        }
    }

    private class HolderViewTopic {
        /* access modifiers changed from: private */
        public TextView mTvTitle;

        private HolderViewTopic() {
        }

        /* synthetic */ HolderViewTopic(AreaUnitListActivity areaUnitListActivity, HolderViewTopic holderViewTopic) {
            this();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    private class GetAreaThread extends Thread {
        public String mCityId;

        public GetAreaThread(String mCityId2) {
            this.mCityId = mCityId2;
        }

        public void run() {
            super.run();
            Message msg = new Message();
            msg.what = 2;
            msg.obj = AreaUnitListActivity.this.resources.getString(R.string.please_wait);
            AreaUnitListActivity.this.myHandler.sendMessage(msg);
            try {
                GetAreaUnit get = new GetAreaUnit(AreaUnitListActivity.this);
                get.mCityId = this.mCityId;
                if (get.GetList()) {
                    AreaUnitListActivity.this.listUnit = get.listUnit;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            AreaUnitListActivity.this.myHandler.sendEmptyMessage(3);
        }
    }
}
