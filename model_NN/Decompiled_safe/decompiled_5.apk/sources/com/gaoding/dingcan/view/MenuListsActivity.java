package com.gaoding.dingcan.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import com.gaoding.dingcan.R;
import com.gaoding.dingcan.database.bean.MenuBean;
import com.gaoding.dingcan.database.bean.MenuTypeBean;
import com.gaoding.dingcan.database.bean.ShopBean;
import com.gaoding.dingcan.database.controller.Shop;
import com.gaoding.dingcan.database.controller.ShopCart;
import com.gaoding.dingcan.http.controller.GetMenu;
import com.gaoding.dingcan.http.controller.GetRestaurantInfo;
import com.gaoding.dingcan.util.LogUtils;
import com.gaoding.dingcan.util.Utils;
import java.util.ArrayList;
import java.util.List;

public class MenuListsActivity extends Activity implements View.OnClickListener {
    private static final int DISMISS_PROGRESS_DIALOG = 3;
    private static final int REFRESH_MENU = 6;
    private static final int REFRESH_TYPE = 5;
    private static final int REFRESH_VIEW = 1;
    private static final int SHOW_PROGRESS_DIALOG = 2;
    private static final int SHOW_TOAST_MESSAGE = 4;
    /* access modifiers changed from: private */
    public String RestaurantId;
    /* access modifiers changed from: private */
    public String UnitName;
    /* access modifiers changed from: private */
    public ArrayAdapter<String> adapter;
    /* access modifiers changed from: private */
    public boolean changeRes = false;
    /* access modifiers changed from: private */
    public List<String> list = new ArrayList();
    /* access modifiers changed from: private */
    public MessageAdapter listAdapter;
    /* access modifiers changed from: private */
    public List<MenuBean> listMenu;
    /* access modifiers changed from: private */
    public List<MenuBean> listShow;
    /* access modifiers changed from: private */
    public List<MenuTypeBean> listType;
    private ListView listView;
    private LinearLayout mLayoutProgress;
    /* access modifiers changed from: private */
    public ProgressDialog mProgressDialog;
    /* access modifiers changed from: private */
    public Handler myHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                case 4:
                default:
                    return;
                case 2:
                    try {
                        if (MenuListsActivity.this.mProgressDialog != null) {
                            if (MenuListsActivity.this.mProgressDialog.isShowing()) {
                                MenuListsActivity.this.mProgressDialog.dismiss();
                            }
                            MenuListsActivity.this.mProgressDialog = null;
                        }
                        MenuListsActivity.this.mProgressDialog = Utils.getProgressDialog(MenuListsActivity.this, (String) msg.obj);
                        MenuListsActivity.this.mProgressDialog.show();
                        return;
                    } catch (Exception e) {
                        e.printStackTrace();
                        return;
                    }
                case 3:
                    try {
                        if (MenuListsActivity.this.mProgressDialog != null && MenuListsActivity.this.mProgressDialog.isShowing()) {
                            MenuListsActivity.this.mProgressDialog.dismiss();
                        }
                        MenuListsActivity.this.listAdapter.notifyDataSetChanged();
                        return;
                    } catch (Exception e2) {
                        e2.printStackTrace();
                        return;
                    }
                case 5:
                    MenuListsActivity.this.adapter.notifyDataSetChanged();
                    return;
                case 6:
                    MenuListsActivity.this.listAdapter.notifyDataSetChanged();
                    return;
            }
        }
    };
    /* access modifiers changed from: private */
    public Resources resources;
    /* access modifiers changed from: private */
    public Shop shop;
    /* access modifiers changed from: private */
    public ShopCart shopCart;
    /* access modifiers changed from: private */
    public String strResAddress = "";
    /* access modifiers changed from: private */
    public String strResAreaTo = "";
    /* access modifiers changed from: private */
    public String strResDesc = "";
    /* access modifiers changed from: private */
    public String strResName = "";
    /* access modifiers changed from: private */
    public String strResNotic = "";
    /* access modifiers changed from: private */
    public String strResOpen = "";
    /* access modifiers changed from: private */
    public String strResSendTime = "";
    /* access modifiers changed from: private */
    public String strResState = "";
    /* access modifiers changed from: private */
    public String strStartPrice = "10";
    /* access modifiers changed from: private */
    public TextView tvMoney;
    private TextView tvNoInfo;
    private Spinner typeSpinner;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LogUtils.logDebug("MenuListActivity");
        setContentView((int) R.layout.activity_menu_lists);
        this.resources = getResources();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            this.RestaurantId = bundle.getString("RestaurantId");
            this.strResName = bundle.getString("RestaurantName");
            this.UnitName = bundle.getString("UnitName");
        }
        initViews();
        loadList();
        this.listMenu = new ArrayList();
        this.listShow = new ArrayList();
        this.listAdapter = new MessageAdapter(this, null);
        this.listView.setAdapter((ListAdapter) this.listAdapter);
        this.shopCart = new ShopCart(this);
        this.shop = new Shop(this);
    }

    private void initViews() {
        this.typeSpinner = (Spinner) findViewById(R.id.res_type);
        this.adapter = new ArrayAdapter<>(this, (int) R.layout.spinner, this.list);
        this.adapter.setDropDownViewResource(17367049);
        this.typeSpinner.setAdapter((SpinnerAdapter) this.adapter);
        this.tvMoney = (TextView) findViewById(R.id.tv_money);
        this.listView = (ListView) findViewById(R.id.listview_menu);
        this.tvNoInfo = (TextView) findViewById(R.id.tv_no_info);
        this.mLayoutProgress = (LinearLayout) findViewById(R.id.linearlayout_progress);
        this.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                if (MenuListsActivity.this.strResState.equals(GetRestaurantInfo.RES_STATE_OPEN)) {
                    MenuBean bean = (MenuBean) MenuListsActivity.this.listShow.get(arg2);
                    Intent intent = new Intent();
                    intent.setClass(MenuListsActivity.this, ShoppingActivity.class);
                    intent.putExtra("MenuId", bean.mId);
                    intent.putExtra("MenuName", bean.mName);
                    intent.putExtra("RestaurantId", MenuListsActivity.this.RestaurantId);
                    intent.putExtra("MenuPrice", bean.mPrice);
                    intent.putExtra("StartPrice", MenuListsActivity.this.strStartPrice);
                    intent.putExtra("UnitName", MenuListsActivity.this.UnitName);
                    int count = MenuListsActivity.this.shop.getItemCount(bean.mId);
                    if (count < 1) {
                        count = 1;
                    }
                    intent.putExtra("MenuCount", new StringBuilder().append(count).toString());
                    MenuListsActivity.this.startActivity(intent);
                    return;
                }
                Utils.showToast(MenuListsActivity.this, "餐厅休息中，不能点菜");
            }
        });
        this.typeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView arg0, View arg1, int arg2, long arg3) {
                if (arg2 == 0) {
                    MenuListsActivity.this.listShow.clear();
                    for (int i = 0; i < MenuListsActivity.this.listMenu.size(); i++) {
                        MenuListsActivity.this.listShow.add((MenuBean) MenuListsActivity.this.listMenu.get(i));
                    }
                } else {
                    LogUtils.logDebug("type index=" + arg2);
                    String typeId = ((MenuTypeBean) MenuListsActivity.this.listType.get(arg2 - 1)).mId;
                    LogUtils.logDebug("type id=" + typeId);
                    MenuListsActivity.this.listShow.clear();
                    for (int i2 = 0; i2 < MenuListsActivity.this.listMenu.size(); i2++) {
                        if (((MenuBean) MenuListsActivity.this.listMenu.get(i2)).mTypeId.equals(typeId)) {
                            MenuListsActivity.this.listShow.add((MenuBean) MenuListsActivity.this.listMenu.get(i2));
                        }
                    }
                }
                MenuListsActivity.this.listAdapter.notifyDataSetChanged();
                arg0.setVisibility(0);
            }

            public void onNothingSelected(AdapterView arg0) {
                arg0.setVisibility(0);
            }
        });
    }

    private void loadList() {
        if (Utils.isNetWorkConnect(this)) {
            this.tvNoInfo.setVisibility(8);
            new GetThread(this.RestaurantId).start();
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnBack:
                finish();
                return;
            case R.id.shopingCart:
                Intent intent = new Intent();
                intent.setClass(this, ShoppingListActivity.class);
                intent.putExtra("RestaurantId", this.RestaurantId);
                intent.putExtra("StartPrice", this.strStartPrice);
                intent.putExtra("UnitName", this.UnitName);
                startActivity(intent);
                return;
            default:
                return;
        }
    }

    private class MessageAdapter extends BaseAdapter {
        private MessageAdapter() {
        }

        /* synthetic */ MessageAdapter(MenuListsActivity menuListsActivity, MessageAdapter messageAdapter) {
            this();
        }

        public int getCount() {
            if (MenuListsActivity.this.listShow == null) {
                return 0;
            }
            return MenuListsActivity.this.listShow.size();
        }

        public Object getItem(int position) {
            return Integer.valueOf(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public void unregisterDataSetObserver(DataSetObserver observer) {
            if (observer != null) {
                super.unregisterDataSetObserver(observer);
            }
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            final int mPosition = position;
            View view = ((LayoutInflater) MenuListsActivity.this.getSystemService("layout_inflater")).inflate((int) R.layout.menu_item, (ViewGroup) null);
            try {
                final HolderViewTopic holderView = new HolderViewTopic(MenuListsActivity.this, null);
                holderView.mTvTitle = (TextView) view.findViewById(R.id.tv_item_title);
                holderView.mIbAdd = (ImageView) view.findViewById(R.id.ib_add_menu);
                holderView.mTvCount = (TextView) view.findViewById(R.id.tv_item_count);
                holderView.mIbReduce = (ImageView) view.findViewById(R.id.ib_cut_menu);
                try {
                    holderView.mTvTitle.setText(String.valueOf(String.valueOf("") + "￥" + ((MenuBean) MenuListsActivity.this.listShow.get(mPosition)).mPrice) + "   " + ((MenuBean) MenuListsActivity.this.listShow.get(mPosition)).mName);
                    holderView.mTvCount.setText(MenuListsActivity.this.getMenuCount(((MenuBean) MenuListsActivity.this.listShow.get(mPosition)).mId));
                    holderView.mIbAdd.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            if (MenuListsActivity.this.changeRes) {
                                MenuListsActivity.this.shopCart.item.mRestaurantId = MenuListsActivity.this.RestaurantId;
                                MenuListsActivity.this.shopCart.item.mStartPrice = MenuListsActivity.this.strStartPrice;
                                MenuListsActivity.this.shopCart.item.mUnitName = MenuListsActivity.this.UnitName;
                                MenuListsActivity.this.shopCart.setToDatabase();
                                MenuListsActivity.this.shop.clean();
                                MenuListsActivity.this.changeRes = false;
                            }
                            MenuBean bean = (MenuBean) MenuListsActivity.this.listShow.get(mPosition);
                            ShopBean shopBean = new ShopBean();
                            shopBean.mCount = GetRestaurantInfo.RES_STATE_OPEN;
                            shopBean.mMenuId = bean.mId;
                            shopBean.mName = bean.mName;
                            shopBean.mPrice = bean.mPrice;
                            holderView.mTvCount.setText("x" + MenuListsActivity.this.shop.addToDatabase(shopBean));
                            MenuListsActivity.this.tvMoney.setText(String.valueOf("购物车中订单总价：") + MenuListsActivity.this.getTotalMoney());
                        }
                    });
                    holderView.mIbReduce.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            MenuBean bean = (MenuBean) MenuListsActivity.this.listShow.get(mPosition);
                            ShopBean shopBean = new ShopBean();
                            shopBean.mCount = GetRestaurantInfo.RES_STATE_OPEN;
                            shopBean.mMenuId = bean.mId;
                            shopBean.mName = bean.mName;
                            shopBean.mPrice = bean.mPrice;
                            int count = MenuListsActivity.this.shop.reduceToDatabase(shopBean);
                            if (count > 0) {
                                holderView.mTvCount.setText("x" + count);
                            } else {
                                holderView.mTvCount.setText("");
                            }
                            MenuListsActivity.this.tvMoney.setText(String.valueOf("购物车中订单总价：") + MenuListsActivity.this.getTotalMoney());
                        }
                    });
                    if (!MenuListsActivity.this.strResState.equals(GetRestaurantInfo.RES_STATE_OPEN)) {
                        holderView.mIbAdd.setVisibility(8);
                        holderView.mIbReduce.setVisibility(8);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            return view;
        }
    }

    private class HolderViewTopic {
        /* access modifiers changed from: private */
        public ImageView mIbAdd;
        /* access modifiers changed from: private */
        public ImageView mIbReduce;
        /* access modifiers changed from: private */
        public TextView mTvCount;
        /* access modifiers changed from: private */
        public TextView mTvTitle;

        private HolderViewTopic() {
        }

        /* synthetic */ HolderViewTopic(MenuListsActivity menuListsActivity, HolderViewTopic holderViewTopic) {
            this();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.shopCart.close();
        this.shop.close();
        super.onDestroy();
    }

    private class GetThread extends Thread {
        public String RestaurantId;

        public GetThread(String RestaurantId2) {
            this.RestaurantId = RestaurantId2;
        }

        public void run() {
            super.run();
            Message msg = new Message();
            msg.what = 2;
            msg.obj = MenuListsActivity.this.resources.getString(R.string.please_wait);
            MenuListsActivity.this.myHandler.sendMessage(msg);
            try {
                GetRestaurantInfo info = new GetRestaurantInfo(MenuListsActivity.this);
                info.mResId = this.RestaurantId;
                if (info.GetInfo()) {
                    LogUtils.logDebug("GetRestaurantInfo return true");
                    MenuListsActivity.this.strResName = info.item.mName;
                    MenuListsActivity.this.strResOpen = info.item.mOpenTime;
                    MenuListsActivity.this.strResState = info.item.mState;
                    MenuListsActivity.this.strResSendTime = info.item.mSendTime;
                    MenuListsActivity.this.strResAddress = info.item.mAddress;
                    MenuListsActivity.this.strResAreaTo = info.item.mSendArea;
                    MenuListsActivity.this.strResNotic = info.item.mNotice;
                    MenuListsActivity.this.strResDesc = info.item.mSendDesc;
                    MenuListsActivity.this.strStartPrice = info.item.mStartPrice;
                    MenuListsActivity.this.myHandler.sendEmptyMessage(1);
                }
                GetMenu get = new GetMenu(MenuListsActivity.this);
                get.mRestaurantId = this.RestaurantId;
                if (get.GetList()) {
                    for (int i = 0; i < get.listMenu.size(); i++) {
                        MenuListsActivity.this.listMenu.add(get.listMenu.get(i));
                        MenuListsActivity.this.listShow.add(get.listMenu.get(i));
                    }
                    MenuListsActivity.this.listType = get.listType;
                    MenuListsActivity.this.list.clear();
                    MenuListsActivity.this.list.add(MenuListsActivity.this.resources.getString(R.string.type_all));
                    for (int i2 = 0; i2 < MenuListsActivity.this.listType.size(); i2++) {
                        MenuListsActivity.this.list.add(((MenuTypeBean) MenuListsActivity.this.listType.get(i2)).mName);
                    }
                    MenuListsActivity.this.myHandler.sendEmptyMessage(5);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            MenuListsActivity.this.myHandler.sendEmptyMessage(3);
        }
    }

    /* access modifiers changed from: private */
    public String getMenuCount(String menuId) {
        int count = this.shop.getItemCount(menuId);
        if (count > 0) {
            return "x" + count;
        }
        return "";
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        this.shopCart.getFromDatabase();
        if (this.shopCart.item.mRestaurantId.equals(this.RestaurantId)) {
            this.changeRes = false;
        } else {
            this.changeRes = true;
        }
        this.shop.getFromDatabase();
        this.listAdapter.notifyDataSetChanged();
        this.tvMoney.setText(String.valueOf("购物车中订单总价：") + getTotalMoney());
        super.onResume();
    }

    /* access modifiers changed from: private */
    public String getTotalMoney() {
        float result = 0.0f;
        try {
            for (ShopBean bean : this.shop.list) {
                result += Float.parseFloat(bean.mPrice) * ((float) Integer.parseInt(bean.mCount));
            }
        } catch (Exception e) {
        }
        return "￥" + result;
    }
}
