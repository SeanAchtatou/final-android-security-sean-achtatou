package com.gaoding.dingcan.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.gaoding.dingcan.R;
import com.gaoding.dingcan.database.bean.ShopBean;
import com.gaoding.dingcan.database.controller.Shop;
import com.gaoding.dingcan.database.controller.ShopCart;

public class ShoppingActivity extends Activity implements View.OnClickListener {
    private String RestaurantId;
    private String UnitName;
    private Button btCancel;
    private Button btOK;
    private ImageView btnBack;
    private boolean changeRes = false;
    private EditText etCount;
    private ShopCart shopCart;
    private String strCount;
    private String strMenuId;
    private String strMenuName;
    private String strMenuPrice;
    private String strStartPrice;
    private TextView tvName;
    private TextView tvPrice;
    private TextView tvWarning;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_shopping);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            this.strMenuId = bundle.getString("MenuId");
            this.strMenuName = bundle.getString("MenuName");
            this.RestaurantId = bundle.getString("RestaurantId");
            this.strMenuPrice = bundle.getString("MenuPrice");
            this.strStartPrice = bundle.getString("StartPrice");
            this.UnitName = bundle.getString("UnitName");
            this.strCount = bundle.getString("MenuCount");
        }
        initViews();
        this.tvName.setText(this.strMenuName);
        this.tvPrice.setText("单价 ￥ " + this.strMenuPrice);
        this.shopCart = new ShopCart(this);
        this.shopCart.getFromDatabase();
        if (this.RestaurantId.equals(this.shopCart.item.mRestaurantId) || this.shopCart.item.mRestaurantId.equals("")) {
            this.changeRes = false;
            this.tvWarning.setVisibility(8);
        } else {
            this.changeRes = true;
            this.tvWarning.setVisibility(0);
        }
        this.etCount.setText(this.strCount);
    }

    private void initViews() {
        this.btnBack = (ImageView) findViewById(R.id.btnBack);
        this.btnBack.setOnClickListener(this);
        this.tvName = (TextView) findViewById(R.id.shop_name);
        this.tvPrice = (TextView) findViewById(R.id.shop_price);
        this.tvWarning = (TextView) findViewById(R.id.shop_warning);
        this.etCount = (EditText) findViewById(R.id.shop_count);
        this.btOK = (Button) findViewById(R.id.shop_ok);
        this.btCancel = (Button) findViewById(R.id.shop_cancel);
        this.btOK.setOnClickListener(this);
        this.btCancel.setOnClickListener(this);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnBack:
                finish();
                return;
            case R.id.shop_ok:
                if (Integer.parseInt(this.etCount.getText().toString()) > 0) {
                    this.shopCart.item.mRestaurantId = this.RestaurantId;
                    this.shopCart.item.mStartPrice = this.strStartPrice;
                    this.shopCart.item.mUnitName = this.UnitName;
                    this.shopCart.setToDatabase();
                    Shop shop = new Shop(this);
                    shop.getFromDatabase();
                    if (this.changeRes) {
                        shop.clean();
                    }
                    ShopBean bean = new ShopBean();
                    bean.mCount = this.etCount.getText().toString();
                    bean.mMenuId = this.strMenuId;
                    bean.mName = this.strMenuName;
                    bean.mPrice = this.strMenuPrice;
                    shop.setToDatabase(bean);
                    shop.close();
                    Intent intent = new Intent();
                    intent.setClass(this, ShoppingListActivity.class);
                    intent.putExtra("RestaurantId", this.RestaurantId);
                    intent.putExtra("StartPrice", this.strStartPrice);
                    intent.putExtra("UnitName", this.UnitName);
                    startActivity(intent);
                    finish();
                    return;
                }
                return;
            case R.id.shop_cancel:
                finish();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.shopCart.close();
        super.onDestroy();
    }
}
