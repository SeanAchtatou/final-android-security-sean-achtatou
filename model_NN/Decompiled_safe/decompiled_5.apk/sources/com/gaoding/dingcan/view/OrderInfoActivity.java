package com.gaoding.dingcan.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.res.Resources;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.MotionEventCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.gaoding.dingcan.R;
import com.gaoding.dingcan.database.bean.ShopBean;
import com.gaoding.dingcan.http.controller.CancelOrder;
import com.gaoding.dingcan.http.controller.GetOrderDetail;
import com.gaoding.dingcan.http.controller.GetRestaurantInfo;
import com.gaoding.dingcan.util.LogUtils;
import com.gaoding.dingcan.util.Utils;
import java.util.List;

public class OrderInfoActivity extends Activity implements View.OnClickListener {
    private static final int CANCEL_ORDER = 5;
    private static final int DISMISS_PROGRESS_DIALOG = 3;
    private static final int REFRESH_VIEW = 1;
    private static final int SHOW_PROGRESS_DIALOG = 2;
    private static final int SHOW_TOAST_MESSAGE = 4;
    private Button btCancel;
    private ImageView btnBack;
    /* access modifiers changed from: private */
    public MessageAdapter listAdapter;
    /* access modifiers changed from: private */
    public List<ShopBean> listMenu;
    private ListView listView;
    /* access modifiers changed from: private */
    public ProgressDialog mProgressDialog;
    /* access modifiers changed from: private */
    public Handler myHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    try {
                        OrderInfoActivity.this.listAdapter.notifyDataSetChanged();
                        return;
                    } catch (Exception e) {
                        e.printStackTrace();
                        return;
                    }
                case 2:
                    try {
                        if (OrderInfoActivity.this.mProgressDialog != null) {
                            if (OrderInfoActivity.this.mProgressDialog.isShowing()) {
                                OrderInfoActivity.this.mProgressDialog.dismiss();
                            }
                            OrderInfoActivity.this.mProgressDialog = null;
                        }
                        OrderInfoActivity.this.mProgressDialog = Utils.getProgressDialog(OrderInfoActivity.this, (String) msg.obj);
                        OrderInfoActivity.this.mProgressDialog.show();
                        return;
                    } catch (Exception e2) {
                        e2.printStackTrace();
                        return;
                    }
                case 3:
                    try {
                        if (OrderInfoActivity.this.mProgressDialog != null && OrderInfoActivity.this.mProgressDialog.isShowing()) {
                            OrderInfoActivity.this.mProgressDialog.dismiss();
                            return;
                        }
                        return;
                    } catch (Exception e3) {
                        e3.printStackTrace();
                        return;
                    }
                case 4:
                    try {
                        Utils.showToast(OrderInfoActivity.this, msg.obj.toString());
                        return;
                    } catch (Exception e4) {
                        e4.printStackTrace();
                        return;
                    }
                case 5:
                    OrderInfoActivity.this.finish();
                    return;
                default:
                    return;
            }
        }
    };
    /* access modifiers changed from: private */
    public Resources resources;
    private String strActiveId = "";
    private String strAddress = "";
    private String strCactivtyId = "";
    private String strDesc = "";
    private String strNum = "";
    private String strOrderId;
    private String strPhone = "";
    private String strPrice = "";
    private String strRestaurantName = "";
    private String strState = "";
    private String strTime = "";
    private String strTotal = "";
    private String strType = "";
    private TextView tvActiveId;
    private TextView tvAddress;
    private TextView tvCactivtyId;
    private TextView tvDesc;
    private TextView tvName;
    private TextView tvNum;
    private TextView tvPhone;
    private TextView tvPrice;
    private TextView tvState;
    private TextView tvTime;
    private TextView tvTotal;
    private TextView tvType;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LogUtils.logDebug("MenuListActivity");
        setContentView((int) R.layout.activity_order_info);
        this.resources = getResources();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            this.strOrderId = bundle.getString("OrderId");
            this.strTime = bundle.getString("Time");
            this.strState = bundle.getString("State");
            this.strAddress = bundle.getString("Address");
            this.strPhone = bundle.getString("Phone");
            this.strDesc = bundle.getString("Desc");
            this.strPrice = bundle.getString("Price");
            this.strTotal = bundle.getString("Total");
            this.strRestaurantName = bundle.getString("Name");
            this.strNum = bundle.getString("Num");
            this.strCactivtyId = bundle.getString("CactivtyId");
            this.strType = bundle.getString("Type");
            this.strActiveId = bundle.getString("ActiveId");
        }
        initViews();
        this.listAdapter = new MessageAdapter(this, null);
        this.listView.setAdapter((ListAdapter) this.listAdapter);
        loadInfo();
        this.tvName.setText("餐厅名称:  " + this.strRestaurantName);
        this.tvTime.setText("下单时间:  " + this.strTime);
        this.tvAddress.setText("送餐地址:  " + this.strAddress);
        this.tvPhone.setText("电话:  " + this.strPhone);
        this.tvDesc.setText("备注:  " + this.strDesc);
        this.tvPrice.setText("总价:  ￥" + this.strPrice);
        this.tvTotal.setText("份数:  " + this.strTotal + "份");
        this.tvNum.setText("订单号:  " + this.strNum);
        this.tvCactivtyId.setText("企业团餐编号:  " + this.strCactivtyId);
        this.tvType.setText("类型:  " + this.strType);
        this.tvActiveId.setText("活动编号:  " + this.strActiveId);
        if (this.strState.equals(GetRestaurantInfo.RES_STATE_CLOSE)) {
            this.btCancel.setVisibility(0);
        } else {
            this.btCancel.setVisibility(8);
        }
        switch (Integer.parseInt(this.strState)) {
            case 0:
                this.strState = "等待接收";
                break;
            case 1:
                this.strState = "订单已接收";
                break;
            case 2:
                this.strState = "订单未接收";
                break;
            case 3:
                this.strState = "派送中";
                break;
            case 4:
                this.strState = "订单成功";
                break;
            case 5:
                this.strState = "订单失败";
                break;
            case 6:
                this.strState = "会员取消订单";
                break;
            case MotionEventCompat.ACTION_HOVER_MOVE:
                this.strState = "商家取消订单";
                break;
        }
        this.tvState.setText("订单状态:  " + this.strState);
    }

    private void initViews() {
        this.btnBack = (ImageView) findViewById(R.id.btnBack);
        this.btnBack.setOnClickListener(this);
        this.listView = (ListView) findViewById(R.id.listview_menu);
        this.tvName = (TextView) findViewById(R.id.res_name);
        this.tvTime = (TextView) findViewById(R.id.order_time);
        this.tvAddress = (TextView) findViewById(R.id.order_address);
        this.tvPhone = (TextView) findViewById(R.id.order_phone);
        this.tvDesc = (TextView) findViewById(R.id.order_desc);
        this.tvPrice = (TextView) findViewById(R.id.order_price);
        this.tvTotal = (TextView) findViewById(R.id.order_total);
        this.tvNum = (TextView) findViewById(R.id.order_num);
        this.tvCactivtyId = (TextView) findViewById(R.id.order_cactivtyId);
        this.tvType = (TextView) findViewById(R.id.order_type);
        this.tvActiveId = (TextView) findViewById(R.id.order_active);
        this.tvState = (TextView) findViewById(R.id.order_state);
        this.btCancel = (Button) findViewById(R.id.btOrderCancel);
        this.btCancel.setOnClickListener(this);
    }

    private void loadInfo() {
        if (Utils.isNetWorkConnect(this)) {
            new GetThread(this.strOrderId).start();
        }
    }

    private void cancelOrder() {
        if (Utils.isNetWorkConnect(this)) {
            new SetThread(this.strOrderId).start();
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnBack:
                finish();
                return;
            case R.id.btOrderCancel:
                cancelOrder();
                return;
            default:
                return;
        }
    }

    private class MessageAdapter extends BaseAdapter {
        private MessageAdapter() {
        }

        /* synthetic */ MessageAdapter(OrderInfoActivity orderInfoActivity, MessageAdapter messageAdapter) {
            this();
        }

        public int getCount() {
            if (OrderInfoActivity.this.listMenu == null) {
                return 0;
            }
            return OrderInfoActivity.this.listMenu.size();
        }

        public Object getItem(int position) {
            return Integer.valueOf(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public void unregisterDataSetObserver(DataSetObserver observer) {
            if (observer != null) {
                super.unregisterDataSetObserver(observer);
            }
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            int mPosition = position;
            View view = ((LayoutInflater) OrderInfoActivity.this.getSystemService("layout_inflater")).inflate((int) R.layout.msg_item, (ViewGroup) null);
            try {
                HolderViewTopic holderView = new HolderViewTopic(OrderInfoActivity.this, null);
                holderView.mTvTitle = (TextView) view.findViewById(R.id.tv_item_title);
                try {
                    holderView.mTvTitle.setText(String.valueOf(String.valueOf(String.valueOf(String.valueOf("") + "￥" + ((ShopBean) OrderInfoActivity.this.listMenu.get(mPosition)).mPrice) + "     " + ((ShopBean) OrderInfoActivity.this.listMenu.get(mPosition)).mName) + "     x" + ((ShopBean) OrderInfoActivity.this.listMenu.get(mPosition)).mCount) + "份");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            return view;
        }
    }

    private class HolderViewTopic {
        /* access modifiers changed from: private */
        public TextView mTvTitle;

        private HolderViewTopic() {
        }

        /* synthetic */ HolderViewTopic(OrderInfoActivity orderInfoActivity, HolderViewTopic holderViewTopic) {
            this();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    private class GetThread extends Thread {
        public String OrderId;

        public GetThread(String OrderId2) {
            this.OrderId = OrderId2;
        }

        public void run() {
            super.run();
            Message msg = new Message();
            msg.what = 2;
            msg.obj = OrderInfoActivity.this.resources.getString(R.string.please_wait);
            OrderInfoActivity.this.myHandler.sendMessage(msg);
            try {
                GetOrderDetail detail = new GetOrderDetail(OrderInfoActivity.this);
                detail.mOrderId = this.OrderId;
                if (detail.GetInfo()) {
                    LogUtils.logDebug("json return true");
                    OrderInfoActivity.this.listMenu = detail.list;
                    OrderInfoActivity.this.myHandler.sendEmptyMessage(1);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            OrderInfoActivity.this.myHandler.sendEmptyMessage(3);
        }
    }

    private class SetThread extends Thread {
        public String OrderId;

        public SetThread(String OrderId2) {
            this.OrderId = OrderId2;
        }

        public void run() {
            super.run();
            Message msg = new Message();
            msg.what = 2;
            msg.obj = OrderInfoActivity.this.resources.getString(R.string.please_wait);
            OrderInfoActivity.this.myHandler.sendMessage(msg);
            try {
                CancelOrder cancel = new CancelOrder(OrderInfoActivity.this);
                cancel.mOrderId = this.OrderId;
                if (cancel.CancelOrder()) {
                    LogUtils.logDebug("json return true");
                    OrderInfoActivity.this.myHandler.sendEmptyMessage(5);
                } else {
                    Message mResultMessage = new Message();
                    mResultMessage.what = 4;
                    mResultMessage.obj = cancel.message;
                    OrderInfoActivity.this.myHandler.sendMessage(mResultMessage);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            OrderInfoActivity.this.myHandler.sendEmptyMessage(3);
        }
    }
}
