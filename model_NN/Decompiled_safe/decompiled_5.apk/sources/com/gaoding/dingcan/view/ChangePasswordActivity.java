package com.gaoding.dingcan.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import com.gaoding.dingcan.R;
import com.gaoding.dingcan.database.controller.UserAccount;
import com.gaoding.dingcan.http.controller.ChangePassword;
import com.gaoding.dingcan.util.Utils;

public class ChangePasswordActivity extends Activity implements View.OnClickListener {
    private static final int CHANGE_SUCCESS = 5;
    private static final int DISMISS_PROGRESS_DIALOG = 3;
    private static final int REFRESH_VIEW = 1;
    private static final int SHOW_PROGRESS_DIALOG = 2;
    private static final int SHOW_TOAST_MESSAGE = 4;
    private ImageView btnBack;
    private Button btnSave;
    private EditText edtComfirm;
    /* access modifiers changed from: private */
    public EditText edtNew;
    /* access modifiers changed from: private */
    public EditText edtOld;
    /* access modifiers changed from: private */
    public ProgressDialog mProgressDialog;
    /* access modifiers changed from: private */
    public Handler myHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                default:
                    return;
                case 2:
                    try {
                        if (ChangePasswordActivity.this.mProgressDialog != null) {
                            if (ChangePasswordActivity.this.mProgressDialog.isShowing()) {
                                ChangePasswordActivity.this.mProgressDialog.dismiss();
                            }
                            ChangePasswordActivity.this.mProgressDialog = null;
                        }
                        ChangePasswordActivity.this.mProgressDialog = Utils.getProgressDialog(ChangePasswordActivity.this, (String) msg.obj);
                        ChangePasswordActivity.this.mProgressDialog.show();
                        return;
                    } catch (Exception e) {
                        e.printStackTrace();
                        return;
                    }
                case 3:
                    try {
                        if (ChangePasswordActivity.this.mProgressDialog != null && ChangePasswordActivity.this.mProgressDialog.isShowing()) {
                            ChangePasswordActivity.this.mProgressDialog.dismiss();
                            return;
                        }
                        return;
                    } catch (Exception e2) {
                        e2.printStackTrace();
                        return;
                    }
                case 4:
                    try {
                        Utils.showToast(ChangePasswordActivity.this, msg.obj.toString());
                        return;
                    } catch (Exception e3) {
                        e3.printStackTrace();
                        return;
                    }
                case 5:
                    ChangePasswordActivity.this.finish();
                    return;
            }
        }
    };
    private Resources resources;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_password);
        this.resources = getResources();
        initViews();
    }

    private void initViews() {
        this.btnBack = (ImageView) findViewById(R.id.btnBack);
        this.btnBack.setOnClickListener(this);
        this.edtOld = (EditText) findViewById(R.id.chg_psw_old);
        this.edtNew = (EditText) findViewById(R.id.chg_psw_new);
        this.edtComfirm = (EditText) findViewById(R.id.chg_psw_confirm);
        this.btnSave = (Button) findViewById(R.id.chg_psw_save);
        this.btnSave.setOnClickListener(this);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public void onClick(View v) {
        try {
            switch (v.getId()) {
                case R.id.btnBack:
                    finish();
                    return;
                case R.id.chg_psw_save:
                    String newPassword = this.edtNew.getText().toString();
                    String confirmPassword = this.edtComfirm.getText().toString();
                    if (newPassword.length() < 6) {
                        Utils.showToast(this, "密码长度须为6-20位");
                        return;
                    } else if (newPassword.equals(confirmPassword)) {
                        new UploadThread(this, null).start();
                        return;
                    } else {
                        Utils.showToast(this, "两次密码不一致");
                        return;
                    }
                default:
                    return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        e.printStackTrace();
    }

    private class UploadThread extends Thread {
        private UploadThread() {
        }

        /* synthetic */ UploadThread(ChangePasswordActivity changePasswordActivity, UploadThread uploadThread) {
            this();
        }

        public void run() {
            super.run();
            Message msg = new Message();
            msg.what = 2;
            msg.obj = "请稍后...";
            ChangePasswordActivity.this.myHandler.sendMessage(msg);
            try {
                ChangePassword change = new ChangePassword(ChangePasswordActivity.this);
                change.strOldPassword = ChangePasswordActivity.this.edtOld.getText().toString();
                change.strNewPassword = ChangePasswordActivity.this.edtNew.getText().toString();
                if (change.changePsw()) {
                    Message msg2 = new Message();
                    msg2.what = 4;
                    msg2.obj = "修改成功";
                    ChangePasswordActivity.this.myHandler.sendMessage(msg2);
                    UserAccount account = new UserAccount(ChangePasswordActivity.this);
                    account.getFromDatabase();
                    account.Item.mPassword = change.strNewPassword;
                    account.setToDatabase();
                    account.close();
                    ChangePasswordActivity.this.myHandler.sendEmptyMessage(5);
                } else {
                    Message msg22 = new Message();
                    msg22.what = 4;
                    msg22.obj = "修改失败";
                    ChangePasswordActivity.this.myHandler.sendMessage(msg22);
                }
            } catch (Exception e) {
                e.printStackTrace();
                Message msg23 = new Message();
                msg23.what = 4;
                msg23.obj = "修改失败，请检查网络";
                ChangePasswordActivity.this.myHandler.sendMessage(msg23);
            }
            ChangePasswordActivity.this.myHandler.sendEmptyMessage(3);
        }
    }
}
