package com.gaoding.dingcan.view;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.widget.ImageView;
import android.widget.TextView;
import com.gaoding.dingcan.R;

public class CustomProgressDialog extends Dialog {
    private static CustomProgressDialog customProgressDialog = null;
    private Context context = null;

    public CustomProgressDialog(Context context2) {
        super(context2);
        this.context = context2;
    }

    public CustomProgressDialog(Context context2, int theme) {
        super(context2, theme);
    }

    public static CustomProgressDialog createDialog(Context context2) {
        customProgressDialog = new CustomProgressDialog(context2, R.style.CustomProgressDialog);
        customProgressDialog.setContentView((int) R.layout.dialog_custom_progress);
        customProgressDialog.getWindow().getAttributes().gravity = 17;
        return customProgressDialog;
    }

    public void onWindowFocusChanged(boolean hasFocus) {
        if (customProgressDialog != null) {
            ((AnimationDrawable) ((ImageView) customProgressDialog.findViewById(R.id.loadingImageView)).getBackground()).start();
        }
    }

    public CustomProgressDialog setTitile(String strTitle) {
        return customProgressDialog;
    }

    public CustomProgressDialog setMessage(String strMessage) {
        TextView tvMsg = (TextView) customProgressDialog.findViewById(R.id.id_tv_loadingmsg);
        if (tvMsg != null) {
            tvMsg.setText(strMessage);
        }
        return customProgressDialog;
    }
}
