package com.gaoding.dingcan.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.gaoding.dingcan.R;
import com.gaoding.dingcan.database.bean.UnitBean;
import com.gaoding.dingcan.database.controller.Unit;
import com.gaoding.dingcan.http.controller.GetUnit;
import com.gaoding.dingcan.util.LogUtils;
import com.gaoding.dingcan.util.Utils;

public class UnitListActivity extends Activity implements View.OnClickListener {
    private static final int DISMISS_PROGRESS_DIALOG = 3;
    private static final int REFRESH_VIEW = 1;
    private static final int SHOW_PROGRESS_DIALOG = 2;
    private static final int SHOW_TOAST_MESSAGE = 4;
    private String AreaId;
    private ImageView btnBack;
    /* access modifiers changed from: private */
    public MessageAdapter listAdapter;
    private ListView listView;
    private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            UnitListActivity.this.unit.query();
            UnitListActivity.this.listAdapter.notifyDataSetChanged();
        }
    };
    private LinearLayout mLayoutProgress;
    /* access modifiers changed from: private */
    public ProgressDialog mProgressDialog;
    /* access modifiers changed from: private */
    public Handler myHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    try {
                        UnitListActivity.this.unit.query();
                        UnitListActivity.this.listAdapter.notifyDataSetChanged();
                        return;
                    } catch (Exception e) {
                        e.printStackTrace();
                        return;
                    }
                case 2:
                    try {
                        if (UnitListActivity.this.mProgressDialog != null) {
                            if (UnitListActivity.this.mProgressDialog.isShowing()) {
                                UnitListActivity.this.mProgressDialog.dismiss();
                            }
                            UnitListActivity.this.mProgressDialog = null;
                        }
                        UnitListActivity.this.mProgressDialog = Utils.getProgressDialog(UnitListActivity.this, (String) msg.obj);
                        UnitListActivity.this.mProgressDialog.show();
                        return;
                    } catch (Exception e2) {
                        e2.printStackTrace();
                        return;
                    }
                case 3:
                    try {
                        if (UnitListActivity.this.mProgressDialog != null && UnitListActivity.this.mProgressDialog.isShowing()) {
                            UnitListActivity.this.mProgressDialog.dismiss();
                        }
                        if (UnitListActivity.this.unit.list.size() == 0) {
                            UnitListActivity.this.tvNoInfo.setVisibility(0);
                        } else {
                            UnitListActivity.this.tvNoInfo.setVisibility(8);
                        }
                        UnitListActivity.this.listAdapter.notifyDataSetChanged();
                        return;
                    } catch (Exception e3) {
                        e3.printStackTrace();
                        return;
                    }
                default:
                    return;
            }
        }
    };
    /* access modifiers changed from: private */
    public Resources resources;
    /* access modifiers changed from: private */
    public TextView tvNoInfo;
    /* access modifiers changed from: private */
    public Unit unit;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LogUtils.logDebug("AreaListActivity");
        setContentView((int) R.layout.activity_unitlist);
        this.resources = getResources();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            this.AreaId = bundle.getString("AreaId");
        }
        initViews();
        loadAreas();
        this.unit = new Unit(this);
        this.unit.query();
        this.listAdapter = new MessageAdapter(this, null);
        this.listView.setAdapter((ListAdapter) this.listAdapter);
    }

    private void initViews() {
        this.btnBack = (ImageView) findViewById(R.id.btnBack);
        this.btnBack.setOnClickListener(this);
        this.listView = (ListView) findViewById(R.id.listview_area);
        this.tvNoInfo = (TextView) findViewById(R.id.tv_no_info);
        this.mLayoutProgress = (LinearLayout) findViewById(R.id.linearlayout_progress);
        this.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                UnitBean bean = UnitListActivity.this.unit.list.get(arg2);
                Intent intent = new Intent();
                intent.setClass(UnitListActivity.this, RestaurantListActivity.class);
                intent.putExtra("UnitId", bean.mId);
                LogUtils.logDebug("UnitId = " + bean.mId);
                UnitListActivity.this.startActivity(intent);
            }
        });
    }

    private void loadAreas() {
        if (Utils.isNetWorkConnect(this)) {
            this.tvNoInfo.setVisibility(8);
            new GetAreaThread(this.AreaId).start();
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnBack:
                finish();
                return;
            default:
                return;
        }
    }

    private class MessageAdapter extends BaseAdapter {
        private MessageAdapter() {
        }

        /* synthetic */ MessageAdapter(UnitListActivity unitListActivity, MessageAdapter messageAdapter) {
            this();
        }

        public int getCount() {
            if (UnitListActivity.this.unit.list == null) {
                return 0;
            }
            return UnitListActivity.this.unit.list.size();
        }

        public Object getItem(int position) {
            return Integer.valueOf(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public void unregisterDataSetObserver(DataSetObserver observer) {
            if (observer != null) {
                super.unregisterDataSetObserver(observer);
            }
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            int mPosition = position;
            View view = ((LayoutInflater) UnitListActivity.this.getSystemService("layout_inflater")).inflate((int) R.layout.msg_item, (ViewGroup) null);
            try {
                HolderViewTopic holderView = new HolderViewTopic(UnitListActivity.this, null);
                holderView.mTvTitle = (TextView) view.findViewById(R.id.tv_item_title);
                try {
                    holderView.mTvTitle.setText(UnitListActivity.this.unit.list.get(mPosition).mName);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            return view;
        }
    }

    private class HolderViewTopic {
        /* access modifiers changed from: private */
        public TextView mTvTitle;

        private HolderViewTopic() {
        }

        /* synthetic */ HolderViewTopic(UnitListActivity unitListActivity, HolderViewTopic holderViewTopic) {
            this();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.unit.close();
        super.onDestroy();
    }

    private class GetAreaThread extends Thread {
        public String mAreaId;

        public GetAreaThread(String mAreaId2) {
            this.mAreaId = mAreaId2;
        }

        public void run() {
            super.run();
            Message msg = new Message();
            msg.what = 2;
            msg.obj = UnitListActivity.this.resources.getString(R.string.please_wait);
            UnitListActivity.this.myHandler.sendMessage(msg);
            try {
                GetUnit get = new GetUnit(UnitListActivity.this);
                get.mAreaId = this.mAreaId;
                if (get.GetList()) {
                    UnitListActivity.this.unit = get.unit;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            UnitListActivity.this.myHandler.sendEmptyMessage(3);
        }
    }
}
