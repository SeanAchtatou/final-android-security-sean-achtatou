package com.gaoding.dingcan.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.gaoding.dingcan.R;
import com.gaoding.dingcan.database.bean.OrderBean;
import com.gaoding.dingcan.database.controller.UserInfo;
import com.gaoding.dingcan.http.controller.GetOrder;
import com.gaoding.dingcan.util.LogUtils;
import com.gaoding.dingcan.util.Utils;
import java.util.List;

public class OrderActivity extends Activity implements View.OnClickListener {
    private static final int DISMISS_PROGRESS_DIALOG = 3;
    public static final String ORDER_TYPE_ALL = "ORDER_TYPE_ALL";
    public static final String ORDER_TYPE_TODAY = "ORDER_TYPE_TODAY";
    private static final int REFRESH_VIEW = 1;
    private static final int SHOW_PROGRESS_DIALOG = 2;
    private static final int SHOW_TOAST_MESSAGE = 4;
    /* access modifiers changed from: private */
    public boolean bIsTodayOrder = false;
    private ImageView btnBack;
    /* access modifiers changed from: private */
    public UserInfo info;
    /* access modifiers changed from: private */
    public MessageAdapter listAdapter;
    /* access modifiers changed from: private */
    public List<OrderBean> listOrder;
    private ListView listView;
    /* access modifiers changed from: private */
    public ProgressDialog mProgressDialog;
    /* access modifiers changed from: private */
    public Handler myHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    try {
                        OrderActivity.this.listAdapter.notifyDataSetChanged();
                        return;
                    } catch (Exception e) {
                        e.printStackTrace();
                        return;
                    }
                case 2:
                    try {
                        if (OrderActivity.this.mProgressDialog != null) {
                            if (OrderActivity.this.mProgressDialog.isShowing()) {
                                OrderActivity.this.mProgressDialog.dismiss();
                            }
                            OrderActivity.this.mProgressDialog = null;
                        }
                        OrderActivity.this.mProgressDialog = Utils.getProgressDialog(OrderActivity.this, (String) msg.obj);
                        OrderActivity.this.mProgressDialog.show();
                        return;
                    } catch (Exception e2) {
                        e2.printStackTrace();
                        return;
                    }
                case 3:
                    try {
                        if (OrderActivity.this.mProgressDialog != null && OrderActivity.this.mProgressDialog.isShowing()) {
                            OrderActivity.this.mProgressDialog.dismiss();
                        }
                        OrderActivity.this.listAdapter.notifyDataSetChanged();
                        return;
                    } catch (Exception e3) {
                        e3.printStackTrace();
                        return;
                    }
                default:
                    return;
            }
        }
    };
    /* access modifiers changed from: private */
    public Resources resources;
    private String strOrderType;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LogUtils.logDebug("OrderActivity");
        setContentView((int) R.layout.activity_order_list);
        this.resources = getResources();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            this.strOrderType = bundle.getString("type");
            if (this.strOrderType.equals(ORDER_TYPE_TODAY)) {
                this.bIsTodayOrder = true;
            } else if (this.strOrderType.equals(ORDER_TYPE_ALL)) {
                this.bIsTodayOrder = false;
            }
        }
        initViews();
        this.info = new UserInfo(this);
        this.info.query();
        this.listAdapter = new MessageAdapter(this, null);
        this.listView.setAdapter((ListAdapter) this.listAdapter);
    }

    private void initViews() {
        this.btnBack = (ImageView) findViewById(R.id.btnBack);
        this.btnBack.setOnClickListener(this);
        this.listView = (ListView) findViewById(R.id.listview_order);
        this.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                OrderBean bean = (OrderBean) OrderActivity.this.listOrder.get(arg2);
                Intent intent = new Intent();
                intent.setClass(OrderActivity.this, OrderInfoActivity.class);
                intent.putExtra("OrderId", bean.mId);
                intent.putExtra("Time", bean.mTime);
                intent.putExtra("State", bean.mState);
                intent.putExtra("Address", bean.mAddress);
                intent.putExtra("Phone", bean.mPhone);
                intent.putExtra("Desc", bean.mDesc);
                intent.putExtra("Price", bean.mPrice);
                intent.putExtra("Total", bean.mTotal);
                intent.putExtra("Name", bean.mRestaurantId);
                intent.putExtra("Num", bean.mNum);
                intent.putExtra("CactivtyId", bean.mCactivtyId);
                intent.putExtra("Type", bean.mType);
                intent.putExtra("ActiveId", bean.mActiveId);
                OrderActivity.this.startActivity(intent);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        new GetThread(this, null).start();
        super.onResume();
    }

    private void sendShopping() {
        if (Utils.isNetWorkConnect(this)) {
            new SendThread(this, null).start();
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnBack:
                finish();
                return;
            default:
                return;
        }
    }

    private class MessageAdapter extends BaseAdapter {
        private MessageAdapter() {
        }

        /* synthetic */ MessageAdapter(OrderActivity orderActivity, MessageAdapter messageAdapter) {
            this();
        }

        public int getCount() {
            if (OrderActivity.this.listOrder == null) {
                return 0;
            }
            return OrderActivity.this.listOrder.size();
        }

        public Object getItem(int position) {
            return Integer.valueOf(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public void unregisterDataSetObserver(DataSetObserver observer) {
            if (observer != null) {
                super.unregisterDataSetObserver(observer);
            }
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View view = ((LayoutInflater) OrderActivity.this.getSystemService("layout_inflater")).inflate((int) R.layout.order_item, (ViewGroup) null);
            try {
                HolderViewTopic holderView = new HolderViewTopic(OrderActivity.this, null);
                holderView.mTvNum = (TextView) view.findViewById(R.id.tv_item_num);
                holderView.mTvName = (TextView) view.findViewById(R.id.tv_item_name);
                holderView.mTvTime = (TextView) view.findViewById(R.id.tv_item_time);
                holderView.mTvMoney = (TextView) view.findViewById(R.id.tv_item_money);
                try {
                    holderView.mTvNum.setText(String.valueOf("") + "订单号：" + ((OrderBean) OrderActivity.this.listOrder.get(position)).mNum);
                    holderView.mTvName.setText(String.valueOf("") + "餐厅名称：" + ((OrderBean) OrderActivity.this.listOrder.get(position)).mRestaurantId);
                    holderView.mTvTime.setText(String.valueOf("") + "时间：" + ((OrderBean) OrderActivity.this.listOrder.get(position)).mTime);
                    holderView.mTvMoney.setText(String.valueOf(String.valueOf("") + "总数量：" + ((OrderBean) OrderActivity.this.listOrder.get(position)).mTotal + "份") + "     总价格：￥" + ((OrderBean) OrderActivity.this.listOrder.get(position)).mPrice);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            return view;
        }
    }

    private class HolderViewTopic {
        /* access modifiers changed from: private */
        public TextView mTvMoney;
        /* access modifiers changed from: private */
        public TextView mTvName;
        /* access modifiers changed from: private */
        public TextView mTvNum;
        /* access modifiers changed from: private */
        public TextView mTvTime;

        private HolderViewTopic() {
        }

        /* synthetic */ HolderViewTopic(OrderActivity orderActivity, HolderViewTopic holderViewTopic) {
            this();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.info.close();
        super.onDestroy();
    }

    private class SendThread extends Thread {
        private SendThread() {
        }

        /* synthetic */ SendThread(OrderActivity orderActivity, SendThread sendThread) {
            this();
        }

        public void run() {
            super.run();
            Message msg = new Message();
            msg.what = 2;
            msg.obj = OrderActivity.this.resources.getString(R.string.please_wait);
            OrderActivity.this.myHandler.sendMessage(msg);
            OrderActivity.this.myHandler.sendEmptyMessage(3);
        }
    }

    private class GetThread extends Thread {
        private GetThread() {
        }

        /* synthetic */ GetThread(OrderActivity orderActivity, GetThread getThread) {
            this();
        }

        public void run() {
            boolean result;
            super.run();
            Message msg = new Message();
            msg.what = 2;
            msg.obj = OrderActivity.this.resources.getString(R.string.please_wait);
            OrderActivity.this.myHandler.sendMessage(msg);
            try {
                GetOrder get = new GetOrder(OrderActivity.this);
                get.mMemberId = OrderActivity.this.info.Item.mUid;
                if (OrderActivity.this.bIsTodayOrder) {
                    result = get.GetTodayList();
                } else {
                    result = get.GetAllList();
                }
                if (result) {
                    OrderActivity.this.listOrder = get.list;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            OrderActivity.this.myHandler.sendEmptyMessage(3);
        }
    }
}
