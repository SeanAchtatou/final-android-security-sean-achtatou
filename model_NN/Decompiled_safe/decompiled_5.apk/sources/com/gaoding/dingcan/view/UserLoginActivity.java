package com.gaoding.dingcan.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.gaoding.dingcan.R;
import com.gaoding.dingcan.database.DataStore;
import com.gaoding.dingcan.database.controller.UserAccount;
import com.gaoding.dingcan.http.controller.Login;
import com.gaoding.dingcan.util.LogUtils;
import com.gaoding.dingcan.util.Utils;

public class UserLoginActivity extends Activity implements View.OnClickListener {
    private static final int DISMISS_PROGRESS_DIALOG = 3;
    private static final int LOGIN_SUCCESS = 5;
    private static final int REFRESH_VIEW = 1;
    private static final int REQUEST_REGISTER = 101;
    private static final int SHOW_PROGRESS_DIALOG = 2;
    private static final int SHOW_TOAST_MESSAGE = 4;
    private ImageView btnBack;
    private Button btnLogin;
    private Button btnRegister;
    private TextView btnforgetPassword;
    private EditText mEditTextPassWord;
    private EditText mEditTextTelNumber;
    Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                default:
                    return;
                case 2:
                    try {
                        if (UserLoginActivity.this.mProgressDialog != null) {
                            if (UserLoginActivity.this.mProgressDialog.isShowing()) {
                                UserLoginActivity.this.mProgressDialog.dismiss();
                            }
                            UserLoginActivity.this.mProgressDialog = null;
                        }
                        UserLoginActivity.this.mProgressDialog = Utils.getProgressDialog(UserLoginActivity.this, (String) msg.obj);
                        UserLoginActivity.this.mProgressDialog.show();
                        return;
                    } catch (Exception e) {
                        e.printStackTrace();
                        return;
                    }
                case 3:
                    try {
                        if (UserLoginActivity.this.mProgressDialog != null && UserLoginActivity.this.mProgressDialog.isShowing()) {
                            UserLoginActivity.this.mProgressDialog.dismiss();
                            return;
                        }
                        return;
                    } catch (Exception e2) {
                        e2.printStackTrace();
                        return;
                    }
                case 4:
                    try {
                        Utils.showToast(UserLoginActivity.this, msg.obj.toString());
                        return;
                    } catch (Exception e3) {
                        e3.printStackTrace();
                        return;
                    }
                case 5:
                    UserLoginActivity.this.startActivity(new Intent(UserLoginActivity.this, AreaUnitListActivity.class));
                    UserLoginActivity.this.finish();
                    return;
            }
        }
    };
    /* access modifiers changed from: private */
    public ProgressDialog mProgressDialog;
    private SharedPreferences mSharedPreferences = null;
    private String mStringAcount;
    private String mStringPassword;
    /* access modifiers changed from: private */
    public Resources resources;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_user_login);
        initViews();
        init();
        userLogin(true);
    }

    private void init() {
        this.resources = getResources();
        this.mSharedPreferences = getSharedPreferences("lRmessage", 0);
    }

    private void initViews() {
        this.btnLogin = (Button) findViewById(R.id.userloginLogin);
        this.btnRegister = (Button) findViewById(R.id.userloginRegister);
        this.btnBack = (ImageView) findViewById(R.id.userloginBack);
        this.btnforgetPassword = (TextView) findViewById(R.id.userloginForgot);
        this.btnRegister.setOnClickListener(this);
        this.btnLogin.setOnClickListener(this);
        this.btnBack.setOnClickListener(this);
        this.btnforgetPassword.setOnClickListener(this);
        this.mEditTextPassWord = (EditText) findViewById(R.id.userloginPassword);
        this.mEditTextTelNumber = (EditText) findViewById(R.id.userloginAccount);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (REQUEST_REGISTER == requestCode && -1 == resultCode) {
            this.mStringAcount = data.getExtras().getString(DataStore.UserTable.USER_ACCOUNT);
            this.mStringPassword = data.getExtras().getString(DataStore.UserTable.USER_PASSWORD);
            this.mEditTextTelNumber.setText(this.mStringAcount);
            this.mEditTextPassWord.setText(this.mStringPassword);
            new LoginThread(this.mStringAcount, this.mStringPassword).start();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public void onClick(View v) {
        try {
            switch (v.getId()) {
                case R.id.userloginBack:
                    finish();
                    return;
                case R.id.userloginAccount:
                case R.id.userloginPassword:
                case R.id.userloginButton:
                default:
                    return;
                case R.id.userloginRegister:
                    startActivityForResult(new Intent(this, UserRegisterActivity.class), REQUEST_REGISTER);
                    return;
                case R.id.userloginLogin:
                    if (this.mEditTextTelNumber.length() < 4 || this.mEditTextPassWord.length() < 6) {
                        Utils.showToast(this, "请确认账号密码输入正确");
                        return;
                    } else {
                        userLogin(false);
                        return;
                    }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        e.printStackTrace();
    }

    private boolean InputValidate() {
        this.mStringPassword = this.mEditTextPassWord.getText().toString();
        this.mStringAcount = this.mEditTextTelNumber.getText().toString();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.mSharedPreferences.edit().putString("userName", this.mEditTextTelNumber.getText().toString()).commit();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            LogUtils.logDebug("Enter LiginRegisterActivity,from back_Key!");
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    public class LoginThread extends Thread {
        String acount;
        boolean isAuto = true;
        String password;

        public LoginThread() {
        }

        public LoginThread(String macount, String mpassword) {
            this.acount = macount;
            this.password = mpassword;
        }

        public void run() {
            boolean result;
            super.run();
            Message mMessage = new Message();
            mMessage.what = 2;
            mMessage.obj = UserLoginActivity.this.resources.getString(R.string.please_wait);
            UserLoginActivity.this.mHandler.sendMessage(mMessage);
            Login login = new Login(UserLoginActivity.this);
            if (this.isAuto) {
                result = login.UserAutoLogin();
            } else {
                result = login.UserLogin(this.acount, this.password);
            }
            if (result) {
                UserLoginActivity.this.mHandler.sendEmptyMessage(5);
            } else {
                Message mResultMessage = new Message();
                mResultMessage.what = 4;
                mResultMessage.obj = login.message;
                UserLoginActivity.this.mHandler.sendMessage(mResultMessage);
            }
            UserLoginActivity.this.mHandler.sendEmptyMessage(3);
        }
    }

    private boolean userLogin(boolean isAuto) {
        if (!Utils.isNetWorkConnect(this)) {
            Utils.showToast(this, getResources().getString(R.string.no_network_message));
            return false;
        } else if (InputValidate()) {
            if (isAuto) {
                UserAccount account = new UserAccount(this);
                if (account.getCount() > 0) {
                    new LoginThread().start();
                }
                account.close();
            } else {
                new LoginThread(this.mStringAcount, this.mStringPassword).start();
            }
            return true;
        } else {
            Utils.showToast(this, getResources().getString(R.string.error_input));
            return false;
        }
    }
}
