package com.gaoding.dingcan.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.TextView;
import com.gaoding.dingcan.R;
import com.gaoding.dingcan.http.controller.GetRestaurantInfo;
import com.gaoding.dingcan.util.LogUtils;
import com.gaoding.dingcan.util.Utils;

public class RestaurantInfoActivity extends Activity implements View.OnClickListener {
    private static final int DISMISS_PROGRESS_DIALOG = 3;
    private static final int REFRESH_MENU = 6;
    private static final int REFRESH_TYPE = 5;
    private static final int REFRESH_VIEW = 1;
    private static final int SHOW_PROGRESS_DIALOG = 2;
    private static final int SHOW_TOAST_MESSAGE = 4;
    private String RestaurantId;
    /* access modifiers changed from: private */
    public ProgressDialog mProgressDialog;
    /* access modifiers changed from: private */
    public Handler myHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    try {
                        RestaurantInfoActivity.this.tvOpen.setText(((Object) RestaurantInfoActivity.this.resources.getText(R.string.res_open)) + ":  " + RestaurantInfoActivity.this.strResOpen);
                        if (RestaurantInfoActivity.this.strResState.equals(GetRestaurantInfo.RES_STATE_CLOSE)) {
                            RestaurantInfoActivity.this.tvState.setText(((Object) RestaurantInfoActivity.this.resources.getText(R.string.res_state)) + ":  " + "休息中");
                        } else {
                            RestaurantInfoActivity.this.tvState.setText(((Object) RestaurantInfoActivity.this.resources.getText(R.string.res_state)) + ":  " + "正在营业");
                        }
                        RestaurantInfoActivity.this.tvTime.setText(((Object) RestaurantInfoActivity.this.resources.getText(R.string.res_send_time)) + ":  " + RestaurantInfoActivity.this.strResSendTime);
                        RestaurantInfoActivity.this.tvAddress.setText(((Object) RestaurantInfoActivity.this.resources.getText(R.string.res_address)) + ":  " + RestaurantInfoActivity.this.strResAddress);
                        RestaurantInfoActivity.this.tvAreaTo.setText(((Object) RestaurantInfoActivity.this.resources.getText(R.string.res_area)) + ":  " + RestaurantInfoActivity.this.strResAreaTo);
                        RestaurantInfoActivity.this.tvNotice.setText(((Object) RestaurantInfoActivity.this.resources.getText(R.string.res_notice)) + ":  " + RestaurantInfoActivity.this.strResNotic);
                        RestaurantInfoActivity.this.tvDesc.setText(((Object) RestaurantInfoActivity.this.resources.getText(R.string.res_desc)) + ":  " + RestaurantInfoActivity.this.strResDesc);
                        RestaurantInfoActivity.this.tvStart.setText(((Object) RestaurantInfoActivity.this.resources.getText(R.string.res_start)) + ":  ￥" + RestaurantInfoActivity.this.strStartPrice);
                        return;
                    } catch (Exception e) {
                        e.printStackTrace();
                        return;
                    }
                case 2:
                    try {
                        if (RestaurantInfoActivity.this.mProgressDialog != null) {
                            if (RestaurantInfoActivity.this.mProgressDialog.isShowing()) {
                                RestaurantInfoActivity.this.mProgressDialog.dismiss();
                            }
                            RestaurantInfoActivity.this.mProgressDialog = null;
                        }
                        RestaurantInfoActivity.this.mProgressDialog = Utils.getProgressDialog(RestaurantInfoActivity.this, (String) msg.obj);
                        RestaurantInfoActivity.this.mProgressDialog.show();
                        return;
                    } catch (Exception e2) {
                        e2.printStackTrace();
                        return;
                    }
                case 3:
                    try {
                        if (RestaurantInfoActivity.this.mProgressDialog != null && RestaurantInfoActivity.this.mProgressDialog.isShowing()) {
                            RestaurantInfoActivity.this.mProgressDialog.dismiss();
                            return;
                        }
                        return;
                    } catch (Exception e3) {
                        e3.printStackTrace();
                        return;
                    }
                default:
                    return;
            }
        }
    };
    /* access modifiers changed from: private */
    public Resources resources;
    /* access modifiers changed from: private */
    public String strResAddress = "";
    /* access modifiers changed from: private */
    public String strResAreaTo = "";
    /* access modifiers changed from: private */
    public String strResDesc = "";
    /* access modifiers changed from: private */
    public String strResName = "";
    /* access modifiers changed from: private */
    public String strResNotic = "";
    /* access modifiers changed from: private */
    public String strResOpen = "";
    /* access modifiers changed from: private */
    public String strResSendTime = "";
    /* access modifiers changed from: private */
    public String strResState = "";
    /* access modifiers changed from: private */
    public String strStartPrice = "10";
    /* access modifiers changed from: private */
    public TextView tvAddress;
    /* access modifiers changed from: private */
    public TextView tvAreaTo;
    /* access modifiers changed from: private */
    public TextView tvDesc;
    private TextView tvName;
    /* access modifiers changed from: private */
    public TextView tvNotice;
    /* access modifiers changed from: private */
    public TextView tvOpen;
    /* access modifiers changed from: private */
    public TextView tvStart;
    /* access modifiers changed from: private */
    public TextView tvState;
    /* access modifiers changed from: private */
    public TextView tvTime;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LogUtils.logDebug("MenuListActivity");
        setContentView((int) R.layout.activity_res_info);
        this.resources = getResources();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            this.RestaurantId = bundle.getString("RestaurantId");
            this.strResName = bundle.getString("RestaurantName");
        }
        initViews();
        loadList();
    }

    private void initViews() {
        this.tvName = (TextView) findViewById(R.id.res_name);
        this.tvOpen = (TextView) findViewById(R.id.res_open);
        this.tvTime = (TextView) findViewById(R.id.res_send_time);
        this.tvTime.setVisibility(8);
        this.tvState = (TextView) findViewById(R.id.res_state);
        this.tvAddress = (TextView) findViewById(R.id.res_address);
        this.tvAreaTo = (TextView) findViewById(R.id.res_areato);
        this.tvNotice = (TextView) findViewById(R.id.res_notice);
        this.tvDesc = (TextView) findViewById(R.id.res_desc);
        this.tvStart = (TextView) findViewById(R.id.res_start);
        this.tvStart.setVisibility(8);
        this.tvName.setText(((Object) this.resources.getText(R.string.res_name)) + ":  " + this.strResName);
    }

    private void loadList() {
        if (Utils.isNetWorkConnect(this)) {
            new GetThread(this.RestaurantId).start();
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnBack:
                finish();
                return;
            case R.id.shopingCart:
                Intent intent = new Intent();
                intent.setClass(this, ShoppingListActivity.class);
                intent.putExtra("RestaurantId", this.RestaurantId);
                intent.putExtra("StartPrice", this.strStartPrice);
                startActivity(intent);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    private class GetThread extends Thread {
        public String RestaurantId;

        public GetThread(String RestaurantId2) {
            this.RestaurantId = RestaurantId2;
        }

        public void run() {
            super.run();
            Message msg = new Message();
            msg.what = 2;
            msg.obj = RestaurantInfoActivity.this.resources.getString(R.string.please_wait);
            RestaurantInfoActivity.this.myHandler.sendMessage(msg);
            try {
                GetRestaurantInfo info = new GetRestaurantInfo(RestaurantInfoActivity.this);
                info.mResId = this.RestaurantId;
                if (info.GetInfo()) {
                    LogUtils.logDebug("GetRestaurantInfo return true");
                    RestaurantInfoActivity.this.strResName = info.item.mName;
                    RestaurantInfoActivity.this.strResOpen = info.item.mOpenTime;
                    RestaurantInfoActivity.this.strResState = info.item.mState;
                    RestaurantInfoActivity.this.strResSendTime = info.item.mSendTime;
                    RestaurantInfoActivity.this.strResAddress = info.item.mAddress;
                    RestaurantInfoActivity.this.strResAreaTo = info.item.mSendArea;
                    RestaurantInfoActivity.this.strResNotic = info.item.mNotice;
                    RestaurantInfoActivity.this.strResDesc = info.item.mSendDesc;
                    RestaurantInfoActivity.this.strStartPrice = info.item.mStartPrice;
                    RestaurantInfoActivity.this.myHandler.sendEmptyMessage(1);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            RestaurantInfoActivity.this.myHandler.sendEmptyMessage(3);
        }
    }
}
