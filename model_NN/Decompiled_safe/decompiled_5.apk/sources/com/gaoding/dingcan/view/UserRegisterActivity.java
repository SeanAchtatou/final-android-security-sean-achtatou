package com.gaoding.dingcan.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import com.gaoding.dingcan.R;
import com.gaoding.dingcan.database.DataStore;
import com.gaoding.dingcan.http.controller.Register;
import com.gaoding.dingcan.util.LogUtils;
import com.gaoding.dingcan.util.Utils;
import com.gaoding.dingcan.util.Verify;
import java.util.regex.Pattern;

public class UserRegisterActivity extends Activity implements View.OnClickListener {
    private static final int DISMISS_PROGRESS_DIALOG = 3;
    private static final int REFRESH_VIEW = 1;
    private static final int REGISTER_SUCCESS = 5;
    private static final int SHOW_PROGRESS_DIALOG = 2;
    private static final int SHOW_TOAST_MESSAGE = 4;
    private ImageView btnBack;
    private Button btnRegister;
    private EditText editTextAccount;
    private EditText editTextEmail;
    private EditText editTextPassWords;
    private EditText editTextPhone;
    Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                default:
                    return;
                case 2:
                    try {
                        if (UserRegisterActivity.this.mProgressDialog != null) {
                            if (UserRegisterActivity.this.mProgressDialog.isShowing()) {
                                UserRegisterActivity.this.mProgressDialog.dismiss();
                            }
                            UserRegisterActivity.this.mProgressDialog = null;
                        }
                        UserRegisterActivity.this.mProgressDialog = Utils.getProgressDialog(UserRegisterActivity.this, (String) msg.obj);
                        UserRegisterActivity.this.mProgressDialog.show();
                        return;
                    } catch (Exception e) {
                        e.printStackTrace();
                        return;
                    }
                case 3:
                    try {
                        if (UserRegisterActivity.this.mProgressDialog != null && UserRegisterActivity.this.mProgressDialog.isShowing()) {
                            UserRegisterActivity.this.mProgressDialog.dismiss();
                            return;
                        }
                        return;
                    } catch (Exception e2) {
                        e2.printStackTrace();
                        return;
                    }
                case 4:
                    try {
                        Utils.showToast(UserRegisterActivity.this, msg.obj.toString());
                        return;
                    } catch (Exception e3) {
                        e3.printStackTrace();
                        return;
                    }
                case 5:
                    Intent data = new Intent();
                    data.putExtra(DataStore.UserTable.USER_ACCOUNT, UserRegisterActivity.this.strAccount);
                    data.putExtra(DataStore.UserTable.USER_PASSWORD, UserRegisterActivity.this.strPassword);
                    UserRegisterActivity.this.setResult(-1, data);
                    UserRegisterActivity.this.finish();
                    return;
            }
        }
    };
    /* access modifiers changed from: private */
    public ProgressDialog mProgressDialog;
    /* access modifiers changed from: private */
    public Resources resources;
    /* access modifiers changed from: private */
    public String strAccount;
    /* access modifiers changed from: private */
    public String strEmail;
    /* access modifiers changed from: private */
    public String strPassword;
    /* access modifiers changed from: private */
    public String strPhone;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_user_register);
        initViews();
        init();
    }

    private void initViews() {
        this.btnRegister = (Button) findViewById(R.id.userregisterRegister);
        this.btnBack = (ImageView) findViewById(R.id.userregisterBack);
        this.editTextPhone = (EditText) findViewById(R.id.userregister_phone);
        this.editTextPassWords = (EditText) findViewById(R.id.userregister_password);
        this.editTextAccount = (EditText) findViewById(R.id.userregister_account);
        this.editTextEmail = (EditText) findViewById(R.id.userregister_email);
        this.btnRegister.setOnClickListener(this);
        this.btnBack.setOnClickListener(this);
    }

    private void init() {
        this.resources = getResources();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public void onClick(View v) {
        try {
            switch (v.getId()) {
                case R.id.userregisterBack:
                    setResult(0);
                    finish();
                    return;
                case R.id.userregisterRegister:
                    userRegister();
                    return;
                case R.id.userregisterStatement:
                default:
                    return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        e.printStackTrace();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            setResult(0);
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    private boolean ValidateTelNumber() {
        this.strPhone = this.editTextPhone.getText().toString();
        Message mMessage = new Message();
        if (Pattern.matches("^1\\d{10}$", this.strPhone.trim())) {
            return true;
        }
        LogUtils.logDebug("TelNumberStr is not Null!");
        Verify.isNull(this.strPhone);
        this.mHandler.sendMessage(mMessage);
        return false;
    }

    private boolean InputValidate() {
        this.strAccount = this.editTextAccount.getText().toString().trim();
        this.strPhone = this.editTextPhone.getText().toString().trim();
        this.strPassword = this.editTextPassWords.getText().toString().trim();
        this.strEmail = this.editTextEmail.getText().toString().trim();
        if (Verify.isNull(this.strAccount) || Verify.isNull(this.strPhone) || Verify.isNull(this.strPassword) || Verify.isNull(this.strEmail)) {
            Utils.showToast(this, this.resources.getString(R.string.please_input_complete));
            return false;
        } else if (!Pattern.matches("^1\\d{10}$", this.strPhone)) {
            Utils.showToast(this, this.resources.getString(R.string.please_check_phone));
            return false;
        } else if (this.strPassword.length() < 6 || this.strPassword.length() > 20) {
            Utils.showToast(this, this.resources.getString(R.string.please_check_password));
            return false;
        } else if (this.strAccount.length() < 4 || this.strAccount.length() > 20) {
            Utils.showToast(this, this.resources.getString(R.string.please_check_account));
            return false;
        } else if (!this.strEmail.contains("@")) {
            Utils.showToast(this, this.resources.getString(R.string.please_check_email));
            return false;
        } else {
            LogUtils.logDebug("InitMessageAndValidate True");
            return true;
        }
    }

    private boolean userRegister() {
        if (!Utils.isNetWorkConnect(this)) {
            Utils.showToast(this, getResources().getString(R.string.no_network_message));
            return false;
        } else if (!InputValidate()) {
            return false;
        } else {
            new RegisterThread(this, null).start();
            return true;
        }
    }

    private class RegisterThread extends Thread {
        private RegisterThread() {
        }

        /* synthetic */ RegisterThread(UserRegisterActivity userRegisterActivity, RegisterThread registerThread) {
            this();
        }

        public void run() {
            super.run();
            Message mMessage = new Message();
            mMessage.what = 2;
            mMessage.obj = UserRegisterActivity.this.resources.getString(R.string.please_wait);
            UserRegisterActivity.this.mHandler.sendMessage(mMessage);
            Register register = new Register(UserRegisterActivity.this);
            if (register.UserRegister(UserRegisterActivity.this.strAccount, UserRegisterActivity.this.strPassword, UserRegisterActivity.this.strEmail, UserRegisterActivity.this.strPhone)) {
                Message mResultMessage = new Message();
                mResultMessage.what = 4;
                mResultMessage.obj = "注册成功";
                UserRegisterActivity.this.mHandler.sendMessage(mResultMessage);
                UserRegisterActivity.this.mHandler.sendEmptyMessage(5);
            } else {
                String strMessage = register.message;
                Message mResultMessage2 = new Message();
                mResultMessage2.what = 4;
                mResultMessage2.obj = strMessage;
                UserRegisterActivity.this.mHandler.sendMessage(mResultMessage2);
            }
            UserRegisterActivity.this.mHandler.sendEmptyMessage(3);
        }
    }
}
