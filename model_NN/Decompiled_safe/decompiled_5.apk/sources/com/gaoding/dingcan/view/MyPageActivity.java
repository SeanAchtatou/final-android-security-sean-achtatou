package com.gaoding.dingcan.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.gaoding.dingcan.R;
import com.gaoding.dingcan.database.DatabaseHelper;
import com.gaoding.dingcan.database.controller.UserInfo;
import com.gaoding.dingcan.util.Utils;

public class MyPageActivity extends Activity implements View.OnClickListener {
    private static final int DISMISS_PROGRESS_DIALOG = 3;
    private static final int REFRESH_VIEW = 1;
    private static final int SHOW_PROGRESS_DIALOG = 2;
    private static final int SHOW_TOAST_MESSAGE = 4;
    private Button btLogout;
    private ImageView btnBack;
    private UserInfo info;
    /* access modifiers changed from: private */
    public ProgressDialog mProgressDialog;
    private Handler myHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                default:
                    return;
                case 2:
                    try {
                        if (MyPageActivity.this.mProgressDialog != null) {
                            if (MyPageActivity.this.mProgressDialog.isShowing()) {
                                MyPageActivity.this.mProgressDialog.dismiss();
                            }
                            MyPageActivity.this.mProgressDialog = null;
                        }
                        MyPageActivity.this.mProgressDialog = Utils.getProgressDialog(MyPageActivity.this, (String) msg.obj);
                        MyPageActivity.this.mProgressDialog.show();
                        return;
                    } catch (Exception e) {
                        e.printStackTrace();
                        return;
                    }
                case 3:
                    try {
                        if (MyPageActivity.this.mProgressDialog != null && MyPageActivity.this.mProgressDialog.isShowing()) {
                            MyPageActivity.this.mProgressDialog.dismiss();
                            return;
                        }
                        return;
                    } catch (Exception e2) {
                        e2.printStackTrace();
                        return;
                    }
                case 4:
                    try {
                        Utils.showToast(MyPageActivity.this, msg.obj.toString());
                        return;
                    } catch (Exception e3) {
                        e3.printStackTrace();
                        return;
                    }
            }
        }
    };
    private Resources resources;
    private TextView tvAllOrder;
    private TextView tvGuide;
    private TextView tvInfo;
    private TextView tvPassword;
    private TextView tvTodayOrder;
    private TextView tvWeb;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_mypage);
        this.resources = getResources();
        Bundle extras = getIntent().getExtras();
        initViews();
        this.info = new UserInfo(this);
        this.info.query();
    }

    private void initViews() {
        this.btnBack = (ImageView) findViewById(R.id.btnBack);
        this.btnBack.setOnClickListener(this);
        this.tvInfo = (TextView) findViewById(R.id.mypage_info);
        this.tvPassword = (TextView) findViewById(R.id.mypage_password);
        this.tvTodayOrder = (TextView) findViewById(R.id.mypage_order_today);
        this.tvAllOrder = (TextView) findViewById(R.id.mypage_order_all);
        this.tvWeb = (TextView) findViewById(R.id.mypage_web);
        this.tvGuide = (TextView) findViewById(R.id.mypage_guide);
        this.btLogout = (Button) findViewById(R.id.userLogout);
        this.btLogout.setOnClickListener(this);
        this.tvInfo.setOnClickListener(this);
        this.tvPassword.setOnClickListener(this);
        this.tvTodayOrder.setOnClickListener(this);
        this.tvAllOrder.setOnClickListener(this);
        this.tvWeb.setOnClickListener(this);
        this.tvGuide.setOnClickListener(this);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnBack:
                finish();
                return;
            case R.id.userLogout:
                new DatabaseHelper(this).deleteDatabase();
                setResult(-1);
                finish();
                return;
            case R.id.mypage_info:
                Intent intent = new Intent();
                intent.setClass(this, UserInfoActivity.class);
                startActivity(intent);
                return;
            case R.id.mypage_password:
                Intent intent2 = new Intent();
                intent2.setClass(this, ChangePasswordActivity.class);
                startActivity(intent2);
                return;
            case R.id.mypage_order_today:
                Intent intent3 = new Intent();
                intent3.setClass(this, OrderActivity.class);
                intent3.putExtra("type", OrderActivity.ORDER_TYPE_TODAY);
                startActivity(intent3);
                return;
            case R.id.mypage_order_all:
                Intent intent4 = new Intent();
                intent4.setClass(this, OrderActivity.class);
                intent4.putExtra("type", OrderActivity.ORDER_TYPE_ALL);
                startActivity(intent4);
                return;
            case R.id.mypage_web:
                startActivity(new Intent(this, ProtocolActivity.class));
                return;
            case R.id.mypage_guide:
                startActivity(new Intent(this, AppGuide.class));
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.info.close();
        super.onDestroy();
    }
}
