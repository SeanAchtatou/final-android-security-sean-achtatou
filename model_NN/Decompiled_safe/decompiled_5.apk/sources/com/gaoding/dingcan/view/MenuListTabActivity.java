package com.gaoding.dingcan.view;

import android.app.TabActivity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TextView;
import com.gaoding.dingcan.R;

public class MenuListTabActivity extends TabActivity implements View.OnClickListener {
    private String RestaurantId;
    private String UnitName;
    private Button btShop;
    private ImageView btnBack;
    private TabHost m_tabHost;
    private Resources resources;
    private String strResName = "";

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_menu_tabs);
        initViews();
        this.m_tabHost = getTabHost();
        this.resources = getResources();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            this.RestaurantId = bundle.getString("RestaurantId");
            this.strResName = bundle.getString("RestaurantName");
            this.UnitName = bundle.getString("UnitName");
        }
        addOneTab();
        addTwoTab();
    }

    private void initViews() {
        this.btnBack = (ImageView) findViewById(R.id.btnBack);
        this.btnBack.setOnClickListener(this);
        this.btShop = (Button) findViewById(R.id.shopingCart);
        this.btShop.setOnClickListener(this);
    }

    public void addOneTab() {
        Intent intent = new Intent();
        intent.setClass(this, RestaurantInfoActivity.class);
        intent.putExtra("RestaurantId", this.RestaurantId);
        intent.putExtra("RestaurantName", this.strResName);
        createTab("餐厅信息", intent);
    }

    public void addTwoTab() {
        Intent intent = new Intent();
        intent.setClass(this, MenuListsActivity.class);
        intent.putExtra("RestaurantId", this.RestaurantId);
        intent.putExtra("UnitName", this.UnitName);
        createTab("点菜", intent);
    }

    private void createTab(String text, Intent intent) {
        this.m_tabHost.addTab(this.m_tabHost.newTabSpec(text).setIndicator(createTabView(text)).setContent(intent));
    }

    private View createTabView(String text) {
        View view = LayoutInflater.from(this).inflate((int) R.layout.tab_indicator, (ViewGroup) null);
        ((TextView) view.findViewById(R.id.tv_tab)).setText(text);
        return view;
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnBack:
                finish();
                return;
            case R.id.shopingCart:
                Intent intent = new Intent();
                intent.setClass(this, ShoppingListActivity.class);
                startActivity(intent);
                return;
            default:
                return;
        }
    }
}
