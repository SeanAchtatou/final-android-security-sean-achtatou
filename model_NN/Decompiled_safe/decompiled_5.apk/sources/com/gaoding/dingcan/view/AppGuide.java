package com.gaoding.dingcan.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ViewFlipper;
import com.gaoding.dingcan.R;

public class AppGuide extends Activity implements GestureDetector.OnGestureListener {
    int FLING_MIN_DISTANCE = 80;
    int FLING_MIN_VELOCITY = 200;
    private boolean autoLogin = false;
    private GestureDetector gestureDetector;
    private int[] guideid = {R.drawable.img1, R.drawable.img2, R.drawable.img3, R.drawable.img4};
    private LinearLayout indicator;
    IndicatorView mIndicatorView;
    private int position = 0;
    private ViewFlipper viewFlipper;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_app_guide);
        initView();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            this.autoLogin = bundle.getBoolean("autoLogin");
        }
    }

    private void initView() {
        this.viewFlipper = (ViewFlipper) findViewById(R.id.viewFlipper);
        this.indicator = (LinearLayout) findViewById(R.id.indicator);
        this.gestureDetector = new GestureDetector(this);
        for (int i = 0; i < this.guideid.length; i++) {
            IndicatorView iv = new IndicatorView(this);
            FlipperView fv = new FlipperView(this, this.guideid[i]);
            if (i == 0) {
                iv.choose();
            }
            this.indicator.addView(iv);
            this.viewFlipper.addView(fv);
        }
    }

    private void setIndicatorChoose() {
        for (int i = 0; i < this.guideid.length; i++) {
            this.mIndicatorView = (IndicatorView) this.indicator.getChildAt(i);
            this.mIndicatorView.normal();
        }
        this.mIndicatorView = (IndicatorView) this.indicator.getChildAt(this.position);
        this.mIndicatorView.choose();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    public boolean onTouchEvent(MotionEvent event) {
        return this.gestureDetector.onTouchEvent(event);
    }

    public boolean onDown(MotionEvent e) {
        return false;
    }

    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        if (e1.getX() - e2.getX() > ((float) this.FLING_MIN_DISTANCE)) {
            this.viewFlipper.setInAnimation(AnimationUtils.loadAnimation(this, R.anim.push_left_in));
            this.viewFlipper.setOutAnimation(AnimationUtils.loadAnimation(this, R.anim.push_left_out));
            if (this.position < this.guideid.length - 1) {
                this.viewFlipper.showNext();
                this.position++;
                setIndicatorChoose();
            } else {
                if (this.autoLogin) {
                    startActivity(new Intent(this, UserLoginActivity.class));
                }
                finish();
            }
        }
        if (e2.getX() - e1.getX() <= ((float) this.FLING_MIN_DISTANCE)) {
            return false;
        }
        this.viewFlipper.setInAnimation(AnimationUtils.loadAnimation(this, R.anim.push_right_in));
        this.viewFlipper.setOutAnimation(AnimationUtils.loadAnimation(this, R.anim.push_right_out));
        if (this.position <= 0) {
            return false;
        }
        this.viewFlipper.showPrevious();
        this.position--;
        setIndicatorChoose();
        return false;
    }

    public void onLongPress(MotionEvent e) {
    }

    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

    public void onShowPress(MotionEvent e) {
    }

    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    private class IndicatorView extends ImageView {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);

        public IndicatorView(Context context) {
            super(context);
            this.layoutParams.setMargins(20, 0, 0, 0);
            setLayoutParams(this.layoutParams);
            normal();
        }

        public void normal() {
            setBackgroundResource(R.drawable.app_tips_point_normal);
        }

        public void choose() {
            setBackgroundResource(R.drawable.app_tips_point_choose);
        }
    }

    private class FlipperView extends ImageView {
        public FlipperView(Context context, int resId) {
            super(context);
            setImageResource(resId);
            setScaleType(ImageView.ScaleType.FIT_XY);
        }
    }
}
