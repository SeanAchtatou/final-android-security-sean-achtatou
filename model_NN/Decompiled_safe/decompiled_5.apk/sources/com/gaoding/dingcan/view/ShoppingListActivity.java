package com.gaoding.dingcan.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.gaoding.dingcan.R;
import com.gaoding.dingcan.database.bean.ShopBean;
import com.gaoding.dingcan.database.controller.Shop;
import com.gaoding.dingcan.database.controller.ShopCart;
import com.gaoding.dingcan.database.controller.UserInfo;
import com.gaoding.dingcan.http.controller.GetRestaurantInfo;
import com.gaoding.dingcan.http.controller.SendShopping;
import com.gaoding.dingcan.util.LogUtils;
import com.gaoding.dingcan.util.Utils;

public class ShoppingListActivity extends Activity implements View.OnClickListener {
    private static final int DISMISS_PROGRESS_DIALOG = 3;
    private static final int REFRESH_VIEW = 1;
    private static final int SHOW_PROGRESS_DIALOG = 2;
    private static final int SHOW_TOAST_MESSAGE = 4;
    private static final int SUCCESS = 5;
    /* access modifiers changed from: private */
    public String UnitName;
    private Button btSend;
    private ImageView btnBack;
    private EditText etAddress;
    private EditText etDesc;
    private EditText etPhone;
    private UserInfo info;
    /* access modifiers changed from: private */
    public MessageAdapter listAdapter;
    private ListView listView;
    /* access modifiers changed from: private */
    public ProgressDialog mProgressDialog;
    /* access modifiers changed from: private */
    public Handler myHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    try {
                        ShoppingListActivity.this.listAdapter.notifyDataSetChanged();
                        return;
                    } catch (Exception e) {
                        e.printStackTrace();
                        return;
                    }
                case 2:
                    try {
                        if (ShoppingListActivity.this.mProgressDialog != null) {
                            if (ShoppingListActivity.this.mProgressDialog.isShowing()) {
                                ShoppingListActivity.this.mProgressDialog.dismiss();
                            }
                            ShoppingListActivity.this.mProgressDialog = null;
                        }
                        ShoppingListActivity.this.mProgressDialog = Utils.getProgressDialog(ShoppingListActivity.this, (String) msg.obj);
                        ShoppingListActivity.this.mProgressDialog.show();
                        return;
                    } catch (Exception e2) {
                        e2.printStackTrace();
                        return;
                    }
                case 3:
                    try {
                        if (ShoppingListActivity.this.mProgressDialog != null && ShoppingListActivity.this.mProgressDialog.isShowing()) {
                            ShoppingListActivity.this.mProgressDialog.dismiss();
                        }
                        ShoppingListActivity.this.listAdapter.notifyDataSetChanged();
                        return;
                    } catch (Exception e3) {
                        e3.printStackTrace();
                        return;
                    }
                case 4:
                    try {
                        Utils.showToast(ShoppingListActivity.this, msg.obj.toString());
                        return;
                    } catch (Exception e4) {
                        e4.printStackTrace();
                        return;
                    }
                case 5:
                    Intent intent3 = new Intent();
                    intent3.setClass(ShoppingListActivity.this, OrderActivity.class);
                    intent3.putExtra("type", OrderActivity.ORDER_TYPE_TODAY);
                    ShoppingListActivity.this.startActivity(intent3);
                    ShoppingListActivity.this.finish();
                    return;
                default:
                    return;
            }
        }
    };
    private String oldAddress;
    /* access modifiers changed from: private */
    public Resources resources;
    /* access modifiers changed from: private */
    public Shop shop;
    /* access modifiers changed from: private */
    public ShopCart shopCart;
    /* access modifiers changed from: private */
    public String strAddress;
    /* access modifiers changed from: private */
    public String strDesc;
    /* access modifiers changed from: private */
    public String strPhone;
    /* access modifiers changed from: private */
    public String strRestaurantId;
    private String strStart;
    private TextView tvCount;
    private TextView tvMoney;
    private TextView tvStart;
    private TextView tvUnit;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LogUtils.logDebug("MenuListActivity");
        setContentView((int) R.layout.activity_shopping_list);
        this.resources = getResources();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            this.strRestaurantId = bundle.getString("RestaurantId");
            this.strStart = bundle.getString("StartPrice");
            this.UnitName = bundle.getString("UnitName");
        } else {
            ShopCart shopCart2 = new ShopCart(this);
            shopCart2.getFromDatabase();
            this.strRestaurantId = shopCart2.item.mRestaurantId;
            this.strStart = shopCart2.item.mStartPrice;
            this.UnitName = shopCart2.item.mUnitName;
        }
        initViews();
        this.info = new UserInfo(this);
        this.info.query();
        this.shop = new Shop(this);
        this.shop.query();
        this.shopCart = new ShopCart(this);
        this.shopCart.getFromDatabase();
        this.listAdapter = new MessageAdapter(this, null);
        this.listView.setAdapter((ListAdapter) this.listAdapter);
        this.etAddress.setText(this.info.Item.mAddress);
        this.oldAddress = this.info.Item.mAddress;
        this.etPhone.setText(this.info.Item.mPhone);
        this.tvStart.setText("￥" + this.strStart);
        this.tvUnit.setText(this.UnitName);
    }

    private void initViews() {
        this.btnBack = (ImageView) findViewById(R.id.btnBack);
        this.btnBack.setOnClickListener(this);
        this.listView = (ListView) findViewById(R.id.listview_menu);
        this.btSend = (Button) findViewById(R.id.shopingSend);
        this.btSend.setOnClickListener(this);
        this.etAddress = (EditText) findViewById(R.id.userAddress);
        this.etPhone = (EditText) findViewById(R.id.userPhone);
        this.etDesc = (EditText) findViewById(R.id.userDesc);
        this.tvMoney = (TextView) findViewById(R.id.totalMoney);
        this.tvStart = (TextView) findViewById(R.id.sendStart);
        this.tvCount = (TextView) findViewById(R.id.totalCount);
        this.tvUnit = (TextView) findViewById(R.id.userUnit);
        this.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                ShopBean bean = ShoppingListActivity.this.shop.list.get(arg2);
                Intent intent = new Intent();
                intent.setClass(ShoppingListActivity.this, ShoppingChangeActivity.class);
                intent.putExtra("MenuId", bean.mMenuId);
                intent.putExtra("MenuName", bean.mName);
                intent.putExtra("MenuCount", bean.mCount);
                intent.putExtra("MenuPrice", bean.mPrice);
                ShoppingListActivity.this.startActivity(intent);
            }
        });
    }

    private void sendShopping() {
        if (Utils.isNetWorkConnect(this)) {
            new SendThread(this, null).start();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        this.shop.query();
        this.myHandler.sendEmptyMessage(1);
        this.tvMoney.setText(getTotalMoney());
        this.tvCount.setText(getTotalCount());
        super.onResume();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnBack:
                finish();
                return;
            case R.id.shopingSend:
                if (this.shop.list.size() == 0) {
                    return;
                }
                if (!enoughtoPrice()) {
                    Utils.showToast(this, "未达到起送价");
                    return;
                }
                this.strAddress = this.etAddress.getText().toString();
                this.strPhone = this.etPhone.getText().toString();
                this.strDesc = this.etDesc.getText().toString();
                if (this.strDesc == null || this.strDesc.equals("")) {
                    this.strDesc = "尽快送达";
                }
                if (this.strAddress == null || this.strAddress.equals("")) {
                    Utils.showToast(this, "请填写地址");
                    return;
                }
                if (!this.oldAddress.equals(this.strAddress)) {
                    this.info.Item.mAddress = this.strAddress;
                    this.info.setToDatabase();
                    this.oldAddress = this.strAddress;
                }
                if (this.strPhone == null || this.strPhone.equals("")) {
                    Utils.showToast(this, "请填写电话");
                    return;
                } else {
                    sendShopping();
                    return;
                }
            default:
                return;
        }
    }

    private class MessageAdapter extends BaseAdapter {
        private MessageAdapter() {
        }

        /* synthetic */ MessageAdapter(ShoppingListActivity shoppingListActivity, MessageAdapter messageAdapter) {
            this();
        }

        public int getCount() {
            if (ShoppingListActivity.this.shop.list == null) {
                return 0;
            }
            return ShoppingListActivity.this.shop.list.size();
        }

        public Object getItem(int position) {
            return Integer.valueOf(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public void unregisterDataSetObserver(DataSetObserver observer) {
            if (observer != null) {
                super.unregisterDataSetObserver(observer);
            }
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            int mPosition = position;
            View view = ((LayoutInflater) ShoppingListActivity.this.getSystemService("layout_inflater")).inflate((int) R.layout.msg_item, (ViewGroup) null);
            try {
                HolderViewTopic holderView = new HolderViewTopic(ShoppingListActivity.this, null);
                holderView.mTvTitle = (TextView) view.findViewById(R.id.tv_item_title);
                try {
                    holderView.mTvTitle.setText(String.valueOf(String.valueOf(String.valueOf(String.valueOf("") + "￥" + ShoppingListActivity.this.shop.list.get(mPosition).mPrice) + "     " + ShoppingListActivity.this.shop.list.get(mPosition).mName) + "     x" + ShoppingListActivity.this.shop.list.get(mPosition).mCount) + "份");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            return view;
        }
    }

    private class HolderViewTopic {
        /* access modifiers changed from: private */
        public TextView mTvTitle;

        private HolderViewTopic() {
        }

        /* synthetic */ HolderViewTopic(ShoppingListActivity shoppingListActivity, HolderViewTopic holderViewTopic) {
            this();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.info.close();
        this.shop.close();
        this.shopCart.close();
        super.onDestroy();
    }

    private String getTotalMoney() {
        float result = 0.0f;
        try {
            for (ShopBean bean : this.shop.list) {
                result += Float.parseFloat(bean.mPrice) * ((float) Integer.parseInt(bean.mCount));
            }
        } catch (Exception e) {
        }
        return "￥" + result;
    }

    private String getTotalCount() {
        int result = 0;
        try {
            for (ShopBean bean : this.shop.list) {
                result += Integer.parseInt(bean.mCount);
            }
        } catch (Exception e) {
        }
        return new StringBuilder().append(result).toString();
    }

    private boolean enoughtoPrice() {
        float startPrice = Float.parseFloat(this.shopCart.item.mStartPrice);
        float result = 0.0f;
        try {
            for (ShopBean bean : this.shop.list) {
                result += Float.parseFloat(bean.mPrice) * ((float) Integer.parseInt(bean.mCount));
            }
        } catch (Exception e) {
        }
        if (result >= startPrice) {
            return true;
        }
        return false;
    }

    private class SendThread extends Thread {
        private SendThread() {
        }

        /* synthetic */ SendThread(ShoppingListActivity shoppingListActivity, SendThread sendThread) {
            this();
        }

        public void run() {
            super.run();
            Message msg = new Message();
            msg.what = 2;
            msg.obj = ShoppingListActivity.this.resources.getString(R.string.please_wait);
            ShoppingListActivity.this.myHandler.sendMessage(msg);
            String strResState = GetRestaurantInfo.RES_STATE_CLOSE;
            try {
                GetRestaurantInfo info = new GetRestaurantInfo(ShoppingListActivity.this);
                info.mResId = ShoppingListActivity.this.strRestaurantId;
                if (info.GetInfo()) {
                    strResState = info.item.mState;
                }
                if (strResState.equals(GetRestaurantInfo.RES_STATE_OPEN)) {
                    if (new SendShopping(ShoppingListActivity.this).send(ShoppingListActivity.this.strRestaurantId, String.valueOf(ShoppingListActivity.this.UnitName) + ShoppingListActivity.this.strAddress, ShoppingListActivity.this.strPhone, ShoppingListActivity.this.strDesc)) {
                        Message msg2 = new Message();
                        msg2.what = 4;
                        msg2.obj = "下单成功";
                        ShoppingListActivity.this.myHandler.sendMessage(msg2);
                        ShoppingListActivity.this.shopCart.item.mRestaurantId = "";
                        ShoppingListActivity.this.shopCart.item.mStartPrice = GetRestaurantInfo.RES_STATE_CLOSE;
                        ShoppingListActivity.this.shopCart.setToDatabase();
                        ShoppingListActivity.this.myHandler.sendEmptyMessage(5);
                    }
                } else {
                    Message msg22 = new Message();
                    msg22.what = 4;
                    msg22.obj = "餐厅正在休息，不能点菜";
                    ShoppingListActivity.this.myHandler.sendMessage(msg22);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            ShoppingListActivity.this.myHandler.sendEmptyMessage(3);
        }
    }
}
