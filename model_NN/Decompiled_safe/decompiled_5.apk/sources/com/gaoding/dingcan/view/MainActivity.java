package com.gaoding.dingcan.view;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import com.gaoding.dingcan.R;
import com.gaoding.dingcan.database.bean.CityBean;
import com.gaoding.dingcan.database.controller.City;
import com.gaoding.dingcan.http.controller.GetRestaurantInfo;
import com.gaoding.dingcan.util.LogUtils;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_main);
        new TestThread(this, null).start();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    private class TestThread extends Thread {
        private TestThread() {
        }

        /* synthetic */ TestThread(MainActivity mainActivity, TestThread testThread) {
            this();
        }

        public void run() {
            super.run();
            City city = new City(MainActivity.this);
            city.getFromDatabase();
            List<CityBean> list = new ArrayList<>();
            CityBean bean = new CityBean();
            bean.mId = GetRestaurantInfo.RES_STATE_OPEN;
            bean.mName = GetRestaurantInfo.RES_STATE_OPEN;
            list.add(bean);
            city.syncToDatabase(list);
            LogUtils.logDebug("city list size =" + city.list.size());
            city.syncToDatabase(list);
            LogUtils.logDebug("city list size =" + city.list.size());
            CityBean bean2 = new CityBean();
            bean.mId = "2";
            bean.mName = "2";
            list.add(bean2);
            city.syncToDatabase(list);
            LogUtils.logDebug("city list size =" + city.list.size());
            List<CityBean> list2 = new ArrayList<>();
            list2.add(bean);
            city.syncToDatabase(list2);
            LogUtils.logDebug("city list size =" + city.list.size());
        }
    }
}
