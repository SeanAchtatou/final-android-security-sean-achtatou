package com.gaoding.dingcan;

public final class R {

    public static final class anim {
        public static final int progress_round = 2130968576;
        public static final int push_left_in = 2130968577;
        public static final int push_left_out = 2130968578;
        public static final int push_right_in = 2130968579;
        public static final int push_right_out = 2130968580;
    }

    public static final class attr {
    }

    public static final class color {
        public static final int LightSteelBlue = 2131099649;
        public static final int RoyalBlue = 2131099648;
        public static final int black = 2131099657;
        public static final int dark_gray = 2131099654;
        public static final int dialog_divider = 2131099658;
        public static final int gray = 2131099663;
        public static final int gray_color = 2131099664;
        public static final int red = 2131099655;
        public static final int solid_blue = 2131099651;
        public static final int solid_green = 2131099652;
        public static final int solid_red = 2131099650;
        public static final int solid_yellow = 2131099653;
        public static final int user_nickname_title = 2131099661;
        public static final int user_normal_title = 2131099662;
        public static final int user_set_value_color = 2131099660;
        public static final int user_title_color = 2131099659;
        public static final int white = 2131099656;
    }

    public static final class dimen {
        public static final int activity_horizontal_margin = 2131165184;
        public static final int activity_vertical_margin = 2131165185;
    }

    public static final class drawable {
        public static final int abs__spinner_ab_focused_holo_light = 2130837504;
        public static final int abs__spinner_ab_pressed_holo_light = 2130837505;
        public static final int app_tips_point_choose = 2130837506;
        public static final int app_tips_point_normal = 2130837507;
        public static final int back1 = 2130837508;
        public static final int back2 = 2130837509;
        public static final int bg_common_popwindow1 = 2130837510;
        public static final int button_back_selector = 2130837511;
        public static final int button_default_green = 2130837512;
        public static final int button_default_green_press = 2130837513;
        public static final int button_default_green_selector = 2130837514;
        public static final int button_default_orange = 2130837515;
        public static final int button_default_orange_press = 2130837516;
        public static final int button_default_orange_selector = 2130837517;
        public static final int button_normal = 2130837518;
        public static final int button_pressed = 2130837519;
        public static final int close = 2130837520;
        public static final int default_bg_white = 2130837521;
        public static final int default_bg_white_bg_repeat = 2130837522;
        public static final int edittext_bottom_image = 2130837523;
        public static final int edittext_mid_image = 2130837524;
        public static final int edittext_top_image = 2130837525;
        public static final int gaoding = 2130837526;
        public static final int hot = 2130837527;
        public static final int ic_launcher = 2130837528;
        public static final int img1 = 2130837529;
        public static final int img2 = 2130837530;
        public static final int img3 = 2130837531;
        public static final int img4 = 2130837532;
        public static final int line_v = 2130837533;
        public static final int list_divider = 2130837534;
        public static final int navi_bar_back_bg_selector = 2130837535;
        public static final int navi_bar_button_back_press = 2130837536;
        public static final int navi_bar_button_next_normal = 2130837537;
        public static final int navi_bar_button_next_press = 2130837538;
        public static final int navi_bar_button_normal = 2130837539;
        public static final int nologo = 2130837540;
        public static final int open = 2130837541;
        public static final int reduce = 2130837542;
        public static final int right_arrow_normal = 2130837543;
        public static final int right_arrow_press = 2130837544;
        public static final int rounded_corner_frame = 2130837545;
        public static final int sharemore = 2130837546;
        public static final int spinner_selector = 2130837547;
        public static final int star0 = 2130837548;
        public static final int star1 = 2130837549;
        public static final int star10 = 2130837550;
        public static final int star11 = 2130837551;
        public static final int star12 = 2130837552;
        public static final int star13 = 2130837553;
        public static final int star2 = 2130837554;
        public static final int star3 = 2130837555;
        public static final int star4 = 2130837556;
        public static final int star5 = 2130837557;
        public static final int star6 = 2130837558;
        public static final int star7 = 2130837559;
        public static final int star8 = 2130837560;
        public static final int star9 = 2130837561;
        public static final int tab_selector = 2130837562;
        public static final int title_bar_bg = 2130837563;
        public static final int title_bar_bg_old = 2130837564;
    }

    public static final class id {
        public static final int action_settings = 2131427441;
        public static final int btOrderCancel = 2131427360;
        public static final int btnBack = 2131427331;
        public static final int chg_psw_confirm = 2131427377;
        public static final int chg_psw_new = 2131427376;
        public static final int chg_psw_old = 2131427375;
        public static final int chg_psw_save = 2131427378;
        public static final int ib_add_menu = 2131427428;
        public static final int ib_cut_menu = 2131427429;
        public static final int id_tv_loadingmsg = 2131427423;
        public static final int indicator = 2131427329;
        public static final int linearlayout_progress = 2131427334;
        public static final int listview_area = 2131427333;
        public static final int listview_menu = 2131427350;
        public static final int listview_order = 2131427373;
        public static final int ll_1 = 2131427424;
        public static final int ll_2 = 2131427425;
        public static final int loadingImageView = 2131427422;
        public static final int msgGetMenuRefresh = 2131427380;
        public static final int msgGetMeunTitle = 2131427337;
        public static final int msgGetinfoRefresh = 2131427332;
        public static final int msgGetinfoTitle = 2131427330;
        public static final int msgMyPageTitle = 2131427352;
        public static final int mypage_guide = 2131427359;
        public static final int mypage_info = 2131427354;
        public static final int mypage_order_all = 2131427357;
        public static final int mypage_order_today = 2131427356;
        public static final int mypage_password = 2131427355;
        public static final int mypage_web = 2131427358;
        public static final int order_active = 2131427372;
        public static final int order_address = 2131427364;
        public static final int order_cactivtyId = 2131427369;
        public static final int order_desc = 2131427366;
        public static final int order_info = 2131427361;
        public static final int order_num = 2131427362;
        public static final int order_phone = 2131427365;
        public static final int order_price = 2131427367;
        public static final int order_state = 2131427370;
        public static final int order_time = 2131427363;
        public static final int order_total = 2131427368;
        public static final int order_type = 2131427371;
        public static final int protocol_tv = 2131427434;
        public static final int psd_old = 2131427374;
        public static final int res_address = 2131427345;
        public static final int res_areato = 2131427346;
        public static final int res_desc = 2131427348;
        public static final int res_name = 2131427340;
        public static final int res_notice = 2131427347;
        public static final int res_open = 2131427341;
        public static final int res_send_time = 2131427342;
        public static final int res_start = 2131427343;
        public static final int res_state = 2131427344;
        public static final int res_type = 2131427349;
        public static final int restaurant_info = 2131427339;
        public static final int sendStart = 2131427399;
        public static final int shopTitle = 2131427379;
        public static final int shop_address = 2131427392;
        public static final int shop_cancel = 2131427387;
        public static final int shop_count = 2131427385;
        public static final int shop_delete = 2131427388;
        public static final int shop_desc = 2131427396;
        public static final int shop_money = 2131427400;
        public static final int shop_name = 2131427383;
        public static final int shop_ok = 2131427386;
        public static final int shop_phone = 2131427394;
        public static final int shop_price = 2131427384;
        public static final int shop_unit = 2131427390;
        public static final int shop_warning = 2131427382;
        public static final int shopingCart = 2131427338;
        public static final int shopingSend = 2131427389;
        public static final int start_money = 2131427398;
        public static final int totalCount = 2131427402;
        public static final int totalMoney = 2131427401;
        public static final int tv_item_address = 2131427436;
        public static final int tv_item_count = 2131427427;
        public static final int tv_item_hot = 2131427438;
        public static final int tv_item_image = 2131427435;
        public static final int tv_item_money = 2131427433;
        public static final int tv_item_name = 2131427431;
        public static final int tv_item_num = 2131427430;
        public static final int tv_item_open = 2131427439;
        public static final int tv_item_speed = 2131427437;
        public static final int tv_item_time = 2131427432;
        public static final int tv_item_title = 2131427426;
        public static final int tv_money = 2131427351;
        public static final int tv_no_info = 2131427335;
        public static final int tv_tab = 2131427440;
        public static final int userAddress = 2131427393;
        public static final int userConfirm = 2131427403;
        public static final int userDesc = 2131427397;
        public static final int userLogout = 2131427353;
        public static final int userPage = 2131427336;
        public static final int userPhone = 2131427395;
        public static final int userUnit = 2131427391;
        public static final int userloginAccount = 2131427406;
        public static final int userloginBack = 2131427405;
        public static final int userloginButton = 2131427408;
        public static final int userloginForgot = 2131427411;
        public static final int userloginInputbox = 2131427381;
        public static final int userloginLogin = 2131427410;
        public static final int userloginPassword = 2131427407;
        public static final int userloginRegister = 2131427409;
        public static final int userloginTitle = 2131427404;
        public static final int userregisterAgree = 2131427420;
        public static final int userregisterBack = 2131427413;
        public static final int userregisterInputbox = 2131427414;
        public static final int userregisterRegister = 2131427419;
        public static final int userregisterStatement = 2131427421;
        public static final int userregisterTitle = 2131427412;
        public static final int userregister_account = 2131427415;
        public static final int userregister_email = 2131427417;
        public static final int userregister_password = 2131427416;
        public static final int userregister_phone = 2131427418;
        public static final int viewFlipper = 2131427328;
    }

    public static final class layout {
        public static final int activity_app_guide = 2130903040;
        public static final int activity_arealist = 2130903041;
        public static final int activity_areaunitlist = 2130903042;
        public static final int activity_main = 2130903043;
        public static final int activity_menu = 2130903044;
        public static final int activity_menu_lists = 2130903045;
        public static final int activity_menu_tabs = 2130903046;
        public static final int activity_mypage = 2130903047;
        public static final int activity_order_info = 2130903048;
        public static final int activity_order_list = 2130903049;
        public static final int activity_password = 2130903050;
        public static final int activity_res_info = 2130903051;
        public static final int activity_restaurant = 2130903052;
        public static final int activity_shopping = 2130903053;
        public static final int activity_shopping_change = 2130903054;
        public static final int activity_shopping_list = 2130903055;
        public static final int activity_splash = 2130903056;
        public static final int activity_unitlist = 2130903057;
        public static final int activity_user_info = 2130903058;
        public static final int activity_user_login = 2130903059;
        public static final int activity_user_register = 2130903060;
        public static final int dialog_custom_progress = 2130903061;
        public static final int menu_item = 2130903062;
        public static final int msg_item = 2130903063;
        public static final int order_item = 2130903064;
        public static final int protocol_activity = 2130903065;
        public static final int restaurant_item = 2130903066;
        public static final int spinner = 2130903067;
        public static final int tab_indicator = 2130903068;
    }

    public static final class menu {
        public static final int main = 2131361792;
    }

    public static final class raw {
        public static final int protocol = 2131034112;
    }

    public static final class string {
        public static final int account = 2131230729;
        public static final int action_settings = 2131230721;
        public static final int address = 2131230739;
        public static final int app_name = 2131230720;
        public static final int area = 2131230747;
        public static final int back = 2131230735;
        public static final int cancel = 2131230724;
        public static final int city = 2131230749;
        public static final int complete = 2131230763;
        public static final int confirm = 2131230723;
        public static final int count = 2131230726;
        public static final int delete = 2131230725;
        public static final int desc = 2131230738;
        public static final int email = 2131230736;
        public static final int error_code_message_no_response = 2131230777;
        public static final int error_code_message_unknown = 2131230776;
        public static final int error_input = 2131230778;
        public static final int fail = 2131230762;
        public static final int forgetpsw = 2131230734;
        public static final int hello_world = 2131230722;
        public static final int load = 2131230744;
        public static final int login = 2131230731;
        public static final int logout = 2131230732;
        public static final int menu = 2131230752;
        public static final int mypage = 2131230746;
        public static final int name = 2131230727;
        public static final int no_network_message = 2131230775;
        public static final int none = 2131230745;
        public static final int order = 2131230754;
        public static final int password = 2131230730;
        public static final int phone = 2131230737;
        public static final int please_check_account = 2131230758;
        public static final int please_check_email = 2131230757;
        public static final int please_check_password = 2131230759;
        public static final int please_check_phone = 2131230756;
        public static final int please_input_complete = 2131230755;
        public static final int please_wait = 2131230760;
        public static final int price = 2131230728;
        public static final int refresh = 2131230741;
        public static final int refreshing = 2131230742;
        public static final int register = 2131230733;
        public static final int res_address = 2131230767;
        public static final int res_area = 2131230768;
        public static final int res_desc = 2131230772;
        public static final int res_name = 2131230764;
        public static final int res_notice = 2131230769;
        public static final int res_open = 2131230765;
        public static final int res_send_time = 2131230766;
        public static final int res_start = 2131230771;
        public static final int res_state = 2131230770;
        public static final int restaurant = 2131230750;
        public static final int send = 2131230743;
        public static final int shopping = 2131230753;
        public static final int success = 2131230761;
        public static final int total_money = 2131230740;
        public static final int type = 2131230751;
        public static final int type_all = 2131230773;
        public static final int unit = 2131230748;
        public static final int warning_shoping = 2131230774;
    }

    public static final class style {
        public static final int AppBaseTheme = 2131296256;
        public static final int AppTheme = 2131296257;
        public static final int CustomDialog = 2131296258;
        public static final int CustomProgressDialog = 2131296259;
    }
}
