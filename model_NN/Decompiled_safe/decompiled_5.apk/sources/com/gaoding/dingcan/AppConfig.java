package com.gaoding.dingcan;

import com.gaoding.dingcan.database.DataStore;

public class AppConfig {
    public static String ALL_ORDER_URL = "http://admin.gaoding100.com/JsonHandler/GetAllOrder_ByMemberId.ashx";
    public static String AREA_URL = "http://admin.gaoding100.com/JsonHandler/GetArea_ByCityId.ashx";
    public static String CANCEL_ORDER_URL = "http://admin.gaoding100.com/JsonHandler/CancelOrder_ByOrderId.ashx";
    public static String CHANGE_PSW_URL = "http://admin.gaoding100.com/JsonHandler/Member_ChangePassword.ashx";
    public static String CITY_URL = "http://admin.gaoding100.com/JsonHandler/GetCity_All.ashx";
    public static final String HTTP_BASE_URL = "http://admin.gaoding100.com/JsonHandler/";
    public static String LOGIN_URL = "http://admin.gaoding100.com/JsonHandler/Member_Login.ashx";
    public static final int MATCHING_LENGHT = 1900;
    public static String MENUTYPE_URL = "http://admin.gaoding100.com/JsonHandler/GetRestaurantMenuTypeByRestaurantId.ashx";
    public static String MENU_URL = "http://admin.gaoding100.com/JsonHandler/GetRestaurantMenu_ByMenuType.ashx";
    public static String MSG_STATUS_MARK = "mark";
    public static String MSG_STATUS_NEW = DataStore.AreaTable.AREA_NEW;
    public static String MSG_STATUS_OPEN = "open";
    public static String ORDER_DETAIL_URL = "http://admin.gaoding100.com/JsonHandler/GetOrderMenu_ByOrderId.ashx";
    public static String REGISTER_URL = "http://admin.gaoding100.com/JsonHandler/Member_Register.ashx";
    public static String RESTAURANT_URL = "http://admin.gaoding100.com/JsonHandler/GetRestaurantInfo_ByAreaUnit.ashx";
    public static String R_DETAIL_URL = "http://admin.gaoding100.com/JsonHandler/GetRestaurantDetails_ByRestaurantId.ashx";
    public static String SEND_ORDER_URL = "http://admin.gaoding100.com/JsonHandler/Member_SendOrder.ashx";
    public static final String SHARED_NOTIFICATION_BOOLEAN = "BOOLEAN_KEY";
    public static final String SHARED_NOTIFICATION_ID = "ID_KEY";
    public static final int SPLASH_DISPLAY_LENGHT = 1300;
    public static final String STORE_CACHE_PATH = "/gaoding/";
    public static String TODAY_ORDER_URL = "http://admin.gaoding100.com/JsonHandler/GetTodayOrder_ByMemberId.ashx";
    public static String UNIT_URL = "http://admin.gaoding100.com/JsonHandler/GetAreaUnitByAreaId.ashx";
    public static String VERSION_URL = "http://admin.gaoding100.com/JsonHandler/Version.ashx";
}
