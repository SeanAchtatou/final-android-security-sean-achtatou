package com.ggdroid.ninjarampage;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import java.io.File;

public class NinjaRampage extends Activity {
    String game2Name = "ninjarampage";
    String gameLoad = "Ninja Rampage";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.main);
        if (!getPackageName().equals("com.ggdroid." + this.game2Name)) {
            this.game2Name = null;
            System.exit(0);
        }
        File engine_dir = new File("/data/data/com.ggdroid.engine/");
        if (new File("/data/data/com.ggdroid.enginepro/").exists()) {
            Intent intent = new Intent("android.intent.action.MAIN");
            intent.setComponent(new ComponentName("com.ggdroid.enginepro", "com.ggdroid.enginepro.splash.Splash"));
            Bundle bundle = new Bundle();
            bundle.putString("gameName", this.gameLoad);
            intent.putExtras(bundle);
            startActivity(intent);
            finish();
            System.exit(0);
        } else if (!engine_dir.exists()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("GG Engine Plugin was Not Found! Hit OK to attempt to download it from the Market.");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    Intent i = new Intent("android.intent.action.VIEW");
                    i.setData(Uri.parse("market://details?id=com.ggdroid.engine"));
                    NinjaRampage.this.startActivity(i);
                    NinjaRampage.this.finish();
                    System.exit(0);
                }
            });
            AlertDialog dialog = builder.create();
            dialog.setCancelable(false);
            dialog.show();
        } else {
            Intent intent2 = new Intent("android.intent.action.MAIN");
            intent2.setComponent(new ComponentName("com.ggdroid.engine", "com.ggdroid.engine.splash.Splash"));
            Bundle bundle2 = new Bundle();
            bundle2.putString("gameName", this.gameLoad);
            intent2.putExtras(bundle2);
            startActivity(intent2);
            finish();
            System.exit(0);
        }
    }
}
