package com.google.android.apps.analytics;

class Referrer {
    private final String referrer;
    private final long timeStamp;

    Referrer(String str, long j) {
        this.referrer = str;
        this.timeStamp = j;
    }

    /* access modifiers changed from: package-private */
    public String getReferrerString() {
        return this.referrer;
    }

    /* access modifiers changed from: package-private */
    public long getTimeStamp() {
        return this.timeStamp;
    }
}
