package com.google.android.apps.analytics;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import java.util.HashSet;
import java.util.Map;

class PersistentHitStore implements HitStore {
    private static final String ACCOUNT_ID = "account_id";
    private static final String ACTION = "action";
    private static final String CATEGORY = "category";
    /* access modifiers changed from: private */
    public static final String CREATE_CUSTOM_VARIABLES_TABLE = ("CREATE TABLE custom_variables (" + String.format(" '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,", CUSTOMVAR_ID) + String.format(" '%s' INTEGER NOT NULL,", EVENT_ID) + String.format(" '%s' INTEGER NOT NULL,", CUSTOMVAR_INDEX) + String.format(" '%s' CHAR(64) NOT NULL,", CUSTOMVAR_NAME) + String.format(" '%s' CHAR(64) NOT NULL,", CUSTOMVAR_VALUE) + String.format(" '%s' INTEGER NOT NULL);", CUSTOMVAR_SCOPE));
    /* access modifiers changed from: private */
    public static final String CREATE_CUSTOM_VAR_CACHE_TABLE = ("CREATE TABLE IF NOT EXISTS custom_var_cache (" + String.format(" '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,", CUSTOMVAR_ID) + String.format(" '%s' INTEGER NOT NULL,", EVENT_ID) + String.format(" '%s' INTEGER NOT NULL,", CUSTOMVAR_INDEX) + String.format(" '%s' CHAR(64) NOT NULL,", CUSTOMVAR_NAME) + String.format(" '%s' CHAR(64) NOT NULL,", CUSTOMVAR_VALUE) + String.format(" '%s' INTEGER NOT NULL);", CUSTOMVAR_SCOPE));
    /* access modifiers changed from: private */
    public static final String CREATE_EVENTS_TABLE = ("CREATE TABLE events (" + String.format(" '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,", EVENT_ID) + String.format(" '%s' INTEGER NOT NULL,", USER_ID) + String.format(" '%s' CHAR(256) NOT NULL,", ACCOUNT_ID) + String.format(" '%s' INTEGER NOT NULL,", RANDOM_VAL) + String.format(" '%s' INTEGER NOT NULL,", TIMESTAMP_FIRST) + String.format(" '%s' INTEGER NOT NULL,", TIMESTAMP_PREVIOUS) + String.format(" '%s' INTEGER NOT NULL,", TIMESTAMP_CURRENT) + String.format(" '%s' INTEGER NOT NULL,", VISITS) + String.format(" '%s' CHAR(256) NOT NULL,", CATEGORY) + String.format(" '%s' CHAR(256) NOT NULL,", ACTION) + String.format(" '%s' CHAR(256), ", LABEL) + String.format(" '%s' INTEGER,", VALUE) + String.format(" '%s' INTEGER,", SCREEN_WIDTH) + String.format(" '%s' INTEGER);", SCREEN_HEIGHT));
    /* access modifiers changed from: private */
    public static final String CREATE_HITS_TABLE = ("CREATE TABLE IF NOT EXISTS hits (" + String.format(" '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,", HIT_ID) + String.format(" '%s' TEXT NOT NULL,", HIT_STRING) + String.format(" '%s' INTEGER NOT NULL);", HIT_TIMESTAMP));
    private static final String CREATE_INSTALL_REFERRER_TABLE = "CREATE TABLE install_referrer (referrer TEXT PRIMARY KEY NOT NULL);";
    /* access modifiers changed from: private */
    public static final String CREATE_ITEM_EVENTS_TABLE = ("CREATE TABLE item_events (" + String.format(" '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,", ITEM_ID) + String.format(" '%s' INTEGER NOT NULL,", EVENT_ID) + String.format(" '%s' TEXT NOT NULL,", ORDER_ID) + String.format(" '%s' TEXT NOT NULL,", ITEM_SKU) + String.format(" '%s' TEXT,", ITEM_NAME) + String.format(" '%s' TEXT,", ITEM_CATEGORY) + String.format(" '%s' TEXT NOT NULL,", ITEM_PRICE) + String.format(" '%s' TEXT NOT NULL);", ITEM_COUNT));
    private static final String CREATE_REFERRER_TABLE = "CREATE TABLE IF NOT EXISTS referrer (referrer TEXT PRIMARY KEY NOT NULL,timestamp_referrer INTEGER NOT NULL);";
    /* access modifiers changed from: private */
    public static final String CREATE_SESSION_TABLE = ("CREATE TABLE IF NOT EXISTS session (" + String.format(" '%s' INTEGER PRIMARY KEY,", TIMESTAMP_FIRST) + String.format(" '%s' INTEGER NOT NULL,", TIMESTAMP_PREVIOUS) + String.format(" '%s' INTEGER NOT NULL,", TIMESTAMP_CURRENT) + String.format(" '%s' INTEGER NOT NULL,", VISITS) + String.format(" '%s' INTEGER NOT NULL);", STORE_ID));
    /* access modifiers changed from: private */
    public static final String CREATE_TRANSACTION_EVENTS_TABLE = ("CREATE TABLE transaction_events (" + String.format(" '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,", TRANSACTION_ID) + String.format(" '%s' INTEGER NOT NULL,", EVENT_ID) + String.format(" '%s' TEXT NOT NULL,", ORDER_ID) + String.format(" '%s' TEXT,", STORE_NAME) + String.format(" '%s' TEXT NOT NULL,", TOTAL_COST) + String.format(" '%s' TEXT,", TOTAL_TAX) + String.format(" '%s' TEXT);", SHIPPING_COST));
    private static final String CUSTOMVAR_ID = "cv_id";
    private static final String CUSTOMVAR_INDEX = "cv_index";
    private static final String CUSTOMVAR_NAME = "cv_name";
    private static final String CUSTOMVAR_SCOPE = "cv_scope";
    private static final String CUSTOMVAR_VALUE = "cv_value";
    private static final String CUSTOM_VARIABLE_COLUMN_TYPE = "CHAR(64) NOT NULL";
    private static final String DATABASE_NAME = "google_analytics.db";
    private static final int DATABASE_VERSION = 4;
    private static final String EVENT_ID = "event_id";
    private static final String HIT_ID = "hit_id";
    private static final String HIT_STRING = "hit_string";
    private static final String HIT_TIMESTAMP = "hit_time";
    private static final String ITEM_CATEGORY = "item_category";
    private static final String ITEM_COUNT = "item_count";
    private static final String ITEM_ID = "item_id";
    private static final String ITEM_NAME = "item_name";
    private static final String ITEM_PRICE = "item_price";
    private static final String ITEM_SKU = "item_sku";
    private static final String LABEL = "label";
    private static final int MAX_HITS = 1000;
    private static final String ORDER_ID = "order_id";
    private static final String RANDOM_VAL = "random_val";
    private static final String REFERRER = "referrer";
    private static final String REFERRER_COLUMN = "referrer";
    private static final String SCREEN_HEIGHT = "screen_height";
    private static final String SCREEN_WIDTH = "screen_width";
    private static final String SHIPPING_COST = "tran_shippingcost";
    private static final String STORE_ID = "store_id";
    private static final String STORE_NAME = "tran_storename";
    private static final String TIMESTAMP_CURRENT = "timestamp_current";
    private static final String TIMESTAMP_FIRST = "timestamp_first";
    private static final String TIMESTAMP_PREVIOUS = "timestamp_previous";
    private static final String TIMESTAMP_REFERRER = "timestamp_referrer";
    private static final String TOTAL_COST = "tran_totalcost";
    private static final String TOTAL_TAX = "tran_totaltax";
    private static final String TRANSACTION_ID = "tran_id";
    private static final boolean UPDATE_TIMESTAMP = true;
    private static final String USER_ID = "user_id";
    private static final String VALUE = "value";
    private static final String VISITS = "visits";
    private boolean anonymizeIp;
    private DataBaseHelper databaseHelper;
    private int numStoredHits;
    private int sampleRate;
    private boolean sessionStarted;
    private int storeId;
    private long timestampCurrent;
    private long timestampFirst;
    private long timestampPrevious;
    private boolean useStoredVisitorVars;
    private int visits;

    static class DataBaseHelper extends SQLiteOpenHelper {
        private final int databaseVersion;
        private final PersistentHitStore store;

        public DataBaseHelper(Context context, PersistentHitStore persistentHitStore) {
            this(context, PersistentHitStore.DATABASE_NAME, PersistentHitStore.DATABASE_VERSION, persistentHitStore);
        }

        DataBaseHelper(Context context, String str, int i, PersistentHitStore persistentHitStore) {
            super(context, str, (SQLiteDatabase.CursorFactory) null, i);
            this.databaseVersion = i;
            this.store = persistentHitStore;
        }

        public DataBaseHelper(Context context, String str, PersistentHitStore persistentHitStore) {
            this(context, str, PersistentHitStore.DATABASE_VERSION, persistentHitStore);
        }

        private void createECommerceTables(SQLiteDatabase sQLiteDatabase) {
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS transaction_events;");
            sQLiteDatabase.execSQL(PersistentHitStore.CREATE_TRANSACTION_EVENTS_TABLE);
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS item_events;");
            sQLiteDatabase.execSQL(PersistentHitStore.CREATE_ITEM_EVENTS_TABLE);
        }

        private void createHitTable(SQLiteDatabase sQLiteDatabase) {
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS hits;");
            sQLiteDatabase.execSQL(PersistentHitStore.CREATE_HITS_TABLE);
        }

        private void createReferrerTable(SQLiteDatabase sQLiteDatabase) {
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS referrer;");
            sQLiteDatabase.execSQL(PersistentHitStore.CREATE_REFERRER_TABLE);
        }

        private void migrateEventsToHits(SQLiteDatabase sQLiteDatabase, int i) {
            this.store.loadExistingSession(sQLiteDatabase);
            Event[] peekEvents = this.store.peekEvents(PersistentHitStore.MAX_HITS, sQLiteDatabase, i);
            for (Event putEvent : peekEvents) {
                this.store.putEvent(putEvent, sQLiteDatabase, false);
            }
            sQLiteDatabase.execSQL("DELETE from events;");
            sQLiteDatabase.execSQL("DELETE from item_events;");
            sQLiteDatabase.execSQL("DELETE from transaction_events;");
            sQLiteDatabase.execSQL("DELETE from custom_variables;");
        }

        /* JADX WARNING: Removed duplicated region for block: B:23:0x006c  */
        /* JADX WARNING: Removed duplicated region for block: B:25:0x0071  */
        /* JADX WARNING: Removed duplicated region for block: B:29:0x007a  */
        /* JADX WARNING: Removed duplicated region for block: B:31:0x007f  */
        /* JADX WARNING: Removed duplicated region for block: B:46:? A[RETURN, SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private void migrateOldReferrer(android.database.sqlite.SQLiteDatabase r15) {
            /*
                r14 = this;
                r12 = 0
                java.lang.String r1 = "install_referrer"
                r0 = 1
                java.lang.String[] r2 = new java.lang.String[r0]     // Catch:{ SQLiteException -> 0x005e, all -> 0x0075 }
                r0 = 0
                java.lang.String r3 = "referrer"
                r2[r0] = r3     // Catch:{ SQLiteException -> 0x005e, all -> 0x0075 }
                r3 = 0
                r4 = 0
                r5 = 0
                r6 = 0
                r7 = 0
                r0 = r15
                android.database.Cursor r8 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x005e, all -> 0x0075 }
                r9 = 0
                boolean r0 = r8.moveToFirst()     // Catch:{ SQLiteException -> 0x008f, all -> 0x0083 }
                if (r0 == 0) goto L_0x009b
                r0 = 0
                java.lang.String r11 = r8.getString(r0)     // Catch:{ SQLiteException -> 0x008f, all -> 0x0083 }
                java.lang.String r1 = "session"
                r2 = 0
                r3 = 0
                r4 = 0
                r5 = 0
                r6 = 0
                r7 = 0
                r0 = r15
                android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x008f, all -> 0x0083 }
                boolean r1 = r0.moveToFirst()     // Catch:{ SQLiteException -> 0x0093, all -> 0x0087 }
                if (r1 == 0) goto L_0x0099
                r1 = 0
                long r1 = r0.getLong(r1)     // Catch:{ SQLiteException -> 0x0093, all -> 0x0087 }
            L_0x003a:
                android.content.ContentValues r3 = new android.content.ContentValues     // Catch:{ SQLiteException -> 0x0093, all -> 0x0087 }
                r3.<init>()     // Catch:{ SQLiteException -> 0x0093, all -> 0x0087 }
                java.lang.String r4 = "referrer"
                r3.put(r4, r11)     // Catch:{ SQLiteException -> 0x0093, all -> 0x0087 }
                java.lang.String r4 = "timestamp_referrer"
                java.lang.Long r1 = java.lang.Long.valueOf(r1)     // Catch:{ SQLiteException -> 0x0093, all -> 0x0087 }
                r3.put(r4, r1)     // Catch:{ SQLiteException -> 0x0093, all -> 0x0087 }
                java.lang.String r1 = "referrer"
                r2 = 0
                r15.insert(r1, r2, r3)     // Catch:{ SQLiteException -> 0x0093, all -> 0x0087 }
            L_0x0053:
                if (r8 == 0) goto L_0x0058
                r8.close()
            L_0x0058:
                if (r0 == 0) goto L_0x005d
                r0.close()
            L_0x005d:
                return
            L_0x005e:
                r0 = move-exception
                r1 = r12
                r2 = r12
            L_0x0061:
                java.lang.String r3 = "GoogleAnalyticsTracker"
                java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x008d }
                android.util.Log.e(r3, r0)     // Catch:{ all -> 0x008d }
                if (r2 == 0) goto L_0x006f
                r2.close()
            L_0x006f:
                if (r1 == 0) goto L_0x005d
                r1.close()
                goto L_0x005d
            L_0x0075:
                r0 = move-exception
                r1 = r12
                r2 = r12
            L_0x0078:
                if (r2 == 0) goto L_0x007d
                r2.close()
            L_0x007d:
                if (r1 == 0) goto L_0x0082
                r1.close()
            L_0x0082:
                throw r0
            L_0x0083:
                r0 = move-exception
                r1 = r12
                r2 = r8
                goto L_0x0078
            L_0x0087:
                r1 = move-exception
                r2 = r8
                r13 = r0
                r0 = r1
                r1 = r13
                goto L_0x0078
            L_0x008d:
                r0 = move-exception
                goto L_0x0078
            L_0x008f:
                r0 = move-exception
                r1 = r12
                r2 = r8
                goto L_0x0061
            L_0x0093:
                r1 = move-exception
                r2 = r8
                r13 = r0
                r0 = r1
                r1 = r13
                goto L_0x0061
            L_0x0099:
                r1 = r9
                goto L_0x003a
            L_0x009b:
                r0 = r12
                goto L_0x0053
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.apps.analytics.PersistentHitStore.DataBaseHelper.migrateOldReferrer(android.database.sqlite.SQLiteDatabase):void");
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
         arg types: [java.lang.String, int]
         candidates:
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
        /* access modifiers changed from: package-private */
        public void createCustomVariableTables(SQLiteDatabase sQLiteDatabase) {
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS custom_variables;");
            sQLiteDatabase.execSQL(PersistentHitStore.CREATE_CUSTOM_VARIABLES_TABLE);
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS custom_var_cache;");
            sQLiteDatabase.execSQL(PersistentHitStore.CREATE_CUSTOM_VAR_CACHE_TABLE);
            for (int i = 1; i <= 5; i++) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(PersistentHitStore.EVENT_ID, (Integer) 0);
                contentValues.put(PersistentHitStore.CUSTOMVAR_INDEX, Integer.valueOf(i));
                contentValues.put(PersistentHitStore.CUSTOMVAR_NAME, "");
                contentValues.put(PersistentHitStore.CUSTOMVAR_SCOPE, (Integer) 3);
                contentValues.put(PersistentHitStore.CUSTOMVAR_VALUE, "");
                sQLiteDatabase.insert("custom_var_cache", PersistentHitStore.EVENT_ID, contentValues);
            }
        }

        public void onCreate(SQLiteDatabase sQLiteDatabase) {
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS events;");
            sQLiteDatabase.execSQL(PersistentHitStore.CREATE_EVENTS_TABLE);
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS install_referrer;");
            sQLiteDatabase.execSQL(PersistentHitStore.CREATE_INSTALL_REFERRER_TABLE);
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS session;");
            sQLiteDatabase.execSQL(PersistentHitStore.CREATE_SESSION_TABLE);
            if (this.databaseVersion > 1) {
                createCustomVariableTables(sQLiteDatabase);
            }
            if (this.databaseVersion > 2) {
                createECommerceTables(sQLiteDatabase);
            }
            if (this.databaseVersion > 3) {
                createHitTable(sQLiteDatabase);
                createReferrerTable(sQLiteDatabase);
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
         arg types: [java.lang.String, int]
         candidates:
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
        public void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
            Log.w(GoogleAnalyticsTracker.LOG_TAG, "Downgrading database version from " + i + " to " + i2 + " not recommended.");
            sQLiteDatabase.execSQL(PersistentHitStore.CREATE_REFERRER_TABLE);
            sQLiteDatabase.execSQL(PersistentHitStore.CREATE_HITS_TABLE);
            sQLiteDatabase.execSQL(PersistentHitStore.CREATE_CUSTOM_VAR_CACHE_TABLE);
            sQLiteDatabase.execSQL(PersistentHitStore.CREATE_SESSION_TABLE);
            HashSet hashSet = new HashSet();
            Cursor query = sQLiteDatabase.query("custom_var_cache", null, null, null, null, null, null, null);
            while (query.moveToNext()) {
                try {
                    hashSet.add(Integer.valueOf(query.getInt(query.getColumnIndex(PersistentHitStore.CUSTOMVAR_INDEX))));
                } catch (SQLiteException e) {
                    Log.e(GoogleAnalyticsTracker.LOG_TAG, "Error on downgrade: " + e.toString());
                } finally {
                    query.close();
                }
            }
            for (int i3 = 1; i3 <= 5; i3++) {
                try {
                    if (!hashSet.contains(Integer.valueOf(i3))) {
                        ContentValues contentValues = new ContentValues();
                        contentValues.put(PersistentHitStore.EVENT_ID, (Integer) 0);
                        contentValues.put(PersistentHitStore.CUSTOMVAR_INDEX, Integer.valueOf(i3));
                        contentValues.put(PersistentHitStore.CUSTOMVAR_NAME, "");
                        contentValues.put(PersistentHitStore.CUSTOMVAR_SCOPE, (Integer) 3);
                        contentValues.put(PersistentHitStore.CUSTOMVAR_VALUE, "");
                        sQLiteDatabase.insert("custom_var_cache", PersistentHitStore.EVENT_ID, contentValues);
                    }
                } catch (SQLiteException e2) {
                    Log.e(GoogleAnalyticsTracker.LOG_TAG, "Error inserting custom variable on downgrade: " + e2.toString());
                }
            }
        }

        public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
            if (i > i2) {
                onDowngrade(sQLiteDatabase, i, i2);
                return;
            }
            if (i < 2 && i2 > 1) {
                createCustomVariableTables(sQLiteDatabase);
            }
            if (i < 3 && i2 > 2) {
                createECommerceTables(sQLiteDatabase);
            }
            if (i < PersistentHitStore.DATABASE_VERSION && i2 > 3) {
                createHitTable(sQLiteDatabase);
                createReferrerTable(sQLiteDatabase);
                migrateEventsToHits(sQLiteDatabase, i);
                migrateOldReferrer(sQLiteDatabase);
            }
        }
    }

    PersistentHitStore(Context context) {
        this(context, DATABASE_NAME, DATABASE_VERSION);
    }

    PersistentHitStore(Context context, String str) {
        this(context, str, DATABASE_VERSION);
    }

    PersistentHitStore(Context context, String str, int i) {
        this.sampleRate = 100;
        this.databaseHelper = new DataBaseHelper(context, str, i, this);
        loadExistingSession();
    }

    PersistentHitStore(DataBaseHelper dataBaseHelper) {
        this.sampleRate = 100;
        this.databaseHelper = dataBaseHelper;
        loadExistingSession();
    }

    static String formatReferrer(String str) {
        if (str == null) {
            return null;
        }
        Map<String, String> parseURLParameters = Utils.parseURLParameters(str);
        boolean z = parseURLParameters.get("utm_campaign") != null;
        boolean z2 = parseURLParameters.get("utm_medium") != null;
        boolean z3 = parseURLParameters.get("utm_source") != null;
        if ((parseURLParameters.get("gclid") != null) || (z && z2 && z3)) {
            String[][] strArr = new String[7][];
            strArr[0] = new String[]{"utmcid", parseURLParameters.get("utm_id")};
            strArr[1] = new String[]{"utmcsr", parseURLParameters.get("utm_source")};
            strArr[2] = new String[]{"utmgclid", parseURLParameters.get("gclid")};
            strArr[3] = new String[]{"utmccn", parseURLParameters.get("utm_campaign")};
            strArr[DATABASE_VERSION] = new String[]{"utmcmd", parseURLParameters.get("utm_medium")};
            strArr[5] = new String[]{"utmctr", parseURLParameters.get("utm_term")};
            strArr[6] = new String[]{"utmcct", parseURLParameters.get("utm_content")};
            StringBuilder sb = new StringBuilder();
            boolean z4 = true;
            for (int i = 0; i < strArr.length; i++) {
                if (strArr[i][1] != null) {
                    String replace = strArr[i][1].replace("+", "%20").replace(" ", "%20");
                    if (z4) {
                        z4 = false;
                    } else {
                        sb.append("|");
                    }
                    sb.append(strArr[i][0]).append("=").append(replace);
                }
            }
            return sb.toString();
        }
        Log.w(GoogleAnalyticsTracker.LOG_TAG, "Badly formatted referrer missing campaign, medium and source or click ID");
        return null;
    }

    public void clearReferrer() {
        try {
            this.databaseHelper.getWritableDatabase().delete("referrer", null, null);
        } catch (SQLiteException e) {
            Log.e(GoogleAnalyticsTracker.LOG_TAG, e.toString());
        }
    }

    public void deleteHit(long j) {
        try {
            this.databaseHelper.getWritableDatabase().delete("hits", "hit_id = ?", new String[]{Long.toString(j)});
            this.numStoredHits--;
        } catch (SQLiteException e) {
            Log.e(GoogleAnalyticsTracker.LOG_TAG, e.toString());
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0071  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.android.apps.analytics.CustomVariableBuffer getCustomVariables(long r12, android.database.sqlite.SQLiteDatabase r14) {
        /*
            r11 = this;
            r9 = 0
            com.google.android.apps.analytics.CustomVariableBuffer r8 = new com.google.android.apps.analytics.CustomVariableBuffer
            r8.<init>()
            java.lang.String r1 = "custom_variables"
            r2 = 0
            java.lang.String r3 = "event_id= ?"
            r0 = 1
            java.lang.String[] r4 = new java.lang.String[r0]     // Catch:{ SQLiteException -> 0x007c, all -> 0x006d }
            r0 = 0
            java.lang.String r5 = java.lang.Long.toString(r12)     // Catch:{ SQLiteException -> 0x007c, all -> 0x006d }
            r4[r0] = r5     // Catch:{ SQLiteException -> 0x007c, all -> 0x006d }
            r5 = 0
            r6 = 0
            r7 = 0
            r0 = r14
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x007c, all -> 0x006d }
        L_0x001d:
            boolean r1 = r0.moveToNext()     // Catch:{ SQLiteException -> 0x0054, all -> 0x0075 }
            if (r1 == 0) goto L_0x0067
            com.google.android.apps.analytics.CustomVariable r1 = new com.google.android.apps.analytics.CustomVariable     // Catch:{ SQLiteException -> 0x0054, all -> 0x0075 }
            java.lang.String r2 = "cv_index"
            int r2 = r0.getColumnIndex(r2)     // Catch:{ SQLiteException -> 0x0054, all -> 0x0075 }
            int r2 = r0.getInt(r2)     // Catch:{ SQLiteException -> 0x0054, all -> 0x0075 }
            java.lang.String r3 = "cv_name"
            int r3 = r0.getColumnIndex(r3)     // Catch:{ SQLiteException -> 0x0054, all -> 0x0075 }
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLiteException -> 0x0054, all -> 0x0075 }
            java.lang.String r4 = "cv_value"
            int r4 = r0.getColumnIndex(r4)     // Catch:{ SQLiteException -> 0x0054, all -> 0x0075 }
            java.lang.String r4 = r0.getString(r4)     // Catch:{ SQLiteException -> 0x0054, all -> 0x0075 }
            java.lang.String r5 = "cv_scope"
            int r5 = r0.getColumnIndex(r5)     // Catch:{ SQLiteException -> 0x0054, all -> 0x0075 }
            int r5 = r0.getInt(r5)     // Catch:{ SQLiteException -> 0x0054, all -> 0x0075 }
            r1.<init>(r2, r3, r4, r5)     // Catch:{ SQLiteException -> 0x0054, all -> 0x0075 }
            r8.setCustomVariable(r1)     // Catch:{ SQLiteException -> 0x0054, all -> 0x0075 }
            goto L_0x001d
        L_0x0054:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
        L_0x0058:
            java.lang.String r2 = "GoogleAnalyticsTracker"
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x007a }
            android.util.Log.e(r2, r0)     // Catch:{ all -> 0x007a }
            if (r1 == 0) goto L_0x0066
            r1.close()
        L_0x0066:
            return r8
        L_0x0067:
            if (r0 == 0) goto L_0x0066
            r0.close()
            goto L_0x0066
        L_0x006d:
            r0 = move-exception
            r1 = r9
        L_0x006f:
            if (r1 == 0) goto L_0x0074
            r1.close()
        L_0x0074:
            throw r0
        L_0x0075:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
            goto L_0x006f
        L_0x007a:
            r0 = move-exception
            goto L_0x006f
        L_0x007c:
            r0 = move-exception
            r1 = r9
            goto L_0x0058
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.apps.analytics.PersistentHitStore.getCustomVariables(long, android.database.sqlite.SQLiteDatabase):com.google.android.apps.analytics.CustomVariableBuffer");
    }

    /* access modifiers changed from: package-private */
    public DataBaseHelper getDatabaseHelper() {
        return this.databaseHelper;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x008d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.android.apps.analytics.Item getItem(long r10, android.database.sqlite.SQLiteDatabase r12) {
        /*
            r9 = this;
            r8 = 0
            java.lang.String r1 = "item_events"
            r2 = 0
            java.lang.String r3 = "event_id= ?"
            r0 = 1
            java.lang.String[] r4 = new java.lang.String[r0]     // Catch:{ SQLiteException -> 0x0078, all -> 0x0089 }
            r0 = 0
            java.lang.String r5 = java.lang.Long.toString(r10)     // Catch:{ SQLiteException -> 0x0078, all -> 0x0089 }
            r4[r0] = r5     // Catch:{ SQLiteException -> 0x0078, all -> 0x0089 }
            r5 = 0
            r6 = 0
            r7 = 0
            r0 = r12
            android.database.Cursor r7 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x0078, all -> 0x0089 }
            boolean r0 = r7.moveToFirst()     // Catch:{ SQLiteException -> 0x0096, all -> 0x0091 }
            if (r0 == 0) goto L_0x0071
            com.google.android.apps.analytics.Item$Builder r0 = new com.google.android.apps.analytics.Item$Builder     // Catch:{ SQLiteException -> 0x0096, all -> 0x0091 }
            java.lang.String r1 = "order_id"
            int r1 = r7.getColumnIndex(r1)     // Catch:{ SQLiteException -> 0x0096, all -> 0x0091 }
            java.lang.String r1 = r7.getString(r1)     // Catch:{ SQLiteException -> 0x0096, all -> 0x0091 }
            java.lang.String r2 = "item_sku"
            int r2 = r7.getColumnIndex(r2)     // Catch:{ SQLiteException -> 0x0096, all -> 0x0091 }
            java.lang.String r2 = r7.getString(r2)     // Catch:{ SQLiteException -> 0x0096, all -> 0x0091 }
            java.lang.String r3 = "item_price"
            int r3 = r7.getColumnIndex(r3)     // Catch:{ SQLiteException -> 0x0096, all -> 0x0091 }
            double r3 = r7.getDouble(r3)     // Catch:{ SQLiteException -> 0x0096, all -> 0x0091 }
            java.lang.String r5 = "item_count"
            int r5 = r7.getColumnIndex(r5)     // Catch:{ SQLiteException -> 0x0096, all -> 0x0091 }
            long r5 = r7.getLong(r5)     // Catch:{ SQLiteException -> 0x0096, all -> 0x0091 }
            r0.<init>(r1, r2, r3, r5)     // Catch:{ SQLiteException -> 0x0096, all -> 0x0091 }
            java.lang.String r1 = "item_name"
            int r1 = r7.getColumnIndex(r1)     // Catch:{ SQLiteException -> 0x0096, all -> 0x0091 }
            java.lang.String r1 = r7.getString(r1)     // Catch:{ SQLiteException -> 0x0096, all -> 0x0091 }
            com.google.android.apps.analytics.Item$Builder r0 = r0.setItemName(r1)     // Catch:{ SQLiteException -> 0x0096, all -> 0x0091 }
            java.lang.String r1 = "item_category"
            int r1 = r7.getColumnIndex(r1)     // Catch:{ SQLiteException -> 0x0096, all -> 0x0091 }
            java.lang.String r1 = r7.getString(r1)     // Catch:{ SQLiteException -> 0x0096, all -> 0x0091 }
            com.google.android.apps.analytics.Item$Builder r0 = r0.setItemCategory(r1)     // Catch:{ SQLiteException -> 0x0096, all -> 0x0091 }
            com.google.android.apps.analytics.Item r0 = r0.build()     // Catch:{ SQLiteException -> 0x0096, all -> 0x0091 }
            if (r7 == 0) goto L_0x0070
            r7.close()
        L_0x0070:
            return r0
        L_0x0071:
            if (r7 == 0) goto L_0x0076
            r7.close()
        L_0x0076:
            r0 = r8
            goto L_0x0070
        L_0x0078:
            r0 = move-exception
            r1 = r8
        L_0x007a:
            java.lang.String r2 = "GoogleAnalyticsTracker"
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0094 }
            android.util.Log.e(r2, r0)     // Catch:{ all -> 0x0094 }
            if (r1 == 0) goto L_0x0076
            r1.close()
            goto L_0x0076
        L_0x0089:
            r0 = move-exception
            r1 = r8
        L_0x008b:
            if (r1 == 0) goto L_0x0090
            r1.close()
        L_0x0090:
            throw r0
        L_0x0091:
            r0 = move-exception
            r1 = r7
            goto L_0x008b
        L_0x0094:
            r0 = move-exception
            goto L_0x008b
        L_0x0096:
            r0 = move-exception
            r1 = r7
            goto L_0x007a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.apps.analytics.PersistentHitStore.getItem(long, android.database.sqlite.SQLiteDatabase):com.google.android.apps.analytics.Item");
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0038  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int getNumStoredHits() {
        /*
            r6 = this;
            r4 = 0
            r3 = 0
            com.google.android.apps.analytics.PersistentHitStore$DataBaseHelper r0 = r6.databaseHelper     // Catch:{ SQLiteException -> 0x0022, all -> 0x0034 }
            android.database.sqlite.SQLiteDatabase r0 = r0.getReadableDatabase()     // Catch:{ SQLiteException -> 0x0022, all -> 0x0034 }
            java.lang.String r1 = "SELECT COUNT(*) from hits"
            r2 = 0
            android.database.Cursor r0 = r0.rawQuery(r1, r2)     // Catch:{ SQLiteException -> 0x0022, all -> 0x0034 }
            boolean r1 = r0.moveToFirst()     // Catch:{ SQLiteException -> 0x0043, all -> 0x003c }
            if (r1 == 0) goto L_0x004c
            r1 = 0
            long r1 = r0.getLong(r1)     // Catch:{ SQLiteException -> 0x0043, all -> 0x003c }
            int r1 = (int) r1
        L_0x001b:
            if (r0 == 0) goto L_0x004a
            r0.close()
            r0 = r1
        L_0x0021:
            return r0
        L_0x0022:
            r0 = move-exception
            r1 = r4
        L_0x0024:
            java.lang.String r2 = "GoogleAnalyticsTracker"
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0041 }
            android.util.Log.e(r2, r0)     // Catch:{ all -> 0x0041 }
            if (r1 == 0) goto L_0x0048
            r1.close()
            r0 = r3
            goto L_0x0021
        L_0x0034:
            r0 = move-exception
            r1 = r4
        L_0x0036:
            if (r1 == 0) goto L_0x003b
            r1.close()
        L_0x003b:
            throw r0
        L_0x003c:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0036
        L_0x0041:
            r0 = move-exception
            goto L_0x0036
        L_0x0043:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0024
        L_0x0048:
            r0 = r3
            goto L_0x0021
        L_0x004a:
            r0 = r1
            goto L_0x0021
        L_0x004c:
            r1 = r3
            goto L_0x001b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.apps.analytics.PersistentHitStore.getNumStoredHits():int");
    }

    public Referrer getReferrer() {
        try {
            return getReferrer(this.databaseHelper.getReadableDatabase(), false);
        } catch (SQLiteException e) {
            Log.e(GoogleAnalyticsTracker.LOG_TAG, e.toString());
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0069  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0072  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.android.apps.analytics.Referrer getReferrer(android.database.sqlite.SQLiteDatabase r12, boolean r13) {
        /*
            r11 = this;
            r8 = 0
            java.lang.String r1 = "referrer"
            r0 = 2
            java.lang.String[] r2 = new java.lang.String[r0]     // Catch:{ SQLiteException -> 0x005c, all -> 0x006e }
            r0 = 0
            java.lang.String r3 = "referrer"
            r2[r0] = r3     // Catch:{ SQLiteException -> 0x005c, all -> 0x006e }
            r0 = 1
            java.lang.String r3 = "timestamp_referrer"
            r2[r0] = r3     // Catch:{ SQLiteException -> 0x005c, all -> 0x006e }
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            r0 = r12
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x005c, all -> 0x006e }
            boolean r1 = r0.moveToFirst()     // Catch:{ SQLiteException -> 0x0080, all -> 0x0076 }
            if (r1 == 0) goto L_0x008c
            r1 = 1
            long r1 = r0.getLong(r1)     // Catch:{ SQLiteException -> 0x0080, all -> 0x0076 }
            r3 = 0
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLiteException -> 0x0080, all -> 0x0076 }
            r4 = 0
            int r4 = (r1 > r4 ? 1 : (r1 == r4 ? 0 : -1))
            if (r4 != 0) goto L_0x0088
            if (r13 == 0) goto L_0x0088
            long r1 = r11.timestampCurrent     // Catch:{ SQLiteException -> 0x0080, all -> 0x0076 }
            r0.close()     // Catch:{ SQLiteException -> 0x0080, all -> 0x0076 }
            android.content.ContentValues r0 = new android.content.ContentValues     // Catch:{ SQLiteException -> 0x005c, all -> 0x006e }
            r0.<init>()     // Catch:{ SQLiteException -> 0x005c, all -> 0x006e }
            java.lang.String r4 = "timestamp_referrer"
            java.lang.Long r5 = java.lang.Long.valueOf(r1)     // Catch:{ SQLiteException -> 0x005c, all -> 0x006e }
            r0.put(r4, r5)     // Catch:{ SQLiteException -> 0x005c, all -> 0x006e }
            java.lang.String r4 = "referrer"
            r5 = 0
            r6 = 0
            r12.update(r4, r0, r5, r6)     // Catch:{ SQLiteException -> 0x005c, all -> 0x006e }
            r9 = r1
            r0 = r9
            r2 = r8
        L_0x004f:
            com.google.android.apps.analytics.Referrer r4 = new com.google.android.apps.analytics.Referrer     // Catch:{ SQLiteException -> 0x0085, all -> 0x007b }
            r4.<init>(r3, r0)     // Catch:{ SQLiteException -> 0x0085, all -> 0x007b }
            r0 = r4
            r1 = r2
        L_0x0056:
            if (r1 == 0) goto L_0x005b
            r1.close()
        L_0x005b:
            return r0
        L_0x005c:
            r0 = move-exception
            r1 = r8
        L_0x005e:
            java.lang.String r2 = "GoogleAnalyticsTracker"
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x007e }
            android.util.Log.e(r2, r0)     // Catch:{ all -> 0x007e }
            if (r1 == 0) goto L_0x006c
            r1.close()
        L_0x006c:
            r0 = r8
            goto L_0x005b
        L_0x006e:
            r0 = move-exception
            r1 = r8
        L_0x0070:
            if (r1 == 0) goto L_0x0075
            r1.close()
        L_0x0075:
            throw r0
        L_0x0076:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x0070
        L_0x007b:
            r0 = move-exception
            r1 = r2
            goto L_0x0070
        L_0x007e:
            r0 = move-exception
            goto L_0x0070
        L_0x0080:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x005e
        L_0x0085:
            r0 = move-exception
            r1 = r2
            goto L_0x005e
        L_0x0088:
            r9 = r1
            r2 = r0
            r0 = r9
            goto L_0x004f
        L_0x008c:
            r1 = r0
            r0 = r8
            goto L_0x0056
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.apps.analytics.PersistentHitStore.getReferrer(android.database.sqlite.SQLiteDatabase, boolean):com.google.android.apps.analytics.Referrer");
    }

    public int getStoreId() {
        return this.storeId;
    }

    /* access modifiers changed from: package-private */
    public long getTimestampCurrent() {
        return this.timestampCurrent;
    }

    /* access modifiers changed from: package-private */
    public long getTimestampFirst() {
        return this.timestampFirst;
    }

    /* access modifiers changed from: package-private */
    public long getTimestampPrevious() {
        return this.timestampPrevious;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0088  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.android.apps.analytics.Transaction getTransaction(long r11, android.database.sqlite.SQLiteDatabase r13) {
        /*
            r10 = this;
            r8 = 0
            java.lang.String r1 = "transaction_events"
            r2 = 0
            java.lang.String r3 = "event_id= ?"
            r0 = 1
            java.lang.String[] r4 = new java.lang.String[r0]     // Catch:{ SQLiteException -> 0x0073, all -> 0x0084 }
            r0 = 0
            java.lang.String r5 = java.lang.Long.toString(r11)     // Catch:{ SQLiteException -> 0x0073, all -> 0x0084 }
            r4[r0] = r5     // Catch:{ SQLiteException -> 0x0073, all -> 0x0084 }
            r5 = 0
            r6 = 0
            r7 = 0
            r0 = r13
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x0073, all -> 0x0084 }
            boolean r1 = r0.moveToFirst()     // Catch:{ SQLiteException -> 0x0093, all -> 0x008c }
            if (r1 == 0) goto L_0x006c
            com.google.android.apps.analytics.Transaction$Builder r1 = new com.google.android.apps.analytics.Transaction$Builder     // Catch:{ SQLiteException -> 0x0093, all -> 0x008c }
            java.lang.String r2 = "order_id"
            int r2 = r0.getColumnIndex(r2)     // Catch:{ SQLiteException -> 0x0093, all -> 0x008c }
            java.lang.String r2 = r0.getString(r2)     // Catch:{ SQLiteException -> 0x0093, all -> 0x008c }
            java.lang.String r3 = "tran_totalcost"
            int r3 = r0.getColumnIndex(r3)     // Catch:{ SQLiteException -> 0x0093, all -> 0x008c }
            double r3 = r0.getDouble(r3)     // Catch:{ SQLiteException -> 0x0093, all -> 0x008c }
            r1.<init>(r2, r3)     // Catch:{ SQLiteException -> 0x0093, all -> 0x008c }
            java.lang.String r2 = "tran_storename"
            int r2 = r0.getColumnIndex(r2)     // Catch:{ SQLiteException -> 0x0093, all -> 0x008c }
            java.lang.String r2 = r0.getString(r2)     // Catch:{ SQLiteException -> 0x0093, all -> 0x008c }
            com.google.android.apps.analytics.Transaction$Builder r1 = r1.setStoreName(r2)     // Catch:{ SQLiteException -> 0x0093, all -> 0x008c }
            java.lang.String r2 = "tran_totaltax"
            int r2 = r0.getColumnIndex(r2)     // Catch:{ SQLiteException -> 0x0093, all -> 0x008c }
            double r2 = r0.getDouble(r2)     // Catch:{ SQLiteException -> 0x0093, all -> 0x008c }
            com.google.android.apps.analytics.Transaction$Builder r1 = r1.setTotalTax(r2)     // Catch:{ SQLiteException -> 0x0093, all -> 0x008c }
            java.lang.String r2 = "tran_shippingcost"
            int r2 = r0.getColumnIndex(r2)     // Catch:{ SQLiteException -> 0x0093, all -> 0x008c }
            double r2 = r0.getDouble(r2)     // Catch:{ SQLiteException -> 0x0093, all -> 0x008c }
            com.google.android.apps.analytics.Transaction$Builder r1 = r1.setShippingCost(r2)     // Catch:{ SQLiteException -> 0x0093, all -> 0x008c }
            com.google.android.apps.analytics.Transaction r1 = r1.build()     // Catch:{ SQLiteException -> 0x0093, all -> 0x008c }
            if (r0 == 0) goto L_0x006a
            r0.close()
        L_0x006a:
            r0 = r1
        L_0x006b:
            return r0
        L_0x006c:
            if (r0 == 0) goto L_0x0071
            r0.close()
        L_0x0071:
            r0 = r8
            goto L_0x006b
        L_0x0073:
            r0 = move-exception
            r1 = r8
        L_0x0075:
            java.lang.String r2 = "GoogleAnalyticsTracker"
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0091 }
            android.util.Log.e(r2, r0)     // Catch:{ all -> 0x0091 }
            if (r1 == 0) goto L_0x0071
            r1.close()
            goto L_0x0071
        L_0x0084:
            r0 = move-exception
            r1 = r8
        L_0x0086:
            if (r1 == 0) goto L_0x008b
            r1.close()
        L_0x008b:
            throw r0
        L_0x008c:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x0086
        L_0x0091:
            r0 = move-exception
            goto L_0x0086
        L_0x0093:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x0075
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.apps.analytics.PersistentHitStore.getTransaction(long, android.database.sqlite.SQLiteDatabase):com.google.android.apps.analytics.Transaction");
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0057  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String getVisitorCustomVar(int r11) {
        /*
            r10 = this;
            r8 = 0
            com.google.android.apps.analytics.PersistentHitStore$DataBaseHelper r0 = r10.databaseHelper     // Catch:{ SQLiteException -> 0x0041, all -> 0x0053 }
            android.database.sqlite.SQLiteDatabase r0 = r0.getReadableDatabase()     // Catch:{ SQLiteException -> 0x0041, all -> 0x0053 }
            java.lang.String r1 = "custom_var_cache"
            r2 = 0
            java.lang.String r3 = "cv_scope = ? AND cv_index = ?"
            r4 = 2
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ SQLiteException -> 0x0041, all -> 0x0053 }
            r5 = 0
            r6 = 1
            java.lang.String r6 = java.lang.Integer.toString(r6)     // Catch:{ SQLiteException -> 0x0041, all -> 0x0053 }
            r4[r5] = r6     // Catch:{ SQLiteException -> 0x0041, all -> 0x0053 }
            r5 = 1
            java.lang.String r6 = java.lang.Integer.toString(r11)     // Catch:{ SQLiteException -> 0x0041, all -> 0x0053 }
            r4[r5] = r6     // Catch:{ SQLiteException -> 0x0041, all -> 0x0053 }
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x0041, all -> 0x0053 }
            int r1 = r0.getCount()     // Catch:{ SQLiteException -> 0x0062, all -> 0x005b }
            if (r1 <= 0) goto L_0x003f
            r0.moveToFirst()     // Catch:{ SQLiteException -> 0x0062, all -> 0x005b }
            java.lang.String r1 = "cv_value"
            int r1 = r0.getColumnIndex(r1)     // Catch:{ SQLiteException -> 0x0062, all -> 0x005b }
            java.lang.String r1 = r0.getString(r1)     // Catch:{ SQLiteException -> 0x0062, all -> 0x005b }
        L_0x0038:
            if (r0 == 0) goto L_0x003d
            r0.close()
        L_0x003d:
            r0 = r1
        L_0x003e:
            return r0
        L_0x003f:
            r1 = r8
            goto L_0x0038
        L_0x0041:
            r0 = move-exception
            r1 = r8
        L_0x0043:
            java.lang.String r2 = "GoogleAnalyticsTracker"
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0060 }
            android.util.Log.e(r2, r0)     // Catch:{ all -> 0x0060 }
            if (r1 == 0) goto L_0x0051
            r1.close()
        L_0x0051:
            r0 = r8
            goto L_0x003e
        L_0x0053:
            r0 = move-exception
            r1 = r8
        L_0x0055:
            if (r1 == 0) goto L_0x005a
            r1.close()
        L_0x005a:
            throw r0
        L_0x005b:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x0055
        L_0x0060:
            r0 = move-exception
            goto L_0x0055
        L_0x0062:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x0043
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.apps.analytics.PersistentHitStore.getVisitorCustomVar(int):java.lang.String");
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0077  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.android.apps.analytics.CustomVariableBuffer getVisitorVarBuffer() {
        /*
            r11 = this;
            r9 = 0
            com.google.android.apps.analytics.CustomVariableBuffer r8 = new com.google.android.apps.analytics.CustomVariableBuffer
            r8.<init>()
            com.google.android.apps.analytics.PersistentHitStore$DataBaseHelper r0 = r11.databaseHelper     // Catch:{ SQLiteException -> 0x0082, all -> 0x0073 }
            android.database.sqlite.SQLiteDatabase r0 = r0.getReadableDatabase()     // Catch:{ SQLiteException -> 0x0082, all -> 0x0073 }
            java.lang.String r1 = "custom_var_cache"
            r2 = 0
            java.lang.String r3 = "cv_scope= ?"
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ SQLiteException -> 0x0082, all -> 0x0073 }
            r5 = 0
            r6 = 1
            java.lang.String r6 = java.lang.Integer.toString(r6)     // Catch:{ SQLiteException -> 0x0082, all -> 0x0073 }
            r4[r5] = r6     // Catch:{ SQLiteException -> 0x0082, all -> 0x0073 }
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x0082, all -> 0x0073 }
        L_0x0023:
            boolean r1 = r0.moveToNext()     // Catch:{ SQLiteException -> 0x005a, all -> 0x007b }
            if (r1 == 0) goto L_0x006d
            com.google.android.apps.analytics.CustomVariable r1 = new com.google.android.apps.analytics.CustomVariable     // Catch:{ SQLiteException -> 0x005a, all -> 0x007b }
            java.lang.String r2 = "cv_index"
            int r2 = r0.getColumnIndex(r2)     // Catch:{ SQLiteException -> 0x005a, all -> 0x007b }
            int r2 = r0.getInt(r2)     // Catch:{ SQLiteException -> 0x005a, all -> 0x007b }
            java.lang.String r3 = "cv_name"
            int r3 = r0.getColumnIndex(r3)     // Catch:{ SQLiteException -> 0x005a, all -> 0x007b }
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLiteException -> 0x005a, all -> 0x007b }
            java.lang.String r4 = "cv_value"
            int r4 = r0.getColumnIndex(r4)     // Catch:{ SQLiteException -> 0x005a, all -> 0x007b }
            java.lang.String r4 = r0.getString(r4)     // Catch:{ SQLiteException -> 0x005a, all -> 0x007b }
            java.lang.String r5 = "cv_scope"
            int r5 = r0.getColumnIndex(r5)     // Catch:{ SQLiteException -> 0x005a, all -> 0x007b }
            int r5 = r0.getInt(r5)     // Catch:{ SQLiteException -> 0x005a, all -> 0x007b }
            r1.<init>(r2, r3, r4, r5)     // Catch:{ SQLiteException -> 0x005a, all -> 0x007b }
            r8.setCustomVariable(r1)     // Catch:{ SQLiteException -> 0x005a, all -> 0x007b }
            goto L_0x0023
        L_0x005a:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
        L_0x005e:
            java.lang.String r2 = "GoogleAnalyticsTracker"
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0080 }
            android.util.Log.e(r2, r0)     // Catch:{ all -> 0x0080 }
            if (r1 == 0) goto L_0x006c
            r1.close()
        L_0x006c:
            return r8
        L_0x006d:
            if (r0 == 0) goto L_0x006c
            r0.close()
            goto L_0x006c
        L_0x0073:
            r0 = move-exception
            r1 = r9
        L_0x0075:
            if (r1 == 0) goto L_0x007a
            r1.close()
        L_0x007a:
            throw r0
        L_0x007b:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
            goto L_0x0075
        L_0x0080:
            r0 = move-exception
            goto L_0x0075
        L_0x0082:
            r0 = move-exception
            r1 = r9
            goto L_0x005e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.apps.analytics.PersistentHitStore.getVisitorVarBuffer():com.google.android.apps.analytics.CustomVariableBuffer");
    }

    public void loadExistingSession() {
        try {
            loadExistingSession(this.databaseHelper.getWritableDatabase());
        } catch (SQLiteException e) {
            Log.e(GoogleAnalyticsTracker.LOG_TAG, e.toString());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
     arg types: [java.lang.String, long]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00cb  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void loadExistingSession(android.database.sqlite.SQLiteDatabase r15) {
        /*
            r14 = this;
            r12 = 1
            r10 = 0
            r9 = 0
            r8 = 0
            java.lang.String r1 = "session"
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            r0 = r15
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x00b6, all -> 0x00c7 }
            boolean r1 = r0.moveToFirst()     // Catch:{ SQLiteException -> 0x00d6, all -> 0x00cf }
            if (r1 == 0) goto L_0x005b
            r1 = 0
            long r1 = r0.getLong(r1)     // Catch:{ SQLiteException -> 0x00d6, all -> 0x00cf }
            r14.timestampFirst = r1     // Catch:{ SQLiteException -> 0x00d6, all -> 0x00cf }
            r1 = 1
            long r1 = r0.getLong(r1)     // Catch:{ SQLiteException -> 0x00d6, all -> 0x00cf }
            r14.timestampPrevious = r1     // Catch:{ SQLiteException -> 0x00d6, all -> 0x00cf }
            r1 = 2
            long r1 = r0.getLong(r1)     // Catch:{ SQLiteException -> 0x00d6, all -> 0x00cf }
            r14.timestampCurrent = r1     // Catch:{ SQLiteException -> 0x00d6, all -> 0x00cf }
            r1 = 3
            int r1 = r0.getInt(r1)     // Catch:{ SQLiteException -> 0x00d6, all -> 0x00cf }
            r14.visits = r1     // Catch:{ SQLiteException -> 0x00d6, all -> 0x00cf }
            r1 = 4
            int r1 = r0.getInt(r1)     // Catch:{ SQLiteException -> 0x00d6, all -> 0x00cf }
            r14.storeId = r1     // Catch:{ SQLiteException -> 0x00d6, all -> 0x00cf }
            r1 = 0
            com.google.android.apps.analytics.Referrer r1 = r14.getReferrer(r15, r1)     // Catch:{ SQLiteException -> 0x00d6, all -> 0x00cf }
            long r2 = r14.timestampFirst     // Catch:{ SQLiteException -> 0x00d6, all -> 0x00cf }
            int r2 = (r2 > r10 ? 1 : (r2 == r10 ? 0 : -1))
            if (r2 == 0) goto L_0x0059
            if (r1 == 0) goto L_0x0050
            long r1 = r1.getTimeStamp()     // Catch:{ SQLiteException -> 0x00d6, all -> 0x00cf }
            int r1 = (r1 > r10 ? 1 : (r1 == r10 ? 0 : -1))
            if (r1 == 0) goto L_0x0059
        L_0x0050:
            r1 = r12
        L_0x0051:
            r14.sessionStarted = r1     // Catch:{ SQLiteException -> 0x00d6, all -> 0x00cf }
        L_0x0053:
            if (r0 == 0) goto L_0x0058
            r0.close()
        L_0x0058:
            return
        L_0x0059:
            r1 = r9
            goto L_0x0051
        L_0x005b:
            r1 = 0
            r14.sessionStarted = r1     // Catch:{ SQLiteException -> 0x00d6, all -> 0x00cf }
            r1 = 1
            r14.useStoredVisitorVars = r1     // Catch:{ SQLiteException -> 0x00d6, all -> 0x00cf }
            java.security.SecureRandom r1 = new java.security.SecureRandom     // Catch:{ SQLiteException -> 0x00d6, all -> 0x00cf }
            r1.<init>()     // Catch:{ SQLiteException -> 0x00d6, all -> 0x00cf }
            int r1 = r1.nextInt()     // Catch:{ SQLiteException -> 0x00d6, all -> 0x00cf }
            r2 = 2147483647(0x7fffffff, float:NaN)
            r1 = r1 & r2
            r14.storeId = r1     // Catch:{ SQLiteException -> 0x00d6, all -> 0x00cf }
            r0.close()     // Catch:{ SQLiteException -> 0x00d6, all -> 0x00cf }
            android.content.ContentValues r0 = new android.content.ContentValues     // Catch:{ SQLiteException -> 0x00b6, all -> 0x00c7 }
            r0.<init>()     // Catch:{ SQLiteException -> 0x00b6, all -> 0x00c7 }
            java.lang.String r1 = "timestamp_first"
            r2 = 0
            java.lang.Long r2 = java.lang.Long.valueOf(r2)     // Catch:{ SQLiteException -> 0x00b6, all -> 0x00c7 }
            r0.put(r1, r2)     // Catch:{ SQLiteException -> 0x00b6, all -> 0x00c7 }
            java.lang.String r1 = "timestamp_previous"
            r2 = 0
            java.lang.Long r2 = java.lang.Long.valueOf(r2)     // Catch:{ SQLiteException -> 0x00b6, all -> 0x00c7 }
            r0.put(r1, r2)     // Catch:{ SQLiteException -> 0x00b6, all -> 0x00c7 }
            java.lang.String r1 = "timestamp_current"
            r2 = 0
            java.lang.Long r2 = java.lang.Long.valueOf(r2)     // Catch:{ SQLiteException -> 0x00b6, all -> 0x00c7 }
            r0.put(r1, r2)     // Catch:{ SQLiteException -> 0x00b6, all -> 0x00c7 }
            java.lang.String r1 = "visits"
            r2 = 0
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ SQLiteException -> 0x00b6, all -> 0x00c7 }
            r0.put(r1, r2)     // Catch:{ SQLiteException -> 0x00b6, all -> 0x00c7 }
            java.lang.String r1 = "store_id"
            int r2 = r14.storeId     // Catch:{ SQLiteException -> 0x00b6, all -> 0x00c7 }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ SQLiteException -> 0x00b6, all -> 0x00c7 }
            r0.put(r1, r2)     // Catch:{ SQLiteException -> 0x00b6, all -> 0x00c7 }
            java.lang.String r1 = "session"
            r2 = 0
            r15.insert(r1, r2, r0)     // Catch:{ SQLiteException -> 0x00b6, all -> 0x00c7 }
            r0 = r8
            goto L_0x0053
        L_0x00b6:
            r0 = move-exception
            r1 = r8
        L_0x00b8:
            java.lang.String r2 = "GoogleAnalyticsTracker"
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00d4 }
            android.util.Log.e(r2, r0)     // Catch:{ all -> 0x00d4 }
            if (r1 == 0) goto L_0x0058
            r1.close()
            goto L_0x0058
        L_0x00c7:
            r0 = move-exception
            r1 = r8
        L_0x00c9:
            if (r1 == 0) goto L_0x00ce
            r1.close()
        L_0x00ce:
            throw r0
        L_0x00cf:
            r1 = move-exception
            r13 = r1
            r1 = r0
            r0 = r13
            goto L_0x00c9
        L_0x00d4:
            r0 = move-exception
            goto L_0x00c9
        L_0x00d6:
            r1 = move-exception
            r13 = r1
            r1 = r0
            r0 = r13
            goto L_0x00b8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.apps.analytics.PersistentHitStore.loadExistingSession(android.database.sqlite.SQLiteDatabase):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x0133  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.android.apps.analytics.Event[] peekEvents(int r22, android.database.sqlite.SQLiteDatabase r23, int r24) {
        /*
            r21 = this;
            java.util.ArrayList r19 = new java.util.ArrayList
            r19.<init>()
            r13 = 0
            java.lang.String r5 = "events"
            r6 = 0
            r7 = 0
            r8 = 0
            r9 = 0
            r10 = 0
            java.lang.String r11 = "event_id"
            java.lang.String r12 = java.lang.Integer.toString(r22)     // Catch:{ SQLiteException -> 0x016c, all -> 0x0167 }
            r4 = r23
            android.database.Cursor r20 = r4.query(r5, r6, r7, r8, r9, r10, r11, r12)     // Catch:{ SQLiteException -> 0x016c, all -> 0x0167 }
        L_0x0019:
            boolean r4 = r20.moveToNext()     // Catch:{ SQLiteException -> 0x00e8, all -> 0x012e }
            if (r4 == 0) goto L_0x0150
            com.google.android.apps.analytics.Event r4 = new com.google.android.apps.analytics.Event     // Catch:{ SQLiteException -> 0x00e8, all -> 0x012e }
            r5 = 0
            r0 = r20
            r1 = r5
            long r5 = r0.getLong(r1)     // Catch:{ SQLiteException -> 0x00e8, all -> 0x012e }
            r7 = 2
            r0 = r20
            r1 = r7
            java.lang.String r7 = r0.getString(r1)     // Catch:{ SQLiteException -> 0x00e8, all -> 0x012e }
            r8 = 3
            r0 = r20
            r1 = r8
            int r8 = r0.getInt(r1)     // Catch:{ SQLiteException -> 0x00e8, all -> 0x012e }
            r9 = 4
            r0 = r20
            r1 = r9
            int r9 = r0.getInt(r1)     // Catch:{ SQLiteException -> 0x00e8, all -> 0x012e }
            r10 = 5
            r0 = r20
            r1 = r10
            int r10 = r0.getInt(r1)     // Catch:{ SQLiteException -> 0x00e8, all -> 0x012e }
            r11 = 6
            r0 = r20
            r1 = r11
            int r11 = r0.getInt(r1)     // Catch:{ SQLiteException -> 0x00e8, all -> 0x012e }
            r12 = 7
            r0 = r20
            r1 = r12
            int r12 = r0.getInt(r1)     // Catch:{ SQLiteException -> 0x00e8, all -> 0x012e }
            r13 = 8
            r0 = r20
            r1 = r13
            java.lang.String r13 = r0.getString(r1)     // Catch:{ SQLiteException -> 0x00e8, all -> 0x012e }
            r14 = 9
            r0 = r20
            r1 = r14
            java.lang.String r14 = r0.getString(r1)     // Catch:{ SQLiteException -> 0x00e8, all -> 0x012e }
            r15 = 10
            r0 = r20
            r1 = r15
            java.lang.String r15 = r0.getString(r1)     // Catch:{ SQLiteException -> 0x00e8, all -> 0x012e }
            r16 = 11
            r0 = r20
            r1 = r16
            int r16 = r0.getInt(r1)     // Catch:{ SQLiteException -> 0x00e8, all -> 0x012e }
            r17 = 12
            r0 = r20
            r1 = r17
            int r17 = r0.getInt(r1)     // Catch:{ SQLiteException -> 0x00e8, all -> 0x012e }
            r18 = 13
            r0 = r20
            r1 = r18
            int r18 = r0.getInt(r1)     // Catch:{ SQLiteException -> 0x00e8, all -> 0x012e }
            r4.<init>(r5, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18)     // Catch:{ SQLiteException -> 0x00e8, all -> 0x012e }
            r5 = 1
            r0 = r20
            r1 = r5
            int r5 = r0.getInt(r1)     // Catch:{ SQLiteException -> 0x00e8, all -> 0x012e }
            r4.setUserId(r5)     // Catch:{ SQLiteException -> 0x00e8, all -> 0x012e }
            java.lang.String r5 = "event_id"
            r0 = r20
            r1 = r5
            int r5 = r0.getColumnIndex(r1)     // Catch:{ SQLiteException -> 0x00e8, all -> 0x012e }
            r0 = r20
            r1 = r5
            long r5 = r0.getLong(r1)     // Catch:{ SQLiteException -> 0x00e8, all -> 0x012e }
            java.lang.String r7 = "__##GOOGLETRANSACTION##__"
            java.lang.String r8 = r4.category     // Catch:{ SQLiteException -> 0x00e8, all -> 0x012e }
            boolean r7 = r7.equals(r8)     // Catch:{ SQLiteException -> 0x00e8, all -> 0x012e }
            if (r7 == 0) goto L_0x00fd
            r0 = r21
            r1 = r5
            r3 = r23
            com.google.android.apps.analytics.Transaction r7 = r0.getTransaction(r1, r3)     // Catch:{ SQLiteException -> 0x00e8, all -> 0x012e }
            if (r7 != 0) goto L_0x00dd
            java.lang.String r8 = "GoogleAnalyticsTracker"
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x00e8, all -> 0x012e }
            r9.<init>()     // Catch:{ SQLiteException -> 0x00e8, all -> 0x012e }
            java.lang.String r10 = "missing expected transaction for event "
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ SQLiteException -> 0x00e8, all -> 0x012e }
            java.lang.StringBuilder r5 = r9.append(r5)     // Catch:{ SQLiteException -> 0x00e8, all -> 0x012e }
            java.lang.String r5 = r5.toString()     // Catch:{ SQLiteException -> 0x00e8, all -> 0x012e }
            android.util.Log.w(r8, r5)     // Catch:{ SQLiteException -> 0x00e8, all -> 0x012e }
        L_0x00dd:
            r4.setTransaction(r7)     // Catch:{ SQLiteException -> 0x00e8, all -> 0x012e }
        L_0x00e0:
            r0 = r19
            r1 = r4
            r0.add(r1)     // Catch:{ SQLiteException -> 0x00e8, all -> 0x012e }
            goto L_0x0019
        L_0x00e8:
            r4 = move-exception
            r5 = r20
        L_0x00eb:
            java.lang.String r6 = "GoogleAnalyticsTracker"
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x016a }
            android.util.Log.e(r6, r4)     // Catch:{ all -> 0x016a }
            r4 = 0
            com.google.android.apps.analytics.Event[] r4 = new com.google.android.apps.analytics.Event[r4]     // Catch:{ all -> 0x016a }
            if (r5 == 0) goto L_0x00fc
            r5.close()
        L_0x00fc:
            return r4
        L_0x00fd:
            java.lang.String r7 = "__##GOOGLEITEM##__"
            java.lang.String r8 = r4.category     // Catch:{ SQLiteException -> 0x00e8, all -> 0x012e }
            boolean r7 = r7.equals(r8)     // Catch:{ SQLiteException -> 0x00e8, all -> 0x012e }
            if (r7 == 0) goto L_0x0137
            r0 = r21
            r1 = r5
            r3 = r23
            com.google.android.apps.analytics.Item r7 = r0.getItem(r1, r3)     // Catch:{ SQLiteException -> 0x00e8, all -> 0x012e }
            if (r7 != 0) goto L_0x012a
            java.lang.String r8 = "GoogleAnalyticsTracker"
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x00e8, all -> 0x012e }
            r9.<init>()     // Catch:{ SQLiteException -> 0x00e8, all -> 0x012e }
            java.lang.String r10 = "missing expected item for event "
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ SQLiteException -> 0x00e8, all -> 0x012e }
            java.lang.StringBuilder r5 = r9.append(r5)     // Catch:{ SQLiteException -> 0x00e8, all -> 0x012e }
            java.lang.String r5 = r5.toString()     // Catch:{ SQLiteException -> 0x00e8, all -> 0x012e }
            android.util.Log.w(r8, r5)     // Catch:{ SQLiteException -> 0x00e8, all -> 0x012e }
        L_0x012a:
            r4.setItem(r7)     // Catch:{ SQLiteException -> 0x00e8, all -> 0x012e }
            goto L_0x00e0
        L_0x012e:
            r4 = move-exception
            r5 = r20
        L_0x0131:
            if (r5 == 0) goto L_0x0136
            r5.close()
        L_0x0136:
            throw r4
        L_0x0137:
            r7 = 1
            r0 = r24
            r1 = r7
            if (r0 <= r1) goto L_0x014a
            r0 = r21
            r1 = r5
            r3 = r23
            com.google.android.apps.analytics.CustomVariableBuffer r5 = r0.getCustomVariables(r1, r3)     // Catch:{ SQLiteException -> 0x00e8, all -> 0x012e }
        L_0x0146:
            r4.setCustomVariableBuffer(r5)     // Catch:{ SQLiteException -> 0x00e8, all -> 0x012e }
            goto L_0x00e0
        L_0x014a:
            com.google.android.apps.analytics.CustomVariableBuffer r5 = new com.google.android.apps.analytics.CustomVariableBuffer     // Catch:{ SQLiteException -> 0x00e8, all -> 0x012e }
            r5.<init>()     // Catch:{ SQLiteException -> 0x00e8, all -> 0x012e }
            goto L_0x0146
        L_0x0150:
            if (r20 == 0) goto L_0x0155
            r20.close()
        L_0x0155:
            int r4 = r19.size()
            com.google.android.apps.analytics.Event[] r4 = new com.google.android.apps.analytics.Event[r4]
            r0 = r19
            r1 = r4
            java.lang.Object[] r21 = r0.toArray(r1)
            com.google.android.apps.analytics.Event[] r21 = (com.google.android.apps.analytics.Event[]) r21
            r4 = r21
            goto L_0x00fc
        L_0x0167:
            r4 = move-exception
            r5 = r13
            goto L_0x0131
        L_0x016a:
            r4 = move-exception
            goto L_0x0131
        L_0x016c:
            r4 = move-exception
            r5 = r13
            goto L_0x00eb
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.apps.analytics.PersistentHitStore.peekEvents(int, android.database.sqlite.SQLiteDatabase, int):com.google.android.apps.analytics.Event[]");
    }

    public Hit[] peekHits() {
        return peekHits(MAX_HITS);
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0063  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.android.apps.analytics.Hit[] peekHits(int r13) {
        /*
            r12 = this;
            r10 = 0
            java.util.ArrayList r9 = new java.util.ArrayList
            r9.<init>()
            com.google.android.apps.analytics.PersistentHitStore$DataBaseHelper r0 = r12.databaseHelper     // Catch:{ SQLiteException -> 0x006e, all -> 0x005f }
            android.database.sqlite.SQLiteDatabase r0 = r0.getReadableDatabase()     // Catch:{ SQLiteException -> 0x006e, all -> 0x005f }
            java.lang.String r1 = "hits"
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            java.lang.String r7 = "hit_id"
            java.lang.String r8 = java.lang.Integer.toString(r13)     // Catch:{ SQLiteException -> 0x006e, all -> 0x005f }
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5, r6, r7, r8)     // Catch:{ SQLiteException -> 0x006e, all -> 0x005f }
        L_0x001d:
            boolean r1 = r0.moveToNext()     // Catch:{ SQLiteException -> 0x0036, all -> 0x0067 }
            if (r1 == 0) goto L_0x004c
            com.google.android.apps.analytics.Hit r1 = new com.google.android.apps.analytics.Hit     // Catch:{ SQLiteException -> 0x0036, all -> 0x0067 }
            r2 = 1
            java.lang.String r2 = r0.getString(r2)     // Catch:{ SQLiteException -> 0x0036, all -> 0x0067 }
            r3 = 0
            long r3 = r0.getLong(r3)     // Catch:{ SQLiteException -> 0x0036, all -> 0x0067 }
            r1.<init>(r2, r3)     // Catch:{ SQLiteException -> 0x0036, all -> 0x0067 }
            r9.add(r1)     // Catch:{ SQLiteException -> 0x0036, all -> 0x0067 }
            goto L_0x001d
        L_0x0036:
            r1 = move-exception
            r11 = r1
            r1 = r0
            r0 = r11
        L_0x003a:
            java.lang.String r2 = "GoogleAnalyticsTracker"
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x006c }
            android.util.Log.e(r2, r0)     // Catch:{ all -> 0x006c }
            r0 = 0
            com.google.android.apps.analytics.Hit[] r0 = new com.google.android.apps.analytics.Hit[r0]     // Catch:{ all -> 0x006c }
            if (r1 == 0) goto L_0x004b
            r1.close()
        L_0x004b:
            return r0
        L_0x004c:
            if (r0 == 0) goto L_0x0051
            r0.close()
        L_0x0051:
            int r0 = r9.size()
            com.google.android.apps.analytics.Hit[] r0 = new com.google.android.apps.analytics.Hit[r0]
            java.lang.Object[] r12 = r9.toArray(r0)
            com.google.android.apps.analytics.Hit[] r12 = (com.google.android.apps.analytics.Hit[]) r12
            r0 = r12
            goto L_0x004b
        L_0x005f:
            r0 = move-exception
            r1 = r10
        L_0x0061:
            if (r1 == 0) goto L_0x0066
            r1.close()
        L_0x0066:
            throw r0
        L_0x0067:
            r1 = move-exception
            r11 = r1
            r1 = r0
            r0 = r11
            goto L_0x0061
        L_0x006c:
            r0 = move-exception
            goto L_0x0061
        L_0x006e:
            r0 = move-exception
            r1 = r10
            goto L_0x003a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.apps.analytics.PersistentHitStore.peekHits(int):com.google.android.apps.analytics.Hit[]");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* access modifiers changed from: package-private */
    public void putCustomVariables(Event event, SQLiteDatabase sQLiteDatabase) {
        if (!"__##GOOGLEITEM##__".equals(event.category) && !"__##GOOGLETRANSACTION##__".equals(event.category)) {
            try {
                CustomVariableBuffer customVariableBuffer = event.getCustomVariableBuffer();
                if (this.useStoredVisitorVars) {
                    if (customVariableBuffer == null) {
                        customVariableBuffer = new CustomVariableBuffer();
                        event.setCustomVariableBuffer(customVariableBuffer);
                    }
                    CustomVariableBuffer visitorVarBuffer = getVisitorVarBuffer();
                    for (int i = 1; i <= 5; i++) {
                        CustomVariable customVariableAt = visitorVarBuffer.getCustomVariableAt(i);
                        CustomVariable customVariableAt2 = customVariableBuffer.getCustomVariableAt(i);
                        if (customVariableAt != null && customVariableAt2 == null) {
                            customVariableBuffer.setCustomVariable(customVariableAt);
                        }
                    }
                    this.useStoredVisitorVars = false;
                }
                if (customVariableBuffer != null) {
                    for (int i2 = 1; i2 <= 5; i2++) {
                        if (!customVariableBuffer.isIndexAvailable(i2)) {
                            CustomVariable customVariableAt3 = customVariableBuffer.getCustomVariableAt(i2);
                            ContentValues contentValues = new ContentValues();
                            contentValues.put(EVENT_ID, (Integer) 0);
                            contentValues.put(CUSTOMVAR_INDEX, Integer.valueOf(customVariableAt3.getIndex()));
                            contentValues.put(CUSTOMVAR_NAME, customVariableAt3.getName());
                            contentValues.put(CUSTOMVAR_SCOPE, Integer.valueOf(customVariableAt3.getScope()));
                            contentValues.put(CUSTOMVAR_VALUE, customVariableAt3.getValue());
                            sQLiteDatabase.update("custom_var_cache", contentValues, "cv_index = ?", new String[]{Integer.toString(customVariableAt3.getIndex())});
                        }
                    }
                }
            } catch (SQLiteException e) {
                Log.e(GoogleAnalyticsTracker.LOG_TAG, e.toString());
            }
        }
    }

    public void putEvent(Event event) {
        if (this.numStoredHits >= MAX_HITS) {
            Log.w(GoogleAnalyticsTracker.LOG_TAG, "Store full. Not storing last event.");
            return;
        }
        if (this.sampleRate != 100) {
            if ((event.getUserId() == -1 ? this.storeId : event.getUserId()) % 10000 >= this.sampleRate * 100) {
                if (GoogleAnalyticsTracker.getInstance().getDebug()) {
                    Log.v(GoogleAnalyticsTracker.LOG_TAG, "User has been sampled out. Aborting hit.");
                    return;
                }
                return;
            }
        }
        if (!this.sessionStarted) {
            storeUpdatedSession();
        }
        try {
            putEvent(event, this.databaseHelper.getWritableDatabase(), UPDATE_TIMESTAMP);
        } catch (SQLiteException e) {
            Log.e(GoogleAnalyticsTracker.LOG_TAG, "putEventOuter:" + e.toString());
        }
    }

    /* access modifiers changed from: package-private */
    public void putEvent(Event event, SQLiteDatabase sQLiteDatabase, boolean z) {
        try {
            if (!event.isSessionInitialized()) {
                event.setRandomVal((int) (Math.random() * 2.147483647E9d));
                event.setTimestampFirst((int) this.timestampFirst);
                event.setTimestampPrevious((int) this.timestampPrevious);
                event.setTimestampCurrent((int) this.timestampCurrent);
                event.setVisits(this.visits);
            }
            event.setAnonymizeIp(this.anonymizeIp);
            if (event.getUserId() == -1) {
                event.setUserId(this.storeId);
            }
            sQLiteDatabase.beginTransaction();
            putCustomVariables(event, sQLiteDatabase);
            ContentValues contentValues = new ContentValues();
            contentValues.put(HIT_STRING, HitBuilder.constructHitRequestPath(event, getReferrer(sQLiteDatabase, UPDATE_TIMESTAMP)));
            contentValues.put(HIT_TIMESTAMP, Long.valueOf(z ? System.currentTimeMillis() : 0));
            sQLiteDatabase.insert("hits", null, contentValues);
            this.numStoredHits++;
            sQLiteDatabase.setTransactionSuccessful();
            if (sQLiteDatabase != null) {
                sQLiteDatabase.endTransaction();
            }
        } catch (SQLiteException e) {
            Log.e(GoogleAnalyticsTracker.LOG_TAG, "putEventInner:" + e.toString());
            if (sQLiteDatabase != null) {
                sQLiteDatabase.endTransaction();
            }
        } catch (Throwable th) {
            if (sQLiteDatabase != null) {
                sQLiteDatabase.endTransaction();
            }
            throw th;
        }
    }

    public void setAnonymizeIp(boolean z) {
        this.anonymizeIp = z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
     arg types: [java.lang.String, long]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void} */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0058  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean setReferrer(java.lang.String r7) {
        /*
            r6 = this;
            r5 = 0
            r2 = 0
            java.lang.String r0 = formatReferrer(r7)
            if (r0 != 0) goto L_0x000a
            r0 = r5
        L_0x0009:
            return r0
        L_0x000a:
            com.google.android.apps.analytics.PersistentHitStore$DataBaseHelper r1 = r6.databaseHelper     // Catch:{ SQLiteException -> 0x0042, all -> 0x0054 }
            android.database.sqlite.SQLiteDatabase r1 = r1.getWritableDatabase()     // Catch:{ SQLiteException -> 0x0042, all -> 0x0054 }
            android.content.ContentValues r2 = new android.content.ContentValues     // Catch:{ SQLiteException -> 0x005e }
            r2.<init>()     // Catch:{ SQLiteException -> 0x005e }
            java.lang.String r3 = "referrer"
            r2.put(r3, r0)     // Catch:{ SQLiteException -> 0x005e }
            java.lang.String r0 = "timestamp_referrer"
            r3 = 0
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ SQLiteException -> 0x005e }
            r2.put(r0, r3)     // Catch:{ SQLiteException -> 0x005e }
            r1.beginTransaction()     // Catch:{ SQLiteException -> 0x005e }
            java.lang.String r0 = "referrer"
            r3 = 0
            r4 = 0
            r1.delete(r0, r3, r4)     // Catch:{ SQLiteException -> 0x005e }
            java.lang.String r0 = "referrer"
            r3 = 0
            r1.insert(r0, r3, r2)     // Catch:{ SQLiteException -> 0x005e }
            r1.setTransactionSuccessful()     // Catch:{ SQLiteException -> 0x005e }
            r6.startNewVisit()     // Catch:{ SQLiteException -> 0x005e }
            if (r1 == 0) goto L_0x0040
            r1.endTransaction()
        L_0x0040:
            r0 = 1
            goto L_0x0009
        L_0x0042:
            r0 = move-exception
            r1 = r2
        L_0x0044:
            java.lang.String r2 = "GoogleAnalyticsTracker"
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x005c }
            android.util.Log.e(r2, r0)     // Catch:{ all -> 0x005c }
            if (r1 == 0) goto L_0x0052
            r1.endTransaction()
        L_0x0052:
            r0 = r5
            goto L_0x0009
        L_0x0054:
            r0 = move-exception
            r1 = r2
        L_0x0056:
            if (r1 == 0) goto L_0x005b
            r1.endTransaction()
        L_0x005b:
            throw r0
        L_0x005c:
            r0 = move-exception
            goto L_0x0056
        L_0x005e:
            r0 = move-exception
            goto L_0x0044
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.apps.analytics.PersistentHitStore.setReferrer(java.lang.String):boolean");
    }

    public void setSampleRate(int i) {
        this.sampleRate = i;
    }

    public void startNewVisit() {
        this.sessionStarted = false;
        this.useStoredVisitorVars = UPDATE_TIMESTAMP;
        this.numStoredHits = getNumStoredHits();
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00a1  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void storeUpdatedSession() {
        /*
            r6 = this;
            r1 = 0
            com.google.android.apps.analytics.PersistentHitStore$DataBaseHelper r0 = r6.databaseHelper     // Catch:{ SQLiteException -> 0x00aa }
            android.database.sqlite.SQLiteDatabase r0 = r0.getWritableDatabase()     // Catch:{ SQLiteException -> 0x00aa }
            r0.beginTransaction()     // Catch:{ SQLiteException -> 0x008b, all -> 0x00a5 }
            java.lang.String r1 = "session"
            r2 = 0
            r3 = 0
            r0.delete(r1, r2, r3)     // Catch:{ SQLiteException -> 0x008b, all -> 0x00a5 }
            long r1 = r6.timestampFirst     // Catch:{ SQLiteException -> 0x008b, all -> 0x00a5 }
            r3 = 0
            int r1 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r1 != 0) goto L_0x0077
            long r1 = java.lang.System.currentTimeMillis()     // Catch:{ SQLiteException -> 0x008b, all -> 0x00a5 }
            r3 = 1000(0x3e8, double:4.94E-321)
            long r1 = r1 / r3
            r6.timestampFirst = r1     // Catch:{ SQLiteException -> 0x008b, all -> 0x00a5 }
            r6.timestampPrevious = r1     // Catch:{ SQLiteException -> 0x008b, all -> 0x00a5 }
            r6.timestampCurrent = r1     // Catch:{ SQLiteException -> 0x008b, all -> 0x00a5 }
            r1 = 1
            r6.visits = r1     // Catch:{ SQLiteException -> 0x008b, all -> 0x00a5 }
        L_0x0029:
            android.content.ContentValues r1 = new android.content.ContentValues     // Catch:{ SQLiteException -> 0x008b, all -> 0x00a5 }
            r1.<init>()     // Catch:{ SQLiteException -> 0x008b, all -> 0x00a5 }
            java.lang.String r2 = "timestamp_first"
            long r3 = r6.timestampFirst     // Catch:{ SQLiteException -> 0x008b, all -> 0x00a5 }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ SQLiteException -> 0x008b, all -> 0x00a5 }
            r1.put(r2, r3)     // Catch:{ SQLiteException -> 0x008b, all -> 0x00a5 }
            java.lang.String r2 = "timestamp_previous"
            long r3 = r6.timestampPrevious     // Catch:{ SQLiteException -> 0x008b, all -> 0x00a5 }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ SQLiteException -> 0x008b, all -> 0x00a5 }
            r1.put(r2, r3)     // Catch:{ SQLiteException -> 0x008b, all -> 0x00a5 }
            java.lang.String r2 = "timestamp_current"
            long r3 = r6.timestampCurrent     // Catch:{ SQLiteException -> 0x008b, all -> 0x00a5 }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ SQLiteException -> 0x008b, all -> 0x00a5 }
            r1.put(r2, r3)     // Catch:{ SQLiteException -> 0x008b, all -> 0x00a5 }
            java.lang.String r2 = "visits"
            int r3 = r6.visits     // Catch:{ SQLiteException -> 0x008b, all -> 0x00a5 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ SQLiteException -> 0x008b, all -> 0x00a5 }
            r1.put(r2, r3)     // Catch:{ SQLiteException -> 0x008b, all -> 0x00a5 }
            java.lang.String r2 = "store_id"
            int r3 = r6.storeId     // Catch:{ SQLiteException -> 0x008b, all -> 0x00a5 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ SQLiteException -> 0x008b, all -> 0x00a5 }
            r1.put(r2, r3)     // Catch:{ SQLiteException -> 0x008b, all -> 0x00a5 }
            java.lang.String r2 = "session"
            r3 = 0
            r0.insert(r2, r3, r1)     // Catch:{ SQLiteException -> 0x008b, all -> 0x00a5 }
            r0.setTransactionSuccessful()     // Catch:{ SQLiteException -> 0x008b, all -> 0x00a5 }
            r1 = 1
            r6.sessionStarted = r1     // Catch:{ SQLiteException -> 0x008b, all -> 0x00a5 }
            if (r0 == 0) goto L_0x0076
            r0.endTransaction()
        L_0x0076:
            return
        L_0x0077:
            long r1 = r6.timestampCurrent     // Catch:{ SQLiteException -> 0x008b, all -> 0x00a5 }
            r6.timestampPrevious = r1     // Catch:{ SQLiteException -> 0x008b, all -> 0x00a5 }
            long r1 = java.lang.System.currentTimeMillis()     // Catch:{ SQLiteException -> 0x008b, all -> 0x00a5 }
            r3 = 1000(0x3e8, double:4.94E-321)
            long r1 = r1 / r3
            r6.timestampCurrent = r1     // Catch:{ SQLiteException -> 0x008b, all -> 0x00a5 }
            int r1 = r6.visits     // Catch:{ SQLiteException -> 0x008b, all -> 0x00a5 }
            int r1 = r1 + 1
            r6.visits = r1     // Catch:{ SQLiteException -> 0x008b, all -> 0x00a5 }
            goto L_0x0029
        L_0x008b:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x008f:
            java.lang.String r2 = "GoogleAnalyticsTracker"
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x009e }
            android.util.Log.e(r2, r0)     // Catch:{ all -> 0x009e }
            if (r1 == 0) goto L_0x0076
            r1.endTransaction()
            goto L_0x0076
        L_0x009e:
            r0 = move-exception
        L_0x009f:
            if (r1 == 0) goto L_0x00a4
            r1.endTransaction()
        L_0x00a4:
            throw r0
        L_0x00a5:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x009f
        L_0x00aa:
            r0 = move-exception
            goto L_0x008f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.apps.analytics.PersistentHitStore.storeUpdatedSession():void");
    }
}
