package tictack.suc;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int back = 2130837504;
        public static final int backcool = 2130837505;
        public static final int btn = 2130837506;
        public static final int genial = 2130837507;
        public static final int matah = 2130837508;
        public static final int stalk = 2130837509;
        public static final int wood1 = 2130837510;
        public static final int xo = 2130837511;
    }

    public static final class id {
        public static final int about_content = 2131099648;
        public static final int button1 = 2131099649;
        public static final int button10 = 2131099665;
        public static final int button12 = 2131099666;
        public static final int button2 = 2131099650;
        public static final int button3 = 2131099651;
        public static final int button4 = 2131099657;
        public static final int button5 = 2131099658;
        public static final int button6 = 2131099659;
        public static final int button7 = 2131099661;
        public static final int button8 = 2131099662;
        public static final int button9 = 2131099663;
        public static final int frameLayout1 = 2131099667;
        public static final int gameOverlay = 2131099664;
        public static final int mainLayout = 2131099652;
        public static final int tableRow1 = 2131099655;
        public static final int tableRow2 = 2131099656;
        public static final int tableRow3 = 2131099660;
        public static final int textCard = 2131099654;
        public static final int textScore = 2131099653;
    }

    public static final class layout {
        public static final int about = 2130903040;
        public static final int difmenu = 2130903041;
        public static final int main = 2130903042;
        public static final int menu = 2130903043;
    }

    public static final class raw {
        public static final int clik = 2130968576;
        public static final int win = 2130968577;
    }

    public static final class string {
        public static final int app_name = 2131034113;
        public static final int hello = 2131034112;
    }
}
