package tictac.suc;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.media.MediaPlayer;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import tictack.suc.R;

public class GameOverlayView extends View {
    MediaPlayer mediaPlayer;
    int ok;
    private Button startButton;
    private Button stopButton;

    public GameOverlayView(Context context) {
        super(context);
    }

    public GameOverlayView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public GameOverlayView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        Paint paint = new Paint();
        paint.setStrokeCap(Paint.Cap.ROUND);
        paint.setColor(0);
        canvas.drawRect(0.0f, 0.0f, (float) getWidth(), (float) getHeight(), paint);
        if (this.startButton != null && this.stopButton != null) {
            int[] start = new int[2];
            int[] stop = new int[2];
            int[] origin = new int[2];
            getLocationOnScreen(origin);
            this.startButton.getLocationOnScreen(start);
            this.stopButton.getLocationOnScreen(stop);
            if (this.startButton.getTop() == this.stopButton.getTop()) {
                start[0] = start[0] + ((this.startButton.getWidth() / 2) - 40);
                start[1] = start[1] + (this.startButton.getHeight() / 2);
                stop[0] = stop[0] + (this.stopButton.getWidth() / 2) + 40;
                stop[1] = stop[1] + (this.stopButton.getHeight() / 2);
            } else if (this.startButton.getLeft() == this.stopButton.getLeft()) {
                start[0] = start[0] + (this.startButton.getWidth() / 2);
                start[1] = start[1] + ((this.startButton.getHeight() / 2) - 30);
                stop[0] = stop[0] + (this.stopButton.getWidth() / 2);
                stop[1] = stop[1] + (this.stopButton.getHeight() / 2) + 30;
            } else if (this.startButton.getTop() > this.stopButton.getTop()) {
                start[0] = start[0] + ((this.startButton.getWidth() / 2) - 30);
                start[1] = start[1] + ((this.startButton.getHeight() / 2) - 30);
                stop[0] = stop[0] + (this.stopButton.getWidth() / 2) + 30;
                stop[1] = stop[1] + (this.stopButton.getHeight() / 2) + 30;
            } else if (this.startButton.getTop() < this.stopButton.getTop()) {
                start[0] = start[0] + ((this.startButton.getWidth() / 2) - 30);
                start[1] = start[1] + (this.startButton.getHeight() / 2) + 30;
                stop[0] = stop[0] + (this.stopButton.getWidth() / 2) + 30;
                stop[1] = stop[1] + ((this.stopButton.getHeight() / 2) - 30);
            }
            start[0] = start[0] - origin[0];
            start[1] = start[1] - origin[1];
            stop[0] = stop[0] - origin[0];
            stop[1] = stop[1] - origin[1];
            paint.setColor(-570490765);
            paint.setStrokeWidth(26.0f);
            canvas.drawLine((float) start[0], (float) start[1], (float) stop[0], (float) stop[1], paint);
        }
    }

    /* access modifiers changed from: package-private */
    public void initMediaPlayer() {
        this.mediaPlayer = new MediaPlayer();
        this.mediaPlayer = MediaPlayer.create(getContext(), (int) R.raw.win);
    }

    /* access modifiers changed from: package-private */
    public void MP() {
        if (this.ok == 1) {
            this.mediaPlayer.release();
        }
    }

    /* access modifiers changed from: package-private */
    public void setStartStopButtons(Button startButton2, Button stopButton2) {
        this.startButton = startButton2;
        this.stopButton = stopButton2;
        invalidate();
        if (startButton2 != null && stopButton2 != null) {
            int ok2 = 0 + 1;
            this.mediaPlayer.start();
        }
    }
}
