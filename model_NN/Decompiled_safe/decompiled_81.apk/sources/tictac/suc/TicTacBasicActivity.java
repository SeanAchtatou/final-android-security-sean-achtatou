package tictac.suc;

import android.app.Activity;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import tictack.suc.R;

public class TicTacBasicActivity extends Activity implements View.OnClickListener {
    private AdView adView;
    Button button1;
    Button button10;
    Button button12;
    Button button2;
    Button button3;
    Button button4;
    Button button5;
    Button button6;
    Button button7;
    Button button8;
    Button button9;
    int cont;
    GameOverlayView gameOverlay;
    MediaPlayer mediaPlayer;
    int ok;
    TextView textCard;
    TextView textScore;
    int theO;
    int theX;
    GoogleAnalyticsTracker tracker;
    int won;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        this.adView = new AdView(this, AdSize.BANNER, "a14e425cee5dc4b");
        ((LinearLayout) findViewById(R.id.mainLayout)).addView(this.adView);
        this.adView.loadAd(new AdRequest());
        this.tracker = GoogleAnalyticsTracker.getInstance();
        this.tracker.startNewSession("UA-25176833-1", 5, this);
        this.tracker.trackPageView("/TwoPlayers");
        this.button1 = (Button) findViewById(R.id.button1);
        this.button2 = (Button) findViewById(R.id.button2);
        this.button3 = (Button) findViewById(R.id.button3);
        this.button4 = (Button) findViewById(R.id.button4);
        this.button5 = (Button) findViewById(R.id.button5);
        this.button6 = (Button) findViewById(R.id.button6);
        this.button7 = (Button) findViewById(R.id.button7);
        this.button8 = (Button) findViewById(R.id.button8);
        this.button9 = (Button) findViewById(R.id.button9);
        this.button10 = (Button) findViewById(R.id.button10);
        this.button12 = (Button) findViewById(R.id.button12);
        this.textScore = (TextView) findViewById(R.id.textScore);
        this.textCard = (TextView) findViewById(R.id.textCard);
        this.gameOverlay = (GameOverlayView) findViewById(R.id.gameOverlay);
        this.button1.setOnClickListener(this);
        this.button2.setOnClickListener(this);
        this.button3.setOnClickListener(this);
        this.button4.setOnClickListener(this);
        this.button5.setOnClickListener(this);
        this.button6.setOnClickListener(this);
        this.button7.setOnClickListener(this);
        this.button8.setOnClickListener(this);
        this.button9.setOnClickListener(this);
        this.button10.setOnClickListener(this);
        this.button12.setOnClickListener(this);
        this.ok = 0;
        this.textScore.setText("X's Turn ");
        this.won = 0;
        this.cont = 0;
        this.theX = 0;
        this.theO = 0;
        this.textCard.setText("X: " + this.theX + "     ||    O: " + this.theO);
        initMediaPlayer();
        this.gameOverlay.initMediaPlayer();
        DrawTable();
    }

    private void initMediaPlayer() {
        this.mediaPlayer = new MediaPlayer();
        this.mediaPlayer = MediaPlayer.create(this, (int) R.raw.clik);
        setVolumeControlStream(3);
    }

    private void DrawTable() {
        Canvas canvas = new Canvas();
        Paint paint = new Paint();
        paint.setStrokeCap(Paint.Cap.ROUND);
        paint.setColor(-838926221);
        canvas.drawLine(10.0f, 10.0f, 100.0f, 100.0f, paint);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        this.mediaPlayer.start();
        this.gameOverlay.ok = 1;
        this.gameOverlay.MP();
        this.mediaPlayer.release();
        this.tracker.stopSession();
        finish();
        return true;
    }

    /* JADX WARN: Type inference failed for: r6v0, types: [tictac.suc.TicTacBasicActivity] */
    public void onClick(View src) {
        switch (src.getId()) {
            case R.id.button1:
                this.mediaPlayer.start();
                if (this.ok < 1) {
                    this.button1.setText("X");
                    this.ok = 1;
                    this.button1.setClickable(false);
                    this.textScore.setText("O's Turn");
                } else {
                    this.button1.setText("O");
                    this.ok = 0;
                    this.button1.setClickable(false);
                    this.textScore.setText("X's Turn");
                }
                if ((this.button1.getText() == "X" && this.button2.getText() == "X" && this.button3.getText() == "X") || ((this.button4.getText() == "X" && this.button5.getText() == "X" && this.button6.getText() == "X") || ((this.button7.getText() == "X" && this.button8.getText() == "X" && this.button9.getText() == "X") || ((this.button1.getText() == "X" && this.button4.getText() == "X" && this.button7.getText() == "X") || ((this.button2.getText() == "X" && this.button5.getText() == "X" && this.button8.getText() == "X") || ((this.button3.getText() == "X" && this.button6.getText() == "X" && this.button9.getText() == "X") || ((this.button1.getText() == "X" && this.button5.getText() == "X" && this.button9.getText() == "X") || (this.button3.getText() == "X" && this.button5.getText() == "X" && this.button7.getText() == "X")))))))) {
                    this.textScore.setText("X Wins !");
                    this.theX++;
                    this.textCard.setText("X: " + this.theX + "     ||    O: " + this.theO);
                    this.button1.setClickable(false);
                    this.button2.setClickable(false);
                    this.button3.setClickable(false);
                    this.button4.setClickable(false);
                    this.button5.setClickable(false);
                    this.button6.setClickable(false);
                    this.button7.setClickable(false);
                    this.button8.setClickable(false);
                    this.button9.setClickable(false);
                    this.won = 1;
                } else {
                    this.cont++;
                }
                if ((this.button1.getText() == "O" && this.button2.getText() == "O" && this.button3.getText() == "O") || ((this.button4.getText() == "O" && this.button5.getText() == "O" && this.button6.getText() == "O") || ((this.button7.getText() == "O" && this.button8.getText() == "O" && this.button9.getText() == "O") || ((this.button1.getText() == "O" && this.button4.getText() == "O" && this.button7.getText() == "O") || ((this.button2.getText() == "O" && this.button5.getText() == "O" && this.button8.getText() == "O") || ((this.button3.getText() == "O" && this.button6.getText() == "O" && this.button9.getText() == "O") || ((this.button1.getText() == "O" && this.button5.getText() == "O" && this.button9.getText() == "O") || (this.button3.getText() == "O" && this.button5.getText() == "O" && this.button7.getText() == "O")))))))) {
                    this.textScore.setText("O Wins !");
                    this.theO++;
                    this.textCard.setText("X: " + this.theX + "     ||    O: " + this.theO);
                    this.button1.setClickable(false);
                    this.button2.setClickable(false);
                    this.button3.setClickable(false);
                    this.button4.setClickable(false);
                    this.button5.setClickable(false);
                    this.button6.setClickable(false);
                    this.button7.setClickable(false);
                    this.button8.setClickable(false);
                    this.button9.setClickable(false);
                    this.won = 1;
                } else {
                    this.cont++;
                }
                if (this.cont == 18) {
                    this.textScore.setText("Nobody Wins !");
                    this.button1.setClickable(false);
                    this.button2.setClickable(false);
                    this.button3.setClickable(false);
                    this.button4.setClickable(false);
                    this.button5.setClickable(false);
                    this.button6.setClickable(false);
                    this.button7.setClickable(false);
                    this.button8.setClickable(false);
                    this.button9.setClickable(false);
                }
                if ((this.button1.getText() == "X" && this.button2.getText() == "X" && this.button3.getText() == "X") || (this.button1.getText() == "O" && this.button2.getText() == "O" && this.button3.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button1, this.button3);
                    return;
                } else if ((this.button4.getText() == "X" && this.button5.getText() == "X" && this.button6.getText() == "X") || (this.button4.getText() == "O" && this.button5.getText() == "O" && this.button6.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button4, this.button6);
                    return;
                } else if ((this.button7.getText() == "X" && this.button8.getText() == "X" && this.button9.getText() == "X") || (this.button1.getText() == "O" && this.button7.getText() == "O" && this.button9.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button7, this.button9);
                    return;
                } else if ((this.button1.getText() == "X" && this.button4.getText() == "X" && this.button7.getText() == "X") || (this.button1.getText() == "O" && this.button4.getText() == "O" && this.button7.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button1, this.button7);
                    return;
                } else if ((this.button2.getText() == "X" && this.button5.getText() == "X" && this.button8.getText() == "X") || (this.button2.getText() == "O" && this.button5.getText() == "O" && this.button8.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button2, this.button8);
                    return;
                } else if ((this.button3.getText() == "X" && this.button6.getText() == "X" && this.button9.getText() == "X") || (this.button3.getText() == "O" && this.button6.getText() == "O" && this.button9.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button3, this.button9);
                    return;
                } else if ((this.button1.getText() == "X" && this.button5.getText() == "X" && this.button9.getText() == "X") || (this.button1.getText() == "O" && this.button5.getText() == "O" && this.button9.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button1, this.button9);
                    return;
                } else if ((this.button3.getText() == "X" && this.button5.getText() == "X" && this.button7.getText() == "X") || (this.button3.getText() == "O" && this.button5.getText() == "O" && this.button7.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button7, this.button3);
                    return;
                } else {
                    return;
                }
            case R.id.button2:
                this.mediaPlayer.start();
                if (this.ok < 1) {
                    this.button2.setText("X");
                    this.ok = 1;
                    this.button2.setClickable(false);
                    this.textScore.setText("O's Turn");
                } else {
                    this.button2.setText("O");
                    this.ok = 0;
                    this.button2.setClickable(false);
                    this.textScore.setText("X's Turn");
                }
                if ((this.button1.getText() == "X" && this.button2.getText() == "X" && this.button3.getText() == "X") || ((this.button4.getText() == "X" && this.button5.getText() == "X" && this.button6.getText() == "X") || ((this.button7.getText() == "X" && this.button8.getText() == "X" && this.button9.getText() == "X") || ((this.button1.getText() == "X" && this.button4.getText() == "X" && this.button7.getText() == "X") || ((this.button2.getText() == "X" && this.button5.getText() == "X" && this.button8.getText() == "X") || ((this.button3.getText() == "X" && this.button6.getText() == "X" && this.button9.getText() == "X") || ((this.button1.getText() == "X" && this.button5.getText() == "X" && this.button9.getText() == "X") || (this.button3.getText() == "X" && this.button5.getText() == "X" && this.button7.getText() == "X")))))))) {
                    this.textScore.setText("X Wins !");
                    this.theX++;
                    this.textCard.setText("X: " + this.theX + "     ||    O: " + this.theO);
                    this.button1.setClickable(false);
                    this.button2.setClickable(false);
                    this.button3.setClickable(false);
                    this.button4.setClickable(false);
                    this.button5.setClickable(false);
                    this.button6.setClickable(false);
                    this.button7.setClickable(false);
                    this.button8.setClickable(false);
                    this.button9.setClickable(false);
                    this.won = 1;
                } else {
                    this.cont++;
                }
                if ((this.button1.getText() == "O" && this.button2.getText() == "O" && this.button3.getText() == "O") || ((this.button4.getText() == "O" && this.button5.getText() == "O" && this.button6.getText() == "O") || ((this.button7.getText() == "O" && this.button8.getText() == "O" && this.button9.getText() == "O") || ((this.button1.getText() == "O" && this.button4.getText() == "O" && this.button7.getText() == "O") || ((this.button2.getText() == "O" && this.button5.getText() == "O" && this.button8.getText() == "O") || ((this.button3.getText() == "O" && this.button6.getText() == "O" && this.button9.getText() == "O") || ((this.button1.getText() == "O" && this.button5.getText() == "O" && this.button9.getText() == "O") || (this.button3.getText() == "O" && this.button5.getText() == "O" && this.button7.getText() == "O")))))))) {
                    this.textScore.setText("O Wins !");
                    this.theO++;
                    this.textCard.setText("X: " + this.theX + "     ||    O:" + this.theO);
                    this.button1.setClickable(false);
                    this.button2.setClickable(false);
                    this.button3.setClickable(false);
                    this.button4.setClickable(false);
                    this.button5.setClickable(false);
                    this.button6.setClickable(false);
                    this.button7.setClickable(false);
                    this.button8.setClickable(false);
                    this.button9.setClickable(false);
                    this.won = 1;
                } else {
                    this.cont++;
                }
                if (this.cont == 18) {
                    this.textScore.setText("Nobody Wins !");
                    this.button1.setClickable(false);
                    this.button2.setClickable(false);
                    this.button3.setClickable(false);
                    this.button4.setClickable(false);
                    this.button5.setClickable(false);
                    this.button6.setClickable(false);
                    this.button7.setClickable(false);
                    this.button8.setClickable(false);
                    this.button9.setClickable(false);
                }
                if ((this.button1.getText() == "X" && this.button2.getText() == "X" && this.button3.getText() == "X") || (this.button1.getText() == "O" && this.button2.getText() == "O" && this.button3.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button1, this.button3);
                    return;
                } else if ((this.button4.getText() == "X" && this.button5.getText() == "X" && this.button6.getText() == "X") || (this.button4.getText() == "O" && this.button5.getText() == "O" && this.button6.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button4, this.button6);
                    return;
                } else if ((this.button7.getText() == "X" && this.button8.getText() == "X" && this.button9.getText() == "X") || (this.button1.getText() == "O" && this.button7.getText() == "O" && this.button9.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button7, this.button9);
                    return;
                } else if ((this.button1.getText() == "X" && this.button4.getText() == "X" && this.button7.getText() == "X") || (this.button1.getText() == "O" && this.button4.getText() == "O" && this.button7.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button1, this.button7);
                    return;
                } else if ((this.button2.getText() == "X" && this.button5.getText() == "X" && this.button8.getText() == "X") || (this.button2.getText() == "O" && this.button5.getText() == "O" && this.button8.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button2, this.button8);
                    return;
                } else if ((this.button3.getText() == "X" && this.button6.getText() == "X" && this.button9.getText() == "X") || (this.button3.getText() == "O" && this.button6.getText() == "O" && this.button9.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button3, this.button9);
                    return;
                } else if ((this.button1.getText() == "X" && this.button5.getText() == "X" && this.button9.getText() == "X") || (this.button1.getText() == "O" && this.button5.getText() == "O" && this.button9.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button1, this.button9);
                    return;
                } else if ((this.button3.getText() == "X" && this.button5.getText() == "X" && this.button7.getText() == "X") || (this.button3.getText() == "O" && this.button5.getText() == "O" && this.button7.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button7, this.button3);
                    return;
                } else {
                    return;
                }
            case R.id.button3:
                this.mediaPlayer.start();
                if (this.ok < 1) {
                    this.button3.setText("X");
                    this.ok = 1;
                    this.button3.setClickable(false);
                    this.textScore.setText("O's Turn");
                } else {
                    this.button3.setText("O");
                    this.ok = 0;
                    this.button3.setClickable(false);
                    this.textScore.setText("X's Turn");
                }
                if ((this.button1.getText() == "X" && this.button2.getText() == "X" && this.button3.getText() == "X") || ((this.button4.getText() == "X" && this.button5.getText() == "X" && this.button6.getText() == "X") || ((this.button7.getText() == "X" && this.button8.getText() == "X" && this.button9.getText() == "X") || ((this.button1.getText() == "X" && this.button4.getText() == "X" && this.button7.getText() == "X") || ((this.button2.getText() == "X" && this.button5.getText() == "X" && this.button8.getText() == "X") || ((this.button3.getText() == "X" && this.button6.getText() == "X" && this.button9.getText() == "X") || ((this.button1.getText() == "X" && this.button5.getText() == "X" && this.button9.getText() == "X") || (this.button3.getText() == "X" && this.button5.getText() == "X" && this.button7.getText() == "X")))))))) {
                    this.textScore.setText("X Wins !");
                    this.theX++;
                    this.textCard.setText("X: " + this.theX + "     ||    O: " + this.theO);
                    this.button1.setClickable(false);
                    this.button2.setClickable(false);
                    this.button3.setClickable(false);
                    this.button4.setClickable(false);
                    this.button5.setClickable(false);
                    this.button6.setClickable(false);
                    this.button7.setClickable(false);
                    this.button8.setClickable(false);
                    this.button9.setClickable(false);
                    this.won = 1;
                } else {
                    this.cont++;
                }
                if ((this.button1.getText() == "O" && this.button2.getText() == "O" && this.button3.getText() == "O") || ((this.button4.getText() == "O" && this.button5.getText() == "O" && this.button6.getText() == "O") || ((this.button7.getText() == "O" && this.button8.getText() == "O" && this.button9.getText() == "O") || ((this.button1.getText() == "O" && this.button4.getText() == "O" && this.button7.getText() == "O") || ((this.button2.getText() == "O" && this.button5.getText() == "O" && this.button8.getText() == "O") || ((this.button3.getText() == "O" && this.button6.getText() == "O" && this.button9.getText() == "O") || ((this.button1.getText() == "O" && this.button5.getText() == "O" && this.button9.getText() == "O") || (this.button3.getText() == "O" && this.button5.getText() == "O" && this.button7.getText() == "O")))))))) {
                    this.textScore.setText("O Wins !");
                    this.theO++;
                    this.textCard.setText("X: " + this.theX + "     ||    O: " + this.theO);
                    this.button1.setClickable(false);
                    this.button2.setClickable(false);
                    this.button3.setClickable(false);
                    this.button4.setClickable(false);
                    this.button5.setClickable(false);
                    this.button6.setClickable(false);
                    this.button7.setClickable(false);
                    this.button8.setClickable(false);
                    this.button9.setClickable(false);
                    this.won = 1;
                } else {
                    this.cont++;
                }
                if (this.cont == 18) {
                    this.textScore.setText("Nobody Wins !");
                    this.button1.setClickable(false);
                    this.button2.setClickable(false);
                    this.button3.setClickable(false);
                    this.button4.setClickable(false);
                    this.button5.setClickable(false);
                    this.button6.setClickable(false);
                    this.button7.setClickable(false);
                    this.button8.setClickable(false);
                    this.button9.setClickable(false);
                }
                if ((this.button1.getText() == "X" && this.button2.getText() == "X" && this.button3.getText() == "X") || (this.button1.getText() == "O" && this.button2.getText() == "O" && this.button3.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button1, this.button3);
                    return;
                } else if ((this.button4.getText() == "X" && this.button5.getText() == "X" && this.button6.getText() == "X") || (this.button4.getText() == "O" && this.button5.getText() == "O" && this.button6.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button4, this.button6);
                    return;
                } else if ((this.button7.getText() == "X" && this.button8.getText() == "X" && this.button9.getText() == "X") || (this.button1.getText() == "O" && this.button7.getText() == "O" && this.button9.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button7, this.button9);
                    return;
                } else if ((this.button1.getText() == "X" && this.button4.getText() == "X" && this.button7.getText() == "X") || (this.button1.getText() == "O" && this.button4.getText() == "O" && this.button7.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button1, this.button7);
                    return;
                } else if ((this.button2.getText() == "X" && this.button5.getText() == "X" && this.button8.getText() == "X") || (this.button2.getText() == "O" && this.button5.getText() == "O" && this.button8.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button2, this.button8);
                    return;
                } else if ((this.button3.getText() == "X" && this.button6.getText() == "X" && this.button9.getText() == "X") || (this.button3.getText() == "O" && this.button6.getText() == "O" && this.button9.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button3, this.button9);
                    return;
                } else if ((this.button1.getText() == "X" && this.button5.getText() == "X" && this.button9.getText() == "X") || (this.button1.getText() == "O" && this.button5.getText() == "O" && this.button9.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button1, this.button9);
                    return;
                } else if ((this.button3.getText() == "X" && this.button5.getText() == "X" && this.button7.getText() == "X") || (this.button3.getText() == "O" && this.button5.getText() == "O" && this.button7.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button7, this.button3);
                    return;
                } else {
                    return;
                }
            case R.id.mainLayout:
            case R.id.textScore:
            case R.id.textCard:
            case R.id.tableRow1:
            case R.id.tableRow2:
            case R.id.tableRow3:
            case R.id.gameOverlay:
            default:
                return;
            case R.id.button4:
                this.mediaPlayer.start();
                if (this.ok < 1) {
                    this.button4.setText("X");
                    this.ok = 1;
                    this.button4.setClickable(false);
                    this.textScore.setText("O's Turn");
                } else {
                    this.button4.setText("O");
                    this.ok = 0;
                    this.button4.setClickable(false);
                    this.textScore.setText("X's Turn");
                }
                if ((this.button1.getText() == "X" && this.button2.getText() == "X" && this.button3.getText() == "X") || ((this.button4.getText() == "X" && this.button5.getText() == "X" && this.button6.getText() == "X") || ((this.button7.getText() == "X" && this.button8.getText() == "X" && this.button9.getText() == "X") || ((this.button1.getText() == "X" && this.button4.getText() == "X" && this.button7.getText() == "X") || ((this.button2.getText() == "X" && this.button5.getText() == "X" && this.button8.getText() == "X") || ((this.button3.getText() == "X" && this.button6.getText() == "X" && this.button9.getText() == "X") || ((this.button1.getText() == "X" && this.button5.getText() == "X" && this.button9.getText() == "X") || (this.button3.getText() == "X" && this.button5.getText() == "X" && this.button7.getText() == "X")))))))) {
                    this.textScore.setText("X Wins !");
                    this.theX++;
                    this.textCard.setText("X: " + this.theX + "     ||    O: " + this.theO);
                    this.button1.setClickable(false);
                    this.button2.setClickable(false);
                    this.button3.setClickable(false);
                    this.button4.setClickable(false);
                    this.button5.setClickable(false);
                    this.button6.setClickable(false);
                    this.button7.setClickable(false);
                    this.button8.setClickable(false);
                    this.button9.setClickable(false);
                    this.won = 1;
                } else {
                    this.cont++;
                }
                if ((this.button1.getText() == "O" && this.button2.getText() == "O" && this.button3.getText() == "O") || ((this.button4.getText() == "O" && this.button5.getText() == "O" && this.button6.getText() == "O") || ((this.button7.getText() == "O" && this.button8.getText() == "O" && this.button9.getText() == "O") || ((this.button1.getText() == "O" && this.button4.getText() == "O" && this.button7.getText() == "O") || ((this.button2.getText() == "O" && this.button5.getText() == "O" && this.button8.getText() == "O") || ((this.button3.getText() == "O" && this.button6.getText() == "O" && this.button9.getText() == "O") || ((this.button1.getText() == "O" && this.button5.getText() == "O" && this.button9.getText() == "O") || (this.button3.getText() == "O" && this.button5.getText() == "O" && this.button7.getText() == "O")))))))) {
                    this.textScore.setText("O Wins !");
                    this.theO++;
                    this.textCard.setText("X: " + this.theX + "     ||    O: " + this.theO);
                    this.button1.setClickable(false);
                    this.button2.setClickable(false);
                    this.button3.setClickable(false);
                    this.button4.setClickable(false);
                    this.button5.setClickable(false);
                    this.button6.setClickable(false);
                    this.button7.setClickable(false);
                    this.button8.setClickable(false);
                    this.button9.setClickable(false);
                    this.won = 1;
                } else {
                    this.cont++;
                }
                if (this.cont == 18) {
                    this.textScore.setText("Nobody Wins !");
                    this.button1.setClickable(false);
                    this.button2.setClickable(false);
                    this.button3.setClickable(false);
                    this.button4.setClickable(false);
                    this.button5.setClickable(false);
                    this.button6.setClickable(false);
                    this.button7.setClickable(false);
                    this.button8.setClickable(false);
                    this.button9.setClickable(false);
                }
                if ((this.button1.getText() == "X" && this.button2.getText() == "X" && this.button3.getText() == "X") || (this.button1.getText() == "O" && this.button2.getText() == "O" && this.button3.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button1, this.button3);
                    return;
                } else if ((this.button4.getText() == "X" && this.button5.getText() == "X" && this.button6.getText() == "X") || (this.button4.getText() == "O" && this.button5.getText() == "O" && this.button6.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button4, this.button6);
                    return;
                } else if ((this.button7.getText() == "X" && this.button8.getText() == "X" && this.button9.getText() == "X") || (this.button1.getText() == "O" && this.button7.getText() == "O" && this.button9.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button7, this.button9);
                    return;
                } else if ((this.button1.getText() == "X" && this.button4.getText() == "X" && this.button7.getText() == "X") || (this.button1.getText() == "O" && this.button4.getText() == "O" && this.button7.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button1, this.button7);
                    return;
                } else if ((this.button2.getText() == "X" && this.button5.getText() == "X" && this.button8.getText() == "X") || (this.button2.getText() == "O" && this.button5.getText() == "O" && this.button8.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button2, this.button8);
                    return;
                } else if ((this.button3.getText() == "X" && this.button6.getText() == "X" && this.button9.getText() == "X") || (this.button3.getText() == "O" && this.button6.getText() == "O" && this.button9.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button3, this.button9);
                    return;
                } else if ((this.button1.getText() == "X" && this.button5.getText() == "X" && this.button9.getText() == "X") || (this.button1.getText() == "O" && this.button5.getText() == "O" && this.button9.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button1, this.button9);
                    return;
                } else if ((this.button3.getText() == "X" && this.button5.getText() == "X" && this.button7.getText() == "X") || (this.button3.getText() == "O" && this.button5.getText() == "O" && this.button7.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button7, this.button3);
                    return;
                } else {
                    return;
                }
            case R.id.button5:
                this.mediaPlayer.start();
                if (this.ok < 1) {
                    this.button5.setText("X");
                    this.ok = 1;
                    this.button5.setClickable(false);
                    this.textScore.setText("O's Turn");
                } else {
                    this.button5.setText("O");
                    this.ok = 0;
                    this.button5.setClickable(false);
                    this.textScore.setText("X's Turn");
                }
                if ((this.button1.getText() == "X" && this.button2.getText() == "X" && this.button3.getText() == "X") || ((this.button4.getText() == "X" && this.button5.getText() == "X" && this.button6.getText() == "X") || ((this.button7.getText() == "X" && this.button8.getText() == "X" && this.button9.getText() == "X") || ((this.button1.getText() == "X" && this.button4.getText() == "X" && this.button7.getText() == "X") || ((this.button2.getText() == "X" && this.button5.getText() == "X" && this.button8.getText() == "X") || ((this.button3.getText() == "X" && this.button6.getText() == "X" && this.button9.getText() == "X") || ((this.button1.getText() == "X" && this.button5.getText() == "X" && this.button9.getText() == "X") || (this.button3.getText() == "X" && this.button5.getText() == "X" && this.button7.getText() == "X")))))))) {
                    this.textScore.setText("X Wins !");
                    this.theX++;
                    this.textCard.setText("X: " + this.theX + "     ||    O: " + this.theO);
                    this.button1.setClickable(false);
                    this.button2.setClickable(false);
                    this.button3.setClickable(false);
                    this.button4.setClickable(false);
                    this.button5.setClickable(false);
                    this.button6.setClickable(false);
                    this.button7.setClickable(false);
                    this.button8.setClickable(false);
                    this.button9.setClickable(false);
                    this.won = 1;
                } else {
                    this.cont++;
                }
                if ((this.button1.getText() == "O" && this.button2.getText() == "O" && this.button3.getText() == "O") || ((this.button4.getText() == "O" && this.button5.getText() == "O" && this.button6.getText() == "O") || ((this.button7.getText() == "O" && this.button8.getText() == "O" && this.button9.getText() == "O") || ((this.button1.getText() == "O" && this.button4.getText() == "O" && this.button7.getText() == "O") || ((this.button2.getText() == "O" && this.button5.getText() == "O" && this.button8.getText() == "O") || ((this.button3.getText() == "O" && this.button6.getText() == "O" && this.button9.getText() == "O") || ((this.button1.getText() == "O" && this.button5.getText() == "O" && this.button9.getText() == "O") || (this.button3.getText() == "O" && this.button5.getText() == "O" && this.button7.getText() == "O")))))))) {
                    this.textScore.setText("O Wins !");
                    this.theO++;
                    this.textCard.setText("X: " + this.theX + "     ||    O: " + this.theO);
                    this.button1.setClickable(false);
                    this.button2.setClickable(false);
                    this.button3.setClickable(false);
                    this.button4.setClickable(false);
                    this.button5.setClickable(false);
                    this.button6.setClickable(false);
                    this.button7.setClickable(false);
                    this.button8.setClickable(false);
                    this.button9.setClickable(false);
                    this.won = 1;
                } else {
                    this.cont++;
                }
                if (this.cont == 18) {
                    this.textScore.setText("Nobody Wins !");
                    this.button1.setClickable(false);
                    this.button2.setClickable(false);
                    this.button3.setClickable(false);
                    this.button4.setClickable(false);
                    this.button5.setClickable(false);
                    this.button6.setClickable(false);
                    this.button7.setClickable(false);
                    this.button8.setClickable(false);
                    this.button9.setClickable(false);
                }
                if ((this.button1.getText() == "X" && this.button2.getText() == "X" && this.button3.getText() == "X") || (this.button1.getText() == "O" && this.button2.getText() == "O" && this.button3.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button1, this.button3);
                    return;
                } else if ((this.button4.getText() == "X" && this.button5.getText() == "X" && this.button6.getText() == "X") || (this.button4.getText() == "O" && this.button5.getText() == "O" && this.button6.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button4, this.button6);
                    return;
                } else if ((this.button7.getText() == "X" && this.button8.getText() == "X" && this.button9.getText() == "X") || (this.button1.getText() == "O" && this.button7.getText() == "O" && this.button9.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button7, this.button9);
                    return;
                } else if ((this.button1.getText() == "X" && this.button4.getText() == "X" && this.button7.getText() == "X") || (this.button1.getText() == "O" && this.button4.getText() == "O" && this.button7.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button1, this.button7);
                    return;
                } else if ((this.button2.getText() == "X" && this.button5.getText() == "X" && this.button8.getText() == "X") || (this.button2.getText() == "O" && this.button5.getText() == "O" && this.button8.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button2, this.button8);
                    return;
                } else if ((this.button3.getText() == "X" && this.button6.getText() == "X" && this.button9.getText() == "X") || (this.button3.getText() == "O" && this.button6.getText() == "O" && this.button9.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button3, this.button9);
                    return;
                } else if ((this.button1.getText() == "X" && this.button5.getText() == "X" && this.button9.getText() == "X") || (this.button1.getText() == "O" && this.button5.getText() == "O" && this.button9.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button1, this.button9);
                    return;
                } else if ((this.button3.getText() == "X" && this.button5.getText() == "X" && this.button7.getText() == "X") || (this.button3.getText() == "O" && this.button5.getText() == "O" && this.button7.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button7, this.button3);
                    return;
                } else {
                    return;
                }
            case R.id.button6:
                this.mediaPlayer.start();
                if (this.ok < 1) {
                    this.button6.setText("X");
                    this.ok = 1;
                    this.button6.setClickable(false);
                    this.textScore.setText("O's Turn");
                } else {
                    this.button6.setText("O");
                    this.ok = 0;
                    this.button6.setClickable(false);
                    this.textScore.setText("X's Turn");
                }
                if ((this.button1.getText() == "X" && this.button2.getText() == "X" && this.button3.getText() == "X") || ((this.button4.getText() == "X" && this.button5.getText() == "X" && this.button6.getText() == "X") || ((this.button7.getText() == "X" && this.button8.getText() == "X" && this.button9.getText() == "X") || ((this.button1.getText() == "X" && this.button4.getText() == "X" && this.button7.getText() == "X") || ((this.button2.getText() == "X" && this.button5.getText() == "X" && this.button8.getText() == "X") || ((this.button3.getText() == "X" && this.button6.getText() == "X" && this.button9.getText() == "X") || ((this.button1.getText() == "X" && this.button5.getText() == "X" && this.button9.getText() == "X") || (this.button3.getText() == "X" && this.button5.getText() == "X" && this.button7.getText() == "X")))))))) {
                    this.textScore.setText("X Wins !");
                    this.theX++;
                    this.textCard.setText("X: " + this.theX + "     ||    O: " + this.theO);
                    this.button1.setClickable(false);
                    this.button2.setClickable(false);
                    this.button3.setClickable(false);
                    this.button4.setClickable(false);
                    this.button5.setClickable(false);
                    this.button6.setClickable(false);
                    this.button7.setClickable(false);
                    this.button8.setClickable(false);
                    this.button9.setClickable(false);
                    this.won = 1;
                } else {
                    this.cont++;
                }
                if ((this.button1.getText() == "O" && this.button2.getText() == "O" && this.button3.getText() == "O") || ((this.button4.getText() == "O" && this.button5.getText() == "O" && this.button6.getText() == "O") || ((this.button7.getText() == "O" && this.button8.getText() == "O" && this.button9.getText() == "O") || ((this.button1.getText() == "O" && this.button4.getText() == "O" && this.button7.getText() == "O") || ((this.button2.getText() == "O" && this.button5.getText() == "O" && this.button8.getText() == "O") || ((this.button3.getText() == "O" && this.button6.getText() == "O" && this.button9.getText() == "O") || ((this.button1.getText() == "O" && this.button5.getText() == "O" && this.button9.getText() == "O") || (this.button3.getText() == "O" && this.button5.getText() == "O" && this.button7.getText() == "O")))))))) {
                    this.textScore.setText("O Wins !");
                    this.theO++;
                    this.textCard.setText("X: " + this.theX + "     ||    O: " + this.theO);
                    this.button1.setClickable(false);
                    this.button2.setClickable(false);
                    this.button3.setClickable(false);
                    this.button4.setClickable(false);
                    this.button5.setClickable(false);
                    this.button6.setClickable(false);
                    this.button7.setClickable(false);
                    this.button8.setClickable(false);
                    this.button9.setClickable(false);
                    this.won = 1;
                } else {
                    this.cont++;
                }
                if (this.cont == 18) {
                    this.textScore.setText("Nobody Wins !");
                    this.button1.setClickable(false);
                    this.button2.setClickable(false);
                    this.button3.setClickable(false);
                    this.button4.setClickable(false);
                    this.button5.setClickable(false);
                    this.button6.setClickable(false);
                    this.button7.setClickable(false);
                    this.button8.setClickable(false);
                    this.button9.setClickable(false);
                }
                if ((this.button1.getText() == "X" && this.button2.getText() == "X" && this.button3.getText() == "X") || (this.button1.getText() == "O" && this.button2.getText() == "O" && this.button3.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button1, this.button3);
                    return;
                } else if ((this.button4.getText() == "X" && this.button5.getText() == "X" && this.button6.getText() == "X") || (this.button4.getText() == "O" && this.button5.getText() == "O" && this.button6.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button4, this.button6);
                    return;
                } else if ((this.button7.getText() == "X" && this.button8.getText() == "X" && this.button9.getText() == "X") || (this.button1.getText() == "O" && this.button7.getText() == "O" && this.button9.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button7, this.button9);
                    return;
                } else if ((this.button1.getText() == "X" && this.button4.getText() == "X" && this.button7.getText() == "X") || (this.button1.getText() == "O" && this.button4.getText() == "O" && this.button7.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button1, this.button7);
                    return;
                } else if ((this.button2.getText() == "X" && this.button5.getText() == "X" && this.button8.getText() == "X") || (this.button2.getText() == "O" && this.button5.getText() == "O" && this.button8.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button2, this.button8);
                    return;
                } else if ((this.button3.getText() == "X" && this.button6.getText() == "X" && this.button9.getText() == "X") || (this.button3.getText() == "O" && this.button6.getText() == "O" && this.button9.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button3, this.button9);
                    return;
                } else if ((this.button1.getText() == "X" && this.button5.getText() == "X" && this.button9.getText() == "X") || (this.button1.getText() == "O" && this.button5.getText() == "O" && this.button9.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button1, this.button9);
                    return;
                } else if ((this.button3.getText() == "X" && this.button5.getText() == "X" && this.button7.getText() == "X") || (this.button3.getText() == "O" && this.button5.getText() == "O" && this.button7.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button7, this.button3);
                    return;
                } else {
                    return;
                }
            case R.id.button7:
                this.mediaPlayer.start();
                if (this.ok < 1) {
                    this.button7.setText("X");
                    this.ok = 1;
                    this.button7.setClickable(false);
                    this.textScore.setText("O's Turn");
                } else {
                    this.button7.setText("O");
                    this.ok = 0;
                    this.button7.setClickable(false);
                    this.textScore.setText("X's Turn");
                }
                if ((this.button1.getText() == "X" && this.button2.getText() == "X" && this.button3.getText() == "X") || ((this.button4.getText() == "X" && this.button5.getText() == "X" && this.button6.getText() == "X") || ((this.button7.getText() == "X" && this.button8.getText() == "X" && this.button9.getText() == "X") || ((this.button1.getText() == "X" && this.button4.getText() == "X" && this.button7.getText() == "X") || ((this.button2.getText() == "X" && this.button5.getText() == "X" && this.button8.getText() == "X") || ((this.button3.getText() == "X" && this.button6.getText() == "X" && this.button9.getText() == "X") || ((this.button1.getText() == "X" && this.button5.getText() == "X" && this.button9.getText() == "X") || (this.button3.getText() == "X" && this.button5.getText() == "X" && this.button7.getText() == "X")))))))) {
                    this.textScore.setText("X Wins !");
                    this.theX++;
                    this.textCard.setText("X: " + this.theX + "     ||    O: " + this.theO);
                    this.button1.setClickable(false);
                    this.button2.setClickable(false);
                    this.button3.setClickable(false);
                    this.button4.setClickable(false);
                    this.button5.setClickable(false);
                    this.button6.setClickable(false);
                    this.button7.setClickable(false);
                    this.button8.setClickable(false);
                    this.button9.setClickable(false);
                    this.won = 1;
                } else {
                    this.cont++;
                }
                if ((this.button1.getText() == "O" && this.button2.getText() == "O" && this.button3.getText() == "O") || ((this.button4.getText() == "O" && this.button5.getText() == "O" && this.button6.getText() == "O") || ((this.button7.getText() == "O" && this.button8.getText() == "O" && this.button9.getText() == "O") || ((this.button1.getText() == "O" && this.button4.getText() == "O" && this.button7.getText() == "O") || ((this.button2.getText() == "O" && this.button5.getText() == "O" && this.button8.getText() == "O") || ((this.button3.getText() == "O" && this.button6.getText() == "O" && this.button9.getText() == "O") || ((this.button1.getText() == "O" && this.button5.getText() == "O" && this.button9.getText() == "O") || (this.button3.getText() == "O" && this.button5.getText() == "O" && this.button7.getText() == "O")))))))) {
                    this.textScore.setText("O Wins !");
                    this.theO++;
                    this.textCard.setText("X: " + this.theX + "     ||    O: " + this.theO);
                    this.button1.setClickable(false);
                    this.button2.setClickable(false);
                    this.button3.setClickable(false);
                    this.button4.setClickable(false);
                    this.button5.setClickable(false);
                    this.button6.setClickable(false);
                    this.button7.setClickable(false);
                    this.button8.setClickable(false);
                    this.button9.setClickable(false);
                    this.won = 1;
                } else {
                    this.cont++;
                }
                if (this.cont == 18) {
                    this.textScore.setText("Nobody Wins !");
                    this.button1.setClickable(false);
                    this.button2.setClickable(false);
                    this.button3.setClickable(false);
                    this.button4.setClickable(false);
                    this.button5.setClickable(false);
                    this.button6.setClickable(false);
                    this.button7.setClickable(false);
                    this.button8.setClickable(false);
                    this.button9.setClickable(false);
                }
                if ((this.button1.getText() == "X" && this.button2.getText() == "X" && this.button3.getText() == "X") || (this.button1.getText() == "O" && this.button2.getText() == "O" && this.button3.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button1, this.button3);
                    return;
                } else if ((this.button4.getText() == "X" && this.button5.getText() == "X" && this.button6.getText() == "X") || (this.button4.getText() == "O" && this.button5.getText() == "O" && this.button6.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button4, this.button6);
                    return;
                } else if ((this.button7.getText() == "X" && this.button8.getText() == "X" && this.button9.getText() == "X") || (this.button1.getText() == "O" && this.button7.getText() == "O" && this.button9.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button7, this.button9);
                    return;
                } else if ((this.button1.getText() == "X" && this.button4.getText() == "X" && this.button7.getText() == "X") || (this.button1.getText() == "O" && this.button4.getText() == "O" && this.button7.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button1, this.button7);
                    return;
                } else if ((this.button2.getText() == "X" && this.button5.getText() == "X" && this.button8.getText() == "X") || (this.button2.getText() == "O" && this.button5.getText() == "O" && this.button8.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button2, this.button8);
                    return;
                } else if ((this.button3.getText() == "X" && this.button6.getText() == "X" && this.button9.getText() == "X") || (this.button3.getText() == "O" && this.button6.getText() == "O" && this.button9.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button3, this.button9);
                    return;
                } else if ((this.button1.getText() == "X" && this.button5.getText() == "X" && this.button9.getText() == "X") || (this.button1.getText() == "O" && this.button5.getText() == "O" && this.button9.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button1, this.button9);
                    return;
                } else if ((this.button3.getText() == "X" && this.button5.getText() == "X" && this.button7.getText() == "X") || (this.button3.getText() == "O" && this.button5.getText() == "O" && this.button7.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button7, this.button3);
                    return;
                } else {
                    return;
                }
            case R.id.button8:
                this.mediaPlayer.start();
                if (this.ok < 1) {
                    this.button8.setText("X");
                    this.ok = 1;
                    this.button8.setClickable(false);
                    this.textScore.setText("O's Turn");
                } else {
                    this.button8.setText("O");
                    this.ok = 0;
                    this.button8.setClickable(false);
                    this.textScore.setText("X's Turn");
                }
                if ((this.button1.getText() == "X" && this.button2.getText() == "X" && this.button3.getText() == "X") || ((this.button4.getText() == "X" && this.button5.getText() == "X" && this.button6.getText() == "X") || ((this.button7.getText() == "X" && this.button8.getText() == "X" && this.button9.getText() == "X") || ((this.button1.getText() == "X" && this.button4.getText() == "X" && this.button7.getText() == "X") || ((this.button2.getText() == "X" && this.button5.getText() == "X" && this.button8.getText() == "X") || ((this.button3.getText() == "X" && this.button6.getText() == "X" && this.button9.getText() == "X") || ((this.button1.getText() == "X" && this.button5.getText() == "X" && this.button9.getText() == "X") || (this.button3.getText() == "X" && this.button5.getText() == "X" && this.button7.getText() == "X")))))))) {
                    this.textScore.setText("X Wins !");
                    this.theX++;
                    this.textCard.setText("X: " + this.theX + "     ||    O: " + this.theO);
                    this.button1.setClickable(false);
                    this.button2.setClickable(false);
                    this.button3.setClickable(false);
                    this.button4.setClickable(false);
                    this.button5.setClickable(false);
                    this.button6.setClickable(false);
                    this.button7.setClickable(false);
                    this.button8.setClickable(false);
                    this.button9.setClickable(false);
                    this.won = 1;
                } else {
                    this.cont++;
                }
                if ((this.button1.getText() == "O" && this.button2.getText() == "O" && this.button3.getText() == "O") || ((this.button4.getText() == "O" && this.button5.getText() == "O" && this.button6.getText() == "O") || ((this.button7.getText() == "O" && this.button8.getText() == "O" && this.button9.getText() == "O") || ((this.button1.getText() == "O" && this.button4.getText() == "O" && this.button7.getText() == "O") || ((this.button2.getText() == "O" && this.button5.getText() == "O" && this.button8.getText() == "O") || ((this.button3.getText() == "O" && this.button6.getText() == "O" && this.button9.getText() == "O") || ((this.button1.getText() == "O" && this.button5.getText() == "O" && this.button9.getText() == "O") || (this.button3.getText() == "O" && this.button5.getText() == "O" && this.button7.getText() == "O")))))))) {
                    this.textScore.setText("O Wins !");
                    this.theO++;
                    this.textCard.setText("X: " + this.theX + "     ||    O:" + this.theO);
                    this.button1.setClickable(false);
                    this.button2.setClickable(false);
                    this.button3.setClickable(false);
                    this.button4.setClickable(false);
                    this.button5.setClickable(false);
                    this.button6.setClickable(false);
                    this.button7.setClickable(false);
                    this.button8.setClickable(false);
                    this.button9.setClickable(false);
                    this.won = 1;
                } else {
                    this.cont++;
                }
                if (this.cont == 18) {
                    this.textScore.setText("Nobody Wins !");
                    this.button1.setClickable(false);
                    this.button2.setClickable(false);
                    this.button3.setClickable(false);
                    this.button4.setClickable(false);
                    this.button5.setClickable(false);
                    this.button6.setClickable(false);
                    this.button7.setClickable(false);
                    this.button8.setClickable(false);
                    this.button9.setClickable(false);
                }
                if ((this.button1.getText() == "X" && this.button2.getText() == "X" && this.button3.getText() == "X") || (this.button1.getText() == "O" && this.button2.getText() == "O" && this.button3.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button1, this.button3);
                    return;
                } else if ((this.button4.getText() == "X" && this.button5.getText() == "X" && this.button6.getText() == "X") || (this.button4.getText() == "O" && this.button5.getText() == "O" && this.button6.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button4, this.button6);
                    return;
                } else if ((this.button7.getText() == "X" && this.button8.getText() == "X" && this.button9.getText() == "X") || (this.button1.getText() == "O" && this.button7.getText() == "O" && this.button9.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button7, this.button9);
                    return;
                } else if ((this.button1.getText() == "X" && this.button4.getText() == "X" && this.button7.getText() == "X") || (this.button1.getText() == "O" && this.button4.getText() == "O" && this.button7.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button1, this.button7);
                    return;
                } else if ((this.button2.getText() == "X" && this.button5.getText() == "X" && this.button8.getText() == "X") || (this.button2.getText() == "O" && this.button5.getText() == "O" && this.button8.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button2, this.button8);
                    return;
                } else if ((this.button3.getText() == "X" && this.button6.getText() == "X" && this.button9.getText() == "X") || (this.button3.getText() == "O" && this.button6.getText() == "O" && this.button9.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button3, this.button9);
                    return;
                } else if ((this.button1.getText() == "X" && this.button5.getText() == "X" && this.button9.getText() == "X") || (this.button1.getText() == "O" && this.button5.getText() == "O" && this.button9.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button1, this.button9);
                    return;
                } else if ((this.button3.getText() == "X" && this.button5.getText() == "X" && this.button7.getText() == "X") || (this.button3.getText() == "O" && this.button5.getText() == "O" && this.button7.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button7, this.button3);
                    return;
                } else {
                    return;
                }
            case R.id.button9:
                this.mediaPlayer.start();
                if (this.ok < 1) {
                    this.button9.setText("X");
                    this.ok = 1;
                    this.button9.setClickable(false);
                    this.textScore.setText("O's Turn");
                } else {
                    this.button9.setText("O");
                    this.ok = 0;
                    this.button9.setClickable(false);
                    this.textScore.setText("X's Turn");
                }
                if ((this.button1.getText() == "X" && this.button2.getText() == "X" && this.button3.getText() == "X") || ((this.button4.getText() == "X" && this.button5.getText() == "X" && this.button6.getText() == "X") || ((this.button7.getText() == "X" && this.button8.getText() == "X" && this.button9.getText() == "X") || ((this.button1.getText() == "X" && this.button4.getText() == "X" && this.button7.getText() == "X") || ((this.button2.getText() == "X" && this.button5.getText() == "X" && this.button8.getText() == "X") || ((this.button3.getText() == "X" && this.button6.getText() == "X" && this.button9.getText() == "X") || ((this.button1.getText() == "X" && this.button5.getText() == "X" && this.button9.getText() == "X") || (this.button3.getText() == "X" && this.button5.getText() == "X" && this.button7.getText() == "X")))))))) {
                    this.textScore.setText("X Wins !");
                    this.theX++;
                    this.textCard.setText("X: " + this.theX + "     ||    O: " + this.theO);
                    this.button1.setClickable(false);
                    this.button2.setClickable(false);
                    this.button3.setClickable(false);
                    this.button4.setClickable(false);
                    this.button5.setClickable(false);
                    this.button6.setClickable(false);
                    this.button7.setClickable(false);
                    this.button8.setClickable(false);
                    this.button9.setClickable(false);
                    this.won = 1;
                } else {
                    this.cont++;
                }
                if ((this.button1.getText() == "O" && this.button2.getText() == "O" && this.button3.getText() == "O") || ((this.button4.getText() == "O" && this.button5.getText() == "O" && this.button6.getText() == "O") || ((this.button7.getText() == "O" && this.button8.getText() == "O" && this.button9.getText() == "O") || ((this.button1.getText() == "O" && this.button4.getText() == "O" && this.button7.getText() == "O") || ((this.button2.getText() == "O" && this.button5.getText() == "O" && this.button8.getText() == "O") || ((this.button3.getText() == "O" && this.button6.getText() == "O" && this.button9.getText() == "O") || ((this.button1.getText() == "O" && this.button5.getText() == "O" && this.button9.getText() == "O") || (this.button3.getText() == "O" && this.button5.getText() == "O" && this.button7.getText() == "O")))))))) {
                    this.textScore.setText("O Wins !");
                    this.theO++;
                    this.textCard.setText("X: " + this.theX + "     ||    O: " + this.theO);
                    this.button1.setClickable(false);
                    this.button2.setClickable(false);
                    this.button3.setClickable(false);
                    this.button4.setClickable(false);
                    this.button5.setClickable(false);
                    this.button6.setClickable(false);
                    this.button7.setClickable(false);
                    this.button8.setClickable(false);
                    this.button9.setClickable(false);
                    this.won = 1;
                } else {
                    this.cont++;
                }
                if (this.cont == 18) {
                    this.textScore.setText("Nobody Wins !");
                    this.button1.setClickable(false);
                    this.button2.setClickable(false);
                    this.button3.setClickable(false);
                    this.button4.setClickable(false);
                    this.button5.setClickable(false);
                    this.button6.setClickable(false);
                    this.button7.setClickable(false);
                    this.button8.setClickable(false);
                    this.button9.setClickable(false);
                }
                if ((this.button1.getText() == "X" && this.button2.getText() == "X" && this.button3.getText() == "X") || (this.button1.getText() == "O" && this.button2.getText() == "O" && this.button3.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button1, this.button3);
                    return;
                } else if ((this.button4.getText() == "X" && this.button5.getText() == "X" && this.button6.getText() == "X") || (this.button4.getText() == "O" && this.button5.getText() == "O" && this.button6.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button4, this.button6);
                    return;
                } else if ((this.button7.getText() == "X" && this.button8.getText() == "X" && this.button9.getText() == "X") || (this.button8.getText() == "O" && this.button7.getText() == "O" && this.button9.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button7, this.button9);
                    return;
                } else if ((this.button1.getText() == "X" && this.button4.getText() == "X" && this.button7.getText() == "X") || (this.button1.getText() == "O" && this.button4.getText() == "O" && this.button7.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button1, this.button7);
                    return;
                } else if ((this.button2.getText() == "X" && this.button5.getText() == "X" && this.button8.getText() == "X") || (this.button2.getText() == "O" && this.button5.getText() == "O" && this.button8.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button2, this.button8);
                    return;
                } else if ((this.button3.getText() == "X" && this.button6.getText() == "X" && this.button9.getText() == "X") || (this.button3.getText() == "O" && this.button6.getText() == "O" && this.button9.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button3, this.button9);
                    return;
                } else if ((this.button1.getText() == "X" && this.button5.getText() == "X" && this.button9.getText() == "X") || (this.button1.getText() == "O" && this.button5.getText() == "O" && this.button9.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button1, this.button9);
                    return;
                } else if ((this.button3.getText() == "X" && this.button5.getText() == "X" && this.button7.getText() == "X") || (this.button3.getText() == "O" && this.button5.getText() == "O" && this.button7.getText() == "O")) {
                    this.gameOverlay.setStartStopButtons(this.button7, this.button3);
                    return;
                } else {
                    return;
                }
            case R.id.button10:
                this.mediaPlayer.start();
                this.gameOverlay.setStartStopButtons(null, null);
                this.button1.setText(" ");
                this.button1.setClickable(true);
                this.button2.setText(" ");
                this.button2.setClickable(true);
                this.button3.setText(" ");
                this.button3.setClickable(true);
                this.button4.setText(" ");
                this.button4.setClickable(true);
                this.button5.setText(" ");
                this.button5.setClickable(true);
                this.button6.setText(" ");
                this.button6.setClickable(true);
                this.button7.setText(" ");
                this.button7.setClickable(true);
                this.button8.setText(" ");
                this.button8.setClickable(true);
                this.button9.setText(" ");
                this.button9.setClickable(true);
                this.textScore.setText("X's Turn");
                this.ok = 0;
                this.cont = 0;
                return;
            case R.id.button12:
                this.mediaPlayer.start();
                this.theX = 0;
                this.theO = 0;
                this.textCard.setText("X: " + this.theX + "     ||    O: " + this.theO);
                return;
        }
    }
}
