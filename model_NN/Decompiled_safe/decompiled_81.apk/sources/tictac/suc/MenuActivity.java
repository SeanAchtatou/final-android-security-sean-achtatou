package tictac.suc;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import tictack.suc.R;

public class MenuActivity extends Activity implements View.OnClickListener {
    private AdView adView;
    Button button1;
    Button button2;
    Button button3;
    Button button4;
    GameOverlayView gameOverlay;
    MediaPlayer mediaPlayer;
    GoogleAnalyticsTracker tracker;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.menu);
        this.adView = new AdView(this, AdSize.BANNER, "a14e425cee5dc4b");
        this.tracker = GoogleAnalyticsTracker.getInstance();
        this.tracker.startNewSession("UA-25176833-1", 5, this);
        this.tracker.trackPageView("/MainMenu");
        ((LinearLayout) findViewById(R.id.mainLayout)).addView(this.adView);
        this.adView.loadAd(new AdRequest());
        this.button2 = (Button) findViewById(R.id.button2);
        this.button2.setOnClickListener(this);
        this.button1 = (Button) findViewById(R.id.button1);
        this.button1.setOnClickListener(this);
        this.button3 = (Button) findViewById(R.id.button3);
        this.button3.setOnClickListener(this);
        this.button4 = (Button) findViewById(R.id.button4);
        this.button4.setOnClickListener(this);
        initMediaPlayer();
    }

    private void initMediaPlayer() {
        this.mediaPlayer = new MediaPlayer();
        this.mediaPlayer = MediaPlayer.create(this, (int) R.raw.clik);
        setVolumeControlStream(3);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        this.mediaPlayer.start();
        this.mediaPlayer.release();
        this.adView.destroy();
        finish();
        return true;
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button1 /*2131099649*/:
                this.mediaPlayer.start();
                startActivity(new Intent(this, DiffMenuActivity.class));
                return;
            case R.id.button2 /*2131099650*/:
                this.mediaPlayer.start();
                startActivity(new Intent(this, TicTacBasicActivity.class));
                return;
            case R.id.button3 /*2131099651*/:
                this.mediaPlayer.start();
                startActivity(new Intent(this, About.class));
                return;
            case R.id.mainLayout /*2131099652*/:
            case R.id.textScore /*2131099653*/:
            case R.id.textCard /*2131099654*/:
            case R.id.tableRow1 /*2131099655*/:
            case R.id.tableRow2 /*2131099656*/:
            default:
                return;
            case R.id.button4 /*2131099657*/:
                this.mediaPlayer.start();
                this.mediaPlayer.release();
                this.tracker.stopSession();
                finish();
                return;
        }
    }
}
