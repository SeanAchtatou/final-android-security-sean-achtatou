package tictac.suc;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import tictack.suc.R;

public class About extends Activity {
    GoogleAnalyticsTracker tracker;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.about);
        this.tracker = GoogleAnalyticsTracker.getInstance();
        this.tracker.startNewSession("UA-25176833-1", 5, this);
        this.tracker.trackPageView("/AboutScreen");
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        this.tracker.stopSession();
        finish();
        return true;
    }
}
