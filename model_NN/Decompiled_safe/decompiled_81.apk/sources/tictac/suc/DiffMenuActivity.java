package tictac.suc;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import tictack.suc.R;

public class DiffMenuActivity extends Activity implements View.OnClickListener {
    Button button1;
    Button button2;
    Button button3;
    GameOverlayView gameOverlay;
    MediaPlayer mediaPlayer;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.difmenu);
        this.button2 = (Button) findViewById(R.id.button2);
        this.button2.setOnClickListener(this);
        this.button1 = (Button) findViewById(R.id.button1);
        this.button1.setOnClickListener(this);
        this.button3 = (Button) findViewById(R.id.button3);
        this.button3.setOnClickListener(this);
        initMediaPlayer();
    }

    private void initMediaPlayer() {
        this.mediaPlayer = new MediaPlayer();
        this.mediaPlayer = MediaPlayer.create(this, (int) R.raw.clik);
        setVolumeControlStream(3);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        this.mediaPlayer.release();
        finish();
        return true;
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button1 /*2131099649*/:
                this.mediaPlayer.start();
                startActivity(new Intent(this, TicTacBasicActivityEasy.class));
                finish();
                return;
            case R.id.button2 /*2131099650*/:
                this.mediaPlayer.start();
                startActivity(new Intent(this, TicTacBasicActivity2.class));
                finish();
                return;
            case R.id.button3 /*2131099651*/:
                this.mediaPlayer.start();
                startActivity(new Intent(this, TicTacBasicActivityUnbeat.class));
                finish();
                return;
            default:
                return;
        }
    }
}
