package tictac.suc;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import java.util.Random;
import tictack.suc.R;

public class TicTacBasicActivityUnbeat extends Activity implements View.OnClickListener {
    private AdView adView;
    Button button1;
    Button button10;
    Button button12;
    Button button2;
    Button button3;
    Button button4;
    Button button5;
    Button button6;
    Button button7;
    Button button8;
    Button button9;
    int cont;
    GameOverlayView gameOverlay;
    MediaPlayer mediaPlayer;
    MediaPlayer mediaPlayer2;
    int ok;
    int step;
    TextView textCard;
    TextView textScore;
    int theO;
    int theX;
    GoogleAnalyticsTracker tracker;
    int won;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        this.adView = new AdView(this, AdSize.BANNER, "a14e425cee5dc4b");
        this.tracker = GoogleAnalyticsTracker.getInstance();
        this.tracker.startNewSession("UA-25176833-1", 5, this);
        this.tracker.trackPageView("/UnbeatDiff");
        ((LinearLayout) findViewById(R.id.mainLayout)).addView(this.adView);
        this.adView.loadAd(new AdRequest());
        this.step = 0;
        this.button1 = (Button) findViewById(R.id.button1);
        this.button2 = (Button) findViewById(R.id.button2);
        this.button3 = (Button) findViewById(R.id.button3);
        this.button4 = (Button) findViewById(R.id.button4);
        this.button5 = (Button) findViewById(R.id.button5);
        this.button6 = (Button) findViewById(R.id.button6);
        this.button7 = (Button) findViewById(R.id.button7);
        this.button8 = (Button) findViewById(R.id.button8);
        this.button9 = (Button) findViewById(R.id.button9);
        this.button10 = (Button) findViewById(R.id.button10);
        this.button12 = (Button) findViewById(R.id.button12);
        this.textScore = (TextView) findViewById(R.id.textScore);
        this.textCard = (TextView) findViewById(R.id.textCard);
        this.gameOverlay = (GameOverlayView) findViewById(R.id.gameOverlay);
        this.button1.setOnClickListener(this);
        this.button2.setOnClickListener(this);
        this.button3.setOnClickListener(this);
        this.button4.setOnClickListener(this);
        this.button5.setOnClickListener(this);
        this.button6.setOnClickListener(this);
        this.button7.setOnClickListener(this);
        this.button8.setOnClickListener(this);
        this.button9.setOnClickListener(this);
        this.button10.setOnClickListener(this);
        this.button12.setOnClickListener(this);
        this.ok = 0;
        this.textScore.setText("Human's Turn ");
        this.won = 0;
        this.cont = 0;
        this.theX = 0;
        this.theO = 0;
        this.textCard.setText("X: " + this.theX + "     ||    O: " + this.theO);
        initMediaPlayer();
        this.gameOverlay.initMediaPlayer();
        this.step = 0;
        this.won = 0;
        this.gameOverlay.setStartStopButtons(null, null);
        this.button1.setText(" ");
        this.button1.setClickable(true);
        this.button2.setText(" ");
        this.button2.setClickable(true);
        this.button3.setText(" ");
        this.button3.setClickable(true);
        this.button4.setText(" ");
        this.button4.setClickable(true);
        this.button5.setText(" ");
        this.button5.setClickable(true);
        this.button6.setText(" ");
        this.button6.setClickable(true);
        this.button7.setText(" ");
        this.button7.setClickable(true);
        this.button8.setText(" ");
        this.button8.setClickable(true);
        this.button9.setText(" ");
        this.button9.setClickable(true);
        this.textScore.setText("Human's Turn");
        this.ok = 0;
        this.cont = 0;
    }

    private void initMediaPlayer() {
        this.mediaPlayer = new MediaPlayer();
        this.mediaPlayer = MediaPlayer.create(this, (int) R.raw.clik);
        setVolumeControlStream(3);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        this.mediaPlayer.start();
        this.gameOverlay.ok = 1;
        this.gameOverlay.MP();
        this.mediaPlayer.release();
        this.tracker.stopSession();
        finish();
        return true;
    }

    public void check() {
        if (this.button1.getText() == "X" && this.button2.getText() == "X" && this.button3.getText() == "X") {
            this.textScore.setText("Human Won !");
            this.theX++;
            this.won = 1;
            freeze();
            this.gameOverlay.setStartStopButtons(this.button1, this.button3);
        } else if (this.button4.getText() == "X" && this.button5.getText() == "X" && this.button6.getText() == "X") {
            this.textScore.setText("Human Won !");
            this.theX++;
            this.won = 1;
            freeze();
            this.gameOverlay.setStartStopButtons(this.button4, this.button6);
        } else if (this.button7.getText() == "X" && this.button8.getText() == "X" && this.button9.getText() == "X") {
            this.textScore.setText("Human Won !");
            this.theX++;
            this.won = 1;
            freeze();
            this.gameOverlay.setStartStopButtons(this.button7, this.button9);
        } else if (this.button4.getText() == "X" && this.button1.getText() == "X" && this.button7.getText() == "X") {
            this.textScore.setText("Human Won !");
            this.theX++;
            this.won = 1;
            freeze();
            this.gameOverlay.setStartStopButtons(this.button1, this.button7);
        } else if (this.button2.getText() == "X" && this.button5.getText() == "X" && this.button8.getText() == "X") {
            this.textScore.setText("Human Won !");
            this.theX++;
            this.won = 1;
            freeze();
            this.gameOverlay.setStartStopButtons(this.button2, this.button8);
        } else if (this.button3.getText() == "X" && this.button6.getText() == "X" && this.button9.getText() == "X") {
            this.textScore.setText("Human Won !");
            this.theX++;
            this.won = 1;
            freeze();
            this.gameOverlay.setStartStopButtons(this.button3, this.button9);
        } else if (this.button1.getText() == "X" && this.button5.getText() == "X" && this.button9.getText() == "X") {
            this.textScore.setText("Human Won !");
            this.theX++;
            this.won = 1;
            freeze();
            this.gameOverlay.setStartStopButtons(this.button1, this.button9);
        } else if (this.button3.getText() == "X" && this.button5.getText() == "X" && this.button7.getText() == "X") {
            this.textScore.setText("Human Won !");
            this.theX++;
            this.won = 1;
            freeze();
            this.gameOverlay.setStartStopButtons(this.button7, this.button3);
        } else {
            Att();
        }
    }

    public void non() {
        if (this.step == 5 && this.won == 0) {
            this.textScore.setText("Nobody Won");
            freeze();
            return;
        }
        check();
    }

    public void freeze() {
        this.button1.setClickable(false);
        this.button2.setClickable(false);
        this.button3.setClickable(false);
        this.button4.setClickable(false);
        this.button5.setClickable(false);
        this.button6.setClickable(false);
        this.button7.setClickable(false);
        this.button8.setClickable(false);
        this.button9.setClickable(false);
        this.textCard.setText("X: " + this.theX + "     ||    O: " + this.theO);
    }

    public void Att() {
        if (this.button7.getText() == "O" && this.button9.getText() == "O" && this.button8.getText().equals(" ")) {
            this.button8.setText("O");
            this.button8.setClickable(false);
            this.gameOverlay.setStartStopButtons(this.button7, this.button9);
            this.textScore.setText("Machine Won");
            this.theO++;
            freeze();
        } else if (this.button7.getText() == "O" && this.button8.getText() == "O" && this.button9.getText().equals(" ")) {
            this.button9.setText("O");
            this.button9.setClickable(false);
            this.gameOverlay.setStartStopButtons(this.button7, this.button9);
            this.textScore.setText("Computer Won");
            this.theO++;
            freeze();
        } else if (this.button8.getText() == "O" && this.button9.getText() == "O" && this.button7.getText().equals(" ")) {
            this.button7.setText("O");
            this.button7.setClickable(false);
            this.gameOverlay.setStartStopButtons(this.button7, this.button9);
            this.textScore.setText("Computer Won");
            this.theO++;
            freeze();
        } else if (this.button4.getText() == "O" && this.button6.getText() == "O" && this.button5.getText().equals(" ")) {
            this.button5.setText("O");
            this.button5.setClickable(false);
            this.textScore.setText("Computer Won");
            this.theO++;
            freeze();
            this.gameOverlay.setStartStopButtons(this.button4, this.button6);
        } else if (this.button4.getText() == "O" && this.button5.getText() == "O" && this.button6.getText().equals(" ")) {
            this.button6.setText("O");
            this.button6.setClickable(false);
            this.textScore.setText("Computer Won");
            this.theO++;
            freeze();
            this.gameOverlay.setStartStopButtons(this.button4, this.button6);
        } else if (this.button5.getText() == "O" && this.button6.getText() == "O" && this.button4.getText().equals(" ")) {
            this.button4.setText("O");
            this.button4.setClickable(false);
            this.textScore.setText("Computer Won");
            this.theO++;
            freeze();
            this.gameOverlay.setStartStopButtons(this.button4, this.button6);
        } else if (this.button3.getText() == "O" && this.button6.getText() == "O" && this.button9.getText().equals(" ")) {
            this.button9.setText("O");
            this.button9.setClickable(false);
            this.textScore.setText("Computer Won");
            this.theO++;
            freeze();
            this.gameOverlay.setStartStopButtons(this.button3, this.button9);
        } else if (this.button6.getText() == "O" && this.button9.getText() == "O" && this.button3.getText().equals(" ")) {
            this.button3.setText("O");
            this.button3.setClickable(false);
            this.textScore.setText("Computer Won");
            this.theO++;
            freeze();
            this.gameOverlay.setStartStopButtons(this.button3, this.button9);
        } else if (this.button3.getText() == "O" && this.button9.getText() == "O" && this.button6.getText().equals(" ")) {
            this.button6.setText("O");
            this.button6.setClickable(false);
            this.textScore.setText("Computer Won");
            this.theO++;
            freeze();
            this.gameOverlay.setStartStopButtons(this.button3, this.button9);
        } else if (this.button2.getText() == "O" && this.button5.getText() == "O" && this.button8.getText().equals(" ")) {
            this.button8.setText("O");
            this.textScore.setText("Computer Won");
            this.theO++;
            freeze();
            this.button8.setClickable(false);
            this.gameOverlay.setStartStopButtons(this.button2, this.button8);
        } else if (this.button2.getText() == "O" && this.button8.getText() == "O" && this.button5.getText().equals(" ")) {
            this.button5.setText("O");
            this.button5.setClickable(false);
            this.textScore.setText("Computer Won");
            this.theO++;
            freeze();
            this.gameOverlay.setStartStopButtons(this.button2, this.button8);
        } else if (this.button5.getText() == "O" && this.button8.getText() == "O" && this.button2.getText().equals(" ")) {
            this.button2.setText("O");
            this.textScore.setText("Computer Won");
            this.theO++;
            freeze();
            this.button2.setClickable(false);
            this.gameOverlay.setStartStopButtons(this.button2, this.button8);
        } else if (this.button3.getText() == "O" && this.button5.getText() == "O" && this.button7.getText().equals(" ")) {
            this.button7.setText("O");
            this.textScore.setText("Computer Won");
            this.theO++;
            freeze();
            this.button7.setClickable(false);
            this.gameOverlay.setStartStopButtons(this.button7, this.button3);
        } else if (this.button7.getText() == "O" && this.button3.getText() == "O" && this.button5.getText().equals(" ")) {
            this.button5.setText("O");
            this.textScore.setText("Computer Won");
            this.theO++;
            freeze();
            this.button5.setClickable(false);
            this.gameOverlay.setStartStopButtons(this.button3, this.button7);
        } else if (this.button7.getText() == "O" && this.button5.getText() == "O" && this.button3.getText().equals(" ")) {
            this.button3.setText("O");
            this.textScore.setText("Computer Won");
            this.theO++;
            freeze();
            this.button3.setClickable(false);
            this.gameOverlay.setStartStopButtons(this.button7, this.button3);
        } else if (this.button1.getText() == "O" && this.button3.getText() == "O" && this.button2.getText().equals(" ")) {
            this.button2.setText("O");
            this.textScore.setText("Computer Won");
            this.theO++;
            freeze();
            this.button2.setClickable(false);
            this.gameOverlay.setStartStopButtons(this.button1, this.button3);
        } else if (this.button2.getText() == "O" && this.button3.getText() == "O" && this.button1.getText().equals(" ")) {
            this.button1.setText("O");
            this.textScore.setText("Computer Won");
            this.theO++;
            freeze();
            this.button1.setClickable(false);
            this.gameOverlay.setStartStopButtons(this.button1, this.button3);
        } else if (this.button1.getText() == "O" && this.button2.getText() == "O" && this.button3.getText().equals(" ")) {
            this.button3.setText("O");
            this.textScore.setText("Computer Won");
            this.theO++;
            freeze();
            this.button3.setClickable(false);
            this.gameOverlay.setStartStopButtons(this.button1, this.button3);
        } else if (this.button1.getText() == "O" && this.button5.getText() == "O" && this.button9.getText().equals(" ")) {
            this.button9.setText("O");
            this.textScore.setText("Computer Won");
            this.theO++;
            freeze();
            this.button9.setClickable(false);
            this.gameOverlay.setStartStopButtons(this.button1, this.button9);
        } else if (this.button1.getText() == "O" && this.button9.getText() == "O" && this.button5.getText().equals(" ")) {
            this.button5.setText("O");
            this.textScore.setText("Computer Won");
            this.theO++;
            freeze();
            this.button5.setClickable(false);
            this.gameOverlay.setStartStopButtons(this.button1, this.button9);
        } else if (this.button5.getText() == "O" && this.button9.getText() == "O" && this.button1.getText().equals(" ")) {
            this.button1.setText("O");
            this.textScore.setText("Computer Won");
            this.theO++;
            freeze();
            this.button1.setClickable(false);
            this.gameOverlay.setStartStopButtons(this.button1, this.button9);
        } else if (this.button1.getText() == "O" && this.button4.getText() == "O" && this.button7.getText().equals(" ")) {
            this.textScore.setText("Computer Won");
            this.theO++;
            freeze();
            this.button7.setText("O");
            this.button7.setClickable(false);
            this.gameOverlay.setStartStopButtons(this.button1, this.button7);
        } else if (this.button1.getText() == "O" && this.button7.getText() == "O" && this.button4.getText().equals(" ")) {
            this.button4.setText("O");
            this.textScore.setText("Computer Won");
            this.theO++;
            freeze();
            this.button4.setClickable(false);
            this.gameOverlay.setStartStopButtons(this.button1, this.button7);
        } else if (this.button4.getText() == "O" && this.button7.getText() == "O" && this.button1.getText().equals(" ")) {
            this.button1.setText("O");
            this.button1.setClickable(false);
            this.textScore.setText("Computer Won");
            this.theO++;
            freeze();
            this.gameOverlay.setStartStopButtons(this.button1, this.button7);
        } else {
            Def();
        }
    }

    public void Def() {
        if (this.button1.getText() == "X" && this.button2.getText() == "X" && this.button3.getText().equals(" ")) {
            this.button3.setText("O");
            this.button3.setClickable(false);
        } else if (this.button1.getText() == "X" && this.button3.getText() == "X" && this.button2.getText().equals(" ")) {
            this.button2.setText("O");
            this.button2.setClickable(false);
        } else if (this.button2.getText() == "X" && this.button3.getText() == "X" && this.button1.getText().equals(" ")) {
            this.button1.setText("O");
            this.button1.setClickable(false);
        } else if (this.button4.getText() == "X" && this.button5.getText() == "X" && this.button6.getText().equals(" ")) {
            this.button6.setText("O");
            this.button6.setClickable(false);
        } else if (this.button4.getText() == "X" && this.button6.getText() == "X" && this.button5.getText().equals(" ")) {
            this.button5.setText("O");
            this.button5.setClickable(false);
        } else if (this.button5.getText() == "X" && this.button6.getText() == "X" && this.button4.getText().equals(" ")) {
            this.button4.setText("O");
            this.button4.setClickable(false);
        } else if (this.button7.getText() == "X" && this.button8.getText() == "X" && this.button9.getText().equals(" ")) {
            this.button9.setText("O");
            this.button9.setClickable(false);
        } else if (this.button7.getText() == "X" && this.button9.getText() == "X" && this.button8.getText().equals(" ")) {
            this.button8.setText("O");
            this.button8.setClickable(false);
        } else if (this.button8.getText() == "X" && this.button9.getText() == "X" && this.button7.getText().equals(" ")) {
            this.button7.setText("O");
            this.button7.setClickable(false);
        } else if (this.button1.getText() == "X" && this.button4.getText() == "X" && this.button7.getText().equals(" ")) {
            this.button7.setText("O");
            this.button7.setClickable(false);
        } else if (this.button1.getText() == "X" && this.button7.getText() == "X" && this.button4.getText().equals(" ")) {
            this.button4.setText("O");
            this.button4.setClickable(false);
        } else if (this.button4.getText() == "X" && this.button7.getText() == "X" && this.button1.getText().equals(" ")) {
            this.button1.setText("O");
            this.button1.setClickable(false);
        } else if (this.button2.getText() == "X" && this.button5.getText() == "X" && this.button8.getText().equals(" ")) {
            this.button8.setText("O");
            this.button8.setClickable(false);
        } else if (this.button2.getText() == "X" && this.button8.getText() == "X" && this.button5.getText().equals(" ")) {
            this.button5.setText("O");
            this.button5.setClickable(false);
        } else if (this.button5.getText() == "X" && this.button8.getText() == "X" && this.button2.getText().equals(" ")) {
            this.button2.setText("O");
            this.button2.setClickable(false);
        } else if (this.button3.getText() == "X" && this.button6.getText() == "X" && this.button9.getText().equals(" ")) {
            this.button9.setText("O");
            this.button9.setClickable(false);
        } else if (this.button3.getText() == "X" && this.button9.getText() == "X" && this.button6.getText().equals(" ")) {
            this.button6.setText("O");
            this.button6.setClickable(false);
        } else if (this.button6.getText() == "X" && this.button9.getText() == "X" && this.button3.getText().equals(" ")) {
            this.button3.setText("O");
            this.button3.setClickable(false);
        } else if (this.button1.getText() == "X" && this.button5.getText() == "X" && this.button9.getText().equals(" ")) {
            this.button9.setText("O");
            this.button9.setClickable(false);
        } else if (this.button1.getText() == "X" && this.button9.getText() == "X" && this.button5.getText().equals(" ")) {
            this.button5.setText("O");
            this.button5.setClickable(false);
        } else if (this.button5.getText() == "X" && this.button9.getText() == "X" && this.button1.getText().equals(" ")) {
            this.button1.setText("O");
            this.button1.setClickable(false);
        } else if (this.button3.getText() == "X" && this.button5.getText() == "X" && this.button7.getText().equals(" ")) {
            this.button7.setText("O");
            this.button7.setClickable(false);
        } else if (this.button3.getText() == "X" && this.button7.getText() == "X" && this.button5.getText().equals(" ")) {
            this.button5.setText("O");
            this.button5.setClickable(false);
        } else if (this.button5.getText() == "X" && this.button7.getText() == "X" && this.button3.getText().equals(" ")) {
            this.button3.setText("O");
            this.button3.setClickable(false);
        } else {
            Rand();
        }
    }

    public void Rand() {
        Random generator = new Random();
        int caz = generator.nextInt(9) + 1;
        int apar = generator.nextInt(2) + 1;
        int wtf = generator.nextInt(4) + 1;
        if ((this.button2.getText().equals("X") || this.button8.getText().equals("X")) && this.button6.getText().equals("X") && ((this.button7.getText().equals("X") || this.button9.getText().equals("X")) && this.button3.getText().equals(" "))) {
            this.button3.setText("O");
            this.button3.setClickable(false);
        } else if (this.button2.getText().equals("X") && this.button4.getText().equals("X") && this.button1.getText().equals(" ")) {
            this.button1.setText("O");
            this.button1.setClickable(false);
        } else if (this.button2.getText().equals("X") && this.button6.getText().equals("X") && this.button3.getText().equals(" ")) {
            this.button3.setText("O");
            this.button3.setClickable(false);
        } else if (this.button8.getText().equals("X") && this.button4.getText().equals("X") && this.button7.getText().equals(" ")) {
            this.button7.setText("O");
            this.button7.setClickable(false);
        } else if (this.button8.getText().equals("X") && this.button6.getText().equals("X") && this.button9.getText().equals(" ")) {
            this.button9.setText("O");
            this.button9.setClickable(false);
        } else if ((this.button2.getText().equals("X") || this.button8.getText().equals("X")) && this.button6.getText().equals("X") && ((this.button7.getText().equals("X") || this.button9.getText().equals("X")) && this.button3.getText().equals(" "))) {
            this.button3.setText("O");
            this.button3.setClickable(false);
        } else if ((this.button2.getText().equals("X") || this.button8.getText().equals("X")) && this.button9.getText().equals("X") && ((this.button4.getText().equals("X") || this.button6.getText().equals("X")) && this.button1.getText().equals(" "))) {
            this.button1.setText("O");
            this.button1.setClickable(false);
        } else if ((this.button2.getText().equals("X") || this.button8.getText().equals("X")) && this.button6.getText().equals("X") && ((this.button1.getText().equals("X") || this.button3.getText().equals("X")) && this.button9.getText().equals(" "))) {
            this.button9.setText("O");
            this.button9.setClickable(false);
        } else if (this.button5.getText().equals(" ")) {
            this.button5.setText("O");
            this.button5.setClickable(false);
        } else if (this.button5.getText().equals("X") && this.button1.getText().equals(" ") && (wtf == 1 || wtf == 2 || wtf == 3)) {
            this.button1.setText("O");
            this.button1.setClickable(false);
        } else if (this.button5.getText().equals("X") && this.button3.getText().equals(" ") && (wtf == 2 || wtf == 3 || wtf == 4)) {
            this.button3.setText("O");
            this.button3.setClickable(false);
        } else if (this.button5.getText().equals("X") && this.button7.getText().equals(" ") && (wtf == 3 || wtf == 4 || wtf == 1)) {
            this.button7.setText("O");
            this.button7.setClickable(false);
        } else if (this.button5.getText().equals("X") && this.button9.getText().equals(" ") && (wtf == 4 || wtf == 1 || wtf == 2)) {
            this.button9.setText("O");
            this.button9.setClickable(false);
        } else if ((this.button2.getText().equals("X") || this.button8.getText().equals("X")) && this.button7.getText().equals("X") && this.button4.getText().equals(" ")) {
            this.button4.setText("O");
            this.button4.setClickable(false);
        } else if ((this.button2.getText().equals("X") || this.button8.getText().equals("X")) && this.button9.getText().equals("X") && this.button6.getText().equals(" ")) {
            this.button6.setText("O");
            this.button6.setClickable(false);
        } else if ((this.button4.getText().equals("X") || this.button6.getText().equals("X")) && this.button1.getText().equals("X") && this.button2.getText().equals(" ")) {
            this.button2.setText("O");
            this.button2.setClickable(false);
        } else if ((this.button4.getText().equals("X") || this.button6.getText().equals("X")) && this.button7.getText().equals("X") && this.button8.getText().equals(" ")) {
            this.button8.setText("O");
            this.button8.setClickable(false);
        } else if ((this.button2.getText().equals("X") || this.button8.getText().equals("X")) && ((this.button3.getText().equals("X") || this.button1.getText().equals("X")) && this.button4.getText().equals(" "))) {
            this.button4.setText("O");
            this.button4.setClickable(false);
        } else if (apar == 1 && this.button2.getText().equals(" ")) {
            this.button2.setText("O");
            this.button2.setClickable(false);
        } else if (apar == 2 && this.button4.getText().equals(" ")) {
            this.button4.setText("O");
            this.button4.setClickable(false);
        } else if (apar == 3 && this.button6.getText().equals(" ")) {
            this.button6.setText("O");
            this.button6.setClickable(false);
        } else if (apar == 2 && this.button8.getText().equals(" ")) {
            this.button8.setText("O");
            this.button8.setClickable(false);
        } else if (caz == 1 && this.button3.getText().equals(" ")) {
            this.button3.setText("O");
            this.button3.setClickable(false);
        } else if (caz == 2 && this.button5.getText().equals(" ")) {
            this.button5.setText("O");
            this.button5.setClickable(false);
        } else if (caz == 3 && this.button7.getText().equals(" ")) {
            this.button7.setText("O");
            this.button7.setClickable(false);
        } else if (caz == 4 && this.button2.getText().equals(" ")) {
            this.button2.setText("O");
            this.button2.setClickable(false);
        } else if (caz == 7 && this.button8.getText().equals(" ")) {
            this.button8.setText("O");
            this.button8.setClickable(false);
        } else if (caz == 8 && this.button9.getText().equals(" ")) {
            this.button9.setText("O");
            this.button9.setClickable(false);
        } else if (caz != 9 || !this.button1.getText().equals(" ")) {
            Rand();
        } else {
            this.button1.setText("O");
            this.button1.setClickable(false);
        }
    }

    public void onClick(View src) {
        switch (src.getId()) {
            case R.id.button1 /*2131099649*/:
                this.mediaPlayer.start();
                this.step++;
                this.button1.setText("X");
                this.button1.setClickable(false);
                non();
                return;
            case R.id.button2 /*2131099650*/:
                this.mediaPlayer.start();
                this.step++;
                this.button2.setText("X");
                this.button2.setClickable(false);
                non();
                return;
            case R.id.button3 /*2131099651*/:
                this.mediaPlayer.start();
                this.step++;
                this.button3.setText("X");
                this.button3.setClickable(false);
                non();
                return;
            case R.id.mainLayout /*2131099652*/:
            case R.id.textScore /*2131099653*/:
            case R.id.textCard /*2131099654*/:
            case R.id.tableRow1 /*2131099655*/:
            case R.id.tableRow2 /*2131099656*/:
            case R.id.tableRow3 /*2131099660*/:
            case R.id.gameOverlay /*2131099664*/:
            default:
                return;
            case R.id.button4 /*2131099657*/:
                this.step++;
                this.mediaPlayer.start();
                this.button4.setText("X");
                this.button4.setClickable(false);
                non();
                return;
            case R.id.button5 /*2131099658*/:
                this.step++;
                this.mediaPlayer.start();
                this.button5.setText("X");
                this.button5.setClickable(false);
                non();
                return;
            case R.id.button6 /*2131099659*/:
                this.step++;
                this.mediaPlayer.start();
                this.button6.setText("X");
                this.button6.setClickable(false);
                non();
                return;
            case R.id.button7 /*2131099661*/:
                this.step++;
                this.mediaPlayer.start();
                this.button7.setText("X");
                this.button7.setClickable(false);
                non();
                return;
            case R.id.button8 /*2131099662*/:
                this.step++;
                this.mediaPlayer.start();
                this.button8.setText("X");
                this.button8.setClickable(false);
                non();
                return;
            case R.id.button9 /*2131099663*/:
                this.step++;
                this.mediaPlayer.start();
                this.button9.setText("X");
                this.button9.setClickable(false);
                non();
                return;
            case R.id.button10 /*2131099665*/:
                this.mediaPlayer.start();
                this.step = 0;
                this.won = 0;
                this.gameOverlay.setStartStopButtons(null, null);
                this.button1.setText(" ");
                this.button1.setClickable(true);
                this.button2.setText(" ");
                this.button2.setClickable(true);
                this.button3.setText(" ");
                this.button3.setClickable(true);
                this.button4.setText(" ");
                this.button4.setClickable(true);
                this.button5.setText(" ");
                this.button5.setClickable(true);
                this.button6.setText(" ");
                this.button6.setClickable(true);
                this.button7.setText(" ");
                this.button7.setClickable(true);
                this.button8.setText(" ");
                this.button8.setClickable(true);
                this.button9.setText(" ");
                this.button9.setClickable(true);
                this.textScore.setText("Human's Turn");
                this.ok = 0;
                this.cont = 0;
                return;
            case R.id.button12 /*2131099666*/:
                this.mediaPlayer.start();
                this.theX = 0;
                this.theO = 0;
                this.textCard.setText("X: " + this.theX + "     ||    O: " + this.theO);
                return;
        }
    }
}
