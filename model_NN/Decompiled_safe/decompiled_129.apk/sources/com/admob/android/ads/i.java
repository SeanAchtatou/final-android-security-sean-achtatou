package com.admob.android.ads;

import android.util.Log;
import com.admob.android.ads.InterstitialAd;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

/* compiled from: AdMobURLConnector */
final class i extends f {
    private HttpURLConnection m;
    private URL n;

    public i(String str, String str2, String str3, h hVar, int i, Map<String, String> map, String str4) {
        super(str2, str3, hVar, i, map, str4);
        try {
            this.n = new URL(str);
            this.i = this.n;
        } catch (MalformedURLException e) {
            this.n = null;
            this.c = e;
        }
        this.m = null;
        this.e = 0;
    }

    /* JADX WARNING: CFG modification limit reached, blocks count: 217 */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0116 A[Catch:{ all -> 0x0245 }] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0145 A[Catch:{ all -> 0x0245 }] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x0150 A[SYNTHETIC, Splitter:B:54:0x0150] */
    /* JADX WARNING: Removed duplicated region for block: B:97:0x0239 A[SYNTHETIC, Splitter:B:97:0x0239] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean d() {
        /*
            r12 = this;
            r10 = 2
            r9 = 1
            r8 = 0
            java.net.URL r1 = r12.n
            if (r1 != 0) goto L_0x0257
            com.admob.android.ads.h r1 = r12.h
            if (r1 == 0) goto L_0x0017
            com.admob.android.ads.h r1 = r12.h
            java.lang.Exception r2 = new java.lang.Exception
            java.lang.String r3 = "url was null"
            r2.<init>(r3)
            r1.a(r12, r2)
        L_0x0017:
            r1 = r8
        L_0x0018:
            if (r1 != 0) goto L_0x0025
            com.admob.android.ads.h r2 = r12.h
            if (r2 == 0) goto L_0x0025
            com.admob.android.ads.h r2 = r12.h
            java.lang.Exception r3 = r12.c
            r2.a(r12, r3)
        L_0x0025:
            return r1
        L_0x0026:
            r4 = 302(0x12e, float:4.23E-43)
            if (r2 != r4) goto L_0x024f
            java.net.HttpURLConnection r2 = r12.m     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            java.lang.String r4 = "Location"
            java.lang.String r2 = r2.getHeaderField(r4)     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            java.lang.String r4 = "AdMobSDK"
            r5 = 3
            boolean r4 = com.admob.android.ads.InterstitialAd.c.a(r4, r5)     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            if (r4 == 0) goto L_0x0053
            java.lang.String r4 = "AdMobSDK"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            r5.<init>()     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            java.lang.String r6 = "Got redirectUrl: "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            java.lang.StringBuilder r5 = r5.append(r2)     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            android.util.Log.d(r4, r5)     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
        L_0x0053:
            java.net.URL r4 = new java.net.URL     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            r4.<init>(r2)     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            r12.n = r4     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            r12.i()
        L_0x005d:
            int r1 = r12.e
            int r2 = r12.f
            if (r1 >= r2) goto L_0x0254
            if (r3 != 0) goto L_0x0254
            java.lang.String r1 = "AdMobSDK"
            boolean r1 = com.admob.android.ads.InterstitialAd.c.a(r1, r10)
            if (r1 == 0) goto L_0x0093
            java.lang.String r1 = "AdMobSDK"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r4 = "attempt "
            java.lang.StringBuilder r2 = r2.append(r4)
            int r4 = r12.e
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r4 = " to connect to url "
            java.lang.StringBuilder r2 = r2.append(r4)
            java.net.URL r4 = r12.n
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r2 = r2.toString()
            android.util.Log.v(r1, r2)
        L_0x0093:
            r4 = 0
            r12.i()     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            java.net.URL r1 = r12.n     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            java.net.URLConnection r1 = r1.openConnection()     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            java.net.HttpURLConnection r1 = (java.net.HttpURLConnection) r1     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            r12.m = r1     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            java.net.HttpURLConnection r1 = r12.m     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            r2 = 1
            r1.setUseCaches(r2)     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            java.net.HttpURLConnection r1 = r12.m     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            r2 = 1
            r1.setInstanceFollowRedirects(r2)     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            java.net.HttpURLConnection r1 = r12.m     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            if (r1 == 0) goto L_0x0251
            java.net.HttpURLConnection r1 = r12.m     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            java.lang.String r2 = "User-Agent"
            java.lang.String r5 = h()     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            r1.setRequestProperty(r2, r5)     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            java.lang.String r1 = r12.g     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            if (r1 == 0) goto L_0x00c9
            java.net.HttpURLConnection r1 = r12.m     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            java.lang.String r2 = "X-ADMOB-ISU"
            java.lang.String r5 = r12.g     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            r1.setRequestProperty(r2, r5)     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
        L_0x00c9:
            java.net.HttpURLConnection r1 = r12.m     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            int r2 = r12.b     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            r1.setConnectTimeout(r2)     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            java.net.HttpURLConnection r1 = r12.m     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            int r2 = r12.b     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            r1.setReadTimeout(r2)     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            java.net.HttpURLConnection r1 = r12.m     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            r2 = 0
            r1.setUseCaches(r2)     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            java.util.Map r1 = r12.d     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            if (r1 == 0) goto L_0x0160
            java.util.Map r1 = r12.d     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            java.util.Set r1 = r1.keySet()     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            java.util.Iterator r5 = r1.iterator()     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
        L_0x00eb:
            boolean r1 = r5.hasNext()     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            if (r1 == 0) goto L_0x0160
            java.lang.Object r1 = r5.next()     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            r0 = r1
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            r2 = r0
            if (r2 == 0) goto L_0x00eb
            java.util.Map r1 = r12.d     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            java.lang.Object r1 = r1.get(r2)     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            if (r1 == 0) goto L_0x00eb
            java.net.HttpURLConnection r6 = r12.m     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            r6.addRequestProperty(r2, r1)     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            goto L_0x00eb
        L_0x010b:
            r1 = move-exception
            r2 = r4
        L_0x010d:
            java.lang.String r3 = "AdMobSDK"
            r4 = 3
            boolean r3 = com.admob.android.ads.InterstitialAd.c.a(r3, r4)     // Catch:{ all -> 0x0245 }
            if (r3 == 0) goto L_0x013c
            java.lang.String r3 = "AdMobSDK"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0245 }
            r4.<init>()     // Catch:{ all -> 0x0245 }
            java.lang.String r5 = "connection attempt "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0245 }
            int r5 = r12.e     // Catch:{ all -> 0x0245 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0245 }
            java.lang.String r5 = " failed, url "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0245 }
            java.net.URL r5 = r12.n     // Catch:{ all -> 0x0245 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0245 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x0245 }
            android.util.Log.d(r3, r4)     // Catch:{ all -> 0x0245 }
        L_0x013c:
            java.lang.String r3 = "AdMobSDK"
            r4 = 2
            boolean r3 = com.admob.android.ads.InterstitialAd.c.a(r3, r4)     // Catch:{ all -> 0x0245 }
            if (r3 == 0) goto L_0x014c
            java.lang.String r3 = "AdMobSDK"
            java.lang.String r4 = "exception: "
            android.util.Log.v(r3, r4, r1)     // Catch:{ all -> 0x0245 }
        L_0x014c:
            r12.c = r1     // Catch:{ all -> 0x0245 }
            if (r2 == 0) goto L_0x0153
            r2.close()     // Catch:{ Exception -> 0x0240 }
        L_0x0153:
            r12.i()
            r1 = r8
        L_0x0157:
            int r2 = r12.e
            int r2 = r2 + 1
            r12.e = r2
            r3 = r1
            goto L_0x005d
        L_0x0160:
            java.lang.String r1 = r12.l     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            if (r1 == 0) goto L_0x0215
            java.net.HttpURLConnection r1 = r12.m     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            java.lang.String r2 = "POST"
            r1.setRequestMethod(r2)     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            java.net.HttpURLConnection r1 = r12.m     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            r2 = 1
            r1.setDoOutput(r2)     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            java.net.HttpURLConnection r1 = r12.m     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            java.lang.String r2 = "Content-Type"
            java.lang.String r5 = r12.a     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            r1.setRequestProperty(r2, r5)     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            java.net.HttpURLConnection r1 = r12.m     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            java.lang.String r2 = "Content-Length"
            java.lang.String r5 = r12.l     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            int r5 = r5.length()     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            java.lang.String r5 = java.lang.Integer.toString(r5)     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            r1.setRequestProperty(r2, r5)     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            java.net.HttpURLConnection r1 = r12.m     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            java.io.OutputStream r1 = r1.getOutputStream()     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            java.io.BufferedWriter r2 = new java.io.BufferedWriter     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            java.io.OutputStreamWriter r5 = new java.io.OutputStreamWriter     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            r5.<init>(r1)     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            r1 = 4096(0x1000, float:5.74E-42)
            r2.<init>(r5, r1)     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            java.lang.String r1 = r12.l     // Catch:{ Exception -> 0x024c }
            r2.write(r1)     // Catch:{ Exception -> 0x024c }
            r2.close()     // Catch:{ Exception -> 0x024c }
            r1 = 0
        L_0x01a6:
            java.net.HttpURLConnection r2 = r12.m     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            int r2 = r2.getResponseCode()     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            java.lang.String r4 = "AdMobSDK"
            r5 = 2
            boolean r4 = com.admob.android.ads.InterstitialAd.c.a(r4, r5)     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            if (r4 == 0) goto L_0x01d7
            java.net.HttpURLConnection r4 = r12.m     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            java.lang.String r5 = "X-AdMob-AdSrc"
            java.lang.String r4 = r4.getHeaderField(r5)     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            if (r4 == 0) goto L_0x01d7
            java.lang.String r5 = "AdMobSDK"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            r6.<init>()     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            java.lang.String r7 = "Ad response came from server "
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            java.lang.StringBuilder r4 = r6.append(r4)     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            android.util.Log.v(r5, r4)     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
        L_0x01d7:
            r4 = 200(0xc8, float:2.8E-43)
            if (r2 < r4) goto L_0x0026
            r4 = 300(0x12c, float:4.2E-43)
            if (r2 >= r4) goto L_0x0026
            java.net.HttpURLConnection r2 = r12.m     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            java.net.URL r2 = r2.getURL()     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            r12.i = r2     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            boolean r2 = r12.k     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            if (r2 == 0) goto L_0x0222
            java.io.BufferedInputStream r2 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            java.net.HttpURLConnection r3 = r12.m     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            java.io.InputStream r3 = r3.getInputStream()     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            r4 = 4096(0x1000, float:5.74E-42)
            r2.<init>(r3, r4)     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            r3 = 4096(0x1000, float:5.74E-42)
            byte[] r3 = new byte[r3]     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            java.io.ByteArrayOutputStream r4 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            r5 = 4096(0x1000, float:5.74E-42)
            r4.<init>(r5)     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
        L_0x0203:
            int r5 = r2.read(r3)     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            r6 = -1
            if (r5 == r6) goto L_0x021c
            r6 = 0
            r4.write(r3, r6, r5)     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            goto L_0x0203
        L_0x020f:
            r2 = move-exception
            r11 = r2
            r2 = r1
            r1 = r11
            goto L_0x010d
        L_0x0215:
            java.net.HttpURLConnection r1 = r12.m     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            r1.connect()     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            r1 = r4
            goto L_0x01a6
        L_0x021c:
            byte[] r2 = r4.toByteArray()     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            r12.j = r2     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
        L_0x0222:
            com.admob.android.ads.h r2 = r12.h     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            if (r2 == 0) goto L_0x022b
            com.admob.android.ads.h r2 = r12.h     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            r2.a(r12)     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
        L_0x022b:
            r2 = r9
        L_0x022c:
            r12.i()     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            r12.i()
            r1 = r2
            goto L_0x0157
        L_0x0235:
            r1 = move-exception
            r2 = r4
        L_0x0237:
            if (r2 == 0) goto L_0x023c
            r2.close()     // Catch:{ Exception -> 0x0243 }
        L_0x023c:
            r12.i()
            throw r1
        L_0x0240:
            r1 = move-exception
            goto L_0x0153
        L_0x0243:
            r2 = move-exception
            goto L_0x023c
        L_0x0245:
            r1 = move-exception
            goto L_0x0237
        L_0x0247:
            r2 = move-exception
            r11 = r2
            r2 = r1
            r1 = r11
            goto L_0x0237
        L_0x024c:
            r1 = move-exception
            goto L_0x010d
        L_0x024f:
            r2 = r3
            goto L_0x022c
        L_0x0251:
            r1 = r4
            r2 = r3
            goto L_0x022c
        L_0x0254:
            r1 = r3
            goto L_0x0018
        L_0x0257:
            r3 = r8
            goto L_0x005d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.admob.android.ads.i.d():boolean");
    }

    private void i() {
        if (this.m != null) {
            this.m.disconnect();
            this.m = null;
        }
    }

    public final void e() {
        i();
        this.h = null;
    }

    public final void run() {
        try {
            d();
        } catch (Exception e) {
            if (InterstitialAd.c.a(AdManager.LOG, 6)) {
                Log.e(AdManager.LOG, "exception caught in AdMobURLConnector.run(), " + e.getMessage());
            }
            if (this.h != null) {
                this.h.a(this, this.c);
            }
        }
    }
}
