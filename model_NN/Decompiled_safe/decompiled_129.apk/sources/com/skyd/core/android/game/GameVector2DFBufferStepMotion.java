package com.skyd.core.android.game;

import com.skyd.core.vector.Vector2DF;

public class GameVector2DFBufferStepMotion extends GameVector2DFStepMotion {
    private float _LessenBuffer = 0.0f;

    public GameVector2DFBufferStepMotion(float stepLength, Vector2DF targetPosition, float buffer, IGameValueDuct<GameObject, Vector2DF> valueDuct) {
        super(stepLength, targetPosition, valueDuct);
    }

    /* access modifiers changed from: protected */
    public void updateSelf(GameObject obj) {
        super.updateSelf(obj);
        setStepLength(getStepLength() * (1.0f - getLessenBuffer()));
    }

    public float getLessenBuffer() {
        return this._LessenBuffer;
    }

    public void setLessenBuffer(float value) {
        this._LessenBuffer = value;
    }

    public void setLessenBufferToDefault() {
        setLessenBuffer(0.0f);
    }
}
