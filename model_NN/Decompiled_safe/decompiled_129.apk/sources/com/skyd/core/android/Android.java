package com.skyd.core.android;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Environment;
import android.view.MotionEvent;
import com.skyd.core.vector.Vector2DF;
import java.io.File;
import java.lang.reflect.InvocationTargetException;

public final class Android {
    public static Context createPackageContext(Context c, String packageName) {
        try {
            return c.createPackageContext(packageName, 0);
        } catch (Exception e) {
            return null;
        }
    }

    public static SharedPreferences getSharedPreferences(Context c, String appkey) {
        return getSharedPreferences(c, appkey, 0);
    }

    public static SharedPreferences getSharedPreferences(Context c, String appkey, int permission) {
        return c.getSharedPreferences(appkey, permission);
    }

    public static void openUrl(Context c, String url) {
        c.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url)));
    }

    public static void openUrl(Context c, int resourceID) {
        openUrl(c, c.getResources().getString(resourceID));
    }

    public static void shareText(Activity act, String title, String content) {
        Intent sintent = new Intent("android.intent.action.SEND");
        sintent.setType("text/plain");
        sintent.putExtra("android.intent.extra.SUBJECT", title);
        sintent.putExtra("android.intent.extra.TEXT", content);
        act.startActivity(Intent.createChooser(sintent, act.getTitle()));
    }

    public static float reflectionGetMotionEventX(MotionEvent e, int pointerIndex) throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        return ((Float) e.getClass().getMethod("getX", Integer.TYPE).invoke(e, Integer.valueOf(pointerIndex))).floatValue();
    }

    public static float reflectionGetMotionEventY(MotionEvent e, int pointerIndex) throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        return ((Float) e.getClass().getMethod("getY", Integer.TYPE).invoke(e, Integer.valueOf(pointerIndex))).floatValue();
    }

    public static Vector2DF reflectionGetMotionEventPosition(MotionEvent e, int pointerIndex) throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        return new Vector2DF(reflectionGetMotionEventX(e, pointerIndex), reflectionGetMotionEventY(e, pointerIndex));
    }

    public static int reflectionGetMotionEventPointerCount(MotionEvent e) throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        return ((Integer) e.getClass().getMethod("getPointerCount", new Class[0]).invoke(e, new Object[0])).intValue();
    }

    public static void setToFullScreen(Activity act) {
        act.getWindow().setFlags(1024, 1024);
        act.requestWindowFeature(1);
        act.getWindow().setFlags(128, 128);
    }

    public static void setToKeepScreenOn(Activity act) {
        act.getWindow().setFlags(128, 128);
    }

    public static File getSDCardDirectory() {
        return Environment.getExternalStorageDirectory();
    }
}
