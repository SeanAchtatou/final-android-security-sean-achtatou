package com.skyd.core.vector;

import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import com.skyd.core.math.MathEx;
import java.util.ArrayList;
import java.util.Iterator;

public class Vector2DF {
    private ArrayList<OnValueChangedListener> _ValueChangedListenerList = null;
    protected float _X = 0.0f;
    private ArrayList<OnXChangingListener> _XChangingListenerList = null;
    protected float _Y = 0.0f;
    private ArrayList<OnYChangingListener> _YChangingListenerList = null;

    public interface OnValueChangedListener {
        void OnValueChangedEvent(Object obj, float f, float f2);
    }

    public interface OnXChangingListener {
        boolean OnXChangingEvent(Object obj, float f, float f2);
    }

    public interface OnYChangingListener {
        boolean OnYChangingEvent(Object obj, float f, float f2);
    }

    public Vector2DF reset(float x, float y) {
        setX(x);
        setY(y);
        return this;
    }

    public Vector2DF resetWith(Vector2DF value) {
        setX(value.getX());
        setY(value.getY());
        return this;
    }

    public int getIntX() {
        return (int) Math.ceil((double) getX());
    }

    public int getIntY() {
        return (int) Math.ceil((double) getY());
    }

    public float getX() {
        return this._X;
    }

    public Vector2DF setX(float value) {
        if (onXChanging(this._X, value)) {
            this._X = value;
            onValueChanged(this._X, this._Y);
        }
        return this;
    }

    public Vector2DF setXToDefault() {
        setX(0.0f);
        return this;
    }

    public float getY() {
        return this._Y;
    }

    public Vector2DF setY(float value) {
        if (onYChanging(this._Y, value)) {
            this._Y = value;
            onValueChanged(this._X, this._Y);
        }
        return this;
    }

    public Vector2DF setYToDefault() {
        setY(0.0f);
        return this;
    }

    public Vector2DF(float x, float y) {
        setX(x);
        setY(y);
    }

    public Vector2DF() {
    }

    public Vector2DF(float[] array) {
        setX(array[0]);
        setY(array[1]);
    }

    public float[] toArray() {
        return new float[]{getX(), getY()};
    }

    public Vector2DF map(Matrix m) {
        float[] a = toArray();
        m.mapPoints(a);
        reset(a[0], a[1]);
        return this;
    }

    public Vector2DF mapNew(Matrix m) {
        float[] a = toArray();
        m.mapPoints(a);
        return new Vector2DF(a);
    }

    /* access modifiers changed from: protected */
    public Object clone() throws CloneNotSupportedException {
        return getClone();
    }

    public Vector2DF getClone() {
        return new Vector2DF(this._X, this._Y);
    }

    public String toString() {
        return "[" + (Math.round(this._X * 1000.0f) / 1000) + "," + (Math.round(this._Y * 1000.0f) / 1000) + "]";
    }

    public Vector2DF plus(Vector2DF value) {
        setX(this._X + value._X);
        setY(this._Y + value._Y);
        return this;
    }

    public Vector2DF plusNew(Vector2DF value) {
        return new Vector2DF(this._X + value._X, this._Y + value._Y);
    }

    public Vector2DF minus(Vector2DF value) {
        setX(this._X - value._X);
        setY(this._Y - value._Y);
        return this;
    }

    public Vector2DF minusNew(Vector2DF value) {
        return new Vector2DF(this._X - value._X, this._Y - value._Y);
    }

    public Vector2DF negate() {
        setX(-this._X);
        setY(-this._Y);
        return this;
    }

    public Vector2DF negateNew() {
        return new Vector2DF(-this._X, -this._Y);
    }

    public Vector2DF scale(float value) {
        scale(value, value);
        return this;
    }

    public Vector2DF scaleNew(float value) {
        return scaleNew(value, value);
    }

    public Vector2DF scale(float xValue, float yValue) {
        setX(this._X * xValue);
        setY(this._Y * yValue);
        return this;
    }

    public Vector2DF scale(Vector2DF value) {
        return scale(value.getX(), value.getY());
    }

    public Vector2DF scaleNew(float xValue, float yValue) {
        return new Vector2DF(this._X * xValue, this._Y * yValue);
    }

    public Vector2DF scaleNew(Vector2DF value) {
        return scaleNew(value.getX(), getY());
    }

    public Vector2DF scale(float xValue, float yValue, Vector2DF center) {
        minus(center);
        setX(this._X * xValue);
        setY(this._Y * yValue);
        plus(center);
        return this;
    }

    public Vector2DF scale(Vector2DF value, Vector2DF center) {
        return scale(value.getX(), value.getY(), center);
    }

    public Vector2DF scaleNew(float xValue, float yValue, Vector2DF center) {
        Vector2DF v = getClone();
        v.scale(xValue, yValue, center);
        return v;
    }

    public Vector2DF scaleNew(Vector2DF value, Vector2DF center) {
        return scaleNew(value.getX(), value.getY(), center);
    }

    public float getLength() {
        return (float) Math.sqrt((double) ((this._X * this._X) + (this._Y * this._Y)));
    }

    public Vector2DF setLength(float value) {
        float r = getLength();
        if (r != 0.0f) {
            scale(value / r);
        } else {
            setX(value);
        }
        return this;
    }

    public float getAngle() {
        return MathEx.atan2Angle(this._Y, this._X);
    }

    public Vector2DF setAngle(float angle) {
        float r = getLength();
        setX(MathEx.cosAngle(angle) * r);
        setY(MathEx.sinAngle(angle) * r);
        return this;
    }

    public Vector2DF rotate(float angle) {
        float ca = MathEx.cosAngle(angle);
        float sa = MathEx.sinAngle(angle);
        setX((this._X * ca) - (this._Y * sa));
        setY((this._X * sa) + (this._Y * ca));
        return this;
    }

    public Vector2DF rotateNew(float angle) {
        Vector2DF v = getClone();
        v.rotate(angle);
        return v;
    }

    public Vector2DF rotate(float angle, Vector2DF center) {
        minus(center);
        rotate(angle);
        plus(center);
        return this;
    }

    public Vector2DF rotateNew(float angle, Vector2DF center) {
        Vector2DF v = getClone();
        v.rotate(angle, center);
        return v;
    }

    public float dot(Vector2DF v) {
        return (this._X * v._X) + (this._Y * v._Y);
    }

    public boolean isPerpTo(Vector2DF v) {
        return dot(v) == 0.0f;
    }

    public Vector2DF getNormalCW() {
        return new Vector2DF(this._Y, -this._X);
    }

    public Vector2DF getNormalCCW() {
        return new Vector2DF(-this._Y, this._X);
    }

    public float angleBetween(Vector2DF v) {
        return MathEx.acosAngle(dot(v) / (getLength() * v.getLength()));
    }

    public boolean addOnXChangingListener(OnXChangingListener listener) {
        if (this._XChangingListenerList == null) {
            this._XChangingListenerList = new ArrayList<>();
        } else if (this._XChangingListenerList.contains(listener)) {
            return false;
        }
        this._XChangingListenerList.add(listener);
        return true;
    }

    public boolean removeOnXChangingListener(OnXChangingListener listener) {
        if (this._XChangingListenerList == null || !this._XChangingListenerList.contains(listener)) {
            return false;
        }
        this._XChangingListenerList.remove(listener);
        return true;
    }

    public void clearOnXChangingListeners() {
        if (this._XChangingListenerList != null) {
            this._XChangingListenerList.clear();
        }
    }

    /* access modifiers changed from: protected */
    public boolean onXChanging(float currentValue, float newValue) {
        if (this._XChangingListenerList != null) {
            Iterator<OnXChangingListener> it = this._XChangingListenerList.iterator();
            while (it.hasNext()) {
                if (!it.next().OnXChangingEvent(this, currentValue, newValue)) {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean addOnYChangingListener(OnYChangingListener listener) {
        if (this._YChangingListenerList == null) {
            this._YChangingListenerList = new ArrayList<>();
        } else if (this._YChangingListenerList.contains(listener)) {
            return false;
        }
        this._YChangingListenerList.add(listener);
        return true;
    }

    public boolean removeOnYChangingListener(OnYChangingListener listener) {
        if (this._YChangingListenerList == null || !this._YChangingListenerList.contains(listener)) {
            return false;
        }
        this._YChangingListenerList.remove(listener);
        return true;
    }

    public void clearOnYChangingListeners() {
        if (this._YChangingListenerList != null) {
            this._YChangingListenerList.clear();
        }
    }

    /* access modifiers changed from: protected */
    public boolean onYChanging(float currentValue, float newValue) {
        if (this._YChangingListenerList != null) {
            Iterator<OnYChangingListener> it = this._YChangingListenerList.iterator();
            while (it.hasNext()) {
                if (!it.next().OnYChangingEvent(this, currentValue, newValue)) {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean addOnValueChangedListener(OnValueChangedListener listener) {
        if (this._ValueChangedListenerList == null) {
            this._ValueChangedListenerList = new ArrayList<>();
        } else if (this._ValueChangedListenerList.contains(listener)) {
            return false;
        }
        this._ValueChangedListenerList.add(listener);
        return true;
    }

    public boolean removeOnValueChangedListener(OnValueChangedListener listener) {
        if (this._ValueChangedListenerList == null || !this._ValueChangedListenerList.contains(listener)) {
            return false;
        }
        this._ValueChangedListenerList.remove(listener);
        return true;
    }

    public void clearOnValueChangedListeners() {
        if (this._ValueChangedListenerList != null) {
            this._ValueChangedListenerList.clear();
        }
    }

    /* access modifiers changed from: protected */
    public void onValueChanged(float newX, float newY) {
        if (this._ValueChangedListenerList != null) {
            Iterator<OnValueChangedListener> it = this._ValueChangedListenerList.iterator();
            while (it.hasNext()) {
                it.next().OnValueChangedEvent(this, newX, newY);
            }
        }
    }

    public boolean isIn(Rect r) {
        return this._X >= ((float) r.left) && this._X <= ((float) r.right) && this._Y >= ((float) r.top) && this._Y <= ((float) r.bottom);
    }

    public boolean isIn(RectF r) {
        return this._X >= r.left && this._X <= r.right && this._Y >= r.top && this._Y <= r.bottom;
    }

    public Vector2DF plusX(float v) {
        setX(this._X + v);
        return this;
    }

    public Vector2DF plusY(float v) {
        setY(this._Y + v);
        return this;
    }

    public Vector2DF scaleX(float v) {
        setX(this._X * v);
        return this;
    }

    public Vector2DF scaleY(float v) {
        setY(this._Y * v);
        return this;
    }

    public Vector2DF restrainLength(float v) {
        if (getLength() > v) {
            setLength(v);
        }
        return this;
    }

    public Vector2DF averageWith(Vector2DF v) {
        setX((this._X + v._X) / 2.0f);
        setY((this._Y + v._Y) / 2.0f);
        return this;
    }

    public Vector2DF averageNewWith(Vector2DF v) {
        return getClone().averageWith(v);
    }
}
