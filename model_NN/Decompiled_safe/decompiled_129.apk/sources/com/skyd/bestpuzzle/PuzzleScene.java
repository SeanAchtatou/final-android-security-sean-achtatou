package com.skyd.bestpuzzle;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.widget.Toast;
import com.scoreloop.client.android.core.model.Score;
import com.scoreloop.client.android.ui.ScoreloopManagerSingleton;
import com.skyd.bestpuzzle.n1665.R;
import com.skyd.core.android.game.GameImageSpirit;
import com.skyd.core.android.game.GameMaster;
import com.skyd.core.android.game.GameObject;
import com.skyd.core.android.game.GameScene;
import com.skyd.core.android.game.GameSpirit;
import com.skyd.core.draw.DrawHelper;
import com.skyd.core.vector.Vector2DF;
import com.skyd.core.vector.VectorRect2DF;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

public class PuzzleScene extends GameScene {
    public Controller Controller;
    public Desktop Desktop;
    boolean IsDrawingPuzzle;
    boolean IsDrawnSelectRect;
    public GameImageSpirit MoveButton;
    public UI MoveButtonSlot;
    public OriginalImage OriginalImage;
    public long PastTime;
    public long StartTime;
    public UI SubmitScoreButton;
    /* access modifiers changed from: private */
    public RectF SubmitScoreButtonRect;
    public GameSpirit Time;
    public UI ViewFullButton;
    /* access modifiers changed from: private */
    public RectF ViewFullButtonRect;
    public UI ZoomInButton;
    public UI ZoomOutButton;
    private boolean _IsFinished = false;
    private ArrayList<OnIsFinishedChangedListener> _IsFinishedChangedListenerList = null;
    long savetime = Long.MIN_VALUE;
    RectF selectRect;

    public interface OnIsFinishedChangedListener {
        void OnIsFinishedChangedEvent(Object obj, boolean z);
    }

    public PuzzleScene() {
        setMaxDrawCacheLayer(25.0f);
        this.StartTime = new Date().getTime();
        loadGameState();
    }

    public void loadGameState() {
        Center c = (Center) GameMaster.getContext().getApplicationContext();
        this.PastTime = c.getPastTime().longValue();
        this._IsFinished = c.getIsFinished().booleanValue();
    }

    public void saveGameState() {
        Center c = (Center) GameMaster.getContext().getApplicationContext();
        if (getIsFinished()) {
            c.setPastTime(Long.valueOf(this.PastTime));
        } else {
            c.setPastTime(Long.valueOf(this.PastTime + (new Date().getTime() - this.StartTime)));
        }
        c.setIsFinished(Boolean.valueOf(getIsFinished()));
    }

    public void setFinished() {
        if (!getIsFinished()) {
            this.SubmitScoreButton.show();
        }
        setIsFinished(true);
        this.PastTime += new Date().getTime() - this.StartTime;
    }

    public boolean getIsFinished() {
        return this._IsFinished;
    }

    private void setIsFinished(boolean value) {
        onIsFinishedChanged(value);
        this._IsFinished = value;
    }

    private void setIsFinishedToDefault() {
        setIsFinished(false);
    }

    public boolean addOnIsFinishedChangedListener(OnIsFinishedChangedListener listener) {
        if (this._IsFinishedChangedListenerList == null) {
            this._IsFinishedChangedListenerList = new ArrayList<>();
        } else if (this._IsFinishedChangedListenerList.contains(listener)) {
            return false;
        }
        this._IsFinishedChangedListenerList.add(listener);
        return true;
    }

    public boolean removeOnIsFinishedChangedListener(OnIsFinishedChangedListener listener) {
        if (this._IsFinishedChangedListenerList == null || !this._IsFinishedChangedListenerList.contains(listener)) {
            return false;
        }
        this._IsFinishedChangedListenerList.remove(listener);
        return true;
    }

    public void clearOnIsFinishedChangedListeners() {
        if (this._IsFinishedChangedListenerList != null) {
            this._IsFinishedChangedListenerList.clear();
        }
    }

    /* access modifiers changed from: protected */
    public void onIsFinishedChanged(boolean newValue) {
        if (this._IsFinishedChangedListenerList != null) {
            Iterator<OnIsFinishedChangedListener> it = this._IsFinishedChangedListenerList.iterator();
            while (it.hasNext()) {
                it.next().OnIsFinishedChangedEvent(this, newValue);
            }
        }
    }

    public int getTargetCacheID() {
        return 1;
    }

    /* access modifiers changed from: protected */
    public void drawChilds(Canvas c, Rect drawArea) {
        this.IsDrawingPuzzle = false;
        this.IsDrawnSelectRect = false;
        super.drawChilds(c, drawArea);
    }

    /* access modifiers changed from: protected */
    public boolean onDrawingChild(GameObject child, Canvas c, Rect drawArea) {
        float lvl = ((GameSpirit) child).getLevel();
        if (!this.IsDrawingPuzzle && lvl <= 10.0f) {
            this.IsDrawingPuzzle = true;
            c.setMatrix(this.Desktop.getMatrix());
        } else if (lvl > 10.0f && this.IsDrawingPuzzle) {
            this.IsDrawingPuzzle = false;
            c.setMatrix(new Matrix());
        }
        if (lvl > 25.0f && this.selectRect != null && !this.IsDrawnSelectRect) {
            this.IsDrawnSelectRect = true;
            Paint p = new Paint();
            p.setARGB(100, 180, 225, 255);
            c.drawRect(this.selectRect, p);
        }
        return super.onDrawingChild(child, c, drawArea);
    }

    public void load(Context c) {
        loadOriginalImage(c);
        loadDesktop(c);
        loadInterface(c);
        loadPuzzle(c);
        loadPuzzleState();
        this.Desktop.updateCurrentObserveAreaRectF();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.skyd.core.draw.DrawHelper.calculateScaleSize(float, float, float, float, boolean):com.skyd.core.vector.Vector2DF
     arg types: [int, int, float, float, int]
     candidates:
      com.skyd.core.draw.DrawHelper.calculateScaleSize(double, double, double, double, boolean):com.skyd.core.vector.Vector2D
      com.skyd.core.draw.DrawHelper.calculateScaleSize(float, float, float, float, boolean):com.skyd.core.vector.Vector2DF */
    public void loadOriginalImage(Context c) {
        this.OriginalImage = new OriginalImage() {
            /* access modifiers changed from: protected */
            public boolean onDrawing(Canvas c, Rect drawArea) {
                c.drawARGB(180, 0, 0, 0);
                return super.onDrawing(c, drawArea);
            }
        };
        this.OriginalImage.setLevel(50.0f);
        this.OriginalImage.setName("OriginalImage");
        this.OriginalImage.getSize().resetWith(DrawHelper.calculateScaleSize(384.0f, 240.0f, (float) (GameMaster.getScreenWidth() - 40), (float) (GameMaster.getScreenHeight() - 40), true));
        this.OriginalImage.setTotalSeparateColumn(8);
        this.OriginalImage.setTotalSeparateRow(5);
        this.OriginalImage.getOriginalSize().reset(800.0f, 500.0f);
        this.OriginalImage.hide();
        this.OriginalImage.setIsUseAbsolutePosition(true);
        this.OriginalImage.setIsUseAbsoluteSize(true);
        this.OriginalImage.getImage().setIsUseAbsolutePosition(true);
        this.OriginalImage.getImage().setIsUseAbsoluteSize(true);
        this.OriginalImage.getPosition().reset(((float) (GameMaster.getScreenWidth() / 2)) - (this.OriginalImage.getSize().getX() / 2.0f), ((float) (GameMaster.getScreenHeight() / 2)) - (this.OriginalImage.getSize().getY() / 2.0f));
        getSpiritList().add(this.OriginalImage);
    }

    public void loadDesktop(Context c) {
        this.Desktop = new Desktop();
        this.Desktop.setLevel(-9999.0f);
        this.Desktop.setName("Desktop");
        this.Desktop.getSize().reset(1600.0f, 1000.0f);
        this.Desktop.getCurrentObservePosition().reset(800.0f, 500.0f);
        this.Desktop.setOriginalImage(this.OriginalImage);
        this.Desktop.setIsUseAbsolutePosition(true);
        this.Desktop.setIsUseAbsoluteSize(true);
        this.Desktop.ColorAPaint = new Paint();
        this.Desktop.ColorAPaint.setColor(Color.argb(255, 32, 30, 43));
        this.Desktop.ColorBPaint = new Paint();
        this.Desktop.ColorBPaint.setColor(Color.argb(255, 26, 24, 37));
        getSpiritList().add(this.Desktop);
    }

    public void loadPuzzle(Context c) {
        this.Controller = new Controller();
        this.Controller.setLevel(10.0f);
        this.Controller.setName("Controller");
        this.Controller.setDesktop(this.Desktop);
        this.Controller.setIsUseAbsolutePosition(true);
        this.Controller.setIsUseAbsoluteSize(true);
        getSpiritList().add(this.Controller);
        Puzzle.getRealitySize().reset(100.0f, 100.0f);
        Puzzle.getMaxRadius().reset(76.66666f, 76.66666f);
        c1597595298(c);
        c752032449(c);
        c420535939(c);
        c785072587(c);
        c2069146210(c);
        c143165714(c);
        c1228693603(c);
        c592212048(c);
        c1813218189(c);
        c729036128(c);
        c979481735(c);
        c1834034071(c);
        c1898171725(c);
        c648230028(c);
        c1507296690(c);
        c845831175(c);
        c866229346(c);
        c1610995847(c);
        c1436859870(c);
        c1850281658(c);
        c209624466(c);
        c1299216389(c);
        c1263433097(c);
        c1433570718(c);
        c533404916(c);
        c1361666732(c);
        c1915830939(c);
        c1752289685(c);
        c1813032396(c);
        c52948811(c);
        c1400463162(c);
        c1805729774(c);
        c1793398239(c);
        c1973987823(c);
        c2114674493(c);
        c805789953(c);
        c1783363977(c);
        c1674759850(c);
        c1323369191(c);
        c236443623(c);
    }

    public void loadInterface(Context c) {
        this.MoveButton = new GameImageSpirit();
        this.MoveButton.setLevel(22.0f);
        this.MoveButton.getImage().loadImageFromResource(c, R.drawable.movebutton);
        this.MoveButton.getSize().reset(155.0f, 155.0f);
        this.MoveButton.setIsUseAbsolutePosition(true);
        getSpiritList().add(this.MoveButton);
        final Vector2DF mvds = this.MoveButton.getDisplaySize();
        mvds.scale(0.5f);
        this.MoveButton.getPosition().reset(5.0f + mvds.getX(), ((float) (GameMaster.getScreenHeight() - 5)) - mvds.getY());
        this.MoveButton.getPositionOffset().resetWith(mvds.negateNew());
        this.MoveButtonSlot = new UI() {
            Vector2DF movevector;
            boolean needmove;

            public void executive(Vector2DF point) {
                this.movevector = point.minusNew(getPosition()).restrainLength(mvds.getX() * 0.4f);
                this.needmove = true;
            }

            /* access modifiers changed from: protected */
            public void updateSelf() {
                if (this.needmove) {
                    PuzzleScene.this.MoveButton.getPosition().resetWith(getPosition().plusNew(this.movevector));
                    PuzzleScene.this.Desktop.getCurrentObservePosition().plus(this.movevector);
                    PuzzleScene.this.refreshDrawCacheBitmap();
                }
                super.updateSelf();
            }

            public boolean isInArea(Vector2DF point) {
                return point.minusNew(getPosition()).getLength() < mvds.getX() + 5.0f;
            }

            public void reset() {
                this.needmove = false;
                PuzzleScene.this.MoveButton.getPosition().resetWith(getPosition());
                PuzzleScene.this.refreshDrawCacheBitmap();
            }
        };
        this.MoveButtonSlot.setLevel(21.0f);
        this.MoveButtonSlot.getImage().loadImageFromResource(c, R.drawable.movebuttonslot);
        this.MoveButtonSlot.getSize().reset(155.0f, 155.0f);
        this.MoveButtonSlot.setIsUseAbsolutePosition(true);
        getSpiritList().add(this.MoveButtonSlot);
        this.MoveButtonSlot.getPosition().resetWith(this.MoveButton.getPosition());
        this.MoveButtonSlot.getPositionOffset().resetWith(this.MoveButton.getPositionOffset());
        this.ZoomInButton = new UI() {
            boolean zoom;

            public void executive(Vector2DF point) {
                this.zoom = true;
            }

            public boolean isInArea(Vector2DF point) {
                return point.minusNew(getPosition()).getLength() < getDisplaySize().getX() / 2.0f;
            }

            public void reset() {
                this.zoom = false;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{java.lang.Math.min(float, float):float}
             arg types: [int, float]
             candidates:
              ClspMth{java.lang.Math.min(double, double):double}
              ClspMth{java.lang.Math.min(long, long):long}
              ClspMth{java.lang.Math.min(int, int):int}
              ClspMth{java.lang.Math.min(float, float):float} */
            /* access modifiers changed from: protected */
            public void updateSelf() {
                if (this.zoom) {
                    PuzzleScene.this.Desktop.setZoom(Math.min(1.3f, PuzzleScene.this.Desktop.getZoom() * 1.05f));
                    PuzzleScene.this.refreshDrawCacheBitmap();
                }
                super.updateSelf();
            }
        };
        this.ZoomInButton.setLevel(21.0f);
        this.ZoomInButton.getImage().loadImageFromResource(c, R.drawable.zoombutton1);
        this.ZoomInButton.getSize().reset(77.0f, 77.0f);
        this.ZoomInButton.setIsUseAbsolutePosition(true);
        getSpiritList().add(this.ZoomInButton);
        Vector2DF zibds = this.ZoomInButton.getDisplaySize().scale(0.5f);
        this.ZoomInButton.getPosition().reset(((float) (GameMaster.getScreenWidth() - 50)) - zibds.getX(), ((float) (GameMaster.getScreenHeight() - 10)) - zibds.getY());
        this.ZoomInButton.getPositionOffset().resetWith(zibds.negateNew());
        this.ZoomOutButton = new UI() {
            boolean zoom;

            public void executive(Vector2DF point) {
                this.zoom = true;
            }

            public boolean isInArea(Vector2DF point) {
                return point.minusNew(getPosition()).getLength() < getDisplaySize().getX() / 2.0f;
            }

            public void reset() {
                this.zoom = false;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{java.lang.Math.max(float, float):float}
             arg types: [int, float]
             candidates:
              ClspMth{java.lang.Math.max(double, double):double}
              ClspMth{java.lang.Math.max(int, int):int}
              ClspMth{java.lang.Math.max(long, long):long}
              ClspMth{java.lang.Math.max(float, float):float} */
            /* access modifiers changed from: protected */
            public void updateSelf() {
                if (this.zoom) {
                    PuzzleScene.this.Desktop.setZoom(Math.max(0.3f, PuzzleScene.this.Desktop.getZoom() * 0.95f));
                    PuzzleScene.this.refreshDrawCacheBitmap();
                    GameMaster.log(this, Float.valueOf(PuzzleScene.this.Desktop.getCurrentObserveAreaRectF().width()));
                }
                super.updateSelf();
            }
        };
        this.ZoomOutButton.setLevel(21.0f);
        this.ZoomOutButton.getImage().loadImageFromResource(c, R.drawable.zoombutton2);
        this.ZoomOutButton.getSize().reset(77.0f, 77.0f);
        this.ZoomOutButton.setIsUseAbsolutePosition(true);
        getSpiritList().add(this.ZoomOutButton);
        Vector2DF zobds = this.ZoomOutButton.getDisplaySize().scale(0.5f);
        this.ZoomOutButton.getPosition().reset((((float) (GameMaster.getScreenWidth() - 100)) - zobds.getX()) - (zibds.getX() * 2.0f), ((float) (GameMaster.getScreenHeight() - 10)) - zobds.getY());
        this.ZoomOutButton.getPositionOffset().resetWith(zobds.negateNew());
        this.ViewFullButton = new UI() {
            public void executive(Vector2DF point) {
                if (PuzzleScene.this.OriginalImage.getImage().getImage() == null) {
                    PuzzleScene.this.OriginalImage.getImage().loadImageFromResource(GameMaster.getContext(), R.drawable.oim);
                }
                PuzzleScene.this.OriginalImage.show();
            }

            public boolean isInArea(Vector2DF point) {
                return point.isIn(PuzzleScene.this.ViewFullButtonRect);
            }

            public void reset() {
                PuzzleScene.this.OriginalImage.hide();
            }
        };
        this.ViewFullButton.getImage().loadImageFromResource(c, R.drawable.viewfull);
        this.ViewFullButton.setLevel(21.0f);
        this.ViewFullButton.setIsUseAbsolutePosition(true);
        this.ViewFullButton.getSize().reset(83.0f, 38.0f);
        getSpiritList().add(this.ViewFullButton);
        this.ViewFullButton.getPosition().reset(((float) (GameMaster.getScreenWidth() - 15)) - this.ViewFullButton.getDisplaySize().getX(), 15.0f);
        this.ViewFullButtonRect = new VectorRect2DF(this.ViewFullButton.getPosition(), this.ViewFullButton.getDisplaySize()).getRectF();
        this.SubmitScoreButton = new UI() {
            public void executive(Vector2DF point) {
                Score score = new Score(Double.valueOf(Math.ceil((double) (PuzzleScene.this.PastTime / 1000))), null);
                score.setMode(17);
                ScoreloopManagerSingleton.get().onGamePlayEnded(score);
                hide();
                Toast.makeText(GameMaster.getContext(), (int) R.string.Submitting, 1).show();
            }

            public boolean isInArea(Vector2DF point) {
                return getVisibleOriginalValue() && point.isIn(PuzzleScene.this.SubmitScoreButtonRect);
            }

            public void reset() {
            }
        };
        this.SubmitScoreButton.getImage().loadImageFromResource(c, R.drawable.submitscore);
        this.SubmitScoreButton.setLevel(21.0f);
        this.SubmitScoreButton.setIsUseAbsolutePosition(true);
        this.SubmitScoreButton.getSize().reset(114.0f, 40.0f);
        getSpiritList().add(this.SubmitScoreButton);
        this.SubmitScoreButton.getPosition().reset(15.0f, 60.0f);
        this.SubmitScoreButtonRect = new VectorRect2DF(this.SubmitScoreButton.getPosition(), this.SubmitScoreButton.getDisplaySize()).getRectF();
        this.SubmitScoreButton.hide();
        this.Time = new GameSpirit() {
            DecimalFormat FMT = new DecimalFormat("00");
            Paint p1;
            Paint p2;

            public GameObject getDisplayContentChild() {
                return null;
            }

            /* access modifiers changed from: protected */
            public void drawSelf(Canvas c, Rect drawArea) {
                if (this.p1 == null) {
                    this.p1 = new Paint();
                    this.p1.setARGB(160, 255, 255, 255);
                    this.p1.setAntiAlias(true);
                    this.p1.setTextSize(28.0f);
                    this.p1.setTypeface(Typeface.createFromAsset(GameMaster.getContext().getAssets(), "fonts/font.ttf"));
                }
                if (this.p2 == null) {
                    this.p2 = new Paint();
                    this.p2.setARGB(100, 0, 0, 0);
                    this.p2.setAntiAlias(true);
                    this.p2.setTextSize(28.0f);
                    this.p2.setTypeface(Typeface.createFromAsset(GameMaster.getContext().getAssets(), "fonts/font.ttf"));
                }
                long t = PuzzleScene.this.getIsFinished() ? PuzzleScene.this.PastTime : PuzzleScene.this.PastTime + (new Date().getTime() - PuzzleScene.this.StartTime);
                long h = t / 3600000;
                long m = (t - (((1000 * h) * 60) * 60)) / 60000;
                String str = String.valueOf(this.FMT.format(h)) + ":" + this.FMT.format(m) + ":" + this.FMT.format(((t - (((1000 * h) * 60) * 60)) - ((1000 * m) * 60)) / 1000) + (PuzzleScene.this.getIsFinished() ? " Finished" : "");
                c.drawText(str, 16.0f, 44.0f, this.p2);
                c.drawText(str, 15.0f, 43.0f, this.p1);
                super.drawSelf(c, drawArea);
            }
        };
        this.Time.setLevel(40.0f);
        getSpiritList().add(this.Time);
    }

    public float getMaxPuzzleLevel() {
        float maxlvl = 0.0f;
        Iterator<Puzzle> it = this.Desktop.getPuzzleList().iterator();
        while (it.hasNext()) {
            Puzzle f = it.next();
            if (f.getLevel() <= 10.0f) {
                maxlvl = Math.max(f.getLevel(), maxlvl);
            }
        }
        return maxlvl;
    }

    public Puzzle getTouchPuzzle(Vector2DF v) {
        Vector2DF rv = this.Desktop.reMapPoint(v);
        Puzzle o = null;
        float olen = 99999.0f;
        for (int i = getSpiritList().size() - 1; i >= 0; i--) {
            if (getSpiritList().get(i) instanceof Puzzle) {
                Puzzle p = (Puzzle) getSpiritList().get(i);
                float len = p.getPositionInDesktop().minusNew(rv).getLength();
                if (len <= Puzzle.getRealitySize().getX() * 0.4f) {
                    return p;
                }
                if (len <= Puzzle.getRealitySize().getX() && olen > len) {
                    o = p;
                    olen = len;
                }
            }
        }
        return o;
    }

    public void startDrawSelectRect(Vector2DF startDragPoint, Vector2DF v) {
        this.selectRect = new VectorRect2DF(startDragPoint, v.minusNew(startDragPoint)).getFixedRectF();
    }

    public void stopDrawSelectRect() {
        this.selectRect = null;
    }

    public ArrayList<Puzzle> getSelectPuzzle(Vector2DF startDragPoint, Vector2DF v) {
        Vector2DF rs = this.Desktop.reMapPoint(startDragPoint);
        ArrayList<Puzzle> l = new ArrayList<>();
        RectF rect = new VectorRect2DF(rs, this.Desktop.reMapPoint(v).minusNew(rs)).getFixedRectF();
        float xo = Puzzle.getRealitySize().getX() / 3.0f;
        float yo = Puzzle.getRealitySize().getY() / 3.0f;
        RectF sr = new RectF(rect.left - xo, rect.top - yo, rect.right + xo, rect.bottom + yo);
        Iterator<Puzzle> it = this.Desktop.getPuzzleList().iterator();
        while (it.hasNext()) {
            Puzzle f = it.next();
            if (f.getPositionInDesktop().isIn(sr)) {
                l.add(f);
            }
        }
        return l;
    }

    public void loadPuzzleState() {
        Center c = (Center) GameMaster.getContext().getApplicationContext();
        SharedPreferences s = c.getSharedPreferences();
        Iterator<Puzzle> it = this.Desktop.getPuzzleList().iterator();
        while (it.hasNext()) {
            it.next().load(c, s);
        }
    }

    public void savePuzzleState() {
        Center c = (Center) GameMaster.getContext().getApplicationContext();
        SharedPreferences.Editor e = c.getSharedPreferences().edit();
        Iterator<Puzzle> it = this.Desktop.getPuzzleList().iterator();
        while (it.hasNext()) {
            it.next().save(c, e);
        }
        e.commit();
    }

    public void reset() {
        this.Controller.reset();
        Iterator<Puzzle> it = this.Desktop.getPuzzleList().iterator();
        while (it.hasNext()) {
            Puzzle f = it.next();
            f.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
            this.Desktop.RandomlyPlaced(f);
        }
        refreshDrawCacheBitmap();
        setIsFinishedToDefault();
        this.StartTime = new Date().getTime();
        this.PastTime = 0;
        this.SubmitScoreButton.hide();
        saveGameState();
        savePuzzleState();
    }

    /* access modifiers changed from: package-private */
    public void c1597595298(Context c) {
        Puzzle p1597595298 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1597595298(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1597595298(editor, this);
            }
        };
        p1597595298.setID(1597595298);
        p1597595298.setName("1597595298");
        p1597595298.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1597595298);
        this.Desktop.RandomlyPlaced(p1597595298);
        p1597595298.setTopEdgeType(EdgeType.Flat);
        p1597595298.setBottomEdgeType(EdgeType.Concave);
        p1597595298.setLeftEdgeType(EdgeType.Flat);
        p1597595298.setRightEdgeType(EdgeType.Convex);
        p1597595298.setTopExactPuzzleID(-1);
        p1597595298.setBottomExactPuzzleID(752032449);
        p1597595298.setLeftExactPuzzleID(-1);
        p1597595298.setRightExactPuzzleID(143165714);
        p1597595298.getDisplayImage().loadImageFromResource(c, R.drawable.p1597595298h);
        p1597595298.setExactRow(0);
        p1597595298.setExactColumn(0);
        p1597595298.getSize().reset(126.6667f, 100.0f);
        p1597595298.getPositionOffset().reset(-50.0f, -50.0f);
        p1597595298.setIsUseAbsolutePosition(true);
        p1597595298.setIsUseAbsoluteSize(true);
        p1597595298.getImage().setIsUseAbsolutePosition(true);
        p1597595298.getImage().setIsUseAbsoluteSize(true);
        p1597595298.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1597595298.resetPosition();
        getSpiritList().add(p1597595298);
    }

    /* access modifiers changed from: package-private */
    public void c752032449(Context c) {
        Puzzle p752032449 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load752032449(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save752032449(editor, this);
            }
        };
        p752032449.setID(752032449);
        p752032449.setName("752032449");
        p752032449.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p752032449);
        this.Desktop.RandomlyPlaced(p752032449);
        p752032449.setTopEdgeType(EdgeType.Convex);
        p752032449.setBottomEdgeType(EdgeType.Concave);
        p752032449.setLeftEdgeType(EdgeType.Flat);
        p752032449.setRightEdgeType(EdgeType.Concave);
        p752032449.setTopExactPuzzleID(1597595298);
        p752032449.setBottomExactPuzzleID(420535939);
        p752032449.setLeftExactPuzzleID(-1);
        p752032449.setRightExactPuzzleID(1228693603);
        p752032449.getDisplayImage().loadImageFromResource(c, R.drawable.p752032449h);
        p752032449.setExactRow(1);
        p752032449.setExactColumn(0);
        p752032449.getSize().reset(100.0f, 126.6667f);
        p752032449.getPositionOffset().reset(-50.0f, -76.66666f);
        p752032449.setIsUseAbsolutePosition(true);
        p752032449.setIsUseAbsoluteSize(true);
        p752032449.getImage().setIsUseAbsolutePosition(true);
        p752032449.getImage().setIsUseAbsoluteSize(true);
        p752032449.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p752032449.resetPosition();
        getSpiritList().add(p752032449);
    }

    /* access modifiers changed from: package-private */
    public void c420535939(Context c) {
        Puzzle p420535939 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load420535939(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save420535939(editor, this);
            }
        };
        p420535939.setID(420535939);
        p420535939.setName("420535939");
        p420535939.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p420535939);
        this.Desktop.RandomlyPlaced(p420535939);
        p420535939.setTopEdgeType(EdgeType.Convex);
        p420535939.setBottomEdgeType(EdgeType.Convex);
        p420535939.setLeftEdgeType(EdgeType.Flat);
        p420535939.setRightEdgeType(EdgeType.Concave);
        p420535939.setTopExactPuzzleID(752032449);
        p420535939.setBottomExactPuzzleID(785072587);
        p420535939.setLeftExactPuzzleID(-1);
        p420535939.setRightExactPuzzleID(592212048);
        p420535939.getDisplayImage().loadImageFromResource(c, R.drawable.p420535939h);
        p420535939.setExactRow(2);
        p420535939.setExactColumn(0);
        p420535939.getSize().reset(100.0f, 153.3333f);
        p420535939.getPositionOffset().reset(-50.0f, -76.66666f);
        p420535939.setIsUseAbsolutePosition(true);
        p420535939.setIsUseAbsoluteSize(true);
        p420535939.getImage().setIsUseAbsolutePosition(true);
        p420535939.getImage().setIsUseAbsoluteSize(true);
        p420535939.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p420535939.resetPosition();
        getSpiritList().add(p420535939);
    }

    /* access modifiers changed from: package-private */
    public void c785072587(Context c) {
        Puzzle p785072587 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load785072587(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save785072587(editor, this);
            }
        };
        p785072587.setID(785072587);
        p785072587.setName("785072587");
        p785072587.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p785072587);
        this.Desktop.RandomlyPlaced(p785072587);
        p785072587.setTopEdgeType(EdgeType.Concave);
        p785072587.setBottomEdgeType(EdgeType.Concave);
        p785072587.setLeftEdgeType(EdgeType.Flat);
        p785072587.setRightEdgeType(EdgeType.Convex);
        p785072587.setTopExactPuzzleID(420535939);
        p785072587.setBottomExactPuzzleID(2069146210);
        p785072587.setLeftExactPuzzleID(-1);
        p785072587.setRightExactPuzzleID(1813218189);
        p785072587.getDisplayImage().loadImageFromResource(c, R.drawable.p785072587h);
        p785072587.setExactRow(3);
        p785072587.setExactColumn(0);
        p785072587.getSize().reset(126.6667f, 100.0f);
        p785072587.getPositionOffset().reset(-50.0f, -50.0f);
        p785072587.setIsUseAbsolutePosition(true);
        p785072587.setIsUseAbsoluteSize(true);
        p785072587.getImage().setIsUseAbsolutePosition(true);
        p785072587.getImage().setIsUseAbsoluteSize(true);
        p785072587.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p785072587.resetPosition();
        getSpiritList().add(p785072587);
    }

    /* access modifiers changed from: package-private */
    public void c2069146210(Context c) {
        Puzzle p2069146210 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load2069146210(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save2069146210(editor, this);
            }
        };
        p2069146210.setID(2069146210);
        p2069146210.setName("2069146210");
        p2069146210.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p2069146210);
        this.Desktop.RandomlyPlaced(p2069146210);
        p2069146210.setTopEdgeType(EdgeType.Convex);
        p2069146210.setBottomEdgeType(EdgeType.Flat);
        p2069146210.setLeftEdgeType(EdgeType.Flat);
        p2069146210.setRightEdgeType(EdgeType.Concave);
        p2069146210.setTopExactPuzzleID(785072587);
        p2069146210.setBottomExactPuzzleID(-1);
        p2069146210.setLeftExactPuzzleID(-1);
        p2069146210.setRightExactPuzzleID(729036128);
        p2069146210.getDisplayImage().loadImageFromResource(c, R.drawable.p2069146210h);
        p2069146210.setExactRow(4);
        p2069146210.setExactColumn(0);
        p2069146210.getSize().reset(100.0f, 126.6667f);
        p2069146210.getPositionOffset().reset(-50.0f, -76.66666f);
        p2069146210.setIsUseAbsolutePosition(true);
        p2069146210.setIsUseAbsoluteSize(true);
        p2069146210.getImage().setIsUseAbsolutePosition(true);
        p2069146210.getImage().setIsUseAbsoluteSize(true);
        p2069146210.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p2069146210.resetPosition();
        getSpiritList().add(p2069146210);
    }

    /* access modifiers changed from: package-private */
    public void c143165714(Context c) {
        Puzzle p143165714 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load143165714(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save143165714(editor, this);
            }
        };
        p143165714.setID(143165714);
        p143165714.setName("143165714");
        p143165714.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p143165714);
        this.Desktop.RandomlyPlaced(p143165714);
        p143165714.setTopEdgeType(EdgeType.Flat);
        p143165714.setBottomEdgeType(EdgeType.Concave);
        p143165714.setLeftEdgeType(EdgeType.Concave);
        p143165714.setRightEdgeType(EdgeType.Convex);
        p143165714.setTopExactPuzzleID(-1);
        p143165714.setBottomExactPuzzleID(1228693603);
        p143165714.setLeftExactPuzzleID(1597595298);
        p143165714.setRightExactPuzzleID(979481735);
        p143165714.getDisplayImage().loadImageFromResource(c, R.drawable.p143165714h);
        p143165714.setExactRow(0);
        p143165714.setExactColumn(1);
        p143165714.getSize().reset(126.6667f, 100.0f);
        p143165714.getPositionOffset().reset(-50.0f, -50.0f);
        p143165714.setIsUseAbsolutePosition(true);
        p143165714.setIsUseAbsoluteSize(true);
        p143165714.getImage().setIsUseAbsolutePosition(true);
        p143165714.getImage().setIsUseAbsoluteSize(true);
        p143165714.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p143165714.resetPosition();
        getSpiritList().add(p143165714);
    }

    /* access modifiers changed from: package-private */
    public void c1228693603(Context c) {
        Puzzle p1228693603 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1228693603(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1228693603(editor, this);
            }
        };
        p1228693603.setID(1228693603);
        p1228693603.setName("1228693603");
        p1228693603.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1228693603);
        this.Desktop.RandomlyPlaced(p1228693603);
        p1228693603.setTopEdgeType(EdgeType.Convex);
        p1228693603.setBottomEdgeType(EdgeType.Concave);
        p1228693603.setLeftEdgeType(EdgeType.Convex);
        p1228693603.setRightEdgeType(EdgeType.Concave);
        p1228693603.setTopExactPuzzleID(143165714);
        p1228693603.setBottomExactPuzzleID(592212048);
        p1228693603.setLeftExactPuzzleID(752032449);
        p1228693603.setRightExactPuzzleID(1834034071);
        p1228693603.getDisplayImage().loadImageFromResource(c, R.drawable.p1228693603h);
        p1228693603.setExactRow(1);
        p1228693603.setExactColumn(1);
        p1228693603.getSize().reset(126.6667f, 126.6667f);
        p1228693603.getPositionOffset().reset(-76.66666f, -76.66666f);
        p1228693603.setIsUseAbsolutePosition(true);
        p1228693603.setIsUseAbsoluteSize(true);
        p1228693603.getImage().setIsUseAbsolutePosition(true);
        p1228693603.getImage().setIsUseAbsoluteSize(true);
        p1228693603.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1228693603.resetPosition();
        getSpiritList().add(p1228693603);
    }

    /* access modifiers changed from: package-private */
    public void c592212048(Context c) {
        Puzzle p592212048 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load592212048(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save592212048(editor, this);
            }
        };
        p592212048.setID(592212048);
        p592212048.setName("592212048");
        p592212048.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p592212048);
        this.Desktop.RandomlyPlaced(p592212048);
        p592212048.setTopEdgeType(EdgeType.Convex);
        p592212048.setBottomEdgeType(EdgeType.Convex);
        p592212048.setLeftEdgeType(EdgeType.Convex);
        p592212048.setRightEdgeType(EdgeType.Concave);
        p592212048.setTopExactPuzzleID(1228693603);
        p592212048.setBottomExactPuzzleID(1813218189);
        p592212048.setLeftExactPuzzleID(420535939);
        p592212048.setRightExactPuzzleID(1898171725);
        p592212048.getDisplayImage().loadImageFromResource(c, R.drawable.p592212048h);
        p592212048.setExactRow(2);
        p592212048.setExactColumn(1);
        p592212048.getSize().reset(126.6667f, 153.3333f);
        p592212048.getPositionOffset().reset(-76.66666f, -76.66666f);
        p592212048.setIsUseAbsolutePosition(true);
        p592212048.setIsUseAbsoluteSize(true);
        p592212048.getImage().setIsUseAbsolutePosition(true);
        p592212048.getImage().setIsUseAbsoluteSize(true);
        p592212048.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p592212048.resetPosition();
        getSpiritList().add(p592212048);
    }

    /* access modifiers changed from: package-private */
    public void c1813218189(Context c) {
        Puzzle p1813218189 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1813218189(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1813218189(editor, this);
            }
        };
        p1813218189.setID(1813218189);
        p1813218189.setName("1813218189");
        p1813218189.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1813218189);
        this.Desktop.RandomlyPlaced(p1813218189);
        p1813218189.setTopEdgeType(EdgeType.Concave);
        p1813218189.setBottomEdgeType(EdgeType.Concave);
        p1813218189.setLeftEdgeType(EdgeType.Concave);
        p1813218189.setRightEdgeType(EdgeType.Convex);
        p1813218189.setTopExactPuzzleID(592212048);
        p1813218189.setBottomExactPuzzleID(729036128);
        p1813218189.setLeftExactPuzzleID(785072587);
        p1813218189.setRightExactPuzzleID(648230028);
        p1813218189.getDisplayImage().loadImageFromResource(c, R.drawable.p1813218189h);
        p1813218189.setExactRow(3);
        p1813218189.setExactColumn(1);
        p1813218189.getSize().reset(126.6667f, 100.0f);
        p1813218189.getPositionOffset().reset(-50.0f, -50.0f);
        p1813218189.setIsUseAbsolutePosition(true);
        p1813218189.setIsUseAbsoluteSize(true);
        p1813218189.getImage().setIsUseAbsolutePosition(true);
        p1813218189.getImage().setIsUseAbsoluteSize(true);
        p1813218189.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1813218189.resetPosition();
        getSpiritList().add(p1813218189);
    }

    /* access modifiers changed from: package-private */
    public void c729036128(Context c) {
        Puzzle p729036128 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load729036128(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save729036128(editor, this);
            }
        };
        p729036128.setID(729036128);
        p729036128.setName("729036128");
        p729036128.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p729036128);
        this.Desktop.RandomlyPlaced(p729036128);
        p729036128.setTopEdgeType(EdgeType.Convex);
        p729036128.setBottomEdgeType(EdgeType.Flat);
        p729036128.setLeftEdgeType(EdgeType.Convex);
        p729036128.setRightEdgeType(EdgeType.Convex);
        p729036128.setTopExactPuzzleID(1813218189);
        p729036128.setBottomExactPuzzleID(-1);
        p729036128.setLeftExactPuzzleID(2069146210);
        p729036128.setRightExactPuzzleID(1507296690);
        p729036128.getDisplayImage().loadImageFromResource(c, R.drawable.p729036128h);
        p729036128.setExactRow(4);
        p729036128.setExactColumn(1);
        p729036128.getSize().reset(153.3333f, 126.6667f);
        p729036128.getPositionOffset().reset(-76.66666f, -76.66666f);
        p729036128.setIsUseAbsolutePosition(true);
        p729036128.setIsUseAbsoluteSize(true);
        p729036128.getImage().setIsUseAbsolutePosition(true);
        p729036128.getImage().setIsUseAbsoluteSize(true);
        p729036128.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p729036128.resetPosition();
        getSpiritList().add(p729036128);
    }

    /* access modifiers changed from: package-private */
    public void c979481735(Context c) {
        Puzzle p979481735 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load979481735(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save979481735(editor, this);
            }
        };
        p979481735.setID(979481735);
        p979481735.setName("979481735");
        p979481735.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p979481735);
        this.Desktop.RandomlyPlaced(p979481735);
        p979481735.setTopEdgeType(EdgeType.Flat);
        p979481735.setBottomEdgeType(EdgeType.Concave);
        p979481735.setLeftEdgeType(EdgeType.Concave);
        p979481735.setRightEdgeType(EdgeType.Convex);
        p979481735.setTopExactPuzzleID(-1);
        p979481735.setBottomExactPuzzleID(1834034071);
        p979481735.setLeftExactPuzzleID(143165714);
        p979481735.setRightExactPuzzleID(845831175);
        p979481735.getDisplayImage().loadImageFromResource(c, R.drawable.p979481735h);
        p979481735.setExactRow(0);
        p979481735.setExactColumn(2);
        p979481735.getSize().reset(126.6667f, 100.0f);
        p979481735.getPositionOffset().reset(-50.0f, -50.0f);
        p979481735.setIsUseAbsolutePosition(true);
        p979481735.setIsUseAbsoluteSize(true);
        p979481735.getImage().setIsUseAbsolutePosition(true);
        p979481735.getImage().setIsUseAbsoluteSize(true);
        p979481735.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p979481735.resetPosition();
        getSpiritList().add(p979481735);
    }

    /* access modifiers changed from: package-private */
    public void c1834034071(Context c) {
        Puzzle p1834034071 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1834034071(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1834034071(editor, this);
            }
        };
        p1834034071.setID(1834034071);
        p1834034071.setName("1834034071");
        p1834034071.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1834034071);
        this.Desktop.RandomlyPlaced(p1834034071);
        p1834034071.setTopEdgeType(EdgeType.Convex);
        p1834034071.setBottomEdgeType(EdgeType.Convex);
        p1834034071.setLeftEdgeType(EdgeType.Convex);
        p1834034071.setRightEdgeType(EdgeType.Concave);
        p1834034071.setTopExactPuzzleID(979481735);
        p1834034071.setBottomExactPuzzleID(1898171725);
        p1834034071.setLeftExactPuzzleID(1228693603);
        p1834034071.setRightExactPuzzleID(866229346);
        p1834034071.getDisplayImage().loadImageFromResource(c, R.drawable.p1834034071h);
        p1834034071.setExactRow(1);
        p1834034071.setExactColumn(2);
        p1834034071.getSize().reset(126.6667f, 153.3333f);
        p1834034071.getPositionOffset().reset(-76.66666f, -76.66666f);
        p1834034071.setIsUseAbsolutePosition(true);
        p1834034071.setIsUseAbsoluteSize(true);
        p1834034071.getImage().setIsUseAbsolutePosition(true);
        p1834034071.getImage().setIsUseAbsoluteSize(true);
        p1834034071.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1834034071.resetPosition();
        getSpiritList().add(p1834034071);
    }

    /* access modifiers changed from: package-private */
    public void c1898171725(Context c) {
        Puzzle p1898171725 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1898171725(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1898171725(editor, this);
            }
        };
        p1898171725.setID(1898171725);
        p1898171725.setName("1898171725");
        p1898171725.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1898171725);
        this.Desktop.RandomlyPlaced(p1898171725);
        p1898171725.setTopEdgeType(EdgeType.Concave);
        p1898171725.setBottomEdgeType(EdgeType.Concave);
        p1898171725.setLeftEdgeType(EdgeType.Convex);
        p1898171725.setRightEdgeType(EdgeType.Concave);
        p1898171725.setTopExactPuzzleID(1834034071);
        p1898171725.setBottomExactPuzzleID(648230028);
        p1898171725.setLeftExactPuzzleID(592212048);
        p1898171725.setRightExactPuzzleID(1610995847);
        p1898171725.getDisplayImage().loadImageFromResource(c, R.drawable.p1898171725h);
        p1898171725.setExactRow(2);
        p1898171725.setExactColumn(2);
        p1898171725.getSize().reset(126.6667f, 100.0f);
        p1898171725.getPositionOffset().reset(-76.66666f, -50.0f);
        p1898171725.setIsUseAbsolutePosition(true);
        p1898171725.setIsUseAbsoluteSize(true);
        p1898171725.getImage().setIsUseAbsolutePosition(true);
        p1898171725.getImage().setIsUseAbsoluteSize(true);
        p1898171725.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1898171725.resetPosition();
        getSpiritList().add(p1898171725);
    }

    /* access modifiers changed from: package-private */
    public void c648230028(Context c) {
        Puzzle p648230028 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load648230028(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save648230028(editor, this);
            }
        };
        p648230028.setID(648230028);
        p648230028.setName("648230028");
        p648230028.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p648230028);
        this.Desktop.RandomlyPlaced(p648230028);
        p648230028.setTopEdgeType(EdgeType.Convex);
        p648230028.setBottomEdgeType(EdgeType.Concave);
        p648230028.setLeftEdgeType(EdgeType.Concave);
        p648230028.setRightEdgeType(EdgeType.Concave);
        p648230028.setTopExactPuzzleID(1898171725);
        p648230028.setBottomExactPuzzleID(1507296690);
        p648230028.setLeftExactPuzzleID(1813218189);
        p648230028.setRightExactPuzzleID(1436859870);
        p648230028.getDisplayImage().loadImageFromResource(c, R.drawable.p648230028h);
        p648230028.setExactRow(3);
        p648230028.setExactColumn(2);
        p648230028.getSize().reset(100.0f, 126.6667f);
        p648230028.getPositionOffset().reset(-50.0f, -76.66666f);
        p648230028.setIsUseAbsolutePosition(true);
        p648230028.setIsUseAbsoluteSize(true);
        p648230028.getImage().setIsUseAbsolutePosition(true);
        p648230028.getImage().setIsUseAbsoluteSize(true);
        p648230028.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p648230028.resetPosition();
        getSpiritList().add(p648230028);
    }

    /* access modifiers changed from: package-private */
    public void c1507296690(Context c) {
        Puzzle p1507296690 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1507296690(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1507296690(editor, this);
            }
        };
        p1507296690.setID(1507296690);
        p1507296690.setName("1507296690");
        p1507296690.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1507296690);
        this.Desktop.RandomlyPlaced(p1507296690);
        p1507296690.setTopEdgeType(EdgeType.Convex);
        p1507296690.setBottomEdgeType(EdgeType.Flat);
        p1507296690.setLeftEdgeType(EdgeType.Concave);
        p1507296690.setRightEdgeType(EdgeType.Concave);
        p1507296690.setTopExactPuzzleID(648230028);
        p1507296690.setBottomExactPuzzleID(-1);
        p1507296690.setLeftExactPuzzleID(729036128);
        p1507296690.setRightExactPuzzleID(1850281658);
        p1507296690.getDisplayImage().loadImageFromResource(c, R.drawable.p1507296690h);
        p1507296690.setExactRow(4);
        p1507296690.setExactColumn(2);
        p1507296690.getSize().reset(100.0f, 126.6667f);
        p1507296690.getPositionOffset().reset(-50.0f, -76.66666f);
        p1507296690.setIsUseAbsolutePosition(true);
        p1507296690.setIsUseAbsoluteSize(true);
        p1507296690.getImage().setIsUseAbsolutePosition(true);
        p1507296690.getImage().setIsUseAbsoluteSize(true);
        p1507296690.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1507296690.resetPosition();
        getSpiritList().add(p1507296690);
    }

    /* access modifiers changed from: package-private */
    public void c845831175(Context c) {
        Puzzle p845831175 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load845831175(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save845831175(editor, this);
            }
        };
        p845831175.setID(845831175);
        p845831175.setName("845831175");
        p845831175.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p845831175);
        this.Desktop.RandomlyPlaced(p845831175);
        p845831175.setTopEdgeType(EdgeType.Flat);
        p845831175.setBottomEdgeType(EdgeType.Convex);
        p845831175.setLeftEdgeType(EdgeType.Concave);
        p845831175.setRightEdgeType(EdgeType.Convex);
        p845831175.setTopExactPuzzleID(-1);
        p845831175.setBottomExactPuzzleID(866229346);
        p845831175.setLeftExactPuzzleID(979481735);
        p845831175.setRightExactPuzzleID(209624466);
        p845831175.getDisplayImage().loadImageFromResource(c, R.drawable.p845831175h);
        p845831175.setExactRow(0);
        p845831175.setExactColumn(3);
        p845831175.getSize().reset(126.6667f, 126.6667f);
        p845831175.getPositionOffset().reset(-50.0f, -50.0f);
        p845831175.setIsUseAbsolutePosition(true);
        p845831175.setIsUseAbsoluteSize(true);
        p845831175.getImage().setIsUseAbsolutePosition(true);
        p845831175.getImage().setIsUseAbsoluteSize(true);
        p845831175.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p845831175.resetPosition();
        getSpiritList().add(p845831175);
    }

    /* access modifiers changed from: package-private */
    public void c866229346(Context c) {
        Puzzle p866229346 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load866229346(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save866229346(editor, this);
            }
        };
        p866229346.setID(866229346);
        p866229346.setName("866229346");
        p866229346.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p866229346);
        this.Desktop.RandomlyPlaced(p866229346);
        p866229346.setTopEdgeType(EdgeType.Concave);
        p866229346.setBottomEdgeType(EdgeType.Convex);
        p866229346.setLeftEdgeType(EdgeType.Convex);
        p866229346.setRightEdgeType(EdgeType.Convex);
        p866229346.setTopExactPuzzleID(845831175);
        p866229346.setBottomExactPuzzleID(1610995847);
        p866229346.setLeftExactPuzzleID(1834034071);
        p866229346.setRightExactPuzzleID(1299216389);
        p866229346.getDisplayImage().loadImageFromResource(c, R.drawable.p866229346h);
        p866229346.setExactRow(1);
        p866229346.setExactColumn(3);
        p866229346.getSize().reset(153.3333f, 126.6667f);
        p866229346.getPositionOffset().reset(-76.66666f, -50.0f);
        p866229346.setIsUseAbsolutePosition(true);
        p866229346.setIsUseAbsoluteSize(true);
        p866229346.getImage().setIsUseAbsolutePosition(true);
        p866229346.getImage().setIsUseAbsoluteSize(true);
        p866229346.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p866229346.resetPosition();
        getSpiritList().add(p866229346);
    }

    /* access modifiers changed from: package-private */
    public void c1610995847(Context c) {
        Puzzle p1610995847 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1610995847(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1610995847(editor, this);
            }
        };
        p1610995847.setID(1610995847);
        p1610995847.setName("1610995847");
        p1610995847.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1610995847);
        this.Desktop.RandomlyPlaced(p1610995847);
        p1610995847.setTopEdgeType(EdgeType.Concave);
        p1610995847.setBottomEdgeType(EdgeType.Convex);
        p1610995847.setLeftEdgeType(EdgeType.Convex);
        p1610995847.setRightEdgeType(EdgeType.Convex);
        p1610995847.setTopExactPuzzleID(866229346);
        p1610995847.setBottomExactPuzzleID(1436859870);
        p1610995847.setLeftExactPuzzleID(1898171725);
        p1610995847.setRightExactPuzzleID(1263433097);
        p1610995847.getDisplayImage().loadImageFromResource(c, R.drawable.p1610995847h);
        p1610995847.setExactRow(2);
        p1610995847.setExactColumn(3);
        p1610995847.getSize().reset(153.3333f, 126.6667f);
        p1610995847.getPositionOffset().reset(-76.66666f, -50.0f);
        p1610995847.setIsUseAbsolutePosition(true);
        p1610995847.setIsUseAbsoluteSize(true);
        p1610995847.getImage().setIsUseAbsolutePosition(true);
        p1610995847.getImage().setIsUseAbsoluteSize(true);
        p1610995847.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1610995847.resetPosition();
        getSpiritList().add(p1610995847);
    }

    /* access modifiers changed from: package-private */
    public void c1436859870(Context c) {
        Puzzle p1436859870 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1436859870(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1436859870(editor, this);
            }
        };
        p1436859870.setID(1436859870);
        p1436859870.setName("1436859870");
        p1436859870.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1436859870);
        this.Desktop.RandomlyPlaced(p1436859870);
        p1436859870.setTopEdgeType(EdgeType.Concave);
        p1436859870.setBottomEdgeType(EdgeType.Concave);
        p1436859870.setLeftEdgeType(EdgeType.Convex);
        p1436859870.setRightEdgeType(EdgeType.Concave);
        p1436859870.setTopExactPuzzleID(1610995847);
        p1436859870.setBottomExactPuzzleID(1850281658);
        p1436859870.setLeftExactPuzzleID(648230028);
        p1436859870.setRightExactPuzzleID(1433570718);
        p1436859870.getDisplayImage().loadImageFromResource(c, R.drawable.p1436859870h);
        p1436859870.setExactRow(3);
        p1436859870.setExactColumn(3);
        p1436859870.getSize().reset(126.6667f, 100.0f);
        p1436859870.getPositionOffset().reset(-76.66666f, -50.0f);
        p1436859870.setIsUseAbsolutePosition(true);
        p1436859870.setIsUseAbsoluteSize(true);
        p1436859870.getImage().setIsUseAbsolutePosition(true);
        p1436859870.getImage().setIsUseAbsoluteSize(true);
        p1436859870.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1436859870.resetPosition();
        getSpiritList().add(p1436859870);
    }

    /* access modifiers changed from: package-private */
    public void c1850281658(Context c) {
        Puzzle p1850281658 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1850281658(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1850281658(editor, this);
            }
        };
        p1850281658.setID(1850281658);
        p1850281658.setName("1850281658");
        p1850281658.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1850281658);
        this.Desktop.RandomlyPlaced(p1850281658);
        p1850281658.setTopEdgeType(EdgeType.Convex);
        p1850281658.setBottomEdgeType(EdgeType.Flat);
        p1850281658.setLeftEdgeType(EdgeType.Convex);
        p1850281658.setRightEdgeType(EdgeType.Concave);
        p1850281658.setTopExactPuzzleID(1436859870);
        p1850281658.setBottomExactPuzzleID(-1);
        p1850281658.setLeftExactPuzzleID(1507296690);
        p1850281658.setRightExactPuzzleID(533404916);
        p1850281658.getDisplayImage().loadImageFromResource(c, R.drawable.p1850281658h);
        p1850281658.setExactRow(4);
        p1850281658.setExactColumn(3);
        p1850281658.getSize().reset(126.6667f, 126.6667f);
        p1850281658.getPositionOffset().reset(-76.66666f, -76.66666f);
        p1850281658.setIsUseAbsolutePosition(true);
        p1850281658.setIsUseAbsoluteSize(true);
        p1850281658.getImage().setIsUseAbsolutePosition(true);
        p1850281658.getImage().setIsUseAbsoluteSize(true);
        p1850281658.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1850281658.resetPosition();
        getSpiritList().add(p1850281658);
    }

    /* access modifiers changed from: package-private */
    public void c209624466(Context c) {
        Puzzle p209624466 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load209624466(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save209624466(editor, this);
            }
        };
        p209624466.setID(209624466);
        p209624466.setName("209624466");
        p209624466.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p209624466);
        this.Desktop.RandomlyPlaced(p209624466);
        p209624466.setTopEdgeType(EdgeType.Flat);
        p209624466.setBottomEdgeType(EdgeType.Concave);
        p209624466.setLeftEdgeType(EdgeType.Concave);
        p209624466.setRightEdgeType(EdgeType.Convex);
        p209624466.setTopExactPuzzleID(-1);
        p209624466.setBottomExactPuzzleID(1299216389);
        p209624466.setLeftExactPuzzleID(845831175);
        p209624466.setRightExactPuzzleID(1361666732);
        p209624466.getDisplayImage().loadImageFromResource(c, R.drawable.p209624466h);
        p209624466.setExactRow(0);
        p209624466.setExactColumn(4);
        p209624466.getSize().reset(126.6667f, 100.0f);
        p209624466.getPositionOffset().reset(-50.0f, -50.0f);
        p209624466.setIsUseAbsolutePosition(true);
        p209624466.setIsUseAbsoluteSize(true);
        p209624466.getImage().setIsUseAbsolutePosition(true);
        p209624466.getImage().setIsUseAbsoluteSize(true);
        p209624466.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p209624466.resetPosition();
        getSpiritList().add(p209624466);
    }

    /* access modifiers changed from: package-private */
    public void c1299216389(Context c) {
        Puzzle p1299216389 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1299216389(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1299216389(editor, this);
            }
        };
        p1299216389.setID(1299216389);
        p1299216389.setName("1299216389");
        p1299216389.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1299216389);
        this.Desktop.RandomlyPlaced(p1299216389);
        p1299216389.setTopEdgeType(EdgeType.Convex);
        p1299216389.setBottomEdgeType(EdgeType.Convex);
        p1299216389.setLeftEdgeType(EdgeType.Concave);
        p1299216389.setRightEdgeType(EdgeType.Concave);
        p1299216389.setTopExactPuzzleID(209624466);
        p1299216389.setBottomExactPuzzleID(1263433097);
        p1299216389.setLeftExactPuzzleID(866229346);
        p1299216389.setRightExactPuzzleID(1915830939);
        p1299216389.getDisplayImage().loadImageFromResource(c, R.drawable.p1299216389h);
        p1299216389.setExactRow(1);
        p1299216389.setExactColumn(4);
        p1299216389.getSize().reset(100.0f, 153.3333f);
        p1299216389.getPositionOffset().reset(-50.0f, -76.66666f);
        p1299216389.setIsUseAbsolutePosition(true);
        p1299216389.setIsUseAbsoluteSize(true);
        p1299216389.getImage().setIsUseAbsolutePosition(true);
        p1299216389.getImage().setIsUseAbsoluteSize(true);
        p1299216389.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1299216389.resetPosition();
        getSpiritList().add(p1299216389);
    }

    /* access modifiers changed from: package-private */
    public void c1263433097(Context c) {
        Puzzle p1263433097 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1263433097(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1263433097(editor, this);
            }
        };
        p1263433097.setID(1263433097);
        p1263433097.setName("1263433097");
        p1263433097.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1263433097);
        this.Desktop.RandomlyPlaced(p1263433097);
        p1263433097.setTopEdgeType(EdgeType.Concave);
        p1263433097.setBottomEdgeType(EdgeType.Convex);
        p1263433097.setLeftEdgeType(EdgeType.Concave);
        p1263433097.setRightEdgeType(EdgeType.Convex);
        p1263433097.setTopExactPuzzleID(1299216389);
        p1263433097.setBottomExactPuzzleID(1433570718);
        p1263433097.setLeftExactPuzzleID(1610995847);
        p1263433097.setRightExactPuzzleID(1752289685);
        p1263433097.getDisplayImage().loadImageFromResource(c, R.drawable.p1263433097h);
        p1263433097.setExactRow(2);
        p1263433097.setExactColumn(4);
        p1263433097.getSize().reset(126.6667f, 126.6667f);
        p1263433097.getPositionOffset().reset(-50.0f, -50.0f);
        p1263433097.setIsUseAbsolutePosition(true);
        p1263433097.setIsUseAbsoluteSize(true);
        p1263433097.getImage().setIsUseAbsolutePosition(true);
        p1263433097.getImage().setIsUseAbsoluteSize(true);
        p1263433097.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1263433097.resetPosition();
        getSpiritList().add(p1263433097);
    }

    /* access modifiers changed from: package-private */
    public void c1433570718(Context c) {
        Puzzle p1433570718 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1433570718(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1433570718(editor, this);
            }
        };
        p1433570718.setID(1433570718);
        p1433570718.setName("1433570718");
        p1433570718.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1433570718);
        this.Desktop.RandomlyPlaced(p1433570718);
        p1433570718.setTopEdgeType(EdgeType.Concave);
        p1433570718.setBottomEdgeType(EdgeType.Convex);
        p1433570718.setLeftEdgeType(EdgeType.Convex);
        p1433570718.setRightEdgeType(EdgeType.Convex);
        p1433570718.setTopExactPuzzleID(1263433097);
        p1433570718.setBottomExactPuzzleID(533404916);
        p1433570718.setLeftExactPuzzleID(1436859870);
        p1433570718.setRightExactPuzzleID(1813032396);
        p1433570718.getDisplayImage().loadImageFromResource(c, R.drawable.p1433570718h);
        p1433570718.setExactRow(3);
        p1433570718.setExactColumn(4);
        p1433570718.getSize().reset(153.3333f, 126.6667f);
        p1433570718.getPositionOffset().reset(-76.66666f, -50.0f);
        p1433570718.setIsUseAbsolutePosition(true);
        p1433570718.setIsUseAbsoluteSize(true);
        p1433570718.getImage().setIsUseAbsolutePosition(true);
        p1433570718.getImage().setIsUseAbsoluteSize(true);
        p1433570718.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1433570718.resetPosition();
        getSpiritList().add(p1433570718);
    }

    /* access modifiers changed from: package-private */
    public void c533404916(Context c) {
        Puzzle p533404916 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load533404916(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save533404916(editor, this);
            }
        };
        p533404916.setID(533404916);
        p533404916.setName("533404916");
        p533404916.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p533404916);
        this.Desktop.RandomlyPlaced(p533404916);
        p533404916.setTopEdgeType(EdgeType.Concave);
        p533404916.setBottomEdgeType(EdgeType.Flat);
        p533404916.setLeftEdgeType(EdgeType.Convex);
        p533404916.setRightEdgeType(EdgeType.Convex);
        p533404916.setTopExactPuzzleID(1433570718);
        p533404916.setBottomExactPuzzleID(-1);
        p533404916.setLeftExactPuzzleID(1850281658);
        p533404916.setRightExactPuzzleID(52948811);
        p533404916.getDisplayImage().loadImageFromResource(c, R.drawable.p533404916h);
        p533404916.setExactRow(4);
        p533404916.setExactColumn(4);
        p533404916.getSize().reset(153.3333f, 100.0f);
        p533404916.getPositionOffset().reset(-76.66666f, -50.0f);
        p533404916.setIsUseAbsolutePosition(true);
        p533404916.setIsUseAbsoluteSize(true);
        p533404916.getImage().setIsUseAbsolutePosition(true);
        p533404916.getImage().setIsUseAbsoluteSize(true);
        p533404916.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p533404916.resetPosition();
        getSpiritList().add(p533404916);
    }

    /* access modifiers changed from: package-private */
    public void c1361666732(Context c) {
        Puzzle p1361666732 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1361666732(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1361666732(editor, this);
            }
        };
        p1361666732.setID(1361666732);
        p1361666732.setName("1361666732");
        p1361666732.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1361666732);
        this.Desktop.RandomlyPlaced(p1361666732);
        p1361666732.setTopEdgeType(EdgeType.Flat);
        p1361666732.setBottomEdgeType(EdgeType.Convex);
        p1361666732.setLeftEdgeType(EdgeType.Concave);
        p1361666732.setRightEdgeType(EdgeType.Concave);
        p1361666732.setTopExactPuzzleID(-1);
        p1361666732.setBottomExactPuzzleID(1915830939);
        p1361666732.setLeftExactPuzzleID(209624466);
        p1361666732.setRightExactPuzzleID(1400463162);
        p1361666732.getDisplayImage().loadImageFromResource(c, R.drawable.p1361666732h);
        p1361666732.setExactRow(0);
        p1361666732.setExactColumn(5);
        p1361666732.getSize().reset(100.0f, 126.6667f);
        p1361666732.getPositionOffset().reset(-50.0f, -50.0f);
        p1361666732.setIsUseAbsolutePosition(true);
        p1361666732.setIsUseAbsoluteSize(true);
        p1361666732.getImage().setIsUseAbsolutePosition(true);
        p1361666732.getImage().setIsUseAbsoluteSize(true);
        p1361666732.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1361666732.resetPosition();
        getSpiritList().add(p1361666732);
    }

    /* access modifiers changed from: package-private */
    public void c1915830939(Context c) {
        Puzzle p1915830939 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1915830939(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1915830939(editor, this);
            }
        };
        p1915830939.setID(1915830939);
        p1915830939.setName("1915830939");
        p1915830939.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1915830939);
        this.Desktop.RandomlyPlaced(p1915830939);
        p1915830939.setTopEdgeType(EdgeType.Concave);
        p1915830939.setBottomEdgeType(EdgeType.Convex);
        p1915830939.setLeftEdgeType(EdgeType.Convex);
        p1915830939.setRightEdgeType(EdgeType.Concave);
        p1915830939.setTopExactPuzzleID(1361666732);
        p1915830939.setBottomExactPuzzleID(1752289685);
        p1915830939.setLeftExactPuzzleID(1299216389);
        p1915830939.setRightExactPuzzleID(1805729774);
        p1915830939.getDisplayImage().loadImageFromResource(c, R.drawable.p1915830939h);
        p1915830939.setExactRow(1);
        p1915830939.setExactColumn(5);
        p1915830939.getSize().reset(126.6667f, 126.6667f);
        p1915830939.getPositionOffset().reset(-76.66666f, -50.0f);
        p1915830939.setIsUseAbsolutePosition(true);
        p1915830939.setIsUseAbsoluteSize(true);
        p1915830939.getImage().setIsUseAbsolutePosition(true);
        p1915830939.getImage().setIsUseAbsoluteSize(true);
        p1915830939.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1915830939.resetPosition();
        getSpiritList().add(p1915830939);
    }

    /* access modifiers changed from: package-private */
    public void c1752289685(Context c) {
        Puzzle p1752289685 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1752289685(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1752289685(editor, this);
            }
        };
        p1752289685.setID(1752289685);
        p1752289685.setName("1752289685");
        p1752289685.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1752289685);
        this.Desktop.RandomlyPlaced(p1752289685);
        p1752289685.setTopEdgeType(EdgeType.Concave);
        p1752289685.setBottomEdgeType(EdgeType.Convex);
        p1752289685.setLeftEdgeType(EdgeType.Concave);
        p1752289685.setRightEdgeType(EdgeType.Convex);
        p1752289685.setTopExactPuzzleID(1915830939);
        p1752289685.setBottomExactPuzzleID(1813032396);
        p1752289685.setLeftExactPuzzleID(1263433097);
        p1752289685.setRightExactPuzzleID(1793398239);
        p1752289685.getDisplayImage().loadImageFromResource(c, R.drawable.p1752289685h);
        p1752289685.setExactRow(2);
        p1752289685.setExactColumn(5);
        p1752289685.getSize().reset(126.6667f, 126.6667f);
        p1752289685.getPositionOffset().reset(-50.0f, -50.0f);
        p1752289685.setIsUseAbsolutePosition(true);
        p1752289685.setIsUseAbsoluteSize(true);
        p1752289685.getImage().setIsUseAbsolutePosition(true);
        p1752289685.getImage().setIsUseAbsoluteSize(true);
        p1752289685.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1752289685.resetPosition();
        getSpiritList().add(p1752289685);
    }

    /* access modifiers changed from: package-private */
    public void c1813032396(Context c) {
        Puzzle p1813032396 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1813032396(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1813032396(editor, this);
            }
        };
        p1813032396.setID(1813032396);
        p1813032396.setName("1813032396");
        p1813032396.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1813032396);
        this.Desktop.RandomlyPlaced(p1813032396);
        p1813032396.setTopEdgeType(EdgeType.Concave);
        p1813032396.setBottomEdgeType(EdgeType.Convex);
        p1813032396.setLeftEdgeType(EdgeType.Concave);
        p1813032396.setRightEdgeType(EdgeType.Convex);
        p1813032396.setTopExactPuzzleID(1752289685);
        p1813032396.setBottomExactPuzzleID(52948811);
        p1813032396.setLeftExactPuzzleID(1433570718);
        p1813032396.setRightExactPuzzleID(1973987823);
        p1813032396.getDisplayImage().loadImageFromResource(c, R.drawable.p1813032396h);
        p1813032396.setExactRow(3);
        p1813032396.setExactColumn(5);
        p1813032396.getSize().reset(126.6667f, 126.6667f);
        p1813032396.getPositionOffset().reset(-50.0f, -50.0f);
        p1813032396.setIsUseAbsolutePosition(true);
        p1813032396.setIsUseAbsoluteSize(true);
        p1813032396.getImage().setIsUseAbsolutePosition(true);
        p1813032396.getImage().setIsUseAbsoluteSize(true);
        p1813032396.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1813032396.resetPosition();
        getSpiritList().add(p1813032396);
    }

    /* access modifiers changed from: package-private */
    public void c52948811(Context c) {
        Puzzle p52948811 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load52948811(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save52948811(editor, this);
            }
        };
        p52948811.setID(52948811);
        p52948811.setName("52948811");
        p52948811.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p52948811);
        this.Desktop.RandomlyPlaced(p52948811);
        p52948811.setTopEdgeType(EdgeType.Concave);
        p52948811.setBottomEdgeType(EdgeType.Flat);
        p52948811.setLeftEdgeType(EdgeType.Concave);
        p52948811.setRightEdgeType(EdgeType.Convex);
        p52948811.setTopExactPuzzleID(1813032396);
        p52948811.setBottomExactPuzzleID(-1);
        p52948811.setLeftExactPuzzleID(533404916);
        p52948811.setRightExactPuzzleID(2114674493);
        p52948811.getDisplayImage().loadImageFromResource(c, R.drawable.p52948811h);
        p52948811.setExactRow(4);
        p52948811.setExactColumn(5);
        p52948811.getSize().reset(126.6667f, 100.0f);
        p52948811.getPositionOffset().reset(-50.0f, -50.0f);
        p52948811.setIsUseAbsolutePosition(true);
        p52948811.setIsUseAbsoluteSize(true);
        p52948811.getImage().setIsUseAbsolutePosition(true);
        p52948811.getImage().setIsUseAbsoluteSize(true);
        p52948811.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p52948811.resetPosition();
        getSpiritList().add(p52948811);
    }

    /* access modifiers changed from: package-private */
    public void c1400463162(Context c) {
        Puzzle p1400463162 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1400463162(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1400463162(editor, this);
            }
        };
        p1400463162.setID(1400463162);
        p1400463162.setName("1400463162");
        p1400463162.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1400463162);
        this.Desktop.RandomlyPlaced(p1400463162);
        p1400463162.setTopEdgeType(EdgeType.Flat);
        p1400463162.setBottomEdgeType(EdgeType.Concave);
        p1400463162.setLeftEdgeType(EdgeType.Convex);
        p1400463162.setRightEdgeType(EdgeType.Convex);
        p1400463162.setTopExactPuzzleID(-1);
        p1400463162.setBottomExactPuzzleID(1805729774);
        p1400463162.setLeftExactPuzzleID(1361666732);
        p1400463162.setRightExactPuzzleID(805789953);
        p1400463162.getDisplayImage().loadImageFromResource(c, R.drawable.p1400463162h);
        p1400463162.setExactRow(0);
        p1400463162.setExactColumn(6);
        p1400463162.getSize().reset(153.3333f, 100.0f);
        p1400463162.getPositionOffset().reset(-76.66666f, -50.0f);
        p1400463162.setIsUseAbsolutePosition(true);
        p1400463162.setIsUseAbsoluteSize(true);
        p1400463162.getImage().setIsUseAbsolutePosition(true);
        p1400463162.getImage().setIsUseAbsoluteSize(true);
        p1400463162.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1400463162.resetPosition();
        getSpiritList().add(p1400463162);
    }

    /* access modifiers changed from: package-private */
    public void c1805729774(Context c) {
        Puzzle p1805729774 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1805729774(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1805729774(editor, this);
            }
        };
        p1805729774.setID(1805729774);
        p1805729774.setName("1805729774");
        p1805729774.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1805729774);
        this.Desktop.RandomlyPlaced(p1805729774);
        p1805729774.setTopEdgeType(EdgeType.Convex);
        p1805729774.setBottomEdgeType(EdgeType.Convex);
        p1805729774.setLeftEdgeType(EdgeType.Convex);
        p1805729774.setRightEdgeType(EdgeType.Convex);
        p1805729774.setTopExactPuzzleID(1400463162);
        p1805729774.setBottomExactPuzzleID(1793398239);
        p1805729774.setLeftExactPuzzleID(1915830939);
        p1805729774.setRightExactPuzzleID(1783363977);
        p1805729774.getDisplayImage().loadImageFromResource(c, R.drawable.p1805729774h);
        p1805729774.setExactRow(1);
        p1805729774.setExactColumn(6);
        p1805729774.getSize().reset(153.3333f, 153.3333f);
        p1805729774.getPositionOffset().reset(-76.66666f, -76.66666f);
        p1805729774.setIsUseAbsolutePosition(true);
        p1805729774.setIsUseAbsoluteSize(true);
        p1805729774.getImage().setIsUseAbsolutePosition(true);
        p1805729774.getImage().setIsUseAbsoluteSize(true);
        p1805729774.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1805729774.resetPosition();
        getSpiritList().add(p1805729774);
    }

    /* access modifiers changed from: package-private */
    public void c1793398239(Context c) {
        Puzzle p1793398239 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1793398239(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1793398239(editor, this);
            }
        };
        p1793398239.setID(1793398239);
        p1793398239.setName("1793398239");
        p1793398239.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1793398239);
        this.Desktop.RandomlyPlaced(p1793398239);
        p1793398239.setTopEdgeType(EdgeType.Concave);
        p1793398239.setBottomEdgeType(EdgeType.Convex);
        p1793398239.setLeftEdgeType(EdgeType.Concave);
        p1793398239.setRightEdgeType(EdgeType.Concave);
        p1793398239.setTopExactPuzzleID(1805729774);
        p1793398239.setBottomExactPuzzleID(1973987823);
        p1793398239.setLeftExactPuzzleID(1752289685);
        p1793398239.setRightExactPuzzleID(1674759850);
        p1793398239.getDisplayImage().loadImageFromResource(c, R.drawable.p1793398239h);
        p1793398239.setExactRow(2);
        p1793398239.setExactColumn(6);
        p1793398239.getSize().reset(100.0f, 126.6667f);
        p1793398239.getPositionOffset().reset(-50.0f, -50.0f);
        p1793398239.setIsUseAbsolutePosition(true);
        p1793398239.setIsUseAbsoluteSize(true);
        p1793398239.getImage().setIsUseAbsolutePosition(true);
        p1793398239.getImage().setIsUseAbsoluteSize(true);
        p1793398239.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1793398239.resetPosition();
        getSpiritList().add(p1793398239);
    }

    /* access modifiers changed from: package-private */
    public void c1973987823(Context c) {
        Puzzle p1973987823 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1973987823(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1973987823(editor, this);
            }
        };
        p1973987823.setID(1973987823);
        p1973987823.setName("1973987823");
        p1973987823.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1973987823);
        this.Desktop.RandomlyPlaced(p1973987823);
        p1973987823.setTopEdgeType(EdgeType.Concave);
        p1973987823.setBottomEdgeType(EdgeType.Convex);
        p1973987823.setLeftEdgeType(EdgeType.Concave);
        p1973987823.setRightEdgeType(EdgeType.Convex);
        p1973987823.setTopExactPuzzleID(1793398239);
        p1973987823.setBottomExactPuzzleID(2114674493);
        p1973987823.setLeftExactPuzzleID(1813032396);
        p1973987823.setRightExactPuzzleID(1323369191);
        p1973987823.getDisplayImage().loadImageFromResource(c, R.drawable.p1973987823h);
        p1973987823.setExactRow(3);
        p1973987823.setExactColumn(6);
        p1973987823.getSize().reset(126.6667f, 126.6667f);
        p1973987823.getPositionOffset().reset(-50.0f, -50.0f);
        p1973987823.setIsUseAbsolutePosition(true);
        p1973987823.setIsUseAbsoluteSize(true);
        p1973987823.getImage().setIsUseAbsolutePosition(true);
        p1973987823.getImage().setIsUseAbsoluteSize(true);
        p1973987823.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1973987823.resetPosition();
        getSpiritList().add(p1973987823);
    }

    /* access modifiers changed from: package-private */
    public void c2114674493(Context c) {
        Puzzle p2114674493 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load2114674493(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save2114674493(editor, this);
            }
        };
        p2114674493.setID(2114674493);
        p2114674493.setName("2114674493");
        p2114674493.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p2114674493);
        this.Desktop.RandomlyPlaced(p2114674493);
        p2114674493.setTopEdgeType(EdgeType.Concave);
        p2114674493.setBottomEdgeType(EdgeType.Flat);
        p2114674493.setLeftEdgeType(EdgeType.Concave);
        p2114674493.setRightEdgeType(EdgeType.Convex);
        p2114674493.setTopExactPuzzleID(1973987823);
        p2114674493.setBottomExactPuzzleID(-1);
        p2114674493.setLeftExactPuzzleID(52948811);
        p2114674493.setRightExactPuzzleID(236443623);
        p2114674493.getDisplayImage().loadImageFromResource(c, R.drawable.p2114674493h);
        p2114674493.setExactRow(4);
        p2114674493.setExactColumn(6);
        p2114674493.getSize().reset(126.6667f, 100.0f);
        p2114674493.getPositionOffset().reset(-50.0f, -50.0f);
        p2114674493.setIsUseAbsolutePosition(true);
        p2114674493.setIsUseAbsoluteSize(true);
        p2114674493.getImage().setIsUseAbsolutePosition(true);
        p2114674493.getImage().setIsUseAbsoluteSize(true);
        p2114674493.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p2114674493.resetPosition();
        getSpiritList().add(p2114674493);
    }

    /* access modifiers changed from: package-private */
    public void c805789953(Context c) {
        Puzzle p805789953 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load805789953(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save805789953(editor, this);
            }
        };
        p805789953.setID(805789953);
        p805789953.setName("805789953");
        p805789953.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p805789953);
        this.Desktop.RandomlyPlaced(p805789953);
        p805789953.setTopEdgeType(EdgeType.Flat);
        p805789953.setBottomEdgeType(EdgeType.Concave);
        p805789953.setLeftEdgeType(EdgeType.Concave);
        p805789953.setRightEdgeType(EdgeType.Flat);
        p805789953.setTopExactPuzzleID(-1);
        p805789953.setBottomExactPuzzleID(1783363977);
        p805789953.setLeftExactPuzzleID(1400463162);
        p805789953.setRightExactPuzzleID(-1);
        p805789953.getDisplayImage().loadImageFromResource(c, R.drawable.p805789953h);
        p805789953.setExactRow(0);
        p805789953.setExactColumn(7);
        p805789953.getSize().reset(100.0f, 100.0f);
        p805789953.getPositionOffset().reset(-50.0f, -50.0f);
        p805789953.setIsUseAbsolutePosition(true);
        p805789953.setIsUseAbsoluteSize(true);
        p805789953.getImage().setIsUseAbsolutePosition(true);
        p805789953.getImage().setIsUseAbsoluteSize(true);
        p805789953.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p805789953.resetPosition();
        getSpiritList().add(p805789953);
    }

    /* access modifiers changed from: package-private */
    public void c1783363977(Context c) {
        Puzzle p1783363977 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1783363977(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1783363977(editor, this);
            }
        };
        p1783363977.setID(1783363977);
        p1783363977.setName("1783363977");
        p1783363977.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1783363977);
        this.Desktop.RandomlyPlaced(p1783363977);
        p1783363977.setTopEdgeType(EdgeType.Convex);
        p1783363977.setBottomEdgeType(EdgeType.Convex);
        p1783363977.setLeftEdgeType(EdgeType.Concave);
        p1783363977.setRightEdgeType(EdgeType.Flat);
        p1783363977.setTopExactPuzzleID(805789953);
        p1783363977.setBottomExactPuzzleID(1674759850);
        p1783363977.setLeftExactPuzzleID(1805729774);
        p1783363977.setRightExactPuzzleID(-1);
        p1783363977.getDisplayImage().loadImageFromResource(c, R.drawable.p1783363977h);
        p1783363977.setExactRow(1);
        p1783363977.setExactColumn(7);
        p1783363977.getSize().reset(100.0f, 153.3333f);
        p1783363977.getPositionOffset().reset(-50.0f, -76.66666f);
        p1783363977.setIsUseAbsolutePosition(true);
        p1783363977.setIsUseAbsoluteSize(true);
        p1783363977.getImage().setIsUseAbsolutePosition(true);
        p1783363977.getImage().setIsUseAbsoluteSize(true);
        p1783363977.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1783363977.resetPosition();
        getSpiritList().add(p1783363977);
    }

    /* access modifiers changed from: package-private */
    public void c1674759850(Context c) {
        Puzzle p1674759850 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1674759850(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1674759850(editor, this);
            }
        };
        p1674759850.setID(1674759850);
        p1674759850.setName("1674759850");
        p1674759850.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1674759850);
        this.Desktop.RandomlyPlaced(p1674759850);
        p1674759850.setTopEdgeType(EdgeType.Concave);
        p1674759850.setBottomEdgeType(EdgeType.Convex);
        p1674759850.setLeftEdgeType(EdgeType.Convex);
        p1674759850.setRightEdgeType(EdgeType.Flat);
        p1674759850.setTopExactPuzzleID(1783363977);
        p1674759850.setBottomExactPuzzleID(1323369191);
        p1674759850.setLeftExactPuzzleID(1793398239);
        p1674759850.setRightExactPuzzleID(-1);
        p1674759850.getDisplayImage().loadImageFromResource(c, R.drawable.p1674759850h);
        p1674759850.setExactRow(2);
        p1674759850.setExactColumn(7);
        p1674759850.getSize().reset(126.6667f, 126.6667f);
        p1674759850.getPositionOffset().reset(-76.66666f, -50.0f);
        p1674759850.setIsUseAbsolutePosition(true);
        p1674759850.setIsUseAbsoluteSize(true);
        p1674759850.getImage().setIsUseAbsolutePosition(true);
        p1674759850.getImage().setIsUseAbsoluteSize(true);
        p1674759850.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1674759850.resetPosition();
        getSpiritList().add(p1674759850);
    }

    /* access modifiers changed from: package-private */
    public void c1323369191(Context c) {
        Puzzle p1323369191 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1323369191(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1323369191(editor, this);
            }
        };
        p1323369191.setID(1323369191);
        p1323369191.setName("1323369191");
        p1323369191.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1323369191);
        this.Desktop.RandomlyPlaced(p1323369191);
        p1323369191.setTopEdgeType(EdgeType.Concave);
        p1323369191.setBottomEdgeType(EdgeType.Convex);
        p1323369191.setLeftEdgeType(EdgeType.Concave);
        p1323369191.setRightEdgeType(EdgeType.Flat);
        p1323369191.setTopExactPuzzleID(1674759850);
        p1323369191.setBottomExactPuzzleID(236443623);
        p1323369191.setLeftExactPuzzleID(1973987823);
        p1323369191.setRightExactPuzzleID(-1);
        p1323369191.getDisplayImage().loadImageFromResource(c, R.drawable.p1323369191h);
        p1323369191.setExactRow(3);
        p1323369191.setExactColumn(7);
        p1323369191.getSize().reset(100.0f, 126.6667f);
        p1323369191.getPositionOffset().reset(-50.0f, -50.0f);
        p1323369191.setIsUseAbsolutePosition(true);
        p1323369191.setIsUseAbsoluteSize(true);
        p1323369191.getImage().setIsUseAbsolutePosition(true);
        p1323369191.getImage().setIsUseAbsoluteSize(true);
        p1323369191.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1323369191.resetPosition();
        getSpiritList().add(p1323369191);
    }

    /* access modifiers changed from: package-private */
    public void c236443623(Context c) {
        Puzzle p236443623 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load236443623(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save236443623(editor, this);
            }
        };
        p236443623.setID(236443623);
        p236443623.setName("236443623");
        p236443623.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p236443623);
        this.Desktop.RandomlyPlaced(p236443623);
        p236443623.setTopEdgeType(EdgeType.Concave);
        p236443623.setBottomEdgeType(EdgeType.Flat);
        p236443623.setLeftEdgeType(EdgeType.Concave);
        p236443623.setRightEdgeType(EdgeType.Flat);
        p236443623.setTopExactPuzzleID(1323369191);
        p236443623.setBottomExactPuzzleID(-1);
        p236443623.setLeftExactPuzzleID(2114674493);
        p236443623.setRightExactPuzzleID(-1);
        p236443623.getDisplayImage().loadImageFromResource(c, R.drawable.p236443623h);
        p236443623.setExactRow(4);
        p236443623.setExactColumn(7);
        p236443623.getSize().reset(100.0f, 100.0f);
        p236443623.getPositionOffset().reset(-50.0f, -50.0f);
        p236443623.setIsUseAbsolutePosition(true);
        p236443623.setIsUseAbsoluteSize(true);
        p236443623.getImage().setIsUseAbsolutePosition(true);
        p236443623.getImage().setIsUseAbsoluteSize(true);
        p236443623.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p236443623.resetPosition();
        getSpiritList().add(p236443623);
    }
}
