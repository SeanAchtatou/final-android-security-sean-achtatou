package com.skyd.bestpuzzle;

public enum PuzzleState {
    Default(1),
    Drag(2),
    Selected(4),
    Take(8);
    
    private int _Value = 1;

    public int getValue() {
        return this._Value;
    }

    public void setValue(int value) {
        this._Value = value;
    }

    public void setValueToDefault() {
        setValue(1);
    }

    private PuzzleState(int Value) {
        this._Value = Value;
    }

    public void Combination(PuzzleState d) {
        this._Value |= d._Value;
    }

    public boolean Is(PuzzleState d) {
        return this._Value == d._Value;
    }

    public boolean IsContain(PuzzleState d) {
        return (this._Value & d._Value) > 0;
    }

    public void Intersection(PuzzleState d) {
        this._Value &= d._Value;
    }

    public void Remove(PuzzleState d) {
        if (IsContain(d)) {
            this._Value = (this._Value & d._Value) ^ this._Value;
        }
    }
}
