package com.skyd.bestpuzzle;

import android.content.SharedPreferences;
import com.skyd.core.android.game.GameImageSpirit;
import com.skyd.core.common.Direction;
import com.skyd.core.math.MathEx;
import com.skyd.core.vector.Vector2DF;
import java.util.Iterator;

public abstract class Puzzle extends GameImageSpirit implements IOnDesktop {
    public static final float Tolerance = 1.0f;
    private static Vector2DF _MaxRadius = new Vector2DF();
    private static Vector2DF _RealitySize = new Vector2DF();
    private EdgeType _BottomEdgeType = EdgeType.Flat;
    private int _BottomExactPuzzleID = -1;
    private Desktop _Desktop = null;
    private int _ExactColumn = -1;
    private int _ExactRow = -1;
    private int _ID = 0;
    private EdgeType _LeftEdgeType = EdgeType.Flat;
    private int _LeftExactPuzzleID = -1;
    private Vector2DF _PositionInDesktop = new Vector2DF();
    private EdgeType _RightEdgeType = EdgeType.Flat;
    private int _RightExactPuzzleID = -1;
    private PuzzleState _State = PuzzleState.Default;
    private EdgeType _TopEdgeType = EdgeType.Flat;
    private int _TopExactPuzzleID = -1;

    public abstract void load(Center center, SharedPreferences sharedPreferences);

    public abstract void save(Center center, SharedPreferences.Editor editor);

    public int getID() {
        return this._ID;
    }

    public void setID(int value) {
        this._ID = value;
    }

    public void setIDToDefault() {
        setID(0);
    }

    public Desktop getDesktop() {
        return this._Desktop;
    }

    public void setDesktop(Desktop value) {
        this._Desktop = value;
    }

    public void setDesktopToDefault() {
        setDesktop(null);
    }

    public static Vector2DF getRealitySize() {
        return _RealitySize;
    }

    public static void setRealitySize(Vector2DF value) {
        _RealitySize = value;
    }

    public static void setRealitySizeToDefault() {
        setRealitySize(new Vector2DF());
    }

    public Vector2DF getPositionInDesktop() {
        return this._PositionInDesktop;
    }

    public Puzzle getBottomPuzzle(Vector2DF position, float rotation) {
        return this._Desktop.getNearPuzzle(getBottomPuzzlePosition(position, rotation), 1.0f);
    }

    public Puzzle getRightPuzzle(Vector2DF position, float rotation) {
        return this._Desktop.getNearPuzzle(getRightPuzzlePosition(position, rotation), 1.0f);
    }

    public Puzzle getLeftPuzzle(Vector2DF position, float rotation) {
        return this._Desktop.getNearPuzzle(getLeftPuzzlePosition(position, rotation), 1.0f);
    }

    public Puzzle getTopPuzzle(Vector2DF position, float rotation) {
        return this._Desktop.getNearPuzzle(getTopPuzzlePosition(position, rotation), 1.0f);
    }

    public int getTopExactPuzzleID() {
        return this._TopExactPuzzleID;
    }

    public void setTopExactPuzzleID(int value) {
        this._TopExactPuzzleID = value;
    }

    public void setTopExactPuzzleIDToDefault() {
        setTopExactPuzzleID(-1);
    }

    public int getLeftExactPuzzleID() {
        return this._LeftExactPuzzleID;
    }

    public void setLeftExactPuzzleID(int value) {
        this._LeftExactPuzzleID = value;
    }

    public void setLeftExactPuzzleIDToDefault() {
        setLeftExactPuzzleID(-1);
    }

    public int getRightExactPuzzleID() {
        return this._RightExactPuzzleID;
    }

    public void setRightExactPuzzleID(int value) {
        this._RightExactPuzzleID = value;
    }

    public void setRightExactPuzzleIDToDefault() {
        setRightExactPuzzleID(-1);
    }

    public int getBottomExactPuzzleID() {
        return this._BottomExactPuzzleID;
    }

    public void setBottomExactPuzzleID(int value) {
        this._BottomExactPuzzleID = value;
    }

    public void setBottomExactPuzzleIDToDefault() {
        setBottomExactPuzzleID(-1);
    }

    public PuzzleState getState() {
        return this._State;
    }

    public void setState(PuzzleState value) {
        this._State = value;
    }

    public void setStateToDefault() {
        setState(PuzzleState.Default);
    }

    public int getExactRow() {
        return this._ExactRow;
    }

    public void setExactRow(int value) {
        this._ExactRow = value;
    }

    public void setExactRowToDefault() {
        setExactRow(-1);
    }

    public int getExactColumn() {
        return this._ExactColumn;
    }

    public void setExactColumn(int value) {
        this._ExactColumn = value;
    }

    public void setExactColumnToDefault() {
        setExactColumn(-1);
    }

    public EdgeType getTopEdgeType() {
        return this._TopEdgeType;
    }

    public void setTopEdgeType(EdgeType value) {
        this._TopEdgeType = value;
    }

    public void setTopEdgeTypeToDefault() {
        setTopEdgeType(EdgeType.Flat);
    }

    public EdgeType getLeftEdgeType() {
        return this._LeftEdgeType;
    }

    public void setLeftEdgeType(EdgeType value) {
        this._LeftEdgeType = value;
    }

    public void setLeftEdgeTypeToDefault() {
        setLeftEdgeType(EdgeType.Flat);
    }

    public EdgeType getRightEdgeType() {
        return this._RightEdgeType;
    }

    public void setRightEdgeType(EdgeType value) {
        this._RightEdgeType = value;
    }

    public void setRightEdgeTypeToDefault() {
        setRightEdgeType(EdgeType.Flat);
    }

    public EdgeType getBottomEdgeType() {
        return this._BottomEdgeType;
    }

    public void setBottomEdgeType(EdgeType value) {
        this._BottomEdgeType = value;
    }

    public void setBottomEdgeTypeToDefault() {
        setBottomEdgeType(EdgeType.Flat);
    }

    public boolean IsExact() {
        boolean b = true;
        if (getTopExactPuzzleID() >= 0) {
            if (getTopPuzzle(this._PositionInDesktop, getRotation()) == null || getTopPuzzle(this._PositionInDesktop, getRotation()).getID() != getTopExactPuzzleID()) {
                b = false;
            }
        } else if (getTopPuzzle(this._PositionInDesktop, getRotation()) != null) {
            b = false;
        }
        if (getLeftExactPuzzleID() >= 0) {
            if (getLeftPuzzle(this._PositionInDesktop, getRotation()) == null || getLeftPuzzle(this._PositionInDesktop, getRotation()).getID() != getLeftExactPuzzleID()) {
                b = false;
            }
        } else if (getLeftPuzzle(this._PositionInDesktop, getRotation()) != null) {
            b = false;
        }
        if (getRightExactPuzzleID() >= 0) {
            if (getRightPuzzle(this._PositionInDesktop, getRotation()) == null || getRightPuzzle(this._PositionInDesktop, getRotation()).getID() != getRightExactPuzzleID()) {
                b = false;
            }
        } else if (getRightPuzzle(this._PositionInDesktop, getRotation()) != null) {
            b = false;
        }
        if (getBottomExactPuzzleID() >= 0) {
            if (getBottomPuzzle(this._PositionInDesktop, getRotation()) == null || getBottomPuzzle(this._PositionInDesktop, getRotation()).getID() != getBottomExactPuzzleID()) {
                return false;
            }
            return b;
        } else if (getBottomPuzzle(this._PositionInDesktop, getRotation()) != null) {
            return false;
        } else {
            return b;
        }
    }

    public boolean Piece(Puzzle target) {
        float rys = ((target.getRotation() + 360.0f) - (target._PositionInDesktop.minusNew(this._PositionInDesktop).getAngle() + 90.0f)) % 90.0f;
        if (rys > 20.0f && rys < 70.0f) {
            return false;
        }
        if (Math.abs(target._PositionInDesktop.minusNew(this._PositionInDesktop).getLength() - getRealitySize().getX()) > 15.0f) {
            return false;
        }
        Vector2DF p = target.getNearPuzzlePosition(target.getDirection(this));
        float r = target.getRulerAngle(getRotation());
        if (!CheckCanPiece(this, getTopPuzzle(p, r))) {
            return false;
        }
        if (!CheckCanPiece(this, getLeftPuzzle(p, r))) {
            return false;
        }
        if (!CheckCanPiece(this, getBottomPuzzle(p, r))) {
            return false;
        }
        if (!CheckCanPiece(this, getRightPuzzle(p, r))) {
            return false;
        }
        Iterator<Puzzle> it = this._Desktop.getPuzzleList().iterator();
        while (it.hasNext()) {
            Puzzle f = it.next();
            if (!f.equals(this) && f._PositionInDesktop.minusNew(p).getLength() < 1.0f) {
                return false;
            }
        }
        setRotation(r);
        this._PositionInDesktop.resetWith(p);
        return true;
    }

    public static boolean CheckCanPiece(Puzzle a, Puzzle b) {
        if (a == null || b == null) {
            return true;
        }
        Direction d = b.getDirection(a);
        Vector2DF nearPuzzlePosition = b.getNearPuzzlePosition(d);
        return b.getEdgeType(d).canPiece(a.getEdgeType(a.getDirection(b)));
    }

    public Vector2DF getTopPuzzlePosition(Vector2DF position, float rotation) {
        return new Vector2DF().setLength(getRealitySize().getX()).setAngle(rotation - 90.0f).plus(position);
    }

    public Vector2DF getRightPuzzlePosition(Vector2DF position, float rotation) {
        return new Vector2DF().setLength(getRealitySize().getX()).setAngle((rotation + 90.0f) - 90.0f).plus(position);
    }

    public Vector2DF getBottomPuzzlePosition(Vector2DF position, float rotation) {
        return new Vector2DF().setLength(getRealitySize().getX()).setAngle((180.0f + rotation) - 90.0f).plus(position);
    }

    public Vector2DF getLeftPuzzlePosition(Vector2DF position, float rotation) {
        return new Vector2DF().setLength(getRealitySize().getX()).setAngle((270.0f + rotation) - 90.0f).plus(position);
    }

    public Vector2DF getNearPuzzlePosition(Direction d) {
        if (d == Direction.Top) {
            return getTopPuzzlePosition(this._PositionInDesktop, getRotation());
        }
        if (d == Direction.Left) {
            return getLeftPuzzlePosition(this._PositionInDesktop, getRotation());
        }
        if (d == Direction.Right) {
            return getRightPuzzlePosition(this._PositionInDesktop, getRotation());
        }
        if (d == Direction.Bottom) {
            return getBottomPuzzlePosition(this._PositionInDesktop, getRotation());
        }
        return this._PositionInDesktop;
    }

    public Puzzle getNearPuzzle(Direction d) {
        if (d == Direction.Top) {
            return getTopPuzzle(this._PositionInDesktop, getRotation());
        }
        if (d == Direction.Left) {
            return getLeftPuzzle(this._PositionInDesktop, getRotation());
        }
        if (d == Direction.Right) {
            return getRightPuzzle(this._PositionInDesktop, getRotation());
        }
        if (d == Direction.Bottom) {
            return getBottomPuzzle(this._PositionInDesktop, getRotation());
        }
        return this;
    }

    public int getNearExactPuzzleID(Direction d) {
        if (d == Direction.Top) {
            return getTopExactPuzzleID();
        }
        if (d == Direction.Left) {
            return getLeftExactPuzzleID();
        }
        if (d == Direction.Right) {
            return getRightExactPuzzleID();
        }
        if (d == Direction.Bottom) {
            return getBottomExactPuzzleID();
        }
        return getID();
    }

    public EdgeType getEdgeType(Direction d) {
        if (d == Direction.Top) {
            return getTopEdgeType();
        }
        if (d == Direction.Left) {
            return getLeftEdgeType();
        }
        if (d == Direction.Right) {
            return getRightEdgeType();
        }
        if (d == Direction.Bottom) {
            return getBottomEdgeType();
        }
        return EdgeType.Flat;
    }

    private float getRelativeAngle(Puzzle puzzle) {
        return getRulerAngle(puzzle._PositionInDesktop.minusNew(this._PositionInDesktop).getAngle());
    }

    private float getRulerAngle(float angle) {
        float r = MathEx.fixAngle(angle - getRotation());
        if (r >= 45.0f && r < 135.0f) {
            return getRotation() + 90.0f;
        }
        if (r >= 135.0f && r < 225.0f) {
            return getRotation() + 180.0f;
        }
        if (r < 225.0f || r >= 315.0f) {
            return getRotation();
        }
        return getRotation() + 270.0f;
    }

    public Direction getDirection(float angle) {
        float r = MathEx.fixAngle(angle - getRotation());
        if (r >= 45.0f && r < 135.0f) {
            return Direction.Right;
        }
        if (r >= 135.0f && r < 225.0f) {
            return Direction.Bottom;
        }
        if (r < 225.0f || r >= 315.0f) {
            return Direction.Top;
        }
        return Direction.Left;
    }

    public Direction getDirection(Puzzle target) {
        return getDirection(target._PositionInDesktop);
    }

    public Direction getDirection(Vector2DF target) {
        Vector2DF v = target.minusNew(this._PositionInDesktop);
        if (v.getLength() < 1.0f) {
            return Direction.Center;
        }
        return getDirection(MathEx.fixAngle(v.getAngle() + 90.0f));
    }

    public Vector2DF getPosition() {
        if (getLevel() > 10.0f) {
            return super.getPosition();
        }
        return this._PositionInDesktop;
    }

    public Vector2DF getScale() {
        if (getLevel() > 10.0f) {
            return new Vector2DF(this._Desktop.getZoom(), this._Desktop.getZoom());
        }
        return super.getScale();
    }

    public static Vector2DF getMaxRadius() {
        return _MaxRadius;
    }

    public static void setMaxRadius(Vector2DF value) {
        _MaxRadius = value;
    }

    public static void setMaxRadiusToDefault() {
        setMaxRadius(new Vector2DF());
    }

    public void resetPosition() {
        super.getPosition().reset(-1000.0f, -1000.0f);
    }
}
