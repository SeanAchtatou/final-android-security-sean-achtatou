package com.a.a;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;

public final class e {
    public static Bundle a(String str) {
        Bundle bundle = new Bundle();
        if (str != null) {
            for (String split : str.split("&")) {
                String[] split2 = split.split("=");
                bundle.putString(URLDecoder.decode(split2[0]), URLDecoder.decode(split2[1]));
            }
        }
        return bundle;
    }

    public static String a(Bundle bundle) {
        if (bundle == null) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        boolean z = true;
        for (String next : bundle.keySet()) {
            if (z) {
                z = false;
            } else {
                sb.append("&");
            }
            sb.append(URLEncoder.encode(next) + "=" + URLEncoder.encode(bundle.getString(next)));
        }
        return sb.toString();
    }

    public static void a(Context context, String str, String str2) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(str);
        builder.setMessage(str2);
        builder.create().show();
    }

    public static Bundle b(String str) {
        try {
            URL url = new URL(str.replace("fbconnect", "http"));
            Bundle a = a(url.getQuery());
            a.putAll(a(url.getRef()));
            return a;
        } catch (MalformedURLException e) {
            return new Bundle();
        }
    }
}
