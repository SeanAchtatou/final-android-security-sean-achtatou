package com.scoreloop.client.android.ui.component.challenge;

import android.app.Dialog;
import android.os.Bundle;
import com.scoreloop.client.android.core.controller.ChallengeController;
import com.scoreloop.client.android.core.controller.ChallengeControllerObserver;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.model.Challenge;
import com.scoreloop.client.android.ui.component.agent.UserDetailsAgent;
import com.scoreloop.client.android.ui.component.base.CaptionListItem;
import com.scoreloop.client.android.ui.component.base.Constant;
import com.scoreloop.client.android.ui.component.base.StringFormatter;
import com.scoreloop.client.android.ui.component.challenge.ChallengeControlsListItem;
import com.scoreloop.client.android.ui.framework.BaseDialog;
import com.scoreloop.client.android.ui.framework.NavigationIntent;
import com.scoreloop.client.android.ui.framework.OkCancelDialog;
import com.scoreloop.client.android.ui.framework.TextButtonDialog;
import com.scoreloop.client.android.ui.framework.ValueStore;
import com.skyd.bestpuzzle.n1665.R;

public class ChallengeAcceptListActivity extends ChallengeActionListActivity implements ChallengeControllerObserver, BaseDialog.OnActionListener, ChallengeControlsListItem.OnControlObserver {
    private Challenge _challenge;
    private ChallengeParticipantsListItem _challengeParticipantsListItem;
    private boolean _isNavigationAllowed;
    private OkCancelDialog _navigationDialog;
    private Runnable _navigationDialogContinuation;
    private NavigationIntent _navigationIntent;
    private ValueStore _opponentValueStore;

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                TextButtonDialog dialogBalance = new TextButtonDialog(this);
                dialogBalance.setText(getResources().getString(R.string.sl_error_message_challenge_balance));
                dialogBalance.setOnActionListener(this);
                dialogBalance.setOnDismissListener(this);
                return dialogBalance;
            case 2:
                TextButtonDialog dialogAccept = new TextButtonDialog(this);
                dialogAccept.setText(getResources().getString(R.string.sl_error_message_challenge_accept));
                dialogAccept.setOnActionListener(this);
                dialogAccept.setOnDismissListener(this);
                return dialogAccept;
            case 3:
                TextButtonDialog dialogReject = new TextButtonDialog(this);
                dialogReject.setText(getResources().getString(R.string.sl_error_message_challenge_reject));
                dialogReject.setOnActionListener(this);
                dialogReject.setOnDismissListener(this);
                return dialogReject;
            case 4:
                OkCancelDialog navigationDialog = new OkCancelDialog(this);
                navigationDialog.setText(getResources().getString(R.string.sl_leave_accept_challenge));
                navigationDialog.setOnActionListener(this);
                navigationDialog.setOnDismissListener(this);
                return navigationDialog;
            default:
                return super.onCreateDialog(id);
        }
    }

    public void challengeControllerDidFailOnInsufficientBalance(ChallengeController challengeController) {
        this._isNavigationAllowed = true;
        showDialogSafe(1, true);
    }

    public void challengeControllerDidFailToAcceptChallenge(ChallengeController challengeController) {
        this._isNavigationAllowed = true;
        showDialogSafe(2, true);
    }

    public void challengeControllerDidFailToRejectChallenge(ChallengeController challengeController) {
        this._isNavigationAllowed = true;
        showDialogSafe(3, true);
    }

    private void doAfterNavigationDialog(Runnable continuation) {
        if (this._navigationDialog == null) {
            continuation.run();
        } else {
            this._navigationDialogContinuation = continuation;
        }
    }

    /* access modifiers changed from: package-private */
    public CaptionListItem getCaptionListItem() {
        return new CaptionListItem(this, null, getString(R.string.sl_accept_challenge));
    }

    /* access modifiers changed from: package-private */
    public ChallengeControlsListItem getChallengeControlsListItem() {
        return new ChallengeControlsListItem(this, this._challenge, this);
    }

    /* access modifiers changed from: package-private */
    public ChallengeParticipantsListItem getChallengeParticipantsListItem() {
        if (this._challengeParticipantsListItem == null) {
            this._challengeParticipantsListItem = new ChallengeParticipantsListItem(this, this._challenge.getContender(), getUser());
        }
        return this._challengeParticipantsListItem;
    }

    /* access modifiers changed from: package-private */
    public ChallengeSettingsListItem getChallengeStakeAndModeListItem() {
        return new ChallengeSettingsListItem(this, this._challenge);
    }

    /* access modifiers changed from: protected */
    public void onPrepareDialog(int id, Dialog dialog) {
        switch (id) {
            case 4:
                OkCancelDialog okCancelDialog = (OkCancelDialog) dialog;
                okCancelDialog.setTarget(this._navigationIntent);
                this._navigationDialog = okCancelDialog;
                break;
        }
        super.onPrepareDialog(id, dialog);
    }

    /* access modifiers changed from: protected */
    public boolean isNavigationAllowed(NavigationIntent navigationIntent) {
        if (this._isNavigationAllowed) {
            return true;
        }
        this._navigationIntent = navigationIntent;
        showDialogSafe(4, true);
        return false;
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public void onAction(BaseDialog dialog, int action) {
        if (dialog == this._navigationDialog) {
            this._navigationDialog = null;
            if (action == 0) {
                dialog.dismiss();
                ((NavigationIntent) dialog.getTarget()).execute();
                return;
            }
            dialog.dismiss();
            onNavigationDialogFinished();
            return;
        }
        dialog.dismiss();
        doAfterNavigationDialog(new Runnable() {
            public void run() {
                ChallengeAcceptListActivity.this.displayPrevious();
            }
        });
    }

    public void onControl1() {
        if (challengeGamePlayAllowed()) {
            this._isNavigationAllowed = false;
            this._challenge.setContestant(getUser());
            ChallengeController challengeController = new ChallengeController(this);
            showSpinnerFor(challengeController);
            challengeController.setChallenge(this._challenge);
            challengeController.acceptChallenge();
        }
    }

    public void onControl2() {
        this._challenge.setContestant(getUser());
        ChallengeController challengeController = new ChallengeController(this);
        showSpinnerFor(challengeController);
        challengeController.setChallenge(this._challenge);
        challengeController.rejectChallenge();
    }

    public void onCreate(Bundle savedInstanceState) {
        this._navigationIntent = (NavigationIntent) getActivityArguments().getValue(Constant.NAVIGATION_INTENT, this._navigationIntent);
        this._navigationDialogContinuation = (Runnable) getActivityArguments().getValue(Constant.NAVIGATION_DIALOG_CONTINUATION);
        Boolean navigationAllowed = (Boolean) getActivityArguments().getValue(Constant.NAVIGATION_ALLOWED, Boolean.TRUE);
        if (navigationAllowed != null) {
            this._isNavigationAllowed = navigationAllowed.booleanValue();
        }
        super.onCreate(savedInstanceState);
        addObservedKeys(ValueStore.concatenateKeys(Constant.USER_VALUES, Constant.NUMBER_CHALLENGES_WON));
        this._challenge = (Challenge) getActivityArguments().getValue(Constant.CHALLENGE, null);
        this._opponentValueStore = new ValueStore();
        this._opponentValueStore.putValue(Constant.USER, this._challenge.getContender());
        this._opponentValueStore.addObserver(Constant.NUMBER_CHALLENGES_WON, this);
        this._opponentValueStore.addValueSources(new UserDetailsAgent());
        setNeedsRefresh();
        initAdapter();
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        getActivityArguments().putValue(Constant.NAVIGATION_INTENT, this._navigationIntent);
        getActivityArguments().putValue(Constant.NAVIGATION_DIALOG_CONTINUATION, this._navigationDialogContinuation);
        getActivityArguments().putValue(Constant.NAVIGATION_ALLOWED, Boolean.valueOf(this._isNavigationAllowed));
    }

    private void onNavigationDialogFinished() {
        if (this._navigationDialogContinuation != null) {
            Runnable continuation = this._navigationDialogContinuation;
            this._navigationDialogContinuation = null;
            continuation.run();
        }
    }

    public void onRefresh(int flags) {
        this._opponentValueStore.retrieveValue(Constant.NUMBER_CHALLENGES_WON, ValueStore.RetrievalMode.NOT_DIRTY, null);
    }

    public void onResume() {
        super.onResume();
        this._isNavigationAllowed = true;
    }

    public void onValueChanged(ValueStore valueStore, String key, Object oldValue, Object newValue) {
        if (valueStore == this._opponentValueStore) {
            if (isValueChangedFor(key, Constant.NUMBER_CHALLENGES_WON, oldValue, newValue)) {
                getChallengeParticipantsListItem().setContenderStats(StringFormatter.getChallengesSubTitle(this, this._opponentValueStore));
                getBaseListAdapter().notifyDataSetChanged();
            }
        } else if (isValueChangedFor(key, Constant.NUMBER_CHALLENGES_WON, oldValue, newValue)) {
            getChallengeParticipantsListItem().setContestantStats(StringFormatter.getChallengesSubTitle(this, valueStore));
            getBaseListAdapter().notifyDataSetChanged();
        }
    }

    public void onValueSetDirty(ValueStore valueStore, String key) {
        if (valueStore == this._opponentValueStore) {
            this._opponentValueStore.retrieveValue(Constant.NUMBER_CHALLENGES_WON, ValueStore.RetrievalMode.NOT_DIRTY, null);
        } else if (Constant.NUMBER_CHALLENGES_WON.equals(key)) {
            getUserValues().retrieveValue(key, ValueStore.RetrievalMode.NOT_DIRTY, null);
        }
    }

    /* access modifiers changed from: protected */
    public void requestControllerDidFailSafe(RequestController aRequestController, Exception anException) {
        this._isNavigationAllowed = true;
        showDialogForExceptionSafe(anException);
    }

    public void requestControllerDidReceiveResponseSafe(RequestController aRequestController) {
        this._isNavigationAllowed = true;
        Challenge challenge = ((ChallengeController) aRequestController).getChallenge();
        if (challenge.isAccepted()) {
            doAfterNavigationDialog(new Runnable() {
                public void run() {
                    ChallengeAcceptListActivity.this.startChallenge();
                }
            });
        } else if (challenge.isRejected()) {
            displayPrevious();
        } else {
            throw new IllegalStateException("this should not happen - illegal state of the accepted/rejected challenge");
        }
    }

    /* access modifiers changed from: private */
    public void startChallenge() {
        finishDisplay();
        getManager().startGamePlay(this._challenge.getMode(), this._challenge);
    }
}
