package com.scoreloop.client.android.ui.component.agent;

import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.RequestControllerObserver;
import com.scoreloop.client.android.ui.framework.ValueStore;
import java.util.Collections;
import java.util.Set;

public abstract class BaseAgent implements ValueStore.ValueSource, RequestControllerObserver {
    private boolean _isRetrieving;
    private final String[] _keys;
    private ValueStore _valueStore;

    /* access modifiers changed from: protected */
    public abstract void onFinishRetrieve(RequestController requestController, ValueStore valueStore);

    /* access modifiers changed from: protected */
    public abstract void onStartRetrieve(ValueStore valueStore);

    protected BaseAgent(String... keys) {
        this._keys = keys;
    }

    public boolean isRetrieving() {
        return this._isRetrieving;
    }

    /* access modifiers changed from: protected */
    public void putValue(String key, Object value) {
        this._valueStore.putValue(key, value);
    }

    public void requestControllerDidFail(RequestController aRequestController, Exception anException) {
        this._isRetrieving = false;
    }

    public void requestControllerDidReceiveResponse(RequestController aRequestController) {
        this._isRetrieving = false;
        onFinishRetrieve(aRequestController, this._valueStore);
    }

    public void retrieve(ValueStore valueStore) {
        this._isRetrieving = true;
        this._valueStore = valueStore;
        onStartRetrieve(valueStore);
    }

    public void supportedKeys(Set<String> keys) {
        Collections.addAll(keys, this._keys);
    }
}
