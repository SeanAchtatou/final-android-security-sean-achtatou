package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.PublishedFor__1_0_0;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.Ranking;
import com.scoreloop.client.android.core.model.Score;
import com.scoreloop.client.android.core.model.SearchList;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.RequestCompletionCallback;
import com.scoreloop.client.android.core.server.RequestMethod;
import com.scoreloop.client.android.core.server.Response;
import com.scoreloop.client.android.ui.component.base.Constant;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class RankingController extends RequestController {
    private Ranking b;
    private SearchList c;

    private static class a extends Request {
        private final Game a;
        private final Integer b;
        private final Score c;
        private final SearchList d;
        private final User e;

        public a(RequestCompletionCallback requestCompletionCallback, Game game, SearchList searchList, User user, Score score, Integer num) {
            super(requestCompletionCallback);
            if (game == null) {
                throw new IllegalStateException("internal error: aGame not being set");
            }
            this.a = game;
            this.d = searchList;
            this.e = user;
            this.c = score;
            this.b = num;
        }

        public String a() {
            return String.format("/service/games/%s/scores/rankings", this.a.getIdentifier());
        }

        public JSONObject b() {
            JSONObject jSONObject = new JSONObject();
            try {
                if (this.d != null) {
                    jSONObject.putOpt("search_list_id", this.d.getIdentifier());
                }
                if (this.c != null) {
                    jSONObject.put(Score.a, this.c.d());
                } else {
                    jSONObject.put("user_id", this.e.getIdentifier());
                    if (this.b != null) {
                        jSONObject.put("mode", this.b);
                    }
                }
                return jSONObject;
            } catch (JSONException e2) {
                throw new IllegalStateException("Invalid ranking data", e2);
            }
        }

        public RequestMethod c() {
            return RequestMethod.GET;
        }
    }

    @PublishedFor__1_0_0
    public RankingController(RequestControllerObserver requestControllerObserver) {
        this(null, requestControllerObserver);
    }

    @PublishedFor__1_0_0
    public RankingController(Session session, RequestControllerObserver requestControllerObserver) {
        super(session, requestControllerObserver, false);
        this.b = new Ranking();
        this.c = null;
        if (getGame() == null) {
            throw new IllegalStateException("I think there's no point in getting a rank of a score in a null game..");
        }
        this.c = SearchList.getDefaultScoreSearchList();
    }

    /* access modifiers changed from: package-private */
    public boolean a(Request request, Response response) throws Exception {
        if (response.f() != 200) {
            throw new Exception("Request failed");
        }
        JSONArray d = response.d();
        this.b = new Ranking();
        this.b.a(d.getJSONObject(0).getJSONObject("ranking"));
        return true;
    }

    @PublishedFor__1_0_0
    public Ranking getRanking() {
        return this.b;
    }

    @PublishedFor__1_0_0
    public SearchList getSearchList() {
        return this.c;
    }

    @PublishedFor__1_0_0
    public void loadRankingForScore(Score score) {
        if (score == null) {
            throw new IllegalArgumentException("score parameter cannot be null");
        } else if (this.c == null) {
            throw new IllegalArgumentException("Search list or user is required for score ranking");
        } else {
            if (score.getUser() == null) {
                score.a(g());
            }
            a aVar = new a(e(), getGame(), this.c, null, score, score.getMode());
            a_();
            aVar.a((long) Constant.NEWS_FEED_REFRESH_TIME);
            b(aVar);
        }
    }

    @PublishedFor__1_0_0
    public void loadRankingForScoreResult(Double d, Map<String, Object> map) {
        loadRankingForScore(new Score(d, map));
    }

    @PublishedFor__1_0_0
    public void loadRankingForUserInGameMode(User user, Integer num) {
        if (user == null) {
            throw new IllegalArgumentException("user paramter cannot be null");
        }
        a aVar = new a(e(), getGame(), this.c, user, null, num);
        a_();
        aVar.a((long) Constant.NEWS_FEED_REFRESH_TIME);
        b(aVar);
    }

    @PublishedFor__1_0_0
    public void setSearchList(SearchList searchList) {
        this.c = searchList;
    }
}
