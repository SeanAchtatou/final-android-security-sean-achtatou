package com.scoreloop.client.android.core.controller;

import android.os.Handler;
import android.os.Message;
import com.scoreloop.client.android.core.PublishedFor__1_0_0;
import com.scoreloop.client.android.core.model.Challenge;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.Money;
import com.scoreloop.client.android.core.model.Score;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.RequestCompletionCallback;
import com.scoreloop.client.android.core.server.RequestMethod;
import com.scoreloop.client.android.core.server.Response;
import com.scoreloop.client.android.ui.component.base.Constant;
import org.json.JSONException;
import org.json.JSONObject;

public class ChallengeController extends RequestController {
    private Challenge b;
    private c c;

    private static class a extends Request {
        protected Challenge a;
        protected Game b;

        public a(RequestCompletionCallback requestCompletionCallback, Game game, Challenge challenge) {
            super(requestCompletionCallback);
            if (game == null) {
                throw new IllegalStateException("internal error: aGame should be set");
            }
            this.b = game;
            this.a = challenge;
        }

        public String a() {
            return String.format("/service/games/%s/challenges", this.b.getIdentifier());
        }

        public JSONObject b() {
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put(Challenge.a, this.a.d());
                return jSONObject;
            } catch (JSONException e) {
                throw new IllegalStateException("Invalid challenge data", e);
            }
        }

        public RequestMethod c() {
            return RequestMethod.POST;
        }
    }

    private static class b extends a {
        public b(RequestCompletionCallback requestCompletionCallback, Game game, Challenge challenge) {
            super(requestCompletionCallback, game, challenge);
        }

        public String a() {
            return String.format("/service/games/%s/challenges/%s", this.b.getIdentifier(), this.a.getIdentifier());
        }

        public RequestMethod c() {
            return RequestMethod.PUT;
        }
    }

    private static class c extends Handler {
        private ChallengeController a;

        public c(ChallengeController challengeController) {
            this.a = challengeController;
        }

        public void handleMessage(Message message) {
            ChallengeControllerObserver challengeControllerObserver = (ChallengeControllerObserver) message.obj;
            switch (message.what) {
                case 1:
                    challengeControllerObserver.challengeControllerDidFailToAcceptChallenge(this.a);
                    return;
                case 2:
                    challengeControllerObserver.challengeControllerDidFailToRejectChallenge(this.a);
                    return;
                case 3:
                    challengeControllerObserver.challengeControllerDidFailOnInsufficientBalance(this.a);
                    return;
                default:
                    return;
            }
        }
    }

    @PublishedFor__1_0_0
    public ChallengeController(ChallengeControllerObserver challengeControllerObserver) {
        this(null, challengeControllerObserver);
    }

    @PublishedFor__1_0_0
    public ChallengeController(Session session, ChallengeControllerObserver challengeControllerObserver) {
        super(session, challengeControllerObserver, false);
        this.c = new c(this);
    }

    private void a(int i, RequestControllerObserver requestControllerObserver) {
        Message obtainMessage = this.c.obtainMessage(i);
        obtainMessage.obj = requestControllerObserver;
        obtainMessage.sendToTarget();
    }

    private void a(Score score, User user) {
        if (score != null && score.getUser() == null && user != null && f().isOwnedByUser(user)) {
            score.a(g());
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a(Request request, Response response) throws Exception {
        ChallengeControllerObserver challengeControllerObserver = (ChallengeControllerObserver) d();
        int f = response.f();
        if (f == 200 || f == 201) {
            JSONObject jSONObject = response.e().getJSONObject(Challenge.a);
            Challenge challenge = getChallenge();
            challenge.a(jSONObject);
            User g = g();
            if ((g.equals(challenge.getContender()) && !challenge.isCreated()) || (g.equals(challenge.getContestant()) && (challenge.isRejected() || challenge.isComplete()))) {
                f().setChallenge(null);
            }
            challengeControllerObserver.requestControllerDidReceiveResponse(this);
            return false;
        }
        Integer a2 = a(response.e());
        if (a2 == null) {
            throw new Exception("Request failed with status:" + f);
        }
        switch (a2.intValue()) {
            case Constant.LIST_ITEM_TYPE_STANDARD /*22*/:
            case Constant.LIST_ITEM_TYPE_USER_FIND_MATCH /*27*/:
                f().setChallenge(null);
                challengeControllerObserver.challengeControllerDidFailToAcceptChallenge(this);
                return false;
            case Constant.LIST_ITEM_TYPE_USER /*23*/:
            case Constant.LIST_ITEM_TYPE_USER_ADD_BUDDY /*25*/:
            case Constant.LIST_ITEM_TYPE_USER_DETAIL /*26*/:
            default:
                f().setChallenge(null);
                challengeControllerObserver.requestControllerDidFail(this, new IllegalArgumentException("error of status: " + f + " and code: " + a2));
                return false;
            case Constant.LIST_ITEM_TYPE_USER_ADD_BUDDIES /*24*/:
                challengeControllerObserver.challengeControllerDidFailOnInsufficientBalance(this);
                return false;
        }
    }

    @PublishedFor__1_0_0
    public void acceptChallenge() {
        ChallengeControllerObserver challengeControllerObserver = (ChallengeControllerObserver) d();
        if (!getChallenge().isPlayableForUser(g())) {
            f().setChallenge(null);
            a(1, challengeControllerObserver);
            return;
        }
        getChallenge().a(g(), true);
        submitChallenge();
    }

    @PublishedFor__1_0_0
    public void createChallenge(Money money, User user) {
        if (money == null) {
            throw new IllegalArgumentException("aSomeMoney parameter cannot be null");
        } else if (!f().isAuthenticated()) {
            throw new IllegalStateException("session needs to be authenticated before calling ChallengeController.createChallenge");
        } else {
            User g = g();
            if (g.equals(user)) {
                throw new IllegalStateException("User cannot challenge himself");
            }
            Challenge challenge = new Challenge(money);
            challenge.setContender(g);
            challenge.setContestant(user);
            this.b = challenge;
            f().setChallenge(challenge);
        }
    }

    @PublishedFor__1_0_0
    public Challenge getChallenge() {
        if (this.b == null) {
            this.b = f().getChallenge();
        }
        return this.b;
    }

    @PublishedFor__1_0_0
    public void rejectChallenge() {
        ChallengeControllerObserver challengeControllerObserver = (ChallengeControllerObserver) d();
        if (!getChallenge().isAssigned() || !getChallenge().getContestant().equals(g())) {
            f().setChallenge(null);
            a(2, challengeControllerObserver);
            return;
        }
        getChallenge().a(g(), false);
        submitChallenge();
    }

    @PublishedFor__1_0_0
    public void setChallenge(Challenge challenge) {
        this.b = challenge;
    }

    @PublishedFor__1_0_0
    public void submitChallenge() {
        Challenge challenge = getChallenge();
        if (challenge == null) {
            throw new IllegalStateException("Set the challenge first");
        }
        a(challenge.getContenderScore(), challenge.getContender());
        a(challenge.getContestantScore(), challenge.getContestant());
        Request aVar = challenge.getIdentifier() == null ? new a(e(), getGame(), challenge) : new b(e(), getGame(), challenge);
        f().setChallenge(challenge);
        a_();
        b(aVar);
    }

    @PublishedFor__1_0_0
    public void submitChallengeScore(Score score) {
        if (score == null) {
            throw new IllegalArgumentException("aScore parameter can't be null");
        }
        Challenge challenge = getChallenge();
        if (challenge == null) {
            throw new IllegalStateException("no challenge to submit score to");
        }
        User user = score.getUser();
        if (user == null) {
            score.a(g());
        } else if (!f().isOwnedByUser(user)) {
            throw new IllegalStateException("User is not participating in the challenge");
        }
        if (challenge == null || challenge.getMode() == score.getMode()) {
            challenge.a(score);
            submitChallenge();
            return;
        }
        throw new IllegalStateException("Score mode does not match challenge mode");
    }
}
