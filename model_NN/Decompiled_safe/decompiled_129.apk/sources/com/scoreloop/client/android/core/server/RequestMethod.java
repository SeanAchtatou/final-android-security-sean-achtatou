package com.scoreloop.client.android.core.server;

public enum RequestMethod {
    DELETE("delete"),
    GET("get"),
    POST("post"),
    PUT("put");
    
    private final String a;

    private RequestMethod(String str) {
        this.a = str;
    }
}
