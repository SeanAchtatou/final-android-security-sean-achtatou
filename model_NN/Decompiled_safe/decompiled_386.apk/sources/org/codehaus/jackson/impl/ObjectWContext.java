package org.codehaus.jackson.impl;

/* compiled from: JsonWriteContext */
final class ObjectWContext extends JsonWriteContext {
    protected String _currentName = null;
    protected boolean _expectValue = false;

    public ObjectWContext(JsonWriteContext parent) {
        super(2, parent);
    }

    public String getCurrentName() {
        return this._currentName;
    }

    public int writeFieldName(String name) {
        if (this._currentName != null) {
            return 4;
        }
        this._currentName = name;
        return this._index < 0 ? 0 : 1;
    }

    public int writeValue() {
        if (this._currentName == null) {
            return 5;
        }
        this._currentName = null;
        this._index++;
        return 2;
    }

    /* access modifiers changed from: protected */
    public void appendDesc(StringBuilder sb) {
        sb.append('{');
        if (this._currentName != null) {
            sb.append('\"');
            sb.append(this._currentName);
            sb.append('\"');
        } else {
            sb.append('?');
        }
        sb.append(']');
    }
}
