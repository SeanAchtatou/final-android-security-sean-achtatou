package org.codehaus.jackson.impl;

import java.io.IOException;
import org.codehaus.jackson.Base64Variant;
import org.codehaus.jackson.JsonLocation;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonToken;
import org.codehaus.jackson.io.IOContext;
import org.codehaus.jackson.util.ByteArrayBuilder;
import org.codehaus.jackson.util.TextBuffer;

public abstract class JsonParserBase extends JsonParser {
    private static /* synthetic */ int[] $SWITCH_TABLE$org$codehaus$jackson$JsonToken = null;
    static final int INT_APOSTROPHE = 39;
    static final int INT_ASTERISK = 42;
    static final int INT_BACKSLASH = 92;
    static final int INT_COLON = 58;
    static final int INT_COMMA = 44;
    static final int INT_CR = 13;
    static final int INT_LBRACKET = 91;
    static final int INT_LCURLY = 123;
    static final int INT_LF = 10;
    static final int INT_QUOTE = 34;
    static final int INT_RBRACKET = 93;
    static final int INT_RCURLY = 125;
    static final int INT_SLASH = 47;
    static final int INT_SPACE = 32;
    static final int INT_TAB = 9;
    static final int INT_b = 98;
    static final int INT_f = 102;
    static final int INT_n = 110;
    static final int INT_r = 114;
    static final int INT_t = 116;
    static final int INT_u = 117;
    protected byte[] _binaryValue;
    ByteArrayBuilder _byteArrayBuilder = null;
    protected boolean _closed;
    protected long _currInputProcessed = 0;
    protected int _currInputRow = 1;
    protected int _currInputRowStart = 0;
    protected int _inputEnd = 0;
    protected int _inputPtr = 0;
    protected final IOContext _ioContext;
    protected boolean _nameCopied = false;
    protected char[] _nameCopyBuffer = null;
    protected JsonToken _nextToken;
    protected JsonReadContext _parsingContext;
    protected final TextBuffer _textBuffer;
    protected boolean _tokenIncomplete = false;
    protected int _tokenInputCol = 0;
    protected int _tokenInputRow = 1;
    protected long _tokenInputTotal = 0;

    /* access modifiers changed from: protected */
    public abstract void _closeInput() throws IOException;

    /* access modifiers changed from: protected */
    public abstract byte[] _decodeBase64(Base64Variant base64Variant) throws IOException, JsonParseException;

    /* access modifiers changed from: protected */
    public abstract void _finishString() throws IOException, JsonParseException;

    /* access modifiers changed from: protected */
    public abstract boolean loadMore() throws IOException;

    public abstract JsonToken nextToken() throws IOException, JsonParseException;

    static /* synthetic */ int[] $SWITCH_TABLE$org$codehaus$jackson$JsonToken() {
        int[] iArr = $SWITCH_TABLE$org$codehaus$jackson$JsonToken;
        if (iArr == null) {
            iArr = new int[JsonToken.values().length];
            try {
                iArr[JsonToken.END_ARRAY.ordinal()] = 5;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[JsonToken.END_OBJECT.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[JsonToken.FIELD_NAME.ordinal()] = 6;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[JsonToken.NOT_AVAILABLE.ordinal()] = 1;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[JsonToken.START_ARRAY.ordinal()] = 4;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[JsonToken.START_OBJECT.ordinal()] = 2;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[JsonToken.VALUE_EMBEDDED_OBJECT.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[JsonToken.VALUE_FALSE.ordinal()] = 12;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[JsonToken.VALUE_NULL.ordinal()] = INT_CR;
            } catch (NoSuchFieldError e9) {
            }
            try {
                iArr[JsonToken.VALUE_NUMBER_FLOAT.ordinal()] = INT_LF;
            } catch (NoSuchFieldError e10) {
            }
            try {
                iArr[JsonToken.VALUE_NUMBER_INT.ordinal()] = INT_TAB;
            } catch (NoSuchFieldError e11) {
            }
            try {
                iArr[JsonToken.VALUE_STRING.ordinal()] = 8;
            } catch (NoSuchFieldError e12) {
            }
            try {
                iArr[JsonToken.VALUE_TRUE.ordinal()] = 11;
            } catch (NoSuchFieldError e13) {
            }
            $SWITCH_TABLE$org$codehaus$jackson$JsonToken = iArr;
        }
        return iArr;
    }

    protected JsonParserBase(IOContext ctxt, int features) {
        this._ioContext = ctxt;
        this._features = features;
        this._textBuffer = ctxt.constructTextBuffer();
        this._parsingContext = JsonReadContext.createRootContext(this._tokenInputRow, this._tokenInputCol);
    }

    public JsonParser skipChildren() throws IOException, JsonParseException {
        if (this._currToken == JsonToken.START_OBJECT || this._currToken == JsonToken.START_ARRAY) {
            int open = 1;
            while (true) {
                JsonToken t = nextToken();
                if (t != null) {
                    switch ($SWITCH_TABLE$org$codehaus$jackson$JsonToken()[t.ordinal()]) {
                        case 2:
                        case 4:
                            open++;
                            break;
                        case 3:
                        case 5:
                            open--;
                            if (open != 0) {
                                break;
                            } else {
                                break;
                            }
                    }
                } else {
                    _handleEOF();
                }
            }
        }
        return this;
    }

    public String getCurrentName() throws IOException, JsonParseException {
        return this._parsingContext.getCurrentName();
    }

    public void close() throws IOException {
        this._closed = true;
        try {
            _closeInput();
        } finally {
            _releaseBuffers();
        }
    }

    public boolean isClosed() {
        return this._closed;
    }

    public JsonReadContext getParsingContext() {
        return this._parsingContext;
    }

    public JsonLocation getTokenLocation() {
        return new JsonLocation(this._ioContext.getSourceReference(), getTokenCharacterOffset(), getTokenLineNr(), getTokenColumnNr());
    }

    public JsonLocation getCurrentLocation() {
        return new JsonLocation(this._ioContext.getSourceReference(), (this._currInputProcessed + ((long) this._inputPtr)) - 1, this._currInputRow, (this._inputPtr - this._currInputRowStart) + 1);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public String getText() throws IOException, JsonParseException {
        if (this._currToken == null) {
            return null;
        }
        switch ($SWITCH_TABLE$org$codehaus$jackson$JsonToken()[this._currToken.ordinal()]) {
            case 6:
                return this._parsingContext.getCurrentName();
            case 7:
            default:
                return this._currToken.asString();
            case 8:
                if (this._tokenIncomplete) {
                    this._tokenIncomplete = false;
                    _finishString();
                    break;
                }
                break;
            case INT_TAB /*9*/:
            case INT_LF /*10*/:
                break;
        }
        return this._textBuffer.contentsAsString();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public char[] getTextCharacters() throws IOException, JsonParseException {
        if (this._currToken == null) {
            return null;
        }
        switch ($SWITCH_TABLE$org$codehaus$jackson$JsonToken()[this._currToken.ordinal()]) {
            case 6:
                if (!this._nameCopied) {
                    String name = this._parsingContext.getCurrentName();
                    int nameLen = name.length();
                    if (this._nameCopyBuffer == null) {
                        this._nameCopyBuffer = this._ioContext.allocNameCopyBuffer(nameLen);
                    } else if (this._nameCopyBuffer.length < nameLen) {
                        this._nameCopyBuffer = new char[nameLen];
                    }
                    name.getChars(0, nameLen, this._nameCopyBuffer, 0);
                    this._nameCopied = true;
                }
                return this._nameCopyBuffer;
            case 7:
            default:
                return this._currToken.asCharArray();
            case 8:
                if (this._tokenIncomplete) {
                    this._tokenIncomplete = false;
                    _finishString();
                    break;
                }
                break;
            case INT_TAB /*9*/:
            case INT_LF /*10*/:
                break;
        }
        return this._textBuffer.getTextBuffer();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public int getTextLength() throws IOException, JsonParseException {
        if (this._currToken == null) {
            return 0;
        }
        switch ($SWITCH_TABLE$org$codehaus$jackson$JsonToken()[this._currToken.ordinal()]) {
            case 6:
                return this._parsingContext.getCurrentName().length();
            case 7:
            default:
                return this._currToken.asCharArray().length;
            case 8:
                if (this._tokenIncomplete) {
                    this._tokenIncomplete = false;
                    _finishString();
                    break;
                }
                break;
            case INT_TAB /*9*/:
            case INT_LF /*10*/:
                break;
        }
        return this._textBuffer.size();
    }

    public int getTextOffset() throws IOException, JsonParseException {
        if (this._currToken != null) {
            switch ($SWITCH_TABLE$org$codehaus$jackson$JsonToken()[this._currToken.ordinal()]) {
                case 6:
                    return 0;
                case 8:
                    if (this._tokenIncomplete) {
                        this._tokenIncomplete = false;
                        _finishString();
                    }
                    return this._textBuffer.getTextOffset();
                case INT_TAB /*9*/:
                case INT_LF /*10*/:
                    return this._textBuffer.getTextOffset();
            }
        }
        return 0;
    }

    public final byte[] getBinaryValue(Base64Variant b64variant) throws IOException, JsonParseException {
        if (this._currToken != JsonToken.VALUE_STRING) {
            _reportError("Current token (" + this._currToken + ") not VALUE_STRING, can not access as binary");
        }
        if (this._tokenIncomplete) {
            try {
                this._binaryValue = _decodeBase64(b64variant);
                this._tokenIncomplete = false;
            } catch (IllegalArgumentException e) {
                throw _constructError("Failed to decode VALUE_STRING as base64 (" + b64variant + "): " + e.getMessage());
            }
        }
        return this._binaryValue;
    }

    public final long getTokenCharacterOffset() {
        return this._tokenInputTotal;
    }

    public final int getTokenLineNr() {
        return this._tokenInputRow;
    }

    public final int getTokenColumnNr() {
        return this._tokenInputCol + 1;
    }

    /* access modifiers changed from: protected */
    public final void loadMoreGuaranteed() throws IOException {
        if (!loadMore()) {
            _reportInvalidEOF();
        }
    }

    /* access modifiers changed from: protected */
    public void _releaseBuffers() throws IOException {
        this._textBuffer.releaseBuffers();
        char[] buf = this._nameCopyBuffer;
        if (buf != null) {
            this._nameCopyBuffer = null;
            this._ioContext.releaseNameCopyBuffer(buf);
        }
    }

    /* access modifiers changed from: protected */
    public void _handleEOF() throws JsonParseException {
        if (!this._parsingContext.inRoot()) {
            _reportInvalidEOF(": expected close marker for " + this._parsingContext.getTypeDesc() + " (from " + this._parsingContext.getStartLocation(this._ioContext.getSourceReference()) + ")");
        }
    }

    /* access modifiers changed from: protected */
    public void _reportUnexpectedChar(int ch, String comment) throws JsonParseException {
        String msg = "Unexpected character (" + _getCharDesc(ch) + ")";
        if (comment != null) {
            msg = String.valueOf(msg) + ": " + comment;
        }
        _reportError(msg);
    }

    /* access modifiers changed from: protected */
    public void _reportInvalidEOF() throws JsonParseException {
        _reportInvalidEOF(" in " + this._currToken);
    }

    /* access modifiers changed from: protected */
    public void _reportInvalidEOF(String msg) throws JsonParseException {
        _reportError("Unexpected end-of-input" + msg);
    }

    /* access modifiers changed from: protected */
    public void _throwInvalidSpace(int i) throws JsonParseException {
        _reportError("Illegal character (" + _getCharDesc((char) i) + "): only regular white space (\\r, \\n, \\t) is allowed between tokens");
    }

    /* access modifiers changed from: protected */
    public void _throwUnquotedSpace(int i, String ctxtDesc) throws JsonParseException {
        if (!isEnabled(JsonParser.Feature.ALLOW_UNQUOTED_CONTROL_CHARS) || i >= INT_SPACE) {
            _reportError("Illegal unquoted character (" + _getCharDesc((char) i) + "): has to be escaped using backslash to be included in " + ctxtDesc);
        }
    }

    /* access modifiers changed from: protected */
    public void _reportMismatchedEndMarker(int actCh, char expCh) throws JsonParseException {
        _reportError("Unexpected close marker '" + ((char) actCh) + "': expected '" + expCh + "' (for " + this._parsingContext.getTypeDesc() + " starting at " + new StringBuilder().append(this._parsingContext.getStartLocation(this._ioContext.getSourceReference())).toString() + ")");
    }

    protected static final String _getCharDesc(int ch) {
        char c = (char) ch;
        if (Character.isISOControl(c)) {
            return "(CTRL-CHAR, code " + ch + ")";
        }
        if (ch > 255) {
            return "'" + c + "' (code " + ch + " / 0x" + Integer.toHexString(ch) + ")";
        }
        return "'" + c + "' (code " + ch + ")";
    }

    /* access modifiers changed from: protected */
    public final void _reportError(String msg) throws JsonParseException {
        throw _constructError(msg);
    }

    /* access modifiers changed from: protected */
    public final void _wrapError(String msg, Throwable t) throws JsonParseException {
        throw _constructError(msg, t);
    }

    /* access modifiers changed from: protected */
    public final void _throwInternal() {
        throw new RuntimeException("Internal error: this code path should never get executed");
    }

    /* access modifiers changed from: protected */
    public final JsonParseException _constructError(String msg, Throwable t) {
        return new JsonParseException(msg, getCurrentLocation(), t);
    }

    public ByteArrayBuilder _getByteArrayBuilder() {
        if (this._byteArrayBuilder == null) {
            this._byteArrayBuilder = new ByteArrayBuilder();
        } else {
            this._byteArrayBuilder.reset();
        }
        return this._byteArrayBuilder;
    }
}
