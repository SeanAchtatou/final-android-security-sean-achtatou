package com.crossfield.ninjafall2;

public class Ground {
    public static final float SCROLL_SPEED = -20.0f;
    private int action;
    private Graphics g;
    private float h;
    private int id;
    private float w;
    private float x;
    private float y;

    public Ground(Graphics g2, int action2, int id2, float x2, float y2, float w2, float h2) {
        this.g = g2;
        setAction(action2);
        setId(id2);
        setX(x2);
        setY(y2);
        setW(w2);
        setH(h2);
    }

    public void init(Graphics g2, int action2, int id2, float x2, float y2, float w2, float h2) {
        this.g = g2;
        setAction(action2);
        setId(id2);
        setX(x2);
        setY(y2);
        setW(w2);
        setH(h2);
    }

    public void action() {
        scroll();
        switch (this.action) {
        }
    }

    public void draw() {
        this.g.drawImage(getId(), (int) getX(), (int) getY(), (int) getW(), (int) getH());
    }

    public void scroll() {
        this.y += -20.0f * this.g.ratio_height;
    }

    public boolean hitPlayer(Player player) {
        float top1 = this.y;
        float bottom1 = this.y + this.h;
        float left1 = this.x;
        float right1 = this.x + this.w;
        float top2 = player.getY() + player.getYoff();
        float bottom2 = top2 + player.getH();
        float left2 = player.getX() + player.getXoff();
        if (left1 > left2 + player.getW() || right1 < left2 || bottom1 < top2 || top1 > bottom2) {
            return false;
        }
        return true;
    }

    public void setAction(int action2) {
        this.action = action2;
    }

    public int getAction() {
        return this.action;
    }

    public void setId(int id2) {
        this.id = id2;
    }

    public int getId() {
        return this.id;
    }

    public void setX(float x2) {
        this.x = x2;
    }

    public float getX() {
        return this.x;
    }

    public void setY(float y2) {
        this.y = y2;
    }

    public float getY() {
        return this.y;
    }

    public void setW(float w2) {
        this.w = w2;
    }

    public float getW() {
        return this.w;
    }

    public void setH(float h2) {
        this.h = h2;
    }

    public float getH() {
        return this.h;
    }
}
