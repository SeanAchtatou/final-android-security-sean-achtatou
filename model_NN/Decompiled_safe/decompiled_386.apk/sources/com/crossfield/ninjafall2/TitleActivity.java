package com.crossfield.ninjafall2;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.text.SpannableStringBuilder;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsoluteLayout;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.crossfield.ninjafall2.BillingRequestService;
import com.crossfield.ninjafall2.android.utility.BillingConsts;
import com.crossfield.ninjafall2.android.utility.Constants;
import com.crossfield.ninjafall2.android.utility.RestWebServiceClient;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.openfeint.api.resource.Achievement;
import com.openfeint.api.resource.Leaderboard;
import com.openfeint.api.resource.Score;
import com.openfeint.api.ui.Dashboard;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

public class TitleActivity extends Activity implements Runnable {
    public static final int ADLANTIS_HEIGHT = 85;
    public static final int ADLANTIS_WIDTH = 480;
    public static final int ADLANTIS_X = 0;
    public static final int ADLANTIS_Y = 512;
    public static final String AMATEUR_ID = "1059432";
    public static final int AMATEUR_P = 500;
    public static final String BEGINNER_ID = "1059122";
    public static final int BEGINNER_P = 100;
    public static final String BEST_FLAG = "Best.Flag";
    public static final String BEST_RECORD = "Best.Record";
    public static final int BUTTON_GROUP_HEIGHT = 342;
    public static final int BUTTON_GROUP_WIDTH = 480;
    public static final int BUTTON_GROUP_X = 0;
    public static final int BUTTON_GROUP_Y = 597;
    public static final String DAILY_FLAG = "Daily.Flag";
    public static final String DAILY_RECORD = "Daily.Record";
    public static final String DAILY_TIME = "Daily.Time";
    public static final String JUMP_FLAG = "Jump.Flag";
    public static final String MASTER_ID = "1059472";
    public static final int MASTER_P = 10000;
    public static final String NAME = "Name";
    public static final int NAME_HEIGHT = -2;
    public static final int NAME_WIDTH = 480;
    public static final int NAME_X = 0;
    public static final int NAME_Y = 427;
    public static final String PROFESSIONAL_ID = "1059462";
    public static final int PROFESSIONAL_P = 5000;
    public static final String SCENE = "Scene";
    public static final String SCORE = "Score";
    public static final String SEMIPROFESSIONAL_ID = "1059452";
    public static final int SEMIPROFESSIONAL_P = 3000;
    private static final String TAG = "RankingActivity (Buy 3 Jump) ";
    public static final int TITLE_HEIGHT = -2;
    public static final String TITLE_PREFERENCE = "TitlePreference";
    public static final int TITLE_WIDTH = 480;
    public static final int TITLE_X = 0;
    public static final int TITLE_Y = 170;
    public static final int WRAP_CONTENT = -2;
    public static final float XPERIA_HEIGHT = 854.0f;
    public static final float XPERIA_WIDTH = 480.0f;
    private int adlantis_height = 85;
    private int adlantis_width = 480;
    private int adlantis_x = 0;
    private int adlantis_y = ADLANTIS_Y;
    private int buttonGroup_height = BUTTON_GROUP_HEIGHT;
    private int buttonGroup_width = 480;
    private int buttonGroup_x = 0;
    private int buttonGroup_y = BUTTON_GROUP_Y;
    /* access modifiers changed from: private */
    public String country = "";
    private ProgressDialog dialog;
    public int disp_height;
    public int disp_width;
    private DoSomethingThread doSomethingThread;
    private final Handler handler = new Handler();
    private Intent intent;
    private BillingRequestService mBillingService;
    private Handler mHandler;
    private PurchaseObserverImpl mPurchaseObserver;
    private int name_height = -2;
    private int name_width = 480;
    private int name_x = 0;
    private int name_y = NAME_Y;
    public float ratio_height;
    public float ratio_width;
    private int title_height = -2;
    private int title_width = 480;
    private int title_x = 0;
    private int title_y = 170;
    GoogleAnalyticsTracker tracker;

    public void getDisplaySize() {
        Display disp = ((WindowManager) getSystemService("window")).getDefaultDisplay();
        this.disp_width = disp.getWidth();
        this.disp_height = disp.getHeight();
    }

    public void getDisplayRatio() {
        getDisplaySize();
        this.ratio_width = ((float) this.disp_width) / 480.0f;
        this.ratio_height = ((float) this.disp_height) / 854.0f;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().addFlags(1024);
        getDisplaySize();
        getDisplayRatio();
        setContentView(R.layout.title);
        this.title_x = 0;
        this.title_x = (int) (((float) this.title_x) * this.ratio_width);
        this.title_y = 170;
        this.title_y = (int) (((float) this.title_y) * this.ratio_height);
        this.title_width = 480;
        this.title_width = (int) (((float) this.title_width) * this.ratio_width);
        this.title_height = -2;
        this.title_height = (int) (((float) this.title_height) * this.ratio_height);
        new LinearLayout(this);
        ((LinearLayout) findViewById(R.id.linearLayout_title)).setLayoutParams(new AbsoluteLayout.LayoutParams(this.title_width, this.title_height, this.title_x, this.title_y));
        this.name_x = 0;
        this.name_x = (int) (((float) this.name_x) * this.ratio_width);
        this.name_y = NAME_Y;
        this.name_y = (int) (((float) this.name_y) * this.ratio_height);
        this.name_width = 480;
        this.name_height = -2;
        this.name_height = (int) (((float) this.name_height) * this.ratio_height);
        new LinearLayout(this);
        ((LinearLayout) findViewById(R.id.linearLayout_name)).setLayoutParams(new AbsoluteLayout.LayoutParams(this.name_width, this.name_height, this.name_x, this.name_y));
        this.adlantis_x = 0;
        this.adlantis_x = (int) (((float) this.adlantis_x) * this.ratio_width);
        this.adlantis_y = ADLANTIS_Y;
        this.adlantis_y = (int) (((float) this.adlantis_y) * this.ratio_height);
        this.adlantis_width = 480;
        this.adlantis_width = (int) (((float) this.adlantis_width) * this.ratio_width);
        this.adlantis_height = 85;
        this.adlantis_height = (int) (((float) this.adlantis_height) * this.ratio_height);
        new LinearLayout(this);
        ((LinearLayout) findViewById(R.id.linearLayout_Adlantis)).setLayoutParams(new AbsoluteLayout.LayoutParams(this.adlantis_width, this.adlantis_height, this.adlantis_x, this.adlantis_y));
        this.buttonGroup_x = 0;
        this.buttonGroup_x = (int) (((float) this.buttonGroup_x) * this.ratio_width);
        this.buttonGroup_y = BUTTON_GROUP_Y;
        this.buttonGroup_y = (int) (((float) this.buttonGroup_y) * this.ratio_height);
        this.buttonGroup_width = 480;
        this.buttonGroup_width = (int) (((float) this.buttonGroup_width) * this.ratio_width);
        this.buttonGroup_height = BUTTON_GROUP_HEIGHT;
        this.buttonGroup_height = (int) (((float) this.buttonGroup_height) * this.ratio_height);
        new LinearLayout(this);
        ((LinearLayout) findViewById(R.id.linearLayout_button)).setLayoutParams(new AbsoluteLayout.LayoutParams(this.buttonGroup_width, this.buttonGroup_height, this.buttonGroup_x, this.buttonGroup_y));
        new Button(this);
        ((Button) findViewById(R.id.button_start)).setLayoutParams(new LinearLayout.LayoutParams(this.buttonGroup_width, (int) (85.4d * ((double) this.ratio_height))));
        new Button(this);
        ((Button) findViewById(R.id.button_ranking)).setLayoutParams(new LinearLayout.LayoutParams(this.buttonGroup_width, (int) (85.4d * ((double) this.ratio_height))));
        new LinearLayout(this);
        ((LinearLayout) findViewById(R.id.linearLayout_buttonMim)).setLayoutParams(new AbsoluteLayout.LayoutParams(this.buttonGroup_width, (int) (85.4d * ((double) this.ratio_height)), this.buttonGroup_x, this.buttonGroup_y + ((int) (170.8d * ((double) this.ratio_height)))));
        new Button(this);
        ((Button) findViewById(R.id.button_more)).setLayoutParams(new LinearLayout.LayoutParams((int) (((double) this.buttonGroup_width) * 1.0d), (int) (85.4d * ((double) this.ratio_height))));
        new Button(this);
        ((Button) findViewById(R.id.button_buy)).setLayoutParams(new LinearLayout.LayoutParams((int) (((double) this.buttonGroup_width) * 0.0d), (int) (85.4d * ((double) this.ratio_height))));
        this.mBillingService = new BillingRequestService();
        this.mBillingService.setContext(this);
        this.mHandler = new Handler();
        this.mPurchaseObserver = new PurchaseObserverImpl(this.mHandler);
        ResponseHandler.register(this.mPurchaseObserver);
        SharedPreferences pref = getSharedPreferences(TITLE_PREFERENCE, 3);
        String name = pref.getString(NAME, "");
        new EditText(this);
        EditText edit = (EditText) findViewById(R.id.editText_edit);
        if (!name.equalsIgnoreCase("")) {
            edit.setText(name);
        }
        if (name.equalsIgnoreCase("")) {
            edit.setText("Ninja");
        }
        edit.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean decision = false;
                if (actionId != 6) {
                    return false;
                }
                String str = ((SpannableStringBuilder) ((EditText) TitleActivity.this.findViewById(R.id.editText_edit)).getText()).toString();
                String oldName = TitleActivity.this.getSharedPreferences(TitleActivity.TITLE_PREFERENCE, 3).getString(TitleActivity.NAME, "");
                if (str.equalsIgnoreCase(oldName)) {
                    decision = false;
                }
                if (!str.equalsIgnoreCase(oldName)) {
                    return TitleActivity.this.yesnoDialog();
                }
                return decision;
            }
        });
        final EditText editText = (EditText) findViewById(R.id.editText_edit);
        edit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean flag) {
                if (!flag) {
                    ((InputMethodManager) TitleActivity.this.getSystemService("input_method")).hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
                editText.setFocusable(true);
                editText.setFocusableInTouchMode(true);
            }
        });
        SharedPreferences.Editor editor = pref.edit();
        for (int i = 0; i < 100; i++) {
            if (pref.getFloat(SCORE + i, -1.0f) == 0.0f) {
                editor.putFloat(SCORE + i, -1.0f);
            }
        }
        if (getJumpCountFromDatabase() == 3) {
            editor.commit();
        }
        editor.commit();
        this.tracker = GoogleAnalyticsTracker.getInstance();
        this.tracker.start("UA-23476120-32", this);
        this.tracker.trackPageView("Title");
        this.tracker.dispatch();
    }

    public void onResume() {
        super.onResume();
        this.tracker.trackPageView("Title");
        this.tracker.dispatch();
        SharedPreferences pref = getSharedPreferences(TITLE_PREFERENCE, 3);
        boolean record = pref.getBoolean(BEST_FLAG, false);
        boolean daily = pref.getBoolean(DAILY_FLAG, false);
        final long scoreValue = (long) pref.getFloat("Score0", -1.0f);
        if (record) {
            this.intent = new Intent(this, RankingActivity.class);
            new AlertDialog.Builder(this).setIcon((int) R.drawable.icon).setTitle("NewRecord!").setMessage("Sending score").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    Achievement a = new Achievement(TitleActivity.BEGINNER_ID);
                    if (100 <= scoreValue) {
                        a = new Achievement(TitleActivity.BEGINNER_ID);
                    } else if (500 <= scoreValue) {
                        a = new Achievement(TitleActivity.AMATEUR_ID);
                    } else if (3000 <= scoreValue) {
                        a = new Achievement(TitleActivity.SEMIPROFESSIONAL_ID);
                    } else if (5000 <= scoreValue) {
                        a = new Achievement(TitleActivity.PROFESSIONAL_ID);
                    } else if (10000 <= scoreValue) {
                        a = new Achievement(TitleActivity.MASTER_ID);
                    }
                    a.unlock(new Achievement.UnlockCB() {
                        public void onSuccess(boolean newUnlock) {
                            TitleActivity.this.setResult(-1);
                        }

                        public void onFailure(String exceptionMessage) {
                            Toast.makeText(TitleActivity.this, "Error (" + exceptionMessage + ") unlocking achievement.", 0).show();
                            TitleActivity.this.setResult(0);
                        }
                    });
                }
            }).show();
            SharedPreferences.Editor editor = pref.edit();
            editor.putBoolean(BEST_FLAG, false);
            editor.commit();
            if (scoreValue != -1) {
                new Score(scoreValue, null).submitTo(new Leaderboard("800966"), new Score.SubmitToCB() {
                    public void onSuccess(boolean newHighScore) {
                        TitleActivity.this.setResult(-1);
                    }

                    public void onFailure(String exceptionMessage) {
                        Toast.makeText(TitleActivity.this, "Error (" + exceptionMessage + ") posting score.", 0).show();
                        TitleActivity.this.setResult(0);
                    }
                });
            }
        } else if (daily) {
            this.intent = new Intent(this, RankingActivity.class);
            new AlertDialog.Builder(this).setIcon((int) R.drawable.icon).setTitle("Today's NewRecord!").setMessage("Sending score").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                }
            }).show();
            SharedPreferences.Editor editor2 = pref.edit();
            editor2.putBoolean(DAILY_FLAG, false);
            editor2.commit();
            if (scoreValue != -1) {
                new Score(scoreValue, null).submitTo(new Leaderboard("800966"), new Score.SubmitToCB() {
                    public void onSuccess(boolean newHighScore) {
                        TitleActivity.this.setResult(-1);
                    }

                    public void onFailure(String exceptionMessage) {
                        Toast.makeText(TitleActivity.this, "Error (" + exceptionMessage + ") posting score.", 0).show();
                        TitleActivity.this.setResult(0);
                    }
                });
            }
        }
    }

    public void insert(int mode) {
        TelephonyManager tel = (TelephonyManager) getSystemService("phone");
        SharedPreferences pref = getSharedPreferences(TITLE_PREFERENCE, 3);
        String name = pref.getString(NAME, "NoData");
        String record = String.valueOf((int) pref.getFloat("Score0", -1.0f));
        if (mode == 0) {
            record = String.valueOf((int) pref.getFloat("Score0", -1.0f));
        }
        if (mode == 1) {
            record = String.valueOf(pref.getInt(DAILY_RECORD, -1));
        }
        RestWebServiceClient restClient = new RestWebServiceClient(Constants.URL_INSER_POINT);
        List<NameValuePair> nameValuePairs = new ArrayList<>();
        nameValuePairs.add(new BasicNameValuePair("user_name", name));
        nameValuePairs.add(new BasicNameValuePair("user_point", record));
        nameValuePairs.add(new BasicNameValuePair("country_code", getResources().getConfiguration().locale.getCountry()));
        nameValuePairs.add(new BasicNameValuePair("device_id", tel.getDeviceId()));
        try {
            restClient.webPost("", nameValuePairs);
        } catch (Exception e) {
            showDialog(this, "Failure", "Transmission failure");
        }
    }

    public void buttonStart(View v) {
        this.tracker.trackEvent("Title", "Click", "Title_Start", 1);
        this.tracker.dispatch();
        int jumpCount = getJumpCount();
        new EditText(this);
        String str = ((SpannableStringBuilder) ((EditText) findViewById(R.id.editText_edit)).getText()).toString();
        if (str.length() != 0) {
            SharedPreferences.Editor editor = getSharedPreferences(TITLE_PREFERENCE, 0).edit();
            editor.putString(NAME, str);
            editor.commit();
            if (jumpCount == 2) {
                startActivity(new Intent(this, MainActivity.class));
            }
            if (jumpCount == 3) {
                startActivity(new Intent(this, ExtraActivity.class));
                return;
            }
            return;
        }
        showDialog(this, "Error", "Input name");
    }

    private int getJumpCount() {
        if (getSharedPreferences(TITLE_PREFERENCE, 3).getBoolean(JUMP_FLAG, false)) {
            return 3;
        }
        return 2;
    }

    private int getJumpCountFromDatabase() {
        try {
            RestWebServiceClient restClient = new RestWebServiceClient(Constants.URL_JUMP_COUNT);
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("device_id", getDeviceId()));
            return ((JSONObject) new JSONTokener(restClient.webPost("", nameValuePairs)).nextValue()).getInt("jumpCount");
        } catch (Exception e) {
            return 2;
        }
    }

    public void buttonRanking(View v) {
        this.tracker.trackEvent("Title", "Click", "Title_Ranking", 1);
        this.tracker.dispatch();
        new EditText(this);
        String str = ((SpannableStringBuilder) ((EditText) findViewById(R.id.editText_edit)).getText()).toString();
        if (str.length() != 0) {
            SharedPreferences.Editor editor = getSharedPreferences(TITLE_PREFERENCE, 0).edit();
            editor.putString(NAME, str);
            editor.commit();
            startActivity(new Intent(this, RankingActivity.class));
            return;
        }
        showDialog(this, "Error", "Input name");
    }

    public void buttonOpenFeint(View v) {
        this.tracker.trackPageView("OpenFeint");
        this.tracker.trackEvent("Title", "Click", "Title_OpenFeint", 1);
        this.tracker.dispatch();
        if (((long) getSharedPreferences(TITLE_PREFERENCE, 3).getFloat("Score0", -1.0f)) != -1) {
            Dashboard.open();
        }
    }

    private static void showDialog(Context context, String title, String text) {
        AlertDialog.Builder ad = new AlertDialog.Builder(context);
        ad.setTitle(title);
        ad.setMessage(text);
        ad.setPositiveButton("OK", (DialogInterface.OnClickListener) null);
        ad.show();
    }

    public boolean yesnoDialog() {
        boolean decision = false;
        final EditText name = (EditText) findViewById(R.id.editText_edit);
        final String str = ((SpannableStringBuilder) name.getText()).toString();
        String newName = getSharedPreferences(TITLE_PREFERENCE, 3).getString(NAME, "");
        new AlertDialog.Builder(this).setIcon((int) R.drawable.icon).setTitle("Look!").setMessage("Change name�H").setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                SharedPreferences.Editor editor = TitleActivity.this.getSharedPreferences(TitleActivity.TITLE_PREFERENCE, 0).edit();
                editor.putString(TitleActivity.NAME, str);
                editor.commit();
                name.setFocusable(false);
                name.setFocusableInTouchMode(true);
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                name.setText(TitleActivity.this.getSharedPreferences(TitleActivity.TITLE_PREFERENCE, 3).getString(TitleActivity.NAME, ""));
                name.setFocusable(false);
                name.setFocusableInTouchMode(true);
            }
        }).show();
        if (newName.equalsIgnoreCase(str)) {
            decision = false;
        }
        if (!newName.equalsIgnoreCase(str)) {
            return true;
        }
        return decision;
    }

    public void buttonMore(View v) {
        this.tracker.trackEvent("Title", "Click", "Title_More", 1);
        this.tracker.dispatch();
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://androida.me")));
    }

    public void buttonBuy(View v) {
        this.tracker.trackEvent("Title", "Click", "Title_Buy", 1);
        this.tracker.dispatch();
        this.country = getResources().getConfiguration().locale.getCountry();
        if (this.country.equalsIgnoreCase("JP")) {
            buyYesNoDialog("Ninja Jump", "�Q�[������1�x���������ł���悤�ɂȂ�܂��B�w��܂����H\n(1�x�w���Ό�ʂ͉i���I�ɑ����܂��B)\n(�w��ꍇ�A���f�����܂�1���قǂ�����܂��B)");
        } else {
            buyYesNoDialog("Ninja Jump", "Buy a \"Life\" to resume game from the same spot of dying. \nSo Ninja can fall more distance continuously! Buy? \n(It will take 1 minute to process buying request)");
        }
    }

    public void connectBuy() {
        this.dialog = new ProgressDialog(this);
        this.dialog.setIndeterminate(true);
        this.dialog.setMessage("connecting");
        this.dialog.show();
        this.doSomethingThread = new DoSomethingThread(this.handler, this);
        this.doSomethingThread.start();
    }

    public void run() {
        if (!this.mBillingService.doRequestPurchase(Constants.PRODUCT_ID, null)) {
            Toast.makeText(getBaseContext(), "Billing Not Supported.", 1).show();
        }
        this.dialog.dismiss();
    }

    private void buyYesNoDialog(String title, String text) {
        new AlertDialog.Builder(this).setIcon((int) R.drawable.icon).setTitle(title).setMessage(text).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                TitleActivity.this.connectBuy();
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        }).show();
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getAction() == 0) {
            event.getKeyCode();
        }
        return super.dispatchKeyEvent(event);
    }

    public void getCountry() {
        this.country = getResources().getConfiguration().locale.getCountry();
    }

    /* access modifiers changed from: private */
    public String getDeviceId() {
        return ((TelephonyManager) getSystemService("phone")).getDeviceId();
    }

    /* access modifiers changed from: private */
    public boolean checkIfProductAlreadyPurchased(String deviceId, String productId) {
        List<String> listId = populateListProductId(deviceId);
        if (listId == null || !listId.contains(productId)) {
            return false;
        }
        return true;
    }

    private List<String> populateListProductId(String deviceId) {
        List<String> listId = new ArrayList<>();
        RestWebServiceClient restClient = new RestWebServiceClient(Constants.URL_PURCHASED_PRD_LIST);
        List<NameValuePair> nameValuePairs = new ArrayList<>();
        nameValuePairs.add(new BasicNameValuePair("device_id", deviceId));
        try {
            JSONArray arrayJson = ((JSONObject) new JSONTokener(restClient.webPost("", nameValuePairs)).nextValue()).getJSONArray("list_product_id");
            for (int i = 0; i < arrayJson.length(); i++) {
                listId.add(arrayJson.getString(i));
            }
        } catch (Exception e) {
            Log.d("Error: ", e.getMessage());
        }
        return listId;
    }

    /* access modifiers changed from: private */
    public boolean insertPurchaseInfoIntoDB(String deviceId, String productId) {
        RestWebServiceClient restClient = new RestWebServiceClient(Constants.URL_PURCHASE_INSERT);
        List<NameValuePair> nameValuePairs = new ArrayList<>();
        nameValuePairs.add(new BasicNameValuePair("device_id", deviceId));
        nameValuePairs.add(new BasicNameValuePair("product_id", productId));
        String response = restClient.webPost("", nameValuePairs);
        boolean status = true;
        try {
            status = ((JSONObject) new JSONTokener(response).nextValue()).getBoolean("status");
        } catch (Exception e) {
            Log.d("Error: ", e.getMessage());
        }
        if (response == null || !status) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: private */
    public boolean deletePurchaseInfoFromDB(String deviceId, String productId) {
        RestWebServiceClient restClient = new RestWebServiceClient(Constants.URL_PURCHASE_DELETE);
        List<NameValuePair> nameValuePairs = new ArrayList<>();
        nameValuePairs.add(new BasicNameValuePair("device_id", deviceId));
        nameValuePairs.add(new BasicNameValuePair("product_id", productId));
        String response = restClient.webPost("", nameValuePairs);
        boolean status = true;
        try {
            status = ((JSONObject) new JSONTokener(response).nextValue()).getBoolean("status");
        } catch (Exception e) {
            Log.d("Error: ", e.getMessage());
        }
        if (response == null || !status) {
            return false;
        }
        return true;
    }

    private class PurchaseObserverImpl extends PurchaseObserver {
        public PurchaseObserverImpl(Handler handler) {
            super(TitleActivity.this, handler);
        }

        public void onBillingSupported(boolean supported) {
            Log.i(TitleActivity.TAG, "supported: " + supported);
            if (!supported) {
                Toast.makeText(TitleActivity.this.getBaseContext(), "Billing Not Supported.", 1).show();
            }
        }

        public void onPurchaseStateChange(BillingConsts.PurchaseState purchaseState, String itemId, int quantity, long purchaseTime, String developerPayload) {
            Log.i(TitleActivity.TAG, "onPurchaseStateChange() itemId: " + itemId + " " + purchaseState);
            if (purchaseState == BillingConsts.PurchaseState.PURCHASED) {
                SharedPreferences.Editor editor = TitleActivity.this.getSharedPreferences(TitleActivity.TITLE_PREFERENCE, 3).edit();
                editor.putBoolean(TitleActivity.JUMP_FLAG, true);
                editor.commit();
                TitleActivity.this.getCountry();
                if (TitleActivity.this.country.equalsIgnoreCase("JP")) {
                    Toast.makeText(TitleActivity.this.getBaseContext(), "���C�t�|�C���g�̍w������܂����B", 1).show();
                } else {
                    Toast.makeText(TitleActivity.this.getBaseContext(), "Your purchase process is completed!", 1).show();
                }
                int i = 0;
                while (i < 1000 && !TitleActivity.this.insertPurchaseInfoIntoDB(TitleActivity.this.getDeviceId(), itemId)) {
                    if (!TitleActivity.this.checkIfProductAlreadyPurchased(TitleActivity.this.getDeviceId(), Constants.PRODUCT_ID)) {
                        try {
                            Thread.sleep(500);
                        } catch (InterruptedException e) {
                        }
                        i++;
                    } else {
                        return;
                    }
                }
            } else if (purchaseState == BillingConsts.PurchaseState.CANCELED) {
                SharedPreferences.Editor editor2 = TitleActivity.this.getSharedPreferences(TitleActivity.TITLE_PREFERENCE, 3).edit();
                editor2.putBoolean(TitleActivity.JUMP_FLAG, false);
                editor2.commit();
                int i2 = 0;
                while (i2 < 1000 && !TitleActivity.this.deletePurchaseInfoFromDB(TitleActivity.this.getDeviceId(), itemId)) {
                    if (TitleActivity.this.checkIfProductAlreadyPurchased(TitleActivity.this.getDeviceId(), Constants.PRODUCT_ID)) {
                        try {
                            Thread.sleep(500);
                        } catch (InterruptedException e2) {
                        }
                        i2++;
                    } else {
                        return;
                    }
                }
            } else if (purchaseState == BillingConsts.PurchaseState.REFUNDED) {
                SharedPreferences.Editor editor3 = TitleActivity.this.getSharedPreferences(TitleActivity.TITLE_PREFERENCE, 3).edit();
                editor3.putBoolean(TitleActivity.JUMP_FLAG, false);
                editor3.commit();
                int i3 = 0;
                while (i3 < 1000 && !TitleActivity.this.deletePurchaseInfoFromDB(TitleActivity.this.getDeviceId(), itemId)) {
                    if (TitleActivity.this.checkIfProductAlreadyPurchased(TitleActivity.this.getDeviceId(), Constants.PRODUCT_ID)) {
                        try {
                            Thread.sleep(500);
                        } catch (InterruptedException e3) {
                        }
                        i3++;
                    } else {
                        return;
                    }
                }
            }
        }

        public void onRequestPurchaseResponse(BillingRequestService.RequestPurchase request, BillingConsts.ResponseCode responseCode) {
            Log.d(TitleActivity.TAG, String.valueOf(request.mProductId) + ": " + responseCode);
            if (responseCode == BillingConsts.ResponseCode.RESULT_OK) {
                Log.i(TitleActivity.TAG, "purchase was successfully sent to server");
            } else if (responseCode == BillingConsts.ResponseCode.RESULT_USER_CANCELED) {
                Log.i(TitleActivity.TAG, "user canceled purchase");
            } else {
                Log.i(TitleActivity.TAG, "purchase failed");
            }
        }

        public void onRestoreTransactionsResponse(BillingRequestService.RestoreTransactions request, BillingConsts.ResponseCode responseCode) {
            if (responseCode == BillingConsts.ResponseCode.RESULT_OK) {
                Log.d(TitleActivity.TAG, "completed RestoreTransactions request");
            } else {
                Log.d(TitleActivity.TAG, "RestoreTransactions error: " + responseCode);
            }
        }
    }
}
