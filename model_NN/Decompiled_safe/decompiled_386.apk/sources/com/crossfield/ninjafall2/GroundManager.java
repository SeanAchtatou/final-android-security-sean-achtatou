package com.crossfield.ninjafall2;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

public class GroundManager {
    public static final int END = 2;
    public static final float GROUND_HEIGHT_MAX = 100.0f;
    public static final float GROUND_HEIGHT_MIN = 20.0f;
    public static final int GROUND_RATE = 20;
    public static final float GROUND_SPACE_HEIGHT = 200.0f;
    public static final float GROUND_SPACE_WIDTH = 100.0f;
    public static final float GROUND_WIDTH_MAX = 100.0f;
    public static final float GROUND_WIDTH_MIN = 100.0f;
    public static final int INIT = 0;
    public static final int PLAY = 1;
    private Graphics g;
    public ArrayList<Ground> ground;
    public int ground_h = 20;
    private float ground_space_height = 200.0f;
    private float ground_space_width = 100.0f;
    private int ground_time;
    public int ground_w = 80;
    private Random random;

    public GroundManager(Graphics g2) {
        this.g = g2;
        this.random = new Random();
        init();
    }

    public void action() {
        this.ground_time--;
        if (this.ground_time <= 0) {
            this.ground_time = this.random.nextInt(20);
            setGround();
        }
        deleteGround();
        Iterator<Ground> it = this.ground.iterator();
        while (it.hasNext()) {
            it.next().action();
        }
    }

    public void draw() {
        Iterator<Ground> it = this.ground.iterator();
        while (it.hasNext()) {
            it.next().draw();
        }
    }

    public void init() {
        float set_w = 100.0f * this.g.ratio_width;
        float set_h = 20.0f * this.g.ratio_height;
        float set_x = (float) (((double) (240.0f * this.g.ratio_width)) - (((double) set_w) * 0.5d));
        float set_y = 768.0f * this.g.ratio_height;
        float set_space_width = this.ground_space_width * this.g.ratio_width;
        float set_space_height = this.ground_space_height * this.g.ratio_height;
        this.ground = new ArrayList<>();
        ArrayList<Ground> arrayList = this.ground;
        Graphics graphics = this.g;
        this.g.getClass();
        arrayList.add(new Ground(graphics, 0, 10, set_x, set_y, set_w, set_h));
        do {
            float oldX = set_x;
            while (true) {
                switch (this.random.nextInt(2)) {
                    case 0:
                        set_x = oldX - (((float) this.random.nextInt((int) set_space_width)) + set_w);
                        break;
                    case 1:
                        set_x = oldX + ((float) this.random.nextInt((int) set_space_width)) + set_w;
                        break;
                }
                if (set_x >= 0.0f && set_x + set_w <= ((float) this.g.disp_width)) {
                    set_y += ((float) this.random.nextInt((int) set_space_height)) + set_h;
                    ArrayList<Ground> arrayList2 = this.ground;
                    Graphics graphics2 = this.g;
                    this.g.getClass();
                    arrayList2.add(new Ground(graphics2, 0, 10, set_x, set_y, set_w, set_h));
                }
            }
        } while (set_y < 768.0f * this.g.ratio_height);
    }

    public void setGround() {
        float set_w = 100.0f * this.g.ratio_width;
        float set_h = 20.0f * this.g.ratio_height;
        float set_x = 0.0f;
        float set_y = (float) this.g.disp_height;
        float set_space_width = this.ground_space_width * this.g.ratio_width;
        float oldX = 0.0f;
        Iterator<Ground> it = this.ground.iterator();
        while (it.hasNext()) {
            Ground ground2 = it.next();
            if (ground2.getY() > 0.0f) {
                oldX = ground2.getX();
            }
        }
        while (true) {
            switch (this.random.nextInt(2)) {
                case 0:
                    set_x = oldX - (((float) this.random.nextInt((int) set_space_width)) + set_w);
                    break;
                case 1:
                    set_x = oldX + ((float) this.random.nextInt((int) set_space_width)) + set_w;
                    break;
            }
            if (set_x >= 0.0f && set_x + set_w <= ((float) this.g.disp_width)) {
                ArrayList<Ground> arrayList = this.ground;
                Graphics graphics = this.g;
                this.g.getClass();
                arrayList.add(new Ground(graphics, 0, 10, set_x, set_y, set_w, set_h));
                return;
            }
        }
    }

    public void deleteGround() {
        while (true) {
            int count = -1;
            Iterator<Ground> it = this.ground.iterator();
            while (true) {
                if (it.hasNext()) {
                    Ground ground2 = it.next();
                    if (ground2.getY() + ground2.getH() < 0.0f) {
                        count = -1 + 1;
                        break;
                    }
                } else {
                    break;
                }
            }
            if (count != -1) {
                this.ground.remove(count);
            } else {
                return;
            }
        }
    }

    public void reset() {
        deleteGround();
        Iterator<Ground> it = this.ground.iterator();
        while (it.hasNext()) {
            it.next().action();
        }
    }

    public boolean checkScreen() {
        boolean exist = false;
        Iterator<Ground> it = this.ground.iterator();
        while (it.hasNext()) {
            Ground ground2 = it.next();
            if (ground2.getX() + ground2.getW() >= 0.0f && ground2.getY() + ground2.getH() >= 0.0f && ground2.getX() <= ((float) this.g.disp_width) && ground2.getY() <= ((float) this.g.disp_height)) {
                exist = true;
            }
        }
        return exist;
    }

    public Ground hitPlayer(Player player) {
        Iterator<Ground> it = this.ground.iterator();
        while (it.hasNext()) {
            Ground ground2 = it.next();
            if (ground2.getY() >= (player.getY() - player.getYspeed()) - 0.21875f) {
                float top1 = ground2.getY();
                float bottom1 = top1 + ground2.getH();
                float left1 = ground2.getX();
                float right1 = left1 + ground2.getW();
                float top2 = (player.getY() + player.getYoff()) - player.getYspeed();
                float bottom2 = player.getY() + player.getYoff() + player.getH();
                float left2 = player.getX() + player.getXoff() + player.getXhit();
                if (left1 <= (player.getW() + left2) - (player.getXhit() * 2.0f) && right1 >= left2 && bottom1 >= top2 && top1 <= bottom2) {
                    return ground2;
                }
            }
        }
        return null;
    }
}
