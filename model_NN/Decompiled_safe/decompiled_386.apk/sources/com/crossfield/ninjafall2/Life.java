package com.crossfield.ninjafall2;

public class Life {
    public static final int LIFE = 1;
    private Graphics g;
    private float h;
    private int id;
    private int life = 1;
    private float w;
    private float x;
    private float y;

    public Life(Graphics g2, int id2, float x2, float y2, float w2, float h2) {
        this.g = g2;
        setX(x2);
        setY(y2);
        setW(w2);
        setH(h2);
        setId(id2);
        setLife(1);
    }

    public void action() {
    }

    public void draw() {
        for (int i = this.life; i > 0; i--) {
            this.g.drawImage(getId(), ((int) getX()) - ((int) ((((float) (this.life - i)) * getW()) + (((float) (this.life - i)) * (getW() / 2.0f)))), (int) getY(), (int) getW(), (int) getH());
        }
    }

    public void setX(float x2) {
        this.x = x2;
    }

    public float getX() {
        return this.x;
    }

    public void setY(float y2) {
        this.y = y2;
    }

    public float getY() {
        return this.y;
    }

    public void setW(float w2) {
        this.w = w2;
    }

    public float getW() {
        return this.w;
    }

    public void setH(float h2) {
        this.h = h2;
    }

    public float getH() {
        return this.h;
    }

    public void setId(int id2) {
        this.id = id2;
    }

    public int getId() {
        return this.id;
    }

    public void setLife(int life2) {
        this.life = life2;
    }

    public int getLife() {
        return this.life;
    }
}
