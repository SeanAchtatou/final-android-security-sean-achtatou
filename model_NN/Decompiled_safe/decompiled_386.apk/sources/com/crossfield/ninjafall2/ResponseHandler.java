package com.crossfield.ninjafall2;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.crossfield.ninjafall2.BillingRequestService;
import com.crossfield.ninjafall2.android.utility.BillingConsts;

public class ResponseHandler {
    private static final String TAG = "ResponseHandler";
    /* access modifiers changed from: private */
    public static PurchaseObserver sPurchaseObserver;

    public static synchronized void register(PurchaseObserver observer) {
        synchronized (ResponseHandler.class) {
            sPurchaseObserver = observer;
        }
    }

    public static synchronized void unregister(PurchaseObserver observer) {
        synchronized (ResponseHandler.class) {
            sPurchaseObserver = null;
        }
    }

    public static void checkBillingSupportedResponse(boolean supported) {
        if (sPurchaseObserver != null) {
            sPurchaseObserver.onBillingSupported(supported);
        }
    }

    public static void buyPageIntentResponse(PendingIntent pendingIntent, Intent intent) {
        if (sPurchaseObserver == null) {
            Log.d(TAG, "UI is not running");
        } else {
            sPurchaseObserver.startBuyPageActivity(pendingIntent, intent);
        }
    }

    public static void purchaseResponse(Context context, BillingConsts.PurchaseState purchaseState, String productId, String orderId, long purchaseTime, String developerPayload) {
        final BillingConsts.PurchaseState purchaseState2 = purchaseState;
        final String str = productId;
        final long j = purchaseTime;
        final String str2 = developerPayload;
        new Thread(new Runnable() {
            public void run() {
                synchronized (ResponseHandler.class) {
                    if (ResponseHandler.sPurchaseObserver != null) {
                        ResponseHandler.sPurchaseObserver.postPurchaseStateChange(BillingConsts.PurchaseState.this, str, 1, j, str2);
                    }
                }
            }
        }).start();
    }

    public static void responseCodeReceived(Context context, BillingRequestService.RequestPurchase request, BillingConsts.ResponseCode responseCode) {
        if (sPurchaseObserver != null) {
            sPurchaseObserver.onRequestPurchaseResponse(request, responseCode);
        }
    }

    public static void responseCodeReceived(Context context, BillingRequestService.RestoreTransactions request, BillingConsts.ResponseCode responseCode) {
        if (sPurchaseObserver != null) {
            sPurchaseObserver.onRestoreTransactionsResponse(request, responseCode);
        }
    }
}
