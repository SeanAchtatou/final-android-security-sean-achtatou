package com.crossfield.ninjafall2.android.utility;

import android.text.TextUtils;
import android.util.Log;
import com.crossfield.ninjafall2.android.utility.BillingConsts;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

public class Security {
    private static final String KEY_FACTORY_ALGORITHM = "RSA";
    private static final SecureRandom RANDOM = new SecureRandom();
    private static final String SIGNATURE_ALGORITHM = "SHA1withRSA";
    private static final String TAG = "Security";
    private static HashSet<Long> sKnownNonces = new HashSet<>();

    public static class VerifiedPurchase {
        public String developerPayload;
        public String notificationId;
        public String orderId;
        public String productId;
        public BillingConsts.PurchaseState purchaseState;
        public long purchaseTime;

        public VerifiedPurchase(BillingConsts.PurchaseState purchaseState2, String notificationId2, String productId2, String orderId2, long purchaseTime2, String developerPayload2) {
            this.purchaseState = purchaseState2;
            this.notificationId = notificationId2;
            this.productId = productId2;
            this.orderId = orderId2;
            this.purchaseTime = purchaseTime2;
            this.developerPayload = developerPayload2;
        }
    }

    public static long generateNonce() {
        long nonce = RANDOM.nextLong();
        sKnownNonces.add(Long.valueOf(nonce));
        return nonce;
    }

    public static void removeNonce(long nonce) {
        sKnownNonces.remove(Long.valueOf(nonce));
    }

    public static boolean isNonceKnown(long nonce) {
        return sKnownNonces.contains(Long.valueOf(nonce));
    }

    /* JADX INFO: Multiple debug info for r4v8 java.lang.String: [D('response' java.lang.String), D('params' java.util.Map<java.lang.String, java.lang.String>)] */
    /* JADX INFO: Multiple debug info for r20v11 boolean: [D('signature' java.lang.String), D('verified' boolean)] */
    /* JADX INFO: Multiple debug info for r4v15 org.json.JSONObject: [D('response' java.lang.String), D('object' org.json.JSONObject)] */
    public static ArrayList<VerifiedPurchase> verifyPurchase(String signedData, String signature) {
        boolean verified;
        int numTransactions;
        String pkInitPart;
        if (signedData == null) {
            Log.e(TAG, "data is null");
            return null;
        }
        Log.i(TAG, "signedData: " + signedData);
        if (!TextUtils.isEmpty(signature)) {
            try {
                pkInitPart = ((JSONObject) new JSONTokener(new RestWebServiceClient(Constants.URL_PK_INITIAL_PART).webGet("", new HashMap<>())).nextValue()).getString("pkInitialPart");
            } catch (Exception e) {
                Log.d("Error: ", e.getMessage());
                pkInitPart = "";
            }
            boolean verified2 = verify(generatePublicKey(String.valueOf(pkInitPart) + "w0BAQEFAAOCAQ8AMIIBCgKCAQEAi7RdOZX/lG6MLP5KQ+t5+2h+CFWIXy/hhmc5sDZrpebYWDsZo7Q+/uWKW6sJsS/PPvznkg+HH5heTOPurdaS+S8Cm525kkD5KP/jhFz4koW+vLrrvg/Wd/D4qzqJi7EYbJ5pf4ef4lxFiGudKqDZR3l+KtB+5n4YQ+UvxSDvGPyvo4hG/mLLIxlqWJOUKRe9+sgm8ysTKDAcUDYqo0OtYCkyjUR/od3WJuFPz/bsCdQQSTAdQY7VhBqLfN5MOMrbbO2ZK5KzeXJiwZqH6cA9WaP8/FAA/U1WsEXB84DHK3w0y9TMCCWOA9e9DRbk8QmX43BurL37Vyq0zg9XU6YofwIDAQAB"), signedData, signature);
            if (!verified2) {
                Log.w(TAG, "signature does not match data.");
                return null;
            }
            verified = verified2;
        } else {
            verified = false;
        }
        try {
            JSONObject jSONObject = new JSONObject(signedData);
            long nonce = jSONObject.optLong("nonce");
            try {
                JSONArray jTransactionsArray = jSONObject.optJSONArray("orders");
                if (jTransactionsArray != null) {
                    try {
                        numTransactions = jTransactionsArray.length();
                    } catch (JSONException e2) {
                        return null;
                    }
                } else {
                    numTransactions = 0;
                }
                if (isNonceKnown(nonce) == 0) {
                    Log.w(TAG, "Nonce not found: " + nonce);
                    return null;
                }
                ArrayList<VerifiedPurchase> purchases = new ArrayList<>();
                int i = 0;
                while (i < numTransactions) {
                    try {
                        JSONObject jElement = jTransactionsArray.getJSONObject(i);
                        BillingConsts.PurchaseState purchaseState = BillingConsts.PurchaseState.valueOf(jElement.getInt("purchaseState"));
                        String productId = jElement.getString("productId");
                        long purchaseTime = jElement.getLong("purchaseTime");
                        String orderId = jElement.optString("orderId", "");
                        String notifyId = null;
                        if (jElement.has("notificationId")) {
                            notifyId = jElement.getString("notificationId");
                        }
                        String developerPayload = jElement.optString("developerPayload", null);
                        if (purchaseState != BillingConsts.PurchaseState.PURCHASED || verified) {
                            purchases.add(new VerifiedPurchase(purchaseState, notifyId, productId, orderId, purchaseTime, developerPayload));
                        }
                        i++;
                    } catch (JSONException e3) {
                        Log.e(TAG, "JSON exception: ", e3);
                        return null;
                    }
                }
                removeNonce(nonce);
                return purchases;
            } catch (JSONException e4) {
            }
        } catch (JSONException e5) {
            return null;
        }
    }

    public static PublicKey generatePublicKey(String encodedPublicKey) {
        try {
            return KeyFactory.getInstance(KEY_FACTORY_ALGORITHM).generatePublic(new X509EncodedKeySpec(Base64.decode(encodedPublicKey)));
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        } catch (InvalidKeySpecException e2) {
            Log.e(TAG, "Invalid key specification.");
            throw new IllegalArgumentException(e2);
        } catch (Base64DecoderException e3) {
            Log.e(TAG, "Base64 decoding failed.");
            throw new IllegalArgumentException(e3);
        }
    }

    public static boolean verify(PublicKey publicKey, String signedData, String signature) {
        Log.i(TAG, "signature: " + signature);
        try {
            Signature sig = Signature.getInstance(SIGNATURE_ALGORITHM);
            sig.initVerify(publicKey);
            sig.update(signedData.getBytes());
            if (sig.verify(Base64.decode(signature))) {
                return true;
            }
            Log.e(TAG, "Signature verification failed.");
            return false;
        } catch (NoSuchAlgorithmException e) {
            Log.e(TAG, "NoSuchAlgorithmException.");
            return false;
        } catch (InvalidKeyException e2) {
            Log.e(TAG, "Invalid key specification.");
            return false;
        } catch (SignatureException e3) {
            Log.e(TAG, "Signature exception.");
            return false;
        } catch (Base64DecoderException e4) {
            Log.e(TAG, "Base64 decoding failed.");
            return false;
        }
    }
}
