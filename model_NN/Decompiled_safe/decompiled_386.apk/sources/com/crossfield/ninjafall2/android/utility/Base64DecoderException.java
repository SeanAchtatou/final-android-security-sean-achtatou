package com.crossfield.ninjafall2.android.utility;

public class Base64DecoderException extends Exception {
    private static final long serialVersionUID = 1;

    public Base64DecoderException() {
    }

    public Base64DecoderException(String s) {
        super(s);
    }
}
