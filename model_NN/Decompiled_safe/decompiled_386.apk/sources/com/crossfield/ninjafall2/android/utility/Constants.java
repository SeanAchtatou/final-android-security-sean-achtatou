package com.crossfield.ninjafall2.android.utility;

public class Constants {
    public static final String PRODUCT_ID = "product_001";
    public static final String RANKING_CATEGORY_ALL = "all";
    public static final String RANKING_CATEGORY_MONTHLY = "monthly";
    public static final String RANKING_CATEGORY_TODAY = "today";
    public static final String RANKING_CATEGORY_WEEKLY = "weekly";
    public static final String RANKING_CATEGORY_YESTERDAY = "yesterday";
    public static final String RANKING_COUNTRY_DOMESTIC = "JP";
    public static final String RANKING_COUNTRY_LOCAL = "JP";
    public static final String RANKING_COUNTRY_WORLD = "world";
    public static final String URL_GET_RANKING = "http://111.68.211.144:8080/ninjafallweb/get_ranking.rs";
    public static final String URL_INSER_POINT = "http://111.68.211.144:8080/ninjafallweb/submit_point.rs";
    public static final String URL_JUMP_COUNT = "http://111.68.211.144:8080/ninjafallweb/get_jump_count.rs";
    public static final String URL_PK_INITIAL_PART = "http://111.68.211.144:8080/ninjafallweb/get_pk_initial_part.rs";
    public static final String URL_PURCHASED_PRD_LIST = "http://111.68.211.144:8080/ninjafallweb/purchased_product_list.rs";
    public static final String URL_PURCHASE_DELETE = "http://111.68.211.144:8080/ninjafallweb/purchase_delete.rs";
    public static final String URL_PURCHASE_INSERT = "http://111.68.211.144:8080/ninjafallweb/purchase_insert.rs";
}
