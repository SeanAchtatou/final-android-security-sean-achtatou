package com.crossfield.ninjafall2;

public class Player {
    public static final int ANM_SPEED = 4;
    public static final int FLOAT = 3;
    public static final float FLOAT_SPEED = -40.0f;
    public static final float GRAVITY = 10.0f;
    public static final float JUMP_SPEED = -50.0f;
    public static final int LEFT = 1;
    public static final int RIGHT = 2;
    public static final int WATE = 0;
    public static final float XSPEED = 20.0f;
    private int anm_cnt = 0;
    private Graphics g;
    private float h;
    private int image_id;
    private int move_mode = 0;
    private int scene;
    private float w;
    private float x;
    private float xhit;
    private float xoff = 0.0f;
    private float xspeed = 20.0f;
    private float y;
    private float yhit;
    private float yoff = 0.0f;
    private float yspeed = 0.0f;

    public Player(Graphics g2, int image_id2, float x2, float y2, float w2, float h2) {
        this.g = g2;
        setScene(0);
        this.image_id = image_id2;
        setX(x2);
        setY(y2);
        setW(w2);
        setH(h2);
        setXoff((w2 / -2.0f) + ((float) ((int) (((double) w2) * -0.2d))));
        setYoff((float) ((int) (((double) h2) * -0.9d)));
        setXhit((float) ((int) ((((double) w2) * 0.6d) / 2.0d)));
        setYhit((float) ((int) ((((double) h2) * 0.0d) / 2.0d)));
        setXspeed(g2.ratio_width * 20.0f);
        setYspeed(0.0f);
    }

    public void action() {
        move();
        warp();
        fall();
    }

    public void draw() {
        animation();
        int i = this.image_id;
        this.g.getClass();
        if (i == 2) {
            this.g.drawImage(this.image_id, (int) (getX() + getXoff()), (int) (getY() + getYoff()), (int) getW(), (int) getH());
            Graphics graphics = this.g;
            this.g.getClass();
            graphics.drawImage(3, (int) (getX() + getXoff()), (int) (((double) getY()) + (((double) getYoff()) * 1.8d)), (int) getW(), (int) getH());
            return;
        }
        this.g.drawImage(this.image_id, (int) (getX() + getXoff()), (int) (getY() + getYoff()), (int) getW(), (int) getH());
    }

    public void animation() {
        switch (getMove_mode()) {
            case 0:
                this.g.getClass();
                this.image_id = 1;
                this.anm_cnt = 0;
                return;
            case 1:
                this.g.getClass();
                this.image_id = 4;
                if (this.anm_cnt >= 2) {
                    this.g.getClass();
                    this.image_id = 5;
                }
                this.anm_cnt++;
                if (this.anm_cnt >= 4) {
                    this.anm_cnt = 0;
                    return;
                }
                return;
            case 2:
                this.g.getClass();
                this.image_id = 6;
                if (this.anm_cnt >= 2) {
                    this.g.getClass();
                    this.image_id = 7;
                }
                this.anm_cnt++;
                if (this.anm_cnt >= 4) {
                    this.anm_cnt = 0;
                    return;
                }
                return;
            case 3:
                this.g.getClass();
                this.image_id = 2;
                this.anm_cnt = 0;
                setMove_mode(0);
                return;
            default:
                return;
        }
    }

    public void move() {
        switch (getMove_mode()) {
            case 0:
            default:
                return;
            case 1:
                setX(getX() - getXspeed());
                return;
            case 2:
                setX(getX() + getXspeed());
                return;
        }
    }

    public void fall() {
        setYspeed(getYspeed() + 10.0f);
        setY(getY() + getYspeed());
    }

    public void jump() {
        setYspeed(-50.0f);
        setY(getY() - 0.08984375f);
    }

    public void warp() {
        if (getX() < getXoff()) {
            setX((float) (this.g.disp_width - 1));
        } else if (((float) this.g.disp_width) < getX() + getXoff()) {
            setX(getXoff() + 1.0f);
        }
    }

    public void pointCorrect(Ground ground) {
        setYspeed(0.0f);
        this.y = ground.getY();
    }

    public void setMove_mode(int move_mode2) {
        this.move_mode = move_mode2;
    }

    public int getMove_mode() {
        return this.move_mode;
    }

    public void setX(float x2) {
        this.x = x2;
    }

    public float getX() {
        return this.x;
    }

    public void setY(float y2) {
        this.y = y2;
    }

    public float getY() {
        return this.y;
    }

    public void setW(float w2) {
        this.w = w2;
    }

    public float getW() {
        return this.w;
    }

    public void setH(float h2) {
        this.h = h2;
    }

    public float getH() {
        return this.h;
    }

    public void setXoff(float xoff2) {
        this.xoff = xoff2;
    }

    public float getXoff() {
        return this.xoff;
    }

    public void setYoff(float yoff2) {
        this.yoff = yoff2;
    }

    public float getYoff() {
        return this.yoff;
    }

    public void setXspeed(float xspeed2) {
        this.xspeed = xspeed2;
    }

    public float getXspeed() {
        return this.xspeed;
    }

    public void setYspeed(float yspeed2) {
        this.yspeed = yspeed2;
    }

    public float getYspeed() {
        return this.yspeed;
    }

    public void setXhit(float xhit2) {
        this.xhit = xhit2;
    }

    public float getXhit() {
        return this.xhit;
    }

    public void setYhit(float yhit2) {
        this.yhit = yhit2;
    }

    public float getYhit() {
        return this.yhit;
    }

    public void setScene(int scene2) {
        this.scene = scene2;
    }

    public int getScene() {
        return this.scene;
    }
}
