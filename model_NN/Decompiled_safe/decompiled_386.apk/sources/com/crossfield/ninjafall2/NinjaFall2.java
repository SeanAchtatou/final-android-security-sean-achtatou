package com.crossfield.ninjafall2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class NinjaFall2 extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startActivity(new Intent(this, TitleActivity.class));
    }

    public void onResume() {
        super.onResume();
    }

    public void onPause() {
        super.onPause();
        finish();
    }
}
