package com.crossfield.ninjafall2;

import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import com.android.vending.billing.IMarketBillingService;
import com.crossfield.ninjafall2.android.utility.BillingConsts;
import com.crossfield.ninjafall2.android.utility.Security;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

public class BillingRequestService extends Service implements ServiceConnection {
    private static final String TAG = "BillingRequestService";
    /* access modifiers changed from: private */
    public static IMarketBillingService iMarketService;
    /* access modifiers changed from: private */
    public static LinkedList<BillingRequestGeneric> mPendingRequests = new LinkedList<>();
    /* access modifiers changed from: private */
    public static HashMap<Long, BillingRequestGeneric> mSentRequests = new HashMap<>();

    public void setContext(Context context) {
        attachBaseContext(context);
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onStart(Intent intent, int startId) {
        handleCommand(intent, startId);
    }

    public void handleCommand(Intent intent, int startId) {
        String action = intent.getAction();
        Log.i(TAG, "handleCommand() action: " + action);
        if (BillingConsts.ACTION_CONFIRM_NOTIFICATION.equals(action)) {
            confirmNotifications(startId, intent.getStringArrayExtra(BillingConsts.NOTIFICATION_ID));
        } else if (BillingConsts.ACTION_GET_PURCHASE_INFORMATION.equals(action)) {
            getPurchaseInformation(startId, new String[]{intent.getStringExtra(BillingConsts.NOTIFICATION_ID)});
        } else if (BillingConsts.ACTION_PURCHASE_STATE_CHANGED.equals(action)) {
            purchaseStateChanged(startId, intent.getStringExtra(BillingConsts.INAPP_SIGNED_DATA), intent.getStringExtra(BillingConsts.INAPP_SIGNATURE));
        } else if (BillingConsts.ACTION_RESPONSE_CODE.equals(action)) {
            checkResponseCode(intent.getLongExtra(BillingConsts.INAPP_REQUEST_ID, -1), BillingConsts.ResponseCode.valueOf(intent.getIntExtra(BillingConsts.INAPP_RESPONSE_CODE, BillingConsts.ResponseCode.RESULT_ERROR.ordinal())));
        }
    }

    /* access modifiers changed from: private */
    public boolean bindToMarketBillingService() {
        try {
            Log.i(TAG, "binding to Market billing service");
            if (bindService(new Intent(BillingConsts.MARKET_BILLING_SERVICE_ACTION), this, 1)) {
                return true;
            }
            Log.e(TAG, "Could not bind to service.");
            return false;
        } catch (SecurityException e) {
            Log.e(TAG, "Security exception: " + e);
        }
    }

    public boolean checkBillingSupported() {
        return new CheckBillingSupported().runRequest();
    }

    public boolean doRequestPurchase(String productId, String developerPayload) {
        return new RequestPurchase(productId, developerPayload).runRequest();
    }

    public boolean doRestoreTransactions() {
        return new RestoreTransactions().runRequest();
    }

    private boolean confirmNotifications(int startId, String[] notifyIds) {
        return new ConfirmNotifications(startId, notifyIds).runRequest();
    }

    private boolean getPurchaseInformation(int startId, String[] notifyIds) {
        return new GetPurchaseInformation(startId, notifyIds).runRequest();
    }

    private void purchaseStateChanged(int startId, String signedData, String signature) {
        ArrayList<Security.VerifiedPurchase> purchases = Security.verifyPurchase(signedData, signature);
        if (purchases != null) {
            ArrayList<String> notifyList = new ArrayList<>();
            Iterator<Security.VerifiedPurchase> it = purchases.iterator();
            while (it.hasNext()) {
                Security.VerifiedPurchase vp = it.next();
                if (vp.notificationId != null) {
                    notifyList.add(vp.notificationId);
                }
                ResponseHandler.purchaseResponse(this, vp.purchaseState, vp.productId, vp.orderId, vp.purchaseTime, vp.developerPayload);
            }
            if (!notifyList.isEmpty()) {
                confirmNotifications(startId, (String[]) notifyList.toArray(new String[notifyList.size()]));
            }
        }
    }

    private void checkResponseCode(long requestId, BillingConsts.ResponseCode responseCode) {
        BillingRequestGeneric request = mSentRequests.get(Long.valueOf(requestId));
        if (request != null) {
            Log.d(TAG, String.valueOf(request.getClass().getSimpleName()) + ": " + responseCode);
            request.responseCodeReceived(responseCode);
        }
        mSentRequests.remove(Long.valueOf(requestId));
    }

    private void runPendingRequests() {
        int maxStartId = -1;
        while (true) {
            BillingRequestGeneric request = mPendingRequests.peek();
            if (request == null) {
                if (maxStartId >= 0) {
                    Log.i(TAG, "stopping service, startId: " + maxStartId);
                    stopSelf(maxStartId);
                    return;
                }
                return;
            } else if (request.runIfConnected()) {
                mPendingRequests.remove();
                if (maxStartId < request.getStartId()) {
                    maxStartId = request.getStartId();
                }
            } else {
                bindToMarketBillingService();
                return;
            }
        }
    }

    public void onServiceConnected(ComponentName name, IBinder service) {
        Log.d(TAG, "Billing service connected");
        iMarketService = IMarketBillingService.Stub.asInterface(service);
        runPendingRequests();
    }

    public void onServiceDisconnected(ComponentName name) {
        Log.w(TAG, "Billing service disconnected");
        iMarketService = null;
    }

    public void unbind() {
        try {
            unbindService(this);
        } catch (IllegalArgumentException e) {
        }
    }

    public abstract class BillingRequestGeneric {
        protected long mRequestId;
        private final int mStartId;

        /* access modifiers changed from: protected */
        public abstract long run(String str) throws RemoteException;

        public BillingRequestGeneric(int startId) {
            this.mStartId = startId;
        }

        public int getStartId() {
            return this.mStartId;
        }

        public boolean runRequest() {
            if (runIfConnected()) {
                return true;
            }
            if (!BillingRequestService.this.bindToMarketBillingService()) {
                return false;
            }
            BillingRequestService.mPendingRequests.add(this);
            return true;
        }

        public boolean runIfConnected() {
            Log.d(BillingRequestService.TAG, getClass().getSimpleName());
            if (BillingRequestService.iMarketService != null) {
                try {
                    this.mRequestId = run(null);
                    Log.d(BillingRequestService.TAG, "request id: " + this.mRequestId);
                    if (this.mRequestId >= 0) {
                        BillingRequestService.mSentRequests.put(Long.valueOf(this.mRequestId), this);
                    }
                    return true;
                } catch (RemoteException e) {
                    onRemoteException(e);
                }
            }
            return false;
        }

        /* access modifiers changed from: protected */
        public void onRemoteException(RemoteException e) {
            Log.w(BillingRequestService.TAG, "remote billing service crashed");
            BillingRequestService.iMarketService = null;
        }

        /* access modifiers changed from: protected */
        public void responseCodeReceived(BillingConsts.ResponseCode responseCode) {
        }

        /* access modifiers changed from: protected */
        public Bundle makeRequestBundle(String method) {
            Bundle request = new Bundle();
            request.putString(BillingConsts.BILLING_REQUEST_METHOD, method);
            request.putInt(BillingConsts.BILLING_REQUEST_API_VERSION, 1);
            request.putString(BillingConsts.BILLING_REQUEST_PACKAGE_NAME, BillingRequestService.this.getPackageName());
            return request;
        }

        /* access modifiers changed from: protected */
        public void logResponseCode(String method, Bundle response) {
            Log.e(BillingRequestService.TAG, String.valueOf(method) + " received " + BillingConsts.ResponseCode.valueOf(response.getInt(BillingConsts.BILLING_RESPONSE_RESPONSE_CODE)).toString());
        }
    }

    class CheckBillingSupported extends BillingRequestGeneric {
        public CheckBillingSupported() {
            super(-1);
        }

        /* access modifiers changed from: protected */
        public long run(String securityCheckString) throws RemoteException {
            int responseCode = BillingRequestService.iMarketService.sendBillingRequest(makeRequestBundle("CHECK_BILLING_SUPPORTED")).getInt(BillingConsts.BILLING_RESPONSE_RESPONSE_CODE);
            Log.i(BillingRequestService.TAG, "CheckBillingSupported response code: " + BillingConsts.ResponseCode.valueOf(responseCode));
            ResponseHandler.checkBillingSupportedResponse(responseCode == BillingConsts.ResponseCode.RESULT_OK.ordinal());
            return BillingConsts.BILLING_RESPONSE_INVALID_REQUEST_ID;
        }
    }

    class RequestPurchase extends BillingRequestGeneric {
        public final String mDeveloperPayload;
        public final String mProductId;

        public RequestPurchase(BillingRequestService billingRequestService, String itemId) {
            this(itemId, null);
        }

        public RequestPurchase(String itemId, String developerPayload) {
            super(-1);
            this.mProductId = itemId;
            this.mDeveloperPayload = developerPayload;
        }

        /* access modifiers changed from: protected */
        public long run(String securityCheckString) throws RemoteException {
            Bundle request = makeRequestBundle("REQUEST_PURCHASE");
            request.putString(BillingConsts.BILLING_REQUEST_ITEM_ID, this.mProductId);
            if (this.mDeveloperPayload != null) {
                request.putString(BillingConsts.BILLING_REQUEST_DEVELOPER_PAYLOAD, this.mDeveloperPayload);
            }
            Bundle response = BillingRequestService.iMarketService.sendBillingRequest(request);
            PendingIntent pendingIntent = (PendingIntent) response.getParcelable(BillingConsts.BILLING_RESPONSE_PURCHASE_INTENT);
            if (pendingIntent == null) {
                Log.e(BillingRequestService.TAG, "Error with requestPurchase");
                return BillingConsts.BILLING_RESPONSE_INVALID_REQUEST_ID;
            }
            ResponseHandler.buyPageIntentResponse(pendingIntent, new Intent());
            return response.getLong(BillingConsts.BILLING_RESPONSE_REQUEST_ID, BillingConsts.BILLING_RESPONSE_INVALID_REQUEST_ID);
        }

        /* access modifiers changed from: protected */
        public void responseCodeReceived(BillingConsts.ResponseCode responseCode) {
            ResponseHandler.responseCodeReceived(BillingRequestService.this, this, responseCode);
        }
    }

    class ConfirmNotifications extends BillingRequestGeneric {
        final String[] mNotifyIds;

        public ConfirmNotifications(int startId, String[] notifyIds) {
            super(startId);
            this.mNotifyIds = notifyIds;
        }

        /* access modifiers changed from: protected */
        public long run(String securityCheckString) throws RemoteException {
            Bundle request = makeRequestBundle("CONFIRM_NOTIFICATIONS");
            request.putStringArray(BillingConsts.BILLING_REQUEST_NOTIFY_IDS, this.mNotifyIds);
            Bundle response = BillingRequestService.iMarketService.sendBillingRequest(request);
            logResponseCode("confirmNotifications", response);
            return response.getLong(BillingConsts.BILLING_RESPONSE_REQUEST_ID, BillingConsts.BILLING_RESPONSE_INVALID_REQUEST_ID);
        }
    }

    class GetPurchaseInformation extends BillingRequestGeneric {
        long mNonce;
        final String[] mNotifyIds;

        public GetPurchaseInformation(int startId, String[] notifyIds) {
            super(startId);
            this.mNotifyIds = notifyIds;
        }

        /* access modifiers changed from: protected */
        public long run(String securityCheckString) throws RemoteException {
            this.mNonce = Security.generateNonce();
            Bundle request = makeRequestBundle("GET_PURCHASE_INFORMATION");
            request.putLong(BillingConsts.BILLING_REQUEST_NONCE, this.mNonce);
            request.putStringArray(BillingConsts.BILLING_REQUEST_NOTIFY_IDS, this.mNotifyIds);
            Bundle response = BillingRequestService.iMarketService.sendBillingRequest(request);
            logResponseCode("getPurchaseInformation", response);
            return response.getLong(BillingConsts.BILLING_RESPONSE_REQUEST_ID, BillingConsts.BILLING_RESPONSE_INVALID_REQUEST_ID);
        }

        /* access modifiers changed from: protected */
        public void onRemoteException(RemoteException e) {
            super.onRemoteException(e);
            Security.removeNonce(this.mNonce);
        }
    }

    class RestoreTransactions extends BillingRequestGeneric {
        long mNonce;

        public RestoreTransactions() {
            super(-1);
        }

        /* access modifiers changed from: protected */
        public long run(String securityCheckString) throws RemoteException {
            this.mNonce = Security.generateNonce();
            Bundle request = makeRequestBundle("RESTORE_TRANSACTIONS");
            request.putLong(BillingConsts.BILLING_REQUEST_NONCE, this.mNonce);
            Bundle response = BillingRequestService.iMarketService.sendBillingRequest(request);
            logResponseCode("restoreTransactions", response);
            return response.getLong(BillingConsts.BILLING_RESPONSE_REQUEST_ID, BillingConsts.BILLING_RESPONSE_INVALID_REQUEST_ID);
        }

        /* access modifiers changed from: protected */
        public void onRemoteException(RemoteException e) {
            super.onRemoteException(e);
            Security.removeNonce(this.mNonce);
        }

        /* access modifiers changed from: protected */
        public void responseCodeReceived(BillingConsts.ResponseCode responseCode) {
            ResponseHandler.responseCodeReceived(BillingRequestService.this, this, responseCode);
        }
    }
}
