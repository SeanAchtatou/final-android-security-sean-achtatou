package com.crossfield.ninjafall2;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.TabActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AbsoluteLayout;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;
import com.crossfield.ninjafall2.BillingRequestService;
import com.crossfield.ninjafall2.android.common.database.model.UserPointDto;
import com.crossfield.ninjafall2.android.utility.BillingConsts;
import com.crossfield.ninjafall2.android.utility.Constants;
import com.crossfield.ninjafall2.android.utility.RestWebServiceClient;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.google.gson.Gson;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

public class RankingActivity extends TabActivity implements TabHost.OnTabChangeListener, Runnable {
    public static final int ADLANTIS_HEIGHT = 100;
    public static final int ADLANTIS_WIDTH = 480;
    public static final int ADLANTIS_X = 0;
    public static final int ADLANTIS_Y = 70;
    public static final int BACK_HEIGHT = 85;
    public static final int BACK_WIDTH = 240;
    public static final int BACK_X = 0;
    public static final int BACK_Y = 769;
    public static final int BUY_HEIGHT = 85;
    public static final int BUY_WIDTH = 240;
    public static final int BUY_X = 120;
    public static final int BUY_Y = 769;
    public static final int LIST_COUNTRY_WIDTH = 48;
    public static final int LIST_DATE_WIDTH = 288;
    public static final int LIST_NAME_WIDTH = 192;
    public static final int LIST_NUMBER_WIDTH = 96;
    public static final int LIST_TIME_WIDTH = 144;
    public static final int REDIO_INTERVAL_HEIGHT = 70;
    public static final int REDIO_INTERVAL_WIDTH = 480;
    public static final int REDIO_INTERVAL_X = 0;
    public static final int REDIO_INTERVAL_Y = 0;
    public static final int TAB_RANKING_HEIGHT = 599;
    public static final int TAB_RANKING_WIDTH = 480;
    public static final int TAB_RANKING_X = 0;
    public static final int TAB_RANKING_Y = 170;
    private static final String TAG = "RankingActivity (Buy 3 Jump) ";
    public static final int WRAP_CONTENT = -2;
    public static final float XPERIA_HEIGHT = 854.0f;
    public static final float XPERIA_WIDTH = 480.0f;
    private ProgressDialog Ranking;
    private int adlantis_height = 100;
    private int adlantis_width = 480;
    private int adlantis_x = 0;
    private int adlantis_y = 70;
    private int back_height = 85;
    private int back_width = 240;
    private int back_x = 0;
    private int back_y = 769;
    private int buy_height = 85;
    private int buy_width = 240;
    private int buy_x = BUY_X;
    private int buy_y = 769;
    /* access modifiers changed from: private */
    public String country = "";
    private ProgressDialog dialog;
    public int disp_height;
    public int disp_width;
    private DoSomethingThread doSomethingThread;
    private final Handler handler = new Handler();
    /* access modifiers changed from: private */
    public int list_country_width = 48;
    /* access modifiers changed from: private */
    public int list_date_width = LIST_DATE_WIDTH;
    /* access modifiers changed from: private */
    public int list_name_width = LIST_NAME_WIDTH;
    /* access modifiers changed from: private */
    public int list_number_width = 96;
    /* access modifiers changed from: private */
    public int list_time_width = LIST_TIME_WIDTH;
    private BillingRequestService mBillingService;
    private Handler mHandler;
    private PurchaseObserverImpl mPurchaseObserver;
    private int radioGroup_interval_height = 70;
    private int radioGroup_interval_width = 480;
    private int radioGroup_interval_x = 0;
    private int radioGroup_interval_y = 0;
    public float ratio_height;
    public float ratio_width;
    private int tabGroup_ranking_height = TAB_RANKING_HEIGHT;
    private int tabGroup_ranking_width = 480;
    private int tabGroup_ranking_x = 0;
    private int tabGroup_ranking_y = 170;
    GoogleAnalyticsTracker tracker;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.widget.FrameLayout, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().addFlags(1024);
        getDisplaySize();
        getDisplayRatio();
        setContentView(R.layout.ranking);
        this.radioGroup_interval_x = 0;
        this.radioGroup_interval_x = (int) (((float) this.radioGroup_interval_x) * this.ratio_width);
        this.radioGroup_interval_y = 0;
        this.radioGroup_interval_y = (int) (((float) this.radioGroup_interval_y) * this.ratio_height);
        this.radioGroup_interval_width = 480;
        this.radioGroup_interval_width = (int) (((float) this.radioGroup_interval_width) * this.ratio_width);
        this.radioGroup_interval_height = 70;
        this.radioGroup_interval_height = (int) (((float) this.radioGroup_interval_height) * this.ratio_height);
        new RadioGroup(this);
        RadioGroup radioGroup_interval = (RadioGroup) findViewById(R.id.radioGroup_interval);
        radioGroup_interval.setLayoutParams(new AbsoluteLayout.LayoutParams(this.radioGroup_interval_width, this.radioGroup_interval_height, this.radioGroup_interval_x, this.radioGroup_interval_y));
        radioGroup_interval.check(R.id.radioButton_daily);
        new RadioButton(this);
        ((RadioButton) findViewById(R.id.radioButton_daily)).setLayoutParams(new RadioGroup.LayoutParams(this.radioGroup_interval_width / 4, this.radioGroup_interval_height));
        RadioButton radioButton = (RadioButton) findViewById(R.id.radioButton_weekly);
        ((RadioButton) findViewById(R.id.radioButton_weekly)).setLayoutParams(new RadioGroup.LayoutParams(this.radioGroup_interval_width / 4, this.radioGroup_interval_height));
        RadioButton radioButton2 = (RadioButton) findViewById(R.id.radioButton_monthly);
        ((RadioButton) findViewById(R.id.radioButton_monthly)).setLayoutParams(new RadioGroup.LayoutParams(this.radioGroup_interval_width / 4, this.radioGroup_interval_height));
        RadioButton radioButton3 = (RadioButton) findViewById(R.id.radioButton_monthly);
        ((RadioButton) findViewById(R.id.radioButton_monthly)).setLayoutParams(new RadioGroup.LayoutParams(this.radioGroup_interval_width / 4, this.radioGroup_interval_height));
        this.back_x = 0;
        this.back_x = (int) (((float) this.back_x) * this.ratio_width);
        this.back_y = 769;
        this.back_y = (int) (((float) this.back_y) * this.ratio_height);
        this.back_width = 240;
        this.back_width = (int) (((float) this.back_width) * this.ratio_width);
        this.back_height = 85;
        this.back_height = (int) (((float) this.back_height) * this.ratio_height);
        new Button(this);
        ((Button) findViewById(R.id.button_back)).setLayoutParams(new LinearLayout.LayoutParams(this.back_width, this.back_height));
        this.buy_x = BUY_X;
        this.buy_x = (int) (((float) this.buy_x) * this.ratio_width);
        this.buy_y = 769;
        this.buy_y = (int) (((float) this.buy_y) * this.ratio_height);
        this.buy_width = 240;
        this.buy_width = (int) (((float) this.buy_width) * this.ratio_width);
        this.buy_height = 85;
        this.buy_height = (int) (((float) this.buy_height) * this.ratio_height);
        new Button(this);
        ((Button) findViewById(R.id.button_buy)).setLayoutParams(new LinearLayout.LayoutParams(this.buy_width, this.buy_height));
        new LinearLayout(this);
        ((LinearLayout) findViewById(R.id.linearLayout_back)).setLayoutParams(new AbsoluteLayout.LayoutParams(this.back_width + this.buy_width, this.back_height, this.back_x, this.back_y));
        this.mBillingService = new BillingRequestService();
        this.mBillingService.setContext(this);
        this.mHandler = new Handler();
        this.mPurchaseObserver = new PurchaseObserverImpl(this.mHandler);
        ResponseHandler.register(this.mPurchaseObserver);
        this.tabGroup_ranking_x = 0;
        this.tabGroup_ranking_x = (int) (((float) this.tabGroup_ranking_x) * this.ratio_width);
        this.tabGroup_ranking_y = 170;
        this.tabGroup_ranking_y = (int) (((float) this.tabGroup_ranking_y) * this.ratio_height);
        this.tabGroup_ranking_width = 480;
        this.tabGroup_ranking_width = (int) (((float) this.tabGroup_ranking_width) * this.ratio_width);
        this.tabGroup_ranking_height = TAB_RANKING_HEIGHT;
        this.tabGroup_ranking_height = (int) (((float) this.tabGroup_ranking_height) * this.ratio_height);
        new LinearLayout(this);
        ((LinearLayout) findViewById(R.id.tabGroup_ranking)).setLayoutParams(new AbsoluteLayout.LayoutParams(this.tabGroup_ranking_width, this.tabGroup_ranking_height, this.tabGroup_ranking_x, this.tabGroup_ranking_y));
        TabHost tabs = getTabHost();
        tabs.setOnTabChangedListener(this);
        LayoutInflater.from(this).inflate((int) R.layout.ranking_tab, (ViewGroup) tabs.getTabContentView(), true);
        TabHost.TabSpec tab_private = tabs.newTabSpec("Private");
        tab_private.setIndicator("Private");
        tab_private.setContent(R.id.tab_private);
        tabs.addTab(tab_private);
        TabHost.TabSpec tab_domestic = tabs.newTabSpec("Domestic");
        tab_domestic.setIndicator("Domestic");
        tab_domestic.setContent(R.id.tab_domestic);
        tabs.addTab(tab_domestic);
        TabHost.TabSpec tab_world = tabs.newTabSpec("World");
        tab_world.setIndicator("World");
        tab_world.setContent(R.id.tab_world);
        tabs.addTab(tab_world);
        tabs.setCurrentTab(0);
        this.adlantis_x = 0;
        this.adlantis_x = (int) (((float) this.adlantis_x) * this.ratio_width);
        this.adlantis_y = 70;
        this.adlantis_y = (int) (((float) this.adlantis_y) * this.ratio_height);
        this.adlantis_width = 480;
        this.adlantis_width = (int) (((float) this.adlantis_width) * this.ratio_width);
        this.adlantis_height = 100;
        this.adlantis_height = (int) (((float) this.adlantis_height) * this.ratio_height);
        new LinearLayout(this);
        ((LinearLayout) findViewById(R.id.linearLayout_Adlantis)).setLayoutParams(new AbsoluteLayout.LayoutParams(this.adlantis_width, this.adlantis_height, this.adlantis_x, this.adlantis_y));
        ArrayList arrayList = new ArrayList();
        ListView list_area = (ListView) findViewById(R.id.list_private);
        SharedPreferences pref = getSharedPreferences(TitleActivity.TITLE_PREFERENCE, 3);
        int num = 0;
        int i = 0;
        while (i < 100 && ((int) pref.getFloat(TitleActivity.SCORE + i, -1.0f)) != -1) {
            num++;
            arrayList.add(setRanking(String.valueOf(num) + "   ", "   " + String.valueOf((int) pref.getFloat(TitleActivity.SCORE + i, -1.0f)) + "m", "", "", getCountry()));
            i++;
        }
        ListAdapter listAdapter = new ListAdapter(getApplicationContext(), arrayList);
        if (list_area != null) {
            list_area.setAdapter((android.widget.ListAdapter) listAdapter);
        }
        ((TextView) findViewById(R.id.tab_sub)).setText("");
        this.tracker = GoogleAnalyticsTracker.getInstance();
        this.tracker.start("UA-23476120-6", this);
        this.tracker.trackPageView("Ranking");
        this.tracker.dispatch();
    }

    class ListAdapter extends ArrayAdapter<RankingBean> {
        private TextView country;
        private TextView date;
        private LayoutInflater mInflater;
        private TextView name;
        private TextView number;
        private TextView time;

        public ListAdapter(Context context, List<RankingBean> objects) {
            super(context, 0, objects);
            this.mInflater = (LayoutInflater) context.getSystemService("layout_inflater");
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            RankingActivity.this.list_number_width = 96;
            RankingActivity rankingActivity = RankingActivity.this;
            rankingActivity.list_number_width = (int) (((float) rankingActivity.list_number_width) * RankingActivity.this.ratio_width);
            RankingActivity.this.list_time_width = RankingActivity.LIST_TIME_WIDTH;
            RankingActivity rankingActivity2 = RankingActivity.this;
            rankingActivity2.list_time_width = (int) (((float) rankingActivity2.list_time_width) * RankingActivity.this.ratio_width);
            RankingActivity.this.list_name_width = RankingActivity.LIST_NAME_WIDTH;
            RankingActivity rankingActivity3 = RankingActivity.this;
            rankingActivity3.list_name_width = (int) (((float) rankingActivity3.list_name_width) * RankingActivity.this.ratio_width);
            RankingActivity.this.list_date_width = RankingActivity.LIST_DATE_WIDTH;
            RankingActivity rankingActivity4 = RankingActivity.this;
            rankingActivity4.list_date_width = (int) (((float) rankingActivity4.list_date_width) * RankingActivity.this.ratio_width);
            RankingActivity.this.list_country_width = 48;
            RankingActivity rankingActivity5 = RankingActivity.this;
            rankingActivity5.list_country_width = (int) (((float) rankingActivity5.list_country_width) * RankingActivity.this.ratio_width);
            if (convertView == null) {
                convertView = this.mInflater.inflate((int) R.layout.list, (ViewGroup) null);
            }
            RankingBean ranking = (RankingBean) getItem(position);
            if (ranking != null) {
                this.number = (TextView) convertView.findViewById(R.id.textView_number);
                this.number.setText(ranking.getNumber());
                this.number.setLayoutParams(new LinearLayout.LayoutParams(RankingActivity.this.list_number_width, -2));
                this.time = (TextView) convertView.findViewById(R.id.textView_time);
                this.time.setText(ranking.getTime());
                this.time.setLayoutParams(new LinearLayout.LayoutParams(RankingActivity.this.list_time_width, -2));
                this.name = (TextView) convertView.findViewById(R.id.textView_name);
                this.name.setText(ranking.getName());
                this.name.setLayoutParams(new LinearLayout.LayoutParams(RankingActivity.this.list_name_width, -2));
                this.date = (TextView) convertView.findViewById(R.id.textView_date);
                this.date.setText(ranking.getDate());
                this.date.setLayoutParams(new LinearLayout.LayoutParams(RankingActivity.this.list_date_width, -2));
                this.country = (TextView) convertView.findViewById(R.id.textView_country);
                this.country.setText(ranking.getCountry());
                this.country.setLayoutParams(new LinearLayout.LayoutParams(RankingActivity.this.list_number_width, -2));
            }
            return convertView;
        }
    }

    public void getDisplaySize() {
        Display disp = ((WindowManager) getSystemService("window")).getDefaultDisplay();
        this.disp_width = disp.getWidth();
        this.disp_height = disp.getHeight();
    }

    public void getDisplayRatio() {
        getDisplaySize();
        this.ratio_width = ((float) this.disp_width) / 480.0f;
        this.ratio_height = ((float) this.disp_height) / 854.0f;
    }

    public RankingBean setRanking(String number, String time, String name, String date, String country2) {
        RankingBean ranking = new RankingBean();
        ranking.setNumber(number);
        ranking.setTime(time);
        ranking.setName(name);
        SimpleDateFormat formatReceive = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            ranking.setDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(formatReceive.parse(date)));
        } catch (ParseException e) {
        }
        ranking.setCountry(country2);
        return ranking;
    }

    private void populateRankingData(String category) {
        TelephonyManager tel = (TelephonyManager) getSystemService("phone");
        String name = getSharedPreferences(TitleActivity.TITLE_PREFERENCE, 3).getString(TitleActivity.NAME, "");
        if (name.length() != 0) {
            RestWebServiceClient restWebServiceClient = new RestWebServiceClient(Constants.URL_GET_RANKING);
            HashMap hashMap = new HashMap();
            hashMap.put("category", category);
            hashMap.put("country_code", getCountry());
            hashMap.put("user_name", name);
            hashMap.put("device_id", tel.getDeviceId());
            try {
                JSONObject object = (JSONObject) new JSONTokener(restWebServiceClient.webGet("", hashMap)).nextValue();
                int userRank = object.getInt("self_rank");
                JSONArray arrayJson = object.getJSONArray("list_rank");
                ArrayList<UserPointDto> arrayList = new ArrayList<>();
                for (int i = 0; i < arrayJson.length(); i++) {
                    arrayList.add((UserPointDto) new Gson().fromJson(arrayJson.getJSONObject(i).toString(), UserPointDto.class));
                }
                TextView tub_sub = (TextView) findViewById(R.id.tab_sub);
                if (userRank != -1) {
                    tub_sub.setText(String.valueOf(userRank) + "th");
                    if (userRank == 1) {
                        tub_sub.setText(String.valueOf(userRank) + "st");
                    }
                    if (userRank == 2) {
                        tub_sub.setText(String.valueOf(userRank) + "nd");
                    }
                    if (userRank == 3) {
                        tub_sub.setText(String.valueOf(userRank) + "rd");
                    }
                }
                if (userRank == -1) {
                    tub_sub.setText("");
                }
                List<RankingBean> ranking_area = new ArrayList<>();
                ListView list_area = null;
                switch (getTabHost().getCurrentTab()) {
                    case 1:
                        list_area = (ListView) findViewById(R.id.list_domestic);
                        break;
                    case 2:
                        list_area = (ListView) findViewById(R.id.list_world);
                        break;
                }
                int num = 0;
                for (UserPointDto dto : arrayList) {
                    num++;
                    ranking_area.add(setRanking(String.valueOf(num) + "   ", "   " + String.valueOf((int) dto.getUserPoint()) + "m", dto.getUserName(), dto.getInsertTime(), dto.getCountryCode()));
                }
                ListAdapter listAdapter = new ListAdapter(getApplicationContext(), ranking_area);
                if (list_area != null) {
                    list_area.setAdapter((android.widget.ListAdapter) listAdapter);
                }
            } catch (Exception e) {
                showDialog(this, "Failure", "Connection Failure!");
            }
        }
    }

    private static void showDialog(Context context, String title, String text) {
        AlertDialog.Builder ad = new AlertDialog.Builder(context);
        ad.setTitle(title);
        ad.setMessage(text);
        ad.setPositiveButton("OK", (DialogInterface.OnClickListener) null);
        ad.show();
    }

    /* access modifiers changed from: private */
    public String getCountry() {
        this.country = getResources().getConfiguration().locale.getCountry();
        switch (getTabHost().getCurrentTab()) {
            case 0:
                return getResources().getConfiguration().locale.getCountry();
            case 1:
                return getResources().getConfiguration().locale.getCountry();
            case 2:
                return Constants.RANKING_COUNTRY_WORLD;
            default:
                return this.country;
        }
    }

    public void radioDaily(View view) {
        switch (getTabHost().getCurrentTab()) {
            case 0:
            default:
                return;
            case 1:
            case 2:
                populateRankingData(Constants.RANKING_CATEGORY_TODAY);
                return;
        }
    }

    public void radioWeekly(View view) {
        switch (getTabHost().getCurrentTab()) {
            case 0:
            default:
                return;
            case 1:
            case 2:
                populateRankingData(Constants.RANKING_CATEGORY_WEEKLY);
                return;
        }
    }

    public void radioMonthly(View view) {
        switch (getTabHost().getCurrentTab()) {
            case 0:
            default:
                return;
            case 1:
            case 2:
                populateRankingData(Constants.RANKING_CATEGORY_MONTHLY);
                return;
        }
    }

    public void radioAll(View view) {
        switch (getTabHost().getCurrentTab()) {
            case 0:
            default:
                return;
            case 1:
            case 2:
                populateRankingData(Constants.RANKING_CATEGORY_ALL);
                return;
        }
    }

    public void onTabChanged(String s) {
        new RadioGroup(this);
        String interval = "";
        switch (((RadioButton) findViewById(((RadioGroup) findViewById(R.id.radioGroup_interval)).getCheckedRadioButtonId())).getId()) {
            case R.id.radioButton_daily:
                interval = Constants.RANKING_CATEGORY_TODAY;
                break;
            case R.id.radioButton_weekly:
                interval = Constants.RANKING_CATEGORY_WEEKLY;
                break;
            case R.id.radioButton_monthly:
                interval = Constants.RANKING_CATEGORY_MONTHLY;
                break;
            case R.id.radioButton_all:
                interval = Constants.RANKING_CATEGORY_ALL;
                break;
        }
        switch (getTabHost().getCurrentTab()) {
            case 0:
                ((TextView) findViewById(R.id.tab_sub)).setText("");
                return;
            case 1:
                populateRankingData(interval);
                return;
            case 2:
                populateRankingData(interval);
                return;
            default:
                return;
        }
    }

    public void buttonBack(View view) {
        this.tracker.trackEvent("Clicks", "Back", "Back", 1);
        this.tracker.dispatch();
        finish();
    }

    public void buttonBuy(View v) {
        this.tracker.trackEvent("Clicks", "Back", "Back", 1);
        this.tracker.dispatch();
        this.country = getResources().getConfiguration().locale.getCountry();
        if (this.country.equalsIgnoreCase("JP")) {
            buyYesNoDialog("Ninja Jump", "�Q�[������1�x���������ł���悤�ɂȂ�܂��B�w��܂����H\n(1�x�w���Ό�ʂ͉i���I�ɑ����܂��B)\n(�w��ꍇ�A���f�����܂�1���قǂ�����܂��B)");
        } else {
            buyYesNoDialog("Ninja Jump", "Buy a \"Life\" to resume game from the same spot of dying. \nSo Ninja can fall more distance continuously! Buy? \n(It will take 1 minute to process buying request)");
        }
    }

    public void connectBuy() {
        this.dialog = new ProgressDialog(this);
        this.dialog.setIndeterminate(true);
        this.dialog.setMessage("connecting");
        this.dialog.show();
        this.doSomethingThread = new DoSomethingThread(this.handler, this);
        this.doSomethingThread.start();
    }

    public void run() {
        if (!this.mBillingService.doRequestPurchase(Constants.PRODUCT_ID, null)) {
            Toast.makeText(getBaseContext(), "Billing Not Supported.", 1).show();
        }
        this.dialog.dismiss();
    }

    private void buyYesNoDialog(String title, String text) {
        new AlertDialog.Builder(this).setIcon((int) R.drawable.icon).setTitle(title).setMessage(text).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                RankingActivity.this.connectBuy();
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        }).show();
    }

    /* access modifiers changed from: private */
    public String getDeviceId() {
        return ((TelephonyManager) getSystemService("phone")).getDeviceId();
    }

    /* access modifiers changed from: private */
    public boolean checkIfProductAlreadyPurchased(String deviceId, String productId) {
        List<String> listId = populateListProductId(deviceId);
        if (listId == null || !listId.contains(productId)) {
            return false;
        }
        return true;
    }

    private List<String> populateListProductId(String deviceId) {
        List<String> listId = new ArrayList<>();
        RestWebServiceClient restClient = new RestWebServiceClient(Constants.URL_PURCHASED_PRD_LIST);
        List<NameValuePair> nameValuePairs = new ArrayList<>();
        nameValuePairs.add(new BasicNameValuePair("device_id", deviceId));
        try {
            JSONArray arrayJson = ((JSONObject) new JSONTokener(restClient.webPost("", nameValuePairs)).nextValue()).getJSONArray("list_product_id");
            for (int i = 0; i < arrayJson.length(); i++) {
                listId.add(arrayJson.getString(i));
            }
        } catch (Exception e) {
            Log.d("Error: ", e.getMessage());
        }
        return listId;
    }

    /* access modifiers changed from: private */
    public boolean insertPurchaseInfoIntoDB(String deviceId, String productId) {
        RestWebServiceClient restClient = new RestWebServiceClient(Constants.URL_PURCHASE_INSERT);
        List<NameValuePair> nameValuePairs = new ArrayList<>();
        nameValuePairs.add(new BasicNameValuePair("device_id", deviceId));
        nameValuePairs.add(new BasicNameValuePair("product_id", productId));
        String response = restClient.webPost("", nameValuePairs);
        boolean status = true;
        try {
            status = ((JSONObject) new JSONTokener(response).nextValue()).getBoolean("status");
        } catch (Exception e) {
            Log.d("Error: ", e.getMessage());
        }
        if (response == null || !status) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: private */
    public boolean deletePurchaseInfoFromDB(String deviceId, String productId) {
        RestWebServiceClient restClient = new RestWebServiceClient(Constants.URL_PURCHASE_DELETE);
        List<NameValuePair> nameValuePairs = new ArrayList<>();
        nameValuePairs.add(new BasicNameValuePair("device_id", deviceId));
        nameValuePairs.add(new BasicNameValuePair("product_id", productId));
        String response = restClient.webPost("", nameValuePairs);
        boolean status = true;
        try {
            status = ((JSONObject) new JSONTokener(response).nextValue()).getBoolean("status");
        } catch (Exception e) {
            Log.d("Error: ", e.getMessage());
        }
        if (response == null || !status) {
            return false;
        }
        return true;
    }

    public void setRanking(ProgressDialog ranking) {
        this.Ranking = ranking;
    }

    public ProgressDialog getRanking() {
        return this.Ranking;
    }

    private class PurchaseObserverImpl extends PurchaseObserver {
        public PurchaseObserverImpl(Handler handler) {
            super(RankingActivity.this, handler);
        }

        public void onBillingSupported(boolean supported) {
            Log.i(RankingActivity.TAG, "supported: " + supported);
            if (!supported) {
                Toast.makeText(RankingActivity.this.getBaseContext(), "Billing Not Supported.", 1).show();
            }
        }

        public void onPurchaseStateChange(BillingConsts.PurchaseState purchaseState, String itemId, int quantity, long purchaseTime, String developerPayload) {
            Log.i(RankingActivity.TAG, "onPurchaseStateChange() itemId: " + itemId + " " + purchaseState);
            if (purchaseState == BillingConsts.PurchaseState.PURCHASED) {
                SharedPreferences.Editor editor = RankingActivity.this.getSharedPreferences(TitleActivity.TITLE_PREFERENCE, 3).edit();
                editor.putBoolean(TitleActivity.JUMP_FLAG, true);
                editor.commit();
                String unused = RankingActivity.this.getCountry();
                if (RankingActivity.this.country.equalsIgnoreCase("JP")) {
                    Toast.makeText(RankingActivity.this.getBaseContext(), "���C�t�|�C���g�̍w������܂����B", 1).show();
                } else {
                    Toast.makeText(RankingActivity.this.getBaseContext(), "Your purchase process is completed!", 1).show();
                }
                int i = 0;
                while (i < 1000 && !RankingActivity.this.insertPurchaseInfoIntoDB(RankingActivity.this.getDeviceId(), itemId)) {
                    if (!RankingActivity.this.checkIfProductAlreadyPurchased(RankingActivity.this.getDeviceId(), Constants.PRODUCT_ID)) {
                        try {
                            Thread.sleep(500);
                        } catch (InterruptedException e) {
                        }
                        i++;
                    } else {
                        return;
                    }
                }
            } else if (purchaseState == BillingConsts.PurchaseState.CANCELED) {
                SharedPreferences.Editor editor2 = RankingActivity.this.getSharedPreferences(TitleActivity.TITLE_PREFERENCE, 3).edit();
                editor2.putBoolean(TitleActivity.JUMP_FLAG, false);
                editor2.commit();
                int i2 = 0;
                while (i2 < 1000 && !RankingActivity.this.deletePurchaseInfoFromDB(RankingActivity.this.getDeviceId(), itemId)) {
                    if (RankingActivity.this.checkIfProductAlreadyPurchased(RankingActivity.this.getDeviceId(), Constants.PRODUCT_ID)) {
                        try {
                            Thread.sleep(500);
                        } catch (InterruptedException e2) {
                        }
                        i2++;
                    } else {
                        return;
                    }
                }
            } else if (purchaseState == BillingConsts.PurchaseState.REFUNDED) {
                SharedPreferences.Editor editor3 = RankingActivity.this.getSharedPreferences(TitleActivity.TITLE_PREFERENCE, 3).edit();
                editor3.putBoolean(TitleActivity.JUMP_FLAG, false);
                editor3.commit();
                int i3 = 0;
                while (i3 < 1000 && !RankingActivity.this.deletePurchaseInfoFromDB(RankingActivity.this.getDeviceId(), itemId)) {
                    if (RankingActivity.this.checkIfProductAlreadyPurchased(RankingActivity.this.getDeviceId(), Constants.PRODUCT_ID)) {
                        try {
                            Thread.sleep(500);
                        } catch (InterruptedException e3) {
                        }
                        i3++;
                    } else {
                        return;
                    }
                }
            }
        }

        public void onRequestPurchaseResponse(BillingRequestService.RequestPurchase request, BillingConsts.ResponseCode responseCode) {
            Log.d(RankingActivity.TAG, String.valueOf(request.mProductId) + ": " + responseCode);
            if (responseCode == BillingConsts.ResponseCode.RESULT_OK) {
                Log.i(RankingActivity.TAG, "purchase was successfully sent to server");
            } else if (responseCode == BillingConsts.ResponseCode.RESULT_USER_CANCELED) {
                Log.i(RankingActivity.TAG, "user canceled purchase");
            } else {
                Log.i(RankingActivity.TAG, "purchase failed");
            }
        }

        public void onRestoreTransactionsResponse(BillingRequestService.RestoreTransactions request, BillingConsts.ResponseCode responseCode) {
            if (responseCode == BillingConsts.ResponseCode.RESULT_OK) {
                Log.d(RankingActivity.TAG, "completed RestoreTransactions request");
            } else {
                Log.d(RankingActivity.TAG, "RestoreTransactions error: " + responseCode);
            }
        }
    }
}
