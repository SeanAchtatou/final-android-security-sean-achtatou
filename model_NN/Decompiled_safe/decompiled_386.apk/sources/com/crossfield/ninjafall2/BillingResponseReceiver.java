package com.crossfield.ninjafall2;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.crossfield.ninjafall2.android.utility.BillingConsts;

public class BillingResponseReceiver extends BroadcastReceiver {
    private static final String TAG = "BillingResponseReceiver";

    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (BillingConsts.ACTION_PURCHASE_STATE_CHANGED.equals(action)) {
            purchaseStateChanged(context, intent.getStringExtra(BillingConsts.INAPP_SIGNED_DATA), intent.getStringExtra(BillingConsts.INAPP_SIGNATURE));
        } else if (BillingConsts.ACTION_NOTIFY.equals(action)) {
            String notifyId = intent.getStringExtra(BillingConsts.NOTIFICATION_ID);
            Log.i(TAG, "notifyId: " + notifyId);
            notify(context, notifyId);
        } else if (BillingConsts.ACTION_RESPONSE_CODE.equals(action)) {
            checkResponseCode(context, intent.getLongExtra(BillingConsts.INAPP_REQUEST_ID, -1), intent.getIntExtra(BillingConsts.INAPP_RESPONSE_CODE, BillingConsts.ResponseCode.RESULT_ERROR.ordinal()));
        } else {
            Log.w(TAG, "unexpected action: " + action);
        }
    }

    private void purchaseStateChanged(Context context, String signedData, String signature) {
        Intent intent = new Intent(BillingConsts.ACTION_PURCHASE_STATE_CHANGED);
        intent.setClass(context, BillingRequestService.class);
        intent.putExtra(BillingConsts.INAPP_SIGNED_DATA, signedData);
        intent.putExtra(BillingConsts.INAPP_SIGNATURE, signature);
        context.startService(intent);
    }

    private void notify(Context context, String notifyId) {
        Intent intent = new Intent(BillingConsts.ACTION_GET_PURCHASE_INFORMATION);
        intent.setClass(context, BillingRequestService.class);
        intent.putExtra(BillingConsts.NOTIFICATION_ID, notifyId);
        context.startService(intent);
    }

    private void checkResponseCode(Context context, long requestId, int responseCodeIndex) {
        Intent intent = new Intent(BillingConsts.ACTION_RESPONSE_CODE);
        intent.setClass(context, BillingRequestService.class);
        intent.putExtra(BillingConsts.INAPP_REQUEST_ID, requestId);
        intent.putExtra(BillingConsts.INAPP_RESPONSE_CODE, responseCodeIndex);
        context.startService(intent);
    }
}
