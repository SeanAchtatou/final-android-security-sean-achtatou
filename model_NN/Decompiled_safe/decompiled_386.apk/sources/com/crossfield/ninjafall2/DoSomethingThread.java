package com.crossfield.ninjafall2;

import android.os.Handler;

public class DoSomethingThread extends Thread {
    private Handler handler;
    private final Runnable listener;

    public DoSomethingThread(Handler _handler, Runnable _listener) {
        this.handler = _handler;
        this.listener = _listener;
    }

    public void run() {
        this.handler.post(this.listener);
    }
}
