package com.crossfield.ninjafall2;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import java.util.Date;

public class MainView extends SurfaceView implements SurfaceHolder.Callback, Runnable {
    public static int BUTTON_H = 85;
    public static int BUTTON_W = 480;
    public static int BUTTON_X = 0;
    public static int BUTTON_Y = 683;
    public static final int END = 3;
    public static final int INIT_X = 240;
    public static final int INIT_Y = 256;
    public static final int PLAY = 1;
    public static final int RECORD = 2;
    public static final float SCROLL_SPEED = 15.0f;
    public static final int START = 0;
    private MainActivity activity;
    private BackGround bg;
    private int bg_h = 854;
    private int bg_w = 480;
    private int bg_x = 0;
    private int bg_y = 0;
    private Button button_float;
    private int button_float_h = 85;
    private int button_float_w = 163;
    private int button_float_x = this.button_left_w;
    private int button_float_y = 683;
    private Button button_left;
    private int button_left_h = 85;
    private int button_left_w = 158;
    private int button_left_x = 0;
    private int button_left_y = 683;
    private Button button_right;
    private int button_right_h = 85;
    private int button_right_w = 158;
    private int button_right_x = (this.button_left_w + this.button_float_w);
    private int button_right_y = 683;
    private Graphics g;
    private GroundManager ground;
    private SurfaceHolder holder;
    private Player player;
    private float player_h = 60.0f;
    private float player_w = 60.0f;
    private float player_x = 0.0f;
    private float player_y = 0.0f;
    private Point point;
    private long point_score = 0;
    private int scene;
    private float[] score;
    private Thread thread;

    public void setScene(int scene2) {
        this.scene = scene2;
    }

    public int getScene() {
        return this.scene;
    }

    public void setScore(float score2, int i) {
        this.score[i] = score2;
    }

    public float getScore(int i) {
        return this.score[i];
    }

    public MainView(Context context, SurfaceView sv) {
        super(context);
        this.holder = sv.getHolder();
        this.holder.addCallback(this);
        this.holder.setFixedSize(getWidth(), getHeight());
        setFocusable(true);
        setClickable(true);
        this.g = new Graphics(this.holder, context);
        this.activity = (MainActivity) context;
        this.score = new float[100];
        for (int i = 0; i < 100; i++) {
            this.score[i] = -1.0f;
        }
        setClickable(true);
        init();
    }

    public void init() {
        this.scene = 0;
        float dw = this.g.ratio_width;
        float dh = this.g.ratio_height;
        this.bg_x = (int) (((float) this.bg_x) * dw);
        this.bg_y = (int) (((float) this.bg_y) * dh);
        this.bg_w = (int) (((float) this.bg_w) * dw);
        this.bg_h = (int) (((float) this.bg_h) * dh);
        Graphics graphics = this.g;
        this.g.getClass();
        this.bg = new BackGround(graphics, 0, this.bg_x, this.bg_y, this.bg_w, this.bg_h);
        this.player_x = 240.0f * dw;
        this.player_y = 256.0f * dh;
        this.player_w *= dw;
        this.player_h *= dh;
        Graphics graphics2 = this.g;
        this.g.getClass();
        this.player = new Player(graphics2, 1, this.player_x, this.player_y, this.player_w, this.player_h);
        this.ground = new GroundManager(this.g);
        this.point = new Point(this.g, ((int) (20.0f * dw)) + 0, ((int) (20.0f * dh)) + 0, 30, 30);
        this.button_left_x = 0;
        this.button_left_y = (int) (((double) dh) * 683.2d);
        this.button_left_w = (int) (((double) (480.0f * dw)) * 0.33d);
        this.button_left_h = (int) (((double) dh) * 85.4d);
        MainActivity mainActivity = this.activity;
        Graphics graphics3 = this.g;
        this.g.getClass();
        this.button_left = new Button(mainActivity, graphics3, 24, this.button_left_x, this.button_left_y, this.button_left_w, this.button_left_h, (int) (20.0f * dw), (int) (30.0f * dh));
        this.button_float_x = this.button_left_w;
        this.button_float_y = (int) (((double) dh) * 683.2d);
        this.button_float_w = (int) (((double) (480.0f * dw)) * 0.34d);
        this.button_float_h = (int) (((double) dh) * 85.4d);
        MainActivity mainActivity2 = this.activity;
        Graphics graphics4 = this.g;
        this.g.getClass();
        this.button_float = new Button(mainActivity2, graphics4, 23, this.button_float_x, this.button_float_y, this.button_float_w, this.button_float_h, (int) (21.0f * dw), (int) (30.0f * dh));
        this.button_right_x = this.button_left_w + this.button_float_w;
        this.button_right_y = (int) (((double) dh) * 683.2d);
        this.button_right_w = (int) (((double) (480.0f * dw)) * 0.33d);
        this.button_right_h = (int) (((double) dh) * 85.4d);
        MainActivity mainActivity3 = this.activity;
        Graphics graphics5 = this.g;
        this.g.getClass();
        this.button_right = new Button(mainActivity3, graphics5, 25, this.button_right_x, this.button_right_y, this.button_right_w, this.button_right_h, (int) (20.0f * dw), (int) (30.0f * dh));
    }

    public void run() {
        while (this.thread != null) {
            update();
            draw();
            sceneChange();
            try {
                Thread.sleep(50);
            } catch (Exception e) {
            }
        }
    }

    public void update() {
        switch (this.scene) {
            case 0:
                this.bg.action();
                this.ground.action();
                this.scene = 1;
                return;
            case 1:
                this.bg.action();
                this.ground.action();
                this.player.action();
                Ground hitGround = this.ground.hitPlayer(this.player);
                if (hitGround != null) {
                    this.player.pointCorrect(hitGround);
                }
                this.point_score += 10;
                return;
            case 2:
            default:
                return;
        }
    }

    public void draw() {
        this.g.lock();
        this.g.drawSquare(0, 0, this.g.disp_width, this.g.disp_height, -16777216, true);
        Graphics graphics = this.g;
        this.g.getClass();
        graphics.drawImage(0, 0, 0, this.g.disp_width, this.g.disp_height);
        switch (this.scene) {
            case 0:
                this.bg.draw();
                this.ground.draw();
                this.player.draw();
                this.point.drawPoint(this.point_score);
                this.button_left.draw();
                this.button_float.draw();
                this.button_right.draw();
                break;
            case 1:
                this.bg.draw();
                this.ground.draw();
                this.player.draw();
                this.point.drawPoint(this.point_score);
                this.button_left.draw();
                this.button_float.draw();
                this.button_right.draw();
                break;
            case 2:
                this.bg.draw();
                this.ground.draw();
                this.player.draw();
                this.point.drawPoint(this.point_score);
                this.button_left.draw();
                this.button_float.draw();
                this.button_right.draw();
                break;
            case 3:
                this.bg.draw();
                this.ground.draw();
                this.player.draw();
                this.point.drawPoint(this.point_score);
                this.button_left.draw();
                this.button_float.draw();
                this.button_right.draw();
                break;
        }
        this.g.unlock();
    }

    public void sceneChange() {
        switch (this.scene) {
            case 0:
            default:
                return;
            case 1:
                if (this.player.getY() < 0.0f || this.player.getY() + this.player.getH() > 768.0f * this.g.ratio_height) {
                    this.scene = 2;
                    return;
                }
                return;
            case 2:
                float[] sub1 = new float[101];
                for (int i = 0; i < 100; i++) {
                    sub1[i + 1] = this.score[i];
                }
                sub1[0] = (float) (this.point_score / 10);
                for (int i2 = 1; i2 < 101; i2++) {
                    for (int l = 1; l < 101; l++) {
                        if (sub1[l] > sub1[l - 1] && sub1[l - 1] != -1.0f) {
                            float sub2 = sub1[l];
                            sub1[l] = sub1[l - 1];
                            sub1[l - 1] = sub2;
                        }
                    }
                }
                for (int i3 = 0; i3 < 100; i3++) {
                    this.score[i3] = sub1[i3];
                }
                SharedPreferences pref = this.activity.getSharedPreferences(TitleActivity.TITLE_PREFERENCE, 3);
                SharedPreferences.Editor editor = pref.edit();
                int best = (int) pref.getFloat("Score0", -1.0f);
                editor.putBoolean(TitleActivity.BEST_FLAG, false);
                Date date = new Date();
                long day = (long) Math.floor((double) (date.getTime() / 86400000));
                if (best < ((int) (this.point_score / 10))) {
                    editor.putInt(TitleActivity.DAILY_RECORD, (int) (this.point_score / 10));
                    editor.putLong(TitleActivity.DAILY_TIME, day);
                    editor.putBoolean(TitleActivity.BEST_FLAG, true);
                } else {
                    long j = pref.getLong(TitleActivity.DAILY_TIME, -1);
                    long yesterday = pref.getLong(TitleActivity.DAILY_TIME, -1);
                    editor.putBoolean(TitleActivity.DAILY_FLAG, false);
                    if (yesterday == -1) {
                        editor.putBoolean(TitleActivity.DAILY_FLAG, true);
                        editor.putInt(TitleActivity.DAILY_RECORD, (int) (this.point_score / 10));
                        editor.putLong(TitleActivity.DAILY_TIME, (long) Math.floor((double) (date.getTime() / 86400000)));
                    } else {
                        if (yesterday < day) {
                            editor.putLong(TitleActivity.DAILY_TIME, day);
                            editor.putInt(TitleActivity.DAILY_RECORD, (int) (this.point_score / 10));
                            editor.putBoolean(TitleActivity.DAILY_FLAG, true);
                        }
                        if (yesterday == day && pref.getInt(TitleActivity.DAILY_RECORD, (int) (this.point_score / 10)) < ((int) (this.point_score / 10))) {
                            editor.putInt(TitleActivity.DAILY_RECORD, (int) (this.point_score / 10));
                            editor.putBoolean(TitleActivity.DAILY_FLAG, true);
                        }
                    }
                }
                for (int i4 = 0; i4 < 100; i4++) {
                    editor.putFloat(TitleActivity.SCORE + i4, this.score[i4]);
                }
                editor.commit();
                this.scene = 3;
                return;
            case 3:
                this.activity.finish();
                return;
        }
    }

    public boolean touchEvent(MotionEvent event) {
        if (this.button_left.buttonEvent(event) != null) {
            if ((this.button_left.buttonEvent(event).getAction() == 0 || this.button_left.buttonEvent(event).getAction() == 2) && this.player != null) {
                this.player.setMove_mode(1);
            }
            if ((this.button_left.buttonEvent(event).getAction() == 1 || this.button_left.alpha == 255) && this.player != null) {
                this.player.setMove_mode(0);
            }
        }
        if (this.button_float.buttonEvent(event) != null && this.button_float.buttonEvent(event).getAction() == 0 && this.player != null && this.player.getYspeed() > 10.0f) {
            this.player.setYspeed(this.player.getYspeed() + (-40.0f * this.g.ratio_height));
            this.player.setMove_mode(3);
        }
        if (this.button_right.buttonEvent(event) != null) {
            if ((this.button_right.buttonEvent(event).getAction() == 0 || this.button_right.buttonEvent(event).getAction() == 2) && this.player != null) {
                this.player.setMove_mode(2);
            }
            if ((this.button_right.buttonEvent(event).getAction() == 1 || this.button_right.alpha == 255) && this.player != null) {
                this.player.setMove_mode(0);
            }
        }
        return false;
    }

    public void surfaceDestroyed(SurfaceHolder arg0) {
        this.thread = null;
    }

    public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
    }

    public void surfaceCreated(SurfaceHolder arg0) {
        this.thread = new Thread(this);
        this.thread.start();
    }
}
