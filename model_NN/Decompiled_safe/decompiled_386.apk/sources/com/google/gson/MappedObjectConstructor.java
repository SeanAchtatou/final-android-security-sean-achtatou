package com.google.gson;

import com.crossfield.ninjafall2.TitleActivity;
import com.google.gson.internal.C$Gson$Types;
import java.lang.reflect.Array;
import java.lang.reflect.Type;

final class MappedObjectConstructor implements ObjectConstructor {
    private static final DefaultConstructorAllocator defaultConstructorAllocator = new DefaultConstructorAllocator(TitleActivity.AMATEUR_P);
    private static final UnsafeAllocator unsafeAllocator = UnsafeAllocator.create();
    private final ParameterizedTypeHandlerMap<InstanceCreator<?>> instanceCreatorMap;

    public MappedObjectConstructor(ParameterizedTypeHandlerMap<InstanceCreator<?>> instanceCreators) {
        this.instanceCreatorMap = instanceCreators;
    }

    public <T> T construct(Type typeOfT) {
        InstanceCreator<T> creator = this.instanceCreatorMap.getHandlerFor(typeOfT);
        if (creator != null) {
            return creator.createInstance(typeOfT);
        }
        return constructWithAllocators(typeOfT);
    }

    public Object constructArray(Type type, int length) {
        return Array.newInstance(C$Gson$Types.getRawType(type), length);
    }

    private <T> T constructWithAllocators(Type typeOfT) {
        try {
            Class<?> rawType = C$Gson$Types.getRawType(typeOfT);
            T obj = defaultConstructorAllocator.newInstance(rawType);
            return obj == null ? unsafeAllocator.newInstance(rawType) : obj;
        } catch (Exception e) {
            throw new RuntimeException("Unable to invoke no-args constructor for " + typeOfT + ". " + "Register an InstanceCreator with Gson for this type may fix this problem.", e);
        }
    }

    public String toString() {
        return this.instanceCreatorMap.toString();
    }
}
