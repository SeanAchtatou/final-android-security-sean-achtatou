package com.openfeint.api.resource;

import com.openfeint.internal.APICallback;
import com.openfeint.internal.OpenFeintInternal;
import com.openfeint.internal.Util;
import com.openfeint.internal.request.CompressedBlobDownloadRequest;
import com.openfeint.internal.request.Compression;
import com.openfeint.internal.request.JSONRequest;
import com.openfeint.internal.resource.ServerException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ByteArrayEntity;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonToken;

public class CloudStorage {
    public static int MAX_SIZE = 262144;
    private static Pattern sValidKeyPattern;

    public static abstract class DeleteCB extends APICallback {
        public abstract void onSuccess();
    }

    public static abstract class ListCB extends APICallback {
        public abstract void onSuccess(List<String> list);
    }

    public static abstract class LoadCB extends APICallback {
        public abstract void onSuccess(byte[] bArr);
    }

    public static abstract class SaveCB extends APICallback {
        public abstract void onSuccess();
    }

    public static void list(final ListCB cb) {
        String userID = OpenFeintInternal.getInstance().getUserID();
        if ((userID == null || userID.length() == 0) && cb != null) {
            cb.onFailure("A user must be logged in to list their persisted CloudStorage blobs.");
        }
        final String path = "/xp/users/" + userID + "/games/" + OpenFeintInternal.getInstance().getAppID() + "/save_cards";
        new JSONRequest() {
            public boolean wantsLogin() {
                return true;
            }

            public String path() {
                return path;
            }

            public String method() {
                return "GET";
            }

            /* access modifiers changed from: protected */
            public Object parseJson(byte[] bodyStream) {
                Object o = Util.getObjFromJson(bodyStream);
                if (o != null && (o instanceof ServerException)) {
                    return o;
                }
                try {
                    JsonParser jp2 = new JsonFactory().createJsonParser(bodyStream);
                    if (jp2.nextToken() != JsonToken.START_OBJECT) {
                        throw new JsonParseException("Couldn't find toplevel wrapper object.", jp2.getTokenLocation());
                    } else if (jp2.nextToken() != JsonToken.FIELD_NAME) {
                        throw new JsonParseException("Couldn't find toplevel wrapper object.", jp2.getTokenLocation());
                    } else if (!jp2.getText().equals("save_cards")) {
                        throw new JsonParseException("Couldn't find toplevel wrapper object.", jp2.getTokenLocation());
                    } else if (jp2.nextToken() != JsonToken.START_ARRAY) {
                        throw new JsonParseException("Couldn't find savecard array.", jp2.getTokenLocation());
                    } else {
                        ArrayList<String> rv = new ArrayList<>();
                        while (jp2.nextToken() != JsonToken.END_ARRAY) {
                            if (jp2.getCurrentToken() != JsonToken.VALUE_STRING) {
                                throw new JsonParseException("Unexpected non-string in savecard array.", jp2.getTokenLocation());
                            }
                            rv.add(jp2.getText());
                        }
                        return rv;
                    }
                } catch (Exception e) {
                    OpenFeintInternal.log(TAG, e.getMessage());
                    return new ServerException("JSONError", "Unexpected response format");
                }
            }

            public void onSuccess(Object responseBody) {
                if (cb != null) {
                    cb.onSuccess((List) responseBody);
                }
            }

            public void onFailure(String reason) {
                if (cb != null) {
                    cb.onFailure(reason);
                }
            }
        }.launch();
    }

    public static void load(String key, final LoadCB cb) {
        String str;
        String userID = OpenFeintInternal.getInstance().getUserID();
        if ((userID == null || userID.length() == 0) && cb != null) {
            cb.onFailure("A user must be logged in to load data from a CloudStorage blob.");
        }
        if (!isValidKey(key) && cb != null) {
            StringBuilder sb = new StringBuilder("'");
            if (key == null) {
                str = "(null)";
            } else {
                str = key;
            }
            cb.onFailure(sb.append(str).append("' is not a valid CloudStorage key.").toString());
        }
        final String path = "/xp/users/" + userID + "/games/" + OpenFeintInternal.getInstance().getAppID() + "/save_cards/" + key;
        new CompressedBlobDownloadRequest() {
            public boolean wantsLogin() {
                return true;
            }

            public String path() {
                return path;
            }

            public void onSuccessDecompress(byte[] body) {
                if (cb != null) {
                    cb.onSuccess(body);
                }
            }

            public void onFailure(String exceptionMessage) {
                if (cb != null) {
                    cb.onFailure(exceptionMessage);
                }
            }
        }.launch();
    }

    public static void save(String key, final byte[] data, final SaveCB cb) {
        String userID = OpenFeintInternal.getInstance().getUserID();
        if (userID == null || userID.length() == 0) {
            if (cb != null) {
                cb.onFailure("Cannot save because the owner of this CloudStorage blob is not logged in.");
            }
        } else if (!isValidKey(key)) {
            if (cb != null) {
                cb.onFailure("'" + (key == null ? "(null)" : key) + "' is not a valid CloudStorage key.");
            }
        } else if (data == null || data.length == 0) {
            if (cb != null) {
                cb.onFailure("data is empty.  data must be set before saving.");
            }
        } else if (MAX_SIZE >= data.length) {
            final String path = "/xp/users/" + userID + "/games/" + OpenFeintInternal.getInstance().getAppID() + "/save_cards/" + key;
            new JSONRequest() {
                public boolean wantsLogin() {
                    return true;
                }

                public String path() {
                    return path;
                }

                public String method() {
                    return "PUT";
                }

                /* access modifiers changed from: protected */
                public HttpUriRequest generateRequest() {
                    HttpPut retval = new HttpPut(url());
                    retval.setEntity(new ByteArrayEntity(Compression.compress(data)));
                    addParams(retval);
                    return retval;
                }

                public void onSuccess(Object body) {
                    if (cb != null) {
                        cb.onSuccess();
                    }
                }

                public void onFailure(String reason) {
                    if (cb != null) {
                        cb.onFailure(reason);
                    }
                }
            }.launch();
        } else if (cb != null) {
            cb.onFailure("You cannot exceed 256 kB per save card");
        }
    }

    public static void delete(String key, final DeleteCB cb) {
        String str;
        String userID = OpenFeintInternal.getInstance().getUserID();
        if ((userID == null || userID.length() == 0) && cb != null) {
            cb.onFailure("The user who owns this CloudStorage blob is not logged in. The CloudStorage blob specified was not deleted.");
        }
        if (!isValidKey(key) && cb != null) {
            StringBuilder sb = new StringBuilder("'");
            if (key == null) {
                str = "(null)";
            } else {
                str = key;
            }
            cb.onFailure(sb.append(str).append("' is not a valid CloudStorage key.").toString());
        }
        final String path = "/xp/users/" + userID + "/games/" + OpenFeintInternal.getInstance().getAppID() + "/save_cards/" + key;
        new JSONRequest() {
            public boolean wantsLogin() {
                return true;
            }

            public String path() {
                return path;
            }

            public String method() {
                return "DELETE";
            }

            public void onSuccess(Object body) {
                if (cb != null) {
                    cb.onSuccess();
                }
            }

            public void onFailure(String reason) {
                if (cb != null) {
                    cb.onFailure(reason);
                }
            }
        }.launch();
    }

    public static boolean isValidKey(String key) {
        if (key == null) {
            return false;
        }
        if (sValidKeyPattern == null) {
            try {
                sValidKeyPattern = Pattern.compile("[a-zA-Z][a-zA-Z0-9-_]*");
            } catch (PatternSyntaxException e) {
                return false;
            }
        }
        return sValidKeyPattern.matcher(key).matches();
    }
}
