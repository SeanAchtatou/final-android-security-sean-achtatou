package com.openfeint.internal.notifications;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.crossfield.ninjafall2.R;
import com.openfeint.api.Notification;
import com.openfeint.internal.OpenFeintInternal;
import com.openfeint.internal.request.BitmapRequest;
import java.util.Map;

public class SimpleNotification extends NotificationBase {
    protected SimpleNotification(String text, String imageName, Notification.Category cat, Notification.Type type, Map<String, Object> userData) {
        super(text, imageName, cat, type, userData);
    }

    /* access modifiers changed from: protected */
    public boolean createView() {
        this.displayView = ((LayoutInflater) OpenFeintInternal.getInstance().getContext().getSystemService("layout_inflater")).inflate((int) R.layout.of_simple_notification, (ViewGroup) null);
        ((TextView) this.displayView.findViewById(R.id.of_text)).setText(getText());
        final ImageView icon = (ImageView) this.displayView.findViewById(R.id.of_icon);
        if (this.imageName != null) {
            Drawable image = getResourceDrawable(this.imageName);
            if (image == null) {
                new BitmapRequest() {
                    public String path() {
                        return SimpleNotification.this.imageName;
                    }

                    public void onSuccess(Bitmap responseBody) {
                        icon.setImageDrawable(new BitmapDrawable(responseBody));
                        SimpleNotification.this.showToast();
                    }

                    public void onFailure(String exceptionMessage) {
                        OpenFeintInternal.log("NotificationImage", "Failed to load image " + SimpleNotification.this.imageName + ":" + exceptionMessage);
                        icon.setVisibility(4);
                        SimpleNotification.this.showToast();
                    }
                }.launch();
                return false;
            }
            icon.setImageDrawable(image);
        } else {
            icon.setVisibility(4);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void drawView(Canvas canvas) {
        this.displayView.draw(canvas);
    }

    public static void show(String text, Notification.Category c, Notification.Type t) {
        show(text, null, c, t);
    }

    public static void show(String text, String imageName, Notification.Category c, Notification.Type t) {
        new SimpleNotification(text, imageName, c, t, null).checkDelegateAndView();
    }
}
