package com.openfeint.internal.resource;

import java.io.IOException;
import java.lang.Enum;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;

public abstract class EnumResourceProperty<T extends Enum<T>> extends PrimitiveResourceProperty {
    Class<T> mEnumClass;

    public abstract T get(Resource resource);

    public abstract void set(Resource resource, T t);

    public EnumResourceProperty(Class<T> enumClass) {
        this.mEnumClass = enumClass;
    }

    public void copy(Resource lhs, Resource rhs) {
        set(lhs, get(rhs));
    }

    public void parse(Resource obj, JsonParser jp2) throws JsonParseException, IOException {
        set(obj, Enum.valueOf(this.mEnumClass, jp2.getText()));
    }

    public void generate(Resource obj, JsonGenerator generator, String key) throws JsonGenerationException, IOException {
        T val = get(obj);
        generator.writeFieldName(key);
        generator.writeString(val.toString());
    }
}
