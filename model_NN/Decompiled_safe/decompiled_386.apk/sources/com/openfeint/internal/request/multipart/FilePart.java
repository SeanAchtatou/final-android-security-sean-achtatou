package com.openfeint.internal.request.multipart;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class FilePart extends PartBase {
    public static final String DEFAULT_CHARSET = "ISO-8859-1";
    public static final String DEFAULT_CONTENT_TYPE = "application/octet-stream";
    public static final String DEFAULT_TRANSFER_ENCODING = "binary";
    protected static final String FILE_NAME = "; filename=";
    private static final byte[] FILE_NAME_BYTES = EncodingUtil.getAsciiBytes(FILE_NAME);
    private PartSource source;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public FilePart(java.lang.String r4, com.openfeint.internal.request.multipart.PartSource r5, java.lang.String r6, java.lang.String r7) {
        /*
            r3 = this;
            if (r6 != 0) goto L_0x0017
            java.lang.String r0 = "application/octet-stream"
        L_0x0004:
            if (r7 != 0) goto L_0x0019
            java.lang.String r1 = "ISO-8859-1"
        L_0x0008:
            java.lang.String r2 = "binary"
            r3.<init>(r4, r0, r1, r2)
            if (r5 != 0) goto L_0x001b
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "Source may not be null"
            r0.<init>(r1)
            throw r0
        L_0x0017:
            r0 = r6
            goto L_0x0004
        L_0x0019:
            r1 = r7
            goto L_0x0008
        L_0x001b:
            r3.source = r5
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.openfeint.internal.request.multipart.FilePart.<init>(java.lang.String, com.openfeint.internal.request.multipart.PartSource, java.lang.String, java.lang.String):void");
    }

    public FilePart(String name, PartSource partSource) {
        this(name, partSource, (String) null, (String) null);
    }

    public FilePart(String name, File file) throws FileNotFoundException {
        this(name, new FilePartSource(file), (String) null, (String) null);
    }

    public FilePart(String name, File file, String contentType, String charset) throws FileNotFoundException {
        this(name, new FilePartSource(file), contentType, charset);
    }

    public FilePart(String name, String fileName, File file) throws FileNotFoundException {
        this(name, new FilePartSource(fileName, file), (String) null, (String) null);
    }

    public FilePart(String name, String fileName, File file, String contentType, String charset) throws FileNotFoundException {
        this(name, new FilePartSource(fileName, file), contentType, charset);
    }

    /* access modifiers changed from: protected */
    public void sendDispositionHeader(OutputStream out) throws IOException {
        super.sendDispositionHeader(out);
        String filename = this.source.getFileName();
        if (filename != null) {
            out.write(FILE_NAME_BYTES);
            out.write(QUOTE_BYTES);
            out.write(EncodingUtil.getAsciiBytes(filename));
            out.write(QUOTE_BYTES);
        }
    }

    /* access modifiers changed from: protected */
    public void sendData(OutputStream out) throws IOException {
        if (lengthOfData() != 0) {
            byte[] tmp = new byte[4096];
            InputStream instream = this.source.createInputStream();
            while (true) {
                try {
                    int len = instream.read(tmp);
                    if (len >= 0) {
                        out.write(tmp, 0, len);
                    } else {
                        return;
                    }
                } finally {
                    instream.close();
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public PartSource getSource() {
        return this.source;
    }

    /* access modifiers changed from: protected */
    public long lengthOfData() throws IOException {
        return this.source.getLength();
    }
}
