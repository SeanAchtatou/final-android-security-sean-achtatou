package com.openfeint.internal.request;

import com.crossfield.ninjafall2.R;
import com.openfeint.internal.OpenFeintInternal;
import com.openfeint.internal.Util;
import com.openfeint.internal.resource.ServerException;

public abstract class JSONRequest extends JSONContentRequest {
    public void onSuccess(Object responseBody) {
    }

    public void onFailure(String exceptionMessage) {
    }

    public JSONRequest() {
    }

    public JSONRequest(OrderedArgList args) {
        super(args);
    }

    public void onResponse(int responseCode, byte[] bodyStream) {
        if (bodyStream.length == 0 || ((bodyStream.length == 1 && bodyStream[0] == 32) || isResponseJSON())) {
            onResponse(responseCode, parseJson(bodyStream));
        } else {
            onResponse(responseCode, notJSONError(responseCode));
        }
    }

    /* access modifiers changed from: protected */
    public Object parseJson(byte[] bodyStream) {
        return Util.getObjFromJson(bodyStream);
    }

    /* access modifiers changed from: protected */
    public void onResponse(int responseCode, Object responseBody) {
        if (200 > responseCode || responseCode >= 300 || (responseBody != null && (responseBody instanceof ServerException))) {
            onFailure(responseBody);
        } else {
            onSuccess(responseBody);
        }
    }

    /* access modifiers changed from: protected */
    public void onFailure(Object responseBody) {
        String exceptionMessage = OpenFeintInternal.getRString(R.string.of_unknown_server_error);
        if (responseBody != null && (responseBody instanceof ServerException)) {
            ServerException e = (ServerException) responseBody;
            exceptionMessage = e.message;
            if (e.needsDeveloperAttention) {
                OpenFeintInternal.log("ServerException", exceptionMessage);
                OpenFeintInternal.getInstance().displayErrorDialog(exceptionMessage);
            }
        }
        onFailure(exceptionMessage);
    }
}
