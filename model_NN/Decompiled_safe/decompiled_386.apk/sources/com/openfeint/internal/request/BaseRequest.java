package com.openfeint.internal.request;

import com.crossfield.ninjafall2.R;
import com.openfeint.internal.OpenFeintInternal;
import com.openfeint.internal.resource.ServerException;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.Future;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;

public abstract class BaseRequest {
    private static int DEFAULT_RETRIES = 2;
    private static long DEFAULT_TIMEOUT = 20000;
    protected static String TAG = "Request";
    private static String sBaseServerURL = null;
    protected OrderedArgList mArgs;
    /* access modifiers changed from: private */
    public String mCurrentURL = null;
    private Future<?> mFuture = null;
    private HttpParams mHttpParams = null;
    private String mKey = null;
    /* access modifiers changed from: private */
    public HttpUriRequest mRequest;
    private boolean mResponded = false;
    /* access modifiers changed from: private */
    public byte[] mResponseBody;
    /* access modifiers changed from: private */
    public int mResponseCode;
    /* access modifiers changed from: private */
    public String mResponseEncoding = null;
    /* access modifiers changed from: private */
    public String mResponseType = null;
    private int mRetriesLeft = 0;
    private long mSecondsSinceEpoch;
    private String mSignature = null;
    /* access modifiers changed from: private */
    public HttpResponse response_;

    public abstract String method();

    public abstract void onResponse(int i, byte[] bArr);

    public abstract String path();

    /* access modifiers changed from: protected */
    public String getResponseEncoding() {
        return this.mResponseEncoding;
    }

    /* access modifiers changed from: protected */
    public String getResponseType() {
        return this.mResponseType;
    }

    public int numRetries() {
        return DEFAULT_RETRIES;
    }

    public long timeout() {
        return DEFAULT_TIMEOUT;
    }

    /* access modifiers changed from: protected */
    public String currentURL() {
        return this.mCurrentURL != null ? this.mCurrentURL : url();
    }

    public void setFuture(Future<?> future) {
        this.mFuture = future;
    }

    public Future<?> getFuture() {
        return this.mFuture;
    }

    /* access modifiers changed from: protected */
    public HttpParams getHttpParams() {
        if (this.mHttpParams == null) {
            this.mHttpParams = new BasicHttpParams();
        }
        return this.mHttpParams;
    }

    public boolean wantsLogin() {
        return false;
    }

    public boolean signed() {
        return true;
    }

    public boolean needsDeviceSession() {
        return signed();
    }

    public BaseRequest() {
    }

    public BaseRequest(OrderedArgList args) {
        setArgs(args);
    }

    public String url() {
        if (sBaseServerURL == null) {
            sBaseServerURL = OpenFeintInternal.getInstance().getServerUrl();
        }
        return String.valueOf(sBaseServerURL) + path();
    }

    public final void sign(Signer authority) {
        if (this.mArgs == null) {
            this.mArgs = new OrderedArgList();
        }
        if (signed()) {
            this.mSecondsSinceEpoch = System.currentTimeMillis() / 1000;
            this.mSignature = authority.sign(path(), method(), this.mSecondsSinceEpoch, this.mArgs);
            this.mKey = authority.getKey();
        }
    }

    public final void setArgs(OrderedArgList args) {
        this.mArgs = args;
    }

    /* access modifiers changed from: protected */
    public HttpEntity genEntity() throws UnsupportedEncodingException {
        UrlEncodedFormEntity entity = new UrlEncodedFormEntity(this.mArgs.getArgs(), "UTF-8");
        entity.setContentType("application/x-www-form-urlencoded; charset=" + "UTF-8");
        return entity;
    }

    /* access modifiers changed from: protected */
    public HttpUriRequest generateRequest() {
        HttpEntityEnclosingRequestBase postReq;
        HttpEntityEnclosingRequestBase retval = null;
        String meth = method();
        if (meth.equals("GET") || meth.equals("DELETE")) {
            String url = url();
            String argString = this.mArgs.getArgString();
            if (argString != null) {
                url = String.valueOf(url) + "?" + argString;
            }
            if (meth.equals("GET")) {
                retval = new HttpGet(url);
            } else if (meth.equals("DELETE")) {
                retval = new HttpDelete(url);
            }
        } else {
            if (meth.equals("POST")) {
                postReq = new HttpPost(url());
            } else if (meth.equals("PUT")) {
                postReq = new HttpPut(url());
            } else {
                throw new RuntimeException("Unsupported HTTP method: " + meth);
            }
            try {
                postReq.setEntity(genEntity());
            } catch (UnsupportedEncodingException e) {
                OpenFeintInternal.log(TAG, "Unable to encode request.");
                e.printStackTrace(System.err);
            }
            retval = postReq;
        }
        if (!(!signed() || this.mSignature == null || this.mKey == null)) {
            retval.addHeader("X-OF-Signature", this.mSignature);
            retval.addHeader("X-OF-Key", this.mKey);
        }
        addParams(retval);
        return retval;
    }

    /* access modifiers changed from: protected */
    public boolean shouldRedirect(String url) {
        return true;
    }

    /* access modifiers changed from: protected */
    public final void addParams(HttpUriRequest retval) {
        if (this.mHttpParams != null) {
            retval.setParams(this.mHttpParams);
        }
    }

    /* JADX WARNING: CFG modification limit reached, blocks count: 124 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void exec(boolean r13) {
        /*
            r12 = this;
            r11 = 1
            r10 = 0
            r9 = 0
            org.apache.http.client.methods.HttpUriRequest r6 = r12.generateRequest()
            r12.mRequest = r6
            int r6 = r12.numRetries()
            r12.mRetriesLeft = r6
            r12.mResponseBody = r9
            r12.mResponseCode = r10
            r12.response_ = r9
            org.apache.http.client.methods.HttpUriRequest r6 = r12.mRequest
            java.net.URI r6 = r6.getURI()
            java.lang.String r4 = r6.getPath()
            java.lang.String r6 = "//"
            boolean r6 = r4.contains(r6)
            if (r6 == 0) goto L_0x00c5
            com.openfeint.internal.resource.ServerException r5 = new com.openfeint.internal.resource.ServerException
            r5.<init>()
            java.lang.String r6 = "RequestError"
            r5.exceptionClass = r6
            r6 = 2131034154(0x7f05002a, float:1.7678818E38)
            java.lang.String r6 = com.openfeint.internal.OpenFeintInternal.getRString(r6)
            r5.message = r6
            r5.needsDeveloperAttention = r11
            r12.fakeServerException(r5)
        L_0x003e:
            int r6 = r12.mResponseCode
            byte[] r7 = r12.mResponseBody
            r12.onResponseOffMainThread(r6, r7)
            return
        L_0x0046:
            if (r13 == 0) goto L_0x00ab
            r6 = 0
            r12.mRetriesLeft = r6     // Catch:{ Exception -> 0x0053 }
            java.lang.Exception r6 = new java.lang.Exception     // Catch:{ Exception -> 0x0053 }
            java.lang.String r7 = "Forced failure"
            r6.<init>(r7)     // Catch:{ Exception -> 0x0053 }
            throw r6     // Catch:{ Exception -> 0x0053 }
        L_0x0053:
            r6 = move-exception
            r2 = r6
            java.lang.String r6 = com.openfeint.internal.request.BaseRequest.TAG
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            java.lang.String r8 = "Error executing request '"
            r7.<init>(r8)
            java.lang.String r8 = r12.path()
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.String r8 = "'."
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.String r7 = r7.toString()
            com.openfeint.internal.OpenFeintInternal.log(r6, r7)
            java.io.PrintStream r6 = java.lang.System.err
            r2.printStackTrace(r6)
            r12.mResponseBody = r9
            r12.mResponseCode = r10
            r12.response_ = r9
            int r6 = r12.mRetriesLeft
            int r6 = r6 - r11
            r12.mRetriesLeft = r6
            if (r6 >= 0) goto L_0x00c5
            com.openfeint.internal.resource.ServerException r5 = new com.openfeint.internal.resource.ServerException
            r5.<init>()
            java.lang.Class r6 = r2.getClass()
            java.lang.String r6 = r6.getName()
            r5.exceptionClass = r6
            java.lang.String r6 = r2.getMessage()
            r5.message = r6
            java.lang.String r6 = r5.message
            if (r6 != 0) goto L_0x00a7
            r6 = 2131034117(0x7f050005, float:1.7678742E38)
            java.lang.String r6 = com.openfeint.internal.OpenFeintInternal.getRString(r6)
            r5.message = r6
        L_0x00a7:
            r12.fakeServerException(r5)
            goto L_0x003e
        L_0x00ab:
            com.openfeint.internal.OpenFeintInternal r6 = com.openfeint.internal.OpenFeintInternal.getInstance()     // Catch:{ Exception -> 0x0053 }
            org.apache.http.impl.client.AbstractHttpClient r0 = r6.getClient()     // Catch:{ Exception -> 0x0053 }
            org.apache.http.protocol.BasicHttpContext r1 = new org.apache.http.protocol.BasicHttpContext     // Catch:{ Exception -> 0x0053 }
            r1.<init>()     // Catch:{ Exception -> 0x0053 }
            com.openfeint.internal.request.BaseRequest$1 r3 = new com.openfeint.internal.request.BaseRequest$1     // Catch:{ Exception -> 0x0053 }
            r3.<init>(r1)     // Catch:{ Exception -> 0x0053 }
            org.apache.http.client.methods.HttpUriRequest r6 = r12.mRequest     // Catch:{ Exception -> 0x0053 }
            r0.execute(r6, r3, r1)     // Catch:{ Exception -> 0x0053 }
            r6 = 0
            r12.mRequest = r6     // Catch:{ Exception -> 0x0053 }
        L_0x00c5:
            byte[] r6 = r12.mResponseBody
            if (r6 == 0) goto L_0x0046
            goto L_0x003e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.openfeint.internal.request.BaseRequest.exec(boolean):void");
    }

    private void fakeServerException(ServerException se) {
        this.mResponseCode = 0;
        this.mResponseBody = se.generate().getBytes();
        this.mResponseType = JSONContentRequest.DESIRED_RESPONSE_PREFIX;
    }

    public HttpResponse getResponse() {
        return this.response_;
    }

    /* access modifiers changed from: protected */
    public void onResponseOffMainThread(int responseCode, byte[] body) {
    }

    public final void onResponse() {
        if (!this.mResponded) {
            this.mResponded = true;
            if (this.mResponseBody == null) {
                this.mResponseCode = 0;
                ServerException se = new ServerException();
                se.exceptionClass = "Unknown";
                se.message = OpenFeintInternal.getRString(R.string.of_unknown_server_error);
                fakeServerException(se);
            }
            onResponse(this.mResponseCode, this.mResponseBody);
            this.response_ = null;
        }
    }

    public void launch() {
        OpenFeintInternal.makeRequest(this);
    }

    public void postTimeoutCleanup() {
        final HttpUriRequest req = this.mRequest;
        this.mRequest = null;
        if (req != null) {
            new Thread(new Runnable() {
                public void run() {
                    try {
                        req.abort();
                    } catch (UnsupportedOperationException e) {
                    }
                }
            }).start();
        }
        ServerException se = new ServerException();
        se.exceptionClass = "Timeout";
        se.message = OpenFeintInternal.getRString(R.string.of_timeout);
        fakeServerException(se);
    }
}
