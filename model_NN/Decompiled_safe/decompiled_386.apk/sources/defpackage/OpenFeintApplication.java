package defpackage;

import android.app.Application;
import com.openfeint.api.OpenFeint;
import com.openfeint.api.OpenFeintDelegate;
import com.openfeint.api.OpenFeintSettings;
import com.openfeint.api.resource.Achievement;
import com.openfeint.api.resource.Leaderboard;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: OpenFeintApplication  reason: default package */
public class OpenFeintApplication extends Application {
    public static List<Achievement> achievements = null;
    static final String gameID = "314502";
    static final String gameKey = "uz7XM1iG71KBvI0gF9Al8A";
    static final String gameName = "OpenFeintTest-2";
    static final String gameSecret = "GJMaY4S8WT2MkN2gKDWy3AiHiAH1xcus3COJcM9H28";
    public static List<Leaderboard> leaderboards = null;

    public void onCreate() {
        super.onCreate();
        Map<String, Object> options = new HashMap<>();
        options.put(OpenFeintSettings.SettingCloudStorageCompressionStrategy, OpenFeintSettings.CloudStorageCompressionStrategyDefault);
        OpenFeint.initialize(this, new OpenFeintSettings(gameName, gameKey, gameSecret, gameID, options), new OpenFeintDelegate() {
        });
        Achievement.list(new Achievement.ListCB() {
            public void onSuccess(List<Achievement> _achievements) {
                OpenFeintApplication.achievements = _achievements;
            }
        });
        Leaderboard.list(new Leaderboard.ListCB() {
            public void onSuccess(List<Leaderboard> _leaderboards) {
                OpenFeintApplication.leaderboards = _leaderboards;
            }
        });
    }
}
