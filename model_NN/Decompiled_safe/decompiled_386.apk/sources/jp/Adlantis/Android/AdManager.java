package jp.Adlantis.Android;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Vector;
import org.apache.http.Header;
import org.apache.http.HttpException;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.AuthState;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.DefaultRedirectHandler;
import org.apache.http.protocol.HttpContext;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class AdManager {
    /* access modifiers changed from: private */
    public static String DEBUG_TASK = "AdManager";
    public long adDisplayInterval;
    public long adFetchInterval;
    public int adIndex;
    public AdlantisAd[] ads;
    private AsyncImageLoader asyncImageLoader;
    private boolean connectionChangeReceiverRegistered;
    /* access modifiers changed from: private */
    public String conversionTag;
    public String conversionTagHost;
    /* access modifiers changed from: private */
    public boolean conversionTagSent;
    public String conversionTagTestHost;
    /* access modifiers changed from: private */
    public HashMap<String, String> defaultParamMap;
    public String host;
    /* access modifiers changed from: private */
    public LinkedList<AdlantisAd> impressionCountQueue;
    private boolean isDoingAdRequest;
    private String keywords;
    public int port;
    private String publisherID;
    private int testAdRequestUrlIndex;
    public String[] testAdRequestUrls;

    public interface AdManagerCallback {
        void adsLoaded(AdManager adManager);
    }

    public interface AdManagerClickUrlProcessedCallback {
        void clickProcessed(Uri uri);
    }

    private static class AdManagerHolder {
        /* access modifiers changed from: private */
        public static final AdManager INSTANCE = new AdManager();

        private AdManagerHolder() {
        }
    }

    private class ConnectionChangeReceiver extends BroadcastReceiver {
        private ConnectionChangeReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            if (AdManager.isNetworkAvailable(context)) {
                if (AdManager.this.impressionCountQueue.size() > 0) {
                    Log.d(AdManager.DEBUG_TASK, "network status changed");
                    Object[] array = AdManager.this.impressionCountQueue.toArray();
                    int i = 0;
                    while (true) {
                        int i2 = i;
                        if (i2 >= array.length) {
                            break;
                        }
                        AdlantisAd adlantisAd = (AdlantisAd) array[i2];
                        AdManager.this.impressionCountQueue.remove(adlantisAd);
                        AdManager.this.sendImpressionCountForAd(adlantisAd);
                        i = i2 + 1;
                    }
                }
                if (AdManager.this.conversionTag != null && !AdManager.this.conversionTagSent) {
                    AdManager.this.sendConversionTag(context, AdManager.this.conversionTag);
                }
            }
        }
    }

    private AdManager() {
        this.host = "sp.ad.adlantis.jp";
        this.conversionTagHost = "sp.conv.adlantis.jp";
        this.conversionTagTestHost = "sp.www.adlantis.jp";
        this.port = 80;
        this.adIndex = 0;
        this.adFetchInterval = 300000;
        this.adDisplayInterval = 10000;
        this.asyncImageLoader = new AsyncImageLoader();
        this.conversionTag = null;
        this.conversionTagSent = false;
        this.connectionChangeReceiverRegistered = false;
        this.impressionCountQueue = new LinkedList<>();
    }

    private Uri adRequestURI(Context context) {
        HashMap<String, String> defaultRequestBuilder = defaultRequestBuilder(context);
        Uri.Builder builder = new Uri.Builder();
        setUriParamsFromMap(builder, defaultRequestBuilder);
        builder.scheme("http");
        builder.authority(this.host);
        builder.path("/sp/load_app_ads");
        builder.appendQueryParameter("callbackid", "0");
        builder.appendQueryParameter("zid", this.publisherID);
        builder.appendQueryParameter("adl_app_flg", "1");
        if (this.keywords != null) {
            builder.appendQueryParameter("keywords", this.keywords);
        }
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        builder.appendQueryParameter("displaySize", displayMetrics.widthPixels + "x" + displayMetrics.heightPixels);
        builder.appendQueryParameter("displayDensity", Float.toString(displayMetrics.density));
        return builder.build();
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.HashMap<java.lang.String, java.lang.String> defaultRequestBuilder(android.content.Context r5) {
        /*
            r4 = this;
            monitor-enter(r4)
            java.util.HashMap<java.lang.String, java.lang.String> r0 = r4.defaultParamMap     // Catch:{ all -> 0x0095 }
            if (r0 == 0) goto L_0x0009
            java.util.HashMap<java.lang.String, java.lang.String> r0 = r4.defaultParamMap     // Catch:{ all -> 0x0095 }
            monitor-exit(r4)     // Catch:{ all -> 0x0095 }
        L_0x0008:
            return r0
        L_0x0009:
            java.util.HashMap r0 = new java.util.HashMap     // Catch:{ all -> 0x0095 }
            r0.<init>()     // Catch:{ all -> 0x0095 }
            r4.defaultParamMap = r0     // Catch:{ all -> 0x0095 }
            if (r5 == 0) goto L_0x001d
            java.util.HashMap<java.lang.String, java.lang.String> r0 = r4.defaultParamMap     // Catch:{ all -> 0x0095 }
            java.lang.String r1 = "appIdentifier"
            java.lang.String r2 = r5.getPackageName()     // Catch:{ all -> 0x0095 }
            r0.put(r1, r2)     // Catch:{ all -> 0x0095 }
        L_0x001d:
            java.util.HashMap<java.lang.String, java.lang.String> r0 = r4.defaultParamMap     // Catch:{ all -> 0x0095 }
            java.lang.String r1 = "deviceClass"
            java.lang.String r2 = "android"
            r0.put(r1, r2)     // Catch:{ all -> 0x0095 }
            java.lang.String r0 = android.os.Build.VERSION.RELEASE     // Catch:{ all -> 0x0095 }
            r1 = 0
            java.text.NumberFormat r2 = java.text.NumberFormat.getNumberInstance()     // Catch:{ ParseException -> 0x0090 }
            java.lang.Number r1 = r2.parse(r0)     // Catch:{ ParseException -> 0x0090 }
        L_0x0031:
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0095 }
            java.util.HashMap<java.lang.String, java.lang.String> r2 = r4.defaultParamMap     // Catch:{ all -> 0x0095 }
            java.lang.String r3 = "deviceOsVersion"
            r2.put(r3, r1)     // Catch:{ all -> 0x0095 }
            java.util.HashMap<java.lang.String, java.lang.String> r1 = r4.defaultParamMap     // Catch:{ all -> 0x0095 }
            java.lang.String r2 = "deviceOsVersionFull"
            r1.put(r2, r0)     // Catch:{ all -> 0x0095 }
            java.lang.String r0 = android.os.Build.MODEL     // Catch:{ all -> 0x0095 }
            java.lang.String r1 = "sdk"
            int r1 = r0.compareTo(r1)     // Catch:{ all -> 0x0095 }
            if (r1 != 0) goto L_0x004f
            java.lang.String r0 = "simulator"
        L_0x004f:
            java.util.HashMap<java.lang.String, java.lang.String> r1 = r4.defaultParamMap     // Catch:{ all -> 0x0095 }
            java.lang.String r2 = "deviceFamily"
            r1.put(r2, r0)     // Catch:{ all -> 0x0095 }
            java.util.HashMap<java.lang.String, java.lang.String> r0 = r4.defaultParamMap     // Catch:{ all -> 0x0095 }
            java.lang.String r1 = "deviceBrand"
            java.lang.String r2 = android.os.Build.BRAND     // Catch:{ all -> 0x0095 }
            r0.put(r1, r2)     // Catch:{ all -> 0x0095 }
            java.util.HashMap<java.lang.String, java.lang.String> r0 = r4.defaultParamMap     // Catch:{ all -> 0x0095 }
            java.lang.String r1 = "deviceName"
            java.lang.String r2 = android.os.Build.DEVICE     // Catch:{ all -> 0x0095 }
            r0.put(r1, r2)     // Catch:{ all -> 0x0095 }
            java.lang.String r0 = r4.md5_uniqueID(r5)     // Catch:{ all -> 0x0095 }
            if (r0 == 0) goto L_0x0075
            java.util.HashMap<java.lang.String, java.lang.String> r1 = r4.defaultParamMap     // Catch:{ all -> 0x0095 }
            java.lang.String r2 = "udid"
            r1.put(r2, r0)     // Catch:{ all -> 0x0095 }
        L_0x0075:
            java.util.HashMap<java.lang.String, java.lang.String> r0 = r4.defaultParamMap     // Catch:{ all -> 0x0095 }
            java.lang.String r1 = "sdkVersion"
            java.lang.String r2 = r4.sdkVersion()     // Catch:{ all -> 0x0095 }
            r0.put(r1, r2)     // Catch:{ all -> 0x0095 }
            java.util.HashMap<java.lang.String, java.lang.String> r0 = r4.defaultParamMap     // Catch:{ all -> 0x0095 }
            java.lang.String r1 = "sdkBuild"
            java.lang.String r2 = r4.sdkBuild()     // Catch:{ all -> 0x0095 }
            r0.put(r1, r2)     // Catch:{ all -> 0x0095 }
            monitor-exit(r4)     // Catch:{ all -> 0x0095 }
            java.util.HashMap<java.lang.String, java.lang.String> r0 = r4.defaultParamMap
            goto L_0x0008
        L_0x0090:
            r2 = move-exception
            r2.printStackTrace()     // Catch:{ all -> 0x0095 }
            goto L_0x0031
        L_0x0095:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0095 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: jp.Adlantis.Android.AdManager.defaultRequestBuilder(android.content.Context):java.util.HashMap");
    }

    /* access modifiers changed from: private */
    public boolean doAdRequest(Context context, AdManagerCallback adManagerCallback) {
        Uri adRequestURI;
        boolean z;
        Object obj;
        this.impressionCountQueue.clear();
        if (this.isDoingAdRequest) {
            return false;
        }
        this.isDoingAdRequest = true;
        if (this.testAdRequestUrls == null || this.testAdRequestUrls.length <= 0) {
            adRequestURI = adRequestURI(context);
        } else {
            adRequestURI = Uri.parse(this.testAdRequestUrls[this.testAdRequestUrlIndex]);
            this.testAdRequestUrlIndex = (this.testAdRequestUrlIndex + 1) % this.testAdRequestUrls.length;
        }
        try {
            AnonymousClass1 r3 = new HttpRequestInterceptor() {
                public void process(HttpRequest httpRequest, HttpContext httpContext) throws HttpException, IOException {
                    Credentials credentials;
                    AuthState authState = (AuthState) httpContext.getAttribute("http.auth.target-scope");
                    CredentialsProvider credentialsProvider = (CredentialsProvider) httpContext.getAttribute("http.auth.credentials-provider");
                    HttpHost httpHost = (HttpHost) httpContext.getAttribute("http.target_host");
                    if (authState.getAuthScheme() == null && (credentials = credentialsProvider.getCredentials(new AuthScope(httpHost.getHostName(), httpHost.getPort()))) != null) {
                        authState.setAuthScheme(new BasicScheme());
                        authState.setCredentials(credentials);
                    }
                }
            };
            HttpHost httpHost = new HttpHost(this.host, this.port, "http");
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
            defaultHttpClient.addRequestInterceptor(r3, 0);
            defaultHttpClient.getCredentialsProvider().setCredentials(new AuthScope(httpHost.getHostName(), httpHost.getPort()), new UsernamePasswordCredentials("3263", "0315"));
            String uri = adRequestURI.toString();
            Log.d(DEBUG_TASK, uri);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(defaultHttpClient.execute(new HttpGet(uri)).getEntity().getContent()), 8192);
            try {
                obj = new JSONParser().parse(bufferedReader);
            } catch (Exception e) {
                Log.e(DEBUG_TASK, e.toString());
                obj = null;
            }
            if (obj != null) {
                JSONArray jSONArray = obj instanceof JSONArray ? (JSONArray) obj : obj instanceof JSONObject ? (JSONArray) ((JSONObject) obj).get("ads") : null;
                if (jSONArray != null) {
                    int size = jSONArray.size();
                    synchronized (this) {
                        this.ads = new AdlantisAd[size];
                        for (int i = 0; i < size; i++) {
                            this.ads[i] = new AdlantisAd((HashMap) jSONArray.get(i));
                        }
                    }
                } else {
                    Log.w(DEBUG_TASK, "Adlantis no ads received");
                }
            }
            bufferedReader.close();
            if (this.ads != null) {
                z = this.ads.length > 0;
                this.isDoingAdRequest = false;
                return z;
            }
        } catch (MalformedURLException e2) {
            Log.e(DEBUG_TASK, e2.toString());
            z = false;
        } catch (IOException e3) {
            Log.e(DEBUG_TASK, e3.toString());
        }
        z = false;
        this.isDoingAdRequest = false;
        return z;
    }

    private AdlantisAd[] filteredAdsForOrientation(int i) {
        if (this.ads == null) {
            return null;
        }
        Vector vector = new Vector();
        synchronized (this) {
            for (int i2 = 0; i2 < this.ads.length; i2++) {
                if (this.ads[i2].hasAdForOrientation(i).booleanValue()) {
                    vector.addElement(this.ads[i2]);
                }
            }
        }
        AdlantisAd[] adlantisAdArr = new AdlantisAd[vector.size()];
        vector.copyInto(adlantisAdArr);
        return adlantisAdArr;
    }

    public static AdManager getInstance() {
        return AdManagerHolder.INSTANCE;
    }

    private void handleHttpClickRequest(final String str, final AdManagerClickUrlProcessedCallback adManagerClickUrlProcessedCallback) {
        final AnonymousClass6 r0 = new Handler() {
            public void handleMessage(Message message) {
                if (adManagerClickUrlProcessedCallback != null) {
                    adManagerClickUrlProcessedCallback.clickProcessed((Uri) message.obj);
                }
            }
        };
        final AnonymousClass7 r1 = new DefaultRedirectHandler() {
            public boolean isRedirectRequested(HttpResponse httpResponse, HttpContext httpContext) {
                String str = null;
                int statusCode = httpResponse.getStatusLine().getStatusCode();
                if (statusCode >= 300 && statusCode < 400) {
                    Header[] headers = httpResponse.getHeaders("Location");
                    if (headers.length > 0) {
                        str = headers[0].getValue();
                        Log.d(AdManager.DEBUG_TASK, "location=" + str);
                    }
                }
                if (str == null) {
                    str = ((HttpHost) httpContext.getAttribute("http.target_host")).toURI() + ((HttpUriRequest) httpContext.getAttribute("http.request")).getURI();
                }
                r0.sendMessage(r0.obtainMessage(0, Uri.parse(str)));
                return false;
            }
        };
        new Thread() {
            public void run() {
                try {
                    DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
                    defaultHttpClient.setRedirectHandler(r1);
                    Uri.Builder buildUpon = Uri.parse(str).buildUpon();
                    AdManager.this.setUriParamsFromMap(buildUpon, AdManager.this.defaultParamMap);
                    String uri = buildUpon.build().toString();
                    Log.d(AdManager.DEBUG_TASK, "handleClickRequest=" + uri);
                    defaultHttpClient.execute(new HttpGet(uri));
                } catch (MalformedURLException e) {
                    Log.e(AdManager.DEBUG_TASK, e.toString());
                } catch (IOException e2) {
                    Log.e(AdManager.DEBUG_TASK, e2.toString());
                }
            }
        }.start();
    }

    /* access modifiers changed from: private */
    public Uri impressionCountURI(AdlantisAd adlantisAd) {
        Uri.Builder buildUpon = Uri.parse((String) adlantisAd.get("count_impression")).buildUpon();
        setUriParamsFromMap(buildUpon, this.defaultParamMap);
        return buildUpon.build();
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager != null) {
            try {
                NetworkInfo[] allNetworkInfo = connectivityManager.getAllNetworkInfo();
                if (allNetworkInfo != null) {
                    for (NetworkInfo isConnected : allNetworkInfo) {
                        if (isConnected.isConnected()) {
                            return true;
                        }
                    }
                }
            } catch (Exception e) {
                Log.e(DEBUG_TASK, e.toString());
            }
        }
        return false;
    }

    public static void main(String[] strArr) {
        new AdManager().connect(null, null);
    }

    private static final String md5(String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(str.getBytes());
            byte[] digest = instance.digest();
            StringBuffer stringBuffer = new StringBuffer();
            for (byte b : digest) {
                String hexString = Integer.toHexString(b & 255);
                while (hexString.length() < 2) {
                    hexString = "0" + hexString;
                }
                stringBuffer.append(hexString);
            }
            return stringBuffer.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return "";
        }
    }

    private String md5_uniqueID(Context context) {
        String uniqueID = uniqueID(context);
        if (uniqueID != null) {
            return md5(uniqueID);
        }
        return null;
    }

    private void sendConversionTagInternal(final Context context, String str, final boolean z) {
        if (!str.equals(this.conversionTag)) {
            this.conversionTag = str;
            this.conversionTagSent = false;
        }
        if (!this.conversionTagSent) {
            new Thread() {
                public void run() {
                    try {
                        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
                        Uri.Builder builder = new Uri.Builder();
                        HashMap unused = AdManager.this.defaultRequestBuilder(context);
                        AdManager.this.setUriParamsFromMap(builder, AdManager.this.defaultParamMap);
                        builder.scheme("http");
                        if (z) {
                            builder.authority(AdManager.this.conversionTagTestHost);
                            builder.path("/ctt");
                        } else {
                            builder.authority(AdManager.this.conversionTagHost);
                            builder.path("/sp/conv");
                        }
                        builder.appendQueryParameter("tid", AdManager.this.conversionTag);
                        builder.appendQueryParameter("output", "js");
                        String builder2 = builder.toString();
                        Log.d(AdManager.DEBUG_TASK, "sendConversionTag url=" + builder2);
                        int statusCode = defaultHttpClient.execute(new HttpGet(builder2)).getStatusLine().getStatusCode();
                        if (statusCode >= 300 && statusCode < 501) {
                            boolean unused2 = AdManager.this.conversionTagSent = true;
                        }
                    } catch (MalformedURLException e) {
                        Log.e(AdManager.DEBUG_TASK, "sendConversionTag exception=" + e.toString());
                    } catch (IOException e2) {
                        Log.e(AdManager.DEBUG_TASK, "sendConversionTag exception=" + e2.toString());
                    }
                }
            }.start();
        }
    }

    /* access modifiers changed from: private */
    public void setUriParamsFromMap(Uri.Builder builder, Map<String, String> map) {
        if (map != null) {
            for (Map.Entry next : map.entrySet()) {
                builder.appendQueryParameter((String) next.getKey(), (String) next.getValue());
            }
        }
    }

    private String uniqueID(Context context) {
        try {
            return ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        } catch (Exception e) {
            Log.e(DEBUG_TASK, e.toString());
            return null;
        }
    }

    public AdlantisAd[] adsForOrientation(int i) {
        return i == 2 ? filteredAdsForOrientation(2) : filteredAdsForOrientation(1);
    }

    public AsyncImageLoader asyncImageLoader() {
        return this.asyncImageLoader;
    }

    public void connect(final Context context, final AdManagerCallback adManagerCallback) {
        if (!this.connectionChangeReceiverRegistered) {
            context.getApplicationContext().registerReceiver(new ConnectionChangeReceiver(), new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
            this.connectionChangeReceiverRegistered = true;
        }
        final AnonymousClass3 r0 = new Handler() {
            public void handleMessage(Message message) {
                if (adManagerCallback != null) {
                    adManagerCallback.adsLoaded(AdManager.this);
                }
            }
        };
        new Thread() {
            public void run() {
                boolean unused = AdManager.this.doAdRequest(context, adManagerCallback);
                r0.sendMessage(r0.obtainMessage(0, this));
            }
        }.start();
    }

    public String getPublisherID() {
        return this.publisherID;
    }

    public void handleClickRequest(String str, AdManagerClickUrlProcessedCallback adManagerClickUrlProcessedCallback) {
        Uri parse = Uri.parse(str);
        String scheme = parse.getScheme();
        if (scheme.compareTo("http") == 0 || scheme.compareTo("https") == 0) {
            handleHttpClickRequest(str, adManagerClickUrlProcessedCallback);
            return;
        }
        Handler handler = new Handler();
        Message obtainMessage = handler.obtainMessage(0, parse);
        handler.sendMessage(obtainMessage);
        if (adManagerClickUrlProcessedCallback != null) {
            adManagerClickUrlProcessedCallback.clickProcessed((Uri) obtainMessage.obj);
        }
    }

    public String sdkBuild() {
        return "109";
    }

    public String sdkVersion() {
        return SDKVersion.VERSION;
    }

    public void sendConversionTag(Context context, String str) {
        sendConversionTagInternal(context, str, false);
    }

    public void sendConversionTagTest(Context context, String str) {
        sendConversionTagInternal(context, str, true);
    }

    public void sendImpressionCountForAd(final AdlantisAd adlantisAd) {
        if (!adlantisAd.sentImpressionCount() && !adlantisAd.sendingImpressionCount()) {
            new Thread() {
                /* JADX WARNING: Removed duplicated region for block: B:10:0x006f  */
                /* JADX WARNING: Removed duplicated region for block: B:20:0x00f3  */
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public void run() {
                    /*
                        r7 = this;
                        r6 = 0
                        r5 = 1
                        java.lang.String r0 = jp.Adlantis.Android.AdManager.DEBUG_TASK
                        java.lang.StringBuilder r1 = new java.lang.StringBuilder
                        r1.<init>()
                        java.lang.String r2 = "sendImpressionCountForAd ad="
                        java.lang.StringBuilder r1 = r1.append(r2)
                        jp.Adlantis.Android.AdlantisAd r2 = r2
                        java.lang.StringBuilder r1 = r1.append(r2)
                        java.lang.String r1 = r1.toString()
                        android.util.Log.d(r0, r1)
                        jp.Adlantis.Android.AdlantisAd r0 = r2
                        r0.setSendingImpressionCount(r5)
                        org.apache.http.impl.client.DefaultHttpClient r0 = new org.apache.http.impl.client.DefaultHttpClient     // Catch:{ MalformedURLException -> 0x0091, IOException -> 0x00b2, OutOfMemoryError -> 0x00d3 }
                        r0.<init>()     // Catch:{ MalformedURLException -> 0x0091, IOException -> 0x00b2, OutOfMemoryError -> 0x00d3 }
                        jp.Adlantis.Android.AdManager r1 = jp.Adlantis.Android.AdManager.this     // Catch:{ MalformedURLException -> 0x0091, IOException -> 0x00b2, OutOfMemoryError -> 0x00d3 }
                        jp.Adlantis.Android.AdlantisAd r2 = r2     // Catch:{ MalformedURLException -> 0x0091, IOException -> 0x00b2, OutOfMemoryError -> 0x00d3 }
                        android.net.Uri r1 = r1.impressionCountURI(r2)     // Catch:{ MalformedURLException -> 0x0091, IOException -> 0x00b2, OutOfMemoryError -> 0x00d3 }
                        java.lang.String r1 = r1.toString()     // Catch:{ MalformedURLException -> 0x0091, IOException -> 0x00b2, OutOfMemoryError -> 0x00d3 }
                        java.lang.String r2 = jp.Adlantis.Android.AdManager.DEBUG_TASK     // Catch:{ MalformedURLException -> 0x0091, IOException -> 0x00b2, OutOfMemoryError -> 0x00d3 }
                        java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0091, IOException -> 0x00b2, OutOfMemoryError -> 0x00d3 }
                        r3.<init>()     // Catch:{ MalformedURLException -> 0x0091, IOException -> 0x00b2, OutOfMemoryError -> 0x00d3 }
                        java.lang.String r4 = "sendImpressionCountForAd url="
                        java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ MalformedURLException -> 0x0091, IOException -> 0x00b2, OutOfMemoryError -> 0x00d3 }
                        java.lang.StringBuilder r3 = r3.append(r1)     // Catch:{ MalformedURLException -> 0x0091, IOException -> 0x00b2, OutOfMemoryError -> 0x00d3 }
                        java.lang.String r3 = r3.toString()     // Catch:{ MalformedURLException -> 0x0091, IOException -> 0x00b2, OutOfMemoryError -> 0x00d3 }
                        android.util.Log.d(r2, r3)     // Catch:{ MalformedURLException -> 0x0091, IOException -> 0x00b2, OutOfMemoryError -> 0x00d3 }
                        org.apache.http.client.methods.HttpGet r2 = new org.apache.http.client.methods.HttpGet     // Catch:{ MalformedURLException -> 0x0091, IOException -> 0x00b2, OutOfMemoryError -> 0x00d3 }
                        r2.<init>(r1)     // Catch:{ MalformedURLException -> 0x0091, IOException -> 0x00b2, OutOfMemoryError -> 0x00d3 }
                        org.apache.http.HttpResponse r0 = r0.execute(r2)     // Catch:{ MalformedURLException -> 0x0091, IOException -> 0x00b2, OutOfMemoryError -> 0x00d3 }
                        org.apache.http.StatusLine r0 = r0.getStatusLine()     // Catch:{ MalformedURLException -> 0x0091, IOException -> 0x00b2, OutOfMemoryError -> 0x00d3 }
                        int r0 = r0.getStatusCode()     // Catch:{ MalformedURLException -> 0x0091, IOException -> 0x00b2, OutOfMemoryError -> 0x00d3 }
                        r1 = 200(0xc8, float:2.8E-43)
                        if (r0 < r1) goto L_0x0075
                        r1 = 400(0x190, float:5.6E-43)
                        if (r0 >= r1) goto L_0x0075
                        r0 = r5
                    L_0x0068:
                        jp.Adlantis.Android.AdlantisAd r1 = r2
                        r1.setSendingImpressionCount(r6)
                        if (r0 == 0) goto L_0x00f3
                        jp.Adlantis.Android.AdlantisAd r0 = r2
                        r0.setSentImpressionCount(r5)
                    L_0x0074:
                        return
                    L_0x0075:
                        java.lang.String r1 = jp.Adlantis.Android.AdManager.DEBUG_TASK     // Catch:{ MalformedURLException -> 0x0091, IOException -> 0x00b2, OutOfMemoryError -> 0x00d3 }
                        java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0091, IOException -> 0x00b2, OutOfMemoryError -> 0x00d3 }
                        r2.<init>()     // Catch:{ MalformedURLException -> 0x0091, IOException -> 0x00b2, OutOfMemoryError -> 0x00d3 }
                        java.lang.String r3 = "sendImpressionCountForAd status="
                        java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ MalformedURLException -> 0x0091, IOException -> 0x00b2, OutOfMemoryError -> 0x00d3 }
                        java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ MalformedURLException -> 0x0091, IOException -> 0x00b2, OutOfMemoryError -> 0x00d3 }
                        java.lang.String r0 = r0.toString()     // Catch:{ MalformedURLException -> 0x0091, IOException -> 0x00b2, OutOfMemoryError -> 0x00d3 }
                        android.util.Log.e(r1, r0)     // Catch:{ MalformedURLException -> 0x0091, IOException -> 0x00b2, OutOfMemoryError -> 0x00d3 }
                    L_0x008f:
                        r0 = r6
                        goto L_0x0068
                    L_0x0091:
                        r0 = move-exception
                        java.lang.String r1 = jp.Adlantis.Android.AdManager.DEBUG_TASK
                        java.lang.StringBuilder r2 = new java.lang.StringBuilder
                        r2.<init>()
                        java.lang.String r3 = "sendImpressionCountForAd exception="
                        java.lang.StringBuilder r2 = r2.append(r3)
                        java.lang.String r0 = r0.toString()
                        java.lang.StringBuilder r0 = r2.append(r0)
                        java.lang.String r0 = r0.toString()
                        android.util.Log.e(r1, r0)
                        r0 = r6
                        goto L_0x0068
                    L_0x00b2:
                        r0 = move-exception
                        java.lang.String r1 = jp.Adlantis.Android.AdManager.DEBUG_TASK
                        java.lang.StringBuilder r2 = new java.lang.StringBuilder
                        r2.<init>()
                        java.lang.String r3 = "sendImpressionCountForAd exception="
                        java.lang.StringBuilder r2 = r2.append(r3)
                        java.lang.String r0 = r0.toString()
                        java.lang.StringBuilder r0 = r2.append(r0)
                        java.lang.String r0 = r0.toString()
                        android.util.Log.e(r1, r0)
                        r0 = r6
                        goto L_0x0068
                    L_0x00d3:
                        r0 = move-exception
                        java.lang.String r1 = jp.Adlantis.Android.AdManager.DEBUG_TASK
                        java.lang.StringBuilder r2 = new java.lang.StringBuilder
                        r2.<init>()
                        java.lang.String r3 = "sendImpressionCountForAd OutOfMemoryError="
                        java.lang.StringBuilder r2 = r2.append(r3)
                        java.lang.String r0 = r0.toString()
                        java.lang.StringBuilder r0 = r2.append(r0)
                        java.lang.String r0 = r0.toString()
                        android.util.Log.e(r1, r0)
                        goto L_0x008f
                    L_0x00f3:
                        if (r6 == 0) goto L_0x0102
                        jp.Adlantis.Android.AdManager r0 = jp.Adlantis.Android.AdManager.this
                        java.util.LinkedList r0 = r0.impressionCountQueue
                        jp.Adlantis.Android.AdlantisAd r1 = r2
                        r0.add(r1)
                        goto L_0x0074
                    L_0x0102:
                        jp.Adlantis.Android.AdlantisAd r0 = r2
                        r0.setSendImpressionCountFailed(r5)
                        goto L_0x0074
                    */
                    throw new UnsupportedOperationException("Method not decompiled: jp.Adlantis.Android.AdManager.AnonymousClass5.run():void");
                }
            }.start();
        }
    }

    public void setKeywords(String str) {
        this.keywords = str;
    }

    public void setPublisherID(String str) {
        this.publisherID = str;
    }
}
