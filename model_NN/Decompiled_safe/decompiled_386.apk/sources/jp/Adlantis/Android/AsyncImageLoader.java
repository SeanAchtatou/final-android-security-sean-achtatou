package jp.Adlantis.Android;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.SoftReference;
import java.net.URL;
import java.util.HashMap;

public class AsyncImageLoader {
    /* access modifiers changed from: private */
    public HashMap<String, SoftReference<Drawable>> imageCache = new HashMap<>();

    public interface BitmapLoadedCallback {
        void bitmapLoaded(Bitmap bitmap, String str);
    }

    public interface ImageCallback {
        void imageLoaded(Drawable drawable, String str);
    }

    public interface ImageLoadedCallback {
        void imageLoaded(Drawable drawable, String str);
    }

    public static Drawable loadImageFromUrl(String str) {
        InputStream inputStream;
        Log.d("AsyncImageLoader", "loadImageFromUrl=" + str);
        if (str == null) {
            return null;
        }
        try {
            inputStream = new URL(str).openStream();
        } catch (IOException e) {
            System.out.println(e);
            inputStream = null;
        }
        if (inputStream == null) {
            return null;
        }
        try {
            return Drawable.createFromStream(inputStream, "src");
        } catch (OutOfMemoryError e2) {
            Log.e("AsyncImageLoader", "exception calling Drawable.createFromStream() " + e2);
            return null;
        }
    }

    public Drawable loadDrawable(final String str, final ImageLoadedCallback imageLoadedCallback) {
        Drawable drawable;
        if (this.imageCache.containsKey(str) && (drawable = (Drawable) this.imageCache.get(str).get()) != null) {
            return drawable;
        }
        final AnonymousClass1 r0 = new Handler() {
            public void handleMessage(Message message) {
                if (imageLoadedCallback != null) {
                    imageLoadedCallback.imageLoaded((Drawable) message.obj, str);
                }
            }
        };
        new Thread() {
            public void run() {
                Drawable loadImageFromUrl = AsyncImageLoader.loadImageFromUrl(str);
                if (loadImageFromUrl != null) {
                    AsyncImageLoader.this.imageCache.put(str, new SoftReference(loadImageFromUrl));
                    Log.d("AsyncImageLoader", "imageCache.size()=" + AsyncImageLoader.this.imageCache.size());
                    r0.sendMessage(r0.obtainMessage(0, loadImageFromUrl));
                }
            }
        }.start();
        return null;
    }
}
