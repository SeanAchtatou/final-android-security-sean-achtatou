package com.xcc.DreamPuzzle3;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Vibrator;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.RatingBar;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.google.ads.AdRequest;
import com.google.ads.AdView;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class GameActivity extends Activity {
    /* access modifiers changed from: private */
    public final int GROUP_LENGTH;
    private final int ITEM_BACK;
    private final int ITEM_NEXT;
    private final int ITEM_ORGPICTURE;
    private final int ITEM_RESTART;
    /* access modifiers changed from: private */
    public AdView adView;
    /* access modifiers changed from: private */
    public Bitmap bitmap0;
    /* access modifiers changed from: private */
    public Bitmap bitmap1;
    /* access modifiers changed from: private */
    public Bitmap bitmap10;
    /* access modifiers changed from: private */
    public Bitmap bitmap11;
    /* access modifiers changed from: private */
    public Bitmap bitmap12;
    /* access modifiers changed from: private */
    public Bitmap bitmap13;
    /* access modifiers changed from: private */
    public Bitmap bitmap14;
    /* access modifiers changed from: private */
    public Bitmap bitmap15;
    /* access modifiers changed from: private */
    public Bitmap bitmap16;
    /* access modifiers changed from: private */
    public Bitmap bitmap17;
    /* access modifiers changed from: private */
    public Bitmap bitmap18;
    /* access modifiers changed from: private */
    public Bitmap bitmap19;
    /* access modifiers changed from: private */
    public Bitmap bitmap2;
    /* access modifiers changed from: private */
    public Bitmap bitmap20;
    /* access modifiers changed from: private */
    public Bitmap bitmap21;
    /* access modifiers changed from: private */
    public Bitmap bitmap22;
    /* access modifiers changed from: private */
    public Bitmap bitmap23;
    /* access modifiers changed from: private */
    public Bitmap bitmap24;
    /* access modifiers changed from: private */
    public Bitmap bitmap3;
    /* access modifiers changed from: private */
    public Bitmap bitmap4;
    /* access modifiers changed from: private */
    public Bitmap bitmap5;
    /* access modifiers changed from: private */
    public Bitmap bitmap6;
    /* access modifiers changed from: private */
    public Bitmap bitmap7;
    /* access modifiers changed from: private */
    public Bitmap bitmap8;
    /* access modifiers changed from: private */
    public Bitmap bitmap9;
    /* access modifiers changed from: private */
    public boolean clickable;
    /* access modifiers changed from: private */
    public int config = -1;
    private int[] correctArray;
    /* access modifiers changed from: private */
    public CountTime countTime;
    /* access modifiers changed from: private */
    public int[] createdArray;
    /* access modifiers changed from: private */
    public boolean flag = false;
    /* access modifiers changed from: private */
    public Handler handler = new Handler();
    /* access modifiers changed from: private */
    public ImageView imageView0;
    /* access modifiers changed from: private */
    public ImageView imageView1;
    /* access modifiers changed from: private */
    public ImageView imageView10;
    /* access modifiers changed from: private */
    public ImageView imageView11;
    /* access modifiers changed from: private */
    public ImageView imageView12;
    /* access modifiers changed from: private */
    public ImageView imageView13;
    /* access modifiers changed from: private */
    public ImageView imageView14;
    /* access modifiers changed from: private */
    public ImageView imageView15;
    /* access modifiers changed from: private */
    public ImageView imageView16;
    /* access modifiers changed from: private */
    public ImageView imageView17;
    /* access modifiers changed from: private */
    public ImageView imageView18;
    /* access modifiers changed from: private */
    public ImageView imageView19;
    /* access modifiers changed from: private */
    public ImageView imageView2;
    /* access modifiers changed from: private */
    public ImageView imageView20;
    /* access modifiers changed from: private */
    public ImageView imageView21;
    /* access modifiers changed from: private */
    public ImageView imageView22;
    /* access modifiers changed from: private */
    public ImageView imageView23;
    /* access modifiers changed from: private */
    public ImageView imageView24;
    /* access modifiers changed from: private */
    public ImageView imageView3;
    /* access modifiers changed from: private */
    public ImageView imageView4;
    /* access modifiers changed from: private */
    public ImageView imageView5;
    /* access modifiers changed from: private */
    public ImageView imageView6;
    /* access modifiers changed from: private */
    public ImageView imageView7;
    /* access modifiers changed from: private */
    public ImageView imageView8;
    /* access modifiers changed from: private */
    public ImageView imageView9;
    private Intent intent;
    /* access modifiers changed from: private */
    public int level;
    private TextView levels;
    boolean mVibrat;
    boolean mVol;
    /* access modifiers changed from: private */
    public MediaPlayer mediaPlayer;
    /* access modifiers changed from: private */
    public int minutes;
    private TextView moves;
    private GridView myGridView;
    public long[] patten;
    /* access modifiers changed from: private */
    public int[] pictureArray;
    /* access modifiers changed from: private */
    public int reduceChance;
    private int score;
    /* access modifiers changed from: private */
    public int seconds;
    private int steps;
    /* access modifiers changed from: private */
    public int temp = -1;
    /* access modifiers changed from: private */
    public Bitmap tempBitmap;
    /* access modifiers changed from: private */
    public TextView times;
    /* access modifiers changed from: private */
    public int unlock;
    /* access modifiers changed from: private */
    public Vibrator vibrator;

    public GameActivity() {
        int[] iArr = new int[25];
        iArr[1] = 1;
        iArr[2] = 2;
        iArr[3] = 3;
        iArr[4] = 4;
        iArr[5] = 5;
        iArr[6] = 6;
        iArr[7] = 7;
        iArr[8] = 8;
        iArr[9] = 9;
        iArr[10] = 10;
        iArr[11] = 11;
        iArr[12] = 12;
        iArr[13] = 13;
        iArr[14] = 14;
        iArr[15] = 15;
        iArr[16] = 16;
        iArr[17] = 17;
        iArr[18] = 18;
        iArr[19] = 19;
        iArr[20] = 20;
        iArr[21] = 21;
        iArr[22] = 22;
        iArr[23] = 23;
        iArr[24] = 24;
        this.correctArray = iArr;
        this.level = 0;
        this.steps = 0;
        this.minutes = 0;
        this.seconds = 0;
        this.reduceChance = 5;
        this.unlock = 1;
        this.score = 0;
        this.clickable = true;
        this.ITEM_BACK = 0;
        this.ITEM_ORGPICTURE = 1;
        this.ITEM_RESTART = 2;
        this.ITEM_NEXT = 3;
        this.GROUP_LENGTH = DateContent.PICTURE_GROUP.length;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        requestWindowFeature(1);
        this.vibrator = (Vibrator) getSystemService("vibrator");
        SharedPreferences getData = getSharedPreferences("DreamPuzzle", 0);
        this.mVol = getData.getBoolean("soundEnable", true);
        if (this.mVol) {
            this.mediaPlayer = MediaPlayer.create(this, (int) R.raw.sound_hint);
            this.mediaPlayer.setLooping(false);
        }
        this.mVibrat = getData.getBoolean("vibratEnable", true);
        if (this.mVibrat) {
            long[] jArr = new long[4];
            jArr[0] = 100;
            jArr[1] = 50;
            jArr[2] = 50;
            this.patten = jArr;
        } else {
            long[] jArr2 = new long[4];
            jArr2[0] = 100;
            jArr2[2] = 100;
            this.patten = jArr2;
        }
        this.unlock = getData.getInt("levels", 1);
        this.intent = getIntent();
        this.level = this.intent.getIntExtra("level", 0) + 1;
        this.pictureArray = this.intent.getIntArrayExtra("pictureArray");
        this.countTime = new CountTime();
        defineLayout();
    }

    /* access modifiers changed from: private */
    public void defineLayout() {
        if (this.level <= (this.GROUP_LENGTH * 1) / 5) {
            setContentView((int) R.layout.level_one);
        } else if (this.level > (this.GROUP_LENGTH * 1) / 5 && this.level <= (this.GROUP_LENGTH * 2) / 5) {
            setContentView((int) R.layout.level_two);
        } else if (this.level > (this.GROUP_LENGTH * 2) / 5 && this.level <= (this.GROUP_LENGTH * 3) / 5) {
            setContentView((int) R.layout.level_three);
        } else if (this.level > (this.GROUP_LENGTH * 3) / 5 && this.level <= (this.GROUP_LENGTH * 4) / 5) {
            setContentView((int) R.layout.level_four);
        } else if (this.level > (this.GROUP_LENGTH * 4) / 5 && this.level <= this.GROUP_LENGTH) {
            setContentView((int) R.layout.level_five);
        }
        if (this.level <= this.GROUP_LENGTH) {
            this.imageView0 = (ImageView) findViewById(R.id.second0);
            this.imageView1 = (ImageView) findViewById(R.id.second1);
            this.imageView2 = (ImageView) findViewById(R.id.second2);
            this.imageView3 = (ImageView) findViewById(R.id.second3);
            this.imageView4 = (ImageView) findViewById(R.id.second4);
            this.imageView5 = (ImageView) findViewById(R.id.second5);
            this.imageView6 = (ImageView) findViewById(R.id.second6);
            this.imageView7 = (ImageView) findViewById(R.id.second7);
            this.imageView8 = (ImageView) findViewById(R.id.second8);
            this.imageView0.setOnClickListener(new View0());
            this.imageView1.setOnClickListener(new View1());
            this.imageView2.setOnClickListener(new View2());
            this.imageView3.setOnClickListener(new View3());
            this.imageView4.setOnClickListener(new View4());
            this.imageView5.setOnClickListener(new View5());
            this.imageView6.setOnClickListener(new View6());
            this.imageView7.setOnClickListener(new View7());
            this.imageView8.setOnClickListener(new View8());
        }
        if (this.level > (this.GROUP_LENGTH * 1) / 5 && this.level <= this.GROUP_LENGTH) {
            this.imageView9 = (ImageView) findViewById(R.id.second9);
            this.imageView10 = (ImageView) findViewById(R.id.second10);
            this.imageView11 = (ImageView) findViewById(R.id.second11);
            this.imageView9.setOnClickListener(new View9());
            this.imageView10.setOnClickListener(new View10());
            this.imageView11.setOnClickListener(new View11());
        }
        if (this.level > (this.GROUP_LENGTH * 2) / 5 && this.level <= this.GROUP_LENGTH) {
            this.imageView12 = (ImageView) findViewById(R.id.second12);
            this.imageView13 = (ImageView) findViewById(R.id.second13);
            this.imageView14 = (ImageView) findViewById(R.id.second14);
            this.imageView15 = (ImageView) findViewById(R.id.second15);
            this.imageView12.setOnClickListener(new View12());
            this.imageView13.setOnClickListener(new View13());
            this.imageView14.setOnClickListener(new View14());
            this.imageView15.setOnClickListener(new View15());
        }
        if (this.level > (this.GROUP_LENGTH * 3) / 5 && this.level <= this.GROUP_LENGTH) {
            this.imageView16 = (ImageView) findViewById(R.id.second16);
            this.imageView17 = (ImageView) findViewById(R.id.second17);
            this.imageView18 = (ImageView) findViewById(R.id.second18);
            this.imageView19 = (ImageView) findViewById(R.id.second19);
            this.imageView16.setOnClickListener(new View16());
            this.imageView17.setOnClickListener(new View17());
            this.imageView18.setOnClickListener(new View18());
            this.imageView19.setOnClickListener(new View19());
        }
        if (this.level > (this.GROUP_LENGTH * 4) / 5 && this.level <= this.GROUP_LENGTH) {
            this.imageView20 = (ImageView) findViewById(R.id.second20);
            this.imageView21 = (ImageView) findViewById(R.id.second21);
            this.imageView22 = (ImageView) findViewById(R.id.second22);
            this.imageView23 = (ImageView) findViewById(R.id.second23);
            this.imageView24 = (ImageView) findViewById(R.id.second24);
            this.imageView20.setOnClickListener(new View20());
            this.imageView21.setOnClickListener(new View21());
            this.imageView22.setOnClickListener(new View22());
            this.imageView23.setOnClickListener(new View23());
            this.imageView24.setOnClickListener(new View24());
        }
        this.levels = (TextView) findViewById(R.id.levels);
        this.moves = (TextView) findViewById(R.id.moves);
        this.times = (TextView) findViewById(R.id.times);
        this.adView = (AdView) findViewById(R.id.adView);
        restart();
        this.myGridView = (GridView) findViewById(R.id.GridView_toolbar);
        this.myGridView.setSelected(true);
        this.myGridView.setAdapter((ListAdapter) getMenuAdapter(new int[]{R.drawable.levelprev, R.drawable.levelviewpic, R.drawable.levelrestart, R.drawable.levelnext}));
        this.myGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                switch (arg2) {
                    case 0:
                        GameActivity.this.handler.removeCallbacks(GameActivity.this.countTime);
                        GameActivity.this.seconds = 0;
                        GameActivity.this.minutes = 0;
                        if (GameActivity.this.level == 1) {
                            GameActivity.this.toastShow(0);
                        } else {
                            GameActivity gameActivity = GameActivity.this;
                            gameActivity.level = gameActivity.level - 1;
                            if (GameActivity.this.level % (GameActivity.this.GROUP_LENGTH / 5) == 0) {
                                GameActivity.this.defineLayout();
                                return;
                            }
                            GameActivity.this.restart();
                        }
                        Log.d("tag", "level =" + GameActivity.this.level);
                        return;
                    case 1:
                        GameActivity gameActivity2 = GameActivity.this;
                        int access$103 = gameActivity2.reduceChance;
                        gameActivity2.reduceChance = access$103 - 1;
                        if (access$103 <= 0) {
                            GameActivity.this.toastShow(1);
                            return;
                        }
                        GameActivity.this.adView.loadAd(new AdRequest());
                        GameActivity.this.showDiaglo();
                        Log.d("tag", "level =" + GameActivity.this.level);
                        return;
                    case 2:
                        if (GameActivity.this.clickable) {
                            GameActivity.this.handler.removeCallbacks(GameActivity.this.countTime);
                            GameActivity.this.seconds = 0;
                            GameActivity.this.minutes = 0;
                            GameActivity.this.restart();
                            Log.d("tag", "level =" + GameActivity.this.level);
                            return;
                        }
                        return;
                    case 3:
                        if (GameActivity.this.level == GameActivity.this.GROUP_LENGTH) {
                            GameActivity.this.toastShow(3);
                            return;
                        } else if (GameActivity.this.level == GameActivity.this.unlock) {
                            GameActivity.this.toastShow(4);
                            return;
                        } else {
                            GameActivity.this.handler.removeCallbacks(GameActivity.this.countTime);
                            GameActivity.this.seconds = 0;
                            GameActivity.this.minutes = 0;
                            if (GameActivity.this.level == GameActivity.this.GROUP_LENGTH) {
                                GameActivity.this.toastShow(3);
                                return;
                            } else if (GameActivity.this.level % (GameActivity.this.GROUP_LENGTH / 5) == 0) {
                                GameActivity gameActivity3 = GameActivity.this;
                                gameActivity3.level = gameActivity3.level + 1;
                                GameActivity.this.defineLayout();
                                return;
                            } else {
                                GameActivity gameActivity4 = GameActivity.this;
                                gameActivity4.level = gameActivity4.level + 1;
                                GameActivity.this.restart();
                                Log.d("tag", "level =" + GameActivity.this.level);
                                return;
                            }
                        }
                    default:
                        return;
                }
            }
        });
    }

    class View0 implements View.OnClickListener {
        View0() {
        }

        public void onClick(View v) {
            if (GameActivity.this.clickable) {
                if (!GameActivity.this.flag) {
                    GameActivity.this.tempBitmap = GameActivity.this.bitmap0;
                    GameActivity.this.temp = GameActivity.this.createdArray[0];
                    if (GameActivity.this.mVol && GameActivity.this.mediaPlayer != null) {
                        GameActivity.this.mediaPlayer.start();
                    }
                    GameActivity.this.flag = true;
                    GameActivity.this.config = 0;
                    GameActivity.this.imageView0.setImageBitmap(GameActivity.this.getFocuse(GameActivity.this.bitmap0));
                    return;
                }
                GameActivity.this.whichBeChanged(GameActivity.this.config, GameActivity.this.bitmap0, GameActivity.this.createdArray[0]);
                GameActivity.this.bitmap0 = GameActivity.this.tempBitmap;
                GameActivity.this.imageView0.setImageBitmap(GameActivity.this.bitmap0);
                GameActivity.this.vibrator.vibrate(GameActivity.this.patten, -1);
                GameActivity.this.createdArray[0] = GameActivity.this.temp;
                GameActivity.this.temp = -1;
                GameActivity.this.tempBitmap = null;
                GameActivity.this.flag = false;
                GameActivity.this.contrast();
            }
        }
    }

    class View1 implements View.OnClickListener {
        View1() {
        }

        public void onClick(View v) {
            if (GameActivity.this.clickable) {
                if (!GameActivity.this.flag) {
                    GameActivity.this.tempBitmap = GameActivity.this.bitmap1;
                    GameActivity.this.temp = GameActivity.this.createdArray[1];
                    if (GameActivity.this.mVol && GameActivity.this.mediaPlayer != null) {
                        GameActivity.this.mediaPlayer.start();
                    }
                    GameActivity.this.flag = true;
                    GameActivity.this.config = 1;
                    GameActivity.this.imageView1.setImageBitmap(GameActivity.this.getFocuse(GameActivity.this.bitmap1));
                    return;
                }
                GameActivity.this.whichBeChanged(GameActivity.this.config, GameActivity.this.bitmap1, GameActivity.this.createdArray[1]);
                GameActivity.this.bitmap1 = GameActivity.this.tempBitmap;
                GameActivity.this.imageView1.setImageBitmap(GameActivity.this.bitmap1);
                GameActivity.this.vibrator.vibrate(GameActivity.this.patten, -1);
                GameActivity.this.createdArray[1] = GameActivity.this.temp;
                GameActivity.this.temp = -1;
                GameActivity.this.tempBitmap = null;
                GameActivity.this.flag = false;
                GameActivity.this.contrast();
            }
        }
    }

    class View2 implements View.OnClickListener {
        View2() {
        }

        public void onClick(View v) {
            if (GameActivity.this.clickable) {
                if (!GameActivity.this.flag) {
                    GameActivity.this.tempBitmap = GameActivity.this.bitmap2;
                    GameActivity.this.temp = GameActivity.this.createdArray[2];
                    if (GameActivity.this.mVol && GameActivity.this.mediaPlayer != null) {
                        GameActivity.this.mediaPlayer.start();
                    }
                    GameActivity.this.flag = true;
                    GameActivity.this.config = 2;
                    GameActivity.this.imageView2.setImageBitmap(GameActivity.this.getFocuse(GameActivity.this.bitmap2));
                    return;
                }
                GameActivity.this.whichBeChanged(GameActivity.this.config, GameActivity.this.bitmap2, GameActivity.this.createdArray[2]);
                GameActivity.this.bitmap2 = GameActivity.this.tempBitmap;
                GameActivity.this.imageView2.setImageBitmap(GameActivity.this.bitmap2);
                GameActivity.this.vibrator.vibrate(GameActivity.this.patten, -1);
                GameActivity.this.createdArray[2] = GameActivity.this.temp;
                GameActivity.this.temp = -1;
                GameActivity.this.tempBitmap = null;
                GameActivity.this.flag = false;
                GameActivity.this.contrast();
            }
        }
    }

    class View3 implements View.OnClickListener {
        View3() {
        }

        public void onClick(View v) {
            if (GameActivity.this.clickable) {
                if (!GameActivity.this.flag) {
                    GameActivity.this.tempBitmap = GameActivity.this.bitmap3;
                    GameActivity.this.temp = GameActivity.this.createdArray[3];
                    if (GameActivity.this.mVol && GameActivity.this.mediaPlayer != null) {
                        GameActivity.this.mediaPlayer.start();
                    }
                    GameActivity.this.flag = true;
                    GameActivity.this.config = 3;
                    GameActivity.this.imageView3.setImageBitmap(GameActivity.this.getFocuse(GameActivity.this.bitmap3));
                    return;
                }
                GameActivity.this.whichBeChanged(GameActivity.this.config, GameActivity.this.bitmap3, GameActivity.this.createdArray[3]);
                GameActivity.this.bitmap3 = GameActivity.this.tempBitmap;
                GameActivity.this.imageView3.setImageBitmap(GameActivity.this.bitmap3);
                GameActivity.this.vibrator.vibrate(GameActivity.this.patten, -1);
                GameActivity.this.createdArray[3] = GameActivity.this.temp;
                GameActivity.this.temp = -1;
                GameActivity.this.tempBitmap = null;
                GameActivity.this.flag = false;
                GameActivity.this.contrast();
            }
        }
    }

    class View4 implements View.OnClickListener {
        View4() {
        }

        public void onClick(View v) {
            if (GameActivity.this.clickable) {
                if (!GameActivity.this.flag) {
                    GameActivity.this.tempBitmap = GameActivity.this.bitmap4;
                    GameActivity.this.temp = GameActivity.this.createdArray[4];
                    if (GameActivity.this.mVol && GameActivity.this.mediaPlayer != null) {
                        GameActivity.this.mediaPlayer.start();
                    }
                    GameActivity.this.flag = true;
                    GameActivity.this.config = 4;
                    GameActivity.this.imageView4.setImageBitmap(GameActivity.this.getFocuse(GameActivity.this.bitmap4));
                    return;
                }
                GameActivity.this.whichBeChanged(GameActivity.this.config, GameActivity.this.bitmap4, GameActivity.this.createdArray[4]);
                GameActivity.this.bitmap4 = GameActivity.this.tempBitmap;
                GameActivity.this.imageView4.setImageBitmap(GameActivity.this.bitmap4);
                GameActivity.this.vibrator.vibrate(GameActivity.this.patten, -1);
                GameActivity.this.createdArray[4] = GameActivity.this.temp;
                GameActivity.this.temp = -1;
                GameActivity.this.tempBitmap = null;
                GameActivity.this.flag = false;
                GameActivity.this.contrast();
            }
        }
    }

    class View5 implements View.OnClickListener {
        View5() {
        }

        public void onClick(View v) {
            if (GameActivity.this.clickable) {
                if (!GameActivity.this.flag) {
                    GameActivity.this.tempBitmap = GameActivity.this.bitmap5;
                    GameActivity.this.temp = GameActivity.this.createdArray[5];
                    if (GameActivity.this.mVol && GameActivity.this.mediaPlayer != null) {
                        GameActivity.this.mediaPlayer.start();
                    }
                    GameActivity.this.flag = true;
                    GameActivity.this.config = 5;
                    GameActivity.this.imageView5.setImageBitmap(GameActivity.this.getFocuse(GameActivity.this.bitmap5));
                    return;
                }
                GameActivity.this.whichBeChanged(GameActivity.this.config, GameActivity.this.bitmap5, GameActivity.this.createdArray[5]);
                GameActivity.this.bitmap5 = GameActivity.this.tempBitmap;
                GameActivity.this.imageView5.setImageBitmap(GameActivity.this.bitmap5);
                GameActivity.this.vibrator.vibrate(GameActivity.this.patten, -1);
                GameActivity.this.createdArray[5] = GameActivity.this.temp;
                GameActivity.this.temp = -1;
                GameActivity.this.tempBitmap = null;
                GameActivity.this.flag = false;
                GameActivity.this.contrast();
            }
        }
    }

    class View6 implements View.OnClickListener {
        View6() {
        }

        public void onClick(View v) {
            if (GameActivity.this.clickable) {
                if (!GameActivity.this.flag) {
                    GameActivity.this.tempBitmap = GameActivity.this.bitmap6;
                    GameActivity.this.temp = GameActivity.this.createdArray[6];
                    if (GameActivity.this.mVol && GameActivity.this.mediaPlayer != null) {
                        GameActivity.this.mediaPlayer.start();
                    }
                    GameActivity.this.flag = true;
                    GameActivity.this.config = 6;
                    GameActivity.this.imageView6.setImageBitmap(GameActivity.this.getFocuse(GameActivity.this.bitmap6));
                    return;
                }
                GameActivity.this.whichBeChanged(GameActivity.this.config, GameActivity.this.bitmap6, GameActivity.this.createdArray[6]);
                GameActivity.this.bitmap6 = GameActivity.this.tempBitmap;
                GameActivity.this.imageView6.setImageBitmap(GameActivity.this.bitmap6);
                GameActivity.this.vibrator.vibrate(GameActivity.this.patten, -1);
                GameActivity.this.createdArray[6] = GameActivity.this.temp;
                GameActivity.this.temp = -1;
                GameActivity.this.tempBitmap = null;
                GameActivity.this.flag = false;
                GameActivity.this.contrast();
            }
        }
    }

    class View7 implements View.OnClickListener {
        View7() {
        }

        public void onClick(View v) {
            if (GameActivity.this.clickable) {
                if (!GameActivity.this.flag) {
                    GameActivity.this.tempBitmap = GameActivity.this.bitmap7;
                    GameActivity.this.temp = GameActivity.this.createdArray[7];
                    if (GameActivity.this.mVol && GameActivity.this.mediaPlayer != null) {
                        GameActivity.this.mediaPlayer.start();
                    }
                    GameActivity.this.flag = true;
                    GameActivity.this.config = 7;
                    GameActivity.this.imageView7.setImageBitmap(GameActivity.this.getFocuse(GameActivity.this.bitmap7));
                    return;
                }
                GameActivity.this.whichBeChanged(GameActivity.this.config, GameActivity.this.bitmap7, GameActivity.this.createdArray[7]);
                GameActivity.this.bitmap7 = GameActivity.this.tempBitmap;
                GameActivity.this.imageView7.setImageBitmap(GameActivity.this.bitmap7);
                GameActivity.this.vibrator.vibrate(GameActivity.this.patten, -1);
                GameActivity.this.createdArray[7] = GameActivity.this.temp;
                GameActivity.this.temp = -1;
                GameActivity.this.tempBitmap = null;
                GameActivity.this.flag = false;
                GameActivity.this.contrast();
            }
        }
    }

    class View8 implements View.OnClickListener {
        View8() {
        }

        public void onClick(View v) {
            if (GameActivity.this.clickable) {
                if (!GameActivity.this.flag) {
                    GameActivity.this.tempBitmap = GameActivity.this.bitmap8;
                    GameActivity.this.temp = GameActivity.this.createdArray[8];
                    if (GameActivity.this.mVol && GameActivity.this.mediaPlayer != null) {
                        GameActivity.this.mediaPlayer.start();
                    }
                    GameActivity.this.flag = true;
                    GameActivity.this.config = 8;
                    GameActivity.this.imageView8.setImageBitmap(GameActivity.this.getFocuse(GameActivity.this.bitmap8));
                    return;
                }
                GameActivity.this.whichBeChanged(GameActivity.this.config, GameActivity.this.bitmap8, GameActivity.this.createdArray[8]);
                GameActivity.this.bitmap8 = GameActivity.this.tempBitmap;
                GameActivity.this.imageView8.setImageBitmap(GameActivity.this.bitmap8);
                GameActivity.this.vibrator.vibrate(GameActivity.this.patten, -1);
                GameActivity.this.createdArray[8] = GameActivity.this.temp;
                GameActivity.this.temp = -1;
                GameActivity.this.tempBitmap = null;
                GameActivity.this.flag = false;
                GameActivity.this.contrast();
            }
        }
    }

    class View9 implements View.OnClickListener {
        View9() {
        }

        public void onClick(View v) {
            if (GameActivity.this.clickable) {
                if (!GameActivity.this.flag) {
                    GameActivity.this.tempBitmap = GameActivity.this.bitmap9;
                    GameActivity.this.temp = GameActivity.this.createdArray[9];
                    if (GameActivity.this.mVol && GameActivity.this.mediaPlayer != null) {
                        GameActivity.this.mediaPlayer.start();
                    }
                    GameActivity.this.flag = true;
                    GameActivity.this.config = 9;
                    GameActivity.this.imageView9.setImageBitmap(GameActivity.this.getFocuse(GameActivity.this.bitmap9));
                    return;
                }
                GameActivity.this.whichBeChanged(GameActivity.this.config, GameActivity.this.bitmap9, GameActivity.this.createdArray[9]);
                GameActivity.this.bitmap9 = GameActivity.this.tempBitmap;
                GameActivity.this.imageView9.setImageBitmap(GameActivity.this.bitmap9);
                GameActivity.this.vibrator.vibrate(GameActivity.this.patten, -1);
                GameActivity.this.createdArray[9] = GameActivity.this.temp;
                GameActivity.this.temp = -1;
                GameActivity.this.tempBitmap = null;
                GameActivity.this.flag = false;
                GameActivity.this.contrast();
            }
        }
    }

    class View10 implements View.OnClickListener {
        View10() {
        }

        public void onClick(View v) {
            if (GameActivity.this.clickable) {
                if (!GameActivity.this.flag) {
                    GameActivity.this.tempBitmap = GameActivity.this.bitmap10;
                    GameActivity.this.temp = GameActivity.this.createdArray[10];
                    if (GameActivity.this.mVol && GameActivity.this.mediaPlayer != null) {
                        GameActivity.this.mediaPlayer.start();
                    }
                    GameActivity.this.flag = true;
                    GameActivity.this.config = 10;
                    GameActivity.this.imageView10.setImageBitmap(GameActivity.this.getFocuse(GameActivity.this.bitmap10));
                    return;
                }
                GameActivity.this.whichBeChanged(GameActivity.this.config, GameActivity.this.bitmap10, GameActivity.this.createdArray[10]);
                GameActivity.this.bitmap10 = GameActivity.this.tempBitmap;
                GameActivity.this.imageView10.setImageBitmap(GameActivity.this.bitmap10);
                GameActivity.this.vibrator.vibrate(GameActivity.this.patten, -1);
                GameActivity.this.createdArray[10] = GameActivity.this.temp;
                GameActivity.this.temp = -1;
                GameActivity.this.tempBitmap = null;
                GameActivity.this.flag = false;
                GameActivity.this.contrast();
            }
        }
    }

    class View11 implements View.OnClickListener {
        View11() {
        }

        public void onClick(View v) {
            if (GameActivity.this.clickable) {
                if (!GameActivity.this.flag) {
                    GameActivity.this.tempBitmap = GameActivity.this.bitmap11;
                    GameActivity.this.temp = GameActivity.this.createdArray[11];
                    if (GameActivity.this.mVol && GameActivity.this.mediaPlayer != null) {
                        GameActivity.this.mediaPlayer.start();
                    }
                    GameActivity.this.flag = true;
                    GameActivity.this.config = 11;
                    GameActivity.this.imageView11.setImageBitmap(GameActivity.this.getFocuse(GameActivity.this.bitmap11));
                    return;
                }
                GameActivity.this.whichBeChanged(GameActivity.this.config, GameActivity.this.bitmap11, GameActivity.this.createdArray[11]);
                GameActivity.this.bitmap11 = GameActivity.this.tempBitmap;
                GameActivity.this.imageView11.setImageBitmap(GameActivity.this.bitmap11);
                GameActivity.this.vibrator.vibrate(GameActivity.this.patten, -1);
                GameActivity.this.createdArray[11] = GameActivity.this.temp;
                GameActivity.this.temp = -1;
                GameActivity.this.tempBitmap = null;
                GameActivity.this.flag = false;
                GameActivity.this.contrast();
            }
        }
    }

    class View12 implements View.OnClickListener {
        View12() {
        }

        public void onClick(View v) {
            if (GameActivity.this.clickable) {
                if (!GameActivity.this.flag) {
                    GameActivity.this.tempBitmap = GameActivity.this.bitmap12;
                    GameActivity.this.temp = GameActivity.this.createdArray[12];
                    if (GameActivity.this.mVol && GameActivity.this.mediaPlayer != null) {
                        GameActivity.this.mediaPlayer.start();
                    }
                    GameActivity.this.flag = true;
                    GameActivity.this.config = 12;
                    GameActivity.this.imageView12.setImageBitmap(GameActivity.this.getFocuse(GameActivity.this.bitmap12));
                    return;
                }
                GameActivity.this.whichBeChanged(GameActivity.this.config, GameActivity.this.bitmap12, GameActivity.this.createdArray[12]);
                GameActivity.this.bitmap12 = GameActivity.this.tempBitmap;
                GameActivity.this.imageView12.setImageBitmap(GameActivity.this.bitmap12);
                GameActivity.this.vibrator.vibrate(GameActivity.this.patten, -1);
                GameActivity.this.createdArray[12] = GameActivity.this.temp;
                GameActivity.this.temp = -1;
                GameActivity.this.tempBitmap = null;
                GameActivity.this.flag = false;
                GameActivity.this.contrast();
            }
        }
    }

    class View13 implements View.OnClickListener {
        View13() {
        }

        public void onClick(View v) {
            if (GameActivity.this.clickable) {
                if (!GameActivity.this.flag) {
                    GameActivity.this.tempBitmap = GameActivity.this.bitmap13;
                    GameActivity.this.temp = GameActivity.this.createdArray[13];
                    if (GameActivity.this.mVol && GameActivity.this.mediaPlayer != null) {
                        GameActivity.this.mediaPlayer.start();
                    }
                    GameActivity.this.flag = true;
                    GameActivity.this.config = 13;
                    GameActivity.this.imageView13.setImageBitmap(GameActivity.this.getFocuse(GameActivity.this.bitmap13));
                    return;
                }
                GameActivity.this.whichBeChanged(GameActivity.this.config, GameActivity.this.bitmap13, GameActivity.this.createdArray[13]);
                GameActivity.this.bitmap13 = GameActivity.this.tempBitmap;
                GameActivity.this.imageView13.setImageBitmap(GameActivity.this.bitmap13);
                GameActivity.this.vibrator.vibrate(GameActivity.this.patten, -1);
                GameActivity.this.createdArray[13] = GameActivity.this.temp;
                GameActivity.this.temp = -1;
                GameActivity.this.tempBitmap = null;
                GameActivity.this.flag = false;
                GameActivity.this.contrast();
            }
        }
    }

    class View14 implements View.OnClickListener {
        View14() {
        }

        public void onClick(View v) {
            if (GameActivity.this.clickable) {
                if (!GameActivity.this.flag) {
                    GameActivity.this.tempBitmap = GameActivity.this.bitmap14;
                    GameActivity.this.temp = GameActivity.this.createdArray[14];
                    if (GameActivity.this.mVol && GameActivity.this.mediaPlayer != null) {
                        GameActivity.this.mediaPlayer.start();
                    }
                    GameActivity.this.flag = true;
                    GameActivity.this.config = 14;
                    GameActivity.this.imageView14.setImageBitmap(GameActivity.this.getFocuse(GameActivity.this.bitmap14));
                    return;
                }
                GameActivity.this.whichBeChanged(GameActivity.this.config, GameActivity.this.bitmap14, GameActivity.this.createdArray[14]);
                GameActivity.this.bitmap14 = GameActivity.this.tempBitmap;
                GameActivity.this.imageView14.setImageBitmap(GameActivity.this.bitmap14);
                GameActivity.this.vibrator.vibrate(GameActivity.this.patten, -1);
                GameActivity.this.createdArray[14] = GameActivity.this.temp;
                GameActivity.this.temp = -1;
                GameActivity.this.tempBitmap = null;
                GameActivity.this.flag = false;
                GameActivity.this.contrast();
            }
        }
    }

    class View15 implements View.OnClickListener {
        View15() {
        }

        public void onClick(View v) {
            if (GameActivity.this.clickable) {
                if (!GameActivity.this.flag) {
                    GameActivity.this.tempBitmap = GameActivity.this.bitmap15;
                    GameActivity.this.temp = GameActivity.this.createdArray[15];
                    if (GameActivity.this.mVol && GameActivity.this.mediaPlayer != null) {
                        GameActivity.this.mediaPlayer.start();
                    }
                    GameActivity.this.flag = true;
                    GameActivity.this.config = 15;
                    GameActivity.this.imageView15.setImageBitmap(GameActivity.this.getFocuse(GameActivity.this.bitmap15));
                    return;
                }
                GameActivity.this.whichBeChanged(GameActivity.this.config, GameActivity.this.bitmap15, GameActivity.this.createdArray[15]);
                GameActivity.this.bitmap15 = GameActivity.this.tempBitmap;
                GameActivity.this.imageView15.setImageBitmap(GameActivity.this.bitmap15);
                GameActivity.this.vibrator.vibrate(GameActivity.this.patten, -1);
                GameActivity.this.createdArray[15] = GameActivity.this.temp;
                GameActivity.this.temp = -1;
                GameActivity.this.tempBitmap = null;
                GameActivity.this.flag = false;
                GameActivity.this.contrast();
            }
        }
    }

    class View16 implements View.OnClickListener {
        View16() {
        }

        public void onClick(View v) {
            if (GameActivity.this.clickable) {
                if (!GameActivity.this.flag) {
                    GameActivity.this.tempBitmap = GameActivity.this.bitmap16;
                    GameActivity.this.temp = GameActivity.this.createdArray[16];
                    if (GameActivity.this.mVol && GameActivity.this.mediaPlayer != null) {
                        GameActivity.this.mediaPlayer.start();
                    }
                    GameActivity.this.flag = true;
                    GameActivity.this.config = 16;
                    GameActivity.this.imageView16.setImageBitmap(GameActivity.this.getFocuse(GameActivity.this.bitmap16));
                    return;
                }
                GameActivity.this.whichBeChanged(GameActivity.this.config, GameActivity.this.bitmap16, GameActivity.this.createdArray[16]);
                GameActivity.this.bitmap16 = GameActivity.this.tempBitmap;
                GameActivity.this.imageView16.setImageBitmap(GameActivity.this.bitmap16);
                GameActivity.this.vibrator.vibrate(GameActivity.this.patten, -1);
                GameActivity.this.createdArray[16] = GameActivity.this.temp;
                GameActivity.this.temp = -1;
                GameActivity.this.tempBitmap = null;
                GameActivity.this.flag = false;
                GameActivity.this.contrast();
            }
        }
    }

    class View17 implements View.OnClickListener {
        View17() {
        }

        public void onClick(View v) {
            if (GameActivity.this.clickable) {
                if (!GameActivity.this.flag) {
                    GameActivity.this.tempBitmap = GameActivity.this.bitmap17;
                    GameActivity.this.temp = GameActivity.this.createdArray[17];
                    if (GameActivity.this.mVol && GameActivity.this.mediaPlayer != null) {
                        GameActivity.this.mediaPlayer.start();
                    }
                    GameActivity.this.flag = true;
                    GameActivity.this.config = 17;
                    GameActivity.this.imageView17.setImageBitmap(GameActivity.this.getFocuse(GameActivity.this.bitmap17));
                    return;
                }
                GameActivity.this.whichBeChanged(GameActivity.this.config, GameActivity.this.bitmap17, GameActivity.this.createdArray[17]);
                GameActivity.this.bitmap17 = GameActivity.this.tempBitmap;
                GameActivity.this.imageView17.setImageBitmap(GameActivity.this.bitmap17);
                GameActivity.this.vibrator.vibrate(GameActivity.this.patten, -1);
                GameActivity.this.createdArray[17] = GameActivity.this.temp;
                GameActivity.this.temp = -1;
                GameActivity.this.tempBitmap = null;
                GameActivity.this.flag = false;
                GameActivity.this.contrast();
            }
        }
    }

    class View18 implements View.OnClickListener {
        View18() {
        }

        public void onClick(View v) {
            if (GameActivity.this.clickable) {
                if (!GameActivity.this.flag) {
                    GameActivity.this.tempBitmap = GameActivity.this.bitmap18;
                    GameActivity.this.temp = GameActivity.this.createdArray[18];
                    if (GameActivity.this.mVol && GameActivity.this.mediaPlayer != null) {
                        GameActivity.this.mediaPlayer.start();
                    }
                    GameActivity.this.flag = true;
                    GameActivity.this.config = 18;
                    GameActivity.this.imageView18.setImageBitmap(GameActivity.this.getFocuse(GameActivity.this.bitmap18));
                    return;
                }
                GameActivity.this.whichBeChanged(GameActivity.this.config, GameActivity.this.bitmap18, GameActivity.this.createdArray[18]);
                GameActivity.this.bitmap18 = GameActivity.this.tempBitmap;
                GameActivity.this.imageView18.setImageBitmap(GameActivity.this.bitmap18);
                GameActivity.this.vibrator.vibrate(GameActivity.this.patten, -1);
                GameActivity.this.createdArray[18] = GameActivity.this.temp;
                GameActivity.this.temp = -1;
                GameActivity.this.tempBitmap = null;
                GameActivity.this.flag = false;
                GameActivity.this.contrast();
            }
        }
    }

    class View19 implements View.OnClickListener {
        View19() {
        }

        public void onClick(View v) {
            if (GameActivity.this.clickable) {
                if (!GameActivity.this.flag) {
                    GameActivity.this.tempBitmap = GameActivity.this.bitmap19;
                    GameActivity.this.temp = GameActivity.this.createdArray[19];
                    if (GameActivity.this.mVol && GameActivity.this.mediaPlayer != null) {
                        GameActivity.this.mediaPlayer.start();
                    }
                    GameActivity.this.flag = true;
                    GameActivity.this.config = 19;
                    GameActivity.this.imageView19.setImageBitmap(GameActivity.this.getFocuse(GameActivity.this.bitmap19));
                    return;
                }
                GameActivity.this.whichBeChanged(GameActivity.this.config, GameActivity.this.bitmap19, GameActivity.this.createdArray[19]);
                GameActivity.this.bitmap19 = GameActivity.this.tempBitmap;
                GameActivity.this.imageView19.setImageBitmap(GameActivity.this.bitmap19);
                GameActivity.this.vibrator.vibrate(GameActivity.this.patten, -1);
                GameActivity.this.createdArray[19] = GameActivity.this.temp;
                GameActivity.this.temp = -1;
                GameActivity.this.tempBitmap = null;
                GameActivity.this.flag = false;
                GameActivity.this.contrast();
            }
        }
    }

    class View20 implements View.OnClickListener {
        View20() {
        }

        public void onClick(View v) {
            if (GameActivity.this.clickable) {
                if (!GameActivity.this.flag) {
                    GameActivity.this.tempBitmap = GameActivity.this.bitmap20;
                    GameActivity.this.temp = GameActivity.this.createdArray[20];
                    if (GameActivity.this.mVol && GameActivity.this.mediaPlayer != null) {
                        GameActivity.this.mediaPlayer.start();
                    }
                    GameActivity.this.flag = true;
                    GameActivity.this.config = 20;
                    GameActivity.this.imageView20.setImageBitmap(GameActivity.this.getFocuse(GameActivity.this.bitmap20));
                    return;
                }
                GameActivity.this.whichBeChanged(GameActivity.this.config, GameActivity.this.bitmap20, GameActivity.this.createdArray[20]);
                GameActivity.this.bitmap20 = GameActivity.this.tempBitmap;
                GameActivity.this.imageView20.setImageBitmap(GameActivity.this.bitmap20);
                GameActivity.this.vibrator.vibrate(GameActivity.this.patten, -1);
                GameActivity.this.createdArray[20] = GameActivity.this.temp;
                GameActivity.this.temp = -1;
                GameActivity.this.tempBitmap = null;
                GameActivity.this.flag = false;
                GameActivity.this.contrast();
            }
        }
    }

    class View21 implements View.OnClickListener {
        View21() {
        }

        public void onClick(View v) {
            if (GameActivity.this.clickable) {
                if (!GameActivity.this.flag) {
                    GameActivity.this.tempBitmap = GameActivity.this.bitmap21;
                    GameActivity.this.temp = GameActivity.this.createdArray[21];
                    if (GameActivity.this.mVol && GameActivity.this.mediaPlayer != null) {
                        GameActivity.this.mediaPlayer.start();
                    }
                    GameActivity.this.flag = true;
                    GameActivity.this.config = 21;
                    GameActivity.this.imageView21.setImageBitmap(GameActivity.this.getFocuse(GameActivity.this.bitmap21));
                    return;
                }
                GameActivity.this.whichBeChanged(GameActivity.this.config, GameActivity.this.bitmap21, GameActivity.this.createdArray[21]);
                GameActivity.this.bitmap21 = GameActivity.this.tempBitmap;
                GameActivity.this.imageView21.setImageBitmap(GameActivity.this.bitmap21);
                GameActivity.this.vibrator.vibrate(GameActivity.this.patten, -1);
                GameActivity.this.createdArray[21] = GameActivity.this.temp;
                GameActivity.this.temp = -1;
                GameActivity.this.tempBitmap = null;
                GameActivity.this.flag = false;
                GameActivity.this.contrast();
            }
        }
    }

    class View22 implements View.OnClickListener {
        View22() {
        }

        public void onClick(View v) {
            if (GameActivity.this.clickable) {
                if (!GameActivity.this.flag) {
                    GameActivity.this.tempBitmap = GameActivity.this.bitmap22;
                    GameActivity.this.temp = GameActivity.this.createdArray[22];
                    if (GameActivity.this.mVol && GameActivity.this.mediaPlayer != null) {
                        GameActivity.this.mediaPlayer.start();
                    }
                    GameActivity.this.flag = true;
                    GameActivity.this.config = 22;
                    GameActivity.this.imageView22.setImageBitmap(GameActivity.this.getFocuse(GameActivity.this.bitmap22));
                    return;
                }
                GameActivity.this.whichBeChanged(GameActivity.this.config, GameActivity.this.bitmap22, GameActivity.this.createdArray[22]);
                GameActivity.this.bitmap22 = GameActivity.this.tempBitmap;
                GameActivity.this.imageView22.setImageBitmap(GameActivity.this.bitmap22);
                GameActivity.this.vibrator.vibrate(GameActivity.this.patten, -1);
                GameActivity.this.createdArray[22] = GameActivity.this.temp;
                GameActivity.this.temp = -1;
                GameActivity.this.tempBitmap = null;
                GameActivity.this.flag = false;
                GameActivity.this.contrast();
            }
        }
    }

    class View23 implements View.OnClickListener {
        View23() {
        }

        public void onClick(View v) {
            if (GameActivity.this.clickable) {
                if (!GameActivity.this.flag) {
                    GameActivity.this.tempBitmap = GameActivity.this.bitmap23;
                    GameActivity.this.temp = GameActivity.this.createdArray[23];
                    if (GameActivity.this.mVol && GameActivity.this.mediaPlayer != null) {
                        GameActivity.this.mediaPlayer.start();
                    }
                    GameActivity.this.flag = true;
                    GameActivity.this.config = 23;
                    GameActivity.this.imageView23.setImageBitmap(GameActivity.this.getFocuse(GameActivity.this.bitmap23));
                    return;
                }
                GameActivity.this.whichBeChanged(GameActivity.this.config, GameActivity.this.bitmap23, GameActivity.this.createdArray[23]);
                GameActivity.this.bitmap23 = GameActivity.this.tempBitmap;
                GameActivity.this.imageView23.setImageBitmap(GameActivity.this.bitmap23);
                GameActivity.this.vibrator.vibrate(GameActivity.this.patten, -1);
                GameActivity.this.createdArray[23] = GameActivity.this.temp;
                GameActivity.this.temp = -1;
                GameActivity.this.tempBitmap = null;
                GameActivity.this.flag = false;
                GameActivity.this.contrast();
            }
        }
    }

    class View24 implements View.OnClickListener {
        View24() {
        }

        public void onClick(View v) {
            if (GameActivity.this.clickable) {
                if (!GameActivity.this.flag) {
                    GameActivity.this.tempBitmap = GameActivity.this.bitmap24;
                    GameActivity.this.temp = GameActivity.this.createdArray[24];
                    if (GameActivity.this.mVol && GameActivity.this.mediaPlayer != null) {
                        GameActivity.this.mediaPlayer.start();
                    }
                    GameActivity.this.flag = true;
                    GameActivity.this.config = 24;
                    GameActivity.this.imageView24.setImageBitmap(GameActivity.this.getFocuse(GameActivity.this.bitmap24));
                    return;
                }
                GameActivity.this.whichBeChanged(GameActivity.this.config, GameActivity.this.bitmap24, GameActivity.this.createdArray[24]);
                GameActivity.this.bitmap24 = GameActivity.this.tempBitmap;
                GameActivity.this.imageView24.setImageBitmap(GameActivity.this.bitmap24);
                GameActivity.this.vibrator.vibrate(GameActivity.this.patten, -1);
                GameActivity.this.createdArray[24] = GameActivity.this.temp;
                GameActivity.this.temp = -1;
                GameActivity.this.tempBitmap = null;
                GameActivity.this.flag = false;
                GameActivity.this.contrast();
            }
        }
    }

    /* access modifiers changed from: private */
    public void whichBeChanged(int config2, Bitmap drawable, int arrayValue) {
        if (config2 != -1) {
            if (config2 == 0) {
                this.bitmap0 = drawable;
                this.createdArray[0] = arrayValue;
                this.imageView0.setImageBitmap(this.bitmap0);
            } else if (config2 == 1) {
                this.bitmap1 = drawable;
                this.createdArray[1] = arrayValue;
                this.imageView1.setImageBitmap(this.bitmap1);
            } else if (config2 == 2) {
                this.bitmap2 = drawable;
                this.createdArray[2] = arrayValue;
                this.imageView2.setImageBitmap(this.bitmap2);
            } else if (config2 == 3) {
                this.bitmap3 = drawable;
                this.createdArray[3] = arrayValue;
                this.imageView3.setImageBitmap(this.bitmap3);
            } else if (config2 == 4) {
                this.bitmap4 = drawable;
                this.createdArray[4] = arrayValue;
                this.imageView4.setImageBitmap(this.bitmap4);
            } else if (config2 == 5) {
                this.bitmap5 = drawable;
                this.createdArray[5] = arrayValue;
                this.imageView5.setImageBitmap(this.bitmap5);
            } else if (config2 == 6) {
                this.bitmap6 = drawable;
                this.createdArray[6] = arrayValue;
                this.imageView6.setImageBitmap(this.bitmap6);
            } else if (config2 == 7) {
                this.bitmap7 = drawable;
                this.createdArray[7] = arrayValue;
                this.imageView7.setImageBitmap(this.bitmap7);
            } else if (config2 == 8) {
                this.bitmap8 = drawable;
                this.createdArray[8] = arrayValue;
                this.imageView8.setImageBitmap(this.bitmap8);
            } else if (config2 == 9) {
                this.bitmap9 = drawable;
                this.createdArray[9] = arrayValue;
                this.imageView9.setImageBitmap(this.bitmap9);
            } else if (config2 == 10) {
                this.bitmap10 = drawable;
                this.createdArray[10] = arrayValue;
                this.imageView10.setImageBitmap(this.bitmap10);
            } else if (config2 == 11) {
                this.bitmap11 = drawable;
                this.createdArray[11] = arrayValue;
                this.imageView11.setImageBitmap(this.bitmap11);
            } else if (config2 == 12) {
                this.bitmap12 = drawable;
                this.createdArray[12] = arrayValue;
                this.imageView12.setImageBitmap(this.bitmap12);
            } else if (config2 == 13) {
                this.bitmap13 = drawable;
                this.createdArray[13] = arrayValue;
                this.imageView13.setImageBitmap(this.bitmap13);
            } else if (config2 == 14) {
                this.bitmap14 = drawable;
                this.createdArray[14] = arrayValue;
                this.imageView14.setImageBitmap(this.bitmap14);
            } else if (config2 == 15) {
                this.bitmap15 = drawable;
                this.createdArray[15] = arrayValue;
                this.imageView15.setImageBitmap(this.bitmap15);
            } else if (config2 == 16) {
                this.bitmap16 = drawable;
                this.createdArray[16] = arrayValue;
                this.imageView16.setImageBitmap(this.bitmap16);
            } else if (config2 == 17) {
                this.bitmap17 = drawable;
                this.createdArray[17] = arrayValue;
                this.imageView17.setImageBitmap(this.bitmap17);
            } else if (config2 == 18) {
                this.bitmap18 = drawable;
                this.createdArray[18] = arrayValue;
                this.imageView18.setImageBitmap(this.bitmap18);
            } else if (config2 == 19) {
                this.bitmap19 = drawable;
                this.createdArray[19] = arrayValue;
                this.imageView19.setImageBitmap(this.bitmap19);
            } else if (config2 == 20) {
                this.bitmap20 = drawable;
                this.createdArray[20] = arrayValue;
                this.imageView20.setImageBitmap(this.bitmap20);
            } else if (config2 == 21) {
                this.bitmap21 = drawable;
                this.createdArray[21] = arrayValue;
                this.imageView21.setImageBitmap(this.bitmap21);
            } else if (config2 == 22) {
                this.bitmap22 = drawable;
                this.createdArray[22] = arrayValue;
                this.imageView22.setImageBitmap(this.bitmap22);
            } else if (config2 == 23) {
                this.bitmap23 = drawable;
                this.createdArray[23] = arrayValue;
                this.imageView23.setImageBitmap(this.bitmap23);
            } else if (config2 == 24) {
                this.bitmap24 = drawable;
                this.createdArray[24] = arrayValue;
                this.imageView24.setImageBitmap(this.bitmap24);
            }
        }
        this.steps++;
        this.moves.setText("moves:" + this.steps);
    }

    /* access modifiers changed from: private */
    public void restart() {
        this.adView.loadAd(new AdRequest());
        if (this.mVol) {
            final MediaPlayer mediaPlayerstart = MediaPlayer.create(this, (int) R.raw.sound_start);
            mediaPlayerstart.setLooping(false);
            if (this.mVol) {
                mediaPlayerstart.setVolume(1.0f, 1.0f);
            } else {
                mediaPlayerstart.setVolume(0.0f, 0.0f);
            }
            mediaPlayerstart.start();
            mediaPlayerstart.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                public void onCompletion(MediaPlayer mp) {
                    mediaPlayerstart.release();
                }
            });
        }
        if (this.level <= (this.GROUP_LENGTH * 1) / 5) {
            int[] restartArray = new CreateRondom().createNine();
            HashMap<String, Object> map = new DividePicture().divideIntoNine(getResources(), DateContent.PICTURE_GROUP[this.pictureArray[this.level - 1]]);
            this.bitmap0 = (Bitmap) map.get("bitmap" + restartArray[0]);
            this.bitmap0 = (Bitmap) map.get("bitmap" + restartArray[0]);
            this.bitmap1 = (Bitmap) map.get("bitmap" + restartArray[1]);
            this.bitmap2 = (Bitmap) map.get("bitmap" + restartArray[2]);
            this.bitmap3 = (Bitmap) map.get("bitmap" + restartArray[3]);
            this.bitmap4 = (Bitmap) map.get("bitmap" + restartArray[4]);
            this.bitmap5 = (Bitmap) map.get("bitmap" + restartArray[5]);
            this.bitmap6 = (Bitmap) map.get("bitmap" + restartArray[6]);
            this.bitmap7 = (Bitmap) map.get("bitmap" + restartArray[7]);
            this.bitmap8 = (Bitmap) map.get("bitmap" + restartArray[8]);
            this.createdArray = restartArray;
        } else if (this.level > (this.GROUP_LENGTH * 1) / 5 && this.level <= (this.GROUP_LENGTH * 2) / 5) {
            int[] restartArray2 = new CreateRondom().createTwelve();
            HashMap<String, Object> map2 = new DividePicture().divideIntoTwelve(getResources(), DateContent.PICTURE_GROUP[this.pictureArray[this.level - 1]]);
            this.bitmap0 = (Bitmap) map2.get("bitmap" + restartArray2[0]);
            this.bitmap1 = (Bitmap) map2.get("bitmap" + restartArray2[1]);
            this.bitmap2 = (Bitmap) map2.get("bitmap" + restartArray2[2]);
            this.bitmap3 = (Bitmap) map2.get("bitmap" + restartArray2[3]);
            this.bitmap4 = (Bitmap) map2.get("bitmap" + restartArray2[4]);
            this.bitmap5 = (Bitmap) map2.get("bitmap" + restartArray2[5]);
            this.bitmap6 = (Bitmap) map2.get("bitmap" + restartArray2[6]);
            this.bitmap7 = (Bitmap) map2.get("bitmap" + restartArray2[7]);
            this.bitmap8 = (Bitmap) map2.get("bitmap" + restartArray2[8]);
            this.bitmap9 = (Bitmap) map2.get("bitmap" + restartArray2[9]);
            this.bitmap10 = (Bitmap) map2.get("bitmap" + restartArray2[10]);
            this.bitmap11 = (Bitmap) map2.get("bitmap" + restartArray2[11]);
            this.createdArray = restartArray2;
        } else if (this.level > (this.GROUP_LENGTH * 2) / 5 && this.level <= (this.GROUP_LENGTH * 3) / 5) {
            int[] restartArray3 = new CreateRondom().createSixteen();
            HashMap<String, Object> map3 = new DividePicture().divideIntoSixteen(getResources(), DateContent.PICTURE_GROUP[this.pictureArray[this.level - 1]]);
            this.bitmap0 = (Bitmap) map3.get("bitmap" + restartArray3[0]);
            this.bitmap1 = (Bitmap) map3.get("bitmap" + restartArray3[1]);
            this.bitmap2 = (Bitmap) map3.get("bitmap" + restartArray3[2]);
            this.bitmap3 = (Bitmap) map3.get("bitmap" + restartArray3[3]);
            this.bitmap4 = (Bitmap) map3.get("bitmap" + restartArray3[4]);
            this.bitmap5 = (Bitmap) map3.get("bitmap" + restartArray3[5]);
            this.bitmap6 = (Bitmap) map3.get("bitmap" + restartArray3[6]);
            this.bitmap7 = (Bitmap) map3.get("bitmap" + restartArray3[7]);
            this.bitmap8 = (Bitmap) map3.get("bitmap" + restartArray3[8]);
            this.bitmap9 = (Bitmap) map3.get("bitmap" + restartArray3[9]);
            this.bitmap10 = (Bitmap) map3.get("bitmap" + restartArray3[10]);
            this.bitmap11 = (Bitmap) map3.get("bitmap" + restartArray3[11]);
            this.bitmap12 = (Bitmap) map3.get("bitmap" + restartArray3[12]);
            this.bitmap13 = (Bitmap) map3.get("bitmap" + restartArray3[13]);
            this.bitmap14 = (Bitmap) map3.get("bitmap" + restartArray3[14]);
            this.bitmap15 = (Bitmap) map3.get("bitmap" + restartArray3[15]);
            this.createdArray = restartArray3;
        } else if (this.level > (this.GROUP_LENGTH * 3) / 5 && this.level <= (this.GROUP_LENGTH * 4) / 5) {
            int[] restartArray4 = new CreateRondom().createTwenty();
            HashMap<String, Object> map4 = new DividePicture().divideIntoTwenty(getResources(), DateContent.PICTURE_GROUP[this.pictureArray[this.level - 1]]);
            this.bitmap0 = (Bitmap) map4.get("bitmap" + restartArray4[0]);
            this.bitmap1 = (Bitmap) map4.get("bitmap" + restartArray4[1]);
            this.bitmap2 = (Bitmap) map4.get("bitmap" + restartArray4[2]);
            this.bitmap3 = (Bitmap) map4.get("bitmap" + restartArray4[3]);
            this.bitmap4 = (Bitmap) map4.get("bitmap" + restartArray4[4]);
            this.bitmap5 = (Bitmap) map4.get("bitmap" + restartArray4[5]);
            this.bitmap6 = (Bitmap) map4.get("bitmap" + restartArray4[6]);
            this.bitmap7 = (Bitmap) map4.get("bitmap" + restartArray4[7]);
            this.bitmap8 = (Bitmap) map4.get("bitmap" + restartArray4[8]);
            this.bitmap9 = (Bitmap) map4.get("bitmap" + restartArray4[9]);
            this.bitmap10 = (Bitmap) map4.get("bitmap" + restartArray4[10]);
            this.bitmap11 = (Bitmap) map4.get("bitmap" + restartArray4[11]);
            this.bitmap12 = (Bitmap) map4.get("bitmap" + restartArray4[12]);
            this.bitmap13 = (Bitmap) map4.get("bitmap" + restartArray4[13]);
            this.bitmap14 = (Bitmap) map4.get("bitmap" + restartArray4[14]);
            this.bitmap15 = (Bitmap) map4.get("bitmap" + restartArray4[15]);
            this.bitmap16 = (Bitmap) map4.get("bitmap" + restartArray4[16]);
            this.bitmap17 = (Bitmap) map4.get("bitmap" + restartArray4[17]);
            this.bitmap18 = (Bitmap) map4.get("bitmap" + restartArray4[18]);
            this.bitmap19 = (Bitmap) map4.get("bitmap" + restartArray4[19]);
            this.createdArray = restartArray4;
        } else if (this.level > (this.GROUP_LENGTH * 4) / 5 && this.level <= this.GROUP_LENGTH) {
            int[] restartArray5 = new CreateRondom().createTwentyfive();
            HashMap<String, Object> map5 = new DividePicture().divideIntoTwentyFive(getResources(), DateContent.PICTURE_GROUP[this.pictureArray[this.level - 1]]);
            this.bitmap0 = (Bitmap) map5.get("bitmap" + restartArray5[0]);
            this.bitmap1 = (Bitmap) map5.get("bitmap" + restartArray5[1]);
            this.bitmap2 = (Bitmap) map5.get("bitmap" + restartArray5[2]);
            this.bitmap3 = (Bitmap) map5.get("bitmap" + restartArray5[3]);
            this.bitmap4 = (Bitmap) map5.get("bitmap" + restartArray5[4]);
            this.bitmap5 = (Bitmap) map5.get("bitmap" + restartArray5[5]);
            this.bitmap6 = (Bitmap) map5.get("bitmap" + restartArray5[6]);
            this.bitmap7 = (Bitmap) map5.get("bitmap" + restartArray5[7]);
            this.bitmap8 = (Bitmap) map5.get("bitmap" + restartArray5[8]);
            this.bitmap9 = (Bitmap) map5.get("bitmap" + restartArray5[9]);
            this.bitmap10 = (Bitmap) map5.get("bitmap" + restartArray5[10]);
            this.bitmap11 = (Bitmap) map5.get("bitmap" + restartArray5[11]);
            this.bitmap12 = (Bitmap) map5.get("bitmap" + restartArray5[12]);
            this.bitmap13 = (Bitmap) map5.get("bitmap" + restartArray5[13]);
            this.bitmap14 = (Bitmap) map5.get("bitmap" + restartArray5[14]);
            this.bitmap15 = (Bitmap) map5.get("bitmap" + restartArray5[15]);
            this.bitmap16 = (Bitmap) map5.get("bitmap" + restartArray5[16]);
            this.bitmap17 = (Bitmap) map5.get("bitmap" + restartArray5[17]);
            this.bitmap18 = (Bitmap) map5.get("bitmap" + restartArray5[18]);
            this.bitmap19 = (Bitmap) map5.get("bitmap" + restartArray5[19]);
            this.bitmap20 = (Bitmap) map5.get("bitmap" + restartArray5[20]);
            this.bitmap21 = (Bitmap) map5.get("bitmap" + restartArray5[21]);
            this.bitmap22 = (Bitmap) map5.get("bitmap" + restartArray5[22]);
            this.bitmap23 = (Bitmap) map5.get("bitmap" + restartArray5[23]);
            this.bitmap24 = (Bitmap) map5.get("bitmap" + restartArray5[24]);
            this.createdArray = restartArray5;
        }
        if (this.level <= this.GROUP_LENGTH) {
            this.imageView0.setImageBitmap(this.bitmap0);
            this.imageView1.setImageBitmap(this.bitmap1);
            this.imageView2.setImageBitmap(this.bitmap2);
            this.imageView3.setImageBitmap(this.bitmap3);
            this.imageView4.setImageBitmap(this.bitmap4);
            this.imageView5.setImageBitmap(this.bitmap5);
            this.imageView6.setImageBitmap(this.bitmap6);
            this.imageView7.setImageBitmap(this.bitmap7);
            this.imageView8.setImageBitmap(this.bitmap8);
        }
        if (this.level > (this.GROUP_LENGTH * 1) / 5 && this.level <= this.GROUP_LENGTH) {
            this.imageView9.setImageBitmap(this.bitmap9);
            this.imageView10.setImageBitmap(this.bitmap10);
            this.imageView11.setImageBitmap(this.bitmap11);
        }
        if (this.level > (this.GROUP_LENGTH * 2) / 5 && this.level <= this.GROUP_LENGTH) {
            this.imageView12.setImageBitmap(this.bitmap12);
            this.imageView13.setImageBitmap(this.bitmap13);
            this.imageView14.setImageBitmap(this.bitmap14);
            this.imageView15.setImageBitmap(this.bitmap15);
        }
        if (this.level > (this.GROUP_LENGTH * 3) / 5 && this.level <= this.GROUP_LENGTH) {
            this.imageView16.setImageBitmap(this.bitmap16);
            this.imageView17.setImageBitmap(this.bitmap17);
            this.imageView18.setImageBitmap(this.bitmap18);
            this.imageView19.setImageBitmap(this.bitmap19);
        }
        if (this.level > (this.GROUP_LENGTH * 4) / 5 && this.level <= this.GROUP_LENGTH) {
            this.imageView20.setImageBitmap(this.bitmap20);
            this.imageView21.setImageBitmap(this.bitmap21);
            this.imageView22.setImageBitmap(this.bitmap22);
            this.imageView23.setImageBitmap(this.bitmap23);
            this.imageView24.setImageBitmap(this.bitmap24);
        }
        this.steps = 0;
        this.reduceChance = 5;
        this.levels.setText("levels:" + this.level + "/" + this.GROUP_LENGTH);
        this.moves.setText("moves:" + this.steps);
        this.handler.post(this.countTime);
        this.clickable = true;
    }

    class CountTime implements Runnable {
        CountTime() {
        }

        public void run() {
            GameActivity gameActivity = GameActivity.this;
            gameActivity.seconds = gameActivity.seconds + 1;
            if (GameActivity.this.seconds > 59) {
                GameActivity.this.seconds = 0;
                GameActivity gameActivity2 = GameActivity.this;
                gameActivity2.minutes = gameActivity2.minutes + 1;
            }
            if (GameActivity.this.minutes > 59) {
                GameActivity.this.seconds = 59;
                GameActivity.this.minutes = 59;
            }
            GameActivity.this.times.setText("time:" + GameActivity.this.minutes + ":" + GameActivity.this.seconds);
            GameActivity.this.handler.postDelayed(GameActivity.this.countTime, 1000);
        }
    }

    private SimpleAdapter getMenuAdapter(int[] imageResourceArray) {
        ArrayList<HashMap<String, Object>> data = new ArrayList<>();
        for (int valueOf : imageResourceArray) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("itemImage", Integer.valueOf(valueOf));
            data.add(map);
        }
        return new SimpleAdapter(this, data, R.layout.item_menu, new String[]{"itemImage"}, new int[]{R.id.item_image});
    }

    /* access modifiers changed from: private */
    public void toastShow(int arg) {
        switch (arg) {
            case 0:
                Toast.makeText(this, (int) R.string.firstLevel, 0).show();
                return;
            case 1:
                Toast.makeText(this, (int) R.string.noChances, 0).show();
                return;
            case 2:
            default:
                return;
            case 3:
                Toast.makeText(this, (int) R.string.lastLevel, 0).show();
                return;
            case 4:
                Toast.makeText(this, (int) R.string.unlockFirst, 0).show();
                return;
        }
    }

    /* access modifiers changed from: private */
    public Bitmap getFocuse(Bitmap tempBitmap2) {
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(-65536);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(3.0f);
        int width = tempBitmap2.getWidth();
        int height = tempBitmap2.getHeight();
        Bitmap bmOverlay = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bmOverlay);
        canvas.drawBitmap(tempBitmap2, 0.0f, 0.0f, (Paint) null);
        canvas.drawRect(0.0f, 0.0f, (float) width, (float) height, paint);
        return bmOverlay;
    }

    /* access modifiers changed from: private */
    public void contrast() {
        int j = 0;
        int i = 0;
        while (i < this.createdArray.length) {
            if (this.createdArray[i] == this.correctArray[i]) {
                j++;
                i++;
            } else {
                return;
            }
        }
        if (j == this.createdArray.length) {
            this.handler.removeCallbacks(this.countTime);
            if (this.unlock == this.level && this.unlock != this.GROUP_LENGTH) {
                this.unlock = this.unlock + 1;
            }
            int tempA = 0;
            if (this.level <= (this.GROUP_LENGTH * 1) / 5) {
                tempA = (600 - (this.steps * 10)) - (((this.minutes * 60) + this.seconds) * 4);
            } else {
                if (this.level <= (this.GROUP_LENGTH * 1) / 5 || this.level > (this.GROUP_LENGTH * 2) / 5) {
                    if (this.level <= (this.GROUP_LENGTH * 2) / 5 || this.level > (this.GROUP_LENGTH * 3) / 5) {
                        if (this.level <= (this.GROUP_LENGTH * 3) / 5 || this.level > (this.GROUP_LENGTH * 4) / 5) {
                            if (this.level > (this.GROUP_LENGTH * 4) / 5 && this.level <= this.GROUP_LENGTH) {
                                tempA = (600 - (this.steps * 3)) - (((this.minutes * 60) + this.seconds) * 2);
                            }
                        } else {
                            tempA = (600 - (this.steps * 4)) - (((this.minutes * 60) + this.seconds) * 3);
                        }
                    } else {
                        tempA = (600 - (this.steps * 6)) - (((this.minutes * 60) + this.seconds) * 3);
                    }
                } else {
                    tempA = (600 - (this.steps * 8)) - (((this.minutes * 60) + this.seconds) * 4);
                }
            }
            if (tempA < 100) {
                this.score = 0;
            } else {
                this.score = tempA / 100;
            }
            if (this.mVol) {
                MediaPlayer mediaPlayerend = MediaPlayer.create(this, R.raw.sound_win);
                mediaPlayerend.setLooping(false);
                if (this.mVol) {
                    mediaPlayerend.setVolume(1.0f, 1.0f);
                } else {
                    mediaPlayerend.setVolume(0.0f, 0.0f);
                }
                mediaPlayerend.start();
                final MediaPlayer mediaPlayer2 = mediaPlayerend;
                mediaPlayerend.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    public void onCompletion(MediaPlayer mp) {
                        mediaPlayer2.release();
                    }
                });
            }
            View layout = ((LayoutInflater) getSystemService("layout_inflater")).inflate(R.layout.compelete_dialog, (ViewGroup) findViewById(R.id.compelete));
            ((TextView) layout.findViewById(R.id.countMoves)).setText("Moves:    " + this.steps);
            ((TextView) layout.findViewById(R.id.countTimes)).setText("Times:    " + this.minutes + ":" + this.seconds);
            ((RatingBar) layout.findViewById(R.id.scoreRating)).setRating((float) this.score);
            ImageButton wallp = (ImageButton) layout.findViewById(R.id.wallp);
            wallp.setImageResource(R.drawable.wallpaper);
            wallp.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    try {
                        GameActivity.this.setWallpaper(BitmapFactory.decodeResource(GameActivity.this.getResources(), DateContent.PICTURE_GROUP[GameActivity.this.pictureArray[GameActivity.this.level - 1]]));
                        Toast.makeText(GameActivity.this, "Set As Wallpaper Success", 1).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
            ImageButton savetosd = (ImageButton) layout.findViewById(R.id.savesd);
            savetosd.setImageResource(R.drawable.savesd);
            savetosd.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Exception e;
                    if (!"mounted".equals(Environment.getExternalStorageState())) {
                        Toast.makeText(GameActivity.this, "Stop: SD No Find", 1).show();
                        return;
                    }
                    Bitmap bm = BitmapFactory.decodeResource(GameActivity.this.getResources(), DateContent.PICTURE_GROUP[GameActivity.this.pictureArray[GameActivity.this.level - 1]]);
                    try {
                        long name = System.currentTimeMillis();
                        FileOutputStream fos = new FileOutputStream("/sdcard/save_" + name + ".jpg");
                        try {
                            Toast.makeText(GameActivity.this, "Save to SD Success", 1).show();
                            bm.compress(Bitmap.CompressFormat.JPEG, 90, fos);
                            fos.close();
                            GameActivity.this.sendBroadcast(new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE", Uri.parse("file://" + Environment.getExternalStorageDirectory() + "/save_" + name + ".jpg")));
                        } catch (Exception e2) {
                            e = e2;
                        }
                    } catch (Exception e3) {
                        e = e3;
                        e.printStackTrace();
                    }
                }
            });
            ImageButton nextlevel = (ImageButton) layout.findViewById(R.id.gotoNextLevel);
            if (this.level >= this.GROUP_LENGTH) {
                int getallScore = getallScore();
                Log.d("tag", "getallScore =" + getallScore);
                if (getallScore >= 5) {
                    nextlevel.setImageResource(R.drawable.complete_perfect);
                } else if (getallScore >= 3) {
                    nextlevel.setImageResource(R.drawable.complete_excellent);
                } else {
                    nextlevel.setImageResource(R.drawable.complete_good);
                }
            } else {
                nextlevel.setImageResource(R.drawable.nextlevel);
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setView(layout);
            AlertDialog alert = builder.create();
            alert.show();
            final AlertDialog alertDialog = alert;
            nextlevel.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    GameActivity.this.handler.removeCallbacks(GameActivity.this.countTime);
                    GameActivity.this.seconds = 0;
                    GameActivity.this.minutes = 0;
                    if (GameActivity.this.level >= GameActivity.this.GROUP_LENGTH) {
                        GameActivity.this.finish();
                    }
                    if (GameActivity.this.level % (GameActivity.this.GROUP_LENGTH / 5) == 0) {
                        GameActivity gameActivity = GameActivity.this;
                        gameActivity.level = gameActivity.level + 1;
                        GameActivity.this.defineLayout();
                        alertDialog.dismiss();
                        return;
                    }
                    GameActivity gameActivity2 = GameActivity.this;
                    gameActivity2.level = gameActivity2.level + 1;
                    GameActivity.this.restart();
                    alertDialog.dismiss();
                }
            });
        }
        saveData();
    }

    /* access modifiers changed from: private */
    public void showDiaglo() {
        View layout = ((LayoutInflater) getSystemService("layout_inflater")).inflate((int) R.layout.check_dialog, (ViewGroup) findViewById(R.id.layout_root));
        ImageView v = (ImageView) layout.findViewById(R.id.selectedP);
        Log.d("tag", "showDiaglolevel =" + this.level);
        v.setImageResource(DateContent.PICTURE_GROUP[this.pictureArray[this.level - 1]]);
        ((TextView) layout.findViewById(R.id.chances)).setText("You have " + this.reduceChance + " chances!");
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(layout);
        final AlertDialog alert = builder.create();
        alert.show();
        v.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                alert.dismiss();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        saveData();
    }

    private void saveData() {
        SharedPreferences gameSave = getSharedPreferences("DreamPuzzle", 0);
        SharedPreferences.Editor editor = gameSave.edit();
        if (gameSave.getInt("levelScore" + this.level, 0) <= this.score) {
            editor.putInt("levelScore" + this.level, this.score);
        }
        editor.putInt("levels", this.unlock);
        editor.commit();
        this.score = 0;
    }

    private int getallScore() {
        SharedPreferences gameSave = getSharedPreferences("DreamPuzzle", 0);
        int tempScore = 0;
        for (int i = 1; i <= this.level; i++) {
            tempScore += gameSave.getInt("levelScore" + i, 0);
        }
        return tempScore / this.level;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.mediaPlayer != null) {
            this.mediaPlayer.release();
        }
        this.handler.removeCallbacks(this.countTime);
        super.onDestroy();
    }
}
