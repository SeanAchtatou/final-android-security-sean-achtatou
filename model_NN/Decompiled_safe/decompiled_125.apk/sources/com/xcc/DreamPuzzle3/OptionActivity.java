package com.xcc.DreamPuzzle3;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import com.google.ads.AdRequest;
import com.google.ads.AdView;

public class OptionActivity extends Activity {
    private AdView adView;
    ImageButton soundOn;
    ImageButton vibratOn;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.option);
        this.adView = (AdView) findViewById(R.id.adView);
        this.adView.loadAd(new AdRequest());
        SharedPreferences settings = getSharedPreferences("DreamPuzzle", 0);
        boolean sound = settings.getBoolean("soundEnable", true);
        boolean vibrat = settings.getBoolean("vibratEnable", true);
        this.soundOn = (ImageButton) findViewById(R.id.soundOn);
        if (sound) {
            this.soundOn.setImageResource(R.drawable.music_open);
        } else {
            this.soundOn.setImageResource(R.drawable.music_close);
        }
        this.vibratOn = (ImageButton) findViewById(R.id.vibratOn);
        if (vibrat) {
            this.vibratOn.setImageResource(R.drawable.shake_open);
        } else {
            this.vibratOn.setImageResource(R.drawable.shake_close);
        }
        this.soundOn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SharedPreferences soundState = OptionActivity.this.getSharedPreferences("DreamPuzzle", 0);
                SharedPreferences.Editor editor = soundState.edit();
                boolean sound1 = soundState.getBoolean("soundEnable", true);
                editor.putBoolean("soundEnable", !sound1);
                editor.commit();
                if (!sound1) {
                    OptionActivity.this.soundOn.setImageResource(R.drawable.music_open);
                } else {
                    OptionActivity.this.soundOn.setImageResource(R.drawable.music_close);
                }
            }
        });
        this.vibratOn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SharedPreferences soundState = OptionActivity.this.getSharedPreferences("DreamPuzzle", 0);
                SharedPreferences.Editor editor = soundState.edit();
                boolean vibrat1 = soundState.getBoolean("vibratEnable", true);
                editor.putBoolean("vibratEnable", !vibrat1);
                editor.commit();
                if (!vibrat1) {
                    OptionActivity.this.vibratOn.setImageResource(R.drawable.shake_open);
                } else {
                    OptionActivity.this.vibratOn.setImageResource(R.drawable.shake_close);
                }
            }
        });
    }
}
