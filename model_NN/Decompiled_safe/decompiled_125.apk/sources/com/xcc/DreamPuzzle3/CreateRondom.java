package com.xcc.DreamPuzzle3;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class CreateRondom {
    public int[] returnArray;

    public int[] createNine() {
        Random random = new Random();
        List<Integer> list = new ArrayList<>();
        this.returnArray = new int[9];
        int j = 0;
        while (list.size() < 9) {
            int n = random.nextInt(9);
            if (!list.contains(Integer.valueOf(n))) {
                list.add(Integer.valueOf(n));
                this.returnArray[j] = n;
                j++;
            }
        }
        return this.returnArray;
    }

    public int[] createTwelve() {
        Random random = new Random();
        List<Integer> list = new ArrayList<>();
        this.returnArray = new int[12];
        int j = 0;
        while (list.size() < 12) {
            int n = random.nextInt(12);
            if (!list.contains(Integer.valueOf(n))) {
                list.add(Integer.valueOf(n));
                this.returnArray[j] = n;
                j++;
            }
        }
        return this.returnArray;
    }

    public int[] createRondomPG() {
        Random random = new Random();
        List<Integer> list = new ArrayList<>();
        this.returnArray = new int[DateContent.PICTURE_GROUP.length];
        int j = 0;
        while (list.size() < this.returnArray.length) {
            int n = random.nextInt(this.returnArray.length);
            if (!list.contains(Integer.valueOf(n))) {
                list.add(Integer.valueOf(n));
                this.returnArray[j] = n;
                j++;
            }
        }
        return this.returnArray;
    }

    public int[] createSixteen() {
        Random random = new Random();
        List<Integer> list = new ArrayList<>();
        this.returnArray = new int[16];
        int j = 0;
        while (list.size() < 16) {
            int n = random.nextInt(16);
            if (!list.contains(Integer.valueOf(n))) {
                list.add(Integer.valueOf(n));
                this.returnArray[j] = n;
                j++;
            }
        }
        return this.returnArray;
    }

    public int[] createTwenty() {
        Random random = new Random();
        List<Integer> list = new ArrayList<>();
        this.returnArray = new int[20];
        int j = 0;
        while (list.size() < 20) {
            int n = random.nextInt(20);
            if (!list.contains(Integer.valueOf(n))) {
                list.add(Integer.valueOf(n));
                this.returnArray[j] = n;
                j++;
            }
        }
        return this.returnArray;
    }

    public int[] createTwentyfive() {
        Random random = new Random();
        List<Integer> list = new ArrayList<>();
        this.returnArray = new int[25];
        int j = 0;
        while (list.size() < 25) {
            int n = random.nextInt(25);
            if (!list.contains(Integer.valueOf(n))) {
                list.add(Integer.valueOf(n));
                this.returnArray[j] = n;
                j++;
            }
        }
        return this.returnArray;
    }
}
