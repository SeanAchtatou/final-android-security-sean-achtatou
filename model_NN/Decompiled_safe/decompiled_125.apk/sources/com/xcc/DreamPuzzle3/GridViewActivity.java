package com.xcc.DreamPuzzle3;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.Toast;

public class GridViewActivity extends Activity {
    private SharedPreferences getData;
    private GridView gridView;
    /* access modifiers changed from: private */
    public int levels = 1;
    private int[] scoreSave = new int[DateContent.PICTURE_GROUP.length];

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.graidview);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.getData = getSharedPreferences("DreamPuzzle", 0);
        this.levels = this.getData.getInt("levels", 1);
        for (int i = 0; i < this.levels; i++) {
            this.scoreSave[i] = this.getData.getInt("levelScore" + (i + 1), 0);
        }
        String[] pString = this.getData.getString("pBuff", null).split("/");
        final int[] pictureArray = new int[DateContent.PICTURE_GROUP.length];
        for (int i2 = 1; i2 < pString.length; i2++) {
            pictureArray[i2 - 1] = Integer.parseInt(pString[i2]);
        }
        this.gridView = (GridView) findViewById(R.id.gridview);
        this.gridView.setAdapter((ListAdapter) new ImageAdapterF(this, this.levels, this.scoreSave, pictureArray));
        this.gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                if (position + 1 > GridViewActivity.this.levels) {
                    GridViewActivity.this.showToast();
                    return;
                }
                Intent intent = new Intent();
                intent.putExtra("level", position);
                intent.putExtra("pictureArray", pictureArray);
                intent.setClass(GridViewActivity.this, GameActivity.class);
                GridViewActivity.this.startActivity(intent);
            }
        });
    }

    /* access modifiers changed from: private */
    public void showToast() {
        Toast.makeText(this, (int) R.string.unlockFirst, 1).show();
    }
}
