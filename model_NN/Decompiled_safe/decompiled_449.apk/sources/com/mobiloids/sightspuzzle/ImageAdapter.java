package com.mobiloids.sightspuzzle;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import java.lang.reflect.Array;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class ImageAdapter extends BaseAdapter implements AdapterView.OnItemClickListener {
    protected static final int DIALOG_OK = 0;
    /* access modifiers changed from: private */
    public static Object lock = new Object();
    String EASY_FIRST = "EASY_FIRST";
    String EASY_SECOND = "EASY_SECOND";
    String EASY_THIRD = "EASY_THIRD";
    String HARD_FIRST = "HARD_FIRST";
    String HARD_SECOND = "HARD_SECOND";
    String HARD_THIRD = "HARD_THIRD";
    String MY_PREFS = "SETTINGS";
    int N;
    AlertDialog.Builder alert;
    Boolean allowClick;
    Context context;
    Display d;
    String default_value = "EMPTY#10000";
    String easy1;
    int easy1Score;
    String easy2;
    int easy2Score;
    String easy3;
    int easy3Score;
    Activity gameActivity;
    private RemoveCards handlerRemove = new RemoveCards();
    String hard1;
    int hard1Score;
    String hard2;
    int hard2Score;
    String hard3;
    int hard3Score;
    EditText input;
    boolean isSoundOn;
    int ix;
    int iy;
    /* access modifiers changed from: private */
    public Context mContext;
    /* access modifiers changed from: private */
    public Handler mDialogHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    ImageAdapter.this.showDialog();
                    return;
                default:
                    return;
            }
        }
    };
    private Integer[] mImageIds = {Integer.valueOf((int) R.drawable.lev1_09), Integer.valueOf((int) R.drawable.lev1_01), Integer.valueOf((int) R.drawable.lev1_02), Integer.valueOf((int) R.drawable.lev1_03), Integer.valueOf((int) R.drawable.lev1_04), Integer.valueOf((int) R.drawable.lev1_05), Integer.valueOf((int) R.drawable.lev1_06), Integer.valueOf((int) R.drawable.lev1_07), Integer.valueOf((int) R.drawable.lev1_08), Integer.valueOf((int) R.drawable.lev1_09)};
    int mb = 10;
    private Integer[][] memoryBoard;
    MediaPlayer movePlayer;
    /* access modifiers changed from: private */
    public int openedPairs = 0;
    int openedTempCount = 0;
    Integer playerScore;
    SharedPreferences pref;
    int prevValue = -1;
    int prevValue2 = -1;
    View prevView;
    View prevView2;
    TextView scoreField;
    Integer secondsPassed = 0;
    CountDownTimer soreTimer;
    String userName;
    MediaPlayer winPlayer;
    WindowManager wm;

    /* access modifiers changed from: private */
    public void showDialog() {
        Log.i("DIALOG", "----------------------------------");
        this.pref = this.gameActivity.getSharedPreferences(this.MY_PREFS, 0);
        this.soreTimer.cancel();
        this.playerScore = this.secondsPassed;
        this.easy1 = this.pref.getString(this.EASY_FIRST, this.default_value);
        this.easy2 = this.pref.getString(this.EASY_SECOND, this.default_value);
        this.easy3 = this.pref.getString(this.EASY_THIRD, this.default_value);
        this.easy1Score = Integer.parseInt(this.easy1.substring(this.easy1.indexOf("#") + 1));
        this.easy2Score = Integer.parseInt(this.easy2.substring(this.easy2.indexOf("#") + 1));
        this.easy3Score = Integer.parseInt(this.easy3.substring(this.easy3.indexOf("#") + 1));
        if (this.playerScore.intValue() > this.easy3Score) {
            AlertDialog.Builder winDialog = new AlertDialog.Builder(this.mContext);
            winDialog.setMessage("You Win!!! You finished the task in " + this.playerScore + "seconds.Unfortunetely your score is not good enough to place you in 'HIGH SCORES' table. Try again");
            winDialog.setTitle("Game Status Message");
            winDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int arg1) {
                    ((Activity) ImageAdapter.this.mContext).finish();
                }
            });
            winDialog.show();
            return;
        }
        this.alert = new AlertDialog.Builder(this.gameActivity);
        this.alert.setTitle("Congratulations!!!");
        this.alert.setMessage("Your result is superb.Please enter your name to store it in High Score list");
        final EditText input2 = new EditText(this.gameActivity);
        this.alert.setView(input2);
        this.alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                ImageAdapter.this.userName = input2.getText().toString();
                if (ImageAdapter.this.playerScore.intValue() < ImageAdapter.this.easy1Score) {
                    ImageAdapter.this.easy3 = ImageAdapter.this.easy2;
                    ImageAdapter.this.easy2 = ImageAdapter.this.easy1;
                    ImageAdapter.this.easy1 = String.valueOf(ImageAdapter.this.userName) + "#" + ImageAdapter.this.playerScore;
                } else if (ImageAdapter.this.playerScore.intValue() >= ImageAdapter.this.easy1Score && ImageAdapter.this.playerScore.intValue() < ImageAdapter.this.easy2Score) {
                    ImageAdapter.this.easy3 = ImageAdapter.this.easy2;
                    ImageAdapter.this.easy2 = String.valueOf(ImageAdapter.this.userName) + "#" + ImageAdapter.this.playerScore;
                } else if (ImageAdapter.this.playerScore.intValue() >= ImageAdapter.this.easy2Score && ImageAdapter.this.playerScore.intValue() < ImageAdapter.this.easy3Score) {
                    ImageAdapter.this.easy3 = String.valueOf(ImageAdapter.this.userName) + "#" + ImageAdapter.this.playerScore;
                }
                SharedPreferences.Editor editor = ImageAdapter.this.pref.edit();
                editor.putString(ImageAdapter.this.EASY_FIRST, ImageAdapter.this.easy1);
                editor.putString(ImageAdapter.this.EASY_SECOND, ImageAdapter.this.easy2);
                editor.putString(ImageAdapter.this.EASY_THIRD, ImageAdapter.this.easy3);
                editor.commit();
                ImageAdapter.this.context.startActivity(new Intent(ImageAdapter.this.context, HighScores.class).addFlags(268435456));
                ImageAdapter.this.gameActivity.finish();
            }
        });
        this.alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                ImageAdapter.this.gameActivity.finish();
            }
        });
        this.alert.show();
    }

    public boolean isGameFinished() {
        for (int i = 0; i < this.N; i++) {
            for (int j = 0; j < this.N; j++) {
                if (i == this.N - 1 && j == this.N - 1) {
                    return true;
                }
                if (this.memoryBoard[i][j].intValue() != (this.N * i) + j + 1) {
                    return false;
                }
            }
        }
        return true;
    }

    public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
        this.ix = position / this.N;
        this.iy = position % this.N;
        for (int i = 0; i < this.N; i++) {
            for (int j = 0; j < this.N; j++) {
                if (this.memoryBoard[i][j].intValue() == 0 && Math.abs(i - this.ix) + Math.abs(j - this.iy) == 1) {
                    GridView grid = null;
                    if (this.N == 3) {
                        grid = (GridView) this.gameActivity.findViewById(R.id.easy_gridview);
                    }
                    if (this.N == 4) {
                        grid = (GridView) this.gameActivity.findViewById(R.id.hard_gridview);
                    }
                    ImageView img2 = (ImageView) grid.getChildAt((this.N * i) + j);
                    img2.setImageResource(this.mImageIds[this.memoryBoard[this.ix][this.iy].intValue()].intValue());
                    ((ImageView) grid.getChildAt((this.ix * this.N) + this.iy)).setVisibility(4);
                    img2.setVisibility(0);
                    this.memoryBoard[i][j] = this.memoryBoard[this.ix][this.iy];
                    this.memoryBoard[this.ix][this.iy] = 0;
                    if (isGameFinished()) {
                        ImageView imgFinal = (ImageView) ((GridView) this.gameActivity.findViewById(R.id.easy_gridview)).getChildAt((this.N * this.N) - 1);
                        imgFinal.setImageResource(this.mImageIds[9].intValue());
                        imgFinal.setVisibility(0);
                        Log.i("GAME FINISHED", "----------------------------------");
                        try {
                            if (this.winPlayer != null && this.isSoundOn) {
                                this.winPlayer.start();
                            }
                        } catch (Exception e) {
                        }
                        new Timer().schedule(new TimerTask() {
                            public void run() {
                                Log.i("TIMER TASK", "----------------------------------");
                                ImageAdapter.this.mDialogHandler.sendEmptyMessage(0);
                            }
                        }, 2000);
                    }
                }
            }
        }
    }

    public ImageAdapter(Context c, int i, Context con, Activity gameAct, boolean soundStatus) {
        this.context = con;
        this.isSoundOn = soundStatus;
        this.gameActivity = gameAct;
        if (i == 1) {
            this.N = 3;
        } else if (i == 2) {
            this.N = 3;
            this.mImageIds[1] = Integer.valueOf((int) R.drawable.lev2_01);
            this.mImageIds[2] = Integer.valueOf((int) R.drawable.lev2_02);
            this.mImageIds[3] = Integer.valueOf((int) R.drawable.lev2_03);
            this.mImageIds[4] = Integer.valueOf((int) R.drawable.lev2_04);
            this.mImageIds[5] = Integer.valueOf((int) R.drawable.lev2_05);
            this.mImageIds[6] = Integer.valueOf((int) R.drawable.lev2_06);
            this.mImageIds[7] = Integer.valueOf((int) R.drawable.lev2_07);
            this.mImageIds[8] = Integer.valueOf((int) R.drawable.lev2_08);
            this.mImageIds[9] = Integer.valueOf((int) R.drawable.lev2_09);
        } else if (i == 3) {
            this.N = 3;
            this.mImageIds[1] = Integer.valueOf((int) R.drawable.lev3_01);
            this.mImageIds[2] = Integer.valueOf((int) R.drawable.lev3_02);
            this.mImageIds[3] = Integer.valueOf((int) R.drawable.lev3_03);
            this.mImageIds[4] = Integer.valueOf((int) R.drawable.lev3_04);
            this.mImageIds[5] = Integer.valueOf((int) R.drawable.lev3_05);
            this.mImageIds[6] = Integer.valueOf((int) R.drawable.lev3_06);
            this.mImageIds[7] = Integer.valueOf((int) R.drawable.lev3_07);
            this.mImageIds[8] = Integer.valueOf((int) R.drawable.lev3_08);
            this.mImageIds[9] = Integer.valueOf((int) R.drawable.lev3_09);
        } else if (i == 4) {
            this.N = 3;
            this.mImageIds[1] = Integer.valueOf((int) R.drawable.lev4_01);
            this.mImageIds[2] = Integer.valueOf((int) R.drawable.lev4_02);
            this.mImageIds[3] = Integer.valueOf((int) R.drawable.lev4_03);
            this.mImageIds[4] = Integer.valueOf((int) R.drawable.lev4_04);
            this.mImageIds[5] = Integer.valueOf((int) R.drawable.lev4_05);
            this.mImageIds[6] = Integer.valueOf((int) R.drawable.lev4_06);
            this.mImageIds[7] = Integer.valueOf((int) R.drawable.lev4_07);
            this.mImageIds[8] = Integer.valueOf((int) R.drawable.lev4_08);
            this.mImageIds[9] = Integer.valueOf((int) R.drawable.lev4_09);
        } else if (i == 5) {
            this.N = 3;
            this.mImageIds[1] = Integer.valueOf((int) R.drawable.lev5_01);
            this.mImageIds[2] = Integer.valueOf((int) R.drawable.lev5_02);
            this.mImageIds[3] = Integer.valueOf((int) R.drawable.lev5_03);
            this.mImageIds[4] = Integer.valueOf((int) R.drawable.lev5_04);
            this.mImageIds[5] = Integer.valueOf((int) R.drawable.lev5_05);
            this.mImageIds[6] = Integer.valueOf((int) R.drawable.lev5_06);
            this.mImageIds[7] = Integer.valueOf((int) R.drawable.lev5_07);
            this.mImageIds[8] = Integer.valueOf((int) R.drawable.lev5_08);
            this.mImageIds[9] = Integer.valueOf((int) R.drawable.lev5_09);
        } else if (i == 6) {
            this.N = 3;
            this.mImageIds[1] = Integer.valueOf((int) R.drawable.lev6_01);
            this.mImageIds[2] = Integer.valueOf((int) R.drawable.lev6_02);
            this.mImageIds[3] = Integer.valueOf((int) R.drawable.lev6_03);
            this.mImageIds[4] = Integer.valueOf((int) R.drawable.lev6_04);
            this.mImageIds[5] = Integer.valueOf((int) R.drawable.lev6_05);
            this.mImageIds[6] = Integer.valueOf((int) R.drawable.lev6_06);
            this.mImageIds[7] = Integer.valueOf((int) R.drawable.lev6_07);
            this.mImageIds[8] = Integer.valueOf((int) R.drawable.lev6_08);
            this.mImageIds[9] = Integer.valueOf((int) R.drawable.lev6_09);
        } else if (i == 7) {
            this.N = 3;
            this.mImageIds[1] = Integer.valueOf((int) R.drawable.lev7_01);
            this.mImageIds[2] = Integer.valueOf((int) R.drawable.lev7_02);
            this.mImageIds[3] = Integer.valueOf((int) R.drawable.lev7_03);
            this.mImageIds[4] = Integer.valueOf((int) R.drawable.lev7_04);
            this.mImageIds[5] = Integer.valueOf((int) R.drawable.lev7_05);
            this.mImageIds[6] = Integer.valueOf((int) R.drawable.lev7_06);
            this.mImageIds[7] = Integer.valueOf((int) R.drawable.lev7_07);
            this.mImageIds[8] = Integer.valueOf((int) R.drawable.lev7_08);
            this.mImageIds[9] = Integer.valueOf((int) R.drawable.lev7_09);
        } else if (i == 8) {
            this.N = 3;
            this.mImageIds[1] = Integer.valueOf((int) R.drawable.lev8_01);
            this.mImageIds[2] = Integer.valueOf((int) R.drawable.lev8_02);
            this.mImageIds[3] = Integer.valueOf((int) R.drawable.lev8_03);
            this.mImageIds[4] = Integer.valueOf((int) R.drawable.lev8_04);
            this.mImageIds[5] = Integer.valueOf((int) R.drawable.lev8_05);
            this.mImageIds[6] = Integer.valueOf((int) R.drawable.lev8_06);
            this.mImageIds[7] = Integer.valueOf((int) R.drawable.lev8_07);
            this.mImageIds[8] = Integer.valueOf((int) R.drawable.lev8_08);
            this.mImageIds[9] = Integer.valueOf((int) R.drawable.lev8_09);
        } else if (i == 9) {
            this.N = 3;
            this.mImageIds[1] = Integer.valueOf((int) R.drawable.lev9_01);
            this.mImageIds[2] = Integer.valueOf((int) R.drawable.lev9_02);
            this.mImageIds[3] = Integer.valueOf((int) R.drawable.lev9_03);
            this.mImageIds[4] = Integer.valueOf((int) R.drawable.lev9_04);
            this.mImageIds[5] = Integer.valueOf((int) R.drawable.lev9_05);
            this.mImageIds[6] = Integer.valueOf((int) R.drawable.lev9_06);
            this.mImageIds[7] = Integer.valueOf((int) R.drawable.lev9_07);
            this.mImageIds[8] = Integer.valueOf((int) R.drawable.lev9_08);
            this.mImageIds[9] = Integer.valueOf((int) R.drawable.lev9_09);
        } else if (i == 10) {
            this.N = 3;
            this.mImageIds[1] = Integer.valueOf((int) R.drawable.lev10_01);
            this.mImageIds[2] = Integer.valueOf((int) R.drawable.lev10_02);
            this.mImageIds[3] = Integer.valueOf((int) R.drawable.lev10_03);
            this.mImageIds[4] = Integer.valueOf((int) R.drawable.lev10_04);
            this.mImageIds[5] = Integer.valueOf((int) R.drawable.lev10_05);
            this.mImageIds[6] = Integer.valueOf((int) R.drawable.lev10_06);
            this.mImageIds[7] = Integer.valueOf((int) R.drawable.lev10_07);
            this.mImageIds[8] = Integer.valueOf((int) R.drawable.lev10_08);
            this.mImageIds[9] = Integer.valueOf((int) R.drawable.lev10_09);
        } else if (i == 11) {
            this.N = 3;
            this.mImageIds[1] = Integer.valueOf((int) R.drawable.lev11_01);
            this.mImageIds[2] = Integer.valueOf((int) R.drawable.lev11_02);
            this.mImageIds[3] = Integer.valueOf((int) R.drawable.lev11_03);
            this.mImageIds[4] = Integer.valueOf((int) R.drawable.lev11_04);
            this.mImageIds[5] = Integer.valueOf((int) R.drawable.lev11_05);
            this.mImageIds[6] = Integer.valueOf((int) R.drawable.lev11_06);
            this.mImageIds[7] = Integer.valueOf((int) R.drawable.lev11_07);
            this.mImageIds[8] = Integer.valueOf((int) R.drawable.lev11_08);
            this.mImageIds[9] = Integer.valueOf((int) R.drawable.lev11_09);
        } else if (i == 12) {
            this.N = 3;
            this.mImageIds[1] = Integer.valueOf((int) R.drawable.lev12_01);
            this.mImageIds[2] = Integer.valueOf((int) R.drawable.lev12_02);
            this.mImageIds[3] = Integer.valueOf((int) R.drawable.lev12_03);
            this.mImageIds[4] = Integer.valueOf((int) R.drawable.lev12_04);
            this.mImageIds[5] = Integer.valueOf((int) R.drawable.lev12_05);
            this.mImageIds[6] = Integer.valueOf((int) R.drawable.lev12_06);
            this.mImageIds[7] = Integer.valueOf((int) R.drawable.lev12_07);
            this.mImageIds[8] = Integer.valueOf((int) R.drawable.lev12_08);
            this.mImageIds[9] = Integer.valueOf((int) R.drawable.lev12_09);
        } else if (i == 13) {
            this.N = 3;
            this.mImageIds[1] = Integer.valueOf((int) R.drawable.lev13_01);
            this.mImageIds[2] = Integer.valueOf((int) R.drawable.lev13_02);
            this.mImageIds[3] = Integer.valueOf((int) R.drawable.lev13_03);
            this.mImageIds[4] = Integer.valueOf((int) R.drawable.lev13_04);
            this.mImageIds[5] = Integer.valueOf((int) R.drawable.lev13_05);
            this.mImageIds[6] = Integer.valueOf((int) R.drawable.lev13_06);
            this.mImageIds[7] = Integer.valueOf((int) R.drawable.lev13_07);
            this.mImageIds[8] = Integer.valueOf((int) R.drawable.lev13_08);
            this.mImageIds[9] = Integer.valueOf((int) R.drawable.lev13_09);
        } else if (i == 14) {
            this.N = 3;
            this.mImageIds[1] = Integer.valueOf((int) R.drawable.lev14_01);
            this.mImageIds[2] = Integer.valueOf((int) R.drawable.lev14_02);
            this.mImageIds[3] = Integer.valueOf((int) R.drawable.lev14_03);
            this.mImageIds[4] = Integer.valueOf((int) R.drawable.lev14_04);
            this.mImageIds[5] = Integer.valueOf((int) R.drawable.lev14_05);
            this.mImageIds[6] = Integer.valueOf((int) R.drawable.lev14_06);
            this.mImageIds[7] = Integer.valueOf((int) R.drawable.lev14_07);
            this.mImageIds[8] = Integer.valueOf((int) R.drawable.lev14_08);
            this.mImageIds[9] = Integer.valueOf((int) R.drawable.lev14_09);
        } else if (i == 15) {
            this.N = 3;
            this.mImageIds[1] = Integer.valueOf((int) R.drawable.lev15_01);
            this.mImageIds[2] = Integer.valueOf((int) R.drawable.lev15_02);
            this.mImageIds[3] = Integer.valueOf((int) R.drawable.lev15_03);
            this.mImageIds[4] = Integer.valueOf((int) R.drawable.lev15_04);
            this.mImageIds[5] = Integer.valueOf((int) R.drawable.lev15_05);
            this.mImageIds[6] = Integer.valueOf((int) R.drawable.lev15_06);
            this.mImageIds[7] = Integer.valueOf((int) R.drawable.lev15_07);
            this.mImageIds[8] = Integer.valueOf((int) R.drawable.lev15_08);
            this.mImageIds[9] = Integer.valueOf((int) R.drawable.lev15_09);
        } else if (i == 16) {
            this.N = 3;
            this.mImageIds[1] = Integer.valueOf((int) R.drawable.lev16_01);
            this.mImageIds[2] = Integer.valueOf((int) R.drawable.lev16_02);
            this.mImageIds[3] = Integer.valueOf((int) R.drawable.lev16_03);
            this.mImageIds[4] = Integer.valueOf((int) R.drawable.lev16_04);
            this.mImageIds[5] = Integer.valueOf((int) R.drawable.lev16_05);
            this.mImageIds[6] = Integer.valueOf((int) R.drawable.lev16_06);
            this.mImageIds[7] = Integer.valueOf((int) R.drawable.lev16_07);
            this.mImageIds[8] = Integer.valueOf((int) R.drawable.lev16_08);
            this.mImageIds[9] = Integer.valueOf((int) R.drawable.lev16_09);
        } else if (i == 17) {
            this.N = 3;
            this.mImageIds[1] = Integer.valueOf((int) R.drawable.lev17_01);
            this.mImageIds[2] = Integer.valueOf((int) R.drawable.lev17_02);
            this.mImageIds[3] = Integer.valueOf((int) R.drawable.lev17_03);
            this.mImageIds[4] = Integer.valueOf((int) R.drawable.lev17_04);
            this.mImageIds[5] = Integer.valueOf((int) R.drawable.lev17_05);
            this.mImageIds[6] = Integer.valueOf((int) R.drawable.lev17_06);
            this.mImageIds[7] = Integer.valueOf((int) R.drawable.lev17_07);
            this.mImageIds[8] = Integer.valueOf((int) R.drawable.lev17_08);
            this.mImageIds[9] = Integer.valueOf((int) R.drawable.lev17_09);
        } else if (i == 18) {
            this.N = 3;
            this.mImageIds[1] = Integer.valueOf((int) R.drawable.lev18_01);
            this.mImageIds[2] = Integer.valueOf((int) R.drawable.lev18_02);
            this.mImageIds[3] = Integer.valueOf((int) R.drawable.lev18_03);
            this.mImageIds[4] = Integer.valueOf((int) R.drawable.lev18_04);
            this.mImageIds[5] = Integer.valueOf((int) R.drawable.lev18_05);
            this.mImageIds[6] = Integer.valueOf((int) R.drawable.lev18_06);
            this.mImageIds[7] = Integer.valueOf((int) R.drawable.lev18_07);
            this.mImageIds[8] = Integer.valueOf((int) R.drawable.lev18_08);
            this.mImageIds[9] = Integer.valueOf((int) R.drawable.lev18_09);
        } else if (i == 19) {
            this.N = 3;
            this.mImageIds[1] = Integer.valueOf((int) R.drawable.lev19_01);
            this.mImageIds[2] = Integer.valueOf((int) R.drawable.lev19_02);
            this.mImageIds[3] = Integer.valueOf((int) R.drawable.lev19_03);
            this.mImageIds[4] = Integer.valueOf((int) R.drawable.lev19_04);
            this.mImageIds[5] = Integer.valueOf((int) R.drawable.lev19_05);
            this.mImageIds[6] = Integer.valueOf((int) R.drawable.lev19_06);
            this.mImageIds[7] = Integer.valueOf((int) R.drawable.lev19_07);
            this.mImageIds[8] = Integer.valueOf((int) R.drawable.lev19_08);
            this.mImageIds[9] = Integer.valueOf((int) R.drawable.lev19_09);
        } else if (i == 20) {
            this.N = 3;
            this.mImageIds[1] = Integer.valueOf((int) R.drawable.lev20_01);
            this.mImageIds[2] = Integer.valueOf((int) R.drawable.lev20_02);
            this.mImageIds[3] = Integer.valueOf((int) R.drawable.lev20_03);
            this.mImageIds[4] = Integer.valueOf((int) R.drawable.lev20_04);
            this.mImageIds[5] = Integer.valueOf((int) R.drawable.lev20_05);
            this.mImageIds[6] = Integer.valueOf((int) R.drawable.lev20_06);
            this.mImageIds[7] = Integer.valueOf((int) R.drawable.lev20_07);
            this.mImageIds[8] = Integer.valueOf((int) R.drawable.lev20_08);
            this.mImageIds[9] = Integer.valueOf((int) R.drawable.lev20_09);
        } else if (i == 21) {
            this.N = 3;
            this.mImageIds[1] = Integer.valueOf((int) R.drawable.lev21_01);
            this.mImageIds[2] = Integer.valueOf((int) R.drawable.lev21_02);
            this.mImageIds[3] = Integer.valueOf((int) R.drawable.lev21_03);
            this.mImageIds[4] = Integer.valueOf((int) R.drawable.lev21_04);
            this.mImageIds[5] = Integer.valueOf((int) R.drawable.lev21_05);
            this.mImageIds[6] = Integer.valueOf((int) R.drawable.lev21_06);
            this.mImageIds[7] = Integer.valueOf((int) R.drawable.lev21_07);
            this.mImageIds[8] = Integer.valueOf((int) R.drawable.lev21_08);
            this.mImageIds[9] = Integer.valueOf((int) R.drawable.lev21_09);
        } else if (i == 22) {
            this.N = 3;
            this.mImageIds[1] = Integer.valueOf((int) R.drawable.lev22_01);
            this.mImageIds[2] = Integer.valueOf((int) R.drawable.lev22_02);
            this.mImageIds[3] = Integer.valueOf((int) R.drawable.lev22_03);
            this.mImageIds[4] = Integer.valueOf((int) R.drawable.lev22_04);
            this.mImageIds[5] = Integer.valueOf((int) R.drawable.lev22_05);
            this.mImageIds[6] = Integer.valueOf((int) R.drawable.lev22_06);
            this.mImageIds[7] = Integer.valueOf((int) R.drawable.lev22_07);
            this.mImageIds[8] = Integer.valueOf((int) R.drawable.lev22_08);
            this.mImageIds[9] = Integer.valueOf((int) R.drawable.lev22_09);
        }
        this.mContext = c;
        generateImages();
        this.movePlayer = MediaPlayer.create(this.context, (int) R.raw.move);
        this.winPlayer = MediaPlayer.create(this.context, (int) R.raw.winning);
        this.allowClick = true;
        this.scoreField = (TextView) this.gameActivity.findViewById(R.id.easy_timer);
        this.soreTimer = new CountDownTimer(1800000, 1000) {
            public void onTick(long millisUntilFinished) {
                String time;
                ImageAdapter imageAdapter = ImageAdapter.this;
                imageAdapter.secondsPassed = Integer.valueOf(imageAdapter.secondsPassed.intValue() + 1);
                Integer minutes = Integer.valueOf(ImageAdapter.this.secondsPassed.intValue() / 60);
                Integer seconds = Integer.valueOf(ImageAdapter.this.secondsPassed.intValue() - (minutes.intValue() * 60));
                String time2 = String.valueOf("") + minutes;
                if (minutes.intValue() < 10) {
                    time2 = "0" + time2;
                }
                String time3 = String.valueOf(time2) + ":";
                if (seconds.intValue() < 10) {
                    time = String.valueOf(time3) + "0" + seconds;
                } else {
                    time = String.valueOf(time3) + seconds;
                }
                ImageAdapter.this.scoreField.setText(time);
            }

            public void onFinish() {
                ImageAdapter.this.scoreField.setText("done!");
            }
        }.start();
    }

    private void generateImages() {
        this.memoryBoard = (Integer[][]) Array.newInstance(Integer.class, this.N, this.N);
        for (int i = 0; i < this.N; i++) {
            for (int j = 0; j < this.N; j++) {
                this.memoryBoard[i][j] = Integer.valueOf((this.N * i) + j + 1);
            }
        }
        this.memoryBoard[this.N - 1][this.N - 1] = 0;
        int blank_x = this.N - 1;
        int blank_y = this.N - 1;
        Random r = new Random();
        int movesRemaining = 100;
        if (this.N == 3) {
            movesRemaining = 100;
        }
        if (this.N == 4) {
            movesRemaining = 300;
        }
        while (movesRemaining > 0) {
            int dx = r.nextInt(3) - 1;
            int dy = r.nextInt(3) - 1;
            if ((Math.abs(dx) + Math.abs(dy)) % 2 != 0 && blank_x + dx >= 0 && blank_y + dy >= 0 && blank_x + dx < this.N && blank_y + dy < this.N) {
                int temp = this.memoryBoard[blank_x + dx][blank_y + dy].intValue();
                this.memoryBoard[blank_x + dx][blank_y + dy] = this.memoryBoard[blank_x][blank_y];
                this.memoryBoard[blank_x][blank_y] = Integer.valueOf(temp);
                blank_x += dx;
                blank_y += dy;
                movesRemaining--;
            }
        }
    }

    public int getCount() {
        return this.N * this.N;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        if (convertView == null) {
            imageView = new ImageView(this.mContext);
            this.wm = (WindowManager) this.gameActivity.getSystemService("window");
            this.d = this.wm.getDefaultDisplay();
            int height = this.d.getHeight();
            int width = this.d.getWidth();
            int dim = height;
            if (width < height) {
                dim = width;
            }
            Integer valueOf = Integer.valueOf(dim);
            imageView.setLayoutParams(new AbsListView.LayoutParams(dim / this.N, dim / this.N));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(0, 0, 0, 0);
        } else {
            imageView = (ImageView) convertView;
        }
        int i_x = position / this.N;
        int i_y = position % this.N;
        Integer valueOf2 = Integer.valueOf(position);
        if (this.memoryBoard[i_x][i_y].intValue() != 0) {
            imageView.setImageResource(this.mImageIds[this.memoryBoard[i_x][i_y].intValue()].intValue());
        }
        return imageView;
    }

    class RemoveCards extends Handler {
        String EASY_FIRST = "EASY_FIRST";
        String EASY_SECOND = "EASY_SECOND";
        String EASY_THIRD = "EASY_THIRD";
        String HARD_FIRST = "HARD_FIRST";
        String HARD_SECOND = "HARD_SECOND";
        String HARD_THIRD = "HARD_THIRD";
        String MY_PREFS = "SETTINGS";
        String default_value = "EMPTY#10000";
        String easy1;
        int easy1Score;
        String easy2;
        int easy2Score;
        String easy3;
        int easy3Score;
        String hard1;
        int hard1Score;
        String hard2;
        int hard2Score;
        String hard3;
        int hard3Score;
        Integer playerScore;
        SharedPreferences pref;
        MediaPlayer removePlayer;

        RemoveCards() {
        }

        public void handleMessage(Message msg) {
            this.removePlayer = MediaPlayer.create(ImageAdapter.this.context, (int) R.raw.move);
            try {
                if (this.removePlayer != null && ImageAdapter.this.isSoundOn) {
                    this.removePlayer.start();
                }
            } catch (Exception e) {
            }
            if (this.removePlayer != null) {
                this.removePlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    public void onCompletion(MediaPlayer mp) {
                        if (RemoveCards.this.removePlayer != null) {
                            RemoveCards.this.removePlayer.release();
                        }
                    }
                });
            }
            synchronized (ImageAdapter.lock) {
                if (ImageAdapter.this.prevView2 != null) {
                    ((ImageView) ImageAdapter.this.prevView2).setVisibility(4);
                }
                if (ImageAdapter.this.prevView != null) {
                    ((ImageView) ImageAdapter.this.prevView).setVisibility(4);
                }
                if (ImageAdapter.this.openedPairs == (ImageAdapter.this.N * ImageAdapter.this.N) / 2) {
                    this.pref = ImageAdapter.this.gameActivity.getSharedPreferences(this.MY_PREFS, 0);
                    ImageAdapter.this.soreTimer.cancel();
                    this.playerScore = ImageAdapter.this.secondsPassed;
                    if (ImageAdapter.this.N == 3) {
                        this.easy1 = this.pref.getString(this.EASY_FIRST, this.default_value);
                        this.easy2 = this.pref.getString(this.EASY_SECOND, this.default_value);
                        this.easy3 = this.pref.getString(this.EASY_THIRD, this.default_value);
                        this.easy1Score = Integer.parseInt(this.easy1.substring(this.easy1.indexOf("#") + 1));
                        this.easy2Score = Integer.parseInt(this.easy2.substring(this.easy2.indexOf("#") + 1));
                        this.easy3Score = Integer.parseInt(this.easy3.substring(this.easy3.indexOf("#") + 1));
                        if (this.playerScore.intValue() > this.easy3Score) {
                            AlertDialog.Builder winDialog = new AlertDialog.Builder(ImageAdapter.this.mContext);
                            winDialog.setMessage("You Win!!! You finished the task in " + this.playerScore + "seconds.Unfortunetely your score is not good enough to place you in 'HIGH SCORES' table. Try again");
                            winDialog.setTitle("Game Status Message");
                            winDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int arg1) {
                                    ((Activity) ImageAdapter.this.mContext).finish();
                                }
                            });
                            winDialog.show();
                        } else {
                            AlertDialog.Builder alert = new AlertDialog.Builder(ImageAdapter.this.gameActivity);
                            alert.setTitle("Congratulations!!!");
                            alert.setMessage("Your result is superb.Please enter your name to store it in High Score list");
                            final EditText input = new EditText(ImageAdapter.this.gameActivity);
                            alert.setView(input);
                            alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    ImageAdapter.this.userName = input.getText().toString();
                                    if (RemoveCards.this.playerScore.intValue() < RemoveCards.this.easy1Score) {
                                        RemoveCards.this.easy3 = RemoveCards.this.easy2;
                                        RemoveCards.this.easy2 = RemoveCards.this.easy1;
                                        RemoveCards.this.easy1 = String.valueOf(ImageAdapter.this.userName) + "#" + RemoveCards.this.playerScore;
                                    } else if (RemoveCards.this.playerScore.intValue() >= RemoveCards.this.easy1Score && RemoveCards.this.playerScore.intValue() < RemoveCards.this.easy2Score) {
                                        RemoveCards.this.easy3 = RemoveCards.this.easy2;
                                        RemoveCards.this.easy2 = String.valueOf(ImageAdapter.this.userName) + "#" + RemoveCards.this.playerScore;
                                    } else if (RemoveCards.this.playerScore.intValue() >= RemoveCards.this.easy2Score && RemoveCards.this.playerScore.intValue() < RemoveCards.this.easy3Score) {
                                        RemoveCards.this.easy3 = String.valueOf(ImageAdapter.this.userName) + "#" + RemoveCards.this.playerScore;
                                    }
                                    SharedPreferences.Editor editor = RemoveCards.this.pref.edit();
                                    editor.putString(RemoveCards.this.EASY_FIRST, RemoveCards.this.easy1);
                                    editor.putString(RemoveCards.this.EASY_SECOND, RemoveCards.this.easy2);
                                    editor.putString(RemoveCards.this.EASY_THIRD, RemoveCards.this.easy3);
                                    editor.commit();
                                    ImageAdapter.this.context.startActivity(new Intent(ImageAdapter.this.context, HighScores.class).addFlags(268435456));
                                    ImageAdapter.this.gameActivity.finish();
                                }
                            });
                            alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    ImageAdapter.this.gameActivity.finish();
                                }
                            });
                            alert.show();
                        }
                    }
                    if (ImageAdapter.this.N == 4) {
                        this.hard1 = this.pref.getString(this.HARD_FIRST, this.default_value);
                        this.hard2 = this.pref.getString(this.HARD_SECOND, this.default_value);
                        this.hard3 = this.pref.getString(this.HARD_THIRD, this.default_value);
                        this.hard1Score = Integer.parseInt(this.hard1.substring(this.hard1.indexOf("#") + 1));
                        this.hard2Score = Integer.parseInt(this.hard2.substring(this.hard2.indexOf("#") + 1));
                        this.hard3Score = Integer.parseInt(this.hard3.substring(this.hard3.indexOf("#") + 1));
                        if (this.playerScore.intValue() > this.hard3Score) {
                            AlertDialog.Builder winDialog2 = new AlertDialog.Builder(ImageAdapter.this.mContext);
                            winDialog2.setMessage("You Win!!! You finished the task in " + this.playerScore + "seconds.Unfortunetely your score is not good enough to place you in 'HIGH SCORES' table. Try again");
                            winDialog2.setTitle("Game Status Message");
                            winDialog2.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int arg1) {
                                    ((Activity) ImageAdapter.this.mContext).finish();
                                }
                            });
                            winDialog2.show();
                        } else {
                            AlertDialog.Builder alert2 = new AlertDialog.Builder(ImageAdapter.this.gameActivity);
                            alert2.setTitle("Congratulations!!!");
                            alert2.setMessage("Your result is superb.Please enter your name to store it in High Score list");
                            final EditText input2 = new EditText(ImageAdapter.this.gameActivity);
                            alert2.setView(input2);
                            alert2.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    ImageAdapter.this.userName = input2.getText().toString();
                                    if (RemoveCards.this.playerScore.intValue() < RemoveCards.this.hard1Score) {
                                        RemoveCards.this.hard3 = RemoveCards.this.hard2;
                                        RemoveCards.this.hard2 = RemoveCards.this.hard1;
                                        RemoveCards.this.hard1 = String.valueOf(ImageAdapter.this.userName) + "#" + RemoveCards.this.playerScore;
                                    } else if (RemoveCards.this.playerScore.intValue() >= RemoveCards.this.hard1Score && RemoveCards.this.playerScore.intValue() < RemoveCards.this.hard2Score) {
                                        RemoveCards.this.hard3 = RemoveCards.this.hard2;
                                        RemoveCards.this.hard2 = String.valueOf(ImageAdapter.this.userName) + "#" + RemoveCards.this.playerScore;
                                    } else if (RemoveCards.this.playerScore.intValue() >= RemoveCards.this.hard2Score && RemoveCards.this.playerScore.intValue() < RemoveCards.this.hard3Score) {
                                        RemoveCards.this.hard3 = String.valueOf(ImageAdapter.this.userName) + "#" + RemoveCards.this.playerScore;
                                    }
                                    SharedPreferences.Editor editor = RemoveCards.this.pref.edit();
                                    editor.putString(RemoveCards.this.HARD_FIRST, RemoveCards.this.hard1);
                                    editor.putString(RemoveCards.this.HARD_SECOND, RemoveCards.this.hard2);
                                    editor.putString(RemoveCards.this.HARD_THIRD, RemoveCards.this.hard3);
                                    editor.commit();
                                    ImageAdapter.this.context.startActivity(new Intent(ImageAdapter.this.context, HighScores.class).addFlags(268435456));
                                    ImageAdapter.this.gameActivity.finish();
                                }
                            });
                            alert2.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    ImageAdapter.this.gameActivity.finish();
                                }
                            });
                            alert2.show();
                        }
                    }
                }
                ImageAdapter.this.prevValue = -1;
                ImageAdapter.this.allowClick = true;
            }
        }
    }
}
