package com.mobiloids.sightspuzzle;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ToggleButton;

public class Settings extends Activity {
    private static String MUSIC_PREF = "MUSIC";
    private static String MY_PREFS = "SETTINGS";
    /* access modifiers changed from: private */
    public static String SOUND_PREF = "SOUND";
    private static final boolean default_value = true;
    private boolean isMusicOn;
    private boolean isSoundOn;
    private int mode = 0;
    /* access modifiers changed from: private */
    public SharedPreferences pref;
    ToggleButton sound;
    /* access modifiers changed from: private */
    public Activity thisActivity;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.thisActivity = this;
        setContentView((int) R.layout.settings);
        setTitle("Settings");
        ((ImageView) findViewById(R.id.settings_image_back)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Settings.this.thisActivity.finish();
            }
        });
        ((ImageView) findViewById(R.id.settings_image_ok)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SharedPreferences.Editor editor = Settings.this.pref.edit();
                editor.putBoolean(Settings.SOUND_PREF, Settings.this.sound.isChecked());
                editor.commit();
                Settings.this.thisActivity.finish();
            }
        });
        this.sound = (ToggleButton) findViewById(R.id.toggleSound);
        this.pref = getSharedPreferences(MY_PREFS, this.mode);
        this.isSoundOn = this.pref.getBoolean(SOUND_PREF, default_value);
        this.sound.setChecked(this.isSoundOn);
    }
}
