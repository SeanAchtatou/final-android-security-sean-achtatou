package com.mobiloids.sightspuzzle;

import android.app.Activity;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.widget.GridView;
import android.widget.ListAdapter;

public class Game extends Activity {
    private static final String LEVEL = "LEVEL";
    private static String MUSIC_PREF = "MUSIC";
    private static String MY_PREFS = "SETTINGS";
    private static String SOUND_PREF = "SOUND";
    private static final boolean default_value = true;
    private Activity aboutActivity;
    private int gameLevel;
    private int height;
    private boolean isMusicOn;
    private boolean isSoundOn;
    private long mStartTime;
    private MediaPlayer menuPlayer;
    private int mode = 0;
    private SharedPreferences pref;
    private int width;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int levelNum = getIntent().getExtras().getInt(LEVEL) + 1;
        this.pref = getSharedPreferences(MY_PREFS, this.mode);
        this.isSoundOn = this.pref.getBoolean(SOUND_PREF, default_value);
        this.isMusicOn = this.pref.getBoolean(MUSIC_PREF, default_value);
        setContentView((int) R.layout.easy_level);
        GridView gridview = (GridView) findViewById(R.id.easy_gridview);
        if (gridview != null) {
            ImageAdapter adapter = new ImageAdapter(this, levelNum, getApplicationContext(), this, this.isSoundOn);
            gridview.setAdapter((ListAdapter) adapter);
            gridview.setOnItemClickListener(adapter);
        }
    }

    private void startMenuMusic(int resource) {
        this.menuPlayer = MediaPlayer.create(getApplicationContext(), resource);
        if (this.isMusicOn && this.menuPlayer != null) {
            this.menuPlayer.setLooping(default_value);
            this.menuPlayer.start();
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
    }
}
