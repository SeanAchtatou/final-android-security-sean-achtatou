package com.mobiloids.sightspuzzle;

import android.content.Context;
import android.content.res.TypedArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;

public class GallaryAdapter extends BaseAdapter {
    private Context mContext;
    int mGalleryItemBackground;
    private Integer[] mImageIds = {Integer.valueOf((int) R.drawable.screen1), Integer.valueOf((int) R.drawable.screen2), Integer.valueOf((int) R.drawable.screen3), Integer.valueOf((int) R.drawable.screen4), Integer.valueOf((int) R.drawable.screen5), Integer.valueOf((int) R.drawable.screen6), Integer.valueOf((int) R.drawable.screen7), Integer.valueOf((int) R.drawable.screen8), Integer.valueOf((int) R.drawable.screen9), Integer.valueOf((int) R.drawable.screen10), Integer.valueOf((int) R.drawable.screen11), Integer.valueOf((int) R.drawable.screen12), Integer.valueOf((int) R.drawable.screen13), Integer.valueOf((int) R.drawable.screen14), Integer.valueOf((int) R.drawable.screen15), Integer.valueOf((int) R.drawable.screen16), Integer.valueOf((int) R.drawable.screen17), Integer.valueOf((int) R.drawable.screen18), Integer.valueOf((int) R.drawable.screen19), Integer.valueOf((int) R.drawable.screen20), Integer.valueOf((int) R.drawable.screen21), Integer.valueOf((int) R.drawable.screen22)};

    public GallaryAdapter(Context c) {
        this.mContext = c;
        TypedArray a = this.mContext.obtainStyledAttributes(R.styleable.HelloGallery);
        this.mGalleryItemBackground = a.getResourceId(0, 0);
        a.recycle();
    }

    public int getCount() {
        return this.mImageIds.length;
    }

    public Object getItem(int position) {
        return Integer.valueOf(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView i = new ImageView(this.mContext);
        i.setImageResource(this.mImageIds[position].intValue());
        i.setLayoutParams(new Gallery.LayoutParams(150, 100));
        i.setScaleType(ImageView.ScaleType.FIT_XY);
        i.setBackgroundResource(this.mGalleryItemBackground);
        return i;
    }
}
