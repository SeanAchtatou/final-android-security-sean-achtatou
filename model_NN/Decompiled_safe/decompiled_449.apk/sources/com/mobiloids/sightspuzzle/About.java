package com.mobiloids.sightspuzzle;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.net.ParseException;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import java.util.List;
import java.util.Vector;

public class About extends ListActivity {
    static final String[] detail = {"Train your brain", "Train your memory with Romantic memory", "Train your memroy cute toys", "Can you determine emotions?", "Do you know all US presidents?", "Do you know all France presidents?", "Very interesting game uses your phone accelerometer", "Help your child learn to quickly add,subtract,multiply,divide,compare fractions", "Learn maps of all US states playing this interesting quiz-game", "To know more about us please visit our site"};
    static final String[] title = {"Puzzle 15 FREE", "Romantic Memory FREE", "Soft toys Memory FREE", "Emotions   FREE", "US Presidents Quiz FREE", "France Presidents Quiz FREE", "Ball Game  FREE", "Kids Math   FREE", "Us States Quiz FREE", "Our web site"};
    private Vector<RowData> data;
    /* access modifiers changed from: private */
    public Integer[] imgid = {Integer.valueOf((int) R.drawable.puzzle), Integer.valueOf((int) R.drawable.love), Integer.valueOf((int) R.drawable.softtoy), Integer.valueOf((int) R.drawable.emotions), Integer.valueOf((int) R.drawable.uspresidents), Integer.valueOf((int) R.drawable.france), Integer.valueOf((int) R.drawable.ball), Integer.valueOf((int) R.drawable.kidsmath), Integer.valueOf((int) R.drawable.usstates), Integer.valueOf((int) R.drawable.web)};
    /* access modifiers changed from: private */
    public LayoutInflater mInflater;
    RowData rd;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.more_games);
        this.mInflater = (LayoutInflater) getSystemService("layout_inflater");
        this.data = new Vector<>();
        for (int i = 0; i < title.length; i++) {
            try {
                this.rd = new RowData(i, title[i], detail[i]);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            this.data.add(this.rd);
        }
        CustomAdapter adapter = new CustomAdapter(this, R.layout.list, R.id.title, this.data);
        getListView().setHeaderDividersEnabled(true);
        setListAdapter(adapter);
        getListView().setTextFilterEnabled(true);
    }

    public void onListItemClick(ListView parent, View v, int position, long id) {
        Intent intent = new Intent("android.intent.action.VIEW");
        if (position == 0) {
            intent.setData(Uri.parse("market://details?id=com.mobiloids.sliding"));
            startActivity(intent);
        }
        if (position == 1) {
            intent.setData(Uri.parse("market://details?id=com.mobiloids.valmemory"));
            startActivity(intent);
        }
        if (position == 2) {
            intent.setData(Uri.parse("market://details?id=com.mobiloids.softtoy"));
            startActivity(intent);
        }
        if (position == 3) {
            intent.setData(Uri.parse("market://details?id=com.mobiloids.emotions"));
            startActivity(intent);
        }
        if (position == 4) {
            intent.setData(Uri.parse("market://details?id=com.mobiloids.uspresidents"));
            startActivity(intent);
        }
        if (position == 5) {
            intent.setData(Uri.parse("market://details?id=com.mobiloids.frpresidents"));
            startActivity(intent);
        }
        if (position == 6) {
            intent.setData(Uri.parse("market://details?id=com.mobiloids.ballgame"));
            startActivity(intent);
        }
        if (position == 7) {
            intent.setData(Uri.parse("market://details?id=com.mobiloids.kidsmath"));
            startActivity(intent);
        }
        if (position == 8) {
            intent.setData(Uri.parse("market://details?id=com.mobiloids.usstates"));
            startActivity(intent);
        }
        if (position == 9) {
            intent.setData(Uri.parse("http://www.mobiloids.com"));
            startActivity(intent);
        }
    }

    private class RowData {
        protected String mDetail;
        protected int mId;
        protected String mTitle;

        RowData(int id, String title, String detail) {
            this.mId = id;
            this.mTitle = title;
            this.mDetail = detail;
        }

        public String toString() {
            return String.valueOf(this.mId) + " " + this.mTitle + " " + this.mDetail;
        }
    }

    private class CustomAdapter extends ArrayAdapter<RowData> {
        public CustomAdapter(Context context, int resource, int textViewResourceId, List<RowData> objects) {
            super(context, resource, textViewResourceId, objects);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            RowData rowData = (RowData) getItem(position);
            if (convertView == null) {
                convertView = About.this.mInflater.inflate((int) R.layout.list, (ViewGroup) null);
                convertView.setTag(new ViewHolder(convertView));
            }
            ViewHolder holder = (ViewHolder) convertView.getTag();
            holder.gettitle().setText(rowData.mTitle);
            holder.getdetail().setText(rowData.mDetail);
            holder.getImage().setImageResource(About.this.imgid[rowData.mId].intValue());
            return convertView;
        }

        private class ViewHolder {
            private TextView detail = null;
            private ImageView i11 = null;
            private View mRow;
            private TextView title = null;

            public ViewHolder(View row) {
                this.mRow = row;
            }

            public TextView gettitle() {
                if (this.title == null) {
                    this.title = (TextView) this.mRow.findViewById(R.id.title);
                }
                return this.title;
            }

            public TextView getdetail() {
                if (this.detail == null) {
                    this.detail = (TextView) this.mRow.findViewById(R.id.detail);
                }
                return this.detail;
            }

            public ImageView getImage() {
                if (this.i11 == null) {
                    this.i11 = (ImageView) this.mRow.findViewById(R.id.img);
                }
                return this.i11;
            }
        }
    }
}
