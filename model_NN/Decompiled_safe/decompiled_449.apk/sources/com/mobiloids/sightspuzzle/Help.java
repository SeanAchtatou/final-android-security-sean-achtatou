package com.mobiloids.sightspuzzle;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class Help extends Activity {
    Activity aboutActivity;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.help);
        setTitle("Help");
        this.aboutActivity = this;
        ((ImageView) findViewById(R.id.help_image_back)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Help.this.aboutActivity.finish();
            }
        });
    }
}
