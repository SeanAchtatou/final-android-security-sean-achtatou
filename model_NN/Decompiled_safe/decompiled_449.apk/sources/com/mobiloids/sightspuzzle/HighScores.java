package com.mobiloids.sightspuzzle;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class HighScores extends Activity {
    private static String MY_PREFS = "SETTINGS";
    private String EASY_FIRST = "EASY_FIRST";
    private String EASY_SECOND = "EASY_SECOND";
    private String EASY_THIRD = "EASY_THIRD";
    private String HARD_FIRST = "HARD_FIRST";
    private String HARD_SECOND = "HARD_SECOND";
    private String HARD_THIRD = "HARD_THIRD";
    private String default_value = "EMPTY#10000";
    private int mode = 0;
    private SharedPreferences pref;
    /* access modifiers changed from: private */
    public Activity thisActivity;

    public String convertSecondsToString(String sec) {
        String time;
        if (sec.equals("")) {
            return "";
        }
        int secondsPassed = Integer.parseInt(sec);
        Integer minutes = Integer.valueOf(secondsPassed / 60);
        Integer seconds = Integer.valueOf(secondsPassed - (minutes.intValue() * 60));
        String time2 = String.valueOf("") + minutes;
        if (minutes.intValue() < 10) {
            time2 = "0" + time2;
        }
        String time3 = String.valueOf(time2) + ":";
        if (seconds.intValue() < 10) {
            time = String.valueOf(time3) + "0" + seconds;
        } else {
            time = String.valueOf(time3) + seconds;
        }
        return time;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.high_scores);
        setTitle("High Scores");
        this.thisActivity = this;
        this.pref = getSharedPreferences(MY_PREFS, this.mode);
        String easy1 = this.pref.getString(this.EASY_FIRST, this.default_value);
        String easy2 = this.pref.getString(this.EASY_SECOND, this.default_value);
        String easy3 = this.pref.getString(this.EASY_THIRD, this.default_value);
        String easy1Score = easy1.substring(easy1.indexOf("#") + 1);
        String easy2Score = easy2.substring(easy2.indexOf("#") + 1);
        String easy3Score = easy3.substring(easy3.indexOf("#") + 1);
        if (easy1Score.equals("10000")) {
            easy1Score = "";
        }
        if (easy2Score.equals("10000")) {
            easy2Score = "";
        }
        if (easy3Score.equals("10000")) {
            easy3Score = "";
        }
        String easy1Name = easy1.substring(0, easy1.indexOf("#"));
        String easy2Name = easy2.substring(0, easy2.indexOf("#"));
        String easy3Name = easy3.substring(0, easy3.indexOf("#"));
        ((TextView) findViewById(R.id.rank_name_easy_1)).setText(easy1Name);
        ((TextView) findViewById(R.id.rank_name_easy_2)).setText(easy2Name);
        ((TextView) findViewById(R.id.rank_name_easy_3)).setText(easy3Name);
        ((TextView) findViewById(R.id.rank_score_easy_1)).setText(convertSecondsToString(easy1Score));
        ((TextView) findViewById(R.id.rank_score_easy_2)).setText(convertSecondsToString(easy2Score));
        ((TextView) findViewById(R.id.rank_score_easy_3)).setText(convertSecondsToString(easy3Score));
        String hard1 = this.pref.getString(this.HARD_FIRST, this.default_value);
        String hard2 = this.pref.getString(this.HARD_SECOND, this.default_value);
        String hard3 = this.pref.getString(this.HARD_THIRD, this.default_value);
        String hard1Score = hard1.substring(hard1.indexOf("#") + 1);
        String hard2Score = hard2.substring(hard2.indexOf("#") + 1);
        String hard3Score = hard3.substring(hard3.indexOf("#") + 1);
        if (hard1Score.equals("10000")) {
        }
        if (hard2Score.equals("10000")) {
        }
        if (hard3Score.equals("10000")) {
        }
        String substring = hard1.substring(0, hard1.indexOf("#"));
        String substring2 = hard2.substring(0, hard2.indexOf("#"));
        String substring3 = hard3.substring(0, hard3.indexOf("#"));
        ((ImageView) findViewById(R.id.rank_image_back)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HighScores.this.thisActivity.finish();
            }
        });
    }
}
