package com.mobiloids.sightspuzzle;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class mainMenu extends Activity {
    private static String MUSIC_PREF = "MUSIC";
    private static String MY_PREFS = "SETTINGS";
    private static final boolean default_value = true;
    private boolean isMusicOn;
    /* access modifiers changed from: private */
    public Activity menuActivity;
    private MediaPlayer menuPlayer;
    private int mode = 0;
    private SharedPreferences pref;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main_alter);
        this.menuActivity = this;
        this.pref = getSharedPreferences(MY_PREFS, this.mode);
        this.isMusicOn = this.pref.getBoolean(MUSIC_PREF, default_value);
        ((Button) findViewById(R.id.exitButton)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mainMenu.this.menuActivity.finish();
            }
        });
        ((Button) findViewById(R.id.aboutButton)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mainMenu.this.startActivity(new Intent(mainMenu.this, About.class));
            }
        });
        ((Button) findViewById(R.id.helpButton)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mainMenu.this.startActivity(new Intent(mainMenu.this, Help.class));
            }
        });
        ((Button) findViewById(R.id.startButton)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mainMenu.this.startActivity(new Intent(mainMenu.this, SelecLevel.class));
            }
        });
        ((Button) findViewById(R.id.settingsButton)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mainMenu.this.startActivity(new Intent(mainMenu.this, Settings.class));
            }
        });
        ((Button) findViewById(R.id.scoresButton)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mainMenu.this.startActivity(new Intent(mainMenu.this, HighScores.class));
            }
        });
    }

    private void startMenuMusic(int resource) {
        this.menuPlayer = MediaPlayer.create(getApplicationContext(), resource);
        if (this.isMusicOn && this.menuPlayer != null) {
            this.menuPlayer.setLooping(default_value);
            this.menuPlayer.start();
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.isMusicOn = this.pref.getBoolean(MUSIC_PREF, default_value);
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
        this.isMusicOn = this.pref.getBoolean(MUSIC_PREF, default_value);
    }
}
