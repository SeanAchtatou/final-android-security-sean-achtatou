package com.mobiloids.sightspuzzle;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Gallery;
import android.widget.SpinnerAdapter;
import android.widget.Toast;
import android.widget.ViewFlipper;

public class SelecLevel extends Activity {
    private static final String LEVEL = "LEVEL";
    private static String MY_PREFS = "SETTINGS";
    private String EASY_FIRST = "EASY_FIRST";
    private String EASY_SECOND = "EASY_SECOND";
    private String EASY_THIRD = "EASY_THIRD";
    private Activity aboutActivity;
    /* access modifiers changed from: private */
    public int courrentPictureIndex = 0;
    private String default_value = "EMPTY#10000";
    private boolean firstTime;
    Intent intent;
    /* access modifiers changed from: private */
    public Integer[] mPicturesIds = {Integer.valueOf((int) R.drawable.screen1), Integer.valueOf((int) R.drawable.screen2), Integer.valueOf((int) R.drawable.screen3), Integer.valueOf((int) R.drawable.screen4), Integer.valueOf((int) R.drawable.screen5), Integer.valueOf((int) R.drawable.screen6), Integer.valueOf((int) R.drawable.screen7), Integer.valueOf((int) R.drawable.screen8), Integer.valueOf((int) R.drawable.screen9), Integer.valueOf((int) R.drawable.screen10), Integer.valueOf((int) R.drawable.screen11), Integer.valueOf((int) R.drawable.screen12), Integer.valueOf((int) R.drawable.screen13), Integer.valueOf((int) R.drawable.screen14), Integer.valueOf((int) R.drawable.screen15), Integer.valueOf((int) R.drawable.screen16), Integer.valueOf((int) R.drawable.screen17), Integer.valueOf((int) R.drawable.screen18), Integer.valueOf((int) R.drawable.screen19), Integer.valueOf((int) R.drawable.screen20), Integer.valueOf((int) R.drawable.screen21), Integer.valueOf((int) R.drawable.screen22)};
    private int mode = 0;
    private SharedPreferences pref;
    /* access modifiers changed from: private */
    public String slidingMessage = "Touch the pictures near empty square to move them do not drag";
    /* access modifiers changed from: private */
    public Activity thisActivity;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.select_level);
        setTitle("Select Picture");
        this.thisActivity = this;
        Button big = (Button) this.thisActivity.findViewById(R.id.bigImage);
        big.setBackgroundResource(this.mPicturesIds[0].intValue());
        big.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SelecLevel.this.intent = new Intent(SelecLevel.this, Game.class);
                SelecLevel.this.intent.putExtra(SelecLevel.LEVEL, SelecLevel.this.courrentPictureIndex);
                Toast.makeText(SelecLevel.this.thisActivity, SelecLevel.this.slidingMessage, 1).show();
                SelecLevel.this.startActivity(SelecLevel.this.intent);
            }
        });
        this.pref = getSharedPreferences(MY_PREFS, this.mode);
        String easy1 = this.pref.getString(this.EASY_FIRST, this.default_value);
        String easy2 = this.pref.getString(this.EASY_SECOND, this.default_value);
        String easy3 = this.pref.getString(this.EASY_THIRD, this.default_value);
        if (easy1.compareTo(this.default_value) == 0 && easy2.compareTo(this.default_value) == 0 && easy3.compareTo(this.default_value) == 0) {
            this.firstTime = true;
        } else {
            this.firstTime = false;
        }
        Gallery g = (Gallery) findViewById(R.id.gallery);
        g.setAdapter((SpinnerAdapter) new GallaryAdapter(this));
        g.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View v, int position, long id) {
                ((Button) SelecLevel.this.thisActivity.findViewById(R.id.bigImage)).setBackgroundResource(SelecLevel.this.mPicturesIds[position].intValue());
                ViewFlipper flipper = (ViewFlipper) SelecLevel.this.findViewById(R.id.layoutswitcher);
                flipper.setInAnimation(AnimationUtils.loadAnimation(SelecLevel.this.thisActivity, R.anim.in));
                flipper.setOutAnimation(AnimationUtils.loadAnimation(SelecLevel.this.thisActivity, R.anim.out));
                flipper.startFlipping();
                flipper.setFlipInterval(1000);
                flipper.showNext();
                flipper.stopFlipping();
                SelecLevel.this.courrentPictureIndex = position;
            }
        });
    }
}
