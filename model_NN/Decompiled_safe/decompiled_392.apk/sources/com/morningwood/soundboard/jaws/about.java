package com.morningwood.soundboard.jaws;

import android.app.Activity;
import android.os.Bundle;
import android.view.MotionEvent;

public class about extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.about);
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() != 0) {
            return true;
        }
        finish();
        return true;
    }
}
