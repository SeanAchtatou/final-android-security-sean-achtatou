package com.morningwood.soundboard.jaws;

public final class R {

    public static final class attr {
        public static final int backgroundColor = 2130771968;
        public static final int keywords = 2130771971;
        public static final int primaryTextColor = 2130771969;
        public static final int refreshInterval = 2130771972;
        public static final int secondaryTextColor = 2130771970;
    }

    public static final class drawable {
        public static final int background = 2130837504;
        public static final int clear = 2130837505;
        public static final int clearly = 2130837506;
        public static final int gray = 2130837507;
        public static final int icon = 2130837508;
    }

    public static final class id {
        public static final int TextView01 = 2131165184;
        public static final int about = 2131165219;
        public static final int adview = 2131165217;
        public static final int front = 2131165186;
        public static final int home = 2131165220;
        public static final int more = 2131165218;
        public static final int quit = 2131165221;
        public static final int scrollview1 = 2131165185;
        public static final int sound1 = 2131165187;
        public static final int sound10 = 2131165196;
        public static final int sound11 = 2131165197;
        public static final int sound12 = 2131165198;
        public static final int sound13 = 2131165199;
        public static final int sound14 = 2131165200;
        public static final int sound15 = 2131165201;
        public static final int sound16 = 2131165202;
        public static final int sound17 = 2131165203;
        public static final int sound18 = 2131165204;
        public static final int sound19 = 2131165205;
        public static final int sound2 = 2131165188;
        public static final int sound20 = 2131165206;
        public static final int sound21 = 2131165207;
        public static final int sound22 = 2131165208;
        public static final int sound23 = 2131165209;
        public static final int sound24 = 2131165210;
        public static final int sound25 = 2131165211;
        public static final int sound26 = 2131165212;
        public static final int sound27 = 2131165213;
        public static final int sound28 = 2131165214;
        public static final int sound29 = 2131165215;
        public static final int sound3 = 2131165189;
        public static final int sound30 = 2131165216;
        public static final int sound4 = 2131165190;
        public static final int sound5 = 2131165191;
        public static final int sound6 = 2131165192;
        public static final int sound7 = 2131165193;
        public static final int sound8 = 2131165194;
        public static final int sound9 = 2131165195;
    }

    public static final class layout {
        public static final int about = 2130903040;
        public static final int main = 2130903041;
        public static final int menu = 2130903042;
    }

    public static final class raw {
        public static final int sound1 = 2130968576;
        public static final int sound10 = 2130968577;
        public static final int sound11 = 2130968578;
        public static final int sound12 = 2130968579;
        public static final int sound13 = 2130968580;
        public static final int sound14 = 2130968581;
        public static final int sound15 = 2130968582;
        public static final int sound16 = 2130968583;
        public static final int sound17 = 2130968584;
        public static final int sound18 = 2130968585;
        public static final int sound19 = 2130968586;
        public static final int sound2 = 2130968587;
        public static final int sound20 = 2130968588;
        public static final int sound21 = 2130968589;
        public static final int sound22 = 2130968590;
        public static final int sound23 = 2130968591;
        public static final int sound24 = 2130968592;
        public static final int sound25 = 2130968593;
        public static final int sound26 = 2130968594;
        public static final int sound27 = 2130968595;
        public static final int sound28 = 2130968596;
        public static final int sound29 = 2130968597;
        public static final int sound3 = 2130968598;
        public static final int sound30 = 2130968599;
        public static final int sound4 = 2130968600;
        public static final int sound5 = 2130968601;
        public static final int sound6 = 2130968602;
        public static final int sound7 = 2130968603;
        public static final int sound8 = 2130968604;
        public static final int sound9 = 2130968605;
        public static final int sound91 = 2130968606;
    }

    public static final class string {
        public static final int about_label = 2131034175;
        public static final int about_ok = 2131034177;
        public static final int about_title = 2131034176;
        public static final int app_name = 2131034113;
        public static final int button10_label = 2131034124;
        public static final int button11_label = 2131034125;
        public static final int button12_label = 2131034126;
        public static final int button13_label = 2131034127;
        public static final int button14_label = 2131034128;
        public static final int button15_label = 2131034129;
        public static final int button16_label = 2131034130;
        public static final int button17_label = 2131034131;
        public static final int button18_label = 2131034132;
        public static final int button19_label = 2131034133;
        public static final int button1_label = 2131034115;
        public static final int button20_label = 2131034134;
        public static final int button21_label = 2131034135;
        public static final int button22_label = 2131034136;
        public static final int button23_label = 2131034137;
        public static final int button24_label = 2131034138;
        public static final int button25_label = 2131034139;
        public static final int button26_label = 2131034140;
        public static final int button27_label = 2131034141;
        public static final int button28_label = 2131034142;
        public static final int button29_label = 2131034143;
        public static final int button2_label = 2131034116;
        public static final int button30_label = 2131034144;
        public static final int button3_label = 2131034117;
        public static final int button4_label = 2131034118;
        public static final int button5_label = 2131034119;
        public static final int button6_label = 2131034120;
        public static final int button7_label = 2131034121;
        public static final int button8_label = 2131034122;
        public static final int button9_label = 2131034123;
        public static final int hello = 2131034112;
        public static final int home_label = 2131034179;
        public static final int more_label = 2131034180;
        public static final int quit_label = 2131034178;
        public static final int string1 = 2131034145;
        public static final int string10 = 2131034154;
        public static final int string11 = 2131034155;
        public static final int string12 = 2131034156;
        public static final int string13 = 2131034157;
        public static final int string14 = 2131034158;
        public static final int string15 = 2131034159;
        public static final int string16 = 2131034160;
        public static final int string17 = 2131034161;
        public static final int string18 = 2131034162;
        public static final int string19 = 2131034163;
        public static final int string2 = 2131034146;
        public static final int string20 = 2131034164;
        public static final int string21 = 2131034165;
        public static final int string22 = 2131034166;
        public static final int string23 = 2131034167;
        public static final int string24 = 2131034168;
        public static final int string25 = 2131034169;
        public static final int string26 = 2131034170;
        public static final int string27 = 2131034171;
        public static final int string28 = 2131034172;
        public static final int string29 = 2131034173;
        public static final int string3 = 2131034147;
        public static final int string30 = 2131034174;
        public static final int string4 = 2131034148;
        public static final int string5 = 2131034149;
        public static final int string6 = 2131034150;
        public static final int string7 = 2131034151;
        public static final int string8 = 2131034152;
        public static final int string9 = 2131034153;
        public static final int version = 2131034114;
    }

    public static final class style {
        public static final int about = 2131099648;
        public static final int about_regular = 2131099649;
    }

    public static final class styleable {
        public static final int[] com_admob_android_ads_AdView = {R.attr.backgroundColor, R.attr.primaryTextColor, R.attr.secondaryTextColor, R.attr.keywords, R.attr.refreshInterval};
        public static final int com_admob_android_ads_AdView_backgroundColor = 0;
        public static final int com_admob_android_ads_AdView_keywords = 3;
        public static final int com_admob_android_ads_AdView_primaryTextColor = 1;
        public static final int com_admob_android_ads_AdView_refreshInterval = 4;
        public static final int com_admob_android_ads_AdView_secondaryTextColor = 2;
    }
}
