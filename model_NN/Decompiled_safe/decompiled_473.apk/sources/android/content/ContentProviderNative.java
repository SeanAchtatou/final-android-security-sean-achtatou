package android.content;

import android.app.IApplicationThread;
import android.content.res.AssetFileDescriptor;
import android.database.CursorWindow;
import android.database.DatabaseUtils;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import java.util.ArrayList;

public abstract class ContentProviderNative extends Binder implements IContentProvider {
    private static final String TAG = "ContentProvider";

    public ContentProviderNative() {
        attachInterface(this, IContentProvider.descriptor);
    }

    public static IContentProvider asInterface(IBinder obj) {
        if (obj == null) {
            return null;
        }
        IContentProvider in = (IContentProvider) obj.queryLocalInterface(IContentProvider.descriptor);
        if (in != null) {
            return in;
        }
        return new ContentProviderProxy(obj);
    }

    public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
        switch (code) {
            case 1:
                try {
                    data.enforceInterface(IContentProvider.descriptor);
                    Uri uri = (Uri) Uri.CREATOR.createFromParcel(data);
                    int num = data.readInt();
                    if (num > 0) {
                        String[] projection = new String[num];
                        for (int i = 0; i < num; i++) {
                            projection[i] = data.readString();
                        }
                    }
                    String readString = data.readString();
                    int num2 = data.readInt();
                    if (num2 > 0) {
                        String[] selectionArgs = new String[num2];
                        for (int i2 = 0; i2 < num2; i2++) {
                            selectionArgs[i2] = data.readString();
                        }
                    }
                    String readString2 = data.readString();
                    CursorWindow cursorWindow = (CursorWindow) CursorWindow.CREATOR.createFromParcel(data);
                    if (data.readInt() != 0) {
                    }
                    reply.writeNoException();
                    reply.writeStrongBinder(null);
                    return true;
                } catch (Exception e) {
                    DatabaseUtils.writeExceptionToParcel(reply, e);
                    return true;
                }
            case 2:
                data.enforceInterface(IContentProvider.descriptor);
                String type = getType((Uri) Uri.CREATOR.createFromParcel(data));
                reply.writeNoException();
                reply.writeString(type);
                return true;
            case 3:
                data.enforceInterface(IContentProvider.descriptor);
                Uri out = insert((Uri) Uri.CREATOR.createFromParcel(data), (ContentValues) ContentValues.CREATOR.createFromParcel(data));
                reply.writeNoException();
                Uri.writeToParcel(reply, out);
                return true;
            case 4:
                return true;
            case 5:
            case 6:
            case IApplicationThread.SCHEDULE_LAUNCH_ACTIVITY_TRANSACTION:
            case 8:
            case IApplicationThread.SCHEDULE_FINISH_ACTIVITY_TRANSACTION:
            case 11:
            case 12:
            case 16:
            case 17:
            case 18:
            case 19:
            default:
                return super.onTransact(code, data, reply, flags);
            case 10:
                return true;
            case 13:
                data.enforceInterface(IContentProvider.descriptor);
                int count = bulkInsert((Uri) Uri.CREATOR.createFromParcel(data), (ContentValues[]) data.createTypedArray(ContentValues.CREATOR));
                reply.writeNoException();
                reply.writeInt(count);
                return true;
            case 14:
                data.enforceInterface(IContentProvider.descriptor);
                ParcelFileDescriptor fd = openFile((Uri) Uri.CREATOR.createFromParcel(data), data.readString());
                reply.writeNoException();
                if (fd != null) {
                    reply.writeInt(1);
                    fd.writeToParcel(reply, 1);
                } else {
                    reply.writeInt(0);
                }
                return true;
            case 15:
                data.enforceInterface(IContentProvider.descriptor);
                AssetFileDescriptor fd2 = openAssetFile((Uri) Uri.CREATOR.createFromParcel(data), data.readString());
                reply.writeNoException();
                if (fd2 != null) {
                    reply.writeInt(1);
                    fd2.writeToParcel(reply, 1);
                } else {
                    reply.writeInt(0);
                }
                return true;
            case 20:
                data.enforceInterface(IContentProvider.descriptor);
                int numOperations = data.readInt();
                ArrayList<ContentProviderOperation> operations = new ArrayList<>(numOperations);
                for (int i3 = 0; i3 < numOperations; i3++) {
                    operations.add(i3, ContentProviderOperation.CREATOR.createFromParcel(data));
                }
                ContentProviderResult[] results = applyBatch(operations);
                reply.writeNoException();
                reply.writeTypedArray(results, 0);
                return true;
            case 21:
                data.enforceInterface(IContentProvider.descriptor);
                Bundle responseBundle = call(data.readString(), data.readString(), data.readBundle());
                reply.writeNoException();
                reply.writeBundle(responseBundle);
                return true;
        }
        DatabaseUtils.writeExceptionToParcel(reply, e);
        return true;
    }

    public IBinder asBinder() {
        return this;
    }
}
