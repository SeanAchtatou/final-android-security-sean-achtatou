package android.app;

import android.app.IInstrumentationWatcher;
import android.content.ComponentName;
import android.content.IIntentReceiver;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.ProviderInfo;
import android.content.pm.ServiceInfo;
import android.content.res.Configuration;
import android.os.Binder;
import android.os.Debug;
import android.os.IBinder;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import java.io.IOException;
import java.util.ArrayList;

public abstract class ApplicationThreadNative extends Binder implements IApplicationThread {
    public static IApplicationThread asInterface(IBinder obj) {
        if (obj == null) {
            return null;
        }
        IApplicationThread in = (IApplicationThread) obj.queryLocalInterface(IApplicationThread.descriptor);
        if (in != null) {
            return in;
        }
        return new ApplicationThreadProxy(obj);
    }

    public ApplicationThreadNative() {
        attachInterface(this, IApplicationThread.descriptor);
    }

    public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
        Intent args;
        boolean notResumed;
        switch (code) {
            case 1:
                data.enforceInterface(IApplicationThread.descriptor);
                schedulePauseActivity(data.readStrongBinder(), data.readInt() != 0, data.readInt() != 0, data.readInt());
                return true;
            case 2:
            case IActivityManager.GET_TASK_FOR_ACTIVITY_TRANSACTION /*27*/:
            default:
                return super.onTransact(code, data, reply, flags);
            case 3:
                data.enforceInterface(IApplicationThread.descriptor);
                scheduleStopActivity(data.readStrongBinder(), data.readInt() != 0, data.readInt());
                return true;
            case 4:
                data.enforceInterface(IApplicationThread.descriptor);
                scheduleWindowVisibility(data.readStrongBinder(), data.readInt() != 0);
                return true;
            case 5:
                data.enforceInterface(IApplicationThread.descriptor);
                scheduleResumeActivity(data.readStrongBinder(), data.readInt() != 0);
                return true;
            case 6:
                data.enforceInterface(IApplicationThread.descriptor);
                scheduleSendResult(data.readStrongBinder(), data.createTypedArrayList(ResultInfo.CREATOR));
                return true;
            case IApplicationThread.SCHEDULE_LAUNCH_ACTIVITY_TRANSACTION /*7*/:
                data.enforceInterface(IApplicationThread.descriptor);
                scheduleLaunchActivity((Intent) Intent.CREATOR.createFromParcel(data), data.readStrongBinder(), data.readInt(), (ActivityInfo) ActivityInfo.CREATOR.createFromParcel(data), data.readBundle(), data.createTypedArrayList(ResultInfo.CREATOR), data.createTypedArrayList(Intent.CREATOR), data.readInt() != 0, data.readInt() != 0);
                return true;
            case 8:
                data.enforceInterface(IApplicationThread.descriptor);
                scheduleNewIntent(data.createTypedArrayList(Intent.CREATOR), data.readStrongBinder());
                return true;
            case IApplicationThread.SCHEDULE_FINISH_ACTIVITY_TRANSACTION /*9*/:
                data.enforceInterface(IApplicationThread.descriptor);
                scheduleDestroyActivity(data.readStrongBinder(), data.readInt() != 0, data.readInt());
                return true;
            case 10:
                data.enforceInterface(IApplicationThread.descriptor);
                scheduleReceiver((Intent) Intent.CREATOR.createFromParcel(data), (ActivityInfo) ActivityInfo.CREATOR.createFromParcel(data), data.readInt(), data.readString(), data.readBundle(), data.readInt() != 0);
                return true;
            case 11:
                data.enforceInterface(IApplicationThread.descriptor);
                scheduleCreateService(data.readStrongBinder(), (ServiceInfo) ServiceInfo.CREATOR.createFromParcel(data));
                return true;
            case 12:
                data.enforceInterface(IApplicationThread.descriptor);
                scheduleStopService(data.readStrongBinder());
                return true;
            case 13:
                data.enforceInterface(IApplicationThread.descriptor);
                bindApplication(data.readString(), (ApplicationInfo) ApplicationInfo.CREATOR.createFromParcel(data), data.createTypedArrayList(ProviderInfo.CREATOR), data.readInt() != 0 ? new ComponentName(data) : null, data.readString(), data.readBundle(), IInstrumentationWatcher.Stub.asInterface(data.readStrongBinder()), data.readInt(), data.readInt() != 0, (Configuration) Configuration.CREATOR.createFromParcel(data), data.readHashMap(null));
                return true;
            case 14:
                data.enforceInterface(IApplicationThread.descriptor);
                scheduleExit();
                return true;
            case 15:
                data.enforceInterface(IApplicationThread.descriptor);
                requestThumbnail(data.readStrongBinder());
                return true;
            case 16:
                data.enforceInterface(IApplicationThread.descriptor);
                scheduleConfigurationChanged((Configuration) Configuration.CREATOR.createFromParcel(data));
                return true;
            case 17:
                data.enforceInterface(IApplicationThread.descriptor);
                IBinder token = data.readStrongBinder();
                int startId = data.readInt();
                int fl = data.readInt();
                if (data.readInt() != 0) {
                    args = (Intent) Intent.CREATOR.createFromParcel(data);
                } else {
                    args = null;
                }
                scheduleServiceArgs(token, startId, fl, args);
                return true;
            case 18:
                data.enforceInterface(IApplicationThread.descriptor);
                updateTimeZone();
                return true;
            case 19:
                data.enforceInterface(IApplicationThread.descriptor);
                processInBackground();
                return true;
            case 20:
                data.enforceInterface(IApplicationThread.descriptor);
                scheduleBindService(data.readStrongBinder(), (Intent) Intent.CREATOR.createFromParcel(data), data.readInt() != 0);
                return true;
            case 21:
                data.enforceInterface(IApplicationThread.descriptor);
                scheduleUnbindService(data.readStrongBinder(), (Intent) Intent.CREATOR.createFromParcel(data));
                return true;
            case 22:
                data.enforceInterface(IApplicationThread.descriptor);
                ParcelFileDescriptor fd = data.readFileDescriptor();
                IBinder service = data.readStrongBinder();
                String[] args2 = new String[0];
                data.readStringArray(args2);
                if (fd != null) {
                    dumpService(fd.getFileDescriptor(), service, args2);
                    try {
                        fd.close();
                    } catch (IOException e) {
                    }
                }
                return true;
            case 23:
                data.enforceInterface(IApplicationThread.descriptor);
                scheduleRegisteredReceiver(IIntentReceiver.Stub.asInterface(data.readStrongBinder()), (Intent) Intent.CREATOR.createFromParcel(data), data.readInt(), data.readString(), data.readBundle(), data.readInt() != 0, data.readInt() != 0);
                return true;
            case 24:
                scheduleLowMemory();
                return true;
            case 25:
                data.enforceInterface(IApplicationThread.descriptor);
                scheduleActivityConfigurationChanged(data.readStrongBinder());
                return true;
            case 26:
                data.enforceInterface(IApplicationThread.descriptor);
                IBinder b = data.readStrongBinder();
                ArrayList createTypedArrayList = data.createTypedArrayList(ResultInfo.CREATOR);
                ArrayList createTypedArrayList2 = data.createTypedArrayList(Intent.CREATOR);
                int configChanges = data.readInt();
                if (data.readInt() != 0) {
                    notResumed = true;
                } else {
                    notResumed = false;
                }
                Configuration config = null;
                if (data.readInt() != 0) {
                    config = (Configuration) Configuration.CREATOR.createFromParcel(data);
                }
                scheduleRelaunchActivity(b, createTypedArrayList, createTypedArrayList2, configChanges, notResumed, config);
                return true;
            case 28:
                data.enforceInterface(IApplicationThread.descriptor);
                profilerControl(data.readInt() != 0, data.readString(), data.readInt() != 0 ? data.readFileDescriptor() : null);
                return true;
            case 29:
                data.enforceInterface(IApplicationThread.descriptor);
                setSchedulingGroup(data.readInt());
                return true;
            case 30:
                data.enforceInterface(IApplicationThread.descriptor);
                scheduleCreateBackupAgent((ApplicationInfo) ApplicationInfo.CREATOR.createFromParcel(data), data.readInt());
                return true;
            case 31:
                data.enforceInterface(IApplicationThread.descriptor);
                scheduleDestroyBackupAgent((ApplicationInfo) ApplicationInfo.CREATOR.createFromParcel(data));
                return true;
            case 32:
                data.enforceInterface(IApplicationThread.descriptor);
                Debug.MemoryInfo mi = new Debug.MemoryInfo();
                getMemoryInfo(mi);
                reply.writeNoException();
                mi.writeToParcel(reply, 0);
                return true;
            case 33:
                data.enforceInterface(IApplicationThread.descriptor);
                scheduleSuicide();
                return true;
            case 34:
                data.enforceInterface(IApplicationThread.descriptor);
                int cmd = data.readInt();
                String[] packages = new String[0];
                data.readStringArray(packages);
                dispatchPackageBroadcast(cmd, packages);
                return true;
            case 35:
                data.enforceInterface(IApplicationThread.descriptor);
                scheduleCrash(data.readString());
                return true;
        }
    }

    public IBinder asBinder() {
        return this;
    }
}
