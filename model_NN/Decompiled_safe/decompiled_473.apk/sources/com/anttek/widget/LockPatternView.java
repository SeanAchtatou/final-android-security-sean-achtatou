package com.anttek.widget;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import org.baole.applocker.R;

public class LockPatternView extends View {
    private static final int ASPECT_LOCK_HEIGHT = 2;
    private static final int ASPECT_LOCK_WIDTH = 1;
    private static final int ASPECT_SQUARE = 0;
    private static final long[] DEFAULT_VIBE_PATTERN = {0, 1, 40, 41};
    private static final int MILLIS_PER_CIRCLE_ANIMATING = 700;
    private static final boolean PROFILE_DRAWING = false;
    static final int STATUS_BAR_HEIGHT = 25;
    private long mAnimatingPeriodStart;
    private int mAspect;
    private Bitmap mBitmapArrowGreenUp;
    private Bitmap mBitmapArrowRedUp;
    private Bitmap mBitmapBtnDefault;
    private Bitmap mBitmapBtnTouched;
    private Bitmap mBitmapCircleDefault;
    private Bitmap mBitmapCircleGreen;
    private Bitmap mBitmapCircleRed;
    private int mBitmapHeight;
    private int mBitmapWidth;
    private final Path mCurrentPath;
    private float mDiameterFactor;
    private boolean mDrawingProfilingStarted;
    private float mHitFactor;
    private float mInProgressX;
    private float mInProgressY;
    private boolean mInStealthMode;
    private boolean mInputEnabled;
    private final Rect mInvalidate;
    private OnPatternListener mOnPatternListener;
    private int mPaddingBottom;
    private int mPaddingLeft;
    private int mPaddingRight;
    private int mPaddingTop;
    private Paint mPaint;
    private Paint mPathPaint;
    private ArrayList<Cell> mPattern;
    private DisplayMode mPatternDisplayMode;
    private boolean[][] mPatternDrawLookup;
    private boolean mPatternInProgress;
    private float mSquareHeight;
    private float mSquareWidth;
    private boolean mTactileFeedbackEnabled;
    private long[] mVibePattern;

    public enum DisplayMode {
        Correct,
        Animate,
        Wrong
    }

    public interface OnPatternListener {
        void onPatternCellAdded(List<Cell> list);

        void onPatternCleared();

        void onPatternDetected(List<Cell> list);

        void onPatternStart();
    }

    public static class Cell {
        static Cell[][] sCells = ((Cell[][]) Array.newInstance(Cell.class, 3, 3));
        int column;
        int row;

        static {
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    sCells[i][j] = new Cell(i, j);
                }
            }
        }

        private Cell(int row2, int column2) {
            checkRange(row2, column2);
            this.row = row2;
            this.column = column2;
        }

        public int getRow() {
            return this.row;
        }

        public int getColumn() {
            return this.column;
        }

        public static synchronized Cell of(int row2, int column2) {
            Cell cell;
            synchronized (Cell.class) {
                checkRange(row2, column2);
                cell = sCells[row2][column2];
            }
            return cell;
        }

        private static void checkRange(int row2, int column2) {
            if (row2 < 0 || row2 > 2) {
                throw new IllegalArgumentException("row must be in range 0-2");
            } else if (column2 < 0 || column2 > 2) {
                throw new IllegalArgumentException("column must be in range 0-2");
            }
        }

        public String toString() {
            return "(row=" + this.row + ",clmn=" + this.column + ")";
        }
    }

    public LockPatternView(Context context) {
        this(context, null);
    }

    public LockPatternView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mDrawingProfilingStarted = PROFILE_DRAWING;
        this.mPaint = new Paint();
        this.mPathPaint = new Paint();
        this.mPattern = new ArrayList<>(9);
        this.mPatternDrawLookup = (boolean[][]) Array.newInstance(Boolean.TYPE, 3, 3);
        this.mInProgressX = -1.0f;
        this.mInProgressY = -1.0f;
        this.mPatternDisplayMode = DisplayMode.Correct;
        this.mInputEnabled = true;
        this.mInStealthMode = PROFILE_DRAWING;
        this.mTactileFeedbackEnabled = true;
        this.mPatternInProgress = PROFILE_DRAWING;
        this.mDiameterFactor = 0.5f;
        this.mHitFactor = 0.6f;
        this.mCurrentPath = new Path();
        this.mInvalidate = new Rect();
        this.mPaddingLeft = 3;
        this.mPaddingRight = 3;
        this.mPaddingBottom = 3;
        this.mPaddingTop = 3;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attrs, R.styleable.LockPatternView);
        if ("square".equals("lock_width")) {
            this.mAspect = 0;
        } else if ("lock_width".equals("lock_width")) {
            this.mAspect = 1;
        } else if ("lock_height".equals("lock_width")) {
            this.mAspect = 2;
        } else {
            this.mAspect = 0;
        }
        setClickable(true);
        this.mPathPaint.setAntiAlias(true);
        this.mPathPaint.setDither(true);
        this.mPathPaint.setColor(-1);
        this.mPathPaint.setAlpha(128);
        this.mPathPaint.setStyle(Paint.Style.STROKE);
        this.mPathPaint.setStrokeJoin(Paint.Join.ROUND);
        this.mPathPaint.setStrokeCap(Paint.Cap.ROUND);
        this.mBitmapBtnDefault = getBitmapFor(org.baole.albs.R.drawable.btn_code_lock_default);
        this.mBitmapBtnTouched = getBitmapFor(org.baole.albs.R.drawable.btn_code_lock_touched);
        this.mBitmapCircleDefault = getBitmapFor(org.baole.albs.R.drawable.indicator_code_lock_point_area_default);
        this.mBitmapCircleGreen = getBitmapFor(org.baole.albs.R.drawable.indicator_code_lock_point_area_green);
        this.mBitmapCircleRed = getBitmapFor(org.baole.albs.R.drawable.indicator_code_lock_point_area_red);
        this.mBitmapArrowGreenUp = getBitmapFor(org.baole.albs.R.drawable.indicator_code_lock_drag_direction_green_up);
        this.mBitmapArrowRedUp = getBitmapFor(org.baole.albs.R.drawable.indicator_code_lock_drag_direction_red_up);
        this.mBitmapWidth = this.mBitmapBtnDefault.getWidth();
        this.mBitmapHeight = this.mBitmapBtnDefault.getHeight();
        this.mVibePattern = loadVibratePattern(org.baole.albs.R.array.config_virtualKeyVibePattern);
    }

    private long[] loadVibratePattern(int id) {
        int[] pattern = null;
        try {
            pattern = getResources().getIntArray(id);
        } catch (Resources.NotFoundException e) {
            Log.e("LockPatternView", "Vibrate pattern missing, using default", e);
        }
        if (pattern == null) {
            return DEFAULT_VIBE_PATTERN;
        }
        long[] tmpPattern = new long[pattern.length];
        for (int i = 0; i < pattern.length; i++) {
            tmpPattern[i] = (long) pattern[i];
        }
        return tmpPattern;
    }

    private Bitmap getBitmapFor(int resId) {
        return BitmapFactory.decodeResource(getContext().getResources(), resId);
    }

    public boolean isInStealthMode() {
        return this.mInStealthMode;
    }

    public boolean isTactileFeedbackEnabled() {
        return this.mTactileFeedbackEnabled;
    }

    public void setInStealthMode(boolean inStealthMode) {
        this.mInStealthMode = inStealthMode;
    }

    public void setTactileFeedbackEnabled(boolean tactileFeedbackEnabled) {
        this.mTactileFeedbackEnabled = tactileFeedbackEnabled;
    }

    public void setOnPatternListener(OnPatternListener onPatternListener) {
        this.mOnPatternListener = onPatternListener;
    }

    public void setPattern(DisplayMode displayMode, List<Cell> pattern) {
        this.mPattern.clear();
        this.mPattern.addAll(pattern);
        clearPatternDrawLookup();
        for (Cell cell : pattern) {
            this.mPatternDrawLookup[cell.getRow()][cell.getColumn()] = true;
        }
        setDisplayMode(displayMode);
    }

    public void setDisplayMode(DisplayMode displayMode) {
        this.mPatternDisplayMode = displayMode;
        if (displayMode == DisplayMode.Animate) {
            if (this.mPattern.size() == 0) {
                throw new IllegalStateException("you must have a pattern to animate if you want to set the display mode to animate");
            }
            this.mAnimatingPeriodStart = SystemClock.elapsedRealtime();
            Cell first = this.mPattern.get(0);
            this.mInProgressX = getCenterXForColumn(first.getColumn());
            this.mInProgressY = getCenterYForRow(first.getRow());
            clearPatternDrawLookup();
        }
        invalidate();
    }

    public void clearPattern() {
        resetPattern();
    }

    private void resetPattern() {
        this.mPattern.clear();
        clearPatternDrawLookup();
        this.mPatternDisplayMode = DisplayMode.Correct;
        invalidate();
    }

    private void clearPatternDrawLookup() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                this.mPatternDrawLookup[i][j] = PROFILE_DRAWING;
            }
        }
    }

    public void disableInput() {
        this.mInputEnabled = PROFILE_DRAWING;
    }

    public void enableInput() {
        this.mInputEnabled = true;
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        this.mSquareWidth = ((float) ((w - this.mPaddingLeft) - this.mPaddingRight)) / 3.0f;
        this.mSquareHeight = ((float) ((h - this.mPaddingTop) - this.mPaddingBottom)) / 3.0f;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = View.MeasureSpec.getSize(widthMeasureSpec);
        int height = View.MeasureSpec.getSize(heightMeasureSpec);
        int viewWidth = width;
        int viewHeight = height;
        switch (this.mAspect) {
            case 0:
                viewHeight = Math.min(width, height);
                viewWidth = viewHeight;
                break;
            case 1:
                viewWidth = width;
                viewHeight = Math.min(width, height);
                break;
            case 2:
                viewWidth = Math.min(width, height);
                viewHeight = height;
                break;
        }
        setMeasuredDimension(viewWidth, viewHeight);
    }

    private Cell detectAndAddHit(float x, float y) {
        int i;
        int i2;
        Cell cell = checkForNewHit(x, y);
        if (cell == null) {
            return null;
        }
        Cell fillInGapCell = null;
        ArrayList<Cell> pattern = this.mPattern;
        if (!pattern.isEmpty()) {
            Cell lastCell = pattern.get(pattern.size() - 1);
            int dRow = cell.row - lastCell.row;
            int dColumn = cell.column - lastCell.column;
            int fillInRow = lastCell.row;
            int fillInColumn = lastCell.column;
            if (Math.abs(dRow) == 2 && Math.abs(dColumn) != 1) {
                int i3 = lastCell.row;
                if (dRow > 0) {
                    i2 = 1;
                } else {
                    i2 = -1;
                }
                fillInRow = i3 + i2;
            }
            if (Math.abs(dColumn) == 2 && Math.abs(dRow) != 1) {
                int i4 = lastCell.column;
                if (dColumn > 0) {
                    i = 1;
                } else {
                    i = -1;
                }
                fillInColumn = i4 + i;
            }
            fillInGapCell = Cell.of(fillInRow, fillInColumn);
        }
        if (fillInGapCell != null && !this.mPatternDrawLookup[fillInGapCell.row][fillInGapCell.column]) {
            addCellToPattern(fillInGapCell);
        }
        addCellToPattern(cell);
        if (this.mTactileFeedbackEnabled) {
        }
        return cell;
    }

    private void addCellToPattern(Cell newCell) {
        this.mPatternDrawLookup[newCell.getRow()][newCell.getColumn()] = true;
        this.mPattern.add(newCell);
        if (this.mOnPatternListener != null) {
            this.mOnPatternListener.onPatternCellAdded(this.mPattern);
        }
    }

    private Cell checkForNewHit(float x, float y) {
        int rowHit = getRowHit(y);
        if (rowHit < 0) {
            return null;
        }
        int columnHit = getColumnHit(x);
        if (columnHit < 0) {
            return null;
        }
        if (this.mPatternDrawLookup[rowHit][columnHit]) {
            return null;
        }
        return Cell.of(rowHit, columnHit);
    }

    private int getRowHit(float y) {
        float squareHeight = this.mSquareHeight;
        float hitSize = squareHeight * this.mHitFactor;
        float offset = ((float) this.mPaddingTop) + ((squareHeight - hitSize) / 2.0f);
        for (int i = 0; i < 3; i++) {
            float hitTop = offset + (((float) i) * squareHeight);
            if (y >= hitTop && y <= hitTop + hitSize) {
                return i;
            }
        }
        return -1;
    }

    private int getColumnHit(float x) {
        float squareWidth = this.mSquareWidth;
        float hitSize = squareWidth * this.mHitFactor;
        float offset = ((float) this.mPaddingLeft) + ((squareWidth - hitSize) / 2.0f);
        for (int i = 0; i < 3; i++) {
            float hitLeft = offset + (((float) i) * squareWidth);
            if (x >= hitLeft && x <= hitLeft + hitSize) {
                return i;
            }
        }
        return -1;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        float left;
        float right;
        float top;
        float bottom;
        float left2;
        float right2;
        float top2;
        float bottom2;
        float right3;
        float left3;
        float bottom3;
        float top3;
        if (!this.mInputEnabled || !isEnabled()) {
            return PROFILE_DRAWING;
        }
        float x = motionEvent.getX();
        float y = motionEvent.getY();
        switch (motionEvent.getAction()) {
            case 0:
                resetPattern();
                Cell hitCell = detectAndAddHit(x, y);
                if (hitCell != null && this.mOnPatternListener != null) {
                    this.mPatternInProgress = true;
                    this.mPatternDisplayMode = DisplayMode.Correct;
                    this.mOnPatternListener.onPatternStart();
                } else if (this.mOnPatternListener != null) {
                    this.mPatternInProgress = PROFILE_DRAWING;
                    this.mOnPatternListener.onPatternCleared();
                }
                if (hitCell != null) {
                    float startX = getCenterXForColumn(hitCell.column);
                    float startY = getCenterYForRow(hitCell.row);
                    float widthOffset = this.mSquareWidth / 2.0f;
                    float heightOffset = this.mSquareHeight / 2.0f;
                    invalidate((int) (startX - widthOffset), (int) (startY - heightOffset), (int) (startX + widthOffset), (int) (startY + heightOffset));
                }
                this.mInProgressX = x;
                this.mInProgressY = y;
                return true;
            case 1:
                if (!this.mPattern.isEmpty() && this.mOnPatternListener != null) {
                    this.mPatternInProgress = PROFILE_DRAWING;
                    this.mOnPatternListener.onPatternDetected(this.mPattern);
                    invalidate();
                }
                return true;
            case 2:
                int patternSizePreHitDetect = this.mPattern.size();
                Cell hitCell2 = detectAndAddHit(x, y);
                int patternSize = this.mPattern.size();
                if (!(hitCell2 == null || this.mOnPatternListener == null || patternSize != 1)) {
                    this.mPatternInProgress = true;
                    this.mOnPatternListener.onPatternStart();
                }
                if (Math.abs(x - this.mInProgressX) + Math.abs(y - this.mInProgressY) > this.mSquareWidth * 0.01f) {
                    float oldX = this.mInProgressX;
                    float oldY = this.mInProgressY;
                    this.mInProgressX = x;
                    this.mInProgressY = y;
                    if (!this.mPatternInProgress || patternSize <= 0) {
                        invalidate();
                    } else {
                        ArrayList<Cell> pattern = this.mPattern;
                        float radius = this.mSquareWidth * this.mDiameterFactor * 0.5f;
                        Cell lastCell = pattern.get(patternSize - 1);
                        float startX2 = getCenterXForColumn(lastCell.column);
                        float startY2 = getCenterYForRow(lastCell.row);
                        Rect invalidateRect = this.mInvalidate;
                        if (startX2 < x) {
                            left = startX2;
                            right = x;
                        } else {
                            left = x;
                            right = startX2;
                        }
                        if (startY2 < y) {
                            top = startY2;
                            bottom = y;
                        } else {
                            top = y;
                            bottom = startY2;
                        }
                        invalidateRect.set((int) (left - radius), (int) (top - radius), (int) (right + radius), (int) (bottom + radius));
                        if (startX2 < oldX) {
                            left2 = startX2;
                            right2 = oldX;
                        } else {
                            left2 = oldX;
                            right2 = startX2;
                        }
                        if (startY2 < oldY) {
                            top2 = startY2;
                            bottom2 = oldY;
                        } else {
                            top2 = oldY;
                            bottom2 = startY2;
                        }
                        invalidateRect.union((int) (left2 - radius), (int) (top2 - radius), (int) (right2 + radius), (int) (bottom2 + radius));
                        if (hitCell2 != null) {
                            float startX3 = getCenterXForColumn(hitCell2.column);
                            float startY3 = getCenterYForRow(hitCell2.row);
                            if (patternSize >= 2) {
                                Cell hitCell3 = pattern.get((patternSize - 1) - (patternSize - patternSizePreHitDetect));
                                float oldX2 = getCenterXForColumn(hitCell3.column);
                                float oldY2 = getCenterYForRow(hitCell3.row);
                                if (startX3 < oldX2) {
                                    left3 = startX3;
                                    right3 = oldX2;
                                } else {
                                    left3 = oldX2;
                                    right3 = startX3;
                                }
                                if (startY3 < oldY2) {
                                    top3 = startY3;
                                    bottom3 = oldY2;
                                } else {
                                    top3 = oldY2;
                                    bottom3 = startY3;
                                }
                            } else {
                                right3 = startX3;
                                left3 = startX3;
                                bottom3 = startY3;
                                top3 = startY3;
                            }
                            float widthOffset2 = this.mSquareWidth / 2.0f;
                            float heightOffset2 = this.mSquareHeight / 2.0f;
                            invalidateRect.set((int) (left3 - widthOffset2), (int) (top3 - heightOffset2), (int) (right3 + widthOffset2), (int) (bottom3 + heightOffset2));
                        }
                        invalidate(invalidateRect);
                    }
                }
                return true;
            case 3:
                resetPattern();
                if (this.mOnPatternListener != null) {
                    this.mPatternInProgress = PROFILE_DRAWING;
                    this.mOnPatternListener.onPatternCleared();
                }
                return true;
            default:
                return PROFILE_DRAWING;
        }
    }

    private float getCenterXForColumn(int column) {
        return ((float) this.mPaddingLeft) + (((float) column) * this.mSquareWidth) + (this.mSquareWidth / 2.0f);
    }

    private float getCenterYForRow(int row) {
        return ((float) this.mPaddingTop) + (((float) row) * this.mSquareHeight) + (this.mSquareHeight / 2.0f);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        boolean drawPath;
        boolean oldFlag;
        boolean needToUpdateInProgressPoint;
        ArrayList<Cell> pattern = this.mPattern;
        int count = pattern.size();
        boolean[][] drawLookup = this.mPatternDrawLookup;
        if (this.mPatternDisplayMode == DisplayMode.Animate) {
            int spotInCycle = ((int) (SystemClock.elapsedRealtime() - this.mAnimatingPeriodStart)) % ((count + 1) * MILLIS_PER_CIRCLE_ANIMATING);
            int numCircles = spotInCycle / MILLIS_PER_CIRCLE_ANIMATING;
            clearPatternDrawLookup();
            for (int i = 0; i < numCircles; i++) {
                Cell cell = pattern.get(i);
                drawLookup[cell.getRow()][cell.getColumn()] = true;
            }
            if (numCircles <= 0 || numCircles >= count) {
                needToUpdateInProgressPoint = false;
            } else {
                needToUpdateInProgressPoint = true;
            }
            if (needToUpdateInProgressPoint) {
                float percentageOfNextCircle = ((float) (spotInCycle % MILLIS_PER_CIRCLE_ANIMATING)) / 700.0f;
                Cell currentCell = pattern.get(numCircles - 1);
                float centerX = getCenterXForColumn(currentCell.column);
                float centerY = getCenterYForRow(currentCell.row);
                Cell nextCell = pattern.get(numCircles);
                float dx = percentageOfNextCircle * (getCenterXForColumn(nextCell.column) - centerX);
                float dy = percentageOfNextCircle * (getCenterYForRow(nextCell.row) - centerY);
                this.mInProgressX = centerX + dx;
                this.mInProgressY = centerY + dy;
            }
            invalidate();
        }
        float squareWidth = this.mSquareWidth;
        float squareHeight = this.mSquareHeight;
        this.mPathPaint.setStrokeWidth(this.mDiameterFactor * squareWidth * 0.5f);
        Path currentPath = this.mCurrentPath;
        currentPath.rewind();
        if (!this.mInStealthMode || this.mPatternDisplayMode == DisplayMode.Wrong) {
            drawPath = true;
        } else {
            drawPath = false;
        }
        if (drawPath) {
            boolean anyCircles = PROFILE_DRAWING;
            for (int i2 = 0; i2 < count; i2++) {
                Cell cell2 = pattern.get(i2);
                if (!drawLookup[cell2.row][cell2.column]) {
                    break;
                }
                anyCircles = true;
                float centerX2 = getCenterXForColumn(cell2.column);
                float centerY2 = getCenterYForRow(cell2.row);
                if (i2 == 0) {
                    currentPath.moveTo(centerX2, centerY2);
                } else {
                    currentPath.lineTo(centerX2, centerY2);
                }
            }
            if ((this.mPatternInProgress || this.mPatternDisplayMode == DisplayMode.Animate) && anyCircles) {
                currentPath.lineTo(this.mInProgressX, this.mInProgressY);
            }
            canvas.drawPath(currentPath, this.mPathPaint);
        }
        int paddingTop = this.mPaddingTop;
        int paddingLeft = this.mPaddingLeft;
        for (int i3 = 0; i3 < 3; i3++) {
            float topY = ((float) paddingTop) + (((float) i3) * squareHeight);
            for (int j = 0; j < 3; j++) {
                drawCircle(canvas, (int) (((float) paddingLeft) + (((float) j) * squareWidth)), (int) topY, drawLookup[i3][j]);
            }
        }
        if ((this.mPaint.getFlags() & 2) != 0) {
            oldFlag = true;
        } else {
            oldFlag = false;
        }
        this.mPaint.setFilterBitmap(true);
        if (drawPath) {
            for (int i4 = 0; i4 < count - 1; i4++) {
                Cell cell3 = pattern.get(i4);
                Cell next = pattern.get(i4 + 1);
                if (!drawLookup[next.row][next.column]) {
                    break;
                }
                drawArrow(canvas, ((float) paddingLeft) + (((float) cell3.column) * squareWidth), ((float) paddingTop) + (((float) cell3.row) * squareHeight), cell3, next);
            }
        }
        this.mPaint.setFilterBitmap(oldFlag);
    }

    private void drawArrow(Canvas canvas, float leftX, float topY, Cell start, Cell end) {
        boolean green;
        if (this.mPatternDisplayMode != DisplayMode.Wrong) {
            green = true;
        } else {
            green = false;
        }
        int endRow = end.row;
        int startRow = start.row;
        int endColumn = end.column;
        int startColumn = start.column;
        int offsetX = (((int) this.mSquareWidth) - this.mBitmapWidth) / 2;
        int offsetY = (((int) this.mSquareHeight) - this.mBitmapHeight) / 2;
        Bitmap arrow = green ? this.mBitmapArrowGreenUp : this.mBitmapArrowRedUp;
        Matrix matrix = new Matrix();
        int cellWidth = this.mBitmapCircleDefault.getWidth();
        int cellHeight = this.mBitmapCircleDefault.getHeight();
        float angle = ((float) Math.toDegrees((double) ((float) Math.atan2((double) (endRow - startRow), (double) (endColumn - startColumn))))) + 90.0f;
        matrix.setTranslate(((float) offsetX) + leftX, ((float) offsetY) + topY);
        matrix.preRotate(angle, ((float) cellWidth) / 2.0f, ((float) cellHeight) / 2.0f);
        matrix.preTranslate(((float) (cellWidth - arrow.getWidth())) / 2.0f, 0.0f);
        canvas.drawBitmap(arrow, matrix, this.mPaint);
    }

    private void drawCircle(Canvas canvas, int leftX, int topY, boolean partOfPattern) {
        Bitmap outerCircle;
        Bitmap innerCircle;
        if (!partOfPattern || (this.mInStealthMode && this.mPatternDisplayMode != DisplayMode.Wrong)) {
            outerCircle = this.mBitmapCircleDefault;
            innerCircle = this.mBitmapBtnDefault;
        } else if (this.mPatternInProgress) {
            outerCircle = this.mBitmapCircleGreen;
            innerCircle = this.mBitmapBtnTouched;
        } else if (this.mPatternDisplayMode == DisplayMode.Wrong) {
            outerCircle = this.mBitmapCircleRed;
            innerCircle = this.mBitmapBtnDefault;
        } else if (this.mPatternDisplayMode == DisplayMode.Correct || this.mPatternDisplayMode == DisplayMode.Animate) {
            outerCircle = this.mBitmapCircleGreen;
            innerCircle = this.mBitmapBtnDefault;
        } else {
            throw new IllegalStateException("unknown display mode " + this.mPatternDisplayMode);
        }
        int width = this.mBitmapWidth;
        int height = this.mBitmapHeight;
        int offsetX = (int) ((this.mSquareWidth - ((float) width)) / 2.0f);
        int offsetY = (int) ((this.mSquareHeight - ((float) height)) / 2.0f);
        canvas.drawBitmap(outerCircle, (float) (leftX + offsetX), (float) (topY + offsetY), this.mPaint);
        canvas.drawBitmap(innerCircle, (float) (leftX + offsetX), (float) (topY + offsetY), this.mPaint);
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        return new SavedState(super.onSaveInstanceState(), LockPatternUtils.patternToString(this.mPattern), this.mPatternDisplayMode.ordinal(), this.mInputEnabled, this.mInStealthMode, this.mTactileFeedbackEnabled);
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable state) {
        SavedState ss = (SavedState) state;
        super.onRestoreInstanceState(ss.getSuperState());
        setPattern(DisplayMode.Correct, LockPatternUtils.stringToPattern(ss.getSerializedPattern()));
        this.mPatternDisplayMode = DisplayMode.values()[ss.getDisplayMode()];
        this.mInputEnabled = ss.isInputEnabled();
        this.mInStealthMode = ss.isInStealthMode();
        this.mTactileFeedbackEnabled = ss.isTactileFeedbackEnabled();
    }

    private static class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
            public SavedState createFromParcel(Parcel in) {
                return new SavedState(in);
            }

            public SavedState[] newArray(int size) {
                return new SavedState[size];
            }
        };
        private final int mDisplayMode;
        private final boolean mInStealthMode;
        private final boolean mInputEnabled;
        private final String mSerializedPattern;
        private final boolean mTactileFeedbackEnabled;

        private SavedState(Parcelable superState, String serializedPattern, int displayMode, boolean inputEnabled, boolean inStealthMode, boolean tactileFeedbackEnabled) {
            super(superState);
            this.mSerializedPattern = serializedPattern;
            this.mDisplayMode = displayMode;
            this.mInputEnabled = inputEnabled;
            this.mInStealthMode = inStealthMode;
            this.mTactileFeedbackEnabled = tactileFeedbackEnabled;
        }

        private SavedState(Parcel in) {
            super(in);
            this.mSerializedPattern = in.readString();
            this.mDisplayMode = in.readInt();
            this.mInputEnabled = ((Boolean) in.readValue(null)).booleanValue();
            this.mInStealthMode = ((Boolean) in.readValue(null)).booleanValue();
            this.mTactileFeedbackEnabled = ((Boolean) in.readValue(null)).booleanValue();
        }

        public String getSerializedPattern() {
            return this.mSerializedPattern;
        }

        public int getDisplayMode() {
            return this.mDisplayMode;
        }

        public boolean isInputEnabled() {
            return this.mInputEnabled;
        }

        public boolean isInStealthMode() {
            return this.mInStealthMode;
        }

        public boolean isTactileFeedbackEnabled() {
            return this.mTactileFeedbackEnabled;
        }

        public void writeToParcel(Parcel dest, int flags) {
            super.writeToParcel(dest, flags);
            dest.writeString(this.mSerializedPattern);
            dest.writeInt(this.mDisplayMode);
            dest.writeValue(Boolean.valueOf(this.mInputEnabled));
            dest.writeValue(Boolean.valueOf(this.mInStealthMode));
            dest.writeValue(Boolean.valueOf(this.mTactileFeedbackEnabled));
        }
    }
}
