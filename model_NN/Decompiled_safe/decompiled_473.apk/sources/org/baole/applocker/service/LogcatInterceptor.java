package org.baole.applocker.service;

public interface LogcatInterceptor {
    void onError(String str, Throwable th);

    void onNewline(String str);
}
