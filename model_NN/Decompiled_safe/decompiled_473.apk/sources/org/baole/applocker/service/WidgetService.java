package org.baole.applocker.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.widget.Toast;
import org.baole.albs.R;
import org.baole.applocker.activity.WidgetUnlockActivity;
import org.baole.applocker.model.Configuration;
import org.baole.applocker.widget.AppLockerWidget;

public class WidgetService extends Service {
    public void onCreate() {
        super.onCreate();
        Configuration conf = Configuration.getInstance(getApplicationContext());
        Context c = getApplicationContext();
        if (conf.mEnable) {
            Intent i = new Intent(c, WidgetUnlockActivity.class);
            i.setAction("org.baole.applocker.ACTION_WIDGET_CONFIG");
            i.addFlags(268435456);
            i.addFlags(2097152);
            i.addFlags(134217728);
            startActivity(i);
        } else {
            conf.setEnable(true);
            AppLockerWidget.updateRemoteView(c);
            Toast.makeText(this, conf.mEnable ? R.string.app_on : R.string.app_off, 0).show();
            Toast.makeText(c, (int) R.string.app_on, 0);
        }
        stopSelf();
    }

    public IBinder onBind(Intent intent) {
        return null;
    }
}
