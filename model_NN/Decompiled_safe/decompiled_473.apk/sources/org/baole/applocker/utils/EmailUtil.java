package org.baole.applocker.utils;

import android.content.Context;
import com.anttek.widget.LockPatternUtils;
import com.anttek.widget.LockPatternView;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.baole.albs.R;
import org.baole.applocker.model.Configuration;

public class EmailUtil {
    public static String buildRecoveryMessage(Context context) {
        Configuration c = Configuration.getInstance(context);
        List<LockPatternView.Cell> pattern = LockPatternUtils.stringToPattern(c.mDefaultPattern.mExtra2);
        StringBuffer patternStr = new StringBuffer();
        for (LockPatternView.Cell cell : pattern) {
            patternStr.append("" + (cell.getColumn() + (cell.getRow() * 3) + 1));
        }
        return context.getString(R.string.recovery_message, c.mDefaultPIN.mExtra2, c.mDefaultPassword.mExtra2, patternStr.toString(), c.mDefaultDial.mExtra2, Long.valueOf(c.mDefaultBlackScreen.mExtra1));
    }

    public static String sendEmail(String to, String message) {
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://apis.anttek.com/aal_email_recovery.php");
        try {
            ArrayList<NameValuePair> nameValuePairs = new ArrayList<>(2);
            nameValuePairs.add(new BasicNameValuePair("uid", "0d5e4bc0-50b3-4323-9952-3c9dce770619"));
            nameValuePairs.add(new BasicNameValuePair("to", to));
            nameValuePairs.add(new BasicNameValuePair("message", message));
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            httppost.addHeader("User-Agent", "3a9e88c5-189a-4c8f-9231-980b51402ae8");
            return EntityUtils.toString(httpclient.execute(httppost).getEntity());
        } catch (IOException | ClientProtocolException e) {
            return null;
        }
    }
}
