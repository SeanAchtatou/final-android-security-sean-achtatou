package org.baole.applocker.utils;

import android.app.Activity;
import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class BitmapUtil {
    public static String ASSET_ICON_FOLDER = "icons";

    public static Bitmap fromByte(byte[] dat) {
        if (dat == null || dat.length == 0) {
            return null;
        }
        try {
            return BitmapFactory.decodeByteArray(dat, 0, dat.length);
        } catch (Throwable th) {
            return null;
        }
    }

    public static byte[] toByte(Bitmap bitmap) {
        if (bitmap == null) {
            return null;
        }
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, os);
        return os.toByteArray();
    }

    public static Bitmap resizeBitmap(Bitmap src, float maxWidth, float maxHeight) {
        float factor;
        if (maxWidth > ((float) src.getWidth()) && maxHeight > ((float) src.getHeight())) {
            return src;
        }
        float factorWidth = ((float) src.getWidth()) / maxWidth;
        float factorHeight = ((float) src.getHeight()) / maxHeight;
        if (factorHeight > factorWidth) {
            factor = factorHeight;
        } else {
            factor = factorWidth;
        }
        return Bitmap.createScaledBitmap(src, (int) (((float) src.getWidth()) / factor), (int) (((float) src.getHeight()) / factor), false);
    }

    public static String getResourceDir(Activity context) {
        return ASSET_ICON_FOLDER;
    }

    public static Bitmap fromResource(Context context, int id) {
        return BitmapFactory.decodeResource(context.getResources(), id);
    }

    public static Bitmap fromAssets(AssetManager am, String filename) {
        try {
            return BitmapFactory.decodeStream(am.open(filename));
        } catch (IOException e) {
            return null;
        }
    }

    public static byte[] toByte(Context context, int id) {
        return toByte(BitmapFactory.decodeResource(context.getResources(), id));
    }
}
