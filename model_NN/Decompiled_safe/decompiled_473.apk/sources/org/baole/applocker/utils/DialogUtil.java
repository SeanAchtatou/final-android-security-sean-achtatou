package org.baole.applocker.utils;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.widget.Toast;
import org.baole.albs.R;

public class DialogUtil {
    public static final Dialog createChangLogDialog(Context context, int titleId, int updateArrayId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(titleId);
        String[] changes = context.getResources().getStringArray(updateArrayId);
        StringBuilder buf = new StringBuilder(changes[0]);
        for (int i = 1; i < changes.length; i++) {
            buf.append("\n\n");
            buf.append(changes[i]);
        }
        builder.setIcon(17301569);
        builder.setMessage(buf.toString());
        builder.setCancelable(true);
        builder.setPositiveButton(17039370, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        return builder.create();
    }

    public static final Dialog createGoProDialog(final Context context, final String pkg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setIcon(17301659).setTitle((int) R.string.pro_version).setMessage((int) R.string.pro_alert).setPositiveButton((int) R.string.purchase, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(String.format("market://details?id=%s", pkg))));
            }
        }).setNegativeButton((int) R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        return builder.create();
    }

    public static final Dialog createQuestionDialog(final Context context, final String url, int quickhelpId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setIcon(17301659).setTitle((int) R.string.quick_help).setMessage(quickhelpId).setPositiveButton((int) R.string.online_help, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url)));
            }
        }).setNegativeButton((int) R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        return builder.create();
    }

    public static final Dialog createInfoDialog(Context context, int titleId, int infoId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setIcon(17301659).setTitle(titleId).setMessage(infoId).setNeutralButton(17039370, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        return builder.create();
    }

    public static final Dialog createInfoDialog(Context context, String title, String info) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setIcon(17301659).setTitle(title).setMessage(info).setNeutralButton(17039370, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        return builder.create();
    }

    public static void showToast(Context c, int id) {
        Toast.makeText(c, id, 0).show();
    }
}
