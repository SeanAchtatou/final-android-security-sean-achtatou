package org.baole.applocker.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.RemoteViews;
import org.baole.albs.R;
import org.baole.applocker.model.Configuration;
import org.baole.applocker.service.WidgetService;

public class AppLockerWidget extends AppWidgetProvider {
    private final String TAG = getClass().getSimpleName();
    Context context;

    public void onEnabled(Context context2) {
        Log.d(this.TAG, "onEnabled()");
        super.onEnabled(context2);
    }

    public void onReceive(Context context2, Intent intent) {
        Log.d(this.TAG, "onReceive()");
        super.onReceive(context2, intent);
    }

    public void onUpdate(Context context2, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        super.onUpdate(context2, appWidgetManager, appWidgetIds);
        Log.d(this.TAG, "onUpdate()");
        this.context = context2;
        updateRemoteView(context2);
    }

    public static void updateRemoteView(Context c) {
        Configuration mConf = Configuration.getInstance(c.getApplicationContext());
        RemoteViews remote = new RemoteViews(c.getPackageName(), (int) R.layout.widget);
        PendingIntent pi = PendingIntent.getService(c, 0, new Intent(c, WidgetService.class), 0);
        remote.setOnClickPendingIntent(R.id.status_layout, pi);
        remote.setOnClickPendingIntent(R.id.image_enable, pi);
        remote.setImageViewResource(R.id.image_enable, mConf.mEnable ? R.drawable.locked : R.drawable.unlocked);
        AppWidgetManager.getInstance(c).updateAppWidget(new ComponentName(c, AppLockerWidget.class), remote);
    }
}
