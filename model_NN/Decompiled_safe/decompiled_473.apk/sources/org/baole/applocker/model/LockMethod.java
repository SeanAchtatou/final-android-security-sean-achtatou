package org.baole.applocker.model;

import android.app.IApplicationThread;

public enum LockMethod {
    NONE(0),
    PIN(1),
    PASSWORD(2),
    PATTERN(3),
    DIALOG(4),
    DIAL(5),
    USE_DEFAULT(6),
    TEXT_SCREEN(7),
    IMAGE_SCREEN(8),
    FORCE_CLOSE(9);
    
    int mType;

    private LockMethod(int type) {
        this.mType = type;
    }

    public int getType() {
        return this.mType;
    }

    public static LockMethod getMethod(int t) {
        switch (t) {
            case 1:
                return PIN;
            case 2:
                return PASSWORD;
            case 3:
                return PATTERN;
            case 4:
                return DIALOG;
            case 5:
                return DIAL;
            case 6:
                return USE_DEFAULT;
            case IApplicationThread.SCHEDULE_LAUNCH_ACTIVITY_TRANSACTION:
                return TEXT_SCREEN;
            case 8:
                return IMAGE_SCREEN;
            case IApplicationThread.SCHEDULE_FINISH_ACTIVITY_TRANSACTION:
                return FORCE_CLOSE;
            default:
                return NONE;
        }
    }
}
