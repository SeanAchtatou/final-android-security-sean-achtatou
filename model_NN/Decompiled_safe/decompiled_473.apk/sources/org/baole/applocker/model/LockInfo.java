package org.baole.applocker.model;

public class LockInfo {
    public boolean mHasUnlocked = false;
    public String mPkg = "";
    public long mTime = -1;
}
