package org.baole.applocker.model;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import java.util.HashMap;
import org.baole.albs.R;
import org.baole.applocker.db.DbHelper;

public class Configuration {
    private static String CONFIG = "config.txt";
    public static final String FALSE = "false";
    public static final String IMAGE_SOURCE = "_image_src";
    public static final String KEY_CURRENT_ACT = "_current_act";
    public static final String KEY_CURRENT_PKG = "_current_pkg";
    public static final String KEY_DEFAULT_DIALOG_MSG = "default_dialog_msg";
    public static final String KEY_DEFAULT_DIALOG_NO_CLICK = "default_no_click";
    public static final String KEY_DEFAULT_DIAL_NUMBER = "default_dial_number";
    public static final String KEY_DEFAULT_LOCK_METHOD = "default_lock_method";
    public static final String KEY_DEFAULT_LOCK_SETTING = "default_lock_setting";
    public static final String KEY_DEFAULT_PASSWORD = "default_password";
    public static final String KEY_DEFAULT_PATTERN = "default_pattern";
    public static final String KEY_DEFAULT_PIN = "default_pin";
    public static final String KEY_ENABLE = "enable";
    public static final String KEY_HINT_PASS = "_hint_pass";
    public static final String KEY_HINT_PATTERN = "_hint_pattern";
    public static final String KEY_HINT_PIN = "_hint_pin";
    public static final String KEY_INVISIBLE_PATTERN = "invisilbe_pattern";
    public static final String KEY_LOCK_ALL = "lock_all";
    public static final String KEY_RECOVERY_EMAIL = "_rec_email";
    public static final String KEY_UNLOCKED = "_unlocked";
    public static final String KEY_UNLOCK_ONE_ALL = "unlock_one_all";
    public static final String KEY_VALIDITY_PERIOD = "validity_period";
    public static final String KEY_WRONG_TO_CLOSE = "_wrong_cls";
    public static final String PREF_LAST_VERSION = "PREF_LAST_VERSION";
    public static final String PREF_SCAN_APPS2 = "PREF_SCAN_APPS2";
    public static final String SEARCH_TERM = "_st";
    private static final String TEXT_TEMPLATE = "_text_template";
    public static final String TRUE = "true";
    private static Configuration instance = null;
    Context mContext = null;
    public String mCurrentUnLockPkg = null;
    public Extras mDefaultBlackScreen = null;
    public Extras mDefaultDial = null;
    public LockMethod mDefaultLockMethod = LockMethod.NONE;
    public Extras mDefaultPIN = null;
    public Extras mDefaultPassword = null;
    public Extras mDefaultPattern = null;
    public boolean mEnable = true;
    public boolean mHasUnLock = false;
    DbHelper mHelper = null;
    public boolean mInvislblePattern;
    public String mLastResumingPkg = null;
    public Long mLastUnlock = -1L;
    public int mLastUnlockMethod = 0;
    public String mLastUnlockPkg = null;
    public String mLatestImageSrc;
    public String mLatestTextTemplate;
    public boolean mLockAll = false;
    private String mPassHint;
    private String mPatternHint;
    private String mPinHint;
    private String mRecoveryEmail;
    public boolean mResetToDefault = true;
    public String mSearchTerm;
    public boolean mUnLockOneAll = false;
    public HashMap<String, LockInfo> mUnlockedCaches = new HashMap<>();
    public int mValidity = 0;
    private int mWrongToClose;

    public static class Extras {
        public long mExtra1 = -1;
        public String mExtra2 = null;
        public String mExtra3 = null;
    }

    public static Configuration getInstance(Context c) {
        if (instance == null) {
            synchronized (Configuration.class) {
                instance = new Configuration();
                instance.init(c);
            }
        }
        return instance;
    }

    private void init(Context c) {
        this.mContext = c;
        this.mHelper = DbHelper.getInstance(c);
        this.mDefaultPIN = new Extras();
        this.mDefaultPassword = new Extras();
        this.mDefaultPattern = new Extras();
        this.mDefaultBlackScreen = new Extras();
        this.mDefaultDial = new Extras();
        if (isConfExists()) {
            readDb();
            return;
        }
        readPref();
        writeDb();
    }

    public void readPref() {
        SharedPreferences mPref = PreferenceManager.getDefaultSharedPreferences(this.mContext);
        this.mEnable = mPref.getBoolean(KEY_ENABLE, true);
        this.mLockAll = mPref.getBoolean(KEY_LOCK_ALL, false);
        this.mInvislblePattern = mPref.getBoolean(KEY_INVISIBLE_PATTERN, false);
        this.mUnLockOneAll = mPref.getBoolean(KEY_UNLOCK_ONE_ALL, false);
        try {
            this.mValidity = Integer.parseInt(mPref.getString(KEY_VALIDITY_PERIOD, "0"));
        } catch (Throwable th) {
            this.mValidity = 0;
            th.printStackTrace();
        }
        this.mDefaultPIN.mExtra2 = mPref.getString(KEY_DEFAULT_PIN, "888");
        this.mDefaultPassword.mExtra2 = mPref.getString(KEY_DEFAULT_PASSWORD, "pass");
        this.mDefaultPattern.mExtra2 = mPref.getString(KEY_DEFAULT_PATTERN, createDefaultPattern());
        this.mDefaultBlackScreen.mExtra2 = mPref.getString(KEY_DEFAULT_DIALOG_MSG, "Force Close");
        try {
            this.mDefaultBlackScreen.mExtra1 = Long.parseLong(mPref.getString(KEY_DEFAULT_DIALOG_NO_CLICK, "2"));
        } catch (NumberFormatException e) {
            this.mDefaultBlackScreen.mExtra1 = 2;
            e.printStackTrace();
        }
        this.mDefaultDial.mExtra2 = mPref.getString(KEY_DEFAULT_DIAL_NUMBER, "123");
        try {
            this.mDefaultLockMethod = LockMethod.getMethod(Integer.parseInt(mPref.getString(KEY_DEFAULT_LOCK_METHOD, this.mContext.getString(R.string.default_lock_method_value))));
        } catch (NumberFormatException e2) {
            NumberFormatException e3 = e2;
            try {
                this.mDefaultLockMethod = LockMethod.getMethod(Integer.parseInt(this.mContext.getString(R.string.default_lock_method_value)));
            } catch (NumberFormatException e4) {
                this.mDefaultLockMethod = LockMethod.PIN;
            }
            e3.printStackTrace();
        }
        this.mSearchTerm = mPref.getString(SEARCH_TERM, "android");
        this.mLatestImageSrc = mPref.getString(IMAGE_SOURCE, null);
        this.mPinHint = mPref.getString(KEY_HINT_PIN, this.mContext.getString(R.string.pin_hint_default));
        this.mPassHint = mPref.getString(KEY_HINT_PASS, this.mContext.getString(R.string.pass_hint_default));
        this.mPatternHint = mPref.getString(KEY_HINT_PATTERN, this.mContext.getString(R.string.pattern_hint_default));
        this.mRecoveryEmail = mPref.getString(KEY_RECOVERY_EMAIL, "");
        try {
            this.mWrongToClose = Integer.parseInt(mPref.getString(KEY_WRONG_TO_CLOSE, "3"));
        } catch (NumberFormatException e5) {
            this.mWrongToClose = 3;
            e5.printStackTrace();
        }
    }

    public String toString() {
        return "Configuration [mEnable=" + this.mEnable + ", mLockAll=" + this.mLockAll + ", mInvislblePattern=" + this.mInvislblePattern + ", mUnLockOneAll=" + this.mUnLockOneAll + ", mValidity=" + this.mValidity + ", mDefaultLockMethod=" + this.mDefaultLockMethod + ", mDefaultPIN=" + this.mDefaultPIN.mExtra2 + ", mDefaultPassword=" + this.mDefaultPassword.mExtra2 + ", mDefaultPattern=" + this.mDefaultPattern.mExtra2 + ", mDefaultBlackScreen=" + this.mDefaultBlackScreen + ", mDefaultDial=" + this.mDefaultDial + ", mSearchTerm=" + this.mSearchTerm + ", mLatestImageSrc=" + this.mLatestImageSrc + ", mResetToDefault=" + this.mResetToDefault + ", mHasUnLock=" + this.mHasUnLock + ", mLastUnlock=" + this.mLastUnlock + ", mLastUnlockPkg=" + this.mLastUnlockPkg + ", mLastUnlockMethod=" + this.mLastUnlockMethod + ", mLastResumingPkg=" + this.mLastResumingPkg + ", mCurrentUnLockPkg=" + this.mCurrentUnLockPkg + ", mUnlockedCaches=" + this.mUnlockedCaches + ", mLatestTextTemplate=" + this.mLatestTextTemplate + "]";
    }

    /* access modifiers changed from: package-private */
    public String createDefaultPattern() {
        return new String(new byte[]{0, 1, 2, 5});
    }

    public void setDefaultPattern(String pattern) {
        this.mHelper.insertOrUpdateConf(KEY_DEFAULT_PATTERN, pattern);
        this.mDefaultPattern.mExtra2 = pattern;
        SharedPreferences.Editor e = PreferenceManager.getDefaultSharedPreferences(this.mContext).edit();
        e.putString(KEY_DEFAULT_PATTERN, pattern);
        e.commit();
    }

    public void setEnable(boolean b) {
        this.mEnable = b;
        this.mHelper.insertOrUpdateConf(KEY_ENABLE, "" + b);
    }

    public void setLatestTextTemplate(String text) {
        this.mHelper.insertOrUpdateConf(TEXT_TEMPLATE, text);
        this.mLatestTextTemplate = text;
    }

    public void setLatestImageSrc(String text) {
        this.mHelper.insertOrUpdateConf(IMAGE_SOURCE, text);
        this.mLatestImageSrc = text;
    }

    public void setSerchTerm(String term) {
        this.mSearchTerm = term;
        this.mHelper.insertOrUpdateConf(SEARCH_TERM, term);
    }

    public void setPrefScanValue(String v) {
        this.mHelper.insertOrUpdateConf(PREF_SCAN_APPS2, v);
    }

    public String getPrefScanValue(String def) {
        try {
            return this.mHelper.getConfValue(PREF_SCAN_APPS2, "" + def);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return def;
        }
    }

    public String getLastVersion(String def) {
        try {
            return this.mHelper.getConfValue(PREF_LAST_VERSION, "" + def);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return def;
        }
    }

    public void setLastVersion(String v) {
        this.mHelper.insertOrUpdateConf(PREF_LAST_VERSION, v);
    }

    private boolean isConfExists() {
        return TRUE.equalsIgnoreCase(this.mHelper.getConfValue(CONFIG, FALSE));
    }

    public void writeDb() {
        this.mHelper.insertOrUpdateConf(CONFIG, TRUE);
        this.mHelper.insertOrUpdateConf(KEY_ENABLE, "" + this.mEnable);
        this.mHelper.insertOrUpdateConf(KEY_LOCK_ALL, "" + this.mLockAll);
        this.mHelper.insertOrUpdateConf(KEY_INVISIBLE_PATTERN, "" + this.mInvislblePattern);
        this.mHelper.insertOrUpdateConf(KEY_UNLOCK_ONE_ALL, "" + this.mUnLockOneAll);
        this.mHelper.insertOrUpdateConf(KEY_VALIDITY_PERIOD, "" + this.mValidity);
        this.mHelper.insertOrUpdateConf(KEY_DEFAULT_PIN, this.mDefaultPIN.mExtra2);
        this.mHelper.insertOrUpdateConf(KEY_DEFAULT_PASSWORD, "" + this.mDefaultPassword.mExtra2);
        this.mHelper.insertOrUpdateConf(KEY_DEFAULT_PATTERN, this.mDefaultPattern.mExtra2);
        this.mHelper.insertOrUpdateConf(KEY_DEFAULT_DIALOG_MSG, "" + this.mDefaultBlackScreen.mExtra2);
        this.mHelper.insertOrUpdateConf(KEY_DEFAULT_DIALOG_NO_CLICK, "" + this.mDefaultBlackScreen.mExtra1);
        this.mHelper.insertOrUpdateConf(KEY_DEFAULT_DIAL_NUMBER, this.mDefaultDial.mExtra2);
        this.mHelper.insertOrUpdateConf(KEY_DEFAULT_LOCK_METHOD, "" + this.mDefaultLockMethod.mType);
        this.mHelper.insertOrUpdateConf(SEARCH_TERM, "" + this.mSearchTerm);
        this.mHelper.insertOrUpdateConf(IMAGE_SOURCE, "" + this.mLatestImageSrc);
        this.mHelper.insertOrUpdateConf(KEY_HINT_PIN, "" + this.mPinHint);
        this.mHelper.insertOrUpdateConf(KEY_HINT_PASS, "" + this.mPassHint);
        this.mHelper.insertOrUpdateConf(KEY_HINT_PATTERN, "" + this.mPatternHint);
        this.mHelper.insertOrUpdateConf(KEY_RECOVERY_EMAIL, "" + this.mRecoveryEmail);
        this.mHelper.insertOrUpdateConf(KEY_WRONG_TO_CLOSE, "" + this.mWrongToClose);
    }

    public void writePref() {
        SharedPreferences.Editor e = PreferenceManager.getDefaultSharedPreferences(this.mContext).edit();
        e.putBoolean(KEY_ENABLE, this.mEnable);
        e.putBoolean(KEY_LOCK_ALL, this.mLockAll);
        e.putBoolean(KEY_INVISIBLE_PATTERN, this.mInvislblePattern);
        e.putBoolean(KEY_UNLOCK_ONE_ALL, this.mUnLockOneAll);
        e.putString(KEY_VALIDITY_PERIOD, "" + this.mValidity);
        e.putString(KEY_DEFAULT_PIN, this.mDefaultPIN.mExtra2);
        e.putString(KEY_DEFAULT_PASSWORD, this.mDefaultPassword.mExtra2);
        e.putString(KEY_DEFAULT_PATTERN, this.mDefaultPattern.mExtra2);
        e.putString(KEY_DEFAULT_DIALOG_MSG, this.mDefaultBlackScreen.mExtra2);
        e.putString(KEY_DEFAULT_DIALOG_NO_CLICK, "" + this.mDefaultBlackScreen.mExtra1);
        e.putString(KEY_DEFAULT_DIAL_NUMBER, this.mDefaultDial.mExtra2);
        e.putString(KEY_DEFAULT_LOCK_METHOD, "" + this.mDefaultLockMethod.mType);
        e.putString(SEARCH_TERM, "" + this.mSearchTerm);
        e.putString(IMAGE_SOURCE, "" + this.mLatestImageSrc);
        e.putString(KEY_HINT_PIN, "" + this.mPinHint);
        e.putString(KEY_HINT_PASS, "" + this.mPassHint);
        e.putString(KEY_HINT_PATTERN, "" + this.mPatternHint);
        e.putString(KEY_RECOVERY_EMAIL, "" + this.mRecoveryEmail);
        e.putString(KEY_WRONG_TO_CLOSE, "" + this.mWrongToClose);
        e.commit();
    }

    public void readDb() {
        try {
            this.mEnable = Boolean.valueOf(this.mHelper.getConfValue(KEY_ENABLE, Boolean.TRUE.toString())).booleanValue();
        } catch (Throwable th) {
            this.mEnable = true;
            th.printStackTrace();
        }
        try {
            this.mLockAll = Boolean.valueOf(this.mHelper.getConfValue(KEY_LOCK_ALL, Boolean.FALSE.toString())).booleanValue();
        } catch (Throwable th2) {
            this.mLockAll = false;
            th2.printStackTrace();
        }
        try {
            this.mInvislblePattern = Boolean.valueOf(this.mHelper.getConfValue(KEY_INVISIBLE_PATTERN, Boolean.FALSE.toString())).booleanValue();
        } catch (Throwable th3) {
            this.mInvislblePattern = false;
            th3.printStackTrace();
        }
        try {
            this.mUnLockOneAll = Boolean.valueOf(this.mHelper.getConfValue(KEY_UNLOCK_ONE_ALL, Boolean.FALSE.toString())).booleanValue();
        } catch (Throwable th4) {
            this.mUnLockOneAll = false;
            th4.printStackTrace();
        }
        try {
            this.mValidity = Integer.valueOf(this.mHelper.getConfValue(KEY_VALIDITY_PERIOD, "0")).intValue();
        } catch (Throwable th5) {
            this.mValidity = 0;
            th5.printStackTrace();
        }
        try {
            this.mDefaultPIN.mExtra2 = this.mHelper.getConfValue(KEY_DEFAULT_PIN, "888");
        } catch (Throwable th6) {
            this.mDefaultPIN.mExtra2 = "888";
            th6.printStackTrace();
        }
        try {
            this.mDefaultPassword.mExtra2 = this.mHelper.getConfValue(KEY_DEFAULT_PASSWORD, "888");
        } catch (Throwable th7) {
            this.mDefaultPassword.mExtra2 = "pass";
            th7.printStackTrace();
        }
        try {
            this.mDefaultPattern.mExtra2 = this.mHelper.getConfValue(KEY_DEFAULT_PATTERN, "888");
        } catch (Throwable th8) {
            Throwable e = th8;
            this.mDefaultPattern.mExtra2 = createDefaultPattern();
            e.printStackTrace();
        }
        try {
            this.mDefaultBlackScreen.mExtra2 = this.mHelper.getConfValue(KEY_DEFAULT_DIALOG_MSG, "888");
        } catch (Throwable th9) {
            this.mDefaultBlackScreen.mExtra2 = "Force Close";
            th9.printStackTrace();
        }
        try {
            this.mDefaultBlackScreen.mExtra1 = Long.parseLong(this.mHelper.getConfValue(KEY_DEFAULT_DIALOG_NO_CLICK, "2"));
        } catch (Throwable th10) {
            this.mDefaultBlackScreen.mExtra1 = 2;
            th10.printStackTrace();
        }
        try {
            this.mDefaultDial.mExtra2 = this.mHelper.getConfValue(KEY_DEFAULT_DIAL_NUMBER, "123");
        } catch (Throwable th11) {
            this.mDefaultBlackScreen.mExtra2 = "123";
            th11.printStackTrace();
        }
        try {
            this.mDefaultLockMethod = LockMethod.getMethod(Integer.parseInt(this.mHelper.getConfValue(KEY_DEFAULT_LOCK_METHOD, this.mContext.getString(R.string.default_lock_method_value))));
        } catch (Throwable th12) {
            Throwable e2 = th12;
            try {
                this.mDefaultLockMethod = LockMethod.getMethod(Integer.parseInt(this.mContext.getString(R.string.default_lock_method_value)));
            } catch (NumberFormatException e3) {
                this.mDefaultLockMethod = LockMethod.PIN;
            }
            e2.printStackTrace();
        }
        try {
            this.mSearchTerm = this.mHelper.getConfValue(SEARCH_TERM, "Android");
        } catch (Throwable th13) {
            this.mSearchTerm = "Android";
            th13.printStackTrace();
        }
        try {
            this.mLatestImageSrc = this.mHelper.getConfValue(IMAGE_SOURCE, "");
        } catch (Throwable th14) {
            this.mLatestImageSrc = "";
            th14.printStackTrace();
        }
        try {
            this.mPinHint = this.mHelper.getConfValue(KEY_HINT_PIN, "");
            if (TextUtils.isEmpty(this.mPinHint)) {
                this.mPinHint = this.mContext.getString(R.string.pin_hint_default);
                this.mHelper.insertOrUpdateConf(KEY_HINT_PIN, this.mPinHint);
            }
        } catch (Throwable th15) {
            this.mPinHint = this.mContext.getString(R.string.pin_hint_default);
            th15.printStackTrace();
        }
        try {
            this.mPassHint = this.mHelper.getConfValue(KEY_HINT_PASS, "");
            if (TextUtils.isEmpty(this.mPassHint)) {
                this.mPassHint = this.mContext.getString(R.string.pass_hint_default);
                this.mHelper.insertOrUpdateConf(KEY_HINT_PASS, this.mPassHint);
            }
        } catch (Throwable th16) {
            this.mPassHint = this.mContext.getString(R.string.pass_hint_default);
            th16.printStackTrace();
        }
        try {
            this.mPatternHint = this.mHelper.getConfValue(KEY_HINT_PATTERN, "");
            if (TextUtils.isEmpty(this.mPatternHint)) {
                this.mPatternHint = this.mContext.getString(R.string.pattern_hint_default);
                this.mHelper.insertOrUpdateConf(KEY_HINT_PATTERN, this.mPatternHint);
            }
        } catch (Throwable th17) {
            this.mPatternHint = this.mContext.getString(R.string.pattern_hint_default);
            th17.printStackTrace();
        }
        try {
            this.mWrongToClose = Integer.parseInt(this.mHelper.getConfValue(KEY_WRONG_TO_CLOSE, "-100"));
            if (this.mWrongToClose == -100) {
                this.mWrongToClose = 3;
                this.mHelper.insertOrUpdateConf(KEY_WRONG_TO_CLOSE, "3");
            }
        } catch (Throwable th18) {
            this.mWrongToClose = 3;
            th18.printStackTrace();
        }
        try {
            this.mRecoveryEmail = this.mHelper.getConfValue(KEY_RECOVERY_EMAIL, "");
        } catch (Throwable th19) {
            th19.printStackTrace();
        }
    }

    public String getPinHint() {
        return this.mPinHint;
    }

    public String getPassHint() {
        return this.mPassHint;
    }

    public String getPatternHint() {
        return this.mPatternHint;
    }

    public int getWrongToClose() {
        return this.mWrongToClose;
    }

    public String getRecoveryEmail() {
        return this.mRecoveryEmail;
    }
}
