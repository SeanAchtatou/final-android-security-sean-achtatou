package org.baole.applocker.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import org.baole.albs.R;
import org.baole.applocker.model.App;
import org.baole.applocker.model.Configuration;
import org.baole.applocker.service.ActivityWatcher;

public class FCUnlockActivity extends UnlockActivity {
    App mApp;
    long mClickCount = 0;
    /* access modifiers changed from: private */
    public Configuration mConf;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.blackscreen_unlock_activity);
        this.mApp = (App) getIntent().getParcelableExtra(ActivityWatcher.APP);
        this.mConf = Configuration.getInstance(getApplicationContext());
        buildLocker();
    }

    /* access modifiers changed from: package-private */
    public void buildLocker() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        TextView text = (TextView) getLayoutInflater().inflate((int) R.layout.force_close_dialog, (ViewGroup) null);
        builder.setView(text);
        builder.setTitle((int) R.string.fc_dialog_title);
        builder.setIcon(17301543);
        text.setText(getString(R.string.fc_dialog_message, new Object[]{this.mApp.getName(), this.mApp.getPackage()}));
        text.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                FCUnlockActivity.this.mClickCount++;
                if (FCUnlockActivity.this.mClickCount >= FCUnlockActivity.this.mConf.mDefaultBlackScreen.mExtra1) {
                    FCUnlockActivity.this.unlock(FCUnlockActivity.this.mApp);
                    FCUnlockActivity.this.finish();
                }
            }
        });
        builder.setNeutralButton(getString(R.string.force_close), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                FCUnlockActivity.this.activateLauncher();
            }
        });
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                FCUnlockActivity.this.activateLauncher();
            }
        });
        builder.create().show();
    }
}
