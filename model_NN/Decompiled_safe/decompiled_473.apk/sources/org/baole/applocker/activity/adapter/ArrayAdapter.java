package org.baole.applocker.activity.adapter;

import android.content.Context;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class ArrayAdapter<T> extends BaseAdapter implements Filterable {
    private Context mContext;
    private ArrayAdapter<T>.ArrayFilter mFilter;
    /* access modifiers changed from: private */
    public final Object mLock = new Object();
    private boolean mNotifyOnChange = true;
    /* access modifiers changed from: private */
    public List<T> mObjects;
    /* access modifiers changed from: private */
    public ArrayList<T> mOriginalValues;

    public ArrayAdapter(Context context, int textViewResourceId, T[] objects) {
        init(context, Arrays.asList(objects));
    }

    public ArrayAdapter(Context context, List<T> objects) {
        init(context, objects);
    }

    public void add(T object) {
        if (this.mOriginalValues != null) {
            synchronized (this.mLock) {
                this.mOriginalValues.add(object);
                if (this.mNotifyOnChange) {
                    notifyDataSetChanged();
                }
            }
            return;
        }
        this.mObjects.add(object);
        if (this.mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }

    public void insert(T object, int index) {
        if (this.mOriginalValues != null) {
            synchronized (this.mLock) {
                this.mOriginalValues.add(index, object);
                if (this.mNotifyOnChange) {
                    notifyDataSetChanged();
                }
            }
            return;
        }
        this.mObjects.add(index, object);
        if (this.mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }

    public void remove(T object) {
        if (this.mOriginalValues != null) {
            synchronized (this.mLock) {
                this.mOriginalValues.remove(object);
            }
        } else {
            this.mObjects.remove(object);
        }
        if (this.mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }

    public void clear() {
        if (this.mOriginalValues != null) {
            synchronized (this.mLock) {
                this.mOriginalValues.clear();
            }
        } else {
            this.mObjects.clear();
        }
        if (this.mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }

    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        this.mNotifyOnChange = true;
    }

    public void setNotifyOnChange(boolean notifyOnChange) {
        this.mNotifyOnChange = notifyOnChange;
    }

    private void init(Context context, List<T> objects) {
        this.mContext = context;
        this.mObjects = objects;
    }

    public Context getContext() {
        return this.mContext;
    }

    public int getCount() {
        return this.mObjects.size();
    }

    public T getItem(int position) {
        return this.mObjects.get(position);
    }

    public int getPosition(T item) {
        return this.mObjects.indexOf(item);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public Filter getFilter() {
        if (this.mFilter == null) {
            this.mFilter = new ArrayFilter();
        }
        return this.mFilter;
    }

    private class ArrayFilter extends Filter {
        private ArrayFilter() {
        }

        /* access modifiers changed from: protected */
        public Filter.FilterResults performFiltering(CharSequence prefix) {
            Filter.FilterResults results = new Filter.FilterResults();
            if (ArrayAdapter.this.mOriginalValues == null) {
                synchronized (ArrayAdapter.this.mLock) {
                    ArrayList unused = ArrayAdapter.this.mOriginalValues = new ArrayList(ArrayAdapter.this.mObjects);
                }
            }
            if (prefix == null || prefix.length() == 0) {
                synchronized (ArrayAdapter.this.mLock) {
                    ArrayList<T> list = new ArrayList<>(ArrayAdapter.this.mOriginalValues);
                    results.values = list;
                    results.count = list.size();
                }
            } else {
                String prefixString = prefix.toString().toLowerCase();
                ArrayList<T> values = ArrayAdapter.this.mOriginalValues;
                int count = values.size();
                ArrayList<T> newValues = new ArrayList<>(count);
                for (int i = 0; i < count; i++) {
                    T value = values.get(i);
                    String valueText = value.toString().toLowerCase();
                    if (valueText.contains(prefixString)) {
                        newValues.add(value);
                    } else {
                        String[] words = valueText.split(" ");
                        int wordCount = words.length;
                        int k = 0;
                        while (true) {
                            if (k >= wordCount) {
                                break;
                            } else if (words[k].startsWith(prefixString)) {
                                newValues.add(value);
                                break;
                            } else {
                                k++;
                            }
                        }
                    }
                }
                results.values = newValues;
                results.count = newValues.size();
            }
            return results;
        }

        /* access modifiers changed from: protected */
        public void publishResults(CharSequence constraint, Filter.FilterResults results) {
            List unused = ArrayAdapter.this.mObjects = (List) results.values;
            if (results.count > 0) {
                ArrayAdapter.this.notifyDataSetChanged();
            } else {
                ArrayAdapter.this.notifyDataSetInvalidated();
            }
        }
    }
}
