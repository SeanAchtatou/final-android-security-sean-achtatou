package org.baole.applocker.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import org.baole.albs.R;
import org.baole.applocker.model.Configuration;

public class HowtoActivity extends Activity implements View.OnClickListener {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.howto_activity);
        findViewById(R.id.button_setting).setOnClickListener(this);
        WebView webView = (WebView) findViewById(R.id.webView1);
        webView.getSettings().setDefaultTextEncodingName("utf-8");
        webView.loadData(getHowToText(), "text/html", "utf-8");
        webView.loadDataWithBaseURL("fake://not/needed", getHowToText(), "text/html", "utf-8", "");
    }

    /* access modifiers changed from: package-private */
    public String getHowToText() {
        StringBuffer buff = new StringBuffer("");
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(getAssets().open("howto.txt"), Charset.forName("UTF-8")));
            while (true) {
                String line = br.readLine();
                if (line == null) {
                    break;
                }
                buff.append(line);
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
        return buff.toString();
    }

    public void onClick(View v) {
        if (v.getId() == R.id.button_setting) {
            Configuration.getInstance(this).writePref();
            startActivity(new Intent(this, Settings.class));
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        AppListActivity.mShouldCheckLockIntent = false;
    }
}
