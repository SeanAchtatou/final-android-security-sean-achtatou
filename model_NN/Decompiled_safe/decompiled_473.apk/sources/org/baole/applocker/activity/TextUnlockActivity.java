package org.baole.applocker.activity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.TextView;
import java.util.Date;
import org.baole.albs.R;
import org.baole.applocker.model.App;
import org.baole.applocker.model.Configuration;
import org.baole.applocker.service.ActivityWatcher;

public class TextUnlockActivity extends UnlockActivity implements View.OnClickListener {
    App mApp;
    long mClickCount = 0;
    private Configuration mConf;
    private Boolean mPreview;
    /* access modifiers changed from: private */
    public TextView mTextView1;
    private int mTryCount = 0;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.text_screen_unlock_activity);
        this.mPreview = Boolean.valueOf(getIntent().getBooleanExtra(UnlockActivity.PREVIEW_MODE, false));
        this.mApp = (App) getIntent().getParcelableExtra(ActivityWatcher.APP);
        this.mConf = Configuration.getInstance(getApplicationContext());
        if (!this.mPreview.booleanValue()) {
            findViewById(R.id.text_view2).setOnClickListener(this);
            findViewById(R.id.text_view1).setOnClickListener(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.mTextView1 = (TextView) findViewById(R.id.text_view1);
        String message = getMessage(this.mApp);
        new AsyncTask<String, String, String>() {
            /* access modifiers changed from: protected */
            public void onProgressUpdate(String... values) {
                super.onProgressUpdate((Object[]) values);
                TextUnlockActivity.this.mTextView1.setText(values[0]);
            }

            /* access modifiers changed from: protected */
            public String doInBackground(String... params) {
                String msg = params[0];
                if (msg == null) {
                    return null;
                }
                if (TextUnlockActivity.this.mApp.getExtra1() == 1) {
                    String[] lists = msg.split("\n");
                    StringBuffer buff = new StringBuffer();
                    for (String s : lists) {
                        buff.append(s);
                        buff.append("\n");
                        publishProgress(buff.toString());
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                        }
                    }
                    return null;
                }
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e2) {
                }
                publishProgress(msg);
                return null;
            }
        }.execute(message);
    }

    private String getMessage(App app) {
        String message = app.getExtra2();
        if (TextUtils.isEmpty(message)) {
            message = getString(R.string.text_template);
        }
        if (message != null) {
            return message.replaceAll("#app_name#", app.getName()).replaceAll("#package#", app.getPackage()).replaceAll("#date#", DateFormat.getLongDateFormat(this).format(new Date(System.currentTimeMillis())));
        }
        return message;
    }

    public void onBackPressed() {
        if (this.mPreview.booleanValue()) {
            finish();
        } else {
            super.onBackPressed();
        }
    }

    public void onClick(View v) {
        if (v.getId() == R.id.text_view2) {
            this.mClickCount++;
            if (this.mClickCount >= this.mConf.mDefaultBlackScreen.mExtra1) {
                unlock(this.mApp);
                finish();
            }
        } else if (v.getId() == R.id.text_view1) {
            this.mClickCount = 0;
            this.mTryCount++;
            checkLaunchLauncher(this.mTryCount);
        }
    }
}
