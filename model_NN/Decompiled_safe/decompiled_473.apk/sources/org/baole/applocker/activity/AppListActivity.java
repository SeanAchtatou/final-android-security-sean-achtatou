package org.baole.applocker.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.IApplicationThread;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.anttek.quickactions.BetterPopupWindow;
import com.markupartist.android.widget.ActionBar;
import java.util.ArrayList;
import java.util.Iterator;
import org.baole.ad.AdmobHelper;
import org.baole.albs.R;
import org.baole.applocker.activity.adapter.AppListAdapter;
import org.baole.applocker.db.DbHelper;
import org.baole.applocker.model.App;
import org.baole.applocker.model.Configuration;
import org.baole.applocker.model.LockMethod;
import org.baole.applocker.service.ActivityWatcher;
import org.baole.applocker.service.AppLockerService;
import org.baole.applocker.utils.DialogUtil;

public class AppListActivity extends Activity implements View.OnClickListener, AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {
    public static final int ACTIVITY_ITEM = 200;
    protected static final int ACTIVITY_SETUP = 104;
    private static final int ACTIVITY_SETUP_IMAGE = 105;
    private static final int ACTIVITY_SETUP_TEXT = 103;
    public static final String AD_ID = "a14e31885a463e4";
    private static final int DIALOG_CHANGLOGS = 100;
    private static final int LOCK_INTENT = 102;
    static boolean mShouldCheckLockIntent = true;
    protected AppListAdapter mAdapter;
    /* access modifiers changed from: private */
    public int mAppFilter = R.id.action_all_apps;
    /* access modifiers changed from: private */
    public Configuration mConf;
    protected int mCurCheckPosition = 0;
    protected ArrayList<App> mData;
    private EditText mEditTextFilter;
    private ImageView mFilterAppView;
    private View mFilterBar;
    private DbHelper mHelper;
    /* access modifiers changed from: private */
    public boolean mIsShowFilter;
    protected ListView mListView;
    private LockPopupWindow mLockPopupWindow;
    private View mOnOffView;
    protected ArrayList<App> mOrgData;
    private boolean mPause = false;
    protected FilterPopupWindow mSettingPopupWindow;
    private TextView mStatus;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mHelper = DbHelper.getInstance(this);
        this.mConf = Configuration.getInstance(getApplicationContext());
        setContentView((int) R.layout.applist_activity);
        setupActionBar();
        new AdmobHelper(this).setup((LinearLayout) findViewById(R.id.ad_container), AD_ID, true);
        AppLockerService.startActivityWatcherService(getApplicationContext());
        if (getLastNonConfigurationInstance() != null) {
            mShouldCheckLockIntent = false;
        }
        this.mOrgData = this.mHelper.queryApps(null, null);
        this.mData = new ArrayList<>();
        this.mAppFilter = R.id.action_all_apps;
        this.mIsShowFilter = false;
        this.mFilterBar = findViewById(R.id.filter_bar);
        refreshFilterBar();
        createHeaderView();
        this.mAdapter = new AppListAdapter(this, this.mData);
        this.mListView = (ListView) findViewById(16908298);
        this.mListView.setEmptyView(findViewById(R.id.empty));
        this.mListView.setOnItemClickListener(this);
        this.mListView.setOnItemLongClickListener(this);
        this.mListView.setAdapter((ListAdapter) this.mAdapter);
        refineAppFilter();
        registerForContextMenu(this.mListView);
        checkImport();
        String v0 = this.mConf.getLastVersion("0");
        String v1 = getString(R.string.app_version);
        if (!v0.equals(v1)) {
            this.mConf.setLastVersion(v1);
            showDialog(100);
            startActivity(new Intent(this, HowtoActivity.class));
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    public Object onRetainNonConfigurationInstance() {
        return new Object();
    }

    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 100:
                return DialogUtil.createChangLogDialog(this, R.string.updates_title, R.array.updates);
            default:
                return null;
        }
    }

    private void checkImport() {
        if (this.mConf.getPrefScanValue("0").compareTo("1") < 0) {
            startActivityForResult(new Intent(this, SetupActivity.class), 104);
        }
    }

    private void changeAppAttributes(int pos, boolean useLock, LockMethod lm) {
        if (pos >= 0 && pos < this.mAdapter.getCount()) {
            App app = (App) this.mAdapter.getItem(pos);
            app.setUseLock(useLock);
            if (useLock) {
                app.setLockMethod(lm);
            }
            this.mHelper.insertOrUpdateApp(app, new Boolean[0]);
            this.mAdapter.notifyDataSetChanged();
        }
    }

    private void configureLocker(int pos, boolean useLock, LockMethod lm, Class clz, int activitySetupText) {
        if (pos >= 0 && pos < this.mAdapter.getCount()) {
            App a = ((App) this.mAdapter.getItem(pos)).clone();
            a.setUseLock(useLock);
            if (useLock) {
                a.setLockMethod(lm);
            }
            Intent intent = new Intent(this, clz);
            intent.putExtra(ActivityWatcher.APP, a);
            startActivityForResult(intent, activitySetupText);
        }
    }

    private void refineAppFilter() {
        this.mData.clear();
        switch (this.mAppFilter) {
            case R.id.action_third_party_apps:
                Iterator i$ = this.mOrgData.iterator();
                while (i$.hasNext()) {
                    App app = i$.next();
                    if ((app.getExtra1() & 1) == 0) {
                        this.mData.add(app);
                    }
                }
                this.mFilterAppView.setImageResource(R.drawable.ic_filter_3rd);
                break;
            case R.id.action_system_apps:
                Iterator i$2 = this.mOrgData.iterator();
                while (i$2.hasNext()) {
                    App app2 = i$2.next();
                    if ((app2.getExtra1() & 1) != 0) {
                        this.mData.add(app2);
                    }
                }
                this.mFilterAppView.setImageResource(R.drawable.ic_filter_sys);
                break;
            case R.id.action_locked_apps:
                Iterator i$3 = this.mOrgData.iterator();
                while (i$3.hasNext()) {
                    App app3 = i$3.next();
                    if (app3.isUseLock()) {
                        this.mData.add(app3);
                    }
                }
                this.mFilterAppView.setImageResource(R.drawable.ic_filter_lock);
                break;
            case R.id.action_unlocked_apps:
                Iterator i$4 = this.mOrgData.iterator();
                while (i$4.hasNext()) {
                    App app4 = i$4.next();
                    if (!app4.isUseLock()) {
                        this.mData.add(app4);
                    }
                }
                this.mFilterAppView.setImageResource(R.drawable.ic_filter_unlock);
                break;
            default:
                this.mData.addAll(this.mOrgData);
                this.mFilterAppView.setImageResource(R.drawable.ic_filter_all);
                break;
        }
        this.mAdapter.notifyDataSetChanged();
        this.mFilterAppView.invalidate();
        this.mFilterAppView.setSelected(!this.mFilterAppView.isSelected());
    }

    /* access modifiers changed from: private */
    public void refreshFilterBar() {
        if (this.mIsShowFilter) {
            this.mFilterBar.setVisibility(0);
        } else {
            this.mFilterBar.setVisibility(8);
        }
    }

    /* access modifiers changed from: protected */
    public void doFilter(CharSequence s) {
        if (this.mAdapter != null) {
            this.mAdapter.getFilter().filter(s);
        }
    }

    public void onItemClick(AdapterView<?> adapterView, View arg1, int position, long arg3) {
        if (position != -1) {
            this.mCurCheckPosition = position;
            this.mLockPopupWindow = new LockPopupWindow(arg1);
            this.mLockPopupWindow.setTitle(((App) this.mAdapter.getItem(this.mCurCheckPosition)).getName());
            this.mLockPopupWindow.showLikeQuickAction(0, 30);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == -1) {
            switch (requestCode) {
                case 103:
                    App app = (App) this.mAdapter.getItem(this.mCurCheckPosition);
                    app.setUseLock(true);
                    app.setLockMethod(LockMethod.TEXT_SCREEN);
                    app.setExtra1(data.getBooleanExtra("_ir", true) ? 1 : 0);
                    app.setExtra2(data.getStringExtra("_c"));
                    this.mHelper.insertOrUpdateApp(app, new Boolean[0]);
                    this.mAdapter.notifyDataSetChanged();
                    break;
                case 104:
                    this.mOrgData = this.mHelper.queryApps(null, null);
                    if (this.mOrgData != null) {
                        refineAppFilter();
                        break;
                    }
                    break;
                case 105:
                    App app2 = (App) this.mAdapter.getItem(this.mCurCheckPosition);
                    app2.setUseLock(true);
                    app2.setLockMethod(LockMethod.IMAGE_SCREEN);
                    app2.setExtra1(data.getLongExtra(SetupImageScreenActivity.IMAGE_SRC_TYPE, 0));
                    app2.setExtra2(data.getStringExtra(SetupImageScreenActivity.IMAGE_SRC_PATH));
                    this.mHelper.insertOrUpdateApp(app2, new Boolean[0]);
                    this.mAdapter.notifyDataSetChanged();
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    /* access modifiers changed from: protected */
    public void createHeaderView() {
        this.mStatus = (TextView) findViewById(R.id.textview_status);
        findViewById(R.id.button_clear).setOnClickListener(this);
        this.mEditTextFilter = (EditText) findViewById(R.id.edittext_search);
        this.mEditTextFilter.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                AppListActivity.this.doFilter(s);
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void afterTextChanged(Editable s) {
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        ArrayList<App> apps;
        Intent intent;
        super.onResume();
        this.mOnOffView.setSelected(this.mConf.mEnable);
        setLockerStatus();
        if (this.mConf.mHasUnLock && getPackageName().equals(this.mConf.mCurrentUnLockPkg)) {
            this.mConf.mCurrentUnLockPkg = "";
        } else if (this.mConf.mEnable && mShouldCheckLockIntent) {
            String pkg = getPackageName();
            if (this.mConf.mLockAll) {
                apps = this.mHelper.queryApps("_package=? ", new String[]{pkg});
            } else {
                apps = this.mHelper.queryApps("_package=? AND _use_lock=?", new String[]{pkg, "1"});
            }
            if (!(apps == null || apps.size() <= 0 || (intent = ActivityWatcher.lockIntent(this, apps.get(0), true)) == null)) {
                startActivityForResult(intent, 102);
            }
        }
        mShouldCheckLockIntent = true;
        this.mPause = false;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.mPause = true;
    }

    /* renamed from: org.baole.applocker.activity.AppListActivity$5  reason: invalid class name */
    static /* synthetic */ class AnonymousClass5 {
        static final /* synthetic */ int[] $SwitchMap$org$baole$applocker$model$LockMethod = new int[LockMethod.values().length];

        static {
            try {
                $SwitchMap$org$baole$applocker$model$LockMethod[LockMethod.DIAL.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$org$baole$applocker$model$LockMethod[LockMethod.DIALOG.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$org$baole$applocker$model$LockMethod[LockMethod.IMAGE_SCREEN.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$org$baole$applocker$model$LockMethod[LockMethod.PATTERN.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$org$baole$applocker$model$LockMethod[LockMethod.TEXT_SCREEN.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                $SwitchMap$org$baole$applocker$model$LockMethod[LockMethod.PIN.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                $SwitchMap$org$baole$applocker$model$LockMethod[LockMethod.PASSWORD.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                $SwitchMap$org$baole$applocker$model$LockMethod[LockMethod.FORCE_CLOSE.ordinal()] = 8;
            } catch (NoSuchFieldError e8) {
            }
        }
    }

    private void setLockerStatus() {
        String lock;
        String validity;
        switch (AnonymousClass5.$SwitchMap$org$baole$applocker$model$LockMethod[this.mConf.mDefaultLockMethod.ordinal()]) {
            case 1:
                lock = getString(R.string.dial_number);
                break;
            case 2:
                lock = getString(R.string.black_screen);
                break;
            case 3:
                lock = getString(R.string.image_screen);
                break;
            case 4:
                lock = getString(R.string.pattern);
                break;
            case 5:
                lock = getString(R.string.text_screen);
                break;
            case 6:
                lock = getString(R.string.pin);
                break;
            case IApplicationThread.SCHEDULE_LAUNCH_ACTIVITY_TRANSACTION:
                lock = getString(R.string.password);
                break;
            case 8:
                lock = getString(R.string.force_close);
                break;
            default:
                lock = LockMethod.NONE.name();
                break;
        }
        switch (this.mConf.mValidity) {
            case -1:
                validity = getString(R.string.screen_off);
                break;
            case 0:
                validity = getString(R.string.app_background);
                break;
            default:
                validity = getString(R.string.n_minutes, new Object[]{Integer.valueOf(this.mConf.mValidity)});
                break;
        }
        this.mStatus.setText(getString(R.string.locker_status, new Object[]{lock, validity}));
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_clear:
                this.mEditTextFilter.setText("");
                this.mIsShowFilter = false;
                refreshFilterBar();
                return;
            case R.id.action_all_apps:
            case R.id.action_third_party_apps:
            case R.id.action_system_apps:
            case R.id.action_locked_apps:
            case R.id.action_unlocked_apps:
                this.mAppFilter = v.getId();
                refineAppFilter();
                this.mSettingPopupWindow.dismiss();
                return;
            case R.id.action_lock_default:
                this.mLockPopupWindow.dismiss();
                changeAppAttributes(this.mCurCheckPosition, true, LockMethod.USE_DEFAULT);
                return;
            case R.id.action_lock_pin:
                this.mLockPopupWindow.dismiss();
                changeAppAttributes(this.mCurCheckPosition, true, LockMethod.PIN);
                return;
            case R.id.action_lock_password:
                this.mLockPopupWindow.dismiss();
                changeAppAttributes(this.mCurCheckPosition, true, LockMethod.PASSWORD);
                return;
            case R.id.action_lock_blackscreen:
                this.mLockPopupWindow.dismiss();
                changeAppAttributes(this.mCurCheckPosition, true, LockMethod.DIALOG);
                return;
            case R.id.action_lock_textscreen:
                this.mLockPopupWindow.dismiss();
                configureLocker(this.mCurCheckPosition, true, LockMethod.TEXT_SCREEN, SetupTextScreenActivity.class, 103);
                return;
            case R.id.action_lock_imagescreen:
                this.mLockPopupWindow.dismiss();
                configureLocker(this.mCurCheckPosition, true, LockMethod.IMAGE_SCREEN, SetupImageScreenActivity.class, 105);
                return;
            case R.id.action_lock_fc:
                this.mLockPopupWindow.dismiss();
                changeAppAttributes(this.mCurCheckPosition, true, LockMethod.FORCE_CLOSE);
                return;
            case R.id.action_lock_pattern:
                this.mLockPopupWindow.dismiss();
                changeAppAttributes(this.mCurCheckPosition, true, LockMethod.PATTERN);
                return;
            case R.id.action_lock_dial:
                this.mLockPopupWindow.dismiss();
                changeAppAttributes(this.mCurCheckPosition, true, LockMethod.DIAL);
                return;
            case R.id.action_lock_unlock:
                this.mLockPopupWindow.dismiss();
                changeAppAttributes(this.mCurCheckPosition, false, LockMethod.USE_DEFAULT);
                return;
            default:
                return;
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_option_menu, menu);
        menu.findItem(R.id.donate).setVisible(false);
        return super.onCreateOptionsMenu(menu);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_rescan_apps:
                startActivityForResult(new Intent(this, SetupActivity.class), 104);
                break;
            case R.id.preferneces:
                this.mConf.writePref();
                startSafelyActivity(new Intent(this, Settings.class), 1);
                return true;
            case R.id.donate:
                startSafelyActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + getString(R.string.pro_package))), -1);
                return true;
            case R.id.howto:
                startActivity(new Intent(this, HowtoActivity.class));
                return true;
            case R.id.about:
                Intent market = new Intent("android.intent.action.VIEW");
                market.setData(Uri.parse("market://search?q=pub:AntTek"));
                startActivity(market);
                return true;
            case R.id.rateit:
                startSafelyActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + getPackageName())), -1);
                return true;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void startSafelyActivity(Intent intent, int requestCode) {
        try {
            startActivityForResult(intent, requestCode);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }

    public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
        onItemClick(arg0, arg1, position, arg3);
        return false;
    }

    private void setupActionBar() {
        ActionBar actionBar = (ActionBar) findViewById(R.id.actionbar);
        actionBar.setTitle((int) R.string.app_name);
        this.mOnOffView = actionBar.addAction(new ActionBar.Action() {
            public void performAction(View view) {
                AppListActivity.this.mConf.mEnable = !AppListActivity.this.mConf.mEnable;
                AppListActivity.this.mConf.setEnable(AppListActivity.this.mConf.mEnable);
                view.setSelected(AppListActivity.this.mConf.mEnable);
                Toast.makeText(AppListActivity.this, AppListActivity.this.mConf.mEnable ? R.string.app_on : R.string.app_off, 0).show();
            }

            public int getDrawable() {
                return R.drawable.ic_onoff;
            }
        });
        this.mOnOffView.setSelected(this.mConf.mEnable);
        this.mFilterAppView = (ImageView) actionBar.addAction(new ActionBar.Action() {
            public int getDrawable() {
                switch (AppListActivity.this.mAppFilter) {
                    case R.id.action_third_party_apps:
                        return R.drawable.ic_filter_3rd;
                    case R.id.action_system_apps:
                        return R.drawable.ic_filter_sys;
                    case R.id.action_locked_apps:
                        return R.drawable.ic_filter_lock;
                    case R.id.action_unlocked_apps:
                        return R.drawable.ic_filter_unlock;
                    default:
                        return R.drawable.ic_filter_all;
                }
            }

            public void performAction(View view) {
                AppListActivity.this.mSettingPopupWindow = new FilterPopupWindow(view);
                AppListActivity.this.mSettingPopupWindow.showLikePopDownMenu();
            }
        });
        actionBar.addAction(new ActionBar.Action() {
            public int getDrawable() {
                return R.drawable.search;
            }

            public void performAction(View view) {
                boolean unused = AppListActivity.this.mIsShowFilter = true;
                AppListActivity.this.refreshFilterBar();
            }
        });
    }

    private class FilterPopupWindow extends BetterPopupWindow {
        public FilterPopupWindow(View anchor) {
            super(anchor);
        }

        /* access modifiers changed from: protected */
        public void onCreate() {
            ViewGroup root = (ViewGroup) ((LayoutInflater) this.anchor.getContext().getSystemService("layout_inflater")).inflate((int) R.layout.popup_filters, (ViewGroup) null);
            root.findViewById(R.id.action_all_apps).setOnClickListener(AppListActivity.this);
            root.findViewById(R.id.action_third_party_apps).setOnClickListener(AppListActivity.this);
            root.findViewById(R.id.action_system_apps).setOnClickListener(AppListActivity.this);
            root.findViewById(R.id.action_locked_apps).setOnClickListener(AppListActivity.this);
            root.findViewById(R.id.action_unlocked_apps).setOnClickListener(AppListActivity.this);
            setContentView(root);
        }
    }

    private class LockPopupWindow extends BetterPopupWindow {
        private TextView mTexTitle;

        public LockPopupWindow(View anchor) {
            super(anchor);
        }

        public void setTitle(String title) {
            this.mTexTitle.setText(title);
        }

        /* access modifiers changed from: protected */
        public void onCreate() {
            ViewGroup root = (ViewGroup) ((LayoutInflater) this.anchor.getContext().getSystemService("layout_inflater")).inflate((int) R.layout.popup_lock_option, (ViewGroup) null);
            this.mTexTitle = (TextView) root.findViewById(R.id.text_title);
            root.findViewById(R.id.action_lock_blackscreen).setOnClickListener(AppListActivity.this);
            root.findViewById(R.id.action_lock_default).setOnClickListener(AppListActivity.this);
            root.findViewById(R.id.action_lock_dial).setOnClickListener(AppListActivity.this);
            root.findViewById(R.id.action_lock_imagescreen).setOnClickListener(AppListActivity.this);
            root.findViewById(R.id.action_lock_password).setOnClickListener(AppListActivity.this);
            root.findViewById(R.id.action_lock_pattern).setOnClickListener(AppListActivity.this);
            root.findViewById(R.id.action_lock_pin).setOnClickListener(AppListActivity.this);
            root.findViewById(R.id.action_lock_textscreen).setOnClickListener(AppListActivity.this);
            root.findViewById(R.id.action_lock_unlock).setOnClickListener(AppListActivity.this);
            root.findViewById(R.id.action_lock_fc).setOnClickListener(AppListActivity.this);
            setContentView(root);
        }
    }
}
