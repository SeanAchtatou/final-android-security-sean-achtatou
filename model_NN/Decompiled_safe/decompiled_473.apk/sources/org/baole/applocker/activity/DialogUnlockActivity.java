package org.baole.applocker.activity;

import android.os.Bundle;
import android.view.View;
import org.baole.albs.R;
import org.baole.applocker.model.App;
import org.baole.applocker.model.Configuration;
import org.baole.applocker.model.LockMethod;
import org.baole.applocker.service.ActivityWatcher;

public class DialogUnlockActivity extends UnlockActivity implements View.OnClickListener {
    App mApp;
    long mClickCount = 0;
    private Configuration mConf;
    private LockMethod mLockMethod;
    private int mTryCount = 0;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.blackscreen_unlock_activity);
        this.mApp = (App) getIntent().getParcelableExtra(ActivityWatcher.APP);
        this.mConf = Configuration.getInstance(getApplicationContext());
        this.mLockMethod = LockMethod.getMethod(getIntent().getIntExtra("_lock_method", LockMethod.DIALOG.getType()));
        if (this.mLockMethod == LockMethod.DIALOG) {
            findViewById(R.id.text_view2).setOnClickListener(this);
            findViewById(R.id.text_view1).setOnClickListener(this);
        }
    }

    public void onClick(View v) {
        if (v.getId() == R.id.text_view2) {
            this.mClickCount++;
            if (this.mClickCount >= this.mConf.mDefaultBlackScreen.mExtra1) {
                unlock(this.mApp);
                finish();
            }
        } else if (v.getId() == R.id.text_view1) {
            this.mClickCount = 0;
            this.mTryCount++;
            checkLaunchLauncher(this.mTryCount);
        }
    }
}
