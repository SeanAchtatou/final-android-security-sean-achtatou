package org.baole.applocker.activity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import org.baole.albs.R;
import org.baole.applocker.model.App;
import org.baole.applocker.model.Configuration;
import org.baole.applocker.service.ActivityWatcher;

public class PINUnlockActivity extends UnlockActivity implements View.OnClickListener {
    App mApp;
    /* access modifiers changed from: private */
    public Configuration mConf;
    private EditText mEditPin;
    int mTryCount = 0;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.pin_unlock_activity);
        this.mApp = (App) getIntent().getParcelableExtra(ActivityWatcher.APP);
        this.mConf = Configuration.getInstance(getApplicationContext());
        this.mEditPin = (EditText) findViewById(R.id.edit_pin);
        this.mEditPin.setInputType(2);
        this.mEditPin.setTransformationMethod(PasswordTransformationMethod.getInstance());
        ((TextView) findViewById(R.id.text_hint)).setText(this.mConf.getPinHint());
        findViewById(R.id.button_clear).setOnClickListener(this);
        findViewById(R.id.button_back).setOnClickListener(this);
        findViewById(R.id.button0).setOnClickListener(this);
        findViewById(R.id.button1).setOnClickListener(this);
        findViewById(R.id.button2).setOnClickListener(this);
        findViewById(R.id.button3).setOnClickListener(this);
        findViewById(R.id.button4).setOnClickListener(this);
        findViewById(R.id.button5).setOnClickListener(this);
        findViewById(R.id.button6).setOnClickListener(this);
        findViewById(R.id.button7).setOnClickListener(this);
        findViewById(R.id.button8).setOnClickListener(this);
        findViewById(R.id.button9).setOnClickListener(this);
        this.mEditPin.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s != null && s.toString().equals(PINUnlockActivity.this.mConf.mDefaultPIN.mExtra2)) {
                    PINUnlockActivity.this.unlock(PINUnlockActivity.this.mApp);
                    PINUnlockActivity.this.finish();
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void afterTextChanged(Editable s) {
            }
        });
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_clear:
                this.mEditPin.setText("");
                this.mTryCount++;
                checkLaunchLauncher(this.mTryCount);
                return;
            case R.id.button_ok:
                if (!this.mEditPin.toString().toString().equals(this.mConf.mDefaultPIN.mExtra2)) {
                    this.mTryCount++;
                    checkLaunchLauncher(this.mTryCount);
                    break;
                } else {
                    unlock(this.mApp);
                    finish();
                    break;
                }
            case R.id.button1:
                this.mEditPin.setText(((Object) this.mEditPin.getText()) + "1");
                return;
            case R.id.button2:
                this.mEditPin.setText(((Object) this.mEditPin.getText()) + "2");
                return;
            case R.id.button3:
                this.mEditPin.setText(((Object) this.mEditPin.getText()) + "3");
                return;
            case R.id.button4:
                this.mEditPin.setText(((Object) this.mEditPin.getText()) + "4");
                return;
            case R.id.button5:
                this.mEditPin.setText(((Object) this.mEditPin.getText()) + "5");
                return;
            case R.id.button6:
                this.mEditPin.setText(((Object) this.mEditPin.getText()) + "6");
                return;
            case R.id.button7:
                this.mEditPin.setText(((Object) this.mEditPin.getText()) + "7");
                return;
            case R.id.button8:
                this.mEditPin.setText(((Object) this.mEditPin.getText()) + "8");
                return;
            case R.id.button9:
                this.mEditPin.setText(((Object) this.mEditPin.getText()) + "9");
                return;
            case R.id.button0:
                this.mEditPin.setText(((Object) this.mEditPin.getText()) + "0");
                return;
            case R.id.button_back:
                String text = this.mEditPin.getText().toString();
                if (text.length() >= this.mConf.mDefaultPIN.mExtra2.length()) {
                    this.mTryCount++;
                    checkLaunchLauncher(this.mTryCount);
                }
                if (!TextUtils.isEmpty(text) && text.length() > 0) {
                    this.mEditPin.setText(text.substring(0, text.length() - 1));
                    return;
                }
                return;
        }
        super.onClick(v);
    }
}
