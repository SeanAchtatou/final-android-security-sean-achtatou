package org.baole.applocker.activity;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;
import org.baole.albs.R;
import org.baole.applocker.model.App;
import org.baole.applocker.service.ActivityWatcher;
import org.baole.applocker.utils.BitmapUtil;

public class SetupImageScreenActivity extends Activity implements View.OnClickListener {
    public static final int ACTIVITY_IMAGE_PICKER = 1;
    public static final int ACTIVITY_IMAGE_PICKER_GALLERY = 2;
    public static final String CONTENT = "_c";
    public static final String IMAGE_SRC_PATH = "_x2";
    public static final String IMAGE_SRC_TYPE = "_x1";
    public static final String IS_RUNNING = "_ir";
    private App mApp;
    private ImageView mContent;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.setup_image_screen_activity);
        this.mApp = (App) getIntent().getParcelableExtra(ActivityWatcher.APP);
        this.mContent = (ImageView) findViewById(R.id.image_view);
        this.mContent.setOnClickListener(this);
        registerForContextMenu(this.mContent);
        findViewById(R.id.button_preview).setOnClickListener(this);
        findViewById(R.id.button_save).setOnClickListener(this);
        findViewById(R.id.button_cancel).setOnClickListener(this);
        setSelectedImage(this.mApp);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        AppListActivity.mShouldCheckLockIntent = false;
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        getMenuInflater().inflate(R.menu.image_picker_context_menu, menu);
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_image_builtin:
                Intent picker = new Intent(this, ImagePickerActivity.class);
                picker.putExtra(ImagePickerActivity.SOURCE_TYPE, ImagePickerActivity.SOURCE_TYPE_BUILD_IN);
                startActivityForResult(picker, 1);
                break;
            case R.id.menu_image_gallery:
                Intent intent = new Intent("android.intent.action.GET_CONTENT");
                intent.setType("image/*");
                startActivityForResult(intent, 2);
                break;
        }
        return super.onContextItemSelected(item);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == -1) {
            switch (requestCode) {
                case 1:
                    String imagePath = data.getStringExtra(ImagePickerActivity.SELECTED_IMAGE);
                    this.mApp.setExtra1(1);
                    this.mApp.setExtra2(imagePath);
                    setSelectedImage(this.mApp);
                    break;
                case 2:
                    Uri uriImage = data.getData();
                    this.mApp.setExtra1(2);
                    this.mApp.setExtra2(getRealPathFromURI(uriImage));
                    setSelectedImage(this.mApp);
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public String getRealPathFromURI(Uri contentUri) {
        Cursor cursor = managedQuery(contentUri, new String[]{"_data"}, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow("_data");
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private void setSelectedImage(App app) {
        try {
            if (app.getExtra1() == 1) {
                this.mContent.setImageBitmap(BitmapUtil.fromAssets(getAssets(), app.getExtra2()));
            } else if (app.getExtra1() == 2) {
                this.mContent.setImageURI(Uri.parse(app.getExtra2()));
            }
        } catch (Throwable th) {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.image_view:
                v.showContextMenu();
                return;
            case R.id.button_cancel:
                setResult(0);
                finish();
                return;
            case R.id.button_preview:
                Intent intent = new Intent(this, ImageUnlockActivity.class);
                intent.putExtra(UnlockActivity.PREVIEW_MODE, true);
                intent.putExtra(ActivityWatcher.APP, this.mApp);
                startActivity(intent);
                Toast.makeText(this, (int) R.string.exit_preview_mode, 0).show();
                return;
            case R.id.button_save:
                Intent data = new Intent();
                data.putExtra(IMAGE_SRC_TYPE, this.mApp.getExtra1());
                data.putExtra(IMAGE_SRC_PATH, this.mApp.getExtra2());
                setResult(-1, data);
                finish();
                return;
            default:
                return;
        }
    }
}
