package org.baole.applocker.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;
import java.util.ArrayList;
import org.baole.albs.R;
import org.baole.applocker.activity.adapter.DialAppAdapter;
import org.baole.applocker.db.DbHelper;
import org.baole.applocker.model.App;
import org.baole.applocker.model.LockMethod;

public class DialUnlockActivity extends UnlockActivity implements AdapterView.OnItemClickListener {
    private DialAppAdapter mAdapter;
    long mClickCount = 0;
    private ArrayList<App> mData;
    private DbHelper mHelper;
    private ListView mListview;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.dial_unlock_activity);
        this.mHelper = DbHelper.getInstance(this);
        this.mData = this.mHelper.queryApps("_lock_method=? AND _use_lock=?", new String[]{"" + LockMethod.DIAL.getType(), "1"});
        if (this.mData.size() <= 0) {
            Toast.makeText(this, "Advanced App Locker: No app is configured to launch from dial pad.", 1).show();
            finish();
            return;
        }
        this.mAdapter = new DialAppAdapter(this, this.mData);
        this.mListview = (ListView) findViewById(R.id.list);
        this.mListview.setAdapter((ListAdapter) this.mAdapter);
        this.mListview.setOnItemClickListener(this);
    }

    public void onItemClick(AdapterView<?> adapterView, View arg1, int pos, long arg3) {
        if (pos >= 0 && pos < this.mData.size()) {
            launchApps((App) this.mAdapter.getItem(pos));
        }
    }

    private void launchApps(App app) {
        unlock(app);
        this.mConf.mLastResumingPkg = app.getPackage();
        Intent launch = getPackageManager().getLaunchIntentForPackage(app.getPackage());
        if (launch == null) {
            Toast.makeText(this, "This is not a launchable package", 0).show();
        } else {
            launch.addCategory("android.intent.category.LAUNCHER");
            launch.addFlags(268435456);
            startActivity(launch);
        }
        finish();
    }

    public void onBackPressed() {
        finish();
    }
}
