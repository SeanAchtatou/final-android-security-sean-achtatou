package org.baole.applocker.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;
import java.util.ArrayList;
import org.baole.applocker.model.App;
import org.baole.applocker.model.LockMethod;
import org.baole.applocker.utils.BitmapUtil;

public class DbHelper {
    private static final String DATABASE_FILE = "ra.db";
    /* access modifiers changed from: private */
    public static int DATABASE_VERSION = 4;
    public static final String EXTRA1 = "_extra1";
    public static final String EXTRA2 = "_extra2";
    public static final String EXTRA3 = "_extra3";
    public static final String ICON = "_icon";
    public static final String LOCK_METHOD = "_lock_method";
    public static final String NAME = "_name";
    public static final String PACKAGE = "_package";
    public static final String TABLE_APPS = "apps";
    public static final String USE_DEFAULT_METHOD = "_use_default_method";
    public static final String USE_LOCK = "_use_lock";
    public static final String VERSION = "_version";
    private static DbHelper mDbHelper = null;
    private Context mContext = null;
    private SQLiteDatabase mDb;

    public static DbHelper getInstance(Context context) {
        if (mDbHelper == null) {
            synchronized (DbHelper.class) {
                mDbHelper = new DbHelper(context);
            }
        }
        return mDbHelper;
    }

    public SQLiteDatabase getDb() {
        return this.mDb;
    }

    public DbHelper(Context context) {
        this.mContext = context;
        this.mDb = new OpenHelper(this.mContext).getWritableDatabase();
    }

    public ArrayList<App> queryApps(String filter, String[] args) {
        boolean z;
        boolean z2;
        String sql = "select _id, _name, _icon, _package, _version, _lock_method,_use_default_method, _extra1, _extra2, _extra3, _use_lock  from apps";
        if (!TextUtils.isEmpty(filter)) {
            sql = sql + " WHERE " + filter;
        }
        Cursor c = this.mDb.rawQuery(sql + " ORDER BY (_name) COLLATE NOCASE ", args);
        ArrayList<App> listApp = new ArrayList<>();
        if (c != null) {
            try {
                if (c.moveToFirst()) {
                    do {
                        App app = new App();
                        app.setId(c.getLong(0));
                        app.setName(c.getString(1));
                        app.setIcon(BitmapUtil.fromByte(c.getBlob(2)));
                        app.setPackage(c.getString(3));
                        app.setVersion(c.getString(4));
                        app.setLockMethod(LockMethod.getMethod(c.getInt(5)));
                        if (c.getInt(6) == 1) {
                            z = true;
                        } else {
                            z = false;
                        }
                        app.setUseDefaultLockMethod(z);
                        app.setExtra1(c.getLong(7));
                        app.setExtra2(c.getString(8));
                        app.setExtra3(c.getString(9));
                        app.setExtra3(c.getString(9));
                        if (c.getInt(10) == 1) {
                            z2 = true;
                        } else {
                            z2 = false;
                        }
                        app.setUseLock(z2);
                        listApp.add(app);
                    } while (c.moveToNext());
                }
            } catch (Throwable th) {
                c.close();
                throw th;
            }
        }
        c.close();
        return listApp;
    }

    public long insertOrUpdateApp(App app, Boolean... params) {
        int i;
        boolean update = true;
        boolean insert = true;
        if (params != null) {
            if (params.length > 0) {
                update = params[0].booleanValue();
            }
            if (params.length > 1) {
                insert = params[1].booleanValue();
            }
        }
        ContentValues v = new ContentValues();
        v.put(NAME, app.getName());
        v.put(ICON, BitmapUtil.toByte(app.getIcon()));
        v.put(PACKAGE, app.getPackage());
        v.put(VERSION, app.getVersion());
        v.put(EXTRA1, Long.valueOf(app.getExtra1()));
        v.put(EXTRA2, app.getExtra2());
        v.put(EXTRA3, app.getExtra3());
        v.put("_lock_method", Integer.valueOf(app.getLockMethod().getType()));
        v.put(USE_DEFAULT_METHOD, Integer.valueOf(app.isUseDefaultLockMethod() ? 1 : 0));
        if (app.isUseLock()) {
            i = 1;
        } else {
            i = 0;
        }
        v.put(USE_LOCK, Integer.valueOf(i));
        if (isExistApp(app)) {
            if (update) {
                return (long) this.mDb.update(TABLE_APPS, v, "_package= ? ", new String[]{"" + app.getPackage()});
            }
        } else if (insert) {
            return this.mDb.insert(TABLE_APPS, null, v);
        }
        return -1;
    }

    /* JADX INFO: finally extract failed */
    private boolean isExistApp(App app) {
        Cursor c = this.mDb.rawQuery("select _id from apps a where a._package = ?", new String[]{app.getPackage()});
        try {
            if (c.getCount() > 0) {
                c.close();
                return true;
            }
            c.close();
            return false;
        } catch (Throwable th) {
            c.close();
            throw th;
        }
    }

    public void removeApp(String packageName) {
        this.mDb.delete(TABLE_APPS, "_package = ?", new String[]{packageName});
    }

    /* JADX INFO: finally extract failed */
    private boolean isExistKey(String key) {
        Cursor c = this.mDb.rawQuery("select _key from conf where _key = ?", new String[]{key});
        try {
            if (c.getCount() > 0) {
                c.close();
                return true;
            }
            c.close();
            return false;
        } catch (Throwable th) {
            c.close();
            throw th;
        }
    }

    public String getConfValue(String key, String defVal) {
        Cursor c = this.mDb.rawQuery("select _value from conf where _key = ?", new String[]{key});
        if (c != null) {
            try {
                if (c.moveToFirst()) {
                    return c.getString(0);
                }
            } finally {
                c.close();
            }
        }
        c.close();
        return defVal;
    }

    public void insertOrUpdateConf(String key, String value) {
        if (isExistKey(key)) {
            updateConf(key, value);
        } else {
            insertConf(key, value);
        }
    }

    private void updateConf(String key, String value) {
        ContentValues v = new ContentValues();
        v.put("_key", key);
        v.put("_value", value);
        this.mDb.update("conf", v, "_key=?", new String[]{key});
    }

    private void insertConf(String key, String value) {
        ContentValues v = new ContentValues();
        v.put("_key", key);
        v.put("_value", value);
        this.mDb.insert("conf", null, v);
    }

    private static class OpenHelper extends SQLiteOpenHelper {
        OpenHelper(Context context) {
            super(context, DbHelper.DATABASE_FILE, (SQLiteDatabase.CursorFactory) null, DbHelper.DATABASE_VERSION);
        }

        public void onCreate(SQLiteDatabase db) {
            db.execSQL("create table IF NOT EXISTS  apps(_id integer primary key autoincrement, _name text , _package text, _version text, _use_default_method integer, _extra1 integer, _extra2 text, _extra3 text, _icon blob, _lock_method integer, _use_lock integer );");
            db.execSQL("create table IF NOT EXISTS  conf (_id integer primary key autoincrement,  _key text , _value text);");
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("create table IF NOT EXISTS  conf (_id integer primary key autoincrement,  _key text , _value text);");
            onCreate(db);
        }
    }
}
