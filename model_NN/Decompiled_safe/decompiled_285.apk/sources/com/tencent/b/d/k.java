package com.tencent.b.d;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Proxy;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.tencent.b.b.b;
import com.tencent.b.b.c;
import com.tencent.connect.common.Constants;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.net.InetAddress;
import java.net.URL;
import java.util.Collections;
import java.util.List;
import java.util.zip.GZIPInputStream;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import org.apache.http.HttpHost;
import org.json.JSONArray;
import org.json.JSONObject;

public class k {
    public static b a(b bVar, b bVar2) {
        if (bVar != null && bVar2 != null) {
            return bVar.a(bVar2) >= 0 ? bVar : bVar2;
        }
        if (bVar != null) {
            return bVar;
        }
        if (bVar2 != null) {
            return bVar2;
        }
        return null;
    }

    public static String a() {
        return "http://pingmid.qq.com:80/";
    }

    public static JSONArray a(Context context, int i) {
        List<ScanResult> scanResults;
        try {
            if (!a(context, "android.permission.INTERNET") || !a(context, "android.permission.ACCESS_NETWORK_STATE")) {
                a("can not get the permisson of android.permission.INTERNET");
                return null;
            }
            WifiManager wifiManager = (WifiManager) context.getSystemService("wifi");
            if (!(wifiManager == null || (scanResults = wifiManager.getScanResults()) == null || scanResults.size() <= 0)) {
                Collections.sort(scanResults, new l());
                JSONArray jSONArray = new JSONArray();
                int i2 = 0;
                while (i2 < scanResults.size() && i2 < i) {
                    ScanResult scanResult = scanResults.get(i2);
                    JSONObject jSONObject = new JSONObject();
                    jSONObject.put("bs", scanResult.BSSID);
                    jSONObject.put("ss", scanResult.SSID);
                    jSONArray.put(jSONObject);
                    i2++;
                }
                return jSONArray;
            }
            return null;
        } catch (Throwable th) {
            a(th);
        }
    }

    public static void a(String str) {
        if (c.a()) {
            Log.i("MID", str);
        }
    }

    public static void a(Throwable th) {
        if (c.a()) {
            Log.w("MID", th);
        }
    }

    public static void a(JSONObject jSONObject, String str, String str2) {
        if (b(str2)) {
            jSONObject.put(str, str2);
        }
    }

    public static boolean a(Context context) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        return (activeNetworkInfo == null || activeNetworkInfo.getTypeName() == null || !activeNetworkInfo.getTypeName().equalsIgnoreCase("WIFI")) ? false : true;
    }

    public static boolean a(Context context, String str) {
        try {
            return context.getPackageManager().checkPermission(str, context.getPackageName()) == 0;
        } catch (Throwable th) {
            Log.e("MID", "checkPermission error", th);
            return false;
        }
    }

    public static byte[] a(String str, String str2) {
        try {
            SecretKeySpec secretKeySpec = new SecretKeySpec(str.getBytes(), "hmacmd5");
            Mac instance = Mac.getInstance("HmacSHA1");
            instance.init(secretKeySpec);
            instance.update(str2.getBytes());
            return instance.doFinal();
        } catch (Exception e) {
            a(e);
            return null;
        }
    }

    public static byte[] a(byte[] bArr) {
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bArr);
        GZIPInputStream gZIPInputStream = new GZIPInputStream(byteArrayInputStream);
        byte[] bArr2 = new byte[4096];
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(bArr.length * 2);
        while (true) {
            int read = gZIPInputStream.read(bArr2);
            if (read != -1) {
                byteArrayOutputStream.write(bArr2, 0, read);
            } else {
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                byteArrayInputStream.close();
                gZIPInputStream.close();
                byteArrayOutputStream.close();
                return byteArray;
            }
        }
    }

    public static boolean b(Context context) {
        ConnectivityManager connectivityManager;
        try {
            if (a(context, "android.permission.INTERNET") && a(context, "android.permission.ACCESS_NETWORK_STATE") && (connectivityManager = (ConnectivityManager) context.getSystemService("connectivity")) != null) {
                NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                if (activeNetworkInfo != null && activeNetworkInfo.isAvailable()) {
                    return true;
                }
                Log.w("MID", "Network error");
                return false;
            }
        } catch (Throwable th) {
            Log.e("MID", "isNetworkAvailable error", th);
        }
        return false;
    }

    public static boolean b(b bVar, b bVar2) {
        return (bVar == null || bVar2 == null) ? bVar == null && bVar2 == null : bVar.a(bVar2) == 0;
    }

    public static boolean b(String str) {
        return (str == null || str.trim().length() == 0) ? false : true;
    }

    public static String c(Context context) {
        try {
            if (a(context, "android.permission.READ_PHONE_STATE")) {
                String deviceId = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
                if (deviceId != null) {
                    return deviceId;
                }
            } else {
                a("Could not get permission of android.permission.READ_PHONE_STATE");
            }
        } catch (Throwable th) {
            a(th);
        }
        return Constants.STR_EMPTY;
    }

    public static boolean c(String str) {
        return str != null && str.trim().length() >= 40;
    }

    public static String d(Context context) {
        if (a(context, "android.permission.ACCESS_WIFI_STATE")) {
            try {
                WifiManager wifiManager = (WifiManager) context.getSystemService("wifi");
                return wifiManager == null ? Constants.STR_EMPTY : wifiManager.getConnectionInfo().getMacAddress();
            } catch (Exception e) {
                a("get wifi address error" + e);
                return Constants.STR_EMPTY;
            }
        } else {
            a("Could not get permission of android.permission.ACCESS_WIFI_STATE");
            return Constants.STR_EMPTY;
        }
    }

    public static String d(String str) {
        if (str == null) {
            return null;
        }
        if (Build.VERSION.SDK_INT < 8) {
            return str;
        }
        try {
            return new String(h.b(a.a(str.getBytes("UTF-8"), 0)), "UTF-8").trim().replace("\t", Constants.STR_EMPTY).replace("\n", Constants.STR_EMPTY).replace("\r", Constants.STR_EMPTY);
        } catch (Throwable th) {
            Log.e("MID", "decode error", th);
            return str;
        }
    }

    public static String e(String str) {
        if (str == null) {
            return null;
        }
        if (Build.VERSION.SDK_INT < 8) {
            return str;
        }
        try {
            return new String(a.b(h.a(str.getBytes("UTF-8")), 0), "UTF-8").trim().replace("\t", Constants.STR_EMPTY).replace("\n", Constants.STR_EMPTY).replace("\r", Constants.STR_EMPTY);
        } catch (Throwable th) {
            Log.w("MID", "encode error", th);
            return str;
        }
    }

    public static HttpHost e(Context context) {
        if (context == null) {
            return null;
        }
        try {
            if (context.getPackageManager().checkPermission("android.permission.ACCESS_NETWORK_STATE", context.getPackageName()) != 0) {
                return null;
            }
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            if (activeNetworkInfo == null) {
                return null;
            }
            if (activeNetworkInfo.getTypeName() != null && activeNetworkInfo.getTypeName().equalsIgnoreCase("WIFI")) {
                return null;
            }
            String extraInfo = activeNetworkInfo.getExtraInfo();
            a("network type:" + extraInfo);
            if (extraInfo == null) {
                return null;
            }
            if (extraInfo.equals("cmwap") || extraInfo.equals("3gwap") || extraInfo.equals("uniwap")) {
                return new HttpHost("10.0.0.172", 80);
            }
            if (extraInfo.equals("ctwap")) {
                return new HttpHost("10.0.0.200", 80);
            }
            String defaultHost = Proxy.getDefaultHost();
            if (defaultHost != null && defaultHost.trim().length() > 0) {
                return new HttpHost(defaultHost, Proxy.getDefaultPort());
            }
            return null;
        } catch (Throwable th) {
            a(th);
        }
    }

    public static WifiInfo f(Context context) {
        WifiManager wifiManager;
        if (!a(context, "android.permission.ACCESS_WIFI_STATE") || (wifiManager = (WifiManager) context.getApplicationContext().getSystemService("wifi")) == null) {
            return null;
        }
        return wifiManager.getConnectionInfo();
    }

    public static String f(String str) {
        InetAddress byName;
        try {
            URL url = new URL(str);
            if (!(url == null || (byName = InetAddress.getByName(url.getHost())) == null)) {
                return byName.getHostAddress();
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
        return null;
    }

    public static String g(Context context) {
        try {
            WifiInfo f = f(context);
            if (f != null) {
                return f.getBSSID();
            }
        } catch (Throwable th) {
            a(th);
        }
        return null;
    }

    public static String h(Context context) {
        try {
            WifiInfo f = f(context);
            if (f != null) {
                return f.getSSID();
            }
        } catch (Throwable th) {
            a(th);
        }
        return null;
    }
}
