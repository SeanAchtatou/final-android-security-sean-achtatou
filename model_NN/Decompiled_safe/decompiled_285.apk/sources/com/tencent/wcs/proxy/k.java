package com.tencent.wcs.proxy;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.tencent.wcs.b.c;

/* compiled from: ProGuard */
class k implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ j f4140a;

    k(j jVar) {
        this.f4140a = jVar;
    }

    public void run() {
        this.f4140a.f4139a.q();
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.f4140a.f4139a.f4124a.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
            if (activeNetworkInfo.getType() == 1) {
                this.f4140a.f4139a.r();
            } else if (activeNetworkInfo.getType() == 0 && c.m) {
                this.f4140a.f4139a.r();
            }
        }
        boolean unused = this.f4140a.f4139a.k = false;
    }
}
