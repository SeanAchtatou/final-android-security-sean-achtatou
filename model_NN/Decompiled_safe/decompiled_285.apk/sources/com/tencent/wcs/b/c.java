package com.tencent.wcs.b;

import android.os.Process;
import com.qq.ndk.NativeFileObject;

/* compiled from: ProGuard */
public class c {

    /* renamed from: a  reason: collision with root package name */
    public static boolean f4064a = false;
    public static int b = 1;
    public static int c = 16;
    public static int d = 20;
    public static int e = 64;
    public static int f = NativeFileObject.S_IFREG;
    public static float g = 1.2f;
    public static int h = Process.PROC_COMBINE;
    public static int i = 180;
    public static int j = 90;
    public static int k = 300;
    public static int l = 15;
    public static boolean m = false;
    public static int n = 20;
    public static int o = 60;
    public static int p = 300;
    public static boolean q = true;

    public static void a(String str, String str2) {
        try {
            if ("VERSION".equalsIgnoreCase(str)) {
                b = Integer.parseInt(str2);
            } else if ("COUNT_LOST_THRESHOLD".equalsIgnoreCase(str)) {
                c = Integer.parseInt(str2);
            } else if ("TIME_LOST_THRESHOLD".equalsIgnoreCase(str)) {
                d = Integer.parseInt(str2);
            } else if ("COUNT_LOST_UPPER_LIMIT".equalsIgnoreCase(str)) {
                e = Integer.parseInt(str2);
            } else if ("CACHE_SIZE_FOR_ACK".equalsIgnoreCase(str)) {
                f = Integer.parseInt(str2);
            } else if ("CACHE_SIZE_FACTOR".equals(str)) {
                g = Float.parseFloat(str2);
                if (Float.compare(g, 1.0f) < 0) {
                    g = 1.2f;
                }
            } else if ("GZIP_LOWER_THRESHOLD".equalsIgnoreCase(str)) {
                h = Integer.parseInt(str2);
            } else if ("REMOTE_OFFLINE_CACHE_TIME".equalsIgnoreCase(str)) {
                i = Integer.parseInt(str2);
            } else if ("CONNECTION_CHECK_INTERVAL".equalsIgnoreCase(str)) {
                j = Integer.parseInt(str2);
            } else if ("HEARTBEAT_INTERVAL".equalsIgnoreCase(str)) {
                k = Integer.parseInt(str2);
            } else if ("HEARTBEAT_TIMEOUT".equalsIgnoreCase(str)) {
                l = Integer.parseInt(str2);
            } else {
                if ("ALLOW_MOBILE_NETWORK".equalsIgnoreCase(str)) {
                    m = Boolean.parseBoolean(str2.toLowerCase());
                }
                if ("MAX_AGENT_SESSION_POOL_SIZE".equalsIgnoreCase(str)) {
                    n = Integer.parseInt(str2);
                } else if ("WCS_LOGIN_TIMEOUT".equals(str)) {
                    o = Integer.parseInt(str2);
                } else if ("CPU_LOCK_TIMEOUT".equals(str)) {
                    p = Integer.parseInt(str2);
                } else if ("WCS_AUTO_CLOSE".equals(str)) {
                    q = Boolean.parseBoolean(str2.toLowerCase());
                }
            }
        } catch (NumberFormatException e2) {
        }
    }
}
