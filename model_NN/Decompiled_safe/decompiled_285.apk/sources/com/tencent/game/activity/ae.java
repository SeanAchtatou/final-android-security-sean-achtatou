package com.tencent.game.activity;

import android.view.View;
import android.widget.AbsListView;
import com.tencent.assistant.component.invalidater.TXRefreshGetMoreListViewScrollListener;
import com.tencent.assistantv2.adapter.smartlist.SmartListAdapter;

/* compiled from: ProGuard */
class ae extends TXRefreshGetMoreListViewScrollListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ w f2614a;

    private ae(w wVar) {
        this.f2614a = wVar;
    }

    /* synthetic */ ae(w wVar, x xVar) {
        this(wVar);
    }

    public void onScroll(View view, int i, int i2, int i3) {
        boolean z = true;
        super.onScroll(view, i, i2, i3);
        if (this.f2614a.aa != null) {
            SmartListAdapter.BannerType unused = this.f2614a.ab = this.f2614a.aa.p();
            boolean z2 = this.f2614a.ab == SmartListAdapter.BannerType.None;
            if (this.f2614a.ab != SmartListAdapter.BannerType.HomePage) {
                z = false;
            }
            if ((!z2 && !z) && this.f2614a.ac != null) {
                this.f2614a.ac.a((AbsListView) view, i, i2, i3);
            }
        }
    }

    public void onScrollStateChanged(AbsListView absListView, int i) {
        super.onScrollStateChanged(absListView, i);
        if (this.f2614a.ac != null) {
            this.f2614a.ac.b(absListView, i);
        }
    }
}
