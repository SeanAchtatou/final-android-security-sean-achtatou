package com.tencent.game.activity;

import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewStub;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.LoadingView;
import com.tencent.assistant.component.NormalErrorRecommendPage;
import com.tencent.assistant.manager.i;
import com.tencent.assistant.protocol.jce.GftAppGiftInfo;
import com.tencent.assistant.protocol.jce.GftGetMyDesktopResponse;
import com.tencent.assistant.protocol.jce.GftMydesktopOtherAppInfo;
import com.tencent.assistant.protocol.jce.OneAppGift;
import com.tencent.assistant.protocol.jce.SimpleAppInfo;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.by;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.game.adapter.a;
import com.tencent.game.c.e;
import com.tencent.game.component.GameHorizontalCard;
import com.tencent.game.component.GameNormalSmartcardBaseItem;
import com.tencent.game.d.a.b;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class GameDesktopShortActivity extends BaseActivity implements b {
    private LinearLayout A;
    private View B;
    private LinearLayout C;
    private RelativeLayout D;
    private TextView E;
    private TextView F;
    private DownloadButton G;
    private RelativeLayout H;
    private TextView I;
    private TextView J;
    private DownloadButton K;
    private LinearLayout L;
    private LinearLayout M;
    private TextView N;
    private ViewPager O;
    private a P;
    private ArrayList<View> Q;
    private ViewStub R;
    private ViewStub S;
    /* access modifiers changed from: private */
    public LoadingView T;
    /* access modifiers changed from: private */
    public NormalErrorRecommendPage U;
    private View.OnClickListener V = new d(this);
    private final String n = "GameDesktopShortActivity";
    /* access modifiers changed from: private */
    public e u = new e();
    private ViewPager v;
    private a w;
    private f x;
    private ArrayList<View> y;
    private LinearLayout z;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_game_short_cut);
        u();
        if (this.T == null) {
            this.T = (LoadingView) this.S.inflate();
        }
        this.y = new ArrayList<>();
        this.Q = new ArrayList<>();
        this.u.a(this);
        this.u.b();
        this.u.a();
    }

    private void u() {
        this.v = (ViewPager) findViewById(R.id.my_game_pager);
        this.w = new a();
        this.x = new f(this);
        this.v.setOnPageChangeListener(this.x);
        this.z = (LinearLayout) findViewById(R.id.my_game_pager_error);
        this.A = (LinearLayout) findViewById(R.id.points);
        this.B = findViewById(R.id.cutline);
        this.M = (LinearLayout) findViewById(R.id.competitive_layout);
        this.N = (TextView) findViewById(R.id.competitive_title);
        this.O = (ViewPager) findViewById(R.id.my_competitive_game_pager);
        this.P = new a();
        this.C = (LinearLayout) findViewById(R.id.libao);
        this.D = (RelativeLayout) findViewById(R.id.libao_layout);
        this.E = (TextView) findViewById(R.id.libao_desc);
        this.F = (TextView) findViewById(R.id.libao_detail);
        this.G = (DownloadButton) findViewById(R.id.libaoBtn);
        this.H = (RelativeLayout) findViewById(R.id.libao_layout2);
        this.I = (TextView) findViewById(R.id.libao_desc2);
        this.J = (TextView) findViewById(R.id.libao_detail2);
        this.K = (DownloadButton) findViewById(R.id.libaoBtn2);
        this.L = (LinearLayout) findViewById(R.id.libaoMore);
        this.S = (ViewStub) findViewById(R.id.loading_stub);
        this.T = (LoadingView) findViewById(R.id.loading_view);
        this.R = (ViewStub) findViewById(R.id.error_page_stub);
        this.U = (NormalErrorRecommendPage) findViewById(R.id.error_page);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    public void a(int i, int i2, List<SimpleAppInfo> list, GftAppGiftInfo gftAppGiftInfo, GftMydesktopOtherAppInfo gftMydesktopOtherAppInfo) {
        if (i2 == 0) {
            if (this.T != null) {
                this.T.setVisibility(8);
            }
            if (this.U != null) {
                this.U.setVisibility(8);
            }
            a(i, list, gftAppGiftInfo, gftMydesktopOtherAppInfo);
            return;
        }
        GftGetMyDesktopResponse s = i.y().s();
        if (i != 0 && i2 != 0 && s == null) {
            if (this.T != null) {
                this.T.setVisibility(8);
            }
            v();
            if (i2 == -800) {
                b(30);
            } else {
                b(10);
            }
        }
    }

    private void v() {
        this.v.setVisibility(8);
        this.z.setVisibility(8);
        this.B.setVisibility(8);
        this.C.setVisibility(8);
        this.M.setVisibility(8);
    }

    private void b(int i) {
        if (this.T != null) {
            this.T.setVisibility(8);
        }
        v();
        if (this.U == null) {
            this.U = (NormalErrorRecommendPage) this.R.inflate();
            this.U.setButtonClickListener(this.V);
            this.U.setActivityPageId(f());
            ((RelativeLayout) this.U.findViewById(R.id.normal_error_recommend_page)).setBackgroundColor(16777215);
        } else {
            this.U.setVisibility(0);
        }
        this.U.setErrorType(i);
        switch (i) {
            case 10:
                this.U.setErrorImage(R.drawable.network_disconnected);
                this.U.setErrorText(getResources().getString(R.string.game_desktop_error_network_instability), R.color.common_topbar_text_color);
                return;
            case 30:
                this.U.setErrorText(getResources().getString(R.string.game_desktop_error_no_network));
                return;
            default:
                return;
        }
    }

    private void a(int i, List<SimpleAppInfo> list, GftAppGiftInfo gftAppGiftInfo, GftMydesktopOtherAppInfo gftMydesktopOtherAppInfo) {
        a(i, list);
        c(0);
        if (a(i, gftAppGiftInfo)) {
            this.M.setVisibility(8);
        } else {
            a(i, gftMydesktopOtherAppInfo);
        }
    }

    private void a(int i, List<SimpleAppInfo> list) {
        int i2;
        int i3;
        ArrayList arrayList;
        PackageInfo d;
        if (list == null || list.size() == 0) {
            this.v.setVisibility(8);
            this.z.setVisibility(0);
            return;
        }
        ArrayList<SimpleAppInfo> arrayList2 = new ArrayList<>();
        for (SimpleAppInfo next : list) {
            if (!(next == null || (d = com.tencent.assistant.utils.e.d(next.f, 0)) == null || (d.applicationInfo.flags & 1) != 0)) {
                arrayList2.add(next);
            }
        }
        if (arrayList2.size() == 0) {
            this.v.setVisibility(8);
            this.z.setVisibility(0);
            return;
        }
        this.v.setVisibility(0);
        this.z.setVisibility(8);
        this.y.clear();
        ArrayList arrayList3 = new ArrayList();
        int size = ((arrayList2.size() + 4) - 1) / 4;
        int size2 = arrayList2.size() % 4;
        if (size2 == 0) {
            i2 = 4;
        } else {
            i2 = size2;
        }
        ArrayList arrayList4 = arrayList3;
        int i4 = 0;
        int i5 = size;
        LinearLayout linearLayout = null;
        for (SimpleAppInfo simpleAppInfo : arrayList2) {
            if (simpleAppInfo != null) {
                if (i4 % 8 == 0) {
                    linearLayout = t();
                    linearLayout.setOrientation(1);
                    this.y.add(linearLayout);
                }
                i4++;
                arrayList4.add(simpleAppInfo);
                if (i5 == 1 || arrayList4.size() != 4) {
                    i3 = i5;
                    arrayList = arrayList4;
                } else {
                    GameHorizontalCard gameHorizontalCard = new GameHorizontalCard(this, arrayList4, new h(this, null));
                    if (linearLayout != null) {
                        linearLayout.addView(gameHorizontalCard);
                    }
                    i3 = i5 - 1;
                    arrayList = new ArrayList();
                }
                if (i3 == 1 && arrayList.size() == i2) {
                    GameHorizontalCard gameHorizontalCard2 = new GameHorizontalCard(this, arrayList, new h(this, null));
                    if (linearLayout != null) {
                        linearLayout.addView(gameHorizontalCard2);
                    }
                }
                arrayList4 = arrayList;
                i5 = i3;
            }
        }
        this.w.a(this.y);
        this.v.setAdapter(this.w);
        this.w.notifyDataSetChanged();
        if (i != 0) {
            b("001_000");
        }
    }

    private boolean a(int i, GftAppGiftInfo gftAppGiftInfo) {
        if (gftAppGiftInfo == null) {
            this.B.setVisibility(8);
            this.C.setVisibility(8);
            return false;
        }
        String str = gftAppGiftInfo.b;
        ArrayList<OneAppGift> a2 = gftAppGiftInfo.a();
        if (a2 == null || a2.size() == 0) {
            this.B.setVisibility(8);
            this.C.setVisibility(8);
            return false;
        }
        this.B.setVisibility(0);
        this.C.setVisibility(0);
        int size = a2.size();
        switch (size) {
            case 0:
                this.C.setVisibility(8);
                break;
            case 1:
                OneAppGift oneAppGift = a2.get(size - 1);
                if (oneAppGift == null) {
                    this.C.setVisibility(8);
                    break;
                } else {
                    this.E.setText(oneAppGift.d);
                    this.F.setText(oneAppGift.c);
                    this.G.setOnClickListener(new g(this, oneAppGift.e, "005_001"));
                    this.H.setVisibility(8);
                    break;
                }
            case 2:
                int i2 = 0;
                for (OneAppGift next : a2) {
                    if (next != null) {
                        i2++;
                        if (i2 == 1) {
                            this.E.setText(next.d);
                            this.F.setText(next.c);
                            this.G.setOnClickListener(new g(this, next.e, "005_001"));
                        } else if (i2 == 2) {
                            this.I.setText(next.d);
                            this.J.setText(next.c);
                            this.K.setOnClickListener(new g(this, next.e, "005_002"));
                        }
                    }
                }
                switch (i2) {
                    case 0:
                        this.C.setVisibility(8);
                        break;
                    case 1:
                        this.H.setVisibility(8);
                        break;
                }
            default:
                int i3 = 0;
                for (OneAppGift next2 : a2) {
                    if (next2 != null) {
                        i3++;
                        if (i3 == 1) {
                            this.E.setText(next2.d);
                            this.F.setText(next2.c);
                            this.G.setOnClickListener(new g(this, next2.e, "005_001"));
                        } else if (i3 == 2) {
                            this.I.setText(next2.d);
                            this.J.setText(next2.c);
                            this.K.setOnClickListener(new g(this, next2.e, "005_002"));
                        }
                    }
                }
                switch (i3) {
                    case 0:
                        this.C.setVisibility(8);
                        break;
                    case 1:
                        this.H.setVisibility(8);
                        break;
                }
        }
        this.L.setOnClickListener(new g(this, str, "005_003"));
        if (i != 0) {
            b("005_000");
        }
        return true;
    }

    private void a(int i, GftMydesktopOtherAppInfo gftMydesktopOtherAppInfo) {
        int i2;
        int i3;
        ArrayList arrayList;
        if (gftMydesktopOtherAppInfo == null) {
            this.B.setVisibility(8);
            this.M.setVisibility(8);
            return;
        }
        ArrayList<SimpleAppInfo> a2 = gftMydesktopOtherAppInfo.a();
        if (a2 == null || a2.size() == 0) {
            this.B.setVisibility(8);
            this.M.setVisibility(8);
            return;
        }
        this.B.setVisibility(0);
        this.M.setVisibility(0);
        this.N.setText(gftMydesktopOtherAppInfo.f1379a);
        this.Q.clear();
        ArrayList arrayList2 = new ArrayList();
        int i4 = gftMydesktopOtherAppInfo.b;
        int size = ((a2.size() + 4) - 1) / 4;
        int size2 = a2.size() % 4;
        if (size2 == 0) {
            i2 = 4;
        } else {
            i2 = size2;
        }
        for (SimpleAppInfo next : a2) {
            if (next != null) {
                arrayList2.add(next);
                if (size == 1 || arrayList2.size() != 4) {
                    i3 = size;
                    arrayList = arrayList2;
                } else {
                    this.Q.add(b(i4, arrayList2));
                    i3 = size - 1;
                    arrayList = new ArrayList();
                }
                if (i3 == 1 && arrayList.size() == i2) {
                    this.Q.add(b(i4, arrayList));
                }
                arrayList2 = arrayList;
                size = i3;
            }
        }
        this.P.a(this.Q);
        this.O.setAdapter(this.P);
        this.P.notifyDataSetChanged();
        if (i != 0) {
            switch (i4) {
                case 1:
                    b("003_000");
                    return;
                case 2:
                    b("004_000");
                    return;
                default:
                    b("004_000");
                    return;
            }
        }
    }

    private View b(int i, List<SimpleAppInfo> list) {
        if (list == null || list.size() == 0) {
            return null;
        }
        switch (i) {
            case 1:
                return new GameHorizontalCard(this, list, new h(this, null), GameNormalSmartcardBaseItem.HorizontalCardType.FRIEND_PLAYING_GAME);
            case 2:
                return new GameHorizontalCard(this, list, new h(this, null), GameNormalSmartcardBaseItem.HorizontalCardType.COMPETITIVE_GAME);
            default:
                return new GameHorizontalCard(this, list, new h(this, null), GameNormalSmartcardBaseItem.HorizontalCardType.COMPETITIVE_GAME);
        }
    }

    /* access modifiers changed from: protected */
    public LinearLayout t() {
        LinearLayout linearLayout = new LinearLayout(this);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        linearLayout.setGravity(48);
        linearLayout.setLayoutParams(layoutParams);
        return linearLayout;
    }

    public int f() {
        return STConst.ST_PAGE_GAME_DESKTOP_SHORTCUT;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (isFinishing()) {
            return true;
        }
        finish();
        return true;
    }

    public void overridePendingTransition(int i, int i2) {
    }

    /* access modifiers changed from: private */
    public void c(int i) {
        this.A.removeAllViews();
        if (this.y.size() >= 2) {
            this.A.setVisibility(0);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
            layoutParams.leftMargin = by.b(2.0f);
            layoutParams.rightMargin = by.b(2.0f);
            layoutParams.height = by.a(this, 5.0f);
            layoutParams.width = by.a(this, 5.0f);
            for (int i2 = 0; i2 < this.y.size(); i2++) {
                ImageView imageView = new ImageView(this);
                imageView.setLayoutParams(layoutParams);
                if (i == i2) {
                    imageView.setImageResource(R.drawable.common_icon_page_current);
                } else {
                    imageView.setImageResource(R.drawable.common_icon_page);
                }
                this.A.addView(imageView);
            }
        }
    }

    private void b(String str) {
        TemporaryThreadManager.get().start(new e(this, str));
    }
}
