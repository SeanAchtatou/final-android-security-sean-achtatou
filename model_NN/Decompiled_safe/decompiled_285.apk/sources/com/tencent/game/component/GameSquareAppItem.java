package com.tencent.game.component;

import android.content.Context;
import android.text.Html;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXAppIconView;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.protocol.jce.SimpleAppInfo;
import com.tencent.assistant.smartcard.component.as;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.downloadsdk.utils.j;

/* compiled from: ProGuard */
public class GameSquareAppItem extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    private final String f2656a;
    /* access modifiers changed from: private */
    public Context b;
    private LayoutInflater c;
    /* access modifiers changed from: private */
    public TXAppIconView d;
    private TextView e;
    private TextView f;

    public GameSquareAppItem(Context context) {
        this(context, null);
    }

    public GameSquareAppItem(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f2656a = "GameSquareAppItem";
        this.b = context;
        this.c = (LayoutInflater) this.b.getSystemService("layout_inflater");
        a();
    }

    private void a() {
        this.c.inflate((int) R.layout.game_smartcard_square_item_app, this);
        this.d = (TXAppIconView) findViewById(R.id.icon);
        this.e = (TextView) findViewById(R.id.name);
        this.f = (TextView) findViewById(R.id.desc);
    }

    public void a(SimpleAppInfo simpleAppInfo, STInfoV2 sTInfoV2, as asVar) {
        if (simpleAppInfo != null) {
            j.a().post(new t(this, simpleAppInfo));
            this.e.setText(simpleAppInfo.b);
            this.f.setVisibility(8);
            setOnClickListener(new u(this, simpleAppInfo, asVar, sTInfoV2));
        }
    }

    public void b(SimpleAppInfo simpleAppInfo, STInfoV2 sTInfoV2, as asVar) {
        if (simpleAppInfo != null) {
            this.d.updateImageView(simpleAppInfo.c, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            this.e.setText(simpleAppInfo.b);
            this.f.setVisibility(0);
            this.f.setText(simpleAppInfo.A);
            setOnClickListener(new v(this, sTInfoV2, simpleAppInfo, asVar));
        }
    }

    public void c(SimpleAppInfo simpleAppInfo, STInfoV2 sTInfoV2, as asVar) {
        if (simpleAppInfo != null) {
            this.d.updateImageView(simpleAppInfo.c, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            this.e.setText(simpleAppInfo.b);
            this.f.setVisibility(0);
            this.f.setText(Html.fromHtml(simpleAppInfo.A));
            setOnClickListener(new w(this, sTInfoV2, simpleAppInfo, asVar));
        }
    }
}
