package com.tencent.game.component;

import com.tencent.assistant.component.invalidater.ViewInvalidateMessage;
import com.tencent.assistant.protocol.jce.AppCategory;
import com.tencent.assistant.protocol.jce.ColorCardItem;
import com.tencent.game.d.a.a;
import java.util.HashMap;
import java.util.List;

/* compiled from: ProGuard */
class g implements a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GameCategoryListView f2662a;

    g(GameCategoryListView gameCategoryListView) {
        this.f2662a = gameCategoryListView;
    }

    public void a(int i, int i2, List<ColorCardItem> list, List<AppCategory> list2, List<AppCategory> list3) {
        if (this.f2662a.z != null) {
            ViewInvalidateMessage viewInvalidateMessage = new ViewInvalidateMessage(1, null, this.f2662a.G);
            viewInvalidateMessage.arg1 = i2;
            viewInvalidateMessage.arg2 = i;
            HashMap hashMap = new HashMap();
            if (this.f2662a.v == -2) {
                hashMap.put(0, list);
                hashMap.put(1, list2);
                if (list3 != null) {
                    hashMap.put(2, list3);
                }
            }
            viewInvalidateMessage.params = hashMap;
            this.f2662a.z.sendMessage(viewInvalidateMessage);
        }
    }
}
