package com.tencent.game.component;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.HeaderViewListAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessage;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessageHandler;
import com.tencent.assistant.component.invalidater.ViewPageScrollListener;
import com.tencent.assistant.component.txscrollview.TXGetMoreListView;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.business.CostTimeSTManager;
import com.tencent.assistantv2.st.l;
import com.tencent.game.adapter.GameRankAggregationListAdapter;
import com.tencent.game.d.a.d;
import com.tencent.game.d.am;
import com.tencent.game.smartcard.b.b;
import java.util.List;

/* compiled from: ProGuard */
public class GameRankAggregationListView extends TXGetMoreListView {
    private final int A;
    private final int B;
    private final int C;
    private d D;
    /* access modifiers changed from: private */
    public ViewInvalidateMessageHandler E;
    private am u;
    /* access modifiers changed from: private */
    public m v;
    private int w;
    /* access modifiers changed from: private */
    public GameRankAggregationListAdapter x;
    /* access modifiers changed from: private */
    public ViewPageScrollListener y;
    private final int z;

    public void a(m mVar) {
        this.v = mVar;
    }

    public GameRankAggregationListView(Context context) {
        super(context);
        this.u = null;
        this.w = 3;
        this.z = 1;
        this.A = 2;
        this.B = 0;
        this.C = 1;
        this.D = new k(this);
        this.E = new l(this);
        this.u = new am();
        this.u.register(this.D);
        g();
        setDivider(null);
    }

    public GameRankAggregationListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.u = null;
        this.w = 3;
        this.z = 1;
        this.A = 2;
        this.B = 0;
        this.C = 1;
        this.D = new k(this);
        this.E = new l(this);
        this.u = new am();
        this.u.register(this.D);
        g();
        setDivider(null);
    }

    public GameRankAggregationListView(Context context, TXScrollViewBase.ScrollMode scrollMode) {
        super(context);
        this.u = null;
        this.w = 3;
        this.z = 1;
        this.A = 2;
        this.B = 0;
        this.C = 1;
        this.D = new k(this);
        this.E = new l(this);
        this.u = new am();
        this.u.register(this.D);
        g();
        setDivider(null);
    }

    public void a(ViewPageScrollListener viewPageScrollListener) {
        this.y = viewPageScrollListener;
    }

    public void g() {
        ListAdapter adapter = ((ListView) this.s).getAdapter();
        if (adapter instanceof HeaderViewListAdapter) {
            this.x = (GameRankAggregationListAdapter) ((HeaderViewListAdapter) adapter).getWrappedAdapter();
        } else {
            this.x = (GameRankAggregationListAdapter) ((ListView) this.s).getAdapter();
        }
    }

    public void q() {
        if (this.x != null) {
        }
    }

    public void r() {
        if (this.x != null) {
            this.x.notifyDataSetChanged();
        }
    }

    public void recycleData() {
        super.recycleData();
        this.u.unregister(this.D);
    }

    public void s() {
        if (this.x == null) {
            g();
        }
        if (this.x == null || this.x.getCount() <= 0) {
            this.u.b();
            return;
        }
        this.y.sendMessage(new ViewInvalidateMessage(2, null, this.E));
    }

    /* access modifiers changed from: private */
    public void a(int i, int i2, List<b> list, int i3) {
        if (i2 == 0) {
            this.v.u();
            if (!(this.x == null || list == null)) {
                this.x.a(list, i3);
                l.a((int) STConst.ST_PAGE_GAME_RANK_THREE, CostTimeSTManager.TIMETYPE.END, System.currentTimeMillis());
            }
        } else {
            l.a((int) STConst.ST_PAGE_GAME_RANK_THREE, CostTimeSTManager.TIMETYPE.CANCEL, System.currentTimeMillis());
            if (-800 == i2) {
                this.v.b(30);
                return;
            } else if (this.w <= 0) {
                this.v.b(20);
                return;
            } else {
                this.u.c();
                this.w--;
            }
        }
        if (this.x == null) {
            g();
        }
        if (this.x != null) {
            this.x.notifyDataSetChanged();
        }
    }

    public ListView getListView() {
        return (ListView) this.s;
    }
}
