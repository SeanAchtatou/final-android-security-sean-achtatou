package com.tencent.game.component;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.protocol.jce.SimpleAppInfo;
import com.tencent.assistant.smartcard.component.as;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.game.component.GameNormalSmartcardBaseItem;
import java.util.List;

/* compiled from: ProGuard */
public class GameHorizontalCard extends GameNormalSmartcardBaseItem {
    private LinearLayout j;

    public GameHorizontalCard(Context context) {
        super(context);
    }

    public GameHorizontalCard(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public GameHorizontalCard(Context context, List<SimpleAppInfo> list, as asVar) {
        super(context, list, null, asVar, null);
    }

    public GameHorizontalCard(Context context, List<SimpleAppInfo> list, as asVar, GameNormalSmartcardBaseItem.HorizontalCardType horizontalCardType) {
        super(context, list, null, asVar, null, horizontalCardType);
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.d.inflate((int) R.layout.game_smartcard_horizontal, this);
        this.j = (LinearLayout) findViewById(R.id.content);
        d();
    }

    private void d() {
        switch (j.f2664a[this.b.ordinal()]) {
            case 1:
                e();
                return;
            case 2:
                f();
                return;
            case 3:
                g();
                return;
            default:
                e();
                return;
        }
    }

    private void e() {
        if (this.f2653a != null && this.f2653a.size() != 0) {
            this.j.removeAllViews();
            int i = 0;
            for (SimpleAppInfo simpleAppInfo : this.f2653a) {
                GameSquareAppItem gameSquareAppItem = new GameSquareAppItem(getContext());
                STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.c, 200);
                if (buildSTInfo != null) {
                    buildSTInfo.slotId = a.a("001", i);
                }
                i++;
                gameSquareAppItem.a(simpleAppInfo, buildSTInfo, this.f);
                this.j.addView(gameSquareAppItem);
            }
        }
    }

    private void f() {
        if (this.f2653a != null && this.f2653a.size() != 0) {
            this.j.removeAllViews();
            int i = 0;
            for (SimpleAppInfo simpleAppInfo : this.f2653a) {
                GameSquareAppItem gameSquareAppItem = new GameSquareAppItem(getContext());
                STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.c, 200);
                if (buildSTInfo != null) {
                    buildSTInfo.slotId = a.a("003", i);
                }
                i++;
                gameSquareAppItem.c(simpleAppInfo, buildSTInfo, this.f);
                this.j.addView(gameSquareAppItem);
            }
        }
    }

    private void g() {
        if (this.f2653a != null && this.f2653a.size() != 0) {
            this.j.removeAllViews();
            int i = 0;
            for (SimpleAppInfo simpleAppInfo : this.f2653a) {
                GameSquareAppItem gameSquareAppItem = new GameSquareAppItem(getContext());
                STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.c, 200);
                if (buildSTInfo != null) {
                    buildSTInfo.slotId = a.a("004", i);
                }
                i++;
                gameSquareAppItem.b(simpleAppInfo, buildSTInfo, this.f);
                this.j.addView(gameSquareAppItem);
            }
        }
    }
}
