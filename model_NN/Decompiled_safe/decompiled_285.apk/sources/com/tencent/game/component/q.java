package com.tencent.game.component;

import android.view.View;
import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.m;
import com.tencent.assistantv2.manager.a;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.game.activity.n;
import com.tencent.nucleus.manager.component.SwitchButton;

/* compiled from: ProGuard */
class q extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GameRankNormalListView f2670a;

    q(GameRankNormalListView gameRankNormalListView) {
        this.f2670a = gameRankNormalListView;
    }

    public void onTMAClick(View view) {
        boolean z = true;
        this.f2670a.F++;
        if (this.f2670a.F < 10) {
            int i = this.f2670a.F - 1;
            this.f2670a.E[i] = true;
            if (i > 0) {
                this.f2670a.E[i - 1] = false;
            }
            this.f2670a.B.postDelayed(new r(this, i), 2000);
            if (this.f2670a.F == 5) {
                this.f2670a.A.setBackgroundResource(R.color.apk_list_bg);
            }
            if (this.f2670a.F > 5) {
                this.f2670a.D.setText(GameRankNormalListView.O[this.f2670a.F % 5]);
                this.f2670a.D.setVisibility(0);
                return;
            }
            SwitchButton switchButton = this.f2670a.B;
            if (this.f2670a.B.b()) {
                z = false;
            }
            switchButton.b(z);
            m.a().t(this.f2670a.B.b());
            a.a().b().a(this.f2670a.B.b());
            if (this.f2670a.B.b()) {
                Toast.makeText(this.f2670a.getContext(), (int) R.string.hide_installed_apps_in_list, 4).show();
            } else if (this.f2670a.G) {
                Toast.makeText(this.f2670a.getContext(), (int) R.string.hide_installed_apps_in_list_cancel_rank, 4).show();
            } else {
                Toast.makeText(this.f2670a.getContext(), (int) R.string.hide_installed_apps_in_list_cancel, 4).show();
            }
            this.f2670a.u.c();
            if (this.f2670a.u.getCount() > 0 && this.f2670a.f != null && !this.f2670a.f.g()) {
                this.f2670a.b_();
            }
        }
    }

    public STInfoV2 getStInfo() {
        STInfoV2 sTInfoV2 = new STInfoV2(this.f2670a.y.G(), "08", this.f2670a.y.G(), "08", 200);
        if (!this.f2670a.G || !(this.f2670a.y instanceof n)) {
            sTInfoV2.slotId = com.tencent.assistantv2.st.page.a.a("08", 0);
        } else {
            sTInfoV2.slotId = com.tencent.assistantv2.st.page.a.a(((n) this.f2670a.y).M(), 0);
        }
        if (!a.a().b().c()) {
            sTInfoV2.status = "01";
        } else {
            sTInfoV2.status = "02";
        }
        sTInfoV2.actionId = 200;
        return sTInfoV2;
    }
}
