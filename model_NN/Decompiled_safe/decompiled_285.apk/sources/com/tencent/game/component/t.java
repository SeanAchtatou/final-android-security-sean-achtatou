package com.tencent.game.component;

import android.content.pm.PackageManager;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.protocol.jce.SimpleAppInfo;

/* compiled from: ProGuard */
class t implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SimpleAppInfo f2672a;
    final /* synthetic */ GameSquareAppItem b;

    t(GameSquareAppItem gameSquareAppItem, SimpleAppInfo simpleAppInfo) {
        this.b = gameSquareAppItem;
        this.f2672a = simpleAppInfo;
    }

    public void run() {
        try {
            this.b.d.setBackgroundDrawable(this.b.b.getPackageManager().getApplicationIcon(this.f2672a.f));
        } catch (PackageManager.NameNotFoundException e) {
            this.b.d.updateImageView(this.f2672a.c, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
        }
    }
}
