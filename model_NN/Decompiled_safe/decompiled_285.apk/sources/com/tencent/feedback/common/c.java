package com.tencent.feedback.common;

/* compiled from: ProGuard */
public abstract class c {

    /* renamed from: a  reason: collision with root package name */
    public static boolean f2542a = true;
    private static c b;

    public abstract boolean a(int i, Runnable runnable, long j, long j2);

    public abstract boolean a(int i, boolean z);

    public abstract boolean a(Runnable runnable);

    public abstract boolean a(Runnable runnable, long j);

    public static synchronized c a() {
        c cVar;
        synchronized (c.class) {
            if (b == null) {
                b = new d();
            }
            cVar = b;
        }
        return cVar;
    }
}
