package com.tencent.feedback.eup.jni;

import android.content.Context;
import com.tencent.feedback.common.f;
import com.tencent.feedback.common.g;
import com.tencent.feedback.proguard.ac;
import com.tencent.feedback.proguard.aj;
import com.tencent.feedback.proguard.al;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public final class a implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private Context f2559a;
    private String b;
    private List<File> c;

    public a(Context context, String str, List<File> list) {
        this.f2559a = context;
        this.b = str;
        this.c = list;
    }

    public final void run() {
        boolean z;
        File[] listFiles;
        ArrayList<File> arrayList = new ArrayList<>();
        if (this.c != null && this.c.size() > 0) {
            arrayList.addAll(this.c);
        }
        File file = new File(this.b);
        if (file.exists() && file.isDirectory() && (listFiles = file.listFiles(new e(this))) != null) {
            for (File file2 : listFiles) {
                if (!arrayList.contains(file2)) {
                    arrayList.add(file2);
                }
            }
        }
        List<al> a2 = aj.a(this.f2559a, null, 1, -1);
        ArrayList arrayList2 = new ArrayList();
        for (File file3 : arrayList) {
            if (a2 != null) {
                Iterator<al> it = a2.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    }
                    al next = it.next();
                    if (file3.getAbsolutePath().equals(next.a()) && file3.lastModified() == next.b() && file3.length() == next.c() && next.d() != null) {
                        it.remove();
                        arrayList2.add(next);
                        g.b("rqdp{  BufFB existed n:}%s ,ar:%s, md:%s ,ut:%d", next.a(), next.f(), next.d(), Long.valueOf(file3.lastModified()));
                        z = true;
                        break;
                    }
                }
            }
            z = false;
            if (!z) {
                long currentTimeMillis = System.currentTimeMillis();
                String a3 = ac.a(file3.getAbsolutePath());
                long currentTimeMillis2 = System.currentTimeMillis() - currentTimeMillis;
                if (a3 != null) {
                    al alVar = new al();
                    alVar.a(1);
                    alVar.a(file3.getAbsolutePath());
                    StringBuilder sb = new StringBuilder();
                    f.a(this.f2559a);
                    alVar.c(sb.append(f.d()).toString());
                    alVar.c(file3.length());
                    alVar.b(file3.lastModified());
                    alVar.b(a3);
                    g.b("rqdp{  BufFB new }n:%s , ar:%s , md:%s , cs:%d", alVar.a(), alVar.f(), alVar.d(), Long.valueOf(currentTimeMillis2));
                    arrayList2.add(alVar);
                } else {
                    g.b("rqdp{  Error BufFB md fail! pth:}%s , rqdp{  cs:}%d", file3.getAbsolutePath(), Long.valueOf(currentTimeMillis2));
                }
            }
        }
        g.b("rqdp{  LBFTask del n: }%d", Integer.valueOf(aj.a(this.f2559a, 1)));
        if (arrayList2.size() > 0) {
            g.b("rqdp{  LBFTask ins n: }%d", Integer.valueOf(aj.c(this.f2559a, arrayList2)));
        }
    }
}
