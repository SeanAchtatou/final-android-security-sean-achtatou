package com.tencent.feedback.eup.jni;

import android.content.Context;
import com.tencent.feedback.common.g;
import com.tencent.feedback.eup.e;
import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/* compiled from: ProGuard */
public class c {

    /* renamed from: a  reason: collision with root package name */
    public byte f2561a;
    public int b;

    private static Map<String, Integer> b(String str) {
        if (str == null) {
            return null;
        }
        try {
            HashMap hashMap = new HashMap();
            for (String str2 : str.split(",")) {
                String[] split = str2.split(":");
                if (split.length != 2) {
                    g.d("error format at %s", str2);
                    return null;
                }
                hashMap.put(split[0], Integer.valueOf(Integer.parseInt(split[1])));
            }
            return hashMap;
        } catch (Exception e) {
            g.d("error format intStateStr %s", str);
            if (g.a(e)) {
                return null;
            }
            e.printStackTrace();
            return null;
        }
    }

    private static e a(Context context, Map<String, String> map) {
        String str;
        String str2;
        String str3;
        String str4;
        if (map == null) {
            return null;
        }
        if (com.tencent.feedback.common.e.a(context) == null) {
            g.d("abnormal com info not created", new Object[0]);
            return null;
        }
        String str5 = map.get("intStateStr");
        if (str5 == null || str5.trim().length() <= 0) {
            g.d("no intStateStr", new Object[0]);
            return null;
        }
        Map<String, Integer> b2 = b(str5);
        if (b2 == null) {
            g.d("parse intSateMap fail", Integer.valueOf(map.size()));
            return null;
        }
        try {
            b2.get("ep").intValue();
            b2.get("et").intValue();
            b2.get("sino").intValue();
            int intValue = b2.get("sico").intValue();
            int intValue2 = b2.get("spd").intValue();
            b2.get("sud").intValue();
            long intValue3 = (long) b2.get("ets").intValue();
            long intValue4 = (long) b2.get("etms").intValue();
            String str6 = map.get("soVersion");
            if (str6 == null) {
                g.d("error format at version", new Object[0]);
                return null;
            }
            String str7 = map.get("errorAddr");
            String str8 = str7 == null ? "unknown2" : str7;
            String str9 = map.get("codeMsg");
            if (str9 == null) {
                str = "unknown2";
            } else {
                str = str9;
            }
            String str10 = map.get("tombPath");
            if (str10 == null) {
                str2 = "unknown2";
            } else {
                str2 = str10;
            }
            String str11 = map.get("signalName");
            if (str11 == null) {
                str3 = "unknown2";
            } else {
                str3 = str11;
            }
            String str12 = map.get("errnoMsg");
            String str13 = map.get("stack");
            if (str13 == null) {
                str4 = "unknown2";
            } else {
                str4 = str13;
            }
            String str14 = map.get("jstack");
            if (str14 != null) {
                str4 = str4 + "java:\n" + str14;
            }
            e a2 = b.a(context, (intValue3 * 1000) + (intValue4 / 1000), str3, str8, str4, str2, intValue, str, -intValue2, str12, null, null, str6);
            if (a2 == null) {
                return a2;
            }
            a2.c(true);
            return a2;
        } catch (Throwable th) {
            g.d("error format", new Object[0]);
            if (!g.a(th)) {
                th.printStackTrace();
            }
            return null;
        }
    }

    private static String a(InputStream inputStream) {
        if (inputStream == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        while (true) {
            int read = inputStream.read();
            if (read == -1) {
                return null;
            }
            if (read == 0) {
                return sb.toString();
            }
            sb.append((char) read);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:58:0x00a9 A[SYNTHETIC, Splitter:B:58:0x00a9] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.tencent.feedback.eup.e a(android.content.Context r6, java.lang.String r7) {
        /*
            r0 = 0
            java.io.File r1 = new java.io.File
            java.lang.String r2 = "rqd_record.eup"
            r1.<init>(r7, r2)
            boolean r2 = r1.exists()
            if (r2 == 0) goto L_0x0014
            boolean r2 = r1.canRead()
            if (r2 != 0) goto L_0x0015
        L_0x0014:
            return r0
        L_0x0015:
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ IOException -> 0x0086, all -> 0x00a4 }
            r2.<init>(r1)     // Catch:{ IOException -> 0x0086, all -> 0x00a4 }
            java.lang.String r1 = a(r2)     // Catch:{ IOException -> 0x00ba }
            if (r1 == 0) goto L_0x0028
            java.lang.String r3 = "NATIVE_RQD_REPORT"
            boolean r3 = r1.equals(r3)     // Catch:{ IOException -> 0x00ba }
            if (r3 != 0) goto L_0x0042
        L_0x0028:
            java.lang.String r3 = "record read fail! %s"
            r4 = 1
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ IOException -> 0x00ba }
            r5 = 0
            r4[r5] = r1     // Catch:{ IOException -> 0x00ba }
            com.tencent.feedback.common.g.d(r3, r4)     // Catch:{ IOException -> 0x00ba }
            r2.close()     // Catch:{ IOException -> 0x0037 }
            goto L_0x0014
        L_0x0037:
            r1 = move-exception
            boolean r2 = com.tencent.feedback.common.g.a(r1)
            if (r2 != 0) goto L_0x0014
            r1.printStackTrace()
            goto L_0x0014
        L_0x0042:
            java.util.HashMap r4 = new java.util.HashMap     // Catch:{ IOException -> 0x00ba }
            r4.<init>()     // Catch:{ IOException -> 0x00ba }
            r1 = r0
        L_0x0048:
            java.lang.String r3 = a(r2)     // Catch:{ IOException -> 0x00ba }
            if (r3 == 0) goto L_0x0057
            if (r1 != 0) goto L_0x0052
            r1 = r3
            goto L_0x0048
        L_0x0052:
            r4.put(r1, r3)     // Catch:{ IOException -> 0x00ba }
            r1 = r0
            goto L_0x0048
        L_0x0057:
            if (r1 == 0) goto L_0x0073
            java.lang.String r3 = "record not pair! drop! %s"
            r4 = 1
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ IOException -> 0x00ba }
            r5 = 0
            r4[r5] = r1     // Catch:{ IOException -> 0x00ba }
            com.tencent.feedback.common.g.d(r3, r4)     // Catch:{ IOException -> 0x00ba }
            r2.close()     // Catch:{ IOException -> 0x0068 }
            goto L_0x0014
        L_0x0068:
            r1 = move-exception
            boolean r2 = com.tencent.feedback.common.g.a(r1)
            if (r2 != 0) goto L_0x0014
            r1.printStackTrace()
            goto L_0x0014
        L_0x0073:
            com.tencent.feedback.eup.e r0 = a(r6, r4)     // Catch:{ IOException -> 0x00ba }
            r2.close()     // Catch:{ IOException -> 0x007b }
            goto L_0x0014
        L_0x007b:
            r1 = move-exception
            boolean r2 = com.tencent.feedback.common.g.a(r1)
            if (r2 != 0) goto L_0x0014
            r1.printStackTrace()
            goto L_0x0014
        L_0x0086:
            r1 = move-exception
            r2 = r0
        L_0x0088:
            boolean r3 = com.tencent.feedback.common.g.a(r1)     // Catch:{ all -> 0x00b8 }
            if (r3 != 0) goto L_0x0091
            r1.printStackTrace()     // Catch:{ all -> 0x00b8 }
        L_0x0091:
            if (r2 == 0) goto L_0x0014
            r2.close()     // Catch:{ IOException -> 0x0098 }
            goto L_0x0014
        L_0x0098:
            r1 = move-exception
            boolean r2 = com.tencent.feedback.common.g.a(r1)
            if (r2 != 0) goto L_0x0014
            r1.printStackTrace()
            goto L_0x0014
        L_0x00a4:
            r1 = move-exception
            r2 = r0
            r0 = r1
        L_0x00a7:
            if (r2 == 0) goto L_0x00ac
            r2.close()     // Catch:{ IOException -> 0x00ad }
        L_0x00ac:
            throw r0
        L_0x00ad:
            r1 = move-exception
            boolean r2 = com.tencent.feedback.common.g.a(r1)
            if (r2 != 0) goto L_0x00ac
            r1.printStackTrace()
            goto L_0x00ac
        L_0x00b8:
            r0 = move-exception
            goto L_0x00a7
        L_0x00ba:
            r1 = move-exception
            goto L_0x0088
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.feedback.eup.jni.c.a(android.content.Context, java.lang.String):com.tencent.feedback.eup.e");
    }

    public static void a(String str) {
        File file = new File(str, "rqd_record.eup");
        if (file.exists() && file.canWrite()) {
            file.delete();
            g.b("delete record file %s", file.getAbsoluteFile());
        }
    }
}
