package com.tencent.assistant.smartcard.c;

import android.util.SparseArray;
import com.tencent.assistant.smartcard.a.a;
import com.tencent.cloud.smartcard.a.c;
import com.tencent.cloud.smartcard.a.d;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: ProGuard */
public class b {

    /* renamed from: a  reason: collision with root package name */
    private static Map<Integer, c> f1680a = new ConcurrentHashMap(5);

    static {
        f1680a.put(34, new c());
        f1680a.put(36, new com.tencent.assistant.smartcard.a.b());
        f1680a.put(38, new com.tencent.assistant.smartcard.a.c());
        f1680a.put(37, new com.tencent.cloud.smartcard.a.b());
        f1680a.put(42, new a());
        f1680a.put(39, new com.tencent.game.smartcard.a.a());
        f1680a.put(40, new d());
        f1680a.put(41, new com.tencent.cloud.smartcard.a.a());
    }

    public static c a(int i) {
        return f1680a.get(Integer.valueOf(i));
    }

    public static void a(SparseArray<z> sparseArray) {
        if (f1680a != null && f1680a.size() > 0) {
            for (Map.Entry next : f1680a.entrySet()) {
                sparseArray.put(((Integer) next.getKey()).intValue(), ((c) next.getValue()).d());
            }
        }
    }
}
