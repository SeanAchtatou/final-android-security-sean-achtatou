package com.tencent.assistant.smartcard.c;

import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.smartcard.d.n;
import com.tencent.assistant.smartcard.d.y;
import com.tencent.assistant.smartcard.d.z;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class ac extends z {
    public boolean a(n nVar, List<Long> list) {
        if (nVar == null || !(nVar instanceof z)) {
            return false;
        }
        return a((z) nVar, list);
    }

    private boolean a(z zVar, List<Long> list) {
        if (zVar.c() == 0) {
            return false;
        }
        zVar.a(list);
        if (!((Boolean) b(zVar).first).booleanValue()) {
            return false;
        }
        a(zVar);
        if (zVar.a() == null || zVar.a().size() == 0) {
            return false;
        }
        if ((zVar.c() == 2 || zVar.c() == 5) && zVar.a().size() < 2) {
            return false;
        }
        if (((zVar.j == 14 || zVar.j == 15) && zVar.c() == 1 && zVar.a().size() < 3) || (zVar.j == 27 && zVar.c() == 1 && zVar.a().size() < 3)) {
            return false;
        }
        if (zVar.j == 11 && zVar.c() == 6 && zVar.a().size() < 3) {
            return false;
        }
        return true;
    }

    private void a(z zVar) {
        LocalApkInfo installedApkInfo;
        if (zVar != null && zVar.a() != null && zVar.a().size() != 0) {
            ArrayList arrayList = new ArrayList();
            for (y next : zVar.f1769a) {
                if (!(next.f1768a == null || (installedApkInfo = ApkResourceManager.getInstance().getInstalledApkInfo(next.f1768a.c)) == null || installedApkInfo.mVersionCode < next.f1768a.g)) {
                    arrayList.add(next);
                }
            }
            zVar.f1769a.removeAll(arrayList);
        }
    }
}
