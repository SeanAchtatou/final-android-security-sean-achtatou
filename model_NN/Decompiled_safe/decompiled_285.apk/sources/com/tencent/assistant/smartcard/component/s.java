package com.tencent.assistant.smartcard.component;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.smartcard.d.p;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class s extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ p f1736a;
    final /* synthetic */ NormalSmartcardPersonalizedItem b;

    s(NormalSmartcardPersonalizedItem normalSmartcardPersonalizedItem, p pVar) {
        this.b = normalSmartcardPersonalizedItem;
        this.f1736a = pVar;
    }

    public void onTMAClick(View view) {
        NormalSmartcardPersonalizedItem.a(this.b, this.f1736a.d);
        int unused = this.b.q = this.b.q % this.f1736a.c.size();
        this.b.b();
    }

    public STInfoV2 getStInfo() {
        return this.b.a("05_001", 200);
    }
}
