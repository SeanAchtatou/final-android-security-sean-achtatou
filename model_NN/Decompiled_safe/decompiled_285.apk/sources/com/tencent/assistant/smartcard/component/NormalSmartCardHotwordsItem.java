package com.tencent.assistant.smartcard.component;

import android.content.Context;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.smartcard.d.l;
import com.tencent.assistant.smartcard.d.n;
import com.tencent.pangu.component.HotwordsCardView;

/* compiled from: ProGuard */
public class NormalSmartCardHotwordsItem extends NormalSmartcardBaseItem {
    private TextView i;
    private HotwordsCardView l;

    public NormalSmartCardHotwordsItem(Context context, n nVar, as asVar, IViewInvalidater iViewInvalidater) {
        super(context, nVar, asVar, iViewInvalidater);
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.b.inflate((int) R.layout.smartcard_hotwords, this);
        this.i = (TextView) findViewById(R.id.title);
        this.l = (HotwordsCardView) findViewById(R.id.hotwords_view);
        f();
    }

    private void f() {
        if (this.d instanceof l) {
            l lVar = (l) this.d;
            this.i.setText(lVar.l);
            this.l.a(c(a(this.f1693a)));
            this.l.a(2, this.f1693a.getResources().getString(R.string.hotwords_card_more_text), lVar.o, lVar.a());
        }
    }

    /* access modifiers changed from: protected */
    public void b() {
        f();
    }
}
