package com.tencent.assistant.smartcard.d;

import com.tencent.assistant.protocol.jce.HotWordItem;
import com.tencent.assistant.protocol.jce.SmartCardHotWords;
import java.util.List;

/* compiled from: ProGuard */
public class l extends n {

    /* renamed from: a  reason: collision with root package name */
    public List<HotWordItem> f1757a;
    private boolean b = false;

    public boolean a(SmartCardHotWords smartCardHotWords, byte b2) {
        this.j = b2;
        if (smartCardHotWords == null) {
            return false;
        }
        this.l = smartCardHotWords.f1508a;
        this.o = smartCardHotWords.c;
        this.f1757a = smartCardHotWords.b;
        return true;
    }

    public List<HotWordItem> a() {
        return this.f1757a;
    }

    public int b() {
        if (this.f1757a != null) {
            return this.f1757a.size();
        }
        return 0;
    }
}
