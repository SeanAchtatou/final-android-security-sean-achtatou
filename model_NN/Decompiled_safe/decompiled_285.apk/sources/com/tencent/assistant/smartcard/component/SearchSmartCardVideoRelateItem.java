package com.tencent.assistant.smartcard.component;

import android.content.Context;
import android.graphics.Canvas;
import android.text.Html;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.smartcard.d.ac;
import com.tencent.assistant.smartcard.d.n;
import com.tencent.assistantv2.adapter.smartlist.z;
import com.tencent.assistantv2.st.page.a;
import com.tencent.assistantv2.st.page.d;
import com.tencent.cloud.model.SimpleVideoModel;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class SearchSmartCardVideoRelateItem extends SearchSmartCardBaseItem {
    private View l;
    private TextView m;
    private TextView n;
    private LinearLayout o;
    /* access modifiers changed from: private */
    public z p;
    private final String q = Constants.VIA_REPORT_TYPE_SHARE_TO_QQ;

    public SearchSmartCardVideoRelateItem(Context context) {
        super(context);
    }

    public SearchSmartCardVideoRelateItem(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public SearchSmartCardVideoRelateItem(Context context, n nVar, as asVar) {
        super(context, nVar, asVar);
    }

    public SearchSmartCardVideoRelateItem(Context context, n nVar, as asVar, z zVar) {
        super(context, nVar, asVar);
        this.p = zVar;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!this.f) {
            this.f = true;
            h();
        }
    }

    /* access modifiers changed from: protected */
    public String c() {
        return a.a(Constants.VIA_REPORT_TYPE_SHARE_TO_QQ, this.p == null ? 0 : this.p.a());
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.c = this.b.inflate((int) R.layout.smartcard_video_relate, this);
        this.l = findViewById(R.id.title_ly);
        this.m = (TextView) findViewById(R.id.title);
        this.n = (TextView) findViewById(R.id.more);
        this.o = (LinearLayout) findViewById(R.id.list);
        i();
    }

    /* access modifiers changed from: protected */
    public void b() {
        i();
    }

    private void i() {
        this.o.removeAllViews();
        ac acVar = (ac) this.d;
        if (acVar == null || acVar.b <= 0 || acVar.f1747a == null || acVar.f1747a.size() == 0 || acVar.f1747a.size() < acVar.b) {
            a(8);
            setBackgroundResource(17170445);
            setPadding(0, 0, 0, 0);
            return;
        }
        setBackgroundResource(R.drawable.common_card_normal);
        a(0);
        ArrayList arrayList = new ArrayList(acVar.f1747a);
        this.m.setText(Html.fromHtml(acVar.l));
        this.n.setOnClickListener(new ag(this, getContext(), acVar));
        this.o.addView(a(arrayList.subList(0, arrayList.size() > acVar.b ? acVar.b : arrayList.size())));
    }

    public void a(int i) {
        this.c.setVisibility(i);
        this.l.setVisibility(i);
        this.o.setVisibility(i);
    }

    private View a(List<SimpleVideoModel> list) {
        int i = 0;
        LinearLayout linearLayout = new LinearLayout(this.f1693a);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        linearLayout.setOrientation(0);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -1);
        layoutParams.weight = 1.0f;
        while (true) {
            int i2 = i;
            if (i2 >= list.size()) {
                return linearLayout;
            }
            SimpleVideoModel simpleVideoModel = list.get(i2);
            View inflate = this.b.inflate((int) R.layout.smartcard_video_relate_item, (ViewGroup) null);
            inflate.setOnClickListener(new af(this, getContext(), i2, simpleVideoModel));
            ((TXImageView) inflate.findViewById(R.id.icon)).updateImageView(simpleVideoModel.c, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            ((TextView) inflate.findViewById(R.id.tag)).setText(simpleVideoModel.i);
            ((TextView) inflate.findViewById(R.id.name)).setText(simpleVideoModel.b);
            linearLayout.addView(inflate, layoutParams);
            i = i2 + 1;
        }
    }

    /* access modifiers changed from: protected */
    public int d() {
        return d.d;
    }

    /* access modifiers changed from: protected */
    public String e() {
        if (this.p == null) {
            return null;
        }
        return this.p.d();
    }

    /* access modifiers changed from: protected */
    public long f() {
        if (this.p == null) {
            return 0;
        }
        return this.p.c();
    }

    /* access modifiers changed from: protected */
    public int g() {
        if (this.p == null) {
            return 2000;
        }
        return this.p.b();
    }
}
