package com.tencent.assistant.smartcard.component;

import android.content.Context;
import android.graphics.Canvas;
import android.text.Html;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.d;
import com.tencent.assistant.smartcard.d.ad;
import com.tencent.assistant.smartcard.d.n;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.at;
import com.tencent.assistant.utils.bm;
import com.tencent.assistantv2.adapter.smartlist.z;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.pangu.component.appdetail.TXDwonloadProcessBar;
import com.tencent.pangu.component.appdetail.process.s;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class SearchSmartCardYuyiTagItem extends SearchSmartCardBaseItem {
    private final String l = "SearchSmartCardYuyiTagItem";
    private View m;
    private TextView n;
    private TextView o;
    private LinearLayout p;
    private z q;
    private final String r = "18";

    public SearchSmartCardYuyiTagItem(Context context) {
        super(context);
    }

    public SearchSmartCardYuyiTagItem(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public SearchSmartCardYuyiTagItem(Context context, n nVar, as asVar) {
        super(context, nVar, asVar);
    }

    public SearchSmartCardYuyiTagItem(Context context, n nVar, as asVar, z zVar) {
        super(context, nVar, asVar);
        this.q = zVar;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!this.f) {
            this.f = true;
            h();
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.c = this.b.inflate((int) R.layout.smartcard_yuyi_tag, this);
        this.m = findViewById(R.id.title_ly);
        this.n = (TextView) findViewById(R.id.title);
        this.o = (TextView) findViewById(R.id.more);
        this.p = (LinearLayout) findViewById(R.id.app_list);
        i();
    }

    /* access modifiers changed from: protected */
    public void b() {
        i();
    }

    private void i() {
        this.p.removeAllViews();
        ad adVar = (ad) this.d;
        if (adVar == null || adVar.f1748a <= 0 || adVar.b == null || adVar.b.size() == 0 || adVar.b.size() < adVar.f1748a) {
            a(8);
            setBackgroundResource(17170445);
            setPadding(0, 0, 0, 0);
            return;
        }
        setBackgroundResource(R.drawable.bg_card_selector_padding);
        a(0);
        ArrayList arrayList = new ArrayList(adVar.b);
        this.n.setText(Html.fromHtml(adVar.l));
        this.o.setOnClickListener(new aj(this, getContext(), adVar));
        this.p.addView(a(arrayList.subList(0, arrayList.size() > adVar.f1748a ? adVar.f1748a : arrayList.size())));
    }

    public void a(int i) {
        this.c.setVisibility(i);
        this.m.setVisibility(i);
        this.p.setVisibility(i);
    }

    private View a(List<SimpleAppModel> list) {
        LinearLayout linearLayout = new LinearLayout(this.f1693a);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        linearLayout.setOrientation(0);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -1);
        layoutParams.weight = 1.0f;
        for (int i = 0; i < list.size(); i++) {
            View inflate = this.b.inflate((int) R.layout.smartcard_app_item, (ViewGroup) null);
            SimpleAppModel simpleAppModel = list.get(i);
            ((TXImageView) inflate.findViewById(R.id.icon)).updateImageView(simpleAppModel.e, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            ((TextView) inflate.findViewById(R.id.name)).setText(simpleAppModel.d);
            TextView textView = (TextView) inflate.findViewById(R.id.size);
            textView.setText(at.a(simpleAppModel.k));
            TXDwonloadProcessBar tXDwonloadProcessBar = (TXDwonloadProcessBar) inflate.findViewById(R.id.progress);
            tXDwonloadProcessBar.a(simpleAppModel, new View[]{textView});
            DownloadButton downloadButton = (DownloadButton) inflate.findViewById(R.id.btn);
            downloadButton.a(simpleAppModel);
            downloadButton.setTag(R.id.tma_st_smartcard_tag, STConst.ST_PAGE_TYPE_SMARTCARD);
            if (s.a(simpleAppModel)) {
                downloadButton.setClickable(false);
            } else {
                downloadButton.setClickable(true);
                STInfoV2 a2 = a(d(i), 200);
                if (a2 != null) {
                    a2.scene = b(0);
                    a2.searchId = this.i;
                    a2.extraData = c(0);
                    a2.updateWithSimpleAppModel(simpleAppModel);
                }
                downloadButton.a(a2, new ah(this), (d) null, downloadButton, tXDwonloadProcessBar);
            }
            inflate.setTag(R.id.tma_st_smartcard_tag, STConst.ST_PAGE_TYPE_SMARTCARD);
            inflate.setOnClickListener(new ai(this, simpleAppModel, i));
            linearLayout.addView(inflate, layoutParams);
        }
        return linearLayout;
    }

    /* access modifiers changed from: private */
    public String d(int i) {
        return c() + bm.a(i + 1);
    }

    /* access modifiers changed from: protected */
    public String c() {
        return a.a("18", this.q == null ? 0 : this.q.a());
    }

    /* access modifiers changed from: protected */
    public int d() {
        return com.tencent.assistantv2.st.page.d.f2062a;
    }

    /* access modifiers changed from: protected */
    public String e() {
        if (this.q == null) {
            return null;
        }
        return this.q.d();
    }

    /* access modifiers changed from: protected */
    public long f() {
        if (this.q == null) {
            return 0;
        }
        return this.q.c();
    }

    /* access modifiers changed from: protected */
    public int g() {
        if (this.q == null) {
            return 2000;
        }
        return this.q.b();
    }
}
