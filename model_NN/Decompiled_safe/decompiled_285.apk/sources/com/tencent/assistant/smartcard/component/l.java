package com.tencent.assistant.smartcard.component;

import android.widget.ImageView;
import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistantv2.component.k;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.download.a;

/* compiled from: ProGuard */
class l extends k {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ NormalSmartcardAppListItem f1729a;

    l(NormalSmartcardAppListItem normalSmartcardAppListItem) {
        this.f1729a = normalSmartcardAppListItem;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.smartcard.component.NormalSmartcardAppListItem.a(com.tencent.assistant.smartcard.component.NormalSmartcardAppListItem, boolean):boolean
     arg types: [com.tencent.assistant.smartcard.component.NormalSmartcardAppListItem, int]
     candidates:
      com.tencent.assistant.smartcard.component.NormalSmartcardBaseItem.a(java.lang.String, int):com.tencent.assistantv2.st.page.STInfoV2
      com.tencent.assistant.smartcard.component.NormalSmartcardBaseItem.a(com.tencent.assistant.model.SimpleAppModel, com.tencent.assistantv2.st.page.STInfoV2):void
      com.tencent.assistant.smartcard.component.NormalSmartcardAppListItem.a(com.tencent.assistant.smartcard.component.NormalSmartcardAppListItem, boolean):boolean */
    public void a(DownloadInfo downloadInfo) {
        a.a().a(downloadInfo);
        com.tencent.assistant.utils.a.a((ImageView) this.f1729a.findViewWithTag(downloadInfo.downloadTicket));
        if (this.f1729a.d.j == 2 && !this.f1729a.r) {
            boolean unused = this.f1729a.r = true;
            Toast.makeText(this.f1729a.f1693a, this.f1729a.f1693a.getResources().getString(R.string.smartcard_installed_delete_app_toast), 0).show();
        }
    }
}
