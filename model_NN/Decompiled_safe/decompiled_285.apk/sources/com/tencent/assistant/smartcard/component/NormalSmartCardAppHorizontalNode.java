package com.tencent.assistant.smartcard.component;

import android.content.Context;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.component.appdetail.TXDwonloadProcessBar;

/* compiled from: ProGuard */
public class NormalSmartCardAppHorizontalNode extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    TXImageView f1694a;
    TextView b;
    TextView c;
    DownloadButton d;
    TXDwonloadProcessBar e;

    public NormalSmartCardAppHorizontalNode(Context context) {
        this(context, null);
    }

    public NormalSmartCardAppHorizontalNode(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a();
    }

    private void a() {
        LayoutInflater.from(getContext()).inflate((int) R.layout.smartcard_horizontal_list_app_node, this);
        this.f1694a = (TXImageView) findViewById(R.id.icon);
        this.b = (TextView) findViewById(R.id.name);
        this.c = (TextView) findViewById(R.id.down_times);
        this.e = (TXDwonloadProcessBar) findViewById(R.id.progress);
        this.d = (DownloadButton) findViewById(R.id.btn);
        setOrientation(1);
        setGravity(1);
    }

    public void a(SimpleAppModel simpleAppModel, Spanned spanned, STInfoV2 sTInfoV2, int i) {
        this.f1694a.updateImageView(simpleAppModel.e, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
        this.b.setText(simpleAppModel.d);
        if (!TextUtils.isEmpty(spanned)) {
            this.c.setText(spanned);
        } else {
            simpleAppModel.t();
            if (!TextUtils.isEmpty(simpleAppModel.aK.f946a)) {
                this.c.setText(simpleAppModel.aK.f946a);
            }
        }
        this.e.a(simpleAppModel, new View[]{this.c});
        this.d.a(simpleAppModel);
        this.d.a(sTInfoV2);
        this.f1694a.setTag(simpleAppModel.q());
        setOnClickListener(new a(this, i, simpleAppModel, sTInfoV2));
    }
}
