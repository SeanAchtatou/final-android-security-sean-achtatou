package com.tencent.assistant.smartcard.d;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.k;
import com.tencent.assistant.protocol.jce.SmartCardPicDownloadNode;
import com.tencent.assistant.protocol.jce.SmartCardPicTemplate;
import com.tencent.assistant.protocol.jce.SmartCardTitle;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class m extends a {
    public List<y> e;
    private byte f;
    private boolean g = false;
    private int h;
    private int i;
    private boolean v = true;
    private int w = 0;
    private String x;

    public boolean a(byte b, JceStruct jceStruct) {
        if (!(jceStruct instanceof SmartCardPicTemplate)) {
            return false;
        }
        SmartCardPicTemplate smartCardPicTemplate = (SmartCardPicTemplate) jceStruct;
        SmartCardTitle smartCardTitle = smartCardPicTemplate.b;
        this.j = b;
        if (smartCardTitle != null) {
            this.f = smartCardTitle.f1527a;
            this.l = smartCardTitle.b;
            this.m = smartCardTitle.e;
            this.p = smartCardTitle.c;
            this.o = smartCardTitle.d;
        }
        this.n = smartCardPicTemplate.c;
        this.k = smartCardPicTemplate.f1514a;
        if (this.e == null) {
            this.e = new ArrayList();
        } else {
            this.e.clear();
        }
        if (smartCardPicTemplate.e != null) {
            Iterator<SmartCardPicDownloadNode> it = smartCardPicTemplate.e.iterator();
            while (it.hasNext()) {
                SmartCardPicDownloadNode next = it.next();
                y yVar = new y();
                yVar.b = next.b;
                yVar.f1768a = k.a(next.f1512a);
                if (yVar.f1768a != null) {
                    k.a(yVar.f1768a);
                }
                this.e.add(yVar);
            }
        }
        this.g = smartCardPicTemplate.a();
        this.h = smartCardPicTemplate.c();
        this.i = smartCardPicTemplate.d();
        this.v = !smartCardPicTemplate.k;
        this.x = smartCardPicTemplate.f;
        this.w = smartCardPicTemplate.e();
        return true;
    }

    public x c() {
        if (this.h <= 0 || this.i <= 0) {
            return null;
        }
        x xVar = new x();
        xVar.f = this.k;
        xVar.e = this.j;
        xVar.f1767a = this.h;
        xVar.b = this.i;
        return xVar;
    }

    public List<SimpleAppModel> a() {
        if (this.e == null || this.e.size() <= 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList(this.e.size());
        for (y yVar : this.e) {
            arrayList.add(yVar.f1768a);
        }
        return arrayList;
    }

    public String d_() {
        return j() + "_" + (this.e == null ? 0 : this.e.size());
    }

    public void b() {
        if (this.v && this.e != null && this.e.size() > 0) {
            Iterator<y> it = this.e.iterator();
            while (it.hasNext()) {
                y next = it.next();
                if (!(next.f1768a == null || ApkResourceManager.getInstance().getInstalledApkInfo(next.f1768a.c) == null)) {
                    it.remove();
                }
            }
        }
    }

    public byte f() {
        return this.f;
    }

    public boolean h() {
        return this.g;
    }

    public int i() {
        return this.w;
    }

    public String e() {
        return this.x;
    }
}
