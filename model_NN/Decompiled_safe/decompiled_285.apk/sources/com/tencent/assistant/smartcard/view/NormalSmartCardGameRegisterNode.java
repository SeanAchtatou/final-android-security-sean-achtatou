package com.tencent.assistant.smartcard.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.k;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.component.ListItemInfoView;
import com.tencent.assistantv2.component.ListRecommendReasonView;
import com.tencent.assistantv2.st.model.STCommonInfo;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.manager.DownloadProxy;

/* compiled from: ProGuard */
public class NormalSmartCardGameRegisterNode extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    private Context f1770a;
    private TXImageView b;
    private DownloadButton c;
    private TextView d;
    private ListItemInfoView e;
    private ListRecommendReasonView f;
    /* access modifiers changed from: private */
    public SimpleAppModel g;

    public NormalSmartCardGameRegisterNode(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f1770a = context;
        a();
    }

    public NormalSmartCardGameRegisterNode(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f1770a = context;
        a();
    }

    public NormalSmartCardGameRegisterNode(Context context) {
        super(context);
        this.f1770a = context;
        a();
    }

    private void a() {
        LayoutInflater.from(this.f1770a).inflate((int) R.layout.game_smartcard_register_item, this);
        this.b = (TXImageView) findViewById(R.id.app_icon_img);
        this.c = (DownloadButton) findViewById(R.id.state_app_btn);
        this.c.c(true);
        this.d = (TextView) findViewById(R.id.app_name);
        this.e = (ListItemInfoView) findViewById(R.id.download_info);
        this.e.a(ListItemInfoView.InfoType.CATEGORY_NO_SIZE);
        setBackgroundResource(R.drawable.v2_button_background_light_selector);
        this.f = (ListRecommendReasonView) findViewById(R.id.reasonView);
    }

    public void a(SimpleAppModel simpleAppModel, STInfoV2 sTInfoV2, boolean z, int i) {
        if (simpleAppModel != null) {
            this.g = simpleAppModel;
            this.b.updateImageView(this.g.e, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            this.c.a(this.g);
            this.d.setText(this.g.d);
            this.e.a(this.g);
            this.f.a(this.g, (IViewInvalidater) null);
            this.c.setOnClickListener(new a(this, sTInfoV2));
            setOnClickListener(new b(this, i, sTInfoV2, simpleAppModel));
        }
    }

    /* access modifiers changed from: protected */
    public void a(SimpleAppModel simpleAppModel, STCommonInfo sTCommonInfo) {
        if (simpleAppModel != null) {
            DownloadInfo a2 = DownloadProxy.a().a(simpleAppModel);
            StatInfo a3 = a.a(sTCommonInfo);
            if (a2 != null && a2.needReCreateInfo(simpleAppModel)) {
                DownloadProxy.a().b(a2.downloadTicket);
                a2 = null;
            }
            if (a2 == null) {
                a2 = DownloadInfo.createDownloadInfo(simpleAppModel, a3);
            } else {
                a2.updateDownloadInfoStatInfo(a3);
            }
            if (ApkResourceManager.getInstance().hasLocalPack(simpleAppModel.c)) {
                com.tencent.pangu.download.a.a().c(a2);
                return;
            }
            switch (c.f1775a[k.d(simpleAppModel).ordinal()]) {
                case 1:
                    com.tencent.pangu.download.a.a().a(a2);
                    return;
                case 2:
                    com.tencent.pangu.download.a.a().c(a2);
                    return;
                case 3:
                case 4:
                    com.tencent.pangu.download.a.a().b(a2.downloadTicket);
                    return;
                case 5:
                    com.tencent.pangu.download.a.a().b(a2);
                    return;
                case 6:
                    com.tencent.pangu.download.a.a().c(a2);
                    return;
                case 7:
                    com.tencent.pangu.download.a.a().c(a2);
                    return;
                case 8:
                case 9:
                    com.tencent.pangu.download.a.a().a(a2);
                    return;
                case 10:
                    Toast.makeText(this.f1770a, (int) R.string.tips_slicent_install, 0).show();
                    return;
                case 11:
                    Toast.makeText(this.f1770a, (int) R.string.tips_slicent_uninstall, 0).show();
                    return;
                case 12:
                    Toast.makeText(this.f1770a, (int) R.string.unsupported, 0).show();
                    return;
                default:
                    return;
            }
        }
    }
}
