package com.tencent.assistant.smartcard.d;

import com.tencent.assistant.manager.o;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.k;
import com.tencent.assistant.protocol.jce.SmartCardGrabNumber;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class s extends n {

    /* renamed from: a  reason: collision with root package name */
    public String f1762a;
    public SimpleAppModel b;
    public int c;
    public String d;
    public boolean e;
    public int f;
    public int g;
    public int h;
    public String i;
    public boolean v = false;

    public void a(SmartCardGrabNumber smartCardGrabNumber, int i2) {
        b(smartCardGrabNumber, i2);
    }

    private void b(SmartCardGrabNumber smartCardGrabNumber, int i2) {
        this.j = i2;
        if (smartCardGrabNumber != null) {
            this.f1762a = smartCardGrabNumber.f1507a;
            this.b = k.a(smartCardGrabNumber.b);
            this.c = smartCardGrabNumber.c;
            this.d = smartCardGrabNumber.d;
            this.g = smartCardGrabNumber.g;
            this.h = smartCardGrabNumber.h;
            this.e = smartCardGrabNumber.e;
            this.f = smartCardGrabNumber.f;
            this.i = smartCardGrabNumber.i;
            o.a().a(Long.valueOf(this.b.f938a), this.c);
        }
    }

    public List<Long> d() {
        ArrayList arrayList = new ArrayList();
        if (this.b != null) {
            arrayList.add(Long.valueOf(this.b.f938a));
        }
        return arrayList;
    }

    public void a(List<Long> list) {
        if (list != null && list.size() != 0 && this.b != null && list.contains(Long.valueOf(this.b.f938a))) {
            this.v = true;
        }
    }
}
