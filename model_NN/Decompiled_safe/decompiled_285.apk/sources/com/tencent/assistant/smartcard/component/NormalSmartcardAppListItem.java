package com.tencent.assistant.smartcard.component;

import android.content.Context;
import android.graphics.Canvas;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.d;
import com.tencent.assistant.smartcard.d.b;
import com.tencent.assistant.smartcard.d.n;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.at;
import com.tencent.assistant.utils.bm;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.component.appdetail.TXDwonloadProcessBar;
import com.tencent.pangu.component.appdetail.process.s;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class NormalSmartcardAppListItem extends NormalSmartcardBaseItem {
    public ArrayList<SimpleAppModel> i;
    private View l;
    private TextView m;
    private TextView n;
    private LinearLayout o;
    /* access modifiers changed from: private */
    public int p = 2000;
    private String q = Constants.STR_EMPTY;
    /* access modifiers changed from: private */
    public boolean r = false;
    private boolean s = true;

    public NormalSmartcardAppListItem(Context context) {
        super(context);
    }

    public NormalSmartcardAppListItem(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public NormalSmartcardAppListItem(Context context, n nVar, as asVar, IViewInvalidater iViewInvalidater) {
        super(context, nVar, asVar, iViewInvalidater);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!this.f) {
            this.f = true;
            d();
            Iterator<SimpleAppModel> it = this.i.iterator();
            while (it.hasNext()) {
                SimpleAppModel next = it.next();
                long j = -1;
                if (next != null) {
                    j = next.f938a;
                }
                a(STConst.ST_DEFAULT_SLOT, 100, this.d.s, j);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.c = this.b.inflate((int) R.layout.smartcard_app, this);
        this.l = findViewById(R.id.title_ly);
        this.m = (TextView) findViewById(R.id.title);
        this.n = (TextView) findViewById(R.id.show_more);
        this.o = (LinearLayout) findViewById(R.id.app_list);
        f();
    }

    /* access modifiers changed from: protected */
    public void b() {
        f();
    }

    public void a(boolean z) {
        this.s = z;
    }

    private void f() {
        this.o.removeAllViews();
        b bVar = (b) this.d;
        if (bVar == null || bVar.f1749a <= 0 || bVar.c == null || bVar.c.size() == 0 || bVar.c.size() < bVar.f1749a) {
            a(8);
            setBackgroundResource(17170445);
            setPadding(0, 0, 0, 0);
            return;
        }
        setBackgroundResource(R.drawable.bg_card_selector_padding);
        a(0);
        ArrayList arrayList = new ArrayList(bVar.c);
        this.m.setText(bVar.l);
        List subList = arrayList.subList(0, arrayList.size() > bVar.f1749a ? bVar.f1749a : arrayList.size());
        if (this.i == null) {
            this.i = new ArrayList<>();
        }
        this.i.clear();
        this.i.addAll(subList);
        this.o.addView(a(subList));
        if (bVar.b <= bVar.f1749a || TextUtils.isEmpty(bVar.o)) {
            this.n.setVisibility(8);
            return;
        }
        this.n.setText(bVar.p);
        this.n.setOnClickListener(this.k);
        this.n.setVisibility(0);
    }

    public void a(int i2) {
        this.c.setVisibility(i2);
        this.l.setVisibility(i2);
        this.o.setVisibility(i2);
    }

    private View a(List<SimpleAppModel> list) {
        int i2 = 0;
        LinearLayout linearLayout = new LinearLayout(this.f1693a);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        linearLayout.setOrientation(0);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -1);
        layoutParams.weight = 1.0f;
        while (true) {
            int i3 = i2;
            if (i3 >= list.size()) {
                return linearLayout;
            }
            View inflate = this.b.inflate((int) R.layout.smartcard_app_item, (ViewGroup) null);
            a(inflate, list.get(i3), i3);
            linearLayout.addView(inflate, layoutParams);
            i2 = i3 + 1;
        }
    }

    public void b(int i2) {
        this.p = i2;
    }

    public void a(String str) {
        this.q = str;
    }

    private String e(int i2) {
        return (TextUtils.isEmpty(this.q) ? "03_" : this.q.endsWith("_") ? this.q : this.q + "_") + bm.a(i2 + 1);
    }

    private void a(View view, SimpleAppModel simpleAppModel, int i2) {
        TXImageView tXImageView = (TXImageView) view.findViewById(R.id.icon);
        tXImageView.updateImageView(simpleAppModel.e, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
        ((TextView) view.findViewById(R.id.name)).setText(simpleAppModel.d);
        TextView textView = (TextView) view.findViewById(R.id.size);
        textView.setText(at.a(simpleAppModel.k));
        if (!this.s) {
            textView.setVisibility(8);
        }
        TXDwonloadProcessBar tXDwonloadProcessBar = (TXDwonloadProcessBar) view.findViewById(R.id.progress);
        tXDwonloadProcessBar.a(simpleAppModel, new View[]{textView});
        DownloadButton downloadButton = (DownloadButton) view.findViewById(R.id.btn);
        downloadButton.a(simpleAppModel);
        tXImageView.setTag(simpleAppModel.q());
        downloadButton.setTag(R.id.tma_st_smartcard_tag, e());
        STInfoV2 a2 = a(e(i2), 100);
        if (a2 != null) {
            a2.updateWithSimpleAppModel(simpleAppModel);
        }
        a(simpleAppModel, a2);
        if (s.a(simpleAppModel)) {
            downloadButton.setClickable(false);
        } else {
            downloadButton.setClickable(true);
            downloadButton.a(a2, new l(this), (d) null, downloadButton, tXDwonloadProcessBar);
        }
        view.setTag(R.id.tma_st_smartcard_tag, e());
        view.setOnClickListener(new m(this, simpleAppModel, a2));
    }
}
