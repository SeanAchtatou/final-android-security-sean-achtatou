package com.tencent.assistant.plugin;

import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.plugin.mgr.j;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.e;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: ProGuard */
public class PluginInfo {
    public static final String META_DATA_COMMANDHANDLE_SERVICE = "command_handle_service";
    public static final String META_DATA_DEEP_ACC_SERVICE = "deep_acc_service";
    public static final String META_DATA_DOCK_SEC_SERVICE = "dock_sec_service";
    public static final String META_DATA_FREE_WIFI_RECEIVER = "freewifi_receiver";
    public static final String META_DATA_FREE_WIFI_SERVICE = "freewifi_service";
    public static final String META_DATA_QREADER_RECEIVER = "qreader_receiver";
    public static final String META_DATA_SWITCH_PHONE_RESTORE_SERVICE = "switch_phone_restore_service";
    public static final String META_DATA_SWITCH_PHONE_SERVICE = "switch_phone_service";

    /* renamed from: a  reason: collision with root package name */
    private String f1068a = "PluginInfo";
    private int b;
    private String c;
    private String d;
    private int e;
    private int f;
    private int g;
    private int h;
    private int i;
    private String j;
    private ApplicationInfo k;
    private List<PluginEntry> l = new ArrayList();
    private String m;
    private String n;
    private String o;
    private String p;
    private String q;
    private String r;
    private String s;
    private String t;
    private String u;
    private String v;
    private Map<String, String> w;
    private Map<String, String> x;

    public String getDockReceiverImpl() {
        return this.u;
    }

    public void setDockReceiverImpl(String str) {
        this.u = str;
    }

    public String getAccelerationServiceImpl() {
        return this.v;
    }

    public void setAccelerationServiceImpl(String str) {
        this.v = str;
    }

    public String getHeartBeatReceiverImpl() {
        return this.r;
    }

    public void setHeartBeatReceiverImpl(String str) {
        this.r = str;
    }

    public String getAuthorReceiverImpl() {
        return this.q;
    }

    public void setAuthorReceiverImpl(String str) {
        this.q = str;
    }

    public String getSmsSentReceiverImpl() {
        return this.p;
    }

    public void setSmsSentReceiverImpl(String str) {
        this.p = str;
    }

    public String getLaunchApplication() {
        return this.j;
    }

    public void setLaunchApplication(String str) {
        this.j = str;
    }

    public int getVersion() {
        return this.b;
    }

    public void setVersion(int i2) {
        this.b = i2;
    }

    public String getPackageName() {
        return this.c;
    }

    public void setPackageName(String str) {
        this.c = str;
    }

    public String getFileMd5() {
        return this.d;
    }

    public void setFileMd5(String str) {
        this.d = str;
    }

    public int getPkgid() {
        return this.e;
    }

    public void setPkgid(int i2) {
        this.e = i2;
    }

    public int getMinBaoVersion() {
        return this.f;
    }

    public void setMinBaoVersion(int i2) {
        this.f = i2;
    }

    public int getMinApiLevel() {
        return this.g;
    }

    public void setMinApiLevel(int i2) {
        this.g = i2;
    }

    public int getMinFLevel() {
        return this.h;
    }

    public void setMinFLevel(int i2) {
        this.h = i2;
    }

    public int getInProcess() {
        return this.i;
    }

    public void setInProcess(int i2) {
        this.i = i2;
    }

    public String getSmsReceiverImpl() {
        return this.m;
    }

    public void setSmsReceiverImpl(String str) {
        this.m = str;
    }

    public void addEntry(String str, String str2) {
        this.l.add(new PluginEntry(this, str, a(str2), str2));
    }

    public List<PluginEntry> getPluginEntryList() {
        return this.l;
    }

    public String getPluginUniqueKey() {
        return j.d(this.c, this.b);
    }

    public String getPluginApkPath() {
        return j.a(this.c, this.b);
    }

    public synchronized ApplicationInfo getApplicationInfo() {
        PackageInfo packageArchiveInfo;
        if (this.k == null && (packageArchiveInfo = AstApp.i().getPackageManager().getPackageArchiveInfo(getPluginApkPath(), 1)) != null) {
            this.k = packageArchiveInfo.applicationInfo;
        }
        return this.k;
    }

    private Drawable a(String str) {
        PackageManager packageManager = AstApp.i().getPackageManager();
        try {
            PackageInfo a2 = e.a(packageManager, getPluginApkPath(), 1);
            if (!(a2 == null || a2.applicationInfo == null)) {
                String pluginApkPath = getPluginApkPath();
                a2.applicationInfo.sourceDir = pluginApkPath;
                a2.applicationInfo.publicSourceDir = pluginApkPath;
                ActivityInfo[] activityInfoArr = a2.activities;
                if (activityInfoArr != null && activityInfoArr.length > 0) {
                    for (ActivityInfo activityInfo : activityInfoArr) {
                        if (activityInfo.name != null && activityInfo.name.equals(str)) {
                            return packageManager.getDrawable(a2.packageName, activityInfo.getIconResource(), a2.applicationInfo);
                        }
                    }
                }
            }
        } catch (Throwable th) {
            XLog.e(this.f1068a, th.getMessage());
        }
        return null;
    }

    public String getMainReceiverImpl() {
        return this.n;
    }

    public void setMainReceiverImpl(String str) {
        this.n = str;
    }

    public String getApkRecieverImpl() {
        return this.o;
    }

    public void setApkRecieverImpl(String str) {
        this.o = str;
    }

    public String getAppServiceImpl() {
        return this.s;
    }

    public void setAppServiceImpl(String str) {
        this.s = str;
    }

    public String getIpcServiceImpl() {
        return this.t;
    }

    public void setIpcServiceImpl(String str) {
        this.t = str;
    }

    public String getPluginLibPath() {
        return j.a(this.c);
    }

    public PluginEntry getPluginEntryByStartActivity(String str) {
        if (this.l != null && this.l.size() > 0) {
            for (PluginEntry next : this.l) {
                if (next.getStartActivity() != null && next.getStartActivity().equals(str)) {
                    return next;
                }
            }
        }
        return null;
    }

    /* compiled from: ProGuard */
    public class PluginEntry {

        /* renamed from: a  reason: collision with root package name */
        private String f1069a;
        private Drawable b;
        private String c;
        private PluginInfo d;

        public PluginEntry(PluginInfo pluginInfo, String str, Drawable drawable, String str2) {
            this.d = pluginInfo;
            this.f1069a = str;
            this.b = drawable;
            this.c = str2;
        }

        public String getName() {
            return this.f1069a;
        }

        public void setName(String str) {
            this.f1069a = str;
        }

        public Drawable getIcon() {
            return this.b;
        }

        public void setIcon(Drawable drawable) {
            this.b = drawable;
        }

        public String getStartActivity() {
            return this.c;
        }

        public void setStartActivity(String str) {
            this.c = str;
        }

        public PluginInfo getHostPlugInfo() {
            return this.d;
        }

        public String toString() {
            return "PluginEntry{name='" + this.f1069a + '\'' + ", icon=" + this.b + ", startActivity='" + this.c + '\'' + '}';
        }
    }

    public String getExtendServiceImpl() {
        return j.a(this.w, ":", "|");
    }

    public String getExtendServiceImpl(String str) {
        if (this.w != null) {
            return this.w.get(str);
        }
        return null;
    }

    public String getExtendReceiverImpl(String str) {
        if (this.x != null) {
            return this.x.get(str);
        }
        return null;
    }

    public void setExtendServiceImpl(String str) {
        this.w = j.a(str, ":", "|");
    }

    public synchronized void addExtendServiceImpl(String str, String str2) {
        if (this.w == null) {
            this.w = new HashMap();
        }
        if (!TextUtils.isEmpty(str) && !TextUtils.isEmpty(str2)) {
            this.w.put(str, str2);
        }
    }

    public String getExtendReceiverImpl() {
        return j.a(this.x, ":", "|");
    }

    public void setExtendReceiverImpl(String str) {
        this.x = j.a(str, ":", "|");
    }

    public synchronized void addExtendReceiverImpl(String str, String str2) {
        if (this.x == null) {
            this.x = new HashMap();
        }
        if (!TextUtils.isEmpty(str) && !TextUtils.isEmpty(str2)) {
            this.x.put(str, str2);
        }
    }

    public String toString() {
        return "PluginInfo{version=" + this.b + ", packageName='" + this.c + '\'' + ", pkgid=" + this.e + ", minBaoVersion=" + this.f + ", minApiLevel=" + this.g + ", minFLevel=" + this.h + ", inProcess=" + this.i + ", launchApplication='" + this.j + '\'' + ", applicationInfo=" + this.k + ", entryList=" + this.l + ", smsReceiverImpl='" + this.m + '\'' + ", mainReceiverImpl='" + this.n + '\'' + ", apkRecieverImpl='" + this.o + '\'' + ", smsSentReceiverImpl='" + this.p + '\'' + ", authorReceiverImpl='" + this.q + '\'' + ", heartBeatReceiverImpl='" + this.r + '\'' + ", AppServiceImpl='" + this.s + '\'' + ", ipcServiceImpl='" + this.t + '\'' + ", dockReceiverImpl='" + this.u + '\'' + ", accelerationServiceImpl='" + this.v + '\'' + ", extendServiceMap=" + this.w + ", extendReceiverMap=" + this.x + '}';
    }
}
