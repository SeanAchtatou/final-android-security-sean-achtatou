package com.tencent.assistant.plugin;

import com.tencent.assistant.module.callback.CallbackHelper;

/* compiled from: ProGuard */
class d implements CallbackHelper.Caller<GetPluginListCallback> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f1093a;
    final /* synthetic */ GetPluginListEngine b;

    d(GetPluginListEngine getPluginListEngine, int i) {
        this.b = getPluginListEngine;
        this.f1093a = i;
    }

    /* renamed from: a */
    public void call(GetPluginListCallback getPluginListCallback) {
        getPluginListCallback.failed(this.f1093a);
    }
}
