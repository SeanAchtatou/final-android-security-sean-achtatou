package com.tencent.assistant.plugin;

import com.tencent.assistant.module.callback.CallbackHelper;

/* compiled from: ProGuard */
class a implements CallbackHelper.Caller<GetPluginListCallback> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GetPluginListEngine f1077a;

    a(GetPluginListEngine getPluginListEngine) {
        this.f1077a = getPluginListEngine;
    }

    /* renamed from: a */
    public void call(GetPluginListCallback getPluginListCallback) {
        getPluginListCallback.start();
    }
}
