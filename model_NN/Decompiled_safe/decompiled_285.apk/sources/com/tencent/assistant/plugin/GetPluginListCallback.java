package com.tencent.assistant.plugin;

import com.tencent.assistant.module.callback.ActionCallback;
import java.util.List;

/* compiled from: ProGuard */
public interface GetPluginListCallback extends ActionCallback {
    void failed(int i);

    void start();

    void success(List<PluginDownloadInfo> list);

    /* compiled from: ProGuard */
    public class Stub implements GetPluginListCallback {
        public void start() {
        }

        public void success(List<PluginDownloadInfo> list) {
        }

        public void failed(int i) {
        }
    }
}
