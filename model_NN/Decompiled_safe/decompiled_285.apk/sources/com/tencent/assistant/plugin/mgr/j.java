package com.tencent.assistant.plugin.mgr;

import android.content.Context;
import android.content.res.AssetManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import com.qq.AppService.AstApp;
import com.tencent.assistant.utils.FileUtil;
import com.tencent.assistant.utils.bm;
import java.io.File;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

/* compiled from: ProGuard */
public abstract class j {

    /* renamed from: a  reason: collision with root package name */
    private static boolean f1104a = false;

    public static boolean a() {
        try {
            Field declaredField = LayoutInflater.class.getDeclaredField("sConstructorMap");
            declaredField.setAccessible(true);
            ((HashMap) declaredField.get(null)).clear();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static String a(String str, int i) {
        return FileUtil.getPluginDir() + "/" + b(str, i);
    }

    public static String b(String str, int i) {
        return str + "_$_" + i + ".apk";
    }

    public static String c(String str, int i) {
        return str + "_$_" + i + ".plg";
    }

    public static String a(String str) {
        String str2 = FileUtil.getPluginDir() + File.separator + str;
        File file = new File(str2);
        if (!file.exists()) {
            file.mkdirs();
        }
        return str2;
    }

    public static String b(String str) {
        File dir = AstApp.i().getDir("plugin_dex_cache", 0);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        return dir.getAbsolutePath();
    }

    public static String d(String str, int i) {
        return c(str) + i;
    }

    public static String c(String str) {
        return str + "_$_";
    }

    public static AssetManager d(String str) {
        Throwable th;
        AssetManager assetManager;
        try {
            assetManager = AssetManager.class.newInstance();
            try {
                AssetManager.class.getDeclaredMethod("addAssetPath", String.class).invoke(assetManager, str);
            } catch (Throwable th2) {
                th = th2;
                th.printStackTrace();
                return assetManager;
            }
        } catch (Throwable th3) {
            Throwable th4 = th3;
            assetManager = null;
            th = th4;
            th.printStackTrace();
            return assetManager;
        }
        return assetManager;
    }

    public static int b() {
        return 3;
    }

    public static void a(Context context) {
    }

    public static void a(boolean z) {
        f1104a = z;
    }

    public static boolean c() {
        return f1104a;
    }

    public static Map<String, String> a(String str, String str2, String str3) {
        String[] strArr;
        String[] b;
        HashMap hashMap = null;
        if (!TextUtils.isEmpty(str)) {
            try {
                strArr = bm.b(str, str3);
            } catch (NoClassDefFoundError e) {
                strArr = null;
            }
            if (strArr != null && strArr.length > 0) {
                hashMap = new HashMap(strArr.length);
                for (String str4 : strArr) {
                    if (!TextUtils.isEmpty(str4) && (b = bm.b(str4, str2)) != null && b.length == 2) {
                        hashMap.put(b[0], b[1]);
                    }
                }
            }
        }
        return hashMap;
    }

    public static String a(Map<String, String> map, String str, String str2) {
        if (map == null || map.size() <= 0) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        for (Map.Entry next : map.entrySet()) {
            sb.append((String) next.getKey()).append(str).append((String) next.getValue()).append(str2);
        }
        return sb.toString();
    }
}
