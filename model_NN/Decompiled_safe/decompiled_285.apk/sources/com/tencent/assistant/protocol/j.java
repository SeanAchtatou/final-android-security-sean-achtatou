package com.tencent.assistant.protocol;

import com.tencent.assistant.protocol.a.a.b;
import com.tencent.assistant.protocol.a.e;
import com.tencent.assistant.protocol.jce.Request;
import com.tencent.assistant.protocol.jce.Response;
import com.tencent.assistant.protocol.jce.RspHead;
import com.tencent.assistant.utils.q;
import com.tencent.assistantv2.st.business.LaunchSpeedSTManager;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/* compiled from: ProGuard */
public class j implements f {
    private static j d;

    /* renamed from: a  reason: collision with root package name */
    protected ExecutorService f1123a = Executors.newFixedThreadPool(10, new q("protocalManager"));
    protected ConcurrentHashMap<Integer, m> b = new ConcurrentHashMap<>();
    private a c = new a();

    public static synchronized j a() {
        j jVar;
        synchronized (j.class) {
            if (d == null) {
                d = new j();
            }
            jVar = d;
        }
        return jVar;
    }

    private j() {
    }

    public int a(int i, Request request, List<Integer> list, g gVar) {
        return a(i, request, list, gVar, b.a());
    }

    public int a(int i, Request request, List<Integer> list, g gVar, ProtocolDecoder protocolDecoder) {
        h a2;
        int i2 = -1;
        if (!(request == null || (a2 = a(i, request, list, protocolDecoder)) == null)) {
            m mVar = new m();
            mVar.b = gVar;
            mVar.f1616a = a2;
            this.b.put(Integer.valueOf(a2.a()), mVar);
            try {
                i2 = a2.a();
            } finally {
                this.f1123a.submit(a2);
            }
        }
        return i2;
    }

    public void a(int i) {
        m remove = this.b.remove(Integer.valueOf(i));
        if (remove != null) {
            remove.f1616a.b();
        }
    }

    private h a(int i, Request request, List<Integer> list, ProtocolDecoder protocolDecoder) {
        b c2 = e.a().c();
        if (c2 == null) {
            return null;
        }
        h hVar = new h(this.c.a(), c2.b, i);
        hVar.a(list);
        hVar.a(c2.f1113a);
        hVar.a(request);
        hVar.a(this);
        hVar.a(protocolDecoder);
        HashMap hashMap = new HashMap();
        hashMap.put("Host", c2.c);
        hashMap.put("X-Online-Host", c2.c);
        hashMap.put("x-tx-host", c2.c);
        hVar.a(hashMap);
        return hVar;
    }

    public void a(int i, int i2, Request request, RspHead rspHead, Response response, long j) {
        if (LaunchSpeedSTManager.d().b(i)) {
            LaunchSpeedSTManager.d().a(LaunchSpeedSTManager.TYPE_TIME_POINT.Protocol_onNetWorkFinish_Begin);
        }
        e.a().a(i2 == 0, j);
        if (LaunchSpeedSTManager.d().b(i)) {
            LaunchSpeedSTManager.d().a(LaunchSpeedSTManager.TYPE_TIME_POINT.Protocol_onNetWorkStateChangeForNAC_End);
        }
        m remove = this.b.remove(Integer.valueOf(i));
        if (remove != null) {
            g gVar = remove.b;
            gVar.a(rspHead);
            if (LaunchSpeedSTManager.d().b(i)) {
                LaunchSpeedSTManager.d().a(LaunchSpeedSTManager.TYPE_TIME_POINT.Protocol_onUpdateRspHeadData_End);
            }
            gVar.a(i, i2, request, response);
        }
    }
}
