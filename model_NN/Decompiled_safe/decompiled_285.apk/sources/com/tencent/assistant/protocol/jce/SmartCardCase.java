package com.tencent.assistant.protocol.jce;

import java.io.Serializable;

/* compiled from: ProGuard */
public final class SmartCardCase implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    public static final SmartCardCase f1500a = new SmartCardCase(0, 0, "SMART_CARD_CASE_HOMEPAGE");
    public static final SmartCardCase b = new SmartCardCase(1, 1, "SMART_CARD_CASE_SOFTWARE_RECOMMENDATION");
    public static final SmartCardCase c = new SmartCardCase(2, 2, "SMART_CARD_CASE_SOFTWARE_POPULAR");
    public static final SmartCardCase d = new SmartCardCase(3, 3, "SMART_CARD_CASE_SOFTWARE_CATEGORY");
    public static final SmartCardCase e = new SmartCardCase(4, 4, "SMART_CARD_CASE_GAME");
    public static final SmartCardCase f = new SmartCardCase(5, 5, "SMART_CARD_DOWNLOAD_MANAGE_PAGE");
    static final /* synthetic */ boolean g = (!SmartCardCase.class.desiredAssertionStatus());
    private static SmartCardCase[] h = new SmartCardCase[6];
    private int i;
    private String j = new String();

    public String toString() {
        return this.j;
    }

    private SmartCardCase(int i2, int i3, String str) {
        this.j = str;
        this.i = i3;
        h[i2] = this;
    }
}
