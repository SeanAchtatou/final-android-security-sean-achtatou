package com.tencent.assistant.sdk;

import java.util.Comparator;

/* compiled from: ProGuard */
public class e implements Comparator<d> {
    /* renamed from: a */
    public int compare(d dVar, d dVar2) {
        if (dVar.e > dVar2.e) {
            return 1;
        }
        if (dVar.e < dVar2.e) {
            return -1;
        }
        return 0;
    }
}
