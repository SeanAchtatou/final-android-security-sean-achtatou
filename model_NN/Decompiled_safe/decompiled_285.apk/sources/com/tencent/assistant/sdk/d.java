package com.tencent.assistant.sdk;

import android.content.Context;

/* compiled from: ProGuard */
public class d {

    /* renamed from: a  reason: collision with root package name */
    public Context f1648a;
    public String b;
    public String c;
    public byte[] d;
    public long e = System.currentTimeMillis();

    public d(Context context, String str, byte[] bArr, String str2) {
        this.f1648a = context;
        this.b = str;
        this.d = bArr;
        this.c = str2;
    }
}
