package com.tencent.assistant.sdk;

import android.content.Context;
import android.text.TextUtils;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.sdk.param.jce.IPCBaseParam;
import com.tencent.assistant.sdk.param.jce.IPCRequest;
import com.tencent.assistant.sdk.param.jce.OperateDownloadTaskRequest;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.download.a;
import com.tencent.pangu.link.b;

/* compiled from: ProGuard */
public class m extends p {
    private OperateDownloadTaskRequest k;

    public m(Context context, IPCRequest iPCRequest) {
        super(context, iPCRequest);
    }

    /* access modifiers changed from: protected */
    public void a(JceStruct jceStruct) {
        DownloadInfo a2;
        if (jceStruct instanceof OperateDownloadTaskRequest) {
            this.k = (OperateDownloadTaskRequest) jceStruct;
            if (this.k != null) {
                this.b = this.k.b;
                if (this.b != null) {
                    this.h = this.b.e;
                    this.i = this.b.f;
                    this.j = this.b.g;
                }
                b("StatSdkToIpc");
                d();
                if ((this.k.f1669a == 4 || this.k.f1669a == 5) && (a2 = n.a(this.k.a())) != null) {
                    a.a().b(a2.downloadTicket);
                }
                b.a(this.c, a(this.k));
            }
        }
    }

    private String a(OperateDownloadTaskRequest operateDownloadTaskRequest) {
        String a2 = a(operateDownloadTaskRequest.f1669a);
        return a2 + b(operateDownloadTaskRequest);
    }

    private String a(int i) {
        String str;
        switch (i) {
            case 1:
                str = "appdetails";
                break;
            case 2:
                str = "download";
                break;
            case 3:
                str = "appdetails";
                break;
            case 4:
                str = "appdetails";
                break;
            case 5:
                str = "download";
                break;
            default:
                str = "appdetails";
                break;
        }
        return "tpmast://" + str + "?";
    }

    private String b(OperateDownloadTaskRequest operateDownloadTaskRequest) {
        String str = com.tencent.assistant.a.a.n + "=" + this.f1655a.c + "&" + com.tencent.assistant.a.a.o + "=" + this.f1655a.d + "&" + com.tencent.assistant.a.a.i + "=" + operateDownloadTaskRequest.b.f1662a + "&" + com.tencent.assistant.a.a.f382a + "=" + operateDownloadTaskRequest.b.b + "&" + com.tencent.assistant.a.a.c + "=" + operateDownloadTaskRequest.b.d + "&" + com.tencent.assistant.a.a.h + "=" + operateDownloadTaskRequest.b.c + "&" + com.tencent.assistant.a.a.e + "=" + operateDownloadTaskRequest.c + "&" + com.tencent.assistant.a.a.j + "=" + operateDownloadTaskRequest.b.g + "&" + com.tencent.assistant.a.a.y + "=" + com.tencent.pangu.b.a.a.a(operateDownloadTaskRequest.b.e) + "&" + com.tencent.assistant.a.a.z + "=" + operateDownloadTaskRequest.b.f + "&" + com.tencent.assistant.a.a.C + "=" + operateDownloadTaskRequest.b.h + "&" + com.tencent.assistant.a.a.ac + "=" + this.f1655a.e;
        if (!TextUtils.isEmpty(operateDownloadTaskRequest.d)) {
            str = str + "&" + com.tencent.assistant.a.a.s + "=" + operateDownloadTaskRequest.d;
        }
        if (!TextUtils.isEmpty(operateDownloadTaskRequest.e)) {
            str = str + "&" + com.tencent.assistant.a.a.g + "=" + operateDownloadTaskRequest.e;
        }
        String str2 = str + "&" + com.tencent.assistant.a.a.u + "=" + true;
        if (!TextUtils.isEmpty(com.tencent.pangu.b.a.a.b(operateDownloadTaskRequest.b.e))) {
            str2 = str2 + "&" + com.tencent.assistant.a.a.v + "=" + true;
        }
        return str2 + "&" + com.tencent.assistant.a.a.ag + "=" + operateDownloadTaskRequest.f1669a;
    }

    public IPCBaseParam b() {
        if (this.k == null) {
            return null;
        }
        return this.k.a();
    }

    /* access modifiers changed from: protected */
    public JceStruct a() {
        return null;
    }

    /* access modifiers changed from: protected */
    public boolean a(DownloadInfo downloadInfo) {
        return this.b != null && !TextUtils.isEmpty(this.b.c) && this.b.c.equals(String.valueOf(downloadInfo.versionCode)) && !TextUtils.isEmpty(this.b.d) && this.b.d.equals(downloadInfo.packageName);
    }
}
