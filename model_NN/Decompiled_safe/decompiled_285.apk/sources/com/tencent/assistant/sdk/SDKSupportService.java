package com.tencent.assistant.sdk;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import com.tencent.assistant.sdk.a.b;

/* compiled from: ProGuard */
public class SDKSupportService extends Service {

    /* renamed from: a  reason: collision with root package name */
    private b f1645a = new o(this);

    /* access modifiers changed from: protected */
    public byte a() {
        return 1;
    }

    public IBinder onBind(Intent intent) {
        return this.f1645a;
    }
}
