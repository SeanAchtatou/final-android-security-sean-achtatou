package com.tencent.assistant.activity;

import android.content.Intent;
import android.view.View;

/* compiled from: ProGuard */
class bd implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ InstalledAppManagerActivity f415a;

    bd(InstalledAppManagerActivity installedAppManagerActivity) {
        this.f415a = installedAppManagerActivity;
    }

    public void onClick(View view) {
        this.f415a.startActivity(new Intent(this.f415a, PanelManagerActivity.class));
        if (this.f415a != null && !this.f415a.isFinishing() && this.f415a.Y != null && this.f415a.Y.isShowing()) {
            this.f415a.Y.dismiss();
        }
    }
}
