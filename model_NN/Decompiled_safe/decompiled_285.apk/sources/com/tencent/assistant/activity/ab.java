package com.tencent.assistant.activity;

import android.view.animation.Animation;

/* compiled from: ProGuard */
class ab implements Animation.AnimationListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppBackupActivity f387a;

    ab(AppBackupActivity appBackupActivity) {
        this.f387a = appBackupActivity;
    }

    public void onAnimationStart(Animation animation) {
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationEnd(Animation animation) {
        long j;
        this.f387a.N.setVisibility(8);
        if (this.f387a.ad) {
            this.f387a.ac.setVisibility(8);
            this.f387a.ab.setVisibility(0);
        } else {
            this.f387a.ac.setVisibility(0);
            this.f387a.ab.setVisibility(8);
        }
        this.f387a.a(this.f387a.aa, 90.0f, 0.0f, false, new ac(this));
        if (this.f387a.ad) {
            j = 5000;
        } else {
            j = 3000;
        }
        if (this.f387a.ao.hasMessages(11901)) {
            this.f387a.ao.removeMessages(11901);
        }
        this.f387a.ao.sendEmptyMessageDelayed(11901, j);
    }
}
