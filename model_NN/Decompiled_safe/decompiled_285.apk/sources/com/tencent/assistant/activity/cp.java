package com.tencent.assistant.activity;

import android.content.Intent;
import android.view.View;
import com.tencent.nucleus.manager.spaceclean.BigFileCleanActivity;

/* compiled from: ProGuard */
class cp implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SpaceCleanActivity f454a;

    cp(SpaceCleanActivity spaceCleanActivity) {
        this.f454a = spaceCleanActivity;
    }

    public void onClick(View view) {
        Intent intent = new Intent(this.f454a, BigFileCleanActivity.class);
        intent.putExtra("dock_plugin", this.f454a.Q);
        this.f454a.startActivity(intent);
        this.f454a.finish();
    }
}
