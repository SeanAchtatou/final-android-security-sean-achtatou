package com.tencent.assistant.activity;

import com.tencent.assistant.utils.XLog;
import com.tencent.nucleus.manager.a;

/* compiled from: ProGuard */
class cq implements a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SpaceCleanActivity f455a;

    cq(SpaceCleanActivity spaceCleanActivity) {
        this.f455a = spaceCleanActivity;
    }

    public void a() {
        XLog.d("miles", "SpaceCleanActivity >> onBindTmsServiceSuccess..");
        if (this.f455a.M) {
            this.f455a.Y.sendEmptyMessage(1);
        }
    }

    public void b() {
        XLog.d("miles", "SpaceCleanActivity >> onBindTmsServiceFailed..");
        if (this.f455a.M) {
            this.f455a.Y.sendEmptyMessage(-2);
        }
    }

    public void c() {
        XLog.d("miles", "SpaceCleanActivity >> onTmsServiceDisconnected.");
        this.f455a.y();
    }
}
