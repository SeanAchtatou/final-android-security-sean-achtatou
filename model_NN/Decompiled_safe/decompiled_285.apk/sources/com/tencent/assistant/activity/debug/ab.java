package com.tencent.assistant.activity.debug;

import android.view.View;
import com.tencent.assistant.utils.TemporaryThreadManager;

/* compiled from: ProGuard */
class ab implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DActivity f475a;

    ab(DActivity dActivity) {
        this.f475a = dActivity;
    }

    public void onClick(View view) {
        TemporaryThreadManager.get().start(new ac(this));
    }
}
