package com.tencent.assistant.activity;

import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.nucleus.manager.spaceclean.RubbishItemView;

/* compiled from: ProGuard */
class cj extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SpaceCleanActivity f448a;

    cj(SpaceCleanActivity spaceCleanActivity) {
        this.f448a = spaceCleanActivity;
    }

    public void onTMAClick(View view) {
        if (this.f448a.A.getFooterViewEnable()) {
            this.f448a.C.b();
            this.f448a.z();
            this.f448a.A();
            RubbishItemView.b = true;
            this.f448a.A.updateContent(this.f448a.getString(R.string.apkmgr_is_deleting));
            this.f448a.A.setFooterViewEnable(false);
        }
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f448a.w, 200);
        buildSTInfo.scene = STConst.ST_PAGE_GARBAGE_INSTALL_STEWARD;
        buildSTInfo.slotId = "05_001";
        return buildSTInfo;
    }
}
