package com.tencent.assistant.activity.debug;

import android.view.View;
import android.widget.Toast;
import com.tencent.pangu.component.CommentDetailView;

/* compiled from: ProGuard */
class h implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DActivity f507a;

    h(DActivity dActivity) {
        this.f507a = dActivity;
    }

    public void onClick(View view) {
        CommentDetailView.f3490a = !CommentDetailView.f3490a;
        if (CommentDetailView.f3490a) {
            Toast.makeText(this.f507a.getApplicationContext(), "评论主人态打开", 1).show();
        } else {
            Toast.makeText(this.f507a.getApplicationContext(), "评论主人态关闭", 1).show();
        }
    }
}
