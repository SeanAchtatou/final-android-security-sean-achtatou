package com.tencent.assistant.db.table;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.qq.AppService.AstApp;
import com.tencent.assistant.db.helper.AstDbHelper;
import com.tencent.assistant.db.helper.SQLiteDatabaseWrapper;
import com.tencent.assistant.db.helper.SqliteHelper;
import java.util.List;

/* compiled from: ProGuard */
public class x implements IBaseTable {
    public x() {
    }

    public x(Context context) {
    }

    public synchronized int a(List<String> list) {
        int i;
        i = 0;
        if (list != null) {
            if (list.size() != 0) {
                b();
                SQLiteDatabaseWrapper writableDatabaseWrapper = getHelper().getWritableDatabaseWrapper();
                int i2 = 0;
                for (String put : list) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("packageName", put);
                    i2 = (int) (((long) i2) + writableDatabaseWrapper.insert("recentTask_table", null, contentValues));
                }
                i = i2;
            }
        }
        return i;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0037, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x003a, code lost:
        return r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0040, code lost:
        if (r1 == null) goto L_0x003a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0035, code lost:
        if (r1 != null) goto L_0x0037;
     */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0047  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List<java.lang.String> a() {
        /*
            r10 = this;
            r8 = 0
            java.util.ArrayList r9 = new java.util.ArrayList
            r9.<init>()
            com.tencent.assistant.db.helper.SqliteHelper r0 = r10.getHelper()
            com.tencent.assistant.db.helper.SQLiteDatabaseWrapper r0 = r0.getReadableDatabaseWrapper()
            java.lang.String r1 = "recentTask_table"
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x003b, all -> 0x0043 }
            if (r1 == 0) goto L_0x0035
            boolean r0 = r1.moveToFirst()     // Catch:{ Exception -> 0x004d }
            if (r0 == 0) goto L_0x0035
        L_0x0022:
            java.lang.String r0 = "packageName"
            int r0 = r1.getColumnIndex(r0)     // Catch:{ Exception -> 0x004d }
            java.lang.String r0 = r1.getString(r0)     // Catch:{ Exception -> 0x004d }
            r9.add(r0)     // Catch:{ Exception -> 0x004d }
            boolean r0 = r1.moveToNext()     // Catch:{ Exception -> 0x004d }
            if (r0 != 0) goto L_0x0022
        L_0x0035:
            if (r1 == 0) goto L_0x003a
        L_0x0037:
            r1.close()
        L_0x003a:
            return r9
        L_0x003b:
            r0 = move-exception
            r1 = r8
        L_0x003d:
            r0.printStackTrace()     // Catch:{ all -> 0x004b }
            if (r1 == 0) goto L_0x003a
            goto L_0x0037
        L_0x0043:
            r0 = move-exception
            r1 = r8
        L_0x0045:
            if (r1 == 0) goto L_0x004a
            r1.close()
        L_0x004a:
            throw r0
        L_0x004b:
            r0 = move-exception
            goto L_0x0045
        L_0x004d:
            r0 = move-exception
            goto L_0x003d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.db.table.x.a():java.util.List");
    }

    public int b() {
        return getHelper().getWritableDatabaseWrapper().delete("recentTask_table", null, null);
    }

    public int tableVersion() {
        return 1;
    }

    public String tableName() {
        return "recentTask_table";
    }

    public String createTableSQL() {
        return "CREATE TABLE if not exists recentTask_table(_id INTEGER PRIMARY KEY AUTOINCREMENT,packageName Text);";
    }

    public String[] getAlterSQL(int i, int i2) {
        return null;
    }

    public void beforeTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public void afterTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public SqliteHelper getHelper() {
        return AstDbHelper.get(AstApp.i());
    }
}
