package com.tencent.assistant.module.update;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.tencent.assistant.localres.model.LocalApkInfo;

/* compiled from: ProGuard */
class i extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b f1035a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    i(b bVar, Looper looper) {
        super(looper);
        this.f1035a = bVar;
    }

    public void handleMessage(Message message) {
        if (message.what == 2) {
            boolean unused = this.f1035a.d();
        } else if (message.what == 1 && message.obj != null && (message.obj instanceof LocalApkInfo)) {
            LocalApkInfo localApkInfo = (LocalApkInfo) message.obj;
            this.f1035a.a(message.arg1, message.getData().getBoolean("isReplacing"), localApkInfo.mPackageName, localApkInfo.mVersionCode);
        }
    }
}
