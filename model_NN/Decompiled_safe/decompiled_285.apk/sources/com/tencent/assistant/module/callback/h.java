package com.tencent.assistant.module.callback;

import com.tencent.assistant.protocol.jce.AppSimpleDetail;

/* compiled from: ProGuard */
public interface h extends ActionCallback {
    void onGetAppInfoFail(int i, int i2);

    void onGetAppInfoSuccess(int i, int i2, AppSimpleDetail appSimpleDetail);
}
