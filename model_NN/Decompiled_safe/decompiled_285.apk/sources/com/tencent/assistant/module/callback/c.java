package com.tencent.assistant.module.callback;

import com.tencent.assistant.module.callback.CallbackHelper;

/* compiled from: ProGuard */
class c implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CallbackHelper.Caller f988a;
    final /* synthetic */ CallbackHelper b;

    c(CallbackHelper callbackHelper, CallbackHelper.Caller caller) {
        this.b = callbackHelper;
        this.f988a = caller;
    }

    public void run() {
        this.b.broadcast(this.f988a);
    }
}
