package com.tencent.assistant.module.update;

import com.tencent.assistant.Global;
import com.tencent.assistant.module.update.AppUpdateConst;
import com.tencent.assistant.protocol.jce.AppInfoForUpdate;
import com.tencent.assistant.st.STConst;
import com.tencent.beacon.event.a;
import java.util.ArrayList;
import java.util.HashMap;

/* compiled from: ProGuard */
class p implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ boolean f1042a;
    final /* synthetic */ AppUpdateConst.RequestLaunchType b;
    final /* synthetic */ j c;

    p(j jVar, boolean z, AppUpdateConst.RequestLaunchType requestLaunchType) {
        this.c = jVar;
        this.f1042a = z;
        this.b = requestLaunchType;
    }

    public void run() {
        int i;
        HashMap hashMap = new HashMap();
        hashMap.put("B1", Global.getQUA());
        hashMap.put("B2", Global.getPhoneGuid());
        hashMap.put("B3", String.valueOf(j.f1036a));
        ArrayList<AppInfoForUpdate> a2 = j.a(this.f1042a);
        hashMap.put("B4", String.valueOf(this.b));
        if (this.f1042a) {
            i = 0;
        } else {
            i = 1;
        }
        hashMap.put("B5", String.valueOf(i));
        hashMap.put("B6", a2 != null ? String.valueOf(a2.size()) : STConst.ST_DEFAULT_SLOT);
        a.a("StatAppUpdateRequest", true, 0, 0, hashMap, true);
    }
}
