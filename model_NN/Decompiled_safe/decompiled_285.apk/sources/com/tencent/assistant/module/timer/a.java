package com.tencent.assistant.module.timer;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.util.Log;

/* compiled from: ProGuard */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private static a f1012a;
    private Handler b = null;

    private a() {
        HandlerThread handlerThread = new HandlerThread("timer");
        handlerThread.start();
        Looper looper = handlerThread.getLooper();
        if (looper != null) {
            this.b = new b(this, looper);
        } else {
            Log.e("SimpleTimer", "SimpleTimer loop == null");
        }
    }

    protected static synchronized a a() {
        a aVar;
        synchronized (a.class) {
            if (f1012a == null) {
                f1012a = new a();
            }
            aVar = f1012a;
        }
        return aVar;
    }

    public void a(ScheduleJob scheduleJob) {
        if (this.b != null && scheduleJob != null) {
            this.b.sendMessageDelayed(this.b.obtainMessage(scheduleJob.b(), 0, 0, scheduleJob), (long) (scheduleJob.h() * 1000));
        }
    }
}
