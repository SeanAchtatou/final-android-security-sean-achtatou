package com.tencent.assistant.kapalaiadapter.a;

import android.content.Context;
import android.telephony.TelephonyManager;
import com.tencent.assistant.kapalaiadapter.g;

/* compiled from: ProGuard */
public class h implements j {

    /* renamed from: a  reason: collision with root package name */
    private TelephonyManager f787a = null;
    private int b = 1;
    private int c = 5;

    public Object a(int i, Context context) {
        if (this.f787a == null) {
            try {
                this.f787a = (TelephonyManager) context.getSystemService("phone");
            } catch (Exception e) {
            }
        }
        return this.f787a;
    }

    public String b(int i, Context context) {
        Object a2 = a(i, context);
        try {
            Object[] objArr = new Object[1];
            objArr[0] = Integer.valueOf(i == 0 ? this.b : this.c);
            return (String) g.a(a2, "getSubscriberIdExt", objArr);
        } catch (Exception e) {
            return null;
        }
    }

    public String c(int i, Context context) {
        Object a2 = a(i, context);
        try {
            Object[] objArr = new Object[1];
            objArr[0] = Integer.valueOf(i == 0 ? this.b : this.c);
            return (String) g.a(a2, "getDeviceIdExt", objArr);
        } catch (Exception e) {
            return null;
        }
    }
}
