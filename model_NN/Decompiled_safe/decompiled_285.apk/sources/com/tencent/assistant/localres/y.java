package com.tencent.assistant.localres;

import com.tencent.assistant.module.callback.ActionCallback;
import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistant.utils.ah;

/* compiled from: ProGuard */
public abstract class y<T extends ActionCallback> {

    /* renamed from: a  reason: collision with root package name */
    private CallbackHelper<T> f846a = new CallbackHelper<>();

    public void a(ActionCallback actionCallback) {
        this.f846a.register(actionCallback);
    }

    public void b(ActionCallback actionCallback) {
        this.f846a.unregister(actionCallback);
    }

    /* access modifiers changed from: protected */
    public void a(CallbackHelper.Caller caller) {
        ah.a().post(new z(this, caller));
    }

    /* access modifiers changed from: protected */
    public void b(CallbackHelper.Caller caller) {
        this.f846a.broadcast(caller);
    }
}
