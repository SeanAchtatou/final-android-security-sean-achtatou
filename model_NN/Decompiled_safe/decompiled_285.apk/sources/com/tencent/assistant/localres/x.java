package com.tencent.assistant.localres;

import android.content.Context;
import android.provider.MediaStore;
import com.tencent.assistant.localres.model.LocalImage;

/* compiled from: ProGuard */
public class x extends LocalMediaLoader<LocalImage> {
    public x(Context context) {
        super(context);
        this.c = 1;
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.b.clear();
        this.b.addAll(a(MediaStore.Images.Media.EXTERNAL_CONTENT_URI));
        if (this.b.size() <= 0) {
            this.b.addAll(a(MediaStore.Images.Media.INTERNAL_CONTENT_URI));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.localres.LocalMediaLoader.a(com.tencent.assistant.localres.LocalMediaLoader, java.util.ArrayList, boolean):void
     arg types: [com.tencent.assistant.localres.x, java.util.ArrayList<com.tencent.assistant.localres.model.LocalImage>, int]
     candidates:
      com.tencent.assistant.localres.x.a(int, java.lang.String[], java.lang.String):java.lang.String
      com.tencent.assistant.localres.LocalMediaLoader.a(com.tencent.assistant.localres.LocalMediaLoader, java.util.ArrayList, boolean):void */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0096, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x009f, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x00a6, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x00a9, code lost:
        r0 = r3;
        r1 = r7;
        r2 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x00b1, code lost:
        r0 = r3;
        r2 = false;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0096 A[Catch:{ Throwable -> 0x0090, all -> 0x009b }] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x009f A[Catch:{ Throwable -> 0x0090, all -> 0x009b }] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x00a6 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:8:0x001c] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:13:0x0031=Splitter:B:13:0x0031, B:36:0x0086=Splitter:B:36:0x0086} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized java.util.ArrayList<com.tencent.assistant.localres.model.LocalImage> a(android.net.Uri r13) {
        /*
            r12 = this;
            r9 = 1
            r8 = 0
            r6 = 0
            monitor-enter(r12)
            r10 = 1
            java.util.ArrayList r7 = new java.util.ArrayList     // Catch:{ all -> 0x00a3 }
            r7.<init>()     // Catch:{ all -> 0x00a3 }
            android.content.Context r0 = r12.f804a     // Catch:{ Throwable -> 0x0090, all -> 0x009b }
            android.content.ContentResolver r0 = r0.getContentResolver()     // Catch:{ Throwable -> 0x0090, all -> 0x009b }
            r2 = 0
            r3 = 0
            r4 = 0
            java.lang.String r5 = "date_added desc"
            r1 = r13
            android.database.Cursor r3 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Throwable -> 0x0090, all -> 0x009b }
            if (r3 == 0) goto L_0x0086
            boolean r0 = r3.moveToFirst()     // Catch:{ Throwable -> 0x00a8, all -> 0x00a6 }
            if (r0 == 0) goto L_0x0086
            java.lang.String r0 = "_data"
            int r8 = r3.getColumnIndexOrThrow(r0)     // Catch:{ Throwable -> 0x00a8, all -> 0x00a6 }
            java.lang.String r0 = "_id"
            int r10 = r3.getColumnIndexOrThrow(r0)     // Catch:{ Throwable -> 0x00a8, all -> 0x00a6 }
            r0 = r6
            r1 = r7
            r2 = r9
        L_0x0031:
            java.lang.String r5 = r3.getString(r8)     // Catch:{ Throwable -> 0x00ad, all -> 0x00a6 }
            boolean r4 = r12.a(r5)     // Catch:{ Throwable -> 0x00ad, all -> 0x00a6 }
            if (r4 == 0) goto L_0x004c
        L_0x003b:
            boolean r4 = r3.moveToNext()     // Catch:{ Throwable -> 0x00ad, all -> 0x00a6 }
            if (r4 != 0) goto L_0x0031
            if (r3 == 0) goto L_0x0046
            r3.close()     // Catch:{ all -> 0x00a3 }
        L_0x0046:
            r0 = r1
        L_0x0047:
            r12.a(r12, r0, r2)     // Catch:{ all -> 0x00a3 }
        L_0x004a:
            monitor-exit(r12)
            return r0
        L_0x004c:
            int r7 = r3.getInt(r10)     // Catch:{ Throwable -> 0x00ad, all -> 0x00a6 }
            r4 = 2
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Throwable -> 0x00ad, all -> 0x00a6 }
            r9 = 0
            java.lang.String r11 = "_id"
            r4[r9] = r11     // Catch:{ Throwable -> 0x00ad, all -> 0x00a6 }
            r9 = 1
            java.lang.String r11 = "_data"
            r4[r9] = r11     // Catch:{ Throwable -> 0x00ad, all -> 0x00a6 }
            java.lang.String r9 = "image_id"
            java.lang.String r4 = r12.a(r7, r4, r9)     // Catch:{ Throwable -> 0x00ad, all -> 0x00a6 }
            if (r4 != 0) goto L_0x0066
            r4 = r5
        L_0x0066:
            com.tencent.assistant.localres.model.LocalImage r9 = new com.tencent.assistant.localres.model.LocalImage     // Catch:{ Throwable -> 0x00ad, all -> 0x00a6 }
            r9.<init>()     // Catch:{ Throwable -> 0x00ad, all -> 0x00a6 }
            r9.id = r7     // Catch:{ Throwable -> 0x00ad, all -> 0x00a6 }
            r9.path = r5     // Catch:{ Throwable -> 0x00ad, all -> 0x00a6 }
            r9.thumbnailPath = r4     // Catch:{ Throwable -> 0x00ad, all -> 0x00a6 }
            r1.add(r9)     // Catch:{ Throwable -> 0x00ad, all -> 0x00a6 }
            r4 = 30
            if (r0 <= r4) goto L_0x0083
            r12.a(r12, r1, r2)     // Catch:{ Throwable -> 0x00ad, all -> 0x00a6 }
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ Throwable -> 0x00b0, all -> 0x00a6 }
            r0.<init>()     // Catch:{ Throwable -> 0x00b0, all -> 0x00a6 }
            r1 = r0
            r2 = r6
            r0 = r6
        L_0x0083:
            int r0 = r0 + 1
            goto L_0x003b
        L_0x0086:
            r12.a(r12, r7, r10)     // Catch:{ Throwable -> 0x00a8, all -> 0x00a6 }
            if (r3 == 0) goto L_0x008e
            r3.close()     // Catch:{ all -> 0x00a3 }
        L_0x008e:
            r0 = r7
            goto L_0x004a
        L_0x0090:
            r0 = move-exception
            r0 = r8
            r1 = r7
            r2 = r9
        L_0x0094:
            if (r0 == 0) goto L_0x0099
            r0.close()     // Catch:{ all -> 0x00a3 }
        L_0x0099:
            r0 = r1
            goto L_0x0047
        L_0x009b:
            r0 = move-exception
            r3 = r8
        L_0x009d:
            if (r3 == 0) goto L_0x00a2
            r3.close()     // Catch:{ all -> 0x00a3 }
        L_0x00a2:
            throw r0     // Catch:{ all -> 0x00a3 }
        L_0x00a3:
            r0 = move-exception
            monitor-exit(r12)
            throw r0
        L_0x00a6:
            r0 = move-exception
            goto L_0x009d
        L_0x00a8:
            r0 = move-exception
            r0 = r3
            r1 = r7
            r2 = r9
            goto L_0x0094
        L_0x00ad:
            r0 = move-exception
            r0 = r3
            goto L_0x0094
        L_0x00b0:
            r0 = move-exception
            r0 = r3
            r2 = r6
            goto L_0x0094
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.localres.x.a(android.net.Uri):java.util.ArrayList");
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x003c  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x004c  */
    /* JADX WARNING: Removed duplicated region for block: B:26:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String a(int r8, java.lang.String[] r9, java.lang.String r10) {
        /*
            r7 = this;
            r6 = 0
            android.content.Context r0 = r7.f804a     // Catch:{ Throwable -> 0x0040, all -> 0x0049 }
            android.content.ContentResolver r0 = r0.getContentResolver()     // Catch:{ Throwable -> 0x0040, all -> 0x0049 }
            android.net.Uri r1 = android.provider.MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI     // Catch:{ Throwable -> 0x0040, all -> 0x0049 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0040, all -> 0x0049 }
            r2.<init>()     // Catch:{ Throwable -> 0x0040, all -> 0x0049 }
            java.lang.StringBuilder r2 = r2.append(r10)     // Catch:{ Throwable -> 0x0040, all -> 0x0049 }
            java.lang.String r3 = "="
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Throwable -> 0x0040, all -> 0x0049 }
            java.lang.StringBuilder r2 = r2.append(r8)     // Catch:{ Throwable -> 0x0040, all -> 0x0049 }
            java.lang.String r3 = r2.toString()     // Catch:{ Throwable -> 0x0040, all -> 0x0049 }
            r4 = 0
            r5 = 0
            r2 = r9
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Throwable -> 0x0040, all -> 0x0049 }
            if (r1 == 0) goto L_0x0056
            boolean r0 = r1.moveToFirst()     // Catch:{ Throwable -> 0x0053, all -> 0x0050 }
            if (r0 == 0) goto L_0x0056
            java.lang.String r0 = "_data"
            int r0 = r1.getColumnIndex(r0)     // Catch:{ Throwable -> 0x0053, all -> 0x0050 }
            java.lang.String r6 = r1.getString(r0)     // Catch:{ Throwable -> 0x0053, all -> 0x0050 }
            r0 = r6
        L_0x003a:
            if (r1 == 0) goto L_0x003f
            r1.close()
        L_0x003f:
            return r0
        L_0x0040:
            r0 = move-exception
            r0 = r6
        L_0x0042:
            if (r0 == 0) goto L_0x0047
            r0.close()
        L_0x0047:
            r0 = r6
            goto L_0x003f
        L_0x0049:
            r0 = move-exception
        L_0x004a:
            if (r6 == 0) goto L_0x004f
            r6.close()
        L_0x004f:
            throw r0
        L_0x0050:
            r0 = move-exception
            r6 = r1
            goto L_0x004a
        L_0x0053:
            r0 = move-exception
            r0 = r1
            goto L_0x0042
        L_0x0056:
            r0 = r6
            goto L_0x003a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.localres.x.a(int, java.lang.String[], java.lang.String):java.lang.String");
    }

    /* access modifiers changed from: protected */
    public boolean a(String str) {
        return false;
    }
}
