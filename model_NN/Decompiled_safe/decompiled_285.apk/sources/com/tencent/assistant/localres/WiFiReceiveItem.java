package com.tencent.assistant.localres;

import com.tencent.assistant.component.txscrollview.TXImageView;

/* compiled from: ProGuard */
public class WiFiReceiveItem {
    public static final int ReceiveState_Receing = 0;
    public static final int ReceiveState_finish = 1;
    public long lastModifiedTime;
    public String mFileId;
    public String mFileName;
    public String mFilePath;
    public String mFileSessionId;
    public long mFileSize = 0;
    public String mFileType;
    public TXImageView.TXImageViewType mImageType = TXImageView.TXImageViewType.UNKNOWN_IMAGE_TYPE;
    public long mReceiveSize = 0;
    public int mReceiveState;
}
