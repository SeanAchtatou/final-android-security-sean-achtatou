package com.tencent.assistant.localres;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import com.qq.AppService.AstApp;
import com.tencent.assistant.localres.callback.a;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.utils.aq;
import com.tencent.assistant.utils.as;
import com.tencent.assistant.utils.av;
import com.tencent.assistant.utils.e;
import com.tencent.connect.common.Constants;
import java.io.File;

/* compiled from: ProGuard */
class c implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f822a;
    final /* synthetic */ a b;
    final /* synthetic */ ApkResourceManager c;

    c(ApkResourceManager apkResourceManager, String str, a aVar) {
        this.c = apkResourceManager;
        this.f822a = str;
        this.b = aVar;
    }

    public void run() {
        try {
            PackageManager packageManager = AstApp.i().getPackageManager();
            PackageInfo packageInfo = packageManager.getPackageInfo(this.f822a, 64);
            if (packageInfo != null) {
                ApplicationInfo applicationInfo = AstApp.i().getApplicationInfo();
                LocalApkInfo localApkInfo = new LocalApkInfo();
                localApkInfo.mPackageName = packageInfo.packageName;
                localApkInfo.mVersionName = packageInfo.versionName == null ? Constants.STR_EMPTY : packageInfo.versionName;
                localApkInfo.mVersionCode = packageInfo.versionCode;
                localApkInfo.mLocalFilePath = applicationInfo.sourceDir;
                localApkInfo.flags = applicationInfo.flags;
                localApkInfo.mInstalleLocation = (byte) e.a(localApkInfo);
                localApkInfo.mAppIconRes = applicationInfo.icon;
                localApkInfo.mAppName = applicationInfo.loadLabel(packageManager).toString().trim();
                localApkInfo.occupySize = 0;
                localApkInfo.mInstallDate = new File(localApkInfo.mLocalFilePath).lastModified();
                localApkInfo.mSortKey = av.a(localApkInfo.mAppName);
                if (packageInfo.signatures.length >= 1) {
                    localApkInfo.signature = aq.b(packageInfo.signatures[packageInfo.signatures.length - 1].toCharsString());
                }
                localApkInfo.manifestMd5 = as.a(localApkInfo.mLocalFilePath);
                this.b.a(localApkInfo);
            }
        } catch (Throwable th) {
            th.printStackTrace();
            this.b.a(null);
        }
    }
}
