package com.tencent.assistant.utils.a;

import java.io.DataInputStream;
import java.io.DataOutputStream;

/* compiled from: ProGuard */
public class h {

    /* renamed from: a  reason: collision with root package name */
    short f1809a;
    short b;
    short c;
    short d;
    int e;
    int f;
    short g;
    byte[] h;

    public void a(DataOutputStream dataOutputStream) {
        dataOutputStream.writeInt(1347093766);
        dataOutputStream.writeShort(e.a(this.f1809a));
        dataOutputStream.writeShort(e.a(this.b));
        dataOutputStream.writeShort(e.a(this.c));
        dataOutputStream.writeShort(e.a(this.d));
        dataOutputStream.writeInt(e.a(this.e));
        dataOutputStream.writeInt(e.a(this.f));
        dataOutputStream.writeShort(e.a(this.g));
        if (this.g > 0) {
            dataOutputStream.write(this.h);
        }
    }

    public void a(DataInputStream dataInputStream) {
        this.f1809a = e.a(dataInputStream.readShort());
        this.b = e.a(dataInputStream.readShort());
        this.c = e.a(dataInputStream.readShort());
        this.d = e.a(dataInputStream.readShort());
        this.e = e.a(dataInputStream.readInt());
        this.f = e.a(dataInputStream.readInt());
        this.g = e.a(dataInputStream.readShort());
        if (this.g > 0) {
            this.h = new byte[this.g];
            dataInputStream.read(this.h, 0, this.g);
        }
    }
}
