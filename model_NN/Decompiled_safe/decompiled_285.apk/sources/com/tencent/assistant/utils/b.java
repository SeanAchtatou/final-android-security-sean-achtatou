package com.tencent.assistant.utils;

import android.os.Environment;
import android.os.StatFs;
import android.text.TextUtils;
import android.util.Pair;
import com.qq.AppService.AstApp;
import com.tencent.assistant.manager.t;
import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/* compiled from: ProGuard */
public class b {

    /* renamed from: a  reason: collision with root package name */
    private static String f1828a = FileUtil.getAPKDir();
    private static String b;
    private static String c;
    private static List<String> d = new ArrayList(1);
    private static Set<String> e = new HashSet(0);

    static {
        d.add(f1828a);
        a();
        t.a().a(new c());
    }

    public static void a() {
        TemporaryThreadManager.get().start(new d());
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x007f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void b() {
        /*
            r10 = 104857600(0x6400000, double:5.1806538E-316)
            r0 = 0
            r2 = 0
            r3 = 4
            java.util.ArrayList r6 = new java.util.ArrayList
            r6.<init>(r3)
            java.util.HashSet r7 = new java.util.HashSet
            r7.<init>(r3)
            boolean r3 = com.tencent.assistant.utils.FileUtil.isSDCardExistAndCanWrite()     // Catch:{ Exception -> 0x00bf }
            if (r3 == 0) goto L_0x00cb
            java.io.File r3 = android.os.Environment.getExternalStorageDirectory()     // Catch:{ Exception -> 0x00bf }
            java.lang.String r8 = r3.getPath()     // Catch:{ Exception -> 0x00bf }
            r7.add(r8)     // Catch:{ Exception -> 0x00bf }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00bf }
            r3.<init>()     // Catch:{ Exception -> 0x00bf }
            java.lang.StringBuilder r3 = r3.append(r8)     // Catch:{ Exception -> 0x00bf }
            java.lang.String r4 = "/tencent/tassistant"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x00bf }
            java.lang.String r4 = "/apk"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x00bf }
            java.lang.String r5 = r3.toString()     // Catch:{ Exception -> 0x00bf }
            long r3 = com.tencent.assistant.utils.e.f()     // Catch:{ Exception -> 0x00bf }
            r6.add(r5)     // Catch:{ Exception -> 0x00bf }
            boolean r9 = b(r5)     // Catch:{ Exception -> 0x00bf }
            if (r9 == 0) goto L_0x0096
            int r0 = (r3 > r10 ? 1 : (r3 == r10 ? 0 : -1))
            if (r0 <= 0) goto L_0x00c9
            r2 = r5
            r1 = r5
        L_0x004e:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00bf }
            r0.<init>()     // Catch:{ Exception -> 0x00bf }
            java.lang.StringBuilder r0 = r0.append(r8)     // Catch:{ Exception -> 0x00bf }
            java.lang.String r5 = "/BaoDownload"
            java.lang.StringBuilder r0 = r0.append(r5)     // Catch:{ Exception -> 0x00bf }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x00bf }
            r6.add(r0)     // Catch:{ Exception -> 0x00bf }
        L_0x0064:
            android.util.Pair r5 = a(r1, r6, r7)     // Catch:{ Exception -> 0x00bf }
            if (r2 != 0) goto L_0x00c3
            java.lang.Object r0 = r5.first     // Catch:{ Exception -> 0x00bf }
            if (r0 == 0) goto L_0x00c3
            java.lang.Object r0 = r5.second     // Catch:{ Exception -> 0x00bf }
            java.lang.Long r0 = (java.lang.Long) r0     // Catch:{ Exception -> 0x00bf }
            if (r1 == 0) goto L_0x00ba
            long r8 = r0.longValue()     // Catch:{ Exception -> 0x00bf }
            int r0 = (r8 > r3 ? 1 : (r8 == r3 ? 0 : -1))
            if (r0 >= 0) goto L_0x00ba
            r0 = r1
        L_0x007d:
            if (r0 != 0) goto L_0x0083
            java.lang.String r0 = com.tencent.assistant.utils.FileUtil.getAPKDir()
        L_0x0083:
            com.tencent.assistant.utils.b.e = r7
            com.tencent.assistant.utils.b.d = r6
            com.tencent.assistant.utils.b.f1828a = r0
            java.lang.String r0 = g()
            com.tencent.assistant.utils.b.b = r0
            java.lang.String r0 = h()
            com.tencent.assistant.utils.b.c = r0
            return
        L_0x0096:
            int r0 = (r3 > r0 ? 1 : (r3 == r0 ? 0 : -1))
            if (r0 <= 0) goto L_0x00c7
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00bf }
            r0.<init>()     // Catch:{ Exception -> 0x00bf }
            java.lang.StringBuilder r0 = r0.append(r8)     // Catch:{ Exception -> 0x00bf }
            java.lang.String r1 = "/BaoDownload"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x00bf }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x00bf }
            boolean r1 = b(r0)     // Catch:{ Exception -> 0x00bf }
            if (r1 == 0) goto L_0x00c7
            int r1 = (r3 > r10 ? 1 : (r3 == r10 ? 0 : -1))
            if (r1 <= 0) goto L_0x00c5
            r2 = r0
            r1 = r0
            goto L_0x004e
        L_0x00ba:
            java.lang.Object r0 = r5.first     // Catch:{ Exception -> 0x00bf }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x00bf }
            goto L_0x007d
        L_0x00bf:
            r0 = move-exception
            r0.printStackTrace()
        L_0x00c3:
            r0 = r2
            goto L_0x007d
        L_0x00c5:
            r1 = r0
            goto L_0x004e
        L_0x00c7:
            r1 = r2
            goto L_0x004e
        L_0x00c9:
            r1 = r5
            goto L_0x004e
        L_0x00cb:
            r3 = r0
            r1 = r2
            goto L_0x0064
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.utils.b.b():void");
    }

    private static Pair<String, Long> a(String str, List<String> list, Set<String> set) {
        String str2;
        long j;
        long j2 = 0;
        String str3 = null;
        List<String> e2 = w.e();
        if (e2 != null && e2.size() > 0) {
            for (String next : e2) {
                set.add(next);
                if (!next.equals(str)) {
                    String str4 = next + "/BaoDownload";
                    list.add(str4);
                    if (b(str4)) {
                        long a2 = a(next);
                        if (a2 > j2) {
                            str2 = str4;
                            j = a2;
                            j2 = j;
                            str3 = str2;
                        }
                    }
                }
                str2 = str3;
                j = j2;
                j2 = j;
                str3 = str2;
            }
        }
        String str5 = AstApp.i().getFilesDir().getAbsolutePath() + FileUtil.APP_SDCARD_UNAMOUNT_ROOT_PATH + FileUtil.APK_DIT_PATH;
        if (!TextUtils.isEmpty(str5)) {
            set.add(Environment.getDataDirectory().getPath());
            long c2 = t.c();
            list.add(str5);
            if (j2 < 104857600 && c2 > j2 && b(str5)) {
                str3 = str5;
                j2 = c2;
            }
        }
        return Pair.create(str3, Long.valueOf(j2));
    }

    public static List<String> c() {
        return d;
    }

    public static String d() {
        return f1828a;
    }

    public static long a(String str) {
        StatFs statFs = new StatFs(new File(str).getPath());
        long availableBlocks = (((long) statFs.getAvailableBlocks()) - 4) * ((long) statFs.getBlockSize());
        if (availableBlocks < 0) {
            return 0;
        }
        return availableBlocks;
    }

    public static String e() {
        return b;
    }

    public static String f() {
        return c;
    }

    private static String g() {
        String str;
        String d2 = d();
        if (d2.endsWith(FileUtil.APK_DIT_PATH)) {
            str = d2.substring(0, d2.lastIndexOf(FileUtil.APK_DIT_PATH)) + FileUtil.VIDEO_DIR_PATH;
        } else {
            str = d2 + FileUtil.VIDEO_DIR_PATH;
        }
        if (b(str)) {
            return str;
        }
        return null;
    }

    private static String h() {
        String str;
        String d2 = d();
        if (d2.endsWith(FileUtil.APK_DIT_PATH)) {
            str = d2.substring(0, d2.lastIndexOf(FileUtil.APK_DIT_PATH)) + FileUtil.FILE_DIR_PATH;
        } else {
            str = d2 + FileUtil.FILE_DIR_PATH;
        }
        if (b(str)) {
            return str;
        }
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x004f A[SYNTHETIC, Splitter:B:17:0x004f] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0057 A[SYNTHETIC, Splitter:B:22:0x0057] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean b(java.lang.String r4) {
        /*
            r1 = 0
            java.io.File r0 = new java.io.File     // Catch:{ IOException -> 0x0049 }
            r0.<init>(r4)     // Catch:{ IOException -> 0x0049 }
            boolean r2 = r0.exists()     // Catch:{ IOException -> 0x0049 }
            if (r2 != 0) goto L_0x000f
            r0.mkdirs()     // Catch:{ IOException -> 0x0049 }
        L_0x000f:
            java.io.RandomAccessFile r2 = new java.io.RandomAccessFile     // Catch:{ IOException -> 0x0049 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0049 }
            r0.<init>()     // Catch:{ IOException -> 0x0049 }
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ IOException -> 0x0049 }
            java.lang.String r3 = java.io.File.separator     // Catch:{ IOException -> 0x0049 }
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ IOException -> 0x0049 }
            java.lang.String r3 = ".bao"
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ IOException -> 0x0049 }
            java.lang.String r0 = r0.toString()     // Catch:{ IOException -> 0x0049 }
            java.lang.String r3 = "rw"
            r2.<init>(r0, r3)     // Catch:{ IOException -> 0x0049 }
            r0 = 10
            r2.setLength(r0)     // Catch:{ IOException -> 0x006d, all -> 0x006a }
            r0 = 0
            r2.seek(r0)     // Catch:{ IOException -> 0x006d, all -> 0x006a }
            r0 = 5
            byte[] r0 = new byte[r0]     // Catch:{ IOException -> 0x006d, all -> 0x006a }
            r0 = {0, 0, 0, 0, 0} // fill-array     // Catch:{ IOException -> 0x006d, all -> 0x006a }
            r2.write(r0)     // Catch:{ IOException -> 0x006d, all -> 0x006a }
            r0 = 1
            if (r2 == 0) goto L_0x0048
            r2.close()     // Catch:{ IOException -> 0x0065 }
        L_0x0048:
            return r0
        L_0x0049:
            r0 = move-exception
        L_0x004a:
            r0.printStackTrace()     // Catch:{ all -> 0x0054 }
            if (r1 == 0) goto L_0x0052
            r1.close()     // Catch:{ IOException -> 0x0060 }
        L_0x0052:
            r0 = 0
            goto L_0x0048
        L_0x0054:
            r0 = move-exception
        L_0x0055:
            if (r1 == 0) goto L_0x005a
            r1.close()     // Catch:{ IOException -> 0x005b }
        L_0x005a:
            throw r0
        L_0x005b:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x005a
        L_0x0060:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0052
        L_0x0065:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0048
        L_0x006a:
            r0 = move-exception
            r1 = r2
            goto L_0x0055
        L_0x006d:
            r0 = move-exception
            r1 = r2
            goto L_0x004a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.utils.b.b(java.lang.String):boolean");
    }
}
