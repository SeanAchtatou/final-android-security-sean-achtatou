package com.tencent.assistant.thumbnailCache;

import com.tencent.assistant.protocol.c;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;

/* compiled from: ProGuard */
public class j {

    /* renamed from: a  reason: collision with root package name */
    private DefaultHttpClient f1797a;

    public synchronized DefaultHttpClient a() {
        if (this.f1797a == null) {
            this.f1797a = c.a();
        }
        return this.f1797a;
    }

    public synchronized void b() {
        if (this.f1797a != null) {
            c.a((HttpClient) this.f1797a);
            c.b((HttpClient) this.f1797a);
        } else {
            try {
                this.f1797a = a();
            } catch (OutOfMemoryError e) {
                System.gc();
                try {
                    Thread.sleep(50);
                } catch (Exception e2) {
                }
                this.f1797a = a();
            }
        }
        return;
    }
}
