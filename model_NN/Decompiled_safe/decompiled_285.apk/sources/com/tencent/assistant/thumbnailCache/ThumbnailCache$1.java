package com.tencent.assistant.thumbnailCache;

import android.graphics.Bitmap;
import java.util.LinkedHashMap;
import java.util.Map;

/* compiled from: ProGuard */
class ThumbnailCache$1 extends LinkedHashMap<String, Bitmap> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b f1786a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    ThumbnailCache$1(b bVar, int i, float f, boolean z) {
        super(i, f, z);
        this.f1786a = bVar;
    }

    /* access modifiers changed from: protected */
    public boolean removeEldestEntry(Map.Entry<String, Bitmap> entry) {
        if (this.f1786a.m / 1024 > this.f1786a.n) {
            if (!(entry == null || entry.getValue() == null)) {
                b.a(this.f1786a, (long) (entry.getValue().getHeight() * entry.getValue().getRowBytes()));
            }
            return true;
        } else if (size() <= this.f1786a.e) {
            return false;
        } else {
            synchronized (this.f1786a.b) {
                this.f1786a.b.put(entry.getKey(), new a(entry.getKey(), entry.getValue(), this.f1786a.c));
            }
            return true;
        }
    }
}
