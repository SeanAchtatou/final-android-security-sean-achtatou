package com.tencent.assistant.component.listener;

import android.content.Context;
import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.manager.i;
import com.tencent.assistant.protocol.jce.UserTaskCfg;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public abstract class OnUserTaskClickListener implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    protected int f680a = 0;

    public abstract void OnUserTaskClick(View view);

    public void onClick(View view) {
        doClick(view);
    }

    public void doClick(View view) {
        this.f680a = view.getId();
        switch (this.f680a) {
            case R.id.header_layout /*2131165492*/:
                a(view);
                break;
            case R.id.close_zone /*2131166060*/:
                a();
                break;
        }
        OnUserTaskClick(view);
    }

    /* access modifiers changed from: protected */
    public void a() {
        UserTaskCfg o = i.y().o();
        o.f1606a = 0;
        o.b = 1;
        i.y().b(o);
    }

    /* access modifiers changed from: protected */
    public void a(View view) {
        Context context = view.getContext();
        if (context instanceof BaseActivity) {
            BaseActivity baseActivity = (BaseActivity) context;
            int f = baseActivity.f();
            int m = baseActivity.m();
            String str = Constants.STR_EMPTY;
            if (view.getTag(R.id.tma_st_slot_tag) instanceof String) {
                str = (String) view.getTag(R.id.tma_st_slot_tag);
            }
            l.a(new STInfoV2(f, str, m, STConst.ST_DEFAULT_SLOT, 200));
        }
    }
}
