package com.tencent.assistant.component;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.listener.OnTMAClickListener;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.pangu.activity.DownloadActivity;

/* compiled from: ProGuard */
class j extends OnTMAClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Context f675a;
    final /* synthetic */ DownloadProgressButton b;

    j(DownloadProgressButton downloadProgressButton, Context context) {
        this.b = downloadProgressButton;
        this.f675a = context;
    }

    public void onTMAClick(View view) {
        Intent intent = new Intent(this.f675a, DownloadActivity.class);
        if (this.f675a instanceof BaseActivity) {
            intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, ((BaseActivity) this.f675a).f());
        }
        this.f675a.startActivity(intent);
    }
}
