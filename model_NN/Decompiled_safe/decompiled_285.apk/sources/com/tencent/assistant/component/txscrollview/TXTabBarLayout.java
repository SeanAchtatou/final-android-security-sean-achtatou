package com.tencent.assistant.component.txscrollview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.by;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: ProGuard */
public class TXTabBarLayout extends TXTabBarLayoutBase {
    public static final int TABITEM_TEXT_ID = 100;
    public static final int TABITEM_TIPS_TEXT_ID = 101;

    public TXTabBarLayout(Context context) {
        super(context);
    }

    public TXTabBarLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void setItemStringList(ArrayList<String> arrayList) {
        if (arrayList != null && arrayList.size() > 0) {
            ArrayList arrayList2 = new ArrayList();
            Iterator<String> it = arrayList.iterator();
            while (it.hasNext()) {
                String next = it.next();
                LinearLayout linearLayout = new LinearLayout(this.d);
                linearLayout.setBackgroundDrawable(this.d.getResources().getDrawable(R.drawable.navigation_bar_right_selector));
                linearLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
                linearLayout.setGravity(17);
                TextView textView = new TextView(this.d);
                textView.setText(next);
                textView.setTextSize(16.0f);
                textView.setGravity(17);
                textView.setId(100);
                linearLayout.addView(textView, new LinearLayout.LayoutParams(-2, -2));
                TextView textView2 = new TextView(this.d);
                textView2.setText(next);
                textView2.setTextSize(10.0f);
                textView2.setGravity(17);
                textView2.setTextColor(getResources().getColor(R.color.white));
                textView2.setId(TABITEM_TIPS_TEXT_ID);
                textView2.setBackgroundResource(R.drawable.bg_icon_cherry_red_normal);
                textView2.setVisibility(8);
                linearLayout.addView(textView2, new LinearLayout.LayoutParams(-2, -2));
                arrayList2.add(linearLayout);
            }
            setTabItemList(arrayList2);
        }
    }

    public void setTabItemSelected(int i) {
        super.setTabItemSelected(i);
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < this.f701a.size()) {
                TextView textView = (TextView) ((View) this.f701a.get(i3)).findViewById(100);
                if (i == i3) {
                    textView.setTextColor(this.d.getResources().getColor(R.color.second_tab_selected_color));
                } else {
                    textView.setTextColor(this.d.getResources().getColor(R.color.second_tab_unselected_color));
                }
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    public LinearLayout getTabItemView(int i) {
        if (this.f701a == null || i >= this.f701a.size() || i < 0) {
            return null;
        }
        return (LinearLayout) this.f701a.get(i);
    }

    /* access modifiers changed from: protected */
    public void a() {
        if (this.c != null && this.c.getParent() == this) {
            removeView(this.c);
            this.c = null;
        }
        this.c = new ImageView(this.d);
        this.c.setBackgroundColor(this.d.getResources().getColor(R.color.second_tab_index));
        int a2 = by.a(this.d, 3.0f);
        this.f = (double) ((((float) getWidth()) * 1.0f) / ((float) this.f701a.size()));
        this.g = this.f;
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams((int) this.g, a2);
        layoutParams.addRule(12);
        addView(this.c, layoutParams);
        a(this.e);
    }

    public void scrollCursor(int i, float f) {
        XLog.i("TXTabBarLayout", "position: " + i + " offset: " + f);
        layoutCursorView((int) ((((float) i) + f) * ((float) this.c.getWidth())));
    }
}
