package com.tencent.assistant.component.dialog;

import android.content.DialogInterface;
import com.tencent.assistant.AppConst;

/* compiled from: ProGuard */
final class h implements DialogInterface.OnCancelListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppConst.TwoBtnDialogInfo f654a;

    h(AppConst.TwoBtnDialogInfo twoBtnDialogInfo) {
        this.f654a = twoBtnDialogInfo;
    }

    public void onCancel(DialogInterface dialogInterface) {
        this.f654a.onCancell();
    }
}
