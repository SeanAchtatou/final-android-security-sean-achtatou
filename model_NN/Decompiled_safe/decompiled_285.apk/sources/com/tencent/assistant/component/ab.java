package com.tencent.assistant.component;

import com.tencent.assistant.localres.model.LocalApkInfo;

/* compiled from: ProGuard */
class ab implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f623a;
    final /* synthetic */ LocalApkInfo b;
    final /* synthetic */ aa c;

    ab(aa aaVar, int i, LocalApkInfo localApkInfo) {
        this.c = aaVar;
        this.f623a = i;
        this.b = localApkInfo;
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x007a  */
    /* JADX WARNING: Removed duplicated region for block: B:33:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r6 = this;
            r5 = 30
            r1 = 1
            int r0 = r6.f623a
            if (r0 == r1) goto L_0x000c
            int r0 = r6.f623a
            r2 = 2
            if (r0 != r2) goto L_0x0081
        L_0x000c:
            com.tencent.assistant.component.aa r0 = r6.c
            com.tencent.assistant.component.NormalErrorRecommendPage r0 = r0.f622a
            int r0 = r0.currentState
            if (r0 != r5) goto L_0x0081
            com.tencent.assistant.component.aa r0 = r6.c
            com.tencent.assistant.component.NormalErrorRecommendPage r0 = r0.f622a
            com.tencent.assistant.smartcard.component.NormalSmartcardAppListItem r0 = r0.recommendAppListItemView
            if (r0 == 0) goto L_0x0081
            com.tencent.assistant.component.aa r0 = r6.c
            com.tencent.assistant.component.NormalErrorRecommendPage r0 = r0.f622a
            com.tencent.assistant.smartcard.component.NormalSmartcardAppListItem r0 = r0.recommendAppListItemView
            java.util.ArrayList<com.tencent.assistant.model.SimpleAppModel> r0 = r0.i
            r2 = 0
            if (r0 == 0) goto L_0x0082
            java.util.Iterator r3 = r0.iterator()
        L_0x0031:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x0082
            java.lang.Object r0 = r3.next()
            com.tencent.assistant.model.SimpleAppModel r0 = (com.tencent.assistant.model.SimpleAppModel) r0
            if (r0 == 0) goto L_0x0031
            java.lang.String r4 = r0.c
            if (r4 == 0) goto L_0x0031
            java.lang.String r0 = r0.c
            com.tencent.assistant.localres.model.LocalApkInfo r4 = r6.b
            java.lang.String r4 = r4.mPackageName
            boolean r0 = r0.equals(r4)
            if (r0 == 0) goto L_0x0031
            java.lang.String r0 = "NormalErrorRecommendPage"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "onInstalledApkDataChanged, pkgname="
            java.lang.StringBuilder r2 = r2.append(r3)
            com.tencent.assistant.localres.model.LocalApkInfo r3 = r6.b
            java.lang.String r3 = r3.mPackageName
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = ", changedType="
            java.lang.StringBuilder r2 = r2.append(r3)
            int r3 = r6.f623a
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            com.tencent.assistant.utils.XLog.d(r0, r2)
            r0 = r1
        L_0x0078:
            if (r0 == 0) goto L_0x0081
            com.tencent.assistant.component.aa r0 = r6.c
            com.tencent.assistant.component.NormalErrorRecommendPage r0 = r0.f622a
            r0.setErrorType(r5)
        L_0x0081:
            return
        L_0x0082:
            r0 = r2
            goto L_0x0078
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.component.ab.run():void");
    }
}
