package com.tencent.assistant.component;

import com.tencent.assistant.activity.PhotoBackupNewActivity;
import com.tencent.assistant.component.WifiTransferMediaAdapter;
import com.tencent.assistant.component.txscrollview.TXGridView;

/* compiled from: ProGuard */
class ag implements WifiTransferMediaAdapter.IWifiTransferMediaAdapterListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PhotoBackupMiddleGridView f628a;

    ag(PhotoBackupMiddleGridView photoBackupMiddleGridView) {
        this.f628a = photoBackupMiddleGridView;
    }

    public void onWifiTransferMediaAdapterLoadDataFinish(WifiTransferMediaAdapter<?> wifiTransferMediaAdapter, int i) {
        TXGridView access$000 = this.f628a.mImageGridView;
        if (!(this.f628a.activity instanceof PhotoBackupNewActivity)) {
            return;
        }
        if (i <= 0) {
            access$000.setTipsView(this.f628a.createNoDataTipsView());
        } else if (!this.f628a.isFirstSet) {
            this.f628a.isFirstSet = true;
            access$000.setTipsView(null);
        }
    }

    public void onWifiTransferMediaAdapterLoaderDataOver() {
        this.f628a.updateCheckBox();
    }
}
