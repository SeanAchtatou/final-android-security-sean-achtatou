package com.tencent.assistant.component;

import android.widget.RelativeLayout;
import com.tencent.assistant.protocol.jce.GetRecommendAppListResponse;
import com.tencent.assistant.protocol.jce.RecommendAppInfo;
import com.tencent.assistant.protocol.jce.SimpleAppInfo;
import com.tencent.assistant.utils.XLog;
import java.util.ArrayList;

/* compiled from: ProGuard */
class x implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GetRecommendAppListResponse f726a;
    final /* synthetic */ int b;
    final /* synthetic */ int c;
    final /* synthetic */ String d;
    final /* synthetic */ w e;

    x(w wVar, GetRecommendAppListResponse getRecommendAppListResponse, int i, int i2, String str) {
        this.e = wVar;
        this.f726a = getRecommendAppListResponse;
        this.b = i;
        this.c = i2;
        this.d = str;
    }

    public void run() {
        int i;
        int i2;
        int i3;
        ArrayList<SimpleAppInfo> arrayList = null;
        ArrayList<RecommendAppInfo> arrayList2 = this.f726a != null ? this.f726a.b : null;
        if (this.f726a != null) {
            arrayList = this.f726a.d;
        }
        StringBuilder append = new StringBuilder().append("initRecommendApp end, errorCode=").append(this.b).append(", recommendAppList.size=");
        if (arrayList2 == null || arrayList2.isEmpty()) {
            i = 0;
        } else {
            i = arrayList2.size();
        }
        XLog.d("NormalErrorRecommendPage", append.append(i).toString());
        if (this.e.f725a.recommendAppView != null) {
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.e.f725a.errorHintLayout.getLayoutParams();
            if (arrayList2 == null || arrayList2.isEmpty()) {
                this.e.f725a.recommendLayout.setVisibility(8);
                if (this.e.f725a.errorHintLayout.getChildCount() > 0) {
                    layoutParams.addRule(12);
                }
            } else {
                XLog.d("NormalErrorRecommendPage", "initRecommendApp end, recommendAppList=" + this.e.f725a.getTestMsg(arrayList2));
                ArrayList access$600 = this.e.f725a.filterRecommendAppInfoListByInstalledList(arrayList2);
                ArrayList access$700 = this.e.f725a.filterSimpleAppInfoListByInstalledList(arrayList);
                StringBuilder append2 = new StringBuilder().append("initRecommendApp end, after filter recommendAppList.size=");
                if (access$600 == null || access$600.isEmpty()) {
                    i2 = 0;
                } else {
                    i2 = access$600.size();
                }
                XLog.d("NormalErrorRecommendPage", append2.append(i2).toString());
                XLog.d("NormalErrorRecommendPage", "initRecommendApp end, after filter recommendAppList=" + this.e.f725a.getTestMsg(access$600));
                if (access$600.size() >= 3) {
                    if (this.e.f725a.mEngineMap.get(this.c, 0) != 0) {
                        i3 = this.e.f725a.mEngineMap.get(this.c);
                    } else {
                        i3 = 3;
                    }
                    int access$900 = this.e.f725a.getRecommandTypeByErrorSence(i3);
                    this.e.f725a.recommendAppView.a(access$900, access$600, this.e.f725a.getRecommendTitle(access$900, this.d), access$700);
                    this.e.f725a.recommendLayout.setVisibility(0);
                }
                layoutParams.addRule(12, 0);
            }
            this.e.f725a.errorHintLayout.setLayoutParams(layoutParams);
            this.e.f725a.errorHintLayout.setVisibility(0);
        }
    }
}
