package com.tencent.assistant.component.txscrollview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXRefreshScrollViewBase;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.utils.bd;

/* compiled from: ProGuard */
public class TXGetMoreListView extends TXRefreshListView implements AbsListView.OnScrollListener {

    /* renamed from: a  reason: collision with root package name */
    protected TXRefreshScrollViewBase.RefreshState f691a = TXRefreshScrollViewBase.RefreshState.RESET;
    protected int b = 0;
    private final String u = "TXGetMoreListView";
    private IScrollListener v;
    private boolean w = true;
    private boolean x = false;

    public TXGetMoreListView(Context context) {
        super(context, TXScrollViewBase.ScrollMode.NONE);
    }

    public TXGetMoreListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public TXGetMoreListView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.h = null;
        l();
    }

    public void onRefreshComplete(boolean z, boolean z2) {
        if (this.f691a == TXRefreshScrollViewBase.RefreshState.REFRESHING) {
            if (z) {
                this.f691a = TXRefreshScrollViewBase.RefreshState.RESET;
            } else {
                this.f691a = TXRefreshScrollViewBase.RefreshState.REFRESH_LOAD_FINISH;
            }
        } else if (this.f691a == TXRefreshScrollViewBase.RefreshState.REFRESH_LOAD_FINISH) {
            this.f691a = TXRefreshScrollViewBase.RefreshState.RESET;
        } else if (this.f691a == TXRefreshScrollViewBase.RefreshState.RESET && !z) {
            this.f691a = TXRefreshScrollViewBase.RefreshState.REFRESH_LOAD_FINISH;
        }
        a(z2);
    }

    public void onRefreshComplete(boolean z) {
        onRefreshComplete(z, true);
    }

    public void addHeaderView(View view) {
        ((ListView) this.s).addHeaderView(view);
    }

    public void removeHeaderView(View view) {
        ((ListView) this.s).removeHeaderView(view);
    }

    public int getHeaderViewsCount() {
        return ((ListView) this.s).getHeaderViewsCount();
    }

    public void addFooterView(TXLoadingLayoutBase tXLoadingLayoutBase) {
        if (tXLoadingLayoutBase != null) {
            if (this.e != null) {
                ((ListView) this.s).removeFooterView(this.e);
            }
            this.e = tXLoadingLayoutBase;
            ((ListView) this.s).addFooterView(this.e);
            this.e.setVisibility(0);
            c_();
        }
    }

    /* access modifiers changed from: protected */
    public void c_() {
        a(true);
    }

    public void setNeedShowSeaLevel(boolean z) {
        this.w = z;
    }

    /* access modifiers changed from: protected */
    public void a(boolean z) {
        if (this.e != null) {
            switch (f.f708a[this.f691a.ordinal()]) {
                case 1:
                    if (z) {
                        this.e.loadSuc();
                        return;
                    } else {
                        this.e.loadFail();
                        return;
                    }
                case 2:
                    int i = 0;
                    if (getRawAdapter() != null) {
                        i = getRawAdapter().getCount();
                    }
                    if (this.w) {
                        this.e.loadFinish(bd.b(getContext(), i));
                        return;
                    } else {
                        this.e.loadFinish(AstApp.i().getString(R.string.refresh_list_loading_loadfinish));
                        return;
                    }
                case 3:
                    this.e.refreshing();
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ListView b(Context context) {
        ListView listView = new ListView(context);
        if (this.o != TXScrollViewBase.ScrollMode.NONE) {
            this.e = a(context, TXScrollViewBase.ScrollMode.PULL_FROM_END);
            this.e.setVisibility(0);
            listView.addFooterView(this.e);
            listView.setOnScrollListener(this);
        }
        return listView;
    }

    public void onScrollStateChanged(AbsListView absListView, int i) {
        this.b = i;
        if (this.v != null) {
            this.v.onScrollStateChanged(absListView, i);
        }
        if (i == 0 && this.x && this.f691a == TXRefreshScrollViewBase.RefreshState.RESET) {
            if (this.k != null) {
                this.k.onTXRefreshListViewRefresh(TXScrollViewBase.ScrollState.ScrollState_FromEnd);
            }
            this.f691a = TXRefreshScrollViewBase.RefreshState.REFRESHING;
            c_();
        }
    }

    public boolean isScrollStateIdle() {
        return this.b == 0;
    }

    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
        if (this.s != null) {
            this.x = f();
            if (this.v != null) {
                this.v.onScroll(absListView, i, i2, i3);
            }
        }
    }

    public void addClickLoadMore() {
        this.e.setOnClickListener(new e(this));
    }

    public void reset() {
        this.f691a = TXRefreshScrollViewBase.RefreshState.RESET;
        this.b = 0;
    }

    public void setIScrollerListener(IScrollListener iScrollListener) {
        this.v = iScrollListener;
    }
}
