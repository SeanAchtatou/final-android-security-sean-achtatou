package com.tencent.assistant.component;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.component.DownloadProgressBar;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.manager.a;
import com.tencent.assistant.manager.b;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.d;
import com.tencent.assistant.module.k;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ah;
import com.tencent.assistantv2.st.model.STCommonInfo;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.download.SimpleDownloadInfo;
import com.tencent.pangu.manager.DownloadProxy;

/* compiled from: ProGuard */
public class AppIconView extends RelativeLayout implements b {
    private long cgyId;
    /* access modifiers changed from: private */
    public IAppIconViewClickListener iconClickListener;
    private AppIconImageView mAppIcon;
    /* access modifiers changed from: private */
    public SimpleAppModel mAppModel;
    /* access modifiers changed from: private */
    public Context mContext;
    private LayoutInflater mInflater;
    /* access modifiers changed from: private */
    public DownloadProgressBar mProgressBar;
    private boolean showDownloadProgressBar;
    /* access modifiers changed from: private */
    public STCommonInfo statInfo;

    /* compiled from: ProGuard */
    public interface IAppIconViewClickListener {
        void onClick();
    }

    public AppIconView(Context context) {
        this(context, null);
    }

    public void setInvalidater(IViewInvalidater iViewInvalidater) {
        this.mAppIcon.setInvalidater(iViewInvalidater);
    }

    public AppIconView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.statInfo = null;
        this.cgyId = -100;
        this.showDownloadProgressBar = false;
        this.mContext = context;
        this.mInflater = LayoutInflater.from(context);
        initView();
    }

    public void setShowDownloadProgressBar(boolean z) {
        this.showDownloadProgressBar = z;
    }

    private void initView() {
        this.mInflater.inflate((int) R.layout.component_appicon, this);
        this.mAppIcon = (AppIconImageView) findViewById(R.id.icon_img);
        if (this.showDownloadProgressBar) {
            this.mProgressBar = (DownloadProgressBar) findViewById(R.id.download_progress);
        }
    }

    private void setSimpleAppModel(SimpleAppModel simpleAppModel, STCommonInfo sTCommonInfo, long j, String str) {
        if (simpleAppModel != null) {
            this.mAppModel = simpleAppModel;
            this.statInfo = sTCommonInfo;
            this.cgyId = j;
            initIcon();
            this.mAppIcon.setTag(str);
            if (this.showDownloadProgressBar) {
                initProgress();
                a.a().a(str, this);
            }
        }
    }

    public void setSimpleAppModel(SimpleAppModel simpleAppModel, STCommonInfo sTCommonInfo, long j) {
        if (simpleAppModel != null) {
            setSimpleAppModel(simpleAppModel, sTCommonInfo, j, simpleAppModel.q());
        }
    }

    public void setSimpleAppModel(SimpleAppModel simpleAppModel, STCommonInfo sTCommonInfo, long j, d dVar) {
        if (simpleAppModel != null) {
            setSimpleAppModel(simpleAppModel, sTCommonInfo, j, dVar == null ? simpleAppModel.q() : dVar.f942a);
        }
    }

    private void initIcon() {
        if (TextUtils.isEmpty(this.mAppModel.e)) {
            this.mAppIcon.setImageDrawable(this.mContext.getResources().getDrawable(R.drawable.pic_defaule));
        } else {
            this.mAppIcon.updateImageView(this.mAppModel.e, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
        }
        if (this.showDownloadProgressBar) {
            this.mAppIcon.setOnClickListener(new a(this));
            updateIcon(k.d(this.mAppModel));
            return;
        }
        this.mAppIcon.setOnClickListener(new b(this));
    }

    private int getHeaderAppIconActionType() {
        AppConst.AppState d = k.d(this.mAppModel);
        if (d == AppConst.AppState.DOWNLOADING || d == AppConst.AppState.PAUSED || d == AppConst.AppState.QUEUING || d == AppConst.AppState.FAIL) {
            return com.tencent.assistantv2.st.page.a.a(d);
        }
        return 200;
    }

    private void updateIcon(AppConst.AppState appState) {
        if (appState == AppConst.AppState.DOWNLOADING || appState == AppConst.AppState.PAUSED || appState == AppConst.AppState.QUEUING || appState == AppConst.AppState.FAIL) {
            this.mAppIcon.setGray(true);
        } else {
            this.mAppIcon.setGray(false);
        }
    }

    private void initProgress() {
        String str = null;
        if (this.mAppModel != null) {
            str = this.mAppModel.q();
        }
        if (!TextUtils.isEmpty(str)) {
            AppConst.AppState d = k.d(this.mAppModel);
            switch (d.f645a[d.ordinal()]) {
                case 1:
                case 2:
                case 3:
                case 4:
                    DownloadInfo a2 = DownloadProxy.a().a(this.mAppModel);
                    if (a2 != null) {
                        updateProgressView(a2, d);
                        return;
                    }
                    return;
                default:
                    this.mProgressBar.setVisibility(8);
                    return;
            }
        } else {
            this.mProgressBar.setVisibility(8);
        }
    }

    /* access modifiers changed from: private */
    public void updateProgressView(DownloadInfo downloadInfo, AppConst.AppState appState) {
        boolean z = false;
        if (this.mProgressBar == null) {
            return;
        }
        if (appState == AppConst.AppState.DOWNLOADING || appState == AppConst.AppState.FAIL || appState == AppConst.AppState.PAUSED || appState == AppConst.AppState.QUEUING) {
            if (downloadInfo != null && downloadInfo.response != null) {
                this.mProgressBar.setVisibility(0);
                DownloadProgressBar.ProgressDownloadType progressDownloadType = DownloadProgressBar.ProgressDownloadType.DEFAULT;
                if (appState == AppConst.AppState.DOWNLOADING) {
                    progressDownloadType = DownloadProgressBar.ProgressDownloadType.DOWNLOADING;
                } else if (appState == AppConst.AppState.QUEUING) {
                    progressDownloadType = DownloadProgressBar.ProgressDownloadType.QUEUING;
                }
                if (appState == AppConst.AppState.DOWNLOADING || (downloadInfo.response.b > 0 && (appState == AppConst.AppState.PAUSED || appState == AppConst.AppState.FAIL))) {
                    z = true;
                }
                if (!z || downloadInfo.response.f <= SimpleDownloadInfo.getPercent(downloadInfo)) {
                    this.mProgressBar.setProgressAndDownloading(SimpleDownloadInfo.getPercent(downloadInfo), progressDownloadType);
                } else {
                    this.mProgressBar.setProgressAndDownloading(downloadInfo.response.f, progressDownloadType);
                }
            }
        } else if (appState == AppConst.AppState.INSTALLED || appState == AppConst.AppState.DOWNLOADED || appState == AppConst.AppState.INSTALLING) {
            this.mProgressBar.setProgressAndDownloading(100, DownloadProgressBar.ProgressDownloadType.QUEUING);
            this.mProgressBar.setVisibility(8);
        } else {
            this.mProgressBar.setProgressAndDownloading(0, DownloadProgressBar.ProgressDownloadType.DEFAULT);
            this.mProgressBar.setVisibility(8);
        }
    }

    /* access modifiers changed from: private */
    public void updateAppIcon(String str, AppConst.AppState appState) {
        if (!TextUtils.isEmpty(str) && this.mProgressBar != null) {
            switch (d.f645a[appState.ordinal()]) {
                case 1:
                case 2:
                case 3:
                case 4:
                    this.mProgressBar.setVisibility(0);
                    break;
                default:
                    this.mProgressBar.setVisibility(8);
                    break;
            }
        }
        updateIcon(appState);
    }

    public void onAppStateChange(String str, AppConst.AppState appState) {
        XLog.d("voken", "button / ticket = " + this + " / " + str);
        if (this.showDownloadProgressBar && !TextUtils.isEmpty(str) && this.mAppModel != null && this.mAppModel.q().equals(str)) {
            ah.a().post(new c(this, str, appState));
        }
    }

    public void setIconClickListener(IAppIconViewClickListener iAppIconViewClickListener) {
        this.iconClickListener = iAppIconViewClickListener;
    }

    public ImageView getIconImageView() {
        return this.mAppIcon;
    }
}
