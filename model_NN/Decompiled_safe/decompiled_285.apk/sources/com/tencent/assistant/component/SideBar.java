package com.tencent.assistant.component;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.utils.by;

/* compiled from: ProGuard */
public class SideBar extends View {
    private static final int SIDE_BAR_TOP_PADDING = 7;
    public static String[] b = {"#", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
    private int choose = -1;
    private Context mContext;
    private TextView mTextDialog;
    private OnTouchingLetterChangedListener onTouchingLetterChangedListener;
    private Paint paint = new Paint();
    private int topPaddingPx;

    /* compiled from: ProGuard */
    public interface OnTouchingLetterChangedListener {
        void onTouchingLetterChanged(String str);
    }

    public void setTextView(TextView textView) {
        this.mTextDialog = textView;
    }

    public SideBar(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.topPaddingPx = by.a(context, 7.0f);
        this.mContext = context;
    }

    public SideBar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.topPaddingPx = by.a(context, 7.0f);
        this.mContext = context;
    }

    public SideBar(Context context) {
        super(context);
        this.topPaddingPx = by.a(context, 7.0f);
        this.mContext = context;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int height = getHeight() - (this.topPaddingPx * 2);
        int width = getWidth();
        int length = height / b.length;
        for (int i = 0; i < b.length; i++) {
            this.paint.setColor(-1);
            this.paint.setTypeface(Typeface.DEFAULT_BOLD);
            this.paint.setAntiAlias(true);
            this.paint.setTextSize(this.mContext.getResources().getDimension(R.dimen.app_admin_side_bar_text));
            canvas.drawText(b[i], ((float) (width / 2)) - (this.paint.measureText(b[i]) / 2.0f), (float) ((length * i) + length + this.topPaddingPx), this.paint);
            this.paint.reset();
        }
    }

    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        float y = motionEvent.getY();
        int i = this.choose;
        OnTouchingLetterChangedListener onTouchingLetterChangedListener2 = this.onTouchingLetterChangedListener;
        int height = (int) ((y / ((float) getHeight())) * ((float) b.length));
        switch (action) {
            case 1:
            case 3:
                this.choose = -1;
                if (this.mTextDialog != null) {
                    this.mTextDialog.setVisibility(4);
                }
                invalidate();
                return true;
            case 2:
            default:
                if (i == height || height < 0 || height >= b.length) {
                    return true;
                }
                if (onTouchingLetterChangedListener2 != null) {
                    onTouchingLetterChangedListener2.onTouchingLetterChanged(b[height]);
                }
                if (this.mTextDialog != null) {
                    this.mTextDialog.setText(b[height]);
                    this.mTextDialog.setVisibility(0);
                }
                this.choose = height;
                invalidate();
                return true;
        }
    }

    public void setOnTouchingLetterChangedListener(OnTouchingLetterChangedListener onTouchingLetterChangedListener2) {
        this.onTouchingLetterChangedListener = onTouchingLetterChangedListener2;
    }

    public void hideTextDialog() {
        if (this.mTextDialog != null) {
            this.mTextDialog.setVisibility(4);
        }
    }
}
