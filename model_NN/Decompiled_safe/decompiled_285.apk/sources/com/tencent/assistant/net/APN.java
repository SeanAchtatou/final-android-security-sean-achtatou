package com.tencent.assistant.net;

import com.qq.taf.jce.JceStruct;

/* compiled from: ProGuard */
public enum APN {
    UN_DETECT,
    WIFI,
    CMWAP,
    CMNET,
    UNIWAP,
    UNINET,
    WAP3G,
    NET3G,
    CTWAP,
    CTNET,
    UNKNOWN,
    UNKNOW_WAP,
    NO_NETWORK,
    WAP4G,
    NET4G;

    public byte getIntValue() {
        switch (a.f1058a[ordinal()]) {
            case 1:
                return 0;
            case 2:
                return 1;
            case 3:
                return 2;
            case 4:
                return 3;
            case 5:
                return 4;
            case 6:
                return 5;
            case 7:
                return 6;
            case 8:
                return 7;
            case 9:
                return 8;
            case 10:
                return 9;
            case 11:
            default:
                return 10;
            case 12:
                return JceStruct.STRUCT_END;
            case 13:
                return JceStruct.ZERO_TAG;
        }
    }
}
