package com.tencent.assistant.manager.notification.a;

import android.graphics.Bitmap;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.manager.notification.a.a.e;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
class j implements e {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ i f872a;

    j(i iVar) {
        this.f872a = iVar;
    }

    public void a(Bitmap bitmap) {
        if (bitmap != null && !bitmap.isRecycled()) {
            if (this.f872a.c.f.f1445a == 6) {
                this.f872a.k.setViewVisibility(R.id.image_normal, 8);
                this.f872a.k.setViewVisibility(R.id.image_large, 0);
                this.f872a.k.setImageViewBitmap(R.id.image_large, bitmap);
                try {
                    Bitmap createBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight() / 2);
                    if (createBitmap != null) {
                        this.f872a.i.setViewVisibility(R.id.image_normal, 0);
                        this.f872a.i.setViewVisibility(R.id.image_large, 8);
                        this.f872a.i.setImageViewBitmap(R.id.image_normal, createBitmap);
                    }
                } catch (Throwable th) {
                    XLog.w(i.p, "createBitmap Exception", th);
                }
            } else {
                this.f872a.i.setViewVisibility(R.id.image_normal, 0);
                this.f872a.i.setViewVisibility(R.id.image_large, 8);
                this.f872a.i.setImageViewBitmap(R.id.image_normal, bitmap);
            }
        }
    }
}
