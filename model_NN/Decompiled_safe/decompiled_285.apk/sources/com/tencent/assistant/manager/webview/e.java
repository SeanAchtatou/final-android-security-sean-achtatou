package com.tencent.assistant.manager.webview;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.webkit.ValueCallback;
import com.tencent.assistant.manager.webview.component.d;
import com.tencent.assistant.manager.webview.js.JsBridge;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.bm;
import com.tencent.pangu.link.b;
import com.tencent.smtt.export.external.interfaces.ConsoleMessage;
import com.tencent.smtt.sdk.WebChromeClient;
import com.tencent.smtt.sdk.WebView;

/* compiled from: ProGuard */
public class e extends WebChromeClient {
    private static final String c = e.class.getName();

    /* renamed from: a  reason: collision with root package name */
    protected d f917a = null;
    private ValueCallback<Uri> b;
    private Context d = null;
    private JsBridge e = null;
    private com.tencent.assistant.manager.webview.component.e f = null;

    public e(Context context, JsBridge jsBridge, com.tencent.assistant.manager.webview.component.e eVar, d dVar) {
        this.d = context;
        this.e = jsBridge;
        this.f = eVar;
        this.f917a = dVar;
    }

    public void onReceivedTitle(WebView webView, String str) {
        if (this.f917a != null) {
            this.f917a.a(webView, str);
        }
    }

    public void openFileChooser(ValueCallback<Uri> valueCallback, String str, String str2) {
        a(valueCallback);
    }

    public void onProgressChanged(WebView webView, int i) {
        if (this.f917a != null) {
            this.f917a.a(webView, i);
        }
    }

    public ValueCallback<Uri> a() {
        return this.b;
    }

    public void b() {
        this.b = null;
    }

    private void a(ValueCallback<Uri> valueCallback) {
        Activity a2;
        try {
            this.b = valueCallback;
            Intent intent = new Intent("android.intent.action.GET_CONTENT");
            intent.addCategory("android.intent.category.OPENABLE");
            intent.setType("*/*");
            if (this.f917a != null && (a2 = this.f917a.a()) != null) {
                a2.startActivityForResult(Intent.createChooser(intent, "请选择一个文件"), 100);
            }
        } catch (Exception e2) {
        }
    }

    public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
        XLog.i("Jie", "[onConsoleMessage] ---> 111");
        if (consoleMessage == null) {
            return false;
        }
        return a(consoleMessage.message());
    }

    private boolean a(String str) {
        boolean z = true;
        XLog.i("Jie", "handleConsoleMessage --- url = " + str);
        if (TextUtils.isEmpty(str)) {
            XLog.i("Jie", "Interface url is empty");
            return false;
        } else if (str.startsWith("http") || str.startsWith("https")) {
            return false;
        } else {
            if (str.startsWith(JsBridge.JS_BRIDGE_SCHEME)) {
                XLog.i("Jie", "Interface request:" + str);
                if (this.e != null) {
                    this.e.invoke(str);
                }
                return true;
            } else if (str.equals("about:blank;") || str.equals("about:blank")) {
                if (Build.VERSION.SDK_INT >= 11) {
                    z = false;
                }
                return z;
            } else {
                Uri parse = Uri.parse(str);
                Intent intent = new Intent("android.intent.action.VIEW", parse);
                if (!b.a(this.d, intent)) {
                    return false;
                }
                String scheme = intent.getScheme();
                if (scheme == null || !scheme.equals("tmast")) {
                    intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, this.f.a());
                    this.d.startActivity(intent);
                } else {
                    Bundle bundle = new Bundle();
                    int a2 = bm.a(parse.getQueryParameter("scene"), 0);
                    if (a2 != 0) {
                        bundle.putInt(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, a2);
                    } else {
                        bundle.putInt(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, this.f.a());
                    }
                    b.b(this.d, str, bundle);
                }
                return true;
            }
        }
    }
}
