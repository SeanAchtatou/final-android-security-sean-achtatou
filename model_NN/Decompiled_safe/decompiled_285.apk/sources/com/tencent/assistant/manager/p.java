package com.tencent.assistant.manager;

import android.content.Context;
import android.content.IntentFilter;
import com.tencent.assistant.receiver.SdCardEventReceiver;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;

/* compiled from: ProGuard */
public class p {

    /* renamed from: a  reason: collision with root package name */
    protected ReferenceQueue<q> f902a = new ReferenceQueue<>();
    protected ConcurrentLinkedQueue<WeakReference<q>> b = new ConcurrentLinkedQueue<>();
    private SdCardEventReceiver c = new SdCardEventReceiver();

    protected p() {
    }

    /* access modifiers changed from: protected */
    public void a(q qVar) {
        if (qVar != null) {
            while (true) {
                Reference<? extends q> poll = this.f902a.poll();
                if (poll == null) {
                    break;
                }
                this.b.remove(poll);
            }
            Iterator<WeakReference<q>> it = this.b.iterator();
            while (it.hasNext()) {
                if (((q) it.next().get()) == qVar) {
                    return;
                }
            }
            this.b.add(new WeakReference(qVar, this.f902a));
        }
    }

    /* access modifiers changed from: protected */
    public void b(q qVar) {
        if (qVar != null) {
            Iterator<WeakReference<q>> it = this.b.iterator();
            while (it.hasNext()) {
                WeakReference next = it.next();
                if (((q) next.get()) == qVar) {
                    this.b.remove(next);
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(Context context) {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.MEDIA_BAD_REMOVAL");
        intentFilter.addAction("android.intent.action.MEDIA_EJECT");
        intentFilter.addAction("android.intent.action.MEDIA_MOUNTED");
        intentFilter.addAction("android.intent.action.MEDIA_REMOVED");
        intentFilter.addAction("android.intent.action.MEDIA_UNMOUNTED");
        intentFilter.addDataScheme("file");
        try {
            context.getApplicationContext().registerReceiver(this.c, intentFilter);
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
        Iterator<WeakReference<q>> it = this.b.iterator();
        while (it.hasNext()) {
            q qVar = (q) it.next().get();
            if (qVar != null) {
                qVar.a();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void b() {
        Iterator<WeakReference<q>> it = this.b.iterator();
        while (it.hasNext()) {
            q qVar = (q) it.next().get();
            if (qVar != null) {
                qVar.b();
            }
        }
    }
}
