package com.tencent.assistant.manager;

import android.net.NetworkInfo;
import com.qq.AppService.AstApp;
import com.tencent.assistant.manager.NetworkMonitor;
import com.tencent.assistant.net.APN;
import com.tencent.assistant.net.c;

/* compiled from: ProGuard */
public class t {

    /* renamed from: a  reason: collision with root package name */
    private static t f905a = null;
    private NetworkMonitor b = new NetworkMonitor();
    private p c = new p();
    private j d = new j();

    private t() {
        this.b.a(AstApp.i());
        this.c.a(AstApp.i());
    }

    public static synchronized t a() {
        t tVar;
        synchronized (t.class) {
            if (f905a == null) {
                f905a = new t();
            }
            tVar = f905a;
        }
        return tVar;
    }

    public void a(NetworkMonitor.ConnectivityChangeListener connectivityChangeListener) {
        this.b.a(connectivityChangeListener);
    }

    public void b(NetworkMonitor.ConnectivityChangeListener connectivityChangeListener) {
        this.b.b(connectivityChangeListener);
    }

    public void a(k kVar) {
        this.d.a(kVar);
    }

    public void a(q qVar) {
        this.c.a(qVar);
    }

    public void b(q qVar) {
        this.c.b(qVar);
    }

    public void b() {
        this.d.a();
        System.gc();
    }

    public void a(NetworkInfo networkInfo) {
        if (networkInfo != null) {
            APN j = c.j();
            c.k();
            APN j2 = c.j();
            if (j == j2) {
                return;
            }
            if (j == APN.NO_NETWORK) {
                this.b.a(j2);
            } else if (j2 == APN.NO_NETWORK) {
                this.b.b(j);
            } else {
                this.b.a(j, j2);
            }
        }
    }

    public void a(boolean z) {
        if (z) {
            this.c.b();
        } else {
            this.c.a();
        }
    }
}
