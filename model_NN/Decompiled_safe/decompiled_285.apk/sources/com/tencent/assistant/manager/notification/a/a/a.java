package com.tencent.assistant.manager.notification.a.a;

/* compiled from: ProGuard */
public abstract class a {

    /* renamed from: a  reason: collision with root package name */
    protected boolean f865a = false;
    protected b b = null;

    public void a(b bVar) {
        this.b = bVar;
    }

    public boolean a() {
        return this.f865a;
    }

    /* access modifiers changed from: protected */
    public void b() {
        this.f865a = false;
        a(0);
    }

    /* access modifiers changed from: protected */
    public void a(int i) {
        this.f865a = true;
        if (this.b != null) {
            this.b.a(i);
        }
    }
}
