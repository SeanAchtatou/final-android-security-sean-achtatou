package com.tencent.assistant.manager.webview.component;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.webkit.ValueCallback;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.NormalErrorRecommendPage;
import com.tencent.assistant.manager.webview.js.JsBridge;
import com.tencent.assistant.net.c;
import com.tencent.assistant.utils.XLog;
import com.tencent.connect.common.Constants;
import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationTargetException;

/* compiled from: ProGuard */
public class TxWebViewContainer extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    private WeakReference<Activity> f909a;
    /* access modifiers changed from: private */
    public TxWebView b;
    private FrameLayout c;
    /* access modifiers changed from: private */
    public ProgressBar d;
    /* access modifiers changed from: private */
    public NormalErrorRecommendPage e;
    private JsBridge f;
    /* access modifiers changed from: private */
    public b g;
    /* access modifiers changed from: private */
    public i h;
    /* access modifiers changed from: private */
    public String i;

    public TxWebViewContainer(Context context) {
        this(context, null);
    }

    public TxWebViewContainer(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.i = Constants.STR_EMPTY;
        this.f909a = new WeakReference<>((Activity) context);
        q();
    }

    private void q() {
        XLog.i("TxWebViewContainer", ">>init>>");
        LayoutInflater.from(t()).inflate((int) R.layout.txwebview_layout, this);
        r();
    }

    private void r() {
        this.c = (FrameLayout) findViewById(R.id.webview_container);
        this.b = new TxWebView(t());
        this.c.addView(this.b);
        this.d = (ProgressBar) findViewById(R.id.loading_view);
        this.f = new JsBridge(t(), this.b, this);
        s();
    }

    private void s() {
        this.e = (NormalErrorRecommendPage) findViewById(R.id.error_page_view);
        this.e.setButtonClickListener(new f(this));
        this.e.setIsAutoLoading(true);
    }

    public void a(b bVar) {
        this.g = bVar;
        g gVar = new g(this);
        h hVar = new h(this);
        if (this.g != null && this.g.c) {
            this.d.setVisibility(4);
        }
        this.b.a(this.f, gVar, hVar, bVar);
    }

    public void a() {
        if (this.b != null) {
            this.b.c();
        }
        if (this.f != null) {
            this.f.onResume();
        }
    }

    public void b() {
        if (this.b != null) {
            this.b.b();
        }
        if (this.f != null) {
            this.f.onPause();
        }
    }

    public void c() {
        if (this.f != null) {
            this.f.recycle();
            this.f = null;
        }
        if (this.e != null) {
            this.e.destory();
        }
        if (this.b != null) {
            this.b.clearCache(false);
            this.b.destroy();
        }
    }

    public void d() {
        try {
            if (this.b != null) {
                this.c.removeAllViews();
                this.b.stopLoading();
                this.b.a();
                this.b = null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public boolean e() {
        return this.b.canGoBack();
    }

    public boolean f() {
        return this.b.canGoForward();
    }

    public void g() {
        a(true);
        this.b.goBack();
    }

    public void h() {
        a(true);
        this.b.goForward();
    }

    public void i() {
        a(true);
        this.b.reload();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.manager.webview.component.TxWebViewContainer.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.manager.webview.component.TxWebViewContainer.a(com.tencent.assistant.manager.webview.component.TxWebViewContainer, int):void
      com.tencent.assistant.manager.webview.component.TxWebViewContainer.a(java.lang.String, java.lang.String):void
      com.tencent.assistant.manager.webview.component.TxWebViewContainer.a(java.lang.String, boolean):void */
    public void a(String str) {
        a(str, true);
    }

    public void a(String str, boolean z) {
        if (z) {
            a(true);
        }
        try {
            if (TextUtils.isEmpty(str) || !str.trim().toLowerCase().startsWith("file:")) {
                this.i = str;
                this.b.loadUrl(this.i);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public ValueCallback<Uri> j() {
        return this.b.d();
    }

    public void k() {
        this.b.e();
    }

    public void a(int i2) {
        this.d.setProgress(i2);
    }

    public void l() {
        this.f.clickCallback();
    }

    public void m() {
        if (this.f != null) {
            this.f.updateStartLoadTime();
        }
    }

    public void a(String str, String str2) {
        this.f.responseFileChooser(str, str2);
    }

    public void showErrorPage(boolean z) {
        if (!z) {
            a(true);
        } else if (!c.a()) {
            c(30);
        } else {
            c(20);
        }
    }

    /* access modifiers changed from: private */
    public void c(int i2) {
        a(false);
        this.e.setErrorType(i2);
        this.e.setVisibility(0);
    }

    public void a(boolean z) {
        int i2 = 0;
        if (this.e != null) {
            this.e.setVisibility(z ? 4 : 0);
        }
        if (this.b != null) {
            TxWebView txWebView = this.b;
            if (!z) {
                i2 = 4;
            }
            txWebView.setVisibility(i2);
        }
    }

    public void n() {
        try {
            this.b.getClass().getMethod("setOverScrollMode", Integer.TYPE).invoke(this.b, 2);
        } catch (NoSuchMethodException e2) {
            e2.printStackTrace();
        } catch (IllegalArgumentException e3) {
            e3.printStackTrace();
        } catch (IllegalAccessException e4) {
            e4.printStackTrace();
        } catch (InvocationTargetException e5) {
            e5.printStackTrace();
        }
    }

    public boolean b(boolean z) {
        if (this.b != null) {
            return this.b.setVideoFullScreen(z);
        }
        return false;
    }

    public void o() {
        if (this.b != null) {
            Bundle bundle = new Bundle();
            bundle.putInt("DefaultVideoScreen", 2);
            bundle.putBoolean("supportLiteWnd", false);
            if (this.b.getX5WebViewExtension() != null) {
                this.b.getX5WebViewExtension().invokeMiscMethod("setVideoParams", bundle);
            }
        }
    }

    public TxWebView p() {
        return this.b;
    }

    public void a(i iVar) {
        this.h = iVar;
    }

    public void b(int i2) {
        if (this.b != null) {
            this.b.setBackgroundColor(i2);
        }
    }

    public void a(c cVar) {
        if (this.b != null && cVar != null) {
            this.b.a(cVar);
        }
    }

    /* access modifiers changed from: private */
    public Activity t() {
        Activity activity = this.f909a.get();
        if (activity == null) {
            return (Activity) getContext();
        }
        return activity;
    }
}
