package com.tencent.assistant.adapter;

import android.os.Message;
import com.tencent.assistant.utils.XLog;
import com.tencent.nucleus.manager.component.SwitchButton;
import com.tencent.nucleus.manager.root.e;
import com.tencent.pangu.utils.installuninstall.InstallUninstallUtil;

/* compiled from: ProGuard */
class ab implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SwitchButton f581a;
    final /* synthetic */ y b;

    ab(y yVar, SwitchButton switchButton) {
        this.b = yVar;
        this.f581a = switchButton;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.adapter.y.b(com.tencent.assistant.adapter.y, boolean):boolean
     arg types: [com.tencent.assistant.adapter.y, int]
     candidates:
      com.tencent.assistant.adapter.y.b(int, int):int
      com.tencent.assistant.adapter.y.b(com.tencent.assistant.adapter.y, boolean):boolean */
    public void run() {
        boolean z;
        boolean unused = this.b.j = true;
        boolean c = e.a().c();
        if (!c) {
            z = InstallUninstallUtil.a();
        } else {
            z = false;
        }
        XLog.d("SettingAdapter", "handle root switch, permRootReqResult=" + z + ", tempRootReqResult=" + c);
        Message obtainMessage = this.b.k.obtainMessage();
        obtainMessage.obj = this.f581a;
        obtainMessage.what = 1;
        if (z || c) {
            obtainMessage.arg1 = 1;
        } else {
            obtainMessage.arg1 = 0;
        }
        this.b.k.removeMessages(1);
        this.b.k.sendMessage(obtainMessage);
        boolean unused2 = this.b.j = false;
        this.b.c = false;
    }
}
