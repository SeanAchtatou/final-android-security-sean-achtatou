package com.tencent.assistant.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.k;
import com.tencent.assistant.utils.bm;
import com.tencent.assistant.utils.by;
import com.tencent.assistantv2.model.ItemElement;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.STPageInfo;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class ChildSettingAdapter extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    private Context f573a;
    private LayoutInflater b;
    private List<ItemElement> c = new ArrayList();
    private STInfoV2 d = null;
    private STPageInfo e = null;

    public ChildSettingAdapter(Context context) {
        this.f573a = context;
        if (this.f573a instanceof BaseActivity) {
            this.e = ((BaseActivity) this.f573a).n();
        } else {
            this.e = new STPageInfo();
        }
        this.b = LayoutInflater.from(context);
    }

    public void a(List<ItemElement> list) {
        if (list != null) {
            if (this.c == null) {
                this.c = new ArrayList();
            }
            this.c.clear();
            this.c.addAll(list);
        }
    }

    public int getCount() {
        if (this.c != null) {
            return this.c.size();
        }
        return 0;
    }

    public Object getItem(int i) {
        if (this.c == null || this.c.size() <= i) {
            return null;
        }
        return this.c.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public int getItemViewType(int i) {
        ItemElement itemElement = null;
        if (this.c != null && this.c.size() > i) {
            itemElement = this.c.get(i);
        }
        if (itemElement != null) {
            return itemElement.c;
        }
        return 1;
    }

    public int getViewTypeCount() {
        return 2;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        ItemElement itemElement = (ItemElement) getItem(i);
        if (itemElement != null) {
            switch (getItemViewType(i)) {
                case 0:
                    view = a(itemElement, view, i);
                    break;
            }
            view.setOnClickListener(new h(this, i));
        }
        return view;
    }

    private View a(ItemElement itemElement, View view, int i) {
        j jVar;
        if (view == null || !(view.getTag() instanceof j)) {
            view = this.b.inflate((int) R.layout.child_setting_item_checkbox, (ViewGroup) null);
            j jVar2 = new j(this);
            jVar2.f594a = view.findViewById(R.id.item_layout);
            jVar2.b = (TextView) view.findViewById(R.id.item_title);
            jVar2.c = (TextView) view.findViewById(R.id.item_description);
            jVar2.d = (LinearLayout) view.findViewById(R.id.item_left_layout);
            jVar2.e = (TextView) view.findViewById(R.id.check);
            jVar2.f = view.findViewById(R.id.item_line);
            view.setTag(jVar2);
            jVar = jVar2;
        } else {
            jVar = (j) view.getTag();
        }
        a(itemElement, jVar, i);
        return view;
    }

    private void a(ItemElement itemElement, j jVar, int i) {
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) jVar.f594a.getLayoutParams();
        marginLayoutParams.setMargins(marginLayoutParams.leftMargin, by.b((float) itemElement.e), marginLayoutParams.rightMargin, marginLayoutParams.bottomMargin);
        jVar.f594a.setLayoutParams(marginLayoutParams);
        jVar.b.setText(itemElement.f2018a);
        if (itemElement.b != null) {
            jVar.c.setText(itemElement.b);
            jVar.c.setVisibility(0);
            jVar.d.setPadding(0, by.a(this.f573a, 20.0f), 0, by.a(this.f573a, 17.0f));
        } else {
            jVar.c.setVisibility(8);
            jVar.d.setPadding(0, 0, 0, 0);
        }
        jVar.e.setSelected(k.a(itemElement.d));
        jVar.e.setOnClickListener(new i(this, jVar, itemElement, i));
        jVar.f594a.setBackgroundResource(R.drawable.helper_cardbg_all_selector_new);
        if (i == this.c.size() - 1) {
            jVar.f.setVisibility(8);
        } else if (i == 0) {
            jVar.f.setVisibility(0);
        } else {
            jVar.f.setVisibility(0);
        }
    }

    /* access modifiers changed from: private */
    public String a(int i) {
        return "03_" + bm.a(i + 1);
    }

    /* access modifiers changed from: private */
    public String a(boolean z) {
        if (z) {
            return "01";
        }
        return "02";
    }

    /* access modifiers changed from: private */
    public STInfoV2 a(String str, String str2, int i) {
        if (this.d == null) {
            this.d = new STInfoV2(this.e.f2060a, str, this.e.c, this.e.d, i);
        }
        this.d.status = str2;
        this.d.slotId = str;
        this.d.actionId = i;
        return this.d;
    }
}
