package com.tencent.assistant.adapter;

import android.content.Intent;
import android.view.View;
import com.tencent.assistant.activity.BrowserActivity;
import com.tencent.assistant.model.SimpleAppModel;

/* compiled from: ProGuard */
class s implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SimpleAppModel f603a;
    final /* synthetic */ RankClassicListAdapter b;

    s(RankClassicListAdapter rankClassicListAdapter, SimpleAppModel simpleAppModel) {
        this.b = rankClassicListAdapter;
        this.f603a = simpleAppModel;
    }

    public void onClick(View view) {
        Intent intent = new Intent(this.b.e, BrowserActivity.class);
        intent.putExtra("com.tencent.assistant.BROWSER_URL", this.f603a.Z);
        this.b.e.startActivity(intent);
    }
}
