package com.tencent.cloud.component;

import android.os.Handler;
import android.os.Message;
import com.tencent.assistant.module.k;

/* compiled from: ProGuard */
class t extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ UpdateIgnoreListView f2292a;

    t(UpdateIgnoreListView updateIgnoreListView) {
        this.f2292a = updateIgnoreListView;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.cloud.adapter.AppUpdateIgnoreListAdapter.a(java.util.List<com.tencent.assistant.model.SimpleAppModel>, boolean):void
     arg types: [java.util.List<com.tencent.assistant.model.SimpleAppModel>, int]
     candidates:
      com.tencent.cloud.adapter.AppUpdateIgnoreListAdapter.a(com.tencent.assistant.model.SimpleAppModel, int):com.tencent.assistantv2.st.page.STInfoV2
      com.tencent.cloud.adapter.AppUpdateIgnoreListAdapter.a(com.tencent.cloud.adapter.AppUpdateIgnoreListAdapter, java.lang.String):java.lang.String
      com.tencent.cloud.adapter.AppUpdateIgnoreListAdapter.a(com.tencent.cloud.adapter.AppUpdateIgnoreListAdapter, com.tencent.assistant.model.SimpleAppModel):void
      com.tencent.cloud.adapter.AppUpdateIgnoreListAdapter.a(com.tencent.cloud.adapter.b, int):void
      com.tencent.cloud.adapter.AppUpdateIgnoreListAdapter.a(java.util.List<com.tencent.assistant.model.SimpleAppModel>, boolean):void */
    public void handleMessage(Message message) {
        switch (message.what) {
            case 1:
                this.f2292a.f2268a.a(k.f(), true);
                return;
            case 2:
                this.f2292a.f2268a.a(k.f(), false);
                return;
            default:
                return;
        }
    }
}
