package com.tencent.cloud.component;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ExpandableListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.a.a;
import com.tencent.assistant.component.invalidater.ListViewScrollListener;
import com.tencent.assistant.component.txscrollview.TXExpandableListView;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.callback.h;
import com.tencent.assistant.module.k;
import com.tencent.assistant.module.update.j;
import com.tencent.assistant.protocol.jce.AppSimpleDetail;
import com.tencent.assistant.protocol.jce.StatUpdateManageAction;
import com.tencent.assistant.sdk.param.entity.BatchDownloadParam;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.at;
import com.tencent.assistant.utils.bm;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.assistantv2.st.page.STExternalInfo;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.cloud.adapter.g;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.manager.DownloadProxy;
import com.tencent.pangu.module.wisedownload.s;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: ProGuard */
public class UpdateListView extends RelativeLayout implements h {
    /* access modifiers changed from: private */
    public int A;
    /* access modifiers changed from: private */
    public int B;
    /* access modifiers changed from: private */
    public String C;
    private STExternalInfo D;
    private List<BatchDownloadParam> E;
    /* access modifiers changed from: private */
    public List<String> F;
    private Map<Long, Integer> G;
    private ListViewScrollListener H;
    private Handler I;

    /* renamed from: a  reason: collision with root package name */
    public g f2269a;
    /* access modifiers changed from: private */
    public Context b;
    private AstApp c;
    /* access modifiers changed from: private */
    public LayoutInflater d;
    private View e;
    /* access modifiers changed from: private */
    public TXExpandableListView f;
    private View g;
    private View h;
    private TextView i;
    private TextView j;
    private UpdateListFooterView k;
    /* access modifiers changed from: private */
    public RelativeLayout l;
    private TextView m;
    private TextView n;
    /* access modifiers changed from: private */
    public View o;
    /* access modifiers changed from: private */
    public CheckBox p;
    /* access modifiers changed from: private */
    public SimpleAppModel q;
    private StatUpdateManageAction r;
    /* access modifiers changed from: private */
    public int s;
    /* access modifiers changed from: private */
    public int t;
    private int u;
    private final int v;
    private final int w;
    private String x;
    private String y;
    private String z;

    /* compiled from: ProGuard */
    enum UpdateAllType {
        ALLDOWNLOADING,
        ALLUPDATED,
        NEEDUPDATE,
        NEEDSTARTDOWNLOAD
    }

    public UpdateListView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, null);
    }

    public UpdateListView(Context context, StatUpdateManageAction statUpdateManageAction) {
        this(context, null, statUpdateManageAction);
    }

    public UpdateListView(Context context, AttributeSet attributeSet, StatUpdateManageAction statUpdateManageAction) {
        super(context, attributeSet);
        this.l = null;
        this.m = null;
        this.n = null;
        this.o = null;
        this.p = null;
        this.q = null;
        this.s = 0;
        this.t = -1;
        this.v = 1;
        this.w = 2;
        this.x = Constants.STR_EMPTY;
        this.y = Constants.STR_EMPTY;
        this.z = Constants.STR_EMPTY;
        this.A = 0;
        this.B = 0;
        this.C = Constants.STR_EMPTY;
        this.E = null;
        this.F = null;
        this.G = null;
        this.H = new aa(this);
        this.I = new ab(this);
        this.r = statUpdateManageAction;
        this.A = 0;
        this.B = 0;
        this.c = AstApp.i();
        this.b = context;
        this.c.j();
        this.d = (LayoutInflater) this.b.getSystemService("layout_inflater");
        h();
    }

    private void h() {
        this.e = this.d.inflate((int) R.layout.updatelist_component, this);
        this.f = (TXExpandableListView) this.e.findViewById(R.id.updatelist);
        this.f.setGroupIndicator(null);
        this.f.setDivider(null);
        this.f.setChildDivider(null);
        this.f.setSelector(R.drawable.transparent_selector);
        this.f.setOnScrollListener(this.H);
        this.f.setOnGroupClickListener(new u(this));
        this.k = new UpdateListFooterView(this.b, TXScrollViewBase.ScrollDirection.SCROLL_DIRECTION_VERTICAL, TXScrollViewBase.ScrollMode.NONE);
        this.k.a(new v(this));
        this.f.addFooterView(this.k);
        this.f2269a = new g(this.b, this.f, this.r);
        this.f.setAdapter(this.f2269a);
        this.l = (RelativeLayout) findViewById(R.id.pop_bar);
        this.m = (TextView) findViewById(R.id.group_title);
        this.n = (TextView) findViewById(R.id.group_title_num);
        this.l.setOnClickListener(new w(this));
        this.g = this.e.findViewById(R.id.btnlayout);
        this.h = this.e.findViewById(R.id.sizeLayout);
        this.i = (TextView) this.e.findViewById(R.id.update_sumsize);
        this.j = (TextView) this.e.findViewById(R.id.update_savesize);
        this.h.setOnClickListener(new x(this));
        l();
    }

    /* access modifiers changed from: private */
    public void i() {
        long j2;
        String[] a2;
        int pointToPosition = this.f.pointToPosition(0, j() - 10);
        int pointToPosition2 = this.f.pointToPosition(0, 0);
        if (pointToPosition2 != -1) {
            long expandableListPosition = this.f.getExpandableListPosition(pointToPosition2);
            int packedPositionChild = ExpandableListView.getPackedPositionChild(expandableListPosition);
            int packedPositionGroup = ExpandableListView.getPackedPositionGroup(expandableListPosition);
            if (packedPositionChild == -1) {
                View expandChildAt = this.f.getExpandChildAt(pointToPosition2 - this.f.getFirstVisiblePosition());
                if (expandChildAt == null) {
                    this.u = 100;
                } else {
                    this.u = expandChildAt.getHeight();
                }
            }
            if (this.u != 0) {
                if (this.s > 0) {
                    this.t = packedPositionGroup;
                    Integer num = (Integer) this.f2269a.getGroup(this.t);
                    if (!(num == null || (a2 = this.f2269a.a(num.intValue())) == null)) {
                        try {
                            this.m.setText(a2[0]);
                            this.n.setText(" " + a2[1]);
                        } catch (Exception e2) {
                        }
                        this.l.setVisibility(0);
                    }
                }
                if (this.s == 0) {
                    this.l.setVisibility(8);
                }
                j2 = expandableListPosition;
            } else {
                return;
            }
        } else {
            j2 = 0;
        }
        if (this.t == -1) {
            return;
        }
        if (j2 == 0 && pointToPosition == 0) {
            this.l.setVisibility(8);
            return;
        }
        int j3 = j();
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) this.l.getLayoutParams();
        marginLayoutParams.topMargin = -(this.u - j3);
        this.l.setLayoutParams(marginLayoutParams);
    }

    private int j() {
        int i2 = this.u;
        int pointToPosition = this.f.pointToPosition(0, this.u);
        if (pointToPosition == -1 || ExpandableListView.getPackedPositionGroup(this.f.getExpandableListPosition(pointToPosition)) == this.t) {
            return i2;
        }
        View expandChildAt = this.f.getExpandChildAt(pointToPosition - this.f.getFirstVisiblePosition());
        if (expandChildAt != null) {
            return expandChildAt.getTop();
        }
        return 100;
    }

    public void a() {
        if (this.f2269a != null) {
            this.f2269a.notifyDataSetChanged();
        }
    }

    private void a(String str) {
        this.f2269a.a(this.G);
        Message obtainMessage = this.I.obtainMessage();
        obtainMessage.what = 1;
        this.I.removeMessages(1);
        this.I.sendMessage(obtainMessage);
    }

    public void a(String str, Intent intent) {
        String[] b2;
        if (intent != null) {
            this.x = intent.getStringExtra(a.m);
            this.y = intent.getStringExtra(a.n);
            this.z = intent.getStringExtra(a.o);
            this.A = intent.getIntExtra(a.S, 0);
            this.B = intent.getIntExtra(a.T, 0);
            Bundle extras = intent.getExtras();
            if (extras != null && extras.containsKey(a.Q)) {
                this.E = extras.getParcelableArrayList(a.Q);
            }
            if (extras != null && extras.containsKey(a.R)) {
                this.F = extras.getStringArrayList(a.R);
            }
            String stringExtra = intent.getStringExtra("sort_list");
            if (!TextUtils.isEmpty(stringExtra) && (b2 = bm.b(stringExtra, ",")) != null && b2.length > 0) {
                this.G = new HashMap(b2.length);
                for (int i2 = 0; i2 < b2.length; i2++) {
                    this.G.put(Long.valueOf(bm.c(b2[i2])), Integer.valueOf(i2));
                }
            }
        }
        a(str);
    }

    public void a(STExternalInfo sTExternalInfo, String str) {
        this.C = str;
        this.D = sTExternalInfo;
        if (this.f2269a != null) {
            this.f2269a.a(sTExternalInfo, str);
        }
    }

    public void b() {
        Message obtainMessage = this.I.obtainMessage();
        obtainMessage.what = 2;
        this.I.removeMessages(2);
        this.I.sendMessage(obtainMessage);
    }

    /* access modifiers changed from: private */
    public void k() {
        int i2 = k.i();
        int b2 = k.b(this.f2269a.b);
        this.k.b(b2 > 0 ? i2 > 0 ? 1 : 2 : 3);
        if (b2 > 0) {
            this.k.a(String.format(getResources().getString(R.string.view_ignorelist), Integer.valueOf(j.b().e().size())));
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00aa A[EDGE_INSN: B:37:0x00aa->B:31:0x00aa ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void l() {
        /*
            r7 = this;
            r1 = 1
            r2 = 0
            r6 = 8
            com.tencent.cloud.adapter.g r0 = r7.f2269a
            boolean r0 = r0.b
            java.util.List r3 = com.tencent.assistant.module.k.a(r0)
            if (r3 == 0) goto L_0x00aa
            int r0 = r3.size()
            if (r0 <= 0) goto L_0x00aa
            java.util.Iterator r4 = r3.iterator()
        L_0x0018:
            boolean r0 = r4.hasNext()
            if (r0 == 0) goto L_0x00aa
            java.lang.Object r0 = r4.next()
            com.tencent.assistant.model.SimpleAppModel r0 = (com.tencent.assistant.model.SimpleAppModel) r0
            com.tencent.assistant.AppConst$AppState r0 = com.tencent.assistant.module.k.d(r0)
            com.tencent.assistant.AppConst$AppState r5 = com.tencent.assistant.AppConst.AppState.DOWNLOAD
            if (r0 == r5) goto L_0x0040
            com.tencent.assistant.AppConst$AppState r5 = com.tencent.assistant.AppConst.AppState.DOWNLOADING
            if (r0 == r5) goto L_0x0040
            com.tencent.assistant.AppConst$AppState r5 = com.tencent.assistant.AppConst.AppState.UPDATE
            if (r0 == r5) goto L_0x0040
            com.tencent.assistant.AppConst$AppState r5 = com.tencent.assistant.AppConst.AppState.PAUSED
            if (r0 == r5) goto L_0x0040
            com.tencent.assistant.AppConst$AppState r5 = com.tencent.assistant.AppConst.AppState.FAIL
            if (r0 == r5) goto L_0x0040
            com.tencent.assistant.AppConst$AppState r5 = com.tencent.assistant.AppConst.AppState.QUEUING
            if (r0 != r5) goto L_0x0018
        L_0x0040:
            r0 = r1
        L_0x0041:
            if (r3 == 0) goto L_0x0095
            int r4 = r3.size()
            if (r4 <= 0) goto L_0x0095
            java.lang.String[] r0 = r7.a(r3, r0)
            if (r0 == 0) goto L_0x0080
            android.view.View r3 = r7.g
            r3.setVisibility(r2)
            android.view.View r3 = r7.h
            r3.setVisibility(r2)
            android.widget.TextView r3 = r7.i
            r3.setVisibility(r2)
            android.widget.TextView r3 = r7.i
            r4 = r0[r2]
            r3.setText(r4)
            r3 = r0[r1]
            boolean r3 = android.text.TextUtils.isEmpty(r3)
            if (r3 != 0) goto L_0x007a
            android.widget.TextView r3 = r7.j
            r3.setVisibility(r2)
            android.widget.TextView r2 = r7.j
            r0 = r0[r1]
            r2.setText(r0)
        L_0x0079:
            return
        L_0x007a:
            android.widget.TextView r0 = r7.j
            r0.setVisibility(r6)
            goto L_0x0079
        L_0x0080:
            android.view.View r0 = r7.g
            r0.setVisibility(r6)
            android.view.View r0 = r7.h
            r0.setVisibility(r6)
            android.widget.TextView r0 = r7.i
            r0.setVisibility(r6)
            android.widget.TextView r0 = r7.j
            r0.setVisibility(r6)
            goto L_0x0079
        L_0x0095:
            android.view.View r0 = r7.g
            r0.setVisibility(r6)
            android.widget.TextView r0 = r7.i
            r0.setVisibility(r6)
            android.widget.TextView r0 = r7.j
            r0.setVisibility(r6)
            android.view.View r0 = r7.h
            r0.setVisibility(r6)
            goto L_0x0079
        L_0x00aa:
            r0 = r2
            goto L_0x0041
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.cloud.component.UpdateListView.l():void");
    }

    private String[] a(List<SimpleAppModel> list, boolean z2) {
        String[] strArr = new String[2];
        if (!z2) {
            strArr[0] = Constants.STR_EMPTY;
            return strArr;
        } else if (list == null || list.size() <= 0) {
            return null;
        } else {
            long j2 = 0;
            long j3 = 0;
            for (SimpleAppModel next : list) {
                j2 += next.k;
                if (next.a()) {
                    if (!s.b(next)) {
                        j3 += next.v;
                    }
                } else if (!s.b(next) && !s.a(next)) {
                    j3 += next.k;
                }
                j3 = j3;
            }
            long j4 = j2 - j3;
            if (j4 > 10240) {
                strArr[0] = at.a(j3, 0);
                strArr[1] = " (" + getResources().getString(R.string.totalsave) + at.a(j4, 0) + ")";
            } else {
                strArr[0] = " (" + at.a(j3) + ")";
                strArr[1] = Constants.STR_EMPTY;
            }
            return strArr;
        }
    }

    /* access modifiers changed from: private */
    public UpdateAllType m() {
        List<SimpleAppModel> c2 = this.f2269a.c();
        int size = c2.size();
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        for (SimpleAppModel d2 : c2) {
            AppConst.AppState d3 = k.d(d2);
            if (d3 == AppConst.AppState.DOWNLOADING || d3 == AppConst.AppState.QUEUING) {
                i3++;
            } else if (d3 == AppConst.AppState.INSTALLED) {
                i2++;
            } else if (d3 == AppConst.AppState.UPDATE || d3 == AppConst.AppState.DOWNLOAD) {
                i4++;
            }
            i4 = i4;
            i3 = i3;
            i2 = i2;
        }
        if (i2 == size) {
            return UpdateAllType.ALLUPDATED;
        }
        if (i3 + i2 >= size) {
            return UpdateAllType.ALLDOWNLOADING;
        }
        if (i4 > 0) {
            return UpdateAllType.NEEDSTARTDOWNLOAD;
        }
        return UpdateAllType.NEEDUPDATE;
    }

    public int c() {
        List<SimpleAppModel> c2;
        if (this.f2269a == null || (c2 = this.f2269a.c()) == null) {
            return 0;
        }
        return c2.size();
    }

    public void a(Activity activity) {
        if (this.f2269a != null) {
            this.f2269a.a(activity);
        }
    }

    /* access modifiers changed from: private */
    public void n() {
        a(q());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.download.a.a(java.util.ArrayList<com.tencent.pangu.download.DownloadInfo>, boolean):void
     arg types: [java.util.ArrayList, int]
     candidates:
      com.tencent.pangu.download.a.a(com.tencent.pangu.download.a, java.util.List):java.util.List
      com.tencent.pangu.download.a.a(long, long):boolean
      com.tencent.pangu.download.a.a(com.tencent.pangu.download.a, boolean):boolean
      com.tencent.pangu.download.a.a(java.lang.String, int):boolean
      com.tencent.pangu.download.a.a(java.util.List<com.tencent.assistant.model.SimpleAppModel>, com.tencent.assistantv2.st.model.StatInfo):int
      com.tencent.pangu.download.a.a(com.tencent.assistant.model.SimpleAppModel, com.tencent.assistantv2.st.model.StatInfo):void
      com.tencent.pangu.download.a.a(com.tencent.pangu.download.DownloadInfo, com.tencent.pangu.download.SimpleDownloadInfo$UIType):void
      com.tencent.pangu.download.a.a(java.lang.String, com.tencent.cloud.a.f):void
      com.tencent.pangu.download.a.a(java.lang.String, java.lang.String):void
      com.tencent.pangu.download.a.a(com.tencent.pangu.download.DownloadInfo, boolean):boolean
      com.tencent.pangu.download.a.a(java.lang.String, boolean):boolean
      com.tencent.pangu.download.a.a(java.util.ArrayList<com.tencent.pangu.download.DownloadInfo>, boolean):void */
    public void a(List<SimpleAppModel> list) {
        if (list != null && list.size() != 0) {
            this.r.b = true;
            ArrayList arrayList = new ArrayList();
            StatInfo a2 = com.tencent.assistantv2.st.page.a.a(e());
            for (SimpleAppModel next : list) {
                if (next != null && !TextUtils.isEmpty(next.q())) {
                    DownloadInfo a3 = DownloadProxy.a().a(next);
                    a2.recommendId = next.y;
                    if (a3 != null && a3.needReCreateInfo(next)) {
                        DownloadProxy.a().b(a3.downloadTicket);
                        a3 = null;
                    }
                    if (this.B == 1 && a3 != null) {
                        a3.hostAppId = this.x;
                        a3.hostPackageName = this.y;
                        a3.hostVersionCode = this.z;
                    }
                    if (a3 == null) {
                        a3 = DownloadInfo.createDownloadInfo(next, a2);
                        a3.isUpdate = 1;
                    } else {
                        a3.updateDownloadInfoStatInfo(a2);
                    }
                    AppConst.AppState d2 = k.d(next);
                    if (d2 == AppConst.AppState.DOWNLOADED && a3.isSuccApkFileExist()) {
                        arrayList.add(a3);
                    } else if (d2 != AppConst.AppState.INSTALLING) {
                        com.tencent.pangu.download.a.a().a(a3);
                    }
                    this.f2269a.c.put(next.c.hashCode(), next.g);
                }
            }
            if (arrayList.size() > 0) {
                com.tencent.pangu.download.a.a((ArrayList<DownloadInfo>) arrayList, false);
            }
        }
    }

    /* access modifiers changed from: private */
    public void o() {
        for (int i2 = 0; i2 < this.f2269a.getGroupCount(); i2++) {
            this.f.expandGroup(i2);
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        return true;
    }

    public void onGetAppInfoSuccess(int i2, int i3, AppSimpleDetail appSimpleDetail) {
        if (appSimpleDetail != null) {
            this.q = k.a(appSimpleDetail);
            p();
            return;
        }
        Toast.makeText(AstApp.i(), this.b.getString(R.string.rec_create_fail), 0).show();
    }

    public void onGetAppInfoFail(int i2, int i3) {
        XLog.w("million", getClass().getSimpleName() + " onGetAppInfoFail...." + i3);
        Toast.makeText(AstApp.i(), this.b.getString(R.string.rec_create_fail), 0).show();
    }

    /* access modifiers changed from: private */
    public void p() {
        DownloadInfo downloadInfo;
        DownloadInfo downloadInfo2;
        if (this.q != null) {
            DownloadInfo a2 = DownloadProxy.a().a(this.q);
            StatInfo statInfo = new StatInfo(this.q.b, STConst.ST_PAGE_UPDATE_UPDATEALL, 0, null, 0);
            if (a2 == null || !a2.needReCreateInfo(this.q)) {
                downloadInfo = a2;
            } else {
                DownloadProxy.a().b(a2.downloadTicket);
                downloadInfo = null;
            }
            if (downloadInfo == null) {
                downloadInfo2 = DownloadInfo.createDownloadInfo(this.q, statInfo);
            } else {
                downloadInfo.updateDownloadInfoStatInfo(statInfo);
                downloadInfo2 = downloadInfo;
            }
            downloadInfo2.autoInstall = false;
            switch (ad.f2276a[k.d(this.q).ordinal()]) {
                case 1:
                case 2:
                case 3:
                case 4:
                    com.tencent.pangu.download.a.a().a(downloadInfo2);
                    break;
                case 5:
                    com.tencent.pangu.download.a.a().b(downloadInfo2);
                    break;
                case 6:
                    com.tencent.pangu.download.a.a().d(downloadInfo2);
                    break;
            }
            XLog.d("million", "UpdateListView after updateall download TencentMobileManager sucess");
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private List<SimpleAppModel> q() {
        switch (this.A) {
            case 0:
                break;
            case 1:
                k.a(this.f2269a.b);
                break;
            case 2:
                return k.i(this.E);
            default:
                return k.a(this.f2269a.b);
        }
        return k.a(this.f2269a.b);
    }

    public List<SimpleAppModel> d() {
        int i2;
        ArrayList arrayList = new ArrayList();
        if (this.B == 1 && (this.A == 1 || this.A == 0)) {
            return arrayList;
        }
        if (this.B == 0) {
            return arrayList;
        }
        if (this.E == null || this.E.size() <= 0) {
            return arrayList;
        }
        List<SimpleAppModel> q2 = q();
        if (!(q2 == null || k.f() == null)) {
            q2.addAll(k.f());
        }
        if (q2 == null || q2.size() <= 0) {
            for (BatchDownloadParam next : this.E) {
                SimpleAppModel simpleAppModel = new SimpleAppModel();
                try {
                    simpleAppModel.f938a = Long.valueOf(next.b).longValue();
                } catch (NumberFormatException e2) {
                    e2.printStackTrace();
                }
                simpleAppModel.c = next.d;
                simpleAppModel.D = Integer.valueOf(next.c).intValue();
                arrayList.add(simpleAppModel);
            }
            return arrayList;
        }
        for (BatchDownloadParam next2 : this.E) {
            if (next2 != null && !TextUtils.isEmpty(next2.d)) {
                int i3 = 0;
                while (true) {
                    i2 = i3;
                    if (i2 < q2.size() && (q2.get(i2) == null || !next2.d.equals(q2.get(i2).c))) {
                        i3 = i2 + 1;
                    }
                }
                if (i2 >= q2.size()) {
                    SimpleAppModel simpleAppModel2 = new SimpleAppModel();
                    try {
                        simpleAppModel2.f938a = Long.valueOf(next2.b).longValue();
                        simpleAppModel2.c = next2.d;
                        simpleAppModel2.D = Integer.valueOf(next2.c).intValue();
                        simpleAppModel2.ac = next2.h;
                    } catch (NumberFormatException e3) {
                        e3.printStackTrace();
                    }
                    arrayList.add(simpleAppModel2);
                }
            }
        }
        return arrayList;
    }

    public STInfoV2 e() {
        SimpleAppModel simpleAppModel;
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.b, 200);
        if (buildSTInfo != null) {
            buildSTInfo.pushInfo = this.C;
            buildSTInfo.updateWithExternalPara(this.D);
            List<SimpleAppModel> a2 = k.a(this.f2269a.b);
            if (!(a2 == null || a2.size() <= 0 || (simpleAppModel = a2.get(0)) == null)) {
                buildSTInfo.recommendId = simpleAppModel.y;
            }
        }
        return buildSTInfo;
    }

    public void f() {
        this.f2269a.d();
        this.h.setBackgroundResource(R.drawable.guanjia_style_btn_backround_big_selector);
    }

    public void g() {
        this.f2269a.e();
        this.h.setBackgroundResource(R.drawable.btn_green_selector);
    }
}
