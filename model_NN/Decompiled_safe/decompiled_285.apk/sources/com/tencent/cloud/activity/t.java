package com.tencent.cloud.activity;

import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.callback.a;
import com.tencent.assistant.protocol.jce.TagGroup;
import java.util.List;

/* compiled from: ProGuard */
class t implements a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TencentAppListActivity f2202a;

    t(TencentAppListActivity tencentAppListActivity) {
        this.f2202a = tencentAppListActivity;
    }

    public void a(int i, int i2, boolean z, List<SimpleAppModel> list, List<TagGroup> list2) {
        if (i2 == 0) {
            if (!(list == null || this.f2202a.w == null)) {
                this.f2202a.w.a(z, list, this.f2202a.n.g());
            }
            if (this.f2202a.w.getCount() > 0) {
                this.f2202a.w();
                this.f2202a.u.onRefreshComplete(this.f2202a.n.g(), true);
                return;
            }
            this.f2202a.b(10);
        } else if (!z) {
            this.f2202a.u.onRefreshComplete(this.f2202a.n.g(), false);
        } else if (-800 == i2) {
            this.f2202a.b(30);
        } else {
            this.f2202a.b(20);
        }
    }
}
