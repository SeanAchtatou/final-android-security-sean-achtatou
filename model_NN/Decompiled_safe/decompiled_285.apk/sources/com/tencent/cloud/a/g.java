package com.tencent.cloud.a;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.CheckBox;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.m;
import com.tencent.assistant.model.SimpleAppModel;

/* compiled from: ProGuard */
public class g extends AppConst.TwoBtnDialogInfo {

    /* renamed from: a  reason: collision with root package name */
    private CheckBox f2161a;
    private h b;

    public g(SimpleAppModel simpleAppModel, h hVar, String str) {
        this.b = hVar;
        str = TextUtils.isEmpty(str) ? AstApp.i().getResources().getString(R.string.applink_dialog_download) : str;
        String string = AstApp.i().getResources().getString(R.string.applink_dialog_download);
        if (simpleAppModel != null && simpleAppModel.u() == AppConst.AppState.UPDATE) {
            string = AstApp.i().getResources().getString(R.string.applink_dialog_update);
        }
        this.titleRes = String.format(AstApp.i().getResources().getString(R.string.applink_dialog_title), str);
        this.contentRes = String.format(AstApp.i().getResources().getString(R.string.applink_dialog_content), string, string, str);
        this.rBtnTxtRes = string;
        if (simpleAppModel != null) {
            this.rBtnTailTxtRes = " (" + simpleAppModel.s() + ")";
        }
        this.extraMsgView = LayoutInflater.from(AstApp.i()).inflate((int) R.layout.applink_dialog_checkbox, (ViewGroup) null);
        this.f2161a = (CheckBox) this.extraMsgView.findViewById(R.id.select_check);
    }

    public void onLeftBtnClick() {
    }

    public void onRightBtnClick() {
        if (this.b != null) {
            this.b.a();
        }
        if (this.f2161a.isChecked()) {
            m.a().B(false);
        }
    }

    public void onCancell() {
    }
}
