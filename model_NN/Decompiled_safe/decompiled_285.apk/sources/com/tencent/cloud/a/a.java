package com.tencent.cloud.a;

import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.utils.TemporaryThreadManager;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/* compiled from: ProGuard */
public class a {
    private static a c = null;

    /* renamed from: a  reason: collision with root package name */
    UIEventListener f2155a = new b(this);
    /* access modifiers changed from: private */
    public Map<String, Integer> b = Collections.synchronizedMap(new HashMap());

    public static synchronized a a() {
        a aVar;
        synchronized (a.class) {
            if (c == null) {
                c = new a();
            }
            aVar = c;
        }
        return aVar;
    }

    private a() {
        b();
    }

    private void b() {
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_GOFRONT, this.f2155a);
        AstApp.i().k().addUIEventListener(1032, this.f2155a);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_INSTALL, this.f2155a);
    }

    public void a(String str, String str2, String str3) {
        if (!TextUtils.isEmpty(str)) {
            this.b.put(str, Integer.valueOf(com.tencent.assistant.link.sdk.a.f794a));
            TemporaryThreadManager.get().start(new d(this, str, str2, str3));
        }
    }

    public void a(String str, String str2) {
        if (!TextUtils.isEmpty(str)) {
            this.b.remove(str);
            TemporaryThreadManager.get().start(new e(this, str, str2));
        }
    }
}
