package com.tencent.cloud.smartcard.b;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.k;
import com.tencent.assistant.protocol.jce.SmartCardPicDownloadNode;
import com.tencent.assistant.protocol.jce.SmartCardPicNode;
import com.tencent.assistant.protocol.jce.SmartCardPicTemplate;
import com.tencent.assistant.protocol.jce.SmartCardTitle;
import com.tencent.assistant.smartcard.d.a;
import com.tencent.assistant.smartcard.d.x;
import com.tencent.assistant.smartcard.d.y;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class d extends a {
    public List<y> e;
    private List<SmartCardPicNode> f;
    private byte g;
    private int h = 100;
    private boolean i = false;
    private int v;
    private int w;
    private boolean x = true;
    private String y;

    public boolean a(byte b, JceStruct jceStruct) {
        if (!(jceStruct instanceof SmartCardPicTemplate)) {
            return false;
        }
        SmartCardPicTemplate smartCardPicTemplate = (SmartCardPicTemplate) jceStruct;
        SmartCardTitle smartCardTitle = smartCardPicTemplate.b;
        this.j = b;
        if (smartCardTitle != null) {
            this.g = smartCardTitle.f1527a;
            this.l = smartCardTitle.b;
            this.p = smartCardTitle.c;
            this.o = smartCardTitle.d;
        }
        this.n = smartCardPicTemplate.c;
        this.k = smartCardPicTemplate.f1514a;
        this.f = smartCardPicTemplate.d;
        if (this.e == null) {
            this.e = new ArrayList();
        } else {
            this.e.clear();
        }
        if (smartCardPicTemplate.e != null) {
            Iterator<SmartCardPicDownloadNode> it = smartCardPicTemplate.e.iterator();
            while (it.hasNext()) {
                SmartCardPicDownloadNode next = it.next();
                y yVar = new y();
                yVar.b = next.b;
                yVar.f1768a = k.a(next.f1512a);
                if (yVar.f1768a != null) {
                    k.a(yVar.f1768a);
                }
                this.e.add(yVar);
            }
        }
        this.h = smartCardPicTemplate.b();
        this.i = smartCardPicTemplate.a();
        this.v = smartCardPicTemplate.c();
        this.w = smartCardPicTemplate.d();
        this.x = !smartCardPicTemplate.k;
        this.y = smartCardPicTemplate.f;
        return true;
    }

    public x c() {
        if (this.v <= 0 || this.w <= 0) {
            return null;
        }
        x xVar = new x();
        xVar.f = this.k;
        xVar.e = this.j;
        xVar.f1767a = this.v;
        xVar.b = this.w;
        return xVar;
    }

    public List<SimpleAppModel> a() {
        if (this.e == null || this.e.size() <= 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList(this.e.size());
        for (y yVar : this.e) {
            arrayList.add(yVar.f1768a);
        }
        return arrayList;
    }

    public List<SmartCardPicNode> h() {
        return this.f;
    }

    public String d_() {
        int i2 = 0;
        StringBuilder append = new StringBuilder().append(j()).append("_").append(this.f == null ? 0 : this.f.size()).append("_");
        if (this.e != null) {
            i2 = this.e.size();
        }
        return append.append(i2).toString();
    }

    public void a(List<Long> list) {
        long j;
        if (this.e != null && this.e.size() > 0) {
            Iterator<y> it = this.e.iterator();
            while (it.hasNext()) {
                y next = it.next();
                if (next == null || next.f1768a == null) {
                    j = -1;
                } else {
                    j = next.f1768a.f938a;
                }
                if (j != -1 && list.contains(Long.valueOf(j))) {
                    it.remove();
                }
            }
        }
    }

    public void b() {
        if (this.x && this.e != null && this.e.size() > 0) {
            Iterator<y> it = this.e.iterator();
            while (it.hasNext()) {
                y next = it.next();
                if (!(next.f1768a == null || ApkResourceManager.getInstance().getInstalledApkInfo(next.f1768a.c) == null)) {
                    it.remove();
                }
            }
        }
    }

    public byte f() {
        return this.g;
    }

    public int i() {
        return this.h;
    }

    public boolean m() {
        return this.i;
    }

    public String e() {
        return this.y;
    }
}
