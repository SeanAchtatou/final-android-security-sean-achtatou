package com.tencent.cloud.smartcard.c;

import android.util.Pair;
import com.tencent.assistant.smartcard.c.z;
import com.tencent.assistant.smartcard.d.n;
import com.tencent.assistant.smartcard.d.w;
import com.tencent.cloud.smartcard.b.d;
import java.util.List;

/* compiled from: ProGuard */
public class b extends z {
    public boolean a(n nVar, List<Long> list) {
        Pair<Boolean, w> b = b(nVar);
        if (nVar instanceof d) {
            if (!((d) nVar).m() && ((d) nVar).e != null && ((d) nVar).e.size() == 0) {
                return false;
            }
            if ((((d) nVar).e == null || ((d) nVar).e.size() == 0) && (((d) nVar).h() == null || ((d) nVar).h().size() == 0)) {
                return false;
            }
        }
        if (!((Boolean) b.first).booleanValue()) {
            return false;
        }
        return true;
    }
}
