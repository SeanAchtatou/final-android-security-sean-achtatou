package com.tencent.cloud.smartcard.view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.smartcard.component.NormalSmartcardBaseItem;
import com.tencent.assistant.smartcard.component.as;
import com.tencent.assistant.smartcard.d.n;
import com.tencent.assistant.utils.by;
import com.tencent.cloud.smartcard.b.a;
import com.tencent.cloud.smartcard.b.b;
import com.tencent.cloud.smartcard.component.ComplexContentItemView;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class NormalSmartCardComplexCollectionView extends NormalSmartcardBaseItem {
    private TextView i;
    private TextView l;
    private LinearLayout m;
    private List<ComplexContentItemView> n;

    public NormalSmartCardComplexCollectionView(Context context, n nVar, as asVar, IViewInvalidater iViewInvalidater) {
        super(context, nVar, asVar, iViewInvalidater);
        setBackgroundResource(R.drawable.common_card_normal);
    }

    /* access modifiers changed from: protected */
    public void a() {
        LayoutInflater.from(this.f1693a).inflate((int) R.layout.smartcard_collection_frame_layout, this);
        this.i = (TextView) findViewById(R.id.card_title);
        this.l = (TextView) findViewById(R.id.more_txt);
        this.m = (LinearLayout) findViewById(R.id.colleciton_list_layout);
        this.n = new ArrayList(3);
        for (int i2 = 0; i2 < 3; i2++) {
            ComplexContentItemView complexContentItemView = new ComplexContentItemView(this.f1693a);
            this.n.add(complexContentItemView);
            this.m.addView(complexContentItemView, new LinearLayout.LayoutParams(-1, -2));
        }
        f();
    }

    private void f() {
        int i2;
        b bVar = (b) this.d;
        if (!TextUtils.isEmpty(bVar.l)) {
            this.i.setText(bVar.l);
            int a2 = com.tencent.assistant.smartcard.f.b.a(bVar.f());
            if (a2 != 0) {
                Drawable drawable = getResources().getDrawable(a2);
                drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
                this.i.setCompoundDrawables(drawable, null, null, null);
                this.i.setCompoundDrawablePadding(by.a(this.f1693a, 5.0f));
                this.i.setPadding(0, 0, 0, 0);
            } else {
                this.i.setCompoundDrawables(null, null, null, null);
                this.i.setPadding(by.a(this.f1693a, 5.0f), 0, 0, 0);
            }
            if (!TextUtils.isEmpty(bVar.p)) {
                this.l.setText(bVar.p);
                this.l.setOnClickListener(this.k);
                this.l.setVisibility(0);
            } else {
                this.l.setVisibility(8);
            }
        }
        ArrayList<a> h = bVar.h();
        if (h != null && h.size() != 0) {
            int size = h.size();
            if (size > 3) {
                i2 = 3;
            } else {
                i2 = size;
            }
            int i3 = 0;
            while (i3 < i2) {
                this.n.get(i3).a(a(com.tencent.assistantv2.st.page.a.a("05", i3), 100), h.get(i3).f2322a, h.get(i3).b, i3 < i2 + -1);
                this.n.get(i3).setVisibility(0);
                i3++;
            }
            if (i2 < 3) {
                while (i2 < 3) {
                    this.n.get(i2).setVisibility(8);
                    i2++;
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void b() {
        f();
    }

    /* access modifiers changed from: protected */
    public String d(int i2) {
        if (this.d instanceof b) {
            return ((b) this.d).e();
        }
        return super.d(i2);
    }
}
