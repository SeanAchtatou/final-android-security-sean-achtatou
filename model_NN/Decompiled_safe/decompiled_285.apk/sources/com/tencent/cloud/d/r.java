package com.tencent.cloud.d;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.module.BaseEngine;
import com.tencent.assistant.module.callback.ActionCallback;
import com.tencent.assistant.protocol.jce.SetBookReadMarkRequest;

/* compiled from: ProGuard */
public class r extends BaseEngine<ActionCallback> {
    public int a(String str, int i) {
        SetBookReadMarkRequest setBookReadMarkRequest = new SetBookReadMarkRequest();
        setBookReadMarkRequest.f1488a = str;
        setBookReadMarkRequest.b = i;
        return send(setBookReadMarkRequest);
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
    }
}
