package com.tencent.connector;

import android.media.MediaPlayer;

/* compiled from: ProGuard */
class c implements MediaPlayer.OnErrorListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CaptureActivity f2385a;

    c(CaptureActivity captureActivity) {
        this.f2385a = captureActivity;
    }

    public boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
        mediaPlayer.release();
        MediaPlayer unused = this.f2385a.i = (MediaPlayer) null;
        return true;
    }
}
