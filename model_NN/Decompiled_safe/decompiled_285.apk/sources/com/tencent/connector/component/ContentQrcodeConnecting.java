package com.tencent.connector.component;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.connector.ConnectionActivity;

/* compiled from: ProGuard */
public class ContentQrcodeConnecting extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    private Context f2393a;
    private ConnectionActivity b;
    private TextView c;

    public ContentQrcodeConnecting(Context context) {
        super(context);
        this.f2393a = context;
    }

    public ContentQrcodeConnecting(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f2393a = context;
    }

    public void setHostActivity(ConnectionActivity connectionActivity) {
        this.b = connectionActivity;
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        this.c = (TextView) findViewById(R.id.pc_name);
    }

    public void display(String str) {
        if (str == null) {
            str = this.f2393a.getString(R.string.tip_no_pc_name);
        }
        if (this.c != null) {
            this.c.setText(getResources().getString(R.string.label_connection_with_pc_doing, str));
        }
    }
}
