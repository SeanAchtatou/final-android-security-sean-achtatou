package com.tencent.downloadsdk.storage.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.SystemClock;
import com.tencent.downloadsdk.storage.a.d;

public abstract class SqliteHelper extends SQLiteOpenHelper {
    private static final String TAG = SqliteHelper.class.getSimpleName();

    public SqliteHelper(Context context, String str, SQLiteDatabase.CursorFactory cursorFactory, int i) {
        super(context, str, cursorFactory, i);
    }

    private void createTable(SQLiteDatabase sQLiteDatabase) {
        String str;
        for (Class<?> newInstance : getTables()) {
            try {
                str = ((d) newInstance.newInstance()).d();
            } catch (Exception e) {
                e.printStackTrace();
                str = null;
            }
            if (str != null && str.length() > 0) {
                sQLiteDatabase.execSQL(str);
            }
        }
    }

    private void deleteTable(SQLiteDatabase sQLiteDatabase) {
        for (Class<?> newInstance : getTables()) {
            try {
                sQLiteDatabase.delete(((d) newInstance.newInstance()).c(), null, null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void dropTable(SQLiteDatabase sQLiteDatabase) {
        Class<?>[] tables = getTables();
        int length = tables.length;
        for (int i = 0; i < length; i++) {
            try {
                sQLiteDatabase.execSQL("DROP TABLE IF EXISTS " + ((d) tables[i].newInstance()).c());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public abstract int getDBVersion();

    public synchronized SQLiteDatabase getReadableDatabase() {
        SQLiteDatabase readableDatabase;
        readableDatabase = super.getReadableDatabase();
        while (true) {
            if (readableDatabase.isDbLockedByCurrentThread() || readableDatabase.isDbLockedByOtherThreads()) {
                SystemClock.sleep(10);
            }
        }
        return readableDatabase;
    }

    public int getSumTableVer() {
        int i;
        Class<?>[] tables = getTables();
        int length = tables.length;
        int i2 = 0;
        int i3 = 0;
        while (i2 < length) {
            try {
                i = ((d) tables[i2].newInstance()).b() + i3;
            } catch (Exception e) {
                e.printStackTrace();
                i = i3;
            }
            i2++;
            i3 = i;
        }
        return i3;
    }

    public abstract Class<?>[] getTables();

    public synchronized SQLiteDatabase getWritableDatabase() {
        SQLiteDatabase writableDatabase;
        writableDatabase = super.getWritableDatabase();
        while (true) {
            if (writableDatabase.isDbLockedByCurrentThread() || writableDatabase.isDbLockedByOtherThreads()) {
                SystemClock.sleep(10);
            }
        }
        return writableDatabase;
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        createTable(sQLiteDatabase);
        for (Class<?> newInstance : getTables()) {
            try {
                ((d) newInstance.newInstance()).a(sQLiteDatabase);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        dropTable(sQLiteDatabase);
        createTable(sQLiteDatabase);
    }

    public void onOpen(SQLiteDatabase sQLiteDatabase) {
        super.onOpen(sQLiteDatabase);
        int version = sQLiteDatabase.getVersion();
        if (version != 0) {
            if (version < getDBVersion()) {
                onUpgrade(sQLiteDatabase, version, getDBVersion());
            } else if (version > getDBVersion()) {
                onDowngrade(sQLiteDatabase, version, getDBVersion());
            }
        }
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        while (i < i2) {
            for (Class<?> newInstance : getTables()) {
                try {
                    d dVar = (d) newInstance.newInstance();
                    dVar.a(i, i + 1, sQLiteDatabase);
                    String[] a2 = dVar.a(i, i + 1);
                    if (a2 != null) {
                        for (String execSQL : a2) {
                            sQLiteDatabase.execSQL(execSQL);
                        }
                    }
                    dVar.b(i, i + 1, sQLiteDatabase);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            i++;
        }
    }
}
