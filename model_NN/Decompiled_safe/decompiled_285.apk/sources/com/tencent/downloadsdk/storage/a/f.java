package com.tencent.downloadsdk.storage.a;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import com.tencent.downloadsdk.DownloadManager;
import com.tencent.downloadsdk.storage.helper.SDKDBHelper;
import com.tencent.downloadsdk.storage.helper.SqliteHelper;

public class f implements d {

    /* renamed from: a  reason: collision with root package name */
    public final String f2509a = "task_speed_table";
    private final String b = "TaskSpeedTable";
    private final String c = "task_id";
    private final String d = "received_length";
    private final String e = "start_time";
    private final String f = "end_time";
    private final String g = "cost_time";
    private final String h = "avg_speed";
    private final String[] i = {"task_id", "received_length", "start_time", "end_time", "cost_time", "avg_speed"};
    private final byte[] j = new byte[0];

    public SqliteHelper a() {
        return SDKDBHelper.getDBHelper(DownloadManager.a().b());
    }

    public void a(int i2, int i3, SQLiteDatabase sQLiteDatabase) {
    }

    public void a(SQLiteDatabase sQLiteDatabase) {
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x007c A[Catch:{ Exception -> 0x006c, all -> 0x0078 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(com.tencent.downloadsdk.a.c r14) {
        /*
            r13 = this;
            r10 = 0
            r11 = 0
            if (r14 != 0) goto L_0x0006
            r0 = r10
        L_0x0005:
            return r0
        L_0x0006:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r0 = "task_id"
            java.lang.StringBuilder r0 = r4.append(r0)
            java.lang.String r1 = "='"
            r0.append(r1)
            java.lang.String r0 = r14.f2452a
            java.lang.StringBuilder r0 = r4.append(r0)
            java.lang.String r1 = "'"
            r0.append(r1)
            byte[] r12 = r13.j
            monitor-enter(r12)
            com.tencent.downloadsdk.storage.helper.SqliteHelper r0 = r13.a()     // Catch:{ Exception -> 0x006c, all -> 0x0078 }
            android.database.sqlite.SQLiteDatabase r0 = r0.getReadableDatabase()     // Catch:{ Exception -> 0x006c, all -> 0x0078 }
            r1 = 1
            java.lang.String r2 = "task_speed_table"
            java.lang.String[] r3 = r13.i     // Catch:{ Exception -> 0x006c, all -> 0x0078 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x006c, all -> 0x0078 }
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            java.lang.String r9 = "1"
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7, r8, r9)     // Catch:{ Exception -> 0x006c, all -> 0x0078 }
            boolean r10 = r1.moveToFirst()     // Catch:{ Exception -> 0x0082 }
            if (r10 == 0) goto L_0x0061
            r0 = 1
            long r2 = r1.getLong(r0)     // Catch:{ Exception -> 0x0082 }
            r14.e = r2     // Catch:{ Exception -> 0x0082 }
            r0 = 2
            long r2 = r1.getLong(r0)     // Catch:{ Exception -> 0x0082 }
            r14.b = r2     // Catch:{ Exception -> 0x0082 }
            r0 = 3
            long r2 = r1.getLong(r0)     // Catch:{ Exception -> 0x0082 }
            r14.c = r2     // Catch:{ Exception -> 0x0082 }
            r0 = 4
            long r2 = r1.getLong(r0)     // Catch:{ Exception -> 0x0082 }
            r14.d = r2     // Catch:{ Exception -> 0x0082 }
        L_0x0061:
            if (r1 == 0) goto L_0x0086
            r1.close()     // Catch:{ all -> 0x0069 }
            r0 = r10
        L_0x0067:
            monitor-exit(r12)     // Catch:{ all -> 0x0069 }
            goto L_0x0005
        L_0x0069:
            r0 = move-exception
            monitor-exit(r12)     // Catch:{ all -> 0x0069 }
            throw r0
        L_0x006c:
            r0 = move-exception
            r1 = r11
        L_0x006e:
            r0.printStackTrace()     // Catch:{ all -> 0x0080 }
            if (r1 == 0) goto L_0x0084
            r1.close()     // Catch:{ all -> 0x0069 }
            r0 = r10
            goto L_0x0067
        L_0x0078:
            r0 = move-exception
            r1 = r11
        L_0x007a:
            if (r1 == 0) goto L_0x007f
            r1.close()     // Catch:{ all -> 0x0069 }
        L_0x007f:
            throw r0     // Catch:{ all -> 0x0069 }
        L_0x0080:
            r0 = move-exception
            goto L_0x007a
        L_0x0082:
            r0 = move-exception
            goto L_0x006e
        L_0x0084:
            r0 = r10
            goto L_0x0067
        L_0x0086:
            r0 = r10
            goto L_0x0067
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.downloadsdk.storage.a.f.a(com.tencent.downloadsdk.a.c):boolean");
    }

    public boolean a(String str) {
        boolean z = false;
        synchronized (this.j) {
            try {
                StringBuilder sb = new StringBuilder();
                sb.append("task_id").append("='");
                sb.append(str).append("'");
                if (a().getWritableDatabase().delete("task_speed_table", sb.toString(), null) > 0) {
                    z = true;
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return z;
    }

    public boolean a(String str, long j2, long j3, long j4, long j5, long j6) {
        boolean z;
        synchronized (this.j) {
            try {
                ContentValues contentValues = new ContentValues();
                contentValues.put("task_id", str);
                contentValues.put("received_length", Long.valueOf(j2));
                contentValues.put("start_time", Long.valueOf(j3));
                contentValues.put("end_time", Long.valueOf(j4));
                contentValues.put("cost_time", Long.valueOf(j5));
                contentValues.put("avg_speed", Long.valueOf(j6));
                z = a().getWritableDatabase().replace("task_speed_table", null, contentValues) > 0;
            } catch (Exception e2) {
                e2.printStackTrace();
                z = false;
            }
        }
        return z;
    }

    public String[] a(int i2, int i3) {
        if (i2 != 1 || i3 != 2) {
            return null;
        }
        return new String[]{d()};
    }

    public int b() {
        return 0;
    }

    public void b(int i2, int i3, SQLiteDatabase sQLiteDatabase) {
    }

    public String c() {
        return "task_speed_table";
    }

    public String d() {
        StringBuilder sb = new StringBuilder();
        sb.append("CREATE TABLE if not exists ").append("task_speed_table");
        sb.append(" (").append("task_id").append(" VARCHAR PRIMARY KEY, ");
        sb.append("received_length").append(" INTEGER, ");
        sb.append("start_time").append(" INTEGER, ");
        sb.append("end_time").append(" INTEGER, ");
        sb.append("cost_time").append(" INTEGER, ");
        sb.append("avg_speed").append(" INTEGER);");
        return sb.toString();
    }
}
