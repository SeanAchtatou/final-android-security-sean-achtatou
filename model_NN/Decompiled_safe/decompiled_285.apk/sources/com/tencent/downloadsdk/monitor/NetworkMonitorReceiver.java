package com.tencent.downloadsdk.monitor;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;

public class NetworkMonitorReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        if ("android.net.conn.CONNECTIVITY_CHANGE".equals(intent.getAction())) {
            f.a().a((NetworkInfo) intent.getParcelableExtra("networkInfo"));
        }
    }
}
