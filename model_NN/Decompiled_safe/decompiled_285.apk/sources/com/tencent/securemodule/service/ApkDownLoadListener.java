package com.tencent.securemodule.service;

public interface ApkDownLoadListener {
    void onDownLoadError();

    void onDownloadStart();

    void onDownloadSuccess();

    void onRefreshProgress(int i, long j, long j2);
}
