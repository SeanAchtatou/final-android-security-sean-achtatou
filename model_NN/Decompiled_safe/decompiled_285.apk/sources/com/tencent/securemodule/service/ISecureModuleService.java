package com.tencent.securemodule.service;

import android.content.Context;

public interface ISecureModuleService {
    void cloudScan();

    void downLoadTMSeucreApk(ApkDownLoadListener apkDownLoadListener);

    int register(ProductInfo productInfo);

    void registerCloudScanListener(Context context, CloudScanListener cloudScanListener);

    void setNotificationUIEnable(boolean z);

    void unregisterCloudScanListener(Context context, CloudScanListener cloudScanListener);
}
