package com.tencent.assistantv2.st.business;

import android.os.Handler;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.utils.ah;

/* compiled from: ProGuard */
public class a extends BaseSTManagerV2 {

    /* renamed from: a  reason: collision with root package name */
    private static a f2030a;
    private static Handler c = null;

    public static synchronized a a() {
        a aVar;
        synchronized (a.class) {
            if (f2030a == null) {
                f2030a = new a();
            }
            aVar = f2030a;
        }
        return aVar;
    }

    public byte getSTType() {
        return 18;
    }

    public void flush() {
    }

    public void a(JceStruct jceStruct) {
        b().postDelayed(new b(this, jceStruct), 50);
    }

    private synchronized Handler b() {
        if (c == null) {
            c = ah.a("ApiInvokingHandler");
        }
        return c;
    }
}
