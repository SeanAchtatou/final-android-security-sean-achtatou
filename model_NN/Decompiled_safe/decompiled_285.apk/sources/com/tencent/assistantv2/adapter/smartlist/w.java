package com.tencent.assistantv2.adapter.smartlist;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.activity.AppDetailActivityV5;

/* compiled from: ProGuard */
class w extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    private Context f1921a;
    private int b;
    private SimpleAppModel c;
    private aa d;

    public w(Context context, int i, SimpleAppModel simpleAppModel, aa aaVar) {
        this.f1921a = context;
        this.b = i;
        this.c = simpleAppModel;
        this.d = aaVar;
    }

    public void onTMAClick(View view) {
        Intent intent = new Intent(this.f1921a, AppDetailActivityV5.class);
        intent.putExtra("st_common_data", this.d.e());
        intent.putExtra("simpleModeInfo", this.c);
        this.f1921a.startActivity(intent);
    }

    public STInfoV2 getStInfo() {
        if (this.d == null || this.d.e() == null) {
            return null;
        }
        this.d.e().actionId = 200;
        this.d.e().status = "01";
        return this.d.e();
    }
}
