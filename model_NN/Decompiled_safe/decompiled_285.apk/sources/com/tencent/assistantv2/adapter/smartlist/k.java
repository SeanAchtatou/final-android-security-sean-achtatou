package com.tencent.assistantv2.adapter.smartlist;

import android.content.Context;
import android.text.TextUtils;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.component.txscrollview.TXAppIconView;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.utils.bm;
import com.tencent.assistant.utils.by;
import com.tencent.cloud.model.SimpleEbookModel;
import com.tencent.pangu.model.b;

/* compiled from: ProGuard */
public class k extends g {
    private IViewInvalidater c;
    private aa d;

    public k(Context context, aa aaVar, IViewInvalidater iViewInvalidater) {
        super(context, aaVar, iViewInvalidater);
        this.c = iViewInvalidater;
        this.d = aaVar;
    }

    public Pair<View, Object> a() {
        View inflate = LayoutInflater.from(this.f1900a).inflate((int) R.layout.ebook_rich_universal_item, (ViewGroup) null);
        m mVar = new m();
        mVar.f1914a = (TextView) inflate.findViewById(R.id.ebook_match_result);
        mVar.b = (TXAppIconView) inflate.findViewById(R.id.ebook_icon);
        mVar.b.setInvalidater(this.c);
        mVar.c = (TextView) inflate.findViewById(R.id.ebook_title);
        mVar.d = (TextView) inflate.findViewById(R.id.ebook_update_info);
        mVar.e = (TextView) inflate.findViewById(R.id.ebook_view_count);
        mVar.f = (TextView) inflate.findViewById(R.id.ebook_author);
        mVar.g = (TextView) inflate.findViewById(R.id.ebook_label);
        mVar.h = (TextView) inflate.findViewById(R.id.ebook_words);
        mVar.i = (TextView) inflate.findViewById(R.id.ebook_desc);
        return Pair.create(inflate, mVar);
    }

    public void a(View view, Object obj, int i, b bVar) {
        SimpleEbookModel simpleEbookModel;
        m mVar = (m) obj;
        if (bVar != null) {
            simpleEbookModel = bVar.e();
        } else {
            simpleEbookModel = null;
        }
        view.setOnClickListener(new j(this, this.f1900a, i, simpleEbookModel, this.b));
        a(mVar, simpleEbookModel, i);
    }

    private void a(m mVar, SimpleEbookModel simpleEbookModel, int i) {
        if (simpleEbookModel != null && mVar != null) {
            mVar.f1914a.setText(simpleEbookModel.k);
            mVar.b.updateImageView(simpleEbookModel.c, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            mVar.c.setText(simpleEbookModel.b);
            if (!TextUtils.isEmpty(bm.f(simpleEbookModel.i))) {
                mVar.d.setPadding(0, 0, by.a(this.f1900a, 10.0f), 0);
                mVar.d.setText(bm.f(simpleEbookModel.i));
            } else {
                mVar.d.setPadding(0, 0, 0, 0);
            }
            if (!TextUtils.isEmpty(bm.f(simpleEbookModel.d))) {
                mVar.f.setPadding(0, 0, by.a(this.f1900a, 10.0f), 0);
                mVar.f.setText(bm.f(simpleEbookModel.d));
            } else {
                mVar.f.setPadding(0, 0, 0, 0);
            }
            mVar.g.setText(simpleEbookModel.f);
            mVar.h.setText(simpleEbookModel.g);
            mVar.e.setText(simpleEbookModel.h);
            mVar.i.setText(bm.e(simpleEbookModel.e));
        }
    }
}
