package com.tencent.assistantv2.adapter.smartlist;

import android.content.Context;
import android.util.Pair;
import android.view.View;
import com.tencent.pangu.model.b;

/* compiled from: ProGuard */
public abstract class a {

    /* renamed from: a  reason: collision with root package name */
    protected Context f1900a;
    protected aa b;

    public abstract Pair<View, Object> a();

    public abstract void a(View view, Object obj, int i, b bVar);

    public a(Context context, aa aaVar) {
        this.f1900a = context;
        this.b = aaVar;
        if (this.b == null) {
            this.b = new aa();
        }
    }
}
