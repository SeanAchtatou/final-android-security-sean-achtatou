package com.tencent.assistantv2.adapter.smartlist;

import android.widget.ListView;
import com.tencent.assistant.model.b;
import com.tencent.assistant.model.d;
import com.tencent.assistantv2.adapter.smartlist.SmartListAdapter;
import com.tencent.assistantv2.component.ListItemInfoView;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
public class aa {

    /* renamed from: a  reason: collision with root package name */
    private boolean f1901a = false;
    private boolean b = false;
    private int c;
    private b d;
    private long e;
    private ListView f;
    private boolean g;
    private boolean h = false;
    private boolean i = true;
    private STInfoV2 j = null;
    private com.tencent.assistantv2.st.b.b k = null;
    private ListItemInfoView.InfoType l = null;
    private d m = null;
    private SmartListAdapter.SmartListType n = null;
    private boolean o = false;
    private int p = -1;

    public d a() {
        return this.m;
    }

    public aa a(d dVar) {
        this.m = dVar;
        return this;
    }

    public void a(ListItemInfoView.InfoType infoType) {
        this.l = infoType;
    }

    public ListItemInfoView.InfoType b() {
        return this.l;
    }

    public com.tencent.assistantv2.st.b.b c() {
        return this.k;
    }

    public void a(com.tencent.assistantv2.st.b.b bVar) {
        this.k = bVar;
    }

    public boolean d() {
        return this.g;
    }

    public void a(boolean z) {
        this.g = z;
    }

    public aa b(boolean z) {
        this.f1901a = z;
        return this;
    }

    public aa a(int i2) {
        this.c = i2;
        return this;
    }

    public aa a(STInfoV2 sTInfoV2) {
        this.j = sTInfoV2;
        return this;
    }

    public STInfoV2 e() {
        return this.j;
    }

    public aa a(ListView listView) {
        this.f = listView;
        return this;
    }

    public aa c(boolean z) {
        this.b = z;
        return this;
    }

    public aa a(b bVar) {
        this.d = bVar;
        return this;
    }

    public aa a(long j2) {
        this.e = j2;
        return this;
    }

    public aa a(SmartListAdapter.SmartListType smartListType) {
        this.n = smartListType;
        return this;
    }

    public SmartListAdapter.SmartListType f() {
        return this.n;
    }

    public boolean g() {
        return this.b;
    }

    public int h() {
        return this.c;
    }

    public b i() {
        if (this.d == null) {
            this.d = new b();
        }
        return this.d;
    }

    public ListView j() {
        return this.f;
    }

    public void d(boolean z) {
        this.i = z;
    }

    public boolean k() {
        return this.i;
    }

    public boolean l() {
        return this.o;
    }

    public void e(boolean z) {
        this.o = z;
    }

    public int m() {
        return this.p;
    }

    public void b(int i2) {
        this.p = i2;
    }
}
