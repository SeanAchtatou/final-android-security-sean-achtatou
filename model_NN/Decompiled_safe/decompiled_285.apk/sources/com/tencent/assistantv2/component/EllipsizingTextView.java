package com.tencent.assistantv2.component;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.TextView;
import com.tencent.android.qqdownloader.b;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class EllipsizingTextView extends TextView {

    /* renamed from: a  reason: collision with root package name */
    private final String f1928a;
    private final int b;
    private final List<s> c;
    private boolean d;
    private boolean e;
    private boolean f;
    private String g;
    private int h;
    private float i;
    private float j;
    private boolean k;

    public EllipsizingTextView(Context context) {
        this(context, null);
    }

    public EllipsizingTextView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public EllipsizingTextView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.f1928a = "EllipsizingTextView";
        this.b = -1;
        this.c = new ArrayList();
        this.d = false;
        this.e = true;
        this.f = false;
        this.h = -1;
        this.i = 1.0f;
        this.j = 0.0f;
        this.k = true;
        a(context, attributeSet);
    }

    private void a(Context context, AttributeSet attributeSet) {
        this.d = false;
        this.f = false;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, b.e);
        if (obtainStyledAttributes != null) {
            this.h = obtainStyledAttributes.getInt(0, -1);
            this.k = obtainStyledAttributes.getBoolean(1, true);
            if (this.h > -1) {
                this.e = true;
                setMaxLines(this.h);
            }
            obtainStyledAttributes.recycle();
        }
    }

    public int getMaxLines() {
        return this.h;
    }

    public void setLineSpacing(float f2, float f3) {
        super.setLineSpacing(f2, f3);
        this.j = f2;
        this.i = f3;
    }

    /* access modifiers changed from: protected */
    public void onTextChanged(CharSequence charSequence, int i2, int i3, int i4) {
        super.onTextChanged(charSequence, i2, i3, i4);
        if (!this.f) {
            if (charSequence != null) {
                this.g = charSequence.toString();
            }
            this.e = true;
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        if (this.e) {
            super.setEllipsize(null);
            a();
        }
        super.onDraw(canvas);
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0072  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x007f  */
    /* JADX WARNING: Removed duplicated region for block: B:40:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a() {
        /*
            r6 = this;
            r2 = 1
            r3 = 0
            java.lang.String r0 = r6.g
            if (r0 != 0) goto L_0x0007
        L_0x0006:
            return
        L_0x0007:
            java.lang.String r0 = r6.g
            int r4 = r6.getMaxLines()
            if (r4 < 0) goto L_0x00a8
            android.text.Layout r1 = r6.a(r0)
            int r5 = r1.getLineCount()
            if (r5 <= r4) goto L_0x00a8
            java.lang.String r0 = r6.g
            int r5 = r4 + -1
            int r1 = r1.getLineEnd(r5)
            java.lang.String r0 = r0.substring(r3, r1)
            java.lang.String r0 = r0.trim()
        L_0x0029:
            boolean r1 = r6.k
            if (r1 == 0) goto L_0x0097
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.StringBuilder r1 = r1.append(r0)
            java.lang.String r5 = "..."
            java.lang.StringBuilder r1 = r1.append(r5)
            java.lang.String r1 = r1.toString()
        L_0x0040:
            android.text.Layout r1 = r6.a(r1)
            int r1 = r1.getLineCount()
            if (r1 <= r4) goto L_0x0050
            int r1 = r0.length()
            if (r1 > 0) goto L_0x0099
        L_0x0050:
            boolean r1 = r6.k
            if (r1 == 0) goto L_0x0067
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r1 = "..."
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
        L_0x0067:
            r1 = r2
        L_0x0068:
            java.lang.CharSequence r4 = r6.getText()
            boolean r4 = r0.equals(r4)
            if (r4 != 0) goto L_0x0079
            r6.f = r2
            r6.setText(r0)     // Catch:{ all -> 0x00a4 }
            r6.f = r3
        L_0x0079:
            r6.e = r3
            boolean r0 = r6.d
            if (r1 == r0) goto L_0x0006
            r6.d = r1
            java.util.List<com.tencent.assistantv2.component.s> r0 = r6.c
            java.util.Iterator r2 = r0.iterator()
        L_0x0087:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x0006
            java.lang.Object r0 = r2.next()
            com.tencent.assistantv2.component.s r0 = (com.tencent.assistantv2.component.s) r0
            r0.a(r1)
            goto L_0x0087
        L_0x0097:
            r1 = r0
            goto L_0x0040
        L_0x0099:
            int r1 = r0.length()
            int r1 = r1 + -1
            java.lang.String r0 = r0.substring(r3, r1)
            goto L_0x0029
        L_0x00a4:
            r0 = move-exception
            r6.f = r3
            throw r0
        L_0x00a8:
            r1 = r3
            goto L_0x0068
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistantv2.component.EllipsizingTextView.a():void");
    }

    private Layout a(String str) {
        return new StaticLayout(str, getPaint(), (getWidth() - getPaddingLeft()) - getPaddingRight(), Layout.Alignment.ALIGN_NORMAL, this.i, this.j, false);
    }

    public void setEllipsize(TextUtils.TruncateAt truncateAt) {
    }
}
