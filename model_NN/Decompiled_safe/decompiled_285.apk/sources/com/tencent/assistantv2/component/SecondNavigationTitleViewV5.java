package com.tencent.assistantv2.component;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.DownloadProgressButton;
import com.tencent.assistant.manager.t;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.c;
import com.tencent.assistant.utils.be;
import com.tencent.assistant.utils.by;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.pangu.component.appdetail.AppdetailFloatingDialog;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class SecondNavigationTitleViewV5 extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    public static final ArrayList<String> f1940a = new ArrayList<>();
    private LinearLayout A;
    private View B;
    private int C = 0;
    private View D = null;
    private View.OnClickListener E = new ay(this);
    private View.OnClickListener F = new az(this);
    private View.OnClickListener G = new ba(this);
    private View.OnClickListener H = new bb(this);
    private View.OnClickListener I = new bc(this);
    /* access modifiers changed from: private */
    public Context b;
    private RelativeLayout c;
    private ImageView d;
    private ImageView e;
    private TextView f;
    private TextView g;
    private TextView h;
    private RelativeLayout i;
    private TextView j;
    /* access modifiers changed from: private */
    public Activity k = null;
    private LinearLayout l;
    private ImageView m;
    private RelativeLayout n;
    private LinearLayout o;
    private ImageView p;
    private DownloadProgressButton q;
    /* access modifiers changed from: private */
    public ImageView r;
    /* access modifiers changed from: private */
    public AppdetailFloatingDialog s;
    private DownloadCenterButton t;
    private LinearLayout u;
    private LinearLayout v;
    /* access modifiers changed from: private */
    public SimpleAppModel w;
    private STInfoV2 x;
    /* access modifiers changed from: private */
    public c y;
    /* access modifiers changed from: private */
    public boolean z = true;

    static {
        f1940a.add("MI-ONE Plus");
    }

    public SecondNavigationTitleViewV5(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.b = context;
        r();
    }

    public SecondNavigationTitleViewV5(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.b = context;
        r();
    }

    public SecondNavigationTitleViewV5(Context context) {
        super(context);
        this.b = context;
        r();
    }

    private void r() {
        LayoutInflater from = LayoutInflater.from(this.b);
        this.B = new View(this.b);
        try {
            View inflate = from.inflate((int) R.layout.second_navigation_layout_v5, this);
            this.n = (RelativeLayout) inflate.findViewById(R.id.title_container_layout);
            this.c = (RelativeLayout) inflate.findViewById(R.id.back_layout_all);
            this.c.setOnClickListener(this.F);
            this.d = (ImageView) inflate.findViewById(R.id.back_Img);
            this.e = (ImageView) inflate.findViewById(R.id.right_img);
            this.f = (TextView) inflate.findViewById(R.id.title_txt);
            this.g = (TextView) inflate.findViewById(R.id.title_num_txt);
            this.A = (LinearLayout) inflate.findViewById(R.id.title_layout);
            this.h = (TextView) inflate.findViewById(R.id.other_title_txt);
            this.i = (RelativeLayout) inflate.findViewById(R.id.action_layout);
            this.j = (TextView) inflate.findViewById(R.id.action_txt);
            this.l = (LinearLayout) findViewById(R.id.right_layout);
            this.l.setVisibility(0);
            this.l.setOnClickListener(this.G);
            this.m = (ImageView) findViewById(R.id.main_icon);
            this.o = (LinearLayout) findViewById(R.id.home_layout);
            this.o.setOnClickListener(this.H);
            this.p = (ImageView) findViewById(R.id.iv_home);
            this.q = (DownloadProgressButton) findViewById(R.id.down_layout);
            this.r = (ImageView) findViewById(R.id.iv_red_dot);
            s();
            this.v = (LinearLayout) findViewById(R.id.show_share);
            this.u = (LinearLayout) findViewById(R.id.show_more);
            this.u.setOnClickListener(new ax(this));
            this.t = (DownloadCenterButton) findViewById(R.id.download_page_area);
            this.t.a(a.a("02", "001"));
            this.D = findViewById(R.id.bottom_line);
        } catch (Throwable th) {
            t.a().b();
        }
    }

    public void a() {
    }

    public void b() {
        this.n.setBackgroundResource(R.color.guanjia_title_background);
        this.d.setImageResource(R.drawable.guanjia_style_back);
        this.p.setImageResource(R.drawable.guanjia_style_logo);
        this.h.setTextColor(getResources().getColor(R.color.white));
        this.t.c();
    }

    public void c() {
        this.n.setBackgroundResource(R.color.common_topbar_bg);
        this.d.setImageResource(R.drawable.icon_backbtn);
        this.p.setImageResource(R.drawable.common_icon_myapplogo);
        this.h.setTextColor(getResources().getColor(R.color.common_topbar_text_color));
        this.t.d();
        n();
    }

    private void s() {
        this.s = new AppdetailFloatingDialog(this.b, R.style.notDimdialog);
        Window window = this.s.getWindow();
        WindowManager.LayoutParams attributes = window.getAttributes();
        attributes.gravity = 53;
        Display defaultDisplay = this.s.getWindow().getWindowManager().getDefaultDisplay();
        attributes.y = getResources().getDimensionPixelSize(R.dimen.app_detail_float_bar_content_height);
        try {
            attributes.height = defaultDisplay.getHeight();
        } catch (Exception e2) {
        }
        attributes.width = defaultDisplay.getWidth();
        window.setAttributes(attributes);
        this.s.setCanceledOnTouchOutside(true);
    }

    public void d() {
        if (this.l != null) {
            this.l.setVisibility(8);
        }
    }

    public void e() {
        if (this.u != null) {
            this.u.setVisibility(0);
        }
    }

    public void a(boolean z2) {
        if (this.v != null) {
            this.v.setVisibility(z2 ? 0 : 8);
        }
    }

    public void f() {
        if (this.s != null) {
            this.s.b();
        }
    }

    public void a(int i2) {
        if (this.p != null) {
            this.p.setImageResource(i2);
            ViewGroup.LayoutParams layoutParams = this.p.getLayoutParams();
            layoutParams.height = by.a(this.b, 52.0f);
            layoutParams.width = by.a(this.b, 52.0f);
            this.p.setLayoutParams(layoutParams);
        }
    }

    public void b(int i2) {
        if (this.p != null) {
            this.p.setImageResource(i2);
            this.p.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        }
    }

    public void a(View.OnClickListener onClickListener) {
        if (this.v != null) {
            this.v.setOnClickListener(onClickListener);
        }
    }

    public void b(View.OnClickListener onClickListener) {
        if (this.o != null) {
            this.o.setOnClickListener(onClickListener);
        }
    }

    public void a(boolean z2, int i2) {
        if (this.o != null) {
            if (i2 > 0) {
                this.o.startAnimation(AnimationUtils.loadAnimation(this.b, i2));
            }
            this.o.setVisibility(z2 ? 0 : 8);
        }
    }

    public void b(boolean z2) {
        boolean z3 = !be.b();
        if (this.s != null) {
            this.s.a(z2);
        }
        if (!z2 || !z3 || this.r != null) {
        }
    }

    public void g() {
        if (this.o != null) {
            this.o.setVisibility(0);
            this.l.setVisibility(8);
        }
    }

    public void h() {
        if (this.o != null) {
            this.o.setVisibility(8);
            this.l.setVisibility(0);
        }
    }

    public void i() {
        if (this.t != null) {
            this.t.setVisibility(0);
        }
    }

    public void j() {
        if (this.t != null) {
            this.t.setVisibility(8);
        }
    }

    public void k() {
        if (this.i != null) {
            this.i.setVisibility(0);
        }
    }

    public void l() {
        if (this.q != null && this.q.getVisibility() == 0) {
            this.q.onResume();
        }
        if (this.t != null) {
            this.t.a();
        }
    }

    public void m() {
        if (this.q != null) {
            this.q.onPause();
        }
        if (this.t != null) {
            this.t.b();
        }
    }

    public void a(String str) {
        if (this.A != null) {
            this.A.setVisibility(8);
        }
        if (this.h != null) {
            this.h.setText(str);
            this.h.setVisibility(0);
        }
        g();
    }

    public void n() {
        if (this.A != null) {
            this.A.setVisibility(0);
        }
        if (this.h != null) {
            this.h.setVisibility(8);
        }
        h();
    }

    public void b(String str) {
        if (this.f != null && !TextUtils.isEmpty(str)) {
            this.f.setText(str);
        }
    }

    public void a(String str, TextUtils.TruncateAt truncateAt) {
        if (this.f != null && !TextUtils.isEmpty(str)) {
            this.f.setEllipsize(truncateAt);
            this.f.setText(str);
        }
    }

    public void a(String str, int i2) {
        if (this.f != null) {
            this.f.setText(str);
            this.f.setMaxEms(i2);
        }
    }

    public void c(int i2) {
        if (this.n != null) {
            this.n.getBackground().setAlpha(i2);
        }
        String str = "#00FFFFFF";
        if (i2 >= 0 && i2 < 255) {
            str = "#" + String.format("%02x", Integer.valueOf(i2)) + "FFFFFF";
        } else if (i2 >= 255) {
            str = "#FF343434";
        }
        if (this.f != null) {
            this.f.setTextColor(Color.parseColor(str));
        }
        if (i2 >= 255) {
            if (this.d != null) {
                try {
                    this.d.setImageResource(R.drawable.common_icon_back);
                } catch (OutOfMemoryError e2) {
                    e2.printStackTrace();
                }
            }
            e((int) R.drawable.download_centrer_up);
            f((int) R.drawable.download_centrer_down);
            a(getResources().getDrawable(R.drawable.icon_search));
        } else if (i2 <= 0) {
            if (this.d != null) {
                try {
                    this.d.setImageResource(R.drawable.common_icon_back_white);
                } catch (OutOfMemoryError e3) {
                    e3.printStackTrace();
                }
            }
            e((int) R.drawable.download_centrer_up_white);
            f((int) R.drawable.download_centrer_down_white);
            a(getResources().getDrawable(R.drawable.icon_search_white));
        }
    }

    public void c(boolean z2) {
        if (this.D != null) {
            this.D.setVisibility(z2 ? 0 : 8);
        }
    }

    public void c(String str) {
        if (this.g == null) {
            return;
        }
        if (!TextUtils.isEmpty(str)) {
            this.g.setText(str);
            this.g.setVisibility(0);
            return;
        }
        this.g.setVisibility(8);
    }

    public void d(String str) {
        if (this.j != null) {
            this.j.setText(str);
        }
    }

    public void a(Drawable drawable) {
        if (this.e != null && drawable != null) {
            this.e.setImageDrawable(drawable);
        }
    }

    public void c(View.OnClickListener onClickListener) {
        if (this.c != null) {
            this.c.setOnClickListener(onClickListener);
        }
    }

    public void d(View.OnClickListener onClickListener) {
        if (this.i != null) {
            this.i.setOnClickListener(onClickListener);
        }
    }

    public void a(Activity activity) {
        this.k = activity;
    }

    /* access modifiers changed from: private */
    public STInfoV2 e(String str) {
        if (this.x == null) {
            this.x = STInfoBuilder.buildSTInfo(getContext(), 200);
        }
        if (this.x != null) {
            this.x.slotId = str;
            this.x.actionId = 200;
        }
        return this.x;
    }

    public void d(boolean z2) {
        if (z2) {
            this.m.setVisibility(0);
            this.d.setVisibility(8);
            this.c.setOnClickListener(null);
            this.c.setClickable(false);
            i();
            return;
        }
        this.m.setVisibility(8);
        this.c.setVisibility(0);
        this.d.setVisibility(0);
        this.c.setOnClickListener(this.F);
        this.c.setClickable(true);
    }

    public void e(View.OnClickListener onClickListener) {
        if (this.c != null) {
            this.c.setOnClickListener(onClickListener);
        }
    }

    public void a(com.tencent.pangu.component.appdetail.t tVar) {
        if (this.s != null) {
            this.s.a(tVar);
        }
    }

    public void a(SimpleAppModel simpleAppModel, STInfoV2 sTInfoV2, c cVar) {
        if (a(simpleAppModel)) {
            this.w = simpleAppModel;
            this.x = sTInfoV2;
            this.y = cVar;
            if (this.s != null && this.s.isShowing()) {
                this.s.a(cVar.f941a.f, this.w.f938a);
            }
        }
    }

    private boolean a(SimpleAppModel simpleAppModel) {
        if (simpleAppModel != null && !TextUtils.isEmpty(simpleAppModel.d) && !TextUtils.isEmpty(simpleAppModel.i) && simpleAppModel.p >= 0) {
            return true;
        }
        return false;
    }

    public AppdetailFloatingDialog o() {
        return this.s;
    }

    public LinearLayout p() {
        return this.v;
    }

    public void d(int i2) {
        this.C = i2;
    }

    public int q() {
        return this.C;
    }

    public void e(int i2) {
        if (this.t != null) {
            this.t.a(i2);
        }
    }

    public void f(int i2) {
        if (this.t != null) {
            this.t.b(i2);
        }
    }
}
