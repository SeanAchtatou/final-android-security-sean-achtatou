package com.tencent.assistantv2.component.fps;

import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.TextView;

/* compiled from: ProGuard */
public class FPSTextView extends TextView {
    public static boolean a() {
        return Build.MODEL.equals("GT-I8150");
    }

    public FPSTextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public FPSTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public FPSTextView(Context context) {
        super(context);
    }
}
