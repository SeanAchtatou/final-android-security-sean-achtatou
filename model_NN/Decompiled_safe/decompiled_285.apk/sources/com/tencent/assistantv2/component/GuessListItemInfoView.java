package com.tencent.assistantv2.component;

import android.content.Context;
import android.text.Html;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.protocol.jce.RecommendAppInfoEx;
import com.tencent.assistant.utils.at;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.Random;

/* compiled from: ProGuard */
public class GuessListItemInfoView extends RelativeLayout implements w {

    /* renamed from: a  reason: collision with root package name */
    private TXImageView f1931a;
    private TXImageView b;
    private TextView c;
    private TextView d;
    private ListItemInfoView e;
    private LinearLayout f;
    private Context g;

    public GuessListItemInfoView(Context context) {
        super(context);
        this.g = context;
        b();
    }

    public GuessListItemInfoView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.g = context;
        b();
    }

    public void a(SimpleAppModel simpleAppModel, RecommendAppInfoEx recommendAppInfoEx) {
        this.e.a(simpleAppModel);
        this.e.a(8);
        ArrayList<String> arrayList = recommendAppInfoEx == null ? null : recommendAppInfoEx.f;
        if (arrayList == null || arrayList.isEmpty()) {
            this.f1931a.setVisibility(8);
            this.b.setVisibility(8);
        } else if (arrayList.size() == 1) {
            this.f1931a.setVisibility(0);
            this.b.setVisibility(8);
            this.f1931a.updateImageView(arrayList.get(0), R.drawable.pic_default_app, TXImageView.TXImageViewType.ROUND_IMAGE);
        } else {
            this.f1931a.setVisibility(0);
            this.b.setVisibility(0);
            this.f1931a.updateImageView(arrayList.get(0), R.drawable.pic_default_app, TXImageView.TXImageViewType.ROUND_IMAGE);
            this.b.updateImageView(arrayList.get(1), R.drawable.pic_default_app, TXImageView.TXImageViewType.ROUND_IMAGE);
        }
        this.d.setVisibility(0);
        this.d.setText(at.a(simpleAppModel.k));
        String str = recommendAppInfoEx != null ? recommendAppInfoEx.c : Constants.STR_EMPTY;
        String string = new Random().nextInt(2) == 0 ? this.g.getString(R.string.hint_recommand_1) : this.g.getString(R.string.hint_recommand_2);
        if (TextUtils.isEmpty(str)) {
            this.c.setText(string);
            return;
        }
        try {
            this.c.setText(Html.fromHtml(str));
        } catch (Exception e2) {
            this.c.setText(string);
        }
    }

    private void b() {
        View inflate = inflate(getContext(), R.layout.list_guess_item_progress_layout, this);
        this.f = (LinearLayout) inflate.findViewById(R.id.layout_rec);
        this.f1931a = (TXImageView) inflate.findViewById(R.id.relate_icon1);
        this.b = (TXImageView) inflate.findViewById(R.id.relate_icon2);
        this.c = (TextView) inflate.findViewById(R.id.app_recommend);
        this.d = (TextView) inflate.findViewById(R.id.app_size_text);
        this.e = (ListItemInfoView) inflate.findViewById(R.id.app_downding_zone);
        this.e.a(this);
    }

    public ListItemInfoView a() {
        return this.e;
    }

    public void a(int i) {
        if (this.f != null) {
            this.f.setVisibility(i);
        }
        if (this.e != null) {
            this.e.a(8);
        }
    }
}
