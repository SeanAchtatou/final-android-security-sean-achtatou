package com.tencent.assistantv2.a;

import com.tencent.assistantv2.manager.MainTabType;

/* compiled from: ProGuard */
public class i {

    /* renamed from: a  reason: collision with root package name */
    public String f1863a;
    public int b;
    public int c;
    public String d;
    public long e;
    public int f;
    public int g;
    public byte h;

    public i() {
    }

    public i(String str, int i, int i2, String str2) {
        this.f1863a = str;
        this.b = i;
        this.c = i2;
        this.d = str2;
    }

    public i(String str, int i, int i2, String str2, long j, int i3, int i4, byte b2) {
        this.f1863a = str;
        this.b = i;
        this.c = i2;
        this.d = str2;
        this.e = j;
        this.f = i3;
        this.g = i4;
        this.h = b2;
    }

    public MainTabType a() {
        if (this.b < 0 || this.b >= MainTabType.values().length) {
            return MainTabType.WEBVIEW;
        }
        return MainTabType.values()[this.b];
    }
}
