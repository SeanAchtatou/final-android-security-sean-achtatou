package com.tencent.assistantv2.activity;

import android.widget.CompoundButton;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class c implements CompoundButton.OnCheckedChangeListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GuideActivity f1869a;

    c(GuideActivity guideActivity) {
        this.f1869a = guideActivity;
    }

    public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
        XLog.i(this.f1869a.v, "id=" + compoundButton.getId() + " equal=" + (compoundButton == this.f1869a.C));
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f1869a, 200);
        buildSTInfo.scene = STConst.ST_PAGE_GUIDE_RESULT;
        if (compoundButton == this.f1869a.C) {
            buildSTInfo.slotId = "03_002";
            buildSTInfo.status = z ? "001" : "002";
        } else if (compoundButton == this.f1869a.D) {
            buildSTInfo.slotId = "03_003";
            buildSTInfo.status = z ? "001" : "002";
        }
        l.a(buildSTInfo);
    }
}
