package com.tencent.assistantv2.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.text.TextUtils;
import com.tencent.assistant.protocol.jce.DesktopShortCut;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.thumbnailCache.o;
import com.tencent.assistant.thumbnailCache.p;
import com.tencent.assistant.utils.by;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.open.SocialConstants;

/* compiled from: ProGuard */
class n implements p {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ MainActivity f1880a;

    n(MainActivity mainActivity) {
        this.f1880a = mainActivity;
    }

    public void thumbnailRequestFailed(o oVar) {
    }

    public void thumbnailRequestCompleted(o oVar) {
        Bitmap bitmap = oVar.f;
        if (bitmap != null && !bitmap.isRecycled() && this.f1880a.aa != null) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < this.f1880a.aa.size()) {
                    DesktopShortCut desktopShortCut = (DesktopShortCut) this.f1880a.aa.get(i2);
                    if (oVar.c() != null && oVar.c().equals(desktopShortCut.a())) {
                        Intent intent = new Intent("com.tencent.assistant.SHORTCUT");
                        intent.setClassName("com.tencent.android.qqdownloader", "com.tencent.assistant.activity.ShortCutActivity");
                        intent.setFlags(67108864);
                        if (!TextUtils.isEmpty(desktopShortCut.c())) {
                            intent.putExtra("pkgName", desktopShortCut.c());
                            if (!TextUtils.isEmpty(desktopShortCut.d())) {
                                intent.putExtra("channelId", desktopShortCut.d());
                            }
                        } else if (!TextUtils.isEmpty(desktopShortCut.e().a())) {
                            intent.putExtra(SocialConstants.PARAM_URL, desktopShortCut.e().a());
                        }
                        this.f1880a.a(ThumbnailUtils.extractThumbnail(bitmap, by.b(48.0f), by.b(48.0f)), desktopShortCut.b(), intent);
                        this.f1880a.sendBroadcast(intent);
                        l.a(new STInfoV2(204003, "03_001", 2000, STConst.ST_DEFAULT_SLOT, 100));
                    }
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }
}
