package com.tencent.smtt.sdk;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Message;
import android.view.KeyEvent;
import com.tencent.smtt.export.external.WebViewWizardBase;
import com.tencent.smtt.export.external.interfaces.HttpAuthHandler;
import com.tencent.smtt.export.external.interfaces.IX5WebViewBase;
import com.tencent.smtt.export.external.interfaces.SslError;
import com.tencent.smtt.export.external.interfaces.SslErrorHandler;
import com.tencent.smtt.export.external.interfaces.WebResourceResponse;
import com.tencent.smtt.export.external.proxy.X5ProxyWebViewClient;

class SmttWebViewClient extends X5ProxyWebViewClient {
    static final String SCHEME_WTAI_MC = "wtai://wp/mc;";
    private WebViewClient mClient;
    private WebView mWebView;

    public SmttWebViewClient(WebViewWizardBase wizard, WebView webView, WebViewClient client) {
        super(wizard);
        this.mWebView = webView;
        this.mClient = client;
        this.mClient.mX5Client = this;
    }

    public void doUpdateVisitedHistory(IX5WebViewBase view, String url, boolean isReload) {
        this.mWebView.setX5WebView(view);
        this.mClient.doUpdateVisitedHistory(this.mWebView, url, isReload);
    }

    public void onFormResubmission(IX5WebViewBase view, Message dontResend, Message resend) {
        this.mWebView.setX5WebView(view);
        this.mClient.onFormResubmission(this.mWebView, dontResend, resend);
    }

    public void onLoadResource(IX5WebViewBase view, String url) {
        this.mWebView.setX5WebView(view);
        this.mClient.onLoadResource(this.mWebView, url);
    }

    public void onPageFinished(IX5WebViewBase view, int loadType, int backforwardLoadType, String url) {
        this.mWebView.setX5WebView(view);
        this.mWebView.mPv++;
        this.mClient.onPageFinished(this.mWebView, url);
        this.mWebView.hideSplashLogo();
    }

    public void onPageStarted(IX5WebViewBase view, int loadType, int backforwardLoadType, String url, Bitmap favicon) {
        this.mWebView.setX5WebView(view);
        this.mClient.onPageStarted(this.mWebView, url, favicon);
    }

    public void onReceivedError(IX5WebViewBase view, int errorCode, String description, String failingUrl) {
        if (errorCode < -15) {
            if (errorCode == -17) {
                errorCode = -1;
            } else {
                return;
            }
        }
        this.mWebView.setX5WebView(view);
        this.mClient.onReceivedError(this.mWebView, errorCode, description, failingUrl);
    }

    public void onReceivedHttpAuthRequest(IX5WebViewBase view, HttpAuthHandler handler, String host, String realm) {
        this.mWebView.setX5WebView(view);
        this.mClient.onReceivedHttpAuthRequest(this.mWebView, handler, host, realm);
    }

    public void onReceivedSslError(IX5WebViewBase view, SslErrorHandler handler, SslError error) {
        this.mWebView.setX5WebView(view);
        this.mClient.onReceivedSslError(this.mWebView, handler, error);
    }

    public void onScaleChanged(IX5WebViewBase view, float oldScale, float newScale) {
        this.mWebView.setX5WebView(view);
        this.mClient.onScaleChanged(this.mWebView, oldScale, newScale);
    }

    public void onUnhandledKeyEvent(IX5WebViewBase view, KeyEvent event) {
        this.mWebView.setX5WebView(view);
        this.mClient.onUnhandledKeyEvent(this.mWebView, event);
    }

    public boolean shouldOverrideKeyEvent(IX5WebViewBase view, KeyEvent event) {
        this.mWebView.setX5WebView(view);
        return this.mClient.shouldOverrideKeyEvent(this.mWebView, event);
    }

    public void doDial(String telUrl) {
        Intent intent = new Intent("android.intent.action.DIAL", Uri.parse(telUrl));
        intent.addFlags(268435456);
        try {
            if (this.mWebView.getContext() != null) {
                this.mWebView.getContext().startActivity(intent);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean shouldOverrideUrlLoading(IX5WebViewBase view, String url) {
        this.mWebView.setX5WebView(view);
        boolean result = this.mClient.shouldOverrideUrlLoading(this.mWebView, url);
        if (result) {
            return result;
        }
        if (url.startsWith(SCHEME_WTAI_MC)) {
            this.mWebView.getContext().startActivity(new Intent("android.intent.action.VIEW", Uri.parse(WebView.SCHEME_TEL + url.substring(SCHEME_WTAI_MC.length()))));
            return true;
        } else if (!url.startsWith(WebView.SCHEME_TEL)) {
            return result;
        } else {
            doDial(url);
            return true;
        }
    }

    public void onTooManyRedirects(IX5WebViewBase view, Message cancelMsg, Message continueMsg) {
        this.mWebView.setX5WebView(view);
        this.mClient.onTooManyRedirects(this.mWebView, cancelMsg, continueMsg);
    }

    public WebResourceResponse shouldInterceptRequest(IX5WebViewBase view, String url) {
        this.mWebView.setX5WebView(view);
        return this.mClient.shouldInterceptRequest(this.mWebView, url);
    }

    public void onReceivedLoginRequest(IX5WebViewBase view, String realm, String account, String args) {
        this.mWebView.setX5WebView(view);
        this.mClient.onReceivedLoginRequest(this.mWebView, realm, account, args);
    }

    public void super_onPageStarted(WebView view, String url, Bitmap favicon) {
        super.onPageStarted(this.mWebView.getX5WebView(), 0, 0, url, favicon);
    }
}
