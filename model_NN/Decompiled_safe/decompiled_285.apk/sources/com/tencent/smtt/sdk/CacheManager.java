package com.tencent.smtt.sdk;

import com.tencent.smtt.utils.ReflectionUtils;
import java.io.File;
import java.util.Map;

public final class CacheManager {
    @Deprecated
    public static File getCacheFileBaseDir() {
        SDKEngine sdkEngineInstance = SDKEngine.getInstance(false);
        if (sdkEngineInstance == null || !sdkEngineInstance.isX5Core()) {
            return (File) ReflectionUtils.invokeStatic("android.webkit.CacheManager", "getCacheFileBaseDir");
        }
        return (File) sdkEngineInstance.wizard().getCachFileBaseDir();
    }

    @Deprecated
    public static boolean cacheDisabled() {
        SDKEngine sdkEngineInstance = SDKEngine.getInstance(false);
        if (sdkEngineInstance != null && sdkEngineInstance.isX5Core()) {
            return ((Boolean) sdkEngineInstance.wizard().cacheDisabled()).booleanValue();
        }
        Object ret = ReflectionUtils.invokeStatic("android.webkit.CacheManager", "cacheDisabled");
        if (ret != null) {
            return ((Boolean) ret).booleanValue();
        }
        return false;
    }

    public static Object getCacheFile(String url, Map<String, String> headers) {
        SDKEngine sdkEngineInstance = SDKEngine.getInstance(false);
        if (sdkEngineInstance != null && sdkEngineInstance.isX5Core()) {
            return sdkEngineInstance.wizard().getCacheFile(url, headers);
        }
        try {
            return ReflectionUtils.invokeStatic(Class.forName("android.webkit.CacheManager"), "getCacheFile", new Class[]{String.class, Map.class}, url, headers);
        } catch (Exception e) {
            return null;
        }
    }
}
