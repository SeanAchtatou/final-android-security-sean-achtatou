package com.tencent.pangu.model.a;

import android.text.TextUtils;
import com.tencent.assistant.protocol.jce.AdvancedHotWord;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: ProGuard */
public class g extends e<Integer, f> {
    public void a(int i, AdvancedHotWord advancedHotWord) {
        f fVar = (f) a(Integer.valueOf(i));
        if (fVar == null) {
            fVar = new f();
            a(Integer.valueOf(i), fVar);
        }
        fVar.a(advancedHotWord);
    }

    public int a(String str) {
        ConcurrentHashMap a2;
        f fVar;
        int i = 0;
        if (TextUtils.isEmpty(str) || (a2 = a()) == null || a2.size() <= 0) {
            return 0;
        }
        Iterator it = a2.entrySet().iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return i2;
            }
            Map.Entry entry = (Map.Entry) it.next();
            if (!(entry == null || (fVar = (f) entry.getValue()) == null)) {
                i2 += fVar.a(str);
            }
            i = i2;
        }
    }

    public void a(int i, String str) {
        f fVar = (f) a(Integer.valueOf(i));
        if (fVar == null) {
            fVar = new f();
            a(Integer.valueOf(i), fVar);
        }
        fVar.b = str;
    }

    public void a(int i, String str, String str2) {
        f fVar = (f) a(Integer.valueOf(i));
        if (fVar == null) {
            fVar = new f();
            a(Integer.valueOf(i), fVar);
        }
        fVar.c = str;
        fVar.d = str2;
    }
}
