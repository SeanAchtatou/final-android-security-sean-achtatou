package com.tencent.pangu.mediadownload;

import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.utils.FileUtil;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.ah;
import com.tencent.assistant.utils.bm;
import com.tencent.downloadsdk.ae;
import com.tencent.pangu.model.AbstractDownloadInfo;
import com.tencent.pangu.model.a;

/* compiled from: ProGuard */
class w implements ae {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ r f3874a;

    w(r rVar) {
        this.f3874a = rVar;
    }

    public void a(int i, String str, String str2) {
        q qVar = (q) this.f3874a.f3869a.get(str);
        if (qVar != null) {
            qVar.q = str2;
            qVar.r = str2;
            qVar.s = AbstractDownloadInfo.DownState.SUCC;
            qVar.t = 0;
            qVar.p = System.currentTimeMillis();
            this.f3874a.c.sendMessage(this.f3874a.c.obtainMessage(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_SUCC, qVar));
            this.f3874a.b.a(qVar);
        }
    }

    public void a(int i, String str) {
        q qVar = (q) this.f3874a.f3869a.get(str);
        if (qVar != null) {
            qVar.s = AbstractDownloadInfo.DownState.DOWNLOADING;
            qVar.t = 0;
            this.f3874a.c.sendMessage(this.f3874a.c.obtainMessage(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_START, qVar));
            this.f3874a.b.a(qVar);
        }
    }

    public void a(int i, String str, long j, String str2, String str3) {
        q qVar = (q) this.f3874a.f3869a.get(str);
        if (qVar != null) {
            if (qVar.u == null) {
                qVar.u = new a();
            }
            qVar.u.f3881a = j;
            qVar.n = j;
            this.f3874a.b.a(qVar);
        }
    }

    public void a(int i, String str, long j, long j2, double d) {
        q qVar = (q) this.f3874a.f3869a.get(str);
        if (qVar != null) {
            if (qVar.s != AbstractDownloadInfo.DownState.DOWNLOADING) {
                this.f3874a.c.sendMessage(this.f3874a.c.obtainMessage(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_START, qVar));
            }
            qVar.s = AbstractDownloadInfo.DownState.DOWNLOADING;
            qVar.t = 0;
            qVar.u.b = j2;
            qVar.u.f3881a = j;
            qVar.u.c = bm.a(d);
            this.f3874a.c.sendMessage(this.f3874a.c.obtainMessage(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOADING, qVar));
        }
    }

    public void a(int i, String str, String str2, String str3) {
        q qVar = (q) this.f3874a.f3869a.get(str);
        if (qVar != null) {
            qVar.r = str2;
            qVar.s = AbstractDownloadInfo.DownState.SUCC;
            qVar.t = 0;
            qVar.p = System.currentTimeMillis();
            qVar.u.b = qVar.u.f3881a;
            this.f3874a.c.sendMessage(this.f3874a.c.obtainMessage(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_SUCC, qVar));
            this.f3874a.b.a(qVar);
        }
    }

    public void a(int i, String str, int i2, byte[] bArr, String str2) {
        q qVar = (q) this.f3874a.f3869a.get(str);
        if (qVar != null) {
            qVar.s = AbstractDownloadInfo.DownState.FAIL;
            qVar.t = i2;
            this.f3874a.c.sendMessage(this.f3874a.c.obtainMessage(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_FAIL, qVar));
            this.f3874a.b.a(qVar);
        }
        if (i2 == -12) {
            TemporaryThreadManager.get().start(new x(this));
        } else if (i2 == -11) {
            ah.a().post(new y(this));
        }
        FileUtil.tryRefreshPath(i2);
    }

    public void b(int i, String str) {
        q qVar = (q) this.f3874a.f3869a.get(str);
        if (qVar == null) {
            return;
        }
        if (qVar.s == AbstractDownloadInfo.DownState.QUEUING || qVar.s == AbstractDownloadInfo.DownState.DOWNLOADING) {
            qVar.s = AbstractDownloadInfo.DownState.PAUSED;
            qVar.t = 0;
            this.f3874a.c.sendMessage(this.f3874a.c.obtainMessage(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_PAUSE, qVar));
            this.f3874a.b.a(qVar);
        }
    }

    public void b(int i, String str, String str2) {
    }
}
