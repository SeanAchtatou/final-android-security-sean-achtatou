package com.tencent.pangu.mediadownload;

import com.tencent.assistant.AppConst;
import com.tencent.assistant.module.k;
import com.tencent.pangu.download.SimpleDownloadInfo;

/* compiled from: ProGuard */
public abstract class d {
    public abstract String q();

    public abstract SimpleDownloadInfo.DownloadType r();

    public AppConst.AppState u() {
        return k.a(this);
    }
}
