package com.tencent.pangu.download;

import com.tencent.assistant.AppConst;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public abstract class SimpleDownloadInfo {
    public List<String> apkUrlList = new ArrayList();
    public long categoryId;
    public long createTime = 0;
    public long downloadEndTime = 0;
    public DownloadState downloadState = DownloadState.INIT;
    public String downloadTicket;
    public String downloadingPath = Constants.STR_EMPTY;
    public int errorCode = 0;
    public String fileMd5 = Constants.STR_EMPTY;
    public String filePath = Constants.STR_EMPTY;
    public long fileSize = 0;
    public DownloadType fileType;
    public String iconUrl = Constants.STR_EMPTY;
    public String name;
    public m response = new m();
    public int scene = 2000;
    public StatInfo statInfo = new StatInfo();

    /* compiled from: ProGuard */
    public enum DownloadState {
        INIT,
        DOWNLOADING,
        PAUSED,
        FAIL,
        SUCC,
        ILLEGAL,
        QUEUING,
        COMPLETE,
        INSTALLING,
        DELETED,
        INSTALLED,
        WAITTING_FOR_WIFI
    }

    /* compiled from: ProGuard */
    public enum DownloadType {
        APK,
        PLUGIN,
        VIDEO,
        MUSIC,
        WALLPAPER,
        EBOOX,
        RINGTONE,
        FILE,
        OTHER
    }

    /* compiled from: ProGuard */
    public enum UIType {
        NORMAL,
        WISE_APP_UPDATE,
        WISE_NEW_DOWNLOAD,
        WISE_SELF_UPDAET,
        PLUGIN_PREDOWNLOAD,
        WISE_BOOKING_DOWNLOAD,
        WISE_SUBSCRIPTION_DOWNLOAD
    }

    public String getDownloadingPath() {
        return this.downloadingPath;
    }

    public String getFilePath() {
        return this.filePath;
    }

    public static int getPercent(SimpleDownloadInfo simpleDownloadInfo) {
        if (simpleDownloadInfo == null || simpleDownloadInfo.response == null) {
            return 0;
        }
        return (int) ((((double) simpleDownloadInfo.response.f3766a) / ((double) simpleDownloadInfo.response.b)) * 100.0d);
    }

    public static double getPercentFloat(SimpleDownloadInfo simpleDownloadInfo) {
        if (simpleDownloadInfo == null || simpleDownloadInfo.response == null || simpleDownloadInfo.response.b <= 0) {
            return 0.0d;
        }
        return (((double) simpleDownloadInfo.response.f3766a) / ((double) simpleDownloadInfo.response.b)) * 100.0d;
    }

    public static boolean isProgressShowFake(DownloadInfo downloadInfo, AppConst.AppState appState) {
        if (downloadInfo == null || downloadInfo.response == null || appState == null) {
            return false;
        }
        if ((appState == AppConst.AppState.DOWNLOADING || appState == AppConst.AppState.PAUSED || (downloadInfo.response.b > 0 && appState == AppConst.AppState.FAIL)) && downloadInfo.response.f > getPercent(downloadInfo)) {
            return true;
        }
        return false;
    }

    public static long getDownloadDisplayByteSize(DownloadInfo downloadInfo) {
        if (downloadInfo == null) {
            return 0;
        }
        if (downloadInfo.response != null && downloadInfo.response.b > 0) {
            return downloadInfo.response.b;
        }
        if (downloadInfo.isSslUpdate()) {
            return downloadInfo.sllFileSize;
        }
        return downloadInfo.fileSize;
    }

    public void setFilePath(String str) {
        this.filePath = str;
    }
}
