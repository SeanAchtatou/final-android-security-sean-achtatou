package com.tencent.pangu.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import com.tencent.assistant.component.listener.OnUserTaskClickListener;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.utils.bm;
import com.tencent.pangu.link.b;

/* compiled from: ProGuard */
class ao extends OnUserTaskClickListener {
    final /* synthetic */ DownloadActivity b;

    ao(DownloadActivity downloadActivity) {
        this.b = downloadActivity;
    }

    public void OnUserTaskClick(View view) {
        String scheme;
        Uri parse = Uri.parse(this.b.aa.g.f1125a);
        Intent intent = new Intent("android.intent.action.VIEW", parse);
        if (b.a(this.b.n, intent) && (scheme = intent.getScheme()) != null) {
            if (scheme.equals("tmast") || scheme.equals("http")) {
                Bundle bundle = new Bundle();
                int a2 = bm.a(parse.getQueryParameter("scene"), 0);
                if (a2 != 0) {
                    bundle.putInt(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, a2);
                } else {
                    bundle.putInt(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, this.b.f());
                }
                b.b(this.b.n, this.b.aa.g.f1125a, bundle);
            }
        }
    }
}
