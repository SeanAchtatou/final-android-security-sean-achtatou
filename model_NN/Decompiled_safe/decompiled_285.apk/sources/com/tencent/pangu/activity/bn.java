package com.tencent.pangu.activity;

import android.support.v4.view.ViewPager;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
class bn implements ViewPager.OnPageChangeListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PopUpNecessaryAcitivity f3338a;

    bn(PopUpNecessaryAcitivity popUpNecessaryAcitivity) {
        this.f3338a = popUpNecessaryAcitivity;
    }

    public void onPageSelected(int i) {
        XLog.i("PopUpNecessaryAcitivity", "onPageSelected");
        this.f3338a.v.setSelect(this.f3338a.A, i, 1);
        int unused = this.f3338a.E = i;
        this.f3338a.y.a(i);
    }

    public void onPageScrolled(int i, float f, int i2) {
    }

    public void onPageScrollStateChanged(int i) {
    }
}
