package com.tencent.pangu.activity;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.PermissionInfo;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class PermissionActivity extends BaseActivity {
    private LinearLayout n;
    private Context u;
    private LayoutInflater v;
    private ArrayList<String> w = new ArrayList<>();
    private PackageManager x;
    private SecondNavigationTitleViewV5 y;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_permission_layout);
        t();
        u();
    }

    public int f() {
        return STConst.ST_PAGE_APP_DETAIL_MORE_PERMISSION;
    }

    private void t() {
        ArrayList<String> stringArrayListExtra = getIntent().getStringArrayListExtra("com.tencent.assistant.PERMISSION_LIST");
        if (stringArrayListExtra != null) {
            this.w.addAll(stringArrayListExtra);
        }
        this.x = getPackageManager();
    }

    private void u() {
        this.u = this;
        this.y = (SecondNavigationTitleViewV5) findViewById(R.id.title_view);
        this.y.b(getResources().getString(R.string.app_permissions));
        this.y.a(this);
        this.y.d();
        this.v = LayoutInflater.from(this.u);
        this.n = (LinearLayout) findViewById(R.id.permission_detail_layout);
        a(this.w);
    }

    private void a(ArrayList<String> arrayList) {
        PermissionInfo permissionInfo;
        int i = 0;
        if (arrayList == null || arrayList.size() == 0) {
            View inflate = this.v.inflate((int) R.layout.permission_item, (ViewGroup) null);
            ((TextView) inflate.findViewById(R.id.permission_title)).setText(this.u.getString(R.string.no_any_premission));
            this.n.addView(inflate);
            return;
        }
        while (true) {
            int i2 = i;
            if (i2 < arrayList.size()) {
                try {
                    permissionInfo = this.x.getPermissionInfo(arrayList.get(i2), 0);
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                    permissionInfo = null;
                }
                if (permissionInfo != null) {
                    String str = (String) permissionInfo.loadLabel(this.x);
                    String str2 = (String) permissionInfo.loadDescription(this.x);
                    if (!TextUtils.isEmpty(str) && !TextUtils.isEmpty(str2)) {
                        View inflate2 = this.v.inflate((int) R.layout.permission_item, (ViewGroup) null);
                        ((TextView) inflate2.findViewById(R.id.permission_title)).setText(str.replace("（", "(").replace("）", ")"));
                        ((TextView) inflate2.findViewById(R.id.permission_detail)).setText(str2.replace("（", "(").replace("）", ")"));
                        this.n.addView(inflate2);
                    }
                }
                i = i2 + 1;
            } else {
                this.n.addView((LinearLayout) this.v.inflate((int) R.layout.nothing_layout, (ViewGroup) null));
                return;
            }
        }
    }
}
