package com.tencent.pangu.activity;

import android.content.DialogInterface;

/* compiled from: ProGuard */
class z implements DialogInterface.OnDismissListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppTreasureBoxActivity f3399a;

    z(AppTreasureBoxActivity appTreasureBoxActivity) {
        this.f3399a = appTreasureBoxActivity;
    }

    public void onDismiss(DialogInterface dialogInterface) {
        this.f3399a.finish();
    }
}
