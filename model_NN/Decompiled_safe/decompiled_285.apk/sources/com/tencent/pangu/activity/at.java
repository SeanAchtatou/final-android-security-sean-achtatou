package com.tencent.pangu.activity;

import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.manager.webview.component.c;

/* compiled from: ProGuard */
class at implements c {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ExternalCallActivity f3319a;

    at(ExternalCallActivity externalCallActivity) {
        this.f3319a = externalCallActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.component.SecondNavigationTitleViewV5.a(boolean, int):void
     arg types: [int, ?]
     candidates:
      com.tencent.assistantv2.component.SecondNavigationTitleViewV5.a(com.tencent.assistantv2.component.SecondNavigationTitleViewV5, java.lang.String):com.tencent.assistantv2.st.page.STInfoV2
      com.tencent.assistantv2.component.SecondNavigationTitleViewV5.a(com.tencent.assistantv2.component.SecondNavigationTitleViewV5, boolean):boolean
      com.tencent.assistantv2.component.SecondNavigationTitleViewV5.a(java.lang.String, int):void
      com.tencent.assistantv2.component.SecondNavigationTitleViewV5.a(java.lang.String, android.text.TextUtils$TruncateAt):void
      com.tencent.assistantv2.component.SecondNavigationTitleViewV5.a(boolean, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.activity.ExternalCallActivity.a(com.tencent.pangu.activity.ExternalCallActivity, boolean):boolean
     arg types: [com.tencent.pangu.activity.ExternalCallActivity, int]
     candidates:
      com.tencent.assistant.activity.BrowserActivity.a(int, java.lang.String):java.lang.String
      com.tencent.assistant.activity.BrowserActivity.a(com.tencent.assistant.activity.BrowserActivity, java.lang.Object):java.lang.String
      com.tencent.assistant.activity.BrowserActivity.a(com.tencent.assistant.activity.BrowserActivity, java.lang.String):java.lang.String
      com.tencent.assistant.activity.BrowserActivity.a(android.content.Intent, boolean):void
      com.tencent.assistant.activity.BrowserActivity.a(int, int):boolean
      com.tencent.assistant.activity.BaseActivity.a(java.lang.String, java.lang.String):void
      com.tencent.pangu.activity.ExternalCallActivity.a(com.tencent.pangu.activity.ExternalCallActivity, boolean):boolean */
    public void a(int i, int i2, int i3, int i4) {
        if (this.f3319a.G && i2 > i4) {
            this.f3319a.E.startAnimation(AnimationUtils.loadAnimation(this.f3319a.n, R.anim.float_banner_header_out_fade));
            this.f3319a.E.setVisibility(8);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
            layoutParams.addRule(3, R.id.browser_header_view);
            this.f3319a.u.setLayoutParams(layoutParams);
            this.f3319a.v.a(true, (int) R.anim.float_banner_header_in_fade);
            boolean unused = this.f3319a.G = false;
            this.f3319a.a((String) null, 100, 2);
        }
    }
}
