package com.tencent.pangu.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.a.a;
import com.tencent.assistant.component.LoadingView;
import com.tencent.assistant.component.NormalErrorRecommendPage;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.manager.NetworkMonitor;
import com.tencent.assistant.manager.t;
import com.tencent.assistant.manager.webview.component.TxWebViewContainer;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.net.APN;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.protocol.jce.AdvancedHotWord;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ah;
import com.tencent.assistantv2.component.TabBarView;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.adapter.SearchMatchAdapter;
import com.tencent.pangu.component.search.ISearchResultPage;
import com.tencent.pangu.component.search.SearchBarView;
import com.tencent.pangu.component.search.SearchHotwordsView;
import com.tencent.pangu.component.search.SearchResultTabPagesBase;
import com.tencent.pangu.component.search.WebSearchResultPage;
import com.tencent.pangu.component.search.g;
import com.tencent.pangu.component.search.j;
import com.tencent.pangu.component.search.m;
import com.tencent.pangu.link.b;
import com.tencent.pangu.model.a.f;
import com.tencent.pangu.module.h;
import com.tencent.pangu.module.p;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/* compiled from: ProGuard */
public class SearchActivity extends ShareBaseActivity implements NetworkMonitor.ConnectivityChangeListener {
    /* access modifiers changed from: private */
    public static int L = 30;
    /* access modifiers changed from: private */
    public SearchMatchAdapter A;
    /* access modifiers changed from: private */
    public NormalErrorRecommendPage B;
    /* access modifiers changed from: private */
    public LoadingView C;
    private TxWebViewContainer D;
    private String E;
    private h F;
    /* access modifiers changed from: private */
    public p G;
    private ck H = new ck(this, null);
    /* access modifiers changed from: private */
    public int I = 0;
    /* access modifiers changed from: private */
    public boolean J = true;
    /* access modifiers changed from: private */
    public Handler K = null;
    private final int M = EventDispatcherEnum.PLUGIN_EVENT_LOGIN_START;
    /* access modifiers changed from: private */
    public int N = STConst.ST_PAGE_SEARCH;
    /* access modifiers changed from: private */
    public int O = 0;
    /* access modifiers changed from: private */
    public String P = null;
    /* access modifiers changed from: private */
    public String Q = null;
    private ApkResCallback.Stub R = new bu(this);
    private boolean S = false;
    /* access modifiers changed from: private */
    public boolean T = false;
    /* access modifiers changed from: private */
    public int U = 0;
    private TextWatcher V = new cf(this);
    private View.OnClickListener W = new cg(this);
    private View.OnClickListener X = new ci(this);
    private View.OnClickListener Y = new cj(this);
    private g Z = new bw(this);
    private View.OnClickListener aa = new bx(this);
    private View.OnClickListener ab = new by(this);
    private View.OnKeyListener ac = new bz(this);
    private View.OnClickListener ad = new cb(this);
    private APN ae = APN.NO_NETWORK;
    boolean n = false;
    private final String u = "SearchActivity";
    /* access modifiers changed from: private */
    public SearchBarView v;
    /* access modifiers changed from: private */
    public SearchHotwordsView w;
    /* access modifiers changed from: private */
    public View x;
    private ListView y;
    /* access modifiers changed from: private */
    public m z;

    /* compiled from: ProGuard */
    enum Layer {
        Search,
        Result
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        try {
            setContentView((int) R.layout.act_search_layout);
            this.K = new cc(this);
            v();
            w();
            t.a().a(this);
        } catch (Throwable th) {
            this.S = true;
            finish();
            t.a().b();
        }
    }

    private void v() {
        this.G = new p();
        this.F = h.a();
    }

    private void w() {
        this.B = (NormalErrorRecommendPage) findViewById(R.id.network_error);
        if (this.B != null) {
            this.B.setButtonClickListener(this.ad);
        }
        this.C = (LoadingView) findViewById(R.id.loading_view);
        this.v = (SearchBarView) findViewById(R.id.search_bar);
        this.v.a(this.V);
        this.v.a(this.W);
        this.v.b(this.X);
        this.v.c(this.ab);
        this.v.setOnKeyListener(this.ac);
        this.x = findViewById(R.id.search_scroll_view);
        this.w = (SearchHotwordsView) findViewById(R.id.search_hot_view);
        this.D = (TxWebViewContainer) findViewById(R.id.webviewcontainer);
        this.w.a(this.Z);
        this.w.a(this.aa);
        this.w.b(this.Y);
        this.w.setVisibility(8);
        this.x.setVisibility(8);
        this.w.setOnClickListener(new cd(this));
        this.y = (ListView) findViewById(R.id.match_list);
        this.A = new SearchMatchAdapter(this, this.y, this.Y);
        this.A.a((int) STConst.ST_PAGE_SEARCH_SUGGEST);
        this.y.setAdapter((ListAdapter) this.A);
        this.y.setDivider(null);
        this.z = new m(this, (TabBarView) findViewById(R.id.search_result_tab_bar), (ViewPager) findViewById(R.id.search_result_tab_content));
        this.C.setVisibility(0);
        String stringExtra = getIntent().getStringExtra("com.tencent.assistant.KEYWORD");
        this.N = getIntent().getIntExtra("com.tencent.assistant.SOURCESCENE", this.N);
        this.P = getIntent().getStringExtra(a.Y);
        this.Q = getIntent().getStringExtra(a.Z);
        this.z.d(0);
        this.O = getIntent().getIntExtra(a.aa, 0);
        if (!TextUtils.isEmpty(this.P)) {
            this.v.a(this.P);
        }
        if (stringExtra == null || stringExtra.length() <= 0 || TextUtils.isEmpty(stringExtra.trim())) {
            C();
        } else {
            this.n = true;
            this.J = false;
            String b = b(stringExtra);
            if (!TextUtils.isEmpty(b)) {
                stringExtra = b;
            }
            if (stringExtra.length() > L) {
                stringExtra = stringExtra.substring(0, L);
            }
            this.v.a().setText(stringExtra);
            this.v.a().setSelection(stringExtra.length());
            F();
            this.z.a(b, this.N, (SimpleAppModel) null);
        }
        y();
        x();
        I();
    }

    private void x() {
        View findViewById = getWindow().getDecorView().findViewById(R.id.search_layout);
        findViewById.getViewTreeObserver().addOnGlobalLayoutListener(new ce(this, findViewById));
    }

    private String b(String str) {
        int indexOf;
        if (!TextUtils.isEmpty(str) && (indexOf = str.indexOf("：")) > 0 && indexOf < str.length()) {
            return str.substring(indexOf + 1);
        }
        return str;
    }

    /* access modifiers changed from: private */
    public void y() {
        XLog.d("SearchActivity", "loadHotWords:" + this.O);
        f b = this.F.b(this.O);
        if (b != null) {
            XLog.d("SearchActivity", "loadHotWords:" + this.O + ":" + b.b());
            this.w.a(b);
            return;
        }
        XLog.d("SearchActivity", "loadHotWords:model == null");
        this.F.b();
    }

    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (!this.S) {
            setIntent(intent);
            int intExtra = getIntent().getIntExtra(a.ab, -1);
            if (intExtra != -1) {
                this.z.b(intExtra);
                return;
            }
            v();
            w();
        }
    }

    public void onResume() {
        super.onResume();
        if (!this.S) {
            XLog.d("SearchActivity", "onResume");
            this.G.register(this.H);
            this.F.register(this.H);
            this.z.h();
            this.A.a();
            this.A.notifyDataSetChanged();
            this.w.a();
            if (this.D != null) {
                this.D.a();
            }
            ApkResourceManager.getInstance().registerApkResCallback(this.R);
            if (this.y.getVisibility() == 0 || this.w.getVisibility() == 0) {
                B();
            } else {
                A();
            }
        }
    }

    public void onPause() {
        XLog.d("SearchActivity", "onPause");
        if (this.S) {
            super.onPause();
            return;
        }
        A();
        this.A.b();
        this.z.g();
        if (this.D != null) {
            this.D.b();
        }
        ApkResourceManager.getInstance().unRegisterApkResCallback(this.R);
        super.onPause();
    }

    public void onDestroy() {
        if (this.S) {
            super.onDestroy();
            return;
        }
        this.F.unregister(this.H);
        this.G.a();
        this.G.unregister(this.H);
        this.z.i();
        if (this.D != null) {
            this.D.c();
        }
        this.A.c();
        this.K.removeMessages(0);
        this.K = null;
        this.B.destory();
        this.B = null;
        try {
            Class.forName("android.view.inputmethod.InputMethodManager").getMethod("wind‌​owDismissed", IBinder.class).invoke(null, this.v.a().getWindowToken());
        } catch (Exception e) {
        }
        this.v = null;
        t.a().b(this);
        super.onDestroy();
    }

    /* access modifiers changed from: private */
    public void z() {
        a(SearchResultTabPagesBase.TabType.NONE, (SimpleAppModel) null);
    }

    /* access modifiers changed from: private */
    public void a(SimpleAppModel simpleAppModel) {
        a(SearchResultTabPagesBase.TabType.ALL, simpleAppModel);
    }

    private void a(SearchResultTabPagesBase.TabType tabType, SimpleAppModel simpleAppModel) {
        A();
        String obj = this.v.a().getText().toString();
        if (!TextUtils.isEmpty(obj)) {
            if (f() != this.z.b()) {
                a(n(), (String) null, (String) null);
            }
            F();
            ah.a().postDelayed(new ch(this, tabType, obj, simpleAppModel), 300);
        }
    }

    /* access modifiers changed from: private */
    public void a(AdvancedHotWord advancedHotWord) {
        this.N = STConst.ST_PAGE_SEARCH_HOTWORDS;
        A();
        this.n = true;
        String str = advancedHotWord.f1126a;
        if (advancedHotWord.b == 2 && advancedHotWord.d != null) {
            str = advancedHotWord.d.f.b;
        }
        if (!TextUtils.isEmpty(str)) {
            this.v.a().setText(str);
            this.v.a().setSelection(str.length() > L ? L : str.length());
            this.x.setVisibility(8);
            this.w.setVisibility(8);
            a(SearchResultTabPagesBase.TabType.ALL, (SimpleAppModel) null);
        }
    }

    /* access modifiers changed from: private */
    public void b(AdvancedHotWord advancedHotWord) {
        if (!TextUtils.isEmpty(advancedHotWord.f)) {
            Bundle bundle = new Bundle();
            bundle.putInt(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, f());
            b.a(this, advancedHotWord.f, bundle);
        }
    }

    /* access modifiers changed from: private */
    public void c(AdvancedHotWord advancedHotWord) {
        if (!TextUtils.isEmpty(advancedHotWord.f)) {
            Bundle bundle = new Bundle();
            bundle.putInt(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, f());
            b.a(this, advancedHotWord.f, bundle);
        }
    }

    /* access modifiers changed from: private */
    public void A() {
        if (this.v != null && this.v.a() != null) {
            EditText a2 = this.v.a();
            a2.clearFocus();
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService("input_method");
            if (inputMethodManager != null) {
                inputMethodManager.hideSoftInputFromWindow(a2.getWindowToken(), 2);
            }
        }
    }

    /* access modifiers changed from: private */
    public void B() {
        if (this.v != null) {
            this.v.a().requestFocus();
            ah.a().postDelayed(new ca(this), 500);
        }
    }

    /* access modifiers changed from: private */
    public void c(int i) {
        this.y.setVisibility(i);
        this.C.setVisibility(8);
    }

    /* access modifiers changed from: private */
    public void d(int i) {
        this.C.setVisibility(8);
        this.z.e(8);
        c(8);
        this.x.setVisibility(8);
        this.w.setVisibility(8);
        this.B.setVisibility(0);
        this.B.setErrorType(i);
        getWindow().setSoftInputMode(32);
    }

    /* access modifiers changed from: private */
    public void C() {
        this.C.setVisibility(8);
        this.z.e(8);
        c(8);
        this.x.setVisibility(0);
        this.w.setVisibility(0);
        this.B.setVisibility(8);
    }

    /* access modifiers changed from: private */
    public void D() {
        t();
        this.C.setVisibility(8);
        this.z.e(8);
        c(0);
        this.x.setVisibility(8);
        this.w.setVisibility(8);
        this.B.setVisibility(8);
        q();
    }

    private void F() {
        this.C.setVisibility(8);
        this.z.e(0);
        c(8);
        this.x.setVisibility(8);
        this.w.setVisibility(8);
        this.B.setVisibility(8);
    }

    /* access modifiers changed from: private */
    public void G() {
        this.C.setVisibility(8);
        this.z.e(8);
        c(8);
        this.x.setVisibility(8);
        this.w.setVisibility(8);
        this.B.setVisibility(8);
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4 || TextUtils.isEmpty(this.v.a().getText().toString())) {
            a("001", 200);
            return super.onKeyDown(i, keyEvent);
        }
        this.v.a().setText(Constants.STR_EMPTY);
        this.v.a().setSelection(0);
        return true;
    }

    /* access modifiers changed from: private */
    public Layer H() {
        if (this.z == null || this.z.j() != 0) {
            return Layer.Search;
        }
        return Layer.Result;
    }

    public int f() {
        if (this.y != null && this.y.getVisibility() == 0) {
            return STConst.ST_PAGE_SEARCH_SUGGEST;
        }
        if (this.z == null || this.z.j() != 0) {
            return STConst.ST_PAGE_SEARCH;
        }
        return this.z.b();
    }

    public boolean g() {
        return true;
    }

    public void t() {
        a(n(), (String) null, (String) null);
    }

    /* access modifiers changed from: private */
    public void a(String str, int i) {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this, i);
        buildSTInfo.slotId = com.tencent.assistantv2.st.page.a.a("03", str);
        buildSTInfo.extraData = Constants.STR_EMPTY;
        l.a(buildSTInfo);
    }

    public void onConnected(APN apn) {
        String str = null;
        if (this.v != null) {
            str = this.v.b();
        }
        if (H() == Layer.Search) {
            XLog.d("SearchActivity", "onConnected:Layer.Search");
            if (str == null || TextUtils.isEmpty(str.trim())) {
                if (this.F != null) {
                    f b = this.F.b(this.O);
                    if (b == null || b.b() <= 0) {
                        this.F.b();
                    }
                }
            } else if ((this.A == null || this.A.getCount() <= 0) && this.G != null) {
                this.G.a();
                this.G.a(str);
            }
        } else if (H() == Layer.Result && this.z != null) {
            this.z.a(str, this.N);
        }
    }

    public void onDisconnected(APN apn) {
        this.ae = apn;
    }

    public void onConnectivityChanged(APN apn, APN apn2) {
        this.ae = apn2;
    }

    private void I() {
        this.D.n();
        this.D.b(getResources().getColor(R.color.apk_list_bg));
        j a2 = m.a(SearchResultTabPagesBase.TabType.STARTPAGE);
        this.E = a2.d();
        if (a2.a() != ISearchResultPage.PageType.WEB || TextUtils.isEmpty(this.E)) {
            XLog.d("SearchActivity", "Native page is loaded!");
            this.D.setVisibility(8);
            this.x.setVisibility(0);
            this.D.a("about:blank");
            return;
        }
        XLog.d("SearchActivity", "H5 page is loaded! mUrl:" + this.E);
        this.D.setVisibility(0);
        this.x.setVisibility(8);
        this.D.a("about:blank");
        this.D.a(J());
    }

    private String J() {
        UnsupportedEncodingException e;
        String str;
        if (TextUtils.isEmpty(this.E)) {
            return this.E;
        }
        String str2 = Constants.STR_EMPTY;
        try {
            str2 = ("srcscene=" + this.N) + "&" + "mobileinfo" + "=" + URLEncoder.encode(WebSearchResultPage.l(), "utf-8");
            str = str2 + "&" + "opentime" + "=" + System.currentTimeMillis();
            try {
                XLog.d("SearchActivity", "params:" + str);
            } catch (UnsupportedEncodingException e2) {
                e = e2;
                e.printStackTrace();
                XLog.d("SearchActivity", String.format(this.E, str));
                return String.format(this.E, str);
            }
        } catch (UnsupportedEncodingException e3) {
            UnsupportedEncodingException unsupportedEncodingException = e3;
            str = str2;
            e = unsupportedEncodingException;
            e.printStackTrace();
            XLog.d("SearchActivity", String.format(this.E, str));
            return String.format(this.E, str);
        }
        XLog.d("SearchActivity", String.format(this.E, str));
        return String.format(this.E, str);
    }
}
