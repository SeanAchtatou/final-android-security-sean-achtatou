package com.tencent.pangu.component;

import android.view.View;
import android.widget.AdapterView;
import com.tencent.assistant.model.SimpleAppModel;

/* compiled from: ProGuard */
class v implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PopUpContentView f3743a;

    v(PopUpContentView popUpContentView) {
        this.f3743a = popUpContentView;
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        this.f3743a.c.c(i);
        this.f3743a.e.a(((SimpleAppModel) this.f3743a.d.get(i)).k, this.f3743a.c.b(i));
    }
}
