package com.tencent.pangu.component.appdetail.process;

import android.os.Bundle;
import com.tencent.assistant.AppConst;
import com.tencent.nucleus.socialcontact.login.j;
import com.tencent.nucleus.socialcontact.login.l;

/* compiled from: ProGuard */
class o extends AppConst.TwoBtnDialogInfo {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ n f3614a;

    o(n nVar) {
        this.f3614a = nVar;
    }

    public void onLeftBtnClick() {
    }

    public void onRightBtnClick() {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConst.KEY_LOGIN_TYPE, 5);
        bundle.putInt(AppConst.KEY_FROM_TYPE, 12);
        l.a(12);
        j.a().a(AppConst.IdentityType.MOBILEQ, bundle);
    }

    public void onCancell() {
    }
}
