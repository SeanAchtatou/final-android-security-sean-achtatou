package com.tencent.pangu.component.homeEntry;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.Html;
import android.text.TextUtils;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.protocol.jce.EntranceBlock;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.by;

/* compiled from: ProGuard */
public class HomeEntryCell1 extends HomeEntryCellBase {
    private TXImageView c;
    private TextView d;

    public HomeEntryCell1(Context context, EntranceBlock entranceBlock) {
        super(context, entranceBlock);
    }

    public void a() {
        inflate(this.f3671a, R.layout.home_entry_template_cell1, this);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        layoutParams.width = by.a(this.f3671a, 86.0f);
        layoutParams.height = by.a(this.f3671a, 74.0f);
        setLayoutParams(layoutParams);
        this.c = (TXImageView) findViewById(R.id.icon);
        this.d = (TextView) findViewById(R.id.title);
        setGravity(1);
        setPadding(0, by.a(this.f3671a, 10.0f), 0, by.a(this.f3671a, 9.0f));
    }

    public void b() {
        if (this.b != null) {
            XLog.v("TAG", "fillValue--name= " + this.b.b);
            if (!TextUtils.isEmpty(this.b.b)) {
                this.d.setText(Html.fromHtml(this.b.b));
            }
        }
    }

    public void a(Bitmap bitmap) {
        this.c.setImageBitmap(bitmap);
    }
}
