package com.tencent.pangu.component.appdetail;

import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;

/* compiled from: ProGuard */
class y implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppdetailScrollView f3625a;
    private long b = -1;
    private long c = 250;
    private int d = 0;
    private Interpolator e = new DecelerateInterpolator();
    private boolean f = true;

    public y(AppdetailScrollView appdetailScrollView, boolean z) {
        this.f3625a = appdetailScrollView;
        this.f = z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.component.appdetail.AppdetailScrollView.a(com.tencent.pangu.component.appdetail.AppdetailScrollView, boolean):boolean
     arg types: [com.tencent.pangu.component.appdetail.AppdetailScrollView, int]
     candidates:
      com.tencent.pangu.component.appdetail.AppdetailScrollView.a(com.tencent.pangu.component.appdetail.AppdetailScrollView, int):void
      com.tencent.pangu.component.appdetail.AppdetailScrollView.a(com.tencent.pangu.component.appdetail.AppdetailScrollView, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    public void run() {
        if (this.b == -1) {
            this.b = System.currentTimeMillis();
        } else {
            int round = Math.round(this.e.getInterpolation(((float) Math.max(Math.min(((System.currentTimeMillis() - this.b) * 1000) / this.c, 1000L), 0L)) / 1000.0f) * ((float) this.f3625a.f3536a));
            if (this.f) {
                this.d = round - this.f3625a.getScrollY();
                this.f3625a.b(-this.d);
            } else {
                this.d = round - (this.f3625a.f3536a - this.f3625a.getScrollY());
                this.f3625a.b(this.d);
            }
        }
        if ((!this.f || this.f3625a.getScrollY() < this.f3625a.f3536a) && (this.f || this.f3625a.getScrollY() > 0)) {
            this.f3625a.post(this);
            return;
        }
        this.f3625a.removeCallbacks(this);
        boolean unused = this.f3625a.p = false;
        if (this.f3625a.i != null) {
            this.f3625a.i.a(this.f);
        }
    }
}
