package com.tencent.pangu.component.appdetail;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.k;
import com.tencent.assistant.protocol.jce.CardItem;
import com.tencent.assistant.protocol.jce.GetRecommendAppListResponse;
import com.tencent.assistant.protocol.jce.RecommendAppInfo;
import com.tencent.assistant.protocol.jce.SimpleAppInfo;
import com.tencent.assistantv2.st.page.a;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class AppdetailRelatedViewV5 extends LinearLayout {
    private static int g = 1;
    private RecommendAuthorViewStatus A = RecommendAuthorViewStatus.LOADING;
    private List<RecommendAppInfo> B = new ArrayList();
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Context f3534a;
    private ViewGroup b;
    private RecommendAppViewV5 c;
    private RecommendAppViewV5 d;
    private List<RecommendAppInfo> e = new ArrayList();
    private List<SimpleAppInfo> f = new ArrayList();
    private boolean h = false;
    private ViewGroup i;
    private ViewGroup j;
    private CustomTextView k;
    private TextView l;
    private LinearLayout m;
    private View n;
    private View o;
    /* access modifiers changed from: private */
    public String p;
    private ViewGroup q;
    private ImageView r;
    private View s;
    private ImageView t;
    private final int u = 1;
    private final int v = 2;
    private final int w = 3;
    private final int x = -1;
    private boolean y = false;
    private RecommendAuthorViewStatus z = RecommendAuthorViewStatus.LOADING;

    /* compiled from: ProGuard */
    enum RecommendAuthorViewStatus {
        LOADING,
        NEEDFEED,
        FINISH
    }

    public AppdetailRelatedViewV5(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    public AppdetailRelatedViewV5(Context context) {
        super(context);
        a(context);
    }

    private void a(Context context) {
        this.f3534a = context;
        LayoutInflater.from(context).inflate((int) R.layout.appdetail_relate_layout_v5, this);
        d();
        e();
    }

    private void d() {
        this.b = (ViewGroup) findViewById(R.id.relate_app_layouts);
        this.c = (RecommendAppViewV5) findViewById(R.id.same_category_soft_gdv1);
        this.c.a(Constants.VIA_REPORT_TYPE_JOININ_GROUP);
        this.c.b(1);
        this.c.setVisibility(8);
        this.d = (RecommendAppViewV5) findViewById(R.id.same_tag_apps);
        this.d.a(Constants.VIA_REPORT_TYPE_SET_AVATAR);
        f();
        b(3);
    }

    private void b(int i2) {
        switch (i2) {
            case -1:
            case 2:
                this.c.setVisibility(8);
                a(false);
                if (!this.y) {
                    this.b.setVisibility(8);
                    return;
                }
                return;
            case 0:
            default:
                return;
            case 1:
                if (!this.y) {
                    this.c.setVisibility(0);
                }
                a(false);
                return;
            case 3:
                this.c.setVisibility(8);
                a(true);
                return;
        }
    }

    private void a(boolean z2) {
        if (!z2) {
            this.s.setVisibility(8);
            this.t.setVisibility(8);
            this.t.setAnimation(null);
            return;
        }
        this.s.setVisibility(0);
        this.t.setVisibility(0);
        this.t.startAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.circle));
    }

    private void e() {
        this.i = (ViewGroup) findViewById(R.id.author_other_work_layout);
        this.j = (ViewGroup) findViewById(R.id.author_other_work_title_layout);
        this.k = (CustomTextView) findViewById(R.id.author_other_title);
        this.l = (TextView) findViewById(R.id.other_apps);
        this.m = (LinearLayout) findViewById(R.id.author_other_app_layouts);
        this.n = findViewById(R.id.author_more_icon);
        this.o = findViewById(R.id.other_apps_txt);
        this.q = (ViewGroup) findViewById(R.id.author_wrok_loading_view_layout);
        this.r = (ImageView) findViewById(R.id.author_wrok_loading_view);
        c(3);
    }

    private void c(int i2) {
        switch (i2) {
            case -1:
            case 2:
                d(8);
                b(false);
                return;
            case 0:
            default:
                return;
            case 1:
                d(0);
                b(false);
                return;
            case 3:
                d(8);
                b(true);
                return;
        }
    }

    private void b(boolean z2) {
        if (!z2) {
            this.q.setVisibility(8);
            this.r.setVisibility(8);
            this.r.setAnimation(null);
            return;
        }
        this.q.setVisibility(0);
        this.r.setVisibility(0);
        this.r.startAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.circle));
    }

    private void d(int i2) {
        this.m.setVisibility(i2);
        this.j.setVisibility(i2);
    }

    private void f() {
        this.s = findViewById(R.id.loading_ly);
        this.t = (ImageView) findViewById(R.id.loading_view);
    }

    public void a(GetRecommendAppListResponse getRecommendAppListResponse) {
        List list;
        int i2 = 0;
        List arrayList = new ArrayList();
        if (getRecommendAppListResponse != null) {
            this.f = getRecommendAppListResponse.d;
            this.e = getRecommendAppListResponse.b;
        }
        if (this.e != null && this.e.size() > 0) {
            List<RecommendAppInfo> b2 = k.b(this.e);
            this.f = k.c(this.f);
            if (b2 != null && b2.size() > 0) {
                arrayList.addAll(b2);
            }
        }
        if (this.f == null) {
            this.f = new ArrayList();
        }
        if (arrayList.size() < 4) {
            int size = 4 - arrayList.size();
            if (this.e != null && this.e.size() > 0) {
                ArrayList arrayList2 = new ArrayList(this.e);
                arrayList2.removeAll(arrayList);
                while (true) {
                    int i3 = i2;
                    if (i3 >= arrayList2.size() || i3 >= size) {
                        break;
                    }
                    arrayList.add((RecommendAppInfo) arrayList2.get(i3));
                    i2 = i3 + 1;
                }
            }
            list = arrayList;
        } else {
            List subList = arrayList.subList(0, 4);
            if (this.f.size() >= 4) {
                this.f.subList(0, 4);
            }
            list = subList;
        }
        if (this.e != null) {
            this.z = RecommendAuthorViewStatus.FINISH;
            a(this.e, list);
            this.e.clear();
            this.e.addAll(list);
            g();
        }
    }

    public void a(String str, ArrayList<CardItem> arrayList, int i2) {
        if ((i2 & 1) == 0) {
            this.c.setVisibility(8);
            this.d.setVisibility(0);
            this.b.setVisibility(0);
            this.y = true;
            this.d.a(this.y);
            this.z = RecommendAuthorViewStatus.FINISH;
            this.d.a(str, 4, k.a(arrayList));
        }
    }

    private void a(List<RecommendAppInfo> list, List<RecommendAppInfo> list2) {
        if (!(this.A == RecommendAuthorViewStatus.FINISH || list == null || list.size() <= 0)) {
            ArrayList arrayList = new ArrayList(list);
            arrayList.removeAll(list2);
            this.B.addAll(arrayList);
        }
        if (this.A == RecommendAuthorViewStatus.NEEDFEED) {
            this.i.setVisibility(8);
        }
    }

    private void g() {
        if (this.e == null || this.e.size() <= 0) {
            b(2);
            return;
        }
        this.c.a(g, this.e, this.f, Constants.STR_EMPTY);
        b(1);
    }

    public void a(int i2) {
        if ((i2 & 1) != 0) {
            this.b.setVisibility(8);
            b(-1);
        }
        if ((i2 & 2) != 0) {
            this.i.setVisibility(8);
            c(-1);
        }
    }

    public void a(String str, String str2, ArrayList<SimpleAppModel> arrayList, byte[] bArr, boolean z2) {
        this.h = z2;
        if (arrayList != null && arrayList.size() != 0) {
            b(str, str2, arrayList, bArr, z2);
            this.A = RecommendAuthorViewStatus.FINISH;
        } else if (this.z == RecommendAuthorViewStatus.FINISH) {
            this.i.setVisibility(8);
            this.A = RecommendAuthorViewStatus.FINISH;
        } else {
            this.A = RecommendAuthorViewStatus.FINISH;
            this.i.setVisibility(8);
            c(-1);
        }
    }

    private void b(String str, String str2, ArrayList<SimpleAppModel> arrayList, byte[] bArr, boolean z2) {
        ArrayList arrayList2 = new ArrayList();
        if (arrayList != null && arrayList.size() > 0) {
            arrayList2.addAll(arrayList);
        }
        this.p = str2;
        c(1);
        if (arrayList2.size() >= 4 && z2) {
            this.i.setOnClickListener(new w(this, str, new ArrayList(arrayList2), bArr, z2));
        }
        b(this.p);
        a(arrayList2);
    }

    /* access modifiers changed from: package-private */
    public String a() {
        return a.a(Constants.VIA_REPORT_TYPE_MAKE_FRIEND, "010");
    }

    private void a(List<SimpleAppModel> list) {
        int i2;
        this.i.setVisibility(0);
        if (list.size() < 4 || !this.h) {
            this.n.setVisibility(8);
            this.o.setVisibility(8);
            this.i.setClickable(false);
        } else {
            this.n.setVisibility(0);
            this.o.setVisibility(0);
            this.i.setClickable(true);
        }
        CustomRelateAppViewV5 customRelateAppViewV5 = new CustomRelateAppViewV5(this.f3534a);
        if (4 > list.size()) {
            i2 = list.size();
        } else {
            i2 = 4;
        }
        customRelateAppViewV5.c = 0;
        customRelateAppViewV5.d = "08";
        customRelateAppViewV5.a(list.subList(0, i2));
        this.m.addView(customRelateAppViewV5);
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) customRelateAppViewV5.getLayoutParams();
        layoutParams.topMargin = (int) this.f3534a.getResources().getDimension(R.dimen.banner_title_desc_margin);
        layoutParams.rightMargin = (int) this.f3534a.getResources().getDimension(R.dimen.app_detail_relate_margin_right);
        this.k.a(customRelateAppViewV5);
        if (list.size() > 4) {
        }
    }

    public void a(String str) {
        if (!TextUtils.isEmpty(str) && this.p == null) {
            b(str);
        }
    }

    public void b(String str) {
        if (!TextUtils.isEmpty(str)) {
            this.p = str;
            this.k.setText(str);
            if ((this.p.equals(getResources().getString(R.string.appdetail_page_default_author_title)) || this.p.equals(getResources().getString(R.string.appdetail_page_default_authhor_title_guess))) && this.l != null) {
                this.l.setVisibility(4);
            }
        }
    }

    public void b() {
        if (this.m.getChildAt(0) instanceof CustomRelateAppViewV5) {
            ((CustomRelateAppViewV5) this.m.getChildAt(0)).c();
        }
        if (this.c != null) {
            this.c.d();
        }
        if (this.d != null) {
            this.d.d();
        }
    }

    public void c() {
        if (this.m.getChildAt(0) instanceof CustomRelateAppViewV5) {
            ((CustomRelateAppViewV5) this.m.getChildAt(0)).d();
        }
        if (this.c != null) {
            this.c.c();
        }
        if (this.d != null) {
            this.d.c();
        }
    }
}
