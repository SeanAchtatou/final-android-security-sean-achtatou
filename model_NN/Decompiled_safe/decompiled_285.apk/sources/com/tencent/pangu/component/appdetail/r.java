package com.tencent.pangu.component.appdetail;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import com.tencent.assistant.activity.BrowserActivity;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.pangu.activity.AppDetailActivityV5;

/* compiled from: ProGuard */
class r extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Context f3619a;
    final /* synthetic */ AppdetailFlagView b;

    r(AppdetailFlagView appdetailFlagView, Context context) {
        this.b = appdetailFlagView;
        this.f3619a = context;
    }

    public void onTMAClick(View view) {
        Intent intent = new Intent(this.f3619a, BrowserActivity.class);
        intent.putExtra("com.tencent.assistant.BROWSER_URL", "http://3gimg.qq.com/xiaoxie/xiaoxie.html?mode=0");
        this.f3619a.startActivity(intent);
    }

    public STInfoV2 getStInfo() {
        if (!(this.f3619a instanceof AppDetailActivityV5)) {
            return null;
        }
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.b.getContext(), 200);
        buildSTInfo.slotId = a.a(this.b.f, this.b.h);
        return buildSTInfo;
    }
}
