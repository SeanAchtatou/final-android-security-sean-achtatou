package com.tencent.pangu.component.treasurebox;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.ImageView;
import com.tencent.android.qqdownloader.R;
import com.tencent.android.qqdownloader.b;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.utils.ah;

/* compiled from: ProGuard */
public class AppTreasureEntryBlinkEyesView extends ImageView {

    /* renamed from: a  reason: collision with root package name */
    private final int f3733a;
    private final int b;
    private final int c;
    private final long d;
    private final long e;
    private int f;
    private int g;
    private int h;
    private volatile long i;
    private volatile long j;
    private volatile boolean k;
    private final int l;
    private final int m;
    private int n;
    /* access modifiers changed from: private */
    public volatile boolean o;
    private Runnable p;
    private Runnable q;
    private boolean r;

    public AppTreasureEntryBlinkEyesView(Context context) {
        this(context, null);
    }

    public AppTreasureEntryBlinkEyesView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f3733a = R.drawable.xiaobao_bg;
        this.b = R.drawable.xiaobao_fg_openeye;
        this.c = R.drawable.xiaobao_fg_closeeye;
        this.d = 4000;
        this.e = 200;
        this.f = 17170445;
        this.g = 17170445;
        this.h = 17170445;
        this.k = false;
        this.l = 0;
        this.m = 1;
        this.o = false;
        this.p = new g(this);
        this.q = new h(this);
        this.r = true;
        a(context, attributeSet);
    }

    private void a(Context context, AttributeSet attributeSet) {
        this.n = 1;
        this.k = false;
        this.o = false;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, b.f377a);
        if (obtainStyledAttributes != null) {
            this.i = (long) obtainStyledAttributes.getInt(3, EventDispatcherEnum.CACHE_EVENT_END);
            this.j = (long) obtainStyledAttributes.getInt(4, 200);
            this.n = obtainStyledAttributes.getInt(5, 1);
            if (this.n == 0) {
                b();
            }
            obtainStyledAttributes.recycle();
        }
        setScaleType(ImageView.ScaleType.FIT_XY);
    }

    public boolean a() {
        return this.k;
    }

    public void a(boolean z) {
        this.k = z;
    }

    public void b() {
        a(true);
        if (!this.o) {
            d();
        }
    }

    public void c() {
        this.o = false;
        a(this.q);
        a(this.p);
    }

    /* access modifiers changed from: private */
    public void d() {
        if (!a()) {
            this.o = false;
            return;
        }
        a(this.q);
        a(this.p);
        this.o = true;
        a(this.p, this.i);
    }

    /* access modifiers changed from: private */
    public void e() {
        if (!a()) {
            this.o = false;
            return;
        }
        this.o = true;
        g();
        a(this.q, this.j);
    }

    /* access modifiers changed from: private */
    public void f() {
        if (!a()) {
            this.o = false;
            return;
        }
        this.o = true;
        h();
        a(new i(this), 0);
    }

    private void g() {
        this.r = false;
        if (this.h > 0) {
            setImageResource(this.h);
        }
        this.r = true;
    }

    private void h() {
        this.r = false;
        if (this.g > 0) {
            setImageResource(this.g);
        }
        this.r = true;
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        if (!z || !a()) {
            c();
        } else {
            b();
        }
    }

    private void a(Runnable runnable) {
        if (runnable != null) {
            ah.a().removeCallbacks(runnable);
        }
    }

    private void a(Runnable runnable, long j2) {
        if (runnable != null) {
            if (j2 <= 0) {
                j2 = 0;
            }
            ah.a().postDelayed(runnable, j2);
        }
    }

    public void requestLayout() {
        if (this.r) {
            super.requestLayout();
        }
    }

    public void setVisibility(int i2) {
        if (i2 == 0) {
            this.f = R.drawable.xiaobao_bg;
            this.g = R.drawable.xiaobao_fg_openeye;
            this.h = R.drawable.xiaobao_fg_closeeye;
            if (this.f > 0 && this.g > 0) {
                setImageResource(this.g);
                setBackgroundResource(this.f);
            }
        } else {
            this.f = 17170445;
            this.g = 17170445;
            this.h = 17170445;
            setImageResource(17170445);
            setBackgroundResource(17170445);
        }
        super.setVisibility(i2);
    }
}
