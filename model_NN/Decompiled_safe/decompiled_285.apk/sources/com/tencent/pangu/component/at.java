package com.tencent.pangu.component;

import android.view.View;
import android.view.animation.Animation;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
class at implements Animation.AnimationListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TextView f3629a;
    final /* synthetic */ View b;
    final /* synthetic */ RankSecondNavigationView c;

    at(RankSecondNavigationView rankSecondNavigationView, TextView textView, View view) {
        this.c = rankSecondNavigationView;
        this.f3629a = textView;
        this.b = view;
    }

    public void onAnimationStart(Animation animation) {
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationEnd(Animation animation) {
        this.f3629a.setTextColor(this.c.f3499a.getResources().getColor(R.color.navigation_title_selected));
        this.c.a(this.b, this.f3629a);
    }
}
