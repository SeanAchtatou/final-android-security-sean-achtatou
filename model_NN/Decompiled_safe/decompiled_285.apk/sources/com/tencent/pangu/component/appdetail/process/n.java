package com.tencent.pangu.component.appdetail.process;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.a.a;
import com.tencent.assistant.component.dialog.DialogUtils;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.manager.b;
import com.tencent.assistant.manager.u;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.l;
import com.tencent.assistant.module.k;
import com.tencent.assistant.net.c;
import com.tencent.assistant.protocol.jce.VerifyInfo;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.at;
import com.tencent.assistant.utils.e;
import com.tencent.assistant.utils.r;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import com.tencent.nucleus.socialcontact.comment.PopViewDialog;
import com.tencent.nucleus.socialcontact.login.j;
import com.tencent.nucleus.socialcontact.login.q;
import com.tencent.pangu.activity.AppDetailActivityV5;
import com.tencent.pangu.component.appdetail.process.AppdetailActionUIListener;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.download.SimpleDownloadInfo;
import com.tencent.pangu.manager.DownloadProxy;
import com.tencent.pangu.manager.ak;

/* compiled from: ProGuard */
public class n extends e implements UIEventListener, b {
    private int A = -1;
    private String B;
    private String C;
    private Bundle D;
    private boolean E = false;
    private boolean F = false;
    private boolean G = false;
    private u H = null;
    private AppConst.TwoBtnDialogInfo I = null;
    /* access modifiers changed from: private */
    public Context n;
    private SimpleAppModel o = null;
    private long p;
    private PopViewDialog q;
    private AstApp r;
    private int s = 1;
    private boolean t = false;
    private boolean u = false;
    private int v = 0;
    private int w = 0;
    private int x = 0;
    private int y = 0;
    private Dialog z;

    public n(a aVar) {
        super(aVar);
    }

    public void a(SimpleAppModel simpleAppModel, long j, boolean z2, PopViewDialog popViewDialog, StatInfo statInfo, Bundle bundle, boolean z3, Context context) {
        this.H = u.b();
        this.D = bundle;
        String string = bundle.getString(a.g);
        String string2 = bundle.getString(a.s);
        this.C = bundle.getString(a.y);
        this.B = bundle.getString(a.z);
        if (bundle.getString(a.D) != null) {
            this.F = true;
        }
        this.E = z3;
        this.n = context;
        this.r = AstApp.i();
        this.f3608a = z2;
        this.o = simpleAppModel;
        this.p = j;
        this.q = popViewDialog;
        if (!TextUtils.isEmpty(string)) {
            try {
                this.v = Integer.valueOf(string).intValue();
                this.x = this.v;
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        } else {
            this.v = simpleAppModel.P;
        }
        if (!TextUtils.isEmpty(string2)) {
            try {
                this.w = Integer.valueOf(string2).intValue();
                this.y = this.w;
            } catch (NumberFormatException e2) {
                e2.printStackTrace();
            }
            this.o.Q = (byte) this.w;
        } else {
            if (this.o.d()) {
                this.o.Q = 1;
            }
            if (this.o.e()) {
                this.o.Q = 2;
            }
        }
        if (AppConst.AppState.SDKUNSUPPORT == a(simpleAppModel)) {
            a(context.getString(R.string.unsupported));
            b(context.getResources().getColor(R.color.apk_size));
            return;
        }
        com.tencent.assistant.manager.a.a().a(this.o.q(), this);
        if (k() || l()) {
            this.r.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_WX_AUTH_SUCCESS, this);
            this.r.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_WX_AUTH_FAIL, this);
            this.r.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_WX_AUTH_CANCEL, this);
            this.r.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_WX_WRITE_TOKEN_SUCCESS, this);
            this.r.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_WX_WRITE_TOKEN_FAIL, this);
            if (!w()) {
                o();
            }
        }
        this.r.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_WX_UNINSTALLED, this);
        m();
    }

    private void m() {
        a(a(this.o));
        n();
    }

    private void a(AppConst.AppState appState) {
        if (k() || l()) {
            b(appState);
        } else {
            d(appState);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.component.appdetail.process.e.a(boolean, com.tencent.pangu.component.appdetail.process.AppdetailActionUIListener$AuthType):void
     arg types: [int, com.tencent.pangu.component.appdetail.process.AppdetailActionUIListener$AuthType]
     candidates:
      com.tencent.pangu.component.appdetail.process.n.a(int, com.tencent.assistant.model.l):void
      com.tencent.pangu.component.appdetail.process.n.a(int, java.lang.String):void
      com.tencent.pangu.component.appdetail.process.n.a(com.tencent.pangu.download.DownloadInfo, com.tencent.assistant.AppConst$AppState):void
      com.tencent.pangu.component.appdetail.process.n.a(boolean, int):void
      com.tencent.pangu.component.appdetail.process.e.a(int, int):void
      com.tencent.pangu.component.appdetail.process.e.a(android.content.Context, com.tencent.assistant.model.SimpleAppModel):void
      com.tencent.pangu.component.appdetail.process.e.a(java.lang.String, int):void
      com.tencent.pangu.component.appdetail.process.e.a(java.lang.String, java.lang.String):void
      com.tencent.pangu.component.appdetail.process.e.a(boolean, com.tencent.pangu.component.appdetail.process.AppdetailActionUIListener$AuthType):void */
    private void n() {
        if (k() || l()) {
            byte a2 = u.a(this.p);
            if (j()) {
                b(false);
                if (s.a(a2, (byte) 0)) {
                    a(false, AppdetailActionUIListener.AuthType.WX);
                }
            } else if ((this.v & 2) != 0) {
                b(true);
                if (s.a(a2, (byte) 0)) {
                    c(false);
                } else {
                    c(true);
                }
                if (s.a(a2, (byte) 1)) {
                    d(false);
                } else {
                    d(true);
                }
            } else {
                b(false);
                if (s.a(a2, (byte) 0)) {
                    a(false, AppdetailActionUIListener.AuthType.WX);
                }
            }
        } else {
            b(false);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.component.appdetail.process.n.a(boolean, int):void
     arg types: [int, ?]
     candidates:
      com.tencent.pangu.component.appdetail.process.n.a(int, com.tencent.assistant.model.l):void
      com.tencent.pangu.component.appdetail.process.n.a(int, java.lang.String):void
      com.tencent.pangu.component.appdetail.process.n.a(com.tencent.pangu.download.DownloadInfo, com.tencent.assistant.AppConst$AppState):void
      com.tencent.pangu.component.appdetail.process.e.a(int, int):void
      com.tencent.pangu.component.appdetail.process.e.a(android.content.Context, com.tencent.assistant.model.SimpleAppModel):void
      com.tencent.pangu.component.appdetail.process.e.a(java.lang.String, int):void
      com.tencent.pangu.component.appdetail.process.e.a(java.lang.String, java.lang.String):void
      com.tencent.pangu.component.appdetail.process.e.a(boolean, com.tencent.pangu.component.appdetail.process.AppdetailActionUIListener$AuthType):void
      com.tencent.pangu.component.appdetail.process.n.a(boolean, int):void */
    private void b(AppConst.AppState appState) {
        boolean z2;
        boolean z3;
        byte a2 = u.a(this.p);
        if ((this.v & 2) != 0) {
            if ((s.a(a2, (byte) 0) || s.a(a2, (byte) 2)) && (s.a(a2, (byte) 1) || s.a(a2, (byte) 3))) {
                z2 = false;
                z3 = true;
            }
            z2 = false;
            z3 = false;
        } else {
            if (s.a(a2, (byte) 0) || s.a(a2, (byte) 2)) {
                z2 = true;
                z3 = false;
            }
            z2 = false;
            z3 = false;
        }
        if (k()) {
            a(0, 100);
            a(true, (int) R.drawable.appdetail_bar_btn_downloaded_selector_v5);
            b(this.n.getResources().getColor(R.color.white));
            if (z2) {
                a(this.n.getString(R.string.auth_btn_no_number));
            } else {
                a(this.n.getString(R.string.jionbeta));
            }
            if (f()) {
                if (z3 || z2) {
                    Toast.makeText(this.n, (int) R.string.auth_fail_list_over, 0).show();
                } else {
                    b(this.n.getString(R.string.start_wx_auth));
                }
                a(false);
            }
        } else if (l()) {
            if (z2) {
                a(this.n.getString(R.string.vie_number_end), (int) R.drawable.icon_wx_disable);
            } else {
                a(this.n.getString(R.string.wx_vie_number), (int) R.drawable.icon_wx);
            }
            if (f()) {
                if (z3 || z2) {
                    Toast.makeText(this.n, (int) R.string.vie_number_fail_no_number, 0).show();
                } else {
                    b(this.o.d + this.n.getString(R.string.start_vie_number));
                }
                a(false);
            }
        } else {
            if (this.s != 2 || ApkResourceManager.getInstance().getLocalApkInfo(this.o.c) == null) {
            }
            if (3 == this.s) {
                a(this.n.getResources().getString(R.string.appdetail_appbar_send_topic));
                return;
            }
            if (appState == null || appState == AppConst.AppState.ILLEGAL) {
                appState = a(this.o);
            }
            switch (r.f3617a[appState.ordinal()]) {
                case 1:
                    a(this.n.getResources().getString(R.string.install));
                    return;
                case 2:
                    a(this.n, this.o);
                    return;
                case 3:
                    if (ak.a().c(this.o.c)) {
                        ak.a();
                        if (!TextUtils.isEmpty(ak.h())) {
                            ak.a();
                            if (ak.h().equals(this.o.c)) {
                                a(this.n.getResources().getString(R.string.qube_apk_using));
                                return;
                            }
                        }
                        a(this.n.getResources().getString(R.string.qube_apk_use));
                        return;
                    }
                    a(this.n.getResources().getString(R.string.open));
                    return;
                case 4:
                case 5:
                    a(this.n.getResources().getString(R.string.continuing));
                    return;
                case 6:
                    r();
                    return;
                case 7:
                    a(this.n.getResources().getString(R.string.queuing));
                    return;
                case 8:
                    if (this.o.c()) {
                        a(this.n.getResources().getString(R.string.jionfirstrelease) + " " + at.a(this.o.k));
                        return;
                    } else if (this.o.h()) {
                        a(this.n.getResources().getString(R.string.jionbeta) + " " + at.a(this.o.k));
                        return;
                    } else {
                        a(this.n, this.o);
                        return;
                    }
                case 9:
                    if (this.o.a() && !this.o.h() && !this.o.i()) {
                        a(this.n.getResources().getString(R.string.slim_update), at.a(this.o.k), at.a(this.o.v));
                        return;
                    } else if (!this.o.a() || (!this.o.h() && !this.o.i())) {
                        a(this.n.getResources().getString(R.string.update) + " " + at.a(this.o.k));
                        return;
                    } else {
                        a(this.n.getResources().getString(R.string.jionbeta));
                        return;
                    }
                case 10:
                    a(this.n.getResources().getString(R.string.installing));
                    return;
                case 11:
                    a(this.n.getResources().getString(R.string.uninstalling));
                    return;
                default:
                    a(this.n, this.o);
                    return;
            }
        }
    }

    private void c(AppConst.AppState appState) {
        DownloadInfo a2 = DownloadProxy.a().a(this.o);
        int i = 0;
        if (a2 != null) {
            if (b(a2, appState)) {
                i = a2.response.f;
            } else {
                i = SimpleDownloadInfo.getPercent(a2);
            }
        }
        if (appState == AppConst.AppState.INSTALLED) {
            b(this.n.getResources().getColor(R.color.white));
        } else if (appState == AppConst.AppState.DOWNLOADING || appState == AppConst.AppState.PAUSED) {
            b(this.n.getResources().getColor(R.color.appdetail_tag_text_color_blue_n));
        } else if (appState == AppConst.AppState.DOWNLOADED) {
            b(this.n.getResources().getColor(R.color.white));
        } else if (appState == AppConst.AppState.FAIL) {
        } else {
            if ((appState == AppConst.AppState.DOWNLOAD || appState == AppConst.AppState.UPDATE) && k()) {
                b(this.n.getResources().getColor(R.color.white));
            } else if (appState == AppConst.AppState.QUEUING && i <= 0) {
                b(this.n.getResources().getColor(R.color.appdetail_tag_text_color_blue_n));
            } else if (appState == AppConst.AppState.QUEUING && i > 0) {
            } else {
                if (appState == AppConst.AppState.INSTALLING) {
                    b(this.n.getResources().getColor(R.color.state_disable));
                } else {
                    b(this.n.getResources().getColor(17170443));
                }
            }
        }
    }

    private void o() {
        if (p() && !s.a(u.a(this.p), (byte) 0)) {
            String string = this.D.getString(a.A);
            if (!TextUtils.isEmpty(string)) {
                a(false);
                j.a().b(new q(string, true));
                a(Constants.STR_EMPTY, (byte) 1, (byte) 1, false);
                u();
                return;
            }
            String string2 = this.D.getString(a.B);
            if (!TextUtils.isEmpty(string2)) {
                a(false);
                a(string2, (byte) 1, (byte) 2, false);
                u();
                return;
            }
            String string3 = this.D.getString(a.y);
            if (!TextUtils.isEmpty(string3) && !TextUtils.isEmpty(this.B)) {
                if ("game_openId".equals(this.B)) {
                    a(false);
                    a(string3, (byte) 1, (byte) 5, false);
                    u();
                } else if ("code".equals(this.B)) {
                    a(false);
                    a(string3, (byte) 1, (byte) 1, false);
                    u();
                }
            }
        }
    }

    public boolean j() {
        if (!p()) {
            return false;
        }
        if (!TextUtils.isEmpty(this.C) && !TextUtils.isEmpty(this.B) && ("game_openId".equals(this.B) || "code".equals(this.B))) {
            return true;
        }
        if (!TextUtils.isEmpty(this.D.getString(a.A))) {
            return true;
        }
        if (!TextUtils.isEmpty(this.D.getString(a.B))) {
            return true;
        }
        return false;
    }

    public boolean k() {
        if (this.o == null || this.G) {
            return false;
        }
        if (!(this.o.d() && (this.o.P & 1) > 0)) {
            return false;
        }
        if (this.E && (this.x & 1) != 0 && (this.y & 1) != 0) {
            return true;
        }
        AppConst.AppState d = k.d(this.o);
        if (d != AppConst.AppState.INSTALLED && d != AppConst.AppState.DOWNLOAD && d != AppConst.AppState.UPDATE) {
            this.G = true;
            return false;
        } else if (!e.a(this.o.c, this.o.g, this.o.ad)) {
            return true;
        } else {
            return false;
        }
    }

    public boolean l() {
        if (this.o == null || this.G) {
            return false;
        }
        if (!(this.o.e() && (this.o.P & 1) > 0)) {
            return false;
        }
        if (this.E && (this.x & 1) != 0 && (this.y & 2) != 0) {
            return true;
        }
        AppConst.AppState d = k.d(this.o);
        if (d != AppConst.AppState.INSTALLED && d != AppConst.AppState.DOWNLOAD && d != AppConst.AppState.UPDATE) {
            this.G = true;
            return false;
        } else if (!e.a(this.o.c, this.o.g, this.o.ad)) {
            return true;
        } else {
            return false;
        }
    }

    private boolean p() {
        return this.E && (this.x & 1) > 0 && (this.y & 3) > 0;
    }

    public void onAppStateChange(String str, AppConst.AppState appState) {
        if (!k() && !l()) {
            a(DownloadProxy.a().d(str), appState);
        }
    }

    private void d(AppConst.AppState appState) {
        String str = null;
        if (this.o != null) {
            str = this.o.q();
        }
        if (!TextUtils.isEmpty(str)) {
            if (appState == null || appState == AppConst.AppState.ILLEGAL) {
                appState = a(this.o);
            }
            a(DownloadProxy.a().a(this.o), appState);
            return;
        }
        a(this.n.getResources().getString(R.string.illegal_data));
    }

    public void handleUIEvent(Message message) {
        int e;
        switch (message.what) {
            case 1016:
                if (this.o != null) {
                    d(a(this.o));
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_WX_AUTH_SUCCESS:
                if (this.A == message.arg2) {
                    this.A = -1;
                    a(Constants.STR_EMPTY, (byte) 1, (byte) 1, true);
                    u();
                    STInfoV2 i = i();
                    a(4, this.o.f938a + "|" + (i != null ? i.sourceScene : 2000) + "|" + this.v + "|" + 4);
                    this.H.a((Message) null);
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_WX_AUTH_FAIL:
                if (this.A == message.arg2) {
                    this.A = -1;
                    a(Constants.STR_EMPTY, (byte) 0, (byte) 1, true);
                    Toast.makeText(this.n, this.n.getResources().getString(R.string.wx_auth_fail), 0).show();
                    this.H.a((Message) null);
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_WX_AUTH_CANCEL:
                if (this.A == message.arg2) {
                    this.A = -1;
                    Toast.makeText(this.n, this.n.getResources().getString(R.string.wx_auth_fail_cancel), 0).show();
                    this.H.a((Message) null);
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_WX_WRITE_TOKEN_SUCCESS:
                if (this.A == message.arg2) {
                    this.A = -1;
                    int i2 = message.arg1;
                    v();
                    a(i2, (l) message.obj);
                    this.t = false;
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_WX_WRITE_TOKEN_FAIL:
                if (this.A == message.arg2) {
                    this.A = -1;
                    v();
                    if (((l) message.obj).f948a.i == 1) {
                        if (l()) {
                            Toast.makeText(this.n, (int) R.string.vie_number_fail, 0).show();
                        } else if (k()) {
                            Toast.makeText(this.n, this.n.getResources().getString(R.string.wx_auth_fail), 0).show();
                        }
                    }
                    this.t = false;
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_WX_UNINSTALLED:
                s();
                this.t = false;
                return;
            case EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS:
                XLog.i("xjp", "[AppdetailWXProcess] : 登录成功");
                Bundle bundle = (Bundle) message.obj;
                if (bundle == null || !bundle.containsKey(AppConst.KEY_FROM_TYPE)) {
                    e = com.tencent.nucleus.socialcontact.login.l.e();
                    com.tencent.nucleus.socialcontact.login.l.a(0);
                } else {
                    e = bundle.getInt(AppConst.KEY_FROM_TYPE);
                    com.tencent.nucleus.socialcontact.login.l.a(0);
                }
                if (11 == e || 12 == e) {
                    com.tencent.pangu.link.b.a(this.n, Uri.parse("tmast://webview?url=http://mq.wsq.qq.com/direct?route=sId/t/new&pkgName=" + this.o.c));
                    return;
                } else if (19 == e && this.q != null) {
                    this.q.e();
                    return;
                } else {
                    return;
                }
            default:
                return;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.component.appdetail.process.e.a(boolean, com.tencent.pangu.component.appdetail.process.AppdetailActionUIListener$AuthType):void
     arg types: [int, com.tencent.pangu.component.appdetail.process.AppdetailActionUIListener$AuthType]
     candidates:
      com.tencent.pangu.component.appdetail.process.n.a(int, com.tencent.assistant.model.l):void
      com.tencent.pangu.component.appdetail.process.n.a(int, java.lang.String):void
      com.tencent.pangu.component.appdetail.process.n.a(com.tencent.pangu.download.DownloadInfo, com.tencent.assistant.AppConst$AppState):void
      com.tencent.pangu.component.appdetail.process.n.a(boolean, int):void
      com.tencent.pangu.component.appdetail.process.e.a(int, int):void
      com.tencent.pangu.component.appdetail.process.e.a(android.content.Context, com.tencent.assistant.model.SimpleAppModel):void
      com.tencent.pangu.component.appdetail.process.e.a(java.lang.String, int):void
      com.tencent.pangu.component.appdetail.process.e.a(java.lang.String, java.lang.String):void
      com.tencent.pangu.component.appdetail.process.e.a(boolean, com.tencent.pangu.component.appdetail.process.AppdetailActionUIListener$AuthType):void */
    private void a(int i, l lVar) {
        if (lVar != null && lVar.f948a != null && lVar.f948a.i == 1) {
            switch (i) {
                case 0:
                    v();
                    e(lVar.d);
                    this.G = true;
                    this.u = true;
                    a(a(this.o));
                    STInfoV2 i2 = i();
                    a(4, this.o.f938a + "|" + (i2 != null ? i2.sourceScene : 2000) + "|" + this.v + "|" + 3);
                    return;
                case 1:
                default:
                    if (k()) {
                        Toast.makeText(this.n, (int) R.string.wx_auth_server_fail, 0).show();
                        return;
                    } else if (l()) {
                        Toast.makeText(this.n, (int) R.string.vie_number_fail_s, 0).show();
                        return;
                    } else {
                        return;
                    }
                case 2:
                    Toast.makeText(this.n, (int) R.string.wx_qq_auth_fail_code_invalid, 0).show();
                    return;
                case 3:
                    if (l()) {
                        Toast.makeText(this.n, (int) R.string.vie_number_fail_no_number, 0).show();
                    } else if (k()) {
                        Toast.makeText(this.n, (int) R.string.auth_fail_list_over, 0).show();
                    }
                    u.b(this.p, (byte) 1);
                    if ((this.v & 2) != 0) {
                        c(false);
                        return;
                    }
                    if (l()) {
                        a(e((int) R.string.vie_number_end), (int) R.drawable.icon_wx_disable);
                    } else if (k()) {
                        a(e((int) R.string.auth_btn_no_number));
                    }
                    a(false, AppdetailActionUIListener.AuthType.WX);
                    return;
                case 4:
                    if (l()) {
                        Toast.makeText(this.n, (int) R.string.vie_number_fail_no_need, 0).show();
                    } else {
                        Toast.makeText(this.n, (int) R.string.auth_fail_no_need, 0).show();
                    }
                    this.v = 0;
                    this.w = 0;
                    this.G = true;
                    n();
                    a(a(this.o));
                    return;
                case 5:
                    if (l()) {
                        Toast.makeText(this.n, (int) R.string.vie_number_fail_today_no_number, 0).show();
                    } else if (k()) {
                        Toast.makeText(this.n, (int) R.string.auth_fail_today_list_over, 0).show();
                    }
                    u.b(this.p, (byte) 4);
                    if ((this.v & 2) != 0) {
                        c(false);
                        return;
                    }
                    if (l()) {
                        a(e((int) R.string.vie_number_end), (int) R.drawable.icon_wx_disable);
                    } else if (k()) {
                        a(e((int) R.string.auth_btn_no_number));
                    }
                    a(false, AppdetailActionUIListener.AuthType.WX);
                    return;
            }
        }
    }

    private void a(int i, String str) {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.n, i);
        if (buildSTInfo != null) {
            buildSTInfo.extraData = str;
        }
        com.tencent.assistantv2.st.l.a(buildSTInfo);
    }

    private void e(boolean z2) {
        b(false);
        boolean l = l();
        if (l) {
            Message obtainMessage = this.r.j().obtainMessage();
            obtainMessage.what = EventDispatcherEnum.UI_EVENT_VIEBUMBER_SUCC;
            obtainMessage.obj = Long.valueOf(this.o.f938a);
            this.r.j().sendMessage(obtainMessage);
        }
        boolean k = k();
        AppConst.AppState a2 = a(this.o);
        if (a2 == AppConst.AppState.DOWNLOADED) {
            if (l) {
                b(this.n.getString(R.string.vie_number_suc_install));
            } else if (k) {
                b(this.n.getString(R.string.auth_suc_install));
            }
        } else if (a2 == AppConst.AppState.INSTALLED) {
            if (l) {
                b(this.n.getString(R.string.vie_number_suc_open));
            } else if (k) {
                b(this.n.getString(R.string.auth_suc_open));
            }
        } else if (c.e()) {
            if (AppConst.AppState.SDKUNSUPPORT != a2) {
                if (this.i != null) {
                    if (l) {
                        this.i.b();
                    } else if (k) {
                        this.i.a();
                    }
                }
                h();
                DownloadInfo a3 = DownloadProxy.a().a(this.o);
                if (a3 != null && a3.needReCreateInfo(this.o)) {
                    DownloadProxy.a().b(a3.downloadTicket);
                    a3 = null;
                }
                StatInfo a4 = com.tencent.assistantv2.st.page.a.a(i());
                a4.d = g();
                if (a3 == null) {
                    a3 = DownloadInfo.createDownloadInfo(this.o, a4);
                    a2 = a(this.o);
                } else {
                    a3.updateDownloadInfoStatInfo(a4);
                }
                if (this.D != null) {
                    a3.initCallParamsFromActionBundle(this.D);
                }
                if (!TextUtils.isEmpty(this.o.q())) {
                    switch (r.f3617a[a2.ordinal()]) {
                        case 2:
                        case 5:
                            com.tencent.pangu.download.a.a().a(a3);
                            c(a3, a2);
                            return;
                        case 3:
                        case 6:
                        case 7:
                        default:
                            return;
                        case 4:
                            com.tencent.pangu.download.a.a().b(a3);
                            c(a3, a2);
                            return;
                        case 8:
                        case 9:
                            com.tencent.pangu.download.a.a().a(a3);
                            c(a3, a2);
                            return;
                    }
                }
            }
        } else if (l) {
            b(this.n.getString(R.string.vie_number_suc_click_download));
        } else if (k) {
            b(this.n.getString(R.string.auth_suc_click_download));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.component.appdetail.process.n.a(boolean, int):void
     arg types: [int, ?]
     candidates:
      com.tencent.pangu.component.appdetail.process.n.a(int, com.tencent.assistant.model.l):void
      com.tencent.pangu.component.appdetail.process.n.a(int, java.lang.String):void
      com.tencent.pangu.component.appdetail.process.n.a(com.tencent.pangu.download.DownloadInfo, com.tencent.assistant.AppConst$AppState):void
      com.tencent.pangu.component.appdetail.process.e.a(int, int):void
      com.tencent.pangu.component.appdetail.process.e.a(android.content.Context, com.tencent.assistant.model.SimpleAppModel):void
      com.tencent.pangu.component.appdetail.process.e.a(java.lang.String, int):void
      com.tencent.pangu.component.appdetail.process.e.a(java.lang.String, java.lang.String):void
      com.tencent.pangu.component.appdetail.process.e.a(boolean, com.tencent.pangu.component.appdetail.process.AppdetailActionUIListener$AuthType):void
      com.tencent.pangu.component.appdetail.process.n.a(boolean, int):void */
    private void a(DownloadInfo downloadInfo, AppConst.AppState appState) {
        if (!(this.s == 2 && ApkResourceManager.getInstance().getLocalApkInfo(this.o.c) != null && appState == AppConst.AppState.INSTALLED)) {
        }
        if (appState == AppConst.AppState.DOWNLOADING || appState == AppConst.AppState.FAIL || appState == AppConst.AppState.PAUSED || appState == AppConst.AppState.QUEUING) {
            if (!(downloadInfo == null || downloadInfo.response == null)) {
                d(0);
                if (3 == this.s) {
                    a(true, (int) R.drawable.btn_green_selector);
                } else {
                    a(false, (int) R.drawable.btn_green_selector);
                }
                if (b(downloadInfo, appState)) {
                    a(downloadInfo.response.f, 0);
                } else {
                    a(SimpleDownloadInfo.getPercent(downloadInfo), 0);
                }
            }
        } else if (appState == AppConst.AppState.INSTALLED) {
            a(0, 100);
            d(8);
            if (this.s == 1) {
                a(true, (int) R.drawable.appdetail_bar_btn_installed_selector_v5_shixin);
            } else {
                a(true, (int) R.drawable.btn_green_selector);
            }
        } else if (appState == AppConst.AppState.DOWNLOADED) {
            d(8);
            a(0, 0);
            a(true, (int) R.drawable.appdetail_bar_btn_downloaded_selector_v5);
        } else if (appState == AppConst.AppState.INSTALLING) {
            a(0, 100);
            a(true, (int) R.drawable.common_btn_big_disabled);
        } else if ((appState == AppConst.AppState.DOWNLOAD || appState == AppConst.AppState.UPDATE) && k()) {
            a(0, 100);
            a(true, (int) R.drawable.appdetail_bar_btn_downloaded_selector_v5);
        } else {
            a(0, 100);
            a(true, (int) R.drawable.btn_green_selector);
        }
        b(appState);
        if ((appState == AppConst.AppState.DOWNLOADING || appState == AppConst.AppState.PAUSED) && SimpleDownloadInfo.getPercent(downloadInfo) >= 68) {
            b(this.n.getResources().getColor(17170443));
        } else {
            c(appState);
        }
    }

    private boolean b(DownloadInfo downloadInfo, AppConst.AppState appState) {
        return (appState == AppConst.AppState.DOWNLOADING || (downloadInfo.response.b > 0 && (appState == AppConst.AppState.PAUSED || appState == AppConst.AppState.FAIL))) && downloadInfo.response.f > SimpleDownloadInfo.getPercent(downloadInfo);
    }

    private void a(boolean z2, int i) {
        if (z2) {
            c(0);
            a(this.n.getResources().getDrawable(i));
            return;
        }
        c(8);
    }

    public void a() {
        if (!this.t) {
            if (k() || l()) {
                this.A = this.H.a(this.n);
                if (this.A > 0) {
                    this.t = true;
                } else {
                    Toast.makeText(this.n, this.n.getResources().getString(R.string.wx_auth_fail_call), 0).show();
                }
            } else {
                AppConst.AppState a2 = a(this.o);
                if (AppConst.AppState.SDKUNSUPPORT == a2) {
                    Toast.makeText(this.n, this.n.getResources().getString(R.string.canot_support_sofrware), 0).show();
                    return;
                }
                if (!(this.s == 2 && ApkResourceManager.getInstance().getLocalApkInfo(this.o.c) == null)) {
                }
                DownloadInfo a3 = DownloadProxy.a().a(this.o);
                if (a3 != null && a3.needReCreateInfo(this.o)) {
                    DownloadProxy.a().b(a3.downloadTicket);
                    a3 = null;
                }
                StatInfo a4 = com.tencent.assistantv2.st.page.a.a(i());
                a4.d = g();
                if (a3 == null) {
                    a3 = DownloadInfo.createDownloadInfo(this.o, a4);
                } else {
                    a3.updateDownloadInfoStatInfo(a4);
                }
                if (this.D != null) {
                    a3.initCallParamsFromActionBundle(this.D);
                }
                if (TextUtils.isEmpty(this.o.q())) {
                    return;
                }
                if (3 != this.s) {
                    switch (r.f3617a[a2.ordinal()]) {
                        case 1:
                            com.tencent.pangu.download.a.a().d(a3);
                            a(this.n.getResources().getString(R.string.install));
                            return;
                        case 2:
                        case 5:
                            com.tencent.pangu.download.a.a().a(a3);
                            AstApp.i().j().sendMessage(AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_APP_DETAIL_DOWNLOAD_CLICK));
                            c(a3, a2);
                            return;
                        case 3:
                            com.tencent.pangu.download.a.a().c(a3);
                            if (this.n != null) {
                                if (!ak.a().c(this.o.c)) {
                                    a(this.n.getResources().getString(R.string.open));
                                }
                                if (!this.F && this.E) {
                                    ((Activity) this.n).finish();
                                    return;
                                }
                                return;
                            }
                            return;
                        case 4:
                            com.tencent.pangu.download.a.a().b(a3);
                            AstApp.i().j().sendMessage(AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_APP_DETAIL_DOWNLOAD_CLICK));
                            c(a3, a2);
                            return;
                        case 6:
                            com.tencent.pangu.download.a.a().b(a3.downloadTicket);
                            a(this.n.getResources().getString(R.string.continuing));
                            return;
                        case 7:
                            com.tencent.pangu.download.a.a().b(a3.downloadTicket);
                            a(this.n.getResources().getString(R.string.pause));
                            return;
                        case 8:
                        case 9:
                            com.tencent.pangu.download.a.a().a(a3);
                            AstApp.i().j().sendMessage(AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_APP_DETAIL_DOWNLOAD_CLICK));
                            c(a3, a2);
                            if (this.i != null && this.u) {
                                if (this.o.e()) {
                                    this.i.a(2);
                                    return;
                                } else if (this.o.d()) {
                                    this.i.a(1);
                                    return;
                                } else {
                                    return;
                                }
                            } else {
                                return;
                            }
                        case 10:
                            Toast.makeText(this.n, (int) R.string.tips_slicent_install, 0).show();
                            return;
                        case 11:
                            Toast.makeText(this.n, (int) R.string.tips_slicent_uninstall, 0).show();
                            return;
                        default:
                            return;
                    }
                } else if (j.a().l()) {
                    q();
                } else if (j.a().k()) {
                    com.tencent.pangu.link.b.a(this.n, Uri.parse("tmast://webview?url=http://mq.wsq.qq.com/direct?route=sId/t/new&pkgName=" + this.o.c));
                } else {
                    Bundle bundle = new Bundle();
                    bundle.putInt(AppConst.KEY_LOGIN_TYPE, 5);
                    bundle.putInt(AppConst.KEY_FROM_TYPE, 11);
                    com.tencent.nucleus.socialcontact.login.l.a(11);
                    j.a().a(AppConst.IdentityType.MOBILEQ, bundle);
                }
            }
        }
    }

    private void q() {
        o oVar = new o(this);
        Resources resources = this.n.getResources();
        oVar.titleRes = resources.getString(R.string.login_prompt);
        oVar.contentRes = resources.getString(R.string.login_prompt_content);
        oVar.lBtnTxtRes = resources.getString(R.string.cancel);
        oVar.rBtnTxtRes = resources.getString(R.string.login_prompt_switch_account);
        DialogUtils.show2BtnDialog(oVar);
    }

    private void r() {
        c(DownloadProxy.a().a(this.o), a(this.o));
    }

    private void c(DownloadInfo downloadInfo, AppConst.AppState appState) {
        double d = 0.0d;
        if (downloadInfo != null) {
            if (b(downloadInfo, appState)) {
                d = (double) downloadInfo.response.f;
            } else {
                d = SimpleDownloadInfo.getPercentFloat(downloadInfo);
            }
        }
        a(String.format(this.n.getResources().getString(R.string.downloading_percent), String.format("%.1f", Double.valueOf(d))));
    }

    public void a(boolean z2, AppConst.AppState appState, long j, String str, int i, int i2, boolean z3) {
        this.f3608a = z2;
        if (i > 0) {
            this.b = str;
            this.c = i;
            this.e = i2;
            this.d = j;
            this.f = z3;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.component.appdetail.process.n.a(boolean, int):void
     arg types: [int, ?]
     candidates:
      com.tencent.pangu.component.appdetail.process.n.a(int, com.tencent.assistant.model.l):void
      com.tencent.pangu.component.appdetail.process.n.a(int, java.lang.String):void
      com.tencent.pangu.component.appdetail.process.n.a(com.tencent.pangu.download.DownloadInfo, com.tencent.assistant.AppConst$AppState):void
      com.tencent.pangu.component.appdetail.process.e.a(int, int):void
      com.tencent.pangu.component.appdetail.process.e.a(android.content.Context, com.tencent.assistant.model.SimpleAppModel):void
      com.tencent.pangu.component.appdetail.process.e.a(java.lang.String, int):void
      com.tencent.pangu.component.appdetail.process.e.a(java.lang.String, java.lang.String):void
      com.tencent.pangu.component.appdetail.process.e.a(boolean, com.tencent.pangu.component.appdetail.process.AppdetailActionUIListener$AuthType):void
      com.tencent.pangu.component.appdetail.process.n.a(boolean, int):void */
    public void a(int i) {
        AppConst.AppState a2 = a(this.o);
        if ((a2 == null || AppConst.AppState.SDKUNSUPPORT != a2) && i <= 3 && i >= 1) {
            this.s = i;
            if (this.o != null && !k() && !l()) {
                if (i == 2) {
                    if (ApkResourceManager.getInstance().getLocalApkInfo(this.o.c) != null) {
                    }
                } else if (1 == i) {
                    if (a2 == AppConst.AppState.INSTALLED) {
                        a(0, 100);
                        a(true, (int) R.drawable.appdetail_bar_btn_installed_selector_v5_shixin);
                    } else {
                        a(DownloadProxy.a().a(this.o), a2);
                    }
                } else if (3 == i) {
                    a(true, (int) R.drawable.btn_green_selector);
                }
                b(a2);
                c(a2);
            }
        }
    }

    private AppConst.AppState a(SimpleAppModel simpleAppModel) {
        AppConst.AppState b = b(simpleAppModel);
        return b != AppConst.AppState.ILLEGAL ? b : k.d(simpleAppModel);
    }

    private AppConst.AppState b(SimpleAppModel simpleAppModel) {
        if (simpleAppModel == null) {
            return AppConst.AppState.ILLEGAL;
        }
        if (simpleAppModel.h > r.d()) {
            return AppConst.AppState.SDKUNSUPPORT;
        }
        AppConst.AppState appState = AppConst.AppState.ILLEGAL;
        DownloadInfo a2 = DownloadProxy.a().a(simpleAppModel.c, simpleAppModel.g, simpleAppModel.ad);
        if (a2 != null) {
            appState = k.b(a2);
        }
        if (appState != AppConst.AppState.ILLEGAL) {
            return appState;
        }
        if (!simpleAppModel.ab) {
            return AppConst.AppState.ILLEGAL;
        }
        if (e.a(simpleAppModel.c, simpleAppModel.g, simpleAppModel.ad)) {
            return AppConst.AppState.INSTALLED;
        }
        return AppConst.AppState.UPDATE;
    }

    private void a(String str, byte b, byte b2, boolean z2) {
        if (str == null) {
            str = Constants.STR_EMPTY;
        }
        VerifyInfo verifyInfo = new VerifyInfo();
        verifyInfo.f1607a = this.o.f938a;
        verifyInfo.b = this.o.b;
        verifyInfo.c = this.o.c;
        verifyInfo.g = str;
        verifyInfo.f = Constants.STR_EMPTY;
        verifyInfo.j = "wx3909f6add1206543";
        verifyInfo.i = b;
        verifyInfo.h = b2;
        verifyInfo.e = t();
        verifyInfo.d = this.o.g;
        verifyInfo.l = x();
        verifyInfo.m = 1;
        l lVar = new l();
        lVar.f948a = verifyInfo;
        lVar.d = z2;
        this.A = this.H.a(lVar);
    }

    private void s() {
        if (!((Activity) this.n).isFinishing()) {
            if (this.I == null) {
                this.I = new p(this);
                this.I.hasTitle = false;
                if (k()) {
                    this.I.contentRes = this.n.getString(R.string.auth_fail_download_old);
                } else {
                    this.I.contentRes = this.n.getString(R.string.auth_fail_download_old_2);
                }
            }
            if (this.I != null) {
                DialogUtils.show2BtnDialog(this.I);
            }
        }
    }

    private int t() {
        PackageInfo d;
        if ((this.v & 1) != 1 || (d = e.d("com.tencent.mm", 0)) == null) {
            return 0;
        }
        return d.versionCode;
    }

    public void b() {
        this.r.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS, this);
    }

    public void c() {
        this.t = false;
        if (this.o != null) {
            AppConst.AppState a2 = a(this.o);
            com.tencent.assistant.manager.a.a().a(this.o.q(), this);
            a(a2);
        }
        this.r.k().addUIEventListener(1016, this);
    }

    public void d() {
        this.r.k().removeUIEventListener(1016, this);
    }

    public void e() {
        this.r.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_WX_AUTH_SUCCESS, this);
        this.r.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_WX_AUTH_FAIL, this);
        this.r.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_WX_AUTH_CANCEL, this);
        this.r.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_WX_WRITE_TOKEN_SUCCESS, this);
        this.r.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_WX_WRITE_TOKEN_FAIL, this);
        this.r.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_WX_UNINSTALLED, this);
        this.r.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS, this);
        this.I = null;
    }

    private void u() {
        if (this.z == null) {
            q qVar = new q(this);
            if (l()) {
                qVar.loadingText = this.n.getResources().getString(R.string.vie_number_doing);
            } else if (k()) {
                qVar.loadingText = this.n.getResources().getString(R.string.wx_auth_doing);
            } else {
                return;
            }
            this.z = DialogUtils.showLoadingDialog(qVar);
        } else if (this.z.getOwnerActivity() != null && !this.z.getOwnerActivity().isFinishing()) {
            this.z.show();
        }
    }

    private void v() {
        if (this.z != null) {
            this.z.dismiss();
        }
    }

    private boolean w() {
        boolean z2 = false;
        Message c = this.H.c();
        if (c != null) {
            this.A = c.arg2;
            AstApp.i().j().sendMessageDelayed(c, 1000);
            z2 = true;
        }
        this.H.a((Message) null);
        return z2;
    }

    private String e(int i) {
        if (this.n == null) {
            return Constants.STR_EMPTY;
        }
        return this.n.getString(i);
    }

    private byte x() {
        if (this.o == null) {
            return 0;
        }
        return this.o.Q;
    }

    public void a(View view) {
        int id = view.getId();
        if (id == R.id.btn_pause_download) {
            a();
        } else if (id == R.id.btn_delete_download) {
            DownloadInfo a2 = DownloadProxy.a().a(this.o);
            if (a2 != null) {
                DownloadProxy.a().b(a2.downloadTicket);
            }
            a((DownloadInfo) null, a(this.o));
        } else if (id == R.id.appdetail_progress_btn_for_cmd) {
            if (ApkResourceManager.getInstance().getLocalApkInfo(this.o.c) != null || (ak.a().c(this.o.c) && ak.a().d(this.o.c))) {
                this.q.a(this.f3608a, this.o.f938a, this.p, this.d, this.b, this.c, this.e, (AppDetailActivityV5) this.n, this.o.d, this.f);
                try {
                    this.q.d();
                } catch (Throwable th) {
                }
            } else {
                Toast.makeText(this.n, (int) R.string.comment_txt_tips_need_install, 1).show();
                ((View) view.getParent()).setVisibility(8);
            }
        } else if (id != R.id.appdetail_progress_btn_for_appbar) {
        } else {
            if (j.a().l()) {
                q();
            } else if (j.a().k()) {
                com.tencent.pangu.link.b.a(this.n, Uri.parse("tmast://webview?url=http://mq.wsq.qq.com/direct?route=sId/t/new&pkgName=" + this.o.c));
            } else {
                Bundle bundle = new Bundle();
                bundle.putInt(AppConst.KEY_LOGIN_TYPE, 5);
                bundle.putInt(AppConst.KEY_FROM_TYPE, 11);
                com.tencent.nucleus.socialcontact.login.l.a(11);
                j.a().a(AppConst.IdentityType.MOBILEQ, bundle);
            }
        }
    }

    public int g() {
        byte x2 = x();
        if (x2 == 2) {
            return 22;
        }
        return x2;
    }
}
