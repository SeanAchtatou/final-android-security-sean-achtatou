package com.tencent.pangu.component.appdetail;

import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.LinearLayout;
import android.widget.ListView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.TouchAnalizer;

/* compiled from: ProGuard */
public class ad implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    public boolean f3563a = false;
    long b = 0;
    int c = 0;
    int d;
    int e = 50;
    int f = TouchAnalizer.CLICK_AREA;
    LinearLayout.LayoutParams g;
    View h;
    public ListView i;
    private long j = -1;
    private Interpolator k = new DecelerateInterpolator();

    public ad(View view) {
        this.h = view;
        this.c = view.getResources().getDimensionPixelSize(R.dimen.hide_installed_apps_area_height);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    public void run() {
        if (this.f3563a) {
            if (this.j == -1) {
                this.j = System.currentTimeMillis();
            } else {
                this.b = ((System.currentTimeMillis() - this.j) * 1000) / ((long) this.f);
                this.b = Math.max(Math.min(this.b, 1000L), 0L);
                this.d = Math.round(((float) this.c) - (((float) this.c) * this.k.getInterpolation(((float) this.b) / 1000.0f)));
                this.g = (LinearLayout.LayoutParams) this.h.getLayoutParams();
                this.g.height = this.d;
                this.h.setLayoutParams(this.g);
            }
            if (this.b >= 1000) {
                this.f3563a = false;
                if (this.i == null) {
                    this.h.setVisibility(8);
                } else {
                    if (this.i.getFirstVisiblePosition() == 0) {
                        this.i.setSelection(1);
                    }
                    this.g.height = this.i.getResources().getDimensionPixelSize(R.dimen.hide_installed_apps_area_height);
                    this.h.setLayoutParams(this.g);
                    this.h.setClickable(true);
                }
                this.j = -1;
                this.b = 0;
                this.h.findViewById(R.id.tips).setVisibility(8);
            } else if (this.f3563a) {
                this.h.postDelayed(this, (long) this.e);
            }
        }
    }
}
