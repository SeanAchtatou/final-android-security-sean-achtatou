package com.tencent.pangu.component.appdetail;

import android.text.TextUtils;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.pangu.activity.AppDetailActivityV5;
import com.tencent.pangu.link.b;

/* compiled from: ProGuard */
class al extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ao f3570a;
    final /* synthetic */ DetailGameNewsView b;

    al(DetailGameNewsView detailGameNewsView, ao aoVar) {
        this.b = detailGameNewsView;
        this.f3570a = aoVar;
    }

    public void onTMAClick(View view) {
        if (!TextUtils.isEmpty(this.f3570a.b)) {
            b.a(this.b.b, this.f3570a.b);
        }
    }

    public STInfoV2 getStInfo() {
        if (!(this.b.b instanceof AppDetailActivityV5)) {
            return null;
        }
        STInfoV2 t = ((AppDetailActivityV5) this.b.b).t();
        t.slotId = a.a(this.f3570a.e == 1 ? "17" : "09", 0);
        t.actionId = 200;
        return t;
    }
}
