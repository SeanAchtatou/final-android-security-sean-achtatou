package com.tencent.pangu.component;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class LineBreakTextView extends TextView {

    /* renamed from: a  reason: collision with root package name */
    private ArrayList<String> f3494a = new ArrayList<>();
    private Paint b = getPaint();
    private int c = -1;

    public LineBreakTextView(Context context) {
        super(context);
    }

    public LineBreakTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public LineBreakTextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        canvas.save();
        int lineHeight = getLineHeight();
        this.b.setColor(getCurrentTextColor());
        canvas.translate((float) getPaddingLeft(), (float) (getPaddingTop() + ((int) (((((float) lineHeight) - this.b.descent()) - this.b.ascent()) / 2.0f))));
        int i = 0;
        for (int i2 = 0; i2 < this.f3494a.size(); i2++) {
            canvas.drawText(this.f3494a.get(i2), 0.0f, (float) i, this.b);
            i += lineHeight;
        }
        canvas.restore();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int lineHeight;
        String substring;
        super.onMeasure(i, i2);
        if (getText() == null || getText().length() == 0 || getVisibility() == 8) {
            setMeasuredDimension(0, 0);
            return;
        }
        String obj = getText().toString();
        int size = View.MeasureSpec.getSize(i);
        int size2 = View.MeasureSpec.getSize(i2) + getPaddingTop() + getPaddingBottom();
        int paddingLeft = (size - getPaddingLeft()) - getPaddingRight();
        this.f3494a.clear();
        if (this.b.measureText(obj) <= ((float) paddingLeft)) {
            this.f3494a.add(obj);
            lineHeight = size2 + getLineHeight();
        } else if (this.c > 0) {
            int i3 = 0;
            int i4 = 0;
            lineHeight = size2;
            int i5 = 0;
            while (i4 < this.c) {
                obj = obj.substring(i3);
                if (i4 != this.c - 1) {
                    i3 = this.b.breakText(obj, true, (float) paddingLeft, null);
                    substring = obj.substring(0, i3);
                } else if (this.b.measureText(obj) > ((float) paddingLeft)) {
                    i3 = this.b.breakText(obj, true, ((float) paddingLeft) - this.b.measureText("..."), null);
                    substring = obj.substring(0, i3) + "...";
                } else {
                    i3 = i5;
                    substring = obj;
                }
                this.f3494a.add(substring);
                lineHeight += getLineHeight();
                i4++;
                i5 = i3;
            }
        } else {
            int i6 = size2;
            String str = obj;
            int i7 = 0;
            int i8 = 0;
            while (true) {
                str = str.substring(i7);
                if (this.b.measureText(str) <= ((float) paddingLeft)) {
                    break;
                }
                i7 = this.b.breakText(str, true, (float) paddingLeft, null);
                this.f3494a.add(str.substring(0, i7));
                i6 += getLineHeight();
                i8++;
            }
            this.f3494a.add(str);
            lineHeight = i6 + getLineHeight();
        }
        setMeasuredDimension(size, lineHeight);
    }

    public void setMaxLines(int i) {
        this.c = i;
        requestLayout();
    }
}
