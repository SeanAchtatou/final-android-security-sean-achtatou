package com.tencent.pangu.component;

import android.content.Context;
import android.text.InputFilter;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.utils.by;

/* compiled from: ProGuard */
public class ShareAppContentView extends LinearLayout implements bn {

    /* renamed from: a  reason: collision with root package name */
    private TextView f3504a;
    private ImageView b;
    private TextView c = null;
    private int d = -1;
    private LinearLayout e = null;
    private ShareAppBar f = null;

    public ShareAppContentView(Context context) {
        super(context);
        a(context);
    }

    public ShareAppContentView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.tencent.pangu.component.ShareAppContentView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void a(Context context) {
        setOrientation(1);
        LayoutInflater.from(context).inflate((int) R.layout.dialog_share_yyb_once, (ViewGroup) this, true);
        this.f3504a = (TextView) findViewById(R.id.share_content);
        this.b = (ImageView) findViewById(R.id.yyb_mascot);
        this.c = (TextView) findViewById(R.id.tv_share_app_bar_txt);
        this.c.setVisibility(8);
        this.e = (LinearLayout) findViewById(R.id.share_app_bottom_bar_layout);
        this.f = (ShareAppBar) findViewById(R.id.layout_share);
        if (this.f != null) {
            this.f.a(this);
        }
    }

    public void a(CharSequence charSequence) {
        if (this.d != -1) {
            this.f3504a.setFilters(new InputFilter[]{new InputFilter.LengthFilter(this.d)});
            ((LinearLayout.LayoutParams) this.f3504a.getLayoutParams()).rightMargin = by.a(getContext(), 15.0f);
            requestLayout();
        }
        this.f3504a.setText(charSequence);
    }

    public void a(int i) {
        this.b.setImageResource(i);
    }

    public void b(int i) {
        this.d = i;
    }

    public void a() {
        if (this.e != null) {
            this.e.setVisibility(8);
        }
    }
}
