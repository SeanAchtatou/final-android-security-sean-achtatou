package com.tencent.pangu.component.search;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.LoadingView;
import com.tencent.assistant.component.NormalErrorRecommendPage;
import com.tencent.assistant.component.txscrollview.ITXRefreshListViewListener;
import com.tencent.assistant.component.txscrollview.TXGetMoreListView;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.b;
import com.tencent.assistant.net.c;
import com.tencent.assistant.protocol.jce.ColorCardItem;
import com.tencent.assistant.utils.by;
import com.tencent.assistantv2.adapter.smartlist.SmartListAdapter;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.adapter.SearchPageListAdapter;
import com.tencent.pangu.component.banner.f;
import com.tencent.pangu.component.search.ISearchResultPage;
import com.tencent.pangu.module.AppSearchResultEngine;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class NativeSearchResultPage extends ISearchResultPage implements ITXRefreshListViewListener {
    protected final String h;
    protected View.OnClickListener i;
    private int j;
    private SmartListAdapter k;
    private TXGetMoreListView l;
    private LoadingView m;
    private NormalErrorRecommendPage n;
    private View o;
    private TextView p;
    /* access modifiers changed from: private */
    public boolean q;
    /* access modifiers changed from: private */
    public AppSearchResultEngine r;
    private b s;

    public NativeSearchResultPage(Context context) {
        this(context, null);
    }

    public NativeSearchResultPage(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public NativeSearchResultPage(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.h = "NativeSearchResultPage";
        this.j = 0;
        this.k = null;
        this.l = null;
        this.q = false;
        this.s = new b(this, null);
        this.i = new a(this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.adapter.smartlist.SmartListAdapter.a(int, long):void
     arg types: [int, int]
     candidates:
      com.tencent.assistantv2.adapter.smartlist.SmartListAdapter.a(int, int):void
      com.tencent.assistantv2.adapter.smartlist.SmartListAdapter.a(com.tencent.assistant.smartcard.d.n, long):boolean
      com.tencent.assistantv2.adapter.smartlist.SmartListAdapter.a(int, long):void */
    /* access modifiers changed from: protected */
    public void a(Context context) {
        try {
            this.g = true;
            a((int) R.layout.search_result_native_page);
            this.m = (LoadingView) findViewById(R.id.loading_view);
            this.m.setVisibility(0);
            this.n = (NormalErrorRecommendPage) findViewById(R.id.error_page);
            this.n.setButtonClickListener(this.i);
            this.l = (TXGetMoreListView) findViewById(R.id.result_list);
            this.l.setVisibility(8);
            this.l.setRefreshListViewListener(this);
            this.l.setDivider(null);
            this.l.setSelector(new ColorDrawable(0));
            this.l.setCacheColorHint(17170445);
            this.k = new SearchPageListAdapter(getContext(), this.l, l());
            this.k.a(c(), -100L);
            this.l.setDivider(null);
            this.l.setSelector(new ColorDrawable(0));
            this.l.addClickLoadMore();
            this.l.setAdapter(this.k);
            this.o = LayoutInflater.from(getContext()).inflate((int) R.layout.search_result_none_head, (ViewGroup) null);
        } catch (Throwable th) {
            th.printStackTrace();
            this.g = false;
        }
    }

    public b l() {
        if (this.r == null) {
            this.r = new AppSearchResultEngine();
            this.r.register(this.s);
        }
        return this.r.b();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.component.search.NativeSearchResultPage.a(int, boolean, boolean):void
     arg types: [int, int, int]
     candidates:
      com.tencent.pangu.component.search.NativeSearchResultPage.a(com.tencent.assistant.component.txscrollview.TXGetMoreListView, java.lang.String, int):void
      com.tencent.pangu.component.search.NativeSearchResultPage.a(java.lang.String, int, com.tencent.assistant.model.SimpleAppModel):void
      com.tencent.pangu.component.search.ISearchResultPage.a(java.lang.String, int, com.tencent.assistant.model.SimpleAppModel):void
      com.tencent.pangu.component.search.NativeSearchResultPage.a(int, boolean, boolean):void */
    public void a(String str, int i2, SimpleAppModel simpleAppModel) {
        a();
        if (!c.a()) {
            a(-800, true, false);
        } else if (this.r != null) {
            n();
            d();
            this.f = 0;
            this.e = simpleAppModel;
            this.f3690a = str;
            this.r.a(b());
            this.j = this.r.a(str, i2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.component.search.NativeSearchResultPage.a(int, boolean, boolean):void
     arg types: [int, int, int]
     candidates:
      com.tencent.pangu.component.search.NativeSearchResultPage.a(com.tencent.assistant.component.txscrollview.TXGetMoreListView, java.lang.String, int):void
      com.tencent.pangu.component.search.NativeSearchResultPage.a(java.lang.String, int, com.tencent.assistant.model.SimpleAppModel):void
      com.tencent.pangu.component.search.ISearchResultPage.a(java.lang.String, int, com.tencent.assistant.model.SimpleAppModel):void
      com.tencent.pangu.component.search.NativeSearchResultPage.a(int, boolean, boolean):void */
    public void a(String str, int i2) {
        a();
        if (!c.a()) {
            a(-800, true, false);
        } else if (!c(str)) {
            a(this.f, this.f3690a);
        } else if (this.r != null) {
            n();
            d();
            this.f = 0;
            this.f3690a = str;
            this.r.a(b());
            this.j = this.r.a(str, i2);
        }
    }

    public void d() {
        super.d();
    }

    public void e() {
        super.e();
        if (this.r != null) {
            this.r.c();
        }
    }

    public void a(String str) {
        if (this.r != null) {
            this.r.a(str);
        }
    }

    public void b(String str) {
        if (this.r != null) {
            this.r.f3890a = str;
        }
    }

    private boolean c(String str) {
        if (!TextUtils.isEmpty(str) && str.equals(this.f3690a) && this.q) {
            return false;
        }
        if (m() == null || m().getCount() <= 0 || (!TextUtils.isEmpty(str) && !str.equals(this.f3690a))) {
            return true;
        }
        return false;
    }

    private void a(TXGetMoreListView tXGetMoreListView, String str, int i2) {
        if (tXGetMoreListView != null) {
            if (this.o != null) {
                tXGetMoreListView.removeHeaderView(this.o);
            }
            if (this.p != null) {
                tXGetMoreListView.removeHeaderView(this.p);
            }
            if (i2 <= 0) {
                tXGetMoreListView.addHeaderView(this.o);
            } else if (!TextUtils.isEmpty(str)) {
                this.p = new TextView(getContext());
                this.p.setTextColor(getResources().getColor(R.color.search_result_head_txt));
                this.p.setTextSize(2, 13.0f);
                this.p.setPadding(by.a(getContext(), 8.0f), by.a(getContext(), 10.0f), 0, 0);
                this.p.setText(str);
                this.p.setBackgroundColor(0);
                tXGetMoreListView.addHeaderView(this.p);
            } else {
                this.p = new TextView(getContext());
                this.p.setHeight(by.a(getContext(), 3.0f));
                this.p.setText(Constants.STR_EMPTY);
                this.p.setBackgroundColor(0);
                tXGetMoreListView.addHeaderView(this.p);
            }
            if (m() != null && (m() instanceof BaseAdapter)) {
                ((BaseAdapter) m()).notifyDataSetChanged();
            }
        }
    }

    public void a(long j2, String str) {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(getContext(), 100);
        buildSTInfo.searchId = j2;
        buildSTInfo.extraData = str;
        l.a(buildSTInfo);
    }

    public void onTXRefreshListViewRefresh(TXScrollViewBase.ScrollState scrollState) {
        if (TXScrollViewBase.ScrollState.ScrollState_FromEnd == scrollState && this.r != null) {
            this.r.a();
        }
    }

    public ListAdapter m() {
        return this.k;
    }

    public ISearchResultPage.PageType h() {
        return ISearchResultPage.PageType.NATIVE;
    }

    /* access modifiers changed from: private */
    public void n() {
        if (this.g) {
            this.m.setVisibility(0);
            this.l.setVisibility(8);
            this.n.setVisibility(8);
            this.l.reset();
            this.l.setAdapter(null);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.adapter.smartlist.SmartListAdapter.a(int, long):void
     arg types: [int, int]
     candidates:
      com.tencent.assistantv2.adapter.smartlist.SmartListAdapter.a(int, int):void
      com.tencent.assistantv2.adapter.smartlist.SmartListAdapter.a(com.tencent.assistant.smartcard.d.n, long):boolean
      com.tencent.assistantv2.adapter.smartlist.SmartListAdapter.a(int, long):void */
    /* access modifiers changed from: private */
    public void a(boolean z, int i2, ArrayList<String> arrayList, boolean z2, List<com.tencent.pangu.model.b> list, long j2, String str, int i3) {
        if (this.g) {
            if (this.k != null) {
                p();
                this.k.a(c(), -100L);
                this.k.a(l());
                if (list != null) {
                    this.k.a(f());
                    this.k.a(z, list, (List<f>) null, (List<ColorCardItem>) null);
                    this.k.a(this.e);
                }
            }
            if (z) {
                g();
                this.l.setAdapter(this.k);
                a(this.l, str, i3);
            }
            this.l.onRefreshComplete(z2, true);
        }
    }

    /* access modifiers changed from: private */
    public void a(int i2, boolean z, boolean z2) {
        if (this.g) {
            g();
            this.m.setVisibility(8);
            if (z) {
                this.l.setVisibility(8);
                this.n.setVisibility(0);
                if (-800 == i2) {
                    this.n.setErrorType(30);
                } else {
                    this.n.setErrorType(20);
                }
            }
            this.l.onRefreshComplete(z2, z);
        }
    }

    /* access modifiers changed from: private */
    public void o() {
        if (this.g) {
            this.m.setVisibility(8);
            this.l.setVisibility(8);
            this.n.setVisibility(0);
            this.n.setErrorType(NormalErrorRecommendPage.ERROR_TYPE_SEARCH_RESULT_EMPTY);
        }
    }

    public void d(int i2) {
        switch (i2) {
            case 0:
                if (this.l != null) {
                    this.l.setVisibility(8);
                    return;
                }
                return;
            default:
                d();
                if (this.l != null) {
                    this.l.setVisibility(0);
                    return;
                }
                return;
        }
    }

    private void p() {
        if (this.g) {
            this.m.setVisibility(8);
            this.l.setVisibility(0);
            this.n.setVisibility(8);
        }
    }

    public void i() {
        if (this.r != null) {
            this.r.register(this.s);
        }
        if (this.k != null) {
            this.k.i();
            this.k.notifyDataSetChanged();
        }
    }

    public void j() {
        if (this.k != null) {
            this.k.h();
        }
    }

    public void k() {
        if (this.r != null) {
            this.r.unregister(this.s);
        }
        if (this.k != null) {
            this.k.j();
        }
    }
}
