package com.tencent.pangu.component.appdetail;

import android.os.Handler;
import com.tencent.assistant.model.c;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.activity.AppDetailActivityV5;
import com.tencent.pangu.module.a.a;

/* compiled from: ProGuard */
class ah implements a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CustomRelateAppViewV5 f3567a;

    ah(CustomRelateAppViewV5 customRelateAppViewV5) {
        this.f3567a = customRelateAppViewV5;
    }

    public void a(int i, int i2, c cVar, int i3) {
        if (i2 != 0 || cVar == null || !AppDetailActivityV5.a(cVar)) {
            for (int i4 = 0; i4 < this.f3567a.m.size(); i4++) {
                if (this.f3567a.k[i4].getTag() != null && this.f3567a.k[i4].getTag().toString().equals(Constants.STR_EMPTY + i)) {
                    this.f3567a.k[i4].c();
                }
            }
            return;
        }
        new Handler().post(new ai(this, i, cVar));
    }
}
