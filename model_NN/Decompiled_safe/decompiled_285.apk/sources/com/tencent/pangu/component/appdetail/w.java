package com.tencent.pangu.component.appdetail;

import android.content.Intent;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.activity.AppDetailActivityV5;
import com.tencent.pangu.activity.AuthorOtherAppsActivity;
import java.util.ArrayList;

/* compiled from: ProGuard */
class w extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f3623a;
    final /* synthetic */ ArrayList b;
    final /* synthetic */ byte[] c;
    final /* synthetic */ boolean d;
    final /* synthetic */ AppdetailRelatedViewV5 e;

    w(AppdetailRelatedViewV5 appdetailRelatedViewV5, String str, ArrayList arrayList, byte[] bArr, boolean z) {
        this.e = appdetailRelatedViewV5;
        this.f3623a = str;
        this.b = arrayList;
        this.c = bArr;
        this.d = z;
    }

    public void onTMAClick(View view) {
        Intent intent = new Intent(this.e.f3534a, AuthorOtherAppsActivity.class);
        intent.putExtra("author_name", this.e.p);
        intent.putExtra("pkgname", this.f3623a);
        intent.putExtra("first_page_data", this.b);
        intent.putExtra("page_context", this.c);
        intent.putExtra("has_next", this.d);
        this.e.f3534a.startActivity(intent);
    }

    public STInfoV2 getStInfo() {
        if (!(this.e.f3534a instanceof AppDetailActivityV5)) {
            return null;
        }
        STInfoV2 t = ((AppDetailActivityV5) this.e.f3534a).t();
        t.slotId = this.e.a();
        t.actionId = 200;
        return t;
    }
}
