package com.tencent.pangu.component;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.link.b;

/* compiled from: ProGuard */
class t extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f3715a;
    final /* synthetic */ int b;
    final /* synthetic */ HotwordsCardView c;

    t(HotwordsCardView hotwordsCardView, String str, int i) {
        this.c = hotwordsCardView;
        this.f3715a = str;
        this.b = i;
    }

    public void onTMAClick(View view) {
        b.a(this.c.getContext(), this.f3715a);
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.c.getContext(), 200);
        if (this.b == 1) {
            buildSTInfo.slotId = a.a("04", 0);
            buildSTInfo.status = Constants.VIA_ACT_TYPE_NINETEEN;
            return buildSTInfo;
        } else if (this.b != 2) {
            return null;
        } else {
            buildSTInfo.scene = this.c.g;
            buildSTInfo.slotId = a.a("03", 0);
            buildSTInfo.status = "02";
            return buildSTInfo;
        }
    }
}
