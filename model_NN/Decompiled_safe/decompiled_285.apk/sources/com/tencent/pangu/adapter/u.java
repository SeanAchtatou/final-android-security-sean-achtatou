package com.tencent.pangu.adapter;

import android.widget.TextView;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.utils.ah;
import com.tencent.pangu.model.d;

/* compiled from: ProGuard */
class u implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ d f3462a;
    final /* synthetic */ TXImageView b;
    final /* synthetic */ TextView c;
    final /* synthetic */ DownloadInfoMultiAdapter d;

    u(DownloadInfoMultiAdapter downloadInfoMultiAdapter, d dVar, TXImageView tXImageView, TextView textView) {
        this.d = downloadInfoMultiAdapter;
        this.f3462a = dVar;
        this.b = tXImageView;
        this.c = textView;
    }

    public void run() {
        ah.a().post(new v(this, this.d.v.getMetaInfo(this.f3462a.r)));
    }
}
