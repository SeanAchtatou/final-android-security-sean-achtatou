package com.tencent.c.a;

import com.qq.taf.jce.JceOutputStream;
import com.qq.taf.jce.JceStruct;
import com.tencent.d.a.e;

/* compiled from: ProGuard */
public class a {
    public static byte[] a(JceStruct jceStruct) {
        if (jceStruct == null) {
            return null;
        }
        JceOutputStream jceOutputStream = new JceOutputStream();
        jceOutputStream.setServerEncoding("utf-8");
        jceStruct.writeTo(jceOutputStream);
        return jceOutputStream.toByteArray();
    }

    public static byte[] a(byte[] bArr, byte[] bArr2) {
        return new e().b(bArr, bArr2);
    }

    public static byte[] b(byte[] bArr, byte[] bArr2) {
        return new e().a(bArr, bArr2);
    }
}
