package com.tencent.nucleus.manager.appbackup;

import android.app.Dialog;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.protocol.jce.BackupDevice;
import com.tencent.assistant.utils.bo;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class BackupDeviceAdapter extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    private Context f2798a;
    private ArrayList<BackupDevice> b;
    /* access modifiers changed from: private */
    public int c = -1;
    /* access modifiers changed from: private */
    public m d;
    private Dialog e;

    public BackupDeviceAdapter(Context context) {
        this.f2798a = context;
    }

    public int getCount() {
        if (this.b == null) {
            return 0;
        }
        return this.b.size();
    }

    /* renamed from: a */
    public BackupDevice getItem(int i) {
        if (this.b == null || this.b.size() == 0) {
            return null;
        }
        return this.b.get(i);
    }

    public long getItemId(int i) {
        return 0;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        n nVar;
        if (view == null || view.getTag() == null) {
            view = View.inflate(this.f2798a, R.layout.device_item, null);
            n nVar2 = new n(null);
            nVar2.f2811a = (TextView) view.findViewById(R.id.device_name);
            nVar2.b = (TextView) view.findViewById(R.id.backup_time);
            nVar2.c = (ProgressBar) view.findViewById(R.id.progress);
            nVar2.d = (ImageView) view.findViewById(R.id.last_line);
            view.setTag(nVar2);
            nVar = nVar2;
        } else {
            nVar = (n) view.getTag();
        }
        BackupDevice a2 = getItem(i);
        if (a2 != null) {
            String b2 = a2.b();
            if (TextUtils.isEmpty(b2)) {
                b2 = "未知设备";
            }
            nVar.f2811a.setText(b2);
            String c2 = bo.c(Long.valueOf(a2.c()));
            if (!TextUtils.isEmpty(c2)) {
                nVar.b.setText(c2);
            }
            ProgressBar progressBar = nVar.c;
            if (this.c == i) {
                progressBar.setVisibility(0);
            } else {
                progressBar.setVisibility(8);
            }
            nVar.d.setVisibility(0);
            view.setOnClickListener(new l(this, a2, progressBar, i));
        }
        return view;
    }

    public void a(ArrayList<BackupDevice> arrayList) {
        this.b = arrayList;
        this.c = -1;
    }

    public void a(Dialog dialog) {
        this.e = dialog;
    }

    public void a(m mVar) {
        this.d = mVar;
    }

    public void a() {
        this.c = -1;
        notifyDataSetChanged();
    }
}
