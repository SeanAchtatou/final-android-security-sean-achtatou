package com.tencent.nucleus.manager.about;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.module.BaseEngine;
import com.tencent.assistant.module.am;
import com.tencent.assistant.protocol.jce.GetFeedbackListRequest;
import com.tencent.assistant.protocol.jce.GetFeedbackListResponse;

/* compiled from: ProGuard */
public class n extends BaseEngine<l> implements am {
    private static int c = 5;
    private static n e = null;

    /* renamed from: a  reason: collision with root package name */
    private int f2750a = -1;
    private byte[] b = new byte[0];
    /* access modifiers changed from: private */
    public int d = 0;

    private n() {
    }

    public static synchronized n a() {
        n nVar;
        synchronized (n.class) {
            if (e == null) {
                e = new n();
            }
            nVar = e;
        }
        return nVar;
    }

    public int b() {
        return this.d;
    }

    public void c() {
        this.b = new byte[0];
        f();
    }

    public void d() {
        f();
    }

    public void e() {
        this.d = 0;
        notifyDataChangedInMainThread(new o(this));
    }

    public void a(String str) {
        int parseInt = Integer.parseInt(str);
        this.d = parseInt;
        notifyDataChangedInMainThread(new p(this, parseInt));
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        GetFeedbackListResponse getFeedbackListResponse = (GetFeedbackListResponse) jceStruct2;
        GetFeedbackListRequest getFeedbackListRequest = (GetFeedbackListRequest) jceStruct;
        this.b = getFeedbackListResponse.b;
        notifyDataChangedInMainThread(new q(this, getFeedbackListRequest.b == null || getFeedbackListRequest.b.length == 0, getFeedbackListResponse.d));
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        GetFeedbackListRequest getFeedbackListRequest = (GetFeedbackListRequest) jceStruct;
        notifyDataChanged(new r(this, i2, getFeedbackListRequest.b == null || getFeedbackListRequest.b.length == 0));
    }

    private void f() {
        if (this.f2750a > 0) {
            cancel(this.f2750a);
        }
        GetFeedbackListRequest getFeedbackListRequest = new GetFeedbackListRequest();
        getFeedbackListRequest.f1286a = c;
        getFeedbackListRequest.b = this.b;
        this.f2750a = send(getFeedbackListRequest);
    }
}
