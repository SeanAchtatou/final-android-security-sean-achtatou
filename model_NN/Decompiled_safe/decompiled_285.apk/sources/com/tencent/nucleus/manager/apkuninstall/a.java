package com.tencent.nucleus.manager.apkuninstall;

import android.content.Intent;
import android.view.View;
import com.tencent.assistant.activity.RootUtilInstallActivity;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class a extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PreInstallAppListView f2783a;

    a(PreInstallAppListView preInstallAppListView) {
        this.f2783a = preInstallAppListView;
    }

    public void onTMAClick(View view) {
        Intent intent = new Intent(this.f2783a.b, RootUtilInstallActivity.class);
        intent.putExtra("king_root_channelid", "000116083139343433333633");
        this.f2783a.b.startActivity(intent);
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f2783a.b, 200);
        buildSTInfo.scene = STConst.ST_PAGE_PRE_APP_UNINSTALL_NO_ROOT;
        buildSTInfo.slotId = "03_001";
        return buildSTInfo;
    }
}
