package com.tencent.nucleus.manager;

import com.tencent.assistant.utils.XLog;
import com.tencent.nucleus.manager.spaceclean.SpaceScanManager;

/* compiled from: ProGuard */
class b implements a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DockRubbishRelateService f2817a;

    b(DockRubbishRelateService dockRubbishRelateService) {
        this.f2817a = dockRubbishRelateService;
    }

    public void a() {
        XLog.d("DockRubbish", "DockRubbishRelateService >> 管家服务绑定成功，开始扫描...");
        long unused = this.f2817a.c = 0;
        long unused2 = this.f2817a.d = 0;
        long unused3 = this.f2817a.e = 0;
        long unused4 = this.f2817a.f = 0;
        long unused5 = this.f2817a.g = 0;
        this.f2817a.a();
        SpaceScanManager.a().a(this.f2817a.i);
    }

    public void b() {
        XLog.d("DockRubbish", "DockRubbishRelateService >> onBindTmsServiceFailed..");
        StringBuilder sb = new StringBuilder();
        sb.append("[status]\nresultcode=").append(-2);
        this.f2817a.a(sb.toString());
        this.f2817a.a(-2, false);
        this.f2817a.stopSelf();
    }

    public void c() {
        XLog.d("DockRubbish", "DockRubbishRelateService >> onTmsServiceDisconnected.");
        StringBuilder sb = new StringBuilder();
        sb.append("[status]\nresultcode=").append(-4);
        this.f2817a.a(sb.toString());
        this.f2817a.a(-4, false);
        this.f2817a.stopSelf();
    }
}
