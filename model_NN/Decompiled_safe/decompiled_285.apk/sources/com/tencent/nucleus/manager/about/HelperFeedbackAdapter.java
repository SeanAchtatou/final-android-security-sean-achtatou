package com.tencent.nucleus.manager.about;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.protocol.jce.Feedback;
import com.tencent.assistant.utils.bo;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class HelperFeedbackAdapter extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    private Context f2732a;
    private LayoutInflater b = null;
    private View.OnClickListener c = null;
    private ArrayList<Feedback> d;

    public HelperFeedbackAdapter() {
    }

    public HelperFeedbackAdapter(Context context, ArrayList<Feedback> arrayList) {
        this.f2732a = context;
        this.b = LayoutInflater.from(context);
        this.d = arrayList;
    }

    public void a(View.OnClickListener onClickListener) {
        this.c = onClickListener;
    }

    public void a(ArrayList<Feedback> arrayList) {
        if (arrayList != null) {
            if (this.d != null) {
                this.d.clear();
            } else {
                this.d = new ArrayList<>();
            }
            this.d.addAll(arrayList);
            notifyDataSetChanged();
        }
    }

    public void b(ArrayList<Feedback> arrayList) {
        if (this.d == null) {
            this.d = new ArrayList<>();
        }
        this.d.addAll(arrayList);
        notifyDataSetChanged();
    }

    public int getCount() {
        if (this.d != null) {
            return this.d.size();
        }
        return 0;
    }

    public Object getItem(int i) {
        if (this.d != null) {
            return this.d.get(i);
        }
        return null;
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        ac acVar;
        if (view == null) {
            view = this.b.inflate((int) R.layout.feedback_list_item, (ViewGroup) null);
            acVar = new ac();
            acVar.f2736a = (TextView) view.findViewById(R.id.feedback_time);
            acVar.b = (TextView) view.findViewById(R.id.feedback_answer_time);
            acVar.c = (TextView) view.findViewById(R.id.feedback_question_author);
            acVar.d = (TextView) view.findViewById(R.id.feedback_question);
            acVar.e = (TextView) view.findViewById(R.id.feedback_answer_author);
            acVar.f = (TextView) view.findViewById(R.id.feedback_answer);
            view.setTag(acVar);
        } else {
            acVar = (ac) view.getTag();
        }
        view.setOnClickListener(this.c);
        a(acVar, (Feedback) getItem(i));
        return view;
    }

    private void a(ac acVar, Feedback feedback) {
        if (acVar != null && feedback != null) {
            acVar.f2736a.setText(bo.c(Long.valueOf(feedback.d * 1000)));
            acVar.b.setText(bo.c(Long.valueOf(feedback.e * 1000)));
            acVar.c.setText(this.f2732a.getResources().getText(R.string.feedback_list_question_author));
            acVar.d.setText(feedback.b);
            acVar.e.setText(String.format(this.f2732a.getResources().getString(R.string.feedback_list_answer_author), feedback.g));
            acVar.f.setText(feedback.c);
        }
    }
}
