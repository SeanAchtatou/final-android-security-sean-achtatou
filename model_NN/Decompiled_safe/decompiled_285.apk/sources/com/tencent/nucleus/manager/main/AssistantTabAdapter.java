package com.tencent.nucleus.manager.main;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.ApkMgrActivity;
import com.tencent.assistant.activity.AppBackupActivity;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.activity.InstalledAppManagerActivity;
import com.tencent.assistant.activity.PhotoBackupNewActivity;
import com.tencent.assistant.activity.SpaceCleanActivity;
import com.tencent.assistant.activity.StartScanActivity;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.manager.i;
import com.tencent.assistant.plugin.GetPluginListCallback;
import com.tencent.assistant.plugin.GetPluginListEngine;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.plugin.PluginDownloadInfo;
import com.tencent.assistant.plugin.PluginInfo;
import com.tencent.assistant.plugin.PluginStartEntry;
import com.tencent.assistant.plugin.accelerate.PluginAccelerateBridgeActivity;
import com.tencent.assistant.plugin.activity.PluginDetailActivity;
import com.tencent.assistant.plugin.mgr.h;
import com.tencent.assistant.plugin.proxy.PluginProxyActivity;
import com.tencent.assistant.plugin.system.PluginBackToBaoReceiver;
import com.tencent.assistant.protocol.jce.ActionUrl;
import com.tencent.assistant.protocol.jce.GetManageInfoListResponse;
import com.tencent.assistant.protocol.jce.MAManageInfo;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ah;
import com.tencent.assistant.utils.bm;
import com.tencent.assistantv2.st.b.b;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.cloud.activity.UpdateListActivity;
import com.tencent.connect.common.Constants;
import com.tencent.connector.ConnectionActivity;
import com.tencent.nucleus.manager.about.HelperFAQActivity;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/* compiled from: ProGuard */
public class AssistantTabAdapter extends BaseAdapter implements UIEventListener, GetPluginListCallback, ai {
    private static final String i = AstApp.i().getPackageName();
    private static HashMap<String, Integer> l = new HashMap<>();

    /* renamed from: a  reason: collision with root package name */
    List<ae> f2927a = Collections.synchronizedList(new LinkedList());
    protected boolean b = false;
    protected boolean c = false;
    private String d = "AssistantTabAdapter";
    private Context e;
    private LayoutInflater f;
    private AstApp g = AstApp.i();
    private List<PluginStartEntry> h = new ArrayList();
    private Random j = new Random(System.currentTimeMillis());
    private b k = null;

    /* compiled from: ProGuard */
    public enum ItemPositionType {
        TOP,
        MID,
        BOTTOM,
        SINGLE
    }

    static {
        l.put("com.tencent.android.qqdownloader_com.tencent.nucleus.manager.apkuninstall.InstalledAppManagerActivity", Integer.valueOf((int) R.drawable.helper_cardicon_app));
        l.put("com.tencent.android.qqdownloader_com.tencent.nucleus.manager.apkMgr.ApkMgrActivity", Integer.valueOf((int) R.drawable.helper_cardicon_apkmgr));
        l.put("com.tencent.android.qqdownloader_com.tencent.nucleus.manager.spaceclean.SpaceCleanActivity", Integer.valueOf((int) R.drawable.helper_cardicon_qingli));
        l.put("com.assistant.accelerate_com.assistant.accelerate.MobileAccelerateActivity", Integer.valueOf((int) R.drawable.helper_cardicon_speed));
        l.put("com.tencent.android.qqdownloader_com.tencent.nucleus.manager.securescan.StartScanActivity", Integer.valueOf((int) R.drawable.helper_cardicon_safescan));
        l.put("com.tencent.android.qqdownloader_com.tencent.nucleus.manager.appbackup.AppBackupActivity", Integer.valueOf((int) R.drawable.helper_cardicon_backup));
        l.put("com.tencent.android.qqdownloader_com.tencent.assistant.activity.PhotoBackupNewActivity", Integer.valueOf((int) R.drawable.helper_cardicon_pic));
        l.put("com.tencent.android.qqdownloader_com.tencent.connector.ConnectionActivity", Integer.valueOf((int) R.drawable.helper_cardicon_pcline));
        l.put("com.tencent.mobileassistant_wifitransfer_com.tencent.assistant.activity.WifiTransferActivity", Integer.valueOf((int) R.drawable.helper_cardicon_tran));
        l.put("com.tencent.assistant.freewifi_com.tencent.assistant.freewifi.FreewifiActivity", Integer.valueOf((int) R.drawable.helper_cardicon_freewifi));
    }

    public AssistantTabAdapter(Context context) {
        this.e = context;
        this.f = LayoutInflater.from(context);
        d();
    }

    private void d() {
        this.f2927a.clear();
        a();
        GetManageInfoListResponse e2 = i.y().e();
        if (e2 == null || e2.b == null || e2.b.size() < 5) {
            ae aeVar = new ae(this);
            aeVar.h = "uninstall";
            aeVar.i = R.drawable.helper_cardicon_app;
            aeVar.j = R.string.soft_admin;
            this.f2927a.add(aeVar);
            ae aeVar2 = new ae(this);
            aeVar2.h = "apk_mgr";
            aeVar2.i = R.drawable.helper_cardicon_apkmgr;
            aeVar2.j = R.string.apkmgr_title;
            this.f2927a.add(aeVar2);
            ae aeVar3 = new ae(this);
            aeVar3.h = "rubbish";
            aeVar3.i = R.drawable.helper_cardicon_qingli;
            aeVar3.j = R.string.rubbish_clear;
            this.f2927a.add(aeVar3);
            ae aeVar4 = new ae(this);
            aeVar4.h = "applist_backup";
            aeVar4.i = R.drawable.helper_cardicon_backup;
            aeVar4.j = R.string.app_backup;
            this.f2927a.add(aeVar4);
            ae aeVar5 = new ae(this);
            aeVar5.h = "secure_scan";
            aeVar5.i = R.drawable.helper_cardicon_safescan;
            aeVar5.j = R.string.secure_scan;
            this.f2927a.add(aeVar5);
            ae aeVar6 = new ae(this);
            aeVar6.h = "connect_pc";
            aeVar6.i = R.drawable.helper_cardicon_pcline;
            aeVar6.j = R.string.cmn_to_pc;
            this.f2927a.add(aeVar6);
            ae aeVar7 = new ae(this);
            aeVar7.h = "photo_backup";
            aeVar7.i = R.drawable.helper_cardicon_pic;
            aeVar7.j = R.string.pic_bak;
            this.f2927a.add(aeVar7);
        } else {
            ArrayList<MAManageInfo> arrayList = e2.b;
            int i2 = 0;
            for (int i3 = 0; i3 < arrayList.size(); i3++) {
                MAManageInfo mAManageInfo = arrayList.get(i3);
                if (!(mAManageInfo == null || (mAManageInfo.c == 0 && GetPluginListEngine.getInstance().getPluginByPackageName(mAManageInfo.f1407a) == null))) {
                    ae aeVar8 = new ae(this);
                    aeVar8.b = mAManageInfo.c;
                    aeVar8.h = "item_entry_" + i2;
                    aeVar8.c = mAManageInfo.f1407a;
                    aeVar8.f2934a = mAManageInfo.b;
                    aeVar8.d = mAManageInfo.d;
                    aeVar8.e = mAManageInfo.e;
                    aeVar8.f = mAManageInfo.f;
                    aeVar8.g = mAManageInfo.g;
                    this.f2927a.add(i2, aeVar8);
                    i2++;
                }
            }
        }
        if (this.f2927a.size() % 3 != 0) {
            int size = 3 - (this.f2927a.size() % 3);
            for (int i4 = 0; i4 < size; i4++) {
                ae aeVar9 = new ae(this);
                aeVar9.h = "empty";
                aeVar9.i = -1;
                aeVar9.j = R.string.p_empty_str;
                this.f2927a.add(aeVar9);
            }
        }
    }

    public void a() {
        this.h.clear();
        List<PluginDownloadInfo> list = GetPluginListEngine.getInstance().getList();
        if (list != null && list.size() > 0) {
            for (PluginDownloadInfo next : list) {
                if (!TextUtils.isEmpty(next.startActivity)) {
                    if ("p.com.tencent.assistant.activity.PhotoBackupNewActivity".equals(next.startActivity) || PhotoBackupNewActivity.class.getName().equals(next.startActivity)) {
                        next.pluginPackageName = AstApp.i().getPackageName();
                        next.startActivity = PhotoBackupNewActivity.class.getName();
                    } else if ("p.com.tencent.connector.ConnectionActivity".equals(next.startActivity) || ConnectionActivity.class.getName().equals(next.startActivity)) {
                        next.pluginPackageName = AstApp.i().getPackageName();
                        next.startActivity = ConnectionActivity.class.getName();
                    }
                    this.h.add(new PluginStartEntry(next.pluginId, next.name, next.pluginPackageName, next.version, next.startActivity, next.iconUrl));
                }
            }
        }
    }

    public int getCount() {
        if (this.f2927a != null) {
            return this.f2927a.size();
        }
        return 0;
    }

    public Object getItem(int i2) {
        if (this.f2927a == null || this.f2927a.size() <= i2) {
            return null;
        }
        return this.f2927a.get(i2);
    }

    public long getItemId(int i2) {
        return (long) i2;
    }

    public View getView(int i2, View view, ViewGroup viewGroup) {
        af afVar;
        ae aeVar = (ae) getItem(i2);
        if (view == null || view.getTag() == null || !(view.getTag() instanceof af)) {
            af afVar2 = new af(this);
            view = this.f.inflate((int) R.layout.lite_assistant_tab_item_v5, (ViewGroup) null);
            afVar2.f2935a = view.findViewById(R.id.root);
            afVar2.b = (TXImageView) view.findViewById(R.id.group_icon);
            afVar2.c = (TextView) view.findViewById(R.id.group_title);
            afVar2.d = (ViewStub) view.findViewById(R.id.extralayoutStub);
            afVar2.e = (ImageView) view.findViewById(R.id.group_promot);
            afVar2.f = (TextView) view.findViewById(R.id.group_number);
            view.setTag(afVar2);
            afVar = afVar2;
        } else {
            afVar = (af) view.getTag();
        }
        a(afVar, aeVar, i2);
        b(i2);
        view.setTag(R.id.tma_st_slot_tag, a(i2));
        if (aeVar != null) {
            view.setTag(R.id.plugin_list_action_tag, aeVar.h);
        }
        return view;
    }

    private void a(af afVar, ae aeVar, int i2) {
        if (afVar != null && aeVar != null) {
            if ("empty".equals(aeVar.h)) {
                afVar.f2935a.setEnabled(false);
                afVar.f2935a.setFocusable(true);
            } else {
                afVar.f2935a.setEnabled(true);
                afVar.f2935a.setFocusable(false);
            }
            if (aeVar.i > 0) {
                afVar.b.setVisibility(0);
                afVar.b.setImageDrawableAndResetImageUrlString(this.e.getResources().getDrawable(aeVar.i));
                afVar.c.setText(this.e.getString(aeVar.j));
            } else if (aeVar.i == -1) {
                afVar.b.setVisibility(8);
                afVar.c.setText(this.e.getString(aeVar.j));
            } else {
                Integer num = l.get((aeVar.c != null ? aeVar.c : Constants.STR_EMPTY) + "_" + (aeVar.d != null ? aeVar.d : Constants.STR_EMPTY));
                afVar.b.updateImageView(aeVar.f, num != null ? num.intValue() : R.drawable.icon_default_plugin, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
                afVar.b.setVisibility(0);
                afVar.c.setText(aeVar.f2934a);
            }
            a(afVar, i2);
            if (a(aeVar)) {
                a(afVar, aeVar);
            } else if (afVar.e != null && afVar.e.getVisibility() != 8) {
                afVar.e.setVisibility(8);
            }
        }
    }

    private void a(af afVar, int i2) {
        if (i2 == 0) {
            afVar.f2935a.setBackgroundResource(R.drawable.card_upleft_selector);
        } else if (i2 == 1) {
            afVar.f2935a.setBackgroundResource(R.drawable.card_upmid_selector);
        } else if (i2 == 2) {
            afVar.f2935a.setBackgroundResource(R.drawable.card_upright_selector);
        } else if (i2 == getCount() - 1) {
            afVar.f2935a.setBackgroundResource(R.drawable.card_downright_selector);
        } else if (i2 == getCount() - 2) {
            afVar.f2935a.setBackgroundResource(R.drawable.card_downmid_selector);
        } else if (i2 == getCount() - 3) {
            afVar.f2935a.setBackgroundResource(R.drawable.card_downleft_selector);
        } else if (i2 % 3 == 0) {
            afVar.f2935a.setBackgroundResource(R.drawable.card_midleft_selector);
        } else if (i2 % 3 == 1) {
            afVar.f2935a.setBackgroundResource(R.drawable.card_midmid_selector);
        } else if (i2 % 3 == 2) {
            afVar.f2935a.setBackgroundResource(R.drawable.card_midright_selector);
        }
    }

    private boolean a(ae aeVar) {
        if (aeVar == null || aeVar.h == null) {
            return false;
        }
        if ("connect_pc".equals(aeVar.h)) {
            return true;
        }
        if (aeVar.b != 0 || !"p.com.tencent.android.qqdownloader".equals(aeVar.c) || !"p.com.tencent.connector.ConnectionActivity".equals(aeVar.d)) {
            return false;
        }
        return true;
    }

    private boolean a(String str) {
        ae aeVar;
        if (TextUtils.isEmpty(str) || !str.startsWith("item_entry_") || (aeVar = this.f2927a.get(bm.a(str.substring("item_entry_".length()), -1))) == null || !"com.assistant.accelerate".equals(aeVar.c) || !"com.assistant.accelerate.MobileAccelerateActivity".equals(aeVar.d)) {
            return false;
        }
        return true;
    }

    public void b() {
        this.g.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_GET_USERINFO_SUCCESS, this);
        this.g.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_GET_USERINFO_FAIL, this);
        this.g.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_CONNECTOR_STATE_CHANGE, this);
        this.g.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_EXTRACT_FINISH, this);
        this.g.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_SUCC, this);
        GetPluginListEngine.getInstance().unregister(this);
        ag.a().b(this);
    }

    public void c() {
        GetPluginListEngine.getInstance().register(this);
        ag.a().a(this);
        this.g.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_ADD, this);
        this.g.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE, this);
        this.g.k().addUIEventListener(1016, this);
        this.g.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_UPDATE_LIST_CHANGED, this);
        this.g.k().addUIEventListener(1019, this);
        this.g.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_UPDATE_IGNORE_LIST_DELETE, this);
        this.g.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_UPDATE_START, this);
        this.g.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_UPDATE_DELETE, this);
        this.g.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_CONNECTOR_STATE_CHANGE, this);
        this.g.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_EXTRACT_FINISH, this);
        this.g.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_SUCC, this);
        notifyDataSetChanged();
        Intent intent = new Intent();
        intent.setAction("com.tencent.android.qqdownloader.action.QUERY_CONNECT_STATE");
        AstApp.i().sendBroadcast(intent);
    }

    public void a(long j2) {
        ah.a().post(new ac(this));
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE:
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_ADD:
            case 1016:
            case EventDispatcherEnum.UI_EVENT_APP_UPDATE_LIST_CHANGED:
            case 1019:
            case EventDispatcherEnum.UI_EVENT_APP_UPDATE_IGNORE_LIST_DELETE:
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_UPDATE_START:
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_UPDATE_DELETE:
            case EventDispatcherEnum.UI_EVENT_GET_USERINFO_SUCCESS:
            case EventDispatcherEnum.UI_EVENT_GET_USERINFO_FAIL:
            case EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_SUCC:
            case EventDispatcherEnum.UI_EVENT_PLUGIN_CONNECTOR_STATE_CHANGE:
            case EventDispatcherEnum.UI_EVENT_PLUGIN_EXTRACT_FINISH:
                ah.a().post(new ad(this));
                return;
            case EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS:
                Bundle bundle = (Bundle) message.obj;
                if (bundle != null && bundle.containsKey("pageId") && bundle.containsKey("tag")) {
                    a(bundle.getInt("pageId"), bundle.getString("tag"));
                    return;
                }
                return;
            default:
                return;
        }
    }

    private void a(af afVar, ae aeVar) {
        if (afVar != null && aeVar != null) {
            switch (PluginBackToBaoReceiver.b()) {
                case 0:
                    afVar.c.setText((int) R.string.cmn_to_pc);
                    return;
                case 1:
                    if (TextUtils.isEmpty(PluginBackToBaoReceiver.a())) {
                        afVar.c.setText((int) R.string.cmn_to_pc);
                        return;
                    } else {
                        afVar.c.setText(this.e.getString(R.string.label_connection_established) + PluginBackToBaoReceiver.a());
                        return;
                    }
                case 2:
                case 3:
                    if (TextUtils.isEmpty(PluginBackToBaoReceiver.a())) {
                        afVar.c.setText((int) R.string.cmn_to_pc);
                        return;
                    } else {
                        afVar.c.setText(this.e.getString(R.string.label_connection_established) + PluginBackToBaoReceiver.a());
                        return;
                    }
                default:
                    return;
            }
        }
    }

    public void a(int i2, String str, View view) {
        if (a(str) && view.getTag() != null) {
            af afVar = (af) view.getTag();
            if (!(afVar.e == null || afVar.e.getVisibility() == 8)) {
                afVar.e.setVisibility(8);
            }
            PluginAccelerateBridgeActivity.a(this.e);
        }
        a(i2, str);
    }

    public void a(int i2, String str) {
        ae aeVar;
        int i3;
        if ("update".equals(str)) {
            Intent intent = new Intent(this.e, UpdateListActivity.class);
            intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, i2);
            this.e.startActivity(intent);
        } else if ("file_exchange".equals(str)) {
        } else {
            if ("uninstall".equals(str)) {
                Intent intent2 = new Intent(this.e, InstalledAppManagerActivity.class);
                intent2.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, i2);
                intent2.putExtra("activityTitleName", this.e.getResources().getString(R.string.soft_admin));
                this.e.startActivity(intent2);
            } else if ("help".equals(str)) {
                Intent intent3 = new Intent(this.e, HelperFAQActivity.class);
                intent3.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, i2);
                intent3.putExtra("com.tencent.assistant.BROWSER_URL", "http://maweb.3g.qq.com/help/help.html");
                this.e.startActivity(intent3);
            } else if ("rubbish".equals(str)) {
                Intent intent4 = new Intent(this.e, SpaceCleanActivity.class);
                intent4.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, i2);
                intent4.putExtra("dock_plugin", b("com.assistant.accelerate"));
                this.e.startActivity(intent4);
            } else if ("apk_mgr".equals(str)) {
                Intent intent5 = new Intent(this.e, ApkMgrActivity.class);
                intent5.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, i2);
                this.e.startActivity(intent5);
            } else if (str != null && str.startsWith("item_entry_")) {
                int a2 = bm.a(str.substring("item_entry_".length()), -1);
                if (a2 < 0 || a2 >= this.f2927a.size()) {
                    aeVar = null;
                } else {
                    aeVar = this.f2927a.get(a2);
                }
                if (aeVar == null) {
                    return;
                }
                if (aeVar.b == 0) {
                    PluginInfo a3 = com.tencent.assistant.plugin.mgr.i.b().a(aeVar.c);
                    PluginStartEntry a4 = a(aeVar.c, aeVar.d);
                    if (a4 != null) {
                        i3 = a4.getVersionCode();
                    } else {
                        i3 = 0;
                    }
                    int a5 = h.a(aeVar.c);
                    if (a3 != null && (a3.getVersion() >= i3 || a5 >= 0)) {
                        PluginInfo.PluginEntry pluginEntryByStartActivity = a3.getPluginEntryByStartActivity(aeVar.d);
                        if (pluginEntryByStartActivity != null) {
                            try {
                                PluginProxyActivity.a(AstApp.i(), pluginEntryByStartActivity.getHostPlugInfo().getPackageName(), pluginEntryByStartActivity.getHostPlugInfo().getVersion(), pluginEntryByStartActivity.getStartActivity(), pluginEntryByStartActivity.getHostPlugInfo().getInProcess(), null, pluginEntryByStartActivity.getHostPlugInfo().getLaunchApplication());
                            } catch (Throwable th) {
                                th.printStackTrace();
                            }
                            STInfoV2 sTInfoV2 = new STInfoV2(STConst.ST_PAGE_PLUGIN, STConst.ST_DEFAULT_SLOT, i2, STConst.ST_DEFAULT_SLOT, 100);
                            sTInfoV2.extraData = (a4 != null ? Integer.valueOf(a4.getPluginId()) : "0") + "|" + pluginEntryByStartActivity.getHostPlugInfo().getPackageName() + "|" + pluginEntryByStartActivity.getStartActivity() + "|" + 1;
                            l.a(sTInfoV2);
                            return;
                        }
                        Toast.makeText(this.e, (int) R.string.plugin_entry_not_exist, 0).show();
                    } else if (a4 == null || a4.getPluginId() <= 0) {
                        Toast.makeText(this.e, (int) R.string.plugin_entry_not_exist, 0).show();
                    } else {
                        try {
                            Intent intent6 = new Intent(this.e, PluginDetailActivity.class);
                            intent6.putExtra("plugin_start_entry", a4);
                            intent6.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, i2);
                            this.e.startActivity(intent6);
                        } catch (Throwable th2) {
                            th2.printStackTrace();
                            if (this.j.nextInt(100) < 10) {
                                throw new RuntimeException(th2);
                            }
                        }
                    }
                } else if (aeVar.b != 1) {
                } else {
                    if (aeVar.e != null && !TextUtils.isEmpty(aeVar.e.f1125a)) {
                        ActionUrl actionUrl = aeVar.e;
                        Bundle bundle = new Bundle();
                        bundle.putInt(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, i2);
                        bundle.putSerializable("dock_plugin", b("com.assistant.accelerate"));
                        com.tencent.pangu.link.b.a(this.e, bundle, actionUrl);
                    } else if (!TextUtils.isEmpty(aeVar.c)) {
                        Intent launchIntentForPackage = this.e.getPackageManager().getLaunchIntentForPackage(aeVar.c);
                        launchIntentForPackage.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, i2);
                        if (!TextUtils.isEmpty(aeVar.d)) {
                            launchIntentForPackage.setClassName(this.e, aeVar.d);
                        }
                        if (!(this.e instanceof Activity)) {
                            launchIntentForPackage.setFlags(268435456);
                        }
                        this.e.startActivity(launchIntentForPackage);
                    }
                }
            } else if ("photo_backup".equals(str)) {
                try {
                    Intent intent7 = new Intent(this.e, Class.forName("com.tencent.assistant.activity.PhotoBackupNewActivity"));
                    intent7.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, i2);
                    intent7.putExtra("activityTitleName", this.e.getResources().getString(R.string.cmn_to_pc));
                    this.e.startActivity(intent7);
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            } else if ("connect_pc".equals(str)) {
                try {
                    Intent intent8 = new Intent(this.e, Class.forName("com.tencent.connector.ConnectionActivity"));
                    intent8.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, i2);
                    intent8.putExtra("activityTitleName", this.e.getResources().getString(R.string.cmn_to_pc));
                    this.e.startActivity(intent8);
                } catch (Exception e3) {
                    e3.printStackTrace();
                }
            } else if ("applist_backup".equals(str)) {
                Intent intent9 = new Intent(this.e, AppBackupActivity.class);
                intent9.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, i2);
                this.e.startActivity(intent9);
            } else if ("secure_scan".equals(str)) {
                Intent intent10 = new Intent(this.e, StartScanActivity.class);
                intent10.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, i2);
                this.e.startActivity(intent10);
            }
        }
    }

    private PluginStartEntry b(String str) {
        if (str == null) {
            return null;
        }
        for (PluginStartEntry next : this.h) {
            if (next.getPackageName().equals(str)) {
                return next;
            }
        }
        return null;
    }

    private PluginStartEntry a(String str, String str2) {
        if (str == null) {
            return null;
        }
        for (PluginStartEntry next : this.h) {
            if (next.getPackageName().equals(str) && next.getStartActivity() != null && next.getStartActivity().equals(str2)) {
                return next;
            }
        }
        return null;
    }

    public void notifyDataSetChanged() {
        d();
        super.notifyDataSetChanged();
        if (this.e instanceof AssistantTabActivity) {
            XLog.i(this.d, ">>trigger notifyDataSetChanged>>");
            ((AssistantTabActivity) this.e).u();
        }
    }

    public void start() {
    }

    public void success(List<PluginDownloadInfo> list) {
        notifyDataSetChanged();
    }

    public void failed(int i2) {
        Toast.makeText(this.e, (int) R.string.plugin_list_fail, 0).show();
    }

    public String a(int i2) {
        return "07_" + bm.a(i2 + 1);
    }

    private STInfoV2 b(int i2) {
        String a2 = a(i2);
        if (!(this.e instanceof BaseActivity)) {
            return STInfoBuilder.buildSTInfo(this.e, 100);
        }
        if (this.k == null) {
            this.k = new b();
        }
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.e, null, a2, 100, null);
        this.k.exposure(buildSTInfo);
        return buildSTInfo;
    }
}
