package com.tencent.nucleus.manager.component;

import android.view.View;
import android.view.animation.Animation;

/* compiled from: ProGuard */
class b implements Animation.AnimationListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ View f2862a;
    final /* synthetic */ boolean b;
    final /* synthetic */ int c;
    final /* synthetic */ e d;
    final /* synthetic */ AnimationExpandableListView e;

    b(AnimationExpandableListView animationExpandableListView, View view, boolean z, int i, e eVar) {
        this.e = animationExpandableListView;
        this.f2862a = view;
        this.b = z;
        this.c = i;
        this.d = eVar;
    }

    public void onAnimationStart(Animation animation) {
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationEnd(Animation animation) {
        this.f2862a.setVisibility(4);
        if (this.b) {
            this.e.a(this.f2862a, this.c, this.d);
        } else if (this.d != null) {
            this.d.a();
        }
    }
}
