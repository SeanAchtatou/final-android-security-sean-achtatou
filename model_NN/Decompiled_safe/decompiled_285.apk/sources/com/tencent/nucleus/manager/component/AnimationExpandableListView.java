package com.tencent.nucleus.manager.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXExpandableListView;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
public class AnimationExpandableListView extends TXExpandableListView {
    volatile boolean f = false;
    volatile int g = 0;
    e h = null;

    public AnimationExpandableListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.manager.component.AnimationExpandableListView.a(android.view.View, int, com.tencent.nucleus.manager.component.e, boolean):void
     arg types: [android.view.View, int, com.tencent.nucleus.manager.component.a, int]
     candidates:
      com.tencent.nucleus.manager.component.AnimationExpandableListView.a(com.tencent.nucleus.manager.component.AnimationExpandableListView, android.view.View, int, com.tencent.nucleus.manager.component.e):void
      com.tencent.nucleus.manager.component.AnimationExpandableListView.a(int, int, boolean, com.tencent.nucleus.manager.component.e):void
      com.tencent.nucleus.manager.component.AnimationExpandableListView.a(android.view.View, int, com.tencent.nucleus.manager.component.e, boolean):void */
    public void a(int i, e eVar) {
        ExpandableListView contentView = getContentView();
        int firstVisiblePosition = contentView.getFirstVisiblePosition();
        int lastVisiblePosition = contentView.getLastVisiblePosition();
        if (i == -1) {
            i = firstVisiblePosition;
        }
        if (i < firstVisiblePosition || i > lastVisiblePosition) {
            eVar.a();
            return;
        }
        View childAt = contentView.getChildAt(i - firstVisiblePosition);
        ExpandableListView.getPackedPositionType(contentView.getExpandableListPosition(i));
        a(childAt, 0, (e) new a(this, i, eVar), false);
    }

    public void c() {
        ExpandableListView contentView = getContentView();
        int childCount = contentView.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = contentView.getChildAt(i);
            childAt.setAnimation(null);
            childAt.setVisibility(0);
        }
    }

    public void a(int i, int i2, boolean z, e eVar) {
        ExpandableListAdapter rawAdapter = getRawAdapter();
        if (i >= 0 && i < rawAdapter.getGroupCount() && i2 >= 0 && i2 < rawAdapter.getChildrenCount(i)) {
            ExpandableListView contentView = getContentView();
            long packedPositionForChild = ExpandableListView.getPackedPositionForChild(i, i2);
            int firstVisiblePosition = contentView.getFirstVisiblePosition();
            int lastVisiblePosition = contentView.getLastVisiblePosition();
            int flatListPosition = contentView.getFlatListPosition(packedPositionForChild);
            if (flatListPosition >= firstVisiblePosition && flatListPosition <= lastVisiblePosition) {
                a(contentView.getChildAt(flatListPosition - firstVisiblePosition), flatListPosition, eVar, z);
            } else if (eVar != null) {
                eVar.a();
            }
        }
    }

    private void a(View view, int i, e eVar, boolean z) {
        Animation loadAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.item_delete_anim);
        loadAnimation.setDuration(200);
        loadAnimation.setFillEnabled(true);
        loadAnimation.setFillAfter(true);
        loadAnimation.setFillBefore(false);
        loadAnimation.setAnimationListener(new b(this, view, z, i, eVar));
        view.startAnimation(loadAnimation);
    }

    /* access modifiers changed from: private */
    public void a(View view, int i, e eVar) {
        d dVar = new d(this, view);
        dVar.setDuration(100);
        dVar.setAnimationListener(new c(this, view, eVar));
        startAnimation(dVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.manager.component.AnimationExpandableListView.a(android.view.View, int, com.tencent.nucleus.manager.component.e, boolean):void
     arg types: [android.view.View, int, com.tencent.nucleus.manager.component.e, int]
     candidates:
      com.tencent.nucleus.manager.component.AnimationExpandableListView.a(com.tencent.nucleus.manager.component.AnimationExpandableListView, android.view.View, int, com.tencent.nucleus.manager.component.e):void
      com.tencent.nucleus.manager.component.AnimationExpandableListView.a(int, int, boolean, com.tencent.nucleus.manager.component.e):void
      com.tencent.nucleus.manager.component.AnimationExpandableListView.a(android.view.View, int, com.tencent.nucleus.manager.component.e, boolean):void */
    public void onScrollStateChanged(AbsListView absListView, int i) {
        super.onScrollStateChanged(absListView, i);
        if (i == 0 && this.f) {
            ExpandableListView contentView = getContentView();
            int firstVisiblePosition = contentView.getFirstVisiblePosition();
            int lastVisiblePosition = contentView.getLastVisiblePosition();
            View childAt = contentView.getChildAt(this.g - firstVisiblePosition);
            XLog.d("AnimationExpand", "positin = " + this.g + " firstVisiblePosition = " + firstVisiblePosition + " lastVisiblePosition = " + lastVisiblePosition);
            a(childAt, this.g, this.h, true);
            this.f = false;
        }
    }

    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
        super.onScroll(absListView, i, i2, i3);
    }
}
