package com.tencent.nucleus.manager.spaceclean;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.text.SpannableString;
import android.view.KeyEvent;
import android.view.ViewStub;
import android.widget.RelativeLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.Global;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.activity.InstalledAppManagerActivity;
import com.tencent.assistant.component.FooterView;
import com.tencent.assistant.component.NormalErrorPage;
import com.tencent.assistant.m;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.plugin.PluginStartEntry;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.at;
import com.tencent.assistant.utils.t;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import com.tencent.assistantv2.st.b.b;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.beacon.event.a;
import com.tencent.connect.common.Constants;
import com.tencent.nucleus.manager.component.ScaningProgressView;
import com.tencent.nucleus.manager.component.TxManagerCommContainView;
import com.tencent.nucleus.manager.main.AssistantTabActivity;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/* compiled from: ProGuard */
public class BigFileCleanActivity extends BaseActivity {
    private BigFileListView A;
    private ViewStub B = null;
    private NormalErrorPage C = null;
    /* access modifiers changed from: private */
    public ArrayList<n> D = new ArrayList<>();
    /* access modifiers changed from: private */
    public int E = 0;
    /* access modifiers changed from: private */
    public long F = 0;
    /* access modifiers changed from: private */
    public boolean G = false;
    /* access modifiers changed from: private */
    public boolean H = false;
    /* access modifiers changed from: private */
    public boolean I = false;
    private PluginStartEntry J = null;
    /* access modifiers changed from: private */
    public long K = 0;
    /* access modifiers changed from: private */
    public long L = 0;
    /* access modifiers changed from: private */
    public long M = 0;
    /* access modifiers changed from: private */
    public long N = 0;
    private ad O = new b(this);
    /* access modifiers changed from: private */
    public Handler P = new h(this);
    /* access modifiers changed from: private */
    public Context n;
    private boolean u = false;
    private boolean v = false;
    private SecondNavigationTitleViewV5 w;
    /* access modifiers changed from: private */
    public FooterView x = null;
    /* access modifiers changed from: private */
    public TxManagerCommContainView y;
    /* access modifiers changed from: private */
    public ScaningProgressView z;

    static /* synthetic */ int a(BigFileCleanActivity bigFileCleanActivity) {
        int i = bigFileCleanActivity.E;
        bigFileCleanActivity.E = i + 1;
        return i;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_rubbish_clear);
        this.n = this;
        w();
        u();
        t();
        x();
        y();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        if (this.w != null) {
            this.w.l();
        }
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        if (this.w != null) {
            this.w.m();
        }
        super.onPause();
    }

    private void u() {
        SpaceScanManager.a().a(this.O);
    }

    private void v() {
        SpaceScanManager.a().b(this.O);
    }

    private void w() {
        this.w = (SecondNavigationTitleViewV5) findViewById(R.id.title_view);
        this.w.a(this);
        this.w.b(getString(R.string.space_clean_big_file_title));
        this.w.d();
        this.w.c(new e(this));
        this.y = (TxManagerCommContainView) findViewById(R.id.contentView);
        this.z = new ScaningProgressView(this.n);
        this.A = new BigFileListView(this.n);
        this.A.a(this.P);
        this.y.a(this.z);
        this.y.b(this.A);
        this.x = new FooterView(this.n);
        this.y.a(this.x, new RelativeLayout.LayoutParams(-1, -2));
        this.x.updateContent(getString(R.string.rubbish_clear_one_key_delete));
        this.x.setFooterViewEnable(false);
        this.x.setOnFooterViewClickListener(new f(this));
        this.B = (ViewStub) findViewById(R.id.error_stub);
    }

    private void a(String str, String str2, String str3, String str4, String str5) {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", Global.getQUAForBeacon());
        hashMap.put("B2", Global.getPhoneGuidAndGen());
        hashMap.put("B3", str);
        hashMap.put("B4", str2);
        hashMap.put("B5", str3);
        hashMap.put("B6", str4);
        hashMap.put("B7", str5);
        hashMap.put("B8", Constants.STR_EMPTY);
        hashMap.put("B9", t.g());
        XLog.d("beacon", "beacon report >> cleanLargeFiles. " + hashMap.toString());
        a.a("cleanLargeFiles", true, -1, -1, hashMap, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean
     arg types: [java.lang.String, boolean]
     candidates:
      com.tencent.assistant.m.b(byte, int):void
      com.tencent.assistant.m.b(byte, java.lang.String):void
      com.tencent.assistant.m.b(int, byte[]):void
      com.tencent.assistant.m.b(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.b(java.lang.Long, int):boolean
      com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean */
    public void t() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.u = extras.getBoolean(com.tencent.assistant.a.a.O);
            if (this.s) {
                m.a().b("key_space_clean_last_push_clicked", (Object) true);
            }
            this.J = (PluginStartEntry) extras.getSerializable("dock_plugin");
        }
    }

    private void x() {
        XLog.d("miles", "initData called...");
        RubbishItemView.b = false;
        this.D.clear();
        this.z.a(0, 1);
        this.y.a();
        this.K = System.currentTimeMillis();
        if (!SpaceScanManager.a().o()) {
            TemporaryThreadManager.get().start(new g(this));
        }
    }

    private void y() {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", Global.getPhoneGuidAndGen());
        hashMap.put("B2", Global.getQUAForBeacon());
        hashMap.put("B3", t.g());
        XLog.d("beacon", "beacon report >> expose_bigfileclean. " + hashMap.toString());
        a.a("expose_bigfileclean", true, -1, -1, hashMap, true);
    }

    /* access modifiers changed from: private */
    public void z() {
        this.v = true;
        this.M = System.currentTimeMillis();
        TemporaryThreadManager.get().start(new i(this));
    }

    /* access modifiers changed from: private */
    public void A() {
        TemporaryThreadManager.get().start(new j(this));
    }

    /* access modifiers changed from: private */
    public boolean a(PackageManager packageManager, String str) {
        try {
            ApplicationInfo applicationInfo = packageManager.getApplicationInfo(str, 0);
            if ((applicationInfo.flags & 1) == 0 || (applicationInfo.flags & 128) != 0) {
                return true;
            }
            return false;
        } catch (Throwable th) {
            th.printStackTrace();
            return false;
        }
    }

    /* access modifiers changed from: private */
    public long a(n nVar) {
        long j = 0;
        if (this.D == null || this.D.size() <= 0) {
            return 0;
        }
        Iterator<n> it = this.D.iterator();
        while (true) {
            long j2 = j;
            if (!it.hasNext()) {
                return j2;
            }
            n next = it.next();
            if (nVar != null && nVar.b.equals(next.b)) {
                next.c = nVar.c;
                next.a(nVar.f);
            }
            j = j2 + next.c;
        }
    }

    /* access modifiers changed from: private */
    public ArrayList<String> B() {
        long j;
        long j2;
        long j3;
        long j4;
        long j5;
        this.F = 0;
        ArrayList<String> arrayList = new ArrayList<>();
        if (this.D == null || this.D.size() <= 0) {
            j = 0;
            j2 = 0;
            j3 = 0;
            j4 = 0;
            j5 = 0;
        } else {
            Iterator<n> it = this.D.iterator();
            long j6 = 0;
            long j7 = 0;
            long j8 = 0;
            long j9 = 0;
            long j10 = 0;
            while (it.hasNext()) {
                n next = it.next();
                Iterator<SubRubbishInfo> it2 = next.f.iterator();
                long j11 = j6;
                long j12 = j7;
                long j13 = j8;
                long j14 = j9;
                long j15 = j10;
                while (it2.hasNext()) {
                    SubRubbishInfo next2 = it2.next();
                    if (next2.d) {
                        arrayList.addAll(next2.f);
                        this.F = this.F + next2.c;
                        switch (d.f3027a[next2.f3011a.ordinal()]) {
                            case 1:
                                j14 += next2.c;
                                break;
                            case 2:
                                j15 += next2.c;
                                break;
                            case 3:
                                j13 += next2.c;
                                break;
                            case 4:
                                j12 += next2.c;
                                break;
                            case 5:
                                j11 += next2.c;
                                break;
                        }
                        it2.remove();
                    }
                }
                if (next.f.isEmpty()) {
                    it.remove();
                }
                j6 = j11;
                j7 = j12;
                j8 = j13;
                j10 = j15;
                j9 = j14;
            }
            j = j9;
            j2 = j10;
            j3 = j7;
            j4 = j8;
            j5 = j6;
        }
        DecimalFormat decimalFormat = new DecimalFormat("#0.00");
        a(decimalFormat.format(((((double) j) * 1.0d) / 1024.0d) / 1024.0d), decimalFormat.format(((((double) j2) * 1.0d) / 1024.0d) / 1024.0d), decimalFormat.format(((((double) j4) * 1.0d) / 1024.0d) / 1024.0d), decimalFormat.format(((((double) j3) * 1.0d) / 1024.0d) / 1024.0d), decimalFormat.format(((((double) j5) * 1.0d) / 1024.0d) / 1024.0d));
        return arrayList;
    }

    /* access modifiers changed from: private */
    public void a(long j, boolean z2) {
        if (z2) {
            b(at.c(j));
        } else {
            b(1);
        }
    }

    private void b(String str) {
        STInfoV2 C2 = C();
        this.y.a(getString(R.string.apkmgr_clear_success), new SpannableString(String.format(getString(R.string.rubbish_clear_big_file_delete_tips), str)));
        float c = (float) t.c();
        float d = (float) t.d();
        if (!(d == 0.0f || c == 0.0f)) {
            float f = c / d;
            XLog.d("miles", "SpaceCleanActivity >> internal avaliable memory percent is " + f);
            if (f < 0.2f) {
                this.y.a(R.drawable.icon_space_clean_xiaobao, getString(R.string.rubbish_skip_to_app_uninstall_tip), 20, 20, 4);
                this.y.a(this, InstalledAppManagerActivity.class, (PluginStartEntry) null);
                C2.slotId = "03_001";
                l.a(C2);
                c(getString(R.string.soft_admin));
                return;
            }
        }
        if (this.G && this.J != null) {
            this.y.a(R.drawable.icon_space_clean_xiaobao, getString(R.string.rubbish_skip_to_accelerate_plugin_tip), 20, 20, 3);
            this.y.a(this, this.J);
            C2.slotId = "03_001";
            l.a(C2);
            c(getString(R.string.mobile_accelerate_title));
        }
    }

    private void c(String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", Global.getPhoneGuidAndGen());
        hashMap.put("B2", Global.getQUAForBeacon());
        hashMap.put("B3", t.g());
        hashMap.put("B4", getString(R.string.space_clean_big_file_title));
        hashMap.put("B5", str);
        XLog.d("beacon", "beacon report >> featureLinkExp. " + hashMap.toString());
        a.a("featureLinkExp", true, -1, -1, hashMap, true);
    }

    private STInfoV2 C() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this, 100);
        buildSTInfo.scene = STConst.ST_PAGE_RUBBISH_CLEAR_FINISH_STEWARD;
        b.getInstance().exposure(buildSTInfo);
        return buildSTInfo;
    }

    private void b(int i) {
        if (this.C == null) {
            this.B.inflate();
            this.C = (NormalErrorPage) findViewById(R.id.error);
        }
        this.C.setErrorType(i);
        if (i == 1) {
            this.C.setErrorHint(getResources().getString(R.string.rubbish_clear_empty_tips));
            this.C.setErrorImage(R.drawable.emptypage_pic_02);
            this.C.setErrorHintTextColor(getResources().getColor(R.color.common_listiteminfo));
            this.C.setErrorHintTextSize(getResources().getDimension(R.dimen.appadmin_empty_page_text_size));
            this.C.setErrorTextVisibility(8);
            this.C.setErrorHintVisibility(0);
            this.C.setFreshButtonVisibility(8);
        }
        this.y.setVisibility(8);
        this.x.setVisibility(8);
        this.C.setVisibility(0);
    }

    /* access modifiers changed from: private */
    public void a(long j, boolean z2, int i) {
        if (z2) {
            this.z.a(j);
        } else if (i == 1) {
            this.z.b(j);
            this.z.c();
            this.P.post(new k(this, i));
        } else if (i == 2) {
            this.z.a(j);
            this.P.postDelayed(new m(this), 1000);
        } else {
            this.z.b(j);
            this.P.post(new c(this));
        }
    }

    /* access modifiers changed from: private */
    public void b(boolean z2) {
        if (z2) {
            this.A.setVisibility(8);
        } else if (this.D != null && !this.D.isEmpty()) {
            this.A.a(this.D);
            this.A.setVisibility(0);
            if (this.C != null) {
                this.C.setVisibility(8);
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(long j) {
        String c = at.c(j);
        String string = getString(R.string.rubbish_clear_one_key_delete);
        if (j > 0) {
            this.x.setFooterViewEnable(true);
            this.x.updateContent(string, " " + String.format(getString(R.string.rubbish_clear_one_key_delete_extra), c));
            return;
        }
        this.x.setFooterViewEnable(false);
        this.x.updateContent(string);
    }

    public int f() {
        return STConst.ST_PAGE_BIG_FILE_CLEAN_STEWARD;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean
     arg types: [java.lang.String, boolean]
     candidates:
      com.tencent.assistant.m.b(byte, int):void
      com.tencent.assistant.m.b(byte, java.lang.String):void
      com.tencent.assistant.m.b(int, byte[]):void
      com.tencent.assistant.m.b(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.b(java.lang.Long, int):boolean
      com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean */
    /* access modifiers changed from: protected */
    public void onDestroy() {
        v();
        com.tencent.assistant.utils.b.a();
        if (this.u && this.v) {
            m.a().b("key_space_clean_has_run_clean", (Object) true);
        }
        super.onDestroy();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (4 != i) {
            return super.onKeyDown(i, keyEvent);
        }
        D();
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: private */
    public void D() {
        if (!this.s) {
            finish();
            return;
        }
        Intent intent = new Intent(this, AssistantTabActivity.class);
        intent.setFlags(67108864);
        intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, f());
        intent.putExtra(com.tencent.assistant.a.a.G, true);
        startActivity(intent);
        this.s = false;
        finish();
        XLog.i("miles", "BigFileCleanActivity >> key back finish");
    }
}
