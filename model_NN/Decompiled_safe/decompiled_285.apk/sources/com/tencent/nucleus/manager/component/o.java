package com.tencent.nucleus.manager.component;

import android.os.Handler;
import android.os.Message;

/* compiled from: ProGuard */
class o extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RollTextView f2871a;

    o(RollTextView rollTextView) {
        this.f2871a = rollTextView;
    }

    public void handleMessage(Message message) {
        switch (message.what) {
            case 100000:
                if (this.f2871a.c > 0.0d && this.f2871a.f2841a < this.f2871a.b) {
                    this.f2871a.setText(this.f2871a.c(this.f2871a.d(this.f2871a.f2841a)));
                    RollTextView.c(this.f2871a, this.f2871a.c);
                    this.f2871a.m.sendEmptyMessageDelayed(100000, 50);
                } else if (this.f2871a.c >= 0.0d || this.f2871a.f2841a <= this.f2871a.b) {
                    this.f2871a.setText(this.f2871a.c(this.f2871a.d(this.f2871a.b)));
                    double unused = this.f2871a.f2841a = this.f2871a.b;
                } else {
                    this.f2871a.setText(this.f2871a.c(this.f2871a.d(this.f2871a.f2841a)));
                    RollTextView.c(this.f2871a, this.f2871a.c);
                    this.f2871a.m.sendEmptyMessageDelayed(100000, 50);
                }
                this.f2871a.e(this.f2871a.f2841a);
                return;
            default:
                return;
        }
    }
}
