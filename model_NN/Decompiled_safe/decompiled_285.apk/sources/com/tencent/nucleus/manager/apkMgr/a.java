package com.tencent.nucleus.manager.apkMgr;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.CommonViewInvalidater;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.at;
import com.tencent.assistant.utils.bm;
import com.tencent.assistantv2.component.MovingProgressBar;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import com.tencent.nucleus.manager.component.m;
import com.tencent.nucleus.manager.component.n;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/* compiled from: ProGuard */
public class a extends BaseExpandableListAdapter implements m {

    /* renamed from: a  reason: collision with root package name */
    public Handler f2764a = null;
    public int b = 0;
    public long c = 0;
    public int d = 0;
    public long e = 0;
    private LinkedHashMap<Integer, ArrayList<LocalApkInfo>> f = new LinkedHashMap<>();
    private ArrayList<Integer> g = new ArrayList<>();
    private Map<String, Drawable> h = new HashMap();
    /* access modifiers changed from: private */
    public Context i;
    private LayoutInflater j;
    private CommonViewInvalidater k = new b(this);
    /* access modifiers changed from: private */
    public boolean l = true;
    private int m = 0;

    public a() {
    }

    public a(Context context, View view) {
        this.i = context;
        this.j = LayoutInflater.from(this.i);
    }

    private void i() {
        int i2 = 0;
        ArrayList arrayList = this.f.get(4);
        this.b = arrayList != null ? arrayList.size() : 0;
        this.c = a(arrayList);
        ArrayList arrayList2 = this.f.get(3);
        if (arrayList2 != null) {
            i2 = arrayList2.size();
        }
        this.d = i2;
        this.e = a(arrayList2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.manager.apkMgr.a.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.tencent.nucleus.manager.apkMgr.a.a(int, int):com.tencent.assistant.localres.model.LocalApkInfo
      com.tencent.nucleus.manager.apkMgr.a.a(java.util.Map<java.lang.Integer, java.util.ArrayList<com.tencent.assistant.localres.model.LocalApkInfo>>, boolean):void
      com.tencent.nucleus.manager.apkMgr.a.a(int, boolean):void */
    public void a(Map<Integer, ArrayList<LocalApkInfo>> map, boolean z) {
        if (map != null && !map.isEmpty()) {
            this.f.clear();
            this.f.putAll(map);
            this.g.clear();
            for (Integer add : this.f.keySet()) {
                this.g.add(add);
            }
            i();
            if (z) {
                int size = this.g.size();
                for (int i2 = 0; i2 < size; i2++) {
                    if (this.g.get(i2).intValue() == 3) {
                        a(i2, true);
                        if (this.f2764a != null) {
                            this.f2764a.sendMessage(this.f2764a.obtainMessage(110005, new LocalApkInfo()));
                        }
                    }
                }
            }
            notifyDataSetChanged();
        }
    }

    /* renamed from: a */
    public LocalApkInfo getChild(int i2, int i3) {
        ArrayList arrayList;
        if (this.f == null || this.g == null || i2 < 0 || this.g.size() <= i2 || (arrayList = this.f.get(this.g.get(i2))) == null || i3 < 0 || arrayList.size() <= i3) {
            return null;
        }
        return (LocalApkInfo) arrayList.get(i3);
    }

    public long getChildId(int i2, int i3) {
        return (long) i3;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.manager.apkMgr.a.a(com.tencent.nucleus.manager.apkMgr.i, com.tencent.assistant.localres.model.LocalApkInfo, com.tencent.assistantv2.st.page.STInfoV2):void
     arg types: [com.tencent.nucleus.manager.apkMgr.j, com.tencent.assistant.localres.model.LocalApkInfo, com.tencent.assistantv2.st.page.STInfoV2]
     candidates:
      com.tencent.nucleus.manager.apkMgr.a.a(com.tencent.nucleus.manager.apkMgr.j, com.tencent.assistant.localres.model.LocalApkInfo, com.tencent.assistantv2.st.page.STInfoV2):void
      com.tencent.nucleus.manager.apkMgr.a.a(com.tencent.nucleus.manager.apkMgr.i, com.tencent.assistant.localres.model.LocalApkInfo, com.tencent.assistantv2.st.page.STInfoV2):void */
    public View getChildView(int i2, int i3, boolean z, View view, ViewGroup viewGroup) {
        j jVar;
        LocalApkInfo a2 = getChild(i2, i3);
        int intValue = this.g.get(i2).intValue();
        if (a2 == null) {
            return new View(this.i);
        }
        if (intValue == 4) {
            if (view == null || view.getTag() == null || !view.getTag().getClass().getSimpleName().equals(j.class.getSimpleName())) {
                view = this.j.inflate((int) R.layout.apkmgr_item_for_keep, (ViewGroup) null);
                j jVar2 = new j(null);
                view.setTag(jVar2);
                jVar2.f2772a = (RelativeLayout) view.findViewById(R.id.content_layout);
                jVar2.c = (TXImageView) view.findViewById(R.id.app_icon_img);
                jVar2.e = (TextView) view.findViewById(R.id.app_version);
                jVar2.f = (TextView) view.findViewById(R.id.app_size);
                jVar2.b = (TextView) view.findViewById(R.id.check);
                jVar2.g = (ImageView) view.findViewById(R.id.last_line);
                jVar2.h = view.findViewById(R.id.bottom_margin);
                jVar2.d = (TextView) view.findViewById(R.id.app_name_txt);
                jVar2.j = (TextView) view.findViewById(R.id.tip_text);
                jVar2.i = view.findViewById(R.id.install_button);
                jVar2.k = (RelativeLayout) view.findViewById(R.id.app_info);
                jVar2.l = (MovingProgressBar) view.findViewById(R.id.app_install_progress_bar);
                jVar2.l.a(AstApp.i().getResources().getDimensionPixelSize(R.dimen.app_detail_pic_gap));
                jVar2.l.b(AstApp.i().getResources().getDimensionPixelSize(R.dimen.install_progress_bar_width));
                jVar2.m = (ImageView) view.findViewById(R.id.install_btn_img);
                jVar2.n = (TextView) view.findViewById(R.id.install_btn_txt);
                jVar2.o = (LinearLayout) view.findViewById(R.id.install_process);
                jVar = jVar2;
            } else {
                try {
                    jVar = (j) view.getTag();
                } catch (Exception e2) {
                    XLog.e("ApkMgrActivity", "CONVERTvIEW TYEP ERROR");
                    return view;
                }
            }
            if (a2.mApkState == 3) {
                jVar.k.setVisibility(8);
                jVar.m.setVisibility(8);
                jVar.n.setVisibility(0);
                jVar.o.setVisibility(0);
                jVar.l.a();
                jVar.i.setEnabled(false);
            } else {
                jVar.k.setVisibility(0);
                jVar.m.setVisibility(0);
                jVar.n.setVisibility(8);
                jVar.o.setVisibility(8);
                jVar.l.b();
                jVar.i.setEnabled(true);
            }
            jVar.c.setTag(a2.mLocalFilePath);
            STInfoV2 c2 = c(i2, i3);
            a((i) jVar, a2, c2);
            a(jVar, a2, c2);
            a(jVar.f2772a, jVar.g, jVar.h, i2, i3, getChildrenCount(i2));
            return view;
        }
        if (view == null || view.getTag() == null || !view.getTag().getClass().getSimpleName().equals(i.class.getSimpleName())) {
            view = this.j.inflate((int) R.layout.apkmgr_item_for_delete, (ViewGroup) null);
            i iVar = new i(null);
            view.setTag(iVar);
            iVar.f2772a = (RelativeLayout) view.findViewById(R.id.content_layout);
            iVar.c = (TXImageView) view.findViewById(R.id.app_icon_img);
            iVar.e = (TextView) view.findViewById(R.id.app_version);
            iVar.f = (TextView) view.findViewById(R.id.app_size);
            iVar.b = (TextView) view.findViewById(R.id.check);
            iVar.g = (ImageView) view.findViewById(R.id.last_line);
            iVar.h = view.findViewById(R.id.bottom_margin);
            iVar.d = (TextView) view.findViewById(R.id.app_name_txt);
        }
        try {
            i iVar2 = (i) view.getTag();
            iVar2.c.setTag(a2.mLocalFilePath);
            a(iVar2, a2, c(i2, i3));
            a(iVar2.f2772a, iVar2.g, iVar2.h, i2, i3, getChildrenCount(i2));
            return view;
        } catch (Exception e3) {
            XLog.e("ApkMgrActivity", "CONVERTvIEW TYEP ERROR");
            return view;
        }
    }

    public void a() {
        synchronized (this) {
            this.l = false;
        }
    }

    public void b() {
        synchronized (this) {
            this.l = true;
        }
        this.k.handleQueueMsg();
    }

    private void a(j jVar, LocalApkInfo localApkInfo, STInfoV2 sTInfoV2) {
        String string;
        if (localApkInfo != null) {
            if (localApkInfo.mIsUpdateApk) {
                string = this.i.getString(R.string.apkmgr_tip_update);
            } else {
                string = this.i.getString(R.string.apkmgr_tip_not_install);
            }
            jVar.j.setText(string);
            jVar.i.setOnClickListener(new c(this, localApkInfo, sTInfoV2));
        }
    }

    private void a(i iVar, LocalApkInfo localApkInfo, STInfoV2 sTInfoV2) {
        if (iVar != null && localApkInfo != null) {
            iVar.b.setSelected(localApkInfo.mIsSelect);
            iVar.b.setOnClickListener(new d(this, iVar, localApkInfo, sTInfoV2));
            iVar.f2772a.setOnClickListener(new e(this, iVar, localApkInfo, sTInfoV2));
            iVar.c.setInvalidater(this.k);
            if (localApkInfo.mLocalFilePath == null || this.h == null || this.h.size() <= 0 || this.h.get(localApkInfo.mLocalFilePath) == null) {
                iVar.c.setImageDrawable(this.i.getResources().getDrawable(R.drawable.pic_defaule));
                TemporaryThreadManager.get().start(new f(this, localApkInfo, iVar));
            } else {
                iVar.c.setImageDrawable(this.h.get(localApkInfo.mLocalFilePath));
            }
            iVar.d.setText(localApkInfo.mAppName);
            if (TextUtils.isEmpty(localApkInfo.mVersionName)) {
                iVar.e.setText(Constants.STR_EMPTY);
            } else {
                iVar.e.setText("版本：" + localApkInfo.mVersionName);
            }
            iVar.f.setText(at.a(localApkInfo.occupySize));
        }
    }

    private STInfoV2 c(int i2, int i3) {
        String d2 = d(i2, i3);
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.i, 100);
        buildSTInfo.slotId = d2;
        return buildSTInfo;
    }

    private String d(int i2, int i3) {
        String str;
        if (this.g.get(i2).intValue() == 4) {
            str = "03";
        } else {
            str = "04";
        }
        return str + "_" + bm.a(i3 + 1);
    }

    public int getChildrenCount(int i2) {
        ArrayList arrayList;
        if (this.f == null || i2 >= this.g.size() || (arrayList = this.f.get(this.g.get(i2))) == null) {
            return 0;
        }
        return arrayList.size();
    }

    public Object getGroup(int i2) {
        if (this.g == null || this.g.size() <= 0 || i2 >= this.g.size() || i2 < 0) {
            return null;
        }
        return b(i2);
    }

    public int getGroupCount() {
        if (this.f != null) {
            return this.f.size();
        }
        return 0;
    }

    public long getGroupId(int i2) {
        return (long) i2;
    }

    public View getGroupView(int i2, boolean z, View view, ViewGroup viewGroup) {
        k kVar;
        if (view == null) {
            view = this.j.inflate((int) R.layout.group_item, (ViewGroup) null);
            k kVar2 = new k(this);
            kVar2.f2773a = (TextView) view.findViewById(R.id.group_title);
            kVar2.b = (TextView) view.findViewById(R.id.select_all);
            kVar2.b.setVisibility(0);
            view.setTag(kVar2);
            kVar = kVar2;
        } else {
            kVar = (k) view.getTag();
        }
        kVar.f2773a.setText(b(i2));
        kVar.b.setSelected(a(i2));
        kVar.b.setOnClickListener(new h(this, i2, kVar));
        return view;
    }

    public boolean hasStableIds() {
        return true;
    }

    public boolean isChildSelectable(int i2, int i3) {
        return true;
    }

    public void a(int i2, boolean z) {
        try {
            ArrayList arrayList = this.f.get(Integer.valueOf(this.g.get(i2).intValue()));
            if (arrayList != null) {
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    ((LocalApkInfo) it.next()).mIsSelect = z;
                }
            }
        } catch (Exception e2) {
        }
    }

    public boolean a(int i2) {
        try {
            ArrayList arrayList = this.f.get(Integer.valueOf(this.g.get(i2).intValue()));
            if (arrayList == null) {
                return false;
            }
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                if (!((LocalApkInfo) it.next()).mIsSelect) {
                    return false;
                }
            }
            return true;
        } catch (Exception e2) {
            return false;
        }
    }

    public ArrayList<Pair<Integer, Integer>> c() {
        int size = this.g.size();
        ArrayList<Pair<Integer, Integer>> arrayList = new ArrayList<>();
        for (int i2 = 0; i2 < size; i2++) {
            ArrayList arrayList2 = this.f.get(Integer.valueOf(this.g.get(i2).intValue()));
            if (arrayList2 != null) {
                Iterator it = arrayList2.iterator();
                int i3 = 0;
                while (it.hasNext()) {
                    if (((LocalApkInfo) it.next()).mIsSelect) {
                        arrayList.add(new Pair(Integer.valueOf(i2), Integer.valueOf(i3)));
                    }
                    i3++;
                }
            }
        }
        return arrayList;
    }

    private String b(int i2) {
        String str;
        if (this.g == null || i2 >= this.g.size()) {
            return Constants.STR_EMPTY;
        }
        if (this.g.get(i2).intValue() == 4) {
            if (this.f.get(4) != null) {
                str = String.format(this.i.getString(R.string.apkmgr_group_suggest_keep_txt), Integer.valueOf(this.b), at.a(this.c));
            } else {
                str = Constants.STR_EMPTY;
            }
            return str;
        } else if (this.g.get(i2).intValue() != 3 || this.f.get(3) == null) {
            return Constants.STR_EMPTY;
        } else {
            return String.format(this.i.getString(R.string.apkmgr_group_suggest_delete_txt), Integer.valueOf(this.d), at.a(this.e));
        }
    }

    public long a(List<LocalApkInfo> list) {
        long j2 = 0;
        if (list == null) {
            return 0;
        }
        Iterator<LocalApkInfo> it = list.iterator();
        while (true) {
            long j3 = j2;
            if (!it.hasNext()) {
                return j3;
            }
            LocalApkInfo next = it.next();
            if (next != null) {
                j2 = j3 + next.occupySize;
            } else {
                j2 = j3;
            }
        }
    }

    public String b(int i2, int i3) {
        String str;
        if (i2 == 0) {
            str = "03_";
        } else {
            str = "06_";
        }
        return str + bm.a(i3 + 1);
    }

    public Drawable a(LocalApkInfo localApkInfo) {
        Drawable drawable = null;
        if (localApkInfo != null) {
            String str = localApkInfo.mLocalFilePath;
            if (!TextUtils.isEmpty(str) && (drawable = this.h.get(str)) == null) {
                PackageManager packageManager = AstApp.i().getBaseContext().getPackageManager();
                try {
                    PackageInfo packageArchiveInfo = packageManager.getPackageArchiveInfo(localApkInfo.mLocalFilePath, 0);
                    if (packageArchiveInfo != null) {
                        ApplicationInfo applicationInfo = packageArchiveInfo.applicationInfo;
                        applicationInfo.sourceDir = localApkInfo.mLocalFilePath;
                        applicationInfo.publicSourceDir = localApkInfo.mLocalFilePath;
                        drawable = packageManager.getApplicationIcon(applicationInfo);
                        if (drawable != null) {
                            this.h.put(str, drawable);
                        }
                    }
                } catch (Throwable th) {
                    th.printStackTrace();
                }
            }
        }
        return drawable;
    }

    public void a(RelativeLayout relativeLayout, ImageView imageView, View view, int i2, int i3, int i4) {
        if (i4 == 1) {
            imageView.setVisibility(8);
            view.setVisibility(0);
        } else if (i3 == 0) {
            imageView.setVisibility(0);
            view.setVisibility(8);
        } else if (i3 == i4 - 1) {
            imageView.setVisibility(8);
            if (i2 == getGroupCount() - 1) {
                view.setVisibility(0);
            } else {
                view.setVisibility(8);
            }
        } else {
            imageView.setVisibility(0);
            view.setVisibility(8);
        }
    }

    public void d() {
        int i2;
        int i3;
        int size = this.f.size();
        int i4 = 0;
        while (i4 < size) {
            int intValue = this.g.get(i4).intValue();
            ArrayList arrayList = this.f.get(Integer.valueOf(intValue));
            if (arrayList != null) {
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    if (((LocalApkInfo) it.next()).mIsSelect) {
                        it.remove();
                    }
                }
                if (arrayList.isEmpty()) {
                    this.f.remove(Integer.valueOf(intValue));
                    this.g.remove(i4);
                    i2 = i4 - 1;
                    i3 = size - 1;
                    i4 = i2 + 1;
                    size = i3;
                }
            }
            i2 = i4;
            i3 = size;
            i4 = i2 + 1;
            size = i3;
        }
        i();
        notifyDataSetChanged();
    }

    public void e() {
        this.m = 0;
    }

    public void f() {
        this.m++;
    }

    public n g() {
        ArrayList<Pair<Integer, Integer>> c2 = c();
        if (c2 == null || c2.isEmpty() || this.m >= c2.size()) {
            return null;
        }
        Pair pair = c2.get(this.m);
        return new n(((Integer) pair.first).intValue(), ((Integer) pair.second).intValue());
    }

    public Object h() {
        n g2 = g();
        if (g2 == null) {
            return null;
        }
        return getChild(g2.f2870a, g2.b);
    }
}
