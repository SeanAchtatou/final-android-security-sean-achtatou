package com.tencent.nucleus.manager.component;

import android.content.Context;
import android.text.Html;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.OverTurnView;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.protocol.jce.AppUpdateInfo;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public class AssistantUpdateOverTurnView extends OverTurnView {

    /* renamed from: a  reason: collision with root package name */
    private Context f2839a;
    private LayoutInflater b;
    private TXImageView c;
    private TextView d;

    public AssistantUpdateOverTurnView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public View makeView() {
        View inflate = this.b.inflate((int) R.layout.item_asistant_update, (ViewGroup) null);
        this.c = (TXImageView) inflate.findViewById(R.id.app_icon);
        this.d = (TextView) inflate.findViewById(R.id.app_text);
        return inflate;
    }

    public void a(AppUpdateInfo appUpdateInfo) {
        if (appUpdateInfo == null) {
            if (this.c != null) {
                this.c.setVisibility(8);
            }
            if (this.d != null) {
                this.d.setText((int) R.string.p_no_update);
                return;
            }
            return;
        }
        View nextView = getNextView();
        this.c = (TXImageView) nextView.findViewById(R.id.app_icon);
        this.d = (TextView) nextView.findViewById(R.id.app_text);
        String a2 = appUpdateInfo.e != null ? appUpdateInfo.e.a() : Constants.STR_EMPTY;
        String str = TextUtils.isEmpty(appUpdateInfo.x) ? Constants.STR_EMPTY : appUpdateInfo.x;
        if (this.c != null) {
            this.c.setVisibility(0);
            this.c.updateImageView(a2, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
        }
        if (this.d != null) {
            if (TextUtils.isEmpty(str)) {
                this.d.setText(this.f2839a.getString(R.string.p_default_update_tips));
            } else {
                try {
                    this.d.setText(Html.fromHtml(str));
                } catch (Throwable th) {
                    this.d.setText(this.f2839a.getString(R.string.p_default_update_tips));
                }
            }
        }
        next();
    }

    public void a() {
        if (this.c != null) {
            this.c.setVisibility(8);
        }
        if (this.d != null) {
            this.d.setText(Constants.STR_EMPTY);
        }
    }

    public void doBeforeInit(Context context) {
        this.f2839a = context;
        this.b = LayoutInflater.from(this.f2839a);
    }

    public Animation setInDownAnim(Context context) {
        return AnimationUtils.loadAnimation(context, R.anim.mgr_fade_in);
    }

    public Animation setOutDownAnim(Context context) {
        return AnimationUtils.loadAnimation(context, R.anim.mgr_fade_out);
    }

    public Animation setInUpAnim(Context context) {
        return AnimationUtils.loadAnimation(context, R.anim.mgr_fade_in);
    }

    public Animation setOutUpAnim(Context context) {
        return AnimationUtils.loadAnimation(context, R.anim.mgr_fade_out);
    }
}
