package com.tencent.nucleus.manager.floatingwindow;

import android.os.Parcel;
import android.os.Parcelable;
import com.tencent.nucleus.manager.floatingwindow.WaveView;

/* compiled from: ProGuard */
final class ae implements Parcelable.Creator<WaveView.SavedState> {
    ae() {
    }

    /* renamed from: a */
    public WaveView.SavedState createFromParcel(Parcel parcel) {
        return new WaveView.SavedState(parcel);
    }

    /* renamed from: a */
    public WaveView.SavedState[] newArray(int i) {
        return new WaveView.SavedState[i];
    }
}
