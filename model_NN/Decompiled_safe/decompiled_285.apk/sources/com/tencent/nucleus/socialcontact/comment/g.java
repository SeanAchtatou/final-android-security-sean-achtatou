package com.tencent.nucleus.socialcontact.comment;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.module.BaseEngine;
import com.tencent.assistant.protocol.jce.CommentAppRequest;
import com.tencent.assistant.protocol.jce.CommentAppResponse;
import com.tencent.assistant.protocol.jce.CommentDetail;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
public class g extends BaseEngine<j> {
    public int a(CommentDetail commentDetail, long j, long j2, String str, String str2, String str3) {
        CommentAppRequest commentAppRequest = new CommentAppRequest();
        commentAppRequest.f1200a = j;
        commentAppRequest.b = j2;
        commentAppRequest.j = str2;
        commentAppRequest.k = str3;
        commentAppRequest.c = commentDetail.b;
        commentAppRequest.d = 0;
        commentAppRequest.e = commentDetail.c;
        commentAppRequest.f = commentDetail.d;
        commentAppRequest.g = commentDetail.g;
        commentAppRequest.h = str;
        commentAppRequest.i = commentDetail.i;
        XLog.d("comment", "CommentAppEngine.sendRequest, CommentAppRequest=" + commentAppRequest.toString());
        return send(commentAppRequest);
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        CommentAppResponse commentAppResponse = (CommentAppResponse) jceStruct2;
        CommentDetail a2 = commentAppResponse.a();
        a2.i = ((CommentAppRequest) jceStruct).i;
        a2.l = true;
        XLog.d("comment", "CommentAppEngine.onRequestSuccessed, CommentAppResponse=" + commentAppResponse.toString());
        notifyDataChangedInMainThread(new h(this, i, commentAppResponse, a2));
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        XLog.d("comment", "CommentAppEngine.onRequestFailed, errorCode=" + i2);
        notifyDataChangedInMainThread(new i(this, i, i2));
    }
}
