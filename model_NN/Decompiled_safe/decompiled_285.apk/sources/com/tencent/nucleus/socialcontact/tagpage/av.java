package com.tencent.nucleus.socialcontact.tagpage;

import android.view.animation.Animation;
import android.widget.RelativeLayout;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
class av implements Animation.AnimationListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ as f3193a;

    av(as asVar) {
        this.f3193a = asVar;
    }

    public void onAnimationStart(Animation animation) {
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationEnd(Animation animation) {
        XLog.i("VideoFullScreenManager", "[onAnimationEnd] ---> removeAllViews");
        this.f3193a.f3190a.removeAllViews();
        RelativeLayout unused = this.f3193a.f3190a = (RelativeLayout) null;
    }
}
