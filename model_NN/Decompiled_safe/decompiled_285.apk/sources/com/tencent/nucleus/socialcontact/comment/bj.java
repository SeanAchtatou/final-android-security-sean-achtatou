package com.tencent.nucleus.socialcontact.comment;

import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class bj extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PopViewDialog f3116a;

    bj(PopViewDialog popViewDialog) {
        this.f3116a = popViewDialog;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.socialcontact.comment.PopViewDialog.a(com.tencent.nucleus.socialcontact.comment.PopViewDialog, boolean):boolean
     arg types: [com.tencent.nucleus.socialcontact.comment.PopViewDialog, int]
     candidates:
      com.tencent.nucleus.socialcontact.comment.PopViewDialog.a(int, java.lang.String):void
      com.tencent.nucleus.socialcontact.comment.PopViewDialog.a(com.tencent.nucleus.socialcontact.comment.PopViewDialog, boolean):boolean */
    public void onTMAClick(View view) {
        this.f3116a.u.setSelected(!this.f3116a.u.isSelected());
        if (this.f3116a.u.isSelected()) {
            this.f3116a.w.setSelected(false);
            boolean unused = this.f3116a.A = true;
            boolean unused2 = this.f3116a.B = false;
            if (!this.f3116a.z.k()) {
                this.f3116a.g.setText((int) R.string.comment_share_qzonetips);
                this.f3116a.g.setVisibility(0);
                return;
            }
            return;
        }
        boolean unused3 = this.f3116a.A = false;
        this.f3116a.g.setVisibility(4);
    }

    public STInfoV2 getStInfo() {
        STInfoV2 c = this.f3116a.i();
        if (c != null) {
            c.slotId = a.a(this.f3116a.h(), "005");
            if (!this.f3116a.u.isSelected()) {
                c.status = "01";
            } else {
                c.status = "02";
            }
            c.actionId = 200;
        }
        return c;
    }
}
