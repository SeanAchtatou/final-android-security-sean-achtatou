package com.tencent.nucleus.socialcontact.tagpage;

import android.view.View;
import android.widget.Toast;
import com.tencent.assistant.net.c;

/* compiled from: ProGuard */
class z implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ai f3219a;
    final /* synthetic */ int b;
    final /* synthetic */ TagPageCardAdapter c;

    z(TagPageCardAdapter tagPageCardAdapter, ai aiVar, int i) {
        this.c = tagPageCardAdapter;
        this.f3219a = aiVar;
        this.b = i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.socialcontact.tagpage.TagPageCardAdapter.a(com.tencent.nucleus.socialcontact.tagpage.TagPageCardAdapter, boolean):boolean
     arg types: [com.tencent.nucleus.socialcontact.tagpage.TagPageCardAdapter, int]
     candidates:
      com.tencent.nucleus.socialcontact.tagpage.TagPageCardAdapter.a(com.tencent.nucleus.socialcontact.tagpage.TagPageCardAdapter, android.graphics.SurfaceTexture):android.graphics.SurfaceTexture
      com.tencent.nucleus.socialcontact.tagpage.TagPageCardAdapter.a(com.tencent.nucleus.socialcontact.tagpage.TagPageCardAdapter, android.media.MediaPlayer):android.media.MediaPlayer
      com.tencent.nucleus.socialcontact.tagpage.TagPageCardAdapter.a(com.tencent.nucleus.socialcontact.tagpage.TagPageCardAdapter, android.view.SurfaceHolder):android.view.SurfaceHolder
      com.tencent.nucleus.socialcontact.tagpage.TagPageCardAdapter.a(com.tencent.assistant.model.SimpleAppModel, int):com.tencent.assistantv2.st.page.STInfoV2
      com.tencent.nucleus.socialcontact.tagpage.TagPageCardAdapter.a(com.tencent.nucleus.socialcontact.tagpage.TagPageCardAdapter, int):java.lang.String
      com.tencent.nucleus.socialcontact.tagpage.TagPageCardAdapter.a(com.tencent.nucleus.socialcontact.tagpage.TagPageCardAdapter, java.lang.String):java.lang.String
      com.tencent.nucleus.socialcontact.tagpage.TagPageCardAdapter.a(com.tencent.nucleus.socialcontact.tagpage.ai, java.lang.String):void
      com.tencent.nucleus.socialcontact.tagpage.TagPageCardAdapter.a(com.tencent.nucleus.socialcontact.tagpage.ai, int):void
      com.tencent.nucleus.socialcontact.tagpage.TagPageCardAdapter.a(com.tencent.nucleus.socialcontact.tagpage.TagPageCardAdapter, boolean):boolean */
    public void onClick(View view) {
        if (c.a()) {
            boolean unused = this.c.u = true;
            this.c.a(this.f3219a, this.b);
            TagPageActivity.n = this.b;
            return;
        }
        Toast.makeText(this.c.b, "请检查网络连接", 0).show();
    }
}
