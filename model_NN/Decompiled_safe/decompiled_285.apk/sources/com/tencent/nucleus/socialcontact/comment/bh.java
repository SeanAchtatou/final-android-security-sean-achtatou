package com.tencent.nucleus.socialcontact.comment;

import android.view.View;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class bh implements View.OnFocusChangeListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PopViewDialog f3114a;

    bh(PopViewDialog popViewDialog) {
        this.f3114a = popViewDialog;
    }

    public void onFocusChange(View view, boolean z) {
        STInfoV2 c = this.f3114a.i();
        if (c != null && z) {
            c.slotId = a.a(this.f3114a.h(), "002");
            c.actionId = 200;
            l.a(c);
        }
    }
}
