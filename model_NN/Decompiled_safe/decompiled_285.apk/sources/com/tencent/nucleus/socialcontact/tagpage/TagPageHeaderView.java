package com.tencent.nucleus.socialcontact.tagpage;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewSwitcher;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.protocol.jce.AppTagInfo;
import com.tencent.assistant.utils.by;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class TagPageHeaderView extends LinearLayout implements View.OnClickListener, ViewSwitcher.ViewFactory {

    /* renamed from: a  reason: collision with root package name */
    private Context f3174a = null;
    private LayoutInflater b = null;
    private RelativeLayout c = null;
    private TextView d = null;
    private TextView e = null;
    private TextView f = null;
    private TextView g = null;
    private TextView h = null;
    private View.OnClickListener i = null;
    private RelativeLayout j = null;
    private RelativeLayout k = null;
    private ImageSwitcher l = null;
    private ArrayList<TextView> m = new ArrayList<>(3);
    private String n = null;

    public TagPageHeaderView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f3174a = context;
        a();
    }

    public TagPageHeaderView(Context context, View.OnClickListener onClickListener, String str) {
        super(context);
        this.f3174a = context;
        this.i = onClickListener;
        this.n = str;
        a();
    }

    private void a() {
        this.b = (LayoutInflater) this.f3174a.getSystemService("layout_inflater");
        View inflate = this.b.inflate((int) R.layout.tag_header_view, this);
        this.c = (RelativeLayout) inflate.findViewById(R.id.tag_header_view_layout);
        this.d = (TextView) inflate.findViewById(R.id.tv_tag_name);
        this.e = (TextView) inflate.findViewById(R.id.tv_tag_subhead);
        this.f = (TextView) inflate.findViewById(R.id.tv_tag_left);
        this.f.setOnClickListener(this);
        this.g = (TextView) inflate.findViewById(R.id.tv_tag_middle);
        this.g.setOnClickListener(this);
        this.h = (TextView) inflate.findViewById(R.id.tv_tag_right);
        this.h.setOnClickListener(this);
        this.j = (RelativeLayout) inflate.findViewById(R.id.rl_separator_layout);
        this.k = (RelativeLayout) inflate.findViewById(R.id.rl_tag_layout);
        this.l = (ImageSwitcher) inflate.findViewById(R.id.iv_switcher);
        this.l.setFactory(this);
        this.l.setInAnimation(AnimationUtils.loadAnimation(this.f3174a, R.anim.tag_page_alpha_in));
        this.l.setOutAnimation(AnimationUtils.loadAnimation(this.f3174a, R.anim.tag_page_alpha_out));
        this.m.add(this.f);
        this.m.add(this.g);
        this.m.add(this.h);
    }

    public void onClick(View view) {
        if (this.i != null) {
            this.i.onClick(view);
        }
    }

    public void a(String str) {
        if (this.d != null) {
            this.d.setText(str);
        }
    }

    public void b(String str) {
        if (this.e != null) {
            this.e.setText(str);
        }
    }

    public void a(ArrayList<AppTagInfo> arrayList) {
        if (arrayList == null || arrayList.size() <= 0) {
            a(false);
            return;
        }
        a(true);
        int size = arrayList.size();
        if (this.m != null) {
            int min = Math.min(this.m.size(), size);
            for (int i2 = 0; i2 < min; i2++) {
                if (arrayList.get(i2) != null) {
                    this.m.get(i2).setText(arrayList.get(i2).b);
                }
            }
        }
        a(size);
    }

    private void a(boolean z) {
        int i2 = 0;
        if (this.j != null) {
            this.j.setVisibility(z ? 0 : 8);
        }
        if (this.k != null) {
            RelativeLayout relativeLayout = this.k;
            if (!z) {
                i2 = 8;
            }
            relativeLayout.setVisibility(i2);
        }
    }

    private void a(int i2) {
        if (i2 >= 3) {
            this.f.setVisibility(0);
            this.g.setVisibility(0);
            this.h.setVisibility(0);
        } else if (2 == i2) {
            this.f.setVisibility(0);
            this.g.setVisibility(0);
            this.h.setVisibility(8);
        } else if (1 == i2) {
            this.f.setVisibility(0);
            this.g.setVisibility(8);
            this.h.setVisibility(8);
        } else {
            this.f.setVisibility(8);
            this.g.setVisibility(8);
            this.h.setVisibility(8);
        }
    }

    public void a(Bitmap bitmap) {
        if (this.l != null && bitmap != null) {
            this.l.setImageDrawable(new BitmapDrawable(bitmap));
        }
    }

    public View makeView() {
        ImageView imageView = new ImageView(this.f3174a);
        imageView.setBackgroundColor(0);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setLayoutParams(new FrameLayout.LayoutParams(-1, by.a(this.f3174a, 235.0f)));
        return imageView;
    }
}
