package com.tencent.nucleus.socialcontact.comment;

import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistant.protocol.jce.ModifyAppCommentRequest;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
class be implements CallbackHelper.Caller<j> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f3111a;
    final /* synthetic */ int b;
    final /* synthetic */ ModifyAppCommentRequest c;
    final /* synthetic */ bc d;

    be(bc bcVar, int i, int i2, ModifyAppCommentRequest modifyAppCommentRequest) {
        this.d = bcVar;
        this.f3111a = i;
        this.b = i2;
        this.c = modifyAppCommentRequest;
    }

    /* renamed from: a */
    public void call(j jVar) {
        jVar.a(this.f3111a, this.b, this.c.f1410a, Constants.STR_EMPTY, -1, -1);
    }
}
