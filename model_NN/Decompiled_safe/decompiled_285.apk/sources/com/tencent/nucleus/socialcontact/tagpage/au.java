package com.tencent.nucleus.socialcontact.tagpage;

import android.media.MediaPlayer;
import android.view.animation.Animation;

/* compiled from: ProGuard */
class au implements Animation.AnimationListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ MediaPlayer f3192a;
    final /* synthetic */ as b;

    au(as asVar, MediaPlayer mediaPlayer) {
        this.b = asVar;
        this.f3192a = mediaPlayer;
    }

    public void onAnimationStart(Animation animation) {
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationEnd(Animation animation) {
        if (this.b.h != null) {
            this.b.h.setVisibility(8);
        }
        try {
            if (this.f3192a != null && !this.f3192a.isPlaying()) {
                this.f3192a.start();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
