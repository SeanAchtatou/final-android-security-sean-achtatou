package com.tencent.beacon.event;

import android.content.Context;
import com.tencent.beacon.a.a;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/* compiled from: ProGuard */
public final class h implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private Context f2137a;
    private int b;
    private int c = -1;
    private Map<String, String> d = new HashMap(2);
    private int e = 0;

    public h(Context context, int i) {
        this.f2137a = context;
        this.b = i;
        this.e = Calendar.getInstance().get(5);
        this.d.put("A63", "N");
        this.d.put("A66", "F");
    }

    public final void run() {
        if (this.b > 0) {
            boolean g = a.g(this.f2137a);
            int i = Calendar.getInstance().get(5);
            if (i != this.e) {
                this.e = i;
                new j(this.f2137a, g).a(true);
            }
            if (this.c != -1) {
                if (this.c == 0 && g) {
                    t.a("rqd_applaunched", true, 0, 0, this.d, true);
                    com.tencent.beacon.d.a.a(" create a applaunched event", new Object[0]);
                    this.c = 1;
                    t.b(true);
                    return;
                } else if (g || this.c != 1) {
                    return;
                }
            } else if (g) {
                this.c = 1;
                return;
            }
            this.c = 0;
        }
    }
}
