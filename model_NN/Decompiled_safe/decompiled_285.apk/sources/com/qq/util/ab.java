package com.qq.util;

import com.qq.provider.n;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/* compiled from: ProGuard */
public class ab {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public static boolean a(String str, String str2) {
        boolean z;
        File file;
        File file2;
        if (!new File(str).exists()) {
            return false;
        }
        File file3 = new File(str2);
        if (!file3.exists()) {
            boolean mkdirs = file3.mkdirs();
            if (!mkdirs) {
                return mkdirs;
            }
        } else if (!file3.isDirectory()) {
            return false;
        }
        try {
            FileInputStream fileInputStream = new FileInputStream(str);
            ZipInputStream zipInputStream = new ZipInputStream(fileInputStream);
            byte[] bArr = new byte[8192];
            while (true) {
                try {
                    ZipEntry nextEntry = zipInputStream.getNextEntry();
                    if (nextEntry != null) {
                        String name = nextEntry.getName();
                        if (name != null) {
                            String replace = name.replace('\\', '/');
                            if (!replace.startsWith(File.separator)) {
                                z = true;
                            } else {
                                z = false;
                            }
                            if (nextEntry.isDirectory()) {
                                if (z) {
                                    file2 = new File(str2 + File.separator + replace);
                                } else {
                                    file2 = new File(str2 + replace);
                                }
                                if (!file2.exists()) {
                                    file2.mkdirs();
                                }
                            } else {
                                if (z) {
                                    file = new File(str2 + File.separator + replace);
                                } else {
                                    file = new File(str2 + replace);
                                }
                                File parentFile = file.getParentFile();
                                if (!parentFile.exists()) {
                                    parentFile.mkdirs();
                                }
                                if (!file.exists()) {
                                    file.createNewFile();
                                }
                                n.a(zipInputStream, file, bArr);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    break;
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
            }
            zipInputStream.close();
            try {
                fileInputStream.close();
            } catch (IOException e3) {
                e3.printStackTrace();
            }
            return true;
        } catch (FileNotFoundException e4) {
            e4.printStackTrace();
            return false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public static boolean a(String str, String str2, String str3) {
        String name;
        boolean z;
        File file;
        File file2;
        if (!new File(str).exists()) {
            return false;
        }
        File file3 = new File(str2);
        if (!file3.exists()) {
            boolean mkdirs = file3.mkdirs();
            if (!mkdirs) {
                return mkdirs;
            }
        } else if (!file3.isDirectory()) {
            return false;
        }
        try {
            FileInputStream fileInputStream = new FileInputStream(str);
            ZipInputStream zipInputStream = new ZipInputStream(fileInputStream);
            byte[] bArr = new byte[8192];
            while (true) {
                try {
                    ZipEntry nextEntry = zipInputStream.getNextEntry();
                    if (nextEntry != null) {
                        if (nextEntry.getName().startsWith(str3) && (name = nextEntry.getName()) != null) {
                            String replace = name.replace('\\', '/');
                            if (!replace.startsWith(File.separator)) {
                                z = true;
                            } else {
                                z = false;
                            }
                            if (nextEntry.isDirectory()) {
                                if (z) {
                                    file2 = new File(str2 + File.separator + replace);
                                } else {
                                    file2 = new File(str2 + replace);
                                }
                                if (!file2.exists()) {
                                    file2.mkdirs();
                                }
                            } else {
                                if (z) {
                                    file = new File(str2 + File.separator + replace);
                                } else {
                                    file = new File(str2 + replace);
                                }
                                File parentFile = file.getParentFile();
                                if (!parentFile.exists()) {
                                    parentFile.mkdirs();
                                }
                                if (!file.exists()) {
                                    file.createNewFile();
                                }
                                n.a(zipInputStream, file, bArr);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    break;
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
            }
            zipInputStream.close();
            try {
                fileInputStream.close();
            } catch (IOException e3) {
                e3.printStackTrace();
            }
            return true;
        } catch (FileNotFoundException e4) {
            e4.printStackTrace();
            return false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public static boolean a(String str, String str2, ArrayList<String> arrayList) {
        int i;
        String name;
        boolean z;
        File file;
        File file2;
        if (arrayList == null || arrayList.size() <= 0) {
            return false;
        }
        if (!new File(str).exists()) {
            return false;
        }
        File file3 = new File(str2);
        if (!file3.exists()) {
            boolean mkdirs = file3.mkdirs();
            if (!mkdirs) {
                return mkdirs;
            }
        } else if (!file3.isDirectory()) {
            return false;
        }
        try {
            FileInputStream fileInputStream = new FileInputStream(str);
            ZipInputStream zipInputStream = new ZipInputStream(fileInputStream);
            byte[] bArr = new byte[8192];
            int size = arrayList.size();
            while (true) {
                try {
                    ZipEntry nextEntry = zipInputStream.getNextEntry();
                    if (nextEntry != null) {
                        String name2 = nextEntry.getName();
                        int i2 = 0;
                        while (true) {
                            if (i2 < size) {
                                String str3 = arrayList.get(i2);
                                if (str3 != null && name2.startsWith(str3)) {
                                    i = i2;
                                    break;
                                }
                                i2++;
                            } else {
                                i = -1;
                                break;
                            }
                        }
                        if (!(i == -1 || (name = nextEntry.getName()) == null)) {
                            String replace = name.replace('\\', '/');
                            if (!replace.startsWith(File.separator)) {
                                z = true;
                            } else {
                                z = false;
                            }
                            if (nextEntry.isDirectory()) {
                                if (z) {
                                    file2 = new File(str2 + File.separator + replace);
                                } else {
                                    file2 = new File(str2 + replace);
                                }
                                if (!file2.exists()) {
                                    file2.mkdirs();
                                }
                            } else {
                                if (z) {
                                    file = new File(str2 + File.separator + replace);
                                } else {
                                    file = new File(str2 + replace);
                                }
                                File parentFile = file.getParentFile();
                                if (!parentFile.exists()) {
                                    parentFile.mkdirs();
                                }
                                if (!file.exists()) {
                                    file.createNewFile();
                                }
                                n.a(zipInputStream, file, bArr);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    break;
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
            }
            zipInputStream.close();
            try {
                fileInputStream.close();
            } catch (IOException e3) {
                e3.printStackTrace();
            }
            return true;
        } catch (FileNotFoundException e4) {
            e4.printStackTrace();
            return false;
        }
    }
}
