package com.qq.util;

import android.net.Uri;
import android.text.TextUtils;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* compiled from: ProGuard */
public final class v implements u {

    /* renamed from: a  reason: collision with root package name */
    public static final Uri f374a = Uri.parse("content://mms");
    public static final Uri b = Uri.withAppendedPath(f374a, "report-request");
    public static final Uri c = Uri.withAppendedPath(f374a, "report-status");
    public static final Pattern d = Pattern.compile("\\s*(\"[^\"]*\"|[^<>\"]+)\\s*<([^<>]+)>\\s*");
    public static final Pattern e = Pattern.compile("\\s*\"([^\"]*)\"\\s*");

    public static String a(String str) {
        Matcher matcher = d.matcher(str);
        if (matcher.matches()) {
            return matcher.group(2);
        }
        return str;
    }

    public static boolean b(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        return o.e.matcher(a(str)).matches();
    }
}
