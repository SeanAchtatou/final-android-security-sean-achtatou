package com.qq.provider;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CallLog;
import com.qq.AppService.r;
import com.qq.d.a.a;
import com.qq.g.c;
import com.tencent.open.SocialConstants;
import java.util.ArrayList;
import java.util.Date;

/* compiled from: ProGuard */
public class f {

    /* renamed from: a  reason: collision with root package name */
    private static f f339a;

    public static f a() {
        if (f339a != null) {
            return f339a;
        }
        f339a = new f();
        return f339a;
    }

    public void a(Context context, c cVar) {
        ArrayList arrayList = new ArrayList();
        if (cVar.e() < 8) {
            cVar.a(1);
            return;
        }
        cVar.g();
        int h = cVar.h();
        int h2 = cVar.h();
        String j = cVar.j();
        int h3 = cVar.h();
        String j2 = cVar.j();
        String j3 = cVar.j();
        String j4 = cVar.j();
        if (r.b(j2)) {
            cVar.a(7);
            return;
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put(SocialConstants.PARAM_TYPE, Integer.valueOf(h));
        if (h2 < 0) {
            h2 = 0;
        }
        contentValues.put("duration", Integer.valueOf(h2));
        if (!r.b(j)) {
            contentValues.put("date", Long.valueOf(r.c(j).getTime()));
        }
        if (r.b(j)) {
            contentValues.put("date", Long.valueOf(new Date().getTime()));
        }
        if (h3 >= 0 && h3 < 7) {
            contentValues.put("numbertype", Integer.valueOf(h3));
        }
        if (!r.b(j2)) {
            contentValues.put("number", j2);
        }
        if (!r.b(j3)) {
            contentValues.put("name", j3);
        }
        if (h3 == 0 && !r.b(j4)) {
            contentValues.put("numberlabel", j4);
        }
        if (context.getContentResolver().insert(CallLog.Calls.CONTENT_URI, contentValues) == null) {
            cVar.a(8);
            return;
        }
        cVar.a(0);
        cVar.a(arrayList);
    }

    public void b(Context context, c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        Cursor query = context.getContentResolver().query(CallLog.Calls.CONTENT_URI, null, "_id=" + h, null, null);
        if (query == null || !query.moveToFirst()) {
            if (query != null) {
                query.close();
            }
            cVar.a(1);
            return;
        }
        a aVar = new a();
        aVar.f269a = h;
        aVar.b = query.getInt(query.getColumnIndex(SocialConstants.PARAM_TYPE));
        aVar.c = query.getInt(query.getColumnIndex("duration"));
        aVar.d = r.b(Long.valueOf(query.getLong(query.getColumnIndex("date"))).longValue());
        aVar.e = query.getInt(query.getColumnIndex("numbertype"));
        aVar.f = query.getString(query.getColumnIndex("number"));
        aVar.g = query.getString(query.getColumnIndex("name"));
        aVar.h = query.getString(query.getColumnIndex("numberlabel"));
        if (query != null) {
            query.close();
        }
        cVar.a(aVar.a());
        cVar.a(0);
    }

    public void c(Context context, c cVar) {
        boolean z;
        if (cVar.e() < 2) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        int h2 = cVar.h();
        if (h2 <= 0) {
            cVar.a(1);
            return;
        }
        Cursor query = context.getContentResolver().query(CallLog.Calls.CONTENT_URI, null, "_id > " + h + " and " + SocialConstants.PARAM_TYPE + " >= " + "0 and " + SocialConstants.PARAM_TYPE + " <= " + 3, null, "_id");
        if (query == null) {
            cVar.a(8);
            return;
        }
        int count = query.getCount();
        if (count > h2) {
            count = h2;
        }
        boolean moveToFirst = query.moveToFirst();
        ArrayList arrayList = new ArrayList();
        arrayList.add(r.a(count));
        int i = 0;
        boolean z2 = false;
        while (i < count && moveToFirst) {
            if (i == 0) {
                int i2 = query.getInt(query.getColumnIndex("_id"));
                query.moveToLast();
                if (query.getInt(query.getColumnIndex("_id")) < i2) {
                    z2 = true;
                } else {
                    query.moveToFirst();
                }
            }
            arrayList.add(r.a(query.getInt(query.getColumnIndex("_id"))));
            arrayList.add(r.a(query.getInt(query.getColumnIndex(SocialConstants.PARAM_TYPE))));
            arrayList.add(r.a(query.getInt(query.getColumnIndex("duration"))));
            arrayList.add(r.a((long) query.getInt(query.getColumnIndex("date"))));
            arrayList.add(r.a(query.getInt(query.getColumnIndex("numbertype"))));
            byte[] blob = query.getBlob(query.getColumnIndex("number"));
            if (blob == null) {
                blob = r.hr;
            }
            arrayList.add(blob);
            byte[] blob2 = query.getBlob(query.getColumnIndex("name"));
            if (blob2 == null) {
                blob2 = r.hr;
            }
            arrayList.add(blob2);
            byte[] blob3 = query.getBlob(query.getColumnIndex("numberlabel"));
            if (blob3 == null) {
                blob3 = r.hr;
            }
            arrayList.add(blob3);
            if (z2) {
                z = query.moveToNext();
            } else {
                query.moveToNext();
                z = moveToFirst;
            }
            i++;
            moveToFirst = z;
        }
        query.close();
        cVar.a(arrayList);
        cVar.a(0);
    }

    public void d(Context context, c cVar) {
        boolean z;
        boolean moveToFirst;
        boolean z2;
        boolean z3;
        if (cVar.e() < 2) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        int h2 = cVar.h();
        if (h2 <= 0) {
            cVar.a(1);
            return;
        }
        if (h <= 0) {
            Cursor query = context.getContentResolver().query(CallLog.Calls.CONTENT_URI, new String[]{"_id"}, null, null, "_id ASC");
            if (query == null) {
                cVar.a(7);
                return;
            } else if (!query.moveToLast()) {
                query.close();
                ArrayList arrayList = new ArrayList();
                arrayList.add(r.a(0));
                cVar.a(arrayList);
                cVar.a(0);
                return;
            } else {
                int i = query.getInt(query.getColumnIndex("_id"));
                query.moveToFirst();
                int i2 = query.getInt(query.getColumnIndex("_id"));
                if (i2 > i) {
                    z3 = true;
                } else {
                    i2 = i;
                    z3 = false;
                }
                query.close();
                h = i2 + 1;
                z = z3;
            }
        } else {
            z = false;
        }
        int i3 = (h - h2) - 500;
        if (i3 < 0) {
            i3 = 0;
        }
        ArrayList arrayList2 = new ArrayList();
        arrayList2.add(r.a(h2));
        int i4 = 0;
        int i5 = h;
        int i6 = i3;
        while (i4 < h2 && i5 > 0) {
            Cursor query2 = context.getContentResolver().query(CallLog.Calls.CONTENT_URI, null, "_id < " + i5 + " and " + "_id" + " >= " + i6 + " and " + SocialConstants.PARAM_TYPE + " >= " + "0 and " + SocialConstants.PARAM_TYPE + " <= " + 3, null, "_id ASC");
            if (query2 == null) {
                cVar.a(8);
                return;
            }
            int columnIndex = query2.getColumnIndex("messageid");
            int count = query2.getCount();
            if (!z) {
                moveToFirst = query2.moveToLast();
            } else {
                moveToFirst = query2.moveToFirst();
            }
            int i7 = i4;
            for (int i8 = 0; i8 < count && z2; i8++) {
                if (columnIndex < 0 || query2.getInt(columnIndex) <= 0) {
                    arrayList2.add(r.a(query2.getInt(query2.getColumnIndex("_id"))));
                    arrayList2.add(r.a(query2.getInt(query2.getColumnIndex(SocialConstants.PARAM_TYPE))));
                    arrayList2.add(r.a(query2.getInt(query2.getColumnIndex("duration"))));
                    arrayList2.add(r.a(query2.getLong(query2.getColumnIndex("date"))));
                    arrayList2.add(r.a(query2.getInt(query2.getColumnIndex("numbertype"))));
                    byte[] blob = query2.getBlob(query2.getColumnIndex("number"));
                    if (blob == null) {
                        blob = r.hr;
                    }
                    arrayList2.add(blob);
                    byte[] blob2 = query2.getBlob(query2.getColumnIndex("name"));
                    if (blob2 == null) {
                        blob2 = r.hr;
                    }
                    arrayList2.add(blob2);
                    byte[] blob3 = query2.getBlob(query2.getColumnIndex("numberlabel"));
                    if (blob3 == null) {
                        blob3 = r.hr;
                    }
                    arrayList2.add(blob3);
                    i7++;
                    if (!z) {
                        z2 = query2.moveToPrevious();
                    } else {
                        z2 = query2.moveToNext();
                    }
                } else if (!z) {
                    z2 = query2.moveToPrevious();
                } else {
                    z2 = query2.moveToNext();
                }
            }
            query2.close();
            int i9 = (i6 - h2) - 500;
            if (i9 < 0) {
                i9 = 0;
            }
            i4 = i7;
            i5 = i6;
            i6 = i9;
        }
        arrayList2.set(0, r.a(i4));
        cVar.a(arrayList2);
        cVar.a(0);
    }

    public void e(Context context, c cVar) {
        ArrayList arrayList = new ArrayList();
        Cursor query = context.getContentResolver().query(CallLog.Calls.CONTENT_URI, null, null, null, null);
        if (query == null) {
            cVar.a(7);
            return;
        }
        int count = query.getCount();
        arrayList.add(r.a(count));
        boolean moveToFirst = query.moveToFirst();
        for (int i = 0; i < count && moveToFirst; i++) {
            int i2 = query.getInt(query.getColumnIndex("_id"));
            int i3 = query.getInt(query.getColumnIndex(SocialConstants.PARAM_TYPE));
            int i4 = query.getInt(query.getColumnIndex("duration"));
            String b = r.b(Long.valueOf(query.getLong(query.getColumnIndex("date"))).longValue());
            int i5 = query.getInt(query.getColumnIndex("numbertype"));
            String string = query.getString(query.getColumnIndex("number"));
            String string2 = query.getString(query.getColumnIndex("name"));
            String string3 = query.getString(query.getColumnIndex("numberlabel"));
            arrayList.add(r.a(i2));
            arrayList.add(r.a(i3));
            arrayList.add(r.a(i4));
            arrayList.add(r.a(b));
            arrayList.add(r.a(i5));
            arrayList.add(r.a(string));
            arrayList.add(r.a(string2));
            arrayList.add(r.a(string3));
            moveToFirst = query.moveToNext();
        }
        query.close();
        cVar.a(arrayList);
        cVar.a(0);
    }

    public void f(Context context, c cVar) {
        ArrayList arrayList = new ArrayList();
        Cursor query = context.getContentResolver().query(CallLog.Calls.CONTENT_URI, null, null, null, null);
        if (query == null) {
            cVar.a(7);
            return;
        }
        arrayList.add(r.a(query.getCount()));
        query.close();
        cVar.a(arrayList);
        cVar.a(0);
    }

    public void g(Context context, c cVar) {
        ArrayList arrayList = new ArrayList();
        Cursor query = context.getContentResolver().query(CallLog.Calls.CONTENT_URI, null, null, null, null);
        if (query == null) {
            cVar.a(7);
            return;
        }
        int count = query.getCount();
        arrayList.add(r.a(count));
        boolean moveToFirst = query.moveToFirst();
        for (int i = 0; i < count && moveToFirst; i++) {
            arrayList.add(r.a(query.getInt(query.getColumnIndex("_id"))));
            moveToFirst = query.moveToNext();
        }
        query.close();
        cVar.a(arrayList);
        cVar.a(0);
    }

    public void h(Context context, c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        if (context.getContentResolver().delete(CallLog.Calls.CONTENT_URI, "_id=" + h, null) != 1) {
            if (context.getContentResolver().delete(Uri.withAppendedPath(CallLog.Calls.CONTENT_URI, String.valueOf(h)), null, null) < 0) {
                cVar.a(1);
                return;
            }
            return;
        }
        cVar.a(0);
    }

    public void i(Context context, c cVar) {
        context.getContentResolver().delete(CallLog.Calls.CONTENT_URI, null, null);
    }
}
