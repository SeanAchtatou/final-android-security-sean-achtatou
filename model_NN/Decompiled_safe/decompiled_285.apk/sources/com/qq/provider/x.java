package com.qq.provider;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import com.qq.AppService.SmsManagerActivity;
import com.qq.AppService.r;
import com.qq.a.a.d;
import com.qq.g.c;
import com.qq.util.j;
import com.tencent.open.SocialConstants;
import java.util.ArrayList;
import java.util.Date;

/* compiled from: ProGuard */
public class x {
    private static volatile x b;

    /* renamed from: a  reason: collision with root package name */
    public Context f355a = null;

    public x(Context context) {
        this.f355a = context.getApplicationContext();
    }

    public static x a(Context context) {
        if (b == null) {
            b = new x(context);
        }
        return b;
    }

    public static Uri a(int i) {
        if (i < 0 || i > 6) {
            return null;
        }
        switch (i) {
            case 0:
                return d.f258a;
            case 1:
                return d.b;
            case 2:
                return d.c;
            case 3:
                return d.d;
            case 4:
                return d.e;
            case 5:
                return d.f;
            case 6:
                return d.g;
            default:
                return d.b;
        }
    }

    public void a(Context context, c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        if (h <= 0) {
            cVar.a(1);
        } else if (cVar.e() < h + 1) {
            cVar.a(1);
        } else {
            for (int i = 0; i < h; i++) {
                int h2 = cVar.h();
                if (h2 > 0) {
                    context.getContentResolver().delete(d.f258a, "_id = " + h2, null);
                }
            }
            cVar.a(0);
        }
    }

    public void a(c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        if (h == 0) {
            this.f355a.getContentResolver().delete(d.f258a, null, null);
        } else {
            this.f355a.getContentResolver().delete(d.f258a, "type = " + h, null);
        }
        cVar.a(0);
    }

    public void b(Context context, c cVar) {
        context.getContentResolver().delete(d.f258a, "type = 1 and read = 0", null);
        cVar.a(0);
    }

    public void b(c cVar) {
        int i;
        if (cVar.e() < 10) {
            cVar.a(1);
            return;
        }
        cVar.g();
        cVar.g();
        String j = cVar.j();
        cVar.g();
        String j2 = cVar.j();
        int h = cVar.h();
        int h2 = cVar.h();
        int h3 = cVar.h();
        cVar.g();
        String j3 = cVar.j();
        ContentValues contentValues = new ContentValues();
        if (!r.b(j)) {
            contentValues.put("address", j);
        } else {
            contentValues.putNull("address");
        }
        contentValues.put("thread_id", Integer.valueOf((int) com.qq.util.x.a(this.f355a, j)));
        if (!r.b(j2)) {
            contentValues.put("date", Long.valueOf(r.c(j2).getTime()));
        } else {
            contentValues.put("date", Long.valueOf(new Date().getTime()));
        }
        contentValues.put("read", Integer.valueOf(h));
        contentValues.put("status", Integer.valueOf(h2));
        contentValues.put(SocialConstants.PARAM_TYPE, Integer.valueOf(h3));
        if (!r.b(j3)) {
            contentValues.put("body", j3);
        } else {
            contentValues.putNull("body");
        }
        Uri insert = this.f355a.getContentResolver().insert(d.f258a, contentValues);
        if (insert == null) {
            cVar.a(8);
            return;
        }
        try {
            i = Integer.parseInt(insert.getLastPathSegment());
        } catch (Exception e) {
            e.printStackTrace();
            i = 0;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(r.a(i));
        cVar.a(0);
        cVar.a(arrayList);
    }

    public void c(Context context, c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        Cursor query = context.getContentResolver().query(Uri.parse("content://sms"), new String[]{"canonical_addresses.address from sms,threads,canonical_addresses where sms.thread_id=threads._id and threads.recipient_ids=canonical_addresses._id and sms._id ='" + String.valueOf(h) + "' --"}, null, null, null);
        if (query == null) {
            cVar.a(8);
        } else if (query.moveToFirst()) {
            String string = query.getString(0);
            ArrayList arrayList = new ArrayList();
            arrayList.add(r.a(string));
            cVar.a(arrayList);
            cVar.a(0);
            query.close();
        } else {
            query.close();
            cVar.a(7);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0059, code lost:
        if (com.qq.provider.p.b(r8, r1) == false) goto L_0x005b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void d(android.content.Context r8, com.qq.g.c r9) {
        /*
            r7 = this;
            r0 = 0
            r6 = 0
            int r1 = com.qq.util.j.c
            r2 = 19
            if (r1 >= r2) goto L_0x000d
            r0 = 4
            r9.a(r0)
        L_0x000c:
            return
        L_0x000d:
            java.lang.String r1 = b(r8)
            if (r1 != 0) goto L_0x0019
            r0 = 8
            r9.a(r0)
            goto L_0x000c
        L_0x0019:
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            java.lang.String r3 = r8.getPackageName()
            boolean r1 = r1.equals(r3)
            if (r1 != 0) goto L_0x0036
            byte[] r0 = com.qq.AppService.r.a(r6)
            r2.add(r0)
            r9.a(r6)
            r9.a(r2)
            goto L_0x000c
        L_0x0036:
            java.lang.Class<com.qq.provider.x> r3 = com.qq.provider.x.class
            monitor-enter(r3)
            java.lang.String r1 = "com.qq.connect"
            r4 = 0
            android.content.SharedPreferences r1 = r8.getSharedPreferences(r1, r4)     // Catch:{ all -> 0x00b6 }
            java.lang.String r4 = "DefaultSmsPackage"
            r5 = 0
            java.lang.String r1 = r1.getString(r4, r5)     // Catch:{ all -> 0x00b6 }
            monitor-exit(r3)     // Catch:{ all -> 0x00b6 }
            if (r1 == 0) goto L_0x0051
            boolean r3 = com.qq.provider.p.b(r8, r1)
            if (r3 != 0) goto L_0x0051
            r1 = r0
        L_0x0051:
            if (r1 != 0) goto L_0x00b9
            java.lang.String r1 = "com.android.mms"
            boolean r3 = com.qq.provider.p.b(r8, r1)
            if (r3 != 0) goto L_0x00b9
        L_0x005b:
            if (r0 != 0) goto L_0x005d
        L_0x005d:
            java.lang.String r1 = "com.qq.connect"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "rlease sms"
            java.lang.StringBuilder r3 = r3.append(r4)
            boolean r4 = com.qq.AppService.SmsManagerActivity.c
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r4 = "."
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.String r3 = r3.toString()
            android.util.Log.d(r1, r3)
            boolean r1 = com.qq.AppService.SmsManagerActivity.c
            if (r1 != 0) goto L_0x00a3
            android.content.Intent r1 = new android.content.Intent
            r1.<init>()
            java.lang.Class<com.qq.AppService.SmsManagerActivity> r3 = com.qq.AppService.SmsManagerActivity.class
            r1.setClass(r8, r3)
            r3 = 268435456(0x10000000, float:2.5243549E-29)
            r1.addFlags(r3)
            java.lang.String r3 = "act"
            java.lang.String r4 = "release"
            r1.putExtra(r3, r4)
            java.lang.String r3 = "pkg"
            r1.putExtra(r3, r0)
            r8.startActivity(r1)
        L_0x00a3:
            r0 = 1
            byte[] r0 = com.qq.AppService.r.a(r0)
            r2.add(r0)
            r9.a(r6)
            r9.a(r2)
            r9.a(r6)
            goto L_0x000c
        L_0x00b6:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x00b6 }
            throw r0
        L_0x00b9:
            r0 = r1
            goto L_0x005b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qq.provider.x.d(android.content.Context, com.qq.g.c):void");
    }

    public void e(Context context, c cVar) {
        if (j.c < 19) {
            cVar.a(4);
            return;
        }
        String b2 = b(context);
        if (b2 == null) {
            cVar.a(8);
            return;
        }
        ArrayList arrayList = new ArrayList();
        if (b2.equals(context.getPackageName())) {
            arrayList.add(r.a(0));
            cVar.a(0);
            cVar.a(arrayList);
            return;
        }
        synchronized (x.class) {
            SharedPreferences.Editor edit = context.getSharedPreferences("com.qq.connect", 0).edit();
            edit.putString("DefaultSmsPackage", b2);
            edit.commit();
        }
        if (!SmsManagerActivity.b) {
            Intent intent = new Intent();
            intent.setClass(context, SmsManagerActivity.class);
            intent.addFlags(268435456);
            intent.putExtra(SocialConstants.PARAM_ACT, SocialConstants.TYPE_REQUEST);
            context.startActivity(intent);
        }
        arrayList.add(r.a(1));
        cVar.a(0);
        cVar.a(arrayList);
        cVar.a(0);
    }

    public void f(Context context, c cVar) {
        int i = 2000;
        if (cVar.e() < 1) {
            cVar.a(1);
        } else if (j.c < 19) {
            cVar.a(4);
        } else {
            int h = cVar.h();
            if (h >= 2000) {
                i = h;
            }
            String b2 = b(context);
            if (b2 == null) {
                cVar.a(8);
                return;
            }
            ArrayList arrayList = new ArrayList();
            if (b2.equals(context.getPackageName())) {
                arrayList.add(r.a(1));
                cVar.a(0);
                cVar.a(arrayList);
                return;
            }
            synchronized (x.class) {
                SharedPreferences.Editor edit = context.getSharedPreferences("com.qq.connect", 0).edit();
                edit.putString("DefaultSmsPackage", b2);
                edit.commit();
            }
            if (!SmsManagerActivity.b) {
                Intent intent = new Intent();
                intent.setClass(context, SmsManagerActivity.class);
                intent.addFlags(268435456);
                intent.putExtra(SocialConstants.PARAM_ACT, SocialConstants.TYPE_REQUEST);
                context.startActivity(intent);
            }
            synchronized (this) {
                try {
                    wait((long) i);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            String b3 = b(context);
            if (b3 == null || !b3.equals(context.getPackageName())) {
                arrayList.add(r.a(0));
            } else {
                arrayList.add(r.a(1));
            }
            cVar.a(0);
            cVar.a(arrayList);
        }
    }

    public static String b(Context context) {
        if (j.c < 19) {
            return context.getPackageName();
        }
        try {
            Object invoke = Class.forName("android.provider.Telephony$Sms").getMethod("getDefaultSmsPackage", Context.class).invoke(null, context);
            if (invoke instanceof String) {
                return (String) invoke;
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
        return null;
    }

    public void g(Context context, c cVar) {
        if (j.c < 19) {
            ArrayList arrayList = new ArrayList();
            arrayList.add(r.a(context.getPackageName()));
            cVar.a(arrayList);
            cVar.a(0);
            return;
        }
        String b2 = b(context);
        if (b2 != null) {
            ArrayList arrayList2 = new ArrayList();
            arrayList2.add(r.a(b2));
            cVar.a(arrayList2);
            cVar.a(0);
            return;
        }
        cVar.a(8);
    }
}
