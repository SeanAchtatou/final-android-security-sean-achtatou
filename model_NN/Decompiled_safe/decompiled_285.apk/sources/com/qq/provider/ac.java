package com.qq.provider;

import android.content.Context;
import android.content.Intent;
import com.qq.AppService.OpenGLActivity;

/* compiled from: ProGuard */
class ac extends Thread {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Context f320a;
    final /* synthetic */ ab b;

    ac(ab abVar, Context context) {
        this.b = abVar;
        this.f320a = context;
    }

    public void run() {
        super.run();
        Intent intent = new Intent();
        intent.setClass(this.f320a, OpenGLActivity.class);
        intent.addFlags(268435456);
        intent.addFlags(134217728);
        this.f320a.startActivity(intent);
    }
}
