package com.qq.provider.ebooks3;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;
import com.tencent.connect.common.Constants;
import com.tencent.open.SocialConstants;
import java.io.File;

/* compiled from: ProGuard */
public class EbooksProvider extends ContentProvider {

    /* renamed from: a  reason: collision with root package name */
    public static final Uri f338a = Uri.parse("content://com.qq.provider.ebooks3.EbooksProvider/ebooks");
    public static final Uri b = Uri.parse("content://com.qq.provider.ebooks3.EbooksProvider/qx_user");
    public static final Uri c = Uri.parse("content://com.qq.provider.ebooks3.EbooksProvider/qx_friends_table");
    public static final Uri d = Uri.parse("content://com.qq.provider.ebooks3.EbooksProvider/qx_cache");
    private static final UriMatcher g = new UriMatcher(-1);
    private DBHelper e;
    private SQLiteDatabase f;

    static {
        g.addURI("com.qq.provider.ebooks3.EbooksProvider", DBHelper.CONTACTS_TABLE, 1);
        g.addURI("com.qq.provider.ebooks3.EbooksProvider", "ebooks/#", 2);
        g.addURI("com.qq.provider.ebooks3.EbooksProvider", "qx_user", 3);
        g.addURI("com.qq.provider.ebooks3.EbooksProvider", "qx_user/#", 4);
        g.addURI("com.qq.provider.ebooks3.EbooksProvider", "qx_friends_table", 5);
        g.addURI("com.qq.provider.ebooks3.EbooksProvider", "qx_friends_table/#", 6);
        g.addURI("com.qq.provider.ebooks3.EbooksProvider", "qx_cache", 7);
        g.addURI("com.qq.provider.ebooks3.EbooksProvider", "qx_cache/#", 8);
    }

    public boolean onCreate() {
        this.e = new DBHelper(getContext());
        try {
            this.f = this.e.getWritableDatabase();
        } catch (Throwable th) {
            th.printStackTrace();
        }
        if (this.f == null) {
            if (this.e != null) {
                this.e.close();
            }
            File file = new File(getContext().getApplicationInfo().dataDir + "/database/" + DBHelper.DATABASE_NAME);
            if (file.exists()) {
                file.delete();
            }
            this.e = new DBHelper(getContext());
            try {
                this.f = this.e.getWritableDatabase();
            } catch (Throwable th2) {
                th2.printStackTrace();
            }
        }
        if (this.f == null) {
            return false;
        }
        return true;
    }

    public int delete(Uri uri, String str, String[] strArr) {
        int delete;
        switch (g.match(uri)) {
            case 1:
                delete = this.f.delete(DBHelper.CONTACTS_TABLE, str, strArr);
                break;
            case 2:
                delete = this.f.delete(DBHelper.CONTACTS_TABLE, "_id=" + uri.getPathSegments().get(1) + (!TextUtils.isEmpty(str) ? " AND (" + str + ")" : Constants.STR_EMPTY), strArr);
                break;
            case 3:
                delete = this.f.delete("qx_user", str, strArr);
                break;
            case 4:
                delete = this.f.delete("qx_user", "_id=" + uri.getPathSegments().get(1) + (!TextUtils.isEmpty(str) ? " AND (" + str + ")" : Constants.STR_EMPTY), strArr);
                break;
            case 5:
                delete = this.f.delete("qx_friends_table", str, strArr);
                break;
            case 6:
                delete = this.f.delete("qx_friends_table", "_id=" + uri.getPathSegments().get(1) + (!TextUtils.isEmpty(str) ? " AND (" + str + ")" : Constants.STR_EMPTY), strArr);
                break;
            case 7:
                delete = this.f.delete("qx_cache", str, strArr);
                break;
            case 8:
                delete = this.f.delete("qx_cache", "_id=" + uri.getPathSegments().get(1) + (!TextUtils.isEmpty(str) ? " AND (" + str + ")" : Constants.STR_EMPTY), strArr);
                break;
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return delete;
    }

    public String getType(Uri uri) {
        switch (g.match(uri)) {
            case 1:
                return "vnd.android.cursor.dir/vnd.qq.android.EBOOKS";
            case 2:
                return "vnd.android.cursor.item/vnd.qq.android.EBOOKS";
            case 3:
                return "vnd.android.cursor.dir/vnd.qq.android.qx.info";
            case 4:
                return "vnd.android.cursor.item/vnd.qq.android.qx.info";
            case 5:
                return "vnd.android.cursor.dir/vnd.qq.android.qx.friend";
            case 6:
                return "vnd.android.cursor.item/vnd.qq.android.qx.friend";
            case 7:
                return "vnd.android.cursor.dir/vnd.qq.android.qx.data";
            case 8:
                return "vnd.android.cursor.item/vnd.qq.android.qx.data";
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }

    private long a(ContentValues contentValues) {
        if (contentValues != null && contentValues.containsKey("f_id") && contentValues.containsKey("data") && contentValues.containsKey(SocialConstants.PARAM_TYPE)) {
            return this.f.insert("qx_cache", null, contentValues);
        }
        return 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    private long b(ContentValues contentValues) {
        if (contentValues == null || !contentValues.containsKey("number")) {
            return 0;
        }
        if (!contentValues.containsKey("de")) {
            contentValues.put("de", (Integer) 0);
        }
        return this.f.insert("qx_friends_table", null, contentValues);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    private long c(ContentValues contentValues) {
        if (contentValues == null || !contentValues.containsKey("key") || !contentValues.containsKey("uid") || !contentValues.containsKey("number")) {
            return 0;
        }
        if (!contentValues.containsKey("lc_scan_count")) {
            contentValues.put("lc_scan_count", (Integer) 0);
        }
        if (!contentValues.containsKey("lc_scan_lid")) {
            contentValues.put("lc_scan_lid", (Integer) 0);
        }
        if (!contentValues.containsKey("lc_scan_time")) {
            contentValues.put("lc_scan_time", (Integer) 0);
        }
        if (!contentValues.containsKey("status")) {
            contentValues.put("status", (Integer) 0);
        }
        return this.f.insert("qx_user", null, contentValues);
    }

    private long d(ContentValues contentValues) {
        if (contentValues == null) {
            return 0;
        }
        if (!contentValues.containsKey("name")) {
            contentValues.putNull("name");
        }
        if (!contentValues.containsKey(SocialConstants.PARAM_TYPE)) {
            contentValues.putNull(SocialConstants.PARAM_TYPE);
        }
        if (!contentValues.containsKey("size")) {
            contentValues.putNull("size");
        }
        if (!contentValues.containsKey("data")) {
            contentValues.putNull("data");
        }
        if (!contentValues.containsKey("kind")) {
            contentValues.putNull("kind");
        }
        if (!contentValues.containsKey("label")) {
            contentValues.putNull("label");
        }
        if (!contentValues.containsKey("date_add")) {
            contentValues.putNull("date_add");
        }
        if (!contentValues.containsKey("date_modify")) {
            contentValues.putNull("date_modify");
        }
        if (!contentValues.containsKey("date_last_read")) {
            contentValues.putNull("date_last_read");
        }
        if (!contentValues.containsKey("read_times")) {
            contentValues.putNull("read_times");
        }
        return this.f.insert(DBHelper.CONTACTS_TABLE, null, contentValues);
    }

    public Uri insert(Uri uri, ContentValues contentValues) {
        ContentValues contentValues2;
        Uri uri2;
        if (contentValues != null) {
            contentValues2 = new ContentValues(contentValues);
        } else {
            contentValues2 = new ContentValues();
        }
        switch (g.match(uri)) {
            case 1:
                long d2 = d(contentValues2);
                if (d2 > 0) {
                    uri2 = ContentUris.withAppendedId(f338a, d2);
                    getContext().getContentResolver().notifyChange(uri2, null);
                    break;
                }
                uri2 = null;
                break;
            case 2:
            case 4:
            case 6:
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
            case 3:
                long c2 = c(contentValues2);
                if (c2 > 0) {
                    uri2 = ContentUris.withAppendedId(b, c2);
                    getContext().getContentResolver().notifyChange(uri2, null);
                    break;
                }
                uri2 = null;
                break;
            case 5:
                long b2 = b(contentValues2);
                if (b2 > 0) {
                    uri2 = ContentUris.withAppendedId(c, b2);
                    getContext().getContentResolver().notifyChange(uri2, null);
                    break;
                }
                uri2 = null;
                break;
            case 7:
                long a2 = a(contentValues2);
                if (a2 > 0) {
                    uri2 = ContentUris.withAppendedId(d, a2);
                    getContext().getContentResolver().notifyChange(uri2, null);
                    break;
                }
                uri2 = null;
                break;
        }
        if (uri2 != null) {
            return uri2;
        }
        throw new SQLException("Failed to insert row into " + uri);
    }

    public Cursor query(Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        String str3;
        SQLiteQueryBuilder sQLiteQueryBuilder = new SQLiteQueryBuilder();
        if (TextUtils.isEmpty(str2)) {
            str3 = "_id";
        } else {
            str3 = str2;
        }
        switch (g.match(uri)) {
            case 1:
                sQLiteQueryBuilder.setTables(DBHelper.CONTACTS_TABLE);
                Cursor query = sQLiteQueryBuilder.query(this.f, strArr, str, strArr2, null, null, str3);
                query.setNotificationUri(getContext().getContentResolver(), uri);
                return query;
            case 2:
                sQLiteQueryBuilder.setTables(DBHelper.CONTACTS_TABLE);
                sQLiteQueryBuilder.appendWhere("_id=" + uri.getPathSegments().get(1));
                Cursor query2 = sQLiteQueryBuilder.query(this.f, strArr, str, strArr2, null, null, str3);
                query2.setNotificationUri(getContext().getContentResolver(), uri);
                return query2;
            case 3:
                sQLiteQueryBuilder.setTables("qx_user");
                Cursor query3 = sQLiteQueryBuilder.query(this.f, strArr, str, strArr2, null, null, str3);
                query3.setNotificationUri(getContext().getContentResolver(), uri);
                return query3;
            case 4:
                sQLiteQueryBuilder.setTables("qx_user");
                sQLiteQueryBuilder.appendWhere("_id=" + uri.getPathSegments().get(1));
                Cursor query4 = sQLiteQueryBuilder.query(this.f, strArr, str, strArr2, null, null, str3);
                query4.setNotificationUri(getContext().getContentResolver(), uri);
                return query4;
            case 5:
                sQLiteQueryBuilder.setTables("qx_friends_table");
                Cursor query5 = sQLiteQueryBuilder.query(this.f, strArr, str, strArr2, null, null, str3);
                query5.setNotificationUri(getContext().getContentResolver(), uri);
                return query5;
            case 6:
                sQLiteQueryBuilder.setTables("qx_friends_table");
                sQLiteQueryBuilder.appendWhere("_id=" + uri.getPathSegments().get(1));
                Cursor query6 = sQLiteQueryBuilder.query(this.f, strArr, str, strArr2, null, null, str3);
                query6.setNotificationUri(getContext().getContentResolver(), uri);
                return query6;
            case 7:
                sQLiteQueryBuilder.setTables("qx_cache");
                Cursor query7 = sQLiteQueryBuilder.query(this.f, strArr, str, strArr2, null, null, str3);
                query7.setNotificationUri(getContext().getContentResolver(), uri);
                return query7;
            case 8:
                sQLiteQueryBuilder.setTables("qx_cache");
                sQLiteQueryBuilder.appendWhere("_id=" + uri.getPathSegments().get(1));
                Cursor query8 = sQLiteQueryBuilder.query(this.f, strArr, str, strArr2, null, null, str3);
                query8.setNotificationUri(getContext().getContentResolver(), uri);
                return query8;
            default:
                return null;
        }
    }

    public int update(Uri uri, ContentValues contentValues, String str, String[] strArr) {
        int update;
        switch (g.match(uri)) {
            case 1:
                update = this.f.update(DBHelper.CONTACTS_TABLE, contentValues, str, strArr);
                break;
            case 2:
                update = this.f.update(DBHelper.CONTACTS_TABLE, contentValues, "_id=" + uri.getPathSegments().get(1) + (!TextUtils.isEmpty(str) ? " AND (" + str + ")" : Constants.STR_EMPTY), strArr);
                break;
            case 3:
                update = this.f.update("qx_user", contentValues, str, strArr);
                break;
            case 4:
                update = this.f.update("qx_user", contentValues, "_id=" + uri.getPathSegments().get(1) + (!TextUtils.isEmpty(str) ? " AND (" + str + ")" : Constants.STR_EMPTY), strArr);
                break;
            case 5:
                update = this.f.update("qx_friends_table", contentValues, str, strArr);
                break;
            case 6:
                update = this.f.update("qx_friends_table", contentValues, "_id=" + uri.getPathSegments().get(1) + (!TextUtils.isEmpty(str) ? " AND (" + str + ")" : Constants.STR_EMPTY), strArr);
                break;
            case 7:
                update = this.f.update("qx_cache", contentValues, str, strArr);
                break;
            case 8:
                update = this.f.update("qx_cache", contentValues, "_id=" + uri.getPathSegments().get(1) + (!TextUtils.isEmpty(str) ? " AND (" + str + ")" : Constants.STR_EMPTY), strArr);
                break;
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return update;
    }
}
