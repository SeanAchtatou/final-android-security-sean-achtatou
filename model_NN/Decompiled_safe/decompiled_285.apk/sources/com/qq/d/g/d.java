package com.qq.d.g;

import android.os.SystemProperties;
import java.io.File;
import java.util.Map;

/* compiled from: ProGuard */
public class d extends r {
    public boolean a(Map<String, String> map, boolean z, s sVar) {
        boolean z2;
        boolean exists = new File("/system/dxversion").exists();
        if (!z && exists) {
            a(map, "rombrand", "dianxin");
            a(map, "romversion", SystemProperties.get("ro.dianxinos.os.version"));
        }
        if (this.b == null) {
            return exists;
        }
        r rVar = this.b;
        if (z || exists) {
            z2 = true;
        } else {
            z2 = false;
        }
        boolean a2 = rVar.a(map, z2, sVar);
        if (exists || a2) {
            return true;
        }
        return false;
    }
}
