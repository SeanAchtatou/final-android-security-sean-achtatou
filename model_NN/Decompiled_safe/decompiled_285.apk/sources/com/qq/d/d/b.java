package com.qq.d.d;

import android.content.Context;
import android.database.Cursor;
import android.provider.MediaStore;
import java.io.File;

/* compiled from: ProGuard */
public class b {
    public static void a(Context context, int i) {
        Cursor query = context.getContentResolver().query(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, null, "image_id = " + i, null, null);
        if (query != null) {
            for (boolean moveToFirst = query.moveToFirst(); moveToFirst; moveToFirst = query.moveToNext()) {
                String string = query.getString(query.getColumnIndex("_data"));
                if (string != null) {
                    File file = new File(string);
                    if (file.exists()) {
                        file.delete();
                    }
                }
            }
            query.close();
        }
    }
}
