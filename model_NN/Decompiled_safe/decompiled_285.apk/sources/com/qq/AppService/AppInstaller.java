package com.qq.AppService;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import com.qq.provider.h;
import com.tencent.assistant.component.txscrollview.TXTabBarLayout;
import com.tencent.open.SocialConstants;
import java.io.File;

/* compiled from: ProGuard */
public class AppInstaller extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private volatile boolean f198a = false;
    private String b = null;
    private String c = null;
    private boolean d = false;

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.d && this.b != null) {
            File file = new File(this.b);
            if (file.exists()) {
                file.delete();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        PackageInfo packageInfo = null;
        super.onCreate(bundle);
        Intent intent = getIntent();
        if (intent == null) {
            finish();
            return;
        }
        Bundle extras = intent.getExtras();
        if (extras == null) {
            finish();
            return;
        }
        String string = extras.getString(SocialConstants.PARAM_ACT);
        if (string == null) {
            finish();
        } else if (string.equals("install")) {
            String string2 = extras.getString("path");
            this.b = string2;
            this.d = extras.getBoolean("delete");
            Uri fromFile = Uri.fromFile(new File(string2));
            try {
                packageInfo = getPackageManager().getPackageArchiveInfo(string2, 0);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (packageInfo == null) {
                this.f198a = true;
                Log.d("com.qq.connect", "archive of apk is bad:" + this.b);
                finish();
                return;
            }
            Intent intent2 = new Intent();
            intent2.setData(fromFile);
            intent2.addFlags(1);
            intent2.setClassName("com.android.packageinstaller", "com.android.packageinstaller.PackageInstallerActivity");
            startActivity(intent2);
        } else if (string.equals("uninstall")) {
            this.c = extras.getString("pkg");
            startActivity(new Intent("android.intent.action.DELETE", Uri.fromParts("package", this.c, null)));
        } else {
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        this.f198a = true;
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.f198a) {
            if (this.b != null) {
                h.a(100, this.b, false, false);
            } else if (this.c != null) {
                h.a(TXTabBarLayout.TABITEM_TIPS_TEXT_ID, this.c, false, false);
            }
            finish();
        }
    }
}
