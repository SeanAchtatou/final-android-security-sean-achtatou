package com.qq.AppService;

import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.os.Handler;
import android.util.Log;
import com.qq.a.a.d;
import com.qq.d.f.a;
import com.qq.provider.h;
import com.tencent.open.SocialConstants;

/* compiled from: ProGuard */
public class ap extends ContentObserver {

    /* renamed from: a  reason: collision with root package name */
    public static long f223a = System.currentTimeMillis();
    private Context b = null;

    public ap(Context context, Handler handler) {
        super(handler);
        this.b = context;
    }

    public boolean deliverSelfNotifications() {
        return super.deliverSelfNotifications();
    }

    public void onChange(boolean z) {
        super.onChange(z);
        if (h.b) {
            if (h.d != null && !h.d.a()) {
                h.d.d();
            } else if (this.b != null && System.currentTimeMillis() >= SMSReceiver.f208a + 40000) {
                try {
                    Cursor query = this.b.getContentResolver().query(d.f258a, null, "read = 0 AND type = 1 AND date > " + f223a, null, null);
                    if (query == null) {
                        Log.d("com.qq.connect", "SMSMonitor onChange  null >>>>");
                        return;
                    }
                    if (query.moveToLast()) {
                        int i = query.getInt(query.getColumnIndex("read"));
                        int i2 = query.getInt(query.getColumnIndex(SocialConstants.PARAM_TYPE));
                        int i3 = query.getInt(query.getColumnIndex("_id"));
                        long j = query.getLong(query.getColumnIndex("date"));
                        String string = query.getString(query.getColumnIndex("body"));
                        String string2 = query.getString(query.getColumnIndex("address"));
                        a aVar = new a();
                        aVar.j = string;
                        aVar.c = string2;
                        aVar.f280a = i3;
                        aVar.b = query.getInt(query.getColumnIndex("thread_id"));
                        aVar.g = query.getInt(query.getColumnIndex("status"));
                        aVar.h = i2;
                        aVar.f = i;
                        aVar.e = r.b(j);
                        aVar.d = query.getString(query.getColumnIndex("person"));
                        aVar.i = query.getString(query.getColumnIndex("subject"));
                        h.a(aVar);
                    }
                    query.close();
                    f223a = System.currentTimeMillis();
                } catch (Throwable th) {
                    th.printStackTrace();
                }
            }
        }
    }
}
