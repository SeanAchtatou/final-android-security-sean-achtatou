package com.qq.AppService;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import com.qq.a.a.d;
import com.qq.d.f.a;
import com.qq.provider.h;
import com.tencent.open.SocialConstants;
import java.util.Vector;

/* compiled from: ProGuard */
public class ag {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public volatile boolean f216a = true;
    /* access modifiers changed from: private */
    public Vector<ai> b = new Vector<>();
    /* access modifiers changed from: private */
    public Object c = new Object();
    /* access modifiers changed from: private */
    public volatile boolean d = false;
    private Context e;
    private aj f = null;

    public boolean a() {
        return this.b.isEmpty();
    }

    public ag(Context context) {
        this.e = context.getApplicationContext();
    }

    public synchronized void b() {
        this.f = new aj(this);
        this.f.start();
    }

    public void c() {
        this.f216a = false;
        synchronized (this) {
            notifyAll();
        }
        synchronized (this.c) {
            this.c.notifyAll();
        }
        this.b.clear();
    }

    public void d() {
        synchronized (this.c) {
            this.d = true;
            this.c.notifyAll();
        }
    }

    public void a(ai aiVar) {
        Cursor cursor;
        if (aiVar != null) {
            try {
                cursor = this.e.getContentResolver().query(d.f258a, null, "address like '" + aiVar.b + '\'' + " AND " + "read" + " = 0 AND " + SocialConstants.PARAM_TYPE + " = " + 1 + " AND " + "date" + " > " + ap.f223a, null, "_id DESC");
            } catch (Exception e2) {
                e2.printStackTrace();
                cursor = null;
            }
            if (cursor == null) {
                Log.d("com.qq.connect", "cursor is null");
                return;
            }
            try {
                if (cursor.moveToFirst()) {
                    String string = cursor.getString(cursor.getColumnIndex("body"));
                    String string2 = cursor.getString(cursor.getColumnIndex("address"));
                    int i = cursor.getInt(cursor.getColumnIndex(SocialConstants.PARAM_TYPE));
                    a aVar = new a();
                    aVar.j = string;
                    aVar.c = string2;
                    aVar.f280a = cursor.getInt(cursor.getColumnIndex("_id"));
                    aVar.b = cursor.getInt(cursor.getColumnIndex("thread_id"));
                    aVar.g = cursor.getInt(cursor.getColumnIndex("status"));
                    aVar.h = i;
                    aVar.f = cursor.getInt(cursor.getColumnIndex("read"));
                    aVar.e = r.b(cursor.getLong(cursor.getColumnIndex("date")));
                    aVar.d = cursor.getString(cursor.getColumnIndex("person"));
                    aVar.i = cursor.getString(cursor.getColumnIndex("subject"));
                    ap.f223a = System.currentTimeMillis();
                    h.a(aVar);
                } else {
                    b(aiVar);
                }
                if (cursor == null) {
                    return;
                }
            } catch (Throwable th) {
                if (cursor != null) {
                    cursor.close();
                }
                throw th;
            }
            cursor.close();
        }
    }

    public void b(ai aiVar) {
        this.b.add(aiVar);
        synchronized (this) {
            notifyAll();
        }
    }

    public void a(String str, String str2) {
        this.b.add(new ai(str, str2, System.currentTimeMillis()));
        synchronized (this) {
            notifyAll();
        }
    }

    public ai e() {
        if (this.b.size() <= 0) {
            return null;
        }
        ai aiVar = this.b.get(0);
        this.b.remove(0);
        return aiVar;
    }
}
