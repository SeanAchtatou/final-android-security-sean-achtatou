package com.qq.l;

import android.os.Build;
import com.tencent.assistant.db.table.i;
import com.tencent.assistant.net.c;
import com.tencent.assistant.utils.t;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public abstract class a {
    private static String b;
    private static String c;
    private static String e;
    private static String f;
    private static String g;

    /* renamed from: a  reason: collision with root package name */
    private String f301a;
    private long d;
    private i h;

    public abstract void l();

    public a() {
        this.d = -1;
        this.h = null;
        this.h = i.a();
    }

    public String a() {
        return Build.VERSION.RELEASE;
    }

    public long b() {
        if (this.d == -1) {
            this.d = t.c();
        }
        return this.d;
    }

    public int c() {
        int c2 = c.c();
        if (c2 == 3) {
            return 0;
        }
        if (c2 == 1) {
            return 1;
        }
        if (c2 == 2) {
            return 2;
        }
        return -1;
    }

    public String d() {
        if (this.f301a == null) {
            this.f301a = "null";
            this.f301a = t.k();
            if (this.f301a != null) {
                this.f301a = this.f301a.replace(":", Constants.STR_EMPTY);
            }
        }
        return this.f301a;
    }

    public String e() {
        if (b == null) {
            b = t.g();
            if (b == "000000000000000" || b == null) {
                b = "null";
            }
        }
        return b;
    }

    public String f() {
        if (c == null) {
            c = t.h();
            if (c == "000000" || c == null) {
                c = "null";
            }
        }
        return c;
    }

    public String g() {
        return t.o() + Constants.STR_EMPTY;
    }

    public String h() {
        if (f == null) {
            f = Build.MODEL;
        }
        return f;
    }

    public String i() {
        if (e == null) {
            e = Build.BRAND;
        }
        return e;
    }

    public String j() {
        if (g == null) {
            g = Build.DEVICE;
        }
        return g;
    }

    public i k() {
        return this.h;
    }
}
