package com.google.zxing.d.b;

import com.google.zxing.NotFoundException;
import com.google.zxing.common.b;
import com.google.zxing.d;
import com.google.zxing.j;
import com.google.zxing.k;
import java.util.Hashtable;
import java.util.Vector;

public class e {

    /* renamed from: a  reason: collision with root package name */
    private final b f189a;
    private final Vector b = new Vector();
    private boolean c;
    private final int[] d = new int[5];
    private final k e;

    public e(b bVar, k kVar) {
        this.f189a = bVar;
        this.e = kVar;
    }

    private float a(int i, int i2, int i3, int i4) {
        b bVar = this.f189a;
        int c2 = bVar.c();
        int[] a2 = a();
        int i5 = i;
        while (i5 >= 0 && bVar.a(i2, i5)) {
            a2[2] = a2[2] + 1;
            i5--;
        }
        if (i5 < 0) {
            return Float.NaN;
        }
        while (i5 >= 0 && !bVar.a(i2, i5) && a2[1] <= i3) {
            a2[1] = a2[1] + 1;
            i5--;
        }
        if (i5 < 0 || a2[1] > i3) {
            return Float.NaN;
        }
        while (i5 >= 0 && bVar.a(i2, i5) && a2[0] <= i3) {
            a2[0] = a2[0] + 1;
            i5--;
        }
        if (a2[0] > i3) {
            return Float.NaN;
        }
        int i6 = i + 1;
        while (i6 < c2 && bVar.a(i2, i6)) {
            a2[2] = a2[2] + 1;
            i6++;
        }
        if (i6 == c2) {
            return Float.NaN;
        }
        while (i6 < c2 && !bVar.a(i2, i6) && a2[3] < i3) {
            a2[3] = a2[3] + 1;
            i6++;
        }
        if (i6 == c2 || a2[3] >= i3) {
            return Float.NaN;
        }
        while (i6 < c2 && bVar.a(i2, i6) && a2[4] < i3) {
            a2[4] = a2[4] + 1;
            i6++;
        }
        if (a2[4] >= i3 || Math.abs(((((a2[0] + a2[1]) + a2[2]) + a2[3]) + a2[4]) - i4) * 5 >= i4 * 2 || !a(a2)) {
            return Float.NaN;
        }
        return a(a2, i6);
    }

    private static float a(int[] iArr, int i) {
        return ((float) ((i - iArr[4]) - iArr[3])) - (((float) iArr[2]) / 2.0f);
    }

    protected static boolean a(int[] iArr) {
        boolean z = true;
        int i = 0;
        for (int i2 = 0; i2 < 5; i2++) {
            int i3 = iArr[i2];
            if (i3 == 0) {
                return false;
            }
            i += i3;
        }
        if (i < 7) {
            return false;
        }
        int i4 = (i << 8) / 7;
        int i5 = i4 / 2;
        if (Math.abs(i4 - (iArr[0] << 8)) >= i5 || Math.abs(i4 - (iArr[1] << 8)) >= i5 || Math.abs((i4 * 3) - (iArr[2] << 8)) >= i5 * 3 || Math.abs(i4 - (iArr[3] << 8)) >= i5 || Math.abs(i4 - (iArr[4] << 8)) >= i5) {
            z = false;
        }
        return z;
    }

    private int[] a() {
        this.d[0] = 0;
        this.d[1] = 0;
        this.d[2] = 0;
        this.d[3] = 0;
        this.d[4] = 0;
        return this.d;
    }

    private float b(int i, int i2, int i3, int i4) {
        b bVar = this.f189a;
        int b2 = bVar.b();
        int[] a2 = a();
        int i5 = i;
        while (i5 >= 0 && bVar.a(i5, i2)) {
            a2[2] = a2[2] + 1;
            i5--;
        }
        if (i5 < 0) {
            return Float.NaN;
        }
        while (i5 >= 0 && !bVar.a(i5, i2) && a2[1] <= i3) {
            a2[1] = a2[1] + 1;
            i5--;
        }
        if (i5 < 0 || a2[1] > i3) {
            return Float.NaN;
        }
        while (i5 >= 0 && bVar.a(i5, i2) && a2[0] <= i3) {
            a2[0] = a2[0] + 1;
            i5--;
        }
        if (a2[0] > i3) {
            return Float.NaN;
        }
        int i6 = i + 1;
        while (i6 < b2 && bVar.a(i6, i2)) {
            a2[2] = a2[2] + 1;
            i6++;
        }
        if (i6 == b2) {
            return Float.NaN;
        }
        while (i6 < b2 && !bVar.a(i6, i2) && a2[3] < i3) {
            a2[3] = a2[3] + 1;
            i6++;
        }
        if (i6 == b2 || a2[3] >= i3) {
            return Float.NaN;
        }
        while (i6 < b2 && bVar.a(i6, i2) && a2[4] < i3) {
            a2[4] = a2[4] + 1;
            i6++;
        }
        if (a2[4] >= i3 || Math.abs(((((a2[0] + a2[1]) + a2[2]) + a2[3]) + a2[4]) - i4) * 5 >= i4 || !a(a2)) {
            return Float.NaN;
        }
        return a(a2, i6);
    }

    private int b() {
        int size = this.b.size();
        if (size <= 1) {
            return 0;
        }
        d dVar = null;
        int i = 0;
        while (i < size) {
            d dVar2 = (d) this.b.elementAt(i);
            if (dVar2.d() < 2) {
                dVar2 = dVar;
            } else if (dVar != null) {
                this.c = true;
                return ((int) (Math.abs(dVar.a() - dVar2.a()) - Math.abs(dVar.b() - dVar2.b()))) / 2;
            }
            i++;
            dVar = dVar2;
        }
        return 0;
    }

    private boolean c() {
        float f;
        float f2 = 0.0f;
        int size = this.b.size();
        int i = 0;
        float f3 = 0.0f;
        int i2 = 0;
        while (i < size) {
            d dVar = (d) this.b.elementAt(i);
            if (dVar.d() >= 2) {
                i2++;
                f = dVar.c() + f3;
            } else {
                f = f3;
            }
            i++;
            i2 = i2;
            f3 = f;
        }
        if (i2 < 3) {
            return false;
        }
        float f4 = f3 / ((float) size);
        for (int i3 = 0; i3 < size; i3++) {
            f2 += Math.abs(((d) this.b.elementAt(i3)).c() - f4);
        }
        return f2 <= 0.05f * f3;
    }

    private d[] d() {
        float f = 0.0f;
        int size = this.b.size();
        if (size < 3) {
            throw NotFoundException.a();
        }
        if (size > 3) {
            float f2 = 0.0f;
            float f3 = 0.0f;
            for (int i = 0; i < size; i++) {
                float c2 = ((d) this.b.elementAt(i)).c();
                f3 += c2;
                f2 += c2 * c2;
            }
            float f4 = f3 / ((float) size);
            com.google.zxing.common.e.a(this.b, new g(f4));
            float max = Math.max(0.2f * f4, (float) Math.sqrt((double) ((f2 / ((float) size)) - (f4 * f4))));
            int i2 = 0;
            while (i2 < this.b.size() && this.b.size() > 3) {
                if (Math.abs(((d) this.b.elementAt(i2)).c() - f4) > max) {
                    this.b.removeElementAt(i2);
                    i2--;
                }
                i2++;
            }
        }
        if (this.b.size() > 3) {
            for (int i3 = 0; i3 < this.b.size(); i3++) {
                f += ((d) this.b.elementAt(i3)).c();
            }
            com.google.zxing.common.e.a(this.b, new f(f / ((float) this.b.size())));
            this.b.setSize(3);
        }
        return new d[]{(d) this.b.elementAt(0), (d) this.b.elementAt(1), (d) this.b.elementAt(2)};
    }

    /* access modifiers changed from: package-private */
    public h a(Hashtable hashtable) {
        int i;
        int i2;
        boolean z;
        boolean z2 = hashtable != null && hashtable.containsKey(d.d);
        int c2 = this.f189a.c();
        int b2 = this.f189a.b();
        int i3 = (c2 * 3) / 228;
        int i4 = (i3 < 3 || z2) ? 3 : i3;
        int[] iArr = new int[5];
        int i5 = i4 - 1;
        boolean z3 = false;
        int i6 = i4;
        while (i5 < c2 && !z3) {
            iArr[0] = 0;
            iArr[1] = 0;
            iArr[2] = 0;
            iArr[3] = 0;
            iArr[4] = 0;
            int i7 = 0;
            int i8 = 0;
            while (i7 < b2) {
                if (this.f189a.a(i7, i5)) {
                    if ((i8 & 1) == 1) {
                        i8++;
                    }
                    iArr[i8] = iArr[i8] + 1;
                } else if ((i8 & 1) != 0) {
                    iArr[i8] = iArr[i8] + 1;
                } else if (i8 != 4) {
                    i8++;
                    iArr[i8] = iArr[i8] + 1;
                } else if (!a(iArr)) {
                    iArr[0] = iArr[2];
                    iArr[1] = iArr[3];
                    iArr[2] = iArr[4];
                    iArr[3] = 1;
                    iArr[4] = 0;
                    i8 = 3;
                } else if (a(iArr, i5, i7)) {
                    if (this.c) {
                        z = c();
                    } else {
                        int b3 = b();
                        if (b3 > iArr[2]) {
                            i2 = i5 + ((b3 - iArr[2]) - 2);
                            i = b2 - 1;
                        } else {
                            i = i7;
                            i2 = i5;
                        }
                        i5 = i2;
                        i7 = i;
                        z = z3;
                    }
                    iArr[0] = 0;
                    iArr[1] = 0;
                    iArr[2] = 0;
                    iArr[3] = 0;
                    iArr[4] = 0;
                    z3 = z;
                    i6 = 2;
                    i8 = 0;
                } else {
                    iArr[0] = iArr[2];
                    iArr[1] = iArr[3];
                    iArr[2] = iArr[4];
                    iArr[3] = 1;
                    iArr[4] = 0;
                    i8 = 3;
                }
                i7++;
            }
            if (a(iArr) && a(iArr, i5, b2)) {
                i6 = iArr[0];
                if (this.c) {
                    z3 = c();
                }
            }
            i5 += i6;
        }
        d[] d2 = d();
        j.a(d2);
        return new h(d2);
    }

    /* access modifiers changed from: protected */
    public boolean a(int[] iArr, int i, int i2) {
        boolean z = false;
        int i3 = iArr[0] + iArr[1] + iArr[2] + iArr[3] + iArr[4];
        float a2 = a(iArr, i2);
        float a3 = a(i, (int) a2, iArr[2], i3);
        if (!Float.isNaN(a3)) {
            float b2 = b((int) a2, (int) a3, iArr[2], i3);
            if (!Float.isNaN(b2)) {
                float f = ((float) i3) / 7.0f;
                int size = this.b.size();
                int i4 = 0;
                while (true) {
                    if (i4 >= size) {
                        break;
                    }
                    d dVar = (d) this.b.elementAt(i4);
                    if (dVar.a(f, a3, b2)) {
                        dVar.e();
                        z = true;
                        break;
                    }
                    i4++;
                }
                if (!z) {
                    d dVar2 = new d(b2, a3, f);
                    this.b.addElement(dVar2);
                    if (this.e != null) {
                        this.e.a(dVar2);
                    }
                }
                return true;
            }
        }
        return false;
    }
}
