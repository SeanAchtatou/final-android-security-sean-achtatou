package com.google.zxing.d.b;

import com.google.zxing.NotFoundException;
import com.google.zxing.common.b;
import com.google.zxing.common.i;
import com.google.zxing.common.l;
import com.google.zxing.common.n;
import com.google.zxing.d;
import com.google.zxing.d.a.r;
import com.google.zxing.j;
import com.google.zxing.k;
import java.util.Hashtable;

public class c {

    /* renamed from: a  reason: collision with root package name */
    private final b f187a;
    private k b;

    public c(b bVar) {
        this.f187a = bVar;
    }

    private float a(int i, int i2, int i3, int i4) {
        int i5;
        float f;
        float f2;
        int i6 = 0;
        float b2 = b(i, i2, i3, i4);
        int i7 = i - (i3 - i);
        if (i7 < 0) {
            f = ((float) i) / ((float) (i - i7));
            i5 = 0;
        } else if (i7 > this.f187a.b()) {
            f = ((float) (this.f187a.b() - i)) / ((float) (i7 - i));
            i5 = this.f187a.b();
        } else {
            i5 = i7;
            f = 1.0f;
        }
        int i8 = (int) (((float) i2) - (f * ((float) (i4 - i2))));
        if (i8 < 0) {
            f2 = ((float) i2) / ((float) (i2 - i8));
        } else if (i8 > this.f187a.c()) {
            f2 = ((float) (this.f187a.c() - i2)) / ((float) (i8 - i2));
            i6 = this.f187a.c();
        } else {
            i6 = i8;
            f2 = 1.0f;
        }
        return b(i, i2, (int) ((f2 * ((float) (i5 - i))) + ((float) i)), i6) + b2;
    }

    private float a(j jVar, j jVar2) {
        float a2 = a((int) jVar.a(), (int) jVar.b(), (int) jVar2.a(), (int) jVar2.b());
        float a3 = a((int) jVar2.a(), (int) jVar2.b(), (int) jVar.a(), (int) jVar.b());
        return Float.isNaN(a2) ? a3 / 7.0f : Float.isNaN(a3) ? a2 / 7.0f : (a2 + a3) / 14.0f;
    }

    private static int a(float f) {
        return (int) (0.5f + f);
    }

    protected static int a(j jVar, j jVar2, j jVar3, float f) {
        int a2 = ((a(j.a(jVar, jVar2) / f) + a(j.a(jVar, jVar3) / f)) >> 1) + 7;
        switch (a2 & 3) {
            case 0:
                return a2 + 1;
            case 1:
            default:
                return a2;
            case 2:
                return a2 - 1;
            case 3:
                throw NotFoundException.a();
        }
    }

    private static b a(b bVar, n nVar, int i) {
        return l.a().a(bVar, i, nVar);
    }

    private float b(int i, int i2, int i3, int i4) {
        int i5;
        int i6;
        int i7;
        boolean z = Math.abs(i4 - i2) > Math.abs(i3 - i);
        if (!z) {
            int i8 = i4;
            i4 = i3;
            i3 = i8;
            int i9 = i2;
            i2 = i;
            i = i9;
        }
        int abs = Math.abs(i4 - i2);
        int abs2 = Math.abs(i3 - i);
        int i10 = (-abs) >> 1;
        int i11 = i < i3 ? 1 : -1;
        int i12 = i2 < i4 ? 1 : -1;
        int i13 = 0;
        int i14 = i2;
        int i15 = i10;
        int i16 = i;
        while (i14 != i4) {
            int i17 = z ? i16 : i14;
            int i18 = z ? i14 : i16;
            if (i13 == 1) {
                if (this.f187a.a(i17, i18)) {
                    i5 = i13 + 1;
                }
                i5 = i13;
            } else {
                if (!this.f187a.a(i17, i18)) {
                    i5 = i13 + 1;
                }
                i5 = i13;
            }
            if (i5 == 3) {
                int i19 = i14 - i2;
                int i20 = i16 - i;
                int i21 = i12 < 0 ? i19 + 1 : i19;
                return (float) Math.sqrt((double) ((i21 * i21) + (i20 * i20)));
            }
            int i22 = i15 + abs2;
            if (i22 <= 0) {
                i6 = i16;
                i7 = i22;
            } else if (i16 == i3) {
                break;
            } else {
                i6 = i16 + i11;
                i7 = i22 - abs;
            }
            i14 += i12;
            i15 = i7;
            i16 = i6;
            i13 = i5;
        }
        int i23 = i4 - i2;
        int i24 = i3 - i;
        return (float) Math.sqrt((double) ((i23 * i23) + (i24 * i24)));
    }

    /* access modifiers changed from: protected */
    public float a(j jVar, j jVar2, j jVar3) {
        return (a(jVar, jVar2) + a(jVar, jVar3)) / 2.0f;
    }

    /* access modifiers changed from: protected */
    public i a(h hVar) {
        d b2 = hVar.b();
        d c = hVar.c();
        d a2 = hVar.a();
        float a3 = a(b2, c, a2);
        if (a3 < 1.0f) {
            throw NotFoundException.a();
        }
        int a4 = a(b2, c, a2, a3);
        r a5 = r.a(a4);
        int d = a5.d() - 7;
        a aVar = null;
        if (a5.b().length > 0) {
            float a6 = (c.a() - b2.a()) + a2.a();
            float b3 = (c.b() - b2.b()) + a2.b();
            float f = 1.0f - (3.0f / ((float) d));
            int a7 = (int) (((a6 - b2.a()) * f) + b2.a());
            int b4 = (int) (b2.b() + (f * (b3 - b2.b())));
            int i = 4;
            while (i <= 16) {
                try {
                    aVar = a(a3, a7, b4, (float) i);
                    break;
                } catch (NotFoundException e) {
                    i <<= 1;
                }
            }
        }
        return new i(a(this.f187a, a(b2, c, a2, aVar, a4), a4), aVar == null ? new j[]{a2, b2, c} : new j[]{a2, b2, c, aVar});
    }

    public i a(Hashtable hashtable) {
        this.b = hashtable == null ? null : (k) hashtable.get(d.h);
        return a(new e(this.f187a, this.b).a(hashtable));
    }

    public n a(j jVar, j jVar2, j jVar3, j jVar4, int i) {
        float a2;
        float b2;
        float f;
        float f2;
        float f3 = ((float) i) - 3.5f;
        if (jVar4 != null) {
            a2 = jVar4.a();
            b2 = jVar4.b();
            f = f3 - 3.0f;
            f2 = f;
        } else {
            a2 = (jVar2.a() - jVar.a()) + jVar3.a();
            b2 = (jVar2.b() - jVar.b()) + jVar3.b();
            f = f3;
            f2 = f3;
        }
        return n.a(3.5f, 3.5f, f3, 3.5f, f2, f, 3.5f, f3, jVar.a(), jVar.b(), jVar2.a(), jVar2.b(), a2, b2, jVar3.a(), jVar3.b());
    }

    /* access modifiers changed from: protected */
    public a a(float f, int i, int i2, float f2) {
        int i3 = (int) (f2 * f);
        int max = Math.max(0, i - i3);
        int min = Math.min(this.f187a.b() - 1, i + i3);
        if (((float) (min - max)) < f * 3.0f) {
            throw NotFoundException.a();
        }
        int max2 = Math.max(0, i2 - i3);
        int min2 = Math.min(this.f187a.c() - 1, i3 + i2);
        if (((float) (min2 - max2)) < f * 3.0f) {
            throw NotFoundException.a();
        }
        return new b(this.f187a, max, max2, min - max, min2 - max2, f, this.b).a();
    }
}
