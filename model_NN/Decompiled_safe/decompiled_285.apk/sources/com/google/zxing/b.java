package com.google.zxing;

import com.google.zxing.common.a;

public abstract class b {

    /* renamed from: a  reason: collision with root package name */
    private final e f125a;

    protected b(e eVar) {
        if (eVar == null) {
            throw new IllegalArgumentException("Source must be non-null.");
        }
        this.f125a = eVar;
    }

    public abstract b a(e eVar);

    public abstract a a(int i, a aVar);

    public e a() {
        return this.f125a;
    }

    public abstract com.google.zxing.common.b b();
}
