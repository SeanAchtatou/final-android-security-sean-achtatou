package com.google.zxing.a.b;

import com.google.zxing.NotFoundException;
import com.google.zxing.common.b;
import com.google.zxing.common.e;
import com.google.zxing.common.i;
import com.google.zxing.common.l;
import com.google.zxing.j;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private static final Integer[] f123a = {new Integer(0), new Integer(1), new Integer(2), new Integer(3), new Integer(4)};
    private final b b;
    private final com.google.zxing.common.a.a c;

    public a(b bVar) {
        this.b = bVar;
        this.c = new com.google.zxing.common.a.a(bVar);
    }

    private static int a(float f) {
        return (int) (0.5f + f);
    }

    private static int a(j jVar, j jVar2) {
        return a((float) Math.sqrt((double) (((jVar.a() - jVar2.a()) * (jVar.a() - jVar2.a())) + ((jVar.b() - jVar2.b()) * (jVar.b() - jVar2.b())))));
    }

    private static b a(b bVar, j jVar, j jVar2, j jVar3, j jVar4, int i) {
        return l.a().a(bVar, i, 0.5f, 0.5f, ((float) i) - 0.5f, 0.5f, ((float) i) - 0.5f, ((float) i) - 0.5f, 0.5f, ((float) i) - 0.5f, jVar.a(), jVar.b(), jVar4.a(), jVar4.b(), jVar3.a(), jVar3.b(), jVar2.a(), jVar2.b());
    }

    private j a(j jVar, j jVar2, j jVar3, j jVar4, int i) {
        float a2 = ((float) a(jVar, jVar2)) / ((float) i);
        int a3 = a(jVar3, jVar4);
        j jVar5 = new j((((jVar4.a() - jVar3.a()) / ((float) a3)) * a2) + jVar4.a(), (a2 * ((jVar4.b() - jVar3.b()) / ((float) a3))) + jVar4.b());
        float a4 = ((float) a(jVar, jVar2)) / ((float) i);
        int a5 = a(jVar2, jVar4);
        j jVar6 = new j((((jVar4.a() - jVar2.a()) / ((float) a5)) * a4) + jVar4.a(), (a4 * ((jVar4.b() - jVar2.b()) / ((float) a5))) + jVar4.b());
        if (a(jVar5)) {
            return !a(jVar6) ? jVar5 : Math.abs(b(jVar3, jVar5).c() - b(jVar2, jVar5).c()) <= Math.abs(b(jVar3, jVar6).c() - b(jVar2, jVar6).c()) ? jVar5 : jVar6;
        }
        if (a(jVar6)) {
            return jVar6;
        }
        return null;
    }

    private static void a(Hashtable hashtable, j jVar) {
        Integer num = (Integer) hashtable.get(jVar);
        hashtable.put(jVar, num == null ? f123a[1] : f123a[num.intValue() + 1]);
    }

    private boolean a(j jVar) {
        return jVar.a() >= 0.0f && jVar.a() < ((float) this.b.f158a) && jVar.b() > 0.0f && jVar.b() < ((float) this.b.b);
    }

    private c b(j jVar, j jVar2) {
        int i;
        int a2 = (int) jVar.a();
        int b2 = (int) jVar.b();
        int a3 = (int) jVar2.a();
        int b3 = (int) jVar2.b();
        boolean z = Math.abs(b3 - b2) > Math.abs(a3 - a2);
        if (!z) {
            int i2 = b3;
            b3 = a3;
            a3 = i2;
            int i3 = b2;
            b2 = a2;
            a2 = i3;
        }
        int abs = Math.abs(b3 - b2);
        int abs2 = Math.abs(a3 - a2);
        int i4 = (-abs) >> 1;
        int i5 = a2 < a3 ? 1 : -1;
        int i6 = b2 < b3 ? 1 : -1;
        int i7 = 0;
        boolean a4 = this.b.a(z ? a2 : b2, z ? b2 : a2);
        int i8 = a2;
        int i9 = i4;
        while (true) {
            if (b2 == b3) {
                i = i7;
                break;
            }
            boolean a5 = this.b.a(z ? i8 : b2, z ? b2 : i8);
            if (a5 != a4) {
                i7++;
                a4 = a5;
            }
            int i10 = i9 + abs2;
            if (i10 > 0) {
                if (i8 == a3) {
                    i = i7;
                    break;
                }
                i8 += i5;
                i10 -= abs;
            }
            b2 += i6;
            i9 = i10;
        }
        return new c(jVar, jVar2, i, null);
    }

    public i a() {
        j jVar;
        j jVar2;
        j[] a2 = this.c.a();
        j jVar3 = a2[0];
        j jVar4 = a2[1];
        j jVar5 = a2[2];
        j jVar6 = a2[3];
        Vector vector = new Vector(4);
        vector.addElement(b(jVar3, jVar4));
        vector.addElement(b(jVar3, jVar5));
        vector.addElement(b(jVar4, jVar6));
        vector.addElement(b(jVar5, jVar6));
        e.a(vector, new d(null));
        c cVar = (c) vector.elementAt(0);
        c cVar2 = (c) vector.elementAt(1);
        Hashtable hashtable = new Hashtable();
        a(hashtable, cVar.a());
        a(hashtable, cVar.b());
        a(hashtable, cVar2.a());
        a(hashtable, cVar2.b());
        j jVar7 = null;
        j jVar8 = null;
        j jVar9 = null;
        Enumeration keys = hashtable.keys();
        while (keys.hasMoreElements()) {
            j jVar10 = (j) keys.nextElement();
            if (((Integer) hashtable.get(jVar10)).intValue() == 2) {
                jVar = jVar10;
                jVar10 = jVar9;
                jVar2 = jVar7;
            } else if (jVar7 == null) {
                jVar = jVar8;
                j jVar11 = jVar9;
                jVar2 = jVar10;
                jVar10 = jVar11;
            } else {
                jVar = jVar8;
                jVar2 = jVar7;
            }
            jVar8 = jVar;
            jVar7 = jVar2;
            jVar9 = jVar10;
        }
        if (jVar7 == null || jVar8 == null || jVar9 == null) {
            throw NotFoundException.a();
        }
        j[] jVarArr = {jVar7, jVar8, jVar9};
        j.a(jVarArr);
        j jVar12 = jVarArr[0];
        j jVar13 = jVarArr[1];
        j jVar14 = jVarArr[2];
        j jVar15 = !hashtable.containsKey(jVar3) ? jVar3 : !hashtable.containsKey(jVar4) ? jVar4 : !hashtable.containsKey(jVar5) ? jVar5 : jVar6;
        int min = Math.min(b(jVar14, jVar15).c(), b(jVar12, jVar15).c());
        if ((min & 1) == 1) {
            min++;
        }
        j a3 = a(jVar13, jVar12, jVar14, jVar15, min + 2);
        if (a3 == null) {
            a3 = jVar15;
        }
        int max = Math.max(b(jVar14, a3).c(), b(jVar12, a3).c()) + 1;
        if ((max & 1) == 1) {
            max++;
        }
        return new i(a(this.b, jVar14, jVar13, jVar12, a3, max), new j[]{jVar14, jVar13, jVar12, a3});
    }
}
