package com.google.zxing.a.a;

final class b {

    /* renamed from: a  reason: collision with root package name */
    private final int f117a;
    private final byte[] b;

    private b(int i, byte[] bArr) {
        this.f117a = i;
        this.b = bArr;
    }

    static b[] a(byte[] bArr, e eVar) {
        h g = eVar.g();
        g[] b2 = g.b();
        int i = 0;
        for (g a2 : b2) {
            i += a2.a();
        }
        b[] bVarArr = new b[i];
        int i2 = 0;
        for (g gVar : b2) {
            int i3 = 0;
            while (i3 < gVar.a()) {
                int b3 = gVar.b();
                bVarArr[i2] = new b(b3, new byte[(g.a() + b3)]);
                i3++;
                i2++;
            }
        }
        int length = bVarArr[0].b.length - g.a();
        int i4 = length - 1;
        int i5 = 0;
        for (int i6 = 0; i6 < i4; i6++) {
            int i7 = 0;
            while (i7 < i2) {
                bVarArr[i7].b[i6] = bArr[i5];
                i7++;
                i5++;
            }
        }
        boolean z = eVar.a() == 24;
        int i8 = z ? 8 : i2;
        int i9 = 0;
        while (i9 < i8) {
            bVarArr[i9].b[length - 1] = bArr[i5];
            i9++;
            i5++;
        }
        int length2 = bVarArr[0].b.length;
        int i10 = i5;
        while (length < length2) {
            int i11 = 0;
            int i12 = i10;
            while (i11 < i2) {
                bVarArr[i11].b[(!z || i11 <= 7) ? length : length - 1] = bArr[i12];
                i11++;
                i12++;
            }
            length++;
            i10 = i12;
        }
        if (i10 == bArr.length) {
            return bVarArr;
        }
        throw new IllegalArgumentException();
    }

    /* access modifiers changed from: package-private */
    public int a() {
        return this.f117a;
    }

    /* access modifiers changed from: package-private */
    public byte[] b() {
        return this.b;
    }
}
