package com.google.zxing.b.a.a.a;

import com.google.zxing.NotFoundException;
import com.tencent.assistant.st.STConst;
import com.tencent.connect.common.Constants;

final class r {

    /* renamed from: a  reason: collision with root package name */
    private static final Object f132a = new Object();
    private static final Object[][] b = {new Object[]{STConst.ST_STATUS_DEFAULT, new Integer(18)}, new Object[]{"01", new Integer(14)}, new Object[]{"02", new Integer(14)}, new Object[]{Constants.VIA_REPORT_TYPE_SHARE_TO_QQ, f132a, new Integer(20)}, new Object[]{Constants.VIA_REPORT_TYPE_SHARE_TO_QZONE, new Integer(6)}, new Object[]{Constants.VIA_REPORT_TYPE_SET_AVATAR, new Integer(6)}, new Object[]{Constants.VIA_REPORT_TYPE_JOININ_GROUP, new Integer(6)}, new Object[]{Constants.VIA_REPORT_TYPE_WPA_STATE, new Integer(6)}, new Object[]{"17", new Integer(6)}, new Object[]{"20", new Integer(2)}, new Object[]{Constants.VIA_REPORT_TYPE_QQFAVORITES, f132a, new Integer(20)}, new Object[]{Constants.VIA_REPORT_TYPE_DATALINE, f132a, new Integer(29)}, new Object[]{"30", f132a, new Integer(8)}, new Object[]{"37", f132a, new Integer(8)}, new Object[]{"90", f132a, new Integer(30)}, new Object[]{"91", f132a, new Integer(30)}, new Object[]{"92", f132a, new Integer(30)}, new Object[]{"93", f132a, new Integer(30)}, new Object[]{"94", f132a, new Integer(30)}, new Object[]{"95", f132a, new Integer(30)}, new Object[]{"96", f132a, new Integer(30)}, new Object[]{"97", f132a, new Integer(30)}, new Object[]{"98", f132a, new Integer(30)}, new Object[]{"99", f132a, new Integer(30)}};
    private static final Object[][] c = {new Object[]{"240", f132a, new Integer(30)}, new Object[]{"241", f132a, new Integer(30)}, new Object[]{"242", f132a, new Integer(6)}, new Object[]{"250", f132a, new Integer(30)}, new Object[]{"251", f132a, new Integer(30)}, new Object[]{"253", f132a, new Integer(17)}, new Object[]{"254", f132a, new Integer(20)}, new Object[]{"400", f132a, new Integer(30)}, new Object[]{"401", f132a, new Integer(30)}, new Object[]{"402", new Integer(17)}, new Object[]{"403", f132a, new Integer(30)}, new Object[]{"410", new Integer(13)}, new Object[]{"411", new Integer(13)}, new Object[]{"412", new Integer(13)}, new Object[]{"413", new Integer(13)}, new Object[]{"414", new Integer(13)}, new Object[]{"420", f132a, new Integer(20)}, new Object[]{"421", f132a, new Integer(15)}, new Object[]{"422", new Integer(3)}, new Object[]{"423", f132a, new Integer(15)}, new Object[]{"424", new Integer(3)}, new Object[]{"425", new Integer(3)}, new Object[]{"426", new Integer(3)}};
    private static final Object[][] d = {new Object[]{"310", new Integer(6)}, new Object[]{"311", new Integer(6)}, new Object[]{"312", new Integer(6)}, new Object[]{"313", new Integer(6)}, new Object[]{"314", new Integer(6)}, new Object[]{"315", new Integer(6)}, new Object[]{"316", new Integer(6)}, new Object[]{"320", new Integer(6)}, new Object[]{"321", new Integer(6)}, new Object[]{"322", new Integer(6)}, new Object[]{"323", new Integer(6)}, new Object[]{"324", new Integer(6)}, new Object[]{"325", new Integer(6)}, new Object[]{"326", new Integer(6)}, new Object[]{"327", new Integer(6)}, new Object[]{"328", new Integer(6)}, new Object[]{"329", new Integer(6)}, new Object[]{"330", new Integer(6)}, new Object[]{"331", new Integer(6)}, new Object[]{"332", new Integer(6)}, new Object[]{"333", new Integer(6)}, new Object[]{"334", new Integer(6)}, new Object[]{"335", new Integer(6)}, new Object[]{"336", new Integer(6)}, new Object[]{"340", new Integer(6)}, new Object[]{"341", new Integer(6)}, new Object[]{"342", new Integer(6)}, new Object[]{"343", new Integer(6)}, new Object[]{"344", new Integer(6)}, new Object[]{"345", new Integer(6)}, new Object[]{"346", new Integer(6)}, new Object[]{"347", new Integer(6)}, new Object[]{"348", new Integer(6)}, new Object[]{"349", new Integer(6)}, new Object[]{"350", new Integer(6)}, new Object[]{"351", new Integer(6)}, new Object[]{"352", new Integer(6)}, new Object[]{"353", new Integer(6)}, new Object[]{"354", new Integer(6)}, new Object[]{"355", new Integer(6)}, new Object[]{"356", new Integer(6)}, new Object[]{"357", new Integer(6)}, new Object[]{"360", new Integer(6)}, new Object[]{"361", new Integer(6)}, new Object[]{"362", new Integer(6)}, new Object[]{"363", new Integer(6)}, new Object[]{"364", new Integer(6)}, new Object[]{"365", new Integer(6)}, new Object[]{"366", new Integer(6)}, new Object[]{"367", new Integer(6)}, new Object[]{"368", new Integer(6)}, new Object[]{"369", new Integer(6)}, new Object[]{"390", f132a, new Integer(15)}, new Object[]{"391", f132a, new Integer(18)}, new Object[]{"392", f132a, new Integer(15)}, new Object[]{"393", f132a, new Integer(18)}, new Object[]{"703", f132a, new Integer(30)}};
    private static final Object[][] e = {new Object[]{"7001", new Integer(13)}, new Object[]{"7002", f132a, new Integer(30)}, new Object[]{"7003", new Integer(10)}, new Object[]{"8001", new Integer(14)}, new Object[]{"8002", f132a, new Integer(20)}, new Object[]{"8003", f132a, new Integer(30)}, new Object[]{"8004", f132a, new Integer(30)}, new Object[]{"8005", new Integer(6)}, new Object[]{"8006", new Integer(18)}, new Object[]{"8007", f132a, new Integer(30)}, new Object[]{"8008", f132a, new Integer(12)}, new Object[]{"8018", new Integer(18)}, new Object[]{"8020", f132a, new Integer(25)}, new Object[]{"8100", new Integer(6)}, new Object[]{"8101", new Integer(10)}, new Object[]{"8102", new Integer(2)}, new Object[]{"8110", f132a, new Integer(30)}};

    private r() {
    }

    private static String a(int i, int i2, String str) {
        if (str.length() < i) {
            throw NotFoundException.a();
        }
        String substring = str.substring(0, i);
        if (str.length() < i + i2) {
            throw NotFoundException.a();
        }
        return new StringBuffer().append('(').append(substring).append(')').append(str.substring(i, i + i2)).append(a(str.substring(i + i2))).toString();
    }

    static String a(String str) {
        if (str.length() == 0) {
            return Constants.STR_EMPTY;
        }
        if (str.length() < 2) {
            throw NotFoundException.a();
        }
        String substring = str.substring(0, 2);
        for (int i = 0; i < b.length; i++) {
            if (b[i][0].equals(substring)) {
                return b[i][1] == f132a ? b(2, ((Integer) b[i][2]).intValue(), str) : a(2, ((Integer) b[i][1]).intValue(), str);
            }
        }
        if (str.length() < 3) {
            throw NotFoundException.a();
        }
        String substring2 = str.substring(0, 3);
        for (int i2 = 0; i2 < c.length; i2++) {
            if (c[i2][0].equals(substring2)) {
                return c[i2][1] == f132a ? b(3, ((Integer) c[i2][2]).intValue(), str) : a(3, ((Integer) c[i2][1]).intValue(), str);
            }
        }
        for (int i3 = 0; i3 < d.length; i3++) {
            if (d[i3][0].equals(substring2)) {
                return d[i3][1] == f132a ? b(4, ((Integer) d[i3][2]).intValue(), str) : a(4, ((Integer) d[i3][1]).intValue(), str);
            }
        }
        if (str.length() < 4) {
            throw NotFoundException.a();
        }
        String substring3 = str.substring(0, 4);
        for (int i4 = 0; i4 < e.length; i4++) {
            if (e[i4][0].equals(substring3)) {
                return e[i4][1] == f132a ? b(4, ((Integer) e[i4][2]).intValue(), str) : a(4, ((Integer) e[i4][1]).intValue(), str);
            }
        }
        throw NotFoundException.a();
    }

    private static String b(int i, int i2, String str) {
        String substring = str.substring(0, i);
        int length = str.length() < i + i2 ? str.length() : i + i2;
        return new StringBuffer().append('(').append(substring).append(')').append(str.substring(i, length)).append(a(str.substring(length))).toString();
    }
}
