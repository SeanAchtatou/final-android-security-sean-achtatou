package android.content.b.a;

import android.content.pm.a;

/* compiled from: ProGuard */
public class e {
    public static boolean a(a aVar) {
        if (aVar == null) {
            return false;
        }
        if (aVar.k != null && aVar.k.startsWith("@")) {
            return true;
        }
        if (aVar.b != null && aVar.b.startsWith("@")) {
            return true;
        }
        if (aVar.f == null || !aVar.f.startsWith("@")) {
            return false;
        }
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.content.b.a.b.a(android.content.pm.a, boolean):void
     arg types: [android.content.pm.a, int]
     candidates:
      android.content.b.a.b.a(byte[], int):int
      android.content.b.a.b.a(int, android.content.b.a.h):java.lang.String
      android.content.b.a.b.a(android.content.b.a.c, int):void
      android.content.b.a.b.a(android.content.b.a.c, android.content.pm.a):void
      android.content.b.a.b.a(android.content.pm.a, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0071  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(android.content.pm.a r7, java.io.InputStream r8) {
        /*
            r1 = 0
            android.content.b.a.b r5 = new android.content.b.a.b
            r5.<init>(r8)
            java.lang.String r0 = r7.k     // Catch:{ Exception -> 0x0088 }
            if (r0 == 0) goto L_0x00d5
            java.lang.String r0 = r7.k     // Catch:{ Exception -> 0x0088 }
            java.lang.String r2 = "@"
            boolean r0 = r0.startsWith(r2)     // Catch:{ Exception -> 0x0088 }
            if (r0 == 0) goto L_0x00d5
            java.lang.String r0 = r7.k     // Catch:{ Exception -> 0x0088 }
            r2 = 1
            java.lang.String r0 = r0.substring(r2)     // Catch:{ Exception -> 0x0088 }
            r2 = 10
            int r3 = java.lang.Integer.parseInt(r0, r2)     // Catch:{ Exception -> 0x0088 }
            r5.a(r3)     // Catch:{ Exception -> 0x00c1 }
        L_0x0024:
            java.lang.String r0 = r7.b     // Catch:{ Exception -> 0x00c1 }
            if (r0 == 0) goto L_0x00d2
            java.lang.String r0 = r7.b     // Catch:{ Exception -> 0x00c1 }
            java.lang.String r2 = "@"
            boolean r0 = r0.startsWith(r2)     // Catch:{ Exception -> 0x00c1 }
            if (r0 == 0) goto L_0x00d2
            java.lang.String r0 = r7.b     // Catch:{ Exception -> 0x00c1 }
            r2 = 1
            java.lang.String r0 = r0.substring(r2)     // Catch:{ Exception -> 0x00c1 }
            r2 = 10
            int r2 = java.lang.Integer.parseInt(r0, r2)     // Catch:{ Exception -> 0x00c1 }
            r5.a(r2)     // Catch:{ Exception -> 0x00c6 }
        L_0x0042:
            java.lang.String r0 = r7.f     // Catch:{ Exception -> 0x00c6 }
            if (r0 == 0) goto L_0x00d0
            java.lang.String r0 = r7.f     // Catch:{ Exception -> 0x00c6 }
            java.lang.String r4 = "@"
            boolean r0 = r0.startsWith(r4)     // Catch:{ Exception -> 0x00c6 }
            if (r0 == 0) goto L_0x00d0
            java.lang.String r0 = r7.f     // Catch:{ Exception -> 0x00c6 }
            r4 = 1
            java.lang.String r0 = r0.substring(r4)     // Catch:{ Exception -> 0x00c6 }
            r4 = 10
            int r0 = java.lang.Integer.parseInt(r0, r4)     // Catch:{ Exception -> 0x00c6 }
            r5.a(r0)     // Catch:{ Exception -> 0x00cb }
        L_0x0060:
            r6 = r2
            r2 = r0
            r0 = r6
        L_0x0063:
            r5.a()
            r4 = 1
            r5.a(r7, r4)     // Catch:{ Exception -> 0x0091 }
        L_0x006a:
            android.content.b.a.f r4 = r5.c
            android.content.b.a.g[] r4 = r4.f25a
            int r4 = r4.length
            if (r1 >= r4) goto L_0x00c0
            android.content.b.a.f r4 = r5.c
            android.content.b.a.g[] r4 = r4.f25a
            r4 = r4[r1]
            int r4 = r4.f26a
            if (r4 != r3) goto L_0x0096
            android.content.b.a.f r4 = r5.c
            android.content.b.a.g[] r4 = r4.f25a
            r4 = r4[r1]
            java.lang.String r4 = r4.b
            r7.l = r4
        L_0x0085:
            int r1 = r1 + 1
            goto L_0x006a
        L_0x0088:
            r0 = move-exception
            r4 = r0
            r2 = r1
            r3 = r1
            r0 = r1
        L_0x008d:
            r4.printStackTrace()
            goto L_0x0063
        L_0x0091:
            r4 = move-exception
            r4.printStackTrace()
            goto L_0x006a
        L_0x0096:
            android.content.b.a.f r4 = r5.c
            android.content.b.a.g[] r4 = r4.f25a
            r4 = r4[r1]
            int r4 = r4.f26a
            if (r4 != r2) goto L_0x00ab
            android.content.b.a.f r4 = r5.c
            android.content.b.a.g[] r4 = r4.f25a
            r4 = r4[r1]
            java.lang.String r4 = r4.b
            r7.g = r4
            goto L_0x0085
        L_0x00ab:
            android.content.b.a.f r4 = r5.c
            android.content.b.a.g[] r4 = r4.f25a
            r4 = r4[r1]
            int r4 = r4.f26a
            if (r4 != r0) goto L_0x0085
            android.content.b.a.f r4 = r5.c
            android.content.b.a.g[] r4 = r4.f25a
            r4 = r4[r1]
            java.lang.String r4 = r4.b
            r7.b = r4
            goto L_0x0085
        L_0x00c0:
            return
        L_0x00c1:
            r0 = move-exception
            r4 = r0
            r2 = r1
            r0 = r1
            goto L_0x008d
        L_0x00c6:
            r0 = move-exception
            r4 = r0
            r0 = r2
            r2 = r1
            goto L_0x008d
        L_0x00cb:
            r4 = move-exception
            r6 = r2
            r2 = r0
            r0 = r6
            goto L_0x008d
        L_0x00d0:
            r0 = r1
            goto L_0x0060
        L_0x00d2:
            r2 = r1
            goto L_0x0042
        L_0x00d5:
            r3 = r1
            goto L_0x0024
        */
        throw new UnsupportedOperationException("Method not decompiled: android.content.b.a.e.a(android.content.pm.a, java.io.InputStream):void");
    }
}
