package android.content.pm;

import android.os.IBinder;
import android.os.Parcel;

/* compiled from: ProGuard */
class d implements IPackageStatsObserver {

    /* renamed from: a  reason: collision with root package name */
    private IBinder f30a;

    d(IBinder iBinder) {
        this.f30a = iBinder;
    }

    public IBinder asBinder() {
        return this.f30a;
    }

    public void onGetStatsCompleted(PackageStats packageStats, boolean z) {
        int i = 1;
        Parcel obtain = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("android.content.pm.IPackageStatsObserver");
            if (packageStats != null) {
                obtain.writeInt(1);
                packageStats.writeToParcel(obtain, 0);
            } else {
                obtain.writeInt(0);
            }
            if (!z) {
                i = 0;
            }
            obtain.writeInt(i);
            this.f30a.transact(1, obtain, null, 1);
        } finally {
            obtain.recycle();
        }
    }
}
