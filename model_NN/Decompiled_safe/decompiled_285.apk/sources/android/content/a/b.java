package android.content.a;

/* compiled from: ProGuard */
final class b {

    /* renamed from: a  reason: collision with root package name */
    private int[] f19a = new int[32];
    private int b;
    private int c;
    private int d;

    public final void a() {
        this.b = 0;
        this.c = 0;
        this.d = 0;
    }

    public final int b() {
        if (this.b == 0) {
            return 0;
        }
        return this.f19a[this.b - 1];
    }

    public final int a(int i) {
        int i2 = 0;
        if (this.b != 0 && i >= 0) {
            if (i > this.d) {
                i = this.d;
            }
            int i3 = 0;
            while (i != 0) {
                int i4 = this.f19a[i3];
                i--;
                i3 = (i4 * 2) + 2 + i3;
                i2 += i4;
            }
        }
        return i2;
    }

    public final void a(int i, int i2) {
        if (this.d == 0) {
            e();
        }
        e(2);
        int i3 = this.b - 1;
        int i4 = this.f19a[i3];
        this.f19a[(i3 - 1) - (i4 * 2)] = i4 + 1;
        this.f19a[i3] = i;
        this.f19a[i3 + 1] = i2;
        this.f19a[i3 + 2] = i4 + 1;
        this.b += 2;
        this.c++;
    }

    public final boolean c() {
        int i;
        int i2;
        if (this.b == 0 || (i2 = this.f19a[this.b - 1]) == 0) {
            return false;
        }
        int i3 = i2 - 1;
        int i4 = i - 2;
        this.f19a[i4] = i3;
        this.f19a[i4 - ((i3 * 2) + 1)] = i3;
        this.b -= 2;
        this.c--;
        return true;
    }

    public final int b(int i) {
        return b(i, true);
    }

    public final int c(int i) {
        return b(i, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.content.a.b.a(int, boolean):int
     arg types: [int, int]
     candidates:
      android.content.a.b.a(int, int):void
      android.content.a.b.a(int, boolean):int */
    public final int d(int i) {
        return a(i, false);
    }

    public final int d() {
        return this.d;
    }

    public final void e() {
        e(2);
        int i = this.b;
        this.f19a[i] = 0;
        this.f19a[i + 1] = 0;
        this.b += 2;
        this.d++;
    }

    public final void f() {
        if (this.b != 0) {
            int i = this.b - 1;
            int i2 = this.f19a[i];
            if ((i - 1) - (i2 * 2) != 0) {
                this.b -= (i2 * 2) + 2;
                this.c -= i2;
                this.d--;
            }
        }
    }

    private void e(int i) {
        int length = this.f19a.length - this.b;
        if (length <= i) {
            int[] iArr = new int[((length + this.f19a.length) * 2)];
            System.arraycopy(this.f19a, 0, iArr, 0, this.b);
            this.f19a = iArr;
        }
    }

    private final int a(int i, boolean z) {
        if (this.b == 0) {
            return -1;
        }
        int i2 = this.b - 1;
        for (int i3 = this.d; i3 != 0; i3--) {
            i2 -= 2;
            for (int i4 = this.f19a[i2]; i4 != 0; i4--) {
                if (z) {
                    if (this.f19a[i2] == i) {
                        return this.f19a[i2 + 1];
                    }
                } else if (this.f19a[i2 + 1] == i) {
                    return this.f19a[i2];
                }
                i2 -= 2;
            }
        }
        return -1;
    }

    private final int b(int i, boolean z) {
        if (this.b == 0 || i < 0) {
            return -1;
        }
        int i2 = 0;
        int i3 = this.d;
        while (i3 != 0) {
            int i4 = this.f19a[i2];
            if (i >= i4) {
                i -= i4;
                i2 += (i4 * 2) + 2;
                i3--;
            } else {
                int i5 = (i * 2) + 1 + i2;
                if (!z) {
                    i5++;
                }
                return this.f19a[i5];
            }
        }
        return -1;
    }
}
