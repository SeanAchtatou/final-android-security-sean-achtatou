package android.support.v4.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.os.Build;

/* compiled from: ProGuard */
public class d {
    private static final g b;

    /* renamed from: a  reason: collision with root package name */
    private Object f103a;

    static {
        if (Build.VERSION.SDK_INT >= 14) {
            b = new f();
        } else {
            b = new e();
        }
    }

    public d(Context context) {
        this.f103a = b.a(context);
    }

    public void a(int i, int i2) {
        b.a(this.f103a, i, i2);
    }

    public boolean a() {
        return b.a(this.f103a);
    }

    public void b() {
        b.b(this.f103a);
    }

    public boolean a(float f) {
        return b.a(this.f103a, f);
    }

    public boolean c() {
        return b.c(this.f103a);
    }

    public boolean a(Canvas canvas) {
        return b.a(this.f103a, canvas);
    }
}
