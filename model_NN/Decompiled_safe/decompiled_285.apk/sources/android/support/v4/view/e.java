package android.support.v4.view;

import android.os.Bundle;
import android.support.v4.view.AccessibilityDelegateCompatJellyBean;
import android.support.v4.view.a.a;
import android.support.v4.view.a.g;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;

/* compiled from: ProGuard */
class e implements AccessibilityDelegateCompatJellyBean.AccessibilityDelegateBridgeJellyBean {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AccessibilityDelegateCompat f90a;
    final /* synthetic */ d b;

    e(d dVar, AccessibilityDelegateCompat accessibilityDelegateCompat) {
        this.b = dVar;
        this.f90a = accessibilityDelegateCompat;
    }

    public boolean dispatchPopulateAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
        return this.f90a.dispatchPopulateAccessibilityEvent(view, accessibilityEvent);
    }

    public void onInitializeAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
        this.f90a.onInitializeAccessibilityEvent(view, accessibilityEvent);
    }

    public void onInitializeAccessibilityNodeInfo(View view, Object obj) {
        this.f90a.onInitializeAccessibilityNodeInfo(view, new a(obj));
    }

    public void onPopulateAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
        this.f90a.onPopulateAccessibilityEvent(view, accessibilityEvent);
    }

    public boolean onRequestSendAccessibilityEvent(ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
        return this.f90a.onRequestSendAccessibilityEvent(viewGroup, view, accessibilityEvent);
    }

    public void sendAccessibilityEvent(View view, int i) {
        this.f90a.sendAccessibilityEvent(view, i);
    }

    public void sendAccessibilityEventUnchecked(View view, AccessibilityEvent accessibilityEvent) {
        this.f90a.sendAccessibilityEventUnchecked(view, accessibilityEvent);
    }

    public Object getAccessibilityNodeProvider(View view) {
        g accessibilityNodeProvider = this.f90a.getAccessibilityNodeProvider(view);
        if (accessibilityNodeProvider != null) {
            return accessibilityNodeProvider.a();
        }
        return null;
    }

    public boolean performAccessibilityAction(View view, int i, Bundle bundle) {
        return this.f90a.performAccessibilityAction(view, i, bundle);
    }
}
