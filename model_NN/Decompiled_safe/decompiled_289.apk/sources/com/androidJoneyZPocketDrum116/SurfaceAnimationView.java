package com.androidJoneyZPocketDrum116;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import com.admob.android.ads.AdView;
import java.util.HashMap;

public class SurfaceAnimationView extends SurfaceView {
    MediaPlayer BigSound;
    /* access modifiers changed from: private */
    public DrawRunning drawRunning;
    private AdView mAd;
    Matrix mMatrix = new Matrix();
    Matrix mMatrix2 = new Matrix();
    /* access modifiers changed from: private */
    public Bitmap myBg1;
    /* access modifiers changed from: private */
    public Bitmap myBg2;
    /* access modifiers changed from: private */
    public Bitmap myBg3;
    /* access modifiers changed from: private */
    public Bitmap myBg4;
    /* access modifiers changed from: private */
    public Bitmap myBg5;
    /* access modifiers changed from: private */
    public Bitmap myBg6;
    /* access modifiers changed from: private */
    public Bitmap myBg7;
    /* access modifiers changed from: private */
    public Bitmap myBg8;
    private SoundPool soundPool = new SoundPool(100, 3, 100);
    private HashMap<Integer, Integer> soundPoolMap = new HashMap<>();
    int streamVolume;

    public void play(int sound, int uLoop) {
        this.soundPool.play(this.soundPoolMap.get(Integer.valueOf(sound)).intValue(), (float) this.streamVolume, (float) this.streamVolume, 1, uLoop, 1.0f);
    }

    public void PlayBgSound() {
        this.BigSound.setLooping(false);
        this.BigSound.start();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public SurfaceAnimationView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.streamVolume = ((AudioManager) context.getSystemService("audio")).getStreamVolume(3);
        this.soundPoolMap.put(1, Integer.valueOf(this.soundPool.load(context, R.raw.s36, 1)));
        this.soundPoolMap.put(2, Integer.valueOf(this.soundPool.load(context, R.raw.s64, 1)));
        this.soundPoolMap.put(3, Integer.valueOf(this.soundPool.load(context, R.raw.s31, 1)));
        this.soundPoolMap.put(5, Integer.valueOf(this.soundPool.load(context, R.raw.s17, 1)));
        this.soundPoolMap.put(6, Integer.valueOf(this.soundPool.load(context, R.raw.s20, 1)));
        this.soundPoolMap.put(7, Integer.valueOf(this.soundPool.load(context, R.raw.s23, 1)));
        this.soundPoolMap.put(8, Integer.valueOf(this.soundPool.load(context, R.raw.s26, 1)));
        this.soundPoolMap.put(9, Integer.valueOf(this.soundPool.load(context, R.raw.s44, 1)));
        this.BigSound = MediaPlayer.create(context, R.raw.s31);
        this.mMatrix.reset();
        this.mMatrix.setScale(0.6f, 0.6f);
        this.mMatrix2.reset();
        this.mMatrix2.setScale(0.5f, 0.5f);
        Bitmap background = BitmapFactory.decodeResource(getResources(), R.drawable.main);
        this.myBg1 = Bitmap.createBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.a3), 0, 0, 129, 59, this.mMatrix, true);
        this.myBg2 = Bitmap.createBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.x3), 0, 0, 170, 40, this.mMatrix2, true);
        this.myBg3 = Bitmap.createBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.z2), 0, 0, 166, 32, this.mMatrix2, true);
        this.myBg4 = BitmapFactory.decodeResource(getResources(), R.drawable.f1);
        this.myBg5 = BitmapFactory.decodeResource(getResources(), R.drawable.a2);
        this.myBg6 = BitmapFactory.decodeResource(getResources(), R.drawable.a2);
        this.myBg7 = BitmapFactory.decodeResource(getResources(), R.drawable.b3);
        this.myBg8 = BitmapFactory.decodeResource(getResources(), R.drawable.h1);
        Bitmap[] bitmapArr = new Bitmap[14];
        Bitmap[] bg1 = {Bitmap.createBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.a1), 0, 0, 129, 59, this.mMatrix, true), Bitmap.createBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.a2), 0, 0, 129, 59, this.mMatrix, true), Bitmap.createBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.a3), 0, 0, 129, 59, this.mMatrix, true), Bitmap.createBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.a4), 0, 0, 129, 59, this.mMatrix, true), Bitmap.createBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.a5), 0, 0, 129, 59, this.mMatrix, true), Bitmap.createBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.a6), 0, 0, 129, 59, this.mMatrix, true), Bitmap.createBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.a5), 0, 0, 129, 59, this.mMatrix, true), Bitmap.createBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.a4), 0, 0, 129, 59, this.mMatrix, true), Bitmap.createBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.a3), 0, 0, 129, 59, this.mMatrix, true), Bitmap.createBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.a2), 0, 0, 129, 59, this.mMatrix, true), Bitmap.createBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.a1), 0, 0, 129, 59, this.mMatrix, true), Bitmap.createBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.a2), 0, 0, 129, 59, this.mMatrix, true)};
        Bitmap[] bigB = {BitmapFactory.decodeResource(getResources(), R.drawable.b1), BitmapFactory.decodeResource(getResources(), R.drawable.b2), BitmapFactory.decodeResource(getResources(), R.drawable.b3), BitmapFactory.decodeResource(getResources(), R.drawable.b4), BitmapFactory.decodeResource(getResources(), R.drawable.b5), BitmapFactory.decodeResource(getResources(), R.drawable.b6), BitmapFactory.decodeResource(getResources(), R.drawable.b5), BitmapFactory.decodeResource(getResources(), R.drawable.b4), BitmapFactory.decodeResource(getResources(), R.drawable.b3), BitmapFactory.decodeResource(getResources(), R.drawable.b2), BitmapFactory.decodeResource(getResources(), R.drawable.b3), BitmapFactory.decodeResource(getResources(), R.drawable.b4)};
        Bitmap[] litileB1 = {Bitmap.createBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.x4), 0, 0, 169, 40, this.mMatrix2, true), Bitmap.createBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.x2), 0, 0, 169, 40, this.mMatrix2, true), Bitmap.createBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.x3), 0, 0, 169, 40, this.mMatrix2, true), Bitmap.createBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.x4), 0, 0, 169, 40, this.mMatrix2, true), Bitmap.createBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.x3), 0, 0, 169, 40, this.mMatrix2, true), Bitmap.createBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.x3), 0, 0, 169, 40, this.mMatrix2, true)};
        Bitmap[] litileB2 = {Bitmap.createBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.z3), 0, 0, 166, 32, this.mMatrix2, true), Bitmap.createBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.z1), 0, 0, 166, 32, this.mMatrix2, true), Bitmap.createBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.z2), 0, 0, 166, 32, this.mMatrix2, true), Bitmap.createBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.z2), 0, 0, 166, 32, this.mMatrix2, true), Bitmap.createBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.z2), 0, 0, 166, 32, this.mMatrix2, true), Bitmap.createBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.z2), 0, 0, 166, 32, this.mMatrix2, true)};
        final Bitmap bitmap = background;
        final Bitmap[] bitmapArr2 = new Bitmap[8];
        getHolder().addCallback(new SurfaceHolder.Callback() {
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
                holder.setFixedSize(width, height);
            }

            public void surfaceCreated(SurfaceHolder holder) {
                SurfaceAnimationView.this.drawRunning = new DrawRunning(holder);
                SurfaceAnimationView.this.drawRunning.addAnimationDraw(new AnimationDraw(0.0f, 0.0f, bitmap, -1));
                SurfaceAnimationView.this.drawRunning.addAnimationDraw(new AnimationDraw(50.0f, 50.0f, bitmapArr2, 100, true));
                SurfaceAnimationView.this.drawRunning.addMyBg(SurfaceAnimationView.this.myBg1, SurfaceAnimationView.this.myBg2, SurfaceAnimationView.this.myBg3, SurfaceAnimationView.this.myBg4, SurfaceAnimationView.this.myBg5, SurfaceAnimationView.this.myBg6, SurfaceAnimationView.this.myBg7, SurfaceAnimationView.this.myBg8);
                new Thread(SurfaceAnimationView.this.drawRunning).start();
            }

            public void surfaceDestroyed(SurfaceHolder holder) {
                SurfaceAnimationView.this.drawRunning.stopDrawing();
                synchronized (holder) {
                }
            }
        });
        final Bitmap[] bitmapArr3 = litileB1;
        final Bitmap[] bitmapArr4 = litileB2;
        final Bitmap[] bitmapArr5 = bg1;
        final Bitmap[] bitmapArr6 = bigB;
        setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() != 0) {
                    return true;
                }
                if (event.getX() > 157.0f && event.getX() < 236.0f && event.getY() > 60.0f && event.getY() < 116.0f) {
                    SurfaceAnimationView.this.play(7, 0);
                }
                if (event.getX() > 248.0f && event.getX() < 334.0f && event.getY() > 62.0f && event.getY() < 122.0f) {
                    SurfaceAnimationView.this.play(8, 0);
                }
                if (event.getX() > 113.0f && event.getX() < 207.0f && event.getY() > 136.0f && event.getY() < 189.0f) {
                    SurfaceAnimationView.this.play(5, 0);
                }
                if (event.getX() > 302.0f && event.getX() < 406.0f && event.getY() > 145.0f && event.getY() < 211.0f) {
                    SurfaceAnimationView.this.play(6, 0);
                }
                if (event.getX() > 199.0f && event.getX() < 293.0f && event.getY() > 154.0f && event.getY() < 256.0f) {
                    SurfaceAnimationView.this.play(9, 0);
                }
                if (event.getX() > 18.0f && event.getX() < 94.0f && event.getY() > 89.0f && event.getY() < 116.0f) {
                    SurfaceAnimationView.this.play(2, 0);
                    SurfaceAnimationView.this.drawRunning.addBg21AnimationDraw(new BombAnimationDraw(15.0f, 90.0f, bitmapArr3, 10));
                    SurfaceAnimationView.this.drawRunning.addBg22AnimationDraw(new BombAnimationDraw(15.0f, 100.0f, bitmapArr4, 10));
                }
                if (event.getX() > 105.0f && event.getX() < 175.0f && event.getY() > 10.0f && event.getY() < 40.0f) {
                    SurfaceAnimationView.this.play(1, 0);
                    SurfaceAnimationView.this.drawRunning.addBg1AnimationDraw(new BombAnimationDraw(100.0f, 0.0f, bitmapArr5, 10));
                }
                if (event.getX() <= 320.0f || event.getX() >= 460.0f || event.getY() <= 10.0f || event.getY() >= 80.0f) {
                    return true;
                }
                SurfaceAnimationView.this.play(3, 0);
                SurfaceAnimationView.this.drawRunning.addBg3AnimationDraw(new BombAnimationDraw(350.0f, 0.0f, bitmapArr6, 10));
                return true;
            }
        });
    }

    public void setAd() {
        this.mAd.setVisibility(0);
        AlphaAnimation animation = new AlphaAnimation(0.0f, 1.0f);
        animation.setDuration(400);
        animation.setFillAfter(true);
        animation.setInterpolator(new AccelerateInterpolator());
        this.mAd.startAnimation(animation);
    }

    public void setTextView(AdView adView) {
        this.mAd = adView;
        this.mAd.setVisibility(0);
    }

    public DrawRunning getThread() {
        return this.drawRunning;
    }
}
