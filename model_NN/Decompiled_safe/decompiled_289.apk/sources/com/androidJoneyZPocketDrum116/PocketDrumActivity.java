package com.androidJoneyZPocketDrum116;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import com.admob.android.ads.AdManager;
import com.admob.android.ads.AdView;
import com.admob.android.ads.SimpleAdListener;

public class PocketDrumActivity extends Activity {
    public AdView ad;
    com.wiyun.ad.AdView adWiyun;
    private View.OnFocusChangeListener onFocusC = new View.OnFocusChangeListener() {
        public void onFocusChange(View v, boolean state) {
            Log.e("OnonFocusChange", "=" + v + state);
        }
    };
    private DrawRunning pocketDrum;
    /* access modifiers changed from: private */
    public SurfaceAnimationView pocketDrumSurfaceview;

    private void initAdContainer() {
        this.adWiyun = new com.wiyun.ad.AdView(this);
        this.adWiyun.setResId("cd4c5b8df3233430");
        ((LinearLayout) findViewById(R.id.ad_container)).addView(this.adWiyun, new LinearLayout.LayoutParams(-1, -2));
        this.adWiyun.setRefreshInterval(30);
        this.adWiyun.requestAd();
    }

    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        this.pocketDrumSurfaceview = (SurfaceAnimationView) findViewById(R.id.pocketDrumSurfaceView);
        this.pocketDrum = this.pocketDrumSurfaceview.getThread();
        AdManager.setPublisherId("a14ca66aad55dab");
        this.pocketDrumSurfaceview.setOnFocusChangeListener(this.onFocusC);
        this.ad = (AdView) findViewById(R.id.ad);
        this.pocketDrumSurfaceview.setTextView(this.ad);
        this.pocketDrumSurfaceview.setAd();
        initAdContainer();
        this.ad.setAdListener(new LunarLanderListener(this, null));
    }

    private class LunarLanderListener extends SimpleAdListener {
        private LunarLanderListener() {
        }

        /* synthetic */ LunarLanderListener(PocketDrumActivity pocketDrumActivity, LunarLanderListener lunarLanderListener) {
            this();
        }

        public void onFailedToReceiveAd(AdView adView) {
            super.onFailedToReceiveAd(adView);
            Log.d("ad", "=" + PocketDrumActivity.this.ad);
            PocketDrumActivity.this.ad.requestFreshAd();
            Log.d("Lunar", "onFailedToReceiveAd");
        }

        public void onFailedToReceiveRefreshedAd(AdView adView) {
            super.onFailedToReceiveRefreshedAd(adView);
            PocketDrumActivity.this.ad.requestFreshAd();
            Log.d("Lunar", "onFailedToReceiveRefreshedAd");
        }

        public void onReceiveAd(AdView adView) {
            super.onReceiveAd(adView);
            Log.d("Lunar", "onReceiveAd");
            PocketDrumActivity.this.pocketDrumSurfaceview.setAd();
            PocketDrumActivity.this.ad.requestFreshAd();
            PocketDrumActivity.this.ad.setRequestInterval(60);
        }

        public void onReceiveRefreshedAd(AdView adView) {
            super.onReceiveRefreshedAd(adView);
            Log.d("Lunar", "onReceiveRefreshedAd");
            PocketDrumActivity.this.pocketDrumSurfaceview.setAd();
            PocketDrumActivity.this.ad.requestFreshAd();
            PocketDrumActivity.this.ad.setRequestInterval(60);
        }
    }
}
