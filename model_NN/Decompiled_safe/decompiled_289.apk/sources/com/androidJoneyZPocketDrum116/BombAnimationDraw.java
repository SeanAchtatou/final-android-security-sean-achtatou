package com.androidJoneyZPocketDrum116;

import android.graphics.Bitmap;

public class BombAnimationDraw extends AnimationDraw {
    public BombAnimationDraw(float x, float y, Bitmap[] bitmaps, long duration) {
        super(x, y, bitmaps, duration, false);
    }
}
