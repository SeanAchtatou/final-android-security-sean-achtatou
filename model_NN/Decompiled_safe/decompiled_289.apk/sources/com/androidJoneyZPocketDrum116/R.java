package com.androidJoneyZPocketDrum116;

public final class R {

    public static final class attr {
        public static final int backgroundColor = 2130771969;
        public static final int keywords = 2130771972;
        public static final int primaryTextColor = 2130771970;
        public static final int refreshInterval = 2130771973;
        public static final int secondaryTextColor = 2130771971;
        public static final int testing = 2130771975;
        public static final int textColor = 2130771974;
        public static final int tileSize = 2130771968;
        public static final int vertical = 2130771976;
    }

    public static final class drawable {
        public static final int a1 = 2130837504;
        public static final int a2 = 2130837505;
        public static final int a3 = 2130837506;
        public static final int a4 = 2130837507;
        public static final int a5 = 2130837508;
        public static final int a6 = 2130837509;
        public static final int b1 = 2130837510;
        public static final int b2 = 2130837511;
        public static final int b3 = 2130837512;
        public static final int b4 = 2130837513;
        public static final int b5 = 2130837514;
        public static final int b6 = 2130837515;
        public static final int bak1 = 2130837516;
        public static final int bak2 = 2130837517;
        public static final int c1 = 2130837518;
        public static final int f1 = 2130837519;
        public static final int h1 = 2130837520;
        public static final int icon = 2130837521;
        public static final int main = 2130837522;
        public static final int x1 = 2130837523;
        public static final int x2 = 2130837524;
        public static final int x3 = 2130837525;
        public static final int x4 = 2130837526;
        public static final int x5 = 2130837527;
        public static final int z1 = 2130837528;
        public static final int z2 = 2130837529;
        public static final int z3 = 2130837530;
    }

    public static final class id {
        public static final int ad = 2131099650;
        public static final int ad_container = 2131099649;
        public static final int pocketDrumSurfaceView = 2131099648;
    }

    public static final class layout {
        public static final int main = 2130903040;
    }

    public static final class raw {
        public static final int s17 = 2130968576;
        public static final int s20 = 2130968577;
        public static final int s23 = 2130968578;
        public static final int s26 = 2130968579;
        public static final int s31 = 2130968580;
        public static final int s36 = 2130968581;
        public static final int s44 = 2130968582;
        public static final int s64 = 2130968583;
    }

    public static final class string {
        public static final int app_name = 2131034113;
        public static final int hello = 2131034112;
    }

    public static final class styleable {
        public static final int[] TileView = {R.attr.tileSize};
        public static final int TileView_tileSize = 0;
        public static final int[] com_admob_android_ads_AdView = {R.attr.backgroundColor, R.attr.primaryTextColor, R.attr.secondaryTextColor, R.attr.keywords, R.attr.refreshInterval};
        public static final int com_admob_android_ads_AdView_backgroundColor = 0;
        public static final int com_admob_android_ads_AdView_keywords = 3;
        public static final int com_admob_android_ads_AdView_primaryTextColor = 1;
        public static final int com_admob_android_ads_AdView_refreshInterval = 4;
        public static final int com_admob_android_ads_AdView_secondaryTextColor = 2;
        public static final int[] com_casee_adsdk_CaseeAdView = {R.attr.vertical};
        public static final int com_casee_adsdk_CaseeAdView_vertical = 0;
        public static final int[] com_wooboo_adlib_android_WoobooAdView = {R.attr.textColor, R.attr.testing};
        public static final int com_wooboo_adlib_android_WoobooAdView_testing = 1;
        public static final int com_wooboo_adlib_android_WoobooAdView_textColor = 0;
    }
}
