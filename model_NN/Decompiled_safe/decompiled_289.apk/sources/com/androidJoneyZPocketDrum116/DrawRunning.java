package com.androidJoneyZPocketDrum116;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.SurfaceHolder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class DrawRunning implements Runnable {
    private List<AnimationDraw> animationDraws = new ArrayList();
    private Bitmap bg1;
    private List<AnimationDraw> bg1Draws = new ArrayList();
    private List<AnimationDraw> bg1buffers = new ArrayList();
    private Bitmap bg2;
    private List<AnimationDraw> bg21Draws = new ArrayList();
    private List<AnimationDraw> bg21buffers = new ArrayList();
    private List<AnimationDraw> bg22Draws = new ArrayList();
    private List<AnimationDraw> bg22buffers = new ArrayList();
    private Bitmap bg3;
    private List<AnimationDraw> bg3Draws = new ArrayList();
    private List<AnimationDraw> bg3buffers = new ArrayList();
    private Bitmap bg4;
    private Bitmap bg5;
    private Bitmap bg6;
    private Bitmap bg7;
    private Bitmap bg8;
    private List<AnimationDraw> buffers = new ArrayList();
    private boolean running = true;
    private SurfaceHolder surfaceHolder;

    public DrawRunning(SurfaceHolder surfaceHolder2) {
        this.surfaceHolder = surfaceHolder2;
    }

    public void run() {
        while (this.running) {
            synchronized (this.surfaceHolder) {
                Canvas canvas = null;
                try {
                    canvas = this.surfaceHolder.lockCanvas(null);
                    doDraw(canvas);
                } finally {
                    if (canvas != null) {
                        this.surfaceHolder.unlockCanvasAndPost(canvas);
                    }
                }
            }
        }
    }

    private void doDraw(Canvas canvas) {
        synchronized (this) {
            if (!this.bg1buffers.isEmpty()) {
                this.bg1Draws.addAll(this.bg1buffers);
                this.bg1buffers.clear();
            }
        }
        synchronized (this) {
            if (!this.buffers.isEmpty()) {
                this.animationDraws.addAll(this.buffers);
                this.buffers.clear();
            }
        }
        synchronized (this) {
            if (!this.bg3buffers.isEmpty()) {
                this.bg3Draws.addAll(this.bg3buffers);
                this.bg3buffers.clear();
            }
        }
        synchronized (this) {
            if (!this.bg21buffers.isEmpty()) {
                this.bg21Draws.addAll(this.bg21buffers);
                this.bg21buffers.clear();
            }
        }
        synchronized (this) {
            if (!this.bg22buffers.isEmpty()) {
                this.bg22Draws.addAll(this.bg22buffers);
                this.bg22buffers.clear();
            }
        }
        Iterator<AnimationDraw> bombIt = this.animationDraws.iterator();
        while (bombIt.hasNext()) {
            AnimationDraw bomb = bombIt.next();
            Bitmap nextFrame = bomb.nextFrame();
            if (nextFrame == null) {
                bombIt.remove();
            } else {
                canvas.drawBitmap(nextFrame, bomb.getX(), bomb.getY(), (Paint) null);
            }
        }
        Iterator<AnimationDraw> bombIt1 = this.bg1Draws.iterator();
        while (bombIt1.hasNext()) {
            AnimationDraw bomb2 = bombIt1.next();
            Bitmap nextFrame2 = bomb2.nextFrame();
            if (nextFrame2 == null) {
                bombIt1.remove();
            } else {
                canvas.drawBitmap(nextFrame2, bomb2.getX(), bomb2.getY(), (Paint) null);
            }
        }
        Iterator<AnimationDraw> bombIt3 = this.bg3Draws.iterator();
        while (bombIt3.hasNext()) {
            AnimationDraw bomb3 = bombIt3.next();
            Bitmap nextFrame3 = bomb3.nextFrame();
            if (nextFrame3 == null) {
                bombIt3.remove();
            } else {
                canvas.drawBitmap(nextFrame3, bomb3.getX(), bomb3.getY(), (Paint) null);
            }
        }
        Iterator<AnimationDraw> bombIt22 = this.bg22Draws.iterator();
        while (bombIt22.hasNext()) {
            AnimationDraw bomb4 = bombIt22.next();
            Bitmap nextFrame4 = bomb4.nextFrame();
            if (nextFrame4 == null) {
                bombIt22.remove();
            } else {
                canvas.drawBitmap(nextFrame4, bomb4.getX(), bomb4.getY(), (Paint) null);
            }
        }
        Iterator<AnimationDraw> bombIt21 = this.bg21Draws.iterator();
        while (bombIt21.hasNext()) {
            AnimationDraw bomb5 = bombIt21.next();
            Bitmap nextFrame5 = bomb5.nextFrame();
            if (nextFrame5 == null) {
                bombIt21.remove();
            } else {
                canvas.drawBitmap(nextFrame5, bomb5.getX(), bomb5.getY(), (Paint) null);
            }
        }
        if (this.bg1Draws.isEmpty()) {
            canvas.drawBitmap(this.bg1, 100.0f, 0.0f, (Paint) null);
        }
        if (this.bg3Draws.isEmpty()) {
            canvas.drawBitmap(this.bg7, 350.0f, 0.0f, (Paint) null);
        }
        if (this.bg21Draws.isEmpty()) {
            canvas.drawBitmap(this.bg3, 15.0f, 100.0f, (Paint) null);
        }
        canvas.drawBitmap(this.bg8, 53.0f, 35.0f, (Paint) null);
        if (this.bg22Draws.isEmpty()) {
            canvas.drawBitmap(this.bg2, 15.0f, 90.0f, (Paint) null);
        }
        canvas.drawBitmap(this.bg4, 63.0f, 267.0f, (Paint) null);
    }

    public void addAnimationDraw(AnimationDraw bomb) {
        synchronized (this) {
            this.buffers.add(bomb);
        }
    }

    public void addBg1AnimationDraw(AnimationDraw bomb) {
        synchronized (this) {
            this.bg1buffers.add(bomb);
        }
    }

    public void addBg21AnimationDraw(AnimationDraw bomb) {
        synchronized (this) {
            this.bg21buffers.add(bomb);
        }
    }

    public void addBg22AnimationDraw(AnimationDraw bomb) {
        synchronized (this) {
            this.bg22buffers.add(bomb);
        }
    }

    public void addBg3AnimationDraw(AnimationDraw bomb) {
        synchronized (this) {
            this.bg3buffers.add(bomb);
        }
    }

    public void addMyBg(Bitmap myBg1, Bitmap myBg2, Bitmap myBg3, Bitmap myBg4, Bitmap myBg5, Bitmap myBg6, Bitmap myBg7, Bitmap myBg8) {
        this.bg1 = myBg1;
        this.bg2 = myBg2;
        this.bg3 = myBg3;
        this.bg4 = myBg4;
        this.bg5 = myBg5;
        this.bg6 = myBg6;
        this.bg7 = myBg7;
        this.bg8 = myBg8;
    }

    public void stopDrawing() {
        this.running = false;
    }
}
