package com.wiyun.ad;

import java.util.Locale;
import java.util.ResourceBundle;

class l {
    private static String a;
    private static String b = Locale.getDefault().getCountry();

    l() {
    }

    public static String a() {
        if (a == null || b()) {
            a = ResourceBundle.getBundle("com.wiyun.ad.sdk").getString("loading_ad");
        }
        return a;
    }

    public static String a(String str) {
        return ResourceBundle.getBundle("com.wiyun.ad.sdk").getString(str);
    }

    private static boolean b() {
        String country = Locale.getDefault().getCountry();
        if (b.equals(country)) {
            return false;
        }
        b = country;
        return true;
    }
}
