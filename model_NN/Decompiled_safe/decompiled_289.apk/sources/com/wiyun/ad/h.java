package com.wiyun.ad;

import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

class h {
    private final String[] a = new String[3000];
    private final Map<String, Character> b = new HashMap(252);

    public h() {
        Properties properties = new Properties();
        InputStream resourceAsStream = h.class.getResourceAsStream("HtmlCharacterEntityReferences.properties");
        if (resourceAsStream == null) {
            throw new IllegalStateException("Cannot find reference definition file [HtmlCharacterEntityReferences.properties] as class path resource");
        }
        try {
            properties.load(resourceAsStream);
            resourceAsStream.close();
            Enumeration<?> propertyNames = properties.propertyNames();
            while (propertyNames.hasMoreElements()) {
                String str = (String) propertyNames.nextElement();
                int parseInt = Integer.parseInt(str);
                int i = parseInt < 1000 ? parseInt : parseInt - 7000;
                String property = properties.getProperty(str);
                this.a[i] = String.valueOf('&') + property + ';';
                this.b.put(property, new Character((char) parseInt));
            }
        } catch (IOException e) {
            throw new IllegalStateException("Failed to parse reference definition file [HtmlCharacterEntityReferences.properties]: " + e.getMessage());
        } catch (Throwable th) {
            resourceAsStream.close();
            throw th;
        }
    }

    public char a(String str) {
        Character ch = this.b.get(str);
        if (ch != null) {
            return ch.charValue();
        }
        return 65535;
    }
}
