package com.wiyun.ad;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class f {
    private static MessageDigest a() {
        return a("MD5");
    }

    static MessageDigest a(String str) {
        try {
            return MessageDigest.getInstance(str);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    public static byte[] a(byte[] bArr) {
        return a().digest(bArr);
    }

    public static String b(byte[] bArr) {
        return r.b(a(bArr));
    }

    public static byte[] b(String str) {
        return a(d(str));
    }

    public static String c(String str) {
        return r.b(b(str));
    }

    private static byte[] d(String str) {
        return n.a(str);
    }
}
