package com.wiyun.ad;

import android.content.Context;
import android.content.res.Configuration;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import java.util.Locale;

class b {
    private static String a;
    private static String b;
    private static String c;
    private static int d = -1;
    private static boolean e = true;

    b() {
    }

    static String a() {
        if (a == null) {
            StringBuffer stringBuffer = new StringBuffer();
            String str = Build.VERSION.RELEASE;
            if (str.length() > 0) {
                stringBuffer.append(str);
            } else {
                stringBuffer.append("1.0");
            }
            stringBuffer.append("; ");
            Locale locale = Locale.getDefault();
            String language = locale.getLanguage();
            if (language != null) {
                stringBuffer.append(language.toLowerCase());
                String country = locale.getCountry();
                if (country != null) {
                    stringBuffer.append("-");
                    stringBuffer.append(country.toLowerCase());
                }
            } else {
                stringBuffer.append("en");
            }
            String str2 = Build.DEVICE;
            if (str2.length() > 0) {
                stringBuffer.append("; ");
                stringBuffer.append(str2);
            }
            a = String.format("Mozilla/5.0 (Linux; U; Android %s) AppleWebKit/525.10+ (KHTML, like Gecko) Version/3.0.4 Mobile Safari/523.12.2", stringBuffer);
        }
        return a;
    }

    public static String a(Context context) {
        if (c == null) {
            if (context.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") != -1) {
                TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
                if (telephonyManager != null) {
                    c = telephonyManager.getDeviceId();
                    if (c == null) {
                        c = Settings.Secure.getString(context.getContentResolver(), "android_id");
                    }
                    if (c == null) {
                        c = "000000000000000";
                    }
                    d = "000000000000000".equals(c) ? 1 : 0;
                } else {
                    d = 1;
                }
            } else if (e) {
                a("Cannot get a user ID.  Open manifest.xml and just before the final </manifest> tag add:  <uses-permission android:name=\"android.permission.READ_PHONE_STATE\" />");
            }
        }
        return c;
    }

    protected static void a(String str) {
        Log.e("WiYun", str);
        throw new IllegalArgumentException(str);
    }

    public static boolean b(Context context) {
        if (d == -1) {
            a(context);
        }
        if (d == -1) {
            d = "sdk".equalsIgnoreCase(Build.MODEL) ? 1 : 0;
        }
        return d != 0;
    }

    public static int c(Context context) {
        if (d(context)) {
            return 4;
        }
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        if (telephonyManager == null) {
            return 0;
        }
        int networkType = telephonyManager.getNetworkType();
        return (networkType == 1 || networkType == 2 || networkType == 0) ? 2 : 3;
    }

    static boolean d(Context context) {
        if (context.checkCallingOrSelfPermission("android.permission.ACCESS_WIFI_STATE") != 0) {
            return false;
        }
        WifiManager wifiManager = (WifiManager) context.getSystemService("wifi");
        if (wifiManager == null) {
            return false;
        }
        WifiInfo connectionInfo = wifiManager.getConnectionInfo();
        return connectionInfo != null && connectionInfo.getSupplicantState() == SupplicantState.COMPLETED;
    }

    static String e(Context context) {
        if (TextUtils.isEmpty(b)) {
            Configuration configuration = context.getResources().getConfiguration();
            b = String.valueOf(configuration.mnc + (configuration.mcc * 100));
        }
        return b;
    }
}
