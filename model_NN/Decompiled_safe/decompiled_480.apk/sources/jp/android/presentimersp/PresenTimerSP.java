package jp.android.presentimersp;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.LinearLayout;
import android.widget.TextView;

public class PresenTimerSP extends Activity {
    boolean bStarted = false;
    Chronometer chronometer;
    long redsec = 0;
    long screenoffsec = 1200;
    long starttime = 600;
    long updownsec = 30;
    long yellowsec = 120;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        loadMyPref();
        final TextView txtRestMin = (TextView) findViewById(R.id.txtRestMin);
        final TextView txtRestSec = (TextView) findViewById(R.id.txtRestSec);
        final TextView txtAllottedMin = (TextView) findViewById(R.id.txtAllottedMin);
        final TextView txtAllottedSec = (TextView) findViewById(R.id.txtAllottedSec);
        txtRestMin.setText(String.format("%d", Integer.valueOf(((int) this.starttime) / 60)));
        txtRestSec.setText(String.format("%1$02d", Long.valueOf(this.starttime % 60)));
        txtAllottedMin.setText(String.format("%d:", Integer.valueOf(((int) this.starttime) / 60)));
        txtAllottedSec.setText(String.format("%1$02d", Long.valueOf(this.starttime % 60)));
        final LinearLayout layoutRest = (LinearLayout) findViewById(R.id.linearLayoutRest);
        layoutRest.setBackgroundColor(-16711936);
        this.chronometer = (Chronometer) findViewById(R.id.chronometer1);
        this.chronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            public void onChronometerTick(Chronometer chronometer) {
                long pasttime = (SystemClock.elapsedRealtime() - chronometer.getBase()) / 1000;
                long resttime = PresenTimerSP.this.starttime - pasttime;
                if (resttime >= 0) {
                    txtRestMin.setText(String.format("%d", Integer.valueOf(((int) resttime) / 60)));
                    txtRestSec.setText(String.format("%1$02d", Long.valueOf(resttime % 60)));
                    if (resttime <= PresenTimerSP.this.redsec) {
                        layoutRest.setBackgroundColor(-65536);
                    } else if (resttime <= PresenTimerSP.this.yellowsec) {
                        layoutRest.setBackgroundColor(-256);
                    }
                }
                if (pasttime >= PresenTimerSP.this.screenoffsec) {
                    PresenTimerSP.this.getWindow().clearFlags(128);
                }
            }
        });
        final Button upbutton = (Button) findViewById(R.id.btnUp);
        upbutton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!PresenTimerSP.this.bStarted) {
                    PresenTimerSP.this.starttime += PresenTimerSP.this.updownsec;
                    txtRestMin.setText(String.format("%d", Integer.valueOf(((int) PresenTimerSP.this.starttime) / 60)));
                    txtRestSec.setText(String.format("%1$02d", Long.valueOf(PresenTimerSP.this.starttime % 60)));
                    txtAllottedMin.setText(String.format("%d:", Integer.valueOf(((int) PresenTimerSP.this.starttime) / 60)));
                    txtAllottedSec.setText(String.format("%1$02d", Long.valueOf(PresenTimerSP.this.starttime % 60)));
                }
            }
        });
        final Button downbutton = (Button) findViewById(R.id.btnDown);
        downbutton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!PresenTimerSP.this.bStarted) {
                    PresenTimerSP.this.starttime -= PresenTimerSP.this.updownsec;
                    if (PresenTimerSP.this.starttime < 0) {
                        PresenTimerSP.this.starttime = 0;
                    }
                    txtRestMin.setText(String.format("%d", Integer.valueOf(((int) PresenTimerSP.this.starttime) / 60)));
                    txtRestSec.setText(String.format("%1$02d", Long.valueOf(PresenTimerSP.this.starttime % 60)));
                    txtAllottedMin.setText(String.format("%d:", Integer.valueOf(((int) PresenTimerSP.this.starttime) / 60)));
                    txtAllottedSec.setText(String.format("%1$02d", Long.valueOf(PresenTimerSP.this.starttime % 60)));
                }
            }
        });
        final Button startbutton = (Button) findViewById(R.id.btnStart);
        final TextView textView = txtAllottedMin;
        final TextView textView2 = txtAllottedSec;
        startbutton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PresenTimerSP.this.chronometer.setFormat("%s");
                if (!PresenTimerSP.this.bStarted) {
                    PresenTimerSP.this.getScreenoffPref();
                    PresenTimerSP.this.getYellowTimePref();
                    PresenTimerSP.this.getRedTimePref();
                    PresenTimerSP.this.chronometer.setBase(SystemClock.elapsedRealtime());
                    PresenTimerSP.this.chronometer.start();
                    PresenTimerSP.this.bStarted = true;
                    PresenTimerSP.this.getWindow().addFlags(128);
                    startbutton.setText("STOP");
                    layoutRest.setBackgroundColor(-16711936);
                    upbutton.setTextColor(-7829368);
                    downbutton.setTextColor(-7829368);
                    textView.setTextColor(-7829368);
                    textView2.setTextColor(-7829368);
                    return;
                }
                PresenTimerSP.this.chronometer.stop();
                PresenTimerSP.this.bStarted = false;
                PresenTimerSP.this.getWindow().clearFlags(128);
                startbutton.setText("START");
                upbutton.setTextColor(-16777216);
                downbutton.setTextColor(-16777216);
                textView.setTextColor(-1);
                textView2.setTextColor(-1);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        saveMyPref();
        super.onDestroy();
    }

    private void saveMyPref() {
        SharedPreferences.Editor editor = getPreferences(0).edit();
        editor.putLong("pref_starttime", this.starttime);
        editor.commit();
    }

    private void loadMyPref() {
        this.starttime = getPreferences(0).getLong("pref_starttime", this.starttime);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add("settings").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                PresenTimerSP.this.openPref();
                return false;
            }
        });
        return true;
    }

    public void openPref() {
        Intent intent = new Intent(this, Settings.class);
        intent.setAction("android.intent.action.VIEW");
        startActivity(intent);
    }

    /* access modifiers changed from: private */
    public void getScreenoffPref() {
        int new_num = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(this).getString("settings_screenoff", "1200"));
        if (this.screenoffsec != ((long) new_num)) {
            this.screenoffsec = (long) new_num;
        }
    }

    /* access modifiers changed from: private */
    public void getYellowTimePref() {
        int new_num = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(this).getString("settings_yellowtime", "60"));
        if (this.yellowsec != ((long) new_num)) {
            this.yellowsec = (long) new_num;
        }
    }

    /* access modifiers changed from: private */
    public void getRedTimePref() {
        int new_num = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(this).getString("settings_redtime", "0"));
        if (this.redsec != ((long) new_num)) {
            this.redsec = (long) new_num;
        }
    }
}
