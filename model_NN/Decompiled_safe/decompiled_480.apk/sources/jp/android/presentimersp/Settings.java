package jp.android.presentimersp;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import java.util.HashMap;
import java.util.Map;

public class Settings extends PreferenceActivity {
    static final String REDTIME_KEY = "settings_redtime";
    static final String SCREENOFF_KEY = "settings_screenoff";
    static final String YELLOWTIME_KEY = "settings_yellowtime";
    private SharedPreferences.OnSharedPreferenceChangeListener listener = new SharedPreferences.OnSharedPreferenceChangeListener() {
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            if (key.equals(Settings.SCREENOFF_KEY)) {
                Settings.this.screenoffPref.setSummary(Settings.this.screenoffMap.get(sharedPreferences.getString(key, Settings.SCREENOFF_KEY)));
            }
            if (key.equals(Settings.YELLOWTIME_KEY)) {
                Settings.this.yellowtimePref.setSummary(Settings.this.yellowtimeMap.get(sharedPreferences.getString(key, Settings.YELLOWTIME_KEY)));
            }
            if (key.equals(Settings.REDTIME_KEY)) {
                Settings.this.redtimePref.setSummary(Settings.this.redtimeMap.get(sharedPreferences.getString(key, "settings_redwtime")));
            }
        }
    };
    Map<String, String> redtimeMap = new HashMap();
    Preference redtimePref;
    Map<String, String> screenoffMap = new HashMap();
    Preference screenoffPref;
    Map<String, String> yellowtimeMap = new HashMap();
    Preference yellowtimePref;

    /* Debug info: failed to restart local var, previous not found, register: 12 */
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
        String[] screenoffKeys = getResources().getStringArray(R.array.list_entries);
        String[] screenoffValues = getResources().getStringArray(R.array.list_entryvalues);
        String[] yellowtimeKeys = getResources().getStringArray(R.array.list_resttime_entries);
        String[] yellowtimeValues = getResources().getStringArray(R.array.list_resttime_entryvalues);
        String[] redtimeKeys = getResources().getStringArray(R.array.list_resttime_entries);
        String[] redtimeValues = getResources().getStringArray(R.array.list_resttime_entryvalues);
        for (int i = 0; i < screenoffKeys.length; i++) {
            this.screenoffMap.put(screenoffValues[i], screenoffKeys[i]);
        }
        this.screenoffPref = findPreference(SCREENOFF_KEY);
        ListPreference list_preference = (ListPreference) getPreferenceScreen().findPreference(SCREENOFF_KEY);
        list_preference.setSummary(this.screenoffMap.get(list_preference.getValue()));
        for (int i2 = 0; i2 < yellowtimeKeys.length; i2++) {
            this.yellowtimeMap.put(yellowtimeValues[i2], yellowtimeKeys[i2]);
        }
        this.yellowtimePref = findPreference(YELLOWTIME_KEY);
        ListPreference list_preference2 = (ListPreference) getPreferenceScreen().findPreference(YELLOWTIME_KEY);
        list_preference2.setSummary(this.yellowtimeMap.get(list_preference2.getValue()));
        for (int i3 = 0; i3 < redtimeKeys.length; i3++) {
            this.redtimeMap.put(redtimeValues[i3], redtimeKeys[i3]);
        }
        this.redtimePref = findPreference(REDTIME_KEY);
        ListPreference list_preference3 = (ListPreference) getPreferenceScreen().findPreference(REDTIME_KEY);
        list_preference3.setSummary(this.redtimeMap.get(list_preference3.getValue()));
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this.listener);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this.listener);
    }
}
