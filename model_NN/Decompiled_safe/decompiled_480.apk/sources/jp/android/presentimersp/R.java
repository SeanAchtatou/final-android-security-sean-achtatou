package jp.android.presentimersp;

public final class R {

    public static final class array {
        public static final int list_entries = 2131034112;
        public static final int list_entryvalues = 2131034113;
        public static final int list_resttime_entries = 2131034114;
        public static final int list_resttime_entryvalues = 2131034115;
    }

    public static final class attr {
    }

    public static final class color {
        public static final int bg_bottom = 2131099660;
        public static final int bg_top = 2131099659;
        public static final int color1 = 2131099648;
        public static final int color2 = 2131099649;
        public static final int color3 = 2131099650;
        public static final int color4 = 2131099651;
        public static final int color5 = 2131099652;
        public static final int color6 = 2131099653;
        public static final int color7 = 2131099654;
        public static final int component_button_bottom = 2131099671;
        public static final int component_button_top = 2131099670;
        public static final int component_frame_disabled = 2131099667;
        public static final int component_frame_normal = 2131099663;
        public static final int component_frame_pressed = 2131099664;
        public static final int component_frame_pressed_bottom = 2131099666;
        public static final int component_frame_pressed_top = 2131099665;
        public static final int component_text_normal = 2131099668;
        public static final int component_text_pressed = 2131099669;
        public static final int medium_text = 2131099662;
        public static final int small_text = 2131099661;
        public static final int title_bg = 2131099656;
        public static final int title_bg_bottom = 2131099658;
        public static final int title_bg_top = 2131099657;
        public static final int window_bg = 2131099655;
    }

    public static final class drawable {
        public static final int bg = 2130837504;
        public static final int btn = 2130837505;
        public static final int icon = 2130837506;
    }

    public static final class id {
        public static final int btnDown = 2131230724;
        public static final int btnStart = 2131230727;
        public static final int btnUp = 2131230725;
        public static final int chronometer1 = 2131230733;
        public static final int linearLayout1 = 2131230720;
        public static final int linearLayout2 = 2131230726;
        public static final int linearLayoutPassed = 2131230731;
        public static final int linearLayoutRest = 2131230728;
        public static final int textView1 = 2131230721;
        public static final int textView2 = 2131230732;
        public static final int txtAllottedMin = 2131230722;
        public static final int txtAllottedSec = 2131230723;
        public static final int txtRestMin = 2131230729;
        public static final int txtRestSec = 2131230730;
    }

    public static final class layout {
        public static final int main = 2130903040;
    }

    public static final class string {
        public static final int allotted = 2131165186;
        public static final int app_name = 2131165185;
        public static final int hello = 2131165184;
        public static final int settings = 2131165187;
        public static final int settings_redtime = 2131165189;
        public static final int settings_screenoff = 2131165188;
        public static final int settings_yellowtime = 2131165190;
    }

    public static final class xml {
        public static final int preferences = 2130968576;
    }
}
