package buunyan.pzl.kurukuru;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.EditText;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;

class Screen extends SurfaceView implements SurfaceHolder.Callback, Runnable {
    static final int AD_DOWNLOAD = 0;
    static final int AD_MONTH = 4;
    static final int AD_SOUND = 28;
    static final int AD_VIB = 32;
    static final int AD_YEAR = 8;
    static final int CLEAR = 11;
    static final int EFFECT1 = 14;
    static final int GAMECLEAR = 13;
    static final int GAMEMENU = 36;
    static final int GAMEOVER = 12;
    static final int GAMEPLAY = 6;
    static final int GAMEPLAY2 = 9;
    static final int GAMEPLAY3 = 10;
    static final int GAMEPLAYLOAD = 8;
    static final int GAMEPLAY_ROT = 7;
    static final int HELP = 20;
    static final int INIT = 4;
    static Bitmap[] Img = new Bitmap[_TAMA3];
    static final int KEY_ASTER = 16;
    static final int KEY_DOWN = 2;
    static final int KEY_FIRE = 0;
    static final int KEY_FREE = 0;
    static final int KEY_HOLD = 2;
    static final int KEY_KEY0 = 5;
    static final int KEY_KEY1 = 6;
    static final int KEY_KEY2 = 7;
    static final int KEY_KEY3 = 8;
    static final int KEY_KEY4 = 9;
    static final int KEY_KEY5 = 10;
    static final int KEY_KEY6 = 11;
    static final int KEY_KEY7 = 12;
    static final int KEY_KEY8 = 13;
    static final int KEY_KEY9 = 14;
    static final int KEY_LEFT = 3;
    static final int KEY_POUND = 15;
    static final int KEY_PULL = 3;
    static final int KEY_PUSH = 1;
    static final int KEY_RIGHT = 4;
    static final int KEY_SOFT1 = 21;
    static final int KEY_SOFT2 = 22;
    static final int KEY_UP = 1;
    static final int MENU = 180;
    static final int PHEIGHT = 6;
    static final int PWIDTH = 10;
    static final int RANKING = 1000;
    static final int RANKING2 = 1002;
    static final int RANKING3 = 1003;
    static final int RANKING4 = 1005;
    static final int RANKINGE = 1004;
    static final int RANKING_TT = 1001;
    static final int SAVE_DATE = 48;
    static final int SAVE_HISCORE1 = 64;
    static final int SAVE_HISCORE10 = 100;
    static final int SAVE_HISCORE11 = 104;
    static final int SAVE_HISCORE12 = 108;
    static final int SAVE_HISCORE13 = 112;
    static final int SAVE_HISCORE14 = 116;
    static final int SAVE_HISCORE15 = 120;
    static final int SAVE_HISCORE16 = 124;
    static final int SAVE_HISCORE17 = 128;
    static final int SAVE_HISCORE18 = 132;
    static final int SAVE_HISCORE19 = 136;
    static final int SAVE_HISCORE2 = 68;
    static final int SAVE_HISCORE20 = 140;
    static final int SAVE_HISCORE3 = 72;
    static final int SAVE_HISCORE4 = 76;
    static final int SAVE_HISCORE5 = 80;
    static final int SAVE_HISCORE6 = 84;
    static final int SAVE_HISCORE7 = 88;
    static final int SAVE_HISCORE8 = 92;
    static final int SAVE_HISCORE9 = 96;
    static final int SAVE_INIT = 40;
    static final int SAVE_RUNCNT = 56;
    static final int SAVE_TORPHY = 52;
    static final int SAVE_VER = 44;
    static final int SELECT = 5;
    static final int TITLE = 1;
    static final int TROPHY = 3;
    static final int VERERR = 1006;
    static final int WINDOW_CONFIG = 16;
    static final int WINDOW_GAMEOVER_SCORE = 3;
    static final int WINDOW_HELP = 8;
    static final int WINDOW_LOAD = 5;
    static final int WINDOW_MENU = 9;
    static final int WINDOW_MSG = 0;
    static final int WINDOW_NARU = 6;
    static final int WINDOW_NET = 17;
    static final int WINDOW_OUTE = 7;
    static final int WINDOW_RANKING = 4;
    static final int WINDOW_TITLE = 1;
    static final int WINDOW_TORPHY = 2;
    static int[] Window_Config = null;
    static int[] Window_Help = {_TITLE_MODE, 19};
    static int[] Window_Load = null;
    static int[] Window_Menu = {14, 10};
    static int[] Window_Msg = {_TITLE_MODE, 8};
    static int[] Window_Net = {9, 3};
    static int[] Window_Ranking = {18, _SELCURSOLX};
    static int[] Window_Score = {12, 11};
    static int[] Window_TMenu = {12, 13};
    static int[] Window_Torphy = {HELP, 13};
    static final int _BCOLOR = 43;
    static final int _COMBO = 13;
    static final int _COMBOF = 14;
    static final int _CTIME1 = 56;
    static final int _CTIME2 = 57;
    static final int _CUR_YESNO = 84;
    static final int _DRAW_H = 3;
    static final int _DRAW_W = 2;
    static final int _GAMEMODE = 4;
    static final int _LOADGRP = 2;
    static final int _LV = 8;
    static final int _LVUPFLAG = 12;
    static final int _MENUCUR = 83;
    static final int _MOVEX = 25;
    static final int _MOVEY = 26;
    static final int _PLAYTIME = 69;
    static final int _SCENESTART = 7;
    static final int _SCORE = 36;
    static final int _SELCURSOL = 22;
    static final int _SELCURSOLX = 23;
    static final int _SELCURSOLY = 24;
    static final int _SPEED = 52;
    static final int _STAGE = 9;
    static final int _TAMA1 = 48;
    static final int _TAMA2 = 49;
    static final int _TAMA3 = 50;
    static final int _TAMA_1 = 73;
    static final int _TAMA_2 = 74;
    static final int _TAMA_3 = 75;
    static final int _TAMA_4 = 76;
    static final int _TAMA_5 = 77;
    static final int _TAMA_6 = 78;
    static final int _TAMA_7 = 79;
    static final int _TENMETU = 37;
    static final int _TIME = 29;
    static final int _TITLE_MODE = 30;
    static final int _TORPHY = 16;
    static int[] color;
    Canvas BG;
    String[] GameStr;
    int[] GameStrC;
    int GameStrCnt;
    int[] GameStrX;
    int[] GameStrY;
    int HelpPage;
    int[] KeyFlag = new int[500];
    int[] KeyFlagOld = new int[500];
    int[] KeyXY;
    String[] MsgBoxStr = new String[5];
    int PCnt;
    int RepeartKey = 0;
    int Tx;
    int Ty;
    long _Time;
    long _Time2 = 0;
    long _Time3;
    Context _app;
    String[] _res;
    String _sC1;
    String _sC2;
    int _sheight;
    int _swidth;
    long _time1;
    long _time2;
    long _timeb;
    int allpi;
    int[] connection = new int[60];
    int count;
    int cx;
    int cy;
    String domain = "http://58.81.14.235/";
    int endf = 0;
    int ex;
    int ey;
    int ez;
    Canvas g;
    String g_res;
    int height;
    int hint = 0;
    int[] hiscore = new int[HELP];
    int[] iSys = new int[500];
    int idxConnection;
    String imei;
    int level;
    Handler mHandler = new Handler();
    private Thread mainLoop;
    int[] map = new int[65536];
    private SurfaceHolder mholder;
    int ng1;
    int ng2;
    int ng3;
    int ng4;
    int ng5;
    int ng6;
    int ng7;
    int ng8;
    Bitmap offs;
    Paint paint;
    String phoneNumber;
    Random r = new Random();
    String sC1;
    String sC2;
    int sb;
    float scalex;
    float scaley;
    byte[] scratchpad = new byte[1024];
    int sg;
    String simSerial;
    String sjis;
    byte[] sjisbuf = new byte[8192];
    String[] sjist = new String[256];
    String softwareVer;
    int sr;
    String subscriberId;
    int sx;
    int sy;
    int sz;
    int turn;
    int[] viewCandyX = new int[7];
    int width;

    static {
        int[] iArr = new int[45];
        iArr[0] = SAVE_HISCORE1;
        iArr[1] = SAVE_HISCORE1;
        iArr[2] = SAVE_HISCORE1;
        iArr[3] = 210;
        iArr[8] = 210;
        iArr[10] = 210;
        iArr[12] = 210;
        iArr[13] = 210;
        iArr[16] = 210;
        iArr[WINDOW_NET] = 210;
        iArr[18] = 210;
        iArr[HELP] = 210;
        iArr[_SELCURSOLY] = SAVE_HISCORE1;
        iArr[_MOVEX] = SAVE_HISCORE1;
        iArr[_MOVEY] = SAVE_HISCORE1;
        iArr[27] = 210;
        iArr[AD_VIB] = 210;
        iArr[34] = 210;
        iArr[36] = 210;
        iArr[_TENMETU] = 210;
        iArr[SAVE_INIT] = 210;
        iArr[41] = 210;
        iArr[42] = AD_VIB;
        iArr[SAVE_VER] = AD_VIB;
        color = iArr;
        int[] iArr2 = new int[222];
        iArr2[0] = HELP;
        iArr2[1] = 11;
        iArr2[3] = 6;
        iArr2[4] = 6;
        iArr2[5] = 6;
        iArr2[6] = 6;
        iArr2[7] = 6;
        iArr2[8] = 6;
        iArr2[9] = 6;
        iArr2[10] = 6;
        iArr2[11] = 6;
        iArr2[12] = 6;
        iArr2[13] = 6;
        iArr2[14] = 6;
        iArr2[KEY_POUND] = 6;
        iArr2[16] = 6;
        iArr2[WINDOW_NET] = 6;
        iArr2[18] = 6;
        iArr2[19] = 6;
        iArr2[HELP] = 6;
        iArr2[KEY_SOFT1] = 1;
        iArr2[22] = 4;
        iArr2[_SELCURSOLX] = 8;
        iArr2[_SELCURSOLY] = 8;
        iArr2[_MOVEX] = 8;
        iArr2[_MOVEY] = 8;
        iArr2[27] = 8;
        iArr2[AD_SOUND] = 8;
        iArr2[_TIME] = 8;
        iArr2[_TITLE_MODE] = 8;
        iArr2[31] = 8;
        iArr2[AD_VIB] = 8;
        iArr2[33] = 8;
        iArr2[34] = 8;
        iArr2[35] = 8;
        iArr2[36] = 8;
        iArr2[_TENMETU] = 8;
        iArr2[38] = 8;
        iArr2[39] = 8;
        iArr2[SAVE_INIT] = 8;
        iArr2[41] = 5;
        iArr2[42] = 4;
        iArr2[_BCOLOR] = 8;
        iArr2[SAVE_VER] = 8;
        iArr2[45] = 8;
        iArr2[46] = 8;
        iArr2[47] = 8;
        iArr2[48] = 8;
        iArr2[_TAMA2] = 8;
        iArr2[_TAMA3] = 8;
        iArr2[51] = 8;
        iArr2[52] = 8;
        iArr2[53] = 8;
        iArr2[54] = 8;
        iArr2[55] = 8;
        iArr2[56] = 8;
        iArr2[_CTIME2] = 8;
        iArr2[58] = 8;
        iArr2[59] = 8;
        iArr2[60] = 8;
        iArr2[61] = 5;
        iArr2[62] = 4;
        iArr2[63] = 8;
        iArr2[SAVE_HISCORE1] = 8;
        iArr2[65] = 8;
        iArr2[66] = 8;
        iArr2[67] = 8;
        iArr2[SAVE_HISCORE2] = 8;
        iArr2[_PLAYTIME] = 8;
        iArr2[70] = 8;
        iArr2[71] = 8;
        iArr2[SAVE_HISCORE3] = 8;
        iArr2[_TAMA_1] = 8;
        iArr2[_TAMA_2] = 8;
        iArr2[_TAMA_3] = 8;
        iArr2[76] = 8;
        iArr2[_TAMA_5] = 8;
        iArr2[_TAMA_6] = 8;
        iArr2[_TAMA_7] = 8;
        iArr2[SAVE_HISCORE5] = 8;
        iArr2[81] = 5;
        iArr2[82] = 4;
        iArr2[_MENUCUR] = 8;
        iArr2[84] = 8;
        iArr2[85] = 8;
        iArr2[86] = 8;
        iArr2[87] = 8;
        iArr2[SAVE_HISCORE7] = 8;
        iArr2[89] = 8;
        iArr2[90] = 8;
        iArr2[91] = 8;
        iArr2[SAVE_HISCORE8] = 8;
        iArr2[93] = 8;
        iArr2[94] = 8;
        iArr2[95] = 8;
        iArr2[SAVE_HISCORE9] = 8;
        iArr2[97] = 8;
        iArr2[98] = 8;
        iArr2[99] = 8;
        iArr2[SAVE_HISCORE10] = 8;
        iArr2[101] = 5;
        iArr2[102] = 4;
        iArr2[103] = 8;
        iArr2[SAVE_HISCORE11] = 8;
        iArr2[105] = 8;
        iArr2[106] = 8;
        iArr2[107] = 8;
        iArr2[SAVE_HISCORE12] = 8;
        iArr2[109] = 8;
        iArr2[110] = 8;
        iArr2[111] = 8;
        iArr2[SAVE_HISCORE13] = 8;
        iArr2[113] = 8;
        iArr2[114] = 8;
        iArr2[115] = 8;
        iArr2[SAVE_HISCORE14] = 8;
        iArr2[117] = 8;
        iArr2[118] = 8;
        iArr2[119] = 8;
        iArr2[SAVE_HISCORE15] = 8;
        iArr2[121] = 5;
        iArr2[122] = 4;
        iArr2[123] = 8;
        iArr2[SAVE_HISCORE16] = 8;
        iArr2[125] = 8;
        iArr2[126] = 8;
        iArr2[127] = 8;
        iArr2[SAVE_HISCORE17] = 8;
        iArr2[129] = 8;
        iArr2[130] = 8;
        iArr2[131] = 8;
        iArr2[SAVE_HISCORE18] = 8;
        iArr2[133] = 8;
        iArr2[134] = 8;
        iArr2[135] = 8;
        iArr2[SAVE_HISCORE19] = 8;
        iArr2[137] = 8;
        iArr2[138] = 8;
        iArr2[139] = 8;
        iArr2[SAVE_HISCORE20] = 8;
        iArr2[141] = 5;
        iArr2[142] = 4;
        iArr2[143] = 8;
        iArr2[144] = 8;
        iArr2[145] = 8;
        iArr2[146] = 8;
        iArr2[147] = 8;
        iArr2[148] = 8;
        iArr2[149] = 8;
        iArr2[150] = 8;
        iArr2[151] = 8;
        iArr2[152] = 8;
        iArr2[153] = 8;
        iArr2[154] = 8;
        iArr2[155] = 8;
        iArr2[156] = 8;
        iArr2[157] = 8;
        iArr2[158] = 8;
        iArr2[159] = 8;
        iArr2[160] = 8;
        iArr2[161] = 5;
        iArr2[162] = 4;
        iArr2[163] = 8;
        iArr2[164] = 8;
        iArr2[165] = 8;
        iArr2[166] = 8;
        iArr2[167] = 8;
        iArr2[168] = 8;
        iArr2[169] = 8;
        iArr2[170] = 8;
        iArr2[171] = 8;
        iArr2[172] = 8;
        iArr2[173] = 8;
        iArr2[174] = 8;
        iArr2[175] = 8;
        iArr2[176] = 8;
        iArr2[177] = 8;
        iArr2[178] = 8;
        iArr2[179] = 8;
        iArr2[MENU] = 8;
        iArr2[181] = 5;
        iArr2[182] = 4;
        iArr2[183] = 8;
        iArr2[184] = 8;
        iArr2[185] = 8;
        iArr2[186] = 8;
        iArr2[187] = 8;
        iArr2[188] = 8;
        iArr2[189] = 8;
        iArr2[190] = 8;
        iArr2[191] = 8;
        iArr2[192] = 8;
        iArr2[193] = 8;
        iArr2[194] = 8;
        iArr2[195] = 8;
        iArr2[196] = 8;
        iArr2[197] = 8;
        iArr2[198] = 8;
        iArr2[199] = 8;
        iArr2[200] = 8;
        iArr2[201] = 5;
        iArr2[202] = 2;
        iArr2[203] = 7;
        iArr2[204] = 7;
        iArr2[205] = 7;
        iArr2[206] = 7;
        iArr2[207] = 7;
        iArr2[208] = 7;
        iArr2[209] = 7;
        iArr2[210] = 7;
        iArr2[211] = 7;
        iArr2[212] = 7;
        iArr2[213] = 7;
        iArr2[214] = 7;
        iArr2[215] = 7;
        iArr2[216] = 7;
        iArr2[217] = 7;
        iArr2[218] = 7;
        iArr2[219] = 7;
        iArr2[220] = 7;
        iArr2[221] = 3;
        Window_Config = iArr2;
        int[] iArr3 = new int[146];
        iArr3[0] = 18;
        iArr3[1] = 8;
        iArr3[3] = 6;
        iArr3[4] = 6;
        iArr3[5] = 6;
        iArr3[6] = 6;
        iArr3[7] = 6;
        iArr3[8] = 6;
        iArr3[9] = 6;
        iArr3[10] = 6;
        iArr3[11] = 6;
        iArr3[12] = 6;
        iArr3[13] = 6;
        iArr3[14] = 6;
        iArr3[KEY_POUND] = 6;
        iArr3[16] = 6;
        iArr3[WINDOW_NET] = 6;
        iArr3[18] = 6;
        iArr3[19] = 1;
        iArr3[HELP] = 4;
        iArr3[KEY_SOFT1] = 8;
        iArr3[22] = 8;
        iArr3[_SELCURSOLX] = 8;
        iArr3[_SELCURSOLY] = 8;
        iArr3[_MOVEX] = 8;
        iArr3[_MOVEY] = 8;
        iArr3[27] = 8;
        iArr3[AD_SOUND] = 8;
        iArr3[_TIME] = 8;
        iArr3[_TITLE_MODE] = 8;
        iArr3[31] = 8;
        iArr3[AD_VIB] = 8;
        iArr3[33] = 8;
        iArr3[34] = 8;
        iArr3[35] = 8;
        iArr3[36] = 8;
        iArr3[_TENMETU] = 5;
        iArr3[38] = 4;
        iArr3[39] = 8;
        iArr3[SAVE_INIT] = 8;
        iArr3[41] = 8;
        iArr3[42] = 8;
        iArr3[_BCOLOR] = 8;
        iArr3[SAVE_VER] = 8;
        iArr3[45] = 8;
        iArr3[46] = 8;
        iArr3[47] = 8;
        iArr3[48] = 8;
        iArr3[_TAMA2] = 8;
        iArr3[_TAMA3] = 8;
        iArr3[51] = 8;
        iArr3[52] = 8;
        iArr3[53] = 8;
        iArr3[54] = 8;
        iArr3[55] = 5;
        iArr3[56] = 4;
        iArr3[_CTIME2] = 8;
        iArr3[58] = 8;
        iArr3[59] = 8;
        iArr3[60] = 8;
        iArr3[61] = 8;
        iArr3[62] = 8;
        iArr3[63] = 8;
        iArr3[SAVE_HISCORE1] = 8;
        iArr3[65] = 8;
        iArr3[66] = 8;
        iArr3[67] = 8;
        iArr3[SAVE_HISCORE2] = 8;
        iArr3[_PLAYTIME] = 8;
        iArr3[70] = 8;
        iArr3[71] = 8;
        iArr3[SAVE_HISCORE3] = 8;
        iArr3[_TAMA_1] = 5;
        iArr3[_TAMA_2] = 4;
        iArr3[_TAMA_3] = 8;
        iArr3[76] = 8;
        iArr3[_TAMA_5] = 8;
        iArr3[_TAMA_6] = 8;
        iArr3[_TAMA_7] = 8;
        iArr3[SAVE_HISCORE5] = 8;
        iArr3[81] = 8;
        iArr3[82] = 8;
        iArr3[_MENUCUR] = 8;
        iArr3[84] = 8;
        iArr3[85] = 8;
        iArr3[86] = 8;
        iArr3[87] = 8;
        iArr3[SAVE_HISCORE7] = 8;
        iArr3[89] = 8;
        iArr3[90] = 8;
        iArr3[91] = 5;
        iArr3[SAVE_HISCORE8] = 4;
        iArr3[93] = 8;
        iArr3[94] = 8;
        iArr3[95] = 8;
        iArr3[SAVE_HISCORE9] = 8;
        iArr3[97] = 8;
        iArr3[98] = 8;
        iArr3[99] = 8;
        iArr3[SAVE_HISCORE10] = 8;
        iArr3[101] = 8;
        iArr3[102] = 8;
        iArr3[103] = 8;
        iArr3[SAVE_HISCORE11] = 8;
        iArr3[105] = 8;
        iArr3[106] = 8;
        iArr3[107] = 8;
        iArr3[SAVE_HISCORE12] = 8;
        iArr3[109] = 5;
        iArr3[110] = 4;
        iArr3[111] = 8;
        iArr3[SAVE_HISCORE13] = 8;
        iArr3[113] = 8;
        iArr3[114] = 8;
        iArr3[115] = 8;
        iArr3[SAVE_HISCORE14] = 8;
        iArr3[117] = 8;
        iArr3[118] = 8;
        iArr3[119] = 8;
        iArr3[SAVE_HISCORE15] = 8;
        iArr3[121] = 8;
        iArr3[122] = 8;
        iArr3[123] = 8;
        iArr3[SAVE_HISCORE16] = 8;
        iArr3[125] = 8;
        iArr3[126] = 8;
        iArr3[127] = 5;
        iArr3[SAVE_HISCORE17] = 2;
        iArr3[129] = 7;
        iArr3[130] = 7;
        iArr3[131] = 7;
        iArr3[SAVE_HISCORE18] = 7;
        iArr3[133] = 7;
        iArr3[134] = 7;
        iArr3[135] = 7;
        iArr3[SAVE_HISCORE19] = 7;
        iArr3[137] = 7;
        iArr3[138] = 7;
        iArr3[139] = 7;
        iArr3[SAVE_HISCORE20] = 7;
        iArr3[141] = 7;
        iArr3[142] = 7;
        iArr3[143] = 7;
        iArr3[144] = 7;
        iArr3[145] = 3;
        Window_Load = iArr3;
    }

    public Screen(Context context, SurfaceView sv) {
        super(context);
        int[] iArr = new int[70];
        iArr[0] = 1;
        iArr[1] = SAVE_HISCORE9;
        iArr[2] = 176;
        iArr[3] = 496;
        iArr[4] = 576;
        iArr[5] = 2;
        iArr[6] = SAVE_HISCORE9;
        iArr[7] = 176;
        iArr[8] = 704;
        iArr[9] = 784;
        iArr[10] = 3;
        iArr[12] = SAVE_HISCORE5;
        iArr[13] = 592;
        iArr[14] = 688;
        iArr[KEY_POUND] = 4;
        iArr[16] = 208;
        iArr[WINDOW_NET] = 288;
        iArr[18] = 592;
        iArr[19] = 688;
        iArr[KEY_SOFT1] = SAVE_HISCORE9;
        iArr[22] = 192;
        iArr[_SELCURSOLX] = 592;
        iArr[_SELCURSOLY] = 688;
        iArr[_MOVEX] = KEY_SOFT1;
        iArr[27] = 144;
        iArr[_TIME] = SAVE_HISCORE1;
        iArr[_TITLE_MODE] = 22;
        iArr[31] = 656;
        iArr[AD_VIB] = 800;
        iArr[34] = SAVE_HISCORE1;
        iArr[35] = 1;
        iArr[_TENMETU] = SAVE_HISCORE5;
        iArr[38] = SAVE_HISCORE9;
        iArr[39] = SAVE_HISCORE19;
        iArr[SAVE_INIT] = 2;
        iArr[42] = SAVE_HISCORE5;
        iArr[_BCOLOR] = 192;
        iArr[SAVE_VER] = 232;
        iArr[45] = 3;
        iArr[47] = SAVE_INIT;
        iArr[48] = SAVE_HISCORE19;
        iArr[_TAMA2] = 192;
        iArr[_TAMA3] = 4;
        iArr[51] = SAVE_INIT;
        iArr[52] = SAVE_HISCORE5;
        iArr[53] = SAVE_HISCORE19;
        iArr[54] = 192;
        iArr[56] = 336;
        iArr[_CTIME2] = 384;
        iArr[58] = 168;
        iArr[59] = 216;
        iArr[60] = KEY_SOFT1;
        iArr[62] = SAVE_HISCORE3;
        iArr[SAVE_HISCORE1] = AD_VIB;
        iArr[65] = 22;
        iArr[66] = 328;
        iArr[67] = 400;
        iArr[_PLAYTIME] = AD_VIB;
        this.KeyXY = iArr;
        this.GameStr = new String[400];
        this.GameStrX = new int[400];
        this.GameStrY = new int[400];
        this.GameStrC = new int[400];
        this.mholder = getHolder();
        this.mholder.addCallback(this);
        setFocusable(true);
        requestFocus();
        this._app = context;
        this.iSys[52] = 47;
        this.iSys[7] = 0;
        this.iSys[4] = 4;
        for (int c = 0; c < 5; c++) {
            this.MsgBoxStr[c] = "";
        }
        this.paint = new Paint();
        this.paint.setAntiAlias(true);
        this.level = 0;
        this.sC1 = "";
        this.sC2 = "";
        this._sC1 = "";
        this._sC2 = "";
    }

    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            this.width = getWidth();
            this.height = getHeight();
            this.scalex = ((float) this.width) / 400.0f;
            this.scaley = ((float) this.height) / 240.0f;
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        this.mainLoop = null;
        save();
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width2, int height2) {
    }

    public void surfaceCreated(SurfaceHolder holder) {
        this.mholder.setFixedSize(getWidth(), getHeight());
        this.mainLoop = new Thread(this);
        this.offs = Bitmap.createBitmap(400, 240, Bitmap.Config.ARGB_8888);
        this.g = new Canvas(this.offs);
        this.mainLoop.start();
    }

    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case 0:
                this.Tx = (int) event.getX();
                this.Ty = (int) event.getY();
                return true;
            case 1:
                this.Tx = -1;
                this.Ty = -1;
                this.RepeartKey = 0;
                return true;
            default:
                return true;
        }
    }

    /* JADX WARNING: CFG modification limit reached, blocks count: 184 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r22 = this;
            long r18 = java.lang.System.currentTimeMillis()
        L_0x0004:
            r0 = r22
            java.lang.Thread r0 = r0.mainLoop
            r5 = r0
            if (r5 != 0) goto L_0x0018
            r0 = r22
            int r0 = r0.endf
            r5 = r0
            if (r5 == 0) goto L_0x0018
            return
        L_0x0013:
            r5 = 40
            java.lang.Thread.sleep(r5)     // Catch:{ Exception -> 0x03d0 }
        L_0x0018:
            long r20 = java.lang.System.currentTimeMillis()
            long r5 = r20 - r18
            r0 = r22
            int[] r0 = r0.iSys
            r7 = r0
            r8 = 52
            r7 = r7[r8]
            long r7 = (long) r7
            int r5 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
            if (r5 < 0) goto L_0x0013
            long r18 = java.lang.System.currentTimeMillis()
            r0 = r22
            android.view.SurfaceHolder r0 = r0.mholder     // Catch:{ Exception -> 0x0133 }
            r5 = r0
            android.graphics.Canvas r17 = r5.lockCanvas()     // Catch:{ Exception -> 0x0133 }
            r16 = 0
        L_0x003b:
            r5 = 7
            r0 = r16
            r1 = r5
            if (r0 < r1) goto L_0x0136
            r5 = 0
            r0 = r5
            r1 = r22
            r1.GameStrCnt = r0     // Catch:{ Exception -> 0x0133 }
            r0 = r22
            int[] r0 = r0.iSys     // Catch:{ Exception -> 0x0133 }
            r5 = r0
            r6 = 4
            r5 = r5[r6]     // Catch:{ Exception -> 0x0133 }
            switch(r5) {
                case 1: goto L_0x027e;
                case 3: goto L_0x027e;
                case 4: goto L_0x0279;
                case 6: goto L_0x0283;
                case 7: goto L_0x0283;
                case 9: goto L_0x0283;
                case 10: goto L_0x0283;
                case 12: goto L_0x0283;
                case 13: goto L_0x0283;
                case 20: goto L_0x0288;
                case 36: goto L_0x0283;
                case 180: goto L_0x0283;
                case 1000: goto L_0x027e;
                case 1001: goto L_0x027e;
                case 1002: goto L_0x027e;
                case 1003: goto L_0x027e;
                case 1004: goto L_0x027e;
                case 1005: goto L_0x027e;
                case 1006: goto L_0x027e;
                default: goto L_0x0052;
            }     // Catch:{ Exception -> 0x0133 }
        L_0x0052:
            r0 = r22
            java.lang.String r0 = r0._sC1     // Catch:{ Exception -> 0x0133 }
            r5 = r0
            r0 = r22
            java.lang.String r0 = r0.sC1     // Catch:{ Exception -> 0x0133 }
            r6 = r0
            if (r5 != r6) goto L_0x006a
            r0 = r22
            java.lang.String r0 = r0._sC2     // Catch:{ Exception -> 0x0133 }
            r5 = r0
            r0 = r22
            java.lang.String r0 = r0.sC2     // Catch:{ Exception -> 0x0133 }
            r6 = r0
            if (r5 == r6) goto L_0x007e
        L_0x006a:
            r0 = r22
            java.lang.String r0 = r0.sC1     // Catch:{ Exception -> 0x0133 }
            r5 = r0
            r0 = r5
            r1 = r22
            r1._sC1 = r0     // Catch:{ Exception -> 0x0133 }
            r0 = r22
            java.lang.String r0 = r0.sC2     // Catch:{ Exception -> 0x0133 }
            r5 = r0
            r0 = r5
            r1 = r22
            r1._sC2 = r0     // Catch:{ Exception -> 0x0133 }
        L_0x007e:
            int r5 = r22.getWidth()     // Catch:{ Exception -> 0x0133 }
            int r6 = r22.getHeight()     // Catch:{ Exception -> 0x0133 }
            if (r5 >= r6) goto L_0x028d
            int r5 = r22.getWidth()     // Catch:{ Exception -> 0x0133 }
            int r5 = r5 / 240
            float r5 = (float) r5     // Catch:{ Exception -> 0x0133 }
            int r6 = r22.getWidth()     // Catch:{ Exception -> 0x0133 }
            int r6 = r6 / 240
            float r6 = (float) r6     // Catch:{ Exception -> 0x0133 }
            r0 = r17
            r1 = r5
            r2 = r6
            r0.scale(r1, r2)     // Catch:{ Exception -> 0x0133 }
            r0 = r22
            android.graphics.Bitmap r0 = r0.offs     // Catch:{ Exception -> 0x0133 }
            r5 = r0
            r6 = 0
            r7 = 0
            r8 = 0
            r0 = r17
            r1 = r5
            r2 = r6
            r3 = r7
            r4 = r8
            r0.drawBitmap(r1, r2, r3, r4)     // Catch:{ Exception -> 0x0133 }
            r0 = r22
            int[] r0 = r0.iSys     // Catch:{ Exception -> 0x0133 }
            r5 = r0
            r6 = 4
            r5 = r5[r6]     // Catch:{ Exception -> 0x0133 }
            r6 = 4
            if (r5 == r6) goto L_0x00d6
            r5 = 1056964608(0x3f000000, float:0.5)
            r6 = 1056964608(0x3f000000, float:0.5)
            r0 = r17
            r1 = r5
            r2 = r6
            r0.scale(r1, r2)     // Catch:{ Exception -> 0x0133 }
            android.graphics.Bitmap[] r5 = buunyan.pzl.kurukuru.Screen.Img     // Catch:{ Exception -> 0x0133 }
            r6 = 5
            r5 = r5[r6]     // Catch:{ Exception -> 0x0133 }
            r6 = 0
            r7 = 1139802112(0x43f00000, float:480.0)
            r8 = 0
            r0 = r17
            r1 = r5
            r2 = r6
            r3 = r7
            r4 = r8
            r0.drawBitmap(r1, r2, r3, r4)     // Catch:{ Exception -> 0x0133 }
        L_0x00d6:
            r5 = 352(0x160, float:4.93E-43)
            r6 = 572(0x23c, float:8.02E-43)
            r0 = r22
            java.lang.String r0 = r0._sC1     // Catch:{ Exception -> 0x0133 }
            r7 = r0
            r0 = r22
            r1 = r17
            r2 = r5
            r3 = r6
            r4 = r7
            r0.PutStr(r1, r2, r3, r4)     // Catch:{ Exception -> 0x0133 }
            r5 = 352(0x160, float:4.93E-43)
            r6 = 700(0x2bc, float:9.81E-43)
            r0 = r22
            java.lang.String r0 = r0._sC2     // Catch:{ Exception -> 0x0133 }
            r7 = r0
            r0 = r22
            r1 = r17
            r2 = r5
            r3 = r6
            r4 = r7
            r0.PutStr(r1, r2, r3, r4)     // Catch:{ Exception -> 0x0133 }
        L_0x00fc:
            r5 = 1065353216(0x3f800000, float:1.0)
            r6 = 1065353216(0x3f800000, float:1.0)
            r0 = r17
            r1 = r5
            r2 = r6
            r0.scale(r1, r2)     // Catch:{ Exception -> 0x0133 }
            r0 = r22
            android.graphics.Paint r0 = r0.paint     // Catch:{ Exception -> 0x0133 }
            r5 = r0
            r6 = 1103101952(0x41c00000, float:24.0)
            r5.setTextSize(r6)     // Catch:{ Exception -> 0x0133 }
            r16 = 0
        L_0x0113:
            r0 = r22
            int r0 = r0.GameStrCnt     // Catch:{ Exception -> 0x0133 }
            r5 = r0
            r0 = r16
            r1 = r5
            if (r0 < r1) goto L_0x0368
            r16 = 0
        L_0x011f:
            r5 = 500(0x1f4, float:7.0E-43)
            r0 = r16
            r1 = r5
            if (r0 < r1) goto L_0x03be
            r0 = r22
            android.view.SurfaceHolder r0 = r0.mholder     // Catch:{ Exception -> 0x0133 }
            r5 = r0
            r0 = r5
            r1 = r17
            r0.unlockCanvasAndPost(r1)     // Catch:{ Exception -> 0x0133 }
            goto L_0x0004
        L_0x0133:
            r5 = move-exception
            goto L_0x0004
        L_0x0136:
            int r5 = r22.getWidth()     // Catch:{ Exception -> 0x0133 }
            int r6 = r22.getHeight()     // Catch:{ Exception -> 0x0133 }
            if (r5 >= r6) goto L_0x01d7
            r0 = r22
            int r0 = r0.Tx     // Catch:{ Exception -> 0x0133 }
            r5 = r0
            float r5 = (float) r5     // Catch:{ Exception -> 0x0133 }
            r0 = r22
            int[] r0 = r0.KeyXY     // Catch:{ Exception -> 0x0133 }
            r6 = r0
            int r7 = r16 * 5
            int r7 = r7 + 1
            r6 = r6[r7]     // Catch:{ Exception -> 0x0133 }
            float r6 = (float) r6     // Catch:{ Exception -> 0x0133 }
            r0 = r22
            float r0 = r0.scalex     // Catch:{ Exception -> 0x0133 }
            r7 = r0
            float r6 = r6 * r7
            int r5 = (r5 > r6 ? 1 : (r5 == r6 ? 0 : -1))
            if (r5 < 0) goto L_0x01c5
            r0 = r22
            int r0 = r0.Tx     // Catch:{ Exception -> 0x0133 }
            r5 = r0
            float r5 = (float) r5     // Catch:{ Exception -> 0x0133 }
            r0 = r22
            int[] r0 = r0.KeyXY     // Catch:{ Exception -> 0x0133 }
            r6 = r0
            int r7 = r16 * 5
            int r7 = r7 + 2
            r6 = r6[r7]     // Catch:{ Exception -> 0x0133 }
            float r6 = (float) r6     // Catch:{ Exception -> 0x0133 }
            r0 = r22
            float r0 = r0.scalex     // Catch:{ Exception -> 0x0133 }
            r7 = r0
            float r6 = r6 * r7
            int r5 = (r5 > r6 ? 1 : (r5 == r6 ? 0 : -1))
            if (r5 >= 0) goto L_0x01c5
            r0 = r22
            int r0 = r0.Ty     // Catch:{ Exception -> 0x0133 }
            r5 = r0
            float r5 = (float) r5     // Catch:{ Exception -> 0x0133 }
            r0 = r22
            int[] r0 = r0.KeyXY     // Catch:{ Exception -> 0x0133 }
            r6 = r0
            int r7 = r16 * 5
            int r7 = r7 + 3
            r6 = r6[r7]     // Catch:{ Exception -> 0x0133 }
            float r6 = (float) r6     // Catch:{ Exception -> 0x0133 }
            r0 = r22
            float r0 = r0.scaley     // Catch:{ Exception -> 0x0133 }
            r7 = r0
            float r6 = r6 * r7
            int r5 = (r5 > r6 ? 1 : (r5 == r6 ? 0 : -1))
            if (r5 < 0) goto L_0x01c5
            r0 = r22
            int r0 = r0.Ty     // Catch:{ Exception -> 0x0133 }
            r5 = r0
            float r5 = (float) r5     // Catch:{ Exception -> 0x0133 }
            r0 = r22
            int[] r0 = r0.KeyXY     // Catch:{ Exception -> 0x0133 }
            r6 = r0
            int r7 = r16 * 5
            int r7 = r7 + 4
            r6 = r6[r7]     // Catch:{ Exception -> 0x0133 }
            float r6 = (float) r6     // Catch:{ Exception -> 0x0133 }
            r0 = r22
            float r0 = r0.scaley     // Catch:{ Exception -> 0x0133 }
            r7 = r0
            float r6 = r6 * r7
            int r5 = (r5 > r6 ? 1 : (r5 == r6 ? 0 : -1))
            if (r5 >= 0) goto L_0x01c5
            r0 = r22
            int[] r0 = r0.KeyFlag     // Catch:{ Exception -> 0x0133 }
            r5 = r0
            r0 = r22
            int[] r0 = r0.KeyXY     // Catch:{ Exception -> 0x0133 }
            r6 = r0
            int r7 = r16 * 5
            r6 = r6[r7]     // Catch:{ Exception -> 0x0133 }
            r7 = 1
            r5[r6] = r7     // Catch:{ Exception -> 0x0133 }
        L_0x01c1:
            int r16 = r16 + 1
            goto L_0x003b
        L_0x01c5:
            r0 = r22
            int[] r0 = r0.KeyFlag     // Catch:{ Exception -> 0x0133 }
            r5 = r0
            r0 = r22
            int[] r0 = r0.KeyXY     // Catch:{ Exception -> 0x0133 }
            r6 = r0
            int r7 = r16 * 5
            r6 = r6[r7]     // Catch:{ Exception -> 0x0133 }
            r7 = 0
            r5[r6] = r7     // Catch:{ Exception -> 0x0133 }
            goto L_0x01c1
        L_0x01d7:
            r0 = r22
            int r0 = r0.Tx     // Catch:{ Exception -> 0x0133 }
            r5 = r0
            float r5 = (float) r5     // Catch:{ Exception -> 0x0133 }
            r0 = r22
            int[] r0 = r0.KeyXY     // Catch:{ Exception -> 0x0133 }
            r6 = r0
            int r7 = r16 + 7
            int r7 = r7 * 5
            int r7 = r7 + 1
            r6 = r6[r7]     // Catch:{ Exception -> 0x0133 }
            float r6 = (float) r6     // Catch:{ Exception -> 0x0133 }
            r0 = r22
            float r0 = r0.scalex     // Catch:{ Exception -> 0x0133 }
            r7 = r0
            float r6 = r6 * r7
            int r5 = (r5 > r6 ? 1 : (r5 == r6 ? 0 : -1))
            if (r5 < 0) goto L_0x0264
            r0 = r22
            int r0 = r0.Tx     // Catch:{ Exception -> 0x0133 }
            r5 = r0
            float r5 = (float) r5     // Catch:{ Exception -> 0x0133 }
            r0 = r22
            int[] r0 = r0.KeyXY     // Catch:{ Exception -> 0x0133 }
            r6 = r0
            int r7 = r16 + 7
            int r7 = r7 * 5
            int r7 = r7 + 2
            r6 = r6[r7]     // Catch:{ Exception -> 0x0133 }
            float r6 = (float) r6     // Catch:{ Exception -> 0x0133 }
            r0 = r22
            float r0 = r0.scalex     // Catch:{ Exception -> 0x0133 }
            r7 = r0
            float r6 = r6 * r7
            int r5 = (r5 > r6 ? 1 : (r5 == r6 ? 0 : -1))
            if (r5 >= 0) goto L_0x0264
            r0 = r22
            int r0 = r0.Ty     // Catch:{ Exception -> 0x0133 }
            r5 = r0
            float r5 = (float) r5     // Catch:{ Exception -> 0x0133 }
            r0 = r22
            int[] r0 = r0.KeyXY     // Catch:{ Exception -> 0x0133 }
            r6 = r0
            int r7 = r16 + 7
            int r7 = r7 * 5
            int r7 = r7 + 3
            r6 = r6[r7]     // Catch:{ Exception -> 0x0133 }
            float r6 = (float) r6     // Catch:{ Exception -> 0x0133 }
            r0 = r22
            float r0 = r0.scaley     // Catch:{ Exception -> 0x0133 }
            r7 = r0
            float r6 = r6 * r7
            int r5 = (r5 > r6 ? 1 : (r5 == r6 ? 0 : -1))
            if (r5 < 0) goto L_0x0264
            r0 = r22
            int r0 = r0.Ty     // Catch:{ Exception -> 0x0133 }
            r5 = r0
            float r5 = (float) r5     // Catch:{ Exception -> 0x0133 }
            r0 = r22
            int[] r0 = r0.KeyXY     // Catch:{ Exception -> 0x0133 }
            r6 = r0
            int r7 = r16 + 7
            int r7 = r7 * 5
            int r7 = r7 + 4
            r6 = r6[r7]     // Catch:{ Exception -> 0x0133 }
            float r6 = (float) r6     // Catch:{ Exception -> 0x0133 }
            r0 = r22
            float r0 = r0.scaley     // Catch:{ Exception -> 0x0133 }
            r7 = r0
            float r6 = r6 * r7
            int r5 = (r5 > r6 ? 1 : (r5 == r6 ? 0 : -1))
            if (r5 >= 0) goto L_0x0264
            r0 = r22
            int[] r0 = r0.KeyFlag     // Catch:{ Exception -> 0x0133 }
            r5 = r0
            r0 = r22
            int[] r0 = r0.KeyXY     // Catch:{ Exception -> 0x0133 }
            r6 = r0
            int r7 = r16 + 7
            int r7 = r7 * 5
            r6 = r6[r7]     // Catch:{ Exception -> 0x0133 }
            r7 = 1
            r5[r6] = r7     // Catch:{ Exception -> 0x0133 }
            goto L_0x01c1
        L_0x0264:
            r0 = r22
            int[] r0 = r0.KeyFlag     // Catch:{ Exception -> 0x0133 }
            r5 = r0
            r0 = r22
            int[] r0 = r0.KeyXY     // Catch:{ Exception -> 0x0133 }
            r6 = r0
            int r7 = r16 + 7
            int r7 = r7 * 5
            r6 = r6[r7]     // Catch:{ Exception -> 0x0133 }
            r7 = 0
            r5[r6] = r7     // Catch:{ Exception -> 0x0133 }
            goto L_0x01c1
        L_0x0279:
            r22.Loop_Init()     // Catch:{ Exception -> 0x0133 }
            goto L_0x0052
        L_0x027e:
            r22.Loop_Title()     // Catch:{ Exception -> 0x0133 }
            goto L_0x0052
        L_0x0283:
            r22.Loop_Game()     // Catch:{ Exception -> 0x0133 }
            goto L_0x0052
        L_0x0288:
            r22.Loop_Help()     // Catch:{ Exception -> 0x0133 }
            goto L_0x0052
        L_0x028d:
            r0 = r22
            java.lang.String r0 = r0._sC1     // Catch:{ Exception -> 0x0133 }
            r5 = r0
            java.lang.String r6 = "戻る"
            boolean r5 = r5.equals(r6)     // Catch:{ Exception -> 0x0133 }
            if (r5 == 0) goto L_0x02b7
            r0 = r22
            android.graphics.Canvas r0 = r0.g     // Catch:{ Exception -> 0x0133 }
            r6 = r0
            android.graphics.Bitmap[] r5 = buunyan.pzl.kurukuru.Screen.Img     // Catch:{ Exception -> 0x0133 }
            r7 = 4
            r7 = r5[r7]     // Catch:{ Exception -> 0x0133 }
            r8 = 0
            r9 = 0
            r10 = 72
            r11 = 24
            r12 = 72
            r13 = 96
            r14 = 72
            r15 = 24
            r5 = r22
            r5.drawImageScale(r6, r7, r8, r9, r10, r11, r12, r13, r14, r15)     // Catch:{ Exception -> 0x0133 }
        L_0x02b7:
            r0 = r22
            java.lang.String r0 = r0._sC1     // Catch:{ Exception -> 0x0133 }
            r5 = r0
            java.lang.String r6 = "ﾒﾆｭｰ"
            boolean r5 = r5.equals(r6)     // Catch:{ Exception -> 0x0133 }
            if (r5 == 0) goto L_0x02e1
            r0 = r22
            android.graphics.Canvas r0 = r0.g     // Catch:{ Exception -> 0x0133 }
            r6 = r0
            android.graphics.Bitmap[] r5 = buunyan.pzl.kurukuru.Screen.Img     // Catch:{ Exception -> 0x0133 }
            r7 = 4
            r7 = r5[r7]     // Catch:{ Exception -> 0x0133 }
            r8 = 0
            r9 = 0
            r10 = 72
            r11 = 24
            r12 = 72
            r13 = 96
            r14 = 72
            r15 = 24
            r5 = r22
            r5.drawImageScale(r6, r7, r8, r9, r10, r11, r12, r13, r14, r15)     // Catch:{ Exception -> 0x0133 }
        L_0x02e1:
            r0 = r22
            java.lang.String r0 = r0._sC2     // Catch:{ Exception -> 0x0133 }
            r5 = r0
            java.lang.String r6 = "ｻｲﾄ"
            boolean r5 = r5.equals(r6)     // Catch:{ Exception -> 0x0133 }
            if (r5 == 0) goto L_0x030b
            r0 = r22
            android.graphics.Canvas r0 = r0.g     // Catch:{ Exception -> 0x0133 }
            r6 = r0
            android.graphics.Bitmap[] r5 = buunyan.pzl.kurukuru.Screen.Img     // Catch:{ Exception -> 0x0133 }
            r7 = 4
            r7 = r5[r7]     // Catch:{ Exception -> 0x0133 }
            r8 = 328(0x148, float:4.6E-43)
            r9 = 0
            r10 = 72
            r11 = 24
            r12 = 0
            r13 = 96
            r14 = 72
            r15 = 24
            r5 = r22
            r5.drawImageScale(r6, r7, r8, r9, r10, r11, r12, r13, r14, r15)     // Catch:{ Exception -> 0x0133 }
        L_0x030b:
            r0 = r22
            float r0 = r0.scalex     // Catch:{ Exception -> 0x0133 }
            r5 = r0
            r0 = r22
            float r0 = r0.scaley     // Catch:{ Exception -> 0x0133 }
            r6 = r0
            r0 = r17
            r1 = r5
            r2 = r6
            r0.scale(r1, r2)     // Catch:{ Exception -> 0x0133 }
            r0 = r22
            android.graphics.Bitmap r0 = r0.offs     // Catch:{ Exception -> 0x0133 }
            r5 = r0
            r6 = 0
            r7 = 0
            r8 = 0
            r0 = r17
            r1 = r5
            r2 = r6
            r3 = r7
            r4 = r8
            r0.drawBitmap(r1, r2, r3, r4)     // Catch:{ Exception -> 0x0133 }
            r0 = r22
            int[] r0 = r0.iSys     // Catch:{ Exception -> 0x0133 }
            r5 = r0
            r6 = 4
            r5 = r5[r6]     // Catch:{ Exception -> 0x0133 }
            r6 = 4
            if (r5 == r6) goto L_0x00fc
            r5 = 1056964608(0x3f000000, float:0.5)
            r6 = 1056964608(0x3f000000, float:0.5)
            r0 = r17
            r1 = r5
            r2 = r6
            r0.scale(r1, r2)     // Catch:{ Exception -> 0x0133 }
            android.graphics.Bitmap[] r5 = buunyan.pzl.kurukuru.Screen.Img     // Catch:{ Exception -> 0x0133 }
            r6 = 6
            r5 = r5[r6]     // Catch:{ Exception -> 0x0133 }
            r6 = 0
            r7 = 0
            r8 = 0
            r0 = r17
            r1 = r5
            r2 = r6
            r3 = r7
            r4 = r8
            r0.drawBitmap(r1, r2, r3, r4)     // Catch:{ Exception -> 0x0133 }
            android.graphics.Bitmap[] r5 = buunyan.pzl.kurukuru.Screen.Img     // Catch:{ Exception -> 0x0133 }
            r6 = 7
            r5 = r5[r6]     // Catch:{ Exception -> 0x0133 }
            r6 = 1142947840(0x44200000, float:640.0)
            r7 = 0
            r8 = 0
            r0 = r17
            r1 = r5
            r2 = r6
            r3 = r7
            r4 = r8
            r0.drawBitmap(r1, r2, r3, r4)     // Catch:{ Exception -> 0x0133 }
            goto L_0x00fc
        L_0x0368:
            r0 = r22
            int[] r0 = r0.GameStrC     // Catch:{ Exception -> 0x0133 }
            r5 = r0
            r5 = r5[r16]     // Catch:{ Exception -> 0x0133 }
            int r5 = r5 >> 16
            r5 = r5 & 255(0xff, float:3.57E-43)
            r0 = r22
            int[] r0 = r0.GameStrC     // Catch:{ Exception -> 0x0133 }
            r6 = r0
            r6 = r6[r16]     // Catch:{ Exception -> 0x0133 }
            int r6 = r6 >> 8
            r6 = r6 & 255(0xff, float:3.57E-43)
            r0 = r22
            int[] r0 = r0.GameStrC     // Catch:{ Exception -> 0x0133 }
            r7 = r0
            r7 = r7[r16]     // Catch:{ Exception -> 0x0133 }
            int r7 = r7 >> 16
            r0 = r22
            r1 = r17
            r2 = r5
            r3 = r6
            r4 = r7
            r0.setColor(r1, r2, r3, r4)     // Catch:{ Exception -> 0x0133 }
            r0 = r22
            java.lang.String[] r0 = r0.GameStr     // Catch:{ Exception -> 0x0133 }
            r5 = r0
            r5 = r5[r16]     // Catch:{ Exception -> 0x0133 }
            r0 = r22
            int[] r0 = r0.GameStrX     // Catch:{ Exception -> 0x0133 }
            r6 = r0
            r6 = r6[r16]     // Catch:{ Exception -> 0x0133 }
            int r6 = r6 * 2
            float r6 = (float) r6     // Catch:{ Exception -> 0x0133 }
            r0 = r22
            int[] r0 = r0.GameStrY     // Catch:{ Exception -> 0x0133 }
            r7 = r0
            r7 = r7[r16]     // Catch:{ Exception -> 0x0133 }
            int r7 = r7 * 2
            float r7 = (float) r7     // Catch:{ Exception -> 0x0133 }
            r0 = r22
            android.graphics.Paint r0 = r0.paint     // Catch:{ Exception -> 0x0133 }
            r8 = r0
            r0 = r17
            r1 = r5
            r2 = r6
            r3 = r7
            r4 = r8
            r0.drawText(r1, r2, r3, r4)     // Catch:{ Exception -> 0x0133 }
            int r16 = r16 + 1
            goto L_0x0113
        L_0x03be:
            r0 = r22
            int[] r0 = r0.KeyFlagOld     // Catch:{ Exception -> 0x0133 }
            r5 = r0
            r0 = r22
            int[] r0 = r0.KeyFlag     // Catch:{ Exception -> 0x0133 }
            r6 = r0
            r6 = r6[r16]     // Catch:{ Exception -> 0x0133 }
            r5[r16] = r6     // Catch:{ Exception -> 0x0133 }
            int r16 = r16 + 1
            goto L_0x011f
        L_0x03d0:
            r5 = move-exception
            goto L_0x0018
        */
        throw new UnsupportedOperationException("Method not decompiled: buunyan.pzl.kurukuru.Screen.run():void");
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00c4 A[LOOP:0: B:16:0x0096->B:24:0x00c4, LOOP_END] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void Loop_Init() {
        /*
            r13 = this;
            android.graphics.Canvas r0 = r13.g
            r1 = 0
            r2 = 0
            r3 = 0
            r13.setColor(r0, r1, r2, r3)
            android.graphics.Canvas r1 = r13.g
            r2 = 0
            r3 = 0
            r4 = 240(0xf0, float:3.36E-43)
            r5 = 240(0xf0, float:3.36E-43)
            r0 = r13
            r0.fillRect(r1, r2, r3, r4, r5)
            android.graphics.Canvas r0 = r13.g
            r1 = 255(0xff, float:3.57E-43)
            r2 = 255(0xff, float:3.57E-43)
            r3 = 255(0xff, float:3.57E-43)
            r13.setColor(r0, r1, r2, r3)
            android.graphics.Canvas r1 = r13.g
            r2 = 138(0x8a, float:1.93E-43)
            r3 = 122(0x7a, float:1.71E-43)
            r4 = 123(0x7b, float:1.72E-43)
            r5 = 23
            r0 = r13
            r0.drawRect(r1, r2, r3, r4, r5)
            android.graphics.Canvas r0 = r13.g
            r1 = 255(0xff, float:3.57E-43)
            r2 = 255(0xff, float:3.57E-43)
            r3 = 0
            r13.setColor(r0, r1, r2, r3)
            int[] r0 = r13.iSys
            r1 = 2
            r0 = r0[r1]
            if (r0 <= 0) goto L_0x0055
            android.graphics.Canvas r1 = r13.g
            r2 = 140(0x8c, float:1.96E-43)
            r3 = 124(0x7c, float:1.74E-43)
            int[] r0 = r13.iSys
            r4 = 2
            r0 = r0[r4]
            r4 = 1
            int r0 = r0 - r4
            int r0 = r0 * 17
            r4 = r0 & 255(0xff, float:3.57E-43)
            r5 = 20
            r0 = r13
            r0.fillRect(r1, r2, r3, r4, r5)
        L_0x0055:
            int[] r0 = r13.iSys
            r1 = 7
            r0 = r0[r1]
            if (r0 != 0) goto L_0x00ad
            int[] r0 = r13.iSys
            r1 = 2
            r0 = r0[r1]
            r13.readGrp(r0)
            int[] r0 = r13.iSys
            r1 = 2
            r2 = r0[r1]
            int r2 = r2 + 1
            r0[r1] = r2
            int[] r0 = r13.iSys
            r1 = 2
            r0 = r0[r1]
            r1 = 8
            if (r0 <= r1) goto L_0x00ad
            r11 = 0
            android.content.Context r0 = r13.getContext()     // Catch:{ Exception -> 0x00be }
            java.lang.String r1 = "save_kurukuru.bin"
            java.io.FileInputStream r10 = r0.openFileInput(r1)     // Catch:{ Exception -> 0x00be }
            java.io.BufferedInputStream r12 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x00be }
            r12.<init>(r10)     // Catch:{ Exception -> 0x00be }
            byte[] r0 = r13.scratchpad     // Catch:{ Exception -> 0x00d3 }
            r12.read(r0)     // Catch:{ Exception -> 0x00d3 }
            r12.close()     // Catch:{ Exception -> 0x00d3 }
            r11 = 0
            java.lang.System.gc()     // Catch:{ Exception -> 0x00be }
            r10.close()     // Catch:{ Exception -> 0x00be }
        L_0x0095:
            r8 = 0
        L_0x0096:
            r0 = 20
            if (r8 < r0) goto L_0x00c4
            java.lang.String[] r0 = r13.sjist
            java.lang.String r1 = "t.txt"
            r13.readText(r0, r1)
            int[] r0 = r13.iSys
            r1 = 4
            r2 = 1
            r0[r1] = r2
            int[] r0 = r13.iSys
            r1 = 7
            r2 = 0
            r0[r1] = r2
        L_0x00ad:
            android.graphics.Canvas r1 = r13.BG
            r2 = 0
            r3 = 16
            r4 = 24
            r5 = 64
            r6 = 128(0x80, float:1.794E-43)
            r7 = 1
            r0 = r13
            r0.GrPtn(r1, r2, r3, r4, r5, r6, r7)
            return
        L_0x00be:
            r0 = move-exception
            r9 = r0
        L_0x00c0:
            r9.printStackTrace()
            goto L_0x0095
        L_0x00c4:
            int[] r0 = r13.hiscore
            int r1 = r8 * 4
            int r1 = r1 + 64
            int r1 = r13.get(r1)
            r0[r8] = r1
            int r8 = r8 + 1
            goto L_0x0096
        L_0x00d3:
            r0 = move-exception
            r9 = r0
            r11 = r12
            goto L_0x00c0
        */
        throw new UnsupportedOperationException("Method not decompiled: buunyan.pzl.kurukuru.Screen.Loop_Init():void");
    }

    /* access modifiers changed from: package-private */
    public void Loop_Help() {
        for (int y = 0; y < 10; y++) {
            for (int x = 0; x < 2; x++) {
                drawImage(this.g, Img[2], (((-(y % 2)) * AD_VIB) + (x * 192)) - this.iSys[_MOVEX], (y * AD_VIB) - this.iSys[_MOVEY], 0, SAVE_INIT, 240, AD_VIB);
            }
        }
        drawImage(this.g, Img[0], SAVE_HISCORE5, 0);
        drawImage(this.g, Img[3], AD_VIB, 208);
        window_Show(this.g, 8);
        if (GetKey(3) == 1) {
            int[] iArr = this.iSys;
            iArr[22] = iArr[22] - 1;
            if (this.iSys[22] < 0) {
                this.iSys[22] = 0;
            }
        } else if (GetKey(4) == 1) {
            int[] iArr2 = this.iSys;
            iArr2[22] = iArr2[22] + 1;
            if (this.iSys[22] > 4) {
                this.iSys[22] = 4;
            }
        } else if (GetKey(0) == 1 || GetKey(KEY_SOFT1) == 1) {
            this.iSys[4] = 1;
            this.iSys[7] = 0;
        }
        if (this.iSys[22] > 0) {
            GrPtn(this.g, 22, 8, 8, SAVE_HISCORE9, 58, WINDOW_NET);
        }
        if (this.iSys[22] < 4) {
            GrPtn(this.g, 22, 8, 8, SAVE_HISCORE19, 58, 18);
        }
        XCenterString(this.g, 56, (this.iSys[22] + 1) + "/5");
        for (int c = 0; c < 6; c++) {
            XCenterString(this.g, (c * 16) + SAVE_HISCORE5, this.sjist[(this.iSys[22] * 6) + 16 + c]);
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 29 */
    /* access modifiers changed from: package-private */
    public void Loop_Title() {
        for (int y = 0; y < 10; y++) {
            for (int x = 0; x < 2; x++) {
                drawImage(this.g, Img[2], (((-(y % 2)) * AD_VIB) + (x * 192)) - this.iSys[_MOVEX], (y * AD_VIB) - this.iSys[_MOVEY], 0, SAVE_INIT, 240, AD_VIB);
            }
        }
        drawImage(this.g, Img[0], SAVE_HISCORE5, AD_VIB);
        drawImage(this.g, Img[3], SAVE_HISCORE13, 208);
        if (this.iSys[7] == 0) {
            this.iSys[_PLAYTIME] = 0;
            this.iSys[36] = 0;
            this.iSys[9] = 0;
            this.iSys[_TITLE_MODE] = 0;
            System.gc();
            this.iSys[_MENUCUR] = 0;
            this.iSys[22] = 0;
            this.iSys[_TITLE_MODE] = 0;
            readGrp(0);
            setMenu("終了", "ｻｲﾄ");
            set(52, this.iSys[16]);
            save();
            this.iSys[7] = 1;
        }
        if (this.iSys[4] == RANKINGE) {
            setColor(this.g, 0, 0, 0);
            fillRect(this.g, 0, 0, this.offs.getWidth(), this.offs.getHeight());
            XCenterString(this.g, SAVE_HISCORE13, "通信エラーです");
            if (GetKey(0) == 1) {
                this.iSys[4] = RANKING;
            }
        } else if (this.iSys[4] == RANKING2 || this.iSys[4] == RANKING3) {
            window_Show(this.g, WINDOW_NET);
            if (this.iSys[4] == RANKING2) {
                setMenu("", "");
                this.iSys[4] = RANKING3;
                return;
            }
            try {
                byte[] w = new byte[1024];
                HttpURLConnection hc = (HttpURLConnection) new URL(this.domain + "game/ranking_s2.php?db=kurukuru&GUID=ON&v=" + "0" + "&s=" + this.hiscore[0] + "&s2=" + this.hiscore[10] + "&imei=" + this.imei).openConnection();
                hc.setRequestMethod("GET");
                hc.connect();
                InputStream in = hc.getInputStream();
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                while (true) {
                    int size = in.read(w);
                    if (size <= 0) {
                        out.close();
                        in.close();
                        hc.disconnect();
                        this._res = split(new String(out.toByteArray()), ",");
                        this.iSys[4] = RANKING4;
                        int[] iArr = this.iSys;
                        iArr[16] = iArr[16] | 8192;
                        return;
                    }
                    out.write(w, 0, size);
                }
            } catch (Exception e) {
                this.iSys[4] = RANKINGE;
            }
        } else if (this.iSys[4] == RANKING4) {
            window_Show(this.g, 2);
            if (this._res[2].equals("0")) {
                XCenterString(this.g, SAVE_HISCORE5, "ﾕｰｻﾞｰ登録がまだです。");
                XCenterString(this.g, SAVE_HISCORE9, "ﾗﾝｷﾝｸﾞに参加するには");
                XCenterString(this.g, SAVE_HISCORE13, "ﾕｰｻﾞｰ登録が必要です");
                XCenterString(this.g, SAVE_HISCORE17, "登録する");
                XCenterString(this.g, 144, "ﾀｲﾄﾙに戻る");
                GrPtn(this.g, 4, 8, 8, this.iSys[_TENMETU] + SAVE_HISCORE1 + SAVE_HISCORE5, (this.iSys[_MENUCUR] * 16) + 130, 8);
                int[] iArr2 = this.iSys;
                iArr2[_TENMETU] = iArr2[_TENMETU] + 1;
                if (this.iSys[_TENMETU] >= 5) {
                    this.iSys[_TENMETU] = 0;
                }
                if (GetKey(KEY_SOFT1) == 1) {
                    this.iSys[7] = 0;
                    this.iSys[_MENUCUR] = 0;
                    this.iSys[4] = 1;
                } else if (GetKey(1) == 1) {
                    int[] iArr3 = this.iSys;
                    iArr3[_MENUCUR] = iArr3[_MENUCUR] - 1;
                    if (this.iSys[_MENUCUR] < 0) {
                        this.iSys[_MENUCUR] = 1;
                    }
                } else if (GetKey(2) == 1) {
                    int[] iArr4 = this.iSys;
                    iArr4[_MENUCUR] = iArr4[_MENUCUR] + 1;
                    if (this.iSys[_MENUCUR] > 1) {
                        this.iSys[_MENUCUR] = 0;
                    }
                } else if (GetKey(0) != 1) {
                } else {
                    if (this.iSys[_MENUCUR] == 0) {
                        try {
                            this.mHandler.post(new Runnable() {
                                public void run() {
                                    final EditText editText = new EditText(Screen.this._app);
                                    AlertDialog.Builder ad = new AlertDialog.Builder(Screen.this._app);
                                    ad.setTitle("名前が登録されていません");
                                    ad.setView(editText);
                                    ad.setMessage("ﾗﾝｷﾝｸﾞに登録する名前を入力してください");
                                    ad.setPositiveButton("はい", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int whichButton) {
                                            try {
                                                byte[] w = new byte[1024];
                                                HttpURLConnection hc = (HttpURLConnection) new URL(String.valueOf(Screen.this.domain) + "game/include/setuser.php?uid=" + Screen.this.imei + "&mode=add&ssID=" + editText.getText().toString()).openConnection();
                                                hc.setRequestMethod("GET");
                                                hc.connect();
                                                InputStream in = hc.getInputStream();
                                                ByteArrayOutputStream out = new ByteArrayOutputStream();
                                                while (true) {
                                                    int size = in.read(w);
                                                    if (size <= 0) {
                                                        out.close();
                                                        in.close();
                                                        hc.disconnect();
                                                        Screen.this.g_res = new String(out.toByteArray());
                                                        Screen.this.iSys[4] = Screen.RANKING;
                                                        return;
                                                    }
                                                    out.write(w, 0, size);
                                                }
                                            } catch (Exception e) {
                                                Screen.this.iSys[4] = Screen.RANKINGE;
                                            }
                                        }
                                    });
                                    ad.setNegativeButton("いいえ", (DialogInterface.OnClickListener) null);
                                    ad.show();
                                }
                            });
                        } catch (Exception e2) {
                            e2.printStackTrace();
                        }
                        if (this.g_res.charAt(0) == '0') {
                            this.iSys[4] = RANKING2;
                            return;
                        }
                        this.iSys[_TITLE_MODE] = 0;
                        this.iSys[7] = 0;
                        this.iSys[_MENUCUR] = 0;
                        this.iSys[4] = 1;
                        return;
                    }
                    this.iSys[7] = 0;
                    this.iSys[_MENUCUR] = 0;
                    this.iSys[4] = 1;
                }
            } else {
                try {
                    XCenterString(this.g, SAVE_HISCORE9, "あなたは" + this._res[4] + "人中");
                    XCenterString(this.g, SAVE_HISCORE13, this._res[3] + "位です");
                    XCenterString(this.g, 144, "決定ｷｰでﾀｲﾄﾙに戻ります");
                } catch (Exception e3) {
                }
                if (GetKey(KEY_SOFT1) == 1 || GetKey(0) == 1) {
                    this.iSys[7] = 0;
                    this.iSys[_MENUCUR] = 0;
                    this.iSys[4] = 1;
                }
            }
        } else if (this.iSys[4] == RANKING || this.iSys[4] == RANKING_TT) {
            window_Show(this.g, 4);
            if (this.iSys[4] == RANKING) {
                XCenterString(this.g, 48, "チャレンジモード");
            } else {
                XCenterString(this.g, 48, "タイムトライアルモード");
            }
            GrPtn(this.g, 4, 8, 8, this.iSys[_TENMETU] + 56 + SAVE_HISCORE5, (this.iSys[_MENUCUR] * 16) + 188, 8);
            GrPtn(this.g, 4, 8, 8, SAVE_HISCORE19, SAVE_HISCORE15, WINDOW_NET);
            GrPtn(this.g, 4, 8, 8, 256, SAVE_HISCORE15, 18);
            XCenterString(this.g, 188, "ランキング送信");
            XCenterString(this.g, 204, "ランキング確認");
            for (int c = 0; c < 10; c++) {
                XCStringR(this.g, 168, (c * 12) + SAVE_HISCORE1, new StringBuilder().append(c + 1).toString());
                if (this.iSys[4] == RANKING) {
                    XCStringR(this.g, 240, (c * 12) + SAVE_HISCORE1, new StringBuilder().append(this.hiscore[c]).toString());
                } else {
                    XCStringR(this.g, 240, (c * 12) + SAVE_HISCORE1, new StringBuilder().append(this.hiscore[c + 10]).toString());
                }
            }
            if (GetKey(KEY_SOFT1) == 1) {
                this.iSys[_MENUCUR] = 0;
                this.iSys[4] = 1;
            } else if (GetKey(1) == 1) {
                int[] iArr5 = this.iSys;
                iArr5[_MENUCUR] = iArr5[_MENUCUR] - 1;
                if (this.iSys[_MENUCUR] < 0) {
                    this.iSys[_MENUCUR] = 1;
                }
            } else if (GetKey(2) == 1) {
                int[] iArr6 = this.iSys;
                iArr6[_MENUCUR] = iArr6[_MENUCUR] + 1;
                if (this.iSys[_MENUCUR] > 1) {
                    this.iSys[_MENUCUR] = 0;
                }
            } else if (GetKey(3) == 1) {
                if (this.iSys[4] == RANKING) {
                    this.iSys[4] = RANKING_TT;
                } else {
                    this.iSys[4] = RANKING;
                }
            } else if (GetKey(4) == 1) {
                if (this.iSys[4] == RANKING) {
                    this.iSys[4] = RANKING_TT;
                } else {
                    this.iSys[4] = RANKING;
                }
            } else if (GetKey(0) == 1) {
                if (this.iSys[_MENUCUR] == 0) {
                    this.iSys[4] = RANKING2;
                }
                if (this.iSys[_MENUCUR] == 1) {
                    try {
                        ((Activity) getContext()).startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://bu-nyan.bne.jp/game/kurukuru/rankview.php")));
                    } catch (Exception e4) {
                    }
                }
            }
            int[] iArr7 = this.iSys;
            iArr7[_TENMETU] = iArr7[_TENMETU] + 1;
            if (this.iSys[_TENMETU] >= 5) {
                this.iSys[_TENMETU] = 0;
            }
        } else if (this.iSys[4] == 3) {
            window_Show(this.g, 2);
            window_Show(this.g, 0);
            for (int c2 = 0; c2 < KEY_POUND; c2++) {
                setColor(this.g, 255, 255, 255);
                if (c2 == this.iSys[_MENUCUR]) {
                    fillRect(this.g, ((this.iSys[_MENUCUR] % 5) * _SELCURSOLY) + 60 + SAVE_HISCORE5, ((this.iSys[_MENUCUR] / 5) * AD_SOUND) + _TAMA_6, _SELCURSOLY, _SELCURSOLY);
                }
                GrPtn(this.g, 2, HELP, HELP, ((c2 % 5) * _SELCURSOLY) + 62 + SAVE_HISCORE5, ((c2 / 5) * AD_SOUND) + SAVE_HISCORE5, 8);
                if (((this.iSys[16] >> c2) & 1) != 0) {
                    GrPtn(this.g, 2, HELP, HELP, ((c2 % 5) * _SELCURSOLY) + 62 + SAVE_HISCORE5, ((c2 / 5) * AD_SOUND) + SAVE_HISCORE5, 9);
                }
            }
            if (((this.iSys[16] >> this.iSys[_MENUCUR]) & 1) != 0) {
                XCenterString(this.g, 184, this.sjist[(this.iSys[_MENUCUR] * 3) + 60]);
                PutStr(this.g, SAVE_HISCORE7, 200, this.sjist[(this.iSys[_MENUCUR] * 3) + 60 + 1]);
                PutStr(this.g, SAVE_HISCORE7, 216, this.sjist[(this.iSys[_MENUCUR] * 3) + 60 + 2]);
            } else {
                XCenterString(this.g, 184, "< ？？？？？ >");
            }
            if (GetKey(KEY_SOFT1) == 1) {
                this.iSys[7] = 0;
                this.iSys[4] = 1;
            }
            if (GetKey(1) == 1) {
                int[] iArr8 = this.iSys;
                iArr8[_MENUCUR] = iArr8[_MENUCUR] - 5;
                if (this.iSys[_MENUCUR] < 0) {
                    int[] iArr9 = this.iSys;
                    iArr9[_MENUCUR] = iArr9[_MENUCUR] + KEY_POUND;
                }
            }
            if (GetKey(2) == 1) {
                int[] iArr10 = this.iSys;
                iArr10[_MENUCUR] = iArr10[_MENUCUR] + 5;
                if (this.iSys[_MENUCUR] >= KEY_POUND) {
                    int[] iArr11 = this.iSys;
                    iArr11[_MENUCUR] = iArr11[_MENUCUR] - KEY_POUND;
                }
            }
            if (GetKey(3) == 1) {
                int[] iArr12 = this.iSys;
                iArr12[_MENUCUR] = iArr12[_MENUCUR] - 1;
                if (this.iSys[_MENUCUR] < 0) {
                    this.iSys[_MENUCUR] = 14;
                }
            }
            if (GetKey(4) == 1) {
                int[] iArr13 = this.iSys;
                iArr13[_MENUCUR] = iArr13[_MENUCUR] + 1;
                if (this.iSys[_MENUCUR] > 14) {
                    this.iSys[_MENUCUR] = 0;
                }
            }
        } else if (this.iSys[7] != 0 && this.iSys[7] == 1) {
            window_Show(this.g, 1);
            if (this.iSys[_TITLE_MODE] == 0) {
                PutStr(this.g, 232, SAVE_HISCORE11, "ﾁｬﾚﾝｼﾞﾓｰﾄﾞ", 255, 255, SAVE_HISCORE17);
            } else {
                PutStr(this.g, 232, SAVE_HISCORE11, "ﾁｬﾚﾝｼﾞﾓｰﾄﾞ");
            }
            if (this.iSys[_TITLE_MODE] == 1) {
                PutStr(this.g, 232, SAVE_HISCORE15, "ﾀｲﾑﾄﾗｲｱﾙﾓｰﾄﾞ", 255, 255, SAVE_HISCORE17);
            } else {
                PutStr(this.g, 232, SAVE_HISCORE15, "ﾀｲﾑﾄﾗｲｱﾙﾓｰﾄﾞ");
            }
            if (this.iSys[_TITLE_MODE] == 2) {
                PutStr(this.g, 232, SAVE_HISCORE19, "トロフィー", 255, 255, SAVE_HISCORE17);
            } else {
                PutStr(this.g, 232, SAVE_HISCORE19, "トロフィー");
            }
            if (this.iSys[_TITLE_MODE] == 3) {
                PutStr(this.g, 232, 152, "操作説明", 255, 255, SAVE_HISCORE17);
            } else {
                PutStr(this.g, 232, 152, "操作説明");
            }
            if (this.iSys[_TITLE_MODE] == 4) {
                PutStr(this.g, 232, 168, "ランキング", 255, 255, SAVE_HISCORE17);
            } else {
                PutStr(this.g, 232, 168, "ランキング");
            }
            if (this.iSys[_TITLE_MODE] == 5) {
                PutStr(this.g, 232, 184, "アプリ終了", 255, 255, SAVE_HISCORE17);
            } else {
                PutStr(this.g, 232, 184, "アプリ終了");
            }
            ShowCur(222, (this.iSys[_TITLE_MODE] * 16) + SAVE_HISCORE11);
            GetKey(KEY_SOFT1);
            if (GetKey(22) == 1) {
                try {
                    ((Activity) getContext()).startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://bu-nyan.bne.jp/game/jm.php")));
                } catch (Exception e5) {
                }
            }
            if (GetKey(1) == 1) {
                int[] iArr14 = this.iSys;
                iArr14[_TITLE_MODE] = iArr14[_TITLE_MODE] - 1;
                if (this.iSys[_TITLE_MODE] < 0) {
                    this.iSys[_TITLE_MODE] = 5;
                }
            }
            if (GetKey(2) == 1) {
                int[] iArr15 = this.iSys;
                iArr15[_TITLE_MODE] = iArr15[_TITLE_MODE] + 1;
                if (this.iSys[_TITLE_MODE] > 5) {
                    this.iSys[_TITLE_MODE] = 0;
                }
            }
            if (GetKey(0) != 1) {
                return;
            }
            if (this.iSys[_TITLE_MODE] == 0) {
                this.iSys[_TITLE_MODE] = 0;
                this.iSys[7] = 0;
                this.iSys[4] = 6;
                this._swidth = 10;
                this._sheight = 6;
            } else if (this.iSys[_TITLE_MODE] == 1) {
                this.iSys[_TITLE_MODE] = 1;
                this.iSys[7] = 0;
                this.iSys[4] = 6;
                this._swidth = 10;
                this._sheight = 6;
            } else if (this.iSys[_TITLE_MODE] == 2) {
                setMenu("戻る", "");
                this.iSys[4] = 3;
            } else if (this.iSys[_TITLE_MODE] == 3) {
                setMenu("戻る", "");
                this.iSys[22] = 0;
                this.iSys[4] = HELP;
            } else if (this.iSys[_TITLE_MODE] == 4) {
                setMenu("戻る", "");
                this.iSys[_MENUCUR] = 0;
                this.iSys[4] = RANKING;
            } else if (this.iSys[_TITLE_MODE] == 5) {
                set(52, this.iSys[16]);
                ((Activity) this._app).finish();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void Loop_Game() {
        int hiddenf;
        int hojuu;
        if (this.iSys[7] == 0) {
            for (int y = 0; y < 6; y++) {
                for (int x = 0; x < 10; x++) {
                    this.map[(y * 10) + x] = Rand(6) + 1;
                }
            }
            this.iSys[_TAMA_1] = 0;
            this.iSys[_TAMA_2] = 0;
            this.iSys[_TAMA_3] = 0;
            this.iSys[76] = 0;
            this.iSys[_TAMA_5] = 0;
            this.iSys[_TAMA_6] = 0;
            this.iSys[_TAMA_7] = 0;
            this.iSys[8] = 1;
            this.iSys[12] = 0;
            this.iSys[13] = 0;
            this.iSys[14] = 0;
            initializeConnection();
            do {
                hiddenf = 0;
                for (int y2 = 0; y2 < 6; y2++) {
                    for (int x2 = 0; x2 < 10; x2++) {
                        getConnection((y2 * 10) + x2);
                        if (this.idxConnection >= 4) {
                            for (int j = 0; j < this.idxConnection; j++) {
                                this.map[this.connection[j]] = 0;
                            }
                            hiddenf++;
                        }
                        this.idxConnection = 0;
                    }
                }
                if (hiddenf > 0) {
                    do {
                        hojuu = 0;
                        for (int y3 = 0; y3 < 6; y3++) {
                            for (int x3 = 0; x3 < 10; x3++) {
                                if (this.map[(y3 * 10) + x3] == 0) {
                                    this.map[(y3 * 10) + x3] = Rand(6) + 1;
                                    hojuu = 1;
                                } else {
                                    if (y3 % 2 == 0 && this.map[((y3 + 1) * 10) + x3] == 0) {
                                        this.map[((y3 + 1) * 10) + x3] = this.map[(y3 * 10) + x3];
                                        this.map[(y3 * 10) + x3] = 0;
                                        hojuu = 1;
                                        this.iSys[7] = 1;
                                    }
                                    if (y3 % 2 == 1 && x3 > 0 && this.map[(((y3 + 1) * 10) + x3) - 1] == 0) {
                                        this.map[(((y3 + 1) * 10) + x3) - 1] = this.map[(y3 * 10) + x3];
                                        this.map[(y3 * 10) + x3] = 0;
                                        hojuu = 1;
                                        this.iSys[7] = 1;
                                    }
                                }
                            }
                        }
                    } while (hojuu != 0);
                    continue;
                }
            } while (hiddenf != 0);
            this.cx = 0;
            this.cy = 0;
            this.iSys[_MENUCUR] = 0;
            this.iSys[_SELCURSOLX] = 4;
            this.iSys[_SELCURSOLY] = 4;
            this.iSys[36] = 0;
            this.iSys[_TIME] = 10000;
            if (this.iSys[_TITLE_MODE] != 0) {
                this.iSys[_TIME] = 120000;
                this.iSys[56] = (int) System.currentTimeMillis();
            }
            for (int c = 0; c < 7; c++) {
                this.viewCandyX[c] = (c * SAVE_INIT) + 8;
            }
            initializeConnection();
            this.iSys[7] = 1;
            this._timeb = System.currentTimeMillis();
            setMenu("ﾒﾆｭｰ", "");
            return;
        }
        this.iSys[_CTIME2] = (int) System.currentTimeMillis();
        if (this.iSys[4] == 9) {
            if (this.iSys[7] > 8) {
                for (int y4 = 0; y4 < 6; y4++) {
                    for (int x4 = 0; x4 < 10; x4++) {
                        if ((this.map[(y4 * 10) + x4] & SAVE_HISCORE17) != 0) {
                            this.map[(y4 * 10) + x4] = 0;
                        }
                    }
                }
                this.iSys[7] = 1;
                this.iSys[4] = 10;
            } else {
                int[] iArr = this.iSys;
                iArr[7] = iArr[7] + 1;
            }
        } else if (this.iSys[4] == 10) {
            int hojuu2 = 0;
            if (this.iSys[7] > 3) {
                for (int y5 = 0; y5 < 6; y5++) {
                    for (int x5 = 0; x5 < 10; x5++) {
                        if (this.map[(y5 * 10) + x5] == 0) {
                            if (this.iSys[8] < 10) {
                                this.map[(y5 * 10) + x5] = Rand(6) + 1;
                            } else if (this.iSys[8] < HELP || Rand(HELP) != 1) {
                                this.map[(y5 * 10) + x5] = Rand(7) + 1;
                            } else {
                                this.map[(y5 * 10) + x5] = 8;
                            }
                            hojuu2 = 1;
                        } else {
                            if (y5 % 2 == 0 && this.map[((y5 + 1) * 10) + x5] == 0) {
                                this.map[((y5 + 1) * 10) + x5] = this.map[(y5 * 10) + x5];
                                this.map[(y5 * 10) + x5] = 0;
                                hojuu2 = 1;
                                this.iSys[7] = 1;
                            }
                            if (y5 % 2 == 1 && x5 > 0 && this.map[(((y5 + 1) * 10) + x5) - 1] == 0) {
                                this.map[(((y5 + 1) * 10) + x5) - 1] = this.map[(y5 * 10) + x5];
                                this.map[(y5 * 10) + x5] = 0;
                                hojuu2 = 1;
                                this.iSys[7] = 1;
                            }
                        }
                    }
                }
                if (hojuu2 == 0) {
                    this.iSys[7] = 1;
                    this.iSys[4] = 6;
                }
            } else {
                int[] iArr2 = this.iSys;
                iArr2[7] = iArr2[7] + 1;
            }
        }
        if (this.iSys[4] != MENU) {
            if (this.iSys[4] != 12) {
                if (GetKey(4) == 1) {
                    this.cx = this.cx + 1;
                    if (this.cx > 8) {
                        this.cx = 0;
                    }
                } else if (GetKey(3) == 1) {
                    this.cx = this.cx - 1;
                    if (this.cx < 0) {
                        this.cx = 8;
                    }
                } else if (GetKey(2) == 1) {
                    this.cy = this.cy + 1;
                    if (this.cy > 4) {
                        this.cy = 0;
                    }
                } else if (GetKey(1) == 1) {
                    this.cy = this.cy - 1;
                    if (this.cy < 0) {
                        this.cy = 4;
                    }
                }
            }
            if (this.iSys[4] == 6) {
                if (GetKey(KEY_SOFT1) == 1) {
                    this.iSys[_MENUCUR] = 1;
                    this.iSys[4] = MENU;
                    setMenu("戻る", "");
                } else if (GetKey(0) == 1 && this.iSys[4] == 6) {
                    if (this.cy % 2 == 0) {
                        this.iSys[48] = (this.cy * 10) + this.cx;
                        this.iSys[_TAMA2] = ((this.cy + 1) * 10) + this.cx;
                        this.iSys[_TAMA3] = ((this.cy + 1) * 10) + this.cx + 1;
                    } else {
                        this.iSys[48] = (this.cy * 10) + this.cx + 1;
                        this.iSys[_TAMA2] = ((this.cy + 1) * 10) + this.cx;
                        this.iSys[_TAMA3] = ((this.cy + 1) * 10) + this.cx + 1;
                    }
                    this.iSys[4] = 7;
                    this.iSys[7] = 10;
                }
            }
        } else if (GetKey(KEY_SOFT1) == 1) {
            this.iSys[4] = 6;
            setMenu("ﾒﾆｭｰ", "");
        } else if (GetKey(2) == 1) {
            int[] iArr3 = this.iSys;
            iArr3[_MENUCUR] = iArr3[_MENUCUR] + 1;
            if (this.iSys[_MENUCUR] > 1) {
                this.iSys[_MENUCUR] = 0;
            }
        } else if (GetKey(1) == 1) {
            int[] iArr4 = this.iSys;
            iArr4[_MENUCUR] = iArr4[_MENUCUR] - 1;
            if (this.iSys[_MENUCUR] < 0) {
                this.iSys[_MENUCUR] = 1;
            }
        } else if (GetKey(0) == 1) {
            if (this.iSys[_MENUCUR] == 0) {
                this.iSys[7] = 0;
                this.iSys[4] = 1;
            } else {
                this.iSys[4] = 6;
            }
        }
        if (this.iSys[4] == 6) {
            initializeConnection();
            int hiddenf2 = 0;
            for (int y6 = 0; y6 < 6; y6++) {
                for (int x6 = 0; x6 < 10; x6++) {
                    if ((this.map[(y6 * 10) + x6] & SAVE_HISCORE17) == 0) {
                        getConnection((y6 * 10) + x6);
                        if (this.idxConnection >= 4) {
                            int lvup = this.iSys[_TAMA_1] + this.iSys[_TAMA_2] + this.iSys[_TAMA_3] + this.iSys[76] + this.iSys[_TAMA_5] + this.iSys[_TAMA_6] + this.iSys[_TAMA_7];
                            int[] iArr5 = this.iSys;
                            int i = (this.map[this.connection[0]] + _TAMA_1) - 1;
                            iArr5[i] = iArr5[i] + this.idxConnection;
                            if (lvup / SAVE_HISCORE10 != (this.idxConnection + lvup) / SAVE_HISCORE10) {
                                int[] iArr6 = this.iSys;
                                iArr6[8] = iArr6[8] + 1;
                                if (this.iSys[_TITLE_MODE] == 0) {
                                    this.iSys[_TIME] = 10000;
                                }
                                this.iSys[12] = AD_VIB;
                                if (this.iSys[8] >= 10) {
                                    int[] iArr7 = this.iSys;
                                    iArr7[16] = iArr7[16] | 256;
                                }
                                if (this.iSys[8] >= KEY_POUND) {
                                    int[] iArr8 = this.iSys;
                                    iArr8[16] = iArr8[16] | 512;
                                }
                                if (this.iSys[8] >= _MOVEX) {
                                    int[] iArr9 = this.iSys;
                                    iArr9[16] = iArr9[16] | 1024;
                                }
                            }
                            for (int j2 = 0; j2 < this.idxConnection; j2++) {
                                int[] iArr10 = this.map;
                                int i2 = this.connection[j2];
                                iArr10[i2] = iArr10[i2] | SAVE_HISCORE17;
                            }
                            hiddenf2++;
                            int[] iArr11 = this.iSys;
                            iArr11[36] = iArr11[36] + ((((this.idxConnection - 2) * (this.idxConnection - 2)) + (this.iSys[8] * this.iSys[13])) * SAVE_HISCORE10);
                            if (this.iSys[_TITLE_MODE] == 0) {
                                int[] iArr12 = this.iSys;
                                iArr12[_TIME] = iArr12[_TIME] + (this.idxConnection * 3);
                                if (this.iSys[_TIME] > 10000) {
                                    this.iSys[_TIME] = 10000;
                                }
                            }
                            this.iSys[14] = 1;
                            int[] iArr13 = this.iSys;
                            iArr13[13] = iArr13[13] + 1;
                            if (this.iSys[13] > 1) {
                                int[] iArr14 = this.iSys;
                                iArr14[16] = iArr14[16] | 1;
                            }
                            if (this.iSys[13] >= 10) {
                                int[] iArr15 = this.iSys;
                                iArr15[16] = iArr15[16] | 2;
                            }
                            if (this.iSys[13] >= HELP) {
                                int[] iArr16 = this.iSys;
                                iArr16[16] = iArr16[16] | 4;
                            }
                            if (this.iSys[36] >= 100000) {
                                int[] iArr17 = this.iSys;
                                iArr17[16] = iArr17[16] | 8;
                            }
                            if (this.iSys[36] >= 1000000) {
                                int[] iArr18 = this.iSys;
                                iArr18[16] = iArr18[16] | 16;
                            }
                            if (this.iSys[36] >= 2000000) {
                                int[] iArr19 = this.iSys;
                                iArr19[16] = iArr19[16] | AD_VIB;
                            }
                            if (this.iSys[_TAMA_1] >= _TAMA3 && this.iSys[_TAMA_2] >= _TAMA3 && this.iSys[_TAMA_3] >= _TAMA3 && this.iSys[76] >= _TAMA3 && this.iSys[_TAMA_5] >= _TAMA3 && this.iSys[_TAMA_6] >= _TAMA3 && this.iSys[_TAMA_7] >= _TAMA3) {
                                int[] iArr20 = this.iSys;
                                iArr20[16] = iArr20[16] | SAVE_HISCORE1;
                            }
                            if (this.iSys[_TAMA_1] >= SAVE_HISCORE10 && this.iSys[_TAMA_2] >= SAVE_HISCORE10 && this.iSys[_TAMA_3] >= SAVE_HISCORE10 && this.iSys[76] >= SAVE_HISCORE10 && this.iSys[_TAMA_5] >= SAVE_HISCORE10 && this.iSys[_TAMA_6] >= SAVE_HISCORE10 && this.iSys[_TAMA_7] >= SAVE_HISCORE10) {
                                int[] iArr21 = this.iSys;
                                iArr21[16] = iArr21[16] | SAVE_HISCORE17;
                            }
                        }
                    }
                    this.idxConnection = 0;
                }
            }
            if (hiddenf2 > 0) {
                this.iSys[4] = 9;
                this.iSys[7] = 1;
            } else {
                this.iSys[14] = 0;
                this.iSys[13] = 0;
            }
        }
        setColor(this.g, 0, 0, 0);
        fillRect(this.g, 0, 0, 240, 240);
        drawImage(this.g, Img[1], 0, 0);
        setColor(this.g, 255, 255, 0);
        if (this.iSys[_TIME] > 0) {
            if (this.iSys[_TITLE_MODE] == 0) {
                fillRect(this.g, SAVE_HISCORE10, 228, (((this.iSys[_TIME] * RANKING) / RANKING) * 200) / 10000, 5);
            } else {
                fillRect(this.g, SAVE_HISCORE10, 228, (this.iSys[_TIME] * 200) / 120000, 5);
            }
        }
        if (this.iSys[_TITLE_MODE] != 0) {
            this.iSys[_TIME] = 120000 - (this.iSys[_CTIME2] - this.iSys[56]);
        } else if (this.iSys[8] < 10) {
            int[] iArr22 = this.iSys;
            iArr22[_TIME] = iArr22[_TIME] - 10;
        } else {
            int[] iArr23 = this.iSys;
            iArr23[_TIME] = iArr23[_TIME] - this.iSys[8];
        }
        if (this.iSys[4] == 6 && this.iSys[_TIME] < 0) {
            this.iSys[_TIME] = 0;
            this.iSys[4] = 12;
        }
        XCString(this.g, 8, SAVE_INIT, "SCORE ");
        XCString(this.g, 8, 48, new StringBuilder().append(this.iSys[36]).toString());
        XCString(this.g, 240, AD_SOUND, "LV " + this.iSys[8]);
        if (this.iSys[13] > 1) {
            XCString(this.g, SAVE_HISCORE7, SAVE_HISCORE5, "COMBO " + this.iSys[13]);
        }
        GrPtn(this.g, 2, HELP, HELP, this.viewCandyX[0] + SAVE_HISCORE5, SAVE_INIT, 12);
        GrPtn(this.g, 2, HELP, HELP, this.viewCandyX[1] + SAVE_HISCORE5, SAVE_INIT, 13);
        GrPtn(this.g, 2, HELP, HELP, this.viewCandyX[2] + SAVE_HISCORE5, SAVE_INIT, 14);
        GrPtn(this.g, 2, HELP, HELP, this.viewCandyX[3] + SAVE_HISCORE5, SAVE_INIT, KEY_POUND);
        GrPtn(this.g, 2, HELP, HELP, this.viewCandyX[4] + SAVE_HISCORE5, SAVE_INIT, 16);
        GrPtn(this.g, 2, HELP, HELP, this.viewCandyX[5] + SAVE_HISCORE5, SAVE_INIT, WINDOW_NET);
        GrPtn(this.g, 2, HELP, HELP, this.viewCandyX[6] + SAVE_HISCORE5, SAVE_INIT, 18);
        if (this.iSys[8] < 10) {
            for (int c2 = 0; c2 < 6; c2++) {
                XCStringR(this.g, this.viewCandyX[c2] + 22 + SAVE_HISCORE5, 60, new StringBuilder().append(this.iSys[c2 + _TAMA_1]).toString());
            }
        } else {
            for (int c3 = 0; c3 < 7; c3++) {
                XCStringR(this.g, this.viewCandyX[c3] + 22 + SAVE_HISCORE5, 60, new StringBuilder().append(this.iSys[c3 + _TAMA_1]).toString());
            }
            if (this.viewCandyX[0] != 2) {
                int[] iArr24 = this.viewCandyX;
                iArr24[0] = iArr24[0] - 1;
            }
            if (this.viewCandyX[1] != 38) {
                int[] iArr25 = this.viewCandyX;
                iArr25[1] = iArr25[1] - 1;
            }
            if (this.viewCandyX[2] != _TAMA_2) {
                int[] iArr26 = this.viewCandyX;
                iArr26[2] = iArr26[2] - 1;
            }
            if (this.viewCandyX[3] != 110) {
                int[] iArr27 = this.viewCandyX;
                iArr27[3] = iArr27[3] - 1;
            }
            if (this.viewCandyX[4] != 146) {
                int[] iArr28 = this.viewCandyX;
                iArr28[4] = iArr28[4] - 1;
            }
            if (this.viewCandyX[5] != 182) {
                int[] iArr29 = this.viewCandyX;
                iArr29[5] = iArr29[5] - 1;
            }
            if (this.viewCandyX[6] != 218) {
                int[] iArr30 = this.viewCandyX;
                iArr30[6] = iArr30[6] - 1;
            }
        }
        if (this.iSys[12] > 0) {
            if (this.iSys[12] % 8 >= 4) {
                XCString(this.g, 168, SAVE_HISCORE5, "LEVEL UP");
            }
            int[] iArr31 = this.iSys;
            iArr31[12] = iArr31[12] - 1;
        }
        XCString(this.g, 240, SAVE_HISCORE5, "NEXTLV:" + ((this.iSys[8] * SAVE_HISCORE10) - ((((((this.iSys[_TAMA_1] + this.iSys[_TAMA_2]) + this.iSys[_TAMA_3]) + this.iSys[76]) + this.iSys[_TAMA_5]) + this.iSys[_TAMA_6]) + this.iSys[_TAMA_7])));
        if (this.iSys[4] == 7) {
            if (this.iSys[7] == 10) {
                if (this.iSys[48] % HELP < 10) {
                    GrPtn(this.g, 2, HELP, HELP, ((this.iSys[48] % 10) * HELP) + KEY_POUND + 10 + SAVE_HISCORE5, ((this.iSys[48] / 10) * HELP) + SAVE_HISCORE10 + 5, (this.map[this.iSys[48]] - 1) + 12);
                } else {
                    GrPtn(this.g, 2, HELP, HELP, ((this.iSys[48] % 10) * HELP) + KEY_POUND + SAVE_HISCORE5, ((this.iSys[48] / 10) * HELP) + SAVE_HISCORE10 + 5, (this.map[this.iSys[48]] - 1) + 12);
                }
                if (this.iSys[_TAMA2] % HELP < 10) {
                    GrPtn(this.g, 2, HELP, HELP, ((this.iSys[_TAMA2] % 10) * HELP) + KEY_POUND + 5 + 10 + SAVE_HISCORE5, (((this.iSys[_TAMA2] / 10) * HELP) + SAVE_HISCORE10) - 5, (this.map[this.iSys[_TAMA2]] - 1) + 12);
                } else {
                    GrPtn(this.g, 2, HELP, HELP, ((this.iSys[_TAMA2] % 10) * HELP) + KEY_POUND + 5 + SAVE_HISCORE5, (((this.iSys[_TAMA2] / 10) * HELP) + SAVE_HISCORE10) - 5, (this.map[this.iSys[_TAMA2]] - 1) + 12);
                }
                if (this.iSys[_TAMA3] % HELP < 10) {
                    GrPtn(this.g, 2, HELP, HELP, ((((((this.iSys[_TAMA3] % 10) * HELP) + KEY_POUND) + 5) + 10) - 10) + SAVE_HISCORE5, ((this.iSys[_TAMA3] / 10) * HELP) + SAVE_HISCORE10, (this.map[this.iSys[_TAMA3]] - 1) + 12);
                } else {
                    GrPtn(this.g, 2, HELP, HELP, (((((this.iSys[_TAMA3] % 10) * HELP) + KEY_POUND) + 5) - 10) + SAVE_HISCORE5, ((this.iSys[_TAMA3] / 10) * HELP) + SAVE_HISCORE10, (this.map[this.iSys[_TAMA3]] - 1) + 12);
                }
                this.iSys[7] = 11;
            } else if (this.iSys[7] == 11) {
                if (this.iSys[48] % HELP < 10) {
                    GrPtn(this.g, 2, HELP, HELP, ((this.iSys[48] % 10) * HELP) + KEY_POUND + 5 + 10 + SAVE_HISCORE5, ((this.iSys[48] / 10) * HELP) + SAVE_HISCORE10 + 10, (this.map[this.iSys[48]] - 1) + 12);
                } else {
                    GrPtn(this.g, 2, HELP, HELP, ((this.iSys[48] % 10) * HELP) + KEY_POUND + 5 + SAVE_HISCORE5, ((this.iSys[48] / 10) * HELP) + SAVE_HISCORE10 + 10, (this.map[this.iSys[48]] - 1) + 12);
                }
                if (this.iSys[_TAMA2] % HELP < 10) {
                    GrPtn(this.g, 2, HELP, HELP, ((this.iSys[_TAMA2] % 10) * HELP) + KEY_POUND + 5 + 10 + SAVE_HISCORE5, (((this.iSys[_TAMA2] / 10) * HELP) + SAVE_HISCORE10) - 10, (this.map[this.iSys[_TAMA2]] - 1) + 12);
                } else {
                    GrPtn(this.g, 2, HELP, HELP, ((this.iSys[_TAMA2] % 10) * HELP) + KEY_POUND + 5 + SAVE_HISCORE5, (((this.iSys[_TAMA2] / 10) * HELP) + SAVE_HISCORE10) - 10, (this.map[this.iSys[_TAMA2]] - 1) + 12);
                }
                if (this.iSys[_TAMA3] % HELP < 10) {
                    GrPtn(this.g, 2, HELP, HELP, ((((((this.iSys[_TAMA3] % 10) * HELP) + KEY_POUND) + 5) + 10) - KEY_POUND) + SAVE_HISCORE5, ((this.iSys[_TAMA3] / 10) * HELP) + SAVE_HISCORE10, (this.map[this.iSys[_TAMA3]] - 1) + 12);
                } else {
                    GrPtn(this.g, 2, HELP, HELP, (((((this.iSys[_TAMA3] % 10) * HELP) + KEY_POUND) + 5) - KEY_POUND) + SAVE_HISCORE5, ((this.iSys[_TAMA3] / 10) * HELP) + SAVE_HISCORE10, (this.map[this.iSys[_TAMA3]] - 1) + 12);
                }
                this.iSys[7] = 12;
            } else if (this.iSys[7] == 12) {
                if (this.iSys[48] % HELP < 10) {
                    GrPtn(this.g, 2, HELP, HELP, ((this.iSys[48] % 10) * HELP) + KEY_POUND + 10 + 10 + SAVE_HISCORE5, ((this.iSys[48] / 10) * HELP) + SAVE_HISCORE10 + KEY_POUND, (this.map[this.iSys[48]] - 1) + 12);
                } else {
                    GrPtn(this.g, 2, HELP, HELP, ((this.iSys[48] % 10) * HELP) + KEY_POUND + 10 + SAVE_HISCORE5, ((this.iSys[48] / 10) * HELP) + SAVE_HISCORE10 + KEY_POUND, (this.map[this.iSys[48]] - 1) + 12);
                }
                if (this.iSys[_TAMA2] % HELP < 10) {
                    GrPtn(this.g, 2, HELP, HELP, ((this.iSys[_TAMA2] % 10) * HELP) + KEY_POUND + 5 + 10 + SAVE_HISCORE5, (((this.iSys[_TAMA2] / 10) * HELP) + SAVE_HISCORE10) - KEY_POUND, (this.map[this.iSys[_TAMA2]] - 1) + 12);
                } else {
                    GrPtn(this.g, 2, HELP, HELP, ((this.iSys[_TAMA2] % 10) * HELP) + KEY_POUND + 5 + SAVE_HISCORE5, (((this.iSys[_TAMA2] / 10) * HELP) + SAVE_HISCORE10) - KEY_POUND, (this.map[this.iSys[_TAMA2]] - 1) + 12);
                }
                if (this.iSys[_TAMA3] % HELP < 10) {
                    GrPtn(this.g, 2, HELP, HELP, ((((((this.iSys[_TAMA3] % 10) * HELP) + KEY_POUND) + 5) + 10) - HELP) + SAVE_HISCORE5, ((this.iSys[_TAMA3] / 10) * HELP) + SAVE_HISCORE10, (this.map[this.iSys[_TAMA3]] - 1) + 12);
                } else {
                    GrPtn(this.g, 2, HELP, HELP, (((((this.iSys[_TAMA3] % 10) * HELP) + KEY_POUND) + 5) - HELP) + SAVE_HISCORE5, ((this.iSys[_TAMA3] / 10) * HELP) + SAVE_HISCORE10, (this.map[this.iSys[_TAMA3]] - 1) + 12);
                }
                this.iSys[7] = 13;
            } else if (this.iSys[7] == 13) {
                int t1 = this.map[this.iSys[48]];
                int t2 = this.map[this.iSys[_TAMA2]];
                int t3 = this.map[this.iSys[_TAMA3]];
                this.map[((this.iSys[48] / 10) * 10) + (this.iSys[48] % 10)] = t2;
                this.map[((this.iSys[_TAMA2] / 10) * 10) + (this.iSys[_TAMA2] % 10)] = t3;
                this.map[((this.iSys[_TAMA3] / 10) * 10) + (this.iSys[_TAMA3] % 10)] = t1;
                this.iSys[4] = 6;
                this.iSys[7] = 1;
            }
        }
        int concnt = 0;
        for (int y7 = 0; y7 < 6; y7++) {
            for (int x7 = 0; x7 < 10; x7++) {
                if (this.map[(y7 * 10) + x7] > 0 && (this.iSys[4] == 1 || this.iSys[4] == MENU || this.iSys[4] == 6 || this.iSys[4] == 9 || this.iSys[4] == 10 || this.iSys[4] == 12 || !(this.iSys[4] != 7 || (y7 * 10) + x7 == this.iSys[48] || (y7 * 10) + x7 == this.iSys[_TAMA2] || (y7 * 10) + x7 == this.iSys[_TAMA3]))) {
                    if (this.connection[concnt] != (y7 * 10) + x7 || (this.connection[concnt] == (y7 * 10) + x7 && (this.idxConnection <= 3 || (this.idxConnection > 3 && this.iSys[_TENMETU] < 3)))) {
                        if (this.connection[concnt] == (y7 * 10) + x7) {
                            concnt++;
                        }
                        if ((this.map[(y7 * 10) + x7] & SAVE_HISCORE17) == 0 || ((this.map[(y7 * 10) + x7] & SAVE_HISCORE17) != 0 && this.iSys[7] % 2 == 1)) {
                            if (y7 % 2 == 1) {
                                GrPtn(this.g, 2, HELP, HELP, (x7 * HELP) + KEY_POUND + SAVE_HISCORE5, (y7 * HELP) + SAVE_HISCORE10, ((this.map[(y7 * 10) + x7] - 1) + 12) & 127);
                            } else {
                                GrPtn(this.g, 2, HELP, HELP, (x7 * HELP) + KEY_POUND + 10 + SAVE_HISCORE5, (y7 * HELP) + SAVE_HISCORE10, ((this.map[(y7 * 10) + x7] - 1) + 12) & 127);
                            }
                        }
                    } else {
                        concnt++;
                    }
                }
            }
        }
        if (this.cy % 2 == 1) {
            GrPtn(this.g, 2, SAVE_INIT, SAVE_INIT, (this.cx * HELP) + KEY_POUND + 10 + SAVE_HISCORE5, (this.cy * HELP) + SAVE_HISCORE10, 5);
        } else {
            GrPtn(this.g, 2, SAVE_INIT, SAVE_INIT, (this.cx * HELP) + KEY_POUND + SAVE_HISCORE5, (this.cy * HELP) + SAVE_HISCORE10, 5);
        }
        int[] iArr32 = this.iSys;
        iArr32[_TENMETU] = iArr32[_TENMETU] + 1;
        if (this.iSys[_TENMETU] >= 5) {
            this.iSys[_TENMETU] = 0;
        }
        if (this.iSys[4] == 12) {
            window_Show(this.g, 3);
            if (GetKey(0) == 1) {
                if (this.iSys[_TITLE_MODE] == 0) {
                    Hiscore(this.iSys[36]);
                } else {
                    Hiscore2(this.iSys[36]);
                }
                set(52, this.iSys[16]);
                if (this.iSys[84] == 0) {
                    this.iSys[4] = 1;
                    this.iSys[7] = 0;
                } else {
                    this.iSys[7] = 0;
                    this.iSys[4] = 1;
                }
            }
        }
        if (this.iSys[4] == MENU) {
            window_Show(this.g, 9);
        }
    }

    /* access modifiers changed from: package-private */
    public void initializeConnection() {
        for (int j = 0; j < this.idxConnection; j++) {
            this.connection[j] = 999;
        }
        this.idxConnection = 0;
    }

    /* access modifiers changed from: package-private */
    public boolean isConnected(int j) {
        for (int k = 0; k < this.idxConnection; k++) {
            if (this.connection[k] == j) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public void sortConnection() {
        for (int j = 0; j < this.idxConnection - 1; j++) {
            for (int k = j + 1; k < this.idxConnection; k++) {
                if (this.connection[j] > this.connection[k]) {
                    int tmp = this.connection[j];
                    this.connection[j] = this.connection[k];
                    this.connection[k] = tmp;
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void getConnection(int j) {
        if (this.map[j] != 0) {
            int[] iArr = this.connection;
            int i = this.idxConnection;
            this.idxConnection = i + 1;
            iArr[i] = j;
            int x = j % 10;
            int y = (j - x) / 10;
            if (x != this._swidth - 1 && this.map[j] == this.map[j + 1] && !isConnected(j + 1)) {
                getConnection(j + 1);
            }
            if (x != 0 && this.map[j] == this.map[j - 1] && !isConnected(j - 1)) {
                getConnection(j - 1);
            }
            if (y != this._sheight - 1 && this.map[j] == this.map[j + 10] && !isConnected(j + 10)) {
                getConnection(j + 10);
            }
            if (y != 0 && this.map[j] == this.map[j - 10] && !isConnected(j - 10)) {
                getConnection(j - 10);
            }
            if (y % 2 == 0) {
                if (y != this._sheight - 1 && x < 9 && this.map[j] == this.map[j + 10 + 1] && !isConnected(j + 10 + 1)) {
                    getConnection(j + 10 + 1);
                }
            } else if (y != this._sheight - 1 && x > 0 && this.map[j] == this.map[(j + 10) - 1] && !isConnected((j + 10) - 1)) {
                getConnection((j + 10) - 1);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void window_Show(Canvas g2, int type) {
        int px = 0;
        int py = 0;
        int WinX = 0;
        int WinY = 0;
        switch (type) {
            case 0:
                px = 0;
                py = 176;
                WinX = Window_Msg[0];
                WinY = Window_Msg[1];
                break;
            case 1:
                px = SAVE_HISCORE19;
                py = SAVE_HISCORE9;
                WinX = Window_TMenu[0];
                WinY = Window_TMenu[1];
                break;
            case 2:
                px = SAVE_HISCORE15 - ((Window_Torphy[0] * 8) / 2);
                py = SAVE_HISCORE15 - ((Window_Torphy[1] * 8) / 2);
                WinX = Window_Torphy[0];
                WinY = Window_Torphy[1];
                break;
            case 3:
                px = SAVE_HISCORE15 - ((Window_Score[0] * 8) / 2);
                py = SAVE_HISCORE15 - ((Window_Score[1] * 8) / 2);
                WinX = Window_Score[0];
                WinY = Window_Score[1];
                break;
            case 4:
                px = 48;
                py = SAVE_INIT;
                WinX = Window_Ranking[0];
                WinY = Window_Ranking[1];
                break;
            case 5:
                setColor(g2, color[this.iSys[_BCOLOR] * 3], color[(this.iSys[_BCOLOR] * 3) + 1], color[(this.iSys[_BCOLOR] * 3) + 2]);
                fillRect(g2, _TAMA3, 114, (Window_Load[0] * 8) - 4, (Window_Load[1] * 8) - 4);
                for (int y = 0; y < Window_Load[1]; y++) {
                    for (int x = 0; x < Window_Load[0]; x++) {
                        if (Window_Load[(Window_Load[0] * y) + 2 + x] != 8) {
                            GrPtn(g2, 22, 8, 8, (x * 8) + 48, (y * 8) + SAVE_HISCORE13, Window_Load[(Window_Load[0] * y) + 2 + x]);
                        }
                    }
                }
                XCenterString(g2, 122, "続きから始めますか？");
                XCenterString(g2, 138, " はい  ");
                XCenterString(g2, 154, " いいえ");
                ShowCur(SAVE_HISCORE7, (this.iSys[_MENUCUR] * 16) + SAVE_HISCORE17 + 12);
                break;
            case 8:
                px = 0;
                py = SAVE_INIT;
                WinX = Window_Help[0];
                WinY = Window_Help[1];
                break;
            case 9:
                px = SAVE_HISCORE1;
                py = SAVE_HISCORE9;
                WinX = Window_Menu[0];
                WinY = Window_Menu[1];
                break;
            case 16:
                setColor(g2, color[this.iSys[_BCOLOR] * 3], color[(this.iSys[_BCOLOR] * 3) + 1], color[(this.iSys[_BCOLOR] * 3) + 2]);
                fillRect(g2, ((this.iSys[2] / 2) - SAVE_HISCORE5) + 2, 66, (Window_Config[0] * 8) - 4, (Window_Config[1] * 8) - 4);
                for (int y2 = 0; y2 < Window_Config[1]; y2++) {
                    for (int x2 = 0; x2 < Window_Config[0]; x2++) {
                        if (Window_Config[(Window_Config[0] * y2) + 2 + x2] != 8) {
                            GrPtn(g2, 22, 8, 8, ((this.iSys[2] / 2) - SAVE_HISCORE5) + (x2 * 8), (y2 * 8) + SAVE_HISCORE1, Window_Config[(Window_Config[0] * y2) + 2 + x2]);
                        }
                    }
                }
                break;
            case WINDOW_NET /*17*/:
                px = SAVE_HISCORE15 - (Window_Net[0] * 4);
                py = SAVE_HISCORE15 - (Window_Net[1] * 4);
                WinX = Window_Net[0];
                WinY = Window_Net[1];
                break;
        }
        for (int y22 = 0; y22 < 1; y22++) {
            for (int x22 = 0; x22 < 1; x22++) {
                if (this.iSys[_BCOLOR] >= 8) {
                    setColor(g2, color[this.iSys[_BCOLOR] * 3], color[(this.iSys[_BCOLOR] * 3) + 1], color[(this.iSys[_BCOLOR] * 3) + 2], SAVE_HISCORE17);
                    fillRect(g2, px + 2 + SAVE_HISCORE5, (y22 * WinY * 8) + py + 2, (WinX * 8) - 4, (WinY * 8) - 4);
                    fillRect(g2, px + 2 + SAVE_HISCORE5, (y22 * WinY * 8) + py + 2, (WinX * 8) - 4, (WinY * 8) - 4);
                } else {
                    for (int c = 0; c < (WinY * 8) - 4; c += 2) {
                        setColor(g2, (((c * RANKING) / ((WinY * 8) - 6)) * color[this.iSys[_BCOLOR] * 3]) / RANKING, (((c * RANKING) / ((WinY * 8) - 6)) * color[(this.iSys[_BCOLOR] * 3) + 1]) / RANKING, (((c * RANKING) / ((WinY * 8) - 6)) * color[(this.iSys[_BCOLOR] * 3) + 2]) / RANKING);
                        fillRect(g2, px + 2 + SAVE_HISCORE5, (y22 * WinY * 8) + py + 2 + c, (WinX * 8) - 4, 2);
                    }
                }
            }
        }
        switch (type) {
            case 0:
                PutStr(g2, px + 6, py + 8 + 0, this.MsgBoxStr[0]);
                PutStr(g2, px + 6, py + 8 + 16, this.MsgBoxStr[1]);
                PutStr(g2, px + 6, py + 8 + AD_VIB, this.MsgBoxStr[2]);
                PutStr(g2, px + 6, py + 8 + 48, this.MsgBoxStr[3]);
                break;
            case 3:
                XCenterString(g2, SAVE_HISCORE7, "TIME OVER");
                XCenterString(g2, SAVE_HISCORE15, "今回の得点");
                XCenterString(g2, SAVE_HISCORE19, this.iSys[36] + "点");
                break;
            case 9:
                XCenterString(g2, 106, "ﾀｲﾄﾙ画面へ");
                XCenterString(g2, 122, "戻りますか？");
                XCenterString(g2, 138, "はい");
                XCenterString(g2, 154, "いいえ");
                if (this.iSys[_MENUCUR] == 0) {
                    XCenterString(g2, 138, "はい", 255, 255, SAVE_HISCORE10);
                }
                if (this.iSys[_MENUCUR] == 1) {
                    XCenterString(g2, 154, "いいえ", 255, 255, SAVE_HISCORE10);
                }
                ShowCur(168, (this.iSys[_MENUCUR] * 16) + SAVE_HISCORE17 + 12);
                break;
            case WINDOW_NET /*17*/:
                XCenterString(g2, py + 6, "通信中");
                break;
        }
        for (int y23 = 0; y23 < 1; y23++) {
            for (int x23 = 0; x23 < 1; x23++) {
                if (WinX > 0 && WinY > 0) {
                    GrPtn(g2, 4, 8, 8, px + SAVE_HISCORE5, py + (y23 * WinY * 8), 0);
                    GrPtn(g2, 4, 8, 8, ((WinX - 1) * 8) + px + SAVE_HISCORE5, py + (y23 * WinY * 8), 1);
                    Canvas canvas = g2;
                    GrPtn(canvas, 4, 8, 8, px + SAVE_HISCORE5, ((WinY - 1) * 8) + py + (y23 * WinY * 8), 2);
                    GrPtn(g2, 4, 8, 8, ((WinX - 1) * 8) + px + SAVE_HISCORE5, ((WinY - 1) * 8) + py + (y23 * WinY * 8), 3);
                    for (int x3 = 1; x3 < WinX - 1; x3++) {
                        GrPtn(g2, 4, 8, 8, (x3 * 8) + px + SAVE_HISCORE5, py + (y23 * WinY * 8), 6);
                        GrPtn(g2, 4, 8, 8, (x3 * 8) + px + SAVE_HISCORE5, ((WinY - 1) * 8) + py + (y23 * WinY * 8), 7);
                    }
                    for (int y3 = 1; y3 < WinY - 1; y3++) {
                        Canvas canvas2 = g2;
                        GrPtn(canvas2, 4, 8, 8, px + SAVE_HISCORE5, (y3 * 8) + py + (y23 * WinY * 8), 4);
                        GrPtn(g2, 4, 8, 8, ((WinX - 1) * 8) + px + SAVE_HISCORE5, (y3 * 8) + py + (y23 * WinY * 8), 5);
                    }
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void ShowCur(int x, int y) {
        if (this.iSys[_TENMETU] < 3) {
            GrPtn(this.g, 4, 8, 8, x, y, 8);
        }
        int[] iArr = this.iSys;
        iArr[_TENMETU] = iArr[_TENMETU] + 1;
        if (this.iSys[_TENMETU] >= 5) {
            this.iSys[_TENMETU] = 0;
        }
    }

    /* access modifiers changed from: package-private */
    public void setColor(Canvas c, int r2, int g2, int b) {
        this.sr = r2;
        this.sg = g2;
        this.sb = b;
        this.paint.setColor(Color.rgb(r2, g2, b));
    }

    /* access modifiers changed from: package-private */
    public void setColor(Canvas c, int r2, int g2, int b, int a) {
        this.sr = r2;
        this.sg = g2;
        this.sb = b;
        this.paint.setColor(Color.rgb(r2, g2, b));
    }

    /* access modifiers changed from: package-private */
    public void drawLine(Canvas c, int x, int y, int x2, int y2) {
        this.paint.setStyle(Paint.Style.STROKE);
        c.drawLine((float) x, (float) y, (float) x2, (float) y2, this.paint);
    }

    /* access modifiers changed from: package-private */
    public void drawRect(Canvas c, int x, int y, int x2, int y2) {
        this.paint.setAntiAlias(false);
        this.paint.setStyle(Paint.Style.STROKE);
        c.drawRect((float) x, (float) y, (float) (x + x2), (float) (y + y2), this.paint);
        this.paint.setAntiAlias(true);
    }

    /* access modifiers changed from: package-private */
    public void fillRect(Canvas c, int x, int y, int x2, int y2) {
        this.paint.setAntiAlias(false);
        this.paint.setStyle(Paint.Style.FILL);
        c.drawRect((float) x, (float) y, (float) (x + x2), (float) (y + y2), this.paint);
        this.paint.setAntiAlias(true);
    }

    /* access modifiers changed from: package-private */
    public void PutStr(Canvas g2, int x, int y, String s) {
        int y2 = y + 8;
        drawText(g2, x + 1, y2, s, 0, 0, 0);
        drawText(g2, x - 1, y2, s, 0, 0, 0);
        drawText(g2, x, y2 + 1, s, 0, 0, 0);
        drawText(g2, x, y2 - 1, s, 0, 0, 0);
        drawText(g2, x + 1, y2 - 1, s, 0, 0, 0);
        drawText(g2, x + 1, y2 + 1, s, 0, 0, 0);
        drawText(g2, x - 1, y2 - 1, s, 0, 0, 0);
        drawText(g2, x - 1, y2 + 1, s, 0, 0, 0);
        drawText(g2, x, y2, s, 255, 255, 255);
    }

    /* access modifiers changed from: package-private */
    public void drawText(Canvas g2, int x, int y, String s, int _r, int _g, int _b) {
        this.GameStr[this.GameStrCnt] = s;
        this.GameStrX[this.GameStrCnt] = x;
        this.GameStrY[this.GameStrCnt] = y;
        this.GameStrC[this.GameStrCnt] = (_r << 16) | (_g << 8) | _b;
        this.GameStrCnt++;
    }

    /* access modifiers changed from: package-private */
    public void PutStr(Canvas g2, int x, int y, String s, int _r, int _g, int _b) {
        int y2 = y + 8;
        drawText(g2, x + 1, y2, s, 0, 0, 0);
        drawText(g2, x + 1, y2 + 1, s, 0, 0, 0);
        drawText(g2, x, y2 + 1, s, 0, 0, 0);
        drawText(g2, x - 1, y2 + 1, s, 0, 0, 0);
        drawText(g2, x - 1, y2, s, 0, 0, 0);
        drawText(g2, x, y2 + 1, s, 0, 0, 0);
        drawText(g2, x - 1, y2 - 1, s, 0, 0, 0);
        drawText(g2, x, y2 - 1, s, 0, 0, 0);
        drawText(g2, x, y2, s, _r, _g, _b);
    }

    /* access modifiers changed from: package-private */
    public void readGrp(int n) {
        Img[n] = BitmapFactory.decodeResource(getResources(), R.drawable.g0 + n);
    }

    /* access modifiers changed from: package-private */
    public void GrPtn(Canvas g2, int n, int sx2, int sy2, int x, int y, int p) {
        int i = p * sx2;
        int _y = 0;
        try {
            int sizex = Img[n].getWidth();
            int _x = p * sx2;
            if (p >= sizex / sx2) {
                _y = (p / (sizex / sx2)) * sy2;
                _x = (p % (sizex / sx2)) * sx2;
            }
            g2.drawBitmap(Img[n], new Rect(_x, _y, _x + sx2, _y + sy2), new Rect(x, y, x + sx2, y + sy2), (Paint) null);
        } catch (Exception e) {
            System.out.println("GrPtnErr:" + n + " " + e.toString());
        }
    }

    /* access modifiers changed from: package-private */
    public void drawImage(Canvas g2, Bitmap I, int x, int y) {
        g2.drawBitmap(I, new Rect(0, 0, I.getWidth(), I.getHeight()), new Rect(x, y, I.getWidth() + x, I.getHeight() + y), (Paint) null);
    }

    /* access modifiers changed from: package-private */
    public void drawImage(Canvas g2, Bitmap I, int x, int y, int _x, int _y, int sx2, int sy2) {
        g2.drawBitmap(I, new Rect(_x, _y, _x + sx2, _y + sy2), new Rect(x, y, x + sx2, y + sy2), (Paint) null);
    }

    /* access modifiers changed from: package-private */
    public void drawImageScale(Canvas g2, Bitmap I, int x, int y, int sx2, int sy2, int _x, int _y, int sx22, int sy22) {
        Rect src = new Rect(_x, _y, _x + sx22, _y + sy22);
        Rect dst = new Rect(x, y, x + sx2, y + sy2);
        Paint p = new Paint();
        p.setFilterBitmap(true);
        g2.drawBitmap(I, src, dst, p);
    }

    /* access modifiers changed from: package-private */
    public void XCenterString(Canvas g2, int y, String s) {
        PutStr(g2, (this.offs.getWidth() / 2) - (((int) this.paint.measureText(s)) / 4), y, s);
    }

    /* access modifiers changed from: package-private */
    public void XCenterString(Canvas g2, int y, String s, int _r, int _g, int _b) {
        PutStr(g2, (this.offs.getWidth() / 2) - (((int) this.paint.measureText(s)) / 4), y, s, _r, _g, _b);
    }

    /* access modifiers changed from: package-private */
    public void XCString(Canvas g2, int x, int y, String s) {
        int px = x;
        int mc = 0;
        for (int c = 0; c < s.length(); c++) {
            if ("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+-*/:?=<>".indexOf(s.charAt(mc)) != -1) {
                GrPtn(g2, 4, 8, 8, px, y, "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+-*/:?=<>".indexOf(s.charAt(mc)) + 19);
            }
            px += 8;
            mc++;
        }
    }

    /* access modifiers changed from: package-private */
    public void XCStringR(Canvas g2, int x, int y, String s) {
        int px = x - (s.length() * 8);
        int mc = 0;
        for (int c = 0; c < s.length(); c++) {
            if ("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-./".indexOf(s.charAt(mc)) != -1) {
                GrPtn(g2, 4, 8, 8, px, y, "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-./".indexOf(s.charAt(mc)) + 19);
            }
            px += 8;
            mc++;
        }
    }

    /* access modifiers changed from: package-private */
    public void XCStringBig(Canvas g2, int x, int y, String s) {
        int px = x;
        int mc = 0;
        for (int c = 0; c < s.length(); c++) {
            if ("0123456789:".indexOf(s.charAt(mc)) != -1) {
                GrPtn(g2, 0, 8, 16, px, y, "0123456789:".indexOf(s.charAt(mc)) + SAVE_HISCORE9 + HELP);
            }
            px += 8;
            mc++;
        }
    }

    /* access modifiers changed from: package-private */
    public int GetKey(int Key) {
        if (this.KeyFlag[Key] != 1) {
            return 0;
        }
        if (this.KeyFlagOld[Key] == 1) {
            return 2;
        }
        return 1;
    }

    /* access modifiers changed from: package-private */
    public int Rand(int n) {
        return this.r.nextInt(n);
    }

    /* access modifiers changed from: package-private */
    public void Hiscore(int Score) {
        if (this.hiscore[9] < Score || this.hiscore[9] == 0) {
            this.hiscore[9] = Score;
        }
        if (this.hiscore[8] < Score || this.hiscore[8] == 0) {
            this.hiscore[9] = this.hiscore[8];
            this.hiscore[8] = Score;
        }
        if (this.hiscore[7] < Score || this.hiscore[7] == 0) {
            this.hiscore[8] = this.hiscore[7];
            this.hiscore[7] = Score;
        }
        if (this.hiscore[6] < Score || this.hiscore[6] == 0) {
            this.hiscore[7] = this.hiscore[6];
            this.hiscore[6] = Score;
        }
        if (this.hiscore[5] < Score || this.hiscore[5] == 0) {
            this.hiscore[6] = this.hiscore[5];
            this.hiscore[5] = Score;
        }
        if (this.hiscore[4] < Score || this.hiscore[4] == 0) {
            this.hiscore[5] = this.hiscore[4];
            this.hiscore[4] = Score;
        }
        if (this.hiscore[3] < Score || this.hiscore[3] == 0) {
            this.hiscore[4] = this.hiscore[3];
            this.hiscore[3] = Score;
        }
        if (this.hiscore[2] < Score || this.hiscore[2] == 0) {
            this.hiscore[3] = this.hiscore[2];
            this.hiscore[2] = Score;
        }
        if (this.hiscore[1] < Score || this.hiscore[1] == 0) {
            this.hiscore[2] = this.hiscore[1];
            this.hiscore[1] = Score;
        }
        if (this.hiscore[0] < Score || this.hiscore[0] == 0) {
            this.hiscore[1] = this.hiscore[0];
            this.hiscore[0] = Score;
        }
        set(SAVE_HISCORE1, this.hiscore[0]);
        set(SAVE_HISCORE2, this.hiscore[1]);
        set(SAVE_HISCORE3, this.hiscore[2]);
        set(76, this.hiscore[3]);
        set(SAVE_HISCORE5, this.hiscore[4]);
        set(84, this.hiscore[5]);
        set(SAVE_HISCORE7, this.hiscore[6]);
        set(SAVE_HISCORE8, this.hiscore[7]);
        set(SAVE_HISCORE9, this.hiscore[8]);
        set(SAVE_HISCORE10, this.hiscore[9]);
    }

    /* access modifiers changed from: package-private */
    public void Hiscore2(int Score) {
        if (this.hiscore[19] < Score || this.hiscore[19] == 0) {
            this.hiscore[19] = Score;
        }
        if (this.hiscore[18] < Score || this.hiscore[18] == 0) {
            this.hiscore[19] = this.hiscore[18];
            this.hiscore[18] = Score;
        }
        if (this.hiscore[WINDOW_NET] < Score || this.hiscore[WINDOW_NET] == 0) {
            this.hiscore[18] = this.hiscore[WINDOW_NET];
            this.hiscore[WINDOW_NET] = Score;
        }
        if (this.hiscore[16] < Score || this.hiscore[16] == 0) {
            this.hiscore[WINDOW_NET] = this.hiscore[16];
            this.hiscore[16] = Score;
        }
        if (this.hiscore[KEY_POUND] < Score || this.hiscore[KEY_POUND] == 0) {
            this.hiscore[16] = this.hiscore[KEY_POUND];
            this.hiscore[KEY_POUND] = Score;
        }
        if (this.hiscore[14] < Score || this.hiscore[14] == 0) {
            this.hiscore[KEY_POUND] = this.hiscore[14];
            this.hiscore[14] = Score;
        }
        if (this.hiscore[13] < Score || this.hiscore[13] == 0) {
            this.hiscore[14] = this.hiscore[13];
            this.hiscore[13] = Score;
        }
        if (this.hiscore[12] < Score || this.hiscore[12] == 0) {
            this.hiscore[13] = this.hiscore[12];
            this.hiscore[12] = Score;
        }
        if (this.hiscore[11] < Score || this.hiscore[11] == 0) {
            this.hiscore[12] = this.hiscore[11];
            this.hiscore[11] = Score;
        }
        if (this.hiscore[10] < Score || this.hiscore[10] == 0) {
            this.hiscore[11] = this.hiscore[10];
            this.hiscore[10] = Score;
        }
        set(SAVE_HISCORE11, this.hiscore[10]);
        set(SAVE_HISCORE12, this.hiscore[11]);
        set(SAVE_HISCORE13, this.hiscore[12]);
        set(SAVE_HISCORE14, this.hiscore[13]);
        set(SAVE_HISCORE15, this.hiscore[14]);
        set(SAVE_HISCORE16, this.hiscore[KEY_POUND]);
        set(SAVE_HISCORE17, this.hiscore[16]);
        set(SAVE_HISCORE18, this.hiscore[WINDOW_NET]);
        set(SAVE_HISCORE19, this.hiscore[18]);
        set(SAVE_HISCORE20, this.hiscore[19]);
    }

    /* access modifiers changed from: package-private */
    public void set(int pos, int data) {
        this.scratchpad[pos] = (byte) ((data >> _SELCURSOLY) & 255);
        this.scratchpad[pos + 1] = (byte) ((data >> 16) & 255);
        this.scratchpad[pos + 2] = (byte) ((data >> 8) & 255);
        this.scratchpad[pos + 3] = (byte) (data & 255);
    }

    /* access modifiers changed from: package-private */
    public int get(int pos) {
        return ((this.scratchpad[pos] & 255) << 24) | ((this.scratchpad[pos + 1] & 255) << 16) | ((this.scratchpad[pos + 2] & 255) << 8) | (this.scratchpad[pos + 3] & 255);
    }

    /* access modifiers changed from: package-private */
    public void save() {
        IOException e;
        try {
            FileOutputStream file = getContext().openFileOutput("save_kurukuru.bin", 0);
            BufferedOutputStream out = new BufferedOutputStream(file);
            try {
                out.write(this.scratchpad);
                out.flush();
                file.close();
            } catch (IOException e2) {
                e = e2;
                e.printStackTrace();
            }
        } catch (IOException e3) {
            e = e3;
            e.printStackTrace();
        }
    }

    private void setMenu(String s1, String s2) {
        this.sC1 = s1;
        this.sC2 = s2;
    }

    /* access modifiers changed from: package-private */
    public int readText(String[] sd, String fname) {
        InputStream in = getResources().openRawResource(R.raw.t);
        new ByteArrayOutputStream();
        byte[] ba = new byte[320];
        int y = 0;
        try {
            byte[] ba2 = null;
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            try {
                byte[] ba3 = new byte[320];
                while (true) {
                    int size = in.read(ba3);
                    if (size <= 0) {
                        break;
                    }
                    baos.write(ba3, 0, size);
                }
                baos.flush();
                this.sjisbuf = baos.toByteArray();
                in.close();
                System.gc();
                this.sjis = new String(this.sjisbuf, "SJIS");
                int length = this.sjis.length();
                int n = 0;
                int leng = this.sjis.length();
                while (true) {
                    int x = this.sjis.indexOf(10, n);
                    if (x < 0) {
                        sd[y] = this.sjis.substring(n, leng);
                        break;
                    }
                    if (this.sjis.charAt(x - 1) == 13) {
                        int x2 = x - 1;
                        sd[y] = this.sjis.substring(n, x2);
                        x = x2 + 1;
                        y++;
                    } else {
                        sd[y] = this.sjis.substring(n, x);
                    }
                    n = x + 1;
                    if (n >= leng) {
                        ByteArrayOutputStream byteArrayOutputStream = baos;
                        break;
                    }
                }
            } catch (Exception e) {
                ByteArrayOutputStream byteArrayOutputStream2 = baos;
            }
        } catch (Exception e2) {
        }
        return y;
    }

    static String[] split(String str, String key) {
        if (str == null || str.length() < 1) {
            return null;
        }
        int count2 = 0;
        int pos = 0;
        do {
            count2++;
            pos = str.indexOf(key, pos) + 1;
        } while (pos > 0);
        String[] result = new String[count2];
        if (count2 < 2) {
            result[0] = str;
            return result;
        }
        int start = 0;
        int index = 0;
        while (index < count2) {
            int pos2 = str.indexOf(key, start);
            if (pos2 < start) {
                result[index] = str.substring(start);
                index++;
            } else {
                result[index] = str.substring(start, pos2);
                start = pos2 + key.length();
                index++;
            }
        }
        return result;
    }
}
