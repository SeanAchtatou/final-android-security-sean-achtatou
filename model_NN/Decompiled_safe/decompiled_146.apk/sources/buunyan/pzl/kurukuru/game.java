package buunyan.pzl.kurukuru;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.SurfaceView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import jp.co.nobot.libAdMaker.AdMakerListener;
import jp.co.nobot.libAdMaker.libAdMaker;

public class game extends Activity implements AdMakerListener {
    private libAdMaker AdMaker;
    private Button button;
    private SurfaceView mSvMain;
    private Screen surface;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(1024);
        requestWindowFeature(1);
        setContentView((int) R.layout.main);
        this.mSvMain = (SurfaceView) findViewById(R.id.SurfaceViewMain);
        this.surface = new Screen(this, this.mSvMain);
        FrameLayout layout = new FrameLayout(this);
        new FrameLayout.LayoutParams(-2, -2);
        TableLayout tlayout = new TableLayout(this);
        TableRow row = new TableRow(this);
        row.setLayoutParams(new TableLayout.LayoutParams(-2, -2));
        row.setGravity(17);
        new TableLayout.LayoutParams(-1, -1);
        this.AdMaker = new libAdMaker(this);
        this.AdMaker.setAdMakerListener(this);
        this.AdMaker.siteId = "1745";
        this.AdMaker.zoneId = "4418";
        this.AdMaker.setUrl("http://images.ad-maker.info/apps/n0d8veloh81k.html");
        row.addView(this.AdMaker, 480, 75);
        tlayout.addView(row);
        layout.addView(this.surface);
        layout.addView(tlayout);
        setContentView(layout);
        this.AdMaker.start();
    }

    private static void showDialog(final Activity activity, String title, String text) {
        AlertDialog.Builder ad = new AlertDialog.Builder(activity);
        ad.setTitle(title);
        ad.setMessage(text);
        ad.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                activity.setResult(-1);
            }
        });
        ad.create();
        ad.show();
    }

    public void onDestroy() {
        this.AdMaker.destroy();
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.AdMaker.stop();
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
        this.AdMaker.start();
    }

    public void onFailedToReceiveAdMaker(String error) {
    }

    public void onReceiveAdMaker() {
    }
}
