package com.mobclix.android.sdk;

public interface MobclixFullScreenAdViewListener {
    public static final int ADSIZE_DISABLED = -999999;
    public static final int UNAVAILABLE = -503;
    public static final int UNKNOWN_ERROR = 0;

    String keywords();

    void onDismissAd(MobclixFullScreenAdView mobclixFullScreenAdView);

    void onFailedLoad(MobclixFullScreenAdView mobclixFullScreenAdView, int i);

    void onFinishLoad(MobclixFullScreenAdView mobclixFullScreenAdView);

    void onPresentAd(MobclixFullScreenAdView mobclixFullScreenAdView);

    String query();
}
