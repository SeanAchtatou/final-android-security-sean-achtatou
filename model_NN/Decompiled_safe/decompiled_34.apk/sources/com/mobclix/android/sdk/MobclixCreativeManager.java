package com.mobclix.android.sdk;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class MobclixCreativeManager {
    JSONArray creatives;
    int nCreative = 0;

    MobclixCreativeManager(String rawResponse, int n) {
        this.nCreative = n;
        try {
            this.creatives = new JSONObject(rawResponse).getJSONArray("creatives");
        } catch (JSONException e) {
        }
    }

    /* access modifiers changed from: package-private */
    public int getIndex() {
        return this.nCreative;
    }

    /* access modifiers changed from: package-private */
    public int length() {
        return this.creatives.length();
    }

    /* access modifiers changed from: package-private */
    public boolean nextCreative() {
        this.nCreative++;
        if (this.nCreative >= this.creatives.length()) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public JSONObject getCreative() {
        try {
            return this.creatives.getJSONObject(this.nCreative);
        } catch (JSONException e) {
            return null;
        }
    }
}
