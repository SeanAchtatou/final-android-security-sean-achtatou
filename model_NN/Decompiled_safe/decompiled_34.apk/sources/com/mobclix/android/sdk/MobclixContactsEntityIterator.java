package com.mobclix.android.sdk;

import android.content.Entity;
import java.util.Iterator;

public interface MobclixContactsEntityIterator extends Iterator<Entity> {
    void close();

    void reset();
}
