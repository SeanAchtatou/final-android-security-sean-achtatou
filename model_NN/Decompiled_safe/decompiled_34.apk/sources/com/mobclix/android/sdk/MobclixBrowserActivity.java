package com.mobclix.android.sdk;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Browser;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;
import com.mobclix.android.sdk.Mobclix;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import org.json.JSONArray;
import org.json.JSONObject;

public class MobclixBrowserActivity extends Activity {
    private static final int TYPE_ADD_CONTACT = 8;
    private static final int TYPE_BROWSER = 2;
    private static final int TYPE_CAMERA = 4;
    private static final int TYPE_EXPANDER = 3;
    private static final int TYPE_FULLSCREEN = 9;
    private static final int TYPE_GALLERY = 5;
    private static final int TYPE_GALLERY_TO_SERVER = 6;
    private static final int TYPE_GET_CONTACT = 7;
    private static final int TYPE_HTML5_VIDEO = 10;
    private static final int TYPE_OFFER = 1;
    private static final int TYPE_VIDEO = 0;
    private final int MENU_BOOKMARK = 0;
    private final int MENU_CLOSE = 2;
    private final int MENU_FORWARD = 1;
    private String TAG = "mobclix-browser";
    /* access modifiers changed from: private */
    public LinkedList<Thread> asyncRequestThreads = new LinkedList<>();
    private String data = "";
    boolean firstOpen = true;
    private Intent forwardingIntent = null;
    /* access modifiers changed from: private */
    public ResourceResponseHandler handler = new ResourceResponseHandler();
    Uri imageUri;
    FrameLayout mFrame;
    private float scale = 1.0f;
    ScreenReceiver screenReceiver;
    private int type;
    /* access modifiers changed from: private */
    public View view;

    /* access modifiers changed from: private */
    public int dp(int p) {
        return (int) (this.scale * ((float) p));
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r14) {
        /*
            r13 = this;
            r11 = 2
            java.lang.String r9 = ".type"
            super.onCreate(r14)
            android.content.res.Resources r9 = r13.getResources()     // Catch:{ Throwable -> 0x00c2 }
            android.util.DisplayMetrics r9 = r9.getDisplayMetrics()     // Catch:{ Throwable -> 0x00c2 }
            float r9 = r9.density     // Catch:{ Throwable -> 0x00c2 }
            r13.scale = r9     // Catch:{ Throwable -> 0x00c2 }
            android.content.Intent r9 = r13.getIntent()     // Catch:{ Throwable -> 0x00c2 }
            android.os.Bundle r2 = r9.getExtras()     // Catch:{ Throwable -> 0x00c2 }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r10 = r13.getPackageName()     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r10 = java.lang.String.valueOf(r10)     // Catch:{ Throwable -> 0x00c2 }
            r9.<init>(r10)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r10 = ".data"
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r9 = r9.toString()     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r9 = r2.getString(r9)     // Catch:{ Throwable -> 0x00c2 }
            r13.data = r9     // Catch:{ Throwable -> 0x00c2 }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r10 = r13.getPackageName()     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r10 = java.lang.String.valueOf(r10)     // Catch:{ Throwable -> 0x00c2 }
            r9.<init>(r10)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r10 = ".type"
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r9 = r9.toString()     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r9 = r2.getString(r9)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r10 = "video"
            boolean r9 = r9.equals(r10)     // Catch:{ Throwable -> 0x00c2 }
            if (r9 == 0) goto L_0x008e
            r9 = 0
            r13.type = r9     // Catch:{ Throwable -> 0x00c2 }
            r9 = 1
            r13.requestWindowFeature(r9)     // Catch:{ Throwable -> 0x00c2 }
            r9 = 0
            r13.setRequestedOrientation(r9)     // Catch:{ Throwable -> 0x00c2 }
            com.mobclix.android.sdk.MobclixBrowserActivity$MobclixVideoView r9 = new com.mobclix.android.sdk.MobclixBrowserActivity$MobclixVideoView     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r10 = r13.data     // Catch:{ Throwable -> 0x00c2 }
            r9.<init>(r13, r10)     // Catch:{ Throwable -> 0x00c2 }
            r13.view = r9     // Catch:{ Throwable -> 0x00c2 }
        L_0x006e:
            android.widget.FrameLayout r9 = new android.widget.FrameLayout     // Catch:{ Throwable -> 0x00c2 }
            r9.<init>(r13)     // Catch:{ Throwable -> 0x00c2 }
            r13.mFrame = r9     // Catch:{ Throwable -> 0x00c2 }
            android.widget.FrameLayout r9 = r13.mFrame     // Catch:{ Throwable -> 0x00c2 }
            android.widget.TableLayout$LayoutParams r10 = new android.widget.TableLayout$LayoutParams     // Catch:{ Throwable -> 0x00c2 }
            r11 = -1
            r12 = -1
            r10.<init>(r11, r12)     // Catch:{ Throwable -> 0x00c2 }
            r9.setLayoutParams(r10)     // Catch:{ Throwable -> 0x00c2 }
            android.widget.FrameLayout r9 = r13.mFrame     // Catch:{ Throwable -> 0x00c2 }
            r13.setContentView(r9)     // Catch:{ Throwable -> 0x00c2 }
            android.widget.FrameLayout r9 = r13.mFrame     // Catch:{ Throwable -> 0x00c2 }
            android.view.View r10 = r13.view     // Catch:{ Throwable -> 0x00c2 }
            r9.addView(r10)     // Catch:{ Throwable -> 0x00c2 }
        L_0x008d:
            return
        L_0x008e:
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r10 = r13.getPackageName()     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r10 = java.lang.String.valueOf(r10)     // Catch:{ Throwable -> 0x00c2 }
            r9.<init>(r10)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r10 = ".type"
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r9 = r9.toString()     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r9 = r2.getString(r9)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r10 = "browser"
            boolean r9 = r9.equals(r10)     // Catch:{ Throwable -> 0x00c2 }
            if (r9 == 0) goto L_0x00c8
            r9 = 2
            r13.type = r9     // Catch:{ Throwable -> 0x00c2 }
            r9 = 2
            r13.requestWindowFeature(r9)     // Catch:{ Throwable -> 0x00c2 }
            com.mobclix.android.sdk.MobclixBrowserActivity$MobclixBrowser r9 = new com.mobclix.android.sdk.MobclixBrowserActivity$MobclixBrowser     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r10 = r13.data     // Catch:{ Throwable -> 0x00c2 }
            r9.<init>(r13, r10)     // Catch:{ Throwable -> 0x00c2 }
            r13.view = r9     // Catch:{ Throwable -> 0x00c2 }
            goto L_0x006e
        L_0x00c2:
            r9 = move-exception
            r6 = r9
            r13.finish()
            goto L_0x008d
        L_0x00c8:
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r10 = r13.getPackageName()     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r10 = java.lang.String.valueOf(r10)     // Catch:{ Throwable -> 0x00c2 }
            r9.<init>(r10)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r10 = ".type"
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r9 = r9.toString()     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r9 = r2.getString(r9)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r10 = "expander"
            boolean r9 = r9.equals(r10)     // Catch:{ Throwable -> 0x00c2 }
            if (r9 == 0) goto L_0x0120
            r9 = 3
            r13.type = r9     // Catch:{ Throwable -> 0x00c2 }
            r9 = 1
            r13.requestWindowFeature(r9)     // Catch:{ Throwable -> 0x00c2 }
            android.view.Window r8 = r13.getWindow()     // Catch:{ Throwable -> 0x00c2 }
            r9 = 1024(0x400, float:1.435E-42)
            r10 = 1024(0x400, float:1.435E-42)
            r8.setFlags(r9, r10)     // Catch:{ Throwable -> 0x00c2 }
            com.mobclix.android.sdk.MobclixBrowserActivity$MobclixExpander r9 = new com.mobclix.android.sdk.MobclixBrowserActivity$MobclixExpander     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r10 = r13.data     // Catch:{ Throwable -> 0x00c2 }
            r9.<init>(r13, r10)     // Catch:{ Throwable -> 0x00c2 }
            r13.view = r9     // Catch:{ Throwable -> 0x00c2 }
            android.content.IntentFilter r4 = new android.content.IntentFilter     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r9 = "android.intent.action.SCREEN_ON"
            r4.<init>(r9)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r9 = "android.intent.action.SCREEN_OFF"
            r4.addAction(r9)     // Catch:{ Throwable -> 0x00c2 }
            com.mobclix.android.sdk.MobclixBrowserActivity$ScreenReceiver r9 = new com.mobclix.android.sdk.MobclixBrowserActivity$ScreenReceiver     // Catch:{ Throwable -> 0x00c2 }
            r9.<init>()     // Catch:{ Throwable -> 0x00c2 }
            r13.screenReceiver = r9     // Catch:{ Throwable -> 0x00c2 }
            com.mobclix.android.sdk.MobclixBrowserActivity$ScreenReceiver r9 = r13.screenReceiver     // Catch:{ Throwable -> 0x00c2 }
            r13.registerReceiver(r9, r4)     // Catch:{ Throwable -> 0x00c2 }
            goto L_0x006e
        L_0x0120:
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r10 = r13.getPackageName()     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r10 = java.lang.String.valueOf(r10)     // Catch:{ Throwable -> 0x00c2 }
            r9.<init>(r10)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r10 = ".type"
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r9 = r9.toString()     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r9 = r2.getString(r9)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r10 = "fullscreen"
            boolean r9 = r9.equals(r10)     // Catch:{ Throwable -> 0x00c2 }
            if (r9 == 0) goto L_0x0177
            r9 = 9
            r13.type = r9     // Catch:{ Throwable -> 0x00c2 }
            r9 = 1
            r13.requestWindowFeature(r9)     // Catch:{ Throwable -> 0x00c2 }
            android.view.Window r8 = r13.getWindow()     // Catch:{ Throwable -> 0x00c2 }
            r9 = 1024(0x400, float:1.435E-42)
            r10 = 1024(0x400, float:1.435E-42)
            r8.setFlags(r9, r10)     // Catch:{ Throwable -> 0x00c2 }
            com.mobclix.android.sdk.MobclixBrowserActivity$MobclixFullScreenAd r9 = new com.mobclix.android.sdk.MobclixBrowserActivity$MobclixFullScreenAd     // Catch:{ Throwable -> 0x00c2 }
            r9.<init>(r13)     // Catch:{ Throwable -> 0x00c2 }
            r13.view = r9     // Catch:{ Throwable -> 0x00c2 }
            android.content.IntentFilter r4 = new android.content.IntentFilter     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r9 = "android.intent.action.SCREEN_ON"
            r4.<init>(r9)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r9 = "android.intent.action.SCREEN_OFF"
            r4.addAction(r9)     // Catch:{ Throwable -> 0x00c2 }
            com.mobclix.android.sdk.MobclixBrowserActivity$ScreenReceiver r9 = new com.mobclix.android.sdk.MobclixBrowserActivity$ScreenReceiver     // Catch:{ Throwable -> 0x00c2 }
            r9.<init>()     // Catch:{ Throwable -> 0x00c2 }
            r13.screenReceiver = r9     // Catch:{ Throwable -> 0x00c2 }
            com.mobclix.android.sdk.MobclixBrowserActivity$ScreenReceiver r9 = r13.screenReceiver     // Catch:{ Throwable -> 0x00c2 }
            r13.registerReceiver(r9, r4)     // Catch:{ Throwable -> 0x00c2 }
            goto L_0x006e
        L_0x0177:
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r10 = r13.getPackageName()     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r10 = java.lang.String.valueOf(r10)     // Catch:{ Throwable -> 0x00c2 }
            r9.<init>(r10)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r10 = ".type"
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r9 = r9.toString()     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r9 = r2.getString(r9)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r10 = "camera"
            boolean r9 = r9.equals(r10)     // Catch:{ Throwable -> 0x00c2 }
            if (r9 == 0) goto L_0x01dc
            r9 = 4
            r13.type = r9     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r3 = "camera.jpg"
            android.content.ContentValues r7 = new android.content.ContentValues     // Catch:{ Throwable -> 0x00c2 }
            r7.<init>()     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r9 = "title"
            r7.put(r9, r3)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r9 = "description"
            java.lang.String r10 = "Image capture by camera"
            r7.put(r9, r10)     // Catch:{ Throwable -> 0x00c2 }
            android.content.ContentResolver r9 = r13.getContentResolver()     // Catch:{ Exception -> 0x01d8 }
            android.net.Uri r10 = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI     // Catch:{ Exception -> 0x01d8 }
            android.net.Uri r9 = r9.insert(r10, r7)     // Catch:{ Exception -> 0x01d8 }
            r13.imageUri = r9     // Catch:{ Exception -> 0x01d8 }
            android.content.Intent r9 = new android.content.Intent     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r10 = "android.media.action.IMAGE_CAPTURE"
            r9.<init>(r10)     // Catch:{ Throwable -> 0x00c2 }
            r13.forwardingIntent = r9     // Catch:{ Throwable -> 0x00c2 }
            android.content.Intent r9 = r13.forwardingIntent     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r10 = "output"
            android.net.Uri r11 = r13.imageUri     // Catch:{ Throwable -> 0x00c2 }
            r9.putExtra(r10, r11)     // Catch:{ Throwable -> 0x00c2 }
            android.content.Intent r9 = r13.forwardingIntent     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r10 = "android.intent.extra.videoQuality"
            r11 = 0
            r9.putExtra(r10, r11)     // Catch:{ Throwable -> 0x00c2 }
            goto L_0x008d
        L_0x01d8:
            r9 = move-exception
            r1 = r9
            goto L_0x008d
        L_0x01dc:
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r10 = r13.getPackageName()     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r10 = java.lang.String.valueOf(r10)     // Catch:{ Throwable -> 0x00c2 }
            r9.<init>(r10)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r10 = ".type"
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r9 = r9.toString()     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r9 = r2.getString(r9)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r10 = "gallery"
            boolean r9 = r9.equals(r10)     // Catch:{ Throwable -> 0x00c2 }
            if (r9 == 0) goto L_0x0219
            r9 = 5
            r13.type = r9     // Catch:{ Throwable -> 0x00c2 }
            android.content.Intent r9 = new android.content.Intent     // Catch:{ Throwable -> 0x00c2 }
            r9.<init>()     // Catch:{ Throwable -> 0x00c2 }
            r13.forwardingIntent = r9     // Catch:{ Throwable -> 0x00c2 }
            android.content.Intent r9 = r13.forwardingIntent     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r10 = "image/*"
            r9.setType(r10)     // Catch:{ Throwable -> 0x00c2 }
            android.content.Intent r9 = r13.forwardingIntent     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r10 = "android.intent.action.GET_CONTENT"
            r9.setAction(r10)     // Catch:{ Throwable -> 0x00c2 }
            goto L_0x008d
        L_0x0219:
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r10 = r13.getPackageName()     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r10 = java.lang.String.valueOf(r10)     // Catch:{ Throwable -> 0x00c2 }
            r9.<init>(r10)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r10 = ".type"
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r9 = r9.toString()     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r9 = r2.getString(r9)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r10 = "sendToServer"
            boolean r9 = r9.equals(r10)     // Catch:{ Throwable -> 0x00c2 }
            if (r9 == 0) goto L_0x0256
            r9 = 6
            r13.type = r9     // Catch:{ Throwable -> 0x00c2 }
            android.content.Intent r9 = new android.content.Intent     // Catch:{ Throwable -> 0x00c2 }
            r9.<init>()     // Catch:{ Throwable -> 0x00c2 }
            r13.forwardingIntent = r9     // Catch:{ Throwable -> 0x00c2 }
            android.content.Intent r9 = r13.forwardingIntent     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r10 = "image/*"
            r9.setType(r10)     // Catch:{ Throwable -> 0x00c2 }
            android.content.Intent r9 = r13.forwardingIntent     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r10 = "android.intent.action.GET_CONTENT"
            r9.setAction(r10)     // Catch:{ Throwable -> 0x00c2 }
            goto L_0x008d
        L_0x0256:
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r10 = r13.getPackageName()     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r10 = java.lang.String.valueOf(r10)     // Catch:{ Throwable -> 0x00c2 }
            r9.<init>(r10)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r10 = ".type"
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r9 = r9.toString()     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r9 = r2.getString(r9)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r10 = "contact"
            boolean r9 = r9.equals(r10)     // Catch:{ Throwable -> 0x00c2 }
            if (r9 == 0) goto L_0x027e
            r9 = 7
            r13.type = r9     // Catch:{ Throwable -> 0x00c2 }
            goto L_0x008d
        L_0x027e:
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r10 = r13.getPackageName()     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r10 = java.lang.String.valueOf(r10)     // Catch:{ Throwable -> 0x00c2 }
            r9.<init>(r10)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r10 = ".type"
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r9 = r9.toString()     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r9 = r2.getString(r9)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r10 = "addContact"
            boolean r9 = r9.equals(r10)     // Catch:{ Throwable -> 0x00c2 }
            if (r9 == 0) goto L_0x0301
            r9 = 8
            r13.type = r9     // Catch:{ Throwable -> 0x00c2 }
            org.json.JSONObject r0 = new org.json.JSONObject     // Catch:{ Exception -> 0x02d1 }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02d1 }
            java.lang.String r10 = r13.getPackageName()     // Catch:{ Exception -> 0x02d1 }
            java.lang.String r10 = java.lang.String.valueOf(r10)     // Catch:{ Exception -> 0x02d1 }
            r9.<init>(r10)     // Catch:{ Exception -> 0x02d1 }
            java.lang.String r10 = ".data"
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ Exception -> 0x02d1 }
            java.lang.String r9 = r9.toString()     // Catch:{ Exception -> 0x02d1 }
            java.lang.String r9 = r2.getString(r9)     // Catch:{ Exception -> 0x02d1 }
            r0.<init>(r9)     // Catch:{ Exception -> 0x02d1 }
            com.mobclix.android.sdk.MobclixContacts r9 = com.mobclix.android.sdk.MobclixContacts.getInstance()     // Catch:{ Exception -> 0x02d1 }
            android.content.Intent r9 = r9.getAddContactIntent(r0)     // Catch:{ Exception -> 0x02d1 }
            r13.forwardingIntent = r9     // Catch:{ Exception -> 0x02d1 }
            goto L_0x008d
        L_0x02d1:
            r9 = move-exception
            r1 = r9
            com.mobclix.android.sdk.Mobclix r9 = com.mobclix.android.sdk.Mobclix.getInstance()     // Catch:{ Throwable -> 0x00c2 }
            java.lang.ref.SoftReference<com.mobclix.android.sdk.MobclixWebView> r9 = r9.cameraWebview     // Catch:{ Throwable -> 0x00c2 }
            if (r9 == 0) goto L_0x02fc
            com.mobclix.android.sdk.Mobclix r9 = com.mobclix.android.sdk.Mobclix.getInstance()     // Catch:{ Throwable -> 0x00c2 }
            java.lang.ref.SoftReference<com.mobclix.android.sdk.MobclixWebView> r9 = r9.cameraWebview     // Catch:{ Throwable -> 0x00c2 }
            java.lang.Object r9 = r9.get()     // Catch:{ Throwable -> 0x00c2 }
            if (r9 == 0) goto L_0x02fc
            com.mobclix.android.sdk.Mobclix r9 = com.mobclix.android.sdk.Mobclix.getInstance()     // Catch:{ Throwable -> 0x00c2 }
            java.lang.ref.SoftReference<com.mobclix.android.sdk.MobclixWebView> r9 = r9.cameraWebview     // Catch:{ Throwable -> 0x00c2 }
            java.lang.Object r9 = r9.get()     // Catch:{ Throwable -> 0x00c2 }
            com.mobclix.android.sdk.MobclixWebView r9 = (com.mobclix.android.sdk.MobclixWebView) r9     // Catch:{ Throwable -> 0x00c2 }
            com.mobclix.android.sdk.MobclixJavascriptInterface r9 = r9.getJavascriptInterface()     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r10 = "Error getting contact."
            r9.contactCanceled(r10)     // Catch:{ Throwable -> 0x00c2 }
        L_0x02fc:
            r13.finish()     // Catch:{ Throwable -> 0x00c2 }
            goto L_0x008d
        L_0x0301:
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r10 = r13.getPackageName()     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r10 = java.lang.String.valueOf(r10)     // Catch:{ Throwable -> 0x00c2 }
            r9.<init>(r10)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r10 = ".type"
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r9 = r9.toString()     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r9 = r2.getString(r9)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r10 = "html5Video"
            boolean r9 = r9.equals(r10)     // Catch:{ Throwable -> 0x00c2 }
            if (r9 == 0) goto L_0x006e
            r9 = 10
            r13.type = r9     // Catch:{ Throwable -> 0x00c2 }
            android.content.res.Resources r9 = r13.getResources()     // Catch:{ Throwable -> 0x00c2 }
            android.content.res.Configuration r9 = r9.getConfiguration()     // Catch:{ Throwable -> 0x00c2 }
            int r5 = r9.orientation     // Catch:{ Throwable -> 0x00c2 }
            if (r5 != r11) goto L_0x0359
            r5 = 0
        L_0x0335:
            r13.setRequestedOrientation(r5)     // Catch:{ Throwable -> 0x00c2 }
            r9 = 1
            r13.requestWindowFeature(r9)     // Catch:{ Throwable -> 0x00c2 }
            android.view.Window r9 = r13.getWindow()     // Catch:{ Throwable -> 0x00c2 }
            r10 = 128(0x80, float:1.794E-43)
            r9.addFlags(r10)     // Catch:{ Throwable -> 0x00c2 }
            android.view.Window r8 = r13.getWindow()     // Catch:{ Throwable -> 0x00c2 }
            r9 = 1024(0x400, float:1.435E-42)
            r10 = 1024(0x400, float:1.435E-42)
            r8.setFlags(r9, r10)     // Catch:{ Throwable -> 0x00c2 }
            com.mobclix.android.sdk.MobclixBrowserActivity$MobclixHTML5Video r9 = new com.mobclix.android.sdk.MobclixBrowserActivity$MobclixHTML5Video     // Catch:{ Throwable -> 0x00c2 }
            r9.<init>(r13)     // Catch:{ Throwable -> 0x00c2 }
            r13.view = r9     // Catch:{ Throwable -> 0x00c2 }
            goto L_0x006e
        L_0x0359:
            r5 = 1
            goto L_0x0335
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobclix.android.sdk.MobclixBrowserActivity.onCreate(android.os.Bundle):void");
    }

    public void onStart() {
        super.onStart();
        try {
            switch (this.type) {
                default:
                    return;
                case 0:
                    if (!((MobclixVideoView) this.view).loadComplete) {
                        runNextAsyncRequest();
                        ((MobclixVideoView) this.view).mProgressDialog = ProgressDialog.show(this, "", "Loading...", true, true);
                        ((MobclixVideoView) this.view).mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                            public void onCancel(DialogInterface dialog) {
                                MobclixBrowserActivity.this.finish();
                            }
                        });
                        return;
                    }
                    ((MobclixVideoView) this.view).mBackgroundImage.setVisibility(0);
                    ((MobclixVideoView) this.view).mVideoView.start();
                    return;
            }
        } catch (Exception e) {
            finish();
        }
        finish();
    }

    public void onResume() {
        super.onResume();
        try {
            switch (this.type) {
                case 3:
                case TYPE_FULLSCREEN /*9*/:
                    ((MobclixFullScreen) this.view).webview.getJavascriptInterface().resumeListeners();
                    ((MobclixFullScreen) this.view).wasAdActivity = false;
                    if (!this.firstOpen) {
                        ((MobclixFullScreen) this.view).webview.getJavascriptInterface().adDidReturnFromHidden();
                    }
                    this.firstOpen = false;
                    return;
                case 4:
                    if (Mobclix.getInstance().cameraWebview == null || Mobclix.getInstance().cameraWebview.get() != null) {
                        finish();
                        return;
                    } else if (this.forwardingIntent != null) {
                        startActivityForResult(this.forwardingIntent, this.type);
                        return;
                    } else {
                        finish();
                        return;
                    }
                case 5:
                    if (this.forwardingIntent != null) {
                        startActivityForResult(Intent.createChooser(this.forwardingIntent, "Select Picture"), this.type);
                        return;
                    } else {
                        finish();
                        return;
                    }
                case 6:
                    if (this.forwardingIntent != null) {
                        startActivityForResult(Intent.createChooser(this.forwardingIntent, "Select Picture"), this.type);
                        return;
                    } else {
                        finish();
                        return;
                    }
                case 7:
                    startActivityForResult(MobclixContacts.getInstance().getPickContactIntent(), this.type);
                    return;
                case 8:
                    try {
                        startActivityForResult(this.forwardingIntent, this.type);
                        return;
                    } catch (Exception e) {
                        if (Mobclix.getInstance().cameraWebview == null || Mobclix.getInstance().cameraWebview.get() != null) {
                            Mobclix.getInstance().cameraWebview.get().getJavascriptInterface().contactCanceled("Error getting contact.");
                        }
                        finish();
                        return;
                    }
                default:
                    return;
            }
        } catch (Exception e2) {
            finish();
        }
        finish();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data2) {
        Bitmap scaledPic;
        Bitmap scaledPic2;
        try {
            if (Mobclix.getInstance().cameraWebview == null || Mobclix.getInstance().cameraWebview.get() == null) {
                finish();
            }
            MobclixWebView webview = Mobclix.getInstance().cameraWebview.get();
            if (requestCode == 4) {
                if (resultCode == -1) {
                    try {
                        Bitmap pic = BitmapFactory.decodeFile(convertImageUriToFile(this.imageUri, this).getAbsolutePath());
                        int height = webview.getJavascriptInterface().photoHeight;
                        int width = webview.getJavascriptInterface().photoWidth;
                        if (height == 0 || width == 0) {
                            scaledPic2 = pic;
                        } else {
                            scaledPic2 = Bitmap.createScaledBitmap(pic, webview.getJavascriptInterface().photoWidth, webview.getJavascriptInterface().photoHeight, true);
                        }
                        ByteArrayOutputStream bos = new ByteArrayOutputStream();
                        scaledPic2.compress(Bitmap.CompressFormat.JPEG, TYPE_HTML5_VIDEO, bos);
                        webview.getJavascriptInterface().photoTaken(Base64.encodeBytes(bos.toByteArray()));
                        bos.close();
                        System.gc();
                    } catch (Exception e) {
                        webview.getJavascriptInterface().photoCanceled(e.toString());
                        webview.getJavascriptInterface().photoCanceled("Error processing photo.");
                    }
                } else if (resultCode == 0) {
                    webview.getJavascriptInterface().photoCanceled("User canceled.");
                }
            } else if (requestCode == 5) {
                if (resultCode == -1) {
                    try {
                        Uri selectedImageUri = data2.getData();
                        String filemanagerstring = selectedImageUri.getPath();
                        String selectedImagePath = getPath(selectedImageUri);
                        if (selectedImagePath == null) {
                            selectedImagePath = filemanagerstring;
                        }
                        File in = new File(selectedImagePath);
                        Bitmap pic2 = BitmapFactory.decodeFile(in.getAbsolutePath());
                        int height2 = webview.getJavascriptInterface().photoHeight;
                        int width2 = webview.getJavascriptInterface().photoWidth;
                        if (height2 == 0 || width2 == 0) {
                            scaledPic = pic2;
                        } else {
                            scaledPic = Bitmap.createScaledBitmap(pic2, webview.getJavascriptInterface().photoWidth, webview.getJavascriptInterface().photoHeight, true);
                        }
                        ByteArrayOutputStream bos2 = new ByteArrayOutputStream();
                        scaledPic.compress(Bitmap.CompressFormat.JPEG, TYPE_HTML5_VIDEO, bos2);
                        webview.getJavascriptInterface().photoTaken(Base64.encodeBytes(bos2.toByteArray()));
                        bos2.close();
                        System.gc();
                        in.delete();
                    } catch (Exception e2) {
                        webview.getJavascriptInterface().photoCanceled(e2.toString());
                        webview.getJavascriptInterface().photoCanceled("Error processing photo.");
                    }
                } else if (resultCode == 0) {
                    webview.getJavascriptInterface().photoCanceled("User canceled.");
                }
            } else if (requestCode == 6) {
                if (resultCode == -1) {
                    try {
                        Uri selectedImageUri2 = data2.getData();
                        String filemanagerstring2 = selectedImageUri2.getPath();
                        String selectedImagePath2 = getPath(selectedImageUri2);
                        if (selectedImagePath2 == null) {
                            selectedImagePath2 = filemanagerstring2;
                        }
                        webview.getJavascriptInterface().sendImageToServer(selectedImagePath2);
                    } catch (Exception e3) {
                        webview.getJavascriptInterface().photoCanceled(e3.toString());
                        webview.getJavascriptInterface().photoCanceled("Error processing photo.");
                    }
                } else if (resultCode == 0) {
                    webview.getJavascriptInterface().photoCanceled("User canceled.");
                }
            } else if (requestCode == 7) {
                if (resultCode == -1) {
                    try {
                        webview.getJavascriptInterface().contactPicked(data2.getData());
                    } catch (Exception e4) {
                        webview.getJavascriptInterface().contactCanceled("Error getting contact.");
                    }
                } else if (resultCode == 0) {
                    webview.getJavascriptInterface().contactCanceled("User canceled.");
                }
            } else if (requestCode == 8) {
                if (resultCode == -1) {
                    try {
                        webview.getJavascriptInterface().contactAdded();
                    } catch (Exception e5) {
                        webview.getJavascriptInterface().contactCanceled("Error getting contact.");
                    }
                } else if (resultCode == 0) {
                    webview.getJavascriptInterface().contactCanceled("User canceled.");
                }
            }
            Mobclix.getInstance().cameraWebview = null;
            finish();
        } catch (Exception e6) {
            finish();
        }
    }

    public String getPath(Uri uri) {
        Cursor cursor = managedQuery(uri, new String[]{"_data"}, null, null, null);
        if (cursor == null) {
            return null;
        }
        int column_index = cursor.getColumnIndexOrThrow("_data");
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public static File convertImageUriToFile(Uri imageUri2, Activity activity) {
        Cursor cursor = null;
        try {
            cursor = activity.managedQuery(imageUri2, new String[]{"_data", "_id", "orientation"}, null, null, null);
            int file_ColumnIndex = cursor.getColumnIndexOrThrow("_data");
            int orientation_ColumnIndex = cursor.getColumnIndexOrThrow("orientation");
            if (cursor.moveToFirst()) {
                String string = cursor.getString(orientation_ColumnIndex);
                File file = new File(cursor.getString(file_ColumnIndex));
            }
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public void onSaveInstanceState(Bundle outState) {
        if (this.imageUri != null) {
            outState.putParcelable("imageUri", this.imageUri);
        }
        super.onSaveInstanceState(outState);
    }

    public void onRestoreInstanceState(Bundle inState) {
        if (inState.containsKey("imageUri")) {
            this.imageUri = (Uri) inState.getParcelable("imageUri");
        }
    }

    public void onStop() {
        super.onStop();
        try {
            switch (this.type) {
                case 3:
                    if (((MobclixFullScreen) this.view).wasAdActivity) {
                        ((MobclixFullScreen) this.view).webview.getJavascriptInterface().adWillBecomeHidden();
                        return;
                    } else if (((MobclixFullScreen) this.view).open) {
                        ((MobclixFullScreen) this.view).exit(1);
                        ((MobclixFullScreen) this.view).webview.getJavascriptInterface().adWillBecomeHidden();
                        return;
                    } else {
                        return;
                    }
                case TYPE_FULLSCREEN /*9*/:
                    if (((MobclixFullScreen) this.view).wasAdActivity) {
                        ((MobclixFullScreen) this.view).webview.getJavascriptInterface().adWillBecomeHidden();
                        return;
                    } else if (((MobclixFullScreen) this.view).open) {
                        ((MobclixFullScreen) this.view).webview.getJavascriptInterface().adWillBecomeHidden();
                        return;
                    } else {
                        return;
                    }
                case TYPE_HTML5_VIDEO /*10*/:
                    try {
                        ((MobclixHTML5Video) this.view).video.stopPlayback();
                        ((MobclixHTML5Video) this.view).video = null;
                    } catch (Exception e) {
                        try {
                            Log.v(this.TAG, e.toString());
                        } catch (Exception e2) {
                        }
                    }
                    try {
                        ((MobclixHTML5Video) this.view).webview.mCustomViewCallback.getClass().getMethod("onCustomViewHidden", new Class[0]).invoke(((MobclixHTML5Video) this.view).webview.mCustomViewCallback, new Object[0]);
                        ((MobclixHTML5Video) this.view).webview.mCustomViewCallback = null;
                    } catch (Exception e3) {
                        Log.v(this.TAG, e3.toString());
                    }
                    finish();
                    return;
                default:
                    return;
            }
        } catch (Exception e4) {
            finish();
        }
        finish();
    }

    public void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(this.screenReceiver);
        } catch (Exception e) {
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public void onPause() {
        super.onPause();
        try {
            switch (this.type) {
                case 0:
                    ((MobclixVideoView) this.view).mProgressDialog.dismiss();
                    return;
                case 1:
                case 2:
                default:
                    return;
                case 3:
                    ((MobclixExpander) this.view).webview.getJavascriptInterface().pauseListeners();
                    return;
            }
        } catch (Exception e) {
            finish();
        }
        finish();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        try {
            switch (this.type) {
                case 0:
                    if (keyCode == 4) {
                        finish();
                    }
                    return super.onKeyDown(keyCode, event);
                case 2:
                    if (keyCode != 4 || !((MobclixBrowser) this.view).getWebView().canGoBack()) {
                        return super.onKeyDown(keyCode, event);
                    }
                    ((MobclixBrowser) this.view).getWebView().goBack();
                    return true;
                case 3:
                    if (keyCode != 4) {
                        return super.onKeyDown(keyCode, event);
                    }
                    if (((MobclixExpander) this.view).webview.canGoBack()) {
                        ((MobclixExpander) this.view).webview.loadAd();
                    }
                    ((MobclixExpander) this.view).exit(500);
                    return true;
                case TYPE_FULLSCREEN /*9*/:
                    if (keyCode != 4) {
                        return super.onKeyDown(keyCode, event);
                    }
                    if (!((MobclixFullScreenAd) this.view).webview.canGoBack()) {
                        ((MobclixFullScreenAd) this.view).exit(0);
                    } else if (((MobclixFullScreenAd) this.view).webview.canGoBackOrForward(-2)) {
                        ((MobclixFullScreenAd) this.view).webview.goBack();
                    } else {
                        ((MobclixFullScreenAd) this.view).webview.loadAd();
                        ((MobclixFullScreenAd) this.view).webview.clearHistory();
                    }
                    return true;
                default:
                    return super.onKeyDown(keyCode, event);
            }
        } catch (Exception e) {
            return super.onKeyDown(keyCode, event);
        }
        return super.onKeyDown(keyCode, event);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        try {
            switch (this.type) {
                case 2:
                case TYPE_FULLSCREEN /*9*/:
                    super.onCreateOptionsMenu(menu);
                    menu.add(0, 0, 0, "Bookmark").setIcon(17301555);
                    menu.add(0, 1, 0, "Forward").setIcon(17301565);
                    menu.add(0, 2, 0, "Close").setIcon(17301560);
                    return true;
                default:
                    return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 5 */
    public boolean onOptionsItemSelected(MenuItem item) {
        try {
            switch (this.type) {
                case 2:
                    switch (item.getItemId()) {
                        case 0:
                            Browser.saveBookmark(this, ((MobclixBrowser) this.view).getWebView().getTitle(), ((MobclixBrowser) this.view).getWebView().getUrl());
                            return true;
                        case 1:
                            if (((MobclixBrowser) this.view).getWebView().canGoForward()) {
                                ((MobclixBrowser) this.view).getWebView().goForward();
                            }
                            return true;
                        case 2:
                            finish();
                            return true;
                        default:
                            return super.onContextItemSelected(item);
                    }
                case TYPE_FULLSCREEN /*9*/:
                    switch (item.getItemId()) {
                        case 0:
                            Browser.saveBookmark(this, ((MobclixFullScreen) this.view).webview.getTitle(), ((MobclixFullScreen) this.view).webview.getUrl());
                            return true;
                        case 1:
                            if (((MobclixFullScreen) this.view).webview.canGoForward()) {
                                ((MobclixFullScreen) this.view).webview.goForward();
                            }
                            return true;
                        case 2:
                            ((MobclixFullScreen) this.view).exit(0);
                            return true;
                        default:
                            return super.onContextItemSelected(item);
                    }
                default:
                    return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    public void runNextAsyncRequest() {
        try {
            if (!this.asyncRequestThreads.isEmpty()) {
                this.asyncRequestThreads.removeFirst().start();
                return;
            }
            switch (this.type) {
                case 0:
                    ((MobclixVideoView) this.view).loadComplete = true;
                    ((MobclixVideoView) this.view).mProgressDialog.dismiss();
                    ((MobclixVideoView) this.view).createAdButtonBanner();
                    ((MobclixVideoView) this.view).mVideoView.setVisibility(0);
                    ((MobclixVideoView) this.view).mVideoView.start();
                    return;
                default:
                    return;
            }
        } catch (Exception e) {
            finish();
        }
    }

    class ResourceResponseHandler extends Handler {
        ResourceResponseHandler() {
        }

        public void handleMessage(Message msg) {
            MobclixBrowserActivity.this.runNextAsyncRequest();
        }
    }

    private class MobclixVideoView extends RelativeLayout implements MediaPlayer.OnPreparedListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener {
        private Activity activity;
        private LinearLayout adButtonBanner = null;
        private LinearLayout adButtons = null;
        private ArrayList<String> buttonImageUrls = new ArrayList<>();
        /* access modifiers changed from: private */
        public ArrayList<Bitmap> buttonImages = new ArrayList<>();
        private ArrayList<String> buttonUrls = new ArrayList<>();
        private String landingUrl = "";
        /* access modifiers changed from: private */
        public boolean loadComplete = false;
        /* access modifiers changed from: private */
        public ImageView mBackgroundImage;
        private MediaController mMediaController;
        /* access modifiers changed from: private */
        public ProgressDialog mProgressDialog = null;
        /* access modifiers changed from: private */
        public VideoView mVideoView;
        private String tagline = "";
        private String taglineImageUrl = "";
        /* access modifiers changed from: private */
        public ImageView taglineImageView = null;
        private ArrayList<String> trackingUrls = new ArrayList<>();
        /* access modifiers changed from: private */
        public boolean videoLoaded = false;
        private String videoUrl;

        MobclixVideoView(Activity a, String data) {
            super(a);
            this.activity = a;
            try {
                JSONObject responseObject = new JSONObject(data);
                try {
                    this.videoUrl = responseObject.getString("videoUrl");
                } catch (Exception e) {
                }
                try {
                    this.landingUrl = responseObject.getString("landingUrl");
                } catch (Exception e2) {
                }
                try {
                    this.tagline = responseObject.getString("tagline");
                } catch (Exception e3) {
                }
                try {
                    this.taglineImageUrl = responseObject.getString("taglineImageUrl");
                } catch (Exception e4) {
                }
                try {
                    JSONArray buttons = responseObject.getJSONArray("buttons");
                    for (int i = 0; i < buttons.length(); i++) {
                        this.buttonImageUrls.add(buttons.getJSONObject(i).getString("imageUrl"));
                        this.buttonUrls.add(buttons.getJSONObject(i).getString("url"));
                    }
                } catch (Exception e5) {
                }
                JSONArray tracking = responseObject.getJSONArray("trackingUrls");
                for (int i2 = 0; i2 < tracking.length(); i2++) {
                    this.trackingUrls.add(tracking.getString(i2));
                }
            } catch (Exception e6) {
            }
            setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
            loadVideoAd();
        }

        public void onPrepared(MediaPlayer mp) {
            this.videoLoaded = true;
            this.mBackgroundImage.setVisibility(8);
            this.mProgressDialog.dismiss();
        }

        public boolean onError(MediaPlayer mp, int what, int extra) {
            this.mProgressDialog.dismiss();
            removeView(this.mVideoView);
            return true;
        }

        public void onCompletion(MediaPlayer mp) {
            this.mBackgroundImage.setVisibility(0);
        }

        public void loadVideoAd() {
            MobclixBrowserActivity.this.getWindow().setFlags(1024, 1024);
            RelativeLayout.LayoutParams paramsCenter = new RelativeLayout.LayoutParams(-1, -1);
            paramsCenter.addRule(13);
            this.mVideoView = new VideoView(this.activity);
            this.mVideoView.setId(1337);
            this.mVideoView.setLayoutParams(paramsCenter);
            this.mMediaController = new MediaController(this.activity);
            this.mMediaController.setAnchorView(this.mVideoView);
            this.mVideoView.setVideoURI(Uri.parse(this.videoUrl));
            this.mVideoView.setMediaController(this.mMediaController);
            this.mVideoView.setOnPreparedListener(this);
            this.mVideoView.setOnErrorListener(this);
            this.mVideoView.setOnCompletionListener(this);
            this.mVideoView.setVisibility(4);
            addView(this.mVideoView);
            this.mBackgroundImage = new ImageView(this.activity);
            this.mBackgroundImage.setLayoutParams(paramsCenter);
            this.mBackgroundImage.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    if (MobclixVideoView.this.videoLoaded) {
                        MobclixVideoView.this.mBackgroundImage.setVisibility(8);
                        if (!MobclixVideoView.this.mVideoView.isPlaying()) {
                            MobclixVideoView.this.mVideoView.start();
                        }
                    }
                }
            });
            addView(this.mBackgroundImage);
            if ((!this.taglineImageUrl.equals("null") && !this.taglineImageUrl.equals("")) || !this.tagline.equals("")) {
                LinearLayout taglineWrap = new LinearLayout(this.activity);
                RelativeLayout.LayoutParams taglineLayout = new RelativeLayout.LayoutParams(-2, -2);
                taglineLayout.addRule(11);
                taglineWrap.setLayoutParams(taglineLayout);
                taglineWrap.setBackgroundColor(Color.parseColor("#CC666666"));
                taglineWrap.setPadding(MobclixBrowserActivity.this.dp(4), MobclixBrowserActivity.this.dp(4), MobclixBrowserActivity.this.dp(4), MobclixBrowserActivity.this.dp(4));
                if (this.taglineImageUrl.equals("null") || this.taglineImageUrl.equals("")) {
                    TextView taglineView = new TextView(this.activity);
                    taglineView.setText(this.tagline);
                    taglineWrap.addView(taglineView);
                } else {
                    this.taglineImageView = new ImageView(this.activity);
                    taglineWrap.addView(this.taglineImageView);
                    loadTaglineImage();
                }
                addView(taglineWrap);
            }
            MobclixBrowserActivity.this.setContentView(this);
            Iterator<String> it = this.buttonImageUrls.iterator();
            while (it.hasNext()) {
                loadButtonImage(it.next());
            }
            if (!this.landingUrl.equals("")) {
                loadBackgroundImage();
            }
            Iterator<String> it2 = this.trackingUrls.iterator();
            while (it2.hasNext()) {
                loadTrackingImage(it2.next());
            }
        }

        public void createAdButtonBanner() {
            if (this.buttonImages.size() != 0) {
                this.adButtonBanner = new LinearLayout(this.activity);
                this.adButtonBanner.setOrientation(1);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
                layoutParams.addRule(12);
                this.adButtonBanner.setLayoutParams(layoutParams);
                this.adButtonBanner.setBackgroundColor(Color.parseColor("#CC666666"));
                ImageView divider = new ImageView(this.activity);
                divider.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
                divider.setBackgroundResource(17301524);
                this.adButtonBanner.addView(divider);
                this.adButtons = new LinearLayout(this.activity);
                this.adButtons.setPadding(0, MobclixBrowserActivity.this.dp(4), 0, 0);
                for (int i = 0; i < this.buttonImages.size(); i++) {
                    ImageView b = new ImageView(this.activity);
                    Bitmap bmImg = this.buttonImages.get(i);
                    LinearLayout.LayoutParams buttonLayout = new LinearLayout.LayoutParams(MobclixBrowserActivity.this.dp(bmImg.getWidth()), MobclixBrowserActivity.this.dp(bmImg.getHeight()));
                    buttonLayout.weight = 1.0f;
                    b.setLayoutParams(buttonLayout);
                    b.setScaleType(ImageView.ScaleType.FIT_CENTER);
                    b.setImageBitmap(bmImg);
                    b.setOnClickListener(new ButtonOnClickListener(this.activity, this.buttonUrls.get(i)));
                    this.adButtons.addView(b);
                }
                this.adButtonBanner.addView(this.adButtons);
                addView(this.adButtonBanner);
            }
        }

        public void loadBackgroundImage() {
            MobclixBrowserActivity.this.asyncRequestThreads.add(new Thread(new Mobclix.FetchImageThread(this.landingUrl, new Mobclix.BitmapHandler() {
                public void handleMessage(Message m) {
                    if (this.bmImg != null) {
                        MobclixVideoView.this.mBackgroundImage.setImageBitmap(this.bmImg);
                    }
                    MobclixBrowserActivity.this.handler.sendEmptyMessage(0);
                }
            })));
        }

        public void loadTaglineImage() {
            MobclixBrowserActivity.this.asyncRequestThreads.add(new Thread(new Mobclix.FetchImageThread(this.taglineImageUrl, new Mobclix.BitmapHandler() {
                public void handleMessage(Message m) {
                    if (this.bmImg != null) {
                        int rawWidth = this.bmImg.getWidth();
                        int rawHeight = this.bmImg.getHeight();
                        MobclixVideoView.this.taglineImageView.setLayoutParams(new LinearLayout.LayoutParams(MobclixBrowserActivity.this.dp(rawWidth), MobclixBrowserActivity.this.dp(rawHeight)));
                        MobclixVideoView.this.taglineImageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                        MobclixVideoView.this.taglineImageView.setImageBitmap(this.bmImg);
                    }
                    MobclixBrowserActivity.this.handler.sendEmptyMessage(0);
                }
            })));
        }

        public void loadButtonImage(String url) {
            MobclixBrowserActivity.this.asyncRequestThreads.add(new Thread(new Mobclix.FetchImageThread(url, new Mobclix.BitmapHandler() {
                public void handleMessage(Message m) {
                    if (this.bmImg != null) {
                        MobclixVideoView.this.buttonImages.add(this.bmImg);
                    }
                    MobclixBrowserActivity.this.handler.sendEmptyMessage(0);
                }
            })));
        }

        public void loadTrackingImage(String url) {
            MobclixBrowserActivity.this.asyncRequestThreads.add(new Thread(new Mobclix.FetchImageThread(url, new Mobclix.BitmapHandler() {
                public void handleMessage(Message m) {
                    MobclixBrowserActivity.this.handler.sendEmptyMessage(0);
                }
            })));
        }

        class ButtonOnClickListener implements View.OnClickListener {
            private Context context;
            private String url;

            public ButtonOnClickListener(Context c, String u) {
                this.context = c;
                this.url = u;
            }

            public void onClick(View arg0) {
                this.context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(this.url)));
            }
        }
    }

    private class MobclixHTML5Video extends RelativeLayout implements MediaPlayer.OnPreparedListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener {
        private Activity activity;
        private FrameLayout mFrame = null;
        private MediaController mMediaController = null;
        private ProgressBar progressBar = null;
        /* access modifiers changed from: private */
        public VideoView video = null;
        /* access modifiers changed from: private */
        public MobclixWebView webview = null;

        MobclixHTML5Video(Activity a) {
            super(a);
            try {
                this.activity = a;
                setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
                setBackgroundColor(-16777216);
                this.progressBar = new ProgressBar(this.activity);
                this.webview = Mobclix.getInstance().cameraWebview.get();
                Mobclix.getInstance().cameraWebview = null;
                this.mFrame = (FrameLayout) Mobclix.getInstance().secondaryView.get();
                Mobclix.getInstance().secondaryView = null;
                if (this.mFrame == null) {
                    a.finish();
                }
                this.video = (VideoView) this.mFrame.getFocusedChild();
                this.video.setOnPreparedListener(this);
                this.video.setOnCompletionListener(this);
                this.video.setOnErrorListener(this);
                this.mFrame.removeView(this.video);
                this.mMediaController = new MediaController(this.activity);
                this.mMediaController.setAnchorView(this.video);
                this.video.setMediaController(this.mMediaController);
                RelativeLayout.LayoutParams videoLayout = new RelativeLayout.LayoutParams(-1, -1);
                videoLayout.addRule(14);
                videoLayout.addRule(15);
                this.video.setLayoutParams(videoLayout);
                RelativeLayout.LayoutParams centerLayout = new RelativeLayout.LayoutParams(-2, -2);
                centerLayout.addRule(14);
                centerLayout.addRule(15);
                this.progressBar.setLayoutParams(centerLayout);
                Mobclix.getInstance().secondaryView = null;
                addView(this.video);
                addView(this.progressBar);
                this.video.start();
                this.video.seekTo(0);
            } catch (Exception e) {
                a.finish();
            }
        }

        public void onPrepared(MediaPlayer mp) {
            this.progressBar.setVisibility(8);
        }

        public boolean onError(MediaPlayer mp, int what, int extra) {
            this.activity.finish();
            return true;
        }

        public void onCompletion(MediaPlayer mp) {
            this.activity.finish();
        }
    }

    private class MobclixBrowser extends RelativeLayout {
        /* access modifiers changed from: private */
        public Activity activity;
        private String browserType = "standard";
        private String cachedHTML = "";
        private WebView mWebView;
        private String url;

        /* JADX WARNING: Code restructure failed: missing block: B:21:0x00e4, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
            r11.url = "http://www.mobclix.com";
         */
        /* JADX WARNING: Code restructure failed: missing block: B:25:0x00ec, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
            r11.url = "http://www.mobclix.com";
         */
        /* JADX WARNING: Code restructure failed: missing block: B:35:0x0102, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:36:0x0103, code lost:
            r9 = r0;
            r11.browserType = "standard";
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Removed duplicated region for block: B:25:0x00ec A[ExcHandler: JSONException (r0v3 'e' org.json.JSONException A[CUSTOM_DECLARE]), Splitter:B:3:0x0011] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        MobclixBrowser(android.app.Activity r13, java.lang.String r14) {
            /*
                r11 = this;
                java.lang.String r1 = ""
                com.mobclix.android.sdk.MobclixBrowserActivity.this = r12
                r11.<init>(r13)
                java.lang.String r0 = ""
                r11.cachedHTML = r1
                java.lang.String r0 = "standard"
                r11.browserType = r0
                r11.activity = r13     // Catch:{ Exception -> 0x00f4 }
                org.json.JSONObject r10 = new org.json.JSONObject     // Catch:{ JSONException -> 0x00ec }
                r10.<init>(r14)     // Catch:{ JSONException -> 0x00ec }
                java.lang.String r0 = "url"
                java.lang.String r0 = r10.getString(r0)     // Catch:{ Exception -> 0x00e4, JSONException -> 0x00ec }
                r11.url = r0     // Catch:{ Exception -> 0x00e4, JSONException -> 0x00ec }
            L_0x001e:
                java.lang.String r0 = "cachedHTML"
                java.lang.String r0 = r10.getString(r0)     // Catch:{ Exception -> 0x00fa, JSONException -> 0x00ec }
                r11.cachedHTML = r0     // Catch:{ Exception -> 0x00fa, JSONException -> 0x00ec }
            L_0x0026:
                java.lang.String r0 = "browserType"
                java.lang.String r0 = r10.getString(r0)     // Catch:{ Exception -> 0x0102, JSONException -> 0x00ec }
                r11.browserType = r0     // Catch:{ Exception -> 0x0102, JSONException -> 0x00ec }
            L_0x002e:
                android.webkit.WebView r0 = new android.webkit.WebView     // Catch:{ Exception -> 0x00f4 }
                r0.<init>(r13)     // Catch:{ Exception -> 0x00f4 }
                r11.mWebView = r0     // Catch:{ Exception -> 0x00f4 }
                r0 = 1
                r12.setProgressBarVisibility(r0)     // Catch:{ Exception -> 0x00f4 }
                android.webkit.WebView r0 = r11.mWebView     // Catch:{ Exception -> 0x00f4 }
                android.webkit.WebSettings r0 = r0.getSettings()     // Catch:{ Exception -> 0x00f4 }
                r1 = 1
                r0.setJavaScriptEnabled(r1)     // Catch:{ Exception -> 0x00f4 }
                android.webkit.WebView r0 = r11.mWebView     // Catch:{ Exception -> 0x00f4 }
                com.mobclix.android.sdk.MobclixBrowserActivity$MobclixBrowser$1 r1 = new com.mobclix.android.sdk.MobclixBrowserActivity$MobclixBrowser$1     // Catch:{ Exception -> 0x00f4 }
                r1.<init>()     // Catch:{ Exception -> 0x00f4 }
                r0.setWebViewClient(r1)     // Catch:{ Exception -> 0x00f4 }
                android.webkit.WebView r0 = r11.mWebView     // Catch:{ Exception -> 0x00f4 }
                com.mobclix.android.sdk.MobclixBrowserActivity$MobclixBrowser$2 r1 = new com.mobclix.android.sdk.MobclixBrowserActivity$MobclixBrowser$2     // Catch:{ Exception -> 0x00f4 }
                r1.<init>()     // Catch:{ Exception -> 0x00f4 }
                r0.setWebChromeClient(r1)     // Catch:{ Exception -> 0x00f4 }
                android.webkit.WebView r0 = r11.mWebView     // Catch:{ Exception -> 0x00f4 }
                android.widget.RelativeLayout$LayoutParams r1 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ Exception -> 0x00f4 }
                r2 = -1
                r3 = -1
                r1.<init>(r2, r3)     // Catch:{ Exception -> 0x00f4 }
                r0.setLayoutParams(r1)     // Catch:{ Exception -> 0x00f4 }
                java.lang.String r0 = r11.cachedHTML     // Catch:{ Exception -> 0x00f4 }
                java.lang.String r1 = ""
                boolean r0 = r0.equals(r1)     // Catch:{ Exception -> 0x00f4 }
                if (r0 == 0) goto L_0x010a
                android.webkit.WebView r0 = r11.mWebView     // Catch:{ Exception -> 0x00f4 }
                java.lang.String r1 = r11.url     // Catch:{ Exception -> 0x00f4 }
                r0.loadUrl(r1)     // Catch:{ Exception -> 0x00f4 }
            L_0x0074:
                android.webkit.WebView r0 = r11.mWebView     // Catch:{ Exception -> 0x00f4 }
                r11.addView(r0)     // Catch:{ Exception -> 0x00f4 }
                java.lang.String r0 = r11.browserType     // Catch:{ Exception -> 0x00f4 }
                java.lang.String r1 = "minimal"
                boolean r0 = r0.equals(r1)     // Catch:{ Exception -> 0x00f4 }
                if (r0 == 0) goto L_0x00e3
                android.widget.RelativeLayout$LayoutParams r8 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ Exception -> 0x00f4 }
                r0 = 30
                int r0 = r12.dp(r0)     // Catch:{ Exception -> 0x00f4 }
                r1 = 30
                int r1 = r12.dp(r1)     // Catch:{ Exception -> 0x00f4 }
                r8.<init>(r0, r1)     // Catch:{ Exception -> 0x00f4 }
                r0 = 5
                int r0 = r12.dp(r0)     // Catch:{ Exception -> 0x00f4 }
                r1 = 5
                int r1 = r12.dp(r1)     // Catch:{ Exception -> 0x00f4 }
                r2 = 0
                r3 = 0
                r8.setMargins(r0, r1, r2, r3)     // Catch:{ Exception -> 0x00f4 }
                android.widget.ImageView r7 = new android.widget.ImageView     // Catch:{ Exception -> 0x00f4 }
                r7.<init>(r12)     // Catch:{ Exception -> 0x00f4 }
                r7.setLayoutParams(r8)     // Catch:{ Exception -> 0x00f4 }
                r0 = 17301594(0x108005a, float:2.4979507E-38)
                r7.setImageResource(r0)     // Catch:{ Exception -> 0x00f4 }
                android.graphics.drawable.ShapeDrawable r6 = new android.graphics.drawable.ShapeDrawable     // Catch:{ Exception -> 0x00f4 }
                android.graphics.drawable.shapes.ArcShape r0 = new android.graphics.drawable.shapes.ArcShape     // Catch:{ Exception -> 0x00f4 }
                r1 = 0
                r2 = 1135869952(0x43b40000, float:360.0)
                r0.<init>(r1, r2)     // Catch:{ Exception -> 0x00f4 }
                r6.<init>(r0)     // Catch:{ Exception -> 0x00f4 }
                r0 = -7
                int r0 = r12.dp(r0)     // Catch:{ Exception -> 0x00f4 }
                r1 = -7
                int r1 = r12.dp(r1)     // Catch:{ Exception -> 0x00f4 }
                r2 = -7
                int r2 = r12.dp(r2)     // Catch:{ Exception -> 0x00f4 }
                r3 = -7
                int r3 = r12.dp(r3)     // Catch:{ Exception -> 0x00f4 }
                r6.setPadding(r0, r1, r2, r3)     // Catch:{ Exception -> 0x00f4 }
                r7.setBackgroundDrawable(r6)     // Catch:{ Exception -> 0x00f4 }
                com.mobclix.android.sdk.MobclixBrowserActivity$MobclixBrowser$3 r0 = new com.mobclix.android.sdk.MobclixBrowserActivity$MobclixBrowser$3     // Catch:{ Exception -> 0x00f4 }
                r0.<init>()     // Catch:{ Exception -> 0x00f4 }
                r7.setOnClickListener(r0)     // Catch:{ Exception -> 0x00f4 }
                r11.addView(r7)     // Catch:{ Exception -> 0x00f4 }
            L_0x00e3:
                return
            L_0x00e4:
                r0 = move-exception
                r9 = r0
                java.lang.String r0 = "http://www.mobclix.com"
                r11.url = r0     // Catch:{ JSONException -> 0x00ec }
                goto L_0x001e
            L_0x00ec:
                r0 = move-exception
                r9 = r0
                java.lang.String r0 = "http://www.mobclix.com"
                r11.url = r0     // Catch:{ Exception -> 0x00f4 }
                goto L_0x002e
            L_0x00f4:
                r0 = move-exception
                r9 = r0
                r13.finish()
                goto L_0x00e3
            L_0x00fa:
                r0 = move-exception
                r9 = r0
                java.lang.String r0 = ""
                r11.cachedHTML = r0     // Catch:{ JSONException -> 0x00ec }
                goto L_0x0026
            L_0x0102:
                r0 = move-exception
                r9 = r0
                java.lang.String r0 = "standard"
                r11.browserType = r0     // Catch:{ JSONException -> 0x00ec }
                goto L_0x002e
            L_0x010a:
                android.webkit.WebView r0 = r11.mWebView     // Catch:{ Exception -> 0x00f4 }
                java.lang.String r1 = r11.url     // Catch:{ Exception -> 0x00f4 }
                java.lang.String r2 = r11.cachedHTML     // Catch:{ Exception -> 0x00f4 }
                java.lang.String r3 = "text/html"
                java.lang.String r4 = "utf-8"
                r5 = 0
                r0.loadDataWithBaseURL(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x00f4 }
                goto L_0x0074
            */
            throw new UnsupportedOperationException("Method not decompiled: com.mobclix.android.sdk.MobclixBrowserActivity.MobclixBrowser.<init>(com.mobclix.android.sdk.MobclixBrowserActivity, android.app.Activity, java.lang.String):void");
        }

        /* access modifiers changed from: private */
        public WebView getWebView() {
            return this.mWebView;
        }
    }

    class MobclixFullScreen extends LinearLayout {
        static final int DEFAULT_DURATION = 500;
        Activity activity;
        boolean open = true;
        boolean wasAdActivity = false;
        MobclixWebView webview;
        ViewGroup webviewContainer;
        int windowFlags;

        MobclixFullScreen(Activity a) {
            super(a);
            int orientation;
            try {
                this.activity = a;
                this.webview = Mobclix.getInstance().webview.get();
                this.webview.getJavascriptInterface().expanderActivity = this;
                this.windowFlags = ((Activity) this.webview.getWorkingContext()).getWindow().getAttributes().flags;
                this.activity.getWindowManager().getDefaultDisplay().getMetrics(new DisplayMetrics());
                if (this.activity.getResources().getConfiguration().orientation == 2) {
                    orientation = 0;
                } else {
                    orientation = 1;
                }
                this.activity.setRequestedOrientation(orientation);
                setBackgroundColor(Color.argb(128, 256, 256, 256));
                setOrientation(1);
                setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
                this.webviewContainer = (ViewGroup) this.webview.getParent();
            } catch (Exception e) {
                a.finish();
            }
        }

        public synchronized void exit(int duration) {
        }
    }

    class MobclixFullScreenAd extends MobclixFullScreen {
        MobclixFullScreenAd(Activity a) {
            super(a);
            try {
                ((Activity) this.webview.getWorkingContext()).getWindow().setFlags(1024, 1024);
                this.webviewContainer.removeView(this.webview);
                this.webview.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
                this.webview.jsInterface.expanded = true;
                addView(this.webview);
            } catch (Exception e) {
                a.finish();
            }
        }

        public synchronized void exit(int duration) {
            try {
                if (!(Mobclix.getInstance().webview == null || Mobclix.getInstance().webview.get() == null)) {
                    this.webview.getJavascriptInterface().expanderActivity = null;
                    Mobclix.getInstance().webview = null;
                    this.open = false;
                    Window w = ((Activity) this.webview.getWorkingContext()).getWindow();
                    WindowManager.LayoutParams wlp = w.getAttributes();
                    wlp.flags = this.windowFlags;
                    w.setAttributes(wlp);
                    MobclixBrowserActivity.this.unregisterReceiver(MobclixBrowserActivity.this.screenReceiver);
                    removeAllViews();
                    this.webview.getJavascriptInterface().adWillContract();
                    if (this.webview.fullScreenAdView != null) {
                        Iterator<MobclixFullScreenAdViewListener> it = this.webview.fullScreenAdView.listeners.iterator();
                        while (it.hasNext()) {
                            MobclixFullScreenAdViewListener listener = it.next();
                            if (listener != null) {
                                listener.onDismissAd(this.webview.fullScreenAdView);
                            }
                        }
                        this.webview.destroy();
                        this.webview = null;
                    }
                    this.activity.finish();
                }
            } catch (Exception e) {
                this.activity.finish();
            }
            return;
        }
    }

    class MobclixExpander extends MobclixFullScreen implements Animation.AnimationListener {
        View hSpacer;
        int iHeight = 0;
        int iLeftMargin = 0;
        int iTopMargin = 0;
        int iWidth = 0;
        int statusBarHeight = 0;
        int titleBarHeight = 0;
        View vSpacer;

        /* JADX WARNING: Code restructure failed: missing block: B:30:0x019b, code lost:
            r3 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:31:0x019c, code lost:
            r13 = r3;
            r0.activity.finish();
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Removed duplicated region for block: B:30:0x019b A[Catch:{ Exception -> 0x01a7 }, ExcHandler: JSONException (r3v7 'e' org.json.JSONException A[CUSTOM_DECLARE, Catch:{ Exception -> 0x01a7 }]), PHI: r5 r7 r8 r9 
          PHI: (r5v1 'fLeftMargin' int) = (r5v0 'fLeftMargin' int), (r5v0 'fLeftMargin' int), (r5v0 'fLeftMargin' int), (r5v0 'fLeftMargin' int), (r5v0 'fLeftMargin' int), (r5v0 'fLeftMargin' int), (r5v0 'fLeftMargin' int), (r5v4 'fLeftMargin' int), (r5v4 'fLeftMargin' int), (r5v4 'fLeftMargin' int), (r5v4 'fLeftMargin' int), (r5v4 'fLeftMargin' int), (r5v4 'fLeftMargin' int), (r5v0 'fLeftMargin' int), (r5v0 'fLeftMargin' int), (r5v0 'fLeftMargin' int), (r5v0 'fLeftMargin' int) binds: [B:1:0x0033, B:2:?, B:4:0x0041, B:7:0x004f, B:10:0x005d, B:13:0x006b, B:16:0x0074, B:19:0x007d, B:22:0x0086, B:25:0x008f, B:26:?, B:23:?, B:20:?, B:14:?, B:11:?, B:8:?, B:5:?] A[DONT_GENERATE, DONT_INLINE]
          PHI: (r7v1 'fTopMargin' int) = (r7v0 'fTopMargin' int), (r7v0 'fTopMargin' int), (r7v0 'fTopMargin' int), (r7v0 'fTopMargin' int), (r7v0 'fTopMargin' int), (r7v0 'fTopMargin' int), (r7v4 'fTopMargin' int), (r7v4 'fTopMargin' int), (r7v4 'fTopMargin' int), (r7v4 'fTopMargin' int), (r7v4 'fTopMargin' int), (r7v4 'fTopMargin' int), (r7v4 'fTopMargin' int), (r7v4 'fTopMargin' int), (r7v0 'fTopMargin' int), (r7v0 'fTopMargin' int), (r7v0 'fTopMargin' int) binds: [B:1:0x0033, B:2:?, B:4:0x0041, B:7:0x004f, B:10:0x005d, B:13:0x006b, B:16:0x0074, B:19:0x007d, B:22:0x0086, B:25:0x008f, B:26:?, B:23:?, B:20:?, B:17:?, B:11:?, B:8:?, B:5:?] A[DONT_GENERATE, DONT_INLINE]
          PHI: (r8v1 'fWidth' int) = (r8v0 'fWidth' int), (r8v0 'fWidth' int), (r8v0 'fWidth' int), (r8v0 'fWidth' int), (r8v0 'fWidth' int), (r8v0 'fWidth' int), (r8v0 'fWidth' int), (r8v0 'fWidth' int), (r8v4 'fWidth' int), (r8v4 'fWidth' int), (r8v4 'fWidth' int), (r8v4 'fWidth' int), (r8v0 'fWidth' int), (r8v0 'fWidth' int), (r8v0 'fWidth' int), (r8v0 'fWidth' int), (r8v0 'fWidth' int) binds: [B:1:0x0033, B:2:?, B:4:0x0041, B:7:0x004f, B:10:0x005d, B:13:0x006b, B:16:0x0074, B:19:0x007d, B:22:0x0086, B:25:0x008f, B:26:?, B:23:?, B:17:?, B:14:?, B:11:?, B:8:?, B:5:?] A[DONT_GENERATE, DONT_INLINE]
          PHI: (r9v1 'fHeight' int) = (r9v0 'fHeight' int), (r9v0 'fHeight' int), (r9v0 'fHeight' int), (r9v0 'fHeight' int), (r9v0 'fHeight' int), (r9v0 'fHeight' int), (r9v0 'fHeight' int), (r9v0 'fHeight' int), (r9v0 'fHeight' int), (r9v4 'fHeight' int), (r9v4 'fHeight' int), (r9v0 'fHeight' int), (r9v0 'fHeight' int), (r9v0 'fHeight' int), (r9v0 'fHeight' int), (r9v0 'fHeight' int), (r9v0 'fHeight' int) binds: [B:1:0x0033, B:2:?, B:4:0x0041, B:7:0x004f, B:10:0x005d, B:13:0x006b, B:16:0x0074, B:19:0x007d, B:22:0x0086, B:25:0x008f, B:26:?, B:20:?, B:17:?, B:14:?, B:11:?, B:8:?, B:5:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:1:0x0033] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        MobclixExpander(android.app.Activity r21, java.lang.String r22) {
            /*
                r19 = this;
                r0 = r20
                r1 = r19
                com.mobclix.android.sdk.MobclixBrowserActivity.this = r0
                r19.<init>(r21)
                r3 = 0
                r0 = r3
                r1 = r19
                r1.iTopMargin = r0
                r3 = 0
                r0 = r3
                r1 = r19
                r1.iLeftMargin = r0
                r3 = 0
                r0 = r3
                r1 = r19
                r1.iWidth = r0
                r3 = 0
                r0 = r3
                r1 = r19
                r1.iHeight = r0
                r3 = 0
                r0 = r3
                r1 = r19
                r1.statusBarHeight = r0
                r3 = 0
                r0 = r3
                r1 = r19
                r1.titleBarHeight = r0
                r10 = 500(0x1f4, float:7.0E-43)
                r7 = 0
                r5 = 0
                r8 = 0
                r9 = 0
                org.json.JSONObject r17 = new org.json.JSONObject     // Catch:{ JSONException -> 0x019b }
                r0 = r17
                r1 = r22
                r0.<init>(r1)     // Catch:{ JSONException -> 0x019b }
                java.lang.String r3 = "statusBarHeight"
                r0 = r17
                r1 = r3
                int r3 = r0.getInt(r1)     // Catch:{ Exception -> 0x01c2, JSONException -> 0x019b }
                r0 = r3
                r1 = r19
                r1.statusBarHeight = r0     // Catch:{ Exception -> 0x01c2, JSONException -> 0x019b }
            L_0x004a:
                java.lang.String r3 = "topMargin"
                r0 = r17
                r1 = r3
                int r3 = r0.getInt(r1)     // Catch:{ Exception -> 0x01bf, JSONException -> 0x019b }
                r0 = r3
                r1 = r19
                r1.iTopMargin = r0     // Catch:{ Exception -> 0x01bf, JSONException -> 0x019b }
            L_0x0058:
                java.lang.String r3 = "leftMargin"
                r0 = r17
                r1 = r3
                int r3 = r0.getInt(r1)     // Catch:{ Exception -> 0x01bc, JSONException -> 0x019b }
                r0 = r3
                r1 = r19
                r1.iLeftMargin = r0     // Catch:{ Exception -> 0x01bc, JSONException -> 0x019b }
            L_0x0066:
                java.lang.String r3 = "fTopMargin"
                r0 = r17
                r1 = r3
                int r7 = r0.getInt(r1)     // Catch:{ Exception -> 0x01b9, JSONException -> 0x019b }
            L_0x006f:
                java.lang.String r3 = "fLeftMargin"
                r0 = r17
                r1 = r3
                int r5 = r0.getInt(r1)     // Catch:{ Exception -> 0x01b6, JSONException -> 0x019b }
            L_0x0078:
                java.lang.String r3 = "fWidth"
                r0 = r17
                r1 = r3
                int r8 = r0.getInt(r1)     // Catch:{ Exception -> 0x01b3, JSONException -> 0x019b }
            L_0x0081:
                java.lang.String r3 = "fHeight"
                r0 = r17
                r1 = r3
                int r9 = r0.getInt(r1)     // Catch:{ Exception -> 0x01b0, JSONException -> 0x019b }
            L_0x008a:
                java.lang.String r3 = "duration"
                r0 = r17
                r1 = r3
                int r10 = r0.getInt(r1)     // Catch:{ Exception -> 0x01ad, JSONException -> 0x019b }
            L_0x0093:
                r0 = r19
                int r0 = r0.iTopMargin     // Catch:{ Exception -> 0x01a7 }
                r3 = r0
                r0 = r19
                int r0 = r0.statusBarHeight     // Catch:{ Exception -> 0x01a7 }
                r4 = r0
                int r3 = r3 - r4
                r0 = r3
                r1 = r19
                r1.iTopMargin = r0     // Catch:{ Exception -> 0x01a7 }
                com.mobclix.android.sdk.MobclixBrowserActivity$MobclixExpander$1 r3 = new com.mobclix.android.sdk.MobclixBrowserActivity$MobclixExpander$1     // Catch:{ Exception -> 0x01a7 }
                r0 = r3
                r1 = r19
                r0.<init>()     // Catch:{ Exception -> 0x01a7 }
                r0 = r19
                r1 = r3
                r0.setOnClickListener(r1)     // Catch:{ Exception -> 0x01a7 }
                android.widget.LinearLayout$LayoutParams r18 = new android.widget.LinearLayout$LayoutParams     // Catch:{ Exception -> 0x01a7 }
                r3 = 1
                r0 = r19
                int r0 = r0.iTopMargin     // Catch:{ Exception -> 0x01a7 }
                r4 = r0
                r0 = r18
                r1 = r3
                r2 = r4
                r0.<init>(r1, r2)     // Catch:{ Exception -> 0x01a7 }
                android.view.View r3 = new android.view.View     // Catch:{ Exception -> 0x01a7 }
                r0 = r19
                android.app.Activity r0 = r0.activity     // Catch:{ Exception -> 0x01a7 }
                r4 = r0
                r3.<init>(r4)     // Catch:{ Exception -> 0x01a7 }
                r0 = r3
                r1 = r19
                r1.vSpacer = r0     // Catch:{ Exception -> 0x01a7 }
                r0 = r19
                android.view.View r0 = r0.vSpacer     // Catch:{ Exception -> 0x01a7 }
                r3 = r0
                r0 = r3
                r1 = r18
                r0.setLayoutParams(r1)     // Catch:{ Exception -> 0x01a7 }
                android.widget.LinearLayout$LayoutParams r16 = new android.widget.LinearLayout$LayoutParams     // Catch:{ Exception -> 0x01a7 }
                r3 = -2
                r4 = -2
                r0 = r16
                r1 = r3
                r2 = r4
                r0.<init>(r1, r2)     // Catch:{ Exception -> 0x01a7 }
                android.widget.LinearLayout r14 = new android.widget.LinearLayout     // Catch:{ Exception -> 0x01a7 }
                r0 = r19
                android.app.Activity r0 = r0.activity     // Catch:{ Exception -> 0x01a7 }
                r3 = r0
                r14.<init>(r3)     // Catch:{ Exception -> 0x01a7 }
                r0 = r14
                r1 = r16
                r0.setLayoutParams(r1)     // Catch:{ Exception -> 0x01a7 }
                android.widget.LinearLayout$LayoutParams r15 = new android.widget.LinearLayout$LayoutParams     // Catch:{ Exception -> 0x01a7 }
                r0 = r19
                int r0 = r0.iLeftMargin     // Catch:{ Exception -> 0x01a7 }
                r3 = r0
                r4 = 1
                r15.<init>(r3, r4)     // Catch:{ Exception -> 0x01a7 }
                android.view.View r3 = new android.view.View     // Catch:{ Exception -> 0x01a7 }
                r0 = r19
                android.app.Activity r0 = r0.activity     // Catch:{ Exception -> 0x01a7 }
                r4 = r0
                r3.<init>(r4)     // Catch:{ Exception -> 0x01a7 }
                r0 = r3
                r1 = r19
                r1.hSpacer = r0     // Catch:{ Exception -> 0x01a7 }
                r0 = r19
                android.view.View r0 = r0.hSpacer     // Catch:{ Exception -> 0x01a7 }
                r3 = r0
                r3.setLayoutParams(r15)     // Catch:{ Exception -> 0x01a7 }
                r0 = r19
                com.mobclix.android.sdk.MobclixWebView r0 = r0.webview     // Catch:{ Exception -> 0x01a7 }
                r3 = r0
                android.view.ViewParent r20 = r3.getParent()     // Catch:{ Exception -> 0x01a7 }
                android.view.ViewGroup r20 = (android.view.ViewGroup) r20     // Catch:{ Exception -> 0x01a7 }
                r0 = r20
                r1 = r19
                r1.webviewContainer = r0     // Catch:{ Exception -> 0x01a7 }
                r0 = r19
                android.view.ViewGroup r0 = r0.webviewContainer     // Catch:{ Exception -> 0x01a7 }
                r3 = r0
                r0 = r19
                com.mobclix.android.sdk.MobclixWebView r0 = r0.webview     // Catch:{ Exception -> 0x01a7 }
                r4 = r0
                r3.removeView(r4)     // Catch:{ Exception -> 0x01a7 }
                r0 = r19
                com.mobclix.android.sdk.MobclixWebView r0 = r0.webview     // Catch:{ Exception -> 0x01a7 }
                r3 = r0
                int r3 = r3.getWidth()     // Catch:{ Exception -> 0x01a7 }
                r0 = r3
                r1 = r19
                r1.iWidth = r0     // Catch:{ Exception -> 0x01a7 }
                r0 = r19
                com.mobclix.android.sdk.MobclixWebView r0 = r0.webview     // Catch:{ Exception -> 0x01a7 }
                r3 = r0
                int r3 = r3.getHeight()     // Catch:{ Exception -> 0x01a7 }
                r0 = r3
                r1 = r19
                r1.iHeight = r0     // Catch:{ Exception -> 0x01a7 }
                android.widget.LinearLayout$LayoutParams r12 = new android.widget.LinearLayout$LayoutParams     // Catch:{ Exception -> 0x01a7 }
                r0 = r19
                int r0 = r0.iWidth     // Catch:{ Exception -> 0x01a7 }
                r3 = r0
                r0 = r19
                int r0 = r0.iHeight     // Catch:{ Exception -> 0x01a7 }
                r4 = r0
                r12.<init>(r3, r4)     // Catch:{ Exception -> 0x01a7 }
                r0 = r19
                com.mobclix.android.sdk.MobclixWebView r0 = r0.webview     // Catch:{ Exception -> 0x01a7 }
                r3 = r0
                r3.setLayoutParams(r12)     // Catch:{ Exception -> 0x01a7 }
                r0 = r19
                android.view.View r0 = r0.vSpacer     // Catch:{ Exception -> 0x01a7 }
                r3 = r0
                r0 = r19
                r1 = r3
                r0.addView(r1)     // Catch:{ Exception -> 0x01a7 }
                r0 = r19
                r1 = r14
                r0.addView(r1)     // Catch:{ Exception -> 0x01a7 }
                r0 = r19
                android.view.View r0 = r0.hSpacer     // Catch:{ Exception -> 0x01a7 }
                r3 = r0
                r14.addView(r3)     // Catch:{ Exception -> 0x01a7 }
                r0 = r19
                com.mobclix.android.sdk.MobclixWebView r0 = r0.webview     // Catch:{ Exception -> 0x01a7 }
                r3 = r0
                r14.addView(r3)     // Catch:{ Exception -> 0x01a7 }
                r0 = r19
                int r0 = r0.iLeftMargin     // Catch:{ Exception -> 0x01a7 }
                r4 = r0
                r0 = r19
                int r0 = r0.iTopMargin     // Catch:{ Exception -> 0x01a7 }
                r6 = r0
                r3 = r19
                r11 = r19
                r3.expand(r4, r5, r6, r7, r8, r9, r10, r11)     // Catch:{ Exception -> 0x01a7 }
            L_0x019a:
                return
            L_0x019b:
                r3 = move-exception
                r13 = r3
                r0 = r19
                android.app.Activity r0 = r0.activity     // Catch:{ Exception -> 0x01a7 }
                r3 = r0
                r3.finish()     // Catch:{ Exception -> 0x01a7 }
                goto L_0x0093
            L_0x01a7:
                r3 = move-exception
                r13 = r3
                r21.finish()
                goto L_0x019a
            L_0x01ad:
                r3 = move-exception
                goto L_0x0093
            L_0x01b0:
                r3 = move-exception
                goto L_0x008a
            L_0x01b3:
                r3 = move-exception
                goto L_0x0081
            L_0x01b6:
                r3 = move-exception
                goto L_0x0078
            L_0x01b9:
                r3 = move-exception
                goto L_0x006f
            L_0x01bc:
                r3 = move-exception
                goto L_0x0066
            L_0x01bf:
                r3 = move-exception
                goto L_0x0058
            L_0x01c2:
                r3 = move-exception
                goto L_0x004a
            */
            throw new UnsupportedOperationException("Method not decompiled: com.mobclix.android.sdk.MobclixBrowserActivity.MobclixExpander.<init>(com.mobclix.android.sdk.MobclixBrowserActivity, android.app.Activity, java.lang.String):void");
        }

        public synchronized void exit(int duration) {
            try {
                if (!(Mobclix.getInstance().webview == null || Mobclix.getInstance().webview.get() == null)) {
                    this.webview.getJavascriptInterface().expanderActivity = null;
                    Mobclix.getInstance().webview = null;
                    this.open = false;
                    Window w = ((Activity) this.webview.getWorkingContext()).getWindow();
                    WindowManager.LayoutParams wlp = w.getAttributes();
                    wlp.flags = this.windowFlags;
                    w.setAttributes(wlp);
                    this.webview.getJavascriptInterface().adWillContract();
                    expand(this.iLeftMargin, this.iTopMargin, this.iWidth, this.iHeight, duration, this);
                }
            } catch (Exception e) {
                this.activity.finish();
            }
            return;
        }

        public void expand(int fLeftMargin, int fTopMargin, int width, int height, int duration, Animation.AnimationListener l) {
            expand(this.hSpacer.getWidth(), fLeftMargin, this.vSpacer.getHeight(), fTopMargin, width, height, duration, l);
        }

        public void expand(int leftMargin, int fLeftMargin, int topMargin, int fTopMargin, int width, int height, int duration, Animation.AnimationListener l) {
            if (duration < 0) {
                duration = 0;
            }
            if (duration > 3000) {
                duration = 3000;
            }
            try {
                if (this.open) {
                    if (topMargin < 0) {
                        topMargin = 0;
                    }
                    if (leftMargin < 0) {
                        leftMargin = 0;
                    }
                    if (fTopMargin < 0) {
                        fTopMargin = 0;
                    }
                    if (fLeftMargin < 0) {
                        fLeftMargin = 0;
                    }
                    if (width < 0) {
                        width = 1;
                    }
                    if (height < 0) {
                        height = 1;
                    }
                    DisplayMetrics dm = new DisplayMetrics();
                    MobclixBrowserActivity.this.getWindowManager().getDefaultDisplay().getMetrics(dm);
                    int screenWidth = dm.widthPixels;
                    int screenHeight = dm.heightPixels;
                    if (width > screenWidth) {
                        width = screenWidth;
                        fLeftMargin = 0;
                    } else if (width + fLeftMargin > screenWidth) {
                        fLeftMargin = screenWidth - width;
                    }
                    if (height > screenHeight) {
                        height = screenHeight;
                        fTopMargin = 0;
                    } else if (height + fTopMargin > screenHeight) {
                        fTopMargin = screenHeight - height;
                    }
                }
                Animation vExpand = new ExpandAnimation(this.vSpacer, 1.0f, 1.0f, (float) topMargin, (float) fTopMargin);
                vExpand.setDuration((long) duration);
                Animation hExpand = new ExpandAnimation(this.hSpacer, (float) leftMargin, (float) fLeftMargin, 1.0f, 1.0f);
                hExpand.setDuration((long) duration);
                Animation webviewExpand = new ExpandAnimation(this.webview, (float) this.webview.getWidth(), (float) width, (float) this.webview.getHeight(), (float) height);
                webviewExpand.setDuration((long) duration);
                webviewExpand.setAnimationListener(this);
                this.vSpacer.startAnimation(vExpand);
                this.hSpacer.startAnimation(hExpand);
                this.webview.startAnimation(webviewExpand);
            } catch (Exception e) {
                this.activity.finish();
            }
        }

        public void onAnimationEnd(Animation animation) {
            try {
                if (!this.open) {
                    this.webview.jsInterface.expanded = false;
                    ((ViewGroup) this.webview.getParent()).removeView(this.webview);
                    this.webviewContainer.addView(this.webview);
                    MobclixBrowserActivity.this.unregisterReceiver(MobclixBrowserActivity.this.screenReceiver);
                    this.activity.finish();
                    return;
                }
                if (this.vSpacer.getHeight() <= this.statusBarHeight) {
                    ((Activity) this.webview.getWorkingContext()).getWindow().setFlags(1024, 1024);
                }
                this.webview.getJavascriptInterface().resumeListeners();
                this.webview.getJavascriptInterface().adFinishedResizing();
            } catch (Exception e) {
                this.activity.finish();
            }
        }

        public void onAnimationRepeat(Animation animation) {
        }

        public void onAnimationStart(Animation animation) {
        }
    }

    class ExpandAnimation extends Animation {
        float dHeight;
        float dWidth;
        float height;
        View view;
        float width;

        ExpandAnimation(View v, float startWidth, float finalWidth, float startHeight, float finalHeight) {
            this.view = v;
            this.height = startHeight;
            this.dHeight = finalHeight - this.height;
            this.width = startWidth;
            this.dWidth = finalWidth - this.width;
        }

        /* access modifiers changed from: protected */
        public void applyTransformation(float interpolatedTime, Transformation t) {
            super.applyTransformation(interpolatedTime, t);
            ViewGroup.LayoutParams lp = this.view.getLayoutParams();
            lp.height = (int) (this.height + (this.dHeight * interpolatedTime));
            lp.width = (int) (this.width + (this.dWidth * interpolatedTime));
            this.view.setLayoutParams(lp);
        }
    }

    public class ScreenReceiver extends BroadcastReceiver {
        public boolean isScreenOn = false;

        public ScreenReceiver() {
        }

        /* Debug info: failed to restart local var, previous not found, register: 2 */
        public void onReceive(Context context, Intent intent) {
            try {
                if (intent.getAction().equals("android.intent.action.SCREEN_OFF")) {
                    ((MobclixFullScreen) MobclixBrowserActivity.this.view).webview.getJavascriptInterface().adWillBecomeHidden();
                } else {
                    intent.getAction().equals("android.intent.action.SCREEN_ON");
                }
            } catch (Exception e) {
            }
        }
    }
}
