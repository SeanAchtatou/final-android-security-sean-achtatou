package com.maxdroid.spanish;

public final class R {

    public static final class attr {
        public static final int backgroundColor = 2130771968;
        public static final int keywords = 2130771970;
        public static final int refreshInterval = 2130771971;
        public static final int textColor = 2130771969;
    }

    public static final class drawable {
        public static final int background = 2130837504;
        public static final int button_background_focus = 2130837505;
        public static final int button_background_normal = 2130837506;
        public static final int button_background_pressed = 2130837507;
        public static final int header = 2130837508;
        public static final int icon = 2130837509;
        public static final int mybutton = 2130837510;
    }

    public static final class id {
        public static final int ad1 = 2131099649;
        public static final int banner_adview = 2131099708;
        public static final int button1 = 2131099651;
        public static final int button10 = 2131099660;
        public static final int button11 = 2131099661;
        public static final int button12 = 2131099662;
        public static final int button13 = 2131099663;
        public static final int button15 = 2131099665;
        public static final int button16 = 2131099701;
        public static final int button17 = 2131099666;
        public static final int button18 = 2131099667;
        public static final int button19 = 2131099668;
        public static final int button2 = 2131099652;
        public static final int button20 = 2131099669;
        public static final int button21 = 2131099670;
        public static final int button22 = 2131099671;
        public static final int button23 = 2131099672;
        public static final int button24 = 2131099673;
        public static final int button25 = 2131099690;
        public static final int button26 = 2131099674;
        public static final int button27 = 2131099675;
        public static final int button28 = 2131099676;
        public static final int button29 = 2131099687;
        public static final int button3 = 2131099653;
        public static final int button30 = 2131099688;
        public static final int button31 = 2131099689;
        public static final int button32 = 2131099691;
        public static final int button33 = 2131099692;
        public static final int button34 = 2131099693;
        public static final int button35 = 2131099694;
        public static final int button36 = 2131099695;
        public static final int button37 = 2131099696;
        public static final int button38 = 2131099697;
        public static final int button39 = 2131099698;
        public static final int button4 = 2131099654;
        public static final int button40 = 2131099678;
        public static final int button41 = 2131099679;
        public static final int button42 = 2131099680;
        public static final int button43 = 2131099700;
        public static final int button44 = 2131099702;
        public static final int button45 = 2131099703;
        public static final int button46 = 2131099681;
        public static final int button47 = 2131099682;
        public static final int button48 = 2131099683;
        public static final int button49 = 2131099684;
        public static final int button5 = 2131099655;
        public static final int button50 = 2131099685;
        public static final int button51 = 2131099686;
        public static final int button53 = 2131099704;
        public static final int button54 = 2131099705;
        public static final int button55 = 2131099706;
        public static final int button56 = 2131099707;
        public static final int button6 = 2131099656;
        public static final int button7 = 2131099657;
        public static final int button8 = 2131099658;
        public static final int button9 = 2131099659;
        public static final int cancelbutton = 2131099711;
        public static final int savebutton = 2131099710;
        public static final int savedialog = 2131099709;
        public static final int textview1 = 2131099650;
        public static final int textview2 = 2131099664;
        public static final int textview3 = 2131099677;
        public static final int textview4 = 2131099699;
        public static final int widget30 = 2131099648;
    }

    public static final class layout {
        public static final int main = 2130903040;
        public static final int savedialog = 2130903041;
    }

    public static final class raw {
        public static final int sound1 = 2130968576;
        public static final int sound10 = 2130968577;
        public static final int sound11 = 2130968578;
        public static final int sound12 = 2130968579;
        public static final int sound13 = 2130968580;
        public static final int sound15 = 2130968581;
        public static final int sound16 = 2130968582;
        public static final int sound17 = 2130968583;
        public static final int sound18 = 2130968584;
        public static final int sound19 = 2130968585;
        public static final int sound2 = 2130968586;
        public static final int sound20 = 2130968587;
        public static final int sound21 = 2130968588;
        public static final int sound22 = 2130968589;
        public static final int sound23 = 2130968590;
        public static final int sound24 = 2130968591;
        public static final int sound25 = 2130968592;
        public static final int sound26 = 2130968593;
        public static final int sound27 = 2130968594;
        public static final int sound28 = 2130968595;
        public static final int sound29 = 2130968596;
        public static final int sound3 = 2130968597;
        public static final int sound30 = 2130968598;
        public static final int sound31 = 2130968599;
        public static final int sound32 = 2130968600;
        public static final int sound33 = 2130968601;
        public static final int sound34 = 2130968602;
        public static final int sound35 = 2130968603;
        public static final int sound36 = 2130968604;
        public static final int sound37 = 2130968605;
        public static final int sound38 = 2130968606;
        public static final int sound39 = 2130968607;
        public static final int sound4 = 2130968608;
        public static final int sound40 = 2130968609;
        public static final int sound41 = 2130968610;
        public static final int sound42 = 2130968611;
        public static final int sound43 = 2130968612;
        public static final int sound44 = 2130968613;
        public static final int sound45 = 2130968614;
        public static final int sound46 = 2130968615;
        public static final int sound47 = 2130968616;
        public static final int sound48 = 2130968617;
        public static final int sound49 = 2130968618;
        public static final int sound5 = 2130968619;
        public static final int sound50 = 2130968620;
        public static final int sound51 = 2130968621;
        public static final int sound53 = 2130968622;
        public static final int sound54 = 2130968623;
        public static final int sound55 = 2130968624;
        public static final int sound56 = 2130968625;
        public static final int sound6 = 2130968626;
        public static final int sound7 = 2130968627;
        public static final int sound8 = 2130968628;
        public static final int sound9 = 2130968629;
    }

    public static final class string {
        public static final int app_name = 2131034113;
        public static final int button10_label = 2131034124;
        public static final int button11_label = 2131034125;
        public static final int button12_label = 2131034126;
        public static final int button13_label = 2131034127;
        public static final int button15_label = 2131034128;
        public static final int button16_label = 2131034129;
        public static final int button17_label = 2131034130;
        public static final int button18_label = 2131034131;
        public static final int button19_label = 2131034132;
        public static final int button1_label = 2131034115;
        public static final int button20_label = 2131034133;
        public static final int button21_label = 2131034134;
        public static final int button22_label = 2131034135;
        public static final int button23_label = 2131034136;
        public static final int button24_label = 2131034137;
        public static final int button25_label = 2131034138;
        public static final int button26_label = 2131034139;
        public static final int button27_label = 2131034140;
        public static final int button28_label = 2131034141;
        public static final int button29_label = 2131034142;
        public static final int button2_label = 2131034116;
        public static final int button30_label = 2131034143;
        public static final int button31_label = 2131034144;
        public static final int button32_label = 2131034145;
        public static final int button33_label = 2131034146;
        public static final int button34_label = 2131034147;
        public static final int button35_label = 2131034148;
        public static final int button36_label = 2131034149;
        public static final int button37_label = 2131034150;
        public static final int button38_label = 2131034151;
        public static final int button39_label = 2131034152;
        public static final int button3_label = 2131034117;
        public static final int button40_label = 2131034153;
        public static final int button41_label = 2131034154;
        public static final int button42_label = 2131034155;
        public static final int button43_label = 2131034156;
        public static final int button44_label = 2131034157;
        public static final int button45_label = 2131034158;
        public static final int button46_label = 2131034159;
        public static final int button47_label = 2131034160;
        public static final int button48_label = 2131034161;
        public static final int button49_label = 2131034162;
        public static final int button4_label = 2131034118;
        public static final int button50_label = 2131034163;
        public static final int button51_label = 2131034164;
        public static final int button53_label = 2131034165;
        public static final int button54_label = 2131034166;
        public static final int button55_label = 2131034167;
        public static final int button56_label = 2131034168;
        public static final int button5_label = 2131034119;
        public static final int button6_label = 2131034120;
        public static final int button7_label = 2131034121;
        public static final int button8_label = 2131034122;
        public static final int button9_label = 2131034123;
        public static final int hello = 2131034112;
        public static final int main_title = 2131034114;
    }

    public static final class styleable {
        public static final int[] com_admob_android_ads_AdView = {R.attr.backgroundColor, R.attr.textColor, R.attr.keywords, R.attr.refreshInterval};
        public static final int com_admob_android_ads_AdView_backgroundColor = 0;
        public static final int com_admob_android_ads_AdView_keywords = 2;
        public static final int com_admob_android_ads_AdView_refreshInterval = 3;
        public static final int com_admob_android_ads_AdView_textColor = 1;
    }
}
