package com.maxdroid.spanish;

import android.app.TabActivity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TabHost;
import com.pontiflex.mobile.webview.sdk.AdManagerFactory;
import com.pontiflex.mobile.webview.sdk.IAdManager;

public class spanish extends TabActivity {
    MediaPlayer mp;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        setVolumeControlStream(3);
        TabHost mTabHost = getTabHost();
        mTabHost.addTab(mTabHost.newTabSpec("tab_test1").setIndicator("Courtesy").setContent((int) R.id.textview1));
        mTabHost.addTab(mTabHost.newTabSpec("tab_test2").setIndicator("Daily Phrases").setContent((int) R.id.textview2));
        mTabHost.addTab(mTabHost.newTabSpec("tab_test3").setIndicator("Espressions").setContent((int) R.id.textview3));
        mTabHost.addTab(mTabHost.newTabSpec("tab_test4").setIndicator("Greetings").setContent((int) R.id.textview4));
        mTabHost.setCurrentTab(0);
        ((Button) findViewById(R.id.button1)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                spanish.this.playSound(R.raw.sound1);
            }
        });
        ((Button) findViewById(R.id.button2)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                spanish.this.playSound(R.raw.sound2);
            }
        });
        ((Button) findViewById(R.id.button3)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                spanish.this.playSound(R.raw.sound3);
            }
        });
        ((Button) findViewById(R.id.button4)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                spanish.this.playSound(R.raw.sound4);
            }
        });
        ((Button) findViewById(R.id.button5)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                spanish.this.playSound(R.raw.sound5);
            }
        });
        ((Button) findViewById(R.id.button6)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                spanish.this.playSound(R.raw.sound6);
            }
        });
        ((Button) findViewById(R.id.button7)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                spanish.this.playSound(R.raw.sound7);
            }
        });
        ((Button) findViewById(R.id.button8)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                spanish.this.playSound(R.raw.sound8);
            }
        });
        ((Button) findViewById(R.id.button9)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                spanish.this.playSound(R.raw.sound9);
            }
        });
        ((Button) findViewById(R.id.button10)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                spanish.this.playSound(R.raw.sound10);
            }
        });
        ((Button) findViewById(R.id.button11)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                spanish.this.playSound(R.raw.sound11);
            }
        });
        ((Button) findViewById(R.id.button12)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                spanish.this.playSound(R.raw.sound12);
            }
        });
        ((Button) findViewById(R.id.button13)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                spanish.this.playSound(R.raw.sound13);
            }
        });
        ((Button) findViewById(R.id.button15)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                spanish.this.playSound(R.raw.sound15);
            }
        });
        ((Button) findViewById(R.id.button16)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                spanish.this.playSound(R.raw.sound16);
            }
        });
        ((Button) findViewById(R.id.button17)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                spanish.this.playSound(R.raw.sound17);
            }
        });
        ((Button) findViewById(R.id.button18)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                spanish.this.playSound(R.raw.sound18);
            }
        });
        ((Button) findViewById(R.id.button19)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                spanish.this.playSound(R.raw.sound19);
            }
        });
        ((Button) findViewById(R.id.button20)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                spanish.this.playSound(R.raw.sound20);
            }
        });
        ((Button) findViewById(R.id.button21)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                spanish.this.playSound(R.raw.sound21);
            }
        });
        ((Button) findViewById(R.id.button22)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                spanish.this.playSound(R.raw.sound22);
            }
        });
        ((Button) findViewById(R.id.button23)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                spanish.this.playSound(R.raw.sound23);
            }
        });
        ((Button) findViewById(R.id.button24)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                spanish.this.playSound(R.raw.sound24);
            }
        });
        ((Button) findViewById(R.id.button25)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                spanish.this.playSound(R.raw.sound25);
            }
        });
        ((Button) findViewById(R.id.button26)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                spanish.this.playSound(R.raw.sound26);
            }
        });
        ((Button) findViewById(R.id.button27)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                spanish.this.playSound(R.raw.sound27);
            }
        });
        ((Button) findViewById(R.id.button28)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                spanish.this.playSound(R.raw.sound28);
            }
        });
        ((Button) findViewById(R.id.button29)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                spanish.this.playSound(R.raw.sound29);
            }
        });
        ((Button) findViewById(R.id.button30)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                spanish.this.playSound(R.raw.sound30);
            }
        });
        ((Button) findViewById(R.id.button31)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                spanish.this.playSound(R.raw.sound31);
            }
        });
        ((Button) findViewById(R.id.button32)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                spanish.this.playSound(R.raw.sound32);
            }
        });
        ((Button) findViewById(R.id.button33)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                spanish.this.playSound(R.raw.sound33);
            }
        });
        ((Button) findViewById(R.id.button34)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                spanish.this.playSound(R.raw.sound34);
            }
        });
        ((Button) findViewById(R.id.button35)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                spanish.this.playSound(R.raw.sound35);
            }
        });
        ((Button) findViewById(R.id.button36)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                spanish.this.playSound(R.raw.sound36);
            }
        });
        ((Button) findViewById(R.id.button37)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                spanish.this.playSound(R.raw.sound37);
            }
        });
        ((Button) findViewById(R.id.button38)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                spanish.this.playSound(R.raw.sound38);
            }
        });
        ((Button) findViewById(R.id.button39)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                spanish.this.playSound(R.raw.sound39);
            }
        });
        ((Button) findViewById(R.id.button40)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                spanish.this.playSound(R.raw.sound40);
            }
        });
        ((Button) findViewById(R.id.button41)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                spanish.this.playSound(R.raw.sound41);
            }
        });
        ((Button) findViewById(R.id.button42)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                spanish.this.playSound(R.raw.sound42);
            }
        });
        ((Button) findViewById(R.id.button43)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                spanish.this.playSound(R.raw.sound43);
            }
        });
        ((Button) findViewById(R.id.button44)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                spanish.this.playSound(R.raw.sound44);
            }
        });
        ((Button) findViewById(R.id.button45)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                spanish.this.playSound(R.raw.sound45);
            }
        });
        ((Button) findViewById(R.id.button46)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                spanish.this.playSound(R.raw.sound46);
            }
        });
        ((Button) findViewById(R.id.button47)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                spanish.this.playSound(R.raw.sound47);
            }
        });
        ((Button) findViewById(R.id.button48)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                spanish.this.playSound(R.raw.sound48);
            }
        });
        ((Button) findViewById(R.id.button49)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                spanish.this.playSound(R.raw.sound49);
            }
        });
        ((Button) findViewById(R.id.button50)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                spanish.this.playSound(R.raw.sound50);
            }
        });
        ((Button) findViewById(R.id.button51)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                spanish.this.playSound(R.raw.sound51);
            }
        });
        ((Button) findViewById(R.id.button53)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                spanish.this.playSound(R.raw.sound53);
            }
        });
        ((Button) findViewById(R.id.button54)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                spanish.this.playSound(R.raw.sound54);
            }
        });
        ((Button) findViewById(R.id.button55)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                spanish.this.playSound(R.raw.sound55);
            }
        });
        ((Button) findViewById(R.id.button56)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                spanish.this.playSound(R.raw.sound56);
            }
        });
        IAdManager adManager = AdManagerFactory.createInstance(getApplication());
        if (adManager.hasValidRegistrationData()) {
            adManager.startMultiOfferActivity();
        } else {
            adManager.startRegistrationActivity();
        }
    }

    /* access modifiers changed from: private */
    public void playSound(int resid) {
        this.mp = MediaPlayer.create(this, resid);
        this.mp.start();
        this.mp.setVolume(1.0f, 1.0f);
        this.mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                mp.release();
            }
        });
    }
}
