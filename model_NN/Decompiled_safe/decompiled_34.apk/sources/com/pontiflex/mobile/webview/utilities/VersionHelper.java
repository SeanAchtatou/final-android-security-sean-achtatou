package com.pontiflex.mobile.webview.utilities;

import android.app.Application;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Log;
import com.pontiflex.mobile.webview.sdk.AdManager;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class VersionHelper {
    private static VersionHelper instance;
    private Properties stringCache;

    public static VersionHelper getInstance(Context context) {
        if (instance == null) {
            instance = createInstance(context);
        }
        if (instance != null) {
            return instance;
        }
        throw new IllegalStateException();
    }

    public static VersionHelper createInstance(Context context) {
        if (instance == null) {
            instance = new VersionHelper(context);
        }
        return instance;
    }

    private VersionHelper(Context context) {
    }

    private String getFromStringCache(Context context, String key) {
        if (this.stringCache == null) {
            this.stringCache = PackageHelper.getInstance(context).loadPontiflexSdkVersion(context);
        }
        if (this.stringCache != null) {
            return this.stringCache.getProperty(key);
        }
        return null;
    }

    public String getPflxAdVersion(Context context) {
        if (context == null) {
            throw new IllegalArgumentException();
        }
        String result = getFromStringCache(context, "PFLEX_AD_VERSION");
        return result == null ? "" : result;
    }

    public String getPflxAdSdkVersion(Context context) {
        if (context == null) {
            throw new IllegalArgumentException();
        }
        return String.format("Android %s Sdk %s", getAndroidOsVersion(), getPflxSdkVersionName(context));
    }

    public String getPflxSdkVersionCode(Context context) {
        if (context == null) {
            throw new IllegalArgumentException();
        }
        String result = getFromStringCache(context, "PFLEX_SDK_VERSION_CODE");
        return result == null ? "" : result;
    }

    public String getPflxSdkVersionName(Context context) {
        if (context != null) {
            return getFromStringCache(context, "PFLEX_SDK_VERSION_NAME");
        }
        throw new IllegalArgumentException();
    }

    public String getAppName(Context context) {
        if (context == null) {
            throw new IllegalArgumentException();
        }
        CharSequence result = null;
        try {
            result = context.getPackageManager().getApplicationLabel(context.getPackageManager().getApplicationInfo(context.getPackageName(), 0));
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("Pontiflex SDK", "Not able to found application name", e);
        }
        if (result == null) {
            return null;
        }
        return result.toString();
    }

    public int getAppVersionCode(Context context) {
        if (context == null) {
            throw new IllegalArgumentException();
        }
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("Pontiflex SDK", "Application Version Name not found", e);
            return -1;
        }
    }

    public String getAppVersionName(Context context) {
        if (context == null) {
            throw new IllegalArgumentException();
        }
        String result = null;
        try {
            result = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("Pontiflex SDK", "Application Version Name not found", e);
        }
        return urlEncode(result);
    }

    public String getAppUri(Context context) {
        if (context == null) {
            throw new IllegalArgumentException();
        }
        String appName = getAppName(context);
        Object[] objArr = new Object[3];
        objArr[0] = appName == null ? null : urlEncode(appName);
        objArr[1] = Integer.valueOf(getAppVersionCode(context));
        objArr[2] = getAppVersionName(context);
        return String.format("app://%s/%s/%s", objArr);
    }

    public String getUserAgent(Context context) {
        return String.format("os=Android %s; device=%s; carrier=%s; language=%s", getAndroidOsVersion(), getDeviceModel(), getDeviceCarrier(), getLocale(context));
    }

    public String getAndroidOsVersion() {
        return urlEncode(Build.VERSION.RELEASE);
    }

    public String getAndroidOsSdkVersionCode() {
        Class<Build.VERSION> cls = Build.VERSION.class;
        int sdkInt = -1;
        try {
            sdkInt = Build.VERSION.class.getField("SDK_INT").getInt(Build.VERSION.class);
        } catch (NoSuchFieldException e) {
            Log.e("Pontiflex SDK", "Current version of android doesn't support android.os.Build.VERSION.SDK_INT ", e);
        } catch (IllegalAccessException e2) {
            Log.e("Pontiflex SDK", "Current version of android doesn't support android.os.Build.VERSION.SDK_INT ", e2);
        }
        if (sdkInt != -1) {
            return Integer.toString(sdkInt);
        }
        return Build.VERSION.SDK;
    }

    public String getDeviceModel() {
        return Build.MODEL != null ? urlEncode(Build.MODEL) : "";
    }

    public String getDeviceCarrier() {
        return Build.BRAND != null ? urlEncode(Build.BRAND) : "";
    }

    public String getLocale(Context context) {
        if (context != null) {
            return context.getResources().getConfiguration().locale.toString();
        }
        throw new IllegalArgumentException("context is null");
    }

    public String getPostalCode(Context context) {
        String countryCodeFieldName;
        String postalCode = null;
        String country = null;
        AdManager adMgr = AdManager.getAdManagerInstance((Application) context.getApplicationContext());
        if (!(!adMgr.hasValidRegistrationData() || (postalCode = adMgr.getRegistrationData(adMgr.getAppInfo().getPostalCodeFieldName())) == null || (countryCodeFieldName = adMgr.getAppInfo().getCountryFieldName()) == null)) {
            country = adMgr.getRegistrationData(countryCodeFieldName);
        }
        if (postalCode == null) {
            return null;
        }
        Object[] objArr = new Object[2];
        objArr[0] = postalCode;
        objArr[1] = country == null ? "US" : country;
        return String.format("zip=%s;country=%s", objArr);
    }

    private String urlEncode(String str) {
        if (str == null) {
            return str;
        }
        try {
            return URLEncoder.encode(str, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            Log.d("Pontiflex SDK", "UTF-8 encoding not supported on device");
            return str;
        }
    }

    public Map<String, String> getHeaders(Context context) {
        Map<String, String> result = new HashMap<>();
        result.put("X-Pflex-Ad-Version", getPflxAdVersion(context));
        result.put("X-Pflex-Ad-Sdk-Version", getPflxSdkVersionCode(context));
        result.put("X-Pflex-Referer", getAppUri(context));
        result.put("X-Pflex-Agent", getUserAgent(context));
        result.put("Accept-Language", getLocale(context));
        String postalCode = getPostalCode(context);
        if (postalCode != null) {
            result.put("X-Pflex-Postalcode", postalCode);
        }
        return result;
    }

    /* access modifiers changed from: protected */
    public void resetCache() {
        this.stringCache = null;
    }
}
