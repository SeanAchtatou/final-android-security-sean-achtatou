package com.pontiflex.mobile.webview.utilities;

import java.util.Properties;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class PontiflexStringsHandler extends DefaultHandler {
    private String currentString;
    private StringBuffer currentValue;
    private Properties props;

    public PontiflexStringsHandler(Properties props2) {
        this.props = props2;
    }

    public Properties getProperties() {
        if (this.props == null) {
            this.props = new Properties();
        }
        return this.props;
    }

    public void characters(char[] ch, int start, int length) throws SAXException {
        if (this.currentString != null) {
            this.currentValue.append(ch, start, length);
        }
    }

    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (this.currentString != null) {
            getProperties().setProperty(this.currentString, this.currentValue.toString());
            this.currentString = null;
        }
    }

    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (localName.equals("string") && attributes != null) {
            for (int i = 0; i < attributes.getLength(); i++) {
                if (attributes.getLocalName(i).equals("name")) {
                    this.currentString = attributes.getValue(i);
                    this.currentValue = new StringBuffer();
                }
            }
        }
    }
}
