package com.pontiflex.mobile.webview.utilities;

import android.os.AsyncTask;
import android.util.Log;
import java.io.IOException;
import java.util.Map;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;

public class HttpAsyncTask extends AsyncTask<Object, Void, Integer> {
    private HttpAsyncTaskDelegate delegate;

    public interface HttpAsyncTaskDelegate {
        void onCancelled();

        void onPostExecute(int i);

        void onPreExecute();
    }

    public HttpAsyncTask(HttpAsyncTaskDelegate delegate2) {
        this.delegate = delegate2;
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        if (this.delegate != null) {
            this.delegate.onCancelled();
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Integer result) {
        if (this.delegate != null) {
            this.delegate.onPostExecute(result.intValue());
        }
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        if (this.delegate != null) {
            this.delegate.onPreExecute();
        }
    }

    /* access modifiers changed from: protected */
    public Integer doInBackground(Object... args) {
        String url = (String) args[0];
        boolean isGet = ((Boolean) args[1]).booleanValue();
        Map<String, String> headers = (Map) args[2];
        DefaultHttpClient httpClient = new DefaultHttpClient(new BasicHttpParams());
        HttpRequestBase request = null;
        if (isGet) {
            request = new HttpGet(url);
        } else {
            new HttpPost(url);
        }
        if (headers != null) {
            for (String headerKey : headers.keySet()) {
                request.addHeader(headerKey, (String) headers.get(headerKey));
            }
        }
        try {
            Log.d("Pontiflex SDK", "Posted request to " + url + " Headers are " + headers + " Response is " + httpClient.execute(request).getStatusLine());
        } catch (ClientProtocolException e) {
            Log.e("Pontiflex SDK", "Unknow http request client protocol", e);
        } catch (IOException e2) {
            Log.e("Pontiflex SDK", "Error posting http request", e2);
        }
        return 0;
    }
}
