package com.pontiflex.mobile.webview.utilities;

import android.app.Application;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import com.pontiflex.mobile.webview.sdk.AdManager;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.MessageDigest;
import java.util.Map;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.json.JSONException;
import org.json.JSONObject;

public class UpdateJsTask extends AsyncTask<Object, Integer, Void> {
    private static final int BUFFERED_READER_SIZE = 256;
    protected static final String PACKAGE_FORCE_UPDATE_PROP = "forceupdate";
    protected static final String PACKAGE_PFLX_SDK_VERSION = "sdkversioncode";
    protected static final String PACKAGE_UPDATE_CHECKSUM = "hash";
    protected static final String PACKAGE_UPDATE_URL = "url";
    protected static String pflxStorageLocation = "pflx_new";
    protected static String pflxTransitionLocation = "pflx_old";

    /* access modifiers changed from: protected */
    public Void doInBackground(Object... args) {
        boolean shouldUpdatePackage;
        if (args == null || args.length < 2) {
            throw new IllegalArgumentException("Invalid URL parameters");
        }
        Context context = (Context) args[1];
        String updateResponseJson = getUpdateResponse((String) args[0], context);
        if (updateResponseJson == null) {
            Log.e("Pontiflex SDK", "No response received from server for package update");
            return null;
        }
        try {
            JSONObject jSONObject = new JSONObject(updateResponseJson);
            boolean shouldForceUpdatePackage = false;
            try {
                shouldForceUpdatePackage = jSONObject.getBoolean(PACKAGE_FORCE_UPDATE_PROP);
            } catch (JSONException e) {
            }
            String currentVersion = VersionHelper.getInstance(context).getPflxSdkVersionCode(context);
            String newVersion = null;
            if (shouldForceUpdatePackage) {
                shouldUpdatePackage = true;
            } else {
                newVersion = jSONObject.getString(PACKAGE_PFLX_SDK_VERSION);
                shouldUpdatePackage = compareVersion(newVersion, currentVersion);
            }
            if (!shouldUpdatePackage) {
                return null;
            }
            String expectedMd5CheckSum = jSONObject.getString(PACKAGE_UPDATE_CHECKSUM);
            if (expectedMd5CheckSum == null || expectedMd5CheckSum.length() == 0) {
                Log.e("Pontiflex SDK", "Invalid expected Md5 Checksum");
                return null;
            }
            String updateUrl = jSONObject.getString(PACKAGE_UPDATE_URL);
            if (updateUrl == null || updateUrl.length() == 0) {
                Log.e("Pontiflex SDK", "Invalid Update Url");
            }
            String jsUpdateHostString = AdManager.getAdManagerInstance((Application) context.getApplicationContext()).getJsUpdateHostString();
            if (jsUpdateHostString == null || jsUpdateHostString.length() == 0) {
                Log.e("Pontiflex SDK", "Invalid ad hosting url");
            }
            if (!getUpdatedPackage(jsUpdateHostString + updateUrl, expectedMd5CheckSum, context)) {
                Log.e("Pontiflex SDK", "Download of updated package failed");
                return null;
            }
            PflxPackageUpdateHelper.getInstance(context).extractAndMovePflxPackage(context, getUpdatedPackageDir(context), PflxPackageUpdateHelper.getInstance(context).getPontiflexPackageDir(context), new File(context.getFilesDir(), pflxTransitionLocation));
            Log.i("Pontiflex SDK", "Pontiflex package is updated from " + currentVersion + " to " + newVersion);
            VersionHelper.getInstance(context).resetCache();
            return null;
        } catch (JSONException e2) {
            JSONException e3 = e2;
            Log.e("Pontiflex SDK", "Failed to retrieve value from JSON: " + e3.getMessage(), e3);
        }
    }

    /* access modifiers changed from: protected */
    public boolean compareVersion(String newVersion, String currentVersion) {
        boolean result = false;
        if (newVersion == null || "".equals(newVersion)) {
            return false;
        }
        if (currentVersion == null || "".equals(currentVersion)) {
            return false;
        }
        String[] currentVersionIdentifiers = currentVersion.split("\\.");
        String[] newVersionIdentifiers = newVersion.split("\\.");
        int ii = 0;
        while (true) {
            if (ii >= newVersionIdentifiers.length) {
                break;
            }
            int newInt = Integer.parseInt(newVersionIdentifiers[ii]);
            int currentInt = -1;
            if (currentVersionIdentifiers.length > ii) {
                currentInt = Integer.parseInt(currentVersionIdentifiers[ii]);
            }
            if (newInt > currentInt) {
                result = true;
                break;
            }
            ii++;
        }
        return result;
    }

    /* access modifiers changed from: protected */
    public String getUpdateResponse(String updateRequestUrl, Context context) {
        InputStream is = null;
        try {
            is = fetchData(updateRequestUrl, context);
            if (is == null) {
                Log.e("Pontiflex SDK", "Couldn't open stream to the destination path");
                if (is != null) {
                    try {
                        is.close();
                    } catch (IOException e) {
                    }
                }
                return null;
            } else if (is != null) {
                StringBuilder sb = new StringBuilder();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"), BUFFERED_READER_SIZE);
                while (true) {
                    String line = reader.readLine();
                    if (line == null) {
                        break;
                    }
                    sb.append(line);
                }
                is.close();
                String sb2 = sb.toString();
                if (is == null) {
                    return sb2;
                }
                try {
                    is.close();
                    return sb2;
                } catch (IOException e2) {
                    return sb2;
                }
            } else {
                if (is != null) {
                    try {
                        is.close();
                    } catch (IOException e3) {
                    }
                }
                return null;
            }
        } catch (ClientProtocolException e4) {
            try {
                Log.e("Pontiflex SDK", "Error in fetching update package " + e4.getMessage());
            } finally {
                if (is != null) {
                    try {
                        is.close();
                    } catch (IOException e5) {
                    }
                }
            }
        } catch (IOException e6) {
            Log.e("Pontiflex SDK", "Error in fetching update package " + e6.getMessage());
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e7) {
                }
            }
        } catch (Throwable th) {
            is.close();
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    public File getUpdatedPackageDir(Context context) {
        return new File(context.getFilesDir(), pflxStorageLocation);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0099 A[SYNTHETIC, Splitter:B:36:0x0099] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x009e A[SYNTHETIC, Splitter:B:39:0x009e] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00b3 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x0109 A[SYNTHETIC, Splitter:B:71:0x0109] */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x010e A[SYNTHETIC, Splitter:B:74:0x010e] */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x0125 A[SYNTHETIC, Splitter:B:83:0x0125] */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x012a A[SYNTHETIC, Splitter:B:86:0x012a] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean getUpdatedPackage(java.lang.String r21, java.lang.String r22, android.content.Context r23) {
        /*
            r20 = this;
            r7 = 0
            r12 = 0
            r14 = 0
            r8 = 0
            r9 = 0
            r0 = r20
            r1 = r21
            r2 = r23
            java.io.InputStream r7 = r0.fetchData(r1, r2)     // Catch:{ ClientProtocolException -> 0x0167, IOException -> 0x00e8 }
            if (r7 != 0) goto L_0x0035
            java.lang.String r17 = "Pontiflex SDK"
            java.lang.String r18 = "Couldn't open stream to the destination path"
            android.util.Log.e(r17, r18)     // Catch:{ ClientProtocolException -> 0x0167, IOException -> 0x00e8 }
            r17 = 0
            if (r7 == 0) goto L_0x001f
            r7.close()     // Catch:{ IOException -> 0x0142 }
        L_0x001f:
            if (r12 == 0) goto L_0x0027
            r12.flush()     // Catch:{ IOException -> 0x0175 }
            r12.close()     // Catch:{ IOException -> 0x0175 }
        L_0x0027:
            if (r14 == 0) goto L_0x0034
            boolean r18 = r14.exists()
            if (r18 == 0) goto L_0x0034
            if (r9 != 0) goto L_0x0034
            r14.delete()
        L_0x0034:
            return r17
        L_0x0035:
            com.pontiflex.mobile.webview.utilities.PflxPackageUpdateHelper r6 = com.pontiflex.mobile.webview.utilities.PflxPackageUpdateHelper.getInstance(r23)     // Catch:{ ClientProtocolException -> 0x0167, IOException -> 0x00e8 }
            r0 = r20
            r1 = r23
            java.io.File r16 = r0.getUpdatedPackageDir(r1)     // Catch:{ ClientProtocolException -> 0x0167, IOException -> 0x00e8 }
            com.pontiflex.mobile.webview.utilities.PflxPackageUpdateHelper.deleteDir(r16)     // Catch:{ ClientProtocolException -> 0x0167, IOException -> 0x00e8 }
            r16.mkdir()     // Catch:{ ClientProtocolException -> 0x0167, IOException -> 0x00e8 }
            java.io.File r15 = new java.io.File     // Catch:{ ClientProtocolException -> 0x0167, IOException -> 0x00e8 }
            java.lang.String r17 = r6.getPontiflexPackageName()     // Catch:{ ClientProtocolException -> 0x0167, IOException -> 0x00e8 }
            r15.<init>(r16, r17)     // Catch:{ ClientProtocolException -> 0x0167, IOException -> 0x00e8 }
            boolean r17 = r15.exists()     // Catch:{ ClientProtocolException -> 0x016c, IOException -> 0x0159, all -> 0x0150 }
            if (r17 == 0) goto L_0x0059
            r15.delete()     // Catch:{ ClientProtocolException -> 0x016c, IOException -> 0x0159, all -> 0x0150 }
        L_0x0059:
            java.io.FileOutputStream r13 = new java.io.FileOutputStream     // Catch:{ ClientProtocolException -> 0x016c, IOException -> 0x0159, all -> 0x0150 }
            r13.<init>(r15)     // Catch:{ ClientProtocolException -> 0x016c, IOException -> 0x0159, all -> 0x0150 }
            r17 = 1024(0x400, float:1.435E-42)
            r0 = r17
            byte[] r0 = new byte[r0]     // Catch:{ ClientProtocolException -> 0x0076, IOException -> 0x015e, all -> 0x0153 }
            r4 = r0
        L_0x0065:
            int r10 = r7.read(r4)     // Catch:{ ClientProtocolException -> 0x0076, IOException -> 0x015e, all -> 0x0153 }
            if (r10 <= 0) goto L_0x00b9
            r17 = 0
            r0 = r13
            r1 = r4
            r2 = r17
            r3 = r10
            r0.write(r1, r2, r3)     // Catch:{ ClientProtocolException -> 0x0076, IOException -> 0x015e, all -> 0x0153 }
            goto L_0x0065
        L_0x0076:
            r17 = move-exception
            r5 = r17
            r14 = r15
            r12 = r13
        L_0x007b:
            java.lang.String r17 = "Pontiflex SDK"
            java.lang.StringBuilder r18 = new java.lang.StringBuilder     // Catch:{ all -> 0x0122 }
            r18.<init>()     // Catch:{ all -> 0x0122 }
            java.lang.String r19 = "Error in fetching update package "
            java.lang.StringBuilder r18 = r18.append(r19)     // Catch:{ all -> 0x0122 }
            java.lang.String r19 = r5.getMessage()     // Catch:{ all -> 0x0122 }
            java.lang.StringBuilder r18 = r18.append(r19)     // Catch:{ all -> 0x0122 }
            java.lang.String r18 = r18.toString()     // Catch:{ all -> 0x0122 }
            android.util.Log.e(r17, r18)     // Catch:{ all -> 0x0122 }
            if (r7 == 0) goto L_0x009c
            r7.close()     // Catch:{ IOException -> 0x0147 }
        L_0x009c:
            if (r12 == 0) goto L_0x00a4
            r12.flush()     // Catch:{ IOException -> 0x0164 }
            r12.close()     // Catch:{ IOException -> 0x0164 }
        L_0x00a4:
            if (r14 == 0) goto L_0x00b1
            boolean r17 = r14.exists()
            if (r17 == 0) goto L_0x00b1
            if (r9 != 0) goto L_0x00b1
            r14.delete()
        L_0x00b1:
            if (r8 == 0) goto L_0x013e
            if (r9 == 0) goto L_0x013e
            r17 = 1
            goto L_0x0034
        L_0x00b9:
            r8 = 1
            r0 = r20
            r1 = r15
            java.lang.String r11 = r0.checksum(r1)     // Catch:{ ClientProtocolException -> 0x0076, IOException -> 0x015e, all -> 0x0153 }
            r0 = r22
            r1 = r11
            boolean r17 = r0.equalsIgnoreCase(r1)     // Catch:{ ClientProtocolException -> 0x0076, IOException -> 0x015e, all -> 0x0153 }
            if (r17 == 0) goto L_0x00cb
            r9 = 1
        L_0x00cb:
            if (r7 == 0) goto L_0x00d0
            r7.close()     // Catch:{ IOException -> 0x0145 }
        L_0x00d0:
            if (r13 == 0) goto L_0x00d8
            r13.flush()     // Catch:{ IOException -> 0x0172 }
            r13.close()     // Catch:{ IOException -> 0x0172 }
        L_0x00d8:
            if (r15 == 0) goto L_0x0178
            boolean r17 = r15.exists()
            if (r17 == 0) goto L_0x0178
            if (r9 != 0) goto L_0x0178
            r15.delete()
            r14 = r15
            r12 = r13
            goto L_0x00b1
        L_0x00e8:
            r17 = move-exception
            r5 = r17
        L_0x00eb:
            java.lang.String r17 = "Pontiflex SDK"
            java.lang.StringBuilder r18 = new java.lang.StringBuilder     // Catch:{ all -> 0x0122 }
            r18.<init>()     // Catch:{ all -> 0x0122 }
            java.lang.String r19 = "Error in fetching update package "
            java.lang.StringBuilder r18 = r18.append(r19)     // Catch:{ all -> 0x0122 }
            java.lang.String r19 = r5.getMessage()     // Catch:{ all -> 0x0122 }
            java.lang.StringBuilder r18 = r18.append(r19)     // Catch:{ all -> 0x0122 }
            java.lang.String r18 = r18.toString()     // Catch:{ all -> 0x0122 }
            android.util.Log.e(r17, r18)     // Catch:{ all -> 0x0122 }
            if (r7 == 0) goto L_0x010c
            r7.close()     // Catch:{ IOException -> 0x014a }
        L_0x010c:
            if (r12 == 0) goto L_0x0114
            r12.flush()     // Catch:{ IOException -> 0x0157 }
            r12.close()     // Catch:{ IOException -> 0x0157 }
        L_0x0114:
            if (r14 == 0) goto L_0x00b1
            boolean r17 = r14.exists()
            if (r17 == 0) goto L_0x00b1
            if (r9 != 0) goto L_0x00b1
            r14.delete()
            goto L_0x00b1
        L_0x0122:
            r17 = move-exception
        L_0x0123:
            if (r7 == 0) goto L_0x0128
            r7.close()     // Catch:{ IOException -> 0x014c }
        L_0x0128:
            if (r12 == 0) goto L_0x0130
            r12.flush()     // Catch:{ IOException -> 0x014e }
            r12.close()     // Catch:{ IOException -> 0x014e }
        L_0x0130:
            if (r14 == 0) goto L_0x013d
            boolean r18 = r14.exists()
            if (r18 == 0) goto L_0x013d
            if (r9 != 0) goto L_0x013d
            r14.delete()
        L_0x013d:
            throw r17
        L_0x013e:
            r17 = 0
            goto L_0x0034
        L_0x0142:
            r18 = move-exception
            goto L_0x001f
        L_0x0145:
            r17 = move-exception
            goto L_0x00d0
        L_0x0147:
            r17 = move-exception
            goto L_0x009c
        L_0x014a:
            r17 = move-exception
            goto L_0x010c
        L_0x014c:
            r18 = move-exception
            goto L_0x0128
        L_0x014e:
            r18 = move-exception
            goto L_0x0130
        L_0x0150:
            r17 = move-exception
            r14 = r15
            goto L_0x0123
        L_0x0153:
            r17 = move-exception
            r14 = r15
            r12 = r13
            goto L_0x0123
        L_0x0157:
            r17 = move-exception
            goto L_0x0114
        L_0x0159:
            r17 = move-exception
            r5 = r17
            r14 = r15
            goto L_0x00eb
        L_0x015e:
            r17 = move-exception
            r5 = r17
            r14 = r15
            r12 = r13
            goto L_0x00eb
        L_0x0164:
            r17 = move-exception
            goto L_0x00a4
        L_0x0167:
            r17 = move-exception
            r5 = r17
            goto L_0x007b
        L_0x016c:
            r17 = move-exception
            r5 = r17
            r14 = r15
            goto L_0x007b
        L_0x0172:
            r17 = move-exception
            goto L_0x00d8
        L_0x0175:
            r18 = move-exception
            goto L_0x0027
        L_0x0178:
            r14 = r15
            r12 = r13
            goto L_0x00b1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.pontiflex.mobile.webview.utilities.UpdateJsTask.getUpdatedPackage(java.lang.String, java.lang.String, android.content.Context):boolean");
    }

    private String checksum(File file) {
        int read;
        try {
            InputStream fin = new FileInputStream(file);
            MessageDigest md5er = MessageDigest.getInstance("MD5");
            byte[] buffer = new byte[1024];
            do {
                read = fin.read(buffer);
                if (read > 0) {
                    md5er.update(buffer, 0, read);
                }
            } while (read != -1);
            fin.close();
            byte[] digest = md5er.digest();
            if (digest == null) {
                return null;
            }
            String strDigest = "";
            for (int i = 0; i < digest.length; i++) {
                strDigest = strDigest + Integer.toString((digest[i] & 255) + 256, 16).substring(1);
            }
            return strDigest;
        } catch (Exception e) {
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onProgressUpdate(Integer... progress) {
        Log.d("Pontiflex SDK", "Updated " + progress[0] + " %");
    }

    private InputStream fetchData(String urlString, Context context) throws ClientProtocolException, IOException {
        DefaultHttpClient httpClient = new DefaultHttpClient(new BasicHttpParams());
        HttpGet request = new HttpGet(urlString);
        Map<String, String> headers = VersionHelper.getInstance(context).getHeaders(context);
        if (headers != null) {
            for (String headerKey : headers.keySet()) {
                request.addHeader(headerKey, headers.get(headerKey));
            }
        }
        HttpResponse response = httpClient.execute(request);
        if (response.getStatusLine().getStatusCode() != 200) {
            return null;
        }
        return response.getEntity().getContent();
    }
}
