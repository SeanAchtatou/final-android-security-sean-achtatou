package com.pontiflex.mobile.webview.sdk.activities;

import android.util.Log;
import android.webkit.WebView;
import com.pontiflex.mobile.webview.sdk.AdManager;
import com.pontiflex.mobile.webview.sdk.IPflexJSInterface;
import com.pontiflex.mobile.webview.utilities.PflxPackageUpdateHelper;
import java.util.Locale;

/* compiled from: BaseActivity */
class PflexJSInterface implements IPflexJSInterface {
    private BaseActivity baseActivity;
    private WebView innerWebView;

    protected PflexJSInterface(BaseActivity activity, WebView webview) {
        this.baseActivity = activity;
        this.innerWebView = webview;
    }

    public void finishActivity() {
        this.baseActivity.setResult(-1);
        this.baseActivity.finish();
    }

    public void logMessage(String message) {
        Log.d(this.baseActivity.getClass().getName(), message);
    }

    public boolean isOnline() {
        return this.baseActivity.isOnline();
    }

    public void initializeData() {
        this.baseActivity.initializeData();
        boolean signupRequired = AdManager.getAdManagerInstance(this.baseActivity.getApplication()).isRegistrationRequired();
        this.innerWebView.loadUrl("javascript:PFLEX.AppInfo.eventHandler.fire('onSetConfig',{signUpRequired:" + signupRequired + ", showMultiAd:" + AdManager.getAdManagerInstance(this.baseActivity.getApplication()).isShowingMultiAdView() + "});");
    }

    public String getAppInfoJsonContent() {
        return this.baseActivity.getAppInfoJsonContent();
    }

    public String getLocale() {
        return Locale.getDefault().getLanguage();
    }

    public void cancelProgressDialog() {
        this.baseActivity.cancelProgressDialog();
    }

    public String getBasePath() {
        return PflxPackageUpdateHelper.getInstance(this.baseActivity.getApplicationContext()).getBasePath(this.baseActivity.getApplicationContext());
    }

    public void asyncHttpRequest(String url, boolean isGet) {
        this.baseActivity.asyncHttpRequest(url, isGet);
    }
}
