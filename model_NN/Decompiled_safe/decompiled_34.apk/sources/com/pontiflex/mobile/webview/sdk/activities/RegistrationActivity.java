package com.pontiflex.mobile.webview.sdk.activities;

import android.os.Bundle;
import com.pontiflex.mobile.webview.utilities.HtmlPageRegistry;
import com.pontiflex.mobile.webview.utilities.PflxPackageUpdateHelper;

public class RegistrationActivity extends SignUpActivity {
    /* access modifiers changed from: protected */
    public void setUpViews() {
        setUpWebview();
        this.webView.loadUrl(PflxPackageUpdateHelper.getInstance(getApplicationContext()).getBasePath(getApplicationContext()) + HtmlPageRegistry.getInstance(getApplicationContext()).getRegistrationHtmlPath(this, false));
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }
}
