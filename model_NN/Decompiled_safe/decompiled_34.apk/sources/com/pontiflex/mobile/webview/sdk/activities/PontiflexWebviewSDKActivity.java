package com.pontiflex.mobile.webview.sdk.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import com.maxdroid.spanish.R;
import com.pontiflex.mobile.webview.sdk.AdManagerFactory;
import com.pontiflex.mobile.webview.sdk.IAdManager;

public class PontiflexWebviewSDKActivity extends Activity {
    private IAdManager adManager;
    private Button clearButton;
    private Button multiAdButton;
    private Button registrationButton;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.adManager = AdManagerFactory.createInstance(getApplication());
        startRegistrationAdHoc();
        setContentView((int) R.layout.main);
        setupViews();
        prefillRegistrationData();
    }

    /* access modifiers changed from: protected */
    public void startRegistrationActivity() {
        this.adManager.startRegistrationActivity(this);
    }

    /* access modifiers changed from: protected */
    public void startMultiOfferActivity() {
        this.adManager.startMultiOfferActivity(this);
    }

    public void clearRegistrationData() {
        this.adManager.clearRegistrationStorage();
    }

    private void startRegistrationAdHoc() {
        this.adManager.setRegistrationMode(IAdManager.RegistrationMode.RegistrationAdHoc);
    }

    private void startRegistrationAfterLaunchCount(int count) {
        this.adManager.setRegistrationInterval(count);
        this.adManager.setRegistrationMode(IAdManager.RegistrationMode.RegistrationAfterIntervalInLaunches);
    }

    private void startRegistrationAtLaunch() {
        this.adManager.setRegistrationMode(IAdManager.RegistrationMode.RegistrationAtLaunch);
    }

    private void prefillRegistrationData() {
        this.adManager.setRegistrationData("first_name", "John");
        this.adManager.setRegistrationData("last_name", "Public");
    }

    private void setupViews() {
        this.registrationButton = (Button) findViewById(R.id.widget30);
        this.registrationButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PontiflexWebviewSDKActivity.this.startRegistrationActivity();
            }
        });
        this.multiAdButton = (Button) findViewById(R.id.ad1);
        this.multiAdButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PontiflexWebviewSDKActivity.this.startMultiOfferActivity();
            }
        });
        this.clearButton = (Button) findViewById(R.id.textview1);
        this.clearButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PontiflexWebviewSDKActivity.this.clearRegistrationData();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1 && resultCode == -1) {
            Log.d("Pontiflex SDK", "Received activity result");
        }
    }

    public IAdManager getAdManager() {
        return this.adManager;
    }
}
