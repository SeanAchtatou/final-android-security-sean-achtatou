package com.pontiflex.mobile.webview.sdk.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.webkit.JsResult;
import android.webkit.WebSettings;
import android.webkit.WebView;
import com.pontiflex.mobile.webview.sdk.AdManager;
import com.pontiflex.mobile.webview.utilities.DeviceHelper;
import com.pontiflex.mobile.webview.utilities.HttpAsyncTask;
import com.pontiflex.mobile.webview.utilities.PackageHelper;
import com.pontiflex.mobile.webview.utilities.RegistrationStorage;
import com.pontiflex.mobile.webview.utilities.VersionHelper;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;
import java.util.concurrent.RejectedExecutionException;

public abstract class BaseActivity extends Activity {
    private static final String AdditionalDataStorageFileName = "PontiflexAdditionalData";
    private static final long PROGRESS_DIALOG_TIMEOUT_MILLIS = 45000;
    private static final String StateKeystoreFilename = "PontiflexStateData";
    private RegistrationStorage additionalDataStorage = null;
    protected PackageHelper helper;
    protected AlertDialog jsConfirmAlertDialog;
    protected JsResult jsResult;
    protected PflexStorageJSInterface jsStorageInterface;
    protected ProgressDialog progressDialog;
    private RegistrationStorage stateStorage = null;
    protected WebView webView;

    /* access modifiers changed from: protected */
    public abstract void initializeData();

    /* access modifiers changed from: protected */
    public abstract void setUpViews();

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().requestFeature(2);
        this.helper = PackageHelper.getInstance(getApplicationContext());
        startProgressDialog();
        launchWebview();
    }

    /* access modifiers changed from: protected */
    public void startProgressDialog() {
        this.progressDialog = ProgressDialog.show(this, "", "Loading ...", true, true);
        new Handler().postDelayed(new Runnable() {
            public void run() {
                BaseActivity.this.cancelProgressDialog();
            }
        }, PROGRESS_DIALOG_TIMEOUT_MILLIS);
    }

    /* access modifiers changed from: protected */
    public void launchWebview() {
        new Handler().post(new Runnable() {
            public void run() {
                BaseActivity.this.initializeData();
                BaseActivity.this.setUpViews();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void cancelProgressDialog() {
        if (this.progressDialog != null && this.progressDialog.isShowing()) {
            this.progressDialog.cancel();
        }
    }

    /* access modifiers changed from: protected */
    public void setProgressDialogProgress(int progress) {
        if (this.progressDialog != null && this.progressDialog.isShowing()) {
            this.progressDialog.setProgress(progress * 100);
        }
    }

    /* access modifiers changed from: protected */
    public void cancelAlertDialog() {
        if (this.jsConfirmAlertDialog != null && this.jsConfirmAlertDialog.isShowing()) {
            this.jsConfirmAlertDialog.cancel();
        }
        if (this.jsResult != null) {
            this.jsResult.cancel();
        }
    }

    public RegistrationStorage getStateStorage() {
        if (this.stateStorage == null) {
            this.stateStorage = new RegistrationStorage(getApplicationContext(), StateKeystoreFilename);
        }
        return this.stateStorage;
    }

    public RegistrationStorage getAdditionalDataStorage() {
        if (this.additionalDataStorage == null) {
            this.additionalDataStorage = new RegistrationStorage(getApplicationContext(), AdditionalDataStorageFileName);
        }
        return this.additionalDataStorage;
    }

    public String getStateData(String key) {
        return getStateStorage().get(key);
    }

    public void clearStateData(String key) {
        getStateStorage().remove(key);
        commitSateStorageChanges(getStateStorage());
    }

    public void saveStateData(String key, String value) {
        getStateStorage().put(key, value);
        commitSateStorageChanges(getStateStorage());
    }

    public String getAdditionalData(String key) {
        return getAdditionalDataStorage().get(key);
    }

    public void clearAdditionalData(String key) {
        getAdditionalDataStorage().remove(key);
        commitSateStorageChanges(getAdditionalDataStorage());
    }

    public void saveAdditionalData(String key, String value) {
        getAdditionalDataStorage().put(key, value);
        commitSateStorageChanges(getAdditionalDataStorage());
    }

    public void commitSateStorageChanges(RegistrationStorage storage) {
        storage.commit(getApplicationContext());
    }

    public void addRegistrationData(String key, String value) {
        AdManager.getInstance(getApplication()).setRegistrationData(key, value);
    }

    public String getRegistrationData(String key) {
        return AdManager.getInstance(getApplication()).getRegistrationData(key);
    }

    public void clearRegistrationData() {
        AdManager.getInstance(getApplication()).clearRegistrationStorage();
    }

    public boolean hasValidRegistrationData() {
        return AdManager.getInstance(getApplication()).hasValidRegistrationData();
    }

    public boolean isOnline() {
        NetworkInfo info = ((ConnectivityManager) getApplicationContext().getSystemService("connectivity")).getActiveNetworkInfo();
        if (info != null) {
            return info.isConnected();
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (isFinishing() && this.jsStorageInterface != null) {
            this.jsStorageInterface.clearState();
        }
        cancelProgressDialog();
        cancelAlertDialog();
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (this.jsStorageInterface != null) {
            this.jsStorageInterface.saveState();
        }
        cancelProgressDialog();
        cancelAlertDialog();
    }

    public String getAppInfoJsonContent() {
        return AdManager.getAdManagerInstance(getApplication()).getAppInfo().toString();
    }

    public void initializeWebSettings() {
        this.webView.getSettings().setJavaScriptEnabled(true);
        enableDomStorage();
    }

    public void enableDomStorage() {
        WebSettings webSettings = this.webView.getSettings();
        try {
            Class[] setDomStorageEnabledSignature = {Boolean.TYPE};
            webSettings.getClass().getMethod("setDomStorageEnabled", setDomStorageEnabledSignature).invoke(webSettings, true);
            webSettings.getClass().getMethod("setDatabaseEnabled", setDomStorageEnabledSignature).invoke(webSettings, true);
        } catch (NoSuchMethodException e) {
            Log.i("Pontiflex SDK", "Dom storage not available.");
        } catch (InvocationTargetException e2) {
            Log.e("Pontiflex SDK", "Not able to initialize dom storage", e2);
        } catch (IllegalAccessException e3) {
            Log.e("Pontiflex SDK", "Not able to initialize dom storage", e3);
        }
    }

    public String getDeviceData(String field) {
        return DeviceHelper.getDeviceData(getApplicationContext(), field);
    }

    /* access modifiers changed from: protected */
    public void setUpWebview() {
        this.webView = new WebView(this);
        setContentView(this.webView);
        initializeWebSettings();
        this.jsStorageInterface = new PflexStorageJSInterface(this, this.webView);
        this.webView.addJavascriptInterface(new PflexJSInterface(this, this.webView), "Activity");
        this.webView.addJavascriptInterface(new PflexVersionJSInterface(this), "ActivityVersionHelper");
        this.webView.addJavascriptInterface(new PflexDeviceJSInterface(this), "ActivityDevice");
        this.webView.addJavascriptInterface(this.jsStorageInterface, "ActivityStorage");
        this.webView.setWebViewClient(new PflexWebViewClient(this));
        this.webView.setWebChromeClient(new PflexWebChromeClient(this));
        this.webView.setBackgroundColor(0);
    }

    public String getAppName(Context context) {
        return VersionHelper.getInstance(getApplicationContext()).getAppName(context);
    }

    public String getAppVersionCode(Context context) {
        int result = VersionHelper.getInstance(context).getAppVersionCode(context);
        return result < 0 ? "" : Integer.toString(result);
    }

    public String getAppVersionName(Context context) {
        return VersionHelper.getInstance(context).getAppVersionName(context);
    }

    public String getUserAgent(Context context) {
        return VersionHelper.getInstance(context).getUserAgent(context);
    }

    public String getLocale(Context context) {
        return VersionHelper.getInstance(context).getLocale(context);
    }

    public String getPostalCode() {
        return VersionHelper.getInstance(getApplicationContext()).getPostalCode(getApplicationContext());
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || !this.webView.canGoBack()) {
            return super.onKeyDown(keyCode, event);
        }
        this.webView.goBack();
        return true;
    }

    public void asyncHttpRequest(String url, boolean isGet) {
        try {
            Map<String, String> headers = VersionHelper.getInstance(getApplicationContext()).getHeaders(getApplicationContext());
            new HttpAsyncTask(null).execute(url, Boolean.valueOf(isGet), headers);
        } catch (RejectedExecutionException e) {
        }
    }
}
