package com.pontiflex.mobile.webview.utilities;

import android.app.Application;
import android.content.Context;
import android.content.pm.PackageManager;
import android.util.Log;
import com.pontiflex.mobile.webview.sdk.AdManager;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Properties;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

public class PflxPackageUpdateHelper {
    protected static final String DefaultPontiflexPackageDirName = "pflx";
    protected static final String DefaultPontiflexPackageName = "js-client-android.zip";
    protected static final String DefaultPontiflexTempPackageDirName = "pflx_tmp";
    private static final String PontiflexSdkVersionBasename = "pflx_version";
    private static PflxPackageUpdateHelper instance;
    private String pontiflexPackageDirName = DefaultPontiflexPackageDirName;
    private String pontiflexPackageName = DefaultPontiflexPackageName;
    private String pontiflexTempPackageDirName = DefaultPontiflexTempPackageDirName;

    public static PflxPackageUpdateHelper getInstance(Context context) {
        if (instance == null) {
            instance = createInstance(context);
        }
        if (instance != null) {
            return instance;
        }
        throw new IllegalStateException();
    }

    public static PflxPackageUpdateHelper createInstance(Context context) {
        if (instance == null) {
            instance = new PflxPackageUpdateHelper(context);
        }
        return instance;
    }

    private PflxPackageUpdateHelper(Context context) {
    }

    public String getPontiflexPackageDirName() {
        return this.pontiflexPackageDirName;
    }

    public void setPontiflexPackageDirName(String pontiflexPackageDirName2) {
        this.pontiflexPackageDirName = pontiflexPackageDirName2;
    }

    public String getPontiflexPackageName() {
        return this.pontiflexPackageName;
    }

    public void setPontiflexPackageName(String pontiflexPackageName2) {
        this.pontiflexPackageName = pontiflexPackageName2;
    }

    public File getPontiflexPackageDir(Context context) {
        return new File(context.getFilesDir(), getPontiflexPackageDirName());
    }

    public String getPontiflexTempPackageDirName() {
        return this.pontiflexTempPackageDirName;
    }

    public void setPontiflexTempPackageDirName(String pontiflexTempPackageDirName2) {
        this.pontiflexTempPackageDirName = pontiflexTempPackageDirName2;
    }

    public void initializePflxPackageLocation(Context context) {
        File pflxDir = new File(context.getFilesDir(), getPontiflexPackageDirName());
        File tmpPflxDir = new File(context.getFilesDir(), getPontiflexTempPackageDirName());
        if (!tmpPflxDir.exists()) {
            tmpPflxDir.mkdir();
        }
        boolean result = copyPflxPackageZipFromAssets(context);
        if (result) {
            extractPflxPackage(context, pflxDir);
        }
        if (!result) {
            result = copyPflxPackageFromAssets(context, tmpPflxDir);
        }
        if (!result) {
            result = copyPflxPackageFromApplicationPackage(context, tmpPflxDir);
        }
        if (result) {
            extractPflxPackage(context, tmpPflxDir);
        }
        if (!pflxDir.exists() || !validatePflxDir(context, pflxDir, tmpPflxDir)) {
            movePflxPackage(context, tmpPflxDir, pflxDir, new File(context.getFilesDir(), UpdateJsTask.pflxTransitionLocation));
            VersionHelper.getInstance(context).resetCache();
            return;
        }
        deleteDir(tmpPflxDir);
        Log.d("Pontiflex SDK", "Pontiflex package is already extracted");
    }

    private boolean validatePflxDir(Context context, File pflxCurrentDir, File pflxTempDir) {
        File htmlDir = new File(pflxCurrentDir, "html");
        if (!htmlDir.exists() || !htmlDir.isDirectory()) {
            return false;
        }
        return validateCurrentVersion(context, pflxCurrentDir, pflxTempDir);
    }

    private boolean validateCurrentVersion(Context context, File pflxCurrentDir, File pflxTempDir) {
        if (!pflxTempDir.exists()) {
            return true;
        }
        try {
            File newVersionFile = new File(pflxTempDir, "pflx_version.xml");
            File currentVersionFile = new File(pflxCurrentDir, "pflx_version.xml");
            if (!newVersionFile.exists()) {
                return true;
            }
            if (!currentVersionFile.exists()) {
                return false;
            }
            Properties newProp = new Properties();
            Properties currentProp = new Properties();
            PackageHelper.getInstance(context).parsePontiflexStrings(new FileInputStream(newVersionFile), newProp);
            PackageHelper.getInstance(context).parsePontiflexStrings(new FileInputStream(currentVersionFile), currentProp);
            return !new UpdateJsTask().compareVersion(newProp.getProperty("PFLEX_SDK_VERSION_CODE"), currentProp.getProperty("PFLEX_SDK_VERSION_CODE"));
        } catch (FileNotFoundException e) {
            Log.e("Pontiflex SDK", "Not able to compare version", e);
            return true;
        }
    }

    private boolean copyPflxPackageZipFromAssets(Context context) {
        File pflxDir = new File(context.getFilesDir(), getPontiflexPackageDirName());
        InputStream is = null;
        FileOutputStream fos = null;
        byte[] buf = new byte[1024];
        try {
            is = context.getAssets().open(getPontiflexPackageName());
            deletePflxDir(context);
            pflxDir.mkdir();
            FileOutputStream fos2 = new FileOutputStream(new File(pflxDir, getPontiflexPackageName()));
            while (true) {
                try {
                    int len = is.read(buf);
                    if (len > 0) {
                        fos2.write(buf, 0, len);
                    } else {
                        closeStreams(is, fos2, null);
                        return true;
                    }
                } catch (IOException e) {
                    fos = fos2;
                    try {
                        Log.e("Pontiflex SDK", "Not able to get " + getPontiflexPackageName() + " from assets folder");
                        closeStreams(is, fos, null);
                        return false;
                    } catch (Throwable th) {
                        th = th;
                        closeStreams(is, fos, null);
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    fos = fos2;
                    closeStreams(is, fos, null);
                    throw th;
                }
            }
        } catch (IOException e2) {
            Log.e("Pontiflex SDK", "Not able to get " + getPontiflexPackageName() + " from assets folder");
            closeStreams(is, fos, null);
            return false;
        }
    }

    private void deletePflxDir(Context context) {
        deleteDir(new File(context.getFilesDir(), getPontiflexPackageDirName()));
    }

    protected static boolean deleteDir(File dir) {
        if (dir == null) {
            return false;
        }
        if (!dir.exists()) {
            return false;
        }
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (String file : children) {
                if (!deleteDir(new File(dir, file))) {
                    return false;
                }
            }
        }
        return dir.delete();
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:25:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean copyPflxPackageFromAssets(android.content.Context r14, java.io.File r15) {
        /*
            r13 = this;
            r11 = 0
            java.lang.String r12 = "pontiflex_sdk.jar"
            r8 = 0
            r2 = 0
            r4 = 0
            r9 = 1024(0x400, float:1.435E-42)
            byte[] r0 = new byte[r9]
            android.content.res.AssetManager r9 = r14.getAssets()     // Catch:{ IOException -> 0x0058 }
            java.lang.String r10 = "pontiflex_sdk.jar"
            java.io.InputStream r4 = r9.open(r10)     // Catch:{ IOException -> 0x0058 }
            java.io.File r6 = new java.io.File     // Catch:{ IOException -> 0x0058 }
            java.lang.String r9 = "pontiflex_sdk.jar"
            r6.<init>(r15, r9)     // Catch:{ IOException -> 0x0058 }
            java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x0058 }
            r3.<init>(r6)     // Catch:{ IOException -> 0x0058 }
        L_0x0020:
            int r5 = r4.read(r0)     // Catch:{ IOException -> 0x002b, all -> 0x0055 }
            if (r5 <= 0) goto L_0x004a
            r9 = 0
            r3.write(r0, r9, r5)     // Catch:{ IOException -> 0x002b, all -> 0x0055 }
            goto L_0x0020
        L_0x002b:
            r9 = move-exception
            r1 = r9
            r2 = r3
        L_0x002e:
            java.lang.String r9 = "Pontiflex SDK"
            java.lang.String r10 = "Not able to extract html package. pontiflex_sdk.jar is not present in assets folder"
            android.util.Log.e(r9, r10)     // Catch:{ all -> 0x0050 }
            r13.closeStreams(r11, r2, r11)
        L_0x0038:
            if (r8 == 0) goto L_0x0049
            java.io.File r9 = new java.io.File
            java.lang.String r10 = "pontiflex_sdk.jar"
            r9.<init>(r15, r12)
            java.lang.String r7 = r9.getAbsolutePath()
            boolean r8 = r13.copyPflxPackageFromJar(r14, r7, r15)
        L_0x0049:
            return r8
        L_0x004a:
            r8 = 1
            r13.closeStreams(r11, r3, r11)
            r2 = r3
            goto L_0x0038
        L_0x0050:
            r9 = move-exception
        L_0x0051:
            r13.closeStreams(r11, r2, r11)
            throw r9
        L_0x0055:
            r9 = move-exception
            r2 = r3
            goto L_0x0051
        L_0x0058:
            r9 = move-exception
            r1 = r9
            goto L_0x002e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.pontiflex.mobile.webview.utilities.PflxPackageUpdateHelper.copyPflxPackageFromAssets(android.content.Context, java.io.File):boolean");
    }

    private boolean copyPflxPackageFromApplicationPackage(Context context, File pflxDir) {
        String packagePath = null;
        try {
            packagePath = context.getPackageManager().getApplicationInfo(context.getPackageName(), 0).sourceDir;
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("Pontiflex SDK", "Not able to extract html package", e);
        }
        if (packagePath != null) {
            return copyPflxPackageFromJar(context, packagePath, pflxDir);
        }
        return false;
    }

    private boolean copyPflxPackageFromJar(Context context, String packagePath, File pflxDir) {
        IOException e;
        FileOutputStream fos;
        InputStream is = null;
        FileOutputStream fos2 = null;
        byte[] buf = new byte[1024];
        try {
            JarFile jarFile = new JarFile(new File(packagePath));
            Enumeration<JarEntry> entries = jarFile.entries();
            while (true) {
                try {
                    fos = fos2;
                    if (entries.hasMoreElements()) {
                        JarEntry entry = entries.nextElement();
                        if (entry.getName().equals(getPontiflexPackageName())) {
                            is = jarFile.getInputStream(entry);
                            fos2 = new FileOutputStream(new File(pflxDir, getPontiflexPackageName()));
                            while (true) {
                                int len = is.read(buf);
                                if (len <= 0) {
                                    break;
                                }
                                fos2.write(buf, 0, len);
                            }
                        } else {
                            fos2 = fos;
                        }
                    } else {
                        closeStreams(is, fos, null);
                        return true;
                    }
                } catch (IOException e2) {
                    e = e2;
                    fos2 = fos;
                    try {
                        Log.e("Pontiflex SDK", "Not able to extract html package", e);
                        closeStreams(is, fos2, null);
                        return false;
                    } catch (Throwable th) {
                        th = th;
                        closeStreams(is, fos2, null);
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    fos2 = fos;
                    closeStreams(is, fos2, null);
                    throw th;
                }
            }
        } catch (IOException e3) {
            e = e3;
            Log.e("Pontiflex SDK", "Not able to extract html package", e);
            closeStreams(is, fos2, null);
            return false;
        }
    }

    private void closeStreams(InputStream is, FileOutputStream fos, FileWriter fw) {
        if (is != null) {
            try {
                is.close();
            } catch (IOException e) {
                Log.e("Pontiflex SDK", "Not able to extract html package", e);
                return;
            }
        }
        if (fos != null) {
            fos.close();
        }
        if (fw != null) {
            fw.close();
        }
    }

    public void extractAndMovePflxPackage(Context context, File pflxSrcDir, File destDir, File transitionDir) {
        extractPflxPackage(context, pflxSrcDir);
        movePflxPackage(context, pflxSrcDir, destDir, transitionDir);
    }

    private void movePflxPackage(Context context, File pflxSrcDir, File destDir, File transitionDir) {
        boolean destDirRenamed = false;
        boolean pflxSrcDirRenamed = false;
        try {
            AdManager.getAdManagerInstance((Application) context.getApplicationContext()).setPackageUpdateInProgress(true);
            deleteDir(transitionDir);
            destDir.renameTo(transitionDir);
            destDirRenamed = true;
            pflxSrcDir.renameTo(destDir);
            pflxSrcDirRenamed = true;
            deleteDir(transitionDir);
        } catch (Exception e) {
            Log.e("Pontiflex SDK", "Error during copying of files", e);
            if (!pflxSrcDirRenamed && destDirRenamed) {
                transitionDir.renameTo(destDir);
            }
        } finally {
            AdManager.getAdManagerInstance((Application) context.getApplicationContext()).setPackageUpdateInProgress(false);
        }
    }

    private void extractPflxPackage(Context context, File pflxDir) {
        ZipException e;
        IOException e2;
        File file = new File(pflxDir, getPontiflexPackageName());
        InputStream is = null;
        FileOutputStream fos = null;
        FileWriter fw = null;
        byte[] buf = new byte[1024];
        try {
            if (file.exists()) {
                ZipFile zipFile = new ZipFile(file);
                Enumeration<? extends ZipEntry> entries = zipFile.entries();
                String replacementPath = "file://" + getPontiflexPackageDir(context) + File.separator;
                FileWriter fw2 = null;
                FileOutputStream fos2 = null;
                while (entries.hasMoreElements()) {
                    try {
                        ZipEntry entry = (ZipEntry) entries.nextElement();
                        File file2 = new File(pflxDir, entry.getName());
                        if (entry.isDirectory()) {
                            file2.mkdir();
                        } else {
                            is = zipFile.getInputStream(entry);
                            fos = new FileOutputStream(file2);
                            try {
                                if (entry.getName().endsWith(".html")) {
                                    String htmlString = PackageHelper.convertStreamToString(is).replaceAll("file:///android_asset", replacementPath);
                                    fw = new FileWriter(file2);
                                    fw.write(htmlString);
                                    fw.flush();
                                    fw.close();
                                } else {
                                    while (true) {
                                        int len = is.read(buf);
                                        if (len <= 0) {
                                            break;
                                        }
                                        fos.write(buf, 0, len);
                                    }
                                    fw = fw2;
                                }
                                fos.close();
                                fw2 = fw;
                                fos2 = fos;
                            } catch (ZipException e3) {
                                e = e3;
                                fw = fw2;
                                try {
                                    Log.e("Pontiflex SDK", "Not able to extract html package", e);
                                    closeStreams(is, fos, fw);
                                } catch (Throwable th) {
                                    th = th;
                                    closeStreams(is, fos, fw);
                                    throw th;
                                }
                            } catch (IOException e4) {
                                e2 = e4;
                                fw = fw2;
                                Log.e("Pontiflex SDK", "Not able to extract html package", e2);
                                closeStreams(is, fos, fw);
                            } catch (Throwable th2) {
                                th = th2;
                                fw = fw2;
                                closeStreams(is, fos, fw);
                                throw th;
                            }
                        }
                    } catch (ZipException e5) {
                        e = e5;
                        fw = fw2;
                        fos = fos2;
                        Log.e("Pontiflex SDK", "Not able to extract html package", e);
                        closeStreams(is, fos, fw);
                    } catch (IOException e6) {
                        e2 = e6;
                        fw = fw2;
                        fos = fos2;
                        Log.e("Pontiflex SDK", "Not able to extract html package", e2);
                        closeStreams(is, fos, fw);
                    } catch (Throwable th3) {
                        th = th3;
                        fw = fw2;
                        fos = fos2;
                        closeStreams(is, fos, fw);
                        throw th;
                    }
                }
                Log.d("Pontiflex SDK", "Extracting pontiflex package is successful");
                VersionHelper.getInstance(context).resetCache();
                fw = fw2;
                fos = fos2;
            }
            closeStreams(is, fos, fw);
        } catch (ZipException e7) {
            e = e7;
            Log.e("Pontiflex SDK", "Not able to extract html package", e);
            closeStreams(is, fos, fw);
        } catch (IOException e8) {
            e2 = e8;
            Log.e("Pontiflex SDK", "Not able to extract html package", e2);
            closeStreams(is, fos, fw);
        }
    }

    public String getBasePath(Context context) {
        String basePath = null;
        InputStream is = null;
        try {
            is = context.getAssets().open(getPontiflexPackageName());
            basePath = "file:///android_asset/";
        } catch (IOException e) {
        } finally {
            closeStreams(is, null, null);
        }
        if (basePath == null) {
            return "file://" + getPontiflexPackageDir(context) + File.separator;
        }
        return basePath;
    }
}
