package com.pontiflex.mobile.webview.utilities;

import android.app.Application;
import android.content.Context;
import com.pontiflex.mobile.webview.sdk.AdManager;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;

public class FieldValidator {
    private static final String ValidationCountry = "country";
    private static final String ValidationEmail = "email";
    private static final String ValidationPostalCode = "postalcode";
    private static final String ValidationRequired = "required";
    private ArrayList<String> errors = new ArrayList<>();

    public static boolean validate(String fieldName, String value, String[] fieldValidations, Map<String, String> additionalValues, Context context) {
        if (fieldValidations == null || fieldValidations.length == 0) {
            return true;
        }
        return new FieldValidator().isValid(fieldName, value, fieldValidations, additionalValues, context);
    }

    public boolean isValid(String fieldName, String value, String[] validations, Map<String, String> additionalValues, Context context) {
        if (value == null) {
            return false;
        }
        String value2 = value.trim();
        String[] arr$ = validations;
        int len$ = arr$.length;
        int i$ = 0;
        while (i$ < len$) {
            String validation = arr$[i$];
            if (!validation.equals(ValidationRequired) || value2.length() != 0) {
                if (validation.equals(ValidationPostalCode)) {
                    String country = null;
                    if (additionalValues != null) {
                        String countryFieldName = AdManager.getAdManagerInstance((Application) context.getApplicationContext()).getAppInfo().getCountryFieldName();
                        if (countryFieldName != null) {
                            country = additionalValues.get(countryFieldName);
                        }
                        if (country == null) {
                            country = "US";
                        }
                    }
                    if (!isZipCode(value2, country)) {
                        addError(fieldName + " must be a valid postal code.");
                        return false;
                    }
                }
                if (validation.equals(ValidationEmail) && !isEmail(value2)) {
                    addError(fieldName + " must be a valid email address.");
                    return false;
                } else if (!validation.equals(ValidationCountry) || isCountry(value2)) {
                    i$++;
                } else {
                    addError(fieldName + " must be a valid 2 letter country code defined in ISO 3166");
                    return false;
                }
            } else {
                addError(fieldName + " is required.");
                return false;
            }
        }
        return true;
    }

    public boolean hasErrors() {
        return getErrors().size() > 0;
    }

    public List<String> getErrors() {
        if (this.errors == null) {
            this.errors = new ArrayList<>();
        }
        return this.errors;
    }

    private void addError(String description) {
        this.errors.add(description);
    }

    private boolean isEmail(String string) {
        if (string == null) {
            return false;
        }
        return Pattern.matches("[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}", string);
    }

    private boolean isZipCode(String string, String country) {
        if (string == null) {
            return false;
        }
        if ("US".equals(country)) {
            return Pattern.matches("[0-9]{5}", string);
        }
        return true;
    }

    private boolean isCountry(String country) {
        if (country == null) {
            return false;
        }
        return Arrays.asList(Locale.getISOCountries()).contains(country);
    }
}
