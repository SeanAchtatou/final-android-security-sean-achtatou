package com.pontiflex.mobile.webview.utilities;

import android.content.Context;
import android.util.Log;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.SecureRandom;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.SecretKeySpec;

public class RegistrationStorage {
    public static final String DefaultKeystoreFilename = "pontiflex_keys";
    private static final String HEX = "0123456789ABCDEF";
    private Properties keystore;
    private String keystoreFilename = DefaultKeystoreFilename;
    private String seed = "yoov5id9lyg6jish7git";

    public RegistrationStorage(Context context) {
        setupKeystore(context);
    }

    public RegistrationStorage(Context context, String keystoreFilename2) {
        this.keystoreFilename = keystoreFilename2;
        setupKeystore(context);
    }

    public void put(String key, String value) {
        this.keystore.setProperty(key, encrypt(value.trim()));
    }

    public String get(String key) {
        return decrypt(this.keystore.getProperty(key));
    }

    public void remove(String key) {
        this.keystore.remove(key);
    }

    public Set<String> keys() {
        HashSet<String> keys = new HashSet<>();
        Enumeration<?> e = this.keystore.propertyNames();
        while (e.hasMoreElements()) {
            keys.add((String) e.nextElement());
        }
        return keys;
    }

    public void clear(Context context) {
        if (context == null) {
            throw new IllegalArgumentException();
        }
        context.deleteFile(this.keystoreFilename);
        this.keystore = new Properties();
    }

    public boolean commit(Context context) {
        if (context == null) {
            throw new IllegalArgumentException();
        } else if (writeKeystore(context)) {
            return true;
        } else {
            return writeKeystore(context);
        }
    }

    public boolean isEmpty() {
        return this.keystore.isEmpty();
    }

    private void setupKeystore(Context context) {
        initializeKeystore();
        readKeystore(context);
    }

    private void initializeKeystore() {
        this.keystore = new Properties();
    }

    private void readKeystore(Context context) {
        InputStream is = null;
        try {
            is = context.openFileInput(this.keystoreFilename);
            if (is != null) {
                initializeKeystore();
                this.keystore.load(is);
            }
        } catch (FileNotFoundException e) {
            Log.e("Pontiflex SDK", "No storage file found");
        } catch (IOException e2) {
            Log.e("Pontiflex SDK", "Error reading storage", e2);
        }
        if (is != null) {
            try {
                is.close();
            } catch (IOException e3) {
                Log.e("Pontiflex SDK", "Error closing storage file", e3);
            }
        }
    }

    private boolean writeKeystore(Context context) {
        Boolean isStored = false;
        OutputStream os = null;
        try {
            os = context.openFileOutput(this.keystoreFilename, 0);
            if (os != null) {
                this.keystore.store(os, "");
                isStored = true;
            }
        } catch (FileNotFoundException e) {
            try {
                new File(context.getFilesDir(), this.keystoreFilename).createNewFile();
            } catch (IOException e1) {
                Log.e("Pontiflex SDK", "Error creating storage file", e1);
            }
        } catch (IOException e2) {
            Log.e("Pontiflex SDK", "Error opening storage file", e2);
        }
        if (os != null) {
            try {
                os.close();
            } catch (IOException e3) {
                Log.e("Pontiflex SDK", "Error closing storage file", e3);
            }
        }
        return isStored.booleanValue();
    }

    /* access modifiers changed from: protected */
    public String encrypt(String plainText) {
        try {
            return xxcrypt(plainText, true);
        } catch (Exception e) {
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public String decrypt(String encryptedText) {
        try {
            return xxcrypt(encryptedText, false);
        } catch (Exception e) {
            return null;
        }
    }

    private String xxcrypt(String text, boolean encrypt) throws Exception {
        KeyGenerator keygen = KeyGenerator.getInstance("AES");
        SecureRandom secrand = SecureRandom.getInstance("SHA1PRNG");
        secrand.setSeed(this.seed.getBytes());
        keygen.init(128, secrand);
        SecretKeySpec skeySpec = new SecretKeySpec(keygen.generateKey().getEncoded(), "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(encrypt ? 1 : 2, skeySpec);
        byte[] xxcrypted = cipher.doFinal(encrypt ? text.getBytes() : toByte(text));
        if (encrypt) {
            return toHex(xxcrypted);
        }
        return new String(xxcrypted);
    }

    private static byte[] toByte(String hexString) {
        int len = hexString.length() / 2;
        byte[] result = new byte[len];
        for (int i = 0; i < len; i++) {
            result[i] = Integer.valueOf(hexString.substring(i * 2, (i * 2) + 2), 16).byteValue();
        }
        return result;
    }

    private static String toHex(byte[] buf) {
        if (buf == null) {
            return "";
        }
        StringBuffer result = new StringBuffer(buf.length * 2);
        for (byte appendHex : buf) {
            appendHex(result, appendHex);
        }
        return result.toString();
    }

    private static void appendHex(StringBuffer sb, byte b) {
        sb.append(HEX.charAt((b >> 4) & 15)).append(HEX.charAt(b & 15));
    }
}
