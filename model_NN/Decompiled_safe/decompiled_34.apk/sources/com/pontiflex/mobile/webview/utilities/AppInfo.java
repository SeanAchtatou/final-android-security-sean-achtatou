package com.pontiflex.mobile.webview.utilities;

import android.content.Context;
import android.util.Log;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.TreeMap;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AppInfo extends JSONObject {
    private static final String AppInfoFileName = "AppInfo.json";
    private static final Comparator<JSONObject> fieldsComparator = new Comparator<JSONObject>() {
        public int compare(JSONObject field1, JSONObject field2) {
            try {
                int compValue = field1.getInt("order") - field2.getInt("order");
                if (compValue != 0 || field1.getString("displayName") == null || field2.getString("displayName") == null) {
                    return compValue;
                }
                return field1.getString("displayName").compareToIgnoreCase(field2.getString("displayName"));
            } catch (JSONException e) {
                Log.e("Pontiflex SDK", "Error sorting fields", e);
                return 0;
            }
        }
    };

    public static AppInfo createAppInfo(Context context) {
        return createAppInfo(context, AppInfoFileName);
    }

    /* JADX INFO: Multiple debug info for r11v5 java.io.InputStream: [D('is' java.io.InputStream), D('appInfoFileame' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r0v17 int: [D('countryValidations' org.json.JSONArray), D('i' int)] */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x00b2, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00b3, code lost:
        android.util.Log.e("Pontiflex SDK", "Error reading feild in json", r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00bb, code lost:
        r11 = e;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00bb A[ExcHandler: IOException (e java.io.IOException), Splitter:B:3:0x0016] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.pontiflex.mobile.webview.utilities.AppInfo createAppInfo(android.content.Context r10, java.lang.String r11) {
        /*
            com.pontiflex.mobile.webview.utilities.AppInfo r0 = new com.pontiflex.mobile.webview.utilities.AppInfo
            r0.<init>()
            android.content.res.AssetManager r10 = r10.getAssets()     // Catch:{ IOException -> 0x00f2, JSONException -> 0x00ee }
            java.io.InputStream r11 = r10.open(r11)     // Catch:{ IOException -> 0x00f2, JSONException -> 0x00ee }
            com.pontiflex.mobile.webview.utilities.AppInfo r10 = new com.pontiflex.mobile.webview.utilities.AppInfo     // Catch:{ IOException -> 0x00f2, JSONException -> 0x00ee }
            java.lang.String r11 = com.pontiflex.mobile.webview.utilities.PackageHelper.convertStreamToString(r11)     // Catch:{ IOException -> 0x00f2, JSONException -> 0x00ee }
            r10.<init>(r11)     // Catch:{ IOException -> 0x00f2, JSONException -> 0x00ee }
            org.json.JSONObject r11 = r10.getRegistrationFields()     // Catch:{ IOException -> 0x00bb, JSONException -> 0x00d2 }
            if (r11 == 0) goto L_0x00c3
            org.json.JSONObject r4 = r10.getRegistrationFields()     // Catch:{ IOException -> 0x00bb, JSONException -> 0x00d2 }
            java.lang.String r2 = r10.getPostalCodeFieldName()     // Catch:{ IOException -> 0x00bb, JSONException -> 0x00d2 }
            java.lang.String r0 = r10.getCountryFieldName()     // Catch:{ IOException -> 0x00bb, JSONException -> 0x00d2 }
            r11 = 0
            if (r2 == 0) goto L_0x00c3
            if (r0 != 0) goto L_0x00ad
            org.json.JSONObject r11 = new org.json.JSONObject     // Catch:{ IOException -> 0x00bb, JSONException -> 0x00d2 }
            r11.<init>()     // Catch:{ IOException -> 0x00bb, JSONException -> 0x00d2 }
            java.lang.String r0 = "country"
            r4.put(r0, r11)     // Catch:{ IOException -> 0x00bb, JSONException -> 0x00d2 }
            java.lang.String r0 = "displayName"
            java.lang.String r1 = "Country"
            r11.put(r0, r1)     // Catch:{ IOException -> 0x00bb, JSONException -> 0x00d2 }
            java.lang.String r0 = "type"
            java.lang.String r1 = "select"
            r11.put(r0, r1)     // Catch:{ IOException -> 0x00bb, JSONException -> 0x00d2 }
            java.lang.String r0 = "order"
            org.json.JSONObject r1 = r4.getJSONObject(r2)     // Catch:{ IOException -> 0x00bb, JSONException -> 0x00d2 }
            java.lang.String r3 = "order"
            int r1 = r1.getInt(r3)     // Catch:{ IOException -> 0x00bb, JSONException -> 0x00d2 }
            r11.put(r0, r1)     // Catch:{ IOException -> 0x00bb, JSONException -> 0x00d2 }
        L_0x0054:
            r0 = 0
            java.lang.String r1 = "optionType"
            java.lang.String r0 = r11.getString(r1)     // Catch:{ JSONException -> 0x00b2, IOException -> 0x00bb }
        L_0x005b:
            if (r0 != 0) goto L_0x0064
            java.lang.String r0 = "optionType"
            java.lang.String r1 = "country"
            r11.put(r0, r1)     // Catch:{ IOException -> 0x00bb, JSONException -> 0x00d2 }
        L_0x0064:
            org.json.JSONObject r0 = r4.getJSONObject(r2)     // Catch:{ IOException -> 0x00bb, JSONException -> 0x00d2 }
            java.lang.String r1 = "validations"
            org.json.JSONArray r6 = r0.getJSONArray(r1)     // Catch:{ IOException -> 0x00bb, JSONException -> 0x00d2 }
            java.util.ArrayList r3 = new java.util.ArrayList     // Catch:{ IOException -> 0x00bb, JSONException -> 0x00d2 }
            r3.<init>()     // Catch:{ IOException -> 0x00bb, JSONException -> 0x00d2 }
            r0 = 0
            r1 = r0
        L_0x0075:
            int r0 = r6.length()     // Catch:{ IOException -> 0x00bb, JSONException -> 0x00d2 }
            if (r1 >= r0) goto L_0x00df
            java.lang.Object r0 = r6.get(r1)     // Catch:{ IOException -> 0x00bb, JSONException -> 0x00d2 }
            java.lang.String r5 = r0.toString()     // Catch:{ IOException -> 0x00bb, JSONException -> 0x00d2 }
            java.lang.String r0 = "required"
            boolean r0 = r5.equals(r0)     // Catch:{ IOException -> 0x00bb, JSONException -> 0x00d2 }
            if (r0 == 0) goto L_0x00c4
            org.json.JSONArray r0 = new org.json.JSONArray     // Catch:{ IOException -> 0x00bb, JSONException -> 0x00d2 }
            r7 = 2
            java.lang.String[] r7 = new java.lang.String[r7]     // Catch:{ IOException -> 0x00bb, JSONException -> 0x00d2 }
            r8 = 0
            java.lang.String r9 = "required"
            r7[r8] = r9     // Catch:{ IOException -> 0x00bb, JSONException -> 0x00d2 }
            r8 = 1
            java.lang.String r9 = "country"
            r7[r8] = r9     // Catch:{ IOException -> 0x00bb, JSONException -> 0x00d2 }
            java.util.List r7 = java.util.Arrays.asList(r7)     // Catch:{ IOException -> 0x00bb, JSONException -> 0x00d2 }
            r0.<init>(r7)     // Catch:{ IOException -> 0x00bb, JSONException -> 0x00d2 }
            java.lang.String r7 = "validations"
            r11.put(r7, r0)     // Catch:{ IOException -> 0x00bb, JSONException -> 0x00d2 }
            r3.add(r5)     // Catch:{ IOException -> 0x00bb, JSONException -> 0x00d2 }
        L_0x00a9:
            int r0 = r1 + 1
            r1 = r0
            goto L_0x0075
        L_0x00ad:
            org.json.JSONObject r11 = r4.getJSONObject(r0)     // Catch:{ IOException -> 0x00bb, JSONException -> 0x00d2 }
            goto L_0x0054
        L_0x00b2:
            r1 = move-exception
            java.lang.String r3 = "Pontiflex SDK"
            java.lang.String r5 = "Error reading feild in json"
            android.util.Log.e(r3, r5, r1)     // Catch:{ IOException -> 0x00bb, JSONException -> 0x00d2 }
            goto L_0x005b
        L_0x00bb:
            r11 = move-exception
        L_0x00bc:
            java.lang.String r0 = "Pontiflex SDK"
            java.lang.String r1 = "Error loading AppInfo file"
            android.util.Log.e(r0, r1, r11)
        L_0x00c3:
            return r10
        L_0x00c4:
            java.lang.String r0 = "uspostalcode"
            boolean r0 = r5.equals(r0)     // Catch:{ IOException -> 0x00bb, JSONException -> 0x00d2 }
            if (r0 == 0) goto L_0x00db
            java.lang.String r0 = "postalcode"
            r3.add(r0)     // Catch:{ IOException -> 0x00bb, JSONException -> 0x00d2 }
            goto L_0x00a9
        L_0x00d2:
            r11 = move-exception
        L_0x00d3:
            java.lang.String r0 = "Pontiflex SDK"
            java.lang.String r1 = "Error parsing AppInfo"
            android.util.Log.e(r0, r1, r11)
            goto L_0x00c3
        L_0x00db:
            r3.add(r5)     // Catch:{ IOException -> 0x00bb, JSONException -> 0x00d2 }
            goto L_0x00a9
        L_0x00df:
            org.json.JSONObject r11 = r4.getJSONObject(r2)     // Catch:{ IOException -> 0x00bb, JSONException -> 0x00d2 }
            java.lang.String r0 = "validations"
            org.json.JSONArray r1 = new org.json.JSONArray     // Catch:{ IOException -> 0x00bb, JSONException -> 0x00d2 }
            r1.<init>(r3)     // Catch:{ IOException -> 0x00bb, JSONException -> 0x00d2 }
            r11.put(r0, r1)     // Catch:{ IOException -> 0x00bb, JSONException -> 0x00d2 }
            goto L_0x00c3
        L_0x00ee:
            r10 = move-exception
            r11 = r10
            r10 = r0
            goto L_0x00d3
        L_0x00f2:
            r10 = move-exception
            r11 = r10
            r10 = r0
            goto L_0x00bc
        */
        throw new UnsupportedOperationException("Method not decompiled: com.pontiflex.mobile.webview.utilities.AppInfo.createAppInfo(android.content.Context, java.lang.String):com.pontiflex.mobile.webview.utilities.AppInfo");
    }

    public AppInfo() {
    }

    public AppInfo(String json) throws JSONException {
        super(json);
    }

    public String getSubSourceId() {
        try {
            return getString("subSourceId");
        } catch (JSONException e) {
            Log.e("Pontiflex SDK", "Error reading JSON", e);
            return null;
        }
    }

    public Integer getPid() {
        try {
            return Integer.valueOf(getInt("pid"));
        } catch (JSONException e) {
            Log.e("Pontiflex SDK", "Error reading JSON", e);
            return 0;
        }
    }

    public int getFieldCount() {
        JSONObject fields = getRegistrationFields();
        if (fields != null) {
            return fields.length();
        }
        return 0;
    }

    public JSONObject getField(String name) {
        try {
            return getRegistrationFields().getJSONObject(name);
        } catch (JSONException e) {
            Log.e("Pontiflex SDK", "Error reading JSON", e);
            return null;
        }
    }

    public String getOfferString(String name) {
        try {
            return getRegistrationOffer().getString(name);
        } catch (JSONException e) {
            Log.e("Pontiflex SDK", "Error reading JSON", e);
            return null;
        }
    }

    public JSONObject getRegistrationFields() {
        try {
            return getJSONObject("registrationFields");
        } catch (JSONException e) {
            Log.e("Pontiflex SDK", "Error reading JSON", e);
            return null;
        }
    }

    public JSONObject getRegistrationOffer() {
        try {
            return getJSONObject("registrationOffer");
        } catch (JSONException e) {
            Log.e("Pontiflex SDK", "Error reading JSON", e);
            return null;
        }
    }

    public String getDisplayNameForFieldNamed(String name) {
        try {
            return getField(name).getString("displayName");
        } catch (JSONException e) {
            Log.e("Pontiflex SDK", "Error reading JSON", e);
            return null;
        }
    }

    public String[] getValidationsForFieldNamed(String name) {
        try {
            JSONArray array = getField(name).getJSONArray("validations");
            String[] validations = new String[array.length()];
            for (int i = 0; i < array.length(); i++) {
                validations[i] = array.getString(i);
            }
            return validations;
        } catch (JSONException e) {
            Log.e("Pontiflex SDK", "Error reading JSON", e);
            return new String[0];
        }
    }

    public JSONObject findOrderedField(int order) {
        JSONObject json = null;
        JSONObject fields = getRegistrationFields();
        if (fields == null) {
            return null;
        }
        Iterator<String> iter = fields.keys();
        while (true) {
            if (!iter.hasNext()) {
                break;
            }
            try {
                JSONObject field = fields.getJSONObject(iter.next());
                if (order == field.getInt("order")) {
                    json = field;
                    break;
                }
            } catch (JSONException e) {
                Log.e("Pontiflex SDK", "Error reading JSON", e);
            }
        }
        return json;
    }

    public String getNameOfOrderedField(int order) {
        String name = null;
        JSONObject fields = getRegistrationFields();
        if (fields == null) {
            return null;
        }
        Iterator<String> iter = fields.keys();
        while (true) {
            if (!iter.hasNext()) {
                break;
            }
            String key = iter.next();
            try {
                if (order == fields.getJSONObject(key).getInt("order")) {
                    name = key;
                    break;
                }
            } catch (JSONException e) {
                Log.e("Pontiflex SDK", "Error reading JSON", e);
            }
        }
        return name;
    }

    public Collection<String> getOrderedFieldNames() {
        TreeMap<JSONObject, String> orderedFieldNames = new TreeMap<>(fieldsComparator);
        JSONObject fields = getRegistrationFields();
        if (fields != null) {
            Iterator<String> iter = fields.keys();
            while (iter.hasNext()) {
                String fieldName = iter.next();
                try {
                    orderedFieldNames.put(fields.getJSONObject(fieldName), fieldName);
                } catch (JSONException e) {
                    Log.e("Pontiflex SDK", "Error reading JSON", e);
                }
            }
        }
        return orderedFieldNames.values();
    }

    public String getPostalCodeFieldName() {
        JSONObject regFields = getRegistrationFields();
        if (regFields != null) {
            try {
                Iterator<String> it = regFields.keys();
                while (it.hasNext()) {
                    String fieldName = it.next();
                    JSONObject field = regFields.getJSONObject(fieldName);
                    if (field.has("validations") && field.getJSONArray("validations").length() > 0) {
                        JSONArray validations = field.getJSONArray("validations");
                        for (int i = 0; i < validations.length(); i++) {
                            if ("uspostalcode".equalsIgnoreCase(validations.get(i).toString()) || "postalcode".equalsIgnoreCase(validations.get(i).toString())) {
                                return fieldName;
                            }
                        }
                        continue;
                    }
                }
            } catch (JSONException e) {
                Log.e(getClass().getName(), "Error getting value from registration fields");
            }
        }
        return null;
    }

    public String getCountryFieldName() {
        JSONObject regFields = getRegistrationFields();
        if (regFields != null) {
            try {
                Iterator<String> it = regFields.keys();
                while (it.hasNext()) {
                    String fieldName = it.next();
                    JSONObject field = regFields.getJSONObject(fieldName);
                    if (field.has("validations") && field.getJSONArray("validations").length() > 0) {
                        JSONArray validations = field.getJSONArray("validations");
                        for (int i = 0; i < validations.length(); i++) {
                            if ("country".equalsIgnoreCase(validations.get(i).toString())) {
                                return fieldName;
                            }
                        }
                        continue;
                    }
                }
                Iterator<String> it1 = regFields.keys();
                while (it1.hasNext()) {
                    String fieldName2 = it1.next();
                    if (fieldName2 != null && fieldName2.toLowerCase().contains("country")) {
                        return fieldName2;
                    }
                }
            } catch (JSONException e) {
                Log.e(getClass().getName(), "Error getting value from registration fields");
            }
        }
        return null;
    }
}
