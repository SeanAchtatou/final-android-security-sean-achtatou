package com.admob.android.ads;

import android.view.animation.Interpolator;

/* compiled from: AdjustedInterpolator */
public final class c implements Interpolator {
    private Interpolator a;
    private float b;
    private float c;

    public c(Interpolator interpolator, long j, long j2, long j3) {
        this.a = interpolator;
        this.b = ((float) j) / ((float) j3);
        this.c = ((float) j2) / ((float) j3);
    }

    public final float getInterpolation(float f) {
        if (f <= this.b) {
            return -1.0f;
        }
        if (f <= this.b + this.c) {
            return this.a.getInterpolation((f - this.b) / this.c);
        }
        return 2.0f;
    }
}
