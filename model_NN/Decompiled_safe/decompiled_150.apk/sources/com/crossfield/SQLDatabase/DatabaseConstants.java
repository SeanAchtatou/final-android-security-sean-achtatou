package com.crossfield.SQLDatabase;

public class DatabaseConstants {
    public static final int COL_COUNTRY = 5;
    public static final int COL_DATE = 4;
    public static final int COL_ID = 0;
    public static final int COL_LEVEL = 3;
    public static final int COL_NAME = 1;
    public static final int COL_POINT = 2;
    public static final String COL_SCORE_COUNTRY = "score_country";
    public static final String COL_SCORE_DATE = "score_date";
    public static final String COL_SCORE_ID = "score_id";
    public static final String COL_SCORE_LEVEL = "score_level";
    public static final String COL_SCORE_NAME = "score_name";
    public static final String COL_SCORE_POINT = "score_point";
    public static final String DATABASE_NAME = "db_game";
    public static final int DATABASE_VERSION = 1;
    public static final String TBL_SCORE = "tbl_score";
}
