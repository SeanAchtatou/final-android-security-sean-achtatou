package com.crossfield.SQLDatabase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class DatabaseAdapter {
    private Context context;
    private SQLiteDatabase database;
    private DatabaseOpenHelper dbHelper;

    public DatabaseAdapter(Context context2) {
        this.context = context2;
    }

    public DatabaseAdapter open() throws SQLException {
        this.dbHelper = new DatabaseOpenHelper(this.context);
        this.database = this.dbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        this.dbHelper.close();
    }

    private ContentValues createContentValues(String name, float point, int level, String date, String country) {
        ContentValues values = new ContentValues();
        values.put(DatabaseConstants.COL_SCORE_NAME, name);
        values.put(DatabaseConstants.COL_SCORE_POINT, Float.valueOf(point));
        values.put(DatabaseConstants.COL_SCORE_LEVEL, Integer.valueOf(level));
        values.put(DatabaseConstants.COL_SCORE_DATE, date);
        values.put(DatabaseConstants.COL_SCORE_COUNTRY, country);
        return values;
    }

    public long addScore(String name, float point, int level, String date, String country) {
        return this.database.insert(DatabaseConstants.TBL_SCORE, null, createContentValues(name, point, level, date, country));
    }

    public Cursor getAllScore() {
        return this.database.rawQuery("SELECT * FROM tbl_score ORDER BY score_point desc ", null);
    }

    public Cursor getNewScore() {
        return this.database.rawQuery("SELECT * FROM tbl_score ORDER BY score_date desc ", null);
    }
}
