package com.crossfield.SQLDatabase;

public class ScoreLocal {
    private String country;
    private String date;
    private int id;
    private int level;
    private String name;
    private float point;

    public ScoreLocal(float point2, int level2, String date2) {
        this.point = point2;
        this.level = level2;
        this.date = date2;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id2) {
        this.id = id2;
    }

    public float getPoint() {
        return this.point;
    }

    public void setPoint(float point2) {
        this.point = point2;
    }

    public int getLevel() {
        return this.level;
    }

    public void setLevel(int level2) {
        this.level = level2;
    }

    public String getDate() {
        return this.date;
    }

    public void setDate(String date2) {
        this.date = date2;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getName() {
        return this.name;
    }

    public void setCountry(String country2) {
        this.country = country2;
    }

    public String getCountry() {
        return this.country;
    }
}
