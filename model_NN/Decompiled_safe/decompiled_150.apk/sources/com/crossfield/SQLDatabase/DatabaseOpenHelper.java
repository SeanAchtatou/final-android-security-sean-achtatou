package com.crossfield.SQLDatabase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseOpenHelper extends SQLiteOpenHelper {
    public DatabaseOpenHelper(Context context) {
        super(context, DatabaseConstants.DATABASE_NAME, (SQLiteDatabase.CursorFactory) null, 1);
    }

    public void onCreate(SQLiteDatabase db) {
        StringBuffer sb = new StringBuffer();
        sb.append("CREATE TABLE ");
        sb.append(DatabaseConstants.TBL_SCORE);
        sb.append(" ( ");
        sb.append("score_id INTEGER PRIMARY KEY autoincrement, ");
        sb.append("score_name TEXT  not null, ");
        sb.append("score_point REAL not null, ");
        sb.append("score_level INTEGER not null, ");
        sb.append("score_date TEXT  not null, ");
        sb.append("score_country TEXT  not null ");
        sb.append(" ); ");
        db.execSQL(sb.toString());
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS tbl_score");
        onCreate(db);
    }
}
