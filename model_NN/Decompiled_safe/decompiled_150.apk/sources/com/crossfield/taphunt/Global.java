package com.crossfield.taphunt;

import com.crossfield.SQLDatabase.DatabaseAdapter;
import com.crossfield.rank.RankActivity;
import com.crossfield.result.ResultActivity;
import com.crossfield.stage.StageActivity;
import com.crossfield.title.TitleActivity;
import java.util.Random;
import javax.microedition.khronos.opengles.GL10;

public class Global {
    public static final int COLLECTION = 5;
    public static final int END = 8;
    public static final String GMAIL_SUBJECT = "Playing TapHunt";
    public static final String GMAIL_TEXT = "Hello I am now playing TapHunt!\t\t\t\t\t\t\t\t\nhttp://goo.gl/tUeVM";
    public static final int MAIN = 0;
    public static final int NEW_RECORD = 1;
    public static final int NORMAL_RECORD = 0;
    public static final int RANK = 4;
    public static final int RESULT = 3;
    public static final int STAGE = 2;
    public static final int STATUS = 6;
    public static final int STORE = 7;
    public static final int TITLE = 1;
    public static final int TODAY_RECORD = 2;
    public static String country;
    public static DatabaseAdapter dbAdapter;
    public static GL10 gl;
    public static MainActivity mainActivity;
    public static String name = "";
    public static int newRecord = 0;
    public static Random rand = new Random(System.currentTimeMillis());
    public static RankActivity rankActivity;
    public static ResultActivity resultActivity;
    public static int scene = 1;
    public static int score;
    public static StageActivity stageActivity;
    public static TitleActivity titleActivity;
    public static Analytics tracker;
}
