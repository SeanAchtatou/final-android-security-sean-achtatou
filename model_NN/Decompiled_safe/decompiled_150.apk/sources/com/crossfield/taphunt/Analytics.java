package com.crossfield.taphunt;

import android.content.Context;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;

public class Analytics {
    public static final String ID = "UA-24622008-21";
    private GoogleAnalyticsTracker tracker;

    public Analytics(GoogleAnalyticsTracker tracker2, Context context) {
        this.tracker = tracker2;
        this.tracker.start(ID, context);
    }

    public void trackPageView(String page) {
        this.tracker.trackPageView(page);
        this.tracker.dispatch();
    }

    public void trackEvent(String category, String action, String label, int value) {
        this.tracker.trackEvent(category, action, label, value);
        this.tracker.dispatch();
    }
}
