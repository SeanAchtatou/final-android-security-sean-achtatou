package com.crossfield.taphunt;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import com.crossfield.SQLDatabase.DatabaseAdapter;
import com.crossfield.rank.RankActivity;
import com.crossfield.result.ResultActivity;
import com.crossfield.stage.StageActivity;
import com.crossfield.title.TitleActivity;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;

public class MainActivity extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().addFlags(1024);
        Global.mainActivity = this;
        Global.tracker = new Analytics(GoogleAnalyticsTracker.getInstance(), this);
        Global.dbAdapter = new DatabaseAdapter(this);
        Global.dbAdapter.open();
        Global.country = getResources().getConfiguration().locale.getCountry();
        Global.scene = 1;
    }

    public void onResume() {
        super.onResume();
        switch (Global.scene) {
            case 1:
                startActivity(new Intent(this, TitleActivity.class));
                return;
            case 2:
                startActivity(new Intent(this, StageActivity.class));
                return;
            case 3:
                startActivity(new Intent(this, ResultActivity.class));
                return;
            case 4:
                startActivity(new Intent(this, RankActivity.class));
                return;
            case 5:
            case 6:
            case Global.STORE /*7*/:
            default:
                return;
            case Global.END /*8*/:
                finish();
                return;
        }
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getAction() == 0) {
            switch (event.getKeyCode()) {
                case 4:
                    Global.scene = 8;
                    finish();
                    break;
            }
        }
        return super.dispatchKeyEvent(event);
    }

    public void onDestroy() {
        super.onDestroy();
        Global.scene = 1;
    }
}
