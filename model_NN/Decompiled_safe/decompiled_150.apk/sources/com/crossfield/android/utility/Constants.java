package com.crossfield.android.utility;

public class Constants {
    public static final String RANKING_CATEGORY_ALL = "all";
    public static final String RANKING_CATEGORY_MONTHLY = "monthly";
    public static final String RANKING_CATEGORY_TODAY = "today";
    public static final String RANKING_CATEGORY_WEEKLY = "weekly";
    public static final String RANKING_CATEGORY_YESTERDAY = "yesterday";
    public static final String RANKING_COUNTRY_DOMESTIC = "JP";
    public static final String RANKING_COUNTRY_LOCAL = "JP";
    public static final String RANKING_COUNTRY_WORLD = "world";
    public static final String URL_GET_RANKING = "http://180.131.136.13:8080/taphuntweb/get_ranking.rs";
    public static final String URL_INSER_POINT = "http://180.131.136.13:8080/taphuntweb/submit_point.rs";
    public static final String URL_PK_INITIAL_PART = "http://180.131.136.13:8080/taphuntweb/get_pk_initial_part.rs";
    public static final String URL_PURCHASED_PRD_LIST = "http://180.131.136.13:8080/taphuntweb/purchased_product_list.rs";
    public static final String URL_PURCHASE_DELETE = "http://180.131.136.13:8080/taphuntweb/purchase_delete.rs";
    public static final String URL_PURCHASE_INSERT = "http://180.131.136.13:8080/taphuntweb/purchase_insert.rs";
    public static final String ip = "180.131.136.13";
}
