package com.crossfield.result;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.telephony.TelephonyManager;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;
import com.crossfield.android.utility.Constants;
import com.crossfield.android.utility.RestWebServiceClient;
import com.crossfield.more.MoreActivity;
import com.crossfield.taphunt.Global;
import com.crossfield.taphunt.R;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

public class ResultActivity extends Activity implements Runnable {
    /* access modifiers changed from: private */
    public static ProgressDialog waitDialog;
    private Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            ResultActivity.this.insert();
            ResultActivity.waitDialog.dismiss();
            ResultActivity.waitDialog = null;
        }
    };
    private Thread thread;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().addFlags(1024);
        setContentView((int) R.layout.result);
        Global.resultActivity = this;
        Global.tracker.trackPageView("result");
        TextView recodMessage = (TextView) findViewById(R.id.textView_recordMessage);
        switch (Global.newRecord) {
            case 0:
                recodMessage.setVisibility(4);
                break;
            case 1:
                recodMessage.setVisibility(0);
                recodMessage.setText("New record!!");
                setWait();
                break;
            case 2:
                recodMessage.setVisibility(0);
                recodMessage.setText("Tday's new record!!");
                setWait();
                break;
        }
        Global.newRecord = 0;
        ((TextView) findViewById(R.id.textView_score)).setText(String.valueOf(Global.score));
    }

    private void setWait() {
        waitDialog = new ProgressDialog(this);
        waitDialog.setMessage("Loading...");
        waitDialog.setProgressStyle(0);
        waitDialog.show();
        this.thread = new Thread(this);
        this.thread.start();
    }

    public void run() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
        }
        this.handler.sendEmptyMessage(0);
    }

    public void insert() {
        RestWebServiceClient restClient = new RestWebServiceClient(Constants.URL_INSER_POINT);
        List<NameValuePair> nameValuePairs = new ArrayList<>();
        nameValuePairs.add(new BasicNameValuePair("user_name", Global.name));
        nameValuePairs.add(new BasicNameValuePair("user_point", String.valueOf(Global.score)));
        nameValuePairs.add(new BasicNameValuePair("country_code", getResources().getConfiguration().locale.getCountry()));
        nameValuePairs.add(new BasicNameValuePair("device_id", ((TelephonyManager) getSystemService("phone")).getDeviceId()));
        nameValuePairs.add(new BasicNameValuePair("user_level", "0"));
        try {
            restClient.webPost("", nameValuePairs);
        } catch (Exception e) {
            showDialog("Failure", "Transmission failure");
        }
    }

    public void showDialog(String title, String text) {
        AlertDialog.Builder ad = new AlertDialog.Builder(this);
        ad.setTitle(title);
        ad.setMessage(text);
        ad.setPositiveButton("OK", (DialogInterface.OnClickListener) null);
        ad.show();
    }

    public void btnRetry(View v) {
        Global.tracker.trackEvent("Result", "Click", "Result_Retry", 1);
        Global.scene = 2;
        finish();
    }

    public void btnRank(View v) {
        Global.tracker.trackEvent("Result", "Click", "Result_Rank", 1);
        Global.scene = 4;
        finish();
    }

    public void btnMore(View v) {
        Global.tracker.trackEvent("Result", "Click", "Result_More", 1);
        startActivity(new Intent(this, MoreActivity.class));
    }

    public void btnShare(View v) {
        Global.tracker.trackEvent("Title", "Click", "Title_Share", 1);
        new AlertDialog.Builder(this).setTitle("Share").setItems(new CharSequence[]{"Gmail"}, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0:
                        ResultActivity.this.gmail();
                        return;
                    default:
                        return;
                }
            }
        }).show();
    }

    public void gmail() {
        Global.tracker.trackEvent("Title", "Click", "Title_Gmail", 1);
        Intent emailIntent = new Intent("android.intent.action.SEND");
        emailIntent.setType("plain/text");
        emailIntent.putExtra("android.intent.extra.EMAIL", new String[]{""});
        emailIntent.putExtra("android.intent.extra.SUBJECT", Global.GMAIL_SUBJECT);
        emailIntent.putExtra("android.intent.extra.TEXT", Global.GMAIL_TEXT);
        startActivity(Intent.createChooser(emailIntent, "Share"));
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getAction() == 0) {
            switch (event.getKeyCode()) {
                case 4:
                    Global.scene = 1;
                    break;
            }
        }
        return super.dispatchKeyEvent(event);
    }
}
