package com.crossfield.stage;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.view.MotionEvent;
import android.view.SurfaceHolder;

public class MyGLSurfaceView extends GLSurfaceView {
    private float mHeight;
    private MyRenderer mMyRenderer;
    private float mWidth;

    public MyGLSurfaceView(Context context) {
        super(context);
        setFocusable(true);
    }

    public void setRenderer(GLSurfaceView.Renderer renderer) {
        super.setRenderer(renderer);
        this.mMyRenderer = (MyRenderer) renderer;
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        super.surfaceChanged(holder, format, w, h);
        this.mWidth = (float) w;
        this.mHeight = (float) h;
    }

    public boolean onTouchEvent(MotionEvent event) {
        this.mMyRenderer.touched(((event.getX() / this.mWidth) * 2.0f) - 1.0f, ((event.getY() / this.mHeight) * -3.0f) + 1.5f);
        return false;
    }
}
