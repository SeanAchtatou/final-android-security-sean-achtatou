package com.crossfield.stage;

import java.nio.FloatBuffer;
import javax.microedition.khronos.opengles.GL10;

public class ParticleSystem {
    public final int mCapacity;
    private Particle[] mParticles = new Particle[this.mCapacity];

    public ParticleSystem(int capacity, int particleLifeSpan) {
        this.mCapacity = capacity;
        Particle[] particles = this.mParticles;
        for (int i = 0; i < this.mCapacity; i++) {
            particles[i] = new Particle();
            particles[i].mLifeSpan = particleLifeSpan;
        }
    }

    public void add(float x, float y, float size, float moveX, float moveY) {
        Particle[] particles = this.mParticles;
        for (int i = 0; i < this.mCapacity; i++) {
            if (!particles[i].mIsActive) {
                particles[i].mIsActive = true;
                particles[i].mX = x;
                particles[i].mY = y;
                particles[i].mSize = size;
                particles[i].mMoveX = moveX;
                particles[i].mMoveY = moveY;
                particles[i].mFrameNumber = 0;
                return;
            }
        }
    }

    public void draw(GL10 gl, int texture) {
        float alpha;
        Particle[] particles = this.mParticles;
        float[] vertices = new float[(this.mCapacity * 12)];
        float[] colors = new float[(this.mCapacity * 24)];
        float[] coords = new float[(this.mCapacity * 12)];
        int vertexIndex = 0;
        int colorIndex = 0;
        int texCoordIndex = 0;
        int activeParticleCount = 0;
        for (int i = 0; i < this.mCapacity; i++) {
            if (particles[i].mIsActive) {
                float centerX = particles[i].mX;
                float centerY = particles[i].mY;
                float size = particles[i].mSize;
                float vLeft = (-0.5f * size) + centerX;
                float vRight = (0.5f * size) + centerX;
                float vTop = (0.5f * size) + centerY;
                float vBottom = (-0.5f * size) + centerY;
                int vertexIndex2 = vertexIndex + 1;
                vertices[vertexIndex] = vLeft;
                int vertexIndex3 = vertexIndex2 + 1;
                vertices[vertexIndex2] = vTop;
                int vertexIndex4 = vertexIndex3 + 1;
                vertices[vertexIndex3] = vRight;
                int vertexIndex5 = vertexIndex4 + 1;
                vertices[vertexIndex4] = vTop;
                int vertexIndex6 = vertexIndex5 + 1;
                vertices[vertexIndex5] = vLeft;
                int vertexIndex7 = vertexIndex6 + 1;
                vertices[vertexIndex6] = vBottom;
                int vertexIndex8 = vertexIndex7 + 1;
                vertices[vertexIndex7] = vRight;
                int vertexIndex9 = vertexIndex8 + 1;
                vertices[vertexIndex8] = vTop;
                int vertexIndex10 = vertexIndex9 + 1;
                vertices[vertexIndex9] = vLeft;
                int vertexIndex11 = vertexIndex10 + 1;
                vertices[vertexIndex10] = vBottom;
                int vertexIndex12 = vertexIndex11 + 1;
                vertices[vertexIndex11] = vRight;
                vertexIndex = vertexIndex12 + 1;
                vertices[vertexIndex12] = vBottom;
                float lifePercentage = ((float) particles[i].mFrameNumber) / ((float) particles[i].mLifeSpan);
                if (lifePercentage <= 0.5f) {
                    alpha = lifePercentage * 2.0f;
                } else {
                    alpha = 1.0f - ((lifePercentage - 0.5f) * 2.0f);
                }
                int colorIndex2 = colorIndex;
                for (int j = 0; j < 6; j++) {
                    int colorIndex3 = colorIndex2 + 1;
                    colors[colorIndex2] = 1.0f;
                    int colorIndex4 = colorIndex3 + 1;
                    colors[colorIndex3] = 1.0f;
                    int colorIndex5 = colorIndex4 + 1;
                    colors[colorIndex4] = 1.0f;
                    colorIndex2 = colorIndex5 + 1;
                    colors[colorIndex5] = alpha;
                }
                int texCoordIndex2 = texCoordIndex + 1;
                coords[texCoordIndex] = 0.0f;
                int texCoordIndex3 = texCoordIndex2 + 1;
                coords[texCoordIndex2] = 0.0f;
                int texCoordIndex4 = texCoordIndex3 + 1;
                coords[texCoordIndex3] = 1.0f;
                int texCoordIndex5 = texCoordIndex4 + 1;
                coords[texCoordIndex4] = 0.0f;
                int texCoordIndex6 = texCoordIndex5 + 1;
                coords[texCoordIndex5] = 0.0f;
                int texCoordIndex7 = texCoordIndex6 + 1;
                coords[texCoordIndex6] = 1.0f;
                int texCoordIndex8 = texCoordIndex7 + 1;
                coords[texCoordIndex7] = 1.0f;
                int texCoordIndex9 = texCoordIndex8 + 1;
                coords[texCoordIndex8] = 0.0f;
                int texCoordIndex10 = texCoordIndex9 + 1;
                coords[texCoordIndex9] = 0.0f;
                int texCoordIndex11 = texCoordIndex10 + 1;
                coords[texCoordIndex10] = 1.0f;
                int texCoordIndex12 = texCoordIndex11 + 1;
                coords[texCoordIndex11] = 1.0f;
                texCoordIndex = texCoordIndex12 + 1;
                coords[texCoordIndex12] = 1.0f;
                activeParticleCount++;
                colorIndex = colorIndex2;
            }
        }
        FloatBuffer verticesBuffer = GraphicUtil.makeFloatBuffer(vertices);
        FloatBuffer colorBuffer = GraphicUtil.makeFloatBuffer(colors);
        FloatBuffer coordBuffer = GraphicUtil.makeFloatBuffer(coords);
        gl.glEnable(3553);
        gl.glBindTexture(3553, texture);
        gl.glVertexPointer(2, 5126, 0, verticesBuffer);
        gl.glEnableClientState(32884);
        gl.glColorPointer(4, 5126, 0, colorBuffer);
        gl.glEnableClientState(32886);
        gl.glTexCoordPointer(2, 5126, 0, coordBuffer);
        gl.glEnableClientState(32888);
        gl.glDrawArrays(4, 0, activeParticleCount * 6);
        gl.glDisableClientState(32888);
        gl.glDisable(3553);
    }

    public void update() {
        Particle[] particles = this.mParticles;
        for (int i = 0; i < this.mCapacity; i++) {
            if (particles[i].mIsActive) {
                particles[i].update();
            }
        }
    }
}
