package com.crossfield.stage;

import javax.microedition.khronos.opengles.GL10;

public class Particle {
    public int mFrameNumber = 0;
    public boolean mIsActive = false;
    public int mLifeSpan = 60;
    public float mMoveX = 0.0f;
    public float mMoveY = 0.0f;
    public float mSize = 1.0f;
    public float mX = 0.0f;
    public float mY = 0.0f;

    public void draw(GL10 gl, int texture) {
        float alpha;
        float lifePercentage = ((float) this.mFrameNumber) / ((float) this.mLifeSpan);
        if (lifePercentage <= 0.5f) {
            alpha = lifePercentage * 2.0f;
        } else {
            alpha = 1.0f - ((lifePercentage - 0.5f) * 2.0f);
        }
        GraphicUtil.drawTexture(gl, this.mX, this.mY, this.mSize, this.mSize, texture, 1.0f, 1.0f, 1.0f, alpha);
    }

    public void update() {
        this.mFrameNumber++;
        if (this.mFrameNumber >= this.mLifeSpan) {
            this.mIsActive = false;
        }
        this.mX += this.mMoveX;
        this.mY += this.mMoveY;
    }
}
