package com.crossfield.stage;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLUtils;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import javax.microedition.khronos.opengles.GL10;

public class GraphicUtil {
    private static final BitmapFactory.Options options = new BitmapFactory.Options();

    /* JADX INFO: Multiple debug info for r21v2 float: [D('r' float), D('totalWidth' float)] */
    /* JADX INFO: Multiple debug info for r14v1 float: [D('x' float), D('rightX' float)] */
    /* JADX INFO: Multiple debug info for r14v2 float: [D('fig1X' float), D('rightX' float)] */
    public static final void drawNumbers(GL10 gl, float x, float y, float width, float height, int texture, int number, int figures, float r, float g, float b, float a) {
        float rightX = (x + ((((float) figures) * width) * 0.5f)) - (0.5f * width);
        for (int i = 0; i < figures; i++) {
            int numberToDraw = number % 10;
            number /= 10;
            drawNumber(gl, rightX - (((float) i) * width), y, width, height, texture, numberToDraw, 1.0f, 1.0f, 1.0f, 1.0f);
        }
    }

    public static final void drawNumber(GL10 gl, float x, float y, float w, float h, int texture, int number, float r, float g, float b, float a) {
        drawTexture(gl, x, y, w, h, texture, ((float) (number % 4)) * 0.25f, ((float) (number / 4)) * 0.25f, 0.25f, 0.25f, r, g, b, a);
    }

    /* JADX INFO: Multiple debug info for r4v5 java.nio.FloatBuffer: [D('colors' float[]), D('polygonColors' java.nio.FloatBuffer)] */
    /* JADX INFO: Multiple debug info for r5v20 java.nio.FloatBuffer: [D('coords' float[]), D('texCoords' java.nio.FloatBuffer)] */
    public static final void drawTexture(GL10 gl, float x, float y, float width, float height, int texture, float u, float v, float tex_w, float tex_h, float r, float g, float b, float a) {
        float[] vertices = {(-0.5f * width) + x, (-0.5f * height) + y, (0.5f * width) + x, (-0.5f * height) + y, (-0.5f * width) + x, (0.5f * height) + y, x + (width * 0.5f), y + (0.5f * height)};
        FloatBuffer polygonVertices = makeFloatBuffer(vertices);
        FloatBuffer polygonColors = makeFloatBuffer(new float[]{r, g, b, a, r, g, b, a, r, g, b, a, r, g, b, a});
        FloatBuffer texCoords = makeFloatBuffer(new float[]{u, v + tex_h, u + tex_w, v + tex_h, u, v, u + tex_w, v});
        gl.glEnable(3553);
        gl.glBindTexture(3553, texture);
        gl.glVertexPointer(2, 5126, 0, polygonVertices);
        gl.glEnableClientState(32884);
        gl.glColorPointer(4, 5126, 0, polygonColors);
        gl.glEnableClientState(32886);
        gl.glTexCoordPointer(2, 5126, 0, texCoords);
        gl.glEnableClientState(32888);
        gl.glDrawArrays(5, 0, 4);
        gl.glDisableClientState(32888);
        gl.glDisable(3553);
    }

    public static final void drawTexture(GL10 gl, float x, float y, float width, float height, int texture, float r, float g, float b, float a) {
        drawTexture(gl, x, y, width, height, texture, 0.0f, 0.0f, 1.0f, 1.0f, r, g, b, a);
    }

    public static final int loadTexture(GL10 gl, Resources resources, int resId) {
        int[] textures = new int[1];
        Bitmap bmp = BitmapFactory.decodeResource(resources, resId, options);
        if (bmp == null) {
            return 0;
        }
        gl.glGenTextures(1, textures, 0);
        gl.glBindTexture(3553, textures[0]);
        GLUtils.texImage2D(3553, 0, bmp, 0);
        gl.glTexParameterf(3553, 10241, 9729.0f);
        gl.glTexParameterf(3553, 10240, 9729.0f);
        gl.glBindTexture(3553, 0);
        bmp.recycle();
        TextureManager.addTexture(resId, textures[0]);
        return textures[0];
    }

    static {
        options.inScaled = false;
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
    }

    /* JADX INFO: Multiple debug info for r14v1 java.nio.FloatBuffer: [D('polygonVertices' java.nio.FloatBuffer), D('x' float)] */
    /* JADX INFO: Multiple debug info for r6v8 int: [D('theta1' float), D('vertexId' int)] */
    public static final void drawCircle(GL10 gl, float x, float y, int divides, float radius, float r, float g, float b, float a) {
        float[] vertices = new float[(divides * 3 * 2)];
        int i = 0;
        int vertexId = 0;
        while (i < divides) {
            float theta1 = (2.0f / ((float) divides)) * ((float) i) * 3.1415927f;
            float theta2 = (2.0f / ((float) divides)) * ((float) (i + 1)) * 3.1415927f;
            int vertexId2 = vertexId + 1;
            vertices[vertexId] = x;
            int vertexId3 = vertexId2 + 1;
            vertices[vertexId2] = y;
            int vertexId4 = vertexId3 + 1;
            vertices[vertexId3] = (((float) Math.cos((double) theta1)) * radius) + x;
            int vertexId5 = vertexId4 + 1;
            vertices[vertexId4] = (((float) Math.sin((double) theta1)) * radius) + y;
            int vertexId6 = vertexId5 + 1;
            vertices[vertexId5] = (((float) Math.cos((double) theta2)) * radius) + x;
            vertices[vertexId6] = (((float) Math.sin((double) theta2)) * radius) + y;
            i++;
            vertexId = vertexId6 + 1;
        }
        FloatBuffer polygonVertices = makeFloatBuffer(vertices);
        gl.glColor4f(r, g, b, a);
        gl.glDisableClientState(32886);
        gl.glVertexPointer(2, 5126, 0, polygonVertices);
        gl.glEnableClientState(32884);
        gl.glDrawArrays(4, 0, divides * 3);
    }

    /* JADX INFO: Multiple debug info for r4v5 java.nio.FloatBuffer: [D('colors' float[]), D('polygonColors' java.nio.FloatBuffer)] */
    public static final void drawRectangle(GL10 gl, float x, float y, float width, float height, float r, float g, float b, float a) {
        float[] vertices = {(-0.5f * width) + x, (-0.5f * height) + y, (0.5f * width) + x, (-0.5f * height) + y, (-0.5f * width) + x, (0.5f * height) + y, x + (width * 0.5f), y + (0.5f * height)};
        FloatBuffer polygonVertices = makeFloatBuffer(vertices);
        FloatBuffer polygonColors = makeFloatBuffer(new float[]{r, g, b, a, r, g, b, a, r, g, b, a, r, g, b, a});
        gl.glVertexPointer(2, 5126, 0, polygonVertices);
        gl.glEnableClientState(32884);
        gl.glColorPointer(4, 5126, 0, polygonColors);
        gl.glEnableClientState(32886);
        gl.glDrawArrays(5, 0, 4);
    }

    public static final void drawSquare(GL10 gl, float x, float y, float r, float g, float b, float a) {
        drawRectangle(gl, x, y, 1.0f, 1.0f, r, g, b, a);
    }

    public static final void drawSquare(GL10 gl, float r, float g, float b, float a) {
        drawSquare(gl, 0.0f, 0.0f, r, g, b, a);
    }

    public static final void drawSquare(GL10 gl) {
        drawSquare(gl, 1.0f, 0.0f, 0.0f, 1.0f);
    }

    public static final FloatBuffer makeFloatBuffer(float[] arr) {
        ByteBuffer bb = ByteBuffer.allocateDirect(arr.length * 4);
        bb.order(ByteOrder.nativeOrder());
        FloatBuffer fb = bb.asFloatBuffer();
        fb.put(arr);
        fb.position(0);
        return fb;
    }
}
