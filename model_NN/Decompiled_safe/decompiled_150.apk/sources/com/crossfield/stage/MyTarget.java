package com.crossfield.stage;

import javax.microedition.khronos.opengles.GL10;

public class MyTarget {
    public float mAngle;
    public float mSize;
    public float mSpeed;
    public float mTurnAngle;
    public float mX;
    public float mY;

    public MyTarget(float x, float y, float angle, float size, float speed, float turnAngle) {
        this.mX = x;
        this.mY = y;
        this.mAngle = angle;
        this.mSize = size;
        this.mSpeed = speed;
        this.mTurnAngle = turnAngle;
    }

    public void move() {
        float theta = (this.mAngle / 180.0f) * 3.1415927f;
        this.mX += ((float) Math.cos((double) theta)) * this.mSpeed;
        this.mY += ((float) Math.sin((double) theta)) * this.mSpeed;
        if (this.mX >= 2.0f) {
            this.mX -= 4.0f;
        }
        if (this.mX <= -2.0f) {
            this.mX += 4.0f;
        }
        if (this.mY >= 2.5f) {
            this.mY -= 5.0f;
        }
        if (this.mY <= -2.5f) {
            this.mY += 5.0f;
        }
    }

    public boolean isPointInside(float x, float y) {
        float dx = x - this.mX;
        float dy = y - this.mY;
        if (((float) Math.sqrt((double) ((dx * dx) + (dy * dy)))) <= this.mSize * 0.5f) {
            return true;
        }
        return false;
    }

    public void draw(GL10 gl, int texture) {
        gl.glPushMatrix();
        gl.glTranslatef(this.mX, this.mY, 0.0f);
        gl.glRotatef(this.mAngle, 0.0f, 0.0f, 1.0f);
        gl.glScalef(this.mSize, this.mSize, 1.0f);
        GraphicUtil.drawTexture(gl, 0.0f, 0.0f, 1.0f, 1.0f, texture, 1.0f, 1.0f, 1.0f, 1.0f);
        gl.glPopMatrix();
    }
}
