package com.crossfield.stage;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import com.crossfield.taphunt.Global;
import com.crossfield.taphunt.R;

public class StageActivity extends Activity {
    private MyBgm mBgm;
    private long mPauseTime = 0;
    /* access modifiers changed from: private */
    public MyRenderer mRenderer;
    private Button mRetryButton;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(1024);
        requestWindowFeature(1);
        setVolumeControlStream(3);
        setContentView((int) R.layout.stage);
        Global.stageActivity = this;
        this.mRenderer = new MyRenderer(this);
        MyGLSurfaceView glSurfaceView = new MyGLSurfaceView(this);
        glSurfaceView.setRenderer(this.mRenderer);
        ((LinearLayout) findViewById(R.id.linearLayout_GlView)).addView(glSurfaceView);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(-2, -2);
        params.gravity = 17;
        params.setMargins(0, 150, 0, 0);
        this.mRetryButton = new Button(this);
        this.mRetryButton.setText("Retry");
        hideRetryButton();
        addContentView(this.mRetryButton, params);
        this.mRetryButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                StageActivity.this.hideRetryButton();
                StageActivity.this.mRenderer.startNewGame();
            }
        });
        this.mBgm = new MyBgm(this);
        if (savedInstanceState != null) {
            long startTime = savedInstanceState.getLong("startTime");
            long pauseTime = savedInstanceState.getLong("pauseTime");
            int score = savedInstanceState.getInt("score");
            this.mRenderer.subtractPausedTime(-(pauseTime - startTime));
            this.mRenderer.setScore(score);
        }
    }

    public void showRetryButton() {
        this.mRetryButton.setVisibility(0);
    }

    public void hideRetryButton() {
        this.mRetryButton.setVisibility(4);
    }

    public void onResume() {
        super.onResume();
        this.mBgm.start();
        if (this.mPauseTime != 0) {
            this.mRenderer.subtractPausedTime(System.currentTimeMillis() - this.mPauseTime);
        }
    }

    public void onPause() {
        super.onPause();
        this.mBgm.stop();
        TextureManager.deleteAll(Global.gl);
        this.mPauseTime = System.currentTimeMillis();
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putLong("startTime", this.mRenderer.getStartTime());
        outState.putLong("pauseTime", System.currentTimeMillis());
        outState.putInt("score", this.mRenderer.getScore());
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getAction() == 0) {
            switch (event.getKeyCode()) {
                case 4:
                    Global.scene = 1;
                    break;
            }
        }
        return super.dispatchKeyEvent(event);
    }
}
