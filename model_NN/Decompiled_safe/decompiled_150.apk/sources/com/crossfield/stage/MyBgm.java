package com.crossfield.stage;

import android.content.Context;
import android.media.MediaPlayer;
import com.crossfield.taphunt.R;

public class MyBgm {
    private MediaPlayer mBgm;

    public MyBgm(Context context) {
        this.mBgm = MediaPlayer.create(context, (int) R.raw.master1);
        this.mBgm.setLooping(true);
        this.mBgm.setVolume(1.0f, 1.0f);
    }

    public void start() {
    }

    public void stop() {
    }
}
