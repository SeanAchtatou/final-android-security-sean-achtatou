package com.crossfield.stage;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.opengl.GLSurfaceView;
import android.os.Handler;
import android.util.Log;
import com.crossfield.taphunt.Global;
import com.crossfield.taphunt.R;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class MyRenderer implements GLSurfaceView.Renderer {
    private static final int GAME_INTERVAL = 30;
    public static final int TARGET_NUM = 20;
    private int mBgTexture;
    private Context mContext;
    private boolean mGameOverFlag;
    private int mGameOverTexture;
    private Handler mHandler = new Handler();
    private int mHeight;
    private int mNumberTexture;
    private ParticleSystem mParticleSystem;
    private int mParticleTexture;
    private int mScore;
    private MySe mSe;
    private long mStartTime;
    private int mTargetTexture;
    private MyTarget[] mTargets = new MyTarget[20];
    private int mWidth;

    public MyRenderer(Context context) {
        this.mContext = context;
        this.mParticleSystem = new ParticleSystem(300, 20);
        this.mSe = new MySe(context);
        startNewGame();
    }

    public void startNewGame() {
        Random rand = Global.rand;
        for (int i = 0; i < 20; i++) {
            this.mTargets[i] = new MyTarget((rand.nextFloat() * 2.0f) - 1.0f, (rand.nextFloat() * 2.0f) - 1.0f, (float) rand.nextInt(360), (rand.nextFloat() * 0.25f) + 0.25f, (rand.nextFloat() * 0.01f) + 0.01f, (rand.nextFloat() * 4.0f) - 2.0f);
        }
        this.mScore = 0;
        this.mStartTime = System.currentTimeMillis();
        this.mGameOverFlag = false;
    }

    public void renderMain(GL10 gl) {
        int remainTime = GAME_INTERVAL - (((int) (System.currentTimeMillis() - this.mStartTime)) / 1000);
        if (remainTime <= 0) {
            remainTime = 0;
            if (!this.mGameOverFlag) {
                this.mGameOverFlag = true;
            }
        }
        Random rand = Global.rand;
        MyTarget[] targets = this.mTargets;
        for (int i = 0; i < 20; i++) {
            if (rand.nextInt(100) == 0) {
                targets[i].mTurnAngle = (rand.nextFloat() * 4.0f) - 2.0f;
            }
            targets[i].mAngle += targets[i].mTurnAngle;
            targets[i].move();
        }
        GraphicUtil.drawTexture(gl, 0.0f, 0.0f, 2.0f, 3.0f, this.mBgTexture, 1.0f, 1.0f, 1.0f, 1.0f);
        this.mParticleSystem.update();
        gl.glEnable(3042);
        gl.glBlendFunc(770, 1);
        this.mParticleSystem.draw(gl, this.mParticleTexture);
        for (int i2 = 0; i2 < 20; i2++) {
            targets[i2].draw(gl, this.mTargetTexture);
        }
        gl.glDisable(3042);
        GraphicUtil.drawNumbers(gl, -0.5f, 1.25f, 0.125f, 0.125f, this.mNumberTexture, this.mScore, 8, 1.0f, 1.0f, 1.0f, 1.0f);
        GraphicUtil.drawNumbers(gl, 0.5f, 1.2f, 0.4f, 0.4f, this.mNumberTexture, remainTime, 2, 1.0f, 1.0f, 1.0f, 1.0f);
        if (this.mGameOverFlag) {
            gameOver();
        }
    }

    public void onDrawFrame(GL10 gl) {
        gl.glViewport(0, 0, this.mWidth, this.mHeight);
        gl.glMatrixMode(5889);
        gl.glLoadIdentity();
        gl.glOrthof(-1.0f, 1.0f, -1.5f, 1.5f, 0.5f, -0.5f);
        gl.glMatrixMode(5888);
        gl.glLoadIdentity();
        gl.glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
        gl.glClear(16384);
        renderMain(gl);
    }

    private void loadTextures(GL10 gl) {
        Resources res = this.mContext.getResources();
        this.mBgTexture = GraphicUtil.loadTexture(gl, res, R.drawable.circuit);
        if (this.mBgTexture == 0) {
            Log.e(getClass().toString(), "load texture error! circuit");
        }
        this.mTargetTexture = GraphicUtil.loadTexture(gl, res, R.drawable.fly);
        if (this.mTargetTexture == 0) {
            Log.e(getClass().toString(), "load texture error! fly");
        }
        this.mNumberTexture = GraphicUtil.loadTexture(gl, res, R.drawable.number_texture);
        if (this.mNumberTexture == 0) {
            Log.e(getClass().toString(), "load texture error! number_texture");
        }
        this.mGameOverTexture = GraphicUtil.loadTexture(gl, res, R.drawable.game_over);
        if (this.mGameOverTexture == 0) {
            Log.e(getClass().toString(), "load texture error! game_over");
        }
        this.mParticleTexture = GraphicUtil.loadTexture(gl, res, R.drawable.particle_blue);
        if (this.mParticleTexture == 0) {
            Log.e(getClass().toString(), "load texture error! particle_blue");
        }
    }

    public void onSurfaceChanged(GL10 gl, int width, int height) {
        this.mWidth = width;
        this.mHeight = height;
        Global.gl = gl;
        loadTextures(gl);
    }

    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
    }

    public void touched(float x, float y) {
        Log.i(getClass().toString(), String.format("touched! x = %f, y = %f", Float.valueOf(x), Float.valueOf(y)));
        MyTarget[] targets = this.mTargets;
        Random rand = Global.rand;
        if (!this.mGameOverFlag) {
            for (int i = 0; i < 20; i++) {
                if (targets[i].isPointInside(x, y)) {
                    for (int j = 0; j < 40; j++) {
                        this.mParticleSystem.add(targets[i].mX, targets[i].mY, 0.2f, (rand.nextFloat() - 0.5f) * 0.05f, (rand.nextFloat() - 0.5f) * 0.05f);
                    }
                    float theta = (((float) Global.rand.nextInt(360)) / 180.0f) * 3.1415927f;
                    targets[i].mX = ((float) Math.cos((double) theta)) * 2.0f;
                    targets[i].mY = ((float) Math.sin((double) theta)) * 2.0f;
                    this.mScore++;
                    Log.i(getClass().toString(), "score = " + this.mScore);
                    this.mSe.playHitSound();
                }
            }
        }
    }

    public void gameOver() {
        if (Global.scene != 3) {
            Global.score = this.mScore;
            long today = (long) Math.floor((double) (new Date().getTime() / 86400000));
            int heighScore = 0;
            int todayScore = 0;
            Cursor cursor = Global.dbAdapter.getAllScore();
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                for (int i = 0; i < cursor.getCount(); i++) {
                    int score = Integer.parseInt(cursor.getString(2));
                    if (score > heighScore) {
                        heighScore = score;
                    }
                    try {
                        String daily = cursor.getString(4);
                        if (((long) Math.floor((double) (new SimpleDateFormat("yyyy-MM-dd").parse(daily.substring(0, 10)).getTime() / 86400000))) == today - 1 && todayScore < score) {
                            todayScore = score;
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    cursor.moveToNext();
                }
                if (!(this.mScore <= todayScore || todayScore == heighScore || Global.newRecord == 1)) {
                    Global.newRecord = 2;
                }
                if (this.mScore > heighScore) {
                    Global.newRecord = 1;
                }
            } else {
                Global.newRecord = 1;
            }
            String value = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
            Global.country = Global.stageActivity.getResources().getConfiguration().locale.getCountry();
            Global.dbAdapter.addScore(Global.name, (float) this.mScore, 0, value, Global.country);
            Global.scene = 3;
            Global.stageActivity.finish();
        }
    }

    public static Date toDate(String str) throws ParseException {
        return DateFormat.getDateInstance().parse(str);
    }

    public void subtractPausedTime(long pausedTime) {
        this.mStartTime += pausedTime;
    }

    public long getStartTime() {
        return this.mStartTime;
    }

    public int getScore() {
        return this.mScore;
    }

    public void setScore(int score) {
        this.mScore = score;
    }
}
