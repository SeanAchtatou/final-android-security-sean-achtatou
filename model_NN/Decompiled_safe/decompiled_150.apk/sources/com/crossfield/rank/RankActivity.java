package com.crossfield.rank;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.telephony.TelephonyManager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.crossfield.android.common.database.model.UserPointDto;
import com.crossfield.android.utility.Constants;
import com.crossfield.android.utility.RestWebServiceClient;
import com.crossfield.more.MoreActivity;
import com.crossfield.taphunt.Global;
import com.crossfield.taphunt.R;
import com.google.gson.Gson;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

public class RankActivity extends Activity implements Runnable {
    public static final int ALL = 3;
    public static final int DAILY = 0;
    public static final int DOMESTIC = 1;
    public static final int MONTHLY = 2;
    public static final int PRIVATE = 0;
    public static String SCORE_UNIT = "pt";
    public static final int WEEKLY = 1;
    public static final int WORLD = 2;
    /* access modifiers changed from: private */
    public static ProgressDialog waitDialog;
    TextView area;
    private int btn = 0;
    private String category;
    private String country;
    private Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            RankActivity.this.loadAction();
            RankActivity.waitDialog.dismiss();
            RankActivity.waitDialog = null;
        }
    };
    private int radio = 0;
    RadioGroup radioGroup_interval;
    TextView rank;
    private Thread thread;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().addFlags(1024);
        setContentView((int) R.layout.rank);
        Global.rankActivity = this;
        Global.tracker.trackPageView("Result");
        getPrivateRanking();
        this.radioGroup_interval = (RadioGroup) findViewById(R.id.radioGroup);
        this.radioGroup_interval.check(R.id.radioButton_daily);
        this.area = (TextView) findViewById(R.id.textView_area);
        this.rank = (TextView) findViewById(R.id.textView_currentRank);
    }

    public void getPrivateRanking() {
        List<RankingBean> ranking = new ArrayList<>();
        ListView list = (ListView) findViewById(R.id.listView);
        Cursor cursor = Global.dbAdapter.getAllScore();
        cursor.moveToFirst();
        for (int i = 0; i < cursor.getCount(); i++) {
            ranking.add(setRanking(String.valueOf(i + 1), String.valueOf(cursor.getString(2)) + SCORE_UNIT, cursor.getString(1), cursor.getString(4), cursor.getString(5)));
            cursor.moveToNext();
        }
        ListAdapter adapter = new ListAdapter(getApplicationContext(), ranking);
        if (list != null) {
            list.setAdapter((android.widget.ListAdapter) adapter);
        }
    }

    public void dropView() {
        ((ListView) findViewById(R.id.listView)).setAdapter((android.widget.ListAdapter) new ListAdapter(getApplicationContext(), new ArrayList<>()));
    }

    public RankingBean setRanking(String number, String score, String name, String date, String country2) {
        RankingBean ranking = new RankingBean();
        ranking.setNumber(number);
        ranking.setScore(score);
        ranking.setName(name);
        SimpleDateFormat formatReceive = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            ranking.setDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(formatReceive.parse(date)));
        } catch (ParseException e) {
        }
        ranking.setCountryName(country2);
        return ranking;
    }

    public void dailyRadio(View view) {
        this.radio = 0;
        switch (this.btn) {
            case 0:
            default:
                return;
            case 1:
            case 2:
                populateRankingData(Constants.RANKING_CATEGORY_TODAY);
                return;
        }
    }

    public void weeklyRadio(View view) {
        this.radio = 1;
        switch (this.btn) {
            case 0:
            default:
                return;
            case 1:
            case 2:
                populateRankingData(Constants.RANKING_CATEGORY_WEEKLY);
                return;
        }
    }

    public void monthlyRadio(View view) {
        this.radio = 2;
        switch (this.btn) {
            case 0:
            default:
                return;
            case 1:
            case 2:
                populateRankingData(Constants.RANKING_CATEGORY_MONTHLY);
                return;
        }
    }

    public void allRadio(View view) {
        this.radio = 3;
        switch (this.btn) {
            case 0:
            default:
                return;
            case 1:
            case 2:
                populateRankingData(Constants.RANKING_CATEGORY_ALL);
                return;
        }
    }

    public void btnPrivate(View view) {
        this.btn = 0;
        getPrivateRanking();
        ((TextView) findViewById(R.id.textView_currentRank)).setText("");
        this.country = Global.country;
        this.area.setText("Private");
        String interval = "";
        switch (((RadioButton) findViewById(this.radioGroup_interval.getCheckedRadioButtonId())).getId()) {
            case R.id.radioButton_daily /*2131165192*/:
                interval = Constants.RANKING_CATEGORY_TODAY;
                break;
            case R.id.radioButton_weekly /*2131165193*/:
                interval = Constants.RANKING_CATEGORY_WEEKLY;
                break;
            case R.id.radioButton_monthly /*2131165194*/:
                interval = Constants.RANKING_CATEGORY_MONTHLY;
                break;
            case R.id.radioButton_all /*2131165195*/:
                interval = Constants.RANKING_CATEGORY_ALL;
                break;
        }
        if (this.btn != 0) {
            populateRankingData(interval);
        }
    }

    public void btnDomestic(View view) {
        this.btn = 1;
        this.country = Global.country;
        this.area.setText("Domestic");
        String interval = "";
        switch (((RadioButton) findViewById(this.radioGroup_interval.getCheckedRadioButtonId())).getId()) {
            case R.id.radioButton_daily /*2131165192*/:
                interval = Constants.RANKING_CATEGORY_TODAY;
                break;
            case R.id.radioButton_weekly /*2131165193*/:
                interval = Constants.RANKING_CATEGORY_WEEKLY;
                break;
            case R.id.radioButton_monthly /*2131165194*/:
                interval = Constants.RANKING_CATEGORY_MONTHLY;
                break;
            case R.id.radioButton_all /*2131165195*/:
                interval = Constants.RANKING_CATEGORY_ALL;
                break;
        }
        if (this.btn != 0) {
            populateRankingData(interval);
        }
    }

    public void btnWorld(View view) {
        this.btn = 2;
        this.country = Constants.RANKING_COUNTRY_WORLD;
        this.area.setText("World");
        String interval = "";
        switch (((RadioButton) findViewById(this.radioGroup_interval.getCheckedRadioButtonId())).getId()) {
            case R.id.radioButton_daily /*2131165192*/:
                interval = Constants.RANKING_CATEGORY_TODAY;
                break;
            case R.id.radioButton_weekly /*2131165193*/:
                interval = Constants.RANKING_CATEGORY_WEEKLY;
                break;
            case R.id.radioButton_monthly /*2131165194*/:
                interval = Constants.RANKING_CATEGORY_MONTHLY;
                break;
            case R.id.radioButton_all /*2131165195*/:
                interval = Constants.RANKING_CATEGORY_ALL;
                break;
        }
        if (this.btn != 0) {
            populateRankingData(interval);
        }
    }

    private void populateRankingData(String category2) {
        this.category = category2;
        dropView();
        setWait();
    }

    private void setWait() {
        waitDialog = new ProgressDialog(this);
        waitDialog.setMessage("Loading...");
        waitDialog.setProgressStyle(0);
        waitDialog.show();
        this.thread = new Thread(this);
        this.thread.start();
    }

    public void run() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
        }
        this.handler.sendEmptyMessage(0);
    }

    /* access modifiers changed from: private */
    public void loadAction() {
        if (Global.name.length() != 0) {
            Global.country = getResources().getConfiguration().locale.getCountry();
            RestWebServiceClient restWebServiceClient = new RestWebServiceClient(Constants.URL_GET_RANKING);
            HashMap hashMap = new HashMap();
            hashMap.put("category", this.category);
            hashMap.put("country_code", this.country);
            hashMap.put("user_name", Global.name);
            hashMap.put("device_id", ((TelephonyManager) getSystemService("phone")).getDeviceId());
            try {
                JSONObject object = (JSONObject) new JSONTokener(restWebServiceClient.webGet("", hashMap)).nextValue();
                int userRank = object.getInt("self_rank");
                JSONArray arrayJson = object.getJSONArray("list_rank");
                ArrayList<UserPointDto> arrayList = new ArrayList<>();
                for (int i = 0; i < arrayJson.length(); i++) {
                    arrayList.add((UserPointDto) new Gson().fromJson(arrayJson.getJSONObject(i).toString(), UserPointDto.class));
                }
                TextView currentRank = (TextView) findViewById(R.id.textView_currentRank);
                currentRank.setText("");
                if (userRank != -1) {
                    currentRank.setText(String.valueOf(userRank) + "th");
                    if (userRank == 1) {
                        currentRank.setText(String.valueOf(userRank) + "st");
                    }
                    if (userRank == 2) {
                        currentRank.setText(String.valueOf(userRank) + "nd");
                    }
                    if (userRank == 3) {
                        currentRank.setText(String.valueOf(userRank) + "rd");
                    }
                }
                if (userRank == -1) {
                    currentRank.setText("");
                }
                List<RankingBean> ranking = new ArrayList<>();
                ListView listView = null;
                switch (this.btn) {
                    case 1:
                        listView = (ListView) findViewById(R.id.listView);
                        break;
                    case 2:
                        listView = (ListView) findViewById(R.id.listView);
                        break;
                }
                int num = 0;
                for (UserPointDto dto : arrayList) {
                    num++;
                    ranking.add(setRanking(String.valueOf(num), String.valueOf(String.valueOf((int) dto.getUserPoint())) + SCORE_UNIT, dto.getUserName(), dto.getInsertTime(), dto.getCountryCode()));
                }
                ListAdapter listAdapter = new ListAdapter(getApplicationContext(), ranking);
                if (listView != null) {
                    listView.setAdapter((android.widget.ListAdapter) listAdapter);
                }
            } catch (Exception e) {
                showDialog("Failure", "Connection Failure!");
            }
        }
    }

    public void showDialog(String title, String text) {
        AlertDialog.Builder ad = new AlertDialog.Builder(this);
        ad.setTitle(title);
        ad.setMessage(text);
        ad.setPositiveButton("OK", (DialogInterface.OnClickListener) null);
        ad.show();
    }

    public void btnTitle(View v) {
        finish();
        Global.scene = 1;
    }

    public void btnMore(View v) {
        Global.tracker.trackEvent("Rank", "Click", "Rank_More", 1);
        startActivity(new Intent(this, MoreActivity.class));
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getAction() == 0) {
            switch (event.getKeyCode()) {
                case 4:
                    Global.scene = 1;
                    break;
            }
        }
        return super.dispatchKeyEvent(event);
    }

    public class ListAdapter extends ArrayAdapter<RankingBean> {
        private ImageView countryImage;
        private TextView countryName;
        private TextView date;
        private LayoutInflater mInflater;
        private TextView name;
        private TextView number;
        private TextView score;

        public ListAdapter(Context context, List<RankingBean> objects) {
            super(context, 0, objects);
            this.mInflater = (LayoutInflater) context.getSystemService("layout_inflater");
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = this.mInflater.inflate((int) R.layout.rank_list, (ViewGroup) null);
            }
            RankingBean ranking = (RankingBean) getItem(position);
            if (ranking != null) {
                this.number = (TextView) convertView.findViewById(R.id.textView_number);
                this.number.setText(ranking.getNumber());
                this.name = (TextView) convertView.findViewById(R.id.textView_name);
                this.name.setText(ranking.getName());
                this.score = (TextView) convertView.findViewById(R.id.textView_score);
                this.score.setText(ranking.getScore());
                this.countryName = (TextView) convertView.findViewById(R.id.textView_country);
                this.countryName.setText(ranking.getCountryName());
                this.countryImage = (ImageView) convertView.findViewById(R.id.imageView_country);
                try {
                    int id = flagSellect(ranking.getCountryName());
                    if (id != -1) {
                        this.countryImage.setImageDrawable(RankActivity.this.getResources().getDrawable(id));
                    }
                } catch (Exception e) {
                }
                this.date = (TextView) convertView.findViewById(R.id.textView_date);
                this.date.setText(ranking.getDate());
            }
            return convertView;
        }

        /* JADX INFO: additional move instructions added (1) to help type inference */
        public int flagSellect(String flag) {
            int id = -1;
            if (flag.equalsIgnoreCase("IS")) {
                id = R.drawable.is;
            }
            int id2 = id;
            if (flag.equalsIgnoreCase("IE")) {
                id2 = R.drawable.ie;
            }
            int id3 = id2;
            if (flag.equalsIgnoreCase("AZ")) {
                id3 = R.drawable.az;
            }
            int id4 = id3;
            if (flag.equalsIgnoreCase("AF")) {
                id4 = R.drawable.af;
            }
            int id5 = id4;
            if (flag.equalsIgnoreCase("US")) {
                id5 = R.drawable.us;
            }
            int id6 = id5;
            if (flag.equalsIgnoreCase("VI")) {
                id6 = R.drawable.vi;
            }
            int id7 = id6;
            if (flag.equalsIgnoreCase("AS")) {
                id7 = R.drawable.as;
            }
            int id8 = id7;
            if (flag.equalsIgnoreCase("AE")) {
                id8 = R.drawable.ae;
            }
            int id9 = id8;
            if (flag.equalsIgnoreCase("DZ")) {
                id9 = R.drawable.dz;
            }
            int id10 = id9;
            if (flag.equalsIgnoreCase("AR")) {
                id10 = R.drawable.ar;
            }
            int id11 = id10;
            if (flag.equalsIgnoreCase("AW")) {
                id11 = R.drawable.aw;
            }
            int id12 = id11;
            if (flag.equalsIgnoreCase("AL")) {
                id12 = R.drawable.al;
            }
            int id13 = id12;
            if (flag.equalsIgnoreCase("AM")) {
                id13 = R.drawable.am;
            }
            int id14 = id13;
            if (flag.equalsIgnoreCase("AI")) {
                id14 = R.drawable.ai;
            }
            int id15 = id14;
            if (flag.equalsIgnoreCase("AO")) {
                id15 = R.drawable.ao;
            }
            int id16 = id15;
            if (flag.equalsIgnoreCase("AG")) {
                id16 = R.drawable.ag;
            }
            int id17 = id16;
            if (flag.equalsIgnoreCase("AD")) {
                id17 = R.drawable.ad;
            }
            int id18 = id17;
            if (flag.equalsIgnoreCase("YE")) {
                id18 = R.drawable.ye;
            }
            int id19 = id18;
            if (flag.equalsIgnoreCase("GB")) {
                id19 = R.drawable.gb;
            }
            int id20 = id19;
            if (flag.equalsIgnoreCase("IO")) {
                id20 = R.drawable.io;
            }
            int id21 = id20;
            if (flag.equalsIgnoreCase("VG")) {
                id21 = R.drawable.vg;
            }
            int id22 = id21;
            if (flag.equalsIgnoreCase("IL")) {
                id22 = R.drawable.il;
            }
            int id23 = id22;
            if (flag.equalsIgnoreCase("IT")) {
                id23 = R.drawable.it;
            }
            int id24 = id23;
            if (flag.equalsIgnoreCase("IQ")) {
                id24 = R.drawable.iq;
            }
            int id25 = id24;
            if (flag.equalsIgnoreCase("IR")) {
                id25 = R.drawable.ir;
            }
            int id26 = id25;
            if (flag.equalsIgnoreCase("IN")) {
                id26 = R.drawable.in;
            }
            int id27 = id26;
            if (flag.equalsIgnoreCase("ID")) {
                id27 = R.drawable.id;
            }
            int id28 = id27;
            if (flag.equalsIgnoreCase("UG")) {
                id28 = R.drawable.ug;
            }
            int id29 = id28;
            if (flag.equalsIgnoreCase("UA")) {
                id29 = R.drawable.ua;
            }
            int id30 = id29;
            if (flag.equalsIgnoreCase("UZ")) {
                id30 = R.drawable.uz;
            }
            int id31 = id30;
            if (flag.equalsIgnoreCase("UY")) {
                id31 = R.drawable.uy;
            }
            int id32 = id31;
            if (flag.equalsIgnoreCase("EC")) {
                id32 = R.drawable.ec;
            }
            int id33 = id32;
            if (flag.equalsIgnoreCase("EE")) {
                id33 = R.drawable.ee;
            }
            int id34 = id33;
            if (flag.equalsIgnoreCase("ET")) {
                id34 = R.drawable.et;
            }
            int id35 = id34;
            if (flag.equalsIgnoreCase("ER")) {
                id35 = R.drawable.er;
            }
            int id36 = id35;
            if (flag.equalsIgnoreCase("SV")) {
                id36 = R.drawable.sv;
            }
            int id37 = id36;
            if (flag.equalsIgnoreCase("AU")) {
                id37 = R.drawable.au;
            }
            int id38 = id37;
            if (flag.equalsIgnoreCase("AT")) {
                id38 = R.drawable.at;
            }
            int id39 = id38;
            if (flag.equalsIgnoreCase("OM")) {
                id39 = R.drawable.om;
            }
            int id40 = id39;
            if (flag.equalsIgnoreCase("NL")) {
                id40 = R.drawable.nl;
            }
            int id41 = id40;
            if (flag.equalsIgnoreCase("AN")) {
                id41 = R.drawable.an;
            }
            int id42 = id41;
            if (flag.equalsIgnoreCase("GH")) {
                id42 = R.drawable.gh;
            }
            int id43 = id42;
            if (flag.equalsIgnoreCase("GG")) {
                id43 = R.drawable.gg;
            }
            int id44 = id43;
            if (flag.equalsIgnoreCase("GY")) {
                id44 = R.drawable.gy;
            }
            int id45 = id44;
            if (flag.equalsIgnoreCase("KZ")) {
                id45 = R.drawable.kz;
            }
            int id46 = id45;
            if (flag.equalsIgnoreCase("QA")) {
                id46 = R.drawable.qa;
            }
            int id47 = id46;
            if (flag.equalsIgnoreCase("CA")) {
                id47 = R.drawable.ca;
            }
            int id48 = id47;
            if (flag.equalsIgnoreCase("CV")) {
                id48 = R.drawable.cv;
            }
            int id49 = id48;
            if (flag.equalsIgnoreCase("GA")) {
                id49 = R.drawable.ga;
            }
            int id50 = id49;
            if (flag.equalsIgnoreCase("CM")) {
                id50 = R.drawable.cm;
            }
            int id51 = id50;
            if (flag.equalsIgnoreCase("GM")) {
                id51 = R.drawable.gm;
            }
            int id52 = id51;
            if (flag.equalsIgnoreCase("KH")) {
                id52 = R.drawable.kh;
            }
            int id53 = id52;
            if (flag.equalsIgnoreCase("MP")) {
                id53 = R.drawable.mp;
            }
            int id54 = id53;
            if (flag.equalsIgnoreCase("GN")) {
                id54 = R.drawable.gn;
            }
            int id55 = id54;
            if (flag.equalsIgnoreCase("GW")) {
                id55 = R.drawable.gw;
            }
            int id56 = id55;
            if (flag.equalsIgnoreCase("CY")) {
                id56 = R.drawable.cy;
            }
            int id57 = id56;
            if (flag.equalsIgnoreCase("CU")) {
                id57 = R.drawable.cu;
            }
            int id58 = id57;
            if (flag.equalsIgnoreCase("GR")) {
                id58 = R.drawable.gr;
            }
            int id59 = id58;
            if (flag.equalsIgnoreCase("KI")) {
                id59 = R.drawable.ki;
            }
            int id60 = id59;
            if (flag.equalsIgnoreCase("KG")) {
                id60 = R.drawable.kg;
            }
            int id61 = id60;
            if (flag.equalsIgnoreCase("GT")) {
                id61 = R.drawable.gt;
            }
            int id62 = id61;
            if (flag.equalsIgnoreCase("GU")) {
                id62 = R.drawable.gu;
            }
            int id63 = id62;
            if (flag.equalsIgnoreCase("KW")) {
                id63 = R.drawable.kw;
            }
            int id64 = id63;
            if (flag.equalsIgnoreCase("CK")) {
                id64 = R.drawable.ck;
            }
            int id65 = id64;
            if (flag.equalsIgnoreCase("GL")) {
                id65 = R.drawable.gl;
            }
            int id66 = id65;
            if (flag.equalsIgnoreCase("CX")) {
                id66 = R.drawable.cx;
            }
            int id67 = id66;
            if (flag.equalsIgnoreCase("GE")) {
                id67 = R.drawable.ge;
            }
            int id68 = id67;
            if (flag.equalsIgnoreCase("GD")) {
                id68 = R.drawable.gd;
            }
            int id69 = id68;
            if (flag.equalsIgnoreCase("HR")) {
                id69 = R.drawable.hr;
            }
            int id70 = id69;
            if (flag.equalsIgnoreCase("KY")) {
                id70 = R.drawable.ky;
            }
            int id71 = id70;
            if (flag.equalsIgnoreCase("KE")) {
                id71 = R.drawable.ke;
            }
            int id72 = id71;
            if (flag.equalsIgnoreCase("CI")) {
                id72 = R.drawable.ci;
            }
            int id73 = id72;
            if (flag.equalsIgnoreCase("CC")) {
                id73 = R.drawable.cc;
            }
            int id74 = id73;
            if (flag.equalsIgnoreCase("CR")) {
                id74 = R.drawable.cr;
            }
            int id75 = id74;
            if (flag.equalsIgnoreCase("KM")) {
                id75 = R.drawable.km;
            }
            int id76 = id75;
            if (flag.equalsIgnoreCase("CO")) {
                id76 = R.drawable.co;
            }
            int id77 = id76;
            if (flag.equalsIgnoreCase("CG")) {
                id77 = R.drawable.cg;
            }
            int id78 = id77;
            if (flag.equalsIgnoreCase("CD")) {
                id78 = R.drawable.cd;
            }
            int id79 = id78;
            if (flag.equalsIgnoreCase("SA")) {
                id79 = R.drawable.sa;
            }
            int id80 = id79;
            if (flag.equalsIgnoreCase("WS")) {
                id80 = R.drawable.ws;
            }
            int id81 = id80;
            if (flag.equalsIgnoreCase("ST")) {
                id81 = R.drawable.st;
            }
            int id82 = id81;
            if (flag.equalsIgnoreCase("ZM")) {
                id82 = R.drawable.zm;
            }
            int id83 = id82;
            if (flag.equalsIgnoreCase("PM")) {
                id83 = R.drawable.pm;
            }
            int id84 = id83;
            if (flag.equalsIgnoreCase("SM")) {
                id84 = R.drawable.sm;
            }
            int id85 = id84;
            if (flag.equalsIgnoreCase("SL")) {
                id85 = R.drawable.sl;
            }
            int id86 = id85;
            if (flag.equalsIgnoreCase("DJ")) {
                id86 = R.drawable.dj;
            }
            int id87 = id86;
            if (flag.equalsIgnoreCase("GI")) {
                id87 = R.drawable.gi;
            }
            int id88 = id87;
            if (flag.equalsIgnoreCase("JE")) {
                id88 = R.drawable.je;
            }
            int id89 = id88;
            if (flag.equalsIgnoreCase("JM")) {
                id89 = R.drawable.jm;
            }
            int id90 = id89;
            if (flag.equalsIgnoreCase("SY")) {
                id90 = R.drawable.sy;
            }
            int id91 = id90;
            if (flag.equalsIgnoreCase("SG")) {
                id91 = R.drawable.sg;
            }
            int id92 = id91;
            if (flag.equalsIgnoreCase("ZW")) {
                id92 = R.drawable.zw;
            }
            int id93 = id92;
            if (flag.equalsIgnoreCase("CH")) {
                id93 = R.drawable.ch;
            }
            int id94 = id93;
            if (flag.equalsIgnoreCase("SE")) {
                id94 = R.drawable.se;
            }
            int id95 = id94;
            if (flag.equalsIgnoreCase("SD")) {
                id95 = R.drawable.sd;
            }
            int id96 = id95;
            if (flag.equalsIgnoreCase("SJ")) {
                id96 = R.drawable.sj;
            }
            int id97 = id96;
            if (flag.equalsIgnoreCase("ES")) {
                id97 = R.drawable.es;
            }
            int id98 = id97;
            if (flag.equalsIgnoreCase("SR")) {
                id98 = R.drawable.sr;
            }
            int id99 = id98;
            if (flag.equalsIgnoreCase("LK")) {
                id99 = R.drawable.lk;
            }
            int id100 = id99;
            if (flag.equalsIgnoreCase("SK")) {
                id100 = R.drawable.sk;
            }
            int id101 = id100;
            if (flag.equalsIgnoreCase("SI")) {
                id101 = R.drawable.si;
            }
            int id102 = id101;
            if (flag.equalsIgnoreCase("SZ")) {
                id102 = R.drawable.sz;
            }
            int id103 = id102;
            if (flag.equalsIgnoreCase("SC")) {
                id103 = R.drawable.sc;
            }
            int id104 = id103;
            if (flag.equalsIgnoreCase("GQ")) {
                id104 = R.drawable.gq;
            }
            int id105 = id104;
            if (flag.equalsIgnoreCase("SN")) {
                id105 = R.drawable.sn;
            }
            int id106 = id105;
            if (flag.equalsIgnoreCase("RS")) {
                id106 = R.drawable.rs;
            }
            int id107 = id106;
            if (flag.equalsIgnoreCase("KN")) {
                id107 = R.drawable.kn;
            }
            int id108 = id107;
            if (flag.equalsIgnoreCase("VC")) {
                id108 = R.drawable.vc;
            }
            int id109 = id108;
            if (flag.equalsIgnoreCase("SH")) {
                id109 = R.drawable.sh;
            }
            int id110 = id109;
            if (flag.equalsIgnoreCase("LC")) {
                id110 = R.drawable.lc;
            }
            int id111 = id110;
            if (flag.equalsIgnoreCase("SO")) {
                id111 = R.drawable.so;
            }
            int id112 = id111;
            if (flag.equalsIgnoreCase("SB")) {
                id112 = R.drawable.sb;
            }
            int id113 = id112;
            if (flag.equalsIgnoreCase("TC")) {
                id113 = R.drawable.tc;
            }
            int id114 = id113;
            if (flag.equalsIgnoreCase("TH")) {
                id114 = R.drawable.th;
            }
            int id115 = id114;
            if (flag.equalsIgnoreCase("KR")) {
                id115 = R.drawable.kr;
            }
            int id116 = id115;
            if (flag.equalsIgnoreCase("TW")) {
                id116 = R.drawable.tw;
            }
            int id117 = id116;
            if (flag.equalsIgnoreCase("TJ")) {
                id117 = R.drawable.tj;
            }
            int id118 = id117;
            if (flag.equalsIgnoreCase("TZ")) {
                id118 = R.drawable.tz;
            }
            int id119 = id118;
            if (flag.equalsIgnoreCase("CZ")) {
                id119 = R.drawable.cz;
            }
            int id120 = id119;
            if (flag.equalsIgnoreCase("TD")) {
                id120 = R.drawable.td;
            }
            int id121 = id120;
            if (flag.equalsIgnoreCase("CF")) {
                id121 = R.drawable.cf;
            }
            int id122 = id121;
            if (flag.equalsIgnoreCase("CN")) {
                id122 = R.drawable.cn;
            }
            int id123 = id122;
            if (flag.equalsIgnoreCase("TN")) {
                id123 = R.drawable.tn;
            }
            int id124 = id123;
            if (flag.equalsIgnoreCase("KP")) {
                id124 = R.drawable.kp;
            }
            int id125 = id124;
            if (flag.equalsIgnoreCase("CL")) {
                id125 = R.drawable.cl;
            }
            int id126 = id125;
            if (flag.equalsIgnoreCase("TV")) {
                id126 = R.drawable.tv;
            }
            int id127 = id126;
            if (flag.equalsIgnoreCase("DK")) {
                id127 = R.drawable.dk;
            }
            int id128 = id127;
            if (flag.equalsIgnoreCase("DE")) {
                id128 = R.drawable.de;
            }
            int id129 = id128;
            if (flag.equalsIgnoreCase("TG")) {
                id129 = R.drawable.tg;
            }
            int id130 = id129;
            if (flag.equalsIgnoreCase("TK")) {
                id130 = R.drawable.tk;
            }
            int id131 = id130;
            if (flag.equalsIgnoreCase("DO")) {
                id131 = R.drawable.doe;
            }
            int id132 = id131;
            if (flag.equalsIgnoreCase("DM")) {
                id132 = R.drawable.dm;
            }
            int id133 = id132;
            if (flag.equalsIgnoreCase("TT")) {
                id133 = R.drawable.tt;
            }
            int id134 = id133;
            if (flag.equalsIgnoreCase("TM")) {
                id134 = R.drawable.tm;
            }
            int id135 = id134;
            if (flag.equalsIgnoreCase("TR")) {
                id135 = R.drawable.tr;
            }
            int id136 = id135;
            if (flag.equalsIgnoreCase("TO")) {
                id136 = R.drawable.to;
            }
            int id137 = id136;
            if (flag.equalsIgnoreCase("NG")) {
                id137 = R.drawable.ng;
            }
            int id138 = id137;
            if (flag.equalsIgnoreCase("NR")) {
                id138 = R.drawable.nr;
            }
            int id139 = id138;
            if (flag.equalsIgnoreCase("NA")) {
                id139 = R.drawable.na;
            }
            int id140 = id139;
            if (flag.equalsIgnoreCase("NU")) {
                id140 = R.drawable.nu;
            }
            int id141 = id140;
            if (flag.equalsIgnoreCase("NI")) {
                id141 = R.drawable.ni;
            }
            int id142 = id141;
            if (flag.equalsIgnoreCase("NE")) {
                id142 = R.drawable.ne;
            }
            int id143 = id142;
            if (flag.equalsIgnoreCase("JP")) {
                id143 = R.drawable.f0jp;
            }
            int id144 = id143;
            if (flag.equalsIgnoreCase("EH")) {
                id144 = R.drawable.eh;
            }
            int id145 = id144;
            if (flag.equalsIgnoreCase("NC")) {
                id145 = R.drawable.nc;
            }
            int id146 = id145;
            if (flag.equalsIgnoreCase("NZ")) {
                id146 = R.drawable.nz;
            }
            int id147 = id146;
            if (flag.equalsIgnoreCase("NP")) {
                id147 = R.drawable.np;
            }
            int id148 = id147;
            if (flag.equalsIgnoreCase("NF")) {
                id148 = R.drawable.nf;
            }
            int id149 = id148;
            if (flag.equalsIgnoreCase("NO")) {
                id149 = R.drawable.no;
            }
            int id150 = id149;
            if (flag.equalsIgnoreCase("BH")) {
                id150 = R.drawable.bh;
            }
            int id151 = id150;
            if (flag.equalsIgnoreCase("HT")) {
                id151 = R.drawable.ht;
            }
            int id152 = id151;
            if (flag.equalsIgnoreCase("PK")) {
                id152 = R.drawable.pk;
            }
            int id153 = id152;
            if (flag.equalsIgnoreCase("VA")) {
                id153 = R.drawable.va;
            }
            int id154 = id153;
            if (flag.equalsIgnoreCase("PA")) {
                id154 = R.drawable.pa;
            }
            int id155 = id154;
            if (flag.equalsIgnoreCase("VU")) {
                id155 = R.drawable.vu;
            }
            int id156 = id155;
            if (flag.equalsIgnoreCase("BS")) {
                id156 = R.drawable.bs;
            }
            int id157 = id156;
            if (flag.equalsIgnoreCase("PG")) {
                id157 = R.drawable.pg;
            }
            int id158 = id157;
            if (flag.equalsIgnoreCase("BM")) {
                id158 = R.drawable.bm;
            }
            int id159 = id158;
            if (flag.equalsIgnoreCase("PW")) {
                id159 = R.drawable.pw;
            }
            int id160 = id159;
            if (flag.equalsIgnoreCase("PY")) {
                id160 = R.drawable.py;
            }
            int id161 = id160;
            if (flag.equalsIgnoreCase("BB")) {
                id161 = R.drawable.bb;
            }
            int id162 = id161;
            if (flag.equalsIgnoreCase("PS")) {
                id162 = R.drawable.ps;
            }
            int id163 = id162;
            if (flag.equalsIgnoreCase("HU")) {
                id163 = R.drawable.hu;
            }
            int id164 = id163;
            if (flag.equalsIgnoreCase("BD")) {
                id164 = R.drawable.bd;
            }
            int id165 = id164;
            if (flag.equalsIgnoreCase("TL")) {
                id165 = R.drawable.tl;
            }
            int id166 = id165;
            if (flag.equalsIgnoreCase("PN")) {
                id166 = R.drawable.pn;
            }
            int id167 = id166;
            if (flag.equalsIgnoreCase("FJ")) {
                id167 = R.drawable.fj;
            }
            int id168 = id167;
            if (flag.equalsIgnoreCase("PH")) {
                id168 = R.drawable.ph;
            }
            int id169 = id168;
            if (flag.equalsIgnoreCase("FI")) {
                id169 = R.drawable.fi;
            }
            int id170 = id169;
            if (flag.equalsIgnoreCase("BT")) {
                id170 = R.drawable.bt;
            }
            int id171 = id170;
            if (flag.equalsIgnoreCase("BV")) {
                id171 = R.drawable.bv;
            }
            int id172 = id171;
            if (flag.equalsIgnoreCase("PR")) {
                id172 = R.drawable.pr;
            }
            int id173 = id172;
            if (flag.equalsIgnoreCase("FO")) {
                id173 = R.drawable.fo;
            }
            int id174 = id173;
            if (flag.equalsIgnoreCase("FK")) {
                id174 = R.drawable.fk;
            }
            int id175 = id174;
            if (flag.equalsIgnoreCase("BR")) {
                id175 = R.drawable.br;
            }
            int id176 = id175;
            if (flag.equalsIgnoreCase("FR")) {
                id176 = R.drawable.fr;
            }
            int id177 = id176;
            if (flag.equalsIgnoreCase("GF")) {
                id177 = R.drawable.gf;
            }
            int id178 = id177;
            if (flag.equalsIgnoreCase("PF")) {
                id178 = R.drawable.pf;
            }
            int id179 = id178;
            if (flag.equalsIgnoreCase("TF")) {
                id179 = R.drawable.tf;
            }
            int id180 = id179;
            if (flag.equalsIgnoreCase("BG")) {
                id180 = R.drawable.bg;
            }
            int id181 = id180;
            if (flag.equalsIgnoreCase("BF")) {
                id181 = R.drawable.bf;
            }
            int id182 = id181;
            if (flag.equalsIgnoreCase("BN")) {
                id182 = R.drawable.bn;
            }
            int id183 = id182;
            if (flag.equalsIgnoreCase("BI")) {
                id183 = R.drawable.bi;
            }
            int id184 = id183;
            if (flag.equalsIgnoreCase("HM")) {
                id184 = R.drawable.hm;
            }
            int id185 = id184;
            if (flag.equalsIgnoreCase("VN")) {
                id185 = R.drawable.vn;
            }
            int id186 = id185;
            if (flag.equalsIgnoreCase("BJ")) {
                id186 = R.drawable.bj;
            }
            int id187 = id186;
            if (flag.equalsIgnoreCase("VE")) {
                id187 = R.drawable.ve;
            }
            int id188 = id187;
            if (flag.equalsIgnoreCase("BY")) {
                id188 = R.drawable.by;
            }
            int id189 = id188;
            if (flag.equalsIgnoreCase("BZ")) {
                id189 = R.drawable.bz;
            }
            int id190 = id189;
            if (flag.equalsIgnoreCase("PE")) {
                id190 = R.drawable.pe;
            }
            int id191 = id190;
            if (flag.equalsIgnoreCase("BE")) {
                id191 = R.drawable.be;
            }
            int id192 = id191;
            if (flag.equalsIgnoreCase("PL")) {
                id192 = R.drawable.pl;
            }
            int id193 = id192;
            if (flag.equalsIgnoreCase("BA")) {
                id193 = R.drawable.ba;
            }
            int id194 = id193;
            if (flag.equalsIgnoreCase("BW")) {
                id194 = R.drawable.bw;
            }
            int id195 = id194;
            if (flag.equalsIgnoreCase("BO")) {
                id195 = R.drawable.bo;
            }
            int id196 = id195;
            if (flag.equalsIgnoreCase("PT")) {
                id196 = R.drawable.pt;
            }
            int id197 = id196;
            if (flag.equalsIgnoreCase("HK")) {
                id197 = R.drawable.hk;
            }
            int id198 = id197;
            if (flag.equalsIgnoreCase("HN")) {
                id198 = R.drawable.hn;
            }
            int id199 = id198;
            if (flag.equalsIgnoreCase("MH")) {
                id199 = R.drawable.mh;
            }
            int id200 = id199;
            if (flag.equalsIgnoreCase("MO")) {
                id200 = R.drawable.mo;
            }
            int id201 = id200;
            if (flag.equalsIgnoreCase("MK")) {
                id201 = R.drawable.mk;
            }
            int id202 = id201;
            if (flag.equalsIgnoreCase("MG")) {
                id202 = R.drawable.mg;
            }
            int id203 = id202;
            if (flag.equalsIgnoreCase("YT")) {
                id203 = R.drawable.yt;
            }
            int id204 = id203;
            if (flag.equalsIgnoreCase("MW")) {
                id204 = R.drawable.mw;
            }
            int id205 = id204;
            if (flag.equalsIgnoreCase("ML")) {
                id205 = R.drawable.ml;
            }
            int id206 = id205;
            if (flag.equalsIgnoreCase("MT")) {
                id206 = R.drawable.mt;
            }
            int id207 = id206;
            if (flag.equalsIgnoreCase("MQ")) {
                id207 = R.drawable.mq;
            }
            int id208 = id207;
            if (flag.equalsIgnoreCase("MY")) {
                id208 = R.drawable.my;
            }
            int id209 = id208;
            if (flag.equalsIgnoreCase("IM")) {
                id209 = R.drawable.im;
            }
            int id210 = id209;
            if (flag.equalsIgnoreCase("FM")) {
                id210 = R.drawable.fm;
            }
            int id211 = id210;
            if (flag.equalsIgnoreCase("ZA")) {
                id211 = R.drawable.za;
            }
            int id212 = id211;
            if (flag.equalsIgnoreCase("GS")) {
                id212 = R.drawable.gs;
            }
            int id213 = id212;
            if (flag.equalsIgnoreCase("MM")) {
                id213 = R.drawable.mm;
            }
            int id214 = id213;
            if (flag.equalsIgnoreCase("MX")) {
                id214 = R.drawable.mx;
            }
            int id215 = id214;
            if (flag.equalsIgnoreCase("MU")) {
                id215 = R.drawable.mu;
            }
            int id216 = id215;
            if (flag.equalsIgnoreCase("MR")) {
                id216 = R.drawable.mr;
            }
            int id217 = id216;
            if (flag.equalsIgnoreCase("MZ")) {
                id217 = R.drawable.mz;
            }
            int id218 = id217;
            if (flag.equalsIgnoreCase("MC")) {
                id218 = R.drawable.mc;
            }
            int id219 = id218;
            if (flag.equalsIgnoreCase("MV")) {
                id219 = R.drawable.mv;
            }
            int id220 = id219;
            if (flag.equalsIgnoreCase("MD")) {
                id220 = R.drawable.md;
            }
            int id221 = id220;
            if (flag.equalsIgnoreCase("MA")) {
                id221 = R.drawable.ma;
            }
            int id222 = id221;
            if (flag.equalsIgnoreCase("MN")) {
                id222 = R.drawable.mn;
            }
            int id223 = id222;
            if (flag.equalsIgnoreCase("ME")) {
                id223 = R.drawable.me;
            }
            int id224 = id223;
            if (flag.equalsIgnoreCase("MS")) {
                id224 = R.drawable.ms;
            }
            int id225 = id224;
            if (flag.equalsIgnoreCase("JO")) {
                id225 = R.drawable.jo;
            }
            int id226 = id225;
            if (flag.equalsIgnoreCase("LA")) {
                id226 = R.drawable.la;
            }
            int id227 = id226;
            if (flag.equalsIgnoreCase("LV")) {
                id227 = R.drawable.lv;
            }
            int id228 = id227;
            if (flag.equalsIgnoreCase("LT")) {
                id228 = R.drawable.lt;
            }
            int id229 = id228;
            if (flag.equalsIgnoreCase("LY")) {
                id229 = R.drawable.ly;
            }
            int id230 = id229;
            if (flag.equalsIgnoreCase("LI")) {
                id230 = R.drawable.li;
            }
            int id231 = id230;
            if (flag.equalsIgnoreCase("LR")) {
                id231 = R.drawable.lr;
            }
            int id232 = id231;
            if (flag.equalsIgnoreCase("RO")) {
                id232 = R.drawable.ro;
            }
            int id233 = id232;
            if (flag.equalsIgnoreCase("LU")) {
                id233 = R.drawable.lu;
            }
            int id234 = id233;
            if (flag.equalsIgnoreCase("RW")) {
                id234 = R.drawable.rw;
            }
            int id235 = id234;
            if (flag.equalsIgnoreCase("LS")) {
                id235 = R.drawable.ls;
            }
            int id236 = id235;
            if (flag.equalsIgnoreCase("LB")) {
                id236 = R.drawable.lb;
            }
            int id237 = id236;
            if (flag.equalsIgnoreCase("RE")) {
                id237 = R.drawable.re;
            }
            int id238 = id237;
            if (flag.equalsIgnoreCase("RU")) {
                id238 = R.drawable.ru;
            }
            if (flag.equalsIgnoreCase("WF")) {
                return R.drawable.wf;
            }
            return id238;
        }
    }
}
