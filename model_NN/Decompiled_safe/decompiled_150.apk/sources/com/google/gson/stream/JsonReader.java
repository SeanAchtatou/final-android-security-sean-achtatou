package com.google.gson.stream;

import com.crossfield.taphunt.Global;
import java.io.Closeable;
import java.io.EOFException;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

public final class JsonReader implements Closeable {
    private static final char[] NON_EXECUTE_PREFIX = ")]}'\n".toCharArray();
    private final char[] buffer = new char[1024];
    private boolean hasToken;
    private final Reader in;
    private boolean lenient = false;
    private int limit = 0;
    private String name;
    private int pos = 0;
    private boolean skipping;
    private final List<JsonScope> stack = new ArrayList();
    private JsonToken token;
    private String value;

    public JsonReader(Reader in2) {
        push(JsonScope.EMPTY_DOCUMENT);
        this.skipping = false;
        if (in2 == null) {
            throw new NullPointerException("in == null");
        }
        this.in = in2;
    }

    public void setLenient(boolean lenient2) {
        this.lenient = lenient2;
    }

    public boolean isLenient() {
        return this.lenient;
    }

    public void beginArray() throws IOException {
        expect(JsonToken.BEGIN_ARRAY);
    }

    public void endArray() throws IOException {
        expect(JsonToken.END_ARRAY);
    }

    public void beginObject() throws IOException {
        expect(JsonToken.BEGIN_OBJECT);
    }

    public void endObject() throws IOException {
        expect(JsonToken.END_OBJECT);
    }

    private void expect(JsonToken expected) throws IOException {
        quickPeek();
        if (this.token != expected) {
            throw new IllegalStateException("Expected " + expected + " but was " + peek());
        }
        advance();
    }

    public boolean hasNext() throws IOException {
        quickPeek();
        return (this.token == JsonToken.END_OBJECT || this.token == JsonToken.END_ARRAY) ? false : true;
    }

    public JsonToken peek() throws IOException {
        quickPeek();
        if (this.token == null) {
            decodeLiteral();
        }
        return this.token;
    }

    private JsonToken quickPeek() throws IOException {
        if (this.hasToken) {
            return this.token;
        }
        switch (AnonymousClass1.$SwitchMap$com$google$gson$stream$JsonScope[peekStack().ordinal()]) {
            case 1:
                if (this.lenient) {
                    consumeNonExecutePrefix();
                }
                replaceTop(JsonScope.NONEMPTY_DOCUMENT);
                JsonToken firstToken = nextValue();
                if (!(this.lenient || firstToken == JsonToken.BEGIN_ARRAY || firstToken == JsonToken.BEGIN_OBJECT)) {
                    syntaxError("Expected JSON document to start with '[' or '{'");
                }
                return firstToken;
            case 2:
                return nextInArray(true);
            case 3:
                return nextInArray(false);
            case 4:
                return nextInObject(true);
            case 5:
                return objectValue();
            case 6:
                return nextInObject(false);
            case Global.STORE:
                try {
                    JsonToken token2 = nextValue();
                    if (this.lenient) {
                        return token2;
                    }
                    throw syntaxError("Expected EOF");
                } catch (EOFException e) {
                    this.hasToken = true;
                    JsonToken jsonToken = JsonToken.END_DOCUMENT;
                    this.token = jsonToken;
                    return jsonToken;
                }
            case Global.END:
                throw new IllegalStateException("JsonReader is closed");
            default:
                throw new AssertionError();
        }
    }

    /* renamed from: com.google.gson.stream.JsonReader$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$gson$stream$JsonScope = new int[JsonScope.values().length];

        static {
            try {
                $SwitchMap$com$google$gson$stream$JsonScope[JsonScope.EMPTY_DOCUMENT.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$google$gson$stream$JsonScope[JsonScope.EMPTY_ARRAY.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$google$gson$stream$JsonScope[JsonScope.NONEMPTY_ARRAY.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$com$google$gson$stream$JsonScope[JsonScope.EMPTY_OBJECT.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$com$google$gson$stream$JsonScope[JsonScope.DANGLING_NAME.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                $SwitchMap$com$google$gson$stream$JsonScope[JsonScope.NONEMPTY_OBJECT.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                $SwitchMap$com$google$gson$stream$JsonScope[JsonScope.NONEMPTY_DOCUMENT.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                $SwitchMap$com$google$gson$stream$JsonScope[JsonScope.CLOSED.ordinal()] = 8;
            } catch (NoSuchFieldError e8) {
            }
        }
    }

    private void consumeNonExecutePrefix() throws IOException {
        nextNonWhitespace();
        this.pos--;
        if (this.pos + NON_EXECUTE_PREFIX.length <= this.limit || fillBuffer(NON_EXECUTE_PREFIX.length)) {
            int i = 0;
            while (i < NON_EXECUTE_PREFIX.length) {
                if (this.buffer[this.pos + i] == NON_EXECUTE_PREFIX[i]) {
                    i++;
                } else {
                    return;
                }
            }
            this.pos += NON_EXECUTE_PREFIX.length;
        }
    }

    private JsonToken advance() throws IOException {
        quickPeek();
        JsonToken result = this.token;
        this.hasToken = false;
        this.token = null;
        this.value = null;
        this.name = null;
        return result;
    }

    public String nextName() throws IOException {
        quickPeek();
        if (this.token != JsonToken.NAME) {
            throw new IllegalStateException("Expected a name but was " + peek());
        }
        String result = this.name;
        advance();
        return result;
    }

    public String nextString() throws IOException {
        peek();
        if (this.value == null || !(this.token == JsonToken.STRING || this.token == JsonToken.NUMBER)) {
            throw new IllegalStateException("Expected a string but was " + peek());
        }
        String result = this.value;
        advance();
        return result;
    }

    public boolean nextBoolean() throws IOException {
        boolean result;
        quickPeek();
        if (this.value == null || this.token == JsonToken.STRING) {
            throw new IllegalStateException("Expected a boolean but was " + peek());
        }
        if (this.value.equalsIgnoreCase("true")) {
            result = true;
        } else if (this.value.equalsIgnoreCase("false")) {
            result = false;
        } else {
            throw new IllegalStateException("Not a boolean: " + this.value);
        }
        advance();
        return result;
    }

    public void nextNull() throws IOException {
        quickPeek();
        if (this.value == null || this.token == JsonToken.STRING) {
            throw new IllegalStateException("Expected null but was " + peek());
        } else if (!this.value.equalsIgnoreCase("null")) {
            throw new IllegalStateException("Not a null: " + this.value);
        } else {
            advance();
        }
    }

    public double nextDouble() throws IOException {
        quickPeek();
        if (this.value == null) {
            throw new IllegalStateException("Expected a double but was " + peek());
        }
        double result = Double.parseDouble(this.value);
        if (result >= 1.0d && this.value.startsWith("0")) {
            throw new NumberFormatException("JSON forbids octal prefixes: " + this.value);
        } else if (this.lenient || (!Double.isNaN(result) && !Double.isInfinite(result))) {
            advance();
            return result;
        } else {
            throw new NumberFormatException("JSON forbids NaN and infinities: " + this.value);
        }
    }

    public long nextLong() throws IOException {
        long result;
        quickPeek();
        if (this.value == null) {
            throw new IllegalStateException("Expected a long but was " + peek());
        }
        try {
            result = Long.parseLong(this.value);
        } catch (NumberFormatException e) {
            double asDouble = Double.parseDouble(this.value);
            result = (long) asDouble;
            if (((double) result) != asDouble) {
                throw new NumberFormatException(this.value);
            }
        }
        if (result < 1 || !this.value.startsWith("0")) {
            advance();
            return result;
        }
        throw new NumberFormatException("JSON forbids octal prefixes: " + this.value);
    }

    public int nextInt() throws IOException {
        int result;
        quickPeek();
        if (this.value == null) {
            throw new IllegalStateException("Expected an int but was " + peek());
        }
        try {
            result = Integer.parseInt(this.value);
        } catch (NumberFormatException e) {
            double asDouble = Double.parseDouble(this.value);
            result = (int) asDouble;
            if (((double) result) != asDouble) {
                throw new NumberFormatException(this.value);
            }
        }
        if (((long) result) < 1 || !this.value.startsWith("0")) {
            advance();
            return result;
        }
        throw new NumberFormatException("JSON forbids octal prefixes: " + this.value);
    }

    public void close() throws IOException {
        this.hasToken = false;
        this.value = null;
        this.token = null;
        this.stack.clear();
        this.stack.add(JsonScope.CLOSED);
        this.in.close();
    }

    public void skipValue() throws IOException {
        this.skipping = true;
        int count = 0;
        do {
            try {
                JsonToken token2 = advance();
                if (token2 == JsonToken.BEGIN_ARRAY || token2 == JsonToken.BEGIN_OBJECT) {
                    count++;
                    continue;
                } else if (token2 == JsonToken.END_ARRAY || token2 == JsonToken.END_OBJECT) {
                    count--;
                    continue;
                }
            } finally {
                this.skipping = false;
            }
        } while (count != 0);
    }

    private JsonScope peekStack() {
        return this.stack.get(this.stack.size() - 1);
    }

    private JsonScope pop() {
        return this.stack.remove(this.stack.size() - 1);
    }

    private void push(JsonScope newTop) {
        this.stack.add(newTop);
    }

    private void replaceTop(JsonScope newTop) {
        this.stack.set(this.stack.size() - 1, newTop);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private JsonToken nextInArray(boolean firstElement) throws IOException {
        if (firstElement) {
            replaceTop(JsonScope.NONEMPTY_ARRAY);
        } else {
            switch (nextNonWhitespace()) {
                case 44:
                    break;
                case 59:
                    checkLenient();
                    break;
                case 93:
                    pop();
                    this.hasToken = true;
                    JsonToken jsonToken = JsonToken.END_ARRAY;
                    this.token = jsonToken;
                    return jsonToken;
                default:
                    throw syntaxError("Unterminated array");
            }
        }
        switch (nextNonWhitespace()) {
            case 44:
            case 59:
                break;
            default:
                this.pos--;
                return nextValue();
            case 93:
                if (firstElement) {
                    pop();
                    this.hasToken = true;
                    JsonToken jsonToken2 = JsonToken.END_ARRAY;
                    this.token = jsonToken2;
                    return jsonToken2;
                }
                break;
        }
        checkLenient();
        this.pos--;
        this.hasToken = true;
        this.value = "null";
        JsonToken jsonToken3 = JsonToken.NULL;
        this.token = jsonToken3;
        return jsonToken3;
    }

    private JsonToken nextInObject(boolean firstElement) throws IOException {
        if (firstElement) {
            switch (nextNonWhitespace()) {
                case 125:
                    pop();
                    this.hasToken = true;
                    JsonToken jsonToken = JsonToken.END_OBJECT;
                    this.token = jsonToken;
                    return jsonToken;
                default:
                    this.pos--;
                    break;
            }
        } else {
            switch (nextNonWhitespace()) {
                case 44:
                case 59:
                    break;
                case 125:
                    pop();
                    this.hasToken = true;
                    JsonToken jsonToken2 = JsonToken.END_OBJECT;
                    this.token = jsonToken2;
                    return jsonToken2;
                default:
                    throw syntaxError("Unterminated object");
            }
        }
        int quote = nextNonWhitespace();
        switch (quote) {
            case 39:
                checkLenient();
            case 34:
                this.name = nextString((char) quote);
                break;
            default:
                checkLenient();
                this.pos--;
                this.name = nextLiteral();
                if (this.name.length() == 0) {
                    throw syntaxError("Expected name");
                }
                break;
        }
        replaceTop(JsonScope.DANGLING_NAME);
        this.hasToken = true;
        JsonToken jsonToken3 = JsonToken.NAME;
        this.token = jsonToken3;
        return jsonToken3;
    }

    private JsonToken objectValue() throws IOException {
        switch (nextNonWhitespace()) {
            case 58:
                break;
            case 59:
            case 60:
            default:
                throw syntaxError("Expected ':'");
            case 61:
                checkLenient();
                if ((this.pos < this.limit || fillBuffer(1)) && this.buffer[this.pos] == '>') {
                    this.pos++;
                    break;
                }
        }
        replaceTop(JsonScope.NONEMPTY_OBJECT);
        return nextValue();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private JsonToken nextValue() throws IOException {
        int c = nextNonWhitespace();
        switch (c) {
            case 34:
                break;
            case 39:
                checkLenient();
                break;
            case 91:
                push(JsonScope.EMPTY_ARRAY);
                this.hasToken = true;
                JsonToken jsonToken = JsonToken.BEGIN_ARRAY;
                this.token = jsonToken;
                return jsonToken;
            case 123:
                push(JsonScope.EMPTY_OBJECT);
                this.hasToken = true;
                JsonToken jsonToken2 = JsonToken.BEGIN_OBJECT;
                this.token = jsonToken2;
                return jsonToken2;
            default:
                this.pos--;
                return readLiteral();
        }
        this.value = nextString((char) c);
        this.hasToken = true;
        JsonToken jsonToken3 = JsonToken.STRING;
        this.token = jsonToken3;
        return jsonToken3;
    }

    private boolean fillBuffer(int minimum) throws IOException {
        if (this.limit != this.pos) {
            this.limit -= this.pos;
            System.arraycopy(this.buffer, this.pos, this.buffer, 0, this.limit);
        } else {
            this.limit = 0;
        }
        this.pos = 0;
        do {
            int total = this.in.read(this.buffer, this.limit, this.buffer.length - this.limit);
            if (total == -1) {
                return false;
            }
            this.limit += total;
        } while (this.limit < minimum);
        return true;
    }

    private int nextNonWhitespace() throws IOException {
        char c;
        while (true) {
            if (this.pos < this.limit || fillBuffer(1)) {
                char[] cArr = this.buffer;
                int i = this.pos;
                this.pos = i + 1;
                c = cArr[i];
                switch (c) {
                    case 9:
                    case 10:
                    case 13:
                    case ' ':
                        break;
                    case '#':
                        checkLenient();
                        skipToEndOfLine();
                        break;
                    case '/':
                        if (this.pos == this.limit && !fillBuffer(1)) {
                            break;
                        } else {
                            checkLenient();
                            switch (this.buffer[this.pos]) {
                                case '*':
                                    this.pos++;
                                    if (!skipTo("*/")) {
                                        throw syntaxError("Unterminated comment");
                                    }
                                    this.pos += 2;
                                    continue;
                                case '/':
                                    this.pos++;
                                    skipToEndOfLine();
                                    continue;
                            }
                        }
                        break;
                }
            } else {
                throw new EOFException("End of input");
            }
        }
        return c;
    }

    private void checkLenient() throws IOException {
        if (!this.lenient) {
            throw syntaxError("Use JsonReader.setLenient(true) to accept malformed JSON");
        }
    }

    private void skipToEndOfLine() throws IOException {
        char c;
        do {
            if (this.pos < this.limit || fillBuffer(1)) {
                char[] cArr = this.buffer;
                int i = this.pos;
                this.pos = i + 1;
                c = cArr[i];
                if (c == 13) {
                    return;
                }
            } else {
                return;
            }
        } while (c != 10);
    }

    private boolean skipTo(String toFind) throws IOException {
        while (true) {
            if (this.pos + toFind.length() >= this.limit && !fillBuffer(toFind.length())) {
                return false;
            }
            int c = 0;
            while (c < toFind.length()) {
                if (this.buffer[this.pos + c] != toFind.charAt(c)) {
                    this.pos++;
                } else {
                    c++;
                }
            }
            return true;
        }
    }

    private String nextString(char quote) throws IOException {
        StringBuilder builder = null;
        do {
            int start = this.pos;
            while (this.pos < this.limit) {
                char[] cArr = this.buffer;
                int i = this.pos;
                this.pos = i + 1;
                char c = cArr[i];
                if (c == quote) {
                    if (this.skipping) {
                        return "skipped!";
                    }
                    if (builder == null) {
                        return new String(this.buffer, start, (this.pos - start) - 1);
                    }
                    builder.append(this.buffer, start, (this.pos - start) - 1);
                    return builder.toString();
                } else if (c == '\\') {
                    if (builder == null) {
                        builder = new StringBuilder();
                    }
                    builder.append(this.buffer, start, (this.pos - start) - 1);
                    builder.append(readEscapeCharacter());
                    start = this.pos;
                }
            }
            if (builder == null) {
                builder = new StringBuilder();
            }
            builder.append(this.buffer, start, this.pos - start);
        } while (fillBuffer(1));
        throw syntaxError("Unterminated string");
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0028  */
    /* JADX WARNING: Removed duplicated region for block: B:4:0x000a  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0021 A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String nextLiteral() throws java.io.IOException {
        /*
            r7 = this;
            r6 = 1
            r0 = 0
        L_0x0002:
            int r2 = r7.pos
        L_0x0004:
            int r3 = r7.pos
            int r4 = r7.limit
            if (r3 >= r4) goto L_0x0042
            char[] r3 = r7.buffer
            int r4 = r7.pos
            int r5 = r4 + 1
            r7.pos = r5
            char r1 = r3[r4]
            switch(r1) {
                case 9: goto L_0x0018;
                case 10: goto L_0x0018;
                case 12: goto L_0x0018;
                case 13: goto L_0x0018;
                case 32: goto L_0x0018;
                case 35: goto L_0x0024;
                case 44: goto L_0x0018;
                case 47: goto L_0x0024;
                case 58: goto L_0x0018;
                case 59: goto L_0x0024;
                case 61: goto L_0x0024;
                case 91: goto L_0x0018;
                case 92: goto L_0x0024;
                case 93: goto L_0x0018;
                case 123: goto L_0x0018;
                case 125: goto L_0x0018;
                default: goto L_0x0017;
            }
        L_0x0017:
            goto L_0x0004
        L_0x0018:
            int r3 = r7.pos
            int r3 = r3 - r6
            r7.pos = r3
            boolean r3 = r7.skipping
            if (r3 == 0) goto L_0x0028
            java.lang.String r3 = "skipped!"
        L_0x0023:
            return r3
        L_0x0024:
            r7.checkLenient()
            goto L_0x0018
        L_0x0028:
            if (r0 != 0) goto L_0x0035
            java.lang.String r3 = new java.lang.String
            char[] r4 = r7.buffer
            int r5 = r7.pos
            int r5 = r5 - r2
            r3.<init>(r4, r2, r5)
            goto L_0x0023
        L_0x0035:
            char[] r3 = r7.buffer
            int r4 = r7.pos
            int r4 = r4 - r2
            r0.append(r3, r2, r4)
            java.lang.String r3 = r0.toString()
            goto L_0x0023
        L_0x0042:
            if (r0 != 0) goto L_0x0049
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
        L_0x0049:
            char[] r3 = r7.buffer
            int r4 = r7.pos
            int r4 = r4 - r2
            r0.append(r3, r2, r4)
            boolean r3 = r7.fillBuffer(r6)
            if (r3 != 0) goto L_0x0002
            java.lang.String r3 = r0.toString()
            goto L_0x0023
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.gson.stream.JsonReader.nextLiteral():java.lang.String");
    }

    public String toString() {
        return getClass().getSimpleName() + " near " + ((Object) getSnippet());
    }

    private char readEscapeCharacter() throws IOException {
        if (this.pos != this.limit || fillBuffer(1)) {
            char[] cArr = this.buffer;
            int i = this.pos;
            this.pos = i + 1;
            char escaped = cArr[i];
            switch (escaped) {
                case 'b':
                    return 8;
                case 'f':
                    return 12;
                case 'n':
                    return 10;
                case 'r':
                    return 13;
                case 't':
                    return 9;
                case 'u':
                    if (this.pos + 4 <= this.limit || fillBuffer(4)) {
                        String hex = new String(this.buffer, this.pos, 4);
                        this.pos += 4;
                        return (char) Integer.parseInt(hex, 16);
                    }
                    throw syntaxError("Unterminated escape sequence");
                default:
                    return escaped;
            }
        } else {
            throw syntaxError("Unterminated escape sequence");
        }
    }

    private JsonToken readLiteral() throws IOException {
        String literal = nextLiteral();
        if (literal.length() == 0) {
            throw syntaxError("Expected literal value");
        }
        this.value = literal;
        this.hasToken = true;
        this.token = null;
        return null;
    }

    private void decodeLiteral() throws IOException {
        if (this.value.equalsIgnoreCase("null")) {
            this.token = JsonToken.NULL;
        } else if (this.value.equalsIgnoreCase("true") || this.value.equalsIgnoreCase("false")) {
            this.token = JsonToken.BOOLEAN;
        } else {
            try {
                Double.parseDouble(this.value);
                this.token = JsonToken.NUMBER;
            } catch (NumberFormatException e) {
                checkLenient();
                this.token = JsonToken.STRING;
            }
        }
    }

    private IOException syntaxError(String message) throws IOException {
        throw new MalformedJsonException(message + " near " + ((Object) getSnippet()));
    }

    private CharSequence getSnippet() {
        StringBuilder snippet = new StringBuilder();
        int beforePos = Math.min(this.pos, 20);
        snippet.append(this.buffer, this.pos - beforePos, beforePos);
        snippet.append(this.buffer, this.pos, Math.min(this.limit - this.pos, 20));
        return snippet;
    }
}
