package com.flurry.android;

import java.lang.Thread;

final class g implements Thread.UncaughtExceptionHandler {
    private Thread.UncaughtExceptionHandler a = Thread.getDefaultUncaughtExceptionHandler();

    g() {
    }

    public final void uncaughtException(Thread thread, Throwable th) {
        try {
            FlurryAgent.f.a(th);
        } catch (Throwable th2) {
            ah.b("FlurryAgent", "", th2);
        }
        if (this.a != null) {
            this.a.uncaughtException(thread, th);
        }
    }
}
