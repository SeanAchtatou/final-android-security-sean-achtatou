package com.commind.gmail;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import com.commind.bubbles.donate.Bubble;
import com.commind.bubbles.donate.ForumWeb;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import oauth.signpost.OAuth;
import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;
import oauth.signpost.commonshttp.CommonsHttpOAuthProvider;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;
import oauth.signpost.exception.OAuthNotAuthorizedException;

public abstract class GmailNotifier {
    public static final String CALLBACK_URL = "com.commind.bubbles://gmail";
    public static final String CONSUMER_KEY = "anonymous_key";
    public static final String CONSUMER_SECRET = "anonymous_secret";
    public static final String MESS_INTENT = "gmail_message";
    public static final String scope = "https://mail.google.com/mail/feed/atom";
    /* access modifiers changed from: private */
    public CommonsHttpOAuthConsumer consumer = new CommonsHttpOAuthConsumer("anonymous", "anonymous");
    /* access modifiers changed from: private */
    public ProgressDialog dialog;
    /* access modifiers changed from: private */
    public Activity mA;
    /* access modifiers changed from: private */
    public final SharedPreferences prefs;
    /* access modifiers changed from: private */
    public CommonsHttpOAuthProvider provider;

    public abstract void onResult(boolean z);

    private class makasyncReqTask extends AsyncTask<String, Void, String> {
        private boolean getauth;

        private makasyncReqTask() {
            this.getauth = false;
        }

        /* synthetic */ makasyncReqTask(GmailNotifier gmailNotifier, makasyncReqTask makasyncreqtask) {
            this();
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... params) {
            String url = params[0];
            if (url == null) {
                try {
                    return GmailNotifier.this.provider.retrieveRequestToken(GmailNotifier.this.consumer, GmailNotifier.CALLBACK_URL);
                } catch (OAuthMessageSignerException e) {
                    e.printStackTrace();
                    return null;
                } catch (OAuthNotAuthorizedException e2) {
                    e2.printStackTrace();
                    return null;
                } catch (OAuthExpectationFailedException e3) {
                    e3.printStackTrace();
                    return null;
                } catch (OAuthCommunicationException e4) {
                    e4.printStackTrace();
                    return null;
                }
            } else {
                try {
                    GmailNotifier.this.provider.retrieveAccessToken(GmailNotifier.this.consumer, url);
                    SharedPreferences.Editor editor = GmailNotifier.this.prefs.edit();
                    editor.putString(GmailNotifier.CONSUMER_KEY, GmailNotifier.this.consumer.getToken());
                    editor.putString(GmailNotifier.CONSUMER_SECRET, GmailNotifier.this.consumer.getTokenSecret());
                    editor.commit();
                    this.getauth = true;
                    return null;
                } catch (OAuthMessageSignerException e5) {
                    e5.printStackTrace();
                    return null;
                } catch (OAuthNotAuthorizedException e6) {
                    e6.printStackTrace();
                    return null;
                } catch (OAuthExpectationFailedException e7) {
                    e7.printStackTrace();
                    return null;
                } catch (OAuthCommunicationException e8) {
                    e8.printStackTrace();
                    return null;
                }
            }
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            if (GmailNotifier.this.dialog != null && !GmailNotifier.this.dialog.isShowing()) {
                GmailNotifier.this.dialog.show();
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String url) {
            if (GmailNotifier.this.dialog != null) {
                GmailNotifier.this.dialog.dismiss();
            }
            if (url != null) {
                Intent i = new Intent(GmailNotifier.this.mA, ForumWeb.class);
                i.putExtra("gmail_url", url);
                GmailNotifier.this.mA.startActivityForResult(i, 1);
            } else if (url != null || !this.getauth) {
                GmailNotifier.this.onResult(false);
            } else {
                GmailNotifier.this.onResult(true);
            }
        }
    }

    public GmailNotifier(Context c, boolean login) {
        this.prefs = c.getSharedPreferences(Bubble.SHARED_PREFS_NAME, 0);
        try {
            this.provider = new CommonsHttpOAuthProvider("https://www.google.com/accounts/OAuthGetRequestToken?scope=" + URLEncoder.encode(scope, "utf-8"), "https://www.google.com/accounts/OAuthGetAccessToken", "https://www.google.com/accounts/OAuthAuthorizeToken?hd=default");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        if (!login) {
            SharedPreferences.Editor editor = this.prefs.edit();
            editor.remove(CONSUMER_KEY);
            editor.remove(CONSUMER_SECRET);
            editor.putBoolean(Bubble.SHARED_GM, false);
            editor.commit();
            return;
        }
        this.mA = (Activity) c;
        this.dialog = new ProgressDialog(c);
        this.dialog.setIndeterminate(true);
        this.dialog.setCancelable(true);
        new makasyncReqTask(this, null).execute(null);
    }

    public void getAuth(Intent i) {
        Uri uri = i.getData();
        if (uri != null && uri.toString().startsWith(CALLBACK_URL)) {
            String verifier = uri.getQueryParameter(OAuth.OAUTH_VERIFIER);
            new makasyncReqTask(this, null).execute(verifier);
        }
    }
}
