package com.commind.twitter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import com.commind.bubbles.donate.Bubble;
import com.commind.bubbles.donate.ForumWeb;
import oauth.signpost.OAuth;
import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;
import oauth.signpost.commonshttp.CommonsHttpOAuthProvider;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;
import oauth.signpost.exception.OAuthNotAuthorizedException;

public abstract class TwitterClient {
    public static final String CALLBACK_URL = "com.commind.bubbles://twitter";
    public static final String CONSUMER_KEY = "tajCtNvl69tM5uy20iq3A";
    public static final String CONSUMER_SECRET = "QE4UvG8H0cSUtgpAxyMMCCnYJTuVI9Nx40JGCzRFM";
    public static final String MENTION_INTENT = "mention";
    public static final String MESS_INTENT = "twitter_message";
    /* access modifiers changed from: private */
    public CommonsHttpOAuthConsumer consumer = new CommonsHttpOAuthConsumer(CONSUMER_KEY, CONSUMER_SECRET);
    /* access modifiers changed from: private */
    public ProgressDialog dialog;
    /* access modifiers changed from: private */
    public Activity mA;
    /* access modifiers changed from: private */
    public final SharedPreferences prefs;
    /* access modifiers changed from: private */
    public CommonsHttpOAuthProvider provider = new CommonsHttpOAuthProvider("http://twitter.com/oauth/request_token", "http://twitter.com/oauth/access_token", "http://twitter.com/oauth/authorize");

    public abstract void onResult(boolean z);

    private class makasyncReqTask extends AsyncTask<String, Void, String> {
        private boolean getauth;

        private makasyncReqTask() {
            this.getauth = false;
        }

        /* synthetic */ makasyncReqTask(TwitterClient twitterClient, makasyncReqTask makasyncreqtask) {
            this();
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... params) {
            String url = params[0];
            if (url == null) {
                try {
                    return TwitterClient.this.provider.retrieveRequestToken(TwitterClient.this.consumer, TwitterClient.CALLBACK_URL);
                } catch (OAuthMessageSignerException e) {
                    e.printStackTrace();
                    return null;
                } catch (OAuthNotAuthorizedException e2) {
                    e2.printStackTrace();
                    return null;
                } catch (OAuthExpectationFailedException e3) {
                    e3.printStackTrace();
                    return null;
                } catch (OAuthCommunicationException e4) {
                    e4.printStackTrace();
                    return null;
                }
            } else {
                try {
                    TwitterClient.this.provider.retrieveAccessToken(TwitterClient.this.consumer, url);
                    SharedPreferences.Editor editor = TwitterClient.this.prefs.edit();
                    editor.putString(TwitterClient.CONSUMER_KEY, TwitterClient.this.consumer.getToken());
                    editor.putString(TwitterClient.CONSUMER_SECRET, TwitterClient.this.consumer.getTokenSecret());
                    editor.commit();
                    this.getauth = true;
                    return null;
                } catch (OAuthMessageSignerException e5) {
                    e5.printStackTrace();
                    return null;
                } catch (OAuthNotAuthorizedException e6) {
                    e6.printStackTrace();
                    return null;
                } catch (OAuthExpectationFailedException e7) {
                    e7.printStackTrace();
                    return null;
                } catch (OAuthCommunicationException e8) {
                    e8.printStackTrace();
                    return null;
                }
            }
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            if (TwitterClient.this.dialog != null && !TwitterClient.this.dialog.isShowing()) {
                TwitterClient.this.dialog.show();
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String url) {
            if (TwitterClient.this.dialog != null) {
                TwitterClient.this.dialog.dismiss();
            }
            if (url != null) {
                Intent i = new Intent(TwitterClient.this.mA, ForumWeb.class);
                i.putExtra("url", url);
                TwitterClient.this.mA.startActivityForResult(i, 0);
            } else if (url != null || !this.getauth) {
                TwitterClient.this.onResult(false);
            } else {
                TwitterClient.this.onResult(true);
            }
        }
    }

    public TwitterClient(Context c, boolean login) {
        this.prefs = c.getSharedPreferences(Bubble.SHARED_PREFS_NAME, 0);
        if (!login) {
            SharedPreferences.Editor editor = this.prefs.edit();
            editor.remove(CONSUMER_KEY);
            editor.remove(CONSUMER_SECRET);
            editor.putBoolean(Bubble.SHARED_TW, false);
            editor.commit();
            return;
        }
        this.mA = (Activity) c;
        this.dialog = new ProgressDialog(c);
        this.dialog.setIndeterminate(true);
        this.dialog.setCancelable(true);
        new makasyncReqTask(this, null).execute(null);
    }

    public void getAuth(Intent i) {
        Uri uri = i.getData();
        if (uri != null && uri.toString().startsWith(CALLBACK_URL)) {
            String verifier = uri.getQueryParameter(OAuth.OAUTH_VERIFIER);
            new makasyncReqTask(this, null).execute(verifier);
        }
    }
}
