package com.commind.bubbles.donate;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.drawable.BitmapDrawable;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;

public class DialogPrefHeart extends DialogPreference {
    /* access modifiers changed from: private */
    public SharedPreferences.Editor editor;
    private GridView sb;

    public DialogPrefHeart(Context context, AttributeSet attrs) {
        super(context, attrs);
        setDialogLayoutResource(R.layout.main);
        this.editor = context.getSharedPreferences(Bubble.SHARED_PREFS_NAME, 0).edit();
        setPositiveButtonText((CharSequence) null);
    }

    /* access modifiers changed from: protected */
    public View onCreateDialogView() {
        View v = super.onCreateDialogView();
        this.sb = (GridView) v.findViewById(R.id.gridView1);
        this.sb.setAdapter((ListAdapter) new GridAdapter(v.getContext()));
        this.sb.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View v, int position, long id) {
                DialogPrefHeart.this.editor.putInt("hearts_color", position);
                DialogPrefHeart.this.editor.commit();
                Dialog d = DialogPrefHeart.this.getDialog();
                if (d != null) {
                    d.dismiss();
                }
            }
        });
        return v;
    }

    private class GridAdapter extends BaseAdapter {
        private Bitmap bm;
        private Context mContext = null;
        private DisplayMetrics mMetrics;
        private Bitmap overlay;
        private Paint ps;

        public GridAdapter(Context c) {
            this.mContext = c;
            this.bm = ((BitmapDrawable) c.getResources().getDrawable(R.drawable.heart_noring_grey)).getBitmap();
            this.mMetrics = c.getResources().getDisplayMetrics();
            this.overlay = ((BitmapDrawable) c.getResources().getDrawable(R.drawable.heart_noring_white)).getBitmap();
            this.ps = new Paint();
            this.ps.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_OVER));
        }

        public int getCount() {
            return BubbleModel.BUBBLE_COLORS.length;
        }

        public Object getItem(int position) {
            return null;
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ImageView iv = new ImageView(this.mContext);
            iv.setLayoutParams(new AbsListView.LayoutParams(-1, -2));
            int color = BubbleModel.BUBBLE_COLORS[position];
            Bitmap todraw = Bitmap.createBitmap(this.bm.getScaledWidth(this.mMetrics), this.bm.getScaledHeight(this.mMetrics), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(todraw);
            BubbleModel.generateBubbleColor(canvas, this.bm, color);
            canvas.drawBitmap(this.overlay, 0.0f, 0.0f, this.ps);
            iv.setImageBitmap(todraw);
            return iv;
        }
    }
}
