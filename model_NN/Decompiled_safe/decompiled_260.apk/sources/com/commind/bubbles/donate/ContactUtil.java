package com.commind.bubbles.donate;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.CallLog;
import android.provider.ContactsContract;
import com.commind.facebook.FacebookProvider;
import java.io.InputStream;

public class ContactUtil {
    public static final Uri CONVERSATION_CONTENT_URI = Uri.withAppendedPath(MMS_SMS_CONTENT_URI, "conversations");
    public static final Uri MMS_CONTENT_URI = Uri.parse("content://mms");
    public static final Uri MMS_INBOX_CONTENT_URI = Uri.withAppendedPath(MMS_CONTENT_URI, "inbox");
    public static final Uri MMS_SMS_CONTENT_URI = Uri.parse("content://mms-sms/");
    public static final Uri SMS_CONTENT_URI = Uri.parse("content://sms");
    public static final Uri SMS_INBOX_CONTENT_URI = Uri.withAppendedPath(SMS_CONTENT_URI, "inbox");
    public static final Uri THREAD_ID_CONTENT_URI = Uri.withAppendedPath(MMS_SMS_CONTENT_URI, "threadID");

    public static long findThreadIdFromAddress(Context context, String address) {
        Cursor cursor;
        if (address == null) {
            return 0;
        }
        Uri.Builder uriBuilder = THREAD_ID_CONTENT_URI.buildUpon();
        uriBuilder.appendQueryParameter("recipient", address);
        long threadId = 0;
        try {
            cursor = context.getContentResolver().query(uriBuilder.build(), new String[]{FacebookProvider.KEY_ID}, null, null, null);
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    threadId = cursor.getLong(0);
                }
                cursor.close();
            }
        } catch (SQLiteException e) {
        } catch (Throwable th) {
            cursor.close();
            throw th;
        }
        return threadId;
    }

    public static boolean checkifRead(int type, String incomingNumber, Context c, long id) {
        boolean result = false;
        switch (type) {
            case Bubble.T_SMS /*0*/:
                if (id <= 0 || !getSmsRead(c, id)) {
                    return false;
                }
                return true;
            case 1:
                Cursor cursor = c.getContentResolver().query(CallLog.Calls.CONTENT_URI, null, "type = ? AND new = ? AND number = ?", new String[]{Integer.toString(3), "1", incomingNumber}, "date DESC ");
                if (cursor == null) {
                    return false;
                }
                if (cursor.getCount() <= 0) {
                    result = true;
                }
                cursor.close();
                return result;
            default:
                return false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public static void setMessageRead(Context context, long messageId, int messageType) {
        Uri messageUri;
        if (messageId > 0) {
            ContentValues values = new ContentValues(1);
            values.put("read", (Integer) 1);
            if (messageType == 0) {
                messageUri = Uri.withAppendedPath(MMS_CONTENT_URI, String.valueOf(messageId));
            } else if (1 == messageType) {
                messageUri = Uri.withAppendedPath(SMS_CONTENT_URI, String.valueOf(messageId));
            } else {
                return;
            }
            try {
                int result = context.getContentResolver().update(messageUri, values, null, null);
            } catch (Exception e) {
            }
        }
    }

    public static long findMessageId(Context context, long threadId) {
        Cursor cursor;
        long id = 0;
        if (threadId > 0 && (cursor = context.getContentResolver().query(ContentUris.withAppendedId(CONVERSATION_CONTENT_URI, threadId), new String[]{FacebookProvider.KEY_ID, "date"}, null, null, "date desc")) != null) {
            try {
                if (cursor.moveToFirst()) {
                    id = cursor.getLong(0);
                }
            } finally {
                cursor.close();
            }
        }
        return id;
    }

    private static boolean getSmsRead(Context context, long id) {
        boolean result = false;
        String WHERE_CONDITION = String.valueOf(String.valueOf("read") + " = 1") + " AND _id == " + id;
        Cursor cursor = context.getContentResolver().query(SMS_INBOX_CONTENT_URI, new String[]{FacebookProvider.KEY_ID}, WHERE_CONDITION, null, "date DESC");
        if (cursor != null) {
            try {
                if (cursor.getCount() > 0) {
                    result = true;
                }
            } finally {
                cursor.close();
            }
        } else {
            Cursor cursor2 = context.getContentResolver().query(MMS_INBOX_CONTENT_URI, new String[]{FacebookProvider.KEY_ID}, WHERE_CONDITION, null, "date DESC");
            if (cursor2 != null) {
                try {
                    if (cursor2.getCount() > 0) {
                        result = true;
                    }
                } finally {
                    cursor2.close();
                }
            }
        }
        return result;
    }

    public static Bitmap loadContactPhoto(ContentResolver cr, Uri uri) {
        InputStream input = ContactsContract.Contacts.openContactPhotoInputStream(cr, uri);
        if (input == null) {
            return null;
        }
        return BitmapFactory.decodeStream(input);
    }
}
