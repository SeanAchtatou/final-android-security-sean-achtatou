package com.commind.bubbles.donate;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.telephony.SmsMessage;
import com.commind.facebook.FacebookProvider;

public class SmsApp extends BroadcastReceiver {
    /* Debug info: failed to restart local var, previous not found, register: 21 */
    public void onReceive(Context context, Intent intent) {
        Bundle extras;
        ContentResolver cr;
        Cursor cursor;
        SharedPreferences prefs = context.getSharedPreferences(Bubble.SHARED_PREFS_NAME, 0);
        boolean usesms = false;
        if (prefs.contains(Bubble.SHARED_SMS_SETTING)) {
            usesms = prefs.getBoolean(Bubble.SHARED_SMS_SETTING, true);
        }
        if (usesms && (extras = intent.getExtras()) != null) {
            Object[] pdus = (Object[]) extras.get("pdus");
            for (Object obj : pdus) {
                SmsMessage message = SmsMessage.createFromPdu((byte[]) obj);
                String fromAddress = message.getOriginatingAddress();
                String messageBody = message.getMessageBody();
                Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(fromAddress));
                String[] projection = {FacebookProvider.KEY_ID, "display_name"};
                if (!(fromAddress == null || fromAddress.length() <= 1 || (cursor = (cr = context.getContentResolver()).query(uri, projection, null, null, null)) == null)) {
                    if (cursor.moveToFirst()) {
                        String id = cursor.getString(0);
                        String dsp = cursor.getString(1);
                        if (id != null) {
                            Bitmap facebm = ContactUtil.loadContactPhoto(cr, ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, Long.parseLong(id)));
                        }
                    }
                    cursor.close();
                }
                if (fromAddress == null || fromAddress.length() <= 0) {
                }
            }
        }
    }
}
