package com.commind.bubbles.donate;

public final class R {

    public static final class anim {
        public static final int grow = 2130968576;
        public static final int shrink = 2130968577;
    }

    public static final class array {
        public static final int background = 2131165185;
        public static final int background_values = 2131165184;
        public static final int fbwallposts = 2131165187;
        public static final int noofbubbles = 2131165186;
    }

    public static final class attr {
    }

    public static final class drawable {
        public static final int bubble_big2 = 2130837504;
        public static final int bubble_big2_white = 2130837505;
        public static final int facebook_icon = 2130837506;
        public static final int facebook_single = 2130837507;
        public static final int gmail_single = 2130837508;
        public static final int grey_hearts_pop = 2130837509;
        public static final int heart_mask = 2130837510;
        public static final int heart_noring_grey = 2130837511;
        public static final int heart_noring_white = 2130837512;
        public static final int icon = 2130837513;
        public static final int main_bubble = 2130837514;
        public static final int main_bubble_single = 2130837515;
        public static final int mask = 2130837516;
        public static final int menu_bg = 2130837517;
        public static final int pop_grey = 2130837518;
        public static final int pop_grey_white = 2130837519;
        public static final int pop_white_hearts = 2130837520;
        public static final int shape = 2130837521;
        public static final int twitter_single = 2130837522;
        public static final int white_missed_call = 2130837523;
        public static final int white_missed_call_small = 2130837524;
        public static final int white_sms = 2130837525;
        public static final int white_sms_small = 2130837526;
    }

    public static final class id {
        public static final int LinLayout01 = 2131296257;
        public static final int ProgressBar01 = 2131296260;
        public static final int RelativeLayout02 = 2131296258;
        public static final int gridView1 = 2131296256;
        public static final int title = 2131296259;
        public static final int webview = 2131296261;
    }

    public static final class layout {
        public static final int main = 2130903040;
        public static final int web = 2130903041;
    }

    public static final class raw {
        public static final int pop = 2131099648;
    }

    public static final class string {
        public static final int accounts = 2131230738;
        public static final int accounts_text = 2131230739;
        public static final int app_name = 2131230720;
        public static final int bubbles_color = 2131230732;
        public static final int bubbles_sound = 2131230737;
        public static final int darling = 2131230745;
        public static final int darling_heart = 2131230748;
        public static final int darling_person = 2131230747;
        public static final int darling_text = 2131230746;
        public static final int facebook_game = 2131230740;
        public static final int facebook_gametext = 2131230741;
        public static final int facebook_login = 2131230731;
        public static final int facebooktext = 2131230729;
        public static final int facebookwall = 2131230730;
        public static final int gmail_login = 2131230735;
        public static final int gmailtext = 2131230736;
        public static final int local_game = 2131230742;
        public static final int local_game_showpop = 2131230744;
        public static final int local_gametext = 2131230743;
        public static final int noofbubbles = 2131230727;
        public static final int noofbubblestext = 2131230728;
        public static final int settings = 2131230721;
        public static final int settings_desc = 2131230723;
        public static final int settings_summary = 2131230724;
        public static final int settings_title = 2131230722;
        public static final int twitter_login = 2131230733;
        public static final int twittertext = 2131230734;
        public static final int usesms = 2131230725;
        public static final int usesmstext = 2131230726;
    }

    public static final class xml {
        public static final int bubble = 2131034112;
        public static final int bubble_settings = 2131034113;
    }
}
