package com.commind.bubbles.donate;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.WallpaperManager;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.service.wallpaper.WallpaperService;
import android.telephony.PhoneStateListener;
import android.telephony.SmsMessage;
import android.telephony.TelephonyManager;
import android.text.TextPaint;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import com.commind.facebook.FacebookProvider;
import com.commind.facebook.Facebookextension;
import com.commind.gmail.GmailNotifier;
import com.commind.twitter.TwitterClient;
import com.flurry.android.FlurryAgent;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

public class Bubble extends WallpaperService implements SharedPreferences.OnSharedPreferenceChangeListener {
    public static int P_E = 0;
    public static int P_F = 0;
    public static int P_G = 0;
    public static int P_P = 0;
    public static int P_S = 0;
    public static int P_T = 0;
    public static final String SHARED_BG = "background";
    public static final String SHARED_B_SOUND = "bubblesound";
    public static final String SHARED_FB = "facebook";
    public static final String SHARED_FBGAME = "facebookgame";
    public static final String SHARED_FBWALL = "facebookwall";
    public static final String SHARED_GM = "gmail";
    public static final String SHARED_LOCALGAME = "localgame";
    public static final String SHARED_LOCALGAME_SHOWPOP = "localgameshowpop";
    public static final String SHARED_LOCAL_TOTAL_BUBBLES = "localtotalbubbles";
    public static final String SHARED_MYDARLING = "mydarling";
    public static final String SHARED_NO_OF_BUBBLES = "noofbubbles";
    public static final String SHARED_PREFS_NAME = "bubblesettings";
    public static final String SHARED_SMS_SETTING = "SMS";
    public static final String SHARED_TOTAL_BUBBLES = "totalbubbles";
    public static final String SHARED_TW = "twitter";
    private static final int T_EMPTY = -1;
    public static final int T_FB_EVENT = 2;
    public static final int T_FB_FEED = 5;
    public static final int T_FB_FR = 4;
    public static final int T_FB_MES = 3;
    private static final int T_GM_MESS = 8;
    private static final int T_MYDARLING = -2;
    public static final int T_PHONE = 1;
    public static final int T_SMS = 0;
    private static final int T_TW_MENTION = 6;
    private static final int T_TW_MESS = 7;
    /* access modifiers changed from: private */
    public AnimateDrawable aD;
    /* access modifiers changed from: private */
    public boolean alwaysshowpop = false;
    /* access modifiers changed from: private */
    public Bitmap bgfile = null;
    /* access modifiers changed from: private */
    public long countpopps = 0;
    private final BroadcastReceiver facebookreceiver = new BroadcastReceiver() {
        public void onReceive(Context arg0, Intent intent) {
            String s;
            String s2;
            String s3;
            String s4;
            String s5;
            String s6;
            BubbleModel f_found = null;
            BubbleModel e_found = null;
            BubbleModel m_found = null;
            BubbleModel tw_found = null;
            BubbleModel tw_m_found = null;
            BubbleModel gm_m_found = null;
            SharedPreferences prefs = Bubble.this.getSharedPreferences(Bubble.SHARED_PREFS_NAME, 0);
            SharedPreferences.Editor editor = Bubble.this.getSharedPreferences(Bubble.SHARED_PREFS_NAME, 0).edit();
            if (Bubble.this.mBubbles == null) {
                Bubble.this.mBubbles = new ArrayList();
            }
            Iterator it = Bubble.this.mBubbles.iterator();
            while (it.hasNext()) {
                BubbleModel info = (BubbleModel) it.next();
                if (info.mType == 4) {
                    f_found = info;
                } else if (info.mType == 2) {
                    e_found = info;
                } else if (info.mType == 3) {
                    m_found = info;
                } else if (info.mType == Bubble.T_TW_MESS) {
                    tw_found = info;
                } else if (info.mType == Bubble.T_TW_MENTION) {
                    tw_m_found = info;
                } else if (info.mType == Bubble.T_GM_MESS) {
                    gm_m_found = info;
                }
            }
            if (intent.hasExtra(GmailNotifier.MESS_INTENT)) {
                String nos = intent.getStringExtra(GmailNotifier.MESS_INTENT);
                int no = -1;
                try {
                    no = Integer.parseInt(nos);
                } catch (NumberFormatException e) {
                }
                if ((no != prefs.getInt(GmailNotifier.MESS_INTENT, 0) || gm_m_found == null) && no >= 0) {
                    if (gm_m_found != null) {
                        Bubble.this.mBubbles.remove(gm_m_found);
                    }
                    if (no > 0) {
                        if (no == 1) {
                            s6 = "email";
                        } else {
                            s6 = "emails";
                        }
                        BubbleModel unused = Bubble.this.addBubble(Bubble.this, "You have " + nos + " unread " + s6, null, true, Bubble.T_GM_MESS, null, -1);
                    }
                    editor.putInt(GmailNotifier.MESS_INTENT, no);
                    editor.commit();
                }
            }
            if (intent.hasExtra(TwitterClient.MESS_INTENT)) {
                String no2 = intent.getStringExtra(TwitterClient.MESS_INTENT);
                if (tw_found != null) {
                    Bubble.this.mBubbles.remove(tw_found);
                }
                if (Integer.parseInt(no2) == 1) {
                    s5 = "message";
                } else {
                    s5 = "message's";
                }
                BubbleModel unused2 = Bubble.this.addBubble(Bubble.this, "You have " + no2 + " new " + s5, null, true, Bubble.T_TW_MESS, null, -1);
            }
            if (intent.hasExtra(TwitterClient.MENTION_INTENT)) {
                String no3 = intent.getStringExtra(TwitterClient.MENTION_INTENT);
                if (tw_m_found != null) {
                    Bubble.this.mBubbles.remove(tw_m_found);
                }
                if (Integer.parseInt(no3) == 1) {
                    s4 = TwitterClient.MENTION_INTENT;
                } else {
                    s4 = "mention's";
                }
                BubbleModel unused3 = Bubble.this.addBubble(Bubble.this, "You have " + no3 + " new " + s4, null, true, Bubble.T_TW_MENTION, null, -1);
            }
            if (intent.hasExtra(FacebookTask.notification_counts)) {
                int no4 = intent.getIntExtra(FacebookTask.notification_counts, 0);
                if ((no4 != prefs.getInt(FacebookTask.notification_counts, 0) || e_found == null) && no4 >= 0) {
                    if (e_found != null) {
                        Bubble.this.mBubbles.remove(e_found);
                    }
                    if (no4 > 0) {
                        if (no4 == 1) {
                            s3 = "event";
                        } else {
                            s3 = FacebookTask.notification_counts_t;
                        }
                        BubbleModel unused4 = Bubble.this.addBubble(Bubble.this, "You have " + Integer.toString(no4) + " new " + s3, null, true, 2, null, -1);
                    }
                    editor.putInt(FacebookTask.notification_counts, no4);
                    editor.commit();
                }
            }
            if (intent.hasExtra(FacebookTask.friend_requests_counts)) {
                int no5 = intent.getIntExtra(FacebookTask.friend_requests_counts, 0);
                if ((no5 != prefs.getInt(FacebookTask.friend_requests_counts, 0) || f_found == null) && no5 >= 0) {
                    if (f_found != null) {
                        Bubble.this.mBubbles.remove(f_found);
                    }
                    if (no5 > 0) {
                        if (no5 == 1) {
                            s2 = "friend request";
                        } else {
                            s2 = FacebookTask.friend_requests_counts_t;
                        }
                        BubbleModel unused5 = Bubble.this.addBubble(Bubble.this, "You have " + Integer.toString(no5) + " new " + s2, null, true, 4, null, -1);
                    }
                    editor.putInt(FacebookTask.friend_requests_counts, no5);
                    editor.commit();
                }
            }
            if (intent.hasExtra("messages")) {
                int no6 = intent.getIntExtra("messages", 0);
                if ((no6 != prefs.getInt("messages", 0) || m_found == null) && no6 >= 0) {
                    if (m_found != null) {
                        Bubble.this.mBubbles.remove(m_found);
                    }
                    if (no6 > 0) {
                        if (no6 == 1) {
                            s = "message";
                        } else {
                            s = "messages";
                        }
                        BubbleModel unused6 = Bubble.this.addBubble(Bubble.this, "You have " + Integer.toString(no6) + " new " + s, null, true, 3, null, -1);
                    }
                    editor.putInt("messages", no6);
                    editor.commit();
                }
            }
        }
    };
    /* access modifiers changed from: private */
    public boolean freeze = false;
    private PendingIntent mAlarmSender;
    /* access modifiers changed from: private */
    public ArrayList<BubbleModel> mBubbles = null;
    /* access modifiers changed from: private */
    public String mColorOfbubble = "";
    /* access modifiers changed from: private */
    public int mH;
    /* access modifiers changed from: private */
    public final Handler mHandler = new Handler();
    /* access modifiers changed from: private */
    public BubbleModel mHitBubble;
    /* access modifiers changed from: private */
    public int mNoOfBubbles = 0;
    /* access modifiers changed from: private */
    public float mOffset;
    /* access modifiers changed from: private */
    public boolean mRinging = false;
    private TelephonyManager mTm;
    /* access modifiers changed from: private */
    public boolean mUseGame = false;
    /* access modifiers changed from: private */
    public boolean mUseLocalGame = true;
    /* access modifiers changed from: private */
    public boolean mUseSms = true;
    /* access modifiers changed from: private */
    public boolean mUseSound = false;
    /* access modifiers changed from: private */
    public int mW;
    /* access modifiers changed from: private */
    public MediaPlayer mp;
    private final PhoneStateListener phonestatelistener = new PhoneStateListener() {
        public void onCallStateChanged(int state, String incomingNumber) {
            String dsp;
            String no;
            ContentResolver cr;
            Cursor cursor;
            Log.i("STATE", Integer.toString(state));
            if (!Bubble.this.mUseSms) {
                return;
            }
            if (state == 1) {
                Bubble.this.mRinging = true;
            } else if (state == 2) {
                Bubble.this.mRinging = false;
            } else if (Bubble.this.mRinging) {
                Bubble.this.mRinging = false;
                if (incomingNumber == null) {
                    return;
                }
                if (Bubble.this.getContentResolver().query(CallLog.Calls.CONTENT_URI, null, "type = ? AND new = ? AND number = ?", new String[]{Integer.toString(3), "1", incomingNumber}, "date DESC ") != null) {
                    Uri url = ContactsContract.PhoneLookup.CONTENT_FILTER_URI;
                    String[] projection = {FacebookProvider.KEY_ID, "display_name"};
                    Uri uri = Uri.withAppendedPath(url, Uri.encode(incomingNumber));
                    Bitmap facebm = null;
                    long lid = -1;
                    if (incomingNumber == null || incomingNumber.length() <= 1 || (cursor = (cr = Bubble.this.getContentResolver()).query(uri, projection, null, null, null)) == null) {
                        dsp = null;
                    } else {
                        if (cursor.moveToFirst()) {
                            String id = cursor.getString(0);
                            dsp = cursor.getString(1);
                            if (id != null) {
                                lid = Long.parseLong(id);
                            }
                            facebm = ContactUtil.loadContactPhoto(cr, ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, lid));
                        } else {
                            dsp = null;
                        }
                        cursor.close();
                    }
                    if (incomingNumber == null || incomingNumber.length() <= 0) {
                        incomingNumber = "Unknown";
                    } else if (lid == -1 && (no = Bubble.getOnlyNumerics(incomingNumber)) != null && no.length() > 1) {
                        try {
                            lid = Long.parseLong(no);
                        } catch (NumberFormatException e) {
                            lid = -1;
                        }
                    }
                    BubbleModel unused = Bubble.this.addBubble(Bubble.this, dsp, incomingNumber, true, 1, facebm, lid);
                }
            }
        }
    };
    /* access modifiers changed from: private */
    public boolean showpop = false;
    private final BroadcastReceiver smsreceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            Bundle extras;
            String fromAddress;
            String dsp;
            String no;
            ContentResolver cr;
            Cursor cursor;
            SharedPreferences prefs = context.getSharedPreferences(Bubble.SHARED_PREFS_NAME, 0);
            boolean usesms = true;
            if (prefs.contains(Bubble.SHARED_SMS_SETTING)) {
                usesms = prefs.getBoolean(Bubble.SHARED_SMS_SETTING, true);
            }
            if (usesms && (extras = intent.getExtras()) != null) {
                Object[] pdus = (Object[]) extras.get("pdus");
                for (int i = 0; i < pdus.length; i++) {
                    SmsMessage message = SmsMessage.createFromPdu((byte[]) pdus[i]);
                    if (!(message == null || (fromAddress = message.getOriginatingAddress()) == null)) {
                        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(fromAddress));
                        String[] projection = {FacebookProvider.KEY_ID, "display_name"};
                        Bitmap facebm = null;
                        long lid = -1;
                        if (fromAddress == null || fromAddress.length() <= 1 || (cursor = (cr = context.getContentResolver()).query(uri, projection, null, null, null)) == null) {
                            dsp = null;
                        } else {
                            if (cursor.moveToFirst()) {
                                String id = cursor.getString(0);
                                dsp = cursor.getString(1);
                                if (id != null) {
                                    lid = Long.parseLong(id);
                                }
                                facebm = ContactUtil.loadContactPhoto(cr, ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, lid));
                            } else {
                                dsp = null;
                            }
                            cursor.close();
                        }
                        if (fromAddress == null || fromAddress.length() <= 0) {
                            fromAddress = "Unknown";
                        } else if (lid == -1 && (no = Bubble.getOnlyNumerics(fromAddress)) != null && no.length() > 1) {
                            try {
                                lid = Long.parseLong(no);
                            } catch (NumberFormatException e) {
                                lid = -1;
                            }
                        }
                        BubbleModel b = Bubble.this.addBubble(context, dsp, fromAddress, true, 0, facebm, lid);
                        if (!(b == null || fromAddress == null || fromAddress.equals("Unknown"))) {
                            long id2 = ContactUtil.findThreadIdFromAddress(context, fromAddress);
                            if (id2 > 0) {
                                b.smsthreadid = id2;
                            }
                        }
                    }
                }
            }
        }
    };

    public static String getOnlyNumerics(String str) {
        if (str == null) {
            return null;
        }
        StringBuffer strBuff = new StringBuffer();
        int len = str.length();
        for (int i = 0; i < len; i++) {
            char c = str.charAt(i);
            if (Character.isDigit(c)) {
                strBuff.append(c);
            }
        }
        return strBuff.toString();
    }

    /* access modifiers changed from: private */
    public void findbubble(int x, int y) {
        if (this.mBubbles != null && this.mBubbles.size() > 0) {
            Iterator<BubbleModel> it = this.mBubbles.iterator();
            while (it.hasNext()) {
                BubbleModel info = it.next();
                if (findbubble(info, x, y)) {
                    if (!this.freeze) {
                        this.mBubbles.remove(info);
                        if (this.mUseGame) {
                            if (info.mType == 0) {
                                P_S++;
                            } else if (info.mType == 1) {
                                P_P++;
                            } else if (info.mType == 4 || info.mType == 3 || info.mType == 2 || info.mType == 5) {
                                P_F++;
                            } else if (info.mType == T_TW_MENTION || info.mType == T_TW_MESS) {
                                P_T++;
                            } else if (info.mType == T_GM_MESS) {
                                P_G++;
                            } else {
                                P_E++;
                            }
                        }
                        if (info.mType == -1) {
                            BubbleModel info2 = addBubble(this, "", null, true, -1, null, -1);
                            return;
                        } else if (!this.freeze && info.mType == T_MYDARLING) {
                            addBubble(this, "", info.mPhoneNo, true, T_MYDARLING, null, -1).smsthreadid = info.smsthreadid;
                            return;
                        } else {
                            return;
                        }
                    } else {
                        return;
                    }
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, int):void}
     arg types: [int, int, int, int]
     candidates:
      ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, long):void}
      ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, int):void} */
    private boolean findbubble(BubbleModel info, int x, int y) {
        BitmapDrawable bd;
        int color;
        Bitmap overlay;
        int w = info.mBitmapW;
        int h = info.mBitmapH;
        int searchx = (int) (info.xCord - 5.0f);
        int searchx2 = (int) (info.xCord + ((float) w) + 5.0f);
        int searchy = (int) (info.yCord - 5.0f);
        int searchy2 = (int) (info.yCord + ((float) h) + 5.0f);
        if (x > searchx && x < searchx2 && y > searchy && y < searchy2) {
            if (this.freeze || info.mType != T_MYDARLING) {
                this.freeze = false;
                Resources res = getResources();
                if (info.mType == T_MYDARLING) {
                    if (x <= (w / 2) + searchx || x >= searchx2 || y <= searchy || y >= searchy2) {
                        info.smsid = -1;
                    } else {
                        info.smsid = 1;
                        if (info.smsthreadid <= 0) {
                            long id = ContactUtil.findThreadIdFromAddress(this, info.mPhoneNo);
                            if (id > 0) {
                                info.smsthreadid = id;
                            }
                        }
                    }
                    bd = (BitmapDrawable) res.getDrawable(R.drawable.grey_hearts_pop);
                } else {
                    bd = (BitmapDrawable) res.getDrawable(R.drawable.pop_grey);
                }
                if (bd != null) {
                    Bitmap scaledbm = bd.getBitmap();
                    int ww = scaledbm.getWidth();
                    int hh = scaledbm.getHeight();
                    Bitmap bm = Bitmap.createBitmap(ww, hh, Bitmap.Config.ARGB_8888);
                    Canvas canvas = new Canvas(bm);
                    if (info.mType == T_MYDARLING) {
                        color = BubbleModel.BUBBLE_COLORS[info.mcolor];
                    } else {
                        color = BubbleModel.BUBBLE_COLORS[info.mcolor];
                    }
                    BubbleModel.generateBubbleColor(canvas, scaledbm, color);
                    Paint ps = new Paint();
                    ps.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_OVER));
                    if (info.mType == T_MYDARLING) {
                        overlay = ((BitmapDrawable) res.getDrawable(R.drawable.pop_white_hearts)).getBitmap();
                    } else {
                        overlay = ((BitmapDrawable) res.getDrawable(R.drawable.pop_grey_white)).getBitmap();
                    }
                    canvas.drawBitmap(overlay, 0.0f, 0.0f, ps);
                    int scw = Math.min(w, ww);
                    int sch = Math.min(h, hh);
                    if (this.mUseLocalGame) {
                        this.countpopps = this.countpopps + 1;
                    }
                    if (this.showpop || this.alwaysshowpop) {
                        this.showpop = false;
                        String popcount = String.valueOf(this.countpopps);
                        TextPaint mTp = new TextPaint();
                        mTp.setAntiAlias(true);
                        int dpi = bm.getDensity();
                        if (dpi == 120) {
                            mTp.setTextSize(8.0f);
                        } else if (dpi == 240) {
                            mTp.setTextSize(12.0f);
                        } else {
                            mTp.setTextSize(10.0f);
                        }
                        mTp.setColor(-1);
                        mTp.setShadowLayer(2.0f, 0.0f, 0.0f, -16777216);
                        mTp.setTextAlign(Paint.Align.CENTER);
                        float textheight = (-mTp.ascent()) + mTp.descent();
                        canvas.drawText(popcount, (float) (ww / 2), ((float) (hh / 2)) + (textheight / 4.0f), mTp);
                    }
                    BitmapDrawable bd2 = new BitmapDrawable(bm);
                    info.mBitmap = bd2.getBitmap();
                    bd2.setBounds(0, 0, scw, sch);
                    Animation a = AnimationUtils.loadAnimation(this, R.anim.grow);
                    a.initialize(scw, sch, this.mW, this.mH);
                    if (ww >= w) {
                        this.aD = new AnimateDrawable(bd2, a, info.xCord, info.yCord);
                    } else if (info.mType == -1) {
                        this.aD = new AnimateDrawable(bd2, a, info.xCord + ((float) (scw / 3)), info.yCord + ((float) (sch / 3)));
                    } else {
                        this.aD = new AnimateDrawable(bd2, a, info.xCord + ((float) (ww / 2)), info.yCord + ((float) (hh / 2)));
                    }
                    this.mHitBubble = info;
                    if (this.mUseSound) {
                        if (this.mp == null) {
                            this.mp = MediaPlayer.create(this, R.raw.pop);
                        }
                        if (this.mp != null) {
                            this.mp.start();
                        }
                    }
                    return true;
                }
            } else {
                this.freeze = true;
                return true;
            }
        }
        this.freeze = false;
        return false;
    }

    public void onCreate() {
        super.onCreate();
        this.mTm = (TelephonyManager) getSystemService("phone");
        this.mTm.listen(this.phonestatelistener, 32);
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.provider.Telephony.SMS_RECEIVED");
        registerReceiver(this.smsreceiver, filter);
        IntentFilter filter2 = new IntentFilter();
        filter2.addAction(FacebookTask.FACEBOOK_RECEIVER);
        registerReceiver(this.facebookreceiver, filter2);
    }

    /* access modifiers changed from: private */
    public void registerAlarmManager() {
        if (this.mAlarmSender == null) {
            this.mAlarmSender = PendingIntent.getService(this, 0, new Intent(this, Poller.class), 0);
            ((AlarmManager) getSystemService("alarm")).setInexactRepeating(1, System.currentTimeMillis(), 900000, this.mAlarmSender);
        }
        new Poller().makeAsyncReq(this);
    }

    private void unregisterAlarmManager() {
        if (this.mAlarmSender != null) {
            ((AlarmManager) getSystemService("alarm")).cancel(this.mAlarmSender);
            this.mAlarmSender = null;
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.mTm != null) {
            this.mTm.listen(this.phonestatelistener, 0);
        }
        unregisterReceiver(this.smsreceiver);
        unregisterReceiver(this.facebookreceiver);
        unregisterAlarmManager();
        if (this.mp != null) {
            this.mp.release();
        }
        this.mp = null;
        if (this.mBubbles != null) {
            this.mBubbles.clear();
        }
        this.mBubbles = null;
    }

    public WallpaperService.Engine onCreateEngine() {
        return new CubeEngine();
    }

    /* access modifiers changed from: private */
    public void updatenoofbubbles(Context c, int n) {
        if (this.mBubbles != null) {
            ArrayList<BubbleModel> temp = new ArrayList<>();
            Iterator<BubbleModel> it = this.mBubbles.iterator();
            while (it.hasNext()) {
                BubbleModel b = it.next();
                if (b.mType == -1) {
                    temp.add(b);
                }
            }
            if (temp.size() > 0) {
                this.mBubbles.removeAll(temp);
            }
            for (int no = n; no > 0; no--) {
                addBubble(c, "", null, true, -1, null, -1);
            }
        }
    }

    private Bitmap getBubble(int type, int[] c) {
        BitmapDrawable bd;
        Bitmap scaledbm = null;
        double d = Math.random();
        String colorstring = this.mColorOfbubble;
        int color = BubbleModel.DEFAULT_COLOR;
        ArrayList<String> array = new ArrayList<>();
        while (colorstring.length() > 0) {
            String cs = colorstring.substring(0, colorstring.indexOf(","));
            colorstring = colorstring.replaceFirst(String.valueOf(cs) + ",", "");
            array.add(cs);
        }
        if (array.size() >= 1) {
            color = Integer.parseInt(array.get(new Random().nextInt(array.size())));
        }
        double d2 = Math.max(d, 0.5d);
        if (type == 0) {
            bd = (BitmapDrawable) getResources().getDrawable(R.drawable.white_sms);
        } else if (type == 1) {
            bd = (BitmapDrawable) getResources().getDrawable(R.drawable.white_missed_call);
        } else if (type == 4 || type == 3 || type == 2 || type == 5) {
            bd = (BitmapDrawable) getResources().getDrawable(R.drawable.facebook_single);
        } else if (type == T_TW_MENTION || type == T_TW_MESS) {
            bd = (BitmapDrawable) getResources().getDrawable(R.drawable.twitter_single);
        } else if (type == T_GM_MESS) {
            bd = (BitmapDrawable) getResources().getDrawable(R.drawable.gmail_single);
        } else {
            bd = (BitmapDrawable) getResources().getDrawable(R.drawable.bubble_big2_white);
            scaledbm = Bitmap.createScaledBitmap(bd.getBitmap(), (int) (((double) bd.getIntrinsicWidth()) * d2), (int) (((double) bd.getIntrinsicHeight()) * d2), true);
        }
        if (c != null) {
            c[0] = color;
        }
        if (scaledbm != null || bd == null) {
            return scaledbm;
        }
        return bd.getBitmap();
    }

    private Bitmap generateDarlingBitmap() {
        Bitmap darling;
        Resources res = getResources();
        File f = getFileStreamPath("darling.png");
        Bitmap bm = ((BitmapDrawable) res.getDrawable(R.drawable.heart_noring_grey)).getBitmap();
        Bitmap mask = ((BitmapDrawable) res.getDrawable(R.drawable.heart_mask)).getBitmap();
        DisplayMetrics mMetrics = res.getDisplayMetrics();
        Bitmap b = Bitmap.createBitmap(mask.getScaledWidth(mMetrics), mask.getScaledHeight(mMetrics), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(b);
        Bitmap overlay = ((BitmapDrawable) res.getDrawable(R.drawable.heart_noring_white)).getBitmap();
        Paint p = new Paint(1);
        if (!(f == null || !f.exists() || (darling = BitmapFactory.decodeFile(f.getAbsolutePath())) == null)) {
            canvas.drawBitmap(Bitmap.createScaledBitmap(darling, mask.getScaledWidth(mMetrics), mask.getScaledHeight(mMetrics), false), 0.0f, 0.0f, p);
            p.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
            canvas.drawBitmap(mask, 0.0f, 0.0f, p);
        }
        BubbleModel.generateBubbleColor(canvas, bm, BubbleModel.BUBBLE_COLORS[getSharedPreferences(SHARED_PREFS_NAME, 0).getInt("hearts_color", 0)]);
        p.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_OVER));
        canvas.drawBitmap(overlay, 0.0f, 0.0f, p);
        return b;
    }

    /* access modifiers changed from: private */
    public BubbleModel addBubble(Context c, String s, String pno, boolean addbubble, int type, Bitmap fbm, long lid) {
        Bitmap mainbm;
        BubbleModel b = null;
        if (this.mBubbles == null) {
            this.mBubbles = new ArrayList<>();
        }
        DisplayMetrics mMetrics = c.getResources().getDisplayMetrics();
        if (type == T_MYDARLING) {
            Bitmap darling = generateDarlingBitmap();
            if (darling != null) {
                BubbleModel bubble = new BubbleModel(this.mW, this.mH, darling, null, null, pno, type, fbm, true, mMetrics, null, lid, false, c.getSharedPreferences(SHARED_PREFS_NAME, 0).getInt("hearts_color", 0));
                if (!(!addbubble || this.mBubbles == null || bubble == null)) {
                    this.mBubbles.add(0, bubble);
                }
                b = bubble;
            }
        } else {
            if (type == 0 || type == 1) {
                Iterator<BubbleModel> it = this.mBubbles.iterator();
                while (it.hasNext()) {
                    BubbleModel bb = it.next();
                    if (lid == bb.mid && lid != -1 && type == bb.mType) {
                        return null;
                    }
                }
            }
            int[] color = new int[1];
            Bitmap scaledbm = getBubble(type, color);
            if (s == null && pno != null) {
                s = pno;
            }
            if (s == null) {
                return null;
            }
            Bitmap mBitmapMask = null;
            if (!(fbm == null || scaledbm == null)) {
                mBitmapMask = ((BitmapDrawable) c.getResources().getDrawable(R.drawable.mask)).getBitmap();
                fbm = Bitmap.createScaledBitmap(fbm, mBitmapMask.getScaledWidth(mMetrics), mBitmapMask.getScaledHeight(mMetrics), false);
            }
            if (scaledbm != null) {
                boolean large = type == 0 || type == 1;
                if (large) {
                    mainbm = ((BitmapDrawable) c.getResources().getDrawable(R.drawable.main_bubble)).getBitmap();
                } else if (type == -1) {
                    mainbm = Bitmap.createScaledBitmap(((BitmapDrawable) c.getResources().getDrawable(R.drawable.bubble_big2)).getBitmap(), scaledbm.getWidth(), scaledbm.getHeight(), true);
                } else {
                    mainbm = ((BitmapDrawable) c.getResources().getDrawable(R.drawable.main_bubble_single)).getBitmap();
                }
                BubbleModel bubbleModel = new BubbleModel(this.mW, this.mH, mainbm, scaledbm, s, pno, type, fbm, false, mMetrics, mBitmapMask, lid, large, color[0]);
                if (!(!addbubble || this.mBubbles == null || bubbleModel == null)) {
                    this.mBubbles.add(bubbleModel);
                }
                b = bubbleModel;
            }
        }
        return b;
    }

    /* access modifiers changed from: private */
    public void setCurrentWallpaer(Context c) {
        this.bgfile = ((BitmapDrawable) WallpaperManager.getInstance(c).getDrawable()).getBitmap();
    }

    class CubeEngine extends WallpaperService.Engine {
        private final Runnable mDrawCube = new Runnable() {
            public void run() {
                CubeEngine.this.drawFrame();
            }
        };
        private boolean mVisible;

        CubeEngine() {
            super(Bubble.this);
        }

        public void onCreate(SurfaceHolder surfaceHolder) {
            super.onCreate(surfaceHolder);
            FlurryAgent.onStartSession(Bubble.this, "QCZU9QYX441QFNNYKCQV");
        }

        public void onDestroy() {
            super.onDestroy();
            Bubble.this.mHandler.removeCallbacks(this.mDrawCube);
            FlurryAgent.onEndSession(Bubble.this);
        }

        public void onVisibilityChanged(boolean visible) {
            this.mVisible = visible;
            if (visible) {
                drawFrame();
                return;
            }
            if (Bubble.this.mp != null) {
                Bubble.this.mp.release();
            }
            Bubble.this.mp = null;
            Bubble.this.mHandler.removeCallbacks(this.mDrawCube);
            if (Bubble.this.mUseLocalGame) {
                Bubble.this.showpop = true;
                Bubble.this.getSharedPreferences(Bubble.SHARED_PREFS_NAME, 0).edit().putLong(Bubble.SHARED_LOCAL_TOTAL_BUBBLES, Bubble.this.countpopps).commit();
            }
        }

        public void onSurfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            super.onSurfaceChanged(holder, format, width, height);
            Bubble.this.mW = width;
            Bubble.this.mH = height;
            if (Bubble.this.mBubbles == null) {
                Bubble.this.mBubbles = new ArrayList();
            }
            if (Bubble.this.mUseSms) {
                Bubble.this.mBubbles.size();
            }
            if (Bubble.this.mBubbles.size() > 0) {
                DisplayMetrics mMetrics = Bubble.this.getResources().getDisplayMetrics();
                Iterator it = Bubble.this.mBubbles.iterator();
                while (it.hasNext()) {
                    BubbleModel b = (BubbleModel) it.next();
                    new BubbleModel(Bubble.this.mW, Bubble.this.mH, b.mBitmap, null, b.mString, b.mPhoneNo, b.mType, null, true, mMetrics, null, b.mid, false, b.mcolor);
                }
            } else {
                SharedPreferences prefs = Bubble.this.getSharedPreferences(Bubble.SHARED_PREFS_NAME, 0);
                Bubble.this.mColorOfbubble = prefs.getString("bubbles_color2", "2,");
                if (prefs.contains(Bubble.SHARED_NO_OF_BUBBLES)) {
                    Bubble.this.mNoOfBubbles = prefs.getInt(Bubble.SHARED_NO_OF_BUBBLES, 0);
                    Bubble.this.updatenoofbubbles(Bubble.this, Bubble.this.mNoOfBubbles);
                } else {
                    Bubble.this.mNoOfBubbles = 10;
                    Bubble.this.updatenoofbubbles(Bubble.this, Bubble.this.mNoOfBubbles);
                }
                String phoneno = prefs.getString(Bubble.SHARED_MYDARLING, "");
                if (phoneno != null && phoneno.length() > 0) {
                    BubbleModel unused = Bubble.this.addBubble(Bubble.this, "", phoneno, true, Bubble.T_MYDARLING, null, -1);
                }
            }
            SharedPreferences prefs2 = Bubble.this.getSharedPreferences(Bubble.SHARED_PREFS_NAME, 0);
            Bubble.this.mUseSms = prefs2.getBoolean(Bubble.SHARED_SMS_SETTING, true);
            Bubble.this.mUseSound = prefs2.getBoolean(Bubble.SHARED_B_SOUND, false);
            Bubble.this.mUseGame = prefs2.getBoolean(Bubble.SHARED_FBGAME, false);
            Bubble.this.mUseLocalGame = prefs2.getBoolean(Bubble.SHARED_LOCALGAME, true);
            if (Bubble.this.mUseLocalGame) {
                Bubble.this.countpopps = prefs2.getLong(Bubble.SHARED_LOCAL_TOTAL_BUBBLES, 0);
                Bubble.this.alwaysshowpop = prefs2.getBoolean(Bubble.SHARED_LOCALGAME_SHOWPOP, true);
                Bubble.this.showpop = true;
            }
            boolean mFbLoggedIn = prefs2.getBoolean(Bubble.SHARED_FB, false);
            boolean mTwLoggedIn = prefs2.getBoolean(Bubble.SHARED_TW, false);
            boolean mGmLoggedIn = prefs2.getBoolean(Bubble.SHARED_GM, false);
            if (mFbLoggedIn && Facebookextension.getAccesToken(Bubble.this) == null) {
                mFbLoggedIn = false;
            }
            if (!prefs2.getBoolean(Bubble.SHARED_BG, true)) {
                Bubble.this.bgfile = null;
            } else {
                Bubble.this.setCurrentWallpaer(Bubble.this);
            }
            FlurryAgent.logEvent("usesms", Bubble.this.mUseSms);
            FlurryAgent.logEvent(Bubble.SHARED_NO_OF_BUBBLES + String.valueOf(Bubble.this.mNoOfBubbles));
            FlurryAgent.logEvent("fb login", mFbLoggedIn);
            FlurryAgent.logEvent("tw login", mTwLoggedIn);
            FlurryAgent.logEvent("gm login", mGmLoggedIn);
            FlurryAgent.logEvent("bs enable", Bubble.this.mUseSound);
            FlurryAgent.logEvent("local enable", Bubble.this.mUseLocalGame);
            FlurryAgent.logEvent("show pop enable", Bubble.this.alwaysshowpop);
            prefs2.registerOnSharedPreferenceChangeListener(Bubble.this);
            if (mFbLoggedIn || mTwLoggedIn || mGmLoggedIn) {
                Bubble.this.registerAlarmManager();
            }
            drawFrame();
        }

        public void onSurfaceCreated(SurfaceHolder holder) {
            super.onSurfaceCreated(holder);
        }

        public void onSurfaceDestroyed(SurfaceHolder holder) {
            super.onSurfaceDestroyed(holder);
            this.mVisible = false;
            Bubble.this.mHandler.removeCallbacks(this.mDrawCube);
        }

        public void onOffsetsChanged(float xOffset, float yOffset, float xStep, float yStep, int xPixels, int yPixels) {
            Bubble.this.mOffset = (float) xPixels;
            drawFrame();
        }

        public void onTouchEvent(MotionEvent event) {
            super.onTouchEvent(event);
        }

        public Bundle onCommand(String action, int x, int y, int z, Bundle extras, boolean resultRequested) {
            if (action.equals("android.wallpaper.tap")) {
                boolean freezefound = false;
                if (Bubble.this.freeze) {
                    freezefound = true;
                }
                Bubble.this.findbubble(x, y);
                if (freezefound) {
                    Bubble.this.freeze = false;
                    drawFrame();
                }
            }
            return super.onCommand(action, x, y, z, extras, resultRequested);
        }

        /* access modifiers changed from: package-private */
        public void drawFrame() {
            SurfaceHolder holder = getSurfaceHolder();
            Canvas c = null;
            try {
                c = holder.lockCanvas();
                if (c != null) {
                    drawCube(c);
                }
                Bubble.this.mHandler.removeCallbacks(this.mDrawCube);
                if (this.mVisible && !Bubble.this.freeze) {
                    Bubble.this.mHandler.postDelayed(this.mDrawCube, 25);
                }
            } finally {
                if (c != null) {
                    holder.unlockCanvasAndPost(c);
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void drawCube(Canvas c) {
            if (Bubble.this.bgfile != null) {
                c.drawBitmap(Bubble.this.bgfile, Bubble.this.mOffset, 0.0f, (Paint) null);
            } else {
                c.drawColor(-16777216);
            }
            if (Bubble.this.mBubbles != null) {
                int size = Bubble.this.mBubbles.size() - 1;
                ArrayList<BubbleModel> tempbubbles = null;
                int startsearch = Bubble.this.mH - (Bubble.this.mH / 5);
                for (int i = size; i >= 0; i--) {
                    BubbleModel b = (BubbleModel) Bubble.this.mBubbles.get(i);
                    float x = -1.0f;
                    float y = -1.0f;
                    boolean slowdown = false;
                    if ((!b.reversed && b.yCord < ((float) startsearch) && b.yCord > 0.0f) || b.reversed) {
                        Rect rect = new Rect((int) b.xCord, (int) b.yCord, ((int) b.xCord) + b.mBitmapW, ((int) b.yCord) + b.mBitmapH);
                        Iterator it = Bubble.this.mBubbles.iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                break;
                            }
                            BubbleModel info = (BubbleModel) it.next();
                            if (info != b) {
                                int searchx = (int) (info.xCord - 5.0f);
                                int searchx2 = (int) (info.xCord + ((float) info.mBitmapW));
                                int searchy = (int) (info.yCord - 5.0f);
                                int searchy2 = (int) (info.yCord + ((float) info.mBitmapH));
                                if (b.reversed) {
                                    if (rect.intersects(searchx, searchy2, searchx2, searchy2)) {
                                        slowdown = true;
                                        break;
                                    }
                                } else if (rect.intersects(searchx, searchy, searchx2, searchy2)) {
                                    x = b.xCord;
                                    y = b.yCord;
                                    break;
                                }
                            }
                        }
                    }
                    boolean destroy = false;
                    if (b.checkread && (b.mType == 1 || b.mType == 0)) {
                        if (b.mType == 0 && b.smsid <= 0) {
                            b.smsid = ContactUtil.findMessageId(Bubble.this, b.smsthreadid);
                        }
                        destroy = ContactUtil.checkifRead(b.mType, b.mPhoneNo, Bubble.this, b.smsid);
                        if (destroy) {
                            if (tempbubbles == null) {
                                tempbubbles = new ArrayList<>();
                            }
                            tempbubbles.add(b);
                        }
                        b.checkread = false;
                    }
                    if (!destroy) {
                        b.updatePath(x, y, slowdown);
                        c.drawBitmap(b.mBitmap, b.xCord, b.yCord, (Paint) null);
                        if (b.mType == Bubble.T_MYDARLING && Bubble.this.freeze) {
                            Resources res = Bubble.this.getResources();
                            Bitmap phone = ((BitmapDrawable) res.getDrawable(R.drawable.white_missed_call_small)).getBitmap();
                            Bitmap sms = ((BitmapDrawable) res.getDrawable(R.drawable.white_sms_small)).getBitmap();
                            c.drawBitmap(phone, b.xCord, b.yCord + ((float) (b.mBitmapH / 2)), (Paint) null);
                            c.drawBitmap(sms, b.xCord + ((float) (b.mBitmapW - sms.getWidth())), b.yCord + ((float) (b.mBitmapH / 2)), (Paint) null);
                        }
                    }
                }
                if (tempbubbles != null) {
                    Bubble.this.mBubbles.removeAll(tempbubbles);
                }
            }
            if (Bubble.this.aD == null || Bubble.this.aD.hasEnded()) {
                Bubble.this.aD = null;
                if (Bubble.this.mHitBubble != null) {
                    long id = -1;
                    long smsid = -1;
                    int t = Bubble.this.mHitBubble.mType;
                    if (t == 0) {
                        id = Bubble.this.mHitBubble.smsthreadid;
                    } else if (t == Bubble.T_MYDARLING) {
                        id = Bubble.this.mHitBubble.smsthreadid;
                        smsid = Bubble.this.mHitBubble.smsid;
                    }
                    Bubble.this.launchNo(Bubble.this.mHitBubble.mPhoneNo, t, id, smsid);
                    Bubble.this.mHitBubble = null;
                    return;
                }
                return;
            }
            Bubble.this.aD.draw(c);
        }
    }

    public void launchNo(String no, int type, long threadid, long smsid) {
        switch (type) {
            case T_MYDARLING /*-2*/:
                if (smsid != 1) {
                    Intent intent = new Intent("android.intent.action.CALL");
                    intent.setFlags(268435456);
                    intent.setData(Uri.parse("tel:" + no));
                    try {
                        startActivity(intent);
                        return;
                    } catch (ActivityNotFoundException e) {
                        return;
                    }
                } else if (threadid > 0) {
                    Intent is = new Intent();
                    is.setAction("android.intent.action.VIEW");
                    is.setFlags(268435456);
                    is.setData(Uri.withAppendedPath(ContactUtil.CONVERSATION_CONTENT_URI, Long.toString(threadid)));
                    try {
                        startActivity(is);
                        return;
                    } catch (ActivityNotFoundException e2) {
                        return;
                    }
                } else {
                    return;
                }
            case -1:
            case T_FB_FEED /*5*/:
            default:
                return;
            case T_SMS /*0*/:
                if (threadid > 0) {
                    Intent is2 = new Intent();
                    is2.setAction("android.intent.action.VIEW");
                    is2.setFlags(268435456);
                    is2.setData(Uri.withAppendedPath(ContactUtil.CONVERSATION_CONTENT_URI, Long.toString(threadid)));
                    try {
                        startActivity(is2);
                        return;
                    } catch (ActivityNotFoundException e3) {
                        return;
                    }
                } else {
                    return;
                }
            case 1:
                Intent i = new Intent();
                i.setAction("android.intent.action.VIEW");
                i.setFlags(268435456);
                i.setType("vnd.android.cursor.dir/calls");
                startActivity(i);
                return;
            case 2:
                startFacebook("facebook://facebook.com/notifications");
                return;
            case T_FB_MES /*3*/:
                startFacebook("facebook://facebook.com/inbox");
                return;
            case T_FB_FR /*4*/:
                startFacebook("facebook://facebook.com/notifications");
                return;
            case T_TW_MENTION /*6*/:
                startTwitter("twitter://mentions");
                return;
            case T_TW_MESS /*7*/:
                startTwitter("twitter://messages");
                return;
            case T_GM_MESS /*8*/:
                startGmail(null);
                return;
        }
    }

    private void startGmail(String uri) {
        boolean result = false;
        if (appInstalledOrNot("com.google.android.gm")) {
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setClassName("com.google.android.gm", "com.google.android.gm.ConversationListActivity");
            intent.setFlags(268435456);
            try {
                startActivity(intent);
                result = true;
            } catch (ActivityNotFoundException e) {
            }
        }
        if (!result) {
            Intent intent2 = new Intent("android.intent.action.VIEW", Uri.parse("http://m.gmail.com/"));
            intent2.setFlags(268435456);
            startActivity(intent2);
        }
    }

    private void startFacebook(String uri) {
        boolean result = false;
        if (appInstalledOrNot("com.facebook.katana")) {
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(uri));
            intent.setFlags(268435456);
            try {
                startActivity(intent);
                result = true;
            } catch (ActivityNotFoundException e) {
            }
        }
        if (!result) {
            Intent intent2 = new Intent("android.intent.action.VIEW", Uri.parse("https://m.facebook.com/"));
            intent2.setFlags(268435456);
            startActivity(intent2);
        }
    }

    private void startTwitter(String uri) {
        boolean result = false;
        if (appInstalledOrNot("com.twitter.android")) {
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(uri));
            intent.setFlags(268435456);
            try {
                startActivity(intent);
                result = true;
            } catch (ActivityNotFoundException e) {
            }
        }
        if (!result) {
            Intent intent2 = new Intent("android.intent.action.VIEW", Uri.parse("http://mobile.twitter.com/"));
            intent2.setFlags(268435456);
            startActivity(intent2);
        }
    }

    private boolean appInstalledOrNot(String uri) {
        try {
            getPackageManager().getPackageInfo(uri, 1);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(SHARED_SMS_SETTING)) {
            this.mUseSms = sharedPreferences.getBoolean(SHARED_SMS_SETTING, false);
        } else if (key.equals(SHARED_B_SOUND)) {
            this.mUseSound = sharedPreferences.getBoolean(SHARED_B_SOUND, false);
        } else if (key.equals(SHARED_FBGAME)) {
            this.mUseGame = sharedPreferences.getBoolean(SHARED_FBGAME, false);
        } else if (key.equals(SHARED_LOCALGAME)) {
            this.mUseLocalGame = sharedPreferences.getBoolean(SHARED_LOCALGAME, true);
            if (!this.mUseLocalGame) {
                this.showpop = false;
            }
        } else if (key.equals(SHARED_LOCALGAME_SHOWPOP)) {
            this.alwaysshowpop = sharedPreferences.getBoolean(SHARED_LOCALGAME_SHOWPOP, true);
        } else if (key.equals(SHARED_BG)) {
            if (!sharedPreferences.getBoolean(SHARED_BG, false)) {
                this.bgfile = null;
            } else {
                setCurrentWallpaer(this);
            }
        } else if (key.equals(SHARED_NO_OF_BUBBLES)) {
            updatenoofbubbles(this, sharedPreferences.getInt(SHARED_NO_OF_BUBBLES, 0));
        } else if (key.equals(SHARED_FB)) {
            boolean mFbLoggedIn = sharedPreferences.getBoolean(SHARED_FB, false);
            if (mFbLoggedIn) {
                registerAlarmManager();
            } else if (!mFbLoggedIn && !sharedPreferences.getBoolean(SHARED_TW, false) && !sharedPreferences.getBoolean(SHARED_GM, false)) {
                unregisterAlarmManager();
            }
        } else if (key.equals("hearts_color")) {
            String phoneno = sharedPreferences.getString(SHARED_MYDARLING, "");
            if (this.mBubbles == null) {
                this.mBubbles = new ArrayList<>();
            }
            Iterator<BubbleModel> it = this.mBubbles.iterator();
            while (true) {
                if (it.hasNext()) {
                    BubbleModel b = it.next();
                    if (b.mType == T_MYDARLING) {
                        this.mBubbles.remove(b);
                        break;
                    }
                } else {
                    break;
                }
            }
            if (phoneno != null && phoneno.length() > 0) {
                addBubble(this, "", phoneno, true, T_MYDARLING, null, -1);
            }
        } else if (key.equals("bubbles_color2")) {
            this.mColorOfbubble = sharedPreferences.getString("bubbles_color2", "2,");
            if (this.mBubbles != null && this.mBubbles.size() > 0) {
                int index = 0;
                Iterator<BubbleModel> it2 = this.mBubbles.iterator();
                while (it2.hasNext()) {
                    if (it2.next().mType == -1) {
                        this.mBubbles.set(index, addBubble(this, "", null, false, -1, null, -1));
                    }
                    index++;
                }
            }
        } else if (key.equals(SHARED_MYDARLING)) {
            String phoneno2 = sharedPreferences.getString(SHARED_MYDARLING, "");
            if (this.mBubbles == null) {
                this.mBubbles = new ArrayList<>();
            }
            Iterator<BubbleModel> it3 = this.mBubbles.iterator();
            while (true) {
                if (it3.hasNext()) {
                    BubbleModel b2 = it3.next();
                    if (b2.mType == T_MYDARLING) {
                        this.mBubbles.remove(b2);
                        break;
                    }
                } else {
                    break;
                }
            }
            if (phoneno2 != null && phoneno2.length() > 0) {
                addBubble(this, "", phoneno2, true, T_MYDARLING, null, -1);
            }
        } else if (key.equals(SHARED_TW)) {
            boolean mTwLoggedIn = sharedPreferences.getBoolean(SHARED_TW, false);
            if (mTwLoggedIn) {
                registerAlarmManager();
            } else if (!mTwLoggedIn && !sharedPreferences.getBoolean(SHARED_GM, false) && !sharedPreferences.getBoolean(SHARED_FB, false)) {
                unregisterAlarmManager();
            }
        } else if (key.equals(SHARED_GM)) {
            boolean mGmLoggedIn = sharedPreferences.getBoolean(SHARED_GM, false);
            if (mGmLoggedIn) {
                registerAlarmManager();
            } else if (!mGmLoggedIn && !sharedPreferences.getBoolean(SHARED_TW, false) && !sharedPreferences.getBoolean(SHARED_FB, false)) {
                unregisterAlarmManager();
            }
        }
    }
}
