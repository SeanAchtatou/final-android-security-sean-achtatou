package com.commind.bubbles.donate;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.drawable.Drawable;

public class ProxyDrawable extends Drawable {
    private boolean mMutated;
    private Drawable mProxy;

    public ProxyDrawable(Drawable target) {
        this.mProxy = target;
    }

    public Drawable getProxy() {
        return this.mProxy;
    }

    public void setProxy(Drawable proxy) {
        if (proxy != this) {
            this.mProxy = proxy;
        }
    }

    public void draw(Canvas canvas) {
        if (this.mProxy != null) {
            this.mProxy.draw(canvas);
        }
    }

    public int getIntrinsicWidth() {
        if (this.mProxy != null) {
            return this.mProxy.getIntrinsicWidth();
        }
        return -1;
    }

    public int getIntrinsicHeight() {
        if (this.mProxy != null) {
            return this.mProxy.getIntrinsicHeight();
        }
        return -1;
    }

    public int getOpacity() {
        if (this.mProxy != null) {
            return this.mProxy.getOpacity();
        }
        return -2;
    }

    public void setFilterBitmap(boolean filter) {
        if (this.mProxy != null) {
            this.mProxy.setFilterBitmap(filter);
        }
    }

    public void setDither(boolean dither) {
        if (this.mProxy != null) {
            this.mProxy.setDither(dither);
        }
    }

    public void setColorFilter(ColorFilter colorFilter) {
        if (this.mProxy != null) {
            this.mProxy.setColorFilter(colorFilter);
        }
    }

    public void setAlpha(int alpha) {
        if (this.mProxy != null) {
            this.mProxy.setAlpha(alpha);
        }
    }

    public Drawable mutate() {
        if (this.mProxy != null && !this.mMutated && super.mutate() == this) {
            this.mProxy.mutate();
            this.mMutated = true;
        }
        return this;
    }
}
