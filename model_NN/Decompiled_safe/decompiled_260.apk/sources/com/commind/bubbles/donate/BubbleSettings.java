package com.commind.bubbles.donate;

import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.provider.ContactsContract;
import android.widget.Toast;
import com.commind.facebook.FacebookProvider;
import com.commind.facebook.Facebookextension;
import com.commind.gmail.GmailNotifier;
import com.commind.twitter.TwitterClient;
import com.google.ads.AdManager;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class BubbleSettings extends PreferenceActivity implements Preference.OnPreferenceChangeListener {
    private GmailNotifier gm;
    private Preference pf;
    private TwitterClient tw;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle icicle) {
        AdManager.init(this);
        super.onCreate(icicle);
        addPreferencesFromResource(R.xml.bubble_settings);
        getPreferenceManager().findPreference(Bubble.SHARED_MYDARLING).setOnPreferenceChangeListener(this);
        getPreferenceManager().findPreference(Bubble.SHARED_BG).setOnPreferenceChangeListener(this);
        getPreferenceManager().findPreference("bubbleasindicator").setOnPreferenceChangeListener(this);
        getPreferenceManager().findPreference(Bubble.SHARED_NO_OF_BUBBLES).setOnPreferenceChangeListener(this);
        getPreferenceManager().findPreference(Bubble.SHARED_FB).setOnPreferenceChangeListener(this);
        getPreferenceManager().findPreference(Bubble.SHARED_FBGAME).setOnPreferenceChangeListener(this);
        getPreferenceManager().findPreference(Bubble.SHARED_LOCALGAME).setOnPreferenceChangeListener(this);
        this.pf = getPreferenceManager().findPreference("localgame_showpop");
        this.pf.setOnPreferenceChangeListener(this);
        getPreferenceManager().findPreference(Bubble.SHARED_TW).setOnPreferenceChangeListener(this);
        getPreferenceManager().findPreference("email").setOnPreferenceChangeListener(this);
        getPreferenceManager().findPreference("bubble_sound").setOnPreferenceChangeListener(this);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    private void showGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction("android.intent.action.GET_CONTENT");
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), 1);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        String number;
        if (resultCode == -1) {
            switch (requestCode) {
                case Bubble.T_SMS /*0*/:
                    break;
                case 1:
                    if (this.gm != null) {
                        this.gm.getAuth(data);
                        this.gm = null;
                        return;
                    }
                    return;
                case 2:
                    ContentResolver cr = getContentResolver();
                    Cursor cursor = cr.query(data.getData(), new String[]{FacebookProvider.KEY_ID, "display_name"}, null, null, null);
                    if (cursor != null) {
                        if (cursor.moveToFirst()) {
                            String string = cursor.getString(cursor.getColumnIndex("display_name"));
                            String id = cursor.getString(0);
                            Bitmap facebm = ContactUtil.loadContactPhoto(cr, ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, Long.parseLong(id)));
                            Cursor phone = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, new String[]{"data1"}, "contact_id=" + id, null, null);
                            if (phone != null) {
                                if (phone.moveToFirst() && (number = phone.getString(0)) != null) {
                                    if (facebm != null) {
                                        try {
                                            FileOutputStream fos = openFileOutput("darling.png", 0);
                                            facebm.compress(Bitmap.CompressFormat.PNG, 100, fos);
                                            fos.close();
                                            Toast.makeText(this, "Darling saved", 0).show();
                                        } catch (FileNotFoundException e) {
                                            e.printStackTrace();
                                        } catch (IOException e2) {
                                            Toast.makeText(this, "Failed to use contact", 0).show();
                                        }
                                    } else {
                                        Toast.makeText(this, "桃心设置已保存, 但是没有找到图片链接", 0).show();
                                    }
                                    getSharedPreferences(Bubble.SHARED_PREFS_NAME, 0).edit().putString(Bubble.SHARED_MYDARLING, number).commit();
                                }
                                phone.close();
                            }
                        }
                        cursor.close();
                        break;
                    }
                    break;
                default:
                    return;
            }
            if (this.tw != null) {
                this.tw.getAuth(data);
                this.tw = null;
            }
        }
    }

    public String getRealPathFromURI(Uri contentUri) {
        Cursor cursor = managedQuery(contentUri, new String[]{"_data"}, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow("_data");
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public boolean onPreferenceChange(Preference preference, Object newValue) {
        boolean usesms;
        final SharedPreferences.Editor editor = getSharedPreferences(Bubble.SHARED_PREFS_NAME, 0).edit();
        if (preference.getKey().compareToIgnoreCase("bubbleasindicator") == 0) {
            if (newValue == null) {
                return true;
            }
            if (((Boolean) newValue).booleanValue()) {
                usesms = true;
            } else {
                usesms = false;
            }
            editor.putBoolean(Bubble.SHARED_SMS_SETTING, usesms);
            editor.commit();
            return true;
        } else if (preference.getKey().compareToIgnoreCase(Bubble.SHARED_NO_OF_BUBBLES) == 0) {
            if (newValue == null) {
                return true;
            }
            editor.putInt(Bubble.SHARED_NO_OF_BUBBLES, Integer.parseInt((String) newValue));
            editor.commit();
            return true;
        } else if (preference.getKey().compareToIgnoreCase(Bubble.SHARED_BG) == 0) {
            if (newValue == null) {
                return true;
            }
            if (Integer.parseInt((String) newValue) == 0) {
                showGallery();
                return true;
            } else if (Integer.parseInt((String) newValue) == 1) {
                editor.putBoolean(Bubble.SHARED_BG, true);
                editor.commit();
                return true;
            } else {
                editor.putBoolean(Bubble.SHARED_BG, false);
                editor.commit();
                return true;
            }
        } else if (preference.getKey().compareToIgnoreCase(Bubble.SHARED_MYDARLING) == 0) {
            if (newValue == null) {
                return true;
            }
            if (((Boolean) newValue).booleanValue()) {
                startActivityForResult(Intent.createChooser(new Intent("android.intent.action.PICK", ContactsContract.Contacts.CONTENT_URI), "Select your darling"), 2);
                return true;
            }
            editor.remove(Bubble.SHARED_MYDARLING).commit();
            File f = getFileStreamPath("darling.png");
            if (f == null || !f.exists()) {
                return true;
            }
            f.delete();
            return true;
        } else if (preference.getKey().compareToIgnoreCase(Bubble.SHARED_FB) == 0) {
            if (newValue == null) {
                return true;
            }
            if (((Boolean) newValue).booleanValue()) {
                final CheckBoxPreference p = (CheckBoxPreference) preference;
                new Facebookextension(this) {
                    public void onResult(boolean result) {
                        if (result) {
                            editor.putBoolean(Bubble.SHARED_FB, true);
                            editor.commit();
                            p.setChecked(true);
                        }
                    }
                }.login();
                return false;
            }
            new Facebookextension(this) {
                public void onResult(boolean result) {
                }
            }.logout();
            editor.putBoolean(Bubble.SHARED_FB, false);
            editor.commit();
            return true;
        } else if (preference.getKey().compareToIgnoreCase(Bubble.SHARED_FBWALL) == 0) {
            if (newValue == null) {
                return true;
            }
            editor.putInt(Bubble.SHARED_FBWALL, Integer.parseInt((String) newValue));
            editor.commit();
            return true;
        } else if (preference.getKey().compareToIgnoreCase(Bubble.SHARED_TW) == 0) {
            if (newValue == null) {
                return true;
            }
            if (((Boolean) newValue).booleanValue()) {
                final CheckBoxPreference p2 = (CheckBoxPreference) preference;
                this.tw = new TwitterClient(this, true) {
                    public void onResult(boolean result) {
                        if (result) {
                            Toast.makeText(BubbleSettings.this, "Twitter login successful!", 0).show();
                            editor.putBoolean(Bubble.SHARED_TW, true);
                            editor.commit();
                            p2.setChecked(true);
                            return;
                        }
                        Toast.makeText(BubbleSettings.this, "Twitter 登录失败.", 0).show();
                    }
                };
                return false;
            }
            new TwitterClient(this, false) {
                public void onResult(boolean result) {
                }
            };
            editor.putBoolean(Bubble.SHARED_TW, false);
            editor.commit();
            return true;
        } else if (preference.getKey().compareToIgnoreCase("email") == 0) {
            if (newValue == null) {
                return true;
            }
            if (((Boolean) newValue).booleanValue()) {
                final CheckBoxPreference p3 = (CheckBoxPreference) preference;
                this.gm = new GmailNotifier(this, true) {
                    public void onResult(boolean result) {
                        if (result) {
                            Toast.makeText(BubbleSettings.this, "Gmail login successful!", 0).show();
                            editor.putBoolean(Bubble.SHARED_GM, true);
                            editor.commit();
                            p3.setChecked(true);
                            return;
                        }
                        Toast.makeText(BubbleSettings.this, "Gmail 登录失败.", 0).show();
                    }
                };
                return false;
            }
            new GmailNotifier(this, false) {
                public void onResult(boolean result) {
                }
            };
            editor.putBoolean(Bubble.SHARED_GM, false);
            editor.commit();
            return true;
        } else if (preference.getKey().compareToIgnoreCase("bubble_sound") == 0) {
            if (newValue == null) {
                return true;
            }
            if (((Boolean) newValue).booleanValue()) {
                editor.putBoolean(Bubble.SHARED_B_SOUND, true);
                editor.commit();
                return true;
            }
            editor.putBoolean(Bubble.SHARED_B_SOUND, false);
            editor.commit();
            return true;
        } else if (preference.getKey().compareToIgnoreCase(Bubble.SHARED_FBGAME) == 0) {
            if (newValue == null) {
                return true;
            }
            if (((Boolean) newValue).booleanValue()) {
                editor.putBoolean(Bubble.SHARED_FBGAME, true);
                editor.commit();
                new AlertDialog.Builder(this).setTitle("Check your stats!").setMessage("Go to your facebook account and check your popping stats with the \"Live Wallpaper Bubbles by Commind\" app!").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                }).show();
                return true;
            }
            editor.putBoolean(Bubble.SHARED_FBGAME, false);
            editor.commit();
            return true;
        } else if (preference.getKey().compareToIgnoreCase(Bubble.SHARED_LOCALGAME) == 0) {
            if (newValue == null) {
                return true;
            }
            if (((Boolean) newValue).booleanValue()) {
                editor.putBoolean(Bubble.SHARED_LOCALGAME, true);
                editor.commit();
                this.pf.setEnabled(true);
                return true;
            }
            editor.putBoolean(Bubble.SHARED_LOCALGAME, false);
            editor.putBoolean(Bubble.SHARED_LOCALGAME_SHOWPOP, false);
            this.pf.setEnabled(false);
            editor.commit();
            return true;
        } else if (preference.getKey().compareToIgnoreCase("localgame_showpop") != 0 || newValue == null) {
            return true;
        } else {
            if (((Boolean) newValue).booleanValue()) {
                editor.putBoolean(Bubble.SHARED_LOCALGAME_SHOWPOP, true);
                editor.commit();
                return true;
            }
            editor.putBoolean(Bubble.SHARED_LOCALGAME_SHOWPOP, false);
            editor.commit();
            return true;
        }
    }
}
