package com.commind.bubbles.donate;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import java.util.ArrayList;
import java.util.Iterator;

public class DialogPref extends DialogPreference {
    /* access modifiers changed from: private */
    public ArrayList<String> colors;
    private int pos = 0;
    private SharedPreferences prefs;
    private GridView sb;
    /* access modifiers changed from: private */
    public Drawable shape;

    public DialogPref(Context context, AttributeSet attrs) {
        super(context, attrs);
        setDialogLayoutResource(R.layout.main);
        this.prefs = context.getSharedPreferences(Bubble.SHARED_PREFS_NAME, 0);
    }

    /* access modifiers changed from: protected */
    public void onDialogClosed(boolean positiveResult) {
        super.onDialogClosed(positiveResult);
        if (positiveResult) {
            String colorstring = "";
            Iterator<String> it = this.colors.iterator();
            while (it.hasNext()) {
                colorstring = String.valueOf(colorstring) + it.next();
            }
            if (colorstring.length() <= 0) {
                colorstring = "2,";
                this.colors.add(colorstring);
            }
            this.prefs.edit().putString("bubbles_color2", colorstring).commit();
        }
    }

    /* access modifiers changed from: protected */
    public View onCreateDialogView() {
        View v = super.onCreateDialogView();
        this.sb = (GridView) v.findViewById(R.id.gridView1);
        GridAdapter gr = new GridAdapter(v.getContext());
        this.shape = v.getContext().getResources().getDrawable(R.drawable.shape);
        this.sb.setAdapter((ListAdapter) gr);
        if (this.colors == null) {
            this.colors = new ArrayList<>();
            String colorss = this.prefs.getString("bubbles_color2", "2,");
            if (colorss.length() <= 0) {
                this.colors.add("2,");
            } else {
                while (colorss.length() > 0) {
                    String c = colorss.substring(0, colorss.indexOf(",") + 1);
                    colorss = colorss.replaceFirst(c, "");
                    this.colors.add(c);
                }
            }
        }
        this.sb.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View v, int position, long id) {
                String colorint = String.valueOf(position);
                if (DialogPref.this.colors.remove(String.valueOf(colorint) + ",")) {
                    v.setBackgroundDrawable(null);
                    return;
                }
                DialogPref.this.colors.add(String.valueOf(colorint) + ",");
                v.setBackgroundDrawable(DialogPref.this.shape);
            }
        });
        return v;
    }

    private class GridAdapter extends BaseAdapter {
        private Bitmap bm;
        private Context mContext = null;
        private DisplayMetrics mMetrics;
        private Bitmap overlay;
        private Paint ps;

        public GridAdapter(Context c) {
            this.mContext = c;
            this.bm = ((BitmapDrawable) c.getResources().getDrawable(R.drawable.bubble_big2)).getBitmap();
            this.mMetrics = c.getResources().getDisplayMetrics();
            this.overlay = ((BitmapDrawable) c.getResources().getDrawable(R.drawable.bubble_big2_white)).getBitmap();
            this.ps = new Paint();
            this.ps.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_OVER));
        }

        public int getCount() {
            return BubbleModel.BUBBLE_COLORS.length;
        }

        public Object getItem(int position) {
            return null;
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ImageView iv = new ImageView(this.mContext);
            iv.setLayoutParams(new AbsListView.LayoutParams(-1, -2));
            int color = BubbleModel.BUBBLE_COLORS[position];
            Bitmap todraw = Bitmap.createBitmap(this.bm.getScaledWidth(this.mMetrics), this.bm.getScaledHeight(this.mMetrics), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(todraw);
            BubbleModel.generateBubbleColor(canvas, this.bm, color);
            canvas.drawBitmap(this.overlay, 0.0f, 0.0f, this.ps);
            iv.setImageBitmap(todraw);
            if (DialogPref.this.colors.contains(String.valueOf(String.valueOf(position)) + ",")) {
                iv.setBackgroundDrawable(DialogPref.this.shape);
            } else {
                iv.setBackgroundDrawable(null);
            }
            return iv;
        }
    }
}
