package com.commind.bubbles.donate;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.LightingColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathMeasure;

public class BubbleModel {
    public static final int[] BUBBLE_COLORS = {-16777216, -16776961, DEFAULT_COLOR, -1310580, -16711936, -16728077, -65281, -65536, -1, -256, -891614, -10471149, -13749870, -10080879, -6684775, -551907, -52276, -16737793, -52429, -16751053};
    public static final int DEFAULT_COLOR = -16711681;
    private float DIST = 2.0f;
    public boolean checkread = false;
    public Bitmap mBitmap;
    public int mBitmapH;
    public int mBitmapW;
    private float mDistance = 1.0f;
    public int mHeight;
    private PathMeasure mPath;
    public String mPhoneNo;
    public String mString;
    public TextBreaker mTextbreaker;
    public int mType = -1;
    public int mWidth;
    private final int mYLimit;
    public int mcolor = -1;
    public long mid = -1;
    private float olddist;
    public boolean reversed;
    public long smsid = -1;
    public long smsthreadid = -1;
    public float xCord;
    public float yCord;

    public static Canvas generateBubbleColor(Canvas c, Bitmap b, int color) {
        Paint pa = new Paint();
        pa.setColorFilter(new LightingColorFilter(color, 1));
        c.drawBitmap(b, 0.0f, 0.0f, pa);
        return c;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Canvas.drawText(java.lang.String, int, int, float, float, android.graphics.Paint):void}
     arg types: [java.lang.String, int, int, float, float, android.text.TextPaint]
     candidates:
      ClspMth{android.graphics.Canvas.drawText(java.lang.CharSequence, int, int, float, float, android.graphics.Paint):void}
      ClspMth{android.graphics.Canvas.drawText(char[], int, int, float, float, android.graphics.Paint):void}
      ClspMth{android.graphics.Canvas.drawText(java.lang.String, int, int, float, float, android.graphics.Paint):void} */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0207  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public BubbleModel(int r34, int r35, android.graphics.Bitmap r36, android.graphics.Bitmap r37, java.lang.String r38, java.lang.String r39, int r40, android.graphics.Bitmap r41, boolean r42, android.util.DisplayMetrics r43, android.graphics.Bitmap r44, long r45, boolean r47, int r48) {
        /*
            r33 = this;
            r33.<init>()
            r5 = 1065353216(0x3f800000, float:1.0)
            r0 = r5
            r1 = r33
            r1.mDistance = r0
            r5 = 1073741824(0x40000000, float:2.0)
            r0 = r5
            r1 = r33
            r1.DIST = r0
            r5 = -1
            r0 = r5
            r1 = r33
            r1.mType = r0
            r5 = -1
            r0 = r5
            r2 = r33
            r2.mid = r0
            r5 = -1
            r0 = r5
            r1 = r33
            r1.mcolor = r0
            r5 = 0
            r0 = r5
            r1 = r33
            r1.checkread = r0
            r5 = -1
            r0 = r5
            r2 = r33
            r2.smsthreadid = r0
            r5 = -1
            r0 = r5
            r2 = r33
            r2.smsid = r0
            r0 = r45
            r2 = r33
            r2.mid = r0
            r0 = r34
            r1 = r33
            r1.mWidth = r0
            r0 = r35
            r1 = r33
            r1.mHeight = r0
            r0 = r48
            r1 = r33
            r1.mcolor = r0
            r0 = r36
            r1 = r43
            int r5 = r0.getScaledHeight(r1)
            r0 = r5
            r1 = r33
            r1.mBitmapH = r0
            r0 = r36
            r1 = r43
            int r5 = r0.getScaledWidth(r1)
            r0 = r5
            r1 = r33
            r1.mBitmapW = r0
            r5 = 0
            r0 = r33
            int r0 = r0.mBitmapH
            r6 = r0
            int r5 = r5 - r6
            int r5 = r5 + 20
            r0 = r5
            r1 = r33
            r1.mYLimit = r0
            if (r42 == 0) goto L_0x00b1
            r0 = r36
            r1 = r33
            r1.mBitmap = r0
            r0 = r38
            r1 = r33
            r1.mString = r0
        L_0x0086:
            r0 = r40
            r1 = r33
            r1.mType = r0
            r0 = r39
            r1 = r33
            r1.mPhoneNo = r0
            r5 = -1082130432(0xffffffffbf800000, float:-1.0)
            r6 = -1082130432(0xffffffffbf800000, float:-1.0)
            r0 = r33
            r1 = r5
            r2 = r6
            android.graphics.PathMeasure r5 = r0.makeFollowPath(r1, r2)
            r0 = r5
            r1 = r33
            r1.mPath = r0
            r5 = -1082130432(0xffffffffbf800000, float:-1.0)
            r6 = -1082130432(0xffffffffbf800000, float:-1.0)
            r7 = 0
            r0 = r33
            r1 = r5
            r2 = r6
            r3 = r7
            r0.updatePath(r1, r2, r3)
            return
        L_0x00b1:
            r0 = r33
            int r0 = r0.mBitmapW
            r5 = r0
            r0 = r33
            int r0 = r0.mBitmapH
            r6 = r0
            android.graphics.Bitmap$Config r7 = android.graphics.Bitmap.Config.ARGB_8888
            android.graphics.Bitmap r5 = android.graphics.Bitmap.createBitmap(r5, r6, r7)
            r0 = r5
            r1 = r33
            r1.mBitmap = r0
            android.graphics.Canvas r12 = new android.graphics.Canvas
            r0 = r33
            android.graphics.Bitmap r0 = r0.mBitmap
            r5 = r0
            r12.<init>(r5)
            android.graphics.Paint r26 = new android.graphics.Paint
            r26.<init>()
            android.graphics.PorterDuffXfermode r5 = new android.graphics.PorterDuffXfermode
            android.graphics.PorterDuff$Mode r6 = android.graphics.PorterDuff.Mode.SRC_OVER
            r5.<init>(r6)
            r0 = r26
            r1 = r5
            r0.setXfermode(r1)
            int[] r5 = com.commind.bubbles.donate.BubbleModel.BUBBLE_COLORS
            r5 = r5[r48]
            r0 = r12
            r1 = r36
            r2 = r5
            generateBubbleColor(r0, r1, r2)
            r5 = 0
            r6 = 0
            r0 = r12
            r1 = r37
            r2 = r5
            r3 = r6
            r4 = r26
            r0.drawBitmap(r1, r2, r3, r4)
            if (r41 == 0) goto L_0x0182
            if (r44 == 0) goto L_0x0182
            r0 = r44
            r1 = r43
            int r5 = r0.getScaledWidth(r1)
            r0 = r44
            r1 = r43
            int r6 = r0.getScaledHeight(r1)
            android.graphics.Bitmap$Config r7 = android.graphics.Bitmap.Config.ARGB_8888
            android.graphics.Bitmap r19 = android.graphics.Bitmap.createBitmap(r5, r6, r7)
            android.graphics.Canvas r22 = new android.graphics.Canvas
            r0 = r22
            r1 = r19
            r0.<init>(r1)
            android.graphics.Paint r25 = new android.graphics.Paint
            r5 = 1
            r0 = r25
            r1 = r5
            r0.<init>(r1)
            r5 = 0
            r0 = r25
            r1 = r5
            r0.setFilterBitmap(r1)
            r0 = r33
            int r0 = r0.mBitmapW
            r5 = r0
            float r5 = (float) r5
            r6 = 1074622628(0x400d70a4, float:2.21)
            float r20 = r5 / r6
            r0 = r33
            int r0 = r0.mBitmapH
            r5 = r0
            int r5 = r5 / 19
            r0 = r5
            float r0 = (float) r0
            r21 = r0
            r5 = 0
            r6 = 0
            r0 = r22
            r1 = r41
            r2 = r5
            r3 = r6
            r4 = r25
            r0.drawBitmap(r1, r2, r3, r4)
            android.graphics.PorterDuffXfermode r5 = new android.graphics.PorterDuffXfermode
            android.graphics.PorterDuff$Mode r6 = android.graphics.PorterDuff.Mode.DST_IN
            r5.<init>(r6)
            r0 = r25
            r1 = r5
            r0.setXfermode(r1)
            r5 = 0
            r6 = 0
            r0 = r22
            r1 = r44
            r2 = r5
            r3 = r6
            r4 = r25
            r0.drawBitmap(r1, r2, r3, r4)
            android.graphics.PorterDuffXfermode r5 = new android.graphics.PorterDuffXfermode
            android.graphics.PorterDuff$Mode r6 = android.graphics.PorterDuff.Mode.DST_OVER
            r5.<init>(r6)
            r0 = r25
            r1 = r5
            r0.setXfermode(r1)
            r0 = r12
            r1 = r19
            r2 = r20
            r3 = r21
            r4 = r25
            r0.drawBitmap(r1, r2, r3, r4)
        L_0x0182:
            r28 = 0
            if (r38 == 0) goto L_0x027e
            int r5 = r38.length()
            if (r5 <= 0) goto L_0x027e
            com.commind.bubbles.donate.TextBreaker r5 = new com.commind.bubbles.donate.TextBreaker
            r6 = 0
            r0 = r33
            int r0 = r0.mBitmapW
            r7 = r0
            r0 = r33
            int r0 = r0.mBitmapH
            r8 = r0
            r0 = r43
            int r0 = r0.densityDpi
            r9 = r0
            r10 = r47
            r5.<init>(r6, r7, r8, r9, r10)
            r0 = r5
            r1 = r33
            r1.mTextbreaker = r0
            r0 = r33
            com.commind.bubbles.donate.TextBreaker r0 = r0.mTextbreaker
            r5 = r0
            r0 = r5
            r1 = r38
            java.util.ArrayList r29 = r0.breakText(r1)
            r0 = r33
            com.commind.bubbles.donate.TextBreaker r0 = r0.mTextbreaker
            r5 = r0
            r0 = r5
            int r0 = r0.mLines
            r24 = r0
            r0 = r33
            com.commind.bubbles.donate.TextBreaker r0 = r0.mTextbreaker
            r5 = r0
            r0 = r5
            float r0 = r0.textheight
            r27 = r0
            r0 = r33
            com.commind.bubbles.donate.TextBreaker r0 = r0.mTextbreaker
            r5 = r0
            android.text.TextPaint r11 = r5.mTp
            if (r29 == 0) goto L_0x027e
            if (r47 == 0) goto L_0x021b
            r0 = r33
            int r0 = r0.mBitmapW
            r5 = r0
            float r5 = (float) r5
            r6 = 1076468122(0x4029999a, float:2.65)
            float r9 = r5 / r6
            r0 = r33
            int r0 = r0.mBitmapH
            r5 = r0
            float r5 = (float) r5
            r6 = 1077306982(0x40366666, float:2.85)
            float r30 = r5 / r6
        L_0x01e9:
            int r31 = r29.size()
            r5 = 1073741824(0x40000000, float:2.0)
            float r5 = r30 / r5
            float r5 = r5 + r30
            int r6 = r31 / 2
            float r6 = (float) r6
            float r6 = r6 * r27
            float r5 = r5 - r6
            float r30 = r5 + r27
            r23 = 0
            r7 = r28
        L_0x01ff:
            r0 = r23
            r1 = r31
            if (r0 < r1) goto L_0x022f
        L_0x0205:
            if (r7 > 0) goto L_0x020b
            int r7 = r38.length()
        L_0x020b:
            r5 = 0
            r0 = r38
            r1 = r5
            r2 = r7
            java.lang.String r5 = r0.substring(r1, r2)
            r0 = r5
            r1 = r33
            r1.mString = r0
            goto L_0x0086
        L_0x021b:
            r0 = r33
            int r0 = r0.mBitmapW
            r5 = r0
            int r5 = r5 / 2
            float r9 = (float) r5
            r0 = r33
            int r0 = r0.mBitmapH
            r5 = r0
            float r5 = (float) r5
            r6 = 1078355558(0x40466666, float:3.1)
            float r30 = r5 / r6
            goto L_0x01e9
        L_0x022f:
            r0 = r29
            r1 = r23
            java.lang.Object r34 = r0.get(r1)
            java.lang.Integer r34 = (java.lang.Integer) r34
            int r32 = r34.intValue()
            if (r32 <= 0) goto L_0x027b
            int r8 = r7 + r32
            r0 = r23
            float r0 = (float) r0
            r5 = r0
            float r5 = r5 * r27
            float r10 = r30 + r5
            r5 = r12
            r6 = r38
            r5.drawText(r6, r7, r8, r9, r10, r11)
            r0 = r23
            r1 = r24
            if (r0 != r1) goto L_0x0272
            int r5 = r38.length()
            if (r8 < r5) goto L_0x0272
            java.lang.String r13 = "..."
            r14 = 0
            r15 = 3
            r5 = 1065353216(0x3f800000, float:1.0)
            float r5 = r27 - r5
            int r6 = r23 + 1
            float r6 = (float) r6
            float r5 = r5 * r6
            float r17 = r30 + r5
            r16 = r9
            r18 = r11
            r12.drawText(r13, r14, r15, r16, r17, r18)
            r7 = r8
            goto L_0x0205
        L_0x0272:
            int r5 = r38.length()
            if (r8 < r5) goto L_0x027a
            r7 = r8
            goto L_0x0205
        L_0x027a:
            r7 = r8
        L_0x027b:
            int r23 = r23 + 1
            goto L_0x01ff
        L_0x027e:
            r7 = r28
            goto L_0x0205
        */
        throw new UnsupportedOperationException("Method not decompiled: com.commind.bubbles.donate.BubbleModel.<init>(int, int, android.graphics.Bitmap, android.graphics.Bitmap, java.lang.String, java.lang.String, int, android.graphics.Bitmap, boolean, android.util.DisplayMetrics, android.graphics.Bitmap, long, boolean, int):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    private PathMeasure makeFollowPath(float xx, float yy) {
        int height;
        float x;
        float xstep;
        Path p = new Path();
        if (xx == -1.0f && yy == -1.0f) {
            height = this.mHeight + 10;
            x = (float) (((double) this.mWidth) * Math.random());
        } else {
            height = (int) yy;
            x = xx;
            this.reversed = true;
        }
        p.moveTo(x, (float) height);
        float step = ((float) height) / Math.max(2.0f, 4.0f * ((float) Math.random()));
        if (step <= 0.0f) {
            step = 100.0f;
        }
        int yStep = (int) (((float) height) / step);
        int bla = 0;
        if (step < 100.0f) {
            bla = 1;
        }
        this.DIST = Math.max((float) (height / 250), ((float) (yStep - bla)) * ((float) Math.random()));
        this.DIST = Math.max(1.4f, this.DIST);
        int xlimit = (int) (((double) this.mWidth) - (((double) this.mBitmapW) / 1.5d));
        boolean reverse = false;
        if (Math.random() >= 0.5d) {
            reverse = true;
        }
        int i = 1;
        float ystep = 0.0f;
        while (ystep > ((float) this.mYLimit)) {
            if (reverse) {
                xstep = x + Math.min((float) (this.mWidth / 3), (((float) this.mWidth) / ((float) Math.random())) * ((float) yStep));
            } else {
                xstep = x - Math.min((float) (this.mWidth / 3), (((float) this.mWidth) / ((float) Math.random())) * ((float) yStep));
            }
            float xmin = 0.0f;
            if (i % 2 == 0) {
                xmin = xstep;
            }
            float x1 = xmin + (xstep / 2.0f);
            float x2 = xmin + (xstep / 3.0f);
            if (xstep > ((float) xlimit)) {
                reverse = false;
                xstep = (float) xlimit;
                x1 = Math.min((float) xlimit, x1 - (x1 / 2.0f));
                x2 = Math.min((float) xlimit, x2 - (x2 / 2.0f));
            } else if (xstep < ((float) -15)) {
                reverse = true;
                xstep = (float) -15;
                x1 = Math.max((float) -15, (x1 / 2.0f) + x1);
                x2 = Math.max((float) -15, x2 + (x2 / 2.0f));
            }
            ystep = ((float) height) - (((float) i) * step);
            float ystep2 = ystep + (ystep / 2.0f);
            if (Math.random() < 0.5d && xstep - x > ((float) (this.mWidth / 3)) && xstep != ((float) xlimit) && i != 1) {
                ystep2 -= ystep2 / 2.0f;
            }
            p.cubicTo(x1, (ystep / 3.0f) + ystep, x2, ystep2, xstep, ystep);
            i++;
        }
        return new PathMeasure(p, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    public void updatePath(float x, float y, boolean slowdown) {
        float[] pos = new float[2];
        if (this.mPath.getPosTan(this.mDistance, pos, new float[2])) {
            if (pos[1] >= this.yCord) {
                this.mDistance += Math.max(1.0f, this.DIST - 1.0f);
            } else if (slowdown && this.DIST > this.olddist) {
                this.DIST = this.olddist;
                this.mDistance += this.DIST;
            } else if (!this.reversed || this.DIST <= this.olddist) {
                this.mDistance += this.DIST;
            } else {
                this.DIST -= 0.01f;
                this.mDistance += this.DIST;
            }
            this.xCord = pos[0];
            this.yCord = pos[1];
            if (this.yCord < ((float) this.mYLimit)) {
                this.mPath = makeFollowPath(x, y);
                this.mDistance = 0.0f;
                this.reversed = false;
                this.checkread = true;
            } else if (x != -1.0f && y != -1.0f && !this.reversed) {
                this.olddist = this.DIST;
                this.DIST *= 1.4f;
                this.reversed = true;
            }
        }
    }
}
