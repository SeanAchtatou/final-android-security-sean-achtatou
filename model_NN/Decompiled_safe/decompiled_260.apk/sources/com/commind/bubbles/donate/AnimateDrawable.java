package com.commind.bubbles.donate;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Transformation;

public class AnimateDrawable extends ProxyDrawable {
    private Animation mAnimation;
    private Transformation mTransformation = new Transformation();
    private float mh;
    private float mw;

    public AnimateDrawable(Drawable target) {
        super(target);
    }

    public AnimateDrawable(Drawable target, Animation animation, float w, float h) {
        super(target);
        this.mAnimation = animation;
        this.mw = w;
        this.mh = h;
    }

    public Animation getAnimation() {
        return this.mAnimation;
    }

    public void setAnimation(Animation anim) {
        this.mAnimation = anim;
    }

    public boolean hasStarted() {
        return this.mAnimation != null && this.mAnimation.hasStarted();
    }

    public boolean hasEnded() {
        return this.mAnimation == null || this.mAnimation.hasEnded();
    }

    public void draw(Canvas canvas) {
        Drawable dr = getProxy();
        if (dr != null) {
            int sc = canvas.save();
            Animation anim = this.mAnimation;
            if (anim != null) {
                canvas.translate(this.mw, this.mh);
                anim.getTransformation(AnimationUtils.currentAnimationTimeMillis(), this.mTransformation);
                canvas.concat(this.mTransformation.getMatrix());
            }
            dr.draw(canvas);
            canvas.restoreToCount(sc);
        }
    }
}
