package com.commind.bubbles.donate;

import android.graphics.Paint;
import android.text.TextPaint;
import java.util.ArrayList;

public class TextBreaker {
    public int mLines;
    public TextPaint mTp;
    private int maxheight;
    private int maxwidth;
    public float textheight;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, int):void}
     arg types: [int, int, int, int]
     candidates:
      ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, long):void}
      ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, int):void} */
    public TextBreaker(TextPaint tp, int bitmapw, int bitmaph, int dens, boolean large) {
        if (tp != null) {
            this.mTp = tp;
            return;
        }
        this.mTp = new TextPaint();
        this.mTp.setAntiAlias(true);
        int dpi = dens;
        if (dpi == 120) {
            this.mTp.setTextSize(8.0f);
        } else if (dpi == 240) {
            this.mTp.setTextSize(12.0f);
        } else {
            this.mTp.setTextSize(10.0f);
        }
        this.mTp.setColor(-1);
        this.mTp.setShadowLayer(2.0f, 0.0f, 0.0f, -16777216);
        this.mTp.setTextAlign(Paint.Align.CENTER);
        float acs = this.mTp.ascent();
        this.textheight = (-acs) + this.mTp.descent();
        if (large) {
            this.maxheight = (int) (((double) bitmaph) / 1.75d);
            this.mLines = (int) (((float) this.maxheight) / this.textheight);
            this.maxwidth = (int) (((double) bitmapw) / 1.5d);
            return;
        }
        this.maxheight = (int) (((double) bitmaph) / 1.2d);
        this.mLines = (int) (((float) this.maxheight) / this.textheight);
        this.maxwidth = (int) (((double) bitmapw) / 1.15d);
    }

    public ArrayList<Integer> breakText(String s) {
        int tempheight;
        ArrayList<Integer> firstlines = new ArrayList<>();
        int i = this.maxwidth;
        int lines = this.mLines;
        while (lines != 0) {
            firstlines.add(Integer.valueOf((int) (((double) this.maxwidth) / ((1.5d * ((double) lines)) / 2.0d))));
            lines--;
            if (lines / 2 == 0) {
                break;
            }
        }
        int textstart = 0;
        int textlen = s.length();
        ArrayList<Integer> textbreaks = new ArrayList<>();
        int tempheight2 = 0;
        for (int t = firstlines.size() - 1; t >= 0; t--) {
            int textwidth = ((Integer) firstlines.get(t)).intValue();
            int numOfChars = this.mTp.breakText(s, textstart, textlen, true, (float) textwidth, null);
            int textstart2 = textstart + numOfChars;
            if (textstart2 > textlen || (tempheight = (int) (((float) tempheight2) + this.textheight)) > this.maxheight) {
                break;
            }
            textbreaks.add(0, Integer.valueOf(numOfChars));
            int numOfChars2 = this.mTp.breakText(s, textstart2, textlen, true, (float) textwidth, null);
            textstart = textstart2 + numOfChars2;
            if (textstart > textlen || (tempheight2 = (int) (((float) tempheight) + this.textheight)) > this.maxheight) {
                break;
            }
            textbreaks.add(Integer.valueOf(numOfChars2));
        }
        return textbreaks;
    }
}
