package com.commind.bubbles.donate;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import java.util.TimerTask;

public class FacebookTask extends TimerTask {
    public static final String FACEBOOK_RECEIVER = "com.commind.bubbles.fbreceiver";
    private static final String event_invites = "event_invites";
    private static final String event_invites_t = "Event invites";
    private static final String friend_requests = "friend_requests";
    public static final String friend_requests_counts = "friend_requests_counts";
    public static final String friend_requests_counts_t = "friend requests";
    private static final String friend_requests_t = "Friend requests";
    private static final String group_invites = "group_invites";
    private static final String group_invites_t = "Group invites";
    /* access modifiers changed from: private */
    public static boolean isreq = false;
    public static final String messages = "messages";
    public static final String messages_t = "messages";
    public static final String notification_counts = "notification_counts";
    public static final String notification_counts_t = "events";
    private static final String pokes = "pokes";
    private static final String pokes_t = "Pokes";
    private static final String shares = "shares";
    private static final String shares_t = "Shares";
    private final Handler handler = new Handler();
    /* access modifiers changed from: private */
    public Context mContext;
    private String mlimit;

    public FacebookTask(Context c, int limit) {
        this.mContext = c;
        if (limit != 0) {
            this.mlimit = Integer.toString(limit);
        } else {
            this.mlimit = null;
        }
    }

    public void run() {
        if (!isreq) {
            isreq = true;
            this.handler.post(new Runnable() {
                public void run() {
                }
            });
        }
    }

    private class HttpRequestTask extends AsyncTask<String, Void, Void> {
        private HttpRequestTask() {
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Void result) {
            FacebookTask.isreq = false;
        }

        /* access modifiers changed from: protected */
        /* JADX WARNING: Code restructure failed: missing block: B:50:0x01e1, code lost:
            r25 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:53:?, code lost:
            r25.printStackTrace();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:54:0x01e8, code lost:
            r25 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:55:0x01e9, code lost:
            r25.printStackTrace();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:56:0x01ef, code lost:
            r25 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:59:?, code lost:
            r25.printStackTrace();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:62:0x01fd, code lost:
            r25 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:65:?, code lost:
            r25.printStackTrace();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:74:?, code lost:
            return null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:76:?, code lost:
            return null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:77:?, code lost:
            return null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:78:?, code lost:
            return null;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Removed duplicated region for block: B:54:0x01e8 A[ExcHandler: ClientProtocolException (r25v6 'e' org.apache.http.client.ClientProtocolException A[CUSTOM_DECLARE]), Splitter:B:1:0x0029] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.lang.Void doInBackground(java.lang.String... r28) {
            /*
                r27 = this;
                org.apache.http.impl.client.DefaultHttpClient r18 = new org.apache.http.impl.client.DefaultHttpClient
                r18.<init>()
                r25 = 0
                r24 = r28[r25]
                org.apache.http.client.methods.HttpGet r11 = new org.apache.http.client.methods.HttpGet
                java.lang.StringBuilder r25 = new java.lang.StringBuilder
                java.lang.String r26 = "https://graph.facebook.com/154743571256665/accounts/test-users?installed=true&permissions=read_stream&access_token="
                r25.<init>(r26)
                java.lang.String r26 = "154743571256665|ixZMSj34g4w25wnre4jdsE6tRqA"
                java.lang.String r26 = java.net.URLEncoder.encode(r26)
                java.lang.StringBuilder r25 = r25.append(r26)
                java.lang.String r25 = r25.toString()
                r0 = r11
                r1 = r25
                r0.<init>(r1)
                r0 = r18
                r1 = r11
                org.apache.http.HttpResponse r10 = r0.execute(r1)     // Catch:{ ClientProtocolException -> 0x01e8, IOException -> 0x01f6 }
                if (r10 == 0) goto L_0x018c
                org.apache.http.StatusLine r25 = r10.getStatusLine()     // Catch:{ ClientProtocolException -> 0x01e8, IOException -> 0x01f6 }
                int r25 = r25.getStatusCode()     // Catch:{ ClientProtocolException -> 0x01e8, IOException -> 0x01f6 }
                r26 = 200(0xc8, float:2.8E-43)
                r0 = r25
                r1 = r26
                if (r0 != r1) goto L_0x018c
                r16 = 0
                com.commind.facebook.JsonParser r17 = new com.commind.facebook.JsonParser     // Catch:{ ClientProtocolException -> 0x01e8, IOException -> 0x01f6 }
                r17.<init>()     // Catch:{ ClientProtocolException -> 0x01e8, IOException -> 0x01f6 }
                org.apache.http.HttpEntity r25 = r10.getEntity()     // Catch:{ IllegalStateException -> 0x01e1, IOException -> 0x01ef, JSONException -> 0x01fd, ClientProtocolException -> 0x01e8 }
                java.io.InputStream r25 = r25.getContent()     // Catch:{ IllegalStateException -> 0x01e1, IOException -> 0x01ef, JSONException -> 0x01fd, ClientProtocolException -> 0x01e8 }
                r0 = r17
                r1 = r25
                org.json.JSONObject r16 = r0.convertToJSONobj(r1)     // Catch:{ IllegalStateException -> 0x01e1, IOException -> 0x01ef, JSONException -> 0x01fd, ClientProtocolException -> 0x01e8 }
                java.lang.String r25 = "data"
                r0 = r16
                r1 = r25
                org.json.JSONArray r3 = r0.getJSONArray(r1)     // Catch:{ IllegalStateException -> 0x01e1, IOException -> 0x01ef, JSONException -> 0x01fd, ClientProtocolException -> 0x01e8 }
                r25 = 0
                r0 = r3
                r1 = r25
                org.json.JSONObject r22 = r0.optJSONObject(r1)     // Catch:{ IllegalStateException -> 0x01e1, IOException -> 0x01ef, JSONException -> 0x01fd, ClientProtocolException -> 0x01e8 }
                r0 = r18
                r1 = r11
                org.apache.http.HttpResponse r10 = r0.execute(r1)     // Catch:{ IllegalStateException -> 0x01e1, IOException -> 0x01ef, JSONException -> 0x01fd, ClientProtocolException -> 0x01e8 }
                org.apache.http.HttpEntity r25 = r10.getEntity()     // Catch:{ IllegalStateException -> 0x01e1, IOException -> 0x01ef, JSONException -> 0x01fd, ClientProtocolException -> 0x01e8 }
                java.io.InputStream r25 = r25.getContent()     // Catch:{ IllegalStateException -> 0x01e1, IOException -> 0x01ef, JSONException -> 0x01fd, ClientProtocolException -> 0x01e8 }
                r0 = r17
                r1 = r25
                org.json.JSONObject r16 = r0.convertToJSONobj(r1)     // Catch:{ IllegalStateException -> 0x01e1, IOException -> 0x01ef, JSONException -> 0x01fd, ClientProtocolException -> 0x01e8 }
                android.content.Intent r14 = new android.content.Intent     // Catch:{ IllegalStateException -> 0x01e1, IOException -> 0x01ef, JSONException -> 0x01fd, ClientProtocolException -> 0x01e8 }
                java.lang.String r25 = "com.commind.bubbles.fbreceiver"
                r0 = r14
                r1 = r25
                r0.<init>(r1)     // Catch:{ IllegalStateException -> 0x01e1, IOException -> 0x01ef, JSONException -> 0x01fd, ClientProtocolException -> 0x01e8 }
                java.lang.String r25 = "notification_counts"
                r0 = r16
                r1 = r25
                boolean r25 = r0.has(r1)     // Catch:{ IllegalStateException -> 0x01e1, IOException -> 0x01ef, JSONException -> 0x01fd, ClientProtocolException -> 0x01e8 }
                if (r25 == 0) goto L_0x00b6
                java.lang.String r25 = "notification_counts"
                r0 = r16
                r1 = r25
                org.json.JSONObject r20 = r0.optJSONObject(r1)     // Catch:{ IllegalStateException -> 0x01e1, IOException -> 0x01ef, JSONException -> 0x01fd, ClientProtocolException -> 0x01e8 }
                java.lang.String r25 = "unseen"
                r0 = r20
                r1 = r25
                int r23 = r0.optInt(r1)     // Catch:{ IllegalStateException -> 0x01e1, IOException -> 0x01ef, JSONException -> 0x01fd, ClientProtocolException -> 0x01e8 }
                if (r23 <= 0) goto L_0x00b6
                java.lang.String r25 = "notification_counts"
                r0 = r14
                r1 = r25
                r2 = r23
                r0.putExtra(r1, r2)     // Catch:{ IllegalStateException -> 0x01e1, IOException -> 0x01ef, JSONException -> 0x01fd, ClientProtocolException -> 0x01e8 }
            L_0x00b6:
                java.lang.String r25 = "friend_requests_counts"
                r0 = r16
                r1 = r25
                boolean r25 = r0.has(r1)     // Catch:{ IllegalStateException -> 0x01e1, IOException -> 0x01ef, JSONException -> 0x01fd, ClientProtocolException -> 0x01e8 }
                if (r25 == 0) goto L_0x00e2
                java.lang.String r25 = "friend_requests_counts"
                r0 = r16
                r1 = r25
                org.json.JSONObject r20 = r0.optJSONObject(r1)     // Catch:{ IllegalStateException -> 0x01e1, IOException -> 0x01ef, JSONException -> 0x01fd, ClientProtocolException -> 0x01e8 }
                java.lang.String r25 = "unseen"
                r0 = r20
                r1 = r25
                int r23 = r0.optInt(r1)     // Catch:{ IllegalStateException -> 0x01e1, IOException -> 0x01ef, JSONException -> 0x01fd, ClientProtocolException -> 0x01e8 }
                if (r23 <= 0) goto L_0x00e2
                java.lang.String r25 = "friend_requests_counts"
                r0 = r14
                r1 = r25
                r2 = r23
                r0.putExtra(r1, r2)     // Catch:{ IllegalStateException -> 0x01e1, IOException -> 0x01ef, JSONException -> 0x01fd, ClientProtocolException -> 0x01e8 }
            L_0x00e2:
                java.lang.String r25 = "messages"
                r0 = r16
                r1 = r25
                boolean r25 = r0.has(r1)     // Catch:{ IllegalStateException -> 0x01e1, IOException -> 0x01ef, JSONException -> 0x01fd, ClientProtocolException -> 0x01e8 }
                if (r25 == 0) goto L_0x0150
                java.lang.String r25 = "messages"
                r0 = r16
                r1 = r25
                org.json.JSONObject r20 = r0.optJSONObject(r1)     // Catch:{ IllegalStateException -> 0x01e1, IOException -> 0x01ef, JSONException -> 0x01fd, ClientProtocolException -> 0x01e8 }
                java.lang.String r25 = "unseen"
                r0 = r20
                r1 = r25
                int r23 = r0.optInt(r1)     // Catch:{ IllegalStateException -> 0x01e1, IOException -> 0x01ef, JSONException -> 0x01fd, ClientProtocolException -> 0x01e8 }
                if (r23 <= 0) goto L_0x0150
                java.lang.String r25 = "messages"
                r0 = r14
                r1 = r25
                r2 = r23
                r0.putExtra(r1, r2)     // Catch:{ IllegalStateException -> 0x01e1, IOException -> 0x01ef, JSONException -> 0x01fd, ClientProtocolException -> 0x01e8 }
                r0 = r18
                r1 = r11
                org.apache.http.HttpResponse r10 = r0.execute(r1)     // Catch:{ IllegalStateException -> 0x01e1, IOException -> 0x01ef, JSONException -> 0x01fd, ClientProtocolException -> 0x01e8 }
                org.apache.http.HttpEntity r25 = r10.getEntity()     // Catch:{ IllegalStateException -> 0x01e1, IOException -> 0x01ef, JSONException -> 0x01fd, ClientProtocolException -> 0x01e8 }
                java.io.InputStream r25 = r25.getContent()     // Catch:{ IllegalStateException -> 0x01e1, IOException -> 0x01ef, JSONException -> 0x01fd, ClientProtocolException -> 0x01e8 }
                r0 = r17
                r1 = r25
                org.json.JSONObject r8 = r0.convertToJSONobj(r1)     // Catch:{ IllegalStateException -> 0x01e1, IOException -> 0x01ef, JSONException -> 0x01fd, ClientProtocolException -> 0x01e8 }
                java.lang.String r25 = "data"
                r0 = r8
                r1 = r25
                org.json.JSONArray r5 = r0.optJSONArray(r1)     // Catch:{ IllegalStateException -> 0x01e1, IOException -> 0x01ef, JSONException -> 0x01fd, ClientProtocolException -> 0x01e8 }
                if (r5 == 0) goto L_0x0150
                int r21 = r5.length()     // Catch:{ IllegalStateException -> 0x01e1, IOException -> 0x01ef, JSONException -> 0x01fd, ClientProtocolException -> 0x01e8 }
                if (r21 <= 0) goto L_0x0150
                java.util.ArrayList r4 = new java.util.ArrayList     // Catch:{ IllegalStateException -> 0x01e1, IOException -> 0x01ef, JSONException -> 0x01fd, ClientProtocolException -> 0x01e8 }
                r4.<init>()     // Catch:{ IllegalStateException -> 0x01e1, IOException -> 0x01ef, JSONException -> 0x01fd, ClientProtocolException -> 0x01e8 }
                r12 = 0
            L_0x013c:
                r0 = r12
                r1 = r21
                if (r0 < r1) goto L_0x018f
                int r25 = r4.size()     // Catch:{ IllegalStateException -> 0x01e1, IOException -> 0x01ef, JSONException -> 0x01fd, ClientProtocolException -> 0x01e8 }
                if (r25 <= 0) goto L_0x0150
                java.lang.String r25 = "array"
                r0 = r14
                r1 = r25
                r2 = r4
                r0.putStringArrayListExtra(r1, r2)     // Catch:{ IllegalStateException -> 0x01e1, IOException -> 0x01ef, JSONException -> 0x01fd, ClientProtocolException -> 0x01e8 }
            L_0x0150:
                java.lang.String r25 = "notification_counts"
                r0 = r14
                r1 = r25
                boolean r25 = r0.hasExtra(r1)     // Catch:{ IllegalStateException -> 0x01e1, IOException -> 0x01ef, JSONException -> 0x01fd, ClientProtocolException -> 0x01e8 }
                if (r25 != 0) goto L_0x017c
                java.lang.String r25 = "friend_requests_counts"
                r0 = r14
                r1 = r25
                boolean r25 = r0.hasExtra(r1)     // Catch:{ IllegalStateException -> 0x01e1, IOException -> 0x01ef, JSONException -> 0x01fd, ClientProtocolException -> 0x01e8 }
                if (r25 != 0) goto L_0x017c
                java.lang.String r25 = "messages"
                r0 = r14
                r1 = r25
                boolean r25 = r0.hasExtra(r1)     // Catch:{ IllegalStateException -> 0x01e1, IOException -> 0x01ef, JSONException -> 0x01fd, ClientProtocolException -> 0x01e8 }
                if (r25 != 0) goto L_0x017c
                java.lang.String r25 = "array"
                r0 = r14
                r1 = r25
                boolean r25 = r0.hasExtra(r1)     // Catch:{ IllegalStateException -> 0x01e1, IOException -> 0x01ef, JSONException -> 0x01fd, ClientProtocolException -> 0x01e8 }
                if (r25 == 0) goto L_0x018c
            L_0x017c:
                r0 = r27
                com.commind.bubbles.donate.FacebookTask r0 = com.commind.bubbles.donate.FacebookTask.this     // Catch:{ IllegalStateException -> 0x01e1, IOException -> 0x01ef, JSONException -> 0x01fd, ClientProtocolException -> 0x01e8 }
                r25 = r0
                android.content.Context r25 = r25.mContext     // Catch:{ IllegalStateException -> 0x01e1, IOException -> 0x01ef, JSONException -> 0x01fd, ClientProtocolException -> 0x01e8 }
                r0 = r25
                r1 = r14
                r0.sendBroadcast(r1)     // Catch:{ IllegalStateException -> 0x01e1, IOException -> 0x01ef, JSONException -> 0x01fd, ClientProtocolException -> 0x01e8 }
            L_0x018c:
                r25 = 0
                return r25
            L_0x018f:
                org.json.JSONObject r15 = r5.optJSONObject(r12)     // Catch:{ IllegalStateException -> 0x01e1, IOException -> 0x01ef, JSONException -> 0x01fd, ClientProtocolException -> 0x01e8 }
                java.lang.String r25 = "from"
                r0 = r15
                r1 = r25
                org.json.JSONObject r9 = r0.optJSONObject(r1)     // Catch:{ IllegalStateException -> 0x01e1, IOException -> 0x01ef, JSONException -> 0x01fd, ClientProtocolException -> 0x01e8 }
                java.lang.String r25 = "id"
                r0 = r9
                r1 = r25
                java.lang.String r13 = r0.optString(r1)     // Catch:{ IllegalStateException -> 0x01e1, IOException -> 0x01ef, JSONException -> 0x01fd, ClientProtocolException -> 0x01e8 }
                java.lang.String r25 = "message"
                r0 = r15
                r1 = r25
                java.lang.String r19 = r0.optString(r1)     // Catch:{ IllegalStateException -> 0x01e1, IOException -> 0x01ef, JSONException -> 0x01fd, ClientProtocolException -> 0x01e8 }
                int r25 = r19.length()     // Catch:{ IllegalStateException -> 0x01e1, IOException -> 0x01ef, JSONException -> 0x01fd, ClientProtocolException -> 0x01e8 }
                r26 = 50
                r0 = r25
                r1 = r26
                if (r0 <= r1) goto L_0x01c8
                r25 = 0
                r26 = 50
                r0 = r19
                r1 = r25
                r2 = r26
                java.lang.String r19 = r0.substring(r1, r2)     // Catch:{ IllegalStateException -> 0x01e1, IOException -> 0x01ef, JSONException -> 0x01fd, ClientProtocolException -> 0x01e8 }
            L_0x01c8:
                int r25 = r13.length()     // Catch:{ IllegalStateException -> 0x01e1, IOException -> 0x01ef, JSONException -> 0x01fd, ClientProtocolException -> 0x01e8 }
                if (r25 <= 0) goto L_0x01dd
                int r25 = r19.length()     // Catch:{ IllegalStateException -> 0x01e1, IOException -> 0x01ef, JSONException -> 0x01fd, ClientProtocolException -> 0x01e8 }
                if (r25 <= 0) goto L_0x01dd
                r4.add(r13)     // Catch:{ IllegalStateException -> 0x01e1, IOException -> 0x01ef, JSONException -> 0x01fd, ClientProtocolException -> 0x01e8 }
                r0 = r4
                r1 = r19
                r0.add(r1)     // Catch:{ IllegalStateException -> 0x01e1, IOException -> 0x01ef, JSONException -> 0x01fd, ClientProtocolException -> 0x01e8 }
            L_0x01dd:
                int r12 = r12 + 1
                goto L_0x013c
            L_0x01e1:
                r25 = move-exception
                r6 = r25
                r6.printStackTrace()     // Catch:{ ClientProtocolException -> 0x01e8, IOException -> 0x01f6 }
                goto L_0x018c
            L_0x01e8:
                r25 = move-exception
                r7 = r25
                r7.printStackTrace()
                goto L_0x018c
            L_0x01ef:
                r25 = move-exception
                r6 = r25
                r6.printStackTrace()     // Catch:{ ClientProtocolException -> 0x01e8, IOException -> 0x01f6 }
                goto L_0x018c
            L_0x01f6:
                r25 = move-exception
                r7 = r25
                r7.printStackTrace()
                goto L_0x018c
            L_0x01fd:
                r25 = move-exception
                r6 = r25
                r6.printStackTrace()     // Catch:{ ClientProtocolException -> 0x01e8, IOException -> 0x01f6 }
                goto L_0x018c
            */
            throw new UnsupportedOperationException("Method not decompiled: com.commind.bubbles.donate.FacebookTask.HttpRequestTask.doInBackground(java.lang.String[]):java.lang.Void");
        }
    }
}
