package com.commind.bubbles.donate;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import com.commind.gmail.GmailNotifier;
import com.commind.twitter.TwitterClient;

public class ForumWeb extends Activity {
    /* access modifiers changed from: private */
    public ProgressBar mBar;
    private WebView mWebView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.web);
        String url = getIntent().getStringExtra("url");
        if (url == null) {
            url = getIntent().getStringExtra("gmail_url");
        }
        if (url != null) {
            this.mWebView = (WebView) findViewById(R.id.webview);
            this.mWebView.getSettings().setJavaScriptEnabled(true);
            this.mWebView.loadUrl(url);
            this.mWebView.setWebViewClient(new HelloWebViewClient(this, null));
        }
        this.mBar = (ProgressBar) findViewById(R.id.ProgressBar01);
    }

    private class HelloWebViewClient extends WebViewClient {
        private HelloWebViewClient() {
        }

        /* synthetic */ HelloWebViewClient(ForumWeb forumWeb, HelloWebViewClient helloWebViewClient) {
            this();
        }

        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            ForumWeb.this.mBar.setVisibility(0);
            super.onPageStarted(view, url, favicon);
        }

        public void onPageFinished(WebView view, String url) {
            ForumWeb.this.mBar.setVisibility(4);
            super.onPageFinished(view, url);
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if ((url == null || !url.startsWith(TwitterClient.CALLBACK_URL)) && (url == null || !url.startsWith(GmailNotifier.CALLBACK_URL))) {
                view.loadUrl(url);
                return false;
            }
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setData(Uri.parse(url));
            ForumWeb.this.setResult(-1, intent);
            ForumWeb.this.finish();
            return true;
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || !this.mWebView.canGoBack()) {
            return super.onKeyDown(keyCode, event);
        }
        this.mWebView.goBack();
        return true;
    }
}
