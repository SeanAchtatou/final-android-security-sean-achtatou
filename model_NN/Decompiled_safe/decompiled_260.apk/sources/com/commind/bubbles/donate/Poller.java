package com.commind.bubbles.donate;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;
import android.util.Xml;
import com.commind.facebook.Facebookextension;
import com.commind.gmail.GmailNotifier;
import com.commind.twitter.TwitterClient;
import com.facebook.android.Facebook;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.util.LinkedList;
import java.util.zip.GZIPInputStream;
import oauth.signpost.OAuth;
import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;
import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HttpEntity;
import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.HttpResponse;
import org.apache.http.HttpResponseInterceptor;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.conn.params.ConnPerRouteBean;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.HttpEntityWrapper;
import org.apache.http.impl.DefaultConnectionReuseStrategy;
import org.apache.http.impl.client.DefaultConnectionKeepAliveStrategy;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.client.DefaultRedirectHandler;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HttpContext;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class Poller extends Service {
    private static final String GAME_URL = "http://bubblesthegame.appspot.com/bubble?";
    /* access modifiers changed from: private */
    public static boolean lock = false;
    private final String LOGTAG = "Poller";

    public void onStart(Intent intent, int startId) {
        Log.i("Poller", "Poller: onStart");
        super.onStart(intent, startId);
        makeAsyncReq(this);
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    private class makasyncReqTask extends AsyncTask<Object, Object, Object> {
        private makasyncReqTask() {
        }

        /* synthetic */ makasyncReqTask(Poller poller, makasyncReqTask makasyncreqtask) {
            this();
        }

        /* access modifiers changed from: protected */
        public Object doInBackground(Object... params) {
            Poller.this.makeReq((Context) params[0]);
            return null;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Object o) {
            Poller.lock = false;
            Poller.this.stopSelf();
            Log.i("Poller", "Poller: stopping self");
        }
    }

    public void makeAsyncReq(Context c) {
        if (!lock) {
            lock = true;
            new makasyncReqTask(this, null).execute(c);
        }
    }

    private DefaultHttpClient createHttpClient() {
        HttpParams httpParams = new BasicHttpParams();
        ConnManagerParams.setMaxTotalConnections(httpParams, 20);
        ConnManagerParams.setMaxConnectionsPerRoute(httpParams, new ConnPerRouteBean(2));
        ConnManagerParams.setTimeout(httpParams, 10000);
        SchemeRegistry schemeRegistry = new SchemeRegistry();
        schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
        schemeRegistry.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));
        DefaultHttpClient httpClient = new DefaultHttpClient(new ThreadSafeClientConnManager(httpParams, schemeRegistry), httpParams);
        httpClient.setHttpRequestRetryHandler(new DefaultHttpRequestRetryHandler(10, true));
        httpClient.setReuseStrategy(new DefaultConnectionReuseStrategy());
        httpClient.setKeepAliveStrategy(new DefaultConnectionKeepAliveStrategy());
        httpClient.setRedirectHandler(new DefaultRedirectHandler());
        return httpClient;
    }

    /* access modifiers changed from: private */
    public void makeReq(Context c) {
        String url_token;
        DefaultHttpClient httpclient = createHttpClient();
        httpclient.addRequestInterceptor(new GzipHttpRequestInterceptor(null));
        httpclient.addResponseInterceptor(new GzipHttpResponseInterceptor(null));
        SharedPreferences prefs = c.getSharedPreferences(Bubble.SHARED_PREFS_NAME, 0);
        Intent intent = new Intent(FacebookTask.FACEBOOK_RECEIVER);
        if (prefs.getBoolean(Bubble.SHARED_FB, false) && (url_token = Facebookextension.getAccesToken(c)) != null) {
            facebookReq(httpclient, URLEncoder.encode(url_token), c, intent);
            if (prefs.getBoolean(Bubble.SHARED_FBGAME, false) && (Bubble.P_E > 0 || Bubble.P_S > 0 || Bubble.P_P > 0 || Bubble.P_F > 0 || Bubble.P_G > 0 || Bubble.P_T > 0)) {
                LinkedList linkedList = new LinkedList();
                linkedList.add(new BasicNameValuePair(Facebook.TOKEN, url_token));
                linkedList.add(new BasicNameValuePair("e", String.valueOf(Bubble.P_E)));
                linkedList.add(new BasicNameValuePair("s", String.valueOf(Bubble.P_S)));
                linkedList.add(new BasicNameValuePair("p", String.valueOf(Bubble.P_P)));
                linkedList.add(new BasicNameValuePair("f", String.valueOf(Bubble.P_F)));
                linkedList.add(new BasicNameValuePair("g", String.valueOf(Bubble.P_G)));
                linkedList.add(new BasicNameValuePair("t", String.valueOf(Bubble.P_T)));
                try {
                    HttpResponse response = httpclient.execute(new HttpPost(GAME_URL + URLEncodedUtils.format(linkedList, "utf-8")));
                    if (response != null && response.getStatusLine().getStatusCode() == 200) {
                        Bubble.P_E = 0;
                        Bubble.P_S = 0;
                        Bubble.P_P = 0;
                        Bubble.P_F = 0;
                        Bubble.P_G = 0;
                        Bubble.P_T = 0;
                    }
                } catch (ClientProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
            }
        }
        if (prefs.getBoolean(Bubble.SHARED_TW, false)) {
            String access_token = prefs.getString(TwitterClient.CONSUMER_KEY, "");
            String access_secret = prefs.getString(TwitterClient.CONSUMER_SECRET, "");
            if (access_token.length() > 0 && access_secret.length() > 0) {
                CommonsHttpOAuthConsumer commonsHttpOAuthConsumer = new CommonsHttpOAuthConsumer(TwitterClient.CONSUMER_KEY, TwitterClient.CONSUMER_SECRET);
                commonsHttpOAuthConsumer.setTokenWithSecret(access_token, access_secret);
                twitterReq(httpclient, commonsHttpOAuthConsumer, c, intent);
            }
        }
        if (prefs.getBoolean(Bubble.SHARED_GM, false)) {
            String access_token2 = prefs.getString(GmailNotifier.CONSUMER_KEY, "");
            String access_secret2 = prefs.getString(GmailNotifier.CONSUMER_SECRET, "");
            if (access_token2.length() > 0 && access_secret2.length() > 0) {
                CommonsHttpOAuthConsumer commonsHttpOAuthConsumer2 = new CommonsHttpOAuthConsumer("anonymous", "anonymous");
                commonsHttpOAuthConsumer2.setTokenWithSecret(access_token2, access_secret2);
                gmailReq(httpclient, commonsHttpOAuthConsumer2, c, intent);
            }
        }
        if (intent.hasExtra(GmailNotifier.MESS_INTENT) || intent.hasExtra(TwitterClient.MESS_INTENT) || intent.hasExtra(TwitterClient.MENTION_INTENT) || intent.hasExtra(FacebookTask.notification_counts) || intent.hasExtra(FacebookTask.friend_requests_counts) || intent.hasExtra("messages") || intent.hasExtra("array")) {
            c.sendBroadcast(intent);
        }
        httpclient.getConnectionManager().shutdown();
    }

    private static void gmailReq(DefaultHttpClient mDefHttpClient, CommonsHttpOAuthConsumer consumer, Context c, Intent intent) {
        String unreadmess = "";
        HttpGet httpget = new HttpGet(GmailNotifier.scope);
        try {
            consumer.sign(httpget);
            HttpResponse getresponse = mDefHttpClient.execute(httpget);
            if (getresponse != null && getresponse.getStatusLine().getStatusCode() == 200) {
                InputStream stream = getresponse.getEntity().getContent();
                XmlPullParser parser = Xml.newPullParser();
                try {
                    parser.setInput(stream, OAuth.ENCODING);
                    while (true) {
                        if (parser.next() != 1) {
                            if (parser.getEventType() == 2 && "fullcount".equals(parser.getName())) {
                                unreadmess = parser.nextText();
                                break;
                            }
                        } else {
                            break;
                        }
                    }
                } catch (XmlPullParserException e) {
                    e.printStackTrace();
                }
                if (unreadmess == null) {
                    return;
                }
                if (unreadmess.length() > 0) {
                    intent.putExtra(GmailNotifier.MESS_INTENT, unreadmess);
                }
            }
        } catch (IllegalStateException e2) {
            e2.printStackTrace();
        } catch (IOException e3) {
            e3.printStackTrace();
        } catch (OAuthMessageSignerException e4) {
            e4.printStackTrace();
        } catch (OAuthExpectationFailedException e5) {
            e5.printStackTrace();
        } catch (OAuthCommunicationException e6) {
            e6.printStackTrace();
        }
    }

    /* JADX INFO: Multiple debug info for r8v3 org.apache.http.HttpResponse: [D('getresponse' org.apache.http.HttpResponse), D('httpget' org.apache.http.client.methods.HttpGet)] */
    /* JADX INFO: Multiple debug info for r7v1 org.apache.http.HttpResponse: [D('getresponse2' org.apache.http.HttpResponse), D('consumer' oauth.signpost.commonshttp.CommonsHttpOAuthConsumer)] */
    /* JADX INFO: Multiple debug info for r8v6 org.json.JSONArray: [D('jp' com.commind.facebook.JsonParser), D('jo' org.json.JSONArray)] */
    /* JADX INFO: Multiple debug info for r0v13 org.json.JSONArray: [D('jp' com.commind.facebook.JsonParser), D('jo' org.json.JSONArray)] */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x011a, code lost:
        r8 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x011b, code lost:
        r6 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0127, code lost:
        r8 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0128, code lost:
        r6 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:?, code lost:
        r6.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0134, code lost:
        r7 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0135, code lost:
        r6 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x013c, code lost:
        r6 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x013d, code lost:
        r6.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x0141, code lost:
        r7 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x0142, code lost:
        r6 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:?, code lost:
        r6.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0149, code lost:
        r6 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x014a, code lost:
        r6.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x014e, code lost:
        r6 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x014f, code lost:
        r6.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0153, code lost:
        r6 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x0156, code lost:
        r6 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0159, code lost:
        r6 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x015c, code lost:
        r6 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00cd A[SYNTHETIC, Splitter:B:23:0x00cd] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x013c A[ExcHandler: OAuthMessageSignerException (r6v3 'e1' oauth.signpost.exception.OAuthMessageSignerException A[CUSTOM_DECLARE]), Splitter:B:7:0x006f] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0149 A[ExcHandler: OAuthExpectationFailedException (r6v2 'e1' oauth.signpost.exception.OAuthExpectationFailedException A[CUSTOM_DECLARE]), Splitter:B:7:0x006f] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x014e A[ExcHandler: OAuthCommunicationException (r6v1 'e1' oauth.signpost.exception.OAuthCommunicationException A[CUSTOM_DECLARE]), Splitter:B:7:0x006f] */
    /* JADX WARNING: Removed duplicated region for block: B:73:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:56:0x0145=Splitter:B:56:0x0145, B:50:0x0138=Splitter:B:50:0x0138, B:44:0x012b=Splitter:B:44:0x012b, B:38:0x011e=Splitter:B:38:0x011e} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void twitterReq(org.apache.http.impl.client.DefaultHttpClient r6, oauth.signpost.commonshttp.CommonsHttpOAuthConsumer r7, android.content.Context r8, android.content.Intent r9) {
        /*
            java.lang.String r0 = "bubblesettings"
            r1 = 0
            android.content.SharedPreferences r2 = r8.getSharedPreferences(r0, r1)
            java.lang.String r8 = ""
            java.lang.String r0 = "mention"
            java.lang.String r1 = ""
            java.lang.String r0 = r2.getString(r0, r1)
            int r1 = r0.length()
            if (r1 <= 0) goto L_0x0162
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            java.lang.String r1 = "&since_id="
            r8.<init>(r1)
            java.lang.StringBuilder r8 = r8.append(r0)
            java.lang.String r8 = r8.toString()
            r0 = r8
        L_0x0027:
            org.apache.http.client.methods.HttpGet r8 = new org.apache.http.client.methods.HttpGet
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r3 = "http://api.twitter.com/1/statuses/mentions.json?trim_user=true&include_rts=false&include_entities=false"
            r1.<init>(r3)
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r0 = r0.toString()
            r8.<init>(r0)
            java.lang.String r0 = "twitter_message"
            java.lang.String r1 = ""
            java.lang.String r1 = r2.getString(r0, r1)
            java.lang.String r0 = ""
            int r3 = r1.length()
            if (r3 <= 0) goto L_0x015f
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r3 = "&since_id="
            r0.<init>(r3)
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            r1 = r0
        L_0x005b:
            org.apache.http.client.methods.HttpGet r0 = new org.apache.http.client.methods.HttpGet
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "http://api.twitter.com/1/direct_messages.json?include_entities=false"
            r3.<init>(r4)
            java.lang.StringBuilder r1 = r3.append(r1)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            r7.sign(r8)     // Catch:{ IllegalStateException -> 0x0122, IOException -> 0x012f, OAuthMessageSignerException -> 0x013c, OAuthExpectationFailedException -> 0x0149, OAuthCommunicationException -> 0x014e }
            org.apache.http.HttpResponse r8 = r6.execute(r8)     // Catch:{ IllegalStateException -> 0x0122, IOException -> 0x012f, OAuthMessageSignerException -> 0x013c, OAuthExpectationFailedException -> 0x0149, OAuthCommunicationException -> 0x014e }
            r7.sign(r0)     // Catch:{ IllegalStateException -> 0x0122, IOException -> 0x012f, OAuthMessageSignerException -> 0x013c, OAuthExpectationFailedException -> 0x0149, OAuthCommunicationException -> 0x014e }
            org.apache.http.HttpResponse r7 = r6.execute(r0)     // Catch:{ IllegalStateException -> 0x0122, IOException -> 0x012f, OAuthMessageSignerException -> 0x013c, OAuthExpectationFailedException -> 0x0149, OAuthCommunicationException -> 0x014e }
            if (r8 == 0) goto L_0x00cb
            org.apache.http.StatusLine r6 = r8.getStatusLine()     // Catch:{ IllegalStateException -> 0x0122, IOException -> 0x012f, OAuthMessageSignerException -> 0x013c, OAuthExpectationFailedException -> 0x0149, OAuthCommunicationException -> 0x014e }
            int r6 = r6.getStatusCode()     // Catch:{ IllegalStateException -> 0x0122, IOException -> 0x012f, OAuthMessageSignerException -> 0x013c, OAuthExpectationFailedException -> 0x0149, OAuthCommunicationException -> 0x014e }
            r0 = 200(0xc8, float:2.8E-43)
            if (r6 != r0) goto L_0x00cb
            r6 = 0
            com.commind.facebook.JsonParser r0 = new com.commind.facebook.JsonParser     // Catch:{ IllegalStateException -> 0x0122, IOException -> 0x012f, OAuthMessageSignerException -> 0x013c, OAuthExpectationFailedException -> 0x0149, OAuthCommunicationException -> 0x014e }
            r0.<init>()     // Catch:{ IllegalStateException -> 0x0122, IOException -> 0x012f, OAuthMessageSignerException -> 0x013c, OAuthExpectationFailedException -> 0x0149, OAuthCommunicationException -> 0x014e }
            org.apache.http.HttpEntity r8 = r8.getEntity()     // Catch:{ IllegalStateException -> 0x011a, IOException -> 0x0127, OAuthMessageSignerException -> 0x013c, OAuthExpectationFailedException -> 0x0149, OAuthCommunicationException -> 0x014e }
            java.io.InputStream r8 = r8.getContent()     // Catch:{ IllegalStateException -> 0x011a, IOException -> 0x0127, OAuthMessageSignerException -> 0x013c, OAuthExpectationFailedException -> 0x0149, OAuthCommunicationException -> 0x014e }
            org.json.JSONArray r0 = r0.convertToJSONarray(r8)     // Catch:{ IllegalStateException -> 0x011a, IOException -> 0x0127, OAuthMessageSignerException -> 0x013c, OAuthExpectationFailedException -> 0x0149, OAuthCommunicationException -> 0x014e }
            if (r0 == 0) goto L_0x00cb
            int r1 = r0.length()     // Catch:{ IllegalStateException -> 0x015c, IOException -> 0x0159, OAuthMessageSignerException -> 0x013c, OAuthExpectationFailedException -> 0x0149, OAuthCommunicationException -> 0x014e }
            if (r1 <= 0) goto L_0x00cb
            r6 = 0
            org.json.JSONObject r6 = r0.optJSONObject(r6)     // Catch:{ IllegalStateException -> 0x015c, IOException -> 0x0159, OAuthMessageSignerException -> 0x013c, OAuthExpectationFailedException -> 0x0149, OAuthCommunicationException -> 0x014e }
            java.lang.String r8 = "id"
            java.lang.String r8 = r6.optString(r8)     // Catch:{ IllegalStateException -> 0x015c, IOException -> 0x0159, OAuthMessageSignerException -> 0x013c, OAuthExpectationFailedException -> 0x0149, OAuthCommunicationException -> 0x014e }
            int r6 = r8.length()     // Catch:{ IllegalStateException -> 0x015c, IOException -> 0x0159, OAuthMessageSignerException -> 0x013c, OAuthExpectationFailedException -> 0x0149, OAuthCommunicationException -> 0x014e }
            if (r6 <= 0) goto L_0x00cb
            android.content.SharedPreferences$Editor r6 = r2.edit()     // Catch:{ IllegalStateException -> 0x015c, IOException -> 0x0159, OAuthMessageSignerException -> 0x013c, OAuthExpectationFailedException -> 0x0149, OAuthCommunicationException -> 0x014e }
            java.lang.String r3 = "mention"
            r6.putString(r3, r8)     // Catch:{ IllegalStateException -> 0x015c, IOException -> 0x0159, OAuthMessageSignerException -> 0x013c, OAuthExpectationFailedException -> 0x0149, OAuthCommunicationException -> 0x014e }
            r6.commit()     // Catch:{ IllegalStateException -> 0x015c, IOException -> 0x0159, OAuthMessageSignerException -> 0x013c, OAuthExpectationFailedException -> 0x0149, OAuthCommunicationException -> 0x014e }
            java.lang.String r6 = "mention"
            java.lang.String r8 = java.lang.String.valueOf(r1)     // Catch:{ IllegalStateException -> 0x015c, IOException -> 0x0159, OAuthMessageSignerException -> 0x013c, OAuthExpectationFailedException -> 0x0149, OAuthCommunicationException -> 0x014e }
            r9.putExtra(r6, r8)     // Catch:{ IllegalStateException -> 0x015c, IOException -> 0x0159, OAuthMessageSignerException -> 0x013c, OAuthExpectationFailedException -> 0x0149, OAuthCommunicationException -> 0x014e }
        L_0x00cb:
            if (r7 == 0) goto L_0x0119
            org.apache.http.StatusLine r6 = r7.getStatusLine()     // Catch:{ IllegalStateException -> 0x0122, IOException -> 0x012f, OAuthMessageSignerException -> 0x013c, OAuthExpectationFailedException -> 0x0149, OAuthCommunicationException -> 0x014e }
            int r6 = r6.getStatusCode()     // Catch:{ IllegalStateException -> 0x0122, IOException -> 0x012f, OAuthMessageSignerException -> 0x013c, OAuthExpectationFailedException -> 0x0149, OAuthCommunicationException -> 0x014e }
            r8 = 200(0xc8, float:2.8E-43)
            if (r6 != r8) goto L_0x0119
            r6 = 0
            com.commind.facebook.JsonParser r8 = new com.commind.facebook.JsonParser     // Catch:{ IllegalStateException -> 0x0122, IOException -> 0x012f, OAuthMessageSignerException -> 0x013c, OAuthExpectationFailedException -> 0x0149, OAuthCommunicationException -> 0x014e }
            r8.<init>()     // Catch:{ IllegalStateException -> 0x0122, IOException -> 0x012f, OAuthMessageSignerException -> 0x013c, OAuthExpectationFailedException -> 0x0149, OAuthCommunicationException -> 0x014e }
            org.apache.http.HttpEntity r7 = r7.getEntity()     // Catch:{ IllegalStateException -> 0x0134, IOException -> 0x0141, OAuthMessageSignerException -> 0x013c, OAuthExpectationFailedException -> 0x0149, OAuthCommunicationException -> 0x014e }
            java.io.InputStream r7 = r7.getContent()     // Catch:{ IllegalStateException -> 0x0134, IOException -> 0x0141, OAuthMessageSignerException -> 0x013c, OAuthExpectationFailedException -> 0x0149, OAuthCommunicationException -> 0x014e }
            org.json.JSONArray r8 = r8.convertToJSONarray(r7)     // Catch:{ IllegalStateException -> 0x0134, IOException -> 0x0141, OAuthMessageSignerException -> 0x013c, OAuthExpectationFailedException -> 0x0149, OAuthCommunicationException -> 0x014e }
            if (r8 == 0) goto L_0x0119
            int r0 = r8.length()     // Catch:{ IllegalStateException -> 0x0156, IOException -> 0x0153, OAuthMessageSignerException -> 0x013c, OAuthExpectationFailedException -> 0x0149, OAuthCommunicationException -> 0x014e }
            if (r0 <= 0) goto L_0x0119
            r6 = 0
            org.json.JSONObject r6 = r8.optJSONObject(r6)     // Catch:{ IllegalStateException -> 0x0156, IOException -> 0x0153, OAuthMessageSignerException -> 0x013c, OAuthExpectationFailedException -> 0x0149, OAuthCommunicationException -> 0x014e }
            java.lang.String r7 = "id"
            java.lang.String r7 = r6.optString(r7)     // Catch:{ IllegalStateException -> 0x0156, IOException -> 0x0153, OAuthMessageSignerException -> 0x013c, OAuthExpectationFailedException -> 0x0149, OAuthCommunicationException -> 0x014e }
            int r6 = r7.length()     // Catch:{ IllegalStateException -> 0x0156, IOException -> 0x0153, OAuthMessageSignerException -> 0x013c, OAuthExpectationFailedException -> 0x0149, OAuthCommunicationException -> 0x014e }
            if (r6 <= 0) goto L_0x0119
            android.content.SharedPreferences$Editor r6 = r2.edit()     // Catch:{ IllegalStateException -> 0x0156, IOException -> 0x0153, OAuthMessageSignerException -> 0x013c, OAuthExpectationFailedException -> 0x0149, OAuthCommunicationException -> 0x014e }
            java.lang.String r1 = "twitter_message"
            r6.putString(r1, r7)     // Catch:{ IllegalStateException -> 0x0156, IOException -> 0x0153, OAuthMessageSignerException -> 0x013c, OAuthExpectationFailedException -> 0x0149, OAuthCommunicationException -> 0x014e }
            r6.commit()     // Catch:{ IllegalStateException -> 0x0156, IOException -> 0x0153, OAuthMessageSignerException -> 0x013c, OAuthExpectationFailedException -> 0x0149, OAuthCommunicationException -> 0x014e }
            java.lang.String r6 = "twitter_message"
            java.lang.String r7 = java.lang.String.valueOf(r0)     // Catch:{ IllegalStateException -> 0x0156, IOException -> 0x0153, OAuthMessageSignerException -> 0x013c, OAuthExpectationFailedException -> 0x0149, OAuthCommunicationException -> 0x014e }
            r9.putExtra(r6, r7)     // Catch:{ IllegalStateException -> 0x0156, IOException -> 0x0153, OAuthMessageSignerException -> 0x013c, OAuthExpectationFailedException -> 0x0149, OAuthCommunicationException -> 0x014e }
        L_0x0119:
            return
        L_0x011a:
            r8 = move-exception
            r5 = r8
            r8 = r6
            r6 = r5
        L_0x011e:
            r6.printStackTrace()     // Catch:{ IllegalStateException -> 0x0122, IOException -> 0x012f, OAuthMessageSignerException -> 0x013c, OAuthExpectationFailedException -> 0x0149, OAuthCommunicationException -> 0x014e }
            goto L_0x00cb
        L_0x0122:
            r6 = move-exception
            r6.printStackTrace()
            goto L_0x0119
        L_0x0127:
            r8 = move-exception
            r5 = r8
            r8 = r6
            r6 = r5
        L_0x012b:
            r6.printStackTrace()     // Catch:{ IllegalStateException -> 0x0122, IOException -> 0x012f, OAuthMessageSignerException -> 0x013c, OAuthExpectationFailedException -> 0x0149, OAuthCommunicationException -> 0x014e }
            goto L_0x00cb
        L_0x012f:
            r6 = move-exception
            r6.printStackTrace()
            goto L_0x0119
        L_0x0134:
            r7 = move-exception
            r5 = r7
            r7 = r6
            r6 = r5
        L_0x0138:
            r6.printStackTrace()     // Catch:{ IllegalStateException -> 0x0122, IOException -> 0x012f, OAuthMessageSignerException -> 0x013c, OAuthExpectationFailedException -> 0x0149, OAuthCommunicationException -> 0x014e }
            goto L_0x0119
        L_0x013c:
            r6 = move-exception
            r6.printStackTrace()
            goto L_0x0119
        L_0x0141:
            r7 = move-exception
            r5 = r7
            r7 = r6
            r6 = r5
        L_0x0145:
            r6.printStackTrace()     // Catch:{ IllegalStateException -> 0x0122, IOException -> 0x012f, OAuthMessageSignerException -> 0x013c, OAuthExpectationFailedException -> 0x0149, OAuthCommunicationException -> 0x014e }
            goto L_0x0119
        L_0x0149:
            r6 = move-exception
            r6.printStackTrace()
            goto L_0x0119
        L_0x014e:
            r6 = move-exception
            r6.printStackTrace()
            goto L_0x0119
        L_0x0153:
            r6 = move-exception
            r7 = r8
            goto L_0x0145
        L_0x0156:
            r6 = move-exception
            r7 = r8
            goto L_0x0138
        L_0x0159:
            r6 = move-exception
            r8 = r0
            goto L_0x012b
        L_0x015c:
            r6 = move-exception
            r8 = r0
            goto L_0x011e
        L_0x015f:
            r1 = r0
            goto L_0x005b
        L_0x0162:
            r0 = r8
            goto L_0x0027
        */
        throw new UnsupportedOperationException("Method not decompiled: com.commind.bubbles.donate.Poller.twitterReq(org.apache.http.impl.client.DefaultHttpClient, oauth.signpost.commonshttp.CommonsHttpOAuthConsumer, android.content.Context, android.content.Intent):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x008a, code lost:
        r8 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x008b, code lost:
        r8.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0096, code lost:
        r8 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0097, code lost:
        r8.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x008a A[ExcHandler: ClientProtocolException (r8v4 'e' org.apache.http.client.ClientProtocolException A[CUSTOM_DECLARE]), Splitter:B:1:0x0014] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void facebookReq(org.apache.http.impl.client.DefaultHttpClient r10, java.lang.String r11, android.content.Context r12, android.content.Intent r13) {
        /*
            org.apache.http.client.methods.HttpGet r3 = new org.apache.http.client.methods.HttpGet
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            java.lang.String r9 = "https://api.facebook.com/method/notifications.get?format=json&access_token="
            r8.<init>(r9)
            java.lang.StringBuilder r8 = r8.append(r11)
            java.lang.String r8 = r8.toString()
            r3.<init>(r8)
            org.apache.http.HttpResponse r2 = r10.execute(r3)     // Catch:{ ClientProtocolException -> 0x008a, IOException -> 0x0096 }
            if (r2 == 0) goto L_0x0083
            org.apache.http.StatusLine r8 = r2.getStatusLine()     // Catch:{ ClientProtocolException -> 0x008a, IOException -> 0x0096 }
            int r8 = r8.getStatusCode()     // Catch:{ ClientProtocolException -> 0x008a, IOException -> 0x0096 }
            r9 = 200(0xc8, float:2.8E-43)
            if (r8 != r9) goto L_0x0083
            r4 = 0
            com.commind.facebook.JsonParser r5 = new com.commind.facebook.JsonParser     // Catch:{ ClientProtocolException -> 0x008a, IOException -> 0x0096 }
            r5.<init>()     // Catch:{ ClientProtocolException -> 0x008a, IOException -> 0x0096 }
            org.apache.http.HttpEntity r8 = r2.getEntity()     // Catch:{ IllegalStateException -> 0x0084, IOException -> 0x0090, ClientProtocolException -> 0x008a }
            java.io.InputStream r8 = r8.getContent()     // Catch:{ IllegalStateException -> 0x0084, IOException -> 0x0090, ClientProtocolException -> 0x008a }
            org.json.JSONObject r4 = r5.convertToJSONobj(r8)     // Catch:{ IllegalStateException -> 0x0084, IOException -> 0x0090, ClientProtocolException -> 0x008a }
            java.lang.String r8 = "notification_counts"
            boolean r8 = r4.has(r8)     // Catch:{ IllegalStateException -> 0x0084, IOException -> 0x0090, ClientProtocolException -> 0x008a }
            if (r8 == 0) goto L_0x0051
            java.lang.String r8 = "notification_counts"
            org.json.JSONObject r6 = r4.optJSONObject(r8)     // Catch:{ IllegalStateException -> 0x0084, IOException -> 0x0090, ClientProtocolException -> 0x008a }
            java.lang.String r8 = "unseen"
            int r7 = r6.optInt(r8)     // Catch:{ IllegalStateException -> 0x0084, IOException -> 0x0090, ClientProtocolException -> 0x008a }
            java.lang.String r8 = "notification_counts"
            r13.putExtra(r8, r7)     // Catch:{ IllegalStateException -> 0x0084, IOException -> 0x0090, ClientProtocolException -> 0x008a }
        L_0x0051:
            java.lang.String r8 = "friend_requests_counts"
            boolean r8 = r4.has(r8)     // Catch:{ IllegalStateException -> 0x0084, IOException -> 0x0090, ClientProtocolException -> 0x008a }
            if (r8 == 0) goto L_0x006a
            java.lang.String r8 = "friend_requests_counts"
            org.json.JSONObject r6 = r4.optJSONObject(r8)     // Catch:{ IllegalStateException -> 0x0084, IOException -> 0x0090, ClientProtocolException -> 0x008a }
            java.lang.String r8 = "unseen"
            int r7 = r6.optInt(r8)     // Catch:{ IllegalStateException -> 0x0084, IOException -> 0x0090, ClientProtocolException -> 0x008a }
            java.lang.String r8 = "friend_requests_counts"
            r13.putExtra(r8, r7)     // Catch:{ IllegalStateException -> 0x0084, IOException -> 0x0090, ClientProtocolException -> 0x008a }
        L_0x006a:
            java.lang.String r8 = "messages"
            boolean r8 = r4.has(r8)     // Catch:{ IllegalStateException -> 0x0084, IOException -> 0x0090, ClientProtocolException -> 0x008a }
            if (r8 == 0) goto L_0x0083
            java.lang.String r8 = "messages"
            org.json.JSONObject r6 = r4.optJSONObject(r8)     // Catch:{ IllegalStateException -> 0x0084, IOException -> 0x0090, ClientProtocolException -> 0x008a }
            java.lang.String r8 = "unread"
            int r7 = r6.optInt(r8)     // Catch:{ IllegalStateException -> 0x0084, IOException -> 0x0090, ClientProtocolException -> 0x008a }
            java.lang.String r8 = "messages"
            r13.putExtra(r8, r7)     // Catch:{ IllegalStateException -> 0x0084, IOException -> 0x0090, ClientProtocolException -> 0x008a }
        L_0x0083:
            return
        L_0x0084:
            r8 = move-exception
            r0 = r8
            r0.printStackTrace()     // Catch:{ ClientProtocolException -> 0x008a, IOException -> 0x0096 }
            goto L_0x0083
        L_0x008a:
            r8 = move-exception
            r1 = r8
            r1.printStackTrace()
            goto L_0x0083
        L_0x0090:
            r8 = move-exception
            r0 = r8
            r0.printStackTrace()     // Catch:{ ClientProtocolException -> 0x008a, IOException -> 0x0096 }
            goto L_0x0083
        L_0x0096:
            r8 = move-exception
            r1 = r8
            r1.printStackTrace()
            goto L_0x0083
        */
        throw new UnsupportedOperationException("Method not decompiled: com.commind.bubbles.donate.Poller.facebookReq(org.apache.http.impl.client.DefaultHttpClient, java.lang.String, android.content.Context, android.content.Intent):void");
    }

    private static final class GzipHttpRequestInterceptor implements HttpRequestInterceptor {
        private GzipHttpRequestInterceptor() {
        }

        /* synthetic */ GzipHttpRequestInterceptor(GzipHttpRequestInterceptor gzipHttpRequestInterceptor) {
            this();
        }

        public void process(HttpRequest request, HttpContext context) throws HttpException, IOException {
            if (!request.containsHeader("Accept-Encoding")) {
                request.addHeader("Accept-Encoding", "gzip");
            }
        }
    }

    private static final class GzipHttpResponseInterceptor implements HttpResponseInterceptor {
        private GzipHttpResponseInterceptor() {
        }

        /* synthetic */ GzipHttpResponseInterceptor(GzipHttpResponseInterceptor gzipHttpResponseInterceptor) {
            this();
        }

        public void process(HttpResponse response, HttpContext context) throws HttpException, IOException {
            Header header = response.getEntity().getContentEncoding();
            if (header != null) {
                HeaderElement[] codecs = header.getElements();
                for (HeaderElement name : codecs) {
                    if (name.getName().equalsIgnoreCase("gzip")) {
                        response.setEntity(new GzipDecompressingEntity(response.getEntity()));
                        return;
                    }
                }
            }
        }
    }

    static class GzipDecompressingEntity extends HttpEntityWrapper {
        public GzipDecompressingEntity(HttpEntity entity) {
            super(entity);
        }

        public InputStream getContent() throws IOException, IllegalStateException {
            return new GZIPInputStream(this.wrappedEntity.getContent());
        }

        public long getContentLength() {
            return -1;
        }
    }
}
