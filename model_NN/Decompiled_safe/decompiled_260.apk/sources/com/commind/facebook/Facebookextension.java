package com.commind.facebook;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.util.Log;
import android.widget.Toast;
import com.commind.facebook.SessionEvents;
import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.FacebookError;
import com.facebook.android.Util;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Iterator;
import org.apache.http.HttpResponse;
import org.json.JSONArray;
import org.json.JSONException;

public abstract class Facebookextension {
    public static final String APP_ID = "154743571256665";
    private static final String[] PERMISSIONS = {"read_stream", "read_mailbox", "user_events", "publish_stream"};
    private AsyncFacebookRunner mAsyncRunner = new AsyncFacebookRunner(this.mFacebook);
    /* access modifiers changed from: private */
    public Context mContext;
    /* access modifiers changed from: private */
    public Facebook mFacebook = new Facebook(APP_ID);

    public abstract void onResult(boolean z);

    public Facebookextension(Context a) {
        SessionEvents.addAuthListener(new SampleAuthListener(this, null));
        SessionEvents.addLogoutListener(new SampleLogoutListener());
        this.mContext = a;
    }

    public void logout() {
        this.mAsyncRunner.logout(this.mContext, new LogoutRequestListener());
    }

    public static String getAccesToken(Context c) {
        Facebook fc = new Facebook(APP_ID);
        if (!SessionStore.restore(fc, c)) {
            return null;
        }
        return fc.getAccessToken();
    }

    public boolean login() {
        if (SessionStore.restore(this.mFacebook, this.mContext)) {
            return true;
        }
        this.mFacebook.authorize((PreferenceActivity) this.mContext, PERMISSIONS, new LoginDialogListener(this, null));
        return false;
    }

    public void getFriends() {
        this.mAsyncRunner.request("me/friends", new SampleRequestListener());
    }

    public void getFeed() {
        this.mAsyncRunner.request("me/feed", new NewsFeedListener(this, null));
    }

    public void getNews() {
        this.mAsyncRunner.request("me/home", new SampleRequestListener());
    }

    public void getPosts() {
        this.mAsyncRunner.request("me/posts", new SampleRequestListener());
    }

    public void getInbox() {
        this.mAsyncRunner.request("me/inbox", new SampleRequestListener());
    }

    public void getEvents() {
        this.mAsyncRunner.request("me/events", new SampleRequestListener());
    }

    public void authorizeCallback(int requestCode, int resultCode, Intent data) {
        this.mFacebook.authorizeCallback(requestCode, resultCode, data);
    }

    private final class LoginDialogListener implements Facebook.DialogListener {
        private LoginDialogListener() {
        }

        /* synthetic */ LoginDialogListener(Facebookextension facebookextension, LoginDialogListener loginDialogListener) {
            this();
        }

        public void onComplete(Bundle values) {
            SessionStore.save(Facebookextension.this.mFacebook, Facebookextension.this.mContext);
            SessionEvents.onLoginSuccess();
            Facebookextension.this.onResult(true);
        }

        public void onFacebookError(FacebookError error) {
            SessionEvents.onLoginError(error.getMessage());
        }

        public void onError(DialogError error) {
            SessionEvents.onLoginError(error.getMessage());
        }

        public void onCancel() {
            SessionEvents.onLoginError("操作已取消");
        }
    }

    private class SampleAuthListener implements SessionEvents.AuthListener {
        private SampleAuthListener() {
        }

        /* synthetic */ SampleAuthListener(Facebookextension facebookextension, SampleAuthListener sampleAuthListener) {
            this();
        }

        public void onAuthSucceed() {
            Toast.makeText(Facebookextension.this.mContext, "Facebook login successful!", 0).show();
        }

        public void onAuthFail(String error) {
            Toast.makeText(Facebookextension.this.mContext, error, 0).show();
        }
    }

    public class SampleLogoutListener implements SessionEvents.LogoutListener {
        public SampleLogoutListener() {
        }

        public void onLogoutBegin() {
        }

        public void onLogoutFinish() {
            Toast.makeText(Facebookextension.this.mContext, "You have logged out!", 0).show();
            SessionStore.clear(Facebookextension.this.mContext);
        }
    }

    public class LogoutRequestListener extends BaseRequestListener {
        public LogoutRequestListener() {
        }

        public void onComplete(String response, Object state) {
            SessionStore.clear(Facebookextension.this.mContext);
        }

        public void onFacebookError(FacebookError e, Object state) {
            SessionStore.clear(Facebookextension.this.mContext);
        }

        public void onFileNotFoundException(FileNotFoundException e, Object state) {
            SessionStore.clear(Facebookextension.this.mContext);
        }

        public void onIOException(IOException e, Object state) {
            SessionStore.clear(Facebookextension.this.mContext);
        }

        public void onMalformedURLException(MalformedURLException e, Object state) {
        }
    }

    private class NewsFeedListener extends BaseRequestListener {
        private NewsFeedListener() {
        }

        /* synthetic */ NewsFeedListener(Facebookextension facebookextension, NewsFeedListener newsFeedListener) {
            this();
        }

        public void onComplete(String response, Object state) {
            try {
                Log.d("Facebook-Example", "Response: " + response.toString());
                JSONArray data = Util.parseJson(response).getJSONArray("data");
                ArrayList<ContentValues> jSONContentValues = new JsonParser().getJSONContentValues(data, new String[]{"from", "message"}, new String[]{"id", "message"});
            } catch (JSONException e) {
                Log.w("Facebook-Example", "JSON Error in response");
            } catch (FacebookError e2) {
                Log.w("Facebook-Example", "Facebook Error: " + e2.getMessage());
            }
        }

        public void onFacebookError(FacebookError e, Object state) {
            e.printStackTrace();
        }

        public void onFileNotFoundException(FileNotFoundException e, Object state) {
            e.printStackTrace();
        }

        public void onIOException(IOException e, Object state) {
            e.printStackTrace();
        }

        public void onMalformedURLException(MalformedURLException e, Object state) {
            e.printStackTrace();
        }
    }

    public class SampleRequestListener extends BaseRequestListener {
        public SampleRequestListener() {
        }

        public void onComplete(String response, Object state) {
            try {
                Log.d("Facebook-Example", "Response: " + response.toString());
                JSONArray friends = Util.parseJson(response).getJSONArray("data");
                ArrayList<ContentValues> values = new JsonParser().getJSONContentValues(friends, new String[]{FacebookProvider.KEY_ID, FacebookProvider.KEY_NAME}, new String[]{"id", "name"});
                if (values != null) {
                    ContentResolver cr = Facebookextension.this.mContext.getContentResolver();
                    cr.delete(FacebookProvider.CONTENT_URI, null, null);
                    HttpClient httpclient = new HttpClient(Facebookextension.this.mContext) {
                        public void onHttpResponse(HttpResponse response, String requestedurl) {
                        }
                    };
                    Iterator<ContentValues> it = values.iterator();
                    while (it.hasNext()) {
                        ContentValues v = it.next();
                        v.put(FacebookProvider.KEY_ID, v.getAsString(FacebookProvider.KEY_ID));
                        cr.insert(FacebookProvider.CONTENT_URI, v);
                    }
                    httpclient.requesFriendsTask(null);
                }
            } catch (JSONException e) {
                Log.w("Facebook-Example", "JSON Error in response");
            } catch (FacebookError e2) {
                Log.w("Facebook-Example", "Facebook Error: " + e2.getMessage());
            }
        }

        public void onFacebookError(FacebookError e, Object state) {
            e.printStackTrace();
        }

        public void onFileNotFoundException(FileNotFoundException e, Object state) {
            e.printStackTrace();
        }

        public void onIOException(IOException e, Object state) {
            e.printStackTrace();
        }

        public void onMalformedURLException(MalformedURLException e, Object state) {
            e.printStackTrace();
        }
    }
}
