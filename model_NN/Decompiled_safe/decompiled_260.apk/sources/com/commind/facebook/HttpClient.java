package com.commind.facebook;

import android.content.ContentResolver;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.widget.ImageView;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;
import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HttpEntity;
import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.HttpResponse;
import org.apache.http.HttpResponseInterceptor;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.conn.params.ConnPerRouteBean;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.HttpEntityWrapper;
import org.apache.http.impl.DefaultConnectionReuseStrategy;
import org.apache.http.impl.client.DefaultConnectionKeepAliveStrategy;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.client.DefaultRedirectHandler;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;

public abstract class HttpClient {
    private final String USERAGENT = (String.valueOf(Build.DEVICE) + "&os=Android-" + Build.VERSION.RELEASE);
    private ContentResolver mContentresolver;
    /* access modifiers changed from: private */
    public HttpRequestTask mCurrentTask = null;
    public DefaultHttpClient mDefHttpClient = null;

    public abstract void onHttpResponse(HttpResponse httpResponse, String str);

    public HttpClient() {
        if (this.mDefHttpClient == null) {
            this.mDefHttpClient = createHttpClient();
        }
    }

    public HttpClient(Context context) {
        if (context != null) {
            this.mContentresolver = context.getContentResolver();
        }
    }

    private DefaultHttpClient createHttpClient() {
        HttpParams httpParams = new BasicHttpParams();
        ConnManagerParams.setMaxTotalConnections(httpParams, 20);
        ConnManagerParams.setMaxConnectionsPerRoute(httpParams, new ConnPerRouteBean(2));
        ConnManagerParams.setTimeout(httpParams, 10000);
        SchemeRegistry schemeRegistry = new SchemeRegistry();
        schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
        schemeRegistry.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));
        DefaultHttpClient httpClient = new DefaultHttpClient(new ThreadSafeClientConnManager(httpParams, schemeRegistry), httpParams);
        httpClient.setHttpRequestRetryHandler(new DefaultHttpRequestRetryHandler(10, true));
        httpClient.setReuseStrategy(new DefaultConnectionReuseStrategy());
        httpClient.setKeepAliveStrategy(new DefaultConnectionKeepAliveStrategy());
        httpClient.setRedirectHandler(new DefaultRedirectHandler());
        return httpClient;
    }

    public void requestURLandParseTask(String url, boolean b, boolean shouldpost) {
        this.mCurrentTask = new HttpRequestTask(this, null);
        this.mCurrentTask.execute(url, Boolean.toString(b), Boolean.toString(shouldpost));
    }

    private class HttpRequestTask extends AsyncTask<String, Void, Boolean> {
        private String requestedurl;
        private HttpResponse response;

        private HttpRequestTask() {
        }

        /* synthetic */ HttpRequestTask(HttpClient httpClient, HttpRequestTask httpRequestTask) {
            this();
        }

        /* access modifiers changed from: protected */
        public Boolean doInBackground(String... url) {
            boolean b = false;
            this.requestedurl = url[0];
            if (url[1].equals("true")) {
                this.response = HttpClient.this.requestURLandCallback(url[0]);
            } else {
                this.response = HttpClient.this.requestURLandCallback(url[0]);
            }
            if (this.response != null) {
                b = true;
            }
            return Boolean.valueOf(b);
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Boolean result) {
            HttpClient.this.onHttpResponse(this.response, this.requestedurl);
            HttpClient.this.mCurrentTask = null;
        }
    }

    private boolean postURLandCallback(String url) {
        try {
            HttpResponse response = this.mDefHttpClient.execute(new HttpPost(url));
            if (response == null || response.getStatusLine().getStatusCode() != 200) {
                return false;
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /* access modifiers changed from: private */
    public HttpResponse requestURLandCallback(String url) {
        HttpGet httpget = new HttpGet(url);
        httpget.setHeader("User-Agent", this.USERAGENT);
        try {
            HttpResponse getresponse = this.mDefHttpClient.execute(httpget);
            if (getresponse == null || getresponse.getStatusLine().getStatusCode() != 200) {
                return null;
            }
            return getresponse;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private class LoadImageTask extends AsyncTask<ImageView, Void, Bitmap> {
        private ImageView mImage;

        private LoadImageTask() {
        }

        /* access modifiers changed from: protected */
        public Bitmap doInBackground(ImageView... img) {
            this.mImage = img[0];
            String url = (String) this.mImage.getContentDescription();
            if (url == null) {
                return null;
            }
            Bitmap bitmap = null;
            try {
                HttpResponse getresponse = HttpClient.this.mDefHttpClient.execute(new HttpGet(url));
                if (getresponse != null && getresponse.getStatusLine().getStatusCode() == 200) {
                    byte[] bArr = null;
                    byte[] icon = EntityUtils.toByteArray(getresponse.getEntity());
                    if (icon != null) {
                        bitmap = BitmapFactory.decodeByteArray(icon, 0, icon.length);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Bitmap bm) {
            if (bm != null && this.mImage != null) {
                this.mImage.setImageBitmap(bm);
            }
        }
    }

    private final class GzipHttpRequestInterceptor implements HttpRequestInterceptor {
        private GzipHttpRequestInterceptor() {
        }

        /* synthetic */ GzipHttpRequestInterceptor(HttpClient httpClient, GzipHttpRequestInterceptor gzipHttpRequestInterceptor) {
            this();
        }

        public void process(HttpRequest request, HttpContext context) throws HttpException, IOException {
            if (!request.containsHeader("Accept-Encoding")) {
                request.addHeader("Accept-Encoding", "gzip");
            }
        }
    }

    private final class GzipHttpResponseInterceptor implements HttpResponseInterceptor {
        private GzipHttpResponseInterceptor() {
        }

        /* synthetic */ GzipHttpResponseInterceptor(HttpClient httpClient, GzipHttpResponseInterceptor gzipHttpResponseInterceptor) {
            this();
        }

        public void process(HttpResponse response, HttpContext context) throws HttpException, IOException {
            Header header = response.getEntity().getContentEncoding();
            if (header != null) {
                HeaderElement[] codecs = header.getElements();
                for (HeaderElement name : codecs) {
                    if (name.getName().equalsIgnoreCase("gzip")) {
                        response.setEntity(new GzipDecompressingEntity(response.getEntity()));
                        return;
                    }
                }
            }
        }
    }

    static class GzipDecompressingEntity extends HttpEntityWrapper {
        public GzipDecompressingEntity(HttpEntity entity) {
            super(entity);
        }

        public InputStream getContent() throws IOException, IllegalStateException {
            return new GZIPInputStream(this.wrappedEntity.getContent());
        }

        public long getContentLength() {
            return -1;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x00d7 A[Catch:{ Exception -> 0x00e8 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void requesFriendsTask(java.lang.String r24) {
        /*
            r23 = this;
            org.apache.http.impl.client.DefaultHttpClient r17 = r23.createHttpClient()
            com.commind.facebook.HttpClient$GzipHttpRequestInterceptor r5 = new com.commind.facebook.HttpClient$GzipHttpRequestInterceptor
            r6 = 0
            r0 = r5
            r1 = r23
            r2 = r6
            r0.<init>(r1, r2)
            r0 = r17
            r1 = r5
            r0.addRequestInterceptor(r1)
            com.commind.facebook.HttpClient$GzipHttpResponseInterceptor r5 = new com.commind.facebook.HttpClient$GzipHttpResponseInterceptor
            r6 = 0
            r0 = r5
            r1 = r23
            r2 = r6
            r0.<init>(r1, r2)
            r0 = r17
            r1 = r5
            r0.addResponseInterceptor(r1)
            r0 = r23
            android.content.ContentResolver r0 = r0.mContentresolver     // Catch:{ Exception -> 0x00e8 }
            r5 = r0
            android.net.Uri r6 = com.commind.facebook.FacebookProvider.CONTENT_URI     // Catch:{ Exception -> 0x00e8 }
            r7 = 1
            java.lang.String[] r7 = new java.lang.String[r7]     // Catch:{ Exception -> 0x00e8 }
            r8 = 0
            java.lang.String r9 = "_id"
            r7[r8] = r9     // Catch:{ Exception -> 0x00e8 }
            r8 = 0
            r9 = 0
            r10 = 0
            android.database.Cursor r13 = r5.query(r6, r7, r8, r9, r10)     // Catch:{ Exception -> 0x00e8 }
            if (r13 == 0) goto L_0x00d5
            boolean r5 = r13.moveToFirst()     // Catch:{ Exception -> 0x00e8 }
            if (r5 == 0) goto L_0x00d5
        L_0x0042:
            r5 = 0
            java.lang.String r20 = r13.getString(r5)     // Catch:{ Exception -> 0x00e8 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00e8 }
            java.lang.String r6 = "http://graph.facebook.com/"
            r5.<init>(r6)     // Catch:{ Exception -> 0x00e8 }
            r0 = r5
            r1 = r20
            java.lang.StringBuilder r5 = r0.append(r1)     // Catch:{ Exception -> 0x00e8 }
            java.lang.String r6 = "/picture"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x00e8 }
            java.lang.String r22 = r5.toString()     // Catch:{ Exception -> 0x00e8 }
            org.apache.http.client.methods.HttpGet r19 = new org.apache.http.client.methods.HttpGet     // Catch:{ Exception -> 0x00e8 }
            r0 = r19
            r1 = r22
            r0.<init>(r1)     // Catch:{ Exception -> 0x00e8 }
            java.lang.String r5 = "User-Agent"
            r0 = r23
            java.lang.String r0 = r0.USERAGENT     // Catch:{ Exception -> 0x00e8 }
            r6 = r0
            r0 = r19
            r1 = r5
            r2 = r6
            r0.setHeader(r1, r2)     // Catch:{ Exception -> 0x00e8 }
            r0 = r17
            r1 = r19
            org.apache.http.HttpResponse r15 = r0.execute(r1)     // Catch:{ Exception -> 0x00e8 }
            if (r15 == 0) goto L_0x00cf
            org.apache.http.StatusLine r5 = r15.getStatusLine()     // Catch:{ Exception -> 0x00e8 }
            int r5 = r5.getStatusCode()     // Catch:{ Exception -> 0x00e8 }
            r6 = 200(0xc8, float:2.8E-43)
            if (r5 != r6) goto L_0x00cf
            org.apache.http.HttpEntity r18 = r15.getEntity()     // Catch:{ Exception -> 0x00e8 }
            org.apache.http.Header r16 = r18.getContentType()     // Catch:{ Exception -> 0x00e8 }
            java.lang.String r12 = r16.getValue()     // Catch:{ Exception -> 0x00e8 }
            java.lang.String r5 = "image"
            boolean r5 = r12.contains(r5)     // Catch:{ Exception -> 0x00e8 }
            if (r5 == 0) goto L_0x00cf
            r11 = 0
            byte[] r11 = (byte[]) r11     // Catch:{ Exception -> 0x00e8 }
            byte[] r11 = org.apache.http.util.EntityUtils.toByteArray(r18)     // Catch:{ IOException -> 0x00e2 }
            android.content.ContentValues r21 = new android.content.ContentValues     // Catch:{ IOException -> 0x00e2 }
            r21.<init>()     // Catch:{ IOException -> 0x00e2 }
            java.lang.String r5 = "photo_data"
            r0 = r21
            r1 = r5
            r2 = r11
            r0.put(r1, r2)     // Catch:{ IOException -> 0x00e2 }
            r0 = r23
            android.content.ContentResolver r0 = r0.mContentresolver     // Catch:{ IOException -> 0x00e2 }
            r5 = r0
            android.net.Uri r6 = com.commind.facebook.FacebookProvider.CONTENT_URI     // Catch:{ IOException -> 0x00e2 }
            long r7 = java.lang.Long.parseLong(r20)     // Catch:{ IOException -> 0x00e2 }
            android.net.Uri r6 = android.content.ContentUris.withAppendedId(r6, r7)     // Catch:{ IOException -> 0x00e2 }
            r7 = 0
            r8 = 0
            r0 = r5
            r1 = r6
            r2 = r21
            r3 = r7
            r4 = r8
            r0.update(r1, r2, r3, r4)     // Catch:{ IOException -> 0x00e2 }
        L_0x00cf:
            boolean r5 = r13.moveToNext()     // Catch:{ Exception -> 0x00e8 }
            if (r5 != 0) goto L_0x0042
        L_0x00d5:
            if (r13 == 0) goto L_0x00da
            r13.close()     // Catch:{ Exception -> 0x00e8 }
        L_0x00da:
            org.apache.http.conn.ClientConnectionManager r5 = r17.getConnectionManager()
            r5.shutdown()
            return
        L_0x00e2:
            r5 = move-exception
            r14 = r5
            r14.printStackTrace()     // Catch:{ Exception -> 0x00e8 }
            goto L_0x00cf
        L_0x00e8:
            r5 = move-exception
            r14 = r5
            r14.printStackTrace()
            goto L_0x00da
        */
        throw new UnsupportedOperationException("Method not decompiled: com.commind.facebook.HttpClient.requesFriendsTask(java.lang.String):void");
    }

    private class HttpRequestFriendsTask extends AsyncTask<String, Void, Void> {
        private HttpRequestFriendsTask() {
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(String... url) {
            return null;
        }
    }
}
