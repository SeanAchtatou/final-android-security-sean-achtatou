package com.commind.facebook;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

public class FacebookProvider extends ContentProvider {
    public static final Uri CONTENT_URI = Uri.parse("content://com.commind.facebook_bubble/FRIENDS");
    private static final String DATABASE_CREATE = "CREATE TABLE friends(_id STRING PRIMARY KEY, text STRING,selected INTEGER DEFAULT 0,photo_data BLOB);";
    private static final String DATABASE_NAME = "facebook.db";
    private static final String DATABASE_TABLE = "friends";
    private static final int DATABASE_VERSION = 1;
    private static final int FRIENDS = 1;
    private static final int FRIENDS_ID = 2;
    public static final String KEY_ID = "_id";
    public static final String KEY_NAME = "text";
    public static final String KEY_PHOTO_DATA = "photo_data";
    public static final String KEY_SELECTED = "selected";
    public static final String KEY_TITLE = "title";
    private static final String PROVIDER_NAME = "com.commind.facebook_bubble";
    private static final String TAG = "FacebookDatabase";
    private static final UriMatcher URI_MATCHER = new UriMatcher(-1);
    private SQLiteDatabase mDB;
    private DatabaseHelper mDatabaseHelper;

    static {
        URI_MATCHER.addURI(PROVIDER_NAME, "FRIENDS", 1);
        URI_MATCHER.addURI(PROVIDER_NAME, "FRIENDS/#", 2);
    }

    private static class DatabaseHelper extends SQLiteOpenHelper {
        /* synthetic */ DatabaseHelper(Context context, String str, SQLiteDatabase.CursorFactory cursorFactory, int i, DatabaseHelper databaseHelper) {
            this(context, str, cursorFactory, i);
        }

        private DatabaseHelper(Context context, String s, SQLiteDatabase.CursorFactory cursorFactory, int version) {
            super(context, s, cursorFactory, version);
        }

        public void onCreate(SQLiteDatabase db) {
            db.execSQL(FacebookProvider.DATABASE_CREATE);
        }

        public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
            if (oldVersion < 20) {
                Log.w(FacebookProvider.TAG, "Upgrading database from version " + oldVersion + " to " + newVersion + ", which will destroy all old data");
                database.execSQL("DROP TABLE IF EXISTS friends");
                onCreate(database);
            }
        }
    }

    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int count;
        switch (URI_MATCHER.match(uri)) {
            case 1:
                count = this.mDB.delete(DATABASE_TABLE, selection, selectionArgs);
                break;
            case 2:
                count = this.mDB.delete(DATABASE_TABLE, "_id = " + uri.getPathSegments().get(1) + (!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : ""), selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    public String getType(Uri uri) {
        switch (URI_MATCHER.match(uri)) {
            case 1:
                return "com.commind.facebook/all";
            case 2:
                return "com.commind.facebook/item";
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }

    public Uri insert(Uri uri, ContentValues values) {
        long rowID = this.mDB.insert(DATABASE_TABLE, "", values);
        if (rowID > 0) {
            Uri _uri = ContentUris.withAppendedId(CONTENT_URI, rowID);
            getContext().getContentResolver().notifyChange(_uri, null);
            return _uri;
        }
        throw new SQLException("Failed to insert row into " + uri);
    }

    public boolean onCreate() {
        try {
            this.mDatabaseHelper = new DatabaseHelper(getContext(), DATABASE_NAME, null, 1, null);
            this.mDB = this.mDatabaseHelper.getWritableDatabase();
            return true;
        } catch (Exception e) {
            Log.e(TAG, "Failed to create content provider.", e);
            return false;
        }
    }

    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder sqlBuilder = new SQLiteQueryBuilder();
        sqlBuilder.setTables(DATABASE_TABLE);
        if (URI_MATCHER.match(uri) == 2) {
            sqlBuilder.appendWhere("_id = " + uri.getPathSegments().get(1));
        }
        if (sortOrder == null || sortOrder == "") {
            sortOrder = KEY_ID;
        }
        Cursor c = sqlBuilder.query(this.mDB, projection, selection, selectionArgs, null, null, sortOrder);
        c.setNotificationUri(getContext().getContentResolver(), uri);
        return c;
    }

    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int count;
        switch (URI_MATCHER.match(uri)) {
            case 1:
                count = this.mDB.update(DATABASE_TABLE, values, selection, selectionArgs);
                break;
            case 2:
                count = this.mDB.update(DATABASE_TABLE, values, "_id = " + uri.getPathSegments().get(1) + (!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : ""), selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }
}
