package com.commind.facebook;

import android.content.ContentValues;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import org.apache.http.util.ByteArrayBuffer;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JsonParser {
    public JSONObject convertToJSONobj(InputStream input) {
        BufferedInputStream bis = new BufferedInputStream(input);
        ByteArrayBuffer baf = new ByteArrayBuffer(50);
        try {
            byte[] buffer = new byte[512];
            while (true) {
                int read = bis.read(buffer);
                if (read == -1) {
                    return new JSONObject(new String(baf.toByteArray()));
                }
                baf.append(buffer, 0, read);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public JSONArray convertToJSONarray(InputStream input) {
        BufferedReader buffreader = new BufferedReader(new InputStreamReader(input));
        StringBuilder stringbuilder = new StringBuilder();
        JSONArray jsonarray = null;
        while (true) {
            try {
                String jsonString = buffreader.readLine();
                if (jsonString == null) {
                    break;
                }
                stringbuilder.append(jsonString);
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
        jsonarray = new JSONArray(stringbuilder.toString());
        try {
            buffreader.close();
        } catch (IOException e3) {
            e3.printStackTrace();
        }
        return jsonarray;
    }

    public ArrayList<String[]> getJSONValues(JSONObject object, String musthave, String[] inputs) {
        if (object == null) {
            return null;
        }
        int inputslen = inputs.length;
        String[] values = new String[inputslen];
        for (int k = 0; k < inputslen; k++) {
            values[k] = object.optString(inputs[k]);
        }
        ArrayList<String[]> listvalues = new ArrayList<>();
        listvalues.add(values);
        return listvalues;
    }

    public ArrayList<String[]> getJSONValues(JSONArray array, String musthave, String[] inputs) {
        ArrayList<String[]> listvalues = null;
        int len = array.length();
        for (int i = 0; i < len; i++) {
            JSONObject jsonobject = array.optJSONObject(i);
            if (jsonobject != null) {
                int inputslen = inputs.length;
                String[] values = new String[inputslen];
                for (int k = 0; k < inputslen; k++) {
                    values[k] = jsonobject.optString(inputs[k]);
                }
                if (listvalues == null) {
                    listvalues = new ArrayList<>();
                }
                listvalues.add(values);
            }
        }
        return listvalues;
    }

    public ArrayList<String[]> getJSONObjectValues(JSONArray array, String object, String[] inputs) {
        ArrayList<String[]> listvalues = null;
        int len = array.length();
        for (int i = 0; i < len; i++) {
            JSONObject jsonobject = array.optJSONObject(i).optJSONObject(object);
            if (jsonobject != null) {
                int inputslen = inputs.length;
                String[] values = new String[inputslen];
                for (int k = 0; k < inputslen; k++) {
                    values[k] = jsonobject.optString(inputs[k]);
                }
                if (listvalues == null) {
                    listvalues = new ArrayList<>();
                }
                listvalues.add(values);
            }
        }
        return listvalues;
    }

    public ArrayList<ContentValues> getJSONContentValues(JSONArray array, String[] contentkeys, String[] inputs) {
        ArrayList<ContentValues> listvalues = null;
        int len = array.length();
        if (inputs == null) {
            inputs = contentkeys;
        }
        for (int i = 0; i < len; i++) {
            JSONObject jsonobject = array.optJSONObject(i);
            if (jsonobject != null) {
                int inputslen = inputs.length;
                String[] values = new String[inputslen];
                ContentValues tempvalues = new ContentValues();
                for (int k = 0; k < inputslen; k++) {
                    values[k] = jsonobject.optString(inputs[k]);
                    if (values[k].length() > 0) {
                        if (listvalues == null) {
                            listvalues = new ArrayList<>();
                        }
                        tempvalues.put(contentkeys[k], values[k]);
                    }
                }
                listvalues.add(tempvalues);
            }
        }
        return listvalues;
    }

    public ContentValues getJSONContentValue(JSONObject jsonobject, String[] contentkeys, String[] inputs) {
        ContentValues listvalues = null;
        if (inputs == null) {
            inputs = contentkeys;
        }
        if (jsonobject != null) {
            int inputslen = inputs.length;
            String[] values = new String[inputslen];
            for (int k = 0; k < inputslen; k++) {
                values[k] = jsonobject.optString(inputs[k]);
                if (values[k].length() > 0) {
                    if (listvalues == null) {
                        listvalues = new ContentValues();
                    }
                    listvalues.put(contentkeys[k], values[k]);
                }
            }
        }
        return listvalues;
    }
}
