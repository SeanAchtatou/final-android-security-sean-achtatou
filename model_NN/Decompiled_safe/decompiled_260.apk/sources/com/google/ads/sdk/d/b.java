package com.google.ads.sdk.d;

import java.util.HashMap;
import java.util.Map;

public final class b extends Exception {
    public static final Integer a = 1;
    public static final Integer b = 2;
    public static final Integer c = 3;
    public static final Integer d = 4;
    private static Map e;
    private Integer f;
    private String g;

    static {
        HashMap hashMap = new HashMap();
        e = hashMap;
        hashMap.put(a, "data length wrong");
        e.put(b, "bad request command");
        e.put(c, "unsupport response command");
        e.put(d, "invalid response command");
    }

    public b(Integer num) {
        this.f = num;
    }

    public b(Integer num, String str) {
        this.f = num;
        this.g = str;
    }

    public b(String str) {
        super(str);
        this.g = str;
    }

    public final String getMessage() {
        return (String) e.get(this.f);
    }

    public final String toString() {
        return this.g != null ? this.g : getMessage();
    }
}
