package com.google.ads.sdk.active;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import com.google.ads.AdWallActivity;
import com.google.ads.sdk.b.d;
import com.google.ads.sdk.e.g;
import com.google.ads.sdk.f.a;
import com.google.ads.sdk.f.e;
import com.google.ads.sdk.f.k;
import com.google.ads.sdk.f.p;
import com.google.ads.sdk.f.r;

public class MyAdsActivity extends Activity {
    private static final String b = k.a(MyAdsActivity.class);
    boolean a = false;
    private d c;

    public void onCreate(Bundle bundle) {
        e.c(b, "onCreate");
        Intent intent = getIntent();
        getWindow().setFlags(1024, 1024);
        requestWindowFeature(1);
        int intExtra = intent.getIntExtra("NOTIFICATION_ACTION_MODE", 0);
        String stringExtra = intent.getStringExtra("PUSH_INFO");
        if (!p.a(stringExtra)) {
            this.c = new d(stringExtra);
        }
        if (this.c == null) {
            String stringExtra2 = intent.getStringExtra("PUSH_INFO_FOR_JSON");
            if (p.a(stringExtra2)) {
                finish();
                return;
            }
            this.c = new d(stringExtra2);
            if (this.c == null) {
                finish();
                return;
            }
        }
        g gVar = new g(this, this.c);
        if (intExtra != 1 || this.c == null || !a.b(this, this.c.b.m) || ((!this.c.b.p || !com.google.ads.sdk.c.a.d(this, this.c.b.m)) && this.c.b.p)) {
            setContentView(gVar);
            if (intExtra == 1) {
                r.a(this, Integer.parseInt(this.c.b.e), 111);
            } else if (intExtra == 2) {
                r.a(this, Integer.parseInt(this.c.b.e), 119);
            } else {
                r.a(this, Integer.parseInt(this.c.b.e), 103);
            }
        } else {
            try {
                if (a.c(this, this.c.b.m)) {
                    r.a(this, Integer.parseInt(this.c.b.e), 112);
                    finish();
                } else {
                    setContentView(gVar);
                    r.a(this, Integer.parseInt(this.c.b.e), 111);
                }
            } catch (Exception e) {
                setContentView(gVar);
                r.a(this, Integer.parseInt(this.c.b.e), 111);
            }
        }
        super.onCreate(bundle);
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 4 && this.c.d != null && this.c.d.size() > 0) {
            Intent intent = new Intent(this, AdWallActivity.class);
            intent.putExtra("PUSH_INFO", this.c);
            startActivity(intent);
            finish();
        }
        return super.onKeyDown(i, keyEvent);
    }
}
