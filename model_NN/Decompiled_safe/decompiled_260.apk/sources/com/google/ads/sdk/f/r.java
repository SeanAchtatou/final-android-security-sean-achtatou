package com.google.ads.sdk.f;

import android.content.Context;
import android.content.SharedPreferences;
import com.google.ads.sdk.c.a;
import com.google.ads.sdk.d.a.f;
import com.google.ads.sdk.d.c;
import java.util.Date;
import java.util.HashMap;
import org.json.JSONObject;

public final class r {
    public static void a(Context context, int i, int i2) {
        HashMap hashMap = new HashMap();
        SharedPreferences sharedPreferences = context.getSharedPreferences("meiline_preference", 0);
        String string = sharedPreferences.getString("REGISTER_ID", null);
        String string2 = sharedPreferences.getString("PASSWORD", null);
        hashMap.put("APP_ID", a.a());
        hashMap.put("PACKAGE_NAME", a.c(context));
        hashMap.put("REGISTER_ID", string);
        hashMap.put("PASSWORD", string2);
        hashMap.put("AD_ID", Integer.valueOf(i));
        hashMap.put("USER_STEP", Integer.valueOf(i2));
        hashMap.put("REPORT_DATE", Long.valueOf(new Date().getTime()));
        c.a(new f(hashMap));
        e.b("Report Step", new JSONObject(hashMap).toString());
    }
}
