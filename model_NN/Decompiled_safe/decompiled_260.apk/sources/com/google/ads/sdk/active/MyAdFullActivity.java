package com.google.ads.sdk.active;

import android.app.Activity;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.google.ads.sdk.b.e;
import com.google.ads.sdk.f.k;
import java.io.IOException;

public class MyAdFullActivity extends Activity {
    /* access modifiers changed from: private */
    public static final String b = k.a(MyAdFullActivity.class);
    Handler a = new c(this);
    /* access modifiers changed from: private */
    public ImageView c;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        RelativeLayout relativeLayout = new RelativeLayout(this);
        setContentView(relativeLayout, new RelativeLayout.LayoutParams(-1, -1));
        ImageView imageView = new ImageView(this);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(11);
        imageView.setPadding(0, 10, 10, 0);
        try {
            imageView.setImageBitmap(BitmapFactory.decodeStream(e.a("shotcut")));
        } catch (IOException e) {
            com.google.ads.sdk.f.e.b(b, "decode back.png exception", e);
        }
        imageView.setOnClickListener(new e(this));
        this.c = new ImageView(this);
        this.c.setScaleType(ImageView.ScaleType.FIT_XY);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, -1);
        this.a.sendEmptyMessage(0);
        relativeLayout.addView(this.c, layoutParams2);
        relativeLayout.addView(imageView, layoutParams);
    }
}
