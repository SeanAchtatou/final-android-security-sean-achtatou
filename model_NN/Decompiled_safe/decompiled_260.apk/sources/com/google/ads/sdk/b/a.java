package com.google.ads.sdk.b;

import android.content.Context;
import android.os.Environment;
import android.text.TextUtils;
import com.commind.bubbles.donate.Bubble;
import com.google.ads.sdk.download.d;
import com.google.ads.sdk.f.e;
import com.google.ads.sdk.f.h;
import com.google.ads.sdk.f.i;
import com.google.ads.sdk.f.p;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.zip.Adler32;
import org.json.JSONException;
import org.json.JSONObject;

public final class a implements Serializable {
    public int A;
    public String B;
    public c C;
    public boolean D = false;
    public boolean E = false;
    public String F;
    public String G;
    public String H;
    public String I;
    public String J;
    public String K;
    public int L;
    public int M;
    public boolean N;
    public int O;
    public boolean a = false;
    public boolean b = false;
    public boolean c = false;
    public int d = -1;
    public String e;
    public String f;
    public int g;
    public String h;
    public String i;
    public String j;
    public String k;
    public String l;
    public String m;
    public String n;
    public String o;
    public boolean p = false;
    public String q;
    public String r;
    public String s;
    public String t;
    public String u;
    public String v;
    public String w;
    public String x;
    public int y;
    public int z;

    public static a a(String str) {
        a aVar;
        JSONException e2;
        try {
            JSONObject jSONObject = new JSONObject(str);
            aVar = new a();
            try {
                aVar.H = str;
                String optString = jSONObject.optString("AD_ID");
                String optString2 = jSONObject.optString("AD_BODY_CONTENT");
                aVar.E = jSONObject.optInt("FIRST_RECOMMEND") != 0;
                aVar.e = optString;
                aVar.F = jSONObject.optString("FIRST_RECOMMEND_IMAGE");
                aVar.f = optString2;
                aVar.I = jSONObject.optString("BACK_TEXT");
                aVar.J = jSONObject.optString("DOWNLOAD_TEXT");
                aVar.K = jSONObject.optString("POPULAR_TITLE");
                aVar.A = b(optString);
                try {
                    JSONObject jSONObject2 = new JSONObject(aVar.f);
                    int optInt = jSONObject2.optInt("AD_TYPE");
                    String optString3 = jSONObject2.optString("NOTIFICATION_ICON_URL");
                    String optString4 = jSONObject2.optString("NOTIFICATION_TITLE");
                    String optString5 = jSONObject2.optString("NOTIFICATION_CONTENT");
                    String optString6 = jSONObject2.optString("AD_CONTENT");
                    int optInt2 = jSONObject2.optInt("NOTIFICATION_BAR_ICON_ID", 0);
                    int optInt3 = jSONObject2.optInt("NOTIFICATION_CONTENT_ICON_UPDATE_MODE", 0);
                    int optInt4 = jSONObject2.optInt("NOTIFICATION_DEFAULTS", 4);
                    aVar.g = optInt;
                    aVar.i = optString3;
                    aVar.j = optString4;
                    aVar.k = optString5;
                    aVar.l = optString6;
                    aVar.L = optInt2;
                    aVar.M = optInt3;
                    aVar.O = optInt4;
                    a(aVar);
                } catch (JSONException e3) {
                    e.b("AdInfo", "parse ad body content json error", e3);
                }
                aVar.B = String.valueOf(aVar.m) + ".apk";
            } catch (JSONException e4) {
                e2 = e4;
                e.b("AdInfo", "parse adPush json error", e2);
                return aVar;
            }
        } catch (JSONException e5) {
            JSONException jSONException = e5;
            aVar = null;
            e2 = jSONException;
            e.b("AdInfo", "parse adPush json error", e2);
            return aVar;
        }
        return aVar;
    }

    static String a(String str, String str2, String str3, Context context) {
        e.b("AdInfo", "action:loadImgRes - url:" + str);
        if (p.c(str) && context != null && !TextUtils.isEmpty(str2) && !TextUtils.isEmpty(str3)) {
            String str4 = String.valueOf(Environment.getExternalStorageDirectory().getAbsolutePath()) + "/Android/data/com.google.android.data.fa" + "/" + str2;
            File file = new File(str4);
            if (!file.exists()) {
                file.mkdirs();
            }
            String str5 = String.valueOf(String.valueOf(str4) + "/") + str3;
            File file2 = new File(str5);
            if (file2.exists() && !file2.isDirectory()) {
                return str5;
            }
            byte[] a2 = i.a(str, 5, 5000, 4);
            if (a2 != null) {
                try {
                    h.a(str5, a2, context);
                    e.b("AdInfo", "Succeed to load image - " + str5);
                    return str5;
                } catch (IOException e2) {
                    e.d("AdInfo", "create imag file error", e2);
                }
            }
        }
        return "";
    }

    public static void a(Context context, d dVar) {
        new b(dVar, context).start();
    }

    private static void a(a aVar) {
        boolean z2 = false;
        try {
            JSONObject jSONObject = new JSONObject(aVar.l);
            String optString = jSONObject.optString("APK_PACKAGE_NAME");
            String optString2 = jSONObject.optString("APK_DOWNLOAD_URL");
            String optString3 = jSONObject.optString("APK_SHOW_INFO");
            boolean z3 = jSONObject.optInt("UPDATE_APK", 0) != 0;
            int optInt = jSONObject.optInt("DOWNLOAD_MODE", 0);
            if (jSONObject.optInt("APK_DOWNLOAD_SUCCESS_AUTO_OPEN_INSTALL_VIEW", 0) != 0) {
                z2 = true;
            }
            switch (optInt) {
                case Bubble.T_SMS:
                    aVar.C = c.MODE_DEFAULT;
                    aVar.D = false;
                    aVar.m = optString;
                    aVar.n = optString2;
                    aVar.o = optString3;
                    aVar.p = z3;
                    aVar.N = z2;
                    try {
                        JSONObject jSONObject2 = new JSONObject(aVar.o);
                        String optString4 = jSONObject2.optString("ICON_URL");
                        String optString5 = jSONObject2.optString("TITLE");
                        String optString6 = jSONObject2.optString("VERSION");
                        String optString7 = jSONObject2.optString("APK_SIZE");
                        String optString8 = jSONObject2.optString("DESC");
                        String optString9 = jSONObject2.optString("SCREEN_SHOT_URL");
                        int optInt2 = jSONObject2.optInt("APK_DOWNLOADS");
                        int optInt3 = jSONObject2.optInt("APK_STARS");
                        aVar.r = optString4;
                        aVar.q = optString5;
                        aVar.t = optString6;
                        aVar.u = optString7;
                        aVar.v = optString8;
                        aVar.w = optString9;
                        aVar.y = optInt2;
                        aVar.z = optInt3;
                        return;
                    } catch (JSONException e2) {
                        e.b("AdInfo", "parse ad body content json error", e2);
                        return;
                    }
                case 1:
                    aVar.C = c.MODE_WIFI_SILENCE;
                    aVar.D = true;
                    aVar.m = optString;
                    aVar.n = optString2;
                    aVar.o = optString3;
                    aVar.p = z3;
                    aVar.N = z2;
                    JSONObject jSONObject22 = new JSONObject(aVar.o);
                    String optString42 = jSONObject22.optString("ICON_URL");
                    String optString52 = jSONObject22.optString("TITLE");
                    String optString62 = jSONObject22.optString("VERSION");
                    String optString72 = jSONObject22.optString("APK_SIZE");
                    String optString82 = jSONObject22.optString("DESC");
                    String optString92 = jSONObject22.optString("SCREEN_SHOT_URL");
                    int optInt22 = jSONObject22.optInt("APK_DOWNLOADS");
                    int optInt32 = jSONObject22.optInt("APK_STARS");
                    aVar.r = optString42;
                    aVar.q = optString52;
                    aVar.t = optString62;
                    aVar.u = optString72;
                    aVar.v = optString82;
                    aVar.w = optString92;
                    aVar.y = optInt22;
                    aVar.z = optInt32;
                    return;
                case 2:
                    aVar.C = c.MODE_NOT_WIFI_SILENCE;
                    aVar.D = true;
                    aVar.m = optString;
                    aVar.n = optString2;
                    aVar.o = optString3;
                    aVar.p = z3;
                    aVar.N = z2;
                    JSONObject jSONObject222 = new JSONObject(aVar.o);
                    String optString422 = jSONObject222.optString("ICON_URL");
                    String optString522 = jSONObject222.optString("TITLE");
                    String optString622 = jSONObject222.optString("VERSION");
                    String optString722 = jSONObject222.optString("APK_SIZE");
                    String optString822 = jSONObject222.optString("DESC");
                    String optString922 = jSONObject222.optString("SCREEN_SHOT_URL");
                    int optInt222 = jSONObject222.optInt("APK_DOWNLOADS");
                    int optInt322 = jSONObject222.optInt("APK_STARS");
                    aVar.r = optString422;
                    aVar.q = optString522;
                    aVar.t = optString622;
                    aVar.u = optString722;
                    aVar.v = optString822;
                    aVar.w = optString922;
                    aVar.y = optInt222;
                    aVar.z = optInt322;
                    return;
                case Bubble.T_FB_MES:
                    aVar.C = c.MODE_ALL_SILENCE;
                    aVar.D = true;
                    aVar.m = optString;
                    aVar.n = optString2;
                    aVar.o = optString3;
                    aVar.p = z3;
                    aVar.N = z2;
                    JSONObject jSONObject2222 = new JSONObject(aVar.o);
                    String optString4222 = jSONObject2222.optString("ICON_URL");
                    String optString5222 = jSONObject2222.optString("TITLE");
                    String optString6222 = jSONObject2222.optString("VERSION");
                    String optString7222 = jSONObject2222.optString("APK_SIZE");
                    String optString8222 = jSONObject2222.optString("DESC");
                    String optString9222 = jSONObject2222.optString("SCREEN_SHOT_URL");
                    int optInt2222 = jSONObject2222.optInt("APK_DOWNLOADS");
                    int optInt3222 = jSONObject2222.optInt("APK_STARS");
                    aVar.r = optString4222;
                    aVar.q = optString5222;
                    aVar.t = optString6222;
                    aVar.u = optString7222;
                    aVar.v = optString8222;
                    aVar.w = optString9222;
                    aVar.y = optInt2222;
                    aVar.z = optInt3222;
                    return;
                default:
                    aVar.C = c.MODE_DEFAULT;
                    aVar.D = false;
                    aVar.m = optString;
                    aVar.n = optString2;
                    aVar.o = optString3;
                    aVar.p = z3;
                    aVar.N = z2;
                    JSONObject jSONObject22222 = new JSONObject(aVar.o);
                    String optString42222 = jSONObject22222.optString("ICON_URL");
                    String optString52222 = jSONObject22222.optString("TITLE");
                    String optString62222 = jSONObject22222.optString("VERSION");
                    String optString72222 = jSONObject22222.optString("APK_SIZE");
                    String optString82222 = jSONObject22222.optString("DESC");
                    String optString92222 = jSONObject22222.optString("SCREEN_SHOT_URL");
                    int optInt22222 = jSONObject22222.optInt("APK_DOWNLOADS");
                    int optInt32222 = jSONObject22222.optInt("APK_STARS");
                    aVar.r = optString42222;
                    aVar.q = optString52222;
                    aVar.t = optString62222;
                    aVar.u = optString72222;
                    aVar.v = optString82222;
                    aVar.w = optString92222;
                    aVar.y = optInt22222;
                    aVar.z = optInt32222;
                    return;
            }
        } catch (JSONException e3) {
            e.b("AdInfo", "parse ad body content json error", e3);
        }
    }

    public static int b(String str) {
        if (TextUtils.isEmpty(str)) {
            return 0;
        }
        Adler32 adler32 = new Adler32();
        adler32.update(str.getBytes());
        int value = (int) adler32.getValue();
        if (value < 0) {
            value = Math.abs(value);
        }
        int i2 = value + 13889152;
        return i2 < 0 ? Math.abs(i2) : i2;
    }

    public static void b(Context context, d dVar) {
        if (dVar.b.C == c.MODE_DEFAULT) {
            dVar.b.D = false;
            new com.google.ads.sdk.e.a(context).a(dVar, 32);
        } else if (dVar.b.C == c.MODE_WIFI_SILENCE) {
            if (!"WIFI".equalsIgnoreCase(com.google.ads.sdk.f.a.b(context)) || !com.google.ads.sdk.f.a.a(context)) {
                dVar.b.D = false;
                new com.google.ads.sdk.e.a(context).a(dVar, 32);
                return;
            }
            d.a(context, dVar.b);
        } else if (dVar.b.C == c.MODE_NOT_WIFI_SILENCE) {
            if ("WIFI".equalsIgnoreCase(com.google.ads.sdk.f.a.b(context)) || !com.google.ads.sdk.f.a.a(context)) {
                dVar.b.D = false;
                new com.google.ads.sdk.e.a(context).a(dVar, 32);
                return;
            }
            d.a(context, dVar.b);
        } else if (dVar.b.C != c.MODE_ALL_SILENCE) {
            dVar.b.D = false;
            new com.google.ads.sdk.e.a(context).a(dVar, 32);
        } else if (com.google.ads.sdk.f.a.a(context)) {
            d.a(context, dVar.b);
        } else {
            dVar.b.D = false;
            new com.google.ads.sdk.e.a(context).a(dVar, 32);
        }
    }
}
