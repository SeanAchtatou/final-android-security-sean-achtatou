package com.google.ads.sdk.d.b;

import com.flurry.android.Constants;
import com.google.ads.sdk.d.a;
import com.google.ads.sdk.d.b;
import com.google.ads.sdk.f.o;
import com.google.ads.sdk.f.p;

public abstract class e extends a {
    public int i;
    public String j;

    public e(byte[] bArr) {
        if (bArr != null) {
            this.g = bArr;
            com.google.ads.sdk.f.e.c("ResponseCommand", "responseData:" + p.a(bArr));
            this.e = new byte[13];
            if (bArr.length < this.e.length) {
                throw new b(b.a, "data length is lower than baisc header len - 6");
            }
            System.arraycopy(bArr, 0, this.e, 0, this.e.length);
            byte[] bArr2 = new byte[4];
            System.arraycopy(this.e, 0, bArr2, 0, bArr2.length);
            this.a = o.a(bArr2);
            this.b = this.e[4] & Constants.UNKNOWN;
            byte[] bArr3 = new byte[2];
            System.arraycopy(this.e, 5, bArr3, 0, bArr3.length);
            this.c = o.a(bArr3);
            byte[] bArr4 = new byte[4];
            System.arraycopy(this.e, 7, bArr4, 0, bArr4.length);
            this.d = o.a(bArr4);
            if (this.a > bArr.length) {
                throw new b(b.a, "header len should not be larger than actual data length - header len:" + this.a + ", data len:" + bArr.length);
            }
            byte[] bArr5 = new byte[2];
            System.arraycopy(bArr, 11, bArr5, 0, bArr5.length);
            this.i = o.a(bArr5);
            this.f = new byte[(bArr.length - 13)];
            System.arraycopy(bArr, 13, this.f, 0, this.f.length);
            if (this.i > 0) {
                this.j = o.a(this.f, 2, a(this.f, 0, 2));
                return;
            }
            try {
                a();
            } catch (ArrayIndexOutOfBoundsException e) {
                com.google.ads.sdk.f.e.b("ResponseCommand", "parseData ArrayIndexOutOfBoundsException", e);
                throw new b(b.d);
            }
        }
    }

    /* access modifiers changed from: protected */
    public abstract void a();
}
