package com.google.ads.sdk.download;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.google.ads.sdk.f.e;

final class b extends Handler {
    final /* synthetic */ a a;
    private c b = null;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public b(a aVar, Looper looper, c cVar) {
        super(looper);
        this.a = aVar;
        this.b = cVar;
    }

    public final void handleMessage(Message message) {
        super.handleMessage(message);
        if (this.a.a) {
            e.b("DownloadControl", "stop refresh download progress.");
            return;
        }
        if (this.b != null) {
            this.b.a(this.a.c, this.a.d);
        }
        this.a.b.sendEmptyMessageDelayed(0, 2000);
    }
}
