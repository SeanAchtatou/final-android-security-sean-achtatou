package com.google.ads.sdk.active;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.widget.Toast;

final class b extends Handler {
    final /* synthetic */ MyAdDownService a;
    private Context b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public b(MyAdDownService myAdDownService, Context context) {
        super(context.getMainLooper());
        this.a = myAdDownService;
        this.b = context;
    }

    public final void handleMessage(Message message) {
        super.handleMessage(message);
        Toast.makeText(this.b, "SDCard不可用", 1).show();
    }
}
