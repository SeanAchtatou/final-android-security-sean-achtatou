package com.google.ads.sdk.b;

import android.content.Context;
import android.content.Intent;
import com.google.ads.AdReceiver;
import com.google.ads.sdk.c.a;
import com.google.ads.sdk.f.p;
import com.google.ads.sdk.f.r;
import java.util.ArrayList;
import java.util.Iterator;

final class b extends Thread {
    int a = 0;
    private final /* synthetic */ d b;
    private final /* synthetic */ Context c;

    b(d dVar, Context context) {
        this.b = dVar;
        this.c = context;
    }

    public final void run() {
        while (true) {
            if (this.a >= 10) {
                break;
            }
            ArrayList arrayList = new ArrayList();
            arrayList.add(this.b.b);
            arrayList.addAll(this.b.d);
            boolean z = false;
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                a aVar = (a) it.next();
                if (p.c(aVar.r)) {
                    String e = a.e(this.c, aVar.r);
                    if (!p.a(e)) {
                        aVar.s = e;
                    } else {
                        aVar.s = a.a(aVar.r, aVar.e, "view_ic", this.c);
                        if (!p.a(aVar.s)) {
                            a.a(this.c, aVar.r, aVar.s);
                        }
                    }
                    if (p.a(aVar.s)) {
                        z = true;
                    }
                }
                if (p.c(aVar.w)) {
                    String e2 = a.e(this.c, aVar.w);
                    if (!p.a(e2)) {
                        aVar.x = e2;
                    } else {
                        aVar.x = a.a(aVar.w, aVar.e, "view_ig", this.c);
                        if (!p.a(aVar.x)) {
                            a.a(this.c, aVar.w, aVar.x);
                        }
                    }
                    if (p.a(aVar.x)) {
                        z = true;
                    }
                }
                if (aVar.E && p.c(aVar.F)) {
                    String e3 = a.e(this.c, aVar.F);
                    if (!p.a(e3)) {
                        aVar.G = e3;
                    } else {
                        aVar.G = a.a(aVar.F, aVar.e, "view_ig_Recommend", this.c);
                        if (!p.a(aVar.x)) {
                            a.a(this.c, aVar.F, aVar.G);
                        }
                    }
                    if (p.a(aVar.G)) {
                        z = true;
                    }
                }
            }
            if (!z) {
                break;
            } else if (this.a >= 9) {
                r.a(this.c, Integer.parseInt(this.b.b.e), 108);
                break;
            } else {
                try {
                    wait(1000);
                } catch (InterruptedException e4) {
                    e4.printStackTrace();
                }
                this.a++;
            }
        }
        Intent intent = new Intent(this.c, AdReceiver.class);
        intent.setAction(com.google.ads.sdk.f.a.d(this.c, "com.suyue168.android.sdk.RESOURCE_DOWNLOAD_SUCCESS_SHOW_NOTIFICATION"));
        intent.putExtra("PUSH_INFO", this.b);
        this.c.sendBroadcast(intent);
    }
}
