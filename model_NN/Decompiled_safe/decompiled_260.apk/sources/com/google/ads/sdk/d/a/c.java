package com.google.ads.sdk.d.a;

import java.io.ByteArrayOutputStream;
import java.util.Map;
import org.json.JSONObject;

public final class c extends e {
    private final Map i;

    public c(Map map) {
        super(1);
        this.i = map;
    }

    /* access modifiers changed from: protected */
    public final void a(ByteArrayOutputStream byteArrayOutputStream) {
        if (this.i != null && this.i.size() > 0) {
            a(byteArrayOutputStream, new JSONObject(this.i).toString());
        }
    }
}
