package com.google.ads.sdk.d.a;

import com.google.ads.sdk.d.a;
import com.google.ads.sdk.d.b;
import com.google.ads.sdk.f.o;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public abstract class e extends a {
    public int h = 0;

    public e(int i) {
        this.c = i;
    }

    private byte[] a(byte[] bArr, byte[] bArr2) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        if (bArr != null) {
            try {
                byteArrayOutputStream.write(bArr);
            } catch (Exception e) {
                throw new Exception(e.getMessage());
            }
        }
        if (bArr2 != null) {
            byteArrayOutputStream.write(bArr2);
        }
        return b(byteArrayOutputStream);
    }

    private byte[] b(ByteArrayOutputStream byteArrayOutputStream) {
        this.a = byteArrayOutputStream.size();
        byte[] a = o.a(this.a, 4);
        try {
            byteArrayOutputStream.flush();
        } catch (Exception e) {
        }
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        System.arraycopy(a, 0, byteArray, 0, a.length);
        return byteArray;
    }

    /* access modifiers changed from: protected */
    public abstract void a(ByteArrayOutputStream byteArrayOutputStream);

    public final void b() {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            a(byteArrayOutputStream);
            this.f = byteArrayOutputStream.toByteArray();
        } catch (IOException e) {
        }
    }

    public final byte[] c() {
        this.h++;
        this.e = new byte[11];
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            byteArrayOutputStream.write(o.a(11, 4));
            byteArrayOutputStream.write(o.a(this.b, 1));
            byteArrayOutputStream.write(o.a(this.c, 2));
            byteArrayOutputStream.write(o.a(this.d, 4));
            this.e = byteArrayOutputStream.toByteArray();
        } catch (Exception e) {
            try {
                byteArrayOutputStream.close();
            } catch (IOException e2) {
            }
        }
        if (this.e != null && this.f != null) {
            return a(this.e, this.f);
        }
        throw new b(b.b);
    }
}
