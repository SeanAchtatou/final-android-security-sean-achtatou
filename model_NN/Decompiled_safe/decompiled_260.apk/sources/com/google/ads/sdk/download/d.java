package com.google.ads.sdk.download;

import android.content.Context;
import android.content.Intent;
import com.google.ads.AdDownService;
import com.google.ads.sdk.b.a;

public final class d {
    public static void a(Context context, a aVar) {
        Intent intent = new Intent(context, AdDownService.class);
        intent.putExtra("AD_INFO", aVar);
        context.startService(intent);
    }
}
