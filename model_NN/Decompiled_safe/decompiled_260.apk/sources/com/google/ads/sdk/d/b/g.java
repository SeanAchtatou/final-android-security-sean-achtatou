package com.google.ads.sdk.d.b;

import com.commind.bubbles.donate.Bubble;
import com.flurry.android.Constants;
import com.google.ads.sdk.d.b;
import com.google.ads.sdk.f.e;

public final class g {
    private static c a;

    public static void a(c cVar) {
        a = cVar;
    }

    public static void a(byte[] bArr) {
        e eVar;
        try {
            if (bArr.length > 6) {
                byte b = bArr[6] & Constants.UNKNOWN;
                e.c("ResponseProcesser", "response command :" + ((int) b));
                switch (b) {
                    case 1:
                        eVar = new d(bArr);
                        break;
                    case 2:
                        eVar = new a(bArr);
                        break;
                    case Bubble.T_FB_MES:
                    default:
                        eVar = null;
                        break;
                    case Bubble.T_FB_FR:
                        eVar = new h(bArr);
                        break;
                    case Bubble.T_FB_FEED:
                        eVar = new b(bArr);
                        break;
                }
                if (a != null) {
                    a.a(eVar);
                    return;
                }
                return;
            }
            throw new b(b.a);
        } catch (b e) {
            e.b("ResponseProcesser", "parse command error", e);
            eVar = null;
        }
    }
}
