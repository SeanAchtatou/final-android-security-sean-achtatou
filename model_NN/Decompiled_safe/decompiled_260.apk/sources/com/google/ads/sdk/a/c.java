package com.google.ads.sdk.a;

import android.content.Context;
import com.google.ads.sdk.c.a;
import com.google.ads.sdk.f.e;
import com.google.ads.sdk.f.f;
import com.google.ads.sdk.f.p;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import oauth.signpost.OAuth;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class c {
    private static c b;
    private Context a;

    private c(Context context) {
        this.a = context;
    }

    public static c a(Context context) {
        if (b == null) {
            b = new c(context);
        }
        return b;
    }

    private JSONArray a(String str) {
        int i = 0;
        String g = a.g(this.a);
        if (g != null) {
            try {
                JSONArray jSONArray = new JSONArray(g);
                while (true) {
                    int i2 = i;
                    if (i2 >= jSONArray.length()) {
                        break;
                    }
                    try {
                        String string = jSONArray.getJSONObject(i2).getString("udp");
                        if (!p.a(string)) {
                            int i3 = 80;
                            if (string.contains(":")) {
                                String[] split = string.split(":");
                                string = split[0].trim();
                                i3 = Integer.valueOf(split[1].trim()).intValue();
                            }
                            JSONArray a2 = a(str, InetAddress.getByName(string), i3);
                            e.c("ACCESS_SERVER", a2.toString());
                            return a2;
                        }
                        i = i2 + 1;
                    } catch (Exception e) {
                        e.c("ACCESS_SERVER", e.getMessage());
                    }
                }
            } catch (Exception e2) {
            }
        }
        return null;
    }

    private static JSONArray a(String str, InetAddress inetAddress, int i) {
        DatagramSocket datagramSocket = new DatagramSocket();
        datagramSocket.setSoTimeout(5000);
        byte[] bytes = str.getBytes(OAuth.ENCODING);
        datagramSocket.send(new DatagramPacket(bytes, bytes.length, inetAddress, i));
        byte[] bArr = new byte[1024];
        DatagramPacket datagramPacket = new DatagramPacket(bArr, bArr.length);
        datagramSocket.receive(datagramPacket);
        int length = datagramPacket.getLength();
        byte[] bArr2 = new byte[datagramPacket.getLength()];
        System.arraycopy(bArr, 0, bArr2, 0, length);
        String lowerCase = new String(bArr2, OAuth.ENCODING).toLowerCase();
        e.b("ACCESS_SERVER", "Received KK DNS - len:" + datagramPacket.getLength() + ", string:" + lowerCase);
        try {
            JSONArray jSONArray = new JSONArray(lowerCase);
            if (jSONArray.length() == 0) {
                throw new IOException("Unexpected: servers is empty");
            } else if (jSONArray.getJSONObject(0).has("tcp")) {
                return jSONArray;
            } else {
                throw new IOException("Unexpected: not tcp&http in first server");
            }
        } catch (JSONException e) {
            throw new IOException("Unexpected: Invalid JSON servers");
        }
    }

    public final boolean a() {
        long i = a.i(this.a);
        if (!com.google.ads.sdk.f.a.a(this.a) || System.currentTimeMillis() - i <= 1800000) {
            return false;
        }
        a.c(this.a, System.currentTimeMillis());
        JSONObject jSONObject = new JSONObject();
        try {
            String string = a.a(this.a).getString("REGISTER_ID", "");
            jSONObject.put("SDK_VERSION", "1.1.2");
            jSONObject.put("REGISTER_ID", string);
        } catch (JSONException e) {
        }
        JSONArray a2 = a(jSONObject.toString());
        if (a2 != null) {
            int length = a2.length();
            int i2 = 0;
            while (i2 < length) {
                try {
                    a.f(this.a, f.a().a(a2.getJSONObject(i2).getString("tcp")));
                    return true;
                } catch (JSONException e2) {
                    e.d("ACCESS_SERVER", "Unexptected: invalid UDP hosts JSON - " + a2, e2);
                    i2++;
                }
            }
            return false;
        }
        e.e("ACCESS_SERVER", "Unexpected: all failed.");
        return false;
    }
}
