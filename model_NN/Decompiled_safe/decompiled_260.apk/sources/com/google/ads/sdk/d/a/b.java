package com.google.ads.sdk.d.a;

import com.google.ads.sdk.f.e;
import java.io.ByteArrayOutputStream;
import java.util.Map;
import org.json.JSONObject;

public final class b extends e {
    private final Map i;

    public b(Map map) {
        super(5);
        this.i = map;
    }

    public final String a() {
        if (this.i == null) {
            return "";
        }
        String optString = new JSONObject(this.i).optString("BATCH_USER_STEP");
        e.b("UserStepRequestCommand", optString);
        return optString;
    }

    /* access modifiers changed from: protected */
    public final void a(ByteArrayOutputStream byteArrayOutputStream) {
        if (this.i != null && this.i.size() > 0) {
            a(byteArrayOutputStream, new JSONObject(this.i).toString());
        }
    }
}
