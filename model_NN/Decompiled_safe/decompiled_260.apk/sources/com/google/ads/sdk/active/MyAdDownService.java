package com.google.ads.sdk.active;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import com.google.ads.AdDownService;
import com.google.ads.AdManager;
import com.google.ads.AdReceiver;
import com.google.ads.sdk.b.a;
import com.google.ads.sdk.download.d;
import com.google.ads.sdk.f.e;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;

public class MyAdDownService extends IntentService {
    public static ConcurrentLinkedQueue a = new ConcurrentLinkedQueue();
    private static Bundle b;
    /* access modifiers changed from: private */
    public NotificationManager c;
    /* access modifiers changed from: private */
    public a d;
    private b e;
    private Notification f;

    public MyAdDownService() {
        super("AdDownService");
    }

    public static void a(Context context) {
        e.c("AdDownService", "Execute old download task - size:" + a.size());
        ArrayList arrayList = new ArrayList();
        while (true) {
            a aVar = (a) a.poll();
            if (aVar == null) {
                break;
            } else if (aVar.b) {
                e.c("AdDownService", "Starting to download - adId:" + aVar.e);
                d.a(context, aVar);
            } else {
                e.b("AdDownService", "Downloading is still there.");
                arrayList.add(aVar);
            }
        }
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            a.offer((a) it.next());
        }
    }

    static /* synthetic */ void a(MyAdDownService myAdDownService, int i, a aVar, int i2) {
        String str;
        if (i2 != 0) {
            int i3 = 4;
            if (2 == i2) {
                str = "下载失败。请稍后点击重新下载！";
                i3 = 32;
            } else if (3 == i2) {
                str = "下载资源失效。请稍后点击重新下载！";
            } else if (1 == i2) {
                str = "当前网络不可用。稍后会继续下载！";
                i3 = 2;
            } else {
                return;
            }
            String str2 = myAdDownService.d.q;
            Intent intent = new Intent();
            if (com.google.ads.sdk.download.a.a(i2)) {
                intent.setClass(myAdDownService.getApplicationContext(), AdDownService.class);
                aVar.d = -1;
                intent.putExtra("AD_INFO", aVar);
            }
            Notification notification = new Notification();
            notification.icon = 17301634;
            notification.when = System.currentTimeMillis();
            notification.flags = i3;
            notification.setLatestEventInfo(myAdDownService.getApplicationContext(), str2, str, PendingIntent.getService(myAdDownService, i, intent, 134217728));
            myAdDownService.c.notify(i, notification);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.ads.sdk.e.a.a(com.google.ads.sdk.b.a, boolean):void
     arg types: [com.google.ads.sdk.b.a, int]
     candidates:
      com.google.ads.sdk.e.a.a(com.google.ads.sdk.b.d, int):void
      com.google.ads.sdk.e.a.a(com.google.ads.sdk.b.a, boolean):void */
    static /* synthetic */ void a(MyAdDownService myAdDownService, a aVar) {
        String str = aVar.h;
        if (!TextUtils.isEmpty(str)) {
            new com.google.ads.sdk.e.a(myAdDownService).a(aVar, false);
        } else {
            e.c("AdDownService", "No end notification. is filePath empty ? - " + str);
        }
    }

    static /* synthetic */ void a(MyAdDownService myAdDownService, a aVar, int i, long j, long j2) {
        if (myAdDownService.f == null) {
            myAdDownService.f = new Notification();
            myAdDownService.f.icon = 17301633;
            myAdDownService.f.when = System.currentTimeMillis();
            myAdDownService.f.flags = 2;
            myAdDownService.f.defaults = 4;
            myAdDownService.f.contentIntent = PendingIntent.getActivity(myAdDownService.getApplicationContext(), i, new Intent(), 134217728);
        }
        String str = aVar.q;
        String str2 = "下载中... ";
        int i2 = (int) ((((float) j) / ((float) j2)) * 100.0f);
        if (j2 > 0) {
            str2 = String.valueOf(str2) + i2 + "%";
        }
        myAdDownService.f.setLatestEventInfo(myAdDownService, str, str2, PendingIntent.getActivity(myAdDownService.getApplicationContext(), i, new Intent(), 134217728));
        myAdDownService.c.notify(i, myAdDownService.f);
    }

    public static void b(Context context) {
        if (a.size() > 0) {
            Intent intent = new Intent(context, AdReceiver.class);
            intent.setAction("com.suyue168.android.sdk.AUTO_DOWNLOAD_CHECK");
            PendingIntent broadcast = PendingIntent.getBroadcast(context, 0, intent, 0);
            Calendar instance = Calendar.getInstance();
            instance.setTimeInMillis(System.currentTimeMillis());
            if (AdManager.a) {
                instance.add(14, com.google.ads.sdk.f.d.d);
            } else {
                instance.add(14, com.google.ads.sdk.f.d.c);
            }
            AlarmManager alarmManager = (AlarmManager) context.getSystemService("alarm");
            alarmManager.cancel(broadcast);
            alarmManager.set(0, instance.getTimeInMillis(), broadcast);
            Log.i("AdDownService", String.format("download Task will start: %ds", 600));
        }
    }

    public void onCreate() {
        e.c("AdDownService", "onCreate()");
        super.onCreate();
        this.e = new b(this, getApplicationContext());
        this.c = (NotificationManager) getSystemService("notification");
        if (b == null) {
            b = new Bundle();
        }
    }

    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onHandleIntent(Intent intent) {
        e.c("AdDownService", "action:onHandleIntent");
        this.d = (a) intent.getSerializableExtra("AD_INFO");
        if (this.d == null) {
            e.d("AdDownService", "NULL ad entity");
        } else if (!Environment.getExternalStorageState().equals("mounted")) {
            e.d("AdDownService", "SDCard没有装载好");
            this.e.sendEmptyMessage(0);
        } else if (this.d.c) {
            e.c("AdDownService", "The AD download is already finished.");
        } else {
            if (!a.contains(this.d)) {
                a.offer(this.d);
            }
            int i = this.d.A;
            Thread.currentThread().setPriority(1);
            new com.google.ads.sdk.download.a(this, this.d, b, new a(this, i), 3000);
        }
    }
}
