package com.google.ads.sdk.a;

import com.google.ads.sdk.f.e;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;

public final class d extends a {
    protected static int a = 30000;
    private Socket b;

    public d(String str, int i) {
        try {
            InetSocketAddress inetSocketAddress = new InetSocketAddress(str, i);
            this.b = new Socket();
            this.b.setTcpNoDelay(true);
            this.b.connect(inetSocketAddress, a);
            this.b.setSoTimeout(a);
        } catch (Exception e) {
            e.e("TCP_CONN", e.getMessage());
            throw e;
        }
    }

    public final InputStream a() {
        return this.b.getInputStream();
    }

    public final OutputStream b() {
        return this.b.getOutputStream();
    }

    public final void c() {
        try {
            this.b.close();
        } catch (IOException e) {
        }
    }
}
