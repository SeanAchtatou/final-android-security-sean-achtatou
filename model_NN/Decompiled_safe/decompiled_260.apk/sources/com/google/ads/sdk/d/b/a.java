package com.google.ads.sdk.d.b;

import com.google.ads.sdk.f.e;
import com.google.ads.sdk.f.o;
import java.util.HashMap;
import java.util.Map;

public final class a extends e {
    public Map h;

    public a(byte[] bArr) {
        super(bArr);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        if (this.i == 0) {
            int a = a(this.f, 0, 2);
            String b = o.b(this.f, 2, a);
            e.c("AdPushResponseCommand", "zip length:" + a);
            if (b != null && !"".equals(b)) {
                this.h = new HashMap();
                this.h.put("PUSH_INFO", b);
            }
        }
    }
}
