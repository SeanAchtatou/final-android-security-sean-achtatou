package com.google.ads.sdk.e;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import com.google.ads.AdWallActivity;
import com.google.ads.sdk.b.e;
import com.google.ads.sdk.f.a;
import com.google.ads.sdk.f.p;
import com.google.ads.sdk.f.r;
import java.io.IOException;

final class c implements View.OnClickListener {
    final /* synthetic */ b a;

    c(b bVar) {
        this.a = bVar;
    }

    public final void onClick(View view) {
        try {
            Bitmap decodeStream = BitmapFactory.decodeStream(e.a("popular_icon"));
            Intent intent = new Intent();
            intent.setClassName(this.a.a.getPackageName(), AdWallActivity.class.getName());
            intent.setAction("android.intent.action.VIEW");
            intent.putExtra("PUSH_INFO_FOR_JSON", this.a.b.a);
            String str = this.a.b.b.K;
            if (p.a(str)) {
                str = "热门推荐";
            }
            a.a(this.a.a, str, intent);
            a.a(this.a.a, intent, str, decodeStream);
            r.a(this.a.a, Integer.parseInt(this.a.b.b.e), 118);
            ((AdWallActivity) this.a.a).finish();
        } catch (IOException e) {
            com.google.ads.sdk.f.e.d("PopularTodayView", "", e);
        }
    }
}
