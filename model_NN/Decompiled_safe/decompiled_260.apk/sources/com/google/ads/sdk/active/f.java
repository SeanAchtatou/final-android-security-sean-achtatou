package com.google.ads.sdk.active;

import android.content.Context;
import android.content.SharedPreferences;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import com.google.ads.sdk.d.b.g;
import com.google.ads.sdk.d.c;
import com.google.ads.sdk.f.a;
import com.google.ads.sdk.f.p;
import com.google.ads.sdk.f.s;
import java.util.HashMap;
import java.util.UUID;

public final class f {
    private SharedPreferences a;
    private String b;
    private String c;
    private Context d;

    public f(Context context) {
        this.d = context;
    }

    public static void a(Context context) {
        String str;
        SharedPreferences sharedPreferences;
        HashMap hashMap = new HashMap();
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        String deviceId = telephonyManager.getDeviceId();
        String subscriberId = telephonyManager.getSubscriberId();
        if (!a.b(deviceId)) {
            str = Settings.Secure.getString(context.getContentResolver(), "android_id");
            if (p.a(str) || "9774d56d682e549c".equals(str.toLowerCase())) {
                str = a.c(context);
                if (p.a(str) && (str = (sharedPreferences = context.getSharedPreferences("meiline_preference", 0)).getString("REGISTER_KEY", null)) == null) {
                    str = UUID.randomUUID().toString();
                    SharedPreferences.Editor edit = sharedPreferences.edit();
                    edit.putString("REGISTER_KEY", str);
                    edit.commit();
                }
            }
        } else {
            str = deviceId;
        }
        hashMap.put("REGISTER_KEY", String.valueOf(str == null ? " " : str) + "$$" + (subscriberId == null ? " " : subscriberId) + "$$");
        hashMap.put("PACKAGE_NAME", com.google.ads.sdk.c.a.c(context));
        hashMap.put("APP_ID", com.google.ads.sdk.c.a.a());
        hashMap.put("DEVICE_INFO", a.a("1.1.2"));
        hashMap.put("APP_CH", com.google.ads.sdk.c.a.b(context));
        c.a(new com.google.ads.sdk.d.a.c(hashMap));
    }

    public final void a() {
        g.a(new com.google.ads.sdk.d.b.f(this.d));
        Thread thread = new Thread(new c(this.d));
        thread.setName("requestThead");
        thread.start();
        if (this.a == null) {
            this.a = this.d.getSharedPreferences("meiline_preference", 0);
        }
        if (p.a(this.b) || p.a(this.c)) {
            this.b = this.a.getString("REGISTER_ID", null);
            this.c = this.a.getString("PASSWORD", null);
        }
        if ((p.a(this.b) || p.a(this.c)) && s.a(this.d)) {
            a(this.d);
        }
    }
}
