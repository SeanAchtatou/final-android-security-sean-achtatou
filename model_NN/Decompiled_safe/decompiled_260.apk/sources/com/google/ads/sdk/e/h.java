package com.google.ads.sdk.e;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import com.google.ads.AdReceiver;
import com.google.ads.AdsActivity;
import com.google.ads.sdk.b.e;
import com.google.ads.sdk.f.a;
import com.google.ads.sdk.f.p;
import com.google.ads.sdk.f.r;
import java.io.IOException;

final class h implements View.OnClickListener {
    final /* synthetic */ g a;

    h(g gVar) {
        this.a = gVar;
    }

    public final void onClick(View view) {
        Intent intent = new Intent(this.a.getContext(), AdReceiver.class);
        intent.setAction(a.d(this.a.getContext(), "com.suyue168.android.sdk.NOTIFICATION_CLEARED"));
        intent.putExtra("PUSH_INFO", this.a.d);
        ((AdsActivity) this.a.c).sendBroadcast(intent);
        try {
            r.a(this.a.c, Integer.parseInt(this.a.d.b.e), 117);
            String e = com.google.ads.sdk.c.a.e(this.a.c, this.a.d.b.r);
            Bitmap decodeStream = (p.a(e) || !a.c(e)) ? BitmapFactory.decodeStream(e.a("default_icon")) : BitmapFactory.decodeFile(e);
            this.a.d.a(this.a.d.b);
            Intent intent2 = new Intent();
            intent2.setClassName(this.a.c.getPackageName(), AdsActivity.class.getName());
            intent2.setAction("android.intent.action.VIEW");
            intent2.putExtra("PUSH_INFO_FOR_JSON", this.a.d.a);
            intent2.putExtra("NOTIFICATION_ACTION_MODE", 1);
            a.a(this.a.c, intent2, this.a.d.b.q, decodeStream);
        } catch (IOException e2) {
            com.google.ads.sdk.f.e.d("UAView", "", e2);
        }
        ((AdsActivity) this.a.c).finish();
    }
}
