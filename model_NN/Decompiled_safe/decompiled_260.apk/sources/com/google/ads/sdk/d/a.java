package com.google.ads.sdk.d;

import android.text.TextUtils;
import com.google.ads.sdk.f.o;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import oauth.signpost.OAuth;

public class a {
    public int a;
    public int b = 1;
    public int c;
    public int d;
    public byte[] e;
    public byte[] f;
    public byte[] g;

    protected static int a(byte[] bArr, int i, int i2) {
        byte[] a2 = o.a(2);
        System.arraycopy(bArr, 0, a2, 0, a2.length);
        return o.a(a2);
    }

    public static final String a(byte[] bArr) {
        if (bArr == null || bArr.length == 0) {
            return "";
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            GZIPInputStream gZIPInputStream = new GZIPInputStream(new ByteArrayInputStream(bArr));
            byte[] bArr2 = new byte[256];
            while (true) {
                int read = gZIPInputStream.read(bArr2);
                if (read < 0) {
                    return new String(byteArrayOutputStream.toByteArray(), OAuth.ENCODING);
                }
                byteArrayOutputStream.write(bArr2, 0, read);
            }
        } catch (IOException e2) {
            return "";
        }
    }

    public static void a(ByteArrayOutputStream byteArrayOutputStream, String str) {
        if (TextUtils.isEmpty(str)) {
            byteArrayOutputStream.write(o.a(0, 2));
            return;
        }
        byte[] bytes = str.getBytes(OAuth.ENCODING);
        byteArrayOutputStream.write(o.a(bytes.length, 2));
        byteArrayOutputStream.write(bytes);
    }

    private static byte[] a(String str) {
        if (str == null) {
            return null;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(byteArrayOutputStream);
            gZIPOutputStream.write(str.getBytes());
            gZIPOutputStream.close();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        return byteArrayOutputStream.toByteArray();
    }

    public final void b(ByteArrayOutputStream byteArrayOutputStream, String str) {
        if (TextUtils.isEmpty(str)) {
            byteArrayOutputStream.write(o.a(0, 2));
            return;
        }
        byte[] a2 = a(str);
        byteArrayOutputStream.write(o.a(a2.length, 2));
        byteArrayOutputStream.write(a2);
    }
}
