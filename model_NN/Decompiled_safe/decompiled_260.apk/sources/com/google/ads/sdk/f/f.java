package com.google.ads.sdk.f;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import oauth.signpost.OAuth;

public final class f {
    private static String c = "ENCRYPTER";
    private static f f;
    byte[] a = {-87, -101, -56, 50, 86, 53, -29, 3};
    int b = 1;
    private Cipher d = null;
    private Cipher e = null;

    private f(String str) {
        try {
            SecretKey generateSecret = SecretKeyFactory.getInstance("PBEWithMD5AndDES").generateSecret(new PBEKeySpec(str.toCharArray(), this.a, this.b));
            this.d = Cipher.getInstance(generateSecret.getAlgorithm());
            this.e = Cipher.getInstance(generateSecret.getAlgorithm());
            PBEParameterSpec pBEParameterSpec = new PBEParameterSpec(this.a, this.b);
            this.d.init(1, generateSecret, pBEParameterSpec);
            this.e.init(2, generateSecret, pBEParameterSpec);
        } catch (Exception e2) {
            e.e(c, "encrypt error cause by:" + e2);
        }
    }

    public static f a() {
        if (f == null) {
            f = new f(d.a);
        }
        return f;
    }

    public final String a(String str) {
        if (str != null) {
            try {
                if (!"".equals(str)) {
                    return b.a(this.d.doFinal(str.getBytes(OAuth.ENCODING)));
                }
            } catch (Exception e2) {
                e.e(c, "encrypt error cause by:" + e2);
            }
        }
        return "";
    }

    public final byte[] a(byte[] bArr) {
        return this.e.doFinal(bArr);
    }

    public final String b(String str) {
        if (str == null) {
            return str;
        }
        try {
            if ("".equals(str)) {
                return str;
            }
            return new String(this.e.doFinal(b.a(str)), "UTF8");
        } catch (Exception e2) {
            e.e(c, "decrypt error cause by:" + e2);
            return str;
        }
    }
}
