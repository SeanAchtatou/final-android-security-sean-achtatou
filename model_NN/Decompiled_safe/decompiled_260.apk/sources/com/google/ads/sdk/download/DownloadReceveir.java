package com.google.ads.sdk.download;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.google.ads.AdDownService;
import com.google.ads.sdk.f.a;
import com.google.ads.sdk.f.r;

public class DownloadReceveir extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action.equals("android.net.conn.CONNECTIVITY_CHANGE")) {
            if (a.a(context)) {
                AdDownService.a(context);
            }
        } else if (action.equals("android.intent.action.PACKAGE_ADDED")) {
            String c = com.google.ads.sdk.c.a.c(context, intent.getDataString().replace("package:", ""));
            if (!TextUtils.isEmpty(c)) {
                r.a(context, Integer.parseInt(c), 106);
            }
        }
    }
}
