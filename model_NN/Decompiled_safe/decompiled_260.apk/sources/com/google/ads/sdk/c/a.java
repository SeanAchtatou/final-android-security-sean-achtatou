package com.google.ads.sdk.c;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import com.google.ads.sdk.f.e;
import com.google.ads.sdk.f.f;
import com.google.ads.sdk.f.p;

public final class a {
    public static String a = null;
    private static String b = null;
    private static String c = "SET_PREFERENCE";
    private static String d = "startTime";
    private static String e = "last_push_server_info";
    private static String f = "access_server_info";

    public static SharedPreferences a(Context context) {
        return context.getSharedPreferences("meiline_preference", 0);
    }

    public static String a() {
        return "111";
    }

    public static void a(Context context, com.google.ads.sdk.b.a aVar) {
        k(context).putString(aVar.m, aVar.e).commit();
    }

    public static void a(Context context, Long l) {
        k(context).putLong("RTC_TYPE", l.longValue()).commit();
    }

    public static void a(Context context, String str) {
        if (!TextUtils.isEmpty(str)) {
            String d2 = d(context);
            if (TextUtils.isEmpty(d2)) {
                k(context).putString("REQUEST_FAIL_INFO", str).commit();
                return;
            }
            k(context).putString("REQUEST_FAIL_INFO", String.valueOf(d2) + "$$" + str).commit();
        }
    }

    public static void a(Context context, String str, String str2) {
        k(context).putString(str, str2).commit();
    }

    public static void a(Context context, String str, boolean z) {
        k(context).putBoolean(String.valueOf(str) + "@SUCCESS", z).commit();
    }

    public static boolean a(Context context, long j) {
        if (context != null) {
            return k(context).putLong(d, System.currentTimeMillis() + j).commit();
        }
        return false;
    }

    public static String b(Context context) {
        if (p.a(b)) {
            try {
                Bundle bundle = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData;
                if (bundle != null) {
                    b = String.valueOf(bundle.get("APP_CH"));
                }
            } catch (PackageManager.NameNotFoundException e2) {
            }
        }
        return b;
    }

    public static void b(Context context, String str) {
        k(context).putString(str, "").commit();
    }

    public static boolean b(Context context, long j) {
        if (context != null) {
            return k(context).putLong("APP_UPLOAD_TIME", j).commit();
        }
        return false;
    }

    public static String c(Context context) {
        return context.getPackageName();
    }

    public static String c(Context context, String str) {
        return context.getSharedPreferences("meiline_preference", 0).getString(str, "");
    }

    public static boolean c(Context context, long j) {
        if (context != null) {
            return k(context).putLong("ADDRESS_CHECK_TIME", j).commit();
        }
        return false;
    }

    public static String d(Context context) {
        String string = context.getSharedPreferences("meiline_preference", 0).getString("REQUEST_FAIL_INFO", "");
        if (!TextUtils.isEmpty(string)) {
            k(context).putString("REQUEST_FAIL_INFO", "").commit();
        }
        e.a("RequestFailInfo", string);
        return string;
    }

    public static boolean d(Context context, long j) {
        if (context != null) {
            return k(context).putLong("AD_CHECK_TIME", j).commit();
        }
        return false;
    }

    public static boolean d(Context context, String str) {
        return context.getSharedPreferences("meiline_preference", 0).getBoolean(String.valueOf(str) + "@SUCCESS", false);
    }

    public static long e(Context context) {
        long j = 0;
        if (context != null) {
            j = context.getSharedPreferences("meiline_preference", 0).getLong(d, 0);
        }
        e.c(String.valueOf(c) + "_" + d, "First time started on: " + j);
        return j;
    }

    public static String e(Context context, String str) {
        return context.getSharedPreferences("meiline_preference", 0).getString(str, "");
    }

    public static String f(Context context) {
        return a != null ? a : f.a().b(context.getSharedPreferences("meiline_preference", 0).getString(e, ""));
    }

    public static boolean f(Context context, String str) {
        if (context != null) {
            return k(context).putString(e, str).commit();
        }
        return false;
    }

    public static String g(Context context) {
        return f.a().b(context.getSharedPreferences("meiline_preference", 0).getString(f, ""));
    }

    public static boolean g(Context context, String str) {
        if (context != null) {
            return k(context).putString(f, str).commit();
        }
        return false;
    }

    public static long h(Context context) {
        long j = 0;
        if (context != null) {
            j = context.getSharedPreferences("meiline_preference", 0).getLong("APP_UPLOAD_TIME", 0);
        }
        e.c(String.valueOf(c) + "_APP_UPLOAD_TIME", "last app upload time at: " + j);
        return j;
    }

    public static long i(Context context) {
        long j = 0;
        if (context != null) {
            j = context.getSharedPreferences("meiline_preference", 0).getLong("ADDRESS_CHECK_TIME", 0);
        }
        e.c(String.valueOf(c) + "_ADDRESS_CHECK_TIME", "last udp checkout time at: " + j);
        return j;
    }

    public static long j(Context context) {
        long j = 0;
        if (context != null) {
            j = context.getSharedPreferences("meiline_preference", 0).getLong("AD_CHECK_TIME", 0);
        }
        e.c(String.valueOf(c) + "_AD_CHECK_TIME", "last ad check time at: " + j);
        return j;
    }

    private static SharedPreferences.Editor k(Context context) {
        return context.getSharedPreferences("meiline_preference", 0).edit();
    }
}
