package com.google.ads.sdk.f;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import com.commind.facebook.FacebookProvider;
import com.flurry.android.Constants;
import java.io.File;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;

public final class a {
    private static String[] a = {"/system/xbin/which", "su"};
    private static List b;

    static {
        ArrayList arrayList = new ArrayList();
        b = arrayList;
        arrayList.add("358673013795895");
        b.add("004999010640000");
        b.add("00000000000000");
        b.add("000000000000000");
    }

    public static String a() {
        String str = String.valueOf(Environment.getExternalStorageDirectory().getAbsolutePath()) + "/Download";
        File file = new File(str);
        if (!file.isDirectory()) {
            file.mkdirs();
        }
        return str;
    }

    public static String a(String str) {
        String str2 = String.valueOf(Build.VERSION.RELEASE) + "," + Integer.toString(Build.VERSION.SDK_INT);
        return String.valueOf(str2) + "$$" + Build.MODEL + "$$" + Build.DEVICE + "$$" + str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public static void a(Context context, Intent intent, String str, Bitmap bitmap) {
        Uri parse;
        ContentResolver contentResolver;
        Cursor query;
        if (e(context, str)) {
            e.b("AndroidUtil", "桌面图标已经存在 删除旧图标");
            String str2 = Build.VERSION.SDK_INT < 8 ? "content://com.android.launcher.settings/favorites?notify=true" : "content://com.android.launcher2.settings/favorites?notify=true";
            String[] strArr = {FacebookProvider.KEY_ID, FacebookProvider.KEY_TITLE, "iconResource"};
            if (!p.a(str) && (query = (contentResolver = context.getContentResolver()).query(parse, strArr, "title=?", new String[]{str}, null)) != null) {
                while (query.moveToNext()) {
                    try {
                        contentResolver.delete(parse, "_id=?", new String[]{String.valueOf(query.getInt(0))});
                    } catch (Exception e) {
                        e.a("AndroidUtil", "", e);
                    } finally {
                        query.close();
                    }
                }
                e.b("AndroidUtil", "del : " + (parse = Uri.parse(str2)).toString());
            }
        }
        Intent intent2 = new Intent("com.android.launcher.action.INSTALL_SHORTCUT");
        intent2.putExtra("duplicate", false);
        intent2.putExtra("android.intent.extra.shortcut.NAME", str);
        intent2.putExtra("android.intent.extra.shortcut.INTENT", intent);
        intent2.putExtra("android.intent.extra.shortcut.ICON", bitmap);
        context.sendBroadcast(intent2);
    }

    public static void a(Context context, String str) {
        Intent intent = new Intent();
        intent.addFlags(268435456);
        intent.setAction("android.intent.action.VIEW");
        intent.setDataAndType(Uri.fromFile(new File(str)), "application/vnd.android.package-archive");
        context.startActivity(intent);
    }

    public static void a(Context context, String str, Intent intent) {
        Intent intent2 = new Intent("com.android.launcher.action.UNINSTALL_SHORTCUT");
        intent2.putExtra("android.intent.extra.shortcut.NAME", str);
        intent2.putExtra("android.intent.extra.shortcut.INTENT", intent);
        context.sendBroadcast(intent2);
    }

    public static boolean a(Context context) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static String b(Context context) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        return activeNetworkInfo == null ? "" : activeNetworkInfo.getTypeName().toUpperCase();
    }

    public static boolean b() {
        return Environment.getExternalStorageState().equals("mounted");
    }

    public static boolean b(Context context, String str) {
        try {
            context.getPackageManager().getApplicationInfo(str, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public static boolean b(String str) {
        if (p.a(str) || str.length() < 10) {
            return false;
        }
        for (int i = 0; i < b.size(); i++) {
            if (str.equals(b.get(i))) {
                return false;
            }
        }
        return true;
    }

    public static String c(Context context) {
        try {
            String macAddress = ((WifiManager) context.getSystemService("wifi")).getConnectionInfo().getMacAddress();
            if (macAddress == null || macAddress.equals("")) {
                return null;
            }
            return d(String.valueOf(macAddress) + Build.MODEL);
        } catch (Exception e) {
            return null;
        }
    }

    public static boolean c(Context context, String str) {
        Intent launchIntentForPackage = context.getPackageManager().getLaunchIntentForPackage(str);
        if (launchIntentForPackage == null) {
            return false;
        }
        context.startActivity(launchIntentForPackage);
        return true;
    }

    public static boolean c(String str) {
        return new File(str).exists();
    }

    public static String d(Context context, String str) {
        return String.valueOf(context.getPackageName()) + "." + str;
    }

    private static String d(String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            char[] charArray = str.toCharArray();
            byte[] bArr = new byte[charArray.length];
            for (int i = 0; i < charArray.length; i++) {
                bArr[i] = (byte) charArray[i];
            }
            byte[] digest = instance.digest(bArr);
            StringBuffer stringBuffer = new StringBuffer();
            for (byte b2 : digest) {
                byte b3 = b2 & Constants.UNKNOWN;
                if (b3 < 16) {
                    stringBuffer.append("0");
                }
                stringBuffer.append(Integer.toHexString(b3));
            }
            return stringBuffer.toString();
        } catch (Exception e) {
            return "";
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x002f  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0043  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean e(android.content.Context r9, java.lang.String r10) {
        /*
            r8 = 1
            r7 = 0
            r6 = 0
            if (r9 != 0) goto L_0x0006
        L_0x0005:
            return r6
        L_0x0006:
            int r0 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x0037, all -> 0x0040 }
            r1 = 8
            if (r0 >= r1) goto L_0x0034
            java.lang.String r0 = "content://com.android.launcher.settings/favorites?notify=true"
        L_0x000e:
            android.net.Uri r1 = android.net.Uri.parse(r0)     // Catch:{ Exception -> 0x0037, all -> 0x0040 }
            android.content.ContentResolver r0 = r9.getContentResolver()     // Catch:{ Exception -> 0x0037, all -> 0x0040 }
            r2 = 0
            java.lang.String r3 = "title=?"
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x0037, all -> 0x0040 }
            r5 = 0
            r4[r5] = r10     // Catch:{ Exception -> 0x0037, all -> 0x0040 }
            r5 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x0037, all -> 0x0040 }
            if (r1 == 0) goto L_0x004f
            int r0 = r1.getCount()     // Catch:{ Exception -> 0x004a, all -> 0x0047 }
            if (r0 <= 0) goto L_0x004f
            r0 = r8
        L_0x002d:
            if (r1 == 0) goto L_0x0032
            r1.close()
        L_0x0032:
            r6 = r0
            goto L_0x0005
        L_0x0034:
            java.lang.String r0 = "content://com.android.launcher2.settings/favorites?notify=true"
            goto L_0x000e
        L_0x0037:
            r0 = move-exception
            r0 = r7
        L_0x0039:
            if (r0 == 0) goto L_0x004d
            r0.close()
            r0 = r6
            goto L_0x0032
        L_0x0040:
            r0 = move-exception
        L_0x0041:
            if (r7 == 0) goto L_0x0046
            r7.close()
        L_0x0046:
            throw r0
        L_0x0047:
            r0 = move-exception
            r7 = r1
            goto L_0x0041
        L_0x004a:
            r0 = move-exception
            r0 = r1
            goto L_0x0039
        L_0x004d:
            r0 = r6
            goto L_0x0032
        L_0x004f:
            r0 = r6
            goto L_0x002d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.ads.sdk.f.a.e(android.content.Context, java.lang.String):boolean");
    }
}
