package com.google.ads.sdk.e;

import android.content.Intent;
import android.view.View;
import com.google.ads.AdWallActivity;
import com.google.ads.AdsActivity;
import org.json.JSONObject;

final class e implements View.OnClickListener {
    final /* synthetic */ b a;

    e(b bVar) {
        this.a = bVar;
    }

    public final void onClick(View view) {
        f fVar = (f) view.getTag();
        if (fVar != null) {
            Intent intent = new Intent(this.a.a, AdsActivity.class);
            try {
                JSONObject jSONObject = new JSONObject(this.a.b.a);
                jSONObject.put("CURRENT_AD_INFO", fVar.h.H);
                this.a.b.a = jSONObject.toString();
                intent.putExtra("PUSH_INFO", this.a.b.a);
                intent.putExtra("NOTIFICATION_ACTION_MODE", 2);
                this.a.a.startActivity(intent);
            } catch (Exception e) {
                com.google.ads.sdk.f.e.d("PopularTodayView", "", e);
            }
            ((AdWallActivity) this.a.a).finish();
        }
    }
}
