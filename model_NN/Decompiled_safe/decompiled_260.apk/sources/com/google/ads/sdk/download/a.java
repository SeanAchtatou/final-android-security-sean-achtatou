package com.google.ads.sdk.download;

import android.content.Context;
import android.os.Bundle;
import com.google.ads.AdDownService;
import com.google.ads.sdk.d.b;
import com.google.ads.sdk.f.e;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import oauth.signpost.OAuth;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpProtocolParams;

public final class a {
    public boolean a = false;
    /* access modifiers changed from: private */
    public b b = null;
    /* access modifiers changed from: private */
    public long c = 0;
    /* access modifiers changed from: private */
    public long d = 0;
    private Bundle e;

    public a(Context context, com.google.ads.sdk.b.a aVar, Bundle bundle, c cVar, int i) {
        e.b("DownloadControl", "Create downloadControl");
        this.a = false;
        this.e = bundle;
        this.b = new b(this, context.getMainLooper(), cVar);
        this.b.sendEmptyMessageDelayed(0, 2000);
        if (aVar.d == -1) {
            aVar.d = 10;
        }
        while (com.google.ads.sdk.f.a.a(context)) {
            if (this.a) {
                e.a("DownloadControl", "Download is already stopped. Dont start again.");
                this.b.removeCallbacksAndMessages(null);
                cVar.a(1);
                return;
            } else if (aVar.d == 0) {
                e.d("DownloadControl", "try to connect too much. stop download now.");
                if (cVar != null) {
                    this.a = true;
                    AdDownService.a.remove(aVar);
                    this.b.removeCallbacksAndMessages(null);
                    cVar.a(2);
                    return;
                }
                return;
            } else {
                aVar.d--;
                if (a(context, cVar, aVar) == 1) {
                    e.c("DownloadControl", "Download succeed.");
                    this.b.removeCallbacksAndMessages(null);
                    this.a = true;
                    return;
                }
                try {
                    Thread.sleep(3000);
                    e.c("DownloadControl", "Connect time out, try rest - " + aVar.d);
                } catch (InterruptedException e2) {
                }
            }
        }
        e.a("DownloadControl", "Network is not available, dont download");
        this.b.removeCallbacksAndMessages(null);
        this.a = true;
        cVar.a(1);
    }

    private static int a(long j) {
        long j2 = j / 10485760;
        return (int) (((double) (j2 < 1 ? 10 : j2 > 5 ? 50 : (int) (j2 * 10))) * 1.1d);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    /* JADX WARNING: Code restructure failed: missing block: B:176:0x0363, code lost:
        r5.flush();
        com.google.ads.sdk.f.e.c("DownloadControl", "Download finished");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:177:0x0373, code lost:
        if (r11.length() != r13) goto L_0x03f0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:178:0x0375, code lost:
        r0.e.remove(r12);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:179:0x037c, code lost:
        if (r19 == null) goto L_0x0387;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:180:0x037e, code lost:
        r19.a(r11.getAbsolutePath());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:181:0x0387, code lost:
        a(r1, r10, r9, r5, r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:210:?, code lost:
        com.google.ads.sdk.f.e.d("DownloadControl", "The download file is not valid, download again");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:211:0x03fb, code lost:
        if (r11.delete() != false) goto L_0x0404;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:212:0x03fd, code lost:
        com.google.ads.sdk.f.e.e("DownloadControl", "delete file fail !!!");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:213:0x0404, code lost:
        a(r1, r10, r9, r5, r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:400:?, code lost:
        return 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:403:?, code lost:
        return -2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:418:?, code lost:
        return 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00f2, code lost:
        r5.flush();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:422:?, code lost:
        return -2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00fb, code lost:
        if (r13.length() != r14) goto L_0x0159;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00fd, code lost:
        r0.e.remove(r12);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0104, code lost:
        if (r19 == null) goto L_0x010f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0106, code lost:
        r19.a(r13.getAbsolutePath());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x010f, code lost:
        a(r11, r10, r6, r5, r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:?, code lost:
        com.google.ads.sdk.f.e.d("DownloadControl", "The download file is not valid, download again");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x0164, code lost:
        if (r13.delete() != false) goto L_0x016d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x0166, code lost:
        com.google.ads.sdk.f.e.e("DownloadControl", "delete file fail !!!");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x016d, code lost:
        a(r11, r10, r6, r5, r2);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int a(android.content.Context r18, com.google.ads.sdk.download.c r19, com.google.ads.sdk.b.a r20) {
        /*
            r17 = this;
            r0 = r20
            java.lang.String r12 = r0.n
            java.lang.String r1 = com.google.ads.sdk.f.a.a()
            r0 = r20
            java.lang.String r2 = r0.B
            boolean r3 = android.text.TextUtils.isEmpty(r12)
            if (r3 != 0) goto L_0x0516
            boolean r3 = android.text.TextUtils.isEmpty(r1)
            if (r3 != 0) goto L_0x0516
            boolean r3 = android.text.TextUtils.isEmpty(r2)
            if (r3 != 0) goto L_0x0516
            java.lang.String r3 = "DownloadControl"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r5 = "action:download - url:"
            r4.<init>(r5)
            java.lang.StringBuilder r4 = r4.append(r12)
            java.lang.String r5 = ", saveFilePath:"
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r4 = r4.append(r1)
            java.lang.String r5 = ", fileName:"
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r4 = r4.append(r2)
            java.lang.String r4 = r4.toString()
            com.google.ads.sdk.f.e.a(r3, r4)
            java.io.File r3 = new java.io.File
            r3.<init>(r1)
            boolean r4 = r3.isDirectory()
            if (r4 != 0) goto L_0x0054
            r3.mkdirs()
        L_0x0054:
            r0 = r17
            android.os.Bundle r3 = r0.e
            r4 = -1
            long r13 = r3.getLong(r12, r4)
            r3 = 0
            r5 = 0
            r9 = 0
            r8 = 0
            r7 = 0
            r6 = 0
            r10 = 0
            int r10 = (r13 > r10 ? 1 : (r13 == r10 ? 0 : -1))
            if (r10 > 0) goto L_0x02c9
            java.io.File r13 = new java.io.File
            r13.<init>(r1, r2)
            boolean r1 = r13.exists()
            if (r1 == 0) goto L_0x0080
            long r1 = r13.length()
            r10 = 0
            int r1 = (r1 > r10 ? 1 : (r1 == r10 ? 0 : -1))
            if (r1 > 0) goto L_0x022c
        L_0x0080:
            java.lang.String r1 = "DownloadControl"
            java.lang.String r2 = "Download first."
            com.google.ads.sdk.f.e.b(r1, r2)
            r1 = -1
            org.apache.http.client.methods.HttpGet r1 = a(r12, r1)
            org.apache.http.impl.client.DefaultHttpClient r2 = a()
            org.apache.http.HttpResponse r1 = r2.execute(r1)     // Catch:{ NumberFormatException -> 0x066d, ClientProtocolException -> 0x0658, IllegalStateException -> 0x01e5, FileNotFoundException -> 0x01f5, IOException -> 0x0205, b -> 0x0215, all -> 0x0225 }
            if (r1 == 0) goto L_0x01d3
            org.apache.http.StatusLine r2 = r1.getStatusLine()     // Catch:{ NumberFormatException -> 0x066d, ClientProtocolException -> 0x0658, IllegalStateException -> 0x01e5, FileNotFoundException -> 0x01f5, IOException -> 0x0205, b -> 0x0215, all -> 0x0225 }
            int r2 = r2.getStatusCode()     // Catch:{ NumberFormatException -> 0x066d, ClientProtocolException -> 0x0658, IllegalStateException -> 0x01e5, FileNotFoundException -> 0x01f5, IOException -> 0x0205, b -> 0x0215, all -> 0x0225 }
            r10 = 200(0xc8, float:2.8E-43)
            if (r2 != r10) goto L_0x0191
            org.apache.http.HttpEntity r2 = r1.getEntity()     // Catch:{ NumberFormatException -> 0x066d, ClientProtocolException -> 0x0658, IllegalStateException -> 0x01e5, FileNotFoundException -> 0x01f5, IOException -> 0x0205, b -> 0x0215, all -> 0x0225 }
            boolean r6 = r2.isStreaming()     // Catch:{ NumberFormatException -> 0x0675, ClientProtocolException -> 0x065d, IllegalStateException -> 0x0642, FileNotFoundException -> 0x062c, IOException -> 0x0616, b -> 0x0600, all -> 0x05e3 }
            if (r6 == 0) goto L_0x0181
            long r14 = a(r1)     // Catch:{ NumberFormatException -> 0x0675, ClientProtocolException -> 0x065d, IllegalStateException -> 0x0642, FileNotFoundException -> 0x062c, IOException -> 0x0616, b -> 0x0600, all -> 0x05e3 }
            r0 = r17
            android.os.Bundle r1 = r0.e     // Catch:{ NumberFormatException -> 0x0675, ClientProtocolException -> 0x065d, IllegalStateException -> 0x0642, FileNotFoundException -> 0x062c, IOException -> 0x0616, b -> 0x0600, all -> 0x05e3 }
            r1.putLong(r12, r14)     // Catch:{ NumberFormatException -> 0x0675, ClientProtocolException -> 0x065d, IllegalStateException -> 0x0642, FileNotFoundException -> 0x062c, IOException -> 0x0616, b -> 0x0600, all -> 0x05e3 }
            int r1 = a(r14)     // Catch:{ NumberFormatException -> 0x0675, ClientProtocolException -> 0x065d, IllegalStateException -> 0x0642, FileNotFoundException -> 0x062c, IOException -> 0x0616, b -> 0x0600, all -> 0x05e3 }
            r0 = r20
            r0.d = r1     // Catch:{ NumberFormatException -> 0x0675, ClientProtocolException -> 0x065d, IllegalStateException -> 0x0642, FileNotFoundException -> 0x062c, IOException -> 0x0616, b -> 0x0600, all -> 0x05e3 }
            r10 = 10
            int r1 = (r14 > r10 ? 1 : (r14 == r10 ? 0 : -1))
            if (r1 >= 0) goto L_0x00cc
            r1 = 10
            r0 = r20
            r0.d = r1     // Catch:{ NumberFormatException -> 0x0675, ClientProtocolException -> 0x065d, IllegalStateException -> 0x0642, FileNotFoundException -> 0x062c, IOException -> 0x0616, b -> 0x0600, all -> 0x05e3 }
        L_0x00cc:
            java.io.InputStream r11 = r2.getContent()     // Catch:{ NumberFormatException -> 0x0675, ClientProtocolException -> 0x065d, IllegalStateException -> 0x0642, FileNotFoundException -> 0x062c, IOException -> 0x0616, b -> 0x0600, all -> 0x05e3 }
            if (r11 == 0) goto L_0x0172
            java.io.BufferedInputStream r10 = new java.io.BufferedInputStream     // Catch:{ NumberFormatException -> 0x067c, ClientProtocolException -> 0x0661, IllegalStateException -> 0x0646, FileNotFoundException -> 0x0630, IOException -> 0x061a, b -> 0x0604 }
            r10.<init>(r11)     // Catch:{ NumberFormatException -> 0x067c, ClientProtocolException -> 0x0661, IllegalStateException -> 0x0646, FileNotFoundException -> 0x0630, IOException -> 0x061a, b -> 0x0604 }
            r13.delete()     // Catch:{ NumberFormatException -> 0x0683, ClientProtocolException -> 0x0664, IllegalStateException -> 0x0649, FileNotFoundException -> 0x0633, IOException -> 0x061d, b -> 0x0607, all -> 0x05ea }
            r13.createNewFile()     // Catch:{ NumberFormatException -> 0x0683, ClientProtocolException -> 0x0664, IllegalStateException -> 0x0649, FileNotFoundException -> 0x0633, IOException -> 0x061d, b -> 0x0607, all -> 0x05ea }
            java.io.FileOutputStream r6 = new java.io.FileOutputStream     // Catch:{ NumberFormatException -> 0x0683, ClientProtocolException -> 0x0664, IllegalStateException -> 0x0649, FileNotFoundException -> 0x0633, IOException -> 0x061d, b -> 0x0607, all -> 0x05ea }
            r6.<init>(r13)     // Catch:{ NumberFormatException -> 0x0683, ClientProtocolException -> 0x0664, IllegalStateException -> 0x0649, FileNotFoundException -> 0x0633, IOException -> 0x061d, b -> 0x0607, all -> 0x05ea }
            java.io.BufferedOutputStream r5 = new java.io.BufferedOutputStream     // Catch:{ NumberFormatException -> 0x068a, ClientProtocolException -> 0x0668, IllegalStateException -> 0x064d, FileNotFoundException -> 0x0637, IOException -> 0x0621, b -> 0x060b, all -> 0x05ee }
            r5.<init>(r6)     // Catch:{ NumberFormatException -> 0x068a, ClientProtocolException -> 0x0668, IllegalStateException -> 0x064d, FileNotFoundException -> 0x0637, IOException -> 0x0621, b -> 0x060b, all -> 0x05ee }
            r1 = 1024(0x400, float:1.435E-42)
            byte[] r1 = new byte[r1]     // Catch:{ NumberFormatException -> 0x0129, ClientProtocolException -> 0x0149, IllegalStateException -> 0x0652, FileNotFoundException -> 0x063c, IOException -> 0x0626, b -> 0x0610, all -> 0x05f3 }
        L_0x00eb:
            int r7 = r10.read(r1)     // Catch:{ NumberFormatException -> 0x0129, ClientProtocolException -> 0x0149, IllegalStateException -> 0x0652, FileNotFoundException -> 0x063c, IOException -> 0x0626, b -> 0x0610, all -> 0x05f3 }
            r8 = -1
            if (r7 != r8) goto L_0x0114
            r5.flush()     // Catch:{ NumberFormatException -> 0x0129, ClientProtocolException -> 0x0149, IllegalStateException -> 0x0652, FileNotFoundException -> 0x063c, IOException -> 0x0626, b -> 0x0610, all -> 0x05f3 }
            long r3 = r13.length()     // Catch:{ NumberFormatException -> 0x0129, ClientProtocolException -> 0x0149, IllegalStateException -> 0x0652, FileNotFoundException -> 0x063c, IOException -> 0x0626, b -> 0x0610, all -> 0x05f3 }
            int r1 = (r3 > r14 ? 1 : (r3 == r14 ? 0 : -1))
            if (r1 != 0) goto L_0x0159
            r0 = r17
            android.os.Bundle r1 = r0.e     // Catch:{ NumberFormatException -> 0x0129, ClientProtocolException -> 0x0149, IllegalStateException -> 0x0652, FileNotFoundException -> 0x063c, IOException -> 0x0626, b -> 0x0610, all -> 0x05f3 }
            r1.remove(r12)     // Catch:{ NumberFormatException -> 0x0129, ClientProtocolException -> 0x0149, IllegalStateException -> 0x0652, FileNotFoundException -> 0x063c, IOException -> 0x0626, b -> 0x0610, all -> 0x05f3 }
            if (r19 == 0) goto L_0x010f
            java.lang.String r1 = r13.getAbsolutePath()     // Catch:{ NumberFormatException -> 0x0129, ClientProtocolException -> 0x0149, IllegalStateException -> 0x0652, FileNotFoundException -> 0x063c, IOException -> 0x0626, b -> 0x0610, all -> 0x05f3 }
            r0 = r19
            r0.a(r1)     // Catch:{ NumberFormatException -> 0x0129, ClientProtocolException -> 0x0149, IllegalStateException -> 0x0652, FileNotFoundException -> 0x063c, IOException -> 0x0626, b -> 0x0610, all -> 0x05f3 }
        L_0x010f:
            a(r11, r10, r6, r5, r2)
            r1 = 1
        L_0x0113:
            return r1
        L_0x0114:
            r0 = r17
            boolean r8 = r0.a     // Catch:{ NumberFormatException -> 0x0129, ClientProtocolException -> 0x0149, IllegalStateException -> 0x0652, FileNotFoundException -> 0x063c, IOException -> 0x0626, b -> 0x0610, all -> 0x05f3 }
            if (r8 == 0) goto L_0x013a
            java.lang.String r1 = "DownloadControl"
            java.lang.String r3 = "stop download by user, throw UAException."
            com.google.ads.sdk.f.e.d(r1, r3)     // Catch:{ NumberFormatException -> 0x0129, ClientProtocolException -> 0x0149, IllegalStateException -> 0x0652, FileNotFoundException -> 0x063c, IOException -> 0x0626, b -> 0x0610, all -> 0x05f3 }
            com.google.ads.sdk.d.b r1 = new com.google.ads.sdk.d.b     // Catch:{ NumberFormatException -> 0x0129, ClientProtocolException -> 0x0149, IllegalStateException -> 0x0652, FileNotFoundException -> 0x063c, IOException -> 0x0626, b -> 0x0610, all -> 0x05f3 }
            java.lang.String r3 = "stop download by user."
            r1.<init>(r3)     // Catch:{ NumberFormatException -> 0x0129, ClientProtocolException -> 0x0149, IllegalStateException -> 0x0652, FileNotFoundException -> 0x063c, IOException -> 0x0626, b -> 0x0610, all -> 0x05f3 }
            throw r1     // Catch:{ NumberFormatException -> 0x0129, ClientProtocolException -> 0x0149, IllegalStateException -> 0x0652, FileNotFoundException -> 0x063c, IOException -> 0x0626, b -> 0x0610, all -> 0x05f3 }
        L_0x0129:
            r1 = move-exception
            r3 = r5
            r4 = r6
            r5 = r10
            r6 = r11
        L_0x012e:
            java.lang.String r7 = "DownloadControl"
            java.lang.String r8 = "NumberFormatException, get content length from http fail."
            com.google.ads.sdk.f.e.d(r7, r8, r1)     // Catch:{ all -> 0x05f9 }
            a(r6, r5, r4, r3, r2)
            r1 = -2
            goto L_0x0113
        L_0x013a:
            r8 = 0
            r5.write(r1, r8, r7)     // Catch:{ NumberFormatException -> 0x0129, ClientProtocolException -> 0x0149, IllegalStateException -> 0x0652, FileNotFoundException -> 0x063c, IOException -> 0x0626, b -> 0x0610, all -> 0x05f3 }
            long r7 = (long) r7     // Catch:{ NumberFormatException -> 0x0129, ClientProtocolException -> 0x0149, IllegalStateException -> 0x0652, FileNotFoundException -> 0x063c, IOException -> 0x0626, b -> 0x0610, all -> 0x05f3 }
            long r3 = r3 + r7
            r0 = r17
            r0.c = r3     // Catch:{ NumberFormatException -> 0x0129, ClientProtocolException -> 0x0149, IllegalStateException -> 0x0652, FileNotFoundException -> 0x063c, IOException -> 0x0626, b -> 0x0610, all -> 0x05f3 }
            r0 = r17
            r0.d = r14     // Catch:{ NumberFormatException -> 0x0129, ClientProtocolException -> 0x0149, IllegalStateException -> 0x0652, FileNotFoundException -> 0x063c, IOException -> 0x0626, b -> 0x0610, all -> 0x05f3 }
            goto L_0x00eb
        L_0x0149:
            r1 = move-exception
            r7 = r5
            r8 = r6
            r9 = r10
        L_0x014d:
            java.lang.String r3 = "DownloadControl"
            java.lang.String r4 = ""
            com.google.ads.sdk.f.e.d(r3, r4, r1)     // Catch:{ all -> 0x05e7 }
            a(r11, r9, r8, r7, r2)
            r1 = -2
            goto L_0x0113
        L_0x0159:
            java.lang.String r1 = "DownloadControl"
            java.lang.String r3 = "The download file is not valid, download again"
            com.google.ads.sdk.f.e.d(r1, r3)     // Catch:{ NumberFormatException -> 0x0129, ClientProtocolException -> 0x0149, IllegalStateException -> 0x0652, FileNotFoundException -> 0x063c, IOException -> 0x0626, b -> 0x0610, all -> 0x05f3 }
            boolean r1 = r13.delete()     // Catch:{ NumberFormatException -> 0x0129, ClientProtocolException -> 0x0149, IllegalStateException -> 0x0652, FileNotFoundException -> 0x063c, IOException -> 0x0626, b -> 0x0610, all -> 0x05f3 }
            if (r1 != 0) goto L_0x016d
            java.lang.String r1 = "DownloadControl"
            java.lang.String r3 = "delete file fail !!!"
            com.google.ads.sdk.f.e.e(r1, r3)     // Catch:{ NumberFormatException -> 0x0129, ClientProtocolException -> 0x0149, IllegalStateException -> 0x0652, FileNotFoundException -> 0x063c, IOException -> 0x0626, b -> 0x0610, all -> 0x05f3 }
        L_0x016d:
            a(r11, r10, r6, r5, r2)
            r1 = -2
            goto L_0x0113
        L_0x0172:
            java.lang.String r1 = "DownloadControl"
            java.lang.String r3 = "NULL response stream."
            com.google.ads.sdk.f.e.d(r1, r3)     // Catch:{ NumberFormatException -> 0x067c, ClientProtocolException -> 0x0661, IllegalStateException -> 0x0646, FileNotFoundException -> 0x0630, IOException -> 0x061a, b -> 0x0604 }
            r1 = 0
            r3 = 0
            r4 = 0
            a(r11, r1, r3, r4, r2)
            r1 = 0
            goto L_0x0113
        L_0x0181:
            java.lang.String r1 = "DownloadControl"
            java.lang.String r3 = "data mode from server is not stream."
            com.google.ads.sdk.f.e.e(r1, r3)     // Catch:{ NumberFormatException -> 0x0675, ClientProtocolException -> 0x065d, IllegalStateException -> 0x0642, FileNotFoundException -> 0x062c, IOException -> 0x0616, b -> 0x0600, all -> 0x05e3 }
            r1 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            a(r1, r3, r4, r5, r2)
            r1 = -2
            goto L_0x0113
        L_0x0191:
            r1 = 404(0x194, float:5.66E-43)
            if (r2 != r1) goto L_0x01b4
            java.lang.String r1 = "DownloadControl"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ NumberFormatException -> 0x066d, ClientProtocolException -> 0x0658, IllegalStateException -> 0x01e5, FileNotFoundException -> 0x01f5, IOException -> 0x0205, b -> 0x0215, all -> 0x0225 }
            java.lang.String r3 = "The resource does not exist - "
            r2.<init>(r3)     // Catch:{ NumberFormatException -> 0x066d, ClientProtocolException -> 0x0658, IllegalStateException -> 0x01e5, FileNotFoundException -> 0x01f5, IOException -> 0x0205, b -> 0x0215, all -> 0x0225 }
            java.lang.StringBuilder r2 = r2.append(r12)     // Catch:{ NumberFormatException -> 0x066d, ClientProtocolException -> 0x0658, IllegalStateException -> 0x01e5, FileNotFoundException -> 0x01f5, IOException -> 0x0205, b -> 0x0215, all -> 0x0225 }
            java.lang.String r2 = r2.toString()     // Catch:{ NumberFormatException -> 0x066d, ClientProtocolException -> 0x0658, IllegalStateException -> 0x01e5, FileNotFoundException -> 0x01f5, IOException -> 0x0205, b -> 0x0215, all -> 0x0225 }
            com.google.ads.sdk.f.e.c(r1, r2)     // Catch:{ NumberFormatException -> 0x066d, ClientProtocolException -> 0x0658, IllegalStateException -> 0x01e5, FileNotFoundException -> 0x01f5, IOException -> 0x0205, b -> 0x0215, all -> 0x0225 }
            r1 = 0
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            a(r1, r2, r3, r4, r5)
            r1 = -3
            goto L_0x0113
        L_0x01b4:
            java.lang.String r1 = "DownloadControl"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ NumberFormatException -> 0x066d, ClientProtocolException -> 0x0658, IllegalStateException -> 0x01e5, FileNotFoundException -> 0x01f5, IOException -> 0x0205, b -> 0x0215, all -> 0x0225 }
            java.lang.String r4 = "network connect status code unexpected - "
            r3.<init>(r4)     // Catch:{ NumberFormatException -> 0x066d, ClientProtocolException -> 0x0658, IllegalStateException -> 0x01e5, FileNotFoundException -> 0x01f5, IOException -> 0x0205, b -> 0x0215, all -> 0x0225 }
            java.lang.StringBuilder r2 = r3.append(r2)     // Catch:{ NumberFormatException -> 0x066d, ClientProtocolException -> 0x0658, IllegalStateException -> 0x01e5, FileNotFoundException -> 0x01f5, IOException -> 0x0205, b -> 0x0215, all -> 0x0225 }
            java.lang.String r2 = r2.toString()     // Catch:{ NumberFormatException -> 0x066d, ClientProtocolException -> 0x0658, IllegalStateException -> 0x01e5, FileNotFoundException -> 0x01f5, IOException -> 0x0205, b -> 0x0215, all -> 0x0225 }
            com.google.ads.sdk.f.e.d(r1, r2)     // Catch:{ NumberFormatException -> 0x066d, ClientProtocolException -> 0x0658, IllegalStateException -> 0x01e5, FileNotFoundException -> 0x01f5, IOException -> 0x0205, b -> 0x0215, all -> 0x0225 }
            r1 = 0
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            a(r1, r2, r3, r4, r5)
            r1 = -2
            goto L_0x0113
        L_0x01d3:
            java.lang.String r1 = "DownloadControl"
            java.lang.String r2 = "NULL response"
            com.google.ads.sdk.f.e.d(r1, r2)     // Catch:{ NumberFormatException -> 0x066d, ClientProtocolException -> 0x0658, IllegalStateException -> 0x01e5, FileNotFoundException -> 0x01f5, IOException -> 0x0205, b -> 0x0215, all -> 0x0225 }
            r1 = 0
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            a(r1, r2, r3, r4, r5)
            r1 = 0
            goto L_0x0113
        L_0x01e5:
            r1 = move-exception
            r2 = r6
            r11 = r5
        L_0x01e8:
            java.lang.String r3 = "DownloadControl"
            java.lang.String r4 = ""
            com.google.ads.sdk.f.e.d(r3, r4, r1)     // Catch:{ all -> 0x05e7 }
            a(r11, r9, r8, r7, r2)
            r1 = -2
            goto L_0x0113
        L_0x01f5:
            r1 = move-exception
            r2 = r6
            r11 = r5
        L_0x01f8:
            java.lang.String r3 = "DownloadControl"
            java.lang.String r4 = ""
            com.google.ads.sdk.f.e.d(r3, r4, r1)     // Catch:{ all -> 0x05e7 }
            a(r11, r9, r8, r7, r2)
            r1 = -2
            goto L_0x0113
        L_0x0205:
            r1 = move-exception
            r2 = r6
            r11 = r5
        L_0x0208:
            java.lang.String r3 = "DownloadControl"
            java.lang.String r4 = ""
            com.google.ads.sdk.f.e.b(r3, r4, r1)     // Catch:{ all -> 0x05e7 }
            a(r11, r9, r8, r7, r2)
            r1 = -1
            goto L_0x0113
        L_0x0215:
            r1 = move-exception
            r2 = r6
            r11 = r5
        L_0x0218:
            java.lang.String r3 = "DownloadControl"
            java.lang.String r4 = "UAException"
            com.google.ads.sdk.f.e.c(r3, r4, r1)     // Catch:{ all -> 0x05e7 }
            a(r11, r9, r8, r7, r2)
            r1 = -2
            goto L_0x0113
        L_0x0225:
            r1 = move-exception
            r2 = r6
            r11 = r5
        L_0x0228:
            a(r11, r9, r8, r7, r2)
            throw r1
        L_0x022c:
            long r1 = r13.length()
            r3 = 0
            int r1 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r1 <= 0) goto L_0x02bf
            java.lang.String r1 = "DownloadControl"
            java.lang.String r2 = "No info and File had been exsit."
            com.google.ads.sdk.f.e.b(r1, r2)
            r1 = -1
            org.apache.http.client.methods.HttpGet r1 = a(r12, r1)
            org.apache.http.impl.client.DefaultHttpClient r2 = a()
            org.apache.http.HttpResponse r1 = r2.execute(r1)     // Catch:{ ClientProtocolException -> 0x029e, IOException -> 0x02a9, b -> 0x02b4 }
            long r1 = a(r1)     // Catch:{ ClientProtocolException -> 0x029e, IOException -> 0x02a9, b -> 0x02b4 }
            long r3 = r13.length()     // Catch:{ ClientProtocolException -> 0x029e, IOException -> 0x02a9, b -> 0x02b4 }
            int r3 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r3 != 0) goto L_0x026c
            java.lang.String r1 = "DownloadControl"
            java.lang.String r2 = "Existed file size is same with target. Use it directly."
            com.google.ads.sdk.f.e.c(r1, r2)     // Catch:{ ClientProtocolException -> 0x029e, IOException -> 0x02a9, b -> 0x02b4 }
            if (r19 == 0) goto L_0x0269
            java.lang.String r1 = r13.getAbsolutePath()     // Catch:{ ClientProtocolException -> 0x029e, IOException -> 0x02a9, b -> 0x02b4 }
            r0 = r19
            r0.a(r1)     // Catch:{ ClientProtocolException -> 0x029e, IOException -> 0x02a9, b -> 0x02b4 }
        L_0x0269:
            r1 = 1
            goto L_0x0113
        L_0x026c:
            java.lang.String r3 = "DownloadControl"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ ClientProtocolException -> 0x029e, IOException -> 0x02a9, b -> 0x02b4 }
            java.lang.String r5 = "Exsit file length:"
            r4.<init>(r5)     // Catch:{ ClientProtocolException -> 0x029e, IOException -> 0x02a9, b -> 0x02b4 }
            long r5 = r13.length()     // Catch:{ ClientProtocolException -> 0x029e, IOException -> 0x02a9, b -> 0x02b4 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ ClientProtocolException -> 0x029e, IOException -> 0x02a9, b -> 0x02b4 }
            java.lang.String r5 = ", fileTotalLength:"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ ClientProtocolException -> 0x029e, IOException -> 0x02a9, b -> 0x02b4 }
            java.lang.StringBuilder r1 = r4.append(r1)     // Catch:{ ClientProtocolException -> 0x029e, IOException -> 0x02a9, b -> 0x02b4 }
            java.lang.String r1 = r1.toString()     // Catch:{ ClientProtocolException -> 0x029e, IOException -> 0x02a9, b -> 0x02b4 }
            com.google.ads.sdk.f.e.a(r3, r1)     // Catch:{ ClientProtocolException -> 0x029e, IOException -> 0x02a9, b -> 0x02b4 }
            boolean r1 = r13.delete()     // Catch:{ ClientProtocolException -> 0x029e, IOException -> 0x02a9, b -> 0x02b4 }
            if (r1 != 0) goto L_0x029b
            java.lang.String r1 = "DownloadControl"
            java.lang.String r2 = "delete file fail !!!"
            com.google.ads.sdk.f.e.e(r1, r2)     // Catch:{ ClientProtocolException -> 0x029e, IOException -> 0x02a9, b -> 0x02b4 }
        L_0x029b:
            r1 = -2
            goto L_0x0113
        L_0x029e:
            r1 = move-exception
            java.lang.String r2 = "DownloadControl"
            java.lang.String r3 = "ClientProtocolException"
            com.google.ads.sdk.f.e.d(r2, r3, r1)
            r1 = -2
            goto L_0x0113
        L_0x02a9:
            r1 = move-exception
            java.lang.String r2 = "DownloadControl"
            java.lang.String r3 = "IOException"
            com.google.ads.sdk.f.e.b(r2, r3, r1)
            r1 = -1
            goto L_0x0113
        L_0x02b4:
            r1 = move-exception
            java.lang.String r2 = "DownloadControl"
            java.lang.String r3 = "UAException"
            com.google.ads.sdk.f.e.c(r2, r3, r1)
            r1 = -2
            goto L_0x0113
        L_0x02bf:
            java.lang.String r1 = "DownloadControl"
            java.lang.String r2 = "unexpected !!"
            com.google.ads.sdk.f.e.e(r1, r2)
            r1 = -2
            goto L_0x0113
        L_0x02c9:
            java.lang.String r10 = "DownloadControl"
            java.lang.String r11 = "Had record, keep download."
            com.google.ads.sdk.f.e.a(r10, r11)
            java.io.File r11 = new java.io.File
            r11.<init>(r1, r2)
            boolean r1 = r11.exists()
            if (r1 == 0) goto L_0x038d
            java.lang.String r1 = "DownloadControl"
            java.lang.String r2 = "File exsit, getting the file length."
            com.google.ads.sdk.f.e.b(r1, r2)
            long r1 = r11.length()
            r3 = r1
        L_0x02e7:
            java.lang.String r10 = "DownloadControl"
            java.lang.StringBuilder r15 = new java.lang.StringBuilder
            java.lang.String r16 = "startPostion: "
            r15.<init>(r16)
            java.lang.StringBuilder r15 = r15.append(r1)
            java.lang.String r15 = r15.toString()
            com.google.ads.sdk.f.e.a(r10, r15)
            r0 = r20
            int r10 = r0.d
            r15 = -1
            if (r10 != r15) goto L_0x0311
            java.lang.String r10 = "DownloadControl"
            java.lang.String r15 = "Reset download retry times because it ever failed."
            com.google.ads.sdk.f.e.c(r10, r15)
            int r10 = a(r13)
            r0 = r20
            r0.d = r10
        L_0x0311:
            org.apache.http.client.methods.HttpGet r1 = a(r12, r1)
            org.apache.http.impl.client.DefaultHttpClient r2 = a()
            org.apache.http.HttpResponse r1 = r2.execute(r1)     // Catch:{ NumberFormatException -> 0x05ce, ClientProtocolException -> 0x05b9, IllegalStateException -> 0x04d9, FileNotFoundException -> 0x04e7, IOException -> 0x04f5, b -> 0x0503 }
            if (r1 == 0) goto L_0x04c7
            org.apache.http.StatusLine r2 = r1.getStatusLine()     // Catch:{ NumberFormatException -> 0x05ce, ClientProtocolException -> 0x05b9, IllegalStateException -> 0x04d9, FileNotFoundException -> 0x04e7, IOException -> 0x04f5, b -> 0x0503 }
            int r2 = r2.getStatusCode()     // Catch:{ NumberFormatException -> 0x05ce, ClientProtocolException -> 0x05b9, IllegalStateException -> 0x04d9, FileNotFoundException -> 0x04e7, IOException -> 0x04f5, b -> 0x0503 }
            r10 = 200(0xc8, float:2.8E-43)
            if (r2 == r10) goto L_0x032f
            r10 = 206(0xce, float:2.89E-43)
            if (r2 != r10) goto L_0x0450
        L_0x032f:
            org.apache.http.HttpEntity r6 = r1.getEntity()     // Catch:{ NumberFormatException -> 0x05ce, ClientProtocolException -> 0x05b9, IllegalStateException -> 0x04d9, FileNotFoundException -> 0x04e7, IOException -> 0x04f5, b -> 0x0503 }
            boolean r2 = r6.isStreaming()     // Catch:{ NumberFormatException -> 0x05ce, ClientProtocolException -> 0x05b9, IllegalStateException -> 0x04d9, FileNotFoundException -> 0x04e7, IOException -> 0x04f5, b -> 0x0503 }
            if (r2 == 0) goto L_0x043f
            long r1 = a(r1)     // Catch:{ NumberFormatException -> 0x05ce, ClientProtocolException -> 0x05b9, IllegalStateException -> 0x04d9, FileNotFoundException -> 0x04e7, IOException -> 0x04f5, b -> 0x0503 }
            long r1 = r1 + r3
            int r1 = (r1 > r13 ? 1 : (r1 == r13 ? 0 : -1))
            if (r1 != 0) goto L_0x041a
            java.io.InputStream r1 = r6.getContent()     // Catch:{ NumberFormatException -> 0x05ce, ClientProtocolException -> 0x05b9, IllegalStateException -> 0x04d9, FileNotFoundException -> 0x04e7, IOException -> 0x04f5, b -> 0x0503 }
            if (r1 == 0) goto L_0x040a
            java.io.BufferedInputStream r10 = new java.io.BufferedInputStream     // Catch:{ NumberFormatException -> 0x05d1, ClientProtocolException -> 0x05bc, IllegalStateException -> 0x059f, FileNotFoundException -> 0x0585, IOException -> 0x056d, b -> 0x0557, all -> 0x0541 }
            r10.<init>(r1)     // Catch:{ NumberFormatException -> 0x05d1, ClientProtocolException -> 0x05bc, IllegalStateException -> 0x059f, FileNotFoundException -> 0x0585, IOException -> 0x056d, b -> 0x0557, all -> 0x0541 }
            java.io.FileOutputStream r9 = new java.io.FileOutputStream     // Catch:{ NumberFormatException -> 0x05d6, ClientProtocolException -> 0x05c1, IllegalStateException -> 0x05a4, FileNotFoundException -> 0x058a, IOException -> 0x0571, b -> 0x055b, all -> 0x0545 }
            r2 = 1
            r9.<init>(r11, r2)     // Catch:{ NumberFormatException -> 0x05d6, ClientProtocolException -> 0x05c1, IllegalStateException -> 0x05a4, FileNotFoundException -> 0x058a, IOException -> 0x0571, b -> 0x055b, all -> 0x0545 }
            java.io.BufferedOutputStream r5 = new java.io.BufferedOutputStream     // Catch:{ NumberFormatException -> 0x05dc, ClientProtocolException -> 0x05c7, IllegalStateException -> 0x05aa, FileNotFoundException -> 0x0590, IOException -> 0x0576, b -> 0x0560, all -> 0x054a }
            r5.<init>(r9)     // Catch:{ NumberFormatException -> 0x05dc, ClientProtocolException -> 0x05c7, IllegalStateException -> 0x05aa, FileNotFoundException -> 0x0590, IOException -> 0x0576, b -> 0x0560, all -> 0x054a }
            r2 = 1024(0x400, float:1.435E-42)
            byte[] r2 = new byte[r2]     // Catch:{ NumberFormatException -> 0x03bb, ClientProtocolException -> 0x03dd, IllegalStateException -> 0x05b1, FileNotFoundException -> 0x0597, IOException -> 0x057d, b -> 0x0566, all -> 0x0550 }
        L_0x035c:
            int r7 = r10.read(r2)     // Catch:{ NumberFormatException -> 0x03bb, ClientProtocolException -> 0x03dd, IllegalStateException -> 0x05b1, FileNotFoundException -> 0x0597, IOException -> 0x057d, b -> 0x0566, all -> 0x0550 }
            r8 = -1
            if (r7 != r8) goto L_0x03a6
            r5.flush()     // Catch:{ NumberFormatException -> 0x03bb, ClientProtocolException -> 0x03dd, IllegalStateException -> 0x05b1, FileNotFoundException -> 0x0597, IOException -> 0x057d, b -> 0x0566, all -> 0x0550 }
            java.lang.String r2 = "DownloadControl"
            java.lang.String r3 = "Download finished"
            com.google.ads.sdk.f.e.c(r2, r3)     // Catch:{ NumberFormatException -> 0x03bb, ClientProtocolException -> 0x03dd, IllegalStateException -> 0x05b1, FileNotFoundException -> 0x0597, IOException -> 0x057d, b -> 0x0566, all -> 0x0550 }
            long r2 = r11.length()     // Catch:{ NumberFormatException -> 0x03bb, ClientProtocolException -> 0x03dd, IllegalStateException -> 0x05b1, FileNotFoundException -> 0x0597, IOException -> 0x057d, b -> 0x0566, all -> 0x0550 }
            int r2 = (r2 > r13 ? 1 : (r2 == r13 ? 0 : -1))
            if (r2 != 0) goto L_0x03f0
            r0 = r17
            android.os.Bundle r2 = r0.e     // Catch:{ NumberFormatException -> 0x03bb, ClientProtocolException -> 0x03dd, IllegalStateException -> 0x05b1, FileNotFoundException -> 0x0597, IOException -> 0x057d, b -> 0x0566, all -> 0x0550 }
            r2.remove(r12)     // Catch:{ NumberFormatException -> 0x03bb, ClientProtocolException -> 0x03dd, IllegalStateException -> 0x05b1, FileNotFoundException -> 0x0597, IOException -> 0x057d, b -> 0x0566, all -> 0x0550 }
            if (r19 == 0) goto L_0x0387
            java.lang.String r2 = r11.getAbsolutePath()     // Catch:{ NumberFormatException -> 0x03bb, ClientProtocolException -> 0x03dd, IllegalStateException -> 0x05b1, FileNotFoundException -> 0x0597, IOException -> 0x057d, b -> 0x0566, all -> 0x0550 }
            r0 = r19
            r0.a(r2)     // Catch:{ NumberFormatException -> 0x03bb, ClientProtocolException -> 0x03dd, IllegalStateException -> 0x05b1, FileNotFoundException -> 0x0597, IOException -> 0x057d, b -> 0x0566, all -> 0x0550 }
        L_0x0387:
            a(r1, r10, r9, r5, r6)
            r1 = 1
            goto L_0x0113
        L_0x038d:
            java.lang.String r1 = "DownloadControl"
            java.lang.String r2 = "File had been delete, start from 0."
            com.google.ads.sdk.f.e.b(r1, r2)
            r1 = 0
            r11.createNewFile()     // Catch:{ IOException -> 0x039b }
            goto L_0x02e7
        L_0x039b:
            r1 = move-exception
            java.lang.String r2 = "DownloadControl"
            java.lang.String r3 = "createNewFile fail."
            com.google.ads.sdk.f.e.d(r2, r3, r1)
            r1 = -2
            goto L_0x0113
        L_0x03a6:
            r0 = r17
            boolean r8 = r0.a     // Catch:{ NumberFormatException -> 0x03bb, ClientProtocolException -> 0x03dd, IllegalStateException -> 0x05b1, FileNotFoundException -> 0x0597, IOException -> 0x057d, b -> 0x0566, all -> 0x0550 }
            if (r8 == 0) goto L_0x03ce
            java.lang.String r2 = "DownloadControl"
            java.lang.String r3 = "stop download by user, throw UAException."
            com.google.ads.sdk.f.e.d(r2, r3)     // Catch:{ NumberFormatException -> 0x03bb, ClientProtocolException -> 0x03dd, IllegalStateException -> 0x05b1, FileNotFoundException -> 0x0597, IOException -> 0x057d, b -> 0x0566, all -> 0x0550 }
            com.google.ads.sdk.d.b r2 = new com.google.ads.sdk.d.b     // Catch:{ NumberFormatException -> 0x03bb, ClientProtocolException -> 0x03dd, IllegalStateException -> 0x05b1, FileNotFoundException -> 0x0597, IOException -> 0x057d, b -> 0x0566, all -> 0x0550 }
            java.lang.String r3 = "stop download by user."
            r2.<init>(r3)     // Catch:{ NumberFormatException -> 0x03bb, ClientProtocolException -> 0x03dd, IllegalStateException -> 0x05b1, FileNotFoundException -> 0x0597, IOException -> 0x057d, b -> 0x0566, all -> 0x0550 }
            throw r2     // Catch:{ NumberFormatException -> 0x03bb, ClientProtocolException -> 0x03dd, IllegalStateException -> 0x05b1, FileNotFoundException -> 0x0597, IOException -> 0x057d, b -> 0x0566, all -> 0x0550 }
        L_0x03bb:
            r2 = move-exception
            r7 = r5
            r8 = r9
            r9 = r10
            r5 = r1
            r1 = r2
        L_0x03c1:
            java.lang.String r2 = "DownloadControl"
            java.lang.String r3 = "NumberFormatException, get content length from http fail."
            com.google.ads.sdk.f.e.d(r2, r3, r1)     // Catch:{ all -> 0x0511 }
            a(r5, r9, r8, r7, r6)
            r1 = -2
            goto L_0x0113
        L_0x03ce:
            r8 = 0
            r5.write(r2, r8, r7)     // Catch:{ NumberFormatException -> 0x03bb, ClientProtocolException -> 0x03dd, IllegalStateException -> 0x05b1, FileNotFoundException -> 0x0597, IOException -> 0x057d, b -> 0x0566, all -> 0x0550 }
            long r7 = (long) r7     // Catch:{ NumberFormatException -> 0x03bb, ClientProtocolException -> 0x03dd, IllegalStateException -> 0x05b1, FileNotFoundException -> 0x0597, IOException -> 0x057d, b -> 0x0566, all -> 0x0550 }
            long r3 = r3 + r7
            r0 = r17
            r0.c = r3     // Catch:{ NumberFormatException -> 0x03bb, ClientProtocolException -> 0x03dd, IllegalStateException -> 0x05b1, FileNotFoundException -> 0x0597, IOException -> 0x057d, b -> 0x0566, all -> 0x0550 }
            r0 = r17
            r0.d = r13     // Catch:{ NumberFormatException -> 0x03bb, ClientProtocolException -> 0x03dd, IllegalStateException -> 0x05b1, FileNotFoundException -> 0x0597, IOException -> 0x057d, b -> 0x0566, all -> 0x0550 }
            goto L_0x035c
        L_0x03dd:
            r2 = move-exception
            r7 = r5
            r8 = r9
            r9 = r10
            r5 = r1
            r1 = r2
        L_0x03e3:
            java.lang.String r2 = "DownloadControl"
            java.lang.String r3 = "ClientProtocolException"
            com.google.ads.sdk.f.e.d(r2, r3, r1)     // Catch:{ all -> 0x0511 }
            a(r5, r9, r8, r7, r6)
            r1 = -2
            goto L_0x0113
        L_0x03f0:
            java.lang.String r2 = "DownloadControl"
            java.lang.String r3 = "The download file is not valid, download again"
            com.google.ads.sdk.f.e.d(r2, r3)     // Catch:{ NumberFormatException -> 0x03bb, ClientProtocolException -> 0x03dd, IllegalStateException -> 0x05b1, FileNotFoundException -> 0x0597, IOException -> 0x057d, b -> 0x0566, all -> 0x0550 }
            boolean r2 = r11.delete()     // Catch:{ NumberFormatException -> 0x03bb, ClientProtocolException -> 0x03dd, IllegalStateException -> 0x05b1, FileNotFoundException -> 0x0597, IOException -> 0x057d, b -> 0x0566, all -> 0x0550 }
            if (r2 != 0) goto L_0x0404
            java.lang.String r2 = "DownloadControl"
            java.lang.String r3 = "delete file fail !!!"
            com.google.ads.sdk.f.e.e(r2, r3)     // Catch:{ NumberFormatException -> 0x03bb, ClientProtocolException -> 0x03dd, IllegalStateException -> 0x05b1, FileNotFoundException -> 0x0597, IOException -> 0x057d, b -> 0x0566, all -> 0x0550 }
        L_0x0404:
            a(r1, r10, r9, r5, r6)
            r1 = -2
            goto L_0x0113
        L_0x040a:
            java.lang.String r2 = "DownloadControl"
            java.lang.String r3 = "NULL response stream"
            com.google.ads.sdk.f.e.d(r2, r3)     // Catch:{ NumberFormatException -> 0x05d1, ClientProtocolException -> 0x05bc, IllegalStateException -> 0x059f, FileNotFoundException -> 0x0585, IOException -> 0x056d, b -> 0x0557, all -> 0x0541 }
        L_0x0411:
            r2 = 0
            r3 = 0
            r4 = 0
            a(r1, r2, r3, r4, r6)
            r1 = 0
            goto L_0x0113
        L_0x041a:
            java.lang.String r1 = "DownloadControl"
            java.lang.String r2 = "File length between last and now were different."
            com.google.ads.sdk.f.e.e(r1, r2)     // Catch:{ NumberFormatException -> 0x05ce, ClientProtocolException -> 0x05b9, IllegalStateException -> 0x04d9, FileNotFoundException -> 0x04e7, IOException -> 0x04f5, b -> 0x0503 }
            r0 = r17
            android.os.Bundle r1 = r0.e     // Catch:{ NumberFormatException -> 0x05ce, ClientProtocolException -> 0x05b9, IllegalStateException -> 0x04d9, FileNotFoundException -> 0x04e7, IOException -> 0x04f5, b -> 0x0503 }
            r1.remove(r12)     // Catch:{ NumberFormatException -> 0x05ce, ClientProtocolException -> 0x05b9, IllegalStateException -> 0x04d9, FileNotFoundException -> 0x04e7, IOException -> 0x04f5, b -> 0x0503 }
            boolean r1 = r11.delete()     // Catch:{ NumberFormatException -> 0x05ce, ClientProtocolException -> 0x05b9, IllegalStateException -> 0x04d9, FileNotFoundException -> 0x04e7, IOException -> 0x04f5, b -> 0x0503 }
            if (r1 != 0) goto L_0x0691
            java.lang.String r1 = "DownloadControl"
            java.lang.String r2 = "delete file fail !!!"
            com.google.ads.sdk.f.e.e(r1, r2)     // Catch:{ NumberFormatException -> 0x05ce, ClientProtocolException -> 0x05b9, IllegalStateException -> 0x04d9, FileNotFoundException -> 0x04e7, IOException -> 0x04f5, b -> 0x0503 }
            r1 = 0
            r2 = 0
            r3 = 0
            r4 = 0
            a(r1, r2, r3, r4, r6)
            r1 = -2
            goto L_0x0113
        L_0x043f:
            java.lang.String r1 = "DownloadControl"
            java.lang.String r2 = "data mode from server is not stream."
            com.google.ads.sdk.f.e.e(r1, r2)     // Catch:{ NumberFormatException -> 0x05ce, ClientProtocolException -> 0x05b9, IllegalStateException -> 0x04d9, FileNotFoundException -> 0x04e7, IOException -> 0x04f5, b -> 0x0503 }
            r1 = 0
            r2 = 0
            r3 = 0
            r4 = 0
            a(r1, r2, r3, r4, r6)
            r1 = -2
            goto L_0x0113
        L_0x0450:
            r1 = 416(0x1a0, float:5.83E-43)
            if (r2 != r1) goto L_0x0485
            java.lang.String r1 = "DownloadControl"
            java.lang.String r2 = "server file length change at the same url, delete all info and download again at 0."
            com.google.ads.sdk.f.e.e(r1, r2)     // Catch:{ NumberFormatException -> 0x05ce, ClientProtocolException -> 0x05b9, IllegalStateException -> 0x04d9, FileNotFoundException -> 0x04e7, IOException -> 0x04f5, b -> 0x0503 }
            r0 = r17
            android.os.Bundle r1 = r0.e     // Catch:{ NumberFormatException -> 0x05ce, ClientProtocolException -> 0x05b9, IllegalStateException -> 0x04d9, FileNotFoundException -> 0x04e7, IOException -> 0x04f5, b -> 0x0503 }
            r1.remove(r12)     // Catch:{ NumberFormatException -> 0x05ce, ClientProtocolException -> 0x05b9, IllegalStateException -> 0x04d9, FileNotFoundException -> 0x04e7, IOException -> 0x04f5, b -> 0x0503 }
            boolean r1 = r11.delete()     // Catch:{ NumberFormatException -> 0x05ce, ClientProtocolException -> 0x05b9, IllegalStateException -> 0x04d9, FileNotFoundException -> 0x04e7, IOException -> 0x04f5, b -> 0x0503 }
            if (r1 != 0) goto L_0x047a
            java.lang.String r1 = "DownloadControl"
            java.lang.String r2 = "delete file fail !!!"
            com.google.ads.sdk.f.e.e(r1, r2)     // Catch:{ NumberFormatException -> 0x05ce, ClientProtocolException -> 0x05b9, IllegalStateException -> 0x04d9, FileNotFoundException -> 0x04e7, IOException -> 0x04f5, b -> 0x0503 }
            r1 = 0
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            a(r1, r2, r3, r4, r5)
            r1 = -2
            goto L_0x0113
        L_0x047a:
            r1 = 0
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            a(r1, r2, r3, r4, r5)
            r1 = 0
            goto L_0x0113
        L_0x0485:
            r1 = 404(0x194, float:5.66E-43)
            if (r2 != r1) goto L_0x04a8
            java.lang.String r1 = "DownloadControl"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ NumberFormatException -> 0x05ce, ClientProtocolException -> 0x05b9, IllegalStateException -> 0x04d9, FileNotFoundException -> 0x04e7, IOException -> 0x04f5, b -> 0x0503 }
            java.lang.String r3 = "The resource does not exist - "
            r2.<init>(r3)     // Catch:{ NumberFormatException -> 0x05ce, ClientProtocolException -> 0x05b9, IllegalStateException -> 0x04d9, FileNotFoundException -> 0x04e7, IOException -> 0x04f5, b -> 0x0503 }
            java.lang.StringBuilder r2 = r2.append(r12)     // Catch:{ NumberFormatException -> 0x05ce, ClientProtocolException -> 0x05b9, IllegalStateException -> 0x04d9, FileNotFoundException -> 0x04e7, IOException -> 0x04f5, b -> 0x0503 }
            java.lang.String r2 = r2.toString()     // Catch:{ NumberFormatException -> 0x05ce, ClientProtocolException -> 0x05b9, IllegalStateException -> 0x04d9, FileNotFoundException -> 0x04e7, IOException -> 0x04f5, b -> 0x0503 }
            com.google.ads.sdk.f.e.c(r1, r2)     // Catch:{ NumberFormatException -> 0x05ce, ClientProtocolException -> 0x05b9, IllegalStateException -> 0x04d9, FileNotFoundException -> 0x04e7, IOException -> 0x04f5, b -> 0x0503 }
            r1 = 0
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            a(r1, r2, r3, r4, r5)
            r1 = -3
            goto L_0x0113
        L_0x04a8:
            java.lang.String r1 = "DownloadControl"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ NumberFormatException -> 0x05ce, ClientProtocolException -> 0x05b9, IllegalStateException -> 0x04d9, FileNotFoundException -> 0x04e7, IOException -> 0x04f5, b -> 0x0503 }
            java.lang.String r4 = "network connect status code unexpected - "
            r3.<init>(r4)     // Catch:{ NumberFormatException -> 0x05ce, ClientProtocolException -> 0x05b9, IllegalStateException -> 0x04d9, FileNotFoundException -> 0x04e7, IOException -> 0x04f5, b -> 0x0503 }
            java.lang.StringBuilder r2 = r3.append(r2)     // Catch:{ NumberFormatException -> 0x05ce, ClientProtocolException -> 0x05b9, IllegalStateException -> 0x04d9, FileNotFoundException -> 0x04e7, IOException -> 0x04f5, b -> 0x0503 }
            java.lang.String r2 = r2.toString()     // Catch:{ NumberFormatException -> 0x05ce, ClientProtocolException -> 0x05b9, IllegalStateException -> 0x04d9, FileNotFoundException -> 0x04e7, IOException -> 0x04f5, b -> 0x0503 }
            com.google.ads.sdk.f.e.d(r1, r2)     // Catch:{ NumberFormatException -> 0x05ce, ClientProtocolException -> 0x05b9, IllegalStateException -> 0x04d9, FileNotFoundException -> 0x04e7, IOException -> 0x04f5, b -> 0x0503 }
            r1 = 0
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            a(r1, r2, r3, r4, r5)
            r1 = -2
            goto L_0x0113
        L_0x04c7:
            java.lang.String r1 = "DownloadControl"
            java.lang.String r2 = "NULL response"
            com.google.ads.sdk.f.e.d(r1, r2)     // Catch:{ NumberFormatException -> 0x05ce, ClientProtocolException -> 0x05b9, IllegalStateException -> 0x04d9, FileNotFoundException -> 0x04e7, IOException -> 0x04f5, b -> 0x0503 }
            r1 = 0
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            a(r1, r2, r3, r4, r5)
            r1 = 0
            goto L_0x0113
        L_0x04d9:
            r1 = move-exception
        L_0x04da:
            java.lang.String r2 = "DownloadControl"
            java.lang.String r3 = "ClientProtocolException"
            com.google.ads.sdk.f.e.d(r2, r3, r1)     // Catch:{ all -> 0x0511 }
            a(r5, r9, r8, r7, r6)
            r1 = -2
            goto L_0x0113
        L_0x04e7:
            r1 = move-exception
        L_0x04e8:
            java.lang.String r2 = "DownloadControl"
            java.lang.String r3 = "FileNotFoundException"
            com.google.ads.sdk.f.e.d(r2, r3, r1)     // Catch:{ all -> 0x0511 }
            a(r5, r9, r8, r7, r6)
            r1 = -2
            goto L_0x0113
        L_0x04f5:
            r1 = move-exception
        L_0x04f6:
            java.lang.String r2 = "DownloadControl"
            java.lang.String r3 = "IOException"
            com.google.ads.sdk.f.e.b(r2, r3, r1)     // Catch:{ all -> 0x0511 }
            a(r5, r9, r8, r7, r6)
            r1 = -1
            goto L_0x0113
        L_0x0503:
            r1 = move-exception
        L_0x0504:
            java.lang.String r2 = "DownloadControl"
            java.lang.String r3 = "UAException"
            com.google.ads.sdk.f.e.c(r2, r3, r1)     // Catch:{ all -> 0x0511 }
            a(r5, r9, r8, r7, r6)
            r1 = -2
            goto L_0x0113
        L_0x0511:
            r1 = move-exception
        L_0x0512:
            a(r5, r9, r8, r7, r6)
            throw r1
        L_0x0516:
            java.lang.String r3 = "DownloadControl"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r5 = "Param error !! url:"
            r4.<init>(r5)
            java.lang.StringBuilder r4 = r4.append(r12)
            java.lang.String r5 = " savefilePath:"
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r1 = r4.append(r1)
            java.lang.String r4 = " fileName:"
            java.lang.StringBuilder r1 = r1.append(r4)
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            com.google.ads.sdk.f.e.e(r3, r1)
            r1 = -2
            goto L_0x0113
        L_0x0541:
            r2 = move-exception
            r5 = r1
            r1 = r2
            goto L_0x0512
        L_0x0545:
            r2 = move-exception
            r9 = r10
            r5 = r1
            r1 = r2
            goto L_0x0512
        L_0x054a:
            r2 = move-exception
            r8 = r9
            r5 = r1
            r9 = r10
            r1 = r2
            goto L_0x0512
        L_0x0550:
            r2 = move-exception
            r7 = r5
            r8 = r9
            r9 = r10
            r5 = r1
            r1 = r2
            goto L_0x0512
        L_0x0557:
            r2 = move-exception
            r5 = r1
            r1 = r2
            goto L_0x0504
        L_0x055b:
            r2 = move-exception
            r9 = r10
            r5 = r1
            r1 = r2
            goto L_0x0504
        L_0x0560:
            r2 = move-exception
            r8 = r9
            r5 = r1
            r9 = r10
            r1 = r2
            goto L_0x0504
        L_0x0566:
            r2 = move-exception
            r7 = r5
            r8 = r9
            r9 = r10
            r5 = r1
            r1 = r2
            goto L_0x0504
        L_0x056d:
            r2 = move-exception
            r5 = r1
            r1 = r2
            goto L_0x04f6
        L_0x0571:
            r2 = move-exception
            r9 = r10
            r5 = r1
            r1 = r2
            goto L_0x04f6
        L_0x0576:
            r2 = move-exception
            r8 = r9
            r5 = r1
            r9 = r10
            r1 = r2
            goto L_0x04f6
        L_0x057d:
            r2 = move-exception
            r7 = r5
            r8 = r9
            r9 = r10
            r5 = r1
            r1 = r2
            goto L_0x04f6
        L_0x0585:
            r2 = move-exception
            r5 = r1
            r1 = r2
            goto L_0x04e8
        L_0x058a:
            r2 = move-exception
            r9 = r10
            r5 = r1
            r1 = r2
            goto L_0x04e8
        L_0x0590:
            r2 = move-exception
            r8 = r9
            r5 = r1
            r9 = r10
            r1 = r2
            goto L_0x04e8
        L_0x0597:
            r2 = move-exception
            r7 = r5
            r8 = r9
            r9 = r10
            r5 = r1
            r1 = r2
            goto L_0x04e8
        L_0x059f:
            r2 = move-exception
            r5 = r1
            r1 = r2
            goto L_0x04da
        L_0x05a4:
            r2 = move-exception
            r9 = r10
            r5 = r1
            r1 = r2
            goto L_0x04da
        L_0x05aa:
            r2 = move-exception
            r8 = r9
            r5 = r1
            r9 = r10
            r1 = r2
            goto L_0x04da
        L_0x05b1:
            r2 = move-exception
            r7 = r5
            r8 = r9
            r9 = r10
            r5 = r1
            r1 = r2
            goto L_0x04da
        L_0x05b9:
            r1 = move-exception
            goto L_0x03e3
        L_0x05bc:
            r2 = move-exception
            r5 = r1
            r1 = r2
            goto L_0x03e3
        L_0x05c1:
            r2 = move-exception
            r9 = r10
            r5 = r1
            r1 = r2
            goto L_0x03e3
        L_0x05c7:
            r2 = move-exception
            r8 = r9
            r5 = r1
            r9 = r10
            r1 = r2
            goto L_0x03e3
        L_0x05ce:
            r1 = move-exception
            goto L_0x03c1
        L_0x05d1:
            r2 = move-exception
            r5 = r1
            r1 = r2
            goto L_0x03c1
        L_0x05d6:
            r2 = move-exception
            r9 = r10
            r5 = r1
            r1 = r2
            goto L_0x03c1
        L_0x05dc:
            r2 = move-exception
            r8 = r9
            r5 = r1
            r9 = r10
            r1 = r2
            goto L_0x03c1
        L_0x05e3:
            r1 = move-exception
            r11 = r5
            goto L_0x0228
        L_0x05e7:
            r1 = move-exception
            goto L_0x0228
        L_0x05ea:
            r1 = move-exception
            r9 = r10
            goto L_0x0228
        L_0x05ee:
            r1 = move-exception
            r8 = r6
            r9 = r10
            goto L_0x0228
        L_0x05f3:
            r1 = move-exception
            r7 = r5
            r8 = r6
            r9 = r10
            goto L_0x0228
        L_0x05f9:
            r1 = move-exception
            r7 = r3
            r8 = r4
            r9 = r5
            r11 = r6
            goto L_0x0228
        L_0x0600:
            r1 = move-exception
            r11 = r5
            goto L_0x0218
        L_0x0604:
            r1 = move-exception
            goto L_0x0218
        L_0x0607:
            r1 = move-exception
            r9 = r10
            goto L_0x0218
        L_0x060b:
            r1 = move-exception
            r8 = r6
            r9 = r10
            goto L_0x0218
        L_0x0610:
            r1 = move-exception
            r7 = r5
            r8 = r6
            r9 = r10
            goto L_0x0218
        L_0x0616:
            r1 = move-exception
            r11 = r5
            goto L_0x0208
        L_0x061a:
            r1 = move-exception
            goto L_0x0208
        L_0x061d:
            r1 = move-exception
            r9 = r10
            goto L_0x0208
        L_0x0621:
            r1 = move-exception
            r8 = r6
            r9 = r10
            goto L_0x0208
        L_0x0626:
            r1 = move-exception
            r7 = r5
            r8 = r6
            r9 = r10
            goto L_0x0208
        L_0x062c:
            r1 = move-exception
            r11 = r5
            goto L_0x01f8
        L_0x0630:
            r1 = move-exception
            goto L_0x01f8
        L_0x0633:
            r1 = move-exception
            r9 = r10
            goto L_0x01f8
        L_0x0637:
            r1 = move-exception
            r8 = r6
            r9 = r10
            goto L_0x01f8
        L_0x063c:
            r1 = move-exception
            r7 = r5
            r8 = r6
            r9 = r10
            goto L_0x01f8
        L_0x0642:
            r1 = move-exception
            r11 = r5
            goto L_0x01e8
        L_0x0646:
            r1 = move-exception
            goto L_0x01e8
        L_0x0649:
            r1 = move-exception
            r9 = r10
            goto L_0x01e8
        L_0x064d:
            r1 = move-exception
            r8 = r6
            r9 = r10
            goto L_0x01e8
        L_0x0652:
            r1 = move-exception
            r7 = r5
            r8 = r6
            r9 = r10
            goto L_0x01e8
        L_0x0658:
            r1 = move-exception
            r2 = r6
            r11 = r5
            goto L_0x014d
        L_0x065d:
            r1 = move-exception
            r11 = r5
            goto L_0x014d
        L_0x0661:
            r1 = move-exception
            goto L_0x014d
        L_0x0664:
            r1 = move-exception
            r9 = r10
            goto L_0x014d
        L_0x0668:
            r1 = move-exception
            r8 = r6
            r9 = r10
            goto L_0x014d
        L_0x066d:
            r1 = move-exception
            r2 = r6
            r3 = r7
            r4 = r8
            r6 = r5
            r5 = r9
            goto L_0x012e
        L_0x0675:
            r1 = move-exception
            r3 = r7
            r4 = r8
            r6 = r5
            r5 = r9
            goto L_0x012e
        L_0x067c:
            r1 = move-exception
            r3 = r7
            r4 = r8
            r5 = r9
            r6 = r11
            goto L_0x012e
        L_0x0683:
            r1 = move-exception
            r3 = r7
            r4 = r8
            r5 = r10
            r6 = r11
            goto L_0x012e
        L_0x068a:
            r1 = move-exception
            r3 = r7
            r4 = r6
            r5 = r10
            r6 = r11
            goto L_0x012e
        L_0x0691:
            r1 = r5
            goto L_0x0411
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.ads.sdk.download.a.a(android.content.Context, com.google.ads.sdk.download.c, com.google.ads.sdk.b.a):int");
    }

    private static long a(HttpResponse httpResponse) {
        Header firstHeader = httpResponse.getFirstHeader("Content-Length");
        long longValue = firstHeader != null ? Long.valueOf(firstHeader.getValue()).longValue() : 0;
        if (longValue > 0) {
            return longValue;
        }
        throw new b("get the file total length from http is 0.");
    }

    private static HttpGet a(String str, long j) {
        HttpGet httpGet = new HttpGet(str);
        httpGet.addHeader("Connection", "Close");
        httpGet.addHeader("User-Agent", "MLAPK");
        if (j >= 0) {
            httpGet.addHeader("Range", "bytes=" + j + "-");
        }
        return httpGet;
    }

    private static DefaultHttpClient a() {
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        HttpProtocolParams.setVersion(basicHttpParams, HttpVersion.HTTP_1_1);
        HttpProtocolParams.setContentCharset(basicHttpParams, OAuth.ENCODING);
        HttpConnectionParams.setTcpNoDelay(basicHttpParams, true);
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, 30000);
        HttpConnectionParams.setSoTimeout(basicHttpParams, 30000);
        return new DefaultHttpClient(basicHttpParams);
    }

    private static void a(InputStream inputStream, BufferedInputStream bufferedInputStream, FileOutputStream fileOutputStream, BufferedOutputStream bufferedOutputStream, HttpEntity httpEntity) {
        if (bufferedOutputStream != null) {
            try {
                bufferedOutputStream.close();
            } catch (IOException e2) {
            }
        }
        if (fileOutputStream != null) {
            try {
                fileOutputStream.close();
            } catch (IOException e3) {
            }
        }
        if (bufferedInputStream != null) {
            try {
                bufferedInputStream.close();
            } catch (IOException e4) {
            }
        }
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (IOException e5) {
            }
        }
        if (httpEntity != null) {
            try {
                httpEntity.consumeContent();
            } catch (IOException e6) {
            }
        }
    }

    public static boolean a(int i) {
        return 2 == i || 3 == i;
    }
}
