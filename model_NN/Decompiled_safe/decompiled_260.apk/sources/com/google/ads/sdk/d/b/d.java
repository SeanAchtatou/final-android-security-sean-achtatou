package com.google.ads.sdk.d.b;

import com.google.ads.sdk.f.e;
import com.google.ads.sdk.f.o;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public final class d extends e {
    public Map h;

    public d(byte[] bArr) {
        super(bArr);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        int a = a(this.f, 0, 2);
        String a2 = o.a(this.f, 2, a);
        e.c("RegisterResponseCommand", "length:" + a + ",respStr:" + a2);
        if (a2 != null && !"".equals(a2)) {
            try {
                JSONObject jSONObject = new JSONObject(a2);
                this.h = new HashMap();
                String optString = jSONObject.optString("REGISTER_ID");
                String optString2 = jSONObject.optString("PASSWORD");
                this.h.put("REGISTER_ID", optString);
                this.h.put("PASSWORD", optString2);
                if (jSONObject.has("NEXT_AD_CHECK_TIME")) {
                    this.h.put("NEXT_AD_CHECK_TIME", Integer.valueOf(jSONObject.getInt("NEXT_AD_CHECK_TIME")));
                }
            } catch (JSONException e) {
                e.b("RegisterResponseCommand", "parse register json error", e);
            }
        }
    }
}
