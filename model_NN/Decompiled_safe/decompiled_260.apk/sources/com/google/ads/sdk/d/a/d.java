package com.google.ads.sdk.d.a;

import com.google.ads.sdk.f.o;
import java.io.ByteArrayOutputStream;
import java.util.Map;
import org.json.JSONObject;

public final class d extends e {
    private final Map i;
    private int j;

    public d(Map map) {
        super(6);
        this.i = map;
    }

    public final void a(int i2) {
        this.j = 0;
    }

    /* access modifiers changed from: protected */
    public final void a(ByteArrayOutputStream byteArrayOutputStream) {
        if (this.i != null && this.i.size() > 0) {
            byteArrayOutputStream.write(o.a(this.j, 2));
            b(byteArrayOutputStream, new JSONObject(this.i).toString());
        }
    }
}
