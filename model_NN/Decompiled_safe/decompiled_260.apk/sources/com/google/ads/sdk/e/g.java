package com.google.ads.sdk.e;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import com.google.ads.sdk.b.d;

public final class g extends LinearLayout {
    private LinearLayout a;
    private ScrollView b;
    /* access modifiers changed from: private */
    public Context c;
    /* access modifiers changed from: private */
    public d d;

    /* JADX WARNING: Removed duplicated region for block: B:21:0x018b  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x01f5  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public g(android.content.Context r11, com.google.ads.sdk.b.d r12) {
        /*
            r10 = this;
            r9 = 30
            r6 = 1
            r5 = -2
            r8 = 0
            r7 = -1
            r0 = 0
            r10.<init>(r11, r0)
            r10.c = r11
            r10.d = r12
            android.widget.LinearLayout$LayoutParams r0 = new android.widget.LinearLayout$LayoutParams
            r0.<init>(r7, r7)
            r10.setLayoutParams(r0)
            java.lang.String r0 = "default_background"
            java.io.InputStream r0 = com.google.ads.sdk.b.e.a(r0)     // Catch:{ IOException -> 0x01c3 }
            java.lang.String r1 = "default_background.jpg"
            android.graphics.drawable.Drawable r0 = android.graphics.drawable.BitmapDrawable.createFromStream(r0, r1)     // Catch:{ IOException -> 0x01c3 }
            android.graphics.drawable.BitmapDrawable r0 = (android.graphics.drawable.BitmapDrawable) r0     // Catch:{ IOException -> 0x01c3 }
            r10.setBackgroundDrawable(r0)     // Catch:{ IOException -> 0x01c3 }
        L_0x0027:
            r10.setOrientation(r6)
            android.widget.LinearLayout r0 = new android.widget.LinearLayout
            android.content.Context r1 = r10.c
            r0.<init>(r1)
            r10.a = r0
            android.widget.LinearLayout$LayoutParams r0 = new android.widget.LinearLayout$LayoutParams
            r0.<init>(r7, r5)
            android.widget.LinearLayout r1 = r10.a
            r1.setOrientation(r6)
            android.widget.LinearLayout r1 = r10.a
            r2 = 10
            r1.setPadding(r9, r8, r2, r8)
            android.widget.LinearLayout r1 = r10.a
            r10.addView(r1, r0)
            android.widget.ImageView r1 = new android.widget.ImageView
            android.content.Context r0 = r10.c
            r1.<init>(r0)
            java.lang.String r0 = "shotcut"
            java.io.InputStream r0 = com.google.ads.sdk.b.e.a(r0)     // Catch:{ IOException -> 0x01c9 }
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeStream(r0)     // Catch:{ IOException -> 0x01c9 }
            r1.setImageBitmap(r0)     // Catch:{ IOException -> 0x01c9 }
        L_0x005d:
            com.google.ads.sdk.e.h r0 = new com.google.ads.sdk.e.h
            r0.<init>(r10)
            r1.setOnClickListener(r0)
            android.widget.LinearLayout$LayoutParams r0 = new android.widget.LinearLayout$LayoutParams
            r0.<init>(r5, r5)
            r2 = 5
            r0.gravity = r2
            android.widget.LinearLayout r2 = r10.a
            r2.addView(r1, r0)
            android.widget.TextView r1 = new android.widget.TextView
            android.content.Context r0 = r10.c
            r1.<init>(r0)
            android.content.Context r0 = r10.c
            int r0 = com.google.ads.sdk.f.g.a(r0, r9)
            r2 = 24
            if (r0 >= r2) goto L_0x0085
            r0 = 24
        L_0x0085:
            float r0 = (float) r0
            r1.setTextSize(r0)
            com.google.ads.sdk.b.d r0 = r10.d
            com.google.ads.sdk.b.a r0 = r0.b
            java.lang.String r0 = r0.I
            r1.setText(r0)
            r1.setTextColor(r7)
            com.google.ads.sdk.e.i r0 = new com.google.ads.sdk.e.i
            r0.<init>(r10)
            r1.setOnClickListener(r0)
            android.widget.LinearLayout$LayoutParams r0 = new android.widget.LinearLayout$LayoutParams
            r0.<init>(r5, r5)
            android.widget.LinearLayout r2 = r10.a
            r2.addView(r1, r0)
            android.widget.ImageView r1 = new android.widget.ImageView
            android.content.Context r0 = r10.c
            r1.<init>(r0)
            android.widget.ImageView$ScaleType r0 = android.widget.ImageView.ScaleType.FIT_XY
            r1.setScaleType(r0)
            android.content.Context r0 = r10.c     // Catch:{ IOException -> 0x01e2 }
            com.google.ads.sdk.b.d r2 = r10.d     // Catch:{ IOException -> 0x01e2 }
            com.google.ads.sdk.b.a r2 = r2.b     // Catch:{ IOException -> 0x01e2 }
            java.lang.String r2 = r2.w     // Catch:{ IOException -> 0x01e2 }
            java.lang.String r0 = com.google.ads.sdk.c.a.e(r0, r2)     // Catch:{ IOException -> 0x01e2 }
            boolean r2 = com.google.ads.sdk.f.p.a(r0)     // Catch:{ IOException -> 0x01e2 }
            if (r2 != 0) goto L_0x01d3
            boolean r2 = com.google.ads.sdk.f.a.c(r0)     // Catch:{ IOException -> 0x01e2 }
            if (r2 == 0) goto L_0x01d3
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeFile(r0)     // Catch:{ IOException -> 0x01e2 }
            r1.setImageBitmap(r0)     // Catch:{ IOException -> 0x01e2 }
        L_0x00d2:
            com.google.ads.sdk.e.j r0 = new com.google.ads.sdk.e.j
            r0.<init>(r10)
            r1.setOnClickListener(r0)
            android.widget.LinearLayout$LayoutParams r0 = new android.widget.LinearLayout$LayoutParams
            android.content.Context r2 = r10.c
            r3 = 420(0x1a4, float:5.89E-43)
            int r2 = com.google.ads.sdk.f.g.a(r2, r3)
            android.content.Context r3 = r10.c
            r4 = 280(0x118, float:3.92E-43)
            int r3 = com.google.ads.sdk.f.g.a(r3, r4)
            r0.<init>(r2, r3)
            android.content.Context r2 = r10.c
            r3 = 10
            int r2 = com.google.ads.sdk.f.g.a(r2, r3)
            r0.setMargins(r8, r2, r8, r8)
            android.widget.LinearLayout r2 = r10.a
            r2.addView(r1, r0)
            r10.a()
            android.widget.LinearLayout$LayoutParams r0 = new android.widget.LinearLayout$LayoutParams
            r0.<init>(r7, r7)
            android.widget.ScrollView r1 = new android.widget.ScrollView
            android.content.Context r2 = r10.c
            r1.<init>(r2)
            r10.b = r1
            r1 = 1065353216(0x3f800000, float:1.0)
            r0.weight = r1
            android.widget.ScrollView r1 = r10.b
            r10.addView(r1, r0)
            android.widget.LinearLayout r1 = new android.widget.LinearLayout
            android.content.Context r2 = r10.c
            r1.<init>(r2)
            r1.setOrientation(r6)
            r1.setGravity(r6)
            r1.setPadding(r9, r8, r9, r8)
            android.widget.ScrollView r2 = r10.b
            r2.addView(r1, r0)
            android.widget.TextView r0 = new android.widget.TextView
            android.content.Context r2 = r10.c
            r0.<init>(r2)
            r0.setTextColor(r7)
            r2 = 1094713344(0x41400000, float:12.0)
            r0.setTextSize(r2)
            r2 = 1079613850(0x4059999a, float:3.4)
            r3 = 1065353216(0x3f800000, float:1.0)
            r0.setLineSpacing(r2, r3)
            com.google.ads.sdk.b.d r2 = r10.d
            com.google.ads.sdk.b.a r2 = r2.b
            java.lang.String r2 = r2.v
            r0.setText(r2)
            android.widget.LinearLayout$LayoutParams r2 = new android.widget.LinearLayout$LayoutParams
            r2.<init>(r7, r5)
            r1.addView(r0, r2)
            android.widget.Button r1 = new android.widget.Button
            android.content.Context r0 = r10.c
            r1.<init>(r0)
            java.lang.String r0 = "download"
            java.io.InputStream r0 = com.google.ads.sdk.b.e.a(r0)     // Catch:{ IOException -> 0x01ec }
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeStream(r0)     // Catch:{ IOException -> 0x01ec }
            android.graphics.drawable.NinePatchDrawable r2 = new android.graphics.drawable.NinePatchDrawable     // Catch:{ IOException -> 0x01ec }
            android.content.Context r3 = r10.c     // Catch:{ IOException -> 0x01ec }
            android.content.res.Resources r3 = r3.getResources()     // Catch:{ IOException -> 0x01ec }
            android.graphics.NinePatch r4 = new android.graphics.NinePatch     // Catch:{ IOException -> 0x01ec }
            byte[] r5 = r0.getNinePatchChunk()     // Catch:{ IOException -> 0x01ec }
            r6 = 0
            r4.<init>(r0, r5, r6)     // Catch:{ IOException -> 0x01ec }
            r2.<init>(r3, r4)     // Catch:{ IOException -> 0x01ec }
            r1.setBackgroundDrawable(r2)     // Catch:{ IOException -> 0x01ec }
        L_0x017f:
            com.google.ads.sdk.b.d r0 = r10.d
            com.google.ads.sdk.b.a r0 = r0.b
            java.lang.String r0 = r0.J
            boolean r0 = com.google.ads.sdk.f.p.a(r0)
            if (r0 == 0) goto L_0x01f5
            java.lang.String r0 = "点击下载"
            r1.setText(r0)
        L_0x0190:
            android.content.Context r0 = r10.c
            r2 = 20
            int r0 = com.google.ads.sdk.f.g.a(r0, r2)
            float r0 = (float) r0
            r1.setTextSize(r0)
            r1.setTextColor(r7)
            com.google.ads.sdk.e.k r0 = new com.google.ads.sdk.e.k
            r0.<init>(r10)
            r1.setOnClickListener(r0)
            android.widget.LinearLayout$LayoutParams r0 = new android.widget.LinearLayout$LayoutParams
            android.content.Context r2 = r10.c
            r3 = 70
            int r2 = com.google.ads.sdk.f.g.a(r2, r3)
            r0.<init>(r7, r2)
            android.content.Context r2 = r10.c
            r3 = 10
            int r2 = com.google.ads.sdk.f.g.a(r2, r3)
            r0.setMargins(r8, r2, r8, r8)
            r10.addView(r1, r0)
            return
        L_0x01c3:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0027
        L_0x01c9:
            r0 = move-exception
            java.lang.String r2 = "UAView"
            java.lang.String r3 = "decode back.png exception"
            com.google.ads.sdk.f.e.b(r2, r3, r0)
            goto L_0x005d
        L_0x01d3:
            java.lang.String r0 = "image_default_l"
            java.io.InputStream r0 = com.google.ads.sdk.b.e.a(r0)     // Catch:{ IOException -> 0x01e2 }
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeStream(r0)     // Catch:{ IOException -> 0x01e2 }
            r1.setImageBitmap(r0)     // Catch:{ IOException -> 0x01e2 }
            goto L_0x00d2
        L_0x01e2:
            r0 = move-exception
            java.lang.String r2 = "UAView"
            java.lang.String r3 = "decode default_preview.png exception"
            com.google.ads.sdk.f.e.b(r2, r3, r0)
            goto L_0x00d2
        L_0x01ec:
            r0 = move-exception
            java.lang.String r2 = "UAView"
            java.lang.String r3 = "decode download.png exception"
            com.google.ads.sdk.f.e.b(r2, r3, r0)
            goto L_0x017f
        L_0x01f5:
            com.google.ads.sdk.b.d r0 = r10.d
            com.google.ads.sdk.b.a r0 = r0.b
            java.lang.String r0 = r0.J
            r1.setText(r0)
            goto L_0x0190
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.ads.sdk.e.g.<init>(android.content.Context, com.google.ads.sdk.b.d):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x015b A[SYNTHETIC, Splitter:B:14:0x015b] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x019d  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x00a6  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a() {
        /*
            r14 = this;
            r9 = 10
            r12 = -1
            r3 = 1
            r11 = -2
            r10 = 0
            android.widget.LinearLayout$LayoutParams r0 = new android.widget.LinearLayout$LayoutParams
            r0.<init>(r12, r11)
            android.widget.LinearLayout r1 = new android.widget.LinearLayout
            android.content.Context r2 = r14.c
            r1.<init>(r2)
            r1.setOrientation(r10)
            r2 = 30
            r4 = 30
            r1.setPadding(r2, r9, r4, r9)
            r14.addView(r1, r0)
            android.widget.ImageView r2 = new android.widget.ImageView
            android.content.Context r0 = r14.c
            r2.<init>(r0)
            android.content.Context r0 = r14.c     // Catch:{ IOException -> 0x0187 }
            com.google.ads.sdk.b.d r4 = r14.d     // Catch:{ IOException -> 0x0187 }
            com.google.ads.sdk.b.a r4 = r4.b     // Catch:{ IOException -> 0x0187 }
            java.lang.String r4 = r4.r     // Catch:{ IOException -> 0x0187 }
            java.lang.String r0 = com.google.ads.sdk.c.a.e(r0, r4)     // Catch:{ IOException -> 0x0187 }
            boolean r4 = com.google.ads.sdk.f.p.a(r0)     // Catch:{ IOException -> 0x0187 }
            if (r4 != 0) goto L_0x0178
            boolean r4 = com.google.ads.sdk.f.a.c(r0)     // Catch:{ IOException -> 0x0187 }
            if (r4 == 0) goto L_0x0178
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeFile(r0)     // Catch:{ IOException -> 0x0187 }
            r2.setImageBitmap(r0)     // Catch:{ IOException -> 0x0187 }
        L_0x0045:
            android.widget.LinearLayout$LayoutParams r0 = new android.widget.LinearLayout$LayoutParams
            android.content.Context r4 = r14.c
            r5 = 72
            int r4 = com.google.ads.sdk.f.g.a(r4, r5)
            android.content.Context r5 = r14.c
            r6 = 72
            int r5 = com.google.ads.sdk.f.g.a(r5, r6)
            r0.<init>(r4, r5)
            r1.addView(r2, r0)
            android.widget.LinearLayout r2 = new android.widget.LinearLayout
            android.content.Context r0 = r14.c
            r2.<init>(r0)
            r2.setOrientation(r3)
            r2.setPadding(r9, r10, r10, r10)
            android.widget.LinearLayout$LayoutParams r4 = new android.widget.LinearLayout$LayoutParams
            r4.<init>(r11, r11)
            r1.addView(r2, r4)
            android.widget.TextView r0 = new android.widget.TextView
            android.content.Context r1 = r14.c
            r0.<init>(r1)
            com.google.ads.sdk.b.d r1 = r14.d
            com.google.ads.sdk.b.a r1 = r1.b
            java.lang.String r1 = r1.q
            r0.setText(r1)
            r0.setTextColor(r12)
            r1 = 1099956224(0x41900000, float:18.0)
            r0.setTextSize(r1)
            r2.addView(r0, r4)
            android.widget.LinearLayout r1 = new android.widget.LinearLayout
            android.content.Context r0 = r14.c
            r1.<init>(r0)
            r1.setOrientation(r10)
            r2.addView(r1, r4)
            android.content.Context r0 = r14.c
            r5 = 8
            int r0 = com.google.ads.sdk.f.g.a(r0, r5)
            r5 = 8
            if (r0 >= r5) goto L_0x00a8
            r0 = 8
        L_0x00a8:
            android.widget.TextView r5 = new android.widget.TextView
            android.content.Context r6 = r14.c
            r5.<init>(r6)
            java.lang.String r6 = "版本：%s"
            java.lang.Object[] r7 = new java.lang.Object[r3]
            com.google.ads.sdk.b.d r8 = r14.d
            com.google.ads.sdk.b.a r8 = r8.b
            java.lang.String r8 = r8.t
            r7[r10] = r8
            java.lang.String r6 = java.lang.String.format(r6, r7)
            r5.setText(r6)
            r5.setTextColor(r12)
            float r6 = (float) r0
            r5.setTextSize(r6)
            r1.addView(r5, r4)
            android.widget.TextView r5 = new android.widget.TextView
            android.content.Context r6 = r14.c
            r5.<init>(r6)
            java.lang.String r6 = "大小：%s"
            java.lang.Object[] r7 = new java.lang.Object[r3]
            com.google.ads.sdk.b.d r8 = r14.d
            com.google.ads.sdk.b.a r8 = r8.b
            java.lang.String r8 = r8.u
            r7[r10] = r8
            java.lang.String r6 = java.lang.String.format(r6, r7)
            r5.setText(r6)
            r5.setTextColor(r12)
            float r6 = (float) r0
            r5.setTextSize(r6)
            r5.setPadding(r9, r10, r10, r10)
            r1.addView(r5, r4)
            android.widget.LinearLayout r1 = new android.widget.LinearLayout
            android.content.Context r5 = r14.c
            r1.<init>(r5)
            r1.setOrientation(r3)
            r2.addView(r1)
            android.widget.LinearLayout$LayoutParams r5 = new android.widget.LinearLayout$LayoutParams
            r5.<init>(r11, r11)
            android.widget.TextView r6 = new android.widget.TextView
            android.content.Context r7 = r14.c
            r6.<init>(r7)
            java.lang.String r7 = "下载次数：%s"
            java.lang.Object[] r8 = new java.lang.Object[r3]
            com.google.ads.sdk.b.d r9 = r14.d
            com.google.ads.sdk.b.a r9 = r9.b
            int r9 = r9.y
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)
            r8[r10] = r9
            java.lang.String r7 = java.lang.String.format(r7, r8)
            r6.setText(r7)
            r6.setTextColor(r12)
            float r0 = (float) r0
            r6.setTextSize(r0)
            r1.addView(r6, r5)
            android.widget.LinearLayout r5 = new android.widget.LinearLayout
            android.content.Context r0 = r14.c
            r5.<init>(r0)
            r5.setOrientation(r10)
            r2.addView(r5, r4)
            android.widget.LinearLayout$LayoutParams r4 = new android.widget.LinearLayout$LayoutParams
            r4.<init>(r11, r11)
            r0 = 5
            r5.setPadding(r10, r0, r10, r10)
            r1 = 0
            r2 = 0
            java.lang.String r0 = "star_full"
            java.io.InputStream r0 = com.google.ads.sdk.b.e.a(r0)     // Catch:{ IOException -> 0x0191 }
            android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeStream(r0)     // Catch:{ IOException -> 0x0191 }
            java.lang.String r0 = "star_blank"
            java.io.InputStream r0 = com.google.ads.sdk.b.e.a(r0)     // Catch:{ IOException -> 0x0191 }
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeStream(r0)     // Catch:{ IOException -> 0x0191 }
            if (r1 == 0) goto L_0x0173
            android.content.Context r2 = r14.c     // Catch:{ IOException -> 0x01b9 }
            int r6 = r1.getWidth()     // Catch:{ IOException -> 0x01b9 }
            int r2 = com.google.ads.sdk.f.g.a(r2, r6)     // Catch:{ IOException -> 0x01b9 }
            r4.width = r2     // Catch:{ IOException -> 0x01b9 }
            android.content.Context r2 = r14.c     // Catch:{ IOException -> 0x01b9 }
            int r6 = r1.getHeight()     // Catch:{ IOException -> 0x01b9 }
            int r2 = com.google.ads.sdk.f.g.a(r2, r6)     // Catch:{ IOException -> 0x01b9 }
            r4.height = r2     // Catch:{ IOException -> 0x01b9 }
        L_0x0173:
            r2 = r3
        L_0x0174:
            r3 = 5
            if (r2 <= r3) goto L_0x019d
            return
        L_0x0178:
            java.lang.String r0 = "default_icon"
            java.io.InputStream r0 = com.google.ads.sdk.b.e.a(r0)     // Catch:{ IOException -> 0x0187 }
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeStream(r0)     // Catch:{ IOException -> 0x0187 }
            r2.setImageBitmap(r0)     // Catch:{ IOException -> 0x0187 }
            goto L_0x0045
        L_0x0187:
            r0 = move-exception
            java.lang.String r4 = "UAView"
            java.lang.String r5 = "decode default_icon.png exception"
            com.google.ads.sdk.f.e.b(r4, r5, r0)
            goto L_0x0045
        L_0x0191:
            r0 = move-exception
            r13 = r0
            r0 = r2
            r2 = r13
        L_0x0195:
            java.lang.String r6 = "UAView"
            java.lang.String r7 = "decode star.png exception"
            com.google.ads.sdk.f.e.b(r6, r7, r2)
            goto L_0x0173
        L_0x019d:
            android.widget.ImageView r3 = new android.widget.ImageView
            android.content.Context r6 = r14.c
            r3.<init>(r6)
            com.google.ads.sdk.b.d r6 = r14.d
            com.google.ads.sdk.b.a r6 = r6.b
            int r6 = r6.z
            if (r2 > r6) goto L_0x01b5
            r3.setImageBitmap(r1)
        L_0x01af:
            r5.addView(r3, r4)
            int r2 = r2 + 1
            goto L_0x0174
        L_0x01b5:
            r3.setImageBitmap(r0)
            goto L_0x01af
        L_0x01b9:
            r2 = move-exception
            goto L_0x0195
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.ads.sdk.e.g.a():void");
    }

    /* access modifiers changed from: protected */
    public final void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
    }

    /* access modifiers changed from: protected */
    public final void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
    }
}
