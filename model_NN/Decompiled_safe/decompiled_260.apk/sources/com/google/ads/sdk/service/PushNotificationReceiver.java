package com.google.ads.sdk.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.flurry.android.CallbackEvent;
import com.google.ads.AdDownService;
import com.google.ads.sdk.b.d;
import com.google.ads.sdk.d.a.b;
import com.google.ads.sdk.d.c;
import com.google.ads.sdk.f.a;
import com.google.ads.sdk.f.e;
import com.google.ads.sdk.f.k;
import com.google.ads.sdk.f.p;
import com.google.ads.sdk.f.r;
import java.util.HashMap;

public class PushNotificationReceiver extends BroadcastReceiver {
    private static final String a = k.a(PushNotificationReceiver.class);

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.ads.sdk.c.a.a(android.content.Context, java.lang.String, boolean):void
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.google.ads.sdk.c.a.a(android.content.Context, java.lang.String, java.lang.String):void
      com.google.ads.sdk.c.a.a(android.content.Context, java.lang.String, boolean):void */
    public void onReceive(Context context, Intent intent) {
        d dVar;
        e.c(a, "NotificationReceiver.onReceive()...");
        String action = intent.getAction();
        e.c(a, "action=" + action);
        if (a.d(context, "com.suyue168.android.sdk.SHOW_NOTIFICATION").equals(action)) {
            d dVar2 = (d) intent.getSerializableExtra("PUSH_INFO");
            if (!a.b(context, dVar2.b.m) || dVar2.b.p) {
                com.google.ads.sdk.b.a.a(context, dVar2);
            } else {
                r.a(context, Integer.parseInt(dVar2.b.e), 102);
            }
        } else if (action.equals("android.net.conn.CONNECTIVITY_CHANGE")) {
            if (a.a(context)) {
                AdDownService.a(context);
                String d = com.google.ads.sdk.c.a.d(context);
                if (!p.a(d)) {
                    HashMap hashMap = new HashMap();
                    hashMap.put("BATCH_USER_STEP", d);
                    c.a(new b(hashMap));
                }
            }
        } else if (action.equals("android.intent.action.PACKAGE_ADDED")) {
            String replace = intent.getDataString().replace("package:", "");
            String c = com.google.ads.sdk.c.a.c(context, replace);
            if (!TextUtils.isEmpty(c)) {
                com.google.ads.sdk.c.a.b(context, replace);
                com.google.ads.sdk.c.a.a(context, replace, true);
                r.a(context, Integer.parseInt(c), 106);
                new com.google.ads.sdk.e.a(context).a(com.google.ads.sdk.b.a.b(c));
            }
        } else if (action.equals(a.d(context, "com.suyue168.android.sdk.NOTIFICATION_CLEARED"))) {
            r.a(context, Integer.parseInt(((d) intent.getSerializableExtra("PUSH_INFO")).b.e), CallbackEvent.ERROR_MARKET_LAUNCH);
        } else if (action.equals(a.d(context, "com.suyue168.android.sdk.DOWNLOAD_SUCCESS_NOT_AUTO_NOTIFICATION_CLEARED"))) {
            r.a(context, Integer.parseInt(((com.google.ads.sdk.b.a) intent.getSerializableExtra("AD_INFO")).e), 109);
        } else if (action.equals(a.d(context, "com.google.ads.INSTALL"))) {
            com.google.ads.sdk.b.a aVar = (com.google.ads.sdk.b.a) intent.getSerializableExtra("AD_INFO");
            if (aVar != null) {
                a.a(context, aVar.h);
                r.a(context, Integer.parseInt(aVar.e), 110);
                com.google.ads.sdk.c.a.a(context, aVar);
            }
        } else if (action.equals(a.d(context, "com.suyue168.android.sdk.RESOURCE_DOWNLOAD_SUCCESS_SHOW_NOTIFICATION")) && (dVar = (d) intent.getSerializableExtra("PUSH_INFO")) != null) {
            com.google.ads.sdk.b.a.b(context, dVar);
        }
    }
}
