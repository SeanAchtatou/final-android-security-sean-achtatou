package com.google.ads.sdk.f;

import android.app.NotificationManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.text.TextUtils;
import com.commind.bubbles.donate.Bubble;
import java.util.zip.Adler32;

public final class n {
    static int a = 0;

    public static int a(int i, Context context) {
        switch (i) {
            case Bubble.T_SMS:
                try {
                    return context.getPackageManager().getPackageInfo(context.getPackageName(), 256).applicationInfo.icon;
                } catch (PackageManager.NameNotFoundException e) {
                    e.b("NotificationHelper", "", e);
                    return 17301586;
                }
            case 1:
                return 17301647;
            case 2:
            default:
                return 17301586;
            case Bubble.T_FB_MES:
                return 17301618;
            case Bubble.T_FB_FR:
                return 17301567;
        }
    }

    public static int a(String str, int i) {
        if (TextUtils.isEmpty(str)) {
            e.c("NotificationHelper", "action:getNofiticationID - empty adId");
            return 0;
        }
        Adler32 adler32 = new Adler32();
        adler32.update(str.getBytes());
        int value = (int) adler32.getValue();
        if (value < 0) {
            value = Math.abs(value);
        }
        int i2 = value + (13889152 * i);
        return i2 < 0 ? Math.abs(i2) : i2;
    }

    public static void a(Context context, String str, int i) {
        e.c("NotificationHelper", "action:cleanNotification - adId:" + str);
        ((NotificationManager) context.getSystemService("notification")).cancel(a(str, 0));
    }
}
