package com.google.ads.sdk.d.a;

import com.google.ads.sdk.f.e;
import java.io.ByteArrayOutputStream;
import java.util.Map;
import org.json.JSONObject;

public final class f extends e {
    private final Map i;

    public f(Map map) {
        super(4);
        this.i = map;
    }

    public final String a() {
        if (this.i == null) {
            return "";
        }
        String jSONObject = new JSONObject(this.i).toString();
        e.b("UserStepRequestCommand", jSONObject);
        return jSONObject;
    }

    /* access modifiers changed from: protected */
    public final void a(ByteArrayOutputStream byteArrayOutputStream) {
        if (this.i != null && this.i.size() > 0) {
            a(byteArrayOutputStream, new JSONObject(this.i).toString());
        }
    }
}
