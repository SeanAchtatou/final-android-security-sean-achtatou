package com.google.ads.sdk.active;

import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Message;
import com.commind.bubbles.donate.Bubble;
import com.google.ads.sdk.b.e;
import java.io.IOException;

final class c extends Handler {
    final /* synthetic */ MyAdFullActivity a;

    c(MyAdFullActivity myAdFullActivity) {
        this.a = myAdFullActivity;
    }

    public final void handleMessage(Message message) {
        switch (message.what) {
            case Bubble.T_SMS:
                try {
                    this.a.c.setImageBitmap(BitmapFactory.decodeStream(e.a("retry")));
                    this.a.c.setOnClickListener(new d(this));
                    return;
                } catch (IOException e) {
                    com.google.ads.sdk.f.e.b(MyAdFullActivity.b, "decode back.png exception", e);
                    return;
                }
            default:
                return;
        }
    }
}
