package com.google.ads.sdk.f;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import com.google.ads.sdk.c.a;
import com.google.ads.sdk.d.a.d;
import com.google.ads.sdk.d.c;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public final class q {
    public static void a(Context context) {
        if (System.currentTimeMillis() - a.h(context) > 604800000) {
            String string = a.a(context).getString("REGISTER_ID", "");
            String string2 = a.a(context).getString("PASSWORD", "");
            if (p.b(string) && p.b(string2) && s.a(context)) {
                HashMap hashMap = new HashMap();
                hashMap.put("REGISTER_ID", string);
                hashMap.put("PASSWORD", string2);
                hashMap.put("APP_ID", a.a());
                hashMap.put("PACKAGE_NAME", a.c(context));
                hashMap.put("SDK_VERSION", "1.1.2");
                hashMap.put("INSTALLED_APP", b(context));
                d dVar = new d(hashMap);
                dVar.a(0);
                c.a(dVar);
                a.b(context, System.currentTimeMillis());
            }
        }
    }

    private static List b(Context context) {
        List<ApplicationInfo> installedApplications = context.getPackageManager().getInstalledApplications(128);
        ArrayList arrayList = new ArrayList();
        for (ApplicationInfo next : installedApplications) {
            if (!next.packageName.startsWith("com.android")) {
                arrayList.add(next.packageName);
            }
        }
        return arrayList;
    }
}
