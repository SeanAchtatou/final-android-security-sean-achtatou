package com.google.ads.sdk.active;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import com.google.ads.AdReceiver;
import com.google.ads.sdk.b.d;
import com.google.ads.sdk.e.b;
import com.google.ads.sdk.f.a;
import com.google.ads.sdk.f.p;
import com.google.ads.sdk.f.r;

public class MyAdWallActivity extends Activity {
    private d a;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        Intent intent = getIntent();
        this.a = (d) intent.getSerializableExtra("PUSH_INFO");
        if (this.a == null) {
            String stringExtra = intent.getStringExtra("PUSH_INFO_FOR_JSON");
            if (p.a(stringExtra)) {
                finish();
                return;
            }
            this.a = new d(stringExtra);
            if (this.a == null) {
                finish();
                return;
            }
            r.a(this, Integer.parseInt(this.a.b.e), 113);
        } else {
            r.a(this, Integer.parseInt(this.a.b.e), 114);
        }
        setContentView(new b(this, this.a));
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 4) {
            Intent intent = new Intent(getApplicationContext(), AdReceiver.class);
            intent.setAction(a.d(getApplicationContext(), "com.suyue168.android.sdk.NOTIFICATION_CLEARED"));
            intent.putExtra("PUSH_INFO", this.a);
            sendBroadcast(intent);
        }
        return super.onKeyDown(i, keyEvent);
    }
}
