package com.google.ads.sdk.service;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import com.google.ads.sdk.f.e;
import com.google.ads.sdk.f.k;

public final class a {
    private static final String a = k.a(a.class);
    private static a g;
    /* access modifiers changed from: private */
    public Context b;
    private SharedPreferences c;
    private String d = "1.0.0";
    private String e;
    private String f;

    private a(Context context) {
        this.b = context;
        if (context instanceof Activity) {
            e.a(a, "Callback Activity...");
            Activity activity = (Activity) context;
            this.e = activity.getPackageName();
            this.f = activity.getClass().getName();
        }
        this.c = context.getSharedPreferences("meiline_preference", 0);
        SharedPreferences.Editor edit = this.c.edit();
        edit.putString("VERSION", this.d);
        edit.putString("CALLBACK_ACTIVITY_PACKAGE_NAME", this.e);
        edit.putString("CALLBACK_ACTIVITY_CLASS_NAME", this.f);
        edit.commit();
    }

    public static synchronized a a(Context context) {
        a aVar;
        synchronized (a.class) {
            if (g == null) {
                g = new a(context);
            }
            aVar = g;
        }
        return aVar;
    }

    public final void a() {
        new Thread(new b(this)).start();
    }
}
