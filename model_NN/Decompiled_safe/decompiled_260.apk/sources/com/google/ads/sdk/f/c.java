package com.google.ads.sdk.f;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public final class c extends FilterOutputStream {
    private boolean a;
    private int b;
    private byte[] c;
    private int d;
    private int e;
    private boolean f;
    private byte[] g;
    private boolean h;
    private int i;
    private byte[] j;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public c(OutputStream outputStream, int i2) {
        super(outputStream);
        boolean z = true;
        this.f = (i2 & 8) != 0;
        this.a = (i2 & 1) == 0 ? false : z;
        this.d = this.a ? 3 : 4;
        this.c = new byte[this.d];
        this.b = 0;
        this.e = 0;
        this.h = false;
        this.g = new byte[4];
        this.i = i2;
        this.j = b.b(i2);
    }

    public final void close() {
        if (this.b > 0) {
            if (this.a) {
                this.out.write(b.a(this.c, 0, this.b, this.g, 0, this.i));
                this.b = 0;
            } else {
                throw new IOException("Base64 input not properly padded.");
            }
        }
        super.close();
        this.c = null;
        this.out = null;
    }

    public final void write(int i2) {
        if (this.h) {
            this.out.write(i2);
        } else if (this.a) {
            byte[] bArr = this.c;
            int i3 = this.b;
            this.b = i3 + 1;
            bArr[i3] = (byte) i2;
            if (this.b >= this.d) {
                this.out.write(b.a(this.c, 0, this.d, this.g, 0, this.i));
                this.e += 4;
                if (this.f && this.e >= 76) {
                    this.out.write(10);
                    this.e = 0;
                }
                this.b = 0;
            }
        } else if (this.j[i2 & 127] > -5) {
            byte[] bArr2 = this.c;
            int i4 = this.b;
            this.b = i4 + 1;
            bArr2[i4] = (byte) i2;
            if (this.b >= this.d) {
                this.out.write(this.g, 0, b.b(this.c, 0, this.g, 0, this.i));
                this.b = 0;
            }
        } else if (this.j[i2 & 127] != -5) {
            throw new IOException("Invalid character in Base64 data.");
        }
    }

    public final void write(byte[] bArr, int i2, int i3) {
        if (this.h) {
            this.out.write(bArr, i2, i3);
            return;
        }
        for (int i4 = 0; i4 < i3; i4++) {
            write(bArr[i2 + i4]);
        }
    }
}
