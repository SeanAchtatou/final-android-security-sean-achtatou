package com.google.ads.sdk.f;

import com.google.ads.sdk.d.a;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import oauth.signpost.OAuth;

public final class o {
    private static DateFormat a = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static final int a(byte[] bArr) {
        long j = 0;
        for (int i = 0; i < bArr.length - 1; i++) {
            j += (((long) bArr[i]) & 255) << (((bArr.length - 1) - i) * 8);
        }
        return (int) (j + (((long) bArr[bArr.length - 1]) & 255));
    }

    public static synchronized String a(byte[] bArr, int i, int i2) {
        String str;
        synchronized (o.class) {
            byte[] a2 = a(i2);
            System.arraycopy(bArr, 2, a2, 0, a2.length);
            try {
                str = new String(a2, OAuth.ENCODING);
            } catch (UnsupportedEncodingException e) {
                str = null;
            }
        }
        return str;
    }

    public static byte[] a(int i) {
        byte[] bArr = new byte[i];
        for (int i2 = 0; i2 < bArr.length; i2++) {
            bArr[0] = 0;
        }
        return bArr;
    }

    public static byte[] a(int i, int i2) {
        int i3;
        int i4 = 0;
        byte[] bArr = new byte[4];
        for (int i5 = 0; i5 < 4; i5++) {
            bArr[i5] = (byte) ((i >>> (((bArr.length - 1) - i5) * 8)) & 255);
        }
        byte[] bArr2 = new byte[i2];
        int length = bArr.length - i2;
        if (i2 > bArr.length) {
            i3 = i2 - bArr.length;
            i2 = bArr.length;
        } else {
            i4 = length;
            i3 = 0;
        }
        System.arraycopy(bArr, i4, bArr2, i3, i2);
        return bArr2;
    }

    public static synchronized String b(byte[] bArr, int i, int i2) {
        String str;
        synchronized (o.class) {
            byte[] a2 = a(i2);
            System.arraycopy(bArr, 2, a2, 0, a2.length);
            try {
                str = a.a(a2);
            } catch (Exception e) {
                str = null;
            }
        }
        return str;
    }
}
