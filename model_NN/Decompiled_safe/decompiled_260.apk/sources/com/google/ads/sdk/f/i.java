package com.google.ads.sdk.f;

public final class i {
    public static boolean a = false;

    /* JADX WARNING: Removed duplicated region for block: B:86:0x0129 A[SYNTHETIC, Splitter:B:86:0x0129] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static byte[] a(java.lang.String r7, int r8, long r9) {
        /*
            r3 = 0
            r1 = 1
            r0 = 0
            if (r8 <= 0) goto L_0x0009
            r2 = 10
            if (r8 <= r2) goto L_0x000a
        L_0x0009:
            r8 = r1
        L_0x000a:
            r1 = 200(0xc8, double:9.9E-322)
            int r1 = (r9 > r1 ? 1 : (r9 == r1 ? 0 : -1))
            if (r1 < 0) goto L_0x0017
            r1 = 60000(0xea60, double:2.9644E-319)
            int r1 = (r9 > r1 ? 1 : (r9 == r1 ? 0 : -1))
            if (r1 <= 0) goto L_0x0019
        L_0x0017:
            r9 = 2000(0x7d0, double:9.88E-321)
        L_0x0019:
            java.lang.String r1 = "HttpHelper"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r4 = "action:httpGet - "
            r2.<init>(r4)
            java.lang.StringBuilder r2 = r2.append(r7)
            java.lang.String r2 = r2.toString()
            com.google.ads.sdk.f.e.c(r1, r2)
            org.apache.http.client.methods.HttpGet r4 = new org.apache.http.client.methods.HttpGet     // Catch:{ Exception -> 0x018f }
            r4.<init>(r7)     // Catch:{ Exception -> 0x018f }
            java.lang.String r1 = "Connection"
            java.lang.String r2 = "Close"
            r4.addHeader(r1, r2)
            r2 = r3
        L_0x003a:
            r1 = 1
            com.google.ads.sdk.f.i.a = r1     // Catch:{ Exception -> 0x0089 }
            org.apache.http.params.BasicHttpParams r1 = new org.apache.http.params.BasicHttpParams     // Catch:{ Exception -> 0x0089 }
            r1.<init>()     // Catch:{ Exception -> 0x0089 }
            org.apache.http.HttpVersion r5 = org.apache.http.HttpVersion.HTTP_1_1     // Catch:{ Exception -> 0x0089 }
            org.apache.http.params.HttpProtocolParams.setVersion(r1, r5)     // Catch:{ Exception -> 0x0089 }
            java.lang.String r5 = "UTF-8"
            org.apache.http.params.HttpProtocolParams.setContentCharset(r1, r5)     // Catch:{ Exception -> 0x0089 }
            java.lang.String r5 = "MLA"
            org.apache.http.params.HttpProtocolParams.setUserAgent(r1, r5)     // Catch:{ Exception -> 0x0089 }
            r5 = 1
            org.apache.http.params.HttpConnectionParams.setTcpNoDelay(r1, r5)     // Catch:{ Exception -> 0x0089 }
            r5 = 30000(0x7530, float:4.2039E-41)
            org.apache.http.params.HttpConnectionParams.setConnectionTimeout(r1, r5)     // Catch:{ Exception -> 0x0089 }
            r5 = 30000(0x7530, float:4.2039E-41)
            org.apache.http.params.HttpConnectionParams.setSoTimeout(r1, r5)     // Catch:{ Exception -> 0x0089 }
            org.apache.http.impl.client.DefaultHttpClient r5 = new org.apache.http.impl.client.DefaultHttpClient     // Catch:{ Exception -> 0x0089 }
            r5.<init>(r1)     // Catch:{ Exception -> 0x0089 }
            org.apache.http.HttpResponse r5 = r5.execute(r4)     // Catch:{ Exception -> 0x0089 }
            if (r5 == 0) goto L_0x0091
            org.apache.http.StatusLine r1 = r5.getStatusLine()
            int r1 = r1.getStatusCode()
            r2 = 200(0xc8, float:2.8E-43)
            if (r2 != r1) goto L_0x0136
            org.apache.http.HttpEntity r2 = r5.getEntity()     // Catch:{ Exception -> 0x010a, all -> 0x0124 }
            if (r2 != 0) goto L_0x00ac
            java.lang.String r1 = "HttpHelper"
            java.lang.String r3 = "Unexpected: server responsed NULL"
            com.google.ads.sdk.f.e.c(r1, r3)     // Catch:{ Exception -> 0x018c }
            if (r2 == 0) goto L_0x0088
            r2.consumeContent()     // Catch:{ IOException -> 0x00a3 }
        L_0x0088:
            return r0
        L_0x0089:
            r1 = move-exception
            java.lang.String r5 = "HttpHelper"
            java.lang.String r6 = "http client execute error"
            com.google.ads.sdk.f.e.b(r5, r6, r1)
        L_0x0091:
            int r1 = r2 + 1
            if (r1 < r8) goto L_0x0099
            r4.abort()
            goto L_0x0088
        L_0x0099:
            long r5 = (long) r1
            long r5 = r5 * r9
            java.lang.Thread.sleep(r5)     // Catch:{ InterruptedException -> 0x00a0 }
            r2 = r1
            goto L_0x003a
        L_0x00a0:
            r2 = move-exception
            r2 = r1
            goto L_0x003a
        L_0x00a3:
            r1 = move-exception
            java.lang.String r2 = "HttpHelper"
            java.lang.String r3 = "consumeContent error"
            com.google.ads.sdk.f.e.b(r2, r3, r1)
            goto L_0x0088
        L_0x00ac:
            java.lang.String r1 = "Content-Length"
            org.apache.http.Header r1 = r5.getFirstHeader(r1)     // Catch:{ Exception -> 0x018c }
            if (r1 == 0) goto L_0x00df
            java.lang.String r1 = r1.getValue()     // Catch:{ Exception -> 0x018c }
        L_0x00b8:
            if (r1 == 0) goto L_0x00bf
            int r1 = java.lang.Integer.parseInt(r1)     // Catch:{ Exception -> 0x018c }
            r3 = r1
        L_0x00bf:
            org.apache.http.HttpEntity r1 = r5.getEntity()     // Catch:{ Exception -> 0x018c }
            byte[] r1 = org.apache.http.util.EntityUtils.toByteArray(r1)     // Catch:{ Exception -> 0x018c }
            if (r3 != 0) goto L_0x00e1
            java.lang.String r1 = "HttpHelper"
            java.lang.String r3 = "Unexpected: downloaded bytes content length is 0"
            com.google.ads.sdk.f.e.c(r1, r3)     // Catch:{ Exception -> 0x018c }
            if (r2 == 0) goto L_0x0088
            r2.consumeContent()     // Catch:{ IOException -> 0x00d6 }
            goto L_0x0088
        L_0x00d6:
            r1 = move-exception
            java.lang.String r2 = "HttpHelper"
            java.lang.String r3 = "consumeContent error"
            com.google.ads.sdk.f.e.b(r2, r3, r1)
            goto L_0x0088
        L_0x00df:
            r1 = r0
            goto L_0x00b8
        L_0x00e1:
            int r4 = r1.length     // Catch:{ Exception -> 0x018c }
            if (r4 >= r3) goto L_0x00fa
            java.lang.String r1 = "HttpHelper"
            java.lang.String r3 = "Download bytes failed. Got bytes len < header content length."
            com.google.ads.sdk.f.e.c(r1, r3)     // Catch:{ Exception -> 0x018c }
            if (r2 == 0) goto L_0x0088
            r2.consumeContent()     // Catch:{ IOException -> 0x00f1 }
            goto L_0x0088
        L_0x00f1:
            r1 = move-exception
            java.lang.String r2 = "HttpHelper"
            java.lang.String r3 = "consumeContent error"
            com.google.ads.sdk.f.e.b(r2, r3, r1)
            goto L_0x0088
        L_0x00fa:
            if (r2 == 0) goto L_0x00ff
            r2.consumeContent()     // Catch:{ IOException -> 0x0101 }
        L_0x00ff:
            r0 = r1
            goto L_0x0088
        L_0x0101:
            r0 = move-exception
            java.lang.String r2 = "HttpHelper"
            java.lang.String r3 = "consumeContent error"
            com.google.ads.sdk.f.e.b(r2, r3, r0)
            goto L_0x00ff
        L_0x010a:
            r1 = move-exception
            r2 = r0
        L_0x010c:
            java.lang.String r3 = "HttpHelper"
            java.lang.String r4 = "parse response error"
            com.google.ads.sdk.f.e.b(r3, r4, r1)     // Catch:{ all -> 0x018a }
            if (r2 == 0) goto L_0x0088
            r2.consumeContent()     // Catch:{ IOException -> 0x011a }
            goto L_0x0088
        L_0x011a:
            r1 = move-exception
            java.lang.String r2 = "HttpHelper"
            java.lang.String r3 = "consumeContent error"
            com.google.ads.sdk.f.e.b(r2, r3, r1)
            goto L_0x0088
        L_0x0124:
            r1 = move-exception
            r2 = r0
            r0 = r1
        L_0x0127:
            if (r2 == 0) goto L_0x012c
            r2.consumeContent()     // Catch:{ IOException -> 0x012d }
        L_0x012c:
            throw r0
        L_0x012d:
            r1 = move-exception
            java.lang.String r2 = "HttpHelper"
            java.lang.String r3 = "consumeContent error"
            com.google.ads.sdk.f.e.b(r2, r3, r1)
            goto L_0x012c
        L_0x0136:
            r2 = 400(0x190, float:5.6E-43)
            if (r2 != r1) goto L_0x0150
            java.lang.String r1 = "HttpHelper"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "server response failure - "
            r2.<init>(r3)
            java.lang.StringBuilder r2 = r2.append(r7)
            java.lang.String r2 = r2.toString()
            com.google.ads.sdk.f.e.c(r1, r2)
            goto L_0x0088
        L_0x0150:
            r2 = 404(0x194, float:5.66E-43)
            if (r2 != r1) goto L_0x016a
            java.lang.String r1 = "HttpHelper"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "Request path does not exist: 404 - "
            r2.<init>(r3)
            java.lang.StringBuilder r2 = r2.append(r7)
            java.lang.String r2 = r2.toString()
            com.google.ads.sdk.f.e.c(r1, r2)
            goto L_0x0088
        L_0x016a:
            java.lang.String r2 = "HttpHelper"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "Other wrong response status - "
            r3.<init>(r4)
            java.lang.StringBuilder r1 = r3.append(r1)
            java.lang.String r3 = ", url:"
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.StringBuilder r1 = r1.append(r7)
            java.lang.String r1 = r1.toString()
            com.google.ads.sdk.f.e.c(r2, r1)
            goto L_0x0088
        L_0x018a:
            r0 = move-exception
            goto L_0x0127
        L_0x018c:
            r1 = move-exception
            goto L_0x010c
        L_0x018f:
            r1 = move-exception
            goto L_0x0088
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.ads.sdk.f.i.a(java.lang.String, int, long):byte[]");
    }

    public static byte[] a(String str, int i, long j, int i2) {
        byte[] bArr = null;
        for (int i3 = 0; i3 < 4 && (bArr = a(str, 5, 5000)) == null; i3++) {
        }
        return bArr;
    }
}
