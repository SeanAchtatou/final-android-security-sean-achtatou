package com.google.ads.sdk.f;

public final class h {
    /* JADX WARNING: Removed duplicated region for block: B:46:0x01a5  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean a(java.lang.String r13, byte[] r14, android.content.Context r15) {
        /*
            r12 = 5
            r1 = 0
            boolean r0 = com.google.ads.sdk.f.a.b()
            if (r0 == 0) goto L_0x0198
            android.content.SharedPreferences r2 = android.preference.PreferenceManager.getDefaultSharedPreferences(r15)
            java.lang.String r0 = "dir"
            java.lang.String r3 = ""
            java.lang.String r3 = r2.getString(r0, r3)
            boolean r0 = android.text.TextUtils.isEmpty(r3)
            if (r0 == 0) goto L_0x008d
            java.io.File r0 = android.os.Environment.getExternalStorageDirectory()
            java.lang.String r0 = r0.getAbsolutePath()
            java.lang.String r4 = "/Android/data/"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            java.lang.String r0 = java.lang.String.valueOf(r0)
            r5.<init>(r0)
            java.lang.StringBuilder r0 = r5.append(r4)
            java.lang.String r0 = r0.toString()
            java.io.File r5 = new java.io.File
            r5.<init>(r0)
            boolean r0 = r5.exists()
            if (r0 == 0) goto L_0x0176
            java.util.ArrayList r6 = new java.util.ArrayList
            r6.<init>()
            java.io.File[] r5 = r5.listFiles()
            int r7 = r5.length
            r0 = r1
        L_0x004b:
            if (r0 < r7) goto L_0x012c
            int r0 = r6.size()
            if (r0 <= 0) goto L_0x0157
            int r0 = r0 / 2
            java.lang.Object r0 = r6.get(r0)
            java.lang.String r0 = (java.lang.String) r0
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            java.lang.String r4 = java.lang.String.valueOf(r4)
            r5.<init>(r4)
            java.lang.StringBuilder r0 = r5.append(r0)
            java.lang.String r0 = r0.toString()
        L_0x006c:
            java.lang.String r4 = "UADirectoryUtils"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            java.lang.String r6 = "The target dir: "
            r5.<init>(r6)
            java.lang.StringBuilder r5 = r5.append(r0)
            java.lang.String r5 = r5.toString()
            com.google.ads.sdk.f.e.a(r4, r5)
            android.content.SharedPreferences$Editor r2 = r2.edit()
            java.lang.String r4 = "dir"
            android.content.SharedPreferences$Editor r0 = r2.putString(r4, r0)
            r0.commit()
        L_0x008d:
            java.io.File r0 = android.os.Environment.getExternalStorageDirectory()
            java.lang.String r0 = r0.getAbsolutePath()
            java.io.File r2 = new java.io.File
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r5 = java.lang.String.valueOf(r0)
            r4.<init>(r5)
            java.lang.StringBuilder r4 = r4.append(r3)
            java.lang.String r4 = r4.toString()
            r2.<init>(r4)
            boolean r4 = r2.isDirectory()
            if (r4 != 0) goto L_0x00b4
            r2.mkdirs()
        L_0x00b4:
            java.io.File r2 = new java.io.File
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r5 = java.lang.String.valueOf(r0)
            r4.<init>(r5)
            java.lang.StringBuilder r4 = r4.append(r3)
            java.lang.String r5 = "/1"
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r4 = r4.toString()
            r2.<init>(r4)
            boolean r4 = r2.isDirectory()
            if (r4 != 0) goto L_0x00d9
            r2.mkdirs()
        L_0x00d9:
            java.io.File r2 = new java.io.File
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r0 = java.lang.String.valueOf(r0)
            r4.<init>(r0)
            java.lang.StringBuilder r0 = r4.append(r3)
            java.lang.String r3 = "/2"
            java.lang.StringBuilder r0 = r0.append(r3)
            java.lang.String r0 = r0.toString()
            r2.<init>(r0)
            boolean r0 = r2.isDirectory()
            if (r0 != 0) goto L_0x00fe
            r2.mkdirs()
        L_0x00fe:
            boolean r0 = android.text.TextUtils.isEmpty(r13)
            if (r0 != 0) goto L_0x01a9
            int r0 = r14.length
            if (r0 <= 0) goto L_0x01a9
            boolean r0 = com.google.ads.sdk.f.a.b()
            if (r0 == 0) goto L_0x01a9
            java.io.File r0 = new java.io.File
            r0.<init>(r13)
            boolean r1 = r0.exists()
            if (r1 != 0) goto L_0x011b
            r0.createNewFile()
        L_0x011b:
            r2 = 0
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ all -> 0x01a1 }
            r1.<init>(r0)     // Catch:{ all -> 0x01a1 }
            r1.write(r14)     // Catch:{ all -> 0x01ab }
            r1.flush()     // Catch:{ all -> 0x01ab }
            r1.close()
            r0 = 1
        L_0x012b:
            return r0
        L_0x012c:
            r8 = r5[r0]
            boolean r9 = r8.isDirectory()
            if (r9 == 0) goto L_0x0153
            java.lang.String r9 = r8.getName()
            r6.add(r9)
            java.lang.String r9 = "UADirectoryUtils"
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            java.lang.String r11 = "data dir: "
            r10.<init>(r11)
            java.lang.String r8 = r8.getName()
            java.lang.StringBuilder r8 = r10.append(r8)
            java.lang.String r8 = r8.toString()
            com.google.ads.sdk.f.e.b(r9, r8)
        L_0x0153:
            int r0 = r0 + 1
            goto L_0x004b
        L_0x0157:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r4 = java.lang.String.valueOf(r4)
            r0.<init>(r4)
            java.util.UUID r4 = java.util.UUID.randomUUID()
            java.lang.String r4 = r4.toString()
            java.lang.String r4 = r4.substring(r1, r12)
            java.lang.StringBuilder r0 = r0.append(r4)
            java.lang.String r0 = r0.toString()
            goto L_0x006c
        L_0x0176:
            r5.mkdirs()
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r4 = java.lang.String.valueOf(r4)
            r0.<init>(r4)
            java.util.UUID r4 = java.util.UUID.randomUUID()
            java.lang.String r4 = r4.toString()
            java.lang.String r4 = r4.substring(r1, r12)
            java.lang.StringBuilder r0 = r0.append(r4)
            java.lang.String r0 = r0.toString()
            goto L_0x006c
        L_0x0198:
            java.lang.String r0 = "UADirectoryUtils"
            java.lang.String r2 = "sd card is not found !"
            com.google.ads.sdk.f.e.e(r0, r2)
            goto L_0x00fe
        L_0x01a1:
            r0 = move-exception
            r1 = r2
        L_0x01a3:
            if (r1 == 0) goto L_0x01a8
            r1.close()
        L_0x01a8:
            throw r0
        L_0x01a9:
            r0 = r1
            goto L_0x012b
        L_0x01ab:
            r0 = move-exception
            goto L_0x01a3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.ads.sdk.f.h.a(java.lang.String, byte[], android.content.Context):boolean");
    }
}
