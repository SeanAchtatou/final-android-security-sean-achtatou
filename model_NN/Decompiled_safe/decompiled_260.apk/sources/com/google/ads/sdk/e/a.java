package com.google.ads.sdk.e;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.google.ads.AdReceiver;
import com.google.ads.AdsActivity;
import com.google.ads.sdk.b.d;
import com.google.ads.sdk.f.e;
import com.google.ads.sdk.f.k;
import com.google.ads.sdk.f.n;
import com.google.ads.sdk.f.p;
import com.google.ads.sdk.f.r;

public class a {
    private static final String a = k.a(a.class);
    private Context b;
    private NotificationManager c;

    public a(Context context) {
        this.b = context;
        this.c = (NotificationManager) context.getSystemService("notification");
    }

    public final void a(int i) {
        this.c.cancel(i);
    }

    public final void a(com.google.ads.sdk.b.a aVar, boolean z) {
        int i = aVar.A;
        String str = aVar.q;
        a(i);
        Notification notification = new Notification(17301633, "下载完成,点击进行安装", System.currentTimeMillis());
        notification.icon = 17301633;
        notification.when = System.currentTimeMillis();
        notification.flags = 48;
        notification.defaults = aVar.O;
        Intent intent = new Intent(this.b, AdReceiver.class);
        intent.setAction(com.google.ads.sdk.f.a.d(this.b, "com.google.ads.INSTALL"));
        intent.putExtra("AD_INFO", aVar);
        PendingIntent broadcast = PendingIntent.getBroadcast(this.b, i, intent, 134217728);
        Intent intent2 = new Intent(com.google.ads.sdk.f.a.d(this.b, "com.suyue168.android.sdk.DOWNLOAD_SUCCESS_NOT_AUTO_NOTIFICATION_CLEARED"));
        intent2.putExtra("AD_INFO", aVar);
        notification.deleteIntent = PendingIntent.getBroadcast(this.b, i, intent2, 134217728);
        notification.setLatestEventInfo(this.b, str, "下载完成,点击进行安装", broadcast);
        if (aVar.N) {
            notification.flags = 16;
            com.google.ads.sdk.f.a.a(this.b, aVar.h);
            r.a(this.b, Integer.parseInt(aVar.e), 121);
        }
        this.c.notify(i, notification);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.ads.sdk.c.a.a(android.content.Context, java.lang.String, boolean):void
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.google.ads.sdk.c.a.a(android.content.Context, java.lang.String, java.lang.String):void
      com.google.ads.sdk.c.a.a(android.content.Context, java.lang.String, boolean):void */
    public final void a(d dVar, int i) {
        Bitmap decodeFile;
        e.c(a, "notify()...");
        String str = dVar.b.j;
        String str2 = dVar.b.k;
        int a2 = n.a(dVar.b.e, 0);
        Notification notification = new Notification(n.a(dVar.b.L, this.b), str2, System.currentTimeMillis());
        notification.defaults = dVar.b.O;
        notification.flags |= 48;
        Intent intent = new Intent(this.b, AdsActivity.class);
        intent.putExtra("PUSH_INFO", dVar.a);
        intent.setFlags(268435456);
        intent.setFlags(8388608);
        intent.setFlags(1073741824);
        intent.setFlags(536870912);
        intent.setFlags(67108864);
        PendingIntent activity = PendingIntent.getActivity(this.b, a2, intent, 134217728);
        Intent intent2 = new Intent(String.valueOf(this.b.getPackageName()) + ".com.suyue168.android.sdk.NOTIFICATION_CLEARED");
        intent2.putExtra("PUSH_INFO", dVar);
        notification.deleteIntent = PendingIntent.getBroadcast(this.b, 0, intent2, 0);
        notification.setLatestEventInfo(this.b, str, str2, activity);
        String e = com.google.ads.sdk.c.a.e(this.b, dVar.b.r);
        if (!(p.a(e) || dVar.b.M == 0 || (decodeFile = BitmapFactory.decodeFile(e)) == null)) {
            notification.contentView.setImageViewBitmap(16908294, decodeFile);
            this.c.cancel(a2);
        }
        this.c.notify(a2, notification);
        if (!com.google.ads.sdk.f.a.b(this.b, dVar.b.m) || !dVar.b.p) {
            r.a(this.b, Integer.parseInt(dVar.b.e), 107);
            return;
        }
        com.google.ads.sdk.c.a.a(this.b, dVar.b.m, false);
        r.a(this.b, Integer.parseInt(dVar.b.e), 116);
    }
}
