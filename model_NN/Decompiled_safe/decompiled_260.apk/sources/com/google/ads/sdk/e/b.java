package com.google.ads.sdk.e;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.ads.sdk.b.a;
import com.google.ads.sdk.b.d;
import com.google.ads.sdk.b.e;
import com.google.ads.sdk.f.g;
import com.google.ads.sdk.f.p;
import java.io.IOException;
import java.util.ArrayList;

public final class b extends LinearLayout {
    /* access modifiers changed from: private */
    public Context a;
    /* access modifiers changed from: private */
    public d b;
    private LinearLayout c;
    private ArrayList d = new ArrayList();
    private f e = new f(this);
    private LinearLayout f;
    private LinearLayout g;

    public b(Context context, d dVar) {
        super(context);
        this.a = context;
        this.b = dVar;
        int i = 1;
        for (a aVar : this.b.d) {
            if (aVar.E) {
                String e2 = com.google.ads.sdk.c.a.e(this.a, aVar.r);
                if (com.google.ads.sdk.f.a.c(e2)) {
                    this.e.b = e2;
                }
                String e3 = com.google.ads.sdk.c.a.e(this.a, aVar.F);
                if (com.google.ads.sdk.f.a.c(e2)) {
                    this.e.c = e3;
                }
                this.e.d = aVar.q;
                this.e.e = aVar.v;
                this.e.f = aVar.z;
                this.e.h = aVar;
            } else {
                f fVar = new f(this);
                String e4 = com.google.ads.sdk.c.a.e(this.a, aVar.r);
                if (com.google.ads.sdk.f.a.c(e4)) {
                    fVar.b = e4;
                }
                String e5 = com.google.ads.sdk.c.a.e(this.a, aVar.w);
                if (com.google.ads.sdk.f.a.c(e4)) {
                    fVar.c = e5;
                }
                fVar.a = i;
                fVar.d = aVar.q;
                fVar.f = aVar.z;
                fVar.h = aVar;
                this.d.add(fVar);
                i++;
            }
        }
        setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        try {
            setBackgroundDrawable((BitmapDrawable) BitmapDrawable.createFromStream(e.a("default_background"), "default_background.jpg"));
        } catch (IOException e6) {
            e6.printStackTrace();
        }
        setOrientation(1);
        setGravity(1);
        this.c = new LinearLayout(this.a);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        this.c.setOrientation(1);
        this.c.setPadding(30, g.a(this.a, 10), g.a(this.a, 10), 0);
        addView(this.c, layoutParams);
        ImageView imageView = new ImageView(this.a);
        try {
            imageView.setImageBitmap(BitmapFactory.decodeStream(e.a("shotcut")));
        } catch (IOException e7) {
            com.google.ads.sdk.f.e.b("PopularTodayView", "decode back.png exception", e7);
        }
        imageView.setOnClickListener(new c(this));
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-2, -2);
        layoutParams2.gravity = 5;
        this.c.addView(imageView, layoutParams2);
        TextView textView = new TextView(this.a);
        int a2 = g.a(this.a, 30);
        textView.setTextSize((float) (a2 < 24 ? 24 : a2));
        if (p.a(this.b.b.K)) {
            textView.setText("今日热门");
        } else {
            textView.setText(this.b.b.K);
        }
        textView.setTextColor(-1);
        this.c.addView(textView, new LinearLayout.LayoutParams(-2, -2));
        a();
        b();
    }

    /* JADX WARNING: Removed duplicated region for block: B:34:0x01fb  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a() {
        /*
            r12 = this;
            r10 = 200(0xc8, float:2.8E-43)
            r6 = 420(0x1a4, float:5.89E-43)
            r7 = -2
            r9 = 10
            r8 = 0
            android.widget.RelativeLayout r1 = new android.widget.RelativeLayout
            android.content.Context r0 = r12.a
            r1.<init>(r0)
            android.widget.LinearLayout$LayoutParams r0 = new android.widget.LinearLayout$LayoutParams
            android.content.Context r2 = r12.a
            int r2 = com.google.ads.sdk.f.g.a(r2, r6)
            android.content.Context r3 = r12.a
            int r3 = com.google.ads.sdk.f.g.a(r3, r10)
            r0.<init>(r2, r3)
            r12.addView(r1, r0)
            com.google.ads.sdk.e.f r0 = r12.e
            r1.setTag(r0)
            com.google.ads.sdk.e.d r0 = new com.google.ads.sdk.e.d
            r0.<init>(r12)
            r1.setOnClickListener(r0)
            android.widget.ImageView r2 = new android.widget.ImageView
            android.content.Context r0 = r12.a
            r2.<init>(r0)
            android.widget.ImageView$ScaleType r0 = android.widget.ImageView.ScaleType.FIT_XY
            r2.setScaleType(r0)
            com.google.ads.sdk.e.f r0 = r12.e     // Catch:{ IOException -> 0x01cc }
            java.lang.String r0 = r0.c     // Catch:{ IOException -> 0x01cc }
            boolean r0 = com.google.ads.sdk.f.p.a(r0)     // Catch:{ IOException -> 0x01cc }
            if (r0 != 0) goto L_0x01bd
            com.google.ads.sdk.e.f r0 = r12.e     // Catch:{ IOException -> 0x01cc }
            java.lang.String r0 = r0.c     // Catch:{ IOException -> 0x01cc }
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeFile(r0)     // Catch:{ IOException -> 0x01cc }
            r2.setImageBitmap(r0)     // Catch:{ IOException -> 0x01cc }
        L_0x0051:
            android.widget.RelativeLayout$LayoutParams r0 = new android.widget.RelativeLayout$LayoutParams
            android.content.Context r3 = r12.a
            int r3 = com.google.ads.sdk.f.g.a(r3, r6)
            android.content.Context r4 = r12.a
            int r4 = com.google.ads.sdk.f.g.a(r4, r10)
            r0.<init>(r3, r4)
            android.content.Context r3 = r12.a
            int r3 = com.google.ads.sdk.f.g.a(r3, r9)
            r0.setMargins(r8, r3, r8, r8)
            r1.addView(r2, r0)
            android.widget.RelativeLayout r2 = new android.widget.RelativeLayout
            android.content.Context r0 = r12.a
            r2.<init>(r0)
            android.widget.RelativeLayout$LayoutParams r0 = new android.widget.RelativeLayout$LayoutParams
            android.content.Context r3 = r12.a
            int r3 = com.google.ads.sdk.f.g.a(r3, r6)
            android.content.Context r4 = r12.a
            r5 = 88
            int r4 = com.google.ads.sdk.f.g.a(r4, r5)
            r0.<init>(r3, r4)
            r3 = 12
            r0.addRule(r3)
            r1.addView(r2, r0)
            android.widget.LinearLayout r3 = new android.widget.LinearLayout
            android.content.Context r0 = r12.a
            r3.<init>(r0)
            int r0 = android.graphics.Color.argb(r10, r8, r8, r8)
            r3.setBackgroundColor(r0)
            r0 = 1
            r3.setOrientation(r0)
            android.widget.RelativeLayout$LayoutParams r0 = new android.widget.RelativeLayout$LayoutParams
            android.content.Context r1 = r12.a
            int r1 = com.google.ads.sdk.f.g.a(r1, r6)
            android.content.Context r4 = r12.a
            r5 = 70
            int r4 = com.google.ads.sdk.f.g.a(r4, r5)
            r0.<init>(r1, r4)
            r1 = 12
            r0.addRule(r1)
            android.content.Context r1 = r12.a
            r4 = 90
            int r1 = com.google.ads.sdk.f.g.a(r1, r4)
            r3.setPadding(r1, r8, r8, r8)
            r2.addView(r3, r0)
            android.widget.ImageView r1 = new android.widget.ImageView
            android.content.Context r0 = r12.a
            r1.<init>(r0)
            android.widget.ImageView$ScaleType r0 = android.widget.ImageView.ScaleType.FIT_XY
            r1.setScaleType(r0)
            com.google.ads.sdk.e.f r0 = r12.e     // Catch:{ IOException -> 0x01e5 }
            java.lang.String r0 = r0.b     // Catch:{ IOException -> 0x01e5 }
            boolean r0 = com.google.ads.sdk.f.p.a(r0)     // Catch:{ IOException -> 0x01e5 }
            if (r0 != 0) goto L_0x01d6
            com.google.ads.sdk.e.f r0 = r12.e     // Catch:{ IOException -> 0x01e5 }
            java.lang.String r0 = r0.b     // Catch:{ IOException -> 0x01e5 }
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeFile(r0)     // Catch:{ IOException -> 0x01e5 }
            r1.setImageBitmap(r0)     // Catch:{ IOException -> 0x01e5 }
        L_0x00e9:
            android.widget.RelativeLayout$LayoutParams r0 = new android.widget.RelativeLayout$LayoutParams
            android.content.Context r4 = r12.a
            r5 = 72
            int r4 = com.google.ads.sdk.f.g.a(r4, r5)
            android.content.Context r5 = r12.a
            r6 = 72
            int r5 = com.google.ads.sdk.f.g.a(r5, r6)
            r0.<init>(r4, r5)
            android.content.Context r4 = r12.a
            int r4 = com.google.ads.sdk.f.g.a(r4, r9)
            android.content.Context r5 = r12.a
            int r5 = com.google.ads.sdk.f.g.a(r5, r9)
            r0.setMargins(r4, r8, r5, r8)
            r2.addView(r1, r0)
            android.widget.LinearLayout r0 = new android.widget.LinearLayout
            android.content.Context r1 = r12.a
            r0.<init>(r1)
            r0.setOrientation(r8)
            r1 = 16
            r0.setGravity(r1)
            android.widget.LinearLayout$LayoutParams r1 = new android.widget.LinearLayout$LayoutParams
            r1.<init>(r7, r7)
            r3.addView(r0, r1)
            android.widget.TextView r2 = new android.widget.TextView
            android.content.Context r4 = r12.a
            r2.<init>(r4)
            com.google.ads.sdk.e.f r4 = r12.e
            java.lang.String r4 = r4.d
            r2.setText(r4)
            r2.setSingleLine()
            r4 = -1
            r2.setTextColor(r4)
            r4 = 1098907648(0x41800000, float:16.0)
            r2.setTextSize(r4)
            r0.addView(r2)
            android.widget.LinearLayout r4 = new android.widget.LinearLayout
            android.content.Context r2 = r12.a
            r4.<init>(r2)
            r4.setOrientation(r8)
            android.content.Context r2 = r12.a
            int r2 = com.google.ads.sdk.f.g.a(r2, r9)
            r4.setPadding(r2, r8, r8, r8)
            r0.addView(r4, r1)
            r1 = 0
            r2 = 0
            android.widget.LinearLayout$LayoutParams r5 = new android.widget.LinearLayout$LayoutParams
            r5.<init>(r7, r7)
            java.lang.String r0 = "star_full"
            java.io.InputStream r0 = com.google.ads.sdk.b.e.a(r0)     // Catch:{ IOException -> 0x01ef }
            android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeStream(r0)     // Catch:{ IOException -> 0x01ef }
            java.lang.String r0 = "star_blank"
            java.io.InputStream r0 = com.google.ads.sdk.b.e.a(r0)     // Catch:{ IOException -> 0x01ef }
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeStream(r0)     // Catch:{ IOException -> 0x01ef }
            if (r1 == 0) goto L_0x018f
            android.content.Context r2 = r12.a     // Catch:{ IOException -> 0x0215 }
            int r6 = r1.getWidth()     // Catch:{ IOException -> 0x0215 }
            int r2 = com.google.ads.sdk.f.g.a(r2, r6)     // Catch:{ IOException -> 0x0215 }
            r5.width = r2     // Catch:{ IOException -> 0x0215 }
            android.content.Context r2 = r12.a     // Catch:{ IOException -> 0x0215 }
            int r6 = r1.getHeight()     // Catch:{ IOException -> 0x0215 }
            int r2 = com.google.ads.sdk.f.g.a(r2, r6)     // Catch:{ IOException -> 0x0215 }
            r5.height = r2     // Catch:{ IOException -> 0x0215 }
        L_0x018f:
            r2 = 1
        L_0x0190:
            r6 = 5
            if (r2 <= r6) goto L_0x01fb
            android.widget.TextView r0 = new android.widget.TextView
            android.content.Context r1 = r12.a
            r0.<init>(r1)
            r1 = -1
            r0.setTextColor(r1)
            r1 = 1091567616(0x41100000, float:9.0)
            r0.setTextSize(r1)
            r0.setPadding(r8, r8, r9, r8)
            r1 = 2
            r0.setMaxLines(r1)
            r0.setMaxEms(r9)
            android.text.TextUtils$TruncateAt r1 = android.text.TextUtils.TruncateAt.END
            r0.setEllipsize(r1)
            com.google.ads.sdk.e.f r1 = r12.e
            java.lang.String r1 = r1.e
            r0.setText(r1)
            r3.addView(r0)
            return
        L_0x01bd:
            java.lang.String r0 = "image_default_ls"
            java.io.InputStream r0 = com.google.ads.sdk.b.e.a(r0)     // Catch:{ IOException -> 0x01cc }
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeStream(r0)     // Catch:{ IOException -> 0x01cc }
            r2.setImageBitmap(r0)     // Catch:{ IOException -> 0x01cc }
            goto L_0x0051
        L_0x01cc:
            r0 = move-exception
            java.lang.String r3 = "PopularTodayView"
            java.lang.String r4 = "decode default_preview.png exception"
            com.google.ads.sdk.f.e.b(r3, r4, r0)
            goto L_0x0051
        L_0x01d6:
            java.lang.String r0 = "default_icon"
            java.io.InputStream r0 = com.google.ads.sdk.b.e.a(r0)     // Catch:{ IOException -> 0x01e5 }
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeStream(r0)     // Catch:{ IOException -> 0x01e5 }
            r1.setImageBitmap(r0)     // Catch:{ IOException -> 0x01e5 }
            goto L_0x00e9
        L_0x01e5:
            r0 = move-exception
            java.lang.String r4 = "PopularTodayView"
            java.lang.String r5 = "decode app_icon.png exception"
            com.google.ads.sdk.f.e.b(r4, r5, r0)
            goto L_0x00e9
        L_0x01ef:
            r0 = move-exception
            r11 = r0
            r0 = r2
            r2 = r11
        L_0x01f3:
            java.lang.String r6 = "PopularTodayView"
            java.lang.String r7 = "decode star.png exception"
            com.google.ads.sdk.f.e.b(r6, r7, r2)
            goto L_0x018f
        L_0x01fb:
            android.widget.ImageView r6 = new android.widget.ImageView
            android.content.Context r7 = r12.a
            r6.<init>(r7)
            com.google.ads.sdk.e.f r7 = r12.e
            int r7 = r7.f
            if (r2 > r7) goto L_0x0211
            r6.setImageBitmap(r1)
        L_0x020b:
            r4.addView(r6, r5)
            int r2 = r2 + 1
            goto L_0x0190
        L_0x0211:
            r6.setImageBitmap(r0)
            goto L_0x020b
        L_0x0215:
            r2 = move-exception
            goto L_0x01f3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.ads.sdk.e.b.a():void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:41:0x021c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(com.google.ads.sdk.e.f r14) {
        /*
            r13 = this;
            r11 = 10
            r3 = 1
            r10 = -2
            r7 = 200(0xc8, float:2.8E-43)
            r9 = 0
            if (r14 == 0) goto L_0x0166
            android.widget.RelativeLayout r4 = new android.widget.RelativeLayout
            android.content.Context r0 = r13.a
            r4.<init>(r0)
            int r0 = r14.a
            switch(r0) {
                case 1: goto L_0x0167;
                case 2: goto L_0x018a;
                case 3: goto L_0x01a2;
                case 4: goto L_0x01c5;
                default: goto L_0x0015;
            }
        L_0x0015:
            android.widget.ImageView r1 = new android.widget.ImageView
            android.content.Context r0 = r13.a
            r1.<init>(r0)
            android.widget.ImageView$ScaleType r0 = android.widget.ImageView.ScaleType.MATRIX
            r1.setScaleType(r0)
            java.lang.String r0 = r14.c     // Catch:{ IOException -> 0x01ec }
            boolean r0 = com.google.ads.sdk.f.p.a(r0)     // Catch:{ IOException -> 0x01ec }
            if (r0 != 0) goto L_0x01dd
            java.lang.String r0 = r14.c     // Catch:{ IOException -> 0x01ec }
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeFile(r0)     // Catch:{ IOException -> 0x01ec }
            r1.setImageBitmap(r0)     // Catch:{ IOException -> 0x01ec }
        L_0x0032:
            android.widget.RelativeLayout$LayoutParams r0 = new android.widget.RelativeLayout$LayoutParams
            android.content.Context r2 = r13.a
            int r2 = com.google.ads.sdk.f.g.a(r2, r7)
            android.content.Context r5 = r13.a
            int r5 = com.google.ads.sdk.f.g.a(r5, r7)
            r0.<init>(r2, r5)
            r4.addView(r1, r0)
            android.widget.RelativeLayout r1 = new android.widget.RelativeLayout
            android.content.Context r0 = r13.a
            r1.<init>(r0)
            android.widget.RelativeLayout$LayoutParams r0 = new android.widget.RelativeLayout$LayoutParams
            android.content.Context r2 = r13.a
            r5 = 420(0x1a4, float:5.89E-43)
            int r2 = com.google.ads.sdk.f.g.a(r2, r5)
            android.content.Context r5 = r13.a
            r6 = 88
            int r5 = com.google.ads.sdk.f.g.a(r5, r6)
            r0.<init>(r2, r5)
            r2 = 12
            r0.addRule(r2)
            r4.addView(r1, r0)
            android.widget.LinearLayout r2 = new android.widget.LinearLayout
            android.content.Context r0 = r13.a
            r2.<init>(r0)
            int r0 = android.graphics.Color.argb(r7, r9, r9, r9)
            r2.setBackgroundColor(r0)
            r2.setOrientation(r3)
            android.widget.RelativeLayout$LayoutParams r0 = new android.widget.RelativeLayout$LayoutParams
            android.content.Context r5 = r13.a
            r6 = 420(0x1a4, float:5.89E-43)
            int r5 = com.google.ads.sdk.f.g.a(r5, r6)
            android.content.Context r6 = r13.a
            r7 = 70
            int r6 = com.google.ads.sdk.f.g.a(r6, r7)
            r0.<init>(r5, r6)
            r5 = 12
            r0.addRule(r5)
            r5 = 16
            r2.setGravity(r5)
            android.content.Context r5 = r13.a
            r6 = 90
            int r5 = com.google.ads.sdk.f.g.a(r5, r6)
            r2.setPadding(r5, r9, r9, r9)
            r1.addView(r2, r0)
            android.widget.ImageView r5 = new android.widget.ImageView
            android.content.Context r0 = r13.a
            r5.<init>(r0)
            android.widget.ImageView$ScaleType r0 = android.widget.ImageView.ScaleType.FIT_XY
            r5.setScaleType(r0)
            java.lang.String r0 = r14.b     // Catch:{ IOException -> 0x0205 }
            boolean r0 = com.google.ads.sdk.f.p.a(r0)     // Catch:{ IOException -> 0x0205 }
            if (r0 != 0) goto L_0x01f6
            java.lang.String r0 = r14.b     // Catch:{ IOException -> 0x0205 }
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeFile(r0)     // Catch:{ IOException -> 0x0205 }
            r5.setImageBitmap(r0)     // Catch:{ IOException -> 0x0205 }
        L_0x00c5:
            android.widget.RelativeLayout$LayoutParams r0 = new android.widget.RelativeLayout$LayoutParams
            android.content.Context r6 = r13.a
            r7 = 72
            int r6 = com.google.ads.sdk.f.g.a(r6, r7)
            android.content.Context r7 = r13.a
            r8 = 72
            int r7 = com.google.ads.sdk.f.g.a(r7, r8)
            r0.<init>(r6, r7)
            android.content.Context r6 = r13.a
            int r6 = com.google.ads.sdk.f.g.a(r6, r11)
            android.content.Context r7 = r13.a
            int r7 = com.google.ads.sdk.f.g.a(r7, r11)
            r0.setMargins(r6, r9, r7, r9)
            r1.addView(r5, r0)
            android.widget.TextView r0 = new android.widget.TextView
            android.content.Context r1 = r13.a
            r0.<init>(r1)
            java.lang.String r1 = r14.d
            r0.setText(r1)
            r1 = -1
            r0.setTextColor(r1)
            r0.setSingleLine()
            r1 = 6
            r0.setMaxEms(r1)
            android.text.TextUtils$TruncateAt r1 = android.text.TextUtils.TruncateAt.END
            r0.setEllipsize(r1)
            r1 = 1094713344(0x41400000, float:12.0)
            r0.setTextSize(r1)
            r2.addView(r0)
            android.widget.LinearLayout$LayoutParams r0 = new android.widget.LinearLayout$LayoutParams
            r0.<init>(r10, r10)
            android.widget.LinearLayout r5 = new android.widget.LinearLayout
            android.content.Context r1 = r13.a
            r5.<init>(r1)
            r5.setOrientation(r9)
            r2.addView(r5, r0)
            r1 = 0
            r2 = 0
            android.widget.LinearLayout$LayoutParams r6 = new android.widget.LinearLayout$LayoutParams
            r6.<init>(r10, r10)
            java.lang.String r0 = "star_full"
            java.io.InputStream r0 = com.google.ads.sdk.b.e.a(r0)     // Catch:{ IOException -> 0x020f }
            android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeStream(r0)     // Catch:{ IOException -> 0x020f }
            java.lang.String r0 = "star_blank"
            java.io.InputStream r0 = com.google.ads.sdk.b.e.a(r0)     // Catch:{ IOException -> 0x020f }
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeStream(r0)     // Catch:{ IOException -> 0x020f }
            if (r1 == 0) goto L_0x0157
            android.content.Context r2 = r13.a     // Catch:{ IOException -> 0x0235 }
            int r7 = r1.getWidth()     // Catch:{ IOException -> 0x0235 }
            int r2 = com.google.ads.sdk.f.g.a(r2, r7)     // Catch:{ IOException -> 0x0235 }
            r6.width = r2     // Catch:{ IOException -> 0x0235 }
            android.content.Context r2 = r13.a     // Catch:{ IOException -> 0x0235 }
            int r7 = r1.getHeight()     // Catch:{ IOException -> 0x0235 }
            int r2 = com.google.ads.sdk.f.g.a(r2, r7)     // Catch:{ IOException -> 0x0235 }
            r6.height = r2     // Catch:{ IOException -> 0x0235 }
        L_0x0157:
            r2 = r3
        L_0x0158:
            r3 = 5
            if (r2 <= r3) goto L_0x021c
            r4.setTag(r14)
            com.google.ads.sdk.e.e r0 = new com.google.ads.sdk.e.e
            r0.<init>(r13)
            r4.setOnClickListener(r0)
        L_0x0166:
            return
        L_0x0167:
            android.widget.LinearLayout$LayoutParams r0 = new android.widget.LinearLayout$LayoutParams
            android.content.Context r1 = r13.a
            int r1 = com.google.ads.sdk.f.g.a(r1, r7)
            android.content.Context r2 = r13.a
            int r2 = com.google.ads.sdk.f.g.a(r2, r7)
            r0.<init>(r1, r2)
            android.content.Context r1 = r13.a
            r2 = 20
            int r1 = com.google.ads.sdk.f.g.a(r1, r2)
            r0.setMargins(r9, r9, r1, r9)
            android.widget.LinearLayout r1 = r13.f
            r1.addView(r4, r0)
            goto L_0x0015
        L_0x018a:
            android.widget.LinearLayout$LayoutParams r0 = new android.widget.LinearLayout$LayoutParams
            android.content.Context r1 = r13.a
            int r1 = com.google.ads.sdk.f.g.a(r1, r7)
            android.content.Context r2 = r13.a
            int r2 = com.google.ads.sdk.f.g.a(r2, r7)
            r0.<init>(r1, r2)
            android.widget.LinearLayout r1 = r13.f
            r1.addView(r4, r0)
            goto L_0x0015
        L_0x01a2:
            android.widget.LinearLayout$LayoutParams r0 = new android.widget.LinearLayout$LayoutParams
            android.content.Context r1 = r13.a
            int r1 = com.google.ads.sdk.f.g.a(r1, r7)
            android.content.Context r2 = r13.a
            int r2 = com.google.ads.sdk.f.g.a(r2, r7)
            r0.<init>(r1, r2)
            android.content.Context r1 = r13.a
            r2 = 20
            int r1 = com.google.ads.sdk.f.g.a(r1, r2)
            r0.setMargins(r9, r9, r1, r9)
            android.widget.LinearLayout r1 = r13.g
            r1.addView(r4, r0)
            goto L_0x0015
        L_0x01c5:
            android.widget.LinearLayout$LayoutParams r0 = new android.widget.LinearLayout$LayoutParams
            android.content.Context r1 = r13.a
            int r1 = com.google.ads.sdk.f.g.a(r1, r7)
            android.content.Context r2 = r13.a
            int r2 = com.google.ads.sdk.f.g.a(r2, r7)
            r0.<init>(r1, r2)
            android.widget.LinearLayout r1 = r13.g
            r1.addView(r4, r0)
            goto L_0x0015
        L_0x01dd:
            java.lang.String r0 = "image_default_s"
            java.io.InputStream r0 = com.google.ads.sdk.b.e.a(r0)     // Catch:{ IOException -> 0x01ec }
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeStream(r0)     // Catch:{ IOException -> 0x01ec }
            r1.setImageBitmap(r0)     // Catch:{ IOException -> 0x01ec }
            goto L_0x0032
        L_0x01ec:
            r0 = move-exception
            java.lang.String r2 = "PopularTodayView"
            java.lang.String r5 = "decode default_preview.png exception"
            com.google.ads.sdk.f.e.b(r2, r5, r0)
            goto L_0x0032
        L_0x01f6:
            java.lang.String r0 = "default_icon"
            java.io.InputStream r0 = com.google.ads.sdk.b.e.a(r0)     // Catch:{ IOException -> 0x0205 }
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeStream(r0)     // Catch:{ IOException -> 0x0205 }
            r5.setImageBitmap(r0)     // Catch:{ IOException -> 0x0205 }
            goto L_0x00c5
        L_0x0205:
            r0 = move-exception
            java.lang.String r6 = "PopularTodayView"
            java.lang.String r7 = "decode app_icon.png exception"
            com.google.ads.sdk.f.e.b(r6, r7, r0)
            goto L_0x00c5
        L_0x020f:
            r0 = move-exception
            r12 = r0
            r0 = r2
            r2 = r12
        L_0x0213:
            java.lang.String r7 = "PopularTodayView"
            java.lang.String r8 = "decode star.png exception"
            com.google.ads.sdk.f.e.b(r7, r8, r2)
            goto L_0x0157
        L_0x021c:
            android.widget.ImageView r3 = new android.widget.ImageView
            android.content.Context r7 = r13.a
            r3.<init>(r7)
            int r7 = r14.f
            if (r2 > r7) goto L_0x0231
            r3.setImageBitmap(r1)
        L_0x022a:
            r5.addView(r3, r6)
            int r2 = r2 + 1
            goto L_0x0158
        L_0x0231:
            r3.setImageBitmap(r0)
            goto L_0x022a
        L_0x0235:
            r2 = move-exception
            goto L_0x0213
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.ads.sdk.e.b.a(com.google.ads.sdk.e.f):void");
    }

    private void b() {
        int i = 0;
        this.f = new LinearLayout(this.a);
        this.f.setGravity(16);
        this.g = new LinearLayout(this.a);
        this.g.setGravity(16);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(g.a(this.a, 420), g.a(this.a, 200));
        layoutParams.setMargins(0, g.a(this.a, 20), 0, 0);
        addView(this.f, layoutParams);
        addView(this.g, layoutParams);
        while (true) {
            int i2 = i;
            if (i2 < this.d.size() && i2 < 4) {
                a((f) this.d.get(i2));
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
    }

    /* access modifiers changed from: protected */
    public final void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
    }
}
