package com.google.ads.sdk.active;

import com.google.ads.sdk.download.c;
import com.google.ads.sdk.f.e;
import com.google.ads.sdk.f.r;

final class a implements c {
    final /* synthetic */ MyAdDownService a;
    private final /* synthetic */ int b;

    a(MyAdDownService myAdDownService, int i) {
        this.a = myAdDownService;
        this.b = i;
    }

    public final void a(int i) {
        this.a.c.cancel(this.b);
        if (com.google.ads.sdk.download.a.a(i)) {
            this.a.d.a = true;
            r.a(this.a, Integer.parseInt(this.a.d.e), 105);
        } else {
            r.a(this.a, Integer.parseInt(this.a.d.e), 122);
            MyAdDownService.b(this.a.getApplicationContext());
        }
        this.a.d.b = true;
        MyAdDownService.a(this.a, this.b, this.a.d, i);
    }

    public final void a(long j, long j2) {
        e.c("AdDownService", "pecent:" + ((int) ((((float) j) / ((float) j2)) * 100.0f)) + ", downloaded:" + j + ", total:" + j2);
        MyAdDownService.a(this.a, this.a.d, this.b, j, j2);
    }

    public final void a(String str) {
        this.a.d.c = true;
        MyAdDownService.a.remove(this.a.d);
        this.a.d.h = str;
        this.a.c.cancel(this.b);
        MyAdDownService.a(this.a, this.a.d);
        com.google.ads.sdk.c.a.a(this.a.getApplicationContext(), this.a.d);
        r.a(this.a.getApplicationContext(), Integer.parseInt(this.a.d.e), 104);
    }
}
