package com.google.ads.sdk.b;

import com.google.ads.sdk.f.e;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class d implements Serializable {
    public String a;
    public a b;
    public int c = 0;
    public List d = new ArrayList();
    public boolean e = true;

    public d(String str) {
        boolean z = true;
        try {
            JSONObject jSONObject = new JSONObject(str);
            if (!jSONObject.isNull("NEXT_AD_CHECK_TIME")) {
                this.c = jSONObject.getInt("NEXT_AD_CHECK_TIME");
            }
            if (!jSONObject.isNull("AD_COUNT")) {
                this.e = jSONObject.getInt("AD_COUNT") <= 0 ? false : z;
            }
            if (this.e) {
                this.a = str;
                this.b = a.a(jSONObject.optString("CURRENT_AD_INFO"));
                JSONArray optJSONArray = jSONObject.optJSONArray("TODAY_AD_POPULAR");
                if (optJSONArray != null && optJSONArray.length() > 0) {
                    for (int i = 0; i < optJSONArray.length(); i++) {
                        this.d.add(a.a(optJSONArray.getString(i)));
                    }
                }
            }
        } catch (JSONException e2) {
            e.b("AdPush", "parse adPush json error", e2);
        }
    }

    public final void a(a aVar) {
        this.b = aVar;
        try {
            JSONObject jSONObject = new JSONObject(this.a);
            jSONObject.put("CURRENT_AD_INFO", aVar.H);
            this.a = jSONObject.toString();
        } catch (JSONException e2) {
            e.b("AdPush", "parse adPush json error", e2);
        }
    }
}
