package com.google.ads.sdk.e;

import android.content.Intent;
import android.view.View;
import com.google.ads.AdWallActivity;
import com.google.ads.AdsActivity;
import com.google.ads.sdk.f.r;

final class i implements View.OnClickListener {
    final /* synthetic */ g a;

    i(g gVar) {
        this.a = gVar;
    }

    public final void onClick(View view) {
        if (this.a.d.d.size() > 0) {
            Intent intent = new Intent(this.a.c, AdWallActivity.class);
            intent.putExtra("PUSH_INFO", this.a.d);
            this.a.c.startActivity(intent);
        } else {
            r.a(this.a.c, Integer.parseInt(this.a.d.b.e), 120);
        }
        ((AdsActivity) this.a.c).finish();
    }
}
