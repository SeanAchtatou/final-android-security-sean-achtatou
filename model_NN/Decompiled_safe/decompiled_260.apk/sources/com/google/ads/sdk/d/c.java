package com.google.ads.sdk.d;

import android.content.Context;
import com.google.ads.sdk.c.a;
import com.google.ads.sdk.d.a.e;
import com.google.ads.sdk.f.p;
import java.util.Iterator;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public final class c implements Runnable {
    private static BlockingQueue a = new LinkedBlockingQueue();
    private boolean b = false;
    private Context c;
    private String d;
    private int e;
    private boolean f = true;

    public c(Context context) {
        this.c = context;
        a();
    }

    private void a() {
        String f2 = a.f(this.c);
        if (!p.b(f2)) {
            return;
        }
        if (f2.contains(":")) {
            String[] split = f2.split(":");
            this.d = split[0].trim();
            this.e = Integer.valueOf(split[1].trim()).intValue();
            return;
        }
        this.d = f2;
        this.e = 80;
    }

    public static void a(e eVar) {
        boolean z;
        if (eVar.c == 2 || eVar.c == 1) {
            synchronized (a) {
                Iterator it = a.iterator();
                while (true) {
                    if (it.hasNext()) {
                        if (((e) it.next()).c == eVar.c) {
                            z = true;
                            break;
                        }
                    } else {
                        z = false;
                        break;
                    }
                }
                if (!z) {
                    a.add(eVar);
                }
            }
            return;
        }
        a.add(eVar);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00c1, code lost:
        r1 = com.google.ads.sdk.a.c.a(r13.c).a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x00d1, code lost:
        a(r0);
        a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x00dc, code lost:
        r3.c();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x011e, code lost:
        if (r0.c == 4) goto L_0x0120;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x0120, code lost:
        com.google.ads.sdk.c.a.a(r13.c, ((com.google.ads.sdk.d.a.f) r0).a());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x013f, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x0140, code lost:
        r3 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x0142, code lost:
        r1 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x0143, code lost:
        r3 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x014b, code lost:
        r1 = false;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00c1 A[Catch:{ all -> 0x0113 }] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x00cd A[Catch:{ all -> 0x0113 }] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x00dc  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x0116  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x011c A[SYNTHETIC, Splitter:B:75:0x011c] */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x013f A[ExcHandler: all (th java.lang.Throwable), Splitter:B:5:0x0014] */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x014b  */
    /* JADX WARNING: Removed duplicated region for block: B:94:0x000a A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r13 = this;
            r2 = 0
            r12 = 4
            r4 = 0
            java.lang.String r0 = "RequestThread"
            java.lang.String r1 = "Request Thread start"
            com.google.ads.sdk.f.e.c(r0, r1)
        L_0x000a:
            boolean r0 = r13.b
            if (r0 == 0) goto L_0x000f
            return
        L_0x000f:
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream
            r1.<init>()
            boolean r0 = r13.f     // Catch:{ Exception -> 0x00ac, all -> 0x013f }
            if (r0 != 0) goto L_0x0020
            monitor-enter(r13)     // Catch:{ Exception -> 0x00ac, all -> 0x013f }
            r5 = 60000(0xea60, double:2.9644E-319)
            r13.wait(r5)     // Catch:{ all -> 0x00a9 }
            monitor-exit(r13)     // Catch:{ all -> 0x00a9 }
        L_0x0020:
            java.lang.String r0 = r13.d     // Catch:{ Exception -> 0x00ac, all -> 0x013f }
            if (r0 != 0) goto L_0x0033
            android.content.Context r0 = r13.c     // Catch:{ Exception -> 0x00ac, all -> 0x013f }
            com.google.ads.sdk.a.c r0 = com.google.ads.sdk.a.c.a(r0)     // Catch:{ Exception -> 0x00ac, all -> 0x013f }
            boolean r0 = r0.a()     // Catch:{ Exception -> 0x00ac, all -> 0x013f }
            if (r0 == 0) goto L_0x0033
            r13.a()     // Catch:{ Exception -> 0x00ac, all -> 0x013f }
        L_0x0033:
            java.util.concurrent.BlockingQueue r0 = com.google.ads.sdk.d.c.a     // Catch:{ Exception -> 0x00ac, all -> 0x013f }
            java.lang.Object r0 = r0.take()     // Catch:{ Exception -> 0x00ac, all -> 0x013f }
            com.google.ads.sdk.d.a.e r0 = (com.google.ads.sdk.d.a.e) r0     // Catch:{ Exception -> 0x00ac, all -> 0x013f }
            java.lang.String r3 = "RequestThread"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0142, all -> 0x013f }
            java.lang.String r6 = "request--command:"
            r5.<init>(r6)     // Catch:{ Exception -> 0x0142, all -> 0x013f }
            int r6 = r0.c     // Catch:{ Exception -> 0x0142, all -> 0x013f }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x0142, all -> 0x013f }
            java.lang.String r6 = ",ip:"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x0142, all -> 0x013f }
            java.lang.String r6 = r13.d     // Catch:{ Exception -> 0x0142, all -> 0x013f }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x0142, all -> 0x013f }
            java.lang.String r6 = ",port:"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x0142, all -> 0x013f }
            int r6 = r13.e     // Catch:{ Exception -> 0x0142, all -> 0x013f }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x0142, all -> 0x013f }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x0142, all -> 0x013f }
            com.google.ads.sdk.f.e.c(r3, r5)     // Catch:{ Exception -> 0x0142, all -> 0x013f }
            com.google.ads.sdk.a.b.a()     // Catch:{ Exception -> 0x0142, all -> 0x013f }
            java.lang.String r5 = r13.d     // Catch:{ Exception -> 0x0142, all -> 0x013f }
            int r6 = r13.e     // Catch:{ Exception -> 0x0142, all -> 0x013f }
            com.google.ads.sdk.a.d r3 = new com.google.ads.sdk.a.d     // Catch:{ Exception -> 0x0142, all -> 0x013f }
            r3.<init>(r5, r6)     // Catch:{ Exception -> 0x0142, all -> 0x013f }
            java.io.OutputStream r5 = r3.b()     // Catch:{ Exception -> 0x00e8 }
            r0.b()     // Catch:{ Exception -> 0x00e8 }
            byte[] r6 = r0.c()     // Catch:{ b -> 0x00e1 }
            r0.g = r6     // Catch:{ b -> 0x00e1 }
        L_0x0082:
            byte[] r6 = r0.g     // Catch:{ Exception -> 0x00e8 }
            if (r6 == 0) goto L_0x009f
            int r7 = r6.length     // Catch:{ Exception -> 0x00e8 }
            r8 = 11
            if (r7 < r8) goto L_0x009f
            r5.write(r6)     // Catch:{ Exception -> 0x00e8 }
            java.io.InputStream r5 = r3.a()     // Catch:{ Exception -> 0x00e8 }
            r6 = 10240(0x2800, float:1.4349E-41)
            byte[] r6 = new byte[r6]     // Catch:{ Exception -> 0x00e8 }
        L_0x0096:
            int r7 = r5.read(r6)     // Catch:{ Exception -> 0x00e8 }
            if (r7 > 0) goto L_0x00ea
        L_0x009c:
            r1.reset()     // Catch:{ Exception -> 0x00e8 }
        L_0x009f:
            r0 = 1
            r13.f = r0     // Catch:{ Exception -> 0x0146 }
            if (r3 == 0) goto L_0x000a
            r3.c()
            goto L_0x000a
        L_0x00a9:
            r0 = move-exception
            monitor-exit(r13)     // Catch:{ Exception -> 0x00ac, all -> 0x013f }
            throw r0     // Catch:{ Exception -> 0x00ac, all -> 0x013f }
        L_0x00ac:
            r0 = move-exception
            r1 = r0
            r3 = r2
            r0 = r2
        L_0x00b0:
            java.lang.String r5 = "RequestThread"
            java.lang.String r1 = r1.getMessage()     // Catch:{ all -> 0x0113 }
            com.google.ads.sdk.f.e.c(r5, r1)     // Catch:{ all -> 0x0113 }
            android.content.Context r1 = r13.c     // Catch:{ all -> 0x0113 }
            boolean r1 = com.google.ads.sdk.f.a.a(r1)     // Catch:{ all -> 0x0113 }
            if (r1 == 0) goto L_0x014b
            android.content.Context r1 = r13.c     // Catch:{ all -> 0x0113 }
            com.google.ads.sdk.a.c r1 = com.google.ads.sdk.a.c.a(r1)     // Catch:{ all -> 0x0113 }
            boolean r1 = r1.a()     // Catch:{ all -> 0x0113 }
        L_0x00cb:
            if (r1 == 0) goto L_0x011a
            boolean r1 = r13.f     // Catch:{ all -> 0x0113 }
            if (r1 != 0) goto L_0x011a
            a(r0)     // Catch:{ all -> 0x0113 }
            r13.a()     // Catch:{ all -> 0x0113 }
        L_0x00d7:
            r0 = 0
            r13.f = r0     // Catch:{ all -> 0x0113 }
            if (r3 == 0) goto L_0x000a
            r3.c()
            goto L_0x000a
        L_0x00e1:
            r6 = move-exception
            r6 = 0
            byte[] r6 = new byte[r6]     // Catch:{ Exception -> 0x00e8 }
            r0.g = r6     // Catch:{ Exception -> 0x00e8 }
            goto L_0x0082
        L_0x00e8:
            r1 = move-exception
            goto L_0x00b0
        L_0x00ea:
            r8 = 0
            r1.write(r6, r8, r7)     // Catch:{ Exception -> 0x00e8 }
            int r7 = r1.size()     // Catch:{ Exception -> 0x00e8 }
            if (r7 < r12) goto L_0x0096
            r7 = 4
            byte[] r7 = new byte[r7]     // Catch:{ Exception -> 0x00e8 }
            byte[] r8 = r1.toByteArray()     // Catch:{ Exception -> 0x00e8 }
            r9 = 0
            r10 = 0
            int r11 = r7.length     // Catch:{ Exception -> 0x00e8 }
            java.lang.System.arraycopy(r8, r9, r7, r10, r11)     // Catch:{ Exception -> 0x00e8 }
            int r7 = com.google.ads.sdk.f.o.a(r7)     // Catch:{ Exception -> 0x00e8 }
            int r8 = r1.size()     // Catch:{ Exception -> 0x00e8 }
            if (r7 > r8) goto L_0x0096
            byte[] r5 = r1.toByteArray()     // Catch:{ Exception -> 0x00e8 }
            com.google.ads.sdk.d.b.g.a(r5)     // Catch:{ Exception -> 0x00e8 }
            goto L_0x009c
        L_0x0113:
            r0 = move-exception
        L_0x0114:
            if (r3 == 0) goto L_0x0119
            r3.c()
        L_0x0119:
            throw r0
        L_0x011a:
            if (r0 == 0) goto L_0x012c
            int r1 = r0.c     // Catch:{ all -> 0x0113 }
            if (r1 != r12) goto L_0x012c
            com.google.ads.sdk.d.a.f r0 = (com.google.ads.sdk.d.a.f) r0     // Catch:{ all -> 0x0113 }
            java.lang.String r0 = r0.a()     // Catch:{ all -> 0x0113 }
            android.content.Context r1 = r13.c     // Catch:{ all -> 0x0113 }
            com.google.ads.sdk.c.a.a(r1, r0)     // Catch:{ all -> 0x0113 }
            goto L_0x00d7
        L_0x012c:
            if (r0 == 0) goto L_0x00d7
            int r1 = r0.c     // Catch:{ all -> 0x0113 }
            r5 = 5
            if (r1 != r5) goto L_0x00d7
            com.google.ads.sdk.d.a.b r0 = (com.google.ads.sdk.d.a.b) r0     // Catch:{ all -> 0x0113 }
            java.lang.String r0 = r0.a()     // Catch:{ all -> 0x0113 }
            android.content.Context r1 = r13.c     // Catch:{ all -> 0x0113 }
            com.google.ads.sdk.c.a.a(r1, r0)     // Catch:{ all -> 0x0113 }
            goto L_0x00d7
        L_0x013f:
            r0 = move-exception
            r3 = r2
            goto L_0x0114
        L_0x0142:
            r1 = move-exception
            r3 = r2
            goto L_0x00b0
        L_0x0146:
            r0 = move-exception
            r1 = r0
            r0 = r2
            goto L_0x00b0
        L_0x014b:
            r1 = r4
            goto L_0x00cb
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.ads.sdk.d.c.run():void");
    }
}
