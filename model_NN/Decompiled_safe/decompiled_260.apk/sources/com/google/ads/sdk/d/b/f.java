package com.google.ads.sdk.d.b;

import android.content.Context;

public final class f implements c {
    private Context a;

    public f(Context context) {
        this.a = context;
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0036  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(com.google.ads.sdk.d.b.e r10) {
        /*
            r9 = this;
            r7 = 1000(0x3e8, double:4.94E-321)
            r3 = 0
            if (r10 != 0) goto L_0x0006
        L_0x0005:
            return
        L_0x0006:
            int r0 = r10.c
            switch(r0) {
                case 1: goto L_0x000c;
                case 2: goto L_0x0087;
                default: goto L_0x000b;
            }
        L_0x000b:
            goto L_0x0005
        L_0x000c:
            com.google.ads.sdk.d.b.d r10 = (com.google.ads.sdk.d.b.d) r10
            java.util.Map r2 = r10.h
            java.lang.String r0 = "REGISTER_ID"
            java.lang.Object r0 = r2.get(r0)
            java.lang.String r0 = (java.lang.String) r0
            java.lang.String r1 = "PASSWORD"
            java.lang.Object r1 = r2.get(r1)
            java.lang.String r1 = (java.lang.String) r1
            java.lang.String r4 = "NEXT_AD_CHECK_TIME"
            boolean r4 = r2.containsKey(r4)
            if (r4 == 0) goto L_0x0085
            java.lang.String r4 = "NEXT_AD_CHECK_TIME"
            java.lang.Object r2 = r2.get(r4)     // Catch:{ Exception -> 0x0084 }
            java.lang.Integer r2 = (java.lang.Integer) r2     // Catch:{ Exception -> 0x0084 }
            int r2 = r2.intValue()     // Catch:{ Exception -> 0x0084 }
        L_0x0034:
            if (r2 <= 0) goto L_0x0042
            android.content.Context r4 = r9.a
            long r5 = (long) r2
            long r5 = r5 * r7
            com.google.ads.sdk.c.a.a(r4, r5)
            android.content.Context r4 = r9.a
            com.google.ads.sdk.active.MyAdReceiver.a(r4)
        L_0x0042:
            android.content.Context r4 = r9.a
            java.lang.String r5 = "meiline_preference"
            android.content.SharedPreferences r3 = r4.getSharedPreferences(r5, r3)
            android.content.SharedPreferences$Editor r3 = r3.edit()
            java.lang.String r4 = "REGISTER_ID"
            r3.putString(r4, r0)
            java.lang.String r4 = "PASSWORD"
            r3.putString(r4, r1)
            r3.commit()
            java.lang.String r3 = "ResponseCommandCallBack"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r5 = "registerId:"
            r4.<init>(r5)
            java.lang.StringBuilder r0 = r4.append(r0)
            java.lang.String r4 = ",password:"
            java.lang.StringBuilder r0 = r0.append(r4)
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = ", ad interval:"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
            com.google.ads.sdk.f.e.c(r3, r0)
            goto L_0x0005
        L_0x0084:
            r2 = move-exception
        L_0x0085:
            r2 = r3
            goto L_0x0034
        L_0x0087:
            com.google.ads.sdk.d.b.a r10 = (com.google.ads.sdk.d.b.a) r10
            int r0 = r10.i
            if (r0 != 0) goto L_0x013a
            java.util.Map r0 = r10.h
            java.lang.String r1 = "PUSH_INFO"
            java.lang.Object r0 = r0.get(r1)
            java.lang.String r0 = (java.lang.String) r0
            com.google.ads.sdk.b.d r1 = new com.google.ads.sdk.b.d
            r1.<init>(r0)
            int r2 = r1.c
            if (r2 != 0) goto L_0x012f
            boolean r2 = com.google.ads.AdManager.a
            if (r2 == 0) goto L_0x0125
            android.content.Context r2 = r9.a
            int r3 = com.google.ads.sdk.f.d.d
            long r3 = (long) r3
            com.google.ads.sdk.c.a.a(r2, r3)
        L_0x00ac:
            boolean r2 = r1.e
            if (r2 == 0) goto L_0x011e
            android.content.Intent r2 = new android.content.Intent
            android.content.Context r3 = r9.a
            java.lang.Class<com.google.ads.AdReceiver> r4 = com.google.ads.AdReceiver.class
            r2.<init>(r3, r4)
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            android.content.Context r4 = r9.a
            java.lang.String r4 = r4.getPackageName()
            java.lang.String r4 = java.lang.String.valueOf(r4)
            r3.<init>(r4)
            java.lang.String r4 = ".com.suyue168.android.sdk.SHOW_NOTIFICATION"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            r2.setAction(r3)
            java.lang.String r3 = "PUSH_INFO"
            r2.putExtra(r3, r1)
            android.content.Context r3 = r9.a
            com.google.ads.sdk.b.a r4 = r1.b
            java.lang.String r4 = r4.e
            int r4 = java.lang.Integer.parseInt(r4)
            r5 = 100
            com.google.ads.sdk.f.r.a(r3, r4, r5)
            android.content.Context r3 = r9.a
            r3.sendBroadcast(r2)
            java.lang.String r2 = "ResponseCommandCallBack"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "pushInfo:"
            r3.<init>(r4)
            java.lang.StringBuilder r0 = r3.append(r0)
            java.lang.String r3 = ",apkSize:"
            java.lang.StringBuilder r0 = r0.append(r3)
            com.google.ads.sdk.b.a r3 = r1.b
            java.lang.String r3 = r3.u
            java.lang.StringBuilder r0 = r0.append(r3)
            java.lang.String r3 = ",adId:"
            java.lang.StringBuilder r0 = r0.append(r3)
            com.google.ads.sdk.b.a r1 = r1.b
            java.lang.String r1 = r1.e
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            com.google.ads.sdk.f.e.c(r2, r0)
        L_0x011e:
            android.content.Context r0 = r9.a
            com.google.ads.AdReceiver.a(r0)
            goto L_0x0005
        L_0x0125:
            android.content.Context r2 = r9.a
            int r3 = com.google.ads.sdk.f.d.c
            long r3 = (long) r3
            com.google.ads.sdk.c.a.a(r2, r3)
            goto L_0x00ac
        L_0x012f:
            android.content.Context r2 = r9.a
            int r3 = r1.c
            long r3 = (long) r3
            long r3 = r3 * r7
            com.google.ads.sdk.c.a.a(r2, r3)
            goto L_0x00ac
        L_0x013a:
            java.lang.String r0 = "ResponseCommandCallBack"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "error reason :"
            r1.<init>(r2)
            java.lang.String r2 = r10.j
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            com.google.ads.sdk.f.e.c(r0, r1)
            boolean r0 = com.google.ads.AdManager.a
            if (r0 == 0) goto L_0x015d
            android.content.Context r0 = r9.a
            int r1 = com.google.ads.sdk.f.d.d
            long r1 = (long) r1
            com.google.ads.sdk.c.a.a(r0, r1)
            goto L_0x011e
        L_0x015d:
            android.content.Context r0 = r9.a
            int r1 = com.google.ads.sdk.f.d.c
            long r1 = (long) r1
            com.google.ads.sdk.c.a.a(r0, r1)
            goto L_0x011e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.ads.sdk.d.b.f.a(com.google.ads.sdk.d.b.e):void");
    }
}
