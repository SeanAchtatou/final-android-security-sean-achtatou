package com.google.ads.sdk.f;

import android.util.Log;

public final class e {
    private static boolean a = false;

    public static void a() {
        a = true;
    }

    public static void a(String str, String str2) {
        if (a) {
            Log.i("PUSHSDK-INFO", "[" + str + "] : " + str2);
        }
    }

    public static void a(String str, String str2, Throwable th) {
        if (a) {
            Log.v("PUSHSDK-INFO", "[" + str + "] : " + str2, th);
        }
    }

    public static void b(String str, String str2) {
        if (a) {
            Log.v("PUSHSDK-INFO", "[" + str + "] : " + str2);
        }
    }

    public static void b(String str, String str2, Throwable th) {
        if (a) {
            Log.d("PUSHSDK-INFO", "[" + str + "] : " + str2, th);
        }
    }

    public static void c(String str, String str2) {
        if (a) {
            Log.d("PUSHSDK-INFO", "[" + str + "] : " + str2);
        }
    }

    public static void c(String str, String str2, Throwable th) {
        if (a) {
            Log.w("PUSHSDK-INFO", "[" + str + "] : " + str2, th);
        }
    }

    public static void d(String str, String str2) {
        if (a) {
            Log.w("PUSHSDK-INFO", "[" + str + "] : " + str2);
        }
    }

    public static void d(String str, String str2, Throwable th) {
        if (a) {
            Log.e("PUSHSDK-INFO", "[" + str + "] : " + str2, th);
        }
    }

    public static void e(String str, String str2) {
        if (a) {
            Log.e("PUSHSDK-INFO", "[" + str + "] : " + str2);
        }
    }
}
