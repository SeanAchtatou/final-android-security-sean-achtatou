package com.google.ads.sdk.f;

import android.content.Context;
import android.net.ConnectivityManager;

public final class s {
    public static boolean a(Context context) {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
            if (connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isAvailable() && connectivityManager.getActiveNetworkInfo().isConnected()) {
                return true;
            }
            e.e("AirpushSDK", "Internet Connection not found.");
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
