package com.google.ads.sdk.f;

import android.content.Context;

public final class g {
    public static int a(Context context, int i) {
        float f = ((float) context.getResources().getDisplayMetrics().widthPixels) / 480.0f;
        float f2 = ((float) context.getResources().getDisplayMetrics().heightPixels) / 800.0f;
        return f < f2 ? (int) (f * ((float) i)) : (int) (((float) i) * f2);
    }
}
