package com.google.ads;

import android.content.Context;
import com.google.ads.sdk.f.e;
import com.google.ads.sdk.f.l;

public class AdManager {
    public static boolean a;

    public static void init(Context context) {
        init(context, false);
    }

    public static void init(Context context, boolean z) {
        a = z;
        if (z) {
            e.a();
            e.a("PUSH_SDK", "is test mode=" + a);
        }
        l.init(context);
    }
}
