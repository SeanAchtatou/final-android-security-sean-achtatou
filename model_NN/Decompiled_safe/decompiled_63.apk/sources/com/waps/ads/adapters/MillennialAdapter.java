package com.waps.ads.adapters;

import android.app.Activity;
import android.util.Log;
import android.view.ViewGroup;
import com.millennialmedia.android.MMAdView;
import com.waps.ads.AdGroupLayout;
import com.waps.ads.AdGroupTargeting;
import com.waps.ads.a.a;
import com.waps.ads.b.c;
import com.waps.ads.f;
import java.util.Hashtable;

public class MillennialAdapter extends a implements MMAdView.MMAdListener {
    public MillennialAdapter(AdGroupLayout adGroupLayout, c cVar) {
        super(adGroupLayout, cVar);
    }

    public void MMAdClickedToNewBrowser(MMAdView mMAdView) {
        if (AdGroupTargeting.getTestMode()) {
            Log.d("AdView SDK", "Millennial Ad clicked, new browser launched");
        }
    }

    public void MMAdClickedToOverlay(MMAdView mMAdView) {
        if (AdGroupTargeting.getTestMode()) {
            Log.d("AdView SDK", "Millennial Ad Clicked to overlay");
        }
    }

    public void MMAdFailed(MMAdView mMAdView) {
        if (AdGroupTargeting.getTestMode()) {
            Log.d("AdView SDK", "Millennial failure");
        }
        mMAdView.setListener((MMAdView.MMAdListener) null);
        AdGroupLayout adGroupLayout = (AdGroupLayout) this.c.get();
        if (adGroupLayout != null) {
            adGroupLayout.j.resetRollover();
            adGroupLayout.rollover();
        }
    }

    public void MMAdOverlayLaunched(MMAdView mMAdView) {
        if (AdGroupTargeting.getTestMode()) {
            Log.d("AdView SDK", "Millennial Ad Overlay Launched");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.waps.ads.f.<init>(com.waps.ads.AdGroupLayout, android.view.ViewGroup):void
     arg types: [com.waps.ads.AdGroupLayout, com.millennialmedia.android.MMAdView]
     candidates:
      com.waps.ads.f.<init>(com.waps.ads.AdGroupLayout, android.view.View):void
      com.waps.ads.f.<init>(com.waps.ads.AdGroupLayout, android.view.ViewGroup):void */
    public void MMAdReturned(MMAdView mMAdView) {
        mMAdView.setListener((MMAdView.MMAdListener) null);
        AdGroupLayout adGroupLayout = (AdGroupLayout) this.c.get();
        if (adGroupLayout != null) {
            adGroupLayout.j.resetRollover();
            adGroupLayout.b.post(new f(adGroupLayout, (ViewGroup) mMAdView));
            adGroupLayout.rotateThreadedDelayed();
        }
    }

    public void handle() {
        AdGroupLayout adGroupLayout = (AdGroupLayout) this.c.get();
        if (adGroupLayout != null) {
            Hashtable hashtable = new Hashtable();
            hashtable.put("vendor", "adview");
            MMAdView mMAdView = new MMAdView((Activity) adGroupLayout.getContext(), this.d.e, "MMBannerAdTop", adGroupLayout.d.i, AdGroupTargeting.getTestMode(), hashtable);
            mMAdView.setListener(this);
            mMAdView.callForAd();
            mMAdView.setHorizontalScrollBarEnabled(false);
            mMAdView.setVerticalScrollBarEnabled(false);
        }
    }
}
