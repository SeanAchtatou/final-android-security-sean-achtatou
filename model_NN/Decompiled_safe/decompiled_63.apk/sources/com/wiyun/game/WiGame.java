package com.wiyun.game;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.hardware.SensorManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.widget.Toast;
import com.saubcy.games.maze.market.MazeGame;
import com.saubcy.games.maze.market.R;
import com.wiyun.game.a.d;
import com.wiyun.game.a.f;
import com.wiyun.game.a.i;
import com.wiyun.game.b.e;
import com.wiyun.game.model.ChallengeRequest;
import com.wiyun.game.model.ChallengeResult;
import com.wiyun.game.model.User;
import com.wiyun.game.model.a.ac;
import com.wiyun.game.model.a.ag;
import com.wiyun.game.model.a.ai;
import com.wiyun.game.model.a.u;
import com.wiyun.game.model.a.v;
import java.io.File;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.json.JSONException;
import org.json.JSONObject;

public class WiGame {
    /* access modifiers changed from: private */
    public static boolean A = false;
    /* access modifiers changed from: private */
    public static boolean B = false;
    private static boolean C = false;
    private static String D = null;
    /* access modifiers changed from: private */
    public static boolean E = false;
    /* access modifiers changed from: private */
    public static int F = 0;
    /* access modifiers changed from: private */
    public static int G = 0;
    /* access modifiers changed from: private */
    public static int H = 0;
    /* access modifiers changed from: private */
    public static int I = 0;
    private static int J = -1;
    private static String K = null;
    private static String L = null;
    private static String M = null;
    /* access modifiers changed from: private */
    public static Location N = null;
    private static int O = -1;
    private static int P = -1;
    /* access modifiers changed from: private */
    public static ag Q = null;
    /* access modifiers changed from: private */
    public static Context R = null;
    /* access modifiers changed from: private */
    public static boolean S = false;
    /* access modifiers changed from: private */
    public static c T = null;
    public static final int TOAST_SIDE_BOTTOM = 1;
    public static final int TOAST_SIDE_TOP = 0;
    private static List<WiGameClient> U;
    private static Object V = new Object();
    /* access modifiers changed from: private */
    public static Handler W;
    private static int X;
    private static String Y;
    private static Intent Z;
    static ArrayList<ag> a;
    /* access modifiers changed from: private */
    public static ChallengeResult aa;
    /* access modifiers changed from: private */
    public static List<b> ab;
    /* access modifiers changed from: private */
    public static boolean ac;
    /* access modifiers changed from: private */
    public static boolean ad;
    /* access modifiers changed from: private */
    public static boolean ae;
    private static boolean af;
    private static boolean ag;
    private static boolean ah;
    private static final com.wiyun.game.b.a ai = new l();
    /* access modifiers changed from: private */
    public static final LocationListener aj = new m();
    static ArrayList<ac> b;
    static long c;
    static Context d;
    static String e;
    static File f;
    static File g;
    static int h = 1;
    static int i = 1;
    static boolean j;
    static List<d> k;
    static a l;
    static String m;
    static boolean n;
    static boolean o;
    private static String p;
    private static String q;
    private static String r;
    private static String s;
    private static String t = null;
    private static boolean u = false;
    private static int v = 0;
    private static boolean w = false;
    /* access modifiers changed from: private */
    public static boolean x;
    /* access modifiers changed from: private */
    public static boolean y;
    /* access modifiers changed from: private */
    public static boolean z = true;

    static final class a {
        String a;
        String b;
        byte[] c;
        byte[] d;
        boolean e;

        a() {
        }
    }

    static final class b {
        String a;
        String b;
        long c;
        boolean d;

        b() {
        }

        public void a(ContentValues values) {
            values.put("uid", this.a);
            values.put("ach_id", this.b);
            values.put("ut", Long.valueOf(this.c));
            values.put("done", Boolean.valueOf(this.d));
            i a2 = new i();
            a2.a(true);
            StringBuilder buf = new StringBuilder();
            buf.append(this.b).append(':').append(this.c);
            values.put("sig", h.a(a2, com.wiyun.game.e.a.a(h.d(buf.toString())), "wiyun.db"));
        }
    }

    private static final class c implements View.OnClickListener {
        private int a;

        c(int mode) {
            this.a = mode;
        }

        public void onClick(View v) {
            int id = v.getId();
            if (id == t.d("wy_b_show_challenge")) {
                switch (this.a) {
                    case 1:
                        WiGame.openPendingChallenges();
                        break;
                    case 2:
                        WiGame.openPendingFriends();
                        break;
                    case 3:
                        WiGame.openUnreadMessages();
                        break;
                    case 4:
                        WiGame.openUnreadNotices();
                        break;
                    case 5:
                        WiGame.openMyProfile();
                        break;
                }
                WiGame.W.removeMessages(11, v.getTag());
                WiGame.W.sendMessage(Message.obtain(WiGame.W, 11, v.getTag()));
            } else if (id == t.d("wy_b_challenge_later")) {
                WiGame.W.removeMessages(11, v.getTag());
                WiGame.W.sendMessage(Message.obtain(WiGame.W, 11, v.getTag()));
            }
        }
    }

    static final class d implements Serializable {
        String a;
        String b;
        int c;
        byte[] d;
        long e;
        double f;
        double g;
        boolean h;
        boolean i;
        long j;

        d() {
        }

        public void a(ContentValues values) {
            values.put("uid", this.a);
            values.put("lb_id", this.b);
            values.put("score", Integer.valueOf(this.c));
            values.put("ct", Long.valueOf(this.e));
            values.put("done", Boolean.valueOf(this.h));
            values.put("lat", Double.valueOf(this.f));
            values.put("lon", Double.valueOf(this.g));
            if (this.d != null) {
                values.put("_blob", this.d);
            }
            i a2 = new i();
            a2.a(true);
            StringBuilder buf = new StringBuilder();
            buf.append(this.b).append(':').append(this.c).append(':').append(this.e).append(':').append(this.f).append(':').append(this.g).append(':').append(this.d == null ? "" : Arrays.toString(com.wiyun.game.e.a.a(this.d)));
            values.put("sig", h.a(a2, com.wiyun.game.e.a.a(h.d(buf.toString())), "wiyun.db"));
        }
    }

    static {
        try {
            Field field = Build.VERSION.class.getField("SDK_INT");
            n = field != null && field.getInt(null) > 3;
        } catch (Exception e2) {
            n = false;
        }
    }

    static String A() {
        try {
            Class<?> cls = Class.forName("android.os.SystemProperties");
            return (String) cls.getMethod("get", String.class).invoke(cls, "ro.jq.channel.name");
        } catch (Exception e2) {
            return "unknown";
        }
    }

    static String B() {
        try {
            Class<?> cls = Class.forName("android.os.SystemProperties");
            Method method = cls.getMethod("get", String.class);
            int c2 = h.c((String) method.invoke(cls, "ro.jq.channel.packages"));
            if (c2 <= 0) {
                return null;
            }
            StringBuilder sb = new StringBuilder();
            for (int i2 = 1; i2 <= c2; i2++) {
                sb.append((String) method.invoke(cls, "ro.jq.channel.package." + i2)).append(',');
            }
            if (sb.length() > 0) {
                sb.deleteCharAt(sb.length() - 1);
            }
            return sb.toString();
        } catch (Exception e2) {
            return null;
        }
    }

    static void C() {
        boolean z2;
        boolean z3;
        if (R != null) {
            PackageManager packageManager = R.getPackageManager();
            try {
                z2 = packageManager.getPackageGids("com.lenovo.appstore") != null;
            } catch (PackageManager.NameNotFoundException e2) {
                z2 = false;
            }
            if (!z2) {
                try {
                    z3 = packageManager.getPackageGids("com.lenovo.leos.appstore") != null;
                } catch (PackageManager.NameNotFoundException e3) {
                    z3 = z2;
                }
            } else {
                z3 = z2;
            }
            if (z3) {
                L = "lenovo";
                if (R.getResources().getDisplayMetrics().heightPixels > 800) {
                    M = "lepad";
                } else {
                    M = "lephone";
                }
            } else {
                L = Build.BRAND;
                M = Build.MODEL;
            }
        }
    }

    static String D() {
        if (L == null) {
            C();
        }
        return L;
    }

    static String E() {
        if (M == null) {
            C();
        }
        return M;
    }

    static boolean F() {
        D();
        return "lephone".equals(M);
    }

    static boolean G() {
        if (O == -1) {
            try {
                O = Class.forName("com.wiyun.game.UserMap") != null ? 1 : 0;
            } catch (Throwable th) {
                O = 0;
            }
        }
        return O == 1;
    }

    static int H() {
        if (P == -1) {
            P = (int) (R.getResources().getDisplayMetrics().density * 160.0f);
        }
        return P;
    }

    static Bitmap a(String id, String url, int defaultId) {
        if (T == null) {
            return null;
        }
        return T.a(id, url, defaultId);
    }

    static d a(long callId) {
        if (k != null) {
            for (d ps : k) {
                if (ps.j == callId) {
                    return ps;
                }
            }
        }
        return null;
    }

    static void a() {
        synchronized (V) {
            x = false;
            t = null;
            Q = new ag();
            E = false;
            a = null;
        }
    }

    static void a(long callId, User user) {
        if (U != null) {
            for (WiGameClient client : U) {
                client.wyUserInfoGot(callId, user);
            }
        }
    }

    static void a(long callId, String userId) {
        if (U != null) {
            for (WiGameClient client : U) {
                client.wyGetUserInfoFailed(callId, userId);
            }
        }
    }

    static void a(long callId, String userId, int start) {
        if (U != null) {
            for (WiGameClient client : U) {
                client.wyGetFriendListFailed(callId, userId, start);
            }
        }
    }

    static void a(long callId, String userId, ArrayList<User> friends, int start) {
        if (U != null) {
            for (WiGameClient client : U) {
                client.wyFriendListGot(callId, userId, friends, start);
            }
        }
    }

    static void a(Context context) {
        if (R == null) {
            init(context, p, q, e, h == 0, j);
        }
    }

    static void a(Intent intent) {
        if (R != null) {
            R.sendBroadcast(intent);
        }
    }

    static void a(Bundle outState) {
        outState.putString("app_key", p);
        outState.putString("secret_key", q);
        outState.putString("session_key", t);
    }

    static void a(ChallengeRequest request) {
        if (U != null) {
            for (WiGameClient client : U) {
                client.wyPlayChallenge(request);
            }
        }
    }

    static void a(ChallengeResult result) {
        if (U != null) {
            for (WiGameClient client : U) {
                client.wyChallengeResultSubmitted(result);
            }
        }
    }

    static void a(String id) {
        h.a(new File(f, "wiyun_id"), h.a((i) null, id, "wiyun_id"));
    }

    static void a(String name, int totalSize) {
        if (U != null) {
            for (WiGameClient client : U) {
                client.wyGameSaveStart(name, totalSize);
            }
        }
    }

    static void a(String leaderboardId, int score, int rank) {
        if (U != null) {
            for (WiGameClient client : U) {
                client.wyScoreSubmitted(leaderboardId, score, rank);
            }
        }
    }

    static void a(String str, v vVar, u uVar, byte[] bArr) {
        if (R != null) {
            ChallengeRequest challengeRequest = new ChallengeRequest();
            challengeRequest.setChallengeDefinitionId(str);
            challengeRequest.setCtuId(vVar.a());
            challengeRequest.setFromUsername(uVar.b());
            challengeRequest.setScore(uVar.d());
            challengeRequest.setPortraitUrl(uVar.c());
            challengeRequest.setBid(uVar.a());
            challengeRequest.setFromUserId(uVar.e());
            if (bArr != null) {
                challengeRequest.setBlob(bArr);
            }
            W.sendMessage(Message.obtain(W, 1014, challengeRequest));
            R.sendBroadcast(new Intent("com.wiyun.game.RESET"));
        }
    }

    static void a(boolean loggingIn) {
        w = loggingIn;
    }

    static void a(byte[] data) {
        h.a(new File(f, "wiyun_app_config"), data);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: private */
    public static void ab() {
        boolean z2;
        if (y && B) {
            if (z) {
                z = false;
                if ((a == null || a.isEmpty()) && X == 0) {
                    Intent intent = new Intent(R, Login.class);
                    intent.setFlags(67108864);
                    intent.putExtra("force", !A);
                    intent.putExtra("prompt_binding", true);
                    R.startActivity(intent);
                    z2 = true;
                    if (!z2 && C && X == 0 && a != null && !a.isEmpty()) {
                        C = false;
                        Intent intent2 = new Intent(R, SwitchAccount.class);
                        intent2.setFlags(67108864);
                        intent2.putParcelableArrayListExtra("bound_users", a);
                        R.startActivity(intent2);
                        return;
                    }
                    return;
                }
            }
            z2 = false;
            if (!z2) {
            }
        }
    }

    private static void ac() {
        W.sendEmptyMessage(3);
        f.a(aa.getCtuId(), aa.getResult(), aa.getScore(), aa.getBlob());
    }

    /* access modifiers changed from: private */
    public static void ad() {
        String str;
        String str2;
        synchronized (V) {
            if (N == null) {
                LocationManager locationManager = (LocationManager) R.getSystemService("location");
                if (R.checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") != 0 || locationManager == null) {
                    str = null;
                } else {
                    Criteria criteria = new Criteria();
                    criteria.setAccuracy(2);
                    criteria.setCostAllowed(false);
                    str = locationManager.getBestProvider(criteria, true);
                }
                Location lastKnownLocation = str == null ? null : locationManager.getLastKnownLocation(str);
                if (R.checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION") != 0 || locationManager == null) {
                    str2 = null;
                } else {
                    Criteria criteria2 = new Criteria();
                    criteria2.setAccuracy(1);
                    criteria2.setCostAllowed(false);
                    str2 = locationManager.getBestProvider(criteria2, true);
                }
                Location lastKnownLocation2 = str2 == null ? null : locationManager.getLastKnownLocation(str2);
                if (lastKnownLocation2 != null) {
                    N = lastKnownLocation2;
                    ae();
                } else if (lastKnownLocation != null) {
                    N = lastKnownLocation;
                    ae();
                } else if (str == null && str2 == null) {
                    ae();
                } else if (!(w() == null || R == null)) {
                    final String str3 = str == null ? str2 : str;
                    w().post(new Runnable() {
                        public void run() {
                            LocationManager locationManager = (LocationManager) WiGame.R.getSystemService("location");
                            locationManager.removeUpdates(WiGame.aj);
                            locationManager.requestLocationUpdates(str3, 3600000, 1000.0f, WiGame.aj);
                        }
                    });
                }
            } else {
                ae();
            }
        }
    }

    public static void addFriend(String userId, String message) {
        f.b(userId, message);
    }

    public static void addWiGameClient(WiGameClient client) {
        if (U != null && !U.contains(client)) {
            U.add(client);
        }
    }

    /* access modifiers changed from: private */
    public static void ae() {
        if (Q != null && !TextUtils.isEmpty(getMyId()) && N != null && !v()) {
            f.a(N.getLatitude(), N.getLongitude());
            try {
                CookieManager.getInstance().setCookie("http://wiyun.com", String.format("wigame_config_location=%f,%f;domain=%s", Double.valueOf(getLatitude()), Double.valueOf(getLongitude()), "wiyun.com"));
            } catch (Throwable th) {
            }
        }
    }

    private static String af() {
        Context context = getContext();
        if (context == null) {
            c("Context is not set");
            return null;
        } else if (context.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") == -1) {
            c("Cannot get a device ID.  Open manifest.xml and just before the final </manifest> tag add:  <uses-permission android:name=\"android.permission.READ_PHONE_STATE\" />");
            return null;
        } else {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
            if (telephonyManager != null) {
                return telephonyManager.getDeviceId();
            }
            return null;
        }
    }

    private static boolean ag() {
        Context context = getContext();
        if (context == null) {
            return false;
        }
        SensorManager sensorManager = (SensorManager) context.getSystemService("sensor");
        if (sensorManager == null) {
            return false;
        }
        return sensorManager.getDefaultSensor(2) != null;
    }

    static void b() {
        com.wiyun.game.b.d.a().a(ai);
    }

    static void b(Bundle state) {
        p = state.getString("app_key");
        q = state.getString("secret_key");
        t = state.getString("session_key");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wiyun.game.h.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.wiyun.game.h.a(int, java.lang.String):int
      com.wiyun.game.h.a(java.lang.String, java.lang.String):java.lang.String
      com.wiyun.game.h.a(android.view.View, int):void
      com.wiyun.game.h.a(java.io.File, byte[]):boolean
      com.wiyun.game.h.a(android.content.Context, int):byte[]
      com.wiyun.game.h.a(java.lang.String, boolean):void */
    /* access modifiers changed from: private */
    public static void b(Message message) {
        switch (message.what) {
            case 1:
                if (R != null && Q != null) {
                    h.i(String.format(t.h("wy_toast_welcome_back"), Q.getName()));
                    return;
                }
                return;
            case 2:
                if (R != null && Q != null) {
                    h.a(String.format(t.h("wy_toast_welcome_back"), Q.getName()), true);
                    return;
                }
                return;
            case 3:
                if (R != null) {
                    h.i(t.h("wy_toast_submitting_challenge_result"));
                    return;
                }
                return;
            case 4:
                if (R != null) {
                    h.i(t.h("wy_toast_challenge_result_submitted"));
                    return;
                }
                return;
            case 5:
                if (R != null) {
                    h.a(t.h("wy_toast_challenge_result_cached"), true);
                    return;
                }
                return;
            case 6:
                f.h();
                e.b(R, p);
                return;
            case 7:
                if (R != null) {
                    e eVar = (e) message.obj;
                    if (eVar.g == 0) {
                        String a2 = eVar.a("leaderboard_id");
                        int c2 = i.c(a2);
                        com.wiyun.game.model.a.c b2 = x.b(a2);
                        if (b2 == null) {
                            return;
                        }
                        if (b2.c()) {
                            int i2 = c2 % 1000;
                            int i3 = c2 / 1000;
                            String format = String.format("%d:%d.%d", Integer.valueOf(i3 / 60), Integer.valueOf(i3 % 60), Integer.valueOf(i2));
                            h.i(String.format(t.h("wy_toast_score_submitted_with_rank_0"), format));
                            return;
                        }
                        h.i(String.format(t.h("wy_toast_score_submitted_with_rank_0"), String.valueOf(c2)));
                        return;
                    }
                    h.i(String.format(t.h("wy_toast_score_submitted_with_rank"), Integer.valueOf(eVar.g)));
                    return;
                }
                return;
            case R.styleable.com_wiyun_ad_AdView_testAdType:
                h.i(t.h("wy_toast_score_cached"));
                return;
            case R.styleable.com_wiyun_ad_AdView_transition:
                if (R == null) {
                    return;
                }
                if (message.obj != null) {
                    h.i(String.format(t.h("wy_toast_x_achievement_unlocked"), (String) message.obj));
                    return;
                }
                h.i(t.h("wy_toast_achievement_unlocked"));
                return;
            case MazeGame.WIDTHREDUCTION:
                h.a(R, (String) message.obj, t.a("wy_toast_enter"), new c(message.arg1), t.d("wy_b_show_challenge"), t.d("wy_b_challenge_later"));
                return;
            case 11:
                h.a((View) message.obj);
                return;
            case 12:
                View view = (View) message.obj;
                if (view != null && view.getParent() != null) {
                    ((ViewGroup) view.getParent()).removeView(view);
                    return;
                }
                return;
            case 13:
                if (R != null && Q != null) {
                    h.i(t.h("wy_toast_device_banned"));
                    return;
                }
                return;
            case 14:
                f.b();
                W.sendEmptyMessageDelayed(14, 300000);
                return;
            case 1000:
                e(t);
                return;
            case 1001:
                z();
                return;
            case 1002:
                h((String) message.obj);
                return;
            case 1003:
                a((String) message.obj, message.arg1);
                return;
            case 1004:
                e eVar2 = (e) message.obj;
                a(h.b(eVar2.a("call_id")), (User) eVar2.e);
                return;
            case 1005:
                e eVar3 = (e) message.obj;
                a(h.b(eVar3.a("call_id")), eVar3.a("user_id"));
                return;
            case 1006:
                e eVar4 = (e) message.obj;
                a(h.b(eVar4.a("call_id")), eVar4.a("user_id"), (ArrayList) eVar4.e, h.c(eVar4.a("start")));
                return;
            case 1007:
                e eVar5 = (e) message.obj;
                a(h.b(eVar5.a("call_id")), eVar5.a("user_id"), h.c(eVar5.a("start")));
                return;
            case 1008:
                i((String) message.obj);
                return;
            case 1009:
                j((String) message.obj);
                return;
            case 1010:
                k((String) message.obj);
                return;
            case 1012:
                a((String) message.obj, message.arg1, message.arg2);
                return;
            case 1013:
                a((ChallengeResult) message.obj);
                return;
            case 1014:
                a((ChallengeRequest) message.obj);
                return;
            default:
                return;
        }
    }

    static void b(String name) {
        h.a(new File(f, "wiyun_name"), h.a((i) null, name, "wiyun_name"));
    }

    static void c(String message) {
        Log.e("WiYun", message);
        throw new IllegalArgumentException(message);
    }

    static boolean c() {
        return E;
    }

    static String d() {
        try {
            return String.valueOf(R.getPackageManager().getPackageInfo(R.getPackageName(), 0).versionCode);
        } catch (Exception e2) {
            return "1.0";
        }
    }

    static void d(String sessionKey) {
        t = sessionKey;
    }

    public static void destroy(Context context) {
        synchronized (V) {
            if (R != null) {
                if (context.hashCode() == R.hashCode()) {
                    S = false;
                    com.wiyun.game.a.d.a();
                    f.b();
                    com.wiyun.game.b.d.a().d();
                    com.wiyun.game.b.d.a().b(ai);
                    if (T != null) {
                        T.b();
                        T = null;
                    }
                    ((LocationManager) R.getSystemService("location")).removeUpdates(aj);
                    f.b();
                    f.c();
                    x();
                    i.a();
                    x.c();
                    W.removeMessages(6);
                    W.removeMessages(14);
                    W = null;
                    e.a = false;
                    w = false;
                    ab.clear();
                    ab = null;
                    if (b != null) {
                        b.clear();
                        b = null;
                    }
                    U.clear();
                    U = null;
                    k.clear();
                    k = null;
                    X = 0;
                    a = null;
                    ad = false;
                    ah = false;
                    N = null;
                    t = null;
                    R = null;
                    p = null;
                    q = null;
                    Q = null;
                    E = false;
                    x = false;
                    C = false;
                    z = true;
                    A = false;
                    B = false;
                    y = false;
                    aa = null;
                }
            }
        }
    }

    static void e(String sessionKey) {
        if (U != null) {
            for (WiGameClient client : U) {
                client.wyLoggedIn(sessionKey);
            }
        }
    }

    static boolean e() {
        if (R == null) {
            return false;
        }
        ConnectivityManager connectivityManager = (ConnectivityManager) R.getSystemService("connectivity");
        if (connectivityManager == null) {
            return true;
        }
        NetworkInfo networkInfo = connectivityManager.getNetworkInfo(1);
        if (networkInfo != null && networkInfo.isAvailable()) {
            return true;
        }
        NetworkInfo networkInfo2 = connectivityManager.getNetworkInfo(0);
        if (networkInfo2 != null) {
            return networkInfo2.isAvailable();
        }
        return false;
    }

    static void f(String name) {
        if (U != null) {
            for (WiGameClient client : U) {
                client.wyGameSaved(name);
            }
        }
    }

    static boolean f() {
        return N != null;
    }

    static void g(String name) {
        if (U != null) {
            for (WiGameClient client : U) {
                client.wyGameSaveFailed(name);
            }
        }
    }

    static boolean g() {
        return N != null;
    }

    public static String getChannel() {
        if (D == null) {
            D = A();
        }
        return D;
    }

    public static Context getContext() {
        return R;
    }

    public static long getFriends(String userId) {
        return f.a(userId, 0, 25);
    }

    public static long getFriends(String userId, int start, int count) {
        return f.a(userId, start, count);
    }

    public static double getLatitude() {
        if (N == null) {
            return 0.0d;
        }
        if (N.getLatitude() < 4.999999873689376E-5d) {
            return 0.0d;
        }
        return N.getLatitude();
    }

    public static double getLongitude() {
        if (N == null) {
            return 0.0d;
        }
        if (N.getLongitude() < 4.999999873689376E-5d) {
            return 0.0d;
        }
        return N.getLongitude();
    }

    public static String getMyId() {
        return (Q != null && !TextUtils.isEmpty(Q.getId())) ? Q.getId() : "";
    }

    public static String getMyName() {
        return Q.getName() == null ? "" : Q.getName();
    }

    public static Bitmap getMyPortrait() {
        return h.b(null, true, h.b("p_", getMyId()), t().getAvatarUrl());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wiyun.game.h.a(java.util.Map<java.lang.String, android.graphics.Bitmap>, boolean, java.lang.String, java.lang.String, boolean):android.graphics.Bitmap
     arg types: [?[OBJECT, ARRAY], int, java.lang.String, java.lang.String, boolean]
     candidates:
      com.wiyun.game.h.a(int, int, int, int, android.net.Uri):android.content.Intent
      com.wiyun.game.h.a(android.content.Context, java.lang.String, int, android.view.View$OnClickListener, int[]):void
      com.wiyun.game.h.a(java.util.Map<java.lang.String, android.graphics.Bitmap>, boolean, java.lang.String, java.lang.String, boolean):android.graphics.Bitmap */
    public static Bitmap getPortrait(User user) {
        return h.a((Map<String, Bitmap>) null, true, h.b("p_", user.getId()), user.getAvatarUrl(), user.isFemale());
    }

    public static Bitmap getPortrait(String str) {
        return h.b(null, true, h.b("p_", str), null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wiyun.game.h.a(java.util.Map<java.lang.String, android.graphics.Bitmap>, boolean, java.lang.String, java.lang.String, boolean):android.graphics.Bitmap
     arg types: [?[OBJECT, ARRAY], int, java.lang.String, java.lang.String, int]
     candidates:
      com.wiyun.game.h.a(int, int, int, int, android.net.Uri):android.content.Intent
      com.wiyun.game.h.a(android.content.Context, java.lang.String, int, android.view.View$OnClickListener, int[]):void
      com.wiyun.game.h.a(java.util.Map<java.lang.String, android.graphics.Bitmap>, boolean, java.lang.String, java.lang.String, boolean):android.graphics.Bitmap */
    public static Bitmap getPortrait(String str, String str2) {
        return h.a((Map<String, Bitmap>) null, true, h.b("p_", str), str2, false);
    }

    public static int getToastSide() {
        return v;
    }

    public static long getUserInfo(String userId) {
        return f.d(userId);
    }

    static void h(String blobPath) {
        if (U != null) {
            for (WiGameClient client : U) {
                client.wyLoadGame(blobPath);
            }
        }
    }

    static boolean h() {
        return f() && g();
    }

    static void i() {
        File file = new File(f, "wiyun_app_config");
        if (file.exists()) {
            String json = h.b(h.b(file));
            if (!TextUtils.isEmpty(json)) {
                try {
                    ai c2 = ai.a(new JSONObject(json));
                    x.a(c2.c());
                    x.b(c2.d());
                } catch (JSONException e2) {
                }
            }
        }
    }

    static void i(String friendId) {
        if (U != null) {
            for (WiGameClient client : U) {
                client.wyFriendRequestSent(friendId);
            }
        }
    }

    public static void init(Context context, String appKey, String secretKey) {
        init(context, appKey, secretKey, false, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wiyun.game.h.a(byte[], byte[], boolean):boolean
     arg types: [byte[], byte[], int]
     candidates:
      com.wiyun.game.h.a(int, java.lang.String, int):int
      com.wiyun.game.h.a(java.lang.String, java.lang.String, int):int
      com.wiyun.game.h.a(android.graphics.Bitmap, int, int):android.graphics.Bitmap
      com.wiyun.game.h.a(byte[], int, int):java.lang.String
      com.wiyun.game.h.a(android.content.Context, java.lang.String, boolean):void
      com.wiyun.game.h.a(java.lang.String, boolean, int):void
      com.wiyun.game.h.a(java.lang.StringBuilder, java.lang.String, java.lang.String):void
      com.wiyun.game.h.a(com.wiyun.game.a.i, java.lang.String, java.lang.String):byte[]
      com.wiyun.game.h.a(com.wiyun.game.a.i, byte[], java.lang.String):byte[]
      com.wiyun.game.h.a(byte[], byte[], boolean):boolean */
    public static void init(Context context, String str, String str2, String str3, boolean z2, boolean z3) {
        if (R != null) {
            destroy(R);
        }
        if (context instanceof Activity) {
            i = ((Activity) context).getRequestedOrientation();
        }
        f.a();
        R = context;
        CookieSyncManager.createInstance(R);
        e = str3;
        f = context.getFilesDir();
        g = context.getCacheDir();
        c = 0;
        p = str;
        q = str2;
        Q = new ag();
        j = z3;
        h = z2 ? 0 : 1;
        ab = new ArrayList();
        U = new ArrayList();
        k = new ArrayList();
        W = new Handler(Looper.getMainLooper()) {
            public void handleMessage(Message msg) {
                WiGame.b(msg);
            }
        };
        byte[] b2 = h.b(new File(f, "wiyun_id"));
        if (b2 != null) {
            Q.i(h.b((i) null, h.b(b2), "wiyun_id"));
            Q.j(h.b((i) null, h.b(h.b(new File(f, "wiyun_name"))), "wiyun_name"));
        }
        i.a(f);
        i();
        com.wiyun.game.a.d.a(R, (d.a) null);
        T = new c(R);
        T.a();
        b();
        com.wiyun.game.b.d.a().c();
        if (TextUtils.isEmpty(getMyId())) {
            x = true;
            f.g();
        } else {
            f.b(getMyId());
        }
        f.c((String) null);
        if (r()) {
            setSandboxMode(true);
        }
        S = true;
        W.sendEmptyMessageDelayed(14, 300000);
        try {
            Class<?> cls = Class.forName("com.wiyun.distribute.DistributeConfig");
            if (cls != null) {
                Field field = cls.getField("channelName");
                if (field != null) {
                    D = (String) field.get(null);
                }
                Field field2 = cls.getField("disableCharge");
                if (field2 != null) {
                    af = field2.getBoolean(null);
                }
                Field field3 = cls.getField("checkRomInfo");
                if (field3 != null) {
                    ah = field3.getBoolean(null);
                }
            }
        } catch (Exception e2) {
        }
        if (ah) {
            String A2 = A();
            if (!TextUtils.isEmpty(A2) && !"unknown".equals(A2)) {
                D = A2;
            }
            if (!o) {
                String B2 = B();
                if (!TextUtils.isEmpty(B2)) {
                    o = m(B2);
                    if (o) {
                        e.a(R, com.wiyun.game.e.a.c(B2));
                    }
                }
            }
        }
        e.b(R, p);
        if (context.checkCallingOrSelfPermission("android.permission.INTERNET") != 0) {
            Toast.makeText(context, "you must add INTERNET permission", 0).show();
        }
        if (context.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") != 0) {
            Toast.makeText(context, "you must add READ_PHONE_STATE permission", 0).show();
        }
        if (context.checkCallingOrSelfPermission("android.permission.ACCESS_WIFI_STATE") != 0) {
            Toast.makeText(context, "you must add ACCESS_WIFI_STATE permission", 0).show();
        }
        if (context.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") != 0) {
            Toast.makeText(context, "you must add android.permission.ACCESS_NETWORK_STATE permission", 0).show();
        }
        if (!"com.wiyun.game".equals(R.getPackageName()) && h.a(h.a(R, t.e("wy_activity_usermap")), h.d("0OY_NeVg2RsyrJJdZLDHjGwb0u-KU29bKFt6YYQ"), true)) {
            Toast.makeText(R, t.f("wy_toast_replace_map_key"), 1).show();
        }
    }

    public static void init(Context context, String appKey, String secretKey, boolean landscape, boolean fullscreen) {
        init(context, appKey, secretKey, d(), landscape, fullscreen);
    }

    public static boolean isAchievementUnlocked(String str) {
        if (R == null) {
            throw new IllegalStateException("You should call init before unlock achievement");
        } else if (!TextUtils.isEmpty(str)) {
            return i.a(str);
        } else {
            throw new IllegalArgumentException("achievement id is empty");
        }
    }

    public static boolean isHideNoticeToast() {
        return ac;
    }

    public static boolean isHideRecharge() {
        return af;
    }

    public static boolean isHideScoreToast() {
        return ae;
    }

    public static boolean isHideWelcomeBackToast() {
        return ad;
    }

    public static boolean isInited() {
        return R != null;
    }

    public static boolean isLoggedIn() {
        return !TextUtils.isEmpty(t);
    }

    public static boolean isSandboxMode() {
        return u;
    }

    public static boolean isSwitchAccountForbidden() {
        return ag;
    }

    static void j() {
        new File(f, "wiyun_id").delete();
    }

    static void j(String userId) {
        if (U != null) {
            for (WiGameClient client : U) {
                client.wySendFriendRequestFailed(userId);
            }
        }
    }

    static void k() {
        new File(f, "wiyun_name").delete();
    }

    static void k(String userId) {
        if (U != null) {
            for (WiGameClient client : U) {
                client.wyPortraitGot(userId);
            }
        }
    }

    static void l() {
        if (T != null) {
            T.c();
        }
    }

    static String m() {
        return p;
    }

    private static boolean m(String str) {
        String[] split = str.split(",");
        PackageManager packageManager = R.getPackageManager();
        int length = split.length;
        int i2 = 0;
        while (i2 < length) {
            try {
                if (packageManager.getPackageGids(split[i2]) == null) {
                    return false;
                }
                i2++;
            } catch (PackageManager.NameNotFoundException e2) {
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: private */
    public static b n(String achievementId) {
        if (ab != null) {
            for (b pa : ab) {
                if (pa.b.equals(achievementId)) {
                    return pa;
                }
            }
        }
        return null;
    }

    static String n() {
        return q;
    }

    static String o() {
        Context context = getContext();
        if (context == null) {
            return null;
        }
        if (context.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") == -1) {
            return null;
        }
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        String line1Number = telephonyManager == null ? null : telephonyManager.getLine1Number();
        if (TextUtils.isEmpty(line1Number)) {
            return null;
        }
        byte[] d2 = h.d(line1Number);
        for (int i2 = 0; i2 < d2.length; i2++) {
            d2[i2] = (byte) (((((d2[i2] >> 4) & 15) | ((d2[i2] & 15) << 4)) ^ 255) & 255);
        }
        return com.wiyun.game.e.c.b(d2);
    }

    private static boolean o(String s2) {
        if (s2 == null) {
            return false;
        }
        int length = s2.length();
        for (int i2 = 0; i2 < length; i2++) {
            if (s2.charAt(i2) != '0') {
                return true;
            }
        }
        return false;
    }

    public static void openAchievements() {
        if (R == null) {
            throw new IllegalStateException("You should call init before open achievements");
        } else if (!e() || !TextUtils.isEmpty(getMyId())) {
            Intent intent = new Intent(R, Home.class);
            intent.setFlags(268435456);
            intent.putExtra("pending_action", 11);
            R.startActivity(intent);
        } else {
            x = true;
            Intent intent2 = new Intent(R, SwitchAccount.class);
            intent2.setFlags(67108864);
            if (a != null) {
                intent2.putParcelableArrayListExtra("bound_users", a);
            }
            R.startActivity(intent2);
            X = 11;
        }
    }

    public static void openDeveloperGames() {
        if (R == null) {
            throw new IllegalStateException("You should call init before open developer games");
        } else if (!e() || !TextUtils.isEmpty(getMyId())) {
            Intent intent = new Intent(R, Home.class);
            intent.setFlags(268435456);
            intent.putExtra("pending_action", 12);
            R.startActivity(intent);
        } else {
            x = true;
            Intent intent2 = new Intent(R, SwitchAccount.class);
            intent2.setFlags(67108864);
            if (a != null) {
                intent2.putParcelableArrayListExtra("bound_users", a);
            }
            R.startActivity(intent2);
            X = 12;
        }
    }

    public static void openDiscussion() {
        if (R == null) {
            throw new IllegalStateException("You should call init before open discussion");
        } else if (!e() || !TextUtils.isEmpty(getMyId())) {
            Intent intent = new Intent(R, Home.class);
            intent.setFlags(268435456);
            intent.putExtra("pending_action", 10);
            R.startActivity(intent);
        } else {
            x = true;
            Intent intent2 = new Intent(R, SwitchAccount.class);
            intent2.setFlags(67108864);
            if (a != null) {
                intent2.putParcelableArrayListExtra("bound_users", a);
            }
            R.startActivity(intent2);
            X = 10;
        }
    }

    public static void openLeaderboard(String str) {
        if (R == null) {
            throw new IllegalStateException("You should call init before open leaderboard");
        } else if (!e() || !TextUtils.isEmpty(getMyId())) {
            Intent intent = new Intent(R, Home.class);
            intent.setFlags(268435456);
            intent.putExtra("pending_action", 2);
            intent.putExtra("lb_id", str);
            R.startActivity(intent);
        } else {
            x = true;
            Intent intent2 = new Intent(R, SwitchAccount.class);
            intent2.setFlags(67108864);
            if (a != null) {
                intent2.putParcelableArrayListExtra("bound_users", a);
            }
            R.startActivity(intent2);
            X = 2;
            Y = str;
        }
    }

    public static void openLeaderboards() {
        if (R == null) {
            throw new IllegalStateException("You should call init before open leaderboard");
        } else if (!e() || !TextUtils.isEmpty(getMyId())) {
            Intent intent = new Intent(R, Home.class);
            intent.setFlags(268435456);
            intent.putExtra("pending_action", 3);
            R.startActivity(intent);
        } else {
            x = true;
            Intent intent2 = new Intent(R, SwitchAccount.class);
            intent2.setFlags(67108864);
            if (a != null) {
                intent2.putParcelableArrayListExtra("bound_users", a);
            }
            R.startActivity(intent2);
            X = 3;
        }
    }

    public static void openMyProfile() {
        if (R == null) {
            throw new IllegalStateException("You should call init before open my profile");
        } else if (!e() || !TextUtils.isEmpty(getMyId())) {
            Intent intent = new Intent(R, Home.class);
            intent.setFlags(268435456);
            intent.putExtra("pending_action", 14);
            R.startActivity(intent);
        } else {
            x = true;
            Intent intent2 = new Intent(R, SwitchAccount.class);
            intent2.setFlags(67108864);
            if (a != null) {
                intent2.putParcelableArrayListExtra("bound_users", a);
            }
            R.startActivity(intent2);
            X = 14;
        }
    }

    public static void openPendingChallenges() {
        if (R == null) {
            throw new IllegalStateException("You should call init before open pending challenges");
        } else if (!e() || !TextUtils.isEmpty(getMyId())) {
            Intent intent = new Intent(R, Home.class);
            intent.setFlags(268435456);
            intent.putExtra("pending_action", 13);
            R.startActivity(intent);
        } else {
            x = true;
            Intent intent2 = new Intent(R, SwitchAccount.class);
            intent2.setFlags(67108864);
            if (a != null) {
                intent2.putParcelableArrayListExtra("bound_users", a);
            }
            R.startActivity(intent2);
            X = 13;
        }
    }

    public static void openPendingFriends() {
        if (R == null) {
            throw new IllegalStateException("You should call init before open pending friends");
        } else if (!e() || !TextUtils.isEmpty(getMyId())) {
            Intent intent = new Intent(R, Home.class);
            intent.setFlags(268435456);
            intent.putExtra("pending_action", 15);
            R.startActivity(intent);
        } else {
            x = true;
            Intent intent2 = new Intent(R, SwitchAccount.class);
            intent2.setFlags(67108864);
            if (a != null) {
                intent2.putParcelableArrayListExtra("bound_users", a);
            }
            R.startActivity(intent2);
            X = 15;
        }
    }

    public static void openShareUI() {
        if (R == null) {
            throw new IllegalStateException("You should call init before open share ui");
        } else if (!e() || !TextUtils.isEmpty(getMyId())) {
            Intent intent = new Intent(R, Home.class);
            intent.setFlags(268435456);
            intent.putExtra("pending_action", 19);
            R.startActivity(intent);
        } else {
            x = true;
            Intent intent2 = new Intent(R, SwitchAccount.class);
            intent2.setFlags(67108864);
            if (a != null) {
                intent2.putParcelableArrayListExtra("bound_users", a);
            }
            R.startActivity(intent2);
            X = 19;
        }
    }

    public static void openUnreadMessage(String str) {
        if (R == null) {
            throw new IllegalStateException("You should call init before open unread messages");
        } else if (!e() || !TextUtils.isEmpty(getMyId())) {
            Intent intent = new Intent(R, Home.class);
            intent.setFlags(268435456);
            intent.putExtra("pending_action", 18);
            intent.putExtra("lb_id", str);
            R.startActivity(intent);
        } else {
            x = true;
            Intent intent2 = new Intent(R, SwitchAccount.class);
            intent2.setFlags(67108864);
            if (a != null) {
                intent2.putParcelableArrayListExtra("bound_users", a);
            }
            R.startActivity(intent2);
            X = 18;
            Y = str;
        }
    }

    public static void openUnreadMessages() {
        if (R == null) {
            throw new IllegalStateException("You should call init before open unread messages");
        } else if (!e() || !TextUtils.isEmpty(getMyId())) {
            Intent intent = new Intent(R, Home.class);
            intent.setFlags(268435456);
            intent.putExtra("pending_action", 16);
            R.startActivity(intent);
        } else {
            x = true;
            Intent intent2 = new Intent(R, SwitchAccount.class);
            intent2.setFlags(67108864);
            if (a != null) {
                intent2.putParcelableArrayListExtra("bound_users", a);
            }
            R.startActivity(intent2);
            X = 16;
        }
    }

    public static void openUnreadNotices() {
        if (R == null) {
            throw new IllegalStateException("You should call init before open unread notices");
        } else if (!e() || !TextUtils.isEmpty(getMyId())) {
            Intent intent = new Intent(R, Home.class);
            intent.setFlags(268435456);
            intent.putExtra("pending_action", 17);
            R.startActivity(intent);
        } else {
            x = true;
            Intent intent2 = new Intent(R, SwitchAccount.class);
            intent2.setFlags(67108864);
            if (a != null) {
                intent2.putParcelableArrayListExtra("bound_users", a);
            }
            R.startActivity(intent2);
            X = 17;
        }
    }

    public static void openWiYunPlaza() {
        if (R == null) {
            throw new IllegalStateException("You should call init before open wiyun plaza");
        } else if (!e() || !TextUtils.isEmpty(getMyId())) {
            Intent intent = new Intent(R, Home.class);
            intent.setFlags(268435456);
            intent.putExtra("pending_action", 20);
            R.startActivity(intent);
        } else {
            x = true;
            Intent intent2 = new Intent(R, SwitchAccount.class);
            intent2.setFlags(67108864);
            if (a != null) {
                intent2.putParcelableArrayListExtra("bound_users", a);
            }
            R.startActivity(intent2);
            X = 20;
        }
    }

    static String p() {
        byte[] b2;
        byte[] b3;
        if (r == null) {
            if (r()) {
                File file = new File(f, "wiyun_fdi");
                String b4 = (!file.exists() || (b2 = h.b(file)) == null || (b3 = h.b(null, b2, "wiyun_fdi")) == null) ? null : h.b(b3);
                if (TextUtils.isEmpty(b4)) {
                    r = "emu" + UUID.randomUUID().toString();
                    h.a(file, h.a((i) null, r, "wiyun_fdi"));
                } else {
                    r = b4;
                }
            } else {
                Context context = getContext();
                if (context == null) {
                    c("Context is not set");
                } else if (context.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") == -1) {
                    c("Cannot get a device ID.  Open manifest.xml and just before the final </manifest> tag add:  <uses-permission android:name=\"android.permission.READ_PHONE_STATE\" />");
                } else {
                    TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
                    if (telephonyManager != null) {
                        r = telephonyManager.getDeviceId();
                        if (!o(r)) {
                            r = q();
                        }
                    } else {
                        r = q();
                        Log.w("WiYun", "No device ID available.");
                    }
                }
            }
        }
        return r;
    }

    static String q() {
        WifiInfo connectionInfo;
        if (!TextUtils.isEmpty(s)) {
            return s;
        }
        Context context = getContext();
        StringBuilder sb = new StringBuilder();
        sb.append(af());
        sb.append(Settings.Secure.getString(context.getContentResolver(), "android_id"));
        sb.append(context.getResources().getDisplayMetrics().density);
        WifiManager wifiManager = (WifiManager) context.getSystemService("wifi");
        if (!(wifiManager == null || (connectionInfo = wifiManager.getConnectionInfo()) == null)) {
            sb.append(connectionInfo.getMacAddress());
        }
        s = h.a(com.wiyun.game.e.a.b(sb.toString()));
        return s;
    }

    static boolean r() {
        if (J == -1) {
            String af2 = af();
            boolean startsWith = Build.FINGERPRINT.startsWith("generic");
            boolean equalsIgnoreCase = "sdk".equalsIgnoreCase(Build.MODEL);
            boolean ag2 = ag();
            int i2 = (TextUtils.isEmpty(af2) || !o(af2)) ? 0 : 0 + 30;
            if (!startsWith) {
                i2 += 30;
            }
            if (!equalsIgnoreCase) {
                i2 += 20;
            }
            if (ag2) {
                i2 += 50;
            }
            J = i2 >= 50 ? 0 : 1;
        }
        return J != 0;
    }

    public static void removeWiGameClient(WiGameClient client) {
        if (U != null) {
            U.remove(client);
        }
    }

    static String s() {
        if (!TextUtils.isEmpty(K)) {
            return K;
        }
        if (R == null) {
            return "";
        }
        Configuration configuration = R.getResources().getConfiguration();
        K = String.valueOf(configuration.mnc + (configuration.mcc * 100));
        return K;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wiyun.game.h.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.wiyun.game.h.a(int, java.lang.String):int
      com.wiyun.game.h.a(java.lang.String, java.lang.String):java.lang.String
      com.wiyun.game.h.a(android.view.View, int):void
      com.wiyun.game.h.a(java.io.File, byte[]):boolean
      com.wiyun.game.h.a(android.content.Context, int):byte[]
      com.wiyun.game.h.a(java.lang.String, boolean):void */
    public static void saveGame(String str, String str2, byte[] bArr, byte[] bArr2, boolean z2) {
        if (R == null) {
            throw new IllegalStateException("You should call init before submit score");
        } else if (e()) {
            l = new a();
            l.c = bArr;
            l.d = bArr2;
            l.a = str;
            l.b = str2;
            l.e = z2;
            if (TextUtils.isEmpty(getMyId())) {
                X = 5;
                Intent intent = new Intent(R, SwitchAccount.class);
                intent.setFlags(67108864);
                if (a != null) {
                    intent.putParcelableArrayListExtra("bound_users", a);
                }
                R.startActivity(intent);
                return;
            }
            X = 5;
            y();
        } else {
            h.a(t.h("wy_label_no_network_to_save_game"), true);
        }
    }

    public static void sendChallenge(String definitionId, int score, byte[] blob) {
        sendChallenge(definitionId, score, blob, null);
    }

    public static void sendChallenge(String str, int i2, byte[] bArr, String str2) {
        if (R == null) {
            throw new IllegalStateException("You should call init before send challenge");
        }
        Intent intent = new Intent(R, SendChallenge.class);
        intent.setFlags(268435456);
        intent.putExtra("def_id", str);
        intent.putExtra("score", i2);
        intent.putExtra("blob", bArr);
        intent.putExtra("leaderboard_id", str2);
        if (TextUtils.isEmpty(getMyId())) {
            Z = intent;
            X = 1;
            Intent intent2 = new Intent(R, SwitchAccount.class);
            intent2.setFlags(67108864);
            if (a != null) {
                intent2.putParcelableArrayListExtra("bound_users", a);
            }
            R.startActivity(intent2);
            return;
        }
        R.startActivity(intent);
    }

    public static void setChannel(String channel) {
        D = channel;
    }

    public static void setForbidSwitchAccount(boolean forbid) {
        ag = forbid;
    }

    public static void setHideNoticeToast(boolean hide) {
        ac = hide;
    }

    public static void setHideRecharge(boolean flag) {
        af = flag;
    }

    public static void setHideScoreToast(boolean hide) {
        ae = hide;
    }

    public static void setHideWelcomeBackToast(boolean hide) {
        ad = hide;
    }

    public static void setHintLogin(boolean hint) {
        C = hint;
    }

    public static void setSandboxMode(boolean sandbox) {
        u = sandbox;
        f.a(sandbox);
    }

    public static void setToastSide(int side) {
        v = side;
    }

    public static void showLoadGameDialog() {
        if (R == null) {
            throw new IllegalStateException("You should call init before start cloud");
        }
        X = 8;
        if (!e() || !TextUtils.isEmpty(getMyId())) {
            y();
            return;
        }
        x = true;
        Intent intent = new Intent(R, SwitchAccount.class);
        intent.setFlags(67108864);
        if (a != null) {
            intent.putParcelableArrayListExtra("bound_users", a);
        }
        R.startActivity(intent);
    }

    public static void showSwitchAccountDialog() {
        Intent intent = new Intent(R, SwitchAccount.class);
        intent.setFlags(67108864);
        if (a != null) {
            intent.putParcelableArrayListExtra("bound_users", a);
        }
        R.startActivity(intent);
    }

    public static void startUI() {
        if (R == null) {
            throw new IllegalStateException("You should call init before start cloud");
        } else if (!e() || !TextUtils.isEmpty(getMyId())) {
            Intent intent = new Intent(R, Home.class);
            intent.setFlags(268435456);
            R.startActivity(intent);
        } else {
            x = true;
            Intent intent2 = new Intent(R, SwitchAccount.class);
            intent2.setFlags(67108864);
            if (a != null) {
                intent2.putParcelableArrayListExtra("bound_users", a);
            }
            R.startActivity(intent2);
            X = 4;
        }
    }

    public static void submitChallengeResult(String str, int i2, int i3, byte[] bArr) {
        if (R == null) {
            throw new IllegalStateException("You should call init before send challenge");
        } else if (str == null) {
            throw new IllegalArgumentException("ChallengeToUser id is empty");
        } else if ("test_ctu_id".equals(str)) {
            W.sendEmptyMessage(4);
        } else if (aa != null) {
            Log.w("WiYun", "another challenge result is in submitting, so quickly you finish another challenge?");
        } else if (TextUtils.isEmpty(getMyId())) {
            Log.w("WiYun", "There is no bound user found, where do you receive this challenge?");
        } else {
            aa = new ChallengeResult();
            aa.setUserId(getMyId());
            aa.setCtuId(str);
            aa.setScore(i3);
            aa.setResult(i2 > 0 ? 1 : i2 < 0 ? -1 : 0);
            aa.setBlob(bArr);
            ac();
        }
    }

    public static void submitScore(String leaderboardId, int score, byte[] blob) {
        submitScore(leaderboardId, score, blob, false);
    }

    public static void submitScore(String str, int i2, byte[] bArr, boolean z2) {
        if (R == null) {
            throw new IllegalStateException("You should call init before submit score");
        } else if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException("leaderboard id is empty");
        } else {
            d dVar = new d();
            dVar.a = getMyId();
            dVar.b = str;
            dVar.c = i2;
            dVar.d = bArr;
            dVar.e = System.currentTimeMillis();
            dVar.f = getLatitude();
            dVar.g = getLongitude();
            dVar.i = z2;
            if (!e()) {
                if (!ae) {
                    W.sendEmptyMessage(8);
                }
                i.a(dVar);
            } else if (TextUtils.isEmpty(getMyId())) {
                X = 9;
                k.add(dVar);
                Intent intent = new Intent(R, SwitchAccount.class);
                intent.setFlags(67108864);
                if (a != null) {
                    intent.putParcelableArrayListExtra("bound_users", a);
                }
                R.startActivity(intent);
            } else if (z2) {
                k.add(dVar);
                dVar.j = f.a(dVar.b, dVar.c, dVar.d, dVar.f, dVar.g);
            } else {
                Intent intent2 = new Intent(R, SubmitScore.class);
                intent2.putExtra("pending_score", dVar);
                R.startActivity(intent2);
            }
        }
    }

    static ag t() {
        return Q;
    }

    public static void testPlayChallenge(ChallengeRequest challengeRequest) {
        if (R != null) {
            challengeRequest.setCtuId("test_ctu_id");
            W.sendMessage(Message.obtain(W, 1014, challengeRequest));
            R.sendBroadcast(new Intent("com.wiyun.game.RESET"));
        }
    }

    static String u() {
        return t;
    }

    public static void unlockAchievement(String str) {
        if (R == null) {
            throw new IllegalStateException("You should call init before unlock achievement");
        } else if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException("achievement id is empty");
        } else if (n(str) != null) {
            Log.w("WiYun", "same achievement is in unlocking, you want to unlock twice?");
        } else if (!i.a(str)) {
            b bVar = new b();
            bVar.a = getMyId();
            bVar.b = str;
            bVar.c = System.currentTimeMillis();
            com.wiyun.game.model.a.f a2 = x.a(str);
            if (a2 == null) {
                W.sendEmptyMessage(9);
            } else {
                W.sendMessage(Message.obtain(W, 9, a2.b()));
            }
            if (TextUtils.isEmpty(getMyId())) {
                i.a(bVar);
                return;
            }
            ab.add(bVar);
            f.e(bVar.b);
        }
    }

    static boolean v() {
        return w;
    }

    static Handler w() {
        return W;
    }

    public static void wyGameSaveProgress(String name, int uploadedSize) {
        if (U != null) {
            for (WiGameClient client : U) {
                client.wyGameSaveProgress(name, uploadedSize);
            }
        }
    }

    static void x() {
        Z = null;
    }

    static void y() {
        switch (X) {
            case 1:
                R.startActivity(Z);
                Z = null;
                break;
            case 2:
            case 3:
            case MazeGame.WIDTHREDUCTION:
            case 11:
            case 12:
            case 13:
            case 14:
            case MazeGame.MAXBLOCKSIZE:
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
                if (!Home.a) {
                    Intent intent = new Intent(R, Home.class);
                    intent.setFlags(268435456);
                    intent.putExtra("pending_action", X);
                    if (X == 2) {
                        intent.putExtra("lb_id", Y);
                    } else if (X == 18) {
                        intent.putExtra("lb_id", Y);
                    }
                    R.startActivity(intent);
                    Y = null;
                    break;
                }
                break;
            case 4:
                R.startActivity(new Intent(R, Home.class));
                break;
            case 5:
                if (l.e) {
                    Intent intent2 = new Intent(R, SaveGameDialog.class);
                    intent2.putExtra("blob", l.c);
                    intent2.putExtra("image", l.d);
                    intent2.putExtra("name", l.a);
                    intent2.putExtra("description", l.b);
                    intent2.putExtra("message", t.h("wy_label_saving_game_data"));
                    R.startActivity(intent2);
                } else {
                    f.a((String) null, l.a, l.b, l.c, l.d);
                }
                l = null;
                break;
            case R.styleable.com_wiyun_ad_AdView_testAdType:
                R.startActivity(new Intent(R, LoadGameDialog.class));
                break;
            case R.styleable.com_wiyun_ad_AdView_transition:
                if (k != null) {
                    for (d next : k) {
                        if (next.j == 0) {
                            if (next.i) {
                                next.j = f.a(next.b, next.c, next.d, next.f, next.g);
                            } else {
                                Intent intent3 = new Intent(R, SubmitScore.class);
                                intent3.putExtra("pending_score", next);
                                R.startActivity(intent3);
                            }
                        }
                    }
                    break;
                }
                break;
        }
        X = 0;
    }

    static void z() {
        if (U != null) {
            for (WiGameClient client : U) {
                client.wyLogInFailed();
            }
        }
    }
}
