package com.wiyun.game;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Message;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import com.wiyun.game.a.d;
import java.io.File;
import java.lang.reflect.Field;
import java.util.LinkedList;
import java.util.Queue;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpProtocolParams;

class c implements Runnable {
    private Queue<a> a;
    private BitmapFactory.Options b = new BitmapFactory.Options();
    private boolean c;
    private boolean d;

    private static final class a {
        String a;
        String b;

        private a() {
        }

        /* synthetic */ a(a aVar) {
            this();
        }
    }

    c(Context context) {
        if (WiGame.n) {
            try {
                Field inDensityField = BitmapFactory.Options.class.getField("inDensity");
                Field inTargetDensityField = BitmapFactory.Options.class.getField("inTargetDensity");
                Field densityDpiField = DisplayMetrics.class.getField("densityDpi");
                if (!(inDensityField == null || inTargetDensityField == null || densityDpiField == null)) {
                    DisplayMetrics dm = context.getResources().getDisplayMetrics();
                    inDensityField.set(this.b, 160);
                    inTargetDensityField.set(this.b, densityDpiField.get(dm));
                }
            } catch (Exception e) {
            }
        }
        this.a = new LinkedList();
        this.c = true;
    }

    private void a(String str, String str2) {
        Intent intent = new Intent(str);
        intent.putExtra("image_id", str2);
        intent.setFlags(1073741824);
        WiGame.a(intent);
    }

    /* JADX WARNING: Removed duplicated region for block: B:69:0x009e A[SYNTHETIC, Splitter:B:69:0x009e] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:44:0x0072=Splitter:B:44:0x0072, B:31:0x0057=Splitter:B:31:0x0057} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean a(org.apache.http.HttpResponse r12, java.io.File r13) throws java.io.IOException {
        /*
            r11 = this;
            r10 = 0
            r8 = 4096(0x1000, float:5.74E-42)
            byte[] r0 = new byte[r8]
            org.apache.http.HttpEntity r2 = r12.getEntity()
            java.io.InputStream r6 = r2.getContent()
            if (r6 != 0) goto L_0x0011
            r8 = r10
        L_0x0010:
            return r8
        L_0x0011:
            boolean r8 = r13.exists()
            if (r8 == 0) goto L_0x001f
            boolean r8 = r13.delete()
            if (r8 != 0) goto L_0x001f
            r8 = r10
            goto L_0x0010
        L_0x001f:
            java.io.File r7 = r13.getParentFile()
            boolean r8 = r7.exists()
            if (r8 != 0) goto L_0x0031
            boolean r8 = r7.mkdirs()
            if (r8 != 0) goto L_0x0031
            r8 = r10
            goto L_0x0010
        L_0x0031:
            r3 = 0
            java.io.FileOutputStream r4 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0085 }
            r4.<init>(r13)     // Catch:{ Exception -> 0x0085 }
            r5 = 0
        L_0x0038:
            r8 = -1
            if (r5 != r8) goto L_0x0046
            if (r4 == 0) goto L_0x00b6
            r4.flush()     // Catch:{ IOException -> 0x00a6 }
            r4.close()     // Catch:{ IOException -> 0x00a6 }
            r3 = 0
        L_0x0044:
            r8 = 1
            goto L_0x0010
        L_0x0046:
            r8 = 0
            r4.write(r0, r8, r5)     // Catch:{ Exception -> 0x00b0, all -> 0x00ab }
            boolean r8 = r11.c     // Catch:{ Exception -> 0x00b0, all -> 0x00ab }
            if (r8 == 0) goto L_0x0065
            if (r4 == 0) goto L_0x00ba
            r4.flush()     // Catch:{ Exception -> 0x00b0, all -> 0x00ab }
            r4.close()     // Catch:{ Exception -> 0x00b0, all -> 0x00ab }
            r3 = 0
        L_0x0057:
            r13.delete()     // Catch:{ Exception -> 0x0085 }
        L_0x005a:
            if (r3 == 0) goto L_0x0063
            r3.flush()     // Catch:{ IOException -> 0x00b4 }
            r3.close()     // Catch:{ IOException -> 0x00b4 }
            r3 = 0
        L_0x0063:
            r8 = r10
            goto L_0x0010
        L_0x0065:
            boolean r8 = r11.d     // Catch:{ Exception -> 0x00b0, all -> 0x00ab }
            if (r8 == 0) goto L_0x0096
            if (r4 == 0) goto L_0x00b8
            r4.flush()     // Catch:{ Exception -> 0x00b0, all -> 0x00ab }
            r4.close()     // Catch:{ Exception -> 0x00b0, all -> 0x00ab }
            r3 = 0
        L_0x0072:
            r13.delete()     // Catch:{ Exception -> 0x0085 }
            java.util.Queue<com.wiyun.game.c$a> r8 = r11.a     // Catch:{ Exception -> 0x0085 }
            monitor-enter(r8)     // Catch:{ Exception -> 0x0085 }
            r9 = 0
            r11.d = r9     // Catch:{ all -> 0x0082 }
            java.util.Queue<com.wiyun.game.c$a> r9 = r11.a     // Catch:{ all -> 0x0082 }
            r9.notify()     // Catch:{ all -> 0x0082 }
            monitor-exit(r8)     // Catch:{ all -> 0x0082 }
            goto L_0x005a
        L_0x0082:
            r9 = move-exception
            monitor-exit(r8)     // Catch:{ all -> 0x0082 }
            throw r9     // Catch:{ Exception -> 0x0085 }
        L_0x0085:
            r8 = move-exception
            r1 = r8
        L_0x0087:
            r13.delete()     // Catch:{ all -> 0x009b }
            if (r3 == 0) goto L_0x0093
            r3.flush()     // Catch:{ IOException -> 0x00ae }
            r3.close()     // Catch:{ IOException -> 0x00ae }
            r3 = 0
        L_0x0093:
            r8 = r10
            goto L_0x0010
        L_0x0096:
            int r5 = r6.read(r0)     // Catch:{ Exception -> 0x00b0, all -> 0x00ab }
            goto L_0x0038
        L_0x009b:
            r8 = move-exception
        L_0x009c:
            if (r3 == 0) goto L_0x00a5
            r3.flush()     // Catch:{ IOException -> 0x00a9 }
            r3.close()     // Catch:{ IOException -> 0x00a9 }
            r3 = 0
        L_0x00a5:
            throw r8
        L_0x00a6:
            r8 = move-exception
            r3 = r4
            goto L_0x0044
        L_0x00a9:
            r9 = move-exception
            goto L_0x00a5
        L_0x00ab:
            r8 = move-exception
            r3 = r4
            goto L_0x009c
        L_0x00ae:
            r8 = move-exception
            goto L_0x0093
        L_0x00b0:
            r8 = move-exception
            r1 = r8
            r3 = r4
            goto L_0x0087
        L_0x00b4:
            r8 = move-exception
            goto L_0x0063
        L_0x00b6:
            r3 = r4
            goto L_0x0044
        L_0x00b8:
            r3 = r4
            goto L_0x0072
        L_0x00ba:
            r3 = r4
            goto L_0x0057
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wiyun.game.c.a(org.apache.http.HttpResponse, java.io.File):boolean");
    }

    private DefaultHttpClient d() {
        HttpHost d2;
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        HttpProtocolParams.setVersion(basicHttpParams, HttpVersion.HTTP_1_0);
        HttpProtocolParams.setContentCharset(basicHttpParams, "UTF-8");
        HttpProtocolParams.setUseExpectContinue(basicHttpParams, false);
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, 10000);
        HttpConnectionParams.setSoTimeout(basicHttpParams, 10000);
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient(basicHttpParams);
        if (!d.b() && d.c() && (d2 = d.d()) != null) {
            defaultHttpClient.getParams().setParameter("http.route.default-proxy", d2);
        }
        return defaultHttpClient;
    }

    public Bitmap a(String id, String url, int defaultId) {
        Bitmap bitmap = null;
        if (!TextUtils.isEmpty(id)) {
            File dst = new File(WiGame.g, id);
            if (dst.exists()) {
                try {
                    bitmap = BitmapFactory.decodeFile(dst.getAbsolutePath(), this.b);
                } catch (OutOfMemoryError e) {
                }
            } else if (!TextUtils.isEmpty(url)) {
                synchronized (this.a) {
                    a entry = new a(null);
                    entry.a = url;
                    entry.b = id;
                    this.a.offer(entry);
                    this.a.notify();
                }
            }
        }
        if (bitmap != null || defaultId == 0) {
            return bitmap;
        }
        try {
            return BitmapFactory.decodeResource(WiGame.getContext().getResources(), defaultId);
        } catch (Throwable th) {
            return bitmap;
        }
    }

    public void a() {
        if (this.c) {
            this.c = false;
            Thread t = new Thread(this);
            t.setPriority(1);
            t.setDaemon(true);
            t.setName("Image manager");
            t.start();
        }
    }

    public void a(String userId) {
        File dst = new File(WiGame.g, userId);
        if (dst.exists()) {
            dst.delete();
        }
    }

    public void b() {
        if (!this.c) {
            synchronized (this.a) {
                this.c = true;
                this.a.notify();
            }
        }
    }

    public void c() {
        if (!this.c) {
            synchronized (this.a) {
                this.d = true;
                this.a.clear();
                this.a.notify();
            }
        }
    }

    public void run() {
        a peek;
        File[] listFiles = WiGame.g.listFiles();
        if (listFiles != null) {
            long currentTimeMillis = System.currentTimeMillis();
            for (File file : listFiles) {
                if (file.lastModified() - currentTimeMillis > 172800000) {
                    file.delete();
                }
            }
        }
        DefaultHttpClient d2 = d();
        while (!this.c) {
            synchronized (this.a) {
                if (this.a.isEmpty()) {
                    try {
                        this.a.wait();
                    } catch (InterruptedException e) {
                    }
                }
                peek = this.a.peek();
            }
            if (peek != null && !this.c) {
                File file2 = new File(WiGame.g, peek.b);
                try {
                    HttpResponse execute = d2.execute(new HttpGet(h.e(peek.a)));
                    if (execute.getStatusLine().getStatusCode() < 300 && a(execute, file2)) {
                        a("com.wiyun.game.IMAGE_DOWNLOADED", peek.b);
                        if ("p_".equals(h.f(peek.b))) {
                            WiGame.w().sendMessage(Message.obtain(WiGame.w(), 1010, h.g(peek.b)));
                        }
                    }
                } catch (Exception e2) {
                    Log.w("WiYun", "failed to download avatar for " + peek.b);
                }
                synchronized (this.a) {
                    this.a.poll();
                }
            } else if (peek == null) {
                this.d = false;
            }
        }
        d2.getConnectionManager().shutdown();
    }
}
