package com.wiyun.game.b;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class d implements Runnable {
    private static d a;
    private List<b> b = new LinkedList();
    private List<b> c = new LinkedList();
    private Queue<a> d = new LinkedList();
    private List<a> e = new LinkedList();
    private Queue<e> f = new LinkedList();
    private boolean g;
    private boolean h;
    private boolean i;

    private d() {
    }

    public static d a() {
        if (a == null) {
            a = new d();
        }
        return a;
    }

    public void a(a i2) {
        synchronized (this.d) {
            this.d.offer(i2);
            this.i = true;
        }
    }

    public void a(b l) {
        synchronized (this.b) {
            this.b.add(0, l);
            this.h = true;
        }
    }

    public void a(e e2) {
        synchronized (this.f) {
            this.f.offer(e2);
            this.f.notify();
        }
    }

    public void b() {
        synchronized (this.d) {
            this.d.clear();
            this.i = true;
        }
        synchronized (this.b) {
            this.b.clear();
            this.h = true;
        }
    }

    public void b(a i2) {
        synchronized (this.d) {
            this.d.remove(i2);
            this.i = true;
        }
    }

    public void b(b l) {
        synchronized (this.b) {
            if (!this.b.isEmpty()) {
                this.b.remove(l);
                this.h = true;
            }
        }
    }

    public void c() {
        this.g = false;
        Thread t = new Thread(this);
        t.setName("WiGame Event Dispatcher");
        t.setDaemon(true);
        t.start();
    }

    public void d() {
        synchronized (this.f) {
            this.g = true;
            this.f.notify();
        }
    }

    public void run() {
        e e2;
        while (!this.g) {
            synchronized (this.f) {
                if (this.f.isEmpty()) {
                    try {
                        this.f.wait();
                    } catch (InterruptedException e3) {
                    }
                }
                e2 = this.g ? null : this.f.poll();
            }
            if (e2 != null) {
                if (this.i) {
                    synchronized (this.d) {
                        this.e.clear();
                        this.e.addAll(this.d);
                        this.i = false;
                    }
                }
                int size = this.e.size();
                for (int i2 = 0; i2 < size && e2.d; i2++) {
                    this.e.get(i2).a(e2);
                }
                if (!e2.d) {
                    continue;
                } else {
                    if (this.h) {
                        synchronized (this.b) {
                            this.c.clear();
                            this.c.addAll(this.b);
                            this.h = false;
                        }
                    }
                    int size2 = this.c.size();
                    for (int i3 = 0; i3 < size2 && e2.d; i3++) {
                        this.c.get(i3).b(e2);
                    }
                }
            }
        }
    }
}
