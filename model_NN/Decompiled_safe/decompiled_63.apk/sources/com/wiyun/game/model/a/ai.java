package com.wiyun.game.model.a;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class ai {
    private boolean a;
    private boolean b;
    private List<f> c;
    private List<c> d;

    public static ai a(JSONObject dict) {
        ai c2 = new ai();
        c2.a(dict.optBoolean("hint_register", true));
        c2.b(dict.optBoolean("register_cancelable", false));
        List achievements = new ArrayList();
        JSONArray achArray = dict.optJSONArray("achievements");
        if (achArray != null) {
            int count = achArray.length();
            for (int i = 0; i < count; i++) {
                JSONObject json = achArray.optJSONObject(i);
                if (json != null) {
                    achievements.add(f.a(json));
                }
            }
        }
        c2.a(achievements);
        List leaderboards = new ArrayList();
        JSONArray lbArray = dict.optJSONArray("leaderboards");
        if (lbArray != null) {
            int count2 = lbArray.length();
            for (int i2 = 0; i2 < count2; i2++) {
                JSONObject json2 = lbArray.optJSONObject(i2);
                if (json2 != null) {
                    leaderboards.add(c.a(json2));
                }
            }
        }
        c2.b(leaderboards);
        return c2;
    }

    public void a(List<f> mAchievements) {
        this.c = mAchievements;
    }

    public void a(boolean hintRegister) {
        this.a = hintRegister;
    }

    public boolean a() {
        return this.a;
    }

    public void b(List<c> mLeaderboards) {
        this.d = mLeaderboards;
    }

    public void b(boolean registerCancelable) {
        this.b = registerCancelable;
    }

    public boolean b() {
        return this.b;
    }

    public List<f> c() {
        return this.c;
    }

    public List<c> d() {
        return this.d;
    }
}
