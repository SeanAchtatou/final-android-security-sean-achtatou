package com.wiyun.game.model;

public class User {
    protected String a;
    protected String b;
    protected boolean c;
    protected int d;
    protected int e;
    protected int f;
    protected int g;
    protected int h;
    protected String i;
    protected String j;
    protected boolean k;
    protected double l;
    protected double m;
    protected String n;
    protected int o;
    protected int p;
    protected int q;
    protected int r;
    protected int s;
    protected int t;

    public int getAppCount() {
        return this.h;
    }

    public int getAppPendingChallengeCount() {
        return this.s;
    }

    public String getAvatarUrl() {
        return this.n;
    }

    public int getCoins() {
        return this.e;
    }

    public int getFriendCount() {
        return this.g;
    }

    public int getHonor() {
        return this.d;
    }

    public String getId() {
        return this.a;
    }

    public String getLastAppId() {
        return this.j;
    }

    public String getLastAppName() {
        return this.i;
    }

    public double getLatitude() {
        return this.l;
    }

    public double getLongitude() {
        return this.m;
    }

    public String getName() {
        return this.b;
    }

    public int getNoticeCount() {
        return this.t;
    }

    public int getPendingChallengeCount() {
        return this.r;
    }

    public int getPendingFriendCount() {
        return this.p;
    }

    public int getPendingSharingCount() {
        return this.q;
    }

    public int getUnreadMessageCount() {
        return this.o;
    }

    public boolean isFemale() {
        return this.c;
    }

    public boolean isFriend() {
        return this.k;
    }

    public boolean isOnline() {
        return this.f == 1;
    }
}
