package com.wiyun.game.model.a;

import android.text.TextUtils;
import org.json.JSONObject;

public class t {
    private String a;
    private String b;
    private int c;

    public static final t a(JSONObject dict) {
        String id = dict.optString("id");
        if (TextUtils.isEmpty(id)) {
            return null;
        }
        t c2 = new t();
        c2.a(id);
        c2.b(dict.optString("name"));
        c2.a(dict.optInt("exchange"));
        return c2;
    }

    public void a(int mExchangeRate) {
        this.c = mExchangeRate;
    }

    public void a(String mId) {
        this.a = mId;
    }

    public void b(String mName) {
        this.b = mName;
    }
}
