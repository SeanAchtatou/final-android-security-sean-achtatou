package com.wiyun.game.model.a;

import android.text.TextUtils;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class y {
    private String a;
    private String b;
    private String c;
    private String d;
    private String e;
    private float f;
    private String g;
    private String h;
    private String i;
    private String[] j;
    private List<n> k;
    private List<w> l;

    public static final y a(JSONObject dict) {
        y info = new y();
        String id = dict.optString("app_id");
        if (TextUtils.isEmpty(id)) {
            return null;
        }
        info.a(id);
        info.b(dict.optString("app_icon"));
        info.c(dict.optString("app_name"));
        info.d(dict.optString("seller"));
        if (TextUtils.isEmpty(info.a())) {
            info.d(dict.optString("username"));
        }
        info.e(dict.optString("package_id"));
        info.a((float) dict.optDouble("price"));
        info.f(dict.optString("price_unit"));
        info.g(dict.optString("short_decription"));
        info.h(dict.optString("decription"));
        String platforms = dict.optString("platform", "android");
        if (platforms.indexOf(44) == -1) {
            info.a(new String[]{platforms});
        } else {
            info.a(platforms.split(","));
        }
        List shots = new ArrayList();
        try {
            JSONArray shotArray = dict.optJSONArray("screenshots");
            if (shotArray != null) {
                int count = shotArray.length();
                for (int i2 = 0; i2 < count; i2++) {
                    n s = n.a(shotArray.getJSONObject(i2));
                    if (s != null) {
                        shots.add(s);
                    }
                }
            }
        } catch (JSONException e2) {
        }
        info.a(shots);
        List links = new ArrayList();
        try {
            JSONArray linkArray = dict.optJSONArray("downloads");
            if (linkArray != null) {
                int count2 = linkArray.length();
                for (int i3 = 0; i3 < count2; i3++) {
                    w l2 = w.a(linkArray.getJSONObject(i3));
                    if (l2 != null) {
                        links.add(l2);
                    }
                }
            }
        } catch (JSONException e3) {
        }
        info.b(links);
        return info;
    }

    public String a() {
        return this.d;
    }

    public void a(float price) {
        this.f = price;
    }

    public void a(String id) {
        this.a = id;
    }

    public void a(List<n> screenshots) {
        this.k = screenshots;
    }

    public void a(String[] platforms) {
        this.j = platforms;
    }

    public void b(String iconUrl) {
        this.b = iconUrl;
    }

    public void b(List<w> marketLinks) {
        this.l = marketLinks;
    }

    public void c(String name) {
        this.c = name;
    }

    public void d(String seller) {
        this.d = seller;
    }

    public void e(String packageName) {
        this.e = packageName;
    }

    public void f(String currency) {
        this.g = currency;
    }

    public void g(String shortDesc) {
        this.h = shortDesc;
    }

    public void h(String description) {
        this.i = description;
    }
}
