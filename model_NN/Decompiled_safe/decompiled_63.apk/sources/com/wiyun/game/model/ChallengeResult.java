package com.wiyun.game.model;

public class ChallengeResult {
    private String a;
    private String b;
    private int c;
    private int d;
    private byte[] e;

    public byte[] getBlob() {
        return this.e;
    }

    public String getCtuId() {
        return this.b;
    }

    public int getResult() {
        return this.d;
    }

    public int getScore() {
        return this.c;
    }

    public String getUserId() {
        return this.a;
    }

    public void setBlob(byte[] blob) {
        this.e = blob;
    }

    public void setCtuId(String ctuId) {
        this.b = ctuId;
    }

    public void setResult(int result) {
        this.d = result;
    }

    public void setScore(int score) {
        this.c = score;
    }

    public void setUserId(String userId) {
        this.a = userId;
    }
}
