package com.wiyun.game.d;

import android.graphics.Bitmap;
import android.graphics.Matrix;

public class c {
    private Bitmap a;
    private int b = 0;

    public c(Bitmap bitmap) {
        this.a = bitmap;
    }

    public int a() {
        return this.b;
    }

    public void a(int rotation) {
        this.b = rotation;
    }

    public void a(Bitmap bitmap) {
        this.a = bitmap;
    }

    public Bitmap b() {
        return this.a;
    }

    public Matrix c() {
        Matrix matrix = new Matrix();
        if (this.b != 0) {
            matrix.preTranslate((float) (-(this.a.getWidth() / 2)), (float) (-(this.a.getHeight() / 2)));
            matrix.postRotate((float) this.b);
            matrix.postTranslate((float) (f() / 2), (float) (e() / 2));
        }
        return matrix;
    }

    public boolean d() {
        return (this.b / 90) % 2 != 0;
    }

    public int e() {
        return d() ? this.a.getWidth() : this.a.getHeight();
    }

    public int f() {
        return d() ? this.a.getHeight() : this.a.getWidth();
    }
}
