package com.wiyun.game.d;

import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.graphics.drawable.Drawable;
import android.view.View;
import com.wiyun.game.t;

public class g {
    View a;
    public boolean b;
    boolean c;
    public Rect d;
    public RectF e;
    public Matrix f;
    private a g = a.None;
    private RectF h;
    private boolean i = false;
    private float j;
    private boolean k = false;
    private Drawable l;
    private Drawable m;
    private Drawable n;
    private final Paint o = new Paint();
    private final Paint p = new Paint();
    private final Paint q = new Paint();

    public enum a {
        None,
        Move,
        Grow
    }

    public g(View ctx) {
        this.a = ctx;
    }

    private void d() {
        Resources resources = this.a.getResources();
        this.l = resources.getDrawable(t.c("wy_camera_crop_width"));
        this.m = resources.getDrawable(t.c("wy_camera_crop_height"));
        this.n = resources.getDrawable(t.c("wy_indicator_autocrop"));
    }

    private Rect e() {
        RectF r = new RectF(this.e.left, this.e.top, this.e.right, this.e.bottom);
        this.f.mapRect(r);
        return new Rect(Math.round(r.left), Math.round(r.top), Math.round(r.right), Math.round(r.bottom));
    }

    public int a(float f2, float f3) {
        Rect e2 = e();
        if (this.k) {
            float centerX = f2 - ((float) e2.centerX());
            float centerY = f3 - ((float) e2.centerY());
            int sqrt = (int) Math.sqrt((double) ((centerX * centerX) + (centerY * centerY)));
            int width = this.d.width() / 2;
            return ((float) Math.abs(sqrt - width)) <= 20.0f ? Math.abs(centerY) > Math.abs(centerX) ? centerY < 0.0f ? 8 : 16 : centerX < 0.0f ? 2 : 4 : sqrt < width ? 32 : 1;
        }
        boolean z = f3 >= ((float) e2.top) - 20.0f && f3 < ((float) e2.bottom) + 20.0f;
        boolean z2 = f2 >= ((float) e2.left) - 20.0f && f2 < ((float) e2.right) + 20.0f;
        int i2 = (Math.abs(((float) e2.left) - f2) >= 20.0f || !z) ? 1 : 1 | 2;
        int i3 = (Math.abs(((float) e2.right) - f2) >= 20.0f || !z) ? i2 : i2 | 4;
        if (Math.abs(((float) e2.top) - f3) < 20.0f && z2) {
            i3 |= 8;
        }
        if (Math.abs(((float) e2.bottom) - f3) < 20.0f && z2) {
            i3 |= 16;
        }
        if (i3 != 1 || !e2.contains((int) f2, (int) f3)) {
            return i3;
        }
        return 32;
    }

    public void a(int edge, float dx, float dy) {
        Rect r = e();
        if (edge != 1) {
            if (edge == 32) {
                b((this.e.width() / ((float) r.width())) * dx, (this.e.height() / ((float) r.height())) * dy);
                return;
            }
            if ((edge & 6) == 0) {
                dx = 0.0f;
            }
            if ((edge & 24) == 0) {
                dy = 0.0f;
            }
            c(((float) ((edge & 2) != 0 ? -1 : 1)) * dx * (this.e.width() / ((float) r.width())), ((float) ((edge & 8) != 0 ? -1 : 1)) * dy * (this.e.height() / ((float) r.height())));
        }
    }

    public void a(Canvas canvas) {
        if (!this.c) {
            canvas.save();
            Path path = new Path();
            if (!a()) {
                this.q.setColor(-16777216);
                canvas.drawRect(this.d, this.q);
                return;
            }
            Rect viewDrawingRect = new Rect();
            this.a.getDrawingRect(viewDrawingRect);
            if (this.k) {
                float width = (float) this.d.width();
                path.addCircle(((float) this.d.left) + (width / 2.0f), ((float) this.d.top) + (((float) this.d.height()) / 2.0f), width / 2.0f, Path.Direction.CW);
                this.q.setColor(-1112874);
            } else {
                path.addRect(new RectF(this.d), Path.Direction.CW);
                this.q.setColor(-30208);
            }
            canvas.clipPath(path, Region.Op.DIFFERENCE);
            canvas.drawRect(viewDrawingRect, a() ? this.o : this.p);
            canvas.restore();
            canvas.drawPath(path, this.q);
            if (this.g != a.Grow) {
                return;
            }
            if (this.k) {
                int width2 = this.n.getIntrinsicWidth();
                int height = this.n.getIntrinsicHeight();
                int d2 = (int) Math.round(Math.cos(0.7853981633974483d) * (((double) this.d.width()) / 2.0d));
                int x = ((this.d.left + (this.d.width() / 2)) + d2) - (width2 / 2);
                int y = ((this.d.top + (this.d.height() / 2)) - d2) - (height / 2);
                this.n.setBounds(x, y, this.n.getIntrinsicWidth() + x, this.n.getIntrinsicHeight() + y);
                this.n.draw(canvas);
                return;
            }
            int left = this.d.left + 1;
            int right = this.d.right + 1;
            int top = this.d.top + 4;
            int bottom = this.d.bottom + 3;
            int widthWidth = this.l.getIntrinsicWidth() / 2;
            int widthHeight = this.l.getIntrinsicHeight() / 2;
            int heightHeight = this.m.getIntrinsicHeight() / 2;
            int heightWidth = this.m.getIntrinsicWidth() / 2;
            int xMiddle = this.d.left + ((this.d.right - this.d.left) / 2);
            int yMiddle = this.d.top + ((this.d.bottom - this.d.top) / 2);
            this.l.setBounds(left - widthWidth, yMiddle - widthHeight, left + widthWidth, yMiddle + widthHeight);
            this.l.draw(canvas);
            this.l.setBounds(right - widthWidth, yMiddle - widthHeight, right + widthWidth, yMiddle + widthHeight);
            this.l.draw(canvas);
            this.m.setBounds(xMiddle - heightWidth, top - heightHeight, xMiddle + heightWidth, top + heightHeight);
            this.m.draw(canvas);
            this.m.setBounds(xMiddle - heightWidth, bottom - heightHeight, xMiddle + heightWidth, bottom + heightHeight);
            this.m.draw(canvas);
        }
    }

    public void a(Matrix matrix, Rect rect, RectF rectF, boolean z, boolean z2) {
        boolean z3 = z ? true : z2;
        this.f = new Matrix(matrix);
        this.e = rectF;
        this.h = new RectF(rect);
        this.i = z3;
        this.k = z;
        this.j = this.e.width() / this.e.height();
        this.d = e();
        this.o.setARGB(125, 50, 50, 50);
        this.p.setARGB(125, 50, 50, 50);
        this.q.setStrokeWidth(3.0f);
        this.q.setStyle(Paint.Style.STROKE);
        this.q.setAntiAlias(true);
        this.g = a.None;
        d();
    }

    public void a(a mode) {
        if (mode != this.g) {
            this.g = mode;
            this.a.invalidate();
        }
    }

    public void a(boolean f2) {
        this.b = f2;
    }

    public boolean a() {
        return this.b;
    }

    public Rect b() {
        return new Rect((int) this.e.left, (int) this.e.top, (int) this.e.right, (int) this.e.bottom);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* access modifiers changed from: package-private */
    public void b(float dx, float dy) {
        Rect invalRect = new Rect(this.d);
        this.e.offset(dx, dy);
        this.e.offset(Math.max(0.0f, this.h.left - this.e.left), Math.max(0.0f, this.h.top - this.e.top));
        this.e.offset(Math.min(0.0f, this.h.right - this.e.right), Math.min(0.0f, this.h.bottom - this.e.bottom));
        this.d = e();
        invalRect.union(this.d);
        invalRect.inset(-10, -10);
        this.a.invalidate(invalRect);
    }

    public void b(boolean hidden) {
        this.c = hidden;
    }

    public void c() {
        this.d = e();
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0067  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0077  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0086  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0092  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00a6  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00ba  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00e0  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00e2  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00f8  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void c(float r9, float r10) {
        /*
            r8 = this;
            r7 = 1103626240(0x41c80000, float:25.0)
            r6 = 1073741824(0x40000000, float:2.0)
            r5 = 0
            boolean r0 = r8.i
            if (r0 == 0) goto L_0x010e
            int r0 = (r9 > r5 ? 1 : (r9 == r5 ? 0 : -1))
            if (r0 == 0) goto L_0x00d5
            float r0 = r8.j
            float r0 = r9 / r0
            r1 = r9
        L_0x0012:
            android.graphics.RectF r2 = new android.graphics.RectF
            android.graphics.RectF r3 = r8.e
            r2.<init>(r3)
            int r3 = (r1 > r5 ? 1 : (r1 == r5 ? 0 : -1))
            if (r3 <= 0) goto L_0x0042
            float r3 = r2.width()
            float r4 = r6 * r1
            float r3 = r3 + r4
            android.graphics.RectF r4 = r8.h
            float r4 = r4.width()
            int r3 = (r3 > r4 ? 1 : (r3 == r4 ? 0 : -1))
            if (r3 <= 0) goto L_0x0042
            android.graphics.RectF r1 = r8.h
            float r1 = r1.width()
            float r3 = r2.width()
            float r1 = r1 - r3
            float r1 = r1 / r6
            boolean r3 = r8.i
            if (r3 == 0) goto L_0x0042
            float r0 = r8.j
            float r0 = r1 / r0
        L_0x0042:
            int r3 = (r0 > r5 ? 1 : (r0 == r5 ? 0 : -1))
            if (r3 <= 0) goto L_0x006a
            float r3 = r2.height()
            float r4 = r6 * r0
            float r3 = r3 + r4
            android.graphics.RectF r4 = r8.h
            float r4 = r4.height()
            int r3 = (r3 > r4 ? 1 : (r3 == r4 ? 0 : -1))
            if (r3 <= 0) goto L_0x006a
            android.graphics.RectF r0 = r8.h
            float r0 = r0.height()
            float r3 = r2.height()
            float r0 = r0 - r3
            float r0 = r0 / r6
            boolean r3 = r8.i
            if (r3 == 0) goto L_0x006a
            float r1 = r8.j
            float r1 = r1 * r0
        L_0x006a:
            float r1 = -r1
            float r0 = -r0
            r2.inset(r1, r0)
            float r0 = r2.width()
            int r0 = (r0 > r7 ? 1 : (r0 == r7 ? 0 : -1))
            if (r0 >= 0) goto L_0x0082
            float r0 = r2.width()
            float r0 = r7 - r0
            float r0 = -r0
            float r0 = r0 / r6
            r2.inset(r0, r5)
        L_0x0082:
            boolean r0 = r8.i
            if (r0 == 0) goto L_0x00e0
            float r0 = r8.j
            float r0 = r7 / r0
        L_0x008a:
            float r1 = r2.height()
            int r1 = (r1 > r0 ? 1 : (r1 == r0 ? 0 : -1))
            if (r1 >= 0) goto L_0x009c
            float r1 = r2.height()
            float r0 = r0 - r1
            float r0 = -r0
            float r0 = r0 / r6
            r2.inset(r5, r0)
        L_0x009c:
            float r0 = r2.left
            android.graphics.RectF r1 = r8.h
            float r1 = r1.left
            int r0 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r0 >= 0) goto L_0x00e2
            android.graphics.RectF r0 = r8.h
            float r0 = r0.left
            float r1 = r2.left
            float r0 = r0 - r1
            r2.offset(r0, r5)
        L_0x00b0:
            float r0 = r2.top
            android.graphics.RectF r1 = r8.h
            float r1 = r1.top
            int r0 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r0 >= 0) goto L_0x00f8
            android.graphics.RectF r0 = r8.h
            float r0 = r0.top
            float r1 = r2.top
            float r0 = r0 - r1
            r2.offset(r5, r0)
        L_0x00c4:
            android.graphics.RectF r0 = r8.e
            r0.set(r2)
            android.graphics.Rect r0 = r8.e()
            r8.d = r0
            android.view.View r0 = r8.a
            r0.invalidate()
            return
        L_0x00d5:
            int r0 = (r10 > r5 ? 1 : (r10 == r5 ? 0 : -1))
            if (r0 == 0) goto L_0x010e
            float r0 = r8.j
            float r0 = r0 * r10
            r1 = r0
            r0 = r10
            goto L_0x0012
        L_0x00e0:
            r0 = r7
            goto L_0x008a
        L_0x00e2:
            float r0 = r2.right
            android.graphics.RectF r1 = r8.h
            float r1 = r1.right
            int r0 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x00b0
            float r0 = r2.right
            android.graphics.RectF r1 = r8.h
            float r1 = r1.right
            float r0 = r0 - r1
            float r0 = -r0
            r2.offset(r0, r5)
            goto L_0x00b0
        L_0x00f8:
            float r0 = r2.bottom
            android.graphics.RectF r1 = r8.h
            float r1 = r1.bottom
            int r0 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x00c4
            float r0 = r2.bottom
            android.graphics.RectF r1 = r8.h
            float r1 = r1.bottom
            float r0 = r0 - r1
            float r0 = -r0
            r2.offset(r5, r0)
            goto L_0x00c4
        L_0x010e:
            r0 = r10
            r1 = r9
            goto L_0x0012
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wiyun.game.d.g.c(float, float):void");
    }
}
