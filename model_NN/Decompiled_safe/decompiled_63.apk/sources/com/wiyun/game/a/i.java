package com.wiyun.game.a;

import java.io.ByteArrayOutputStream;
import java.util.Random;

public class i {
    private static Random k = new Random();
    private byte[] a;
    private byte[] b;
    private byte[] c;
    private int d;
    private int e;
    private int f;
    private int g;
    private byte[] h;
    private boolean i = true;
    private int j;
    private ByteArrayOutputStream l = new ByteArrayOutputStream(8);
    private boolean m;

    private static long a(byte[] in, int offset, int len) {
        long ret = 0;
        int end = len > 8 ? offset + 8 : offset + len;
        for (int i2 = offset; i2 < end; i2++) {
            ret = (ret << 8) | ((long) (in[i2] & 255));
        }
        return (4294967295L & ret) | (ret >>> 32);
    }

    private void a() {
        this.f = 0;
        while (this.f < 8) {
            if (this.i) {
                byte[] bArr = this.a;
                int i2 = this.f;
                bArr[i2] = (byte) (bArr[i2] ^ this.b[this.f]);
            } else {
                byte[] bArr2 = this.a;
                int i3 = this.f;
                bArr2[i3] = (byte) (bArr2[i3] ^ this.c[this.e + this.f]);
            }
            this.f++;
        }
        System.arraycopy(a(this.a), 0, this.c, this.d, 8);
        this.f = 0;
        while (this.f < 8) {
            byte[] bArr3 = this.c;
            int i4 = this.d + this.f;
            bArr3[i4] = (byte) (bArr3[i4] ^ this.b[this.f]);
            this.f++;
        }
        System.arraycopy(this.a, 0, this.b, 0, 8);
        this.e = this.d;
        this.d += 8;
        this.f = 0;
        this.i = false;
    }

    private void a(int t) {
        this.l.write(t >>> 24);
        this.l.write(t >>> 16);
        this.l.write(t >>> 8);
        this.l.write(t);
    }

    private byte[] a(byte[] in) {
        int loop = 16;
        long y = a(in, 0, 4);
        long z = a(in, 4, 4);
        long[] k2 = {a(this.h, 0, 4), a(this.h, 4, 4), a(this.h, 8, 4), a(this.h, 12, 4)};
        long sum = 0;
        long delta = -1640531527 & 4294967295L;
        while (true) {
            int loop2 = loop;
            loop = loop2 - 1;
            if (loop2 <= 0) {
                this.l.reset();
                a((int) y);
                a((int) z);
                return this.l.toByteArray();
            }
            y = (y + ((((z << 4) ^ (z >> 5)) + z) ^ (k2[(int) (3 & sum)] + sum))) & 4294967295L;
            sum = (sum + delta) & 4294967295L;
            z = (z + ((((y << 4) ^ (y >> 5)) + y) ^ (k2[(int) ((sum >> 11) & 3)] + sum))) & 4294967295L;
        }
    }

    private byte[] a(byte[] in, int offset) {
        int loop = 16;
        long y = a(in, offset, 4);
        long z = a(in, offset + 4, 4);
        long[] k2 = {a(this.h, 0, 4), a(this.h, 4, 4), a(this.h, 8, 4), a(this.h, 12, 4)};
        long delta = -1640531527 & 4294967295L;
        long sum = (delta * ((long) 16)) & 4294967295L;
        while (true) {
            int loop2 = loop;
            loop = loop2 - 1;
            if (loop2 <= 0) {
                this.l.reset();
                a((int) y);
                a((int) z);
                return this.l.toByteArray();
            }
            z = (z - ((((y << 4) ^ (y >> 5)) + y) ^ (k2[(int) ((sum >> 11) & 3)] + sum))) & 4294967295L;
            sum = (sum - delta) & 4294967295L;
            y = (y - ((((z << 4) ^ (z >> 5)) + z) ^ (k2[(int) (3 & sum)] + sum))) & 4294967295L;
        }
    }

    private int b() {
        if (this.m) {
            return 537920776;
        }
        return k.nextInt();
    }

    private boolean b(byte[] in, int offset, int len) {
        this.f = 0;
        while (this.f < 8) {
            if (this.j + this.f >= len) {
                return true;
            }
            byte[] bArr = this.b;
            int i2 = this.f;
            bArr[i2] = (byte) (bArr[i2] ^ in[(this.d + offset) + this.f]);
            this.f++;
        }
        this.b = b(this.b);
        if (this.b == null) {
            return false;
        }
        this.j += 8;
        this.d += 8;
        this.f = 0;
        return true;
    }

    private byte[] b(byte[] in) {
        return a(in, 0);
    }

    public void a(boolean flag) {
        this.m = flag;
    }

    public byte[] a(byte[] in, int offset, int len, byte[] k2) {
        if (k2 == null) {
            return null;
        }
        this.e = 0;
        this.d = 0;
        this.h = k2;
        byte[] m2 = new byte[(offset + 8)];
        if (len % 8 != 0 || len < 16) {
            return null;
        }
        this.b = a(in, offset);
        this.f = this.b[0] & 7;
        int count = (len - this.f) - 10;
        if (count < 0) {
            return null;
        }
        for (int i2 = offset; i2 < m2.length; i2++) {
            m2[i2] = 0;
        }
        this.c = new byte[count];
        this.e = 0;
        this.d = 8;
        this.j = 8;
        this.f++;
        this.g = 1;
        while (this.g <= 2) {
            if (this.f < 8) {
                this.f++;
                this.g++;
            }
            if (this.f == 8) {
                m2 = in;
                if (!b(in, offset, len)) {
                    return null;
                }
            }
        }
        int i3 = 0;
        while (count != 0) {
            if (this.f < 8) {
                this.c[i3] = (byte) (m2[(this.e + offset) + this.f] ^ this.b[this.f]);
                i3++;
                count--;
                this.f++;
            }
            if (this.f == 8) {
                m2 = in;
                this.e = this.d - 8;
                if (!b(in, offset, len)) {
                    return null;
                }
            }
        }
        this.g = 1;
        while (this.g < 8) {
            if (this.f < 8) {
                if ((m2[(this.e + offset) + this.f] ^ this.b[this.f]) != 0) {
                    return null;
                }
                this.f++;
            }
            if (this.f == 8) {
                m2 = in;
                this.e = this.d;
                if (!b(in, offset, len)) {
                    return null;
                }
            }
            this.g++;
        }
        return this.c;
    }

    public byte[] a(byte[] in, byte[] k2) {
        return a(in, 0, in.length, k2);
    }

    public byte[] b(byte[] in, int offset, int len, byte[] k2) {
        int i2;
        if (k2 == null) {
            return in;
        }
        this.a = new byte[8];
        this.b = new byte[8];
        this.f = 1;
        this.g = 0;
        this.e = 0;
        this.d = 0;
        this.h = k2;
        this.i = true;
        this.f = (len + 10) % 8;
        if (this.f != 0) {
            this.f = 8 - this.f;
        }
        this.c = new byte[(this.f + len + 10)];
        this.a[0] = (byte) ((b() & 248) | this.f);
        for (int i3 = 1; i3 <= this.f; i3++) {
            this.a[i3] = (byte) (b() & 255);
        }
        this.f++;
        for (int i4 = 0; i4 < 8; i4++) {
            this.b[i4] = 0;
        }
        this.g = 1;
        while (this.g <= 2) {
            if (this.f < 8) {
                byte[] bArr = this.a;
                int i5 = this.f;
                this.f = i5 + 1;
                bArr[i5] = (byte) (b() & 255);
                this.g++;
            }
            if (this.f == 8) {
                a();
            }
        }
        int i6 = offset;
        while (len > 0) {
            if (this.f < 8) {
                byte[] bArr2 = this.a;
                int i7 = this.f;
                this.f = i7 + 1;
                i2 = i6 + 1;
                bArr2[i7] = in[i6];
                len--;
            } else {
                i2 = i6;
            }
            if (this.f == 8) {
                a();
            }
            i6 = i2;
        }
        this.g = 1;
        while (this.g <= 7) {
            if (this.f < 8) {
                byte[] bArr3 = this.a;
                int i8 = this.f;
                this.f = i8 + 1;
                bArr3[i8] = 0;
                this.g++;
            }
            if (this.f == 8) {
                a();
            }
        }
        return this.c;
    }

    public byte[] b(byte[] in, byte[] k2) {
        return b(in, 0, in.length, k2);
    }
}
