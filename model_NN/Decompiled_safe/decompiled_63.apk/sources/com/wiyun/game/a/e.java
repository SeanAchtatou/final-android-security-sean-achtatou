package com.wiyun.game.a;

import com.waps.AnimationType;
import com.wiyun.game.e.a;
import java.io.IOException;
import java.util.Random;

public class e {
    static Random a = new Random();
    static int[][] b;

    static {
        int[] iArr = new int[6];
        iArr[0] = 2;
        iArr[1] = 3;
        iArr[2] = 5;
        iArr[3] = 4;
        iArr[4] = 1;
        int[] iArr2 = new int[6];
        iArr2[0] = 3;
        iArr2[1] = 4;
        iArr2[2] = 5;
        iArr2[3] = 1;
        iArr2[5] = 2;
        int[] iArr3 = new int[6];
        iArr3[1] = 5;
        iArr3[2] = 2;
        iArr3[3] = 1;
        iArr3[4] = 3;
        iArr3[5] = 4;
        int[] iArr4 = new int[6];
        iArr4[0] = 5;
        iArr4[1] = 4;
        iArr4[2] = 3;
        iArr4[3] = 2;
        iArr4[5] = 1;
        int[] iArr5 = new int[6];
        iArr5[0] = 5;
        iArr5[1] = 3;
        iArr5[2] = 1;
        iArr5[3] = 4;
        iArr5[5] = 2;
        int[] iArr6 = new int[6];
        iArr6[0] = 2;
        iArr6[1] = 4;
        iArr6[3] = 3;
        iArr6[4] = 5;
        iArr6[5] = 1;
        int[] iArr7 = new int[6];
        iArr7[0] = 4;
        iArr7[1] = 2;
        iArr7[3] = 3;
        iArr7[4] = 1;
        iArr7[5] = 5;
        int[] iArr8 = new int[6];
        iArr8[0] = 4;
        iArr8[2] = 2;
        iArr8[3] = 5;
        iArr8[4] = 1;
        iArr8[5] = 3;
        b = new int[][]{iArr, iArr2, iArr3, iArr4, iArr5, iArr6, iArr7, iArr8};
    }

    public static String a(String resId) {
        if (resId == null || resId.length() < 10) {
            return null;
        }
        try {
            int vNum = Integer.parseInt(resId.substring(0, 2), 16);
            String key = resId.substring(0, 8);
            int remainder = Integer.parseInt(resId.substring(resId.length() - 2), 16);
            int appId = Integer.parseInt(a(String.format("%06x", Integer.valueOf(Integer.parseInt(resId.substring(resId.length() - 8, resId.length() - 2), 16) ^ remainder)), key, a.c(key)), 16);
            if ((appId * 997) % vNum == remainder) {
                return String.valueOf(appId);
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    private static String a(String origStr, int boxID) {
        String result = new String();
        int boxID2 = boxID % 8;
        char[] temp = origStr.toCharArray();
        for (int i = 0; i < 6; i++) {
            temp[b[boxID2][i]] = origStr.charAt(i);
        }
        for (int i2 = 0; i2 < 6; i2++) {
            result = String.valueOf(result) + temp[i2];
        }
        return result;
    }

    private static String a(String s, int mode, int step) {
        String result = s;
        int step2 = step % s.length();
        if (step2 <= 0) {
            return s;
        }
        switch (mode) {
            case AnimationType.NONE:
                int step3 = step2 + 1;
                result = String.valueOf(result.substring(step3 - 1, result.length())) + result.substring(0, step3 - 1);
                break;
            case 1:
                result = String.valueOf(result.substring(result.length() - step2, result.length())) + result.substring(0, result.length() - step2);
                break;
        }
        return result;
    }

    /* JADX INFO: Multiple debug info for r8v7 java.lang.String: [D('tempkey' java.lang.String), D('k2' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r7v13 java.lang.String: [D('j' int), D('r' java.lang.String)] */
    private static String a(String key, String keyMask) throws IOException {
        long k = Math.abs(Long.parseLong(a.c(key).substring(0, 8), 16));
        long km = Math.abs(Long.parseLong(a.c(keyMask).substring(0, 8), 16));
        String k2 = a.c(String.valueOf(String.format("%010d", Long.valueOf(k))) + String.format("%010d", Long.valueOf(km)));
        long x = k ^ km;
        for (int j = 0; j < 16; j++) {
            x = j % 2 == 0 ? Math.abs((((x ^ ((long) k2.charAt(j))) << 2) ^ km) % 1000000000) : x ^ ((long) k2.charAt(j));
        }
        String r = Long.toBinaryString(x);
        if (r.length() > 56) {
            r.substring(0, 55);
        }
        return r;
    }

    /* JADX INFO: Multiple debug info for r8v1 java.lang.String: [D('codeNumber' java.lang.String), D('k' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r9v2 java.lang.String: [D('key' java.lang.String), D('k' java.lang.String)] */
    private static String a(String codeNumber, String key, String keyMask) throws IOException {
        int splitPos = codeNumber.length() / 2;
        int origLen = codeNumber.length();
        String k = a(a(key, keyMask), 1, 15);
        String code = codeNumber;
        for (int i = 15; i >= 0; i--) {
            int m1 = (int) Long.parseLong(code.substring(0, splitPos), 16);
            int m2 = (int) Long.parseLong(code.substring(splitPos, origLen), 16);
            int k1 = Integer.valueOf(k.substring(0, splitPos), 2).intValue();
            int k2 = Integer.valueOf(k.substring(k.length() - splitPos, k.length()), 2).intValue();
            String r1 = String.format("%03x", Integer.valueOf(k1 ^ m1));
            k = a(k, -1, 1);
            code = a(String.valueOf(r1) + String.format("%03x", Integer.valueOf(k2 ^ m2)), i + 1);
        }
        return code;
    }
}
