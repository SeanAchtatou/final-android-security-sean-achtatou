package com.wiyun.game.c;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Random;
import org.apache.http.Header;
import org.apache.http.entity.AbstractHttpEntity;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EncodingUtils;

public class d extends AbstractHttpEntity {
    private static byte[] b = EncodingUtils.getAsciiBytes("-_1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ");
    protected e[] a;
    private byte[] c;
    private HttpParams d;
    private boolean e = false;

    public d(e[] parts) {
        setContentType("multipart/form-data");
        if (parts == null) {
            throw new IllegalArgumentException("parts cannot be null");
        }
        this.a = parts;
        this.d = null;
    }

    private static byte[] b() {
        Random rand = new Random();
        byte[] bytes = new byte[(rand.nextInt(11) + 30)];
        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = b[rand.nextInt(b.length)];
        }
        return bytes;
    }

    /* access modifiers changed from: protected */
    public byte[] a() {
        if (this.c == null) {
            String temp = null;
            if (this.d != null) {
                temp = (String) this.d.getParameter("http.method.multipart.boundary");
            }
            if (temp != null) {
                this.c = EncodingUtils.getAsciiBytes(temp);
            } else {
                this.c = b();
            }
        }
        return this.c;
    }

    public InputStream getContent() throws IOException, IllegalStateException {
        if (isRepeatable() || !this.e) {
            this.e = true;
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            e.a(baos, this.a, this.c);
            return new ByteArrayInputStream(baos.toByteArray());
        }
        throw new IllegalStateException("Content has been consumed");
    }

    public long getContentLength() {
        try {
            return e.a(this.a, a());
        } catch (Exception e2) {
            return 0;
        }
    }

    public Header getContentType() {
        StringBuffer buffer = new StringBuffer("multipart/form-data");
        buffer.append("; boundary=");
        buffer.append(EncodingUtils.getAsciiString(a()));
        return new BasicHeader("Content-Type", buffer.toString());
    }

    public boolean isRepeatable() {
        for (e h : this.a) {
            if (!h.h()) {
                return false;
            }
        }
        return true;
    }

    public boolean isStreaming() {
        return false;
    }

    public void writeTo(OutputStream out) throws IOException {
        e.a(out, this.a, a());
    }
}
