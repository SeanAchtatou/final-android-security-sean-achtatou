package com.wiyun.game.c;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

public class c implements g {
    private String a;
    private byte[] b;

    public c(String fileName, byte[] bytes) {
        this.a = fileName;
        this.b = bytes;
    }

    public long a() {
        return (long) this.b.length;
    }

    public String b() {
        return this.a;
    }

    public InputStream c() {
        return new ByteArrayInputStream(this.b);
    }
}
