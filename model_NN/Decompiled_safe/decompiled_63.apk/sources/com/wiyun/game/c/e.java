package com.wiyun.game.c;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import org.apache.http.util.EncodingUtils;

public abstract class e {
    protected static final byte[] a = EncodingUtils.getAsciiBytes("----------------314159265358979323846");
    protected static final byte[] b = EncodingUtils.getAsciiBytes("\r\n");
    protected static final byte[] c = EncodingUtils.getAsciiBytes("\"");
    protected static final byte[] d = EncodingUtils.getAsciiBytes("--");
    protected static final byte[] e = EncodingUtils.getAsciiBytes("Content-Disposition: form-data; name=");
    protected static final byte[] f = EncodingUtils.getAsciiBytes("Content-Type: ");
    protected static final byte[] g = EncodingUtils.getAsciiBytes("; charset=");
    protected static final byte[] h = EncodingUtils.getAsciiBytes("Content-Transfer-Encoding: ");
    private static final byte[] i = a;
    private boolean j;
    private String k;
    private byte[] l;

    public static long a(e[] parts, byte[] partBoundary) throws IOException {
        if (parts == null) {
            throw new IllegalArgumentException("Parts may not be null");
        }
        long total = 0;
        for (int i2 = 0; i2 < parts.length; i2++) {
            parts[i2].a(partBoundary);
            long l2 = parts[i2].i();
            if (l2 < 0) {
                return -1;
            }
            total += l2;
        }
        return total + ((long) d.length) + ((long) partBoundary.length) + ((long) d.length) + ((long) b.length);
    }

    public static void a(OutputStream out, e[] parts, byte[] partBoundary) throws IOException {
        if (parts == null) {
            throw new IllegalArgumentException("Parts may not be null");
        } else if (partBoundary == null || partBoundary.length == 0) {
            throw new IllegalArgumentException("partBoundary may not be empty");
        } else {
            for (int i2 = 0; i2 < parts.length; i2++) {
                parts[i2].a(partBoundary);
                parts[i2].h(out);
            }
            out.write(d);
            out.write(partBoundary);
            out.write(d);
            out.write(b);
        }
    }

    public abstract long a() throws IOException;

    /* access modifiers changed from: protected */
    public void a(OutputStream out) throws IOException {
        out.write(e);
        out.write(c);
        out.write(EncodingUtils.getAsciiBytes(c()));
        out.write(c);
    }

    public void a(String name) {
        this.k = name;
    }

    public void a(boolean monitorProgress) {
        this.j = monitorProgress;
    }

    /* access modifiers changed from: package-private */
    public void a(byte[] boundaryBytes) {
        this.l = boundaryBytes;
    }

    /* access modifiers changed from: protected */
    public abstract void b(OutputStream outputStream) throws IOException;

    public abstract String c();

    /* access modifiers changed from: protected */
    public void c(OutputStream out) throws IOException {
        out.write(d);
        out.write(g());
        out.write(b);
    }

    public abstract String d();

    /* access modifiers changed from: protected */
    public void d(OutputStream out) throws IOException {
        String contentType = d();
        if (contentType != null) {
            out.write(b);
            out.write(f);
            out.write(EncodingUtils.getAsciiBytes(contentType));
            String charSet = e();
            if (charSet != null) {
                out.write(g);
                out.write(EncodingUtils.getAsciiBytes(charSet));
            }
        }
    }

    public abstract String e();

    /* access modifiers changed from: protected */
    public void e(OutputStream out) throws IOException {
        String transferEncoding = f();
        if (transferEncoding != null) {
            out.write(b);
            out.write(h);
            out.write(EncodingUtils.getAsciiBytes(transferEncoding));
        }
    }

    public abstract String f();

    /* access modifiers changed from: protected */
    public void f(OutputStream out) throws IOException {
        out.write(b);
        out.write(b);
    }

    /* access modifiers changed from: protected */
    public void g(OutputStream out) throws IOException {
        out.write(b);
    }

    /* access modifiers changed from: protected */
    public byte[] g() {
        return this.l == null ? i : this.l;
    }

    public void h(OutputStream out) throws IOException {
        c(out);
        a(out);
        d(out);
        e(out);
        f(out);
        b(out);
        g(out);
    }

    public boolean h() {
        return true;
    }

    public long i() throws IOException {
        if (a() < 0) {
            return -1;
        }
        ByteArrayOutputStream overhead = new ByteArrayOutputStream();
        c(overhead);
        a(overhead);
        d(overhead);
        e(overhead);
        f(overhead);
        g(overhead);
        return ((long) overhead.size()) + a();
    }

    public boolean j() {
        return this.j;
    }

    public String k() {
        return this.k;
    }

    public String toString() {
        return c();
    }
}
