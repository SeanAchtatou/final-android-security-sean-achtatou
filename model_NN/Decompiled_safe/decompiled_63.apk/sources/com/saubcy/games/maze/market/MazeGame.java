package com.saubcy.games.maze.market;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import com.waps.AppConnect;
import com.waps.ads.AdGroupLayout;
import com.wiyun.game.WiGame;
import com.wiyun.game.WiGameClient;
import com.wiyun.game.model.ChallengeRequest;
import java.text.NumberFormat;
import java.util.List;

public class MazeGame extends Activity implements View.OnClickListener, SensorEventListener, View.OnTouchListener, GestureDetector.OnGestureListener {
    public static final int DODGE = 3;
    public static final int DRAG = 5;
    public static final int GATHERING = 2;
    public static final int GATHERINGPRO = 6;
    public static final int HIGHTREDUCTION = 50;
    public static final int MAXBLOCKSIZE = 15;
    public static final int MINBLOCKSIZE = 5;
    public static final int MIXED = 4;
    public static final int OP4GESTURE = 2;
    public static final int OP4SCREEN = 0;
    public static final int OP4SENSOR = 1;
    protected static final int SensorCrossDelay = 500;
    protected static final int SensorOpDelay = 0;
    public static final int TIMERTRIALS = 0;
    public static final int VSCOM = 1;
    public static final int WIDTHREDUCTION = 10;
    protected static final String[] achievementIds = {"4ccf32a3262d9690", "452b7bc20b662ec6", "33fcd30114814818", "22be8c39bb15d4a3", "3c895518c4ce5e38", "78daa1342b5f2b40", "36c955cd327c93c3"};
    protected static final String appKey = "57c62111100b1a08";
    protected static final String leaderboardId = "84651578689551d5";
    protected static final String secretKey = "vx3SwK5hSJVQqEYeZRqZfRNwu4fNNFLu";
    protected int GameMode;
    private SharedPreferences GameSettings;
    protected boolean GestureMoveStatus = false;
    protected int OpMode;
    protected int THINKTIME = 300;
    protected int best = 0;
    /* access modifiers changed from: private */
    public int blockSize;
    protected String controlPause;
    protected String controlStart;
    protected boolean finishFlag = false;
    protected boolean firstBlock;
    protected GestureDetector gd = new GestureDetector(this);
    protected GestureMoveHandler gmh;
    protected boolean isCrossState = false;
    protected long lastSensorGetTimer;
    protected int lastTimePass = 1000000;
    protected String mChallengeToUserId = null;
    private WiGameClient mClient = new WiGameClient() {
        public void wyLoggedIn(String sessionKey) {
        }

        public void wyLogInFailed() {
        }

        public void wyPlayChallenge(ChallengeRequest request) {
            Log.d("trace", "wyPlayChallenge()");
            MazeGame.this.mChallengeToUserId = request.getCtuId();
            MazeGame.this.mCompetitorScore = request.getScore();
            MazeGame.this.resetGame();
        }

        public void wyPortraitGot(String userId) {
        }

        public void wyGameSaveStart(String name, int totalSize) {
        }

        public void wyGameSaveProgress(String name, int uploadedSize) {
        }

        public void wyGameSaved(String name) {
        }

        public void wyGameSaveFailed(String name) {
        }

        public void wyLoadGame(String blobPath) {
        }
    };
    protected int mCompetitorScore = -1;
    protected MazeGenerator mg = null;
    protected MazeRobot mr = null;
    protected int nHeight;
    protected int nHeightUnit = 10;
    protected int nWidth;
    protected int nWidthUnit = 10;
    protected boolean pauseFlag = false;
    protected boolean runFlag = false;
    private Sensor sensor = null;
    private SensorManager sensorManager = null;
    private ScoreManager sm = null;
    protected StopWatch sw = null;
    protected Vibrator vibrator;
    protected LinearLayout wapsView = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(128, 128);
        setRequestedOrientation(1);
        getScreenSize();
        getParam();
        loadViews();
        genMaze();
    }

    private void setBest(int time) {
        if (time < this.best) {
            this.best = time;
            SharedPreferences.Editor editor = this.GameSettings.edit();
            editor.putInt(Welcome.BEST, this.best);
            editor.commit();
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_newgame:
                resetGame();
                return true;
            case R.id.menu_mode:
                showModeChooseDialog();
                return true;
            case R.id.menu_setting:
                showSettings();
                return true;
            case R.id.menu_best_time:
                WiGame.openLeaderboard(leaderboardId);
                return true;
            case R.id.menu_more:
                more();
                return true;
            case R.id.menu_challenge:
                WiGame.sendChallenge("0d31fc3e28139e7c", this.best, null, leaderboardId);
                return true;
            case R.id.menu_rules:
                showHelpTips();
                return true;
            case R.id.menu_about:
                showAbout();
                return true;
            default:
                return false;
        }
    }

    /* access modifiers changed from: protected */
    public void more() {
        try {
            Intent installIntent = new Intent("android.intent.action.VIEW");
            try {
                installIntent.setData(Uri.parse("market://search?q=pub:saubcyj"));
                startActivity(installIntent);
            } catch (Exception e) {
                AppConnect.getInstance(this).showOffers(this);
            }
        } catch (Exception e2) {
            AppConnect.getInstance(this).showOffers(this);
        }
    }

    private void showAbout() {
        startActivity(new Intent(this, About.class));
    }

    private void showSettings() {
        Intent intent = new Intent(this, MazeSettings.class);
        intent.putExtra(Welcome.DEFAULTSIZE, this.nWidthUnit);
        intent.putExtra(Welcome.GAMEMODE, this.GameMode);
        intent.putExtra(Welcome.DEFAULTSPEED, this.THINKTIME);
        intent.putExtra(Welcome.OPMODE, this.OpMode);
        startActivityForResult(intent, 0);
    }

    private void showModeChooseDialog() {
        new AlertDialog.Builder(this).setIcon((int) R.drawable.icon).setTitle((int) R.string.dialog_game_mode_list).setItems((int) R.array.GameModeList, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialoginterface, int i) {
                MazeGame.this.GameMode = i;
                MazeGame.this.recordMode(i);
                MazeGame.this.getParam();
                MazeGame.this.genMaze();
            }
        }).show();
    }

    /* access modifiers changed from: protected */
    public void showBestTime() {
        int BestTime = this.sm.getBestTime(this.GameMode, this.blockSize);
        String message = getBaseContext().getResources().getString(R.string.default_best_time);
        if (BestTime > 0) {
            int hours = BestTime / 3600;
            int minutes = (BestTime - ((hours * 60) * 60)) / 60;
            message = String.valueOf(String.format("%02d", Integer.valueOf(hours))) + ":" + String.format("%02d", Integer.valueOf(minutes)) + ":" + String.format("%02d", Integer.valueOf((BestTime - ((hours * 60) * 60)) - (minutes * 60)));
        }
        new AlertDialog.Builder(this).setMessage(message).setCancelable(false).setPositiveButton(getBaseContext().getResources().getString(R.string.game_result_btn), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        }).show();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (-1 == resultCode) {
            int blocksize = Integer.valueOf(data.getStringExtra(Welcome.DEFAULTSIZE)).intValue();
            this.THINKTIME = Integer.valueOf(data.getStringExtra(Welcome.DEFAULTSPEED)).intValue();
            this.OpMode = Integer.valueOf(data.getStringExtra(Welcome.OPMODE)).intValue();
            recordOpMode(this.OpMode);
            if (this.nWidthUnit != blocksize) {
                this.blockSize = blocksize;
                recordLevel(this.blockSize);
                showSettingChangeDialog();
            }
        }
    }

    private void showSettingChangeDialog() {
        new AlertDialog.Builder(this).setMessage(getBaseContext().getResources().getString(R.string.setting_change_tips)).setCancelable(false).setPositiveButton(getBaseContext().getResources().getString(R.string.setting_change_agree), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                MazeGame.this.finishFlag = false;
                MazeGame.this.runFlag = false;
                MazeGame.this.sw.resetTime();
                MazeGame.this.genMaze();
            }
        }).setNegativeButton(getBaseContext().getResources().getString(R.string.setting_change_not_agree), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        }).show();
    }

    private void showUseSensorDialog() {
        new AlertDialog.Builder(this).setMessage(getBaseContext().getResources().getString(R.string.sensor_open_tips)).setCancelable(false).setPositiveButton(getBaseContext().getResources().getString(R.string.sensor_open_agree), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                MazeGame.this.OpMode = 1;
                MazeGame.this.connectSensor();
                MazeGame.this.recordOpMode(1);
                MazeGame.this.finishFlag = false;
                MazeGame.this.runFlag = false;
                MazeGame.this.sw.resetTime();
                MazeGame.this.genMaze();
                dialog.cancel();
            }
        }).setNegativeButton(getBaseContext().getResources().getString(R.string.sensor_open_not_agree), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                MazeGame.this.OpMode = 2;
                MazeGame.this.disconnectSensor();
                MazeGame.this.recordOpMode(2);
                dialog.cancel();
                MazeGame.this.resetGame();
            }
        }).show();
    }

    /* access modifiers changed from: protected */
    public void showGameFinishDialog() {
        String szResult;
        String szPositiveButton;
        this.runFlag = false;
        int timePass = (this.sw.hours * 60 * 60 * 1000) + (this.sw.minutes * 60 * 1000) + (this.sw.seconds * 1000) + (this.sw.tseconds * 100);
        this.lastTimePass = timePass;
        if (this.mCompetitorScore >= 0) {
            WiGame.submitChallengeResult(this.mChallengeToUserId, this.mCompetitorScore - timePass, timePass, null);
            this.mCompetitorScore = -1;
        }
        unlockAchievement();
        setBest(timePass);
        int TimeConsume = (this.sw.hours * 60 * 60) + (this.sw.minutes * 60) + this.sw.seconds;
        int record = this.sm.getBestTime(this.GameMode, this.blockSize);
        if (record < 0 || (record > 0 && TimeConsume < record)) {
            szResult = getBaseContext().getResources().getString(R.string.game_result_record);
            this.sm.addNewRecord(this.GameMode, this.blockSize, TimeConsume);
        } else {
            szResult = getBaseContext().getResources().getString(R.string.game_result_win);
        }
        NumberFormat nf = NumberFormat.getInstance();
        nf.setMinimumIntegerDigits(2);
        nf.setMaximumIntegerDigits(2);
        String szResult2 = String.valueOf(szResult) + " " + nf.format((long) this.sw.hours) + ":" + " " + nf.format((long) this.sw.minutes) + ":" + " " + nf.format((long) this.sw.seconds) + ":" + this.sw.tseconds;
        if (this.blockSize - 1 < 5) {
            szPositiveButton = getBaseContext().getResources().getString(R.string.game_result_backbegin_btn);
        } else {
            szPositiveButton = getBaseContext().getResources().getString(R.string.game_result_nextlevel_btn);
        }
        new AlertDialog.Builder(this).setMessage(szResult2).setCancelable(false).setPositiveButton(szPositiveButton, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                MazeGame mazeGame = MazeGame.this;
                mazeGame.blockSize = mazeGame.blockSize - 1;
                if (MazeGame.this.blockSize < 5) {
                    MazeGame.this.blockSize = 15;
                }
                MazeGame.this.recordLevel(MazeGame.this.blockSize);
                MazeGame.this.resetGame();
                dialog.cancel();
            }
        }).setNegativeButton(getBaseContext().getResources().getString(R.string.game_result_tryagain_btn), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                MazeGame.this.resetGame();
                dialog.cancel();
            }
        }).show();
        showSubmitCheckDialog();
    }

    private void unlockAchievement() {
        WiGame.unlockAchievement(achievementIds[this.GameMode]);
    }

    /* access modifiers changed from: protected */
    public void showHelpTips() {
        String szTipsS = null;
        switch (this.GameMode) {
            case 0:
                szTipsS = getBaseContext().getResources().getString(R.string.game_mode_timertrials_tips);
                break;
            case 1:
                szTipsS = getBaseContext().getResources().getString(R.string.game_mode_vscom_tips);
                break;
            case 2:
                szTipsS = getBaseContext().getResources().getString(R.string.game_mode_gathering_tips);
                break;
            case 3:
                szTipsS = getBaseContext().getResources().getString(R.string.game_mode_dodge_tips);
                break;
            case 4:
                szTipsS = getBaseContext().getResources().getString(R.string.game_mode_mixed_tips);
                break;
            case 5:
                szTipsS = getBaseContext().getResources().getString(R.string.game_mode_drag_tips);
                break;
            case 6:
                szTipsS = getBaseContext().getResources().getString(R.string.game_mode_gathering_pro_tips);
                break;
        }
        new AlertDialog.Builder(this).setMessage(szTipsS).setCancelable(false).setPositiveButton(getBaseContext().getResources().getString(R.string.game_result_btn), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        }).show();
    }

    /* access modifiers changed from: protected */
    public void loseGame() {
        this.runFlag = false;
        new AlertDialog.Builder(this).setMessage(getBaseContext().getResources().getString(R.string.game_result_lose)).setCancelable(false).setPositiveButton(getBaseContext().getResources().getString(R.string.game_result_btn), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                MazeGame.this.resetGame();
                dialog.cancel();
            }
        }).show();
    }

    public boolean checkIsGet() {
        if (3 != this.GameMode && 4 != this.GameMode) {
            return false;
        }
        int[] iArr = this.mr.currentPos;
        this.mr.getClass();
        int i = iArr[0];
        int[] iArr2 = this.mg.cursorPos;
        this.mr.getClass();
        if (i == iArr2[0]) {
            int[] iArr3 = this.mr.currentPos;
            this.mr.getClass();
            int i2 = iArr3[1];
            int[] iArr4 = this.mg.cursorPos;
            this.mr.getClass();
            if (i2 == iArr4[1]) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    public void getParam() {
        this.GameSettings = getPreferences(0);
        this.GameMode = this.GameSettings.getInt(Welcome.GAMEMODE, 0);
        this.blockSize = this.GameSettings.getInt(new StringBuilder(String.valueOf(this.GameMode)).toString(), -1);
        this.OpMode = this.GameSettings.getInt(Welcome.OPMODE, -1);
        this.best = this.GameSettings.getInt(Welcome.BEST, 3600000);
        if (this.blockSize < 0) {
            this.blockSize = 15;
            if (5 == this.GameMode) {
                this.blockSize *= 3;
            }
            recordLevel(this.blockSize);
        }
        if (-1 == this.OpMode && checkSensor()) {
            showUseSensorDialog();
        }
    }

    /* access modifiers changed from: protected */
    public boolean checkSensor() {
        if (this.sensorManager == null) {
            this.sensorManager = (SensorManager) getSystemService("sensor");
        }
        if (this.sensorManager.getSensorList(3).size() > 0) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean connectSensor() {
        if (this.sensorManager == null) {
            this.sensorManager = (SensorManager) getSystemService("sensor");
        }
        List<Sensor> sensors = this.sensorManager.getSensorList(3);
        if (sensors.size() <= 0) {
            return false;
        }
        this.sensor = sensors.get(0);
        this.sensorManager.registerListener(this, this.sensor, 1);
        return true;
    }

    /* access modifiers changed from: protected */
    public void disconnectSensor() {
        if (this.sensor != null) {
            this.sensorManager.unregisterListener(this);
            this.sensor = null;
        }
    }

    /* access modifiers changed from: private */
    public void recordLevel(int level) {
        SharedPreferences.Editor editor = this.GameSettings.edit();
        editor.putInt(new StringBuilder(String.valueOf(this.GameMode)).toString(), level);
        editor.commit();
    }

    /* access modifiers changed from: private */
    public void recordMode(int Mode) {
        SharedPreferences.Editor editor = this.GameSettings.edit();
        editor.putInt(Welcome.GAMEMODE, Mode);
        editor.commit();
    }

    /* access modifiers changed from: private */
    public void recordOpMode(int Mode) {
        SharedPreferences.Editor editor = this.GameSettings.edit();
        editor.putInt(Welcome.OPMODE, Mode);
        editor.commit();
    }

    private void loadViews() {
        this.gmh = new GestureMoveHandler(this);
        this.sm = new ScoreManager(this);
        setContentView((int) R.layout.maze);
        this.controlStart = getBaseContext().getResources().getString(R.string.btn_control_start);
        this.controlPause = getBaseContext().getResources().getString(R.string.btn_control_pause);
        this.sw = (StopWatch) findViewById(R.id.stopwatch);
        this.mg = (MazeGenerator) findViewById(R.id.maze);
        this.sw.setFocusable(false);
        this.mg.setFocusable(true);
        this.mg.setOnTouchListener(this);
        this.mg.setLongClickable(true);
        this.wapsView = (LinearLayout) findViewById(R.id.AdLinearLayout);
        this.wapsView.setVisibility(0);
        this.wapsView.addView(new AdGroupLayout(this));
    }

    public void onClick(View v) {
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.pauseFlag) {
            this.pauseFlag = false;
            this.sw.controlCount(true);
        }
        if (1 == this.OpMode) {
            connectSensor();
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.runFlag) {
            this.pauseFlag = true;
            this.sw.controlCount(false);
            if (1 == this.GameMode || 3 == this.GameMode || 4 == this.GameMode) {
                this.mr.stopRobot();
            }
        }
        disconnectSensor();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        if (1 == this.GameMode || 3 == this.GameMode || 4 == this.GameMode) {
            this.mr.stopRobot();
        }
        if (this.vibrator != null) {
            this.vibrator.cancel();
        }
        disconnectSensor();
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void vibratorBack() {
        this.vibrator = (Vibrator) getSystemService("vibrator");
        this.vibrator.vibrate(35);
    }

    /* access modifiers changed from: private */
    public void resetGame() {
        if (1 == this.GameMode || 3 == this.GameMode || 4 == this.GameMode) {
            this.mr.stopRobot();
        }
        this.runFlag = true;
        this.sw.controlCount(false);
        this.finishFlag = false;
        this.sw.resetTime();
        genMaze();
    }

    /* access modifiers changed from: private */
    public void genMaze() {
        this.GestureMoveStatus = false;
        this.firstBlock = true;
        this.lastSensorGetTimer = 0;
        this.nHeightUnit = this.blockSize;
        this.nWidthUnit = this.blockSize;
        this.mg.setMaze(this, ((this.nHeight / this.nHeightUnit) - 1) / 2, (((this.nWidth - 10) / this.nWidthUnit) - 1) / 2);
        this.mg.buildMazeByStack(1);
        if (1 == this.GameMode || 3 == this.GameMode || 4 == this.GameMode) {
            this.mr = new MazeRobot(this);
        }
    }

    /* access modifiers changed from: protected */
    public void resteMazeWall() {
        this.mg.buildMazeByStack(2);
    }

    /* access modifiers changed from: protected */
    public void setFullScreen() {
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
    }

    /* access modifiers changed from: protected */
    public void getScreenSize() {
        int adsHeiht;
        AppConnect.getInstance(this);
        WiGame.init(this, appKey, secretKey, "1.0", false, false);
        WiGame.addWiGameClient(this.mClient);
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        this.nHeight = dm.heightPixels;
        this.nWidth = dm.widthPixels;
        int swHeigh = (int) (50.0f * (((float) this.nWidth) / 320.0f) * 0.6f);
        if (((double) getResources().getDisplayMetrics().density) > 1.0d) {
            adsHeiht = 75;
        } else {
            adsHeiht = 50;
        }
        this.nHeight -= (swHeigh + adsHeiht) + 50;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        WiGame.destroy(this);
        AppConnect.getInstance(this).finalize();
        super.onDestroy();
    }

    public void onAccuracyChanged(Sensor sensor2, int accuracy) {
    }

    public void onSensorChanged(SensorEvent event) {
        if (!this.runFlag || 5 == this.GameMode || event.sensor.getType() != 3 || event.values.length < 3) {
            return;
        }
        if (event.values[1] < 45.0f || event.values[1] > 90.0f) {
            if (event.values[1] > -45.0f || event.values[1] < -90.0f) {
                if (event.values[2] < 45.0f || event.values[2] > 90.0f) {
                    if (event.values[2] <= -45.0f && event.values[2] >= -90.0f) {
                        if (this.mg.move(2)) {
                            this.firstBlock = true;
                        } else if (this.firstBlock) {
                            this.firstBlock = false;
                            vibratorBack();
                        }
                    }
                } else if (this.mg.move(0)) {
                    this.firstBlock = true;
                } else if (this.firstBlock) {
                    this.firstBlock = false;
                    vibratorBack();
                }
            } else if (this.mg.move(3)) {
                this.firstBlock = true;
            } else if (this.firstBlock) {
                this.firstBlock = false;
                vibratorBack();
            }
        } else if (this.mg.move(1)) {
            this.firstBlock = true;
        } else if (this.firstBlock) {
            this.firstBlock = false;
            vibratorBack();
        }
    }

    public boolean onDown(MotionEvent e) {
        if (2 == this.OpMode) {
            return true;
        }
        return false;
    }

    public void onShowPress(MotionEvent e) {
    }

    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

    public void onLongPress(MotionEvent e) {
    }

    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        if (this.GestureMoveStatus) {
            return false;
        }
        if (e2.getX() - e1.getX() > 100.0f && Math.abs(velocityX) > 200.0f) {
            new GestureMoveAction(this, 2).start();
        } else if (e2.getY() - e1.getY() > 100.0f && Math.abs(velocityY) > 200.0f) {
            new GestureMoveAction(this, 3).start();
        } else if (e1.getX() - e2.getX() > 50.0f && Math.abs(velocityX) > 100.0f) {
            new GestureMoveAction(this, 0).start();
        } else if (e1.getY() - e2.getY() > 33.0f && Math.abs(velocityY) > 66.0f) {
            new GestureMoveAction(this, 1).start();
        }
        return false;
    }

    public boolean onTouch(View v, MotionEvent event) {
        if (2 == this.OpMode) {
            return this.gd.onTouchEvent(event);
        }
        return false;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        showExitDialog();
        return true;
    }

    /* access modifiers changed from: protected */
    public void showExitDialog() {
        new AlertDialog.Builder(this).setMessage(getBaseContext().getResources().getString(R.string.exit)).setCancelable(true).setPositiveButton(getBaseContext().getResources().getString(R.string.positive), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                MazeGame.this.finish();
            }
        }).setNeutralButton(getBaseContext().getResources().getString(R.string.rate), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                MazeGame.this.rateit();
            }
        }).setNegativeButton(getBaseContext().getResources().getString(R.string.menu_more), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                MazeGame.this.more();
            }
        }).show();
    }

    /* access modifiers changed from: private */
    public void rateit() {
        try {
            Intent installIntent = new Intent("android.intent.action.VIEW");
            try {
                installIntent.setData(Uri.parse("market://search?q=pname:com.saubcy.games.maze.market"));
                startActivity(installIntent);
            } catch (Exception e) {
                finish();
            }
        } catch (Exception e2) {
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void showSubmitCheckDialog() {
        new AlertDialog.Builder(this).setMessage(getBaseContext().getResources().getString(R.string.submit_check)).setCancelable(false).setPositiveButton(getBaseContext().getResources().getString(R.string.positive), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                WiGame.submitScore(MazeGame.leaderboardId, MazeGame.this.lastTimePass, null, true);
            }
        }).setNegativeButton(getBaseContext().getResources().getString(R.string.negative), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        }).show();
    }

    public static class GestureMoveAction extends Thread {
        private int direction;
        private final int interval = 10;
        private Message msg = null;
        private MazeGame parent = null;
        private int step;

        public GestureMoveAction(MazeGame p, int d) {
            this.parent = p;
            this.direction = d;
            this.parent.GestureMoveStatus = true;
            this.step = 0;
        }

        public void run() {
            while (this.parent.GestureMoveStatus) {
                try {
                    this.msg = new Message();
                    this.msg.what = this.direction;
                    this.msg.arg1 = this.step;
                    this.step++;
                    this.parent.gmh.sendMessage(this.msg);
                    sleep(10);
                } catch (Exception e) {
                    this.parent.GestureMoveStatus = false;
                }
            }
            this.parent.GestureMoveStatus = false;
        }
    }

    class GestureMoveHandler extends Handler {
        private MazeGame parent = null;

        public GestureMoveHandler(MazeGame p) {
            this.parent = p;
        }

        public void handleMessage(Message msg) {
            if (this.parent.GestureMoveStatus) {
                this.parent.GestureMoveStatus = this.parent.mg.move4Gesture(msg.what, msg.arg1);
            }
        }
    }
}
