package com.saubcy.games.maze.market;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Welcome extends Activity implements View.OnClickListener {
    public static final String BEST = "com.saubcy.games.maze.Welcome.BEST";
    public static final String BLOCKSIZE = "com.saubcy.games.maze.Welcome.BLOCKSIZE";
    public static final String DEFAULTSIZE = "com.saubcy.games.maze.Welcome.DEFAULTSIZE";
    public static final String DEFAULTSPEED = "com.saubcy.games.maze.Welcome.DEFAULTSPEED";
    public static final String GAMEMODE = "com.saubcy.games.maze.Welcome.GAMEMODE";
    public static final String OPMODE = "com.saubcy.games.maze.Welcome.OPMODE";
    private Button btn_aboutme;
    private Button btn_exit;
    private Button btn_start;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LoadWelcomeView();
    }

    private void LoadWelcomeView() {
        setContentView((int) R.layout.welcome);
        this.btn_start = (Button) findViewById(R.id.btn_start);
        this.btn_exit = (Button) findViewById(R.id.btn_exit);
        this.btn_aboutme = (Button) findViewById(R.id.btn_aboutme);
        this.btn_exit.setOnClickListener(this);
        this.btn_start.setOnClickListener(this);
        this.btn_aboutme.setOnClickListener(this);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_start:
                showModeChooseDialog();
                return;
            case R.id.btn_aboutme:
                showAbout();
                return;
            case R.id.btn_exit:
                System.exit(0);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public void startNewGame(int i) {
        Intent intent = new Intent(this, MazeGame.class);
        intent.putExtra(BLOCKSIZE, i);
        startActivity(intent);
    }

    private void showAbout() {
        startActivity(new Intent(this, About.class));
    }

    private void showModeChooseDialog() {
        new AlertDialog.Builder(this).setIcon((int) R.drawable.icon).setTitle((int) R.string.dialog_game_mode_list).setItems((int) R.array.GameModeList, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialoginterface, int i) {
                Welcome.this.startNewGame(i);
            }
        }).show();
    }
}
