package com.saubcy.games.maze.market;

import android.app.Activity;
import android.os.Bundle;
import android.view.MotionEvent;

public class About extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(4, 4);
        setContentView((int) R.layout.about);
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() != 0) {
            return false;
        }
        finish();
        return true;
    }
}
