package com.saubcy.games.maze.market;

import android.os.Handler;
import android.os.Message;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

public class MazeRobot {
    public static final int COMPLETEACTION = 1;
    public static final int GETACTION = 2;
    public static final int MOVEACTION = 0;
    public final int COL = 1;
    public final int ROW = 0;
    protected int[] currentPos = new int[2];
    /* access modifiers changed from: private */
    public moveHandler mh = null;
    /* access modifiers changed from: private */
    public MazeGame parent = null;
    private RoutingThread rt = null;
    /* access modifiers changed from: private */
    public int[] startPos = new int[2];
    private int[] targetPos = new int[2];

    public MazeRobot(MazeGame m) {
        this.parent = m;
        this.mh = new moveHandler(this, null);
        setPos();
    }

    private void setPos() {
        switch (this.parent.GameMode) {
            case 1:
                this.startPos[0] = 0;
                this.startPos[1] = ((this.parent.mg.m_nCol * 2) + 1) - 2;
                this.targetPos[0] = ((this.parent.mg.m_nRow * 2) + 1) - 1;
                this.targetPos[1] = ((this.parent.mg.m_nCol * 2) + 1) - 2;
                break;
            case 3:
                this.startPos[0] = ((this.parent.mg.m_nRow * 2) + 1) / 2;
                this.startPos[1] = 1;
                this.targetPos[0] = ((this.parent.mg.m_nRow * 2) + 1) / 2;
                this.targetPos[1] = ((this.parent.mg.m_nCol * 2) + 1) - 2;
                break;
            case 4:
                this.startPos[0] = ((this.parent.mg.m_nRow * 2) + 1) / 2;
                this.startPos[1] = 1;
                this.targetPos[0] = ((this.parent.mg.m_nRow * 2) + 1) / 2;
                this.targetPos[1] = ((this.parent.mg.m_nCol * 2) + 1) - 2;
                break;
        }
        this.currentPos[0] = this.startPos[0];
        this.currentPos[1] = this.startPos[1];
    }

    public void findTheWay(boolean isResume) {
        if (this.rt != null && this.rt.isAlive()) {
            this.rt.interrupt();
        }
        this.rt = new RoutingThread(isResume);
        this.rt.start();
    }

    public void stopRobot() {
        if (this.rt != null && this.rt.isAlive()) {
            this.rt.interrupt();
        }
        this.rt = null;
    }

    public boolean checkTarget() {
        if (this.currentPos[0] == this.targetPos[0] && this.currentPos[1] == this.targetPos[1]) {
            return true;
        }
        return false;
    }

    public boolean checkIsGet() {
        return this.parent.checkIsGet();
    }

    public void setCurrentPos(RoutingCell cell) {
        if (this.rt != null && this.rt.isAlive()) {
            this.currentPos[0] = cell.nRow;
            this.currentPos[1] = cell.nCol;
        }
    }

    /* access modifiers changed from: private */
    public void inverseStart() {
        int[] tmpPos = this.startPos;
        this.startPos = this.targetPos;
        this.targetPos = tmpPos;
    }

    private class moveHandler extends Handler {
        private moveHandler() {
        }

        /* synthetic */ moveHandler(MazeRobot mazeRobot, moveHandler movehandler) {
            this();
        }

        public void handleMessage(Message msg) {
            if (msg.what == 0) {
                MazeRobot.this.parent.mg.setRobot(MazeRobot.this.currentPos[0], MazeRobot.this.currentPos[1]);
            } else if (1 == msg.what) {
                switch (MazeRobot.this.parent.GameMode) {
                    case 1:
                        MazeRobot.this.parent.loseGame();
                        return;
                    case 2:
                    default:
                        return;
                    case 3:
                        MazeRobot.this.inverseStart();
                        MazeRobot.this.findTheWay(false);
                        return;
                    case 4:
                        MazeRobot.this.inverseStart();
                        MazeRobot.this.findTheWay(false);
                        return;
                }
            } else if (2 == msg.what) {
                MazeRobot.this.parent.loseGame();
            }
        }
    }

    private class RoutingCell {
        protected int nCol;
        protected int nRow;
        protected int[] prePos = new int[2];

        public RoutingCell(int row, int col, int lastRow, int lastCol) {
            this.nRow = row;
            this.nCol = col;
            this.prePos[0] = lastRow;
            this.prePos[1] = lastCol;
        }
    }

    private class RoutingThread extends Thread {
        private Stack<RoutingCell> footPrint = null;
        private boolean isResume = false;
        private boolean rollback = false;
        private boolean[][] routingMap = null;
        private Stack<RoutingCell> tobeRouting = null;

        public RoutingThread(boolean resumeFlag) {
            this.isResume = resumeFlag;
            this.routingMap = (boolean[][]) Array.newInstance(Boolean.TYPE, (MazeRobot.this.parent.mg.m_nRow * 2) + 1, (MazeRobot.this.parent.mg.m_nCol * 2) + 1);
            for (int i = 0; i < ((MazeRobot.this.parent.mg.m_nRow * 2) + 1) - 1; i++) {
                for (int j = 0; j < ((MazeRobot.this.parent.mg.m_nCol * 2) + 1) - 1; j++) {
                    this.routingMap[i][j] = false;
                }
            }
        }

        public void run() {
            RoutingCell start;
            RoutingCell cell;
            this.tobeRouting = new Stack<>();
            this.footPrint = new Stack<>();
            if (this.isResume) {
                start = new RoutingCell(MazeRobot.this.currentPos[0], MazeRobot.this.currentPos[1], -1, -1);
            } else {
                start = new RoutingCell(MazeRobot.this.startPos[0], MazeRobot.this.startPos[1], -1, -1);
            }
            this.tobeRouting.push(start);
            while (this.tobeRouting.size() > 0) {
                if (this.rollback) {
                    cell = goBack();
                } else {
                    cell = this.tobeRouting.pop();
                }
                try {
                    MazeRobot.this.setCurrentPos(cell);
                    this.footPrint.push(cell);
                    MazeRobot.this.mh.sendEmptyMessage(0);
                    if (MazeRobot.this.checkIsGet()) {
                        MazeRobot.this.mh.sendEmptyMessage(2);
                        return;
                    } else if (MazeRobot.this.checkTarget()) {
                        MazeRobot.this.mh.sendEmptyMessage(1);
                        return;
                    } else {
                        try {
                            operateMove(cell);
                            try {
                                sleep((long) MazeRobot.this.parent.THINKTIME);
                            } catch (InterruptedException e) {
                                return;
                            }
                        } catch (Exception e2) {
                            return;
                        }
                    }
                } catch (NullPointerException e3) {
                    return;
                }
            }
        }

        private void operateMove(RoutingCell cell) {
            List<Integer> list = randomNeighbor();
            int nLastPush = -1;
            this.routingMap[cell.nRow][cell.nCol] = true;
            for (int i = 0; i < list.size(); i++) {
                int nPos = list.get(i).intValue();
                int nRow = cell.nRow + MazeGenerator.ROWOPVALUE[nPos];
                int nCol = cell.nCol + MazeGenerator.COLOPVALUE[nPos];
                if (isMoveValid(nRow, nCol)) {
                    nLastPush = nPos;
                    this.tobeRouting.push(new RoutingCell(nRow, nCol, cell.nRow, cell.nCol));
                }
            }
            if (nLastPush < 0) {
                this.rollback = true;
            }
        }

        private RoutingCell goBack() {
            if (this.tobeRouting.size() <= 0) {
                return null;
            }
            RoutingCell cell = this.tobeRouting.pop();
            while (this.footPrint.size() > 0) {
                RoutingCell backNode = this.footPrint.pop();
                MazeRobot.this.setCurrentPos(backNode);
                MazeRobot.this.mh.sendEmptyMessage(0);
                try {
                    sleep((long) MazeRobot.this.parent.THINKTIME);
                    if (cell.prePos[0] == backNode.nRow && cell.prePos[1] == backNode.nCol) {
                        break;
                    }
                } catch (InterruptedException e) {
                    return null;
                }
            }
            this.rollback = false;
            return cell;
        }

        private boolean isMoveValid(int nRow, int nCol) {
            if (nRow < 0 || nCol < 0 || nRow > ((MazeRobot.this.parent.mg.m_nRow * 2) + 1) - 1 || nCol > ((MazeRobot.this.parent.mg.m_nCol * 2) + 1) - 1) {
                return false;
            }
            if (MazeRobot.this.parent.mg.drawMap[nRow][nCol].m_blocked) {
                return false;
            }
            if (this.routingMap[nRow][nCol]) {
                return false;
            }
            return true;
        }

        private List<Integer> randomNeighbor() {
            List<Integer> list = new ArrayList<>();
            list.add(0);
            list.add(1);
            list.add(2);
            list.add(3);
            Collections.shuffle(list);
            return list;
        }
    }
}
