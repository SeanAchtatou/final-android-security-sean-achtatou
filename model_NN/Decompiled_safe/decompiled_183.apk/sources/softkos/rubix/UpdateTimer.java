package softkos.rubix;

import android.os.Message;
import java.util.TimerTask;
import softkos.rubix.Vars;

public class UpdateTimer extends TimerTask {
    public void run() {
        Vars vars = Vars.getInstance();
        if (vars.gameState == Vars.GameState.Game) {
            Message msg = new Message();
            msg.arg1 = 100;
            Vars.getInstance().getMessageHandler().sendMessage(msg);
        }
        if (vars.gameState == Vars.GameState.Menu) {
            Message msg2 = new Message();
            msg2.arg1 = 100;
            Vars.getInstance().getMessageHandler().sendMessage(msg2);
        }
    }
}
