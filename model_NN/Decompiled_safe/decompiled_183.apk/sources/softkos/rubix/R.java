package softkos.rubix;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int arrow_left_menu = 2130837504;
        public static final int box_0 = 2130837505;
        public static final int box_1 = 2130837506;
        public static final int box_2 = 2130837507;
        public static final int box_3 = 2130837508;
        public static final int box_4 = 2130837509;
        public static final int box_5 = 2130837510;
        public static final int box_6 = 2130837511;
        public static final int box_7 = 2130837512;
        public static final int box_8 = 2130837513;
        public static final int boxit_ad = 2130837514;
        public static final int btn240x50off = 2130837515;
        public static final int btn240x50on = 2130837516;
        public static final int cubix_ad = 2130837517;
        public static final int frogs_jump_ad = 2130837518;
        public static final int gameback = 2130837519;
        public static final int gamename = 2130837520;
        public static final int icon = 2130837521;
        public static final int progress = 2130837522;
        public static final int solved = 2130837523;
        public static final int star_0 = 2130837524;
        public static final int star_1 = 2130837525;
        public static final int star_2 = 2130837526;
        public static final int star_3 = 2130837527;
        public static final int star_4 = 2130837528;
        public static final int star_5 = 2130837529;
        public static final int turnmeon_ad = 2130837530;
        public static final int ume_ad = 2130837531;
        public static final int untangle_ad = 2130837532;
    }

    public static final class id {
        public static final int Settings_empty1 = 2131099687;
        public static final int Settings_title = 2131099686;
        public static final int cancel = 2131099670;
        public static final int cancelentername = 2131099652;
        public static final int close_dlg = 2131099665;
        public static final int close_settings_dlg = 2131099690;
        public static final int closescores = 2131099685;
        public static final int editname = 2131099649;
        public static final int empty2 = 2131099659;
        public static final int empty3 = 2131099661;
        public static final int empty4 = 2131099662;
        public static final int empty_5 = 2131099664;
        public static final int gameover_msg = 2131099654;
        public static final int gameover_no = 2131099656;
        public static final int gameover_txt = 2131099653;
        public static final int gameover_yes = 2131099655;
        public static final int home_page = 2131099689;
        public static final int how_to_play_text1 = 2131099657;
        public static final int how_to_play_text2 = 2131099658;
        public static final int how_to_play_text3 = 2131099660;
        public static final int how_to_play_text5 = 2131099663;
        public static final int layout_nexprev = 2131099682;
        public static final int layout_root = 2131099648;
        public static final int layout_root3 = 2131099650;
        public static final int next_level = 2131099668;
        public static final int ok = 2131099669;
        public static final int okentername = 2131099651;
        public static final int prev_level = 2131099666;
        public static final int score_pos0 = 2131099672;
        public static final int score_pos1 = 2131099673;
        public static final int score_pos2 = 2131099674;
        public static final int score_pos3 = 2131099675;
        public static final int score_pos4 = 2131099676;
        public static final int score_pos5 = 2131099677;
        public static final int score_pos6 = 2131099678;
        public static final int score_pos7 = 2131099679;
        public static final int score_pos8 = 2131099680;
        public static final int score_pos9 = 2131099681;
        public static final int scores_next_page = 2131099684;
        public static final int scores_prev_page = 2131099683;
        public static final int selected_level = 2131099667;
        public static final int sound_setting = 2131099688;
        public static final int textView1 = 2131099671;
    }

    public static final class layout {
        public static final int enter_name = 2130903040;
        public static final int game_over = 2130903041;
        public static final int how_to_play = 2130903042;
        public static final int level_selection = 2130903043;
        public static final int main = 2130903044;
        public static final int online_scores = 2130903045;
        public static final int settings = 2130903046;
    }

    public static final class raw {
        public static final int block_break = 2130968576;
    }

    public static final class string {
        public static final int app_name = 2131034112;
    }
}
