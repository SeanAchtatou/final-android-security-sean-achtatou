package softkos.rubix;

public class Box extends GameObject {
    double angle = 0.0d;
    int board_x;
    int board_y;
    boolean falling = false;
    int speed_x;
    int speed_y;
    double time;
    double tmp_px;
    double tmp_py;
    int type;
    double vo;
    double vx;
    double vy;

    public void fall() {
        this.time = 0.0d;
        this.vo = (double) (Vars.getInstance().GetNextInt(40) - 20);
        this.angle = (Vars.getInstance().getRandom().nextDouble() % 3.1415d) - 1.507d;
        this.tmp_px = (double) getPx();
        this.tmp_py = (double) getPy();
        this.falling = true;
    }

    public void setSpeeds(int sx, int sy) {
        this.speed_x = sx;
        this.speed_y = sy;
    }

    public int getSpeedX() {
        return this.speed_x;
    }

    public int getSpeedY() {
        return this.speed_y;
    }

    public void setType(int t) {
        this.type = t;
    }

    public int getType() {
        return this.type;
    }

    public void setBoardPos(int x, int y) {
        this.board_x = x;
        this.board_y = y;
    }

    public int getBoardPosX() {
        return this.board_x;
    }

    public int getBoardPosY() {
        return this.board_y;
    }

    public boolean update() {
        if (!isVisible()) {
            return false;
        }
        boolean moved = false;
        if (this.falling) {
            this.vx = this.vo * Math.cos(this.angle);
            this.vy = (this.vo * Math.sin(this.angle)) - (9.81d * this.time);
            this.time += 0.2d;
            this.tmp_px += this.vx;
            this.tmp_py -= this.vy;
            SetPos((int) this.tmp_px, (int) this.tmp_py);
            moved = true;
        } else if (this.speed_y > 0) {
            if (getDestPy() != getPy()) {
                if (Math.abs(getDestPy() - getPy()) <= this.speed_y) {
                    SetPos(getPx(), getDestPy());
                } else {
                    SetPos(getPx(), getPy() + this.speed_y);
                }
                moved = true;
            }
            this.speed_y += 2;
        }
        return moved;
    }
}
