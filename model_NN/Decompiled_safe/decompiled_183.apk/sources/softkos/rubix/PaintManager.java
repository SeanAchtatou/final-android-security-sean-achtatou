package softkos.rubix;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Typeface;

public class PaintManager {
    public static int STR_CENTER = 0;
    public static int STR_LEFT = 1;
    public static int STR_RIGHT = 2;
    static PaintManager instance = null;
    Paint paint = new Paint();

    public PaintManager() {
        this.paint.setAntiAlias(true);
    }

    public void drawImage(Canvas canvas, String image, int x, int y, int w, int h) {
        if (ImageLoader.getInstance().getImage(image) != null) {
            ImageLoader.getInstance().getImage(image).setBounds(x, y, x + w, y + h);
            ImageLoader.getInstance().getImage(image).draw(canvas);
        }
    }

    public void setFont(Typeface f) {
        this.paint.setTypeface(f);
    }

    public int getTextSize() {
        return (int) this.paint.getTextSize();
    }

    public void drawLine(Canvas canvas, int x1, int y1, int x2, int y2) {
        canvas.drawLine((float) x1, (float) y1, (float) x2, (float) y2, this.paint);
    }

    public void setColor(int color) {
        this.paint.setColor(color);
    }

    public void fillRectangle(Canvas canvas, int x, int y, int w, int h) {
        this.paint.setStyle(Paint.Style.FILL);
        canvas.drawRect((float) x, (float) y, (float) (x + w), (float) (y + h), this.paint);
    }

    public void fillCircle(Canvas canvas, int x, int y, int w, int h) {
        this.paint.setStyle(Paint.Style.FILL);
        canvas.drawOval(new RectF((float) x, (float) y, (float) (x + w), (float) (y + h)), this.paint);
    }

    public void drawCircle(Canvas canvas, int x, int y, int w, int h) {
        this.paint.setStyle(Paint.Style.STROKE);
        canvas.drawOval(new RectF((float) x, (float) y, (float) (x + w), (float) (y + h)), this.paint);
    }

    public void drawRectangle(Canvas canvas, int x, int y, int w, int h) {
        this.paint.setStyle(Paint.Style.STROKE);
        canvas.drawRect((float) x, (float) y, (float) (x + w), (float) (y + h), this.paint);
    }

    public void setTextSize(int size) {
        this.paint.setTextSize((float) size);
    }

    public void drawString(Canvas canvas, String str, int x, int y, int w, int h, int format) {
        int text_h = (int) this.paint.getTextSize();
        int text_w = (int) this.paint.measureText(str);
        int px = x;
        int py = y;
        if (format == STR_CENTER) {
            px = ((w / 2) + x) - (text_w / 2);
            py = y + (h / 2);
        }
        if (format == STR_LEFT) {
            py = (h / 2) + y + (text_h / 3);
        }
        if (format == STR_RIGHT) {
            py = (h / 2) + y + (text_h / 3);
            px = (x + w) - text_w;
        }
        canvas.drawText(str, (float) px, (float) py, this.paint);
    }

    public static PaintManager getInstance() {
        if (instance == null) {
            instance = new PaintManager();
        }
        return instance;
    }
}
