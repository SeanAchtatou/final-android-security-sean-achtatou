package softkos.rubix;

import android.graphics.Point;
import android.os.Handler;
import android.os.Message;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;

public class Vars {
    public static final int UPDATE = 100;
    static Vars instance = null;
    public int GO_TO_MOBILSOFT;
    public int HOWTOPLAY;
    public int OTHER_APP;
    public int PLAY_NEXT_LEVEL;
    public int RESET_ALL_LEVELS;
    public int SOUND_SETTINGS;
    public int START_GAME;
    public int TWITTER;
    int[][] board;
    int board_h;
    int board_pos_x;
    int board_pos_y;
    int board_w;
    ArrayList<Box> boxList;
    ArrayList<gButton> buttonList;
    public GameState gameState;
    ArrayList<GameString> gameStringList;
    boolean game_over;
    int level;
    int[] level_borders;
    int level_progress;
    int max_level_progress;
    Handler messageHandler;
    public int painting;
    Random random;
    int scores;
    int[][] scores_board;
    int screenHeight;
    int screenWidth;
    int selectedBox;
    ArrayList<Star> starList;
    int target_level_progress;
    Timer updateTimer;

    public enum GameState {
        Menu,
        Game,
        Highscores,
        HowPlay,
        Settings
    }

    public Vars() {
        this.painting = 0;
        this.level = 0;
        this.scores = 0;
        this.board_w = 7;
        this.board_h = 7;
        this.buttonList = null;
        this.gameStringList = null;
        this.boxList = null;
        this.starList = null;
        this.messageHandler = null;
        this.game_over = false;
        this.SOUND_SETTINGS = 1001;
        this.RESET_ALL_LEVELS = 1002;
        this.START_GAME = 100;
        this.GO_TO_MOBILSOFT = 101;
        this.HOWTOPLAY = 102;
        this.TWITTER = 103;
        this.OTHER_APP = 104;
        this.PLAY_NEXT_LEVEL = 200;
        this.buttonList = new ArrayList<>();
        this.buttonList.clear();
        this.gameStringList = new ArrayList<>();
        this.gameStringList.clear();
        this.boxList = new ArrayList<>();
        this.boxList.clear();
        this.starList = new ArrayList<>();
        this.starList.clear();
        this.random = new Random();
        this.updateTimer = new Timer();
        this.updateTimer.schedule(new UpdateTimer(), 10, 50);
        this.board = (int[][]) Array.newInstance(Integer.TYPE, this.board_w, this.board_h);
        this.level = 0;
        this.messageHandler = new Handler() {
            public void handleMessage(Message msg) {
                if (msg.arg1 == 100) {
                    Vars.this.advanceFrame();
                }
            }
        };
    }

    public Handler getMessageHandler() {
        return this.messageHandler;
    }

    public void setBoardAt(int x, int y, int v) {
        if (x >= 0 && x < this.board_w && y >= 0 && y < this.board_h) {
            this.board[x][y] = v;
        }
    }

    public int getBoardWidth() {
        return this.board_w;
    }

    public int getBoardHeight() {
        return this.board_h;
    }

    public void clearDeadObjects() {
        for (int i = 0; i < this.boxList.size(); i++) {
            if (!getBox(i).isVisible() || getBox(i).getPy() > this.screenHeight) {
                this.boxList.remove(i);
            }
        }
        for (int i2 = 0; i2 < this.starList.size(); i2++) {
            if (getStar(i2).getPy() > this.screenHeight) {
                this.starList.remove(i2);
            }
        }
    }

    public void advanceFrame() {
        if (this.gameState == GameState.Menu || this.gameState == GameState.Game) {
            if (this.gameState == GameState.Game) {
                updateBoxes();
                checkEndLevel();
                for (int i = 0; i < this.starList.size(); i++) {
                    this.starList.get(i).update();
                }
                for (int i2 = 0; i2 < this.gameStringList.size(); i2++) {
                    this.gameStringList.get(i2).update();
                }
                checkLevelFailed();
                clearDeadObjects();
                clearGameStrings();
                if (this.level_progress < this.target_level_progress) {
                    this.level_progress++;
                    if (this.level_progress >= this.max_level_progress) {
                        nextLevel();
                    }
                }
                GameCanvas.getInstance().postInvalidate();
            }
            if (this.gameState == GameState.Menu) {
                for (int i3 = 0; i3 < this.buttonList.size(); i3++) {
                    getButton(i3).updatePosition();
                }
                MenuCanvas.getInstance().postInvalidate();
            }
        }
    }

    public void clearGameStrings() {
        for (int i = 0; i < this.gameStringList.size(); i++) {
            if (getGameString(i).getTimer() < 0) {
                this.gameStringList.remove(i);
            }
        }
    }

    public ArrayList<Star> getStarList() {
        return this.starList;
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public Star getStar(int i) {
        if (i < 0 || i >= this.starList.size()) {
            return null;
        }
        return this.starList.get(i);
    }

    public void updateBoxes() {
        boolean moved = false;
        for (int i = 0; i < this.boxList.size(); i++) {
            moved |= getBox(i).update();
        }
        if (!moved) {
            addNewBoxes();
        }
    }

    public boolean checkLevelFailed() {
        return false;
    }

    public boolean checkEndLevel() {
        return true;
    }

    public void hideAllButtons() {
        for (int i = 0; i < this.buttonList.size(); i++) {
            getButton(i).hide();
        }
    }

    public int getBoardPosY() {
        return this.board_pos_y;
    }

    public int getBoardPosX() {
        return this.board_pos_x;
    }

    public void initLevel() {
        this.board = (int[][]) Array.newInstance(Integer.TYPE, this.board_w, this.board_h);
        this.starList.clear();
        this.max_level_progress = (getLevel() + 1) * 200;
        this.level_progress = 0;
        this.target_level_progress = 0;
        int boxSize = this.screenWidth / this.board_w;
        this.board_pos_y = (this.screenHeight / 2) - ((this.board_h * boxSize) / 2);
        this.board_pos_x = (this.screenWidth - (this.board_w * boxSize)) / 2;
        for (int y = 0; y < this.board_h; y++) {
            for (int x = 0; x < this.board_w; x++) {
                addBox(x, y);
            }
        }
    }

    public double getLevelProgress() {
        return ((double) this.level_progress) / ((double) this.max_level_progress);
    }

    public void addBox(int x, int y) {
        int boxSize = this.screenWidth / this.board_w;
        this.board[x][y] = nextInt(5);
        Box b = new Box();
        b.setSize(boxSize, boxSize);
        b.SetPos(this.board_pos_x + (x * boxSize), this.board_pos_y + (y * boxSize));
        b.setBoardPos(x, y);
        b.setType(this.board[x][y]);
        this.boxList.add(b);
    }

    public void nextLevel() {
        this.level++;
        for (int i = 0; i < this.boxList.size(); i++) {
            getBox(i).fall();
        }
        addGameString("Level UP!", 50, 255, 100, 255, 100, new Point(0, (this.screenHeight / 2) - 20));
        initLevel();
    }

    public int getScore() {
        return this.scores;
    }

    public int getBoardAt(int x, int y) {
        if (x < 0 || x >= this.board_w || y < 0 || y >= this.board_h) {
            return -1;
        }
        return this.board[x][y];
    }

    public double dist(int x1, int y1, int x2, int y2) {
        return Math.sqrt((double) (((x1 - x2) * (x1 - x2)) + ((y1 - y2) * (y1 - y2))));
    }

    public void setLevel(int l) {
        this.level = l;
    }

    public void prevLevel() {
        this.level--;
        if (this.level < 0) {
            this.level = 0;
        }
        initLevel();
    }

    public void resetLevel() {
        initLevel();
    }

    public int getLevel() {
        return this.level;
    }

    public void newGame() {
        this.scores = 0;
        this.game_over = false;
        this.level = 0;
        this.selectedBox = -1;
        this.boxList.clear();
        initLevel();
    }

    public int getSelectedBox() {
        return this.selectedBox;
    }

    public void selectBoxAt(int x, int y) {
        int s = -1;
        int i = 0;
        while (true) {
            if (i < this.boxList.size()) {
                if (getBox(i).isVisible() && x >= getBox(i).getPx() && x < getBox(i).getPx() + getBox(i).getWidth() && y >= getBox(i).getPy() && y < getBox(i).getPy() + getBox(i).getHeight()) {
                    s = i;
                    break;
                }
                i++;
            } else {
                break;
            }
        }
        if (s >= 0) {
            hideBoxes(s);
        }
    }

    public void addNewBoxes() {
        for (int y = 0; y < this.board_h; y++) {
            for (int x = 0; x < this.board_w; x++) {
                if (this.board[x][y] < 0) {
                    addBox(x, y);
                }
            }
        }
        if (!movePossible()) {
            gameOver();
        }
    }

    public boolean movePossible() {
        for (int i = 0; i < this.boxList.size(); i++) {
            if (boxesToHide(i) != null) {
                return true;
            }
        }
        return false;
    }

    public void addScores(int s, Point p) {
        int a = (s * 5) + getLevel();
        this.scores += a;
        this.target_level_progress += a;
        addGameString("+" + a, 34, 255, 255, 255, 255, p);
    }

    public void addStars(int x, int y) {
        for (int z = 0; z < 7; z++) {
            Star s = new Star();
            s.setSize(this.screenWidth / 12, this.screenWidth / 12);
            s.setPos(x, y);
            this.starList.add(s);
        }
    }

    public boolean fallBoxes() {
        boolean fall = false;
        for (int x = 0; x < this.board_w; x++) {
            for (int y = this.board_h - 1; y >= 0; y--) {
                if (getBoardAt(x, y) < 0) {
                    int cnt = 0;
                    for (int yy = y - 1; yy >= 0; yy--) {
                        cnt++;
                        if (getBoardAt(x, yy) >= 0) {
                            break;
                        }
                    }
                    setBoardAt(x, y, getBoardAt(x, y - cnt));
                    setBoardAt(x, y - cnt, -1);
                    int nb = 0;
                    while (true) {
                        if (nb >= this.boxList.size()) {
                            break;
                        } else if (getBox(nb).isVisible() && getBox(nb).getBoardPosX() == x && getBox(nb).getBoardPosY() == y - cnt) {
                            getBox(nb).setBoardPos(x, y);
                            getBox(nb).setDestPos(getBox(nb).getPx(), getBox(nb).getPy() + (getBox(nb).getWidth() * cnt));
                            getBox(nb).setSpeeds(0, 5);
                            fall = true;
                            break;
                        } else {
                            nb++;
                        }
                    }
                }
            }
        }
        return fall;
    }

    public int getBoxAtBoardXY(int x, int y) {
        for (int i = 0; i < this.boxList.size(); i++) {
            if (getBox(i).getBoardPosX() == x && getBox(i).getBoardPosY() == y) {
                return i;
            }
        }
        return -1;
    }

    public int[] boxesToHide(int b) {
        if (b < 0) {
            return null;
        }
        int type = getBox(b).getType();
        int[] list = new int[100];
        for (int i = 0; i < list.length; i++) {
            list[i] = -1;
        }
        list[0] = b;
        int last_index = 0 + 1;
        int c = 0;
        while (c < list.length && list[c] >= 0) {
            if (getBox(list[c]) != null) {
                int x = getBox(list[c]).getBoardPosX();
                int y = getBox(list[c]).getBoardPosY();
                int[] boxes = {getBoxAtBoardXY(x - 1, y), getBoxAtBoardXY(x + 1, y), getBoxAtBoardXY(x, y - 1), getBoxAtBoardXY(x, y + 1)};
                for (int a = 0; a < boxes.length; a++) {
                    if (boxes[a] >= 0 && type >= 0 && getBox(boxes[a]).getType() == type) {
                        boolean on_list = false;
                        int i2 = 0;
                        while (true) {
                            if (i2 >= list.length) {
                                break;
                            } else if (boxes[a] == list[i2]) {
                                on_list = true;
                                break;
                            } else {
                                i2++;
                            }
                        }
                        if (!on_list) {
                            list[last_index] = boxes[a];
                            last_index++;
                        }
                    }
                }
            }
            c++;
        }
        int scr = 0;
        int i3 = 0;
        while (i3 < list.length && list[i3] >= 0) {
            scr++;
            i3++;
        }
        if (scr < 3) {
            return null;
        }
        return list;
    }

    public void hideBoxes(int b) {
        int[] list = boxesToHide(b);
        if (list != null) {
            int scr = 0;
            int i = 0;
            while (i < list.length && list[i] >= 0) {
                scr++;
                i++;
            }
            if (scr >= 3) {
                int ax = 0;
                int ay = 0;
                int i2 = 0;
                while (i2 < list.length && list[i2] >= 0) {
                    getBox(list[i2]).hide();
                    setBoardAt(getBox(list[i2]).getBoardPosX(), getBox(list[i2]).getBoardPosY(), -1);
                    addStars(getBox(list[i2]).getPx(), getBox(list[i2]).getPy());
                    ax += getBox(list[i2]).getPx();
                    ay += getBox(list[i2]).getPy();
                    i2++;
                }
                addScores(scr, new Point(ax / (scr + 1), ay / (scr + 1)));
                this.selectedBox = -1;
                fallBoxes();
            }
        }
    }

    public int getButtonListSize() {
        return this.buttonList.size();
    }

    public void addButton(gButton b) {
        this.buttonList.add(b);
    }

    public ArrayList<gButton> getButtonList() {
        return this.buttonList;
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public gButton getButton(int b) {
        if (b < 0 || b >= this.buttonList.size()) {
            return null;
        }
        return this.buttonList.get(b);
    }

    public ArrayList<GameString> getGameStringList() {
        return this.gameStringList;
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public GameString getGameString(int b) {
        if (b < 0 || b >= this.gameStringList.size()) {
            return null;
        }
        return this.gameStringList.get(b);
    }

    public void addGameString(String s, int size, int a, int r, int g, int b, Point p) {
        GameString gs = new GameString();
        gs.setText(s);
        gs.setSize(size);
        gs.setColor(a, r, g, b);
        gs.SetPos(p.x, p.y);
        gs.setSize(s.length() * ((int) (((double) size) * 0.8d)), size);
        this.gameStringList.add(gs);
    }

    public void addGameString(String s, int size, int color, Point p) {
        GameString gs = new GameString();
        gs.setText(s);
        gs.setSize(size);
        gs.setColor(color);
        gs.SetPos(p.x, p.y);
        gs.setSize(s.length() * ((int) (((double) size) * 0.8d)), size);
        this.gameStringList.add(gs);
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public Box getBox(int i) {
        if (i < 0 || i >= this.boxList.size()) {
            return null;
        }
        return this.boxList.get(i);
    }

    public ArrayList<Box> getBoxList() {
        return this.boxList;
    }

    public int getScreenWidth() {
        return this.screenWidth;
    }

    public int getScreenHeight() {
        return this.screenHeight;
    }

    public void init() {
        this.random = new Random();
    }

    public int GetNextInt(int max) {
        if (max == 0) {
            max = 1;
        }
        return Math.abs(this.random.nextInt() % max);
    }

    public Random getRandom() {
        return this.random;
    }

    public void screenSize(int w, int h) {
        this.screenWidth = w;
        this.screenHeight = h;
    }

    public static Vars getInstance() {
        if (instance == null) {
            instance = new Vars();
            instance.init();
        }
        return instance;
    }

    public int nextInt(int max) {
        if (max < 1) {
            return 0;
        }
        return this.random.nextInt(max);
    }

    public void gameOver() {
        this.game_over = true;
    }

    public boolean isGameOver() {
        return this.game_over;
    }
}
