package softkos.rubix;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import java.util.Random;

public class PushNotifications {
    public static final String PREFS_NAME = "SoftkosUsedNotifications";
    static PushNotifications instance = null;
    private int SIMPLE_NOTFICATION_ID;
    int current = 0;
    int[] images = {R.drawable.ume_ad, R.drawable.untangle_ad, R.drawable.frogs_jump_ad};
    private NotificationManager mNotificationManager;
    Random random = new Random();
    CharSequence[] text = {"Extreme Untangle ME ;)", "Just... Untangle me ;)", "Jump over frogs, and make them disappear!"};
    CharSequence[] title = {"Cubix", "Untangle me", "Frogs Jump"};
    String[] urls = {"market://details?id=softkos.untanglemeextreme", "market://details?id=softkos.untangleme", "market://details?id=sofkos.frogsjump"};
    long used_notifications = 0;

    public boolean notificationUsed(int n) {
        long used = MainActivity.getInstance().getSharedPreferences(PREFS_NAME, 0).getLong("used_notifications", 0);
        this.used_notifications = used;
        if ((used & ((long) (1 << n))) != 0) {
            return true;
        }
        return false;
    }

    public void saveUsedNotifications(int c) {
        SharedPreferences.Editor editor = MainActivity.getInstance().getSharedPreferences(PREFS_NAME, 0).edit();
        editor.putLong("used_notifications", this.used_notifications | ((long) (1 << c)));
        editor.commit();
    }

    public void pushNotification() {
        try {
            this.current = this.random.nextInt(this.images.length * 5);
            if (this.current < this.images.length && !notificationUsed(this.current)) {
                saveUsedNotifications(this.current);
                this.mNotificationManager = (NotificationManager) MainActivity.getInstance().getSystemService("notification");
                Notification notifyDetails = new Notification(this.images[this.current], this.title[this.current], System.currentTimeMillis());
                notifyDetails.flags = 16;
                notifyDetails.setLatestEventInfo(MainActivity.getInstance().getApplicationContext(), this.title[this.current], this.text[this.current], PendingIntent.getActivity(MainActivity.getInstance(), 0, new Intent("android.intent.action.VIEW", Uri.parse(this.urls[this.current])), 268435456));
                this.mNotificationManager.notify(this.SIMPLE_NOTFICATION_ID, notifyDetails);
            }
        } catch (Exception e) {
            System.out.println("ex: " + e.getMessage());
        }
    }

    public static PushNotifications getInstance() {
        if (instance == null) {
            instance = new PushNotifications();
        }
        return instance;
    }
}
