package softkos.rubix;

public class Star extends GameObject {
    double angle;
    double time;
    double tmp_px;
    double tmp_py;
    double vo;
    double vx;
    double vy;

    public Star() {
        this.angle = 0.0d;
        this.time = 0.0d;
        this.vo = (double) (Vars.getInstance().GetNextInt(40) - 20);
        this.angle = (Vars.getInstance().getRandom().nextDouble() % 3.1415d) - 1.507d;
    }

    public int getPx() {
        return (int) this.tmp_px;
    }

    public int getPy() {
        return (int) this.tmp_py;
    }

    public void update() {
        this.vx = this.vo * Math.cos(this.angle);
        this.vy = (this.vo * Math.sin(this.angle)) - (9.81d * this.time);
        this.time += 0.2d;
        this.tmp_px += this.vx;
        this.tmp_py -= this.vy;
    }

    public void setPos(int x, int y) {
        this.tmp_px = (double) x;
        this.tmp_py = (double) y;
    }
}
