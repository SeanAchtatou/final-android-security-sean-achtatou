package com.mobcent.lib.android.ui.activity.adapter;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import com.mobcent.android.seed.autovGhnzo98NIzLsRPLHS.R;
import com.mobcent.android.service.impl.MCLibUserInfoServiceImpl;
import com.mobcent.android.service.impl.MCLibUserPrivateMsgServiceImpl;
import com.mobcent.lib.android.ui.activity.MCLibChatRoomActivity;
import com.mobcent.lib.android.ui.delegate.MCLibQuickMsgPanelDelegate;
import java.util.List;

public class MCLibQuickMsgListAdapter extends MCLibBaseArrayAdapter {
    /* access modifiers changed from: private */
    public final MCLibChatRoomActivity activity;
    /* access modifiers changed from: private */
    public MCLibQuickMsgPanelDelegate delegate;
    /* access modifiers changed from: private */
    public List<String> quickMsgList;
    private int rowResourceId;

    public MCLibQuickMsgListAdapter(MCLibChatRoomActivity activity2, int rowResourceId2, List<String> quickMsgList2, MCLibQuickMsgPanelDelegate delegate2) {
        super(activity2, rowResourceId2, quickMsgList2);
        this.rowResourceId = rowResourceId2;
        this.inflater = LayoutInflater.from(activity2);
        this.activity = activity2;
        this.quickMsgList = quickMsgList2;
        this.delegate = delegate2;
    }

    public int getCount() {
        return this.quickMsgList.size();
    }

    public Object getItem(int position) {
        return this.quickMsgList.get(position);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        final String quickMsg = this.quickMsgList.get(position);
        if (quickMsg == null || quickMsg.equals("")) {
            View convertView2 = this.inflater.inflate((int) R.layout.mc_lib_quick_msg_input_list_row, (ViewGroup) null);
            final EditText quickMsgEditText = (EditText) convertView2.findViewById(R.id.mcLibAddQuickMsgEditText);
            ((Button) convertView2.findViewById(R.id.mcLibQuickMsgAddBtn)).setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    String newMsg = quickMsgEditText.getText().toString();
                    if (newMsg != null && !newMsg.equals("")) {
                        if (new MCLibUserPrivateMsgServiceImpl(MCLibQuickMsgListAdapter.this.getContext()).addUserCustomizedQuickMsg(new MCLibUserInfoServiceImpl(MCLibQuickMsgListAdapter.this.getContext()).getLoginUserId(), newMsg)) {
                            MCLibQuickMsgListAdapter.this.quickMsgList.add(newMsg);
                            MCLibQuickMsgListAdapter.this.notifyDataSetInvalidated();
                            MCLibQuickMsgListAdapter.this.notifyDataSetChanged();
                        }
                    }
                }
            });
            return convertView2;
        }
        View convertView3 = this.inflater.inflate(this.rowResourceId, (ViewGroup) null);
        ((TextView) convertView3.findViewById(R.id.mcLibMsgTextView)).setText(quickMsg);
        ((ImageButton) convertView3.findViewById(R.id.mcLibRemoveQuickMsgBtn)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCLibQuickMsgListAdapter.this.showRemoveWarningDialog(quickMsg);
            }
        });
        convertView3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCLibQuickMsgListAdapter.this.activity.msgInputEditText.setText(quickMsg);
                MCLibQuickMsgListAdapter.this.activity.sendMsgBtn.performClick();
                MCLibQuickMsgListAdapter.this.delegate.closeQuickRespPanel();
            }
        });
        return convertView3;
    }

    /* access modifiers changed from: private */
    public void showRemoveWarningDialog(final String msg) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
        alertDialog.setTitle((int) R.string.mc_lib_tips);
        alertDialog.setMessage((int) R.string.mc_lib_confirm_remove_msg);
        alertDialog.setPositiveButton((int) R.string.mc_lib_confirm, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                if (new MCLibUserPrivateMsgServiceImpl(MCLibQuickMsgListAdapter.this.getContext()).removeUserCustomizedQuickMsg(new MCLibUserInfoServiceImpl(MCLibQuickMsgListAdapter.this.getContext()).getLoginUserId(), msg)) {
                    MCLibQuickMsgListAdapter.this.quickMsgList.remove(msg);
                    MCLibQuickMsgListAdapter.this.notifyDataSetInvalidated();
                    MCLibQuickMsgListAdapter.this.notifyDataSetChanged();
                }
            }
        });
        alertDialog.setNegativeButton((int) R.string.mc_lib_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        });
        alertDialog.show();
    }
}
