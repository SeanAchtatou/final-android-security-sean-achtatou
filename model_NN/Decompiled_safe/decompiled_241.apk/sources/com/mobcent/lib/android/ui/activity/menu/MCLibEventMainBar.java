package com.mobcent.lib.android.ui.activity.menu;

import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import com.mobcent.android.seed.autovGhnzo98NIzLsRPLHS.R;
import com.mobcent.lib.android.ui.activity.MCLibEventBundleActivity;
import com.mobcent.lib.android.ui.delegate.MCLibUpdateListDelegate;

public class MCLibEventMainBar {
    /* access modifiers changed from: private */
    public MCLibEventBundleActivity activity;
    private TextView eventHallText;
    private TextView friendsEventText;
    private MCLibUpdateListDelegate menuUpdateStatusDelegate;
    /* access modifiers changed from: private */
    public Integer statusType;

    public void buildActionBar(MCLibEventBundleActivity activity2, MCLibUpdateListDelegate menuUpdateStatusDelegate2, Integer statusType2) {
        this.activity = activity2;
        this.menuUpdateStatusDelegate = menuUpdateStatusDelegate2;
        this.statusType = statusType2;
        initWidgets();
        initEventHallAction();
        initFriendsEventAction();
        initCurrentMenu();
    }

    private void initWidgets() {
        this.eventHallText = (TextView) this.activity.findViewById(R.id.mcLibPublicEvents);
        this.friendsEventText = (TextView) this.activity.findViewById(R.id.mcLibFriendsEvents);
    }

    private void initEventHallAction() {
        this.eventHallText.setOnTouchListener(new ActionButtonOnTouchListener(1));
        this.eventHallText.setOnClickListener(new MicroblogMainBarOnClickListener(this.menuUpdateStatusDelegate, 1));
    }

    private void initFriendsEventAction() {
        this.friendsEventText.setOnTouchListener(new ActionButtonOnTouchListener(2));
        this.friendsEventText.setOnClickListener(new MicroblogMainBarOnClickListener(this.menuUpdateStatusDelegate, 2));
    }

    /* access modifiers changed from: private */
    public void initCurrentMenu() {
        if (this.statusType.intValue() == 1) {
            this.friendsEventText.setBackgroundResource(R.drawable.mc_lib_blank);
            this.eventHallText.setBackgroundResource(R.drawable.mc_lib_submenu_bg);
            this.eventHallText.performClick();
        } else if (this.statusType.intValue() == 2) {
            this.eventHallText.setBackgroundResource(R.drawable.mc_lib_blank);
            this.friendsEventText.setBackgroundResource(R.drawable.mc_lib_submenu_bg);
            this.friendsEventText.performClick();
        }
    }

    public class ActionButtonOnTouchListener implements View.OnTouchListener {
        private int statusType;

        public ActionButtonOnTouchListener(int statusType2) {
            this.statusType = statusType2;
        }

        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() != 0) {
                if (1 == event.getAction()) {
                    Integer unused = MCLibEventMainBar.this.statusType = Integer.valueOf(this.statusType);
                    MCLibEventMainBar.this.initCurrentMenu();
                } else if (3 == event.getAction() || 2 == event.getAction() || 4 == event.getAction()) {
                }
            }
            return true;
        }
    }

    public class MicroblogMainBarOnClickListener implements View.OnClickListener {
        private MCLibUpdateListDelegate delegate;
        private Integer statusType;

        public MicroblogMainBarOnClickListener(MCLibUpdateListDelegate delegate2, Integer statusType2) {
            this.delegate = delegate2;
            this.statusType = statusType2;
        }

        public void onClick(View v) {
            MCLibEventMainBar.this.activity.setStatusType(this.statusType);
            Integer unused = MCLibEventMainBar.this.statusType = this.statusType;
            this.delegate.updateList(1, 10, true);
        }
    }
}
