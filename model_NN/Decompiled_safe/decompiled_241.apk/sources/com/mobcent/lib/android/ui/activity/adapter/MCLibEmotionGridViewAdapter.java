package com.mobcent.lib.android.ui.activity.adapter;

import android.app.Activity;
import android.os.Handler;
import android.text.Selection;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import com.mobcent.android.seed.autovGhnzo98NIzLsRPLHS.R;
import com.mobcent.lib.android.ui.delegate.MCLibQuickMsgPanelDelegate;
import com.mobcent.lib.android.utils.MCLibTextViewUtil;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class MCLibEmotionGridViewAdapter extends MCLibBaseSimpleAdapter {
    /* access modifiers changed from: private */
    public Activity activity;
    /* access modifiers changed from: private */
    public EditText contentEditText;
    private List<HashMap<String, Integer>> data;
    /* access modifiers changed from: private */
    public MCLibQuickMsgPanelDelegate delegate;
    private ImageView emotionImage;
    private LayoutInflater inflater;
    /* access modifiers changed from: private */
    public Handler mHandler;
    private int resource;

    public MCLibEmotionGridViewAdapter(Activity activity2, List<HashMap<String, Integer>> data2, int resource2, String[] from, int[] to, EditText contentEditText2, MCLibQuickMsgPanelDelegate delegate2, Handler mHandler2) {
        super(activity2, data2, resource2, from, to);
        this.data = data2;
        this.resource = resource2;
        this.inflater = LayoutInflater.from(activity2);
        this.activity = activity2;
        this.mHandler = mHandler2;
        this.delegate = delegate2;
        this.contentEditText = contentEditText2;
    }

    public int getCount() {
        return this.data.size();
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        HashMap<String, Integer> currentData = this.data.get(position);
        final String key = getCurrentKey(currentData);
        View convertView2 = this.inflater.inflate(this.resource, (ViewGroup) null);
        this.emotionImage = (ImageView) convertView2.findViewById(R.id.mcLibEmotionImage);
        this.emotionImage.setBackgroundResource(((Integer) currentData.get(key)).intValue());
        convertView2.setOnClickListener(new View.OnClickListener() {
            public void onClick(final View v) {
                MCLibEmotionGridViewAdapter.this.mHandler.post(new Runnable() {
                    public void run() {
                        Animation anim = AnimationUtils.loadAnimation(MCLibEmotionGridViewAdapter.this.activity, R.anim.mc_lib_grow_to_middle);
                        anim.setInterpolator(new AccelerateInterpolator());
                        v.startAnimation(anim);
                        anim.setAnimationListener(new Animation.AnimationListener() {
                            public void onAnimationStart(Animation animation) {
                            }

                            public void onAnimationRepeat(Animation animation) {
                            }

                            public void onAnimationEnd(Animation animation) {
                                MCLibEmotionGridViewAdapter.this.contentEditText.setText(MCLibTextViewUtil.transformCharSequenceWithEmotions(MCLibEmotionGridViewAdapter.this.contentEditText.getText().toString() + key + " ", MCLibEmotionGridViewAdapter.this.activity));
                                Selection.setSelection(MCLibEmotionGridViewAdapter.this.contentEditText.getText(), MCLibEmotionGridViewAdapter.this.contentEditText.getText().length());
                                MCLibEmotionGridViewAdapter.this.delegate.closeQuickRespPanel();
                            }
                        });
                    }
                });
            }
        });
        return convertView2;
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    private String getCurrentKey(HashMap<String, Integer> map) {
        Iterator<Map.Entry<String, Integer>> it = map.entrySet().iterator();
        if (it.hasNext()) {
            return (String) it.next().getKey();
        }
        return "";
    }
}
