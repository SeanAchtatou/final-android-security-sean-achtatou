package com.mobcent.lib.android.ui.activity.adapter;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import com.mobcent.android.model.MCLibActionModel;
import com.mobcent.android.seed.autovGhnzo98NIzLsRPLHS.R;
import com.mobcent.lib.android.ui.activity.MCLibChatRoomActivity;
import com.mobcent.lib.android.ui.activity.view.util.MCLibDisplayGifUtil;
import com.mobcent.lib.android.ui.delegate.MCLibUserActionDelegate;
import com.mobcent.lib.android.utils.MCLibImageUtil;
import java.util.HashMap;
import java.util.List;

public class MCLibMagicGridViewAdapter extends MCLibBaseSimpleAdapter {
    /* access modifiers changed from: private */
    public MCLibChatRoomActivity activity;
    private LayoutInflater inflater;
    /* access modifiers changed from: private */
    public Handler mHandler;
    private List<MCLibActionModel> magicActionList;
    private int resource;

    public MCLibMagicGridViewAdapter(MCLibChatRoomActivity activity2, List<HashMap<String, Integer>> data, List<MCLibActionModel> magicActionList2, int resource2, String[] from, int[] to, Handler mHandler2) {
        super(activity2, data, resource2, from, to);
        this.resource = resource2;
        this.inflater = LayoutInflater.from(activity2);
        this.activity = activity2;
        this.mHandler = mHandler2;
        this.magicActionList = magicActionList2;
    }

    public int getCount() {
        return this.magicActionList.size();
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        final MCLibActionModel actionModel = this.magicActionList.get(position);
        View convertView2 = this.inflater.inflate(this.resource, (ViewGroup) null);
        MCLibImageUtil.updateImage((ImageView) convertView2.findViewById(R.id.mcLibMagicImage), actionModel.getBaseUrl() + actionModel.getActionPrevImg(), this.activity, R.drawable.mc_lib_unloaded_img, this.mHandler);
        ((TextView) convertView2.findViewById(R.id.mcLibMagicShortNameText)).setText(actionModel.getShortName());
        convertView2.setOnClickListener(new View.OnClickListener() {
            public void onClick(final View v) {
                MCLibMagicGridViewAdapter.this.mHandler.post(new Runnable() {
                    public void run() {
                        Animation anim = AnimationUtils.loadAnimation(MCLibMagicGridViewAdapter.this.activity, R.anim.mc_lib_grow_to_middle);
                        anim.setInterpolator(new AccelerateInterpolator());
                        v.startAnimation(anim);
                        anim.setAnimationListener(new Animation.AnimationListener() {
                            public void onAnimationStart(Animation animation) {
                            }

                            public void onAnimationRepeat(Animation animation) {
                            }

                            public void onAnimationEnd(Animation animation) {
                                MCLibMagicGridViewAdapter.this.activity.gifMaskLayer.setVisibility(0);
                                new MCLibDisplayGifUtil(MCLibMagicGridViewAdapter.this.activity, MCLibMagicGridViewAdapter.this.activity.previewGifView, MCLibMagicGridViewAdapter.this.activity.gifPrgBarBox, true, MCLibMagicGridViewAdapter.this.mHandler).showMobCentBouncePanel(actionModel, new UpdateGifFinishDelegate(actionModel));
                                MCLibMagicGridViewAdapter.this.activity.confirmBtn.setTag(actionModel);
                            }
                        });
                    }
                });
            }
        });
        return convertView2;
    }

    public class UpdateGifFinishDelegate implements MCLibUserActionDelegate {
        /* access modifiers changed from: private */
        public MCLibActionModel actionModel;

        public UpdateGifFinishDelegate(MCLibActionModel actionModel2) {
            this.actionModel = actionModel2;
        }

        public void doUserAction() {
            MCLibMagicGridViewAdapter.this.mHandler.postDelayed(new Runnable() {
                public void run() {
                    new MCLibDisplayGifUtil(MCLibMagicGridViewAdapter.this.activity, MCLibMagicGridViewAdapter.this.activity.previewGifView, MCLibMagicGridViewAdapter.this.activity.gifPrgBarBox, false, MCLibMagicGridViewAdapter.this.mHandler).showMobCentBouncePanel(UpdateGifFinishDelegate.this.actionModel, new UpdateGifFinishDelegate(UpdateGifFinishDelegate.this.actionModel));
                }
            }, 1000);
        }
    }
}
