package com.mobcent.lib.android.ui.activity.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.SimpleAdapter;
import com.mobcent.lib.android.ui.delegate.MCLibUpdateListDelegate;
import java.util.List;
import java.util.Map;

public class MCLibBaseSimpleAdapter extends SimpleAdapter {
    protected MCLibUpdateListDelegate delegate;
    protected LayoutInflater inflater;

    public MCLibBaseSimpleAdapter(Context context, List<? extends Map<String, ?>> data, int resource, String[] from, int[] to) {
        super(context, data, resource, from, to);
    }
}
