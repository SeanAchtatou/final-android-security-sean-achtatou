package com.mobcent.lib.android.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.RelativeLayout;
import com.mobcent.android.seed.autovGhnzo98NIzLsRPLHS.R;

public class MCLibWebViewDialog extends Dialog {
    /* access modifiers changed from: private */
    public RelativeLayout dialogLoadingBox;
    /* access modifiers changed from: private */
    public String webUrl;

    public MCLibWebViewDialog(Context context, int theme, String webUrl2) {
        super(context, theme);
        this.webUrl = webUrl2;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.mc_lib_dialog_webview);
        final WebView webView = (WebView) findViewById(R.id.mcLibDialogWebView);
        this.dialogLoadingBox = (RelativeLayout) findViewById(R.id.mcLibDialogLoadingBox);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setSupportZoom(true);
        webView.setWebViewClient(new WebViewClient() {
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                MCLibWebViewDialog.this.dialogLoadingBox.setVisibility(0);
            }

            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                MCLibWebViewDialog.this.dialogLoadingBox.setVisibility(8);
            }
        });
        webView.loadUrl(this.webUrl);
        ((Button) findViewById(R.id.mcLibRefreshWebViewButton)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                webView.loadUrl(MCLibWebViewDialog.this.webUrl);
            }
        });
        ((Button) findViewById(R.id.mcLibCloseButton)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                MCLibWebViewDialog.this.dismiss();
            }
        });
    }
}
