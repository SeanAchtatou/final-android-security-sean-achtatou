package com.mobcent.lib.android.ui.activity.menu;

import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import com.mobcent.android.seed.autovGhnzo98NIzLsRPLHS.R;
import com.mobcent.lib.android.ui.activity.MCLibUserBundleActivity;
import com.mobcent.lib.android.ui.delegate.MCLibUpdateListDelegate;

public class MCLibUserMainBar {
    /* access modifiers changed from: private */
    public MCLibUserBundleActivity activity;
    private MCLibUpdateListDelegate menuUpdateStatusDelegate;
    private TextView recommendUserText;
    /* access modifiers changed from: private */
    public Integer tabType;
    private TextView userFansText;
    private TextView userFollowsText;

    public void buildActionBar(MCLibUserBundleActivity activity2, MCLibUpdateListDelegate menuUpdateStatusDelegate2, Integer tabType2) {
        this.activity = activity2;
        this.menuUpdateStatusDelegate = menuUpdateStatusDelegate2;
        this.tabType = tabType2;
        initWidgets();
        initUserFollowsAction();
        initUserFansAction();
        initRecommendUserAction();
        initCurrentMenu();
    }

    private void initWidgets() {
        this.userFollowsText = (TextView) this.activity.findViewById(R.id.mcLibUserFollows);
        this.userFansText = (TextView) this.activity.findViewById(R.id.mcLibUserFans);
        this.recommendUserText = (TextView) this.activity.findViewById(R.id.mcLibRecommendUsers);
    }

    private void initUserFollowsAction() {
        this.userFollowsText.setOnTouchListener(new ActionButtonOnTouchListener(100));
        this.userFollowsText.setOnClickListener(new MicroblogMainBarOnClickListener(this.menuUpdateStatusDelegate, 100));
    }

    private void initUserFansAction() {
        this.userFansText.setOnTouchListener(new ActionButtonOnTouchListener(101));
        this.userFansText.setOnClickListener(new MicroblogMainBarOnClickListener(this.menuUpdateStatusDelegate, 101));
    }

    private void initRecommendUserAction() {
        this.recommendUserText.setOnTouchListener(new ActionButtonOnTouchListener(103));
        this.recommendUserText.setOnClickListener(new MicroblogMainBarOnClickListener(this.menuUpdateStatusDelegate, 103));
    }

    /* access modifiers changed from: private */
    public void initCurrentMenu() {
        if (this.tabType.intValue() == 100) {
            this.userFansText.setBackgroundResource(R.drawable.mc_lib_blank);
            this.recommendUserText.setBackgroundResource(R.drawable.mc_lib_blank);
            this.userFollowsText.setBackgroundResource(R.drawable.mc_lib_submenu_bg);
            this.userFollowsText.performClick();
        } else if (this.tabType.intValue() == 101) {
            this.userFollowsText.setBackgroundResource(R.drawable.mc_lib_blank);
            this.recommendUserText.setBackgroundResource(R.drawable.mc_lib_blank);
            this.userFansText.setBackgroundResource(R.drawable.mc_lib_submenu_bg);
            this.userFansText.performClick();
        } else if (this.tabType.intValue() == 103) {
            this.userFollowsText.setBackgroundResource(R.drawable.mc_lib_blank);
            this.userFansText.setBackgroundResource(R.drawable.mc_lib_blank);
            this.recommendUserText.setBackgroundResource(R.drawable.mc_lib_submenu_bg);
            this.recommendUserText.performClick();
        }
    }

    public class ActionButtonOnTouchListener implements View.OnTouchListener {
        private int tabType;

        public ActionButtonOnTouchListener(int tabType2) {
            this.tabType = tabType2;
        }

        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() != 0) {
                if (1 == event.getAction()) {
                    Integer unused = MCLibUserMainBar.this.tabType = Integer.valueOf(this.tabType);
                    MCLibUserMainBar.this.initCurrentMenu();
                } else if (3 == event.getAction() || 2 == event.getAction() || 4 == event.getAction()) {
                }
            }
            return true;
        }
    }

    public class MicroblogMainBarOnClickListener implements View.OnClickListener {
        private MCLibUpdateListDelegate delegate;
        private Integer tabType;

        public MicroblogMainBarOnClickListener(MCLibUpdateListDelegate delegate2, Integer tabType2) {
            this.delegate = delegate2;
            this.tabType = tabType2;
        }

        public void onClick(View v) {
            MCLibUserMainBar.this.activity.setTabType(this.tabType.intValue());
            Integer unused = MCLibUserMainBar.this.tabType = this.tabType;
            this.delegate.updateList(1, 10, true);
        }
    }
}
