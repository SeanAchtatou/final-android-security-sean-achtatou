package com.mobcent.lib.android.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.mobcent.android.constants.MCLibConstants;
import com.mobcent.android.constants.MCLibMobCentApiConstant;
import com.mobcent.android.constants.MCLibParameterKeyConstant;
import com.mobcent.android.model.MCLibUserStatus;
import com.mobcent.android.seed.autovGhnzo98NIzLsRPLHS.R;
import com.mobcent.android.service.MCLibStatusService;
import com.mobcent.android.service.impl.MCLibStatusServiceImpl;
import com.mobcent.android.service.impl.MCLibUserInfoServiceImpl;
import com.mobcent.lib.android.ui.activity.adapter.MCLibStatusListAdapter;
import com.mobcent.lib.android.ui.activity.listener.MCLibListViewItemOnClickListener;
import com.mobcent.lib.android.ui.delegate.MCLibUpdateListDelegate;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class MCLibStatusRepliesActivity extends MCLibUIBaseActivity implements MCLibUpdateListDelegate {
    /* access modifiers changed from: private */
    public MCLibStatusListAdapter adapter;
    public Button backButton;
    private ListView bundleListView;
    private boolean isCommentList;
    /* access modifiers changed from: private */
    public Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            if (msg.what == 1 || msg.what == 5) {
                MCLibStatusRepliesActivity.this.updateEmptyAdapter(msg.obj);
            } else if (msg.what == 2) {
                MCLibStatusRepliesActivity.this.updateExistAdapter(msg.obj);
            } else if (msg.what == 3) {
                MCLibStatusRepliesActivity.this.hideProgressBar();
            } else if (msg.what == 4) {
                MCLibStatusRepliesActivity.this.showProgressBar();
            }
        }
    };
    private String objectId;
    /* access modifiers changed from: private */
    public ImageView previewImage;
    /* access modifiers changed from: private */
    public RelativeLayout previewImageBox;
    /* access modifiers changed from: private */
    public ProgressBar previewPrgBar;
    public ImageButton refreshReplyHistoryBtn;
    private TextView replyHistoryTitle;
    public Button replyTopicButton;
    protected AbsListView.OnScrollListener statusListOnScrollListener = new AbsListView.OnScrollListener() {
        private Vector<String> currentUrls;
        private int firstVisibleItem;
        private int totalItemCount;
        private int visibleItemCount;

        public void onScrollStateChanged(AbsListView view, int scrollState) {
            if (scrollState == 0) {
                List<String> imageUrls = new ArrayList<>();
                parseCurrentUrls();
                List<String> preUrls = getPreviousUrls();
                List<String> nextUrls = getNextImageUrls();
                if (preUrls != null) {
                    imageUrls.addAll(preUrls);
                }
                if (nextUrls != null) {
                    imageUrls.addAll(nextUrls);
                }
                MCLibStatusRepliesActivity.this.adapter.asyncImageLoader.recycleBitmap(imageUrls);
            }
        }

        public void onScroll(AbsListView view, int firstVisibleItem2, int visibleItemCount2, int totalItemCount2) {
            this.firstVisibleItem = firstVisibleItem2;
            this.visibleItemCount = visibleItemCount2;
            this.totalItemCount = totalItemCount2;
        }

        private void parseCurrentUrls() {
            String imgUrlReply;
            this.currentUrls = new Vector<>();
            int firstIndex = 0;
            int endIndex = this.firstVisibleItem + this.visibleItemCount + 10;
            if (this.firstVisibleItem - 10 > 0) {
                firstIndex = this.firstVisibleItem - 10;
            }
            if (endIndex >= this.totalItemCount) {
                endIndex = this.totalItemCount;
            }
            for (int i = firstIndex; i < endIndex; i++) {
                if (((MCLibUserStatus) MCLibStatusRepliesActivity.this.userStatusList.get(i)).getUserImageUrl() != null && !((MCLibUserStatus) MCLibStatusRepliesActivity.this.userStatusList.get(i)).getUserImageUrl().equals("")) {
                    this.currentUrls.add(((MCLibUserStatus) MCLibStatusRepliesActivity.this.userStatusList.get(i)).getUserImageUrl());
                }
                String imgUrlMain = ((MCLibUserStatus) MCLibStatusRepliesActivity.this.userStatusList.get(i)).getImgUrl() + ((MCLibUserStatus) MCLibStatusRepliesActivity.this.userStatusList.get(i)).getPhotoPath();
                if (imgUrlMain != null && !imgUrlMain.equals("")) {
                    this.currentUrls.add(imgUrlMain);
                }
                if (!(((MCLibUserStatus) MCLibStatusRepliesActivity.this.userStatusList.get(i)).getReplyStatus() == null || (imgUrlReply = ((MCLibUserStatus) MCLibStatusRepliesActivity.this.userStatusList.get(i)).getReplyStatus().getImgUrl() + ((MCLibUserStatus) MCLibStatusRepliesActivity.this.userStatusList.get(i)).getReplyStatus().getPhotoPath()) == null || imgUrlReply.equals(""))) {
                    this.currentUrls.add(imgUrlReply);
                }
            }
        }

        private List<String> getPreviousUrls() {
            String imgUrlReply;
            if (this.firstVisibleItem - 10 <= 0) {
                return null;
            }
            List<String> urls = new ArrayList<>();
            for (int i = 0; i < this.firstVisibleItem - 10; i++) {
                if (((MCLibUserStatus) MCLibStatusRepliesActivity.this.userStatusList.get(i)).getUserImageUrl() != null && !((MCLibUserStatus) MCLibStatusRepliesActivity.this.userStatusList.get(i)).getUserImageUrl().equals("")) {
                    if (!this.currentUrls.contains(((MCLibUserStatus) MCLibStatusRepliesActivity.this.userStatusList.get(i)).getUserImageUrl())) {
                        urls.add(((MCLibUserStatus) MCLibStatusRepliesActivity.this.userStatusList.get(i)).getUserImageUrl());
                    }
                    String imgUrlMain = ((MCLibUserStatus) MCLibStatusRepliesActivity.this.userStatusList.get(i)).getImgUrl() + ((MCLibUserStatus) MCLibStatusRepliesActivity.this.userStatusList.get(i)).getPhotoPath();
                    if (imgUrlMain != null && !imgUrlMain.equals("") && !this.currentUrls.contains(imgUrlMain)) {
                        urls.add(imgUrlMain);
                    }
                    if (((MCLibUserStatus) MCLibStatusRepliesActivity.this.userStatusList.get(i)).getReplyStatus() != null && (imgUrlReply = ((MCLibUserStatus) MCLibStatusRepliesActivity.this.userStatusList.get(i)).getReplyStatus().getImgUrl() + ((MCLibUserStatus) MCLibStatusRepliesActivity.this.userStatusList.get(i)).getReplyStatus().getPhotoPath()) != null && !imgUrlReply.equals("") && !this.currentUrls.contains(imgUrlReply)) {
                        urls.add(imgUrlReply);
                    }
                }
            }
            return urls;
        }

        private List<String> getNextImageUrls() {
            String imgUrlReply;
            int currentVisibleBottom = this.firstVisibleItem + this.visibleItemCount;
            if (this.totalItemCount - currentVisibleBottom <= 10) {
                return null;
            }
            List<String> urls = new ArrayList<>();
            for (int i = currentVisibleBottom + 10; i < this.totalItemCount; i++) {
                if (((MCLibUserStatus) MCLibStatusRepliesActivity.this.userStatusList.get(i)).getUserImageUrl() != null && !((MCLibUserStatus) MCLibStatusRepliesActivity.this.userStatusList.get(i)).getUserImageUrl().equals("")) {
                    if (!this.currentUrls.contains(((MCLibUserStatus) MCLibStatusRepliesActivity.this.userStatusList.get(i)).getUserImageUrl())) {
                        urls.add(((MCLibUserStatus) MCLibStatusRepliesActivity.this.userStatusList.get(i)).getUserImageUrl());
                    }
                    String imgUrlMain = ((MCLibUserStatus) MCLibStatusRepliesActivity.this.userStatusList.get(i)).getImgUrl() + ((MCLibUserStatus) MCLibStatusRepliesActivity.this.userStatusList.get(i)).getPhotoPath();
                    if (imgUrlMain != null && !imgUrlMain.equals("") && !this.currentUrls.contains(imgUrlMain)) {
                        urls.add(imgUrlMain);
                    }
                    if (((MCLibUserStatus) MCLibStatusRepliesActivity.this.userStatusList.get(i)).getReplyStatus() != null && (imgUrlReply = ((MCLibUserStatus) MCLibStatusRepliesActivity.this.userStatusList.get(i)).getReplyStatus().getImgUrl() + ((MCLibUserStatus) MCLibStatusRepliesActivity.this.userStatusList.get(i)).getReplyStatus().getPhotoPath()) != null && !imgUrlReply.equals("") && !this.currentUrls.contains(imgUrlReply)) {
                        urls.add(imgUrlReply);
                    }
                }
            }
            return urls;
        }
    };
    /* access modifiers changed from: private */
    public long statusRootId;
    private String type;
    /* access modifiers changed from: private */
    public MCLibUpdateListDelegate udateStatusDelegate = new MCLibUpdateListDelegate() {
        public void updateList(final int page, int pageSize, boolean isReadLocally) {
            MCLibStatusRepliesActivity.this.mHandler.sendEmptyMessage(4);
            MCLibStatusRepliesActivity.this.mHandler.sendMessage(MCLibStatusRepliesActivity.this.mHandler.obtainMessage(5, new ArrayList()));
            new Thread() {
                public void run() {
                    try {
                        List<MCLibUserStatus> statusList = MCLibStatusRepliesActivity.this.getUserStatuses(page, 10, false);
                        MCLibStatusRepliesActivity.this.mHandler.sendEmptyMessage(3);
                        if (statusList == null || statusList.size() <= 0) {
                            MCLibStatusRepliesActivity.this.showEmptyInfoTip(R.string.mc_lib_no_such_data);
                        } else {
                            MCLibStatusRepliesActivity.this.updateBundleListView(statusList);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }.start();
        }
    };
    /* access modifiers changed from: private */
    public List<MCLibUserStatus> userStatusList;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.mc_lib_status_replies_bundle);
        initWindowTitle();
        initSysMsgWidgets();
        this.isCommentList = getIntent().getBooleanExtra(MCLibMobCentApiConstant.IS_COMMENT_LIST, false);
        if (this.isCommentList) {
            this.objectId = getIntent().getStringExtra(MCLibMobCentApiConstant.OBJECT_ID);
            this.type = getIntent().getStringExtra("type");
        }
        this.statusRootId = getIntent().getLongExtra("id", 0);
        initWidgets();
        initNavBottomBar(100);
    }

    private void initWidgets() {
        this.bundleListView = (ListView) findViewById(R.id.mcLibBundledListView);
        this.replyTopicButton = (Button) findViewById(R.id.mcLibReplyTopicBtn);
        this.backButton = (Button) findViewById(R.id.mcLibBackBtn);
        this.refreshReplyHistoryBtn = (ImageButton) findViewById(R.id.mcLibRefreshReplyHistoryBtn);
        this.previewImageBox = (RelativeLayout) findViewById(R.id.mcLibPreviewImageBox);
        this.previewPrgBar = (ProgressBar) findViewById(R.id.mcLibPreviewProgressBar);
        this.previewImage = (ImageView) findViewById(R.id.mcLibPreviewImage);
        this.replyHistoryTitle = (TextView) findViewById(R.id.mcLibReplyHistoryTitle);
        this.previewImageBox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCLibStatusRepliesActivity.this.hidePreviewImage();
            }
        });
        if (this.isCommentList) {
            this.replyHistoryTitle.setText((int) R.string.mc_lib_status_comment_thread);
        }
        initProgressBox();
        initBundleListView();
        initReplyMainTopicButton();
        initBackButton();
        initRefreshReplyHistoryButton();
        this.udateStatusDelegate.updateList(1, 10, false);
    }

    private void initRefreshReplyHistoryButton() {
        this.refreshReplyHistoryBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCLibStatusRepliesActivity.this.udateStatusDelegate.updateList(1, 10, false);
            }
        });
    }

    private void initBackButton() {
        this.backButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCLibStatusRepliesActivity.this.finish();
            }
        });
    }

    private void initReplyMainTopicButton() {
        this.replyTopicButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (MCLibStatusRepliesActivity.this.userStatusList != null && !MCLibStatusRepliesActivity.this.userStatusList.isEmpty()) {
                    Intent intent = new Intent(MCLibStatusRepliesActivity.this, MCLibPublishStatusActivity.class);
                    intent.putExtra(MCLibParameterKeyConstant.STATUS_ROOT_ID, MCLibStatusRepliesActivity.this.statusRootId);
                    intent.putExtra(MCLibParameterKeyConstant.STATUS_REPLY_ID, MCLibStatusRepliesActivity.this.statusRootId);
                    MCLibStatusRepliesActivity.this.startActivity(intent);
                }
            }
        });
    }

    private void initBundleListView() {
        this.bundleListView.setOnItemClickListener(new MCLibListViewItemOnClickListener());
        this.bundleListView.setOnScrollListener(this.statusListOnScrollListener);
    }

    /* access modifiers changed from: private */
    public void updateBundleListView(List<MCLibUserStatus> list) {
        if (this.adapter == null) {
            this.mHandler.sendMessage(this.mHandler.obtainMessage(1, list));
        } else {
            this.mHandler.sendMessage(this.mHandler.obtainMessage(2, list));
        }
        this.mHandler.post(new Runnable() {
            public void run() {
                if (MCLibStatusRepliesActivity.this.refreshReplyHistoryBtn.getVisibility() == 4) {
                    MCLibStatusRepliesActivity.this.refreshReplyHistoryBtn.setVisibility(0);
                }
            }
        });
    }

    public void updateList(final int page, final int pageSize, final boolean isReadLocally) {
        new Thread() {
            public void run() {
                List<MCLibUserStatus> nextPageUserStatusList;
                List<MCLibUserStatus> resultList = new ArrayList<>();
                if (page == 1) {
                    MCLibStatusRepliesActivity.this.mHandler.sendEmptyMessage(4);
                    MCLibStatusRepliesActivity.this.updateBundleListView(resultList);
                    if (MCLibStatusRepliesActivity.this.userStatusList != null) {
                        MCLibStatusRepliesActivity.this.userStatusList.clear();
                    }
                }
                if (MCLibStatusRepliesActivity.this.userStatusList == null || MCLibStatusRepliesActivity.this.userStatusList.isEmpty()) {
                    nextPageUserStatusList = MCLibStatusRepliesActivity.this.getUserStatuses(page, pageSize, false);
                } else {
                    nextPageUserStatusList = MCLibStatusRepliesActivity.this.getUserStatuses(page, pageSize, isReadLocally);
                }
                if (nextPageUserStatusList != null && nextPageUserStatusList.size() > 0 && page == 1) {
                    MCLibStatusListAdapter unused = MCLibStatusRepliesActivity.this.adapter = null;
                    resultList = nextPageUserStatusList;
                } else if (nextPageUserStatusList != null && nextPageUserStatusList.size() > 0) {
                    resultList.addAll(MCLibStatusRepliesActivity.this.userStatusList);
                    resultList.remove(resultList.size() - 1);
                    resultList.addAll(nextPageUserStatusList);
                } else if (MCLibStatusRepliesActivity.this.userStatusList.size() > 0) {
                    resultList.addAll(MCLibStatusRepliesActivity.this.userStatusList);
                    resultList.remove(resultList.size() - 1);
                }
                MCLibStatusRepliesActivity.this.mHandler.sendEmptyMessage(3);
                MCLibStatusRepliesActivity.this.updateBundleListView(resultList);
            }
        }.start();
    }

    public List<MCLibUserStatus> getUserStatuses(int page, int pageSize, boolean isReadLocally) {
        MCLibStatusService statusService = new MCLibStatusServiceImpl(this);
        int loginUserId = new MCLibUserInfoServiceImpl(this).getLoginUserId();
        if (!this.isCommentList) {
            return statusService.getStatusThread(loginUserId, this.statusRootId, page, pageSize);
        }
        List<MCLibUserStatus> statusList = statusService.getStatusThreadAsComment(loginUserId, this.objectId, this.type, page, pageSize);
        if (page == 1 && !statusList.isEmpty()) {
            this.statusRootId = statusList.get(0).getStatusId();
        }
        return statusList;
    }

    /* access modifiers changed from: private */
    public void updateEmptyAdapter(Object obj) {
        this.adapter = new MCLibStatusListAdapter(this, this.bundleListView, R.layout.mc_lib_status_list_row, (List) obj, this, true, this.isCommentList, this.mHandler);
        this.bundleListView.setAdapter((ListAdapter) this.adapter);
        this.userStatusList = (List) obj;
    }

    /* access modifiers changed from: private */
    public void updateExistAdapter(Object obj) {
        this.adapter.setStatusList((List) obj);
        this.adapter.notifyDataSetInvalidated();
        this.adapter.notifyDataSetChanged();
        this.userStatusList = (List) obj;
    }

    public void showPreviewImage(final String imagePath) {
        this.mHandler.post(new Runnable() {
            public void run() {
                MCLibStatusRepliesActivity.this.previewImage.setVisibility(0);
                MCLibStatusRepliesActivity.this.previewImageBox.setVisibility(0);
                MCLibStatusRepliesActivity.this.previewPrgBar.setVisibility(0);
                Animation anim = AnimationUtils.loadAnimation(MCLibStatusRepliesActivity.this, R.anim.mc_lib_grow_from_middle);
                anim.setInterpolator(new AccelerateInterpolator());
                MCLibStatusRepliesActivity.this.previewImage.startAnimation(anim);
                MCLibStatusRepliesActivity.this.adapter.updateImage(0, imagePath, MCLibConstants.RESOLUTION_320X480, MCLibStatusRepliesActivity.this.previewImage, MCLibStatusRepliesActivity.this.previewPrgBar, true, true);
            }
        });
    }

    public void hidePreviewImage() {
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.mc_lib_shrink_to_middle);
        anim.setInterpolator(new AccelerateInterpolator());
        anim.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
                MCLibStatusRepliesActivity.this.mHandler.post(new Runnable() {
                    public void run() {
                        MCLibStatusRepliesActivity.this.previewImage.setVisibility(4);
                        MCLibStatusRepliesActivity.this.previewImageBox.setVisibility(4);
                        MCLibStatusRepliesActivity.this.previewPrgBar.setVisibility(4);
                        MCLibStatusRepliesActivity.this.previewImage.setBackgroundResource(R.drawable.mc_lib_unloaded_img);
                    }
                });
            }
        });
        this.previewImage.startAnimation(anim);
    }

    public Handler getCurrentHandler() {
        return this.mHandler;
    }
}
