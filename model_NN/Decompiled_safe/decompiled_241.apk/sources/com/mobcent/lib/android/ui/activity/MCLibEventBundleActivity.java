package com.mobcent.lib.android.ui.activity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.AbsListView;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.mobcent.android.model.MCLibUserStatus;
import com.mobcent.android.seed.autovGhnzo98NIzLsRPLHS.R;
import com.mobcent.android.service.MCLibStatusService;
import com.mobcent.android.service.impl.MCLibStatusServiceImpl;
import com.mobcent.android.service.impl.MCLibUserInfoServiceImpl;
import com.mobcent.lib.android.ui.activity.adapter.MCLibStatusListAdapter;
import com.mobcent.lib.android.ui.activity.listener.MCLibListViewItemOnClickListener;
import com.mobcent.lib.android.ui.activity.menu.MCLibEventMainBar;
import com.mobcent.lib.android.ui.delegate.MCLibUpdateListDelegate;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class MCLibEventBundleActivity extends MCLibUIBaseActivity implements MCLibUpdateListDelegate {
    public static final int EVENTS_FRIEND = 2;
    public static final int EVENTS_HALL = 1;
    /* access modifiers changed from: private */
    public MCLibStatusListAdapter adapter;
    private ListView bundleListView;
    /* access modifiers changed from: private */
    public Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            if (msg.what == 1 || msg.what == 5) {
                MCLibEventBundleActivity.this.updateEmptyAdapter(msg.obj);
            } else if (msg.what == 2) {
                MCLibEventBundleActivity.this.updateExistAdapter(msg.obj);
            } else if (msg.what == 3) {
                MCLibEventBundleActivity.this.hideProgressBar();
            } else if (msg.what == 4) {
                MCLibEventBundleActivity.this.showProgressBar();
            }
        }
    };
    private MCLibUpdateListDelegate menuUpdateStatusDelegate = new MCLibUpdateListDelegate() {
        public void updateList(final int page, int pageSize, final boolean isReadLocally) {
            MCLibEventBundleActivity.this.mHandler.sendMessage(MCLibEventBundleActivity.this.mHandler.obtainMessage(5, new ArrayList()));
            MCLibEventBundleActivity.this.mHandler.sendEmptyMessage(4);
            new Thread() {
                public void run() {
                    try {
                        List<MCLibUserStatus> statusList = MCLibEventBundleActivity.this.getUserStatuses(MCLibEventBundleActivity.this.statusType.intValue(), page, 10, isReadLocally);
                        MCLibEventBundleActivity.this.mHandler.sendEmptyMessage(3);
                        if (statusList == null || statusList.size() <= 0) {
                            MCLibEventBundleActivity.this.showEmptyInfoTip(R.string.mc_lib_no_such_data);
                        } else {
                            MCLibEventBundleActivity.this.updateBundledListView(statusList);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }.start();
        }
    };
    protected AbsListView.OnScrollListener statusListOnScrollListener = new AbsListView.OnScrollListener() {
        private Vector<String> currentUrls;
        private int firstVisibleItem;
        private int totalItemCount;
        private int visibleItemCount;

        public void onScrollStateChanged(AbsListView view, int scrollState) {
            if (scrollState == 0) {
                List<String> imageUrls = new ArrayList<>();
                parseCurrentUrls();
                List<String> preUrls = getPreviousUrls();
                List<String> nextUrls = getNextImageUrls();
                if (preUrls != null) {
                    imageUrls.addAll(preUrls);
                }
                if (nextUrls != null) {
                    imageUrls.addAll(nextUrls);
                }
                MCLibEventBundleActivity.this.adapter.asyncImageLoader.recycleBitmap(imageUrls);
            }
        }

        public void onScroll(AbsListView view, int firstVisibleItem2, int visibleItemCount2, int totalItemCount2) {
            this.firstVisibleItem = firstVisibleItem2;
            this.visibleItemCount = visibleItemCount2;
            this.totalItemCount = totalItemCount2;
        }

        private void parseCurrentUrls() {
            this.currentUrls = new Vector<>();
            int firstIndex = 0;
            int endIndex = this.firstVisibleItem + this.visibleItemCount + 10;
            if (this.firstVisibleItem - 10 > 0) {
                firstIndex = this.firstVisibleItem - 10;
            }
            if (endIndex >= this.totalItemCount) {
                endIndex = this.totalItemCount;
            }
            for (int i = firstIndex; i < endIndex; i++) {
                if (((MCLibUserStatus) MCLibEventBundleActivity.this.userStatusList.get(i)).getUserImageUrl() != null && !((MCLibUserStatus) MCLibEventBundleActivity.this.userStatusList.get(i)).getUserImageUrl().equals("")) {
                    this.currentUrls.add(((MCLibUserStatus) MCLibEventBundleActivity.this.userStatusList.get(i)).getUserImageUrl());
                }
            }
        }

        private List<String> getPreviousUrls() {
            if (this.firstVisibleItem - 10 <= 0) {
                return null;
            }
            List<String> urls = new ArrayList<>();
            for (int i = 0; i < this.firstVisibleItem - 10; i++) {
                if (((MCLibUserStatus) MCLibEventBundleActivity.this.userStatusList.get(i)).getUserImageUrl() != null && !((MCLibUserStatus) MCLibEventBundleActivity.this.userStatusList.get(i)).getUserImageUrl().equals("") && !this.currentUrls.contains(((MCLibUserStatus) MCLibEventBundleActivity.this.userStatusList.get(i)).getUserImageUrl())) {
                    urls.add(((MCLibUserStatus) MCLibEventBundleActivity.this.userStatusList.get(i)).getUserImageUrl());
                }
            }
            return urls;
        }

        private List<String> getNextImageUrls() {
            int currentVisibleBottom = this.firstVisibleItem + this.visibleItemCount;
            if (this.totalItemCount - currentVisibleBottom <= 10) {
                return null;
            }
            List<String> urls = new ArrayList<>();
            for (int i = currentVisibleBottom + 10; i < this.totalItemCount; i++) {
                if (((MCLibUserStatus) MCLibEventBundleActivity.this.userStatusList.get(i)).getUserImageUrl() != null && !((MCLibUserStatus) MCLibEventBundleActivity.this.userStatusList.get(i)).getUserImageUrl().equals("") && !this.currentUrls.contains(((MCLibUserStatus) MCLibEventBundleActivity.this.userStatusList.get(i)).getUserImageUrl())) {
                    urls.add(((MCLibUserStatus) MCLibEventBundleActivity.this.userStatusList.get(i)).getUserImageUrl());
                }
            }
            return urls;
        }
    };
    /* access modifiers changed from: private */
    public Integer statusType = 2;
    /* access modifiers changed from: private */
    public List<MCLibUserStatus> userStatusList;

    /* access modifiers changed from: private */
    public void updateEmptyAdapter(Object obj) {
        this.adapter = new MCLibStatusListAdapter(this, this.bundleListView, R.layout.mc_lib_status_list_row, (List) obj, this, false, false, this.mHandler);
        this.bundleListView.setAdapter((ListAdapter) this.adapter);
        this.userStatusList = (List) obj;
    }

    /* access modifiers changed from: private */
    public void updateExistAdapter(Object obj) {
        this.adapter.setStatusList((List) obj);
        this.adapter.notifyDataSetInvalidated();
        this.adapter.notifyDataSetChanged();
        this.userStatusList = (List) obj;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.mc_lib_events_bundle);
        initWindowTitle();
        initSysMsgWidgets();
        initWidgets();
        initEventMenu();
        initLoginUserHeader();
        initNavBottomBar(101);
    }

    private void initWidgets() {
        this.bundleListView = (ListView) findViewById(R.id.mcLibBundledListView);
        initProgressBox();
        initBundleListView();
    }

    private void initBundleListView() {
        this.bundleListView.setOnItemClickListener(new MCLibListViewItemOnClickListener());
        this.bundleListView.setOnScrollListener(this.statusListOnScrollListener);
    }

    private void initEventMenu() {
        new MCLibEventMainBar().buildActionBar(this, this.menuUpdateStatusDelegate, this.statusType);
    }

    /* access modifiers changed from: private */
    public void updateBundledListView(List<MCLibUserStatus> list) {
        if (this.adapter == null) {
            this.mHandler.sendMessage(this.mHandler.obtainMessage(1, list));
            return;
        }
        this.mHandler.sendMessage(this.mHandler.obtainMessage(2, list));
    }

    public void updateList(final int page, final int pageSize, final boolean isReadLocally) {
        new Thread() {
            public void run() {
                List<MCLibUserStatus> nextPageUserStatusList;
                List<MCLibUserStatus> resultList = new ArrayList<>();
                if (page == 1) {
                    MCLibEventBundleActivity.this.mHandler.sendEmptyMessage(4);
                    MCLibEventBundleActivity.this.updateBundledListView(resultList);
                    if (MCLibEventBundleActivity.this.userStatusList != null) {
                        MCLibEventBundleActivity.this.userStatusList.clear();
                    }
                }
                if (MCLibEventBundleActivity.this.userStatusList == null || MCLibEventBundleActivity.this.userStatusList.isEmpty()) {
                    nextPageUserStatusList = MCLibEventBundleActivity.this.getUserStatuses(MCLibEventBundleActivity.this.statusType.intValue(), page, pageSize, false);
                } else {
                    nextPageUserStatusList = MCLibEventBundleActivity.this.getUserStatuses(MCLibEventBundleActivity.this.statusType.intValue(), page, pageSize, isReadLocally);
                }
                if (nextPageUserStatusList != null && nextPageUserStatusList.size() > 0 && page == 1) {
                    MCLibStatusListAdapter unused = MCLibEventBundleActivity.this.adapter = null;
                    resultList = nextPageUserStatusList;
                } else if (nextPageUserStatusList != null && nextPageUserStatusList.size() > 0) {
                    resultList.addAll(MCLibEventBundleActivity.this.userStatusList);
                    resultList.remove(resultList.size() - 1);
                    resultList.addAll(nextPageUserStatusList);
                } else if (MCLibEventBundleActivity.this.userStatusList.size() > 0) {
                    resultList.addAll(MCLibEventBundleActivity.this.userStatusList);
                    resultList.remove(resultList.size() - 1);
                }
                MCLibEventBundleActivity.this.mHandler.sendEmptyMessage(3);
                MCLibEventBundleActivity.this.updateBundledListView(resultList);
            }
        }.start();
    }

    public List<MCLibUserStatus> getUserStatuses(int type, int page, int pageSize, boolean isReadLocally) {
        MCLibStatusService statusService = new MCLibStatusServiceImpl(this);
        int uid = new MCLibUserInfoServiceImpl(this).getLoginUserId();
        if (type == 1) {
            return statusService.getHallEvents(uid, page, pageSize, isReadLocally);
        }
        if (type == 2) {
            return statusService.getFriendEvents(uid, page, pageSize, isReadLocally);
        }
        return statusService.getFriendEvents(uid, page, pageSize, isReadLocally);
    }

    public Integer getStatusType() {
        return this.statusType;
    }

    public void setStatusType(Integer statusType2) {
        this.statusType = statusType2;
    }

    public Handler getCurrentHandler() {
        return this.mHandler;
    }
}
