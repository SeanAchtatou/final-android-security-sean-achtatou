package com.mobcent.lib.android.ui.activity;

import android.app.Activity;
import android.content.IntentFilter;
import android.os.Bundle;
import com.mobcent.android.os.service.MCLibUserPublishQueueService;
import com.mobcent.android.os.service.receiver.MCLibPublishCallBackReceiver;
import com.mobcent.android.state.MCLibAppState;
import com.mobcent.lib.android.ui.activity.receiver.MCLibMsgNotificationReceiver;
import com.mobcent.lib.android.ui.delegate.impl.MCLibMsgBundleDelegateImpl;

public abstract class MCLibAbstractBaseActivity extends Activity {
    protected MCLibMsgNotificationReceiver msgNotificationReceiver;
    protected MCLibPublishCallBackReceiver publishCallBackReceiver;

    /* access modifiers changed from: protected */
    public abstract void initMsgNotificationReceiver();

    /* access modifiers changed from: protected */
    public void onResume() {
        MCLibAppState.isActive = true;
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        MCLibAppState.isActive = false;
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        initPublishReceiver();
        initMsgNotificationReceiver();
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addActivityToStack();
    }

    /* access modifiers changed from: protected */
    public void addActivityToStack() {
        MCLibAppState.addActivityToStack(this);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        removeActivityFromStack();
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void removeActivityFromStack() {
        MCLibAppState.removeActivityFromStack(this);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        if (this.publishCallBackReceiver != null) {
            unregisterReceiver(this.publishCallBackReceiver);
        }
        if (this.msgNotificationReceiver != null) {
            unregisterReceiver(this.msgNotificationReceiver);
        }
    }

    private void initPublishReceiver() {
        if (this.publishCallBackReceiver == null) {
            this.publishCallBackReceiver = new MCLibPublishCallBackReceiver(new MCLibMsgBundleDelegateImpl(this));
        }
        IntentFilter filter = new IntentFilter();
        filter.addAction(MCLibUserPublishQueueService.PUBLISH_SERVICE_MSG);
        registerReceiver(this.publishCallBackReceiver, filter);
    }
}
