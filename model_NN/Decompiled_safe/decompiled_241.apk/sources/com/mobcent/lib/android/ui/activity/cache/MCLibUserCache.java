package com.mobcent.lib.android.ui.activity.cache;

import com.mobcent.android.model.MCLibUserInfo;
import java.util.ArrayList;
import java.util.List;

public class MCLibUserCache {
    public static String lastRefreshFansListTime = "";
    public static String lastRefreshFriendsListTime = "";
    public static String lastRefreshRecommendedUserListTime = "";
    public static List<MCLibUserInfo> myFansList = new ArrayList();
    public static List<MCLibUserInfo> myFriendsList = new ArrayList();
    public static List<MCLibUserInfo> recommendedUserList = new ArrayList();

    public static void addFans(List<MCLibUserInfo> myFans) {
        for (MCLibUserInfo userInfo : myFans) {
            if (!userInfo.isFetchMore() && !userInfo.isRefreshNeeded() && !isUserInFansList(userInfo.getUid())) {
                myFansList.add(userInfo);
            }
        }
    }

    private static boolean isUserInFansList(int uid) {
        for (MCLibUserInfo userInfo : myFansList) {
            if (userInfo.getUid() == uid) {
                return true;
            }
        }
        return false;
    }

    public static void addRecommendedUsers(List<MCLibUserInfo> recommendedUsers) {
        for (MCLibUserInfo userInfo : recommendedUsers) {
            if (!userInfo.isFetchMore() && !userInfo.isRefreshNeeded() && !isUserInRecommendedUserList(userInfo.getUid())) {
                recommendedUserList.add(userInfo);
            }
        }
    }

    private static boolean isUserInRecommendedUserList(int uid) {
        for (MCLibUserInfo userInfo : recommendedUserList) {
            if (userInfo.getUid() == uid) {
                return true;
            }
        }
        return false;
    }

    public static void addMyFriends(List<MCLibUserInfo> myFriends) {
        for (MCLibUserInfo userInfo : myFriends) {
            if (!userInfo.isFetchMore() && !userInfo.isRefreshNeeded() && !isUserInMyFriendsList(userInfo.getUid())) {
                myFriendsList.add(userInfo);
            }
        }
    }

    private static boolean isUserInMyFriendsList(int uid) {
        for (MCLibUserInfo userInfo : myFriendsList) {
            if (userInfo.getUid() == uid) {
                return true;
            }
        }
        return false;
    }
}
