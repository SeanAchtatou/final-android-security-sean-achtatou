package com.mobcent.lib.android.utils.i18n;

import android.content.Context;

public class MCLibStringBundleUtil {
    public static String resolveString(int stringSource, String[] args, Context context) {
        String string = context.getResources().getString(stringSource);
        for (int i = 0; i < args.length; i++) {
            string = string.replace("{" + i + "}", args[i]);
        }
        return string;
    }
}
