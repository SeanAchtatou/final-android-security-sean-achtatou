package com.mobcent.lib.android.utils;

import android.content.Context;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.ImageSpan;
import com.mobcent.android.seed.autovGhnzo98NIzLsRPLHS.R;
import com.mobcent.lib.android.constants.MCLibEmotionsConstant;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class MCLibTextViewUtil {
    public static CharSequence transformCharSequenceWithEmotions(CharSequence text, Context mContext) {
        SpannableStringBuilder builder = new SpannableStringBuilder(text);
        LinkedHashMap<String, Integer> emotionMap = MCLibEmotionsConstant.geEmotionConstant().getAllEmotionsMap();
        for (Map.Entry<String, Integer> entity : emotionMap.entrySet()) {
            replaceAllKeyWithEmotion((String) entity.getKey(), builder, emotionMap, mContext);
        }
        return builder;
    }

    private static CharSequence replaceAllKeyWithEmotion(String key, SpannableStringBuilder builder, HashMap<String, Integer> emotionMap, Context mContext) {
        String content = new String(builder.toString());
        boolean isFinishKeyMatching = false;
        int startPos = 0;
        while (!isFinishKeyMatching) {
            if (startPos > content.length()) {
                isFinishKeyMatching = true;
            }
            int index = content.indexOf(key, startPos);
            if (index > -1) {
                builder.setSpan(new ImageSpan(mContext, emotionMap.get(key).intValue()), index, key.length() + index, 33);
                startPos = key.length() + index + 1;
            } else {
                isFinishKeyMatching = true;
            }
        }
        return builder;
    }

    public static CharSequence highlightTag(CharSequence text, String tag, Context mContext) {
        if (tag == null || "".equals(tag)) {
            return text;
        }
        SpannableStringBuilder buf = new SpannableStringBuilder(text);
        int startOffset = text.toString().indexOf(tag);
        buf.setSpan(new ForegroundColorSpan(mContext.getResources().getColor(R.color.mc_lib_red)), startOffset, tag.length() + startOffset, 33);
        return buf;
    }
}
