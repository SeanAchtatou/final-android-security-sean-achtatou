package com.mobcent.lib.android.ui.activity.view.util;

import android.app.Activity;
import android.os.Handler;
import android.widget.RelativeLayout;
import com.mobcent.android.constants.MCLibConstants;
import com.mobcent.android.model.MCLibActionModel;
import com.mobcent.android.service.impl.MCLibUserActionServiceImpl;
import com.mobcent.lib.android.ui.activity.view.MCLibGifView;
import com.mobcent.lib.android.ui.delegate.MCLibUserActionDelegate;
import com.mobcent.lib.android.utils.MCLibImageLoader;
import com.mobcent.lib.android.utils.MCLibImageUtil;
import com.mobcent.lib.android.utils.MCLibPhoneVibratorUtil;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class MCLibDisplayGifUtil {
    /* access modifiers changed from: private */
    public Activity activity;
    /* access modifiers changed from: private */
    public RelativeLayout gifPrgBarBox;
    /* access modifiers changed from: private */
    public MCLibGifView gifView;
    private boolean isVibrator;
    /* access modifiers changed from: private */
    public Handler mHandler;

    public MCLibDisplayGifUtil(Activity activity2, MCLibGifView gifView2, RelativeLayout gifPrgBarBox2, boolean isVibrator2, Handler mHandler2) {
        this.activity = activity2;
        this.gifView = gifView2;
        this.gifPrgBarBox = gifPrgBarBox2;
        this.mHandler = mHandler2;
        this.isVibrator = isVibrator2;
    }

    public void showMobCentBouncePanel(MCLibActionModel actionModel, final MCLibUserActionDelegate delegate) {
        if (actionModel != null) {
            this.mHandler.post(new Runnable() {
                public void run() {
                    if (MCLibDisplayGifUtil.this.gifPrgBarBox != null) {
                        MCLibDisplayGifUtil.this.gifPrgBarBox.setVisibility(0);
                    }
                }
            });
            MCLibActionModel model = new MCLibUserActionServiceImpl(this.activity).getActionInDictById(this.activity, actionModel.getActionId());
            actionModel.setBaseUrl(model.getBaseUrl());
            actionModel.setIcon(model.getIcon());
            final String icon = actionModel.getIcon();
            if (icon != null) {
                final String imagePath = MCLibImageLoader.getImageCachePath(icon);
                final File file = new File(imagePath);
                if (imagePath == null || !file.exists()) {
                    final MCLibActionModel mCLibActionModel = actionModel;
                    final MCLibUserActionDelegate mCLibUserActionDelegate = delegate;
                    new Thread() {
                        public void run() {
                            if (MCLibImageUtil.downloadGifImage(mCLibActionModel.getBaseUrl() + icon, MCLibConstants.RESOLUTION_320X480, MCLibDisplayGifUtil.this.activity) && imagePath != null && file.exists()) {
                                MCLibDisplayGifUtil.this.mHandler.post(new Runnable() {
                                    public void run() {
                                        MCLibPhoneVibratorUtil.shotVibratePhone(MCLibDisplayGifUtil.this.activity);
                                        try {
                                            InputStream is = new FileInputStream(imagePath);
                                            MCLibDisplayGifUtil.this.gifView.setGifImageType(MCLibGifView.GifImageType.COVER);
                                            MCLibDisplayGifUtil.this.gifView.setGifImage(is);
                                            MCLibDisplayGifUtil.this.gifView.setShowDimension(300, 300);
                                            MCLibDisplayGifUtil.this.gifView.showCover();
                                            MCLibDisplayGifUtil.this.gifView.showAnimation();
                                        } catch (FileNotFoundException e) {
                                        }
                                        if (MCLibDisplayGifUtil.this.gifPrgBarBox != null) {
                                            MCLibDisplayGifUtil.this.gifPrgBarBox.setVisibility(8);
                                        }
                                        MCLibDisplayGifUtil.this.gifView.setVisibility(0);
                                        MCLibDisplayGifUtil.this.gifView.setUserActionDelegate(mCLibUserActionDelegate);
                                    }
                                });
                            }
                        }
                    }.start();
                    return;
                }
                if (this.isVibrator) {
                    MCLibPhoneVibratorUtil.shotVibratePhone(this.activity);
                }
                try {
                    InputStream is = new FileInputStream(imagePath);
                    this.gifView.setGifImageType(MCLibGifView.GifImageType.COVER);
                    this.gifView.setGifImage(is);
                    this.gifView.setShowDimension(300, 300);
                    this.gifView.showCover();
                    this.gifView.showAnimation();
                } catch (FileNotFoundException e) {
                }
                this.mHandler.post(new Runnable() {
                    public void run() {
                        if (MCLibDisplayGifUtil.this.gifPrgBarBox != null) {
                            MCLibDisplayGifUtil.this.gifPrgBarBox.setVisibility(8);
                        }
                        MCLibDisplayGifUtil.this.gifView.setVisibility(0);
                        MCLibDisplayGifUtil.this.gifView.setUserActionDelegate(delegate);
                    }
                });
            }
        }
    }
}
