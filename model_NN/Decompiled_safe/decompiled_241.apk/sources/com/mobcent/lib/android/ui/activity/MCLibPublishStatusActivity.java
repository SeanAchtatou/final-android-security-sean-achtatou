package com.mobcent.lib.android.ui.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.mobcent.android.constants.MCLibConstants;
import com.mobcent.android.constants.MCLibParameterKeyConstant;
import com.mobcent.android.model.MCLibUserStatus;
import com.mobcent.android.os.service.MCLibUserPublishQueueService;
import com.mobcent.android.seed.autovGhnzo98NIzLsRPLHS.R;
import com.mobcent.android.service.MCLibStatusService;
import com.mobcent.android.service.impl.MCLibStatusServiceImpl;
import com.mobcent.android.service.impl.MCLibUserInfoServiceImpl;
import com.mobcent.android.utils.MCLibIOUtil;
import com.mobcent.lib.android.constants.MCLibEmotionsConstant;
import com.mobcent.lib.android.ui.activity.adapter.MCLibEmotionGridViewAdapter;
import com.mobcent.lib.android.ui.delegate.MCLibQuickMsgPanelDelegate;
import com.mobcent.lib.android.utils.MCLibImageLoader;
import com.mobcent.lib.android.utils.MCLibImageUtil;
import com.mobcent.lib.android.utils.i18n.MCLibStringBundleUtil;
import com.mobcent.share.android.activity.MCShareBindSitesActivity;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MCLibPublishStatusActivity extends MCLibUIBaseActivity implements MCLibQuickMsgPanelDelegate {
    public static final String COMPRESSED_IMAGE_NAME = "_mob_compress_image.jpg";
    public static final String MOB_CAMERA_FILE_NAME = "_mob_camera_image_.jpg";
    public static final String MOB_SELECTED_FILE_NAME = "_mob_select_image_.jpg";
    /* access modifiers changed from: private */
    public int IMAGE_UPLOADED = 102;
    private int IMAGE_UPLOADING = 101;
    /* access modifiers changed from: private */
    public int NO_IMAGE_ATTACHED = 100;
    /* access modifiers changed from: private */
    public int SELECT_LOCAL_PHOTO = 2;
    /* access modifiers changed from: private */
    public int TAKE_PHOTO = 1;
    private Button backBtn;
    /* access modifiers changed from: private */
    public Button cancelBtn;
    /* access modifiers changed from: private */
    public String compressedImagePath = "";
    /* access modifiers changed from: private */
    public Button confirmBtn;
    /* access modifiers changed from: private */
    public EditText contentEditText;
    /* access modifiers changed from: private */
    public ImageView deleteImage;
    /* access modifiers changed from: private */
    public GridView emotionGridView;
    /* access modifiers changed from: private */
    public int imageType = this.TAKE_PHOTO;
    private Button launchCameraBtn;
    /* access modifiers changed from: private */
    public Handler mHandler = new Handler();
    /* access modifiers changed from: private */
    public RelativeLayout maskLayer;
    /* access modifiers changed from: private */
    public int photoId = 0;
    /* access modifiers changed from: private */
    public String photoPath = "";
    /* access modifiers changed from: private */
    public ImageView previewImage;
    /* access modifiers changed from: private */
    public ImageView previewSmallImage;
    private Button publishBtn;
    private TextView publishTitle;
    private Button quickRespBackBtn;
    private LinearLayout quickRespInnerBox;
    /* access modifiers changed from: private */
    public RelativeLayout quickRespLayer;
    private long rootId;
    private Button selectEmotionBtn;
    private Button selectLocalImageBtn;
    private Button syncBtn;
    /* access modifiers changed from: private */
    public long toStatusId;
    /* access modifiers changed from: private */
    public int uploadImageStatus = this.NO_IMAGE_ATTACHED;
    /* access modifiers changed from: private */
    public ProgressBar uploadingProgressBar;
    /* access modifiers changed from: private */
    public int upperLimit = MCLibConstants.MICROBLOG_WORDS_UPPER_LIMIT;
    /* access modifiers changed from: private */
    public TextView upperLimitText;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.mc_lib_publish_status);
        initWindowTitle();
        initSysMsgWidgets();
        initProgressBox();
        hideProgressBar();
        this.rootId = getIntent().getLongExtra(MCLibParameterKeyConstant.STATUS_ROOT_ID, -1);
        this.toStatusId = getIntent().getLongExtra(MCLibParameterKeyConstant.STATUS_REPLY_ID, 0);
        initWidgets();
    }

    private void initWidgets() {
        this.backBtn = (Button) findViewById(R.id.mcLibBackBtn);
        this.publishBtn = (Button) findViewById(R.id.mcLibPublishBtn);
        this.selectLocalImageBtn = (Button) findViewById(R.id.mcLibLocalImageBtn);
        this.selectEmotionBtn = (Button) findViewById(R.id.mcLibEmotionBtn);
        this.launchCameraBtn = (Button) findViewById(R.id.mcLibCameraBtn);
        this.syncBtn = (Button) findViewById(R.id.mcLibSyncBtn);
        this.confirmBtn = (Button) findViewById(R.id.mcLibConfirmBtn);
        this.cancelBtn = (Button) findViewById(R.id.mcLibCancelBtn);
        this.previewImage = (ImageView) findViewById(R.id.mcLibPreviewImage);
        this.previewSmallImage = (ImageView) findViewById(R.id.mcLibPreviewSmallImage);
        this.deleteImage = (ImageView) findViewById(R.id.mcLibDeleteImage);
        this.uploadingProgressBar = (ProgressBar) findViewById(R.id.mcLibUploadingProgressBar);
        this.maskLayer = (RelativeLayout) findViewById(R.id.mcLibMaskLayer);
        this.contentEditText = (EditText) findViewById(R.id.mcLibContentEditText);
        this.upperLimitText = (TextView) findViewById(R.id.mcLibWordsUpperLimit);
        this.publishTitle = (TextView) findViewById(R.id.mcLibPubTitle);
        this.quickRespBackBtn = (Button) findViewById(R.id.mcLibQuickRespBackBtn);
        this.quickRespLayer = (RelativeLayout) findViewById(R.id.mcLibQuickRespBox);
        this.quickRespInnerBox = (LinearLayout) findViewById(R.id.mcLibQuickRespInnerBox);
        this.emotionGridView = (GridView) findViewById(R.id.mcLibEmotionGridView);
        if (this.toStatusId > 0) {
            this.publishTitle.setText((int) R.string.mc_lib_title_reply);
        } else {
            this.publishTitle.setText((int) R.string.mc_lib_title_publish);
        }
        this.upperLimitText.setText(MCLibStringBundleUtil.resolveString(R.string.mc_lib_words_left, new String[]{this.upperLimit + ""}, this));
        this.maskLayer.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            }
        });
        this.previewImage.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            }
        });
        this.confirmBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCLibPublishStatusActivity.this.mHandler.post(new Runnable() {
                    public void run() {
                        List<View> views = new ArrayList<>();
                        views.add(MCLibPublishStatusActivity.this.maskLayer);
                        views.add(MCLibPublishStatusActivity.this.previewImage);
                        views.add(MCLibPublishStatusActivity.this.confirmBtn);
                        views.add(MCLibPublishStatusActivity.this.cancelBtn);
                        MCLibPublishStatusActivity.this.setViewVisibility(views, 8);
                        File cameraFile = new File(MCLibImageLoader.getImageCacheBasePath() + MCLibIOUtil.FS + MCLibPublishStatusActivity.MOB_CAMERA_FILE_NAME);
                        if (cameraFile.exists()) {
                            cameraFile.delete();
                        }
                        MCLibPublishStatusActivity.this.proceedUploadImage();
                    }
                });
            }
        });
        this.cancelBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                List<View> views = new ArrayList<>();
                views.add(MCLibPublishStatusActivity.this.maskLayer);
                views.add(MCLibPublishStatusActivity.this.previewImage);
                views.add(MCLibPublishStatusActivity.this.confirmBtn);
                views.add(MCLibPublishStatusActivity.this.cancelBtn);
                MCLibPublishStatusActivity.this.setViewVisibility(views, 8);
                int unused = MCLibPublishStatusActivity.this.uploadImageStatus = MCLibPublishStatusActivity.this.NO_IMAGE_ATTACHED;
                File cameraFile = new File(MCLibImageLoader.getImageCacheBasePath() + MCLibIOUtil.FS + MCLibPublishStatusActivity.MOB_CAMERA_FILE_NAME);
                if (cameraFile.exists()) {
                    cameraFile.delete();
                }
            }
        });
        this.backBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCLibPublishStatusActivity.this.cleanTempFiles();
                MCLibPublishStatusActivity.this.finish();
            }
        });
        this.publishBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String content = MCLibPublishStatusActivity.this.contentEditText.getText().toString();
                if (content == null || content.equals("")) {
                    MCLibPublishStatusActivity.this.contentEditText.setHint((int) R.string.mc_lib_say_something);
                    MCLibPublishStatusActivity.this.contentEditText.setHintTextColor((int) R.color.mc_lib_red);
                } else if (MCLibPublishStatusActivity.this.upperLimit - MCLibPublishStatusActivity.this.contentEditText.getText().length() < 0) {
                    MCLibPublishStatusActivity.this.upperLimitText.setText(MCLibStringBundleUtil.resolveString(R.string.mc_lib_publish_words_tip, new String[]{MCLibPublishStatusActivity.this.upperLimit + ""}, MCLibPublishStatusActivity.this));
                } else if (MCLibPublishStatusActivity.this.toStatusId > 0) {
                    MCLibPublishStatusActivity.this.replyStatus(content, MCLibPublishStatusActivity.this.photoId, MCLibPublishStatusActivity.this.photoPath);
                } else {
                    MCLibPublishStatusActivity.this.publishStatus(content, MCLibPublishStatusActivity.this.photoId, MCLibPublishStatusActivity.this.photoPath);
                }
            }
        });
        this.contentEditText.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void afterTextChanged(Editable s) {
                MCLibPublishStatusActivity.this.mHandler.post(new Runnable() {
                    public void run() {
                        int wordsLeft = MCLibPublishStatusActivity.this.upperLimit - MCLibPublishStatusActivity.this.contentEditText.getText().length();
                        if (wordsLeft < 0) {
                            MCLibPublishStatusActivity.this.upperLimitText.setTextColor(MCLibPublishStatusActivity.this.getResources().getColor(R.color.mc_lib_red));
                        } else {
                            MCLibPublishStatusActivity.this.upperLimitText.setTextColor(MCLibPublishStatusActivity.this.getResources().getColor(R.color.mc_lib_black));
                        }
                        MCLibPublishStatusActivity.this.upperLimitText.setText(MCLibStringBundleUtil.resolveString(R.string.mc_lib_words_left, new String[]{wordsLeft + ""}, MCLibPublishStatusActivity.this));
                    }
                });
            }
        });
        this.selectLocalImageBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int unused = MCLibPublishStatusActivity.this.imageType = MCLibPublishStatusActivity.this.SELECT_LOCAL_PHOTO;
                File file = new File(MCLibImageLoader.getImageCacheBasePath() + MCLibIOUtil.FS + MCLibPublishStatusActivity.MOB_SELECTED_FILE_NAME);
                if (!file.getParentFile().exists()) {
                    file.mkdirs();
                }
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction("android.intent.action.GET_CONTENT");
                MCLibPublishStatusActivity.this.startActivityForResult(intent, 1);
            }
        });
        this.launchCameraBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int unused = MCLibPublishStatusActivity.this.imageType = MCLibPublishStatusActivity.this.TAKE_PHOTO;
                File file = new File(MCLibImageLoader.getImageCacheBasePath() + MCLibIOUtil.FS + MCLibPublishStatusActivity.MOB_CAMERA_FILE_NAME);
                if (!file.getParentFile().exists()) {
                    file.mkdirs();
                }
                Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                intent.putExtra("output", Uri.fromFile(file));
                MCLibPublishStatusActivity.this.startActivityForResult(intent, 1);
            }
        });
        this.selectEmotionBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCLibPublishStatusActivity.this.openQuickRespPanel();
            }
        });
        this.syncBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(MCLibPublishStatusActivity.this, MCShareBindSitesActivity.class);
                intent.putExtra("uid", new MCLibUserInfoServiceImpl(MCLibPublishStatusActivity.this).getLoginUserId());
                intent.putExtra("appKey", MCLibPublishStatusActivity.this.getResources().getString(R.string.mc_lib_bp_app_key));
                MCLibPublishStatusActivity.this.startActivity(intent);
            }
        });
        this.deleteImage.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int unused = MCLibPublishStatusActivity.this.uploadImageStatus = MCLibPublishStatusActivity.this.NO_IMAGE_ATTACHED;
                int unused2 = MCLibPublishStatusActivity.this.photoId = 0;
                String unused3 = MCLibPublishStatusActivity.this.photoPath = "";
                List<View> views = new ArrayList<>();
                views.add(MCLibPublishStatusActivity.this.previewSmallImage);
                views.add(MCLibPublishStatusActivity.this.uploadingProgressBar);
                views.add(MCLibPublishStatusActivity.this.deleteImage);
                MCLibPublishStatusActivity.this.setViewVisibility(views, 8);
            }
        });
        this.quickRespBackBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCLibPublishStatusActivity.this.closeQuickRespPanel();
            }
        });
        this.quickRespLayer.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCLibPublishStatusActivity.this.closeQuickRespPanel();
            }
        });
        this.quickRespInnerBox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            }
        });
    }

    /* access modifiers changed from: private */
    public void openQuickRespPanel() {
        this.mHandler.post(new Runnable() {
            public void run() {
                ((InputMethodManager) MCLibPublishStatusActivity.this.getSystemService("input_method")).hideSoftInputFromWindow(MCLibPublishStatusActivity.this.contentEditText.getWindowToken(), 0);
                MCLibPublishStatusActivity.this.quickRespLayer.setVisibility(0);
                Animation animation = AnimationUtils.loadAnimation(MCLibPublishStatusActivity.this, R.anim.mc_lib_grow_from_middle);
                animation.setDuration(200);
                MCLibPublishStatusActivity.this.quickRespLayer.startAnimation(animation);
                MCLibPublishStatusActivity.this.updateGridView();
            }
        });
    }

    /* access modifiers changed from: private */
    public void updateGridView() {
        this.mHandler.post(new Runnable() {
            public void run() {
                ArrayList<HashMap<String, Integer>> emotionList = new ArrayList<>();
                for (Map.Entry<String, Integer> entry : MCLibEmotionsConstant.geEmotionConstant().getAllEmotionsMap().entrySet()) {
                    HashMap<String, Integer> hashMap = new HashMap<>();
                    hashMap.put(entry.getKey(), entry.getValue());
                    emotionList.add(hashMap);
                }
                MCLibPublishStatusActivity.this.emotionGridView.setAdapter((ListAdapter) new MCLibEmotionGridViewAdapter(MCLibPublishStatusActivity.this, emotionList, R.layout.mc_lib_emotion_grid_item, new String[0], new int[0], MCLibPublishStatusActivity.this.contentEditText, MCLibPublishStatusActivity.this, MCLibPublishStatusActivity.this.mHandler));
            }
        });
    }

    public void closeQuickRespPanel() {
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.mc_lib_shrink_to_middle);
        animation.setDuration(400);
        animation.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
                MCLibPublishStatusActivity.this.mHandler.post(new Runnable() {
                    public void run() {
                        MCLibPublishStatusActivity.this.quickRespLayer.setVisibility(4);
                    }
                });
            }
        });
        this.quickRespLayer.startAnimation(animation);
    }

    /* access modifiers changed from: private */
    public void publishStatus(String content, int photoId2, String photoPath2) {
        int msg;
        if (this.uploadImageStatus == this.IMAGE_UPLOADING) {
            showMsg(R.string.mc_lib_image_uploading);
            return;
        }
        int userId = new MCLibUserInfoServiceImpl(this).getLoginUserId();
        MCLibStatusService statusService = new MCLibStatusServiceImpl(this);
        MCLibUserStatus userStatus = new MCLibUserStatus();
        userStatus.setUid(userId);
        userStatus.setContent(content);
        userStatus.setCommunicationType(1);
        userStatus.setPhotoId(photoId2);
        userStatus.setPhotoPath(photoPath2);
        boolean isSucc = statusService.putStatusInQueue(userStatus);
        if (isSucc) {
            startService(new Intent(this, MCLibUserPublishQueueService.class));
            msg = R.string.mc_lib_status_in_queue;
        } else {
            msg = R.string.mc_lib_pub_failed;
        }
        showMsg(msg);
        if (isSucc) {
            cleanTempFiles();
            finish();
        }
    }

    /* access modifiers changed from: private */
    public void replyStatus(String content, int photoId2, String photoPath2) {
        int msg;
        if (this.uploadImageStatus == this.IMAGE_UPLOADING) {
            showMsg(R.string.mc_lib_image_uploading);
            return;
        }
        int userId = new MCLibUserInfoServiceImpl(this).getLoginUserId();
        MCLibStatusService statusService = new MCLibStatusServiceImpl(this);
        MCLibUserStatus userStatus = new MCLibUserStatus();
        userStatus.setUid(userId);
        userStatus.setContent(content);
        userStatus.setCommunicationType(2);
        userStatus.setToUid(Integer.parseInt(this.toStatusId + ""));
        userStatus.setPhotoId(photoId2);
        userStatus.setPhotoPath(photoPath2);
        if (this.rootId == -1) {
            userStatus.setToStatusId(this.toStatusId);
        } else {
            userStatus.setToStatusId(this.rootId);
        }
        boolean isSucc = statusService.putStatusInQueue(userStatus);
        if (isSucc) {
            startService(new Intent(this, MCLibUserPublishQueueService.class));
            msg = R.string.mc_lib_status_in_queue;
        } else {
            msg = R.string.mc_lib_pub_failed;
        }
        showMsg(msg);
        if (isSucc) {
            cleanTempFiles();
            finish();
        }
    }

    /* access modifiers changed from: private */
    public void cleanTempFiles() {
        File cameraFile = new File(MCLibImageLoader.getImageCacheBasePath() + MCLibIOUtil.FS + MOB_CAMERA_FILE_NAME);
        File selectFile = new File(MCLibImageLoader.getImageCacheBasePath() + MCLibIOUtil.FS + MOB_SELECTED_FILE_NAME);
        if (cameraFile.exists()) {
            cameraFile.delete();
        }
        if (selectFile.exists()) {
            selectFile.delete();
        }
        if (this.compressedImagePath != null && !"".equals(this.compressedImagePath)) {
            File compressFile = new File(this.compressedImagePath);
            if (compressFile.exists()) {
                compressFile.delete();
            }
        }
    }

    private void showMsg(final int msg) {
        this.mHandler.post(new Runnable() {
            public void run() {
                Toast.makeText(MCLibPublishStatusActivity.this, msg, 0).show();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String path = "";
        if (this.imageType == this.TAKE_PHOTO) {
            path = MCLibImageLoader.getImageCacheBasePath() + MCLibIOUtil.FS + MOB_CAMERA_FILE_NAME;
            if (!new File(path).exists()) {
                if (data == null || data.getExtras() == null) {
                    showMsg(R.string.mc_lib_invalid_image_type);
                    return;
                }
                Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                if (!MCLibIOUtil.saveImageFile(this, bitmap, Bitmap.CompressFormat.JPEG, MOB_CAMERA_FILE_NAME, MCLibImageLoader.getImageCacheBasePath())) {
                    showMsg(R.string.mc_lib_invalid_image_type);
                    return;
                } else if (bitmap != null) {
                    bitmap.recycle();
                }
            }
        } else if (this.imageType == this.SELECT_LOCAL_PHOTO) {
            if (resultCode == -1) {
                try {
                    Bitmap bitmap2 = BitmapFactory.decodeStream(getContentResolver().openInputStream(data.getData()));
                    path = MCLibImageLoader.getImageCacheBasePath() + MCLibIOUtil.FS + MOB_SELECTED_FILE_NAME;
                    if (!MCLibIOUtil.saveImageFile(this, bitmap2, Bitmap.CompressFormat.JPEG, MOB_SELECTED_FILE_NAME, MCLibImageLoader.getImageCacheBasePath())) {
                        showMsg(R.string.mc_lib_invalid_image_type);
                        return;
                    } else if (bitmap2 != null) {
                        bitmap2.recycle();
                    }
                } catch (FileNotFoundException e) {
                    showMsg(R.string.mc_lib_invalid_image_type);
                    return;
                }
            } else {
                return;
            }
        }
        this.compressedImagePath = MCLibImageUtil.compressBitmap(path, 480, 100, COMPRESSED_IMAGE_NAME, 3, this);
        if (this.compressedImagePath == null || "".equals(this.compressedImagePath)) {
            cleanTempFiles();
        } else {
            showPreviewMaskPanel();
        }
    }

    private void showPreviewMaskPanel() {
        this.mHandler.post(new Runnable() {
            public void run() {
                MCLibPublishStatusActivity.this.maskLayer.setVisibility(0);
                MCLibPublishStatusActivity.this.previewImage.setVisibility(0);
                MCLibPublishStatusActivity.this.confirmBtn.setVisibility(0);
                MCLibPublishStatusActivity.this.cancelBtn.setVisibility(0);
                MCLibPublishStatusActivity.this.previewImage.setBackgroundDrawable(new BitmapDrawable(MCLibImageLoader.getBitmap(MCLibPublishStatusActivity.this.compressedImagePath, 1)));
            }
        });
    }

    /* access modifiers changed from: private */
    public void proceedUploadImage() {
        this.uploadImageStatus = this.IMAGE_UPLOADING;
        setViewVisibility(this.uploadingProgressBar, 0);
        setViewVisibility(this.previewSmallImage, 0);
        setViewVisibility(this.deleteImage, 8);
        this.mHandler.post(new Runnable() {
            public void run() {
                MCLibPublishStatusActivity.this.previewSmallImage.setBackgroundResource(R.drawable.mc_lib_img_waiting);
            }
        });
        new Thread() {
            public void run() {
                final MCLibUserStatus userStatus = new MCLibStatusServiceImpl(MCLibPublishStatusActivity.this).publishStatusImage(new MCLibUserInfoServiceImpl(MCLibPublishStatusActivity.this).getLoginUserId(), MCLibPublishStatusActivity.this.compressedImagePath);
                if (userStatus.getErrorMsg() == null || "".equals(userStatus.getErrorMsg())) {
                    int unused = MCLibPublishStatusActivity.this.photoId = userStatus.getPhotoId();
                    String unused2 = MCLibPublishStatusActivity.this.photoPath = userStatus.getPhotoPath();
                    MCLibPublishStatusActivity.this.setViewVisibility(MCLibPublishStatusActivity.this.uploadingProgressBar, 4);
                    MCLibPublishStatusActivity.this.setViewVisibility(MCLibPublishStatusActivity.this.previewSmallImage, 0);
                    MCLibPublishStatusActivity.this.mHandler.post(new Runnable() {
                        public void run() {
                            MCLibPublishStatusActivity.this.previewSmallImage.setBackgroundDrawable(new BitmapDrawable(MCLibImageLoader.getBitmap(MCLibPublishStatusActivity.this.compressedImagePath, 8)));
                            MCLibPublishStatusActivity.this.deleteImage.setVisibility(0);
                        }
                    });
                    int unused3 = MCLibPublishStatusActivity.this.uploadImageStatus = MCLibPublishStatusActivity.this.IMAGE_UPLOADED;
                    return;
                }
                MCLibPublishStatusActivity.this.mHandler.post(new Runnable() {
                    public void run() {
                        Toast.makeText(MCLibPublishStatusActivity.this, userStatus.getErrorMsg(), 1).show();
                        MCLibPublishStatusActivity.this.setViewVisibility(MCLibPublishStatusActivity.this.uploadingProgressBar, 4);
                        MCLibPublishStatusActivity.this.setViewVisibility(MCLibPublishStatusActivity.this.previewSmallImage, 4);
                    }
                });
                int unused4 = MCLibPublishStatusActivity.this.uploadImageStatus = MCLibPublishStatusActivity.this.NO_IMAGE_ATTACHED;
            }
        }.start();
    }

    /* access modifiers changed from: private */
    public void setViewVisibility(final List<View> views, final int visibleValue) {
        this.mHandler.post(new Runnable() {
            public void run() {
                for (int i = 0; i < views.size(); i++) {
                    ((View) views.get(i)).setVisibility(visibleValue);
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void setViewVisibility(final View view, final int visibleValue) {
        this.mHandler.post(new Runnable() {
            public void run() {
                view.setVisibility(visibleValue);
            }
        });
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            cleanTempFiles();
        }
        return super.onKeyDown(keyCode, event);
    }

    public Handler getCurrentHandler() {
        return this.mHandler;
    }
}
