package com.mobcent.lib.android.ui.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.mobcent.android.model.MCLibUserInfo;
import com.mobcent.android.os.service.helper.MCLibDeveloperServiceHelper;
import com.mobcent.android.seed.autovGhnzo98NIzLsRPLHS.R;
import com.mobcent.android.service.MCLibUserInfoService;
import com.mobcent.android.service.impl.MCLibUserInfoServiceImpl;
import com.mobcent.android.state.MCLibAppState;
import com.mobcent.lib.android.developer.MCLibDeveloperProfile;
import com.mobcent.lib.android.ui.activity.MCLibCommunityBundleActivity;
import com.mobcent.lib.android.ui.delegate.impl.MCLibUserAutoRegDelegateImpl;
import com.mobcent.lib.android.utils.MCLibImageUtil;
import com.mobcent.lib.android.utils.MCLibMD5Util;
import com.mobcent.lib.android.utils.i18n.MCLibStringBundleUtil;
import java.util.List;

public class MCLibRegLoginDialog extends Dialog {
    /* access modifiers changed from: private */
    public Activity activity;
    private ImageView appIcon;
    /* access modifiers changed from: private */
    public RelativeLayout autoLoginBox;
    /* access modifiers changed from: private */
    public CheckBox autoLoginCB;
    /* access modifiers changed from: private */
    public Button autoRegBtn;
    /* access modifiers changed from: private */
    public String changeDefaultPwd = "(*& ^%$";
    /* access modifiers changed from: private */
    public String changedPwd;
    private final String defaultPwd = "$%^ &*(";
    /* access modifiers changed from: private */
    public Class<?> goToActicityClass;
    /* access modifiers changed from: private */
    public boolean isForceProvideAccount = true;
    /* access modifiers changed from: private */
    public boolean isStayOnCurrentActivity;
    /* access modifiers changed from: private */
    public Button loginBtn;
    /* access modifiers changed from: private */
    public Handler loginHandler = new Handler();
    /* access modifiers changed from: private */
    public TextView loginRegErrorMsg;
    private TextView loginRegTitle;
    private TextView loginingCommMsg;
    /* access modifiers changed from: private */
    public ProgressBar progressBar;
    /* access modifiers changed from: private */
    public EditText pwdText;
    /* access modifiers changed from: private */
    public LinearLayout regLoginBox;
    /* access modifiers changed from: private */
    public CheckBox savePwdCB;
    /* access modifiers changed from: private */
    public AutoCompleteTextView userNameOrEmailText;
    /* access modifiers changed from: private */
    public ImageView userPhoto;

    public MCLibRegLoginDialog(Activity activity2, int theme, boolean isForceProvideAccount2, boolean isStayOnCurrentActivity2, Class<?> goToActicityClass2) {
        super(activity2, theme);
        this.activity = activity2;
        this.isForceProvideAccount = isForceProvideAccount2;
        this.goToActicityClass = goToActicityClass2;
        this.isStayOnCurrentActivity = isStayOnCurrentActivity2;
        MCLibAppState.isNeedRelogin = MCLibAppState.IS_RELOGINING;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.mc_lib_dialog_reg_login);
        updateAppKey();
        MCLibUserInfoService uis = new MCLibUserInfoServiceImpl(getContext());
        MCLibUserInfo loginUserInfo = uis.getLoginUser();
        boolean isSavePwd = uis.isSavePwd();
        boolean isAutoLogin = uis.isAutoLogin();
        initBoxs(loginUserInfo, isAutoLogin, this.isForceProvideAccount);
        initErrorMsg();
        initUserPhoto(loginUserInfo);
        initUserNameOrEmailText(loginUserInfo);
        initPwdText(loginUserInfo, isSavePwd);
        initSavePwdCB(isSavePwd);
        initAutoLoginCB(isAutoLogin);
        initLoginBtn();
        initAutoRegBtn();
        initProgressBar();
    }

    private void updateAppKey() {
        MCLibDeveloperServiceHelper.updateAppKey(MCLibDeveloperProfile.getBpAppKey(this.activity), this.activity);
    }

    /* access modifiers changed from: private */
    public void startService() {
        MCLibDeveloperServiceHelper.onAppLaunched(R.raw.mc_lib_msg, this.activity);
    }

    private void initBoxs(MCLibUserInfo loginUserInfo, boolean isAutoLogin, boolean isForceProvideAccount2) {
        this.autoLoginBox = (RelativeLayout) findViewById(R.id.mcLibAutoLoginBox);
        this.regLoginBox = (LinearLayout) findViewById(R.id.mcLibRegLoginBox);
        this.loginRegTitle = (TextView) findViewById(R.id.mcLibLoginRegTitle);
        this.loginingCommMsg = (TextView) findViewById(R.id.mcLibLoginingCommMsg);
        this.appIcon = (ImageView) findViewById(R.id.mcLibAppIcon);
        String[] arg = {MCLibDeveloperProfile.getBpAppName(getContext())};
        this.loginRegTitle.setText(MCLibStringBundleUtil.resolveString(R.string.mc_lib_login_reg_title, arg, getContext()));
        this.loginingCommMsg.setText(MCLibStringBundleUtil.resolveString(R.string.mc_lib_login_community, arg, getContext()));
        this.appIcon.setBackgroundDrawable(this.activity.getApplicationInfo().loadIcon(this.activity.getPackageManager()));
        this.appIcon.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                MCLibRegLoginDialog.this.dismiss();
            }
        });
        if (loginUserInfo == null || !isAutoLogin || isForceProvideAccount2) {
            hideAutoLoginBox();
            return;
        }
        showAutoLoginBox();
        doUserLogin(loginUserInfo.getNickName(), loginUserInfo.getPassword(), true, true);
    }

    private void initErrorMsg() {
        this.loginRegErrorMsg = (TextView) findViewById(R.id.mcLibLoginRegError);
    }

    private void initProgressBar() {
        this.progressBar = (ProgressBar) findViewById(R.id.mcLibLoginProgressBar);
    }

    private void initUserPhoto(MCLibUserInfo loginUserInfo) {
        this.userPhoto = (ImageView) findViewById(R.id.mcLibUserPhotoImageView);
        if (loginUserInfo != null) {
            MCLibImageUtil.updateUserPhotoImage(this.userPhoto, loginUserInfo.getImage(), getContext(), R.drawable.mc_lib_face_u0, this.loginHandler);
        }
    }

    private void initUserNameOrEmailText(MCLibUserInfo user) {
        this.userNameOrEmailText = (AutoCompleteTextView) findViewById(R.id.mcLibUserNameOrEmailField);
        if (!(user == null || user.getName() == null)) {
            this.userNameOrEmailText.setText(user.getNickName());
        }
        initHintAccounts();
    }

    private void initHintAccounts() {
        final List<MCLibUserInfo> userAccounts = new MCLibUserInfoServiceImpl(getContext()).getAllLocalUsers();
        this.loginHandler.postDelayed(new Runnable() {
            public void run() {
                if (userAccounts != null && !userAccounts.isEmpty()) {
                    String[] accounts = new String[userAccounts.size()];
                    for (int i = 0; i < userAccounts.size(); i++) {
                        accounts[i] = ((MCLibUserInfo) userAccounts.get(i)).getNickName();
                    }
                    MCLibRegLoginDialog.this.userNameOrEmailText.setAdapter(new ArrayAdapter<>(MCLibRegLoginDialog.this.getContext(), 17367050, accounts));
                    MCLibRegLoginDialog.this.userNameOrEmailText.setThreshold(0);
                    MCLibRegLoginDialog.this.userNameOrEmailText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        public void onFocusChange(View v, boolean hasFocus) {
                            if (hasFocus) {
                                MCLibRegLoginDialog.this.userNameOrEmailText.showDropDown();
                            }
                        }
                    });
                    MCLibRegLoginDialog.this.userNameOrEmailText.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            MCLibRegLoginDialog.this.userNameOrEmailText.showDropDown();
                        }
                    });
                }
            }
        }, 10);
        this.userNameOrEmailText.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void afterTextChanged(Editable s) {
                setUserImagePwd(MCLibRegLoginDialog.this.userNameOrEmailText.getText().toString());
            }

            /* JADX WARNING: Removed duplicated region for block: B:7:0x0019  */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            private void setUserImagePwd(java.lang.String r8) {
                /*
                    r7 = this;
                    java.util.List r2 = r0
                    if (r2 == 0) goto L_0x000c
                    java.util.List r2 = r0
                    boolean r2 = r2.isEmpty()
                    if (r2 == 0) goto L_0x000d
                L_0x000c:
                    return
                L_0x000d:
                    java.util.List r2 = r0
                    java.util.Iterator r0 = r2.iterator()
                L_0x0013:
                    boolean r2 = r0.hasNext()
                    if (r2 == 0) goto L_0x000c
                    java.lang.Object r1 = r0.next()
                    com.mobcent.android.model.MCLibUserInfo r1 = (com.mobcent.android.model.MCLibUserInfo) r1
                    if (r8 == 0) goto L_0x000c
                    java.lang.String r2 = r1.getName()
                    boolean r2 = r8.equals(r2)
                    if (r2 != 0) goto L_0x0035
                    java.lang.String r2 = r1.getNickName()
                    boolean r2 = r8.equals(r2)
                    if (r2 == 0) goto L_0x0013
                L_0x0035:
                    com.mobcent.lib.android.ui.dialog.MCLibRegLoginDialog r2 = com.mobcent.lib.android.ui.dialog.MCLibRegLoginDialog.this
                    android.widget.ImageView r2 = r2.userPhoto
                    java.lang.String r3 = r1.getImage()
                    com.mobcent.lib.android.ui.dialog.MCLibRegLoginDialog r4 = com.mobcent.lib.android.ui.dialog.MCLibRegLoginDialog.this
                    android.content.Context r4 = r4.getContext()
                    r5 = 2130837677(0x7f0200ad, float:1.7280315E38)
                    com.mobcent.lib.android.ui.dialog.MCLibRegLoginDialog r6 = com.mobcent.lib.android.ui.dialog.MCLibRegLoginDialog.this
                    android.os.Handler r6 = r6.loginHandler
                    com.mobcent.lib.android.utils.MCLibImageUtil.updateUserPhotoImage(r2, r3, r4, r5, r6)
                    int r2 = r1.getIsSavePwd()
                    r3 = 1
                    if (r2 != r3) goto L_0x0067
                    com.mobcent.lib.android.ui.dialog.MCLibRegLoginDialog r2 = com.mobcent.lib.android.ui.dialog.MCLibRegLoginDialog.this
                    android.os.Handler r2 = r2.loginHandler
                    com.mobcent.lib.android.ui.dialog.MCLibRegLoginDialog$3$1 r3 = new com.mobcent.lib.android.ui.dialog.MCLibRegLoginDialog$3$1
                    r3.<init>(r1)
                    r2.post(r3)
                    goto L_0x000c
                L_0x0067:
                    com.mobcent.lib.android.ui.dialog.MCLibRegLoginDialog r2 = com.mobcent.lib.android.ui.dialog.MCLibRegLoginDialog.this
                    android.os.Handler r2 = r2.loginHandler
                    com.mobcent.lib.android.ui.dialog.MCLibRegLoginDialog$3$2 r3 = new com.mobcent.lib.android.ui.dialog.MCLibRegLoginDialog$3$2
                    r3.<init>(r1)
                    r2.post(r3)
                    goto L_0x000c
                */
                throw new UnsupportedOperationException("Method not decompiled: com.mobcent.lib.android.ui.dialog.MCLibRegLoginDialog.AnonymousClass3.setUserImagePwd(java.lang.String):void");
            }
        });
    }

    private void initPwdText(MCLibUserInfo user, boolean isSavePwd) {
        this.pwdText = (EditText) findViewById(R.id.mcLibPasswordField);
        if (user != null && user.getPassword() != null && !"".equals(user.getPassword()) && isSavePwd) {
            this.pwdText.setText("$%^ &*(");
        }
    }

    private void initSavePwdCB(boolean isSavePwd) {
        this.savePwdCB = (CheckBox) findViewById(R.id.mcLibSavePwdCB);
        this.savePwdCB.setChecked(isSavePwd);
        this.savePwdCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked) {
                    MCLibRegLoginDialog.this.autoLoginCB.setChecked(false);
                }
            }
        });
    }

    private void initAutoLoginCB(boolean isAutoLogin) {
        this.autoLoginCB = (CheckBox) findViewById(R.id.mcLibAutoLoginCB);
        this.autoLoginCB.setChecked(isAutoLogin);
        this.autoLoginCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    MCLibRegLoginDialog.this.savePwdCB.setChecked(true);
                }
            }
        });
    }

    private void initLoginBtn() {
        this.loginBtn = (Button) findViewById(R.id.mcLibLoginButton);
        this.loginBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String pwd;
                String userNameOrEmail = MCLibRegLoginDialog.this.userNameOrEmailText.getText().toString();
                String pwd2 = MCLibRegLoginDialog.this.pwdText.getText().toString();
                boolean isSavePwd = MCLibRegLoginDialog.this.savePwdCB.isChecked();
                boolean isAutoLogin = MCLibRegLoginDialog.this.autoLoginCB.isChecked();
                if (userNameOrEmail == null || userNameOrEmail.equals("")) {
                    MCLibRegLoginDialog.this.userNameOrEmailText.setHint((int) R.string.mc_lib_user_account_or_email_hint);
                    MCLibRegLoginDialog.this.userNameOrEmailText.setHintTextColor(MCLibRegLoginDialog.this.getContext().getResources().getColor(R.color.mc_lib_red));
                } else if (pwd2 == null || pwd2.equals("")) {
                    MCLibRegLoginDialog.this.pwdText.setHint((int) R.string.mc_lib_user_password_hint);
                    MCLibRegLoginDialog.this.pwdText.setHintTextColor(MCLibRegLoginDialog.this.getContext().getResources().getColor(R.color.mc_lib_red));
                } else if (!MCLibRegLoginDialog.isNetworkAvailable(MCLibRegLoginDialog.this.getContext())) {
                    Toast.makeText(MCLibRegLoginDialog.this.getContext(), (int) R.string.mc_lib_connection_fail, 1).show();
                } else {
                    if (pwd2.equals("$%^ &*(")) {
                        pwd = new MCLibUserInfoServiceImpl(MCLibRegLoginDialog.this.getContext()).getLoginUser().getPassword();
                    } else if (pwd2.equals(MCLibRegLoginDialog.this.changeDefaultPwd)) {
                        pwd = MCLibRegLoginDialog.this.changedPwd;
                    } else {
                        pwd = MCLibMD5Util.toMD5(pwd2);
                    }
                    MCLibRegLoginDialog.this.doUserLogin(userNameOrEmail, pwd, isSavePwd, isAutoLogin);
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void doUserLogin(String userNameOrEmail, String pwd, boolean isSavePwd, boolean isAutoLogin) {
        showWaitingPrg();
        final String str = userNameOrEmail;
        final String str2 = pwd;
        final boolean z = isSavePwd;
        final boolean z2 = isAutoLogin;
        new Thread() {
            public void run() {
                if (MCLibRegLoginDialog.this.isForceProvideAccount || MCLibAppState.serverHeartBeatReq != MCLibAppState.APP_RUNNING || !MCLibAppState.isValidLogin) {
                    String isLoginSucc = new MCLibUserInfoServiceImpl(MCLibRegLoginDialog.this.getContext()).doLogin(-1, str.indexOf("@") > -1 ? "-1" : str, str.indexOf("@") == -1 ? "-1" : str, str2, z, z2, "false");
                    if (isLoginSucc.equals("rs_succ")) {
                        MCLibRegLoginDialog.this.startService();
                        if (!MCLibRegLoginDialog.this.isStayOnCurrentActivity) {
                            if (MCLibRegLoginDialog.this.goToActicityClass == null) {
                                MCLibRegLoginDialog.this.activity.startActivity(new Intent(MCLibRegLoginDialog.this.getContext(), MCLibCommunityBundleActivity.class));
                            } else {
                                MCLibRegLoginDialog.this.activity.startActivity(new Intent(MCLibRegLoginDialog.this.getContext(), MCLibRegLoginDialog.this.goToActicityClass));
                            }
                        }
                        MCLibRegLoginDialog.this.dismiss();
                        return;
                    }
                    MCLibRegLoginDialog.this.hideAutoLoginBox();
                    MCLibRegLoginDialog.this.hideWaitingPrg();
                    if (isLoginSucc.equals("network_unavailable")) {
                        MCLibRegLoginDialog.this.showLoginErrorMsg(R.string.mc_lib_connection_fail);
                    } else if (str.indexOf("@") > -1) {
                        MCLibRegLoginDialog.this.showLoginErrorMsg(R.string.mc_lib_login_email_pwd_error_msg);
                    } else {
                        MCLibRegLoginDialog.this.showLoginErrorMsg(R.string.mc_lib_login_account_pwd_error_msg);
                    }
                } else {
                    MCLibRegLoginDialog.this.startService();
                    if (!MCLibRegLoginDialog.this.isStayOnCurrentActivity) {
                        if (MCLibRegLoginDialog.this.goToActicityClass == null) {
                            MCLibRegLoginDialog.this.activity.startActivity(new Intent(MCLibRegLoginDialog.this.getContext(), MCLibCommunityBundleActivity.class));
                        } else {
                            MCLibRegLoginDialog.this.activity.startActivity(new Intent(MCLibRegLoginDialog.this.getContext(), MCLibRegLoginDialog.this.goToActicityClass));
                        }
                        MCLibRegLoginDialog.this.dismiss();
                    }
                }
            }
        }.start();
    }

    public void showLoginErrorMsg(final int errorMsgId) {
        this.loginHandler.post(new Runnable() {
            public void run() {
                MCLibRegLoginDialog.this.loginRegErrorMsg.setText(errorMsgId);
                MCLibRegLoginDialog.this.loginRegErrorMsg.setVisibility(0);
                Animation anim = new AlphaAnimation(1.0f, 0.0f);
                anim.setFillAfter(false);
                anim.setDuration(2000);
                anim.setStartOffset(10000);
                MCLibRegLoginDialog.this.loginRegErrorMsg.startAnimation(anim);
                anim.setAnimationListener(new Animation.AnimationListener() {
                    public void onAnimationStart(Animation animation) {
                    }

                    public void onAnimationRepeat(Animation animation) {
                    }

                    public void onAnimationEnd(Animation animation) {
                        MCLibRegLoginDialog.this.loginRegErrorMsg.setVisibility(8);
                    }
                });
            }
        });
    }

    private void initAutoRegBtn() {
        this.autoRegBtn = (Button) findViewById(R.id.mcLibAutoRegButton);
        this.autoRegBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                new MCLibUserAutoRegDelegateImpl(MCLibRegLoginDialog.this.activity, MCLibRegLoginDialog.this, MCLibRegLoginDialog.this.isStayOnCurrentActivity, MCLibRegLoginDialog.this.goToActicityClass).doUserAction();
            }
        });
    }

    public void showWaitingPrg() {
        this.loginHandler.post(new Runnable() {
            public void run() {
                MCLibRegLoginDialog.this.loginBtn.setVisibility(8);
                MCLibRegLoginDialog.this.autoRegBtn.setVisibility(8);
                MCLibRegLoginDialog.this.progressBar.setVisibility(0);
            }
        });
    }

    public void hideWaitingPrg() {
        this.loginHandler.post(new Runnable() {
            public void run() {
                MCLibRegLoginDialog.this.progressBar.setVisibility(8);
                MCLibRegLoginDialog.this.loginBtn.setVisibility(0);
                MCLibRegLoginDialog.this.autoRegBtn.setVisibility(0);
            }
        });
    }

    private void showAutoLoginBox() {
        this.loginHandler.post(new Runnable() {
            public void run() {
                MCLibRegLoginDialog.this.autoLoginBox.setVisibility(0);
                MCLibRegLoginDialog.this.regLoginBox.setVisibility(8);
            }
        });
    }

    /* access modifiers changed from: private */
    public void hideAutoLoginBox() {
        this.loginHandler.post(new Runnable() {
            public void run() {
                MCLibRegLoginDialog.this.autoLoginBox.setVisibility(8);
                MCLibRegLoginDialog.this.regLoginBox.setVisibility(0);
            }
        });
    }

    public static boolean isNetworkAvailable(Context context) {
        NetworkInfo info = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (info == null || !info.isAvailable()) {
            return false;
        }
        return true;
    }
}
