package com.mobcent.lib.android.ui.activity.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.View;
import com.mobcent.lib.android.ui.delegate.MCLibUserActionDelegate;
import com.mobcent.lib.android.utils.MCLibGifAction;
import com.mobcent.lib.android.utils.MCLibGifDecoder;
import java.io.InputStream;

public class MCLibGifView extends View implements MCLibGifAction {
    private GifImageType animationType;
    /* access modifiers changed from: private */
    public Bitmap currentImage;
    /* access modifiers changed from: private */
    public DrawThread drawThread;
    /* access modifiers changed from: private */
    public MCLibGifDecoder gifDecoder;
    /* access modifiers changed from: private */
    public boolean isRun;
    /* access modifiers changed from: private */
    public boolean pause;
    private Rect rect;
    /* access modifiers changed from: private */
    public Handler redrawHandler;
    private int showWidth;
    private MCLibUserActionDelegate userActionDelegate;
    public int windowHeight;
    public int windowWidth;

    public enum GifImageType {
        WAIT_FINISH(0),
        SYNC_DECODER(1),
        COVER(2);
        
        final int nativeInt;

        private GifImageType(int i) {
            this.nativeInt = i;
        }
    }

    public MCLibGifView(Context context) {
        super(context);
        this.gifDecoder = null;
        this.currentImage = null;
        this.isRun = true;
        this.pause = false;
        this.showWidth = -1;
        this.rect = null;
        this.drawThread = null;
        this.animationType = GifImageType.SYNC_DECODER;
        this.redrawHandler = new Handler() {
            public void handleMessage(Message msg) {
                MCLibGifView.this.invalidate();
            }
        };
    }

    public MCLibGifView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MCLibGifView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.gifDecoder = null;
        this.currentImage = null;
        this.isRun = true;
        this.pause = false;
        this.showWidth = -1;
        this.rect = null;
        this.drawThread = null;
        this.animationType = GifImageType.SYNC_DECODER;
        this.redrawHandler = new Handler() {
            public void handleMessage(Message msg) {
                MCLibGifView.this.invalidate();
            }
        };
    }

    private void setGifDecoderImage(byte[] gif) {
        if (this.gifDecoder != null) {
            this.gifDecoder.free();
            this.gifDecoder = null;
        }
        this.gifDecoder = new MCLibGifDecoder(gif, this);
        this.gifDecoder.start();
    }

    private void setGifDecoderImage(InputStream is) {
        if (this.gifDecoder != null) {
            this.gifDecoder.free();
            this.gifDecoder = null;
        }
        this.gifDecoder = new MCLibGifDecoder(is, this);
        this.gifDecoder.start();
    }

    public void setGifImage(byte[] gif) {
        setGifDecoderImage(gif);
    }

    public void setGifImage(InputStream is) {
        setGifDecoderImage(is);
    }

    public void setGifImage(int resId) {
        setGifDecoderImage(getResources().openRawResource(resId));
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.gifDecoder != null) {
            if (this.currentImage == null) {
                this.currentImage = this.gifDecoder.getImage();
            }
            if (this.currentImage != null) {
                int saveCount = canvas.getSaveCount();
                canvas.save();
                canvas.translate((float) ((this.windowWidth - this.gifDecoder.width) / 2), (float) ((this.windowHeight - this.gifDecoder.height) / 3));
                if (this.showWidth == -1) {
                    canvas.drawBitmap(this.currentImage, 0.0f, 0.0f, (Paint) null);
                } else {
                    canvas.drawBitmap(this.currentImage, (Rect) null, this.rect, (Paint) null);
                }
                canvas.restoreToCount(saveCount);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int w;
        int h;
        int pleft = getPaddingLeft();
        int pright = getPaddingRight();
        int ptop = getPaddingTop();
        int pbottom = getPaddingBottom();
        if (this.gifDecoder == null) {
            w = 1;
            h = 1;
        } else {
            w = this.gifDecoder.width;
            h = this.gifDecoder.height;
        }
        setMeasuredDimension(resolveSize(Math.max(w + pleft + pright, getSuggestedMinimumWidth()), widthMeasureSpec), resolveSize(Math.max(h + ptop + pbottom, getSuggestedMinimumHeight()), heightMeasureSpec));
    }

    public void showCover() {
        if (this.gifDecoder != null) {
            this.pause = true;
            this.currentImage = this.gifDecoder.getImage();
            invalidate();
        }
    }

    public void showAnimation() {
        if (this.pause) {
            this.pause = false;
        }
    }

    public void setGifImageType(GifImageType type) {
        if (this.gifDecoder == null) {
            this.animationType = type;
        }
    }

    public void setShowDimension(int width, int height) {
        if (width > 0 && height > 0) {
            this.showWidth = width;
            this.rect = new Rect();
            this.rect.left = 0;
            this.rect.top = 0;
            this.rect.right = width;
            this.rect.bottom = height;
        }
    }

    public void parseOk(boolean parseStatus, int frameIndex) {
        if (parseStatus && this.gifDecoder != null) {
            switch (this.animationType) {
                case WAIT_FINISH:
                    if (frameIndex != -1) {
                        return;
                    }
                    if (this.gifDecoder.getFrameCount() > 1) {
                        new DrawThread().start();
                        return;
                    } else {
                        reDraw();
                        return;
                    }
                case COVER:
                    if (frameIndex == 1) {
                        this.currentImage = this.gifDecoder.getImage();
                        reDraw();
                        return;
                    } else if (frameIndex != -1) {
                        return;
                    } else {
                        if (this.gifDecoder.getFrameCount() <= 1) {
                            reDraw();
                            return;
                        } else if (this.drawThread == null) {
                            this.drawThread = new DrawThread();
                            this.drawThread.start();
                            return;
                        } else {
                            return;
                        }
                    }
                case SYNC_DECODER:
                    if (frameIndex == 1) {
                        this.currentImage = this.gifDecoder.getImage();
                        reDraw();
                        return;
                    } else if (frameIndex == -1) {
                        reDraw();
                        return;
                    } else if (this.drawThread == null) {
                        this.drawThread = new DrawThread();
                        this.drawThread.start();
                        return;
                    } else {
                        return;
                    }
                default:
                    return;
            }
        }
    }

    private void reDraw() {
        if (this.redrawHandler != null) {
            this.redrawHandler.sendMessage(this.redrawHandler.obtainMessage());
        }
    }

    public MCLibUserActionDelegate getUserActionDelegate() {
        return this.userActionDelegate;
    }

    public void setUserActionDelegate(MCLibUserActionDelegate userActionDelegate2) {
        this.userActionDelegate = userActionDelegate2;
    }

    private class DrawThread extends Thread {
        private DrawThread() {
        }

        /* JADX WARNING: CFG modification limit reached, blocks count: 119 */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r6 = this;
                com.mobcent.lib.android.ui.activity.view.MCLibGifView r4 = com.mobcent.lib.android.ui.activity.view.MCLibGifView.this
                com.mobcent.lib.android.utils.MCLibGifDecoder r4 = r4.gifDecoder
                if (r4 != 0) goto L_0x0031
            L_0x0008:
                return
            L_0x0009:
                com.mobcent.lib.android.ui.activity.view.MCLibGifView r4 = com.mobcent.lib.android.ui.activity.view.MCLibGifView.this
                android.graphics.Bitmap r5 = r0.image
                android.graphics.Bitmap unused = r4.currentImage = r5
                int r4 = r0.delay
                long r2 = (long) r4
                com.mobcent.lib.android.ui.activity.view.MCLibGifView r4 = com.mobcent.lib.android.ui.activity.view.MCLibGifView.this
                android.os.Handler r4 = r4.redrawHandler
                if (r4 == 0) goto L_0x004d
                com.mobcent.lib.android.ui.activity.view.MCLibGifView r4 = com.mobcent.lib.android.ui.activity.view.MCLibGifView.this
                android.os.Handler r4 = r4.redrawHandler
                android.os.Message r1 = r4.obtainMessage()
                com.mobcent.lib.android.ui.activity.view.MCLibGifView r4 = com.mobcent.lib.android.ui.activity.view.MCLibGifView.this
                android.os.Handler r4 = r4.redrawHandler
                r4.sendMessage(r1)
                android.os.SystemClock.sleep(r2)
            L_0x0031:
                com.mobcent.lib.android.ui.activity.view.MCLibGifView r4 = com.mobcent.lib.android.ui.activity.view.MCLibGifView.this
                boolean r4 = r4.isRun
                if (r4 == 0) goto L_0x004d
                com.mobcent.lib.android.ui.activity.view.MCLibGifView r4 = com.mobcent.lib.android.ui.activity.view.MCLibGifView.this
                boolean r4 = r4.pause
                if (r4 != 0) goto L_0x0062
                com.mobcent.lib.android.ui.activity.view.MCLibGifView r4 = com.mobcent.lib.android.ui.activity.view.MCLibGifView.this
                com.mobcent.lib.android.utils.MCLibGifDecoder r4 = r4.gifDecoder
                com.mobcent.lib.android.utils.MCLibGifFrame r0 = r4.next()
                if (r0 != 0) goto L_0x0009
            L_0x004d:
                com.mobcent.lib.android.ui.activity.view.MCLibGifView r4 = com.mobcent.lib.android.ui.activity.view.MCLibGifView.this
                android.os.Handler r4 = r4.redrawHandler
                com.mobcent.lib.android.ui.activity.view.MCLibGifView$DrawThread$1 r5 = new com.mobcent.lib.android.ui.activity.view.MCLibGifView$DrawThread$1
                r5.<init>()
                r4.post(r5)
                com.mobcent.lib.android.ui.activity.view.MCLibGifView r4 = com.mobcent.lib.android.ui.activity.view.MCLibGifView.this
                r5 = 0
                com.mobcent.lib.android.ui.activity.view.MCLibGifView.DrawThread unused = r4.drawThread = r5
                goto L_0x0008
            L_0x0062:
                r4 = 10
                android.os.SystemClock.sleep(r4)
                goto L_0x0031
            */
            throw new UnsupportedOperationException("Method not decompiled: com.mobcent.lib.android.ui.activity.view.MCLibGifView.DrawThread.run():void");
        }
    }
}
