package com.mobcent.lib.android.ui.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.mobcent.android.constants.MCLibConstants;
import com.mobcent.android.model.MCLibUserInfo;
import com.mobcent.android.seed.autovGhnzo98NIzLsRPLHS.R;
import com.mobcent.android.service.MCLibUserInfoService;
import com.mobcent.android.service.impl.MCLibUserInfoServiceImpl;
import com.mobcent.android.utils.MCLibIOUtil;
import com.mobcent.lib.android.constants.MCLibUpdateUserPhotoTypes;
import com.mobcent.lib.android.ui.activity.contextmenu.MCLibUpdateUserPhotoContextMenu;
import com.mobcent.lib.android.ui.delegate.impl.MCLibUpdateUserPhotoDelegateImpl;
import com.mobcent.lib.android.ui.dialog.MCLibDatePicker;
import com.mobcent.lib.android.ui.dialog.MCLibUserPhotoSelectorDialog;
import com.mobcent.lib.android.utils.MCLibImageLoader;
import com.mobcent.lib.android.utils.MCLibImageUtil;
import com.mobcent.lib.android.utils.MCLibPhoneUtil;
import com.mobcent.lib.android.utils.MCLibStringUtil;
import com.mobcent.lib.android.utils.i18n.MCLibStringBundleUtil;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class MCLibUserSettingActivity extends MCLibUIBaseActivity {
    public static final String COMPRESSED_IMAGE_IMAGE = "_mob_sqr_compress_image.jpg";
    public static final String MOB_CAMERA_FILE_NAME = "_mob_camera_icon_.jpg";
    public static final String MOB_SELECTED_FILE_NAME = "_mob_select_icon_.jpg";
    private final int HIDE_PRG_BAR = 2;
    private final int HIDE_PWD_SHOW_USER_INFO = 5;
    private final int HIDE_USER_INFO_SHOW_PWD = 4;
    private final int MENU_BASIC_SETTING = 100;
    private final int MENU_PWD_SETTING = 101;
    private final int QUERY_USER_INFO_SUCC = 1;
    private int SELECT_LOCAL_PHOTO = 201;
    private final int SHOW_PRG_BAR = 3;
    private int TAKE_PHOTO = 200;
    private LinearLayout basicSettingBox;
    /* access modifiers changed from: private */
    public Button birthdayButton;
    /* access modifiers changed from: private */
    public Button cancelPhotoBtn;
    /* access modifiers changed from: private */
    public EditText cityEditText;
    /* access modifiers changed from: private */
    public String compressedImagePath = "";
    /* access modifiers changed from: private */
    public Button confirmPhotoBtn;
    /* access modifiers changed from: private */
    public EditText confirmPwdEditText;
    /* access modifiers changed from: private */
    public int currentMenu = 100;
    /* access modifiers changed from: private */
    public EditText emailEditText;
    private Spinner genderSpinner;
    private int imageType = this.TAKE_PHOTO;
    private CheckBox isSoundEnableCB;
    /* access modifiers changed from: private */
    public Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            if (msg.what == 1) {
                MCLibUserSettingActivity.this.queryUserInfoSucc();
            } else if (msg.what == 2) {
                MCLibUserSettingActivity.this.hideProgressBar();
            } else if (msg.what == 3) {
                MCLibUserSettingActivity.this.showProgressBar();
            } else if (msg.what == 4) {
                MCLibUserSettingActivity.this.hideUserInfoShowPwd();
            } else if (msg.what == 5) {
                MCLibUserSettingActivity.this.hidePwdShowUserInfo();
            }
        }
    };
    /* access modifiers changed from: private */
    public RelativeLayout maskLayer;
    /* access modifiers changed from: private */
    public TextView menuBasicSettingText;
    /* access modifiers changed from: private */
    public TextView menuPwdSettingText;
    /* access modifiers changed from: private */
    public EditText newPwdEditText;
    /* access modifiers changed from: private */
    public EditText nickNameEditText;
    /* access modifiers changed from: private */
    public ImageView previewImage;
    private LinearLayout pwdSettingBox;
    /* access modifiers changed from: private */
    public Button saveButton;
    /* access modifiers changed from: private */
    public ProgressBar savingPrgBar;
    private TextView userAccountText;
    /* access modifiers changed from: private */
    public MCLibUserInfo userInfo;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.mc_lib_user_setting);
        initWindowTitle();
        initSysMsgWidgets();
        initWidgets();
        initNavBottomBar(105);
        queryUserInfo();
    }

    private void initWidgets() {
        this.menuBasicSettingText = (TextView) findViewById(R.id.mcLibBasicSetting);
        this.menuPwdSettingText = (TextView) findViewById(R.id.mcLibPasswordSetting);
        this.saveButton = (Button) findViewById(R.id.mcLibSaveBtn);
        this.basicSettingBox = (LinearLayout) findViewById(R.id.mcLibBasicSettingBox);
        this.pwdSettingBox = (LinearLayout) findViewById(R.id.mcLibPasswordBox);
        this.userAccountText = (TextView) findViewById(R.id.mcLibUserIdText);
        this.userPhotoImage = (ImageView) findViewById(R.id.mcLibUserPhotoImage);
        this.nickNameEditText = (EditText) findViewById(R.id.mcLibNickNameEditText);
        this.emailEditText = (EditText) findViewById(R.id.mcLibEmailEditText);
        this.birthdayButton = (Button) findViewById(R.id.mcLibBirthdayButton);
        this.genderSpinner = (Spinner) findViewById(R.id.mcLibGenderSpinner);
        this.isSoundEnableCB = (CheckBox) findViewById(R.id.mcLibOpenTipSoundCB);
        this.cityEditText = (EditText) findViewById(R.id.mcLibCityEditText);
        this.newPwdEditText = (EditText) findViewById(R.id.mcLibNewPwdEditText);
        this.confirmPwdEditText = (EditText) findViewById(R.id.mcLibConfirmPwdEditText);
        this.previewImage = (ImageView) findViewById(R.id.mcLibPreviewImage);
        this.confirmPhotoBtn = (Button) findViewById(R.id.mcLibConfirmPhotoBtn);
        this.cancelPhotoBtn = (Button) findViewById(R.id.mcLibCancelPhotoBtn);
        this.maskLayer = (RelativeLayout) findViewById(R.id.mcLibMaskLayer);
        this.savingPrgBar = (ProgressBar) findViewById(R.id.mcLibSavingPrgBar);
        initProgressBox();
        initMenus();
        initMaskLayer();
    }

    private void initMenus() {
        this.menuBasicSettingText.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int unused = MCLibUserSettingActivity.this.currentMenu = 100;
                MCLibUserSettingActivity.this.mHandler.sendEmptyMessage(5);
                MCLibUserSettingActivity.this.mHandler.post(new Runnable() {
                    public void run() {
                        MCLibUserSettingActivity.this.menuBasicSettingText.setBackgroundResource(R.drawable.mc_lib_submenu_bg);
                        MCLibUserSettingActivity.this.menuPwdSettingText.setBackgroundResource(R.drawable.mc_lib_blank);
                    }
                });
            }
        });
        this.menuPwdSettingText.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int unused = MCLibUserSettingActivity.this.currentMenu = 101;
                MCLibUserSettingActivity.this.mHandler.sendEmptyMessage(4);
                MCLibUserSettingActivity.this.mHandler.post(new Runnable() {
                    public void run() {
                        MCLibUserSettingActivity.this.menuBasicSettingText.setBackgroundResource(R.drawable.mc_lib_blank);
                        MCLibUserSettingActivity.this.menuPwdSettingText.setBackgroundResource(R.drawable.mc_lib_submenu_bg);
                    }
                });
            }
        });
    }

    private void queryUserInfo() {
        new Thread() {
            public void run() {
                try {
                    if (MCLibUserSettingActivity.this.userInfo == null) {
                        MCLibUserInfoService userInfoService = new MCLibUserInfoServiceImpl(MCLibUserSettingActivity.this);
                        MCLibUserInfo unused = MCLibUserSettingActivity.this.userInfo = userInfoService.getUserProfile(userInfoService.getLoginUserId());
                        if (MCLibUserSettingActivity.this.userInfo != null && MCLibUserSettingActivity.this.userInfo.getUid() > 0) {
                            MCLibUserInfoService uis = new MCLibUserInfoServiceImpl(MCLibUserSettingActivity.this);
                            uis.updateSharedUserPhoto(MCLibUserSettingActivity.this.userInfo.getImage());
                            uis.updateShareUserNickName(MCLibUserSettingActivity.this.userInfo.getNickName());
                            uis.updateShareUserEmail(MCLibUserSettingActivity.this.userInfo.getEmail());
                            uis.updateShareUserCity(MCLibUserSettingActivity.this.userInfo.getPos());
                            uis.updateShareUserBirthday(MCLibUserSettingActivity.this.userInfo.getBirthday());
                            uis.updateShareUserGender(Integer.parseInt(MCLibUserSettingActivity.this.userInfo.getGender()));
                        }
                    }
                    MCLibUserSettingActivity.this.mHandler.sendEmptyMessage(2);
                    MCLibUserSettingActivity.this.mHandler.sendEmptyMessage(5);
                    MCLibUserSettingActivity.this.mHandler.sendEmptyMessage(1);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    /* access modifiers changed from: private */
    public void queryUserInfoSucc() {
        initUserId();
        initNickName();
        initBirthdayWidget();
        initChangeImageWidget();
        initGenderSpinner();
        initEmailWidget();
        initCityWidget();
        initSoundEnableWidget();
        initSaveButtonWidget();
    }

    private void initSoundEnableWidget() {
        final MCLibUserInfoService uis = new MCLibUserInfoServiceImpl(this);
        this.isSoundEnableCB.setChecked(uis.isSoundEnable());
        this.isSoundEnableCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                uis.setSoundEnable(isChecked);
            }
        });
    }

    private void initSaveButtonWidget() {
        this.saveButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (MCLibUserSettingActivity.this.currentMenu == 100) {
                    MCLibUserSettingActivity.this.saveBasicInfo();
                } else if (MCLibUserSettingActivity.this.currentMenu == 101) {
                    MCLibUserSettingActivity.this.savePassword();
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void saveBasicInfo() {
        new Thread() {
            public void run() {
                MCLibUserInfoService userInfoService = new MCLibUserInfoServiceImpl(MCLibUserSettingActivity.this);
                String nickName = MCLibUserSettingActivity.this.nickNameEditText.getText().toString();
                int checkResult = MCLibUserSettingActivity.this.isNickNameValid(nickName);
                if (checkResult > 0) {
                    MCLibUserSettingActivity.this.showToastMsg(checkResult);
                    return;
                }
                String email = MCLibUserSettingActivity.this.emailEditText.getText().toString();
                if (email == null || email.trim().equals("")) {
                    email = "";
                } else {
                    int checkResult2 = MCLibUserSettingActivity.this.isEmailValid(email);
                    if (checkResult2 > 0) {
                        MCLibUserSettingActivity.this.showToastMsg(checkResult2);
                        return;
                    }
                }
                MCLibUserSettingActivity.this.showWaitingPrg();
                String city = MCLibUserSettingActivity.this.cityEditText.getText().toString() == null ? "" : MCLibUserSettingActivity.this.cityEditText.getText().toString();
                String rs = userInfoService.changeUserProfile(userInfoService.getLoginUserId(), nickName, Integer.parseInt(MCLibUserSettingActivity.this.userInfo.getGender()), MCLibUserSettingActivity.this.userInfo.getBirthday(), email, city);
                if (rs.equals("rs_succ")) {
                    MCLibUserSettingActivity.this.userInfo.setNickName(nickName);
                    MCLibUserSettingActivity.this.userInfo.setPos(city);
                    MCLibUserSettingActivity.this.userInfo.setEmail(email);
                    MCLibUserSettingActivity.this.showToastMsg(R.string.mc_lib_save_succ);
                    MCLibUserSettingActivity.this.hideWaitingPrg();
                } else if (rs.equals("emailExist")) {
                    MCLibUserSettingActivity.this.showToastMsg(R.string.mc_lib_user_email_exist);
                    MCLibUserSettingActivity.this.hideWaitingPrg();
                } else if (rs.equals("nickNameExist")) {
                    MCLibUserSettingActivity.this.showToastMsg(R.string.mc_lib_user_nick_name_exist);
                    MCLibUserSettingActivity.this.hideWaitingPrg();
                } else {
                    MCLibUserSettingActivity.this.showToastMsg(R.string.mc_lib_generic_error);
                    MCLibUserSettingActivity.this.hideWaitingPrg();
                }
            }
        }.start();
    }

    /* access modifiers changed from: private */
    public int isNickNameValid(String nickName) {
        if (nickName == null || nickName.length() < 2 || nickName.length() > 12) {
            return R.string.mc_lib_nick_name_length_tip;
        }
        if (!MCLibStringUtil.isNickNameMatchRule(nickName)) {
            return R.string.mc_lib_nick_name_invalid_tip;
        }
        return 0;
    }

    /* access modifiers changed from: private */
    public int isEmailValid(String email) {
        if (MCLibStringUtil.isEmailValid(email)) {
            return 0;
        }
        return R.string.mc_lib_email_invalid_tip;
    }

    /* access modifiers changed from: private */
    public void savePassword() {
        new Thread() {
            public void run() {
                String newPwd = MCLibUserSettingActivity.this.newPwdEditText.getText().toString();
                String confirmPwd = MCLibUserSettingActivity.this.confirmPwdEditText.getText().toString();
                if (newPwd == null || newPwd.length() > 12 || newPwd.length() < 6) {
                    MCLibUserSettingActivity.this.showToastMsg(R.string.mc_lib_password_length_tip);
                } else if (!MCLibStringUtil.isPwdMatchRule(newPwd)) {
                    MCLibUserSettingActivity.this.showToastMsg(R.string.mc_lib_password_invalid_tip);
                } else if (!newPwd.equals(confirmPwd)) {
                    MCLibUserSettingActivity.this.showToastMsg(R.string.mc_lib_password_not_same);
                } else {
                    MCLibUserSettingActivity.this.showWaitingPrg();
                    MCLibUserInfoService userInfoService = new MCLibUserInfoServiceImpl(MCLibUserSettingActivity.this);
                    if (userInfoService.changeUserPwd(userInfoService.getLoginUserId(), newPwd).equals("rs_succ")) {
                        MCLibUserSettingActivity.this.showToastMsg(R.string.mc_lib_save_succ);
                        MCLibUserSettingActivity.this.hideWaitingPrg();
                        MCLibUserSettingActivity.this.clearPwdFields();
                        return;
                    }
                    MCLibUserSettingActivity.this.showToastMsg(R.string.mc_lib_generic_error);
                    MCLibUserSettingActivity.this.hideWaitingPrg();
                }
            }
        }.start();
    }

    /* access modifiers changed from: private */
    public void showToastMsg(final int msg) {
        this.mHandler.post(new Runnable() {
            public void run() {
                Toast.makeText(MCLibUserSettingActivity.this, msg, 1).show();
            }
        });
    }

    /* access modifiers changed from: private */
    public void showWaitingPrg() {
        this.mHandler.post(new Runnable() {
            public void run() {
                MCLibUserSettingActivity.this.saveButton.setVisibility(4);
                MCLibUserSettingActivity.this.savingPrgBar.setVisibility(0);
            }
        });
    }

    /* access modifiers changed from: private */
    public void hideWaitingPrg() {
        this.mHandler.post(new Runnable() {
            public void run() {
                MCLibUserSettingActivity.this.saveButton.setVisibility(0);
                MCLibUserSettingActivity.this.savingPrgBar.setVisibility(4);
            }
        });
    }

    /* access modifiers changed from: private */
    public void clearPwdFields() {
        this.mHandler.post(new Runnable() {
            public void run() {
                MCLibUserSettingActivity.this.newPwdEditText.setText("");
                MCLibUserSettingActivity.this.confirmPwdEditText.setText("");
            }
        });
    }

    private void initMaskLayer() {
        this.maskLayer.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            }
        });
        this.previewImage.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            }
        });
        this.confirmPhotoBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCLibUserSettingActivity.this.mHandler.post(new Runnable() {
                    public void run() {
                        List<View> views = new ArrayList<>();
                        views.add(MCLibUserSettingActivity.this.maskLayer);
                        views.add(MCLibUserSettingActivity.this.previewImage);
                        views.add(MCLibUserSettingActivity.this.confirmPhotoBtn);
                        views.add(MCLibUserSettingActivity.this.cancelPhotoBtn);
                        MCLibUserSettingActivity.this.setViewVisibility(views, 8);
                        File cameraFile = new File(MCLibImageLoader.getImageCacheBasePath() + MCLibIOUtil.FS + MCLibUserSettingActivity.MOB_CAMERA_FILE_NAME);
                        if (cameraFile.exists()) {
                            cameraFile.delete();
                        }
                        MCLibUserSettingActivity.this.proceedUploadImage();
                    }
                });
            }
        });
        this.cancelPhotoBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                List<View> views = new ArrayList<>();
                views.add(MCLibUserSettingActivity.this.maskLayer);
                views.add(MCLibUserSettingActivity.this.previewImage);
                views.add(MCLibUserSettingActivity.this.confirmPhotoBtn);
                views.add(MCLibUserSettingActivity.this.cancelPhotoBtn);
                MCLibUserSettingActivity.this.setViewVisibility(views, 8);
                File cameraFile = new File(MCLibImageLoader.getImageCacheBasePath() + MCLibIOUtil.FS + MCLibUserSettingActivity.MOB_CAMERA_FILE_NAME);
                if (cameraFile.exists()) {
                    cameraFile.delete();
                }
            }
        });
    }

    private void initNickName() {
        this.nickNameEditText.setText(this.userInfo.getNickName());
    }

    private void initUserId() {
        this.userAccountText.setText(MCLibStringBundleUtil.resolveString(R.string.mc_lib_user_id, new String[]{new MCLibUserInfoServiceImpl(this).getLoginUserName()}, this));
    }

    private void initCityWidget() {
        this.cityEditText.setText(this.userInfo.getPos());
    }

    private void initEmailWidget() {
        this.emailEditText.setText(this.userInfo.getEmail());
    }

    private void initChangeImageWidget() {
        MCLibImageUtil.updateUserPhotoImage(this.userPhotoImage, this.userInfo.getImage(), this, R.drawable.mc_lib_face_u0, this.mHandler);
        this.userPhotoImage.setOnCreateContextMenuListener(new MCLibUpdateUserPhotoContextMenu());
        this.userPhotoImage.setOnClickListener(new ChangeUserPhotoOnClickListener());
    }

    private void initGenderSpinner() {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, 17367048);
        adapter.add(getResources().getString(R.string.mc_lib_male));
        adapter.add(getResources().getString(R.string.mc_lib_female));
        adapter.add(getResources().getString(R.string.mc_lib_secret));
        adapter.setDropDownViewResource(17367049);
        this.genderSpinner.setAdapter((SpinnerAdapter) adapter);
        this.genderSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                if (position == 0) {
                    MCLibUserSettingActivity.this.userInfo.setGender(MCLibConstants.ANDROID);
                } else if (position == 1) {
                    MCLibUserSettingActivity.this.userInfo.setGender("0");
                } else {
                    MCLibUserSettingActivity.this.userInfo.setGender("2");
                }
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
                MCLibUserSettingActivity.this.userInfo.setGender("2");
            }
        });
        if ("0".equals(this.userInfo.getGender())) {
            this.genderSpinner.setSelection(1, true);
        } else if (MCLibConstants.ANDROID.equals(this.userInfo.getGender())) {
            this.genderSpinner.setSelection(0, true);
        } else {
            this.genderSpinner.setSelection(2, true);
        }
    }

    private void initBirthdayWidget() {
        if (this.userInfo.getBirthday() == null || this.userInfo.getBirthday().trim().equals("")) {
            this.birthdayButton.setText((int) R.string.mc_lib_click_modify_birthday);
        } else {
            this.birthdayButton.setText(this.userInfo.getBirthday());
        }
        this.birthdayButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCLibUserSettingActivity.this.showBirthdayDialog();
            }
        });
    }

    /* access modifiers changed from: private */
    public void showBirthdayDialog() {
        String[] birthdayArray;
        String birthday = this.userInfo.getBirthday();
        int day = 1;
        int month = 0;
        int year = 1990;
        if (birthday != null && !birthday.equals("") && (birthdayArray = birthday.split("/")) != null && birthdayArray.length == 3) {
            year = Integer.parseInt(birthdayArray[0]);
            month = Integer.parseInt(birthdayArray[1]) - 1;
            day = Integer.parseInt(birthdayArray[2]);
        }
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        final MCLibDatePicker dateView = new MCLibDatePicker(this, year, month, day);
        alertDialog.setTitle((int) R.string.mc_lib_user_birthday);
        alertDialog.setView(dateView);
        alertDialog.setPositiveButton((int) R.string.mc_lib_confirm, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                String birthday = dateView.getDate();
                MCLibUserSettingActivity.this.birthdayButton.setText(birthday);
                MCLibUserSettingActivity.this.userInfo.setBirthday(birthday);
            }
        });
        alertDialog.show();
    }

    /* access modifiers changed from: private */
    public void hidePwdShowUserInfo() {
        this.basicSettingBox.setVisibility(0);
        this.pwdSettingBox.setVisibility(8);
    }

    /* access modifiers changed from: private */
    public void hideUserInfoShowPwd() {
        this.pwdSettingBox.setVisibility(0);
        this.basicSettingBox.setVisibility(8);
    }

    public Handler getCurrentHandler() {
        return this.mHandler;
    }

    public class ChangeUserPhotoOnClickListener implements View.OnClickListener {
        public ChangeUserPhotoOnClickListener() {
        }

        public void onClick(View v) {
            MCLibUserSettingActivity.this.userPhotoImage.showContextMenu();
        }
    }

    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case MCLibUpdateUserPhotoTypes.TAKE_PHOTO:
                this.imageType = this.TAKE_PHOTO;
                File cameroFile = new File(MCLibImageLoader.getImageCacheBasePath() + MCLibIOUtil.FS + MOB_CAMERA_FILE_NAME);
                if (!cameroFile.getParentFile().exists()) {
                    cameroFile.mkdirs();
                }
                Intent cameraIntent = new Intent("android.media.action.IMAGE_CAPTURE");
                cameraIntent.putExtra("output", Uri.fromFile(cameroFile));
                startActivityForResult(cameraIntent, 1);
                return true;
            case MCLibUpdateUserPhotoTypes.SELECT_LOCAL_PHOTO:
                this.imageType = this.SELECT_LOCAL_PHOTO;
                File file = new File(MCLibImageLoader.getImageCacheBasePath() + MCLibIOUtil.FS + MOB_SELECTED_FILE_NAME);
                if (!file.getParentFile().exists()) {
                    file.mkdirs();
                }
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction("android.intent.action.GET_CONTENT");
                startActivityForResult(intent, 1);
                return true;
            case MCLibUpdateUserPhotoTypes.SELECT_PACKAGED_PHOTO:
                MCLibUserPhotoSelectorDialog dialog = new MCLibUserPhotoSelectorDialog(this, R.style.mc_lib_dialog, new MCLibUpdateUserPhotoDelegateImpl(this, this.mHandler, this.userPhotoImage), this.mHandler);
                dialog.requestWindowFeature(1);
                dialog.show();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String path = "";
        if (this.imageType == this.TAKE_PHOTO) {
            path = MCLibImageLoader.getImageCacheBasePath() + MCLibIOUtil.FS + MOB_CAMERA_FILE_NAME;
            if (!new File(path).exists()) {
                if (data == null || data.getExtras() == null) {
                    showMsg(R.string.mc_lib_invalid_image_type);
                    return;
                }
                Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                if (!MCLibIOUtil.saveImageFile(this, bitmap, Bitmap.CompressFormat.JPEG, MOB_CAMERA_FILE_NAME, MCLibImageLoader.getImageCacheBasePath())) {
                    showMsg(R.string.mc_lib_invalid_image_type);
                    return;
                } else if (bitmap != null) {
                    bitmap.recycle();
                }
            }
        } else if (this.imageType == this.SELECT_LOCAL_PHOTO) {
            if (resultCode == -1) {
                try {
                    Bitmap bitmap2 = BitmapFactory.decodeStream(getContentResolver().openInputStream(data.getData()));
                    path = MCLibImageLoader.getImageCacheBasePath() + MCLibIOUtil.FS + MOB_SELECTED_FILE_NAME;
                    if (!MCLibIOUtil.saveImageFile(this, bitmap2, Bitmap.CompressFormat.JPEG, MOB_SELECTED_FILE_NAME, MCLibImageLoader.getImageCacheBasePath())) {
                        showMsg(R.string.mc_lib_invalid_image_type);
                        return;
                    } else if (bitmap2 != null) {
                        bitmap2.recycle();
                    }
                } catch (FileNotFoundException e) {
                    showMsg(R.string.mc_lib_invalid_image_type);
                    return;
                }
            } else {
                return;
            }
        }
        this.compressedImagePath = MCLibImageUtil.compressBitmap(path, 100, 100, COMPRESSED_IMAGE_IMAGE, 1, this);
        if (this.compressedImagePath == null || "".equals(this.compressedImagePath)) {
            cleanTempFiles();
        } else {
            showPreviewMaskPanel();
        }
    }

    /* access modifiers changed from: private */
    public void showMsg(final int msg) {
        this.mHandler.post(new Runnable() {
            public void run() {
                Toast.makeText(MCLibUserSettingActivity.this, msg, 1).show();
            }
        });
    }

    private void showPreviewMaskPanel() {
        this.mHandler.post(new Runnable() {
            public void run() {
                MCLibUserSettingActivity.this.maskLayer.setVisibility(0);
                MCLibUserSettingActivity.this.previewImage.setVisibility(0);
                MCLibUserSettingActivity.this.confirmPhotoBtn.setVisibility(0);
                MCLibUserSettingActivity.this.cancelPhotoBtn.setVisibility(0);
                MCLibUserSettingActivity.this.previewImage.setBackgroundDrawable(new BitmapDrawable(MCLibUserSettingActivity.this.getScaledBitmap(MCLibImageLoader.getBitmap(MCLibUserSettingActivity.this.compressedImagePath, 1), 0.5f)));
            }
        });
    }

    /* access modifiers changed from: private */
    public Bitmap getScaledBitmap(Bitmap mBitmap, float proposeWidthRatio) {
        if (mBitmap == null) {
            return null;
        }
        int proposeWidth = (int) (((float) MCLibPhoneUtil.getDisplayWidth(this)) * proposeWidthRatio);
        return Bitmap.createScaledBitmap(mBitmap, proposeWidth, (int) (((float) mBitmap.getHeight()) * (((float) proposeWidth) / ((float) mBitmap.getWidth()))), true);
    }

    private void cleanTempFiles() {
        File cameraFile = new File(MCLibImageLoader.getImageCacheBasePath() + MCLibIOUtil.FS + MOB_CAMERA_FILE_NAME);
        File selectFile = new File(MCLibImageLoader.getImageCacheBasePath() + MCLibIOUtil.FS + MOB_SELECTED_FILE_NAME);
        if (cameraFile.exists()) {
            cameraFile.delete();
        }
        if (selectFile.exists()) {
            selectFile.delete();
        }
        if (this.compressedImagePath != null && !"".equals(this.compressedImagePath)) {
            File compressFile = new File(this.compressedImagePath);
            if (compressFile.exists()) {
                compressFile.delete();
            }
        }
    }

    /* access modifiers changed from: private */
    public void setViewVisibility(final List<View> views, final int visibleValue) {
        this.mHandler.post(new Runnable() {
            public void run() {
                for (int i = 0; i < views.size(); i++) {
                    ((View) views.get(i)).setVisibility(visibleValue);
                }
            }
        });
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            cleanTempFiles();
        }
        return super.onKeyDown(keyCode, event);
    }

    /* access modifiers changed from: private */
    public void proceedUploadImage() {
        new Thread() {
            public void run() {
                MCLibUserSettingActivity.this.showMsg(R.string.mc_lib_image_uploading);
                MCLibUserInfoService uis = new MCLibUserInfoServiceImpl(MCLibUserSettingActivity.this);
                final MCLibUserInfo updateUserInfo = uis.uploadUserPhoto(uis.getLoginUserId(), MCLibUserSettingActivity.this.compressedImagePath);
                if (updateUserInfo.getErrorMsg() == null || "".equals(updateUserInfo.getErrorMsg())) {
                    MCLibUserSettingActivity.this.userInfo.setImage(updateUserInfo.getPhotoPath());
                    uis.updateSharedUserPhoto(updateUserInfo.getPhotoPath());
                    MCLibUserSettingActivity.this.showMsg(R.string.mc_lib_upload_succ);
                    MCLibUserSettingActivity.this.mHandler.post(new Runnable() {
                        public void run() {
                            MCLibUserSettingActivity.this.userPhotoImage.setBackgroundDrawable(new BitmapDrawable(MCLibImageLoader.getBitmap(MCLibUserSettingActivity.this.compressedImagePath, 1)));
                        }
                    });
                    return;
                }
                MCLibUserSettingActivity.this.mHandler.post(new Runnable() {
                    public void run() {
                        Toast.makeText(MCLibUserSettingActivity.this, updateUserInfo.getErrorMsg(), 1).show();
                    }
                });
            }
        }.start();
    }
}
