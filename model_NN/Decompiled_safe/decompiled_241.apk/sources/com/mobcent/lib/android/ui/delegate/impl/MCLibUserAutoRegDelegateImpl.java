package com.mobcent.lib.android.ui.delegate.impl;

import android.app.Activity;
import android.content.Intent;
import com.mobcent.android.constants.MCLibParameterKeyConstant;
import com.mobcent.android.model.MCLibUserInfo;
import com.mobcent.android.os.service.helper.MCLibDeveloperServiceHelper;
import com.mobcent.android.seed.autovGhnzo98NIzLsRPLHS.R;
import com.mobcent.android.service.impl.MCLibUserInfoServiceImpl;
import com.mobcent.android.service.impl.MCLibUserPrivateMsgServiceImpl;
import com.mobcent.lib.android.constants.MCLibQuickMsgConstant;
import com.mobcent.lib.android.ui.activity.MCLibGuideBasicActivity;
import com.mobcent.lib.android.ui.activity.model.MCLibGoToActivityModel;
import com.mobcent.lib.android.ui.delegate.MCLibUserActionDelegate;
import com.mobcent.lib.android.ui.dialog.MCLibRegLoginDialog;
import java.util.List;

public class MCLibUserAutoRegDelegateImpl implements MCLibUserActionDelegate {
    /* access modifiers changed from: private */
    public Activity activity;
    /* access modifiers changed from: private */
    public Class<?> goToActicityClass;
    /* access modifiers changed from: private */
    public boolean isStayOnCurrentActivity;
    /* access modifiers changed from: private */
    public MCLibRegLoginDialog regDialog;

    public MCLibUserAutoRegDelegateImpl(Activity activity2, MCLibRegLoginDialog regDialog2, boolean isStayOnCurrentActivity2, Class<?> goToActicityClass2) {
        this.activity = activity2;
        this.regDialog = regDialog2;
        this.isStayOnCurrentActivity = isStayOnCurrentActivity2;
        this.goToActicityClass = goToActicityClass2;
    }

    public void doUserAction() {
        doUserAutoReg();
    }

    private void doUserAutoReg() {
        this.regDialog.showWaitingPrg();
        new Thread() {
            public void run() {
                MCLibUserInfo user = new MCLibUserInfoServiceImpl(MCLibUserAutoRegDelegateImpl.this.activity).regUser();
                if (user.getUid() > 0) {
                    MCLibDeveloperServiceHelper.onAppLaunched(R.raw.mc_lib_msg, MCLibUserAutoRegDelegateImpl.this.activity);
                    if (!MCLibUserAutoRegDelegateImpl.this.isStayOnCurrentActivity) {
                        List<String> quickMsgList = MCLibQuickMsgConstant.getMCQuickMsgConstant(MCLibUserAutoRegDelegateImpl.this.activity).getQuickMsgList();
                        MCLibUserPrivateMsgServiceImpl upms = new MCLibUserPrivateMsgServiceImpl(MCLibUserAutoRegDelegateImpl.this.activity);
                        for (String quickMsg : quickMsgList) {
                            upms.addUserCustomizedQuickMsg(user.getUid(), quickMsg);
                        }
                        Intent intent = new Intent(MCLibUserAutoRegDelegateImpl.this.activity, MCLibGuideBasicActivity.class);
                        if (MCLibUserAutoRegDelegateImpl.this.goToActicityClass != null) {
                            intent.putExtra(MCLibParameterKeyConstant.GO_TO_ACTIVITY_CLASS, new MCLibGoToActivityModel(MCLibUserAutoRegDelegateImpl.this.goToActicityClass));
                        }
                        MCLibUserAutoRegDelegateImpl.this.activity.startActivity(intent);
                    }
                    MCLibUserAutoRegDelegateImpl.this.regDialog.dismiss();
                    return;
                }
                MCLibUserAutoRegDelegateImpl.this.regDialog.hideWaitingPrg();
                MCLibUserAutoRegDelegateImpl.this.regDialog.showLoginErrorMsg(R.string.mc_lib_generic_error);
            }
        }.start();
    }
}
