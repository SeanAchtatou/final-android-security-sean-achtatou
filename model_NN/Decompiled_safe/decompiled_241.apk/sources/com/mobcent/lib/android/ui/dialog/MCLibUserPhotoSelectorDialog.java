package com.mobcent.lib.android.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListAdapter;
import com.mobcent.android.seed.autovGhnzo98NIzLsRPLHS.R;
import com.mobcent.lib.android.constants.MCLibResourceConstant;
import com.mobcent.lib.android.ui.delegate.impl.MCLibUpdateUserPhotoDelegateImpl;
import com.mobcent.lib.android.ui.dialog.adapter.MCLibPhotoGridViewAdapter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MCLibUserPhotoSelectorDialog extends Dialog {
    public static final int LOCAL_PHOTO = 1;
    public static final int REMOTE_PHOTO = 0;
    private Button backText;
    /* access modifiers changed from: private */
    public Context context;
    /* access modifiers changed from: private */
    public MCLibUpdateUserPhotoDelegateImpl delegate;
    /* access modifiers changed from: private */
    public Button femaleItem;
    private Handler mHandler;
    /* access modifiers changed from: private */
    public Button maleItem;
    /* access modifiers changed from: private */
    public GridView photoGridView;

    public MCLibUserPhotoSelectorDialog(Context context2, int theme, MCLibUpdateUserPhotoDelegateImpl delegate2, Handler mHandler2) {
        super(context2, theme);
        this.context = context2;
        this.delegate = delegate2;
        this.mHandler = mHandler2;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.mc_lib_dialog_user_photo);
        this.maleItem = (Button) findViewById(R.id.mcLibMaleItem);
        this.femaleItem = (Button) findViewById(R.id.mcLibFemaleItem);
        this.backText = (Button) findViewById(R.id.mcLibBackBtn);
        this.photoGridView = (GridView) findViewById(R.id.mcLibUserPhotoGridView);
        this.backText.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCLibUserPhotoSelectorDialog.this.dismiss();
            }
        });
        initMenu();
        updateGridView(1);
    }

    /* access modifiers changed from: private */
    public void updateGridView(final int gender) {
        this.mHandler.post(new Runnable() {
            public void run() {
                HashMap<String, Integer> genderMap;
                ArrayList<HashMap<String, Integer>> userPhotoList = new ArrayList<>();
                if (gender == 1) {
                    genderMap = MCLibResourceConstant.getMCResourceConstant().getMaleResourceMap();
                } else {
                    genderMap = MCLibResourceConstant.getMCResourceConstant().getFemaleResourceMap();
                }
                for (Map.Entry<String, Integer> entry : genderMap.entrySet()) {
                    HashMap<String, Integer> hashMap = new HashMap<>();
                    hashMap.put(entry.getKey(), entry.getValue());
                    userPhotoList.add(hashMap);
                }
                MCLibUserPhotoSelectorDialog.this.photoGridView.setAdapter((ListAdapter) new MCLibPhotoGridViewAdapter(MCLibUserPhotoSelectorDialog.this.context, userPhotoList, R.layout.mc_lib_dialog_user_photo_grid_item, new String[0], new int[0], MCLibUserPhotoSelectorDialog.this.delegate, MCLibUserPhotoSelectorDialog.this));
            }
        });
    }

    private void initMenu() {
        initMaleItem();
        initFemaleItem();
    }

    private void initMaleItem() {
        this.maleItem.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                MCLibUserPhotoSelectorDialog.this.maleItem.setBackgroundResource(R.drawable.mc_lib_boy_h);
                MCLibUserPhotoSelectorDialog.this.femaleItem.setBackgroundResource(R.drawable.mc_lib_girl_n);
                MCLibUserPhotoSelectorDialog.this.updateGridView(1);
            }
        });
    }

    private void initFemaleItem() {
        this.femaleItem.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                MCLibUserPhotoSelectorDialog.this.maleItem.setBackgroundResource(R.drawable.mc_lib_boy_n);
                MCLibUserPhotoSelectorDialog.this.femaleItem.setBackgroundResource(R.drawable.mc_lib_girl_h);
                MCLibUserPhotoSelectorDialog.this.updateGridView(0);
            }
        });
    }
}
