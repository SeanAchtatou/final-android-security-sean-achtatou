package com.mobcent.lib.android.ui.activity.listener;

import android.view.View;
import android.widget.AdapterView;

public class MCLibListViewItemOnClickListener implements AdapterView.OnItemClickListener {
    public void onItemClick(AdapterView<?> adapterView, View view, int arg2, long arg3) {
        view.performClick();
    }
}
