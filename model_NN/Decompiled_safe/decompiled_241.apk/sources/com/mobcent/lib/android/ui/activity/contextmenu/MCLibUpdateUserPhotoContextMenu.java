package com.mobcent.lib.android.ui.activity.contextmenu;

import android.view.ContextMenu;
import android.view.View;
import com.mobcent.android.seed.autovGhnzo98NIzLsRPLHS.R;
import com.mobcent.lib.android.constants.MCLibUpdateUserPhotoTypes;

public class MCLibUpdateUserPhotoContextMenu implements View.OnCreateContextMenuListener, MCLibUpdateUserPhotoTypes {
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        menu.add(0, (int) MCLibUpdateUserPhotoTypes.TAKE_PHOTO, 1, (int) R.string.mc_lib_take_photo);
        menu.add(0, (int) MCLibUpdateUserPhotoTypes.SELECT_LOCAL_PHOTO, 2, (int) R.string.mc_lib_select_local_photo);
        menu.add(0, (int) MCLibUpdateUserPhotoTypes.SELECT_PACKAGED_PHOTO, 3, (int) R.string.mc_lib_select_packaged_photo);
    }
}
