package com.mobcent.lib.android.ui.delegate.impl;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;
import com.mobcent.android.model.MCLibUserStatus;
import com.mobcent.android.os.service.MCLibUserPublishQueueService;
import com.mobcent.android.seed.autovGhnzo98NIzLsRPLHS.R;
import com.mobcent.android.service.MCLibStatusService;
import com.mobcent.android.service.MCLibUserInfoService;
import com.mobcent.android.service.impl.MCLibStatusServiceImpl;
import com.mobcent.android.service.impl.MCLibUserInfoServiceImpl;
import com.mobcent.lib.android.ui.delegate.MCLibPublishWordsDelegate;
import com.mobcent.lib.android.ui.dialog.MCLibPublishWordsDialog;

public class MCLibSendFeedbackDelegateImpl implements MCLibPublishWordsDelegate {
    private Context context;
    private MCLibPublishWordsDialog dialog;

    public MCLibSendFeedbackDelegateImpl(Context context2) {
        this.context = context2;
    }

    public void publishWords(String content) {
        MCLibUserInfoService userInfoService = new MCLibUserInfoServiceImpl(this.context);
        MCLibStatusService statusService = new MCLibStatusServiceImpl(this.context);
        MCLibUserStatus userStatus = new MCLibUserStatus();
        userStatus.setUid(userInfoService.getLoginUserId());
        userStatus.setContent(content);
        userStatus.setCommunicationType(15);
        if (statusService.putStatusInQueue(userStatus)) {
            this.context.startService(new Intent(this.context, MCLibUserPublishQueueService.class));
            this.dialog.dismiss();
            return;
        }
        Toast.makeText(this.context, (int) R.string.mc_lib_pub_failed, 1).show();
    }

    public void setDialog(MCLibPublishWordsDialog dialog2) {
        this.dialog = dialog2;
    }
}
