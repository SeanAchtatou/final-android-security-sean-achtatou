package com.mobcent.lib.android.ui.dialog;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import com.mobcent.android.seed.autovGhnzo98NIzLsRPLHS.R;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class MCLibDatePicker extends LinearLayout {
    private static Context mContext;
    private int day = 1;
    /* access modifiers changed from: private */
    public Calendar mCalender;
    private DatePicker mDatePicker;
    private int month = 1;
    /* access modifiers changed from: private */
    public DatePicker.OnDateChangedListener onDateChangedListener = new DatePicker.OnDateChangedListener() {
        public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            boolean mIsLeapYear = ((GregorianCalendar) MCLibDatePicker.this.mCalender).isLeapYear(year);
            if (mIsLeapYear && monthOfYear == 1 && dayOfMonth > 29) {
                view.init(year, monthOfYear, 29, MCLibDatePicker.this.onDateChangedListener);
            } else if (mIsLeapYear && monthOfYear == 1) {
                view.init(year, monthOfYear, dayOfMonth, MCLibDatePicker.this.onDateChangedListener);
            } else if (!mIsLeapYear && monthOfYear == 1 && dayOfMonth > 28) {
                view.init(year, monthOfYear, 28, MCLibDatePicker.this.onDateChangedListener);
            } else if (!mIsLeapYear && monthOfYear != 1 && (monthOfYear + 1) % 2 == 0 && dayOfMonth > 30) {
                view.init(year, monthOfYear, 30, MCLibDatePicker.this.onDateChangedListener);
            } else if (monthOfYear != 1 && (monthOfYear + 1) % 2 == 0 && dayOfMonth > 30) {
                view.init(year, monthOfYear, 30, MCLibDatePicker.this.onDateChangedListener);
            }
        }
    };
    private int year = 1990;

    public MCLibDatePicker(Context context, int year2, int month2, int day2) {
        super(context);
        this.year = year2;
        this.month = month2;
        this.day = day2;
        initialize(context);
    }

    public MCLibDatePicker(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize(context);
    }

    private void initialize(Context context) {
        this.mCalender = Calendar.getInstance();
        mContext = context;
        View view = LayoutInflater.from(mContext).inflate((int) R.layout.mc_lib_dialog_date_picker, (ViewGroup) null);
        this.mDatePicker = (DatePicker) view.findViewById(R.id.mcLibDatePicker);
        this.mDatePicker.init(this.year, this.month, this.day, this.onDateChangedListener);
        addView(view);
    }

    public String getDate() {
        return this.mDatePicker.getYear() + "/" + (this.mDatePicker.getMonth() + 1) + "/" + this.mDatePicker.getDayOfMonth();
    }
}
