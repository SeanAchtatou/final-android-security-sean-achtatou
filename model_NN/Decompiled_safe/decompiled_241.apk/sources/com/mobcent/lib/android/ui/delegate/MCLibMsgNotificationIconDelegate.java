package com.mobcent.lib.android.ui.delegate;

import android.os.Handler;

public interface MCLibMsgNotificationIconDelegate {
    void headbeatNotificationBox(Handler handler);

    void hideMsgNotificationBox(Handler handler);

    void showMsgNotificationBox(Handler handler);

    void updateCurrentIconHasReply(Handler handler);

    void updateCurrentIconMsg(int i, Handler handler);

    void updateCurrentIconSysMsg(int i, Handler handler);
}
