package com.mobcent.lib.android.ui.activity.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.mobcent.android.model.MCLibDownloadProfileModel;
import com.mobcent.android.service.impl.MCLibAppDownloaderServiceImpl;
import com.mobcent.lib.android.ui.activity.adapter.MCLibDownloadManagerListAdapter;
import java.util.ArrayList;
import java.util.List;

public class MCLibUpdateDownloadStatusReceiver extends BroadcastReceiver {
    private MCLibDownloadManagerListAdapter adapter;

    public MCLibDownloadManagerListAdapter getAdapter() {
        return this.adapter;
    }

    public void setAdapter(MCLibDownloadManagerListAdapter adapter2) {
        this.adapter = adapter2;
    }

    public MCLibUpdateDownloadStatusReceiver() {
    }

    public MCLibUpdateDownloadStatusReceiver(MCLibDownloadManagerListAdapter adapter2) {
        this.adapter = adapter2;
    }

    public void onReceive(Context context, Intent intent) {
        if (this.adapter != null) {
            List<MCLibDownloadProfileModel> allDownloadList = new MCLibAppDownloaderServiceImpl(context).getAllDownloadAppList();
            List<MCLibDownloadProfileModel> allModels = new ArrayList<>();
            allModels.addAll(allDownloadList);
            this.adapter.setDownloadModels(allModels);
            this.adapter.notifyDataSetInvalidated();
            this.adapter.notifyDataSetChanged();
        }
    }
}
