package com.mobcent.lib.android.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.mobcent.android.seed.autovGhnzo98NIzLsRPLHS.R;
import com.mobcent.lib.android.ui.delegate.MCLibPublishWordsDelegate;
import com.mobcent.lib.android.utils.i18n.MCLibStringBundleUtil;

public class MCLibPublishWordsDialog extends Dialog {
    /* access modifiers changed from: private */
    public MCLibPublishWordsDelegate delegate;
    private int hintText;
    private int title;
    /* access modifiers changed from: private */
    public int upperLimit;

    public MCLibPublishWordsDialog(Context context, int title2, MCLibPublishWordsDelegate delegate2, int upperLimit2) {
        super(context);
        this.title = title2;
        this.delegate = delegate2;
        this.upperLimit = upperLimit2;
    }

    public MCLibPublishWordsDialog(Context context, int title2, MCLibPublishWordsDelegate delegate2, int hintText2, int upperLimit2) {
        super(context);
        this.title = title2;
        this.delegate = delegate2;
        this.hintText = hintText2;
        this.upperLimit = upperLimit2;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.mc_lib_dialog_publish_words);
        setTitle(this.title);
        final EditText text = (EditText) findViewById(R.id.mcLibContentDialogText);
        Button buttonYes = (Button) findViewById(R.id.mcLibDialogYesButton);
        TextView hintTextView = (TextView) findViewById(R.id.mcLibDialogHint);
        if (this.hintText != 0) {
            hintTextView.setText(this.hintText);
        }
        final TextView upperLimitText = (TextView) findViewById(R.id.mcLibWordsUpperLimit);
        upperLimitText.setText(MCLibStringBundleUtil.resolveString(R.string.mc_lib_words_left, new String[]{this.upperLimit + ""}, getContext()));
        final Button buttonNo = (Button) findViewById(R.id.mcLibDialogNoButton);
        buttonYes.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String content = text.getText().toString();
                if (content == null || content.equals("")) {
                    text.setHint((int) R.string.mc_lib_say_something);
                } else if (MCLibPublishWordsDialog.this.upperLimit - text.getText().length() < 0) {
                    upperLimitText.setText(MCLibStringBundleUtil.resolveString(R.string.mc_lib_publish_words_tip, new String[]{MCLibPublishWordsDialog.this.upperLimit + ""}, MCLibPublishWordsDialog.this.getContext()));
                } else {
                    v.setVisibility(4);
                    buttonNo.setVisibility(4);
                    MCLibPublishWordsDialog.this.findViewById(R.id.mcLibPubProgressBar).setVisibility(0);
                    MCLibPublishWordsDialog.this.delegate.publishWords(content);
                }
            }
        });
        buttonNo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCLibPublishWordsDialog.this.dismiss();
            }
        });
        text.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void afterTextChanged(Editable s) {
                int wordsLeft = MCLibPublishWordsDialog.this.upperLimit - text.getText().length();
                if (wordsLeft < 0) {
                    upperLimitText.setTextColor(MCLibPublishWordsDialog.this.getContext().getResources().getColor(R.color.mc_lib_red));
                } else {
                    upperLimitText.setTextColor(MCLibPublishWordsDialog.this.getContext().getResources().getColor(R.color.mc_lib_white));
                }
                upperLimitText.setText(MCLibStringBundleUtil.resolveString(R.string.mc_lib_words_left, new String[]{wordsLeft + ""}, MCLibPublishWordsDialog.this.getContext()));
            }
        });
    }
}
