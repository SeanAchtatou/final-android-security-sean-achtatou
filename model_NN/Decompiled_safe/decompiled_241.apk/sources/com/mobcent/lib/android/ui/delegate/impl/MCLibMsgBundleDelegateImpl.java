package com.mobcent.lib.android.ui.delegate.impl;

import android.content.Context;
import com.mobcent.android.seed.autovGhnzo98NIzLsRPLHS.R;
import com.mobcent.android.service.delegate.MCLibMsgBundleDelegate;

public class MCLibMsgBundleDelegateImpl implements MCLibMsgBundleDelegate {
    private Context context;

    public MCLibMsgBundleDelegateImpl(Context context2) {
        this.context = context2;
    }

    public String getPublishSuccMsg() {
        return this.context.getResources().getString(R.string.mc_lib_pub_succ);
    }

    public String getPublishFailMsg() {
        return this.context.getResources().getString(R.string.mc_lib_pub_failed);
    }

    public String getPublishProhibitionMsg() {
        return this.context.getResources().getString(R.string.mc_lib_prohibit_publish);
    }
}
