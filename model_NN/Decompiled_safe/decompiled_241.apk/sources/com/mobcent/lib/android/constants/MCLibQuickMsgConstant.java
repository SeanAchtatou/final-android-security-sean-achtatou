package com.mobcent.lib.android.constants;

import android.content.Context;
import com.mobcent.android.seed.autovGhnzo98NIzLsRPLHS.R;
import java.util.ArrayList;
import java.util.List;

public class MCLibQuickMsgConstant {
    private static MCLibQuickMsgConstant mcLibQuickMsgList;
    private static List<String> quickMsgList = new ArrayList();

    private MCLibQuickMsgConstant(Context context) {
        quickMsgList.add(context.getResources().getString(R.string.mc_lib_quick_msg_hello));
        quickMsgList.add(context.getResources().getString(R.string.mc_lib_quick_msg_busy));
    }

    public static MCLibQuickMsgConstant getMCQuickMsgConstant(Context context) {
        if (mcLibQuickMsgList == null) {
            mcLibQuickMsgList = new MCLibQuickMsgConstant(context);
        }
        return mcLibQuickMsgList;
    }

    public List<String> getQuickMsgList() {
        return quickMsgList;
    }
}
