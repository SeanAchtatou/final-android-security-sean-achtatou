package com.mobcent.lib.android.utils;

import android.content.Context;
import android.os.Vibrator;

public class MCLibPhoneVibratorUtil {
    public static void shotVibratePhone(Context context) {
        ((Vibrator) context.getSystemService("vibrator")).vibrate(new long[]{800, 50, 400, 30}, -1);
    }
}
