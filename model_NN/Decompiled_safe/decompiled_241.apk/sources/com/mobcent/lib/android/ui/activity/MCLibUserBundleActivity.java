package com.mobcent.lib.android.ui.activity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;
import com.mobcent.android.model.MCLibUserInfo;
import com.mobcent.android.seed.autovGhnzo98NIzLsRPLHS.R;
import com.mobcent.android.service.MCLibUserInfoService;
import com.mobcent.android.service.impl.MCLibUserInfoServiceImpl;
import com.mobcent.lib.android.ui.activity.adapter.MCLibUserInfoAdapter;
import com.mobcent.lib.android.ui.activity.cache.MCLibUserCache;
import com.mobcent.lib.android.ui.activity.listener.MCLibListViewItemOnClickListener;
import com.mobcent.lib.android.ui.activity.menu.MCLibUserMainBar;
import com.mobcent.lib.android.ui.delegate.MCLibUpdateListDelegate;
import com.mobcent.lib.android.utils.MCLibDateUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class MCLibUserBundleActivity extends MCLibUIBaseActivity implements MCLibUpdateListDelegate {
    public static final int TAB_FANS = 101;
    public static final int TAB_FRIENDS = 100;
    public static final int TAB_RECOMMEND_FRIENDS = 103;
    public static final int TAB_SEARCH_FRIENDS = 102;
    /* access modifiers changed from: private */
    public MCLibUserInfoAdapter adapter;
    /* access modifiers changed from: private */
    public String keyWord = "";
    /* access modifiers changed from: private */
    public Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            if (msg.what == 1 || msg.what == 5) {
                MCLibUserBundleActivity.this.updateEmptyAdapter(msg.obj);
            } else if (msg.what == 2) {
                MCLibUserBundleActivity.this.updateExistAdapter(msg.obj);
            } else if (msg.what == 3) {
                MCLibUserBundleActivity.this.hideProgressBar();
            } else if (msg.what == 4) {
                MCLibUserBundleActivity.this.showProgressBar();
            }
        }
    };
    /* access modifiers changed from: private */
    public MCLibUpdateListDelegate menuUpdateUserListDelegate = new MCLibUpdateListDelegate() {
        public void updateList(final int page, int pageSize, boolean isReadLocally) {
            MCLibUserBundleActivity.this.mHandler.sendMessage(MCLibUserBundleActivity.this.mHandler.obtainMessage(5, new ArrayList()));
            new Thread() {
                public void run() {
                    try {
                        MCLibUserBundleActivity.this.mHandler.sendEmptyMessage(4);
                        List<MCLibUserInfo> userList = new ArrayList<>();
                        if (MCLibUserBundleActivity.this.tabType == 100) {
                            if (MCLibUserCache.myFriendsList == null || MCLibUserCache.myFriendsList.isEmpty()) {
                                userList = MCLibUserBundleActivity.this.getUserFriends(MCLibUserBundleActivity.this.tabType, page, 10);
                            } else {
                                userList.addAll(MCLibUserCache.myFriendsList);
                                int totalNum = ((MCLibUserInfo) userList.get(0)).getTotalNum();
                                if (userList.size() > 10) {
                                    userList.add(MCLibUserBundleActivity.this.getRefreshModel(totalNum, page, MCLibUserCache.lastRefreshFriendsListTime));
                                }
                                userList.add(0, MCLibUserBundleActivity.this.getRefreshModel(totalNum, page, MCLibUserCache.lastRefreshFriendsListTime));
                            }
                        } else if (MCLibUserBundleActivity.this.tabType == 101) {
                            if (MCLibUserCache.myFansList == null || MCLibUserCache.myFansList.isEmpty()) {
                                userList = MCLibUserBundleActivity.this.getUserFriends(MCLibUserBundleActivity.this.tabType, page, 10);
                            } else {
                                userList.addAll(MCLibUserCache.myFansList);
                                int totalNum2 = ((MCLibUserInfo) userList.get(0)).getTotalNum();
                                if (userList.size() > 10) {
                                    userList.add(MCLibUserBundleActivity.this.getRefreshModel(totalNum2, page, MCLibUserCache.lastRefreshFansListTime));
                                }
                                userList.add(0, MCLibUserBundleActivity.this.getRefreshModel(totalNum2, page, MCLibUserCache.lastRefreshFansListTime));
                            }
                        } else if (MCLibUserBundleActivity.this.tabType != 103) {
                            userList = MCLibUserBundleActivity.this.getUserFriends(MCLibUserBundleActivity.this.tabType, page, 10);
                        } else if (MCLibUserCache.recommendedUserList == null || MCLibUserCache.recommendedUserList.isEmpty()) {
                            userList = MCLibUserBundleActivity.this.getUserFriends(MCLibUserBundleActivity.this.tabType, page, 10);
                        } else {
                            userList.addAll(MCLibUserCache.recommendedUserList);
                            int totalNum3 = ((MCLibUserInfo) userList.get(0)).getTotalNum();
                            if (userList.size() > 10) {
                                userList.add(MCLibUserBundleActivity.this.getRefreshModel(totalNum3, page, MCLibUserCache.lastRefreshRecommendedUserListTime));
                            }
                            userList.add(0, MCLibUserBundleActivity.this.getRefreshModel(totalNum3, page, MCLibUserCache.lastRefreshRecommendedUserListTime));
                        }
                        if (userList == null || userList.size() <= 0) {
                            MCLibUserBundleActivity.this.showEmptyInfoTip(R.string.mc_lib_no_such_data);
                            return;
                        }
                        MCLibUserBundleActivity.this.updateUserListView(userList, true);
                        MCLibUserBundleActivity.this.mHandler.sendEmptyMessage(3);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }.start();
        }
    };
    private Button searchButton;
    /* access modifiers changed from: private */
    public EditText searchUserEditText;
    /* access modifiers changed from: private */
    public int tabType = 100;
    protected List<MCLibUserInfo> userInfoList;
    protected AbsListView.OnScrollListener userListOnScrollListener = new AbsListView.OnScrollListener() {
        private Vector<String> currentUrls;
        private int firstVisibleItem;
        private int totalItemCount;
        private int visibleItemCount;

        public void onScrollStateChanged(AbsListView view, int scrollState) {
            if (scrollState == 0) {
                List<String> imageUrls = new ArrayList<>();
                parseCurrentUrls();
                List<String> preUrls = getPreviousUrls();
                List<String> nextUrls = getNextImageUrls();
                if (preUrls != null) {
                    imageUrls.addAll(preUrls);
                }
                if (nextUrls != null) {
                    imageUrls.addAll(nextUrls);
                }
                MCLibUserBundleActivity.this.adapter.asyncImageLoader.recycleBitmap(imageUrls);
            }
        }

        public void onScroll(AbsListView view, int firstVisibleItem2, int visibleItemCount2, int totalItemCount2) {
            this.firstVisibleItem = firstVisibleItem2;
            this.visibleItemCount = visibleItemCount2;
            this.totalItemCount = totalItemCount2;
        }

        private void parseCurrentUrls() {
            this.currentUrls = new Vector<>();
            int firstIndex = 0;
            int endIndex = this.firstVisibleItem + this.visibleItemCount + 10;
            if (this.firstVisibleItem - 10 > 0) {
                firstIndex = this.firstVisibleItem - 10;
            }
            if (endIndex >= this.totalItemCount) {
                endIndex = this.totalItemCount;
            }
            for (int i = firstIndex; i < endIndex; i++) {
                if (MCLibUserBundleActivity.this.userInfoList.get(i).getImage() != null && !MCLibUserBundleActivity.this.userInfoList.get(i).getImage().equals("")) {
                    this.currentUrls.add(MCLibUserBundleActivity.this.userInfoList.get(i).getImage());
                }
            }
        }

        private List<String> getPreviousUrls() {
            if (this.firstVisibleItem - 10 <= 0) {
                return null;
            }
            List<String> urls = new ArrayList<>();
            for (int i = 0; i < this.firstVisibleItem - 10; i++) {
                if (MCLibUserBundleActivity.this.userInfoList.get(i).getImage() != null && !MCLibUserBundleActivity.this.userInfoList.get(i).getImage().equals("") && !this.currentUrls.contains(MCLibUserBundleActivity.this.userInfoList.get(i).getImage())) {
                    urls.add(MCLibUserBundleActivity.this.userInfoList.get(i).getImage());
                }
            }
            return urls;
        }

        private List<String> getNextImageUrls() {
            int currentVisibleBottom = this.firstVisibleItem + this.visibleItemCount;
            if (this.totalItemCount - currentVisibleBottom <= 10) {
                return null;
            }
            List<String> urls = new ArrayList<>();
            for (int i = currentVisibleBottom + 10; i < this.totalItemCount; i++) {
                if (MCLibUserBundleActivity.this.userInfoList.get(i).getImage() != null && !MCLibUserBundleActivity.this.userInfoList.get(i).getImage().equals("") && !this.currentUrls.contains(MCLibUserBundleActivity.this.userInfoList.get(i).getImage())) {
                    urls.add(MCLibUserBundleActivity.this.userInfoList.get(i).getImage());
                }
            }
            return urls;
        }
    };
    private ListView userListView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.mc_lib_user_list_bundled);
        initWindowTitle();
        initSysMsgWidgets();
        this.userListView = (ListView) findViewById(R.id.mcLibUserBundledListView);
        this.userListView.setOnScrollListener(this.userListOnScrollListener);
        this.userListView.setOnItemClickListener(new MCLibListViewItemOnClickListener());
        initProgressBox();
        initLoginUserHeader();
        initSearchBox();
        initNavBottomBar(102);
        new MCLibUserMainBar().buildActionBar(this, this.menuUpdateUserListDelegate, Integer.valueOf(this.tabType));
    }

    private void initSearchBox() {
        this.searchButton = (Button) findViewById(R.id.mcLibSearchBtn);
        this.searchUserEditText = (EditText) findViewById(R.id.mcLibSearchUserEditText);
        this.searchButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String searchKeyword = MCLibUserBundleActivity.this.searchUserEditText.getText().toString();
                if (searchKeyword == null || "".equals(searchKeyword)) {
                    MCLibUserBundleActivity.this.mHandler.post(new Runnable() {
                        public void run() {
                            Toast.makeText(MCLibUserBundleActivity.this, (int) R.string.mc_lib_search_user_hint, 1).show();
                        }
                    });
                    return;
                }
                String unused = MCLibUserBundleActivity.this.keyWord = searchKeyword;
                int unused2 = MCLibUserBundleActivity.this.tabType = 102;
                ((InputMethodManager) MCLibUserBundleActivity.this.getSystemService("input_method")).hideSoftInputFromWindow(MCLibUserBundleActivity.this.searchUserEditText.getWindowToken(), 0);
                MCLibUserBundleActivity.this.menuUpdateUserListDelegate.updateList(1, 10, false);
            }
        });
    }

    /* access modifiers changed from: private */
    public void updateEmptyAdapter(Object obj) {
        this.adapter = new MCLibUserInfoAdapter(this, this.userListView, R.layout.mc_lib_user_list_row, (List) obj, this, this.mHandler);
        this.userListView.setAdapter((ListAdapter) this.adapter);
        this.userInfoList = (List) obj;
    }

    /* access modifiers changed from: private */
    public void updateExistAdapter(Object obj) {
        this.adapter.setUserInfoList((List) obj);
        this.adapter.notifyDataSetInvalidated();
        this.adapter.notifyDataSetChanged();
        this.userInfoList = (List) obj;
    }

    public void updateList(final int page, final int pageSize, boolean isReadLocally) {
        new Thread() {
            public void run() {
                List<MCLibUserInfo> resultList = new ArrayList<>();
                boolean isShowSearchBar = false;
                if (page == 1) {
                    MCLibUserBundleActivity.this.mHandler.sendEmptyMessage(4);
                    MCLibUserBundleActivity.this.mHandler.sendMessage(MCLibUserBundleActivity.this.mHandler.obtainMessage(5, resultList));
                    isShowSearchBar = true;
                    if (MCLibUserBundleActivity.this.userInfoList != null) {
                        MCLibUserBundleActivity.this.userInfoList.clear();
                    }
                }
                List<MCLibUserInfo> nextPageUserInfoList = MCLibUserBundleActivity.this.getUserFriends(MCLibUserBundleActivity.this.tabType, page, pageSize);
                if (nextPageUserInfoList != null && nextPageUserInfoList.size() > 0 && page == 1) {
                    resultList = nextPageUserInfoList;
                    MCLibUserBundleActivity.this.mHandler.sendEmptyMessage(3);
                } else if (nextPageUserInfoList != null && nextPageUserInfoList.size() > 0) {
                    resultList.addAll(MCLibUserBundleActivity.this.userInfoList);
                    resultList.remove(resultList.size() - 1);
                    resultList.addAll(nextPageUserInfoList);
                } else if (MCLibUserBundleActivity.this.userInfoList.size() > 0) {
                    resultList.addAll(MCLibUserBundleActivity.this.userInfoList);
                    resultList.remove(resultList.size() - 1);
                }
                MCLibUserBundleActivity.this.updateUserListView(resultList, isShowSearchBar);
            }
        }.start();
    }

    /* access modifiers changed from: private */
    public void updateUserListView(List<MCLibUserInfo> userList, boolean isShowSearchBar) {
        if (this.adapter == null) {
            this.mHandler.sendMessage(this.mHandler.obtainMessage(1, userList));
            return;
        }
        this.mHandler.sendMessage(this.mHandler.obtainMessage(2, userList));
    }

    public List<MCLibUserInfo> getUserFriends(int type, int page, int pageSize) {
        MCLibUserInfoService userInfoService = new MCLibUserInfoServiceImpl(this);
        int uid = userInfoService.getLoginUserId();
        if (type == 100) {
            List<MCLibUserInfo> friendsList = userInfoService.getFriendsUserFollowed(uid, page, pageSize);
            MCLibUserCache.addMyFriends(friendsList);
            MCLibUserCache.lastRefreshFriendsListTime = MCLibDateUtil.getGenericCurrentTime();
            return friendsList;
        } else if (type == 101) {
            List<MCLibUserInfo> fansList = userInfoService.getFriendsFollowedTheUser(uid, page, pageSize);
            MCLibUserCache.addFans(fansList);
            MCLibUserCache.lastRefreshFansListTime = MCLibDateUtil.getGenericCurrentTime();
            return fansList;
        } else if (type == 102) {
            return userInfoService.searchUsers(uid, "", this.keyWord, 2, page, pageSize);
        } else {
            if (type != 103) {
                return userInfoService.getFriendsFollowedTheUser(uid, page, pageSize);
            }
            List<MCLibUserInfo> recommendedUserList = userInfoService.getRecommendUsers(uid, page, pageSize);
            MCLibUserCache.addRecommendedUsers(recommendedUserList);
            MCLibUserCache.lastRefreshRecommendedUserListTime = MCLibDateUtil.getGenericCurrentTime();
            return recommendedUserList;
        }
    }

    public int getTabType() {
        return this.tabType;
    }

    public void setTabType(int tabType2) {
        this.tabType = tabType2;
    }

    public class SearchFriendUpdateListDelegate implements MCLibUpdateListDelegate {
        public SearchFriendUpdateListDelegate() {
        }

        public void updateList(final int page, final int pageSize, boolean isReadLocally) {
            MCLibUserBundleActivity.this.mHandler.sendEmptyMessage(4);
            MCLibUserBundleActivity.this.mHandler.sendMessage(MCLibUserBundleActivity.this.mHandler.obtainMessage(5, new ArrayList()));
            new Thread() {
                public void run() {
                    try {
                        List<MCLibUserInfo> userList = MCLibUserBundleActivity.this.getUserFriends(102, page, pageSize);
                        MCLibUserBundleActivity.this.mHandler.sendEmptyMessage(3);
                        if (userList == null || userList.size() <= 0) {
                            MCLibUserBundleActivity.this.showEmptyInfoTip(R.string.mc_lib_no_such_data);
                        } else {
                            MCLibUserBundleActivity.this.updateUserListView(userList, true);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }.start();
        }

        public void updateKeyword(String keyWord) {
            String unused = MCLibUserBundleActivity.this.keyWord = keyWord;
        }
    }

    /* access modifiers changed from: private */
    public MCLibUserInfo getRefreshModel(int totalNum, int page, String time) {
        MCLibUserInfo refreshModel = new MCLibUserInfo();
        refreshModel.setRefreshNeeded(true);
        refreshModel.setReadFromLocal(true);
        refreshModel.setLastUpdateTime(time);
        refreshModel.setTotalNum(totalNum);
        refreshModel.setCurrentPage(page);
        return refreshModel;
    }

    public Handler getCurrentHandler() {
        return this.mHandler;
    }
}
