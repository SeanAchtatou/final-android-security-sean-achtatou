package com.mobcent.lib.android.ui.activity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.mobcent.android.model.MCLibUserInfo;
import com.mobcent.android.seed.autovGhnzo98NIzLsRPLHS.R;
import com.mobcent.android.service.MCLibUserInfoService;
import com.mobcent.android.service.impl.MCLibUserInfoServiceImpl;
import com.mobcent.lib.android.ui.activity.adapter.MCLibUserInfoAdapter;
import com.mobcent.lib.android.ui.activity.listener.MCLibListViewItemOnClickListener;
import com.mobcent.lib.android.ui.delegate.MCLibUpdateListDelegate;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class MCLibBlackListActivity extends MCLibUIBaseActivity implements MCLibUpdateListDelegate {
    /* access modifiers changed from: private */
    public MCLibUserInfoAdapter adapter;
    /* access modifiers changed from: private */
    public Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            if (msg.what == 1 || msg.what == 5) {
                MCLibBlackListActivity.this.updateEmptyAdapter(msg.obj);
            } else if (msg.what == 2) {
                MCLibBlackListActivity.this.updateExistAdapter(msg.obj);
            } else if (msg.what == 3) {
                MCLibBlackListActivity.this.hideProgressBar();
            } else if (msg.what == 4) {
                MCLibBlackListActivity.this.showProgressBar();
            }
        }
    };
    private ImageButton refreshBtn;
    /* access modifiers changed from: private */
    public MCLibUpdateListDelegate updateUserListDelegate = new MCLibUpdateListDelegate() {
        public void updateList(final int page, int pageSize, boolean isReadLocally) {
            MCLibBlackListActivity.this.mHandler.sendMessage(MCLibBlackListActivity.this.mHandler.obtainMessage(5, new ArrayList()));
            new Thread() {
                public void run() {
                    MCLibBlackListActivity.this.mHandler.sendEmptyMessage(4);
                    List<MCLibUserInfo> userList = MCLibBlackListActivity.this.getUserFriends(page, 10);
                    if (userList == null || userList.size() <= 0) {
                        MCLibBlackListActivity.this.showEmptyInfoTip(R.string.mc_lib_no_such_data);
                        return;
                    }
                    MCLibBlackListActivity.this.updateUserListView(userList, true);
                    MCLibBlackListActivity.this.mHandler.sendEmptyMessage(3);
                }
            }.start();
        }
    };
    protected List<MCLibUserInfo> userInfoList;
    protected AbsListView.OnScrollListener userListOnScrollListener = new AbsListView.OnScrollListener() {
        private Vector<String> currentUrls;
        private int firstVisibleItem;
        private int totalItemCount;
        private int visibleItemCount;

        public void onScrollStateChanged(AbsListView view, int scrollState) {
            if (scrollState == 0) {
                List<String> imageUrls = new ArrayList<>();
                parseCurrentUrls();
                List<String> preUrls = getPreviousUrls();
                List<String> nextUrls = getNextImageUrls();
                if (preUrls != null) {
                    imageUrls.addAll(preUrls);
                }
                if (nextUrls != null) {
                    imageUrls.addAll(nextUrls);
                }
                MCLibBlackListActivity.this.adapter.asyncImageLoader.recycleBitmap(imageUrls);
            }
        }

        public void onScroll(AbsListView view, int firstVisibleItem2, int visibleItemCount2, int totalItemCount2) {
            this.firstVisibleItem = firstVisibleItem2;
            this.visibleItemCount = visibleItemCount2;
            this.totalItemCount = totalItemCount2;
        }

        private void parseCurrentUrls() {
            this.currentUrls = new Vector<>();
            int firstIndex = 0;
            int endIndex = this.firstVisibleItem + this.visibleItemCount + 10;
            if (this.firstVisibleItem - 10 > 0) {
                firstIndex = this.firstVisibleItem - 10;
            }
            if (endIndex >= this.totalItemCount) {
                endIndex = this.totalItemCount;
            }
            for (int i = firstIndex; i < endIndex; i++) {
                if (MCLibBlackListActivity.this.userInfoList.get(i).getImage() != null && !MCLibBlackListActivity.this.userInfoList.get(i).getImage().equals("")) {
                    this.currentUrls.add(MCLibBlackListActivity.this.userInfoList.get(i).getImage());
                }
            }
        }

        private List<String> getPreviousUrls() {
            if (this.firstVisibleItem - 10 <= 0) {
                return null;
            }
            List<String> urls = new ArrayList<>();
            for (int i = 0; i < this.firstVisibleItem - 10; i++) {
                if (MCLibBlackListActivity.this.userInfoList.get(i).getImage() != null && !MCLibBlackListActivity.this.userInfoList.get(i).getImage().equals("") && !this.currentUrls.contains(MCLibBlackListActivity.this.userInfoList.get(i).getImage())) {
                    urls.add(MCLibBlackListActivity.this.userInfoList.get(i).getImage());
                }
            }
            return urls;
        }

        private List<String> getNextImageUrls() {
            int currentVisibleBottom = this.firstVisibleItem + this.visibleItemCount;
            if (this.totalItemCount - currentVisibleBottom <= 10) {
                return null;
            }
            List<String> urls = new ArrayList<>();
            for (int i = currentVisibleBottom + 10; i < this.totalItemCount; i++) {
                if (MCLibBlackListActivity.this.userInfoList.get(i).getImage() != null && !MCLibBlackListActivity.this.userInfoList.get(i).getImage().equals("") && !this.currentUrls.contains(MCLibBlackListActivity.this.userInfoList.get(i).getImage())) {
                    urls.add(MCLibBlackListActivity.this.userInfoList.get(i).getImage());
                }
            }
            return urls;
        }
    };
    private ListView userListView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.mc_lib_black_list);
        initWindowTitle();
        initSysMsgWidgets();
        this.userListView = (ListView) findViewById(R.id.mcLibUserBundledListView);
        this.userListView.setOnScrollListener(this.userListOnScrollListener);
        this.userListView.setOnItemClickListener(new MCLibListViewItemOnClickListener());
        this.refreshBtn = (ImageButton) findViewById(R.id.mcLibRefreshContentBtn);
        this.refreshBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCLibBlackListActivity.this.updateUserListDelegate.updateList(1, 10, false);
            }
        });
        initProgressBox();
        initLoginUserHeader();
        initNavBottomBar(102);
        this.refreshBtn.performClick();
    }

    /* access modifiers changed from: private */
    public void updateEmptyAdapter(Object obj) {
        this.adapter = new MCLibUserInfoAdapter(this, this.userListView, R.layout.mc_lib_user_list_row, (List) obj, this, true, this.mHandler);
        this.userListView.setAdapter((ListAdapter) this.adapter);
        this.userInfoList = (List) obj;
    }

    /* access modifiers changed from: private */
    public void updateExistAdapter(Object obj) {
        this.adapter.setUserInfoList((List) obj);
        this.adapter.notifyDataSetInvalidated();
        this.adapter.notifyDataSetChanged();
        this.userInfoList = (List) obj;
    }

    public void updateList(final int page, final int pageSize, boolean isReadLocally) {
        new Thread() {
            public void run() {
                List<MCLibUserInfo> resultList = new ArrayList<>();
                boolean isShowSearchBar = false;
                if (page == 1) {
                    MCLibBlackListActivity.this.mHandler.sendEmptyMessage(4);
                    MCLibBlackListActivity.this.mHandler.sendMessage(MCLibBlackListActivity.this.mHandler.obtainMessage(5, resultList));
                    isShowSearchBar = true;
                    if (MCLibBlackListActivity.this.userInfoList != null) {
                        MCLibBlackListActivity.this.userInfoList.clear();
                    }
                }
                List<MCLibUserInfo> nextPageUserInfoList = MCLibBlackListActivity.this.getUserFriends(page, pageSize);
                if (nextPageUserInfoList != null && nextPageUserInfoList.size() > 0 && page == 1) {
                    resultList = nextPageUserInfoList;
                    MCLibBlackListActivity.this.mHandler.sendEmptyMessage(3);
                } else if (nextPageUserInfoList != null && nextPageUserInfoList.size() > 0) {
                    resultList.addAll(MCLibBlackListActivity.this.userInfoList);
                    resultList.remove(resultList.size() - 1);
                    resultList.addAll(nextPageUserInfoList);
                } else if (MCLibBlackListActivity.this.userInfoList.size() > 0) {
                    resultList.addAll(MCLibBlackListActivity.this.userInfoList);
                    resultList.remove(resultList.size() - 1);
                }
                MCLibBlackListActivity.this.updateUserListView(resultList, isShowSearchBar);
            }
        }.start();
    }

    /* access modifiers changed from: private */
    public void updateUserListView(List<MCLibUserInfo> userList, boolean isShowSearchBar) {
        if (this.adapter == null) {
            this.mHandler.sendMessage(this.mHandler.obtainMessage(1, userList));
            return;
        }
        this.mHandler.sendMessage(this.mHandler.obtainMessage(2, userList));
    }

    public List<MCLibUserInfo> getUserFriends(int page, int pageSize) {
        MCLibUserInfoService userInfoService = new MCLibUserInfoServiceImpl(this);
        return userInfoService.getBlockList(userInfoService.getLoginUserId(), page, pageSize);
    }

    public Handler getCurrentHandler() {
        return this.mHandler;
    }
}
