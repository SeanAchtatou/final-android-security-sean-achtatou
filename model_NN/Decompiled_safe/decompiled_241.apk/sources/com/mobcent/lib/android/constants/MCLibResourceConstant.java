package com.mobcent.lib.android.constants;

import com.mobcent.android.seed.autovGhnzo98NIzLsRPLHS.R;
import java.util.HashMap;

public class MCLibResourceConstant {
    private static HashMap<String, Integer> allHashMap = new HashMap<>();
    private static HashMap<String, Integer> femaleHashMap = new HashMap<>();
    private static HashMap<String, Integer> maleHashMap = new HashMap<>();
    private static MCLibResourceConstant mcLibResourceMap;

    private MCLibResourceConstant() {
        allHashMap.put("mc_lib_face_f01.jpg", Integer.valueOf((int) R.drawable.mc_lib_face_f01));
        allHashMap.put("mc_lib_face_f02.jpg", Integer.valueOf((int) R.drawable.mc_lib_face_f02));
        allHashMap.put("mc_lib_face_f03.jpg", Integer.valueOf((int) R.drawable.mc_lib_face_f03));
        allHashMap.put("mc_lib_face_f04.jpg", Integer.valueOf((int) R.drawable.mc_lib_face_f04));
        allHashMap.put("mc_lib_face_f09.jpg", Integer.valueOf((int) R.drawable.mc_lib_face_f09));
        allHashMap.put("mc_lib_face_f14.jpg", Integer.valueOf((int) R.drawable.mc_lib_face_f14));
        allHashMap.put("mc_lib_face_f15.jpg", Integer.valueOf((int) R.drawable.mc_lib_face_f15));
        allHashMap.put("mc_lib_face_f16.jpg", Integer.valueOf((int) R.drawable.mc_lib_face_f16));
        allHashMap.put("mc_lib_face_f26.jpg", Integer.valueOf((int) R.drawable.mc_lib_face_f26));
        allHashMap.put("mc_lib_face_m09.jpg", Integer.valueOf((int) R.drawable.mc_lib_face_m09));
        allHashMap.put("mc_lib_face_m10.jpg", Integer.valueOf((int) R.drawable.mc_lib_face_m10));
        allHashMap.put("mc_lib_face_m17.jpg", Integer.valueOf((int) R.drawable.mc_lib_face_m17));
        allHashMap.put("mc_lib_face_m19.jpg", Integer.valueOf((int) R.drawable.mc_lib_face_m19));
        allHashMap.put("mc_lib_face_m20.jpg", Integer.valueOf((int) R.drawable.mc_lib_face_m20));
        allHashMap.put("mc_lib_face_m21.jpg", Integer.valueOf((int) R.drawable.mc_lib_face_m21));
        allHashMap.put("mc_lib_face_m22.jpg", Integer.valueOf((int) R.drawable.mc_lib_face_m22));
        allHashMap.put("mc_lib_face_m24.jpg", Integer.valueOf((int) R.drawable.mc_lib_face_m24));
        allHashMap.put("mc_lib_face_m26.jpg", Integer.valueOf((int) R.drawable.mc_lib_face_m26));
        allHashMap.put("mc_lib_face_u0.jpg", Integer.valueOf((int) R.drawable.mc_lib_face_u0));
        femaleHashMap.put("mc_lib_face_f01.jpg", Integer.valueOf((int) R.drawable.mc_lib_face_f01));
        femaleHashMap.put("mc_lib_face_f02.jpg", Integer.valueOf((int) R.drawable.mc_lib_face_f02));
        femaleHashMap.put("mc_lib_face_f03.jpg", Integer.valueOf((int) R.drawable.mc_lib_face_f03));
        femaleHashMap.put("mc_lib_face_f04.jpg", Integer.valueOf((int) R.drawable.mc_lib_face_f04));
        femaleHashMap.put("mc_lib_face_f09.jpg", Integer.valueOf((int) R.drawable.mc_lib_face_f09));
        femaleHashMap.put("mc_lib_face_f14.jpg", Integer.valueOf((int) R.drawable.mc_lib_face_f14));
        femaleHashMap.put("mc_lib_face_f15.jpg", Integer.valueOf((int) R.drawable.mc_lib_face_f15));
        femaleHashMap.put("mc_lib_face_f16.jpg", Integer.valueOf((int) R.drawable.mc_lib_face_f16));
        femaleHashMap.put("mc_lib_face_f26.jpg", Integer.valueOf((int) R.drawable.mc_lib_face_f26));
        maleHashMap.put("mc_lib_face_m09.jpg", Integer.valueOf((int) R.drawable.mc_lib_face_m09));
        maleHashMap.put("mc_lib_face_m10.jpg", Integer.valueOf((int) R.drawable.mc_lib_face_m10));
        maleHashMap.put("mc_lib_face_m17.jpg", Integer.valueOf((int) R.drawable.mc_lib_face_m17));
        maleHashMap.put("mc_lib_face_m19.jpg", Integer.valueOf((int) R.drawable.mc_lib_face_m19));
        maleHashMap.put("mc_lib_face_m20.jpg", Integer.valueOf((int) R.drawable.mc_lib_face_m20));
        maleHashMap.put("mc_lib_face_m21.jpg", Integer.valueOf((int) R.drawable.mc_lib_face_m21));
        maleHashMap.put("mc_lib_face_m22.jpg", Integer.valueOf((int) R.drawable.mc_lib_face_m22));
        maleHashMap.put("mc_lib_face_m24.jpg", Integer.valueOf((int) R.drawable.mc_lib_face_m24));
        maleHashMap.put("mc_lib_face_m26.jpg", Integer.valueOf((int) R.drawable.mc_lib_face_m26));
    }

    public static MCLibResourceConstant getMCResourceConstant() {
        if (mcLibResourceMap == null) {
            mcLibResourceMap = new MCLibResourceConstant();
        }
        return mcLibResourceMap;
    }

    public HashMap<String, Integer> getAllResourceMap() {
        return allHashMap;
    }

    public HashMap<String, Integer> getMaleResourceMap() {
        return maleHashMap;
    }

    public HashMap<String, Integer> getFemaleResourceMap() {
        return femaleHashMap;
    }
}
