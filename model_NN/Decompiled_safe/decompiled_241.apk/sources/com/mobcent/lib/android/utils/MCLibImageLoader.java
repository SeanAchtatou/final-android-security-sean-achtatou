package com.mobcent.lib.android.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.mobcent.android.service.impl.MCLibFileTransferServiceImpl;
import com.mobcent.android.utils.MCLibIOUtil;
import java.io.File;

public class MCLibImageLoader {
    public static final String LOCAL_POSITION_DIR = (MCLibIOUtil.FS + "mobcent" + MCLibIOUtil.FS + "imageCache" + MCLibIOUtil.FS);
    private static MCLibImageLoader instance;
    private Context activity;

    private MCLibImageLoader(Context activity2) {
        this.activity = activity2;
    }

    public static MCLibImageLoader getInstance(Context activity2) {
        if (instance == null) {
            instance = new MCLibImageLoader(activity2);
        }
        return instance;
    }

    public Bitmap loadBitmap(String urlString, String size, boolean isForceGetByHttp) {
        if (urlString == null || size == null || urlString.equals("")) {
            return null;
        }
        if (urlString.indexOf("xgsize") > -1) {
            urlString = urlString.replace("xgsize", size);
        }
        String baseLocation = MCLibIOUtil.getBaseLocalLocation(this.activity);
        String imagePath = baseLocation + LOCAL_POSITION_DIR;
        if (!MCLibIOUtil.isDirExist(imagePath) && !MCLibIOUtil.makeDirs(imagePath)) {
            return new MCLibFileTransferServiceImpl(this.activity).getBitmapFromUrl(urlString);
        }
        String fileName = getPathName(urlString);
        if (MCLibIOUtil.isFileExist(imagePath + fileName) && !isForceGetByHttp) {
            return BitmapFactory.decodeFile(imagePath + fileName);
        }
        Bitmap bitmap = new MCLibFileTransferServiceImpl(this.activity).getBitmapFromUrl(urlString);
        if (bitmap != null && !isForceGetByHttp) {
            MCLibIOUtil.saveImageFile(this.activity, bitmap, getImageType(fileName.substring(fileName.lastIndexOf(".") + 1)), LOCAL_POSITION_DIR + fileName, baseLocation);
        }
        return bitmap;
    }

    public static String getPathName(String url) {
        return url.substring(url.lastIndexOf("/") + 1);
    }

    public Bitmap.CompressFormat getImageType(String imageType) {
        if (imageType.equalsIgnoreCase("png")) {
            return Bitmap.CompressFormat.PNG;
        }
        if (imageType.equalsIgnoreCase("jpg")) {
            return Bitmap.CompressFormat.JPEG;
        }
        return Bitmap.CompressFormat.PNG;
    }

    public static boolean downloadGif(String urlString, String size, Context context) {
        if (urlString == null || size == null || urlString.equals("")) {
            return false;
        }
        if (urlString.indexOf("xgsize") > -1) {
            urlString = urlString.replace("xgsize", size);
        }
        String baseLocation = MCLibIOUtil.getSDCardPath();
        if (baseLocation == null) {
            return false;
        }
        String imagePath = baseLocation + LOCAL_POSITION_DIR;
        if (!MCLibIOUtil.isDirExist(imagePath) && !MCLibIOUtil.makeDirs(imagePath)) {
            return false;
        }
        String fileName = getPathName(urlString);
        if (MCLibIOUtil.isFileExist(imagePath + fileName)) {
            return true;
        }
        return MCLibIOUtil.saveFile(new MCLibFileTransferServiceImpl(context).getImageStream(urlString), LOCAL_POSITION_DIR + fileName, baseLocation);
    }

    public static String getImageCachePath(String icon) {
        String baseLocation = MCLibIOUtil.getSDCardPath();
        if (baseLocation == null) {
            return null;
        }
        String imagePath = baseLocation + LOCAL_POSITION_DIR;
        if (!MCLibIOUtil.isDirExist(imagePath)) {
            new File(imagePath).mkdirs();
        }
        return imagePath + icon;
    }

    public static String getImageCacheBasePath() {
        String baseLocation = MCLibIOUtil.getSDCardPath();
        if (baseLocation == null) {
            return null;
        }
        return baseLocation + LOCAL_POSITION_DIR;
    }

    public static Bitmap getBitmap(String filePath, int sampleSize) {
        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
        bitmapOptions.inSampleSize = sampleSize;
        return BitmapFactory.decodeFile(filePath, bitmapOptions);
    }
}
