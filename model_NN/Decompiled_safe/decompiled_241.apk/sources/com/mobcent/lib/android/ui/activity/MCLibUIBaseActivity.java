package com.mobcent.lib.android.ui.activity;

import android.content.Intent;
import android.content.IntentFilter;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import com.mobcent.android.constants.MCLibParameterKeyConstant;
import com.mobcent.android.model.MCLibUserInfo;
import com.mobcent.android.os.service.MCLibHeartBeatOSService;
import com.mobcent.android.os.service.helper.MCLibDeveloperServiceHelper;
import com.mobcent.android.seed.autovGhnzo98NIzLsRPLHS.R;
import com.mobcent.android.service.impl.MCLibUserInfoServiceImpl;
import com.mobcent.android.service.impl.MCLibUserPrivateMsgServiceImpl;
import com.mobcent.lib.android.developer.MCLibDeveloperProfile;
import com.mobcent.lib.android.ui.activity.receiver.MCLibMsgNotificationReceiver;
import com.mobcent.lib.android.ui.delegate.impl.MCLibMsgNotificationIconDelegateImpl;
import com.mobcent.lib.android.ui.delegate.impl.MCLibPublishMoodDelegateImpl;
import com.mobcent.lib.android.ui.delegate.impl.MCLibSendFeedbackDelegateImpl;
import com.mobcent.lib.android.ui.dialog.MCLibPublishWordsDialog;
import com.mobcent.lib.android.ui.dialog.MCLibRegLoginDialog;
import com.mobcent.lib.android.utils.MCLibImageUtil;
import com.mobcent.lib.android.utils.i18n.MCLibStringBundleUtil;
import com.mobcent.share.android.activity.MCShareAppActivity;
import com.mobcent.share.android.activity.MCShareBindSitesActivity;

public abstract class MCLibUIBaseActivity extends MCLibSysBaseActivity {
    public static final int HIDE_PRG_BAR = 3;
    protected static final int MENU_BLACK_LIST = 8;
    protected static final int MENU_CHANGE_ACCOUNT = 3;
    protected static final int MENU_CHANGE_MOOD = 5;
    protected static final int MENU_MY_DOWNLOAD = 2;
    protected static final int MENU_MY_PUBLISH_TOPIC = 4;
    protected static final int MENU_MY_SETTING = 6;
    protected static final int MENU_RETURN_APP = 10;
    protected static final int MENU_SEND_FEEDBACK = 1;
    protected static final int MENU_SHARE_APP = 9;
    protected static final int MENU_SYNC_SITE = 7;
    public static final int NAV_CHAT_SESSION = 104;
    public static final int NAV_COMMUNITY = 100;
    public static final int NAV_EVENTS = 101;
    public static final int NAV_FRIENDS = 102;
    public static final int NAV_MORE_MENU = 103;
    public static final int NAV_OTHER = 105;
    public static final int REMOVE_ALL_LIST_ENTRIES = 5;
    public static final int SHOW_PRG_BAR = 4;
    public static final int UPDATE_EMPTY_ADAPTER = 1;
    public static final int UPDATE_EXIST_ADAPTER = 2;
    private final int MORE_MENU_HIDDEN = 0;
    private final int MORE_MENU_SHOW_UP = 1;
    protected ImageView blackListImage;
    protected ImageView bpAppIconImage;
    protected ImageView changeAccountImage;
    protected ImageView changeMoodImage;
    public ImageView chatSessionNavImage;
    protected ImageView communityBottomNavImage;
    protected ImageView downloadManagerImage;
    protected ImageView eventBottomNavImage;
    protected ImageView feedbackImage;
    protected ImageView friendBottomNavImage;
    protected TableLayout genericActionMenuBox;
    protected RelativeLayout loadingBox;
    protected RelativeLayout loginUserHeader;
    protected RelativeLayout mcLibBlackListContainer;
    protected RelativeLayout mcLibChangeAccountContainer;
    protected RelativeLayout mcLibDownloadManagerContainer;
    protected RelativeLayout mcLibFeedbackContainer;
    protected RelativeLayout mcLibPubMoodContainer;
    protected RelativeLayout mcLibPubTopicContainer;
    protected RelativeLayout mcLibReturnAppContainer;
    protected RelativeLayout mcLibShareContainer;
    protected RelativeLayout mcLibSyncContainer;
    protected RelativeLayout mcLibUserSettingContainer;
    protected ImageView moreMenuBottomNavImage;
    protected RelativeLayout moreMenuBox;
    public RelativeLayout msgNotificationBox;
    public TextView msgNotificationText;
    public ImageView newMsgImage;
    private View.OnClickListener newMsgImageOnClickListener = new View.OnClickListener() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
         arg types: [java.lang.String, int]
         candidates:
          ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
        public void onClick(View v) {
            if (MCLibUIBaseActivity.this.newMsgImage.getTag() != null) {
                if (MCLibUIBaseActivity.this.newMsgImage.getTag().equals(MCLibMsgNotificationReceiver.CURRENT_TYPE_MSG)) {
                    MCLibUIBaseActivity.this.startActivity(new Intent(MCLibUIBaseActivity.this, MCLibChatSessionListActivity.class));
                } else if (MCLibUIBaseActivity.this.newMsgImage.getTag().equals(MCLibMsgNotificationReceiver.CURRENT_TYPE_HAS_REPLY)) {
                    Intent intent = new Intent(MCLibUIBaseActivity.this, MCLibCommunityBundleActivity.class);
                    intent.putExtra(MCLibParameterKeyConstant.STATUS_TYPE, 4);
                    intent.putExtra(MCLibParameterKeyConstant.IS_FORCE_REFRESH, true);
                    MCLibUIBaseActivity.this.startActivity(intent);
                } else if (MCLibUIBaseActivity.this.newMsgImage.getTag().equals(MCLibMsgNotificationReceiver.CURRENT_TYPE_SYS_MSG)) {
                    MCLibUIBaseActivity.this.showSysMsgBox();
                }
            }
        }
    };
    public TextView plsWaitText;
    protected ProgressBar progressBar;
    protected ImageView publishTopicImage;
    protected ImageView returnAppImage;
    protected ImageView shareAppImage;
    protected ImageView syncSiteImage;
    public TextView userMoodText;
    public TextView userNameText;
    protected ImageView userPhotoImage;
    protected ImageView userSettingImage;

    /* access modifiers changed from: protected */
    public void initWindowTitle() {
        setTitle(MCLibStringBundleUtil.resolveString(R.string.mc_lib_community_title, new String[]{MCLibDeveloperProfile.getBpAppName(this)}, this));
    }

    /* access modifiers changed from: protected */
    public void initLoginUserHeader() {
        this.userNameText = (TextView) findViewById(R.id.mcLibUserNameTextView);
        this.userMoodText = (TextView) findViewById(R.id.mcLibUserMoodTextView);
        this.userPhotoImage = (ImageView) findViewById(R.id.mcLibUserPhotoImageView);
        this.bpAppIconImage = (ImageView) findViewById(R.id.mcLibBPAppIcon);
        this.loginUserHeader = (RelativeLayout) findViewById(R.id.mcLibHeader);
        MCLibUserInfo loginUserInfo = new MCLibUserInfoServiceImpl(this).getLoginUser();
        this.userNameText.setText(loginUserInfo.getNickName());
        String mood = loginUserInfo.getEmotionWords();
        if (mood == null || mood.equals("null")) {
            mood = "";
            this.userMoodText.setVisibility(4);
        } else if (mood.length() > 15) {
            mood = mood.substring(0, 15) + "...";
        }
        this.userMoodText.setText(MCLibStringBundleUtil.resolveString(R.string.mc_lib_mood, new String[]{mood}, this));
        MCLibImageUtil.updateUserPhotoImage(this.userPhotoImage, loginUserInfo.getImage(), this, R.drawable.mc_lib_face_u0, getCurrentHandler());
        this.bpAppIconImage.setBackgroundDrawable(getApplicationInfo().loadIcon(getPackageManager()));
        initHeaderListener();
        initBpAppIconListener();
    }

    private void initHeaderListener() {
        this.loginUserHeader.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCLibUIBaseActivity.this.startActivity(new Intent(MCLibUIBaseActivity.this, MCLibUserSettingActivity.class));
            }
        });
    }

    private void initBpAppIconListener() {
        this.bpAppIconImage.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCLibUIBaseActivity.this.returnToBpApp();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void returnToBpApp() {
        MCLibDeveloperServiceHelper.closeCommunityActivities();
    }

    /* access modifiers changed from: protected */
    public void initNavBottomBar(int type) {
        this.msgNotificationText = (TextView) findViewById(R.id.mcLibMsgNotificationTextView);
        this.newMsgImage = (ImageView) findViewById(R.id.mcLibMsgNotificationImageView);
        this.msgNotificationBox = (RelativeLayout) findViewById(R.id.mcLibMsgNotificationBox);
        this.communityBottomNavImage = (ImageView) findViewById(R.id.mcLibCommunityImageView);
        this.eventBottomNavImage = (ImageView) findViewById(R.id.mcLibEventImageView);
        this.friendBottomNavImage = (ImageView) findViewById(R.id.mcLibFriendsImageView);
        this.moreMenuBottomNavImage = (ImageView) findViewById(R.id.mcLibMoreMenuImageView);
        this.chatSessionNavImage = (ImageView) findViewById(R.id.mcLibChatSessionImageView);
        this.moreMenuBox = (RelativeLayout) findViewById(R.id.mcLibMoreMenuBox);
        this.genericActionMenuBox = (TableLayout) findViewById(R.id.mcLibGenericActionMenuBox);
        this.changeMoodImage = (ImageView) findViewById(R.id.mcLibPubMoodImageView);
        this.publishTopicImage = (ImageView) findViewById(R.id.mcLibPubTopicImageView);
        this.feedbackImage = (ImageView) findViewById(R.id.mcLibFeedbackImageView);
        this.shareAppImage = (ImageView) findViewById(R.id.mcLibShareImageView);
        this.downloadManagerImage = (ImageView) findViewById(R.id.mcLibDownloadManagerImageView);
        this.changeAccountImage = (ImageView) findViewById(R.id.mcLibChangeAccountImageView);
        this.userSettingImage = (ImageView) findViewById(R.id.mcLibUserSettingImageView);
        this.syncSiteImage = (ImageView) findViewById(R.id.mcLibSyncImageView);
        this.blackListImage = (ImageView) findViewById(R.id.mcLibBlackListImageView);
        this.returnAppImage = (ImageView) findViewById(R.id.mcLibReturnAppImageView);
        this.mcLibPubMoodContainer = (RelativeLayout) findViewById(R.id.mcLibPubMoodContainer);
        this.mcLibPubTopicContainer = (RelativeLayout) findViewById(R.id.mcLibPubTopicContainer);
        this.mcLibFeedbackContainer = (RelativeLayout) findViewById(R.id.mcLibFeedbackContainer);
        this.mcLibShareContainer = (RelativeLayout) findViewById(R.id.mcLibShareContainer);
        this.mcLibDownloadManagerContainer = (RelativeLayout) findViewById(R.id.mcLibDownloadManagerContainer);
        this.mcLibSyncContainer = (RelativeLayout) findViewById(R.id.mcLibSyncContainer);
        this.mcLibBlackListContainer = (RelativeLayout) findViewById(R.id.mcLibBlackListContainer);
        this.mcLibChangeAccountContainer = (RelativeLayout) findViewById(R.id.mcLibChangeAccountContainer);
        this.mcLibUserSettingContainer = (RelativeLayout) findViewById(R.id.mcLibUserSettingContainer);
        this.mcLibReturnAppContainer = (RelativeLayout) findViewById(R.id.mcLibReturnAppContainer);
        if (type == 100) {
            this.communityBottomNavImage.setBackgroundResource(R.drawable.mc_lib_bottom_bar_buntton2_h);
        } else if (type == 101) {
            this.eventBottomNavImage.setBackgroundResource(R.drawable.mc_lib_bottom_bar_buntton1_h);
        } else if (type == 102) {
            this.friendBottomNavImage.setBackgroundResource(R.drawable.mc_lib_bottom_bar_buntton3_h);
        } else if (type == 103) {
            this.moreMenuBottomNavImage.setBackgroundResource(R.drawable.mc_lib_bottom_bar_buntton4_h);
        } else if (type == 104) {
            this.chatSessionNavImage.setBackgroundResource(R.drawable.mc_lib_bottom_bar_buntton5_h);
        }
        initMsgNotificationListener();
        initCommunityNav(type);
        initEventNav(type);
        initFriendNav(type);
        initMoreMenuNav();
        initChatSessionNav(type);
    }

    private void initCommunityNav(int type) {
        if (type != 100) {
            this.communityBottomNavImage.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    MCLibUIBaseActivity.this.startActivity(new Intent(MCLibUIBaseActivity.this, MCLibCommunityBundleActivity.class));
                }
            });
            this.communityBottomNavImage.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == 0) {
                        MCLibUIBaseActivity.this.communityBottomNavImage.setBackgroundResource(R.drawable.mc_lib_bottom_bar_buntton2_d);
                        return false;
                    } else if (1 == event.getAction()) {
                        MCLibUIBaseActivity.this.communityBottomNavImage.setBackgroundResource(R.drawable.mc_lib_bottom_bar_buntton2_n);
                        return false;
                    } else if (3 != event.getAction()) {
                        return false;
                    } else {
                        MCLibUIBaseActivity.this.communityBottomNavImage.setBackgroundResource(R.drawable.mc_lib_bottom_bar_buntton2_n);
                        return false;
                    }
                }
            });
        }
    }

    private void initEventNav(int type) {
        if (type != 101) {
            this.eventBottomNavImage.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    MCLibUIBaseActivity.this.startActivity(new Intent(MCLibUIBaseActivity.this, MCLibEventBundleActivity.class));
                }
            });
            this.eventBottomNavImage.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == 0) {
                        MCLibUIBaseActivity.this.eventBottomNavImage.setBackgroundResource(R.drawable.mc_lib_bottom_bar_buntton1_d);
                        return false;
                    } else if (1 == event.getAction()) {
                        MCLibUIBaseActivity.this.eventBottomNavImage.setBackgroundResource(R.drawable.mc_lib_bottom_bar_buntton1_n);
                        return false;
                    } else if (3 != event.getAction()) {
                        return false;
                    } else {
                        MCLibUIBaseActivity.this.eventBottomNavImage.setBackgroundResource(R.drawable.mc_lib_bottom_bar_buntton1_n);
                        return false;
                    }
                }
            });
        }
    }

    private void initFriendNav(int type) {
        if (type != 102) {
            this.friendBottomNavImage.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    MCLibUIBaseActivity.this.startActivity(new Intent(MCLibUIBaseActivity.this, MCLibUserBundleActivity.class));
                }
            });
            this.friendBottomNavImage.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == 0) {
                        MCLibUIBaseActivity.this.friendBottomNavImage.setBackgroundResource(R.drawable.mc_lib_bottom_bar_buntton3_d);
                        return false;
                    } else if (1 == event.getAction()) {
                        MCLibUIBaseActivity.this.friendBottomNavImage.setBackgroundResource(R.drawable.mc_lib_bottom_bar_buntton3_n);
                        return false;
                    } else if (3 != event.getAction()) {
                        return false;
                    } else {
                        MCLibUIBaseActivity.this.friendBottomNavImage.setBackgroundResource(R.drawable.mc_lib_bottom_bar_buntton3_n);
                        return false;
                    }
                }
            });
        }
    }

    private void initMoreMenuNav() {
        this.moreMenuBox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCLibUIBaseActivity.this.hideMoreMenu();
            }
        });
        this.changeMoodImage.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCLibUIBaseActivity.this.onMoreMenuClickHandler(5);
            }
        });
        this.publishTopicImage.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCLibUIBaseActivity.this.onMoreMenuClickHandler(4);
            }
        });
        this.feedbackImage.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCLibUIBaseActivity.this.onMoreMenuClickHandler(1);
            }
        });
        this.shareAppImage.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCLibUIBaseActivity.this.onMoreMenuClickHandler(9);
            }
        });
        this.downloadManagerImage.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCLibUIBaseActivity.this.onMoreMenuClickHandler(2);
            }
        });
        this.changeAccountImage.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCLibUIBaseActivity.this.onMoreMenuClickHandler(3);
            }
        });
        this.userSettingImage.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCLibUIBaseActivity.this.onMoreMenuClickHandler(6);
            }
        });
        this.syncSiteImage.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCLibUIBaseActivity.this.onMoreMenuClickHandler(7);
            }
        });
        this.blackListImage.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCLibUIBaseActivity.this.onMoreMenuClickHandler(8);
            }
        });
        this.returnAppImage.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                MCLibUIBaseActivity.this.onMoreMenuClickHandler(10);
            }
        });
        this.mcLibPubMoodContainer.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCLibUIBaseActivity.this.changeMoodImage.performClick();
            }
        });
        this.mcLibPubTopicContainer.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCLibUIBaseActivity.this.publishTopicImage.performClick();
            }
        });
        this.mcLibFeedbackContainer.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCLibUIBaseActivity.this.feedbackImage.performClick();
            }
        });
        this.mcLibShareContainer.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCLibUIBaseActivity.this.shareAppImage.performClick();
            }
        });
        this.mcLibDownloadManagerContainer.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCLibUIBaseActivity.this.downloadManagerImage.performClick();
            }
        });
        this.mcLibSyncContainer.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCLibUIBaseActivity.this.syncSiteImage.performClick();
            }
        });
        this.mcLibBlackListContainer.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCLibUIBaseActivity.this.blackListImage.performClick();
            }
        });
        this.mcLibChangeAccountContainer.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCLibUIBaseActivity.this.changeAccountImage.performClick();
            }
        });
        this.mcLibUserSettingContainer.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCLibUIBaseActivity.this.userSettingImage.performClick();
            }
        });
        this.mcLibReturnAppContainer.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                MCLibUIBaseActivity.this.returnAppImage.performClick();
            }
        });
        this.moreMenuBottomNavImage.setTag(0);
        this.moreMenuBottomNavImage.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int type = ((Integer) v.getTag()).intValue();
                if (type == 0) {
                    MCLibUIBaseActivity.this.showMoreMenu();
                } else if (type == 1) {
                    MCLibUIBaseActivity.this.hideMoreMenu();
                }
            }
        });
        this.moreMenuBottomNavImage.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == 0) {
                    MCLibUIBaseActivity.this.moreMenuBottomNavImage.setBackgroundResource(R.drawable.mc_lib_bottom_bar_buntton4_d);
                    return false;
                } else if (1 == event.getAction()) {
                    MCLibUIBaseActivity.this.moreMenuBottomNavImage.setBackgroundResource(R.drawable.mc_lib_bottom_bar_buntton4_h);
                    return false;
                } else if (3 != event.getAction()) {
                    return false;
                } else {
                    MCLibUIBaseActivity.this.moreMenuBottomNavImage.setBackgroundResource(R.drawable.mc_lib_bottom_bar_buntton4_n);
                    return false;
                }
            }
        });
    }

    private void initChatSessionNav(int type) {
        if (type != 104) {
            this.chatSessionNavImage.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    MCLibUIBaseActivity.this.startActivity(new Intent(MCLibUIBaseActivity.this, MCLibChatSessionListActivity.class));
                }
            });
            this.chatSessionNavImage.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == 0) {
                        MCLibUIBaseActivity.this.chatSessionNavImage.setBackgroundResource(R.drawable.mc_lib_bottom_bar_buntton5_d);
                        return false;
                    } else if (1 == event.getAction()) {
                        MCLibUIBaseActivity.this.chatSessionNavImage.setBackgroundResource(R.drawable.mc_lib_bottom_bar_buntton5_n);
                        return false;
                    } else if (3 != event.getAction()) {
                        return false;
                    } else {
                        MCLibUIBaseActivity.this.chatSessionNavImage.setBackgroundResource(R.drawable.mc_lib_bottom_bar_buntton5_n);
                        return false;
                    }
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    public void initProgressBox() {
        this.loadingBox = (RelativeLayout) findViewById(R.id.mcLibLoadingBox);
        this.plsWaitText = (TextView) findViewById(R.id.mcLibPlsWaitTextView);
        this.progressBar = (ProgressBar) findViewById(R.id.mcLibProgressBar);
    }

    public void hideProgressBar() {
        getCurrentHandler().post(new Runnable() {
            public void run() {
                MCLibUIBaseActivity.this.loadingBox.setVisibility(8);
            }
        });
    }

    public void showEmptyInfoTip(final int tipRes) {
        getCurrentHandler().postDelayed(new Runnable() {
            public void run() {
                MCLibUIBaseActivity.this.loadingBox.setVisibility(0);
                MCLibUIBaseActivity.this.progressBar.setVisibility(8);
                MCLibUIBaseActivity.this.plsWaitText.setText(tipRes);
            }
        }, 100);
    }

    public void showProgressBar() {
        getCurrentHandler().post(new Runnable() {
            public void run() {
                MCLibUIBaseActivity.this.plsWaitText.setText((int) R.string.mc_lib_pls_wait);
                MCLibUIBaseActivity.this.progressBar.setVisibility(0);
                MCLibUIBaseActivity.this.loadingBox.setVisibility(0);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void initMsgNotificationReceiver() {
        if (this.msgNotificationReceiver == null) {
            this.msgNotificationReceiver = new MCLibMsgNotificationReceiver(this, new MCLibMsgNotificationIconDelegateImpl(this), getCurrentHandler());
        }
        IntentFilter filter = new IntentFilter();
        filter.addAction(getPackageName() + MCLibHeartBeatOSService.SERVER_NOTIFICATION_MSG);
        registerReceiver(this.msgNotificationReceiver, filter);
    }

    public void onMoreMenuClickHandler(int menuId) {
        switch (menuId) {
            case 1:
                hideMoreMenu();
                MCLibSendFeedbackDelegateImpl delegate = new MCLibSendFeedbackDelegateImpl(this);
                MCLibPublishWordsDialog publishWordsDialog = new MCLibPublishWordsDialog(this, R.string.mc_lib_send_feedback, delegate, 500);
                delegate.setDialog(publishWordsDialog);
                publishWordsDialog.show();
                return;
            case 2:
                hideMoreMenu();
                startActivity(new Intent(this, MCLibDownloadManagerActivity.class));
                return;
            case 3:
                hideMoreMenu();
                new MCLibRegLoginDialog(this, R.style.mc_lib_dialog, true, false, null).show();
                return;
            case 4:
                hideMoreMenu();
                Intent intent = new Intent(this, MCLibPublishStatusActivity.class);
                intent.putExtra(MCLibParameterKeyConstant.STATUS_ROOT_ID, 0);
                intent.putExtra(MCLibParameterKeyConstant.STATUS_REPLY_ID, 0);
                startActivity(intent);
                return;
            case 5:
                hideMoreMenu();
                MCLibPublishMoodDelegateImpl delegate2 = new MCLibPublishMoodDelegateImpl(this);
                MCLibPublishWordsDialog publishWordsDialog2 = new MCLibPublishWordsDialog(this, R.string.mc_lib_change_mood, delegate2, 50);
                delegate2.setDialog(publishWordsDialog2);
                publishWordsDialog2.show();
                return;
            case 6:
                hideMoreMenu();
                startActivity(new Intent(this, MCLibUserSettingActivity.class));
                return;
            case 7:
                hideMoreMenu();
                Intent intent2 = new Intent(this, MCShareBindSitesActivity.class);
                intent2.putExtra("uid", new MCLibUserInfoServiceImpl(this).getLoginUserId());
                intent2.putExtra("appKey", getResources().getString(R.string.mc_lib_bp_app_key));
                startActivity(intent2);
                return;
            case 8:
                hideMoreMenu();
                startActivity(new Intent(this, MCLibBlackListActivity.class));
                return;
            case 9:
                String[] arg = {getApplicationInfo().loadLabel(getPackageManager()).toString()};
                Intent intent3 = new Intent(this, MCShareAppActivity.class);
                intent3.putExtra("shareContent", MCLibStringBundleUtil.resolveString(R.string.mc_lib_share_app_content, arg, this));
                intent3.putExtra("uid", new MCLibUserInfoServiceImpl(this).getLoginUserId());
                intent3.putExtra("appKey", getResources().getString(R.string.mc_lib_bp_app_key));
                startActivity(intent3);
                return;
            case 10:
                returnToBpApp();
                return;
            default:
                hideMoreMenu();
                return;
        }
    }

    /* access modifiers changed from: private */
    public void showMoreMenu() {
        getCurrentHandler().post(new Runnable() {
            public void run() {
                MCLibUIBaseActivity.this.moreMenuBottomNavImage.setBackgroundResource(R.drawable.mc_lib_bottom_bar_buntton4_h);
                MCLibUIBaseActivity.this.moreMenuBottomNavImage.setTag(1);
                MCLibUIBaseActivity.this.moreMenuBox.setVisibility(0);
                MCLibUIBaseActivity.this.genericActionMenuBox.startAnimation(AnimationUtils.loadAnimation(MCLibUIBaseActivity.this, R.anim.mc_lib_grow_from_right_bottom));
            }
        });
    }

    /* access modifiers changed from: private */
    public void hideMoreMenu() {
        getCurrentHandler().post(new Runnable() {
            public void run() {
                MCLibUIBaseActivity.this.moreMenuBottomNavImage.setBackgroundResource(R.drawable.mc_lib_bottom_bar_buntton4_n);
                MCLibUIBaseActivity.this.moreMenuBottomNavImage.setTag(0);
                Animation anim = AnimationUtils.loadAnimation(MCLibUIBaseActivity.this, R.anim.mc_lib_shrink_to_right_bottom);
                MCLibUIBaseActivity.this.genericActionMenuBox.startAnimation(anim);
                anim.setAnimationListener(new Animation.AnimationListener() {
                    public void onAnimationStart(Animation animation) {
                    }

                    public void onAnimationRepeat(Animation animation) {
                    }

                    public void onAnimationEnd(Animation animation) {
                        MCLibUIBaseActivity.this.moreMenuBox.setVisibility(4);
                    }
                });
            }
        });
    }

    private void initMsgNotificationListener() {
        this.newMsgImage.setOnClickListener(this.newMsgImageOnClickListener);
        this.msgNotificationBox.setOnClickListener(this.newMsgImageOnClickListener);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 82) {
            return super.onKeyDown(keyCode, event);
        }
        if (this.moreMenuBottomNavImage != null) {
            this.moreMenuBottomNavImage.performClick();
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        getCurrentHandler().post(new Runnable() {
            public void run() {
                if (MCLibUIBaseActivity.this.msgNotificationText != null && MCLibUIBaseActivity.this.newMsgImage != null && MCLibUIBaseActivity.this.chatSessionNavImage != null) {
                    MCLibUIBaseActivity.this.newMsgImage.setVisibility(4);
                    MCLibUIBaseActivity.this.msgNotificationText.setVisibility(4);
                    MCLibUIBaseActivity.this.newMsgImage.setAnimation(null);
                    MCLibUIBaseActivity.this.chatSessionNavImage.setVisibility(0);
                    MCLibUIBaseActivity.this.broadcastToActivities();
                }
            }
        });
        super.onResume();
    }

    /* access modifiers changed from: private */
    public void broadcastToActivities() {
        int msgCount = new MCLibUserPrivateMsgServiceImpl(this).getUserUnreadMsgCount(this, new MCLibUserInfoServiceImpl(this).getLoginUserId());
        Intent intent = new Intent(getPackageName() + MCLibHeartBeatOSService.SERVER_NOTIFICATION_MSG);
        intent.putExtra(MCLibHeartBeatOSService.NEW_MSG_COUNT, msgCount);
        intent.putExtra(MCLibHeartBeatOSService.HAS_NEW_REPLY, MCLibHeartBeatOSService.hasReply);
        intent.putExtra(MCLibHeartBeatOSService.NEW_SYS_MSG_COUNT, MCLibHeartBeatOSService.sysMsgList == null ? 0 : MCLibHeartBeatOSService.sysMsgList.size());
        sendBroadcast(intent);
    }
}
