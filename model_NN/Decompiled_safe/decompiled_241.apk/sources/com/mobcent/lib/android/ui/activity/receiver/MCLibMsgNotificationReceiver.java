package com.mobcent.lib.android.ui.activity.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.widget.TextView;
import com.mobcent.android.os.service.MCLibHeartBeatOSService;
import com.mobcent.android.seed.autovGhnzo98NIzLsRPLHS.R;
import com.mobcent.android.state.MCLibAppState;
import com.mobcent.lib.android.ui.activity.MCLibUIBaseActivity;
import com.mobcent.lib.android.ui.delegate.MCLibMsgNotificationIconDelegate;
import com.mobcent.lib.android.ui.dialog.MCLibRegLoginDialog;
import java.util.HashMap;

public class MCLibMsgNotificationReceiver extends BroadcastReceiver {
    public static final String CURRENT_TYPE_HAS_REPLY = "CURRENT_TYPE_HAS_REPLY";
    public static final String CURRENT_TYPE_MSG = "CURRENT_TYPE_MSG";
    public static final String CURRENT_TYPE_SYS_MSG = "CURRENT_TYPE_SYS_MSG";
    public static HashMap<String, Boolean> magicActionDownloadStatusMap = new HashMap<>();
    private MCLibUIBaseActivity activity;
    private MCLibMsgNotificationIconDelegate delegate;
    private Handler mHandler;
    private TextView newMsgText;

    public MCLibMsgNotificationReceiver(MCLibUIBaseActivity activity2, MCLibMsgNotificationIconDelegate delegate2, Handler mHandler2) {
        this.mHandler = mHandler2;
        this.activity = activity2;
        this.delegate = delegate2;
    }

    public void onReceive(Context context, Intent intent) {
        if (MCLibAppState.isNeedRelogin == MCLibAppState.NEED_RELOGIN) {
            new MCLibRegLoginDialog(this.activity, R.style.mc_lib_dialog, true, false, null).show();
            return;
        }
        this.newMsgText = this.activity.msgNotificationText;
        if (this.newMsgText != null) {
            String msgActionCurrentType = (String) this.newMsgText.getTag();
            if (msgActionCurrentType == null) {
                this.newMsgText.setTag(CURRENT_TYPE_MSG);
                msgActionCurrentType = CURRENT_TYPE_MSG;
            }
            if (intent.getExtras() != null) {
                int msgCount = intent.getIntExtra(MCLibHeartBeatOSService.NEW_MSG_COUNT, 0);
                boolean hasReply = intent.getBooleanExtra(MCLibHeartBeatOSService.HAS_NEW_REPLY, false);
                int sysMsgCount = intent.getIntExtra(MCLibHeartBeatOSService.NEW_SYS_MSG_COUNT, 0);
                if (msgCount > 0 || hasReply || sysMsgCount > 0) {
                    this.delegate.showMsgNotificationBox(this.mHandler);
                    if (msgActionCurrentType.equals(CURRENT_TYPE_MSG)) {
                        if (msgCount <= 0 && sysMsgCount > 0) {
                            msgActionCurrentType = CURRENT_TYPE_SYS_MSG;
                        } else if (msgCount <= 0 && hasReply) {
                            msgActionCurrentType = CURRENT_TYPE_HAS_REPLY;
                        }
                    } else if (msgActionCurrentType.equals(CURRENT_TYPE_SYS_MSG)) {
                        if (sysMsgCount <= 0 && hasReply) {
                            msgActionCurrentType = CURRENT_TYPE_HAS_REPLY;
                        } else if (sysMsgCount <= 0 && msgCount > 0) {
                            msgActionCurrentType = CURRENT_TYPE_MSG;
                        }
                    } else if (msgActionCurrentType.equals(CURRENT_TYPE_HAS_REPLY)) {
                        if (!hasReply && msgCount > 0) {
                            msgActionCurrentType = CURRENT_TYPE_MSG;
                        } else if (!hasReply && sysMsgCount > 0) {
                            msgActionCurrentType = CURRENT_TYPE_SYS_MSG;
                        }
                    }
                    if (msgCount > 0 && sysMsgCount <= 0 && !hasReply) {
                        this.activity.msgNotificationText.setTag(CURRENT_TYPE_MSG);
                    } else if (sysMsgCount > 0 && msgCount <= 0 && !hasReply) {
                        this.activity.msgNotificationText.setTag(CURRENT_TYPE_SYS_MSG);
                    } else if (hasReply && msgCount <= 0 && sysMsgCount <= 0) {
                        this.activity.msgNotificationText.setTag(CURRENT_TYPE_HAS_REPLY);
                    } else if (msgCount <= 0 || sysMsgCount <= 0 || !hasReply) {
                        if (msgCount <= 0 || sysMsgCount <= 0 || hasReply) {
                            if (msgCount <= 0 || sysMsgCount > 0 || !hasReply) {
                                if (msgCount <= 0 && sysMsgCount > 0 && hasReply) {
                                    if (msgActionCurrentType.equals(CURRENT_TYPE_MSG)) {
                                        this.activity.msgNotificationText.setTag(CURRENT_TYPE_SYS_MSG);
                                    } else if (msgActionCurrentType.equals(CURRENT_TYPE_SYS_MSG)) {
                                        this.activity.msgNotificationText.setTag(CURRENT_TYPE_HAS_REPLY);
                                    } else if (msgActionCurrentType.equals(CURRENT_TYPE_HAS_REPLY)) {
                                        this.activity.msgNotificationText.setTag(CURRENT_TYPE_SYS_MSG);
                                    }
                                }
                            } else if (msgActionCurrentType.equals(CURRENT_TYPE_MSG)) {
                                this.activity.msgNotificationText.setTag(CURRENT_TYPE_HAS_REPLY);
                            } else if (msgActionCurrentType.equals(CURRENT_TYPE_SYS_MSG)) {
                                this.activity.msgNotificationText.setTag(CURRENT_TYPE_HAS_REPLY);
                            } else if (msgActionCurrentType.equals(CURRENT_TYPE_HAS_REPLY)) {
                                this.activity.msgNotificationText.setTag(CURRENT_TYPE_MSG);
                            }
                        } else if (msgActionCurrentType.equals(CURRENT_TYPE_MSG)) {
                            this.activity.msgNotificationText.setTag(CURRENT_TYPE_SYS_MSG);
                        } else if (msgActionCurrentType.equals(CURRENT_TYPE_SYS_MSG)) {
                            this.activity.msgNotificationText.setTag(CURRENT_TYPE_MSG);
                        } else if (msgActionCurrentType.equals(CURRENT_TYPE_HAS_REPLY)) {
                            this.activity.msgNotificationText.setTag(CURRENT_TYPE_MSG);
                        }
                    } else if (msgActionCurrentType.equals(CURRENT_TYPE_MSG)) {
                        this.activity.msgNotificationText.setTag(CURRENT_TYPE_SYS_MSG);
                    } else if (msgActionCurrentType.equals(CURRENT_TYPE_SYS_MSG)) {
                        this.activity.msgNotificationText.setTag(CURRENT_TYPE_HAS_REPLY);
                    } else if (msgActionCurrentType.equals(CURRENT_TYPE_HAS_REPLY)) {
                        this.activity.msgNotificationText.setTag(CURRENT_TYPE_MSG);
                    }
                    if (msgActionCurrentType.equals(CURRENT_TYPE_MSG)) {
                        this.delegate.updateCurrentIconMsg(msgCount, this.mHandler);
                    } else if (msgActionCurrentType.equals(CURRENT_TYPE_HAS_REPLY)) {
                        this.delegate.updateCurrentIconHasReply(this.mHandler);
                    } else if (msgActionCurrentType.equals(CURRENT_TYPE_SYS_MSG)) {
                        this.delegate.updateCurrentIconSysMsg(sysMsgCount, this.mHandler);
                    }
                    this.delegate.headbeatNotificationBox(this.mHandler);
                    return;
                }
                this.newMsgText.setTag(CURRENT_TYPE_MSG);
                this.delegate.hideMsgNotificationBox(this.mHandler);
            }
        }
    }
}
