package com.mobcent.lib.android.constants;

public interface MCLibConstant {
    public static final String FOLLOW_MSG_KEY = "96E79218965EB72C92A549DD5A330112";
    public static final String RS_EMAIL_EXIST = "emailExist";
    public static final String RS_FAIL = "Connection fail, pls try again.";
    public static final String RS_LOGIN_FAIL = "loginFail";
    public static final String RS_NETWORK_UNAVAILABLE = "network_unavailable";
    public static final String RS_NICK_NAME_EXIST = "nickNameExist";
    public static final String RS_REASON = "reason";
    public static final String RS_SUCC = "rs_succ";
}
