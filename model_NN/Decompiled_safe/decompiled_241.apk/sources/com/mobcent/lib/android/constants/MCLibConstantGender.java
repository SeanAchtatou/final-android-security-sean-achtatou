package com.mobcent.lib.android.constants;

public interface MCLibConstantGender {
    public static final int FEMALE = 0;
    public static final int MALE = 1;
    public static final int SECRET = 2;
}
