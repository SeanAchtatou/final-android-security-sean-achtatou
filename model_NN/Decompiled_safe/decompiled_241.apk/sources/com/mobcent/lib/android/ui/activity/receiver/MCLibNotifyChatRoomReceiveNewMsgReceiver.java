package com.mobcent.lib.android.ui.activity.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.mobcent.lib.android.ui.delegate.MCLibNotifyReceiveNewMsgDelegate;

public class MCLibNotifyChatRoomReceiveNewMsgReceiver extends BroadcastReceiver {
    private MCLibNotifyReceiveNewMsgDelegate delegate;

    public MCLibNotifyChatRoomReceiveNewMsgReceiver(MCLibNotifyReceiveNewMsgDelegate delegate2) {
        this.delegate = delegate2;
    }

    public void onReceive(Context context, Intent intent) {
        this.delegate.onReceiveNewMsg();
    }
}
