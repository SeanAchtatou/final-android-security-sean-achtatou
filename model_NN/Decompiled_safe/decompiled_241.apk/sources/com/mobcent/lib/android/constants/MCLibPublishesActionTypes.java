package com.mobcent.lib.android.constants;

public interface MCLibPublishesActionTypes {
    public static final int PUBLISH_MENU_CANCEL = 108;
    public static final int PUBLISH_MENU_EVENT_GO_TO_APP = 107;
    public static final int PUBLISH_MENU_EVENT_USER_HOME = 104;
    public static final int PUBLISH_MENU_EVENT_USER_HOME_RECOMMEND = 105;
    public static final int PUBLISH_MENU_REPLY = 101;
    public static final int PUBLISH_MENU_REPLY_LIST = 106;
    public static final int PUBLISH_MENU_USER_HOME = 103;
}
