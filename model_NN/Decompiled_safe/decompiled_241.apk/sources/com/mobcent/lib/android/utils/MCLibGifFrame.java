package com.mobcent.lib.android.utils;

import android.graphics.Bitmap;

public class MCLibGifFrame {
    public int currentFrameIndex = 0;
    public int delay;
    public Bitmap image;
    public MCLibGifFrame nextFrame = null;

    public MCLibGifFrame(Bitmap im, int del) {
        this.image = im;
        this.delay = del;
    }
}
