package com.mobcent.lib.android.ui.activity.menu;

import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import com.mobcent.android.seed.autovGhnzo98NIzLsRPLHS.R;
import com.mobcent.android.service.impl.MCLibUserInfoServiceImpl;
import com.mobcent.lib.android.ui.activity.MCLibCommunityBundleActivity;
import com.mobcent.lib.android.ui.delegate.MCLibUpdateListDelegate;

public class MCLibStatusMainBar {
    /* access modifiers changed from: private */
    public MCLibCommunityBundleActivity activity;
    private TextView atMeStatusText;
    private TextView friendsStatusText;
    private MCLibUpdateListDelegate menuUpdateStatusDelegate;
    private TextView myStatusText;
    private TextView statusHallText;
    /* access modifiers changed from: private */
    public Integer statusType;

    public void buildActionBar(MCLibCommunityBundleActivity activity2, MCLibUpdateListDelegate menuUpdateStatusDelegate2, Integer statusType2) {
        this.activity = activity2;
        this.menuUpdateStatusDelegate = menuUpdateStatusDelegate2;
        this.statusType = statusType2;
        initWidgets();
        initTopicHallAction();
        initFriendsStatusAction();
        initMyStatusAction();
        initAtMeStatusAction();
        initCurrentMenu();
    }

    private void initWidgets() {
        this.statusHallText = (TextView) this.activity.findViewById(R.id.mcLibHallStatus);
        this.friendsStatusText = (TextView) this.activity.findViewById(R.id.mcLibFriendsStatus);
        this.myStatusText = (TextView) this.activity.findViewById(R.id.mcLibMyStatus);
        this.atMeStatusText = (TextView) this.activity.findViewById(R.id.mcLibAtMeStatus);
    }

    private void initTopicHallAction() {
        this.statusHallText.setOnTouchListener(new ActionButtonOnTouchListener(1));
        this.statusHallText.setOnClickListener(new MicroblogMainBarOnClickListener(this.menuUpdateStatusDelegate, 1));
    }

    private void initFriendsStatusAction() {
        this.friendsStatusText.setOnTouchListener(new ActionButtonOnTouchListener(2));
        this.friendsStatusText.setOnClickListener(new MicroblogMainBarOnClickListener(this.menuUpdateStatusDelegate, 2));
    }

    private void initAtMeStatusAction() {
        this.atMeStatusText.setOnTouchListener(new ActionButtonOnTouchListener(4));
        this.atMeStatusText.setOnClickListener(new MicroblogMainBarOnClickListener(this.menuUpdateStatusDelegate, 4));
    }

    private void initMyStatusAction() {
        this.myStatusText.setOnTouchListener(new ActionButtonOnTouchListener(3));
        this.myStatusText.setOnClickListener(new MicroblogMainBarOnClickListener(this.menuUpdateStatusDelegate, 3));
    }

    /* access modifiers changed from: private */
    public void initCurrentMenu() {
        if (this.statusType.intValue() == 1 || this.statusType.intValue() == 5) {
            this.friendsStatusText.setBackgroundResource(R.drawable.mc_lib_blank);
            this.myStatusText.setBackgroundResource(R.drawable.mc_lib_blank);
            this.atMeStatusText.setBackgroundResource(R.drawable.mc_lib_blank);
            this.statusHallText.setBackgroundResource(R.drawable.mc_lib_submenu_bg);
            this.activity.setDisplayTypeVisibility(0);
            this.statusHallText.performClick();
        } else if (this.statusType.intValue() == 2) {
            this.statusHallText.setBackgroundResource(R.drawable.mc_lib_blank);
            this.myStatusText.setBackgroundResource(R.drawable.mc_lib_blank);
            this.atMeStatusText.setBackgroundResource(R.drawable.mc_lib_blank);
            this.friendsStatusText.setBackgroundResource(R.drawable.mc_lib_submenu_bg);
            this.activity.setDisplayTypeVisibility(8);
            this.friendsStatusText.performClick();
        } else if (this.statusType.intValue() == 3) {
            this.statusHallText.setBackgroundResource(R.drawable.mc_lib_blank);
            this.friendsStatusText.setBackgroundResource(R.drawable.mc_lib_blank);
            this.atMeStatusText.setBackgroundResource(R.drawable.mc_lib_blank);
            this.myStatusText.setBackgroundResource(R.drawable.mc_lib_submenu_bg);
            this.activity.setDisplayTypeVisibility(8);
            this.myStatusText.performClick();
        } else if (this.statusType.intValue() == 4) {
            this.statusHallText.setBackgroundResource(R.drawable.mc_lib_blank);
            this.myStatusText.setBackgroundResource(R.drawable.mc_lib_blank);
            this.friendsStatusText.setBackgroundResource(R.drawable.mc_lib_blank);
            this.atMeStatusText.setBackgroundResource(R.drawable.mc_lib_submenu_bg);
            this.activity.setDisplayTypeVisibility(8);
            this.atMeStatusText.performClick();
        }
    }

    public class ActionButtonOnTouchListener implements View.OnTouchListener {
        private int statusType;

        public ActionButtonOnTouchListener(int statusType2) {
            this.statusType = statusType2;
        }

        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() != 0) {
                if (1 == event.getAction()) {
                    Integer unused = MCLibStatusMainBar.this.statusType = Integer.valueOf(this.statusType);
                    MCLibStatusMainBar.this.initCurrentMenu();
                } else if (3 == event.getAction() || 2 == event.getAction() || 4 == event.getAction()) {
                }
            }
            return true;
        }
    }

    public class MicroblogMainBarOnClickListener implements View.OnClickListener {
        private MCLibUpdateListDelegate delegate;
        private Integer statusType;

        public MicroblogMainBarOnClickListener(MCLibUpdateListDelegate delegate2, Integer statusType2) {
            this.delegate = delegate2;
            this.statusType = statusType2;
        }

        public void onClick(View v) {
            if (this.statusType.intValue() == 1 || this.statusType.intValue() == 5) {
                this.statusType = Integer.valueOf(new MCLibUserInfoServiceImpl(MCLibStatusMainBar.this.activity).getCommunityHomeMode());
            }
            MCLibStatusMainBar.this.activity.setStatusType(this.statusType);
            Integer unused = MCLibStatusMainBar.this.statusType = this.statusType;
            this.delegate.updateList(1, 10, true);
        }
    }
}
