package com.mobcent.lib.android.ui.activity.adapter.holder;

import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MCLibUserInfoHolder {
    private RelativeLayout actionBtnBox;
    private ImageView blockBtn;
    private RelativeLayout blockContainer;
    private TextView blockTextView;
    private ImageView chatBtn;
    private RelativeLayout chatContainer;
    private TextView chatTextView;
    private TextView fansNumText;
    private RelativeLayout followActionContainer;
    private ImageView followBtn;
    private TextView followTextView;
    private TextView isFollowed;
    private TextView isOnlineText;
    private TextView lastMsgUpdatTimeTextView;
    private ImageView newChatIndicatorImage;
    private TextView sourceName;
    private RelativeLayout sourceNameContainer;
    private TextView userAge;
    private TextView userCity;
    private TextView userEmotion;
    private TextView userGender;
    private ImageView userImage;
    private TextView userName;
    private ImageView visitUserHomeBtn;
    private RelativeLayout visitUserHomeContainer;
    private TextView visitUserHomeTextView;

    public TextView getUserName() {
        return this.userName;
    }

    public void setUserName(TextView userName2) {
        this.userName = userName2;
    }

    public TextView getUserGender() {
        return this.userGender;
    }

    public void setUserGender(TextView userGender2) {
        this.userGender = userGender2;
    }

    public TextView getUserAge() {
        return this.userAge;
    }

    public void setUserAge(TextView userAge2) {
        this.userAge = userAge2;
    }

    public TextView getUserCity() {
        return this.userCity;
    }

    public void setUserCity(TextView userCity2) {
        this.userCity = userCity2;
    }

    public ImageView getUserImage() {
        return this.userImage;
    }

    public void setUserImage(ImageView userImage2) {
        this.userImage = userImage2;
    }

    public TextView getUserEmotion() {
        return this.userEmotion;
    }

    public void setUserEmotion(TextView userEmotion2) {
        this.userEmotion = userEmotion2;
    }

    public TextView getIsOnlineText() {
        return this.isOnlineText;
    }

    public void setIsOnlineText(TextView isOnlineText2) {
        this.isOnlineText = isOnlineText2;
    }

    public TextView getFansNumText() {
        return this.fansNumText;
    }

    public void setFansNumText(TextView fansNumText2) {
        this.fansNumText = fansNumText2;
    }

    public TextView getIsFollowed() {
        return this.isFollowed;
    }

    public void setIsFollowed(TextView isFollowed2) {
        this.isFollowed = isFollowed2;
    }

    public RelativeLayout getActionBtnBox() {
        return this.actionBtnBox;
    }

    public void setActionBtnBox(RelativeLayout actionBtnBox2) {
        this.actionBtnBox = actionBtnBox2;
    }

    public TextView getFollowTextView() {
        return this.followTextView;
    }

    public void setFollowTextView(TextView followTextView2) {
        this.followTextView = followTextView2;
    }

    public RelativeLayout getFollowActionContainer() {
        return this.followActionContainer;
    }

    public void setFollowActionContainer(RelativeLayout followActionContainer2) {
        this.followActionContainer = followActionContainer2;
    }

    public ImageView getFollowBtn() {
        return this.followBtn;
    }

    public void setFollowBtn(ImageView followBtn2) {
        this.followBtn = followBtn2;
    }

    public TextView getSourceName() {
        return this.sourceName;
    }

    public void setSourceName(TextView sourceName2) {
        this.sourceName = sourceName2;
    }

    public RelativeLayout getSourceNameContainer() {
        return this.sourceNameContainer;
    }

    public void setSourceNameContainer(RelativeLayout sourceNameContainer2) {
        this.sourceNameContainer = sourceNameContainer2;
    }

    public ImageView getChatBtn() {
        return this.chatBtn;
    }

    public void setChatBtn(ImageView chatBtn2) {
        this.chatBtn = chatBtn2;
    }

    public TextView getChatTextView() {
        return this.chatTextView;
    }

    public void setChatTextView(TextView chatTextView2) {
        this.chatTextView = chatTextView2;
    }

    public RelativeLayout getChatContainer() {
        return this.chatContainer;
    }

    public void setChatContainer(RelativeLayout chatContainer2) {
        this.chatContainer = chatContainer2;
    }

    public ImageView getVisitUserHomeBtn() {
        return this.visitUserHomeBtn;
    }

    public void setVisitUserHomeBtn(ImageView visitUserHomeBtn2) {
        this.visitUserHomeBtn = visitUserHomeBtn2;
    }

    public TextView getVisitUserHomeTextView() {
        return this.visitUserHomeTextView;
    }

    public void setVisitUserHomeTextView(TextView visitUserHomeTextView2) {
        this.visitUserHomeTextView = visitUserHomeTextView2;
    }

    public RelativeLayout getVisitUserHomeContainer() {
        return this.visitUserHomeContainer;
    }

    public void setVisitUserHomeContainer(RelativeLayout visitUserHomeContainer2) {
        this.visitUserHomeContainer = visitUserHomeContainer2;
    }

    public ImageView getNewChatIndicatorImage() {
        return this.newChatIndicatorImage;
    }

    public void setNewChatIndicatorImage(ImageView newChatIndicatorImage2) {
        this.newChatIndicatorImage = newChatIndicatorImage2;
    }

    public TextView getLastMsgUpdatTimeTextView() {
        return this.lastMsgUpdatTimeTextView;
    }

    public void setLastMsgUpdatTimeTextView(TextView lastMsgUpdatTimeTextView2) {
        this.lastMsgUpdatTimeTextView = lastMsgUpdatTimeTextView2;
    }

    public ImageView getBlockBtn() {
        return this.blockBtn;
    }

    public void setBlockBtn(ImageView blockBtn2) {
        this.blockBtn = blockBtn2;
    }

    public TextView getBlockTextView() {
        return this.blockTextView;
    }

    public void setBlockTextView(TextView blockTextView2) {
        this.blockTextView = blockTextView2;
    }

    public RelativeLayout getBlockContainer() {
        return this.blockContainer;
    }

    public void setBlockContainer(RelativeLayout blockContainer2) {
        this.blockContainer = blockContainer2;
    }
}
