package com.mobcent.lib.android.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.mobcent.android.constants.MCLibConstants;
import com.mobcent.android.constants.MCLibParameterKeyConstant;
import com.mobcent.android.model.MCLibUserStatus;
import com.mobcent.android.os.service.MCLibHeartBeatOSService;
import com.mobcent.android.seed.autovGhnzo98NIzLsRPLHS.R;
import com.mobcent.android.service.MCLibStatusService;
import com.mobcent.android.service.impl.MCLibStatusServiceImpl;
import com.mobcent.android.service.impl.MCLibUserInfoServiceImpl;
import com.mobcent.lib.android.ui.activity.adapter.MCLibStatusListAdapter;
import com.mobcent.lib.android.ui.activity.listener.MCLibListViewItemOnClickListener;
import com.mobcent.lib.android.ui.activity.menu.MCLibStatusMainBar;
import com.mobcent.lib.android.ui.delegate.MCLibUpdateListDelegate;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class MCLibCommunityBundleActivity extends MCLibUIBaseActivity implements MCLibUpdateListDelegate {
    public static final int STATUS_AT_ME = 4;
    public static final int STATUS_FRIENDS = 2;
    public static final int STATUS_HALL = 5;
    public static final int STATUS_MINE = 3;
    public static final int TOPIC_HALL = 1;
    /* access modifiers changed from: private */
    public MCLibStatusListAdapter adapter;
    private ListView bundleListView;
    public ImageButton changeDisplayTypeButton;
    /* access modifiers changed from: private */
    public boolean isForceRefresh = false;
    /* access modifiers changed from: private */
    public Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            if (msg.what == 1 || msg.what == 5) {
                MCLibCommunityBundleActivity.this.updateEmptyAdapter(msg.obj);
            } else if (msg.what == 2) {
                MCLibCommunityBundleActivity.this.updateExistAdapter(msg.obj);
            } else if (msg.what == 3) {
                MCLibCommunityBundleActivity.this.hideProgressBar();
            } else if (msg.what == 4) {
                MCLibCommunityBundleActivity.this.showProgressBar();
            }
        }
    };
    /* access modifiers changed from: private */
    public MCLibUpdateListDelegate menuUpdateStatusDelegate = new MCLibUpdateListDelegate() {
        public void updateList(final int page, int pageSize, final boolean isReadLocally) {
            MCLibCommunityBundleActivity.this.mHandler.sendEmptyMessage(4);
            MCLibCommunityBundleActivity.this.mHandler.sendMessage(MCLibCommunityBundleActivity.this.mHandler.obtainMessage(5, new ArrayList()));
            new Thread() {
                public void run() {
                    List<MCLibUserStatus> statusList;
                    try {
                        if (MCLibCommunityBundleActivity.this.statusType.intValue() == 4) {
                            MCLibHeartBeatOSService.hasReply = false;
                        } else {
                            MCLibCommunityBundleActivity.this.mHandler.post(new Runnable() {
                                public void run() {
                                    if (MCLibCommunityBundleActivity.this.statusType.intValue() == 1) {
                                        MCLibCommunityBundleActivity.this.changeDisplayTypeButton.setBackgroundResource(R.drawable.mc_lib_button_comm_type_time);
                                        Toast.makeText(MCLibCommunityBundleActivity.this, (int) R.string.mc_lib_forum_mode, 1).show();
                                    } else if (MCLibCommunityBundleActivity.this.statusType.intValue() == 5) {
                                        MCLibCommunityBundleActivity.this.changeDisplayTypeButton.setBackgroundResource(R.drawable.mc_lib_button_comm_type_topic);
                                        Toast.makeText(MCLibCommunityBundleActivity.this, (int) R.string.mc_lib_twitter_mode, 1).show();
                                    }
                                }
                            });
                        }
                        if (MCLibCommunityBundleActivity.this.isForceRefresh) {
                            statusList = MCLibCommunityBundleActivity.this.getUserStatuses(MCLibCommunityBundleActivity.this.statusType.intValue(), page, 10, false);
                        } else {
                            statusList = MCLibCommunityBundleActivity.this.getUserStatuses(MCLibCommunityBundleActivity.this.statusType.intValue(), page, 10, isReadLocally);
                        }
                        boolean unused = MCLibCommunityBundleActivity.this.isForceRefresh = false;
                        MCLibCommunityBundleActivity.this.mHandler.sendEmptyMessage(3);
                        if (statusList == null || statusList.size() <= 0) {
                            MCLibCommunityBundleActivity.this.showEmptyInfoTip(R.string.mc_lib_no_such_data);
                        } else {
                            MCLibCommunityBundleActivity.this.updateBundleListView(statusList);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }.start();
        }
    };
    /* access modifiers changed from: private */
    public ImageView previewImage;
    /* access modifiers changed from: private */
    public RelativeLayout previewImageBox;
    /* access modifiers changed from: private */
    public ProgressBar previewPrgBar;
    public ImageButton publishTopicButton;
    public ImageButton refreshContentButton;
    protected AbsListView.OnScrollListener statusListOnScrollListener = new AbsListView.OnScrollListener() {
        private Vector<String> currentUrls;
        private int firstVisibleItem;
        private int totalItemCount;
        private int visibleItemCount;

        public void onScrollStateChanged(AbsListView view, int scrollState) {
            if (scrollState == 0) {
                List<String> imageUrls = new ArrayList<>();
                parseCurrentUrls();
                List<String> preUrls = getPreviousUrls();
                List<String> nextUrls = getNextImageUrls();
                if (preUrls != null) {
                    imageUrls.addAll(preUrls);
                }
                if (nextUrls != null) {
                    imageUrls.addAll(nextUrls);
                }
                MCLibCommunityBundleActivity.this.adapter.asyncImageLoader.recycleBitmap(imageUrls);
            }
        }

        public void onScroll(AbsListView view, int firstVisibleItem2, int visibleItemCount2, int totalItemCount2) {
            this.firstVisibleItem = firstVisibleItem2;
            this.visibleItemCount = visibleItemCount2;
            this.totalItemCount = totalItemCount2;
        }

        private void parseCurrentUrls() {
            String imgUrlReply;
            this.currentUrls = new Vector<>();
            int firstIndex = 0;
            int endIndex = this.firstVisibleItem + this.visibleItemCount + 10;
            if (this.firstVisibleItem - 10 > 0) {
                firstIndex = this.firstVisibleItem - 10;
            }
            if (endIndex >= this.totalItemCount) {
                endIndex = this.totalItemCount;
            }
            for (int i = firstIndex; i < endIndex; i++) {
                if (((MCLibUserStatus) MCLibCommunityBundleActivity.this.userStatusList.get(i)).getUserImageUrl() != null && !((MCLibUserStatus) MCLibCommunityBundleActivity.this.userStatusList.get(i)).getUserImageUrl().equals("")) {
                    this.currentUrls.add(((MCLibUserStatus) MCLibCommunityBundleActivity.this.userStatusList.get(i)).getUserImageUrl());
                }
                String imgUrlMain = ((MCLibUserStatus) MCLibCommunityBundleActivity.this.userStatusList.get(i)).getImgUrl() + ((MCLibUserStatus) MCLibCommunityBundleActivity.this.userStatusList.get(i)).getPhotoPath();
                if (imgUrlMain != null && !imgUrlMain.equals("")) {
                    this.currentUrls.add(imgUrlMain);
                }
                if (!(((MCLibUserStatus) MCLibCommunityBundleActivity.this.userStatusList.get(i)).getReplyStatus() == null || (imgUrlReply = ((MCLibUserStatus) MCLibCommunityBundleActivity.this.userStatusList.get(i)).getReplyStatus().getImgUrl() + ((MCLibUserStatus) MCLibCommunityBundleActivity.this.userStatusList.get(i)).getReplyStatus().getPhotoPath()) == null || imgUrlReply.equals(""))) {
                    this.currentUrls.add(imgUrlReply);
                }
            }
        }

        private List<String> getPreviousUrls() {
            String imgUrlReply;
            if (this.firstVisibleItem - 10 <= 0) {
                return null;
            }
            List<String> urls = new ArrayList<>();
            for (int i = 0; i < this.firstVisibleItem - 10; i++) {
                if (((MCLibUserStatus) MCLibCommunityBundleActivity.this.userStatusList.get(i)).getUserImageUrl() != null && !((MCLibUserStatus) MCLibCommunityBundleActivity.this.userStatusList.get(i)).getUserImageUrl().equals("")) {
                    if (!this.currentUrls.contains(((MCLibUserStatus) MCLibCommunityBundleActivity.this.userStatusList.get(i)).getUserImageUrl())) {
                        urls.add(((MCLibUserStatus) MCLibCommunityBundleActivity.this.userStatusList.get(i)).getUserImageUrl());
                    }
                    String imgUrlMain = ((MCLibUserStatus) MCLibCommunityBundleActivity.this.userStatusList.get(i)).getImgUrl() + ((MCLibUserStatus) MCLibCommunityBundleActivity.this.userStatusList.get(i)).getPhotoPath();
                    if (imgUrlMain != null && !imgUrlMain.equals("") && !this.currentUrls.contains(imgUrlMain)) {
                        urls.add(imgUrlMain);
                    }
                    if (((MCLibUserStatus) MCLibCommunityBundleActivity.this.userStatusList.get(i)).getReplyStatus() != null && (imgUrlReply = ((MCLibUserStatus) MCLibCommunityBundleActivity.this.userStatusList.get(i)).getReplyStatus().getImgUrl() + ((MCLibUserStatus) MCLibCommunityBundleActivity.this.userStatusList.get(i)).getReplyStatus().getPhotoPath()) != null && !imgUrlReply.equals("") && !this.currentUrls.contains(imgUrlReply)) {
                        urls.add(imgUrlReply);
                    }
                }
            }
            return urls;
        }

        private List<String> getNextImageUrls() {
            String imgUrlReply;
            int currentVisibleBottom = this.firstVisibleItem + this.visibleItemCount;
            if (this.totalItemCount - currentVisibleBottom <= 10) {
                return null;
            }
            List<String> urls = new ArrayList<>();
            for (int i = currentVisibleBottom + 10; i < this.totalItemCount; i++) {
                if (((MCLibUserStatus) MCLibCommunityBundleActivity.this.userStatusList.get(i)).getUserImageUrl() != null && !((MCLibUserStatus) MCLibCommunityBundleActivity.this.userStatusList.get(i)).getUserImageUrl().equals("")) {
                    if (!this.currentUrls.contains(((MCLibUserStatus) MCLibCommunityBundleActivity.this.userStatusList.get(i)).getUserImageUrl())) {
                        urls.add(((MCLibUserStatus) MCLibCommunityBundleActivity.this.userStatusList.get(i)).getUserImageUrl());
                    }
                    String imgUrlMain = ((MCLibUserStatus) MCLibCommunityBundleActivity.this.userStatusList.get(i)).getImgUrl() + ((MCLibUserStatus) MCLibCommunityBundleActivity.this.userStatusList.get(i)).getPhotoPath();
                    if (imgUrlMain != null && !imgUrlMain.equals("") && !this.currentUrls.contains(imgUrlMain)) {
                        urls.add(imgUrlMain);
                    }
                    if (((MCLibUserStatus) MCLibCommunityBundleActivity.this.userStatusList.get(i)).getReplyStatus() != null && (imgUrlReply = ((MCLibUserStatus) MCLibCommunityBundleActivity.this.userStatusList.get(i)).getReplyStatus().getImgUrl() + ((MCLibUserStatus) MCLibCommunityBundleActivity.this.userStatusList.get(i)).getReplyStatus().getPhotoPath()) != null && !imgUrlReply.equals("") && !this.currentUrls.contains(imgUrlReply)) {
                        urls.add(imgUrlReply);
                    }
                }
            }
            return urls;
        }
    };
    /* access modifiers changed from: private */
    public Integer statusType = 1;
    /* access modifiers changed from: private */
    public List<MCLibUserStatus> userStatusList;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.mc_lib_community_bundle);
        initWindowTitle();
        initSysMsgWidgets();
        if (getIntent().getExtras() == null) {
            this.statusType = Integer.valueOf(new MCLibUserInfoServiceImpl(this).getCommunityHomeMode());
        } else if (getIntent().getExtras().get(MCLibParameterKeyConstant.STATUS_TYPE) != null) {
            this.statusType = (Integer) getIntent().getExtras().get(MCLibParameterKeyConstant.STATUS_TYPE);
            this.isForceRefresh = getIntent().getBooleanExtra(MCLibParameterKeyConstant.IS_FORCE_REFRESH, false);
            Toast.makeText(this, (int) R.string.mc_lib_new_reply, 0).show();
        }
        initWidgets();
        initCommunityMenu();
        initLoginUserHeader();
        initNavBottomBar(100);
    }

    private void initWidgets() {
        this.bundleListView = (ListView) findViewById(R.id.mcLibBundledListView);
        this.publishTopicButton = (ImageButton) findViewById(R.id.mcLibPublishTopicBtn);
        this.changeDisplayTypeButton = (ImageButton) findViewById(R.id.mcLibChangeCommunityHallDispTypeBtn);
        this.refreshContentButton = (ImageButton) findViewById(R.id.mcLibRefreshContentBtn);
        this.previewImageBox = (RelativeLayout) findViewById(R.id.mcLibPreviewImageBox);
        this.previewPrgBar = (ProgressBar) findViewById(R.id.mcLibPreviewProgressBar);
        this.previewImage = (ImageView) findViewById(R.id.mcLibPreviewImage);
        this.previewImageBox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCLibCommunityBundleActivity.this.hidePreviewImage();
            }
        });
        initProgressBox();
        initBundleListView();
        initPublishButton();
        initChangeCommunityDisplayTypeButton();
        initRefreshContentButton();
    }

    private void initRefreshContentButton() {
        this.refreshContentButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCLibCommunityBundleActivity.this.menuUpdateStatusDelegate.updateList(1, 10, false);
            }
        });
    }

    private void initChangeCommunityDisplayTypeButton() {
        this.changeDisplayTypeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCLibCommunityBundleActivity.this.mHandler.postDelayed(new Runnable() {
                    public void run() {
                        if (MCLibCommunityBundleActivity.this.statusType.intValue() == 1) {
                            Integer unused = MCLibCommunityBundleActivity.this.statusType = 5;
                        } else if (MCLibCommunityBundleActivity.this.statusType.intValue() == 5) {
                            Integer unused2 = MCLibCommunityBundleActivity.this.statusType = 1;
                        }
                        new MCLibUserInfoServiceImpl(MCLibCommunityBundleActivity.this).updateCommunityHomeMode(MCLibCommunityBundleActivity.this.statusType.intValue());
                        MCLibCommunityBundleActivity.this.menuUpdateStatusDelegate.updateList(1, 10, true);
                    }
                }, 200);
            }
        });
    }

    private void initPublishButton() {
        this.publishTopicButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(MCLibCommunityBundleActivity.this, MCLibPublishStatusActivity.class);
                intent.putExtra(MCLibParameterKeyConstant.STATUS_ROOT_ID, 0);
                intent.putExtra(MCLibParameterKeyConstant.STATUS_REPLY_ID, 0);
                MCLibCommunityBundleActivity.this.startActivity(intent);
            }
        });
    }

    private void initBundleListView() {
        this.bundleListView.setOnItemClickListener(new MCLibListViewItemOnClickListener());
        this.bundleListView.setOnScrollListener(this.statusListOnScrollListener);
    }

    private void initCommunityMenu() {
        new MCLibStatusMainBar().buildActionBar(this, this.menuUpdateStatusDelegate, this.statusType);
    }

    /* access modifiers changed from: private */
    public void updateBundleListView(List<MCLibUserStatus> list) {
        if (this.adapter == null) {
            this.mHandler.sendMessage(this.mHandler.obtainMessage(1, list));
            return;
        }
        this.mHandler.sendMessage(this.mHandler.obtainMessage(2, list));
    }

    public void updateList(final int page, final int pageSize, final boolean isReadLocally) {
        new Thread() {
            public void run() {
                List<MCLibUserStatus> nextPageUserStatusList;
                List<MCLibUserStatus> resultList = new ArrayList<>();
                if (page == 1) {
                    MCLibCommunityBundleActivity.this.mHandler.sendEmptyMessage(4);
                    MCLibCommunityBundleActivity.this.updateBundleListView(resultList);
                    if (MCLibCommunityBundleActivity.this.userStatusList != null) {
                        MCLibCommunityBundleActivity.this.userStatusList.clear();
                    }
                }
                if (MCLibCommunityBundleActivity.this.userStatusList == null || MCLibCommunityBundleActivity.this.userStatusList.isEmpty()) {
                    nextPageUserStatusList = MCLibCommunityBundleActivity.this.getUserStatuses(MCLibCommunityBundleActivity.this.statusType.intValue(), page, pageSize, false);
                } else {
                    nextPageUserStatusList = MCLibCommunityBundleActivity.this.getUserStatuses(MCLibCommunityBundleActivity.this.statusType.intValue(), page, pageSize, isReadLocally);
                }
                if (nextPageUserStatusList != null && nextPageUserStatusList.size() > 0 && page == 1) {
                    MCLibStatusListAdapter unused = MCLibCommunityBundleActivity.this.adapter = null;
                    resultList = nextPageUserStatusList;
                } else if (nextPageUserStatusList != null && nextPageUserStatusList.size() > 0) {
                    resultList.addAll(MCLibCommunityBundleActivity.this.userStatusList);
                    resultList.remove(resultList.size() - 1);
                    resultList.addAll(nextPageUserStatusList);
                } else if (MCLibCommunityBundleActivity.this.userStatusList.size() > 0) {
                    resultList.addAll(MCLibCommunityBundleActivity.this.userStatusList);
                    resultList.remove(resultList.size() - 1);
                }
                MCLibCommunityBundleActivity.this.mHandler.sendEmptyMessage(3);
                MCLibCommunityBundleActivity.this.updateBundleListView(resultList);
            }
        }.start();
    }

    public List<MCLibUserStatus> getUserStatuses(int type, int page, int pageSize, boolean isReadLocally) {
        MCLibStatusService statusService = new MCLibStatusServiceImpl(this);
        int uid = new MCLibUserInfoServiceImpl(this).getLoginUserId();
        if (type == 1) {
            return statusService.getHallTopic(uid, page, pageSize, isReadLocally);
        }
        if (type == 2) {
            return statusService.getFollowedUserStatus(uid, page, pageSize, isReadLocally);
        }
        if (type == 3) {
            return statusService.getMyStatus(uid, uid, page, pageSize, isReadLocally, true);
        }
        if (type == 4) {
            return statusService.getTalkToMeStatus(uid, page, pageSize, isReadLocally);
        }
        if (type == 5) {
            return statusService.getHallStatus(uid, page, pageSize, isReadLocally);
        }
        return statusService.getFollowedUserStatus(uid, page, pageSize, isReadLocally);
    }

    /* access modifiers changed from: private */
    public void updateEmptyAdapter(Object obj) {
        this.adapter = new MCLibStatusListAdapter(this, this.bundleListView, R.layout.mc_lib_status_list_row, (List) obj, this, false, false, this.mHandler);
        this.bundleListView.setAdapter((ListAdapter) this.adapter);
        this.userStatusList = (List) obj;
    }

    /* access modifiers changed from: private */
    public void updateExistAdapter(Object obj) {
        this.adapter.setStatusList((List) obj);
        this.adapter.notifyDataSetInvalidated();
        this.adapter.notifyDataSetChanged();
        this.userStatusList = (List) obj;
    }

    public void setDisplayTypeVisibility(final int visibility) {
        this.mHandler.post(new Runnable() {
            public void run() {
                MCLibCommunityBundleActivity.this.changeDisplayTypeButton.setVisibility(visibility);
            }
        });
    }

    public void showPreviewImage(final String imagePath) {
        this.mHandler.post(new Runnable() {
            public void run() {
                MCLibCommunityBundleActivity.this.previewImage.setVisibility(0);
                MCLibCommunityBundleActivity.this.previewImageBox.setVisibility(0);
                MCLibCommunityBundleActivity.this.previewPrgBar.setVisibility(0);
                Animation anim = AnimationUtils.loadAnimation(MCLibCommunityBundleActivity.this, R.anim.mc_lib_grow_from_middle);
                anim.setInterpolator(new AccelerateInterpolator());
                MCLibCommunityBundleActivity.this.previewImage.startAnimation(anim);
                MCLibCommunityBundleActivity.this.adapter.updateImage(0, imagePath, MCLibConstants.RESOLUTION_320X480, MCLibCommunityBundleActivity.this.previewImage, MCLibCommunityBundleActivity.this.previewPrgBar, true, true);
            }
        });
    }

    public void hidePreviewImage() {
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.mc_lib_shrink_to_middle);
        anim.setInterpolator(new AccelerateInterpolator());
        anim.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
                MCLibCommunityBundleActivity.this.mHandler.post(new Runnable() {
                    public void run() {
                        MCLibCommunityBundleActivity.this.previewImage.setVisibility(4);
                        MCLibCommunityBundleActivity.this.previewImageBox.setVisibility(4);
                        MCLibCommunityBundleActivity.this.previewPrgBar.setVisibility(4);
                        MCLibCommunityBundleActivity.this.previewImage.setBackgroundResource(R.drawable.mc_lib_unloaded_img);
                    }
                });
            }
        });
        this.previewImage.startAnimation(anim);
    }

    public Integer getStatusType() {
        return this.statusType;
    }

    public void setStatusType(Integer statusType2) {
        this.statusType = statusType2;
    }

    public Handler getCurrentHandler() {
        return this.mHandler;
    }
}
