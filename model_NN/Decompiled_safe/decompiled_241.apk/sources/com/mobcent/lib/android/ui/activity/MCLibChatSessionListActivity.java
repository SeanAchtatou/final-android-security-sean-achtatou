package com.mobcent.lib.android.ui.activity;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.mobcent.android.model.MCLibUserInfo;
import com.mobcent.android.seed.autovGhnzo98NIzLsRPLHS.R;
import com.mobcent.android.service.impl.MCLibRecentChatUserServiceImpl;
import com.mobcent.lib.android.ui.activity.adapter.MCLibChatSessionListAdapter;
import com.mobcent.lib.android.ui.activity.listener.MCLibListViewItemOnClickListener;
import java.util.List;

public class MCLibChatSessionListActivity extends MCLibUIBaseActivity {
    private MCLibChatSessionListAdapter adapter;
    private Button backBtn;
    private ListView bundleListView;
    private Handler mHandler = new Handler();
    private List<MCLibUserInfo> userInfoList;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.mc_lib_chat_session_list);
        initWindowTitle();
        initSysMsgWidgets();
        this.bundleListView = (ListView) findViewById(R.id.mcLibBundledListView);
        this.bundleListView.setOnItemClickListener(new MCLibListViewItemOnClickListener());
        this.backBtn = (Button) findViewById(R.id.mcLibBackBtn);
        this.backBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCLibChatSessionListActivity.this.finish();
            }
        });
        initLoginUserHeader();
        initNavBottomBar(104);
        updateChatSessionList();
    }

    private void updateChatSessionList() {
        this.userInfoList = new MCLibRecentChatUserServiceImpl(this).getRecentChatUsers();
        this.adapter = new MCLibChatSessionListAdapter(this, this.bundleListView, R.layout.mc_lib_chat_session_list_row, this.userInfoList, this.mHandler);
        this.bundleListView.setAdapter((ListAdapter) this.adapter);
    }

    public Handler getCurrentHandler() {
        return this.mHandler;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        updateChatSessionList();
    }
}
