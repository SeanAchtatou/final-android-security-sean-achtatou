package com.mobcent.lib.android.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.mobcent.android.constants.MCLibParameterKeyConstant;
import com.mobcent.android.seed.autovGhnzo98NIzLsRPLHS.R;
import com.mobcent.android.service.MCLibUserInfoService;
import com.mobcent.android.service.impl.MCLibUserInfoServiceImpl;
import com.mobcent.lib.android.ui.activity.model.MCLibGoToActivityModel;
import com.mobcent.lib.android.utils.MCLibStringUtil;

public class MCLibGuidePwdActivity extends MCLibUIBaseActivity {
    /* access modifiers changed from: private */
    public EditText confirmField;
    /* access modifiers changed from: private */
    public MCLibGoToActivityModel goToActivityModel;
    private Handler mHandler = new Handler();
    /* access modifiers changed from: private */
    public Button nextButton;
    /* access modifiers changed from: private */
    public EditText passwordField;
    /* access modifiers changed from: private */
    public ProgressBar progressBar;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.mc_lib_guide_pwd);
        this.goToActivityModel = (MCLibGoToActivityModel) getIntent().getSerializableExtra(MCLibParameterKeyConstant.GO_TO_ACTIVITY_CLASS);
        initWindowTitle();
        initWidgets();
        initFinishButton();
    }

    private void initWidgets() {
        this.passwordField = (EditText) findViewById(R.id.mcLibUserPasswordField);
        this.confirmField = (EditText) findViewById(R.id.mcLibConfirmPwdField);
        this.nextButton = (Button) findViewById(R.id.mcLibFinishBtn);
        this.progressBar = (ProgressBar) findViewById(R.id.mcLibProgressBar);
    }

    private void initFinishButton() {
        this.nextButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                new Thread() {
                    public void run() {
                        String newPwd = MCLibGuidePwdActivity.this.passwordField.getText().toString();
                        String confirmPwd = MCLibGuidePwdActivity.this.confirmField.getText().toString();
                        if (newPwd == null || newPwd.length() > 12 || newPwd.length() < 6) {
                            MCLibGuidePwdActivity.this.showToastMsg(R.string.mc_lib_password_length_tip);
                        } else if (!MCLibStringUtil.isPwdMatchRule(newPwd)) {
                            MCLibGuidePwdActivity.this.showToastMsg(R.string.mc_lib_password_invalid_tip);
                        } else if (!newPwd.equals(confirmPwd)) {
                            MCLibGuidePwdActivity.this.showToastMsg(R.string.mc_lib_password_not_same);
                        } else {
                            MCLibGuidePwdActivity.this.showWaitingPrg();
                            MCLibUserInfoService userInfoService = new MCLibUserInfoServiceImpl(MCLibGuidePwdActivity.this);
                            if (userInfoService.changeUserPwd(userInfoService.getLoginUserId(), newPwd).equals("rs_succ")) {
                                Intent intent = new Intent(MCLibGuidePwdActivity.this, MCLibGuideSyncActivity.class);
                                if (MCLibGuidePwdActivity.this.goToActivityModel != null) {
                                    intent.putExtra(MCLibParameterKeyConstant.GO_TO_ACTIVITY_CLASS, MCLibGuidePwdActivity.this.goToActivityModel);
                                }
                                MCLibGuidePwdActivity.this.startActivity(intent);
                                MCLibGuidePwdActivity.this.finish();
                                return;
                            }
                            MCLibGuidePwdActivity.this.showToastMsg(R.string.mc_lib_generic_error);
                            MCLibGuidePwdActivity.this.hideWaitingPrg();
                        }
                    }
                }.start();
            }
        });
    }

    /* access modifiers changed from: private */
    public void showToastMsg(final int msg) {
        this.mHandler.post(new Runnable() {
            public void run() {
                Toast.makeText(MCLibGuidePwdActivity.this, msg, 1).show();
            }
        });
    }

    /* access modifiers changed from: private */
    public void showWaitingPrg() {
        this.mHandler.post(new Runnable() {
            public void run() {
                MCLibGuidePwdActivity.this.nextButton.setVisibility(4);
                MCLibGuidePwdActivity.this.progressBar.setVisibility(0);
            }
        });
    }

    /* access modifiers changed from: private */
    public void hideWaitingPrg() {
        this.mHandler.post(new Runnable() {
            public void run() {
                MCLibGuidePwdActivity.this.nextButton.setVisibility(0);
                MCLibGuidePwdActivity.this.progressBar.setVisibility(4);
            }
        });
    }

    public Handler getCurrentHandler() {
        return this.mHandler;
    }
}
