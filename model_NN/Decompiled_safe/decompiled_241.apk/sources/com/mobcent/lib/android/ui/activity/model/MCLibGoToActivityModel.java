package com.mobcent.lib.android.ui.activity.model;

import java.io.Serializable;

public class MCLibGoToActivityModel implements Serializable {
    private static final long serialVersionUID = 4288139110298900790L;
    private Class<?> goToActicityClass;

    public Class<?> getGoToActicityClass() {
        return this.goToActicityClass;
    }

    public MCLibGoToActivityModel(Class<?> goToActicityClass2) {
        this.goToActicityClass = goToActicityClass2;
    }
}
