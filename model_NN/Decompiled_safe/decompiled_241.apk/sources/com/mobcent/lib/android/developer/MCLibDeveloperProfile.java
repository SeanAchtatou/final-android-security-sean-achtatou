package com.mobcent.lib.android.developer;

import android.content.Context;
import com.mobcent.android.seed.autovGhnzo98NIzLsRPLHS.R;

public class MCLibDeveloperProfile {
    public static String getBpAppName(Context context) {
        return context.getApplicationInfo().loadLabel(context.getPackageManager()).toString();
    }

    public static String getBpAppKey(Context context) {
        return context.getResources().getString(R.string.mc_lib_bp_app_key);
    }
}
