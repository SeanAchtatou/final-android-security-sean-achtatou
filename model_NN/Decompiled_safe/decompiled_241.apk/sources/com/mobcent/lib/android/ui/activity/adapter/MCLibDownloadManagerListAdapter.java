package com.mobcent.lib.android.ui.activity.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.mobcent.android.model.MCLibDownloadProfileModel;
import com.mobcent.android.os.service.MCLibDownloadMonitorService;
import com.mobcent.android.seed.autovGhnzo98NIzLsRPLHS.R;
import com.mobcent.android.service.MCLibAppDownloaderService;
import com.mobcent.android.service.MCLibDownloadManagerService;
import com.mobcent.android.service.impl.MCLibAppDownloaderServiceImpl;
import com.mobcent.android.service.impl.MCLibDownloadManagerServiceImpl;
import com.mobcent.android.service.installer.manager.impl.MCLibInstallerManagerImpl;
import com.mobcent.android.utils.MCLibIOUtil;
import com.mobcent.lib.android.utils.i18n.MCLibStringBundleUtil;
import java.io.File;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;

public class MCLibDownloadManagerListAdapter extends ArrayAdapter<MCLibDownloadProfileModel> {
    /* access modifiers changed from: private */
    public final Activity context;
    /* access modifiers changed from: private */
    public List<MCLibDownloadProfileModel> downloadModels;
    private LayoutInflater inflater;
    private ProgressBar progressBar;
    private int rowResourceId;

    public MCLibDownloadManagerListAdapter(Activity context2, int rowResourceId2, List<MCLibDownloadProfileModel> allModels) {
        super(context2, rowResourceId2, allModels);
        this.rowResourceId = rowResourceId2;
        this.inflater = LayoutInflater.from(context2);
        this.context = context2;
        this.downloadModels = allModels;
    }

    public int getCount() {
        return this.downloadModels.size();
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        MCLibDownloadProfileModel appDownloadModel = this.downloadModels.get(position);
        MCLibDownloadProfileModel updateAppModel = getDownloadModel(appDownloadModel.getAppId());
        View row = this.inflater.inflate(this.rowResourceId, (ViewGroup) null);
        RelativeLayout stopResumeBox = (RelativeLayout) row.findViewById(R.id.mcLibStopResumeBox);
        ImageButton stopResumeBtn = (ImageButton) row.findViewById(R.id.mcLibStopResumeBtn);
        RelativeLayout cancelDownloadBox = (RelativeLayout) row.findViewById(R.id.mcLibCancelDownloadBox);
        ImageButton cancelDownloadBtn = (ImageButton) row.findViewById(R.id.mcLibCancelDownloadBtn);
        TextView appName = (TextView) row.findViewById(R.id.mcLibAppName);
        TextView appDownloadStatus = (TextView) row.findViewById(R.id.mcLibAppDownloadStatus);
        TextView appPercent = (TextView) row.findViewById(R.id.mcLibAppPercent);
        if (updateAppModel.getStatus() == 0 && updateAppModel.getStopReq() == 1) {
            stopResumeBtn.setBackgroundResource(R.drawable.mc_lib_download_button2_n);
        } else if (updateAppModel.getStatus() == 1) {
            stopResumeBtn.setBackgroundResource(R.drawable.mc_lib_download_button3_n);
        } else if (updateAppModel.getStatus() == 2) {
            stopResumeBtn.setBackgroundResource(R.drawable.mc_lib_download_button4_n);
        } else if (updateAppModel.getStatus() == 3) {
            stopResumeBtn.setVisibility(4);
            cancelDownloadBtn.setVisibility(4);
        }
        appName.setText(updateAppModel.getAppName());
        if (updateAppModel.getStatus() == 3) {
            appDownloadStatus.setText((int) R.string.mc_lib_app_removed);
            appPercent.setText("");
        } else {
            appDownloadStatus.setText(MCLibStringBundleUtil.resolveString(R.string.mc_lib_app_download_size, new String[]{getFormattedFileSize(updateAppModel.getDownloadSize()) + " / " + getFormattedFileSize(updateAppModel.getFileSize())}, this.context));
            appPercent.setText(getDownloadPercent(updateAppModel.getDownloadSize(), updateAppModel.getFileSize()));
        }
        stopResumeBtn.setTag(Integer.valueOf(appDownloadModel.getStatus()));
        initStopResumeBtn(updateAppModel, stopResumeBtn, stopResumeBox);
        initCancelBtn(updateAppModel, position, cancelDownloadBtn, cancelDownloadBox);
        updateProgressBar(updateAppModel, row, stopResumeBtn);
        return row;
    }

    /* access modifiers changed from: private */
    public MCLibDownloadProfileModel getDownloadModel(int appId) {
        return new MCLibDownloadManagerServiceImpl(this.context).getDownloadProfileModel(appId);
    }

    private void updateProgressBar(MCLibDownloadProfileModel appDownloadModel, View row, ImageButton stopResumeBtn) {
        this.progressBar = (ProgressBar) row.findViewById(R.id.mcLibDownloadProgressBar);
        int progress = 0;
        if (appDownloadModel.getStatus() == 2) {
            progress = 100;
        } else if (appDownloadModel.getFileSize() > 0) {
            progress = (int) (100.0d * (((double) appDownloadModel.getDownloadSize()) / Double.parseDouble("" + appDownloadModel.getFileSize())));
        }
        if (progress == 100) {
            stopResumeBtn.setBackgroundResource(R.drawable.mc_lib_download_button4_n);
        }
        this.progressBar.setProgress(progress);
    }

    private void initStopResumeBtn(final MCLibDownloadProfileModel downloadModel, final ImageButton stopResumeBtn, RelativeLayout stopResumeBox) {
        stopResumeBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String filePath;
                MCLibDownloadManagerService dms = new MCLibDownloadManagerServiceImpl(MCLibDownloadManagerListAdapter.this.context);
                MCLibDownloadProfileModel model = dms.getDownloadProfileModel(downloadModel.getAppId());
                if (model.getStatus() == 1) {
                    v.setBackgroundResource(R.drawable.mc_lib_download_button2_n);
                    model.setStopReq(1);
                    try {
                        dms.addOrUpdateDownloadProfile(model, true);
                    } catch (Exception e) {
                    }
                } else if (model.getStatus() == 0 && model.getStopReq() == 1) {
                    model.setStopReq(0);
                    try {
                        dms.addOrUpdateDownloadProfile(model, true);
                    } catch (Exception e2) {
                    }
                    MCLibDownloadManagerListAdapter.this.context.startService(new Intent(MCLibDownloadManagerListAdapter.this.context, MCLibDownloadMonitorService.class));
                } else if (model.getStatus() == 2 && (filePath = MCLibAppDownloaderServiceImpl.getAppFileLocation(model.getUrl(), MCLibDownloadManagerListAdapter.this.context)) != null && !filePath.equals("")) {
                    File file = new File(filePath);
                    if (file.exists()) {
                        new MCLibInstallerManagerImpl(MCLibDownloadManagerListAdapter.this.context).doInstallPkg(file);
                    }
                }
            }
        });
        stopResumeBtn.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                MCLibDownloadProfileModel model = MCLibDownloadManagerListAdapter.this.getDownloadModel(downloadModel.getAppId());
                if (event.getAction() == 0) {
                    if (model.getStatus() == 1) {
                        v.setBackgroundResource(R.drawable.mc_lib_download_button3_h);
                        return false;
                    } else if (model.getStatus() == 0 && model.getStopReq() == 1) {
                        v.setBackgroundResource(R.drawable.mc_lib_download_button2_h);
                        return false;
                    } else if (model.getStatus() == 2) {
                        v.setBackgroundResource(R.drawable.mc_lib_download_button4_h);
                        return false;
                    } else {
                        v.setBackgroundResource(R.drawable.mc_lib_download_button5_h);
                        return false;
                    }
                } else if (event.getAction() == 1) {
                    if (model.getStatus() == 1) {
                        v.setBackgroundResource(R.drawable.mc_lib_download_button2_n);
                        return false;
                    } else if (model.getStatus() == 2) {
                        v.setBackgroundResource(R.drawable.mc_lib_download_button4_n);
                        return false;
                    } else {
                        v.setBackgroundResource(R.drawable.mc_lib_download_button5_n);
                        return false;
                    }
                } else if (event.getAction() != 3) {
                    return false;
                } else {
                    if (model.getStatus() == 1) {
                        v.setBackgroundResource(R.drawable.mc_lib_download_button2_n);
                        return false;
                    } else if (model.getStatus() == 2) {
                        v.setBackgroundResource(R.drawable.mc_lib_download_button4_n);
                        return false;
                    } else {
                        v.setBackgroundResource(R.drawable.mc_lib_download_button5_n);
                        return false;
                    }
                }
            }
        });
        stopResumeBox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                stopResumeBtn.performClick();
            }
        });
    }

    private void initCancelBtn(final MCLibDownloadProfileModel downloadModel, final int position, ImageButton cancelDownloadBtn, RelativeLayout cancelDownloadBox) {
        cancelDownloadBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCLibDownloadManagerListAdapter.this.showRemoveWarningDialog(downloadModel, position);
            }
        });
        cancelDownloadBox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                MCLibDownloadManagerListAdapter.this.showRemoveWarningDialog(downloadModel, position);
            }
        });
    }

    /* access modifiers changed from: private */
    public void showRemoveWarningDialog(final MCLibDownloadProfileModel appModel, final int position) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this.context);
        alertDialog.setTitle((int) R.string.mc_lib_tips);
        alertDialog.setMessage(MCLibStringBundleUtil.resolveString(R.string.mc_lib_confirm_remove_app, new String[]{appModel.getAppName()}, this.context));
        alertDialog.setPositiveButton((int) R.string.mc_lib_confirm, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                MCLibDownloadManagerService dms = new MCLibDownloadManagerServiceImpl(MCLibDownloadManagerListAdapter.this.context);
                MCLibDownloadProfileModel model = MCLibDownloadManagerListAdapter.this.getDownloadModel(appModel.getAppId());
                if (model.getStatus() != 1) {
                    File f = new File(MCLibDownloadManagerListAdapter.this.getAppFileLocation(model.getUrl()));
                    if (f.exists() && f.isFile()) {
                        f.delete();
                    }
                }
                model.setStatus(3);
                try {
                    dms.addOrUpdateDownloadProfile(model, true);
                } catch (Exception e) {
                }
                MCLibDownloadManagerListAdapter.this.remove(MCLibDownloadManagerListAdapter.this.downloadModels.get(position));
                MCLibDownloadManagerListAdapter.this.notifyDataSetInvalidated();
                MCLibDownloadManagerListAdapter.this.notifyDataSetChanged();
            }
        });
        alertDialog.setNegativeButton((int) R.string.mc_lib_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        });
        alertDialog.show();
    }

    private String getFormattedFileSize(int size) {
        NumberFormat formatter = new DecimalFormat("#0.00");
        if (size >= 1024000) {
            return formatter.format(((double) size) / 1024000.0d) + "MB";
        } else if (size < 1024) {
            return size + "B";
        } else {
            return formatter.format(((double) size) / 1024.0d) + "KB";
        }
    }

    private String getDownloadPercent(int downloadSize, int totalSize) {
        if (totalSize <= 0) {
            return "0%";
        }
        return NumberFormat.getPercentInstance().format(((double) downloadSize) / Double.parseDouble("" + totalSize));
    }

    public List<MCLibDownloadProfileModel> getDownloadModels() {
        return this.downloadModels;
    }

    public void setDownloadModels(List<MCLibDownloadProfileModel> downloadModels2) {
        this.downloadModels = downloadModels2;
    }

    private String getAppPath() {
        return MCLibIOUtil.getBaseLocalLocation(this.context) + MCLibAppDownloaderService.LOCAL_POSITION_DIR;
    }

    /* access modifiers changed from: private */
    public String getAppFileLocation(String url) {
        String appPath = getAppPath();
        return appPath + MCLibIOUtil.FS + url.substring(url.lastIndexOf("/") + 1);
    }
}
