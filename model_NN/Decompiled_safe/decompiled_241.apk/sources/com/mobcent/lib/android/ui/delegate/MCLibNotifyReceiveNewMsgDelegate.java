package com.mobcent.lib.android.ui.delegate;

public interface MCLibNotifyReceiveNewMsgDelegate {
    void onReceiveNewMsg();
}
