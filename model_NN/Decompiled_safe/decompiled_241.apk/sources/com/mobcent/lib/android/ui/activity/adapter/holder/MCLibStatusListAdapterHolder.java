package com.mobcent.lib.android.ui.activity.adapter.holder;

import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MCLibStatusListAdapterHolder {
    private RelativeLayout actionBtnBox;
    private ImageView bulletinIconIndicator;
    private TextView floorTextView;
    private TextView mainContentTextView;
    private ImageView mainPhotoImage;
    private ImageView mainPhotoIndicator;
    private ProgressBar mainPhotoPrgBar;
    private LinearLayout microblogBody;
    private TextView pubTimeTextView;
    private TextView referContentTextView;
    private TextView referTitleTextView;
    private RelativeLayout replyBox;
    private ImageView replyBtn;
    private RelativeLayout replyContainer;
    private TextView replyNumTextView;
    private ImageView replyPhotoImage;
    private ProgressBar replyPhotoPrgBar;
    private TextView replyTextView;
    private ImageView replyThreadBtn;
    private RelativeLayout replyThreadContainer;
    private TextView replyThreadTextView;
    private ImageView reviewSourceArticleBtn;
    private RelativeLayout reviewSourceArticleContainer;
    private ImageView shareContentBtn;
    private RelativeLayout shareContentContainer;
    private TextView sourceName;
    private ImageView topIconIndicator;
    private TextView userNameTextView;
    private ImageView userPhotoImageView;

    public ImageView getUserPhotoImageView() {
        return this.userPhotoImageView;
    }

    public void setUserPhotoImageView(ImageView userPhotoImageView2) {
        this.userPhotoImageView = userPhotoImageView2;
    }

    public ImageView getTopIconIndicator() {
        return this.topIconIndicator;
    }

    public void setTopIconIndicator(ImageView topIconIndicator2) {
        this.topIconIndicator = topIconIndicator2;
    }

    public TextView getUserNameTextView() {
        return this.userNameTextView;
    }

    public void setUserNameTextView(TextView userNameTextView2) {
        this.userNameTextView = userNameTextView2;
    }

    public TextView getPubTimeTextView() {
        return this.pubTimeTextView;
    }

    public void setPubTimeTextView(TextView pubTimeTextView2) {
        this.pubTimeTextView = pubTimeTextView2;
    }

    public TextView getMainContentTextView() {
        return this.mainContentTextView;
    }

    public void setMainContentTextView(TextView mainContentTextView2) {
        this.mainContentTextView = mainContentTextView2;
    }

    public TextView getReferTitleTextView() {
        return this.referTitleTextView;
    }

    public void setReferTitleTextView(TextView referTitleTextView2) {
        this.referTitleTextView = referTitleTextView2;
    }

    public TextView getReferContentTextView() {
        return this.referContentTextView;
    }

    public void setReferContentTextView(TextView referContentTextView2) {
        this.referContentTextView = referContentTextView2;
    }

    public TextView getReplyNumTextView() {
        return this.replyNumTextView;
    }

    public void setReplyNumTextView(TextView replyNumTextView2) {
        this.replyNumTextView = replyNumTextView2;
    }

    public TextView getSourceName() {
        return this.sourceName;
    }

    public void setSourceName(TextView sourceName2) {
        this.sourceName = sourceName2;
    }

    public LinearLayout getMicroblogBody() {
        return this.microblogBody;
    }

    public void setMicroblogBody(LinearLayout microblogBody2) {
        this.microblogBody = microblogBody2;
    }

    public ImageView getReplyBtn() {
        return this.replyBtn;
    }

    public void setReplyBtn(ImageView replyBtn2) {
        this.replyBtn = replyBtn2;
    }

    public ImageView getShareContentBtn() {
        return this.shareContentBtn;
    }

    public void setShareContentBtn(ImageView shareContentBtn2) {
        this.shareContentBtn = shareContentBtn2;
    }

    public RelativeLayout getShareContentContainer() {
        return this.shareContentContainer;
    }

    public void setShareContentContainer(RelativeLayout shareContentContainer2) {
        this.shareContentContainer = shareContentContainer2;
    }

    public ImageView getReplyThreadBtn() {
        return this.replyThreadBtn;
    }

    public void setReplyThreadBtn(ImageView replyThreadBtn2) {
        this.replyThreadBtn = replyThreadBtn2;
    }

    public TextView getReplyTextView() {
        return this.replyTextView;
    }

    public void setReplyTextView(TextView replyTextView2) {
        this.replyTextView = replyTextView2;
    }

    public TextView getReplyThreadTextView() {
        return this.replyThreadTextView;
    }

    public void setReplyThreadTextView(TextView replyThreadTextView2) {
        this.replyThreadTextView = replyThreadTextView2;
    }

    public RelativeLayout getActionBtnBox() {
        return this.actionBtnBox;
    }

    public void setActionBtnBox(RelativeLayout actionBtnBox2) {
        this.actionBtnBox = actionBtnBox2;
    }

    public RelativeLayout getReplyContainer() {
        return this.replyContainer;
    }

    public void setReplyContainer(RelativeLayout replyContainer2) {
        this.replyContainer = replyContainer2;
    }

    public RelativeLayout getReplyThreadContainer() {
        return this.replyThreadContainer;
    }

    public void setReplyThreadContainer(RelativeLayout replyThreadContainer2) {
        this.replyThreadContainer = replyThreadContainer2;
    }

    public RelativeLayout getReplyBox() {
        return this.replyBox;
    }

    public void setReplyBox(RelativeLayout replyBox2) {
        this.replyBox = replyBox2;
    }

    public ImageView getMainPhotoImage() {
        return this.mainPhotoImage;
    }

    public void setMainPhotoImage(ImageView mainPhotoImage2) {
        this.mainPhotoImage = mainPhotoImage2;
    }

    public ImageView getReplyPhotoImage() {
        return this.replyPhotoImage;
    }

    public void setReplyPhotoImage(ImageView replyPhotoImage2) {
        this.replyPhotoImage = replyPhotoImage2;
    }

    public ImageView getMainPhotoIndicator() {
        return this.mainPhotoIndicator;
    }

    public void setMainPhotoIndicator(ImageView mainPhotoIndicator2) {
        this.mainPhotoIndicator = mainPhotoIndicator2;
    }

    public ProgressBar getMainPhotoPrgBar() {
        return this.mainPhotoPrgBar;
    }

    public void setMainPhotoPrgBar(ProgressBar mainPhotoPrgBar2) {
        this.mainPhotoPrgBar = mainPhotoPrgBar2;
    }

    public ProgressBar getReplyPhotoPrgBar() {
        return this.replyPhotoPrgBar;
    }

    public void setReplyPhotoPrgBar(ProgressBar replyPhotoPrgBar2) {
        this.replyPhotoPrgBar = replyPhotoPrgBar2;
    }

    public ImageView getBulletinIconIndicator() {
        return this.bulletinIconIndicator;
    }

    public void setBulletinIconIndicator(ImageView bulletinIconIndicator2) {
        this.bulletinIconIndicator = bulletinIconIndicator2;
    }

    public TextView getFloorTextView() {
        return this.floorTextView;
    }

    public void setFloorTextView(TextView floorTextView2) {
        this.floorTextView = floorTextView2;
    }

    public ImageView getReviewSourceArticleBtn() {
        return this.reviewSourceArticleBtn;
    }

    public void setReviewSourceArticleBtn(ImageView reviewSourceArticleBtn2) {
        this.reviewSourceArticleBtn = reviewSourceArticleBtn2;
    }

    public RelativeLayout getReviewSourceArticleContainer() {
        return this.reviewSourceArticleContainer;
    }

    public void setReviewSourceArticleContainer(RelativeLayout reviewSourceArticleContainer2) {
        this.reviewSourceArticleContainer = reviewSourceArticleContainer2;
    }
}
