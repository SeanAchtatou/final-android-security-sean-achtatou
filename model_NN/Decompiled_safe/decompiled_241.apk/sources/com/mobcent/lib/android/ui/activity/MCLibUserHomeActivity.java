package com.mobcent.lib.android.ui.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import com.mobcent.android.constants.MCLibConstants;
import com.mobcent.android.constants.MCLibParameterKeyConstant;
import com.mobcent.android.model.MCLibUserInfo;
import com.mobcent.android.model.MCLibUserStatus;
import com.mobcent.android.seed.autovGhnzo98NIzLsRPLHS.R;
import com.mobcent.android.service.MCLibStatusService;
import com.mobcent.android.service.MCLibUserInfoService;
import com.mobcent.android.service.impl.MCLibStatusServiceImpl;
import com.mobcent.android.service.impl.MCLibUserInfoServiceImpl;
import com.mobcent.lib.android.ui.activity.adapter.MCLibStatusListAdapter;
import com.mobcent.lib.android.ui.activity.adapter.MCLibUserInfoAdapter;
import com.mobcent.lib.android.ui.activity.listener.MCLibListViewItemOnClickListener;
import com.mobcent.lib.android.ui.delegate.MCLibUpdateListDelegate;
import com.mobcent.lib.android.utils.MCLibImageUtil;
import com.mobcent.lib.android.utils.i18n.MCLibStringBundleUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class MCLibUserHomeActivity extends MCLibUIBaseActivity implements MCLibUpdateListDelegate {
    public static final int HIDE_LIST_VIEW = 10;
    public static final int HIDE_PRG_BAR = 3;
    public static final int HIDE_USER_INFO = 7;
    public static final int REMOVE_ALL_LIST_ENTRIES = 5;
    public static final int SHOW_LIST_VIEW = 9;
    public static final int SHOW_PRG_BAR = 4;
    public static final int SHOW_USER_INFO = 6;
    public static final int UPDATE_EMPTY_ADAPTER = 1;
    public static final int UPDATE_EXIST_ADAPTER = 2;
    public static final int UPDATE_USER_INFO_PANEL = 8;
    public static final int USER_EVENTS = 5;
    public static final int USER_FANS = 3;
    public static final int USER_FRIENDS = 2;
    public static final int USER_INFO = 1;
    public static final int USER_STATUS = 4;
    public Button backBtn;
    public Button blockUnblockBtn;
    /* access modifiers changed from: private */
    public TextView blockUnblockText;
    protected AbsListView.OnScrollListener bundledListOnScrollListener = new AbsListView.OnScrollListener() {
        private Vector<String> currentUrls;
        private int firstVisibleItem;
        private int totalItemCount;
        private int visibleItemCount;

        public void onScrollStateChanged(AbsListView view, int scrollState) {
            if (scrollState == 0) {
                List<String> imageUrls = new ArrayList<>();
                parseCurrentUrls();
                List<String> preUrls = getPreviousUrls();
                List<String> nextUrls = getNextImageUrls();
                if (preUrls != null) {
                    imageUrls.addAll(preUrls);
                }
                if (nextUrls != null) {
                    imageUrls.addAll(nextUrls);
                }
                if (MCLibUserHomeActivity.this.linkType == 3 || MCLibUserHomeActivity.this.linkType == 2) {
                    MCLibUserHomeActivity.this.userInfoAdapter.asyncImageLoader.recycleBitmap(imageUrls);
                } else if (MCLibUserHomeActivity.this.linkType == 5 || MCLibUserHomeActivity.this.linkType == 4) {
                    MCLibUserHomeActivity.this.statusAdapter.asyncImageLoader.recycleBitmap(imageUrls);
                }
            }
        }

        public void onScroll(AbsListView view, int firstVisibleItem2, int visibleItemCount2, int totalItemCount2) {
            this.firstVisibleItem = firstVisibleItem2;
            this.visibleItemCount = visibleItemCount2;
            this.totalItemCount = totalItemCount2;
        }

        private void parseCurrentUrls() {
            String imgUrlReply;
            this.currentUrls = new Vector<>();
            int firstIndex = 0;
            int endIndex = this.firstVisibleItem + this.visibleItemCount + 10;
            if (this.firstVisibleItem - 10 > 0) {
                firstIndex = this.firstVisibleItem - 10;
            }
            if (endIndex >= this.totalItemCount) {
                endIndex = this.totalItemCount;
            }
            if (MCLibUserHomeActivity.this.linkType == 3 || MCLibUserHomeActivity.this.linkType == 2) {
                for (int i = firstIndex; i < endIndex; i++) {
                    if (MCLibUserHomeActivity.this.userInfoList.get(i).getImage() != null && !MCLibUserHomeActivity.this.userInfoList.get(i).getImage().equals("")) {
                        this.currentUrls.add(MCLibUserHomeActivity.this.userInfoList.get(i).getImage());
                    }
                }
            } else if (MCLibUserHomeActivity.this.linkType == 5 || MCLibUserHomeActivity.this.linkType == 4) {
                for (int i2 = firstIndex; i2 < endIndex; i2++) {
                    if (((MCLibUserStatus) MCLibUserHomeActivity.this.userStatusList.get(i2)).getUserImageUrl() != null && !((MCLibUserStatus) MCLibUserHomeActivity.this.userStatusList.get(i2)).getUserImageUrl().equals("")) {
                        this.currentUrls.add(((MCLibUserStatus) MCLibUserHomeActivity.this.userStatusList.get(i2)).getUserImageUrl());
                    }
                    if (MCLibUserHomeActivity.this.linkType == 4) {
                        String imgUrlMain = ((MCLibUserStatus) MCLibUserHomeActivity.this.userStatusList.get(i2)).getImgUrl() + ((MCLibUserStatus) MCLibUserHomeActivity.this.userStatusList.get(i2)).getPhotoPath();
                        if (imgUrlMain != null && !imgUrlMain.equals("")) {
                            this.currentUrls.add(imgUrlMain);
                        }
                        if (!(((MCLibUserStatus) MCLibUserHomeActivity.this.userStatusList.get(i2)).getReplyStatus() == null || (imgUrlReply = ((MCLibUserStatus) MCLibUserHomeActivity.this.userStatusList.get(i2)).getReplyStatus().getImgUrl() + ((MCLibUserStatus) MCLibUserHomeActivity.this.userStatusList.get(i2)).getReplyStatus().getPhotoPath()) == null || imgUrlReply.equals(""))) {
                            this.currentUrls.add(imgUrlReply);
                        }
                    }
                }
            }
        }

        private List<String> getPreviousUrls() {
            String imgUrlReply;
            if (this.firstVisibleItem - 10 <= 0) {
                return null;
            }
            List<String> urls = new ArrayList<>();
            if (MCLibUserHomeActivity.this.linkType == 3 || MCLibUserHomeActivity.this.linkType == 2) {
                for (int i = 0; i < this.firstVisibleItem - 10; i++) {
                    if (MCLibUserHomeActivity.this.userInfoList.get(i).getImage() != null && !MCLibUserHomeActivity.this.userInfoList.get(i).getImage().equals("") && !this.currentUrls.contains(MCLibUserHomeActivity.this.userInfoList.get(i).getImage())) {
                        urls.add(MCLibUserHomeActivity.this.userInfoList.get(i).getImage());
                    }
                }
            } else if (MCLibUserHomeActivity.this.linkType == 5 || MCLibUserHomeActivity.this.linkType == 4) {
                for (int i2 = 0; i2 < this.firstVisibleItem - 10; i2++) {
                    if (((MCLibUserStatus) MCLibUserHomeActivity.this.userStatusList.get(i2)).getUserImageUrl() != null && !((MCLibUserStatus) MCLibUserHomeActivity.this.userStatusList.get(i2)).getUserImageUrl().equals("") && !this.currentUrls.contains(((MCLibUserStatus) MCLibUserHomeActivity.this.userStatusList.get(i2)).getUserImageUrl())) {
                        urls.add(((MCLibUserStatus) MCLibUserHomeActivity.this.userStatusList.get(i2)).getUserImageUrl());
                    }
                    if (MCLibUserHomeActivity.this.linkType == 4) {
                        String imgUrlMain = ((MCLibUserStatus) MCLibUserHomeActivity.this.userStatusList.get(i2)).getImgUrl() + ((MCLibUserStatus) MCLibUserHomeActivity.this.userStatusList.get(i2)).getPhotoPath();
                        if (imgUrlMain != null && !imgUrlMain.equals("") && !this.currentUrls.contains(imgUrlMain)) {
                            urls.add(imgUrlMain);
                        }
                        if (((MCLibUserStatus) MCLibUserHomeActivity.this.userStatusList.get(i2)).getReplyStatus() != null && (imgUrlReply = ((MCLibUserStatus) MCLibUserHomeActivity.this.userStatusList.get(i2)).getReplyStatus().getImgUrl() + ((MCLibUserStatus) MCLibUserHomeActivity.this.userStatusList.get(i2)).getReplyStatus().getPhotoPath()) != null && !imgUrlReply.equals("") && !this.currentUrls.contains(imgUrlReply)) {
                            urls.add(imgUrlReply);
                        }
                    }
                }
            }
            return urls;
        }

        private List<String> getNextImageUrls() {
            String imgUrlReply;
            int currentVisibleBottom = this.firstVisibleItem + this.visibleItemCount;
            if (this.totalItemCount - currentVisibleBottom <= 10) {
                return null;
            }
            List<String> urls = new ArrayList<>();
            if (MCLibUserHomeActivity.this.linkType == 3 || MCLibUserHomeActivity.this.linkType == 2) {
                for (int i = currentVisibleBottom + 10; i < this.totalItemCount; i++) {
                    if (MCLibUserHomeActivity.this.userInfoList.get(i).getImage() != null && !MCLibUserHomeActivity.this.userInfoList.get(i).getImage().equals("") && !this.currentUrls.contains(MCLibUserHomeActivity.this.userInfoList.get(i).getImage())) {
                        urls.add(MCLibUserHomeActivity.this.userInfoList.get(i).getImage());
                    }
                }
            } else if (MCLibUserHomeActivity.this.linkType == 5 || MCLibUserHomeActivity.this.linkType == 4) {
                for (int i2 = currentVisibleBottom + 10; i2 < this.totalItemCount; i2++) {
                    if (((MCLibUserStatus) MCLibUserHomeActivity.this.userStatusList.get(i2)).getUserImageUrl() != null && !((MCLibUserStatus) MCLibUserHomeActivity.this.userStatusList.get(i2)).getUserImageUrl().equals("") && !this.currentUrls.contains(((MCLibUserStatus) MCLibUserHomeActivity.this.userStatusList.get(i2)).getUserImageUrl())) {
                        urls.add(((MCLibUserStatus) MCLibUserHomeActivity.this.userStatusList.get(i2)).getUserImageUrl());
                    }
                    if (MCLibUserHomeActivity.this.linkType == 4) {
                        String imgUrlMain = ((MCLibUserStatus) MCLibUserHomeActivity.this.userStatusList.get(i2)).getImgUrl() + ((MCLibUserStatus) MCLibUserHomeActivity.this.userStatusList.get(i2)).getPhotoPath();
                        if (imgUrlMain != null && !imgUrlMain.equals("") && !this.currentUrls.contains(imgUrlMain)) {
                            urls.add(imgUrlMain);
                        }
                        if (((MCLibUserStatus) MCLibUserHomeActivity.this.userStatusList.get(i2)).getReplyStatus() != null && (imgUrlReply = ((MCLibUserStatus) MCLibUserHomeActivity.this.userStatusList.get(i2)).getReplyStatus().getImgUrl() + ((MCLibUserStatus) MCLibUserHomeActivity.this.userStatusList.get(i2)).getReplyStatus().getPhotoPath()) != null && !imgUrlReply.equals("") && !this.currentUrls.contains(imgUrlReply)) {
                            urls.add(imgUrlReply);
                        }
                    }
                }
            }
            return urls;
        }
    };
    private ListView bundledListView;
    public Button chatBtn;
    private TextView cityText;
    private RelativeLayout eventContainer;
    private View.OnClickListener eventContainerOnClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            int unused = MCLibUserHomeActivity.this.linkType = 5;
            MCLibUserHomeActivity.this.mHandler.sendEmptyMessage(7);
            MCLibUserHomeActivity.this.updateListDelegate.updateList(1, 10, false);
        }
    };
    private TextView eventNum;
    private RelativeLayout fansContainer;
    private View.OnClickListener fansContainerOnClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            int unused = MCLibUserHomeActivity.this.linkType = 3;
            MCLibUserHomeActivity.this.mHandler.sendEmptyMessage(7);
            MCLibUserHomeActivity.this.updateListDelegate.updateList(1, 10, false);
        }
    };
    private TextView fansNum;
    private RelativeLayout followContainer;
    private View.OnClickListener followContainerOnClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            int unused = MCLibUserHomeActivity.this.linkType = 2;
            MCLibUserHomeActivity.this.mHandler.sendEmptyMessage(7);
            MCLibUserHomeActivity.this.updateListDelegate.updateList(1, 10, false);
        }
    };
    public Button followDisfollowButton;
    /* access modifiers changed from: private */
    public TextView followDisfollowText;
    private TextView followNum;
    private TextView genderText;
    /* access modifiers changed from: private */
    public int linkType = 1;
    /* access modifiers changed from: private */
    public int loginUid;
    /* access modifiers changed from: private */
    public Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            if (msg.what == 1 || msg.what == 5) {
                MCLibUserHomeActivity.this.updateEmptyAdapter(msg.obj);
            } else if (msg.what == 2) {
                MCLibUserHomeActivity.this.updateExistAdapter(msg.obj);
            } else if (msg.what == 3) {
                MCLibUserHomeActivity.this.hideProgressBar();
            } else if (msg.what == 4) {
                MCLibUserHomeActivity.this.showProgressBar();
            } else if (msg.what == 7) {
                MCLibUserHomeActivity.this.hideUserInfo();
            } else if (msg.what == 6) {
                MCLibUserHomeActivity.this.showUserInfo();
            } else if (msg.what == 8) {
                MCLibUserHomeActivity.this.updateUserInfoPanel();
            } else if (msg.what == 9) {
                MCLibUserHomeActivity.this.shwoListView();
            } else if (msg.what == 10) {
                MCLibUserHomeActivity.this.hideListView();
            }
        }
    };
    private TextView moodText;
    protected int position = 0;
    /* access modifiers changed from: private */
    public MCLibStatusListAdapter statusAdapter;
    private RelativeLayout statusContainer;
    private View.OnClickListener statusContainerOnClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            int unused = MCLibUserHomeActivity.this.linkType = 4;
            MCLibUserHomeActivity.this.mHandler.sendEmptyMessage(7);
            MCLibUserHomeActivity.this.updateListDelegate.updateList(1, 10, false);
        }
    };
    private TextView statusNum;
    /* access modifiers changed from: private */
    public MCLibUpdateListDelegate updateListDelegate = new MCLibUpdateListDelegate() {
        public void updateList(final int page, final int pageSize, boolean isReadLocally) {
            MCLibUserHomeActivity.this.mHandler.sendMessage(MCLibUserHomeActivity.this.mHandler.obtainMessage(5, new ArrayList()));
            MCLibUserHomeActivity.this.mHandler.sendEmptyMessage(4);
            new Thread() {
                public void run() {
                    try {
                        if (MCLibUserHomeActivity.this.linkType == 3 || MCLibUserHomeActivity.this.linkType == 2) {
                            List<MCLibUserInfo> list = MCLibUserHomeActivity.this.getUserInfoList(page, pageSize);
                            MCLibUserHomeActivity.this.mHandler.sendEmptyMessage(3);
                            MCLibUserHomeActivity.this.updateBundledListView(list);
                            if (list == null || list.isEmpty()) {
                                MCLibUserHomeActivity.this.showEmptyInfoTip(R.string.mc_lib_no_such_data);
                            }
                        } else if (MCLibUserHomeActivity.this.linkType == 5 || MCLibUserHomeActivity.this.linkType == 4) {
                            List<MCLibUserStatus> list2 = MCLibUserHomeActivity.this.getUserStatusList(page, pageSize);
                            MCLibUserHomeActivity.this.mHandler.sendEmptyMessage(3);
                            MCLibUserHomeActivity.this.updateBundledListView(list2);
                            if (list2 == null || list2.isEmpty()) {
                                MCLibUserHomeActivity.this.showEmptyInfoTip(R.string.mc_lib_no_such_data);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }.start();
        }
    };
    private TextView userAcountText;
    private ScrollView userHomeScrollView;
    /* access modifiers changed from: private */
    public int userId;
    public MCLibUserInfo userInfo = null;
    /* access modifiers changed from: private */
    public MCLibUserInfoAdapter userInfoAdapter;
    private RelativeLayout userInfoBar;
    private View.OnClickListener userInfoBarOnClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            MCLibUserHomeActivity.this.getUserInfo();
        }
    };
    protected List<MCLibUserInfo> userInfoList;
    private TextView userNameText;
    private TextView userOnlineText;
    private ImageView userPhotoImage;
    /* access modifiers changed from: private */
    public List<MCLibUserStatus> userStatusList;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.mc_lib_user_home);
        initWindowTitle();
        initSysMsgWidgets();
        initProgressBox();
        initNavBottomBar(102);
        initUserHomeWidgets();
        this.loginUid = new MCLibUserInfoServiceImpl(this).getLoginUserId();
        if (getIntent().getExtras() == null) {
            this.linkType = 1;
            this.userId = this.loginUid;
        } else if (getIntent().getExtras().get("userId") != null) {
            this.userId = getIntent().getExtras().getInt("userId");
            if (this.userId <= 0) {
                return;
            }
        }
        this.bundledListView.setOnItemClickListener(new MCLibListViewItemOnClickListener());
        this.userInfoBar.performClick();
    }

    public void initUserHomeWidgets() {
        this.bundledListView = (ListView) findViewById(R.id.mcLibUserBundledListView);
        this.userHomeScrollView = (ScrollView) findViewById(R.id.mcLibUserHomeDetailScrollView);
        this.bundledListView.setOnScrollListener(this.bundledListOnScrollListener);
        this.backBtn = (Button) findViewById(R.id.mcLibBackBtn);
        this.chatBtn = (Button) findViewById(R.id.mcLibChatBtn);
        this.followDisfollowButton = (Button) findViewById(R.id.mcLibFollowDisfollowBtn);
        this.blockUnblockBtn = (Button) findViewById(R.id.mcLibBlockUnblockBtn);
        this.followDisfollowText = (TextView) findViewById(R.id.mcLibFollowDisfollowText);
        this.blockUnblockText = (TextView) findViewById(R.id.mcLibBlockUnblockText);
        this.userInfoBar = (RelativeLayout) findViewById(R.id.mcLibUserInfoBar);
        this.userPhotoImage = (ImageView) findViewById(R.id.mcLibUserPhoto);
        this.userNameText = (TextView) findViewById(R.id.mcLibUserNameText);
        this.userAcountText = (TextView) findViewById(R.id.mcLibUserAcountText);
        this.userOnlineText = (TextView) findViewById(R.id.mcLibUserOnlineText);
        this.genderText = (TextView) findViewById(R.id.mcLibGenderText);
        this.cityText = (TextView) findViewById(R.id.mcLibCityText);
        this.moodText = (TextView) findViewById(R.id.mcLibMoodText);
        this.followNum = (TextView) findViewById(R.id.mcLibFollowNum);
        this.fansNum = (TextView) findViewById(R.id.mcLibFansNum);
        this.statusNum = (TextView) findViewById(R.id.mcLibStatusNum);
        this.eventNum = (TextView) findViewById(R.id.mcLibEventsNum);
        this.followContainer = (RelativeLayout) findViewById(R.id.mcLibFollowContainer);
        this.fansContainer = (RelativeLayout) findViewById(R.id.mcLibFansContainer);
        this.statusContainer = (RelativeLayout) findViewById(R.id.mcLibStatusContainer);
        this.eventContainer = (RelativeLayout) findViewById(R.id.mcLibEventsContainer);
        this.followDisfollowButton.setEnabled(false);
        this.blockUnblockBtn.setEnabled(false);
        initUserInfoBar();
    }

    private void initUserInfoBar() {
        this.userInfoBar.setOnClickListener(this.userInfoBarOnClickListener);
        this.userPhotoImage.setOnClickListener(this.userInfoBarOnClickListener);
        this.userNameText.setOnClickListener(this.userInfoBarOnClickListener);
        this.userAcountText.setOnClickListener(this.userInfoBarOnClickListener);
    }

    /* access modifiers changed from: private */
    public void getUserInfo() {
        this.mHandler.sendEmptyMessage(4);
        this.mHandler.sendMessage(this.mHandler.obtainMessage(5, new ArrayList()));
        new Thread() {
            public void run() {
                try {
                    if (MCLibUserHomeActivity.this.userInfo == null) {
                        MCLibUserInfoService uis = new MCLibUserInfoServiceImpl(MCLibUserHomeActivity.this);
                        MCLibUserHomeActivity.this.userInfo = uis.getUserHomeInfo(MCLibUserHomeActivity.this.loginUid, MCLibUserHomeActivity.this.userId);
                    }
                    MCLibUserHomeActivity.this.mHandler.sendEmptyMessage(3);
                    if (MCLibUserHomeActivity.this.userInfo != null) {
                        int unused = MCLibUserHomeActivity.this.linkType = 1;
                        MCLibUserHomeActivity.this.mHandler.sendEmptyMessage(10);
                        MCLibUserHomeActivity.this.mHandler.sendEmptyMessage(6);
                        MCLibUserHomeActivity.this.mHandler.sendEmptyMessage(8);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    /* access modifiers changed from: private */
    public void showUserInfo() {
        this.userHomeScrollView.setVisibility(0);
    }

    /* access modifiers changed from: private */
    public void hideUserInfo() {
        this.userHomeScrollView.setVisibility(8);
    }

    /* access modifiers changed from: private */
    public void hideListView() {
        this.bundledListView.setVisibility(4);
    }

    /* access modifiers changed from: private */
    public void shwoListView() {
        this.bundledListView.setVisibility(0);
    }

    /* access modifiers changed from: private */
    public void updateUserInfoPanel() {
        String genderStr;
        if (this.userInfo.getGender().equals("0")) {
            genderStr = getResources().getString(R.string.mc_lib_female);
        } else if (this.userInfo.getGender().equals(MCLibConstants.ANDROID)) {
            genderStr = getResources().getString(R.string.mc_lib_male);
        } else {
            genderStr = getResources().getString(R.string.mc_lib_secret);
        }
        this.genderText.setText(getResources().getString(R.string.mc_lib_user_gender_user_home) + genderStr + "        " + getResources().getString(R.string.mc_lib_age) + this.userInfo.getAge());
        this.cityText.setText(getResources().getString(R.string.mc_lib_city_user_home) + this.userInfo.getPos());
        String mood = this.userInfo.getEmotionWords();
        if (this.userInfo.getEmotionWords() == null || "".equals(this.userInfo.getEmotionWords())) {
            mood = getResources().getString(R.string.mc_lib_mood_empty);
        }
        this.moodText.setText(MCLibStringBundleUtil.resolveString(R.string.mc_lib_mood, new String[]{mood}, this));
        this.followNum.setText(this.userInfo.getFocusNum() + "");
        this.fansNum.setText(this.userInfo.getFansNum() + "");
        this.statusNum.setText(this.userInfo.getStatusNum() + "");
        this.eventNum.setText(this.userInfo.getEventNum() + "");
        this.userNameText.setText(this.userInfo.getNickName());
        int isOnline = R.string.mc_lib_user_offline;
        if (this.userInfo.isOnline()) {
            isOnline = R.string.mc_lib_user_online;
        }
        this.userOnlineText.setText(isOnline);
        this.userAcountText.setText(getResources().getString(R.string.mc_lib_user_account) + ": " + this.userInfo.getName());
        MCLibImageUtil.updateUserPhotoImage(this.userPhotoImage, this.userInfo.getImage(), this, R.drawable.mc_lib_face_u0, this.mHandler);
        initBackButton();
        initFollowDisfollowButton();
        initChatButton();
        initBlockUnblockButton();
        initUserFollowedContainer();
        initUserFansContainer();
        initUserStatusContainer();
        initUserEventsContainer();
    }

    private void initBackButton() {
        this.backBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCLibUserHomeActivity.this.performBackAction();
            }
        });
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        performBackAction();
        return false;
    }

    /* access modifiers changed from: private */
    public void performBackAction() {
        if (this.linkType == 1) {
            finish();
        } else {
            this.userInfoBar.performClick();
        }
    }

    private void initChatButton() {
        this.chatBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(MCLibUserHomeActivity.this, MCLibChatRoomActivity.class);
                intent.putExtra("userId", MCLibUserHomeActivity.this.userId);
                intent.putExtra("nickName", MCLibUserHomeActivity.this.userInfo.getNickName());
                intent.putExtra(MCLibParameterKeyConstant.USER_PHOTO, MCLibUserHomeActivity.this.userInfo.getImage());
                MCLibUserHomeActivity.this.startActivity(intent);
            }
        });
    }

    private void initUserFansContainer() {
        this.fansContainer.setOnClickListener(this.fansContainerOnClickListener);
    }

    private void initUserEventsContainer() {
        this.eventContainer.setOnClickListener(this.eventContainerOnClickListener);
    }

    private void initUserStatusContainer() {
        this.statusContainer.setOnClickListener(this.statusContainerOnClickListener);
    }

    private void initUserFollowedContainer() {
        this.followContainer.setOnClickListener(this.followContainerOnClickListener);
    }

    private void initFollowDisfollowButton() {
        if (this.userInfo.getUid() != new MCLibUserInfoServiceImpl(this).getLoginUserId()) {
            this.followDisfollowButton.setEnabled(true);
        }
        if (this.userInfo.isFollowed()) {
            this.followDisfollowText.setText((int) R.string.mc_lib_cancel_follow);
            this.followDisfollowButton.setBackgroundResource(R.drawable.mc_lib_button_disfollow);
        } else {
            this.followDisfollowText.setText((int) R.string.mc_lib_follow);
            this.followDisfollowButton.setBackgroundResource(R.drawable.mc_lib_button_follow);
        }
        this.followDisfollowButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(final View v) {
                v.setEnabled(false);
                new Thread() {
                    public void run() {
                        String resultTemp;
                        MCLibUserHomeActivity.this.mHandler.post(new Runnable() {
                            public void run() {
                                Toast.makeText(MCLibUserHomeActivity.this, (int) R.string.mc_lib_send_request, 0).show();
                            }
                        });
                        MCLibUserInfoService userInfoService = new MCLibUserInfoServiceImpl(MCLibUserHomeActivity.this);
                        if (MCLibUserHomeActivity.this.userInfo.isFollowed()) {
                            resultTemp = userInfoService.cancelFollowUser(userInfoService.getLoginUserId(), MCLibUserHomeActivity.this.userInfo.getUid());
                        } else {
                            resultTemp = userInfoService.followUser(userInfoService.getLoginUserId(), MCLibUserHomeActivity.this.userInfo.getUid());
                        }
                        final String result = resultTemp;
                        MCLibUserHomeActivity.this.mHandler.post(new Runnable() {
                            public void run() {
                                int tipWords;
                                if (result == null || !result.equals("rs_succ")) {
                                    Toast.makeText(MCLibUserHomeActivity.this, (int) R.string.mc_lib_connection_fail, 0).show();
                                } else {
                                    if (MCLibUserHomeActivity.this.userInfo.isFollowed()) {
                                        MCLibUserHomeActivity.this.userInfo.setFollowed(false);
                                        tipWords = R.string.mc_lib_disfollow_succ;
                                        MCLibUserHomeActivity.this.followDisfollowText.setText((int) R.string.mc_lib_follow);
                                        MCLibUserHomeActivity.this.followDisfollowButton.setBackgroundResource(R.drawable.mc_lib_button_follow);
                                    } else {
                                        MCLibUserHomeActivity.this.userInfo.setFollowed(true);
                                        tipWords = R.string.mc_lib_follow_succ;
                                        MCLibUserHomeActivity.this.followDisfollowText.setText((int) R.string.mc_lib_cancel_follow);
                                        MCLibUserHomeActivity.this.followDisfollowButton.setBackgroundResource(R.drawable.mc_lib_button_disfollow);
                                    }
                                    Toast.makeText(MCLibUserHomeActivity.this, tipWords, 0).show();
                                }
                                v.setEnabled(true);
                            }
                        });
                    }
                }.start();
            }
        });
    }

    private void initBlockUnblockButton() {
        if (this.userInfo.getUid() != new MCLibUserInfoServiceImpl(this).getLoginUserId()) {
            this.blockUnblockBtn.setEnabled(true);
        }
        if (this.userInfo.isBlocked()) {
            this.blockUnblockText.setText((int) R.string.mc_lib_unblock);
            this.blockUnblockBtn.setBackgroundResource(R.drawable.mc_lib_button_unblock);
        } else {
            this.blockUnblockText.setText((int) R.string.mc_lib_block);
            this.blockUnblockBtn.setBackgroundResource(R.drawable.mc_lib_button_block);
        }
        this.blockUnblockBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCLibUserHomeActivity.this.blockUnblockBtn.setEnabled(false);
                if (MCLibUserHomeActivity.this.userInfo.isBlocked()) {
                    MCLibUserHomeActivity.this.removeFromBlackList();
                } else {
                    MCLibUserHomeActivity.this.showAddToBlackListDialog();
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void showAddToBlackListDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle((int) R.string.mc_lib_tips);
        alertDialog.setMessage(MCLibStringBundleUtil.resolveString(R.string.mc_lib_block_alert_msg, new String[]{this.userInfo.getNickName()}, this));
        alertDialog.setPositiveButton((int) R.string.mc_lib_confirm, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                new Thread() {
                    public void run() {
                        MCLibUserHomeActivity.this.mHandler.post(new Runnable() {
                            public void run() {
                                Toast.makeText(MCLibUserHomeActivity.this, (int) R.string.mc_lib_send_request, 0).show();
                            }
                        });
                        MCLibUserInfoService userInfoService = new MCLibUserInfoServiceImpl(MCLibUserHomeActivity.this);
                        final String result = userInfoService.addToBlackList(userInfoService.getLoginUserId(), MCLibUserHomeActivity.this.userInfo.getUid());
                        MCLibUserHomeActivity.this.mHandler.post(new Runnable() {
                            public void run() {
                                if (result == null || !result.equals("rs_succ")) {
                                    Toast.makeText(MCLibUserHomeActivity.this, (int) R.string.mc_lib_connection_fail, 0).show();
                                } else {
                                    MCLibUserHomeActivity.this.userInfo.setBlocked(true);
                                    MCLibUserHomeActivity.this.blockUnblockText.setText((int) R.string.mc_lib_unblock);
                                    MCLibUserHomeActivity.this.blockUnblockBtn.setBackgroundResource(R.drawable.mc_lib_button_unblock);
                                    Toast.makeText(MCLibUserHomeActivity.this, (int) R.string.mc_lib_block_succ, 0).show();
                                }
                                MCLibUserHomeActivity.this.blockUnblockBtn.setEnabled(true);
                            }
                        });
                    }
                }.start();
                dialog.dismiss();
            }
        });
        alertDialog.setNegativeButton((int) R.string.mc_lib_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                MCLibUserHomeActivity.this.blockUnblockBtn.setEnabled(true);
                dialog.dismiss();
            }
        });
        alertDialog.show();
    }

    /* access modifiers changed from: private */
    public void removeFromBlackList() {
        new Thread() {
            public void run() {
                MCLibUserHomeActivity.this.mHandler.post(new Runnable() {
                    public void run() {
                        Toast.makeText(MCLibUserHomeActivity.this, (int) R.string.mc_lib_send_request, 0).show();
                    }
                });
                MCLibUserInfoService userInfoService = new MCLibUserInfoServiceImpl(MCLibUserHomeActivity.this);
                final String result = userInfoService.removeFromBlackList(userInfoService.getLoginUserId(), MCLibUserHomeActivity.this.userInfo.getUid());
                MCLibUserHomeActivity.this.mHandler.post(new Runnable() {
                    public void run() {
                        if (result == null || !result.equals("rs_succ")) {
                            Toast.makeText(MCLibUserHomeActivity.this, (int) R.string.mc_lib_connection_fail, 0).show();
                        } else {
                            MCLibUserHomeActivity.this.userInfo.setBlocked(false);
                            MCLibUserHomeActivity.this.blockUnblockText.setText((int) R.string.mc_lib_block);
                            MCLibUserHomeActivity.this.blockUnblockBtn.setBackgroundResource(R.drawable.mc_lib_button_block);
                            Toast.makeText(MCLibUserHomeActivity.this, (int) R.string.mc_lib_unblock_succ, 0).show();
                        }
                        MCLibUserHomeActivity.this.blockUnblockBtn.setEnabled(true);
                    }
                });
            }
        }.start();
    }

    /* access modifiers changed from: private */
    public void updateEmptyAdapter(Object obj) {
        if (this.linkType == 3 || this.linkType == 2) {
            this.userInfoAdapter = new MCLibUserInfoAdapter(this, this.bundledListView, R.layout.mc_lib_user_list_row, (List) obj, this, this.mHandler);
            this.bundledListView.setAdapter((ListAdapter) this.userInfoAdapter);
            this.userInfoList = (List) obj;
            return;
        }
        this.statusAdapter = new MCLibStatusListAdapter(this, this.bundledListView, R.layout.mc_lib_status_list_row, (List) obj, this, false, false, this.mHandler);
        this.bundledListView.setAdapter((ListAdapter) this.statusAdapter);
        this.userStatusList = (List) obj;
    }

    /* access modifiers changed from: private */
    public void updateExistAdapter(Object obj) {
        if (this.linkType == 3 || this.linkType == 2) {
            this.userInfoAdapter.setUserInfoList((List) obj);
            this.userInfoAdapter.notifyDataSetInvalidated();
            this.userInfoAdapter.notifyDataSetChanged();
            this.userInfoList = (List) obj;
            return;
        }
        this.statusAdapter.setStatusList((List) obj);
        this.statusAdapter.notifyDataSetInvalidated();
        this.statusAdapter.notifyDataSetChanged();
        this.userStatusList = (List) obj;
    }

    /* access modifiers changed from: private */
    public void updateBundledListView(List<?> list) {
        this.mHandler.sendEmptyMessage(9);
        if (this.linkType == 3 || this.linkType == 2) {
            if (this.userInfoAdapter == null) {
                this.mHandler.sendMessage(this.mHandler.obtainMessage(1, list));
                return;
            }
            this.mHandler.sendMessage(this.mHandler.obtainMessage(2, list));
        } else if (this.statusAdapter == null) {
            this.mHandler.sendMessage(this.mHandler.obtainMessage(1, list));
        } else {
            this.mHandler.sendMessage(this.mHandler.obtainMessage(2, list));
        }
    }

    public void updateList(final int page, final int pageSize, boolean isReadLocally) {
        new Thread() {
            public void run() {
                if (MCLibUserHomeActivity.this.linkType == 3 || MCLibUserHomeActivity.this.linkType == 2) {
                    List<MCLibUserInfo> resultList = new ArrayList<>();
                    List<MCLibUserInfo> nextPageUserInfoList = MCLibUserHomeActivity.this.getUserInfoList(page, pageSize);
                    if (nextPageUserInfoList != null && nextPageUserInfoList.size() > 0) {
                        resultList.addAll(MCLibUserHomeActivity.this.userInfoList);
                        resultList.remove(resultList.size() - 1);
                        resultList.addAll(nextPageUserInfoList);
                    } else if (MCLibUserHomeActivity.this.userInfoList.size() > 0) {
                        resultList.addAll(MCLibUserHomeActivity.this.userInfoList);
                        resultList.remove(resultList.size() - 1);
                    }
                    MCLibUserHomeActivity.this.updateBundledListView(resultList);
                } else if (MCLibUserHomeActivity.this.linkType == 5 || MCLibUserHomeActivity.this.linkType == 4) {
                    List<MCLibUserStatus> resultList2 = new ArrayList<>();
                    List<MCLibUserStatus> nextPageUserStatusList = MCLibUserHomeActivity.this.getUserStatusList(page, pageSize);
                    if (nextPageUserStatusList != null && nextPageUserStatusList.size() > 0) {
                        resultList2.addAll(MCLibUserHomeActivity.this.userStatusList);
                        resultList2.remove(resultList2.size() - 1);
                        resultList2.addAll(nextPageUserStatusList);
                    } else if (MCLibUserHomeActivity.this.userStatusList.size() > 0) {
                        resultList2.addAll(MCLibUserHomeActivity.this.userStatusList);
                        resultList2.remove(resultList2.size() - 1);
                    }
                    MCLibUserHomeActivity.this.updateBundledListView(resultList2);
                }
            }
        }.start();
    }

    /* access modifiers changed from: private */
    public List<MCLibUserStatus> getUserStatusList(int page, int pageSize) {
        MCLibStatusService statusService = new MCLibStatusServiceImpl(this);
        if (this.linkType == 4) {
            return statusService.getMyStatus(this.loginUid, this.userInfo.getUid(), page, pageSize, false, false);
        }
        if (this.linkType == 5) {
            return statusService.getUserEvents(this.loginUid, this.userInfo.getUid(), page, pageSize);
        }
        return new ArrayList();
    }

    /* access modifiers changed from: private */
    public List<MCLibUserInfo> getUserInfoList(int page, int pageSize) {
        MCLibUserInfoService uis = new MCLibUserInfoServiceImpl(this);
        if (this.linkType == 3) {
            return uis.getUserFans(this.loginUid, this.userInfo.getUid(), page, pageSize);
        }
        if (this.linkType == 2) {
            return uis.getUserFriends(this.loginUid, this.userInfo.getUid(), page, pageSize);
        }
        return new ArrayList();
    }

    public Handler getCurrentHandler() {
        return this.mHandler;
    }
}
