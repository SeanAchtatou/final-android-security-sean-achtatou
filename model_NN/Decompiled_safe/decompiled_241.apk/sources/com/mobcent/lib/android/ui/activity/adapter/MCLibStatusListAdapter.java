package com.mobcent.lib.android.ui.activity.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.mobcent.android.constants.MCLibCommunicationType;
import com.mobcent.android.constants.MCLibConstants;
import com.mobcent.android.constants.MCLibMobCentApiConstant;
import com.mobcent.android.constants.MCLibParameterKeyConstant;
import com.mobcent.android.model.MCLibUserStatus;
import com.mobcent.android.seed.autovGhnzo98NIzLsRPLHS.R;
import com.mobcent.android.service.impl.MCLibUserInfoServiceImpl;
import com.mobcent.lib.android.ui.activity.MCLibCommunityBundleActivity;
import com.mobcent.lib.android.ui.activity.MCLibPublishStatusActivity;
import com.mobcent.lib.android.ui.activity.MCLibStatusRepliesActivity;
import com.mobcent.lib.android.ui.activity.MCLibUserHomeActivity;
import com.mobcent.lib.android.ui.activity.adapter.holder.MCLibStatusListAdapterHolder;
import com.mobcent.lib.android.ui.delegate.MCLibUpdateListDelegate;
import com.mobcent.lib.android.ui.dialog.MCLibAppInfoDialog;
import com.mobcent.lib.android.ui.dialog.MCLibWebViewDialog;
import com.mobcent.lib.android.utils.MCLibAsyncImageLoader;
import com.mobcent.lib.android.utils.MCLibBitmapCallback;
import com.mobcent.lib.android.utils.MCLibDateUtil;
import com.mobcent.lib.android.utils.MCLibPhoneUtil;
import com.mobcent.lib.android.utils.MCLibTextViewUtil;
import com.mobcent.lib.android.utils.i18n.MCLibStringBundleUtil;
import com.mobcent.share.android.activity.MCShareAppActivity;
import java.util.List;

public class MCLibStatusListAdapter extends MCLibBaseArrayAdapter implements MCLibMobCentApiConstant, MCLibCommunicationType, MCLibConstants {
    public static String colorUserName = "#0066FF";
    private final int TOPIC_IS_BULLETIN = 2;
    private final int TOPIC_IS_TOP = 1;
    public MCLibAsyncImageLoader asyncImageLoader;
    /* access modifiers changed from: private */
    public final Activity context;
    private boolean isCommentList;
    private boolean isReplyHistory;
    /* access modifiers changed from: private */
    public ListView listView;
    public Handler mHandler;
    private int rowResourceId;
    /* access modifiers changed from: private */
    public String sourceArticleUrl;
    private List<MCLibUserStatus> statusList;

    public MCLibStatusListAdapter(Activity context2, ListView listView2, int rowResourceId2, List<MCLibUserStatus> statusList2, MCLibUpdateListDelegate delegate, boolean isReplyHistory2, boolean isCommentList2, Handler mHandler2) {
        super(context2, rowResourceId2, statusList2);
        this.rowResourceId = rowResourceId2;
        this.listView = listView2;
        this.inflater = LayoutInflater.from(context2);
        this.context = context2;
        this.statusList = statusList2;
        this.delegate = delegate;
        this.isReplyHistory = isReplyHistory2;
        this.isCommentList = isCommentList2;
        this.mHandler = mHandler2;
        this.asyncImageLoader = new MCLibAsyncImageLoader(context2);
    }

    public int getCount() {
        return this.statusList.size();
    }

    public Object getItem(int position) {
        return this.statusList.get(position);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        MCLibStatusListAdapterHolder holder;
        MCLibUserStatus statusModel = this.statusList.get(position);
        if (statusModel.isFetchMore()) {
            return getFetchMoreView(statusModel.getCurrentPage() + 1, statusModel.isReadFromLocal());
        }
        if (statusModel.isRefreshNeeded()) {
            return getRefreshView(statusModel.getLastUpdateTime(), this.context);
        }
        if (convertView == null) {
            convertView = this.inflater.inflate(this.rowResourceId, (ViewGroup) null);
            holder = new MCLibStatusListAdapterHolder();
            convertView.setTag(holder);
            initHolder(holder, convertView);
        } else {
            holder = (MCLibStatusListAdapterHolder) convertView.getTag();
        }
        if (holder == null) {
            convertView = this.inflater.inflate(this.rowResourceId, (ViewGroup) null);
            holder = new MCLibStatusListAdapterHolder();
            convertView.setTag(holder);
            initHolder(holder, convertView);
        }
        holder.getUserPhotoImageView().setBackgroundResource(R.drawable.mc_lib_face_u0);
        holder.getMainPhotoImage().setBackgroundDrawable(null);
        holder.getReplyPhotoImage().setBackgroundDrawable(null);
        updateTitlePubTime(holder, statusModel);
        updateFloorNum(holder, position);
        updateUserImage(holder, statusModel);
        updateSourceInfo(holder, statusModel);
        updatePhotos(holder, statusModel);
        initReplyBox(holder, statusModel);
        initActionBoxBtns(holder, statusModel);
        initConvertViewOnClickListener(convertView, holder, statusModel);
        return convertView;
    }

    private void initConvertViewOnClickListener(View convertView, final MCLibStatusListAdapterHolder theHolder, final MCLibUserStatus statusModel) {
        convertView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (statusModel.getCommunicationType() == 18) {
                    new MCLibAppInfoDialog(MCLibStatusListAdapter.this.context, R.style.mc_lib_dialog, statusModel.getTargetJumpId(), MCLibStatusListAdapter.this.mHandler).show();
                } else if (statusModel.getCommunicationType() == 12 || statusModel.getCommunicationType() == 17 || statusModel.getCommunicationType() == 16) {
                    Intent intent = new Intent(MCLibStatusListAdapter.this.context, MCLibUserHomeActivity.class);
                    intent.putExtra("userId", statusModel.getTargetJumpId());
                    MCLibStatusListAdapter.this.context.startActivity(intent);
                } else if ((MCLibStatusListAdapter.this.context instanceof MCLibCommunityBundleActivity) && ((MCLibCommunityBundleActivity) MCLibStatusListAdapter.this.context).getStatusType().intValue() == 1) {
                    Intent intent2 = new Intent(MCLibStatusListAdapter.this.context, MCLibStatusRepliesActivity.class);
                    intent2.putExtra("id", statusModel.getRootId() == -1 ? statusModel.getStatusId() : statusModel.getRootId());
                    MCLibStatusListAdapter.this.context.startActivity(intent2);
                } else if (theHolder.getActionBtnBox() == null || theHolder.getActionBtnBox().getVisibility() != 0) {
                    MCLibStatusListAdapter.this.mHandler.post(new Runnable() {
                        public void run() {
                            theHolder.getActionBtnBox().setVisibility(0);
                            MCLibStatusListAdapter.this.showAnimActionBox(theHolder.getActionBtnBox());
                        }
                    });
                } else {
                    MCLibStatusListAdapter.this.hideAnimActionBox(theHolder.getActionBtnBox());
                }
            }
        });
    }

    private void initReplyBox(MCLibStatusListAdapterHolder holder, MCLibUserStatus statusModel) {
        if (statusModel.getReplyStatus() == null || statusModel.getReplyStatus().getContent() == null || "".equals(statusModel.getReplyStatus().getContent())) {
            holder.getReplyBox().setVisibility(8);
        } else {
            holder.getReplyBox().setVisibility(0);
        }
    }

    private void initHolder(MCLibStatusListAdapterHolder holder, View convertView) {
        holder.setMicroblogBody((LinearLayout) convertView.findViewById(R.id.mcLibMicroblogBody));
        holder.setUserPhotoImageView((ImageView) convertView.findViewById(R.id.mcLibUserPhoto));
        holder.setMainContentTextView((TextView) convertView.findViewById(R.id.mcLibMainContent));
        holder.setPubTimeTextView((TextView) convertView.findViewById(R.id.mcLibPublishTime));
        holder.setReplyNumTextView((TextView) convertView.findViewById(R.id.mcLibReplyNum));
        holder.setUserNameTextView((TextView) convertView.findViewById(R.id.mcLibUserName));
        holder.setSourceName((TextView) convertView.findViewById(R.id.mcLibSourceName));
        holder.setReplyBox((RelativeLayout) convertView.findViewById(R.id.mcLibReplyBox));
        holder.setReferTitleTextView((TextView) convertView.findViewById(R.id.mcLibReferTitle));
        holder.setReferContentTextView((TextView) convertView.findViewById(R.id.mcLibReferContent));
        holder.setMainPhotoIndicator((ImageView) convertView.findViewById(R.id.mcLibMainPhotoIndicator));
        holder.setMainPhotoImage((ImageView) convertView.findViewById(R.id.mcLibMainPhotoImage));
        holder.setReplyPhotoImage((ImageView) convertView.findViewById(R.id.mcLibReplyPhotoImage));
        holder.setTopIconIndicator((ImageView) convertView.findViewById(R.id.mcLibTopIndicator));
        holder.setBulletinIconIndicator((ImageView) convertView.findViewById(R.id.mcLibBulletinIndicator));
        holder.setActionBtnBox((RelativeLayout) convertView.findViewById(R.id.mcLibActionBtnBox));
        holder.setReplyBtn((ImageView) convertView.findViewById(R.id.mcLibReplyBtn));
        holder.setReplyContainer((RelativeLayout) convertView.findViewById(R.id.mcLibReplyContainer));
        holder.setShareContentBtn((ImageView) convertView.findViewById(R.id.mcLibShareStatusBtn));
        holder.setShareContentContainer((RelativeLayout) convertView.findViewById(R.id.mcLibShareStatusContainer));
        holder.setReplyThreadBtn((ImageView) convertView.findViewById(R.id.mcLibReplyThreadBtn));
        holder.setReplyThreadContainer((RelativeLayout) convertView.findViewById(R.id.mcLibReplyThreadContainer));
        holder.setReviewSourceArticleBtn((ImageView) convertView.findViewById(R.id.mcLibReviewSourceBtn));
        holder.setReviewSourceArticleContainer((RelativeLayout) convertView.findViewById(R.id.mcLibReviewSourceContainer));
        holder.setMainPhotoPrgBar((ProgressBar) convertView.findViewById(R.id.mcLibMainPhotoProgressBar));
        holder.setReplyPhotoPrgBar((ProgressBar) convertView.findViewById(R.id.mcLibReplyPhotoProgressBar));
        holder.setFloorTextView((TextView) convertView.findViewById(R.id.mcLibFloorText));
    }

    private void updateTitlePubTime(MCLibStatusListAdapterHolder holder, MCLibUserStatus statusModel) {
        holder.getPubTimeTextView().setText(MCLibDateUtil.getTimeInterval(statusModel.getTime(), this.context));
        String tag = "";
        if (statusModel.getTopicStatus() == 1) {
            holder.getTopIconIndicator().setVisibility(0);
            tag = getContext().getResources().getString(R.string.mc_lib_status_top_tag);
        } else {
            holder.getTopIconIndicator().setVisibility(4);
        }
        if (statusModel.getTopicStatus() == 2) {
            holder.getBulletinIconIndicator().setVisibility(0);
            tag = getContext().getResources().getString(R.string.mc_lib_status_bulletin_tag);
        } else {
            holder.getBulletinIconIndicator().setVisibility(4);
        }
        if (statusModel.getCommunicationType() == 12 || statusModel.getCommunicationType() == 18 || statusModel.getCommunicationType() == 17 || statusModel.getCommunicationType() == 16) {
            holder.getReplyNumTextView().setVisibility(8);
            formEventContent(this.context, holder.getUserNameTextView(), holder.getMainContentTextView(), statusModel);
            return;
        }
        if (statusModel.isDel()) {
            holder.getMainContentTextView().setText((int) R.string.mc_lib_status_deleted);
        } else {
            holder.getMainContentTextView().setText(MCLibTextViewUtil.highlightTag(MCLibTextViewUtil.transformCharSequenceWithEmotions(tag + statusModel.getContent(), this.context), tag, this.context));
        }
        holder.getReplyNumTextView().setText(this.context.getResources().getString(R.string.mc_lib_status_replies) + ": " + statusModel.getReplyNum());
        formStatusTitle(this.context, holder, statusModel);
    }

    private void updateFloorNum(MCLibStatusListAdapterHolder holder, int floorNum) {
        if (this.isReplyHistory) {
            if (floorNum == 0) {
                holder.getFloorTextView().setText((int) R.string.mc_lib_topic_starter);
            } else {
                holder.getFloorTextView().setText(MCLibStringBundleUtil.resolveString(R.string.mc_lib_at_floor, new String[]{"" + floorNum}, this.context));
            }
            holder.getFloorTextView().setVisibility(0);
        }
    }

    private void updatePhotos(MCLibStatusListAdapterHolder holder, final MCLibUserStatus statusModel) {
        if (statusModel.getPhotoPath() == null || "".equals(statusModel.getPhotoPath()) || statusModel.isDel()) {
            holder.getMainPhotoIndicator().setVisibility(8);
            holder.getMainPhotoImage().setVisibility(8);
            holder.getMainPhotoPrgBar().setVisibility(8);
        } else {
            holder.getMainPhotoIndicator().setVisibility(0);
            if (!(this.context instanceof MCLibCommunityBundleActivity)) {
                holder.getMainPhotoImage().setVisibility(0);
                holder.getMainPhotoPrgBar().setVisibility(0);
                holder.getMainPhotoImage().setTag(statusModel.getStatusId() + statusModel.getImgUrl() + statusModel.getPhotoPath());
                updateImage(statusModel.getStatusId(), statusModel.getImgUrl() + statusModel.getPhotoPath(), MCLibConstants.RESOLUTION_100X100, holder.getMainPhotoImage(), holder.getMainPhotoPrgBar(), false, false);
                holder.getMainPhotoImage().setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        if (MCLibStatusListAdapter.this.context instanceof MCLibCommunityBundleActivity) {
                            ((MCLibCommunityBundleActivity) MCLibStatusListAdapter.this.context).showPreviewImage(statusModel.getImgUrl() + statusModel.getPhotoPath());
                        } else if (MCLibStatusListAdapter.this.context instanceof MCLibStatusRepliesActivity) {
                            ((MCLibStatusRepliesActivity) MCLibStatusListAdapter.this.context).showPreviewImage(statusModel.getImgUrl() + statusModel.getPhotoPath());
                        }
                    }
                });
            } else if (((MCLibCommunityBundleActivity) this.context).getStatusType().intValue() != 1) {
                holder.getMainPhotoImage().setVisibility(0);
                holder.getMainPhotoPrgBar().setVisibility(0);
                holder.getMainPhotoImage().setTag(statusModel.getStatusId() + statusModel.getImgUrl() + statusModel.getPhotoPath());
                updateImage(statusModel.getStatusId(), statusModel.getImgUrl() + statusModel.getPhotoPath(), MCLibConstants.RESOLUTION_100X100, holder.getMainPhotoImage(), holder.getMainPhotoPrgBar(), false, false);
                holder.getMainPhotoImage().setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        if (MCLibStatusListAdapter.this.context instanceof MCLibCommunityBundleActivity) {
                            ((MCLibCommunityBundleActivity) MCLibStatusListAdapter.this.context).showPreviewImage(statusModel.getImgUrl() + statusModel.getPhotoPath());
                        }
                    }
                });
            } else {
                holder.getMainPhotoImage().setVisibility(8);
                holder.getMainPhotoPrgBar().setVisibility(8);
            }
        }
        if (statusModel.getReplyStatus() == null || statusModel.getReplyStatus().getPhotoPath() == null || "".equals(statusModel.getReplyStatus().getPhotoPath()) || statusModel.getReplyStatus().isDel()) {
            holder.getReplyPhotoImage().setVisibility(8);
            holder.getReplyPhotoImage().setVisibility(8);
            holder.getReplyPhotoPrgBar().setVisibility(8);
            return;
        }
        holder.getReplyPhotoImage().setBackgroundResource(R.drawable.mc_lib_saying_icon1);
        holder.getReplyPhotoImage().setVisibility(0);
        holder.getReplyPhotoPrgBar().setVisibility(0);
        holder.getReplyPhotoImage().setTag(statusModel.getStatusId() + statusModel.getReplyStatus().getImgUrl() + statusModel.getReplyStatus().getPhotoPath());
        updateImage(statusModel.getStatusId(), statusModel.getReplyStatus().getImgUrl() + statusModel.getReplyStatus().getPhotoPath(), MCLibConstants.RESOLUTION_100X100, holder.getReplyPhotoImage(), holder.getReplyPhotoPrgBar(), false, false);
        holder.getReplyPhotoImage().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (MCLibStatusListAdapter.this.context instanceof MCLibCommunityBundleActivity) {
                    ((MCLibCommunityBundleActivity) MCLibStatusListAdapter.this.context).showPreviewImage(statusModel.getReplyStatus().getImgUrl() + statusModel.getReplyStatus().getPhotoPath());
                } else if (MCLibStatusListAdapter.this.context instanceof MCLibStatusRepliesActivity) {
                    ((MCLibStatusRepliesActivity) MCLibStatusListAdapter.this.context).showPreviewImage(statusModel.getReplyStatus().getImgUrl() + statusModel.getReplyStatus().getPhotoPath());
                }
            }
        });
    }

    private void updateUserImage(MCLibStatusListAdapterHolder holder, final MCLibUserStatus statusModel) {
        holder.getUserPhotoImageView().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(MCLibStatusListAdapter.this.context, MCLibUserHomeActivity.class);
                if (statusModel.getCommunicationType() == 18 || statusModel.getCommunicationType() == 12 || statusModel.getCommunicationType() == 17 || statusModel.getCommunicationType() == 16) {
                    intent.putExtra("userId", statusModel.getFromUserId());
                    MCLibStatusListAdapter.this.context.startActivity(intent);
                    return;
                }
                intent.putExtra("userId", statusModel.getUid());
                MCLibStatusListAdapter.this.context.startActivity(intent);
            }
        });
        holder.getUserPhotoImageView().setTag(statusModel.getStatusId() + statusModel.getUserImageUrl() + "");
        updateUserImage(statusModel, holder);
    }

    private void updateSourceInfo(MCLibStatusListAdapterHolder holder, final MCLibUserStatus statusModel) {
        if (statusModel.getSourceProId() == 0) {
            holder.getSourceName().setVisibility(8);
            return;
        }
        holder.getSourceName().setVisibility(0);
        holder.getSourceName().setText(Html.fromHtml("<u>" + this.context.getResources().getString(R.string.mc_lib_source) + ": " + statusModel.getSourceName() + "</u>"));
        holder.getSourceName().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                new MCLibAppInfoDialog(MCLibStatusListAdapter.this.context, R.style.mc_lib_dialog, statusModel.getSourceProId(), MCLibStatusListAdapter.this.mHandler).show();
            }
        });
    }

    private void initActionBoxBtns(MCLibStatusListAdapterHolder holder, final MCLibUserStatus userStatus) {
        holder.getActionBtnBox().setVisibility(8);
        holder.getReplyContainer().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(MCLibStatusListAdapter.this.context, MCLibPublishStatusActivity.class);
                intent.putExtra(MCLibParameterKeyConstant.STATUS_ROOT_ID, userStatus.getRootId() == -1 ? userStatus.getStatusId() : userStatus.getRootId());
                intent.putExtra(MCLibParameterKeyConstant.STATUS_REPLY_ID, userStatus.getStatusId());
                MCLibStatusListAdapter.this.context.startActivity(intent);
            }
        });
        holder.getShareContentContainer().setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Intent intent = new Intent(MCLibStatusListAdapter.this.context, MCShareAppActivity.class);
                intent.putExtra("shareContent", userStatus.getContent());
                if (userStatus.getPhotoPath() == null || "".equals(userStatus.getPhotoPath())) {
                    intent.putExtra("sharePic", "");
                } else {
                    intent.putExtra("sharePic", userStatus.getImgUrl() + userStatus.getPhotoPath());
                }
                intent.putExtra("uid", new MCLibUserInfoServiceImpl(MCLibStatusListAdapter.this.context).getLoginUserId());
                intent.putExtra("appKey", MCLibStatusListAdapter.this.context.getResources().getString(R.string.mc_lib_bp_app_key));
                MCLibStatusListAdapter.this.context.startActivity(intent);
            }
        });
        holder.getReplyThreadContainer().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(MCLibStatusListAdapter.this.context, MCLibStatusRepliesActivity.class);
                intent.putExtra("id", userStatus.getRootId() == -1 ? userStatus.getStatusId() : userStatus.getRootId());
                MCLibStatusListAdapter.this.context.startActivity(intent);
            }
        });
        if (this.isCommentList) {
            holder.getReviewSourceArticleContainer().setVisibility(0);
            holder.getReviewSourceArticleContainer().setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    new MCLibWebViewDialog(MCLibStatusListAdapter.this.getContext(), R.style.mc_lib_dialog, MCLibStatusListAdapter.this.sourceArticleUrl).show();
                }
            });
        } else if (userStatus.getCommunicationType() == 8) {
            holder.getReviewSourceArticleContainer().setVisibility(0);
            holder.getReviewSourceArticleContainer().setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    new MCLibWebViewDialog(MCLibStatusListAdapter.this.getContext(), R.style.mc_lib_dialog, userStatus.getContentPathUrl()).show();
                }
            });
        } else {
            holder.getReviewSourceArticleContainer().setVisibility(8);
        }
    }

    /* access modifiers changed from: private */
    public void showAnimActionBox(RelativeLayout actionBox) {
        Animation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setFillAfter(false);
        anim.setDuration(200);
        actionBox.startAnimation(anim);
    }

    public void hideAnimActionBox(final RelativeLayout actionBox) {
        this.mHandler.post(new Runnable() {
            public void run() {
                actionBox.setVisibility(8);
            }
        });
    }

    public static void formStatusTitle(Context context2, MCLibStatusListAdapterHolder holder, MCLibUserStatus statusModel) {
        if (statusModel.getCommunicationType() == 1 || statusModel.getCommunicationType() == 8) {
            String name = statusModel.getUserName();
            if (name == null) {
                name = statusModel.getFromUserName();
            }
            if (statusModel.getToUid() > 0) {
                holder.getUserNameTextView().setText(Html.fromHtml(MCLibStringBundleUtil.resolveString(R.string.mc_lib_who_reply_who, new String[]{"<font color=" + colorUserName + ">" + name + "</font>", "<font color=" + colorUserName + ">" + statusModel.getToUserName() + "</font>"}, context2)));
            } else {
                holder.getUserNameTextView().setText(Html.fromHtml("<font color=" + colorUserName + ">" + name + "</font>"));
            }
        } else if (statusModel.getCommunicationType() == 2 || statusModel.getCommunicationType() == 0) {
            String fromName = statusModel.getUserName();
            String toName = statusModel.getToUserName();
            if (fromName == null) {
                fromName = statusModel.getFromUserName();
            }
            holder.getUserNameTextView().setText(Html.fromHtml(MCLibStringBundleUtil.resolveString(R.string.mc_lib_who_reply_who, new String[]{"<font color=" + colorUserName + ">" + fromName + "</font>", "<font color=" + colorUserName + ">" + toName + "</font>"}, context2)));
        }
        if (statusModel.getReplyStatus() != null && statusModel.getReplyStatus().getContent() != null && !"".equals(statusModel.getReplyStatus().getContent()) && holder.getReferTitleTextView() != null && holder.getReferContentTextView() != null) {
            MCLibStatusListAdapterHolder holderReply = new MCLibStatusListAdapterHolder();
            holderReply.setUserNameTextView(holder.getReferTitleTextView());
            formStatusTitle(context2, holderReply, statusModel.getReplyStatus());
            if (statusModel.getReplyStatus() == null) {
                holder.getReferContentTextView().setText("");
            } else if (statusModel.getReplyStatus().isDel()) {
                holder.getReferContentTextView().setText((int) R.string.mc_lib_status_deleted);
            } else {
                holder.getReferContentTextView().setText(MCLibTextViewUtil.transformCharSequenceWithEmotions(statusModel.getReplyStatus().getContent(), context2));
            }
        }
    }

    public static void formEventContent(Context context2, TextView userText, TextView contentText, MCLibUserStatus statusModel) {
        String name = statusModel.getFromUserName();
        String targetName = statusModel.getTargetJumpName();
        userText.setText(Html.fromHtml("<font color=" + colorUserName + ">" + name + "</font>"));
        if (statusModel.getCommunicationType() == 18) {
            contentText.setText(Html.fromHtml(MCLibStringBundleUtil.resolveString(R.string.mc_lib_app_using_content, new String[]{name, targetName}, context2)));
        } else if (statusModel.getCommunicationType() == 12) {
            contentText.setText(Html.fromHtml(MCLibStringBundleUtil.resolveString(R.string.mc_lib_follow_content, new String[]{name, targetName}, context2)));
        } else if (statusModel.getCommunicationType() == 17) {
            userText.setText(Html.fromHtml(MCLibStringBundleUtil.resolveString(R.string.mc_lib_update_mood_title, new String[]{"<font color=" + colorUserName + ">" + name + "</font>"}, context2)));
            contentText.setText(statusModel.getContent());
        } else if (statusModel.getCommunicationType() == 16) {
            contentText.setText(Html.fromHtml(MCLibStringBundleUtil.resolveString(R.string.mc_lib_change_photo_content, new String[]{name}, context2)));
        }
    }

    private void updateUserImage(final MCLibUserStatus statusModel, MCLibStatusListAdapterHolder holder) {
        Bitmap cachedImage = this.asyncImageLoader.loadBitmap(statusModel.getUserImageUrl() + "", new MCLibBitmapCallback() {
            public void imageLoaded(Bitmap bitmap, String imageUrl) {
                ImageView imageViewByTag = (ImageView) MCLibStatusListAdapter.this.listView.findViewWithTag(statusModel.getStatusId() + imageUrl);
                if (imageViewByTag == null) {
                    return;
                }
                if (bitmap == null) {
                    imageViewByTag.setBackgroundResource(R.drawable.mc_lib_face_u0);
                } else if (bitmap.isRecycled()) {
                    imageViewByTag.setBackgroundResource(R.drawable.mc_lib_face_u0);
                } else {
                    imageViewByTag.setBackgroundDrawable(new BitmapDrawable(bitmap));
                }
            }
        });
        if (cachedImage == null) {
            holder.getUserPhotoImageView().setBackgroundResource(R.drawable.mc_lib_face_u0);
        } else if (cachedImage.isRecycled()) {
            holder.getUserPhotoImageView().setBackgroundResource(R.drawable.mc_lib_face_u0);
        } else {
            holder.getUserPhotoImageView().setBackgroundDrawable(new BitmapDrawable(cachedImage));
        }
    }

    public void updateImage(long statusId, String imgUrl, String resolution, final ImageView imageView, final ProgressBar prgBar, boolean isForceReload, boolean isForceGetByHttp) {
        final ProgressBar progressBar = prgBar;
        final long j = statusId;
        final ImageView imageView2 = imageView;
        final Bitmap cachedImage = this.asyncImageLoader.loadBitmap(imgUrl + "", resolution, isForceReload, isForceGetByHttp, new MCLibBitmapCallback() {
            public void imageLoaded(Bitmap bitmap, String imageUrl) {
                progressBar.setVisibility(8);
                ImageView imageViewByTag = (ImageView) MCLibStatusListAdapter.this.listView.findViewWithTag(j + imageUrl);
                if (imageViewByTag != null) {
                    imageViewByTag.setVisibility(0);
                    if (bitmap == null) {
                        imageViewByTag.setBackgroundResource(R.drawable.mc_lib_saying_icon1);
                    } else if (bitmap.isRecycled()) {
                        imageViewByTag.setBackgroundResource(R.drawable.mc_lib_saying_icon1);
                    } else {
                        imageViewByTag.setBackgroundDrawable(new BitmapDrawable(MCLibStatusListAdapter.this.getScaledBitmap(bitmap, 0.33333334f)));
                    }
                } else {
                    imageView2.setBackgroundDrawable(new BitmapDrawable(MCLibStatusListAdapter.this.getScaledBitmap(bitmap, 0.6666667f)));
                }
            }
        });
        if (statusId != 0) {
            this.mHandler.post(new Runnable() {
                public void run() {
                    if (cachedImage == null) {
                        return;
                    }
                    if (cachedImage.isRecycled()) {
                        prgBar.setVisibility(0);
                        imageView.setVisibility(8);
                        return;
                    }
                    prgBar.setVisibility(8);
                    imageView.setVisibility(0);
                    imageView.setBackgroundDrawable(new BitmapDrawable(MCLibStatusListAdapter.this.getScaledBitmap(cachedImage, 0.33333334f)));
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public Bitmap getScaledBitmap(Bitmap mBitmap, float proposeWidthRatio) {
        if (mBitmap == null) {
            return null;
        }
        int proposeWidth = (int) (((float) MCLibPhoneUtil.getDisplayWidth(this.context)) * proposeWidthRatio);
        return Bitmap.createScaledBitmap(mBitmap, proposeWidth, (int) (((float) mBitmap.getHeight()) * (((float) proposeWidth) / ((float) mBitmap.getWidth()))), true);
    }

    public List<MCLibUserStatus> getStatusList() {
        return this.statusList;
    }

    public void setStatusList(List<MCLibUserStatus> statusList2) {
        if (this.isCommentList && !statusList2.isEmpty()) {
            this.sourceArticleUrl = statusList2.get(0).getContentPathUrl();
        }
        this.statusList = statusList2;
    }
}
