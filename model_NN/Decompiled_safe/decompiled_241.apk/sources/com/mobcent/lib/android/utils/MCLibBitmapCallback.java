package com.mobcent.lib.android.utils;

import android.graphics.Bitmap;

public interface MCLibBitmapCallback {
    void imageLoaded(Bitmap bitmap, String str);
}
