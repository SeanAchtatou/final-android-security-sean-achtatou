package com.mobcent.lib.android.ui.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.DownloadListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import com.mobcent.android.constants.MCLibParameterKeyConstant;
import com.mobcent.android.seed.autovGhnzo98NIzLsRPLHS.R;
import com.mobcent.android.service.impl.MCLibUserInfoServiceImpl;
import com.mobcent.android.utils.MCSharePhoneUtil;
import com.mobcent.lib.android.ui.activity.model.MCLibGoToActivityModel;

public class MCLibGuideSyncActivity extends MCLibUIBaseActivity {
    private Button finishButton;
    /* access modifiers changed from: private */
    public MCLibGoToActivityModel goToActivityModel;
    private Handler mHandler = new Handler();
    private WebView mWebView;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.mc_lib_guide_sync);
        this.goToActivityModel = (MCLibGoToActivityModel) getIntent().getSerializableExtra(MCLibParameterKeyConstant.GO_TO_ACTIVITY_CLASS);
        initWindowTitle();
        initWidgets();
        initProgressBox();
        initFinishButton();
        initWebViewPage("http://sdk.mobcent.com/sharesdk/weibo/weiboList.do?imei=" + MCSharePhoneUtil.getIMEI(this) + "&appKey=" + getResources().getString(R.string.mc_lib_bp_app_key) + "&platType=1&isJson=false&userId=" + new MCLibUserInfoServiceImpl(this).getLoginUserId());
    }

    private void initWidgets() {
        this.finishButton = (Button) findViewById(R.id.mcLibFinishBtn);
    }

    private void initFinishButton() {
        this.finishButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (MCLibGuideSyncActivity.this.goToActivityModel == null) {
                    MCLibGuideSyncActivity.this.startActivity(new Intent(MCLibGuideSyncActivity.this, MCLibCommunityBundleActivity.class));
                } else {
                    MCLibGuideSyncActivity.this.startActivity(new Intent(MCLibGuideSyncActivity.this, MCLibGuideSyncActivity.this.goToActivityModel.getGoToActicityClass()));
                }
                MCLibGuideSyncActivity.this.finish();
            }
        });
    }

    public Handler getCurrentHandler() {
        return this.mHandler;
    }

    /* access modifiers changed from: protected */
    public void initWebViewPage(String webUrl) {
        this.mWebView = (WebView) findViewById(R.id.mcLibWebView);
        this.mWebView.getSettings().setJavaScriptEnabled(true);
        this.mWebView.getSettings().setBuiltInZoomControls(false);
        this.mWebView.getSettings().setSupportZoom(true);
        this.mWebView.setDownloadListener(new DownloadListener() {
            public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimetype, long contentLength) {
                MCLibGuideSyncActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url)));
            }
        });
        this.mWebView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                MCLibGuideSyncActivity.this.showProgressBar();
            }

            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                MCLibGuideSyncActivity.this.hideProgressBar();
            }
        });
        this.mWebView.loadUrl(webUrl);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (!this.mWebView.canGoBack() || keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        this.mWebView.goBack();
        return true;
    }
}
