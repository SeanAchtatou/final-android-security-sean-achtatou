package com.mobcent.lib.android.ui.delegate.impl;

import android.content.Context;
import android.os.Handler;
import android.widget.ImageView;
import android.widget.Toast;
import com.mobcent.android.seed.autovGhnzo98NIzLsRPLHS.R;
import com.mobcent.android.service.MCLibUserInfoService;
import com.mobcent.android.service.impl.MCLibUserInfoServiceImpl;
import com.mobcent.lib.android.constants.MCLibResourceConstant;
import com.mobcent.lib.android.ui.delegate.MCLibUserActionDelegate;

public class MCLibUpdateUserPhotoDelegateImpl implements MCLibUserActionDelegate {
    /* access modifiers changed from: private */
    public Context context;
    /* access modifiers changed from: private */
    public Handler handler;
    /* access modifiers changed from: private */
    public String iconUrl;
    /* access modifiers changed from: private */
    public int type;
    /* access modifiers changed from: private */
    public ImageView userImageView;

    public MCLibUpdateUserPhotoDelegateImpl(Context context2, Handler handler2, ImageView userImageView2) {
        this.handler = handler2;
        this.context = context2;
        this.userImageView = userImageView2;
    }

    public void doUserAction() {
        new Thread() {
            public void run() {
                if (MCLibUpdateUserPhotoDelegateImpl.this.userImageView != null) {
                    MCLibUpdateUserPhotoDelegateImpl.this.handler.post(new Runnable() {
                        public void run() {
                            MCLibUpdateUserPhotoDelegateImpl.this.userImageView.setBackgroundResource(MCLibResourceConstant.getMCResourceConstant().getAllResourceMap().get(MCLibUpdateUserPhotoDelegateImpl.this.iconUrl).intValue());
                        }
                    });
                }
                final MCLibUserInfoService userInfoService = new MCLibUserInfoServiceImpl(MCLibUpdateUserPhotoDelegateImpl.this.context);
                final String result = userInfoService.changeUserPhoto(userInfoService.getLoginUserId(), MCLibUpdateUserPhotoDelegateImpl.this.iconUrl, MCLibUpdateUserPhotoDelegateImpl.this.type);
                MCLibUpdateUserPhotoDelegateImpl.this.handler.post(new Runnable() {
                    public void run() {
                        if (result == null || !result.equals("rs_succ")) {
                            Toast.makeText(MCLibUpdateUserPhotoDelegateImpl.this.context, result, 1).show();
                            return;
                        }
                        userInfoService.updateSharedUserPhoto(MCLibUpdateUserPhotoDelegateImpl.this.iconUrl);
                        Toast.makeText(MCLibUpdateUserPhotoDelegateImpl.this.context, (int) R.string.mc_lib_upload_succ, 1).show();
                    }
                });
            }
        }.start();
    }

    public String getIconUrl() {
        return this.iconUrl;
    }

    public void setIconUrl(String iconUrl2) {
        this.iconUrl = iconUrl2;
    }

    public int getType() {
        return this.type;
    }

    public void setType(int type2) {
        this.type = type2;
    }
}
