package com.mobcent.lib.android.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Message;
import com.mobcent.android.constants.MCLibConstants;
import com.mobcent.lib.android.constants.MCLibResourceConstant;
import java.lang.ref.SoftReference;
import java.util.HashMap;
import java.util.List;

public class MCLibAsyncImageLoader {
    /* access modifiers changed from: private */
    public HashMap<String, SoftReference<Bitmap>> bitmapCache = new HashMap<>();
    /* access modifiers changed from: private */
    public Context context;

    public MCLibAsyncImageLoader(Context context2) {
        this.context = context2;
    }

    public Bitmap loadBitmap(String imageUrl, MCLibBitmapCallback bitmapCallback) {
        return loadBitmap(imageUrl, MCLibConstants.RESOLUTION_320X480, false, false, bitmapCallback);
    }

    /* Debug info: failed to restart local var, previous not found, register: 15 */
    public Bitmap loadBitmap(String imageUrl, String resolution, boolean isForceReload, boolean isForceGetByHttp, MCLibBitmapCallback bitmapCallback) {
        Bitmap bitmap;
        if (imageUrl == null || "".equals(imageUrl)) {
            return null;
        }
        if (!isForceReload) {
            HashMap<String, Integer> resourceMap = MCLibResourceConstant.getMCResourceConstant().getAllResourceMap();
            String urlFileName = imageUrl.substring(imageUrl.lastIndexOf("/") + 1);
            if (resourceMap.containsKey(urlFileName)) {
                return BitmapFactory.decodeResource(this.context.getResources(), resourceMap.get(urlFileName).intValue());
            }
            if (this.bitmapCache.containsKey(imageUrl) && (bitmap = (Bitmap) this.bitmapCache.get(imageUrl).get()) != null) {
                return bitmap;
            }
        }
        final MCLibBitmapCallback mCLibBitmapCallback = bitmapCallback;
        final String str = imageUrl;
        final AnonymousClass1 r0 = new Handler() {
            public void handleMessage(Message message) {
                mCLibBitmapCallback.imageLoaded((Bitmap) message.obj, str);
            }
        };
        final String str2 = imageUrl;
        final String str3 = resolution;
        final boolean z = isForceGetByHttp;
        final boolean z2 = isForceReload;
        MCLibImageUtil.threadPool.execute(new Thread() {
            public void run() {
                Bitmap bitmap = MCLibImageLoader.getInstance(MCLibAsyncImageLoader.this.context).loadBitmap(str2, str3, z);
                if (bitmap != null && bitmap.isRecycled()) {
                    bitmap = MCLibImageLoader.getInstance(MCLibAsyncImageLoader.this.context).loadBitmap(str2, str3, z);
                }
                if (!z2 && !z) {
                    MCLibAsyncImageLoader.this.bitmapCache.put(str2, new SoftReference(bitmap));
                }
                r0.sendMessage(r0.obtainMessage(0, bitmap));
            }
        });
        return null;
    }

    public void recycleBitmap(List<String> imageUrls) {
        Bitmap bitmap;
        for (String imageUrl : imageUrls) {
            SoftReference<Bitmap> softReference = this.bitmapCache.get(imageUrl);
            if (!(softReference == null || (bitmap = (Bitmap) softReference.get()) == null)) {
                if (!bitmap.isRecycled()) {
                    bitmap.recycle();
                }
                this.bitmapCache.remove(imageUrl);
            }
        }
    }
}
