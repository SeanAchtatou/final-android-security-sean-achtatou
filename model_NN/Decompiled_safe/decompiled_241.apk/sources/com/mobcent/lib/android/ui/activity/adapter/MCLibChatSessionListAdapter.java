package com.mobcent.lib.android.ui.activity.adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.mobcent.android.constants.MCLibParameterKeyConstant;
import com.mobcent.android.model.MCLibUserInfo;
import com.mobcent.android.seed.autovGhnzo98NIzLsRPLHS.R;
import com.mobcent.lib.android.ui.activity.MCLibChatRoomActivity;
import com.mobcent.lib.android.ui.activity.MCLibUserHomeActivity;
import com.mobcent.lib.android.ui.activity.adapter.holder.MCLibUserInfoHolder;
import com.mobcent.lib.android.ui.dialog.MCLibAppInfoDialog;
import com.mobcent.lib.android.utils.MCLibAsyncImageLoader;
import com.mobcent.lib.android.utils.MCLibBitmapCallback;
import com.mobcent.lib.android.utils.MCLibDateUtil;
import java.util.List;

public class MCLibChatSessionListAdapter extends MCLibBaseArrayAdapter {
    public static String colorUserName = "#0066FF";
    /* access modifiers changed from: private */
    public final Activity activity;
    public MCLibAsyncImageLoader asyncImageLoader;
    /* access modifiers changed from: private */
    public ListView listView;
    /* access modifiers changed from: private */
    public Handler mHandler;
    private int rowResourceId;
    private List<MCLibUserInfo> userInfoList;

    public MCLibChatSessionListAdapter(Activity activity2, ListView listView2, int rowResourceId2, List<MCLibUserInfo> userInfoList2, Handler mHandler2) {
        super(activity2, rowResourceId2, userInfoList2);
        this.rowResourceId = rowResourceId2;
        this.inflater = LayoutInflater.from(activity2);
        this.activity = activity2;
        this.userInfoList = userInfoList2;
        this.listView = listView2;
        this.mHandler = mHandler2;
        this.asyncImageLoader = new MCLibAsyncImageLoader(activity2);
    }

    public int getCount() {
        return this.userInfoList.size();
    }

    public Object getItem(int position) {
        return this.userInfoList.get(position);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        MCLibUserInfoHolder holder;
        final MCLibUserInfo userInfo = this.userInfoList.get(position);
        if (convertView == null) {
            convertView = this.inflater.inflate(this.rowResourceId, (ViewGroup) null);
            holder = new MCLibUserInfoHolder();
            convertView.setTag(holder);
            initUserInfoHolder(convertView, holder);
        } else {
            holder = (MCLibUserInfoHolder) convertView.getTag();
        }
        if (holder == null) {
            convertView = this.inflater.inflate(this.rowResourceId, (ViewGroup) null);
            holder = new MCLibUserInfoHolder();
            convertView.setTag(holder);
            initUserInfoHolder(convertView, holder);
        }
        if (userInfo.getUnreadMsgCount() > 0) {
            holder.getNewChatIndicatorImage().setVisibility(0);
            holder.getUserName().setText(Html.fromHtml("<font color=" + colorUserName + ">" + userInfo.getName() + "</font>   " + "<font color=green>(" + userInfo.getUnreadMsgCount() + ")</font>"));
        } else {
            holder.getNewChatIndicatorImage().setVisibility(8);
            holder.getUserName().setText(Html.fromHtml("<font color=" + colorUserName + ">" + userInfo.getName() + "</font>"));
        }
        holder.getLastMsgUpdatTimeTextView().setText(MCLibDateUtil.getTimeInterval(MCLibDateUtil.getFormatTime(userInfo.getLastMsgTime()), this.activity));
        holder.getUserImage().setTag(userInfo.getImage() + "");
        updateImage(userInfo, holder);
        holder.getUserImage().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(MCLibChatSessionListAdapter.this.getContext(), MCLibUserHomeActivity.class);
                intent.putExtra("userId", userInfo.getUid());
                MCLibChatSessionListAdapter.this.getContext().startActivity(intent);
            }
        });
        if (userInfo.getSourceProId() == 0) {
            holder.getSourceName().setVisibility(8);
        } else {
            holder.getSourceName().setVisibility(0);
            holder.getSourceName().setText(Html.fromHtml("<u>" + this.activity.getResources().getString(R.string.mc_lib_source) + ": " + userInfo.getSourceName() + "</u>"));
            holder.getSourceName().setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    new MCLibAppInfoDialog(MCLibChatSessionListAdapter.this.activity, R.style.mc_lib_dialog, userInfo.getSourceProId(), MCLibChatSessionListAdapter.this.mHandler).show();
                }
            });
        }
        convertView.setOnClickListener(new ConvertViewOnClickListener(userInfo));
        return convertView;
    }

    private void initUserInfoHolder(View convertView, MCLibUserInfoHolder holder) {
        holder.setUserName((TextView) convertView.findViewById(R.id.mcLibUserName));
        holder.setUserImage((ImageView) convertView.findViewById(R.id.mcLibUserImage));
        holder.setNewChatIndicatorImage((ImageView) convertView.findViewById(R.id.mcLibChatUpdateIndicator));
        holder.setLastMsgUpdatTimeTextView((TextView) convertView.findViewById(R.id.mcLibLastMsgTimeText));
        holder.setSourceName((TextView) convertView.findViewById(R.id.mcLibSourceName));
    }

    private void updateImage(MCLibUserInfo userInfo, MCLibUserInfoHolder holder) {
        Bitmap cachedImage = this.asyncImageLoader.loadBitmap(userInfo.getImage() + "", new MCLibBitmapCallback() {
            public void imageLoaded(Bitmap bitmap, String imageUrl) {
                ImageView imageViewByTag = (ImageView) MCLibChatSessionListAdapter.this.listView.findViewWithTag(imageUrl);
                if (imageViewByTag == null) {
                    return;
                }
                if (bitmap == null) {
                    imageViewByTag.setBackgroundResource(R.drawable.mc_lib_face_u0);
                } else if (bitmap.isRecycled()) {
                    imageViewByTag.setBackgroundResource(R.drawable.mc_lib_face_u0);
                } else {
                    imageViewByTag.setBackgroundDrawable(new BitmapDrawable(bitmap));
                }
            }
        });
        if (cachedImage == null) {
            holder.getUserImage().setBackgroundResource(R.drawable.mc_lib_face_u0);
        } else if (cachedImage.isRecycled()) {
            holder.getUserImage().setBackgroundResource(R.drawable.mc_lib_face_u0);
        } else {
            holder.getUserImage().setBackgroundDrawable(new BitmapDrawable(cachedImage));
        }
    }

    public class ConvertViewOnClickListener implements View.OnClickListener {
        private MCLibUserInfo userInfo;

        public ConvertViewOnClickListener(MCLibUserInfo userInfo2) {
            this.userInfo = userInfo2;
        }

        public void onClick(View v) {
            Intent intent = new Intent(MCLibChatSessionListAdapter.this.activity, MCLibChatRoomActivity.class);
            intent.putExtra("userId", this.userInfo.getUid());
            intent.putExtra("nickName", this.userInfo.getName());
            intent.putExtra(MCLibParameterKeyConstant.USER_PHOTO, this.userInfo.getImage());
            MCLibChatSessionListAdapter.this.activity.startActivity(intent);
        }
    }

    public List<MCLibUserInfo> getUserInfoList() {
        return this.userInfoList;
    }

    public void setUserInfoList(List<MCLibUserInfo> userInfoList2) {
        this.userInfoList = userInfoList2;
    }
}
