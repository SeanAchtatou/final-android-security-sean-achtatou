package com.mobcent.lib.android.utils;

import java.util.regex.Pattern;

public class MCLibStringUtil {
    public static final String ORIGINAL_SIZE = "_24x24";

    public static String constructPicture(String s, String newSize) {
        return s.replace(ORIGINAL_SIZE, newSize);
    }

    public static String constructSmallPicture(String s) {
        return constructPicture(s, ORIGINAL_SIZE);
    }

    public static String constructNormalPicture(String s) {
        return constructPicture(s, "_48x48");
    }

    public static boolean validateEmail(String email) {
        if (email == null || email.equals("")) {
            return false;
        }
        if (!isEmailValid(email)) {
            return false;
        }
        return true;
    }

    public static boolean isEmailValid(String s) {
        return Pattern.compile("^([a-z0-9A-Z_]+[-\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$").matcher(s).matches();
    }

    public static boolean isPwdMatchRule(String pwd) {
        if (pwd == null) {
            return false;
        }
        return Pattern.compile("[A-Za-z0-9]+").matcher(pwd).matches();
    }

    public static boolean isNameMatchRule(String name) {
        if (name == null) {
            return false;
        }
        return Pattern.compile("[A-Za-z0-9_]+").matcher(name).matches();
    }

    public static boolean isChinese(String str) {
        return Pattern.matches("[一-龥]", str);
    }

    public static boolean isNickNameMatchRule(String nickName) {
        int len = nickName.length();
        for (int i = 0; i < len; i++) {
            String str = nickName.substring(i, i + 1);
            if (!isChinese(str) && !isNameMatchRule(str)) {
                return false;
            }
        }
        return true;
    }

    public static boolean isNumeric(String str) {
        if (!Pattern.compile("[0-9]*").matcher(str).matches()) {
            return false;
        }
        return true;
    }
}
