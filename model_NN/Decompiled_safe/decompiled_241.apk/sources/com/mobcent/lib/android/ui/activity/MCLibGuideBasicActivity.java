package com.mobcent.lib.android.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.mobcent.android.constants.MCLibConstants;
import com.mobcent.android.constants.MCLibParameterKeyConstant;
import com.mobcent.android.model.MCLibUserInfo;
import com.mobcent.android.seed.autovGhnzo98NIzLsRPLHS.R;
import com.mobcent.android.service.MCLibUserInfoService;
import com.mobcent.android.service.impl.MCLibUserInfoServiceImpl;
import com.mobcent.lib.android.ui.activity.model.MCLibGoToActivityModel;
import com.mobcent.lib.android.utils.MCLibStringUtil;
import com.mobcent.lib.android.utils.i18n.MCLibStringBundleUtil;

public class MCLibGuideBasicActivity extends MCLibUIBaseActivity {
    /* access modifiers changed from: private */
    public EditText emailField;
    private Spinner genderSpinner;
    /* access modifiers changed from: private */
    public MCLibGoToActivityModel goToActivityModel;
    private Handler mHandler = new Handler();
    /* access modifiers changed from: private */
    public Button nextButton;
    /* access modifiers changed from: private */
    public EditText nickNameField;
    /* access modifiers changed from: private */
    public ProgressBar progressBar;
    private TextView userAccountTextView;
    /* access modifiers changed from: private */
    public MCLibUserInfo userInfo;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.mc_lib_guide_basic);
        initWindowTitle();
        this.userInfo = new MCLibUserInfoServiceImpl(this).getLoginUser();
        this.goToActivityModel = (MCLibGoToActivityModel) getIntent().getSerializableExtra(MCLibParameterKeyConstant.GO_TO_ACTIVITY_CLASS);
        initWidgets();
        initGenderSpinner();
        initNextStepWidget();
    }

    private void initWidgets() {
        this.userAccountTextView = (TextView) findViewById(R.id.mcLibUserAccount);
        this.nickNameField = (EditText) findViewById(R.id.mcLibUserNickNameField);
        this.emailField = (EditText) findViewById(R.id.mcLibUserEmailField);
        this.genderSpinner = (Spinner) findViewById(R.id.mcLibGenderSpinner);
        this.nextButton = (Button) findViewById(R.id.mcLibNextStep);
        this.progressBar = (ProgressBar) findViewById(R.id.mcLibProgressBar);
        this.userAccountTextView.setText(MCLibStringBundleUtil.resolveString(R.string.mc_lib_user_id, new String[]{this.userInfo.getName()}, this));
    }

    private void initGenderSpinner() {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, 17367048);
        adapter.add(getResources().getString(R.string.mc_lib_male));
        adapter.add(getResources().getString(R.string.mc_lib_female));
        adapter.add(getResources().getString(R.string.mc_lib_secret));
        adapter.setDropDownViewResource(17367049);
        this.genderSpinner.setAdapter((SpinnerAdapter) adapter);
        this.genderSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                if (position == 0) {
                    MCLibGuideBasicActivity.this.userInfo.setGender(MCLibConstants.ANDROID);
                } else if (position == 1) {
                    MCLibGuideBasicActivity.this.userInfo.setGender("0");
                } else {
                    MCLibGuideBasicActivity.this.userInfo.setGender("2");
                }
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
                MCLibGuideBasicActivity.this.userInfo.setGender("2");
            }
        });
        if ("0".equals(this.userInfo.getGender())) {
            this.genderSpinner.setSelection(1, true);
        } else if (MCLibConstants.ANDROID.equals(this.userInfo.getGender())) {
            this.genderSpinner.setSelection(0, true);
        } else {
            this.genderSpinner.setSelection(2, true);
        }
    }

    private void initNextStepWidget() {
        this.nextButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                new Thread() {
                    public void run() {
                        MCLibUserInfoService userInfoService = new MCLibUserInfoServiceImpl(MCLibGuideBasicActivity.this);
                        String nickName = MCLibGuideBasicActivity.this.nickNameField.getText().toString();
                        int checkResult = MCLibGuideBasicActivity.this.isNickNameValid(nickName);
                        if (checkResult > 0) {
                            MCLibGuideBasicActivity.this.showToastMsg(checkResult);
                            return;
                        }
                        String email = MCLibGuideBasicActivity.this.emailField.getText().toString();
                        if (email == null || email.trim().equals("")) {
                            email = "";
                        } else {
                            int checkResult2 = MCLibGuideBasicActivity.this.isEmailValid(email);
                            if (checkResult2 > 0) {
                                MCLibGuideBasicActivity.this.showToastMsg(checkResult2);
                                return;
                            }
                        }
                        MCLibGuideBasicActivity.this.showWaitingPrg();
                        String rs = userInfoService.changeUserProfile(userInfoService.getLoginUserId(), nickName, Integer.parseInt(MCLibGuideBasicActivity.this.userInfo.getGender()), "", email, "");
                        if (rs.equals("rs_succ")) {
                            Intent intent = new Intent(MCLibGuideBasicActivity.this, MCLibGuidePwdActivity.class);
                            if (MCLibGuideBasicActivity.this.goToActivityModel != null) {
                                intent.putExtra(MCLibParameterKeyConstant.GO_TO_ACTIVITY_CLASS, MCLibGuideBasicActivity.this.goToActivityModel);
                            }
                            MCLibGuideBasicActivity.this.startActivity(intent);
                            MCLibGuideBasicActivity.this.finish();
                        } else if (rs.equals("emailExist")) {
                            MCLibGuideBasicActivity.this.showToastMsg(R.string.mc_lib_user_email_exist);
                            MCLibGuideBasicActivity.this.hideWaitingPrg();
                        } else if (rs.equals("nickNameExist")) {
                            MCLibGuideBasicActivity.this.showToastMsg(R.string.mc_lib_user_nick_name_exist);
                            MCLibGuideBasicActivity.this.hideWaitingPrg();
                        } else {
                            MCLibGuideBasicActivity.this.showToastMsg(R.string.mc_lib_generic_error);
                            MCLibGuideBasicActivity.this.hideWaitingPrg();
                        }
                    }
                }.start();
            }
        });
    }

    /* access modifiers changed from: private */
    public int isNickNameValid(String nickName) {
        if (nickName == null || nickName.length() < 2 || nickName.length() > 12) {
            return R.string.mc_lib_nick_name_length_tip;
        }
        if (!MCLibStringUtil.isNickNameMatchRule(nickName)) {
            return R.string.mc_lib_nick_name_invalid_tip;
        }
        return 0;
    }

    /* access modifiers changed from: private */
    public int isEmailValid(String email) {
        if (MCLibStringUtil.isEmailValid(email)) {
            return 0;
        }
        return R.string.mc_lib_email_invalid_tip;
    }

    /* access modifiers changed from: private */
    public void showToastMsg(final int msg) {
        this.mHandler.post(new Runnable() {
            public void run() {
                Toast.makeText(MCLibGuideBasicActivity.this, msg, 1).show();
            }
        });
    }

    /* access modifiers changed from: private */
    public void showWaitingPrg() {
        this.mHandler.post(new Runnable() {
            public void run() {
                MCLibGuideBasicActivity.this.nextButton.setVisibility(4);
                MCLibGuideBasicActivity.this.progressBar.setVisibility(0);
            }
        });
    }

    /* access modifiers changed from: private */
    public void hideWaitingPrg() {
        this.mHandler.post(new Runnable() {
            public void run() {
                MCLibGuideBasicActivity.this.nextButton.setVisibility(0);
                MCLibGuideBasicActivity.this.progressBar.setVisibility(4);
            }
        });
    }

    public Handler getCurrentHandler() {
        return this.mHandler;
    }
}
