package com.mobcent.lib.android.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.view.View;
import com.mobcent.android.constants.MCLibConstants;
import com.mobcent.lib.android.constants.MCLibResourceConstant;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MCLibImageUtil {
    public static final int DEPENDS_ON_HEIGHT = 4;
    public static final int DEPENDS_ON_LONGEST_EDGE = 2;
    public static final int DEPENDS_ON_SHORTEST_EDGE = 1;
    public static final int DEPENDS_ON_WIDTH = 3;
    public static ExecutorService threadPool = Executors.newCachedThreadPool();

    public static void updateUserPhotoImage(final View view, String imageUrl, Context context, final int defaultResource, Handler handler) {
        HashMap<String, Integer> map = MCLibResourceConstant.getMCResourceConstant().getAllResourceMap();
        if (imageUrl == null || imageUrl.lastIndexOf("/") < 0) {
            handler.post(new Runnable() {
                public void run() {
                    view.setBackgroundResource(defaultResource);
                }
            });
            return;
        }
        String key = imageUrl.substring(imageUrl.lastIndexOf("/") + 1);
        if (map.containsKey(key)) {
            final int rid = map.get(key).intValue();
            handler.post(new Runnable() {
                public void run() {
                    view.setBackgroundResource(rid);
                }
            });
            return;
        }
        final Context context2 = context;
        final String str = imageUrl;
        final Handler handler2 = handler;
        final View view2 = view;
        final int i = defaultResource;
        threadPool.execute(new Thread() {
            public void run() {
                final Bitmap bitmap = MCLibImageLoader.getInstance(context2).loadBitmap(str, MCLibConstants.RESOLUTION_320X480, false);
                handler2.post(new Runnable() {
                    public void run() {
                        if (bitmap == null) {
                            view2.setBackgroundResource(i);
                        } else if (bitmap.isRecycled()) {
                            Bitmap bitmapNew = MCLibImageLoader.getInstance(context2).loadBitmap(str, MCLibConstants.RESOLUTION_320X480, false);
                            if (bitmapNew != null) {
                                view2.setBackgroundDrawable(new BitmapDrawable(bitmapNew));
                            } else {
                                view2.setBackgroundResource(i);
                            }
                        } else {
                            view2.setBackgroundDrawable(new BitmapDrawable(bitmap));
                        }
                    }
                });
            }
        });
    }

    public static void updateImage(final View view, String imageUrl, Context context, final int defaultResourceId, Handler handler) {
        if (imageUrl == null || "".equals(imageUrl)) {
            handler.post(new Runnable() {
                public void run() {
                    view.setBackgroundResource(defaultResourceId);
                }
            });
            return;
        }
        final HashMap<String, Integer> resourceMap = MCLibResourceConstant.getMCResourceConstant().getAllResourceMap();
        final String urlFileName = imageUrl.substring(imageUrl.lastIndexOf("/") + 1);
        if (resourceMap.containsKey(urlFileName)) {
            handler.post(new Runnable() {
                public void run() {
                    view.setBackgroundResource(((Integer) resourceMap.get(urlFileName)).intValue());
                }
            });
            return;
        }
        final Context context2 = context;
        final String str = imageUrl;
        final Handler handler2 = handler;
        final View view2 = view;
        final int i = defaultResourceId;
        threadPool.execute(new Thread() {
            public void run() {
                final Bitmap bitmap = MCLibImageLoader.getInstance(context2).loadBitmap(str, MCLibConstants.RESOLUTION_320X480, false);
                handler2.post(new Runnable() {
                    public void run() {
                        if (bitmap == null) {
                            view2.setBackgroundResource(i);
                        } else if (bitmap.isRecycled()) {
                            Bitmap bitmapNew = MCLibImageLoader.getInstance(context2).loadBitmap(str, MCLibConstants.RESOLUTION_320X480, false);
                            if (bitmapNew != null) {
                                view2.setBackgroundDrawable(new BitmapDrawable(bitmapNew));
                            } else {
                                view2.setBackgroundResource(i);
                            }
                        } else {
                            view2.setBackgroundDrawable(new BitmapDrawable(bitmap));
                        }
                    }
                });
            }
        });
    }

    public static boolean downloadGifImage(String url, String size, Context context) {
        return MCLibImageLoader.downloadGif(url, size, context);
    }

    public static String compressBitmap(String filePath, int upperLimitSize, int quality, String fileName, int policy, Context context) {
        if (filePath == null || "".equals(filePath)) {
            return null;
        }
        Bitmap resizedBitmap = resizeBitmap(BitmapFactory.decodeFile(filePath), upperLimitSize, policy);
        String resultFilePath = MCLibImageLoader.getImageCacheBasePath() + fileName;
        try {
            FileOutputStream fos = new FileOutputStream(new File(resultFilePath));
            if (resizedBitmap != null) {
                try {
                    if (!resizedBitmap.isRecycled()) {
                        resizedBitmap.compress(Bitmap.CompressFormat.JPEG, quality, fos);
                        if (resizedBitmap != null && !resizedBitmap.isRecycled()) {
                            resizedBitmap.recycle();
                        }
                        return resultFilePath;
                    }
                } catch (FileNotFoundException e) {
                } catch (Throwable th) {
                    if (resizedBitmap != null && !resizedBitmap.isRecycled()) {
                        resizedBitmap.recycle();
                    }
                    throw th;
                }
            }
            if (resizedBitmap != null && !resizedBitmap.isRecycled()) {
                resizedBitmap.recycle();
            }
            return null;
        } catch (FileNotFoundException e2) {
            if (resizedBitmap == null || resizedBitmap.isRecycled()) {
                return "";
            }
            resizedBitmap.recycle();
            return "";
        }
    }

    public static Bitmap resizeBitmap(Bitmap bitmap, int upperLimitSize, int policy) {
        if (policy == 4) {
            return resizeAccordHeightEdgeBitmap(bitmap, upperLimitSize);
        }
        if (policy == 3) {
            return resizeAccordWidthEdgeBitmap(bitmap, upperLimitSize);
        }
        if (policy == 1) {
            return resizeAccordShortestEdgeBitmap(bitmap, upperLimitSize);
        }
        if (policy == 2) {
            return resizeAccordLongestEdgeBitmap(bitmap, upperLimitSize);
        }
        return resizeAccordWidthEdgeBitmap(bitmap, upperLimitSize);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static Bitmap resizeAccordWidthEdgeBitmap(Bitmap bitmap, int upperLimitSize) {
        if (bitmap == null) {
            return null;
        }
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        if (width <= upperLimitSize && height <= upperLimitSize) {
            return bitmap;
        }
        Matrix matrix = new Matrix();
        matrix.postScale(((float) upperLimitSize) / ((float) width), ((float) ((int) (((float) upperLimitSize) * (((float) height) / ((float) width))))) / ((float) height));
        Bitmap resizedBitmap = Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
        bitmap.recycle();
        return resizedBitmap;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static Bitmap resizeAccordHeightEdgeBitmap(Bitmap bitmap, int upperLimitSize) {
        if (bitmap == null) {
            return null;
        }
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        if (width <= upperLimitSize && height <= upperLimitSize) {
            return bitmap;
        }
        Matrix matrix = new Matrix();
        matrix.postScale(((float) ((int) (((float) upperLimitSize) * (((float) width) / ((float) height))))) / ((float) width), ((float) upperLimitSize) / ((float) height));
        Bitmap resizedBitmap = Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
        bitmap.recycle();
        return resizedBitmap;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static Bitmap resizeAccordShortestEdgeBitmap(Bitmap bitmap, int upperLimitSize) {
        float scaleWidth;
        float scaleHeight;
        if (bitmap == null) {
            return null;
        }
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        if (width <= height) {
            scaleWidth = ((float) upperLimitSize) / ((float) width);
            scaleHeight = ((float) ((int) (((float) upperLimitSize) * (((float) height) / ((float) width))))) / ((float) height);
        } else {
            scaleWidth = ((float) ((int) (((float) upperLimitSize) * (((float) width) / ((float) height))))) / ((float) width);
            scaleHeight = ((float) upperLimitSize) / ((float) height);
        }
        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);
        Bitmap resizedBitmap = Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
        bitmap.recycle();
        return resizedBitmap;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static Bitmap resizeAccordLongestEdgeBitmap(Bitmap bitmap, int upperLimitSize) {
        float scaleWidth;
        float scaleHeight;
        if (bitmap == null) {
            return null;
        }
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        if (width <= upperLimitSize && height <= upperLimitSize) {
            return bitmap;
        }
        if (width >= height) {
            scaleWidth = ((float) upperLimitSize) / ((float) width);
            scaleHeight = ((float) ((int) (((float) upperLimitSize) * (((float) height) / ((float) width))))) / ((float) height);
        } else {
            scaleWidth = ((float) ((int) (((float) upperLimitSize) * (((float) width) / ((float) height))))) / ((float) width);
            scaleHeight = ((float) upperLimitSize) / ((float) height);
        }
        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);
        Bitmap resizedBitmap = Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
        bitmap.recycle();
        return resizedBitmap;
    }
}
