package com.mobcent.lib.android.ui.delegate.impl;

import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import com.mobcent.android.seed.autovGhnzo98NIzLsRPLHS.R;
import com.mobcent.lib.android.ui.activity.MCLibUIBaseActivity;
import com.mobcent.lib.android.ui.activity.receiver.MCLibMsgNotificationReceiver;
import com.mobcent.lib.android.ui.delegate.MCLibMsgNotificationIconDelegate;

public class MCLibMsgNotificationIconDelegateImpl implements MCLibMsgNotificationIconDelegate {
    /* access modifiers changed from: private */
    public MCLibUIBaseActivity activity;
    /* access modifiers changed from: private */
    public ImageView chatSessionNavImage;
    /* access modifiers changed from: private */
    public ImageView newMsgImage;
    /* access modifiers changed from: private */
    public TextView newMsgText;

    public MCLibMsgNotificationIconDelegateImpl(MCLibUIBaseActivity activity2) {
        this.activity = activity2;
    }

    public void updateCurrentIconMsg(final int msgCount, Handler mHandler) {
        mHandler.post(new Runnable() {
            public void run() {
                TextView unused = MCLibMsgNotificationIconDelegateImpl.this.newMsgText = MCLibMsgNotificationIconDelegateImpl.this.activity.msgNotificationText;
                ImageView unused2 = MCLibMsgNotificationIconDelegateImpl.this.newMsgImage = MCLibMsgNotificationIconDelegateImpl.this.activity.newMsgImage;
                if (MCLibMsgNotificationIconDelegateImpl.this.newMsgText != null && MCLibMsgNotificationIconDelegateImpl.this.newMsgImage != null) {
                    MCLibMsgNotificationIconDelegateImpl.this.newMsgImage.setBackgroundResource(R.drawable.mc_lib_new1_n);
                    MCLibMsgNotificationIconDelegateImpl.this.newMsgImage.setTag(MCLibMsgNotificationReceiver.CURRENT_TYPE_MSG);
                    MCLibMsgNotificationIconDelegateImpl.this.newMsgText.setText("(" + msgCount + ")");
                }
            }
        });
    }

    public void updateCurrentIconSysMsg(final int sysMsgCount, Handler mHandler) {
        mHandler.post(new Runnable() {
            public void run() {
                TextView unused = MCLibMsgNotificationIconDelegateImpl.this.newMsgText = MCLibMsgNotificationIconDelegateImpl.this.activity.msgNotificationText;
                ImageView unused2 = MCLibMsgNotificationIconDelegateImpl.this.newMsgImage = MCLibMsgNotificationIconDelegateImpl.this.activity.newMsgImage;
                if (MCLibMsgNotificationIconDelegateImpl.this.newMsgText != null && MCLibMsgNotificationIconDelegateImpl.this.newMsgImage != null) {
                    MCLibMsgNotificationIconDelegateImpl.this.newMsgImage.setBackgroundResource(R.drawable.mc_lib_new2_n);
                    MCLibMsgNotificationIconDelegateImpl.this.newMsgImage.setTag(MCLibMsgNotificationReceiver.CURRENT_TYPE_SYS_MSG);
                    MCLibMsgNotificationIconDelegateImpl.this.newMsgText.setText("(" + sysMsgCount + ")");
                }
            }
        });
    }

    public void updateCurrentIconHasReply(Handler mHandler) {
        mHandler.post(new Runnable() {
            public void run() {
                TextView unused = MCLibMsgNotificationIconDelegateImpl.this.newMsgText = MCLibMsgNotificationIconDelegateImpl.this.activity.msgNotificationText;
                ImageView unused2 = MCLibMsgNotificationIconDelegateImpl.this.newMsgImage = MCLibMsgNotificationIconDelegateImpl.this.activity.newMsgImage;
                if (MCLibMsgNotificationIconDelegateImpl.this.newMsgText != null && MCLibMsgNotificationIconDelegateImpl.this.newMsgImage != null) {
                    MCLibMsgNotificationIconDelegateImpl.this.newMsgImage.setBackgroundResource(R.drawable.mc_lib_new3_n);
                    MCLibMsgNotificationIconDelegateImpl.this.newMsgImage.setTag(MCLibMsgNotificationReceiver.CURRENT_TYPE_HAS_REPLY);
                    MCLibMsgNotificationIconDelegateImpl.this.newMsgText.setText("New");
                }
            }
        });
    }

    public void hideMsgNotificationBox(Handler mHandler) {
        mHandler.post(new Runnable() {
            public void run() {
                TextView unused = MCLibMsgNotificationIconDelegateImpl.this.newMsgText = MCLibMsgNotificationIconDelegateImpl.this.activity.msgNotificationText;
                ImageView unused2 = MCLibMsgNotificationIconDelegateImpl.this.newMsgImage = MCLibMsgNotificationIconDelegateImpl.this.activity.newMsgImage;
                ImageView unused3 = MCLibMsgNotificationIconDelegateImpl.this.chatSessionNavImage = MCLibMsgNotificationIconDelegateImpl.this.activity.chatSessionNavImage;
                if (MCLibMsgNotificationIconDelegateImpl.this.newMsgText != null && MCLibMsgNotificationIconDelegateImpl.this.newMsgImage != null && MCLibMsgNotificationIconDelegateImpl.this.chatSessionNavImage != null) {
                    MCLibMsgNotificationIconDelegateImpl.this.newMsgImage.setVisibility(4);
                    MCLibMsgNotificationIconDelegateImpl.this.newMsgText.setVisibility(4);
                    MCLibMsgNotificationIconDelegateImpl.this.chatSessionNavImage.setVisibility(0);
                }
            }
        });
    }

    public void showMsgNotificationBox(Handler mHandler) {
        mHandler.post(new Runnable() {
            public void run() {
                TextView unused = MCLibMsgNotificationIconDelegateImpl.this.newMsgText = MCLibMsgNotificationIconDelegateImpl.this.activity.msgNotificationText;
                ImageView unused2 = MCLibMsgNotificationIconDelegateImpl.this.newMsgImage = MCLibMsgNotificationIconDelegateImpl.this.activity.newMsgImage;
                ImageView unused3 = MCLibMsgNotificationIconDelegateImpl.this.chatSessionNavImage = MCLibMsgNotificationIconDelegateImpl.this.activity.chatSessionNavImage;
                if (MCLibMsgNotificationIconDelegateImpl.this.newMsgText != null && MCLibMsgNotificationIconDelegateImpl.this.newMsgImage != null && MCLibMsgNotificationIconDelegateImpl.this.chatSessionNavImage != null) {
                    MCLibMsgNotificationIconDelegateImpl.this.newMsgImage.setVisibility(0);
                    MCLibMsgNotificationIconDelegateImpl.this.newMsgText.setVisibility(0);
                    MCLibMsgNotificationIconDelegateImpl.this.chatSessionNavImage.setVisibility(4);
                }
            }
        });
    }

    public void headbeatNotificationBox(final Handler mHandler) {
        this.newMsgText = this.activity.msgNotificationText;
        this.newMsgImage = this.activity.newMsgImage;
        if (this.newMsgText != null && this.newMsgImage != null) {
            mHandler.post(new Runnable() {
                public void run() {
                    if (MCLibMsgNotificationIconDelegateImpl.this.newMsgImage.getTag().equals(MCLibMsgNotificationReceiver.CURRENT_TYPE_MSG)) {
                        MCLibMsgNotificationIconDelegateImpl.this.newMsgImage.setBackgroundResource(R.drawable.mc_lib_new1_h);
                    } else if (MCLibMsgNotificationIconDelegateImpl.this.newMsgImage.getTag().equals(MCLibMsgNotificationReceiver.CURRENT_TYPE_HAS_REPLY)) {
                        MCLibMsgNotificationIconDelegateImpl.this.newMsgImage.setBackgroundResource(R.drawable.mc_lib_new3_h);
                    } else if (MCLibMsgNotificationIconDelegateImpl.this.newMsgImage.getTag().equals(MCLibMsgNotificationReceiver.CURRENT_TYPE_SYS_MSG)) {
                        MCLibMsgNotificationIconDelegateImpl.this.newMsgImage.setBackgroundResource(R.drawable.mc_lib_new2_h);
                    }
                    Animation anim = AnimationUtils.loadAnimation(MCLibMsgNotificationIconDelegateImpl.this.activity, R.anim.mc_lib_heart_beat);
                    anim.setAnimationListener(new Animation.AnimationListener() {
                        public void onAnimationStart(Animation animation) {
                        }

                        public void onAnimationRepeat(Animation animation) {
                        }

                        public void onAnimationEnd(Animation animation) {
                            Animation anim = AnimationUtils.loadAnimation(MCLibMsgNotificationIconDelegateImpl.this.activity, R.anim.mc_lib_heart_beat);
                            anim.setAnimationListener(new Animation.AnimationListener() {
                                public void onAnimationStart(Animation animation) {
                                }

                                public void onAnimationRepeat(Animation animation) {
                                }

                                public void onAnimationEnd(Animation animation) {
                                    mHandler.postDelayed(new Runnable() {
                                        public void run() {
                                            if (MCLibMsgNotificationIconDelegateImpl.this.newMsgImage.getTag().equals(MCLibMsgNotificationReceiver.CURRENT_TYPE_MSG)) {
                                                MCLibMsgNotificationIconDelegateImpl.this.newMsgImage.setBackgroundResource(R.drawable.mc_lib_new1_n);
                                            } else if (MCLibMsgNotificationIconDelegateImpl.this.newMsgImage.getTag().equals(MCLibMsgNotificationReceiver.CURRENT_TYPE_HAS_REPLY)) {
                                                MCLibMsgNotificationIconDelegateImpl.this.newMsgImage.setBackgroundResource(R.drawable.mc_lib_new3_n);
                                            } else if (MCLibMsgNotificationIconDelegateImpl.this.newMsgImage.getTag().equals(MCLibMsgNotificationReceiver.CURRENT_TYPE_SYS_MSG)) {
                                                MCLibMsgNotificationIconDelegateImpl.this.newMsgImage.setBackgroundResource(R.drawable.mc_lib_new2_n);
                                            }
                                        }
                                    }, 100);
                                }
                            });
                            MCLibMsgNotificationIconDelegateImpl.this.newMsgImage.startAnimation(anim);
                        }
                    });
                    MCLibMsgNotificationIconDelegateImpl.this.newMsgImage.startAnimation(anim);
                }
            });
        }
    }
}
