package com.mobcent.lib.android.ui.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import com.mobcent.android.constants.MCLibParameterKeyConstant;
import com.mobcent.android.model.MCLibActionModel;
import com.mobcent.android.model.MCLibSysMsgModel;
import com.mobcent.android.model.MCLibUserInfo;
import com.mobcent.android.model.MCLibUserStatus;
import com.mobcent.android.os.service.MCLibHeartBeatOSService;
import com.mobcent.android.seed.autovGhnzo98NIzLsRPLHS.R;
import com.mobcent.android.service.MCLibRecentChatUserService;
import com.mobcent.android.service.MCLibUserPrivateMsgService;
import com.mobcent.android.service.impl.MCLibRecentChatUserServiceImpl;
import com.mobcent.android.service.impl.MCLibStatusServiceImpl;
import com.mobcent.android.service.impl.MCLibUserActionServiceImpl;
import com.mobcent.android.service.impl.MCLibUserInfoServiceImpl;
import com.mobcent.android.service.impl.MCLibUserPrivateMsgServiceImpl;
import com.mobcent.lib.android.constants.MCLibEmotionsConstant;
import com.mobcent.lib.android.ui.activity.adapter.MCLibChatRoomListAdapter;
import com.mobcent.lib.android.ui.activity.adapter.MCLibEmotionGridViewAdapter;
import com.mobcent.lib.android.ui.activity.adapter.MCLibMagicGridViewAdapter;
import com.mobcent.lib.android.ui.activity.adapter.MCLibQuickMsgListAdapter;
import com.mobcent.lib.android.ui.activity.receiver.MCLibNotifyChatRoomReceiveNewMsgReceiver;
import com.mobcent.lib.android.ui.activity.view.MCLibGifView;
import com.mobcent.lib.android.ui.activity.view.util.MCLibDisplayGifUtil;
import com.mobcent.lib.android.ui.delegate.MCLibNotifyReceiveNewMsgDelegate;
import com.mobcent.lib.android.ui.delegate.MCLibQuickMsgPanelDelegate;
import com.mobcent.lib.android.ui.delegate.MCLibUserActionDelegate;
import com.mobcent.lib.android.utils.MCLibPhoneUtil;
import com.mobcent.lib.android.utils.i18n.MCLibStringBundleUtil;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MCLibChatRoomActivity extends MCLibUIBaseActivity implements MCLibNotifyReceiveNewMsgDelegate, MCLibQuickMsgPanelDelegate {
    public static final int USER_MSG_BLOCK = 500;
    public static final int USER_MSG_PUB_FAIL = 501;
    private final String QUICK_RESP_CURRENT_TAB = "quickRespCurrentTab";
    private MCLibChatRoomListAdapter adapter;
    private Button backBtn;
    public Button cancelBtn;
    private ListView chatHistoryListView;
    /* access modifiers changed from: private */
    public int chatWithUid;
    public String chatWithUserNickName;
    public String chatWithUserPhoto;
    private Button clearChatHistoryBtn;
    public Button confirmBtn;
    /* access modifiers changed from: private */
    public GridView emotionGridView;
    /* access modifiers changed from: private */
    public TextView emotionTab;
    private TextView friendNameText;
    public RelativeLayout gifMaskLayer;
    public RelativeLayout gifPrgBarBox;
    /* access modifiers changed from: private */
    public MCLibUserInfo loginUser;
    /* access modifiers changed from: private */
    public int loginUserId;
    /* access modifiers changed from: private */
    public Handler mHandler = new Handler() {
        /* Debug info: failed to restart local var, previous not found, register: 8 */
        public void handleMessage(Message msg) {
            if (msg.what == 1) {
                MCLibChatRoomActivity.this.updateEmptyAdapter((List) msg.obj);
            } else if (msg.what == 2) {
                MCLibChatRoomActivity.this.updateExistAdapter((List) msg.obj);
            } else if (msg.what == 3) {
                MCLibChatRoomActivity.this.hideProgressBar();
            } else if (msg.what == 4) {
                MCLibChatRoomActivity.this.showProgressBar();
            } else if (msg.what == 500) {
                String blockMsg = MCLibStringBundleUtil.resolveString(R.string.mc_lib_msg_block, new String[]{MCLibChatRoomActivity.this.chatWithUserNickName, MCLibChatRoomActivity.this.chatWithUserNickName}, MCLibChatRoomActivity.this);
                MCLibSysMsgModel sysMsgModel = new MCLibSysMsgModel();
                sysMsgModel.setContent(blockMsg);
                sysMsgModel.setFromName("");
                sysMsgModel.setSysMsgType(2);
                MCLibHeartBeatOSService.sysMsgList.add(sysMsgModel);
                MCLibChatRoomActivity.this.showSysMsgBox();
            } else if (msg.what == 501) {
                Toast.makeText(MCLibChatRoomActivity.this, (int) R.string.mc_lib_pub_failed, 0);
            }
        }
    };
    /* access modifiers changed from: private */
    public TextView magicEmotionTab;
    /* access modifiers changed from: private */
    public GridView magicGridView;
    private LinearLayout magicTabBox;
    /* access modifiers changed from: private */
    public List<MCLibUserStatus> msgHistoryList;
    public EditText msgInputEditText;
    protected MCLibNotifyChatRoomReceiveNewMsgReceiver newMsgReceiver;
    public MCLibGifView previewGifView;
    /* access modifiers changed from: private */
    public TextView quickMsgTab;
    private LinearLayout quickMsgTabBox;
    private Button quickRespBackBtn;
    private Button quickRespBtn;
    /* access modifiers changed from: private */
    public ListView quickRespBundleListView;
    private LinearLayout quickRespInnerBox;
    public RelativeLayout quickRespLayer;
    private TableLayout quickRespTabBar;
    public MCLibGifView receivedGifView;
    public Button sendMsgBtn;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.mc_lib_chat_room_list);
        initWindowTitle();
        initSysMsgWidgets();
        this.chatWithUid = getIntent().getIntExtra("userId", 0);
        this.chatWithUserNickName = getIntent().getStringExtra("nickName");
        this.chatWithUserPhoto = getIntent().getStringExtra(MCLibParameterKeyConstant.USER_PHOTO);
        this.loginUser = new MCLibUserInfoServiceImpl(this).getLoginUser();
        this.loginUserId = this.loginUser.getUid();
        initWidgets();
    }

    private void initWidgets() {
        this.friendNameText = (TextView) findViewById(R.id.mcLibFriendNameText);
        this.backBtn = (Button) findViewById(R.id.mcLibBackBtn);
        this.clearChatHistoryBtn = (Button) findViewById(R.id.mcLibClearChatHistoryBtn);
        this.sendMsgBtn = (Button) findViewById(R.id.mcLibSendMsgBtn);
        this.quickRespBtn = (Button) findViewById(R.id.mcLibQuickRespBtn);
        this.quickRespBackBtn = (Button) findViewById(R.id.mcLibQuickRespBackBtn);
        this.quickRespLayer = (RelativeLayout) findViewById(R.id.mcLibQuickRespBox);
        this.quickRespInnerBox = (LinearLayout) findViewById(R.id.mcLibQuickRespInnerBox);
        this.quickRespTabBar = (TableLayout) findViewById(R.id.mcLibQuickRespTabBar);
        this.quickRespTabBar.setVisibility(0);
        this.msgInputEditText = (EditText) findViewById(R.id.mcLibMsgEditText);
        this.emotionGridView = (GridView) findViewById(R.id.mcLibEmotionGridView);
        this.magicGridView = (GridView) findViewById(R.id.mcLibMagicGridView);
        this.quickRespBundleListView = (ListView) findViewById(R.id.mcLibQuickRespListView);
        this.chatHistoryListView = (ListView) findViewById(R.id.mcLibMsgHistoryListView);
        this.confirmBtn = (Button) findViewById(R.id.mcLibConfirmBtn);
        this.cancelBtn = (Button) findViewById(R.id.mcLibCancelBtn);
        this.gifMaskLayer = (RelativeLayout) findViewById(R.id.mcLibGifMaskLayer);
        this.gifPrgBarBox = (RelativeLayout) findViewById(R.id.mcLibGifPrgBox);
        this.previewGifView = (MCLibGifView) findViewById(R.id.mcLibMagicGifView);
        this.receivedGifView = (MCLibGifView) findViewById(R.id.mcLibReceivedMagicGifView);
        this.previewGifView.windowWidth = MCLibPhoneUtil.getDisplayWidth(this);
        this.previewGifView.windowHeight = MCLibPhoneUtil.getDisplayHeight(this);
        this.receivedGifView.windowWidth = MCLibPhoneUtil.getDisplayWidth(this);
        this.receivedGifView.windowHeight = MCLibPhoneUtil.getDisplayHeight(this);
        initQuickResponsePanelTabs();
        this.chatHistoryListView.setDivider(null);
        this.friendNameText.setText(this.chatWithUserNickName);
        this.backBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCLibChatRoomActivity.this.finish();
            }
        });
        this.sendMsgBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String msg = MCLibChatRoomActivity.this.msgInputEditText.getText().toString();
                if (msg != null && !msg.equals("")) {
                    MCLibUserStatus userStatus = new MCLibUserStatus();
                    userStatus.setUid(MCLibChatRoomActivity.this.loginUserId);
                    userStatus.setUserImageUrl(MCLibChatRoomActivity.this.loginUser.getImage());
                    userStatus.setFromUserId(MCLibChatRoomActivity.this.loginUserId);
                    userStatus.setContent(msg);
                    userStatus.setToUid(MCLibChatRoomActivity.this.chatWithUid);
                    userStatus.setToUserName(MCLibChatRoomActivity.this.chatWithUserNickName);
                    userStatus.setTime(Calendar.getInstance().getTimeInMillis() + "");
                    userStatus.setSending(true);
                    MCLibChatRoomActivity.this.msgHistoryList.add(userStatus);
                    MCLibChatRoomActivity.this.mHandler.sendMessage(MCLibChatRoomActivity.this.mHandler.obtainMessage(2, MCLibChatRoomActivity.this.msgHistoryList));
                    MCLibChatRoomActivity.this.msgInputEditText.setText("");
                    MCLibChatRoomActivity.this.sendMsg(userStatus);
                }
            }
        });
        this.quickRespBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCLibChatRoomActivity.this.openQuickRespPanel();
            }
        });
        this.quickRespBackBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCLibChatRoomActivity.this.closeQuickRespPanel();
            }
        });
        this.quickRespLayer.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCLibChatRoomActivity.this.closeQuickRespPanel();
            }
        });
        this.quickRespInnerBox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            }
        });
        this.clearChatHistoryBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                showRemoveWarningDialog();
            }

            private void showRemoveWarningDialog() {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(MCLibChatRoomActivity.this);
                alertDialog.setTitle((int) R.string.mc_lib_tips);
                alertDialog.setMessage((int) R.string.mc_lib_clear_msg_confirm);
                alertDialog.setPositiveButton((int) R.string.mc_lib_confirm, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        if (new MCLibUserPrivateMsgServiceImpl(MCLibChatRoomActivity.this).removeAllMsg(MCLibChatRoomActivity.this.loginUserId, MCLibChatRoomActivity.this.chatWithUid)) {
                            MCLibChatRoomActivity.this.msgHistoryList.clear();
                            new MCLibRecentChatUserServiceImpl(MCLibChatRoomActivity.this).removeRecentChatUser(MCLibChatRoomActivity.this.chatWithUid);
                            MCLibChatRoomActivity.this.mHandler.sendMessage(MCLibChatRoomActivity.this.mHandler.obtainMessage(1, MCLibChatRoomActivity.this.msgHistoryList));
                        }
                    }
                });
                alertDialog.setNegativeButton((int) R.string.mc_lib_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                });
                alertDialog.show();
            }
        });
        this.cancelBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCLibChatRoomActivity.this.closeMagicPanel();
            }
        });
        this.confirmBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCLibChatRoomActivity.this.confirmBtn.setEnabled(false);
                MCLibActionModel model = (MCLibActionModel) MCLibChatRoomActivity.this.confirmBtn.getTag();
                if (model == null) {
                    MCLibChatRoomActivity.this.confirmBtn.setEnabled(true);
                    return;
                }
                MCLibUserStatus userStatus = new MCLibUserStatus();
                userStatus.setUid(MCLibChatRoomActivity.this.loginUserId);
                userStatus.setUserImageUrl(MCLibChatRoomActivity.this.loginUser.getImage());
                userStatus.setFromUserId(MCLibChatRoomActivity.this.loginUserId);
                userStatus.setContent(model.getShortName());
                userStatus.setToUid(MCLibChatRoomActivity.this.chatWithUid);
                userStatus.setToUserName(MCLibChatRoomActivity.this.chatWithUserNickName);
                userStatus.setTime(Calendar.getInstance().getTimeInMillis() + "");
                userStatus.setSending(true);
                userStatus.setActionId(model.getActionId());
                MCLibChatRoomActivity.this.msgHistoryList.add(userStatus);
                MCLibChatRoomActivity.this.mHandler.sendMessage(MCLibChatRoomActivity.this.mHandler.obtainMessage(2, MCLibChatRoomActivity.this.msgHistoryList));
                MCLibChatRoomActivity.this.msgInputEditText.setText("");
                MCLibChatRoomActivity.this.sendMagicAction(userStatus);
                MCLibChatRoomActivity.this.closeQuickRespPanel();
                MCLibChatRoomActivity.this.closeMagicPanel();
                MCLibChatRoomActivity.this.confirmBtn.setEnabled(true);
            }
        });
        this.gifMaskLayer.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            }
        });
        updateChatHistory();
    }

    private void initQuickResponsePanelTabs() {
        this.emotionTab = (TextView) findViewById(R.id.mcLibEmotionTab);
        this.quickMsgTab = (TextView) findViewById(R.id.mcLibQuickMsgTab);
        this.magicEmotionTab = (TextView) findViewById(R.id.mcLibMagicTab);
        this.quickMsgTabBox = (LinearLayout) findViewById(R.id.mcLibQuickMsgTabBox);
        this.magicTabBox = (LinearLayout) findViewById(R.id.mcLibMagicTabBox);
        if (new MCLibUserActionServiceImpl(this).isMagicEmotionEnable()) {
            TableRow.LayoutParams params = new TableRow.LayoutParams(-1, -2);
            this.emotionTab.setLayoutParams(params);
            this.quickMsgTabBox.setLayoutParams(params);
            this.magicTabBox.setVisibility(0);
        }
        this.emotionTab.setTag("quickRespCurrentTab");
        this.emotionTab.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (MCLibChatRoomActivity.this.emotionTab.getTag() == null) {
                    MCLibChatRoomActivity.this.quickMsgTab.setBackgroundResource(R.drawable.mc_lib_popup_box_botton1_n);
                    MCLibChatRoomActivity.this.magicEmotionTab.setBackgroundResource(R.drawable.mc_lib_popup_box_botton1_n);
                    MCLibChatRoomActivity.this.emotionTab.setTag("quickRespCurrentTab");
                    MCLibChatRoomActivity.this.quickMsgTab.setTag(null);
                    MCLibChatRoomActivity.this.magicEmotionTab.setTag(null);
                    MCLibChatRoomActivity.this.updateEmotionGridView();
                }
            }
        });
        this.emotionTab.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == 0) {
                    MCLibChatRoomActivity.this.emotionTab.setBackgroundResource(R.drawable.mc_lib_popup_box_botton1_h);
                    return false;
                } else if (1 == event.getAction()) {
                    MCLibChatRoomActivity.this.emotionTab.setBackgroundResource(R.drawable.mc_lib_popup_box_botton1_h);
                    return false;
                } else if (3 != event.getAction()) {
                    return false;
                } else {
                    if (MCLibChatRoomActivity.this.emotionTab.getTag() == null) {
                        MCLibChatRoomActivity.this.emotionTab.setBackgroundResource(R.drawable.mc_lib_popup_box_botton1_n);
                        return false;
                    }
                    MCLibChatRoomActivity.this.emotionTab.setBackgroundResource(R.drawable.mc_lib_popup_box_botton1_h);
                    return false;
                }
            }
        });
        this.quickMsgTab.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (MCLibChatRoomActivity.this.quickMsgTab.getTag() == null) {
                    MCLibChatRoomActivity.this.emotionTab.setBackgroundResource(R.drawable.mc_lib_popup_box_botton1_n);
                    MCLibChatRoomActivity.this.magicEmotionTab.setBackgroundResource(R.drawable.mc_lib_popup_box_botton1_n);
                    MCLibChatRoomActivity.this.quickMsgTab.setTag("quickRespCurrentTab");
                    MCLibChatRoomActivity.this.emotionTab.setTag(null);
                    MCLibChatRoomActivity.this.magicEmotionTab.setTag(null);
                    MCLibChatRoomActivity.this.updateQuickMsgListView();
                }
            }
        });
        this.quickMsgTab.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == 0) {
                    MCLibChatRoomActivity.this.quickMsgTab.setBackgroundResource(R.drawable.mc_lib_popup_box_botton1_h);
                    return false;
                } else if (1 == event.getAction()) {
                    MCLibChatRoomActivity.this.quickMsgTab.setBackgroundResource(R.drawable.mc_lib_popup_box_botton1_h);
                    return false;
                } else if (3 != event.getAction()) {
                    return false;
                } else {
                    if (MCLibChatRoomActivity.this.quickMsgTab.getTag() == null) {
                        MCLibChatRoomActivity.this.quickMsgTab.setBackgroundResource(R.drawable.mc_lib_popup_box_botton1_n);
                        return false;
                    }
                    MCLibChatRoomActivity.this.quickMsgTab.setBackgroundResource(R.drawable.mc_lib_popup_box_botton1_h);
                    return false;
                }
            }
        });
        this.magicEmotionTab.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (MCLibChatRoomActivity.this.magicEmotionTab.getTag() == null) {
                    MCLibChatRoomActivity.this.emotionTab.setBackgroundResource(R.drawable.mc_lib_popup_box_botton1_n);
                    MCLibChatRoomActivity.this.quickMsgTab.setBackgroundResource(R.drawable.mc_lib_popup_box_botton1_n);
                    MCLibChatRoomActivity.this.magicEmotionTab.setTag("quickRespCurrentTab");
                    MCLibChatRoomActivity.this.emotionTab.setTag(null);
                    MCLibChatRoomActivity.this.quickMsgTab.setTag(null);
                    MCLibChatRoomActivity.this.updateMagicEmotionGridView();
                }
            }
        });
        this.magicEmotionTab.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == 0) {
                    MCLibChatRoomActivity.this.magicEmotionTab.setBackgroundResource(R.drawable.mc_lib_popup_box_botton1_h);
                    return false;
                } else if (1 == event.getAction()) {
                    MCLibChatRoomActivity.this.magicEmotionTab.setBackgroundResource(R.drawable.mc_lib_popup_box_botton1_h);
                    return false;
                } else if (3 != event.getAction()) {
                    return false;
                } else {
                    if (MCLibChatRoomActivity.this.magicEmotionTab.getTag() == null) {
                        MCLibChatRoomActivity.this.magicEmotionTab.setBackgroundResource(R.drawable.mc_lib_popup_box_botton1_n);
                        return false;
                    }
                    MCLibChatRoomActivity.this.magicEmotionTab.setBackgroundResource(R.drawable.mc_lib_popup_box_botton1_h);
                    return false;
                }
            }
        });
    }

    private void updateChatHistory() {
        new Thread() {
            public void run() {
                MCLibUserPrivateMsgService upms = new MCLibUserPrivateMsgServiceImpl(MCLibChatRoomActivity.this);
                List<MCLibUserStatus> unreadMsgs = upms.getUserUnreadPrivateMsg(MCLibChatRoomActivity.this.loginUserId, MCLibChatRoomActivity.this.chatWithUid);
                MCLibChatRoomActivity.this.mHandler.sendMessage(MCLibChatRoomActivity.this.mHandler.obtainMessage(1, upms.getUserHistoryPrivateMsg(MCLibChatRoomActivity.this.loginUserId, MCLibChatRoomActivity.this.chatWithUid, 1, 99999999)));
                MCLibChatRoomActivity.this.showReceivedMagicAction(unreadMsgs);
            }
        }.start();
    }

    /* access modifiers changed from: private */
    public void updateEmptyAdapter(List<MCLibUserStatus> list) {
        this.adapter = new MCLibChatRoomListAdapter(this, this.chatHistoryListView, R.layout.mc_lib_chat_room_list_row, list, this.mHandler);
        this.chatHistoryListView.setAdapter((ListAdapter) this.adapter);
        this.msgHistoryList = list;
        if (this.msgHistoryList.size() > 1) {
            this.chatHistoryListView.setSelection(this.msgHistoryList.size() - 1);
        }
    }

    /* access modifiers changed from: private */
    public void updateExistAdapter(List<MCLibUserStatus> list) {
        this.adapter.setMsgHistoryList(list);
        this.adapter.notifyDataSetInvalidated();
        this.adapter.notifyDataSetChanged();
        this.msgHistoryList = list;
        if (this.msgHistoryList.size() > 1) {
            this.chatHistoryListView.setSelection(this.msgHistoryList.size() - 1);
        }
    }

    /* access modifiers changed from: private */
    public void sendMsg(final MCLibUserStatus userStatus) {
        new Thread() {
            public void run() {
                ((InputMethodManager) MCLibChatRoomActivity.this.getSystemService("input_method")).hideSoftInputFromWindow(MCLibChatRoomActivity.this.msgInputEditText.getWindowToken(), 0);
                String result = new MCLibStatusServiceImpl(MCLibChatRoomActivity.this).sendMsg(userStatus.getUid(), userStatus.getContent(), userStatus.getToUid(), userStatus.getToUserName(), userStatus.getActionId());
                if (result == null || !result.equals("rs_succ")) {
                    MCLibChatRoomActivity.this.msgHistoryList.remove(userStatus);
                    MCLibChatRoomActivity.this.mHandler.sendMessage(MCLibChatRoomActivity.this.mHandler.obtainMessage(2, MCLibChatRoomActivity.this.msgHistoryList));
                    if ("msgBlock".equals(result)) {
                        MCLibChatRoomActivity.this.mHandler.sendEmptyMessage(500);
                    } else {
                        MCLibChatRoomActivity.this.mHandler.sendEmptyMessage(MCLibChatRoomActivity.USER_MSG_PUB_FAIL);
                    }
                } else {
                    userStatus.setSending(false);
                    MCLibChatRoomActivity.this.mHandler.sendMessage(MCLibChatRoomActivity.this.mHandler.obtainMessage(2, MCLibChatRoomActivity.this.msgHistoryList));
                    MCLibRecentChatUserService recentChatUserService = new MCLibRecentChatUserServiceImpl(MCLibChatRoomActivity.this);
                    if (recentChatUserService.getRecentChatUser(MCLibChatRoomActivity.this.chatWithUid) == null) {
                        MCLibUserInfo userInfo = new MCLibUserInfo();
                        userInfo.setUid(MCLibChatRoomActivity.this.chatWithUid);
                        userInfo.setName(MCLibChatRoomActivity.this.chatWithUserNickName);
                        userInfo.setNickName(MCLibChatRoomActivity.this.chatWithUserNickName);
                        userInfo.setImage(MCLibChatRoomActivity.this.chatWithUserPhoto);
                        userInfo.setUnreadMsgCount(0);
                        userInfo.setLastMsgTime(Calendar.getInstance().getTimeInMillis());
                        recentChatUserService.addOrUpdateRecentChatUser(userInfo);
                    }
                }
            }
        }.start();
    }

    /* access modifiers changed from: private */
    public void sendMagicAction(final MCLibUserStatus userStatus) {
        new Thread() {
            public void run() {
                ((InputMethodManager) MCLibChatRoomActivity.this.getSystemService("input_method")).hideSoftInputFromWindow(MCLibChatRoomActivity.this.msgInputEditText.getWindowToken(), 0);
                String result = new MCLibUserPrivateMsgServiceImpl(MCLibChatRoomActivity.this).sendMagicAction(MCLibChatRoomActivity.this, MCLibChatRoomActivity.this.loginUserId, MCLibChatRoomActivity.this.chatWithUid, userStatus.getContent(), userStatus.getActionId(), MCLibChatRoomActivity.this.chatWithUserNickName);
                if (result == null || !result.equals("rs_succ")) {
                    MCLibChatRoomActivity.this.msgHistoryList.remove(userStatus);
                    MCLibChatRoomActivity.this.mHandler.sendMessage(MCLibChatRoomActivity.this.mHandler.obtainMessage(2, MCLibChatRoomActivity.this.msgHistoryList));
                    if ("msgBlock".equals(result)) {
                        MCLibChatRoomActivity.this.mHandler.sendEmptyMessage(500);
                    } else {
                        MCLibChatRoomActivity.this.mHandler.sendEmptyMessage(MCLibChatRoomActivity.USER_MSG_PUB_FAIL);
                    }
                } else {
                    userStatus.setSending(false);
                    MCLibChatRoomActivity.this.mHandler.sendMessage(MCLibChatRoomActivity.this.mHandler.obtainMessage(2, MCLibChatRoomActivity.this.msgHistoryList));
                    MCLibRecentChatUserService recentChatUserService = new MCLibRecentChatUserServiceImpl(MCLibChatRoomActivity.this);
                    if (recentChatUserService.getRecentChatUser(MCLibChatRoomActivity.this.chatWithUid) == null) {
                        MCLibUserInfo userInfo = new MCLibUserInfo();
                        userInfo.setUid(MCLibChatRoomActivity.this.chatWithUid);
                        userInfo.setName(MCLibChatRoomActivity.this.chatWithUserNickName);
                        userInfo.setNickName(MCLibChatRoomActivity.this.chatWithUserNickName);
                        userInfo.setImage(MCLibChatRoomActivity.this.chatWithUserPhoto);
                        userInfo.setUnreadMsgCount(0);
                        userInfo.setLastMsgTime(Calendar.getInstance().getTimeInMillis());
                        recentChatUserService.addOrUpdateRecentChatUser(userInfo);
                    }
                }
            }
        }.start();
    }

    public Handler getCurrentHandler() {
        return this.mHandler;
    }

    public void onReceiveNewMsg() {
        if (this.msgHistoryList == null) {
            this.msgHistoryList = new ArrayList();
        }
        List<MCLibUserStatus> list = new MCLibUserPrivateMsgServiceImpl(this).getUserUnreadPrivateMsg(this.loginUserId, this.chatWithUid);
        List<MCLibUserStatus> newList = new ArrayList<>();
        newList.addAll(this.msgHistoryList);
        newList.addAll(list);
        this.mHandler.sendMessage(this.mHandler.obtainMessage(2, newList));
        showReceivedMagicAction(list);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        initNewMsgReceiver();
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        if (this.newMsgReceiver != null) {
            unregisterReceiver(this.newMsgReceiver);
        }
    }

    private void initNewMsgReceiver() {
        if (this.newMsgReceiver == null) {
            this.newMsgReceiver = new MCLibNotifyChatRoomReceiveNewMsgReceiver(this);
        }
        IntentFilter filter = new IntentFilter();
        filter.addAction(getPackageName() + MCLibHeartBeatOSService.NOTIFY_CHAT_UPDATE + this.chatWithUid);
        registerReceiver(this.newMsgReceiver, filter);
    }

    public void closeQuickRespPanel() {
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.mc_lib_shrink_to_middle);
        animation.setDuration(400);
        animation.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
                MCLibChatRoomActivity.this.mHandler.post(new Runnable() {
                    public void run() {
                        MCLibChatRoomActivity.this.quickRespLayer.setVisibility(4);
                    }
                });
            }
        });
        this.quickRespLayer.startAnimation(animation);
    }

    public void closeMagicPanel() {
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.mc_lib_shrink_to_middle);
        animation.setDuration(400);
        animation.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
                MCLibChatRoomActivity.this.mHandler.post(new Runnable() {
                    public void run() {
                        MCLibChatRoomActivity.this.gifMaskLayer.setVisibility(4);
                        MCLibChatRoomActivity.this.previewGifView.setUserActionDelegate(null);
                    }
                });
            }
        });
        this.gifMaskLayer.startAnimation(animation);
    }

    /* access modifiers changed from: private */
    public void updateEmotionGridView() {
        this.mHandler.post(new Runnable() {
            public void run() {
                ArrayList<HashMap<String, Integer>> emotionList = new ArrayList<>();
                for (Map.Entry<String, Integer> entry : MCLibEmotionsConstant.geEmotionConstant().getAllEmotionsMap().entrySet()) {
                    HashMap<String, Integer> hashMap = new HashMap<>();
                    hashMap.put(entry.getKey(), entry.getValue());
                    emotionList.add(hashMap);
                }
                MCLibChatRoomActivity.this.quickRespBundleListView.setAdapter((ListAdapter) new MCLibQuickMsgListAdapter(MCLibChatRoomActivity.this, R.layout.mc_lib_quick_msg_list_row, new ArrayList(), MCLibChatRoomActivity.this));
                MCLibChatRoomActivity.this.magicGridView.setAdapter((ListAdapter) new MCLibMagicGridViewAdapter(MCLibChatRoomActivity.this, new ArrayList<>(), new ArrayList(), R.layout.mc_lib_magic_grid_item, new String[0], new int[0], MCLibChatRoomActivity.this.mHandler));
                MCLibChatRoomActivity.this.emotionGridView.setAdapter((ListAdapter) new MCLibEmotionGridViewAdapter(MCLibChatRoomActivity.this, emotionList, R.layout.mc_lib_emotion_grid_item, new String[0], new int[0], MCLibChatRoomActivity.this.msgInputEditText, MCLibChatRoomActivity.this, MCLibChatRoomActivity.this.mHandler));
            }
        });
    }

    /* access modifiers changed from: private */
    public void updateQuickMsgListView() {
        this.mHandler.post(new Runnable() {
            public void run() {
                ArrayList<HashMap<String, Integer>> emptyHashList = new ArrayList<>();
                MCLibChatRoomActivity.this.emotionGridView.setAdapter((ListAdapter) new MCLibEmotionGridViewAdapter(MCLibChatRoomActivity.this, emptyHashList, R.layout.mc_lib_emotion_grid_item, new String[0], new int[0], MCLibChatRoomActivity.this.msgInputEditText, MCLibChatRoomActivity.this, MCLibChatRoomActivity.this.mHandler));
                MCLibChatRoomActivity.this.magicGridView.setAdapter((ListAdapter) new MCLibMagicGridViewAdapter(MCLibChatRoomActivity.this, emptyHashList, new ArrayList(), R.layout.mc_lib_magic_grid_item, new String[0], new int[0], MCLibChatRoomActivity.this.mHandler));
                List<String> quickMsgList = new ArrayList<>();
                quickMsgList.add("");
                quickMsgList.addAll(new MCLibUserPrivateMsgServiceImpl(MCLibChatRoomActivity.this).getAllUserCustomizedQuickMsgs(MCLibChatRoomActivity.this.loginUserId));
                MCLibChatRoomActivity.this.quickRespBundleListView.setAdapter((ListAdapter) new MCLibQuickMsgListAdapter(MCLibChatRoomActivity.this, R.layout.mc_lib_quick_msg_list_row, quickMsgList, MCLibChatRoomActivity.this));
            }
        });
    }

    /* access modifiers changed from: private */
    public void updateMagicEmotionGridView() {
        this.mHandler.post(new Runnable() {
            public void run() {
                MCLibChatRoomActivity.this.quickRespBundleListView.setAdapter((ListAdapter) new MCLibQuickMsgListAdapter(MCLibChatRoomActivity.this, R.layout.mc_lib_quick_msg_list_row, new ArrayList(), MCLibChatRoomActivity.this));
                ArrayList<HashMap<String, Integer>> emptyHashList = new ArrayList<>();
                MCLibChatRoomActivity.this.emotionGridView.setAdapter((ListAdapter) new MCLibEmotionGridViewAdapter(MCLibChatRoomActivity.this, emptyHashList, R.layout.mc_lib_emotion_grid_item, new String[0], new int[0], MCLibChatRoomActivity.this.msgInputEditText, MCLibChatRoomActivity.this, MCLibChatRoomActivity.this.mHandler));
                ArrayList<HashMap<String, Integer>> arrayList = emptyHashList;
                MCLibChatRoomActivity.this.magicGridView.setAdapter((ListAdapter) new MCLibMagicGridViewAdapter(MCLibChatRoomActivity.this, arrayList, new MCLibUserActionServiceImpl(MCLibChatRoomActivity.this).getLocalMagicActions(MCLibChatRoomActivity.this, 0), R.layout.mc_lib_magic_grid_item, new String[0], new int[0], MCLibChatRoomActivity.this.mHandler));
            }
        });
    }

    /* access modifiers changed from: private */
    public void openQuickRespPanel() {
        this.mHandler.post(new Runnable() {
            public void run() {
                ((InputMethodManager) MCLibChatRoomActivity.this.getSystemService("input_method")).hideSoftInputFromWindow(MCLibChatRoomActivity.this.msgInputEditText.getWindowToken(), 0);
                MCLibChatRoomActivity.this.quickRespLayer.setVisibility(0);
                Animation animation = AnimationUtils.loadAnimation(MCLibChatRoomActivity.this, R.anim.mc_lib_grow_from_middle);
                animation.setDuration(200);
                MCLibChatRoomActivity.this.quickRespLayer.startAnimation(animation);
                MCLibChatRoomActivity.this.emotionTab.setBackgroundResource(R.drawable.mc_lib_popup_box_botton1_h);
                MCLibChatRoomActivity.this.quickMsgTab.setBackgroundResource(R.drawable.mc_lib_popup_box_botton1_n);
                MCLibChatRoomActivity.this.magicEmotionTab.setBackgroundResource(R.drawable.mc_lib_popup_box_botton1_n);
                MCLibChatRoomActivity.this.emotionTab.setTag("quickRespCurrentTab");
                MCLibChatRoomActivity.this.quickMsgTab.setTag(null);
                MCLibChatRoomActivity.this.magicEmotionTab.setTag(null);
                MCLibChatRoomActivity.this.updateEmotionGridView();
            }
        });
    }

    /* access modifiers changed from: private */
    public void showReceivedMagicAction(final List<MCLibUserStatus> list) {
        this.mHandler.post(new Runnable() {
            public void run() {
                new ShowNextMagicActionDelegateImpl(list, 0).doUserAction();
            }
        });
    }

    private class ShowNextMagicActionDelegateImpl implements MCLibUserActionDelegate {
        private List<MCLibUserStatus> list;
        private int pos;

        static /* synthetic */ int access$2008(ShowNextMagicActionDelegateImpl x0) {
            int i = x0.pos;
            x0.pos = i + 1;
            return i;
        }

        public ShowNextMagicActionDelegateImpl(List<MCLibUserStatus> list2, int pos2) {
            this.list = list2;
            this.pos = pos2;
        }

        public void doUserAction() {
            if (!this.list.isEmpty()) {
                if (this.pos >= this.list.size()) {
                    MCLibChatRoomActivity.this.receivedGifView.setUserActionDelegate(null);
                    MCLibChatRoomActivity.this.receivedGifView.setVisibility(4);
                    return;
                }
                final MCLibUserStatus userStatus = this.list.get(this.pos);
                if (userStatus.getActionId() <= 0) {
                    this.pos++;
                    doUserAction();
                    return;
                }
                MCLibChatRoomActivity.this.mHandler.postDelayed(new Runnable() {
                    public void run() {
                        new MCLibDisplayGifUtil(MCLibChatRoomActivity.this, MCLibChatRoomActivity.this.receivedGifView, null, true, MCLibChatRoomActivity.this.mHandler).showMobCentBouncePanel(new MCLibUserActionServiceImpl(MCLibChatRoomActivity.this).getActionInDictById(MCLibChatRoomActivity.this, userStatus.getActionId()), ShowNextMagicActionDelegateImpl.this);
                        ShowNextMagicActionDelegateImpl.access$2008(ShowNextMagicActionDelegateImpl.this);
                    }
                }, 500);
            }
        }
    }
}
