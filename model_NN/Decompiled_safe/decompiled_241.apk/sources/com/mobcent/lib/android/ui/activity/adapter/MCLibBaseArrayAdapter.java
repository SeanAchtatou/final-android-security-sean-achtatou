package com.mobcent.lib.android.ui.activity.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.mobcent.android.seed.autovGhnzo98NIzLsRPLHS.R;
import com.mobcent.lib.android.ui.delegate.MCLibUpdateListDelegate;
import java.util.List;

public class MCLibBaseArrayAdapter extends ArrayAdapter<Object> {
    protected MCLibUpdateListDelegate delegate;
    protected LayoutInflater inflater;

    public MCLibBaseArrayAdapter(Context context, int textViewResourceId, List objects) {
        super(context, textViewResourceId, objects);
    }

    public View getFetchMoreLoadingView() {
        return (RelativeLayout) this.inflater.inflate((int) R.layout.mc_lib_fetch_more_loading, (ViewGroup) null);
    }

    public View getFetchMoreView(int nextPageNum, boolean isReadLocally) {
        RelativeLayout layoutRow = (RelativeLayout) this.inflater.inflate((int) R.layout.mc_lib_fetch_more_loading, (ViewGroup) null);
        this.delegate.updateList(nextPageNum, 10, isReadLocally);
        return layoutRow;
    }

    public View getRefreshView(String lastUpdateTime, Context context) {
        RelativeLayout layoutRow = (RelativeLayout) this.inflater.inflate((int) R.layout.mc_lib_refresh_list, (ViewGroup) null);
        ((TextView) layoutRow.findViewById(R.id.mcLibLastUpdateTime)).setText(context.getResources().getString(R.string.mc_lib_last_update_time) + lastUpdateTime);
        layoutRow.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCLibBaseArrayAdapter.this.delegate.updateList(1, 10, false);
            }
        });
        return layoutRow;
    }
}
