package com.mobcent.lib.android.constants;

public interface MCLibUpdateUserPhotoTypes {
    public static final int SELECT_LOCAL_PHOTO = 3011;
    public static final int SELECT_PACKAGED_PHOTO = 3021;
    public static final int TAKE_PHOTO = 3001;
}
