package com.mobcent.lib.android.utils;

import android.content.Context;
import com.mobcent.android.seed.autovGhnzo98NIzLsRPLHS.R;
import com.mobcent.lib.android.utils.i18n.MCLibStringBundleUtil;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class MCLibDateUtil {
    public static Timestamp getTimestampByTimeString(String time) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            return Timestamp.valueOf(simpleDateFormat.format(simpleDateFormat.parse(time)));
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getGenericCurrentTime() {
        return new SimpleDateFormat("yyyy/MM/dd HH:mm").format(Calendar.getInstance().getTime());
    }

    public static String getFormatTime(long time) {
        return new SimpleDateFormat("yyyy/MM/dd HH:mm").format(Long.valueOf(time));
    }

    public static String getFormatDate(long time) {
        return new SimpleDateFormat("yyyy/MM/dd").format(Long.valueOf(time));
    }

    /* JADX INFO: Multiple debug info for r1v1 java.util.Date: [D('simpleDateFormat' java.text.SimpleDateFormat), D('date' java.util.Date)] */
    /* JADX INFO: Multiple debug info for r0v3 int: [D('currentTime' java.util.Date), D('hoursInterval' int)] */
    /* JADX INFO: Multiple debug info for r0v7 int: [D('currentTime' java.util.Date), D('minsInterval' int)] */
    public static String getTimeInterval(String time, Context context) {
        try {
            Date date = new SimpleDateFormat("yyyy/MM/dd HH:mm").parse(time);
            int year = date.getYear();
            int month = date.getMonth();
            int day = date.getDay();
            int hours = date.getHours();
            int mins = date.getMinutes();
            Date currentTime = new Date();
            if (year != currentTime.getYear() || month != currentTime.getMonth() || day != currentTime.getDay()) {
                return time;
            }
            if (hours == currentTime.getHours()) {
                int minsInterval = currentTime.getMinutes() - mins;
                if (minsInterval <= 0) {
                    return context.getResources().getString(R.string.mc_lib_just_now);
                }
                return MCLibStringBundleUtil.resolveString(R.string.mc_lib_mins_interval, new String[]{"" + minsInterval}, context);
            }
            return MCLibStringBundleUtil.resolveString(R.string.mc_lib_hours_interval, new String[]{"" + (currentTime.getHours() - hours)}, context);
        } catch (ParseException e) {
            return time;
        }
    }
}
