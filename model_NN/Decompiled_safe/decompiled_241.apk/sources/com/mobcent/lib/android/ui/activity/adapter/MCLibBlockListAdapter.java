package com.mobcent.lib.android.ui.activity.adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.mobcent.android.constants.MCLibParameterKeyConstant;
import com.mobcent.android.model.MCLibUserInfo;
import com.mobcent.android.seed.autovGhnzo98NIzLsRPLHS.R;
import com.mobcent.android.service.MCLibUserInfoService;
import com.mobcent.android.service.impl.MCLibUserInfoServiceImpl;
import com.mobcent.lib.android.ui.activity.MCLibChatRoomActivity;
import com.mobcent.lib.android.ui.activity.MCLibUserHomeActivity;
import com.mobcent.lib.android.ui.activity.adapter.holder.MCLibUserInfoHolder;
import com.mobcent.lib.android.ui.delegate.MCLibUpdateListDelegate;
import com.mobcent.lib.android.ui.dialog.MCLibAppInfoDialog;
import com.mobcent.lib.android.utils.MCLibAsyncImageLoader;
import com.mobcent.lib.android.utils.MCLibBitmapCallback;
import com.mobcent.lib.android.utils.i18n.MCLibStringBundleUtil;
import java.util.List;

public class MCLibBlockListAdapter extends MCLibBaseArrayAdapter {
    /* access modifiers changed from: private */
    public final Activity activity;
    public MCLibAsyncImageLoader asyncImageLoader;
    /* access modifiers changed from: private */
    public ListView listView;
    private int loginUserId;
    /* access modifiers changed from: private */
    public Handler mHandler;
    private int rowResourceId;
    private List<MCLibUserInfo> userInfoList;

    public MCLibBlockListAdapter(Activity activity2, ListView listView2, int rowResourceId2, List<MCLibUserInfo> userInfoList2, MCLibUpdateListDelegate delegate, Handler mHandler2) {
        super(activity2, rowResourceId2, userInfoList2);
        this.rowResourceId = rowResourceId2;
        this.inflater = LayoutInflater.from(activity2);
        this.activity = activity2;
        this.userInfoList = userInfoList2;
        this.delegate = delegate;
        this.mHandler = mHandler2;
        this.loginUserId = new MCLibUserInfoServiceImpl(activity2).getLoginUserId();
        this.listView = listView2;
        this.asyncImageLoader = new MCLibAsyncImageLoader(activity2);
    }

    public int getCount() {
        return this.userInfoList.size();
    }

    public Object getItem(int position) {
        return this.userInfoList.get(position);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        MCLibUserInfoHolder holder;
        String moodStr;
        final MCLibUserInfo userInfo = this.userInfoList.get(position);
        if (userInfo.isFetchMore()) {
            return getFetchMoreView(userInfo.getCurrentPage() + 1, userInfo.isReadFromLocal());
        }
        if (userInfo.isRefreshNeeded()) {
            return getRefreshView(userInfo.getLastUpdateTime(), this.activity);
        }
        if (convertView == null) {
            convertView = this.inflater.inflate(this.rowResourceId, (ViewGroup) null);
            holder = new MCLibUserInfoHolder();
            convertView.setTag(holder);
            initUserInfoHolder(convertView, holder);
        } else {
            holder = (MCLibUserInfoHolder) convertView.getTag();
        }
        if (holder == null) {
            convertView = this.inflater.inflate(this.rowResourceId, (ViewGroup) null);
            holder = new MCLibUserInfoHolder();
            convertView.setTag(holder);
            initUserInfoHolder(convertView, holder);
        }
        holder.getActionBtnBox().setVisibility(8);
        holder.getUserName().setText(userInfo.getName());
        holder.getUserGender().setText(userInfo.getGender());
        holder.getUserAge().setText(userInfo.getAge());
        holder.getUserCity().setText(userInfo.getPos());
        if (userInfo.getEmotionWords() == null || "".equals(userInfo.getEmotionWords())) {
            moodStr = this.activity.getResources().getString(R.string.mc_lib_mood_empty);
        } else {
            moodStr = userInfo.getEmotionWords();
        }
        holder.getUserEmotion().setText(MCLibStringBundleUtil.resolveString(R.string.mc_lib_mood, new String[]{moodStr}, this.activity));
        holder.getFansNumText().setText(MCLibStringBundleUtil.resolveString(R.string.mc_lib_fans_num, new String[]{userInfo.getFansNum() + ""}, this.activity));
        holder.getIsOnlineText().setText(userInfo.isOnline() ? R.string.mc_lib_user_online : R.string.mc_lib_user_offline);
        if (this.loginUserId == userInfo.getUid()) {
            holder.getIsFollowed().setText((int) R.string.mc_lib_myself);
        } else {
            holder.getIsFollowed().setText(userInfo.isFollowed() ? R.string.mc_lib_followed : R.string.mc_lib_not_follow);
            holder.getFollowTextView().setText(userInfo.isFollowed() ? R.string.mc_lib_cancel_follow : R.string.mc_lib_follow);
            if (userInfo.isFriend()) {
                holder.getIsFollowed().setText((int) R.string.mc_lib_is_friend);
            }
            if (userInfo.isFollowed()) {
                holder.getFollowBtn().setBackgroundResource(R.drawable.mc_lib_friends_popup_button3);
            } else {
                holder.getFollowBtn().setBackgroundResource(R.drawable.mc_lib_friends_popup_button2);
            }
        }
        holder.getUserImage().setTag(userInfo.getImage() + "");
        holder.getUserImage().setOnClickListener(new VisitUserHomeButtonOnClickListener(userInfo));
        updateImage(userInfo, holder);
        if (userInfo.getSourceProId() == 0) {
            holder.getSourceName().setVisibility(8);
        } else {
            holder.getSourceName().setVisibility(0);
            holder.getSourceName().setText(Html.fromHtml("<u>" + this.activity.getResources().getString(R.string.mc_lib_source) + ": " + userInfo.getSourceName() + "</u>"));
            holder.getSourceName().setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    new MCLibAppInfoDialog(MCLibBlockListAdapter.this.activity, R.style.mc_lib_dialog, userInfo.getSourceProId(), MCLibBlockListAdapter.this.mHandler).show();
                }
            });
        }
        convertView.setOnClickListener(new ConvertViewOnClickListener(holder));
        return convertView;
    }

    private void initUserInfoHolder(View convertView, MCLibUserInfoHolder holder) {
        holder.setUserName((TextView) convertView.findViewById(R.id.mcLibUserName));
        holder.setUserGender((TextView) convertView.findViewById(R.id.mcLibUserGender));
        holder.setUserAge((TextView) convertView.findViewById(R.id.mcLibUserAge));
        holder.setUserCity((TextView) convertView.findViewById(R.id.mcLibUserCity));
        holder.setUserImage((ImageView) convertView.findViewById(R.id.mcLibUserImage));
        holder.setFansNumText((TextView) convertView.findViewById(R.id.mcLibFansNumText));
        holder.setIsOnlineText((TextView) convertView.findViewById(R.id.mcLibIsOnlineText));
        holder.setUserEmotion((TextView) convertView.findViewById(R.id.mcLibUserMoodText));
        holder.setIsFollowed((TextView) convertView.findViewById(R.id.mcLibIsFollowed));
        holder.setActionBtnBox((RelativeLayout) convertView.findViewById(R.id.mcLibActionBtnBox));
        holder.setChatBtn((ImageView) convertView.findViewById(R.id.mcLibChatBtn));
        holder.setChatTextView((TextView) convertView.findViewById(R.id.mcLibChatBtnText));
        holder.setChatContainer((RelativeLayout) convertView.findViewById(R.id.mcLibChatContainer));
        holder.setFollowBtn((ImageView) convertView.findViewById(R.id.mcLibFollowBtn));
        holder.setFollowTextView((TextView) convertView.findViewById(R.id.mcLibFollowBtnText));
        holder.setFollowActionContainer((RelativeLayout) convertView.findViewById(R.id.mcLibFollowContainer));
        holder.setVisitUserHomeBtn((ImageView) convertView.findViewById(R.id.mcLibVisitUserHomeBtn));
        holder.setVisitUserHomeTextView((TextView) convertView.findViewById(R.id.mcLibVisitUserHomeBtnText));
        holder.setVisitUserHomeContainer((RelativeLayout) convertView.findViewById(R.id.mcLibVisitUserHomeContainer));
        holder.setSourceName((TextView) convertView.findViewById(R.id.mcLibSourceName));
        holder.setSourceNameContainer((RelativeLayout) convertView.findViewById(R.id.mcLibSourceNameContainer));
    }

    /* access modifiers changed from: private */
    public void showAnimActionBox(RelativeLayout actionBox) {
        Animation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setFillAfter(false);
        anim.setDuration(200);
        actionBox.startAnimation(anim);
    }

    public void hideAnimActionBox(final RelativeLayout actionBox) {
        this.mHandler.post(new Runnable() {
            public void run() {
                actionBox.setVisibility(8);
            }
        });
    }

    public List<MCLibUserInfo> getUserInfoList() {
        return this.userInfoList;
    }

    public void setUserInfoList(List<MCLibUserInfo> userInfoList2) {
        this.userInfoList = userInfoList2;
    }

    private void updateImage(MCLibUserInfo userInfo, MCLibUserInfoHolder holder) {
        Bitmap cachedImage = this.asyncImageLoader.loadBitmap(userInfo.getImage() + "", new MCLibBitmapCallback() {
            public void imageLoaded(Bitmap bitmap, String imageUrl) {
                ImageView imageViewByTag = (ImageView) MCLibBlockListAdapter.this.listView.findViewWithTag(imageUrl);
                if (imageViewByTag == null) {
                    return;
                }
                if (bitmap == null) {
                    imageViewByTag.setBackgroundResource(R.drawable.mc_lib_face_u0);
                } else if (bitmap.isRecycled()) {
                    imageViewByTag.setBackgroundResource(R.drawable.mc_lib_face_u0);
                } else {
                    imageViewByTag.setBackgroundDrawable(new BitmapDrawable(bitmap));
                }
            }
        });
        if (cachedImage == null) {
            holder.getUserImage().setBackgroundResource(R.drawable.mc_lib_face_u0);
        } else if (cachedImage.isRecycled()) {
            holder.getUserImage().setBackgroundResource(R.drawable.mc_lib_face_u0);
        } else {
            holder.getUserImage().setBackgroundDrawable(new BitmapDrawable(cachedImage));
        }
    }

    public class ChatButtonOnClickListener implements View.OnClickListener {
        private MCLibUserInfo userInfo;

        public ChatButtonOnClickListener(MCLibUserInfo userInfo2) {
            this.userInfo = userInfo2;
        }

        public void onClick(View v) {
            Intent intent = new Intent(MCLibBlockListAdapter.this.activity, MCLibChatRoomActivity.class);
            intent.putExtra("userId", this.userInfo.getUid());
            intent.putExtra("nickName", this.userInfo.getName());
            intent.putExtra(MCLibParameterKeyConstant.USER_PHOTO, this.userInfo.getImage());
            MCLibBlockListAdapter.this.activity.startActivity(intent);
        }
    }

    public class FollowUserOnClickListener implements View.OnClickListener {
        /* access modifiers changed from: private */
        public ImageView followBtn;
        /* access modifiers changed from: private */
        public TextView followText;
        /* access modifiers changed from: private */
        public MCLibUserInfo userInfo;

        public FollowUserOnClickListener(MCLibUserInfo userInfo2, ImageView followBtn2, TextView followText2) {
            this.userInfo = userInfo2;
            this.followBtn = followBtn2;
            this.followText = followText2;
        }

        public void onClick(View v) {
            new Thread() {
                public void run() {
                    String resultTemp;
                    MCLibBlockListAdapter.this.mHandler.post(new Runnable() {
                        public void run() {
                            Toast.makeText(MCLibBlockListAdapter.this.activity, (int) R.string.mc_lib_send_request, 0).show();
                        }
                    });
                    MCLibUserInfoService userInfoService = new MCLibUserInfoServiceImpl(MCLibBlockListAdapter.this.activity);
                    if (FollowUserOnClickListener.this.userInfo.isFollowed()) {
                        resultTemp = userInfoService.cancelFollowUser(userInfoService.getLoginUserId(), FollowUserOnClickListener.this.userInfo.getUid());
                    } else {
                        resultTemp = userInfoService.followUser(userInfoService.getLoginUserId(), FollowUserOnClickListener.this.userInfo.getUid());
                    }
                    final String result = resultTemp;
                    MCLibBlockListAdapter.this.mHandler.post(new Runnable() {
                        public void run() {
                            int tipWords;
                            if (result == null || !result.equals("rs_succ")) {
                                Toast.makeText(MCLibBlockListAdapter.this.activity, (int) R.string.mc_lib_connection_fail, 0).show();
                                return;
                            }
                            if (FollowUserOnClickListener.this.userInfo.isFollowed()) {
                                FollowUserOnClickListener.this.userInfo.setFollowed(false);
                                tipWords = R.string.mc_lib_disfollow_succ;
                                FollowUserOnClickListener.this.followBtn.setBackgroundResource(R.drawable.mc_lib_friends_popup_button2);
                                FollowUserOnClickListener.this.followText.setText((int) R.string.mc_lib_follow);
                            } else {
                                FollowUserOnClickListener.this.userInfo.setFollowed(true);
                                tipWords = R.string.mc_lib_follow_succ;
                                FollowUserOnClickListener.this.followBtn.setBackgroundResource(R.drawable.mc_lib_friends_popup_button3);
                                FollowUserOnClickListener.this.followText.setText((int) R.string.mc_lib_cancel_follow);
                            }
                            Toast.makeText(MCLibBlockListAdapter.this.activity, tipWords, 0).show();
                        }
                    });
                }
            }.start();
        }
    }

    public class VisitUserHomeButtonOnClickListener implements View.OnClickListener {
        private MCLibUserInfo userInfo;

        public VisitUserHomeButtonOnClickListener(MCLibUserInfo userInfo2) {
            this.userInfo = userInfo2;
        }

        public void onClick(View v) {
            Intent intent = new Intent(MCLibBlockListAdapter.this.activity, MCLibUserHomeActivity.class);
            intent.putExtra("userId", this.userInfo.getUid());
            MCLibBlockListAdapter.this.activity.startActivity(intent);
        }
    }

    public class ConvertViewOnClickListener implements View.OnClickListener {
        /* access modifiers changed from: private */
        public MCLibUserInfoHolder holder;

        public ConvertViewOnClickListener(MCLibUserInfoHolder holder2) {
            this.holder = holder2;
        }

        public void onClick(View v) {
            if (this.holder.getActionBtnBox() == null || this.holder.getActionBtnBox().getVisibility() != 0) {
                MCLibBlockListAdapter.this.mHandler.post(new Runnable() {
                    public void run() {
                        ConvertViewOnClickListener.this.holder.getActionBtnBox().setVisibility(0);
                        MCLibBlockListAdapter.this.showAnimActionBox(ConvertViewOnClickListener.this.holder.getActionBtnBox());
                    }
                });
            } else {
                MCLibBlockListAdapter.this.hideAnimActionBox(this.holder.getActionBtnBox());
            }
        }
    }
}
