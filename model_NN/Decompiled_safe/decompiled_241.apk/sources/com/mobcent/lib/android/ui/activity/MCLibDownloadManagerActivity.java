package com.mobcent.lib.android.ui.activity;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.mobcent.android.model.MCLibDownloadProfileModel;
import com.mobcent.android.os.service.MCLibDownloadMonitorService;
import com.mobcent.android.seed.autovGhnzo98NIzLsRPLHS.R;
import com.mobcent.android.service.MCLibAppDownloaderService;
import com.mobcent.android.service.impl.MCLibAppDownloaderServiceImpl;
import com.mobcent.lib.android.ui.activity.adapter.MCLibDownloadManagerListAdapter;
import com.mobcent.lib.android.ui.activity.receiver.MCLibUpdateDownloadStatusReceiver;
import java.util.ArrayList;
import java.util.List;

public class MCLibDownloadManagerActivity extends MCLibUIBaseActivity {
    /* access modifiers changed from: private */
    public MCLibDownloadManagerListAdapter adapter;
    private Button backBtn;
    /* access modifiers changed from: private */
    public List<MCLibDownloadProfileModel> downloadedModelList;
    /* access modifiers changed from: private */
    public List<MCLibDownloadProfileModel> downloadingModelList;
    private Handler mHandler = new Handler();
    /* access modifiers changed from: private */
    public ListView myDownloadListView;
    /* access modifiers changed from: private */
    public MCLibUpdateDownloadStatusReceiver receiver;
    /* access modifiers changed from: private */
    public List<MCLibDownloadProfileModel> waitingModelList;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.mc_lib_download_manager);
        initWindowTitle();
        initSysMsgWidgets();
        startService(new Intent(this, MCLibDownloadMonitorService.class));
        this.myDownloadListView = (ListView) findViewById(R.id.mcLibBundledListView);
        this.myDownloadListView.setDivider(null);
        this.backBtn = (Button) findViewById(R.id.mcLibBackBtn);
        this.backBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCLibDownloadManagerActivity.this.finish();
            }
        });
        initNavBottomBar(103);
        getAppDownloadList();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        initDownloadReceiver();
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        if (this.receiver != null) {
            unregisterReceiver(this.receiver);
        }
    }

    private void initDownloadReceiver() {
        if (this.receiver == null) {
            this.receiver = new MCLibUpdateDownloadStatusReceiver(this.adapter);
        }
        IntentFilter filter = new IntentFilter();
        filter.addAction(MCLibDownloadMonitorService.DOWNLOAD_SERVICE_MSG);
        registerReceiver(this.receiver, filter);
    }

    private void getAppDownloadList() {
        new Thread() {
            public void run() {
                try {
                    MCLibAppDownloaderService appDownloaderService = new MCLibAppDownloaderServiceImpl(MCLibDownloadManagerActivity.this);
                    List unused = MCLibDownloadManagerActivity.this.downloadingModelList = appDownloaderService.getDownloadingAppList();
                    List unused2 = MCLibDownloadManagerActivity.this.waitingModelList = appDownloaderService.getWaitingAppList();
                    List unused3 = MCLibDownloadManagerActivity.this.downloadedModelList = appDownloaderService.getDownloadedAppList();
                    MCLibDownloadManagerActivity.this.updateAppListView();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    /* access modifiers changed from: private */
    public void updateAppListView() {
        this.mHandler.post(new Runnable() {
            public void run() {
                if (MCLibDownloadManagerActivity.this.adapter == null) {
                    List<MCLibDownloadProfileModel> allModels = new ArrayList<>();
                    allModels.addAll(MCLibDownloadManagerActivity.this.downloadingModelList);
                    allModels.addAll(MCLibDownloadManagerActivity.this.waitingModelList);
                    allModels.addAll(MCLibDownloadManagerActivity.this.downloadedModelList);
                    MCLibDownloadManagerListAdapter unused = MCLibDownloadManagerActivity.this.adapter = new MCLibDownloadManagerListAdapter(MCLibDownloadManagerActivity.this, R.layout.mc_lib_download_manager_list_row, allModels);
                    MCLibDownloadManagerActivity.this.receiver.setAdapter(MCLibDownloadManagerActivity.this.adapter);
                    MCLibDownloadManagerActivity.this.myDownloadListView.setAdapter((ListAdapter) MCLibDownloadManagerActivity.this.adapter);
                }
            }
        });
    }

    public Handler getCurrentHandler() {
        return this.mHandler;
    }
}
