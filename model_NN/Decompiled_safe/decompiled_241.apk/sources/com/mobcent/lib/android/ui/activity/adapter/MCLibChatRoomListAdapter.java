package com.mobcent.lib.android.ui.activity.adapter;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.mobcent.android.model.MCLibUserStatus;
import com.mobcent.android.seed.autovGhnzo98NIzLsRPLHS.R;
import com.mobcent.android.service.impl.MCLibUserActionServiceImpl;
import com.mobcent.android.service.impl.MCLibUserInfoServiceImpl;
import com.mobcent.lib.android.constants.MCLibConstant;
import com.mobcent.lib.android.ui.activity.MCLibChatRoomActivity;
import com.mobcent.lib.android.ui.activity.MCLibUserHomeActivity;
import com.mobcent.lib.android.ui.activity.adapter.holder.MCLibChatRoomListAdapterHolder;
import com.mobcent.lib.android.ui.activity.view.util.MCLibDisplayGifUtil;
import com.mobcent.lib.android.utils.MCLibAsyncImageLoader;
import com.mobcent.lib.android.utils.MCLibBitmapCallback;
import com.mobcent.lib.android.utils.MCLibDateUtil;
import com.mobcent.lib.android.utils.MCLibTextViewUtil;
import com.mobcent.lib.android.utils.i18n.MCLibStringBundleUtil;
import java.util.Calendar;
import java.util.List;

public class MCLibChatRoomListAdapter extends MCLibBaseArrayAdapter {
    /* access modifiers changed from: private */
    public MCLibChatRoomActivity activity;
    public MCLibAsyncImageLoader asyncImageLoader;
    /* access modifiers changed from: private */
    public Bitmap friendImageBitmap;
    /* access modifiers changed from: private */
    public ListView listView;
    private int loginUserId;
    private Handler mHandler;
    private List<MCLibUserStatus> msgHistoryList;
    private int rowResourceId;
    /* access modifiers changed from: private */
    public Bitmap userImageBitmap;

    public MCLibChatRoomListAdapter(MCLibChatRoomActivity activity2, ListView listView2, int rowResourceId2, List<MCLibUserStatus> msgHistoryList2, Handler mHandler2) {
        super(activity2, rowResourceId2, msgHistoryList2);
        this.activity = activity2;
        this.rowResourceId = rowResourceId2;
        this.inflater = LayoutInflater.from(activity2);
        this.msgHistoryList = msgHistoryList2;
        this.listView = listView2;
        this.asyncImageLoader = new MCLibAsyncImageLoader(activity2);
        this.loginUserId = new MCLibUserInfoServiceImpl(activity2).getLoginUserId();
        this.mHandler = mHandler2;
    }

    public int getCount() {
        return this.msgHistoryList.size();
    }

    public Object getItem(int position) {
        return this.msgHistoryList.get(position);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        MCLibChatRoomListAdapterHolder holder;
        final MCLibUserStatus userStatus = this.msgHistoryList.get(position);
        if (convertView == null) {
            convertView = this.inflater.inflate(this.rowResourceId, (ViewGroup) null);
            holder = new MCLibChatRoomListAdapterHolder();
            convertView.setTag(holder);
            initChatRoomListAdapterHolder(convertView, holder);
        } else {
            holder = (MCLibChatRoomListAdapterHolder) convertView.getTag();
        }
        String date = MCLibDateUtil.getFormatDate(Calendar.getInstance().getTimeInMillis());
        if (userStatus.getFromUserId() == this.loginUserId) {
            holder.getUserMsgBox().setVisibility(0);
            holder.getFriendMsgBox().setVisibility(8);
            holder.getUserMsgContent().setText(MCLibTextViewUtil.transformCharSequenceWithEmotions(userStatus.getContent(), getContext()));
            holder.getUserPubTimeTextView().setText(MCLibDateUtil.getFormatTime(new Long(userStatus.getTime()).longValue()).replace(date, ""));
            holder.getUserPhotoImageView().setTag(userStatus.getUserImageUrl() + "" + userStatus.getStatusId());
            updateImage(userStatus, holder, false);
            if (userStatus.isSending()) {
                holder.getSendingMsgPrgBar().setVisibility(0);
            } else {
                holder.getSendingMsgPrgBar().setVisibility(8);
            }
        } else {
            holder.getUserMsgBox().setVisibility(8);
            holder.getFriendMsgBox().setVisibility(0);
            if (userStatus.getContent() == null || !userStatus.getContent().trim().equals(MCLibConstant.FOLLOW_MSG_KEY)) {
                holder.getFriendMsgContent().setText(MCLibTextViewUtil.transformCharSequenceWithEmotions(userStatus.getContent(), getContext()));
            } else {
                holder.getFriendMsgContent().setText(MCLibStringBundleUtil.resolveString(R.string.mc_lib_follow_notice, new String[]{this.activity.chatWithUserNickName}, getContext()));
            }
            holder.getFriendPubTimeTextView().setText(MCLibDateUtil.getFormatTime(new Long(userStatus.getTime()).longValue()).replace(date + " ", ""));
            holder.getFriendPhotoImageView().setTag(userStatus.getUserImageUrl() + "" + userStatus.getStatusId());
            updateImage(userStatus, holder, true);
            holder.getFriendPhotoImageView().setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Intent intent = new Intent(MCLibChatRoomListAdapter.this.getContext(), MCLibUserHomeActivity.class);
                    intent.putExtra("userId", userStatus.getFromUserId());
                    MCLibChatRoomListAdapter.this.getContext().startActivity(intent);
                }
            });
        }
        convertView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (userStatus.getActionId() > 0) {
                    MCLibChatRoomListAdapter.this.activity.getCurrentHandler().post(new Runnable() {
                        public void run() {
                            new MCLibDisplayGifUtil(MCLibChatRoomListAdapter.this.activity, MCLibChatRoomListAdapter.this.activity.receivedGifView, null, true, MCLibChatRoomListAdapter.this.activity.getCurrentHandler()).showMobCentBouncePanel(new MCLibUserActionServiceImpl(MCLibChatRoomListAdapter.this.activity).getActionInDictById(MCLibChatRoomListAdapter.this.activity, userStatus.getActionId()), null);
                        }
                    });
                }
            }
        });
        return convertView;
    }

    private void initChatRoomListAdapterHolder(View convertView, MCLibChatRoomListAdapterHolder holder) {
        holder.setFriendMsgContent((TextView) convertView.findViewById(R.id.mcLibFriendMsgText));
        holder.setFriendPubTimeTextView((TextView) convertView.findViewById(R.id.mcLibFriendMsgTimeText));
        holder.setFriendPhotoImageView((ImageView) convertView.findViewById(R.id.mcLibFriendImage));
        holder.setFriendMsgBox((LinearLayout) convertView.findViewById(R.id.mcLibFriendBox));
        holder.setUserMsgContent((TextView) convertView.findViewById(R.id.mcLibUserMsgText));
        holder.setUserPubTimeTextView((TextView) convertView.findViewById(R.id.mcLibUserMsgTimeText));
        holder.setUserPhotoImageView((ImageView) convertView.findViewById(R.id.mcLibUserImage));
        holder.setUserMsgBox((RelativeLayout) convertView.findViewById(R.id.mcLibUserBox));
        holder.setSendingMsgPrgBar((ProgressBar) convertView.findViewById(R.id.mcLibMsgSendingProgressBar));
    }

    private void updateImage(final MCLibUserStatus userStatus, final MCLibChatRoomListAdapterHolder holder, final boolean isFriendImage) {
        if (isFriendImage) {
            if (this.friendImageBitmap != null && !this.friendImageBitmap.isRecycled()) {
                this.mHandler.post(new Runnable() {
                    public void run() {
                        holder.getFriendPhotoImageView().setBackgroundDrawable(new BitmapDrawable(MCLibChatRoomListAdapter.this.friendImageBitmap));
                    }
                });
                return;
            }
        } else if (this.userImageBitmap != null && !this.userImageBitmap.isRecycled()) {
            this.mHandler.post(new Runnable() {
                public void run() {
                    holder.getUserPhotoImageView().setBackgroundDrawable(new BitmapDrawable(MCLibChatRoomListAdapter.this.userImageBitmap));
                }
            });
            return;
        }
        Bitmap cachedImage = this.asyncImageLoader.loadBitmap(userStatus.getUserImageUrl() + "", new MCLibBitmapCallback() {
            public void imageLoaded(Bitmap bitmap, String imageUrl) {
                ImageView imageViewByTag = (ImageView) MCLibChatRoomListAdapter.this.listView.findViewWithTag(imageUrl + "" + userStatus.getStatusId());
                if (imageViewByTag == null) {
                    return;
                }
                if (bitmap == null) {
                    imageViewByTag.setBackgroundResource(R.drawable.mc_lib_face_u0);
                } else if (bitmap.isRecycled()) {
                    imageViewByTag.setBackgroundResource(R.drawable.mc_lib_face_u0);
                } else {
                    if (isFriendImage) {
                        Bitmap unused = MCLibChatRoomListAdapter.this.friendImageBitmap = bitmap;
                    } else {
                        Bitmap unused2 = MCLibChatRoomListAdapter.this.userImageBitmap = bitmap;
                    }
                    imageViewByTag.setBackgroundDrawable(new BitmapDrawable(bitmap));
                }
            }
        });
        if (cachedImage == null) {
            if (isFriendImage) {
                holder.getFriendPhotoImageView().setBackgroundResource(R.drawable.mc_lib_face_u0);
            } else {
                holder.getUserPhotoImageView().setBackgroundResource(R.drawable.mc_lib_face_u0);
            }
        } else if (cachedImage.isRecycled()) {
            if (isFriendImage) {
                holder.getFriendPhotoImageView().setBackgroundResource(R.drawable.mc_lib_face_u0);
            } else {
                holder.getUserPhotoImageView().setBackgroundResource(R.drawable.mc_lib_face_u0);
            }
        } else if (isFriendImage) {
            this.friendImageBitmap = cachedImage;
            holder.getFriendPhotoImageView().setBackgroundDrawable(new BitmapDrawable(cachedImage));
        } else {
            this.userImageBitmap = cachedImage;
            holder.getUserPhotoImageView().setBackgroundDrawable(new BitmapDrawable(cachedImage));
        }
    }

    public List<MCLibUserStatus> getMsgHistoryList() {
        return this.msgHistoryList;
    }

    public void setMsgHistoryList(List<MCLibUserStatus> msgHistoryList2) {
        this.msgHistoryList = msgHistoryList2;
    }
}
