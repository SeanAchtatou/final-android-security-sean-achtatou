package com.mobcent.lib.android.ui.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.mobcent.android.model.MCLibAppStoreAppModel;
import com.mobcent.android.model.MCLibDownloadProfileModel;
import com.mobcent.android.seed.autovGhnzo98NIzLsRPLHS.R;
import com.mobcent.android.service.impl.MCLibAppStoreServiceImpl;
import com.mobcent.android.service.impl.MCLibDownloadManagerServiceImpl;
import com.mobcent.android.service.impl.MCLibUserInfoServiceImpl;
import com.mobcent.lib.android.ui.activity.MCLibDownloadManagerActivity;
import com.mobcent.lib.android.utils.MCLibImageUtil;

public class MCLibAppInfoDialog extends Dialog {
    /* access modifiers changed from: private */
    public Activity activity;
    /* access modifiers changed from: private */
    public TextView appDesc;
    /* access modifiers changed from: private */
    public TextView appDeveloper;
    /* access modifiers changed from: private */
    public TextView appDownloadTimes;
    /* access modifiers changed from: private */
    public TextView appFee;
    /* access modifiers changed from: private */
    public int appId;
    /* access modifiers changed from: private */
    public ImageView appImage;
    /* access modifiers changed from: private */
    public LinearLayout appInfoLayout;
    /* access modifiers changed from: private */
    public TextView appName;
    /* access modifiers changed from: private */
    public TextView appSize;
    /* access modifiers changed from: private */
    public TextView appUpdateTime;
    /* access modifiers changed from: private */
    public TextView appVersion;
    /* access modifiers changed from: private */
    public Button cancelBtn;
    /* access modifiers changed from: private */
    public Button downloadNowBtn;
    /* access modifiers changed from: private */
    public Handler mHandler;
    /* access modifiers changed from: private */
    public RelativeLayout plsWaitLayout;

    public MCLibAppInfoDialog(Activity activity2, int theme, int appId2, Handler mHandler2) {
        super(activity2, theme);
        this.activity = activity2;
        this.appId = appId2;
        this.mHandler = mHandler2;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (this.appId == -1) {
            setContentView((int) R.layout.mc_lib_dialog_app_info_under_review);
            return;
        }
        setContentView((int) R.layout.mc_lib_dialog_app_info);
        initAppInfoDialog();
        getAppInfo();
    }

    private void initAppInfoDialog() {
        this.plsWaitLayout = (RelativeLayout) findViewById(R.id.mcLibPlsWaitBox);
        this.appInfoLayout = (LinearLayout) findViewById(R.id.mcLibAppInfoBox);
        this.appName = (TextView) findViewById(R.id.mcLibAppName);
        this.appFee = (TextView) findViewById(R.id.mcLibAppFee);
        this.appDownloadTimes = (TextView) findViewById(R.id.mcLibAppDownloadTimes);
        this.appSize = (TextView) findViewById(R.id.mcLibAppSize);
        this.appVersion = (TextView) findViewById(R.id.mcLibAppVersion);
        this.appUpdateTime = (TextView) findViewById(R.id.mcLibAppUpdateTime);
        this.appDeveloper = (TextView) findViewById(R.id.mcLibAppDeveloper);
        this.appDesc = (TextView) findViewById(R.id.mcLibAppDesc);
        this.appImage = (ImageView) findViewById(R.id.mcLibAppImage);
        this.downloadNowBtn = (Button) findViewById(R.id.mcLibAppDownloadButton);
        this.cancelBtn = (Button) findViewById(R.id.mcLibAppCancelButton);
    }

    private void getAppInfo() {
        new Thread() {
            public void run() {
                MCLibAppStoreAppModel model = new MCLibAppStoreServiceImpl(MCLibAppInfoDialog.this.activity).getAppInfo(new MCLibUserInfoServiceImpl(MCLibAppInfoDialog.this.activity).getLoginUserId(), MCLibAppInfoDialog.this.appId);
                if (model != null) {
                    MCLibAppInfoDialog.this.updateAppInfo(model);
                }
            }
        }.start();
    }

    /* access modifiers changed from: private */
    public void updateAppInfo(final MCLibAppStoreAppModel appModel) {
        this.mHandler.post(new Runnable() {
            public void run() {
                MCLibAppInfoDialog.this.plsWaitLayout.setVisibility(8);
                MCLibAppInfoDialog.this.appInfoLayout.setVisibility(0);
                MCLibAppInfoDialog.this.appName.setText(appModel.getAppName());
                MCLibAppInfoDialog.this.appFee.setText(MCLibAppInfoDialog.this.activity.getResources().getString(R.string.mc_lib_app_fee) + (appModel.getFee().equals("0") ? MCLibAppInfoDialog.this.activity.getResources().getString(R.string.mc_lib_app_free) : MCLibAppInfoDialog.this.activity.getResources().getString(R.string.mc_lib_app_charge)));
                MCLibAppInfoDialog.this.appDownloadTimes.setText(MCLibAppInfoDialog.this.activity.getResources().getString(R.string.mc_lib_app_download_times) + appModel.getDownloadTimes());
                MCLibAppInfoDialog.this.appSize.setText(MCLibAppInfoDialog.this.activity.getResources().getString(R.string.mc_lib_app_size) + appModel.getSize());
                MCLibAppInfoDialog.this.appVersion.setText(MCLibAppInfoDialog.this.activity.getResources().getString(R.string.mc_lib_app_version) + appModel.getVersion());
                MCLibAppInfoDialog.this.appUpdateTime.setText(MCLibAppInfoDialog.this.activity.getResources().getString(R.string.mc_lib_app_update_time) + appModel.getPublishTime());
                MCLibAppInfoDialog.this.appDeveloper.setText(MCLibAppInfoDialog.this.activity.getResources().getString(R.string.mc_lib_app_developer_name) + appModel.getDeveloperName());
                MCLibAppInfoDialog.this.appDesc.setText(MCLibAppInfoDialog.this.activity.getResources().getString(R.string.mc_lib_app_desc) + appModel.getDescription());
                MCLibImageUtil.updateImage(MCLibAppInfoDialog.this.appImage, appModel.getAppImageUrl(), MCLibAppInfoDialog.this.activity, R.drawable.mc_lib_app_img, MCLibAppInfoDialog.this.mHandler);
                if (appModel.getDownloadPath() == null || appModel.getDownloadPath().equals("") || appModel.getDownloadPath().equals(appModel.getBaseUrl())) {
                    MCLibAppInfoDialog.this.downloadNowBtn.setEnabled(false);
                }
                MCLibAppInfoDialog.this.downloadNowBtn.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        MCLibDownloadProfileModel model = new MCLibDownloadProfileModel();
                        model.setAppId(appModel.getAppId());
                        model.setAppName(appModel.getAppName());
                        model.setDownloadSize(0);
                        model.setFileSize(0);
                        model.setUrl(appModel.getDownloadPath());
                        model.setStatus(0);
                        model.setAppType(appModel.getCategoryId());
                        new MCLibDownloadManagerServiceImpl(MCLibAppInfoDialog.this.activity).addOrUpdateDownloadProfile(model, false);
                        MCLibAppInfoDialog.this.activity.startActivity(new Intent(MCLibAppInfoDialog.this.activity, MCLibDownloadManagerActivity.class));
                        MCLibAppInfoDialog.this.dismiss();
                    }
                });
                MCLibAppInfoDialog.this.cancelBtn.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        MCLibAppInfoDialog.this.dismiss();
                    }
                });
            }
        });
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (this.appId == -1) {
            dismiss();
        }
        return super.onTouchEvent(event);
    }
}
