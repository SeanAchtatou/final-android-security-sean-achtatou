package com.mobcent.lib.android.ui.dialog.adapter;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import com.mobcent.android.seed.autovGhnzo98NIzLsRPLHS.R;
import com.mobcent.lib.android.ui.activity.adapter.MCLibBaseSimpleAdapter;
import com.mobcent.lib.android.ui.delegate.impl.MCLibUpdateUserPhotoDelegateImpl;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class MCLibPhotoGridViewAdapter extends MCLibBaseSimpleAdapter {
    /* access modifiers changed from: private */
    public Context context;
    private List<HashMap<String, Integer>> data;
    /* access modifiers changed from: private */
    public MCLibUpdateUserPhotoDelegateImpl delegate;
    /* access modifiers changed from: private */
    public Dialog dialog;
    private LayoutInflater inflater;
    private int resource;
    private ImageView userPhotoImage;

    public MCLibPhotoGridViewAdapter(Context context2, List<HashMap<String, Integer>> data2, int resource2, String[] from, int[] to, MCLibUpdateUserPhotoDelegateImpl delegate2, Dialog dialog2) {
        super(context2, data2, resource2, from, to);
        this.data = data2;
        this.resource = resource2;
        this.inflater = LayoutInflater.from(context2);
        this.context = context2;
        this.delegate = delegate2;
        this.dialog = dialog2;
    }

    public int getCount() {
        return this.data.size();
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        HashMap<String, Integer> currentData = this.data.get(position);
        final String imageUrl = getCurrentKey(currentData);
        View convertView2 = this.inflater.inflate(this.resource, (ViewGroup) null);
        this.userPhotoImage = (ImageView) convertView2.findViewById(R.id.mcLibUserPhotoImage);
        this.userPhotoImage.setBackgroundResource(((Integer) currentData.get(imageUrl)).intValue());
        convertView2.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                ImageView userPhoto = (ImageView) v.findViewById(R.id.mcLibUserPhotoImage);
                if (event.getAction() == 1) {
                    Animation anim = AnimationUtils.loadAnimation(MCLibPhotoGridViewAdapter.this.context, R.anim.mc_lib_grow_to_middle);
                    anim.setInterpolator(new AccelerateInterpolator());
                    userPhoto.startAnimation(anim);
                    anim.setAnimationListener(new Animation.AnimationListener() {
                        public void onAnimationStart(Animation animation) {
                        }

                        public void onAnimationRepeat(Animation animation) {
                        }

                        public void onAnimationEnd(Animation animation) {
                            MCLibPhotoGridViewAdapter.this.delegate.setIconUrl(imageUrl);
                            MCLibPhotoGridViewAdapter.this.delegate.setType(1);
                            MCLibPhotoGridViewAdapter.this.delegate.doUserAction();
                            MCLibPhotoGridViewAdapter.this.dialog.dismiss();
                        }
                    });
                } else if (event.getAction() == 3) {
                }
                return true;
            }
        });
        return convertView2;
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    private String getCurrentKey(HashMap<String, Integer> map) {
        Iterator<Map.Entry<String, Integer>> it = map.entrySet().iterator();
        if (it.hasNext()) {
            return (String) it.next().getKey();
        }
        return "";
    }
}
