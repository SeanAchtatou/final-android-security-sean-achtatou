package com.mobcent.lib.android.ui.activity;

import android.os.Handler;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.mobcent.android.model.MCLibSysMsgModel;
import com.mobcent.android.os.service.MCLibHeartBeatOSService;
import com.mobcent.android.seed.autovGhnzo98NIzLsRPLHS.R;
import com.mobcent.lib.android.ui.delegate.impl.MCLibSendFeedbackDelegateImpl;
import com.mobcent.lib.android.ui.dialog.MCLibAppInfoDialog;
import com.mobcent.lib.android.ui.dialog.MCLibPublishWordsDialog;
import java.util.ArrayList;
import java.util.List;

public abstract class MCLibSysBaseActivity extends MCLibAbstractBaseActivity {
    protected ImageButton closeSysMsgBoxBtn;
    protected ImageButton nextSysMsgBtn;
    protected ImageButton previousSysMsgBtn;
    protected RelativeLayout sysMsgBox;
    protected TextView sysMsgContent;
    protected TextView sysMsgCountText;
    protected Button sysMsgFunctionBtn;
    protected List<MCLibSysMsgModel> sysMsgList;
    protected RelativeLayout sysMsgMaskLayer;
    /* access modifiers changed from: private */
    public int sysMsgPos = 0;
    protected TextView sysMsgTitle;

    public abstract Handler getCurrentHandler();

    static /* synthetic */ int access$008(MCLibSysBaseActivity x0) {
        int i = x0.sysMsgPos;
        x0.sysMsgPos = i + 1;
        return i;
    }

    static /* synthetic */ int access$010(MCLibSysBaseActivity x0) {
        int i = x0.sysMsgPos;
        x0.sysMsgPos = i - 1;
        return i;
    }

    /* access modifiers changed from: protected */
    public void initSysMsgWidgets() {
        this.closeSysMsgBoxBtn = (ImageButton) findViewById(R.id.mcLibSysMsgClsBtn);
        this.previousSysMsgBtn = (ImageButton) findViewById(R.id.mcLibSysMsgPreviousBtn);
        this.nextSysMsgBtn = (ImageButton) findViewById(R.id.mcLibSysMsgNextBtn);
        this.sysMsgFunctionBtn = (Button) findViewById(R.id.mcLibSysMsgFunctionBtn);
        this.sysMsgTitle = (TextView) findViewById(R.id.mcLibSysMsgTitleText);
        this.sysMsgContent = (TextView) findViewById(R.id.mcLibSysMsgContentText);
        this.sysMsgCountText = (TextView) findViewById(R.id.mcLibSysMsgCountText);
        this.sysMsgMaskLayer = (RelativeLayout) findViewById(R.id.mcLibSysMsgMaskLayer);
        this.sysMsgBox = (RelativeLayout) findViewById(R.id.mcLibSysMsgBox);
    }

    /* access modifiers changed from: protected */
    public void initSysMsgButtonListeners() {
        this.closeSysMsgBoxBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCLibSysBaseActivity.this.hideSysMsgBox();
            }
        });
        this.previousSysMsgBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCLibSysBaseActivity.access$010(MCLibSysBaseActivity.this);
                int sysMsgSize = MCLibSysBaseActivity.this.sysMsgList.size();
                if (MCLibSysBaseActivity.this.sysMsgPos < 0) {
                    int unused = MCLibSysBaseActivity.this.sysMsgPos = sysMsgSize - 1;
                }
                MCLibSysBaseActivity.this.updateSysMsgPanel(MCLibSysBaseActivity.this.sysMsgList.get(MCLibSysBaseActivity.this.sysMsgPos), MCLibSysBaseActivity.this.sysMsgPos + 1);
            }
        });
        this.nextSysMsgBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCLibSysBaseActivity.access$008(MCLibSysBaseActivity.this);
                if (MCLibSysBaseActivity.this.sysMsgPos >= MCLibSysBaseActivity.this.sysMsgList.size()) {
                    int unused = MCLibSysBaseActivity.this.sysMsgPos = 0;
                }
                MCLibSysBaseActivity.this.updateSysMsgPanel(MCLibSysBaseActivity.this.sysMsgList.get(MCLibSysBaseActivity.this.sysMsgPos), MCLibSysBaseActivity.this.sysMsgPos + 1);
            }
        });
        this.sysMsgFunctionBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCLibSysMsgModel sysMsgModel = MCLibSysBaseActivity.this.sysMsgList.get(MCLibSysBaseActivity.this.sysMsgPos);
                if (sysMsgModel.getTargetId() > 0) {
                    new MCLibAppInfoDialog(MCLibSysBaseActivity.this, R.style.mc_lib_dialog, sysMsgModel.getTargetId(), MCLibSysBaseActivity.this.getCurrentHandler()).show();
                    return;
                }
                MCLibSendFeedbackDelegateImpl delegate = new MCLibSendFeedbackDelegateImpl(MCLibSysBaseActivity.this);
                MCLibPublishWordsDialog publishWordsDialog = new MCLibPublishWordsDialog(MCLibSysBaseActivity.this, R.string.mc_lib_send_feedback, delegate, 500);
                delegate.setDialog(publishWordsDialog);
                publishWordsDialog.show();
            }
        });
        this.sysMsgMaskLayer.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            }
        });
    }

    /* access modifiers changed from: protected */
    public void showSysMsgBox() {
        this.sysMsgList = new ArrayList();
        this.sysMsgList.addAll(MCLibHeartBeatOSService.sysMsgList);
        this.sysMsgPos = 0;
        if (!this.sysMsgList.isEmpty()) {
            final MCLibSysMsgModel sysMsgModel = this.sysMsgList.get(0);
            if (sysMsgModel.getSysMsgType() > 1) {
                this.sysMsgFunctionBtn.setVisibility(4);
            } else if (sysMsgModel.getSysMsgType() != 0 || sysMsgModel.getTargetId() > 0) {
                this.sysMsgFunctionBtn.setVisibility(0);
            } else {
                this.sysMsgFunctionBtn.setVisibility(4);
            }
            initSysMsgButtonListeners();
            getCurrentHandler().post(new Runnable() {
                public void run() {
                    MCLibSysBaseActivity.this.sysMsgMaskLayer.setVisibility(0);
                    MCLibSysBaseActivity.this.sysMsgBox.startAnimation(AnimationUtils.loadAnimation(MCLibSysBaseActivity.this, R.anim.mc_lib_sys_msg_box_show));
                    MCLibSysBaseActivity.this.updateSysMsgPanel(sysMsgModel, 1);
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void updateSysMsgPanel(final MCLibSysMsgModel sysMsgModel, final int position) {
        getCurrentHandler().post(new Runnable() {
            public void run() {
                MCLibSysBaseActivity.this.sysMsgTitle.setText(sysMsgModel.getFromName());
                MCLibSysBaseActivity.this.sysMsgContent.setText(sysMsgModel.getContent());
                MCLibSysBaseActivity.this.sysMsgCountText.setText(position + "/" + MCLibSysBaseActivity.this.sysMsgList.size());
                if (sysMsgModel.getTargetId() > 0) {
                    MCLibSysBaseActivity.this.sysMsgFunctionBtn.setText((int) R.string.mc_lib_sys_msg_preview);
                } else {
                    MCLibSysBaseActivity.this.sysMsgFunctionBtn.setText((int) R.string.mc_lib_send_feedback);
                }
                MCLibHeartBeatOSService.sysMsgList.remove(MCLibSysBaseActivity.this.sysMsgList.get(MCLibSysBaseActivity.this.sysMsgPos));
            }
        });
    }

    /* access modifiers changed from: protected */
    public void hideSysMsgBox() {
        this.sysMsgMaskLayer.setOnClickListener(null);
        getCurrentHandler().post(new Runnable() {
            public void run() {
                Animation anim = AnimationUtils.loadAnimation(MCLibSysBaseActivity.this, R.anim.mc_lib_shrink_to_middle);
                MCLibSysBaseActivity.this.sysMsgBox.startAnimation(anim);
                anim.setAnimationListener(new Animation.AnimationListener() {
                    public void onAnimationStart(Animation animation) {
                    }

                    public void onAnimationRepeat(Animation animation) {
                    }

                    public void onAnimationEnd(Animation animation) {
                        MCLibSysBaseActivity.this.sysMsgMaskLayer.setVisibility(4);
                    }
                });
            }
        });
    }
}
