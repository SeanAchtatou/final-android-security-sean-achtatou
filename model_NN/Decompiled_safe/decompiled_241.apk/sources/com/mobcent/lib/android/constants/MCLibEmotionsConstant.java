package com.mobcent.lib.android.constants;

import com.mobcent.android.seed.autovGhnzo98NIzLsRPLHS.R;
import java.util.LinkedHashMap;

public class MCLibEmotionsConstant {
    private static LinkedHashMap<String, Integer> allHashMap = new LinkedHashMap<>();
    private static MCLibEmotionsConstant mobCentResourceMap;

    private MCLibEmotionsConstant() {
        allHashMap.put("*^^*", Integer.valueOf((int) R.drawable.mc_lib_emotions1));
        allHashMap.put("~_~", Integer.valueOf((int) R.drawable.mc_lib_emotions2));
        allHashMap.put("^o^", Integer.valueOf((int) R.drawable.mc_lib_emotions3));
        allHashMap.put("=_=", Integer.valueOf((int) R.drawable.mc_lib_emotions4));
        allHashMap.put("^_^", Integer.valueOf((int) R.drawable.mc_lib_emotions5));
        allHashMap.put("╯ε╰", Integer.valueOf((int) R.drawable.mc_lib_emotions6));
        allHashMap.put("^O^~", Integer.valueOf((int) R.drawable.mc_lib_emotions7));
        allHashMap.put("ˋ^ˊ", Integer.valueOf((int) R.drawable.mc_lib_emotions8));
        allHashMap.put("+﹏+", Integer.valueOf((int) R.drawable.mc_lib_emotions9));
        allHashMap.put("x_x", Integer.valueOf((int) R.drawable.mc_lib_emotions10));
        allHashMap.put("e_e", Integer.valueOf((int) R.drawable.mc_lib_emotions11));
        allHashMap.put("╰_╯", Integer.valueOf((int) R.drawable.mc_lib_emotions12));
        allHashMap.put(".Q.", Integer.valueOf((int) R.drawable.mc_lib_emotions13));
        allHashMap.put("⊙_⊙", Integer.valueOf((int) R.drawable.mc_lib_emotions14));
        allHashMap.put("╯﹏╰", Integer.valueOf((int) R.drawable.mc_lib_emotions15));
        allHashMap.put("$_$", Integer.valueOf((int) R.drawable.mc_lib_emotions16));
        allHashMap.put("☆_☆", Integer.valueOf((int) R.drawable.mc_lib_emotions17));
        allHashMap.put("～o～", Integer.valueOf((int) R.drawable.mc_lib_emotions18));
        allHashMap.put("._.", Integer.valueOf((int) R.drawable.mc_lib_emotions19));
        allHashMap.put("^ε^", Integer.valueOf((int) R.drawable.mc_lib_emotions20));
        allHashMap.put("~.~", Integer.valueOf((int) R.drawable.mc_lib_emotions21));
        allHashMap.put("〒_〒", Integer.valueOf((int) R.drawable.mc_lib_emotions22));
        allHashMap.put("╰.╯#", Integer.valueOf((int) R.drawable.mc_lib_emotions23));
        allHashMap.put("－_－^", Integer.valueOf((int) R.drawable.mc_lib_emotions24));
        allHashMap.put("}_}", Integer.valueOf((int) R.drawable.mc_lib_emotions25));
        allHashMap.put(">^<", Integer.valueOf((int) R.drawable.mc_lib_emotions26));
        allHashMap.put("≥◇≤", Integer.valueOf((int) R.drawable.mc_lib_emotions27));
        allHashMap.put("x_o", Integer.valueOf((int) R.drawable.mc_lib_emotions28));
        allHashMap.put("^◎-", Integer.valueOf((int) R.drawable.mc_lib_emotions29));
        allHashMap.put("-(-", Integer.valueOf((int) R.drawable.mc_lib_emotions30));
        allHashMap.put("-|-", Integer.valueOf((int) R.drawable.mc_lib_emotions31));
        allHashMap.put("--#", Integer.valueOf((int) R.drawable.mc_lib_emotions32));
        allHashMap.put("╰^╯", Integer.valueOf((int) R.drawable.mc_lib_emotions33));
        allHashMap.put("－_－b", Integer.valueOf((int) R.drawable.mc_lib_emotions34));
        allHashMap.put("⊙o⊙", Integer.valueOf((int) R.drawable.mc_lib_emotions35));
        allHashMap.put("T_T", Integer.valueOf((int) R.drawable.mc_lib_emotions36));
        allHashMap.put("ˇ^ˇ", Integer.valueOf((int) R.drawable.mc_lib_emotions37));
        allHashMap.put("O_o", Integer.valueOf((int) R.drawable.mc_lib_emotions38));
        allHashMap.put("∪_∪", Integer.valueOf((int) R.drawable.mc_lib_emotions39));
        allHashMap.put("π_π", Integer.valueOf((int) R.drawable.mc_lib_emotions40));
        allHashMap.put("+o+", Integer.valueOf((int) R.drawable.mc_lib_emotions41));
        allHashMap.put("^O^y", Integer.valueOf((int) R.drawable.mc_lib_emotions42));
        allHashMap.put("－O－", Integer.valueOf((int) R.drawable.mc_lib_emotions43));
        allHashMap.put("p_p", Integer.valueOf((int) R.drawable.mc_lib_emotions44));
        allHashMap.put("o_O?", Integer.valueOf((int) R.drawable.mc_lib_emotions45));
        allHashMap.put("Q_Q", Integer.valueOf((int) R.drawable.mc_lib_emotions46));
        allHashMap.put("→_→", Integer.valueOf((int) R.drawable.mc_lib_emotions47));
        allHashMap.put("@o@", Integer.valueOf((int) R.drawable.mc_lib_emotions48));
    }

    public static MCLibEmotionsConstant geEmotionConstant() {
        if (mobCentResourceMap == null) {
            mobCentResourceMap = new MCLibEmotionsConstant();
        }
        return mobCentResourceMap;
    }

    public LinkedHashMap<String, Integer> getAllEmotionsMap() {
        return allHashMap;
    }
}
