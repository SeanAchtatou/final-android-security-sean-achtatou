package com.mobcent.lib.android.ui.activity.adapter.holder;

import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MCLibChatRoomListAdapterHolder {
    private LinearLayout friendMsgBox;
    private TextView friendMsgContent;
    private ImageView friendPhotoImageView;
    private TextView friendPubTimeTextView;
    private ProgressBar sendingMsgPrgBar;
    private RelativeLayout userMsgBox;
    private TextView userMsgContent;
    private ImageView userPhotoImageView;
    private TextView userPubTimeTextView;

    public ImageView getUserPhotoImageView() {
        return this.userPhotoImageView;
    }

    public void setUserPhotoImageView(ImageView userPhotoImageView2) {
        this.userPhotoImageView = userPhotoImageView2;
    }

    public ImageView getFriendPhotoImageView() {
        return this.friendPhotoImageView;
    }

    public void setFriendPhotoImageView(ImageView friendPhotoImageView2) {
        this.friendPhotoImageView = friendPhotoImageView2;
    }

    public TextView getUserMsgContent() {
        return this.userMsgContent;
    }

    public void setUserMsgContent(TextView userMsgContent2) {
        this.userMsgContent = userMsgContent2;
    }

    public TextView getUserPubTimeTextView() {
        return this.userPubTimeTextView;
    }

    public void setUserPubTimeTextView(TextView userPubTimeTextView2) {
        this.userPubTimeTextView = userPubTimeTextView2;
    }

    public TextView getFriendMsgContent() {
        return this.friendMsgContent;
    }

    public void setFriendMsgContent(TextView friendMsgContent2) {
        this.friendMsgContent = friendMsgContent2;
    }

    public TextView getFriendPubTimeTextView() {
        return this.friendPubTimeTextView;
    }

    public void setFriendPubTimeTextView(TextView friendPubTimeTextView2) {
        this.friendPubTimeTextView = friendPubTimeTextView2;
    }

    public LinearLayout getFriendMsgBox() {
        return this.friendMsgBox;
    }

    public void setFriendMsgBox(LinearLayout friendMsgBox2) {
        this.friendMsgBox = friendMsgBox2;
    }

    public RelativeLayout getUserMsgBox() {
        return this.userMsgBox;
    }

    public void setUserMsgBox(RelativeLayout userMsgBox2) {
        this.userMsgBox = userMsgBox2;
    }

    public ProgressBar getSendingMsgPrgBar() {
        return this.sendingMsgPrgBar;
    }

    public void setSendingMsgPrgBar(ProgressBar sendingMsgPrgBar2) {
        this.sendingMsgPrgBar = sendingMsgPrgBar2;
    }
}
