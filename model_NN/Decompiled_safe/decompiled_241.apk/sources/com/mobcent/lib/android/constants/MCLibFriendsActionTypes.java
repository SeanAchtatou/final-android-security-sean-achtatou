package com.mobcent.lib.android.constants;

public interface MCLibFriendsActionTypes {
    public static final int FRIEND_MENU_ACTION = 4;
    public static final int FRIEND_MENU_CANCEL = 6;
    public static final int FRIEND_MENU_CHAT_HISTORY = 8;
    public static final int FRIEND_MENU_FOCUS = 1;
    public static final int FRIEND_MENU_MAGIC_ACTION = 7;
    public static final int FRIEND_MENU_PRIVATE_TALK = 3;
    public static final int FRIEND_MENU_TALK_TO = 2;
    public static final int FRIEND_MENU_USER_HOME = 5;
}
