package com.mobcent.share.android.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.DownloadListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import com.mobcent.android.seed.autovGhnzo98NIzLsRPLHS.R;

public abstract class MCShareWebBaseActivity extends MCShareBaseActivity {
    protected WebView mWebView;

    public abstract Handler getCurrentHandler();

    /* access modifiers changed from: protected */
    public void initWebViewPage(final String webUrl) {
        this.mWebView = (WebView) findViewById(R.id.mcShareWebView);
        this.mWebView.getSettings().setJavaScriptEnabled(true);
        this.mWebView.getSettings().setBuiltInZoomControls(true);
        this.mWebView.getSettings().setSupportZoom(true);
        this.mWebView.setDownloadListener(new DownloadListener() {
            public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimetype, long contentLength) {
                MCShareWebBaseActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url)));
            }
        });
        this.mWebView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                MCShareWebBaseActivity.this.showProgressBar();
            }

            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                MCShareWebBaseActivity.this.hideProgressBar();
            }
        });
        this.mWebView.loadUrl(webUrl);
        ((Button) findViewById(R.id.mcShareBackBtn)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                MCShareWebBaseActivity.this.finish();
            }
        });
        ((Button) findViewById(R.id.mcShareRefreshBtn)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                MCShareWebBaseActivity.this.mWebView.loadUrl(webUrl);
            }
        });
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (!this.mWebView.canGoBack() || keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        this.mWebView.goBack();
        return true;
    }
}
