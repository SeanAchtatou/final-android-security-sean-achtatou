package com.mobcent.share.android.activity;

import android.app.Activity;
import android.os.Handler;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.mobcent.android.seed.autovGhnzo98NIzLsRPLHS.R;

public abstract class MCShareBaseActivity extends Activity {
    /* access modifiers changed from: private */
    public RelativeLayout loadingBox;
    public TextView plsWaitText;
    protected ProgressBar progressBar;

    public abstract Handler getCurrentHandler();

    public void hideProgressBar() {
        getCurrentHandler().post(new Runnable() {
            public void run() {
                MCShareBaseActivity.this.loadingBox.setVisibility(8);
            }
        });
    }

    public void showProgressBar() {
        getCurrentHandler().post(new Runnable() {
            public void run() {
                MCShareBaseActivity.this.plsWaitText.setText((int) R.string.mc_share_pls_wait);
                MCShareBaseActivity.this.progressBar.setVisibility(0);
                MCShareBaseActivity.this.loadingBox.setVisibility(0);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void initProgressBox() {
        this.loadingBox = (RelativeLayout) findViewById(R.id.mcShareLoadingBox);
        this.plsWaitText = (TextView) findViewById(R.id.mcSharePlsWaitTextView);
        this.progressBar = (ProgressBar) findViewById(R.id.mcShareProgressBar);
    }
}
