package com.mobcent.share.android.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.mobcent.android.constants.MCLibConstants;
import com.mobcent.android.model.MCShareSyncSiteModel;
import com.mobcent.android.seed.autovGhnzo98NIzLsRPLHS.R;
import com.mobcent.android.service.MCShareSyncSiteService;
import com.mobcent.android.service.impl.MCShareSyncSiteServiceImpl;
import com.mobcent.share.android.activity.adapter.MCShareSitesAdapter;
import com.mobcent.share.android.activity.utils.MCShareStringBundleUtil;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MCShareAppActivity extends MCShareBaseActivity {
    /* access modifiers changed from: private */
    public static String shareAppContent;
    /* access modifiers changed from: private */
    public String appKey;
    private Button backBtn;
    /* access modifiers changed from: private */
    public ImageButton closeBtn;
    /* access modifiers changed from: private */
    public EditText contentEditText;
    public TextView errorMsgText;
    /* access modifiers changed from: private */
    public Handler mHandler = new Handler();
    public TextView noSiteBindTipText;
    public TextView recommendAppsBtn;
    /* access modifiers changed from: private */
    public Button sendShareBtn;
    private Button shareBtn;
    /* access modifiers changed from: private */
    public String sharePic;
    /* access modifiers changed from: private */
    public RelativeLayout shareSiteSelectorMaskLayer;
    /* access modifiers changed from: private */
    public GridView siteGridView;
    public List<String> sitesSelected = new ArrayList();
    /* access modifiers changed from: private */
    public RelativeLayout succMaskLayer;
    public TextView syncBtn;
    private Button syncSiteBtn;
    /* access modifiers changed from: private */
    public int uid = -1;
    /* access modifiers changed from: private */
    public int upperLimit = MCLibConstants.MICROBLOG_WORDS_UPPER_LIMIT;
    public TextView upperLimitText;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.mc_share_share_status);
        shareAppContent = getIntent().getStringExtra("shareContent") == null ? shareAppContent : getIntent().getStringExtra("shareContent");
        this.sharePic = getIntent().getStringExtra("sharePic") == null ? "" : getIntent().getStringExtra("sharePic");
        this.uid = getIntent().getIntExtra("uid", -1);
        String appKeyIn = getIntent().getStringExtra("appKey");
        if (appKeyIn == null || "".equals(appKeyIn)) {
            this.appKey = getResources().getString(R.string.mc_share_app_key);
        } else {
            this.appKey = appKeyIn;
        }
        initProgressBox();
        initWidgets();
        hideProgressBar();
    }

    private void initWidgets() {
        this.backBtn = (Button) findViewById(R.id.mcShareBackBtn);
        this.shareBtn = (Button) findViewById(R.id.mcSharePublishBtn);
        this.syncBtn = (TextView) findViewById(R.id.mcShareSyncBtn);
        this.recommendAppsBtn = (TextView) findViewById(R.id.mcShareRecommendBtn);
        this.contentEditText = (EditText) findViewById(R.id.mcShareContentEditText);
        this.upperLimitText = (TextView) findViewById(R.id.mcShareWordsUpperLimit);
        this.errorMsgText = (TextView) findViewById(R.id.mcShareErrorMsg);
        this.shareSiteSelectorMaskLayer = (RelativeLayout) findViewById(R.id.mcShareSiteSelectorMaskLayer);
        this.closeBtn = (ImageButton) findViewById(R.id.mcShareClsBtn);
        this.siteGridView = (GridView) findViewById(R.id.mcShareSitesGridView);
        this.syncSiteBtn = (Button) findViewById(R.id.mcShareBindSiteBtn);
        this.sendShareBtn = (Button) findViewById(R.id.mcShareSendShareBtn);
        this.noSiteBindTipText = (TextView) findViewById(R.id.mcShareNoSiteBindTip);
        this.succMaskLayer = (RelativeLayout) findViewById(R.id.mcShareSuccBox);
        if (shareAppContent != null && !"".equals(shareAppContent)) {
            this.contentEditText.setText(shareAppContent);
        }
        this.upperLimitText.setText(MCShareStringBundleUtil.resolveString(R.string.mc_share_words_left, new String[]{this.upperLimit + ""}, this));
        this.shareSiteSelectorMaskLayer.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
            }
        });
        this.closeBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                MCShareAppActivity.this.clsSelectSitePanel();
                MCShareAppActivity.this.shareSiteSelectorMaskLayer.setOnClickListener(null);
            }
        });
        this.recommendAppsBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Intent intent = new Intent(MCShareAppActivity.this, MCShareMoreInfoActivity.class);
                intent.putExtra("appKey", MCShareAppActivity.this.appKey);
                MCShareAppActivity.this.startActivity(intent);
            }
        });
        this.syncSiteBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                MCShareAppActivity.this.closeBtn.performClick();
                MCShareAppActivity.this.syncBtn.performClick();
            }
        });
        this.backBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCShareAppActivity.this.finish();
            }
        });
        this.shareBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                MCShareAppActivity.this.openSelectSitePanel();
            }
        });
        this.sendShareBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String content = MCShareAppActivity.this.contentEditText.getText().toString();
                if (content == null || content.equals("")) {
                    MCShareAppActivity.this.contentEditText.setHint((int) R.string.mc_share_say_something);
                    MCShareAppActivity.this.contentEditText.setHintTextColor((int) R.color.mc_share_red);
                } else if (MCShareAppActivity.this.upperLimit - MCShareAppActivity.this.contentEditText.getText().length() < 0) {
                    MCShareAppActivity.this.upperLimitText.setText(MCShareStringBundleUtil.resolveString(R.string.mc_share_publish_words_tip, new String[]{MCShareAppActivity.this.upperLimit + ""}, MCShareAppActivity.this));
                } else {
                    MCShareAppActivity.this.shareApp(content);
                }
            }
        });
        this.contentEditText.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void afterTextChanged(Editable s) {
                MCShareAppActivity.this.mHandler.post(new Runnable() {
                    public void run() {
                        int wordsLeft = MCShareAppActivity.this.upperLimit - MCShareAppActivity.this.contentEditText.getText().length();
                        if (wordsLeft < 0) {
                            MCShareAppActivity.this.upperLimitText.setTextColor(MCShareAppActivity.this.getResources().getColor(R.color.mc_share_red));
                        } else {
                            MCShareAppActivity.this.upperLimitText.setTextColor(MCShareAppActivity.this.getResources().getColor(R.color.mc_share_black));
                        }
                        MCShareAppActivity.this.upperLimitText.setText(MCShareStringBundleUtil.resolveString(R.string.mc_share_words_left, new String[]{wordsLeft + ""}, MCShareAppActivity.this));
                    }
                });
            }
        });
        this.syncBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(MCShareAppActivity.this, MCShareBindSitesActivity.class);
                intent.putExtra("uid", MCShareAppActivity.this.uid);
                intent.putExtra("appKey", MCShareAppActivity.this.appKey);
                MCShareAppActivity.this.startActivity(intent);
            }
        });
    }

    /* access modifiers changed from: private */
    public void openSelectSitePanel() {
        showProgressBar();
        this.mHandler.post(new Runnable() {
            public void run() {
                ((InputMethodManager) MCShareAppActivity.this.getSystemService("input_method")).hideSoftInputFromWindow(MCShareAppActivity.this.contentEditText.getWindowToken(), 0);
                MCShareAppActivity.this.updateGridView();
            }
        });
    }

    /* access modifiers changed from: private */
    public void clsSelectSitePanel() {
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.mc_share_shrink_to_middle);
        animation.setDuration(400);
        animation.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
                MCShareAppActivity.this.mHandler.post(new Runnable() {
                    public void run() {
                        MCShareAppActivity.this.shareSiteSelectorMaskLayer.setVisibility(4);
                    }
                });
            }
        });
        this.shareSiteSelectorMaskLayer.startAnimation(animation);
    }

    /* access modifiers changed from: private */
    public void updateGridView() {
        new Thread() {
            public void run() {
                final List<MCShareSyncSiteModel> sites = new MCShareSyncSiteServiceImpl(MCShareAppActivity.this.uid, MCShareAppActivity.this).getAllSyncSites(MCShareAppActivity.this.appKey);
                MCShareAppActivity.this.sitesSelected.clear();
                for (MCShareSyncSiteModel syncSiteModel : sites) {
                    MCShareAppActivity.this.sitesSelected.add(syncSiteModel.getSiteId() + "");
                }
                MCShareAppActivity.this.mHandler.post(new Runnable() {
                    public void run() {
                        MCShareAppActivity.this.shareSiteSelectorMaskLayer.setVisibility(0);
                        Animation animation = AnimationUtils.loadAnimation(MCShareAppActivity.this, R.anim.mc_share_grow_from_middle);
                        animation.setDuration(200);
                        MCShareAppActivity.this.shareSiteSelectorMaskLayer.startAnimation(animation);
                        ArrayList<HashMap<String, Integer>> sitesHashList = new ArrayList<>();
                        for (int i = 0; i < sites.size(); i++) {
                            HashMap<String, Integer> hashMap = new HashMap<>();
                            hashMap.put(((MCShareSyncSiteModel) sites.get(i)).getSiteName(), Integer.valueOf(((MCShareSyncSiteModel) sites.get(i)).getSiteId()));
                            sitesHashList.add(hashMap);
                        }
                        if (sitesHashList.isEmpty()) {
                            MCShareAppActivity.this.noSiteBindTipText.setVisibility(0);
                            MCShareAppActivity.this.sendShareBtn.setVisibility(8);
                        } else {
                            MCShareAppActivity.this.noSiteBindTipText.setVisibility(4);
                            MCShareAppActivity.this.sendShareBtn.setVisibility(0);
                        }
                        MCShareAppActivity.this.siteGridView.setAdapter((ListAdapter) new MCShareSitesAdapter(MCShareAppActivity.this, sitesHashList, R.layout.mc_share_widget_site_grid_item, new String[0], new int[0], MCShareAppActivity.this.mHandler));
                        MCShareAppActivity.this.hideProgressBar();
                    }
                });
            }
        }.start();
    }

    /* access modifiers changed from: private */
    public void shareApp(final String content) {
        showProgressBar();
        this.sendShareBtn.setEnabled(false);
        if (this.sitesSelected.isEmpty()) {
            this.mHandler.post(new Runnable() {
                public void run() {
                    MCShareAppActivity.this.errorMsgText.setText((int) R.string.mc_share_select_at_least_one);
                    MCShareAppActivity.this.sendShareBtn.setEnabled(true);
                }
            });
            hideProgressBar();
            return;
        }
        new Thread() {
            public void run() {
                MCShareSyncSiteService syncSiteService = new MCShareSyncSiteServiceImpl(MCShareAppActivity.this.uid, MCShareAppActivity.this);
                String unused = MCShareAppActivity.shareAppContent = content;
                String ids = ",";
                for (String siteId : MCShareAppActivity.this.sitesSelected) {
                    ids = ids + siteId + ",";
                }
                final String rs = syncSiteService.shareApp(content, MCShareAppActivity.this.sharePic, ids, MCShareAppActivity.this.appKey);
                MCShareAppActivity.this.hideProgressBar();
                MCShareAppActivity.this.mHandler.post(new Runnable() {
                    public void run() {
                        MCShareAppActivity.this.sendShareBtn.setEnabled(true);
                        if ("rs_succ".equals(rs)) {
                            MCShareAppActivity.this.succMaskLayer.setVisibility(0);
                            MCShareAppActivity.this.succMaskLayer.setOnClickListener(new View.OnClickListener() {
                                public void onClick(View arg0) {
                                    MCShareAppActivity.this.finish();
                                }
                            });
                            Animation animation = AnimationUtils.loadAnimation(MCShareAppActivity.this, R.anim.mc_share_grow_from_middle);
                            animation.setDuration(200);
                            MCShareAppActivity.this.succMaskLayer.startAnimation(animation);
                            animation.setAnimationListener(new Animation.AnimationListener() {
                                public void onAnimationStart(Animation arg0) {
                                }

                                public void onAnimationRepeat(Animation arg0) {
                                }

                                public void onAnimationEnd(Animation arg0) {
                                    MCShareAppActivity.this.mHandler.postDelayed(new Runnable() {
                                        public void run() {
                                            MCShareAppActivity.this.finish();
                                        }
                                    }, 3000);
                                }
                            });
                            return;
                        }
                        new AlertDialog.Builder(MCShareAppActivity.this).setMessage((int) R.string.mc_share_share_fail).setPositiveButton((int) R.string.mc_share_confirm, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.dismiss();
                            }
                        }).show();
                    }
                });
            }
        }.start();
    }

    public Handler getCurrentHandler() {
        return this.mHandler;
    }
}
