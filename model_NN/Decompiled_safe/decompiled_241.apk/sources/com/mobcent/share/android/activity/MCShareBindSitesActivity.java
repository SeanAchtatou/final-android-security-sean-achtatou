package com.mobcent.share.android.activity;

import android.os.Bundle;
import android.os.Handler;
import com.mobcent.android.seed.autovGhnzo98NIzLsRPLHS.R;
import com.mobcent.android.utils.MCSharePhoneUtil;

public class MCShareBindSitesActivity extends MCShareWebBaseActivity {
    private String appKey;
    private Handler mHandler = new Handler();
    private int uid = -1;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.mc_share_web_view);
        initProgressBox();
        this.uid = getIntent().getIntExtra("uid", -1);
        String appKeyIn = getIntent().getStringExtra("appKey");
        if (appKeyIn == null || "".equals(appKeyIn)) {
            this.appKey = getResources().getString(R.string.mc_share_app_key);
        } else {
            this.appKey = appKeyIn;
        }
        initWebViewPage("http://sdk.mobcent.com/sharesdk/weibo/weiboList.do?imei=" + MCSharePhoneUtil.getIMEI(this) + "&appKey=" + this.appKey + "&platType=1&isJson=false&userId=" + this.uid);
    }

    public Handler getCurrentHandler() {
        return this.mHandler;
    }
}
