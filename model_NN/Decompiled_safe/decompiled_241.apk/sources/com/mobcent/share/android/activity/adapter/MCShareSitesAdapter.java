package com.mobcent.share.android.activity.adapter;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SimpleAdapter;
import com.mobcent.android.seed.autovGhnzo98NIzLsRPLHS.R;
import com.mobcent.share.android.activity.MCShareAppActivity;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class MCShareSitesAdapter extends SimpleAdapter {
    /* access modifiers changed from: private */
    public MCShareAppActivity activity;
    private List<HashMap<String, Integer>> data;
    private LayoutInflater inflater;
    /* access modifiers changed from: private */
    public Handler mHandler;
    private int resource;

    public MCShareSitesAdapter(MCShareAppActivity activity2, List<HashMap<String, Integer>> data2, int resource2, String[] from, int[] to, Handler mHandler2) {
        super(activity2, data2, resource2, from, to);
        this.data = data2;
        this.resource = resource2;
        this.inflater = LayoutInflater.from(activity2);
        this.activity = activity2;
        this.mHandler = mHandler2;
    }

    public int getCount() {
        return this.data.size();
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        HashMap<String, Integer> currentData = this.data.get(position);
        String key = getCurrentKey(currentData);
        final Integer siteId = (Integer) currentData.get(key);
        View convertView2 = this.inflater.inflate(this.resource, (ViewGroup) null);
        Button siteBtn = (Button) convertView2.findViewById(R.id.mcShareSiteBtn);
        final ImageView siteSelectedIndicator = (ImageView) convertView2.findViewById(R.id.mcShareSiteSelectedIndicator);
        siteBtn.setText(key);
        if (this.activity.sitesSelected.contains(siteId + "")) {
            this.mHandler.post(new Runnable() {
                public void run() {
                    siteSelectedIndicator.setVisibility(0);
                }
            });
        } else {
            this.mHandler.post(new Runnable() {
                public void run() {
                    siteSelectedIndicator.setVisibility(4);
                }
            });
        }
        siteBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (siteId.intValue() == -100) {
                    MCShareSitesAdapter.this.activity.syncBtn.performClick();
                } else if (MCShareSitesAdapter.this.activity.sitesSelected.contains(siteId + "")) {
                    MCShareSitesAdapter.this.activity.sitesSelected.remove(siteId + "");
                    MCShareSitesAdapter.this.mHandler.post(new Runnable() {
                        public void run() {
                            siteSelectedIndicator.setVisibility(4);
                        }
                    });
                } else {
                    MCShareSitesAdapter.this.activity.sitesSelected.add(siteId + "");
                    MCShareSitesAdapter.this.mHandler.post(new Runnable() {
                        public void run() {
                            siteSelectedIndicator.setVisibility(0);
                        }
                    });
                }
            }
        });
        return convertView2;
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    private String getCurrentKey(HashMap<String, Integer> map) {
        Iterator<Map.Entry<String, Integer>> it = map.entrySet().iterator();
        if (it.hasNext()) {
            return (String) it.next().getKey();
        }
        return "";
    }
}
