package com.mobcent.share.android.activity;

import android.os.Bundle;
import android.os.Handler;
import com.mobcent.android.seed.autovGhnzo98NIzLsRPLHS.R;
import com.mobcent.android.utils.MCSharePhoneUtil;

public class MCShareMoreInfoActivity extends MCShareWebBaseActivity {
    private Handler mHandler = new Handler();

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.mc_share_web_view);
        initProgressBox();
        String appKey = getIntent().getStringExtra("appKey");
        if (appKey == null || "".equals("")) {
            appKey = getResources().getString(R.string.mc_share_app_key);
        }
        initWebViewPage("http://sdk.mobcent.com/sharesdk/wb/app.do?platType=1&imei=" + MCSharePhoneUtil.getIMEI(this) + "&appKey=" + appKey);
    }

    public Handler getCurrentHandler() {
        return this.mHandler;
    }
}
