package com.mobcent.shell.android.activity;

import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.widget.TextView;
import com.mobcent.android.seed.autovGhnzo98NIzLsRPLHS.R;
import com.mobcent.lib.android.ui.dialog.MCLibRegLoginDialog;
import com.mobcent.lib.android.utils.i18n.MCLibStringBundleUtil;
import com.mobcent.shell.android.application.MCShellApplication;

public class MCShellWelcomeActivity extends MCShellUIBaseActivity {
    private Handler mHandler = new Handler();

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.mc_shell_welcome_activity);
        ((TextView) findViewById(R.id.mcShellCopyrightVersion)).setText(Html.fromHtml(getResources().getString(R.string.mc_shell_developer) + "<br/>" + MCLibStringBundleUtil.resolveString(R.string.mc_shell_version_with_tag, new String[]{getResources().getString(R.string.mc_shell_version)}, this)));
        this.mHandler.postDelayed(new Runnable() {
            public void run() {
                new MCLibRegLoginDialog(MCShellWelcomeActivity.this, R.style.mc_lib_dialog, false, false, MCShellInfoListActivity.class).show();
            }
        }, 1000);
        ((MCShellApplication) getApplication()).welcomeActivity = this;
    }

    public Handler getCurrentHandler() {
        return this.mHandler;
    }
}
