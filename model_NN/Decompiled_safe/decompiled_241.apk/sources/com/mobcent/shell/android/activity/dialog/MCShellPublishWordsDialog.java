package com.mobcent.shell.android.activity.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.mobcent.android.seed.autovGhnzo98NIzLsRPLHS.R;
import com.mobcent.android.service.impl.MCLibUserInfoServiceImpl;
import com.mobcent.lib.android.utils.i18n.MCLibStringBundleUtil;
import com.mobcent.shell.android.service.impl.MCShellCommentServiceImpl;

public class MCShellPublishWordsDialog extends Dialog {
    /* access modifiers changed from: private */
    public Button buttonNo;
    /* access modifiers changed from: private */
    public Button buttonYes;
    /* access modifiers changed from: private */
    public Handler mHandler;
    /* access modifiers changed from: private */
    public String newsId;
    /* access modifiers changed from: private */
    public String newsUrl;
    /* access modifiers changed from: private */
    public String photoPath;
    /* access modifiers changed from: private */
    public ProgressBar progressBar;
    /* access modifiers changed from: private */
    public TextView pubMsgText;
    /* access modifiers changed from: private */
    public EditText text;
    /* access modifiers changed from: private */
    public String title;
    /* access modifiers changed from: private */
    public int upperLimit;
    /* access modifiers changed from: private */
    public TextView upperLimitText;

    public MCShellPublishWordsDialog(Context context, int theme, String newsId2, String title2, String newsUrl2, String photoPath2, int upperLimit2, Handler mHandler2) {
        super(context, theme);
        this.newsId = newsId2;
        this.title = title2;
        this.newsUrl = newsUrl2;
        this.photoPath = photoPath2;
        this.upperLimit = upperLimit2;
        this.mHandler = mHandler2;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.mc_shell_dialog_publish_words);
        this.pubMsgText = (TextView) findViewById(R.id.mcShellDialogErrorMsg);
        this.text = (EditText) findViewById(R.id.mcShellContentDialogText);
        this.buttonYes = (Button) findViewById(R.id.mcShellDialogYesButton);
        this.upperLimitText = (TextView) findViewById(R.id.mcShellWordsUpperLimit);
        final String[] arg = {"" + this.upperLimit};
        this.upperLimitText.setText(MCLibStringBundleUtil.resolveString(R.string.mc_lib_words_left, arg, getContext()));
        this.buttonNo = (Button) findViewById(R.id.mcShellDialogNoButton);
        this.progressBar = (ProgressBar) findViewById(R.id.mcShellPubProgressBar);
        this.buttonYes.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String content = MCShellPublishWordsDialog.this.text.getText().toString();
                if (content == null || content.equals("")) {
                    MCShellPublishWordsDialog.this.text.setHint((int) R.string.mc_lib_say_something);
                    MCShellPublishWordsDialog.this.updatePubMsg(MCShellPublishWordsDialog.this.getContext().getResources().getString(R.string.mc_lib_say_something), true);
                } else if (MCShellPublishWordsDialog.this.upperLimit - MCShellPublishWordsDialog.this.text.getText().length() < 0) {
                    MCShellPublishWordsDialog.this.upperLimitText.setText(MCLibStringBundleUtil.resolveString(R.string.mc_lib_publish_words_tip, arg, MCShellPublishWordsDialog.this.getContext()));
                } else {
                    MCShellPublishWordsDialog.this.buttonYes.setVisibility(4);
                    MCShellPublishWordsDialog.this.buttonNo.setVisibility(4);
                    MCShellPublishWordsDialog.this.progressBar.setVisibility(0);
                    MCShellPublishWordsDialog.this.publishWorld(content);
                }
            }
        });
        this.buttonNo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCShellPublishWordsDialog.this.dismiss();
            }
        });
        this.text.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void afterTextChanged(Editable s) {
                if (MCShellPublishWordsDialog.this.upperLimit - MCShellPublishWordsDialog.this.text.getText().length() < 0) {
                    MCShellPublishWordsDialog.this.upperLimitText.setTextColor(MCShellPublishWordsDialog.this.getContext().getResources().getColor(R.color.mc_lib_red));
                } else {
                    MCShellPublishWordsDialog.this.upperLimitText.setTextColor(MCShellPublishWordsDialog.this.getContext().getResources().getColor(R.color.mc_lib_white));
                }
                MCShellPublishWordsDialog.this.upperLimitText.setText(MCLibStringBundleUtil.resolveString(R.string.mc_lib_words_left, arg, MCShellPublishWordsDialog.this.getContext()));
            }
        });
    }

    /* access modifiers changed from: private */
    public void publishWorld(final String comment) {
        new Thread() {
            public void run() {
                String rs = new MCShellCommentServiceImpl(MCShellPublishWordsDialog.this.getContext()).addComment(new MCLibUserInfoServiceImpl(MCShellPublishWordsDialog.this.getContext()).getLoginUserId(), MCShellPublishWordsDialog.this.newsId, "snsAppNews", comment, MCShellPublishWordsDialog.this.title, MCShellPublishWordsDialog.this.newsUrl, MCShellPublishWordsDialog.this.photoPath);
                if (rs.equals("rs_succ")) {
                    MCShellPublishWordsDialog.this.mHandler.post(new Runnable() {
                        public void run() {
                            MCShellPublishWordsDialog.this.updatePubMsg(MCShellPublishWordsDialog.this.getContext().getResources().getString(R.string.mc_lib_pub_succ), false);
                            MCShellPublishWordsDialog.this.mHandler.postDelayed(new Runnable() {
                                public void run() {
                                    MCShellPublishWordsDialog.this.dismiss();
                                }
                            }, 1500);
                        }
                    });
                } else if (rs.equals("connection_fail")) {
                    MCShellPublishWordsDialog.this.updatePubMsg(MCShellPublishWordsDialog.this.getContext().getResources().getString(R.string.mc_lib_connection_fail), true);
                } else {
                    MCShellPublishWordsDialog.this.updatePubMsg(MCShellPublishWordsDialog.this.getContext().getResources().getString(R.string.mc_lib_pub_failed), true);
                }
            }
        }.start();
    }

    /* access modifiers changed from: private */
    public void updatePubMsg(final String errorMsg, final boolean isShowButton) {
        this.mHandler.post(new Runnable() {
            public void run() {
                MCShellPublishWordsDialog.this.pubMsgText.setText(errorMsg);
                if (isShowButton) {
                    MCShellPublishWordsDialog.this.buttonYes.setVisibility(0);
                    MCShellPublishWordsDialog.this.buttonNo.setVisibility(0);
                    MCShellPublishWordsDialog.this.progressBar.setVisibility(4);
                }
            }
        });
    }
}
