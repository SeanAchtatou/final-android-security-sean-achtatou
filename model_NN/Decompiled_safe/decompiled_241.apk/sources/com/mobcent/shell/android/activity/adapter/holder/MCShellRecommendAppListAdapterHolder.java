package com.mobcent.shell.android.activity.adapter.holder;

import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class MCShellRecommendAppListAdapterHolder {
    private TextView appDesc;
    private TextView appDeveloper;
    private ImageView appImageView;
    private TextView appNameView;
    private TextView appSize;
    private TextView appUpdateTime;
    private TextView appVersionView;
    private Button downloadButton;

    public ImageView getAppImageView() {
        return this.appImageView;
    }

    public void setAppImageView(ImageView appImageView2) {
        this.appImageView = appImageView2;
    }

    public TextView getAppNameView() {
        return this.appNameView;
    }

    public void setAppNameView(TextView appNameView2) {
        this.appNameView = appNameView2;
    }

    public TextView getAppVersionView() {
        return this.appVersionView;
    }

    public void setAppVersionView(TextView appVersionView2) {
        this.appVersionView = appVersionView2;
    }

    public TextView getAppSize() {
        return this.appSize;
    }

    public void setAppSize(TextView appSize2) {
        this.appSize = appSize2;
    }

    public TextView getAppDeveloper() {
        return this.appDeveloper;
    }

    public void setAppDeveloper(TextView appDeveloper2) {
        this.appDeveloper = appDeveloper2;
    }

    public TextView getAppUpdateTime() {
        return this.appUpdateTime;
    }

    public void setAppUpdateTime(TextView appUpdateTime2) {
        this.appUpdateTime = appUpdateTime2;
    }

    public TextView getAppDesc() {
        return this.appDesc;
    }

    public void setAppDesc(TextView appDesc2) {
        this.appDesc = appDesc2;
    }

    public Button getDownloadButton() {
        return this.downloadButton;
    }

    public void setDownloadButton(Button downloadButton2) {
        this.downloadButton = downloadButton2;
    }
}
