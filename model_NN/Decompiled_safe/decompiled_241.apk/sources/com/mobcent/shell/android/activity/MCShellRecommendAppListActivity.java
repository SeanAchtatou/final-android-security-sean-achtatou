package com.mobcent.shell.android.activity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.mobcent.android.model.MCLibAppStoreAppModel;
import com.mobcent.android.seed.autovGhnzo98NIzLsRPLHS.R;
import com.mobcent.lib.android.ui.activity.listener.MCLibListViewItemOnClickListener;
import com.mobcent.lib.android.ui.delegate.MCLibUpdateListDelegate;
import com.mobcent.shell.android.activity.adapter.MCShellRecommendAppListAdapter;
import java.util.ArrayList;
import java.util.List;

public class MCShellRecommendAppListActivity extends MCShellUIBaseActivity implements MCLibUpdateListDelegate {
    private MCShellRecommendAppListAdapter adapter;
    protected List<MCLibAppStoreAppModel> infoList;
    private ListView infoListView;
    /* access modifiers changed from: private */
    public Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            if (msg.what == 1 || msg.what == 5) {
                MCShellRecommendAppListActivity.this.updateEmptyAdapter(msg.obj);
            } else if (msg.what == 2) {
                MCShellRecommendAppListActivity.this.updateExistAdapter(msg.obj);
            } else if (msg.what == 3) {
                MCShellRecommendAppListActivity.this.hideProgressBar();
            } else if (msg.what == 4) {
                MCShellRecommendAppListActivity.this.showProgressBar();
            }
        }
    };
    private ImageButton refreshBtn;
    /* access modifiers changed from: private */
    public MCLibUpdateListDelegate updateInfoListDelegate = new MCLibUpdateListDelegate() {
        public void updateList(final int page, final int pageSize, boolean isReadLocally) {
            MCShellRecommendAppListActivity.this.mHandler.sendMessage(MCShellRecommendAppListActivity.this.mHandler.obtainMessage(5, new ArrayList()));
            new Thread() {
                public void run() {
                    MCShellRecommendAppListActivity.this.mHandler.sendEmptyMessage(4);
                    List<MCLibAppStoreAppModel> infoList = MCShellRecommendAppListActivity.this.getAppInfo(page, pageSize);
                    if (infoList == null || infoList.size() <= 0) {
                        MCShellRecommendAppListActivity.this.showEmptyInfoTip(R.string.mc_lib_no_such_data);
                        return;
                    }
                    MCShellRecommendAppListActivity.this.updateAppInfoListView(infoList);
                    MCShellRecommendAppListActivity.this.mHandler.sendEmptyMessage(3);
                }
            }.start();
        }
    };

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.mc_shell_app_list);
        initWindowTitle();
        initSysMsgWidgets();
        initShellTopMenus(true, 2);
        this.infoListView = (ListView) findViewById(R.id.mcShellInfoListView);
        this.infoListView.setOnItemClickListener(new MCLibListViewItemOnClickListener());
        this.infoListView.setDivider(null);
        this.refreshBtn = (ImageButton) findViewById(R.id.mcLibRefreshContentBtn);
        this.refreshBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCShellRecommendAppListActivity.this.updateInfoListDelegate.updateList(1, 10, false);
            }
        });
        initProgressBox();
        initNavBottomBar(105);
        this.refreshBtn.performClick();
    }

    /* access modifiers changed from: private */
    public void updateEmptyAdapter(Object obj) {
        this.adapter = new MCShellRecommendAppListAdapter(this, this.infoListView, R.layout.mc_shell_app_list_row, (List) obj, this);
        this.infoListView.setAdapter((ListAdapter) this.adapter);
        this.infoList = (List) obj;
    }

    /* access modifiers changed from: private */
    public void updateExistAdapter(Object obj) {
        this.adapter.setAppInfoList((List) obj);
        this.adapter.notifyDataSetInvalidated();
        this.adapter.notifyDataSetChanged();
        this.infoList = (List) obj;
    }

    public void updateList(final int page, final int pageSize, boolean isReadLocally) {
        new Thread() {
            public void run() {
                List<MCLibAppStoreAppModel> resultList = new ArrayList<>();
                if (page == 1) {
                    MCShellRecommendAppListActivity.this.mHandler.sendEmptyMessage(4);
                    MCShellRecommendAppListActivity.this.mHandler.sendMessage(MCShellRecommendAppListActivity.this.mHandler.obtainMessage(5, resultList));
                    if (MCShellRecommendAppListActivity.this.infoList != null) {
                        MCShellRecommendAppListActivity.this.infoList.clear();
                    }
                }
                List<MCLibAppStoreAppModel> nextPageUserInfoList = MCShellRecommendAppListActivity.this.getAppInfo(page, pageSize);
                if (nextPageUserInfoList != null && nextPageUserInfoList.size() > 0 && page == 1) {
                    resultList = nextPageUserInfoList;
                    MCShellRecommendAppListActivity.this.mHandler.sendEmptyMessage(3);
                } else if (nextPageUserInfoList != null && nextPageUserInfoList.size() > 0) {
                    resultList.addAll(MCShellRecommendAppListActivity.this.infoList);
                    resultList.remove(resultList.size() - 1);
                    resultList.addAll(nextPageUserInfoList);
                } else if (MCShellRecommendAppListActivity.this.infoList.size() > 0) {
                    resultList.addAll(MCShellRecommendAppListActivity.this.infoList);
                    resultList.remove(resultList.size() - 1);
                }
                MCShellRecommendAppListActivity.this.updateAppInfoListView(resultList);
            }
        }.start();
    }

    /* access modifiers changed from: private */
    public List<MCLibAppStoreAppModel> getAppInfo(int page, int pageSize) {
        return this.infoList;
    }

    /* access modifiers changed from: private */
    public void updateAppInfoListView(List<MCLibAppStoreAppModel> userList) {
        if (this.adapter == null) {
            this.mHandler.sendMessage(this.mHandler.obtainMessage(1, userList));
            return;
        }
        this.mHandler.sendMessage(this.mHandler.obtainMessage(2, userList));
    }

    public Handler getCurrentHandler() {
        return this.mHandler;
    }
}
