package com.mobcent.shell.android.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import com.mobcent.android.seed.autovGhnzo98NIzLsRPLHS.R;
import com.mobcent.lib.android.ui.activity.MCLibUIBaseActivity;
import com.mobcent.shell.android.application.MCShellApplication;

public abstract class MCShellUIBaseActivity extends MCLibUIBaseActivity {
    protected final int NEWS_MENU = 1;
    protected final int RECOMMEND_APP_MENU = 2;
    protected ImageButton backBtn;
    protected ImageButton exitBtn;
    protected TextView newsMenu;
    protected TextView recommendAppsMenu;

    public abstract Handler getCurrentHandler();

    /* access modifiers changed from: protected */
    public void initShellTopMenus(final boolean isShowBackMenu, int currentMenu) {
        this.backBtn = (ImageButton) findViewById(R.id.mcShellBackBtn);
        this.exitBtn = (ImageButton) findViewById(R.id.mcShellExitBtn);
        this.newsMenu = (TextView) findViewById(R.id.mcShellInfoMenu);
        this.recommendAppsMenu = (TextView) findViewById(R.id.mcShellRecommendAppMenu);
        if (!isShowBackMenu) {
            this.backBtn.setBackgroundResource(R.drawable.mc_shell_home);
        }
        if (currentMenu == 1) {
            this.newsMenu.setBackgroundResource(R.drawable.mc_shell_bar_button_d);
            this.recommendAppsMenu.setBackgroundDrawable(null);
        } else if (currentMenu == 2) {
            this.newsMenu.setBackgroundDrawable(null);
            this.recommendAppsMenu.setBackgroundResource(R.drawable.mc_shell_bar_button_d);
        }
        this.backBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (isShowBackMenu) {
                    MCShellUIBaseActivity.this.finish();
                }
            }
        });
        this.exitBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCShellUIBaseActivity.this.showExitAppDialog();
            }
        });
        this.recommendAppsMenu.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCShellUIBaseActivity.this.startActivity(new Intent(MCShellUIBaseActivity.this, MCShellRecommendAppListActivity.class));
            }
        });
        this.newsMenu.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCShellUIBaseActivity.this.startActivity(new Intent(MCShellUIBaseActivity.this, MCShellInfoListActivity.class));
            }
        });
    }

    /* access modifiers changed from: private */
    public void showExitAppDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle((int) R.string.mc_lib_tips);
        alertDialog.setMessage((int) R.string.mc_shell_exit_app);
        alertDialog.setPositiveButton((int) R.string.mc_lib_confirm, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                ((MCShellApplication) MCShellUIBaseActivity.this.getApplication()).exitApp(MCShellUIBaseActivity.this);
            }
        });
        alertDialog.setNegativeButton((int) R.string.mc_lib_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        });
        alertDialog.show();
    }

    /* access modifiers changed from: protected */
    public void returnToBpApp() {
        startActivity(new Intent(this, MCShellInfoListActivity.class));
    }

    /* access modifiers changed from: protected */
    public void addActivityToStack() {
        ((MCShellApplication) getApplication()).addShellActivityToStack(this);
    }

    /* access modifiers changed from: protected */
    public void removeActivityFromStack() {
        ((MCShellApplication) getApplication()).removeShellActivityFromStack(this);
    }
}
