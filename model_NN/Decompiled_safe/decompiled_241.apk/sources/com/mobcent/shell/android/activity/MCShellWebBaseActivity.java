package com.mobcent.shell.android.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.DownloadListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.RelativeLayout;
import com.mobcent.android.constants.MCLibConstants;
import com.mobcent.android.constants.MCLibMobCentApiConstant;
import com.mobcent.android.seed.autovGhnzo98NIzLsRPLHS.R;
import com.mobcent.lib.android.ui.activity.MCLibStatusRepliesActivity;
import com.mobcent.lib.android.utils.i18n.MCLibStringBundleUtil;
import com.mobcent.shell.android.activity.dialog.MCShellPublishWordsDialog;

public abstract class MCShellWebBaseActivity extends MCShellUIBaseActivity {
    protected Button addCommentBtn;
    protected RelativeLayout commentBox;
    protected Button commentListBtn;
    protected WebView mWebView;

    public abstract Handler getCurrentHandler();

    /* access modifiers changed from: protected */
    public void initWebViewPage(final String webUrl) {
        this.commentBox = (RelativeLayout) findViewById(R.id.mcShellCommentBox);
        this.addCommentBtn = (Button) findViewById(R.id.mcShellAddCommentBtn);
        this.commentListBtn = (Button) findViewById(R.id.mcShellCommentListBtn);
        this.mWebView = (WebView) findViewById(R.id.mcShellWebView);
        this.mWebView.getSettings().setJavaScriptEnabled(true);
        this.mWebView.getSettings().setBuiltInZoomControls(false);
        this.mWebView.getSettings().setSupportZoom(true);
        this.mWebView.setDownloadListener(new DownloadListener() {
            public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimetype, long contentLength) {
                MCShellWebBaseActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url)));
            }
        });
        this.mWebView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                if (url.indexOf("content.jsp") == -1) {
                    MCShellWebBaseActivity.this.hideCommentBox();
                }
                MCShellWebBaseActivity.this.showProgressBar();
            }

            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                Uri uri = Uri.parse(url);
                String pageType = uri.getQueryParameter("pageType");
                if (pageType != null && pageType.equals("detail")) {
                    MCShellWebBaseActivity.this.showAndInitCommentBox(uri.getQueryParameter("newsId"), uri.getQueryParameter("title"), uri.getQueryParameter("pageFile"));
                }
                MCShellWebBaseActivity.this.hideProgressBar();
                if (MCShellWebBaseActivity.this.mWebView.canGoBack()) {
                    MCShellWebBaseActivity.this.getCurrentHandler().post(new Runnable() {
                        public void run() {
                            MCShellWebBaseActivity.this.backBtn.setBackgroundResource(R.drawable.mc_shell_button_back);
                            MCShellWebBaseActivity.this.backBtn.setOnClickListener(new View.OnClickListener() {
                                public void onClick(View arg0) {
                                    MCShellWebBaseActivity.this.mWebView.goBack();
                                }
                            });
                        }
                    });
                } else {
                    MCShellWebBaseActivity.this.getCurrentHandler().post(new Runnable() {
                        public void run() {
                            MCShellWebBaseActivity.this.backBtn.setBackgroundResource(R.drawable.mc_shell_home);
                            MCShellWebBaseActivity.this.backBtn.setOnClickListener(null);
                        }
                    });
                }
            }
        });
        this.mWebView.loadUrl(webUrl);
        this.newsMenu.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                MCShellWebBaseActivity.this.mWebView.loadUrl(webUrl);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void showAndInitCommentBox(final String newsId, final String title, final String newsUrl) {
        getCurrentHandler().post(new Runnable() {
            public void run() {
                MCShellWebBaseActivity.this.commentBox.setVisibility(0);
                MCShellWebBaseActivity.this.commentListBtn.setText(MCLibStringBundleUtil.resolveString(R.string.mc_shell_comment_list, new String[]{""}, MCShellWebBaseActivity.this));
                MCShellWebBaseActivity.this.addCommentBtn.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View arg0) {
                        new MCShellPublishWordsDialog(MCShellWebBaseActivity.this, R.style.mc_lib_dialog, newsId, title, newsUrl, "", MCLibConstants.MICROBLOG_WORDS_UPPER_LIMIT, MCShellWebBaseActivity.this.getCurrentHandler()).show();
                    }
                });
                MCShellWebBaseActivity.this.commentListBtn.setOnClickListener(new View.OnClickListener() {
                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
                     arg types: [java.lang.String, int]
                     candidates:
                      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
                    public void onClick(View arg0) {
                        Intent intent = new Intent(MCShellWebBaseActivity.this, MCLibStatusRepliesActivity.class);
                        intent.putExtra(MCLibMobCentApiConstant.IS_COMMENT_LIST, true);
                        intent.putExtra(MCLibMobCentApiConstant.OBJECT_ID, newsId);
                        intent.putExtra("type", "snsAppNews");
                        MCShellWebBaseActivity.this.startActivity(intent);
                    }
                });
            }
        });
    }

    /* access modifiers changed from: protected */
    public void hideCommentBox() {
        getCurrentHandler().post(new Runnable() {
            public void run() {
                MCShellWebBaseActivity.this.commentBox.setVisibility(8);
            }
        });
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (!this.mWebView.canGoBack() || keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        this.mWebView.goBack();
        return true;
    }
}
