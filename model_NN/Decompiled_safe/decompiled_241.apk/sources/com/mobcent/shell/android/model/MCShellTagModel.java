package com.mobcent.shell.android.model;

public class MCShellTagModel {
    private int tagId;
    private String tagName;

    public int getTagId() {
        return this.tagId;
    }

    public void setTagId(int tagId2) {
        this.tagId = tagId2;
    }

    public String getTagName() {
        return this.tagName;
    }

    public void setTagName(String tagName2) {
        this.tagName = tagName2;
    }
}
