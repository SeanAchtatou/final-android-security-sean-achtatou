package com.mobcent.shell.android.activity.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.mobcent.android.model.MCLibAppStoreAppModel;
import com.mobcent.android.model.MCLibDownloadProfileModel;
import com.mobcent.android.seed.autovGhnzo98NIzLsRPLHS.R;
import com.mobcent.android.service.impl.MCLibDownloadManagerServiceImpl;
import com.mobcent.lib.android.ui.activity.MCLibDownloadManagerActivity;
import com.mobcent.lib.android.ui.activity.adapter.MCLibBaseArrayAdapter;
import com.mobcent.lib.android.ui.delegate.MCLibUpdateListDelegate;
import com.mobcent.lib.android.utils.MCLibAsyncImageLoader;
import com.mobcent.lib.android.utils.MCLibBitmapCallback;
import com.mobcent.lib.android.utils.i18n.MCLibStringBundleUtil;
import com.mobcent.shell.android.activity.adapter.holder.MCShellRecommendAppListAdapterHolder;
import java.util.List;

public class MCShellRecommendAppListAdapter extends MCLibBaseArrayAdapter {
    private List<MCLibAppStoreAppModel> appInfoList;
    public MCLibAsyncImageLoader asyncImageLoader;
    /* access modifiers changed from: private */
    public ListView listView;
    private int rowResourceId;

    public List<MCLibAppStoreAppModel> getAppInfoList() {
        return this.appInfoList;
    }

    public void setAppInfoList(List<MCLibAppStoreAppModel> appInfoList2) {
        this.appInfoList = appInfoList2;
    }

    public MCShellRecommendAppListAdapter(Context context, ListView listView2, int textViewResourceId, List<MCLibAppStoreAppModel> objects, MCLibUpdateListDelegate delegate) {
        super(context, textViewResourceId, objects);
        this.rowResourceId = textViewResourceId;
        this.inflater = LayoutInflater.from(context);
        this.appInfoList = objects;
        this.listView = listView2;
        this.delegate = delegate;
        this.asyncImageLoader = new MCLibAsyncImageLoader(context);
    }

    public int getCount() {
        return this.appInfoList.size();
    }

    public Object getItem(int position) {
        return this.appInfoList.get(position);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        MCShellRecommendAppListAdapterHolder holder;
        final MCLibAppStoreAppModel appInfoModel = this.appInfoList.get(position);
        if (appInfoModel.isFetchMore()) {
            return getFetchMoreView(appInfoModel.getCurrentPage() + 1, appInfoModel.isReadFromLocal());
        }
        if (convertView == null) {
            convertView = this.inflater.inflate(this.rowResourceId, (ViewGroup) null);
            holder = new MCShellRecommendAppListAdapterHolder();
            convertView.setTag(holder);
            initAppInfoHolder(convertView, holder);
        } else {
            holder = (MCShellRecommendAppListAdapterHolder) convertView.getTag();
        }
        if (holder == null) {
            convertView = this.inflater.inflate(this.rowResourceId, (ViewGroup) null);
            holder = new MCShellRecommendAppListAdapterHolder();
            convertView.setTag(holder);
            initAppInfoHolder(convertView, holder);
        }
        holder.getAppDesc().setText(MCLibStringBundleUtil.resolveString(R.string.mc_shell_app_desc, new String[]{appInfoModel.getDescription()}, getContext()));
        holder.getAppDeveloper().setText(MCLibStringBundleUtil.resolveString(R.string.mc_shell_app_developer, new String[]{appInfoModel.getDeveloperName()}, getContext()));
        holder.getAppNameView().setText(appInfoModel.getAppName());
        holder.getAppSize().setText(MCLibStringBundleUtil.resolveString(R.string.mc_shell_app_size, new String[]{appInfoModel.getSize()}, getContext()));
        holder.getAppUpdateTime().setText(MCLibStringBundleUtil.resolveString(R.string.mc_shell_app_pub_date, new String[]{appInfoModel.getPublishTime()}, getContext()));
        holder.getAppVersionView().setText(MCLibStringBundleUtil.resolveString(R.string.mc_shell_app_version, new String[]{appInfoModel.getVersion()}, getContext()));
        holder.getAppImageView().setTag(appInfoModel.getAppImageUrl() + "");
        updateImage(appInfoModel, holder);
        holder.getDownloadButton().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCLibDownloadProfileModel model = new MCLibDownloadProfileModel();
                model.setAppId(appInfoModel.getAppId());
                model.setAppName(appInfoModel.getAppName());
                model.setDownloadSize(0);
                model.setFileSize(0);
                model.setUrl(appInfoModel.getDownloadPath());
                model.setStatus(0);
                model.setAppType(appInfoModel.getCategoryId());
                new MCLibDownloadManagerServiceImpl(MCShellRecommendAppListAdapter.this.getContext()).addOrUpdateDownloadProfile(model, false);
                MCShellRecommendAppListAdapter.this.getContext().startActivity(new Intent(MCShellRecommendAppListAdapter.this.getContext(), MCLibDownloadManagerActivity.class));
            }
        });
        return convertView;
    }

    private void initAppInfoHolder(View convertView, MCShellRecommendAppListAdapterHolder holder) {
        holder.setAppNameView((TextView) convertView.findViewById(R.id.mcShellAppName));
        holder.setAppVersionView((TextView) convertView.findViewById(R.id.mcShellAppVersion));
        holder.setAppSize((TextView) convertView.findViewById(R.id.mcShellAppSize));
        holder.setAppDeveloper((TextView) convertView.findViewById(R.id.mcShellAppDeveloper));
        holder.setAppUpdateTime((TextView) convertView.findViewById(R.id.mcShellAppUpdateTime));
        holder.setAppDesc((TextView) convertView.findViewById(R.id.mcShellAppDesc));
        holder.setAppImageView((ImageView) convertView.findViewById(R.id.mcShellAppImage));
        holder.setDownloadButton((Button) convertView.findViewById(R.id.mcShellDownloadBtn));
    }

    private void updateImage(MCLibAppStoreAppModel appInfo, MCShellRecommendAppListAdapterHolder holder) {
        Bitmap cachedImage = this.asyncImageLoader.loadBitmap(appInfo.getAppImageUrl() + "", new MCLibBitmapCallback() {
            public void imageLoaded(Bitmap bitmap, String imageUrl) {
                ImageView imageViewByTag = (ImageView) MCShellRecommendAppListAdapter.this.listView.findViewWithTag(imageUrl);
                if (imageViewByTag == null) {
                    return;
                }
                if (bitmap == null) {
                    imageViewByTag.setBackgroundResource(R.drawable.mc_lib_app_img);
                } else if (bitmap.isRecycled()) {
                    imageViewByTag.setBackgroundResource(R.drawable.mc_lib_app_img);
                } else {
                    imageViewByTag.setBackgroundDrawable(new BitmapDrawable(bitmap));
                }
            }
        });
        if (cachedImage == null) {
            holder.getAppImageView().setBackgroundResource(R.drawable.mc_lib_app_img);
        } else if (cachedImage.isRecycled()) {
            holder.getAppImageView().setBackgroundResource(R.drawable.mc_lib_app_img);
        } else {
            holder.getAppImageView().setBackgroundDrawable(new BitmapDrawable(cachedImage));
        }
    }
}
