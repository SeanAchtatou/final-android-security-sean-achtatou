package com.mobcent.shell.android.application;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import com.mobcent.android.os.service.helper.MCLibDeveloperServiceHelper;
import com.mobcent.android.utils.PhoneUtil;
import com.mobcent.shell.android.activity.MCShellWelcomeActivity;
import com.mobcent.shell.android.model.MCShellTagModel;
import java.util.ArrayList;
import java.util.List;

public class MCShellApplication extends Application {
    private List<Activity> shellActivityStack = new ArrayList();
    private List<MCShellTagModel> tagModels;
    public MCShellWelcomeActivity welcomeActivity;

    public void setTagModels(List<MCShellTagModel> tagModels2) {
        this.tagModels = tagModels2;
    }

    public List<MCShellTagModel> getTagModels() {
        return this.tagModels;
    }

    public void exitApp(Activity activity) {
        MCLibDeveloperServiceHelper.onExit(activity);
        if (Integer.parseInt(PhoneUtil.getSDKVersion()) < 8) {
            ((ActivityManager) getSystemService("activity")).restartPackage(getPackageName());
            activity.finish();
        } else if (this.shellActivityStack != null && !this.shellActivityStack.isEmpty()) {
            for (Activity activityToClose : this.shellActivityStack) {
                if (activityToClose != null) {
                    activityToClose.finish();
                }
            }
            this.shellActivityStack.clear();
        }
    }

    public List<Activity> getShellActivityStack() {
        return this.shellActivityStack;
    }

    public void addShellActivityToStack(Activity activity) {
        if (!this.shellActivityStack.contains(activity)) {
            this.shellActivityStack.add(activity);
        }
    }

    public void removeShellActivityFromStack(Activity activity) {
        if (this.shellActivityStack.contains(activity)) {
            this.shellActivityStack.remove(activity);
        }
    }
}
