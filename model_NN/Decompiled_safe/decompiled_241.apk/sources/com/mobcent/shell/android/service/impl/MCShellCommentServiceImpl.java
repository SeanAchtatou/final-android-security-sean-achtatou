package com.mobcent.shell.android.service.impl;

import android.content.Context;
import com.mobcent.android.api.HttpClientUtil;
import com.mobcent.android.constants.MCLibMobCentApiConstant;
import com.mobcent.android.service.impl.helper.BaseJsonHelper;
import com.mobcent.shell.android.service.MCShellCommentService;
import java.util.HashMap;

public class MCShellCommentServiceImpl implements MCShellCommentService, MCLibMobCentApiConstant {
    private Context context;

    public MCShellCommentServiceImpl(Context context2) {
        this.context = context2;
    }

    public String addComment(int userId, String objectId, String type, String comment, String content, String addrPath, String photoPath) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", userId + "");
        params.put(MCLibMobCentApiConstant.OBJECT_ID, objectId);
        params.put("type", type);
        params.put("comment", comment);
        params.put("content", content);
        params.put("addrPath", addrPath);
        params.put("photoPath", photoPath);
        String jsonString = HttpClientUtil.doPostRequest("http://sdk.mobcent.com/sdk/action/commentForum.do", params, this.context);
        if (jsonString.equals("connection_fail")) {
            return "connection_fail";
        }
        return BaseJsonHelper.formJsonRS(jsonString);
    }
}
