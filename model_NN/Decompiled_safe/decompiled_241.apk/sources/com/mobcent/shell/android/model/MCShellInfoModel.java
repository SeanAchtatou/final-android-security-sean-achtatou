package com.mobcent.shell.android.model;

import com.mobcent.android.model.MCLibBaseModel;

public class MCShellInfoModel extends MCLibBaseModel {
    private static final long serialVersionUID = -4809983651398137440L;
    private String infoTitle;
    private String pubTime;
    private String readUrl;

    public String getInfoTitle() {
        return this.infoTitle;
    }

    public void setInfoTitle(String infoTitle2) {
        this.infoTitle = infoTitle2;
    }

    public String getPubTime() {
        return this.pubTime;
    }

    public void setPubTime(String pubTime2) {
        this.pubTime = pubTime2;
    }

    public String getReadUrl() {
        return this.readUrl;
    }

    public void setReadUrl(String readUrl2) {
        this.readUrl = readUrl2;
    }
}
