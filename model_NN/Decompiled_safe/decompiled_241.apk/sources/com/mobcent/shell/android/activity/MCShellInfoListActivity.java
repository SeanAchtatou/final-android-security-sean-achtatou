package com.mobcent.shell.android.activity;

import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageButton;
import com.mobcent.android.seed.autovGhnzo98NIzLsRPLHS.R;
import com.mobcent.shell.android.application.MCShellApplication;

public class MCShellInfoListActivity extends MCShellWebBaseActivity {
    private Handler mHandler = new Handler();
    private ImageButton searchBtn;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.mc_shell_info_list);
        clearWelcomeActivity();
        initWindowTitle();
        initSysMsgWidgets();
        initShellTopMenus(false, 1);
        initProgressBox();
        initNavBottomBar(105);
        initSearchBtn();
        initWebViewPage("http://app.bizsung.com/snsApp/index.jsp?appId=" + getResources().getString(R.string.mc_shell_app_id));
    }

    private void initSearchBtn() {
        this.searchBtn = (ImageButton) findViewById(R.id.mcShellSearchBtn);
        this.searchBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                MCShellInfoListActivity.this.mWebView.loadUrl("http://app.bizsung.com/snsApp/search.jsp?appId=" + MCShellInfoListActivity.this.getResources().getString(R.string.mc_shell_app_id));
            }
        });
    }

    private void clearWelcomeActivity() {
        MCShellApplication application = (MCShellApplication) getApplication();
        if (application.welcomeActivity != null) {
            application.welcomeActivity.finish();
            application.welcomeActivity = null;
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (this.mWebView.canGoBack() || keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        this.exitBtn.performClick();
        return true;
    }

    public Handler getCurrentHandler() {
        return this.mHandler;
    }
}
