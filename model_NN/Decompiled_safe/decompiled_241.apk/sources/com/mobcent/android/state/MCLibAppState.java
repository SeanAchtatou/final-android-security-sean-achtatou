package com.mobcent.android.state;

import android.app.Activity;
import java.util.ArrayList;
import java.util.List;

public class MCLibAppState {
    public static int APP_OFF_LINE = 3;
    public static int APP_RELOGIN = 4;
    public static int APP_RUNNING = 10000;
    public static int APP_SLEEP = 2;
    public static int IS_RELOGINING = 2;
    public static int NEED_RELOGIN = 1;
    public static int NO_NEED_RELOGIN = 0;
    private static List<Activity> activityStack = new ArrayList();
    public static boolean isActive = true;
    public static int isNeedRelogin = NO_NEED_RELOGIN;
    public static boolean isValidLogin = false;
    public static int serverHeartBeatReq = APP_RUNNING;
    public static String sessionId = "";

    public static List<Activity> getActivityStack() {
        return activityStack;
    }

    public static void addActivityToStack(Activity activity) {
        if (!activityStack.contains(activity)) {
            activityStack.add(activity);
        }
    }

    public static void removeActivityFromStack(Activity activity) {
        if (activityStack.contains(activity)) {
            activityStack.remove(activity);
        }
    }
}
