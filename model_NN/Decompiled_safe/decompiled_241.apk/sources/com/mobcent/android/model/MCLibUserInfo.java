package com.mobcent.android.model;

import java.util.List;

public class MCLibUserInfo extends MCLibBaseModel {
    private static final long serialVersionUID = 8230733980676943366L;
    private String age;
    private int appTotalNum;
    private String birthday;
    private String date;
    private String email;
    private String emotionWords;
    private int eventNum;
    private int fansNum;
    private int feedType;
    private int focusNum;
    private String gender;
    private String image;
    private int isAutoLogin;
    private boolean isBlocked;
    private int isCurrentUser;
    private boolean isFollowed;
    private int isForceShowMagicImage;
    private boolean isFriend;
    private boolean isOnline;
    private int isSavePwd;
    private boolean isSelected;
    private long lastMsgTime;
    private String name;
    private int newMsgNum;
    private String nickName;
    private String password;
    private int photoId;
    private String photoPath;
    private String pos;
    private int sourceCategoryId;
    private String sourceName;
    private int sourceProId;
    private String sourceUrl;
    private int speakType;
    private int statusNum;
    private int uid;
    private int unreadMsgCount;
    private List<MCLibUserStatus> userEventList;
    private int userSex;
    private List<MCLibUserStatus> userStatusList;

    public int getUid() {
        return this.uid;
    }

    public void setUid(int uid2) {
        this.uid = uid2;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getGender() {
        return this.gender;
    }

    public void setGender(String gender2) {
        this.gender = gender2;
    }

    public String getAge() {
        return this.age;
    }

    public void setAge(String age2) {
        this.age = age2;
    }

    public String getPos() {
        return this.pos;
    }

    public void setPos(String pos2) {
        this.pos = pos2;
    }

    public String getImage() {
        return this.image;
    }

    public void setImage(String image2) {
        this.image = image2;
    }

    public boolean isFollowed() {
        return this.isFollowed;
    }

    public void setFollowed(boolean isFollowed2) {
        this.isFollowed = isFollowed2;
    }

    public int getFansNum() {
        return this.fansNum;
    }

    public void setFansNum(int fansNum2) {
        this.fansNum = fansNum2;
    }

    public boolean isOnline() {
        return this.isOnline;
    }

    public void setOnline(boolean isOnline2) {
        this.isOnline = isOnline2;
    }

    public String getEmotionWords() {
        return this.emotionWords;
    }

    public void setEmotionWords(String emotionWords2) {
        this.emotionWords = emotionWords2;
    }

    public String getBirthday() {
        return this.birthday;
    }

    public void setBirthday(String birthday2) {
        this.birthday = birthday2;
    }

    public String getNickName() {
        return this.nickName;
    }

    public void setNickName(String nickName2) {
        this.nickName = nickName2;
    }

    public List<MCLibUserStatus> getUserStatusList() {
        return this.userStatusList;
    }

    public void setUserStatusList(List<MCLibUserStatus> userStatusList2) {
        this.userStatusList = userStatusList2;
    }

    public List<MCLibUserStatus> getUserEventList() {
        return this.userEventList;
    }

    public void setUserEventList(List<MCLibUserStatus> userEventList2) {
        this.userEventList = userEventList2;
    }

    public int getFocusNum() {
        return this.focusNum;
    }

    public void setFocusNum(int focusNum2) {
        this.focusNum = focusNum2;
    }

    public int getStatusNum() {
        return this.statusNum;
    }

    public void setStatusNum(int statusNum2) {
        this.statusNum = statusNum2;
    }

    public int getEventNum() {
        return this.eventNum;
    }

    public void setEventNum(int eventNum2) {
        this.eventNum = eventNum2;
    }

    public int getNewMsgNum() {
        return this.newMsgNum;
    }

    public void setNewMsgNum(int newMsgNum2) {
        this.newMsgNum = newMsgNum2;
    }

    public int getAppTotalNum() {
        return this.appTotalNum;
    }

    public void setAppTotalNum(int appTotalNum2) {
        this.appTotalNum = appTotalNum2;
    }

    public String getDate() {
        return this.date;
    }

    public void setDate(String date2) {
        this.date = date2;
    }

    public int getFeedType() {
        return this.feedType;
    }

    public void setFeedType(int feedType2) {
        this.feedType = feedType2;
    }

    public int getSpeakType() {
        return this.speakType;
    }

    public void setSpeakType(int speakType2) {
        this.speakType = speakType2;
    }

    public boolean isFriend() {
        return this.isFriend;
    }

    public void setFriend(boolean isFriend2) {
        this.isFriend = isFriend2;
    }

    public String getSourceName() {
        return this.sourceName;
    }

    public void setSourceName(String sourceName2) {
        this.sourceName = sourceName2;
    }

    public String getSourceUrl() {
        return this.sourceUrl;
    }

    public void setSourceUrl(String sourceUrl2) {
        this.sourceUrl = sourceUrl2;
    }

    public int getSourceProId() {
        return this.sourceProId;
    }

    public void setSourceProId(int sourceProId2) {
        this.sourceProId = sourceProId2;
    }

    public int getSourceCategoryId() {
        return this.sourceCategoryId;
    }

    public void setSourceCategoryId(int sourceCategoryId2) {
        this.sourceCategoryId = sourceCategoryId2;
    }

    public boolean isSelected() {
        return this.isSelected;
    }

    public void setSelected(boolean isSelected2) {
        this.isSelected = isSelected2;
    }

    public int getPhotoId() {
        return this.photoId;
    }

    public void setPhotoId(int photoId2) {
        this.photoId = photoId2;
    }

    public String getPhotoPath() {
        return this.photoPath;
    }

    public void setPhotoPath(String photoPath2) {
        this.photoPath = photoPath2;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email2) {
        this.email = email2;
    }

    public int getUserSex() {
        return this.userSex;
    }

    public void setUserSex(int userSex2) {
        this.userSex = userSex2;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password2) {
        this.password = password2;
    }

    public int getIsSavePwd() {
        return this.isSavePwd;
    }

    public void setIsSavePwd(int isSavePwd2) {
        this.isSavePwd = isSavePwd2;
    }

    public int getIsAutoLogin() {
        return this.isAutoLogin;
    }

    public void setIsAutoLogin(int isAutoLogin2) {
        this.isAutoLogin = isAutoLogin2;
    }

    public int getIsForceShowMagicImage() {
        return this.isForceShowMagicImage;
    }

    public void setIsForceShowMagicImage(int isForceShowMagicImage2) {
        this.isForceShowMagicImage = isForceShowMagicImage2;
    }

    public int getIsCurrentUser() {
        return this.isCurrentUser;
    }

    public void setIsCurrentUser(int isCurrentUser2) {
        this.isCurrentUser = isCurrentUser2;
    }

    public long getLastMsgTime() {
        return this.lastMsgTime;
    }

    public void setLastMsgTime(long lastMsgTime2) {
        this.lastMsgTime = lastMsgTime2;
    }

    public int getUnreadMsgCount() {
        return this.unreadMsgCount;
    }

    public void setUnreadMsgCount(int unreadMsgCount2) {
        this.unreadMsgCount = unreadMsgCount2;
    }

    public boolean isBlocked() {
        return this.isBlocked;
    }

    public void setBlocked(boolean isBlocked2) {
        this.isBlocked = isBlocked2;
    }
}
