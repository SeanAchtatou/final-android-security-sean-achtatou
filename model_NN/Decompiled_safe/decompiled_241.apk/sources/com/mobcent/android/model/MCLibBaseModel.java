package com.mobcent.android.model;

import java.io.Serializable;

public class MCLibBaseModel implements Serializable {
    private static final long serialVersionUID = 2559599578059798417L;
    private int currentPage = 1;
    private String errorMsg;
    private boolean isFetchMore = false;
    private boolean isLoading = false;
    private boolean isReadFromLocal = false;
    private boolean isRefreshNeeded = false;
    private String lastUpdateTime = "";
    private int totalNum;

    public int getTotalNum() {
        return this.totalNum;
    }

    public void setTotalNum(int totalNum2) {
        this.totalNum = totalNum2;
    }

    public boolean isFetchMore() {
        return this.isFetchMore;
    }

    public void setFetchMore(boolean isFetchMore2) {
        this.isFetchMore = isFetchMore2;
    }

    public int getCurrentPage() {
        return this.currentPage;
    }

    public void setCurrentPage(int currentPage2) {
        this.currentPage = currentPage2;
    }

    public boolean isLoading() {
        return this.isLoading;
    }

    public void setLoading(boolean isLoading2) {
        this.isLoading = isLoading2;
    }

    public boolean isRefreshNeeded() {
        return this.isRefreshNeeded;
    }

    public void setRefreshNeeded(boolean isRefreshNeeded2) {
        this.isRefreshNeeded = isRefreshNeeded2;
    }

    public boolean isReadFromLocal() {
        return this.isReadFromLocal;
    }

    public void setReadFromLocal(boolean isReadFromLocal2) {
        this.isReadFromLocal = isReadFromLocal2;
    }

    public String getLastUpdateTime() {
        return this.lastUpdateTime;
    }

    public void setLastUpdateTime(String lastUpdateTime2) {
        this.lastUpdateTime = lastUpdateTime2;
    }

    public String getErrorMsg() {
        return this.errorMsg;
    }

    public void setErrorMsg(String errorMsg2) {
        this.errorMsg = errorMsg2;
    }
}
