package com.mobcent.android.model;

public class MCLibMobcentSoftwareInfo extends MCLibBaseModel {
    private static final long serialVersionUID = 5712524437155272270L;
    private int sid;
    private String softwareDownloadPath;
    private String softwareName;
    private int softwareType;

    public int getSid() {
        return this.sid;
    }

    public void setSid(int sid2) {
        this.sid = sid2;
    }

    public int getSoftwareType() {
        return this.softwareType;
    }

    public void setSoftwareType(int softwareType2) {
        this.softwareType = softwareType2;
    }

    public String getSoftwareName() {
        return this.softwareName;
    }

    public void setSoftwareName(String softwareName2) {
        this.softwareName = softwareName2;
    }

    public String getSoftwareDownloadPath() {
        return this.softwareDownloadPath;
    }

    public void setSoftwareDownloadPath(String softwareDownloadPath2) {
        this.softwareDownloadPath = softwareDownloadPath2;
    }
}
