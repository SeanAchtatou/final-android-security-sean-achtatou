package com.mobcent.android.model;

public class MCLibSyncSiteModel {
    private int OAUTHType;
    private String accessTokenEndpointUrl;
    private String appKey;
    private String appSecret;
    private String authorizationWebsiteUrl;
    private boolean isBind;
    private String requestTokenEndpointUrl;
    private int siteId;
    private String siteImage;
    private String siteName;

    public int getSiteId() {
        return this.siteId;
    }

    public void setSiteId(int siteId2) {
        this.siteId = siteId2;
    }

    public boolean isBind() {
        return this.isBind;
    }

    public void setBind(boolean isBind2) {
        this.isBind = isBind2;
    }

    public int getOAUTHType() {
        return this.OAUTHType;
    }

    public void setOAUTHType(int oAUTHType) {
        this.OAUTHType = oAUTHType;
    }

    public String getSiteName() {
        return this.siteName;
    }

    public void setSiteName(String siteName2) {
        this.siteName = siteName2;
    }

    public String getSiteImage() {
        return this.siteImage;
    }

    public void setSiteImage(String siteImage2) {
        this.siteImage = siteImage2;
    }

    public String getAppKey() {
        return this.appKey;
    }

    public void setAppKey(String appKey2) {
        this.appKey = appKey2;
    }

    public String getAppSecret() {
        return this.appSecret;
    }

    public void setAppSecret(String appSecret2) {
        this.appSecret = appSecret2;
    }

    public String getRequestTokenEndpointUrl() {
        return this.requestTokenEndpointUrl;
    }

    public void setRequestTokenEndpointUrl(String requestTokenEndpointUrl2) {
        this.requestTokenEndpointUrl = requestTokenEndpointUrl2;
    }

    public String getAccessTokenEndpointUrl() {
        return this.accessTokenEndpointUrl;
    }

    public void setAccessTokenEndpointUrl(String accessTokenEndpointUrl2) {
        this.accessTokenEndpointUrl = accessTokenEndpointUrl2;
    }

    public String getAuthorizationWebsiteUrl() {
        return this.authorizationWebsiteUrl;
    }

    public void setAuthorizationWebsiteUrl(String authorizationWebsiteUrl2) {
        this.authorizationWebsiteUrl = authorizationWebsiteUrl2;
    }
}
