package com.mobcent.android.model;

public class MCLibActionModel extends MCLibBaseModel {
    private static final long serialVersionUID = -4511907161757810408L;
    private int actionId;
    private String actionPrevImg;
    private String baseUrl;
    private String content;
    private int fromUserId;
    private String fromUserName;
    private String icon;
    private int id;
    private boolean isActionEnable;
    private String shortName;
    private String time;

    public int getActionId() {
        return this.actionId;
    }

    public void setActionId(int actionId2) {
        this.actionId = actionId2;
    }

    public String getShortName() {
        return this.shortName;
    }

    public void setShortName(String shortName2) {
        this.shortName = shortName2;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content2) {
        this.content = content2;
    }

    public String getBaseUrl() {
        return this.baseUrl;
    }

    public void setBaseUrl(String baseUrl2) {
        this.baseUrl = baseUrl2;
    }

    public String getIcon() {
        return this.icon;
    }

    public void setIcon(String icon2) {
        this.icon = icon2;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id2) {
        this.id = id2;
    }

    public int getFromUserId() {
        return this.fromUserId;
    }

    public void setFromUserId(int fromUserId2) {
        this.fromUserId = fromUserId2;
    }

    public String getFromUserName() {
        return this.fromUserName;
    }

    public void setFromUserName(String fromUserName2) {
        this.fromUserName = fromUserName2;
    }

    public String getTime() {
        return this.time;
    }

    public void setTime(String time2) {
        this.time = time2;
    }

    public boolean isActionEnable() {
        return this.isActionEnable;
    }

    public void setActionEnable(boolean isActionEnable2) {
        this.isActionEnable = isActionEnable2;
    }

    public String getActionPrevImg() {
        return this.actionPrevImg;
    }

    public void setActionPrevImg(String actionPrevImg2) {
        this.actionPrevImg = actionPrevImg2;
    }
}
