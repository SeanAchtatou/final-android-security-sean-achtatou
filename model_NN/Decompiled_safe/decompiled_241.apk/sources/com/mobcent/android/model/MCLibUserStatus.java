package com.mobcent.android.model;

public class MCLibUserStatus extends MCLibBaseModel {
    private static final long serialVersionUID = 6821634683534199679L;
    private int actionId;
    private int actionRecordId;
    private int belongUid;
    private int categoryId;
    private int communicationType;
    private String content;
    private String contentPathUrl;
    private String domainUrl;
    private int forwardNum;
    private int fromUserId;
    private String fromUserName;
    private String imgUrl;
    private boolean isDel;
    private int isRead;
    private boolean isSending;
    private int photoId;
    private String photoPath;
    private int replyNum;
    private MCLibUserStatus replyStatus;
    private long rootId;
    private int shareCategoryId;
    private int shareProId;
    private String shareUrl;
    private int sourceCategoryId;
    private String sourceName;
    private int sourceProId;
    private String sourceUrl;
    private long statusId;
    private int targetJumpId;
    private String targetJumpName;
    private String time;
    private int toAppId;
    private long toStatusId;
    private int toUid;
    private String toUserName;
    private int topicStatus;
    private String typeName;
    private int uid;
    private int userCommunicationQueueId;
    private String userImageUrl;
    private String userName;

    public int getUserCommunicationQueueId() {
        return this.userCommunicationQueueId;
    }

    public void setUserCommunicationQueueId(int userCommunicationQueueId2) {
        this.userCommunicationQueueId = userCommunicationQueueId2;
    }

    public int getUid() {
        return this.uid;
    }

    public void setUid(int uid2) {
        this.uid = uid2;
    }

    public int getToUid() {
        return this.toUid;
    }

    public void setToUid(int toUid2) {
        this.toUid = toUid2;
    }

    public long getToStatusId() {
        return this.toStatusId;
    }

    public void setToStatusId(long toStatusId2) {
        this.toStatusId = toStatusId2;
    }

    public int getToAppId() {
        return this.toAppId;
    }

    public void setToAppId(int toAppId2) {
        this.toAppId = toAppId2;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content2) {
        this.content = content2;
    }

    public int getCommunicationType() {
        return this.communicationType;
    }

    public void setCommunicationType(int communicationType2) {
        this.communicationType = communicationType2;
    }

    public int getActionRecordId() {
        return this.actionRecordId;
    }

    public void setActionRecordId(int actionRecordId2) {
        this.actionRecordId = actionRecordId2;
    }

    public int getActionId() {
        return this.actionId;
    }

    public void setActionId(int actionId2) {
        this.actionId = actionId2;
    }

    public long getStatusId() {
        return this.statusId;
    }

    public void setStatusId(long statusId2) {
        this.statusId = statusId2;
    }

    public String getToUserName() {
        return this.toUserName;
    }

    public void setToUserName(String toUserName2) {
        this.toUserName = toUserName2;
    }

    public String getUserName() {
        return this.userName;
    }

    public void setUserName(String userName2) {
        this.userName = userName2;
    }

    public String getTime() {
        return this.time;
    }

    public void setTime(String time2) {
        this.time = time2;
    }

    public int getFromUserId() {
        return this.fromUserId;
    }

    public void setFromUserId(int fromUserId2) {
        this.fromUserId = fromUserId2;
    }

    public String getFromUserName() {
        return this.fromUserName;
    }

    public void setFromUserName(String fromUserName2) {
        this.fromUserName = fromUserName2;
    }

    public int getReplyNum() {
        return this.replyNum;
    }

    public void setReplyNum(int replyNum2) {
        this.replyNum = replyNum2;
    }

    public int getForwardNum() {
        return this.forwardNum;
    }

    public void setForwardNum(int forwardNum2) {
        this.forwardNum = forwardNum2;
    }

    public String getUserImageUrl() {
        return this.userImageUrl;
    }

    public void setUserImageUrl(String userImageUrl2) {
        this.userImageUrl = userImageUrl2;
    }

    public int getTargetJumpId() {
        return this.targetJumpId;
    }

    public void setTargetJumpId(int targetJumpId2) {
        this.targetJumpId = targetJumpId2;
    }

    public String getTargetJumpName() {
        return this.targetJumpName;
    }

    public void setTargetJumpName(String targetJumpName2) {
        this.targetJumpName = targetJumpName2;
    }

    public long getRootId() {
        return this.rootId;
    }

    public void setRootId(long rootId2) {
        this.rootId = rootId2;
    }

    public int getIsRead() {
        return this.isRead;
    }

    public void setIsRead(int isRead2) {
        this.isRead = isRead2;
    }

    public MCLibUserStatus getReplyStatus() {
        return this.replyStatus;
    }

    public void setReplyStatus(MCLibUserStatus replyStatus2) {
        this.replyStatus = replyStatus2;
    }

    public String getTypeName() {
        return this.typeName;
    }

    public void setTypeName(String typeName2) {
        this.typeName = typeName2;
    }

    public String getDomainUrl() {
        return this.domainUrl;
    }

    public void setDomainUrl(String domainUrl2) {
        this.domainUrl = domainUrl2;
    }

    public int getBelongUid() {
        return this.belongUid;
    }

    public void setBelongUid(int belongUid2) {
        this.belongUid = belongUid2;
    }

    public String getSourceName() {
        return this.sourceName;
    }

    public void setSourceName(String sourceName2) {
        this.sourceName = sourceName2;
    }

    public String getSourceUrl() {
        return this.sourceUrl;
    }

    public void setSourceUrl(String sourceUrl2) {
        this.sourceUrl = sourceUrl2;
    }

    public int getSourceProId() {
        return this.sourceProId;
    }

    public void setSourceProId(int sourceProId2) {
        this.sourceProId = sourceProId2;
    }

    public int getSourceCategoryId() {
        return this.sourceCategoryId;
    }

    public void setSourceCategoryId(int sourceCategoryId2) {
        this.sourceCategoryId = sourceCategoryId2;
    }

    public String getShareUrl() {
        return this.shareUrl;
    }

    public void setShareUrl(String shareUrl2) {
        this.shareUrl = shareUrl2;
    }

    public int getShareCategoryId() {
        return this.shareCategoryId;
    }

    public void setShareCategoryId(int shareCategoryId2) {
        this.shareCategoryId = shareCategoryId2;
    }

    public int getShareProId() {
        return this.shareProId;
    }

    public void setShareProId(int shareProId2) {
        this.shareProId = shareProId2;
    }

    public int getCategoryId() {
        return this.categoryId;
    }

    public void setCategoryId(int categoryId2) {
        this.categoryId = categoryId2;
    }

    public int getPhotoId() {
        return this.photoId;
    }

    public void setPhotoId(int photoId2) {
        this.photoId = photoId2;
    }

    public String getPhotoPath() {
        return this.photoPath;
    }

    public void setPhotoPath(String photoPath2) {
        this.photoPath = photoPath2;
    }

    public String getImgUrl() {
        return this.imgUrl;
    }

    public void setImgUrl(String imgUrl2) {
        this.imgUrl = imgUrl2;
    }

    public int getTopicStatus() {
        return this.topicStatus;
    }

    public void setTopicStatus(int topicStatus2) {
        this.topicStatus = topicStatus2;
    }

    public boolean isSending() {
        return this.isSending;
    }

    public void setSending(boolean isSending2) {
        this.isSending = isSending2;
    }

    public boolean isDel() {
        return this.isDel;
    }

    public void setDel(boolean isDel2) {
        this.isDel = isDel2;
    }

    public String getContentPathUrl() {
        return this.contentPathUrl;
    }

    public void setContentPathUrl(String contentPathUrl2) {
        this.contentPathUrl = contentPathUrl2;
    }
}
