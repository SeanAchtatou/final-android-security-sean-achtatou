package com.mobcent.android.model;

public class MCLibAppStoreAppModel extends MCLibBaseModel {
    private static final long serialVersionUID = 7309469363362895059L;
    private int appId;
    private String appImageUrl;
    private String appName;
    private String baseUrl;
    private String category;
    private int categoryId;
    private int comments;
    private String description;
    private int developerId;
    private String developerName;
    private String domainUrl;
    private String downloadPath;
    private String downloadTimes;
    private String fee;
    private String language;
    private int parentCategoryId;
    private int priority;
    private String publishTime;
    private String rating;
    private String size;
    private String version;

    public int getAppId() {
        return this.appId;
    }

    public void setAppId(int appId2) {
        this.appId = appId2;
    }

    public String getAppName() {
        return this.appName;
    }

    public void setAppName(String appName2) {
        this.appName = appName2;
    }

    public String getCategory() {
        return this.category;
    }

    public void setCategory(String category2) {
        this.category = category2;
    }

    public String getFee() {
        return this.fee;
    }

    public void setFee(String fee2) {
        this.fee = fee2;
    }

    public String getSize() {
        return this.size;
    }

    public void setSize(String size2) {
        this.size = size2;
    }

    public String getRating() {
        return this.rating;
    }

    public void setRating(String rating2) {
        this.rating = rating2;
    }

    public int getComments() {
        return this.comments;
    }

    public void setComments(int comments2) {
        this.comments = comments2;
    }

    public String getAppImageUrl() {
        return this.appImageUrl;
    }

    public void setAppImageUrl(String appImageUrl2) {
        this.appImageUrl = appImageUrl2;
    }

    public String getVersion() {
        return this.version;
    }

    public void setVersion(String version2) {
        this.version = version2;
    }

    public String getLanguage() {
        return this.language;
    }

    public void setLanguage(String language2) {
        this.language = language2;
    }

    public String getPublishTime() {
        return this.publishTime;
    }

    public void setPublishTime(String publishTime2) {
        this.publishTime = publishTime2;
    }

    public String getDownloadTimes() {
        return this.downloadTimes;
    }

    public void setDownloadTimes(String downloadTimes2) {
        this.downloadTimes = downloadTimes2;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description2) {
        this.description = description2;
    }

    public int getCategoryId() {
        return this.categoryId;
    }

    public void setCategoryId(int categoryId2) {
        this.categoryId = categoryId2;
    }

    public int getParentCategoryId() {
        return this.parentCategoryId;
    }

    public void setParentCategoryId(int parentCategoryId2) {
        this.parentCategoryId = parentCategoryId2;
    }

    public int getDeveloperId() {
        return this.developerId;
    }

    public void setDeveloperId(int developerId2) {
        this.developerId = developerId2;
    }

    public String getDeveloperName() {
        return this.developerName;
    }

    public void setDeveloperName(String developerName2) {
        this.developerName = developerName2;
    }

    public String getDownloadPath() {
        return this.downloadPath;
    }

    public void setDownloadPath(String downloadPath2) {
        this.downloadPath = downloadPath2;
    }

    public String getBaseUrl() {
        return this.baseUrl;
    }

    public void setBaseUrl(String baseUrl2) {
        this.baseUrl = baseUrl2;
    }

    public String getDomainUrl() {
        return this.domainUrl;
    }

    public void setDomainUrl(String domainUrl2) {
        this.domainUrl = domainUrl2;
    }

    public int getPriority() {
        return this.priority;
    }

    public void setPriority(int priority2) {
        this.priority = priority2;
    }
}
