package com.mobcent.android.model;

public class MCLibSysMsgModel extends MCLibBaseModel {
    private static final long serialVersionUID = 8484797056274960524L;
    private String content;
    private int fromId;
    private String fromName;
    private long sysMsgId;
    private int sysMsgType;
    private int targetId;

    public int getTargetId() {
        return this.targetId;
    }

    public void setTargetId(int targetId2) {
        this.targetId = targetId2;
    }

    public int getFromId() {
        return this.fromId;
    }

    public void setFromId(int fromId2) {
        this.fromId = fromId2;
    }

    public int getSysMsgType() {
        return this.sysMsgType;
    }

    public void setSysMsgType(int sysMsgType2) {
        this.sysMsgType = sysMsgType2;
    }

    public long getSysMsgId() {
        return this.sysMsgId;
    }

    public void setSysMsgId(long sysMsgId2) {
        this.sysMsgId = sysMsgId2;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content2) {
        this.content = content2;
    }

    public String getFromName() {
        return this.fromName;
    }

    public void setFromName(String fromName2) {
        this.fromName = fromName2;
    }
}
