package com.mobcent.android.model;

public class MCLibDownloadProfileModel extends MCLibBaseModel {
    private static final long serialVersionUID = -6218132148559674170L;
    private int appId;
    private String appName;
    private int appType;
    private int clientId;
    private int downloadSize;
    private long downloadTime;
    private int fileSize;
    private int speed;
    private int status;
    private int stopReq;
    private String url;
    private String version;

    public int getAppId() {
        return this.appId;
    }

    public void setAppId(int appId2) {
        this.appId = appId2;
    }

    public int getFileSize() {
        return this.fileSize;
    }

    public void setFileSize(int fileSize2) {
        this.fileSize = fileSize2;
    }

    public int getDownloadSize() {
        return this.downloadSize;
    }

    public void setDownloadSize(int downloadSize2) {
        this.downloadSize = downloadSize2;
    }

    public String getAppName() {
        return this.appName;
    }

    public void setAppName(String appName2) {
        this.appName = appName2;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url2) {
        this.url = url2;
    }

    public int getSpeed() {
        return this.speed;
    }

    public void setSpeed(int speed2) {
        this.speed = speed2;
    }

    public int getStatus() {
        return this.status;
    }

    public void setStatus(int status2) {
        this.status = status2;
    }

    public int getStopReq() {
        return this.stopReq;
    }

    public void setStopReq(int stopReq2) {
        this.stopReq = stopReq2;
    }

    public int getAppType() {
        return this.appType;
    }

    public void setAppType(int appType2) {
        this.appType = appType2;
    }

    public int getClientId() {
        return this.clientId;
    }

    public void setClientId(int clientId2) {
        this.clientId = clientId2;
    }

    public String getVersion() {
        return this.version;
    }

    public void setVersion(String version2) {
        this.version = version2;
    }

    public long getDownloadTime() {
        return this.downloadTime;
    }

    public void setDownloadTime(long downloadTime2) {
        this.downloadTime = downloadTime2;
    }
}
