package com.mobcent.android.model;

import java.util.List;

public class MCLibHeartBeatModel extends MCLibBaseModel {
    private static final long serialVersionUID = -2628851373838019165L;
    private List<MCLibUserStatus> actionList;
    private boolean hasReply;
    private boolean hasUpdates;
    private int interval;
    private List<MCLibUserStatus> msgList;
    private int requestAppState;
    private List<MCLibSysMsgModel> sysMsgList;

    public int getInterval() {
        return this.interval;
    }

    public void setInterval(int interval2) {
        this.interval = interval2;
    }

    public List<MCLibUserStatus> getMsgList() {
        return this.msgList;
    }

    public void setMsgList(List<MCLibUserStatus> msgList2) {
        this.msgList = msgList2;
    }

    public List<MCLibUserStatus> getActionList() {
        return this.actionList;
    }

    public void setActionList(List<MCLibUserStatus> actionList2) {
        this.actionList = actionList2;
    }

    public int getRequestAppState() {
        return this.requestAppState;
    }

    public void setRequestAppState(int requestAppState2) {
        this.requestAppState = requestAppState2;
    }

    public boolean isHasReply() {
        return this.hasReply;
    }

    public void setHasReply(boolean hasReply2) {
        this.hasReply = hasReply2;
    }

    public boolean isHasUpdates() {
        return this.hasUpdates;
    }

    public void setHasUpdates(boolean hasUpdates2) {
        this.hasUpdates = hasUpdates2;
    }

    public List<MCLibSysMsgModel> getSysMsgList() {
        return this.sysMsgList;
    }

    public void setSysMsgList(List<MCLibSysMsgModel> sysMsgList2) {
        this.sysMsgList = sysMsgList2;
    }
}
