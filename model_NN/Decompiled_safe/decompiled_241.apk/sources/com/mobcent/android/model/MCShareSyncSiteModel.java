package com.mobcent.android.model;

public class MCShareSyncSiteModel {
    private int siteId;
    private String siteName;

    public int getSiteId() {
        return this.siteId;
    }

    public void setSiteId(int siteId2) {
        this.siteId = siteId2;
    }

    public String getSiteName() {
        return this.siteName;
    }

    public void setSiteName(String siteName2) {
        this.siteName = siteName2;
    }
}
