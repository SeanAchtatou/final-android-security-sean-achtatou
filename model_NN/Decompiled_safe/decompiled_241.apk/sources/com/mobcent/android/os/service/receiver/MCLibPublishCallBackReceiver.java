package com.mobcent.android.os.service.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;
import com.mobcent.android.service.delegate.MCLibMsgBundleDelegate;

public class MCLibPublishCallBackReceiver extends BroadcastReceiver {
    private MCLibMsgBundleDelegate delegate;

    public MCLibPublishCallBackReceiver(MCLibMsgBundleDelegate delegate2) {
        this.delegate = delegate2;
    }

    public void onReceive(Context context, Intent intent) {
        String result = intent.getStringExtra("rs");
        if (result.equals("publish_succ")) {
            Toast.makeText(context, this.delegate.getPublishSuccMsg(), 1).show();
        } else if (result.equals("prohibitPublish")) {
            Toast.makeText(context, this.delegate.getPublishProhibitionMsg(), 1).show();
        } else {
            Toast.makeText(context, this.delegate.getPublishFailMsg(), 1).show();
        }
    }
}
