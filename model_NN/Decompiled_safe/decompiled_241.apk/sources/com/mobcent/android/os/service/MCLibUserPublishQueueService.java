package com.mobcent.android.os.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.Looper;
import com.mobcent.android.constants.MCLibMobCentApiConstant;
import com.mobcent.android.db.UserCommunicationQueueDBUtil;
import com.mobcent.android.model.MCLibUserStatus;
import com.mobcent.android.service.impl.MCLibCommonServiceImpl;
import com.mobcent.android.service.impl.MCLibStatusServiceImpl;
import com.mobcent.android.service.impl.MCLibUserInfoServiceImpl;
import java.util.List;

public class MCLibUserPublishQueueService extends Service implements MCLibMobCentApiConstant {
    public static final String PUBLISH_SERVICE_MSG = "com.mobcent.service.publish.msg";
    /* access modifiers changed from: private */
    public Thread publishContentThread;

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public void onStart(Intent intent, int startid) {
        if (this.publishContentThread == null) {
            this.publishContentThread = new Thread() {
                public void run() {
                    Looper.prepare();
                    MCLibUserStatus userStatus = MCLibUserPublishQueueService.this.getQueueTopUserStatus();
                    while (userStatus != null) {
                        try {
                            MCLibUserPublishQueueService.this.publishStatus(userStatus);
                            userStatus = MCLibUserPublishQueueService.this.getQueueTopUserStatus();
                            sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    MCLibUserPublishQueueService.this.publishContentThread.interrupt();
                    MCLibUserPublishQueueService.this.publishContentThread = null;
                }
            };
            this.publishContentThread.start();
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    public MCLibUserStatus getQueueTopUserStatus() {
        List<MCLibUserStatus> userStatus = UserCommunicationQueueDBUtil.getInstance(this).getUserStatusList(1);
        if (userStatus.isEmpty()) {
            return null;
        }
        return userStatus.get(0);
    }

    public void publishStatus(MCLibUserStatus userStatus) {
        if (userStatus != null) {
            String result = "";
            if (1 == userStatus.getCommunicationType()) {
                result = new MCLibStatusServiceImpl(this).publishUserStatus(userStatus.getUid(), userStatus.getContent(), userStatus.getPhotoId(), userStatus.getPhotoPath());
                if (result != null && result.equals("rs_succ")) {
                    UserCommunicationQueueDBUtil.getInstance(this).removeUserStatusInQueue(userStatus.getUserCommunicationQueueId());
                }
            } else if (11 == userStatus.getCommunicationType()) {
                result = new MCLibStatusServiceImpl(this).modifyMyMood(userStatus.getUid(), userStatus.getContent());
                if (result != null && result.equals("rs_succ")) {
                    UserCommunicationQueueDBUtil.getInstance(this).removeUserStatusInQueue(userStatus.getUserCommunicationQueueId());
                    new MCLibUserInfoServiceImpl(this).updateShareUserMood(userStatus.getContent());
                }
            } else if (10 == userStatus.getCommunicationType()) {
                result = new MCLibStatusServiceImpl(this).talkToUser(userStatus.getUid(), userStatus.getContent(), userStatus.getToUid());
                if (result != null && result.equals("rs_succ")) {
                    UserCommunicationQueueDBUtil.getInstance(this).removeUserStatusInQueue(userStatus.getUserCommunicationQueueId());
                }
            } else if (9 == userStatus.getCommunicationType()) {
                result = new MCLibStatusServiceImpl(this).sendMsg(userStatus.getUid(), userStatus.getContent(), userStatus.getToUid(), userStatus.getToUserName(), userStatus.getActionId());
                if (result != null && result.equals("rs_succ")) {
                    UserCommunicationQueueDBUtil.getInstance(this).removeUserStatusInQueue(userStatus.getUserCommunicationQueueId());
                }
            } else if (2 == userStatus.getCommunicationType()) {
                result = new MCLibStatusServiceImpl(this).replyToUserStatus(userStatus.getUid(), userStatus.getContent(), (long) userStatus.getToUid(), userStatus.getToStatusId(), userStatus.getPhotoId(), userStatus.getPhotoPath());
                if (result != null && result.equals("rs_succ")) {
                    UserCommunicationQueueDBUtil.getInstance(this).removeUserStatusInQueue(userStatus.getUserCommunicationQueueId());
                }
            } else if (15 == userStatus.getCommunicationType() && (result = new MCLibCommonServiceImpl(this).sendUserFeedback(userStatus.getUid(), userStatus.getContent())) != null && result.equals("rs_succ")) {
                UserCommunicationQueueDBUtil.getInstance(this).removeUserStatusInQueue(userStatus.getUserCommunicationQueueId());
            }
            if (result != null && result.equals("rs_succ")) {
                notifyPublishStatus("publish_succ", userStatus.getUserCommunicationQueueId());
            } else if (result == null || result.equals("rs_succ")) {
                notifyPublishStatus("publish_fail", userStatus.getUserCommunicationQueueId());
                UserCommunicationQueueDBUtil.getInstance(this).removeUserStatusInQueue(userStatus.getUserCommunicationQueueId());
            } else {
                notifyPublishStatus(result, userStatus.getUserCommunicationQueueId());
                UserCommunicationQueueDBUtil.getInstance(this).removeUserStatusInQueue(userStatus.getUserCommunicationQueueId());
            }
        }
    }

    private void notifyPublishStatus(String rs, int queueId) {
        Intent intent = new Intent(PUBLISH_SERVICE_MSG);
        intent.putExtra("rs", rs);
        sendBroadcast(intent);
    }
}
