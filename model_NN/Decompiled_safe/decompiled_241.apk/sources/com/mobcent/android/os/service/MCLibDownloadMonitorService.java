package com.mobcent.android.os.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.Looper;
import com.mobcent.android.db.DownloadAppDBUtil;
import com.mobcent.android.model.MCLibDownloadProfileModel;
import com.mobcent.android.service.impl.MCLibAppDownloaderServiceImpl;
import java.util.List;

public class MCLibDownloadMonitorService extends Service {
    public static final String DOWNLOAD_SERVICE_MSG = "com.mobcent.service.download.msg";
    private Thread downloadMonitorThread;
    private boolean isStopMonitor = false;

    public boolean isStopMonitor() {
        return this.isStopMonitor;
    }

    public void setStopMonitor(boolean isStopMonitor2) {
        this.isStopMonitor = isStopMonitor2;
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
    }

    public void onDestroy() {
        setStopMonitor(true);
        this.downloadMonitorThread.interrupt();
        super.onDestroy();
    }

    public void onStart(Intent intent, int startid) {
        if (this.downloadMonitorThread == null) {
            this.downloadMonitorThread = new Thread() {
                public void run() {
                    Looper.prepare();
                    while (!MCLibDownloadMonitorService.this.isStopMonitor()) {
                        try {
                            MCLibDownloadMonitorService.this.checkStatus();
                            sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            };
            this.downloadMonitorThread.start();
        }
    }

    public void checkStatus() {
        DownloadAppDBUtil util = DownloadAppDBUtil.getInstance(this);
        List<MCLibDownloadProfileModel> downloadingModels = util.getDownloadProfileByStatus(1);
        List<MCLibDownloadProfileModel> downloadWaitingModels = util.getDownloadProfileByStatus(0);
        MCLibDownloadProfileModel model = null;
        if (downloadingModels != null && !downloadingModels.isEmpty()) {
            MCLibDownloadProfileModel model2 = downloadingModels.get(0);
            notifyDownloadPanel();
        } else if (downloadWaitingModels == null || downloadWaitingModels.isEmpty()) {
            notifyDownloadPanel();
            stopService(new Intent(this, MCLibDownloadMonitorService.class));
        } else {
            int i = 0;
            while (true) {
                if (i >= downloadWaitingModels.size()) {
                    break;
                }
                MCLibDownloadProfileModel modelTemp = downloadWaitingModels.get(i);
                if (modelTemp.getStopReq() != 1) {
                    model = modelTemp;
                    break;
                }
                i++;
            }
            if (model != null) {
                model.setStatus(1);
                util.addOrUpdateAppDownloadProfile(model, true);
                new MCLibAppDownloaderServiceImpl(model, this).doDownloadApp();
                notifyDownloadPanel();
                return;
            }
            notifyDownloadPanel();
            stopService(new Intent(this, MCLibDownloadMonitorService.class));
        }
    }

    private void notifyDownloadPanel() {
        sendBroadcast(new Intent(DOWNLOAD_SERVICE_MSG));
    }
}
