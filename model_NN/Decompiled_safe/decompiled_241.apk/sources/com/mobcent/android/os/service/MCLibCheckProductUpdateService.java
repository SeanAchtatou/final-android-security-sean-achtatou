package com.mobcent.android.os.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import com.mobcent.android.db.UserMagicActionDictDBUtil;
import com.mobcent.android.model.MCLibActionModel;
import com.mobcent.android.service.MCLibUserActionService;
import com.mobcent.android.service.impl.MCLibUserActionServiceImpl;
import com.mobcent.android.service.impl.MCLibUserInfoServiceImpl;
import java.util.List;

public class MCLibCheckProductUpdateService extends Service {
    /* access modifiers changed from: private */
    public int userId;

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        new Thread() {
            public void run() {
                MCLibCheckProductUpdateService.this.userId = new MCLibUserInfoServiceImpl(MCLibCheckProductUpdateService.this).getLoginUserId();
                MCLibCheckProductUpdateService.this.updateMagicActions();
                MCLibCheckProductUpdateService.this.stopService(new Intent(MCLibCheckProductUpdateService.this, MCLibCheckProductUpdateService.class));
            }
        }.start();
    }

    /* access modifiers changed from: private */
    public void updateMagicActions() {
        MCLibUserActionService userActionService = new MCLibUserActionServiceImpl(this);
        String lastUpateTime = UserMagicActionDictDBUtil.getInstance(this).getMagicActionTypeTime();
        List<MCLibActionModel> magicActionModel = userActionService.getLocalMagicActions(this, 1);
        if (magicActionModel == null || magicActionModel.isEmpty()) {
            lastUpateTime = "";
        }
        List<MCLibActionModel> newMagicActionModels = userActionService.getRemoteUpdateMagicActionType(this.userId, lastUpateTime);
        if (newMagicActionModels != null && !newMagicActionModels.isEmpty()) {
            userActionService.updateMagicActionType(this, newMagicActionModels);
        }
    }
}
