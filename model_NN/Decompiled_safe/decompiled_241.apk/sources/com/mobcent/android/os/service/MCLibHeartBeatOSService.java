package com.mobcent.android.os.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.Looper;
import com.mobcent.android.constants.MCLibConstants;
import com.mobcent.android.constants.MCLibParameterKeyConstant;
import com.mobcent.android.db.UserMagicActionDictDBUtil;
import com.mobcent.android.model.MCLibHeartBeatModel;
import com.mobcent.android.model.MCLibSysMsgModel;
import com.mobcent.android.model.MCLibUserStatus;
import com.mobcent.android.service.impl.MCLibHeartBeatServiceImpl;
import com.mobcent.android.service.impl.MCLibMediaServiceImpl;
import com.mobcent.android.service.impl.MCLibUserInfoServiceImpl;
import com.mobcent.android.service.impl.MCLibUserPrivateMsgServiceImpl;
import com.mobcent.android.state.MCLibAppState;
import com.mobcent.android.utils.PhoneConnectionUtil;
import com.mobcent.android.utils.PhoneVibratorUtil;
import java.util.ArrayList;
import java.util.List;

public class MCLibHeartBeatOSService extends Service {
    public static int CHECK_LOCAL_HEART_BEAT_INTERVAL = 4000;
    public static String HAS_NEW_REPLY = "HAS_NEW_REPLY";
    public static int HEART_BEAT_INTERVAL = MCLibConstants.RECONNECTION_INTERVAL;
    public static String NEW_MSG_COUNT = "NEW_MSG_COUNT";
    public static String NEW_SYS_MSG_COUNT = "NEW_SYS_MSG_COUNT";
    public static final String NOTIFY_CHAT_UPDATE = ".lib.service.chat.update.user.";
    public static final String SERVER_NOTIFICATION_MSG = ".lib.service.msg.notify";
    public static boolean hasReply = false;
    public static List<MCLibSysMsgModel> sysMsgList = new ArrayList();
    private Thread checkLocalHeartBeatThread;
    private Thread heartBeatThread;
    private boolean isStopMonitorServer = false;
    private int msgAudioRid = 0;

    public boolean isStopMonitorServer() {
        return this.isStopMonitorServer;
    }

    public void setStopMonitorServer(boolean isStopMonitorServer2) {
        this.isStopMonitorServer = isStopMonitorServer2;
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        this.heartBeatThread = new Thread() {
            public void run() {
                Looper.prepare();
                while (!MCLibHeartBeatOSService.this.isStopMonitorServer()) {
                    try {
                        if (PhoneConnectionUtil.isNetworkAvailable(MCLibHeartBeatOSService.this)) {
                            if (MCLibAppState.serverHeartBeatReq == MCLibAppState.APP_OFF_LINE) {
                                MCLibAppState.sessionId = "";
                                MCLibAppState.isValidLogin = false;
                            } else if (MCLibAppState.isActive) {
                                MCLibHeartBeatOSService.this.sendHeartBeat();
                            } else if (MCLibAppState.serverHeartBeatReq == MCLibAppState.APP_RUNNING) {
                                MCLibHeartBeatOSService.this.sendHeartBeat();
                            } else if (MCLibAppState.serverHeartBeatReq != MCLibAppState.APP_SLEEP && !MCLibAppState.isActive) {
                                MCLibHeartBeatOSService.this.sendHeartBeat();
                            }
                        }
                        sleep((long) MCLibHeartBeatOSService.HEART_BEAT_INTERVAL);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        this.checkLocalHeartBeatThread = new Thread() {
            public void run() {
                Looper.prepare();
                while (!MCLibHeartBeatOSService.this.isStopMonitorServer()) {
                    try {
                        sleep((long) MCLibHeartBeatOSService.CHECK_LOCAL_HEART_BEAT_INTERVAL);
                        MCLibHeartBeatOSService.this.checkLocalInfo();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
    }

    public void onDestroy() {
        setStopMonitorServer(true);
        this.heartBeatThread.interrupt();
        this.checkLocalHeartBeatThread.interrupt();
    }

    public void onStart(Intent intent, int startId) {
        if (intent != null) {
            this.msgAudioRid = intent.getIntExtra(MCLibParameterKeyConstant.MSG_AUDIO_RID, 0);
        }
        if (!this.heartBeatThread.isAlive()) {
            this.heartBeatThread.start();
        }
        if (!this.checkLocalHeartBeatThread.isAlive()) {
            this.checkLocalHeartBeatThread.start();
        }
    }

    /* access modifiers changed from: private */
    public void sendHeartBeat() {
        MCLibHeartBeatModel heartBeatModel = new MCLibHeartBeatServiceImpl().updateHeartBeatInfo(this, new MCLibUserInfoServiceImpl(this).getLoginUserId(), MCLibAppState.isActive);
        if (heartBeatModel.isHasUpdates()) {
            boolean isSoundEnable = UserMagicActionDictDBUtil.getInstance(this).isSoundEnable();
            if (this.msgAudioRid != 0 && isSoundEnable) {
                new MCLibMediaServiceImpl().playAudio(this, this.msgAudioRid);
            }
            if (heartBeatModel.getSysMsgList() != null && heartBeatModel.getSysMsgList().size() > 0) {
                sysMsgList.addAll(heartBeatModel.getSysMsgList());
            }
            PhoneVibratorUtil.shotVibratePhone(this);
            checkLocalInfo();
            notifyChatRoomUpdate(heartBeatModel);
        }
        MCLibAppState.serverHeartBeatReq = heartBeatModel.getRequestAppState();
    }

    /* access modifiers changed from: private */
    public void checkLocalInfo() {
        broadcastToActivities(new MCLibUserPrivateMsgServiceImpl(this).getUserUnreadMsgCount(this, new MCLibUserInfoServiceImpl(this).getLoginUserId()));
    }

    private void notifyChatRoomUpdate(MCLibHeartBeatModel heartBeatModel) {
        List<Integer> userIdList = new ArrayList<>();
        for (MCLibUserStatus userStatus : heartBeatModel.getMsgList()) {
            if (!isUserInList(userStatus.getFromUserId(), userIdList)) {
                userIdList.add(Integer.valueOf(userStatus.getFromUserId()));
            }
        }
        for (MCLibUserStatus userStatus2 : heartBeatModel.getActionList()) {
            if (!isUserInList(userStatus2.getFromUserId(), userIdList)) {
                userIdList.add(Integer.valueOf(userStatus2.getFromUserId()));
            }
        }
        for (Integer userId : userIdList) {
            sendBroadcast(new Intent(String.valueOf(getPackageName()) + NOTIFY_CHAT_UPDATE + userId));
        }
    }

    private boolean isUserInList(int uid, List<Integer> userIdList) {
        for (Integer userId : userIdList) {
            if (uid == userId.intValue()) {
                return true;
            }
        }
        return false;
    }

    private void broadcastToActivities(int msgCount) {
        Intent intent = new Intent(String.valueOf(getPackageName()) + SERVER_NOTIFICATION_MSG);
        intent.putExtra(NEW_MSG_COUNT, msgCount);
        intent.putExtra(HAS_NEW_REPLY, hasReply);
        intent.putExtra(NEW_SYS_MSG_COUNT, sysMsgList == null ? 0 : sysMsgList.size());
        sendBroadcast(intent);
    }
}
