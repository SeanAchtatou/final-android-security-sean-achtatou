package com.mobcent.android.os.service.helper;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import com.mobcent.android.constants.MCLibParameterKeyConstant;
import com.mobcent.android.db.UserMagicActionDictDBUtil;
import com.mobcent.android.os.service.MCLibCheckProductUpdateService;
import com.mobcent.android.os.service.MCLibDownloadMonitorService;
import com.mobcent.android.os.service.MCLibHeartBeatOSService;
import com.mobcent.android.os.service.MCLibUserPublishQueueService;
import com.mobcent.android.state.MCLibAppState;

public class MCLibDeveloperServiceHelper {
    public static void onAppLaunched(int audioMsgRid, Context context) {
        context.startService(new Intent(context, MCLibUserPublishQueueService.class));
        context.startService(new Intent(context, MCLibCheckProductUpdateService.class));
        Intent intent = new Intent(context, MCLibHeartBeatOSService.class);
        intent.putExtra(MCLibParameterKeyConstant.MSG_AUDIO_RID, audioMsgRid);
        context.startService(intent);
    }

    public static void updateAppKey(String appKey, Context context) {
        UserMagicActionDictDBUtil.getInstance(context).updateAppKey(appKey);
    }

    public static void onExit(Context context) {
        MCLibAppState.sessionId = "";
        MCLibAppState.isValidLogin = false;
        context.stopService(new Intent(context, MCLibDownloadMonitorService.class));
        context.stopService(new Intent(context, MCLibHeartBeatOSService.class));
        context.stopService(new Intent(context, MCLibUserPublishQueueService.class));
        if (MCLibAppState.getActivityStack() != null && !MCLibAppState.getActivityStack().isEmpty()) {
            for (Activity activity : MCLibAppState.getActivityStack()) {
                if (activity != null) {
                    activity.finish();
                }
            }
            MCLibAppState.getActivityStack().clear();
        }
    }

    public static void closeCommunityActivities() {
        if (MCLibAppState.getActivityStack() != null && !MCLibAppState.getActivityStack().isEmpty()) {
            for (Activity activity : MCLibAppState.getActivityStack()) {
                if (activity != null) {
                    activity.finish();
                }
            }
            MCLibAppState.getActivityStack().clear();
        }
    }
}
