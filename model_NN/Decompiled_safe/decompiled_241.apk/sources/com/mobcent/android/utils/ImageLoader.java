package com.mobcent.android.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.mobcent.android.service.impl.MCLibFileTransferServiceImpl;

public class ImageLoader {
    public static final String LOCAL_POSITION_DIR = (String.valueOf(MCLibIOUtil.FS) + "mobcent" + MCLibIOUtil.FS + "imageCache" + MCLibIOUtil.FS);
    private static ImageLoader instance;
    private Context activity;

    private ImageLoader(Context activity2) {
        this.activity = activity2;
    }

    public static ImageLoader getInstance(Context activity2) {
        if (instance == null) {
            instance = new ImageLoader(activity2);
        }
        return instance;
    }

    public Bitmap loadBitmap(String urlString, String size, boolean isForceGetByHttp) {
        if (urlString == null || size == null || urlString.equals("")) {
            return null;
        }
        if (urlString.indexOf("xgsize") > -1) {
            urlString = urlString.replace("xgsize", size);
        }
        String baseLocation = MCLibIOUtil.getBaseLocalLocation(this.activity);
        String imagePath = String.valueOf(baseLocation) + LOCAL_POSITION_DIR;
        if (!MCLibIOUtil.isDirExist(imagePath) && !MCLibIOUtil.makeDirs(imagePath)) {
            return new MCLibFileTransferServiceImpl(this.activity).getBitmapFromUrl(urlString);
        }
        String fileName = getPathName(urlString);
        if (!MCLibIOUtil.isFileExist(String.valueOf(imagePath) + fileName) || isForceGetByHttp) {
            Bitmap bitmap = new MCLibFileTransferServiceImpl(this.activity).getBitmapFromUrl(urlString);
            if (bitmap != null) {
                MCLibIOUtil.saveImageFile(this.activity, bitmap, getImageType(fileName.substring(fileName.lastIndexOf(".") + 1)), String.valueOf(LOCAL_POSITION_DIR) + fileName, baseLocation);
            }
            return bitmap;
        }
        return BitmapFactory.decodeFile(String.valueOf(imagePath) + fileName, new BitmapFactory.Options());
    }

    public static String getPathName(String url) {
        return url.substring(url.lastIndexOf("/") + 1);
    }

    public Bitmap.CompressFormat getImageType(String imageType) {
        if (imageType.equalsIgnoreCase("png")) {
            return Bitmap.CompressFormat.PNG;
        }
        if (imageType.equalsIgnoreCase("jpg")) {
            return Bitmap.CompressFormat.JPEG;
        }
        return Bitmap.CompressFormat.PNG;
    }

    public static boolean downloadGif(String urlString, String size, Context context) {
        if (urlString == null || size == null || urlString.equals("")) {
            return false;
        }
        if (urlString.indexOf("xgsize") > -1) {
            urlString = urlString.replace("xgsize", size);
        }
        String baseLocation = MCLibIOUtil.getSDCardPath();
        if (baseLocation == null) {
            return false;
        }
        String imagePath = String.valueOf(baseLocation) + LOCAL_POSITION_DIR;
        if (!MCLibIOUtil.isDirExist(imagePath) && !MCLibIOUtil.makeDirs(imagePath)) {
            return false;
        }
        String fileName = getPathName(urlString);
        if (MCLibIOUtil.isFileExist(String.valueOf(imagePath) + fileName)) {
            return true;
        }
        return MCLibIOUtil.saveFile(new MCLibFileTransferServiceImpl(context).getImageStream(urlString), String.valueOf(LOCAL_POSITION_DIR) + fileName, baseLocation);
    }

    public static String getImageCachePath(String icon) {
        String baseLocation = MCLibIOUtil.getSDCardPath();
        if (baseLocation == null) {
            return null;
        }
        String imagePath = String.valueOf(baseLocation) + LOCAL_POSITION_DIR;
        if (!MCLibIOUtil.isDirExist(imagePath)) {
            return null;
        }
        if (MCLibIOUtil.isFileExist(String.valueOf(imagePath) + icon)) {
            return String.valueOf(imagePath) + icon;
        }
        return null;
    }

    public static String getImageCacheBasePath() {
        String baseLocation = MCLibIOUtil.getSDCardPath();
        if (baseLocation == null) {
            return null;
        }
        return String.valueOf(baseLocation) + LOCAL_POSITION_DIR;
    }

    public static Bitmap getBitmap(String filePath, int sampleSize) {
        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
        bitmapOptions.inSampleSize = sampleSize;
        return BitmapFactory.decodeFile(filePath, bitmapOptions);
    }
}
