package com.mobcent.android.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {
    public static Date getTimestampByTimeString(String time) {
        try {
            return new SimpleDateFormat("yyyy/MM/dd HH:mm").parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getGenericCurrentTime() {
        return new SimpleDateFormat("yyyy/MM/dd HH:mm").format(Calendar.getInstance().getTime());
    }

    public static String getFormatTime(long time) {
        return new SimpleDateFormat("yyyy/MM/dd HH:mm").format(Long.valueOf(time));
    }
}
