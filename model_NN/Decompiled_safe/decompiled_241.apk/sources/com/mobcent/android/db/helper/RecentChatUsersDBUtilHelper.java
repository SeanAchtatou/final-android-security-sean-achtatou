package com.mobcent.android.db.helper;

import android.database.Cursor;
import com.mobcent.android.model.MCLibUserInfo;

public class RecentChatUsersDBUtilHelper {
    public static MCLibUserInfo buildRecentChatUserModel(Cursor c) {
        MCLibUserInfo userInfo = new MCLibUserInfo();
        userInfo.setUid(c.getInt(0));
        userInfo.setName(c.getString(1));
        userInfo.setNickName(c.getString(2));
        userInfo.setImage(c.getString(3));
        userInfo.setUnreadMsgCount(c.getInt(4));
        userInfo.setLastMsgTime(c.getLong(5));
        userInfo.setSourceProId(c.getInt(6));
        userInfo.setSourceName(c.getString(7));
        userInfo.setSourceUrl(c.getString(8));
        return userInfo;
    }
}
