package com.mobcent.android.db.constants;

public interface BaseDBConstant {
    public static final String COLUMN_TIME = "time";
    public static final String COLUMN_TIME_ID = "timeId";
    public static final String TABLE_LAST_UPDATE_TIME = "LastUpdateTime";
}
