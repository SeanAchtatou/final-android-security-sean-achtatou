package com.mobcent.android.db.helper;

import android.database.Cursor;
import com.mobcent.android.model.MCLibUserStatus;

public class PrivateMsgDBUtilHelper {
    public static MCLibUserStatus buildPrivateMsgModel(Cursor c, int totalNum) {
        MCLibUserStatus userStatus = new MCLibUserStatus();
        userStatus.setStatusId(c.getLong(0));
        userStatus.setUid(c.getInt(1));
        userStatus.setContent(c.getString(2));
        userStatus.setFromUserId(c.getInt(3));
        userStatus.setFromUserName(c.getString(4));
        userStatus.setUserImageUrl(c.getString(5));
        userStatus.setTime(new StringBuilder(String.valueOf(c.getLong(6))).toString());
        userStatus.setBelongUid(c.getInt(7));
        userStatus.setActionId(c.getInt(8));
        userStatus.setIsRead(c.getInt(9));
        userStatus.setTotalNum(totalNum);
        return userStatus;
    }
}
