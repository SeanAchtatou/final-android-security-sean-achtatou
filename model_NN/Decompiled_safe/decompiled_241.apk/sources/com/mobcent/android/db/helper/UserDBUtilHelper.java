package com.mobcent.android.db.helper;

import android.database.Cursor;
import com.mobcent.android.model.MCLibUserInfo;

public class UserDBUtilHelper {
    public static MCLibUserInfo buildUserInfoModel(Cursor c, int totalNum, int page) {
        boolean z;
        MCLibUserInfo model = new MCLibUserInfo();
        model.setUid(c.getInt(0));
        model.setName(c.getString(1));
        model.setGender(c.getString(2));
        model.setAge(c.getString(3));
        model.setPos(c.getString(4));
        model.setImage(c.getString(5));
        model.setEmotionWords(c.getString(6));
        model.setFollowed(c.getInt(7) == 1);
        if (c.getInt(8) == 1) {
            z = true;
        } else {
            z = false;
        }
        model.setFriend(z);
        model.setFansNum(c.getInt(10));
        model.setTotalNum(totalNum);
        model.setCurrentPage(page);
        model.setReadFromLocal(true);
        return model;
    }
}
