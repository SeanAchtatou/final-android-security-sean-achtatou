package com.mobcent.android.db.helper;

import android.database.Cursor;
import com.mobcent.android.model.MCLibUserStatus;

public class UserCommunicationQueueDBUtilHelper {
    public static MCLibUserStatus buildUserStatusModel(Cursor c) {
        MCLibUserStatus userStatus = new MCLibUserStatus();
        userStatus.setUserCommunicationQueueId(c.getInt(0));
        userStatus.setUid(c.getInt(1));
        userStatus.setContent(c.getString(2));
        userStatus.setToUid(c.getInt(3));
        userStatus.setToStatusId((long) c.getInt(4));
        userStatus.setToAppId(c.getInt(5));
        userStatus.setCommunicationType(c.getInt(7));
        userStatus.setActionId(c.getInt(8));
        userStatus.setToUserName(c.getString(9));
        userStatus.setPhotoId(c.getInt(10));
        userStatus.setPhotoPath(c.getString(11));
        return userStatus;
    }
}
