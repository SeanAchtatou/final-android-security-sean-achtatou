package com.mobcent.android.db.constants;

public interface RecentChatUsersDBConstant {
    public static final String COLUMN_LAST_MSG_TIME = "lastMsgTime";
    public static final String COLUMN_SOURCE_NAME = "sourceName";
    public static final String COLUMN_SOURCE_PRO_ID = "sourceProId";
    public static final String COLUMN_SOURCE_URL = "sourceUrl";
    public static final String COLUMN_UNREAD_MSG_COUNT = "unreadMsgCount";
    public static final String COLUMN_USER_ID = "uid";
    public static final String COLUMN_USER_IMAGE = "userImage";
    public static final String COLUMN_USER_NAME = "userName";
    public static final String COLUMN_USER_NICK_NAME = "nickName";
    public static final String TABLE_RECENT_CHAT_USERS = "RecentChatUsers";
}
