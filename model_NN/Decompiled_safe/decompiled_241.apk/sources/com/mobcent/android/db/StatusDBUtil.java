package com.mobcent.android.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.mobcent.android.db.constants.StatusDBConstant;
import com.mobcent.android.db.helper.StatusDBUtilHelper;
import com.mobcent.android.model.MCLibUserStatus;
import com.mobcent.android.service.impl.MCLibUserInfoServiceImpl;
import java.util.ArrayList;
import java.util.List;

public class StatusDBUtil extends BaseDBUtil implements StatusDBConstant {
    private static final String DATABASE_NAME = "mobcent_mbs_";
    private static final String DROP_TABLE_AT_ME_STATUS = "DROP TABLE TNAtMeStatus";
    private static final String DROP_TABLE_FRIEND_EVENT = "DROP TABLE FriendEvent";
    private static final String DROP_TABLE_FRIEND_STATUS = "DROP TABLE TNFriendStatus";
    private static final String DROP_TABLE_HALL_EVENT = "DROP TABLE HallEvent";
    private static final String DROP_TABLE_HALL_STATUS = "DROP TABLE TNHallStatus";
    private static final String DROP_TABLE_HALL_TOPIC = "DROP TABLE TNHallTopic";
    private static final String DROP_TABLE_MY_STATUS = "DROP TABLE TNMyStatus";
    private static final String SQL_CREATE_TABLE_AT_ME_STATUS = "CREATE TABLE IF NOT EXISTS TNAtMeStatus(seId INTEGER PRIMARY KEY,content TEXT,userId INTEGER,userName TEXT,toUserId INTEGER,toUserName TEXT,pubTime TEXT,replyNum INTEGER,forwordNum INTEGER,communicationType INTEGER,typeName TEXT,userImage TEXT,rootId INTEGER,seReplyId INTEGER,replyContent TEXT,replyUserId INTEGER,replyUserName TEXT,replyToUserId INTEGER,replyToUserName TEXT,replyPubTime TEXT,replyReplyNum INTEGER,replyForwordNum INTEGER,replyCommunicationType INTEGER,replyTypeName TEXT,replyUserImage TEXT,replyRootId INTEGER,topicStatus INTEGER,sourceProId INTEGER,sourceName TEXT,sourceUrl TEXT,sourceCategoryId INTEGER,imgUrl TEXT,photoPath TEXT,replyImgUrl TEXT,replyPhotoPath TEXT,statusDeleted INTEGER,replyStatusDeleted INTEGER,linkUrl TEXT,reserve TEXT);";
    private static final String SQL_CREATE_TABLE_FRIEND_EVENT = "CREATE TABLE IF NOT EXISTS FriendEvent(content TEXT,fromUserId INTEGER,fromUserName TEXT,targetJumpId INTEGER,targetJumpName TEXT,pubTime TEXT,communicationType INTEGER,userImage TEXT,topicStatus INTEGER,eventUrl TEXT,sourceProId INTEGER,sourceName TEXT,sourceUrl TEXT,sourceCategoryId INTEGER,reserve TEXT);";
    private static final String SQL_CREATE_TABLE_FRIEND_STATUS = "CREATE TABLE IF NOT EXISTS TNFriendStatus(seId INTEGER PRIMARY KEY,content TEXT,userId INTEGER,userName TEXT,toUserId INTEGER,toUserName TEXT,pubTime TEXT,replyNum INTEGER,forwordNum INTEGER,communicationType INTEGER,typeName TEXT,userImage TEXT,rootId INTEGER,seReplyId INTEGER,replyContent TEXT,replyUserId INTEGER,replyUserName TEXT,replyToUserId INTEGER,replyToUserName TEXT,replyPubTime TEXT,replyReplyNum INTEGER,replyForwordNum INTEGER,replyCommunicationType INTEGER,replyTypeName TEXT,replyUserImage TEXT,replyRootId INTEGER,topicStatus INTEGER,sourceProId INTEGER,sourceName TEXT,sourceUrl TEXT,sourceCategoryId INTEGER,imgUrl TEXT,photoPath TEXT,replyImgUrl TEXT,replyPhotoPath TEXT,statusDeleted INTEGER,replyStatusDeleted INTEGER,linkUrl TEXT,reserve TEXT);";
    private static final String SQL_CREATE_TABLE_HALL_STATUS = "CREATE TABLE IF NOT EXISTS TNHallStatus(seId INTEGER PRIMARY KEY,content TEXT,userId INTEGER,userName TEXT,toUserId INTEGER,toUserName TEXT,pubTime TEXT,replyNum INTEGER,forwordNum INTEGER,communicationType INTEGER,typeName TEXT,userImage TEXT,rootId INTEGER,seReplyId INTEGER,replyContent TEXT,replyUserId INTEGER,replyUserName TEXT,replyToUserId INTEGER,replyToUserName TEXT,replyPubTime TEXT,replyReplyNum INTEGER,replyForwordNum INTEGER,replyCommunicationType INTEGER,replyTypeName TEXT,replyUserImage TEXT,replyRootId INTEGER,topicStatus INTEGER,sourceProId INTEGER,sourceName TEXT,sourceUrl TEXT,sourceCategoryId INTEGER,imgUrl TEXT,photoPath TEXT,replyImgUrl TEXT,replyPhotoPath TEXT,statusDeleted INTEGER,replyStatusDeleted INTEGER,linkUrl TEXT,reserve TEXT);";
    private static final String SQL_CREATE_TABLE_HALL_TOPIC = "CREATE TABLE IF NOT EXISTS TNHallTopic(seId INTEGER PRIMARY KEY,content TEXT,userId INTEGER,userName TEXT,toUserId INTEGER,toUserName TEXT,pubTime TEXT,replyNum INTEGER,forwordNum INTEGER,communicationType INTEGER,typeName TEXT,userImage TEXT,rootId INTEGER,seReplyId INTEGER,replyContent TEXT,replyUserId INTEGER,replyUserName TEXT,replyToUserId INTEGER,replyToUserName TEXT,replyPubTime TEXT,replyReplyNum INTEGER,replyForwordNum INTEGER,replyCommunicationType INTEGER,replyTypeName TEXT,replyUserImage TEXT,replyRootId INTEGER,topicStatus INTEGER,sourceProId INTEGER,sourceName TEXT,sourceUrl TEXT,sourceCategoryId INTEGER,imgUrl TEXT,photoPath TEXT,replyImgUrl TEXT,replyPhotoPath TEXT,statusDeleted INTEGER,replyStatusDeleted INTEGER,linkUrl TEXT,reserve TEXT);";
    private static final String SQL_CREATE_TABLE_MY_STATUS = "CREATE TABLE IF NOT EXISTS TNMyStatus(seId INTEGER PRIMARY KEY,content TEXT,userId INTEGER,userName TEXT,toUserId INTEGER,toUserName TEXT,pubTime TEXT,replyNum INTEGER,forwordNum INTEGER,communicationType INTEGER,typeName TEXT,userImage TEXT,rootId INTEGER,seReplyId INTEGER,replyContent TEXT,replyUserId INTEGER,replyUserName TEXT,replyToUserId INTEGER,replyToUserName TEXT,replyPubTime TEXT,replyReplyNum INTEGER,replyForwordNum INTEGER,replyCommunicationType INTEGER,replyTypeName TEXT,replyUserImage TEXT,replyRootId INTEGER,topicStatus INTEGER,sourceProId INTEGER,sourceName TEXT,sourceUrl TEXT,sourceCategoryId INTEGER,imgUrl TEXT,photoPath TEXT,replyImgUrl TEXT,replyPhotoPath TEXT,statusDeleted INTEGER,replyStatusDeleted INTEGER,linkUrl TEXT,reserve TEXT);";
    private static final String SQL_CREATE_TIME_HALL_EVENT = "CREATE TABLE IF NOT EXISTS HallEvent(content TEXT,fromUserId INTEGER,fromUserName TEXT,targetJumpId INTEGER,targetJumpName TEXT,pubTime TEXT,communicationType INTEGER,userImage TEXT,topicStatus INTEGER,eventUrl INTEGER,sourceProId INTEGER,sourceName TEXT,sourceUrl TEXT,sourceCategoryId INTEGER,reserve TEXT);";
    public static final int TIME_AT_ME_STATUS = 2;
    public static final int TIME_FRIEND_EVENT = 6;
    public static final int TIME_FRIEND_STATUS = 3;
    public static final int TIME_HALL_EVENT = 7;
    public static final int TIME_HALL_STATUS = 4;
    public static final int TIME_HALL_TOPIC = 5;
    public static final int TIME_MY_STATUS = 1;
    private static StatusDBUtil mobcentDBUtil;
    private Context ctx;

    protected StatusDBUtil(Context ctx2) {
        this.ctx = ctx2;
        SQLiteDatabase db = null;
        try {
            db = openDB();
            db.execSQL(SQL_CREATE_TABLE_MY_STATUS);
            db.execSQL(SQL_CREATE_TABLE_AT_ME_STATUS);
            db.execSQL(SQL_CREATE_TABLE_FRIEND_STATUS);
            db.execSQL(SQL_CREATE_TABLE_HALL_STATUS);
            db.execSQL(SQL_CREATE_TABLE_FRIEND_EVENT);
            db.execSQL(SQL_CREATE_TIME_HALL_EVENT);
            db.execSQL(SQL_CREATE_TABLE_HALL_TOPIC);
            db.execSQL(BaseDBUtil.SQL_CREATE_TABLE_LAST_UPDATE_TIME);
        } finally {
            db.close();
        }
    }

    public void dropAllTables() {
        SQLiteDatabase db = null;
        try {
            db = openDB();
            db.execSQL(DROP_TABLE_MY_STATUS);
            db.execSQL(DROP_TABLE_AT_ME_STATUS);
            db.execSQL(DROP_TABLE_FRIEND_STATUS);
            db.execSQL(DROP_TABLE_HALL_STATUS);
            db.execSQL(DROP_TABLE_HALL_TOPIC);
            db.execSQL(DROP_TABLE_FRIEND_EVENT);
            db.execSQL(DROP_TABLE_HALL_EVENT);
            db.execSQL(BaseDBUtil.DROP_TABLE_LAST_UPDATE_TIME);
        } finally {
            db.close();
        }
    }

    public static StatusDBUtil getInstance(Context context) {
        mobcentDBUtil = new StatusDBUtil(context);
        return mobcentDBUtil;
    }

    /* access modifiers changed from: protected */
    public SQLiteDatabase openDB() {
        return this.ctx.openOrCreateDatabase(DATABASE_NAME + new MCLibUserInfoServiceImpl(this.ctx).getLoginUserId() + ".db", 0, null);
    }

    public boolean addOrUpdateMyStatus(int loginUid, List<MCLibUserStatus> userStatuses) {
        if (userStatuses == null) {
            return true;
        }
        SQLiteDatabase db = null;
        try {
            db = openDB();
            for (MCLibUserStatus userStatus : userStatuses) {
                ContentValues values = formUserStatusContentValues(userStatus);
                if (!isRowExisted(StatusDBConstant.TABLE_MY_STATUS, StatusDBConstant.COLUMN_STATUS_EVENT_ID, userStatus.getStatusId())) {
                    values.put(StatusDBConstant.COLUMN_STATUS_EVENT_ID, Long.valueOf(userStatus.getStatusId()));
                    db.insertOrThrow(StatusDBConstant.TABLE_MY_STATUS, null, values);
                } else {
                    db.update(StatusDBConstant.TABLE_MY_STATUS, values, "seId=" + userStatus.getStatusId(), null);
                }
            }
            if (db != null) {
                db.close();
            }
            return true;
        } catch (Exception e) {
            if (db != null) {
                db.close();
            }
            return false;
        } catch (Throwable th) {
            if (db != null) {
                db.close();
            }
            throw th;
        }
    }

    public List<MCLibUserStatus> getMyStatuses(int loginUid, int page, int pageSize) {
        List<MCLibUserStatus> userStatuses = new ArrayList<>();
        SQLiteDatabase db = null;
        Cursor c = null;
        try {
            db = openDB();
            c = db.rawQuery("select * from TNMyStatus order by seId desc limit " + pageSize + " offset " + ((page - 1) * pageSize), null);
            int totalNum = getMyStatusesCount(loginUid);
            if (c.getCount() > 0 && page == 1) {
                MCLibUserStatus model = getRefreshUserStatusModel(getLastUpdateTime(1));
                model.setTotalNum(totalNum);
                model.setCurrentPage(page);
                userStatuses.add(model);
            } else if (c.getCount() <= 0 && page == 1) {
                if (c != null) {
                    c.close();
                }
                if (db != null) {
                    db.close();
                }
                return userStatuses;
            } else if (c.getCount() <= 0) {
                MCLibUserStatus model2 = getRefreshUserStatusModel(getLastUpdateTime(1));
                model2.setTotalNum(totalNum);
                model2.setCurrentPage(page);
                userStatuses.add(model2);
            }
            for (int i = 0; i < c.getCount(); i++) {
                c.moveToPosition(i);
                userStatuses.add(StatusDBUtilHelper.buildUserStatusModel(c, totalNum, page));
            }
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
        } catch (Exception e) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            throw th;
        }
        return userStatuses;
    }

    public int getMyStatusesCount(int loginUid) {
        SQLiteDatabase db = null;
        Cursor c = null;
        try {
            SQLiteDatabase db2 = openDB();
            Cursor c2 = db2.rawQuery("select count(*) from TNMyStatus", null);
            if (c2.moveToFirst()) {
                int i = c2.getInt(0);
                if (c2 != null) {
                    c2.close();
                }
                if (db2 == null) {
                    return i;
                }
                db2.close();
                return i;
            }
            if (c2 != null) {
                c2.close();
            }
            if (db2 != null) {
                db2.close();
            }
            return 0;
        } catch (Exception e) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            return 0;
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            throw th;
        }
    }

    public boolean removeAllMyStatus() {
        SQLiteDatabase db = null;
        try {
            db = openDB();
            db.delete(StatusDBConstant.TABLE_MY_STATUS, null, null);
            if (db != null) {
                db.close();
            }
            return true;
        } catch (Exception e) {
            if (db != null) {
                db.close();
            }
            return false;
        } catch (Throwable th) {
            if (db != null) {
                db.close();
            }
            throw th;
        }
    }

    public boolean addOrUpdateAtMeStatus(int loginUid, List<MCLibUserStatus> userStatuses) {
        if (userStatuses == null) {
            return true;
        }
        SQLiteDatabase db = null;
        try {
            db = openDB();
            for (MCLibUserStatus userStatus : userStatuses) {
                ContentValues values = formUserStatusContentValues(userStatus);
                if (!isRowExisted(StatusDBConstant.TABLE_AT_ME_STATUS, StatusDBConstant.COLUMN_STATUS_EVENT_ID, userStatus.getStatusId())) {
                    values.put(StatusDBConstant.COLUMN_STATUS_EVENT_ID, Long.valueOf(userStatus.getStatusId()));
                    db.insertOrThrow(StatusDBConstant.TABLE_AT_ME_STATUS, null, values);
                } else {
                    db.update(StatusDBConstant.TABLE_AT_ME_STATUS, values, "seId=" + userStatus.getStatusId(), null);
                }
            }
            if (db != null) {
                db.close();
            }
            return true;
        } catch (Exception e) {
            if (db != null) {
                db.close();
            }
            return false;
        } catch (Throwable th) {
            if (db != null) {
                db.close();
            }
            throw th;
        }
    }

    public List<MCLibUserStatus> getAtMeStatuses(int loginUid, int page, int pageSize) {
        List<MCLibUserStatus> userStatuses = new ArrayList<>();
        SQLiteDatabase db = null;
        Cursor c = null;
        try {
            db = openDB();
            c = db.rawQuery("select * from TNAtMeStatus order by seId desc limit " + pageSize + " offset " + ((page - 1) * pageSize), null);
            int totalNum = getAtMeStatusesCount(loginUid);
            if (c.getCount() > 0 && page == 1) {
                MCLibUserStatus model = getRefreshUserStatusModel(getLastUpdateTime(2));
                model.setTotalNum(totalNum);
                model.setCurrentPage(page);
                userStatuses.add(model);
            } else if (c.getCount() <= 0 && page == 1) {
                if (c != null) {
                    c.close();
                }
                if (db != null) {
                    db.close();
                }
                return userStatuses;
            } else if (c.getCount() <= 0) {
                MCLibUserStatus model2 = getRefreshUserStatusModel(getLastUpdateTime(2));
                model2.setTotalNum(totalNum);
                model2.setCurrentPage(page);
                userStatuses.add(model2);
            }
            for (int i = 0; i < c.getCount(); i++) {
                c.moveToPosition(i);
                userStatuses.add(StatusDBUtilHelper.buildUserStatusModel(c, totalNum, page));
            }
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
        } catch (Exception e) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            throw th;
        }
        return userStatuses;
    }

    public int getAtMeStatusesCount(int loginUid) {
        SQLiteDatabase db = null;
        Cursor c = null;
        try {
            SQLiteDatabase db2 = openDB();
            Cursor c2 = db2.rawQuery("select count(*) from TNAtMeStatus", null);
            if (c2.moveToFirst()) {
                int i = c2.getInt(0);
                if (c2 != null) {
                    c2.close();
                }
                if (db2 == null) {
                    return i;
                }
                db2.close();
                return i;
            }
            if (c2 != null) {
                c2.close();
            }
            if (db2 != null) {
                db2.close();
            }
            return 0;
        } catch (Exception e) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            return 0;
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            throw th;
        }
    }

    public boolean removeAllAtMeStatus() {
        SQLiteDatabase db = null;
        try {
            db = openDB();
            db.delete(StatusDBConstant.TABLE_AT_ME_STATUS, null, null);
            if (db != null) {
                db.close();
            }
            return true;
        } catch (Exception e) {
            if (db != null) {
                db.close();
            }
            return false;
        } catch (Throwable th) {
            if (db != null) {
                db.close();
            }
            throw th;
        }
    }

    public boolean addOrUpdateFriendStatus(int loginUid, List<MCLibUserStatus> userStatuses) {
        if (userStatuses == null) {
            return true;
        }
        SQLiteDatabase db = null;
        try {
            db = openDB();
            for (MCLibUserStatus userStatus : userStatuses) {
                ContentValues values = formUserStatusContentValues(userStatus);
                if (!isRowExisted(StatusDBConstant.TABLE_FRIEND_STATUS, StatusDBConstant.COLUMN_STATUS_EVENT_ID, userStatus.getStatusId())) {
                    values.put(StatusDBConstant.COLUMN_STATUS_EVENT_ID, Long.valueOf(userStatus.getStatusId()));
                    db.insertOrThrow(StatusDBConstant.TABLE_FRIEND_STATUS, null, values);
                } else {
                    db.update(StatusDBConstant.TABLE_FRIEND_STATUS, values, "seId=" + userStatus.getStatusId(), null);
                }
            }
            if (db != null) {
                db.close();
            }
            return true;
        } catch (Exception e) {
            if (db != null) {
                db.close();
            }
            return false;
        } catch (Throwable th) {
            if (db != null) {
                db.close();
            }
            throw th;
        }
    }

    public List<MCLibUserStatus> getFriendStatuses(int loginUid, int page, int pageSize) {
        List<MCLibUserStatus> userStatuses = new ArrayList<>();
        SQLiteDatabase db = null;
        Cursor c = null;
        try {
            db = openDB();
            c = db.rawQuery("select * from TNFriendStatus order by seId desc limit " + pageSize + " offset " + ((page - 1) * pageSize), null);
            int totalNum = getFriendStatusesCount(loginUid);
            if (c.getCount() > 0 && page == 1) {
                MCLibUserStatus model = getRefreshUserStatusModel(getLastUpdateTime(3));
                model.setTotalNum(totalNum);
                model.setCurrentPage(page);
                userStatuses.add(model);
            } else if (c.getCount() <= 0 && page == 1) {
                if (c != null) {
                    c.close();
                }
                if (db != null) {
                    db.close();
                }
                return userStatuses;
            } else if (c.getCount() <= 0) {
                MCLibUserStatus model2 = getRefreshUserStatusModel(getLastUpdateTime(3));
                model2.setTotalNum(totalNum);
                model2.setCurrentPage(page);
                userStatuses.add(model2);
            }
            for (int i = 0; i < c.getCount(); i++) {
                c.moveToPosition(i);
                userStatuses.add(StatusDBUtilHelper.buildUserStatusModel(c, totalNum, page));
            }
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
        } catch (Exception e) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            throw th;
        }
        return userStatuses;
    }

    public int getFriendStatusesCount(int loginUid) {
        SQLiteDatabase db = null;
        Cursor c = null;
        try {
            SQLiteDatabase db2 = openDB();
            Cursor c2 = db2.rawQuery("select count(*) from TNFriendStatus", null);
            if (c2.moveToFirst()) {
                int i = c2.getInt(0);
                if (c2 != null) {
                    c2.close();
                }
                if (db2 == null) {
                    return i;
                }
                db2.close();
                return i;
            }
            if (c2 != null) {
                c2.close();
            }
            if (db2 != null) {
                db2.close();
            }
            return 0;
        } catch (Exception e) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            return 0;
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            throw th;
        }
    }

    public boolean removeAllFriendStatus() {
        SQLiteDatabase db = null;
        try {
            db = openDB();
            db.delete(StatusDBConstant.TABLE_FRIEND_STATUS, null, null);
            if (db != null) {
                db.close();
            }
            return true;
        } catch (Exception e) {
            if (db != null) {
                db.close();
            }
            return false;
        } catch (Throwable th) {
            if (db != null) {
                db.close();
            }
            throw th;
        }
    }

    public boolean addOrUpdateHallStatus(int loginUid, List<MCLibUserStatus> userStatuses) {
        if (userStatuses == null) {
            return true;
        }
        SQLiteDatabase db = null;
        try {
            db = openDB();
            for (MCLibUserStatus userStatus : userStatuses) {
                ContentValues values = formUserStatusContentValues(userStatus);
                if (!isRowExisted(StatusDBConstant.TABLE_HALL_STATUS, StatusDBConstant.COLUMN_STATUS_EVENT_ID, userStatus.getStatusId())) {
                    values.put(StatusDBConstant.COLUMN_STATUS_EVENT_ID, Long.valueOf(userStatus.getStatusId()));
                    db.insertOrThrow(StatusDBConstant.TABLE_HALL_STATUS, null, values);
                } else {
                    db.update(StatusDBConstant.TABLE_HALL_STATUS, values, "seId=" + userStatus.getStatusId(), null);
                }
            }
            if (db != null) {
                db.close();
            }
            return true;
        } catch (Exception e) {
            if (db != null) {
                db.close();
            }
            return false;
        } catch (Throwable th) {
            if (db != null) {
                db.close();
            }
            throw th;
        }
    }

    public List<MCLibUserStatus> getHallStatuses(int loginUid, int page, int pageSize) {
        List<MCLibUserStatus> userStatuses = new ArrayList<>();
        SQLiteDatabase db = null;
        Cursor c = null;
        try {
            db = openDB();
            c = db.rawQuery("select * from TNHallStatus order by seId desc limit " + pageSize + " offset " + ((page - 1) * pageSize), null);
            int totalNum = getHallStatusesCount(loginUid);
            if (c.getCount() > 0 && page == 1) {
                MCLibUserStatus model = getRefreshUserStatusModel(getLastUpdateTime(4));
                model.setTotalNum(totalNum);
                model.setCurrentPage(page);
                userStatuses.add(model);
            } else if (c.getCount() <= 0 && page == 1) {
                if (c != null) {
                    c.close();
                }
                if (db != null) {
                    db.close();
                }
                return userStatuses;
            } else if (c.getCount() <= 0) {
                MCLibUserStatus model2 = getRefreshUserStatusModel(getLastUpdateTime(4));
                model2.setTotalNum(totalNum);
                model2.setCurrentPage(page);
                userStatuses.add(model2);
            }
            for (int i = 0; i < c.getCount(); i++) {
                c.moveToPosition(i);
                userStatuses.add(StatusDBUtilHelper.buildUserStatusModel(c, totalNum, page));
            }
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
        } catch (Exception e) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            throw th;
        }
        return userStatuses;
    }

    public int getHallStatusesCount(int loginUid) {
        SQLiteDatabase db = null;
        Cursor c = null;
        try {
            SQLiteDatabase db2 = openDB();
            Cursor c2 = db2.rawQuery("select count(*) from TNHallStatus", null);
            if (c2.moveToFirst()) {
                int i = c2.getInt(0);
                if (c2 != null) {
                    c2.close();
                }
                if (db2 == null) {
                    return i;
                }
                db2.close();
                return i;
            }
            if (c2 != null) {
                c2.close();
            }
            if (db2 != null) {
                db2.close();
            }
            return 0;
        } catch (Exception e) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            return 0;
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            throw th;
        }
    }

    public boolean removeAllHallStatus() {
        SQLiteDatabase db = null;
        try {
            db = openDB();
            db.delete(StatusDBConstant.TABLE_HALL_STATUS, null, null);
            if (db != null) {
                db.close();
            }
            return true;
        } catch (Exception e) {
            if (db != null) {
                db.close();
            }
            return false;
        } catch (Throwable th) {
            if (db != null) {
                db.close();
            }
            throw th;
        }
    }

    public boolean addOrUpdateHallTopics(int loginUid, List<MCLibUserStatus> userStatuses) {
        if (userStatuses == null) {
            return true;
        }
        SQLiteDatabase db = null;
        try {
            db = openDB();
            for (MCLibUserStatus userStatus : userStatuses) {
                ContentValues values = formUserStatusContentValues(userStatus);
                if (!isRowExisted(StatusDBConstant.TABLE_HALL_TOPIC, StatusDBConstant.COLUMN_STATUS_EVENT_ID, userStatus.getStatusId())) {
                    values.put(StatusDBConstant.COLUMN_STATUS_EVENT_ID, Long.valueOf(userStatus.getStatusId()));
                    db.insertOrThrow(StatusDBConstant.TABLE_HALL_TOPIC, null, values);
                } else {
                    db.update(StatusDBConstant.TABLE_HALL_STATUS, values, "seId=" + userStatus.getStatusId(), null);
                }
            }
            if (db != null) {
                db.close();
            }
            return true;
        } catch (Exception e) {
            if (db != null) {
                db.close();
            }
            return false;
        } catch (Throwable th) {
            if (db != null) {
                db.close();
            }
            throw th;
        }
    }

    public List<MCLibUserStatus> getHallTopics(int loginUid, int page, int pageSize) {
        List<MCLibUserStatus> topicTops;
        List<MCLibUserStatus> userStatuses = new ArrayList<>();
        SQLiteDatabase db = null;
        Cursor c = null;
        try {
            db = openDB();
            c = db.rawQuery("select * from TNHallTopic where topicStatus=0 order by seId desc limit " + pageSize + " offset " + ((page - 1) * pageSize), null);
            int totalNum = getHallTopicsCount();
            if (c.getCount() > 0 && page == 1) {
                MCLibUserStatus model = getRefreshUserStatusModel(getLastUpdateTime(5));
                model.setTotalNum(totalNum);
                model.setCurrentPage(page);
                userStatuses.add(model);
            } else if (c.getCount() <= 0 && page == 1) {
                if (c != null) {
                    c.close();
                }
                if (db != null) {
                    db.close();
                }
                return userStatuses;
            } else if (c.getCount() <= 0) {
                MCLibUserStatus model2 = getRefreshUserStatusModel(getLastUpdateTime(5));
                model2.setTotalNum(totalNum);
                model2.setCurrentPage(page);
                userStatuses.add(model2);
            }
            for (int i = 0; i < c.getCount(); i++) {
                c.moveToPosition(i);
                userStatuses.add(StatusDBUtilHelper.buildUserStatusModel(c, totalNum, page));
            }
            if (page == 1 && (topicTops = getHallTopicsTops(totalNum, page)) != null && !topicTops.isEmpty()) {
                userStatuses.addAll(1, topicTops);
            }
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
        } catch (Exception e) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            throw th;
        }
        return userStatuses;
    }

    public List<MCLibUserStatus> getHallTopicsTops(int totalNum, int page) {
        List<MCLibUserStatus> userStatuses = new ArrayList<>();
        SQLiteDatabase db = null;
        Cursor c = null;
        try {
            db = openDB();
            c = db.rawQuery("select * from TNHallTopic where topicStatus>0 order by seId desc", null);
            for (int i = 0; i < c.getCount(); i++) {
                c.moveToPosition(i);
                userStatuses.add(StatusDBUtilHelper.buildUserStatusModel(c, totalNum, page));
            }
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
        } catch (Exception e) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            throw th;
        }
        return userStatuses;
    }

    public int getHallTopicsCount() {
        SQLiteDatabase db = null;
        Cursor c = null;
        try {
            SQLiteDatabase db2 = openDB();
            Cursor c2 = db2.rawQuery("select count(*) from TNHallTopic where topicStatus=0", null);
            if (c2.moveToFirst()) {
                int i = c2.getInt(0);
                if (c2 != null) {
                    c2.close();
                }
                if (db2 == null) {
                    return i;
                }
                db2.close();
                return i;
            }
            if (c2 != null) {
                c2.close();
            }
            if (db2 != null) {
                db2.close();
            }
            return 0;
        } catch (Exception e) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            return 0;
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            throw th;
        }
    }

    public boolean removeAllHallTopics() {
        SQLiteDatabase db = null;
        try {
            db = openDB();
            db.delete(StatusDBConstant.TABLE_HALL_TOPIC, null, null);
            if (db != null) {
                db.close();
            }
            return true;
        } catch (Exception e) {
            if (db != null) {
                db.close();
            }
            return false;
        } catch (Throwable th) {
            if (db != null) {
                db.close();
            }
            throw th;
        }
    }

    public boolean addOrUpdateHallEvent(int loginUid, List<MCLibUserStatus> userStatuses) {
        if (userStatuses == null) {
            return true;
        }
        SQLiteDatabase db = null;
        try {
            db = openDB();
            for (MCLibUserStatus userStatus : userStatuses) {
                ContentValues values = formUserEventContentValues(userStatus);
                if (!isEventRowExisted(StatusDBConstant.TABLE_HALL_EVENT, userStatus, loginUid)) {
                    db.insertOrThrow(StatusDBConstant.TABLE_HALL_EVENT, null, values);
                }
            }
            if (db != null) {
                db.close();
            }
            return true;
        } catch (Exception e) {
            if (db != null) {
                db.close();
            }
            return false;
        } catch (Throwable th) {
            if (db != null) {
                db.close();
            }
            throw th;
        }
    }

    public List<MCLibUserStatus> getHallEvents(int loginUid, int page, int pageSize) {
        List<MCLibUserStatus> userStatuses = new ArrayList<>();
        SQLiteDatabase db = null;
        Cursor c = null;
        try {
            db = openDB();
            c = db.rawQuery("select * from HallEvent limit " + pageSize + " offset " + ((page - 1) * pageSize), null);
            int totalNum = getHallEventsCount(loginUid);
            if (c.getCount() > 0 && page == 1) {
                MCLibUserStatus model = getRefreshUserStatusModel(getLastUpdateTime(7));
                model.setTotalNum(totalNum);
                model.setCurrentPage(page);
                userStatuses.add(model);
            } else if (c.getCount() <= 0 && page == 1) {
                if (c != null) {
                    c.close();
                }
                if (db != null) {
                    db.close();
                }
                return userStatuses;
            } else if (c.getCount() <= 0) {
                MCLibUserStatus model2 = getRefreshUserStatusModel(getLastUpdateTime(7));
                model2.setTotalNum(totalNum);
                model2.setCurrentPage(page);
                userStatuses.add(model2);
            }
            for (int i = 0; i < c.getCount(); i++) {
                c.moveToPosition(i);
                userStatuses.add(StatusDBUtilHelper.buildUserEventModel(c, totalNum, page));
            }
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
        } catch (Exception e) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            throw th;
        }
        return userStatuses;
    }

    public int getHallEventsCount(int loginUid) {
        SQLiteDatabase db = null;
        Cursor c = null;
        try {
            SQLiteDatabase db2 = openDB();
            Cursor c2 = db2.rawQuery("select count(*) from HallEvent", null);
            if (c2.moveToFirst()) {
                int i = c2.getInt(0);
                if (c2 != null) {
                    c2.close();
                }
                if (db2 == null) {
                    return i;
                }
                db2.close();
                return i;
            }
            if (c2 != null) {
                c2.close();
            }
            if (db2 != null) {
                db2.close();
            }
            return 0;
        } catch (Exception e) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            return 0;
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            throw th;
        }
    }

    public boolean removeAllHallEvents() {
        SQLiteDatabase db = null;
        try {
            db = openDB();
            db.delete(StatusDBConstant.TABLE_HALL_EVENT, null, null);
            if (db != null) {
                db.close();
            }
            return true;
        } catch (Exception e) {
            if (db != null) {
                db.close();
            }
            return false;
        } catch (Throwable th) {
            if (db != null) {
                db.close();
            }
            throw th;
        }
    }

    public boolean addOrUpdateFriendEvent(int loginUid, List<MCLibUserStatus> userStatuses) {
        if (userStatuses == null) {
            return true;
        }
        SQLiteDatabase db = null;
        try {
            db = openDB();
            for (MCLibUserStatus userStatus : userStatuses) {
                ContentValues values = formUserEventContentValues(userStatus);
                if (!isEventRowExisted(StatusDBConstant.TABLE_FRIEND_EVENT, userStatus, loginUid)) {
                    db.insertOrThrow(StatusDBConstant.TABLE_FRIEND_EVENT, null, values);
                } else {
                    db.update(StatusDBConstant.TABLE_FRIEND_EVENT, values, "seId=" + userStatus.getStatusId(), null);
                }
            }
            if (db != null) {
                db.close();
            }
            return true;
        } catch (Exception e) {
            if (db != null) {
                db.close();
            }
            return false;
        } catch (Throwable th) {
            if (db != null) {
                db.close();
            }
            throw th;
        }
    }

    public List<MCLibUserStatus> getFriendEvents(int loginUid, int page, int pageSize) {
        List<MCLibUserStatus> userStatuses = new ArrayList<>();
        SQLiteDatabase db = null;
        Cursor c = null;
        try {
            db = openDB();
            c = db.rawQuery("select * from FriendEvent limit " + pageSize + " offset " + ((page - 1) * pageSize), null);
            int totalNum = getFriendEventsCount(loginUid);
            if (c.getCount() > 0 && page == 1) {
                MCLibUserStatus model = getRefreshUserStatusModel(getLastUpdateTime(6));
                model.setTotalNum(totalNum);
                model.setCurrentPage(page);
                userStatuses.add(model);
            } else if (c.getCount() <= 0 && page == 1) {
                if (c != null) {
                    c.close();
                }
                if (db != null) {
                    db.close();
                }
                return userStatuses;
            } else if (c.getCount() <= 0) {
                MCLibUserStatus model2 = getRefreshUserStatusModel(getLastUpdateTime(6));
                model2.setTotalNum(totalNum);
                model2.setCurrentPage(page);
                userStatuses.add(model2);
            }
            for (int i = 0; i < c.getCount(); i++) {
                c.moveToPosition(i);
                userStatuses.add(StatusDBUtilHelper.buildUserEventModel(c, totalNum, page));
            }
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
        } catch (Exception e) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            throw th;
        }
        return userStatuses;
    }

    public int getFriendEventsCount(int loginUid) {
        SQLiteDatabase db = null;
        Cursor c = null;
        try {
            SQLiteDatabase db2 = openDB();
            Cursor c2 = db2.rawQuery("select count(*) from FriendEvent", null);
            if (c2.moveToFirst()) {
                int i = c2.getInt(0);
                if (c2 != null) {
                    c2.close();
                }
                if (db2 == null) {
                    return i;
                }
                db2.close();
                return i;
            }
            if (c2 != null) {
                c2.close();
            }
            if (db2 != null) {
                db2.close();
            }
            return 0;
        } catch (Exception e) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            return 0;
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            throw th;
        }
    }

    public boolean removeAllFriendEvents() {
        SQLiteDatabase db = null;
        try {
            db = openDB();
            db.delete(StatusDBConstant.TABLE_HALL_EVENT, null, null);
            if (db != null) {
                db.close();
            }
            return true;
        } catch (Exception e) {
            if (db != null) {
                db.close();
            }
            return false;
        } catch (Throwable th) {
            if (db != null) {
                db.close();
            }
            throw th;
        }
    }

    public boolean isEventRowExisted(String table, MCLibUserStatus userStatus, int loginUid) {
        SQLiteDatabase db = null;
        Cursor c = null;
        try {
            db = openDB();
            c = db.rawQuery("select count(*) from " + table + " where " + "content" + "=" + userStatus.getContent() + " and " + "fromUserId" + "=" + userStatus.getFromUserId() + " and " + StatusDBConstant.COLUMN_TARGET_JUMP_ID + "=" + userStatus.getTargetJumpId() + " and " + "pubTime" + "=" + userStatus.getTime() + " and " + "communicationType" + "=" + userStatus.getCommunicationType(), null);
            boolean isExist = false;
            if (c.getCount() > 0) {
                isExist = true;
            }
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            return isExist;
        } catch (Exception e) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            return false;
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            throw th;
        }
    }

    public static MCLibUserStatus getRefreshUserStatusModel(String time) {
        MCLibUserStatus model = new MCLibUserStatus();
        model.setRefreshNeeded(true);
        model.setReadFromLocal(true);
        model.setLastUpdateTime(time);
        return model;
    }

    private static ContentValues formUserStatusContentValues(MCLibUserStatus userStatus) {
        int i;
        int i2;
        ContentValues values = new ContentValues();
        values.put("content", userStatus.getContent());
        values.put("userId", Integer.valueOf(userStatus.getUid()));
        values.put("userName", userStatus.getUserName());
        values.put("toUserId", Integer.valueOf(userStatus.getToUid()));
        values.put("toUserName", userStatus.getToUserName());
        values.put("pubTime", userStatus.getTime());
        values.put("replyNum", Integer.valueOf(userStatus.getReplyNum()));
        values.put(StatusDBConstant.COLUMN_FORWARD_NUM, Integer.valueOf(userStatus.getForwardNum()));
        values.put("communicationType", Integer.valueOf(userStatus.getCommunicationType()));
        values.put("typeName", userStatus.getTypeName());
        values.put("userImage", userStatus.getUserImageUrl());
        values.put("rootId", Long.valueOf(userStatus.getRootId()));
        values.put("imgUrl", userStatus.getImgUrl());
        values.put("photoPath", userStatus.getPhotoPath());
        values.put("topicStatus", Integer.valueOf(userStatus.getTopicStatus()));
        if (userStatus.isDel()) {
            i = 1;
        } else {
            i = 0;
        }
        values.put(StatusDBConstant.COLUMN_STATUS_DELETED, Integer.valueOf(i));
        MCLibUserStatus replyUserStatus = new MCLibUserStatus();
        if (userStatus.getReplyStatus() != null && userStatus.getReplyStatus().getStatusId() > 0) {
            replyUserStatus = userStatus.getReplyStatus();
        }
        values.put(StatusDBConstant.COLUMN_REPLY_STATUS_EVENT_ID, Long.valueOf(replyUserStatus.getStatusId()));
        values.put("replyContent", replyUserStatus.getContent());
        values.put(StatusDBConstant.COLUMN_REPLY_USER_ID, Integer.valueOf(replyUserStatus.getUid()));
        values.put(StatusDBConstant.COLUMN_REPLY_USER_NAME, replyUserStatus.getUserName());
        values.put(StatusDBConstant.COLUMN_REPLY_TO_USER_ID, Integer.valueOf(replyUserStatus.getToUid()));
        values.put(StatusDBConstant.COLUMN_REPLY_TO_USER_NAME, replyUserStatus.getToUserName());
        values.put(StatusDBConstant.COLUMN_REPLY_PUB_TIME, replyUserStatus.getTime());
        values.put(StatusDBConstant.COLUMN_REPLY_REPLY_NUM, Integer.valueOf(replyUserStatus.getReplyNum()));
        values.put(StatusDBConstant.COLUMN_REPLY_FORWARD_NUM, Integer.valueOf(replyUserStatus.getForwardNum()));
        values.put(StatusDBConstant.COLUMN_REPLY_COMMUNICATION_TYPE, Integer.valueOf(replyUserStatus.getCommunicationType()));
        values.put(StatusDBConstant.COLUMN_REPLY_TYPE_NAME, replyUserStatus.getTypeName());
        values.put(StatusDBConstant.COLUMN_REPLY_USER_IMAGE, replyUserStatus.getUserImageUrl());
        values.put(StatusDBConstant.COLUMN_REPLY_STATUS_ROOT_ID, Long.valueOf(replyUserStatus.getRootId()));
        values.put("replyImgUrl", replyUserStatus.getImgUrl());
        values.put("replyPhotoPath", replyUserStatus.getPhotoPath());
        if (replyUserStatus.isDel()) {
            i2 = 1;
        } else {
            i2 = 0;
        }
        values.put(StatusDBConstant.COLUMN_STATUS_REPLY_DELETED, Integer.valueOf(i2));
        values.put("sourceProId", Integer.valueOf(userStatus.getSourceProId()));
        values.put("sourceCategoryId", Integer.valueOf(userStatus.getSourceCategoryId()));
        values.put("sourceName", userStatus.getSourceName());
        values.put("sourceUrl", userStatus.getSourceUrl());
        values.put(StatusDBConstant.COLUMN_STATUS_LINK_CONTENT_URL, userStatus.getContentPathUrl());
        values.put(StatusDBConstant.COLUMN_RESERVE, "");
        return values;
    }

    private static ContentValues formUserEventContentValues(MCLibUserStatus userStatus) {
        ContentValues values = new ContentValues();
        values.put("content", userStatus.getContent());
        values.put("fromUserId", Integer.valueOf(userStatus.getFromUserId()));
        values.put("fromUserName", userStatus.getFromUserName());
        values.put(StatusDBConstant.COLUMN_TARGET_JUMP_ID, Integer.valueOf(userStatus.getTargetJumpId()));
        values.put(StatusDBConstant.COLUMN_TARGET_JUMP_NAME, userStatus.getTargetJumpName());
        values.put("pubTime", userStatus.getTime());
        values.put("communicationType", Integer.valueOf(userStatus.getCommunicationType()));
        values.put("userImage", userStatus.getUserImageUrl());
        values.put(StatusDBConstant.COLUMN_URL, userStatus.getDomainUrl());
        values.put("sourceProId", Integer.valueOf(userStatus.getSourceProId()));
        values.put("sourceCategoryId", Integer.valueOf(userStatus.getSourceCategoryId()));
        values.put("sourceName", userStatus.getSourceName());
        values.put("sourceUrl", userStatus.getSourceUrl());
        values.put(StatusDBConstant.COLUMN_RESERVE, "");
        return values;
    }
}
