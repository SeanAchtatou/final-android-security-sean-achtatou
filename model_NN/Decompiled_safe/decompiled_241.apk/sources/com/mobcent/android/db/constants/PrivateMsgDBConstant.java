package com.mobcent.android.db.constants;

public interface PrivateMsgDBConstant {
    public static final String COLUMN_ACTION_ID = "actionId";
    public static final String COLUMN_BELONG_UID = "belongUid";
    public static final String COLUMN_CONTENT = "content";
    public static final String COLUMN_FROM_UID = "fromUid";
    public static final String COLUMN_FROM_USER_NAME = "fromUserName";
    public static final String COLUMN_ICON = "icon";
    public static final String COLUMN_IS_READ = "isRead";
    public static final String COLUMN_LOGIN_UID = "loginUid";
    public static final String COLUMN_MSG_ID = "mid";
    public static final String COLUMN_PUB_TIME = "pubTime";
    public static final String TABLE_PRIVATE_MSG = "TPrivateMsg";
}
