package com.mobcent.android.db.constants;

public interface MobcentSoftwareDBConstant {
    public static final String COLUMN_MOBCENT_SOFTWARE_DOWNLOAD_PATH = "downloadPath";
    public static final String COLUMN_MOBCENT_SOFTWARE_ID = "sid";
    public static final String COLUMN_MOBCENT_SOFTWARE_NAME = "softwareName";
    public static final String COLUMN_MOBCENT_SOFTWARE_TYPE = "softwareType";
    public static final String TABLE_MOBCENT_SOFTWARE = "MobcentSoftware";
}
