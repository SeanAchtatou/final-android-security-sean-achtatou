package com.mobcent.android.db.helper;

import android.database.Cursor;
import com.mobcent.android.model.MCLibActionModel;

public class UserMagicActionDictDBUtilHelper {
    public static MCLibActionModel buildMagicActionModel(Cursor c) {
        MCLibActionModel actionModel = new MCLibActionModel();
        actionModel.setActionId(c.getInt(0));
        actionModel.setShortName(c.getString(1));
        actionModel.setContent(c.getString(2));
        actionModel.setBaseUrl(c.getString(3));
        actionModel.setActionPrevImg(c.getString(4));
        actionModel.setIcon(c.getString(5));
        return actionModel;
    }
}
