package com.mobcent.android.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.mobcent.android.db.constants.SharedUserDBConstant;
import com.mobcent.android.db.helper.ShareUserDBUtilHelper;
import com.mobcent.android.model.MCLibUserInfo;
import com.mobcent.android.utils.MCLibIOUtil;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SharedUserDBUtil extends BaseDBUtil implements SharedUserDBConstant {
    private static final String DATABASE_NAME = "ms";
    private static final String DROP_TABLE_SHARE_USER_INFO = "DROP TABLE TUserShare";
    private static final String SQL_CREATE_TABLE_SHARE_USER_INFO = "CREATE TABLE IF NOT EXISTS TUserShare(uid INTEGER PRIMARY KEY,userName TEXT,nickName TEXT,email TEXT,userGender INTEGER,userBirthday TEXT,userImage TEXT,userCity TEXT,userPwd TEXT,userMood TEXT,isSavePwd INTEGER,isAutoLogin INTEGER,isForceShowMI INTEGER,isCurrentUser INTEGER);";
    private static SharedUserDBUtil mobcentDBUtil;
    private Context ctx;

    protected SharedUserDBUtil(Context ctx2) {
        this.ctx = ctx2;
        SQLiteDatabase db = null;
        try {
            db = openDB();
            db.execSQL(SQL_CREATE_TABLE_SHARE_USER_INFO);
        } finally {
            if (db != null) {
                db.close();
            }
        }
    }

    public void dropAllTables() {
        SQLiteDatabase db = null;
        try {
            db = openDB();
            db.execSQL(DROP_TABLE_SHARE_USER_INFO);
        } finally {
            if (db != null) {
                db.close();
            }
        }
    }

    public static SharedUserDBUtil getInstance(Context context) {
        if (mobcentDBUtil == null) {
            mobcentDBUtil = new SharedUserDBUtil(context);
        }
        return mobcentDBUtil;
    }

    public static SharedUserDBUtil getNewInstance(Context context) {
        SharedUserDBUtil sharedUserDBUtil = new SharedUserDBUtil(context);
        mobcentDBUtil = sharedUserDBUtil;
        return sharedUserDBUtil;
    }

    /* access modifiers changed from: protected */
    public SQLiteDatabase openDB() {
        String basePath = String.valueOf(MCLibIOUtil.getBaseLocalLocation(this.ctx)) + MCLibIOUtil.FS + ".mc" + MCLibIOUtil.FS;
        File file = new File(basePath);
        if (!file.exists()) {
            file.mkdirs();
        }
        return SQLiteDatabase.openOrCreateDatabase(String.valueOf(basePath) + DATABASE_NAME, (SQLiteDatabase.CursorFactory) null);
    }

    public boolean isForceBouncePanel() {
        MCLibUserInfo userInfo = getCurrentUserInfo();
        if (userInfo == null) {
            return true;
        }
        return userInfo.getIsForceShowMagicImage() == 1;
    }

    public void setForceBouncePanel(boolean isForceBouncePanel) {
        MCLibUserInfo userInfo = getCurrentUserInfo();
        if (userInfo != null) {
            userInfo.setIsForceShowMagicImage(isForceBouncePanel ? 1 : 0);
            addOrUpdateUserInfo(userInfo);
        }
    }

    public boolean addUser(MCLibUserInfo userInfo) {
        addOrUpdateUserInfo(userInfo);
        setUserAsCurrentUserInfo(userInfo.getUid());
        return true;
    }

    public void updateCurrentUserPwd(String pwd) {
        MCLibUserInfo userInfo = getCurrentUserInfo();
        if (userInfo != null) {
            userInfo.setPassword(pwd);
            addOrUpdateUserInfo(userInfo);
        }
    }

    public void updateCurrentUserBirthday(String birthday) {
        MCLibUserInfo userInfo = getCurrentUserInfo();
        if (userInfo != null) {
            userInfo.setBirthday(birthday);
            addOrUpdateUserInfo(userInfo);
        }
    }

    public void updateCurrentUserNickName(String nickName) {
        MCLibUserInfo userInfo = getCurrentUserInfo();
        if (userInfo != null) {
            userInfo.setNickName(nickName);
            addOrUpdateUserInfo(userInfo);
        }
    }

    public void updateCurrentUserPhoto(String photo) {
        MCLibUserInfo userInfo = getCurrentUserInfo();
        if (userInfo != null) {
            userInfo.setImage(photo);
            addOrUpdateUserInfo(userInfo);
        }
    }

    public void updateCurrentUserMood(String mood) {
        MCLibUserInfo userInfo = getCurrentUserInfo();
        if (userInfo != null) {
            userInfo.setEmotionWords(mood);
            addOrUpdateUserInfo(userInfo);
        }
    }

    public void updateCurrentUserCity(String city) {
        MCLibUserInfo userInfo = getCurrentUserInfo();
        if (userInfo != null) {
            userInfo.setPos(city);
            addOrUpdateUserInfo(userInfo);
        }
    }

    public void updateCurrentUserEmail(String email) {
        MCLibUserInfo userInfo = getCurrentUserInfo();
        if (userInfo != null) {
            userInfo.setEmail(email);
            addOrUpdateUserInfo(userInfo);
        }
    }

    public void updateCurrentUserGender(int gender) {
        MCLibUserInfo userInfo = getCurrentUserInfo();
        if (userInfo != null) {
            userInfo.setUserSex(gender);
            addOrUpdateUserInfo(userInfo);
        }
    }

    public void updateLoginStatus(boolean isSavePwd, boolean isAutoLogin) {
        int i;
        int i2;
        MCLibUserInfo userInfo = getCurrentUserInfo();
        if (userInfo != null) {
            if (isSavePwd) {
                i = 1;
            } else {
                i = 0;
            }
            userInfo.setIsSavePwd(i);
            if (isAutoLogin) {
                i2 = 1;
            } else {
                i2 = 0;
            }
            userInfo.setIsAutoLogin(i2);
            addOrUpdateUserInfo(userInfo);
        }
    }

    public boolean isAutoLogin() {
        MCLibUserInfo userInfo = getCurrentUserInfo();
        if (userInfo == null) {
            return true;
        }
        return userInfo.getIsAutoLogin() == 1;
    }

    public boolean isSavePwd() {
        MCLibUserInfo userInfo = getCurrentUserInfo();
        if (userInfo == null) {
            return true;
        }
        return userInfo.getIsSavePwd() == 1;
    }

    public MCLibUserInfo getUserInfo(int uid) {
        SQLiteDatabase db = null;
        Cursor c = null;
        try {
            SQLiteDatabase db2 = openDB();
            Cursor c2 = db2.query(SharedUserDBConstant.TABLE_USER_SHARE, null, "uid=" + uid, null, null, null, null);
            if (c2.moveToFirst()) {
                MCLibUserInfo buildShareUserInfoModel = ShareUserDBUtilHelper.buildShareUserInfoModel(c2);
                if (c2 != null) {
                    c2.close();
                }
                if (db2 == null) {
                    return buildShareUserInfoModel;
                }
                db2.close();
                return buildShareUserInfoModel;
            }
            if (c2 != null) {
                c2.close();
            }
            if (db2 != null) {
                db2.close();
            }
            return null;
        } catch (Exception e) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            throw th;
        }
    }

    public MCLibUserInfo getUserInfo(String userName) {
        SQLiteDatabase db = null;
        Cursor c = null;
        try {
            SQLiteDatabase db2 = openDB();
            Cursor c2 = db2.query(SharedUserDBConstant.TABLE_USER_SHARE, null, "userName=" + userName, null, null, null, null);
            if (c2.moveToFirst()) {
                MCLibUserInfo buildShareUserInfoModel = ShareUserDBUtilHelper.buildShareUserInfoModel(c2);
                if (c2 != null) {
                    c2.close();
                }
                if (db2 == null) {
                    return buildShareUserInfoModel;
                }
                db2.close();
                return buildShareUserInfoModel;
            }
            if (c2 != null) {
                c2.close();
            }
            if (db2 != null) {
                db2.close();
            }
            return null;
        } catch (Exception e) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            throw th;
        }
    }

    public MCLibUserInfo getCurrentUserInfo() {
        SQLiteDatabase db = null;
        Cursor c = null;
        try {
            SQLiteDatabase db2 = openDB();
            Cursor c2 = db2.query(SharedUserDBConstant.TABLE_USER_SHARE, null, "isCurrentUser=1", null, null, null, null);
            if (c2.moveToFirst()) {
                MCLibUserInfo buildShareUserInfoModel = ShareUserDBUtilHelper.buildShareUserInfoModel(c2);
                if (c2 != null) {
                    c2.close();
                }
                if (db2 == null) {
                    return buildShareUserInfoModel;
                }
                db2.close();
                return buildShareUserInfoModel;
            }
            if (c2 != null) {
                c2.close();
            }
            if (db2 != null) {
                db2.close();
            }
            return null;
        } catch (Exception e) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            throw th;
        }
    }

    public boolean addOrUpdateUserInfo(MCLibUserInfo userInfo) {
        if (!tabbleIsExist(SharedUserDBConstant.TABLE_USER_SHARE)) {
            getNewInstance(this.ctx);
        }
        SQLiteDatabase db = null;
        try {
            db = openDB();
            ContentValues values = new ContentValues();
            values.put("userName", userInfo.getName());
            values.put("nickName", userInfo.getNickName());
            values.put("email", userInfo.getEmail());
            values.put("userGender", Integer.valueOf(userInfo.getUserSex()));
            values.put(SharedUserDBConstant.COLUMN_USER_BIRTH_DAY, userInfo.getBirthday());
            values.put("userImage", userInfo.getImage());
            values.put("userCity", userInfo.getPos());
            values.put("userPwd", userInfo.getPassword());
            values.put("userMood", userInfo.getEmotionWords());
            values.put(SharedUserDBConstant.COLUMN_IS_SAVE_PWD, Integer.valueOf(userInfo.getIsSavePwd()));
            values.put(SharedUserDBConstant.COLUMN_IS_AUTO_LOGIN, Integer.valueOf(userInfo.getIsAutoLogin()));
            values.put(SharedUserDBConstant.COLUMN_IS_FORCE_SHOW_MAGIC_IMAGE, Integer.valueOf(userInfo.getIsForceShowMagicImage()));
            values.put(SharedUserDBConstant.COLUMN_IS_CURRENT_USER, Integer.valueOf(userInfo.getIsCurrentUser()));
            if (!isUserExisted(userInfo.getUid())) {
                values.put("uid", Integer.valueOf(userInfo.getUid()));
                db.insertOrThrow(SharedUserDBConstant.TABLE_USER_SHARE, null, values);
            } else {
                db.update(SharedUserDBConstant.TABLE_USER_SHARE, values, "uid=" + userInfo.getUid(), null);
            }
            if (db != null) {
                db.close();
            }
            return true;
        } catch (Exception e) {
            Log.e("addUser", "fail" + e.getMessage());
            if (db != null) {
                db.close();
            }
            return false;
        } catch (Throwable th) {
            if (db != null) {
                db.close();
            }
            throw th;
        }
    }

    public boolean isUserExisted(int uid) {
        return isRowExisted(SharedUserDBConstant.TABLE_USER_SHARE, "uid", (long) uid);
    }

    public List<MCLibUserInfo> getAllUsers() {
        SQLiteDatabase db = null;
        Cursor c = null;
        try {
            db = openDB();
            c = db.query(SharedUserDBConstant.TABLE_USER_SHARE, null, null, null, null, null, null);
            List<MCLibUserInfo> users = new ArrayList<>();
            for (int i = 0; i < c.getCount(); i++) {
                c.moveToPosition(i);
                users.add(ShareUserDBUtilHelper.buildShareUserInfoModel(c));
            }
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            return users;
        } catch (Exception e) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            return null;
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            throw th;
        }
    }

    public boolean setUserAsCurrentUserInfo(int uid) {
        boolean isSucc = false;
        List<MCLibUserInfo> allUsers = getAllUsers();
        if (allUsers == null) {
            return false;
        }
        for (MCLibUserInfo userInfo : allUsers) {
            if (userInfo.getUid() == uid) {
                isSucc = true;
                userInfo.setIsCurrentUser(1);
            } else {
                userInfo.setIsCurrentUser(0);
            }
            addOrUpdateUserInfo(userInfo);
        }
        return isSucc;
    }

    public boolean addOrUpdateSyncSite(int loginUid, int siteId, String userName) {
        SQLiteDatabase db = null;
        try {
            db = openDB();
            ContentValues values = new ContentValues();
            values.put(SharedUserDBConstant.COLUMN_SITE_USER_NAME, userName);
            values.put("uid", Integer.valueOf(loginUid));
            if (!isSyncSiteExisted(loginUid, siteId)) {
                values.put(SharedUserDBConstant.COLUMN_SITE_ID, Integer.valueOf(siteId));
                db.insertOrThrow(SharedUserDBConstant.TABLE_USER_SYNC_SITE, null, values);
            } else {
                db.update(SharedUserDBConstant.TABLE_USER_SYNC_SITE, values, "siteId=" + siteId + " and " + "uid" + "=" + loginUid, null);
            }
            if (db != null) {
                db.close();
            }
            return true;
        } catch (Exception e) {
            if (db != null) {
                db.close();
            }
            return false;
        } catch (Throwable th) {
            if (db != null) {
                db.close();
            }
            throw th;
        }
    }

    public boolean removeSyncSite(int loginUid, int siteId) {
        SQLiteDatabase db = null;
        try {
            db = openDB();
            db.delete(SharedUserDBConstant.TABLE_USER_SYNC_SITE, "siteId=" + siteId + " and " + "uid" + "=" + loginUid, null);
            if (db != null) {
                db.close();
            }
            return true;
        } catch (Exception e) {
            if (db != null) {
                db.close();
            }
            return false;
        } catch (Throwable th) {
            if (db != null) {
                db.close();
            }
            throw th;
        }
    }

    public boolean isSyncSiteExisted(int userId, int siteId) {
        SQLiteDatabase db = null;
        Cursor c = null;
        try {
            db = openDB();
            c = db.query(SharedUserDBConstant.TABLE_USER_SYNC_SITE, null, "siteId=" + siteId + " and " + "uid" + "=" + userId, null, null, null, null);
            boolean isExist = false;
            if (c.getCount() > 0) {
                isExist = true;
            }
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            return isExist;
        } catch (Exception e) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            return false;
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            throw th;
        }
    }

    public HashMap<Integer, String> getAllSyncSites(int userId) {
        HashMap<Integer, String> map = new HashMap<>();
        SQLiteDatabase db = null;
        Cursor c = null;
        try {
            db = openDB();
            c = db.query(SharedUserDBConstant.TABLE_USER_SYNC_SITE, null, "uid=" + userId, null, null, null, null);
            for (int i = 0; i < c.getCount(); i++) {
                c.moveToPosition(i);
                map.put(Integer.valueOf(c.getInt(0)), c.getString(2));
            }
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
        } catch (Exception e) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            throw th;
        }
        return map;
    }
}
