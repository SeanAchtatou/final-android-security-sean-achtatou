package com.mobcent.android.db.helper;

import android.database.Cursor;
import com.mobcent.android.model.MCLibMobcentSoftwareInfo;

public class MobcentSoftwareDBUtilHelper {
    public static MCLibMobcentSoftwareInfo buildMobcentSoftwareInfo(Cursor c) {
        MCLibMobcentSoftwareInfo model = new MCLibMobcentSoftwareInfo();
        model.setSid(c.getInt(0));
        model.setSoftwareName(c.getString(1));
        model.setSoftwareDownloadPath(c.getString(2));
        model.setSoftwareType(c.getInt(3));
        return model;
    }
}
