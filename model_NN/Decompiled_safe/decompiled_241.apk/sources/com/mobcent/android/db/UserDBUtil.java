package com.mobcent.android.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.mobcent.android.db.constants.UserDBConstant;
import com.mobcent.android.db.helper.UserDBUtilHelper;
import com.mobcent.android.model.MCLibUserInfo;
import java.util.ArrayList;
import java.util.List;

public class UserDBUtil extends BaseDBUtil implements UserDBConstant {
    private static final String DATABASE_NAME = "mobcent_users.db";
    private static final String DROP_TABLE_USER_FRIENDS = "DROP TABLE UserFriends";
    private static final String SQL_CREATE_TABLE_USER_FRIENDS = "CREATE TABLE IF NOT EXISTS UserFriends(uid INTEGER PRIMARY KEY,userName TEXT,userGender TEXT,userAge TEXT,userCity TEXT,userImage TEXT,userMood TEXT,isFollowed INTEGER,isFriend INTEGER,loginUid INTEGER,userFanNun INTEGER);";
    public static final int TIME_USER_FRIENDS_ID = 100;
    private static UserDBUtil mobcentDBUtil;
    private Context ctx;

    protected UserDBUtil(Context ctx2) {
        this.ctx = ctx2;
        SQLiteDatabase db = null;
        try {
            db = openDB();
            db.execSQL(SQL_CREATE_TABLE_USER_FRIENDS);
            db.execSQL(BaseDBUtil.SQL_CREATE_TABLE_LAST_UPDATE_TIME);
        } finally {
            db.close();
        }
    }

    public void dropAllTables() {
        SQLiteDatabase db = null;
        try {
            db = openDB();
            db.execSQL(DROP_TABLE_USER_FRIENDS);
            db.execSQL(BaseDBUtil.DROP_TABLE_LAST_UPDATE_TIME);
        } finally {
            db.close();
        }
    }

    public static UserDBUtil getInstance(Context context) {
        if (mobcentDBUtil == null) {
            mobcentDBUtil = new UserDBUtil(context);
        }
        return mobcentDBUtil;
    }

    /* access modifiers changed from: protected */
    public SQLiteDatabase openDB() {
        return this.ctx.openOrCreateDatabase(DATABASE_NAME, 0, null);
    }

    public boolean addOrUpdateUserFriends(int loginUid, List<MCLibUserInfo> userInfos) {
        int i;
        int i2;
        if (userInfos == null) {
            return true;
        }
        SQLiteDatabase db = null;
        try {
            db = openDB();
            for (MCLibUserInfo userInfo : userInfos) {
                ContentValues values = new ContentValues();
                values.put("uid", Integer.valueOf(userInfo.getUid()));
                values.put("userName", userInfo.getName());
                values.put("userGender", userInfo.getGender());
                values.put(UserDBConstant.COLUMN_USER_AGE, userInfo.getAge());
                values.put("userCity", userInfo.getPos());
                values.put("userImage", userInfo.getImage());
                values.put("userMood", userInfo.getEmotionWords());
                if (userInfo.isFollowed()) {
                    i = 1;
                } else {
                    i = 0;
                }
                values.put(UserDBConstant.COLUMN_USER_IS_FOLLOWED, Integer.valueOf(i));
                if (userInfo.isFriend()) {
                    i2 = 1;
                } else {
                    i2 = 0;
                }
                values.put(UserDBConstant.COLUMN_USER_IS_FRIEND, Integer.valueOf(i2));
                values.put("loginUid", Integer.valueOf(loginUid));
                values.put(UserDBConstant.COLUMN_USER_FANS_NUM, Integer.valueOf(userInfo.getFansNum()));
                if (!isRowExisted(UserDBConstant.TABLE_USER_FRIENDS, "uid", (long) userInfo.getUid())) {
                    values.put("uid", Integer.valueOf(userInfo.getUid()));
                    db.insertOrThrow(UserDBConstant.TABLE_USER_FRIENDS, null, values);
                } else {
                    db.update(UserDBConstant.TABLE_USER_FRIENDS, values, "uid=" + userInfo.getUid(), null);
                }
            }
            if (db != null) {
                db.close();
            }
            return true;
        } catch (Exception e) {
            if (db != null) {
                db.close();
            }
            return false;
        } catch (Throwable th) {
            if (db != null) {
                db.close();
            }
            throw th;
        }
    }

    public List<MCLibUserInfo> getUserFollowedFriends(int loginUid, int page, int pageSize) {
        List<MCLibUserInfo> userInfos = new ArrayList<>();
        SQLiteDatabase db = null;
        Cursor c = null;
        try {
            db = openDB();
            c = db.rawQuery("select * from UserFriends where loginUid=" + loginUid + " limit " + pageSize + " offset " + ((page - 1) * pageSize), null);
            int totalNum = getUserFollowedFriendsCount(loginUid);
            if (c.getCount() > 0 && page == 1) {
                MCLibUserInfo model = getRefreshUserModel(getLastUpdateTime(100));
                model.setTotalNum(totalNum);
                model.setCurrentPage(page);
                userInfos.add(model);
            } else if (c.getCount() <= 0 && page == 1) {
                if (c != null) {
                    c.close();
                }
                if (db != null) {
                    db.close();
                }
                return userInfos;
            } else if (c.getCount() <= 0) {
                MCLibUserInfo model2 = getRefreshUserModel(getLastUpdateTime(100));
                model2.setTotalNum(totalNum);
                model2.setCurrentPage(page);
                userInfos.add(model2);
            }
            for (int i = 0; i < c.getCount(); i++) {
                c.moveToPosition(i);
                userInfos.add(UserDBUtilHelper.buildUserInfoModel(c, totalNum, page));
            }
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
        } catch (Exception e) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            throw th;
        }
        return userInfos;
    }

    public List<MCLibUserInfo> getUserFriends(int loginUid, int page, int pageSize) {
        List<MCLibUserInfo> userInfos = new ArrayList<>();
        SQLiteDatabase db = null;
        Cursor c = null;
        try {
            db = openDB();
            c = db.rawQuery("select * from UserFriends where loginUid=" + loginUid + " limit " + pageSize + " offset " + ((page - 1) * pageSize), null);
            int totalNum = getUserFollowedFriendsCount(loginUid);
            for (int i = 0; i < c.getCount(); i++) {
                c.moveToPosition(i);
                userInfos.add(UserDBUtilHelper.buildUserInfoModel(c, totalNum, page));
            }
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
        } catch (Exception e) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            throw th;
        }
        return userInfos;
    }

    public int getUserFollowedFriendsCount(int loginUid) {
        SQLiteDatabase db = null;
        Cursor c = null;
        try {
            SQLiteDatabase db2 = openDB();
            Cursor c2 = db2.rawQuery("select count(*) from UserFriends where loginUid=" + loginUid, null);
            if (c2.moveToFirst()) {
                int i = c2.getInt(0);
                if (c2 != null) {
                    c2.close();
                }
                if (db2 == null) {
                    return i;
                }
                db2.close();
                return i;
            }
            if (c2 != null) {
                c2.close();
            }
            if (db2 != null) {
                db2.close();
            }
            return 0;
        } catch (Exception e) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            return 0;
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            throw th;
        }
    }

    public static MCLibUserInfo getRefreshUserModel(String time) {
        MCLibUserInfo model = new MCLibUserInfo();
        model.setRefreshNeeded(true);
        model.setReadFromLocal(true);
        model.setLastUpdateTime(time);
        return model;
    }
}
