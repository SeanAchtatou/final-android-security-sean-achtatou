package com.mobcent.android.db.constants;

public interface QuickMsgDBConstant {
    public static final String COLUMN_LOGIN_USER_ID = "loginUid";
    public static final String COLUMN_QUICK_MSG = "quickMsg";
    public static final String TABLE_QUICK_MSG = "TQuickMessage";
}
