package com.mobcent.android.db;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.mobcent.android.db.constants.UserMagicActionDictDBConstant;
import com.mobcent.android.db.helper.UserMagicActionDictDBUtilHelper;
import com.mobcent.android.model.MCLibActionModel;
import com.mobcent.android.utils.MCLibIOUtil;
import java.util.ArrayList;
import java.util.List;

public class UserMagicActionDictDBUtil extends BaseDBUtil implements UserMagicActionDictDBConstant {
    private static final String DATABASE_NAME = "mobcent_mad.db";
    private static final String DROP_TABLE_USER_MAGIC_ACTION_DICT = "DROP TABLE TUserMagicActionDict";
    private static final String SQL_CREATE_TABLE_USER_MAGIC_ACTION_DICT = "CREATE TABLE IF NOT EXISTS TUserMagicActionDict(umaId INTEGER PRIMARY KEY,shortName TEXT,content TEXT,baseUrl TEXT,previewImage TEXT,icon TEXT);";
    private static UserMagicActionDictDBUtil mobcentDBUtil;
    private final String BP_APP_KEY = "BP_APP_KEY";
    private final String COMMUNITY_CURRENT_MODE = "COMMUNITY_CURRENT_MODE";
    private final String IS_ACTION_ENABLE = "IS_ACTION_ENABLE";
    private final String IS_SOUND_ENABLE = "IS_SOUND_ENABLE";
    private final String LAST_MAGIC_ACTION_UPDATE_TIME = "LAST_MAGIC_ACTION_UPDATE_TIME";
    private Context ctx;
    private SharedPreferences prefs = null;

    protected UserMagicActionDictDBUtil(Context ctx2) {
        this.ctx = ctx2;
        this.prefs = ctx2.getSharedPreferences(MCLibIOUtil.PREFS_FILE, 3);
        SQLiteDatabase db = null;
        try {
            db = openDB();
            db.execSQL(SQL_CREATE_TABLE_USER_MAGIC_ACTION_DICT);
        } finally {
            db.close();
        }
    }

    public void dropAllTables() {
        SQLiteDatabase db = null;
        try {
            db = openDB();
            db.execSQL(DROP_TABLE_USER_MAGIC_ACTION_DICT);
        } finally {
            db.close();
        }
    }

    public static UserMagicActionDictDBUtil getInstance(Context context) {
        if (mobcentDBUtil == null) {
            mobcentDBUtil = new UserMagicActionDictDBUtil(context);
        }
        return mobcentDBUtil;
    }

    /* access modifiers changed from: protected */
    public SQLiteDatabase openDB() {
        return this.ctx.openOrCreateDatabase(DATABASE_NAME, 0, null);
    }

    public boolean addUserMagicAction(MCLibActionModel actionModel) {
        SQLiteDatabase db = null;
        try {
            db = openDB();
            ContentValues values = new ContentValues();
            values.put("content", actionModel.getContent());
            values.put(UserMagicActionDictDBConstant.COLUMN_SHORT_NAME, actionModel.getShortName());
            values.put("baseUrl", actionModel.getBaseUrl());
            values.put("icon", actionModel.getIcon());
            values.put(UserMagicActionDictDBConstant.COLUMN_PREVIEW_IMAGE, actionModel.getActionPrevImg());
            if (isMagicActionExisted(actionModel.getActionId())) {
                db.update(UserMagicActionDictDBConstant.TABLE_USER_MAGIC_ACTION_DICT, values, "umaId=" + actionModel.getActionId(), null);
            } else {
                values.put(UserMagicActionDictDBConstant.COLUMN_UMA_ID, Integer.valueOf(actionModel.getActionId()));
                db.insertOrThrow(UserMagicActionDictDBConstant.TABLE_USER_MAGIC_ACTION_DICT, null, values);
            }
            if (db != null) {
                db.close();
            }
            return true;
        } catch (Exception e) {
            if (db != null) {
                db.close();
            }
            return false;
        } catch (Throwable th) {
            if (db != null) {
                db.close();
            }
            throw th;
        }
    }

    public boolean isMagicActionExisted(int id) {
        SQLiteDatabase db = null;
        Cursor c = null;
        try {
            db = openDB();
            c = db.rawQuery("select * from TUserMagicActionDict where umaId=" + id, null);
            if (c.getCount() > 0) {
                if (c != null) {
                    c.close();
                }
                if (db != null) {
                    db.close();
                }
                return true;
            }
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            return false;
        } catch (Exception e) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            return true;
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            throw th;
        }
    }

    public List<MCLibActionModel> getUserMagicActionList(int limit) {
        List<MCLibActionModel> magicActionList = new ArrayList<>();
        SQLiteDatabase db = null;
        Cursor c = null;
        try {
            db = openDB();
            String sql = "select * from TUserMagicActionDict order by umaId desc ";
            if (limit > 0) {
                sql = String.valueOf(sql) + "limit " + limit;
            }
            c = db.rawQuery(sql, null);
            for (int i = 0; i < c.getCount(); i++) {
                c.moveToPosition(i);
                magicActionList.add(UserMagicActionDictDBUtilHelper.buildMagicActionModel(c));
            }
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
        } catch (Exception e) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            throw th;
        }
        return magicActionList;
    }

    public List<MCLibActionModel> getUserMagicActionById(int id) {
        List<MCLibActionModel> magicActionList = new ArrayList<>();
        SQLiteDatabase db = null;
        Cursor c = null;
        try {
            db = openDB();
            c = db.rawQuery("select * from TUserMagicActionDict where umaId=" + id + " order by " + UserMagicActionDictDBConstant.COLUMN_UMA_ID + " desc limit 1", null);
            for (int i = 0; i < c.getCount(); i++) {
                c.moveToPosition(i);
                magicActionList.add(UserMagicActionDictDBUtilHelper.buildMagicActionModel(c));
            }
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
        } catch (Exception e) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            throw th;
        }
        return magicActionList;
    }

    public String getMagicActionTypeTime() {
        return this.prefs.getString("LAST_MAGIC_ACTION_UPDATE_TIME", "");
    }

    public void updateMagicActionTypeTime(String time) {
        SharedPreferences.Editor editor = this.prefs.edit();
        editor.putString("LAST_MAGIC_ACTION_UPDATE_TIME", time);
        editor.commit();
    }

    public boolean isActionEnable() {
        return this.prefs.getBoolean("IS_ACTION_ENABLE", false);
    }

    public void updateActionEnable(boolean isActionEnable) {
        SharedPreferences.Editor editor = this.prefs.edit();
        editor.putBoolean("IS_ACTION_ENABLE", isActionEnable);
        editor.commit();
    }

    public boolean isSoundEnable() {
        return this.prefs.getBoolean("IS_SOUND_ENABLE", true);
    }

    public void updateSoundEnable(boolean isSoundEnable) {
        SharedPreferences.Editor editor = this.prefs.edit();
        editor.putBoolean("IS_SOUND_ENABLE", isSoundEnable);
        editor.commit();
    }

    public void updateCommunityHomeMode(int mode) {
        SharedPreferences.Editor editor = this.prefs.edit();
        editor.putInt("COMMUNITY_CURRENT_MODE", mode);
        editor.commit();
    }

    public int getCommunityHomeMode() {
        return this.prefs.getInt("COMMUNITY_CURRENT_MODE", 1);
    }

    public void updateAppKey(String appKey) {
        String existAppKey = getAppKey();
        if (existAppKey == null || !existAppKey.equals(appKey)) {
            SharedPreferences.Editor editor = this.prefs.edit();
            editor.putString("BP_APP_KEY", appKey);
            editor.commit();
        }
    }

    public String getAppKey() {
        return this.prefs.getString("BP_APP_KEY", "");
    }
}
