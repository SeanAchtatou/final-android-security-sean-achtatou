package com.mobcent.android.db.helper;

import android.database.Cursor;
import com.mobcent.android.model.MCLibActionModel;
import com.mobcent.android.model.MCLibUserStatus;

public class DBUtilHelper {
    public static MCLibActionModel buildBasicActionModel(Cursor c) {
        MCLibActionModel actionModel = new MCLibActionModel();
        actionModel.setActionId(c.getInt(0));
        actionModel.setShortName(c.getString(1));
        actionModel.setContent(c.getString(2));
        return actionModel;
    }

    public static MCLibActionModel buildLocalActionModel(Cursor c) {
        MCLibActionModel actionModel = new MCLibActionModel();
        actionModel.setId(c.getInt(0));
        actionModel.setActionId(c.getInt(2));
        actionModel.setContent(c.getString(3));
        actionModel.setFromUserId(c.getInt(4));
        actionModel.setFromUserName(c.getString(5));
        actionModel.setTime(c.getString(6));
        return actionModel;
    }

    public static MCLibUserStatus buildPrivateMsgModel(Cursor c, int totalNum) {
        MCLibUserStatus userStatus = new MCLibUserStatus();
        userStatus.setStatusId((long) c.getInt(0));
        userStatus.setUid(c.getInt(1));
        userStatus.setContent(c.getString(2));
        userStatus.setFromUserId(c.getInt(3));
        userStatus.setFromUserName(c.getString(4));
        userStatus.setTime(c.getString(5));
        userStatus.setUserImageUrl(c.getString(6));
        userStatus.setIsRead(c.getInt(7));
        userStatus.setSourceProId(c.getInt(8));
        userStatus.setSourceName(c.getString(9));
        userStatus.setSourceUrl(c.getString(10));
        userStatus.setSourceCategoryId(c.getInt(11));
        userStatus.setShareUrl(c.getString(12));
        userStatus.setShareProId(c.getInt(13));
        userStatus.setShareCategoryId(c.getInt(14));
        userStatus.setTotalNum(totalNum);
        return userStatus;
    }
}
