package com.mobcent.android.db.constants;

public interface SharedUserDBConstant {
    public static final String COLUMN_IS_AUTO_LOGIN = "isAutoLogin";
    public static final String COLUMN_IS_CURRENT_USER = "isCurrentUser";
    public static final String COLUMN_IS_FORCE_SHOW_MAGIC_IMAGE = "isForceShowMI";
    public static final String COLUMN_IS_SAVE_PWD = "isSavePwd";
    public static final String COLUMN_SITE_ID = "siteId";
    public static final String COLUMN_SITE_USER_NAME = "siteUserName";
    public static final String COLUMN_USER_BIRTH_DAY = "userBirthday";
    public static final String COLUMN_USER_CITY = "userCity";
    public static final String COLUMN_USER_EMAIL = "email";
    public static final String COLUMN_USER_GENDER = "userGender";
    public static final String COLUMN_USER_ID = "uid";
    public static final String COLUMN_USER_IMAGE = "userImage";
    public static final String COLUMN_USER_MOOD = "userMood";
    public static final String COLUMN_USER_NAME = "userName";
    public static final String COLUMN_USER_NICK_NAME = "nickName";
    public static final String COLUMN_USER_PWD = "userPwd";
    public static final int IS_AUTO_LOGIN = 1;
    public static final int IS_CURRENT_USER = 1;
    public static final int IS_FORCE_SHOW_MAGIC_IMAGE = 1;
    public static final int IS_SAVE_PWD = 1;
    public static final int NOT_AUTO_LOGIN = 0;
    public static final int NOT_CURRENT_USER = 0;
    public static final int NOT_FORCE_SHOW_MAGIC_IAMGE = 0;
    public static final int NOT_SAVE_PWD = 0;
    public static final String TABLE_USER_SHARE = "TUserShare";
    public static final String TABLE_USER_SYNC_SITE = "TUserSyncSite";
}
