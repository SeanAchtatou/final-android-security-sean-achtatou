package com.mobcent.android.db.helper;

import android.database.Cursor;
import com.mobcent.android.model.MCLibDownloadProfileModel;

public class DownloadAppDBUtilHelper {
    public static MCLibDownloadProfileModel buildDownloadProfileModel(Cursor c) {
        MCLibDownloadProfileModel model = new MCLibDownloadProfileModel();
        model.setAppId(c.getInt(0));
        model.setAppName(c.getString(1));
        model.setUrl(c.getString(2));
        model.setDownloadSize(c.getInt(3));
        model.setFileSize(c.getInt(4));
        model.setSpeed(c.getInt(5));
        model.setStopReq(c.getInt(6));
        model.setStatus(c.getInt(7));
        model.setAppType(c.getInt(8));
        model.setVersion(c.getString(9));
        model.setClientId(c.getInt(10));
        model.setDownloadTime(c.getLong(11));
        return model;
    }
}
