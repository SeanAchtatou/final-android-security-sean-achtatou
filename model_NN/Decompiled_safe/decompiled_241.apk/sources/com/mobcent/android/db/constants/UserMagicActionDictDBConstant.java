package com.mobcent.android.db.constants;

public interface UserMagicActionDictDBConstant {
    public static final String COLUMN_BASE_URL = "baseUrl";
    public static final String COLUMN_CONTENT = "content";
    public static final String COLUMN_ICON = "icon";
    public static final String COLUMN_PREVIEW_IMAGE = "previewImage";
    public static final String COLUMN_SHORT_NAME = "shortName";
    public static final String COLUMN_UMA_ID = "umaId";
    public static final String TABLE_USER_MAGIC_ACTION_DICT = "TUserMagicActionDict";
}
