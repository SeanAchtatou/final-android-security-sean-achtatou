package com.mobcent.android.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.mobcent.android.db.constants.PrivateMsgDBConstant;
import com.mobcent.android.db.helper.PrivateMsgDBUtilHelper;
import com.mobcent.android.model.MCLibUserStatus;
import com.mobcent.android.utils.DateUtil;
import com.mobcent.android.utils.MCLibIOUtil;
import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class PrivateMsgDBUtil extends BaseDBUtil implements PrivateMsgDBConstant {
    private static final String DATABASE_NAME = "msm";
    private static final String DROP_TABLE_PRIVATE_MSG = "DROP TABLE TPrivateMsg";
    private static final String SQL_CREATE_TABLE_PRIVATE_MSG = "CREATE TABLE IF NOT EXISTS TPrivateMsg(mid LONG PRIMARY KEY,loginUid INTEGER,content TEXT,fromUid INTEGER,fromUserName TEXT,icon TEXT,pubTime LONG,belongUid INTEGER,actionId INTEGER,isRead INTEGER);";
    private static PrivateMsgDBUtil mobcentDBUtil;
    private Context ctx;

    protected PrivateMsgDBUtil(Context ctx2) {
        this.ctx = ctx2;
        SQLiteDatabase db = null;
        try {
            db = openDB();
            db.execSQL(SQL_CREATE_TABLE_PRIVATE_MSG);
        } finally {
            db.close();
        }
    }

    public void dropAllTables() {
        SQLiteDatabase db = null;
        try {
            db = openDB();
            db.execSQL(DROP_TABLE_PRIVATE_MSG);
        } finally {
            db.close();
        }
    }

    public static PrivateMsgDBUtil getInstance(Context context) {
        if (mobcentDBUtil == null) {
            mobcentDBUtil = new PrivateMsgDBUtil(context);
        }
        return mobcentDBUtil;
    }

    /* access modifiers changed from: protected */
    public SQLiteDatabase openDB() {
        String basePath = String.valueOf(MCLibIOUtil.getBaseLocalLocation(this.ctx)) + MCLibIOUtil.FS + ".mc" + MCLibIOUtil.FS;
        File file = new File(basePath);
        if (!file.exists()) {
            file.mkdirs();
        }
        return SQLiteDatabase.openOrCreateDatabase(String.valueOf(basePath) + DATABASE_NAME, (SQLiteDatabase.CursorFactory) null);
    }

    public List<MCLibUserStatus> getPrivateMsgs(int loginUid, int belongUid, int page, int pageSize) {
        List<MCLibUserStatus> userPrivateMsgList = new ArrayList<>();
        int totalNum = getUserPrivateMsgCount(loginUid, belongUid);
        SQLiteDatabase db = null;
        Cursor c = null;
        try {
            db = openDB();
            c = db.rawQuery("select * from TPrivateMsg where loginUid=" + loginUid + " and " + PrivateMsgDBConstant.COLUMN_BELONG_UID + "=" + belongUid + " order by " + PrivateMsgDBConstant.COLUMN_MSG_ID + " asc limit " + pageSize + " offset " + ((page - 1) * pageSize), null);
            for (int i = 0; i < c.getCount(); i++) {
                c.moveToPosition(i);
                userPrivateMsgList.add(PrivateMsgDBUtilHelper.buildPrivateMsgModel(c, totalNum));
            }
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
        } catch (Exception e) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            throw th;
        }
        return userPrivateMsgList;
    }

    public int getUserPrivateMsgCount(int uid, int belongUid) {
        SQLiteDatabase db = null;
        Cursor c = null;
        try {
            SQLiteDatabase db2 = openDB();
            Cursor c2 = db2.rawQuery("select count(*) from TPrivateMsg where loginUid=" + uid + " and " + PrivateMsgDBConstant.COLUMN_BELONG_UID + "=" + belongUid, null);
            if (c2.moveToFirst()) {
                int i = c2.getInt(0);
                if (c2 != null) {
                    c2.close();
                }
                if (db2 == null) {
                    return i;
                }
                db2.close();
                return i;
            }
            if (c2 != null) {
                c2.close();
            }
            if (db2 != null) {
                db2.close();
            }
            return 0;
        } catch (Exception e) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            return 0;
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            throw th;
        }
    }

    public boolean removeUserMsg(int mid) {
        SQLiteDatabase db = null;
        try {
            db = openDB();
            db.delete(PrivateMsgDBConstant.TABLE_PRIVATE_MSG, "mid=" + mid, null);
            if (db != null) {
                db.close();
            }
            return true;
        } catch (Exception e) {
            if (db != null) {
                db.close();
            }
            return false;
        } catch (Throwable th) {
            if (db != null) {
                db.close();
            }
            throw th;
        }
    }

    public boolean removeAllUserMsg(int loginUid, int belongUid) {
        SQLiteDatabase db = null;
        try {
            db = openDB();
            db.delete(PrivateMsgDBConstant.TABLE_PRIVATE_MSG, "loginUid=" + loginUid + " and " + PrivateMsgDBConstant.COLUMN_BELONG_UID + "=" + belongUid, null);
            if (db != null) {
                db.close();
            }
            return true;
        } catch (Exception e) {
            if (db != null) {
                db.close();
            }
            return false;
        } catch (Throwable th) {
            if (db != null) {
                db.close();
            }
            throw th;
        }
    }

    public boolean addOrUpdatePrivateMsg(int loginUid, MCLibUserStatus userStatus, long indent) {
        Date date;
        SQLiteDatabase db = null;
        try {
            SQLiteDatabase db2 = openDB();
            ContentValues values = new ContentValues();
            values.put(PrivateMsgDBConstant.COLUMN_MSG_ID, Long.valueOf(userStatus.getStatusId()));
            values.put("loginUid", Integer.valueOf(loginUid));
            values.put("content", userStatus.getContent());
            values.put(PrivateMsgDBConstant.COLUMN_FROM_UID, Integer.valueOf(userStatus.getFromUserId()));
            values.put("fromUserName", userStatus.getFromUserName());
            values.put("icon", userStatus.getUserImageUrl());
            long timeInMillis = Calendar.getInstance().getTimeInMillis();
            if (!(userStatus.getTime() == null || "".equals(userStatus.getTime()) || (date = DateUtil.getTimestampByTimeString(userStatus.getTime())) == null)) {
                timeInMillis = date.getTime();
            }
            values.put("pubTime", Long.valueOf(timeInMillis + indent));
            values.put(PrivateMsgDBConstant.COLUMN_BELONG_UID, Integer.valueOf(userStatus.getBelongUid()));
            values.put("actionId", Integer.valueOf(userStatus.getActionId()));
            values.put(PrivateMsgDBConstant.COLUMN_IS_READ, Integer.valueOf(userStatus.getIsRead()));
            db2.insertOrThrow(PrivateMsgDBConstant.TABLE_PRIVATE_MSG, null, values);
            if (db2 != null) {
                db2.close();
            }
            return true;
        } catch (Exception e) {
            if (db != null) {
                db.close();
            }
            return false;
        } catch (Throwable th) {
            if (db != null) {
                db.close();
            }
            throw th;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void updateUserPrivateMsgRead(MCLibUserStatus userStatus, int loginUid) {
        SQLiteDatabase db = null;
        try {
            SQLiteDatabase db2 = openDB();
            ContentValues values = new ContentValues();
            values.put("loginUid", Integer.valueOf(loginUid));
            values.put("content", userStatus.getContent());
            values.put(PrivateMsgDBConstant.COLUMN_FROM_UID, Integer.valueOf(userStatus.getFromUserId()));
            values.put("fromUserName", userStatus.getFromUserName());
            values.put("icon", userStatus.getUserImageUrl());
            values.put("pubTime", Long.valueOf(new Long(userStatus.getTime()).longValue()));
            values.put(PrivateMsgDBConstant.COLUMN_BELONG_UID, Integer.valueOf(userStatus.getBelongUid()));
            values.put("actionId", Integer.valueOf(userStatus.getActionId()));
            values.put(PrivateMsgDBConstant.COLUMN_IS_READ, (Integer) 0);
            if (isRowExisted(PrivateMsgDBConstant.TABLE_PRIVATE_MSG, PrivateMsgDBConstant.COLUMN_MSG_ID, userStatus.getStatusId())) {
                db2.update(PrivateMsgDBConstant.TABLE_PRIVATE_MSG, values, "mid=" + userStatus.getStatusId(), null);
            }
            if (db2 != null) {
                db2.close();
            }
        } catch (Exception e) {
            if (db != null) {
                db.close();
            }
        } catch (Throwable th) {
            if (db != null) {
                db.close();
            }
            throw th;
        }
    }

    public int getUserUnreadMsgCount(int loginUid) {
        SQLiteDatabase db = null;
        Cursor c = null;
        try {
            SQLiteDatabase db2 = openDB();
            Cursor c2 = db2.rawQuery("select count(*) from TPrivateMsg where loginUid=" + loginUid + " and " + PrivateMsgDBConstant.COLUMN_IS_READ + "=" + 1, null);
            if (c2.moveToFirst()) {
                int count = c2.getInt(0);
                if (c2 != null) {
                    c2.close();
                }
                if (db2 != null) {
                    db2.close();
                }
                return count;
            }
            if (c2 != null) {
                c2.close();
            }
            if (db2 != null) {
                db2.close();
            }
            return 0;
        } catch (Exception e) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            return 0;
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            throw th;
        }
    }

    public List<MCLibUserStatus> getUserUnreadMsgs(int loginUid, int belongUid) {
        List<MCLibUserStatus> userPrivateMsgList = new ArrayList<>();
        SQLiteDatabase db = null;
        Cursor c = null;
        try {
            db = openDB();
            c = db.rawQuery("select * from TPrivateMsg where loginUid=" + loginUid + " and " + PrivateMsgDBConstant.COLUMN_BELONG_UID + "=" + belongUid + " and " + PrivateMsgDBConstant.COLUMN_IS_READ + "=" + 1 + " order by " + PrivateMsgDBConstant.COLUMN_MSG_ID + " asc", null);
            for (int i = 0; i < c.getCount(); i++) {
                c.moveToPosition(i);
                userPrivateMsgList.add(PrivateMsgDBUtilHelper.buildPrivateMsgModel(c, 0));
            }
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
        } catch (Exception e) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            throw th;
        }
        return userPrivateMsgList;
    }

    public int getUserUnreadActionCount(int loginUid) {
        SQLiteDatabase db = null;
        Cursor c = null;
        try {
            SQLiteDatabase db2 = openDB();
            Cursor c2 = db2.rawQuery("select count(*) from TPrivateMsg where loginUid=" + loginUid + " and " + PrivateMsgDBConstant.COLUMN_IS_READ + "=" + 1 + " and " + "actionId" + ">0", null);
            if (c2.moveToFirst()) {
                int count = c2.getInt(0);
                if (c2 != null) {
                    c2.close();
                }
                if (db2 != null) {
                    db2.close();
                }
                return count;
            }
            if (c2 != null) {
                c2.close();
            }
            if (db2 != null) {
                db2.close();
            }
            return 0;
        } catch (Exception e) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            return 0;
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            throw th;
        }
    }
}
