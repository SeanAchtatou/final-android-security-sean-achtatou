package com.mobcent.android.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.mobcent.android.db.constants.QuickMsgDBConstant;
import com.mobcent.android.db.helper.QuickMsgDBUtilHelper;
import com.mobcent.android.utils.MCLibIOUtil;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class QuickMsgDBUtil extends BaseDBUtil implements QuickMsgDBConstant {
    private static final String DATABASE_NAME = "ms";
    private static final String DROP_TABLE_QUICK_MSG = "DROP TABLE TQuickMessage";
    private static final String SQL_CREATE_TABLE_QUICK_MSG = "CREATE TABLE IF NOT EXISTS TQuickMessage(loginUid INTEGER,quickMsg TEXT);";
    private static QuickMsgDBUtil mobcentDBUtil;
    private Context ctx;

    protected QuickMsgDBUtil(Context ctx2) {
        this.ctx = ctx2;
        SQLiteDatabase db = null;
        try {
            db = openDB();
            db.execSQL(SQL_CREATE_TABLE_QUICK_MSG);
        } finally {
            db.close();
        }
    }

    public void dropAllTables() {
        SQLiteDatabase db = null;
        try {
            db = openDB();
            db.execSQL(DROP_TABLE_QUICK_MSG);
        } finally {
            db.close();
        }
    }

    public static QuickMsgDBUtil getInstance(Context context) {
        if (mobcentDBUtil == null) {
            mobcentDBUtil = new QuickMsgDBUtil(context);
        }
        return mobcentDBUtil;
    }

    /* access modifiers changed from: protected */
    public SQLiteDatabase openDB() {
        String basePath = String.valueOf(MCLibIOUtil.getBaseLocalLocation(this.ctx)) + MCLibIOUtil.FS + ".mc" + MCLibIOUtil.FS;
        File file = new File(basePath);
        if (!file.exists()) {
            file.mkdirs();
        }
        return SQLiteDatabase.openOrCreateDatabase(String.valueOf(basePath) + DATABASE_NAME, (SQLiteDatabase.CursorFactory) null);
    }

    public boolean addQuickMsg(int loginUid, String quickMsg) {
        if (quickMsg == null) {
            return true;
        }
        SQLiteDatabase db = null;
        try {
            db = openDB();
            ContentValues values = new ContentValues();
            values.put("loginUid", Integer.valueOf(loginUid));
            values.put(QuickMsgDBConstant.COLUMN_QUICK_MSG, quickMsg);
            db.insertOrThrow(QuickMsgDBConstant.TABLE_QUICK_MSG, null, values);
            if (db != null) {
                db.close();
            }
            return true;
        } catch (Exception e) {
            if (db != null) {
                db.close();
            }
            return false;
        } catch (Throwable th) {
            if (db != null) {
                db.close();
            }
            throw th;
        }
    }

    public List<String> getAllQuickMsgs(int loginUid) {
        List<String> quickMsgList = new ArrayList<>();
        SQLiteDatabase db = null;
        Cursor c = null;
        try {
            db = openDB();
            c = db.rawQuery("select * from TQuickMessage where loginUid=" + loginUid, null);
            for (int i = 0; i < c.getCount(); i++) {
                c.moveToPosition(i);
                quickMsgList.add(QuickMsgDBUtilHelper.buildQuickMsgModel(c));
            }
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
        } catch (Exception e) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            throw th;
        }
        return quickMsgList;
    }

    public boolean removeQuickMsg(int loginUid, String quickMsg) {
        SQLiteDatabase db = null;
        try {
            db = openDB();
            db.delete(QuickMsgDBConstant.TABLE_QUICK_MSG, "quickMsg='" + quickMsg + "' and " + "loginUid" + "=" + loginUid, null);
            if (db != null) {
                db.close();
            }
            return true;
        } catch (Exception e) {
            if (db != null) {
                db.close();
            }
            return false;
        } catch (Throwable th) {
            if (db != null) {
                db.close();
            }
            throw th;
        }
    }
}
