package com.mobcent.android.db.helper;

import android.database.Cursor;
import com.mobcent.android.model.MCLibUserInfo;

public class ShareUserDBUtilHelper {
    public static MCLibUserInfo buildShareUserInfoModel(Cursor c) {
        MCLibUserInfo model = new MCLibUserInfo();
        model.setUid(c.getInt(0));
        model.setName(c.getString(1));
        model.setNickName(c.getString(2));
        model.setEmail(c.getString(3));
        model.setUserSex(c.getInt(4));
        model.setBirthday(c.getString(5));
        model.setImage(c.getString(6));
        model.setPos(c.getString(7));
        model.setPassword(c.getString(8));
        model.setEmotionWords(c.getString(9));
        model.setIsSavePwd(c.getInt(10));
        model.setIsAutoLogin(c.getInt(11));
        model.setIsForceShowMagicImage(c.getInt(12));
        model.setIsCurrentUser(c.getInt(13));
        return model;
    }
}
