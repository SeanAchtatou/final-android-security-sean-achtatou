package com.mobcent.android.db.constants;

public interface UserDBConstant {
    public static final String COLUMN_LOGIN_USER_ID = "loginUid";
    public static final String COLUMN_USER_AGE = "userAge";
    public static final String COLUMN_USER_CITY = "userCity";
    public static final String COLUMN_USER_FANS_NUM = "userFanNun";
    public static final String COLUMN_USER_GENDER = "userGender";
    public static final String COLUMN_USER_ID = "uid";
    public static final String COLUMN_USER_IMAGE = "userImage";
    public static final String COLUMN_USER_IS_FOLLOWED = "isFollowed";
    public static final String COLUMN_USER_IS_FRIEND = "isFriend";
    public static final String COLUMN_USER_MOOD = "userMood";
    public static final String COLUMN_USER_NAME = "userName";
    public static final String COLUMN_USER_PWD = "userPwd";
    public static final int IS_FOLLOWED = 1;
    public static final int IS_FRIEND = 1;
    public static final int IS_NOT_FOLLOWED = 0;
    public static final int IS_NOT_FRIEND = 0;
    public static final String TABLE_USER_FRIENDS = "UserFriends";
    public static final String TABLE_USER_REG = "UserReg";
}
