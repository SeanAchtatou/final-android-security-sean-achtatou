package com.mobcent.android.db;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.mobcent.android.db.constants.BaseDBConstant;

public abstract class BaseDBUtil implements BaseDBConstant {
    public static final String DROP_TABLE_LAST_UPDATE_TIME = "DROP TABLE LastUpdateTime";
    public static final String SQL_CREATE_TABLE_LAST_UPDATE_TIME = "CREATE TABLE IF NOT EXISTS LastUpdateTime(timeId INTEGER PRIMARY KEY,time TEXT);";

    /* access modifiers changed from: protected */
    public abstract SQLiteDatabase openDB();

    public boolean isRowExisted(String table, String column, long id) {
        SQLiteDatabase db = null;
        Cursor c = null;
        try {
            db = openDB();
            c = db.query(table, null, String.valueOf(column) + "=" + id, null, null, null, null);
            boolean isExist = false;
            if (c.getCount() > 0) {
                isExist = true;
            }
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            return isExist;
        } catch (Exception e) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            return false;
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            throw th;
        }
    }

    public boolean removeAllEntries(String tableName) {
        SQLiteDatabase db = null;
        try {
            db = openDB();
            db.delete(tableName, null, null);
            if (db != null) {
                db.close();
            }
            return true;
        } catch (Exception e) {
            if (db != null) {
                db.close();
            }
            return false;
        } catch (Throwable th) {
            if (db != null) {
                db.close();
            }
            throw th;
        }
    }

    public boolean addOrUpdateLastUpdateTime(int timeId, String time) {
        SQLiteDatabase db = null;
        try {
            db = openDB();
            ContentValues values = new ContentValues();
            values.put("time", time);
            if (!isRowExisted(BaseDBConstant.TABLE_LAST_UPDATE_TIME, BaseDBConstant.COLUMN_TIME_ID, (long) timeId)) {
                values.put(BaseDBConstant.COLUMN_TIME_ID, Integer.valueOf(timeId));
                db.insertOrThrow(BaseDBConstant.TABLE_LAST_UPDATE_TIME, null, values);
            } else {
                db.update(BaseDBConstant.TABLE_LAST_UPDATE_TIME, values, "timeId=" + timeId, null);
            }
            if (db != null) {
                db.close();
            }
            return true;
        } catch (Exception e) {
            if (db != null) {
                db.close();
            }
            return false;
        } catch (Throwable th) {
            if (db != null) {
                db.close();
            }
            throw th;
        }
    }

    public String getLastUpdateTime(int timeId) {
        SQLiteDatabase db = null;
        Cursor c = null;
        try {
            SQLiteDatabase db2 = openDB();
            Cursor c2 = db2.rawQuery("select * from LastUpdateTime where timeId=" + timeId, null);
            if (c2 == null || c2.getCount() <= 0) {
                if (c2 != null) {
                    c2.close();
                }
                if (db2 != null) {
                    db2.close();
                }
                return "";
            }
            c2.moveToPosition(0);
            String string = c2.getString(1);
            if (c2 != null) {
                c2.close();
            }
            if (db2 == null) {
                return string;
            }
            db2.close();
            return string;
        } catch (Exception e) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            return "";
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            throw th;
        }
    }

    public boolean tabbleIsExist(String tableName) {
        boolean result = false;
        if (tableName == null) {
            return false;
        }
        SQLiteDatabase db = null;
        Cursor c = null;
        try {
            SQLiteDatabase db2 = openDB();
            Cursor c2 = db2.rawQuery("select count(*) as c from Sqlite_master  where type ='table' and name ='" + tableName.trim() + "' ", null);
            if (c2.moveToNext() && c2.getInt(0) > 0) {
                result = true;
            }
            if (c2 != null) {
                c2.close();
            }
            if (db2 != null) {
                db2.close();
            }
            return result;
        } catch (Exception e) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            return false;
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            throw th;
        }
    }
}
