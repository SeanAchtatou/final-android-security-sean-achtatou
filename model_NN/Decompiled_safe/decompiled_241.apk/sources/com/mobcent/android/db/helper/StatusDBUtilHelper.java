package com.mobcent.android.db.helper;

import android.database.Cursor;
import com.mobcent.android.model.MCLibUserStatus;

public class StatusDBUtilHelper {
    public static MCLibUserStatus buildUserStatusModel(Cursor c, int totalNum, int page) {
        boolean z;
        MCLibUserStatus userStatusModel = new MCLibUserStatus();
        userStatusModel.setStatusId((long) c.getInt(0));
        userStatusModel.setContent(c.getString(1));
        userStatusModel.setUid(c.getInt(2));
        userStatusModel.setUserName(c.getString(3));
        userStatusModel.setToUid(c.getInt(4));
        userStatusModel.setToUserName(c.getString(5));
        userStatusModel.setTime(c.getString(6));
        userStatusModel.setReplyNum(c.getInt(7));
        userStatusModel.setForwardNum(c.getInt(8));
        userStatusModel.setCommunicationType(c.getInt(9));
        userStatusModel.setTypeName(c.getString(10));
        userStatusModel.setUserImageUrl(c.getString(11));
        userStatusModel.setRootId((long) c.getInt(12));
        MCLibUserStatus replyStatusModel = new MCLibUserStatus();
        replyStatusModel.setStatusId((long) c.getInt(13));
        replyStatusModel.setContent(c.getString(14));
        replyStatusModel.setUid(c.getInt(15));
        replyStatusModel.setUserName(c.getString(16));
        replyStatusModel.setToUid(c.getInt(17));
        replyStatusModel.setToUserName(c.getString(18));
        replyStatusModel.setTime(c.getString(19));
        replyStatusModel.setReplyNum(c.getInt(20));
        replyStatusModel.setForwardNum(c.getInt(21));
        replyStatusModel.setCommunicationType(c.getInt(22));
        replyStatusModel.setTypeName(c.getString(23));
        replyStatusModel.setUserImageUrl(c.getString(24));
        replyStatusModel.setRootId((long) c.getInt(25));
        userStatusModel.setTopicStatus(c.getInt(26));
        userStatusModel.setSourceProId(c.getInt(27));
        userStatusModel.setSourceName(c.getString(28));
        userStatusModel.setSourceUrl(c.getString(29));
        userStatusModel.setSourceCategoryId(c.getInt(30));
        userStatusModel.setImgUrl(c.getString(31));
        userStatusModel.setPhotoPath(c.getString(32));
        replyStatusModel.setImgUrl(c.getString(33));
        replyStatusModel.setPhotoPath(c.getString(34));
        userStatusModel.setDel(c.getInt(35) != 0);
        if (c.getInt(36) == 0) {
            z = false;
        } else {
            z = true;
        }
        replyStatusModel.setDel(z);
        userStatusModel.setContentPathUrl(c.getString(37));
        userStatusModel.setReplyStatus(replyStatusModel);
        userStatusModel.setTotalNum(totalNum);
        userStatusModel.setCurrentPage(page);
        userStatusModel.setReadFromLocal(true);
        return userStatusModel;
    }

    public static MCLibUserStatus buildUserEventModel(Cursor c, int totalNum, int page) {
        MCLibUserStatus userStatusModel = new MCLibUserStatus();
        userStatusModel.setContent(c.getString(0));
        userStatusModel.setFromUserId(c.getInt(1));
        userStatusModel.setFromUserName(c.getString(2));
        userStatusModel.setTargetJumpId(c.getInt(3));
        userStatusModel.setTargetJumpName(c.getString(4));
        userStatusModel.setTime(c.getString(5));
        userStatusModel.setCommunicationType(c.getInt(6));
        userStatusModel.setUserImageUrl(c.getString(7));
        userStatusModel.setDomainUrl(c.getString(9));
        userStatusModel.setSourceProId(c.getInt(10));
        userStatusModel.setSourceName(c.getString(11));
        userStatusModel.setSourceUrl(c.getString(12));
        userStatusModel.setSourceCategoryId(c.getInt(13));
        userStatusModel.setTotalNum(totalNum);
        userStatusModel.setCurrentPage(page);
        userStatusModel.setReadFromLocal(true);
        return userStatusModel;
    }
}
