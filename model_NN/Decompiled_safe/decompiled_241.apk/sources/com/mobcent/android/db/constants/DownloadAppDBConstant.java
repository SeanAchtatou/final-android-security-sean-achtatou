package com.mobcent.android.db.constants;

public interface DownloadAppDBConstant {
    public static final String COLUMN_ADD_DOWNLOAD_TIME = "addDownloadTime";
    public static final String COLUMN_APP_ID = "appId";
    public static final String COLUMN_APP_NAME = "appName";
    public static final String COLUMN_APP_TYPE = "appType";
    public static final String COLUMN_CLIENT_ID = "clientId";
    public static final String COLUMN_DOWNLOAD_SIZE = "downloadSize";
    public static final String COLUMN_FILE_SIZE = "fileSize";
    public static final String COLUMN_SPEED = "speed";
    public static final String COLUMN_STATUS = "status";
    public static final String COLUMN_STOP_REQ = "stopReq";
    public static final String COLUMN_URL = "url";
    public static final String COLUMN_VERSION = "version";
    public static final String TABLE_DOWNLOAD_APP = "TDownloadApp";
}
