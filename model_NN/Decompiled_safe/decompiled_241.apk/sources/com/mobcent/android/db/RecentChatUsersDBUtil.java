package com.mobcent.android.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.mobcent.android.db.constants.RecentChatUsersDBConstant;
import com.mobcent.android.db.helper.RecentChatUsersDBUtilHelper;
import com.mobcent.android.model.MCLibUserInfo;
import com.mobcent.android.service.impl.MCLibUserInfoServiceImpl;
import com.mobcent.android.utils.MCLibIOUtil;
import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class RecentChatUsersDBUtil extends BaseDBUtil implements RecentChatUsersDBConstant {
    private static final String DATABASE_NAME = "mrc_";
    private static final String DROP_TABLE_RECENT_CHAT_USERS = "DROP TABLE RecentChatUsers";
    private static final String SQL_CREATE_TABLE_RECENT_CHAT_USERS = "CREATE TABLE IF NOT EXISTS RecentChatUsers(uid INTEGER PRIMARY KEY,userName TEXT,nickName TEXT,userImage TEXT,unreadMsgCount INTEGER,lastMsgTime LONG,sourceProId INTEGER,sourceName TEXT,sourceUrl TEXT);";
    private static RecentChatUsersDBUtil mobcentDBUtil;
    private Context ctx;

    protected RecentChatUsersDBUtil(Context ctx2) {
        this.ctx = ctx2;
        SQLiteDatabase db = null;
        try {
            db = openDB();
            db.execSQL(SQL_CREATE_TABLE_RECENT_CHAT_USERS);
        } finally {
            db.close();
        }
    }

    public void dropAllTables() {
        SQLiteDatabase db = null;
        try {
            db = openDB();
            db.execSQL(DROP_TABLE_RECENT_CHAT_USERS);
        } finally {
            db.close();
        }
    }

    public static RecentChatUsersDBUtil getInstance(Context context) {
        mobcentDBUtil = new RecentChatUsersDBUtil(context);
        return mobcentDBUtil;
    }

    /* access modifiers changed from: protected */
    public SQLiteDatabase openDB() {
        String basePath = String.valueOf(MCLibIOUtil.getBaseLocalLocation(this.ctx)) + MCLibIOUtil.FS + ".mc" + MCLibIOUtil.FS;
        File file = new File(basePath);
        if (!file.exists()) {
            file.mkdirs();
        }
        return SQLiteDatabase.openOrCreateDatabase(String.valueOf(basePath) + DATABASE_NAME + new MCLibUserInfoServiceImpl(this.ctx).getLoginUserId(), (SQLiteDatabase.CursorFactory) null);
    }

    public List<MCLibUserInfo> getRecentChatUsers() {
        List<MCLibUserInfo> userInfoList = new ArrayList<>();
        SQLiteDatabase db = null;
        Cursor c = null;
        try {
            db = openDB();
            c = db.rawQuery("select * from RecentChatUsers order by unreadMsgCount desc", null);
            for (int i = 0; i < c.getCount(); i++) {
                c.moveToPosition(i);
                userInfoList.add(RecentChatUsersDBUtilHelper.buildRecentChatUserModel(c));
            }
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
        } catch (Exception e) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            throw th;
        }
        return userInfoList;
    }

    public MCLibUserInfo getRecentChatUser(int uid) {
        SQLiteDatabase db = null;
        Cursor c = null;
        try {
            SQLiteDatabase db2 = openDB();
            Cursor c2 = db2.rawQuery("select * from RecentChatUsers where uid=" + uid, null);
            if (c2.moveToFirst()) {
                MCLibUserInfo buildRecentChatUserModel = RecentChatUsersDBUtilHelper.buildRecentChatUserModel(c2);
                if (c2 != null) {
                    c2.close();
                }
                if (db2 == null) {
                    return buildRecentChatUserModel;
                }
                db2.close();
                return buildRecentChatUserModel;
            }
            if (c2 != null) {
                c2.close();
            }
            if (db2 != null) {
                db2.close();
            }
            return null;
        } catch (Exception e) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            return null;
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            throw th;
        }
    }

    public boolean removeRecentChatUser(int uid) {
        SQLiteDatabase db = null;
        try {
            db = openDB();
            db.delete(RecentChatUsersDBConstant.TABLE_RECENT_CHAT_USERS, "uid=" + uid, null);
            if (db != null) {
                db.close();
            }
            return true;
        } catch (Exception e) {
            if (db != null) {
                db.close();
            }
            return false;
        } catch (Throwable th) {
            if (db != null) {
                db.close();
            }
            throw th;
        }
    }

    public boolean addOrUpdateRecentChatUser(MCLibUserInfo userInfo) {
        SQLiteDatabase db = null;
        try {
            db = openDB();
            ContentValues values = new ContentValues();
            MCLibUserInfo user = getRecentChatUser(userInfo.getUid());
            if (user != null) {
                values.put("userName", userInfo.getName());
                values.put("nickName", userInfo.getNickName());
                values.put("userImage", userInfo.getImage());
                values.put(RecentChatUsersDBConstant.COLUMN_UNREAD_MSG_COUNT, Integer.valueOf(userInfo.getUnreadMsgCount() + user.getUnreadMsgCount()));
                values.put("sourceProId", Integer.valueOf(userInfo.getSourceProId()));
                values.put("sourceName", userInfo.getSourceName());
                values.put("sourceUrl", userInfo.getSourceUrl());
                if (userInfo.getUnreadMsgCount() + user.getUnreadMsgCount() > 0) {
                    values.put(RecentChatUsersDBConstant.COLUMN_LAST_MSG_TIME, Long.valueOf(Calendar.getInstance().getTimeInMillis()));
                } else {
                    values.put(RecentChatUsersDBConstant.COLUMN_LAST_MSG_TIME, Long.valueOf(userInfo.getLastMsgTime()));
                }
                db.update(RecentChatUsersDBConstant.TABLE_RECENT_CHAT_USERS, values, "uid=" + userInfo.getUid(), null);
            } else {
                values.put("userName", userInfo.getName());
                values.put("nickName", userInfo.getNickName());
                values.put("userImage", userInfo.getImage());
                values.put(RecentChatUsersDBConstant.COLUMN_UNREAD_MSG_COUNT, Integer.valueOf(userInfo.getUnreadMsgCount()));
                values.put("uid", Integer.valueOf(userInfo.getUid()));
                values.put(RecentChatUsersDBConstant.COLUMN_LAST_MSG_TIME, Long.valueOf(Calendar.getInstance().getTimeInMillis()));
                values.put("sourceProId", Integer.valueOf(userInfo.getSourceProId()));
                values.put("sourceName", userInfo.getSourceName());
                values.put("sourceUrl", userInfo.getSourceUrl());
                db.insertOrThrow(RecentChatUsersDBConstant.TABLE_RECENT_CHAT_USERS, null, values);
            }
            if (db != null) {
                db.close();
            }
            return true;
        } catch (Exception e) {
            if (db != null) {
                db.close();
            }
            return false;
        } catch (Throwable th) {
            if (db != null) {
                db.close();
            }
            throw th;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void updateRecentChatUserMsgRead(MCLibUserInfo userInfo) {
        SQLiteDatabase db = null;
        try {
            SQLiteDatabase db2 = openDB();
            ContentValues values = new ContentValues();
            values.put("userName", userInfo.getName());
            values.put("nickName", userInfo.getNickName());
            values.put("userImage", userInfo.getImage());
            values.put(RecentChatUsersDBConstant.COLUMN_UNREAD_MSG_COUNT, (Integer) 0);
            values.put(RecentChatUsersDBConstant.COLUMN_LAST_MSG_TIME, Long.valueOf(userInfo.getLastMsgTime()));
            values.put("sourceProId", Integer.valueOf(userInfo.getSourceProId()));
            values.put("sourceName", userInfo.getSourceName());
            values.put("sourceUrl", userInfo.getSourceUrl());
            if (isRowExisted(RecentChatUsersDBConstant.TABLE_RECENT_CHAT_USERS, "uid", (long) userInfo.getUid())) {
                db2.update(RecentChatUsersDBConstant.TABLE_RECENT_CHAT_USERS, values, "uid=" + userInfo.getUid(), null);
            }
            if (db2 != null) {
                db2.close();
            }
        } catch (Exception e) {
            if (db != null) {
                db.close();
            }
        } catch (Throwable th) {
            if (db != null) {
                db.close();
            }
            throw th;
        }
    }
}
