package com.mobcent.android.db.constants;

public interface UserCommunicationQueueDBConstant {
    public static final String COLUMN_ACTION_ID = "actionId";
    public static final String COLUMN_COMMUNICATION_TYPE = "communicationType";
    public static final String COLUMN_CONTENT = "content";
    public static final String COLUMN_PHOTO_ID = "photoId";
    public static final String COLUMN_PHOTO_PATH = "photoPath";
    public static final String COLUMN_TO_APP_ID = "toAppId";
    public static final String COLUMN_TO_LBS_ID = "toLbsId";
    public static final String COLUMN_TO_STATUS_ID = "toStatusId";
    public static final String COLUMN_TO_UID = "toUid";
    public static final String COLUMN_TO_USER_NAME = "toUserName";
    public static final String COLUMN_UCQ_ID = "ucqId";
    public static final String COLUMN_UID = "uid";
    public static final String TABLE_USER_COMMUNICATION_QUEUE = "TUserCommunicationQueue";
}
