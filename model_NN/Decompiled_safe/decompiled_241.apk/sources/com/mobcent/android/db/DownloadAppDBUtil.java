package com.mobcent.android.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.mobcent.android.db.constants.DownloadAppDBConstant;
import com.mobcent.android.db.helper.DownloadAppDBUtilHelper;
import com.mobcent.android.model.MCLibDownloadProfileModel;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class DownloadAppDBUtil extends BaseDBUtil implements DownloadAppDBConstant {
    private static final String DATABASE_NAME = "mobcent_da.db";
    private static final String DROP_TABLE_DOWNLOAD_APP = "DROP TABLE TDownloadApp";
    private static final String SQL_CREATE_TABLE_DOWNLOAD_APP = "CREATE TABLE IF NOT EXISTS TDownloadApp(appId INTEGER PRIMARY KEY,appName TEXT,url TEXT,downloadSize INTEGER,fileSize INTEGER,speed INTEGER,stopReq INTEGER,status INTEGER,appType INTEGER,version TEXT,clientId INTEGER,addDownloadTime LONG);";
    private static DownloadAppDBUtil mobcentDBUtil;
    private Context ctx;

    protected DownloadAppDBUtil(Context ctx2) {
        this.ctx = ctx2;
        SQLiteDatabase db = null;
        try {
            db = openDB();
            db.execSQL(SQL_CREATE_TABLE_DOWNLOAD_APP);
        } finally {
            db.close();
        }
    }

    public void dropAllTables() {
        SQLiteDatabase db = null;
        try {
            db = openDB();
            db.execSQL(DROP_TABLE_DOWNLOAD_APP);
        } finally {
            db.close();
        }
    }

    public static DownloadAppDBUtil getInstance(Context context) {
        if (mobcentDBUtil == null) {
            mobcentDBUtil = new DownloadAppDBUtil(context);
        }
        return mobcentDBUtil;
    }

    /* access modifiers changed from: protected */
    public SQLiteDatabase openDB() {
        return this.ctx.openOrCreateDatabase(DATABASE_NAME, 0, null);
    }

    public boolean addOrUpdateAppDownloadProfile(MCLibDownloadProfileModel model, boolean isNeedUpdate) {
        SQLiteDatabase db = null;
        try {
            ContentValues values = new ContentValues();
            values.put(DownloadAppDBConstant.COLUMN_APP_NAME, model.getAppName());
            values.put("url", model.getUrl());
            values.put(DownloadAppDBConstant.COLUMN_DOWNLOAD_SIZE, Integer.valueOf(model.getDownloadSize()));
            values.put(DownloadAppDBConstant.COLUMN_FILE_SIZE, Integer.valueOf(model.getFileSize()));
            values.put(DownloadAppDBConstant.COLUMN_SPEED, Integer.valueOf(model.getSpeed()));
            values.put(DownloadAppDBConstant.COLUMN_STOP_REQ, Integer.valueOf(model.getStopReq()));
            values.put(DownloadAppDBConstant.COLUMN_STATUS, Integer.valueOf(model.getStatus()));
            values.put(DownloadAppDBConstant.COLUMN_APP_TYPE, Integer.valueOf(model.getAppType()));
            values.put("version", model.getVersion() == null ? "" : model.getVersion());
            values.put("clientId", Integer.valueOf(model.getClientId()));
            values.put(DownloadAppDBConstant.COLUMN_ADD_DOWNLOAD_TIME, Long.valueOf(model.getDownloadTime()));
            db = openDB();
            MCLibDownloadProfileModel modelExist = getDownloadProfile(model.getAppId());
            if (modelExist == null) {
                values.put(DownloadAppDBConstant.COLUMN_APP_ID, Integer.valueOf(model.getAppId()));
                values.put(DownloadAppDBConstant.COLUMN_ADD_DOWNLOAD_TIME, Long.valueOf(Calendar.getInstance().getTimeInMillis()));
                db.insertOrThrow(DownloadAppDBConstant.TABLE_DOWNLOAD_APP, null, values);
            } else if (modelExist.getStatus() == 2 || modelExist.getStatus() == 3) {
                values.put(DownloadAppDBConstant.COLUMN_ADD_DOWNLOAD_TIME, Long.valueOf(Calendar.getInstance().getTimeInMillis()));
                db.update(DownloadAppDBConstant.TABLE_DOWNLOAD_APP, values, "appId=" + model.getAppId(), null);
            } else if (isNeedUpdate) {
                db.update(DownloadAppDBConstant.TABLE_DOWNLOAD_APP, values, "appId=" + model.getAppId(), null);
            }
            if (db != null) {
                db.close();
            }
            return true;
        } catch (Exception e) {
            if (db != null) {
                db.close();
            }
            return false;
        } catch (Throwable th) {
            if (db != null) {
                db.close();
            }
            throw th;
        }
    }

    public MCLibDownloadProfileModel getDownloadProfile(int appId) {
        MCLibDownloadProfileModel model = null;
        SQLiteDatabase db = null;
        Cursor c = null;
        try {
            SQLiteDatabase db2 = openDB();
            Cursor c2 = db2.query(DownloadAppDBConstant.TABLE_DOWNLOAD_APP, null, "appId=" + appId, null, null, null, null);
            if (c2.moveToFirst()) {
                model = DownloadAppDBUtilHelper.buildDownloadProfileModel(c2);
            }
            if (c2 != null) {
                c2.close();
            }
            if (db2 != null) {
                db2.close();
            }
            return model;
        } catch (Exception e) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            return null;
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            throw th;
        }
    }

    public List<MCLibDownloadProfileModel> getDownloadProfileByStatus(int status) {
        List<MCLibDownloadProfileModel> downloadModels = new ArrayList<>();
        SQLiteDatabase db = null;
        Cursor c = null;
        try {
            db = openDB();
            c = db.query(DownloadAppDBConstant.TABLE_DOWNLOAD_APP, null, "status=" + status, null, null, null, null);
            for (int i = 0; i < c.getCount(); i++) {
                c.moveToPosition(i);
                downloadModels.add(DownloadAppDBUtilHelper.buildDownloadProfileModel(c));
            }
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
        } catch (Exception e) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            throw th;
        }
        return downloadModels;
    }

    public List<MCLibDownloadProfileModel> getAllDownloadProfile() {
        List<MCLibDownloadProfileModel> downloadModels = new ArrayList<>();
        SQLiteDatabase db = null;
        Cursor c = null;
        try {
            db = openDB();
            c = db.query(DownloadAppDBConstant.TABLE_DOWNLOAD_APP, null, "status!=3", null, null, null, "addDownloadTime desc");
            for (int i = 0; i < c.getCount(); i++) {
                c.moveToPosition(i);
                downloadModels.add(DownloadAppDBUtilHelper.buildDownloadProfileModel(c));
            }
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
        } catch (Exception e) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            throw th;
        }
        return downloadModels;
    }
}
