package com.mobcent.android.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.mobcent.android.db.constants.UserCommunicationQueueDBConstant;
import com.mobcent.android.db.helper.UserCommunicationQueueDBUtilHelper;
import com.mobcent.android.model.MCLibUserStatus;
import com.mobcent.android.utils.MathUtil;
import java.util.ArrayList;
import java.util.List;

public class UserCommunicationQueueDBUtil extends BaseDBUtil implements UserCommunicationQueueDBConstant {
    private static final String DATABASE_NAME = "mobcent_ucq.db";
    private static final String DROP_TABLE_USER_COMMUNICATION_QUEUE = "DROP TABLE TUserCommunicationQueue";
    private static final String SQL_CREATE_TABLE_USER_COMMUNICATION_QUEUE = "CREATE TABLE IF NOT EXISTS TUserCommunicationQueue(ucqId INTEGER PRIMARY KEY,uid INTEGER,content TEXT,toUid INTEGER,toStatusId INTEGER,toAppId INTEGER,toLbsId INTEGER,communicationType INTEGER,actionId INTEGER,toUserName TEXT,photoId INTEGER,photoPath TEXT);";
    private static UserCommunicationQueueDBUtil mobcentDBUtil;
    private Context ctx;

    protected UserCommunicationQueueDBUtil(Context ctx2) {
        this.ctx = ctx2;
        SQLiteDatabase db = null;
        try {
            db = openDB();
            db.execSQL(SQL_CREATE_TABLE_USER_COMMUNICATION_QUEUE);
        } finally {
            db.close();
        }
    }

    public void dropAllTables() {
        SQLiteDatabase db = null;
        try {
            db = openDB();
            db.execSQL(DROP_TABLE_USER_COMMUNICATION_QUEUE);
        } finally {
            db.close();
        }
    }

    public static UserCommunicationQueueDBUtil getInstance(Context context) {
        if (mobcentDBUtil == null) {
            mobcentDBUtil = new UserCommunicationQueueDBUtil(context);
        }
        return mobcentDBUtil;
    }

    /* access modifiers changed from: protected */
    public SQLiteDatabase openDB() {
        return this.ctx.openOrCreateDatabase(DATABASE_NAME, 0, null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public boolean putCommunicationContent(MCLibUserStatus userStatus) {
        SQLiteDatabase db = null;
        try {
            db = openDB();
            userStatus.setUserCommunicationQueueId(generateUserCommunicationQueueId());
            ContentValues values = new ContentValues();
            values.put(UserCommunicationQueueDBConstant.COLUMN_UCQ_ID, Integer.valueOf(userStatus.getUserCommunicationQueueId()));
            values.put("uid", Integer.valueOf(userStatus.getUid()));
            values.put("content", userStatus.getContent());
            values.put(UserCommunicationQueueDBConstant.COLUMN_TO_UID, Integer.valueOf(userStatus.getToUid()));
            values.put(UserCommunicationQueueDBConstant.COLUMN_TO_STATUS_ID, Long.valueOf(userStatus.getToStatusId()));
            values.put(UserCommunicationQueueDBConstant.COLUMN_TO_APP_ID, Integer.valueOf(userStatus.getToAppId()));
            values.put(UserCommunicationQueueDBConstant.COLUMN_TO_LBS_ID, (Integer) 0);
            values.put("communicationType", Integer.valueOf(userStatus.getCommunicationType()));
            values.put("actionId", Integer.valueOf(userStatus.getActionId()));
            values.put("toUserName", userStatus.getToUserName());
            values.put("photoId", Integer.valueOf(userStatus.getPhotoId()));
            values.put("photoPath", userStatus.getPhotoPath());
            db.insertOrThrow(UserCommunicationQueueDBConstant.TABLE_USER_COMMUNICATION_QUEUE, null, values);
            if (db != null) {
                db.close();
            }
            return true;
        } catch (Exception e) {
            if (db != null) {
                db.close();
            }
            return false;
        } catch (Throwable th) {
            if (db != null) {
                db.close();
            }
            throw th;
        }
    }

    public List<MCLibUserStatus> getUserStatusList(int size) {
        List<MCLibUserStatus> userStatusList = new ArrayList<>();
        SQLiteDatabase db = null;
        Cursor c = null;
        try {
            db = openDB();
            c = db.rawQuery("select * from TUserCommunicationQueue limit " + size, null);
            for (int i = 0; i < c.getCount(); i++) {
                c.moveToPosition(i);
                userStatusList.add(UserCommunicationQueueDBUtilHelper.buildUserStatusModel(c));
            }
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
        } catch (Exception e) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            throw th;
        }
        return userStatusList;
    }

    public boolean removeUserStatusInQueue(int ucqId) {
        SQLiteDatabase db = null;
        try {
            db = openDB();
            db.delete(UserCommunicationQueueDBConstant.TABLE_USER_COMMUNICATION_QUEUE, "ucqId=" + ucqId, null);
            if (db != null) {
                db.close();
            }
            return true;
        } catch (Exception e) {
            if (db != null) {
                db.close();
            }
            return false;
        } catch (Throwable th) {
            if (db != null) {
                db.close();
            }
            throw th;
        }
    }

    public int generateUserCommunicationQueueId() {
        int ucqId = MathUtil.generateId();
        if (!isUserStatusExisted(ucqId)) {
            return ucqId;
        }
        return generateUserCommunicationQueueId();
    }

    public boolean isUserStatusExisted(int ucqId) {
        SQLiteDatabase db = null;
        Cursor c = null;
        try {
            db = openDB();
            c = db.query(UserCommunicationQueueDBConstant.TABLE_USER_COMMUNICATION_QUEUE, null, "ucqId=" + ucqId, null, null, null, null);
            boolean isExist = false;
            if (c.getCount() > 0) {
                isExist = true;
            }
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            return isExist;
        } catch (Exception e) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            return false;
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            throw th;
        }
    }
}
