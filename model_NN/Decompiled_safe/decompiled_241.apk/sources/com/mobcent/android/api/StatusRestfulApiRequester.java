package com.mobcent.android.api;

import android.content.Context;
import com.mobcent.android.constants.MCLibMobCentApiConstant;
import com.mobcent.android.db.UserMagicActionDictDBUtil;
import com.mobcent.android.state.MCLibAppState;
import java.util.HashMap;

public class StatusRestfulApiRequester implements MCLibMobCentApiConstant {
    public static String publishUserStatus(int userId, String content, int photoId, String photoPath, Context context) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        params.put("content", content);
        params.put("photoId", new StringBuilder(String.valueOf(photoId)).toString());
        params.put("photoPath", photoPath == null ? "" : photoPath);
        String jsonString = HttpClientUtil.doPostRequest("http://sdk.mobcent.com/sdk/action/speak.do", params, context);
        if (jsonString.equals("connection_fail")) {
            return "{}";
        }
        return jsonString;
    }

    public static String publishStatusImage(int userId, String uploadFile, Context context) {
        return HttpClientUtil.uploadFile("http://img.mobcent.com/sdk/action/upload.do?userId=" + userId + "&" + MCLibMobCentApiConstant.SESSION_ID + "=" + MCLibAppState.sessionId + "&" + "packageName" + "=" + context.getPackageName() + "&" + "appKey" + "=" + UserMagicActionDictDBUtil.getInstance(context).getAppKey(), uploadFile, context);
    }

    public static String sendMsg(int userId, String content, int toUserId, int actionId, Context context) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        params.put("content", content);
        params.put(MCLibMobCentApiConstant.TARGET_ID, new StringBuilder(String.valueOf(toUserId)).toString());
        if (actionId > 0) {
            params.put("actionId", new StringBuilder(String.valueOf(actionId)).toString());
        }
        String jsonString = HttpClientUtil.doPostRequest("http://sdk.mobcent.com/sdk/action/sendMsg.do", params, context);
        if (jsonString.equals("connection_fail")) {
            return "{}";
        }
        return jsonString;
    }

    public static String talkToUser(int userId, String content, int toUserId, Context context) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        params.put("content", content);
        params.put(MCLibMobCentApiConstant.TARGET_ID, new StringBuilder(String.valueOf(toUserId)).toString());
        String jsonString = HttpClientUtil.doPostRequest("http://sdk.mobcent.com/sdk/action/speakTo.do", params, context);
        if (jsonString.equals("connection_fail")) {
            return "{}";
        }
        return jsonString;
    }

    public static String replyToUserStatus(int userId, String content, long statusId, long rootId, int photoId, String photoPath, Context context) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        params.put("content", content);
        params.put(MCLibMobCentApiConstant.TARGET_ID, new StringBuilder(String.valueOf(statusId)).toString());
        params.put("rootId", new StringBuilder(String.valueOf(rootId)).toString());
        params.put("photoId", new StringBuilder(String.valueOf(photoId)).toString());
        params.put("photoPath", photoPath == null ? "" : photoPath);
        String jsonString = HttpClientUtil.doPostRequest("http://sdk.mobcent.com/sdk/action/replyTo.do", params, context);
        if (jsonString.equals("connection_fail")) {
            return "{}";
        }
        return jsonString;
    }

    public static String forwordStatus(int userId, long statusId, Context context) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        params.put(MCLibMobCentApiConstant.TARGET_ID, new StringBuilder(String.valueOf(statusId)).toString());
        String jsonString = HttpClientUtil.doPostRequest("http://sdk.mobcent.com/sdk/action/forward.do", params, context);
        if (jsonString.equals("connection_fail")) {
            return "{}";
        }
        return jsonString;
    }

    public static String getMyStatus(int userId, int targetId, int page, int pageSize, Context context) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        params.put(MCLibMobCentApiConstant.TARGET_ID, new StringBuilder(String.valueOf(targetId)).toString());
        params.put(MCLibMobCentApiConstant.PAGE, new StringBuilder(String.valueOf(page)).toString());
        params.put(MCLibMobCentApiConstant.PAGE_SIZE, new StringBuilder(String.valueOf(pageSize)).toString());
        String jsonString = HttpClientUtil.doPostRequest("http://sdk.mobcent.com/sdk/action/queryMy.do", params, context);
        if (jsonString.equals("connection_fail")) {
            return "{}";
        }
        return jsonString;
    }

    public static String getTalkToMeStatus(int userId, int page, int pageSize, Context context) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        params.put(MCLibMobCentApiConstant.PAGE, new StringBuilder(String.valueOf(page)).toString());
        params.put(MCLibMobCentApiConstant.PAGE_SIZE, new StringBuilder(String.valueOf(pageSize)).toString());
        String jsonString = HttpClientUtil.doPostRequest("http://sdk.mobcent.com/sdk/action/queryReply.do", params, context);
        if (jsonString.equals("connection_fail")) {
            return "{}";
        }
        return jsonString;
    }

    public static String getStatusThread(int userId, long statusId, int page, int pageSize, Context context) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        params.put(MCLibMobCentApiConstant.TARGET_ID, new StringBuilder(String.valueOf(statusId)).toString());
        params.put(MCLibMobCentApiConstant.PAGE, new StringBuilder(String.valueOf(page)).toString());
        params.put(MCLibMobCentApiConstant.PAGE_SIZE, new StringBuilder(String.valueOf(pageSize)).toString());
        String jsonString = HttpClientUtil.doPostRequest("http://sdk.mobcent.com/sdk/action/reply.do", params, context);
        if (jsonString.equals("connection_fail")) {
            return "{}";
        }
        return jsonString;
    }

    public static String getStatusThreadAsComment(int userId, String objectId, String type, int page, int pageSize, Context context) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        params.put(MCLibMobCentApiConstant.OBJECT_ID, objectId);
        params.put("type", type);
        params.put(MCLibMobCentApiConstant.PAGE, new StringBuilder(String.valueOf(page)).toString());
        params.put(MCLibMobCentApiConstant.PAGE_SIZE, new StringBuilder(String.valueOf(pageSize)).toString());
        String jsonString = HttpClientUtil.doPostRequest("http://sdk.mobcent.com/sdk/action/queryComment.do", params, context);
        if (jsonString.equals("connection_fail")) {
            return "{}";
        }
        return jsonString;
    }

    public static String getFollowedUserStatus(int userId, int page, int pageSize, Context context) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        params.put(MCLibMobCentApiConstant.PAGE, new StringBuilder(String.valueOf(page)).toString());
        params.put(MCLibMobCentApiConstant.PAGE_SIZE, new StringBuilder(String.valueOf(pageSize)).toString());
        String jsonString = HttpClientUtil.doPostRequest("http://sdk.mobcent.com/sdk/action/qs.do", params, context);
        if (jsonString.equals("connection_fail")) {
            return "{}";
        }
        return jsonString;
    }

    public static String getHallStatus(int userId, int page, int pageSize, Context context) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        params.put(MCLibMobCentApiConstant.PAGE, new StringBuilder(String.valueOf(page)).toString());
        params.put(MCLibMobCentApiConstant.PAGE_SIZE, new StringBuilder(String.valueOf(pageSize)).toString());
        String jsonString = HttpClientUtil.doPostRequest("http://sdk.mobcent.com/sdk/action/queryPublicTalk.do", params, context);
        if (jsonString.equals("connection_fail")) {
            return "{}";
        }
        return jsonString;
    }

    public static String getHallTopic(int userId, int page, int pageSize, Context context) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        params.put(MCLibMobCentApiConstant.PAGE, new StringBuilder(String.valueOf(page)).toString());
        params.put(MCLibMobCentApiConstant.PAGE_SIZE, new StringBuilder(String.valueOf(pageSize)).toString());
        String jsonString = HttpClientUtil.doPostRequest("http://sdk.mobcent.com/sdk/action/queryTopic.do", params, context);
        if (jsonString.equals("connection_fail")) {
            return "{}";
        }
        return jsonString;
    }

    public static String getHallEvents(int userId, int page, int pageSize, Context context) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        params.put(MCLibMobCentApiConstant.PAGE, new StringBuilder(String.valueOf(page)).toString());
        params.put(MCLibMobCentApiConstant.PAGE_SIZE, new StringBuilder(String.valueOf(pageSize)).toString());
        String jsonString = HttpClientUtil.doPostRequest("http://sdk.mobcent.com/sdk/action/queryPublicFeed.do", params, context);
        if (jsonString.equals("connection_fail")) {
            return "{}";
        }
        return jsonString;
    }

    public static String getFriendEvents(int userId, int page, int pageSize, Context context) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        params.put(MCLibMobCentApiConstant.PAGE, new StringBuilder(String.valueOf(page)).toString());
        params.put(MCLibMobCentApiConstant.PAGE_SIZE, new StringBuilder(String.valueOf(pageSize)).toString());
        String jsonString = HttpClientUtil.doPostRequest("http://sdk.mobcent.com/sdk/action/queryFriendFeed.do", params, context);
        if (jsonString.equals("connection_fail")) {
            return "{}";
        }
        return jsonString;
    }

    public static String modifyMyMood(int userId, String mood, Context context) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        params.put(MCLibMobCentApiConstant.MOOD, mood);
        String jsonString = HttpClientUtil.doPostRequest("http://sdk.mobcent.com/sdk/action/modifyMood.do", params, context);
        if (jsonString.equals("connection_fail")) {
            return "{}";
        }
        return jsonString;
    }

    public static String getUserEvents(int userId, int targetId, int page, int pageSize, Context context) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        params.put(MCLibMobCentApiConstant.TARGET_ID, new StringBuilder(String.valueOf(targetId)).toString());
        params.put(MCLibMobCentApiConstant.PAGE, new StringBuilder(String.valueOf(page)).toString());
        params.put(MCLibMobCentApiConstant.PAGE_SIZE, new StringBuilder(String.valueOf(pageSize)).toString());
        String jsonString = HttpClientUtil.doPostRequest("http://sdk.mobcent.com/sdk/action/queryHisFeed.do", params, context);
        if (jsonString.equals("connection_fail")) {
            return "{}";
        }
        return jsonString;
    }

    public static String notifyServerMsgRead(int userId, String ids, Context context) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        params.put("ids", new StringBuilder(String.valueOf(ids)).toString());
        String jsonString = HttpClientUtil.doPostRequest("http://sdk.mobcent.com/sdk/action/deleteMsg.do", params, context);
        if (jsonString.equals("connection_fail")) {
            return "{}";
        }
        return jsonString;
    }
}
