package com.mobcent.android.api;

import android.content.Context;
import com.mobcent.android.constants.MCLibMobCentApiConstant;
import com.mobcent.android.db.UserMagicActionDictDBUtil;
import com.mobcent.android.model.MCLibUserInfo;
import com.mobcent.android.service.MCLibUserInfoService;
import com.mobcent.android.service.impl.MCLibUserInfoServiceImpl;
import com.mobcent.android.service.impl.helper.BaseJsonHelper;
import com.mobcent.android.state.MCLibAppState;
import com.mobcent.android.utils.MCLibIOUtil;
import com.mobcent.android.utils.PhoneConnectionUtil;
import java.io.Closeable;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.zip.GZIPInputStream;
import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.json.JSONException;
import org.json.JSONObject;

public class HttpClientUtil {
    public static int RECONNECTION_TIMES = 1;
    public static HashMap<String, Integer> urlMap = new HashMap<>();

    private static String getUnavailableNetworkString() {
        JSONObject json = new JSONObject();
        try {
            json.put("rs", 0);
            json.put("reason", "connection_fail");
            return json.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            return "{}";
        }
    }

    /* JADX INFO: Multiple debug info for r1v3 org.apache.http.client.methods.HttpPost: [D('proxy' org.apache.http.HttpHost), D('httpPost' org.apache.http.client.methods.HttpPost)] */
    /* JADX INFO: Multiple debug info for r0v9 org.apache.http.HttpResponse: [D('httpClient' org.apache.http.client.HttpClient), D('httpResponse' org.apache.http.HttpResponse)] */
    /* JADX INFO: Multiple debug info for r0v16 com.mobcent.android.service.impl.MCLibUserInfoServiceImpl: [D('rs' int), D('userInfoService' com.mobcent.android.service.MCLibUserInfoService)] */
    /* JADX INFO: Multiple debug info for r0v17 java.lang.String: [D('userInfoService' com.mobcent.android.service.MCLibUserInfoService), D('isLoginSucc' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r0v26 org.apache.http.HttpResponse: [D('httpClient' org.apache.http.client.HttpClient), D('httpResponse' org.apache.http.HttpResponse)] */
    public static String doPostRequest(String urlString, HashMap<String, String> params, Context context) {
        HttpResponse httpResponse;
        boolean isApnWap = PhoneConnectionUtil.isCmwap(context) || PhoneConnectionUtil.isUniwap(context);
        if (isApnWap) {
            RECONNECTION_TIMES = 2;
        } else {
            RECONNECTION_TIMES = 1;
        }
        if (urlMap.containsKey(urlString)) {
            int times = urlMap.get(urlString).intValue();
            if (times >= RECONNECTION_TIMES) {
                urlMap.remove(urlString);
                return getUnavailableNetworkString();
            }
            urlMap.put(urlString, Integer.valueOf(times + 1));
        } else {
            urlMap.put(urlString, 1);
        }
        HttpHost proxy = new HttpHost("10.0.0.172", 80, "http");
        HttpHost target = new HttpHost(getUrlHost(urlString), 80, "http");
        HttpClient httpClient = new DefaultHttpClient();
        if (isApnWap) {
            httpClient.getParams().setParameter("http.route.default-proxy", proxy);
        }
        HttpPost httpPost = new HttpPost(urlString);
        HttpConnectionParams.setConnectionTimeout(httpClient.getParams(), 120000);
        HttpConnectionParams.setSoTimeout(httpClient.getParams(), 120000);
        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            params.put(MCLibMobCentApiConstant.SESSION_ID, MCLibAppState.sessionId);
            params.put("packageName", context.getPackageName());
            params.put("appKey", UserMagicActionDictDBUtil.getInstance(context).getAppKey());
            if (params != null && !params.isEmpty()) {
                for (String key : params.keySet()) {
                    nameValuePairs.add(new BasicNameValuePair(key, params.get(key)));
                }
            }
            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));
            if (isApnWap) {
                httpResponse = httpClient.execute(target, httpPost);
            } else {
                httpResponse = httpClient.execute(httpPost);
            }
            if (httpResponse.getStatusLine().getStatusCode() != 200 && httpResponse.getStatusLine().getStatusCode() != 206) {
                return "connection_fail";
            }
            Header[] headers = httpResponse.getHeaders("Content-Type");
            if (headers != null) {
                for (Header header : headers) {
                    if ("text/vnd.wap.wml".equalsIgnoreCase(header.getValue())) {
                        return "connection_fail";
                    }
                }
            }
            String results = MCLibIOUtil.unGzip(httpResponse.getEntity());
            urlMap.remove(urlString);
            int rs = BaseJsonHelper.getJsonRS(results);
            if (rs == MCLibAppState.APP_RELOGIN) {
                MCLibUserInfoService userInfoService = new MCLibUserInfoServiceImpl(context);
                MCLibUserInfo user = userInfoService.getLoginUser();
                if (userInfoService.doLogin(user.getUid(), user.getName(), user.getEmail(), user.getPassword(), true, true, "true").equals("rs_succ")) {
                    return doPostRequest(urlString, params, context);
                }
            } else if (rs == MCLibAppState.APP_OFF_LINE) {
                if (MCLibAppState.isNeedRelogin != MCLibAppState.IS_RELOGINING) {
                    MCLibAppState.isNeedRelogin = MCLibAppState.NEED_RELOGIN;
                }
                return "warning_relogin";
            }
            return results == null ? "{}" : results;
        } catch (ClientProtocolException e) {
            return "connection_fail";
        } catch (IOException e2) {
            return "connection_fail";
        } catch (Exception e3) {
            return "connection_fail";
        }
    }

    /* JADX WARN: Type inference failed for: r11v17, types: [java.net.URLConnection] */
    /* JADX WARN: Type inference failed for: r11v19, types: [java.net.URLConnection] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized byte[] getFileInByte(java.lang.String r10, android.content.Context r11) {
        /*
            java.lang.Class<com.mobcent.android.api.HttpClientUtil> r6 = com.mobcent.android.api.HttpClientUtil.class
            monitor-enter(r6)
            r2 = 0
            r1 = 0
            byte[] r1 = (byte[]) r1     // Catch:{ all -> 0x0088 }
            java.net.URL r3 = new java.net.URL     // Catch:{ Exception -> 0x005a }
            r3.<init>(r10)     // Catch:{ Exception -> 0x005a }
            r2 = 0
            r10 = 0
            boolean r4 = com.mobcent.android.utils.PhoneConnectionUtil.isCmwap(r11)     // Catch:{ IOException -> 0x0070, all -> 0x007a }
            if (r4 != 0) goto L_0x001a
            boolean r11 = com.mobcent.android.utils.PhoneConnectionUtil.isUniwap(r11)     // Catch:{ IOException -> 0x0070, all -> 0x007a }
            if (r11 == 0) goto L_0x0061
        L_0x001a:
            java.net.Proxy r11 = new java.net.Proxy     // Catch:{ IOException -> 0x0070, all -> 0x007a }
            java.net.Proxy$Type r4 = java.net.Proxy.Type.HTTP     // Catch:{ IOException -> 0x0070, all -> 0x007a }
            java.net.InetSocketAddress r5 = new java.net.InetSocketAddress     // Catch:{ IOException -> 0x0070, all -> 0x007a }
            java.lang.String r7 = "10.0.0.172"
            r8 = 80
            r5.<init>(r7, r8)     // Catch:{ IOException -> 0x0070, all -> 0x007a }
            r11.<init>(r4, r5)     // Catch:{ IOException -> 0x0070, all -> 0x007a }
            java.net.URLConnection r11 = r3.openConnection(r11)     // Catch:{ IOException -> 0x0070, all -> 0x007a }
            r0 = r11
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ IOException -> 0x0070, all -> 0x007a }
            r10 = r0
        L_0x0032:
            r11 = 1
            r10.setDoInput(r11)     // Catch:{ IOException -> 0x0070, all -> 0x007a }
            r10.connect()     // Catch:{ IOException -> 0x0070, all -> 0x007a }
            java.io.InputStream r2 = r10.getInputStream()     // Catch:{ IOException -> 0x0070, all -> 0x007a }
            int r11 = r10.getContentLength()     // Catch:{ IOException -> 0x0070, all -> 0x007a }
            r4 = -1
            if (r11 == r4) goto L_0x008c
            byte[] r1 = new byte[r11]     // Catch:{ IOException -> 0x0070, all -> 0x007a }
            r11 = 512(0x200, float:7.175E-43)
            byte[] r5 = new byte[r11]     // Catch:{ IOException -> 0x0070, all -> 0x007a }
            r4 = 0
            r11 = 0
        L_0x004c:
            int r4 = r2.read(r5)     // Catch:{ IOException -> 0x0070, all -> 0x007a }
            if (r4 > 0) goto L_0x006a
            r10 = r1
        L_0x0053:
            closeStream(r2)     // Catch:{ all -> 0x0083 }
            r11 = r3
            r1 = r10
        L_0x0058:
            monitor-exit(r6)
            return r1
        L_0x005a:
            r10 = move-exception
            r10 = 0
            r11 = r2
            r9 = r1
            r1 = r10
            r10 = r9
            goto L_0x0058
        L_0x0061:
            java.net.URLConnection r11 = r3.openConnection()     // Catch:{ IOException -> 0x0070, all -> 0x007a }
            r0 = r11
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ IOException -> 0x0070, all -> 0x007a }
            r10 = r0
            goto L_0x0032
        L_0x006a:
            r7 = 0
            java.lang.System.arraycopy(r5, r7, r1, r11, r4)     // Catch:{ IOException -> 0x0070, all -> 0x007a }
            int r11 = r11 + r4
            goto L_0x004c
        L_0x0070:
            r11 = move-exception
            closeStream(r2)     // Catch:{ all -> 0x0083 }
            r10 = 0
            r11 = r3
            r9 = r1
            r1 = r10
            r10 = r9
            goto L_0x0058
        L_0x007a:
            r11 = move-exception
            r9 = r11
            r11 = r1
            r1 = r2
            r2 = r9
            closeStream(r1)     // Catch:{ all -> 0x0083 }
            throw r2     // Catch:{ all -> 0x0083 }
        L_0x0083:
            r10 = move-exception
            r11 = r10
            r10 = r3
        L_0x0086:
            monitor-exit(r6)
            throw r11
        L_0x0088:
            r10 = move-exception
            r11 = r10
            r10 = r2
            goto L_0x0086
        L_0x008c:
            r10 = r1
            goto L_0x0053
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobcent.android.api.HttpClientUtil.getFileInByte(java.lang.String, android.content.Context):byte[]");
    }

    private static void closeStream(Closeable stream) {
        if (stream != null) {
            try {
                stream.close();
            } catch (IOException e) {
            }
        }
    }

    /* JADX INFO: Multiple debug info for r8v23 byte[]: [D('buffer' byte[]), D('bufferSize' int)] */
    /* JADX INFO: Multiple debug info for r8v30 java.lang.String: [D('is' java.io.InputStream), D('actionUrl' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r8v33 java.net.HttpURLConnection: [D('proxy' java.net.Proxy), D('con' java.net.HttpURLConnection)] */
    public static String uploadFile(String actionUrl, String uploadFile, Context context) {
        String fileName;
        HttpURLConnection con;
        if (!PhoneConnectionUtil.isNetworkAvailable(context)) {
            return "{rs:0,reason:\"upload_images_fail\"}";
        }
        if (uploadFile == null || uploadFile.lastIndexOf(MCLibIOUtil.FS) <= -1) {
            fileName = "";
        } else {
            fileName = uploadFile.substring(uploadFile.lastIndexOf(MCLibIOUtil.FS) + 1);
        }
        try {
            URL url = new URL(actionUrl);
            if (PhoneConnectionUtil.isCmwap(context) || PhoneConnectionUtil.isUniwap(context)) {
                con = (HttpURLConnection) url.openConnection(new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.0.0.172", 80)));
            } else {
                con = (HttpURLConnection) url.openConnection();
            }
            con.setDoInput(true);
            con.setDoOutput(true);
            con.setUseCaches(false);
            con.setRequestMethod("POST");
            con.setRequestProperty("Connection", "Keep-Alive");
            con.setRequestProperty("Charset", "UTF-8");
            con.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + "*****");
            DataOutputStream ds = new DataOutputStream(con.getOutputStream());
            ds.writeBytes(String.valueOf("--") + "*****" + "\r\n");
            ds.writeBytes("Content-Disposition: form-data; name=\"file1\";filename=\"" + fileName + "\"" + "\r\n");
            ds.writeBytes("\r\n");
            FileInputStream fStream = new FileInputStream(uploadFile);
            byte[] buffer = new byte[1024];
            while (true) {
                int length = fStream.read(buffer);
                if (length == -1) {
                    ds.writeBytes("\r\n");
                    ds.writeBytes(String.valueOf("--") + "*****" + "--" + "\r\n");
                    fStream.close();
                    ds.flush();
                    String actionUrl2 = MCLibIOUtil.convertStreamToString(new GZIPInputStream(con.getInputStream()));
                    ds.close();
                    return actionUrl2;
                }
                ds.write(buffer, 0, length);
            }
        } catch (Exception e) {
            return "{rs:0,reason:\"upload_images_fail\"}";
        }
    }

    public static String getUrlHost(String srcUrl) {
        int k = srcUrl.toLowerCase().indexOf("/", 8);
        if (k == -1) {
            return "";
        }
        if (srcUrl.startsWith("http://")) {
            return srcUrl.substring(7, k);
        }
        if (srcUrl.startsWith("https://")) {
            return srcUrl.substring(8, k);
        }
        return srcUrl.substring(k + 1);
    }
}
