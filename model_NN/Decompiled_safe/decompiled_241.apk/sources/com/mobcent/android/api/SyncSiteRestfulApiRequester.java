package com.mobcent.android.api;

import android.content.Context;
import com.mobcent.android.constants.MCLibMobCentApiConstant;
import java.util.HashMap;

public class SyncSiteRestfulApiRequester implements MCLibMobCentApiConstant {
    public static String getAllSyncSites(int userId, Context context) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        return HttpClientUtil.doPostRequest("http://sdk.mobcent.com/sdk/weibo/weiboList.do", params, context);
    }

    public static String syncSite(int userId, int mark, String userKey, String userSecret, Context context) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        params.put("mark", new StringBuilder(String.valueOf(mark)).toString());
        params.put("accessToken", userKey);
        params.put("accessSecret", userSecret);
        return HttpClientUtil.doPostRequest("http://sdk.mobcent.com/sdk/weibo/bind.do", params, context);
    }

    public static String cancelSyncSite(int userId, int siteId, Context context) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        params.put("mark", new StringBuilder(String.valueOf(siteId)).toString());
        return HttpClientUtil.doPostRequest("http://sdk.mobcent.com/sdk/weibo/unBind.do", params, context);
    }

    public static String shareApp(int userId, String content, Context context) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        params.put("content", content);
        return HttpClientUtil.doPostRequest("http://sdk.mobcent.com/sdk/weibo/shareTo.do", params, context);
    }
}
