package com.mobcent.android.api;

import android.content.Context;
import com.mobcent.android.constants.MCLibConstants;
import com.mobcent.android.constants.MCLibMobCentApiConstant;
import com.mobcent.android.utils.PhoneUtil;
import java.util.HashMap;

public class CommonRestfulApiRequester implements MCLibMobCentApiConstant {
    public static String doRegister(String userAgent, String imsi, String imei, Context context) {
        HashMap<String, String> params = new HashMap<>();
        params.put(MCLibMobCentApiConstant.UA, userAgent);
        params.put(MCLibMobCentApiConstant.IMSI, imsi);
        params.put("imei", imei);
        params.put("platType", MCLibConstants.ANDROID);
        params.put("version", PhoneUtil.getSDKVersion());
        params.put("version", MCLibConstants.SDK_VERSION);
        return HttpClientUtil.doPostRequest("http://sdk.mobcent.com/sdk/reg.do", params, context);
    }

    public static String doLogin(int uid, String userName, String email, String password, String userAgent, String imsi, String imei, String simulate, Context context) {
        HashMap<String, String> params = new HashMap<>();
        params.put("uid", new StringBuilder(String.valueOf(uid)).toString());
        params.put("userName", userName);
        params.put("email", email);
        params.put(MCLibMobCentApiConstant.PWD, password);
        params.put(MCLibMobCentApiConstant.UA, userAgent);
        params.put(MCLibMobCentApiConstant.IMSI, imsi);
        params.put("imei", imei);
        params.put("platType", MCLibConstants.ANDROID);
        params.put("version", PhoneUtil.getSDKVersion());
        params.put(MCLibMobCentApiConstant.SIMULATE, simulate);
        params.put("version", MCLibConstants.SDK_VERSION);
        return HttpClientUtil.doPostRequest("http://sdk.mobcent.com/sdk/login.do", params, context);
    }

    public static String getBundleAccounts(String imsi, String imei, int isAll, String ua, Context context) {
        HashMap<String, String> params = new HashMap<>();
        params.put(MCLibMobCentApiConstant.IMSI, imsi);
        params.put("imei", imei);
        params.put(MCLibMobCentApiConstant.UA, ua);
        params.put("isAll", new StringBuilder(String.valueOf(isAll)).toString());
        String jsonString = HttpClientUtil.doPostRequest("http://sdk.mobcent.com/sdk/gu.do", params, context);
        if (jsonString.equals("connection_fail")) {
            return "{}";
        }
        return jsonString;
    }

    public static String sendUserFeedback(int userId, String content, Context context) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        params.put("content", content);
        params.put("version", MCLibConstants.SDK_VERSION);
        String jsonString = HttpClientUtil.doPostRequest("http://sdk.mobcent.com/sdk/action/feedBack.do", params, context);
        if (jsonString.equals("connection_fail")) {
            return "{}";
        }
        return jsonString;
    }
}
