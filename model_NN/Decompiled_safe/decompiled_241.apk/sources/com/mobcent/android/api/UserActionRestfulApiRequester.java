package com.mobcent.android.api;

import android.content.Context;
import com.mobcent.android.constants.MCLibConstants;
import com.mobcent.android.constants.MCLibMobCentApiConstant;
import com.mobcent.android.utils.PhoneUtil;
import java.util.HashMap;

public class UserActionRestfulApiRequester implements MCLibMobCentApiConstant {
    public static String getUpdateBasicActionType(int userId, String time, Context context) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        params.put("time", time);
        String jsonString = HttpClientUtil.doPostRequest("http://sdk.mobcent.com/sdk/action/queryActionType.do", params, context);
        if (jsonString.equals("connection_fail")) {
            return "{}";
        }
        return jsonString;
    }

    public static String getUpdateMagicActionType(int userId, String time, Context context) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        params.put(MCLibMobCentApiConstant.LANGUAGE, context.getResources().getConfiguration().locale.getLanguage());
        params.put(MCLibMobCentApiConstant.TIME_STAMP, time);
        String jsonString = HttpClientUtil.doPostRequest("http://sdk.mobcent.com/sdk/action/queryMagicType.do", params, context);
        if (jsonString.equals("connection_fail")) {
            return "{}";
        }
        return jsonString;
    }

    public static String getHeartBeatInfo(int userId, boolean isActive, Context context) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        params.put(MCLibMobCentApiConstant.UA, PhoneUtil.getUserAgent());
        params.put("platType", MCLibConstants.ANDROID);
        params.put("version", PhoneUtil.getSDKVersion());
        params.put(MCLibMobCentApiConstant.LANGUAGE, context.getResources().getConfiguration().locale.getLanguage());
        params.put(MCLibMobCentApiConstant.IS_ACTIVE, isActive ? "true" : "false");
        String jsonString = HttpClientUtil.doPostRequest("http://sdk.mobcent.com/sdk/action/heartBeat.do", params, context);
        if (jsonString.equals("connection_fail")) {
            return "{}";
        }
        return jsonString;
    }

    public static String sendMagicAction(int userId, int targetId, int actionId, Context context) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        params.put(MCLibMobCentApiConstant.TARGET_ID, new StringBuilder(String.valueOf(targetId)).toString());
        params.put("actionId", new StringBuilder(String.valueOf(actionId)).toString());
        String jsonString = HttpClientUtil.doPostRequest("http://sdk.mobcent.com/sdk/action/sendAction.do", params, context);
        if (jsonString.equals("connection_fail")) {
            return "{}";
        }
        return jsonString;
    }

    public static String removeMagicAction(int userId, String ids, Context context) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        params.put("ids", new StringBuilder(String.valueOf(ids)).toString());
        String jsonString = HttpClientUtil.doPostRequest("http://sdk.mobcent.com/sdk/action/deleteAction.do", params, context);
        if (jsonString.equals("connection_fail")) {
            return "{}";
        }
        return jsonString;
    }
}
