package com.mobcent.android.api;

import android.content.Context;
import com.mobcent.android.constants.MCLibMobCentApiConstant;
import com.mobcent.android.db.UserMagicActionDictDBUtil;
import com.mobcent.android.state.MCLibAppState;
import java.util.HashMap;

public class UserInfoRestfulApiRequester implements MCLibMobCentApiConstant {
    public static String followUser(int userId, int toUserId, Context context) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        params.put(MCLibMobCentApiConstant.TARGET_ID, new StringBuilder(String.valueOf(toUserId)).toString());
        String jsonString = HttpClientUtil.doPostRequest("http://sdk.mobcent.com/sdk/action/addFocus.do", params, context);
        if (jsonString.equals("connection_fail")) {
            return "{}";
        }
        return jsonString;
    }

    public static String cancelFollowUser(int userId, int toUserId, Context context) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        params.put(MCLibMobCentApiConstant.TARGET_ID, new StringBuilder(String.valueOf(toUserId)).toString());
        String jsonString = HttpClientUtil.doPostRequest("http://sdk.mobcent.com/sdk/action/deleteFocus.do", params, context);
        if (jsonString.equals("connection_fail")) {
            return "{}";
        }
        return jsonString;
    }

    public static String getFriendsUserFollowed(int userId, int page, int pageSize, Context context) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        params.put(MCLibMobCentApiConstant.PAGE, new StringBuilder(String.valueOf(page)).toString());
        params.put(MCLibMobCentApiConstant.PAGE_SIZE, new StringBuilder(String.valueOf(pageSize)).toString());
        String jsonString = HttpClientUtil.doPostRequest("http://sdk.mobcent.com/sdk/action/queryMyFocus.do", params, context);
        if (jsonString.equals("connection_fail")) {
            return "{}";
        }
        return jsonString;
    }

    public static String getFriendsFollowedTheUser(int userId, int page, int pageSize, Context context) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        params.put(MCLibMobCentApiConstant.PAGE, new StringBuilder(String.valueOf(page)).toString());
        params.put(MCLibMobCentApiConstant.PAGE_SIZE, new StringBuilder(String.valueOf(pageSize)).toString());
        String jsonString = HttpClientUtil.doPostRequest("http://sdk.mobcent.com/sdk/action/queryMyFollower.do", params, context);
        if (jsonString.equals("connection_fail")) {
            return "{}";
        }
        return jsonString;
    }

    public static String getUserInfo(int userId, Context context) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        String jsonString = HttpClientUtil.doPostRequest("http://sdk.mobcent.com/sdk/action/queryProfile.do", params, context);
        if (jsonString.equals("connection_fail")) {
            return "{}";
        }
        return jsonString;
    }

    public static String changeUserProfile(int userId, String nickName, int gender, String birthday, String email, String location, Context context) {
        String str;
        String str2;
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        params.put("nickName", nickName == null ? "" : nickName);
        params.put(MCLibMobCentApiConstant.USER_GENDER, new StringBuilder(String.valueOf(gender)).toString());
        params.put(MCLibMobCentApiConstant.USER_BIRTHDAY, birthday);
        if (email == null) {
            str = "";
        } else {
            str = email;
        }
        params.put("email", str);
        if (location == null) {
            str2 = "";
        } else {
            str2 = location;
        }
        params.put(MCLibMobCentApiConstant.PLACE, str2);
        String jsonString = HttpClientUtil.doPostRequest("http://sdk.mobcent.com/sdk/action/profile.do", params, context);
        if (jsonString.equals("connection_fail")) {
            return "{}";
        }
        return jsonString;
    }

    public static String changeUserPwd(int userId, String pwd, Context context) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        params.put(MCLibMobCentApiConstant.USER_PWD, pwd);
        String jsonString = HttpClientUtil.doPostRequest("http://sdk.mobcent.com/sdk/action/pwd.do", params, context);
        if (jsonString.equals("connection_fail")) {
            return "{}";
        }
        return jsonString;
    }

    public static String getDownloadTheAppUsers(int userId, int appId, int page, int pageSize, Context context) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        params.put(MCLibMobCentApiConstant.TARGET_ID, new StringBuilder(String.valueOf(appId)).toString());
        params.put(MCLibMobCentApiConstant.PAGE, new StringBuilder(String.valueOf(page)).toString());
        params.put(MCLibMobCentApiConstant.PAGE_SIZE, new StringBuilder(String.valueOf(pageSize)).toString());
        String jsonString = HttpClientUtil.doPostRequest("http://sdk.mobcent.com/sdk/action/queryAppDownloadUser.do", params, context);
        if (jsonString.equals("connection_fail")) {
            return "{}";
        }
        return jsonString;
    }

    public static String searchUsers(int userId, String userNum, String userName, int type, int page, int pageSize, Context context) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        params.put("userNo", userNum);
        params.put("userName", userName);
        params.put("type", new StringBuilder(String.valueOf(type)).toString());
        params.put(MCLibMobCentApiConstant.PAGE, new StringBuilder(String.valueOf(page)).toString());
        params.put(MCLibMobCentApiConstant.PAGE_SIZE, new StringBuilder(String.valueOf(pageSize)).toString());
        String jsonString = HttpClientUtil.doPostRequest("http://sdk.mobcent.com/sdk/action/searchFriends.do", params, context);
        if (jsonString.equals("connection_fail")) {
            return "{}";
        }
        return jsonString;
    }

    public static String getRecommendUsers(int userId, int page, int pageSize, Context context) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        params.put(MCLibMobCentApiConstant.PAGE, new StringBuilder(String.valueOf(page)).toString());
        params.put(MCLibMobCentApiConstant.PAGE_SIZE, new StringBuilder(String.valueOf(pageSize)).toString());
        String jsonString = HttpClientUtil.doPostRequest("http://sdk.mobcent.com/sdk/action/searchFriends.do", params, context);
        if (jsonString.equals("connection_fail")) {
            return "{}";
        }
        return jsonString;
    }

    public static String changeUserPhoto(int userId, String iconPath, int type, Context context) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        params.put(MCLibMobCentApiConstant.ICON_PATH, iconPath);
        params.put(MCLibMobCentApiConstant.ICON_TYPE, new StringBuilder(String.valueOf(type)).toString());
        String jsonString = HttpClientUtil.doPostRequest("http://sdk.mobcent.com/sdk/action/modifyHead.do", params, context);
        if (jsonString.equals("connection_fail")) {
            return "{}";
        }
        return jsonString;
    }

    public static String uploadUserPhoto(int userId, String uploadFile, Context context) {
        return HttpClientUtil.uploadFile("http://img.mobcent.com/sdk/action/iconUpload.do?userId=" + userId + "&" + MCLibMobCentApiConstant.SESSION_ID + "=" + MCLibAppState.sessionId + "&" + "packageName" + "=" + context.getPackageName() + "&" + "appKey" + "=" + UserMagicActionDictDBUtil.getInstance(context).getAppKey(), uploadFile, context);
    }

    public static String getUserHomeInfo(int userId, int targetId, Context context) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        params.put(MCLibMobCentApiConstant.TARGET_ID, new StringBuilder(String.valueOf(targetId)).toString());
        String jsonString = HttpClientUtil.doPostRequest("http://sdk.mobcent.com/sdk/action/hisHome.do", params, context);
        if (jsonString.equals("connection_fail")) {
            return "{}";
        }
        return jsonString;
    }

    public static String getUserFans(int userId, int targetId, int page, int pageSize, Context context) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        params.put(MCLibMobCentApiConstant.TARGET_ID, new StringBuilder(String.valueOf(targetId)).toString());
        params.put(MCLibMobCentApiConstant.PAGE, new StringBuilder(String.valueOf(page)).toString());
        params.put(MCLibMobCentApiConstant.PAGE_SIZE, new StringBuilder(String.valueOf(pageSize)).toString());
        String jsonString = HttpClientUtil.doPostRequest("http://sdk.mobcent.com/sdk/action/queryHisFollower.do", params, context);
        if (jsonString.equals("connection_fail")) {
            return "{}";
        }
        return jsonString;
    }

    public static String getUserFriends(int userId, int targetId, int page, int pageSize, Context context) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        params.put(MCLibMobCentApiConstant.TARGET_ID, new StringBuilder(String.valueOf(targetId)).toString());
        params.put(MCLibMobCentApiConstant.PAGE, new StringBuilder(String.valueOf(page)).toString());
        params.put(MCLibMobCentApiConstant.PAGE_SIZE, new StringBuilder(String.valueOf(pageSize)).toString());
        String jsonString = HttpClientUtil.doPostRequest("http://sdk.mobcent.com/sdk/action/queryHisFocus.do", params, context);
        if (jsonString.equals("connection_fail")) {
            return "{}";
        }
        return jsonString;
    }

    public static String syncSites(int userId, String name, String pwd, int type, boolean isCancelSync, Context context) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        params.put("userName", name);
        params.put(MCLibMobCentApiConstant.PWD, pwd);
        params.put("type", new StringBuilder(String.valueOf(type)).toString());
        params.put("isCancel", new StringBuilder(String.valueOf(isCancelSync)).toString());
        String jsonString = HttpClientUtil.doPostRequest("http://sdk.mobcent.com/sdk/action/syncSite.do", params, context);
        if (jsonString.equals("connection_fail")) {
            return "{}";
        }
        return jsonString;
    }

    public static String addToBlackList(int userId, int targetId, Context context) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        params.put(MCLibMobCentApiConstant.TARGET_ID, new StringBuilder(String.valueOf(targetId)).toString());
        String jsonString = HttpClientUtil.doPostRequest("http://sdk.mobcent.com/sdk/action/addBlack.do", params, context);
        if (jsonString.equals("connection_fail")) {
            return "{}";
        }
        return jsonString;
    }

    public static String removeFromBlackList(int userId, int targetId, Context context) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        params.put(MCLibMobCentApiConstant.TARGET_ID, new StringBuilder(String.valueOf(targetId)).toString());
        String jsonString = HttpClientUtil.doPostRequest("http://sdk.mobcent.com/sdk/action/deleteBlack.do", params, context);
        if (jsonString.equals("connection_fail")) {
            return "{}";
        }
        return jsonString;
    }

    public static String getBlockUsers(int userId, int page, int pageSize, Context context) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        params.put(MCLibMobCentApiConstant.PAGE, new StringBuilder(String.valueOf(page)).toString());
        params.put(MCLibMobCentApiConstant.PAGE_SIZE, new StringBuilder(String.valueOf(pageSize)).toString());
        String jsonString = HttpClientUtil.doPostRequest("http://sdk.mobcent.com/sdk/action/queryBlack.do", params, context);
        if (jsonString.equals("connection_fail")) {
            return "{}";
        }
        return jsonString;
    }
}
