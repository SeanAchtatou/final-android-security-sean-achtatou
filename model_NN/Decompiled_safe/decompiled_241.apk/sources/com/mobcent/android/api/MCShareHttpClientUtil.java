package com.mobcent.android.api;

import android.content.Context;
import com.mobcent.android.utils.MCSharePhoneConnectionUtil;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

public class MCShareHttpClientUtil {
    public static int RECONNECTION_TIMES = 1;
    public static HashMap<String, Integer> urlMap = new HashMap<>();

    private static String getUnavailableNetworkString() {
        JSONObject json = new JSONObject();
        try {
            json.put("rs", 0);
            json.put("reason", "connection_fail");
            return json.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            return "{}";
        }
    }

    /* JADX INFO: Multiple debug info for r1v3 org.apache.http.client.methods.HttpPost: [D('proxy' org.apache.http.HttpHost), D('httpPost' org.apache.http.client.methods.HttpPost)] */
    /* JADX INFO: Multiple debug info for r8v8 java.lang.String: [D('entity' org.apache.http.HttpEntity), D('results' java.lang.String)] */
    public static String doPostRequest(String urlString, HashMap<String, String> params, String appKey, Context context) {
        HttpResponse httpResponse;
        boolean isApnWap = MCSharePhoneConnectionUtil.isCmwap(context) || MCSharePhoneConnectionUtil.isUniwap(context);
        if (isApnWap) {
            RECONNECTION_TIMES = 2;
        } else {
            RECONNECTION_TIMES = 1;
        }
        if (urlMap.containsKey(urlString)) {
            int times = urlMap.get(urlString).intValue();
            if (times >= RECONNECTION_TIMES) {
                urlMap.remove(urlString);
                return getUnavailableNetworkString();
            }
            urlMap.put(urlString, Integer.valueOf(times + 1));
        } else {
            urlMap.put(urlString, 1);
        }
        HttpHost proxy = new HttpHost("10.0.0.172", 80, "http");
        HttpHost target = new HttpHost(getUrlHost(urlString), 80, "http");
        HttpClient httpClient = new DefaultHttpClient();
        if (isApnWap) {
            httpClient.getParams().setParameter("http.route.default-proxy", proxy);
        }
        HttpPost httpPost = new HttpPost(urlString);
        HttpConnectionParams.setConnectionTimeout(httpClient.getParams(), 120000);
        HttpConnectionParams.setSoTimeout(httpClient.getParams(), 120000);
        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            params.put("packageName", context.getPackageName());
            params.put("appKey", appKey);
            if (params != null && !params.isEmpty()) {
                for (String key : params.keySet()) {
                    nameValuePairs.add(new BasicNameValuePair(key, params.get(key)));
                }
            }
            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));
            if (isApnWap) {
                httpResponse = httpClient.execute(target, httpPost);
            } else {
                httpResponse = httpClient.execute(httpPost);
            }
            if (httpResponse.getStatusLine().getStatusCode() != 200 && httpResponse.getStatusLine().getStatusCode() != 206) {
                return "connection_fail";
            }
            Header[] headers = httpResponse.getHeaders("Content-Type");
            if (headers != null) {
                for (Header header : headers) {
                    if ("text/vnd.wap.wml".equalsIgnoreCase(header.getValue())) {
                        return "connection_fail";
                    }
                }
            }
            String results = EntityUtils.toString(httpResponse.getEntity());
            urlMap.remove(urlString);
            if (results == null) {
                return "{}";
            }
            return results;
        } catch (ClientProtocolException e) {
            return "connection_fail";
        } catch (IOException e2) {
            return "connection_fail";
        } catch (Exception e3) {
            return "connection_fail";
        }
    }

    public static String getUrlHost(String srcUrl) {
        int k = srcUrl.toLowerCase().indexOf("/", 8);
        if (k == -1) {
            return "";
        }
        if (srcUrl.startsWith("http://")) {
            return srcUrl.substring(7, k);
        }
        if (srcUrl.startsWith("https://")) {
            return srcUrl.substring(8, k);
        }
        return srcUrl.substring(k + 1);
    }
}
