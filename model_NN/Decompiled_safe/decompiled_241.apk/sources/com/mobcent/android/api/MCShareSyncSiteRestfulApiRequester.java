package com.mobcent.android.api;

import android.content.Context;
import com.mobcent.android.constants.MCLibConstants;
import com.mobcent.android.constants.MCShareMobCentApiConstant;
import com.mobcent.android.utils.MCSharePhoneUtil;
import java.util.HashMap;

public class MCShareSyncSiteRestfulApiRequester implements MCShareMobCentApiConstant {
    public static String getAllSyncSites(int userId, String appKey, Context context) {
        HashMap<String, String> params = new HashMap<>();
        params.put("imei", MCSharePhoneUtil.getIMEI(context));
        params.put("platType", MCLibConstants.ANDROID);
        params.put(MCShareMobCentApiConstant.IS_JSON, "true");
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        params.put("appKey", appKey);
        return MCShareHttpClientUtil.doPostRequest("http://sdk.mobcent.com/sharesdk/weibo/weiboList.do", params, appKey, context);
    }

    public static String shareApp(int userId, String content, String picPath, String ids, String appKey, Context context) {
        HashMap<String, String> params = new HashMap<>();
        params.put("imei", MCSharePhoneUtil.getIMEI(context));
        params.put("appKey", appKey);
        params.put("content", content);
        params.put("picPath", picPath);
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        params.put("ids", ids);
        return MCShareHttpClientUtil.doPostRequest("http://sdk.mobcent.com/sharesdk/weibo/shareTo.do", params, appKey, context);
    }
}
