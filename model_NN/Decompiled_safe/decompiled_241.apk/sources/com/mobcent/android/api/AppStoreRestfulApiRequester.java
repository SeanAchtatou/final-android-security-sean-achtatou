package com.mobcent.android.api;

import android.content.Context;
import com.mobcent.android.constants.MCLibMobCentApiConstant;
import java.util.HashMap;

public class AppStoreRestfulApiRequester implements MCLibMobCentApiConstant {
    public static String getAppStoreAppInfo(int uid, int appId, Context context) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(uid)).toString());
        params.put(MCLibMobCentApiConstant.TARGET_ID, new StringBuilder(String.valueOf(appId)).toString());
        String jsonString = HttpClientUtil.doPostRequest("http://sdk.mobcent.com/sdk/action/queryAppDetail.do", params, context);
        if (jsonString.equals("connection_fail")) {
            return "{}";
        }
        return jsonString;
    }
}
