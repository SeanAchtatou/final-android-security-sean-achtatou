package com.mobcent.android.service.impl;

import android.content.Context;
import com.mobcent.android.db.RecentChatUsersDBUtil;
import com.mobcent.android.model.MCLibUserInfo;
import com.mobcent.android.service.MCLibRecentChatUserService;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MCLibRecentChatUserServiceImpl implements MCLibRecentChatUserService {
    private Context context;

    public MCLibRecentChatUserServiceImpl(Context context2) {
        this.context = context2;
    }

    public List<MCLibUserInfo> getRecentChatUsers() {
        List<MCLibUserInfo> recentChatUsers = RecentChatUsersDBUtil.getInstance(this.context).getRecentChatUsers();
        if (recentChatUsers != null && !recentChatUsers.isEmpty()) {
            Collections.sort(recentChatUsers, new ComparatorRecentChatUser());
        }
        return recentChatUsers;
    }

    public MCLibUserInfo getRecentChatUser(int uid) {
        return RecentChatUsersDBUtil.getInstance(this.context).getRecentChatUser(uid);
    }

    public boolean removeRecentChatUser(int uid) {
        return RecentChatUsersDBUtil.getInstance(this.context).removeRecentChatUser(uid);
    }

    public boolean addOrUpdateRecentChatUser(MCLibUserInfo userInfo) {
        return RecentChatUsersDBUtil.getInstance(this.context).addOrUpdateRecentChatUser(userInfo);
    }

    public void updateRecentChatUserMsgRead(MCLibUserInfo userInfo) {
        RecentChatUsersDBUtil.getInstance(this.context).updateRecentChatUserMsgRead(userInfo);
    }

    public class ComparatorRecentChatUser implements Comparator {
        public ComparatorRecentChatUser() {
        }

        public int compare(Object arg0, Object arg1) {
            MCLibUserInfo user0 = (MCLibUserInfo) arg0;
            MCLibUserInfo user1 = (MCLibUserInfo) arg1;
            int flag = 0;
            if (user0.getUnreadMsgCount() > user1.getUnreadMsgCount()) {
                flag = -1;
            } else if (user0.getUnreadMsgCount() < user1.getUnreadMsgCount()) {
                flag = 1;
            }
            if (flag != 0) {
                return flag;
            }
            if (user0.getLastMsgTime() > user1.getLastMsgTime()) {
                return -1;
            }
            if (user0.getLastMsgTime() < user1.getLastMsgTime()) {
                return 1;
            }
            return 0;
        }
    }
}
