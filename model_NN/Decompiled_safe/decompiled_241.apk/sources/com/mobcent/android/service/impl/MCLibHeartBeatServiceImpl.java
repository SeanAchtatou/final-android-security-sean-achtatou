package com.mobcent.android.service.impl;

import android.content.Context;
import android.util.Log;
import com.mobcent.android.api.UserActionRestfulApiRequester;
import com.mobcent.android.model.MCLibHeartBeatModel;
import com.mobcent.android.model.MCLibUserInfo;
import com.mobcent.android.model.MCLibUserStatus;
import com.mobcent.android.os.service.MCLibHeartBeatOSService;
import com.mobcent.android.service.MCLibHeartBeatService;
import com.mobcent.android.service.MCLibRecentChatUserService;
import com.mobcent.android.service.MCLibUserPrivateMsgService;
import com.mobcent.android.service.impl.helper.HeartBeatServiceHelper;
import java.util.Calendar;
import java.util.List;

public class MCLibHeartBeatServiceImpl implements MCLibHeartBeatService {
    public MCLibHeartBeatModel updateHeartBeatInfo(Context context, int userId, boolean isActive) {
        MCLibHeartBeatModel heartBeatModel = HeartBeatServiceHelper.formHeatBeatModel(UserActionRestfulApiRequester.getHeartBeatInfo(userId, isActive, context));
        if (heartBeatModel != null) {
            if (heartBeatModel.getInterval() > 0) {
                MCLibHeartBeatOSService.HEART_BEAT_INTERVAL = heartBeatModel.getInterval();
            }
            boolean isNew = false;
            if (heartBeatModel.getMsgList() != null && !heartBeatModel.getMsgList().isEmpty()) {
                String rs = new MCLibStatusServiceImpl(context).notifyServerMsgRead(userId, new MCLibUserPrivateMsgServiceImpl(context).addReceivedPrivateMsgs(userId, heartBeatModel.getMsgList()));
                if (rs != null && rs.equals("rs_succ")) {
                    Log.i("Remove remote msg", "Remove remote msg succ");
                }
                isNew = true;
            }
            if (heartBeatModel.getActionList() != null && !heartBeatModel.getActionList().isEmpty()) {
                MCLibUserPrivateMsgService upms = new MCLibUserPrivateMsgServiceImpl(context);
                String rs2 = upms.removeMagicActions(userId, upms.addReceivedPrivateMsgs(userId, heartBeatModel.getActionList()));
                if (rs2 != null && rs2.equals("rs_succ")) {
                    Log.i("Remove remote actions", "Remove remote actions succ");
                }
                isNew = true;
            }
            if (heartBeatModel.getSysMsgList() != null && !heartBeatModel.getSysMsgList().isEmpty()) {
                isNew = true;
            }
            if (heartBeatModel.isHasReply()) {
                MCLibHeartBeatOSService.hasReply = true;
                isNew = true;
            }
            heartBeatModel.setHasUpdates(isNew);
            handleReceivedMsgAction(heartBeatModel.getMsgList(), heartBeatModel.getActionList(), context);
        }
        return heartBeatModel;
    }

    private void handleReceivedMsgAction(List<MCLibUserStatus> msgs, List<MCLibUserStatus> actions, Context context) {
        MCLibRecentChatUserService recentChatUserService = new MCLibRecentChatUserServiceImpl(context);
        if (msgs != null && msgs.size() > 0) {
            for (MCLibUserStatus userStatus : msgs) {
                MCLibUserInfo userInfo = new MCLibUserInfo();
                userInfo.setUid(userStatus.getFromUserId());
                userInfo.setName(userStatus.getFromUserName());
                userInfo.setNickName(userStatus.getFromUserName());
                userInfo.setImage(userStatus.getUserImageUrl());
                userInfo.setUnreadMsgCount(1);
                userInfo.setLastMsgTime(Calendar.getInstance().getTimeInMillis());
                userInfo.setSourceName(userStatus.getSourceName());
                userInfo.setSourceProId(userStatus.getSourceProId());
                userInfo.setSourceUrl(userStatus.getSourceUrl());
                recentChatUserService.addOrUpdateRecentChatUser(userInfo);
            }
        }
        if (actions != null && actions.size() > 0) {
            for (MCLibUserStatus userStatus2 : actions) {
                MCLibUserInfo userInfo2 = new MCLibUserInfo();
                userInfo2.setUid(userStatus2.getFromUserId());
                userInfo2.setName(userStatus2.getFromUserName());
                userInfo2.setNickName(userStatus2.getFromUserName());
                userInfo2.setImage(userStatus2.getUserImageUrl());
                userInfo2.setUnreadMsgCount(1);
                userInfo2.setLastMsgTime(Calendar.getInstance().getTimeInMillis());
                userInfo2.setSourceName(userStatus2.getSourceName());
                userInfo2.setSourceProId(userStatus2.getSourceProId());
                userInfo2.setSourceUrl(userStatus2.getSourceUrl());
                recentChatUserService.addOrUpdateRecentChatUser(userInfo2);
            }
        }
    }
}
