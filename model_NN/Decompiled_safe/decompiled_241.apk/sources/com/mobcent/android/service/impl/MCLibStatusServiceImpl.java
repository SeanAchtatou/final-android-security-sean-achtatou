package com.mobcent.android.service.impl;

import android.content.Context;
import com.mobcent.android.api.StatusRestfulApiRequester;
import com.mobcent.android.db.SharedUserDBUtil;
import com.mobcent.android.db.StatusDBUtil;
import com.mobcent.android.db.UserCommunicationQueueDBUtil;
import com.mobcent.android.model.MCLibUserInfo;
import com.mobcent.android.model.MCLibUserStatus;
import com.mobcent.android.service.MCLibStatusService;
import com.mobcent.android.service.impl.helper.BaseJsonHelper;
import com.mobcent.android.service.impl.helper.StatusJsonHelper;
import com.mobcent.android.utils.DateUtil;
import java.util.ArrayList;
import java.util.List;

public class MCLibStatusServiceImpl implements MCLibStatusService {
    /* access modifiers changed from: private */
    public Context context;

    public MCLibStatusServiceImpl(Context context2) {
        this.context = context2;
    }

    public String forwordStatus(int userId, long statusId) {
        return StatusJsonHelper.formJsonRS(StatusRestfulApiRequester.forwordStatus(userId, statusId, this.context));
    }

    public List<MCLibUserStatus> getFollowedUserStatus(final int userId, final int page, int pageSize, boolean isReadLocally) {
        List<MCLibUserStatus> statusList = null;
        if (isReadLocally) {
            statusList = StatusDBUtil.getInstance(this.context).getFriendStatuses(userId, page, pageSize);
        }
        if (statusList == null || statusList.isEmpty() || !isReadLocally) {
            isReadLocally = false;
            statusList = StatusJsonHelper.formJsonUserStatusList(StatusRestfulApiRequester.getFollowedUserStatus(userId, page, pageSize, this.context), page, null, null);
            final List<MCLibUserStatus> toUpdateModels = new ArrayList<>();
            toUpdateModels.addAll(statusList);
            new Thread() {
                public void run() {
                    if (page == 1) {
                        StatusDBUtil.getInstance(MCLibStatusServiceImpl.this.context).removeAllFriendStatus();
                    }
                    StatusDBUtil.getInstance(MCLibStatusServiceImpl.this.context).addOrUpdateFriendStatus(userId, toUpdateModels);
                    StatusDBUtil.getInstance(MCLibStatusServiceImpl.this.context).addOrUpdateLastUpdateTime(3, DateUtil.getGenericCurrentTime());
                }
            }.start();
        }
        if (statusList != null && statusList.size() > 0) {
            int totalNum = statusList.get(0).getTotalNum();
            if (totalNum > statusList.size() + ((page - 1) * pageSize) && totalNum > pageSize) {
                MCLibUserStatus fetchMoreModel = new MCLibUserStatus();
                fetchMoreModel.setFetchMore(true);
                fetchMoreModel.setCurrentPage(page);
                fetchMoreModel.setReadFromLocal(isReadLocally);
                statusList.add(fetchMoreModel);
            } else if (isReadLocally && page > 1) {
                MCLibUserStatus refreshModel = StatusDBUtil.getRefreshUserStatusModel(StatusDBUtil.getInstance(this.context).getLastUpdateTime(3));
                refreshModel.setTotalNum(totalNum);
                refreshModel.setCurrentPage(page);
                statusList.add(refreshModel);
            }
        }
        return statusList;
    }

    public List<MCLibUserStatus> getMyStatus(final int userId, int targetId, final int page, int pageSize, boolean isReadLocally, boolean isNeedPersistent) {
        List<MCLibUserStatus> statusList = null;
        if (isReadLocally) {
            statusList = StatusDBUtil.getInstance(this.context).getMyStatuses(userId, page, pageSize);
        }
        if (statusList == null || statusList.isEmpty() || !isReadLocally) {
            isReadLocally = false;
            statusList = StatusJsonHelper.formJsonUserStatusList(StatusRestfulApiRequester.getMyStatus(userId, targetId, page, pageSize, this.context), page, null, null);
            if (isNeedPersistent) {
                final List<MCLibUserStatus> toUpdateModels = new ArrayList<>();
                toUpdateModels.addAll(statusList);
                new Thread() {
                    public void run() {
                        if (page == 1) {
                            StatusDBUtil.getInstance(MCLibStatusServiceImpl.this.context).removeAllMyStatus();
                        }
                        StatusDBUtil.getInstance(MCLibStatusServiceImpl.this.context).addOrUpdateMyStatus(userId, toUpdateModels);
                        StatusDBUtil.getInstance(MCLibStatusServiceImpl.this.context).addOrUpdateLastUpdateTime(1, DateUtil.getGenericCurrentTime());
                    }
                }.start();
            }
        }
        if (statusList != null && statusList.size() > 0) {
            int totalNum = statusList.get(0).getTotalNum();
            if (totalNum > statusList.size() + ((page - 1) * pageSize) && totalNum > pageSize) {
                MCLibUserStatus fetchMoreModel = new MCLibUserStatus();
                fetchMoreModel.setFetchMore(true);
                fetchMoreModel.setCurrentPage(page);
                fetchMoreModel.setReadFromLocal(isReadLocally);
                statusList.add(fetchMoreModel);
            } else if (isReadLocally && page > 1 && isNeedPersistent) {
                MCLibUserStatus refreshModel = StatusDBUtil.getRefreshUserStatusModel(StatusDBUtil.getInstance(this.context).getLastUpdateTime(1));
                refreshModel.setTotalNum(totalNum);
                refreshModel.setCurrentPage(page);
                statusList.add(refreshModel);
            }
        }
        return statusList;
    }

    public List<MCLibUserStatus> getStatusThread(int userId, long statusId, int page, int pageSize) {
        int totalNum;
        List<MCLibUserStatus> statusList = StatusJsonHelper.formJsonUserStatusList(StatusRestfulApiRequester.getStatusThread(userId, statusId, page, pageSize, this.context), page, null, null);
        if (statusList != null && statusList.size() > 0 && (totalNum = statusList.get(0).getTotalNum()) > statusList.size() + ((page - 1) * pageSize) && totalNum > pageSize) {
            MCLibUserStatus fetchMoreModel = new MCLibUserStatus();
            fetchMoreModel.setFetchMore(true);
            fetchMoreModel.setCurrentPage(page);
            statusList.add(fetchMoreModel);
        }
        return statusList;
    }

    public List<MCLibUserStatus> getStatusThreadAsComment(int userId, String objectId, String type, int page, int pageSize) {
        int totalNum;
        List<MCLibUserStatus> statusList = StatusJsonHelper.formJsonUserStatusList(StatusRestfulApiRequester.getStatusThreadAsComment(userId, objectId, type, page, pageSize, this.context), page, null, null);
        if (statusList != null && statusList.size() > 0 && (totalNum = statusList.get(0).getTotalNum()) > statusList.size() + ((page - 1) * pageSize) && totalNum > pageSize) {
            MCLibUserStatus fetchMoreModel = new MCLibUserStatus();
            fetchMoreModel.setFetchMore(true);
            fetchMoreModel.setCurrentPage(page);
            statusList.add(fetchMoreModel);
        }
        return statusList;
    }

    public List<MCLibUserStatus> getTalkToMeStatus(final int userId, final int page, int pageSize, boolean isReadLocally) {
        List<MCLibUserStatus> statusList = null;
        if (isReadLocally) {
            statusList = StatusDBUtil.getInstance(this.context).getAtMeStatuses(userId, page, pageSize);
        }
        if (statusList == null || statusList.isEmpty() || !isReadLocally) {
            isReadLocally = false;
            statusList = StatusJsonHelper.formJsonUserStatusList(StatusRestfulApiRequester.getTalkToMeStatus(userId, page, pageSize, this.context), page, null, null);
            final List<MCLibUserStatus> toUpdateModels = new ArrayList<>();
            toUpdateModels.addAll(statusList);
            new Thread() {
                public void run() {
                    if (page == 1) {
                        StatusDBUtil.getInstance(MCLibStatusServiceImpl.this.context).removeAllAtMeStatus();
                    }
                    StatusDBUtil.getInstance(MCLibStatusServiceImpl.this.context).addOrUpdateAtMeStatus(userId, toUpdateModels);
                    StatusDBUtil.getInstance(MCLibStatusServiceImpl.this.context).addOrUpdateLastUpdateTime(2, DateUtil.getGenericCurrentTime());
                }
            }.start();
        }
        if (statusList != null && statusList.size() > 0) {
            int totalNum = statusList.get(0).getTotalNum();
            if (totalNum > statusList.size() + ((page - 1) * pageSize) && totalNum > pageSize) {
                MCLibUserStatus fetchMoreModel = new MCLibUserStatus();
                fetchMoreModel.setFetchMore(true);
                fetchMoreModel.setCurrentPage(page);
                fetchMoreModel.setReadFromLocal(isReadLocally);
                statusList.add(fetchMoreModel);
            } else if (isReadLocally && page > 1) {
                MCLibUserStatus refreshModel = StatusDBUtil.getRefreshUserStatusModel(StatusDBUtil.getInstance(this.context).getLastUpdateTime(2));
                refreshModel.setTotalNum(totalNum);
                refreshModel.setCurrentPage(page);
                statusList.add(refreshModel);
            }
        }
        return statusList;
    }

    public List<MCLibUserStatus> getFriendEvents(final int userId, final int page, int pageSize, boolean isReadLocally) {
        List<MCLibUserStatus> eventList = null;
        if (isReadLocally) {
            eventList = StatusDBUtil.getInstance(this.context).getFriendEvents(userId, page, pageSize);
        }
        if (eventList == null || eventList.isEmpty() || !isReadLocally) {
            isReadLocally = false;
            eventList = StatusJsonHelper.formJsonUserEventList(StatusRestfulApiRequester.getFriendEvents(userId, page, pageSize, this.context), page, null);
            final List<MCLibUserStatus> toUpdateModels = new ArrayList<>();
            toUpdateModels.addAll(eventList);
            new Thread() {
                public void run() {
                    if (page == 1) {
                        StatusDBUtil.getInstance(MCLibStatusServiceImpl.this.context).removeAllFriendEvents();
                    }
                    StatusDBUtil.getInstance(MCLibStatusServiceImpl.this.context).addOrUpdateFriendEvent(userId, toUpdateModels);
                    StatusDBUtil.getInstance(MCLibStatusServiceImpl.this.context).addOrUpdateLastUpdateTime(6, DateUtil.getGenericCurrentTime());
                }
            }.start();
        }
        if (eventList != null && eventList.size() > 0) {
            int totalNum = eventList.get(0).getTotalNum();
            if (totalNum > eventList.size() + ((page - 1) * pageSize) && totalNum > pageSize) {
                MCLibUserStatus fetchMoreModel = new MCLibUserStatus();
                fetchMoreModel.setFetchMore(true);
                fetchMoreModel.setCurrentPage(page);
                fetchMoreModel.setReadFromLocal(isReadLocally);
                eventList.add(fetchMoreModel);
            } else if (isReadLocally && page > 1) {
                MCLibUserStatus refreshModel = StatusDBUtil.getRefreshUserStatusModel(StatusDBUtil.getInstance(this.context).getLastUpdateTime(6));
                refreshModel.setTotalNum(totalNum);
                refreshModel.setCurrentPage(page);
                eventList.add(refreshModel);
            }
        }
        return eventList;
    }

    public List<MCLibUserStatus> getHallEvents(final int userId, final int page, int pageSize, boolean isReadLocally) {
        List<MCLibUserStatus> eventList = null;
        if (isReadLocally) {
            eventList = StatusDBUtil.getInstance(this.context).getHallEvents(userId, page, pageSize);
        }
        if (eventList == null || eventList.isEmpty() || !isReadLocally) {
            isReadLocally = false;
            eventList = StatusJsonHelper.formJsonUserEventList(StatusRestfulApiRequester.getHallEvents(userId, page, pageSize, this.context), page, null);
            final List<MCLibUserStatus> toUpdateModels = new ArrayList<>();
            toUpdateModels.addAll(eventList);
            new Thread() {
                public void run() {
                    if (page == 1) {
                        StatusDBUtil.getInstance(MCLibStatusServiceImpl.this.context).removeAllHallEvents();
                    }
                    StatusDBUtil.getInstance(MCLibStatusServiceImpl.this.context).addOrUpdateHallEvent(userId, toUpdateModels);
                    StatusDBUtil.getInstance(MCLibStatusServiceImpl.this.context).addOrUpdateLastUpdateTime(7, DateUtil.getGenericCurrentTime());
                }
            }.start();
        }
        if (eventList != null && eventList.size() > 0) {
            int totalNum = eventList.get(0).getTotalNum();
            if (totalNum > eventList.size() + ((page - 1) * pageSize) && totalNum > pageSize) {
                MCLibUserStatus fetchMoreModel = new MCLibUserStatus();
                fetchMoreModel.setFetchMore(true);
                fetchMoreModel.setCurrentPage(page);
                fetchMoreModel.setReadFromLocal(isReadLocally);
                eventList.add(fetchMoreModel);
            } else if (isReadLocally && page > 1) {
                MCLibUserStatus refreshModel = StatusDBUtil.getRefreshUserStatusModel(StatusDBUtil.getInstance(this.context).getLastUpdateTime(7));
                refreshModel.setTotalNum(totalNum);
                refreshModel.setCurrentPage(page);
                eventList.add(refreshModel);
            }
        }
        return eventList;
    }

    public List<MCLibUserStatus> getHallStatus(final int userId, final int page, int pageSize, boolean isReadLocally) {
        List<MCLibUserStatus> statusList = null;
        if (isReadLocally) {
            statusList = StatusDBUtil.getInstance(this.context).getHallStatuses(userId, page, pageSize);
        }
        if (statusList == null || statusList.isEmpty() || !isReadLocally) {
            isReadLocally = false;
            statusList = StatusJsonHelper.formJsonUserStatusList(StatusRestfulApiRequester.getHallStatus(userId, page, pageSize, this.context), page, null, null);
            final List<MCLibUserStatus> toUpdateModels = new ArrayList<>();
            toUpdateModels.addAll(statusList);
            new Thread() {
                public void run() {
                    if (page == 1) {
                        StatusDBUtil.getInstance(MCLibStatusServiceImpl.this.context).removeAllHallStatus();
                    }
                    StatusDBUtil.getInstance(MCLibStatusServiceImpl.this.context).addOrUpdateHallStatus(userId, toUpdateModels);
                    StatusDBUtil.getInstance(MCLibStatusServiceImpl.this.context).addOrUpdateLastUpdateTime(4, DateUtil.getGenericCurrentTime());
                }
            }.start();
        }
        if (statusList != null && statusList.size() > 0) {
            int totalNum = statusList.get(0).getTotalNum();
            if (totalNum > statusList.size() + ((page - 1) * pageSize) && totalNum > pageSize) {
                MCLibUserStatus fetchMoreModel = new MCLibUserStatus();
                fetchMoreModel.setFetchMore(true);
                fetchMoreModel.setCurrentPage(page);
                fetchMoreModel.setReadFromLocal(isReadLocally);
                statusList.add(fetchMoreModel);
            } else if (isReadLocally && page > 1) {
                MCLibUserStatus refreshModel = StatusDBUtil.getRefreshUserStatusModel(StatusDBUtil.getInstance(this.context).getLastUpdateTime(4));
                refreshModel.setTotalNum(totalNum);
                refreshModel.setCurrentPage(page);
                statusList.add(refreshModel);
            }
        }
        return statusList;
    }

    public List<MCLibUserStatus> getHallTopic(final int userId, final int page, int pageSize, boolean isReadLocally) {
        List<MCLibUserStatus> statusList = null;
        if (isReadLocally) {
            statusList = StatusDBUtil.getInstance(this.context).getHallTopics(userId, page, pageSize);
        }
        if (statusList == null || statusList.isEmpty() || !isReadLocally) {
            isReadLocally = false;
            statusList = StatusJsonHelper.formJsonUserStatusList(StatusRestfulApiRequester.getHallTopic(userId, page, pageSize, this.context), page, null, null);
            final List<MCLibUserStatus> toUpdateModels = new ArrayList<>();
            toUpdateModels.addAll(statusList);
            new Thread() {
                public void run() {
                    if (page == 1) {
                        StatusDBUtil.getInstance(MCLibStatusServiceImpl.this.context).removeAllHallTopics();
                    }
                    StatusDBUtil.getInstance(MCLibStatusServiceImpl.this.context).addOrUpdateHallTopics(userId, toUpdateModels);
                    StatusDBUtil.getInstance(MCLibStatusServiceImpl.this.context).addOrUpdateLastUpdateTime(5, DateUtil.getGenericCurrentTime());
                }
            }.start();
        }
        if (statusList != null && statusList.size() > 0) {
            int totalNum = statusList.get(0).getTotalNum();
            if (totalNum > statusList.size() + ((page - 1) * pageSize) && totalNum > pageSize) {
                MCLibUserStatus fetchMoreModel = new MCLibUserStatus();
                fetchMoreModel.setFetchMore(true);
                fetchMoreModel.setCurrentPage(page);
                fetchMoreModel.setReadFromLocal(isReadLocally);
                statusList.add(fetchMoreModel);
            } else if (isReadLocally && page > 1) {
                MCLibUserStatus refreshModel = StatusDBUtil.getRefreshUserStatusModel(StatusDBUtil.getInstance(this.context).getLastUpdateTime(5));
                refreshModel.setTotalNum(totalNum);
                refreshModel.setCurrentPage(page);
                statusList.add(refreshModel);
            }
        }
        return statusList;
    }

    public String publishUserStatus(int userId, String content, int photoId, String photoPath) {
        return StatusJsonHelper.formJsonRS(StatusRestfulApiRequester.publishUserStatus(userId, content, photoId, photoPath, this.context));
    }

    public MCLibUserStatus publishStatusImage(int userId, String uploadFile) {
        return StatusJsonHelper.formUploadImageJson(StatusRestfulApiRequester.publishStatusImage(userId, uploadFile, this.context));
    }

    public String replyToUserStatus(int userId, String content, long statusId, long rootId, int photoId, String photoPath) {
        return StatusJsonHelper.formJsonRS(StatusRestfulApiRequester.replyToUserStatus(userId, content, statusId, rootId, photoId, photoPath, this.context));
    }

    public String sendMsg(int userId, String content, int toUserId, String toUserName, int actionId) {
        String jsonStr = StatusRestfulApiRequester.sendMsg(userId, content, toUserId, actionId, this.context);
        String rs = StatusJsonHelper.formJsonRS(jsonStr);
        if (rs != null && rs.equals("rs_succ")) {
            long id = BaseJsonHelper.getJsonId(jsonStr);
            MCLibUserInfo user = SharedUserDBUtil.getInstance(this.context).getCurrentUserInfo();
            String name = user.getNickName();
            if (name == null || name.trim().equals("")) {
                name = user.getName();
            }
            String photo = user.getImage();
            MCLibUserStatus userStatus = new MCLibUserStatus();
            userStatus.setStatusId(id);
            userStatus.setContent(content);
            userStatus.setFromUserId(userId);
            userStatus.setFromUserName(name);
            userStatus.setBelongUid(toUserId);
            userStatus.setUserImageUrl(photo);
            new MCLibUserPrivateMsgServiceImpl(this.context).addRepliedPrivateMsg(userId, toUserId, userStatus);
        }
        return rs;
    }

    public String talkToUser(int userId, String content, int toUserId) {
        return StatusJsonHelper.formJsonRS(StatusRestfulApiRequester.talkToUser(userId, content, toUserId, this.context));
    }

    public String modifyMyMood(int userId, String mood) {
        return StatusJsonHelper.formJsonRS(StatusRestfulApiRequester.modifyMyMood(userId, mood, this.context));
    }

    public List<MCLibUserStatus> getUserEvents(int userId, int targetId, int page, int pageSize) {
        int totalNum;
        List<MCLibUserStatus> statusList = StatusJsonHelper.formJsonUserEventList(StatusRestfulApiRequester.getUserEvents(userId, targetId, page, pageSize, this.context), page, null);
        if (statusList != null && statusList.size() > 0 && (totalNum = statusList.get(0).getTotalNum()) > statusList.size() + ((page - 1) * pageSize) && totalNum > pageSize) {
            MCLibUserStatus fetchMoreModel = new MCLibUserStatus();
            fetchMoreModel.setFetchMore(true);
            fetchMoreModel.setCurrentPage(page);
            statusList.add(fetchMoreModel);
        }
        return statusList;
    }

    public String notifyServerMsgRead(int userId, String ids) {
        return StatusJsonHelper.formJsonRS(StatusRestfulApiRequester.notifyServerMsgRead(userId, ids, this.context));
    }

    public boolean putStatusInQueue(MCLibUserStatus userStatus) {
        return UserCommunicationQueueDBUtil.getInstance(this.context).putCommunicationContent(userStatus);
    }
}
