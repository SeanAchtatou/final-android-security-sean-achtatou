package com.mobcent.android.service;

import com.mobcent.android.model.MCLibDownloadProfileModel;

public interface MCLibDownloadManagerService {
    void addOrUpdateDownloadProfile(MCLibDownloadProfileModel mCLibDownloadProfileModel, boolean z);

    MCLibDownloadProfileModel getDownloadProfileModel(int i);
}
