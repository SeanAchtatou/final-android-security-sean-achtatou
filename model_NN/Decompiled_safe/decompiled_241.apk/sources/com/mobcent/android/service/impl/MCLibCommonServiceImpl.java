package com.mobcent.android.service.impl;

import android.content.Context;
import com.mobcent.android.api.CommonRestfulApiRequester;
import com.mobcent.android.service.MCLibCommonService;
import com.mobcent.android.service.impl.helper.UserActionServiceHelper;

public class MCLibCommonServiceImpl implements MCLibCommonService {
    private Context context;

    public MCLibCommonServiceImpl(Context context2) {
        this.context = context2;
    }

    public String sendUserFeedback(int userId, String content) {
        return UserActionServiceHelper.formJsonRS(CommonRestfulApiRequester.sendUserFeedback(userId, content, this.context));
    }
}
