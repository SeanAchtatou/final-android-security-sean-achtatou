package com.mobcent.android.service.impl.helper;

import com.mobcent.android.constants.MCLibMobCentApiConstant;
import com.mobcent.android.model.MCLibUserStatus;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class StatusJsonHelper extends BaseJsonHelper implements MCLibMobCentApiConstant {
    /* JADX INFO: Multiple debug info for r9v2 java.util.List<com.mobcent.android.model.MCLibUserStatus>: [D('statusList' java.util.List<com.mobcent.android.model.MCLibUserStatus>), D('e' org.json.JSONException)] */
    /* JADX INFO: Multiple debug info for r9v7 java.util.List<com.mobcent.android.model.MCLibUserStatus>: [D('statusList' java.util.List<com.mobcent.android.model.MCLibUserStatus>), D('array' org.json.JSONArray)] */
    /* JADX INFO: Multiple debug info for r0v5 com.mobcent.android.model.MCLibUserStatus: [D('userStatus' com.mobcent.android.model.MCLibUserStatus), D('statusJson' org.json.JSONObject)] */
    /* JADX INFO: Multiple debug info for r9v8 java.util.List<com.mobcent.android.model.MCLibUserStatus>: [D('statusList' java.util.List<com.mobcent.android.model.MCLibUserStatus>), D('array' org.json.JSONArray)] */
    public static List<MCLibUserStatus> formJsonUserStatusList(String jsonStr, int page, String baseUrl, String imgUrl) {
        List<MCLibUserStatus> statusList = new ArrayList<>();
        try {
            JSONObject jsonObj = new JSONObject(jsonStr);
            int totalNum = jsonObj.optInt(MCLibMobCentApiConstant.TOTAL_NUM);
            if (baseUrl == null || "".equals(baseUrl)) {
                baseUrl = jsonObj.optString("baseUrl");
            }
            if (imgUrl == null || "".equals(imgUrl)) {
                imgUrl = jsonObj.optString("imgUrl");
            }
            String contentUrl = jsonObj.optString(MCLibMobCentApiConstant.CONTENT_URL);
            JSONArray array = jsonObj.optJSONArray("list");
            if (array == null) {
                return statusList;
            }
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= array.length()) {
                    return statusList;
                }
                statusList.add(getUserStatus(array.getJSONObject(i2), totalNum, baseUrl, imgUrl, contentUrl, page, false));
                i = i2 + 1;
            }
        } catch (JSONException e) {
            return statusList;
        }
    }

    /* JADX INFO: Multiple debug info for r28v2 com.mobcent.android.model.MCLibUserStatus: [D('statusJson' org.json.JSONObject), D('replyStatus' com.mobcent.android.model.MCLibUserStatus)] */
    /* JADX INFO: Multiple debug info for r3v3 org.json.JSONObject: [D('content' java.lang.String), D('replyContent' org.json.JSONObject)] */
    /* JADX INFO: Multiple debug info for r28v3 com.mobcent.android.model.MCLibUserStatus: [D('statusJson' org.json.JSONObject), D('replyStatus' com.mobcent.android.model.MCLibUserStatus)] */
    public static MCLibUserStatus getUserStatus(JSONObject statusJson, int totalNum, String baseUrl, String imgUrl, String contentUrl, int page, boolean ignoreReplyContent) {
        String str;
        if (statusJson == null) {
            return null;
        }
        MCLibUserStatus userStatus = new MCLibUserStatus();
        long id = statusJson.optLong("id");
        String content = statusJson.optString("content");
        int toUserId = statusJson.optInt("toUserId");
        String toUserName = statusJson.optString("toUserName");
        int userId = statusJson.optInt("userId");
        String userName = statusJson.optString("userName");
        String time = statusJson.optString("time");
        int replyNum = statusJson.optInt("replyNum");
        int forwardNum = statusJson.optInt(MCLibMobCentApiConstant.FORWARD_NUM);
        int type = statusJson.optInt("type");
        String typeName = statusJson.optString("typeName");
        String icon = statusJson.optString("icon");
        long rootId = statusJson.optLong("rootId");
        String domainUrl = statusJson.optString("url");
        String sourceName = statusJson.optString("sourceName");
        String sourceUrl = statusJson.optString("sourceUrl");
        int sourceProId = statusJson.optInt(MCLibMobCentApiConstant.SOURCE_PRO_ID);
        int sourceCategoryId = statusJson.optInt("sourceCategoryId");
        String photoPath = statusJson.optString("photoPath");
        int topicStatus = statusJson.optInt("topicStatus");
        boolean isDel = statusJson.optBoolean(MCLibMobCentApiConstant.IS_DELETE);
        String contentAddr = statusJson.optString(MCLibMobCentApiConstant.CONTENT_ADDR);
        userStatus.setCurrentPage(page);
        userStatus.setTotalNum(totalNum);
        userStatus.setStatusId(id);
        userStatus.setCommunicationType(type);
        userStatus.setContent(content);
        userStatus.setToUid(toUserId);
        userStatus.setToUserName(toUserName);
        userStatus.setUid(userId);
        userStatus.setUserName(userName);
        userStatus.setTime(time);
        userStatus.setReplyNum(replyNum);
        userStatus.setForwardNum(forwardNum);
        userStatus.setTypeName(typeName);
        userStatus.setRootId(rootId);
        userStatus.setUserImageUrl(String.valueOf(baseUrl) + icon);
        userStatus.setDomainUrl(domainUrl);
        userStatus.setSourceName(sourceName);
        userStatus.setSourceProId(sourceProId);
        userStatus.setSourceUrl(sourceUrl);
        userStatus.setSourceCategoryId(sourceCategoryId);
        if (type == 8) {
            str = "";
        } else {
            str = imgUrl;
        }
        userStatus.setImgUrl(str);
        userStatus.setPhotoPath(photoPath);
        userStatus.setTopicStatus(topicStatus);
        userStatus.setDel(isDel);
        userStatus.setContentPathUrl(String.valueOf(contentUrl) + contentAddr);
        if (!ignoreReplyContent) {
            userStatus.setReplyStatus(getUserStatus(statusJson.optJSONObject("replyContent"), 1, baseUrl, imgUrl, contentUrl, 1, true));
        } else {
            userStatus.setReplyStatus(null);
        }
        return userStatus;
    }

    /* JADX INFO: Multiple debug info for r21v2 java.util.List<com.mobcent.android.model.MCLibUserStatus>: [D('e' org.json.JSONException), D('eventList' java.util.List<com.mobcent.android.model.MCLibUserStatus>)] */
    /* JADX INFO: Multiple debug info for r21v6 java.util.List<com.mobcent.android.model.MCLibUserStatus>: [D('array' org.json.JSONArray), D('eventList' java.util.List<com.mobcent.android.model.MCLibUserStatus>)] */
    /* JADX INFO: Multiple debug info for r5v1 int: [D('sourceCategoryId' int), D('eventJson' org.json.JSONObject)] */
    /* JADX INFO: Multiple debug info for r21v7 java.util.List<com.mobcent.android.model.MCLibUserStatus>: [D('array' org.json.JSONArray), D('eventList' java.util.List<com.mobcent.android.model.MCLibUserStatus>)] */
    public static List<MCLibUserStatus> formJsonUserEventList(String jsonStr, int page, String baseUrl) {
        List<MCLibUserStatus> eventList = new ArrayList<>();
        try {
            JSONObject jSONObject = new JSONObject(jsonStr);
            int totalNum = jSONObject.optInt(MCLibMobCentApiConstant.TOTAL_NUM);
            if (baseUrl == null || "".equals(baseUrl)) {
                baseUrl = jSONObject.optString("baseUrl");
            }
            JSONArray array = jSONObject.optJSONArray("list");
            if (array == null) {
                return eventList;
            }
            int categoryId = 0;
            while (true) {
                int i = categoryId;
                if (i >= array.length()) {
                    return eventList;
                }
                JSONObject eventJson = array.getJSONObject(i);
                MCLibUserStatus userStatus = new MCLibUserStatus();
                String content = eventJson.optString(MCLibMobCentApiConstant.DESC);
                int fromUserId = eventJson.optInt("fromUserId");
                String fromUserName = eventJson.optString("fromUserName");
                int targetJumpId = eventJson.optInt(MCLibMobCentApiConstant.TARGET_ID);
                String targetJumpName = eventJson.optString(MCLibMobCentApiConstant.TARGET_NAME);
                String time = eventJson.optString("time");
                int type = eventTypeAdapter(eventJson.optInt("type"));
                String icon = eventJson.optString("icon");
                String domainUrl = eventJson.optString("url");
                int categoryId2 = eventJson.optInt(MCLibMobCentApiConstant.CATEGORY_ID);
                String sourceName = eventJson.optString("sourceName");
                String sourceUrl = eventJson.optString("sourceUrl");
                int sourceProId = eventJson.optInt(MCLibMobCentApiConstant.SOURCE_PRO_ID);
                int sourceCategoryId = eventJson.optInt("sourceCategoryId");
                userStatus.setCurrentPage(page);
                userStatus.setTotalNum(totalNum);
                userStatus.setCommunicationType(type);
                userStatus.setContent(content);
                userStatus.setTime(time);
                userStatus.setFromUserId(fromUserId);
                userStatus.setFromUserName(fromUserName);
                userStatus.setTargetJumpId(targetJumpId);
                userStatus.setTargetJumpName(targetJumpName);
                userStatus.setUserImageUrl(String.valueOf(baseUrl) + icon);
                userStatus.setDomainUrl(domainUrl);
                userStatus.setCategoryId(categoryId2);
                userStatus.setSourceName(sourceName);
                userStatus.setSourceProId(sourceProId);
                userStatus.setSourceUrl(sourceUrl);
                userStatus.setSourceCategoryId(sourceCategoryId);
                eventList.add(userStatus);
                categoryId = i + 1;
            }
        } catch (JSONException e) {
            return eventList;
        }
    }

    public static MCLibUserStatus formUploadImageJson(String jsonStr) {
        MCLibUserStatus userStatus = new MCLibUserStatus();
        try {
            JSONObject jsonObj = new JSONObject(jsonStr);
            if (jsonObj.optInt("rs") == 0) {
                userStatus.setErrorMsg(jsonObj.optString("reason"));
            } else {
                userStatus.setPhotoId(jsonObj.optInt("photoId"));
                userStatus.setPhotoPath(jsonObj.optString("photoPath"));
            }
        } catch (JSONException e) {
            userStatus.setErrorMsg("Upload fail!");
        }
        return userStatus;
    }

    public static int eventTypeAdapter(int type) {
        if (type == 1) {
            return 12;
        }
        if (type == 4) {
            return 16;
        }
        if (type == 5) {
            return 17;
        }
        return type == 6 ? 18 : 12;
    }
}
