package com.mobcent.android.service;

import com.mobcent.android.model.MCLibUserInfo;
import java.util.HashMap;
import java.util.List;

public interface MCLibUserInfoService {
    String addToBlackList(int i, int i2);

    String cancelFollowUser(int i, int i2);

    String changeUserPhoto(int i, String str, int i2);

    String changeUserProfile(int i, String str, int i2, String str2, String str3, String str4);

    String changeUserPwd(int i, String str);

    String doLogin(int i, String str, String str2, String str3, boolean z, boolean z2, String str4);

    String followUser(int i, int i2);

    List<MCLibUserInfo> getAllLocalUsers();

    HashMap<Integer, String> getAllSyncSites(int i);

    List<MCLibUserInfo> getBlockList(int i, int i2, int i3);

    int getCommunityHomeMode();

    List<MCLibUserInfo> getFriendsFollowedTheUser(int i, int i2, int i3);

    List<MCLibUserInfo> getFriendsUserFollowed(int i, int i2, int i3);

    MCLibUserInfo getLoginUser();

    int getLoginUserId();

    String getLoginUserName();

    String getLoginUserNickName();

    List<MCLibUserInfo> getRecommendUsers(int i, int i2, int i3);

    List<MCLibUserInfo> getUserFans(int i, int i2, int i3, int i4);

    List<MCLibUserInfo> getUserFriends(int i, int i2, int i3, int i4);

    MCLibUserInfo getUserHomeInfo(int i, int i2);

    MCLibUserInfo getUserProfile(int i);

    boolean isAutoLogin();

    boolean isSavePwd();

    boolean isSoundEnable();

    MCLibUserInfo regUser();

    String removeFromBlackList(int i, int i2);

    List<MCLibUserInfo> searchUsers(int i, String str, String str2, int i2, int i3, int i4);

    void setSoundEnable(boolean z);

    String syncSites(int i, String str, String str2, int i2, boolean z);

    void updateCommunityHomeMode(int i);

    void updateShareUserBirthday(String str);

    void updateShareUserCity(String str);

    void updateShareUserEmail(String str);

    void updateShareUserGender(int i);

    void updateShareUserMood(String str);

    void updateShareUserNickName(String str);

    void updateSharedUserPhoto(String str);

    MCLibUserInfo uploadUserPhoto(int i, String str);
}
