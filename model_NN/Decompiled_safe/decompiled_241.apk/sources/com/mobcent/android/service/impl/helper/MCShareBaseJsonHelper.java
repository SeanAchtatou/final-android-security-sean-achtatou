package com.mobcent.android.service.impl.helper;

import com.mobcent.android.constants.MCShareMobCentApiConstant;
import org.json.JSONException;
import org.json.JSONObject;

public class MCShareBaseJsonHelper implements MCShareMobCentApiConstant {
    public static String formJsonRS(String jsonStr) {
        try {
            JSONObject jsonObj = new JSONObject(jsonStr);
            int rs = jsonObj.optInt("rs");
            if (rs == 1) {
                return "rs_succ";
            }
            if (rs == 10000) {
                return "emailExist";
            }
            if (rs == 10001) {
                return "nickNameExist";
            }
            if (rs == 10002) {
                return "prohibitPublish";
            }
            if (rs == 10003) {
                return "msgBlock";
            }
            if (rs == 10004) {
                return "blockItself";
            }
            if (rs == 10005) {
                return "nonBindSite";
            }
            if (rs == 10010) {
                return "blockNonUser";
            }
            return jsonObj.optString("reason");
        } catch (JSONException e) {
            return "Connection fail, pls try again.";
        }
    }

    public static long getJsonId(String jsonStr) {
        try {
            return new JSONObject(jsonStr).optLong("id", 0);
        } catch (JSONException e) {
            return 0;
        }
    }
}
