package com.mobcent.android.service.impl.helper;

import com.mobcent.android.constants.MCLibMobCentApiConstant;
import com.mobcent.android.model.MCLibActionModel;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class UserActionServiceHelper extends BaseJsonHelper implements MCLibMobCentApiConstant {
    /* JADX INFO: Multiple debug info for r13v2 java.util.List<com.mobcent.android.model.MCLibActionModel>: [D('list' java.util.List<com.mobcent.android.model.MCLibActionModel>), D('e' org.json.JSONException)] */
    /* JADX INFO: Multiple debug info for r0v1 org.json.JSONArray: [D('jsonObj' org.json.JSONObject), D('array' org.json.JSONArray)] */
    /* JADX INFO: Multiple debug info for r13v14 java.lang.String: [D('actionPreviewImageUrl' java.lang.String), D('object' org.json.JSONObject)] */
    public static List<MCLibActionModel> formJsonMagicActionModelList(String jsonStr) {
        List<MCLibActionModel> list = new ArrayList<>();
        try {
            JSONObject jsonObj = new JSONObject(jsonStr);
            int totalNum = jsonObj.optInt(MCLibMobCentApiConstant.TOTAL_NUM);
            String baseUrl = jsonObj.optString("baseUrl");
            String timeStamp = jsonObj.optString(MCLibMobCentApiConstant.TIME_STAMP);
            boolean isActionEnable = jsonObj.optBoolean(MCLibMobCentApiConstant.ACTION_ENABLE);
            JSONArray array = jsonObj.optJSONArray("list");
            if (array == null) {
                return list;
            }
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= array.length()) {
                    return list;
                }
                MCLibActionModel model = new MCLibActionModel();
                JSONObject object = (JSONObject) array.get(i2);
                int id = object.optInt("id");
                String content = object.optString("content");
                String icon = object.optString("icon");
                String shortName = object.optString(MCLibMobCentApiConstant.SHORT_NAME);
                String actionPreviewImageUrl = object.optString(MCLibMobCentApiConstant.ACTION_PREVIEW_IMAGE_URL);
                model.setActionId(id);
                model.setBaseUrl(baseUrl);
                model.setContent(content);
                model.setIcon(icon);
                model.setActionPrevImg(actionPreviewImageUrl);
                model.setTotalNum(totalNum);
                model.setShortName(shortName);
                model.setTime(timeStamp);
                model.setActionEnable(isActionEnable);
                list.add(model);
                i = i2 + 1;
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return list;
        }
    }

    public static int formJsonIsActionEnable(String jsonStr) {
        try {
            return new JSONObject(jsonStr).getBoolean(MCLibMobCentApiConstant.ACTION_ENABLE) ? 1 : 0;
        } catch (JSONException e) {
            return 2;
        }
    }

    public static List<MCLibActionModel> formJsonBasicActionModelList(String jsonStr) {
        List<MCLibActionModel> list = new ArrayList<>();
        try {
            JSONObject jsonObj = new JSONObject(jsonStr);
            JSONArray array = jsonObj.optJSONArray("list");
            if (array != null) {
                int totalNum = jsonObj.optInt(MCLibMobCentApiConstant.TOTAL_NUM);
                for (int i = 0; i < array.length(); i++) {
                    MCLibActionModel model = new MCLibActionModel();
                    JSONObject object = (JSONObject) array.get(i);
                    int id = object.optInt("id");
                    String content = object.optString("content");
                    String shortName = object.optString(MCLibMobCentApiConstant.SHORT_NAME);
                    model.setActionId(id);
                    model.setContent(content);
                    model.setTotalNum(totalNum);
                    model.setShortName(shortName);
                    list.add(model);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return list;
    }
}
