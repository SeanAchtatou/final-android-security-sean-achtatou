package com.mobcent.android.service;

import com.mobcent.android.model.MCLibUserInfo;
import java.util.List;

public interface MCLibRecentChatUserService {
    boolean addOrUpdateRecentChatUser(MCLibUserInfo mCLibUserInfo);

    MCLibUserInfo getRecentChatUser(int i);

    List<MCLibUserInfo> getRecentChatUsers();

    boolean removeRecentChatUser(int i);

    void updateRecentChatUserMsgRead(MCLibUserInfo mCLibUserInfo);
}
