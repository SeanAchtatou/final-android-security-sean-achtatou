package com.mobcent.android.service.installer.manager;

import com.mobcent.android.model.MCLibAppStoreAppModel;
import java.io.File;

public interface MCLibInstallerManager {
    MCLibAppStoreAppModel doCheckPkgStatus(MCLibAppStoreAppModel mCLibAppStoreAppModel);

    void doInstallPkg(File file);

    void doUninstallPkg(MCLibAppStoreAppModel mCLibAppStoreAppModel);

    boolean isPackageInstalled(String str);
}
