package com.mobcent.android.service;

import android.content.Context;
import com.mobcent.android.model.MCLibUserStatus;
import java.util.List;

public interface MCLibUserPrivateMsgService {
    String addReceivedPrivateMsgs(int i, List<MCLibUserStatus> list);

    boolean addRepliedPrivateMsg(int i, int i2, MCLibUserStatus mCLibUserStatus);

    boolean addUserCustomizedQuickMsg(int i, String str);

    List<String> getAllUserCustomizedQuickMsgs(int i);

    List<MCLibUserStatus> getUserHistoryPrivateMsg(int i, int i2, int i3, int i4);

    int getUserUnreadActionCount(Context context, int i);

    int getUserUnreadMsgCount(Context context, int i);

    List<MCLibUserStatus> getUserUnreadPrivateMsg(int i, int i2);

    boolean removeAllMsg(int i, int i2);

    String removeMagicActions(int i, String str);

    boolean removeMsg(int i);

    boolean removeUserCustomizedQuickMsg(int i, String str);

    String sendMagicAction(Context context, int i, int i2, String str, int i3, String str2);
}
