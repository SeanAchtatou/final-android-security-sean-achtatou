package com.mobcent.android.service.delegate;

public interface MCLibMsgBundleDelegate {
    String getPublishFailMsg();

    String getPublishProhibitionMsg();

    String getPublishSuccMsg();
}
