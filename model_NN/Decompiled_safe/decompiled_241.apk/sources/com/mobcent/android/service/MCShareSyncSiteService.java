package com.mobcent.android.service;

import com.mobcent.android.model.MCShareSyncSiteModel;
import java.util.List;

public interface MCShareSyncSiteService {
    List<MCShareSyncSiteModel> getAllSyncSites(String str);

    String shareApp(String str, String str2, String str3, String str4);
}
