package com.mobcent.android.service.impl;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.mobcent.android.api.HttpClientUtil;
import com.mobcent.android.service.MCLibFileTransferService;

public class MCLibFileTransferServiceImpl implements MCLibFileTransferService {
    private Context context;

    public MCLibFileTransferServiceImpl(Context context2) {
        this.context = context2;
    }

    public Bitmap getBitmapFromUrl(String url) {
        byte[] imgData = HttpClientUtil.getFileInByte(url, this.context);
        if (imgData == null) {
            return null;
        }
        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
        bitmapOptions.inSampleSize = 1;
        return BitmapFactory.decodeByteArray(imgData, 0, imgData.length, bitmapOptions);
    }

    public byte[] getImageStream(String url) {
        return HttpClientUtil.getFileInByte(url, this.context);
    }
}
