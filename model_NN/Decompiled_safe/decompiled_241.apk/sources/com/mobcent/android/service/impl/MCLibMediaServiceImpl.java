package com.mobcent.android.service.impl;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import com.mobcent.android.service.MCLibMediaService;

public class MCLibMediaServiceImpl implements MCLibMediaService {
    public void playAudio(Context context, int resId) {
        final MediaPlayer mediaPlayer = MediaPlayer.create(context, resId);
        mediaPlayer.setAudioStreamType(3);
        AudioManager mAudioManager = (AudioManager) context.getSystemService("audio");
        int maxVolumeMusic = mAudioManager.getStreamMaxVolume(3);
        int currentVolume = (maxVolumeMusic * mAudioManager.getStreamVolume(2)) / mAudioManager.getStreamMaxVolume(2);
        mediaPlayer.setVolume((float) currentVolume, (float) currentVolume);
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                mediaPlayer.release();
            }
        });
        mediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            public boolean onError(MediaPlayer mp, int what, int extra) {
                mediaPlayer.release();
                return false;
            }
        });
        mediaPlayer.start();
    }
}
