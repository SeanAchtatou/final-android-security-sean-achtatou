package com.mobcent.android.service.impl.helper;

import com.mobcent.android.constants.MCLibMobCentApiConstant;
import com.mobcent.android.model.MCLibAppStoreAppModel;
import com.mobcent.android.model.MCLibUserInfo;
import com.mobcent.android.state.MCLibAppState;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class UserInfoJsonHelper extends BaseJsonHelper implements MCLibMobCentApiConstant {
    public static MCLibUserInfo formLoginStatus(String jsonStr) {
        if (jsonStr.equals("{}")) {
            MCLibUserInfo user = new MCLibUserInfo();
            user.setUid(0);
            return user;
        }
        try {
            JSONObject jsonObj = new JSONObject(jsonStr);
            int userId = jsonObj.optInt("userId");
            String userName = jsonObj.optString("userName");
            String nickName = jsonObj.optString("nickName");
            String mood = jsonObj.optString(MCLibMobCentApiConstant.EMOTION_WORDS);
            String userPhoto = jsonObj.optString("icon");
            MCLibAppState.sessionId = jsonObj.optString(MCLibMobCentApiConstant.SESSION_ID);
            MCLibAppState.isValidLogin = true;
            MCLibAppState.serverHeartBeatReq = MCLibAppState.APP_RUNNING;
            MCLibUserInfo user2 = new MCLibUserInfo();
            user2.setUid(userId);
            user2.setName(userName);
            user2.setNickName(nickName);
            user2.setEmotionWords(mood);
            user2.setImage(userPhoto);
            return user2;
        } catch (JSONException e) {
            MCLibUserInfo user3 = new MCLibUserInfo();
            user3.setUid(0);
            return user3;
        }
    }

    public static MCLibUserInfo formRegJsonUser(String jsonStr) {
        if (jsonStr.equals("{\"rs\":0}")) {
            MCLibUserInfo user = new MCLibUserInfo();
            user.setUid(0);
            return user;
        }
        try {
            JSONObject jsonObj = new JSONObject(jsonStr);
            int userId = jsonObj.optInt("uid");
            String userName = jsonObj.optString("userName");
            String pwd = jsonObj.optString(MCLibMobCentApiConstant.PWD);
            String icon = jsonObj.optString("icon");
            MCLibAppState.sessionId = jsonObj.optString(MCLibMobCentApiConstant.SESSION_ID);
            MCLibAppState.isValidLogin = true;
            MCLibAppState.serverHeartBeatReq = MCLibAppState.APP_RUNNING;
            MCLibUserInfo user2 = new MCLibUserInfo();
            user2.setUid(userId);
            user2.setName(userName);
            user2.setPassword(pwd);
            user2.setImage(icon);
            user2.setNickName(userName);
            return user2;
        } catch (JSONException e) {
            MCLibUserInfo user3 = new MCLibUserInfo();
            user3.setUid(0);
            return user3;
        }
    }

    public static List<MCLibUserInfo> getJsonUsers(String jsonStr, int currentPage) {
        if (jsonStr.equals("{\"rs\":0}")) {
            return new ArrayList();
        }
        List<MCLibUserInfo> users = new ArrayList<>();
        try {
            JSONObject result = new JSONObject(jsonStr);
            int totalNum = result.optInt(MCLibMobCentApiConstant.TOTAL_NUM);
            String baseUrl = result.optString("baseUrl");
            JSONArray jsonArray = result.optJSONArray("list");
            if (jsonArray == null) {
                return new ArrayList();
            }
            for (int i = 0; i < jsonArray.length(); i++) {
                users.add(getJsonUser(jsonArray.optJSONObject(i), currentPage, totalNum, baseUrl));
            }
            return users;
        } catch (JSONException e) {
            JSONException jSONException = e;
            return users;
        }
    }

    public static List<MCLibUserInfo> getJsonUsers4Search(String jsonStr, int currentPage, int pageSize) {
        if (jsonStr.equals("{\"rs\":0}")) {
            return new ArrayList();
        }
        List<MCLibUserInfo> users = new ArrayList<>();
        try {
            JSONObject result = new JSONObject(jsonStr);
            boolean haveNextPage = result.optBoolean(MCLibMobCentApiConstant.HAVE_NEXT_PAGE);
            String baseUrl = result.optString("baseUrl");
            JSONArray jsonArray = result.optJSONArray("list");
            if (jsonArray == null) {
                return new ArrayList();
            }
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.optJSONObject(i);
                int totalNum = 0;
                if (haveNextPage) {
                    totalNum = (currentPage + 1) * pageSize;
                }
                users.add(getJsonUser(obj, currentPage, totalNum, baseUrl));
            }
            return users;
        } catch (JSONException e) {
            JSONException jSONException = e;
            return users;
        }
    }

    /* JADX INFO: Multiple debug info for r19v1 boolean: [D('obj' org.json.JSONObject), D('isBlocked' boolean)] */
    public static MCLibUserInfo getJsonUser(JSONObject obj, int currentPage, int totalNum, String baseUrl) {
        MCLibUserInfo user = new MCLibUserInfo();
        String userName = obj.optString("name");
        int uid = obj.optInt("id");
        int userGender = obj.optInt(MCLibMobCentApiConstant.USER_GENDER_INT);
        String userAge = obj.optString(MCLibMobCentApiConstant.USER_AGE);
        String userImage = obj.optString("icon");
        String userCity = obj.optString(MCLibMobCentApiConstant.USER_CITY);
        boolean isFollowed = obj.optBoolean(MCLibMobCentApiConstant.IS_FOLLOWED);
        boolean isOnline = obj.optBoolean(MCLibMobCentApiConstant.IS_ONLINE);
        int fansNum = obj.optInt(MCLibMobCentApiConstant.FANS_NUM);
        String emotionWords = obj.optString(MCLibMobCentApiConstant.EMOTION_WORDS);
        boolean isFriend = obj.optBoolean(MCLibMobCentApiConstant.IS_FRIEND);
        String sourceName = obj.optString("sourceName");
        String sourceUrl = obj.optString("sourceUrl");
        int sourceProId = obj.optInt(MCLibMobCentApiConstant.SOURCE_PRO_ID);
        int sourceCategoryId = obj.optInt("sourceCategoryId");
        boolean isBlocked = obj.optBoolean(MCLibMobCentApiConstant.IS_BLOCK);
        user.setTotalNum(totalNum);
        user.setAge(userAge);
        user.setFetchMore(false);
        user.setGender(new StringBuilder(String.valueOf(userGender)).toString());
        user.setUid(uid);
        user.setName(userName);
        user.setNickName(userName);
        user.setImage(String.valueOf(baseUrl) + userImage);
        user.setPos(userCity);
        user.setCurrentPage(currentPage);
        user.setFollowed(isFollowed);
        user.setBlocked(isBlocked);
        user.setOnline(isOnline);
        user.setFansNum(fansNum);
        user.setEmotionWords(emotionWords);
        user.setFriend(isFriend);
        user.setSourceName(sourceName);
        user.setSourceProId(sourceProId);
        user.setSourceUrl(sourceUrl);
        user.setSourceCategoryId(sourceCategoryId);
        return user;
    }

    public static MCLibUserInfo getJsonUser(String jsonStr) {
        try {
            JSONObject result = new JSONObject(jsonStr);
            int userId = result.optInt("userId");
            String nickName = result.optString("nickName");
            String birthday = result.optString(MCLibMobCentApiConstant.USER_BIRTHDAY);
            int gender = result.optInt(MCLibMobCentApiConstant.USER_GENDER);
            String icon = result.optString("icon");
            String city = result.optString(MCLibMobCentApiConstant.USER_CITY);
            String email = result.optString("email");
            MCLibUserInfo userInfo = new MCLibUserInfo();
            userInfo.setUid(userId);
            userInfo.setNickName(nickName);
            userInfo.setBirthday(birthday);
            userInfo.setGender(new StringBuilder(String.valueOf(gender)).toString());
            userInfo.setImage(icon);
            userInfo.setPos(city);
            userInfo.setEmail(email);
            return userInfo;
        } catch (JSONException e) {
            return null;
        }
    }

    /* JADX INFO: Multiple debug info for r12v1 int: [D('sourceCategoryId' int), D('result' org.json.JSONObject)] */
    public static MCLibUserInfo getJsonUserHomeInfo(String jsonStr) {
        try {
            JSONObject jSONObject = new JSONObject(jsonStr);
            String age = jSONObject.optString(MCLibMobCentApiConstant.USER_AGE);
            String baseUrl = jSONObject.optString("baseUrl");
            int userFocusNum = jSONObject.optInt(MCLibMobCentApiConstant.USER_FOCUS_NUM);
            int userFans = jSONObject.optInt(MCLibMobCentApiConstant.USER_FANS_NUM);
            String icon = jSONObject.optString("icon");
            int userId = jSONObject.optInt("userId");
            String pos = jSONObject.optString(MCLibMobCentApiConstant.USER_CITY);
            int gender = jSONObject.optInt(MCLibMobCentApiConstant.USER_GENDER_INT);
            String nickName = jSONObject.optString("userName");
            String userAccount = jSONObject.optString("userNo");
            boolean isOnline = jSONObject.optBoolean(MCLibMobCentApiConstant.IS_ONLINE);
            int statusNum = jSONObject.optInt(MCLibMobCentApiConstant.USER_STATUS_COUNT);
            int eventNum = jSONObject.optInt(MCLibMobCentApiConstant.USER_EVENT_COUNT);
            String mood = jSONObject.optString(MCLibMobCentApiConstant.EMOTION_WORDS);
            boolean isFollowed = jSONObject.optBoolean(MCLibMobCentApiConstant.IS_FOLLOWED);
            boolean isBlocked = jSONObject.optBoolean(MCLibMobCentApiConstant.IS_BLOCK);
            String sourceName = jSONObject.optString("sourceName");
            String sourceUrl = jSONObject.optString("sourceUrl");
            int sourceProId = jSONObject.optInt(MCLibMobCentApiConstant.SOURCE_PRO_ID);
            int sourceCategoryId = jSONObject.optInt("sourceCategoryId");
            MCLibUserInfo userInfo = new MCLibUserInfo();
            userInfo.setAge(age);
            userInfo.setFansNum(userFans);
            userInfo.setFocusNum(userFocusNum);
            userInfo.setImage(String.valueOf(baseUrl) + icon);
            userInfo.setUid(userId);
            userInfo.setPos(pos);
            userInfo.setGender(new StringBuilder(String.valueOf(gender)).toString());
            userInfo.setName(userAccount);
            userInfo.setNickName(nickName);
            userInfo.setOnline(isOnline);
            userInfo.setStatusNum(statusNum);
            userInfo.setEventNum(eventNum);
            userInfo.setEmotionWords(mood);
            userInfo.setFollowed(isFollowed);
            userInfo.setBlocked(isBlocked);
            userInfo.setSourceName(sourceName);
            userInfo.setSourceProId(sourceProId);
            userInfo.setSourceUrl(sourceUrl);
            userInfo.setSourceCategoryId(sourceCategoryId);
            return userInfo;
        } catch (JSONException e) {
            return new MCLibUserInfo();
        }
    }

    public static List<MCLibAppStoreAppModel> getApps(JSONArray array) {
        List<MCLibAppStoreAppModel> apps = new ArrayList<>();
        if (array != null) {
            for (int i = 0; i < array.length(); i++) {
                JSONObject obj = array.optJSONObject(i);
                int id = obj.optInt("id");
                int totalNum = obj.optInt(MCLibMobCentApiConstant.TOTAL_NUM);
                String name = obj.optString("name");
                MCLibAppStoreAppModel model = new MCLibAppStoreAppModel();
                model.setAppId(id);
                model.setTotalNum(totalNum);
                model.setAppName(name);
                apps.add(model);
            }
        }
        return apps;
    }

    public static MCLibUserInfo formUploadImageJson(String jsonStr) {
        MCLibUserInfo userInfo = new MCLibUserInfo();
        try {
            JSONObject jsonObj = new JSONObject(jsonStr);
            if (jsonObj.optInt("rs") == 0) {
                userInfo.setErrorMsg(jsonObj.optString("reason"));
            } else {
                userInfo.setPhotoId(jsonObj.optInt("photoId"));
                userInfo.setPhotoPath(jsonObj.optString("photoPath"));
            }
        } catch (JSONException e) {
            userInfo.setErrorMsg("upload_images_fail");
        }
        return userInfo;
    }
}
