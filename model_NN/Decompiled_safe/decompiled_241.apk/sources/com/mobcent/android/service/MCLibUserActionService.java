package com.mobcent.android.service;

import android.content.Context;
import com.mobcent.android.model.MCLibActionModel;
import java.util.List;

public interface MCLibUserActionService {
    MCLibActionModel getActionInDictById(Context context, int i);

    List<MCLibActionModel> getLocalMagicActions(Context context, int i);

    List<MCLibActionModel> getRemoteUpdateMagicActionType(int i, String str);

    boolean isMagicEmotionEnable();

    void updateMagicActionType(Context context, List<MCLibActionModel> list);
}
