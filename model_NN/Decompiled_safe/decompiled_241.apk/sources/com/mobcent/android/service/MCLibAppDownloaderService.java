package com.mobcent.android.service;

import com.mobcent.android.model.MCLibDownloadProfileModel;
import com.mobcent.android.utils.MCLibIOUtil;
import java.util.List;

public interface MCLibAppDownloaderService {
    public static final String LOCAL_POSITION_DIR = (String.valueOf(MCLibIOUtil.FS) + "mobcent" + MCLibIOUtil.FS + "apps" + MCLibIOUtil.FS);
    public static final int REQUEST_START = 0;
    public static final int REQUEST_STOP = 1;
    public static final int STATUS_FINISHED = 2;
    public static final int STATUS_REMOVED = 3;
    public static final int STATUS_UNDERGOING = 1;
    public static final int STATUS_WAITING = 0;

    boolean doCancelDownload();

    void doDownloadApp();

    boolean doDownloadFinish();

    boolean doPauseDownload();

    boolean doRemoveDownload();

    boolean doResumeDownload();

    List<MCLibDownloadProfileModel> getAllDownloadAppList();

    List<MCLibDownloadProfileModel> getDownloadedAppList();

    int getDownloadedSize();

    List<MCLibDownloadProfileModel> getDownloadingAppList();

    List<MCLibDownloadProfileModel> getRemovedAppList();

    List<MCLibDownloadProfileModel> getWaitingAppList();
}
