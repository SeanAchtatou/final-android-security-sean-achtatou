package com.mobcent.android.service.impl;

import android.content.Context;
import com.mobcent.android.api.UserActionRestfulApiRequester;
import com.mobcent.android.db.UserMagicActionDictDBUtil;
import com.mobcent.android.model.MCLibActionModel;
import com.mobcent.android.service.MCLibUserActionService;
import com.mobcent.android.service.impl.helper.UserActionServiceHelper;
import java.util.List;

public class MCLibUserActionServiceImpl implements MCLibUserActionService {
    private Context context;

    public MCLibUserActionServiceImpl(Context context2) {
        this.context = context2;
    }

    public List<MCLibActionModel> getRemoteUpdateMagicActionType(int userId, String time) {
        String jsonStr = UserActionRestfulApiRequester.getUpdateMagicActionType(userId, time, this.context);
        List<MCLibActionModel> models = UserActionServiceHelper.formJsonMagicActionModelList(jsonStr);
        int isActionEnable = UserActionServiceHelper.formJsonIsActionEnable(jsonStr);
        if (isActionEnable == 1) {
            UserMagicActionDictDBUtil.getInstance(this.context).updateActionEnable(true);
        } else if (isActionEnable == 0) {
            UserMagicActionDictDBUtil.getInstance(this.context).updateActionEnable(false);
        }
        return models;
    }

    public List<MCLibActionModel> getLocalMagicActions(Context context2, int limit) {
        return UserMagicActionDictDBUtil.getInstance(context2).getUserMagicActionList(limit);
    }

    public void updateMagicActionType(Context context2, List<MCLibActionModel> actions) {
        if (actions != null) {
            for (int i = 0; i < actions.size(); i++) {
                MCLibActionModel actionModel = actions.get(i);
                UserMagicActionDictDBUtil.getInstance(context2).addUserMagicAction(actionModel);
                UserMagicActionDictDBUtil.getInstance(context2).updateMagicActionTypeTime(actionModel.getTime());
            }
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public MCLibActionModel getActionInDictById(Context context2, int umaId) {
        List<MCLibActionModel> actions = UserMagicActionDictDBUtil.getInstance(context2).getUserMagicActionById(umaId);
        if (actions == null || actions.isEmpty()) {
            return null;
        }
        return actions.get(0);
    }

    public boolean isMagicEmotionEnable() {
        return UserMagicActionDictDBUtil.getInstance(this.context).isActionEnable();
    }
}
