package com.mobcent.android.service.impl;

import java.io.File;
import java.net.URLConnection;

public class MCLibFileDownloadThread extends Thread {
    private static final int BUFFER_SIZE = 1024;
    private URLConnection con;
    private int curPosition;
    private int downloadSize = 0;
    private int endPosition;
    private File file;
    private boolean finished = false;
    private boolean isRemoveRequested = false;
    private boolean isStopRequested = false;
    private int startPosition;

    public MCLibFileDownloadThread(File file2, int startPosition2, int endPosition2, URLConnection conn) {
        this.file = file2;
        this.startPosition = startPosition2;
        this.curPosition = startPosition2;
        this.endPosition = endPosition2;
        this.con = conn;
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x0075 A[SYNTHETIC, Splitter:B:31:0x0075] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x007a A[Catch:{ IOException -> 0x007e }] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x008f A[SYNTHETIC, Splitter:B:42:0x008f] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0094 A[Catch:{ IOException -> 0x00ab }] */
    /* JADX WARNING: Removed duplicated region for block: B:71:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r10 = this;
            r7 = 1024(0x400, float:1.435E-42)
            r0 = 0
            r4 = 0
            byte[] r2 = new byte[r7]
            java.io.RandomAccessFile r5 = new java.io.RandomAccessFile     // Catch:{ IOException -> 0x00be, all -> 0x00b9 }
            java.io.File r7 = r10.file     // Catch:{ IOException -> 0x00be, all -> 0x00b9 }
            java.lang.String r8 = "rw"
            r5.<init>(r7, r8)     // Catch:{ IOException -> 0x00be, all -> 0x00b9 }
            int r7 = r10.startPosition     // Catch:{ IOException -> 0x00c0, all -> 0x00bb }
            long r7 = (long) r7     // Catch:{ IOException -> 0x00c0, all -> 0x00bb }
            r5.seek(r7)     // Catch:{ IOException -> 0x00c0, all -> 0x00bb }
            java.io.BufferedInputStream r1 = new java.io.BufferedInputStream     // Catch:{ IOException -> 0x00c0, all -> 0x00bb }
            java.net.URLConnection r7 = r10.con     // Catch:{ IOException -> 0x00c0, all -> 0x00bb }
            java.io.InputStream r7 = r7.getInputStream()     // Catch:{ IOException -> 0x00c0, all -> 0x00bb }
            r1.<init>(r7)     // Catch:{ IOException -> 0x00c0, all -> 0x00bb }
        L_0x0020:
            int r7 = r10.curPosition     // Catch:{ IOException -> 0x0070, all -> 0x008a }
            int r8 = r10.endPosition     // Catch:{ IOException -> 0x0070, all -> 0x008a }
            if (r7 >= r8) goto L_0x0032
            boolean r7 = r10.isStopRequested()     // Catch:{ IOException -> 0x0070, all -> 0x008a }
            if (r7 != 0) goto L_0x0032
            boolean r7 = r10.isRemoveRequested()     // Catch:{ IOException -> 0x0070, all -> 0x008a }
            if (r7 == 0) goto L_0x0048
        L_0x0032:
            boolean r7 = r10.isStopRequested()     // Catch:{ IOException -> 0x0070, all -> 0x008a }
            if (r7 == 0) goto L_0x0098
            r7 = 0
            r10.finished = r7     // Catch:{ IOException -> 0x0070, all -> 0x008a }
        L_0x003b:
            if (r1 == 0) goto L_0x0040
            r1.close()     // Catch:{ IOException -> 0x00b1 }
        L_0x0040:
            if (r5 == 0) goto L_0x00b6
            r5.close()     // Catch:{ IOException -> 0x00b1 }
            r4 = r5
            r0 = r1
        L_0x0047:
            return
        L_0x0048:
            r7 = 0
            r8 = 1024(0x400, float:1.435E-42)
            int r6 = r1.read(r2, r7, r8)     // Catch:{ IOException -> 0x0070, all -> 0x008a }
            r7 = -1
            if (r6 == r7) goto L_0x0032
            r7 = 0
            r5.write(r2, r7, r6)     // Catch:{ IOException -> 0x0070, all -> 0x008a }
            int r7 = r10.curPosition     // Catch:{ IOException -> 0x0070, all -> 0x008a }
            int r7 = r7 + r6
            r10.curPosition = r7     // Catch:{ IOException -> 0x0070, all -> 0x008a }
            int r7 = r10.curPosition     // Catch:{ IOException -> 0x0070, all -> 0x008a }
            int r8 = r10.endPosition     // Catch:{ IOException -> 0x0070, all -> 0x008a }
            if (r7 <= r8) goto L_0x0084
            int r7 = r10.downloadSize     // Catch:{ IOException -> 0x0070, all -> 0x008a }
            int r8 = r10.curPosition     // Catch:{ IOException -> 0x0070, all -> 0x008a }
            int r9 = r10.endPosition     // Catch:{ IOException -> 0x0070, all -> 0x008a }
            int r8 = r8 - r9
            int r8 = r6 - r8
            int r8 = r8 + 1
            int r7 = r7 + r8
            r10.downloadSize = r7     // Catch:{ IOException -> 0x0070, all -> 0x008a }
            goto L_0x0020
        L_0x0070:
            r7 = move-exception
            r4 = r5
            r0 = r1
        L_0x0073:
            if (r0 == 0) goto L_0x0078
            r0.close()     // Catch:{ IOException -> 0x007e }
        L_0x0078:
            if (r4 == 0) goto L_0x0047
            r4.close()     // Catch:{ IOException -> 0x007e }
            goto L_0x0047
        L_0x007e:
            r7 = move-exception
            r3 = r7
            r3.printStackTrace()
            goto L_0x0047
        L_0x0084:
            int r7 = r10.downloadSize     // Catch:{ IOException -> 0x0070, all -> 0x008a }
            int r7 = r7 + r6
            r10.downloadSize = r7     // Catch:{ IOException -> 0x0070, all -> 0x008a }
            goto L_0x0020
        L_0x008a:
            r7 = move-exception
            r4 = r5
            r0 = r1
        L_0x008d:
            if (r0 == 0) goto L_0x0092
            r0.close()     // Catch:{ IOException -> 0x00ab }
        L_0x0092:
            if (r4 == 0) goto L_0x0097
            r4.close()     // Catch:{ IOException -> 0x00ab }
        L_0x0097:
            throw r7
        L_0x0098:
            boolean r7 = r10.isRemoveRequested()     // Catch:{ IOException -> 0x0070, all -> 0x008a }
            if (r7 == 0) goto L_0x00a7
            java.io.File r7 = r10.file     // Catch:{ IOException -> 0x0070, all -> 0x008a }
            r7.delete()     // Catch:{ IOException -> 0x0070, all -> 0x008a }
            r7 = 0
            r10.finished = r7     // Catch:{ IOException -> 0x0070, all -> 0x008a }
            goto L_0x003b
        L_0x00a7:
            r7 = 1
            r10.finished = r7     // Catch:{ IOException -> 0x0070, all -> 0x008a }
            goto L_0x003b
        L_0x00ab:
            r8 = move-exception
            r3 = r8
            r3.printStackTrace()
            goto L_0x0097
        L_0x00b1:
            r7 = move-exception
            r3 = r7
            r3.printStackTrace()
        L_0x00b6:
            r4 = r5
            r0 = r1
            goto L_0x0047
        L_0x00b9:
            r7 = move-exception
            goto L_0x008d
        L_0x00bb:
            r7 = move-exception
            r4 = r5
            goto L_0x008d
        L_0x00be:
            r7 = move-exception
            goto L_0x0073
        L_0x00c0:
            r7 = move-exception
            r4 = r5
            goto L_0x0073
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobcent.android.service.impl.MCLibFileDownloadThread.run():void");
    }

    public boolean isFinished() {
        return this.finished;
    }

    public int getDownloadSize() {
        return this.downloadSize;
    }

    public boolean isStopRequested() {
        return this.isStopRequested;
    }

    public void setStopRequested(boolean isStopRequested2) {
        this.isStopRequested = isStopRequested2;
    }

    public boolean isRemoveRequested() {
        return this.isRemoveRequested;
    }

    public void setRemoveRequested(boolean isRemoveRequested2) {
        this.isRemoveRequested = isRemoveRequested2;
    }
}
