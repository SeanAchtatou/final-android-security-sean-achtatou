package com.mobcent.android.service.impl;

import android.content.Context;
import com.mobcent.android.api.MCShareSyncSiteRestfulApiRequester;
import com.mobcent.android.model.MCShareSyncSiteModel;
import com.mobcent.android.service.MCShareSyncSiteService;
import com.mobcent.android.service.impl.helper.MCShareBaseJsonHelper;
import com.mobcent.android.service.impl.helper.MCShareSyncSiteServiceHelper;
import java.util.List;

public class MCShareSyncSiteServiceImpl implements MCShareSyncSiteService {
    private Context context;
    private int userId;

    public MCShareSyncSiteServiceImpl(int userId2, Context context2) {
        this.context = context2;
        this.userId = userId2;
    }

    public List<MCShareSyncSiteModel> getAllSyncSites(String appKey) {
        return MCShareSyncSiteServiceHelper.formSyncSiteModels(MCShareSyncSiteRestfulApiRequester.getAllSyncSites(this.userId, appKey, this.context));
    }

    public String shareApp(String content, String picPath, String ids, String appKey) {
        return MCShareBaseJsonHelper.formJsonRS(MCShareSyncSiteRestfulApiRequester.shareApp(this.userId, content, picPath, ids, appKey, this.context));
    }
}
