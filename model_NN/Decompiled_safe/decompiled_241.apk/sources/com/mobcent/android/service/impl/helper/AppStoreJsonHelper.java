package com.mobcent.android.service.impl.helper;

import com.mobcent.android.constants.MCLibMobCentApiConstant;
import com.mobcent.android.model.MCLibAppStoreAppModel;
import org.json.JSONException;
import org.json.JSONObject;

public class AppStoreJsonHelper extends BaseJsonHelper implements MCLibMobCentApiConstant {
    /* JADX INFO: Multiple debug info for r5v1 int: [D('downloadTimes' int), D('jsonObj' org.json.JSONObject)] */
    public static MCLibAppStoreAppModel formJsonAppModel(String jsonStr, int appId) {
        MCLibAppStoreAppModel appStoreAppModel = new MCLibAppStoreAppModel();
        try {
            JSONObject jsonObj = new JSONObject(jsonStr);
            int appId2 = jsonObj.optInt("id");
            String baseUrl = jsonObj.optString("baseUrl");
            String description = jsonObj.optString(MCLibMobCentApiConstant.DESCRIPTION);
            String developerName = jsonObj.optString(MCLibMobCentApiConstant.DEVELOPER_NAME);
            String downloadPath = jsonObj.optString("downloadPath");
            String language = jsonObj.optString(MCLibMobCentApiConstant.LANGUAGE);
            String lastUpdateTime = jsonObj.optString(MCLibMobCentApiConstant.LAST_UPDATE_TIME);
            String version = jsonObj.optString(MCLibMobCentApiConstant.VERSION);
            String size = jsonObj.optString(MCLibMobCentApiConstant.SIZE);
            String fee = jsonObj.optString(MCLibMobCentApiConstant.FEE);
            String name = jsonObj.optString("name");
            String imageUrl = jsonObj.optString("picPath");
            String categoryName = jsonObj.optString(MCLibMobCentApiConstant.CATEGORY_NAME);
            int downloadTimes = jsonObj.optInt(MCLibMobCentApiConstant.DOWNLOAD_TIMES);
            appStoreAppModel.setAppId(appId2);
            appStoreAppModel.setAppImageUrl(imageUrl);
            appStoreAppModel.setAppName(name);
            appStoreAppModel.setCategory(categoryName);
            appStoreAppModel.setFee(fee);
            appStoreAppModel.setDescription(description);
            appStoreAppModel.setDeveloperName(developerName);
            appStoreAppModel.setLanguage(language);
            appStoreAppModel.setBaseUrl(baseUrl);
            appStoreAppModel.setDownloadPath(String.valueOf(baseUrl) + downloadPath);
            appStoreAppModel.setVersion(version);
            appStoreAppModel.setSize(size);
            appStoreAppModel.setPublishTime(lastUpdateTime);
            appStoreAppModel.setDownloadTimes(new StringBuilder(String.valueOf(downloadTimes)).toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return appStoreAppModel;
    }
}
