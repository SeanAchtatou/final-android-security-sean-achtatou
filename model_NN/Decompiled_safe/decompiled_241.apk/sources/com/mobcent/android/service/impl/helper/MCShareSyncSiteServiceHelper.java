package com.mobcent.android.service.impl.helper;

import com.mobcent.android.constants.MCShareMobCentApiConstant;
import com.mobcent.android.model.MCShareSyncSiteModel;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MCShareSyncSiteServiceHelper extends MCShareBaseJsonHelper implements MCShareMobCentApiConstant {
    public static List<MCShareSyncSiteModel> formSyncSiteModels(String jsonStr) {
        List<MCShareSyncSiteModel> models = new ArrayList<>();
        try {
            JSONArray array = new JSONObject(jsonStr).optJSONArray("list");
            if (array != null) {
                for (int i = 0; i < array.length(); i++) {
                    MCShareSyncSiteModel model = new MCShareSyncSiteModel();
                    JSONObject siteObj = array.getJSONObject(i);
                    int siteId = siteObj.optInt("mark");
                    String name = siteObj.optString("name");
                    model.setSiteId(siteId);
                    model.setSiteName(name);
                    models.add(model);
                }
            }
        } catch (JSONException e) {
        }
        return models;
    }
}
