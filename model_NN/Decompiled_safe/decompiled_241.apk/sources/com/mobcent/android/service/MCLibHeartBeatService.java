package com.mobcent.android.service;

import android.content.Context;
import com.mobcent.android.model.MCLibHeartBeatModel;

public interface MCLibHeartBeatService {
    MCLibHeartBeatModel updateHeartBeatInfo(Context context, int i, boolean z);
}
