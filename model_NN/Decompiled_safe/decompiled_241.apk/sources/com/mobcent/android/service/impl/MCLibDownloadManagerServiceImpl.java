package com.mobcent.android.service.impl;

import android.content.Context;
import com.mobcent.android.db.DownloadAppDBUtil;
import com.mobcent.android.model.MCLibDownloadProfileModel;
import com.mobcent.android.service.MCLibDownloadManagerService;

public class MCLibDownloadManagerServiceImpl implements MCLibDownloadManagerService {
    private Context context;

    public MCLibDownloadManagerServiceImpl(Context context2) {
        this.context = context2;
    }

    public MCLibDownloadProfileModel getDownloadProfileModel(int appId) {
        return DownloadAppDBUtil.getInstance(this.context).getDownloadProfile(appId);
    }

    public void addOrUpdateDownloadProfile(MCLibDownloadProfileModel model, boolean isNeedUpdate) {
        DownloadAppDBUtil.getInstance(this.context).addOrUpdateAppDownloadProfile(model, isNeedUpdate);
    }
}
