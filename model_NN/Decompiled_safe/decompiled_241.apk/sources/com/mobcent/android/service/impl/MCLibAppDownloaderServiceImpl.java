package com.mobcent.android.service.impl;

import android.content.Context;
import android.util.Log;
import com.mobcent.android.constants.MCLibConstants;
import com.mobcent.android.constants.MCLibMobCentApiConstant;
import com.mobcent.android.db.DownloadAppDBUtil;
import com.mobcent.android.db.UserMagicActionDictDBUtil;
import com.mobcent.android.model.MCLibDownloadProfileModel;
import com.mobcent.android.service.MCLibAppDownloaderService;
import com.mobcent.android.state.MCLibAppState;
import com.mobcent.android.utils.MCLibIOUtil;
import com.mobcent.android.utils.PhoneConnectionUtil;
import java.io.File;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

public class MCLibAppDownloaderServiceImpl implements Runnable, MCLibAppDownloaderService, MCLibMobCentApiConstant {
    private Context context;
    private MCLibDownloadProfileModel downloadModel;
    private Thread downloadThread;
    private int downloadedSize;
    private File file;
    private MCLibFileDownloadThread fileDownloadThread;
    private String fileName;
    private int fileSize;
    private boolean pauseRequested = false;
    private boolean removeRequested = false;
    private String urlStr;

    public MCLibAppDownloaderServiceImpl(Context context2) {
        this.context = context2;
    }

    public MCLibAppDownloaderServiceImpl(MCLibDownloadProfileModel downloadModel2, Context context2) {
        this.urlStr = downloadModel2.getUrl();
        this.context = context2;
        this.downloadModel = downloadModel2;
        this.downloadedSize = downloadModel2.getDownloadSize();
        this.fileName = getAppFileLocation(this.urlStr, context2);
    }

    public boolean doCancelDownload() {
        doPauseDownload();
        doRemoveDownload();
        return true;
    }

    public void doDownloadApp() {
        DownloadAppDBUtil util = DownloadAppDBUtil.getInstance(this.context);
        this.downloadModel.setStopReq(0);
        this.downloadModel.setStatus(1);
        try {
            util.addOrUpdateAppDownloadProfile(this.downloadModel, true);
        } catch (Exception e) {
        }
        this.downloadThread = new Thread(this);
        this.downloadThread.start();
    }

    public boolean doDownloadFinish() {
        DownloadAppDBUtil util = DownloadAppDBUtil.getInstance(this.context);
        this.downloadModel.setStopReq(0);
        this.downloadModel.setStatus(2);
        try {
            util.addOrUpdateAppDownloadProfile(this.downloadModel, true);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean doPauseDownload() {
        this.pauseRequested = true;
        if (!(this.downloadThread == null || this.fileDownloadThread == null)) {
            this.fileDownloadThread.setStopRequested(true);
            DownloadAppDBUtil util = DownloadAppDBUtil.getInstance(this.context);
            this.downloadModel.setStatus(0);
            this.downloadModel.setStopReq(1);
            try {
                util.addOrUpdateAppDownloadProfile(this.downloadModel, true);
            } catch (Exception e) {
                return false;
            }
        }
        return true;
    }

    public boolean isPauseRequest() {
        List<MCLibDownloadProfileModel> models = DownloadAppDBUtil.getInstance(this.context).getDownloadProfileByStatus(1);
        if (models.isEmpty()) {
            return false;
        }
        Log.i("undergoing models", new StringBuilder().append(models.size()).toString());
        if (models.get(0).getStopReq() == 1) {
            return true;
        }
        return false;
    }

    public boolean isRemoveRequest(int appId) {
        MCLibDownloadProfileModel model = DownloadAppDBUtil.getInstance(this.context).getDownloadProfile(appId);
        if (model == null) {
            return false;
        }
        if (model.getStatus() == 3) {
            return true;
        }
        return false;
    }

    public boolean doRemoveDownload() {
        this.removeRequested = true;
        this.fileDownloadThread.setRemoveRequested(true);
        return true;
    }

    public boolean doResumeDownload() {
        DownloadAppDBUtil util = DownloadAppDBUtil.getInstance(this.context);
        this.downloadModel.setStopReq(0);
        this.downloadModel.setStatus(0);
        try {
            util.addOrUpdateAppDownloadProfile(this.downloadModel, true);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public List<MCLibDownloadProfileModel> getDownloadedAppList() {
        return DownloadAppDBUtil.getInstance(this.context).getDownloadProfileByStatus(2);
    }

    public List<MCLibDownloadProfileModel> getWaitingAppList() {
        return DownloadAppDBUtil.getInstance(this.context).getDownloadProfileByStatus(0);
    }

    public List<MCLibDownloadProfileModel> getDownloadingAppList() {
        return DownloadAppDBUtil.getInstance(this.context).getDownloadProfileByStatus(1);
    }

    public List<MCLibDownloadProfileModel> getRemovedAppList() {
        return DownloadAppDBUtil.getInstance(this.context).getDownloadProfileByStatus(3);
    }

    public List<MCLibDownloadProfileModel> getAllDownloadAppList() {
        return DownloadAppDBUtil.getInstance(this.context).getAllDownloadProfile();
    }

    public void run() {
        URLConnection conn;
        try {
            int alreadyDownloadSize = getDownloadedSize();
            initAppDir(this.downloadModel.getAppType(), this.downloadModel.getAppId());
            URL url = new URL(String.valueOf(this.urlStr) + "&" + "userId" + "=" + new MCLibUserInfoServiceImpl(this.context).getLoginUserId() + "&" + MCLibMobCentApiConstant.TARGET_ID + "=" + this.downloadModel.getAppId() + "&" + "platType" + "=" + MCLibConstants.ANDROID + "&" + MCLibMobCentApiConstant.SESSION_ID + "=" + MCLibAppState.sessionId + "&" + "packageName" + "=" + this.context.getPackageName() + "&" + "appKey" + "=" + UserMagicActionDictDBUtil.getInstance(this.context).getAppKey() + "&" + "gzip" + "=false");
            if (PhoneConnectionUtil.isCmwap(this.context) || PhoneConnectionUtil.isUniwap(this.context)) {
                conn = (HttpURLConnection) url.openConnection(new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.0.0.172", 80)));
            } else {
                conn = (HttpURLConnection) url.openConnection();
            }
            conn.setAllowUserInteraction(true);
            conn.setRequestProperty("Range", "bytes=" + getDownloadedSize() + "-");
            this.fileSize = conn.getContentLength();
            this.downloadModel.setFileSize(this.fileSize + alreadyDownloadSize);
            this.file = new File(this.fileName);
            int endPosition = (this.fileSize + alreadyDownloadSize) - 1;
            if (conn.getHeaderFieldInt("rs", MCLibAppState.APP_RUNNING) != MCLibAppState.APP_RUNNING) {
                DownloadAppDBUtil util = DownloadAppDBUtil.getInstance(this.context);
                this.downloadModel.setStopReq(1);
                util.addOrUpdateAppDownloadProfile(this.downloadModel, true);
                return;
            }
            this.fileDownloadThread = new MCLibFileDownloadThread(this.file, getDownloadedSize(), endPosition, conn);
            this.fileDownloadThread.setName("Thread-" + this.fileName);
            this.fileDownloadThread.start();
            boolean finished = false;
            while (!finished && !this.pauseRequested && !this.removeRequested) {
                finished = true;
                this.downloadedSize = this.fileDownloadThread.getDownloadSize();
                if (!this.fileDownloadThread.isFinished()) {
                    finished = false;
                }
                this.downloadModel.setDownloadSize(this.downloadedSize + alreadyDownloadSize);
                this.downloadModel.setSpeed(this.fileDownloadThread.getDownloadSize());
                if (isPauseRequest()) {
                    Log.i("Pause", "paused");
                    doPauseDownload();
                } else if (isRemoveRequest(this.downloadModel.getAppId())) {
                    doRemoveDownload();
                } else {
                    Log.i("update", "update");
                    updateDownloadStatus();
                }
                Thread.sleep(1000);
            }
            if (finished) {
                Log.i("dofinish", "finish download");
                doDownloadFinish();
            }
        } catch (Exception e) {
            Log.i("Download", "Exception, Download file in download task manager");
        }
    }

    private void updateDownloadStatus() {
        DownloadAppDBUtil util = DownloadAppDBUtil.getInstance(this.context);
        this.downloadModel.setStatus(1);
        util.addOrUpdateAppDownloadProfile(this.downloadModel, true);
    }

    private void initAppDir(int type, int bookId) {
        File file2 = new File(getAppPath(this.context));
        if (!file2.exists()) {
            file2.mkdirs();
        }
    }

    public int getDownloadedSize() {
        return this.downloadedSize;
    }

    public static String getAppPath(Context context2) {
        return String.valueOf(MCLibIOUtil.getBaseLocalLocation(context2)) + LOCAL_POSITION_DIR;
    }

    public static String getAppFileLocation(String url, Context context2) {
        String appPath = getAppPath(context2);
        return String.valueOf(appPath) + MCLibIOUtil.FS + url.substring(url.lastIndexOf("/") + 1);
    }
}
