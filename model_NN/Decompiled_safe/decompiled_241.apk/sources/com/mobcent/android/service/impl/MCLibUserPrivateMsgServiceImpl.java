package com.mobcent.android.service.impl;

import android.content.Context;
import com.mobcent.android.api.UserActionRestfulApiRequester;
import com.mobcent.android.db.PrivateMsgDBUtil;
import com.mobcent.android.db.QuickMsgDBUtil;
import com.mobcent.android.db.RecentChatUsersDBUtil;
import com.mobcent.android.db.SharedUserDBUtil;
import com.mobcent.android.model.MCLibUserInfo;
import com.mobcent.android.model.MCLibUserStatus;
import com.mobcent.android.service.MCLibUserPrivateMsgService;
import com.mobcent.android.service.impl.helper.BaseJsonHelper;
import com.mobcent.android.service.impl.helper.UserActionServiceHelper;
import java.util.ArrayList;
import java.util.List;

public class MCLibUserPrivateMsgServiceImpl implements MCLibUserPrivateMsgService {
    private Context context;

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: Method info already added: com.mobcent.android.service.impl.helper.UserActionServiceHelper.formJsonRS(java.lang.String):java.lang.String in method: com.mobcent.android.service.impl.MCLibUserPrivateMsgServiceImpl.removeMagicActions(int, java.lang.String):java.lang.String, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at java.base/java.util.ArrayList.forEach(ArrayList.java:1540)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:59)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Method info already added: com.mobcent.android.service.impl.helper.UserActionServiceHelper.formJsonRS(java.lang.String):java.lang.String
        	at jadx.core.dex.info.InfoStorage.putMethod(InfoStorage.java:42)
        	at jadx.core.dex.info.MethodInfo.fromDex(MethodInfo.java:50)
        	at jadx.core.dex.instructions.InsnDecoder.invoke(InsnDecoder.java:678)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:528)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 6 more
        */
    public java.lang.String removeMagicActions(int r1, java.lang.String r2) {
        /*
            r2 = this;
            android.content.Context r1 = r2.context
            java.lang.String r0 = com.mobcent.android.api.UserActionRestfulApiRequester.removeMagicAction(r3, r4, r1)
            java.lang.String r1 = com.mobcent.android.service.impl.helper.UserActionServiceHelper.formJsonRS(r0)
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobcent.android.service.impl.MCLibUserPrivateMsgServiceImpl.removeMagicActions(int, java.lang.String):java.lang.String");
    }

    public MCLibUserPrivateMsgServiceImpl(Context context2) {
        this.context = context2;
    }

    public String addReceivedPrivateMsgs(int loginUid, List<MCLibUserStatus> userStatusList) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < userStatusList.size(); i++) {
            MCLibUserStatus msg = userStatusList.get(i);
            msg.setBelongUid(msg.getFromUserId());
            msg.setIsRead(1);
            PrivateMsgDBUtil.getInstance(this.context).addOrUpdatePrivateMsg(loginUid, msg, (long) i);
            sb.append(msg.getStatusId()).append(",");
        }
        return sb.toString();
    }

    public boolean addRepliedPrivateMsg(int loginUid, int belongUid, MCLibUserStatus userStatus) {
        userStatus.setBelongUid(belongUid);
        userStatus.setIsRead(0);
        return PrivateMsgDBUtil.getInstance(this.context).addOrUpdatePrivateMsg(loginUid, userStatus, 0);
    }

    public List<MCLibUserStatus> getUserHistoryPrivateMsg(int loginUid, int belongUid, int page, int pageSize) {
        int totalNum;
        List<MCLibUserStatus> statusList = PrivateMsgDBUtil.getInstance(this.context).getPrivateMsgs(loginUid, belongUid, page, pageSize);
        for (MCLibUserStatus userStatus : statusList) {
            if (userStatus.getIsRead() == 1) {
                PrivateMsgDBUtil.getInstance(this.context).updateUserPrivateMsgRead(userStatus, loginUid);
            }
        }
        RecentChatUsersDBUtil.getInstance(this.context).updateRecentChatUserMsgRead(RecentChatUsersDBUtil.getInstance(this.context).getRecentChatUser(belongUid));
        if (statusList != null && statusList.size() > 0 && (totalNum = statusList.get(0).getTotalNum()) > statusList.size() + ((page - 1) * pageSize) && totalNum > pageSize) {
            MCLibUserStatus fetchMoreModel = new MCLibUserStatus();
            fetchMoreModel.setFetchMore(true);
            fetchMoreModel.setCurrentPage(page);
            statusList.add(fetchMoreModel);
        }
        return statusList;
    }

    public List<MCLibUserStatus> getUserUnreadPrivateMsg(int loginUid, int belongUid) {
        List<MCLibUserStatus> statusList = PrivateMsgDBUtil.getInstance(this.context).getUserUnreadMsgs(loginUid, belongUid);
        for (MCLibUserStatus userStatus : statusList) {
            if (userStatus.getIsRead() == 1) {
                PrivateMsgDBUtil.getInstance(this.context).updateUserPrivateMsgRead(userStatus, loginUid);
            }
        }
        RecentChatUsersDBUtil.getInstance(this.context).updateRecentChatUserMsgRead(RecentChatUsersDBUtil.getInstance(this.context).getRecentChatUser(belongUid));
        if (statusList == null) {
            return new ArrayList();
        }
        return statusList;
    }

    public boolean removeAllMsg(int loginUid, int belongUid) {
        return PrivateMsgDBUtil.getInstance(this.context).removeAllUserMsg(loginUid, belongUid);
    }

    public boolean removeMsg(int mid) {
        return PrivateMsgDBUtil.getInstance(this.context).removeUserMsg(mid);
    }

    public int getUserUnreadMsgCount(Context context2, int loginUid) {
        return PrivateMsgDBUtil.getInstance(context2).getUserUnreadMsgCount(loginUid);
    }

    public int getUserUnreadActionCount(Context context2, int loginUid) {
        return PrivateMsgDBUtil.getInstance(context2).getUserUnreadActionCount(loginUid);
    }

    public String sendMagicAction(Context context2, int userId, int targetId, String content, int actionId, String toUserName) {
        String jsonStr = UserActionRestfulApiRequester.sendMagicAction(userId, targetId, actionId, context2);
        String rs = UserActionServiceHelper.formJsonRS(jsonStr);
        if (rs != null && rs.equals("rs_succ")) {
            long id = BaseJsonHelper.getJsonId(jsonStr);
            MCLibUserInfo user = SharedUserDBUtil.getInstance(context2).getCurrentUserInfo();
            String name = user.getNickName();
            if (name == null || name.trim().equals("")) {
                name = user.getName();
            }
            String photo = user.getImage();
            MCLibUserStatus userStatus = new MCLibUserStatus();
            userStatus.setStatusId(id);
            userStatus.setContent(content);
            userStatus.setFromUserId(userId);
            userStatus.setFromUserName(name);
            userStatus.setBelongUid(targetId);
            userStatus.setUserImageUrl(photo);
            userStatus.setActionId(actionId);
            new MCLibUserPrivateMsgServiceImpl(context2).addRepliedPrivateMsg(userId, targetId, userStatus);
        }
        return rs;
    }

    public List<String> getAllUserCustomizedQuickMsgs(int loginUid) {
        return QuickMsgDBUtil.getInstance(this.context).getAllQuickMsgs(loginUid);
    }

    public boolean addUserCustomizedQuickMsg(int loginUid, String msg) {
        return QuickMsgDBUtil.getInstance(this.context).addQuickMsg(loginUid, msg);
    }

    public boolean removeUserCustomizedQuickMsg(int loginUid, String msg) {
        return QuickMsgDBUtil.getInstance(this.context).removeQuickMsg(loginUid, msg);
    }
}
