package com.mobcent.android.service;

import android.graphics.Bitmap;

public interface MCLibFileTransferService {
    Bitmap getBitmapFromUrl(String str);

    byte[] getImageStream(String str);
}
