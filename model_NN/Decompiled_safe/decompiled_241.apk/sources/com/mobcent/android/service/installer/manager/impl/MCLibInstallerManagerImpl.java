package com.mobcent.android.service.installer.manager.impl;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import com.mobcent.android.model.MCLibAppStoreAppModel;
import com.mobcent.android.service.installer.manager.MCLibInstallerManager;
import java.io.File;

public class MCLibInstallerManagerImpl implements MCLibInstallerManager {
    private Context context;

    public MCLibInstallerManagerImpl(Context context2) {
        this.context = context2;
    }

    public MCLibAppStoreAppModel doCheckPkgStatus(MCLibAppStoreAppModel appModel) {
        return null;
    }

    public void doInstallPkg(File file) {
        if (file != null && file.getName().toLowerCase().endsWith(".apk")) {
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
            this.context.startActivity(intent);
        }
    }

    public void doUninstallPkg(MCLibAppStoreAppModel appModel) {
    }

    public boolean isPackageInstalled(String packageName) {
        PackageManager manager = this.context.getPackageManager();
        Intent mainIntent = new Intent("android.intent.action.MAIN", (Uri) null);
        mainIntent.addCategory("android.intent.category.LAUNCHER");
        for (ResolveInfo resolveInfo : manager.queryIntentActivities(mainIntent, 0)) {
            if (resolveInfo.activityInfo.packageName.equals(packageName)) {
                return true;
            }
        }
        return false;
    }
}
