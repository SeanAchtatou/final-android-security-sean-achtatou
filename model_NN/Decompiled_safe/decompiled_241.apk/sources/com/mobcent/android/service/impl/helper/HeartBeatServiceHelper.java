package com.mobcent.android.service.impl.helper;

import com.mobcent.android.constants.MCLibMobCentApiConstant;
import com.mobcent.android.model.MCLibHeartBeatModel;
import com.mobcent.android.model.MCLibSysMsgModel;
import com.mobcent.android.model.MCLibUserStatus;
import com.mobcent.android.state.MCLibAppState;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class HeartBeatServiceHelper extends BaseJsonHelper implements MCLibMobCentApiConstant {
    public static MCLibHeartBeatModel formHeatBeatModel(String jsonStr) {
        MCLibHeartBeatModel model = new MCLibHeartBeatModel();
        try {
            JSONObject jsonObj = new JSONObject(jsonStr);
            String baseUrl = jsonObj.optString("baseUrl");
            int interval = jsonObj.optInt(MCLibMobCentApiConstant.INTERVAL);
            int requestAppState = jsonObj.optInt("rs", MCLibAppState.APP_RUNNING);
            boolean hasReply = jsonObj.optBoolean(MCLibMobCentApiConstant.HAS_REPLY);
            model.setInterval(interval);
            model.setRequestAppState(requestAppState);
            model.setHasReply(hasReply);
            JSONArray msgArray = jsonObj.optJSONArray(MCLibMobCentApiConstant.MSG_LIST);
            JSONArray actionArray = jsonObj.optJSONArray(MCLibMobCentApiConstant.ACTION_LIST);
            JSONArray sysNoticeList = jsonObj.optJSONArray(MCLibMobCentApiConstant.SYS_NOTICE_LIST);
            model.setMsgList(getMsgList(msgArray, baseUrl));
            model.setActionList(getActionList(actionArray, baseUrl));
            model.setSysMsgList(getSysMsgList(sysNoticeList));
        } catch (JSONException e) {
        }
        return model;
    }

    /* JADX INFO: Multiple debug info for r14v2 java.util.List<com.mobcent.android.model.MCLibUserStatus>: [D('list' java.util.List<com.mobcent.android.model.MCLibUserStatus>), D('e' org.json.JSONException)] */
    /* JADX INFO: Multiple debug info for r14v3 java.util.List<com.mobcent.android.model.MCLibUserStatus>: [D('list' java.util.List<com.mobcent.android.model.MCLibUserStatus>), D('array' org.json.JSONArray)] */
    /* JADX INFO: Multiple debug info for r9v2 int: [D('sourceProId' int), D('object' org.json.JSONObject)] */
    /* JADX INFO: Multiple debug info for r14v4 java.util.List<com.mobcent.android.model.MCLibUserStatus>: [D('list' java.util.List<com.mobcent.android.model.MCLibUserStatus>), D('array' org.json.JSONArray)] */
    public static List<MCLibUserStatus> getMsgList(JSONArray array, String baseUrl) {
        List<MCLibUserStatus> list = new ArrayList<>();
        if (array == null) {
            return list;
        }
        int i = 0;
        while (true) {
            try {
                int i2 = i;
                if (i2 >= array.length()) {
                    return list;
                }
                MCLibUserStatus model = new MCLibUserStatus();
                JSONObject object = (JSONObject) array.get(i2);
                long id = object.optLong("id");
                String content = object.optString("content");
                int fromUserId = object.optInt("fromUserId");
                String fromUserName = object.optString("fromUserName");
                String time = object.optString("time");
                String icon = object.optString("icon");
                String sourceName = object.optString("sourceName");
                String sourceUrl = object.optString("sourceUrl");
                int sourceProId = object.optInt(MCLibMobCentApiConstant.SOURCE_PRO_ID);
                model.setStatusId(id);
                model.setContent(content);
                model.setFromUserId(fromUserId);
                model.setFromUserName(fromUserName);
                model.setTime(time);
                model.setUserImageUrl(String.valueOf(baseUrl) + icon);
                model.setSourceName(sourceName);
                model.setSourceProId(sourceProId);
                model.setSourceUrl(sourceUrl);
                list.add(model);
                i = i2 + 1;
            } catch (JSONException e) {
                return list;
            }
        }
    }

    /* JADX INFO: Multiple debug info for r15v2 java.util.List<com.mobcent.android.model.MCLibUserStatus>: [D('list' java.util.List<com.mobcent.android.model.MCLibUserStatus>), D('e' org.json.JSONException)] */
    /* JADX INFO: Multiple debug info for r15v3 java.util.List<com.mobcent.android.model.MCLibUserStatus>: [D('list' java.util.List<com.mobcent.android.model.MCLibUserStatus>), D('array' org.json.JSONArray)] */
    /* JADX INFO: Multiple debug info for r10v2 int: [D('sourceProId' int), D('object' org.json.JSONObject)] */
    /* JADX INFO: Multiple debug info for r15v4 java.util.List<com.mobcent.android.model.MCLibUserStatus>: [D('list' java.util.List<com.mobcent.android.model.MCLibUserStatus>), D('array' org.json.JSONArray)] */
    public static List<MCLibUserStatus> getActionList(JSONArray array, String baseUrl) {
        List<MCLibUserStatus> list = new ArrayList<>();
        if (array == null) {
            return list;
        }
        int i = 0;
        while (true) {
            try {
                int i2 = i;
                if (i2 >= array.length()) {
                    return list;
                }
                MCLibUserStatus model = new MCLibUserStatus();
                JSONObject object = (JSONObject) array.get(i2);
                long id = object.optLong("id");
                String content = object.optString(MCLibMobCentApiConstant.SHORT_NAME);
                int fromUserId = object.optInt("fromUserId");
                String fromUserName = object.optString("fromUserName");
                String time = object.optString("time");
                String icon = object.optString("icon");
                int actionId = object.optInt("actionId");
                String sourceName = object.optString("sourceName");
                String sourceUrl = object.optString("sourceUrl");
                int sourceProId = object.optInt(MCLibMobCentApiConstant.SOURCE_PRO_ID);
                model.setStatusId(id);
                model.setActionId(actionId);
                model.setContent(content);
                model.setFromUserId(fromUserId);
                model.setFromUserName(fromUserName);
                model.setTime(time);
                model.setUserImageUrl(String.valueOf(baseUrl) + icon);
                model.setSourceName(sourceName);
                model.setSourceProId(sourceProId);
                model.setSourceUrl(sourceUrl);
                list.add(model);
                i = i2 + 1;
            } catch (JSONException e) {
                e.printStackTrace();
                return list;
            }
        }
    }

    public static List<MCLibSysMsgModel> getSysMsgList(JSONArray array) {
        List<MCLibSysMsgModel> list = new ArrayList<>();
        if (array != null) {
            int i = 0;
            while (i < array.length()) {
                try {
                    MCLibSysMsgModel model = new MCLibSysMsgModel();
                    JSONObject object = (JSONObject) array.get(i);
                    long sysMsgId = object.optLong("id");
                    String content = object.optString("content");
                    int fromId = object.optInt(MCLibMobCentApiConstant.FROM_ID);
                    String fromName = object.optString(MCLibMobCentApiConstant.FROM_NAME);
                    int targetId = object.optInt(MCLibMobCentApiConstant.TARGET_ID);
                    int sysMsgType = object.optInt("type");
                    model.setContent(content);
                    model.setFromId(fromId);
                    model.setFromName(fromName);
                    model.setSysMsgId(sysMsgId);
                    model.setTargetId(targetId);
                    model.setSysMsgType(sysMsgType);
                    list.add(model);
                    i++;
                } catch (JSONException e) {
                }
            }
        }
        return list;
    }
}
