package com.mobcent.android.service.impl;

import android.content.Context;
import com.mobcent.android.api.CommonRestfulApiRequester;
import com.mobcent.android.api.UserInfoRestfulApiRequester;
import com.mobcent.android.db.SharedUserDBUtil;
import com.mobcent.android.db.UserMagicActionDictDBUtil;
import com.mobcent.android.model.MCLibUserInfo;
import com.mobcent.android.service.MCLibUserInfoService;
import com.mobcent.android.service.impl.helper.UserInfoJsonHelper;
import com.mobcent.android.state.MCLibAppState;
import com.mobcent.android.utils.MD5Util;
import com.mobcent.android.utils.PhoneUtil;
import java.util.HashMap;
import java.util.List;

public class MCLibUserInfoServiceImpl implements MCLibUserInfoService {
    private Context context;

    public MCLibUserInfoServiceImpl(Context context2) {
        this.context = context2;
    }

    public String doLogin(int uid, String userName, String email, String password, boolean isSavePwd, boolean isAutoLogin, String simulate) {
        if (uid == 0) {
            uid = -1;
        }
        if (userName == null || userName.equals("")) {
            userName = "-1";
        }
        if (email == null || email.equals("")) {
            email = "-1";
        }
        if (password == null || password.equals("")) {
            password = "-1";
        }
        String jsonStr = CommonRestfulApiRequester.doLogin(uid, userName, email, password, PhoneUtil.getUserAgent(), PhoneUtil.getIMSI(this.context), PhoneUtil.getIMEI(this.context), simulate, this.context);
        if (jsonStr == null || jsonStr.equals("connection_fail")) {
            return "network_unavailable";
        }
        MCLibUserInfo user = UserInfoJsonHelper.formLoginStatus(jsonStr);
        if (user.getUid() > 0) {
            SharedUserDBUtil util = SharedUserDBUtil.getInstance(this.context);
            user.setEmail(email);
            if (isAutoLogin || isSavePwd) {
                user.setPassword(password);
            }
            if (!util.setUserAsCurrentUserInfo(user.getUid())) {
                util.addUser(user);
            } else {
                util.updateCurrentUserPhoto(user.getImage());
                util.updateCurrentUserMood(user.getEmotionWords());
                util.updateCurrentUserNickName(user.getNickName());
                if (isAutoLogin || isSavePwd) {
                    util.updateCurrentUserPwd(password);
                }
            }
            util.updateLoginStatus(isSavePwd, isAutoLogin);
            MCLibAppState.isNeedRelogin = MCLibAppState.NO_NEED_RELOGIN;
            return "rs_succ";
        }
        MCLibAppState.isValidLogin = false;
        return "loginFail";
    }

    public MCLibUserInfo regUser() {
        MCLibUserInfo user = UserInfoJsonHelper.formRegJsonUser(CommonRestfulApiRequester.doRegister(PhoneUtil.getUserAgent(), PhoneUtil.getIMSI(this.context), PhoneUtil.getIMEI(this.context), this.context));
        if (user.getUid() > 0) {
            SharedUserDBUtil util = SharedUserDBUtil.getInstance(this.context);
            user.setIsAutoLogin(1);
            user.setIsSavePwd(1);
            user.setIsForceShowMagicImage(1);
            util.addUser(user);
            MCLibAppState.isNeedRelogin = MCLibAppState.NO_NEED_RELOGIN;
        }
        return user;
    }

    public MCLibUserInfo getLoginUser() {
        return SharedUserDBUtil.getInstance(this.context).getCurrentUserInfo();
    }

    public boolean isSavePwd() {
        return SharedUserDBUtil.getInstance(this.context).isSavePwd();
    }

    public boolean isAutoLogin() {
        return SharedUserDBUtil.getInstance(this.context).isAutoLogin();
    }

    public List<MCLibUserInfo> getAllLocalUsers() {
        return SharedUserDBUtil.getInstance(this.context).getAllUsers();
    }

    public int getLoginUserId() {
        MCLibUserInfo userInfo = SharedUserDBUtil.getInstance(this.context).getCurrentUserInfo();
        if (userInfo == null) {
            return 0;
        }
        return userInfo.getUid();
    }

    public String getLoginUserName() {
        MCLibUserInfo userInfo = SharedUserDBUtil.getInstance(this.context).getCurrentUserInfo();
        if (userInfo == null) {
            return "";
        }
        return userInfo.getName();
    }

    public String getLoginUserNickName() {
        MCLibUserInfo userInfo = SharedUserDBUtil.getInstance(this.context).getCurrentUserInfo();
        if (userInfo == null) {
            return "";
        }
        return userInfo.getNickName();
    }

    public String cancelFollowUser(int userId, int toUserId) {
        return UserInfoJsonHelper.formJsonRS(UserInfoRestfulApiRequester.cancelFollowUser(userId, toUserId, this.context));
    }

    public String followUser(int userId, int toUserId) {
        return UserInfoJsonHelper.formJsonRS(UserInfoRestfulApiRequester.followUser(userId, toUserId, this.context));
    }

    public List<MCLibUserInfo> getFriendsFollowedTheUser(int userId, int page, int pageSize) {
        int totalNum;
        List<MCLibUserInfo> users = UserInfoJsonHelper.getJsonUsers(UserInfoRestfulApiRequester.getFriendsFollowedTheUser(userId, page, pageSize, this.context), page);
        if (users != null && users.size() > 0 && (totalNum = users.get(0).getTotalNum()) > users.size() + ((page - 1) * pageSize) && totalNum > pageSize) {
            MCLibUserInfo fetchMoreModel = new MCLibUserInfo();
            fetchMoreModel.setFetchMore(true);
            fetchMoreModel.setCurrentPage(page);
            users.add(fetchMoreModel);
        }
        return users;
    }

    public List<MCLibUserInfo> getFriendsUserFollowed(int userId, int page, int pageSize) {
        int totalNum;
        List<MCLibUserInfo> users = UserInfoJsonHelper.getJsonUsers(UserInfoRestfulApiRequester.getFriendsUserFollowed(userId, page, pageSize, this.context), page);
        if (users != null && users.size() > 0 && (totalNum = users.get(0).getTotalNum()) > users.size() + ((page - 1) * pageSize) && totalNum > pageSize) {
            MCLibUserInfo fetchMoreModel = new MCLibUserInfo();
            fetchMoreModel.setFetchMore(true);
            fetchMoreModel.setCurrentPage(page);
            users.add(fetchMoreModel);
        }
        return users;
    }

    public String changeUserProfile(int userId, String nickName, int gender, String birthday, String email, String location) {
        String rs = UserInfoJsonHelper.formJsonRS(UserInfoRestfulApiRequester.changeUserProfile(userId, nickName, gender, birthday, email, location, this.context));
        if (rs.equals("rs_succ")) {
            SharedUserDBUtil.getInstance(this.context).updateCurrentUserBirthday(birthday);
            SharedUserDBUtil.getInstance(this.context).updateCurrentUserGender(gender);
            SharedUserDBUtil.getInstance(this.context).updateCurrentUserNickName(nickName);
        }
        return rs;
    }

    public String changeUserPwd(int userId, String pwd) {
        String pwd2 = MD5Util.toMD5(pwd);
        String rs = UserInfoJsonHelper.formJsonRS(UserInfoRestfulApiRequester.changeUserPwd(userId, pwd2, this.context));
        if (rs.equals("rs_succ")) {
            SharedUserDBUtil.getInstance(this.context).updateCurrentUserPwd(pwd2);
        }
        return rs;
    }

    public MCLibUserInfo getUserProfile(int userId) {
        MCLibUserInfo userInfo = UserInfoJsonHelper.getJsonUser(UserInfoRestfulApiRequester.getUserInfo(userId, this.context));
        if (userInfo == null) {
            return SharedUserDBUtil.getInstance(this.context).getCurrentUserInfo();
        }
        return userInfo;
    }

    public List<MCLibUserInfo> searchUsers(int userId, String userNum, String userName, int type, int page, int pageSize) {
        int totalNum;
        List<MCLibUserInfo> users = UserInfoJsonHelper.getJsonUsers4Search(UserInfoRestfulApiRequester.searchUsers(userId, userNum, userName, type, page, pageSize, this.context), page, pageSize);
        if (users != null && users.size() > 0 && (totalNum = users.get(0).getTotalNum()) > users.size() + ((page - 1) * pageSize) && totalNum > pageSize) {
            MCLibUserInfo fetchMoreModel = new MCLibUserInfo();
            fetchMoreModel.setFetchMore(true);
            fetchMoreModel.setCurrentPage(page);
            users.add(fetchMoreModel);
        }
        return users;
    }

    public List<MCLibUserInfo> getRecommendUsers(int userId, int page, int pageSize) {
        int totalNum;
        List<MCLibUserInfo> users = UserInfoJsonHelper.getJsonUsers4Search(UserInfoRestfulApiRequester.getRecommendUsers(userId, page, pageSize, this.context), page, pageSize);
        if (users != null && users.size() > 0 && (totalNum = users.get(0).getTotalNum()) > users.size() + ((page - 1) * pageSize) && totalNum > pageSize) {
            MCLibUserInfo fetchMoreModel = new MCLibUserInfo();
            fetchMoreModel.setFetchMore(true);
            fetchMoreModel.setCurrentPage(page);
            users.add(fetchMoreModel);
        }
        return users;
    }

    public String changeUserPhoto(int userId, String iconPath, int type) {
        return UserInfoJsonHelper.formJsonRS(UserInfoRestfulApiRequester.changeUserPhoto(userId, iconPath, type, this.context));
    }

    public MCLibUserInfo uploadUserPhoto(int userId, String photoPath) {
        return UserInfoJsonHelper.formUploadImageJson(UserInfoRestfulApiRequester.uploadUserPhoto(userId, photoPath, this.context));
    }

    public void updateSharedUserPhoto(String picName) {
        SharedUserDBUtil.getInstance(this.context).updateCurrentUserPhoto(picName);
    }

    public void updateShareUserMood(String mood) {
        SharedUserDBUtil.getInstance(this.context).updateCurrentUserMood(mood);
    }

    public MCLibUserInfo getUserHomeInfo(int userId, int targetId) {
        return UserInfoJsonHelper.getJsonUserHomeInfo(UserInfoRestfulApiRequester.getUserHomeInfo(userId, targetId, this.context));
    }

    public List<MCLibUserInfo> getUserFans(int userId, int targetId, int page, int pageSize) {
        int totalNum;
        List<MCLibUserInfo> users = UserInfoJsonHelper.getJsonUsers(UserInfoRestfulApiRequester.getUserFans(userId, targetId, page, pageSize, this.context), page);
        if (users != null && users.size() > 0 && (totalNum = users.get(0).getTotalNum()) > users.size() + ((page - 1) * pageSize) && totalNum > pageSize) {
            MCLibUserInfo fetchMoreModel = new MCLibUserInfo();
            fetchMoreModel.setFetchMore(true);
            fetchMoreModel.setCurrentPage(page);
            users.add(fetchMoreModel);
        }
        return users;
    }

    public List<MCLibUserInfo> getUserFriends(int userId, int targetId, int page, int pageSize) {
        int totalNum;
        List<MCLibUserInfo> users = UserInfoJsonHelper.getJsonUsers(UserInfoRestfulApiRequester.getUserFriends(userId, targetId, page, pageSize, this.context), page);
        if (users != null && users.size() > 0 && (totalNum = users.get(0).getTotalNum()) > users.size() + ((page - 1) * pageSize) && totalNum > pageSize) {
            MCLibUserInfo fetchMoreModel = new MCLibUserInfo();
            fetchMoreModel.setFetchMore(true);
            fetchMoreModel.setCurrentPage(page);
            users.add(fetchMoreModel);
        }
        return users;
    }

    public void updateShareUserNickName(String nickName) {
        SharedUserDBUtil.getInstance(this.context).updateCurrentUserNickName(nickName);
    }

    public void updateShareUserBirthday(String birthday) {
        SharedUserDBUtil.getInstance(this.context).updateCurrentUserBirthday(birthday);
    }

    public void updateShareUserCity(String city) {
        SharedUserDBUtil.getInstance(this.context).updateCurrentUserCity(city);
    }

    public void updateShareUserEmail(String email) {
        SharedUserDBUtil.getInstance(this.context).updateCurrentUserEmail(email);
    }

    public void updateShareUserGender(int gender) {
        SharedUserDBUtil.getInstance(this.context).updateCurrentUserGender(gender);
    }

    public void setSoundEnable(boolean isSoundEnable) {
        UserMagicActionDictDBUtil.getInstance(this.context).updateSoundEnable(isSoundEnable);
    }

    public boolean isSoundEnable() {
        return UserMagicActionDictDBUtil.getInstance(this.context).isSoundEnable();
    }

    public void updateCommunityHomeMode(int mode) {
        UserMagicActionDictDBUtil.getInstance(this.context).updateCommunityHomeMode(mode);
    }

    public int getCommunityHomeMode() {
        return UserMagicActionDictDBUtil.getInstance(this.context).getCommunityHomeMode();
    }

    public String syncSites(int userId, String name, String pwd, int type, boolean isCancelSync) {
        String rs = UserInfoJsonHelper.formJsonRS(UserInfoRestfulApiRequester.syncSites(userId, name, pwd, type, isCancelSync, this.context));
        if (rs.equals("rs_succ")) {
            if (isCancelSync) {
                SharedUserDBUtil.getInstance(this.context).removeSyncSite(userId, type);
            } else {
                SharedUserDBUtil.getInstance(this.context).addOrUpdateSyncSite(userId, type, name);
            }
        }
        return rs;
    }

    public HashMap<Integer, String> getAllSyncSites(int userId) {
        return SharedUserDBUtil.getInstance(this.context).getAllSyncSites(userId);
    }

    public String addToBlackList(int userId, int targetId) {
        return UserInfoJsonHelper.formJsonRS(UserInfoRestfulApiRequester.addToBlackList(userId, targetId, this.context));
    }

    public String removeFromBlackList(int userId, int targetId) {
        return UserInfoJsonHelper.formJsonRS(UserInfoRestfulApiRequester.removeFromBlackList(userId, targetId, this.context));
    }

    public List<MCLibUserInfo> getBlockList(int userId, int page, int pageSize) {
        int totalNum;
        List<MCLibUserInfo> users = UserInfoJsonHelper.getJsonUsers(UserInfoRestfulApiRequester.getBlockUsers(userId, page, pageSize, this.context), page);
        if (users != null && users.size() > 0 && (totalNum = users.get(0).getTotalNum()) > users.size() + ((page - 1) * pageSize) && totalNum > pageSize) {
            MCLibUserInfo fetchMoreModel = new MCLibUserInfo();
            fetchMoreModel.setFetchMore(true);
            fetchMoreModel.setCurrentPage(page);
            users.add(fetchMoreModel);
        }
        return users;
    }
}
