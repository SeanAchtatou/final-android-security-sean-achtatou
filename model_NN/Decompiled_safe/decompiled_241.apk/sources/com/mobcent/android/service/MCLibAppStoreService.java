package com.mobcent.android.service;

import com.mobcent.android.model.MCLibAppStoreAppModel;

public interface MCLibAppStoreService {
    MCLibAppStoreAppModel getAppInfo(int i, int i2);
}
