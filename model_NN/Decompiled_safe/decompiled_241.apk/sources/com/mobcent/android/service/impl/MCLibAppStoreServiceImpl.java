package com.mobcent.android.service.impl;

import android.content.Context;
import com.mobcent.android.api.AppStoreRestfulApiRequester;
import com.mobcent.android.model.MCLibAppStoreAppModel;
import com.mobcent.android.service.MCLibAppStoreService;
import com.mobcent.android.service.impl.helper.AppStoreJsonHelper;

public class MCLibAppStoreServiceImpl implements MCLibAppStoreService {
    private Context context;

    public MCLibAppStoreServiceImpl(Context context2) {
        this.context = context2;
    }

    public MCLibAppStoreAppModel getAppInfo(int uid, int appId) {
        return AppStoreJsonHelper.formJsonAppModel(AppStoreRestfulApiRequester.getAppStoreAppInfo(uid, appId, this.context), appId);
    }
}
