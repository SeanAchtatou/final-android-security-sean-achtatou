package com.mobcent.android.service;

import com.mobcent.android.model.MCLibUserStatus;
import java.util.List;

public interface MCLibStatusService {
    String forwordStatus(int i, long j);

    List<MCLibUserStatus> getFollowedUserStatus(int i, int i2, int i3, boolean z);

    List<MCLibUserStatus> getFriendEvents(int i, int i2, int i3, boolean z);

    List<MCLibUserStatus> getHallEvents(int i, int i2, int i3, boolean z);

    List<MCLibUserStatus> getHallStatus(int i, int i2, int i3, boolean z);

    List<MCLibUserStatus> getHallTopic(int i, int i2, int i3, boolean z);

    List<MCLibUserStatus> getMyStatus(int i, int i2, int i3, int i4, boolean z, boolean z2);

    List<MCLibUserStatus> getStatusThread(int i, long j, int i2, int i3);

    List<MCLibUserStatus> getStatusThreadAsComment(int i, String str, String str2, int i2, int i3);

    List<MCLibUserStatus> getTalkToMeStatus(int i, int i2, int i3, boolean z);

    List<MCLibUserStatus> getUserEvents(int i, int i2, int i3, int i4);

    String modifyMyMood(int i, String str);

    String notifyServerMsgRead(int i, String str);

    MCLibUserStatus publishStatusImage(int i, String str);

    String publishUserStatus(int i, String str, int i2, String str2);

    boolean putStatusInQueue(MCLibUserStatus mCLibUserStatus);

    String replyToUserStatus(int i, String str, long j, long j2, int i2, String str2);

    String sendMsg(int i, String str, int i2, String str2, int i3);

    String talkToUser(int i, String str, int i2);
}
