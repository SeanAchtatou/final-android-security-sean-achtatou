package com.mobcent.android.constants;

public interface MCLibReturnCodeConstant {
    public static final String CONNECTION_FAIL = "connection_fail";
    public static final String PUBLISH_FAIL = "publish_fail";
    public static final String PUBLISH_SUCC = "publish_succ";
    public static final String UPLOAD_IMAGE_FAIL_MSG = "upload_images_fail";
    public static final String WARNING_RELOGIN = "warning_relogin";
}
