package com.mobcent.android.constants;

public interface MCLibConstants {
    public static final String ANDROID = "1";
    public static final int DEFAULT_GRID_PAGE_SIZE = 30;
    public static final int DEFAULT_PAGE_SIZE = 10;
    public static final int DEFAULT_PRIVATE_MSG_PAGE_SIZE = 20;
    public static final int DEFAULT_RANGE = 1000;
    public static final int DOWNLOAD_APP_UPDATE_DB_INTERVAL = 1000;
    public static final int MICROBLOG_WORDS_UPPER_LIMIT = 140;
    public static final int RECONNECTION_INTERVAL = 15000;
    public static final String RESOLUTION_100X100 = "100x100";
    public static final String RESOLUTION_240X320 = "240x320";
    public static final String RESOLUTION_320X480 = "320x480";
    public static final String SDK_VERSION = "1.0.0";
    public static final int USER_FEEDBACK = 500;
    public static final int USER_MOOD = 50;
}
