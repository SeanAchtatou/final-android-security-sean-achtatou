package com.mobcent.android.constants;

public interface MCLibParameterKeyConstant {
    public static final String GO_TO_ACTIVITY_CLASS = "goToActicityClass";
    public static final String IS_FORCE_REFRESH = "isForceRefresh";
    public static final String IS_SHARE_APP = "isShareApp";
    public static final String MSG_AUDIO_RID = "MSG_AUDIO_RID";
    public static final String STATUS_REPLY_ID = "statusReplyId";
    public static final String STATUS_ROOT_ID = "statusRootId";
    public static final String STATUS_TYPE = "statusType";
    public static final String USER_ID = "userId";
    public static final String USER_NICK_NAME = "nickName";
    public static final String USER_PHOTO = "userPhoto";
}
