package com.mobcent.android.constants;

public interface MCLibCommunicationType {
    public static final int EVENT_APP_USING = 18;
    public static final int EVENT_FOLLOW = 12;
    public static final int EVENT_MODIFY_MOOD = 17;
    public static final int EVENT_MODIFY_PHOTO = 16;
    public static final int USER_COMMENT_SOURCE = 8;
    public static final int USER_FEED_BACK = 15;
    public static final int USER_MOOD = 11;
    public static final int USER_PRIVATE_MSG = 9;
    public static final int USER_REPLY = 2;
    public static final int USER_REPLY_LIST = 0;
    public static final int USER_SHARE_APP = 5;
    public static final int USER_STATUS = 1;
    public static final int USER_TALK_TO_USER = 10;
}
