package com.mobcent.android.constants;

public interface MCLibMsgConstant {
    public static final int NEVER_READ = 1;
    public static final int READ = 0;
}
