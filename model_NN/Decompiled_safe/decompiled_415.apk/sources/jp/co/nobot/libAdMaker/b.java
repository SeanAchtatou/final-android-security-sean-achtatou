package jp.co.nobot.libAdMaker;

import android.content.Intent;
import android.net.Uri;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import java.util.Date;

final class b extends WebViewClient {
    private /* synthetic */ libAdMaker a;

    b(libAdMaker libadmaker) {
        this.a = libadmaker;
    }

    public final void onLoadResource(WebView webView, String str) {
        int indexOf;
        String str2;
        if (this.a.f) {
            webView.stopLoading();
        } else if (this.a.h()) {
            if (str.indexOf("http://images.ad-maker.info/apps") == 0) {
                try {
                    str2 = e.a(String.format("%s%s%s", this.a.l, Long.valueOf(new Date().getTime()), "mXFTQ9fp73rqK5aaOAuQ8yP8"));
                } catch (Exception e) {
                    str2 = "";
                }
                CookieManager.getInstance().setCookie("images.ad-maker.info", this.a.p);
                CookieManager.getInstance().setCookie("images.ad-maker.info", this.a.q);
                CookieManager.getInstance().setCookie("images.ad-maker.info", this.a.r);
                CookieManager.getInstance().setCookie("images.ad-maker.info", this.a.s);
                CookieManager.getInstance().setCookie("images.ad-maker.info", this.a.t);
                CookieManager.getInstance().setCookie("images.ad-maker.info", "admaker_sgt=" + str2 + "; domain=" + "images.ad-maker.info");
                CookieSyncManager.getInstance().sync();
            }
            int indexOf2 = str.indexOf("expand.html");
            if (str.indexOf("exp=2") != -1 && !this.a.g) {
                this.a.g = true;
                this.a.c.loadUrl(str);
                this.a.c();
            }
            if (indexOf2 == -1 && (indexOf = str.indexOf("www/delivery/ck.php?oaparams")) != -1 && indexOf < 35) {
                try {
                    webView.stopLoading();
                    this.a.h.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        }
    }

    public final void onPageFinished(WebView webView, String str) {
        String title = webView.getTitle();
        if ((title != null ? title.indexOf("404") : -1) != -1) {
            this.a.setVisibility(8);
            if (this.a.u != null) {
                this.a.u.a();
                return;
            }
            return;
        }
        this.a.k = true;
        CookieSyncManager.getInstance().sync();
        str.indexOf("exp=2");
    }

    public final void onReceivedError(WebView webView, int i, String str, String str2) {
        this.a.setVisibility(8);
        if (this.a.u != null) {
            this.a.u.a();
        }
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        try {
            webView.stopLoading();
            this.a.h.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return true;
        }
    }
}
