package com.kajirin.android.pyramid_breed;

import android.preference.Preference;

final class b implements Preference.OnPreferenceChangeListener {
    private /* synthetic */ Setting a;

    b(Setting setting) {
        this.a = setting;
    }

    public final boolean onPreferenceChange(Preference preference, Object obj) {
        Pyramid_Breed.d = Setting.b((String) obj);
        this.a.getPreferenceManager().findPreference("live_preference").setSummary(Setting.d[Pyramid_Breed.d]);
        return true;
    }
}
