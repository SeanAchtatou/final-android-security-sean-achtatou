package com.kajirin.android.pyramid_breed;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import java.lang.reflect.Array;

public final class n extends SurfaceView implements SurfaceHolder.Callback, Runnable {
    private int A;
    private int B;
    private long C;
    private int D;
    private int E;
    private int F;
    private int G;
    private int H;
    private int I;
    private int J;
    private int K;
    private int L;
    private int M;
    private int N;
    private int O;
    private int P;
    private int Q;
    private int R;
    private int S;
    private int T;
    private float U;
    private int V;
    private int W;
    private int X;
    private int Y;
    private int Z;
    private int a;
    private int[] aA;
    private int aB;
    private int aC;
    private int aD;
    private int aE;
    private int aF;
    private int aa;
    private int ab;
    private float ac;
    private int ad;
    private int ae;
    private int af;
    private int ag;
    private int ah;
    private int ai;
    private int aj;
    private Rect ak;
    private Rect al;
    private int am;
    private int[] an;
    private int[] ao;
    private int[] ap;
    private int[][] aq;
    private int[][] ar;
    private int[][] as;
    private int[] at;
    private int[] au;
    private int[] av;
    private int[] aw;
    private int[] ax;
    private int[] ay;
    private int[] az;
    private final int b;
    private Handler c;
    private Runnable d;
    private float e;
    private float f;
    private int g;
    private SurfaceHolder h;
    private Thread i;
    private int j;
    private Bitmap k;
    private Bitmap l;
    private Bitmap m;
    private Bitmap n;
    private Bitmap o;
    private Bitmap p;
    private Bitmap q;
    private Bitmap r;
    private Bitmap[] s;
    private int t;
    private int u;
    private Canvas v;
    private Bitmap[] w;
    private int[] x;
    private int[] y;
    private int[] z;

    public n(Context context) {
        super(context);
        this.a = 0;
        this.b = 5000;
        this.c = new Handler();
        this.e = 1.0f;
        this.f = 1.0f;
        this.g = 0;
        this.j = 0;
        this.k = null;
        this.s = new Bitmap[4];
        this.t = 0;
        this.u = 0;
        this.w = new Bitmap[9];
        this.x = new int[9];
        this.y = new int[9];
        this.z = new int[9];
        this.F = 0;
        this.G = 0;
        this.H = 0;
        this.I = 0;
        this.J = 0;
        this.K = 0;
        this.L = 0;
        this.M = 0;
        this.N = 0;
        this.O = 0;
        this.P = -999;
        this.Q = 0;
        this.R = 0;
        this.S = 0;
        this.T = 0;
        this.aa = 0;
        this.ab = 0;
        this.an = new int[52];
        this.ao = new int[52];
        this.ap = new int[25];
        this.aq = (int[][]) Array.newInstance(Integer.TYPE, 5, 5);
        this.ar = (int[][]) Array.newInstance(Integer.TYPE, 5, 5);
        this.as = (int[][]) Array.newInstance(Integer.TYPE, 5, 5);
        this.at = new int[52];
        this.au = new int[52];
        this.av = new int[52];
        this.aw = new int[52];
        this.ax = new int[28];
        this.ay = new int[28];
        this.az = new int[28];
        this.aA = new int[28];
        this.aE = 0;
        this.aF = 0;
        this.g = 0;
        this.F = 200;
        this.G = 200;
        this.f = context.getResources().getDisplayMetrics().density;
        this.e = this.f;
        this.A = 0;
        this.B = 0;
        this.h = getHolder();
        this.h.addCallback(this);
        this.h.setFixedSize(getWidth(), getHeight());
    }

    private static int a(int i2, int i3) {
        int i4 = (i3 - i2) + 1;
        return i4 > 0 ? o.a(i4) + i2 : i2;
    }

    private static int a(int i2, int i3, Rect rect) {
        return (i3 <= rect.top || i3 > rect.bottom || i2 <= rect.left || i2 > rect.right) ? 0 : 1;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:111:0x02f3  */
    /* JADX WARNING: Removed duplicated region for block: B:192:0x0450  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a() {
        /*
            r15 = this;
            r9 = 99
            r13 = 3
            r7 = 25
            r12 = 1
            r11 = 0
            int r0 = com.kajirin.android.pyramid_breed.Pyramid_Breed.r
            if (r0 != r12) goto L_0x0018
            r15.aj = r11
            r15.E = r11
            r15.ai = r11
            int r0 = com.kajirin.android.pyramid_breed.Pyramid_Breed.m
            com.kajirin.android.pyramid_breed.Pyramid_Breed.l = r0
            com.kajirin.android.pyramid_breed.Pyramid_Breed.r = r11
        L_0x0017:
            return
        L_0x0018:
            int r0 = com.kajirin.android.pyramid_breed.Pyramid_Breed.s
            if (r0 != r12) goto L_0x0042
            int r0 = com.kajirin.android.pyramid_breed.Pyramid_Breed.t
            if (r0 != 0) goto L_0x0221
            int r0 = r15.S
            r15.F = r0
            int r0 = r15.T
            r15.G = r0
            float r0 = r15.U
            r15.e = r0
            int r0 = r15.V
            r15.H = r0
            int r0 = r15.W
            r15.I = r0
            int r0 = r15.X
            r15.J = r0
            int r0 = r15.Y
            r15.K = r0
            int r0 = r15.Z
            r15.L = r0
        L_0x0040:
            com.kajirin.android.pyramid_breed.Pyramid_Breed.s = r11
        L_0x0042:
            long r0 = java.lang.System.currentTimeMillis()
            long r2 = r15.C
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 > 0) goto L_0x0050
            int r0 = com.kajirin.android.pyramid_breed.Pyramid_Breed.p
            if (r0 != 0) goto L_0x0482
        L_0x0050:
            r0 = 5
            r15.c(r0)
            int r0 = r15.ai
            if (r0 != r12) goto L_0x0243
            int r0 = com.kajirin.android.pyramid_breed.Pyramid_Breed.w
            r1 = 10000(0x2710, float:1.4013E-41)
            if (r0 >= r1) goto L_0x0064
            int r0 = com.kajirin.android.pyramid_breed.Pyramid_Breed.w
            int r0 = r0 + 1
            com.kajirin.android.pyramid_breed.Pyramid_Breed.w = r0
        L_0x0064:
            int r0 = r15.M
            if (r0 <= 0) goto L_0x0075
            int r0 = r15.M
            int r0 = r0 - r12
            r15.M = r0
            int r0 = r15.M
            if (r0 != 0) goto L_0x0075
            int r0 = r15.J
            r15.H = r0
        L_0x0075:
            int r0 = r15.aD
            int r0 = r0 + 1
            r15.aD = r0
            int r0 = r15.aD
            r1 = 70
            if (r0 <= r1) goto L_0x0083
            r15.aD = r11
        L_0x0083:
            int r0 = com.kajirin.android.pyramid_breed.Pyramid_Breed.q
            if (r0 != r12) goto L_0x0247
            int r0 = r15.K
            r15.H = r0
            r0 = 2
            r15.M = r0
        L_0x008e:
            int r0 = com.kajirin.android.pyramid_breed.Pyramid_Breed.p
            if (r0 != 0) goto L_0x00b1
            int r0 = r15.ai
            if (r0 != r12) goto L_0x026e
            int r0 = r15.E
            if (r0 != 0) goto L_0x026e
            r15.E = r12
            r15.ai = r11
            int r0 = com.kajirin.android.pyramid_breed.Pyramid_Breed.e
            if (r0 <= 0) goto L_0x025c
            int r0 = com.kajirin.android.pyramid_breed.Pyramid_Breed.e
            if (r0 <= r7) goto L_0x0254
            r15.aj = r7
            int r0 = com.kajirin.android.pyramid_breed.Pyramid_Breed.e
            int r0 = r0 - r7
            com.kajirin.android.pyramid_breed.Pyramid_Breed.e = r0
        L_0x00ad:
            r0 = -999(0xfffffffffffffc19, float:NaN)
            com.kajirin.android.pyramid_breed.Pyramid_Breed.p = r0
        L_0x00b1:
            int r0 = com.kajirin.android.pyramid_breed.Pyramid_Breed.k
            if (r0 != r12) goto L_0x00be
            r0 = 2
            a(r0)
            r15.r()
            com.kajirin.android.pyramid_breed.Pyramid_Breed.k = r11
        L_0x00be:
            int r0 = com.kajirin.android.pyramid_breed.Pyramid_Breed.l
            switch(r0) {
                case 1: goto L_0x0478;
                case 2: goto L_0x047d;
                default: goto L_0x00c3;
            }
        L_0x00c3:
            r15.D = r12
        L_0x00c5:
            int r0 = r15.D
            if (r0 != r12) goto L_0x0017
            int r0 = com.kajirin.android.pyramid_breed.Pyramid_Breed.e
            r15.b(r0)
            int r0 = r15.aE
            if (r0 <= 0) goto L_0x0163
            android.graphics.Paint r1 = new android.graphics.Paint
            r1.<init>()
            r1.setAntiAlias(r12)
            android.graphics.Paint$Style r0 = android.graphics.Paint.Style.FILL
            r1.setStyle(r0)
            r0 = 1098907648(0x41800000, float:16.0)
            r1.setTextSize(r0)
            r0 = -7829368(0xffffffffff888888, float:NaN)
            r1.setColor(r0)
            android.graphics.Canvas r0 = r15.v
            android.graphics.Rect r2 = new android.graphics.Rect
            r3 = 21
            r4 = 176(0xb0, float:2.47E-43)
            r5 = 291(0x123, float:4.08E-43)
            r6 = 231(0xe7, float:3.24E-43)
            r2.<init>(r3, r4, r5, r6)
            r0.drawRect(r2, r1)
            r0 = -65536(0xffffffffffff0000, float:NaN)
            r1.setColor(r0)
            android.graphics.Canvas r0 = r15.v
            android.graphics.Rect r2 = new android.graphics.Rect
            r3 = 20
            r4 = 175(0xaf, float:2.45E-43)
            r5 = 290(0x122, float:4.06E-43)
            r6 = 230(0xe6, float:3.22E-43)
            r2.<init>(r3, r4, r5, r6)
            r0.drawRect(r2, r1)
            r0 = -1
            r1.setColor(r0)
            android.graphics.Canvas r0 = r15.v
            android.graphics.Rect r2 = new android.graphics.Rect
            r3 = 21
            r4 = 176(0xb0, float:2.47E-43)
            r5 = 288(0x120, float:4.04E-43)
            r6 = 228(0xe4, float:3.2E-43)
            r2.<init>(r3, r4, r5, r6)
            r0.drawRect(r2, r1)
            r0 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r1.setColor(r0)
            int r0 = r15.aF
            if (r0 != 0) goto L_0x015e
            android.graphics.Canvas r2 = r15.v
            android.content.res.Resources r0 = r15.getResources()
            r3 = 2131165210(0x7f07001a, float:1.794463E38)
            java.lang.CharSequence r0 = r0.getText(r3)
            java.lang.String r0 = (java.lang.String) r0
            r3 = 1108082688(0x420c0000, float:35.0)
            r4 = 1128792064(0x43480000, float:200.0)
            r2.drawText(r0, r3, r4, r1)
            android.graphics.Canvas r2 = r15.v
            android.content.res.Resources r0 = r15.getResources()
            r3 = 2131165211(0x7f07001b, float:1.7944633E38)
            java.lang.CharSequence r0 = r0.getText(r3)
            java.lang.String r0 = (java.lang.String) r0
            r3 = 1108082688(0x420c0000, float:35.0)
            r4 = 1130102784(0x435c0000, float:220.0)
            r2.drawText(r0, r3, r4, r1)
        L_0x015e:
            int r0 = r15.aE
            int r0 = r0 - r12
            r15.aE = r0
        L_0x0163:
            android.view.SurfaceHolder r0 = r15.h
            android.graphics.Canvas r0 = r0.lockCanvas()
            r1 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r0.drawColor(r1)
            int r1 = com.kajirin.android.pyramid_breed.Pyramid_Breed.t
            if (r1 != r12) goto L_0x017b
            r1 = 1119092736(0x42b40000, float:90.0)
            r2 = 0
            int r3 = r15.G
            float r3 = (float) r3
            r0.rotate(r1, r2, r3)
        L_0x017b:
            r1 = 1120403456(0x42c80000, float:100.0)
            float r2 = r15.e
            float r1 = r1 * r2
            int r1 = (int) r1
            int r1 = r1 * 100
            int r1 = r1 / 100
            r2 = 1120403456(0x42c80000, float:100.0)
            float r3 = r15.e
            float r2 = r2 * r3
            int r2 = (int) r2
            int r2 = r2 * 100
            int r2 = r2 / 100
            android.graphics.Bitmap r3 = r15.r
            android.graphics.Bitmap r3 = android.graphics.Bitmap.createScaledBitmap(r3, r2, r1, r12)
            int r4 = r15.F
            int r4 = r4 / r2
            int r4 = r4 + 1
            int r5 = r15.G
            int r5 = r5 / r1
            int r5 = r5 + 1
            int r6 = com.kajirin.android.pyramid_breed.Pyramid_Breed.t
            if (r6 != 0) goto L_0x04a0
            r6 = r11
        L_0x01a4:
            if (r6 < r5) goto L_0x048c
        L_0x01a6:
            float r1 = r15.e
            r2 = 1065353216(0x3f800000, float:1.0)
            int r1 = (r1 > r2 ? 1 : (r1 == r2 ? 0 : -1))
            if (r1 != 0) goto L_0x04d3
            android.graphics.Rect r1 = new android.graphics.Rect
            r2 = 600(0x258, float:8.41E-43)
            r3 = 400(0x190, float:5.6E-43)
            r1.<init>(r11, r11, r2, r3)
            int r2 = com.kajirin.android.pyramid_breed.Pyramid_Breed.t
            if (r2 != 0) goto L_0x04ba
            android.graphics.Rect r2 = new android.graphics.Rect
            int r3 = r15.H
            int r4 = r15.I
            int r5 = r15.H
            int r5 = r5 + 600
            int r6 = r15.I
            int r6 = r6 + 400
            r2.<init>(r3, r4, r5, r6)
        L_0x01cc:
            android.graphics.Bitmap r3 = r15.k
            r4 = 0
            r0.drawBitmap(r3, r1, r2, r4)
        L_0x01d2:
            r15.a(r0)
            int r1 = r15.ai
            if (r1 != r12) goto L_0x0218
            int r1 = com.kajirin.android.pyramid_breed.Pyramid_Breed.w
            r2 = 700(0x2bc, float:9.81E-43)
            if (r1 <= r2) goto L_0x0218
            int r1 = com.kajirin.android.pyramid_breed.Pyramid_Breed.t
            if (r1 != 0) goto L_0x050c
            int r1 = r15.S
            r2 = 210(0xd2, float:2.94E-43)
            int r1 = r1 - r2
            int r1 = r1 / 2
            int r2 = r15.T
            r3 = 140(0x8c, float:1.96E-43)
            int r2 = r2 - r3
            int r2 = r2 / 2
            r14 = r2
            r2 = r1
            r1 = r14
        L_0x01f4:
            android.graphics.Bitmap[] r3 = r15.s
            int r4 = r15.t
            r3 = r3[r4]
            float r2 = (float) r2
            float r1 = (float) r1
            r4 = 0
            r0.drawBitmap(r3, r2, r1, r4)
            int r1 = r15.u
            int r1 = r1 + 1
            r15.u = r1
            int r1 = r15.u
            if (r1 <= r13) goto L_0x0218
            r15.u = r11
            int r1 = r15.t
            int r1 = r1 + 1
            r15.t = r1
            int r1 = r15.t
            if (r1 <= r13) goto L_0x0218
            r15.t = r11
        L_0x0218:
            android.view.SurfaceHolder r1 = r15.h
            r1.unlockCanvasAndPost(r0)
            r15.D = r11
            goto L_0x0017
        L_0x0221:
            int r0 = r15.aa
            r15.F = r0
            int r0 = r15.ab
            r15.G = r0
            float r0 = r15.ac
            r15.e = r0
            int r0 = r15.ad
            r15.H = r0
            int r0 = r15.ae
            r15.I = r0
            int r0 = r15.af
            r15.J = r0
            int r0 = r15.ag
            r15.K = r0
            int r0 = r15.ah
            r15.L = r0
            goto L_0x0040
        L_0x0243:
            com.kajirin.android.pyramid_breed.Pyramid_Breed.w = r11
            goto L_0x0064
        L_0x0247:
            int r0 = com.kajirin.android.pyramid_breed.Pyramid_Breed.q
            if (r0 != r13) goto L_0x008e
            int r0 = r15.L
            r15.H = r0
            r0 = 2
            r15.M = r0
            goto L_0x008e
        L_0x0254:
            int r0 = com.kajirin.android.pyramid_breed.Pyramid_Breed.e
            r15.aj = r0
            com.kajirin.android.pyramid_breed.Pyramid_Breed.e = r11
            goto L_0x00ad
        L_0x025c:
            q()
            r15.aj = r7
            int r0 = com.kajirin.android.pyramid_breed.Pyramid_Breed.e
            int r0 = r0 - r7
            com.kajirin.android.pyramid_breed.Pyramid_Breed.e = r0
            r0 = 20
            r15.aE = r0
            r15.aF = r11
            goto L_0x00ad
        L_0x026e:
            int r0 = com.kajirin.android.pyramid_breed.Pyramid_Breed.n
            int r1 = com.kajirin.android.pyramid_breed.Pyramid_Breed.o
            r2 = 28
            int[] r2 = new int[r2]
            r2 = {210, 180, 240, 150, 210, 270, 120, 180, 240, 300, 90, 150, 210, 270, 330, 60, 120, 180, 240, 300, 360, 30, 90, 150, 210, 270, 330, 390} // fill-array
            r3 = 28
            int[] r3 = new int[r3]
            r3 = {30, 70, 70, 110, 110, 110, 150, 150, 150, 150, 190, 190, 190, 190, 190, 230, 230, 230, 230, 230, 230, 270, 270, 270, 270, 270, 270, 270} // fill-array
            com.kajirin.android.pyramid_breed.Pyramid_Breed.c = r11
            android.graphics.Rect r4 = new android.graphics.Rect
            r5 = 600(0x258, float:8.41E-43)
            r6 = 400(0x190, float:5.6E-43)
            r4.<init>(r11, r11, r5, r6)
            int r5 = r15.ai
            if (r5 != r12) goto L_0x02d2
            int r4 = a(r0, r1, r4)
            if (r4 != r12) goto L_0x02d2
            r15.E = r12
            r15.ai = r11
            int r0 = com.kajirin.android.pyramid_breed.Pyramid_Breed.e
            if (r0 <= 0) goto L_0x02c1
            int r0 = com.kajirin.android.pyramid_breed.Pyramid_Breed.e
            if (r0 <= r7) goto L_0x02ba
            r15.aj = r7
            int r0 = com.kajirin.android.pyramid_breed.Pyramid_Breed.e
            int r0 = r0 - r7
            com.kajirin.android.pyramid_breed.Pyramid_Breed.e = r0
        L_0x02a8:
            r0 = r11
        L_0x02a9:
            int r1 = com.kajirin.android.pyramid_breed.Pyramid_Breed.q
            r2 = 2
            if (r1 != r2) goto L_0x00ad
            if (r0 != r12) goto L_0x0461
            int r0 = r15.K
            r15.H = r0
            r0 = 40
            r15.M = r0
            goto L_0x00ad
        L_0x02ba:
            int r0 = com.kajirin.android.pyramid_breed.Pyramid_Breed.e
            r15.aj = r0
            com.kajirin.android.pyramid_breed.Pyramid_Breed.e = r11
            goto L_0x02a8
        L_0x02c1:
            q()
            r15.aj = r7
            int r0 = com.kajirin.android.pyramid_breed.Pyramid_Breed.e
            int r0 = r0 - r7
            com.kajirin.android.pyramid_breed.Pyramid_Breed.e = r0
            r0 = 20
            r15.aE = r0
            r15.aF = r11
            goto L_0x02a8
        L_0x02d2:
            android.graphics.Rect r4 = new android.graphics.Rect
            r5 = 520(0x208, float:7.29E-43)
            r6 = 120(0x78, float:1.68E-43)
            r7 = 580(0x244, float:8.13E-43)
            r8 = 180(0xb4, float:2.52E-43)
            r4.<init>(r5, r6, r7, r8)
            int r4 = a(r0, r1, r4)
            if (r4 != r12) goto L_0x02ea
            r15.r()
            r0 = r11
            goto L_0x02a9
        L_0x02ea:
            int r4 = com.kajirin.android.pyramid_breed.Pyramid_Breed.l
            switch(r4) {
                case 1: goto L_0x02f5;
                case 2: goto L_0x03fe;
                default: goto L_0x02ef;
            }
        L_0x02ef:
            int r1 = com.kajirin.android.pyramid_breed.Pyramid_Breed.t
            if (r1 != r12) goto L_0x0450
            r0 = r11
            goto L_0x02a9
        L_0x02f5:
            int r4 = r15.ai
            if (r4 != r13) goto L_0x0392
            int r4 = r15.am
            if (r4 > 0) goto L_0x0301
            int r4 = r15.aB
            if (r4 <= 0) goto L_0x0392
        L_0x0301:
            android.graphics.Rect r4 = r15.ak
            int r4 = a(r0, r1, r4)
            if (r4 != r12) goto L_0x0392
            int r0 = r15.am
            if (r0 != 0) goto L_0x033e
            int r0 = r15.aB
            if (r0 <= 0) goto L_0x033e
            int r0 = r15.aB
            int r0 = r0 - r12
            r15.aB = r0
            int[] r0 = r15.at
            r0 = r0[r11]
            if (r0 == r9) goto L_0x0329
            int[] r0 = r15.ao
            int[] r1 = r15.at
            r1 = r1[r11]
            a(r0, r1)
            int[] r0 = r15.at
            r0[r11] = r9
        L_0x0329:
            r0 = r11
        L_0x032a:
            r1 = 52
            if (r0 < r1) goto L_0x035f
            r15.f()
            r15.g()
            int[] r0 = r15.an
            a(r0)
            r0 = r11
        L_0x033a:
            r1 = 52
            if (r0 < r1) goto L_0x036a
        L_0x033e:
            int[] r0 = r15.at
            r0 = r0[r11]
            if (r0 == r9) goto L_0x034d
            int[] r0 = r15.ao
            int[] r1 = r15.at
            r1 = r1[r11]
            a(r0, r1)
        L_0x034d:
            int[] r0 = r15.an
            r0 = r0[r11]
            if (r0 == r9) goto L_0x0371
            r15.a = r11
            r0 = 20
            r15.E = r0
        L_0x0359:
            r0 = 4
            r15.ai = r0
            r0 = r11
            goto L_0x02a9
        L_0x035f:
            int[] r1 = r15.an
            int[] r2 = r15.ao
            r2 = r2[r0]
            r1[r0] = r2
            int r0 = r0 + 1
            goto L_0x032a
        L_0x036a:
            int[] r1 = r15.ao
            r1[r0] = r9
            int r0 = r0 + 1
            goto L_0x033a
        L_0x0371:
            int[] r0 = r15.at
            int[] r1 = r15.an
            int r1 = b(r1)
            r0[r11] = r1
            int[] r0 = r15.av
            r0[r11] = r11
            int[] r0 = r15.aw
            r0[r11] = r11
            r0 = r11
        L_0x0384:
            r1 = 28
            if (r0 < r1) goto L_0x038b
            r15.E = r11
            goto L_0x0359
        L_0x038b:
            int[] r1 = r15.aA
            r1[r0] = r11
            int r0 = r0 + 1
            goto L_0x0384
        L_0x0392:
            int r4 = r15.ai
            if (r4 != r13) goto L_0x03bb
            int[] r4 = r15.at
            r4 = r4[r11]
            if (r4 == r9) goto L_0x03bb
            android.graphics.Rect r4 = r15.al
            int r4 = a(r0, r1, r4)
            if (r4 != r12) goto L_0x03bb
            int[] r0 = r15.av
            r0 = r0[r11]
            if (r0 != 0) goto L_0x03b6
            int[] r0 = r15.av
            r0[r11] = r12
        L_0x03ae:
            r15.E = r11
            r0 = 4
            r15.ai = r0
            r0 = r11
            goto L_0x02a9
        L_0x03b6:
            int[] r0 = r15.av
            r0[r11] = r11
            goto L_0x03ae
        L_0x03bb:
            int r4 = r15.ai
            if (r4 != r13) goto L_0x02ef
            r15.j()
            r4 = r11
        L_0x03c3:
            r5 = 28
            if (r4 >= r5) goto L_0x02ef
            int[] r5 = r15.ay
            r5 = r5[r4]
            if (r5 != r12) goto L_0x03fb
            android.graphics.Rect r5 = new android.graphics.Rect
            r6 = r2[r4]
            r7 = r3[r4]
            r8 = r2[r4]
            int r8 = r8 + 53
            r9 = r3[r4]
            int r9 = r9 + 67
            r5.<init>(r6, r7, r8, r9)
            int r5 = a(r0, r1, r5)
            if (r5 != r12) goto L_0x03fb
            int[] r0 = r15.az
            r0 = r0[r4]
            if (r0 != 0) goto L_0x03f6
            int[] r0 = r15.az
            r0[r4] = r12
        L_0x03ee:
            r15.E = r11
            r0 = 4
            r15.ai = r0
            r0 = r11
            goto L_0x02a9
        L_0x03f6:
            int[] r0 = r15.az
            r0[r4] = r11
            goto L_0x03ee
        L_0x03fb:
            int r4 = r4 + 1
            goto L_0x03c3
        L_0x03fe:
            int r2 = r15.ai
            if (r2 != r13) goto L_0x02ef
            r2 = r11
        L_0x0403:
            r3 = 5
            if (r2 >= r3) goto L_0x02ef
            r3 = r11
        L_0x0407:
            r4 = 5
            if (r3 < r4) goto L_0x040d
            int r2 = r2 + 1
            goto L_0x0403
        L_0x040d:
            int[][] r4 = r15.aq
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 == r9) goto L_0x044d
            android.graphics.Rect r4 = new android.graphics.Rect
            int r5 = r3 * 54
            int r5 = r5 + 30
            int r6 = r2 * 68
            int r6 = r6 + 20
            int r7 = r3 * 54
            int r7 = r7 + 83
            int r8 = r2 * 68
            int r8 = r8 + 87
            r4.<init>(r5, r6, r7, r8)
            int r4 = a(r0, r1, r4)
            if (r4 != r12) goto L_0x044d
            int[][] r0 = r15.ar
            r0 = r0[r2]
            r0 = r0[r3]
            if (r0 != 0) goto L_0x0446
            int[][] r0 = r15.ar
            r0 = r0[r2]
            r0[r3] = r12
        L_0x043e:
            r15.E = r11
            r0 = 4
            r15.ai = r0
            r0 = r11
            goto L_0x02a9
        L_0x0446:
            int[][] r0 = r15.ar
            r0 = r0[r2]
            r0[r3] = r11
            goto L_0x043e
        L_0x044d:
            int r3 = r3 + 1
            goto L_0x0407
        L_0x0450:
            int r1 = r15.N
            if (r1 <= r0) goto L_0x0457
            r0 = r12
            goto L_0x02a9
        L_0x0457:
            int r1 = r15.O
            if (r1 >= r0) goto L_0x045e
            r0 = r13
            goto L_0x02a9
        L_0x045e:
            r0 = 2
            goto L_0x02a9
        L_0x0461:
            r1 = 2
            if (r0 != r1) goto L_0x046c
            int r0 = r15.J
            r15.H = r0
            r15.M = r11
            goto L_0x00ad
        L_0x046c:
            if (r0 != r13) goto L_0x00ad
            int r0 = r15.L
            r15.H = r0
            r0 = 40
            r15.M = r0
            goto L_0x00ad
        L_0x0478:
            r15.h()
            goto L_0x00c3
        L_0x047d:
            r15.l()
            goto L_0x00c3
        L_0x0482:
            r0 = 10
            java.lang.Thread.sleep(r0)     // Catch:{ Exception -> 0x0489 }
            goto L_0x00c5
        L_0x0489:
            r0 = move-exception
            goto L_0x00c5
        L_0x048c:
            r7 = r11
        L_0x048d:
            if (r7 < r4) goto L_0x0493
            int r6 = r6 + 1
            goto L_0x01a4
        L_0x0493:
            int r8 = r2 * r7
            float r8 = (float) r8
            int r9 = r1 * r6
            float r9 = (float) r9
            r10 = 0
            r0.drawBitmap(r3, r8, r9, r10)
            int r7 = r7 + 1
            goto L_0x048d
        L_0x04a0:
            r6 = r11
        L_0x04a1:
            if (r6 >= r5) goto L_0x01a6
            r7 = r11
        L_0x04a4:
            if (r7 < r4) goto L_0x04a9
            int r6 = r6 + 1
            goto L_0x04a1
        L_0x04a9:
            int r8 = r15.G
            int r8 = -r8
            int r9 = r2 * r7
            int r8 = r8 + r9
            float r8 = (float) r8
            int r9 = r1 * r6
            float r9 = (float) r9
            r10 = 0
            r0.drawBitmap(r3, r8, r9, r10)
            int r7 = r7 + 1
            goto L_0x04a4
        L_0x04ba:
            android.graphics.Rect r2 = new android.graphics.Rect
            int r3 = r15.H
            int r4 = r15.G
            int r3 = r3 - r4
            int r4 = r15.I
            int r5 = r15.H
            int r6 = r15.G
            int r5 = r5 - r6
            int r5 = r5 + 600
            int r6 = r15.I
            int r6 = r6 + 400
            r2.<init>(r3, r4, r5, r6)
            goto L_0x01cc
        L_0x04d3:
            r1 = 1120403456(0x42c80000, float:100.0)
            float r2 = r15.e
            float r1 = r1 * r2
            int r1 = (int) r1
            int r1 = r1 * 400
            int r1 = r1 / 100
            r2 = 1120403456(0x42c80000, float:100.0)
            float r3 = r15.e
            float r2 = r2 * r3
            int r2 = (int) r2
            int r2 = r2 * 600
            int r2 = r2 / 100
            android.graphics.Bitmap r3 = r15.k
            android.graphics.Bitmap r1 = android.graphics.Bitmap.createScaledBitmap(r3, r2, r1, r12)
            int r2 = com.kajirin.android.pyramid_breed.Pyramid_Breed.t
            if (r2 != 0) goto L_0x04fd
            int r2 = r15.H
            float r2 = (float) r2
            int r3 = r15.I
            float r3 = (float) r3
            r4 = 0
            r0.drawBitmap(r1, r2, r3, r4)
            goto L_0x01d2
        L_0x04fd:
            int r2 = r15.H
            int r3 = r15.G
            int r2 = r2 - r3
            float r2 = (float) r2
            int r3 = r15.I
            float r3 = (float) r3
            r4 = 0
            r0.drawBitmap(r1, r2, r3, r4)
            goto L_0x01d2
        L_0x050c:
            int r1 = r15.aa
            r2 = 140(0x8c, float:1.96E-43)
            int r1 = r1 - r2
            int r1 = r1 / 2
            int r2 = r15.G
            int r1 = r1 - r2
            int r2 = r15.ab
            r3 = 210(0xd2, float:2.94E-43)
            int r2 = r2 - r3
            int r2 = r2 / 2
            r14 = r2
            r2 = r1
            r1 = r14
            goto L_0x01f4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kajirin.android.pyramid_breed.n.a():void");
    }

    private static void a(int i2) {
        if (Pyramid_Breed.g == 1) {
            Pyramid_Breed.a(i2);
        }
    }

    private void a(int i2, int i3, int i4) {
        int i5;
        int i6;
        int i7;
        int i8;
        if (Pyramid_Breed.t != 0) {
            if (this.e == 1.0f) {
                int i9 = (this.G - i3) - this.I;
                i5 = i4 - this.H;
                i6 = i9;
            } else {
                i5 = ((i4 - this.H) * 100) / ((int) (this.e * 100.0f));
                i6 = (((this.G - i3) - this.I) * 100) / ((int) (this.e * 100.0f));
            }
            int i10 = this.G - i3;
            i7 = i4 - this.G;
            i8 = i10;
        } else if (this.e == 1.0f) {
            i8 = i4;
            i7 = i3;
            int i11 = i4 - this.I;
            i5 = i3 - this.H;
            i6 = i11;
        } else {
            int i12 = ((i3 - this.H) * 100) / ((int) (this.e * 100.0f));
            i8 = i4;
            i7 = i3;
            i5 = i12;
            i6 = ((i4 - this.I) * 100) / ((int) (this.e * 100.0f));
        }
        if (i2 == 0) {
            if (this.A == 0) {
                Pyramid_Breed.n = i5;
                Pyramid_Breed.o = i6;
                Pyramid_Breed.p = i2;
            }
            this.A = 1;
            for (int i13 = 0; i13 < 9; i13++) {
                this.x[i13] = 0;
            }
            this.x[4] = 3;
            this.y[4] = i7;
            this.z[4] = i8;
        } else if (i2 == 2) {
            if (this.A == 1) {
                this.B = 1;
            } else if (this.B == 0) {
                Pyramid_Breed.n = i5;
                Pyramid_Breed.o = i6;
                Pyramid_Breed.p = i2;
                this.A = 1;
                for (int i14 = 0; i14 < 9; i14++) {
                    this.x[i14] = 0;
                }
                this.x[4] = 3;
                this.y[4] = i7;
                this.z[4] = i8;
            }
        }
        if (i2 == 1) {
            if (this.A == 1) {
                this.x[4] = 0;
                this.x[5] = 3;
                this.y[5] = i7;
                this.z[5] = i8;
            }
            this.A = 0;
            this.B = 0;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    private void a(int i2, int i3, int i4, float f2) {
        Matrix matrix = new Matrix();
        matrix.postRotate((float) i4);
        matrix.postScale(f2, f2);
        this.v.drawBitmap(Bitmap.createBitmap(this.n, 0, 528, 53, 67, matrix, true), (float) i2, (float) i3, (Paint) null);
        this.D = 1;
    }

    private void a(int i2, int i3, int i4, int i5) {
        if (i5 == 1) {
            this.v.drawBitmap(this.q, new Rect(0, (i2 * 30) + 0, 30, (i2 * 30) + 30), new Rect(i3 + 0, i4 + 0, i3 + 30, i4 + 30), (Paint) null);
        } else {
            this.v.drawBitmap(this.p, new Rect(0, (i2 * 60) + 0, 60, (i2 * 60) + 60), new Rect(i3 + 0, i4 + 0, i3 + 60, i4 + 60), (Paint) null);
        }
        this.D = 1;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    private void a(int i2, int i3, int i4, int i5, int i6) {
        int i7;
        int i8;
        if (i2 < 52) {
            i7 = (i2 % 13) % 7;
            i8 = ((i2 / 13) * 2) + ((i2 % 13) / 7);
        } else if (i2 < 56) {
            i7 = 6;
            i8 = ((i2 - 52) * 2) + 1;
        } else {
            i7 = (i2 - 56) % 7;
            i8 = 8;
        }
        Rect rect = new Rect((i7 * 52) + 0, (i8 * 66) + 0, (i7 * 52) + 53, (i8 * 66) + 67);
        Rect rect2 = i5 == 1 ? new Rect(i3 + 0, (i4 + 0) - ((this.aD % 4) * 5), i3 + 53, (i4 + 67) - ((this.aD % 4) * 5)) : i6 == 1 ? new Rect(i3 + 0, (i4 + 0) - ((this.aD % 4) * 3), i3 + 53, (i4 + 67) - ((this.aD % 4) * 3)) : new Rect(i3 + 0, i4 + 0, i3 + 53, i4 + 67);
        if (i5 == 1) {
            Matrix matrix = new Matrix();
            matrix.postRotate(30.0f);
            matrix.postScale(1.2f, 1.2f);
            this.v.drawBitmap(Bitmap.createBitmap(this.o, (i7 * 52) + 0, (i8 * 66) + 0, 53, 67, matrix, true), (float) (i3 - 20), (float) (i4 - 20), (Paint) null);
        } else {
            this.v.drawBitmap(this.n, rect, rect2, (Paint) null);
            if (i6 == 1) {
                a((this.aD / 2) % 2, i3 + 10, (i4 + 20) - ((this.aD % 4) * 3), 1);
            }
        }
        this.D = 1;
    }

    private void a(Canvas canvas) {
        for (int i2 = 4; i2 < 6; i2++) {
            if (this.x[i2] > 0) {
                int[] iArr = this.x;
                iArr[i2] = iArr[i2] - 1;
                canvas.drawBitmap(this.w[i2], (float) (this.y[i2] - 35), (float) (this.z[i2] - 35), (Paint) null);
            }
        }
    }

    private static void a(int[] iArr) {
        int i2 = 0;
        for (int i3 = 0; i3 < 52; i3++) {
            if (iArr[i3] != 99) {
                iArr[i2] = iArr[i3];
                i2++;
            }
        }
        while (i2 < 52) {
            iArr[i2] = 99;
            i2++;
        }
    }

    private static void a(int[] iArr, int i2) {
        for (int i3 = 0; i3 < 51; i3++) {
            iArr[51 - i3] = iArr[50 - i3];
        }
        iArr[0] = i2;
    }

    private static int b(int[] iArr) {
        int i2 = 0;
        int i3 = iArr[0];
        iArr[0] = 99;
        for (int i4 = 0; i4 < 52; i4++) {
            if (iArr[i4] != 99) {
                iArr[i2] = iArr[i4];
                i2++;
            }
        }
        while (i2 < 52) {
            iArr[i2] = 99;
            i2++;
        }
        return i3;
    }

    private void b() {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false;
        Resources resources = getResources();
        this.l = BitmapFactory.decodeResource(resources, R.drawable.a00, options);
        Pyramid_Breed.j.incrementProgressBy(4);
        this.m = BitmapFactory.decodeResource(resources, R.drawable.b00, options);
        Pyramid_Breed.j.incrementProgressBy(4);
        Pyramid_Breed.j.incrementProgressBy(4);
        Pyramid_Breed.j.incrementProgressBy(4);
        this.n = BitmapFactory.decodeResource(resources, R.drawable.c02, options);
        Pyramid_Breed.j.incrementProgressBy(4);
        this.o = BitmapFactory.decodeResource(resources, R.drawable.c03, options);
        Pyramid_Breed.j.incrementProgressBy(4);
        this.p = BitmapFactory.decodeResource(resources, R.drawable.d00, options);
        Pyramid_Breed.j.incrementProgressBy(4);
        this.q = BitmapFactory.decodeResource(resources, R.drawable.d01, options);
        Pyramid_Breed.j.incrementProgressBy(4);
        this.r = BitmapFactory.decodeResource(resources, R.drawable.tile, options);
        Rect rect = new Rect(0, 0, 600, 400);
        this.v.drawBitmap(this.l, rect, rect, (Paint) null);
        if (this.k != null) {
            Canvas lockCanvas = this.h.lockCanvas();
            if (Pyramid_Breed.t == 1) {
                lockCanvas.rotate(90.0f, 0.0f, (float) this.G);
            }
            int i2 = (((int) (this.e * 100.0f)) * 100) / 100;
            int i3 = (((int) (this.e * 100.0f)) * 100) / 100;
            Bitmap createScaledBitmap = Bitmap.createScaledBitmap(this.r, i3, i2, true);
            int i4 = (this.F / i3) + 1;
            int i5 = (this.G / i2) + 1;
            if (Pyramid_Breed.t == 0) {
                for (int i6 = 0; i6 < i5; i6++) {
                    for (int i7 = 0; i7 < i4; i7++) {
                        lockCanvas.drawBitmap(createScaledBitmap, (float) (i3 * i7), (float) (i2 * i6), (Paint) null);
                    }
                }
            } else {
                for (int i8 = 0; i8 < i5; i8++) {
                    for (int i9 = 0; i9 < i4; i9++) {
                        lockCanvas.drawBitmap(createScaledBitmap, (float) ((-this.G) + (i3 * i9)), (float) (i2 * i8), (Paint) null);
                    }
                }
            }
            if (this.e == 1.0f) {
                lockCanvas.drawBitmap(this.k, new Rect(0, 0, 600, 400), Pyramid_Breed.t == 0 ? new Rect(this.H, this.I, this.H + 600, this.I + 400) : new Rect(this.H - this.G, this.I, (this.H - this.G) + 600, this.I + 400), (Paint) null);
            } else {
                Bitmap createScaledBitmap2 = Bitmap.createScaledBitmap(this.k, (((int) (this.e * 100.0f)) * 600) / 100, (((int) (this.e * 100.0f)) * 400) / 100, true);
                if (Pyramid_Breed.t == 0) {
                    lockCanvas.drawBitmap(createScaledBitmap2, (float) this.H, (float) this.I, (Paint) null);
                } else {
                    lockCanvas.drawBitmap(createScaledBitmap2, (float) (this.H - this.G), (float) this.I, (Paint) null);
                }
            }
            this.h.unlockCanvasAndPost(lockCanvas);
        }
    }

    private void b(int i2) {
        int[] iArr = new int[9];
        int[] iArr2 = new int[9];
        int i3 = i2;
        for (int i4 = 0; i4 < 9; i4++) {
            iArr[8 - i4] = i3 % 10;
            i3 /= 10;
        }
        boolean z2 = false;
        for (int i5 = 0; i5 < 8; i5++) {
            if (iArr[i5] != 0) {
                iArr2[i5] = 1;
                z2 = true;
            } else if (!z2) {
                iArr2[i5] = 0;
            } else {
                iArr2[i5] = 1;
            }
        }
        iArr2[8] = 1;
        this.v.drawBitmap(this.m, new Rect(120, 0, 160, 13), new Rect(409, 370, 449, 383), (Paint) null);
        for (int i6 = 0; i6 < 9; i6++) {
            this.v.drawBitmap(this.m, iArr2[i6] == 1 ? new Rect(iArr[i6] * 10, 0, (iArr[i6] * 10) + 10, 13) : new Rect(100, 0, 110, 13), new Rect((i6 * 10) + 449, 370, (i6 * 10) + 462, 383), (Paint) null);
        }
        this.D = 1;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kajirin.android.pyramid_breed.n.a(int, int, int, float):void
     arg types: [int, int, int, int]
     candidates:
      com.kajirin.android.pyramid_breed.n.a(int, int, int, int):void
      com.kajirin.android.pyramid_breed.n.a(int, int, int, float):void */
    private void b(int i2, int i3, int i4) {
        if (i4 == 0) {
            a(i2 + 410, i3 + 30, 330, 1.5f);
        } else if (i4 == 1) {
            a((i2 * 2) + 410, (i3 * 2) + 30, 290, 2.0f);
        } else if (i4 == 2) {
            a((i2 * 3) + 410, (i3 * 3) + 30, 240, 2.5f);
        } else {
            a((i2 * 4) + 410, (i3 * 4) + 30, 180, 3.0f);
        }
    }

    private void c() {
        int i2 = 410;
        int i3 = 30;
        int i4 = 0;
        for (int i5 = 0; i5 < 52; i5++) {
            if (this.an[i5] != 99) {
                i4++;
            }
        }
        int i6 = (i4 == 0 || i4 <= 3) ? i4 : (i4 / 14) + 3;
        if (i6 == 0 && this.aB > 0) {
            a(55, 410, 30, 0, 0);
        }
        int i7 = 0;
        while (i7 < i6) {
            a((this.aD / 10) + 56, i2, i3, 0, 0);
            i3 -= 2;
            i7++;
            i2 -= 2;
        }
        this.ak = new Rect(i2, i3, i2 + 53, i3 + 67);
        this.am = i6;
    }

    private void c(int i2) {
        this.C = System.currentTimeMillis() + ((long) ((i2 * 50) / 3));
    }

    private void d() {
        if (this.at[0] != 99) {
            a(this.at[0], 490, 30, this.av[0], this.aw[0]);
            this.al = new Rect(490, 30, 543, 97);
        }
    }

    private void d(int i2) {
        int[] iArr = {210, 180, 240, 150, 210, 270, 120, 180, 240, 300, 90, 150, 210, 270, 330, 60, 120, 180, 240, 300, 360, 30, 90, 150, 210, 270, 330, 390};
        int[] iArr2 = {30, 70, 70, 110, 110, 110, 150, 150, 150, 150, 190, 190, 190, 190, 190, 230, 230, 230, 230, 230, 230, 270, 270, 270, 270, 270, 270, 270};
        for (int i3 = 0; i3 < i2 + 1; i3++) {
            if (i3 < 21) {
                a((this.aD / 10) + 56, iArr[i3], iArr2[i3], 0, 0);
            } else {
                a(this.ax[i3], iArr[i3], iArr2[i3], 0, 0);
            }
        }
        this.D = 1;
    }

    private void e() {
        for (int i2 = 0; i2 < 52; i2++) {
            this.an[i2] = i2;
            this.ao[i2] = 99;
            this.at[i2] = 99;
            this.au[i2] = 0;
            this.av[i2] = 0;
            this.aw[i2] = 0;
        }
        for (int i3 = 0; i3 < 28; i3++) {
            this.ax[i3] = 99;
            this.ay[i3] = 0;
            this.az[i3] = 0;
            this.aA[i3] = 0;
        }
    }

    private void e(int i2) {
        for (int i3 = 0; i3 < i2 + 1; i3++) {
            int i4 = (i3 / 5) + 1;
            int i5 = (i3 % 5) + 1;
            for (int i6 = 0; i6 < i4; i6++) {
                for (int i7 = 0; i7 < i5; i7++) {
                    if (this.aq[i6][i7] != 99) {
                        a(this.aq[i6][i7], (i7 * 54) + 30, (i6 * 68) + 20, this.ar[i6][i7], this.as[i6][i7]);
                    }
                }
            }
        }
        this.D = 1;
    }

    private void f() {
        int a2 = a(0, 100) + 321;
        for (int i2 = 0; i2 < a2; i2++) {
            int a3 = a(0, 51);
            int a4 = a(0, 51);
            int i3 = this.an[a3];
            this.an[a3] = this.an[a4];
            this.an[a4] = i3;
        }
    }

    private void f(int i2) {
        if (i2 == 0) {
            a((this.aD / 4) % 2, 520, 120, 0);
        } else {
            a((this.aD / 4) % 4, 520, 120, 0);
        }
    }

    private void g() {
        int[] iArr = new int[52];
        for (int i2 = 0; i2 < 52; i2++) {
            iArr[i2] = this.an[i2];
        }
        int a2 = a(10, 40);
        int i3 = 0;
        int i4 = a2;
        while (i4 < 52) {
            this.an[i3] = iArr[i4];
            i4++;
            i3++;
        }
        int i5 = 0;
        while (i5 < a2) {
            this.an[i3] = iArr[i5];
            i5++;
            i3++;
        }
    }

    private void h() {
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        switch (this.ai) {
            case 0:
                e();
                f();
                g();
                a(this.an);
                if (this.E == 1) {
                    for (int i2 = 0; i2 < 28; i2++) {
                        this.ax[i2] = b(this.an);
                        this.az[i2] = 0;
                        this.aA[i2] = 0;
                    }
                } else {
                    for (int i3 = 0; i3 < 28; i3++) {
                        this.ax[i3] = 99;
                        this.az[i3] = 0;
                        this.aA[i3] = 0;
                    }
                }
                Rect rect = new Rect(0, 0, 600, 400);
                this.v.drawBitmap(this.l, rect, rect, (Paint) null);
                c();
                d();
                this.aB = 1;
                if (this.E == 1) {
                    this.a = 0;
                    this.E = 0;
                    this.ai = 2;
                    return;
                }
                this.E = 0;
                this.ai = 1;
                return;
            case 1:
                Rect rect2 = new Rect(0, 0, 600, 400);
                this.v.drawBitmap(this.l, rect2, rect2, (Paint) null);
                c();
                d();
                i();
                return;
            case 2:
                Rect rect3 = new Rect(0, 0, 600, 400);
                this.v.drawBitmap(this.l, rect3, rect3, (Paint) null);
                c();
                d();
                if (this.a < 2) {
                    if (this.E > 0) {
                        d(this.E - 1);
                    }
                    b(-50, 30, this.a);
                    this.a++;
                    c(1);
                } else {
                    this.a = 0;
                    d(this.E);
                    this.E++;
                    a(7);
                    c(1);
                }
                if (this.E > 27) {
                    this.E = 0;
                    this.ai = 3;
                    return;
                }
                return;
            case 3:
                Rect rect4 = new Rect(0, 0, 600, 400);
                this.v.drawBitmap(this.l, rect4, rect4, (Paint) null);
                c();
                d();
                i();
                f(Pyramid_Breed.c);
                return;
            case 4:
                switch (this.E) {
                    case 0:
                        Rect rect5 = new Rect(0, 0, 600, 400);
                        this.v.drawBitmap(this.l, rect5, rect5, (Paint) null);
                        c();
                        d();
                        i();
                        f(Pyramid_Breed.c);
                        a(7);
                        this.E = 1;
                        return;
                    case 1:
                        int i4 = 0;
                        for (int i5 = 0; i5 < 28; i5++) {
                            if (this.az[i5] == 1) {
                                i4++;
                            }
                        }
                        if (this.av[0] == 1) {
                            i4++;
                        }
                        if (i4 > 2) {
                            for (int i6 = 0; i6 < 28; i6++) {
                                this.az[i6] = 0;
                                this.aA[i6] = 0;
                            }
                        } else {
                            for (int i7 = 0; i7 < 28; i7++) {
                                if (this.az[i7] == 1 && (this.ax[i7] % 13) + 2 == 13) {
                                    a(this.ao, this.ax[i7]);
                                    this.ax[i7] = 99;
                                    this.az[i7] = 0;
                                    this.aA[i7] = 0;
                                    c(20);
                                }
                            }
                            if (this.av[0] == 1 && (this.at[0] % 13) + 2 == 13) {
                                a(this.ao, this.at[0]);
                                this.at[0] = 99;
                                this.av[0] = 0;
                                this.aw[0] = 0;
                                c(20);
                            }
                            if (i4 == 2) {
                                int i8 = 0;
                                for (int i9 = 0; i9 < 28; i9++) {
                                    if (this.az[i9] == 1) {
                                        i8 = i8 + (this.ax[i9] % 13) + 2;
                                    }
                                }
                                if (this.av[0] == 1) {
                                    i8 = i8 + (this.at[0] % 13) + 2;
                                }
                                if (i8 == 13 || i8 == 26) {
                                    for (int i10 = 0; i10 < 28; i10++) {
                                        if (this.az[i10] == 1) {
                                            a(this.ao, this.ax[i10]);
                                            this.ax[i10] = 99;
                                            this.az[i10] = 0;
                                            this.aA[i10] = 0;
                                        }
                                    }
                                    if (this.av[0] == 1) {
                                        a(this.ao, this.at[0]);
                                        this.at[0] = 99;
                                        this.av[0] = 0;
                                        this.aw[0] = 0;
                                    }
                                }
                                for (int i11 = 0; i11 < 28; i11++) {
                                    this.az[i11] = 0;
                                    this.aA[i11] = 0;
                                }
                                this.av[0] = 0;
                                this.aw[0] = 0;
                                c(20);
                            }
                        }
                        this.E = 0;
                        this.ai = 5;
                        return;
                    case 20:
                        Rect rect6 = new Rect(0, 0, 600, 400);
                        this.v.drawBitmap(this.l, rect6, rect6, (Paint) null);
                        c();
                        d();
                        i();
                        f(Pyramid_Breed.c);
                        if (this.a < 4) {
                            b(-10, 10, this.a);
                            this.a++;
                            if (this.a == 4) {
                                this.at[0] = b(this.an);
                                this.av[0] = 0;
                                this.aw[0] = 0;
                                for (int i12 = 0; i12 < 28; i12++) {
                                    this.aA[i12] = 0;
                                }
                            }
                            c(1);
                            return;
                        }
                        this.a = 0;
                        this.E = 0;
                        c(1);
                        return;
                    default:
                        return;
                }
            case 5:
                if (this.ax[0] == 99) {
                    int i13 = Pyramid_Breed.e + (this.aj * 8);
                    Pyramid_Breed.e = i13;
                    if (i13 > 999999999) {
                        Pyramid_Breed.e = 999999999;
                    }
                    this.aj = 0;
                    this.E = 0;
                    this.ai = 6;
                } else {
                    if (this.an[0] == 99 && this.aB == 0) {
                        j();
                        this.aC = k();
                        if (this.aC != 1) {
                            this.E = 0;
                            this.ai = 7;
                        }
                    }
                    this.E = 0;
                    this.ai = 3;
                }
                Rect rect7 = new Rect(0, 0, 600, 400);
                this.v.drawBitmap(this.l, rect7, rect7, (Paint) null);
                c();
                d();
                i();
                f(Pyramid_Breed.c);
                return;
            case 6:
                c(120);
                paint.setTextSize(36.0f);
                paint.setColor(-256);
                this.v.drawText("Game Over", 330.0f, 94.0f, paint);
                paint.setTextSize(48.0f);
                paint.setColor(-256);
                this.v.drawText("WIN !", 380.0f, 144.0f, paint);
                a(6);
                this.E = 0;
                this.ai = 1;
                return;
            case 7:
                c(120);
                paint.setTextSize(36.0f);
                paint.setColor(-65536);
                this.v.drawText("Game Over", 330.0f, 94.0f, paint);
                paint.setTextSize(48.0f);
                paint.setColor(-65536);
                this.v.drawText("LOST", 380.0f, 144.0f, paint);
                a(1);
                this.aj = 0;
                this.E = 0;
                this.ai = 1;
                return;
            default:
                return;
        }
    }

    private void i() {
        int[] iArr = {210, 180, 240, 150, 210, 270, 120, 180, 240, 300, 90, 150, 210, 270, 330, 60, 120, 180, 240, 300, 360, 30, 90, 150, 210, 270, 330, 390};
        int[] iArr2 = {30, 70, 70, 110, 110, 110, 150, 150, 150, 150, 190, 190, 190, 190, 190, 230, 230, 230, 230, 230, 230, 270, 270, 270, 270, 270, 270, 270};
        int[][] iArr3 = {new int[]{1, 2}, new int[]{3, 4}, new int[]{4, 5}, new int[]{6, 7}, new int[]{7, 8}, new int[]{8, 9}, new int[]{10, 11}, new int[]{11, 12}, new int[]{12, 13}, new int[]{13, 14}, new int[]{15, 16}, new int[]{16, 17}, new int[]{17, 18}, new int[]{18, 19}, new int[]{19, 20}, new int[]{21, 22}, new int[]{22, 23}, new int[]{23, 24}, new int[]{24, 25}, new int[]{25, 26}, new int[]{26, 27}};
        for (int i2 = 0; i2 < 21; i2++) {
            if (this.ax[iArr3[i2][0]] != 99 || this.ax[iArr3[i2][1]] != 99) {
                a((this.aD / 10) + 56, iArr[i2], iArr2[i2], this.az[i2], this.aA[i2]);
            } else if (this.ax[i2] != 99) {
                a(this.ax[i2], iArr[i2], iArr2[i2], this.az[i2], this.aA[i2]);
            }
        }
        for (int i3 = 0; i3 < 7; i3++) {
            if (this.ax[i3 + 21] != 99) {
                a(this.ax[i3 + 21], iArr[i3 + 21], iArr2[i3 + 21], this.az[i3 + 21], this.aA[i3 + 21]);
            }
        }
        this.D = 1;
    }

    private void j() {
        int[][] iArr = {new int[]{1, 2}, new int[]{3, 4}, new int[]{4, 5}, new int[]{6, 7}, new int[]{7, 8}, new int[]{8, 9}, new int[]{10, 11}, new int[]{11, 12}, new int[]{12, 13}, new int[]{13, 14}, new int[]{15, 16}, new int[]{16, 17}, new int[]{17, 18}, new int[]{18, 19}, new int[]{19, 20}, new int[]{21, 22}, new int[]{22, 23}, new int[]{23, 24}, new int[]{24, 25}, new int[]{25, 26}, new int[]{26, 27}};
        for (int i2 = 0; i2 < 28; i2++) {
            if (i2 < 21) {
                if (this.ax[iArr[i2][0]] != 99 || this.ax[iArr[i2][1]] != 99) {
                    this.ay[i2] = 0;
                } else if (this.ax[i2] != 99) {
                    this.ay[i2] = 1;
                } else {
                    this.ay[i2] = 0;
                }
            } else if (this.ax[i2] != 99) {
                this.ay[i2] = 1;
            } else {
                this.ay[i2] = 0;
            }
        }
        if (this.at[0] == 99) {
            this.au[0] = 0;
        } else {
            this.au[0] = 1;
        }
    }

    private int k() {
        for (int i2 = 0; i2 < 28; i2++) {
            if (this.ay[i2] == 1) {
                if ((this.ax[i2] % 13) + 2 == 13) {
                    return 1;
                }
                if (i2 < 27) {
                    for (int i3 = i2 + 1; i3 < 28; i3++) {
                        if (this.ay[i3] == 1 && ((this.ax[i2] % 13) + 2 + (this.ax[i3] % 13) + 2 == 13 || (this.ax[i2] % 13) + 2 + (this.ax[i3] % 13) + 2 == 26)) {
                            return 1;
                        }
                    }
                }
                if (this.au[0] == 1 && ((this.ax[i2] % 13) + 2 + (this.at[0] % 13) + 2 == 13 || (this.ax[i2] % 13) + 2 + (this.at[0] % 13) + 2 == 26)) {
                    return 1;
                }
            }
        }
        return 0;
    }

    private void l() {
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        switch (this.ai) {
            case 0:
                e();
                f();
                g();
                a(this.an);
                if (this.E == 1) {
                    for (int i2 = 0; i2 < 5; i2++) {
                        for (int i3 = 0; i3 < 5; i3++) {
                            this.aq[i2][i3] = b(this.an);
                            this.ar[i2][i3] = 0;
                            this.as[i2][i3] = 0;
                        }
                    }
                } else {
                    for (int i4 = 0; i4 < 5; i4++) {
                        for (int i5 = 0; i5 < 5; i5++) {
                            this.aq[i4][i5] = 99;
                            this.ar[i4][i5] = 0;
                            this.as[i4][i5] = 0;
                        }
                    }
                }
                Rect rect = new Rect(0, 0, 600, 400);
                this.v.drawBitmap(this.l, rect, rect, (Paint) null);
                c();
                d();
                this.aB = 0;
                if (this.E == 1) {
                    this.a = 0;
                    this.E = 0;
                    this.ai = 2;
                    return;
                }
                this.E = 0;
                this.ai = 1;
                return;
            case 1:
                Rect rect2 = new Rect(0, 0, 600, 400);
                this.v.drawBitmap(this.l, rect2, rect2, (Paint) null);
                c();
                d();
                m();
                return;
            case 2:
                Rect rect3 = new Rect(0, 0, 600, 400);
                this.v.drawBitmap(this.l, rect3, rect3, (Paint) null);
                c();
                if (this.a < 2) {
                    if (this.E > 0) {
                        e(this.E - 1);
                    }
                    b(-60, 20, this.a);
                    this.a++;
                    c(1);
                } else {
                    this.a = 0;
                    e(this.E);
                    this.E++;
                    a(7);
                    c(1);
                }
                if (this.E > 24) {
                    this.aC = n();
                    if (this.aC == 1) {
                        this.E = 0;
                        this.ai = 3;
                        return;
                    }
                    this.E = 0;
                    this.ai = 7;
                    return;
                }
                return;
            case 3:
                Rect rect4 = new Rect(0, 0, 600, 400);
                this.v.drawBitmap(this.l, rect4, rect4, (Paint) null);
                c();
                d();
                m();
                f(Pyramid_Breed.c);
                return;
            case 4:
                switch (this.E) {
                    case 0:
                        Rect rect5 = new Rect(0, 0, 600, 400);
                        this.v.drawBitmap(this.l, rect5, rect5, (Paint) null);
                        c();
                        d();
                        m();
                        f(Pyramid_Breed.c);
                        a(7);
                        this.E = 1;
                        return;
                    case 1:
                        int i6 = 0;
                        for (int i7 = 0; i7 < 5; i7++) {
                            for (int i8 = 0; i8 < 5; i8++) {
                                if (this.aq[i7][i8] != 99 && this.ar[i7][i8] == 1) {
                                    i6++;
                                }
                            }
                        }
                        if (i6 == 2) {
                            this.E = 2;
                            return;
                        }
                        if (i6 > 2) {
                            for (int i9 = 0; i9 < 5; i9++) {
                                for (int i10 = 0; i10 < 5; i10++) {
                                    this.ar[i9][i10] = 0;
                                    this.as[i9][i10] = 0;
                                }
                            }
                        }
                        this.E = 0;
                        this.ai = 5;
                        return;
                    case 2:
                        c(20);
                        this.aC = o();
                        if (this.aC == 1) {
                            for (int i11 = 0; i11 < 5; i11++) {
                                for (int i12 = 0; i12 < 5; i12++) {
                                    if (this.ar[i11][i12] == 1) {
                                        this.aq[i11][i12] = 99;
                                        this.ar[i11][i12] = 0;
                                    }
                                }
                            }
                            Rect rect6 = new Rect(0, 0, 600, 400);
                            this.v.drawBitmap(this.l, rect6, rect6, (Paint) null);
                            c();
                            d();
                            m();
                            f(Pyramid_Breed.c);
                            p();
                        }
                        for (int i13 = 0; i13 < 5; i13++) {
                            for (int i14 = 0; i14 < 5; i14++) {
                                this.ar[i13][i14] = 0;
                                this.as[i13][i14] = 0;
                            }
                        }
                        this.E = 0;
                        this.ai = 5;
                        return;
                    default:
                        return;
                }
            case 5:
                int i15 = 0;
                for (int i16 = 0; i16 < 5; i16++) {
                    for (int i17 = 0; i17 < 5; i17++) {
                        if (this.aq[i16][i17] != 99) {
                            i15++;
                        }
                    }
                }
                if (i15 == 0) {
                    int i18 = Pyramid_Breed.e + (this.aj * 8);
                    Pyramid_Breed.e = i18;
                    if (i18 > 999999999) {
                        Pyramid_Breed.e = 999999999;
                    }
                    this.aj = 0;
                    this.E = 0;
                    this.ai = 6;
                } else {
                    this.aC = n();
                    if (this.aC == 1) {
                        this.E = 0;
                        this.ai = 3;
                    } else {
                        this.E = 0;
                        this.ai = 7;
                    }
                }
                Rect rect7 = new Rect(0, 0, 600, 400);
                this.v.drawBitmap(this.l, rect7, rect7, (Paint) null);
                c();
                d();
                m();
                f(Pyramid_Breed.c);
                return;
            case 6:
                c(120);
                paint.setTextSize(36.0f);
                paint.setColor(-256);
                this.v.drawText("Game Over", 330.0f, 94.0f, paint);
                paint.setTextSize(48.0f);
                paint.setColor(-256);
                this.v.drawText("WIN !", 380.0f, 144.0f, paint);
                a(6);
                this.E = 0;
                this.ai = 1;
                return;
            case 7:
                c(120);
                paint.setTextSize(36.0f);
                paint.setColor(-65536);
                this.v.drawText("Game Over", 330.0f, 94.0f, paint);
                paint.setTextSize(48.0f);
                paint.setColor(-65536);
                this.v.drawText("LOST", 380.0f, 144.0f, paint);
                a(1);
                this.aj = 0;
                this.E = 0;
                this.ai = 1;
                return;
            default:
                return;
        }
    }

    private void m() {
        for (int i2 = 0; i2 < 5; i2++) {
            for (int i3 = 0; i3 < 5; i3++) {
                if (this.aq[i2][i3] != 99) {
                    a(this.aq[i2][i3], (i3 * 54) + 30, (i2 * 68) + 20, this.ar[i2][i3], this.as[i2][i3]);
                }
            }
        }
        this.D = 1;
    }

    private int n() {
        for (int i2 = 0; i2 < 5; i2++) {
            for (int i3 = 0; i3 < 4; i3++) {
                if (this.aq[i2][i3] % 13 == this.aq[i2][i3 + 1] % 13 && this.aq[i2][i3] != 99 && this.aq[i2][i3 + 1] != 99) {
                    return 1;
                }
            }
        }
        for (int i4 = 0; i4 < 4; i4++) {
            for (int i5 = 0; i5 < 5; i5++) {
                if (this.aq[i4][i5] % 13 == this.aq[i4 + 1][i5] % 13 && this.aq[i4][i5] != 99 && this.aq[i4 + 1][i5] != 99) {
                    return 1;
                }
            }
        }
        for (int i6 = 0; i6 < 4; i6++) {
            for (int i7 = 1; i7 < 5; i7++) {
                if (this.aq[i6][i7] % 13 == this.aq[i6 + 1][i7 - 1] % 13 && this.aq[i6][i7] != 99 && this.aq[i6 + 1][i7 - 1] != 99) {
                    return 1;
                }
            }
        }
        for (int i8 = 0; i8 < 4; i8++) {
            for (int i9 = 0; i9 < 4; i9++) {
                if (this.aq[i8][i9] % 13 == this.aq[i8 + 1][i9 + 1] % 13 && this.aq[i8][i9] != 99 && this.aq[i8 + 1][i9 + 1] != 99) {
                    return 1;
                }
            }
        }
        return 0;
    }

    private int o() {
        for (int i2 = 0; i2 < 5; i2++) {
            for (int i3 = 0; i3 < 4; i3++) {
                if (this.ar[i2][i3] == this.ar[i2][i3 + 1] && this.aq[i2][i3] % 13 == this.aq[i2][i3 + 1] % 13 && this.aq[i2][i3] != 99 && this.ar[i2][i3] == 1) {
                    return 1;
                }
            }
        }
        for (int i4 = 0; i4 < 4; i4++) {
            for (int i5 = 0; i5 < 5; i5++) {
                if (this.ar[i4][i5] == this.ar[i4 + 1][i5] && this.aq[i4][i5] % 13 == this.aq[i4 + 1][i5] % 13 && this.aq[i4][i5] != 99 && this.ar[i4][i5] == 1) {
                    return 1;
                }
            }
        }
        for (int i6 = 0; i6 < 4; i6++) {
            for (int i7 = 1; i7 < 5; i7++) {
                if (this.ar[i6][i7] == this.ar[i6 + 1][i7 - 1] && this.aq[i6][i7] % 13 == this.aq[i6 + 1][i7 - 1] % 13 && this.aq[i6][i7] != 99 && this.ar[i6][i7] == 1) {
                    return 1;
                }
            }
        }
        for (int i8 = 0; i8 < 4; i8++) {
            for (int i9 = 0; i9 < 4; i9++) {
                if (this.ar[i8][i9] == this.ar[i8 + 1][i9 + 1] && this.aq[i8][i9] % 13 == this.aq[i8 + 1][i9 + 1] % 13 && this.aq[i8][i9] != 99 && this.ar[i8][i9] == 1) {
                    return 1;
                }
            }
        }
        return 0;
    }

    private void p() {
        for (int i2 = 0; i2 < 25; i2++) {
            this.ap[i2] = 99;
        }
        int i3 = 0;
        for (int i4 = 0; i4 < 5; i4++) {
            for (int i5 = 0; i5 < 5; i5++) {
                if (this.aq[i4][i5] != 99) {
                    this.ap[i3] = this.aq[i4][i5];
                    i3++;
                }
            }
        }
        if (this.an[0] != 99) {
            this.ap[i3] = b(this.an);
            this.ap[i3 + 1] = b(this.an);
        }
        int i6 = 0;
        for (int i7 = 0; i7 < 5; i7++) {
            int i8 = 0;
            while (i8 < 5) {
                this.aq[i7][i8] = this.ap[i6];
                this.ar[i7][i8] = 0;
                this.as[i7][i8] = 0;
                i8++;
                i6++;
            }
        }
    }

    private static void q() {
        Pyramid_Breed.e += 300;
    }

    private void r() {
        Pyramid_Breed.c = 1;
        if (this.ai == 3) {
            switch (Pyramid_Breed.l) {
                case 1:
                    j();
                    for (int i2 = 0; i2 < 28; i2++) {
                        this.az[i2] = 0;
                        this.aA[i2] = 0;
                    }
                    this.av[0] = 0;
                    this.aw[0] = 0;
                    for (int i3 = 0; i3 < 28; i3++) {
                        if (this.ay[i3] == 1) {
                            if ((this.ax[i3] % 13) + 2 == 13) {
                                this.aA[i3] = 1;
                                return;
                            }
                            if (i3 < 27) {
                                for (int i4 = i3 + 1; i4 < 28; i4++) {
                                    if (this.ay[i4] == 1 && ((this.ax[i3] % 13) + 2 + (this.ax[i4] % 13) + 2 == 13 || (this.ax[i3] % 13) + 2 + (this.ax[i4] % 13) + 2 == 26)) {
                                        this.aA[i3] = 1;
                                        this.aA[i4] = 1;
                                        return;
                                    }
                                }
                            }
                            if (this.au[0] == 1 && ((this.ax[i3] % 13) + 2 + (this.at[0] % 13) + 2 == 13 || (this.ax[i3] % 13) + 2 + (this.at[0] % 13) + 2 == 26)) {
                                this.aA[i3] = 1;
                                this.aw[0] = 1;
                                return;
                            }
                        }
                    }
                    return;
                case 2:
                    for (int i5 = 0; i5 < 4; i5++) {
                        int i6 = 1;
                        while (i6 < 5) {
                            if (this.aq[i5][i6] % 13 != this.aq[i5 + 1][i6 - 1] % 13 || this.aq[i5][i6] == 99 || this.aq[i5 + 1][i6 - 1] == 99) {
                                i6++;
                            } else {
                                this.as[i5][i6] = 1;
                                this.as[i5 + 1][i6 - 1] = 1;
                                return;
                            }
                        }
                    }
                    for (int i7 = 0; i7 < 4; i7++) {
                        int i8 = 0;
                        while (i8 < 4) {
                            if (this.aq[i7][i8] % 13 != this.aq[i7 + 1][i8 + 1] % 13 || this.aq[i7][i8] == 99 || this.aq[i7 + 1][i8 + 1] == 99) {
                                i8++;
                            } else {
                                this.as[i7][i8] = 1;
                                this.as[i7 + 1][i8 + 1] = 1;
                                return;
                            }
                        }
                    }
                    for (int i9 = 0; i9 < 5; i9++) {
                        int i10 = 0;
                        while (i10 < 4) {
                            if (this.aq[i9][i10] % 13 != this.aq[i9][i10 + 1] % 13 || this.aq[i9][i10] == 99 || this.aq[i9][i10 + 1] == 99) {
                                i10++;
                            } else {
                                this.as[i9][i10] = 1;
                                this.as[i9][i10 + 1] = 1;
                                return;
                            }
                        }
                    }
                    for (int i11 = 0; i11 < 4; i11++) {
                        int i12 = 0;
                        while (i12 < 5) {
                            if (this.aq[i11][i12] % 13 != this.aq[i11 + 1][i12] % 13 || this.aq[i11][i12] == 99 || this.aq[i11 + 1][i12] == 99) {
                                i12++;
                            } else {
                                this.as[i11][i12] = 1;
                                this.as[i11 + 1][i12] = 1;
                                return;
                            }
                        }
                    }
                    return;
                default:
                    return;
            }
        }
    }

    public final boolean onKeyDown(int i2, KeyEvent keyEvent) {
        return super.onKeyDown(i2, keyEvent);
    }

    public final boolean onKeyUp(int i2, KeyEvent keyEvent) {
        return super.onKeyUp(i2, keyEvent);
    }

    public final boolean onTouchEvent(MotionEvent motionEvent) {
        this.Q = (int) motionEvent.getX();
        this.R = (int) motionEvent.getY();
        this.P = motionEvent.getAction();
        return true;
    }

    public final void run() {
        while (this.i != null && !Pyramid_Breed.i) {
            if (this.g == 0) {
                this.j = 1;
                this.P = -999;
                this.A = 0;
                this.B = 0;
                if (this.k == null) {
                    this.k = Bitmap.createBitmap(600, 400, Bitmap.Config.RGB_565);
                    this.v = new Canvas(this.k);
                    this.v.setBitmap(this.k);
                    this.v.drawColor(-1);
                }
                Pyramid_Breed.j.incrementProgressBy(20);
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inScaled = false;
                Resources resources = getResources();
                this.w[0] = BitmapFactory.decodeResource(resources, R.drawable.up, options);
                Pyramid_Breed.j.incrementProgressBy(3);
                this.w[1] = BitmapFactory.decodeResource(resources, R.drawable.down, options);
                Pyramid_Breed.j.incrementProgressBy(3);
                this.w[2] = BitmapFactory.decodeResource(resources, R.drawable.left, options);
                Pyramid_Breed.j.incrementProgressBy(3);
                this.w[3] = BitmapFactory.decodeResource(resources, R.drawable.right, options);
                Pyramid_Breed.j.incrementProgressBy(3);
                this.w[4] = BitmapFactory.decodeResource(resources, R.drawable.dot, options);
                Pyramid_Breed.j.incrementProgressBy(3);
                this.w[5] = BitmapFactory.decodeResource(resources, R.drawable.maru, options);
                Pyramid_Breed.j.incrementProgressBy(3);
                this.w[6] = BitmapFactory.decodeResource(resources, R.drawable.batsu, options);
                Pyramid_Breed.j.incrementProgressBy(3);
                this.w[7] = BitmapFactory.decodeResource(resources, R.drawable.sankaku, options);
                Pyramid_Breed.j.incrementProgressBy(3);
                this.w[8] = BitmapFactory.decodeResource(resources, R.drawable.sikaku, options);
                Pyramid_Breed.j.incrementProgressBy(3);
                this.s[0] = BitmapFactory.decodeResource(resources, R.drawable.p0, options);
                this.s[1] = BitmapFactory.decodeResource(resources, R.drawable.p1, options);
                this.s[2] = BitmapFactory.decodeResource(resources, R.drawable.p2, options);
                this.s[3] = BitmapFactory.decodeResource(resources, R.drawable.p3, options);
                for (int i2 = 0; i2 < 9; i2++) {
                    this.x[i2] = 0;
                    this.y[i2] = 0;
                    this.z[i2] = 0;
                }
                this.A = 0;
                this.B = 0;
                System.gc();
                this.D = 0;
                Pyramid_Breed.k = 0;
                this.E = 0;
                this.ai = 0;
                this.aj = 0;
                this.aD = 0;
                Pyramid_Breed.j.incrementProgressBy(10);
                if (Pyramid_Breed.f != Pyramid_Breed.b(Pyramid_Breed.e)) {
                    Pyramid_Breed.e = 0;
                    Pyramid_Breed.f = Pyramid_Breed.b(0);
                }
                if (Pyramid_Breed.l <= 0 || Pyramid_Breed.l > 2) {
                    Pyramid_Breed.l = 1;
                }
                b();
                this.C = System.currentTimeMillis() + 83;
                Pyramid_Breed.h = false;
                Pyramid_Breed.j.incrementProgressBy(10);
                this.g = 1;
                this.d = new c(this);
                this.c.postDelayed(this.d, 5000);
                this.j = 0;
            } else {
                a(this.P, this.Q, this.R);
                if (!Pyramid_Breed.h) {
                    this.j = 1;
                    a();
                    this.j = 0;
                }
            }
        }
    }

    public final void surfaceChanged(SurfaceHolder surfaceHolder, int i2, int i3, int i4) {
        this.S = i3;
        this.T = i4;
        this.aa = i4;
        this.ab = i3;
        this.F = this.S;
        this.G = this.T;
        this.U = this.f;
        float f2 = ((float) this.G) / 400.0f;
        float f3 = 1800.0f / ((float) this.F);
        if (f2 < f3) {
            this.U = f2;
        } else {
            this.U = f3;
        }
        this.V = (this.F - ((((int) (this.U * 100.0f)) * 600) / 100)) / 2;
        this.W = (this.G - ((((int) (this.U * 100.0f)) * 400) / 100)) / 2;
        if (this.V >= 0) {
            this.X = this.V;
            this.Y = this.V;
            this.Z = this.V;
        } else {
            this.X = this.V;
            this.Y = 0;
            this.Z = this.F - ((((int) (this.U * 100.0f)) * 600) / 100);
        }
        this.F = this.aa;
        this.G = this.ab;
        this.ac = ((float) this.F) / 604.0f;
        float f4 = ((float) this.G) / 404.0f;
        if (this.ac > f4) {
            this.ac = f4;
        }
        if (this.ac > 3.0f) {
            this.ac = 3.0f;
        } else if (this.ac < 0.4f) {
            this.ac = 0.4f;
        }
        this.ad = (this.F - ((((int) (this.ac * 100.0f)) * 600) / 100)) / 2;
        this.ae = (this.G - ((((int) (this.ac * 100.0f)) * 400) / 100)) / 2;
        if (this.ad >= 0) {
            this.af = this.ad;
            this.ag = this.ad;
            this.ah = this.ad;
        } else {
            this.af = this.ad;
            this.ag = 0;
            this.ah = this.F - ((((int) (this.ac * 100.0f)) * 600) / 100);
        }
        if (Pyramid_Breed.t == 0) {
            this.F = this.S;
            this.G = this.T;
            this.e = this.U;
            this.H = this.V;
            this.I = this.W;
            this.J = this.X;
            this.K = this.Y;
            this.L = this.Z;
        } else {
            this.F = this.aa;
            this.G = this.ab;
            this.e = this.ac;
            this.H = this.ad;
            this.I = this.ae;
            this.J = this.af;
            this.K = this.ag;
            this.L = this.ah;
        }
        this.M = 0;
        this.N = 200;
        this.O = 600 - this.N;
        this.A = 0;
        this.B = 0;
        setFocusable(true);
    }

    public final void surfaceCreated(SurfaceHolder surfaceHolder) {
        Pyramid_Breed.i = false;
        this.i = new Thread(this);
        this.i.start();
        setFocusable(true);
    }

    public final void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        Pyramid_Breed.h = true;
        Pyramid_Breed.i = true;
        try {
            this.i.join();
        } catch (Exception e2) {
        }
        this.i = null;
    }
}
