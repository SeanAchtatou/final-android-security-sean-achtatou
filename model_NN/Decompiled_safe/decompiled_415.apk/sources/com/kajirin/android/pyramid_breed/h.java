package com.kajirin.android.pyramid_breed;

import android.app.Activity;
import android.content.DialogInterface;

final class h implements DialogInterface.OnClickListener {
    private /* synthetic */ Pyramid_Breed a;
    private final /* synthetic */ Activity b;

    h(Pyramid_Breed pyramid_Breed, Activity activity) {
        this.a = pyramid_Breed;
        this.b = activity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.b.setResult(-1);
        try {
            this.a.finish();
        } catch (Exception e) {
        }
    }
}
