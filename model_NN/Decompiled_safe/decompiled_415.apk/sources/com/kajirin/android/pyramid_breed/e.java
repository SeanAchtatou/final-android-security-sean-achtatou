package com.kajirin.android.pyramid_breed;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.SharedPreferences;

final class e implements DialogInterface.OnClickListener {
    private /* synthetic */ Pyramid_Breed a;
    private final /* synthetic */ Activity b;

    e(Pyramid_Breed pyramid_Breed, Activity activity) {
        this.a = pyramid_Breed;
        this.b = activity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.b.setResult(-1);
        Pyramid_Breed.h = false;
        try {
            SharedPreferences sharedPreferences = this.a.L.getSharedPreferences(Pyramid_Breed.b(new byte[]{65, 104, 106, 110, 124}), 0);
            int i2 = sharedPreferences.getInt(Pyramid_Breed.b(new byte[]{79, 98, 103, 97}), 0);
            if (sharedPreferences.getInt(Pyramid_Breed.b(new byte[]{95, 120, 99, 76, Byte.MAX_VALUE, 120, 124}), 0) != Pyramid_Breed.b(i2)) {
                Pyramid_Breed.b(0);
                i2 = 0;
            }
            int i3 = i2 + 1500;
            int b2 = Pyramid_Breed.b(i3);
            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.putInt(Pyramid_Breed.b(new byte[]{79, 98, 103, 97}), i3);
            edit.putInt(Pyramid_Breed.b(new byte[]{95, 120, 99, 76, Byte.MAX_VALUE, 120, 124}), b2);
            edit.commit();
            int i4 = Pyramid_Breed.e - 1500;
            Pyramid_Breed.e = i4;
            Pyramid_Breed.f = Pyramid_Breed.b(i4);
            this.a.b();
        } catch (Exception e) {
        }
    }
}
