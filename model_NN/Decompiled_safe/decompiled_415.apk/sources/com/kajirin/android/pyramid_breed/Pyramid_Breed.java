package com.kajirin.android.pyramid_breed;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Locale;
import jp.co.nobot.libAdMaker.c;
import jp.co.nobot.libAdMaker.libAdMaker;

public class Pyramid_Breed extends Activity implements SensorEventListener, View.OnClickListener, c {
    private static final String B = System.getProperty("line.separator");
    private static int C = 0;
    private static int D = -1;
    private static int E;
    private static int F;
    private static MediaPlayer[] G = new MediaPlayer[9];
    public static RelativeLayout a;
    public static ProgressDialog b;
    public static int c = 0;
    public static int d = 1;
    public static int e;
    public static int f;
    public static int g = 1;
    public static boolean h;
    public static boolean i = false;
    public static ProgressDialog j;
    public static int k = 0;
    public static int l = 1;
    public static int m = 1;
    public static int n = 0;
    public static int o = 0;
    public static int p = -999;
    public static int q = 0;
    public static int r = 0;
    public static int s = 0;
    public static int t = 1;
    public static int u = 0;
    public static int v = 0;
    public static int w = 0;
    private libAdMaker A = null;
    private SensorManager H;
    private Sensor I;
    private float[] J = {0.0f, 0.0f, 0.0f};
    private float[] K = {0.0f, 0.0f, 0.0f};
    /* access modifiers changed from: private */
    public Context L;
    /* access modifiers changed from: private */
    public final Handler M = new Handler();
    /* access modifiers changed from: private */
    public final Runnable N = new d(this);
    private final int x = 1000;
    private Handler y = new Handler();
    private Runnable z;

    public static void a(int i2) {
        if (g == 1 && G[i2] != null) {
            MediaPlayer mediaPlayer = G[i2];
            mediaPlayer.setLooping(false);
            mediaPlayer.seekTo(0);
            mediaPlayer.start();
        }
    }

    private void a(Activity activity, String str, String str2) {
        h = true;
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(str);
        builder.setIcon((int) R.drawable.icon);
        builder.setMessage(str2);
        builder.setPositiveButton((String) getText(R.string.set_live1_7), new j(this, activity));
        builder.setNegativeButton((String) getText(R.string.set_live1_8), new k(this, activity));
        builder.setCancelable(false);
        builder.create();
        builder.show();
    }

    private void a(Activity activity, String str, String str2, String str3) {
        h = true;
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setIcon((int) R.drawable.icon);
        builder.setTitle(str);
        builder.setMessage(str2);
        builder.setPositiveButton(str3, new f(this, activity));
        builder.setCancelable(false);
        builder.create();
        builder.show();
    }

    public static int b(int i2) {
        String num = Integer.toString(i2);
        int length = num.length();
        int i3 = i2 < 200 ? 72 : i2 < 600 ? 23 : i2 < 1000 ? 235 : 542;
        for (int i4 = 0; i4 < length; i4++) {
            i3 = ((i3 + (num.charAt(i4) * num.charAt(i4))) % 100000) + (num.charAt(i4) * 15 * length);
        }
        return i3;
    }

    /* access modifiers changed from: private */
    public static String b(byte[] bArr) {
        byte[] bArr2 = new byte[bArr.length];
        byte b2 = 12;
        for (int i2 = 0; i2 < bArr.length; i2++) {
            bArr2[i2] = (byte) (bArr[i2] ^ b2);
            b2 = (byte) (b2 + 1);
            if (b2 > 120) {
                b2 = 12;
            }
        }
        try {
            return new String(bArr2, "UTF-8");
        } catch (UnsupportedEncodingException e2) {
            return new String(bArr2);
        }
    }

    private static void b(Activity activity, String str, String str2) {
        h = true;
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(str);
        builder.setIcon((int) R.drawable.icon);
        builder.setMessage(str2);
        builder.setPositiveButton(b(new byte[]{67, 70}), new g(activity));
        builder.setCancelable(false);
        builder.create();
        builder.show();
    }

    private static void c(int i2) {
        if (G[i2] != null && G[i2].isPlaying()) {
            G[i2].stop();
            try {
                G[i2].prepare();
            } catch (IOException | IllegalStateException e2) {
            }
        }
    }

    public final void a() {
        String language = Locale.getDefault().getLanguage();
        if (language.equals(Locale.JAPAN.toString()) || language.equals(Locale.JAPANESE.toString())) {
            a(this, b(new byte[]{-17, -113, -90, -20, -109, -72, -15, -112, -88}), b(new byte[]{-17, -114, -101, -20, -109, -69, -15, -112, -88, -14, -97, -97, -5, -104, -67, -8, -99, -78, -3, -99, -124, -62, -95, -112, -57, -89, -103, -60, -85, -107, -55, -88, -95, -50, -83, -84, -45, -78, -70, -43, -70, -112, -47, -127, -94, -38, -69, -82, -33, -65, -78, -36, -63, -25, -95, -62, -64, -90, -60, -52, -83, -10, -49, -93, -22, -52, -83, -50, -36, -78, -45, -47, -73, -41, -36, -76, -39, -25, -71, -38, -59, -66, -34, -35}), b(new byte[]{-21, -72, -116, -21, -86, -105}));
        } else {
            a(this, b(new byte[]{73, Byte.MAX_VALUE, 124, 96, 98}), b(new byte[]{88, 101, 107, 47, 118, 99, 119, 118, 52, 99, 115, 101, 107, 112, 117, 117, 60, 112, 107, 108, 84, 1, 74, 66, 82, 64, 6, 70, 70, 9, 99, 69, 88, 72, 92, 65, 85, 69, 18, 80, 91, 91, 88, 82, 91, 77, 83, 84, 82, 19}), b(new byte[]{73, 117, 103, 123}));
        }
    }

    public final void b() {
        SharedPreferences.Editor edit = getSharedPreferences(b(new byte[]{92, 116, 124, 110, 125, 120, 118, 76, 86, 103, 115, 114, 124}), 0).edit();
        F = b(E);
        f = b(e);
        edit.putInt(b(new byte[]{75, 108, 99, 106}), E);
        edit.putInt(b(new byte[]{95, 120, 99, 72, 113, 124, 119}), F);
        edit.putInt(b(new byte[]{79, 98, 103, 97}), e);
        edit.putInt(b(new byte[]{95, 120, 99, 76, Byte.MAX_VALUE, 120, 124}), f);
        edit.putInt(b(new byte[]{79, 101, 107, 108, 123, 67}), C);
        edit.commit();
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        if (keyEvent.getAction() == 0) {
            switch (keyEvent.getKeyCode()) {
                case 4:
                    h = true;
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle((String) getText(R.string.app_name));
                    builder.setIcon((int) R.drawable.icon);
                    builder.setMessage((String) getText(R.string.Pyramid28));
                    builder.setPositiveButton(b(new byte[]{67, 70}), new h(this, this));
                    builder.setNegativeButton(b(new byte[]{79, 76, 64, 76, 85, 93}), new l(this, this));
                    builder.setCancelable(false);
                    builder.create();
                    builder.show();
                    return true;
            }
        }
        return super.dispatchKeyEvent(keyEvent);
    }

    public void onAccuracyChanged(Sensor sensor, int i2) {
    }

    public void onClick(View view) {
        if (((String) view.getTag()).equals("123")) {
            if (a.getVisibility() == 0) {
                a.setVisibility(4);
            }
            w = 0;
            Intent intent = new Intent(this, Setting.class);
            intent.putExtra(b(new byte[]{120, 104, 118, 123}), b(new byte[]{88, 72, 93, 91, 48, 85, 83, 71, 85}));
            startActivityForResult(intent, 0);
        }
    }

    public void onCreate(Bundle bundle) {
        boolean z2;
        NetworkInfo activeNetworkInfo;
        String language;
        int i2;
        int i3;
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setFormat(-3);
        this.M.postDelayed(this.N, 10000);
        getWindow().addFlags(1024);
        C = 0;
        D = -1;
        h = true;
        ProgressDialog progressDialog = new ProgressDialog(this);
        j = progressDialog;
        progressDialog.setTitle(getText(R.string.app_name));
        j.setMessage(getText(R.string.Pyramid4));
        j.setIndeterminate(false);
        j.setProgressStyle(0);
        j.setIcon((int) R.drawable.icon);
        j.setMax(100);
        j.setCancelable(false);
        j.show();
        SharedPreferences sharedPreferences = getSharedPreferences(b(new byte[]{92, 116, 124, 110, 125, 120, 118, 76, 86, 103, 115, 114, 124}), 0);
        if (Setting.a(this, b(new byte[]{120, 98, 105, 104, 124, 116, 77, 99, 102, 112, 112, 114, 106, 124, 116, 120, 121}))) {
            g = 1;
        } else {
            g = 0;
        }
        if (Setting.a(this, b(new byte[]{120, 98, 105, 104, 124, 116, 77, 99, 102, 112, 112, 114, 106, 124, 116, 120, 121, 47}))) {
            u = 1;
        } else {
            u = 0;
        }
        int a2 = Setting.a(Setting.a(this, b(new byte[]{107, 108, 99, 106, 79, 97, 96, 118, 114, 112, 100, 114, 118, 122, Byte.MAX_VALUE}), b(new byte[]{107, 108, 99, 106, 32, 32})));
        l = a2;
        m = a2;
        d = Setting.b(Setting.a(this, b(new byte[]{96, 100, 120, 106, 79, 97, 96, 118, 114, 112, 100, 114, 118, 122, Byte.MAX_VALUE}), b(new byte[]{96, 100, 120, 106, 32, 32})));
        E = sharedPreferences.getInt(b(new byte[]{75, 108, 99, 106}), 0);
        F = sharedPreferences.getInt(b(new byte[]{95, 120, 99, 72, 113, 124, 119}), 0);
        e = sharedPreferences.getInt(b(new byte[]{79, 98, 103, 97}), 0);
        f = sharedPreferences.getInt(b(new byte[]{95, 120, 99, 76, Byte.MAX_VALUE, 120, 124}), 0);
        C = sharedPreferences.getInt(b(new byte[]{79, 101, 107, 108, 123, 67}), 0);
        if (E == 0) {
            F = b(E);
        }
        if (e == 0) {
            f = b(e);
        }
        if (u == 0) {
            t = 0;
            v = 0;
        } else {
            t = 1;
            v = 1;
        }
        j.incrementProgressBy(10);
        try {
            G[0] = MediaPlayer.create(this, (int) R.raw.s27000);
            j.incrementProgressBy(3);
            G[1] = MediaPlayer.create(this, (int) R.raw.s27001);
            j.incrementProgressBy(3);
            G[2] = MediaPlayer.create(this, (int) R.raw.s27002);
            j.incrementProgressBy(3);
            G[3] = MediaPlayer.create(this, (int) R.raw.s27003);
            j.incrementProgressBy(3);
            G[4] = MediaPlayer.create(this, (int) R.raw.s27004);
            j.incrementProgressBy(3);
            G[5] = MediaPlayer.create(this, (int) R.raw.s27005);
            j.incrementProgressBy(3);
            G[6] = MediaPlayer.create(this, (int) R.raw.s27006);
            j.incrementProgressBy(3);
            G[7] = MediaPlayer.create(this, (int) R.raw.s27007);
            j.incrementProgressBy(3);
            G[8] = MediaPlayer.create(this, (int) R.raw.s27008);
            j.incrementProgressBy(3);
        } catch (Exception e2) {
        }
        setVolumeControlStream(3);
        try {
            String[] strArr = getPackageManager().getPackageInfo(getApplicationContext().getPackageName(), 4096).requestedPermissions;
            int length = strArr.length;
            int i4 = 0;
            while (true) {
                if (i4 >= length) {
                    i2 = 0;
                    break;
                } else if (strArr[i4].equals("android.permission.INTERNET")) {
                    i2 = 0 + 1;
                    break;
                } else {
                    i4++;
                }
            }
            int i5 = 0;
            while (true) {
                if (i5 >= length) {
                    i3 = i2;
                    break;
                } else if (strArr[i5].equals("android.permission.ACCESS_NETWORK_STATE")) {
                    i3 = i2 + 1;
                    break;
                } else {
                    i5++;
                }
            }
            if (i3 == 2) {
                z2 = true;
                activeNetworkInfo = ((ConnectivityManager) getApplicationContext().getSystemService("connectivity")).getActiveNetworkInfo();
                if ((activeNetworkInfo == null && activeNetworkInfo.isConnected()) || !z2) {
                    language = Locale.getDefault().getLanguage();
                    if (!language.equals(Locale.JAPAN.toString()) || language.equals(Locale.JAPANESE.toString())) {
                        a(this, b(new byte[]{-17, -113, -90, -20, -109, -72, -15, -112, -88}), b(new byte[]{-17, -114, -101, -20, -109, -69, -15, -112, -88, -14, -97, -97, -5, -104, -67, -8, -99, -78, -3, -99, -124, -62, -95, -112, -57, -89, -103, -60, -85, -107, -55, -88, -95, -50, -83, -84, -45, -78, -70, -43, -70, -112, -47, -127, -94, -38, -69, -82, -33, -65, -78, -36, -63, -25, -95, -62, -64, -90, -60, -52, -83, -10, -49, -93, -22, -52, -83, -50, -36, -78, -45, -47, -73, -41, -36, -76, -39, -25, -71, -38, -59, -66, -34, -35}), b(new byte[]{-21, -72, -116, -21, -86, -105}));
                    } else {
                        a(this, b(new byte[]{73, Byte.MAX_VALUE, 124, 96, 98}), b(new byte[]{88, 101, 107, 47, 118, 99, 119, 118, 52, 99, 115, 101, 107, 112, 117, 117, 60, 112, 107, 108, 84, 1, 74, 66, 82, 64, 6, 70, 70, 9, 99, 69, 88, 72, 92, 65, 85, 69, 18, 80, 91, 91, 88, 82, 91, 77, 83, 84, 82, 19}), b(new byte[]{73, 117, 103, 123}));
                        return;
                    }
                } else {
                    LinearLayout linearLayout = new LinearLayout(this);
                    linearLayout.setBackgroundColor(-16777216);
                    linearLayout.setOrientation(1);
                    linearLayout.setGravity(51);
                    this.A = new libAdMaker(this);
                    this.A.setHorizontalScrollBarEnabled(false);
                    this.A.a(this);
                    this.A.a = b(new byte[]{58, 63, 54});
                    this.A.b = b(new byte[]{61, 52, 58, 62});
                    this.A.a(b(new byte[]{100, 121, 122, Byte.MAX_VALUE, 42, 62, 61, 122, 121, 116, 113, 114, 107, 55, 123, Byte.MAX_VALUE, 49, 112, Byte.MAX_VALUE, 116, 69, 83, 12, 74, 74, 67, 73, 8, 73, 89, 90, 88, 3, 70, 94, 86, 6, 71, 83, 7, 65, 71, 66, 66, 73, 23, 82, 79, 81, 81}));
                    this.A.setBackgroundColor(-16777216);
                    this.A.d();
                    linearLayout.addView(this.A, -2, (int) ((getApplicationContext().getResources().getDisplayMetrics().density * 50.0f) + 0.5f));
                    linearLayout.addView(new n(this), -2, -2);
                    RelativeLayout relativeLayout = new RelativeLayout(this);
                    a = relativeLayout;
                    relativeLayout.setBackgroundColor(Color.argb(0, 255, 255, 255));
                    a.setGravity(83);
                    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
                    layoutParams.addRule(12);
                    layoutParams.addRule(14);
                    Button button = new Button(this);
                    button.setText(getText(R.string.menu_item0));
                    button.setTag("123");
                    button.setOnClickListener(this);
                    button.setLayoutParams(layoutParams);
                    button.setBackgroundDrawable(getResources().getDrawable(R.drawable.color_stateful));
                    a.addView(button);
                    a.setVisibility(4);
                    FrameLayout frameLayout = new FrameLayout(this);
                    setContentView(frameLayout);
                    frameLayout.addView(linearLayout, new ViewGroup.LayoutParams(-2, -2));
                    frameLayout.addView(a, new ViewGroup.LayoutParams(-2, -2));
                    return;
                }
            }
        } catch (PackageManager.NameNotFoundException e3) {
        }
        z2 = false;
        activeNetworkInfo = ((ConnectivityManager) getApplicationContext().getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo == null && activeNetworkInfo.isConnected()) {
        }
        language = Locale.getDefault().getLanguage();
        if (!language.equals(Locale.JAPAN.toString())) {
        }
        a(this, b(new byte[]{-17, -113, -90, -20, -109, -72, -15, -112, -88}), b(new byte[]{-17, -114, -101, -20, -109, -69, -15, -112, -88, -14, -97, -97, -5, -104, -67, -8, -99, -78, -3, -99, -124, -62, -95, -112, -57, -89, -103, -60, -85, -107, -55, -88, -95, -50, -83, -84, -45, -78, -70, -43, -70, -112, -47, -127, -94, -38, -69, -82, -33, -65, -78, -36, -63, -25, -95, -62, -64, -90, -60, -52, -83, -10, -49, -93, -22, -52, -83, -50, -36, -78, -45, -47, -73, -41, -36, -76, -39, -25, -71, -38, -59, -66, -34, -35}), b(new byte[]{-21, -72, -116, -21, -86, -105}));
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, 0, 0, (int) R.string.menu_item0).setIcon(17301577);
        menu.add(0, 1, 0, (int) R.string.menu_item1).setIcon(17301568);
        menu.add(0, 2, 0, (int) R.string.menu_item2).setIcon(17301569);
        menu.add(0, 3, 0, (int) R.string.menu_item3).setIcon((int) R.drawable.favicon);
        menu.add(0, 4, 0, (int) R.string.menu_item4).setIcon(17301567);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        try {
            if (this.A != null) {
                this.A.e();
                this.A = null;
            }
        } catch (Exception e2) {
        }
        if (e > 999999999) {
            e = 999999999;
        }
        b();
        for (int i2 = 0; i2 < 9; i2++) {
            if (G[i2] != null) {
                c(i2);
                G[i2].release();
            }
        }
        if (this.I != null) {
            this.H.unregisterListener(this);
            this.I = null;
        }
        System.exit(0);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        String str;
        if (a.getVisibility() == 0) {
            a.setVisibility(4);
        }
        w = 0;
        switch (menuItem.getItemId()) {
            case 0:
                Intent intent = new Intent(this, Setting.class);
                intent.putExtra(b(new byte[]{120, 104, 118, 123}), b(new byte[]{88, 72, 93, 91, 48, 85, 83, 71, 85}));
                startActivityForResult(intent, 0);
                return true;
            case 1:
                startActivityForResult(new Intent(this, Help.class), 1);
                return true;
            case 2:
                try {
                    str = getPackageManager().getPackageInfo(getApplicationContext().getPackageName(), 0).versionName;
                } catch (PackageManager.NameNotFoundException e2) {
                    str = " ";
                }
                b(this, (String) getText(R.string.app_name), String.valueOf(b(new byte[]{90, 104, 124, 124, 121, 126, 124, 41, 52})) + str + B + B + b(new byte[]{79, 98, 126, 118, 98, 120, 117, 123, 96, 53, 36, 39, 41, 40, 54, 59, 95, 124, 112, 123, 89, 105, 77, 86, 87, 64}));
                return true;
            case 3:
                if (d < 2) {
                    if (e < 2000) {
                        try {
                            this.L = createPackageContext(b(new byte[]{111, 98, 99, 33, 123, 112, 120, 122, 102, 124, 120, 57, 121, 119, 126, 105, 115, 116, 122, 49, 77, 68, 70, 66, 72, 78, 79, 73, 79, 80, 69}), 4);
                            b(this, (String) getText(R.string.set_live1_1), (String) getText(R.string.set_live1_2));
                        } catch (Exception e3) {
                            a(this, (String) getText(R.string.set_live1_1), (String) getText(R.string.set_live1_3));
                        }
                    } else {
                        try {
                            this.L = createPackageContext(b(new byte[]{111, 98, 99, 33, 123, 112, 120, 122, 102, 124, 120, 57, 121, 119, 126, 105, 115, 116, 122, 49, 77, 68, 70, 66, 72, 78, 79, 73, 79, 80, 69}), 4);
                            h = true;
                            AlertDialog.Builder builder = new AlertDialog.Builder(this);
                            builder.setTitle((String) getText(R.string.set_live1_4));
                            builder.setIcon((int) R.drawable.icon);
                            builder.setMessage((String) getText(R.string.set_live1_5));
                            builder.setPositiveButton((String) getText(R.string.set_live1_7), new e(this, this));
                            builder.setNegativeButton((String) getText(R.string.set_live1_8), new i(this, this));
                            builder.setCancelable(false);
                            builder.create();
                            builder.show();
                        } catch (Exception e4) {
                            a(this, (String) getText(R.string.set_live1_1), (String) getText(R.string.set_live1_3));
                        }
                    }
                }
                return true;
            case 4:
                try {
                    this.L = createPackageContext(b(new byte[]{111, 98, 99, 33, 123, 112, 120, 122, 102, 124, 120, 57, 121, 119, 126, 105, 115, 116, 122, 49, 77, 68, 70, 66, 72, 78, 79, 73, 79, 80, 69}), 4);
                    if (d < 2) {
                        this.L = createPackageContext(b(new byte[]{111, 98, 99, 33, 123, 112, 120, 122, 102, 124, 120, 57, 121, 119, 126, 105, 115, 116, 122, 49, 77, 68, 70, 66, 72, 78, 79, 73, 79, 80, 69}), 4);
                    }
                    Toast.makeText(getApplicationContext(), (String) getText(R.string.set_live1_6), 1).show();
                    Intent intent2 = new Intent();
                    intent2.setAction("android.service.wallpaper.LIVE_WALLPAPER_CHOOSER");
                    startActivityForResult(intent2, 1);
                } catch (Exception e5) {
                    a(this, (String) getText(R.string.set_live1_1), (String) getText(R.string.set_live1_3));
                }
                return true;
            default:
                return true;
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        try {
            if (this.A != null) {
                this.A.f();
            }
        } catch (Exception e2) {
        }
        h = true;
        c(0);
        c(1);
        c(2);
        if (this.I != null) {
            this.H.unregisterListener(this);
            this.I = null;
        }
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
        ProgressDialog progressDialog = new ProgressDialog(this);
        b = progressDialog;
        progressDialog.setTitle(getText(R.string.app_name));
        b.setIndeterminate(false);
        b.setProgressStyle(0);
        b.setCancelable(false);
        b.show();
        this.z = new m(this);
        this.y.postDelayed(this.z, 1000);
        try {
            if (this.A != null) {
                this.A.d();
            }
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        h = false;
        this.H = (SensorManager) getSystemService("sensor");
        List<Sensor> sensorList = this.H.getSensorList(1);
        if (sensorList.size() > 0) {
            this.I = sensorList.get(0);
        }
        if (this.I != null) {
            this.H.registerListener(this, this.I, 2);
        }
        try {
            if (this.A != null) {
                this.A.d();
            }
        } catch (Exception e2) {
        }
    }

    public void onSensorChanged(SensorEvent sensorEvent) {
        float[] fArr = sensorEvent.values;
        this.J[0] = (fArr[0] * 0.1f) + (this.J[0] * 0.9f);
        this.J[1] = (fArr[1] * 0.1f) + (this.J[1] * 0.9f);
        this.J[2] = (fArr[2] * 0.1f) + (this.J[2] * 0.9f);
        this.K[0] = fArr[0] - this.J[0];
        this.K[1] = fArr[1] - this.J[1];
        this.K[2] = fArr[2] - this.J[2];
        v = 0;
        if (t == 0) {
            if (fArr[0] < -8.0f) {
                v = 1;
            } else if (fArr[0] > 8.0f) {
                v = 1;
            }
        } else if (fArr[0] < -2.0f) {
            v = 1;
        } else if (fArr[0] > 2.0f) {
            v = 1;
        }
        if (u == 0) {
            if (t != 0) {
                s = 1;
            }
            t = 0;
        } else if (fArr[2] < 8.0f && t != v) {
            t = v;
            s = 1;
        }
        q = 2;
        if (t == 0) {
            if (fArr[0] < -2.0f) {
                q = 1;
            } else if (fArr[0] > 2.0f) {
                q = 3;
            }
        }
        if ((Math.abs(this.K[0]) + Math.abs(this.K[1])) + Math.abs(this.K[2]) > 15.0f) {
            k = 1;
        }
    }
}
