package com.kajirin.android.pyramid_breed;

import android.content.Context;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;

public class Setting extends PreferenceActivity {
    private static int b;
    /* access modifiers changed from: private */
    public static String[] c;
    /* access modifiers changed from: private */
    public static String[] d;
    private int a;
    private int e;
    private Preference.OnPreferenceChangeListener f = new a(this);
    private Preference.OnPreferenceChangeListener g = new b(this);

    public static int a(String str) {
        if (str.equals("game01")) {
            return 1;
        }
        return str.equals("game02") ? 2 : 1;
    }

    public static String a(Context context, String str, String str2) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(str, str2);
    }

    public static boolean a(Context context, String str) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(str, true);
    }

    public static int b(String str) {
        return str.equals("live01") ? 1 : 1;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        c = new String[]{" ", (String) getText(R.string.game01), (String) getText(R.string.game02)};
        d = new String[]{" ", (String) getText(R.string.live01)};
        setVolumeControlStream(3);
        this.a = a(a(this, "game_preference", "game01"));
        this.e = b(a(this, "live_preference", "live01"));
        b = Pyramid_Breed.u;
        addPreferencesFromResource(R.xml.preferences);
        getPreferenceManager().findPreference("game_preference").setSummary(c[this.a]);
        ((ListPreference) findPreference("game_preference")).setOnPreferenceChangeListener(this.f);
        getPreferenceManager().findPreference("live_preference").setSummary(d[this.e]);
        ((ListPreference) findPreference("live_preference")).setOnPreferenceChangeListener(this.g);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (a(this, "toggle_preference")) {
            Pyramid_Breed.g = 1;
        } else {
            Pyramid_Breed.g = 0;
        }
        if (a(this, "toggle_preference2")) {
            Pyramid_Breed.u = 1;
        } else {
            Pyramid_Breed.u = 0;
        }
        if (b != Pyramid_Breed.u) {
            if (Pyramid_Breed.u == 0) {
                if (Pyramid_Breed.t != 0) {
                    Pyramid_Breed.s = 1;
                }
                Pyramid_Breed.t = 0;
            } else if (Pyramid_Breed.t != Pyramid_Breed.v) {
                Pyramid_Breed.t = Pyramid_Breed.v;
                Pyramid_Breed.s = 1;
            }
        }
        Pyramid_Breed.m = a(a(this, "game_preference", "game01"));
        if (this.a != Pyramid_Breed.m) {
            Pyramid_Breed.c = 0;
            Pyramid_Breed.r = 1;
        }
        Pyramid_Breed.d = b(a(this, "live_preference", "live01"));
    }
}
