package com.kajirin.android.pyramid_breed;

import android.preference.Preference;

final class a implements Preference.OnPreferenceChangeListener {
    private /* synthetic */ Setting a;

    a(Setting setting) {
        this.a = setting;
    }

    public final boolean onPreferenceChange(Preference preference, Object obj) {
        Pyramid_Breed.m = Setting.a((String) obj);
        this.a.getPreferenceManager().findPreference("game_preference").setSummary(Setting.c[Pyramid_Breed.m]);
        return true;
    }
}
