package com.box2d;

public class b2MyContactListener {
    protected boolean swigCMemOwn;
    private int swigCPtr;

    protected b2MyContactListener(int cPtr, boolean cMemoryOwn) {
        this.swigCMemOwn = cMemoryOwn;
        this.swigCPtr = cPtr;
    }

    protected static int getCPtr(b2MyContactListener obj) {
        if (obj == null) {
            return 0;
        }
        return obj.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public synchronized void delete() {
        if (this.swigCPtr != 0) {
            if (this.swigCMemOwn) {
                this.swigCMemOwn = false;
                Box2DWrapJNI.delete_b2MyContactListener(this.swigCPtr);
            }
            this.swigCPtr = 0;
        }
    }

    public b2MyContactListener() {
        this(Box2DWrapJNI.new_b2MyContactListener(), true);
    }

    public void initiate(int maxContactsCount, int maxSensorsCount) {
        Box2DWrapJNI.b2MyContactListener_initiate(this.swigCPtr, maxContactsCount, maxSensorsCount);
    }

    public void put(int index, b2ContactWrapper pContact) {
        Box2DWrapJNI.b2MyContactListener_put__SWIG_0(this.swigCPtr, index, b2ContactWrapper.getCPtr(pContact));
    }

    public void put(int index, b2SensorContactWrapper pSensor) {
        Box2DWrapJNI.b2MyContactListener_put__SWIG_1(this.swigCPtr, index, b2SensorContactWrapper.getCPtr(pSensor));
    }

    public void clear() {
        Box2DWrapJNI.b2MyContactListener_clear(this.swigCPtr);
    }

    public int getContactCount() {
        return Box2DWrapJNI.b2MyContactListener_getContactCount(this.swigCPtr);
    }

    public int getSensorContactCount() {
        return Box2DWrapJNI.b2MyContactListener_getSensorContactCount(this.swigCPtr);
    }

    public void setImpulseThreshold(float threshold) {
        Box2DWrapJNI.b2MyContactListener_setImpulseThreshold(this.swigCPtr, threshold);
    }
}
