package com.box2d;

public class b2World {
    protected boolean swigCMemOwn;
    private int swigCPtr;

    protected b2World(int cPtr, boolean cMemoryOwn) {
        this.swigCMemOwn = cMemoryOwn;
        this.swigCPtr = cPtr;
    }

    protected static int getCPtr(b2World obj) {
        if (obj == null) {
            return 0;
        }
        return obj.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public synchronized void delete() {
        if (this.swigCPtr != 0) {
            if (this.swigCMemOwn) {
                this.swigCMemOwn = false;
                Box2DWrapJNI.delete_b2World(this.swigCPtr);
            }
            this.swigCPtr = 0;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.box2d.b2World.<init>(int, boolean):void
     arg types: [int, int]
     candidates:
      com.box2d.b2World.<init>(com.box2d.b2Vec2, boolean):void
      com.box2d.b2World.<init>(int, boolean):void */
    public b2World(b2Vec2 gravity, boolean doSleep) {
        this(Box2DWrapJNI.new_b2World(b2Vec2.getCPtr(gravity), doSleep), true);
    }

    public b2Body CreateBody(b2BodyDef def) {
        int cPtr = Box2DWrapJNI.b2World_CreateBody(this.swigCPtr, b2BodyDef.getCPtr(def));
        if (cPtr == 0) {
            return null;
        }
        return new b2Body(cPtr, false);
    }

    public void DestroyBody(b2Body body) {
        Box2DWrapJNI.b2World_DestroyBody(this.swigCPtr, b2Body.getCPtr(body));
    }

    public b2Joint CreateJoint(b2JointDef def) {
        int cPtr = Box2DWrapJNI.b2World_CreateJoint(this.swigCPtr, b2JointDef.getCPtr(def));
        if (cPtr == 0) {
            return null;
        }
        return new b2Joint(cPtr, false);
    }

    public void DestroyJoint(b2Joint joint) {
        Box2DWrapJNI.b2World_DestroyJoint(this.swigCPtr, b2Joint.getCPtr(joint));
    }

    public void Step(float timeStep, int velocityIterations, int positionIterations) {
        Box2DWrapJNI.b2World_Step(this.swigCPtr, timeStep, velocityIterations, positionIterations);
    }

    public void ClearForces() {
        Box2DWrapJNI.b2World_ClearForces(this.swigCPtr);
    }

    public void SetWarmStarting(boolean flag) {
        Box2DWrapJNI.b2World_SetWarmStarting(this.swigCPtr, flag);
    }

    public void SetContinuousPhysics(boolean flag) {
        Box2DWrapJNI.b2World_SetContinuousPhysics(this.swigCPtr, flag);
    }

    public int GetProxyCount() {
        return Box2DWrapJNI.b2World_GetProxyCount(this.swigCPtr);
    }

    public int GetBodyCount() {
        return Box2DWrapJNI.b2World_GetBodyCount(this.swigCPtr);
    }

    public int GetJointCount() {
        return Box2DWrapJNI.b2World_GetJointCount(this.swigCPtr);
    }

    public int GetContactCount() {
        return Box2DWrapJNI.b2World_GetContactCount(this.swigCPtr);
    }

    public void SetGravity(b2Vec2 gravity) {
        Box2DWrapJNI.b2World_SetGravity(this.swigCPtr, b2Vec2.getCPtr(gravity));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.box2d.b2Vec2.<init>(int, boolean):void
     arg types: [int, int]
     candidates:
      com.box2d.b2Vec2.<init>(float, float):void
      com.box2d.b2Vec2.<init>(int, boolean):void */
    public b2Vec2 GetGravity() {
        return new b2Vec2(Box2DWrapJNI.b2World_GetGravity(this.swigCPtr), true);
    }

    public boolean IsLocked() {
        return Box2DWrapJNI.b2World_IsLocked(this.swigCPtr);
    }

    public void SetAutoClearForces(boolean flag) {
        Box2DWrapJNI.b2World_SetAutoClearForces(this.swigCPtr, flag);
    }

    public boolean GetAutoClearForces() {
        return Box2DWrapJNI.b2World_GetAutoClearForces(this.swigCPtr);
    }
}
