package com.box2d;

public class b2Fixture {
    protected boolean swigCMemOwn;
    private int swigCPtr;

    protected b2Fixture(int cPtr, boolean cMemoryOwn) {
        this.swigCMemOwn = cMemoryOwn;
        this.swigCPtr = cPtr;
    }

    protected static int getCPtr(b2Fixture obj) {
        if (obj == null) {
            return 0;
        }
        return obj.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public synchronized void delete() {
        if (this.swigCPtr != 0) {
            if (this.swigCMemOwn) {
                this.swigCMemOwn = false;
                Box2DWrapJNI.delete_b2Fixture(this.swigCPtr);
            }
            this.swigCPtr = 0;
        }
    }

    public void SetSensor(boolean sensor) {
        Box2DWrapJNI.b2Fixture_SetSensor(this.swigCPtr, sensor);
    }

    public boolean IsSensor() {
        return Box2DWrapJNI.b2Fixture_IsSensor(this.swigCPtr);
    }

    public void setFilter(int categoryBits, int maskBits, int groupIndex) {
        Box2DWrapJNI.b2Fixture_setFilter(this.swigCPtr, categoryBits, maskBits, groupIndex);
    }

    public void SetFilterData(b2Filter filter) {
        Box2DWrapJNI.b2Fixture_SetFilterData(this.swigCPtr, b2Filter.getCPtr(filter));
    }

    public b2Filter GetFilterData() {
        return new b2Filter(Box2DWrapJNI.b2Fixture_GetFilterData(this.swigCPtr), false);
    }

    public b2Body GetBody() {
        int cPtr = Box2DWrapJNI.b2Fixture_GetBody__SWIG_0(this.swigCPtr);
        if (cPtr == 0) {
            return null;
        }
        return new b2Body(cPtr, false);
    }

    public boolean TestPoint(b2Vec2 p) {
        return Box2DWrapJNI.b2Fixture_TestPoint(this.swigCPtr, b2Vec2.getCPtr(p));
    }

    public void GetMassData(b2MassData massData) {
        Box2DWrapJNI.b2Fixture_GetMassData(this.swigCPtr, b2MassData.getCPtr(massData));
    }

    public void SetDensity(float density) {
        Box2DWrapJNI.b2Fixture_SetDensity(this.swigCPtr, density);
    }

    public float GetDensity() {
        return Box2DWrapJNI.b2Fixture_GetDensity(this.swigCPtr);
    }

    public float GetFriction() {
        return Box2DWrapJNI.b2Fixture_GetFriction(this.swigCPtr);
    }

    public void SetFriction(float friction) {
        Box2DWrapJNI.b2Fixture_SetFriction(this.swigCPtr, friction);
    }

    public float GetRestitution() {
        return Box2DWrapJNI.b2Fixture_GetRestitution(this.swigCPtr);
    }

    public void SetRestitution(float restitution) {
        Box2DWrapJNI.b2Fixture_SetRestitution(this.swigCPtr, restitution);
    }
}
