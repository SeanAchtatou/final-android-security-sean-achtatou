package com.box2d;

public class b2MouseJointDef extends b2JointDef {
    private int swigCPtr;

    protected b2MouseJointDef(int cPtr, boolean cMemoryOwn) {
        super(Box2DWrapJNI.SWIGb2MouseJointDefUpcast(cPtr), cMemoryOwn);
        this.swigCPtr = cPtr;
    }

    protected static int getCPtr(b2MouseJointDef obj) {
        if (obj == null) {
            return 0;
        }
        return obj.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public synchronized void delete() {
        if (this.swigCPtr != 0) {
            if (this.swigCMemOwn) {
                this.swigCMemOwn = false;
                Box2DWrapJNI.delete_b2MouseJointDef(this.swigCPtr);
            }
            this.swigCPtr = 0;
        }
        super.delete();
    }

    public b2MouseJointDef() {
        this(Box2DWrapJNI.new_b2MouseJointDef(), true);
    }

    public void setTarget(b2Vec2 value) {
        Box2DWrapJNI.b2MouseJointDef_target_set(this.swigCPtr, b2Vec2.getCPtr(value));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.box2d.b2Vec2.<init>(int, boolean):void
     arg types: [int, int]
     candidates:
      com.box2d.b2Vec2.<init>(float, float):void
      com.box2d.b2Vec2.<init>(int, boolean):void */
    public b2Vec2 getTarget() {
        int cPtr = Box2DWrapJNI.b2MouseJointDef_target_get(this.swigCPtr);
        if (cPtr == 0) {
            return null;
        }
        return new b2Vec2(cPtr, false);
    }

    public void setMaxForce(float value) {
        Box2DWrapJNI.b2MouseJointDef_maxForce_set(this.swigCPtr, value);
    }

    public float getMaxForce() {
        return Box2DWrapJNI.b2MouseJointDef_maxForce_get(this.swigCPtr);
    }

    public void setFrequencyHz(float value) {
        Box2DWrapJNI.b2MouseJointDef_frequencyHz_set(this.swigCPtr, value);
    }

    public float getFrequencyHz() {
        return Box2DWrapJNI.b2MouseJointDef_frequencyHz_get(this.swigCPtr);
    }

    public void setDampingRatio(float value) {
        Box2DWrapJNI.b2MouseJointDef_dampingRatio_set(this.swigCPtr, value);
    }

    public float getDampingRatio() {
        return Box2DWrapJNI.b2MouseJointDef_dampingRatio_get(this.swigCPtr);
    }
}
