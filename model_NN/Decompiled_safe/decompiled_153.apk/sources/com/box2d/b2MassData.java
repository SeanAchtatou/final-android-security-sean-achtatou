package com.box2d;

public class b2MassData {
    protected boolean swigCMemOwn;
    private int swigCPtr;

    protected b2MassData(int cPtr, boolean cMemoryOwn) {
        this.swigCMemOwn = cMemoryOwn;
        this.swigCPtr = cPtr;
    }

    protected static int getCPtr(b2MassData obj) {
        if (obj == null) {
            return 0;
        }
        return obj.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public synchronized void delete() {
        if (this.swigCPtr != 0) {
            if (this.swigCMemOwn) {
                this.swigCMemOwn = false;
                Box2DWrapJNI.delete_b2MassData(this.swigCPtr);
            }
            this.swigCPtr = 0;
        }
    }

    public void setMass(float value) {
        Box2DWrapJNI.b2MassData_mass_set(this.swigCPtr, value);
    }

    public float getMass() {
        return Box2DWrapJNI.b2MassData_mass_get(this.swigCPtr);
    }

    public void setCenter(b2Vec2 value) {
        Box2DWrapJNI.b2MassData_center_set(this.swigCPtr, b2Vec2.getCPtr(value));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.box2d.b2Vec2.<init>(int, boolean):void
     arg types: [int, int]
     candidates:
      com.box2d.b2Vec2.<init>(float, float):void
      com.box2d.b2Vec2.<init>(int, boolean):void */
    public b2Vec2 getCenter() {
        int cPtr = Box2DWrapJNI.b2MassData_center_get(this.swigCPtr);
        if (cPtr == 0) {
            return null;
        }
        return new b2Vec2(cPtr, false);
    }

    public void setI(float value) {
        Box2DWrapJNI.b2MassData_I_set(this.swigCPtr, value);
    }

    public float getI() {
        return Box2DWrapJNI.b2MassData_I_get(this.swigCPtr);
    }

    public b2MassData() {
        this(Box2DWrapJNI.new_b2MassData(), true);
    }
}
