package com.box2d;

public class b2RevoluteJointDef extends b2JointDef {
    private int swigCPtr;

    protected b2RevoluteJointDef(int cPtr, boolean cMemoryOwn) {
        super(Box2DWrapJNI.SWIGb2RevoluteJointDefUpcast(cPtr), cMemoryOwn);
        this.swigCPtr = cPtr;
    }

    protected static int getCPtr(b2RevoluteJointDef obj) {
        if (obj == null) {
            return 0;
        }
        return obj.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public synchronized void delete() {
        if (this.swigCPtr != 0) {
            if (this.swigCMemOwn) {
                this.swigCMemOwn = false;
                Box2DWrapJNI.delete_b2RevoluteJointDef(this.swigCPtr);
            }
            this.swigCPtr = 0;
        }
        super.delete();
    }

    public b2RevoluteJointDef() {
        this(Box2DWrapJNI.new_b2RevoluteJointDef(), true);
    }

    public void Initialize(b2Body bodyA, b2Body bodyB, b2Vec2 anchor) {
        Box2DWrapJNI.b2RevoluteJointDef_Initialize(this.swigCPtr, b2Body.getCPtr(bodyA), b2Body.getCPtr(bodyB), b2Vec2.getCPtr(anchor));
    }

    public void setLocalAnchorA(b2Vec2 value) {
        Box2DWrapJNI.b2RevoluteJointDef_localAnchorA_set(this.swigCPtr, b2Vec2.getCPtr(value));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.box2d.b2Vec2.<init>(int, boolean):void
     arg types: [int, int]
     candidates:
      com.box2d.b2Vec2.<init>(float, float):void
      com.box2d.b2Vec2.<init>(int, boolean):void */
    public b2Vec2 getLocalAnchorA() {
        int cPtr = Box2DWrapJNI.b2RevoluteJointDef_localAnchorA_get(this.swigCPtr);
        if (cPtr == 0) {
            return null;
        }
        return new b2Vec2(cPtr, false);
    }

    public void setLocalAnchorB(b2Vec2 value) {
        Box2DWrapJNI.b2RevoluteJointDef_localAnchorB_set(this.swigCPtr, b2Vec2.getCPtr(value));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.box2d.b2Vec2.<init>(int, boolean):void
     arg types: [int, int]
     candidates:
      com.box2d.b2Vec2.<init>(float, float):void
      com.box2d.b2Vec2.<init>(int, boolean):void */
    public b2Vec2 getLocalAnchorB() {
        int cPtr = Box2DWrapJNI.b2RevoluteJointDef_localAnchorB_get(this.swigCPtr);
        if (cPtr == 0) {
            return null;
        }
        return new b2Vec2(cPtr, false);
    }

    public void setReferenceAngle(float value) {
        Box2DWrapJNI.b2RevoluteJointDef_referenceAngle_set(this.swigCPtr, value);
    }

    public float getReferenceAngle() {
        return Box2DWrapJNI.b2RevoluteJointDef_referenceAngle_get(this.swigCPtr);
    }

    public void setEnableLimit(boolean value) {
        Box2DWrapJNI.b2RevoluteJointDef_enableLimit_set(this.swigCPtr, value);
    }

    public boolean getEnableLimit() {
        return Box2DWrapJNI.b2RevoluteJointDef_enableLimit_get(this.swigCPtr);
    }

    public void setLowerAngle(float value) {
        Box2DWrapJNI.b2RevoluteJointDef_lowerAngle_set(this.swigCPtr, value);
    }

    public float getLowerAngle() {
        return Box2DWrapJNI.b2RevoluteJointDef_lowerAngle_get(this.swigCPtr);
    }

    public void setUpperAngle(float value) {
        Box2DWrapJNI.b2RevoluteJointDef_upperAngle_set(this.swigCPtr, value);
    }

    public float getUpperAngle() {
        return Box2DWrapJNI.b2RevoluteJointDef_upperAngle_get(this.swigCPtr);
    }

    public void setEnableMotor(boolean value) {
        Box2DWrapJNI.b2RevoluteJointDef_enableMotor_set(this.swigCPtr, value);
    }

    public boolean getEnableMotor() {
        return Box2DWrapJNI.b2RevoluteJointDef_enableMotor_get(this.swigCPtr);
    }

    public void setMotorSpeed(float value) {
        Box2DWrapJNI.b2RevoluteJointDef_motorSpeed_set(this.swigCPtr, value);
    }

    public float getMotorSpeed() {
        return Box2DWrapJNI.b2RevoluteJointDef_motorSpeed_get(this.swigCPtr);
    }

    public void setMaxMotorTorque(float value) {
        Box2DWrapJNI.b2RevoluteJointDef_maxMotorTorque_set(this.swigCPtr, value);
    }

    public float getMaxMotorTorque() {
        return Box2DWrapJNI.b2RevoluteJointDef_maxMotorTorque_get(this.swigCPtr);
    }
}
