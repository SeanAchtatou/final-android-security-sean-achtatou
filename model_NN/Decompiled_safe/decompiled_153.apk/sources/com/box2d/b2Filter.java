package com.box2d;

public class b2Filter {
    protected boolean swigCMemOwn;
    private int swigCPtr;

    protected b2Filter(int cPtr, boolean cMemoryOwn) {
        this.swigCMemOwn = cMemoryOwn;
        this.swigCPtr = cPtr;
    }

    protected static int getCPtr(b2Filter obj) {
        if (obj == null) {
            return 0;
        }
        return obj.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public synchronized void delete() {
        if (this.swigCPtr != 0) {
            if (this.swigCMemOwn) {
                this.swigCMemOwn = false;
                Box2DWrapJNI.delete_b2Filter(this.swigCPtr);
            }
            this.swigCPtr = 0;
        }
    }

    public void setCategoryBits(int value) {
        Box2DWrapJNI.b2Filter_categoryBits_set(this.swigCPtr, value);
    }

    public int getCategoryBits() {
        return Box2DWrapJNI.b2Filter_categoryBits_get(this.swigCPtr);
    }

    public void setMaskBits(int value) {
        Box2DWrapJNI.b2Filter_maskBits_set(this.swigCPtr, value);
    }

    public int getMaskBits() {
        return Box2DWrapJNI.b2Filter_maskBits_get(this.swigCPtr);
    }

    public void setGroupIndex(short value) {
        Box2DWrapJNI.b2Filter_groupIndex_set(this.swigCPtr, value);
    }

    public short getGroupIndex() {
        return Box2DWrapJNI.b2Filter_groupIndex_get(this.swigCPtr);
    }

    public b2Filter() {
        this(Box2DWrapJNI.new_b2Filter(), true);
    }
}
