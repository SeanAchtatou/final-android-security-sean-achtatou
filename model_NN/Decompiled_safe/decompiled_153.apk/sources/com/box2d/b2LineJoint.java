package com.box2d;

public class b2LineJoint extends b2Joint {
    private int swigCPtr;

    public b2LineJoint(int cPtr, boolean cMemoryOwn) {
        super(Box2DWrapJNI.SWIGb2LineJointUpcast(cPtr), cMemoryOwn);
        this.swigCPtr = cPtr;
    }

    protected static int getCPtr(b2LineJoint obj) {
        if (obj == null) {
            return 0;
        }
        return obj.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public synchronized void delete() {
        if (this.swigCPtr != 0) {
            if (this.swigCMemOwn) {
                this.swigCMemOwn = false;
                Box2DWrapJNI.delete_b2LineJoint(this.swigCPtr);
            }
            this.swigCPtr = 0;
        }
        super.delete();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.box2d.b2Vec2.<init>(int, boolean):void
     arg types: [int, int]
     candidates:
      com.box2d.b2Vec2.<init>(float, float):void
      com.box2d.b2Vec2.<init>(int, boolean):void */
    public b2Vec2 GetAnchorA() {
        return new b2Vec2(Box2DWrapJNI.b2LineJoint_GetAnchorA(this.swigCPtr), true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.box2d.b2Vec2.<init>(int, boolean):void
     arg types: [int, int]
     candidates:
      com.box2d.b2Vec2.<init>(float, float):void
      com.box2d.b2Vec2.<init>(int, boolean):void */
    public b2Vec2 GetAnchorB() {
        return new b2Vec2(Box2DWrapJNI.b2LineJoint_GetAnchorB(this.swigCPtr), true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.box2d.b2Vec2.<init>(int, boolean):void
     arg types: [int, int]
     candidates:
      com.box2d.b2Vec2.<init>(float, float):void
      com.box2d.b2Vec2.<init>(int, boolean):void */
    public b2Vec2 GetReactionForce(float inv_dt) {
        return new b2Vec2(Box2DWrapJNI.b2LineJoint_GetReactionForce(this.swigCPtr, inv_dt), true);
    }

    public float GetReactionTorque(float inv_dt) {
        return Box2DWrapJNI.b2LineJoint_GetReactionTorque(this.swigCPtr, inv_dt);
    }

    public float GetJointTranslation() {
        return Box2DWrapJNI.b2LineJoint_GetJointTranslation(this.swigCPtr);
    }

    public float GetJointSpeed() {
        return Box2DWrapJNI.b2LineJoint_GetJointSpeed(this.swigCPtr);
    }

    public boolean IsLimitEnabled() {
        return Box2DWrapJNI.b2LineJoint_IsLimitEnabled(this.swigCPtr);
    }

    public void EnableLimit(boolean flag) {
        Box2DWrapJNI.b2LineJoint_EnableLimit(this.swigCPtr, flag);
    }

    public float GetLowerLimit() {
        return Box2DWrapJNI.b2LineJoint_GetLowerLimit(this.swigCPtr);
    }

    public float GetUpperLimit() {
        return Box2DWrapJNI.b2LineJoint_GetUpperLimit(this.swigCPtr);
    }

    public void SetLimits(float lower, float upper) {
        Box2DWrapJNI.b2LineJoint_SetLimits(this.swigCPtr, lower, upper);
    }

    public boolean IsMotorEnabled() {
        return Box2DWrapJNI.b2LineJoint_IsMotorEnabled(this.swigCPtr);
    }

    public void EnableMotor(boolean flag) {
        Box2DWrapJNI.b2LineJoint_EnableMotor(this.swigCPtr, flag);
    }

    public void SetMotorSpeed(float speed) {
        Box2DWrapJNI.b2LineJoint_SetMotorSpeed(this.swigCPtr, speed);
    }

    public float GetMotorSpeed() {
        return Box2DWrapJNI.b2LineJoint_GetMotorSpeed(this.swigCPtr);
    }

    public void SetMaxMotorForce(float force) {
        Box2DWrapJNI.b2LineJoint_SetMaxMotorForce(this.swigCPtr, force);
    }

    public float GetMaxMotorForce() {
        return Box2DWrapJNI.b2LineJoint_GetMaxMotorForce(this.swigCPtr);
    }

    public float GetMotorForce(float inv_dt) {
        return Box2DWrapJNI.b2LineJoint_GetMotorForce(this.swigCPtr, inv_dt);
    }
}
