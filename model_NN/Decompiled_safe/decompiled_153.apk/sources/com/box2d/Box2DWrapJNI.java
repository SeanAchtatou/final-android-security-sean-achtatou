package com.box2d;

class Box2DWrapJNI {
    public static final native int SWIGb2DistanceJointDefUpcast(int i);

    public static final native int SWIGb2DistanceJointUpcast(int i);

    public static final native int SWIGb2LineJointDefUpcast(int i);

    public static final native int SWIGb2LineJointUpcast(int i);

    public static final native int SWIGb2MouseJointDefUpcast(int i);

    public static final native int SWIGb2MouseJointUpcast(int i);

    public static final native int SWIGb2PrismaticJointDefUpcast(int i);

    public static final native int SWIGb2PrismaticJointUpcast(int i);

    public static final native int SWIGb2RevoluteJointDefUpcast(int i);

    public static final native int SWIGb2RevoluteJointUpcast(int i);

    public static final native boolean b2BodyDef_active_get(int i);

    public static final native void b2BodyDef_active_set(int i, boolean z);

    public static final native boolean b2BodyDef_allowSleep_get(int i);

    public static final native void b2BodyDef_allowSleep_set(int i, boolean z);

    public static final native float b2BodyDef_angle_get(int i);

    public static final native void b2BodyDef_angle_set(int i, float f);

    public static final native float b2BodyDef_angularDamping_get(int i);

    public static final native void b2BodyDef_angularDamping_set(int i, float f);

    public static final native float b2BodyDef_angularVelocity_get(int i);

    public static final native void b2BodyDef_angularVelocity_set(int i, float f);

    public static final native boolean b2BodyDef_awake_get(int i);

    public static final native void b2BodyDef_awake_set(int i, boolean z);

    public static final native boolean b2BodyDef_bullet_get(int i);

    public static final native void b2BodyDef_bullet_set(int i, boolean z);

    public static final native boolean b2BodyDef_fixedRotation_get(int i);

    public static final native void b2BodyDef_fixedRotation_set(int i, boolean z);

    public static final native float b2BodyDef_inertiaScale_get(int i);

    public static final native void b2BodyDef_inertiaScale_set(int i, float f);

    public static final native float b2BodyDef_linearDamping_get(int i);

    public static final native void b2BodyDef_linearDamping_set(int i, float f);

    public static final native int b2BodyDef_linearVelocity_get(int i);

    public static final native void b2BodyDef_linearVelocity_set(int i, int i2);

    public static final native int b2BodyDef_position_get(int i);

    public static final native void b2BodyDef_position_set(int i, int i2);

    public static final native void b2BodyDef_setPosition(int i, float f, float f2);

    public static final native int b2BodyDef_type_get(int i);

    public static final native void b2BodyDef_type_set(int i, int i2);

    public static final native void b2Body_ApplyAngularImpulse(int i, float f);

    public static final native void b2Body_ApplyForce(int i, float f, float f2, float f3, float f4);

    public static final native void b2Body_ApplyLinearImpulse(int i, float f, float f2, float f3, float f4);

    public static final native void b2Body_ApplyTorque(int i, float f);

    public static final native void b2Body_DestroyAllFixture(int i);

    public static final native void b2Body_DestroyFixture(int i, int i2);

    public static final native float b2Body_GetAngle(int i);

    public static final native float b2Body_GetAngularDamping(int i);

    public static final native float b2Body_GetAngularVelocity(int i);

    public static final native boolean b2Body_GetCollisionCallback(int i);

    public static final native int b2Body_GetCollisionCallback2(int i);

    public static final native int b2Body_GetId(int i);

    public static final native float b2Body_GetInertia(int i);

    public static final native float b2Body_GetLinearDamping(int i);

    public static final native int b2Body_GetLinearVelocityFromLocalPoint(int i, int i2);

    public static final native int b2Body_GetLinearVelocityFromWorldPoint(int i, int i2);

    public static final native float b2Body_GetLinearVelocityX(int i);

    public static final native float b2Body_GetLinearVelocityY(int i);

    public static final native float b2Body_GetLocalCenterX(int i);

    public static final native float b2Body_GetLocalCenterY(int i);

    public static final native void b2Body_GetLocalPoint(int i, int i2, int i3);

    public static final native int b2Body_GetLocalVector(int i, int i2);

    public static final native float b2Body_GetMass(int i);

    public static final native void b2Body_GetMassData(int i, int i2);

    public static final native int b2Body_GetType(int i);

    public static final native float b2Body_GetWorldCenterX(int i);

    public static final native float b2Body_GetWorldCenterY(int i);

    public static final native void b2Body_GetWorldPoint(int i, int i2, int i3);

    public static final native void b2Body_GetWorldVector(int i, int i2, int i3);

    public static final native boolean b2Body_IsActive(int i);

    public static final native boolean b2Body_IsAwake(int i);

    public static final native boolean b2Body_IsBullet(int i);

    public static final native boolean b2Body_IsFixedRotation(int i);

    public static final native boolean b2Body_IsSleepingAllowed(int i);

    public static final native void b2Body_ResetMassData(int i);

    public static final native void b2Body_SetActive(int i, boolean z);

    public static final native void b2Body_SetAngle(int i, float f);

    public static final native void b2Body_SetAngularDamping(int i, float f);

    public static final native void b2Body_SetAngularVelocity(int i, float f);

    public static final native void b2Body_SetAwake(int i, boolean z);

    public static final native void b2Body_SetBullet(int i, boolean z);

    public static final native void b2Body_SetCollisionCallback(int i, boolean z);

    public static final native void b2Body_SetCollisionCallback2(int i, int i2);

    public static final native void b2Body_SetFixedRotation(int i, boolean z);

    public static final native void b2Body_SetId(int i, int i2);

    public static final native void b2Body_SetLinearDamping(int i, float f);

    public static final native void b2Body_SetLinearVelocity(int i, float f, float f2);

    public static final native void b2Body_SetMassCenter(int i, float f, float f2);

    public static final native void b2Body_SetMassData(int i, int i2);

    public static final native void b2Body_SetPosition(int i, float f, float f2);

    public static final native void b2Body_SetSleepingAllowed(int i, boolean z);

    public static final native void b2Body_SetType(int i, int i2);

    public static final native float b2Body_getPositionX(int i);

    public static final native float b2Body_getPositionY(int i);

    public static final native void b2Body_setDensity(int i, float f);

    public static final native void b2Body_setFilter(int i, int i2, int i3, int i4);

    public static final native void b2Body_setFriction(int i, float f);

    public static final native void b2Body_setRestitution(int i, float f);

    public static final native int b2ContactWrapper_bodyId1_get(int i);

    public static final native void b2ContactWrapper_bodyId1_set(int i, int i2);

    public static final native int b2ContactWrapper_bodyId2_get(int i);

    public static final native void b2ContactWrapper_bodyId2_set(int i, int i2);

    public static final native float b2ContactWrapper_normalImpulse_get(int i);

    public static final native void b2ContactWrapper_normalImpulse_set(int i, float f);

    public static final native float b2ContactWrapper_normalX_get(int i);

    public static final native void b2ContactWrapper_normalX_set(int i, float f);

    public static final native float b2ContactWrapper_normalY_get(int i);

    public static final native void b2ContactWrapper_normalY_set(int i, float f);

    public static final native float b2ContactWrapper_tangentImpulse_get(int i);

    public static final native void b2ContactWrapper_tangentImpulse_set(int i, float f);

    public static final native float b2ContactWrapper_touchX_get(int i);

    public static final native void b2ContactWrapper_touchX_set(int i, float f);

    public static final native float b2ContactWrapper_touchY_get(int i);

    public static final native void b2ContactWrapper_touchY_set(int i, float f);

    public static final native void b2DistanceJointDef_Initialize(int i, int i2, int i3, int i4, int i5);

    public static final native float b2DistanceJointDef_dampingRatio_get(int i);

    public static final native void b2DistanceJointDef_dampingRatio_set(int i, float f);

    public static final native float b2DistanceJointDef_frequencyHz_get(int i);

    public static final native void b2DistanceJointDef_frequencyHz_set(int i, float f);

    public static final native float b2DistanceJointDef_length_get(int i);

    public static final native void b2DistanceJointDef_length_set(int i, float f);

    public static final native int b2DistanceJointDef_localAnchorA_get(int i);

    public static final native void b2DistanceJointDef_localAnchorA_set(int i, int i2);

    public static final native int b2DistanceJointDef_localAnchorB_get(int i);

    public static final native void b2DistanceJointDef_localAnchorB_set(int i, int i2);

    public static final native int b2DistanceJoint_GetAnchorA(int i);

    public static final native int b2DistanceJoint_GetAnchorB(int i);

    public static final native float b2DistanceJoint_GetDampingRatio(int i);

    public static final native float b2DistanceJoint_GetFrequency(int i);

    public static final native float b2DistanceJoint_GetLength(int i);

    public static final native int b2DistanceJoint_GetReactionForce(int i, float f);

    public static final native float b2DistanceJoint_GetReactionTorque(int i, float f);

    public static final native void b2DistanceJoint_SetDampingRatio(int i, float f);

    public static final native void b2DistanceJoint_SetFrequency(int i, float f);

    public static final native void b2DistanceJoint_SetLength(int i, float f);

    public static final native int b2Filter_categoryBits_get(int i);

    public static final native void b2Filter_categoryBits_set(int i, int i2);

    public static final native short b2Filter_groupIndex_get(int i);

    public static final native void b2Filter_groupIndex_set(int i, short s);

    public static final native int b2Filter_maskBits_get(int i);

    public static final native void b2Filter_maskBits_set(int i, int i2);

    public static final native int b2Fixture_GetBody__SWIG_0(int i);

    public static final native float b2Fixture_GetDensity(int i);

    public static final native int b2Fixture_GetFilterData(int i);

    public static final native float b2Fixture_GetFriction(int i);

    public static final native void b2Fixture_GetMassData(int i, int i2);

    public static final native float b2Fixture_GetRestitution(int i);

    public static final native boolean b2Fixture_IsSensor(int i);

    public static final native void b2Fixture_SetDensity(int i, float f);

    public static final native void b2Fixture_SetFilterData(int i, int i2);

    public static final native void b2Fixture_SetFriction(int i, float f);

    public static final native void b2Fixture_SetRestitution(int i, float f);

    public static final native void b2Fixture_SetSensor(int i, boolean z);

    public static final native boolean b2Fixture_TestPoint(int i, int i2);

    public static final native void b2Fixture_setFilter(int i, int i2, int i3, int i4);

    public static final native int b2JointDef_bodyA_get(int i);

    public static final native void b2JointDef_bodyA_set(int i, int i2);

    public static final native int b2JointDef_bodyB_get(int i);

    public static final native void b2JointDef_bodyB_set(int i, int i2);

    public static final native boolean b2JointDef_collideConnected_get(int i);

    public static final native void b2JointDef_collideConnected_set(int i, boolean z);

    public static final native int b2Joint_GetAnchorA(int i);

    public static final native int b2Joint_GetAnchorB(int i);

    public static final native int b2Joint_GetBodyA(int i);

    public static final native int b2Joint_GetBodyB(int i);

    public static final native int b2Joint_GetReactionForce(int i, float f);

    public static final native float b2Joint_GetReactionTorque(int i, float f);

    public static final native boolean b2Joint_IsActive(int i);

    public static final native void b2LineJointDef_Initialize(int i, int i2, int i3, int i4, int i5);

    public static final native boolean b2LineJointDef_enableLimit_get(int i);

    public static final native void b2LineJointDef_enableLimit_set(int i, boolean z);

    public static final native boolean b2LineJointDef_enableMotor_get(int i);

    public static final native void b2LineJointDef_enableMotor_set(int i, boolean z);

    public static final native int b2LineJointDef_localAnchorA_get(int i);

    public static final native void b2LineJointDef_localAnchorA_set(int i, int i2);

    public static final native int b2LineJointDef_localAnchorB_get(int i);

    public static final native void b2LineJointDef_localAnchorB_set(int i, int i2);

    public static final native int b2LineJointDef_localAxisA_get(int i);

    public static final native void b2LineJointDef_localAxisA_set(int i, int i2);

    public static final native float b2LineJointDef_lowerTranslation_get(int i);

    public static final native void b2LineJointDef_lowerTranslation_set(int i, float f);

    public static final native float b2LineJointDef_maxMotorForce_get(int i);

    public static final native void b2LineJointDef_maxMotorForce_set(int i, float f);

    public static final native float b2LineJointDef_motorSpeed_get(int i);

    public static final native void b2LineJointDef_motorSpeed_set(int i, float f);

    public static final native float b2LineJointDef_upperTranslation_get(int i);

    public static final native void b2LineJointDef_upperTranslation_set(int i, float f);

    public static final native void b2LineJoint_EnableLimit(int i, boolean z);

    public static final native void b2LineJoint_EnableMotor(int i, boolean z);

    public static final native int b2LineJoint_GetAnchorA(int i);

    public static final native int b2LineJoint_GetAnchorB(int i);

    public static final native float b2LineJoint_GetJointSpeed(int i);

    public static final native float b2LineJoint_GetJointTranslation(int i);

    public static final native float b2LineJoint_GetLowerLimit(int i);

    public static final native float b2LineJoint_GetMaxMotorForce(int i);

    public static final native float b2LineJoint_GetMotorForce(int i, float f);

    public static final native float b2LineJoint_GetMotorSpeed(int i);

    public static final native int b2LineJoint_GetReactionForce(int i, float f);

    public static final native float b2LineJoint_GetReactionTorque(int i, float f);

    public static final native float b2LineJoint_GetUpperLimit(int i);

    public static final native boolean b2LineJoint_IsLimitEnabled(int i);

    public static final native boolean b2LineJoint_IsMotorEnabled(int i);

    public static final native void b2LineJoint_SetLimits(int i, float f, float f2);

    public static final native void b2LineJoint_SetMaxMotorForce(int i, float f);

    public static final native void b2LineJoint_SetMotorSpeed(int i, float f);

    public static final native float b2MassData_I_get(int i);

    public static final native void b2MassData_I_set(int i, float f);

    public static final native int b2MassData_center_get(int i);

    public static final native void b2MassData_center_set(int i, int i2);

    public static final native float b2MassData_mass_get(int i);

    public static final native void b2MassData_mass_set(int i, float f);

    public static final native float b2MouseJointDef_dampingRatio_get(int i);

    public static final native void b2MouseJointDef_dampingRatio_set(int i, float f);

    public static final native float b2MouseJointDef_frequencyHz_get(int i);

    public static final native void b2MouseJointDef_frequencyHz_set(int i, float f);

    public static final native float b2MouseJointDef_maxForce_get(int i);

    public static final native void b2MouseJointDef_maxForce_set(int i, float f);

    public static final native int b2MouseJointDef_target_get(int i);

    public static final native void b2MouseJointDef_target_set(int i, int i2);

    public static final native int b2MouseJoint_GetAnchorA(int i);

    public static final native int b2MouseJoint_GetAnchorB(int i);

    public static final native float b2MouseJoint_GetDampingRatio(int i);

    public static final native float b2MouseJoint_GetFrequency(int i);

    public static final native float b2MouseJoint_GetMaxForce(int i);

    public static final native int b2MouseJoint_GetReactionForce(int i, float f);

    public static final native float b2MouseJoint_GetReactionTorque(int i, float f);

    public static final native int b2MouseJoint_GetTarget(int i);

    public static final native void b2MouseJoint_SetDampingRatio(int i, float f);

    public static final native void b2MouseJoint_SetFrequency(int i, float f);

    public static final native void b2MouseJoint_SetMaxForce(int i, float f);

    public static final native void b2MouseJoint_SetTarget__SWIG_0(int i, int i2);

    public static final native void b2MouseJoint_SetTarget__SWIG_1(int i, float f, float f2);

    public static final native void b2MyContactListener_clear(int i);

    public static final native int b2MyContactListener_getContactCount(int i);

    public static final native int b2MyContactListener_getSensorContactCount(int i);

    public static final native void b2MyContactListener_initiate(int i, int i2, int i3);

    public static final native void b2MyContactListener_put__SWIG_0(int i, int i2, int i3);

    public static final native void b2MyContactListener_put__SWIG_1(int i, int i2, int i3);

    public static final native void b2MyContactListener_setImpulseThreshold(int i, float f);

    public static final native int b2MyHelper_CreateCircle__SWIG_0(int i, int i2, float f, float f2, float f3, float f4);

    public static final native int b2MyHelper_CreateCircle__SWIG_1(int i, int i2, float f, float f2, float f3, float f4, float f5, float f6);

    public static final native void b2MyHelper_CreateEdges(int i, int i2, int i3, float f, float f2, float f3);

    public static final native void b2MyHelper_CreateLoop(int i, int i2, int i3, float f, float f2, float f3);

    public static final native int b2MyHelper_CreatePolygon(int i, int i2, int i3, float f, float f2, float f3);

    public static final native void b2MyHelper_CreateProfileBoxes(int i, int i2, int i3, float f, float f2, float f3, float f4);

    public static final native int b2MyHelper_CreateRect__SWIG_0(int i, int i2, float f, float f2, float f3, float f4, float f5);

    public static final native int b2MyHelper_CreateRect__SWIG_1(int i, int i2, float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8);

    public static final native int b2MyHelper_getContactListener(int i);

    public static final native int b2MyHelper_getWorld(int i);

    public static final native int b2MyVertices_Get(int i, int i2);

    public static final native int b2MyVertices_GetCount(int i);

    public static final native int b2MyVertices_GetVertices(int i);

    public static final native void b2MyVertices_Set(int i, float f, float f2, int i2);

    public static final native void b2PrismaticJointDef_Initialize(int i, int i2, int i3, int i4, int i5);

    public static final native boolean b2PrismaticJointDef_enableLimit_get(int i);

    public static final native void b2PrismaticJointDef_enableLimit_set(int i, boolean z);

    public static final native boolean b2PrismaticJointDef_enableMotor_get(int i);

    public static final native void b2PrismaticJointDef_enableMotor_set(int i, boolean z);

    public static final native int b2PrismaticJointDef_localAnchorA_get(int i);

    public static final native void b2PrismaticJointDef_localAnchorA_set(int i, int i2);

    public static final native int b2PrismaticJointDef_localAnchorB_get(int i);

    public static final native void b2PrismaticJointDef_localAnchorB_set(int i, int i2);

    public static final native int b2PrismaticJointDef_localAxis1_get(int i);

    public static final native void b2PrismaticJointDef_localAxis1_set(int i, int i2);

    public static final native float b2PrismaticJointDef_lowerTranslation_get(int i);

    public static final native void b2PrismaticJointDef_lowerTranslation_set(int i, float f);

    public static final native float b2PrismaticJointDef_maxMotorForce_get(int i);

    public static final native void b2PrismaticJointDef_maxMotorForce_set(int i, float f);

    public static final native float b2PrismaticJointDef_motorSpeed_get(int i);

    public static final native void b2PrismaticJointDef_motorSpeed_set(int i, float f);

    public static final native float b2PrismaticJointDef_referenceAngle_get(int i);

    public static final native void b2PrismaticJointDef_referenceAngle_set(int i, float f);

    public static final native float b2PrismaticJointDef_upperTranslation_get(int i);

    public static final native void b2PrismaticJointDef_upperTranslation_set(int i, float f);

    public static final native void b2PrismaticJoint_EnableLimit(int i, boolean z);

    public static final native void b2PrismaticJoint_EnableMotor(int i, boolean z);

    public static final native int b2PrismaticJoint_GetAnchorA(int i);

    public static final native int b2PrismaticJoint_GetAnchorB(int i);

    public static final native float b2PrismaticJoint_GetJointSpeed(int i);

    public static final native float b2PrismaticJoint_GetJointTranslation(int i);

    public static final native float b2PrismaticJoint_GetLowerLimit(int i);

    public static final native float b2PrismaticJoint_GetMotorForce(int i, float f);

    public static final native float b2PrismaticJoint_GetMotorSpeed(int i);

    public static final native int b2PrismaticJoint_GetReactionForce(int i, float f);

    public static final native float b2PrismaticJoint_GetReactionTorque(int i, float f);

    public static final native float b2PrismaticJoint_GetUpperLimit(int i);

    public static final native boolean b2PrismaticJoint_IsLimitEnabled(int i);

    public static final native boolean b2PrismaticJoint_IsMotorEnabled(int i);

    public static final native void b2PrismaticJoint_SetLimits(int i, float f, float f2);

    public static final native void b2PrismaticJoint_SetMaxMotorForce(int i, float f);

    public static final native void b2PrismaticJoint_SetMotorSpeed(int i, float f);

    public static final native void b2RevoluteJointDef_Initialize(int i, int i2, int i3, int i4);

    public static final native boolean b2RevoluteJointDef_enableLimit_get(int i);

    public static final native void b2RevoluteJointDef_enableLimit_set(int i, boolean z);

    public static final native boolean b2RevoluteJointDef_enableMotor_get(int i);

    public static final native void b2RevoluteJointDef_enableMotor_set(int i, boolean z);

    public static final native int b2RevoluteJointDef_localAnchorA_get(int i);

    public static final native void b2RevoluteJointDef_localAnchorA_set(int i, int i2);

    public static final native int b2RevoluteJointDef_localAnchorB_get(int i);

    public static final native void b2RevoluteJointDef_localAnchorB_set(int i, int i2);

    public static final native float b2RevoluteJointDef_lowerAngle_get(int i);

    public static final native void b2RevoluteJointDef_lowerAngle_set(int i, float f);

    public static final native float b2RevoluteJointDef_maxMotorTorque_get(int i);

    public static final native void b2RevoluteJointDef_maxMotorTorque_set(int i, float f);

    public static final native float b2RevoluteJointDef_motorSpeed_get(int i);

    public static final native void b2RevoluteJointDef_motorSpeed_set(int i, float f);

    public static final native float b2RevoluteJointDef_referenceAngle_get(int i);

    public static final native void b2RevoluteJointDef_referenceAngle_set(int i, float f);

    public static final native float b2RevoluteJointDef_upperAngle_get(int i);

    public static final native void b2RevoluteJointDef_upperAngle_set(int i, float f);

    public static final native void b2RevoluteJoint_EnableLimit(int i, boolean z);

    public static final native void b2RevoluteJoint_EnableMotor(int i, boolean z);

    public static final native int b2RevoluteJoint_GetAnchorA(int i);

    public static final native int b2RevoluteJoint_GetAnchorB(int i);

    public static final native float b2RevoluteJoint_GetJointAngle(int i);

    public static final native float b2RevoluteJoint_GetJointSpeed(int i);

    public static final native float b2RevoluteJoint_GetLowerLimit(int i);

    public static final native float b2RevoluteJoint_GetMotorSpeed(int i);

    public static final native float b2RevoluteJoint_GetMotorTorque(int i, float f);

    public static final native int b2RevoluteJoint_GetReactionForce(int i, float f);

    public static final native float b2RevoluteJoint_GetReactionTorque(int i, float f);

    public static final native float b2RevoluteJoint_GetUpperLimit(int i);

    public static final native boolean b2RevoluteJoint_IsLimitEnabled(int i);

    public static final native boolean b2RevoluteJoint_IsMotorEnabled(int i);

    public static final native void b2RevoluteJoint_SetLimits(int i, float f, float f2);

    public static final native void b2RevoluteJoint_SetMaxMotorTorque(int i, float f);

    public static final native void b2RevoluteJoint_SetMotorSpeed(int i, float f);

    public static final native int b2SensorContactWrapper_bodyId1_get(int i);

    public static final native void b2SensorContactWrapper_bodyId1_set(int i, int i2);

    public static final native int b2SensorContactWrapper_bodyId2_get(int i);

    public static final native void b2SensorContactWrapper_bodyId2_set(int i, int i2);

    public static final native boolean b2SensorContactWrapper_isBegin_get(int i);

    public static final native void b2SensorContactWrapper_isBegin_set(int i, boolean z);

    public static final native float b2SensorContactWrapper_normalX_get(int i);

    public static final native void b2SensorContactWrapper_normalX_set(int i, float f);

    public static final native float b2SensorContactWrapper_normalY_get(int i);

    public static final native void b2SensorContactWrapper_normalY_set(int i, float f);

    public static final native float b2SensorContactWrapper_touchX_get(int i);

    public static final native void b2SensorContactWrapper_touchX_set(int i, float f);

    public static final native float b2SensorContactWrapper_touchY_get(int i);

    public static final native void b2SensorContactWrapper_touchY_set(int i, float f);

    public static final native boolean b2Vec2_IsValid(int i);

    public static final native float b2Vec2_Length(int i);

    public static final native float b2Vec2_LengthSquared(int i);

    public static final native float b2Vec2_Normalize(int i);

    public static final native void b2Vec2_Set(int i, float f, float f2);

    public static final native void b2Vec2_SetZero(int i);

    public static final native float b2Vec2_x_get(int i);

    public static final native void b2Vec2_x_set(int i, float f);

    public static final native float b2Vec2_y_get(int i);

    public static final native void b2Vec2_y_set(int i, float f);

    public static final native void b2World_ClearForces(int i);

    public static final native int b2World_CreateBody(int i, int i2);

    public static final native int b2World_CreateJoint(int i, int i2);

    public static final native void b2World_DestroyBody(int i, int i2);

    public static final native void b2World_DestroyJoint(int i, int i2);

    public static final native boolean b2World_GetAutoClearForces(int i);

    public static final native int b2World_GetBodyCount(int i);

    public static final native int b2World_GetContactCount(int i);

    public static final native int b2World_GetGravity(int i);

    public static final native int b2World_GetJointCount(int i);

    public static final native int b2World_GetProxyCount(int i);

    public static final native boolean b2World_IsLocked(int i);

    public static final native void b2World_SetAutoClearForces(int i, boolean z);

    public static final native void b2World_SetContinuousPhysics(int i, boolean z);

    public static final native void b2World_SetGravity(int i, int i2);

    public static final native void b2World_SetWarmStarting(int i, boolean z);

    public static final native void b2World_Step(int i, float f, int i2, int i3);

    public static final native double b2_aabbExtension_get();

    public static final native double b2_aabbMultiplier_get();

    public static final native double b2_angularSleepTolerance_get();

    public static final native double b2_angularSlop_get();

    public static final native double b2_contactBaumgarte_get();

    public static final native double b2_linearSleepTolerance_get();

    public static final native double b2_linearSlop_get();

    public static final native double b2_maxAngularCorrection_get();

    public static final native double b2_maxLinearCorrection_get();

    public static final native int b2_maxManifoldPoints_get();

    public static final native int b2_maxPolygonVertices_get();

    public static final native double b2_maxRotationSquared_get();

    public static final native double b2_maxRotation_get();

    public static final native int b2_maxSubSteps_get();

    public static final native int b2_maxTOIContacts_get();

    public static final native double b2_maxTranslationSquared_get();

    public static final native double b2_maxTranslation_get();

    public static final native double b2_pi_get();

    public static final native double b2_polygonRadius_get();

    public static final native int b2_staticBody_get();

    public static final native double b2_timeToSleep_get();

    public static final native double b2_velocityThreshold_get();

    public static final native void delete_b2BodyDef(int i);

    public static final native void delete_b2ContactWrapper(int i);

    public static final native void delete_b2DistanceJoint(int i);

    public static final native void delete_b2DistanceJointDef(int i);

    public static final native void delete_b2Filter(int i);

    public static final native void delete_b2Fixture(int i);

    public static final native void delete_b2JointDef(int i);

    public static final native void delete_b2LineJoint(int i);

    public static final native void delete_b2LineJointDef(int i);

    public static final native void delete_b2MassData(int i);

    public static final native void delete_b2MouseJoint(int i);

    public static final native void delete_b2MouseJointDef(int i);

    public static final native void delete_b2MyContactListener(int i);

    public static final native void delete_b2MyHelper(int i);

    public static final native void delete_b2MyVertices(int i);

    public static final native void delete_b2PrismaticJoint(int i);

    public static final native void delete_b2PrismaticJointDef(int i);

    public static final native void delete_b2RevoluteJoint(int i);

    public static final native void delete_b2RevoluteJointDef(int i);

    public static final native void delete_b2SensorContactWrapper(int i);

    public static final native void delete_b2Vec2(int i);

    public static final native void delete_b2World(int i);

    public static final native int new_b2BodyDef();

    public static final native int new_b2ContactWrapper();

    public static final native int new_b2DistanceJointDef();

    public static final native int new_b2Filter();

    public static final native int new_b2JointDef();

    public static final native int new_b2LineJointDef();

    public static final native int new_b2MassData();

    public static final native int new_b2MouseJointDef();

    public static final native int new_b2MyContactListener();

    public static final native int new_b2MyHelper(int i);

    public static final native int new_b2MyVertices(int i);

    public static final native int new_b2PrismaticJointDef();

    public static final native int new_b2RevoluteJointDef();

    public static final native int new_b2SensorContactWrapper();

    public static final native int new_b2Vec2__SWIG_0();

    public static final native int new_b2Vec2__SWIG_1(float f, float f2);

    public static final native int new_b2World(int i, boolean z);

    Box2DWrapJNI() {
    }
}
