package com.box2d;

public class b2SensorContactWrapper {
    protected boolean swigCMemOwn;
    private int swigCPtr;

    protected b2SensorContactWrapper(int cPtr, boolean cMemoryOwn) {
        this.swigCMemOwn = cMemoryOwn;
        this.swigCPtr = cPtr;
    }

    protected static int getCPtr(b2SensorContactWrapper obj) {
        if (obj == null) {
            return 0;
        }
        return obj.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public synchronized void delete() {
        if (this.swigCPtr != 0) {
            if (this.swigCMemOwn) {
                this.swigCMemOwn = false;
                Box2DWrapJNI.delete_b2SensorContactWrapper(this.swigCPtr);
            }
            this.swigCPtr = 0;
        }
    }

    public void setNormalX(float value) {
        Box2DWrapJNI.b2SensorContactWrapper_normalX_set(this.swigCPtr, value);
    }

    public float getNormalX() {
        return Box2DWrapJNI.b2SensorContactWrapper_normalX_get(this.swigCPtr);
    }

    public void setNormalY(float value) {
        Box2DWrapJNI.b2SensorContactWrapper_normalY_set(this.swigCPtr, value);
    }

    public float getNormalY() {
        return Box2DWrapJNI.b2SensorContactWrapper_normalY_get(this.swigCPtr);
    }

    public void setBodyId1(int value) {
        Box2DWrapJNI.b2SensorContactWrapper_bodyId1_set(this.swigCPtr, value);
    }

    public int getBodyId1() {
        return Box2DWrapJNI.b2SensorContactWrapper_bodyId1_get(this.swigCPtr);
    }

    public void setBodyId2(int value) {
        Box2DWrapJNI.b2SensorContactWrapper_bodyId2_set(this.swigCPtr, value);
    }

    public int getBodyId2() {
        return Box2DWrapJNI.b2SensorContactWrapper_bodyId2_get(this.swigCPtr);
    }

    public void setTouchX(float value) {
        Box2DWrapJNI.b2SensorContactWrapper_touchX_set(this.swigCPtr, value);
    }

    public float getTouchX() {
        return Box2DWrapJNI.b2SensorContactWrapper_touchX_get(this.swigCPtr);
    }

    public void setTouchY(float value) {
        Box2DWrapJNI.b2SensorContactWrapper_touchY_set(this.swigCPtr, value);
    }

    public float getTouchY() {
        return Box2DWrapJNI.b2SensorContactWrapper_touchY_get(this.swigCPtr);
    }

    public void setIsBegin(boolean value) {
        Box2DWrapJNI.b2SensorContactWrapper_isBegin_set(this.swigCPtr, value);
    }

    public boolean getIsBegin() {
        return Box2DWrapJNI.b2SensorContactWrapper_isBegin_get(this.swigCPtr);
    }

    public b2SensorContactWrapper() {
        this(Box2DWrapJNI.new_b2SensorContactWrapper(), true);
    }
}
