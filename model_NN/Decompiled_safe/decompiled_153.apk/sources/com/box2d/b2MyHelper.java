package com.box2d;

public class b2MyHelper {
    protected boolean swigCMemOwn;
    private int swigCPtr;

    protected b2MyHelper(int cPtr, boolean cMemoryOwn) {
        this.swigCMemOwn = cMemoryOwn;
        this.swigCPtr = cPtr;
    }

    public static int getCPtr(b2MyHelper obj) {
        if (obj == null) {
            return 0;
        }
        return obj.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public synchronized void delete() {
        if (this.swigCPtr != 0) {
            if (this.swigCMemOwn) {
                this.swigCMemOwn = false;
                Box2DWrapJNI.delete_b2MyHelper(this.swigCPtr);
            }
            this.swigCPtr = 0;
        }
    }

    public b2MyHelper(b2World pWorld) {
        this(Box2DWrapJNI.new_b2MyHelper(b2World.getCPtr(pWorld)), true);
    }

    public b2Fixture CreateCircle(b2Body pBody, float radius, float density, float friction, float restitution) {
        int cPtr = Box2DWrapJNI.b2MyHelper_CreateCircle__SWIG_0(this.swigCPtr, b2Body.getCPtr(pBody), radius, density, friction, restitution);
        if (cPtr == 0) {
            return null;
        }
        return new b2Fixture(cPtr, false);
    }

    public b2Fixture CreateCircle(b2Body pBody, float radius, float centerX, float centerY, float density, float friction, float restitution) {
        int cPtr = Box2DWrapJNI.b2MyHelper_CreateCircle__SWIG_1(this.swigCPtr, b2Body.getCPtr(pBody), radius, centerX, centerY, density, friction, restitution);
        if (cPtr == 0) {
            return null;
        }
        return new b2Fixture(cPtr, false);
    }

    public b2Fixture CreateRect(b2Body pBody, float width, float height, float density, float friction, float restitution) {
        int cPtr = Box2DWrapJNI.b2MyHelper_CreateRect__SWIG_0(this.swigCPtr, b2Body.getCPtr(pBody), width, height, density, friction, restitution);
        if (cPtr == 0) {
            return null;
        }
        return new b2Fixture(cPtr, false);
    }

    public b2Fixture CreateRect(b2Body pBody, float width, float height, float centerX, float centerY, float angle, float density, float friction, float restitution) {
        int cPtr = Box2DWrapJNI.b2MyHelper_CreateRect__SWIG_1(this.swigCPtr, b2Body.getCPtr(pBody), width, height, centerX, centerY, angle, density, friction, restitution);
        if (cPtr == 0) {
            return null;
        }
        return new b2Fixture(cPtr, false);
    }

    public b2Fixture CreatePolygon(b2Body pBody, b2MyVertices pVertices, float density, float friction, float restitution) {
        int cPtr = Box2DWrapJNI.b2MyHelper_CreatePolygon(this.swigCPtr, b2Body.getCPtr(pBody), b2MyVertices.getCPtr(pVertices), density, friction, restitution);
        if (cPtr == 0) {
            return null;
        }
        return new b2Fixture(cPtr, false);
    }

    public void CreateProfileBoxes(b2Body pBody, b2MyVertices pVertices, float thick, float density, float friction, float restitution) {
        Box2DWrapJNI.b2MyHelper_CreateProfileBoxes(this.swigCPtr, b2Body.getCPtr(pBody), b2MyVertices.getCPtr(pVertices), thick, density, friction, restitution);
    }

    public void CreateLoop(b2Body pBody, b2MyVertices pVertices, float density, float friction, float restitution) {
        Box2DWrapJNI.b2MyHelper_CreateLoop(this.swigCPtr, b2Body.getCPtr(pBody), b2MyVertices.getCPtr(pVertices), density, friction, restitution);
    }

    public void CreateEdges(b2Body pBody, b2MyVertices pVertices, float density, float friction, float restitution) {
        Box2DWrapJNI.b2MyHelper_CreateEdges(this.swigCPtr, b2Body.getCPtr(pBody), b2MyVertices.getCPtr(pVertices), density, friction, restitution);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.box2d.b2World.<init>(int, boolean):void
     arg types: [int, int]
     candidates:
      com.box2d.b2World.<init>(com.box2d.b2Vec2, boolean):void
      com.box2d.b2World.<init>(int, boolean):void */
    public b2World getWorld() {
        int cPtr = Box2DWrapJNI.b2MyHelper_getWorld(this.swigCPtr);
        if (cPtr == 0) {
            return null;
        }
        return new b2World(cPtr, false);
    }

    public b2MyContactListener getContactListener() {
        int cPtr = Box2DWrapJNI.b2MyHelper_getContactListener(this.swigCPtr);
        if (cPtr == 0) {
            return null;
        }
        return new b2MyContactListener(cPtr, false);
    }
}
