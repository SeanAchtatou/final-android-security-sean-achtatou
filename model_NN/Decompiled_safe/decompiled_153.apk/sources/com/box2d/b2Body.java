package com.box2d;

public class b2Body {
    protected boolean swigCMemOwn;
    private int swigCPtr;

    protected b2Body(int cPtr, boolean cMemoryOwn) {
        this.swigCMemOwn = cMemoryOwn;
        this.swigCPtr = cPtr;
    }

    public static int getCPtr(b2Body obj) {
        if (obj == null) {
            return 0;
        }
        return obj.swigCPtr;
    }

    public synchronized void delete() {
        if (this.swigCPtr != 0) {
            if (this.swigCMemOwn) {
                this.swigCMemOwn = false;
                throw new UnsupportedOperationException("C++ destructor does not have public access");
            }
            this.swigCPtr = 0;
        }
    }

    public void DestroyFixture(b2Fixture fixture) {
        Box2DWrapJNI.b2Body_DestroyFixture(this.swigCPtr, b2Fixture.getCPtr(fixture));
    }

    public void DestroyAllFixture() {
        Box2DWrapJNI.b2Body_DestroyAllFixture(this.swigCPtr);
    }

    public void SetPosition(float x, float y) {
        Box2DWrapJNI.b2Body_SetPosition(this.swigCPtr, x, y);
    }

    public void SetAngle(float angle) {
        Box2DWrapJNI.b2Body_SetAngle(this.swigCPtr, angle);
    }

    public float getPositionX() {
        return Box2DWrapJNI.b2Body_getPositionX(this.swigCPtr);
    }

    public float getPositionY() {
        return Box2DWrapJNI.b2Body_getPositionY(this.swigCPtr);
    }

    public float GetAngle() {
        return Box2DWrapJNI.b2Body_GetAngle(this.swigCPtr);
    }

    public float GetWorldCenterX() {
        return Box2DWrapJNI.b2Body_GetWorldCenterX(this.swigCPtr);
    }

    public float GetWorldCenterY() {
        return Box2DWrapJNI.b2Body_GetWorldCenterY(this.swigCPtr);
    }

    public float GetLocalCenterX() {
        return Box2DWrapJNI.b2Body_GetLocalCenterX(this.swigCPtr);
    }

    public float GetLocalCenterY() {
        return Box2DWrapJNI.b2Body_GetLocalCenterY(this.swigCPtr);
    }

    public void SetLinearVelocity(float vX, float vY) {
        Box2DWrapJNI.b2Body_SetLinearVelocity(this.swigCPtr, vX, vY);
    }

    public float GetLinearVelocityX() {
        return Box2DWrapJNI.b2Body_GetLinearVelocityX(this.swigCPtr);
    }

    public float GetLinearVelocityY() {
        return Box2DWrapJNI.b2Body_GetLinearVelocityY(this.swigCPtr);
    }

    public void SetAngularVelocity(float omega) {
        Box2DWrapJNI.b2Body_SetAngularVelocity(this.swigCPtr, omega);
    }

    public float GetAngularVelocity() {
        return Box2DWrapJNI.b2Body_GetAngularVelocity(this.swigCPtr);
    }

    public void ApplyForce(float forceX, float forceY, float pointX, float pointY) {
        Box2DWrapJNI.b2Body_ApplyForce(this.swigCPtr, forceX, forceY, pointX, pointY);
    }

    public void ApplyTorque(float torque) {
        Box2DWrapJNI.b2Body_ApplyTorque(this.swigCPtr, torque);
    }

    public void ApplyLinearImpulse(float impulseX, float impulseY, float pointX, float pointY) {
        Box2DWrapJNI.b2Body_ApplyLinearImpulse(this.swigCPtr, impulseX, impulseY, pointX, pointY);
    }

    public void ApplyAngularImpulse(float impulse) {
        Box2DWrapJNI.b2Body_ApplyAngularImpulse(this.swigCPtr, impulse);
    }

    public float GetMass() {
        return Box2DWrapJNI.b2Body_GetMass(this.swigCPtr);
    }

    public float GetInertia() {
        return Box2DWrapJNI.b2Body_GetInertia(this.swigCPtr);
    }

    public void GetMassData(b2MassData data) {
        Box2DWrapJNI.b2Body_GetMassData(this.swigCPtr, b2MassData.getCPtr(data));
    }

    public void SetMassData(b2MassData data) {
        Box2DWrapJNI.b2Body_SetMassData(this.swigCPtr, b2MassData.getCPtr(data));
    }

    public void ResetMassData() {
        Box2DWrapJNI.b2Body_ResetMassData(this.swigCPtr);
    }

    public void SetMassCenter(float localCenterX, float localCenterY) {
        Box2DWrapJNI.b2Body_SetMassCenter(this.swigCPtr, localCenterX, localCenterY);
    }

    public void GetWorldPoint(b2Vec2 localPoint, b2Vec2 worldPoint) {
        Box2DWrapJNI.b2Body_GetWorldPoint(this.swigCPtr, b2Vec2.getCPtr(localPoint), b2Vec2.getCPtr(worldPoint));
    }

    public void GetWorldVector(b2Vec2 localVector, b2Vec2 worldVector) {
        Box2DWrapJNI.b2Body_GetWorldVector(this.swigCPtr, b2Vec2.getCPtr(localVector), b2Vec2.getCPtr(worldVector));
    }

    public void GetLocalPoint(b2Vec2 worldPoint, b2Vec2 localPoint) {
        Box2DWrapJNI.b2Body_GetLocalPoint(this.swigCPtr, b2Vec2.getCPtr(worldPoint), b2Vec2.getCPtr(localPoint));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.box2d.b2Vec2.<init>(int, boolean):void
     arg types: [int, int]
     candidates:
      com.box2d.b2Vec2.<init>(float, float):void
      com.box2d.b2Vec2.<init>(int, boolean):void */
    public b2Vec2 GetLocalVector(b2Vec2 worldVector) {
        return new b2Vec2(Box2DWrapJNI.b2Body_GetLocalVector(this.swigCPtr, b2Vec2.getCPtr(worldVector)), true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.box2d.b2Vec2.<init>(int, boolean):void
     arg types: [int, int]
     candidates:
      com.box2d.b2Vec2.<init>(float, float):void
      com.box2d.b2Vec2.<init>(int, boolean):void */
    public b2Vec2 GetLinearVelocityFromWorldPoint(b2Vec2 worldPoint) {
        return new b2Vec2(Box2DWrapJNI.b2Body_GetLinearVelocityFromWorldPoint(this.swigCPtr, b2Vec2.getCPtr(worldPoint)), true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.box2d.b2Vec2.<init>(int, boolean):void
     arg types: [int, int]
     candidates:
      com.box2d.b2Vec2.<init>(float, float):void
      com.box2d.b2Vec2.<init>(int, boolean):void */
    public b2Vec2 GetLinearVelocityFromLocalPoint(b2Vec2 localPoint) {
        return new b2Vec2(Box2DWrapJNI.b2Body_GetLinearVelocityFromLocalPoint(this.swigCPtr, b2Vec2.getCPtr(localPoint)), true);
    }

    public float GetLinearDamping() {
        return Box2DWrapJNI.b2Body_GetLinearDamping(this.swigCPtr);
    }

    public void SetLinearDamping(float linearDamping) {
        Box2DWrapJNI.b2Body_SetLinearDamping(this.swigCPtr, linearDamping);
    }

    public float GetAngularDamping() {
        return Box2DWrapJNI.b2Body_GetAngularDamping(this.swigCPtr);
    }

    public void SetAngularDamping(float angularDamping) {
        Box2DWrapJNI.b2Body_SetAngularDamping(this.swigCPtr, angularDamping);
    }

    public void SetType(b2BodyType type) {
        Box2DWrapJNI.b2Body_SetType(this.swigCPtr, type.swigValue());
    }

    public b2BodyType GetType() {
        return b2BodyType.swigToEnum(Box2DWrapJNI.b2Body_GetType(this.swigCPtr));
    }

    public void SetBullet(boolean flag) {
        Box2DWrapJNI.b2Body_SetBullet(this.swigCPtr, flag);
    }

    public boolean IsBullet() {
        return Box2DWrapJNI.b2Body_IsBullet(this.swigCPtr);
    }

    public void SetSleepingAllowed(boolean flag) {
        Box2DWrapJNI.b2Body_SetSleepingAllowed(this.swigCPtr, flag);
    }

    public boolean IsSleepingAllowed() {
        return Box2DWrapJNI.b2Body_IsSleepingAllowed(this.swigCPtr);
    }

    public void SetAwake(boolean flag) {
        Box2DWrapJNI.b2Body_SetAwake(this.swigCPtr, flag);
    }

    public boolean IsAwake() {
        return Box2DWrapJNI.b2Body_IsAwake(this.swigCPtr);
    }

    public void SetActive(boolean flag) {
        Box2DWrapJNI.b2Body_SetActive(this.swigCPtr, flag);
    }

    public boolean IsActive() {
        return Box2DWrapJNI.b2Body_IsActive(this.swigCPtr);
    }

    public void SetFixedRotation(boolean flag) {
        Box2DWrapJNI.b2Body_SetFixedRotation(this.swigCPtr, flag);
    }

    public boolean IsFixedRotation() {
        return Box2DWrapJNI.b2Body_IsFixedRotation(this.swigCPtr);
    }

    public void SetId(int id) {
        Box2DWrapJNI.b2Body_SetId(this.swigCPtr, id);
    }

    public int GetId() {
        return Box2DWrapJNI.b2Body_GetId(this.swigCPtr);
    }

    public void SetCollisionCallback(boolean collisionCallback) {
        Box2DWrapJNI.b2Body_SetCollisionCallback(this.swigCPtr, collisionCallback);
    }

    public boolean GetCollisionCallback() {
        return Box2DWrapJNI.b2Body_GetCollisionCallback(this.swigCPtr);
    }

    public void SetCollisionCallback2(int collisionCallbackCount) {
        Box2DWrapJNI.b2Body_SetCollisionCallback2(this.swigCPtr, collisionCallbackCount);
    }

    public int GetCollisionCallback2() {
        return Box2DWrapJNI.b2Body_GetCollisionCallback2(this.swigCPtr);
    }

    public void setFilter(int categoryBits, int maskBits, int groupIndex) {
        Box2DWrapJNI.b2Body_setFilter(this.swigCPtr, categoryBits, maskBits, groupIndex);
    }

    public void setDensity(float density) {
        Box2DWrapJNI.b2Body_setDensity(this.swigCPtr, density);
    }

    public void setFriction(float friction) {
        Box2DWrapJNI.b2Body_setFriction(this.swigCPtr, friction);
    }

    public void setRestitution(float restitution) {
        Box2DWrapJNI.b2Body_setRestitution(this.swigCPtr, restitution);
    }
}
