package com.box2d;

public class b2JointDef {
    protected boolean swigCMemOwn;
    private int swigCPtr;

    protected b2JointDef(int cPtr, boolean cMemoryOwn) {
        this.swigCMemOwn = cMemoryOwn;
        this.swigCPtr = cPtr;
    }

    protected static int getCPtr(b2JointDef obj) {
        if (obj == null) {
            return 0;
        }
        return obj.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public synchronized void delete() {
        if (this.swigCPtr != 0) {
            if (this.swigCMemOwn) {
                this.swigCMemOwn = false;
                Box2DWrapJNI.delete_b2JointDef(this.swigCPtr);
            }
            this.swigCPtr = 0;
        }
    }

    public b2JointDef() {
        this(Box2DWrapJNI.new_b2JointDef(), true);
    }

    public void setBodyA(b2Body value) {
        Box2DWrapJNI.b2JointDef_bodyA_set(this.swigCPtr, b2Body.getCPtr(value));
    }

    public b2Body getBodyA() {
        int cPtr = Box2DWrapJNI.b2JointDef_bodyA_get(this.swigCPtr);
        if (cPtr == 0) {
            return null;
        }
        return new b2Body(cPtr, false);
    }

    public void setBodyB(b2Body value) {
        Box2DWrapJNI.b2JointDef_bodyB_set(this.swigCPtr, b2Body.getCPtr(value));
    }

    public b2Body getBodyB() {
        int cPtr = Box2DWrapJNI.b2JointDef_bodyB_get(this.swigCPtr);
        if (cPtr == 0) {
            return null;
        }
        return new b2Body(cPtr, false);
    }

    public void setCollideConnected(boolean value) {
        Box2DWrapJNI.b2JointDef_collideConnected_set(this.swigCPtr, value);
    }

    public boolean getCollideConnected() {
        return Box2DWrapJNI.b2JointDef_collideConnected_get(this.swigCPtr);
    }
}
