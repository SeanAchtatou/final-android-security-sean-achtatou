package com.box2d;

public final class b2BodyType {
    public static final b2BodyType b2_dynamicBody = new b2BodyType("b2_dynamicBody");
    public static final b2BodyType b2_kinematicBody = new b2BodyType("b2_kinematicBody");
    public static final b2BodyType b2_staticBody = new b2BodyType("b2_staticBody", Box2DWrapJNI.b2_staticBody_get());
    private static int swigNext = 0;
    private static b2BodyType[] swigValues = {b2_staticBody, b2_kinematicBody, b2_dynamicBody};
    private final String swigName;
    private final int swigValue;

    public final int swigValue() {
        return this.swigValue;
    }

    public String toString() {
        return this.swigName;
    }

    public static b2BodyType swigToEnum(int swigValue2) {
        if (swigValue2 < swigValues.length && swigValue2 >= 0 && swigValues[swigValue2].swigValue == swigValue2) {
            return swigValues[swigValue2];
        }
        for (int i = 0; i < swigValues.length; i++) {
            if (swigValues[i].swigValue == swigValue2) {
                return swigValues[i];
            }
        }
        throw new IllegalArgumentException("No enum " + b2BodyType.class + " with value " + swigValue2);
    }

    private b2BodyType(String swigName2) {
        this.swigName = swigName2;
        int i = swigNext;
        swigNext = i + 1;
        this.swigValue = i;
    }

    private b2BodyType(String swigName2, int swigValue2) {
        this.swigName = swigName2;
        this.swigValue = swigValue2;
        swigNext = swigValue2 + 1;
    }

    private b2BodyType(String swigName2, b2BodyType swigEnum) {
        this.swigName = swigName2;
        this.swigValue = swigEnum.swigValue;
        swigNext = this.swigValue + 1;
    }
}
