package com.box2d;

public class b2LineJointDef extends b2JointDef {
    private int swigCPtr;

    protected b2LineJointDef(int cPtr, boolean cMemoryOwn) {
        super(Box2DWrapJNI.SWIGb2LineJointDefUpcast(cPtr), cMemoryOwn);
        this.swigCPtr = cPtr;
    }

    protected static int getCPtr(b2LineJointDef obj) {
        if (obj == null) {
            return 0;
        }
        return obj.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public synchronized void delete() {
        if (this.swigCPtr != 0) {
            if (this.swigCMemOwn) {
                this.swigCMemOwn = false;
                Box2DWrapJNI.delete_b2LineJointDef(this.swigCPtr);
            }
            this.swigCPtr = 0;
        }
        super.delete();
    }

    public b2LineJointDef() {
        this(Box2DWrapJNI.new_b2LineJointDef(), true);
    }

    public void Initialize(b2Body bodyA, b2Body bodyB, b2Vec2 anchor, b2Vec2 axis) {
        Box2DWrapJNI.b2LineJointDef_Initialize(this.swigCPtr, b2Body.getCPtr(bodyA), b2Body.getCPtr(bodyB), b2Vec2.getCPtr(anchor), b2Vec2.getCPtr(axis));
    }

    public void setLocalAnchorA(b2Vec2 value) {
        Box2DWrapJNI.b2LineJointDef_localAnchorA_set(this.swigCPtr, b2Vec2.getCPtr(value));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.box2d.b2Vec2.<init>(int, boolean):void
     arg types: [int, int]
     candidates:
      com.box2d.b2Vec2.<init>(float, float):void
      com.box2d.b2Vec2.<init>(int, boolean):void */
    public b2Vec2 getLocalAnchorA() {
        int cPtr = Box2DWrapJNI.b2LineJointDef_localAnchorA_get(this.swigCPtr);
        if (cPtr == 0) {
            return null;
        }
        return new b2Vec2(cPtr, false);
    }

    public void setLocalAnchorB(b2Vec2 value) {
        Box2DWrapJNI.b2LineJointDef_localAnchorB_set(this.swigCPtr, b2Vec2.getCPtr(value));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.box2d.b2Vec2.<init>(int, boolean):void
     arg types: [int, int]
     candidates:
      com.box2d.b2Vec2.<init>(float, float):void
      com.box2d.b2Vec2.<init>(int, boolean):void */
    public b2Vec2 getLocalAnchorB() {
        int cPtr = Box2DWrapJNI.b2LineJointDef_localAnchorB_get(this.swigCPtr);
        if (cPtr == 0) {
            return null;
        }
        return new b2Vec2(cPtr, false);
    }

    public void setLocalAxisA(b2Vec2 value) {
        Box2DWrapJNI.b2LineJointDef_localAxisA_set(this.swigCPtr, b2Vec2.getCPtr(value));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.box2d.b2Vec2.<init>(int, boolean):void
     arg types: [int, int]
     candidates:
      com.box2d.b2Vec2.<init>(float, float):void
      com.box2d.b2Vec2.<init>(int, boolean):void */
    public b2Vec2 getLocalAxisA() {
        int cPtr = Box2DWrapJNI.b2LineJointDef_localAxisA_get(this.swigCPtr);
        if (cPtr == 0) {
            return null;
        }
        return new b2Vec2(cPtr, false);
    }

    public void setEnableLimit(boolean value) {
        Box2DWrapJNI.b2LineJointDef_enableLimit_set(this.swigCPtr, value);
    }

    public boolean getEnableLimit() {
        return Box2DWrapJNI.b2LineJointDef_enableLimit_get(this.swigCPtr);
    }

    public void setLowerTranslation(float value) {
        Box2DWrapJNI.b2LineJointDef_lowerTranslation_set(this.swigCPtr, value);
    }

    public float getLowerTranslation() {
        return Box2DWrapJNI.b2LineJointDef_lowerTranslation_get(this.swigCPtr);
    }

    public void setUpperTranslation(float value) {
        Box2DWrapJNI.b2LineJointDef_upperTranslation_set(this.swigCPtr, value);
    }

    public float getUpperTranslation() {
        return Box2DWrapJNI.b2LineJointDef_upperTranslation_get(this.swigCPtr);
    }

    public void setEnableMotor(boolean value) {
        Box2DWrapJNI.b2LineJointDef_enableMotor_set(this.swigCPtr, value);
    }

    public boolean getEnableMotor() {
        return Box2DWrapJNI.b2LineJointDef_enableMotor_get(this.swigCPtr);
    }

    public void setMaxMotorForce(float value) {
        Box2DWrapJNI.b2LineJointDef_maxMotorForce_set(this.swigCPtr, value);
    }

    public float getMaxMotorForce() {
        return Box2DWrapJNI.b2LineJointDef_maxMotorForce_get(this.swigCPtr);
    }

    public void setMotorSpeed(float value) {
        Box2DWrapJNI.b2LineJointDef_motorSpeed_set(this.swigCPtr, value);
    }

    public float getMotorSpeed() {
        return Box2DWrapJNI.b2LineJointDef_motorSpeed_get(this.swigCPtr);
    }
}
