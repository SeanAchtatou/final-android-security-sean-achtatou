package com.box2d;

public class b2Vec2 {
    protected boolean swigCMemOwn;
    private int swigCPtr;

    protected b2Vec2(int cPtr, boolean cMemoryOwn) {
        this.swigCMemOwn = cMemoryOwn;
        this.swigCPtr = cPtr;
    }

    protected static int getCPtr(b2Vec2 obj) {
        if (obj == null) {
            return 0;
        }
        return obj.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public synchronized void delete() {
        if (this.swigCPtr != 0) {
            if (this.swigCMemOwn) {
                this.swigCMemOwn = false;
                Box2DWrapJNI.delete_b2Vec2(this.swigCPtr);
            }
            this.swigCPtr = 0;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.box2d.b2Vec2.<init>(int, boolean):void
     arg types: [int, int]
     candidates:
      com.box2d.b2Vec2.<init>(float, float):void
      com.box2d.b2Vec2.<init>(int, boolean):void */
    public b2Vec2() {
        this(Box2DWrapJNI.new_b2Vec2__SWIG_0(), true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.box2d.b2Vec2.<init>(int, boolean):void
     arg types: [int, int]
     candidates:
      com.box2d.b2Vec2.<init>(float, float):void
      com.box2d.b2Vec2.<init>(int, boolean):void */
    public b2Vec2(float x, float y) {
        this(Box2DWrapJNI.new_b2Vec2__SWIG_1(x, y), true);
    }

    public void SetZero() {
        Box2DWrapJNI.b2Vec2_SetZero(this.swigCPtr);
    }

    public void Set(float x_, float y_) {
        Box2DWrapJNI.b2Vec2_Set(this.swigCPtr, x_, y_);
    }

    public float Length() {
        return Box2DWrapJNI.b2Vec2_Length(this.swigCPtr);
    }

    public float LengthSquared() {
        return Box2DWrapJNI.b2Vec2_LengthSquared(this.swigCPtr);
    }

    public float Normalize() {
        return Box2DWrapJNI.b2Vec2_Normalize(this.swigCPtr);
    }

    public boolean IsValid() {
        return Box2DWrapJNI.b2Vec2_IsValid(this.swigCPtr);
    }

    public void setX(float value) {
        Box2DWrapJNI.b2Vec2_x_set(this.swigCPtr, value);
    }

    public float getX() {
        return Box2DWrapJNI.b2Vec2_x_get(this.swigCPtr);
    }

    public void setY(float value) {
        Box2DWrapJNI.b2Vec2_y_set(this.swigCPtr, value);
    }

    public float getY() {
        return Box2DWrapJNI.b2Vec2_y_get(this.swigCPtr);
    }
}
