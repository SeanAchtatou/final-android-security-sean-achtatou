package com.box2d;

public class b2MyVertices {
    protected boolean swigCMemOwn;
    private int swigCPtr;

    protected b2MyVertices(int cPtr, boolean cMemoryOwn) {
        this.swigCMemOwn = cMemoryOwn;
        this.swigCPtr = cPtr;
    }

    protected static int getCPtr(b2MyVertices obj) {
        if (obj == null) {
            return 0;
        }
        return obj.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public synchronized void delete() {
        if (this.swigCPtr != 0) {
            if (this.swigCMemOwn) {
                this.swigCMemOwn = false;
                Box2DWrapJNI.delete_b2MyVertices(this.swigCPtr);
            }
            this.swigCPtr = 0;
        }
    }

    public b2MyVertices(int count) {
        this(Box2DWrapJNI.new_b2MyVertices(count), true);
    }

    public void Set(float x, float y, int index) {
        Box2DWrapJNI.b2MyVertices_Set(this.swigCPtr, x, y, index);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.box2d.b2Vec2.<init>(int, boolean):void
     arg types: [int, int]
     candidates:
      com.box2d.b2Vec2.<init>(float, float):void
      com.box2d.b2Vec2.<init>(int, boolean):void */
    public b2Vec2 Get(int index) {
        int cPtr = Box2DWrapJNI.b2MyVertices_Get(this.swigCPtr, index);
        if (cPtr == 0) {
            return null;
        }
        return new b2Vec2(cPtr, false);
    }

    public int GetCount() {
        return Box2DWrapJNI.b2MyVertices_GetCount(this.swigCPtr);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.box2d.b2Vec2.<init>(int, boolean):void
     arg types: [int, int]
     candidates:
      com.box2d.b2Vec2.<init>(float, float):void
      com.box2d.b2Vec2.<init>(int, boolean):void */
    public b2Vec2 GetVertices() {
        int cPtr = Box2DWrapJNI.b2MyVertices_GetVertices(this.swigCPtr);
        if (cPtr == 0) {
            return null;
        }
        return new b2Vec2(cPtr, false);
    }
}
