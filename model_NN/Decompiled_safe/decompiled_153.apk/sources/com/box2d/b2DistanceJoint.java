package com.box2d;

public class b2DistanceJoint extends b2Joint {
    private int swigCPtr;

    public b2DistanceJoint(int cPtr, boolean cMemoryOwn) {
        super(Box2DWrapJNI.SWIGb2DistanceJointUpcast(cPtr), cMemoryOwn);
        this.swigCPtr = cPtr;
    }

    protected static int getCPtr(b2DistanceJoint obj) {
        if (obj == null) {
            return 0;
        }
        return obj.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public synchronized void delete() {
        if (this.swigCPtr != 0) {
            if (this.swigCMemOwn) {
                this.swigCMemOwn = false;
                Box2DWrapJNI.delete_b2DistanceJoint(this.swigCPtr);
            }
            this.swigCPtr = 0;
        }
        super.delete();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.box2d.b2Vec2.<init>(int, boolean):void
     arg types: [int, int]
     candidates:
      com.box2d.b2Vec2.<init>(float, float):void
      com.box2d.b2Vec2.<init>(int, boolean):void */
    public b2Vec2 GetAnchorA() {
        return new b2Vec2(Box2DWrapJNI.b2DistanceJoint_GetAnchorA(this.swigCPtr), true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.box2d.b2Vec2.<init>(int, boolean):void
     arg types: [int, int]
     candidates:
      com.box2d.b2Vec2.<init>(float, float):void
      com.box2d.b2Vec2.<init>(int, boolean):void */
    public b2Vec2 GetAnchorB() {
        return new b2Vec2(Box2DWrapJNI.b2DistanceJoint_GetAnchorB(this.swigCPtr), true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.box2d.b2Vec2.<init>(int, boolean):void
     arg types: [int, int]
     candidates:
      com.box2d.b2Vec2.<init>(float, float):void
      com.box2d.b2Vec2.<init>(int, boolean):void */
    public b2Vec2 GetReactionForce(float inv_dt) {
        return new b2Vec2(Box2DWrapJNI.b2DistanceJoint_GetReactionForce(this.swigCPtr, inv_dt), true);
    }

    public float GetReactionTorque(float inv_dt) {
        return Box2DWrapJNI.b2DistanceJoint_GetReactionTorque(this.swigCPtr, inv_dt);
    }

    public void SetLength(float length) {
        Box2DWrapJNI.b2DistanceJoint_SetLength(this.swigCPtr, length);
    }

    public float GetLength() {
        return Box2DWrapJNI.b2DistanceJoint_GetLength(this.swigCPtr);
    }

    public void SetFrequency(float hz) {
        Box2DWrapJNI.b2DistanceJoint_SetFrequency(this.swigCPtr, hz);
    }

    public float GetFrequency() {
        return Box2DWrapJNI.b2DistanceJoint_GetFrequency(this.swigCPtr);
    }

    public void SetDampingRatio(float ratio) {
        Box2DWrapJNI.b2DistanceJoint_SetDampingRatio(this.swigCPtr, ratio);
    }

    public float GetDampingRatio() {
        return Box2DWrapJNI.b2DistanceJoint_GetDampingRatio(this.swigCPtr);
    }
}
