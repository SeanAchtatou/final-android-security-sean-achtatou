package com.box2d;

public interface Box2DWrapConstants {
    public static final double b2_aabbExtension = Box2DWrapJNI.b2_aabbExtension_get();
    public static final double b2_aabbMultiplier = Box2DWrapJNI.b2_aabbMultiplier_get();
    public static final double b2_angularSleepTolerance = Box2DWrapJNI.b2_angularSleepTolerance_get();
    public static final double b2_angularSlop = Box2DWrapJNI.b2_angularSlop_get();
    public static final double b2_contactBaumgarte = Box2DWrapJNI.b2_contactBaumgarte_get();
    public static final double b2_linearSleepTolerance = Box2DWrapJNI.b2_linearSleepTolerance_get();
    public static final double b2_linearSlop = Box2DWrapJNI.b2_linearSlop_get();
    public static final double b2_maxAngularCorrection = Box2DWrapJNI.b2_maxAngularCorrection_get();
    public static final double b2_maxLinearCorrection = Box2DWrapJNI.b2_maxLinearCorrection_get();
    public static final int b2_maxManifoldPoints = Box2DWrapJNI.b2_maxManifoldPoints_get();
    public static final int b2_maxPolygonVertices = Box2DWrapJNI.b2_maxPolygonVertices_get();
    public static final double b2_maxRotation = Box2DWrapJNI.b2_maxRotation_get();
    public static final double b2_maxRotationSquared = Box2DWrapJNI.b2_maxRotationSquared_get();
    public static final int b2_maxSubSteps = Box2DWrapJNI.b2_maxSubSteps_get();
    public static final int b2_maxTOIContacts = Box2DWrapJNI.b2_maxTOIContacts_get();
    public static final double b2_maxTranslation = Box2DWrapJNI.b2_maxTranslation_get();
    public static final double b2_maxTranslationSquared = Box2DWrapJNI.b2_maxTranslationSquared_get();
    public static final double b2_pi = Box2DWrapJNI.b2_pi_get();
    public static final double b2_polygonRadius = Box2DWrapJNI.b2_polygonRadius_get();
    public static final double b2_timeToSleep = Box2DWrapJNI.b2_timeToSleep_get();
    public static final double b2_velocityThreshold = Box2DWrapJNI.b2_velocityThreshold_get();
}
