package com.box2d;

public class b2Joint {
    protected boolean swigCMemOwn;
    private int swigCPtr;

    protected b2Joint(int cPtr, boolean cMemoryOwn) {
        this.swigCMemOwn = cMemoryOwn;
        this.swigCPtr = cPtr;
    }

    public static int getCPtr(b2Joint obj) {
        if (obj == null) {
            return 0;
        }
        return obj.swigCPtr;
    }

    public synchronized void delete() {
        if (this.swigCPtr != 0) {
            if (this.swigCMemOwn) {
                this.swigCMemOwn = false;
                throw new UnsupportedOperationException("C++ destructor does not have public access");
            }
            this.swigCPtr = 0;
        }
    }

    public b2Body GetBodyA() {
        int cPtr = Box2DWrapJNI.b2Joint_GetBodyA(this.swigCPtr);
        if (cPtr == 0) {
            return null;
        }
        return new b2Body(cPtr, false);
    }

    public b2Body GetBodyB() {
        int cPtr = Box2DWrapJNI.b2Joint_GetBodyB(this.swigCPtr);
        if (cPtr == 0) {
            return null;
        }
        return new b2Body(cPtr, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.box2d.b2Vec2.<init>(int, boolean):void
     arg types: [int, int]
     candidates:
      com.box2d.b2Vec2.<init>(float, float):void
      com.box2d.b2Vec2.<init>(int, boolean):void */
    public b2Vec2 GetAnchorA() {
        return new b2Vec2(Box2DWrapJNI.b2Joint_GetAnchorA(this.swigCPtr), true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.box2d.b2Vec2.<init>(int, boolean):void
     arg types: [int, int]
     candidates:
      com.box2d.b2Vec2.<init>(float, float):void
      com.box2d.b2Vec2.<init>(int, boolean):void */
    public b2Vec2 GetAnchorB() {
        return new b2Vec2(Box2DWrapJNI.b2Joint_GetAnchorB(this.swigCPtr), true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.box2d.b2Vec2.<init>(int, boolean):void
     arg types: [int, int]
     candidates:
      com.box2d.b2Vec2.<init>(float, float):void
      com.box2d.b2Vec2.<init>(int, boolean):void */
    public b2Vec2 GetReactionForce(float inv_dt) {
        return new b2Vec2(Box2DWrapJNI.b2Joint_GetReactionForce(this.swigCPtr, inv_dt), true);
    }

    public float GetReactionTorque(float inv_dt) {
        return Box2DWrapJNI.b2Joint_GetReactionTorque(this.swigCPtr, inv_dt);
    }

    public boolean IsActive() {
        return Box2DWrapJNI.b2Joint_IsActive(this.swigCPtr);
    }
}
