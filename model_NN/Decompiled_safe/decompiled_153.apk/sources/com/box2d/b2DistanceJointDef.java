package com.box2d;

public class b2DistanceJointDef extends b2JointDef {
    private int swigCPtr;

    protected b2DistanceJointDef(int cPtr, boolean cMemoryOwn) {
        super(Box2DWrapJNI.SWIGb2DistanceJointDefUpcast(cPtr), cMemoryOwn);
        this.swigCPtr = cPtr;
    }

    protected static int getCPtr(b2DistanceJointDef obj) {
        if (obj == null) {
            return 0;
        }
        return obj.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public synchronized void delete() {
        if (this.swigCPtr != 0) {
            if (this.swigCMemOwn) {
                this.swigCMemOwn = false;
                Box2DWrapJNI.delete_b2DistanceJointDef(this.swigCPtr);
            }
            this.swigCPtr = 0;
        }
        super.delete();
    }

    public b2DistanceJointDef() {
        this(Box2DWrapJNI.new_b2DistanceJointDef(), true);
    }

    public void Initialize(b2Body bodyA, b2Body bodyB, b2Vec2 anchorA, b2Vec2 anchorB) {
        Box2DWrapJNI.b2DistanceJointDef_Initialize(this.swigCPtr, b2Body.getCPtr(bodyA), b2Body.getCPtr(bodyB), b2Vec2.getCPtr(anchorA), b2Vec2.getCPtr(anchorB));
    }

    public void setLocalAnchorA(b2Vec2 value) {
        Box2DWrapJNI.b2DistanceJointDef_localAnchorA_set(this.swigCPtr, b2Vec2.getCPtr(value));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.box2d.b2Vec2.<init>(int, boolean):void
     arg types: [int, int]
     candidates:
      com.box2d.b2Vec2.<init>(float, float):void
      com.box2d.b2Vec2.<init>(int, boolean):void */
    public b2Vec2 getLocalAnchorA() {
        int cPtr = Box2DWrapJNI.b2DistanceJointDef_localAnchorA_get(this.swigCPtr);
        if (cPtr == 0) {
            return null;
        }
        return new b2Vec2(cPtr, false);
    }

    public void setLocalAnchorB(b2Vec2 value) {
        Box2DWrapJNI.b2DistanceJointDef_localAnchorB_set(this.swigCPtr, b2Vec2.getCPtr(value));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.box2d.b2Vec2.<init>(int, boolean):void
     arg types: [int, int]
     candidates:
      com.box2d.b2Vec2.<init>(float, float):void
      com.box2d.b2Vec2.<init>(int, boolean):void */
    public b2Vec2 getLocalAnchorB() {
        int cPtr = Box2DWrapJNI.b2DistanceJointDef_localAnchorB_get(this.swigCPtr);
        if (cPtr == 0) {
            return null;
        }
        return new b2Vec2(cPtr, false);
    }

    public void setLength(float value) {
        Box2DWrapJNI.b2DistanceJointDef_length_set(this.swigCPtr, value);
    }

    public float getLength() {
        return Box2DWrapJNI.b2DistanceJointDef_length_get(this.swigCPtr);
    }

    public void setFrequencyHz(float value) {
        Box2DWrapJNI.b2DistanceJointDef_frequencyHz_set(this.swigCPtr, value);
    }

    public float getFrequencyHz() {
        return Box2DWrapJNI.b2DistanceJointDef_frequencyHz_get(this.swigCPtr);
    }

    public void setDampingRatio(float value) {
        Box2DWrapJNI.b2DistanceJointDef_dampingRatio_set(this.swigCPtr, value);
    }

    public float getDampingRatio() {
        return Box2DWrapJNI.b2DistanceJointDef_dampingRatio_get(this.swigCPtr);
    }
}
