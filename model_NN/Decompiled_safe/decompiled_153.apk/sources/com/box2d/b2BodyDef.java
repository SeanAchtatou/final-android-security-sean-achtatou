package com.box2d;

public class b2BodyDef {
    protected boolean swigCMemOwn;
    private int swigCPtr;

    protected b2BodyDef(int cPtr, boolean cMemoryOwn) {
        this.swigCMemOwn = cMemoryOwn;
        this.swigCPtr = cPtr;
    }

    protected static int getCPtr(b2BodyDef obj) {
        if (obj == null) {
            return 0;
        }
        return obj.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public synchronized void delete() {
        if (this.swigCPtr != 0) {
            if (this.swigCMemOwn) {
                this.swigCMemOwn = false;
                Box2DWrapJNI.delete_b2BodyDef(this.swigCPtr);
            }
            this.swigCPtr = 0;
        }
    }

    public void setType(b2BodyType value) {
        Box2DWrapJNI.b2BodyDef_type_set(this.swigCPtr, value.swigValue());
    }

    public b2BodyType getType() {
        return b2BodyType.swigToEnum(Box2DWrapJNI.b2BodyDef_type_get(this.swigCPtr));
    }

    public void setPosition(b2Vec2 value) {
        Box2DWrapJNI.b2BodyDef_position_set(this.swigCPtr, b2Vec2.getCPtr(value));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.box2d.b2Vec2.<init>(int, boolean):void
     arg types: [int, int]
     candidates:
      com.box2d.b2Vec2.<init>(float, float):void
      com.box2d.b2Vec2.<init>(int, boolean):void */
    public b2Vec2 getPosition() {
        int cPtr = Box2DWrapJNI.b2BodyDef_position_get(this.swigCPtr);
        if (cPtr == 0) {
            return null;
        }
        return new b2Vec2(cPtr, false);
    }

    public void setPosition(float x, float y) {
        Box2DWrapJNI.b2BodyDef_setPosition(this.swigCPtr, x, y);
    }

    public void setAngle(float value) {
        Box2DWrapJNI.b2BodyDef_angle_set(this.swigCPtr, value);
    }

    public float getAngle() {
        return Box2DWrapJNI.b2BodyDef_angle_get(this.swigCPtr);
    }

    public void setLinearVelocity(b2Vec2 value) {
        Box2DWrapJNI.b2BodyDef_linearVelocity_set(this.swigCPtr, b2Vec2.getCPtr(value));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.box2d.b2Vec2.<init>(int, boolean):void
     arg types: [int, int]
     candidates:
      com.box2d.b2Vec2.<init>(float, float):void
      com.box2d.b2Vec2.<init>(int, boolean):void */
    public b2Vec2 getLinearVelocity() {
        int cPtr = Box2DWrapJNI.b2BodyDef_linearVelocity_get(this.swigCPtr);
        if (cPtr == 0) {
            return null;
        }
        return new b2Vec2(cPtr, false);
    }

    public void setAngularVelocity(float value) {
        Box2DWrapJNI.b2BodyDef_angularVelocity_set(this.swigCPtr, value);
    }

    public float getAngularVelocity() {
        return Box2DWrapJNI.b2BodyDef_angularVelocity_get(this.swigCPtr);
    }

    public void setLinearDamping(float value) {
        Box2DWrapJNI.b2BodyDef_linearDamping_set(this.swigCPtr, value);
    }

    public float getLinearDamping() {
        return Box2DWrapJNI.b2BodyDef_linearDamping_get(this.swigCPtr);
    }

    public void setAngularDamping(float value) {
        Box2DWrapJNI.b2BodyDef_angularDamping_set(this.swigCPtr, value);
    }

    public float getAngularDamping() {
        return Box2DWrapJNI.b2BodyDef_angularDamping_get(this.swigCPtr);
    }

    public void setAllowSleep(boolean value) {
        Box2DWrapJNI.b2BodyDef_allowSleep_set(this.swigCPtr, value);
    }

    public boolean getAllowSleep() {
        return Box2DWrapJNI.b2BodyDef_allowSleep_get(this.swigCPtr);
    }

    public void setAwake(boolean value) {
        Box2DWrapJNI.b2BodyDef_awake_set(this.swigCPtr, value);
    }

    public boolean getAwake() {
        return Box2DWrapJNI.b2BodyDef_awake_get(this.swigCPtr);
    }

    public void setFixedRotation(boolean value) {
        Box2DWrapJNI.b2BodyDef_fixedRotation_set(this.swigCPtr, value);
    }

    public boolean getFixedRotation() {
        return Box2DWrapJNI.b2BodyDef_fixedRotation_get(this.swigCPtr);
    }

    public void setBullet(boolean value) {
        Box2DWrapJNI.b2BodyDef_bullet_set(this.swigCPtr, value);
    }

    public boolean getBullet() {
        return Box2DWrapJNI.b2BodyDef_bullet_get(this.swigCPtr);
    }

    public void setActive(boolean value) {
        Box2DWrapJNI.b2BodyDef_active_set(this.swigCPtr, value);
    }

    public boolean getActive() {
        return Box2DWrapJNI.b2BodyDef_active_get(this.swigCPtr);
    }

    public void setInertiaScale(float value) {
        Box2DWrapJNI.b2BodyDef_inertiaScale_set(this.swigCPtr, value);
    }

    public float getInertiaScale() {
        return Box2DWrapJNI.b2BodyDef_inertiaScale_get(this.swigCPtr);
    }

    public b2BodyDef() {
        this(Box2DWrapJNI.new_b2BodyDef(), true);
    }
}
