package com.box2d;

public class b2MouseJoint extends b2Joint {
    private int swigCPtr;

    public b2MouseJoint(int cPtr, boolean cMemoryOwn) {
        super(Box2DWrapJNI.SWIGb2MouseJointUpcast(cPtr), cMemoryOwn);
        this.swigCPtr = cPtr;
    }

    protected static int getCPtr(b2MouseJoint obj) {
        if (obj == null) {
            return 0;
        }
        return obj.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public synchronized void delete() {
        if (this.swigCPtr != 0) {
            if (this.swigCMemOwn) {
                this.swigCMemOwn = false;
                Box2DWrapJNI.delete_b2MouseJoint(this.swigCPtr);
            }
            this.swigCPtr = 0;
        }
        super.delete();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.box2d.b2Vec2.<init>(int, boolean):void
     arg types: [int, int]
     candidates:
      com.box2d.b2Vec2.<init>(float, float):void
      com.box2d.b2Vec2.<init>(int, boolean):void */
    public b2Vec2 GetAnchorA() {
        return new b2Vec2(Box2DWrapJNI.b2MouseJoint_GetAnchorA(this.swigCPtr), true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.box2d.b2Vec2.<init>(int, boolean):void
     arg types: [int, int]
     candidates:
      com.box2d.b2Vec2.<init>(float, float):void
      com.box2d.b2Vec2.<init>(int, boolean):void */
    public b2Vec2 GetAnchorB() {
        return new b2Vec2(Box2DWrapJNI.b2MouseJoint_GetAnchorB(this.swigCPtr), true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.box2d.b2Vec2.<init>(int, boolean):void
     arg types: [int, int]
     candidates:
      com.box2d.b2Vec2.<init>(float, float):void
      com.box2d.b2Vec2.<init>(int, boolean):void */
    public b2Vec2 GetReactionForce(float inv_dt) {
        return new b2Vec2(Box2DWrapJNI.b2MouseJoint_GetReactionForce(this.swigCPtr, inv_dt), true);
    }

    public float GetReactionTorque(float inv_dt) {
        return Box2DWrapJNI.b2MouseJoint_GetReactionTorque(this.swigCPtr, inv_dt);
    }

    public void SetTarget(b2Vec2 target) {
        Box2DWrapJNI.b2MouseJoint_SetTarget__SWIG_0(this.swigCPtr, b2Vec2.getCPtr(target));
    }

    public void SetTarget(float targetX, float targetY) {
        Box2DWrapJNI.b2MouseJoint_SetTarget__SWIG_1(this.swigCPtr, targetX, targetY);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.box2d.b2Vec2.<init>(int, boolean):void
     arg types: [int, int]
     candidates:
      com.box2d.b2Vec2.<init>(float, float):void
      com.box2d.b2Vec2.<init>(int, boolean):void */
    public b2Vec2 GetTarget() {
        return new b2Vec2(Box2DWrapJNI.b2MouseJoint_GetTarget(this.swigCPtr), false);
    }

    public void SetMaxForce(float force) {
        Box2DWrapJNI.b2MouseJoint_SetMaxForce(this.swigCPtr, force);
    }

    public float GetMaxForce() {
        return Box2DWrapJNI.b2MouseJoint_GetMaxForce(this.swigCPtr);
    }

    public void SetFrequency(float hz) {
        Box2DWrapJNI.b2MouseJoint_SetFrequency(this.swigCPtr, hz);
    }

    public float GetFrequency() {
        return Box2DWrapJNI.b2MouseJoint_GetFrequency(this.swigCPtr);
    }

    public void SetDampingRatio(float ratio) {
        Box2DWrapJNI.b2MouseJoint_SetDampingRatio(this.swigCPtr, ratio);
    }

    public float GetDampingRatio() {
        return Box2DWrapJNI.b2MouseJoint_GetDampingRatio(this.swigCPtr);
    }
}
