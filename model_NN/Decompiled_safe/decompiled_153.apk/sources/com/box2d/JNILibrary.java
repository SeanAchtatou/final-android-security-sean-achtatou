package com.box2d;

public class JNILibrary {
    public static final native void setMyHelper(int i);

    public static final native void syncData(int i, float[] fArr);
}
